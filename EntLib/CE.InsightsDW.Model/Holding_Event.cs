//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace CE.InsightsDW.Model
{
    using System;
    using System.Collections.Generic;
    
    public partial class Holding_Event
    {
        public int ClientID { get; set; }
        public string CustomerID { get; set; }
        public string AccountID { get; set; }
        public string PremiseID { get; set; }
        public System.DateTime EventDate { get; set; }
        public int EventTypeID { get; set; }
        public Nullable<System.DateTime> FlagDateCreated { get; set; }
        public Nullable<System.DateTime> FlagDateUpdated { get; set; }
        public string TrackingId { get; set; }
        public System.DateTime TrackingDate { get; set; }
        public string EmailID { get; set; }
        public string Program { get; set; }
    }
}
