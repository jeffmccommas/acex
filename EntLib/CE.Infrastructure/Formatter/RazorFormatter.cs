﻿using System;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Net.Http.Headers;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using RazorEngine;

namespace CE.Infrastructure.Formatter
{

    public class RazorFormatter : MediaTypeFormatter
    {
        private string SITEROOTPATH = @"~";
        private static Regex reg = new Regex(@"(?<=[^])\t{2,}|(?<=[>])\s{2,}(?=[<])|(?<=[>])\s{2,11}(?=[<])|(?=[\n])\s{2,}", RegexOptions.Multiline | RegexOptions.Compiled);

        public RazorFormatter(string root)
        {
            SupportedMediaTypes.Add(new MediaTypeHeaderValue("text/html"));
            SupportedMediaTypes.Add(new MediaTypeHeaderValue("application/xhtml+xml"));
            SITEROOTPATH = root;
        }
        public override bool CanReadType(Type type)
        {
            return true;

        }
        public override bool CanWriteType(Type type)
        {
            return true;
        }

        public override Task WriteToStreamAsync(Type type,
                                                object value,
                                                Stream stream,
                                                HttpContent content,
                                                TransportContext transportContext)
        {

            var task = Task.Factory.StartNew(() =>
                {
                    if (!type.Name.Contains("String") && !type.Name.Contains("IQueryable`1"))
                    {
                        var loc = new RazorViewLocator();
                        var template = loc.GetView(SITEROOTPATH, type.Name);

                        //var tplCfg = new RazorEngine.Configuration.TemplateServiceConfiguration();
                        //tplCfg.BaseTemplateType = typeof(System.Web.Mvc.HtmlTemplateBase<>);
                        //var tplSvc = new TemplateService(tplCfg);
                        //Razor.SetTemplateService(tplSvc);

                        Razor.Compile(template, type, type.Name);
                        var razor = Razor.Run(type.Name, value);
                        var buf = System.Text.Encoding.Default.GetBytes(razor);

                        string html = System.Text.Encoding.Default.GetString(buf);
                        html = reg.Replace(html, string.Empty);
                        buf = System.Text.Encoding.Default.GetBytes(html);

                        stream.Write(buf, 0, buf.Length);
                        stream.Flush();
                    }
                });
            return task;
        }
    }

    public interface IViewLocator
    {
        string GetView(string siteRootPath, string view);
    }

    public class RazorViewLocator : IViewLocator
    {
        private readonly string[] viewLocationFormats = new[]
        {             
             "~\\Views\\{0}.cshtml",
             "~\\Views\\{0}.vbhtml",
             "~\\Views\\Shared\\{0}.cshtml",
             "~\\Views\\Shared\\{0}.vbhtml"
         };


        public string GetView(string siteRootPath, string view)
        {
            if (view == null)
                throw new ArgumentNullException("view");

            var path = siteRootPath;

            foreach (string viewLocationFormat in viewLocationFormats)
            {
                var potentialViewPathFormat = viewLocationFormat.Replace("~", siteRootPath); ;
                var viewPath = string.Format(potentialViewPathFormat, view.ToString());
                if (File.Exists(viewPath))
                    return File.ReadAllText(viewPath);
            }

            throw new FileNotFoundException(string.Format("Can't find a view with the name '{0}.cshtml' or '{0}.vbhtml in the '\\Views' folder under  path '{1}'", view.ToString(), path));
        }
    }
}