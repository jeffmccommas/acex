﻿using System.Web.WebPages;
using RazorEngine.Templating;

namespace System.Web.Mvc
{
    [RequireNamespaces("System.Web.Mvc.Html")]
    public class HtmlTemplateBase<t> : TemplateBase<t>, IViewDataContainer
    {
        private HtmlHelper<t> helper = null;
        private ViewDataDictionary viewdata = null;
        private System.Dynamic.DynamicObject viewbag = null;
        private UrlHelper urlhelper = null;


        //public dynamic ViewBag
        //{
        //    get
        //    {
        //        return (WebPageContext.Current.Page as WebViewPage).ViewBag;
        //    }
        //}

        public HtmlHelper<t> Html
        {
            get
            {
                if (helper == null)
                {
                    var p1 = HttpContext.Current;
                    var p = WebPageContext.Current;
                    var wvp = p.Page as WebViewPage;
                    var context = wvp != null ? wvp.ViewContext : null;

                    helper = new HtmlHelper<t>(context, this);
                }
                return helper;
            }
        }

        public ViewDataDictionary ViewData
        {
            get
            {
                if (viewbag == null)
                {
                    var p = WebPageContext.Current;
                    var viewcontainer = p.Page as IViewDataContainer;
                    viewdata = new ViewDataDictionary(viewcontainer.ViewData);

                    if (this.Model != null)
                    {
                        viewdata.Model = Model;
                    }

                }

                return viewdata;
            }
            set
            {
                viewdata = value;
            }
        }

        public UrlHelper Url
        {
            get
            {
                if (urlhelper == null)
                {
                    
                    urlhelper = new UrlHelper(HttpContext.Current.Request.RequestContext);
                }
                return urlhelper;
            }
        }

    }

}


