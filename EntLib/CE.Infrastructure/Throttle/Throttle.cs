﻿using System;
using System.Linq;
using System.Net.Http;

namespace CE.Infrastructure
{
    public class ThrottlingHandlerWithFixedIdentifier : ThrottlingHandler
    {
        public ThrottlingHandlerWithFixedIdentifier(IThrottleStore store, Func<string, long> maxRequestsForUserIdentifier, TimeSpan period)
            : base(store, maxRequestsForUserIdentifier, period)
        {
        }

        //protected override string GetUserIdentifier(HttpRequestMessage request)
        //{
        //    if (!request.Headers.Contains(HeaderConfiguration.UsernameHeader))
        //    {
        //        //return "";

        //        // For now to enable browser output, return IP
        //        return "10.0.0.1";
        //    }
        //    else
        //    {
        //        return request.Headers.GetValues(HeaderConfiguration.UsernameHeader).First();
        //    }
        //}
    }

}