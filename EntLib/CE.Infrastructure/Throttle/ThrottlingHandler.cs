﻿using System;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;

namespace CE.Infrastructure
{
    public class ThrottlingHandler
        : DelegatingHandler
    {
        private readonly IThrottleStore _store;
        private readonly Func<string, long> _maxRequestsForUserIdentifier;
        private readonly TimeSpan _period;
        private readonly string _message;

        protected ThrottlingHandler(IThrottleStore store, Func<string, long> maxRequestsForUserIdentifier, TimeSpan period)
            : this(store, maxRequestsForUserIdentifier, period, "The allowed number of requests has been exceeded.")
        {
        }

        private ThrottlingHandler(IThrottleStore store, Func<string, long> maxRequestsForUserIdentifier, TimeSpan period, string message)
        {
            _store = store;
            _maxRequestsForUserIdentifier = maxRequestsForUserIdentifier;
            _period = period;
            _message = message;
        }

        private static string GetUserIdentifier(HttpRequestMessage request) => request.GetClientIpAddress();

        protected override Task<HttpResponseMessage> SendAsync(HttpRequestMessage request, CancellationToken cancellationToken)
        {
            Task<HttpResponseMessage> response;
            if (request.RequestUri.AbsolutePath.IndexOf("oauth/token", 0, StringComparison.Ordinal) > 0 ||
                request.RequestUri.AbsolutePath.IndexOf("resource", 0, StringComparison.Ordinal) > 0)
            {
                var identifier = GetUserIdentifier(request);

                if (string.IsNullOrEmpty(identifier))
                {
                    return CreateResponse(request, HttpStatusCode.Forbidden, "Could not identify client.");
                }

                var maxRequests = _maxRequestsForUserIdentifier(identifier);

                ThrottleEntry entry;
                if (_store.TryGetValue(identifier, out entry))
                {
                    if (entry.PeriodStart + _period < DateTime.UtcNow)
                    {
                        _store.Rollover(identifier);
                    }
                }
                _store.IncrementRequests(identifier);
                if (!_store.TryGetValue(identifier, out entry))
                {
                    return CreateResponse(request, HttpStatusCode.Forbidden, "Could not identify client.");
                }

                response = entry.Requests > maxRequests ? CreateResponse(request, HttpStatusCode.Conflict, _message) : base.SendAsync(request, cancellationToken);

                return response.ContinueWith(task =>
                {
                    var remaining = maxRequests - entry.Requests;
                    if (remaining < 0)
                    {
                        remaining = 0;
                    }

                    var httpResponse = task.Result;
                    httpResponse.Headers.Add("RateLimit-Limit", maxRequests.ToString());
                    httpResponse.Headers.Add("RateLimit-Remaining", remaining.ToString());
                    httpResponse.Headers.Add("RateLimit-Ip", identifier);

                    return httpResponse;
                }, cancellationToken);
            }

            response = base.SendAsync(request, cancellationToken);
            return response.ContinueWith(task =>
            {
                var httpResponse = task.Result;
                return httpResponse;
            }, cancellationToken);
        }

        private static Task<HttpResponseMessage> CreateResponse(HttpRequestMessage request, HttpStatusCode statusCode, string message)
        {
            var tsc = new TaskCompletionSource<HttpResponseMessage>();
            var response = request.CreateResponse(statusCode);
            response.ReasonPhrase = message;
            response.Content = new StringContent(message);
            tsc.SetResult(response);
            return tsc.Task;
        }
    }
}
