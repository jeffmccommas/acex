﻿using System;
using System.Linq;
using System.Net.Http.Headers;
using System.Text;

namespace CE.Infrastructure
{
    public class ApiCredentials
    {
        public string Username { get; set; }
        public string Signature { get; set; }

        public static ApiCredentials GetFromRequestHeaders(HttpRequestHeaders requestHeaders)
        {
            //this could/should be another interface for testing purposes
            var authenticationHeader = requestHeaders.Authorization;
            if (authenticationHeader.Scheme != HeaderConfiguration.AuthenticationScheme)
            {
                return null;
            }
            if (!requestHeaders.Contains(HeaderConfiguration.UsernameHeader))
            {
                return null;
            }

            var username = requestHeaders.GetValues(HeaderConfiguration.UsernameHeader).FirstOrDefault();
            if (username == null)
            {
                return null;
            }

            var decodedBytes = Convert.FromBase64String(authenticationHeader.Parameter);
            var signature = Encoding.UTF8.GetString(decodedBytes);
            return new ApiCredentials()
            {
                Signature = signature,
                Username = username
            };
        }

        public AuthenticationHeaderValue ToAuthenticationHeader()
        {
            var encoded = Convert.ToBase64String(Encoding.UTF8.GetBytes(this.Signature));
            return new AuthenticationHeaderValue(HeaderConfiguration.AuthenticationScheme, encoded);
        }
    }
}