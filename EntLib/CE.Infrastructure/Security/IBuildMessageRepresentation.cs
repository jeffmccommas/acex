﻿using System.Net.Http;

namespace CE.Infrastructure
{
    public interface IBuildMessageRepresentation
    {
        string BuildRequestRepresentation(HttpRequestMessage requestMessage);
    }
}