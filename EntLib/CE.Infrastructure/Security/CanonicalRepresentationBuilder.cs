﻿using System;
using System.Globalization;
using System.Linq;
using System.Net.Http;
using System.Web;

namespace CE.Infrastructure
{
    public class CanonicalRepresentationBuilder : IBuildMessageRepresentation
    {
        /// <summary>
        /// Builds message representation as follows:
        /// HTTP METHOD\n +
        /// Content-MD5\n +  
        /// Timestamp\n +
        /// Username\n +
        /// Request URI
        /// </summary>
        /// <returns></returns>
        public string BuildRequestRepresentation(HttpRequestMessage requestMessage)
        {
            bool valid = IsRequestValid(requestMessage);
            if (!valid)
            {
                return null;
            }
            DateTime date;
            if (!requestMessage.Headers.Contains(HeaderConfiguration.DateTime))
            {
                if (!requestMessage.Headers.Date.HasValue)
                {
                    return null;
                }
                date = requestMessage.Headers.Date.Value.UtcDateTime;
            }
            else
            {
                date = Convert.ToDateTime(requestMessage.Headers.GetValues(HeaderConfiguration.DateTime).First());
            }

            string md5 = requestMessage.Content == null ||
                requestMessage.Content.Headers.ContentMD5 == null ? ""
                : Convert.ToBase64String(requestMessage.Content.Headers.ContentMD5);

            string queryString = String.Empty;

            if (requestMessage.Method == HttpMethod.Get)
            {
                //Be sure to UrlDecode before UrlEncode here, since we must get back to original 
                //for representation to mimic the caller process used to create the signature  
                if (!string.IsNullOrEmpty(requestMessage.RequestUri.ParseQueryString().ToString()))
                {
                    if (requestMessage.RequestUri.ParseQueryString().ToString().Substring(0, 3) == "enc")
                    {
                        var regular = HttpUtility.UrlDecode(requestMessage.RequestUri.ParseQueryString().Get(0));
                        queryString = HttpUtility.UrlEncode(regular);
                    }
                    else
                    {
                        var regular = HttpUtility.UrlDecode(requestMessage.RequestUri.ParseQueryString().ToString());
                        queryString = HttpUtility.UrlEncode(regular);
                    }
                }

            }
            else if (requestMessage.Method == HttpMethod.Post)
            {
                if (!string.IsNullOrEmpty(requestMessage.Content.ReadAsStringAsync().Result.ToString()))
                {
                    if (requestMessage.Content.ReadAsStringAsync().Result.ToString().Substring(0, 3) == "enc")
                    {                       
                        queryString = HttpUtility.UrlDecode(requestMessage.Content.ReadAsStringAsync().Result.ToString().Substring(4, (requestMessage.Content.ReadAsStringAsync().Result.ToString().Length - 4)));
                    }
                    else
                    {                   
                        queryString = HttpUtility.UrlDecode(requestMessage.Content.ReadAsStringAsync().Result.ToString());         
                    }
                }
            }

            //md5 = requestMessage.Headers.GetValues(Configuration.MD5).First();

            string httpMethod = requestMessage.Method.Method;
            //string contentType = requestMessage.Content.Headers.ContentType.MediaType;
            if (!requestMessage.Headers.Contains(HeaderConfiguration.UsernameHeader))
            {
                return null;
            }

            string username = requestMessage.Headers.GetValues(HeaderConfiguration.UsernameHeader).First();
            //string uri = requestMessage.RequestUri.AbsolutePath.ToLower();

            var appUrl = VirtualPathUtility.ToAbsolute("~/");
            var uri = HttpContext.Current.Request.Url.AbsolutePath.Remove(0, appUrl.Length - 1);


            // you may need to add more headers if thats required for security reasons
            string representation = String.Join("\n", httpMethod,
                queryString, date.ToString(CultureInfo.InvariantCulture),
                username, uri);

            return representation;
        }

        private bool IsRequestValid(HttpRequestMessage requestMessage)
        {
            //for simplicity I am omitting headers check (all required headers should be present)

            return true;
        }
    }
}