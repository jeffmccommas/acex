﻿//using System;
//using System.Collections.Generic;
//using System.Security.Cryptography;
//using System.Text;

//namespace CE.Infrastructure
//{
//    public class DummySecretRepository : ISecretRepository
//    {
//        private readonly IDictionary<string, string> _userPasswords
//            = new Dictionary<string, string>()
//                  {
//                      {"KEYID23456789EXAMPLE","wJalrXUtnFEMI/K7MDENG/bPxRfiCYESECRETKEY"}
//                  };

//        public string GetPasswordForUser(string username)
//        {
//            if (!_userPasswords.ContainsKey(username))
//            {
//                return null;
//            }

//            var userPassword = _userPasswords[username];
//            return userPassword;
//        }

//        public string GetSecretForUser(string username)
//        {
//            if (!_userPasswords.ContainsKey(username))
//            {
//                return null;
//            }

//            var userPassword = _userPasswords[username];
//            var hashed = ComputeHash(userPassword, new SHA1CryptoServiceProvider());
//            return hashed;
//        }

//        private string ComputeHash(string inputData, HashAlgorithm algorithm)
//        {
//            byte[] inputBytes = Encoding.UTF8.GetBytes(inputData);
//            byte[] hashed = algorithm.ComputeHash(inputBytes);
//            return Convert.ToBase64String(hashed);
//        }
//    }
//}