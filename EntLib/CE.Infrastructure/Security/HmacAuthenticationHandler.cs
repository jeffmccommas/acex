﻿using System;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Security.Cryptography;
using System.Threading.Tasks;
using CE.Models;

namespace CE.Infrastructure
{
    public class HmacAuthenticationHandler : DelegatingHandler
    {
        private const string UnauthorizedMessage = "Unauthorized request";
        private readonly IBuildMessageRepresentation _representationBuilder;
        private readonly ICalculteSignature _signatureCalculator;

        public HmacAuthenticationHandler(
            IBuildMessageRepresentation representationBuilder,
            ICalculteSignature signatureCalculator)
        {
            _representationBuilder = representationBuilder;
            _signatureCalculator = signatureCalculator;
        }

#pragma warning disable 1998
        protected async Task<bool> IsAuthenticated(HttpRequestMessage requestMessage)
#pragma warning restore 1998
        {
            var acceptHeader = requestMessage.Headers.Accept;
            //foreach (var v in acceptHeader)
            //{
            //    if (v.ToString() == "text/html")
            //    {
            //        return true;
            //    }
            //}

            if (requestMessage.RequestUri.AbsolutePath.IndexOf("oauth", 0, StringComparison.Ordinal) > 0 )
            {
                return true;
            }
            if(requestMessage.RequestUri.AbsolutePath.IndexOf("resource", 0, StringComparison.Ordinal) > 0)
            {
                if (requestMessage.Headers.Authorization == null)
                {
                    return false;
                }
                return requestMessage.Headers.Authorization.Scheme == AuthType.Bearer.ToString();
            }
            // get secret key, get valid username
            var secret = string.Empty;
            var username = string.Empty;
            ClientUser clientUser = null;
            var authType = AuthType.CE;
            var basicKey = String.Empty;

            if (requestMessage.Properties.ContainsKey(CEConfiguration.CEClientUser))
            {
                clientUser = ((ClientUser)requestMessage.Properties[CEConfiguration.CEClientUser]);

                if (clientUser != null)
                {
                    secret = clientUser.CESecretAccessKey;
                    username = clientUser.CEAccessKeyID;
                    authType = clientUser.ClientAuthType;
                    basicKey = clientUser.BasicKey;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }

            if (authType == AuthType.Basic)
            {
                if (requestMessage.Headers.Authorization == null
                   || requestMessage.Headers.Authorization.Scheme != HeaderConfiguration.BasicAuthenticationScheme)
                {
                    return false;
                }
                //first check length of auth parameter to see if it can be decoded
                if (requestMessage.Headers.Authorization.Parameter.Length % 4 > 0)
                {
                    return false;
                }

                string[] basic = CE.Infrastructure.Encrypter.EncryptedActionParameterAttribute.Base64Decode(requestMessage.Headers.Authorization.Parameter).Split(new char[] { ':' });

                //check that username and password could be parsed
                if (basic.Length != 2)
                {
                    return false;
                }

                // check the validity of parsed username and password
                if (username != basic[0] || basicKey != basic[1])
                {
                    return false;
                }

                // endpoint authorization check
                var ind = requestMessage.RequestUri.AbsolutePath.IndexOf("api", 0) - 1;
                var len = requestMessage.RequestUri.AbsolutePath.Length - ind;
                var resource = requestMessage.RequestUri.AbsolutePath.Substring(ind, len);

                //if (!clientUser.Endpoints.Exists(p => p.Name.ToLower() == resource.ToLower()))
                //{
                //    return false;
                //}
                return true;
            }
            if (!requestMessage.Headers.Contains(HeaderConfiguration.UsernameHeader))
            {
                return false;
            }

            var isDateValid = IsDateValid(requestMessage);
            if (!isDateValid)
            {
                return false;
            }

            if (requestMessage.Headers.Authorization == null
                || requestMessage.Headers.Authorization.Scheme != HeaderConfiguration.AuthenticationScheme)
            {
                return false;
            }

            var representation = _representationBuilder.BuildRequestRepresentation(requestMessage);
            if (representation == null)
            {
                return false;
            }

            //if (requestMessage.Content.Headers.ContentMD5 != null
            //    && !await IsMd5Valid(requestMessage))
            //{
            //    return false;
            //}

            var signature = _signatureCalculator.Signature(secret, representation);
            signature = username + ":" + signature;

            var result = requestMessage.Headers.Authorization.Parameter == signature;

            // endpoint authorization check
            if (result)
            {
                var ind = requestMessage.RequestUri.AbsolutePath.IndexOf("api", 0) - 1;
                var len = requestMessage.RequestUri.AbsolutePath.Length - ind;
                var resource = requestMessage.RequestUri.AbsolutePath.Substring(ind, len);

                if (!clientUser.Endpoints.Exists(p => p.Name == resource))
                {
                    result = false;
                }
            }

            return result;
        }

        private async Task<bool> IsMd5Valid(HttpRequestMessage requestMessage)
        {
            var hashHeader = requestMessage.Content.Headers.ContentMD5;
            if (requestMessage.Content == null)
            {
                return hashHeader == null || hashHeader.Length == 0;
            }
            var hash = await MD5Helper.ComputeHash(requestMessage.Content);

            var s = Convert.ToBase64String(hash);
            var s1 = Convert.ToBase64String(hashHeader);
            return hash.SequenceEqual(hashHeader);
        }

        public static string GetMD5HashFromStream(Stream stream)
        {
            using (MD5 md5 = MD5.Create())
            {
                byte[] hash = md5.ComputeHash(stream);
                return Convert.ToBase64String(hash);
            }
        }

        private bool IsDateValid(HttpRequestMessage requestMessage)
        {
            var utcNow = DateTime.UtcNow;
            DateTime date;

            if (requestMessage.Headers.Contains(HeaderConfiguration.DateTime))
            {
                date = Convert.ToDateTime(requestMessage.Headers.GetValues(HeaderConfiguration.DateTime).First());
            }
            else if (requestMessage.Headers.Date != null)
            {
                date = requestMessage.Headers.Date.Value.UtcDateTime;
            }
            else
            {
                return false;
            }

            if (date >= utcNow.AddMinutes(HeaderConfiguration.DateTime_ValidityWindowInMinutes)
                || date <= utcNow.AddMinutes(-HeaderConfiguration.DateTime_ValidityWindowInMinutes))
            {
                return false;
            }

            return true;
        }

        protected async override Task<HttpResponseMessage> SendAsync(HttpRequestMessage request, System.Threading.CancellationToken cancellationToken)
        {
            var isAuthenticated = await IsAuthenticated(request);

            if (!isAuthenticated)
            {
                var response = request.CreateErrorResponse(HttpStatusCode.Unauthorized, UnauthorizedMessage);

                //default value for WWW-Authenticate is Basic.  When authentication scheme is actually known, this should return either Basic or CE (TODO in future)
                response.Headers.WwwAuthenticate.Add(new AuthenticationHeaderValue(HeaderConfiguration.BasicAuthenticationScheme, HeaderConfiguration.AuthenticationRealm));

                return (response);
            }

            return await base.SendAsync(request, cancellationToken);
        }


    }
}