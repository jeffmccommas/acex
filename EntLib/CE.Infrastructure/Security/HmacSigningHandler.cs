﻿using System;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using CE.Models;

namespace CE.Infrastructure
{
    public class HmacSigningHandler : HttpClientHandler
    {
        private readonly IBuildMessageRepresentation _representationBuilder;
        private readonly ICalculteSignature _signatureCalculator;

        public string Username { get; set; }

        public HmacSigningHandler(
                              IBuildMessageRepresentation representationBuilder,
                              ICalculteSignature signatureCalculator)
        {
            _representationBuilder = representationBuilder;
            _signatureCalculator = signatureCalculator;
        }

        protected override Task<HttpResponseMessage> SendAsync(HttpRequestMessage request,
                                                                System.Threading.CancellationToken cancellationToken)
        {
            if (!request.Headers.Contains(HeaderConfiguration.UsernameHeader))
            {
                request.Headers.Add(HeaderConfiguration.UsernameHeader, Username);
            }
            request.Headers.Date = new DateTimeOffset(DateTime.Now, DateTime.Now - DateTime.UtcNow);
            var representation = _representationBuilder.BuildRequestRepresentation(request);
            ClientUser clientUser = null;
            var secret = clientUser.CESecretAccessKey;

            if (request.Properties.ContainsKey(CEConfiguration.CEClientUser))
            {
                clientUser = ((ClientUser)request.Properties[CEConfiguration.CEClientUser]);

                if (clientUser != null)
                {
                    secret = clientUser.CESecretAccessKey;
                }
            }

            string signature = _signatureCalculator.Signature(secret,
                representation);

            var header = new AuthenticationHeaderValue(HeaderConfiguration.AuthenticationScheme, signature);

            request.Headers.Authorization = header;
            return base.SendAsync(request, cancellationToken);
        }
    }
}