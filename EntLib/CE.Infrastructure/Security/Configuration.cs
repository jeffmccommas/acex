﻿namespace CE.Infrastructure
{
    public class HeaderConfiguration
    {
        public const string AuthenticationScheme = "CE";
        public const string BasicAuthenticationScheme = "Basic";
        public const string AuthenticationRealm = "realm=\"CEInsights\"";
        public const string UsernameHeader = "X-CE-AccessKeyId";
        public const string MessageIdHeader = "X-CE-MessageId";
        public const string ChannelHeader = "X-CE-Channel";
        public const string LocaleHeader = "X-CE-Locale";
        public const string MetaHeader = "X-CE-Meta";
        public const string DateTime = "X-DateTime";
        public const int DateTime_ValidityWindowInMinutes = 5;
    }
}