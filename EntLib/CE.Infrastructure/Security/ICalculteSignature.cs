﻿namespace CE.Infrastructure
{
    public interface ICalculteSignature
    {
        string Signature(string secret, string value);
    }
}