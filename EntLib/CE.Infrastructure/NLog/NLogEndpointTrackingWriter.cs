﻿using System;
using CE.Models;
using NLog;

namespace CE.Infrastructure
{
    public class NLogEndpointTrackingWriter
    {
        public const string ET_ClientID_Key = "et_clientid";
        public const string ET_UserID_Key = "et_userid";
        public const string ET_Verb_Key = "et_verb";
        public const string ET_ResourceName_Key = "et_resourcename";
        public const string ET_EndpointID_Key = "et_endpointid";
        public const string ET_UserAgent_Key = "et_useragent";
        public const string ET_SourceIP_Key = "et_sourceip";
        public const string ET_XCEMessageId_Key = "et_xcemessageid";
        public const string ET_XCEChannel_Key = "et_xcechannel";
        public const string ET_XCELocale_Key = "et_xcelocale";
        public const string ET_XCEMeta_Key = "et_xcemeta";
        public const string ET_Query_Key = "et_query";
        public const string ET_Include_Key = "et_include";
        public const string ET_ThirdPartyClientId_Key = "et_thirdpartyclientid";

        public NLogEndpointTrackingWriter() { }

        public void Track(EndpointTrack record)
        {
            var logger = LogManager.GetLogger("EndpointTracker");

            try
            {
                // only log and trace if enabled
                var logLevel = LogLevel.Info;

                if (logger.IsEnabled(LogLevel.Info))
                {
                    // create the log event and log  - this will log to the configured nlog target, rules, etc. specified by the app
                    logger.Log(CreateEvent(logLevel, record));
                }
            }
            catch (Exception e)
            {
                // log exception - this exception usually occurs within the user's trace action code                
                logger.LogException(LogLevel.Error, "Failed to trace message due to an error.", e);
                throw;
            }
        }

        private LogEventInfo CreateEvent(LogLevel logLevel, EndpointTrack record)
        {
            // create new log event with custom context information
            var logEvent = new LogEventInfo
                           {
                               Level = logLevel,
                               Message = "EndpointTracker-Message",
                               LoggerName = "EndpointTracker",
                               TimeStamp = DateTime.UtcNow
                           };

            // load properties on the log event, this will be used downstream by the layout renderers
            PopulateProperties(logEvent, record);

            return logEvent;
        }

        private void PopulateProperties(LogEventInfo logEvent, EndpointTrack record)
        {
            logEvent.Properties[ET_ClientID_Key] = record.ClientId;
            logEvent.Properties[ET_UserID_Key] = record.UserId;
            logEvent.Properties[ET_Verb_Key] = record.Verb;
            logEvent.Properties[ET_ResourceName_Key] = record.ResourceName;
            logEvent.Properties[ET_EndpointID_Key] = record.EndpointId;
            logEvent.Properties[ET_UserAgent_Key] = record.UserAgent;
            logEvent.Properties[ET_SourceIP_Key] = record.SourceIP;
            logEvent.Properties[ET_XCEMessageId_Key] = record.XCEMessageId;
            logEvent.Properties[ET_XCEChannel_Key] = record.XCEChannel;
            logEvent.Properties[ET_XCELocale_Key] = record.XCELocale;
            logEvent.Properties[ET_XCEMeta_Key] = record.XCEMeta;
            logEvent.Properties[ET_Query_Key] = record.QueryString;
            logEvent.Properties[ET_Include_Key] = record.Include;
            logEvent.Properties[ET_ThirdPartyClientId_Key] = record.ThirdPartyClientId;
        }

    }

}
