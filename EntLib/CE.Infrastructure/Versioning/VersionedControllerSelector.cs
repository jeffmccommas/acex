﻿ // ReSharper disable once CheckNamespace
namespace CE.Infrastructure
{
    using System;
    using System.Collections.Concurrent;
    using System.Collections.Generic;
    using System.Diagnostics.CodeAnalysis;
    using System.Diagnostics.Contracts;
    using System.Linq;
    using System.Net;
    using System.Net.Http;
    using System.Text;
    using System.Text.RegularExpressions;
    using System.Web.Http;
    using System.Web.Http.Controllers;
    using System.Web.Http.Dispatcher;
    using System.Web.Http.Routing;
    using Util;
    using Versioning.Resources;

    /// <summary>
    ///   Represents an <see cref="IHttpControllerSelector" /> implementation that supports versioning and selects an controller based on versioning by convention (namespace.Api.Version1.xxxController).
    ///   How the actual controller to be invoked is determined, is up to the derived class to implement.
    /// </summary>
    public abstract class VersionedControllerSelector : IHttpControllerSelector
    {
        private static LockValue<string> _versionPrefix = "Version";
        private static LockValue<string> _controllerSuffix = DefaultHttpControllerSelector.ControllerSuffix;

        protected const string ControllerKey = "controller";

        /// <summary>
        /// Gets the suffix in the Controller <see cref="Type"/>s <see cref="Type.Name"/>
        /// </summary>
        public static string ControllerSuffix
        {
            get { return _controllerSuffix; }
            set
            {
                if (value == null)
                {
                    throw new ArgumentNullException("value");
                }

                if (String.IsNullOrWhiteSpace(value))
                {
                    throw new ArgumentException(String.Format(ExceptionStrings.CannotSetEmptyValue, "ControllerSuffix"), "value");
                }

                if (_controllerSuffix.IsLocked)
                {
                    throw new InvalidOperationException(String.Format(ExceptionStrings.ControllerDiscoveryProcessAlreadyRun, "ControllerSuffix"));
                }

                _controllerSuffix = value;
            }
        }

        /// <summary>
        /// Gets the prefix used for identifying a controller version in a <see cref="Type"/>.<see cref="Type.FullName"/>. Examples and usage in remarks.
        /// </summary>
        /// <remarks>
        ///  <para>
        ///     Make sure to set this property in the Application_Start method.
        /// </para>
        /// 
        ///  <para>
        ///     For example, when this is set to "V", a controller in the namespace of Company.V1.ProductController will identify the ProductController as being version 1, but will not identify 
        ///     Company.Version1.ProductController as being a version 1 controller.
        /// </para>
        /// </remarks>
        public static string VersionPrefix
        {
            get { return _versionPrefix; }
            set
            {
                if (value == null)
                {
                    throw new ArgumentNullException("value");
                }

                if (String.IsNullOrWhiteSpace(value))
                {
                    throw new ArgumentException(String.Format(ExceptionStrings.CannotSetEmptyValue, "VersionPrefix"), "value");
                }

                if (_versionPrefix.IsLocked)
                {
                    throw new InvalidOperationException(String.Format(ExceptionStrings.ControllerDiscoveryProcessAlreadyRun, "VersionPrefix"));
                }

                _versionPrefix = value;
            }
        }

        private readonly HttpConfiguration _configuration;
        private readonly Lazy<ConcurrentDictionary<ControllerIdentification, HttpControllerDescriptor>> _controllerInfoCache;
        private readonly HttpControllerTypeCache _controllerTypeCache;

        /// <summary>
        ///   Initializes a new instance of the <see cref="System.Web.Http.Dispatcher.DefaultHttpControllerSelector" /> class.
        /// </summary>
        /// <param name="configuration"> The configuration. </param>
        protected VersionedControllerSelector(HttpConfiguration configuration)
        {
            if (configuration == null)
            {
                throw new ArgumentNullException("configuration");
            }

            _controllerInfoCache =
                    new Lazy<ConcurrentDictionary<ControllerIdentification, HttpControllerDescriptor>>(InitializeControllerInfoCache);
            _configuration = configuration;
            _controllerTypeCache = new HttpControllerTypeCache(_configuration);
        }


        #region IHttpControllerSelector Members

        [SuppressMessage("Microsoft.Reliability", "CA2000:Dispose objects before losing scope",
                Justification = "Caller is responsible for disposing of response instance.")]
        public HttpControllerDescriptor SelectController(HttpRequestMessage request)
        {
            if (request == null)
            {
                throw new ArgumentNullException("request");
            }

            ControllerIdentification controllerName = GetControllerIdentificationFromRequest(request);

            if (String.IsNullOrEmpty(controllerName.Name))
            {
                throw new HttpResponseException(request.CreateResponse(HttpStatusCode.NotFound));
            }

            if (controllerName.Version == null) controllerName.Version = GetVersionRouteDefaults(request);

            HttpControllerDescriptor controllerDescriptor;
            if (_controllerInfoCache.Value.TryGetValue(controllerName, out controllerDescriptor))
            {
                return controllerDescriptor;
            }

            ICollection<Type> matchingTypes = _controllerTypeCache.GetControllerTypes(controllerName);

            // ControllerInfoCache is already initialized.
            Contract.Assert(matchingTypes.Count != 1);

            if (matchingTypes.Count == 0)
            {
                // no matching types
                throw new HttpResponseException(request.CreateResponse(
                                                                       HttpStatusCode.NotFound,
                                                                       "The API '" + controllerName + "' doesn't exist"));
            }

            // multiple matching types
            throw new HttpResponseException(request.CreateResponse(
                                                                   HttpStatusCode.InternalServerError,
                                                                   CreateAmbiguousControllerExceptionMessage(request.GetRouteData().Route,
                                                                                                             controllerName.Name,
                                                                                                             matchingTypes)));
        }

        private string GetVersionRouteDefaults(HttpRequestMessage request)
        {
            if (request == null)
            {
                throw new ArgumentNullException("request");
            }

            object data;

            if (request.GetRouteData().Values.TryGetValue("version", out data))
            {
                return data as string;
            }
            return null;
        }

        public IDictionary<string, HttpControllerDescriptor> GetControllerMapping()
        {
            return _controllerInfoCache.Value.ToDictionary(c => VersionPrefix + c.Key.Version + "." + c.Key.Name, c => c.Value, StringComparer.OrdinalIgnoreCase);
        }

        #endregion


        /// <summary>
        /// Gets the name of the controller from the request route date
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        protected string GetControllerNameFromRequest(HttpRequestMessage request)
        {
            if (request == null)
            {
                throw new ArgumentNullException("request");
            }

            IHttpRouteData routeData = request.GetRouteData();
            if (routeData == null)
            {
                return default(String);
            }

            // Look up controller in route data
            object controllerName;
            routeData.Values.TryGetValue(ControllerKey, out controllerName);

            if (controllerName != null) return controllerName.ToString();
            var req = request.RequestUri.AbsolutePath.IndexOf("resource", StringComparison.Ordinal);
            if (req > -1)
            {
                var batchRequest = request.RequestUri.AbsolutePath.IndexOf("batch", StringComparison.InvariantCultureIgnoreCase);
                var requestSubString = batchRequest > -1
                    ? request.RequestUri.AbsolutePath.Substring(batchRequest + 5,
                        request.RequestUri.AbsolutePath.Length - batchRequest - 5)
                    : request.RequestUri.AbsolutePath.Substring(req + 8,
                        request.RequestUri.AbsolutePath.Length - req - 8);

                var requestArray = requestSubString.Split('/');
                var arrayUpperBound = requestArray.GetUpperBound(0);
                if (arrayUpperBound < 2 || (arrayUpperBound == 2 && string.IsNullOrEmpty(requestArray[2])))
                {
                    controllerName = requestArray[1];
                }
                else if (arrayUpperBound < 3 || (arrayUpperBound == 3 && string.IsNullOrEmpty(requestArray[3])))
                {
                    if (requestArray[1].ToLower() == "subscription")
                    {
                        // hardcode to usagepoint controller when request for subscription/{subscriptionId}
                        controllerName = "usagepoint";
                    }
                    else
                    {
                        controllerName = requestArray[1];
                    }
                }
                else if(arrayUpperBound < 5 || (arrayUpperBound == 5 && string.IsNullOrEmpty(requestArray[5])))
                    {
                    controllerName = requestArray[3];
                }
                else if (arrayUpperBound < 7 || (arrayUpperBound == 7 && string.IsNullOrEmpty(requestArray[7])))
                {
                    controllerName = requestArray[5];
                }
                else
                {
                    controllerName = requestArray[7];
                }
            }

            if (controllerName != null) return controllerName.ToString();

            return string.Empty;
        }

        protected abstract ControllerIdentification GetControllerIdentificationFromRequest(HttpRequestMessage request);

        private static string CreateAmbiguousControllerExceptionMessage(IHttpRoute route, string controllerName,
                                                                         IEnumerable<Type> matchingTypes)
        {
            Contract.Assert(route != null);
            Contract.Assert(controllerName != null);
            Contract.Assert(matchingTypes != null);

            // Generate an exception containing all the controller types
            StringBuilder typeList = new StringBuilder();
            foreach (Type matchedType in matchingTypes)
            {
                typeList.AppendLine();
                typeList.Append(matchedType.FullName);
            }

            return String.Format("Multiple possibilities for {0}, using route template {1}. The following types were selected: {2}.",
                                 controllerName,
                                 route.RouteTemplate,
                                 typeList);
        }

        private ConcurrentDictionary<ControllerIdentification, HttpControllerDescriptor> InitializeControllerInfoCache()
        {
            // lock dependend properties
            _versionPrefix.Lock();


            // let's find and cache the found controllers
            var result = new ConcurrentDictionary<ControllerIdentification, HttpControllerDescriptor>(ControllerIdentification.Comparer);
            var duplicateControllers = new HashSet<ControllerIdentification>();
            Dictionary<ControllerIdentification, ILookup<string, Type>> controllerTypeGroups = _controllerTypeCache.Cache;

            foreach (KeyValuePair<ControllerIdentification, ILookup<string, Type>> controllerTypeGroup in controllerTypeGroups)
            {
                ControllerIdentification controllerName = controllerTypeGroup.Key;

                foreach (IGrouping<string, Type> controllerTypesGroupedByNs in controllerTypeGroup.Value)
                {
                    foreach (Type controllerType in controllerTypesGroupedByNs)
                    {
                        if (result.Keys.Contains(controllerName))
                        {
                            duplicateControllers.Add(controllerName);
                            break;
                        }
                        else
                        {
                            result.TryAdd(controllerName,
                                          new HttpControllerDescriptor(_configuration, controllerName.Name, controllerType));
                        }
                    }
                }
            }

            foreach (ControllerIdentification duplicateController in duplicateControllers)
            {
                HttpControllerDescriptor descriptor;
                result.TryRemove(duplicateController, out descriptor);
            }

            return result;
        }

        /// <summary>
        /// Determines whether part of a namespace string is a valid version number
        /// </summary>
        /// <param name="namespacePart">Part of a namespace string to check</param>
        /// <returns>True if the namespace part is a valid version number, false otherwise</returns>
        internal static bool IsVersionNumber(string namespacePart)
        {
            return Regex.IsMatch(namespacePart, String.Format(@"{0}[0-9]+(_[0-9]+)*", VersionPrefix));
        }

        /// <summary>
        /// Converts part of a namespace string to a valid version number
        /// </summary>
        /// <param name="namespacePart">Part of a namespace string to convert</param>
        /// <returns>A string containing a valid version number. e.g. for the namespace part Version2_1 the string 2.1 would be returned</returns>
        internal static string ToVersionNumber(string namespacePart)
        {
            // we have a version, strip the prefix and convert version separators to ensure a valid namespace
            return namespacePart.Substring(VersionPrefix.Length).Replace("_", ".");
        }
    }
}
