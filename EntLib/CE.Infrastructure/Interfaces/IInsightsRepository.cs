﻿using System;
using CE.Models.Insights;

namespace CE.Infrastructure
{
    public interface IInsightsRepository : IDisposable
    {
        BillDisaggResponse GetBillDisagg(int clientId, BillDisaggRequest request);
    }
}
