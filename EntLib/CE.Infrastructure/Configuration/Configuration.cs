﻿namespace CE.Infrastructure
{
    public class CEConfiguration
    {
        /// <summary>
        /// ClientUser key name for request message properties dictionary
        /// </summary>
        public const string CEClientUser = "CE_ClientUser";

        /// <summary>
        /// Locale key name for request message properties dictionary
        /// </summary>
        public const string CELocale = "CE_Locale";

        /// <summary>
        /// Delimiter for use by list parameters in requests
        /// </summary>
        public const char ParameterListDelimiter = ',';


        /// <summary>
        /// Controllers common messages for HTTP responses.  EndUses.
        /// </summary>
        public const string IllegalParameter_EndUse = "Unrecognized EndUse in EndUses";

        /// <summary>
        /// Controllers common messages for HTTP responses.  Appliances.
        /// </summary>
        public const string IllegalParameter_Appliance = "Unrecognized Appliance in Appliances";

        /// <summary>
        /// Controllers common messages for HTTP responses.  Commodities.
        /// </summary>
        public const string IllegalParameter_Commodity = "Unrecognized Commodity in Commodities";

        /// <summary>
        /// Controllers common messages for HTTP responses.  Measurements.
        /// </summary>
        public const string IllegalParameter_Measurement = "Unrecognized Measurement in Measurements";

        /// <summary>
        /// Controllers common messages for HTTP responses.  Benchmark groups.
        /// </summary>
        public const string IllegalParameter_Groups = "Unrecognized Group in Benchmark Groups";

        /// <summary>
        /// Controllers common messages for HTTP responses.  No results.
        /// </summary>
        public const string Response_NoData = "Your request did not generate any results";

        /// <summary>
        /// Invalid data in a request.  Profile.
        /// </summary>
        public const string Invalid_ProfileAttribute = "Your request contained one or more invalid profile attributes";

        /// <summary>
        /// Duplicate attribute keys in a request.  Profile.
        /// </summary>
        public const string Invalid_DuplicateProfileAttributeKey = "Your request contained one or more duplicate profile attribute keys";

        // <summary>
        /// Invalid attribute value in a request.  Profile.
        /// </summary>
        public const string Invalid_ProfileAttributeValue = "Your request contained one or more invalid profile attribute values";

        // <summary>
        /// Invalid attribute source in a request.  Profile.
        /// </summary>
        public const string Invalid_ProfileAttributeSource = "Your request contained one or more invalid profile attribute sources";

        /// <summary>
        /// Data was expected in post.
        /// </summary>
        public const string Error_PostEmpty = "No data was posted";

        /// <summary>
        /// Error attempting to insert or update data. 
        /// </summary>
        public const string Error_PostFailed = "Internal error posting data";

        /// <summary>
        /// Error attempting to execute energy model. 
        /// </summary>
        public const string Error_EnergyModelFailed = "Internal error executing energy model";

        /// <summary>
        /// Error attempting to execute energy model. 
        /// </summary>
        public const string Error_BillDisaggFailed = "Internal error executing bill disagg";

        /// <summary>
        /// Error attempting to get data. 
        /// </summary>
        public const string Error_GetFailed = "Internal error getting data";

        /// <summary>
        /// Content Key for retrieving specific blob of content.  ApplianceList.
        /// </summary>
        public const string ContentKey_CEApplianceList1 = "CEApplianceList1";

        /// <summary>
        /// Content Key for retrieving specific blob of content.  EnduseList.
        /// </summary>
        public const string ContentKey_CEEnduseList1 = "CEEnduseList1";

        /// <summary>
        /// Invalid data in a request.  Action.
        /// </summary>
        public const string Invalid_Action = "Your request contained one or more invalid actions";

        /// <summary>
        /// Duplicate attribute keys in a request.  Action.
        /// </summary>
        public const string Invalid_DuplicateAction = "Your request contained one or more duplicate actions";

        /// <summary>
        /// Invalid data in a request.  Action.
        /// </summary>
        public const string Invalid_ActionStatus = "Your request contained one or more invalid action statuses";

        /// <summary>
        /// Invalid data in a request.  Action.
        /// </summary>
        public const string Invalid_ActionSource = "Your request contained one or more invalid action sources";

        /// <summary>
        /// No data available to delete for a given CustomerId and WebToken.
        /// </summary>
        public const string ErrorNoDataAvailableToDelete = "No data available for CustomerId:{0} and WebToken:{1}";

        /// <summary>
        /// Access denied for a given CustomerId and WebToken due to authorization failure.
        /// </summary>
        public const string ErrorAccessDenied = "Access Denied for CustomerId:{0} and WebToken:{1}";

        /// <summary>
        /// No customer found for a given CustomerId.
        /// </summary>
        public const string ErrorNoCustomerFound = "No customer found for CustomerId:{0}";

        /// <summary>
        /// No customer found for a given CustomerId.
        /// </summary>
        public const string ErrorNoZipCodeFound = "No zipcode found for ZipCode:{0}";

        /// <summary>
        /// Invalid group name or role name.
        /// </summary>
        public const string ErrorInvalidGroupOrRole = "Invalid group name or role name.";

        /// <summary>
        /// Web token for a given CustomerId has expired.
        /// </summary>
        public const string ErrorTokenExpired = "WebToken:{0} for CustomerId:{1} has expired.";


        /// <summary>
        /// Energy Model no parameters error.
        /// </summary>
        public const string ErrorEMIllegalParameters = "No parameters available to excute energy model.";

        /// <summary>
        /// Energy Model failed execution.
        /// </summary>
        public const string ErrorEMFailedExecution = "Error executing energy model";

        /// <summary>
        /// Missing data in a request.  Meter Ids.
        /// </summary>
        public const string Required_MeterIds = "Meter Ids is missing for your request";

        /// <summary>
        /// Missing data in a request.  Meter Ids and ServicePoint Ids.
        /// </summary>
        public const string Required_MeterId_ServicePointIds = "MeterId and ServicePointId cannot both be empty for your request";

        /// <summary>
        /// Missing data in a request.  Start/End/Projected Date.
        /// </summary>
        public const string Required_Start_End_ProjectedDate = "Start Date/End Date/Projected Date is missing for your request";

        /// <summary>
        /// Error customer id or email required.
        /// </summary>
        public const string ErrorEitherEmailCustomerId = "EitherEmailCustomerId";

        /// <summary>
        /// Error both customer id and email passed.
        /// </summary>
        public const string ErrorBothEmailCustomerId = "BothEmailCustomerId";

        /// <summary>
        /// Zipcode is not valid.
        /// </summary>
        public const string ErrorZipCodeNotValid = "ZipCodeNotValid";

        /// <summary>
        /// Email is not valid.
        /// </summary>
        public const string ErrorEmailNotValid = "EmailNotValid";

    }

}