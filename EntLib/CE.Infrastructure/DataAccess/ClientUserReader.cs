﻿using System.Configuration;
using System.Text;
using CE.Infrastructure.Cache;
using CE.Models;

namespace CE.Infrastructure.DataAccess
{
    /// <summary>
    /// Get the ClientUser data from cache or database.
    /// </summary>
    public class ClientUserReader
    {
        private DataAccessLayer _dataAccess;
        private int _cacheTimeoutInMinutes = 10;
        private EnvironmentType _environment = EnvironmentType.prod;    // default = 0
        private const string AppSetting_CEEnvironment = "CEEnvironment";

        public ClientUserReader()
        {
            _dataAccess = new DataAccessLayer();

            string s = ConfigurationManager.AppSettings.Get(AppSetting_CEEnvironment);
            if (!string.IsNullOrEmpty(s))
            {
                _environment = (EnvironmentType)System.Enum.Parse(typeof(EnvironmentType), s, true);
            }

        }

        public ClientUserReader(EnvironmentType environment)
        {
            _dataAccess = new DataAccessLayer();
            _environment = environment;
        }

        public ClientUserReader(int cacheTimeoutInMinutes)
        {
            _cacheTimeoutInMinutes = cacheTimeoutInMinutes;
            _dataAccess = new DataAccessLayer();
        }

        public ClientUserReader(EnvironmentType environment, int cacheTimeoutInMinutes)
        {
            _environment = environment;
            _cacheTimeoutInMinutes = cacheTimeoutInMinutes;
            _dataAccess = new DataAccessLayer();
        }


        public ClientUser GetClientUser(string accessKeyID)
        {
            return (GetClientUser_Cache(accessKeyID, _environment));
            //return (_dataAccess.GetClientUser_Database(accessKeyID, _environment));  //use this when debugging the database retrieval
        }

        private ClientUser GetClientUser_Cache(string accessKeyID, EnvironmentType environmentID)
        {
            ClientUser x;
            const string cacheKeyTemplate = "GetClientUser-{0}-{1}";

            StringBuilder cacheKeySB = new StringBuilder();
            string cacheKey;

            cacheKey = cacheKeySB.AppendFormat(cacheKeyTemplate, accessKeyID, (int)environmentID).ToString();

            x = CacheLayer.Get<ClientUser>(cacheKey);

            if (x == null)
            {
                x = _dataAccess.GetClientUser_Database(accessKeyID, environmentID);

                if (x != null)
                {
                    CacheLayer.Add(x, cacheKey, _cacheTimeoutInMinutes);
                }
                else
                {
                    // no user was found anywhere
                    x = null;
                }
            }

            return (x);
        }


    }
}
