﻿using System.Configuration;

namespace CE.Infrastructure.DataAccess
{
    public partial class DataAccessLayer
    {
        private const string InsightsConnName = "InsightsConn";
        private const string InsightsDwConnName = "InsightsDWConn";
        private readonly string _dataSourceConnString = string.Empty;
        private readonly string _dataSourceConnStringDw = string.Empty;

        public DataAccessLayer()
        {
            _dataSourceConnString = InsightsConnName;
            _dataSourceConnStringDw = InsightsDwConnName;
        }

        private string GetConnection()
        {
            return (ConfigurationManager.ConnectionStrings[_dataSourceConnString].ConnectionString);
        }

        private string GetConnectionDw()
        {
            return (ConfigurationManager.ConnectionStrings[_dataSourceConnStringDw].ConnectionString);
        }
    }
}
