﻿using CE.Models.Insights;
using CE.Models.Insights.Types;
using System.Collections.Generic;
using System.Linq;

namespace CE.Infrastructure.DataAccess
{
    /// <summary>
    /// Get and then process billdisagg data.
    /// </summary>
    public class BillDisaggReader
    {
        private DataAccessLayer _dataAccess;
        public IInsightsRepository billdisaggRepo;

        public BillDisaggReader()
        {
            _dataAccess = new DataAccessLayer();
        }

        /// <summary>
        /// For Testability
        /// </summary>
        /// <param name="insightrepo"></param>
        public BillDisaggReader(IInsightsRepository insightsrepo)
        {
            billdisaggRepo = insightsrepo;
        }

        public BillDisaggResponse GetBillDisagg(int clientId, BillDisaggRequest request)
        {
            BillDisaggResponse res = null;

            if (!string.IsNullOrEmpty(request.CustomerId))
            {
                IEnumerable<int> enduses = null;
                IEnumerable<int> appliances = null;
                IEnumerable<byte> commodities = null;

                if (!string.IsNullOrEmpty(request.EndUseKeys))
                {
                    var arr = request.EndUseKeys.Split(CEConfiguration.ParameterListDelimiter);
                    List<EndUseType> items = arr.Select(a => System.Enum.Parse(typeof(EndUseType), a)).OfType<EndUseType>().ToList();
                    enduses = items.Select(i => (int)i);
                }

                if (!string.IsNullOrEmpty(request.ApplianceKeys))
                {
                    var arr = request.ApplianceKeys.Split(CEConfiguration.ParameterListDelimiter);
                    List<ApplianceType> items = arr.Select(a => System.Enum.Parse(typeof(ApplianceType), a)).OfType<ApplianceType>().ToList();
                    appliances = items.Select(i => (int)i);
                }

                if (!string.IsNullOrEmpty(request.CommodityKeys))
                {
                    var arr = request.CommodityKeys.Split(CEConfiguration.ParameterListDelimiter);
                    List<CommodityType> items = arr.Select(a => System.Enum.Parse(typeof(CommodityType), a)).OfType<CommodityType>().ToList();
                    commodities = items.Select(i => (byte)i);
                }

                res = _dataAccess.GetBillDisagg_Database(clientId, request.StartDate, request.EndDate, request.CustomerId, request.AccountId, request.PremiseId, enduses, appliances, commodities);

            }

            if (res != null)
            {
                //Add DissaggDetails at EndUse level, from sums of Appliance DisaggDetails
                SumApplianceCommoditiesUpToEndUse(res.Customer);

                // Execute Measurements for each DissAgg detail at Appliance & EndUse level
                if (!string.IsNullOrEmpty(request.MeasurementKeys))
                {
                    var arr = request.MeasurementKeys.Split(CEConfiguration.ParameterListDelimiter);
                    List<MeasurementType> items = arr.Select(a => System.Enum.Parse(typeof(MeasurementType), a)).OfType<MeasurementType>().ToList();

                    CreateMeasurementsWithinBillDisagg(res.Customer, items);
                }

                // Assign content to names within Appliances and Enduses.  They are already filled with the enum name string value.  Now replace with name from content

            }

            return (res);
        }


        private void SumApplianceCommoditiesUpToEndUse(Customer customer)
        {
            var commodityElectric = CommodityType.Electric.ToString().ToLower();
            var commodityGas = CommodityType.Gas.ToString().ToLower();
            var commodityWater = CommodityType.Water.ToString().ToLower();

            foreach (var a in customer.Accounts)
            {
                foreach (var p in a.Premises)
                {
                    foreach (var e in p.EndUses)
                    {
                        // electric DisaggDetail total at endUse level if any exists at appliance level
                        var electricCount = e.Appliances.Sum(ap => ap.DisaggDetails.FindAll(f => f.CommodityKey == commodityElectric).Count());
                        if (electricCount > 0)
                        {
                            var cost = e.Appliances.Sum(ap => ap.DisaggDetails.FindAll(f => f.CommodityKey == commodityElectric).Sum(dd => dd.CostAmount));
                            var usage = e.Appliances.Sum(ap => ap.DisaggDetails.FindAll(f => f.CommodityKey == commodityElectric).Sum(dd => dd.UsageQuantity));

                            var dissagDetail = new DisaggDetail()
                            {
                                CommodityKey = commodityElectric,
                                CostAmount = cost,
                                UsageQuantity = usage,
                                CostCurrencyKey = CurrencyType.USD.ToString().ToLower(),
                                UsageUOMKey = UnitOfMeasureType.kWh.ToString().ToLower()
                            };

                            if (e.DisaggDetails == null)
                            {
                                e.DisaggDetails = new List<DisaggDetail>();
                            }

                            e.DisaggDetails.Add(dissagDetail);
                        }

                        //gas DisaggDetail
                        var gasCount = e.Appliances.Sum(ap => ap.DisaggDetails.FindAll(f => f.CommodityKey == commodityGas).Count());
                        if (gasCount > 0)
                        {
                            var cost = e.Appliances.Sum(ap => ap.DisaggDetails.FindAll(f => f.CommodityKey == commodityGas).Sum(dd => dd.CostAmount));
                            var usage = e.Appliances.Sum(ap => ap.DisaggDetails.FindAll(f => f.CommodityKey == commodityGas).Sum(dd => dd.UsageQuantity));

                            var dissagDetail = new DisaggDetail()
                            {
                                CommodityKey = commodityGas,
                                CostAmount = cost,
                                UsageQuantity = usage,
                                CostCurrencyKey = CurrencyType.USD.ToString().ToLower(),
                                UsageUOMKey = UnitOfMeasureType.CCF.ToString().ToLower()
                            };

                            if (e.DisaggDetails == null)
                            {
                                e.DisaggDetails = new List<DisaggDetail>();
                            }

                            e.DisaggDetails.Add(dissagDetail);

                        }

                        //water DisaggDetail
                        var waterCount = e.Appliances.Sum(ap => ap.DisaggDetails.FindAll(f => f.CommodityKey == commodityWater).Count());
                        if (waterCount > 0)
                        {
                            var cost = e.Appliances.Sum(ap => ap.DisaggDetails.FindAll(f => f.CommodityKey == commodityWater).Sum(dd => dd.CostAmount));
                            var usage = e.Appliances.Sum(ap => ap.DisaggDetails.FindAll(f => f.CommodityKey == commodityWater).Sum(dd => dd.UsageQuantity));

                            var dissagDetail = new DisaggDetail()
                            {
                                CommodityKey = commodityWater,
                                CostAmount = cost,
                                UsageQuantity = usage,
                                CostCurrencyKey = CurrencyType.USD.ToString().ToLower(),
                                UsageUOMKey = UnitOfMeasureType.Gal.ToString().ToLower()
                            };

                            if (e.DisaggDetails == null)
                            {
                                e.DisaggDetails = new List<DisaggDetail>();
                            }

                            e.DisaggDetails.Add(dissagDetail);

                        }
                    }
                }
            }
        }

        private void CreateMeasurementsWithinBillDisagg(Customer customer, List<MeasurementType> items)
        {
            foreach (var a in customer.Accounts)
            {
                foreach (var p in a.Premises)
                {
                    foreach (var e in p.EndUses)
                    {
                        foreach (var ap in e.Appliances)
                        {
                            if (ap.DisaggDetails != null)
                            {
                                CreateMeasurements(ap.DisaggDetails, items);
                            }

                        }

                        CreateMeasurements(e.DisaggDetails, items);
                    }
                }
            }
        }

        private void CreateMeasurements(List<DisaggDetail> disaggDetails, List<MeasurementType> items)
        {

            foreach (var dd in disaggDetails)
            {
                if (dd.Measurements == null) dd.Measurements = new List<Measurement>();

                foreach (var mt in items)
                {
                    switch (mt)
                    {
                        case MeasurementType.CO2:
                            dd.Measurements.Add(new Measurement()
                            {
                                Key = MeasurementType.CO2.ToString().ToLower(),
                                Quantity = dd.UsageQuantity * 1.5M,
                                Units = MeasurementUnitType.lbs
                            });

                            break;

                        case MeasurementType.Points:
                            dd.Measurements.Add(new Measurement()
                            {
                                Key = MeasurementType.Points.ToString().ToLower(),
                                Quantity = dd.UsageQuantity * 0.25M,
                                Units = MeasurementUnitType.pts
                            });
                            break;
                    }
                }
            }

        }

    }
}
