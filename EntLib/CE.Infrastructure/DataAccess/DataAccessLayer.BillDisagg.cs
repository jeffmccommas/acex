﻿using CE.Models.Insights;
using CE.Models.Insights.Types;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;

namespace CE.Infrastructure.DataAccess
{
    public partial class DataAccessLayer
    {
        /// <summary>
        /// GetBillDisagg overload.  Call this via the BillDisaggReader.
        /// </summary>
        /// <param name="clientId"></param>
        /// <param name="startDate"></param>
        /// <param name="endDate"></param>
        /// <param name="customerId"></param>
        /// <param name="accountId"></param>
        /// <param name="premiseId"></param>
        /// <param name="enduses"></param>
        /// <param name="appliances"></param>
        /// <param name="commodities"></param>
        /// <returns></returns>
        public BillDisaggResponse GetBillDisagg_Database(int clientId, DateTime startDate, DateTime endDate, string customerId, string accountId, string premiseId, IEnumerable<int> enduses, IEnumerable<int> appliances, IEnumerable<byte> commodities)
        {
            BillDisaggResponse res = null;
            List<BillDisaggApplianceRow> data = null;

            const string storedProcedureName = "[wh].[SelectBillDisaggAppliance]";
            const string param1_Name = "@ClientID";
            const string param2_Name = "@StartDate";
            const string param3_Name = "@EndDate";
            const string param4_Name = "@CustomerID";
            const string param5_Name = "@AccountID";
            const string param6_Name = "@PremiseID";
            const string param7_Name = "@EndUses";
            const string param8_Name = "@Appliances";
            const string param9_Name = "@Commodities";

            string aid = null;
            string pid = null;

            if (!string.IsNullOrEmpty(accountId))
            {
                aid = accountId;
            }

            if (!string.IsNullOrEmpty(premiseId))
            {
                pid = premiseId;
            }

            using (SqlConnection connection = new SqlConnection(GetConnection()))
            {
                using (var cmd = new SqlCommand(storedProcedureName, connection))
                {
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.Add(new SqlParameter(param1_Name, typeof(int)) { Value = clientId });
                    cmd.Parameters.Add(new SqlParameter(param2_Name, typeof(DateTime)) { Value = startDate });
                    cmd.Parameters.Add(new SqlParameter(param3_Name, typeof(DateTime)) { Value = endDate });
                    cmd.Parameters.Add(new SqlParameter(param4_Name, typeof(string)) { Value = customerId });
                    cmd.Parameters.Add(new SqlParameter(param5_Name, typeof(string)) { Value = aid });
                    cmd.Parameters.Add(new SqlParameter(param6_Name, typeof(string)) { Value = pid });

                    if (enduses != null)
                    {
                        var endUsesParameter = new SqlParameter();
                        endUsesParameter.ParameterName = param7_Name; //@EndUses
                        endUsesParameter.Direction = ParameterDirection.Input;
                        endUsesParameter.TypeName = "IntegerListTableType";
                        endUsesParameter.Value = enduses.ToIntegerListDataTable("Value");
                        cmd.Parameters.Add(endUsesParameter);
                    }
                    else
                    {
                        var endUsesParameter = new SqlParameter();
                        endUsesParameter.ParameterName = param7_Name; //@EndUses
                        endUsesParameter.Direction = ParameterDirection.Input;
                        endUsesParameter.Value = null;
                        cmd.Parameters.Add(endUsesParameter);
                    }

                    if (appliances != null)
                    {
                        var appliancesParameter = new SqlParameter();
                        appliancesParameter.ParameterName = param8_Name; //@Appliances
                        appliancesParameter.Direction = ParameterDirection.Input;
                        appliancesParameter.TypeName = "IntegerListTableType";
                        appliancesParameter.Value = appliances.ToIntegerListDataTable("Value");
                        cmd.Parameters.Add(appliancesParameter);
                    }
                    else
                    {
                        var appliancesParameter = new SqlParameter();
                        appliancesParameter.ParameterName = param8_Name; //@Appliances
                        appliancesParameter.Direction = ParameterDirection.Input;
                        appliancesParameter.Value = null;
                        cmd.Parameters.Add(appliancesParameter);
                    }

                    if (commodities != null)
                    {
                        var commoditiesParameter = new SqlParameter();
                        commoditiesParameter.ParameterName = param9_Name; //@Commodities
                        commoditiesParameter.Direction = ParameterDirection.Input;
                        commoditiesParameter.TypeName = "ByteListTableType";
                        commoditiesParameter.Value = commodities.ToByteListDataTable("Value");
                        cmd.Parameters.Add(commoditiesParameter);
                    }
                    else
                    {
                        var commoditiesParameter = new SqlParameter();
                        commoditiesParameter.ParameterName = param9_Name; //@Commodities
                        commoditiesParameter.Direction = ParameterDirection.Input;
                        commoditiesParameter.Value = null;
                        cmd.Parameters.Add(commoditiesParameter);
                    }

                    connection.Open();

                    using (var reader = cmd.ExecuteReader())
                    {
                        if (reader.HasRows)
                        {
                            data = PopulateBillDisaggDataList(reader);
                        }
                    }

                }

            }

            if (data != null)
            {
                res = BuildBillDisaggResponse(data);
            }

            return (res);
        }


        /// <summary>
        /// Populate list of data from pseudo warehouse call.
        /// </summary>
        /// <param name="reader"></param>
        /// <returns></returns>
        private List<BillDisaggApplianceRow> PopulateBillDisaggDataList(SqlDataReader reader)
        {
            List<BillDisaggApplianceRow> rowList = new List<BillDisaggApplianceRow>();
            BillDisaggApplianceRow row = null;

            while (reader.Read())
            {
                row = new BillDisaggApplianceRow()
                {
                    ClientID = Convert.ToInt32(reader["ClientID"]),
                    CustomerID = reader["CustomerID"].ToString(),
                    AccountID = reader["AccountID"].ToString(),
                    PremiseID = reader["PremiseID"].ToString(),
                    BillStartDate = Convert.ToDateTime(reader["MinBillDate"]),
                    BillEndDate = Convert.ToDateTime(reader["MaxBillDate"]),
                    EndUseID = (EndUseType)Convert.ToInt32(reader["EndUseID"]),
                    ApplianceID = (ApplianceType)Convert.ToInt32(reader["ApplianceID"]),
                    CommodityID = (CommodityType)Convert.ToInt32(reader["CommodityID"]),
                    Usage = Decimal.Round(Convert.ToDecimal(reader["Usage"]), 3),
                    UsageUomID = (UnitOfMeasureType)Convert.ToInt32(reader["UsageUomID"]),
                    Cost = Decimal.Round(Convert.ToDecimal(reader["Cost"]), 2),
                    CurrencyID = (CurrencyType)Convert.ToInt32(reader["CurrencyId"]),
                };

                rowList.Add(row);
            }

            return (rowList);
        }


        /// <summary>
        /// Strongly typed row data to fill from flat query.
        /// </summary>
        private class BillDisaggApplianceRow
        {
            public int ClientID { get; set; }
            public string CustomerID { get; set; }
            public string AccountID { get; set; }
            public DateTime BillStartDate { get; set; }
            public DateTime BillEndDate { get; set; }
            public string PremiseID { get; set; }
            public EndUseType EndUseID { get; set; }
            public ApplianceType ApplianceID { get; set; }
            public CommodityType CommodityID { get; set; }
            public decimal Usage { get; set; }
            public UnitOfMeasureType UsageUomID { get; set; }
            public decimal Cost { get; set; }
            public CurrencyType CurrencyID { get; set; }
        }


        /// <summary>
        /// Build the heirarchical BillDisagg response object from the flat rows.  The result is what the caller will serialize to json.
        /// </summary>
        /// <param name="dataList"></param>
        /// <returns></returns>
        private BillDisaggResponse BuildBillDisaggResponse(List<BillDisaggApplianceRow> dataList)
        {
            BillDisaggResponse res = null;
            Customer customer = new Customer();
            Account account = null;
            Premise premise = null;
            EndUse enduse = null;
            Appliance appliance = null;
            DisaggDetail detail = null;

            var lastAccountId = string.Empty;
            var lastPremiseId = string.Empty;
            EndUseType? lastEndUseId = null;
            ApplianceType? lastApplianceId = null;
            CommodityType? lastCommodityId = null;

            bool accountAdded = false;
            bool premiseAdded = false;
            bool endUseAdded = false;
            bool applianceAdded = false;
            bool commodityAdded = false;

            foreach (var row in dataList)
            {
                if (lastAccountId != row.AccountID)
                {
                    // account
                    lastAccountId = row.AccountID;
                    if (account != null)
                    {
                        appliance.DisaggDetails.Add(detail);  //special
                        enduse.Appliances.Add(appliance);  //special
                        premise.EndUses.Add(enduse); //special
                        account.Premises.Add(premise); //special
                        customer.Accounts.Add(account);
                        accountAdded = true;
                    }

                    account = new Account()
                    {
                        Id = row.AccountID,
                        BillStartDate = row.BillStartDate,
                        BillEndDate = row.BillEndDate
                    };
                }


                if (lastPremiseId != row.PremiseID || accountAdded)
                {
                    // premise
                    lastPremiseId = row.PremiseID;
                    if (!accountAdded && premise != null)
                    {
                        appliance.DisaggDetails.Add(detail);  //special
                        enduse.Appliances.Add(appliance);  //special
                        premise.EndUses.Add(enduse); //special
                        account.Premises.Add(premise);
                        premiseAdded = true;
                    }

                    premise = new Premise()
                    {
                        Id = row.PremiseID,
                        ProfileStatus = ProfileStatusType.Partial
                    };
                }


                if (lastEndUseId == null || premiseAdded || accountAdded || (lastEndUseId != null && (lastEndUseId.Value != row.EndUseID)))
                {
                    // enduse
                    lastEndUseId = row.EndUseID;
                    if (!accountAdded && !premiseAdded && enduse != null)
                    {
                        appliance.DisaggDetails.Add(detail);  //special
                        enduse.Appliances.Add(appliance);  //special
                        premise.EndUses.Add(enduse);
                        endUseAdded = true;
                    }

                    enduse = new EndUse()
                    {
                        Key = row.EndUseID.ToString().ToLower(),
                    };
                }

                if (lastApplianceId == null || endUseAdded || premiseAdded || accountAdded || (lastApplianceId != null && (lastApplianceId.Value != row.ApplianceID)))
                {
                    // appliance
                    lastApplianceId = row.ApplianceID;
                    if (!accountAdded && !premiseAdded && !endUseAdded && appliance != null)
                    {
                        appliance.DisaggDetails.Add(detail); //special
                        enduse.Appliances.Add(appliance);
                        applianceAdded = true;
                    }

                    appliance = new Appliance()
                    {
                        Key = row.ApplianceID.ToString().ToLower(),
                    };
                }


                if (lastCommodityId == null || applianceAdded || endUseAdded || premiseAdded || accountAdded || (lastCommodityId != null && (lastCommodityId.Value != row.CommodityID)))
                {
                    // disaggDetail (via commodity)
                    lastCommodityId = row.CommodityID;
                    if (!accountAdded && !premiseAdded && !endUseAdded && !applianceAdded && detail != null)
                    {
                        appliance.DisaggDetails.Add(detail);
                        commodityAdded = true;
                    }

                    detail = new DisaggDetail()
                    {
                        CommodityKey = row.CommodityID.ToString().ToLower(),
                        UsageQuantity = row.Usage,
                        UsageUOMKey = row.UsageUomID.ToString().ToLower(),
                        CostAmount = row.Cost,
                        CostCurrencyKey = row.CurrencyID.ToString().ToLower()
                    };
                }

                // reset all of these
                if (accountAdded) accountAdded = false;
                if (premiseAdded) premiseAdded = false;
                if (endUseAdded) endUseAdded = false;
                if (applianceAdded) applianceAdded = false;
                if (commodityAdded) commodityAdded = false;

            }

            // last items need to be added
            appliance.DisaggDetails.Add(detail);
            enduse.Appliances.Add(appliance);
            premise.EndUses.Add(enduse);
            account.Premises.Add(premise);
            customer.Accounts.Add(account);

            res = new BillDisaggResponse();
            res.Customer = customer;
            res.ClientId = dataList[0].ClientID;

            return (res);
        }

    }

    public static class CEExtensions
    {
        /// <summary>
        /// Extension method to aid in filling TVP of integers for stored procedures.
        /// </summary>
        /// <param name="intValues"></param>
        /// <param name="columnName"></param>
        /// <returns></returns>
        public static DataTable ToIntegerListDataTable(this IEnumerable<int> intValues, string columnName)
        {
            var tbl = new DataTable();

            tbl.Columns.Add(columnName);

            foreach (var intValue in intValues)
            {
                var nextRow = tbl.NewRow();
                nextRow[columnName] = intValue;
                tbl.Rows.Add(nextRow);
            }

            return (tbl);
        }

        /// <summary>
        /// Extension method to aid in filling TVP of bytes for stored procedures.
        /// </summary>
        /// <param name="byteValues"></param>
        /// <param name="columnName"></param>
        /// <returns></returns>
        public static DataTable ToByteListDataTable(this IEnumerable<byte> byteValues, string columnName)
        {
            var tbl = new DataTable();

            tbl.Columns.Add(columnName);

            foreach (var byteValue in byteValues)
            {
                var nextRow = tbl.NewRow();
                nextRow[columnName] = byteValue;
                tbl.Rows.Add(nextRow);
            }

            return (tbl);
        }


    }
}