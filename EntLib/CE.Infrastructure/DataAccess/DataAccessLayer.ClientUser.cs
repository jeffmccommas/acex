﻿using System;
using System.Data;
using System.Data.SqlClient;
using CE.Models;

namespace CE.Infrastructure.DataAccess
{
    public partial class DataAccessLayer
    {
        /// <summary>
        /// GetClientUser_Database.  Call this via the ClientUserReader.
        /// </summary>
        /// <param name="accessKeyID"></param>
        /// <returns></returns>
        public ClientUser GetClientUser_Database(string accessKeyID, EnvironmentType environmentID)
        {
            ClientUser cu = null;
            const string storedProcedureName = "GetClientUserByAccessKey";
            const string param1_Name = "@CEAccessKeyID";
            const string param2_Name = "@EnvID";

            using (SqlConnection connection = new SqlConnection(GetConnection()))
            {
                using (var cmd = new SqlCommand(storedProcedureName, connection))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter(param1_Name, typeof(string)) { Value = accessKeyID });
                    cmd.Parameters.Add(new SqlParameter(param2_Name, typeof(int)) { Value = (int)environmentID });

                    connection.Open();

                    using (var reader = cmd.ExecuteReader())
                    {
                        // User with client & user properties
                        while (reader.Read())
                        {
                            cu = new ClientUser();
                            cu.Enabled = true;
                            cu.UserID = Convert.ToInt32(reader["UserID"]);
                            cu.ClientID = Convert.ToInt32(reader["ClientID"]);
                            cu.CEAccessKeyID = reader["CEAccessKeyID"].ToString();
                            cu.CESecretAccessKey = reader["CESecretAccessKey"].ToString();
                            cu.BasicKey = reader["BasicKey"].ToString();
                            cu.ClientAuthType = (AuthType)Convert.ToInt32(reader["AuthType"]);
                            cu.Environment = environmentID;
                            cu.RateCompanyID = Convert.ToInt32(reader["RateCompanyID"]);
                            // always add list objects, fill below if applicable.
                            cu.Endpoints = new System.Collections.Generic.List<Endpoint>();
                            cu.Roles = new System.Collections.Generic.List<Role>();
                            cu.ClientProperties = new System.Collections.Generic.List<ClientProperties>();
                        }

                        // user endpoints
                        if (reader.NextResult())
                        {
                            while (reader.Read())
                            {
                                cu.Endpoints.Add(new Endpoint((EndpointType)Convert.ToInt32(reader["EndpointID"]), reader["Name"].ToString(), reader["ShortName"].ToString()));
                            }
                        }

                        // user roles
                        if (reader.NextResult())
                        {
                            while (reader.Read())
                            {
                                cu.Roles.Add(new Role((RoleType)Convert.ToInt32(reader["RoleID"]), reader["Name"].ToString()));
                            }
                        }

                        // client properties
                        if (reader.NextResult())
                        {
                            while (reader.Read())
                            {
                                cu.ClientProperties.Add(new ClientProperties((PropertyType)Convert.ToInt32(reader["PropertyID"]), reader["PropValue"].ToString()));
                            }
                        }

                    }

                }
            }

            return (cu);
        }

    }
}
