﻿
namespace CE.Infrastructure.Messages
{
    public class Error
    {
        public string Name { get; set; }
        public string Message { get; set; }
    }
}
