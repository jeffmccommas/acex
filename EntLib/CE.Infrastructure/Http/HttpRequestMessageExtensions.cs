﻿using System;
using System.Linq;
using System.Net.Http;
using CE.Infrastructure.DataAccess;

namespace CE.Infrastructure
{
    public static class HttpRequestMessageExtensions
    {
        private const string HttpContext = "MS_HttpContext";
        private const string RemoteEndpointMessage = "System.ServiceModel.Channels.RemoteEndpointMessageProperty";

        public static bool IsLocal(this HttpRequestMessage request)
        {
            var localFlag = request.Properties["MS_IsLocal"] as Lazy<bool>;
            return localFlag != null && localFlag.Value;
        }

        public static string GetClientIpAddress(this HttpRequestMessage request)
        {
            //dynamic req = request.Properties[HttpContext];
            //if (req != null)
            //{
            //    // Look for a proxy address first
            //    var ip = req.Request.ServerVariables["HTTP_X_FORWARDED_FOR"];

            //    // If there is no proxy, get the standard remote address
            //    if(ip == null || ip == "" || ip.ToLower == "unknown")
            //    return req.Request.ServerVariables["REMOTE_ADDR"];
            //    return ip;
            //}
            
            if (request.Properties.ContainsKey(HttpContext))
            {
                dynamic ctx = request.Properties[HttpContext];
                if (ctx != null)
                {
                    return ctx.Request.UserHostAddress;
                }
            }

            if (request.Properties.ContainsKey(RemoteEndpointMessage))
            {
                dynamic remoteEndpoint = request.Properties[RemoteEndpointMessage];
                if (remoteEndpoint != null)
                {
                    return remoteEndpoint.Address;
                }
            }
            return null;
        }

        public static void GetClientUser(this HttpRequestMessage request)
        {
            // grab accessKeyID from header, use to lookup
            if (!request.Headers.Contains(HeaderConfiguration.UsernameHeader)) return;
            var accessKeyId = request.Headers.GetValues(HeaderConfiguration.UsernameHeader).First();
            var reader = new ClientUserReader();
            var clientUser = reader.GetClientUser(accessKeyId);

            if (clientUser != null)
            {
                if (!request.Properties.ContainsKey(CEConfiguration.CEClientUser))
                {
                    request.Properties.Add(CEConfiguration.CEClientUser, clientUser);
                }
            }
            if (!request.Properties.ContainsKey(CEConfiguration.CELocale) && request.Headers.Contains(HeaderConfiguration.LocaleHeader))
            {
                request.Properties.Add(CEConfiguration.CELocale, request.Headers.GetValues(HeaderConfiguration.LocaleHeader).First());
            }
        }
    }
}
