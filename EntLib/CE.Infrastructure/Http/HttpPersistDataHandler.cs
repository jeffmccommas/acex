﻿using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;

namespace CE.Infrastructure
{
    public class HttpPersistDataHandler : DelegatingHandler
    {
        protected override Task<HttpResponseMessage> SendAsync(HttpRequestMessage request, CancellationToken cancellationToken)
        {
            request.GetClientUser();
            Task<HttpResponseMessage> response = null;
            response = base.SendAsync(request, cancellationToken);
            return response.ContinueWith(task =>
            {
                var httpResponse = task.Result;
                return httpResponse;
            });
        }
    }
}
