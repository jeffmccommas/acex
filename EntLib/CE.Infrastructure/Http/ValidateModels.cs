﻿using System.Collections.Generic;

using System.ComponentModel.DataAnnotations;
using System.Web.Http.ModelBinding;

namespace CE.Infrastructure
{
    /// <summary>
    /// This will accept your annotated object and check each property appropriately.  
    /// </summary>
    public static class ValidateModels
    {

        /// <summary>
        /// The provided object will be validated against any annotations on the model properties.
        /// The returned model state dictionary has error information when the model does not successfully validate.
        /// </summary>
        /// <param name="req"></param>
        /// <param name="success"></param>
        /// <returns></returns>
        public static ModelStateDictionary Validate(object req, out bool success)
        {
            ModelStateDictionary dict = null;
            ValidationContext context = new ValidationContext(req, null, null);
            List<ValidationResult> results = new List<ValidationResult>();
            bool valid = Validator.TryValidateObject(req, context, results, true);
            success = true;

            if (!valid)
            {
                dict = new ModelStateDictionary();

                foreach (var r in results)
                {
                    foreach (var k in r.MemberNames)
                    {
                        dict.AddModelError(k, r.ErrorMessage);
                    }

                }

                success = false;
            }

            return (dict);
        }
    }
}
