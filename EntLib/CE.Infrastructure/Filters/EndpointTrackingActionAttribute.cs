﻿using System;
using System.Linq;
using System.Web.Http.Controllers;
using CE.GreenButtonConnect;
using CE.Models;

namespace CE.Infrastructure.Filters
{

    /// <summary>
    /// Endpoint tracking action filter.
    /// </summary>
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method)]
    public class EndpointTrackingActionAttribute : System.Web.Http.Filters.ActionFilterAttribute
    {
        public override void OnActionExecuting(HttpActionContext actionContext)
        {
            if (actionContext.Request.Properties.ContainsKey(CEConfiguration.CEClientUser))
            {
                var tracker =
                    new NLogEndpointTrackingWriter();
                var record = new EndpointTrack();

                var clientuser = ((ClientUser) actionContext.Request.Properties[CEConfiguration.CEClientUser]);
                record.ClientId = clientuser.ClientID;
                record.UserId = clientuser.UserID;
                record.UserAgent = actionContext.Request.Headers.UserAgent.ToString();
                record.Verb = actionContext.Request.Method.Method;
                record.ResourceName = actionContext.Request.RequestUri.AbsolutePath;
                record.QueryString = actionContext.Request.RequestUri.Query;
                record.Include = true;

                var controller = record.ResourceName.ToLower()
                    .Split(new[] {"api/"}, StringSplitOptions.None)
                    .LastOrDefault();

                var endpoint =
                    clientuser.Endpoints.Find(
                        e => controller != null && e.Name.Contains(controller));

                if (endpoint != null)
                {
                    record.EndpointId = (int) endpoint.Id;

                    if (actionContext.Request.Headers.Contains(HeaderConfiguration.MessageIdHeader))
                    {
                        record.XCEMessageId =
                            actionContext.Request.Headers.GetValues(HeaderConfiguration.MessageIdHeader).First();
                    }

                    if (actionContext.Request.Headers.Contains(HeaderConfiguration.ChannelHeader))
                    {
                        record.XCEChannel =
                            actionContext.Request.Headers.GetValues(HeaderConfiguration.ChannelHeader).First();
                    }

                    if (actionContext.Request.Headers.Contains(HeaderConfiguration.LocaleHeader))
                    {
                        record.XCELocale =
                            actionContext.Request.Headers.GetValues(HeaderConfiguration.LocaleHeader).First();
                    }

                    if (actionContext.Request.Headers.Contains(HeaderConfiguration.MetaHeader))
                    {
                        record.XCEMeta = actionContext.Request.Headers.GetValues(HeaderConfiguration.MetaHeader).First();

                        //control panel callers hitting a client endpoint should have endpoint records excluded from totals when querying results
                        //so the Include property is set to false when this custom meta string is detected.  We use the # so it is less likely to occur by accident.
                        if (record.XCEMeta.ToLower().Contains("#cecp:1#"))
                        {
                            record.Include = false;
                        }
                    }

                    if (actionContext.Request.Properties.ContainsKey("MS_HttpContext"))
                    {
                        record.SourceIP =
                            ((System.Web.HttpContextWrapper) actionContext.Request.Properties["MS_HttpContext"]).Request
                                .UserHostAddress;
                    }

                    tracker.Track(record);
                }
            }
            else
            {
                var req = actionContext.Request.RequestUri.AbsolutePath.IndexOf("resource", StringComparison.Ordinal);
                // Green Button Connect Tracking
                if (req > -1)
                {
                    var tracker =
                        new NLogEndpointTrackingWriter();
                    var record = new EndpointTrack();
                    
                    record.UserId = -1; // always -1 for green button connect api trackings
                    var token = actionContext.Request.Headers.Authorization.Parameter;

                    var decVal = Helper.Base64Decode(token);
                    decVal = Helper.Decrypt(decVal);
                    var thirdPartyClientId = decVal.Split(',')[0];
                    record.ThirdPartyClientId = thirdPartyClientId;
                    if(decVal.Split(',').GetUpperBound(0) > 2)
                    {
                        var clientId = Convert.ToInt32(Helper.GetAclaraClientIdFromDataCustodianId(decVal.Split(',')[3]).Split(':')[0]);
                        record.ClientId = clientId; // Ameren for now
                    }
                    else
                    {
                        record.ClientId = -1;
                    }
                    record.UserAgent = actionContext.Request.Headers.UserAgent.ToString();
                    record.Verb = actionContext.Request.Method.Method;
                    record.ResourceName = actionContext.Request.RequestUri.AbsolutePath;
                    record.QueryString = actionContext.Request.RequestUri.Query;
                    record.Include = true;
                    if (actionContext.Request.Headers.Contains(HeaderConfiguration.MessageIdHeader))
                    {
                        record.XCEMessageId =
                            actionContext.Request.Headers.GetValues(HeaderConfiguration.MessageIdHeader).First();
                    }

                    if (actionContext.Request.Headers.Contains(HeaderConfiguration.ChannelHeader))
                    {
                        record.XCEChannel =
                            actionContext.Request.Headers.GetValues(HeaderConfiguration.ChannelHeader).First();
                    }

                    if (actionContext.Request.Headers.Contains(HeaderConfiguration.LocaleHeader))
                    {
                        record.XCELocale =
                            actionContext.Request.Headers.GetValues(HeaderConfiguration.LocaleHeader).First();
                    }

                    if (actionContext.Request.Headers.Contains(HeaderConfiguration.MetaHeader))
                    {
                        record.XCEMeta =
                            actionContext.Request.Headers.GetValues(HeaderConfiguration.MetaHeader).First();

                        //control panel callers hitting a client endpoint should have endpoint records excluded from totals when querying results
                        //so the Include property is set to false when this custom meta string is detected.  We use the # so it is less likely to occur by accident.
                        if (record.XCEMeta.ToLower().Contains("#cecp:1#"))
                        {
                            record.Include = false;
                        }
                    }

                    if (actionContext.Request.Properties.ContainsKey("MS_HttpContext"))
                    {
                        record.SourceIP =
                            ((System.Web.HttpContextWrapper)actionContext.Request.Properties["MS_HttpContext"])
                                .Request
                                .UserHostAddress;
                    }

                    tracker.Track(record);
                }
            }

        }

    }
}
