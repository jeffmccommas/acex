﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using System.Web.Http.Controllers;
using CE.Models;

namespace CE.Infrastructure.Encrypter
{
    /// <summary>
    /// Action filter for enc processing.
    /// </summary>
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method)]
    public class EncryptedActionParameterAttribute : System.Web.Http.Filters.ActionFilterAttribute,
    System.Web.Http.Filters.IActionFilter, System.Web.Http.Filters.IFilter
    {
        public override void OnActionExecuting(HttpActionContext actionContext)
        {
            ClientUser clientUser = null;
            string secret = string.Empty;
            if (HttpContext.Current.Request.QueryString.Get("enc") != null)
            {
                if (actionContext.Request.Properties.ContainsKey(CEConfiguration.CEClientUser))
                {
                    clientUser = ((ClientUser)actionContext.Request.Properties[CEConfiguration.CEClientUser]);

                    if (clientUser != null)
                    {
                        if (clientUser.ClientAuthType == AuthType.CE)
                        {
                            secret = clientUser.CESecretAccessKey;
                        }
                        else
                        {
                            secret = clientUser.BasicKey;
                        }
                    }
                }

                if (secret.Length > 0)
                {
                    Dictionary<string, object> decryptedParameters = new Dictionary<string, object>();
                    string encryptedQueryString = HttpContext.Current.Request.QueryString.Get("enc");
                    string decrptedString = Decrypt(encryptedQueryString.ToString(), secret);
                    string[] paramsArrs = decrptedString.Split('&');

                    for (int i = 0; i < paramsArrs.Length; i++)
                    {
                        string[] paramArr = paramsArrs[i].Split('=');
                        decryptedParameters.Add(paramArr[0], paramArr[1]);
                    }

                    //actionContext.ActionArguments.Remove("enc");
                    for (int i = 0; i < decryptedParameters.Count; i++)
                    {
                        actionContext.ActionArguments[decryptedParameters.Keys.ElementAt(i)] = decryptedParameters.Values.ElementAt(i);
                    }
                }
            }
        }

        private string Decrypt(string encryptedText, string secret)
        {
            byte[] DecryptKey = { };
            byte[] IV = { 55, 34, 87, 64, 87, 195, 54, 21 };
            byte[] inputByte = new byte[encryptedText.Length];
            DecryptKey = System.Text.Encoding.UTF8.GetBytes(secret.Substring(0, 8));
            DESCryptoServiceProvider des = new DESCryptoServiceProvider();
            inputByte = Convert.FromBase64String(HttpUtility.UrlDecode(encryptedText));
            MemoryStream ms = new MemoryStream();
            CryptoStream cs = new CryptoStream(ms, des.CreateDecryptor(DecryptKey, IV), CryptoStreamMode.Write);
            cs.Write(inputByte, 0, inputByte.Length);
            cs.FlushFinalBlock();
            System.Text.Encoding encoding = System.Text.Encoding.UTF8;
            return encoding.GetString(ms.ToArray());
        }

        public static string Encrypt(string plainText, string secret)
        {
            byte[] EncryptKey = { };
            byte[] IV = { 55, 34, 87, 64, 87, 195, 54, 21 };
            EncryptKey = System.Text.Encoding.UTF8.GetBytes(secret.Substring(0, 8));
            DESCryptoServiceProvider des = new DESCryptoServiceProvider();
            byte[] inputByte = Encoding.UTF8.GetBytes(plainText);
            MemoryStream mStream = new MemoryStream();
            CryptoStream cStream = new CryptoStream(mStream, des.CreateEncryptor(EncryptKey, IV), CryptoStreamMode.Write);
            cStream.Write(inputByte, 0, inputByte.Length);
            cStream.FlushFinalBlock();
            return HttpUtility.UrlEncode(Convert.ToBase64String(mStream.ToArray()));
        }

        public static string Base64Encode(string plainText)
        {
            var plainTextBytes = System.Text.Encoding.UTF8.GetBytes(plainText);
            return System.Convert.ToBase64String(plainTextBytes);
        }

        public static string Base64Decode(string base64EncodedData)
        {
            var base64EncodedBytes = System.Convert.FromBase64String(base64EncodedData);
            return System.Text.Encoding.UTF8.GetString(base64EncodedBytes);
        }
    }
}
