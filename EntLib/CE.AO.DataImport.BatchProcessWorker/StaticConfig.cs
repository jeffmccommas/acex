﻿namespace CE.AO.DataImport.BatchProcessWorker
{
    /// <summary>
    /// Constant variables are defined in this class
    /// </summary>
    public static class StaticConfig
    {
        public const string AclaraOneStorageConnectionString = "AclaraOneStorageConnectionString";
        public const string ImportDataEventHubSendConnectionString = "ImportDataEventHubSendConnectionString";
        public const string ImportDataEventHubPath = "ImportDataEventHubPath";
        public const string ImportBatchProcessQueue = "%ImportBatchProcessQueue%";
    }
}