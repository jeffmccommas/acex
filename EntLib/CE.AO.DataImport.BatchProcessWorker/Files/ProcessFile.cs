﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using CE.AO.Logging;
using CE.AO.Models;
using CE.AO.Models.RawFileModels;
using CsvHelper;
using Microsoft.Azure;
using Microsoft.ServiceBus.Messaging;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Blob;
using Newtonsoft.Json;
using Enums = CE.AO.Utilities.Enums;

namespace CE.AO.DataImport.BatchProcessWorker.Files
{
    /// <summary>
    /// ProcessFile abstract class which is inherited by all other classes which processes specific files
    /// </summary>
    public abstract class ProcessFile
    {
        /// <summary>
        /// Parameterized contructor
        /// </summary>
        /// <param name="logModel">Used for logging details in case of any error</param>
        protected ProcessFile(LogModel logModel)
        {
            LogModel = logModel;
        }

        /// <summary>
        /// Initialize processing of file abstract method
        /// </summary>
        /// <param name="fileName">filename of file to process</param>
        /// <param name="containerName">blob container name</param>
        /// <param name="clientId">client id</param>
        public abstract void Initialize(string fileName, string containerName, string clientId);

        /// <summary>
        /// Used for logging details in case of any error
        /// </summary>
        public LogModel LogModel { get; set; }

        /// <summary>
        /// CloudBlobClient private variable
        /// </summary>
        private CloudBlobClient _cloudBlobClient;

        private string _lastConvertRoot = "";
        private string _lastImportRoot = "";
        private string _lastValidateRoot = "";
        private string _lastPurgeRoot = "";

        public bool ArchiveBlob = true;

        public CloudBlobClient CloudBlobClient
        {
            get
            {
                if (_cloudBlobClient != null) return _cloudBlobClient;
                var storageAccount =
                    CloudStorageAccount.Parse(
                        CloudConfigurationManager.GetSetting(StaticConfig.AclaraOneStorageConnectionString));
                _cloudBlobClient = storageAccount.CreateCloudBlobClient();
                return _cloudBlobClient;
            }
        }

        /// <summary>
        /// Get Event Hub client
        /// </summary>
        /// <param name="connectionString">Event Hub connection string</param>
        /// <param name="path">The path to the Event Hub</param>
        /// <returns>EventHubClient</returns>
        private static EventHubClient GetEventHubClient(string connectionString, string path)
        {
            var messagingFactory = MessagingFactory.CreateFromConnectionString(connectionString);
            messagingFactory.GetSettings().OperationTimeout = TimeSpan.FromHours(1);
            var client = messagingFactory.CreateEventHubClient(path);
            return client;
        }

        /// <summary>
        /// Add custom properties to the event
        /// </summary>
        /// <param name="data">Event data</param>
        /// <param name="customProperties">Custom properties that needs to be added</param>
        /// <param name="logModel">Used for logging details in case of any error</param>
        public void AddCustomProperties(EventData data, IDictionary<string, object> customProperties, LogModel logModel)
        {
            if (customProperties.Count > 0)
            {
                foreach (var property in customProperties)
                {
                    data.Properties.Add(property);
                }
            }
            data.Properties.Add("Source", logModel.Source);
            data.Properties.Add("ProcessingType", Convert.ToString(logModel.ProcessingType));
            data.Properties.Add("RowIndex", Convert.ToString(logModel.RowIndex));
        }

        /// <summary>
        /// Gets instance of event hub client
        /// </summary>
        /// <returns>Instance of Event Hub Client</returns>
        public virtual EventHubClient GetEventHubClient()
        {
            var connection = CloudConfigurationManager.GetSetting(StaticConfig.ImportDataEventHubSendConnectionString);
            var eventHubPath = CloudConfigurationManager.GetSetting(StaticConfig.ImportDataEventHubPath);
            return GetEventHubClient(connection, eventHubPath);
        }

        /// <summary>
        /// Close the connection for instance of event hub client
        /// </summary>
        /// <param name="client">Instance of Event Hub Client</param>
        public virtual void CloseEventHubClient(EventHubClient client)
        {
            client.Close();
        }

        public virtual void PreProcessRow(Enums.EventType eventType, IRawFileModel record)
        {

        }

        /// <summary>
        /// Process event
        /// </summary>
        /// <typeparam name="T">Type of event</typeparam>
        /// <param name="blockBlob">Blob</param>
        /// <param name="customProperties">Custom properties that needs to be added to the event</param>
        /// <param name="eventType">EventType enum which specifies the type of event</param>
        /// <param name="client">Instance of Event Hub client</param>
        public virtual void Process<T>(ICloudBlob blockBlob, IDictionary<string, object> customProperties,
            Enums.EventType eventType, EventHubClient client) where T : IRawFileModel
        {
            var index = 1;
            var eventsList = new List<EventData>();

            var stream = blockBlob.OpenRead();
            var sr = new StreamReader(stream);

            using (var reader = new CsvReader(sr))
            {
                reader.Configuration.IsHeaderCaseSensitive = false;
                reader.Configuration.TrimHeaders = true;
                reader.Configuration.TrimFields =
                    true; //If there is any space in blank row then reader.IsRecordEmpty() will be true and that row will not get processed.
                while (reader.Read())
                {
                    try
                    {
                        if (!reader.IsRecordEmpty())
                        {
                            LogModel.Metadata = JsonConvert.SerializeObject(reader.CurrentRecord);
                            LogModel.RowIndex = Convert.ToString(index);
                            var record = reader.GetRecord<T>();

                            PreProcessRow(eventType, record);

                            var serializedEventData = JsonConvert.SerializeObject(record);
                            var data = new EventData(Encoding.UTF8.GetBytes(serializedEventData));

                            AddCustomProperties(data, customProperties, LogModel);
                            eventsList.Add(data);

                            if (index == 1)
                            {
                                var firstEventData = new EventData();
                                AddCustomProperties(firstEventData, customProperties, LogModel);
                                firstEventData.Properties.Add("FirstEventOfFile", 1);
                                client.Send(firstEventData);
                            }

                            if (index % 100 == 0)
                            {
                                SendEventsInBatch(client, eventsList);
                                eventsList = new List<EventData>();
                                if (index % 1000 == 0)
                                {
                                    LogModel.ProcessingEventCount = index;
                                    Logger.Info($"{index} records ingested into Batch Process Event Hub.", LogModel);
                                }
                            }
                            index++;
                        }
                    }
                    catch (Exception ex)
                    {
                        Logger.Error(ex, LogModel);
                    }
                }
            }

            if (eventsList.Count > 0)
                SendEventsInBatch(client, eventsList);

            var lastEventData = new EventData();
            AddCustomProperties(lastEventData, customProperties, LogModel);
            lastEventData.Properties.Add("LastEventOfFile", index - 1);
            client.Send(lastEventData);

            LogModel.ProcessingEventCount = index - 1;
            LogModel.Metadata = null;
            Logger.Info($"{index - 1} records ingested into Batch Process Event Hub.", LogModel);
        }
        
        public virtual bool ProcessAdvanceAmi(ICloudBlob blockBlob, string fileName, string interval, string containerName)
        {
            string containerN = (containerName == null) ? "nocontainer" : containerName;
            string info = $"{interval}  {fileName} container={containerN} ";
            Logger.Info(info, LogModel);
            Debug.WriteLine(info);
            string importControl = "enabled";
            string destLocation = CloudConfigurationManager.GetSetting("AdvAmiImportDirectory");
            if (!string.IsNullOrEmpty(destLocation))
            {
                Debug.WriteLine($"{info} cfglocation={destLocation}");
                importControl = destLocation;
            }
            bool success = false;
            info = $"{interval} is {importControl}, {info}";
            if (importControl.Equals("disabled"))
            {
                Logger.Warn(info, LogModel);
                return false;
            }
            Logger.Info(info, LogModel);

            try
            {
                //string fnameAdjust = DateTime.Now.ToString("MMddyyyy_HHmmssFFFFFF") + "_";
                string filePath = Path.Combine(importControl, fileName);
                info += $" destfile={filePath}";
                StringWriter sw = new StringWriter(); // watch this for huge files -- maybe keep under 500MB?
                List<string> firstFewLines = new List<string>();
                {
                    int maxFirstLines = 10;
                    using (StreamReader reader = new StreamReader(blockBlob.OpenRead()))
                    {
                        while (!reader.EndOfStream)
                        {
                            string row = reader.ReadLine();
                            if (!string.IsNullOrEmpty(row))
                            {
                                sw.WriteLine(row);
                                if (maxFirstLines-- > 0)
                                {
                                    firstFewLines.Add(row);
                                }
                            }
                        }
                    }
                    sw.Flush();
                    Debug.WriteLine($" {interval} finished blob extract {info}");
                    Logger.Info($" {interval} finished blob extract {info}", LogModel);
                }
                AmiAdvancedConnector advAmiC = new AmiAdvancedConnector();

                string impResult = advAmiC.ImportFile(filePath, interval, LogModel, firstFewLines, sw.ToString());
                _lastConvertRoot = advAmiC.LastConvertRoot;
                _lastImportRoot = advAmiC.LastImportRoot;
                _lastValidateRoot = advAmiC.LastValidateRoot;
                _lastPurgeRoot = advAmiC.LastPurgeRoot;
                Logger.Info($" {interval} finished import {info}", LogModel);
                if (!impResult.ToLower().Contains("exception")){
                    success = true;
                }
                PostProcessCmds(impResult);
            }
            catch (Exception exc)
            {
                string msg = $" Process{interval}AmiFileObject {info}, exception {exc.Message}, {exc.InnerException?.Message}";
                Logger.Fatal(msg, exc, LogModel);
                Debug.WriteLine(msg);
            }
            finally
            {
                if (success && ArchiveBlob)
                {
                    ArchieveBlob(blockBlob, fileName, containerName, Enums.EventType.Ami);
                }
                else
                {
                    Logger.Info($"Ami{interval} not archiving blob success={success} archive flag={ArchiveBlob}", LogModel);
                }
                Logger.Info($"Ami{interval} go archive logs", LogModel);
                ArchiveLogFiles(interval, containerName);
            }

            return success;
        }


        public virtual void ArchiveLogFiles(string interval, string containerName)
        {
            try
            {
                string[] fileNames = { _lastConvertRoot, _lastImportRoot, _lastPurgeRoot, _lastValidateRoot };
                Logger.Info($"Ami{interval} archiving logs {_lastConvertRoot},{_lastImportRoot}, {_lastValidateRoot}", LogModel);
                foreach (string name in fileNames)
                {
                    try
                    {
                        Logger.Info($"Ami{interval} try archiving log {name}", LogModel);
                        if (string.IsNullOrEmpty(name)) continue;
                        string rootPath = Path.GetDirectoryName(name);
                        string fName = Path.GetFileName(name) + "*";
                        Logger.Info($"Ami{interval} archiving log  check {rootPath} for {fName}", LogModel);
                        if (Directory.Exists(rootPath))
                        {
                            IEnumerable<string> fnames = Directory.EnumerateFiles(rootPath, fName);
                            foreach (string fname in fnames)
                            {
                                Logger.Info($"Ami{interval}Min archiving log  file {fname}", LogModel);
                                CloudBlobContainer container = CloudBlobClient.GetContainerReference(containerName);
                                CloudBlockBlob logBlockBlob = container.GetBlockBlobReference($"log/advami/{Path.GetFileName(fname)}");
                                using (var fileStream = File.OpenRead(fname))
                                {
                                    logBlockBlob.UploadFromStream(fileStream);
                                }
                            }
                        }
                        else
                        {
                            Logger.Info($"Ami{interval} archiving log nodirectory {rootPath}", LogModel);
                        }
                    }
                    catch (Exception exc)
                    {
                        string msg = $" Process05MinAmiFileObject ArchiveLogFile '{name}', exception {exc.Message}, {exc.InnerException?.Message}";
                        Logger.Error(msg, exc, LogModel);
                        Debug.WriteLine(msg);
                    }
                }
            }
            catch (Exception exc)
            {
                string msg = $" Process{interval}AmiFileObject ArchiveLogFiles, exception {exc.Message}, {exc.InnerException?.Message}";
                Logger.Error(msg, exc, LogModel);
                Debug.WriteLine(msg);
            }
        }

        /// <summary>
        /// perform post processing on the import based on what was returned from the advanced ami connector import
        /// </summary>
        /// <param name="impResult"></param>
        private static void PostProcessCmds(string impResult)
        {
            string[] cmdStgs = impResult.Split(new[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
            if (cmdStgs.Length > 0)
            {
                int maxCmds = 10;
                foreach (string cmdStg in cmdStgs)
                {
                    if (maxCmds-- <= 0) break;
                    switch (cmdStg)
                    {
                        // deleted old commands for safety reasons
                        //
                        case "nada":
                        {
                            //SuppressErrors until this is filled back in.
                        }
                            break;
                    }
                }
            }
        }

        /// <summary>
        /// Sends events in batch
        /// </summary>
        /// <param name="client">Event Hub client</param>
        /// <param name="eventsList">List of events</param>
        public virtual void SendEventsInBatch(EventHubClient client, List<EventData> eventsList)
        {
            var eventData = eventsList.First();
            var totalSizeOfEvents = eventData.SerializedSizeInBytes * eventsList.Count;
            if (totalSizeOfEvents > 250000) //Events size is greater >  250000 bytes
            {
                var maxEventsCanBeSent = Convert.ToInt32(eventsList.Count * 250000 / totalSizeOfEvents);
                var loopCount = Convert.ToInt32(eventsList.Count / maxEventsCanBeSent);
                var totalEventSent = 0;
                for (var i = 0; i <= loopCount - 1; i++)
                {
                    var events = eventsList.Skip(i * maxEventsCanBeSent).Take(maxEventsCanBeSent).ToList();
                    if (events.Count > 0) client.SendBatch(events);
                    totalEventSent = (i + 1) * maxEventsCanBeSent;
                }
                if (totalEventSent != eventsList.Count)
                {
                    var events = eventsList.Skip(totalEventSent).ToList();
                    if (events.Count > 0) client.SendBatch(events);
                }
            }
            else
            {
                if (eventsList.Count > 0) client.SendBatch(eventsList);
            }
        }

        /// <summary>
        /// Archieves Blob 
        /// </summary>
        /// <param name="blockBlob">Blob</param>
        /// <param name="fileName">Name of file</param>
        /// <param name="containerName">Container Name</param>
        /// <param name="eventType">EventType enum which specifies the event type of file</param>
        public virtual void ArchieveBlob(ICloudBlob blockBlob, string fileName, string containerName,
            Enums.EventType eventType)
        {
            using (var ms = new MemoryStream())
            {
                ms.Position = 0;
                blockBlob.DownloadToStream(ms);
                var container = CloudBlobClient.GetContainerReference(containerName);
                var archieveBlockBlob = container.GetBlockBlobReference($"archive/{fileName}");
                ms.Position = 0;
                archieveBlockBlob.UploadFromStream(ms);
                if (eventType == Enums.EventType.NccAmiReading)
                {
                    var nccAmiConsumptionBlockBlob = container.GetBlockBlobReference($"NccCalcConsumption{fileName}");
                    ms.Position = 0;
                    nccAmiConsumptionBlockBlob.UploadFromStream(ms);
                }
            }
            blockBlob.Delete();
        }

        /// <summary>
        /// Gets the CloudBlockBlob using container name and file name
        /// </summary>
        /// <param name="fileName">Name of the blob</param>
        /// <param name="containerName">Name of the container in which the blob resides</param>
        /// <returns></returns>
        public virtual ICloudBlob GetBlockBlob(string fileName, string containerName)
        {
            var container = CloudBlobClient.GetContainerReference(containerName);
            return container.GetBlockBlobReference(fileName);
        }
       
    }
}