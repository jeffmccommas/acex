﻿using System;
using System.Collections.Generic;
using CE.AO.Models;
using CE.AO.Models.RawFileModels;
using Enums = CE.AO.Utilities.Enums;

namespace CE.AO.DataImport.BatchProcessWorker.Files
{
    public class SubscriptionProcessFile : ProcessFile
    {

        public SubscriptionProcessFile(LogModel logModel) : base(logModel) { }
        public override void Initialize(string fileName, string containerName, string clientId)
        {
            var blockBlob = GetBlockBlob(fileName, containerName);

            var customProperties = new Dictionary<string, object>
            {
                { "EventType",Convert.ToString(Enums.EventType.Subscription)},
                { "ClientId",clientId}
            };

            var client = GetEventHubClient();
            Process<RawSubscriptionModel>(blockBlob, customProperties, Enums.EventType.Subscription, client);
            CloseEventHubClient(client);
            ArchieveBlob(blockBlob, fileName, containerName, Enums.EventType.Subscription);
        }
    }
}
