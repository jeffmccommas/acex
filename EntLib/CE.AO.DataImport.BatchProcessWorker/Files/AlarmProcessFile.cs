﻿using System;
using System.Collections.Generic;
using CE.AO.Models;
using CE.AO.Models.RawFileModels;
using Enums = CE.AO.Utilities.Enums;

namespace CE.AO.DataImport.BatchProcessWorker.Files
{
    /// <summary>
    /// Alarm process File for Batch Process
    /// </summary>
    public class AlarmProcessFile : ProcessFile
    {
        /// <summary>
        /// Parameterized contructor
        /// </summary>
        /// <param name="logModel">Used for logging details in case of any error</param>
        public AlarmProcessFile(LogModel logModel) : base(logModel) { }

        /// <summary>
        /// Initialize processing of file
        /// </summary>
        /// <param name="fileName">filename of file to process</param>
        /// <param name="containerName">blob container name</param>
        /// <param name="clientId">client id</param>
        public override void Initialize(string fileName, string containerName, string clientId)
        {
            var blockBlob = GetBlockBlob(fileName, containerName);

            var customProperties = new Dictionary<string, object>
            {
                { "EventType",Convert.ToString(Enums.EventType.Alarm)},
                { "ClientId",clientId}
            };

            var client = GetEventHubClient();
            Process<RawAlarmModel>(blockBlob, customProperties, Enums.EventType.Alarm, client);
            CloseEventHubClient(client);
            ArchieveBlob(blockBlob, fileName, containerName, Enums.EventType.Alarm);
        }
    }
}
