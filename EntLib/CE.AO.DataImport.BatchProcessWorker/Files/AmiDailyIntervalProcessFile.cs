﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Globalization;
using System.IO;
using AutoMapper;
using CE.AO.Logging;
using CE.AO.Models;
using CE.AO.Models.RawFileModels;
using CsvHelper;
using Microsoft.WindowsAzure.Storage.Blob;
using Newtonsoft.Json;
using Enums = CE.AO.Utilities.Enums;

namespace CE.AO.DataImport.BatchProcessWorker.Files
{
    /// <summary>
    /// To process the daily ami data to eventhub
    /// </summary>
    public class AmiDailyIntervalProcessFile : ProcessFile
    {
        public AmiDailyIntervalProcessFile(LogModel logModel) : base(logModel) { }

        /// <summary>
        /// Create custom properties, process data and then archieve blob
        /// </summary>
        /// <param name="fileName">Name of the file to be processed</param>
        /// <param name="containerName">Blob container name containing the file to be processed</param>
        /// <param name="clientId">Current client id for the file to be processed</param>
        public override void Initialize(string fileName, string containerName, string clientId)
        {
            var blockBlob = GetBlockBlob(fileName, containerName);
            //For daily ami file we need to validate it first- preprocessing, if valid then process file else not.
            if (ValidateFile(blockBlob, clientId))
            {
                var client = GetEventHubClient();
                var customProperties = new Dictionary<string, object>
                {
                    {"EventType", Convert.ToString(Enums.EventType.Ami)},
                    {"IntervalType", Convert.ToInt32(Enums.IntervalType.Daily)},
                    {"ClientId", clientId}
                };
                Process<RawAmiDailyIntervalModel>(blockBlob, customProperties, Enums.EventType.Ami, client);
            }
            ArchieveBlob(blockBlob, fileName, containerName, Enums.EventType.Ami);
        }

        /// <summary>
        /// Read the file from blob and validate each record - preprocessing
        /// </summary>
        /// <param name="cloudBlob">Blob/file to be validated</param>
        /// <param name="clientId">Current client id for the file to be validated</param>
        /// <returns>boolean value</returns>
        private static bool ValidateFile(ICloudBlob cloudBlob, string clientId)
        {
            var logModel = new LogModel
            {
                Module = Enums.Module.FileProcessing,
                ProcessingType = Enums.ProcessingType.File,
                Source = cloudBlob.Name,
                ClientId = clientId
            };
            var stream = cloudBlob.OpenRead();
            var sr = new StreamReader(stream);
            using (var reader = new CsvReader(sr)) {
                reader.Configuration.IsHeaderCaseSensitive = false;
                while (reader.Read())
                {
                    if (!reader.IsRecordEmpty())
                    {
                        var record = reader.GetRecord<RawAmiDailyIntervalModel>();
                        var amiModel = Mapper.Map<RawAmiDailyIntervalModel, AmiDailyIntervalModel>(record);
                        List<ValidationResult> validationResuts;
                        if (Validate(amiModel, clientId, out validationResuts))
                            continue;                                           //If record is valid then continue with next record
                        var errorString = string.Empty;
                        if (validationResuts.Count > 0)
                            errorString = $"Error: {JsonConvert.SerializeObject(validationResuts)}.";
                        Logger.Fatal($@"Event of type ""AMI - Daily"" is not valid. {errorString}", logModel);
                        return false;
                    }
                }
            }
            return true;
        }

        /// <summary>
        /// Validate the data in the AmiDailyIntervalModel
        /// </summary>
        /// <param name="model">The AmiDailyIntervalModel to validate</param>
        /// <param name="clientId">The clientId to be validated</param>
        /// <param name="validationResuts">The validation results messages</param>
        /// <returns>True, if valid.</returns>
        private static bool Validate(AmiDailyIntervalModel model, string clientId, out List<ValidationResult> validationResuts)
        {
            if (!Models.Utilities.Validate(model, out validationResuts))
            {
                return false;
            }
            // Check the ClientId
            if (model.ClientId != Convert.ToInt32(clientId))
            {
                validationResuts.Add(new ValidationResult("ClientId is not valid", new List<string> { "ClientId" }));
                return false;
            }
            // Check the CommodityType
            if (!Enum.IsDefined(typeof(Enums.CommodityType), model.CommodityId))
            {
                validationResuts.Add(new ValidationResult("CommodityId is not valid.", new List<string> { "CommodityId" }));
                return false;
            }
            // Check the UOM
            if (!Enum.IsDefined(typeof(Enums.UomType), model.UOMId))
            {
                validationResuts.Add(new ValidationResult("UOMId is not valid.", new List<string> { "UOMId" }));
                return false;
            }
            // Check the TOU
            if (!Enum.IsDefined(typeof(Enums.TOUType), model.TOUID))
            {
                validationResuts.Add(new ValidationResult("TOUID is not valid.", new List<string> { "TOUID" }));
                return false;
            }
            DateTime amiDateValue;
            string[] formats = { "M-d-yyyy", "M-dd-yyyy", "MM-dd-yyyy", "M/d/yyyy", "M/dd/yyyy", "MM/dd/yyyy"};
            if (!DateTime.TryParseExact(model.AmiTimeStamp.ToShortDateString(), formats, CultureInfo.InvariantCulture, DateTimeStyles.None, out amiDateValue))
            {
                validationResuts.Add(new ValidationResult($"{model.AmiTimeStamp} is not a valid date.", new List<string> { "AmiTimeStamp" }));
                return false;
            }
            // Check the Direction for UIL only
            if (model.ClientId == 224 && model.Direction != null)
            {
                validationResuts.Add(new ValidationResult("Direction must be blank.", new List<string> { "Direction" }));
                return false;
            }
            // If all checks pass return true.
            return true;
        }
    }
}
