﻿using System;
using System.Collections.Generic;
using CE.AO.Models;
using CE.AO.Models.RawFileModels;
using Enums = CE.AO.Utilities.Enums;

namespace CE.AO.DataImport.BatchProcessWorker.Files
{
    public class NccAmiConsumptionProcessFile : ReadsProcessFile
    {
        public NccAmiConsumptionProcessFile(LogModel logModel) : base(logModel) { }
        public override void Initialize(string fileName, string containerName, string clientId)
        {
            var blockBlob = GetBlockBlob(fileName, containerName);

            var customProperties = new Dictionary<string, object>
            {
                {"EventType", Convert.ToString(Enums.EventType.NccAmiConsumption)},
                {"ClientId", clientId}
            };

            var client = GetEventHubClient();
            ProcessFile<RawNccAmiReadingModel>(blockBlob, customProperties, Enums.EventType.NccAmiConsumption, client);
            CloseEventHubClient(client);
            ArchieveBlob(blockBlob, fileName, containerName, Enums.EventType.NccAmiConsumption);
        }
    }
}
