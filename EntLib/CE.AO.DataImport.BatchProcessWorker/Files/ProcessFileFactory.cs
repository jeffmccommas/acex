﻿using AO.TimeSeriesProcessing;
using CE.AO.Logging;
using CE.AO.Models;
using Microsoft.WindowsAzure.Storage.Blob;
using Enums = CE.AO.Utilities.Enums;

namespace CE.AO.DataImport.BatchProcessWorker.Files
{
    /// <summary>
    /// Factory class for processing files
    /// </summary>
    internal class ProcessFileFactory
    {
        /// <summary>
        /// Gets instance of process file depending on the filename
        /// modified to pass additional params so that the new AMI could be initialized (here we have a single entrypoint 
        /// and didn't want to refactor the 'Initialize' method for all the other classes).
        /// </summary>
        /// <param name="fileName">Name of the file</param>
        /// <param name="logModel">Used for logging details in case of any error</param>
        /// <param name="msgParts"></param>
        /// <param name="containerName"></param>
        /// <param name="cloudBlobClient"></param>
        /// <returns>Instance of ProcessFile</returns>
        public static ProcessFile GetProcessFileObject(string fileName, LogModel logModel, string[] msgParts, string containerName = null, CloudBlobClient cloudBlobClient = null) {
            Logger.Info($"Queue message GetProcessFileObject fn:{fileName}, cn:{containerName} cbc:{cloudBlobClient?.BaseUri}.", logModel);
            ProcessFile processFileObject = null;
            logModel.Module = Enums.Module.FileProcessing;
            ProcessingArgs pa = new ProcessingArgs();
            string vsrc = pa.GetControlVal("reqVsrc", "evc");
            ProcessingArgs.AccumulateRequestPars(vsrc, pa["aclenv"], "none", pa);

            var fileNameToLower = fileName.ToLower();
            if (fileNameToLower.Contains("ami")) {
                string processFile = pa.GetClientControlVal(logModel.ClientId, "dataProcessFileMaps", "");
                if (processFile.ToLower().Contains("advami")) {
                    logModel.Module = Enums.Module.AMIBatchProcess;
                    processFileObject = new AdvanceAmiProcessFile(logModel, fileName, msgParts, containerName);
                }
                else if (fileNameToLower.Contains("15min")) {
                    logModel.Module = Enums.Module.AMIBatchProcess;
                    processFileObject = new Ami15MinuteIntervalProcessFile(logModel);
                }
                else if (fileNameToLower.Contains("30min"))
                {
                    logModel.Module = Enums.Module.AMIBatchProcess;
                    processFileObject = new Ami30MinuteIntervalProcessFile(logModel);
                }
                else if (fileNameToLower.Contains("60min"))
                {
                    logModel.Module = Enums.Module.AMIBatchProcess;
                    processFileObject = new Ami60MinuteIntervalProcessFile(logModel);
                }
                else if (fileNameToLower.Contains("daily"))
                {
                    logModel.Module = Enums.Module.AMIBatchProcess;
                    processFileObject = new AmiDailyIntervalProcessFile(logModel);
                }
            }
            else if (fileNameToLower.Contains("bill"))
            {
                logModel.Module = Enums.Module.BillingBatchProcess;
                processFileObject = new BillingProcessFile(logModel);
            }
            else if (fileNameToLower.Contains("accountupdates"))
            {
                logModel.Module = Enums.Module.AccountUpdatesBatchProcess;
                processFileObject = new AccountUpdatesProcessFile(logModel);
            }
            else if (fileNameToLower.Contains("subscription"))
            {
                logModel.Module = Enums.Module.SubscriptionBatchProcess;
                processFileObject = new SubscriptionProcessFile(logModel);
            }
            else if (fileNameToLower.Contains("ncccalcconsumption"))
            {
                logModel.Module = Enums.Module.NccConsumptionBatchProcess;
                processFileObject = new NccAmiConsumptionProcessFile(logModel);
            }
            else if (fileNameToLower.Contains("ncc"))
            {
                logModel.Module = Enums.Module.NccBatchProcess;
                processFileObject = new NccAmiReadingProcessFile(logModel);
            }
            else if (fileNameToLower.Contains("meteraccountmtu"))
            {
                logModel.Module = Enums.Module.MeterAccountMtuBatchProcess;
                processFileObject = new MeterAccountMtuProcessFile(logModel);
            }
            else if (fileNameToLower.Contains("meterread"))
            {
                logModel.Module = Enums.Module.MeterReadBatchProcess;
                processFileObject = new MeterReadProcessFile(logModel);
            }
            else if (fileNameToLower.Contains("dcudata"))
            {
                logModel.Module = Enums.Module.DcuProvisioningBatchProcess;
                processFileObject = new DcuProvisioningProcessFile(logModel);
            }
            else if (fileNameToLower.Contains("dcualarm"))
            {
                logModel.Module = Enums.Module.DcuAlarmBatchProcess;
                processFileObject = new DcuAlarmProcessFile(logModel);
            }
            else if (fileNameToLower.Contains("dcucalldata"))
            {
                logModel.Module = Enums.Module.DcuCallDataBatchProcess;
                processFileObject = new DcuCallDataProcessFile(logModel);
            }
            // This "alarm" condition should allways be placed at end or after "dcualarm" so that it will not match "dcualarm" files. 
            else if (fileNameToLower.Contains("alarm"))
            {
                logModel.Module = Enums.Module.AlarmBatchProcess;
                processFileObject = new AlarmProcessFile(logModel);
            }

            return processFileObject;
        }

    }
}
