﻿using System;
using System.Collections.Generic;
using CE.AO.Models;
using CE.AO.Models.RawFileModels;
using Enums = CE.AO.Utilities.Enums;

namespace CE.AO.DataImport.BatchProcessWorker.Files
{
    /// <summary>
    /// Class for processing MeterRead file
    /// </summary>
    public class MeterReadProcessFile : ReadsProcessFile
    {
        /// <summary>
        /// Parameterized contructor
        /// </summary>
        /// <param name="logModel">Used for logging details in case of any error</param>
        public MeterReadProcessFile(LogModel logModel) : base(logModel) { }

        /// <summary>
        /// Initialize processing of file
        /// </summary>
        /// <param name="fileName">filename of file to process</param>
        /// <param name="containerName">blob container name</param>
        /// <param name="clientId">client id</param>
        public override void Initialize(string fileName, string containerName, string clientId)
        {
            var blockBlob = GetBlockBlob(fileName, containerName);

            var customProperties = new Dictionary<string, object>
            {
                { "EventType",Convert.ToString(Enums.EventType.MeterRead)},
                { "ClientId",clientId}
            };

            var client = GetEventHubClient();
            ProcessFile<RawMeterReadModel>(blockBlob, customProperties, Enums.EventType.MeterRead, client);
            CloseEventHubClient(client);
            ArchieveBlob(blockBlob, fileName, containerName, Enums.EventType.MeterRead);
        }
    }
}
