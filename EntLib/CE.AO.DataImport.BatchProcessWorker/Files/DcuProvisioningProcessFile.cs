﻿using System;
using System.Collections.Generic;
using CE.AO.Models;
using CE.AO.Models.RawFileModels;
using Enums = CE.AO.Utilities.Enums;

namespace CE.AO.DataImport.BatchProcessWorker.Files
{
    /// <summary>
    /// Class for processing DcuProvisioning file
    /// </summary>
    public class DcuProvisioningProcessFile : ProcessFile
    {
        /// <summary>
        /// Parameterized contructor
        /// </summary>
        /// <param name="logModel">Used for logging details in case of any error</param>
        public DcuProvisioningProcessFile(LogModel logModel) : base(logModel) { }

        /// <summary>
        /// Initialize processing of file
        /// </summary>
        /// <param name="fileName">filename of file to process</param>
        /// <param name="containerName">blob container name</param>
        /// <param name="clientId">client id</param>
        public override void Initialize(string fileName, string containerName, string clientId)
        {
            var blockBlob = GetBlockBlob(fileName, containerName);

            var customProperties = new Dictionary<string, object>
            {
                { "EventType",Convert.ToString(Enums.EventType.DcuProvisioningData)},
                { "ClientId",clientId}
            };

            var client = GetEventHubClient();
            Process<RawDcuProvisioningModel>(blockBlob, customProperties, Enums.EventType.DcuProvisioningData, client);
            CloseEventHubClient(client);
            ArchieveBlob(blockBlob, fileName, containerName, Enums.EventType.DcuProvisioningData);
        }
    }
}
