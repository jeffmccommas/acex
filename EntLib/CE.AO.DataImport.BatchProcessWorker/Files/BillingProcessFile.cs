﻿using System;
using System.Collections.Generic;
using CE.AO.Models;
using CE.AO.Models.RawFileModels;
using Enums = CE.AO.Utilities.Enums;

namespace CE.AO.DataImport.BatchProcessWorker.Files
{
    public class BillingProcessFile : ProcessFile
    {

        public BillingProcessFile(LogModel logModel) : base(logModel) { }
        public override void Initialize(string fileName, string containerName, string clientId)
        {
            var blockBlob = GetBlockBlob(fileName, containerName);

            var customProperties = new Dictionary<string, object>
            {
                { "EventType",Convert.ToString(Enums.EventType.Bill)},
                { "ClientId",clientId}
            };

            var client = GetEventHubClient();
            Process<RawBillingModel>(blockBlob, customProperties, Enums.EventType.Bill, client);
            CloseEventHubClient(client);
            ArchieveBlob(blockBlob, fileName, containerName, Enums.EventType.Bill);
        }
    }
}
