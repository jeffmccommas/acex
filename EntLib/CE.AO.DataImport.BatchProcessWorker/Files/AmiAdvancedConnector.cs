﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using AO.TimeSeriesProcessing;
using Cassandra;
using CE.AO.Models;
using Microsoft.Azure;
using Logger = CE.AO.Logging.Logger;


namespace CE.AO.DataImport.BatchProcessWorker.Files {
    public class AmiAdvancedConnector {
        /// <summary>
        /// Import a file
        /// </summary>
        /// <param name="filePathName"></param>
        /// <param name="interval"></param>
        /// <param name="logModel"></param>
        /// <param name="firstFewLines"></param>
        /// <param name="impContent">if not null, use this as imput source instead of file</param>
        /// <returns>true if success</returns>
        public string ImportFile(string filePathName, string interval, LogModel logModel, List<string> firstFewLines, string impContent = null) {
            string rVal = "";
            try {
                Logger.Info($" AmiAdvancedConnector.ImportFile rx {filePathName} for {interval} impContent length={impContent?.Length}", logModel);
                using (AdvancedAmiImport amiImport = new AdvancedAmiImport()) {
                    rVal = amiImport.ImportAmiDataValidate(interval, filePathName, impContent, firstFewLines, logModel);
                    LastConvertRoot = amiImport.LastConvertRoot;
                    LastImportRoot = amiImport.LastImportRoot;
                    LastPurgeRoot = amiImport.LastPurgeRoot;
                    LastValidateRoot = amiImport.LastValidateRoot;
                }
            }
            catch (Exception exc) {
                string msg = $"Exception AmiAdvancedConnector.ImportFile {filePathName},  exception {exc.Message}, {exc.InnerException?.Message}, {exc.StackTrace}";
                Logger.Fatal(msg, exc, logModel);
                Debug.WriteLine(msg);
            }
            return rVal;
        }

        public string LastConvertRoot { get; set; } = "";
        public string LastImportRoot { get; set; } = "";
        public string LastPurgeRoot { get; set; } = "";
        public string LastValidateRoot { get; set; } = "";


    }

    class AdvancedAmiImport : IDisposable {

        // some data for testing functionality
        private static string[] testList = {
            "oper=tseries clientId=87 procLimit=50  doAsyncBatch=1 aclenv=work collid=collectionStarterId dtStt=1/1/2017 dtstp=1/4/2017",
            "oper=tseries clientId=87 procLimit=50  doAsyncBatch=1 aclenv=work collid=36023832 dtStt=2/23/2017 dtstp=2/25/2017",
            "oper=tseries clientId=87 procLimit=50  doAsyncBatch=1 aclenv=work collid=36023832 dtStt=1/23/2017 dtstp=3/25/2017"
        };
        private static string cassImp1 = "c,s,v\n10000000,201701,2017-01-01 00:00:00Z,0,0,(*( 'estDesc':'D2NO' (*)'estInd':'Yes' )*)";
        private static string cassImp2 = "c,s,v\n10000000,201701,2017-01-01 00:00:00Z,50,0,(*( 'estDesc':'D2NO' (*)'estInd':'No' )*)";

        /// <summary>
        /// Recall the test seed in cassandra
        /// </summary>
        /// <param name="pa"></param>
        /// <param name="cc1"></param>
        /// <returns></returns>
        private RowSet RecallCassandraDataTest(ProcessingArgs pa, CassConn1 cc1) {
            pa["qRows"] = "*";
            pa["reqVsrc"] = "value_series_meta";
            pa["maxrows"] = "2";
            pa["allowFilter"] = "";
            string selStg = "where id = 10000000 and did = 201701";
            pa["qSel"] = selStg;
            RowSet rs = cc1.PerformCasQuery(pa, ref selStg);
            rs.GetAvailableWithoutFetching();
            return rs;
        }

        /// <summary>
        /// validate that complete path for importing ami series date exists
        /// </summary>
        public string ImportAmiDataValidate(string interval, string filename, string impContent, List<string> firstFewLines, LogModel logModel) {
            Logger.Info($" AmiAdvancedConnector.ImportAmiDataValidate impContent length={impContent?.Length} ffcount={firstFewLines?.Count}", logModel);
            string rVal = "";
            ProcessingArgs pa = new ProcessingArgs();
            string vsrc = pa.GetControlVal("reqVsrc", "evc");
            ProcessingArgs.AccumulateRequestPars(vsrc, pa["aclenv"], "none", pa);
            bool isImported = false;


            if (impContent != null && !impContent.StartsWith("FunkyTestFileForImportBatch")) {

                Logger.Info($" 2 insert AmiAdvancedConnector.ImportAmiDataValidate ", logModel);

                if ((firstFewLines != null) && (firstFewLines.Count > 0)) {
                    // make sure this is coming through the new import process
                    string[] someLines = firstFewLines.ToArray();
                    bool alreadyProcessed = false;
                    foreach (string stg in firstFewLines) {
                        if (stg.IndexOf(DataUtils1.CsvFormatEscapeOpen, StringComparison.InvariantCultureIgnoreCase) >= 0) {
                            alreadyProcessed = true;
                            break;
                        }
                    }

                    if (alreadyProcessed) {
                        // data has been pretranslated/scrubbed and there is no header line
                        Logger.Info(
                            $" 2.0.0  AmiAdvancedConnector.ImportAmiDataValidate : for\n{someLines.Aggregate((a, b) => b + "\n" + b)}",
                            logModel);
                        rVal = InsertCassandraDataTest(pa, filename, impContent, logModel);
                        isImported = true;
                    }
                    else {
                        try {
                            Logger.Info(
                                $" 2.0.1 should follow with 2.0.2, 2.0.3 AmiAdvancedConnector.ImportAmiDataValidate : for\n{someLines.Aggregate((a, b) => b + "\n" + b)}",
                                logModel);
                            impContent = ConvertCassandraDataTest(pa, filename, interval, impContent, someLines, logModel);
                            if (impContent == "retContent" || impContent == string.Empty) {
                                rVal = $" 2.0.2  AmiAdvancedConnector.ImportAmiDataValidate : Error Converting data filename = {filename}";
                                Logger.Error(rVal, logModel);
                                rVal = $"Exception {rVal}";
                            }
                            else {
                                Logger.Info($" 2.0.2  AmiAdvancedConnector.ImportAmiDataValidate : datlen = {impContent.Length}  ", logModel);
                                rVal = InsertCassandraDataTest(pa, filename, impContent, logModel);
                                isImported = true;
                            }
                            Logger.Info($" 2.0.3  AmiAdvancedConnector.ImportAmiDataValidate : ", logModel);
                        }
                        catch (Exception exc) {
                            Logger.Error($" ** 2.0.4  AmiAdvancedConnector.ImportAmiDataValidate Exception {exc.Message}, {exc.InnerException?.Message}", logModel);
                        }
                    }
                    // auto purge logs with configured age (command generated here due to older safety measure)
                    string purgeVal = PurgeLogs(pa, "",logModel);
                    rVal += purgeVal + "; ";

                    // delay validate, QA purpose
                    if (filename.Contains("valdelay1")) {
                        Logger.Info($" 3.0.0  AmiAdvancedConnector.ImportAmiDataValidate :  sleep 60", logModel);
                        Thread.Sleep(60000);
                    }
                    else if (filename.Contains("valdelay5")) {
                        Logger.Info($" 3.0.0  AmiAdvancedConnector.ImportAmiDataValidate :  sleep 300", logModel);
                        Thread.Sleep(300000);
                    }

                    // validate import if there's an import
                    if (isImported) {
                        Logger.Info($" 3.0.1  AmiAdvancedConnector.ImportAmiDataValidate :  requesting validate", logModel);
                        string validateVal = ValidateCassandraImportedData(pa, filename, impContent, logModel);
                        rVal += validateVal + "; ";
                    }
                    else {
                        Logger.Warn($" 3.0.1  AmiAdvancedConnector.ImportAmiDataValidate :  no validate, isImp={isImported}, impfrag=for\n{someLines.Aggregate((a, b) => b + "\n" + b)} \n or fname={filename}", logModel);
                    }
                }
                else {
                    Logger.Error($" 9 insert AmiAdvancedConnector.ImportAmiDataValidate doesn't have information for determining what to do ", logModel);
                }
            }
            else {  // this is for some detailed functional analysis and/or control operations
                
                if(impContent == null)
                    throw new Exception("AdvancedAmiImport.ImportAmiDataValidate: impContent string is null");

                string[] testDataStgs = impContent.Split(new[] { '\n' }, StringSplitOptions.RemoveEmptyEntries);
                Logger.Info($" 1  AmiAdvancedConnector.ImportAmiDataValidate ", logModel);
                CassConn1 cc1 = new CassConn1();
                Logger.Info($" 2  AmiAdvancedConnector.ImportAmiDataValidate ", logModel);
                pa.Override(testList[0]);
                //ProcessingArgs pa = new ProcessingArgs(testList[0].Split(' '));
                Logger.Info($" 3  AmiAdvancedConnector.ImportAmiDataValidate ", logModel);
                DateTime dtStt = DateTime.Now;
                Console.WriteLine($"Starting at {dtStt}");
                if (pa["sqlMeterMgtCxnStg"] == null)
                {
                    pa["sqlMeterMgtCxnStg"] = CloudConfigurationManager.GetSetting("sqlMeterMgt");
                }
                Logger.Info($" 4  AmiAdvancedConnector.ImportAmiDataValidate ", logModel);
                ContextData.InitContextStrings("evc", pa["aclenv"], "none", pa, null);
                Logger.Info($" 5  AmiAdvancedConnector.ImportAmiDataValidate ", logModel);
                ProcessingArgs.AccumulateRequestPars(null, "evc", pa["aclenv"], "none", pa);

                Logger.Info($" 6  AmiAdvancedConnector.ImportAmiDataValidate ", logModel);
                cc1.Connect(pa);
                Logger.Info($" 7  AmiAdvancedConnector.ImportAmiDataValidate ", logModel);
                RowSet rs;
                bool done = false;
                for (int i = 0; i < testDataStgs.Length; i++)
                {
                    if (done)
                    {
                        break;
                    }
                    string testDataStg = testDataStgs[i];
                    if (!string.IsNullOrEmpty(testDataStg))
                    {
                        switch (testDataStg[0])
                        {
                            case '#':
                            {  // it is a control commad.  Maybe purge logs, etc.
                                rVal = testDataStg.Trim('#');
                                string[] cmdStgs = rVal.Split(new[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
                                if (cmdStgs.Length > 0)
                                {
                                    foreach (string cmdStg in cmdStgs)
                                    {
                                        switch (cmdStg)
                                        {
                                            case "purge":
                                            {
                                                // a purge operation
                                                int operIdx = testDataStg.IndexOf(' ');
                                                if (operIdx > 0)
                                                {
                                                    string operStg = testDataStg.Substring(operIdx).Trim();
                                                    Logger.Info($" 8  AmiAdvancedConnector.ImportAmiDataValidate {cmdStg}", logModel);
                                                    PurgeLogs(pa, operStg, logModel);
                                                }
                                            }
                                                break;
                                        }
                                    }
                                }
                            }
                                break;
                            case '1':
                            {
                                InsertCassandraDataTest(pa, filename, cassImp1, logModel);
                                Logger.Info($" 8  AmiAdvancedConnector.ImportAmiDataValidate ", logModel);
                                rs = RecallCassandraDataTest(pa, cc1);
                                Logger.Info($" 9  AmiAdvancedConnector.ImportAmiDataValidate ", logModel);
                                // now validate content
                                foreach (Row row in rs)
                                {
                                    double val = (double)row["val"];
                                    if (!val.Equals(0)) throw new Exception($" ImportAmiDataValidate {val} != {0} ");
                                    IDictionary<string, string> map = (IDictionary<string, string>)row["meta"];
                                    if (!map["estInd"].Equals("Yes"))
                                    {
                                        Logger.Error($" 9.1  AmiAdvancedConnector.ImportAmiDataValidate fail: {map["estInd"]} != {"Yes"}", logModel);
                                        throw new Exception($" ImportAmiDataValidate {map["estInd"]} != {"Yes"} ");
                                    }
                                }
                            }
                                break;
                            case '2':
                            {
                                InsertCassandraDataTest(pa, filename, cassImp2, logModel);
                                rs = RecallCassandraDataTest(pa, cc1);
                                // now validate content
                                foreach (Row row in rs)
                                {
                                    double val = (double)row["val"];
                                    if (!val.Equals(50))
                                    {
                                        Logger.Error($" 9.2  AmiAdvancedConnector.ImportAmiDataValidate fail: {val} != {50}", logModel);
                                        throw new Exception($" ImportAmiDataValidate {val} != {50} ");
                                    }
                                    IDictionary<string, string> map = (IDictionary<string, string>)row["meta"];
                                    if (!map["estInd"].Equals("No"))
                                        throw new Exception($" ImportAmiDataValidate {map["estInd"]} != {"No"} ");
                                }
                            }
                                break;
                            case '0':
                            {
                                done = true;
                                StringBuilder sb = new StringBuilder();
                                for (++i; i < testDataStgs.Length; i++)
                                {
                                    sb.Append(testDataStgs[i] + "\n");
                                }
                                string dataBody = sb.ToString().Trim();
                                Logger.Info($" 9.0  AmiAdvancedConnector.ImportAmiDataValidate : for\n{dataBody}", logModel);
                                InsertCassandraDataTest(pa, filename, dataBody, logModel);
                                rs = RecallCassandraDataTest(pa, cc1);
                                Logger.Info($" 9.0.1  AmiAdvancedConnector.ImportAmiDataValidate : rs count = {rs.GetAvailableWithoutFetching()}  ", logModel);
                            }
                                break;
                            case '8':
                            {
                                done = true;
                                StringBuilder sb = new StringBuilder();
                                for (++i; i < testDataStgs.Length; i++)
                                {
                                    sb.Append(testDataStgs[i] + "\n");
                                }
                                string[] someLines = { "" };
                                string dataBody = sb.ToString().Trim();
                                Logger.Info($" 8.0  AmiAdvancedConnector.ImportAmiDataValidate : for\n{dataBody}", logModel);
                                string cvtData = ConvertCassandraDataTest(pa, filename, interval, dataBody, someLines, logModel);
                                Logger.Info($" 8.0.1  AmiAdvancedConnector.ImportAmiDataValidate : datlen = {cvtData.Length}  ", logModel);
                                InsertCassandraDataTest(pa, filename, cvtData, logModel);
                                Logger.Info($" 8.0.1  AmiAdvancedConnector.ImportAmiDataValidate : ", logModel);
                            }
                                break;
                            case '9':
                            {
                                try
                                {
                                    Logger.Info($" 9.9  AmiAdvancedConnector.ImportAmiDataValidate :", logModel);
                                    string path = Path.GetTempPath();
                                    string fileNamePath = Path.Combine(path, "AdvAmiTestFile.txt");
                                    Logger.Info($" 9.9.1  AmiAdvancedConnector.ImportAmiDataValidate : '{fileNamePath}'", logModel);
                                    if (File.Exists(fileNamePath))
                                    {
                                        File.Delete(fileNamePath);
                                    }
                                    using (StreamWriter sw = new StreamWriter(fileNamePath))
                                    {
                                        sw.WriteLine($" AmiAdvancedConnector.ImportAmiDataValidate test file path '{fileNamePath}'  '{DateTime.Now}'");
                                    }
                                    Logger.Info($" 9.9.2  AmiAdvancedConnector.ImportAmiDataValidate : '{fileNamePath}'", logModel);
                                }
                                catch (Exception exc)
                                {
                                    Logger.Error($" 9.9.3  AmiAdvancedConnector.ImportAmiDataValidate : {exc.Message}, {exc.InnerException?.Message}", logModel);
                                }
                                done = true;
                                StringBuilder sb = new StringBuilder();
                                for (++i; i < testDataStgs.Length; i++)
                                {
                                    if (!string.IsNullOrEmpty(testDataStgs[i]))
                                    { // ignore blanks
                                        if (!testDataStgs[i].StartsWith("##!!"))
                                        {  // allow comment line -- watch this as assumes '##!!' is not normally encountered here
                                            sb.Append(testDataStgs[i] + "\n");
                                        }
                                    }
                                }
                                string dataBody = sb.ToString().Trim();
                                Logger.Info($" 9.0  AmiAdvancedConnector.ImportAmiDataValidate : for\n{dataBody}", logModel);
                                InsertCassandraDataTest(pa, filename, dataBody, logModel);
                                rs = RecallCassandraDataTest(pa, cc1);
                                Logger.Info($" 9.0.1  AmiAdvancedConnector.ImportAmiDataValidate : rs count = {rs.GetAvailableWithoutFetching()}  ", logModel);
                            }
                                break;
                        }
                    }
                }

            }
            return rVal;
        }


        /// <summary>
        /// purge logs.
        /// Accepts command string because 
        /// </summary>
        /// <param name="pa"></param>
        /// <param name="cmdStg"></param>
        /// <param name="logModel"></param>
        public string PurgeLogs(ProcessingArgs pa, string cmdStg, LogModel logModel) {
            //string commandStg = $"oper=purgelogweek purgeShowOnly=justshow purgedirectory=getcurrentlog aclenv=work";
            if (!string.IsNullOrEmpty(cmdStg)) pa.Override(cmdStg);
            string purgeDir = pa.GetControlVal("purgeDirectory","getcurrentlog");
            string purgeshow = pa.GetControlVal("purgeDirectory", "zzjustshow");  // just show means it will not purge just inform of what could be
            string retdatapa = pa.GetControlVal("purgeDirectory", "yes");
            string disablelog = pa.GetControlVal("purgeDirectory", "no");
            int  purgeLogAgeDays = pa.GetControlVal("purgeDirectory", 14);

            string reqVsrc = pa.GetClientControlVal(logModel.ClientId, "consumptionTableName", "");
            
            string purgeCmd =
                $"oper=purgelogday clientId={logModel.ClientId} reqVsrc={reqVsrc} purgeDirectory={purgeDir} purgeShowOnly={purgeshow} retdatapa={retdatapa} disablelog={disablelog} purgeLogAgeDays={purgeLogAgeDays}";

            Logger.Info($" purge 1 AmiAdvancedConnector.PurgeLogs impContent use '{purgeCmd}' for '{cmdStg}'", logModel);
            string[] rVals = ServicesBroker.TestProcess(purgeCmd.Split(new [] {' '},StringSplitOptions.RemoveEmptyEntries));
            LastPurgeRoot = rVals[0];
            string sval = "";
            foreach (string rVal in rVals) {
                sval += rVal + "; ";
            }
            if (sval.ToLower().Contains("exception")) {
                Logger.Error($" purge 2 AmiAdvancedConnector.PurgeLogs impContent rval={sval}", logModel);
            }
            else {
                Logger.Info($" purge 2 AmiAdvancedConnector.PurgeLogs impContent rval={sval}", logModel);
            }

            return sval;
        }

        /// <summary>
        /// validate the imported data
        /// </summary>
        /// <param name="pa"></param>
        /// <param name="filename"></param>
        /// <param name="dat"></param>
        /// <param name="logModel"></param>
        /// <returns></returns>
        private string ValidateCassandraImportedData(ProcessingArgs pa, string filename, string dat, LogModel logModel) {
            string reqVsrc = pa.GetClientControlVal(logModel.ClientId, "consumptionTableName", "");

            // save the file locally
            string dir = Path.GetDirectoryName(LastImportRoot);
            if (!string.IsNullOrEmpty(filename)) {
                var filePathInfo = filename.Split(new char[]{'\\','/'});
                filename = filePathInfo[filePathInfo.GetUpperBound(0)];
            }
            filename = Path.Combine(dir, filename);
            using (StreamWriter file = new StreamWriter(filename)) {
                file.Write(dat);
            }
            // figure out the max number of rows to validate in a file and how many to skip.
            long skipRows = 0;
            long fileLength = new FileInfo(filename).Length;
            long approxRows = fileLength / 70;
            long procLimit = pa.GetControlVal("procLimit", 2000000);
            long maxValRows = pa.GetControlVal("maxValRows", 5000);
            if (maxValRows > 0) {
                if (approxRows < 1500) {
                    skipRows = 0;
                }
                else {
                    skipRows = approxRows / maxValRows;
                }
            }
            string heOper0 = "{**}";
            string cmdLine = $"oper=validator clientId={logModel.ClientId} procLimit={procLimit} origSize={fileLength} approxRows={approxRows} skipRows={skipRows} useRefFile={filename} importfile={filename} maxFiles=20 justListData=no skipFiles=0 heOper0={heOper0} reqVsrc={reqVsrc}";
            Logger.Info($" val 1 AmiAdvancedConnector.ValidateCassandraImportedData impContent cmd='{cmdLine}'", logModel);

            string[] rVals = ServicesBroker.TestProcess(cmdLine.Split(' '), dat);
            LastValidateRoot = rVals[1];
            string sval = "";
            foreach (string rVal in rVals) {
                sval += rVal + "; ";
            }
            if (sval.ToLower().Contains("exception") || sval.ToLower().Contains("nofix=0") || !sval.ToLower().Contains("autofix=0")) {
                Logger.Error($" val 2 AmiAdvancedConnector.ValidateCassandraImportedData end rval={sval}", logModel);
            }
            else {
                Logger.Info($" val 2 AmiAdvancedConnector.ValidateCassandraImportedData end rval={sval}", logModel);
            }

            return sval;
        }

        private string InsertCassandraDataTest(ProcessingArgs pa, string filename, string dat, LogModel logModel) {
            //string stg = $"oper=cassimport procLimit=5000000 importfile={tmpFile} doAsyncBatch=1 aclenv=work";
            

            string altQueryPath = pa.GetClientControlVal(logModel.ClientId, "consumptionQuery", "");
            string reqVsrc = pa.GetClientControlVal(logModel.ClientId, "consumptionTableName", "");
            
            Logger.Info($" ins 1 AmiAdvancedConnector.InsertCassandraDataTest impContent length={dat?.Length}", logModel);
            string stg = $"oper=cassimport clientId={logModel.ClientId} procLimit=5000000 importLength={dat?.Length} doAsyncBatch=1 aclenv=work reqVsrc={reqVsrc} importfile={filename} doFilterIngestion=0";
            if (!string.IsNullOrEmpty(altQueryPath)) {
                stg = stg + $" altQueryPath={altQueryPath}";
            }
            string[] rVals = ServicesBroker.TestProcess(stg.Split(' '),dat);
            LastImportRoot = rVals[0];
            string sval = "";
            foreach (string rVal in rVals) {
                sval += rVal + "; ";
            }

            if (sval.ToLower().Contains("exception")) {
                Logger.Error($" ins 2 AmiAdvancedConnector.InsertCassandraDataTest impContent rval={sval}", logModel);
            }
            else {
                Logger.Info($" ins 2 AmiAdvancedConnector.InsertCassandraDataTest impContent rval={sval}", logModel);
            }

            return sval;
        }

        private string ConvertCassandraDataTest(ProcessingArgs pa, string filename, string interval, string dat, string[] firstFewLines, LogModel logModel) {
            //string stg = $"oper=cassimport procLimit=5000000 importfile={tmpFile} doAsyncBatch=1 aclenv=work";
            int hasHeaders = 0;
            if ((firstFewLines != null) && (firstFewLines.Length > 0) && !string.IsNullOrEmpty(firstFewLines[0])) {
                if (!char.IsDigit(firstFewLines[0][0])) {  // assume there is a header line
                    hasHeaders = 1;
                }
            }

            string oper = pa.GetClientControlVal(logModel.ClientId, "preprocessCmd", "");
            string reqVsrc = pa.GetClientControlVal(logModel.ClientId, "consumptionTableName", "");

            if (logModel.ClientId == "174" && interval.ToLower() == "daily")
                return dat;
           
            //string stg = $"oper=cvtCsv2Tall clientId={logModel.ClientId} procLimit=5000000 importLength={dat?.Length} doAsyncBatch=1 aclenv=work retContent=retContent  reqVsrc=consumption_by_meter_account_direction";
            Logger.Info($" cvt 1 AmiAdvancedConnector.ConvertCassandraDataTest impContent length={dat?.Length} hasheaders={hasHeaders}" , logModel);
            string stg = $"oper={oper} clientId={logModel.ClientId} importfile={filename} procLimit=5000000 importLength={dat?.Length} doAsyncBatch=1 aclenv=work reqVsrc={reqVsrc}";
            string retData = "retContent";
            string[] rVals = ServicesBroker.ImportProcess(stg.Split(' '), ref retData, dat);
            LastConvertRoot = rVals[0];
            string sval = "";
            foreach (string rVal in rVals) {
                sval += rVal + "; ";
            }
            if (sval.ToLower().Contains("exception") || sval.ToLower().Contains("errors=")) {
                Logger.Error($" cvt 2 AmiAdvancedConnector.ConvertCassandraDataTest impContent rval={sval}", logModel);
            }
            else {
                Logger.Info($" cvt 2 AmiAdvancedConnector.ConvertCassandraDataTest impContent rval={sval}", logModel);
            }
            return retData;
        }

        public string LastConvertRoot { get; set; } = "";
        public string LastImportRoot { get; set; } = "";
        public string LastPurgeRoot { get; set; } = "";

        public string LastValidateRoot { get; set; } = "";

        public void Dispose() {
            // noting to do yet?
        }
        
    }



}
