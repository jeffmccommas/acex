﻿using System;
using System.Configuration;
using System.Diagnostics;
using AO.TimeSeriesProcessing;
using CE.AO.Logging;
using CE.AO.Models;
using Microsoft.WindowsAzure.ServiceRuntime;
using Microsoft.WindowsAzure.Storage.Blob;

namespace CE.AO.DataImport.BatchProcessWorker.Files
{
    public class AdvanceAmiProcessFile : ProcessFile
    {

        private LogModel _logModel;
        private string _fileName;
        private string[] _msgParts;
        private string _containerName;
        private ICloudBlob _blockBlob;


        //public string LastConvertRoot { get; set; } = "";
        //public string LastImportRoot { get; set; } = "";


        /// <summary>
        /// little different than the 'classic' process file stuff.  need more info and don't want to refactor all the old stuff.
        /// </summary>
        /// <param name="logModel"></param>
        /// <param name="fileName"></param>
        /// <param name="msgParts"></param>
        /// <param name="containerName"></param>
        public AdvanceAmiProcessFile(LogModel logModel, string fileName, string[] msgParts, string containerName) : base(logModel) {
            _logModel = logModel;
            _fileName = fileName;
            _msgParts = msgParts;
            _containerName = containerName;
        }


        /// <summary>
        /// little different than the 'classic' process file stuff.  need more info and don't want to refactor all the old stuff.
        /// </summary>
        /// <param name="fileName"></param>
        /// <param name="containerName"></param>
        /// <param name="clientId"></param>
        public override void Initialize(string fileName, string containerName, string clientId)
        {

            var interval = fileName.ToLower().Contains("05min")
                ? "5min"
                : fileName.ToLower().Contains("15min")
                    ? "15min"
                    : fileName.ToLower().Contains("30min")
                        ? "30min"
                        : fileName.ToLower().Contains("60min")
                            ? "60min"
                            : "Daily";
            _blockBlob = GetBlockBlob(fileName, containerName);

            if (_msgParts.Length > 3)
            {
                Logger.Info($"Ami{interval} additional control info {_msgParts[3]}", _logModel);
                if (!string.IsNullOrEmpty(_msgParts[3]) && _msgParts[3].ToLower().Equals("noarchive"))
                {
                    ArchiveBlob = false;
                    Logger.Info($"Ami{interval} additional control info for archive", _logModel);
                }
                if ((_msgParts.Length > 4) && !string.IsNullOrEmpty(_msgParts[4]) && _msgParts[4].ToLower().Equals("restartcomms"))
                {
                    try
                    {
                        ProcessingArgs pa = new ProcessingArgs();
                        string vsrc = pa.GetControlVal("reqVsrc", "evc");
                        ProcessingArgs.AccumulateRequestPars(vsrc, pa["aclenv"], "none", pa);
                        Logger.Info($"Ami{interval} restartcomms {_msgParts[4]}", _logModel);
                        string hostAddr = "127.0.0.1";
                        string hostPort = "4443";
                        string overrideAmi = "";
                        string tmpStg = ConfigurationManager.AppSettings["AdvAmiHostAddress"];
                        if (!string.IsNullOrEmpty(tmpStg))
                        {
                            hostAddr = tmpStg;
                            Logger.Info($"Ami{interval} restartcomms config  AdvAmiHostAddress={hostAddr}", _logModel);
                        }
                        tmpStg = ConfigurationManager.AppSettings["AdvAmiHostPort"];
                        if (!string.IsNullOrEmpty(tmpStg))
                        {
                            hostPort = tmpStg;
                            Logger.Info($"Ami{interval} restartcomms config  AdvAmiHostPort={hostPort}", _logModel);
                        }
                        tmpStg = ConfigurationManager.AppSettings["overrideAMIPrefix"];
                        if (!string.IsNullOrEmpty(tmpStg))
                        {
                            overrideAmi = tmpStg;
                            pa["overrideAMIPrefix"] = overrideAmi;
                            Logger.Info($"Ami{interval} restartcomms config  overrideAMIPrefix={overrideAmi}", _logModel);
                        }
                        if ((_msgParts.Length > 5) && !string.IsNullOrEmpty(_msgParts[5]))
                        {
                            overrideAmi = _msgParts[5];
                            overrideAmi = overrideAmi.Replace(';', ':');
                            pa["overrideAMIPrefix"] = overrideAmi;
                            Logger.Info($"Ami{interval} restartcomms message  overrideAMIPrefix={overrideAmi}", _logModel);
                        }


                        pa["hostIp"] = hostAddr;
                        pa["port"] = hostPort;
                        Logger.Info($"Ami{interval} restartcomms {hostAddr}:{hostPort} override={overrideAmi}", _logModel);
                        
                        Logger.Info(
                            $"Event Process Worker (Instance: {RoleEnvironment.CurrentRoleInstance.Id}) CheckEndpoint SimpleWebServer running at 127",
                            null);
                    }
                    catch (Exception e2)
                    {
                        Trace.TraceError($"restartcomms exception Ami5MinuteIntervalProcessFile.Initialize . {e2.Message}, {e2.InnerException?.Message} Details: {0}", e2);
                        Logger.Error($"restartcomms exception Ami5MinuteIntervalProcessFile.Initialize { e2.Message}, { e2.InnerException?.Message}", null);
                    }
                }
            }
            else
            {
                Logger.Info($"Ami{interval} no additional control info", _logModel);
            }
            ProcessAdvanceAmi(_blockBlob, _fileName, interval, _containerName);
        }
    }
}
