﻿using System;
using System.Collections.Generic;
using CE.AO.Models;
using CE.AO.Models.RawFileModels;
using Enums = CE.AO.Utilities.Enums;

namespace CE.AO.DataImport.BatchProcessWorker.Files
{
    public class NccAmiReadingProcessFile : ProcessFile
    {
        public NccAmiReadingProcessFile(LogModel logModel) : base(logModel) { }
        public override void Initialize(string fileName, string containerName, string clientId)
        {
            var blockBlob = GetBlockBlob(fileName, containerName);

            var customProperties = new Dictionary<string, object>
            {
                {"EventType", Convert.ToString(Enums.EventType.NccAmiReading)},
                {"ClientId", clientId},
                {"BlobContainer", containerName}
            };

            var client = GetEventHubClient();
            Process<RawNccAmiReadingModel>(blockBlob, customProperties, Enums.EventType.NccAmiReading, client);
            CloseEventHubClient(client);
            ArchieveBlob(blockBlob, fileName, containerName, Enums.EventType.NccAmiReading);
        }
    }
}
