﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AutoMapper;
using CE.AO.Models;
using CE.AO.Models.RawFileModels;
using Microsoft.ServiceBus.Messaging;
using Microsoft.WindowsAzure.Storage.Blob;
using Newtonsoft.Json;
using Enums = CE.AO.Utilities.Enums;

namespace CE.AO.DataImport.BatchProcessWorker.Files
{
    public abstract class ReadsProcessFile : ProcessFile
    {
        protected ReadsProcessFile(LogModel logModel) : base(logModel)
        {
        }

        /// <summary>
        /// List of RawBaseReadModel to trigger calculations
        /// </summary>
        public List<RawBaseReadModel> TriggerCalculationForList { get; set; }

        /// <summary>
        /// Depending on event type, check if record is already added to the list to trigger calculation; if not add record to the list
        /// </summary>        
        private void AddRecordToTriggerCalculation(RawBaseReadModel record)
        {
            if (TriggerCalculationForList == null)
                TriggerCalculationForList = new List<RawBaseReadModel>();
            List<RawBaseReadModel> existingModels =
               TriggerCalculationForList.Where(
                   m => m.METER_ID == record.METER_ID).ToList();
            if (existingModels.Count == 0)
                TriggerCalculationForList.Add(record);
        }

        public override void PreProcessRow(Enums.EventType eventType, IRawFileModel record)
        {
            AddRecordToTriggerCalculation(record as RawBaseReadModel);
        }

        /// <summary>
        /// Depending on event type, send events to event hub to trigger calculations
        /// </summary>        
        private void SendEventToTriggerCalculations(Enums.EventType eventType, List<RawBaseReadModel> triggerCalculationForList, IDictionary<string, object> customProperties, EventHubClient client)
        {
            if (triggerCalculationForList != null && triggerCalculationForList.Count > 0 && (eventType == Enums.EventType.NccAmiConsumption || eventType == Enums.EventType.MeterRead))
            {
                var eventsList = new List<EventData>();
                var logModel = Mapper.Map<LogModel, LogModel>(LogModel);
                logModel.RowIndex = "0";
                logModel.ProcessingEventCount = null;
                customProperties.Remove("EventType");

                if (eventType == Enums.EventType.NccAmiConsumption)
                {
                    logModel.Module = Enums.Module.NccAmiCalculationBatchProcess;
                    customProperties.Add("EventType", Convert.ToString(Enums.EventType.NccAmiCalculation));
                }
                else if (eventType == Enums.EventType.MeterRead)
                {
                    logModel.Module = Enums.Module.MeterReadCalculationBatchProcess;
                    customProperties.Add("EventType", Convert.ToString(Enums.EventType.MeterReadTriggerCalculation));
                }

                triggerCalculationForList.ForEach(model =>
                {
                    LogModel.Metadata = JsonConvert.SerializeObject(model);
                    var serializedEventData = JsonConvert.SerializeObject(model);
                    var data = new EventData(Encoding.UTF8.GetBytes(serializedEventData));

                    AddCustomProperties(data, customProperties, logModel);
                    eventsList.Add(data);
                });

                SendEventsInBatch(client, eventsList);
            }
        }

        public virtual void ProcessFile<T>(ICloudBlob blockBlob, IDictionary<string, object> customProperties,
            Enums.EventType eventType, EventHubClient client) where T : IRawFileModel
        {
            Process<T>(blockBlob, customProperties, eventType, client);
            SendEventToTriggerCalculations(eventType, TriggerCalculationForList, customProperties, client);
        }
    }
}