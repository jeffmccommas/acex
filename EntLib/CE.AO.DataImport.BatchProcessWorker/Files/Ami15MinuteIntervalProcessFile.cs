﻿using CE.AO.Models.RawFileModels;
using System;
using System.Collections.Generic;
using CE.AO.Models;
using Enums = CE.AO.Utilities.Enums;

namespace CE.AO.DataImport.BatchProcessWorker.Files
{
    public class Ami15MinuteIntervalProcessFile : ProcessFile
    {
        public Ami15MinuteIntervalProcessFile(LogModel logModel) : base(logModel) { }
        public override void Initialize(string fileName, string containerName, string clientId)
        {
            var blockBlob = GetBlockBlob(fileName, containerName);

            var customProperties = new Dictionary<string, object>
            {
                {"EventType", Convert.ToString(Enums.EventType.Ami)},
                {"IntervalType", Convert.ToInt32(Enums.IntervalType.Fifteen)},
                {"ClientId", clientId}
            };

            var client = GetEventHubClient();
            Process<RawAmi15MinuteIntervalModel>(blockBlob, customProperties, Enums.EventType.Ami, client);
            CloseEventHubClient(client);
            ArchieveBlob(blockBlob, fileName, containerName, Enums.EventType.Ami);
        }
    }
}
