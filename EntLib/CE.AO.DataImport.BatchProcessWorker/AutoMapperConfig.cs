﻿using System;
using AutoMapper;
using CE.AO.Logging;
using CE.AO.Models;
using CE.AO.Models.RawFileModels;

namespace CE.AO.DataImport.BatchProcessWorker
{
    /// <summary>
    /// AutoMapper configuration are configured in this class
    /// </summary>
    public static class AutoMapperConfig
    {
        /// <summary>
        /// AutoMapper configuration are configured in this method
        /// </summary>
        public static void AutoMapperMappings()
        {
            Mapper.CreateMap<string, bool?>()
               .ConvertUsing(str => !string.IsNullOrWhiteSpace(str) ? Convert.ToBoolean(str.Trim()) : (bool?)null);

            Mapper.CreateMap<string, DateTime?>()
                .ConvertUsing(str => !string.IsNullOrWhiteSpace(str) ? Convert.ToDateTime(str.Trim()) : (DateTime?)null);

            Mapper.CreateMap<string, double?>()
                .ConvertUsing(str => !string.IsNullOrWhiteSpace(str) ? Convert.ToDouble(str.Trim()) : (double?)null);

            Mapper.CreateMap<string, Guid?>()
                .ConvertUsing(str => !string.IsNullOrWhiteSpace(str) ? Guid.Parse(str.Trim()) : (Guid?)null);

            Mapper.CreateMap<string, int?>()
                .ConvertUsing(str => !string.IsNullOrWhiteSpace(str) ? Convert.ToInt32(str.Trim()) : (int?)null);

            Mapper.CreateMap<string, string>()
                .ConvertUsing(str => !string.IsNullOrWhiteSpace(str) ? str.Trim() : str);

            #region LogModel

            Mapper.CreateMap<LogModel, LogModel>()
                .ForAllUnmappedMembers(o => o.Ignore());

            #endregion

            #region AmiDailyInterval
            
            Mapper.CreateMap<RawAmiDailyIntervalModel, AmiDailyIntervalModel>()
               .ForMember(dest => dest.AmiTimeStamp, opts => opts.MapFrom(src => src.TimeStamp))
               .ForAllUnmappedMembers(o => o.Ignore());
            #endregion

            Mapper.AssertConfigurationIsValid();
        }

        /// <summary>
        /// Extension Method for configuring unmapped members
        /// </summary>
        /// <typeparam name="TSource">Type to map from</typeparam>
        /// <typeparam name="TDestination">Type to map to</typeparam>
        /// <param name="mapping">Mapping Expression</param>
        /// <param name="memberOptions">MemberConfigurationExpression</param>
        public static void ForAllUnmappedMembers<TSource, TDestination>(this IMappingExpression<TSource, TDestination> mapping, Action<IMemberConfigurationExpression<TSource>> memberOptions)
        {
            var typeMap = Mapper.FindTypeMapFor<TSource, TDestination>();
            foreach (var memberName in typeMap.GetUnmappedPropertyNames())
                mapping.ForMember(memberName, memberOptions);
        }
    }
}