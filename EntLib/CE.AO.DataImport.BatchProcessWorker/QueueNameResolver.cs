﻿using Microsoft.Azure;
using Microsoft.Azure.WebJobs;

namespace CE.AO.DataImport.BatchProcessWorker
{
    /// <summary>
    /// QueueNameResolver helps in resolving the queue name, so that in the web job functions we can use a constant as queue name parameter
    /// </summary>
    class QueueNameResolver : INameResolver
    {
        /// <summary>
        /// Gets the setting value from setting name 
        /// </summary>
        /// <param name="name">Setting Name</param>
        /// <returns>Setting Value</returns>
        public string Resolve(string name)
        {
            return CloudConfigurationManager.GetSetting(name);
        }
    }
}