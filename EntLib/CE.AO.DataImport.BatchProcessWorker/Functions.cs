﻿using System;
using System.IO;
using System.Linq;
using CE.AO.DataImport.BatchProcessWorker.Files;
using CE.AO.Logging;
using CE.AO.Models;
using Microsoft.Azure;
using Microsoft.Azure.WebJobs;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Blob;
using Enums = CE.AO.Utilities.Enums;

namespace CE.AO.DataImport.BatchProcessWorker
{
    /// <summary>
    /// Web Job functions to pop queue message
    /// </summary>
    public class Functions
    {
        private static CloudBlobClient _cloudBlobClient;
        public static CloudBlobClient CloudBlobClient
        {
            get
            {
                if (_cloudBlobClient != null) return _cloudBlobClient;
                var storageAccount =
                    CloudStorageAccount.Parse(
                        CloudConfigurationManager.GetSetting(StaticConfig.AclaraOneStorageConnectionString));
                _cloudBlobClient = storageAccount.CreateCloudBlobClient();
                return _cloudBlobClient;
            }
        }

        public static LogModel LogModel
        {
            get; set;
        }
        /// <summary>
        /// Method to pop queue message from ImportBatchProcessQueue queue
        /// </summary>
        /// <param name="blobNameContainerNameAndClientId">queue message</param>
        /// <param name="log">textwriter</param>
        public static void ProcessBatch([QueueTrigger(StaticConfig.ImportBatchProcessQueue)] string blobNameContainerNameAndClientId, TextWriter log)
        {
            var logModel = new LogModel();
            var startEndLogModel = new LogModel();
            if (LogModel == null)
            {
                logModel.Module = Enums.Module.FileProcessing;
                logModel.ProcessingType = Enums.ProcessingType.File;
                startEndLogModel.Module = Enums.Module.FileProcessing;
                startEndLogModel.ProcessingType = Enums.ProcessingType.File;
            }
            else
            {
                logModel = LogModel;
                startEndLogModel = LogModel;
            }

            Logger.Info($@"Queue message ""{blobNameContainerNameAndClientId}"" received for batch processing.", logModel);
            Logger.Info($@" 2  Queue message ""{blobNameContainerNameAndClientId}"" received for batch processing.", logModel);
            try {
                string[] blobInfo = blobNameContainerNameAndClientId.Split(':');
                Logger.Info($@" 2.5 {blobInfo.Length}  Queue message ""{blobNameContainerNameAndClientId}"" received for batch processing.", logModel);
                if (blobInfo.Length >= 3)
                {
                    Logger.Info($@" 2.6 0:{blobInfo[0]} 1:{blobInfo[1]} 2:{blobInfo[2]}  Queue message ""{blobNameContainerNameAndClientId}"" received for batch processing.", logModel);
                    string blobName = blobInfo[0];
                    string containerName = blobInfo[1];
                    string clientId = blobInfo[2];
                    logModel.Source = blobName;
                    startEndLogModel.Source = blobName;
                    logModel.ClientId = clientId;
                    startEndLogModel.ClientId = clientId;
                    Logger.Info($@" 3  Queue message ""{blobNameContainerNameAndClientId}"" received for batch processing.", logModel);
                    var processFile = ProcessFileFactory.GetProcessFileObject(blobName, logModel, blobInfo, containerName,CloudBlobClient);
                    Logger.Info($@" 4  Queue message ""{blobNameContainerNameAndClientId}"" received for batch processing.", logModel);
                    Logger.Info($@"Batch Processing of file ""{logModel.Source}"" started.", startEndLogModel);
                    processFile.Initialize(blobName, containerName, clientId);
                    Logger.Info($@"Batch Processing of file ""{logModel.Source}"" finished.", startEndLogModel);
                }
                else
                    Logger.Fatal(
                        $@"Queue message value '{blobNameContainerNameAndClientId}' received for batch processing is not in proper format. Format should be at least three components: 'BlobName:ContainerName:ClientId'.",
                        logModel);
            }
            catch (Exception ex)
            {
                Logger.Error(ex, logModel);
                throw;
            }
        }
    }
}
