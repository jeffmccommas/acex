﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CE.InsightDataAccess.InsightModels;
using CE.Portals.DataService.Interfaces;
using CE.Portals.DataService.Services;
using Microsoft.EntityFrameworkCore;
using Moq;
using Xunit;

namespace CE.Portals.Tests.DataAccess
{
    public class LoginServiceTests
    {
        Mock<InsightsContext> _mockContext = new Mock<InsightsContext>();
        ILoginService _sut; 
        public LoginServiceTests()
        {
            var portalLoginData = new List<PortalLogin>
            {
                new PortalLogin { UserId = 17,Username = "unittestadmin1",ClientId = 224,FirstName = "unit1",LastName = "admin1",PasswordHash = "gtB8Wwr/qjREhZpJpTawJ2yXJqMKxu1oIAAwAcs7xXQ=",PasswordSalt = "CC1F5582EE11B1EFD454613C1C400DCC",EnvId = 3,UserType = "admin",FailedLoginCount = 0,Enabled = true},
                new PortalLogin { UserId = 18,Username = "unitteststandard1",ClientId = 224,FirstName = "unit2",LastName = "standard1",PasswordHash = "gtB8Wwr/qjREhZpJpTawJ2yXJqMKxu1oIAAwAcs7xXQ=",PasswordSalt = "CC1F5582EE11B1EFD454613C1C400DCC",EnvId = 3,UserType = "admin",FailedLoginCount = 0,Enabled = true},
                new PortalLogin { UserId = 19,Username = "unittestadmin2",ClientId = 224,FirstName = "unit3",LastName = "admin2",PasswordHash = "gtB8Wwr/qjREhZpJpTawJ2yXJqMKxu1oIAAwAcs7xXQ=",PasswordSalt = "CC1F5582EE11B1EFD454613C1C400DCC",EnvId = 3,UserType = "admin",FailedLoginCount = 0,Enabled = true},
            }.AsQueryable();

            var mockSetPortalLogin = new Mock<DbSet<PortalLogin>>();
            mockSetPortalLogin.As<IQueryable<PortalLogin>>().Setup(m => m.Provider).Returns(portalLoginData.Provider);
            mockSetPortalLogin.As<IQueryable<PortalLogin>>().Setup(m => m.Expression).Returns(portalLoginData.Expression);
            mockSetPortalLogin.As<IQueryable<PortalLogin>>().Setup(m => m.ElementType).Returns(portalLoginData.ElementType);
            mockSetPortalLogin.As<IQueryable<PortalLogin>>().Setup(m => m.GetEnumerator()).Returns(portalLoginData.GetEnumerator());


            var clientPortalConfigData = new List<ClientPortalConfig>
            {
                new ClientPortalConfig { ClientId = 224,ClientPortalConfigId = 1,PortalConfigId = 1,PortalConfigValue = "UUFhY2NvdW50OlBBJHN3MHI2",IsEnabled = true},
                new ClientPortalConfig { ClientId = 224,ClientPortalConfigId = 2,PortalConfigId = 2,PortalConfigValue = "3",IsEnabled = true},
            }.AsQueryable();

            var mockSetClientPortalConfig = new Mock<DbSet<ClientPortalConfig>>();
            mockSetClientPortalConfig.As<IQueryable<ClientPortalConfig>>().Setup(m => m.Provider).Returns(clientPortalConfigData.Provider);
            mockSetClientPortalConfig.As<IQueryable<ClientPortalConfig>>().Setup(m => m.Expression).Returns(clientPortalConfigData.Expression);
            mockSetClientPortalConfig.As<IQueryable<ClientPortalConfig>>().Setup(m => m.ElementType).Returns(clientPortalConfigData.ElementType);
            mockSetClientPortalConfig.As<IQueryable<ClientPortalConfig>>().Setup(m => m.GetEnumerator()).Returns(clientPortalConfigData.GetEnumerator());

            var portalConfigData = new List<PortalConfig>
            {
                new PortalConfig { PortalConfigId = 1, PortalConfigName = "ESPMSECRETKEY", IsEnabled = true, PortalConfigTypeId= 1},
                new PortalConfig { PortalConfigId = 2, PortalConfigName = "INCORRECTLOGINS", IsEnabled = true, PortalConfigTypeId= 1}
            }.AsQueryable();

            var mockSetPortalConfig = new Mock<DbSet<PortalConfig>>();

            mockSetPortalConfig.As<IQueryable<PortalConfig>>().Setup(m => m.Provider).Returns(portalConfigData.Provider);
            mockSetPortalConfig.As<IQueryable<PortalConfig>>().Setup(m => m.Expression).Returns(portalConfigData.Expression);
            mockSetPortalConfig.As<IQueryable<PortalConfig>>().Setup(m => m.ElementType).Returns(portalConfigData.ElementType);
            mockSetPortalConfig.As<IQueryable<PortalConfig>>().Setup(m => m.GetEnumerator()).Returns(portalConfigData.GetEnumerator());
            
            var clientData = new List<Client>
            {
                new Client { ClientId = 224,Name = "UIL",Description = "test client",AuthType = 1,EnableInd = true}
            }.AsQueryable();

            var mockSetClient = new Mock<DbSet<Client>>();

            mockSetClient.As<IQueryable<Client>>().Setup(m => m.Provider).Returns(clientData.Provider);
            mockSetClient.As<IQueryable<Client>>().Setup(m => m.Expression).Returns(clientData.Expression);
            mockSetClient.As<IQueryable<Client>>().Setup(m => m.ElementType).Returns(clientData.ElementType);
            mockSetClient.As<IQueryable<Client>>().Setup(m => m.GetEnumerator()).Returns(clientData.GetEnumerator());

            var userData = new List<User>
            {
                new User { UserId = 39,ClientId = 224,ActorName = "EnergyStar User1",CeaccessKeyId = "9489cabf64db4242bb1881febd7e811e8d2b",EnableInd = true},
                new User { UserId = 38,ClientId = 87,ActorName = "EnergyStar User2",CeaccessKeyId = "9842c605b40a3a41b3ab0b779189b8a293ec",EnableInd = true}
            }.AsQueryable();

            var mockSetUser = new Mock<DbSet<User>>();

            mockSetUser.As<IQueryable<User>>().Setup(m => m.Provider).Returns(userData.Provider);
            mockSetUser.As<IQueryable<User>>().Setup(m => m.Expression).Returns(userData.Expression);
            mockSetUser.As<IQueryable<User>>().Setup(m => m.ElementType).Returns(userData.ElementType);
            mockSetUser.As<IQueryable<User>>().Setup(m => m.GetEnumerator()).Returns(userData.GetEnumerator());

            var userEnvironmentData = new List<UserEnvironment>
            {
                new UserEnvironment { UserId = 39,EnvId = 0,CesecretAccessKey = "DemoBasicKeyprod",BasicKey = "DemoBasicKey233"},
                new UserEnvironment { UserId = 39,EnvId = 1,CesecretAccessKey = "DemoBasicKeyuat",BasicKey = "DemoBasicKey233"},
                new UserEnvironment { UserId = 39,EnvId = 2,CesecretAccessKey = "DemoBasicKeyqa",BasicKey = "DemoBasicKey233"},
                new UserEnvironment { UserId = 39,EnvId = 3,CesecretAccessKey = "DemoBasicKeydev",BasicKey = "DemoBasicKey233"},

                new UserEnvironment { UserId = 38,EnvId = 0,CesecretAccessKey = "DemoBasicKeyprod",BasicKey = "DemoBasicKey233"},
                new UserEnvironment { UserId = 38,EnvId = 1,CesecretAccessKey = "DemoBasicKeyuat",BasicKey = "DemoBasicKey233"},
                new UserEnvironment { UserId = 38,EnvId = 2,CesecretAccessKey = "DemoBasicKeyqa",BasicKey = "DemoBasicKey233"},
                new UserEnvironment { UserId = 38,EnvId = 3,CesecretAccessKey = "DemoBasicKeydev",BasicKey = "DemoBasicKey233"},

            }.AsQueryable();

            var mockSetUserEnvironment = new Mock<DbSet<UserEnvironment>>();

            mockSetUserEnvironment.As<IQueryable<UserEnvironment>>().Setup(m => m.Provider).Returns(userEnvironmentData.Provider);
            mockSetUserEnvironment.As<IQueryable<UserEnvironment>>().Setup(m => m.Expression).Returns(userEnvironmentData.Expression);
            mockSetUserEnvironment.As<IQueryable<UserEnvironment>>().Setup(m => m.ElementType).Returns(userEnvironmentData.ElementType);
            mockSetUserEnvironment.As<IQueryable<UserEnvironment>>().Setup(m => m.GetEnumerator()).Returns(userEnvironmentData.GetEnumerator());



            _mockContext.Setup(m => m.PortalLogin).Returns(mockSetPortalLogin.Object);
            _mockContext.Setup(m => m.ClientPortalConfig).Returns(mockSetClientPortalConfig.Object);
            _mockContext.Setup(m => m.PortalConfig).Returns(mockSetPortalConfig.Object);
            _mockContext.Setup(m => m.Client).Returns(mockSetClient.Object);

            _mockContext.Setup(m => m.User).Returns(mockSetUser.Object);
            _mockContext.Setup(m => m.UserEnvironment).Returns(mockSetUserEnvironment.Object);
            _sut = new LoginService(_mockContext.Object);
        }
        [Fact]
        public void ValidateESPMUserTest()
        {
            
            var userDetails = _sut.ValidateESPMUser(Convert.ToBase64String(System.Text.Encoding.UTF8.GetBytes("unittestadmin1")), Convert.ToBase64String(System.Text.Encoding.UTF8.GetBytes("Password2")));
            Assert.Equal(userDetails.FirstName,"unit1");
        }

        [Fact]
        public void GetClientESPMSecretKeysTest()
        {
            
            var key = _sut.GetClientESPMSecretKeys("224");
            Assert.Equal(key, "UUFhY2NvdW50OlBBJHN3MHI2");
        }
        [Fact]
        public void GetClientESPMSecretKeysNullTest()
        {

            var key = _sut.GetClientESPMSecretKeys("123");
            Assert.Equal(key, null);
        }
        [Fact]
        public void GetUserDetailsTest()
        {
            var userDetails = _sut.GetUserDetails(19);
            Assert.Equal(userDetails.FirstName, "unit3");
        }

        [Fact]
        public void AddNewUserTest()
        {
            var userDetails = _sut.GetUserDetails(19);
            Assert.Equal(userDetails.FirstName, "unit3");

            _sut.AddNewUser(userDetails,3);
        }
        [Fact]
        public void CheckIfUserExistsTest()
        {
            var userexists = _sut.CheckIfUserExists("unitteststandard1");
            Assert.Equal(userexists, true);

            userexists = _sut.CheckIfUserExists("Error");
            Assert.Equal(userexists, false);
        }
        [Fact]
        public void GetUserProfileTest()
        {
            var userprofile = _sut.GetUserProfile(19);
            Assert.Equal(userprofile.FirstName, "unit3");

        }

        [Fact]
        public void GetUserListTest()
        {
            var userprofile = _sut.GetUserList(224);
            Assert.Equal(userprofile.Count, 3);

        }
        [Fact]
        public void UpdateUserTest()
        {
            var userDetails = _sut.GetUserDetails(19);
            Assert.Equal(userDetails.FirstName, "unit3");
            userDetails.Enabled = false;
            _sut.UpdateUser(userDetails, false);
            var userDetails2 = _sut.GetUserDetails(19);
            Assert.Equal(userDetails2.Enabled, false);
            userDetails.FailedLoginAttempts = 3;
            _sut.UpdateUser(userDetails, true);
            var userDetails3 = _sut.GetUserDetails(19);
            Assert.Equal(userDetails3.FailedLoginAttempts, 3);

        }
        [Fact]
        public void UpdateUserPasswordTest()
        {
           


            var userDetails = _sut.GetUserDetails(19);
            Assert.Equal(userDetails.FirstName, "unit3");
            userDetails.PassWord = "test";

            var baselineData_loginUsers = (from u in _mockContext.Object.PortalLogin
                where u.UserId == int.Parse(userDetails.UserId)
                select u).SingleOrDefault();
            string originalPasswordSalt = baselineData_loginUsers.PasswordSalt;

            _sut.UpdateUser(userDetails, false);

            var updated_loginUsers = (from u in _mockContext.Object.PortalLogin
                where u.UserId == int.Parse(userDetails.UserId)
                select u).SingleOrDefault();

            Assert.NotEqual(originalPasswordSalt, updated_loginUsers.PasswordSalt);
        }

        [Fact]
        public void GetInsightsUserCredsTest()
        {
            var insightsUserCreds = _sut.GetInsightsUserCreds(224, "EnergyStar User1",0);
            Assert.Equal(insightsUserCreds.CeSecretAccessKey, "DemoBasicKeyprod");
            Assert.Equal(insightsUserCreds.CeAccessKeyId, "9489cabf64db4242bb1881febd7e811e8d2b");

        }
        [Fact]
        public void GetInsightsUserCredsNullTest()
        {
            var insightsUserCreds = _sut.GetInsightsUserCreds(123, "EnergyStar User1", 0);
            Assert.Equal(insightsUserCreds, null);

        }

    }
}
