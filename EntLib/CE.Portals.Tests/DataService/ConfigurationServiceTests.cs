﻿using System.Collections.Generic;
using CE.InsightDataAccess.InsightModels;
using CE.InsightDataAccess.InsightsMetaDataModels;
using CE.Portals.DataService.Interfaces;
using CE.Portals.DataService.Services;
using Moq;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using Xunit;

namespace CE.Portals.Tests.DataService
{
    public class ConfigurationServiceTests
    {
        Mock<InsightsContext> _mockInsightsContext = new Mock<InsightsContext>();
        Mock<InsightsMetaDataContext> _mockInsightsMDContext = new Mock<InsightsMetaDataContext>();
        IConfigurationService _sut;

        public ConfigurationServiceTests()
        {
            var clientConfigurationData = new List<ClientConfiguration>
            {
                new ClientConfiguration
                {
                    ClientConfigurationId = 3477,
                    ClientId = 224,
                    EnvironmentKey = "dev",
                    ConfigurationKey = "espmbenchmark.shardname",
                    CategoryKey = "espmbenchmark",
                    Description = "This is the shard name for Client DW Database",
                    Value = "SHARDNAMEHERE"
                },
            }.AsQueryable();

            var mockDataSet = new Mock<DbSet<ClientConfiguration>>();
            mockDataSet.As<IQueryable<ClientConfiguration>>().Setup(m => m.Provider).Returns(clientConfigurationData.Provider);
            mockDataSet.As<IQueryable<ClientConfiguration>>().Setup(m => m.Expression).Returns(clientConfigurationData.Expression);
            mockDataSet.As<IQueryable<ClientConfiguration>>().Setup(m => m.ElementType).Returns(clientConfigurationData.ElementType);
            mockDataSet.As<IQueryable<ClientConfiguration>>().Setup(m => m.GetEnumerator()).Returns(clientConfigurationData.GetEnumerator());
            _mockInsightsMDContext.Setup(m => m.ClientConfiguration).Returns(mockDataSet.Object);

            _sut = new ConfigurationService(_mockInsightsContext.Object, _mockInsightsMDContext.Object);
        }

        [Fact]
        public void GetDwDatabaseConfigValue()
        {
            var result = _sut.GetDwDatabaseConfigValue(224, "dev", "espmbenchmark.shardname");
            Assert.Equal(result, "SHARDNAMEHERE");
        }

    }
}
