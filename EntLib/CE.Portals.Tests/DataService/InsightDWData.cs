﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CE.InsightDataAccess.InsightsDWModels;
using Microsoft.EntityFrameworkCore;
using Moq;
using Remotion.Linq.Utilities;

namespace CE.Portals.Tests.DataService
{
    public static class InsightDWMockData
    {
        public static Mock<DbSet<DimCustomer>> GetMockDimCustomerDataSet()
        {
            var dimCustomerData = new List<DimCustomer>
            {
                new DimCustomer {CustomerKey = 5832232,
                    ClientKey = 22,
                    ClientId = 224,
                    PostalCode = "12345",
                    CustomerAuthenticationTypeKey = 1,
                    SourceKey = 8,
                    CityKey = 20556,
                    CustomerId = "0010000000079390",
                    FirstName = "Sample1",
                    LastName = "LastName1",
                    Street1 = "32 HASENFUS CIR",
                    Street2 = "2ND",
                    City = "Needham",
                    StateProvince = "MA",
                    Country = "MA",

                },
                new DimCustomer {CustomerKey = 5832233,
                    ClientKey = 22,
                    ClientId = 224,
                    PostalCode = "12345",
                    CustomerAuthenticationTypeKey = 1,
                    SourceKey = 8,
                    CityKey = 3407,
                    CustomerId = "0010000000114775",
                    FirstName = "BRIAN",
                    LastName = "SCHATZ",
                    Street1 = "90 OLIVER ST",
                    Street2 = "",
                    City = "NORTH HAVEN",
                    StateProvince = "MA",
                    Country = "MA",

                },
                new DimCustomer {CustomerKey = 5832241,
                    ClientKey = 22,
                    ClientId = 224,
                    PostalCode = "12345",
                    CustomerAuthenticationTypeKey = 1,
                    SourceKey = 8,
                    CityKey = 21374,
                    CustomerId = "0010000000115012",
                    FirstName = "PAUL RALPH",
                    LastName = "DERENZO",
                    Street1 = "3 FRANCIS ST",
                    Street2 = "EAST HAVEN",
                    City = "Needham",
                    StateProvince = "MA",
                    Country = "MA",

                },
            }.AsQueryable();

            var mockSetData = new Mock<DbSet<DimCustomer>>();
            mockSetData.As<IQueryable<DimCustomer>>().Setup(m => m.Provider).Returns(dimCustomerData.Provider);
            mockSetData.As<IQueryable<DimCustomer>>().Setup(m => m.Expression).Returns(dimCustomerData.Expression);
            mockSetData.As<IQueryable<DimCustomer>>().Setup(m => m.ElementType).Returns(dimCustomerData.ElementType);
            mockSetData.As<IQueryable<DimCustomer>>().Setup(m => m.GetEnumerator()).Returns(dimCustomerData.GetEnumerator());

            return mockSetData;
        }

        public static Mock<DbSet<FactCustomerPremise>> GetMockFactCustomerPremiseDataSet()
        {
            var factCustomerPremiseData = new List<FactCustomerPremise>
            {
                new FactCustomerPremise {
                    ClientId = 224,
                    CustomerKey = 5832232,
                    PremiseKey = 6982579
                },
                new FactCustomerPremise {

                    ClientId = 224,
                    CustomerKey = 5832233,
                    PremiseKey = 6982576
                },
                new FactCustomerPremise {

                    ClientId = 224,
                    CustomerKey = 5832241,
                    PremiseKey = 6982577
                }
            }.AsQueryable();

            var mockSetData = new Mock<DbSet<FactCustomerPremise>>();
            mockSetData.As<IQueryable<FactCustomerPremise>>().Setup(m => m.Provider).Returns(factCustomerPremiseData.Provider);
            mockSetData.As<IQueryable<FactCustomerPremise>>().Setup(m => m.Expression).Returns(factCustomerPremiseData.Expression);
            mockSetData.As<IQueryable<FactCustomerPremise>>().Setup(m => m.ElementType).Returns(factCustomerPremiseData.ElementType);
            mockSetData.As<IQueryable<FactCustomerPremise>>().Setup(m => m.GetEnumerator()).Returns(factCustomerPremiseData.GetEnumerator());

            return mockSetData;
        }

        public static Mock<DbSet<DimPremise>> GetMockDimPremiseDataSet()
        {
            var dimPremiseData = new List<DimPremise>
            {
                new DimPremise {
                    PremiseKey = 6982577,
                    PostalCodeKey = 375748,
                    CityKey = 20781,
                    SourceKey = 8,
                    AccountId = "01000012317441",
                    PremiseId = "1120029514025",
                    Street1 = "52 DERBY AVE 2ND",
                    Street2 = "NEW HAVEN",
                    City = "CT",
                    StateProvince = "MA",
                    Country = "US",
                    PostalCode = "06511",
                    GasService = 1,
                    ElectricService = 1,
                    WaterService = 1,
                    ClientId = 224,
                    SourceId = 8,
                    IsInferred = 1

                },
                new DimPremise {
                    PremiseKey = 6982576,
                    PostalCodeKey = 375748,
                    CityKey = 20781,
                    SourceKey = 8,
                    AccountId = "01000012316963",
                    PremiseId = "1120029529030",
                    Street1 = "69 COUNTY ST 2-3",
                    Street2 = "NEW HAVEN",
                    City = "CT",
                    StateProvince = "MA",
                    Country = "US",
                    PostalCode = "06511",
                    GasService = 1,
                    ElectricService = 1,
                    WaterService = 1,
                    ClientId = 224,
                    SourceId = 8,
                    IsInferred = 1

                },
                new DimPremise {
                    PremiseKey = 6982579,
                    PostalCodeKey = 375748,
                    CityKey = 20781,
                    SourceKey = 8,
                    AccountId = "01000004504980",
                    PremiseId = "1130020252025",
                    Street1 = "101 GILBERT AVE 3RD",
                    Street2 = "NEW HAVEN",
                    City = "CT",
                    StateProvince = "MA",
                    Country = "US",
                    PostalCode = "06511",
                    GasService = 1,
                    ElectricService = 1,
                    WaterService = 1,
                    ClientId = 224,
                    SourceId = 8,
                    IsInferred = 1

                }
            }.AsQueryable();

            var mockSetData = new Mock<DbSet<DimPremise>>();
            mockSetData.As<IQueryable<DimPremise>>().Setup(m => m.Provider).Returns(dimPremiseData.Provider);
            mockSetData.As<IQueryable<DimPremise>>().Setup(m => m.Expression).Returns(dimPremiseData.Expression);
            mockSetData.As<IQueryable<DimPremise>>().Setup(m => m.ElementType).Returns(dimPremiseData.ElementType);
            mockSetData.As<IQueryable<DimPremise>>().Setup(m => m.GetEnumerator()).Returns(dimPremiseData.GetEnumerator());

            return mockSetData;
        }

        public static Mock<DbSet<Espmmap>> GetMockEspmmapDataSet()
        {
            var espmmapData = new List<Espmmap>
            {
                new Espmmap {
                    MapId = 11,
                    AccountId = "01000004504980",
                    PremiseId = "1130020252025",
                    EspmMapDetails = new List<EspmMapDetails>
                    {
                        new EspmMapDetails
                        {
                            Enabled = true,
                            MapId = 11,
                            MapAccountId = "01000004504980",
                            MapPremiseId = "1130020252025",
                            MapDetailId = 27

                        }
                    }
                },
                new Espmmap {

                },
                new Espmmap {

                }
            }.AsQueryable();

            var mockSetData = new Mock<DbSet<Espmmap>>();
            mockSetData.As<IQueryable<Espmmap>>().Setup(m => m.Provider).Returns(espmmapData.Provider);
            mockSetData.As<IQueryable<Espmmap>>().Setup(m => m.Expression).Returns(espmmapData.Expression);
            mockSetData.As<IQueryable<Espmmap>>().Setup(m => m.ElementType).Returns(espmmapData.ElementType);
            mockSetData.As<IQueryable<Espmmap>>().Setup(m => m.GetEnumerator()).Returns(espmmapData.GetEnumerator());

            return mockSetData;
        }
        public static Mock<DbSet<EspmMapDetails>> GetMockEspmmapDetailsDataSet()
        {
            var espmmapData = new List<EspmMapDetails>
            {
                new EspmMapDetails {
                    Enabled = true,
                    MapId = 11,
                    MapAccountId = "01000004504980",
                    MapPremiseId = "1130020252025",
                    MapDetailId = 27

                },
                new EspmMapDetails {

                },
                new EspmMapDetails {

                }
            }.AsQueryable();

            var mockSetData = new Mock<DbSet<EspmMapDetails>>();
            mockSetData.As<IQueryable<EspmMapDetails>>().Setup(m => m.Provider).Returns(espmmapData.Provider);
            mockSetData.As<IQueryable<EspmMapDetails>>().Setup(m => m.Expression).Returns(espmmapData.Expression);
            mockSetData.As<IQueryable<EspmMapDetails>>().Setup(m => m.ElementType).Returns(espmmapData.ElementType);
            mockSetData.As<IQueryable<EspmMapDetails>>().Setup(m => m.GetEnumerator()).Returns(espmmapData.GetEnumerator());

            return mockSetData;
        }
    }
}
