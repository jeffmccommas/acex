﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CE.InsightDataAccess.InsightModels;
using CE.InsightDataAccess.InsightsDWModels;
using CE.Portals.DataService.Interfaces;
using CE.Portals.DataService.Services;
using Microsoft.EntityFrameworkCore;
using Moq;
using Xunit;

namespace CE.Portals.Tests.DataService
{
    public class MappingServiceTests
    {
        Mock<InsightsContext> _mockInsightsContext = new Mock<InsightsContext>();
        Mock<InsightsDWContext> _mockInsightsDWContext = new Mock<InsightsDWContext>();
        IMappingService _sut;

        public MappingServiceTests()
        {
            _mockInsightsDWContext.Setup(m => m.DimCustomer).Returns(InsightDWMockData.GetMockDimCustomerDataSet().Object);
            _mockInsightsDWContext.Setup(m => m.FactCustomerPremise).Returns(InsightDWMockData.GetMockFactCustomerPremiseDataSet().Object);
            _mockInsightsDWContext.Setup(m => m.DimPremise).Returns(InsightDWMockData.GetMockDimPremiseDataSet().Object);

            _sut = new MappingService(_mockInsightsContext.Object, _mockInsightsDWContext.Object);
        }



        [Fact]
        public void CheckMappingTest()
        {
            var mappingExists = _sut.CheckMapping("InsightsDW_224", "01000012317441", "1120029514025");
            Assert.Equal(mappingExists, true);
        }

        [Fact]
        public void GetMappedCustomerDetailsTest()
        {
            var customerDetails = _sut.GetMappedCustomerDetails("InsightsDW_224", "01000012317441",null);
            Assert.Equal(customerDetails.FirstName, "PAUL RALPH");
        }
        [Fact]
        public void GetMappedMetersFromDimPremiseTest()
        {
            var premiseDetails = _sut.GetMappedMetersFromDimPremise("InsightsDW_224", "01000012316963", "1120029529030");
            Assert.Equal(premiseDetails.AccountId, "01000012316963");
            Assert.Equal(premiseDetails.PremiseId, "1120029529030");
        }
    }
}
