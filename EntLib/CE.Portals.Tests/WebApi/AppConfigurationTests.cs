﻿using Xunit;
using Moq;
using Microsoft.EntityFrameworkCore;
using CE.Portals.WebApi.Core.Controllers;
using CE.Portals.Integrations.Models;
using CE.Portals.DataService.Services;
using System.Net;
using CE.Portals.Integrations.Common;
using CE.Portals.WebApi.Core.Models;
using Microsoft.Extensions.Options;
using Microsoft.Extensions.Configuration;
using System.Collections.Generic;
using CE.InsightDataAccess.InsightModels;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using CE.Portals.WebApi.Core.Helpers;
using Microsoft.Extensions.Caching.Memory;

namespace CE.Portals.Tests.WebApi
{
    public class AppConfigurationTests
    {
        private Mock<InsightsContext> _dbContextMock = new Mock<InsightsContext>();
        private LoginService loginService;
        private EnergyStarServices energyStartService;
        private PortalsApiController _apiController;
        private Mock<DbSet<ClientPortalConfig>> clientPortalconfigDbSetMock;
        private Mock<DbSet<PortalConfig>> portalconfigDbSetMock;
        private IOptions<AppSettings> appSettings;
        private ILogger<EnergyStarController> moqLogger;
        private MemoryCache _cache;
        public AppConfigurationTests()
        {
            _cache = new MemoryCache(new MemoryCacheOptions());
            var fakePortalConfig = new PortalConfig() { IsEnabled = true, PortalConfigId = 1, PortalConfigName = ApplicationConstants.ESPMSecterKey };
            var fakeClientPortalConfig = new ClientPortalConfig() { ClientId = 1, IsEnabled = true, PortalConfigId = 1, PortalConfigValue = "TEST" };
            var loggerService = new Mock<ILogger<PortalsApiController>>();
            
            clientPortalconfigDbSetMock = DbSetMock.Create(fakeClientPortalConfig);
            portalconfigDbSetMock = DbSetMock.Create(fakePortalConfig);

            _dbContextMock.Setup(m => m.PortalConfig).Returns(portalconfigDbSetMock.Object);
            _dbContextMock.Setup(m => m.ClientPortalConfig).Returns(clientPortalconfigDbSetMock.Object);
            loginService = new LoginService(_dbContextMock.Object);
            var configuration = new ConfigurationBuilder()
               .AddInMemoryCollection(
                   new Dictionary<string, string>
                   {
                        {"AppSettings:EventTrackingKey", "oFZini0ZhGzpQt1RUmMTF+RIvghns1bt3dcNsL+1c8M="},
                        {"AppSettings:EventTrackingURL", "//aclaceeventtrackv2np.servicebus.windows.net/aclaceeventtrackv2eventhubnp" },
                        { "AppSettings:CEENvironment", "dev"},
                        { "AppSettings:ESPMBaseURL", "http://localhost/MockEnergyStarServices/MockESService.svc"},
                        { "AppSettings:APIBaseURL", "http://localhost:5000"},
                        { "AppSettings:InsightsAPIBaseURL", "http://localhost/"},
                        { "AppSettings:PurposeKey", "AAECAwQFBgcICQoLDA0ODw=="}
                   })
               .Build();

            loginService = new LoginService(_dbContextMock.Object);
            energyStartService = new EnergyStarServices(_dbContextMock.Object);
            var services = new ServiceCollection();
            services.Configure<AppSettings>(configuration.GetSection("Appsettings"));
            services.AddOptions();
            var serviceProvider = services.BuildServiceProvider();
            var options = serviceProvider.GetService<IOptions<AppSettings>>();
            var optionsJWT = serviceProvider.GetService<IOptions<JWTIssuer>>();
            _apiController = new PortalsApiController(loginService, options, loggerService.Object, _cache, energyStartService, optionsJWT);
        }

        //[Fact]
        //public void GetApplicationSettings()
        //{
        //    var result = (_apiController.GetAuthKey("1", ApplicationConstants.ESPMSecterKey));
        //    var content = ((Microsoft.AspNetCore.Mvc.OkObjectResult)result);
        //    Assert.NotNull(content);
        //    Assert.Equal(content.StatusCode, (int)HttpStatusCode.OK);
        //    Assert.Equal(content.Value, "TEST");
        //}

        //[Fact]
        //public void ValidateSecretKeys()
        //{
        //    var result = (_apiController.GetAuthKey("1", ApplicationConstants.ESPMSecterKey));
        //    var content = ((Microsoft.AspNetCore.Mvc.OkObjectResult)result);
        //    Assert.Equal(content.StatusCode, (int)HttpStatusCode.OK);
        //    Assert.Equal(content.Value, "TEST");
        //}

        //[Fact]
        //public void ValidateSecretKeysInvalidClient()
        //{
        //    var result =_apiController.GetAuthKey("3", ApplicationConstants.ESPMSecterKey);
        //    var content = ((Microsoft.AspNetCore.Mvc.StatusCodeResult)result);
        //    //Assert.Null(content);
        //    Assert.Equal(content.StatusCode, (int)HttpStatusCode.Forbidden);
        //}

        [Fact]
        public void WriteEventHandleMessages()
        {
           
        }
    }
}
