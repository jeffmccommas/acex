﻿using Xunit;

using CE.Portals.Integrations.Security;
using System;
using Moq;
using Microsoft.EntityFrameworkCore;
using CE.Portals.WebApi.Core.Controllers;
using CE.Portals.DataService.Services;
using System.Net;
using CE.Portals.WebApi.Core.Models;
using Microsoft.Extensions.Options;
using CE.Portals.Integrations.Common;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Configuration;
using System.Collections.Generic;
using CE.InsightDataAccess.InsightModels;
using CE.Portals.DataService.Interfaces;
using CE.Portals.DataService.Models;
using Microsoft.Extensions.Logging;
using CE.Portals.WebApi.Core.Helpers;

namespace CE.Portals.Tests.WebApi
{
    public class LoginServiceTest
    {
        private Mock<ILogger<PortalsApiController>> _moqLogger;
        private PortalLogin fakeportalLogin;
        private Mock<InsightsContext> _dbContextMock = new Mock<InsightsContext>();
        private Mock<DbSet<PortalLogin>> userDbSetMock;
        private LoginService loginService;
        private IOptions<AppSettings> appSettings;
        private Mock<DbSet<ClientPortalConfig>> clientPortalconfigDbSetMock;
        private Mock<DbSet<PortalConfig>> portalconfigDbSetMock;
        private EnergyStarServices energyStartService;

        public LoginServiceTest()
        {
            var encryptor = new Encryptor("test");
            string salt = Guid.NewGuid().ToString();
            var hashedpassword = encryptor.GenerateHashWithSalt("test", salt);

            fakeportalLogin = new PortalLogin() { UserId = 1, Username = "test", ClientId = 1, PasswordHash = hashedpassword, PasswordSalt = salt, EnvId = 1 };
            var fakePortalConfig1 = new PortalConfig() { IsEnabled = true, PortalConfigId = 1, PortalConfigName = ApplicationConstants.ESPMSecterKey };
            var fakePortalConfig2 = new PortalConfig() { IsEnabled = true, PortalConfigId = 2, PortalConfigName = ApplicationConstants.MAXINCORRECTLOGINS };
            var fakePortalConfig = new List<PortalConfig> { fakePortalConfig1, fakePortalConfig2 };

            var fakeClientPortalConfig1 = new ClientPortalConfig() { ClientId = 1, IsEnabled = true, PortalConfigId = 1, PortalConfigValue = "TEST" };
            var fakeClientPortalConfig2 = new ClientPortalConfig() { ClientId = 1, IsEnabled = true, PortalConfigId = 2, PortalConfigValue = "1" };
            var fakeClientPortalConfig = new List<ClientPortalConfig> { fakeClientPortalConfig1, fakeClientPortalConfig2 };


            userDbSetMock = DbSetMock.Create(fakeportalLogin);
            clientPortalconfigDbSetMock = fakeClientPortalConfig.AsDbSetMock();
            portalconfigDbSetMock = fakePortalConfig.AsDbSetMock();

            _dbContextMock.Setup(m => m.PortalLogin).Returns(userDbSetMock.Object);
            _dbContextMock.Setup(m => m.PortalConfig).Returns(portalconfigDbSetMock.Object);
            _dbContextMock.Setup(m => m.ClientPortalConfig).Returns(clientPortalconfigDbSetMock.Object);
        }

        private PortalsApiController TestSetup(HttpStatusCode returnStatusCode)
        {
            _moqLogger = new Mock<ILogger<PortalsApiController>>();
            var configuration = new ConfigurationBuilder()
                .AddInMemoryCollection(
                    new Dictionary<string, string>
                    {
                        {"AppSettings:EventTrackingKey", "oFZini0ZhGzpQt1RUmMTF+RIvghns1bt3dcNsL+1c8M="},
                        {"AppSettings:EventTrackingURL", "//aclaceeventtrackv2np.servicebus.windows.net/aclaceeventtrackv2eventhubnp" },
                        { "AppSettings:CEENvironment", "dev"},
                        { "AppSettings:ESPMBaseURL", "http://localhost/MockEnergyStarServices/MockESService.svc"},
                        { "AppSettings:APIBaseURL", "http://localhost:5000"},
                        { "AppSettings:InsightsAPIBaseURL", "http://localhost/"},
                        { "AppSettings:PurposeKey", "AAECAwQFBgcICQoLDA0ODw=="}
                    })
                .Build();

            loginService = new LoginService(_dbContextMock.Object);
            energyStartService = new EnergyStarServices(_dbContextMock.Object);
            var services = new ServiceCollection();
            services.Configure<AppSettings>(configuration.GetSection("Appsettings"));
            services.AddOptions();
            var serviceProvider = services.BuildServiceProvider();
            var options = serviceProvider.GetService<IOptions<AppSettings>>();
            var optionsJWT = serviceProvider.GetService<IOptions<JWTIssuer>>();

            Mock<ILoginService> mockloginService = new Mock<ILoginService>();
            mockloginService.Setup(x => x.CheckDbConnection()).Returns("Connection Successful");
            mockloginService.Setup(x => x.ValidateESPMUser("", "")).Returns(loginService.ValidateESPMUser("", ""));
            mockloginService.Setup(x => x.ValidateESPMUser(Convert.ToBase64String(System.Text.Encoding.UTF8.GetBytes("test")), Convert.ToBase64String(System.Text.Encoding.UTF8.GetBytes("test")))).Returns(loginService.ValidateESPMUser(Convert.ToBase64String(System.Text.Encoding.UTF8.GetBytes("test")), Convert.ToBase64String(System.Text.Encoding.UTF8.GetBytes("test"))));



            return new PortalsApiController(mockloginService.Object, options, _moqLogger.Object,null, energyStartService, optionsJWT);
        }
        [Fact]
        public void ValidateValidUserwithValidPassword()
        {
            var controller = TestSetup(HttpStatusCode.OK);
            var result = controller.GetUserAuth(new ApplicationUser() { UserName = Convert.ToBase64String(System.Text.Encoding.UTF8.GetBytes("test")), Password = Convert.ToBase64String(System.Text.Encoding.UTF8.GetBytes("test")) });
            var content = (((Microsoft.AspNetCore.Mvc.ObjectResult)result).Value.ToString());
            Assert.NotEmpty(content);
            //Assert.Equal(content.ClientId, fakeportalLogin.ClientId.ToString());            
        }

        [Fact]
        public void ValidateValidUserwithInValidPassword()
        {
            var controller = TestSetup(HttpStatusCode.OK);
            var result = controller.GetUserAuth(new ApplicationUser() { UserName = Convert.ToBase64String(System.Text.Encoding.UTF8.GetBytes("test")), Password = Convert.ToBase64String(System.Text.Encoding.UTF8.GetBytes("Nottest")) });
            var content = ((Microsoft.AspNetCore.Mvc.StatusCodeResult)result).StatusCode;
            Assert.Equal(content, (int)HttpStatusCode.NotFound);
        }

        [Fact]
        public void ValidateInValidUser()
        {
            var controller = TestSetup(HttpStatusCode.OK);
            var result = controller.GetUserAuth(new ApplicationUser() { UserName = Convert.ToBase64String(System.Text.Encoding.UTF8.GetBytes("nottest")), Password = Convert.ToBase64String(System.Text.Encoding.UTF8.GetBytes("Nottest")) });
            var content = ((Microsoft.AspNetCore.Mvc.StatusCodeResult)result).StatusCode;
            Assert.Equal(content, (int)HttpStatusCode.NotFound);

        }

      /*  [Fact]
        public void ValidateInValidUser()
        {
            var controller = TestSetup(HttpStatusCode.OK);
            var result = controller.GetUserAuth(new ApplicationUser() { UserName = "nottest", Password = "Nottest" });
            var content = ((Microsoft.AspNetCore.Mvc.StatusCodeResult)result).StatusCode;
            Assert.Equal(content, (int)HttpStatusCode.NotFound);

        }*/
    }
}
