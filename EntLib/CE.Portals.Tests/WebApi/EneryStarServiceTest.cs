﻿using Xunit;
using CE.InsightDataAccess.InsightModels;
using System;
using Moq;
using Microsoft.EntityFrameworkCore;
using CE.Portals.WebApi.Core.Controllers;
using CE.Portals.DataService.Services;
using CE.Portals.Integrations.Common;
using CE.Portals.WebApi.Core.Models;
using Microsoft.Extensions.Options;
using Microsoft.Extensions.Configuration;
using System.Collections.Generic;
using System.Net;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Security.Claims;
using CE.InsightDataAccess.InsightsDWModels;
using CE.InsightDataAccess.InsightsMetaDataModels;
using CE.Portals.Tests.DataService;
using CE.Portals.Tests.Moq;
using Microsoft.Extensions.Logging;

namespace CE.Portals.Tests.WebApi
{
    public class EneryStarServiceTest
    {
        private Mock<ILogger<EnergyStarController>> _moqLogger;
        private PortalLogin fakeportalLogin;
        private Mock<InsightsContext> _dbContextMock = new Mock<InsightsContext>();
        private Mock<InsightsDWContext> _dbDwContextMock = new Mock<InsightsDWContext>();
        private Mock<InsightsMetaDataContext> _dbMetadataContextMock = new Mock<InsightsMetaDataContext>();
        private Mock<DbSet<PortalLogin>> userDbSetMock;
        private LoginService loginService;
        private Mock<DbSet<ClientConfiguration>> clientConfigurationDbSetMock;
        private Mock<DbSet<PortalConfig>> portalconfigDbSetMock;
        private MoqHttpHandlersESPM moqHttpHandlersESPM;
        private Uri _uri;
        private EnergyStarServices energyStartService;
        private ConfigurationService configurationService;
        private MappingService mappingService;

        public EneryStarServiceTest()
        {
            _moqLogger = new Mock<ILogger<EnergyStarController>>();
            var fakePortalConfig = new PortalConfig() { IsEnabled = true, PortalConfigId = 1, PortalConfigName = ApplicationConstants.ESPMSecterKey };
            var fakeClientConfiguration = new ClientConfiguration() { ClientId = 1,Description = "test desc",Value = "shardname",ClientConfigurationId = 1,CategoryKey = "shardname", EnvironmentKey = "dev",ConfigurationKey = "espmbenchmark.shardname", DateUpdated = DateTime.Now};
            _uri = new Uri("https://portfoliomanager.energystar.gov/wstest/");

            userDbSetMock = DbSetMock.Create(fakeportalLogin);
            clientConfigurationDbSetMock = DbSetMock.Create(fakeClientConfiguration);
            portalconfigDbSetMock = DbSetMock.Create(fakePortalConfig);
            _dbContextMock.Setup(m => m.PortalLogin).Returns(userDbSetMock.Object);
            _dbContextMock.Setup(m => m.PortalConfig).Returns(portalconfigDbSetMock.Object);
            _dbMetadataContextMock.Setup(m => m.ClientConfiguration).Returns(clientConfigurationDbSetMock.Object);

            _dbDwContextMock.Setup(m => m.DimCustomer).Returns(InsightDWMockData.GetMockDimCustomerDataSet().Object);
            _dbDwContextMock.Setup(m => m.FactCustomerPremise).Returns(InsightDWMockData.GetMockFactCustomerPremiseDataSet().Object);
            _dbDwContextMock.Setup(m => m.DimPremise).Returns(InsightDWMockData.GetMockDimPremiseDataSet().Object);
            _dbDwContextMock.Setup(m => m.Espmmap).Returns(InsightDWMockData.GetMockEspmmapDataSet().Object);
            _dbDwContextMock.Setup(m => m.EspmMapDetails).Returns(InsightDWMockData.GetMockEspmmapDetailsDataSet().Object);


        }

        private EnergyStarController TestSetup(HttpStatusCode returnStatusCode,bool setMessageHandlerToNull=false)
        {
            moqHttpHandlersESPM = new MoqHttpHandlersESPM(_uri, "Unit Test Content", returnStatusCode);
            if (setMessageHandlerToNull)
                moqHttpHandlersESPM = null;


            var configuration = new ConfigurationBuilder()
                .AddInMemoryCollection(
                    new Dictionary<string, string>
                    {
                        {"AppSettings:EventTrackingKey", "oFZini0ZhGzpQt1RUmMTF+RIvghns1bt3dcNsL+1c8M="},
                        {"AppSettings:EventTrackingURL", "//aclaceeventtrackv2np.servicebus.windows.net/aclaceeventtrackv2eventhubnp" },
                        { "AppSettings:CEENvironment", "dev"},
                        { "AppSettings:ESPMBaseURL", "https://portfoliomanager.energystar.gov/wstest/"},
                        { "AppSettings:APIBaseURL", ""},
                        { "AppSettings:InsightsAPIBaseURL", ""},
                        { "AppSettings:PurposeKey", "AAECAwQFBgcICQoLDA0ODw=="}
                    })
                .Build();

            loginService = new LoginService(_dbContextMock.Object);
            energyStartService = new EnergyStarServices(_dbContextMock.Object);
            configurationService = new ConfigurationService(_dbContextMock.Object, _dbMetadataContextMock.Object);
            mappingService = new MappingService(_dbContextMock.Object, _dbDwContextMock.Object);
            var services = new ServiceCollection();
            services.Configure<AppSettings>(configuration.GetSection("Appsettings"));
            services.AddOptions();
            var serviceProvider = services.BuildServiceProvider();
            var options = serviceProvider.GetService<IOptions<AppSettings>>();
            var apiController = new EnergyStarController(options, _moqLogger.Object, energyStartService, mappingService, configurationService, null, loginService, moqHttpHandlersESPM);
            apiController.ControllerContext = new ControllerContext();
            return apiController;

        }
        private HttpContext GetMockedHttpContext()
        {
            var httpcontext = new Mock<HttpContext>();
            httpcontext.Setup(ctx => ctx.Request).Returns(new Mock<HttpRequest>().Object);
            httpcontext.Setup(ctx => ctx.Response).Returns(new Mock<HttpResponse>().Object);
            httpcontext.Setup(ctx => ctx.User).Returns(new Mock<ClaimsPrincipal>().Object);
            httpcontext.Setup(ctx => ctx.Session).Returns(new Mock<ISession>().Object);
            var items = new Dictionary<object, object>();
            var tokenstring = "{UserId: \"1\",ClientId: \"1\",ESPMSecretKey: \"test\"}";
            items.Add(ApplicationConstants.WEBTOKEN, tokenstring);
            httpcontext.Setup(ctx => ctx.Items).Returns(items);
            return httpcontext.Object;

        }
        private HttpContext GetMockedHttpContextNoUserInfo()
        {

            var httpcontext = new Mock<HttpContext>();
            httpcontext.Setup(ctx => ctx.Request).Returns(new Mock<HttpRequest>().Object);
            httpcontext.Setup(ctx => ctx.Response).Returns(new Mock<HttpResponse>().Object);
            //httpcontext.Setup(ctx => ctx.User).Returns(new Mock<ClaimsPrincipal>().Object);
            httpcontext.Setup(ctx => ctx.Session).Returns(new Mock<ISession>().Object);
            var items = new Dictionary<object, object>();
            items.Add(ApplicationConstants.WEBTOKEN, "");
            httpcontext.Setup(ctx => ctx.Items).Returns(items);
            return httpcontext.Object;

        }
        #region GetAcceptedCustomers
        [Fact]
        public void ValidateAcceptAccountRequest()
        {
            var controller = TestSetup(HttpStatusCode.OK);
            controller.ControllerContext.HttpContext = GetMockedHttpContext();
            var result = controller.AcceptConnection("100");
            var content = ((Microsoft.AspNetCore.Mvc.ObjectResult)result).Value;
            Assert.Equal(content, "https://portfoliomanager.energystar.gov/wstest/connect/account/100");

        }
        [Fact]
        public void ValidateAcceptAccountRequest_NoLoginInfo()
        {
            var controller = TestSetup(HttpStatusCode.OK);
            controller.ControllerContext.HttpContext = GetMockedHttpContextNoUserInfo();
            var result = controller.AcceptConnection("100");

            _moqLogger.Verify(l => l.Log<Object>(LogLevel.Error, It.IsAny<EventId>(), It.Is<Object>(o => o.ToString() == "AcceptConnection returned HttpStatusCode.Forbidden"), null, It.IsAny<Func<Object, Exception, String>>()), Times.AtLeastOnce);


        }
        [Fact]
        public void ValidateAcceptAccountRequest_BadRequest()
        {
            var controller = TestSetup(HttpStatusCode.BadRequest);
            controller.ControllerContext.HttpContext = GetMockedHttpContext();
            var result = controller.AcceptConnection("100");

            _moqLogger.Verify(l => l.Log<Object>(LogLevel.Error, It.IsAny<EventId>(), It.Is<Object>(o => o.ToString().Contains("AcceptConnection returned HttpStatusCode.BadRequest - ")), null, It.IsAny<Func<Object, Exception, String>>()), Times.AtLeastOnce);
        }
        #endregion

        [Fact]
        public void ValidateRejectAccountRequest()
        {
            var controller = TestSetup(HttpStatusCode.OK);
            controller.ControllerContext.HttpContext = GetMockedHttpContext();
            var result = controller.RejectConnection("100");
            var content = (((Microsoft.AspNetCore.Mvc.ObjectResult)result).Value);
            Assert.Equal(content, "https://portfoliomanager.energystar.gov/wstest/connect/account/100");

        }


        [Fact]
        public void ValidateGetPendingRequests()
        {
            var controller = TestSetup(HttpStatusCode.OK);
            controller.ControllerContext.HttpContext = GetMockedHttpContext();
            var result = controller.GetPendingAccounts();
            var content = (((Microsoft.AspNetCore.Mvc.ObjectResult)result).Value);
            Assert.Equal(content, "https://portfoliomanager.energystar.gov/wstest/connect/account/pending/list?");
        }

        #region GetPendingMeters
        [Fact]
        public void ValidateGetPendingMeters()
        {
            var controller = TestSetup(HttpStatusCode.OK);
            controller.ControllerContext.HttpContext = GetMockedHttpContext();
            var result = controller.GetPendingMeters();
            var content = (((Microsoft.AspNetCore.Mvc.ObjectResult)result).Value);
            Assert.Equal(content, "https://portfoliomanager.energystar.gov/wstest/share/meter/pending/list?");
        }
        [Fact]
        public void ValidateGetPendingMeters_NoLoginInfo()
        {
            var controller = TestSetup(HttpStatusCode.OK);
            controller.ControllerContext.HttpContext = GetMockedHttpContextNoUserInfo();
            var result = controller.GetPendingMeters();

            _moqLogger.Verify(l => l.Log<Object>(LogLevel.Error, It.IsAny<EventId>(), It.Is<Object>(o => o.ToString() == "GetPendingMeters returned HttpStatusCode.Forbidden"), null, It.IsAny<Func<Object, Exception, String>>()), Times.AtLeastOnce);


        }
        [Fact]
        public void ValidateGetPendingMeters_BadRequest()
        {
            var controller = TestSetup(HttpStatusCode.BadRequest);
            controller.ControllerContext.HttpContext = GetMockedHttpContext();
            var result = controller.GetPendingMeters();

            _moqLogger.Verify(l => l.Log<Object>(LogLevel.Error, It.IsAny<EventId>(), It.Is<Object>(o => o.ToString().Contains("GetPendingMeters returned HttpStatusCode.BadRequest")), null, It.IsAny<Func<Object, Exception, String>>()), Times.AtLeastOnce);
        }
        #endregion

        #region GetAcceptedCustomers
        [Fact]
        public void ValidateGetAcceptedCustomers()
        {
            var controller = TestSetup(HttpStatusCode.OK);
            controller.ControllerContext.HttpContext = GetMockedHttpContext();
            var result = controller.GetAcceptedAccounts();
            var content = (((Microsoft.AspNetCore.Mvc.ObjectResult)result).Value);
            Assert.Equal(content, "https://portfoliomanager.energystar.gov/wstest/customer/list?");


        }
        [Fact]
        public void ValidateGetAcceptedCustomers_NoLoginInfo()
        {
            var controller = TestSetup(HttpStatusCode.OK);
            controller.ControllerContext.HttpContext = GetMockedHttpContextNoUserInfo();
            var result = controller.GetAcceptedAccounts();

            _moqLogger.Verify(l => l.Log<Object>(LogLevel.Error, It.IsAny<EventId>(), It.Is<Object>(o => o.ToString() == "GetAcceptedAccounts returned HttpStatusCode.Forbidden"), null, It.IsAny<Func<Object, Exception, String>>()), Times.AtLeastOnce);


        }
        [Fact(Skip = "Work in progress")]
        public void ValidateGetAcceptedCustomers_BadRequest()
        {
            var controller = TestSetup(HttpStatusCode.BadRequest);
            controller.ControllerContext.HttpContext = GetMockedHttpContext();
            var result = controller.GetAcceptedAccounts();

            _moqLogger.Verify(l => l.Log<Object>(LogLevel.Error, It.IsAny<EventId>(), It.Is<Object>(o => o.ToString().Contains("GetAcceptedAccounts returned HttpStatusCode.BadRequest - ")), null, It.IsAny<Func<Object, Exception, String>>()), Times.AtLeastOnce);
        }
        #endregion

        #region AcceptedPropertyList
        [Fact]
        public void ValidateGetAcceptedConnections()
        {
            var controller = TestSetup(HttpStatusCode.OK);
            controller.ControllerContext.HttpContext = GetMockedHttpContext();
            var result = controller.AcceptedConnectionsList("0", "0", "1");
            var content = (((Microsoft.AspNetCore.Mvc.ObjectResult)result).Value);
            Assert.Equal(content, "https://portfoliomanager.energystar.gov/wstest/customer/list?");

        }
        [Fact]
        public void ValidateAcceptedConnectionsList_NoLoginInfo()
        {
            var controller = TestSetup(HttpStatusCode.OK);
            controller.ControllerContext.HttpContext = GetMockedHttpContextNoUserInfo();
            var result = controller.AcceptedConnectionsList("0", "0", "1");

            _moqLogger.Verify(l => l.Log<Object>(LogLevel.Error, It.IsAny<EventId>(), It.Is<Object>(o => o.ToString() == "AcceptedConnectionsList returned HttpStatusCode.Forbidden"), null, It.IsAny<Func<Object, Exception, String>>()), Times.AtLeastOnce);


        }
        [Fact(Skip = "Work in progress")]
        public void ValidateAcceptedConnectionsList_BadRequest()
        {
            var controller = TestSetup(HttpStatusCode.BadRequest);
            controller.ControllerContext.HttpContext = GetMockedHttpContext();
            var result = controller.AcceptedConnectionsList("0", "0", "1");

            _moqLogger.Verify(l => l.Log<Object>(LogLevel.Error, It.IsAny<EventId>(), It.Is<Object>(o => o.ToString().Contains("AcceptedConnectionsList returned HttpStatusCode.BadRequest - ")), null, It.IsAny<Func<Object, Exception, String>>()), Times.AtLeastOnce);
        }
        #endregion

        #region AcceptedPropertyList
        [Fact]
        public void ValidateGetAcceptedProperties()
        {
            var controller = TestSetup(HttpStatusCode.OK);
            controller.ControllerContext.HttpContext = GetMockedHttpContext();
            var result = controller.AcceptedPropertyList("0", "0", "1", "1");
            var content = (((Microsoft.AspNetCore.Mvc.ObjectResult)result).Value);
            Assert.Equal(content, "https://portfoliomanager.energystar.gov/wstest/account/1/property/list?accountId=1");

        }
        [Fact]
        public void ValidateGetAcceptedProperties_NoLoginInfo()
        {
            var controller = TestSetup(HttpStatusCode.OK);
            controller.ControllerContext.HttpContext = GetMockedHttpContextNoUserInfo();
            var result = controller.AcceptedPropertyList("0", "0", "1", "1");

            _moqLogger.Verify(l => l.Log<Object>(LogLevel.Error, It.IsAny<EventId>(), It.Is<Object>(o => o.ToString() == "AcceptedPropertyList returned HttpStatusCode.Forbidden"), null, It.IsAny<Func<Object, Exception, String>>()), Times.AtLeastOnce);


        }
        [Fact]
        public void ValidateGetAcceptedProperties_BadRequest()
        {
            var controller = TestSetup(HttpStatusCode.BadRequest,true);
            controller.ControllerContext.HttpContext = GetMockedHttpContext();
            var result = controller.AcceptedPropertyList("0", "0", "1", "1");
            //it shluld be bad request
            _moqLogger.Verify(l => l.Log<Object>(LogLevel.Error, It.IsAny<EventId>(), It.Is<Object>(o => o.ToString().Contains("AcceptedPropertyList returned HttpStatusCode.Forbidden")), null, It.IsAny<Func<Object, Exception, String>>()), Times.AtLeastOnce);


        }
        #endregion
        #region GetAcceptedMeters
        [Fact]
        public void ValidateGetAcceptedMeters()
        {
            var controller = TestSetup(HttpStatusCode.OK);
            controller.ControllerContext.HttpContext = GetMockedHttpContext();
            var result = controller.AcceptedMetersList("1");
            var content = (((Microsoft.AspNetCore.Mvc.ObjectResult)result).Value);
            Assert.Equal(content, "https://portfoliomanager.energystar.gov/wstest/property/1/meter/list?myAccessOnly=true&propertyId=1");
        }
        [Fact]
        public void ValidateGetAcceptedMeters_NoLoginInfo()
        {
            var controller = TestSetup(HttpStatusCode.OK);
            controller.ControllerContext.HttpContext = GetMockedHttpContextNoUserInfo();
            var result = controller.AcceptedMetersList("1");

            _moqLogger.Verify(l => l.Log<Object>(LogLevel.Error, It.IsAny<EventId>(), It.Is<Object>(o => o.ToString() == "AcceptedMetersList returned HttpStatusCode.Forbidden"), null, It.IsAny<Func<Object, Exception, String>>()), Times.AtLeastOnce);


        }
        [Fact(Skip = "Work in progress")]
        public void ValidateGetAcceptedMeters_BadRequest()
        {
            var controller = TestSetup(HttpStatusCode.BadRequest);
            controller.ControllerContext.HttpContext = GetMockedHttpContext();
            var result = controller.AcceptedMetersList("1");

            _moqLogger.Verify(l => l.Log<Object>(LogLevel.Error, It.IsAny<EventId>(), It.Is<Object>(o => o.ToString().Contains("AcceptedMetersList returned HttpStatusCode.BadRequest - ")), null, It.IsAny<Func<Object, Exception, String>>()), Times.AtLeastOnce);


        }
        #endregion
        [Fact(Skip = "Work in progress")]
        public void ValidateAcceptMeters()
        {
            var controller = TestSetup(HttpStatusCode.OK);
            controller.ControllerContext.HttpContext = GetMockedHttpContext();
            MeterMap meterMap = new MeterMap() { AccountId = "01000004504980", Customer = "100", PremiseId = "1130020252025" };
            var result = controller.AcceptMeter(meterMap);
            var content = (((Microsoft.AspNetCore.Mvc.ObjectResult)result).Value);
            Assert.Equal(content, "https://portfoliomanager.energystar.gov/wstest/share/meter/100");


        }

        #region RejectMeter

        [Fact]
        public void ValidateRejectMeter()
        {
            var controller = TestSetup(HttpStatusCode.OK);
            controller.ControllerContext.HttpContext = GetMockedHttpContext();
            MeterMap meterMap = new MeterMap() { AccountId = null, Customer = "100", PremiseId = null };
            var result = controller.RejectMeter(meterMap);
            var content = (((Microsoft.AspNetCore.Mvc.ObjectResult)result).Value);
            Assert.Equal(content, "https://portfoliomanager.energystar.gov/wstest/share/meter/100");


        }
        [Fact]
        public void ValidateRejectMeter_NoLoginInfo()
        {
            var controller = TestSetup(HttpStatusCode.OK);
            MeterMap meterMap = new MeterMap() { AccountId = null, Customer = "100", PremiseId = null };
            controller.ControllerContext.HttpContext = GetMockedHttpContextNoUserInfo();
            var result = controller.RejectMeter(meterMap);

            _moqLogger.Verify(l => l.Log<Object>(LogLevel.Error, It.IsAny<EventId>(), It.Is<Object>(o => o.ToString() == "RejectMeter/{meterId} returned HttpStatusCode.Forbidden"), null, It.IsAny<Func<Object, Exception, String>>()), Times.AtLeastOnce);


        }
        [Fact]
        public void ValidateRejectMeter_BadRequest()
        {
            var controller = TestSetup(HttpStatusCode.BadRequest);
            MeterMap meterMap = new MeterMap() { AccountId = null, Customer = "100", PremiseId = null };
            controller.ControllerContext.HttpContext = GetMockedHttpContext();
            var result = controller.RejectMeter(meterMap);

            _moqLogger.Verify(l => l.Log<Object>(LogLevel.Error, It.IsAny<EventId>(), It.Is<Object>(o => o.ToString().Contains("RejectMeter/{meterId} returned HttpStatusCode.BadRequest - ")), null, It.IsAny<Func<Object, Exception, String>>()), Times.AtLeastOnce);


        }
        #endregion

        #region RejectMeters

        [Fact]
        public void ValidateRejectMeters()
        {
            var controller = TestSetup(HttpStatusCode.OK);
            controller.ControllerContext.HttpContext = GetMockedHttpContext();
            MeterMap meterMap1 = new MeterMap() { AccountId = null, Customer = "100", PremiseId = null };
            MeterMap meterMap2 = new MeterMap() { AccountId = null, Customer = "101", PremiseId = null };
            var metermaplist = new List<MeterMap>();
            metermaplist.Add(meterMap1);
            metermaplist.Add(meterMap2);
            var result = controller.RejectMeters(metermaplist);
            var content = (((Microsoft.AspNetCore.Mvc.ObjectResult)result).Value);
            var meterMapResult = (List<Meter>) content;
            Assert.Equal(meterMapResult[0].meterId, "100");
            Assert.Equal(meterMapResult[1].meterId, "101");



        }
        [Fact]
        public void ValidateRejectMeters_NoLoginInfo()
        {
            var controller = TestSetup(HttpStatusCode.OK);
            MeterMap meterMap1 = new MeterMap() { AccountId = null, Customer = "100", PremiseId = null };
            MeterMap meterMap2 = new MeterMap() { AccountId = null, Customer = "101", PremiseId = null };
            var metermaplist = new List<MeterMap>();
            metermaplist.Add(meterMap1);
            metermaplist.Add(meterMap2);
            controller.ControllerContext.HttpContext = GetMockedHttpContextNoUserInfo();
            var result = controller.RejectMeters(metermaplist);

            _moqLogger.Verify(l => l.Log<Object>(LogLevel.Error, It.IsAny<EventId>(), It.Is<Object>(o => o.ToString() == "RejectMeters returned HttpStatusCode.Forbidden"), null, It.IsAny<Func<Object, Exception, String>>()), Times.AtLeastOnce);


        }
        [Fact]
        public void ValidateRejectMeters_BadRequest()
        {
            var controller = TestSetup(HttpStatusCode.BadRequest);
            MeterMap meterMap1 = new MeterMap() { AccountId = null, Customer = "100", PremiseId = null };
            MeterMap meterMap2 = new MeterMap() { AccountId = null, Customer = "101", PremiseId = null };
            var metermaplist = new List<MeterMap>();
            metermaplist.Add(meterMap1);
            metermaplist.Add(meterMap2);
            controller.ControllerContext.HttpContext = GetMockedHttpContext();
            var result = controller.RejectMeters(metermaplist);

            _moqLogger.Verify(l => l.Log<Object>(LogLevel.Error, It.IsAny<EventId>(), It.Is<Object>(o => o.ToString().Contains("RejectMeters returned HttpStatusCode.BadRequest")), null, It.IsAny<Func<Object, Exception, String>>()), Times.AtLeastOnce);


        }
        #endregion

        #region GetMeterDetails

        [Fact]
        public void ValidateGetMeterDetails()
        {
            var controller = TestSetup(HttpStatusCode.OK);
            controller.ControllerContext.HttpContext = GetMockedHttpContext();
            var result = controller.GetMeterDetails("100");
            var content = (((Microsoft.AspNetCore.Mvc.ObjectResult)result).Value);
            Assert.Equal(content, "https://portfoliomanager.energystar.gov/wstest/meter/100?meterId=100");


        }
        [Fact]
        public void ValidateGetMeterDetails_NoLoginInfo()
        {
            var controller = TestSetup(HttpStatusCode.OK);
            controller.ControllerContext.HttpContext = GetMockedHttpContextNoUserInfo();
            var result = controller.GetMeterDetails("100100");

            _moqLogger.Verify(l => l.Log<Object>(LogLevel.Error, It.IsAny<EventId>(), It.Is<Object>(o => o.ToString() == "GetMeterDetails returned HttpStatusCode.Forbidden"), null, It.IsAny<Func<Object, Exception, String>>()), Times.AtLeastOnce);


        }
        [Fact]
        public void ValidateGetMeterDetails_BadRequest()
        {
            var controller = TestSetup(HttpStatusCode.BadRequest);
            controller.ControllerContext.HttpContext = GetMockedHttpContext();
            var result = controller.GetMeterDetails("100100");

            _moqLogger.Verify(l => l.Log<Object>(LogLevel.Error, It.IsAny<EventId>(), It.Is<Object>(o => o.ToString().Contains("GetMeterDetails returned HttpStatusCode.BadRequest- ")), null, It.IsAny<Func<Object, Exception, String>>()), Times.AtLeastOnce);


        }
        [Fact]
        public void ValidateGetMeterDetails_othererror()
        {
            var controller = TestSetup(HttpStatusCode.BadGateway);
            controller.ControllerContext.HttpContext = GetMockedHttpContext();
            var result = controller.GetMeterDetails("100100");

            _moqLogger.Verify(l => l.Log<Object>(LogLevel.Error, It.IsAny<EventId>(), It.Is<Object>(o => o.ToString().Contains("GetMeterDetails returned HttpStatusCode.Forbidden")), null, It.IsAny<Func<Object, Exception, String>>()), Times.AtLeastOnce);

        }
        #endregion

        #region GetPropertyDetails

        [Fact]
        public void ValidateGetPropertyDetails()
        {
            var controller = TestSetup(HttpStatusCode.OK);
            controller.ControllerContext.HttpContext = GetMockedHttpContext();
            var result = controller.GetPropertyDetails("100100");
            var content = (((Microsoft.AspNetCore.Mvc.ObjectResult)result).Value);
            Assert.Equal(content, "https://portfoliomanager.energystar.gov/wstest/property/100100?propertyId=100100");

        }
        [Fact]
        public void ValidateGetPropertyDetails_NoLoginInfo()
        {
            var controller = TestSetup(HttpStatusCode.OK);
            controller.ControllerContext.HttpContext = GetMockedHttpContextNoUserInfo();
            var result = controller.GetPropertyDetails("100100");

            _moqLogger.Verify(l => l.Log<Object>(LogLevel.Error, It.IsAny<EventId>(), It.Is<Object>(o => o.ToString() == "GetPropertyDetails returned HttpStatusCode.Forbidden"), null, It.IsAny<Func<Object, Exception, String>>()), Times.AtLeastOnce);


        }
        [Fact]
        public void ValidateGetPropertyDetails_BadRequest()
        {
            var controller = TestSetup(HttpStatusCode.BadRequest);
            controller.ControllerContext.HttpContext = GetMockedHttpContext();
            var result = controller.GetPropertyDetails("100100");

            _moqLogger.Verify(l => l.Log<Object>(LogLevel.Error, It.IsAny<EventId>(), It.Is<Object>(o => o.ToString().Contains("GetPropertyDetails returned HttpStatusCode.BadRequest- ")), null, It.IsAny<Func<Object, Exception, String>>()), Times.AtLeastOnce);


        }
        [Fact]
        public void ValidateGetPropertyDetails_othererror()
        {
            var controller = TestSetup(HttpStatusCode.BadGateway);
            controller.ControllerContext.HttpContext = GetMockedHttpContext();
            var result = controller.GetPropertyDetails("100100");

            _moqLogger.Verify(l => l.Log<Object>(LogLevel.Error, It.IsAny<EventId>(), It.Is<Object>(o => o.ToString().Contains("GetPropertyDetails returned HttpStatusCode.Forbidden")), null, It.IsAny<Func<Object, Exception, String>>()), Times.AtLeastOnce);


        }
        #endregion

        #region GetAcceptedAccountDetails

        [Fact]
        public void ValidateGetAcceptedAccountDetails()
        {
            var controller = TestSetup(HttpStatusCode.OK);
            controller.ControllerContext.HttpContext = GetMockedHttpContext();
            var result = controller.GetAcceptedAccountDetails("100100");
            var content = (((Microsoft.AspNetCore.Mvc.ObjectResult)result).Value);
            Assert.Equal(content, "https://portfoliomanager.energystar.gov/wstest/customer/100100?accountId=100100");

        }
        [Fact]
        public void ValidateGetAcceptedAccountDetails_NoLoginInfo()
        {
            var controller = TestSetup(HttpStatusCode.OK);
            controller.ControllerContext.HttpContext = GetMockedHttpContextNoUserInfo();
            var result = controller.GetAcceptedAccountDetails("100100");

            _moqLogger.Verify(l => l.Log<Object>(LogLevel.Error, It.IsAny<EventId>(), It.Is<Object>(o => o.ToString() == "GetAcceptedAccountDetails returned HttpStatusCode.Forbidden"), null, It.IsAny<Func<Object, Exception, String>>()), Times.AtLeastOnce);


        }
        [Fact]
        public void ValidateGetAcceptedAccountDetails_BadRequest()
        {
            var controller = TestSetup(HttpStatusCode.BadRequest);
            controller.ControllerContext.HttpContext = GetMockedHttpContext();
            var result = controller.GetAcceptedAccountDetails("100100");

            _moqLogger.Verify(l => l.Log<Object>(LogLevel.Error, It.IsAny<EventId>(), It.Is<Object>(o => o.ToString().Contains("GetAcceptedAccountDetails returned HttpStatusCode.BadRequest- ")), null, It.IsAny<Func<Object, Exception, String>>()), Times.AtLeastOnce);


        }
        [Fact]
        public void ValidateGetAcceptedAccountDetails_othererror()
        {
            var controller = TestSetup(HttpStatusCode.BadGateway);
            controller.ControllerContext.HttpContext = GetMockedHttpContext();
            var result = controller.GetAcceptedAccountDetails("100100");

            _moqLogger.Verify(l => l.Log<Object>(LogLevel.Error, It.IsAny<EventId>(), It.Is<Object>(o => o.ToString().Contains("GetAcceptedAccountDetails returned HttpStatusCode.Forbidden")), null, It.IsAny<Func<Object, Exception, String>>()), Times.AtLeastOnce);


        }
        #endregion

        #region GetMeterDetailsList

        /* need more mocking  [Fact]
        public void ValidateGetMeterDetailsList()
        {
            var controller = TestSetup(HttpStatusCode.OK);
            controller.ControllerContext.HttpContext = GetMockedHttpContext();
            var result = controller.GetMeterDetailsList("100100");
            var content = (((Microsoft.AspNetCore.Mvc.ObjectResult)result).Value);
            Assert.Equal(content, "https://portfoliomanager.energystar.gov/wstest/customer/100100?accountId=100100");

        }*/
        [Fact]
        public void ValidateGetMeterDetailsList_NoLoginInfo()
        {
            var controller = TestSetup(HttpStatusCode.OK);
            controller.ControllerContext.HttpContext = GetMockedHttpContextNoUserInfo();
            var result = controller.GetMeterDetailsList("100100", DateTime.Now.ToString());

            _moqLogger.Verify(l => l.Log<Object>(LogLevel.Error, It.IsAny<EventId>(), It.Is<Object>(o => o.ToString() == "GetMeterDetailsList returned HttpStatusCode.Forbidden"), null, It.IsAny<Func<Object, Exception, String>>()), Times.AtLeastOnce);


        }
        [Fact]
        public void ValidateGetMeterDetailsList_BadRequest()
        {
            var controller = TestSetup(HttpStatusCode.BadRequest);
            controller.ControllerContext.HttpContext = GetMockedHttpContext();
            var result = controller.GetMeterDetailsList("100100", DateTime.Now.ToString());

            _moqLogger.Verify(l => l.Log<Object>(LogLevel.Error, It.IsAny<EventId>(), It.Is<Object>(o => o.ToString().Contains("GetMeterDetailsList returned HttpStatusCode.BadRequest- ")), null, It.IsAny<Func<Object, Exception, String>>()), Times.AtLeastOnce);


        }
        [Fact]
        public void ValidateGetMeterDetailsList_othererror()
        {
            var controller = TestSetup(HttpStatusCode.BadGateway);
            controller.ControllerContext.HttpContext = GetMockedHttpContext();
            var result = controller.GetMeterDetailsList("100100", DateTime.Now.ToString());

            _moqLogger.Verify(l => l.Log<Object>(LogLevel.Error, It.IsAny<EventId>(), It.Is<Object>(o => o.ToString().Contains("GetMeterDetailsList returned HttpStatusCode.Forbidden")), null, It.IsAny<Func<Object, Exception, String>>()), Times.AtLeastOnce);

        }
        #endregion

        #region GetCustomfields

        [Fact]
        public void ValidateGetCustomfields()
        {
            var controller = TestSetup(HttpStatusCode.OK);
            controller.ControllerContext.HttpContext = GetMockedHttpContext();
            var result = controller.GetCustomMeterfields("100100");
            var content = (((Microsoft.AspNetCore.Mvc.ObjectResult)result).Value);
            Assert.Equal(content, "https://portfoliomanager.energystar.gov/wstest/meter/100100/customFieldList?meterId=100100");

        }
        [Fact]
        public void ValidateGetCustomfields_NoLoginInfo()
        {
            var controller = TestSetup(HttpStatusCode.OK);
            controller.ControllerContext.HttpContext = GetMockedHttpContextNoUserInfo();
            var result = controller.GetCustomMeterfields("100100");

            _moqLogger.Verify(l => l.Log<Object>(LogLevel.Error, It.IsAny<EventId>(), It.Is<Object>(o => o.ToString() == "GetCustomfields returned HttpStatusCode.Forbidden"), null, It.IsAny<Func<Object, Exception, String>>()), Times.AtLeastOnce);


        }
        [Fact]
        public void ValidateGetCustomfields_BadRequest()
        {
            var controller = TestSetup(HttpStatusCode.BadRequest);
            controller.ControllerContext.HttpContext = GetMockedHttpContext();
            var result = controller.GetCustomMeterfields("100100");

            _moqLogger.Verify(l => l.Log<Object>(LogLevel.Error, It.IsAny<EventId>(), It.Is<Object>(o => o.ToString().Contains("GetCustomfields returned HttpStatusCode.BadRequest- ")), null, It.IsAny<Func<Object, Exception, String>>()), Times.AtLeastOnce);


        }
        [Fact]
        public void ValidateGetCustomfields_othererror()
        {
            var controller = TestSetup(HttpStatusCode.BadGateway);
            controller.ControllerContext.HttpContext = GetMockedHttpContext();
            var result = controller.GetCustomMeterfields("100100");

            _moqLogger.Verify(l => l.Log<Object>(LogLevel.Error, It.IsAny<EventId>(), It.Is<Object>(o => o.ToString().Contains("GetCustomfields returned HttpStatusCode.Forbidden")), null, It.IsAny<Func<Object, Exception, String>>()), Times.AtLeastOnce);

        }
        #endregion

        #region GetAccounts

        /* [Fact]
         public void ValidateGetAccounts()
         {
             var controller = TestSetup(HttpStatusCode.OK);
             controller.ControllerContext.HttpContext = GetMockedHttpContext();
             var result = controller.GetAccounts("100100");
             var content = (((Microsoft.AspNetCore.Mvc.ObjectResult)result).Value);
             Assert.Equal(content, "https://portfoliomanager.energystar.gov/wstest/account/100100/customFieldList?accountId=100100");

         }*/
        [Fact]
        public void ValidateGetAccounts_NoLoginInfo()
        {
            var controller = TestSetup(HttpStatusCode.OK);
            controller.ControllerContext.HttpContext = GetMockedHttpContextNoUserInfo();
            var result = controller.GetAccounts("100100");

            _moqLogger.Verify(l => l.Log<Object>(LogLevel.Error, It.IsAny<EventId>(), It.Is<Object>(o => o.ToString() == "GetAccounts returned HttpStatusCode.Forbidden"), null, It.IsAny<Func<Object, Exception, String>>()), Times.AtLeastOnce);


        }
        [Fact]
        public void ValidateGetAccounts_BadRequest()
        {
            var controller = TestSetup(HttpStatusCode.BadRequest);
            controller.ControllerContext.HttpContext = GetMockedHttpContext();
            var result = controller.GetAccounts("100100");

            _moqLogger.Verify(l => l.Log<Object>(LogLevel.Error, It.IsAny<EventId>(), It.Is<Object>(o => o.ToString().Contains("GetAccounts returned HttpStatusCode.BadRequest- ")), null, It.IsAny<Func<Object, Exception, String>>()), Times.AtLeastOnce);


        }
        [Fact]
        public void ValidateGetAccounts_othererror()
        {
            var controller = TestSetup(HttpStatusCode.BadGateway);
            controller.ControllerContext.HttpContext = GetMockedHttpContext();
            var result = controller.GetAccounts("100100");

            _moqLogger.Verify(l => l.Log<Object>(LogLevel.Error, It.IsAny<EventId>(), It.Is<Object>(o => o.ToString().Contains("GetAccounts returned HttpStatusCode.Forbidden")), null, It.IsAny<Func<Object, Exception, String>>()), Times.AtLeastOnce);

        }
        #endregion
    }
}
