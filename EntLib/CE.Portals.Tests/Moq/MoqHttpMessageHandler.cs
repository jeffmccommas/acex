﻿using CE.Portals.Integrations.Models;
using Newtonsoft.Json;
using System;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;

namespace CE.Portals.Tests.Moq
{
    public class MoqHttpMessageHandler :HttpMessageHandler
    {

        private Uri _Url;
        private string _ReturnContent;

        public MoqHttpMessageHandler(Uri Url, string ReturnContent)
        {
            _Url = Url;
            _ReturnContent = ReturnContent;
        }

        protected override async Task<HttpResponseMessage> SendAsync(HttpRequestMessage request, CancellationToken cancellationToken)
        {
            var responseMessage = new HttpResponseMessage(HttpStatusCode.NotFound);
            if (request.RequestUri.Host == _Url.Host)
            {
                responseMessage.StatusCode = HttpStatusCode.OK;
                var response = new ResponseModel() { StatusDescription = _ReturnContent, Content = _ReturnContent, StatusCode = HttpStatusCode.OK };

                responseMessage.Content = new StringContent(response.Content);

                //responseMessage.Content = JsonConvert.DeserializeObject(new Response() { StatusCode = HttpStatusCode.OK, Content = _ReturnContent, StatusDescription = _ReturnContent });
            }

            return await Task.FromResult(responseMessage);
        }
    }
}
