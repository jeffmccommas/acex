﻿using System;
using Xunit;
using CE.Portals.Integrations.WebServiceClient;
using CE.Portals.Tests.Moq;
using System.Net;

//How to use and run xunit tests for .net core:
//http://xunit.github.io/docs/getting-started-dotnet-core.html
namespace CE.Portals.Tests
{
    public class RestClientUnitTests
    {
        private string _testWebserviceBaseUrl;
        private RestClient _restClient;
        private MoqHttpMessageHandler _moqMessageHandler;

        public RestClientUnitTests()
        {
            //test setup
            Uri _uri = new Uri("http://myurl.com");
            _moqMessageHandler = new MoqHttpMessageHandler(_uri, "Unit Test Content");
            _restClient = new RestClient(_uri, null,null, _moqMessageHandler);           
        }
        [Fact]
        public void GetTest()
        {
            var response = _restClient.Get(null);
            Assert.Equal(response.Content, "Unit Test Content");
            Assert.Equal(HttpStatusCode.OK, response.StatusCode);
        }

        [Fact]
        public void PostTest()
        {
            var response = _restClient.Post();
            Assert.Equal(response.Content, "Unit Test Content");
            Assert.Equal(HttpStatusCode.OK, response.StatusCode);
        }

        [Fact]
        public void DeleteTest()
        {
            var response = _restClient.Delete();
            Assert.Equal(response.Content, "Unit Test Content");
            Assert.Equal(HttpStatusCode.OK, response.StatusCode);
        }

    }
}
