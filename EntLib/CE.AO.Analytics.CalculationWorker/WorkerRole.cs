using System;
using System.Diagnostics;
using System.Net;
using System.Threading;
using AO.Registrar;
using Microsoft.Azure;
using Microsoft.Azure.WebJobs;
using Microsoft.WindowsAzure.ServiceRuntime;
using Microsoft.Practices.Unity;

namespace CE.AO.Analytics.CalculationWorker
{
    public class WorkerRole : RoleEntryPoint
    {
        private readonly CancellationTokenSource _cancellationTokenSource = new CancellationTokenSource();
        private readonly ManualResetEvent _runCompleteEvent = new ManualResetEvent(false);

        public override void Run()
        {
            Trace.TraceInformation("CE.AO.Analytics.CalculationWorker is running");

            try
            {
                if (!_cancellationTokenSource.Token.IsCancellationRequested)
                {
                    new DataStorageRegistrar().Initialize<TransientLifetimeManager>();
                    new ContentModel.AutoMapperBootStrapper().BootStrap();

                    var storageConn = CloudConfigurationManager.GetSetting("AclaraOneStorageConnectionString");
                    var config = new JobHostConfiguration { NameResolver = new QueueNameResolver() };
                    config.Queues.BatchSize = 8; //(default is 16).
                    config.Queues.MaxDequeueCount = 5; //(default is 5).
                    config.Queues.MaxPollingInterval = TimeSpan.FromSeconds(15); //(default is 1 minute).
                    config.StorageConnectionString = storageConn;
                    config.DashboardConnectionString = storageConn;
                    config.FunctionTimeout = TimeSpan.MaxValue;

                    var host = new JobHost(config);
                    host.RunAndBlock();
                }

                //Wait for shutdown to be called, else the role will recycle
                _runCompleteEvent.WaitOne();
            }
            catch (Exception ex)
            {
                Trace.TraceInformation(ex.Message);
            }
            finally
            {
                _runCompleteEvent.Set();
            }
        }

        public override bool OnStart()
        {
            // Set the maximum number of concurrent connections
            ServicePointManager.DefaultConnectionLimit = 12;

            // For information on handling configuration changes
            // see the MSDN topic at http://go.microsoft.com/fwlink/?LinkId=166357.

            bool result = base.OnStart();

            Trace.TraceInformation("CE.AO.Analytics.CalculationWorker has been started");

            return result;
        }

        public override void OnStop()
        {
            Trace.TraceInformation("CE.AO.Analytics.CalculationWorker is stopping");

            _cancellationTokenSource.Cancel();
            _runCompleteEvent.WaitOne();

            base.OnStop();

            Trace.TraceInformation("CE.AO.Analytics.CalculationWorker has stopped");
        }
    }
}
