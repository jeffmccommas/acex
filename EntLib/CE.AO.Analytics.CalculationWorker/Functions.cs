﻿using CE.AO.Logging;
using Microsoft.Azure.WebJobs;
using Microsoft.Practices.Unity;
using System;
using System.IO;
using System.Linq;
using AO.BusinessContracts;
using AO.Registrar;
using CE.AO.Models;
using Enums = CE.AO.Utilities.Enums;

namespace CE.AO.Analytics.CalculationWorker
{
	/// <summary>
	/// Function class is used to process message in calculation queue
	/// </summary>
	public static class Functions
	{
		public static LogModel LogModel { get; set; }
		public static IUnityContainer UnityContainer { get; set; }

		/// <summary>
		/// To process the message in the AOCalculationQueue queue
		/// </summary>
		/// <param name="message">Message in the queue</param>
		/// <param name="log">Textwriter object</param>
		public static void ProcessCalculation([QueueTrigger("%AOCalculationQueue%")] string message, TextWriter log)
		{
			LogModel logModel = new LogModel();
			if (LogModel == null)
			{
				logModel.Module = Enums.Module.Calculation;
			}
			else
			{
				logModel = LogModel;
			}

			try
			{
				IUnityContainer unityContainer = new UnityContainer();
				if (UnityContainer != null)
					unityContainer = UnityContainer;
				else
					(new DataStorageRegistrar()).Initialize<TransientLifetimeManager>(unityContainer);

				string[] parameter = message.Split(new[] { "^^" }, StringSplitOptions.None).ToArray();

				if (parameter.Length.Equals(7))
				{
					var clientId = parameter[0];
					var meterId = parameter[1];
					var date = parameter[2];
					var fileName = parameter[3];
					var rowIndex = parameter[4];
					var metaData = parameter[5];
					var processingType = parameter[6];
					logModel.ClientId = clientId;
					logModel.MeterId = meterId;
					logModel.Source = fileName;
					logModel.RowIndex = rowIndex;
					logModel.Metadata = metaData;
					logModel.ProcessingType = (Enums.ProcessingType)Convert.ToInt32(processingType);

					var calculationFacade = unityContainer.Resolve<ICalculationFacade>(
					new ParameterOverride("logModel", logModel));

					var asOfDate = DateTime.Now;
					if (!string.IsNullOrEmpty(date))
						asOfDate = Convert.ToDateTime(date);

					calculationFacade.CalculateCostToDate(Convert.ToInt32(clientId), meterId, asOfDate).Wait();
				}
				else
					Logger.Fatal(
						$@"Queue message ""{message
							}"" received for calculation is not in proper format. Format should be ""ClientId^^MeterId^^AsOfDate^^FileName:RowIndex:MetaData:ProcessingType"".",
						logModel);
			}
			catch (Exception ex)
			{
				Logger.Error(ex, logModel);
				throw;
			}
		}

		/// <summary>
		/// To process the message in the AOScheduleCalculationQueue queue
		/// </summary>
		/// <param name="message">Message in the queue</param>
		/// <param name="log">Textwriter object</param>
		public static void ScheduledCalculation([QueueTrigger("%AOScheduleCalculationQueue%")] string message, TextWriter log)
		{
			LogModel logModel = new LogModel();
			if (LogModel == null)
			{
				logModel.Module = Enums.Module.Calculation;
			}
			else
			{
				logModel = LogModel;
			}

			try
			{
				IUnityContainer unityContainer = new UnityContainer();
				if (UnityContainer != null)
					unityContainer = UnityContainer;
				else
					(new DataStorageRegistrar()).Initialize<TransientLifetimeManager>(unityContainer);

				string[] parameter = message.Split(new[] { "^^" }, StringSplitOptions.None).ToArray();
				if (parameter.Length.Equals(2))
				{
					var clientId = parameter[0];
					var date = parameter[1];

					logModel.Source = "Scheduled";
					logModel.ClientId = clientId;
					logModel.ProcessingType = Enums.ProcessingType.Schedule;
					var asOfDate = DateTime.Now;
					if (!string.IsNullOrEmpty(date))
						asOfDate = Convert.ToDateTime(date);

					var calculationFacade = unityContainer.Resolve<ICalculationFacade>(
					new ParameterOverride("logModel", logModel));

					Logger.Info($"Scheduled Calculation Started for Client {clientId}", logModel);
					calculationFacade.ScheduledCalculation(Convert.ToInt32(clientId), asOfDate.ToShortDateString());
				}
				else
					Logger.Fatal(
						$@"Queue message ""{message
							}"" received for calculating during Schedule is not in proper format. Format should be ""ClientId^^AsOfDate"".",
						logModel);
			}
			catch (Exception ex)
			{
				Logger.Error(ex, logModel);
				throw;
			}
		}

		/// <summary>
		/// To process the message in the AoProcessScheduleCalculationQueue queue
		/// </summary>
		/// <param name="message">Message in the queue</param>
		/// <param name="log">Textwriter object</param>
		public static void ProcessScheduleCalculation([QueueTrigger("%AoProcessScheduleCalculationQueue%")] string message, TextWriter log)
		{
			LogModel logModel = new LogModel();
			if (LogModel == null)
			{
				logModel.Module = Enums.Module.Calculation;
			}
			else
			{
				logModel = LogModel;
			}

			try
			{
				IUnityContainer unityContainer = new UnityContainer();
				if (UnityContainer != null)
					unityContainer = UnityContainer;
				else
					(new DataStorageRegistrar()).Initialize<TransientLifetimeManager>(unityContainer);

				string[] parameter = message.Split(new[] { "^^" }, StringSplitOptions.None).ToArray();
				if (parameter.Length.Equals(4))
				{                    
					var accountId = parameter[0];
					var clientId = parameter[1];
					var customerId = parameter[2];
					var asOfDate = parameter[3];

					logModel.Source = "Scheduled";
					logModel.ClientId = clientId;
					logModel.ProcessingType = Enums.ProcessingType.Schedule;

					var currentDate = DateTime.Now;
					if (!string.IsNullOrEmpty(asOfDate))
						currentDate = Convert.ToDateTime(asOfDate);

					var calculationFacade = unityContainer.Resolve<ICalculationFacade>(
						new ParameterOverride("logModel", logModel));

					calculationFacade.CalculateBillToDate(Convert.ToInt32(clientId), accountId, customerId, currentDate).Wait();
				}
				else
					Logger.Fatal(
						$@"Queue message ""{message
							}"" received for processing schedule is not in proper format. Format should be ""AccountId^^ClientId^^CustomerId^^AsOfDate"".",
						logModel);
			}
			catch (Exception ex)
			{
				Logger.Error(ex, logModel);
				throw;
			}
		}
	}
}