﻿using System.Threading.Tasks;
using CE.AO.Models;

namespace CE.AO.Analytics.CalculationWorker.Calculation
{
    public abstract class CalculationProcess
    {
        protected CalculationProcess(LogModel logModel)
        {
            LogModel = logModel;
        }
        public LogModel LogModel { get; set; }
        public string ClientId { get; set; }
        public string MeterId { get; set; }
        public abstract Task<bool> Process(string clientId, string meterId, LogModel logModel);
    }
}
