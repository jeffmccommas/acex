﻿using System;
using NLog;
using NLog.Targets;

// ReSharper disable once CheckNamespace
namespace CE.InsightsDW.ImportData.Infastructure
{
    public class DWImportLogger :ILogger
    {
        private readonly Logger _logger = LogManager.GetCurrentClassLogger();
        private const string PropTrackingId = "TrackingId";
        private const string PropClientId = "ClientId";

        public string TrackingId { get; set; }

        public int ClientId { get; set; }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="logFileName">The log filename</param>
        /// <param name="trackingId">The TrackingId</param>
        /// <param name="clientId">The clientId</param>
        public DWImportLogger(string logFileName, string trackingId, int clientId)
        {
            var target = LogManager.Configuration.FindTargetByName("textFile") as FileTarget;
            if (target != null)
            {
                target.FileName = logFileName;
            }
            TrackingId = trackingId;
            ClientId = clientId;
        }

        public void SetName(string logFileName)
        {
            var target = LogManager.Configuration.FindTargetByName("textFile") as FileTarget;
            if (target != null)
            {
                target.FileName = logFileName;
            }
        }

        public string GetName()
        {
            var target = LogManager.Configuration.FindTargetByName("textFile") as FileTarget;
            if (target != null)
            {
                return target.FileName.ToString();
            }

            return string.Empty;
        }

        private void LogEvent(LogLevel logLevel, string message)
        {
            var theEvent = new LogEventInfo(logLevel, "", message);
            theEvent.Properties[PropTrackingId] = TrackingId;
            theEvent.Properties[PropClientId] = ClientId;
            _logger.Log(theEvent);
        }

        private void LogEventException(LogLevel logLevel, Exception ex)
      {
          var theEvent = new LogEventInfo(logLevel, "", ex.ToString()) {Exception = ex};
          theEvent.Properties[PropTrackingId] = TrackingId;
          theEvent.Properties[PropClientId] = ClientId;
          _logger.Log(theEvent);
        }

      public void Info(string trackingid,string message)
        {
            LogEvent(LogLevel.Info, message);
        }

        public void Warn(string trackingid,string message)
        {
            throw new NotImplementedException();
        }

        public void Debug(string trackingid,string message)
        {
            LogEvent(LogLevel.Debug, message);
        }

        public void Error(string trackingid,string message)
        {
            LogEvent(LogLevel.Error, message);

        }

        public void ErrorException(string trackingid,Exception ex)
        {
            LogEventException(LogLevel.Error, ex);
        }

        public void Fatal(string trackingid,string message)
        {
            throw new NotImplementedException();
        }
    }
}
