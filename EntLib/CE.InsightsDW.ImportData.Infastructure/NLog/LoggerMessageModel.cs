﻿using System;
using NLog;

// ReSharper disable once CheckNamespace
namespace CE.InsightsDW.ImportData.Infastructure
{
    public class LoggerMessageModel
    {
        public LogLevel LogLevel { get; set; }

        public string MessageToLog { get; set; }

        public Exception Exception { get; set; }

    }
}
