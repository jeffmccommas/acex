﻿using System.Xml.Linq;

// ReSharper disable once CheckNamespace
namespace CE.InsightsDW.ImportData.Infastructure
{
    public static class LinqHelper
    {
        /// <summary>
        /// Determine if an XElement's value is null. If it is return an empty string.
        /// </summary>
        /// <param name="element">The XElement</param>
        /// <returns>The value.</returns>
        public static string ElementValueNull(this XElement element)
        {
            return element?.Value ?? string.Empty;
        }
    }
}
