using System;
using System.Collections.Generic;
using CE.InsightsDW.Model;

namespace CE.InsightsDW.ImportData.Infastructure.SharedClasses
{
    /// <summary>
    /// Compare Premise Attributes
    /// </summary>
    public class DistinctItemComparer_PremiseAttribute : IEqualityComparer<Holding_PremiseAttribute>
    {
        public bool Equals(Holding_PremiseAttribute xAttribute, Holding_PremiseAttribute yAttribute)
        {
            return xAttribute.ClientID == yAttribute.ClientID &&
                   xAttribute.CustomerID.Equals(yAttribute.CustomerID, StringComparison.InvariantCultureIgnoreCase) &&
                   xAttribute.AccountID.Equals(yAttribute.AccountID, StringComparison.InvariantCultureIgnoreCase) &&
                   xAttribute.PremiseID.Equals(yAttribute.PremiseID, StringComparison.InvariantCultureIgnoreCase) &&
                   xAttribute.AttributeKey.Equals(yAttribute.AttributeKey, StringComparison.InvariantCultureIgnoreCase);
        }

        public int GetHashCode(Holding_PremiseAttribute attribute)
        {
            return attribute.ClientID.GetHashCode() ^
                   attribute.CustomerID.GetHashCode() ^
                   attribute.AccountID.GetHashCode() ^
                   attribute.PremiseID.GetHashCode() ^
                   attribute.AttributeKey.GetHashCode();
        }
    }

    /// <summary>
    /// Compare Customers
    /// </summary>
    public class DistinctItemComparer_Customer : IEqualityComparer<Holding_Customer>
    {

        public bool Equals(Holding_Customer xCustomer, Holding_Customer yCustomer)
        {
            return xCustomer.ClientID == yCustomer.ClientID &&
                   xCustomer.CustomerID.Equals(yCustomer.CustomerID, StringComparison.InvariantCultureIgnoreCase);
        }

        public int GetHashCode(Holding_Customer customer)
        {
            return customer.ClientID.GetHashCode() ^
                   customer.CustomerID.GetHashCode();
        }
    }

    /// <summary>
    /// Compare Premises
    /// </summary>
    public class DistinctItemComparer_Premise : IEqualityComparer<Holding_Premise>
    {

        public bool Equals(Holding_Premise xPremise, Holding_Premise yPremise)
        {
            return xPremise.ClientID == yPremise.ClientID &&
                   xPremise.PremiseID.Equals(yPremise.PremiseID, StringComparison.InvariantCultureIgnoreCase) &&
                   xPremise.AccountID.Equals(yPremise.AccountID, StringComparison.InvariantCultureIgnoreCase);
        }

        public int GetHashCode(Holding_Premise premise)
        {
            return premise.ClientID.GetHashCode() ^
                   premise.CustomerID.GetHashCode();
        }
    }

    /// <summary>
    /// Compare Bills
    /// </summary>
    public class DistinctItemComparer_Billing : IEqualityComparer<Holding_Billing>
    {

        public bool Equals(Holding_Billing xBilling, Holding_Billing yBilling)
        {
            return xBilling.ClientId == yBilling.ClientId &&
                   xBilling.PremiseId.Equals(yBilling.PremiseId, StringComparison.InvariantCultureIgnoreCase) &&
                   xBilling.AccountId.Equals(yBilling.AccountId, StringComparison.InvariantCultureIgnoreCase) &&
                   xBilling.ServiceContractId.Equals(yBilling.ServiceContractId, StringComparison.InvariantCultureIgnoreCase) &&
                   xBilling.BillStartDate.Equals(yBilling.BillStartDate) &&
                   xBilling.BillEndDate.Equals(yBilling.BillEndDate) &&
                   xBilling.CommodityId.Equals(yBilling.CommodityId) &&
                   xBilling.UOMId.Equals(yBilling.UOMId);
        }

        public int GetHashCode(Holding_Billing billing)
        {
            return billing.ClientId.GetHashCode() ^
                   billing.PremiseId.GetHashCode() ^
                   billing.AccountId.GetHashCode() ^
                   billing.ServiceContractId.GetHashCode() ^
                   billing.BillStartDate.GetHashCode() ^
                   billing.BillEndDate.GetHashCode() ^
                   billing.CommodityId.GetHashCode() ^
                   billing.UOMId.GetHashCode();
        }
    }

    /// <summary>
    /// Compare Bills
    /// </summary>
    public class DistinctItemComparer_ActionItem : IEqualityComparer<Holding_ActionItem>
    {

        public bool Equals(Holding_ActionItem xActionItem, Holding_ActionItem yActionItem)
        {
            return xActionItem.ClientId == yActionItem.ClientId &&
                   xActionItem.PremiseId.Equals(yActionItem.PremiseId, StringComparison.InvariantCultureIgnoreCase) &&
                   xActionItem.AccountID.Equals(yActionItem.AccountID, StringComparison.InvariantCultureIgnoreCase) &&
                   xActionItem.ActionKey.Equals(yActionItem.ActionKey, StringComparison.InvariantCultureIgnoreCase) &&
                   xActionItem.SubActionKey.Equals(yActionItem.SubActionKey,
                       StringComparison.InvariantCultureIgnoreCase) &&
                   xActionItem.StatusId.Equals(yActionItem.StatusId) &&
                   xActionItem.StatusDate.Equals(yActionItem.StatusDate);
        }

        public int GetHashCode(Holding_ActionItem actionItem)
        {
            return actionItem.ClientId.GetHashCode() ^
                   actionItem.PremiseId.GetHashCode() ^
                   actionItem.AccountID.GetHashCode() ^
                   actionItem.ActionKey.GetHashCode() ^
                   actionItem.SubActionKey.GetHashCode() ^
                   actionItem.StatusId.GetHashCode() ^
                   actionItem.StatusDate.GetHashCode();
        }
    }

}