﻿using System;
using System.Runtime.Serialization;

// ReSharper disable once CheckNamespace
namespace CE.InsightsDW.ImportData.Infastructure
{
        [Serializable]
    public class InsightsDWImportException : Exception
    {

            #region "Public Constructors"

            public InsightsDWImportException()
            {
            }

            public InsightsDWImportException(string message)
                : base(message)
            {
            }

            public InsightsDWImportException(string message, Exception inner)
                : base(message, inner)
            {
            }

            #endregion

            #region "Protected Constructors"

            protected InsightsDWImportException(SerializationInfo info, StreamingContext context)
                : base(info, context)
            {
            }

            #endregion


    
    }
}
