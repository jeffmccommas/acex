﻿using System;
using System.Configuration;
using System.Linq;
using System.Xml.Linq;

// ReSharper disable once CheckNamespace
namespace CE.InsightsDW.ImportData.Infastructure
{
    public class AppConfiguration : IConfig
    {
        private readonly int _clientId;
        private string _environment;

        #region Constants ############################################################

        // Generic
        private const string ComponentName = "InsightsDW.ImportData";

        private const string XmlRoot = "InsightsDW.ImportData";

        private const string ConfigFileworkingextension = "FileWorkingExtension";
        private const string ConfigFiledoneextension = "FileDoneExtension";

        private const string LogFileExtension = "FileOutputExtension";

        private const string ConfigEnvironment = "Environment";
        private const string MaxBatchsize = "MaxBatchSize";
        private const string ParallelismStr = "Parallelism";

        private const string FileCompleteWitherrorsExt = "FileDoneWithErrorsExtension";
        private const string ErrorLogFile = "ErrorLogFile";
        private const string Importspecificloggingon = "ImportSpecificLoggingOn";

        private const string Shard = "Shard";
        private const string EtlAvoidStartTime = "ETLAvoidanceStartTime";
        private const string EtlAvoidEndTime = "ETLAvoidanceEndTime";

        // Customer
        private const string XmlCustomerFilesearchpattern = "CustomerFileSearchPattern";
        private const string XmlCustomerFilepath = "CustomerFilePath";
        private const string XmlCustomerArchivePath = "CustomerArchivePath";
        private const string XmlCustomerXsd = "CustomerXSD";

        // Billing
        private const string XmlBillFilesearchpattern = "BillFileSearchPattern";
        private const string XmlBillFilepath = "BillFilePath";
        private const string XmlBillArchivePath = "BillArchivePath";
        private const string XmlBillingXsd = "BillingXSD";

        // Profile
        private const string XmlProfileFilesearchpattern = "ProfileFileSearchPattern";
        private const string XmlProfileFilepath = "ProfileFilePath";
        private const string XmlProfileArchivePath = "ProfileArchivePath";
        private const string XmlProfileXsd = "ProfileXSD";

        // ActionItem
        private const string XmlActionitemFilesearchpattern = "ActionItemFileSearchPattern";
        private const string XmlActionitemFilepath = "ActionItemFilePath";
        private const string XmlActionitemArchivePath = "ActionItemArchivePath";
        private const string XmlActionitemXsd = "ActionItemXSD";

        #endregion Constants

        public string ErrorLog { get; set; }

        public string LogFileOutputExtension { get; set; }

        private bool _importSpecificLoggingOn;

        public bool ImportSpecificLoggingOn
        {
            get { return _importSpecificLoggingOn; }
            set { _importSpecificLoggingOn = value; }
        }

        public string InputFilePath { get; set; }

        public string ArchivePath { get; set; }

        public string FileSearchPattern { get; set; }

        public string FileWorkingExtension { get; set; }

        public int MaxBatchSize { get; set; }

        public string FileDoneExtension { get; set; }

        public string InputXSD { get; set; }

        public string FileErrorExtension { get; set; }

        public int Parallelism { get; set; }

        public string ShardName { get; set; }

        public DateTime EtlAvoidanceStartTime { get; set; }

        public DateTime EtlAvoidanceEndTime { get; set; }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="clientId"></param>
        public AppConfiguration(int clientId)
        {
            _clientId = clientId;
        }


        /// <summary>
        /// Load the application settings
        /// </summary>
        private void LoadAppSettings()
        {
            var appSettings = ConfigurationManager.AppSettings;

            _environment = appSettings[ConfigEnvironment];

            FileDoneExtension = appSettings[ConfigFiledoneextension];
            FileWorkingExtension = appSettings[ConfigFileworkingextension];
            MaxBatchSize = Convert.ToInt32(appSettings[MaxBatchsize]);
            FileErrorExtension = appSettings[FileCompleteWitherrorsExt];
            Parallelism = Convert.ToInt32(appSettings[ParallelismStr]);
        }

        /// <summary>
        /// Load the generic configuration settings
        /// </summary>
        /// <param name="xdoc"></param>
        private void GetGenericConfiguration(XContainer xdoc)
        {
            // Error log
            ErrorLog = xdoc.Elements(XmlRoot)
                .Select(x => new { x, o = x.Element(ErrorLogFile) })
                .Where(t => t.o != null)
                .Select(t => t.o.Value).ToList().FirstOrDefault();

            // Shard Name
            ShardName = xdoc.Elements(XmlRoot)
                .Select(x => new {x, xElement = x.Element(Shard)})
                .Where(t => t.xElement != null)
                .Select(t => t.xElement.Value).ToList().FirstOrDefault();

            // ETL Avoidance start time.
            var etlAvoidanceStartTimeTemp = xdoc.Elements(XmlRoot)
                .Select(x => new {x, xElement = x.Element(EtlAvoidStartTime)})
                .Where(t => t.xElement != null)
                .Select(t => t.xElement.Value).ToList().FirstOrDefault();

            DateTime etlAvoidanceStartTimeDt;
            if (!string.IsNullOrEmpty(etlAvoidanceStartTimeTemp) &&
                DateTime.TryParse(etlAvoidanceStartTimeTemp, out etlAvoidanceStartTimeDt))
            {
                EtlAvoidanceStartTime = etlAvoidanceStartTimeDt;
            }
            else
            {
                EtlAvoidanceStartTime = DateTime.MinValue;
            }

            // ETL Avoidance end time.
            var etlAvoidanceEndTimeTemp = xdoc.Elements(XmlRoot)
                .Select(x => new {x, xElement = x.Element(EtlAvoidEndTime)})
                .Where(t => t.xElement != null)
                .Select(t => t.xElement.Value).ToList().FirstOrDefault();

            DateTime etlAvoidanceEndTimeDt;
            if (!string.IsNullOrEmpty(etlAvoidanceEndTimeTemp) &&
                DateTime.TryParse(etlAvoidanceEndTimeTemp, out etlAvoidanceEndTimeDt))
            {
                EtlAvoidanceEndTime = etlAvoidanceEndTimeDt;
            }
            else
            {
                EtlAvoidanceEndTime = DateTime.MinValue;
            }

            // Log File Output Extension
            LogFileOutputExtension = xdoc.Elements(XmlRoot)
                .Select(x => new { x, o = x.Element(LogFileExtension) })
                .Where(t => t.o != null)
                .Select(t => t.o.Value).ToList().FirstOrDefault();

            // Turn logging on?
            var loggingon = xdoc.Elements(XmlRoot).
                Select(x => x.Element(Importspecificloggingon).
                    ElementValueNull()).FirstOrDefault();

            if (!bool.TryParse(loggingon, out _importSpecificLoggingOn))
            {
                _importSpecificLoggingOn = true;
            }
        }

        /// <summary>
        /// Load the Customer configuration
        /// </summary>
        public void LoadCustomerConfiguration()
        {
            var config = new InsightsMetadata();
            string xmlConfig;

            //load settings from appconfig
            LoadAppSettings();

            //Load Settings from InsightsMetadata
            config.GetAppConfiguration(_clientId, ComponentName, _environment, out xmlConfig);

            if (string.IsNullOrEmpty(xmlConfig))
            {
                throw new ConfigurationErrorsException($"Customer Configuration is missing for client {_clientId}");
            }

            // Load the configuration settings in InsightsMetadata
            var xdoc = XDocument.Parse(xmlConfig);

            GetGenericConfiguration(xdoc);

            InputFilePath = xdoc.Elements(XmlRoot)
                .Select(x => new {x, xElement = x.Element(XmlCustomerFilepath)})
                .Where(t => t.xElement != null)
                .Select(t => t.xElement.Value).ToList().FirstOrDefault();

            FileSearchPattern = xdoc.Elements(XmlRoot)
                .Select(x => new {x, element = x.Element(XmlCustomerFilesearchpattern)})
                .Where(t => t.element != null)
                .Select(t => t.element.Value).ToList().FirstOrDefault();

            ArchivePath = xdoc.Elements(XmlRoot)
                .Select(x => new {x, xElement1 = x.Element(XmlCustomerArchivePath)})
                .Where(t => t.xElement1 != null)
                .Select(t => t.xElement1.Value).ToList().FirstOrDefault();

            InputXSD = xdoc.Elements(XmlRoot)
                .Select(x => x.Element(XmlCustomerXsd))
                .Where(element1 => element1 != null)
                .Select(element1 => element1.Value).ToList().FirstOrDefault();
        }

        /// <summary>
        /// Load the Billing configuration
        /// </summary>
        public void LoadBillingConfiguration()
        {

            var config = new InsightsMetadata();
            string xmlConfig;

            //load settings from appconfig
            LoadAppSettings();

            //Load Settings from InsightsMetadata
            config.GetAppConfiguration(_clientId, ComponentName, _environment, out xmlConfig);
            if (string.IsNullOrEmpty(xmlConfig))
            {
                throw new ConfigurationErrorsException($"Billing Configuration is missing for client {_clientId}");
            }

            // Load the configuration settings in InsightsMetadata
            var xdoc = XDocument.Parse(xmlConfig);

            GetGenericConfiguration(xdoc);

            InputFilePath = xdoc.Elements(XmlRoot)
                .Select(x => new {x, xElement = x.Element(XmlBillFilepath)})
                .Where(t => t.xElement != null)
                .Select(t => t.xElement.Value).ToList().FirstOrDefault();

            FileSearchPattern = xdoc.Elements(XmlRoot)
                .Select(x => new {x, element = x.Element(XmlBillFilesearchpattern)})
                .Where(t => t.element != null)
                .Select(t => t.element.Value).ToList().FirstOrDefault();

            ArchivePath = xdoc.Elements(XmlRoot)
                .Select(x => new {x, xElement1 = x.Element(XmlBillArchivePath)})
                .Where(t => t.xElement1 != null)
                .Select(t => t.xElement1.Value).ToList().FirstOrDefault();

            InputXSD = xdoc.Elements(XmlRoot)
                .Select(x => new {x, element1 = x.Element(XmlBillingXsd)})
                .Where(t => t.element1 != null)
                .Select(t => t.element1.Value).ToList().FirstOrDefault();
        }

        /// <summary>
        /// Load the Billing configuration
        /// </summary>
        public void LoadProfileConfiguration()
        {
            var config = new InsightsMetadata();
            string xmlConfig;

            //load settings from appconfig
            LoadAppSettings();

            //Load Settings from InsightsMetadata
            config.GetAppConfiguration(_clientId, ComponentName, _environment, out xmlConfig);

            if (string.IsNullOrEmpty(xmlConfig))
            {
                throw new ConfigurationErrorsException($"Profile Configuration is missing for client {_clientId}");
            }

            // Load the configuration settings in InsightsMetadata
            var xdoc = XDocument.Parse(xmlConfig);

            GetGenericConfiguration(xdoc);

            InputFilePath = xdoc.Elements(XmlRoot)
                .Select(x => new {x, xElement = x.Element(XmlProfileFilepath)})
                .Where(t => t.xElement != null)
                .Select(t => t.xElement.Value).ToList().FirstOrDefault();

            FileSearchPattern = xdoc.Elements(XmlRoot)
                .Select(x => x.Element(XmlProfileFilesearchpattern))
                .Where(element => element != null)
                .Select(element => element.Value).ToList().FirstOrDefault();

            ArchivePath = xdoc.Elements(XmlRoot)
                .Select(x => new {x, xElement1 = x.Element(XmlProfileArchivePath)})
                .Where(t => t.xElement1 != null)
                .Select(t => t.xElement1.Value).ToList().FirstOrDefault();

            InputXSD = xdoc.Elements(XmlRoot)
                .Select(x => new {x, element1 = x.Element(XmlProfileXsd)})
                .Where(t => t.element1 != null)
                .Select(t => t.element1.Value).ToList().FirstOrDefault();
        }

        /// <summary>
        /// Load the Action Item configuration
        /// </summary>
        public void LoadActionItemConfiguration()
        {
            var config = new InsightsMetadata();
            string xmlConfig;

            //load settings from appconfig
            LoadAppSettings();

            //Load Settings from InsightsMetadata
            config.GetAppConfiguration(_clientId, ComponentName, _environment, out xmlConfig);

            if (string.IsNullOrEmpty(xmlConfig))
            {
                throw new ConfigurationErrorsException($"Action Item Configuration is missing for client {_clientId}");
            }

            // Load the configuration settings in InsightsMetadata
            var xdoc = XDocument.Parse(xmlConfig);

            GetGenericConfiguration(xdoc);

            InputFilePath = xdoc.Elements(XmlRoot)
                .Select(x => new {x, xElement = x.Element(XmlActionitemFilepath)})
                .Where(t => t.xElement != null)
                .Select(t => t.xElement.Value).ToList().FirstOrDefault();

            FileSearchPattern = xdoc.Elements(XmlRoot)
                .Select(x => new {x, element = x.Element(XmlActionitemFilesearchpattern)})
                .Where(t => t.element != null)
                .Select(t => t.element.Value).ToList().FirstOrDefault();

            ArchivePath = xdoc.Elements(XmlRoot)
                .Select(x => new {x, xElement1 = x.Element(XmlActionitemArchivePath)})
                .Where(t => t.xElement1 != null)
                .Select(t => t.xElement1.Value).ToList().FirstOrDefault();

            InputXSD = xdoc.Elements(XmlRoot)
                .Select(x => new {x, element1 = x.Element(XmlActionitemXsd)})
                .Where(t => t.element1 != null)
                .Select(t => t.element1.Value).ToList().FirstOrDefault();
        }
    }
}
