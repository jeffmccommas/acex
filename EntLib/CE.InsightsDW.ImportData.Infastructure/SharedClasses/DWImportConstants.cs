﻿ // ReSharper disable once CheckNamespace
namespace CE.InsightsDW.ImportData.Infastructure
{
    public class DWImportConstants
    {
        public enum RunType
        {
            Customer,
            Bill,
            Profile,
            ActionItem
        }

        public const string Premiseid = "PremiseId";
        public const string Accountid = "AccountId";
        public const string Customerid = "CustomerId";
        public const string Trackingid = "TrackingId";
        public const string Clientid = "ClientId";
        public const string Actionkey = "ActionKey";

        //values for Profile Data
        public const string Attributekey = "AttributeKey";
        public const string Attributevalue = "AttributeValue";

        //Values for Billing Data
        public const string Servicepointid = "ServicePointId";
        public const string Startdate = "StartDate";
        public const string Enddate = "EndDate";

    }
}
