﻿using System;
using System.Data.Entity;
using System.Data.Entity.Core.EntityClient;
using System.Data.SqlClient;

// ReSharper disable once CheckNamespace
namespace CE.InsightsDW.ImportData.Infastructure
{
    /// <summary>
    /// Modify the connection string for Entity Framework usage.
    /// </summary>
    public static class ConnectionTools
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="source"></param>
        /// <param name="initialCatalog"></param>
        public static void ChangeDatabase(this DbContext source, string initialCatalog = "")
        {
            try
            {
                // get the connection string name.
                var configNameEf = source.GetType().Name;

                // add a reference to System.Configuration
                var entityCnxStringBuilder =
                    new EntityConnectionStringBuilder(
                        System.Configuration.ConfigurationManager.ConnectionStrings[configNameEf].ConnectionString);

                // init the sqlbuilder with the full EF connectionstring cargo
                var sqlCnxStringBuilder = new SqlConnectionStringBuilder
                    (entityCnxStringBuilder.ProviderConnectionString);

                // only populate parameters with values if added
                if (!string.IsNullOrEmpty(initialCatalog))
                {
                    sqlCnxStringBuilder.InitialCatalog = initialCatalog;
                }

                // now flip the properties that were changed
                source.Database.Connection.ConnectionString
                    = sqlCnxStringBuilder.ConnectionString;
            }
            catch (Exception eX)
            {
                var san = eX.Message;
                // set log item if required
            }
        }
    }
}
