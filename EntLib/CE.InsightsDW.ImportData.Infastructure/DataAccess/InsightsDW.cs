﻿using System;
using System.Collections.Generic;
using System.Linq;
using CE.InsightsDW.ImportData.Infastructure.SharedClasses;
using CE.InsightsDW.Model;

// ReSharper disable once CheckNamespace
namespace CE.InsightsDW.ImportData.Infastructure
{
    public class InsightsDW : IInsightsDWAccess
    {

        private readonly string _shardName;

        public InsightsDW(string shardName = "InsightsDW_Test")
        {
            _shardName = shardName;
        }

        /// <summary>
        /// Get the InsightsDW dbContext.
        /// </summary>
        /// <returns>A DBcontext</returns>
        private InsightsDWEntities GetInsightsDwEntities()
        {
            var insightsDwEntities = new InsightsDWEntities();
            insightsDwEntities.ChangeDatabase(_shardName);
            return insightsDwEntities;
        }

        private static List<DimUOM> ValidUoms { get; set; }
        /// <summary>
        /// Determine if a specified UOMId is valid.
        /// </summary>
        /// <param name="uomId">The UOMId</param>
        /// <returns>True, if valid.</returns>
        public bool IsValidUom(int uomId)
        {
            if (ValidUoms == null || !ValidUoms.Any())
            {
                ValidUoms = GetInsightsDwEntities().DimUOMs.ToList();
            }

            return ValidUoms.Count(m => m.UOMId == uomId) != 0;
        }

        private static List<DimCommodity> ValidCommodities { get; set; }
        /// <summary>
        /// Determine if a specified CommodityId is valid.
        /// </summary>
        /// <param name="commodityId">The CommodityId</param>
        /// <returns>True, if valid.</returns>

        public bool IsValidCommodity(int commodityId)
        {
            if (ValidCommodities == null || !ValidCommodities.Any())
            {
                ValidCommodities = GetInsightsDwEntities().DimCommodities.ToList();
            }

            return ValidCommodities.Count(m => m.CommodityId == commodityId) != 0;
        }

        private static List<DimBillPeriodType> ValidDimBillPeriodType { get; set; }
        /// <summary>
        /// Determine if a specified BillPeriodTypeId is valid.
        /// </summary>
        /// <param name="billPeriodTypeId">The BillPeriodTypeId</param>
        /// <returns>True, if valid.</returns>

        public bool IsValidBillPeriod(int billPeriodTypeId)
        {
            if (ValidDimBillPeriodType == null || !ValidDimBillPeriodType.Any())
            {
                ValidDimBillPeriodType = GetInsightsDwEntities().DimBillPeriodTypes.ToList();
            }

            return ValidDimBillPeriodType.Count(m => m.BillPeriodTypeId == billPeriodTypeId) != 0;
        }

        private static List<DimMeterType> ValidDimMeterTypes { get; set; }
        /// <summary>
        /// Determine if a specified MeterType is valid.
        /// </summary>
        /// <param name="meterType">The MeterType</param>
        /// <returns>True, if valid.</returns>
        public bool IsValidMeterType(string meterType)
        {
            if (ValidDimMeterTypes == null || !ValidDimMeterTypes.Any())
            {
                ValidDimMeterTypes = GetInsightsDwEntities().DimMeterTypes.ToList();
            }

            return ValidDimMeterTypes.Count(m => m.MeterTypeName.Equals(meterType, StringComparison.InvariantCultureIgnoreCase)) != 0;
        }

        private static List<DimNexusType> ValidDimNexusTypes { get; set; }
        /// <summary>
        /// Determine if a specified NexusType is valid and validate its value.
        /// </summary>
        /// <param name="nexusType">The NexusType</param>
        /// <param name="nexusValue">The value to validate.</param>
        /// <returns>Returns -1 if invalid NexusType; -2 if invalid value; otherwise it returns a zero.</returns>
        public int IsValidNexusType(string nexusType, string nexusValue)
        {
            try
            {

                if (ValidDimNexusTypes == null || !ValidDimNexusTypes.Any())
                {
                    ValidDimNexusTypes = GetInsightsDwEntities().DimNexusTypes.ToList();
                }

                // Find the nexusType
                var foundNexusType =
                    ValidDimNexusTypes.First(
                            m => m.NexusType.Equals(nexusType, StringComparison.InvariantCultureIgnoreCase));

                // validate the nexus value
                if (string.IsNullOrEmpty(nexusValue))
                {
                    return -2;
                }

                switch (foundNexusType.Validation.ToLower())
                {
                    case "decimal":
                        decimal outDecimal;
                        if (!decimal.TryParse(nexusValue, out outDecimal))
                        {
                            return -2;
                        }
                        break;
                    case "int":
                        int outInt;
                        if (!int.TryParse(nexusValue, out outInt))
                        {
                            return -2;
                        }
                        break;
                    case "datetime":
                        DateTime outDateTime;
                        if (!DateTime.TryParse(nexusValue, out outDateTime))
                        {
                            return -2;
                        }
                        break;
                }
            }
            catch
            {
                return -1;
            }

            return 0;
        }

        private static List<DimSource> ValidDimSources { get; set; }
        /// <summary>
        /// Determine if a specified Source is valid.
        /// </summary>
        /// <param name="theSource">The Source</param>
        /// <returns>-1 if not a valid source, otherwise the sourceId</returns>
        public int ValidateAndGetSource(string theSource)
        {
            int theSourceId;

            try
            {

                if (ValidDimSources == null || !ValidDimSources.Any())
                {
                    ValidDimSources = GetInsightsDwEntities().DimSources.ToList();
                }

                var theFoundSource = ValidDimSources.First(m => m.SourceDesc.Equals(theSource, StringComparison.InvariantCultureIgnoreCase));
                int.TryParse(theFoundSource.SourceId.ToString(), out theSourceId);
            }
            catch
            {
                theSourceId = -1;
            }

            return theSourceId < 1 ? -1 : theSourceId;
        }


        private static List<DimCountryRegion> ValidDimCountryRegions { get; set; }
        /// <summary>
        /// Determine if a specified State/Province and Country are valid.
        /// </summary>
        /// <param name="theState">The State or Province</param>
        /// <param name="theCountry">The Country</param>
        /// <returns>True, if valid.</returns>
        public bool IsValidStateProvinceAndCountry(string theState, string theCountry)
        {

            try
            {

                if (ValidDimCountryRegions == null || !ValidDimCountryRegions.Any())
                {
                    ValidDimCountryRegions = GetInsightsDwEntities().DimCountryRegions.ToList();
                }

                var theFoundCountry =
                    ValidDimCountryRegions.First(
                            c => c.CountryRegionCode.Equals(theCountry, StringComparison.InvariantCultureIgnoreCase));


                // ReSharper disable once ReturnValueOfPureMethodIsNotUsed
                    theFoundCountry.DimStateProvinces.First(
                        s => s.StateProvinceName.Equals(theState, StringComparison.InvariantCultureIgnoreCase));

                return true;
            }
            catch
            {
                return false;
            }
        }


        #region ++++++++++++++++ WriteDataToHolding ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

        private static readonly object BillLock = new object();
        private static readonly object BillCostDetailsLock = new object();
        private static readonly object BillServiceCostDetailsLock = new object();
        private static readonly object CustomerLock = new object();
        private static readonly object PremiseLock = new object();
        private static readonly object PremiseAttributeLock = new object();
        private static readonly object ActionKeyLock = new object();

        /// <summary>
        /// Write Customer Data to Holding table
        /// </summary>
        /// <param name="customers"></param>
        /// <returns>rowsAffected</returns>

        public int WriteCustomerData(List<Holding_Customer> customers)
        {
            lock (CustomerLock)
            {
                using (var ent = GetInsightsDwEntities())
                {
                    ent.Configuration.AutoDetectChangesEnabled = false;

                    // Compare the Customers with what is in the Holding table
                    var preCount = customers.Count;
                    customers = customers.Except(ent.Holding_Customer.ToList(), new DistinctItemComparer_Customer()).ToList();

                    // All records already in Holding table.
                    if (customers.Count == 0)
                    {
                        return preCount;
                    }

                    // Save the Customers
                    var postCount = preCount - customers.Count;

                    var initialCount = ent.Holding_Customer.Count();
                    ent.BulkInsert(customers);
                    var afterInsertCount = ent.Holding_Customer.Count();

                    return afterInsertCount - initialCount + postCount;
                }
            }
        }

        /// <summary>
        /// Write Premise Data to Holding table
        /// </summary>
        /// <param name="premises"></param>
        /// <returns>rowsAffected</returns>
        public int WritePremiseData(List<Holding_Premise> premises)
        {
            lock (PremiseLock)
            {
                using (var ent = GetInsightsDwEntities())
                {
                    ent.Configuration.AutoDetectChangesEnabled = false;

                    // Compare the Premises with what is in the Holding table
                    var preCount = premises.Count;
                    premises = premises.Except(ent.Holding_Premise.ToList(), new DistinctItemComparer_Premise()).ToList();

                    // All records already in Holding table.
                    if (premises.Count == 0)
                    {
                        return preCount;
                    }

                    // Save the Premises
                    var postCount = preCount - premises.Count;

                    var initialCount = ent.Holding_Premise.Count();
                    ent.BulkInsert(premises);
                    var afterInsertCount = ent.Holding_Premise.Count();

                    return afterInsertCount - initialCount + postCount;
                }
            }
        }
        
        /// <summary>
        /// Write Billing Data to Holding table
        /// </summary>
        /// <param name="bills"></param>
        /// <returns>rowsAffected</returns>
        public int WriteBillingData(List<Holding_Billing> bills)
        {
            lock (BillLock)
            {
                using (var ent = GetInsightsDwEntities())
                {
                    ent.Configuration.AutoDetectChangesEnabled = false;

                    // Compare the Bills with what is in the Holding table
                    var preCount = bills.Count;
                    //bills = bills.Except(ent.Holding_Billing.ToList(), new DistinctItemComparer_Billing()).ToList();

                    // All records already in Holding table.
                    if (bills.Count == 0)
                    {
                        return preCount;
                    }

                    // Save the Bills
                    var postCount = preCount - bills.Count;

                    var initialCount = ent.Holding_Billing.Count();
                    ent.BulkInsert(bills);
                    var afterInsertCount = ent.Holding_Billing.Count();

                    return afterInsertCount - initialCount + postCount;
                }
            }
        }

        /// <summary>
        /// Write BillingCostDetails Data to Holding table
        /// </summary>
        /// <param name="billingCostDetails"></param>
        /// <returns>rowsAffected</returns>

        public int WriteBilling_BillCostDetailsData(List<Holding_Billing_BillCostDetails> billingCostDetails)
        {
            lock (BillCostDetailsLock)
            {
                using (var ent = GetInsightsDwEntities())
                {
                    ent.Configuration.AutoDetectChangesEnabled = false;

                    var initialCount = ent.Holding_Billing_BillCostDetails.Count();
                    ent.BulkInsert(billingCostDetails);
                    var afterInsertCount = ent.Holding_Billing_BillCostDetails.Count();

                    return afterInsertCount - initialCount;
                }
            }
        }

        /// <summary>
        /// Write ServiceCostDetails Data to Holding table
        /// </summary>
        /// <param name="serviceCostDetails"></param>
        /// <returns>rowsAffected</returns>
        public int WriteBilling_ServiceCostDetailsData(List<Holding_Billing_ServiceCostDetails> serviceCostDetails)
        {
            lock (BillServiceCostDetailsLock)
            {
                using (var ent = GetInsightsDwEntities())
                {
                    ent.Configuration.AutoDetectChangesEnabled = false;

                    var initialCount = ent.Holding_Billing_ServiceCostDetails.Count();
                    ent.BulkInsert(serviceCostDetails);
                    var afterInsertCount = ent.Holding_Billing_ServiceCostDetails.Count();

                    return afterInsertCount - initialCount;
                }
            }
        }


        /// <summary>
        /// Write PremiseAttribute Data to Holding table
        /// </summary>
        /// <param name="premiseAttributes"></param>
        /// <returns>rowsAffected</returns>
        public int WritePremiseAttributeData(List<Holding_PremiseAttribute> premiseAttributes)
        {
            lock (PremiseAttributeLock)
            {
                using (var ent = GetInsightsDwEntities())
                {
                    ent.Configuration.AutoDetectChangesEnabled = false;

                    // Compare the Premise Attributes with what is in the Holding table
                    var preCount = premiseAttributes.Count;
                    premiseAttributes = premiseAttributes.Except(ent.Holding_PremiseAttribute.ToList(), new DistinctItemComparer_PremiseAttribute()).ToList();

                    // All records already in Holding table.
                    if (premiseAttributes.Count == 0)
                    {
                        return preCount;
                    }

                    // Load remaining records to Holding table.
                    var postCount = preCount - premiseAttributes.Count;

                    var initialCount = ent.Holding_PremiseAttribute.Count();
                    ent.BulkInsert(premiseAttributes);
                    var afterInsertCount = ent.Holding_PremiseAttribute.Count();

                    return afterInsertCount - initialCount + postCount;
                }
            }
        }

        /// <summary>
        /// Write ActionKey Data to Holding table
        /// </summary>
        /// <param name="actionItems"></param>
        /// <returns>rowsAffected</returns>
        public int WriteActionKeyData(List<Holding_ActionItem> actionItems)
        {
            lock (ActionKeyLock)
            {
                using (var ent = GetInsightsDwEntities())
                {
                    ent.Configuration.AutoDetectChangesEnabled = false;

                    // Compare the ActionKeys with what is in the Holding table
                    var preCount = actionItems.Count;
                    actionItems = actionItems.Except(ent.Holding_ActionItem.ToList(), new DistinctItemComparer_ActionItem()).ToList();

                    // All records already in Holding table.
                    if (actionItems.Count == 0)
                    {
                        return preCount;
                    }

                    // Save the ActionKeys
                    var postCount = preCount - actionItems.Count;

                    var initialCount = ent.Holding_ActionItem.Count();
                    ent.BulkInsert(actionItems);
                    var afterInsertCount = ent.Holding_ActionItem.Count();

                    return afterInsertCount - initialCount + postCount;
                }
            }
        }

        #endregion ++++++++++++++++ WriteDataToHolding ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

        #region ++++++++++++++++ SQL Helpers ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

        /// <summary>
        /// ConvertDatabaseDataTypeToSystemDataType
        /// </summary>
        /// <param name="databaseSystemType"></param>
        /// <returns></returns>
        protected string ConvertDatabaseDataTypeToSystemDataType(string databaseSystemType)
        {
            string result;

            switch (databaseSystemType.ToLower())
            {
                case "bigint":
                    result = "System.Int64";
                    break;
                case "bit":
                    result = "System.Boolean";
                    break;
                case "char":
                    result = "System.String";
                    break;
                case "float":
                    result = "System.Double";
                    break;
                case "decimal":
                    result = "System.Double";
                    break;
                case "int":
                    result = "System.Int32";
                    break;
                case "nchar":
                    result = "System.String";
                    break;
                case "ntext":
                    result = "System.String";
                    break;
                case "nvarchar":
                    result = "System.String";
                    break;
                case "real":
                    result = "System.Single";
                    break;
                case "smallint":
                    result = "System.Int16";
                    break;
                case "tinyint":
                    result = "System.Byte";
                    break;
                case "text":
                    result = "System.String";
                    break;
                case "varchar":
                    result = "System.String";
                    break;
                case "xml":
                    result = "System.String";
                    break;
                case "datetime":
                    result = "System.String";
                    break;
                case "uniqueidentifier":
                    result = "System.Guid";
                    break;
                case "sysname":
                    result = "System.String";
                    break;
                default:
                    throw new ArgumentOutOfRangeException($"Unexpected database system type: {databaseSystemType}.");
            }

            return result;
        }



        #endregion  ++++++++++++++++ SQL Helpers ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

        #region ++++++++++++++++ Log Errors ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

        public void LogErrorsByInputType(Dictionary<string, string> dict, string message, string fileName,
            DWImportConstants.RunType runType, bool logErrors)
        {
            if (logErrors)
            {
                using (var context = GetInsightsDwEntities())
                {
                    switch (runType)
                    {
                        case DWImportConstants.RunType.Customer:

                            var customerError = new ImportData_Errors_Customer
                            {
                                TrackingId = dict[DWImportConstants.Trackingid],
                                AccountId = dict[DWImportConstants.Accountid],
                                ClientId = Convert.ToInt32(dict[DWImportConstants.Clientid]),
                                CustomerId = dict[DWImportConstants.Customerid],
                                ErrorDateTime = DateTime.Now,
                                PremiseId = dict[DWImportConstants.Premiseid],
                                MessageText = message,
                                FileName = fileName
                            };

                            context.ImportData_Errors_Customer.Add(customerError);

                            break;

                        case DWImportConstants.RunType.Bill:

                            var billError = new ImportData_Errors_Bill
                            {
                                TrackingId = dict[DWImportConstants.Trackingid],
                                FileName = fileName,
                                ClientId = Convert.ToInt32(dict[DWImportConstants.Clientid]),
                                AccountId = dict[DWImportConstants.Accountid],
                                PremiseId = dict[DWImportConstants.Premiseid],
                                ServicePointId = dict[DWImportConstants.Servicepointid],
                                ErrorDateTime = DateTime.Now,
                                EndDate = dict[DWImportConstants.Enddate],
                                StartDate = dict[DWImportConstants.Startdate],
                                MessageText = message
                            };

                            context.ImportData_Errors_Bill.Add(billError);

                            break;

                        case DWImportConstants.RunType.Profile:

                            var profileError = new ImportData_Errors_Profile
                            {
                                TrackingId = dict[DWImportConstants.Trackingid],
                                FileName = fileName,
                                ClientId = Convert.ToInt32(dict[DWImportConstants.Clientid]),
                                AccountId = dict[DWImportConstants.Accountid],
                                PremiseId = dict[DWImportConstants.Premiseid],
                                ErrorDateTime = DateTime.Now,
                                AttributeKey = dict[DWImportConstants.Attributekey],
                                AttributeValue = dict[DWImportConstants.Attributevalue],
                                MessageText = message
                            };

                            context.ImportData_Errors_Profile.Add(profileError);

                            break;

                        case DWImportConstants.RunType.ActionItem:

                            var actionitemError = new ImportData_Errors_ActonItem
                            {
                                TrackingId = dict[DWImportConstants.Trackingid],
                                FileName = fileName,
                                ClientId = Convert.ToInt32(dict[DWImportConstants.Clientid]),
                                ErrorDateTime = DateTime.Now,
                                CustomerId = dict[DWImportConstants.Customerid],
                                AccountId = dict[DWImportConstants.Accountid],
                                PremiseId = dict[DWImportConstants.Premiseid],
                                ActionKey = dict[DWImportConstants.Actionkey],
                                MessageText = message
                            };


                            context.ImportData_Errors_ActonItem.Add(actionitemError);

                            break;
                    }


                    context.SaveChanges();


                }
            }
        }
        #endregion ++++++++++++++++ Log Errors ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

        #region ++++++++++++++++ Log Counts ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

        /// <summary>
        /// Log counts for reporting
        /// </summary>
        /// <param name="clientId">The current client id.</param>
        /// <param name="processType">The process type</param>
        /// <param name="count">The count</param>
        /// <param name="fileName">The fileName</param>
        /// <param name="comment">A comment</param>
        public void LogCountsForReporting(int clientId, string processType, int count, string fileName, string comment)
        {
            using (var context = GetInsightsDwEntities())
            {
                context.insClientProcessCount(clientId, processType, count, fileName, comment);
            }
        }

        #endregion ++++++++++++++++ Log Counts ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    }

}
