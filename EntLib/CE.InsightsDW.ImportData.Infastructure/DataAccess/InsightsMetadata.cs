﻿using System;
using System.Collections.Generic;
using System.Linq;
using CE.InsightsMeta.Model;

// ReSharper disable once CheckNamespace
namespace CE.InsightsDW.ImportData.Infastructure
{
    public class InsightsMetadata : IMetaDataAccess
    {


        public void LogDbEvent(string message, int clientId)
        {
            using (var ent = new InsightsMetadataEntities())
            {
                ent.WriteExecutionError("Gen Error", "CE.InsightsDW.ImportData",
                    DateTime.Now.ToString("yyyy-MM-dd H:mm:ss.fff"), message, clientId, message);
            }

        }

        /// <summary>
        /// Get the client configuration file for a specific component name and enviornment.
        /// </summary>
        /// <param name="clientId">The client Id </param>
        /// <param name="componentName">The component name</param>
        /// <param name="environment">The enviornment</param>
        /// <param name="configuration">The found configuration</param>
        public void GetAppConfiguration(int clientId, string componentName, string environment, out string configuration)
        {
            configuration = "";
            var db = new InsightsMetadataEntities();

            var theClientIds = new List<int> {clientId, 0};

            var query = db.ETLBulkConfigurations.Where(c => c.ClientComponent == componentName &&
                                                            theClientIds.Contains(c.ClientID) &&
                                                            c.Environment == environment);


            foreach (var item in query)
            {
                configuration = item.XMLConfiguration;
            }
        }


        private static List<ClientProfileAttribute> ValidClientProfileAttributes { get; set; }
        /// <summary>
        /// Determine if a specified ClientProfileAttribute  is valid for a client.
        /// </summary>
        /// <param name="clientProfileAttribute">The Client Profile Attribute</param>
        /// <returns>True, if Valid</returns>
        public bool IsValidClientProfileAttribute(string clientProfileAttribute)
        {
            var db = new InsightsMetadataEntities();

            if (ValidClientProfileAttributes == null || !ValidClientProfileAttributes.Any())
            {
                ValidClientProfileAttributes = db.ClientProfileAttributes.ToList();
            }


            return ValidClientProfileAttributes.Count(
                    pa => pa.ProfileAttributeKey.Equals(clientProfileAttribute, StringComparison.InvariantCultureIgnoreCase)) != 0;
        }


        private static List<TypeActionStatu> ValidTypeActionStatuses { get; set; }
        /// <summary>
        /// Determine if a specified Action Status  is valid for a client.
        /// </summary>
        /// <param name="actionStatus">The Action Status</param>
        /// <returns></returns>
        public int IsValidActionStatus(string actionStatus)
        {
            var db = new InsightsMetadataEntities();

            int theStatusId;

            try
            {
                if (ValidTypeActionStatuses == null || !ValidTypeActionStatuses.Any())
                {
                    ValidTypeActionStatuses = db.TypeActionStatus.ToList();
                }


                var theFoundActionStatus = ValidTypeActionStatuses.First(m => m.ActionStatusKey.Equals(actionStatus, StringComparison.InvariantCultureIgnoreCase));
                int.TryParse(theFoundActionStatus.ActionStatusID.ToString(), out theStatusId);
            }
            catch
            {
                theStatusId = -1;
            }

            return theStatusId < 1 ? -1 : theStatusId;
        }


        private static List<ClientAction> ValidClientActions { get; set; }
        /// <summary>
        /// Determine if a specified Action Key is valid for a client.
        /// </summary>
        /// <param name="actionKey">The Action Key Type</param>
        /// <param name="clientId">The Client Id</param>
        /// <returns>True, if valid.</returns>
        public bool IsValidActionKey(string actionKey, int clientId)
        {
            var db = new InsightsMetadataEntities();

            if (ValidClientActions == null || !ValidClientActions.Any())
            {
                ValidClientActions = db.ClientActions.ToList();
            }

            return
                ValidClientActions.Count(
                    m =>
                        (m.ClientID == clientId || m.ClientID == 0) &&
                        m.ActionKey.Equals(actionKey, StringComparison.InvariantCultureIgnoreCase)) != 0;
        }
    }
}
