﻿ // ReSharper disable once CheckNamespace
namespace CE.InsightsDW.ImportData.Infastructure
{
    /// <summary>
        /// User defined table type column.
        /// </summary>
        public class UserDefinedTableTypeColumn
        {
            public string ColumnName { get; set; }

            public int ColumnId { get; set; }

            public int SystemTypeId { get; set; }

            public string SystemTypeName { get; set; }

            public int MaxLength { get; set; }
    }

}
