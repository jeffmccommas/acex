﻿namespace CE.InsightsDW.ImportData.Infastructure.DataAccess
{
    public class CountsForReportingModel
    {
        public int ClientId { get; set; }

        public string ProcessType { get; set; }

        public int Count { get; set; }

        public string FileName { get; set; }

        public string Comment { get; set; }
    }

}
