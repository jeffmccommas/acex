﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Xml.Serialization;
using System.Configuration;
using System.Net.Mail;
using System.Net.Mime;

namespace CE.InsightsDW.ImportData.Infastructure.Notification
{
    public class Notification
    {
        private const string ComponentName = "InsightsDW.ImportData.Notification";

        /// <summary>
        /// Get the notification configurations
        /// </summary>
        /// <param name="clientId">The clientId</param>
        /// <returns></returns>
        public NotificationConfigModel GetNotificationConfiguration(int clientId)
        {
            // Get the enviornment
            var appSettings = ConfigurationManager.AppSettings;
            var environment = appSettings["Environment"];

            // Get access to InsightsMetadata
            var insightsMetadata = new InsightsMetadata();
            var xmlConfig = string.Empty;
            var theConfigs = new NotificationConfigModel();

            //Load Settings from InsightsMetadata
            insightsMetadata.GetAppConfiguration(clientId, ComponentName, environment, out xmlConfig);

            // Load the configuration settings in InsightsMetadata
            var deserializer = new XmlSerializer(typeof(NotificationConfigModel));
            TextReader textReader = new StringReader(xmlConfig);
            theConfigs = (NotificationConfigModel)deserializer.Deserialize(textReader);
            textReader.Close();

            return theConfigs;
        }

        /// <summary>
        /// Send the error email.
        /// </summary>
        /// <param name="notificationConfig">The notification settings</param>
        /// <param name="emailContent">The email content</param>
        /// <param name="subjectSuffix">The subject suffix</param>
        /// <returns></returns>
        public bool SendEmail(NotificationConfigModel notificationConfig, List<string> emailContent,
            string subjectSuffix)
        {
            var mailMsg = new MailMessage();

            // To
            foreach (var emailAddress in notificationConfig.EmailDistributionList.Split(','))
            {
                mailMsg.To.Add(new MailAddress(emailAddress));
            }

            // From
            mailMsg.From = new MailAddress(notificationConfig.EmailFrom, notificationConfig.EmailFromName);

            // Subject and multipart/alternative Body
            mailMsg.Subject = notificationConfig.EmailSubject + " -- " + subjectSuffix;

            mailMsg.AlternateViews.Add(AlternateView.CreateAlternateViewFromString(emailContent.Aggregate((a, b) => a + "%0D%0A" + b), null,
                    MediaTypeNames.Text.Plain));

            mailMsg.AlternateViews.Add(AlternateView.CreateAlternateViewFromString(emailContent.Aggregate((a, b) => a + "<br/>" + b), null, MediaTypeNames.Text.Html));

            // Init SmtpClient and send
            var smtpClient = new SmtpClient(notificationConfig.EmailSmtpServer,
                Convert.ToInt32(notificationConfig.EmailSmtpPort));

            var credentials = new System.Net.NetworkCredential(notificationConfig.EmailSmtpUser,
                notificationConfig.EmailSmtpPassword);

            smtpClient.Credentials = credentials;
            smtpClient.EnableSsl = Convert.ToBoolean(notificationConfig.EmailSmtpSsl);

            smtpClient.Send(mailMsg);

            return true;

        }

    }
}
