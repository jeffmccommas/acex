﻿using System;
using System.Xml.Serialization;

namespace CE.InsightsDW.ImportData.Infastructure.Notification
{
    [Serializable()]
    [System.Xml.Serialization.XmlRoot("InsightsDW.ImportData.Notification")]
    public class NotificationConfigModel
    {
        [XmlElement("EmailEnabled", typeof(bool))]
        public bool EmailEnabled { get; set; }

        [XmlElement("EmailDistributionList", typeof(string))]
        public string EmailDistributionList { get; set; }

        [XmlElement("EmailFrom", typeof(string))]
        public string EmailFrom { get; set; }

        [XmlElement("EmailFromName", typeof(string))]
        public string EmailFromName { get; set; }

        [XmlElement("EmailSmtpAuth", typeof(string))]
        public string EmailSmtpAuth { get; set; }

        [XmlElement("EmailSmtpPassword", typeof(string))]
        public string EmailSmtpPassword { get; set; }

        [XmlElement("EmailSmtpPort", typeof(string))]
        public string EmailSmtpPort { get; set; }

        [XmlElement("EmailSmtpServer", typeof(string))]
        public string EmailSmtpServer { get; set; }

        [XmlElement("EmailSmtpSsl", typeof(string))]
        public string EmailSmtpSsl { get; set; }

        [XmlElement("EmailSmtpUser", typeof(string))]
        public string EmailSmtpUser { get; set; }

        [XmlElement("EmailSubject", typeof(string))]
        public string EmailSubject { get; set; }
    }
}
