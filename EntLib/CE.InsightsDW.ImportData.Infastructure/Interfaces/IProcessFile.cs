﻿using System.Collections.Generic;
using System.IO;
using Aclara.UFx.StatusManagement;
using CE.InsightsDW.ImportData.Infastructure.DataAccess;

// ReSharper disable once CheckNamespace
namespace CE.InsightsDW.ImportData.Infastructure
{
    public interface IProcessFile
    {
        string ProcessFile(FileInfo custFile, ICollection<string> emailMessageContents,
            ICollection<LoggerMessageModel> logMessageContents, CountsForReportingModel captureCountsForReporting,
            out StatusList statusList);

    }
}
