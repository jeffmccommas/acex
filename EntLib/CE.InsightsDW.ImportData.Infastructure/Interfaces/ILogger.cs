﻿using System;

// ReSharper disable once CheckNamespace
namespace CE.InsightsDW.ImportData.Infastructure { 
    public interface ILogger
    {
        void Info(string trackingid, string message);
        void Warn(string trackingid, string message);
        void Debug(string trackingid,string message);
        void Error(string trackingid,string message);
        void ErrorException(string trackingid, Exception ex);
        void Fatal(string trackingid, string message);

        void SetName(string logFileName);
    }
}
