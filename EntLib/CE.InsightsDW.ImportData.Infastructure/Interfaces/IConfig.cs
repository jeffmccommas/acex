﻿ // ReSharper disable once CheckNamespace

using System;

namespace CE.InsightsDW.ImportData.Infastructure
{
    public interface IConfig
    {
        void LoadBillingConfiguration();

        void LoadCustomerConfiguration();

        void LoadProfileConfiguration();

        void LoadActionItemConfiguration();

        string InputXSD { get; set; }

        string ErrorLog { get; set; }

        int MaxBatchSize { get; set; }

        string InputFilePath { get; set; }

        string FileSearchPattern { get; set; }

        string LogFileOutputExtension { get; set; }

        string FileWorkingExtension { get; set; }

        string ArchivePath { get; set; }

        string FileErrorExtension { get; set; }

        string FileDoneExtension { get; set; }

        int Parallelism { get; set; }

        bool ImportSpecificLoggingOn { get; set; }

        string ShardName { get; set; }

        DateTime EtlAvoidanceStartTime { get; set; }

        DateTime EtlAvoidanceEndTime { get; set; }
    }
}
