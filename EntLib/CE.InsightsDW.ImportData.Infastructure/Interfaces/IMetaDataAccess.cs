﻿
// ReSharper disable once CheckNamespace
namespace CE.InsightsDW.ImportData.Infastructure
{
    public interface IMetaDataAccess
    {
        void LogDbEvent(string message, int clientId);

        void GetAppConfiguration(int clientId, string componentName, string environment, out string configuration);
    }
}
