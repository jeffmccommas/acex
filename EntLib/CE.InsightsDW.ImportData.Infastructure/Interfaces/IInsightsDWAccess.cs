﻿using System.Collections.Generic;
using CE.InsightsDW.Model;

// ReSharper disable once CheckNamespace
namespace CE.InsightsDW.ImportData.Infastructure
{
    public interface IInsightsDWAccess
    {
        int WriteCustomerData(List<Holding_Customer> customers);

        int WritePremiseData(List<Holding_Premise>premises);

        int WriteBillingData(List<Holding_Billing> billings);

        int WritePremiseAttributeData(List<Holding_PremiseAttribute> premiseAttributes);

        int WriteActionKeyData(List<Holding_ActionItem> actionItems);

        int WriteBilling_BillCostDetailsData(List<Holding_Billing_BillCostDetails> billingCostDetails);

        int WriteBilling_ServiceCostDetailsData(List<Holding_Billing_ServiceCostDetails> serviceCostDetails);

        void LogCountsForReporting(int clientId, string processType, int count, string fileName, string comment);

    }
}