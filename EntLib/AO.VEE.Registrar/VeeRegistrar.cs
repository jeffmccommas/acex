﻿using AO.VEE.Business;
using AO.VEE.Domain;
using Cassandra.Mapping;
using Microsoft.Practices.Unity;
using AO.DataAccess;

namespace AO.VEE.Registrar
{
    /// <summary>
    /// Generic Dependency Injection Registrar for VEE.
    /// </summary>
    public class VeeRegistrar
    {
        private static bool CassandraMappingsInitialized { get; set; }

        private static bool CassandraSessionInitialized { get; set; }

        /// <summary>
        /// Will Initialize dependency registrations and return a constructed IUnityContainer.
        /// </summary>
        /// <typeparam name="TLifetime"></typeparam>
        public IUnityContainer Initialize<TLifetime>() where TLifetime : LifetimeManager, new()
        {
            var container = new UnityContainer();

            Initialize<TLifetime>(container);

            return container;
        }

        /// <summary>
        /// Initialize DI registrations.
        /// </summary>
        /// <typeparam name="TLifetime"></typeparam>
        /// <param name="container"></param>
        public void Initialize<TLifetime>(IUnityContainer container) where TLifetime : LifetimeManager, new()
        {
            AutoMapperConfig.AutoMapperMappings();

            InitializeCassandraMappings();

            InitializeCassandraSession();

            RegisterTypesForUnity.RegisterTypes<TLifetime>(container);
        }

        /// <summary>
        /// Make sure Cassandra Mappings are only initiated once per App Domain.
        /// </summary>
        private void InitializeCassandraMappings()
        {
            if (!CassandraMappingsInitialized)
            {
                MappingConfiguration.Global.Define<CassandraMapperConfig>();
                CassandraMappingsInitialized = true;
            }
        }

        /// <summary>
        /// Make sure Cassandra Session is created once per App Domain.
        /// </summary>
        private void InitializeCassandraSession()
        {
            if (CassandraSessionInitialized) return;
            var nodesValues = System.Configuration.ConfigurationManager.AppSettings.Get(StaticConfig.CassandraNodes);
            if (nodesValues != null)
                CassandraSession.GetSession();
            CassandraSessionInitialized = true;
        }
    }
}