﻿using System.Collections.Generic;
using AO.Business;
using Microsoft.Practices.Unity;
using AO.BusinessContracts;
using AO.DataAccess;
using AO.VEE.Business;
using CE.AO.Models;
using AO.VEE.Business.Contracts;
using AO.VEE.DataAccess;
using AO.VEE.DataAccess.Contracts;
using AO.VEE.DTO;
using AO.DataAccess.Contracts;
using CE.ContentModel;

namespace AO.VEE.Registrar
{
    public static class RegisterTypesForUnity
    {
        public static void RegisterTypes<TLifetime>(IUnityContainer container) where TLifetime : LifetimeManager, new()
        {
			container.RegisterType<ICacheProvider, MemoryCacheProvider>(new TLifetime());

			container.RegisterType<IProcessTrigger<VeeValidationEstimationTriggerDto>, ValidationEstimationEventHubTrigger>(new TLifetime());
            container.RegisterType<IVeeStagedClientMeterByDateRepository, VeeStagedClientMeterByDateRepository>(new TLifetime());
            container.RegisterType<IVeeConsumptionByMeterRepository, VeeConsumptionByMeterRepository>(new TLifetime());

			container.RegisterType<IVeeValidator, MissingValueValidator>("MissingValueValidator", new TLifetime(),
                new InjectionConstructor(1));
            container.RegisterType<IVeeValidator, NegativeValueValidator>("NegativeValueValidator", new TLifetime(),
                new InjectionConstructor(2));
            container.RegisterType<IVeeValidator, ZeroValueValidator>("ZeroValueValidator", new TLifetime(),
                new InjectionConstructor(3));
            container.RegisterType<IVeeValidator, StaticValueValidator>("StaticValueValidator", new TLifetime(),
                new InjectionConstructor(4));
            container.RegisterType<IVeeValidator, SpikeValueValidator>("SpikeValueValidator", new TLifetime(),
                new InjectionConstructor(5));
            container.RegisterType<IList<IVeeValidator>, IVeeValidator[]>(new TLifetime());

			container.RegisterType<IVeeFacade, VeeFacade>(new TLifetime());

			container.RegisterType<IVeeEstimator, LinearEstimator>("LinearEstimator", new TLifetime());
            container.RegisterType<IList<IVeeEstimator>, IVeeEstimator[]>(new TLifetime());
            container.RegisterType<IVeeEstimatorFacade, VeeEstimatorFacade>(new TLifetime());

			container.RegisterType<IMeterHistoryByMeter, MeterHistoryByMeter>(new TLifetime());
			container.RegisterType<ITallAMI, ConsumptionByMeter>(new TLifetime());

            container.RegisterType<ICassandraRepository, CassandraRepository>(new TLifetime());
			container.RegisterType<IClientConfigFacade, ClientConfigFacade>(new TLifetime());
			container.RegisterType<IContentModelFactory, ContentModelFactoryContentful>(new TLifetime());
			container.RegisterType<IClientConfigFacade, ClientConfigFacade>(new TLifetime());
			container.RegisterType<ICassandraRepository, CassandraRepository>(new TLifetime());
		}

        /// <summary>
        /// To Enable Logging
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="unityContainer"></param>
        /// <param name="logModel"></param>
        /// <returns></returns>
        public static T Resolve<T>(this IUnityContainer unityContainer, LogModel logModel)
        {
            return unityContainer.Resolve<T>(new ParameterOverride("logModel", new LogModel()));
        }
    }
}