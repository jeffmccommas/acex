﻿using Microsoft.WindowsAzure.Storage.Table;
namespace CE.AO.Entities.TableStorageEntities
{
    public class EmailRequestDetailEntity : TableEntity
    {

        public EmailRequestDetailEntity(string partitionKey, string rowKey)
        {
            PartitionKey = partitionKey;
            RowKey = rowKey;
        }

        public EmailRequestDetailEntity()
        {
            PartitionKey = string.Empty;
            RowKey = string.Empty;
        }
        
        public string Description { get; set; }
        public string ErrorMessage { get; set; }
        public string Event { get; set; }
        public string Url { get; set; }
        public static string SetNotificationPartitionKey(int clientId, string accountId)
        {
            return $"{clientId}_{accountId.ToLower()}";
        }

        public static string SetNotificationRowKey(string programName, string serviceContractId, string insight, string channel)
        {
            return string.IsNullOrEmpty(serviceContractId) ? $"{programName}_{insight}_{channel}" : $"{programName}_{serviceContractId.ToLower()}_{insight}_{channel}";
        }
    }
}
