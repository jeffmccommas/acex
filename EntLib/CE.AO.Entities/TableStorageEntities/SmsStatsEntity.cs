﻿using Microsoft.WindowsAzure.Storage.Table;

namespace CE.AO.Entities.TableStorageEntities
{
    public class SmsStatsEntity : TableEntity
    {
        public string Response { get; set; }
    }
}
