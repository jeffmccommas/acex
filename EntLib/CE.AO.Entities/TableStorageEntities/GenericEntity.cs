﻿using System.Collections.Generic;
using System.Data.Services.Common;

namespace CE.AO.Entities.TableStorageEntities
{
    [DataServiceKey("PartitionKey", "RowKey")]
    public class GenericEntity
    {
        public string PartitionKey { get; set; }
        public string RowKey { get; set; }

        protected readonly Dictionary<string, object> Properties = new Dictionary<string, object>();

        internal object this[string key]
        {
            get
            {
                return Properties[key];
            }

            set
            {
                Properties[key] = value;
            }
        }

        public override string ToString()
        {
            // TODO: append each property   
            return "";
        }
    }
}
