﻿using Microsoft.WindowsAzure.Storage.Table;
using System;

namespace CE.AO.Entities.TableStorageEntities
{
    public class CalculationEntity : TableEntity
    {
        public CalculationEntity(string partitionKey, string rowKey)
        {
            PartitionKey = partitionKey;
            RowKey = rowKey;
        }
        public CalculationEntity()
        {
            PartitionKey = string.Empty;
            RowKey = string.Empty;
        }
        public int ClientId { get; set; }
        public string AccountId { get; set; }
        public string CustomerId { get; set; }
        public string MeterId { get; set; }
        public string ServiceContractId { get; set; }
        public int? CommodityId { get; set; } // 
        public int? Unit { get; set; } // Id from Enum UomType
        public double? Cost { get; set; }
        public double? ProjectedCost { get; set; } // MeterProjectedCost
        public double? ProjectedUsage { get; set; }
        public double? Usage { get; set; }
        public double? BillDays { get; set; }
        public DateTime BillCycleStartDate { get; set; }
        public DateTime BillCycleEndDate { get; set; }
        public DateTime? AsOfAmiDate { get; set; }
        public DateTime AsOfCalculationDate { get; set; }
        public double? AverageDailyUsage { get; set; }
        public double? AverageDailyCost { get; set; }
        public string RateClass { get; set; }

        public static string SetPartitionKey(int clientId, string accountId)
        {
            return $"{clientId}_{accountId.ToLower()}";
        }

        public static string SetCtdRowKey(string serviceContractId, bool history, DateTime? date = null)
        {
            if (history && date != null)
            {
                var dateString = ((DateTime)date).ToString("yyyy-MM-dd");
                return $@"C_{dateString}_{serviceContractId.ToLower()}";
            }

            return $"C_{serviceContractId.ToLower()}";
        }

        public static string SetBtdRowKey(bool history, DateTime? date = null)
        {
            if (history && date != null)
            {
                var dateString = ((DateTime)date).ToString("yyyy-MM-dd");
                return $@"B_{dateString}";
            }

            return "B";
        }
    }
}
