﻿using System.Globalization;
using Microsoft.WindowsAzure.Storage.Table;

namespace CE.AO.Entities.TableStorageEntities
{
    public class AccountLookupEntity : TableEntity
    {
        public AccountLookupEntity(string partitionKey, string rowKey)
        {
            PartitionKey = partitionKey;
            RowKey = rowKey;
        }

        public AccountLookupEntity()
        {
            PartitionKey = string.Empty;
            RowKey = string.Empty;
        }

        public int ClientId { get; set; }
        public string AccountId { get; set; }
        public string MeterId { get; set; }
        public bool? IsActive { get; set; }
        public string ServiceContractId { get; set; }
        public string CustomerId { get; set; }
        public bool IsServiceContractCreatedBySystem { get; set; }

        public static string SetPartitionKey(int clientId, string meterId)
        {
            return $"{clientId}_{meterId.ToLower(CultureInfo.CurrentCulture)}";
        }

        public static string SetRowKey(string customerId)
        {
            return $"{customerId.ToLower(CultureInfo.CurrentCulture)}";
        }
    }
}
