﻿using Microsoft.WindowsAzure.Storage.Table;

namespace CE.AO.Entities.TableStorageEntities
{
   public class ClientAccountEntity :TableEntity
    {
        public ClientAccountEntity(string partitionKey, string rowKey)
        {
            PartitionKey = partitionKey;
            RowKey = rowKey;
        }
        public ClientAccountEntity()
        {
            PartitionKey = "";
            RowKey = "";
        }

        public int ClientId { get; set; }
        public string AccountId { get; set; }
        public string CustomerId { get; set; }

        public static string SetPartitionKey(int clientId)
        {
            return clientId.ToString();
        }

        public static string SetRowKey(string accountId)
        {
            return accountId.ToLower();
        }

    }


}
