﻿using Microsoft.WindowsAzure.Storage.Table;

namespace CE.AO.Entities.TableStorageEntities
{
    public class CustomerEntity : TableEntity
    {
        public CustomerEntity(string partitionKey, string rowKey)
        {
            PartitionKey = partitionKey;
            RowKey = rowKey;
        }
        public CustomerEntity()
        {
            PartitionKey = "";
            RowKey = "";
        }
        public int ClientId { get; set; }
        public string CustomerId { get; set; }
        public string Country { get; set; }
        public string Source { get; set; }
        public bool IsBusiness { get; set; }
        public string AddressLine1 { get; set; }
        public string AddressLine2 { get; set; }
        public string AddressLine3 { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string PostalCode { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Phone1 { get; set; }
        public string Phone2 { get; set; }
        public string EmailAddress { get; set; }
        public string CustomerType { get; set; }
        public string TrumpiaSubscriptionId { get; set; }
        public string TrumpiaRequestDetailPkRk { get; set; }

        public static string SetPartitionKey(int clientId, string customerId)
        {
            return $"{clientId}_{customerId.ToLower()}";
        }

        public static string SetRowKey()
        {
            return "C";
        }
    }
}
