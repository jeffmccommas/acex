﻿using System;
using Microsoft.WindowsAzure.Storage.Table;
// ReSharper disable InconsistentNaming

namespace CE.AO.Entities.TableStorageEntities
{
    public class TallAmiEntity : TableEntity
    {
        public TallAmiEntity(string partitionKey, string rowKey)
        {
            PartitionKey = partitionKey;
            RowKey = rowKey;
        }

        public TallAmiEntity()
        {
            PartitionKey = "";
            RowKey = "";
        }

        public int ClientId { get; set; }
        public string AccountNumber { get; set; }
        public string MeterId { get; set; }
        public string ServicePointId { get; set; }
        public int CommodityId { get; set; }
        public int UOMId { get; set; }
        public double? VolumeFactor { get; set; }
        public int? Direction { get; set; }
        public DateTime? ProjectedReadDate { get; set; }
        public string Timezone { get; set; }
        public DateTime AmiTimeStamp { get; set; }
        public double? Consumption { get; set; }
        public string BatteryVoltage { get; set; }
        public string TOUBin { get; set; }
        public string TransponderId { get; set; }
        public string TransponderPort { get; set; }
        public string DialRead { get; set; }
        public string QualityCode { get; set; }
        public string Scenario { get; set; }
        public string ReadingType { get; set; }
        public double? TOU_Regular { get; set; }
        public double? TOU_OnPeak { get; set; }
        public double? TOU_OffPeak { get; set; }
        public double? TOU_Shoulder1 { get; set; }
        public double? TOU_Shoulder2 { get; set; }
        public double? TOU_CriticalPeak { get; set; }

        public static string SetPartitionKey(int clientId, string meterId, DateTime amiTimeStamp)
        {
            return $"{clientId}_{meterId.ToLower()}_{amiTimeStamp.ToString("yyyy-MM-dd")}";
        }

        public static string SetRowKey(DateTime amiTimeStamp)
        {
            return $"{amiTimeStamp.Hour.ToString().PadLeft(2, '0')}:{amiTimeStamp.Minute.ToString().PadLeft(2, '0')}";
        }
    }
}
