﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.WindowsAzure.Storage.Table;
// ReSharper disable InconsistentNaming

namespace CE.AO.Entities.TableStorageEntities
{
    public class AmiDailyIntervalEntity : TableEntity
    {
        public AmiDailyIntervalEntity(string partitionKey, string rowKey)
        {
            PartitionKey = partitionKey;
            RowKey = rowKey;
        }
        public AmiDailyIntervalEntity()
        {
            PartitionKey = "";
            RowKey = "";
        }
        public int ClientId { get; set; }
        public string AccountNumber { get; set; }
        public string MeterId { get; set; }
        public string ServicePointId { get; set; }
        public int CommodityId { get; set; }
        public int UOMId { get; set; }
        public double? VolumeFactor { get; set; }
        public int? Direction { get; set; }
        public DateTime? ProjectedReadDate { get; set; }
        public string Timezone { get; set; }
        public DateTime AmiTimeStamp { get; set; }
        public int IntervalType { get; set; }
        //public double? IntValue0000 { get; set; }
        public double? TOU_Regular { get; set; }
        public double? TOU_OnPeak { get; set; }
        public double? TOU_OffPeak { get; set; }
        public double? TOU_Shoulder1 { get; set; }
        public double? TOU_Shoulder2 { get; set; }
        public double? TOU_CriticalPeak { get; set; }

        public static string SetPartitionKey(int clientId, string meterId)
        {
            return $"{clientId}_{meterId.ToLower()}";
        }

        public static string SetRowKey(DateTime amiTimeStamp)
        {
            return $"{amiTimeStamp.ToString("yyyy-MM-dd")}";
        }
    }
}
