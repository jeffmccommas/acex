﻿using Microsoft.WindowsAzure.Storage.Table;

namespace CE.AO.Entities.TableStorageEntities
{
    public class CustomerMobileNumberLookupEntity : TableEntity
    {
        public CustomerMobileNumberLookupEntity(string partitionKey, string rowKey)
        {
            PartitionKey = partitionKey;
            RowKey = rowKey;
        }

        public CustomerMobileNumberLookupEntity()
        {
            PartitionKey = string.Empty;
            RowKey = string.Empty;
        }

        public int ClientId { get; set; }
        public string CustomerId { get; set; }
        public string AccountId { get; set; }
        public string MobileNumber { get; set; }

        public static string SetPartitionKey(int clientId, string mobileNumber)
        {
            return $"{clientId}_{mobileNumber}";
        }

        public static string SetRowKey(string accountId)
        {
            return $"{accountId.ToLower()}";
        }
    }
}
