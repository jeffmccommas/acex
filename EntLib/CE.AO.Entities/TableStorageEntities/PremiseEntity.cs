﻿using Microsoft.WindowsAzure.Storage.Table;

namespace CE.AO.Entities.TableStorageEntities
{
    public class PremiseEntity : TableEntity
    {
        public PremiseEntity(string partitionKey, string rowKey)
        {
            PartitionKey = partitionKey;
            RowKey = rowKey;
        }
        public PremiseEntity()
        {
            PartitionKey = "";
            RowKey = "";
        }



        public string Country { get; set; }
        public int ClientId { get; set; }
        public string CustomerId { get; set; }
        public string PremiseId { get; set; }
        public string AccountId{ get; set; }
        public string AddressLine1 { get; set; }
        public string AddressLine2 { get; set; }
        public string AddressLine3 { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string PostalCode { get; set; }

        public static string SetPartitionKey(int clientId, string customerId)
        {
            return $"{clientId}_{customerId.ToLower()}";
        }

        public static string SetRowKey(string premiseId)
        {
            return $"P_{premiseId.ToLower()}";
        }

    }
}
