﻿using Microsoft.WindowsAzure.Storage.Table;
using System;

namespace CE.AO.Entities.TableStorageEntities
{
    public class BillingEntity : TableEntity
    {
        public BillingEntity(string partitionKey, string rowKey)
        {
            PartitionKey = partitionKey;
            RowKey = rowKey;
        }

        public BillingEntity()
        {
            PartitionKey = "";
            RowKey = "";
        }

        public int ClientId { get; set; }
        public string CustomerId { get; set; }
        public string AccountId { get; set; }
        public string PremiseId { get; set; }
        public DateTime? AmiStartDate { get; set; }
        public DateTime? AmiEndDate { get; set; }
        public string BillCycleScheduleId { get; set; }
        public string RateClass { get; set; }
        public string ServicePointId { get; set; }
        public string Source { get; set; }
        public string MeterType { get; set; }
        // ReSharper disable once InconsistentNaming
        public int? UOMId { get; set; }
        public double? TotalUsage { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public int BillDays { get; set; }
        public string ReadQuality { get; set; }
        public double TotalCost { get; set; }
        public int CommodityId { get; set; }
        public string MeterId { get; set; }
        public int BillPeriodType { get; set; }
        public string ReplacedMeterId { get; set; }
        public DateTime? ReadDate { get; set; }
        public string ServiceContractId { get; set; }
        public bool? IsFault { get; set; }

        public static string SetPartitionKey(int clientId, string customerId, string accountId)
        {
            return $"{clientId}_{customerId.ToLower()}_{accountId.ToLower()}";
        }

        public static string SetLatestRowKey(string serviceContractId)
        {
            return $"B_{serviceContractId.ToLower()}";
        }

        public static string SetHistoryRowKey(string serviceContractId)
        {
            return $"H_{serviceContractId.ToLower()}";
        }

        public static string SetHistoryRowKey(string serviceContractId, DateTime endDate)
        {
            return $"H_{serviceContractId.ToLower()}_{endDate.ToString("yyyy-MM-dd")}";
        }


        //public static string SetHistoryRowKey(string serviceContractId, string meterId)
        //{
        //    return $"H_{serviceContractId}_{meterId}_";
        //}

        public static string GetServiceContractId(string meterId, string accountId, string premiseId, int commodityId)
        {
            return !string.IsNullOrWhiteSpace(meterId)
                ? $"{accountId.ToLower()}_{premiseId.ToLower()}_{commodityId}_{meterId.ToLower()}"
                : $"{accountId.ToLower()}_{premiseId.ToLower()}_{commodityId}";
        }
    }
}
