﻿using Microsoft.WindowsAzure.Storage.Table;
using System;
using System.Globalization;
using Enums = CE.AO.Utilities.Enums;
namespace CE.AO.Entities.TableStorageEntities
{
    public class AccountUpdatesEntity : TableEntity
    {
        public AccountUpdatesEntity(string partitionKey, string rowKey)
        {
            PartitionKey = partitionKey;
            RowKey = rowKey;
        }

        public AccountUpdatesEntity()
        {
            PartitionKey = string.Empty;
            RowKey = string.Empty;
        }

        public int ClientId { get; set; }
        public string CustomerId { get; set; }
        public string AccountId { get; set; }
        public string PremiseId { get; set; }
        public string MeterId { get; set; }
        public string ServiceContractId { get; set; }
        public string UpdateType { get; set; }
        public string OldValue { get; set; }
        public string NewValue { get; set; }
        public DateTime? UpdateSentDate { get; set; }

        public static string SetPartitionKey(int clientId, string customerId)
        {
            return $"{clientId}_{customerId}";
        }

        public static string SetRowKey(string updateType, string serviceContractId, string customerId, bool history)
        {
            var accountUpdateTypeEnum = (Enums.AccountUpdateType)Enum.Parse(typeof(Enums.AccountUpdateType), Convert.ToString(updateType));
            var utc = DateTime.UtcNow;

            switch (accountUpdateTypeEnum)
            {
                case Enums.AccountUpdateType.BillCycle:
                    return !history
                        ? $"{Convert.ToString(Enums.AccountUpdateType.BillCycle)}_S_{serviceContractId.ToLower(CultureInfo.CurrentCulture)}"
                        : $"H_{Convert.ToString(Enums.AccountUpdateType.BillCycle)}_S_{serviceContractId.ToLower(CultureInfo.CurrentCulture)}_{utc:yyyy-MM-dd}";

                case Enums.AccountUpdateType.RateClass:
                    return !history
                        ? $"{Convert.ToString(Enums.AccountUpdateType.RateClass)}_S_{serviceContractId.ToLower(CultureInfo.CurrentCulture)}"
                        : $"H_{Convert.ToString(Enums.AccountUpdateType.RateClass)}_S_{serviceContractId.ToLower(CultureInfo.CurrentCulture)}_{utc:yyyy-MM-dd}";

                case Enums.AccountUpdateType.Email:
                    return !history
                        ? $"{Convert.ToString(Enums.AccountUpdateType.Email)}_C_{customerId.ToLower(CultureInfo.CurrentCulture)}"
                        : $"H_{Convert.ToString(Enums.AccountUpdateType.Email)}_C_{customerId.ToLower(CultureInfo.CurrentCulture)}_{utc:yyyy-MM-dd}";

                case Enums.AccountUpdateType.Phone1:
                    return !history
                        ? $"{Convert.ToString(Enums.AccountUpdateType.Phone1)}_C_{customerId.ToLower(CultureInfo.CurrentCulture)}"
                        : $"H_{Convert.ToString(Enums.AccountUpdateType.Phone1)}_C_{customerId.ToLower(CultureInfo.CurrentCulture)}_{utc:yyyy-MM-dd}";

                case Enums.AccountUpdateType.MeterReplacement:
                    return !history
                        ? $"{Convert.ToString(Enums.AccountUpdateType.MeterReplacement)}_S_{serviceContractId.ToLower(CultureInfo.CurrentCulture)}"
                        : $"H_{Convert.ToString(Enums.AccountUpdateType.MeterReplacement)}_S_{serviceContractId.ToLower(CultureInfo.CurrentCulture)}_{utc:yyyy-MM-dd}";

                case Enums.AccountUpdateType.ReadStartDate:
                    return !history
                        ? $"{Convert.ToString(Enums.AccountUpdateType.ReadStartDate)}_S_{serviceContractId.ToLower(CultureInfo.CurrentCulture)}"
                        : $"H_{Convert.ToString(Enums.AccountUpdateType.ReadStartDate)}_S_{serviceContractId.ToLower(CultureInfo.CurrentCulture)}_{utc:yyyy-MM-dd}";

                case Enums.AccountUpdateType.ReadEndDate:
                    return !history
                        ? $"{Convert.ToString(Enums.AccountUpdateType.ReadEndDate)}_S_{serviceContractId.ToLower(CultureInfo.CurrentCulture)}"
                        : $"H_{Convert.ToString(Enums.AccountUpdateType.ReadEndDate)}_S_{serviceContractId.ToLower(CultureInfo.CurrentCulture)}_{utc:yyyy-MM-dd}";

            }
            return null;
        }

    }
}
