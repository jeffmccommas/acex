﻿using Microsoft.WindowsAzure.Storage.Table;
using System;

namespace CE.AO.Entities.TableStorageEntities
{
    public class NotificationEntity : TableEntity
    {
        public NotificationEntity(string partitionKey, string rowKey)
        {
            PartitionKey = partitionKey;
            RowKey = rowKey;
        }
        public NotificationEntity()
        {
            PartitionKey = "";
            RowKey = "";
        }

        public int ClientId { get; set; }
        public string AccountId { get; set; }
        public string ServiceContractId { get; set; }
        public string ProgramName { get; set; }
        public string Insight { get; set; }
        public bool? IsNotified { get; set; }
        public bool? IsDelivered { get; set; }
        public bool? IsDropped { get; set; }
        public bool? IsBounced { get; set; }
        public bool? IsOpened { get; set; }
        public bool? IsClicked { get; set; }
        public bool? IsScheduled { get; set; }
        public string Channel { get; set; }
        public string TemplateId { get; set; }
        public string CustomerId { get; set; }
        // ReSharper disable once InconsistentNaming
        public string EvaluationPK { get; set; }
        // ReSharper disable once InconsistentNaming
        public string EvaluationRK { get; set; }
        public DateTime NotifiedDateTime { get; set; }
        public string TrumpiaRequestDetailPkRk { get; set; }
        public string Tier { get; set; }

        public static string SetNotificationPartitionKey(int clientId, string accountId)
        {
            return $"{clientId}_{accountId.ToLower()}";
        }

        public static string SetNotificationRowKey(string programName, string serviceContractId, string insight, string channel, bool history, DateTime? date = null)
        {
            if (history && date != null)
            {
                var dateString = ((DateTime)date).ToString("yyyy-MM-dd");
                return string.IsNullOrEmpty(serviceContractId) ? $@"{dateString}_{programName}_{insight}_{channel}" : $@"{dateString}_{programName}_{serviceContractId.ToLower()}_{insight}_{channel}";
            }
            return string.IsNullOrEmpty(serviceContractId) ? $"{programName}_{insight}_{channel}" : $"{programName}_{serviceContractId.ToLower()}_{insight}_{channel}";
        }
    }
}
