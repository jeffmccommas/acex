﻿using System;
using Microsoft.WindowsAzure.Storage.Table;

namespace CE.AO.Entities.TableStorageEntities
{
    public class AmiReadingEntity : TableEntity
    {
        public AmiReadingEntity(string partitionKey, string rowKey)
        {
            PartitionKey = partitionKey;
            RowKey = rowKey;
        }

        public AmiReadingEntity()
        {
            PartitionKey = "";
            RowKey = "";
        }

        public int ClientId { get; set; }        
        public int CommodityId { get; set; }
        public string MeterId { get; set; }
        public int TransponderId { get; set; }
        public int TransponderPort { get; set; }
        public string CustomerId { get; set; }
        public double ReadingValue { get; set; }
        // ReSharper disable once InconsistentNaming
        public int UOMId { get; set; }
        public DateTime AmiTimeStamp { get; set; }
        public string Timezone { get; set; }
        public string BatteryVoltage { get; set; }
        public string AccountId { get; set; }

        public static string SetPartitionKey(int clientId, string meterId, DateTime amiTimeStamp)
        {
            return $"{clientId}_{meterId.ToLower()}_{amiTimeStamp.ToString("yyyy-MM-dd")}";
        }

        public static string SetRowKey(DateTime amiTimeStamp)
        {
            return $"{amiTimeStamp.Hour.ToString().PadLeft(2, '0')}:{amiTimeStamp.Minute.ToString().PadLeft(2, '0')}";
        }
    }
}
