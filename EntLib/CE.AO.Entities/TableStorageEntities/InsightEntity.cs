﻿using System;
using Microsoft.WindowsAzure.Storage.Table;

namespace CE.AO.Entities.TableStorageEntities
{
    public class InsightEntity : TableEntity
    {
        public InsightEntity(string partitionKey, string rowKey)
        {
            PartitionKey = partitionKey;
            RowKey = rowKey;
        }
        public InsightEntity()
        {
            PartitionKey = "";
            RowKey = "";
        }

        public int ClientId { get; set; }
        public string AccountId { get; set; }
        public string ServiceContractId { get; set; }
        //public DateTime? AsOfAmiDate { get; set; }
        public double? BilllDays { get; set; }
        public double? BtdCost { get; set; }
        public double? BtdProjectedCost { get; set; }
        public double? Usage { get; set; }
        public bool? SupressInsight { get; set; }
        public bool? AccountLevelCostThresholdExceed { get; set; }
        public bool? ServiceLevelUsageThresholdExceed { get; set; }
        public bool? ServiceLevelCostThresholdExceed { get; set; }
        public bool? DayThresholdExceed { get; set; }
        public bool? ServiceLevelTiered1ThresholdApproaching { get; set; }
        public bool? ServiceLevelTiered2ThresholdApproaching { get; set; }
        public bool? ServiceLevelTiered3ThresholdApproaching { get; set; }
        public bool? ServiceLevelTiered4ThresholdApproaching { get; set; }
        public bool? ServiceLevelTiered5ThresholdApproaching { get; set; }
        public bool? ServiceLevelTiered6ThresholdApproaching { get; set; }
        public bool? ServiceLevelTieredThresholdExceed { get; set; }
        public double? ServiceDays { get; set; }
        public double? CtdCost { get; set; }
        public double? CtdProjectedCost { get; set; }
        public string NotifyTime { get; set; }
        public string ProgramName { get; set; }
        public DateTime AsOfEvaluationDate { get; set; }
        public long AsOfEvaluationTickCount { get; set; }
        public string BillCycleStartDate { get; set; }
        public string BillCycleEndDate { get; set; }

        public static string SetInsightPartitionKey(int clientId, string accountId)
        {
            return $"{clientId}_{accountId.ToLower()}";
        }

        public static string SetAccountLevelInsightRowKey(string programName, bool history, DateTime? date = null)
        {
            if (history && date != null)
            {
                var dateString = ((DateTime)date).ToString("yyyy-MM-dd");
                return $@"{dateString}_A_{programName}";
            }

            return $@"A_{programName}";
        }

        public static string SetServiceLevelInsightRowKey(string programName, string serviceContractId, bool history, DateTime? date = null)
        {
            if (history && date != null)
            {
                var dateString = ((DateTime)date).ToString("yyyy-MM-dd");
                return $@"{dateString}_S_{programName}_{serviceContractId.ToLower()}";
            }

            return $"S_{programName}_{serviceContractId.ToLower()}";
        }
    }
}
