﻿using Microsoft.WindowsAzure.Storage.Table;

namespace CE.AO.Entities.TableStorageEntities
{
    public class TrumpiaKeywordConfigurationEntity : TableEntity
    {
        public TrumpiaKeywordConfigurationEntity(string partitionKey, string rowKey)
        {
            PartitionKey = partitionKey;
            RowKey = rowKey;
        }

        public TrumpiaKeywordConfigurationEntity()
        {
            PartitionKey = string.Empty;
            RowKey = string.Empty;
        }

        public string ClientId { get; set; }
        public string TrumpiaKeyword { get; set; }

        public static string SetRowKey(string clientId)
        {
            return $"{clientId}";
        }

        public static string SetPartitionKey(string trumpiaKeyword)
        {
            return $"{trumpiaKeyword}";
        }
    }
}
