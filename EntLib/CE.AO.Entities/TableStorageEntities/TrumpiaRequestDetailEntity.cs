﻿using Microsoft.WindowsAzure.Storage.Table;
using System.Diagnostics.CodeAnalysis;

namespace CE.AO.Entities.TableStorageEntities
{
    [SuppressMessage("ReSharper", "InconsistentNaming")]
    public class TrumpiaRequestDetailEntity : TableEntity
    {
        public TrumpiaRequestDetailEntity(string partitionKey, string rowKey)
        {
            PartitionKey = partitionKey;
            RowKey = rowKey;
        }

        public TrumpiaRequestDetailEntity()
        {
            PartitionKey = string.Empty;
            RowKey = string.Empty;
        }

        public string MessageId { get; set; }
        public string RequestId { get; set; }
        public string Description { get; set; }
        public string StatusCode { get; set; }
        public string ErrorMessage { get; set; }
        public string SmsBody { get; set; }

        public static string SetPartitionKey(string requestId)
        {
            return $"{requestId}";
        }

        public static string SetRowKey(string requestId)
        {
            return $"{requestId}";
        }
    }
}
