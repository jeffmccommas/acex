﻿using System;
using Microsoft.WindowsAzure.Storage.Table;

namespace CE.AO.Entities.TableStorageEntities
{
    public class SubscriptionEntity : TableEntity
    {
        public SubscriptionEntity(string partitionKey, string rowKey)
        {
            PartitionKey = partitionKey;
            RowKey = rowKey;
        }

        public SubscriptionEntity()
        {
            PartitionKey = string.Empty;
            RowKey = string.Empty;
        }
        public int ClientId { get; set; }
        public string CustomerId { get; set; }
        public string AccountId { get; set; }
        public string ProgramName { get; set; }
        public string InsightTypeName { get; set; }
        public string ServiceContractId { get; set; }
        public bool? IsEmailOptInCompleted { get; set; }
        public bool? IsSmsOptInCompleted { get; set; }
        public bool IsSelected { get; set; }
        public string Channel { get; set; }
        public double? SubscriberThresholdValue { get; set; }
        public int? EmailConfirmationCount { get; set; }
        public int? SmsConfirmationCount { get; set; }
        public DateTime? SmsOptInCompletedDate{ get; set; }
        public DateTime? EmailOptInCompletedDate{ get; set; }
        public DateTime? SubscriptionDate { get; set; }
        public DateTime? OptOutDate { get; set; }
        public int? UomId { get; set; }

        public static string SetPartitionKey(int clientId,string customerId, string accountId)
        {
            return $"{clientId}_{customerId.ToLower()}_{accountId.ToLower()}";
        }

        public static string SetLatestRowKey(string programName, string serviceContractId, string insightTypeName)
        {
            return string.IsNullOrWhiteSpace(serviceContractId) ? (string.IsNullOrWhiteSpace(insightTypeName) ? $"A_{programName}" : $"S_{programName}_{insightTypeName}") : (string.IsNullOrWhiteSpace(insightTypeName) ? $"S_{programName}_{serviceContractId.ToLower()}" : $"S_{programName}_{serviceContractId.ToLower()}_{insightTypeName}");
            //return string.IsNullOrWhiteSpace(insightTypeName) ? $"A_{programName}" : $"S_{programName}_{insightTypeName}";
        }

        public static string SetHistoryRowKey(string programName, string serviceContractId, string insightTypeName)
        {
            return string.IsNullOrWhiteSpace(serviceContractId) ? (string.IsNullOrWhiteSpace(insightTypeName) ? $"H_A_{programName}_{DateTime.UtcNow.ToString("yyyy-MM-dd")}" : $"H_S_{programName}_{insightTypeName}_{DateTime.UtcNow.ToString("yyyy-MM-dd")}") : (string.IsNullOrWhiteSpace(insightTypeName) ? $"H_S_{programName}_{serviceContractId.ToLower()}_{DateTime.UtcNow.ToString("yyyy-MM-dd")}" : $"H_S_{programName}_{serviceContractId.ToLower()}_{insightTypeName}_{DateTime.UtcNow.ToString("yyyy-MM-dd")}");
            //return string.IsNullOrWhiteSpace(insightTypeName) ? $"H_A_{programName}_{DateTime.UtcNow.ToString("yyyy-MM-dd")}" : $"H_S_{programName}_{insightTypeName}_{DateTime.UtcNow.ToString("yyyy-MM-dd")}";
        }
    }
}
