﻿using Microsoft.WindowsAzure.Storage.Table;
using System;

namespace CE.AO.Entities.TableStorageEntities
{
   public class CostToDateEntity : TableEntity
    {
        public CostToDateEntity(string partitionKey, string rowKey)
        {
            PartitionKey = partitionKey;
            RowKey = rowKey;
        }
        public CostToDateEntity()
        {
            PartitionKey = string.Empty;
            RowKey = string.Empty;
        }
        public string ClientId { get; set; }
        public string AccountId { get; set; }
        public string CustomerId { get; set; }
        public string MeterId { get; set; }
        public string ServiceContractId { get; set; }
        public int CommodityId { get; set; } // 
        public string Unit { get; set; }
        public double ServiceCtd { get; set; }
        public double ServiceProjectedCost { get; set; } // MeterProjectedCost
        public double ServiceUsage { get; set; }
        public double CtdDays { get; set; }
        public DateTime BillCycleStartDate { get; set; }
        public DateTime BillCycleEndDate { get; set; }
        public double AverageUsage { get; set; }
        public double AverageCost { get; set; }
        public int UnitId { get; set; }
        public string AsOfAmiDate { get; set; }

    }
}
