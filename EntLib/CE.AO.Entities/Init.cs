﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using CE.AO.DataAccess;


namespace CE.AO.Entities
{
    public static class Init
    {
        public static async Task<bool> AzureTableInit()
        {
            var tableRepository = new TableRepository();
            var tableNames = GetConstants(typeof(Constants.TableNames)).ToList();

            var tasks = new List<Task<bool>>();
            tableNames.ForEach(tableName =>
            {
                tasks.Add(tableRepository.CreateTableAsync(Convert.ToString(tableName.GetRawConstantValue())));
            });

            await Task.WhenAll(tasks);
            return true;
        }

        public static async Task<bool> AzureQueueInit()
        {
            var queueRepository = new QueueRepository();
            var queueNames = GetConstants(typeof(Constants.QueueNames)).ToList();

            var tasks = new List<Task<bool>>();
            queueNames.ForEach(queueName =>
            {
                tasks.Add(queueRepository.CreateQueueAsync(Convert.ToString(queueName.GetRawConstantValue())));
            });

            await Task.WhenAll(tasks);
            return true;
        }

        private static IEnumerable<FieldInfo> GetConstants(IReflect type)
        {
            return type.GetFields(BindingFlags.Public | BindingFlags.Static |
                                  BindingFlags.FlattenHierarchy)
                .Where(fi => fi.IsLiteral && !fi.IsInitOnly).ToList();
        }
    }
}
