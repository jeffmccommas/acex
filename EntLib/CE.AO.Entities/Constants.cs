﻿namespace CE.AO.Entities
{
    public static class Constants
    {
        public static class TableNames
        {
            //public const string Ami = "Ami";
            //public const string TallAmi = "TallAmi";
            //public const string AmiReading = "MeterReading";
            //public const string Billing = "Billing";
            //public const string Logging = "LogTable";
            //public const string Calculation = "Calculation";
            //public const string Insight = "Insight";
            //public const string BillCycleSchedule = "BillCycleSchedule";
            //public const string AccountUpdates = "AccountUpdates";
            //public const string Subscription = "Subscription";
            //public const string ClientAccount = "ClientAccount";
            //public const string Notification = "Notification";
            public const string ClientWidgetConfiguration = "ClientWidgetConfiguration";
            //public const string SmsTemplate = "SmsTemplate";
            //public const string TrumpiaRequestDetail = "TrumpiaRequestDetail";
            //public const string TrumpiaKeywordConfiguration = "TrumpiaKeywordConfiguration";
            //public const string Customer = "Customer";
            //public const string Premise = "Premise";
            //public const string ClientMeter = "ClientMeter";
            //public const string AccountMeterLookup = "AccountMeterLookup";            
            //public const string CustomerMobileNumberLookup = "CustomerMobileNumberLookup";
            //public const string CustomerPremiseProfile = "CustomerPremiseProfile";
            //public const string EmailRequestDetail = "EmailRequestDetail";
        }

        public static class QueueNames
        {
            public const string AoCalculationQueue = "aocalculationqueue";
            public const string AoEvaluationQueue = "aoevaluationqueue";
            public const string AoImportBatchProcessQueue = "aoimportbatchprocessqueue";
            public const string AoScheduleCalculationQueue = "aoschedulecalculationqueue";
            public const string AoProcessScheduleCalculationQueue = "aoprocessschedulecalculationqueue";
            public const string AoNotificationQueue = "aonotificationqueue";
            public const string AoInsightReportQueue = "aoinsightreportqueue";
            public const string AoNotificationReportQueue = "aonotificationreportqueue";
        }
    }
}
