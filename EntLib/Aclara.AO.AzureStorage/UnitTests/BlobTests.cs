﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Aclara.AO.AzureStorage;

namespace UnitTests
{
    [TestClass]
    public class BlobTests
    {
        private const string _storageConnString = @"DefaultEndpointsProtocol=https;AccountName=aclaoaclaraonestgdev;AccountKey=5Kv12Tw2/O9+jFaHqfy9yrWf9b0uQM5xa0K9559guWhsb6ugUmS/u5FN86qCS8LtZfAYsDqtOvi8VisNaaRnXQ==";

        private const string _storageConnStringPOC = @"DefaultEndpointsProtocol=https;AccountName=tablestoragetestaclara;AccountKey=/RIHyFmrTtrk3ghfUoAARvCDlb/masYycS66u1Lo9d+dU98JlPI5yya/Zvmtabq4TWAhV8MLUfgLtiO71sn35A==";
        [TestMethod]
        public void TestPDFStorage_onPOCStorage1()
        {
            var storageConnString = _storageConnStringPOC;
            var containerName = "pdf-poc";
            var blobName = "AclaraOneFlowDiagram.pdf";
            var fileName = @"C:\Import\AclaraOneFlowDiagram.pdf";

            var mgr = new StorageManager();
            var result = mgr.CopyFileToBlobStorage(storageConnString, containerName, blobName, fileName);
            Assert.IsTrue(result.Equals("Success"));
        }

        [TestMethod]
        public void TestPDFStorage_onPOCStorage2()
        {
            var storageConnString = _storageConnStringPOC;
            var containerName = "pdf-poc";
            var blobName = "Aclara Insights Website Design.pdf";
            var fileName = @"C:\Import\Aclara Insights Website Design.pdf";

            var mgr = new StorageManager();
            var result = mgr.CopyFileToBlobStorage(storageConnString, containerName, blobName, fileName);
            Assert.IsTrue(result.Equals("Success"));
        }

        [TestMethod]
        public void TestPDFextract_fromPOCStorage1()
        {
            var storageConnString = _storageConnStringPOC;
            var containerName = "pdf-poc";
            var blobName = "AclaraOneFlowDiagram.pdf";
            var mgr = new StorageManager();
            var result = mgr.ExtractFileFromBlobStorage(storageConnString, containerName, blobName);
            Assert.IsTrue(result.Equals("Success"));
        }

        [TestMethod]
        public void TestPDFextract_fromPOCStorage2()
        {
            var storageConnString = _storageConnStringPOC;
            var containerName = "pdf-poc";
            var blobName = "Aclara Insights Website Design.pdf";
            var mgr = new StorageManager();
            var result = mgr.ExtractFileFromBlobStorage(storageConnString, containerName, blobName);
            Assert.IsTrue(result.Equals("Success"));
        }


        [TestMethod]
        public void TestStorageAccount_BLOB_A()
        {
            var storageConnString = _storageConnString;
            var containerName = "client87";
            var blobName = "testfile.txt";
            var fileName = @"C:\Import\Local\testfile.txt";

            var mgr = new StorageManager();
            var result = mgr.CopyFileToBlobStorage(storageConnString, containerName, blobName, fileName);
        }

        [TestMethod]
        public void TestStorageAccount_BLOB_NullFilename()
        {
            var storageConnString = _storageConnString;
            var containerName = "client87";
            var blobName = "testfile.txt";

            var mgr = new StorageManager();
            var result = mgr.CopyFileToBlobStorage(storageConnString, containerName, blobName, null);
        }

        [TestMethod]
        public void TestStorageAccount_BLOB_BadConnString()
        {
            var storageConnString = "DefaultEndpointsProtocol=https;AccountName=Xaclaoaclaraonestgdev;AccountKey=5Kv12Tw2/O9+jFaHqfy9yrWf9b0uQM5xa0K9559guWhsb6ugUmS/u5FN86qCS8LtZfAYsDqtOvi8VisNaaRnXQ==";
            var containerName = "client87";
            var blobName = "testfile.txt";
            var fileName = @"C:\Import\Local\testfile.txt";

            var mgr = new StorageManager();
            var result = mgr.CopyFileToBlobStorage(storageConnString, containerName, blobName, fileName);
        }

        [TestMethod]
        public void TestStorageAccount_BLOB_BadContainerName()
        {
            var storageConnString = _storageConnString;
            var containerName = "bogusName";
            var blobName = "testfile.txt";
            var fileName = @"C:\Import\Local\testfile.txt";

            var mgr = new StorageManager();
            var result = mgr.CopyFileToBlobStorage(storageConnString, containerName, blobName, fileName);
        }

        [TestMethod]
        public void TestStorageAccount_QUEUE_A()
        {
            var storageConnString = _storageConnString;
            var queueName = "aoimportbatchprocessqueue";
            var message = "testmessage:yes";

            var mgr = new StorageManager();
            var result = mgr.WriteMessageToQueue(storageConnString, queueName, message);
        }


        [TestMethod]
        public void TestStorageAccount_QUEUE_NullMessage()
        {
            var storageConnString = _storageConnString;
            var queueName = "aoimportbatchprocessqueue";

            var mgr = new StorageManager();
            var result = mgr.WriteMessageToQueue(storageConnString, queueName, null);
        }

        [TestMethod]
        public void TestStorageAccount_QUEUE_BadConnString()
        {
            var storageConnString = "xyz123";
            var queueName = "aoimportbatchprocessqueue";
            var message = "testmessage:yes";

            var mgr = new StorageManager();
            var result = mgr.WriteMessageToQueue(storageConnString, queueName, message);
        }

        [TestMethod]
        public void TestStorageAccount_QUEUE_BadQueueName()
        {
            var storageConnString = _storageConnString;
            var queueName = "bogusQueueName";
            var message = "testmessage:yes";

            var mgr = new StorageManager();
            var result = mgr.WriteMessageToQueue(storageConnString, queueName, message);
        }

    }
}
