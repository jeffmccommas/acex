﻿using System;
using System.IO;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Blob;
using Microsoft.WindowsAzure.Storage.Queue;
using System.Runtime.InteropServices;

namespace Aclara.AO.AzureStorage
{
    /// <summary>
    /// Storage Manager for exposing functionality for simple blob copying and queue writing.  Also COM enabled.  See readme file for regasm and instructions.
    /// </summary>
    [ComVisible(true)]
    [Guid("3147C061-BAAB-4A6B-B99E-2904E2F33CAA")]
    [ClassInterface(ClassInterfaceType.None)]
    [ProgId("Aclara.AO.AzureStorage.StorageManager")]
    public class StorageManager : IStorageManager
    {
        /// <summary>
        /// Function to copy a file from local disk into blob storage.
        /// </summary>
        /// <param name="storageConnString"></param>
        /// <param name="containerName"></param>
        /// <param name="blobName"></param>
        /// <param name="stream"></param>
        /// <returns></returns>
        public string CopyFileToBlobStorage(string storageConnString, string containerName, string blobName, Stream stream)
        {
            var retval = "Success";

            try
            {
                if (storageConnString != null && containerName != null && blobName != null && stream != null)
                {
                    // Retrieve storage account from connection string
                    CloudStorageAccount storageAccount = CloudStorageAccount.Parse(storageConnString);

                    // Create the blob client.
                    CloudBlobClient blobClient = storageAccount.CreateCloudBlobClient();

                    // Retrieve a reference to a container
                    CloudBlobContainer container = blobClient.GetContainerReference(containerName);

                    container.CreateIfNotExists();

                    // Retrieve reference to an existing blob for overwrite, or one will be created if this does not exist
                    CloudBlockBlob blockBlob = container.GetBlockBlobReference(blobName);

                    blockBlob.UploadFromStream(stream);
                }
                else
                {
                    retval = "Failed:A parameter was null";
                }

            }
            catch (Exception ex)
            {
                if (!string.IsNullOrEmpty(ex.Message))
                {
                    retval = "Failed:" + ex.Message;
                }
                else
                {
                    retval = "Failed:Exception without a message";
                }
               
            }

            return (retval);
        }

        /// <summary>
        /// Function to extract a file from blob storage and view it.
        /// </summary>
        /// <param name="storageConnString"></param>
        /// <param name="containerName"></param>
        /// <param name="blobName"></param>
        /// <param name="memStream"></param>
        /// <returns></returns>
        public string ExtractFileFromBlobStorage(string storageConnString, string containerName, string blobName, out MemoryStream memStream)
        {
            var retval = "Success";
            memStream = new MemoryStream();
            try
            {

                if (storageConnString != null && containerName != null && blobName != null)
                {
                    // Retrieve storage account from connection string
                    CloudStorageAccount storageAccount = CloudStorageAccount.Parse(storageConnString);

                    // Create the blob client.
                    CloudBlobClient blobClient = storageAccount.CreateCloudBlobClient();

                    // Retrieve a reference to a container
                    CloudBlobContainer container = blobClient.GetContainerReference(containerName);

                    // Retrieve reference to an existing blob for overwrite, or one will be created if this does not exist
                    CloudBlockBlob blockBlob = container.GetBlockBlobReference(blobName);
                    
                    blockBlob.DownloadToStream(memStream);
                }
                else
                {
                    retval = "Failed:A parameter was null";
                }

            }
            catch (Exception ex)
            {
                if (!string.IsNullOrEmpty(ex.Message))
                {
                    retval = "Failed:" + ex.Message;
                }
                else
                {
                    retval = "Failed:Exception without a message";
                }
            }
            //return memStream;
            return (retval);
        }


        /// <summary>
        /// Function to write a message into a message queue.
        /// </summary>
        /// <param name="storageConnString"></param>
        /// <param name="queueName"></param>
        /// <param name="message"></param>
        /// <returns></returns>
        public string WriteMessageToQueue(string storageConnString, string queueName, string message)
        {
            var retval = "Success";

            try
            {

                if (storageConnString != null && queueName != null && message != null)
                {
                    // Retrieve storage account from connection string
                    CloudStorageAccount storageAccount = CloudStorageAccount.Parse(storageConnString);

                    // Create the queue client
                    CloudQueueClient queueClient = storageAccount.CreateCloudQueueClient();

                    // Retrieve a reference to a queue
                    CloudQueue queue = queueClient.GetQueueReference(queueName);

                    // Create a message and add it to the queue
                    CloudQueueMessage queueMessage = new CloudQueueMessage(message);
                    queue.AddMessage(queueMessage);

                }
                else
                {
                    retval = "Failed:A parameter was null";
                }

            }
            catch (Exception ex)
            {
                if (!string.IsNullOrEmpty(ex.Message))
                {
                    retval = "Failed:" + ex.Message;
                }
                else
                {
                    retval = "Failed:Exception without a message";
                }
            }

            return (retval);
        }

        /// <summary>
        /// Function to delete a file from blob storage.
        /// </summary>
        /// <param name="storageConnString"></param>
        /// <param name="containerName"></param>
        /// <param name="blobName"></param>
        /// <returns></returns>
        public string DeleteFileFromBlobStorage(string storageConnString, string containerName, string blobName)
        {
            var retval = "Success";

            try
            {

                if (storageConnString != null && containerName != null && blobName != null)
                {
                    // Retrieve storage account from connection string
                    CloudStorageAccount storageAccount = CloudStorageAccount.Parse(storageConnString);

                    // Create the blob client.
                    CloudBlobClient blobClient = storageAccount.CreateCloudBlobClient();

                    // Retrieve a reference to a container
                    CloudBlobContainer container = blobClient.GetContainerReference(containerName);

                    // Retrieve reference to an existing blob for overwrite, or one will be created if this does not exist
                    CloudBlockBlob blockBlob = container.GetBlockBlobReference(blobName);
                    blockBlob.Delete();
                }
                else
                {
                    retval = "Failed:A parameter was null";
                }

            }
            catch (Exception ex)
            {
                if (!string.IsNullOrEmpty(ex.Message))
                {
                    retval = "Failed:" + ex.Message;
                }
                else
                {
                    retval = "Failed:Exception without a message";
                }
            }

            return (retval);
        }
    }

}
