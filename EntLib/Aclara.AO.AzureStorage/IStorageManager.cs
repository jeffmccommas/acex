﻿using System.Runtime.InteropServices;
using System.IO;

namespace Aclara.AO.AzureStorage
{
    [ComVisible(true)]
    [Guid("1F83F401-55F5-4781-BA30-759C75E30F13")]
    [InterfaceType(ComInterfaceType.InterfaceIsDual)]
    public interface IStorageManager
    {
        string CopyFileToBlobStorage(string storageConnString, string containerName, string blobName, Stream stream );
        string WriteMessageToQueue(string storageConnString, string queueName, string message);
        string ExtractFileFromBlobStorage(string storageConnString, string containerName, string blobName, out MemoryStream memStream);

        string DeleteFileFromBlobStorage(string storageConnString, string containerName, string blobName);
    }

}
