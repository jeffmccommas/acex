﻿using System;
using System.Collections.Generic;

namespace CE.InsightDataAccess.InsightModels
{
    public partial class FileSource
    {
        public int SourceId { get; set; }
        public string SourceName { get; set; }
        public string SourceDescription { get; set; }
    }
}
