﻿using System;
using System.Collections.Generic;

namespace CE.InsightDataAccess.InsightModels
{
    public partial class Property
    {
        public Property()
        {
            ClientProperty = new HashSet<ClientProperty>();
        }

        public int PropertyId { get; set; }
        public string Name { get; set; }

        public virtual ICollection<ClientProperty> ClientProperty { get; set; }
    }
}
