﻿using System;
using System.Collections.Generic;

namespace CE.InsightDataAccess.InsightModels
{
    public partial class ActionCustomerAccount
    {
        public int ClientId { get; set; }
        public string CustomerId { get; set; }
        public string AccountId { get; set; }
    }
}
