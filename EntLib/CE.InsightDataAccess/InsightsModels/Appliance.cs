﻿using System;
using System.Collections.Generic;

namespace CE.InsightDataAccess.InsightModels
{
    public partial class Appliance
    {
        public int ApplianceId { get; set; }
        public string Name { get; set; }
    }
}
