﻿using System;
using System.Collections.Generic;

namespace CE.InsightDataAccess.InsightModels
{
    public partial class PowerBiportalLogin
    {
        public int UserId { get; set; }
        public string Username { get; set; }
        public int ClientId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string PasswordHash { get; set; }
        public string PasswordSalt { get; set; }
        public DateTime? UpdateDate { get; set; }
        public byte EnvId { get; set; }
        public string UserType { get; set; }
        public bool? Enabled { get; set; }

        public virtual PowerBiportalClient Client { get; set; }
        public virtual Environment Env { get; set; }
    }
}
