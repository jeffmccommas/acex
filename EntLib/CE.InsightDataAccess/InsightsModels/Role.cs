﻿using System;
using System.Collections.Generic;

namespace CE.InsightDataAccess.InsightModels
{
    public partial class Role
    {
        public Role()
        {
            UserRole = new HashSet<UserRole>();
        }

        public byte RoleId { get; set; }
        public string Name { get; set; }
        public byte SortOrder { get; set; }

        public virtual ICollection<UserRole> UserRole { get; set; }
    }
}
