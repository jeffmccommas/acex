﻿using System;
using System.Collections.Generic;

namespace CE.InsightDataAccess.InsightModels
{
    public partial class UserEnvironment
    {
        public int UserId { get; set; }
        public byte EnvId { get; set; }
        public string CesecretAccessKey { get; set; }
        public string BasicKey { get; set; }
        public DateTime UpdDate { get; set; }

        public virtual Environment Env { get; set; }
        public virtual User User { get; set; }
    }
}
