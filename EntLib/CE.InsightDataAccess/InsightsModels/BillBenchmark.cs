﻿using System;
using System.Collections.Generic;

namespace CE.InsightDataAccess.InsightModels
{
    public partial class BillBenchmark
    {
        public int ClientId { get; set; }
        public string CustomerId { get; set; }
        public string AccountId { get; set; }
        public DateTime BillDate { get; set; }
        public string PremiseId { get; set; }
        public int GroupId { get; set; }
        public byte CommodityId { get; set; }
        public int GroupCount { get; set; }
        public decimal UsageQuantity { get; set; }
        public byte UsageUomId { get; set; }
        public decimal CostAmount { get; set; }
        public byte CurrencyId { get; set; }
        public DateTime NewDate { get; set; }

        public virtual Commodity Commodity { get; set; }
        public virtual Currency Currency { get; set; }
        public virtual BenchmarkGroup Group { get; set; }
        public virtual Uom UsageUom { get; set; }
    }
}
