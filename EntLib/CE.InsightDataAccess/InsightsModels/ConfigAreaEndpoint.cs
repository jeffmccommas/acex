﻿using System;
using System.Collections.Generic;

namespace CE.InsightDataAccess.InsightModels
{
    public partial class ConfigAreaEndpoint
    {
        public byte ConfigAreaId { get; set; }
        public int EndpointId { get; set; }

        public virtual ConfigArea ConfigArea { get; set; }
        public virtual Endpoint Endpoint { get; set; }
    }
}
