﻿using System;
using System.Collections.Generic;

namespace CE.InsightDataAccess.InsightModels
{
    public partial class EndpointTracking
    {
        public int EndpointTrackingId { get; set; }
        public int ClientId { get; set; }
        public int UserId { get; set; }
        public string Verb { get; set; }
        public string ResourceName { get; set; }
        public int? EndpointId { get; set; }
        public string UserAgent { get; set; }
        public string SourceIpaddress { get; set; }
        public string XcemessageId { get; set; }
        public string Xcechannel { get; set; }
        public string Xcelocale { get; set; }
        public string Xcemeta { get; set; }
        public string Query { get; set; }
        public string MachineName { get; set; }
        public bool? IncludeInd { get; set; }
        public DateTime? LogDate { get; set; }
        public DateTime NewDate { get; set; }
        public byte PartitionKey { get; set; }
    }
}
