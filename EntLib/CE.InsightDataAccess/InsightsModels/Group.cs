﻿using System;
using System.Collections.Generic;

namespace CE.InsightDataAccess.InsightModels
{
    public partial class Group
    {
        public int GroupId { get; set; }
        public string GroupName { get; set; }
    }
}
