﻿using System;
using System.Collections.Generic;

namespace CE.InsightDataAccess.InsightModels
{
    public partial class ControlPanelLoginClient
    {
        public int CpId { get; set; }
        public int ClientId { get; set; }

        public virtual ControlPanelLogin Cp { get; set; }
    }
}
