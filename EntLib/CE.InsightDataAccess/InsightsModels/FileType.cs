﻿using System;
using System.Collections.Generic;

namespace CE.InsightDataAccess.InsightModels
{
    public partial class FileType
    {
        public int TypeId { get; set; }
        public string TypeExtension { get; set; }
        public string TypeDescription { get; set; }
    }
}
