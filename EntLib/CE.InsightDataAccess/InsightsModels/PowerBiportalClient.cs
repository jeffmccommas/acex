﻿using System;
using System.Collections.Generic;

namespace CE.InsightDataAccess.InsightModels
{
    public partial class PowerBiportalClient
    {
        public PowerBiportalClient()
        {
            PowerBiportalLogin = new HashSet<PowerBiportalLogin>();
        }

        public int ClientId { get; set; }
        public string ClientName { get; set; }
        public string Description { get; set; }
        public string PbiworkspaceId { get; set; }
        public byte? EnvId { get; set; }

        public virtual ICollection<PowerBiportalLogin> PowerBiportalLogin { get; set; }
        public virtual Environment Env { get; set; }
    }
}
