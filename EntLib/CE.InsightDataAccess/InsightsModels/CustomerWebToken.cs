﻿using System;
using System.Collections.Generic;

namespace CE.InsightDataAccess.InsightModels
{
    public partial class CustomerWebToken
    {
        public int ClientId { get; set; }
        public string CustomerId { get; set; }
        public string WebToken { get; set; }
        public string AdditionalInfo { get; set; }
        public DateTime UtcDateTimeCreated { get; set; }
        public DateTime UtcDateTimeUpdated { get; set; }
        public string UserId { get; set; }
        public int? GroupId { get; set; }
        public int? RoleId { get; set; }

        public virtual Client Client { get; set; }
    }
}
