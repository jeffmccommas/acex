﻿using System;
using System.Collections.Generic;

namespace CE.InsightDataAccess.InsightModels
{
    public partial class BenchmarkGroup
    {
        public BenchmarkGroup()
        {
            BillBenchmark = new HashSet<BillBenchmark>();
        }

        public int GroupId { get; set; }
        public string Name { get; set; }

        public virtual ICollection<BillBenchmark> BillBenchmark { get; set; }
    }
}
