﻿using System;
using System.Collections.Generic;

namespace CE.InsightDataAccess.InsightModels
{
    public partial class ActionPremisePlan
    {
        public int ClientId { get; set; }
        public string AccountId { get; set; }
        public string PremiseId { get; set; }
        public string ActionKey { get; set; }
        public string SubActionKey { get; set; }
        public string ActionStatus { get; set; }
        public DateTime ActionStatusDate { get; set; }
        public DateTime UpdateDate { get; set; }
        public DateTime CreateDate { get; set; }
        public string SourceKey { get; set; }
        public string AdditionalInfo { get; set; }
    }
}
