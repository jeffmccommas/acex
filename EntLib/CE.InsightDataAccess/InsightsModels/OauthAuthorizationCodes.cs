﻿using System;
using System.Collections.Generic;

namespace CE.InsightDataAccess.InsightModels
{
    public partial class OauthAuthorizationCodes
    {
        public string AuthorizationCode { get; set; }
        public long ClientId { get; set; }
        public string UserId { get; set; }
        public string RedirectUri { get; set; }
        public int Expires { get; set; }
        public string Scope { get; set; }
        public DateTime UtcDateTimeCreated { get; set; }
    }
}
