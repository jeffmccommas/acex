﻿using System;
using System.Collections.Generic;

namespace CE.InsightDataAccess.InsightModels
{
    public partial class Uom
    {
        public Uom()
        {
            BillBenchmark = new HashSet<BillBenchmark>();
        }

        public byte UomId { get; set; }
        public string Name { get; set; }

        public virtual ICollection<BillBenchmark> BillBenchmark { get; set; }
    }
}
