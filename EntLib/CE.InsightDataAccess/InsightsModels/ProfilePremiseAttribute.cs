﻿using System;
using System.Collections.Generic;

namespace CE.InsightDataAccess.InsightModels
{
    public partial class ProfilePremiseAttribute
    {
        public int ClientId { get; set; }
        public string AccountId { get; set; }
        public string PremiseId { get; set; }
        public string AttributeKey { get; set; }
        public string AttributeValue { get; set; }
        public string Source { get; set; }
        public string Criteria { get; set; }
        public DateTime EffectiveDate { get; set; }
    }
}
