﻿using System;
using System.Collections.Generic;

namespace CE.InsightDataAccess.InsightModels
{
    public partial class Bill
    {
        public int ClientId { get; set; }
        public string CustomerId { get; set; }
        public string AccountId { get; set; }
        public string PremiseId { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public string CommodityKey { get; set; }
        public decimal? TotalUnits { get; set; }
        public decimal? TotalCost { get; set; }
        public string UomKey { get; set; }
        public int? BillDays { get; set; }
    }
}
