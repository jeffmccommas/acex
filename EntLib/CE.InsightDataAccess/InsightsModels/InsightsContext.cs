﻿using System;
using CE.InsightDataAccess.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace CE.InsightDataAccess.InsightModels
{
    public partial class InsightsContext : DbContext
    {
        public virtual DbSet<AcceptedMeters> AcceptedMeters { get; set; }
        public virtual DbSet<ActionAccountPlan> ActionAccountPlan { get; set; }
        public virtual DbSet<ActionCustomerAccount> ActionCustomerAccount { get; set; }
        public virtual DbSet<ActionCustomerPlan> ActionCustomerPlan { get; set; }
        public virtual DbSet<ActionPremisePlan> ActionPremisePlan { get; set; }
        public virtual DbSet<Appliance> Appliance { get; set; }
        public virtual DbSet<AuthType> AuthType { get; set; }
        public virtual DbSet<BenchmarkGroup> BenchmarkGroup { get; set; }
        public virtual DbSet<Bill> Bill { get; set; }
        public virtual DbSet<BillBenchmark> BillBenchmark { get; set; }
        public virtual DbSet<BillDisagg> BillDisagg { get; set; }
        public virtual DbSet<Billing> Billing { get; set; }
        public virtual DbSet<Client> Client { get; set; }
        public virtual DbSet<ClientPortalConfig> ClientPortalConfig { get; set; }
        public virtual DbSet<ClientProperty> ClientProperty { get; set; }
        public virtual DbSet<ClientTabConditions> ClientTabConditions { get; set; }
        public virtual DbSet<Commodity> Commodity { get; set; }
        public virtual DbSet<ConfigArea> ConfigArea { get; set; }
        public virtual DbSet<ConfigAreaClient> ConfigAreaClient { get; set; }
        public virtual DbSet<ConfigAreaEndpoint> ConfigAreaEndpoint { get; set; }
        public virtual DbSet<ControlPanelLogin> ControlPanelLogin { get; set; }
        public virtual DbSet<ControlPanelLoginClient> ControlPanelLoginClient { get; set; }
        public virtual DbSet<Currency> Currency { get; set; }
        public virtual DbSet<CustomerWebToken> CustomerWebToken { get; set; }
        public virtual DbSet<EndUse> EndUse { get; set; }
        public virtual DbSet<Endpoint> Endpoint { get; set; }
        public virtual DbSet<EndpointTracking> EndpointTracking { get; set; }
        public virtual DbSet<Environment> Environment { get; set; }
        public virtual DbSet<EventLog> EventLog { get; set; }
        public virtual DbSet<FileDetail> FileDetail { get; set; }
        public virtual DbSet<FileSource> FileSource { get; set; }
        public virtual DbSet<FileType> FileType { get; set; }
        public virtual DbSet<Group> Group { get; set; }
        public virtual DbSet<GroupRole> GroupRole { get; set; }
        public virtual DbSet<Measure> Measure { get; set; }
        public virtual DbSet<OauthAccessTokens> OauthAccessTokens { get; set; }
        public virtual DbSet<OauthAuthorizationCodes> OauthAuthorizationCodes { get; set; }
        public virtual DbSet<OauthClients> OauthClients { get; set; }
        public virtual DbSet<OauthRefreshTokens> OauthRefreshTokens { get; set; }
        public virtual DbSet<OauthScopes> OauthScopes { get; set; }
        public virtual DbSet<PortalClient> PortalClient { get; set; }
        public virtual DbSet<PortalConfig> PortalConfig { get; set; }
        public virtual DbSet<PortalConfigType> PortalConfigType { get; set; }
        public virtual DbSet<PortalLogin> PortalLogin { get; set; }
        public virtual DbSet<PowerBiportalClient> PowerBiportalClient { get; set; }
        public virtual DbSet<PowerBiportalLogin> PowerBiportalLogin { get; set; }
        public virtual DbSet<ProfileAccountAttribute> ProfileAccountAttribute { get; set; }
        public virtual DbSet<ProfileCustomerAccount> ProfileCustomerAccount { get; set; }
        public virtual DbSet<ProfileCustomerAttribute> ProfileCustomerAttribute { get; set; }
        public virtual DbSet<ProfilePremiseAttribute> ProfilePremiseAttribute { get; set; }
        public virtual DbSet<Property> Property { get; set; }
        public virtual DbSet<QueueStatus> QueueStatus { get; set; }
        public virtual DbSet<RequestQueue> RequestQueue { get; set; }
        public virtual DbSet<Role> Role { get; set; }
        public virtual DbSet<Role1> Role1 { get; set; }
        public virtual DbSet<Uom> Uom { get; set; }
        public virtual DbSet<User> User { get; set; }
        public virtual DbSet<UserEndpoint> UserEndpoint { get; set; }
        public virtual DbSet<UserEnvironment> UserEnvironment { get; set; }
        public virtual DbSet<UserRole> UserRole { get; set; }

        // Unable to generate entity type for table 'ETL.PremiseAttributes_API'. Please see the warning messages.
        // Unable to generate entity type for table 'ETL.U_ProfileCustomerAccounts_API'. Please see the warning messages.
        // Unable to generate entity type for table 'ETL.PremiseActions_API'. Please see the warning messages.
        // Unable to generate entity type for table 'ETL.KEY_CustomerProfileAccounts_API'. Please see the warning messages.

        //protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        //{
        //    //#warning To protect potentially sensitive information in your connection string, you should move it out of source code. See http://go.microsoft.com/fwlink/?LinkId=723263 for guidance on storing connection strings.
        //    //optionsBuilder.UseSqlServer(@"Server=tcp:acedsql1c0.database.windows.net,1433;Database=Insights;User ID=Aclweb@acedsql1c0;Password=Acl@r@393;");
        //}

        public InsightsContext(DbContextOptions<InsightsContext> options) : base(options)
        {
        }

        public InsightsContext() : base()
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            //Configure default schema
            //modelBuilder.HasDefaultSchema("dbo");
            modelBuilder.Entity<PortalLogin>().ToTable("PortalLogin", "portal");
            modelBuilder.Entity<ClientPortalConfig>().ToTable("ClientPortalConfig", "portal");
            modelBuilder.Entity<PortalConfig>().ToTable("PortalConfig", "portal");
            modelBuilder.Entity<PortalConfigType>().ToTable("PortalConfigType", "portal");

            modelBuilder.Entity<AcceptedMeters>(entity =>
            {
                entity.HasKey(e => e.MeterId)
                    .HasName("PK_AcceptedMeters");

                entity.Property(e => e.MeterId).ValueGeneratedNever();

                entity.Property(e => e.ModifiedBy).HasMaxLength(50);

                entity.Property(e => e.ModifiedDate).HasColumnType("datetime");

                entity.Property(e => e.UpdateBy)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.UpdatedDate).HasColumnType("datetime");
            });

            modelBuilder.Entity<ActionAccountPlan>(entity =>
            {
                entity.HasKey(e => new { e.ClientId, e.AccountId, e.ActionKey })
                    .HasName("PK_ActionAccountPlan");

                entity.ToTable("ActionAccountPlan", "wh");

                entity.Property(e => e.ClientId).HasColumnName("ClientID");

                entity.Property(e => e.AccountId)
                    .HasColumnName("AccountID")
                    .HasColumnType("varchar(50)");

                entity.Property(e => e.ActionKey).HasColumnType("varchar(256)");

                entity.Property(e => e.ActionStatus).HasColumnType("varchar(256)");

                entity.Property(e => e.ActionStatusDate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("getdate()");

                entity.Property(e => e.CreateDate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("getdate()");

                entity.Property(e => e.UpdateDate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("getdate()");
            });

            modelBuilder.Entity<ActionCustomerAccount>(entity =>
            {
                entity.HasKey(e => new { e.ClientId, e.CustomerId, e.AccountId })
                    .HasName("PK_ActionCustomerAccount");

                entity.ToTable("ActionCustomerAccount", "wh");

                entity.Property(e => e.ClientId).HasColumnName("ClientID");

                entity.Property(e => e.CustomerId)
                    .HasColumnName("CustomerID")
                    .HasColumnType("varchar(50)");

                entity.Property(e => e.AccountId)
                    .HasColumnName("AccountID")
                    .HasColumnType("varchar(50)");
            });

            modelBuilder.Entity<ActionCustomerPlan>(entity =>
            {
                entity.HasKey(e => new { e.ClientId, e.CustomerId, e.ActionKey })
                    .HasName("PK_ActionCustomerPlan");

                entity.ToTable("ActionCustomerPlan", "wh");

                entity.Property(e => e.ClientId).HasColumnName("ClientID");

                entity.Property(e => e.CustomerId)
                    .HasColumnName("CustomerID")
                    .HasColumnType("varchar(50)");

                entity.Property(e => e.ActionKey).HasColumnType("varchar(256)");

                entity.Property(e => e.ActionStatus).HasColumnType("varchar(256)");

                entity.Property(e => e.ActionStatusDate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("getdate()");

                entity.Property(e => e.CreateDate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("getdate()");

                entity.Property(e => e.UpdateDate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("getdate()");
            });

            modelBuilder.Entity<ActionPremisePlan>(entity =>
            {
                entity.HasKey(e => new { e.ClientId, e.AccountId, e.PremiseId, e.ActionKey, e.SubActionKey })
                    .HasName("PK_ActionPremisePlan");

                entity.ToTable("ActionPremisePlan", "wh");

                entity.Property(e => e.ClientId).HasColumnName("ClientID");

                entity.Property(e => e.AccountId)
                    .HasColumnName("AccountID")
                    .HasColumnType("varchar(50)");

                entity.Property(e => e.PremiseId)
                    .HasColumnName("PremiseID")
                    .HasColumnType("varchar(50)");

                entity.Property(e => e.ActionKey).HasColumnType("varchar(64)");

                entity.Property(e => e.SubActionKey)
                    .HasColumnType("varchar(64)")
                    .HasDefaultValueSql("''");

                entity.Property(e => e.ActionStatus).HasColumnType("varchar(128)");

                entity.Property(e => e.ActionStatusDate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("getdate()");

                entity.Property(e => e.AdditionalInfo).HasColumnType("varchar(max)");

                entity.Property(e => e.CreateDate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("getdate()");

                entity.Property(e => e.SourceKey).HasColumnType("varchar(50)");

                entity.Property(e => e.UpdateDate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("getdate()");
            });

            modelBuilder.Entity<Appliance>(entity =>
            {
                entity.ToTable("Appliance", "wh");

                entity.Property(e => e.ApplianceId)
                    .HasColumnName("ApplianceID")
                    .ValueGeneratedNever();

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasColumnType("varchar(32)");
            });

            modelBuilder.Entity<AuthType>(entity =>
            {
                entity.Property(e => e.AuthTypeId)
                    .HasColumnName("AuthTypeID")
                    .ValueGeneratedNever();

                entity.Property(e => e.AuthDescription).HasColumnType("varchar(128)");

                entity.Property(e => e.AuthName)
                    .IsRequired()
                    .HasColumnType("varchar(32)");
            });

            modelBuilder.Entity<BenchmarkGroup>(entity =>
            {
                entity.HasKey(e => e.GroupId)
                    .HasName("PK_Group");

                entity.ToTable("BenchmarkGroup", "wh");

                entity.Property(e => e.GroupId)
                    .HasColumnName("GroupID")
                    .ValueGeneratedNever();

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasColumnType("varchar(32)");
            });

            modelBuilder.Entity<Bill>(entity =>
            {
                entity.HasKey(e => new { e.ClientId, e.CustomerId, e.AccountId, e.PremiseId, e.StartDate, e.EndDate, e.CommodityKey })
                    .HasName("PK_Bill");

                entity.ToTable("Bill", "wh");

                entity.Property(e => e.CustomerId)
                    .HasColumnName("CustomerID")
                    .HasColumnType("varchar(50)");

                entity.Property(e => e.AccountId)
                    .HasColumnName("AccountID")
                    .HasColumnType("varchar(50)");

                entity.Property(e => e.PremiseId)
                    .HasColumnName("PremiseID")
                    .HasColumnType("varchar(50)");

                entity.Property(e => e.StartDate).HasColumnType("datetime");

                entity.Property(e => e.EndDate).HasColumnType("datetime");

                entity.Property(e => e.CommodityKey).HasColumnType("varchar(50)");

                entity.Property(e => e.TotalCost).HasColumnType("decimal");

                entity.Property(e => e.TotalUnits).HasColumnType("decimal");

                entity.Property(e => e.UomKey).HasColumnType("varchar(16)");
            });

            modelBuilder.Entity<BillBenchmark>(entity =>
            {
                entity.HasKey(e => new { e.ClientId, e.CustomerId, e.AccountId, e.BillDate, e.PremiseId, e.GroupId, e.CommodityId })
                    .HasName("PK_BillBenchmark");

                entity.ToTable("BillBenchmark", "wh");

                entity.Property(e => e.ClientId).HasColumnName("ClientID");

                entity.Property(e => e.CustomerId)
                    .HasColumnName("CustomerID")
                    .HasColumnType("varchar(50)");

                entity.Property(e => e.AccountId)
                    .HasColumnName("AccountID")
                    .HasColumnType("varchar(50)");

                entity.Property(e => e.BillDate).HasColumnType("datetime");

                entity.Property(e => e.PremiseId)
                    .HasColumnName("PremiseID")
                    .HasColumnType("varchar(50)");

                entity.Property(e => e.GroupId).HasColumnName("GroupID");

                entity.Property(e => e.CommodityId).HasColumnName("CommodityID");

                entity.Property(e => e.CostAmount).HasColumnType("money");

                entity.Property(e => e.NewDate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("getdate()");

                entity.Property(e => e.UsageQuantity).HasColumnType("decimal");

                entity.Property(e => e.UsageUomId).HasColumnName("UsageUomID");

                entity.HasOne(d => d.Commodity)
                    .WithMany(p => p.BillBenchmark)
                    .HasForeignKey(d => d.CommodityId)
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_BillBenchmark_Commodity");

                entity.HasOne(d => d.Currency)
                    .WithMany(p => p.BillBenchmark)
                    .HasForeignKey(d => d.CurrencyId)
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_BillBenchmark_Currency");

                entity.HasOne(d => d.Group)
                    .WithMany(p => p.BillBenchmark)
                    .HasForeignKey(d => d.GroupId)
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_BillBenchmark_BenchmarkGroup");

                entity.HasOne(d => d.UsageUom)
                    .WithMany(p => p.BillBenchmark)
                    .HasForeignKey(d => d.UsageUomId)
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_BillBenchmark_Uom");
            });

            modelBuilder.Entity<BillDisagg>(entity =>
            {
                entity.HasKey(e => new { e.ClientId, e.CustomerId, e.AccountId, e.PremiseId, e.BillDate, e.EndUseKey, e.ApplianceKey, e.CommodityKey, e.PeriodId })
                    .HasName("PK_BillDisagg");

                entity.ToTable("BillDisagg", "wh");

                entity.Property(e => e.CustomerId).HasColumnType("varchar(50)");

                entity.Property(e => e.AccountId).HasColumnType("varchar(50)");

                entity.Property(e => e.PremiseId).HasColumnType("varchar(50)");

                entity.Property(e => e.BillDate).HasColumnType("datetime");

                entity.Property(e => e.EndUseKey).HasColumnType("varchar(50)");

                entity.Property(e => e.ApplianceKey).HasColumnType("varchar(50)");

                entity.Property(e => e.CommodityKey).HasColumnType("varchar(50)");

                entity.Property(e => e.Cost).HasColumnType("decimal");

                entity.Property(e => e.CurrencyKey)
                    .IsRequired()
                    .HasColumnType("varchar(16)");

                entity.Property(e => e.NewDate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("getdate()");

                entity.Property(e => e.ReconciliationRatio).HasColumnType("decimal");

                entity.Property(e => e.StatusId).HasDefaultValueSql("0");

                entity.Property(e => e.UomKey)
                    .IsRequired()
                    .HasColumnType("varchar(16)");

                entity.Property(e => e.Usage).HasColumnType("decimal");
            });

            modelBuilder.Entity<Billing>(entity =>
            {
                entity.HasKey(e => new { e.ClientId, e.CustomerId, e.AccountId, e.PremiseId, e.ServiceContractId, e.BillStartDate, e.BillEndDate })
                    .HasName("PK_Billing");

                entity.ToTable("Billing", "wh");

                entity.Property(e => e.CustomerId).HasColumnType("varchar(50)");

                entity.Property(e => e.AccountId).HasColumnType("varchar(50)");

                entity.Property(e => e.PremiseId).HasColumnType("varchar(50)");

                entity.Property(e => e.ServiceContractId).HasColumnType("varchar(64)");

                entity.Property(e => e.BillStartDate).HasColumnType("datetime");

                entity.Property(e => e.BillEndDate).HasColumnType("datetime");

                entity.Property(e => e.AmiendDate)
                    .HasColumnName("AMIEndDate")
                    .HasColumnType("datetime");

                entity.Property(e => e.AmistartDate)
                    .HasColumnName("AMIStartDate")
                    .HasColumnType("datetime");

                entity.Property(e => e.BillCycleScheduleId).HasColumnType("varchar(50)");

                entity.Property(e => e.CostOfUsage).HasColumnType("decimal");

                entity.Property(e => e.DueDate).HasColumnType("date");

                entity.Property(e => e.MeterId).HasColumnType("varchar(64)");

                entity.Property(e => e.MeterType).HasColumnType("varchar(64)");

                entity.Property(e => e.RateClass).HasColumnType("varchar(256)");

                entity.Property(e => e.ReadDate).HasColumnType("datetime");

                entity.Property(e => e.ReadQuality).HasColumnType("varchar(50)");

                entity.Property(e => e.ReplacedMeterId).HasColumnType("varchar(64)");

                entity.Property(e => e.ServicePointId).HasColumnType("varchar(64)");

                entity.Property(e => e.TotalUsage).HasColumnType("decimal");

                entity.Property(e => e.Uomid).HasColumnName("UOMId");
            });

            modelBuilder.Entity<Client>(entity =>
            {
                entity.Property(e => e.ClientId)
                    .HasColumnName("ClientID")
                    .ValueGeneratedNever();

                entity.Property(e => e.AuthType).HasDefaultValueSql("0");

                entity.Property(e => e.Description).HasColumnType("varchar(128)");

                entity.Property(e => e.EnableInd).HasDefaultValueSql("1");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasColumnType("varchar(64)");

                entity.Property(e => e.NewDate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("getdate()");

                entity.Property(e => e.RateCompanyId).HasColumnName("RateCompanyID");

                entity.Property(e => e.ReferrerId).HasColumnName("ReferrerID");

                entity.Property(e => e.UpdDate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("getdate()");
            });

            modelBuilder.Entity<ClientPortalConfig>(entity =>
            {
                entity.Property(e => e.ClientId).HasColumnName("ClientID");

                entity.Property(e => e.PortalConfigValue)
                    .IsRequired()
                    .HasColumnType("varchar(100)");

                entity.HasOne(d => d.Client)
                    .WithMany(p => p.ClientPortalConfig)
                    .HasForeignKey(d => d.ClientId)
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_ClientPortalConfigId_Client");

                entity.HasOne(d => d.PortalConfig)
                    .WithMany(p => p.ClientPortalConfig)
                    .HasForeignKey(d => d.PortalConfigId)
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_ClientPortalConfigId_PortalConfig");
            });

            modelBuilder.Entity<ClientProperty>(entity =>
            {
                entity.HasKey(e => new { e.ClientId, e.EnvId, e.PropertyId })
                    .HasName("PK_ClientProperty_1");

                entity.Property(e => e.ClientId).HasColumnName("ClientID");

                entity.Property(e => e.EnvId).HasColumnName("EnvID");

                entity.Property(e => e.PropertyId).HasColumnName("PropertyID");

                entity.Property(e => e.PropValue).HasColumnType("varchar(256)");

                entity.HasOne(d => d.Client)
                    .WithMany(p => p.ClientProperty)
                    .HasForeignKey(d => d.ClientId)
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_ClientProperty_Client");

                entity.HasOne(d => d.Env)
                    .WithMany(p => p.ClientProperty)
                    .HasForeignKey(d => d.EnvId)
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_ClientProperty_Environment");

                entity.HasOne(d => d.Property)
                    .WithMany(p => p.ClientProperty)
                    .HasForeignKey(d => d.PropertyId)
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_ClientProperty_Property");
            });

            modelBuilder.Entity<ClientTabConditions>(entity =>
            {
                entity.HasKey(e => new { e.ClientId, e.TabId, e.GroupId, e.RoleId })
                    .HasName("PK_ClientTabConditions");

                entity.ToTable("ClientTabConditions", "auth");

                entity.Property(e => e.ClientId).HasColumnName("ClientID");

                entity.Property(e => e.TabId)
                    .HasColumnName("TabID")
                    .HasColumnType("varchar(50)");

                entity.Property(e => e.GroupId).HasColumnName("GroupID");

                entity.Property(e => e.RoleId).HasColumnName("RoleID");
            });

            modelBuilder.Entity<Commodity>(entity =>
            {
                entity.ToTable("Commodity", "wh");

                entity.Property(e => e.CommodityId).ValueGeneratedNever();

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasColumnType("varchar(16)");
            });

            modelBuilder.Entity<ConfigArea>(entity =>
            {
                entity.Property(e => e.ConfigAreaId)
                    .HasColumnName("ConfigAreaID")
                    .ValueGeneratedNever();

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasColumnType("varchar(32)");
            });

            modelBuilder.Entity<ConfigAreaClient>(entity =>
            {
                entity.HasKey(e => new { e.ConfigAreaId, e.ClientId })
                    .HasName("PK_ConfigAreaReferrer");

                entity.Property(e => e.ConfigAreaId).HasColumnName("ConfigAreaID");

                entity.Property(e => e.ClientId).HasColumnName("ClientID");

                entity.Property(e => e.ConfigData).HasColumnType("varchar(max)");

                entity.HasOne(d => d.ConfigArea)
                    .WithMany(p => p.ConfigAreaClient)
                    .HasForeignKey(d => d.ConfigAreaId)
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_ConfigAreaClient_ConfigArea");
            });

            modelBuilder.Entity<ConfigAreaEndpoint>(entity =>
            {
                entity.HasKey(e => new { e.ConfigAreaId, e.EndpointId })
                    .HasName("PK_ConfigAreaEndpoint");

                entity.Property(e => e.ConfigAreaId).HasColumnName("ConfigAreaID");

                entity.Property(e => e.EndpointId).HasColumnName("EndpointID");

                entity.HasOne(d => d.ConfigArea)
                    .WithMany(p => p.ConfigAreaEndpoint)
                    .HasForeignKey(d => d.ConfigAreaId)
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_ConfigAreaEndpoint_ConfigArea");

                entity.HasOne(d => d.Endpoint)
                    .WithMany(p => p.ConfigAreaEndpoint)
                    .HasForeignKey(d => d.EndpointId)
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_ConfigAreaEndpoint_Endpoint");
            });

            modelBuilder.Entity<ControlPanelLogin>(entity =>
            {
                entity.HasKey(e => e.CpId)
                    .HasName("PK_ControlPanelLogin");

                entity.ToTable("ControlPanelLogin", "cp");

                entity.HasIndex(e => e.UserName)
                    .HasName("UQ_ControlPanelLogin_Username")
                    .IsUnique();

                entity.Property(e => e.CpId).HasColumnName("CpID");

                entity.Property(e => e.EnableInd).HasDefaultValueSql("1");

                entity.Property(e => e.FirstName)
                    .IsRequired()
                    .HasColumnType("varchar(32)");

                entity.Property(e => e.LastName)
                    .IsRequired()
                    .HasColumnType("varchar(32)");

                entity.Property(e => e.NewDate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("getdate()");

                entity.Property(e => e.PasswordHash).HasColumnType("varchar(128)");

                entity.Property(e => e.PasswordSalt).HasColumnType("varchar(64)");

                entity.Property(e => e.SuperUserInd).HasDefaultValueSql("0");

                entity.Property(e => e.UpdDate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("getdate()");

                entity.Property(e => e.UserName)
                    .IsRequired()
                    .HasColumnType("varchar(20)");
            });

            modelBuilder.Entity<ControlPanelLoginClient>(entity =>
            {
                entity.HasKey(e => new { e.CpId, e.ClientId })
                    .HasName("PK_ControlPanelLoginClient");

                entity.ToTable("ControlPanelLoginClient", "cp");

                entity.Property(e => e.CpId).HasColumnName("CpID");

                entity.Property(e => e.ClientId).HasColumnName("ClientID");

                entity.HasOne(d => d.Cp)
                    .WithMany(p => p.ControlPanelLoginClient)
                    .HasForeignKey(d => d.CpId)
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_ControlPanelLoginClient_ControlPanelLogin");
            });

            modelBuilder.Entity<Currency>(entity =>
            {
                entity.ToTable("Currency", "wh");

                entity.Property(e => e.CurrencyId)
                    .HasColumnName("CurrencyID")
                    .ValueGeneratedNever();

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasColumnType("varchar(8)");
            });

            modelBuilder.Entity<CustomerWebToken>(entity =>
            {
                entity.HasKey(e => new { e.ClientId, e.CustomerId, e.WebToken })
                    .HasName("PK_CustomerWebToken");

                entity.HasIndex(e => e.UtcDateTimeUpdated)
                    .HasName("IX_CustomerWebToken");

                entity.Property(e => e.ClientId).HasColumnName("ClientID");

                entity.Property(e => e.CustomerId)
                    .HasColumnName("CustomerID")
                    .HasColumnType("varchar(50)");

                entity.Property(e => e.WebToken).HasColumnType("varchar(50)");

                entity.Property(e => e.AdditionalInfo).HasColumnType("varchar(max)");

                entity.Property(e => e.GroupId).HasColumnName("GroupID");

                entity.Property(e => e.RoleId).HasColumnName("RoleID");

                entity.Property(e => e.UserId)
                    .HasColumnName("UserID")
                    .HasColumnType("varchar(50)");

                entity.Property(e => e.UtcDateTimeCreated).HasColumnType("datetime");

                entity.Property(e => e.UtcDateTimeUpdated).HasColumnType("datetime");

                entity.HasOne(d => d.Client)
                    .WithMany(p => p.CustomerWebToken)
                    .HasForeignKey(d => d.ClientId)
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_CustomerWebToken_Client");
            });

            modelBuilder.Entity<EndUse>(entity =>
            {
                entity.ToTable("EndUse", "wh");

                entity.Property(e => e.EndUseId)
                    .HasColumnName("EndUseID")
                    .ValueGeneratedNever();

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasColumnType("varchar(32)");
            });

            modelBuilder.Entity<Endpoint>(entity =>
            {
                entity.Property(e => e.EndpointId)
                    .HasColumnName("EndpointID")
                    .ValueGeneratedNever();

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasColumnType("varchar(64)");

                entity.Property(e => e.ShortName).HasColumnType("varchar(32)");

                entity.Property(e => e.SortOrder).HasDefaultValueSql("0");
            });

            modelBuilder.Entity<EndpointTracking>(entity =>
            {
                entity.HasKey(e => new { e.EndpointTrackingId, e.PartitionKey })
                    .HasName("PK_EndpointTracking");

                entity.HasIndex(e => new { e.ResourceName, e.ClientId, e.IncludeInd, e.LogDate })
                    .HasName("ix_EndpointTracking_ClientID_IncludeInd_LogDate_includes");

                entity.HasIndex(e => new { e.UserId, e.Verb, e.ClientId, e.ResourceName, e.IncludeInd, e.LogDate })
                    .HasName("IX_EndPoint_Tracking");

                entity.Property(e => e.EndpointTrackingId)
                    .HasColumnName("EndpointTrackingID")
                    .ValueGeneratedOnAdd();

                entity.Property(e => e.ClientId)
                    .HasColumnName("ClientID")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.EndpointId).HasColumnName("EndpointID");

                entity.Property(e => e.LogDate).HasColumnType("datetime");

                entity.Property(e => e.MachineName).HasColumnType("varchar(50)");

                entity.Property(e => e.NewDate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("getutcdate()");

                entity.Property(e => e.Query).HasColumnType("varchar(256)");

                entity.Property(e => e.ResourceName).HasColumnType("varchar(64)");

                entity.Property(e => e.SourceIpaddress)
                    .HasColumnName("SourceIPAddress")
                    .HasColumnType("varchar(32)");

                entity.Property(e => e.UserAgent).HasColumnType("varchar(128)");

                entity.Property(e => e.UserId).HasColumnName("UserID");

                entity.Property(e => e.Verb).HasColumnType("varchar(8)");

                entity.Property(e => e.Xcechannel)
                    .HasColumnName("XCEChannel")
                    .HasColumnType("varchar(64)");

                entity.Property(e => e.Xcelocale)
                    .HasColumnName("XCELocale")
                    .HasColumnType("varchar(64)");

                entity.Property(e => e.XcemessageId)
                    .HasColumnName("XCEMessageId")
                    .HasColumnType("varchar(64)");

                entity.Property(e => e.Xcemeta)
                    .HasColumnName("XCEMeta")
                    .HasColumnType("varchar(64)");
            });

            modelBuilder.Entity<Environment>(entity =>
            {
                entity.HasKey(e => e.EnvId)
                    .HasName("PK_Environment");

                entity.Property(e => e.EnvId)
                    .HasColumnName("EnvID")
                    .ValueGeneratedNever();

                entity.Property(e => e.Description).HasColumnType("varchar(64)");

                entity.Property(e => e.Name).HasColumnType("varchar(24)");
            });

            modelBuilder.Entity<EventLog>(entity =>
            {
                entity.HasKey(e => new { e.LogId, e.PartitionKey })
                    .HasName("PK_EventLog");

                entity.Property(e => e.LogId)
                    .HasColumnName("LogID")
                    .ValueGeneratedOnAdd();

                entity.Property(e => e.EventLevel).HasColumnType("varchar(50)");

                entity.Property(e => e.Exception).HasColumnType("varchar(max)");

                entity.Property(e => e.LogDate).HasColumnType("datetime");

                entity.Property(e => e.LogMessage).HasColumnType("varchar(max)");

                entity.Property(e => e.LoggerName).HasColumnType("varchar(500)");

                entity.Property(e => e.MachineName).HasColumnType("varchar(100)");

                entity.Property(e => e.NewDate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("getutcdate()");

                entity.Property(e => e.UserId).HasColumnName("UserID");
            });

            modelBuilder.Entity<FileDetail>(entity =>
            {
                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.AccountId)
                    .IsRequired()
                    .HasColumnType("varchar(50)");

                entity.Property(e => e.CreateDate).HasColumnType("datetime");

                entity.Property(e => e.CustomerId)
                    .IsRequired()
                    .HasColumnType("varchar(50)");

                entity.Property(e => e.Description).HasColumnType("varchar(500)");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasColumnType("varchar(120)");

                entity.Property(e => e.PremiseId)
                    .IsRequired()
                    .HasColumnType("varchar(50)");

                entity.Property(e => e.Title).HasColumnType("varchar(100)");

                entity.Property(e => e.UserId).HasColumnType("varchar(50)");
            });

            modelBuilder.Entity<FileSource>(entity =>
            {
                entity.HasKey(e => e.SourceId)
                    .HasName("PK_FileSource");

                entity.Property(e => e.SourceId).HasColumnName("SourceID");

                entity.Property(e => e.SourceDescription).HasColumnType("varchar(100)");

                entity.Property(e => e.SourceName)
                    .IsRequired()
                    .HasColumnType("varchar(50)");
            });

            modelBuilder.Entity<FileType>(entity =>
            {
                entity.HasKey(e => e.TypeId)
                    .HasName("PK_FileType");

                entity.Property(e => e.TypeId).HasColumnName("TypeID");

                entity.Property(e => e.TypeDescription).HasColumnType("varchar(100)");

                entity.Property(e => e.TypeExtension)
                    .IsRequired()
                    .HasColumnType("varchar(50)");
            });

            modelBuilder.Entity<Group>(entity =>
            {
                entity.ToTable("Group", "auth");

                entity.Property(e => e.GroupId)
                    .HasColumnName("GroupID")
                    .ValueGeneratedNever();

                entity.Property(e => e.GroupName)
                    .IsRequired()
                    .HasColumnType("varchar(50)");
            });

            modelBuilder.Entity<GroupRole>(entity =>
            {
                entity.HasKey(e => new { e.GroupId, e.RoleId })
                    .HasName("PK_GroupRole");

                entity.ToTable("GroupRole", "auth");

                entity.Property(e => e.GroupId).HasColumnName("GroupID");

                entity.Property(e => e.RoleId).HasColumnName("RoleID");
            });

            modelBuilder.Entity<Measure>(entity =>
            {
                entity.HasKey(e => new { e.ClientId, e.CustomerId, e.AccountId, e.PremiseId, e.ActionKey })
                    .HasName("PK_Measure");

                entity.ToTable("Measure", "wh");

                entity.Property(e => e.CustomerId).HasColumnType("varchar(50)");

                entity.Property(e => e.AccountId).HasColumnType("varchar(50)");

                entity.Property(e => e.PremiseId).HasColumnType("varchar(50)");

                entity.Property(e => e.ActionKey).HasColumnType("varchar(50)");

                entity.Property(e => e.ActionTypeKey)
                    .IsRequired()
                    .HasColumnType("varchar(50)");

                entity.Property(e => e.AnnualCost).HasColumnType("decimal");

                entity.Property(e => e.AnnualCostVariancePercent).HasColumnType("decimal");

                entity.Property(e => e.AnnualSavingsEstimate).HasColumnType("decimal");

                entity.Property(e => e.AnnualSavingsEstimateCurrencyKey)
                    .IsRequired()
                    .HasColumnType("varchar(16)");

                entity.Property(e => e.CommodityKey)
                    .IsRequired()
                    .HasColumnType("varchar(50)");

                entity.Property(e => e.ElecSavEst).HasColumnType("decimal");

                entity.Property(e => e.ElecSavEstCurrencyKey).HasColumnType("varchar(16)");

                entity.Property(e => e.ElecUsgSavEst).HasColumnType("decimal");

                entity.Property(e => e.ElecUsgSavEstUomKey).HasColumnType("varchar(16)");

                entity.Property(e => e.GasSavEst).HasColumnType("decimal");

                entity.Property(e => e.GasSavEstCurrencyKey).HasColumnType("varchar(16)");

                entity.Property(e => e.GasUsgSavEst).HasColumnType("decimal");

                entity.Property(e => e.GasUsgSavEstUomKey).HasColumnType("varchar(16)");

                entity.Property(e => e.NewDate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("getutcdate()");

                entity.Property(e => e.Payback).HasColumnType("decimal");

                entity.Property(e => e.Priority).HasDefaultValueSql("0");

                entity.Property(e => e.RebateAmount).HasColumnType("decimal");

                entity.Property(e => e.Roi).HasColumnType("decimal");

                entity.Property(e => e.UpfrontCost).HasColumnType("decimal");

                entity.Property(e => e.WaterSavEst).HasColumnType("decimal");

                entity.Property(e => e.WaterSavEstCurrencyKey).HasColumnType("varchar(16)");

                entity.Property(e => e.WaterUsgSavEst).HasColumnType("decimal");

                entity.Property(e => e.WaterUsgSavEstUomKey).HasColumnType("varchar(16)");
            });

            modelBuilder.Entity<OauthAccessTokens>(entity =>
            {
                entity.HasKey(e => e.AccessToken)
                    .HasName("access_token_pk");

                entity.ToTable("oauth_access_tokens", "oauth2");

                entity.HasIndex(e => new { e.AccessToken, e.ClientId })
                    .HasName("access_token_client_id")
                    .IsUnique();

                entity.Property(e => e.AccessToken)
                    .HasColumnName("access_token")
                    .HasColumnType("varchar(250)");

                entity.Property(e => e.ClientId).HasColumnName("client_id");

                entity.Property(e => e.Expires).HasColumnName("expires");

                entity.Property(e => e.Scope)
                    .HasColumnName("scope")
                    .HasColumnType("varchar(2000)");

                entity.Property(e => e.UserId)
                    .HasColumnName("user_id")
                    .HasColumnType("varchar(255)");

                entity.Property(e => e.UtcDateTimeCreated)
                    .HasColumnName("utc_date_time_created")
                    .HasColumnType("datetime");
            });

            modelBuilder.Entity<OauthAuthorizationCodes>(entity =>
            {
                entity.HasKey(e => e.AuthorizationCode)
                    .HasName("auth_code_pk");

                entity.ToTable("oauth_authorization_codes", "oauth2");

                entity.HasIndex(e => new { e.AuthorizationCode, e.ClientId })
                    .HasName("authorization_codes_client_id")
                    .IsUnique();

                entity.Property(e => e.AuthorizationCode)
                    .HasColumnName("authorization_code")
                    .HasColumnType("varchar(50)");

                entity.Property(e => e.ClientId).HasColumnName("client_id");

                entity.Property(e => e.Expires).HasColumnName("expires");

                entity.Property(e => e.RedirectUri)
                    .HasColumnName("redirect_uri")
                    .HasColumnType("varchar(2000)");

                entity.Property(e => e.Scope)
                    .HasColumnName("scope")
                    .HasColumnType("varchar(2000)");

                entity.Property(e => e.UserId)
                    .HasColumnName("user_id")
                    .HasColumnType("varchar(255)");

                entity.Property(e => e.UtcDateTimeCreated)
                    .HasColumnName("utc_date_time_created")
                    .HasColumnType("datetime");
            });

            modelBuilder.Entity<OauthClients>(entity =>
            {
                entity.HasKey(e => e.ClientId)
                    .HasName("clients_client_id_pk");

                entity.ToTable("oauth_clients", "oauth2");

                entity.Property(e => e.ClientId).HasColumnName("client_id");

                entity.Property(e => e.Active).HasColumnName("active");

                entity.Property(e => e.ClientDescription)
                    .HasColumnName("client_description")
                    .HasColumnType("varchar(2000)");

                entity.Property(e => e.ClientName)
                    .HasColumnName("client_name")
                    .HasColumnType("varchar(255)");

                entity.Property(e => e.ClientSecret)
                    .HasColumnName("client_secret")
                    .HasColumnType("varchar(80)");

                entity.Property(e => e.GrantTypes)
                    .HasColumnName("grant_types")
                    .HasColumnType("varchar(80)");

                entity.Property(e => e.RedirectUri)
                    .IsRequired()
                    .HasColumnName("redirect_uri")
                    .HasColumnType("varchar(2000)");

                entity.Property(e => e.Scope)
                    .HasColumnName("scope")
                    .HasColumnType("varchar(2000)");

                entity.Property(e => e.UserId)
                    .HasColumnName("user_id")
                    .HasColumnType("varchar(255)");

                entity.Property(e => e.UtcDateTimeCreated)
                    .HasColumnName("utc_date_time_created")
                    .HasColumnType("datetime");
            });

            modelBuilder.Entity<OauthRefreshTokens>(entity =>
            {
                entity.HasKey(e => e.RefreshToken)
                    .HasName("refresh_token_pk");

                entity.ToTable("oauth_refresh_tokens", "oauth2");

                entity.HasIndex(e => new { e.RefreshToken, e.ClientId })
                    .HasName("refresh_tokens_client_id")
                    .IsUnique();

                entity.Property(e => e.RefreshToken)
                    .HasColumnName("refresh_token")
                    .HasColumnType("varchar(50)");

                entity.Property(e => e.ClientId).HasColumnName("client_id");

                entity.Property(e => e.Expires).HasColumnName("expires");

                entity.Property(e => e.Scope)
                    .HasColumnName("scope")
                    .HasColumnType("varchar(2000)");

                entity.Property(e => e.UserId)
                    .HasColumnName("user_id")
                    .HasColumnType("varchar(255)");

                entity.Property(e => e.UtcDateTimeCreated)
                    .HasColumnName("utc_date_time_created")
                    .HasColumnType("datetime");
            });

            modelBuilder.Entity<OauthScopes>(entity =>
            {
                entity.HasKey(e => e.Scope)
                    .HasName("oauth_scopes_pk");

                entity.ToTable("oauth_scopes", "oauth2");

                entity.Property(e => e.Scope)
                    .HasColumnName("scope")
                    .HasColumnType("varchar(50)");

                entity.Property(e => e.IsDefault).HasColumnName("is_default");
            });

            modelBuilder.Entity<PortalClient>(entity =>
            {
                entity.HasKey(e => e.ClientId)
                    .HasName("PK_ClientID");

                entity.Property(e => e.ClientId).HasColumnName("ClientID");

                entity.Property(e => e.ClientName).HasColumnType("varchar(64)");

                entity.Property(e => e.Description).HasColumnType("varchar(128)");

                entity.Property(e => e.EnvId).HasColumnName("EnvID");

                entity.HasOne(d => d.Env)
                    .WithMany(p => p.PortalClient)
                    .HasForeignKey(d => d.EnvId)
                    .HasConstraintName("FK_PortalClient_EnvID");
            });

            modelBuilder.Entity<PortalConfig>(entity =>
            {
                entity.Property(e => e.PortalConfigName)
                    .IsRequired()
                    .HasColumnType("varchar(100)");

                entity.HasOne(d => d.PortalConfigType)
                    .WithMany(p => p.PortalConfig)
                    .HasForeignKey(d => d.PortalConfigTypeId)
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_PortalConfig_PortalConfigType");
            });

            modelBuilder.Entity<PortalConfigType>(entity =>
            {
                entity.Property(e => e.PortalConfigTypeName)
                    .IsRequired()
                    .HasColumnType("varchar(100)");
            });

            modelBuilder.Entity<PortalLogin>(entity =>
            {
                entity.HasKey(e => e.UserId)
                    .HasName("PK_PortalLogin_UserID");

                entity.HasIndex(e => new { e.UserId, e.ClientId, e.EnvId })
                    .HasName("UQ_PortalLogin_Username_ClientID_EnvID")
                    .IsUnique();

                entity.Property(e => e.UserId).HasColumnName("UserID");

                entity.Property(e => e.ClientId).HasColumnName("ClientID");

                entity.Property(e => e.EnvId).HasColumnName("EnvID");

                entity.Property(e => e.FirstName).HasColumnType("varchar(32)");

                entity.Property(e => e.LastName).HasColumnType("varchar(32)");

                entity.Property(e => e.PasswordHash).HasColumnType("varchar(128)");

                entity.Property(e => e.PasswordSalt).HasColumnType("varchar(64)");

                entity.Property(e => e.UpdateDate).HasColumnType("datetime");

                entity.Property(e => e.UserType).HasColumnType("varchar(5)");
                
                entity.Property(e => e.FailedLoginCount).HasColumnType("int");

                entity.Property(e => e.Username)
                    .IsRequired()
                    .HasColumnType("varchar(255)");

                entity.HasOne(d => d.Client)
                    .WithMany(p => p.PortalLogin)
                    .HasForeignKey(d => d.ClientId)
                    .HasConstraintName("FK_PortalLogin_ClientID");

                entity.HasOne(d => d.Env)
                    .WithMany(p => p.PortalLogin)
                    .HasForeignKey(d => d.EnvId)
                    .HasConstraintName("FK_PortalLogin_EnvID");
            });

            modelBuilder.Entity<PowerBiportalClient>(entity =>
            {
                entity.HasKey(e => e.ClientId)
                    .HasName("PK__PowerBIP__E67E1A041520D0C6");

                entity.ToTable("PowerBIPortalClient");

                entity.Property(e => e.ClientId)
                    .HasColumnName("ClientID")
                    .ValueGeneratedNever();

                entity.Property(e => e.ClientName).HasColumnType("varchar(64)");

                entity.Property(e => e.Description).HasColumnType("varchar(128)");

                entity.Property(e => e.EnvId).HasColumnName("EnvID");

                entity.Property(e => e.PbiworkspaceId)
                    .IsRequired()
                    .HasColumnName("PBIWorkspaceID")
                    .HasColumnType("varchar(64)");

                entity.HasOne(d => d.Env)
                    .WithMany(p => p.PowerBiportalClient)
                    .HasForeignKey(d => d.EnvId)
                    .HasConstraintName("FK_PowerBIPortlClient_EnvID");
            });

            modelBuilder.Entity<PowerBiportalLogin>(entity =>
            {
                entity.HasKey(e => e.UserId)
                    .HasName("PK_PowerBIPortalLogin_UserID");

                entity.ToTable("PowerBIPortalLogin");

                entity.HasIndex(e => new { e.Username, e.ClientId, e.EnvId })
                    .HasName("UQ_PowerBIPortalLogin_Username_ClientID_EnvID")
                    .IsUnique();

                entity.Property(e => e.UserId).HasColumnName("UserID");

                entity.Property(e => e.ClientId).HasColumnName("ClientID");

                entity.Property(e => e.EnvId).HasColumnName("EnvID");

                entity.Property(e => e.FirstName).HasColumnType("varchar(32)");

                entity.Property(e => e.LastName).HasColumnType("varchar(32)");

                entity.Property(e => e.PasswordHash)
                    .IsRequired()
                    .HasColumnType("varchar(128)");

                entity.Property(e => e.PasswordSalt)
                    .IsRequired()
                    .HasColumnType("varchar(64)");

                entity.Property(e => e.UpdateDate).HasColumnType("datetime");

                entity.Property(e => e.UserType).HasColumnType("varchar(5)");

                entity.Property(e => e.Username)
                    .IsRequired()
                    .HasColumnType("varchar(20)");

                entity.HasOne(d => d.Client)
                    .WithMany(p => p.PowerBiportalLogin)
                    .HasForeignKey(d => d.ClientId)
                    .HasConstraintName("FK_PowerBIPortalLogin_ClientID");

                entity.HasOne(d => d.Env)
                    .WithMany(p => p.PowerBiportalLogin)
                    .HasForeignKey(d => d.EnvId)
                    .HasConstraintName("FK_PowerBIPortalLogin_EnvID");
            });

            modelBuilder.Entity<ProfileAccountAttribute>(entity =>
            {
                entity.HasKey(e => new { e.ClientId, e.AccountId, e.AttributeKey })
                    .HasName("PK_ProfileAccountAttribute");

                entity.ToTable("ProfileAccountAttribute", "wh");

                entity.Property(e => e.ClientId).HasColumnName("ClientID");

                entity.Property(e => e.AccountId)
                    .HasColumnName("AccountID")
                    .HasColumnType("varchar(50)");

                entity.Property(e => e.AttributeKey).HasColumnType("varchar(64)");

                entity.Property(e => e.AttributeValue).HasColumnType("varchar(128)");

                entity.Property(e => e.Criteria).HasColumnType("varchar(64)");

                entity.Property(e => e.EffectiveDate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("getdate()");

                entity.Property(e => e.Source).HasColumnType("varchar(256)");
            });

            modelBuilder.Entity<ProfileCustomerAccount>(entity =>
            {
                entity.HasKey(e => new { e.ClientId, e.CustomerId, e.AccountId })
                    .HasName("PK_ProfileCustomerAccount");

                entity.ToTable("ProfileCustomerAccount", "wh");

                entity.Property(e => e.ClientId).HasColumnName("ClientID");

                entity.Property(e => e.CustomerId)
                    .HasColumnName("CustomerID")
                    .HasColumnType("varchar(50)");

                entity.Property(e => e.AccountId)
                    .HasColumnName("AccountID")
                    .HasColumnType("varchar(50)");

                entity.Property(e => e.EmailAddress).HasMaxLength(255);

                entity.Property(e => e.PhoneNumber).HasMaxLength(15);

                entity.Property(e => e.UpdateFromEtldate)
                    .HasColumnName("UpdateFromETLDate")
                    .HasColumnType("datetime");

                entity.Property(e => e.UpdateFromUidate)
                    .HasColumnName("UpdateFromUIDate")
                    .HasColumnType("datetime");
            });

            modelBuilder.Entity<ProfileCustomerAttribute>(entity =>
            {
                entity.HasKey(e => new { e.ClientId, e.CustomerId, e.AttributeKey })
                    .HasName("PK_ProfileCustomerAttribute");

                entity.ToTable("ProfileCustomerAttribute", "wh");

                entity.Property(e => e.ClientId).HasColumnName("ClientID");

                entity.Property(e => e.CustomerId)
                    .HasColumnName("CustomerID")
                    .HasColumnType("varchar(50)");

                entity.Property(e => e.AttributeKey).HasColumnType("varchar(64)");

                entity.Property(e => e.AttributeValue).HasColumnType("varchar(128)");

                entity.Property(e => e.Criteria).HasColumnType("varchar(64)");

                entity.Property(e => e.EffectiveDate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("getdate()");

                entity.Property(e => e.Source).HasColumnType("varchar(256)");
            });

            modelBuilder.Entity<ProfilePremiseAttribute>(entity =>
            {
                entity.HasKey(e => new { e.ClientId, e.AccountId, e.PremiseId, e.AttributeKey })
                    .HasName("PK_ProfilePremiseAttribute");

                entity.ToTable("ProfilePremiseAttribute", "wh");

                entity.HasIndex(e => new { e.AccountId, e.AttributeKey, e.AttributeValue, e.PremiseId, e.Source, e.ClientId, e.EffectiveDate })
                    .HasName("IX_ProfilePremiseAttribute");

                entity.Property(e => e.ClientId).HasColumnName("ClientID");

                entity.Property(e => e.AccountId)
                    .HasColumnName("AccountID")
                    .HasColumnType("varchar(50)");

                entity.Property(e => e.PremiseId)
                    .HasColumnName("PremiseID")
                    .HasColumnType("varchar(50)");

                entity.Property(e => e.AttributeKey).HasColumnType("varchar(64)");

                entity.Property(e => e.AttributeValue).HasColumnType("varchar(128)");

                entity.Property(e => e.Criteria).HasColumnType("varchar(64)");

                entity.Property(e => e.EffectiveDate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("getdate()");

                entity.Property(e => e.Source).HasColumnType("varchar(256)");
            });

            modelBuilder.Entity<Property>(entity =>
            {
                entity.Property(e => e.PropertyId)
                    .HasColumnName("PropertyID")
                    .ValueGeneratedNever();

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasColumnType("varchar(64)");
            });

            modelBuilder.Entity<QueueStatus>(entity =>
            {
                entity.Property(e => e.QueueStatusId)
                    .HasColumnName("QueueStatusID")
                    .ValueGeneratedNever();

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasColumnType("varchar(32)");
            });

            modelBuilder.Entity<RequestQueue>(entity =>
            {
                entity.Property(e => e.RequestQueueId).HasColumnName("RequestQueueID");

                entity.Property(e => e.EndpointId).HasColumnName("EndpointID");

                entity.Property(e => e.NewDate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("getdate()");

                entity.Property(e => e.QueueStatusId).HasColumnName("QueueStatusID");

                entity.Property(e => e.RequestData).HasColumnType("varchar(max)");

                entity.Property(e => e.RequestGuid).HasDefaultValueSql("newsequentialid()");

                entity.Property(e => e.ResponseData).HasColumnType("varchar(max)");

                entity.Property(e => e.UpdDate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("getdate()");

                entity.Property(e => e.UserId).HasColumnName("UserID");

                entity.Property(e => e.Verb).HasColumnType("varchar(8)");

                entity.HasOne(d => d.QueueStatus)
                    .WithMany(p => p.RequestQueue)
                    .HasForeignKey(d => d.QueueStatusId)
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_RequestQueue_QueueStatus");
            });

            modelBuilder.Entity<Role>(entity =>
            {
                entity.Property(e => e.RoleId)
                    .HasColumnName("RoleID")
                    .ValueGeneratedNever();

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasColumnType("varchar(32)");
            });

            modelBuilder.Entity<Role1>(entity =>
            {
                entity.HasKey(e => e.RoleId)
                    .HasName("PK_Role_1");

                entity.ToTable("Role", "auth");

                entity.Property(e => e.RoleId)
                    .HasColumnName("RoleID")
                    .ValueGeneratedNever();

                entity.Property(e => e.RoleName)
                    .IsRequired()
                    .HasColumnType("varchar(50)");
            });

            modelBuilder.Entity<Uom>(entity =>
            {
                entity.ToTable("Uom", "wh");

                entity.Property(e => e.UomId)
                    .HasColumnName("UomID")
                    .ValueGeneratedNever();

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasColumnType("varchar(16)");
            });

            modelBuilder.Entity<User>(entity =>
            {
                entity.Property(e => e.UserId).HasColumnName("UserID");

                entity.Property(e => e.ActorName).HasColumnType("varchar(32)");

                entity.Property(e => e.CeaccessKeyId)
                    .HasColumnName("CEAccessKeyID")
                    .HasColumnType("varchar(64)");

                entity.Property(e => e.ClientId).HasColumnName("ClientID");

                entity.Property(e => e.EnableInd).HasDefaultValueSql("1");

                entity.Property(e => e.NewDate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("getdate()");

                entity.Property(e => e.UpdDate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("getdate()");

                entity.HasOne(d => d.Client)
                    .WithMany(p => p.User)
                    .HasForeignKey(d => d.ClientId)
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_User_Client");
            });

            modelBuilder.Entity<UserEndpoint>(entity =>
            {
                entity.HasKey(e => new { e.UserId, e.EndpointId })
                    .HasName("PK_AuthorizedUserEndpoint");

                entity.Property(e => e.UserId).HasColumnName("UserID");

                entity.Property(e => e.EndpointId).HasColumnName("EndpointID");

                entity.Property(e => e.EnableInd).HasDefaultValueSql("1");

                entity.HasOne(d => d.Endpoint)
                    .WithMany(p => p.UserEndpoint)
                    .HasForeignKey(d => d.EndpointId)
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_UserEndpoint_Endpoint");

                entity.HasOne(d => d.User)
                    .WithMany(p => p.UserEndpoint)
                    .HasForeignKey(d => d.UserId)
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_UserEndpoint_User");
            });

            modelBuilder.Entity<UserEnvironment>(entity =>
            {
                entity.HasKey(e => new { e.UserId, e.EnvId })
                    .HasName("PK_UserEnvironment");

                entity.Property(e => e.UserId).HasColumnName("UserID");

                entity.Property(e => e.EnvId).HasColumnName("EnvID");

                entity.Property(e => e.BasicKey).HasColumnType("varchar(64)");

                entity.Property(e => e.CesecretAccessKey)
                    .HasColumnName("CESecretAccessKey")
                    .HasColumnType("varchar(64)");

                entity.Property(e => e.UpdDate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("getdate()");

                entity.HasOne(d => d.Env)
                    .WithMany(p => p.UserEnvironment)
                    .HasForeignKey(d => d.EnvId)
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_UserEnvironment_Environment");

                entity.HasOne(d => d.User)
                    .WithMany(p => p.UserEnvironment)
                    .HasForeignKey(d => d.UserId)
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_UserEnvironment_User");
            });

            modelBuilder.Entity<UserRole>(entity =>
            {
                entity.HasKey(e => new { e.UserId, e.RoleId })
                    .HasName("PK_UserRole");

                entity.Property(e => e.UserId).HasColumnName("UserID");

                entity.Property(e => e.RoleId).HasColumnName("RoleID");

                entity.Property(e => e.EnableInd).HasDefaultValueSql("1");

                entity.HasOne(d => d.Role)
                    .WithMany(p => p.UserRole)
                    .HasForeignKey(d => d.RoleId)
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_UserRole_Role");

                entity.HasOne(d => d.User)
                    .WithMany(p => p.UserRole)
                    .HasForeignKey(d => d.UserId)
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_UserRole_User");
            });
        }
    }
}