﻿using System;
using System.Collections.Generic;

namespace CE.InsightDataAccess.InsightModels
{
    public partial class ConfigArea
    {
        public ConfigArea()
        {
            ConfigAreaClient = new HashSet<ConfigAreaClient>();
            ConfigAreaEndpoint = new HashSet<ConfigAreaEndpoint>();
        }

        public byte ConfigAreaId { get; set; }
        public string Name { get; set; }

        public virtual ICollection<ConfigAreaClient> ConfigAreaClient { get; set; }
        public virtual ICollection<ConfigAreaEndpoint> ConfigAreaEndpoint { get; set; }
    }
}
