﻿using System;
using System.Collections.Generic;

namespace CE.InsightDataAccess.InsightModels
{
    public partial class ClientPortalConfig
    {
        public int ClientPortalConfigId { get; set; }
        public int ClientId { get; set; }
        public int PortalConfigId { get; set; }
        public string PortalConfigValue { get; set; }
        public bool IsEnabled { get; set; }

        public virtual Client Client { get; set; }
        public virtual PortalConfig PortalConfig { get; set; }
    }
}
