﻿using System;
using System.Collections.Generic;

namespace CE.InsightDataAccess.InsightModels
{
    public partial class Role1
    {
        public int RoleId { get; set; }
        public string RoleName { get; set; }
    }
}
