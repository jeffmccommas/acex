﻿using System;
using System.Collections.Generic;

namespace CE.InsightDataAccess.InsightModels
{
    public partial class Commodity
    {
        public Commodity()
        {
            BillBenchmark = new HashSet<BillBenchmark>();
        }

        public byte CommodityId { get; set; }
        public string Name { get; set; }

        public virtual ICollection<BillBenchmark> BillBenchmark { get; set; }
    }
}
