﻿using System;
using System.Collections.Generic;

namespace CE.InsightDataAccess.InsightModels
{
    public partial class UserRole
    {
        public int UserId { get; set; }
        public byte RoleId { get; set; }
        public bool EnableInd { get; set; }

        public virtual Role Role { get; set; }
        public virtual User User { get; set; }
    }
}
