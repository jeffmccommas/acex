﻿using System;
using System.Collections.Generic;

namespace CE.InsightDataAccess.InsightModels
{
    public partial class UserEndpoint
    {
        public int UserId { get; set; }
        public int EndpointId { get; set; }
        public bool EnableInd { get; set; }

        public virtual Endpoint Endpoint { get; set; }
        public virtual User User { get; set; }
    }
}
