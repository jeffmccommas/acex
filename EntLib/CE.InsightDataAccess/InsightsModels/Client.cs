﻿using System;
using System.Collections.Generic;

namespace CE.InsightDataAccess.InsightModels
{
    public partial class Client
    {
        public Client()
        {
            ClientPortalConfig = new HashSet<ClientPortalConfig>();
            ClientProperty = new HashSet<ClientProperty>();
            CustomerWebToken = new HashSet<CustomerWebToken>();
            PortalLogin = new HashSet<PortalLogin>();
            User = new HashSet<User>();
        }

        public int ClientId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public int? RateCompanyId { get; set; }
        public int? ReferrerId { get; set; }
        public DateTime NewDate { get; set; }
        public DateTime UpdDate { get; set; }
        public byte AuthType { get; set; }
        public bool EnableInd { get; set; }

        public virtual ICollection<ClientPortalConfig> ClientPortalConfig { get; set; }
        public virtual ICollection<ClientProperty> ClientProperty { get; set; }
        public virtual ICollection<CustomerWebToken> CustomerWebToken { get; set; }
        public virtual ICollection<PortalLogin> PortalLogin { get; set; }
        public virtual ICollection<User> User { get; set; }
    }
}
