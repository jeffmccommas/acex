﻿using System;
using System.Collections.Generic;

namespace CE.InsightDataAccess.InsightModels
{
    public partial class Endpoint
    {
        public Endpoint()
        {
            ConfigAreaEndpoint = new HashSet<ConfigAreaEndpoint>();
            UserEndpoint = new HashSet<UserEndpoint>();
        }

        public int EndpointId { get; set; }
        public string Name { get; set; }
        public string ShortName { get; set; }
        public int SortOrder { get; set; }

        public virtual ICollection<ConfigAreaEndpoint> ConfigAreaEndpoint { get; set; }
        public virtual ICollection<UserEndpoint> UserEndpoint { get; set; }
    }
}
