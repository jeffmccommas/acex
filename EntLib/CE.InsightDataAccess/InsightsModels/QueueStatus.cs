﻿using System;
using System.Collections.Generic;

namespace CE.InsightDataAccess.InsightModels
{
    public partial class QueueStatus
    {
        public QueueStatus()
        {
            RequestQueue = new HashSet<RequestQueue>();
        }

        public byte QueueStatusId { get; set; }
        public string Name { get; set; }

        public virtual ICollection<RequestQueue> RequestQueue { get; set; }
    }
}
