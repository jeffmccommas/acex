﻿using System;
using System.Collections.Generic;

namespace CE.InsightDataAccess.InsightModels
{
    public partial class Environment
    {
        public Environment()
        {
            ClientProperty = new HashSet<ClientProperty>();
            PortalClient = new HashSet<PortalClient>();
            PortalLogin = new HashSet<PortalLogin>();
            PowerBiportalClient = new HashSet<PowerBiportalClient>();
            PowerBiportalLogin = new HashSet<PowerBiportalLogin>();
            UserEnvironment = new HashSet<UserEnvironment>();
        }

        public byte EnvId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public byte? SortOrder { get; set; }

        public virtual ICollection<ClientProperty> ClientProperty { get; set; }
        public virtual ICollection<PortalClient> PortalClient { get; set; }
        public virtual ICollection<PortalLogin> PortalLogin { get; set; }
        public virtual ICollection<PowerBiportalClient> PowerBiportalClient { get; set; }
        public virtual ICollection<PowerBiportalLogin> PowerBiportalLogin { get; set; }
        public virtual ICollection<UserEnvironment> UserEnvironment { get; set; }
    }
}
