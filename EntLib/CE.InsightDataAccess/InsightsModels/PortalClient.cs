﻿using System;
using System.Collections.Generic;

namespace CE.InsightDataAccess.InsightModels
{
    public partial class PortalClient
    {
        public string ClientName { get; set; }
        public string Description { get; set; }
        public byte? EnvId { get; set; }
        public int ClientId { get; set; }

        public virtual Environment Env { get; set; }
    }
}
