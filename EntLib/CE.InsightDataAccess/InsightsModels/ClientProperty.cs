﻿using System;
using System.Collections.Generic;

namespace CE.InsightDataAccess.InsightModels
{
    public partial class ClientProperty
    {
        public int ClientId { get; set; }
        public byte EnvId { get; set; }
        public int PropertyId { get; set; }
        public string PropValue { get; set; }

        public virtual Client Client { get; set; }
        public virtual Environment Env { get; set; }
        public virtual Property Property { get; set; }
    }
}
