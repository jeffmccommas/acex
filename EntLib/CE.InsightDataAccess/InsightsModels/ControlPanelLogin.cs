﻿using System;
using System.Collections.Generic;

namespace CE.InsightDataAccess.InsightModels
{
    public partial class ControlPanelLogin
    {
        public ControlPanelLogin()
        {
            ControlPanelLoginClient = new HashSet<ControlPanelLoginClient>();
        }

        public int CpId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string UserName { get; set; }
        public string PasswordHash { get; set; }
        public string PasswordSalt { get; set; }
        public bool SuperUserInd { get; set; }
        public bool EnableInd { get; set; }
        public DateTime NewDate { get; set; }
        public DateTime UpdDate { get; set; }

        public virtual ICollection<ControlPanelLoginClient> ControlPanelLoginClient { get; set; }
    }
}
