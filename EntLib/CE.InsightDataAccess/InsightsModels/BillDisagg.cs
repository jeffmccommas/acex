﻿using System;
using System.Collections.Generic;

namespace CE.InsightDataAccess.InsightModels
{
    public partial class BillDisagg
    {
        public int ClientId { get; set; }
        public string CustomerId { get; set; }
        public string AccountId { get; set; }
        public string PremiseId { get; set; }
        public DateTime BillDate { get; set; }
        public string EndUseKey { get; set; }
        public string ApplianceKey { get; set; }
        public string CommodityKey { get; set; }
        public byte PeriodId { get; set; }
        public decimal Usage { get; set; }
        public decimal Cost { get; set; }
        public string UomKey { get; set; }
        public string CurrencyKey { get; set; }
        public int? Confidence { get; set; }
        public decimal? ReconciliationRatio { get; set; }
        public byte StatusId { get; set; }
        public DateTime NewDate { get; set; }
    }
}
