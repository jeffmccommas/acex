﻿using System;
using System.Collections.Generic;

namespace CE.InsightDataAccess.InsightModels
{
    public partial class ProfileCustomerAttribute
    {
        public int ClientId { get; set; }
        public string CustomerId { get; set; }
        public string AttributeKey { get; set; }
        public string AttributeValue { get; set; }
        public string Source { get; set; }
        public string Criteria { get; set; }
        public DateTime EffectiveDate { get; set; }
    }
}
