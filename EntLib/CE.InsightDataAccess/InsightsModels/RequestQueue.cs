﻿using System;
using System.Collections.Generic;

namespace CE.InsightDataAccess.InsightModels
{
    public partial class RequestQueue
    {
        public int RequestQueueId { get; set; }
        public Guid RequestGuid { get; set; }
        public int UserId { get; set; }
        public byte QueueStatusId { get; set; }
        public string RequestData { get; set; }
        public string ResponseData { get; set; }
        public int? EndpointId { get; set; }
        public string Verb { get; set; }
        public DateTime NewDate { get; set; }
        public DateTime UpdDate { get; set; }

        public virtual QueueStatus QueueStatus { get; set; }
    }
}
