﻿using System;
using System.Collections.Generic;

namespace CE.InsightDataAccess.InsightModels
{
    public partial class PortalConfig
    {
        public PortalConfig()
        {
            ClientPortalConfig = new HashSet<ClientPortalConfig>();
        }

        public int PortalConfigId { get; set; }
        public string PortalConfigName { get; set; }
        public bool IsEnabled { get; set; }
        public int PortalConfigTypeId { get; set; }

        public virtual ICollection<ClientPortalConfig> ClientPortalConfig { get; set; }
        public virtual PortalConfigType PortalConfigType { get; set; }
    }
}
