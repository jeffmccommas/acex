﻿using System;
using System.Collections.Generic;

namespace CE.InsightDataAccess.InsightModels
{
    public partial class EndUse
    {
        public int EndUseId { get; set; }
        public string Name { get; set; }
    }
}
