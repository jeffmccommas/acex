﻿using System;
using System.Collections.Generic;

namespace CE.InsightDataAccess.InsightModels
{
    public partial class OauthClients
    {
        public long ClientId { get; set; }
        public string ClientName { get; set; }
        public string ClientDescription { get; set; }
        public string ClientSecret { get; set; }
        public string RedirectUri { get; set; }
        public string GrantTypes { get; set; }
        public string Scope { get; set; }
        public string UserId { get; set; }
        public bool Active { get; set; }
        public DateTime UtcDateTimeCreated { get; set; }
    }
}
