﻿using System;
using System.Collections.Generic;

namespace CE.InsightDataAccess.InsightModels
{
    public partial class GroupRole
    {
        public int GroupId { get; set; }
        public int RoleId { get; set; }
    }
}
