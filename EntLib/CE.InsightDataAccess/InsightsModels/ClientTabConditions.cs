﻿using System;
using System.Collections.Generic;

namespace CE.InsightDataAccess.InsightModels
{
    public partial class ClientTabConditions
    {
        public int ClientId { get; set; }
        public string TabId { get; set; }
        public int GroupId { get; set; }
        public int RoleId { get; set; }
    }
}
