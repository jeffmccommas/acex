﻿using System;
using System.Collections.Generic;

namespace CE.InsightDataAccess.InsightModels
{
    public partial class User
    {
        public User()
        {
            UserEndpoint = new HashSet<UserEndpoint>();
            UserEnvironment = new HashSet<UserEnvironment>();
            UserRole = new HashSet<UserRole>();
        }

        public int UserId { get; set; }
        public int ClientId { get; set; }
        public string ActorName { get; set; }
        public string CeaccessKeyId { get; set; }
        public DateTime NewDate { get; set; }
        public DateTime UpdDate { get; set; }
        public bool EnableInd { get; set; }

        public virtual ICollection<UserEndpoint> UserEndpoint { get; set; }
        public virtual ICollection<UserEnvironment> UserEnvironment { get; set; }
        public virtual ICollection<UserRole> UserRole { get; set; }
        public virtual Client Client { get; set; }
    }
}
