﻿using System;
using System.Collections.Generic;

namespace CE.InsightDataAccess.InsightModels
{
    public partial class ActionCustomerPlan
    {
        public int ClientId { get; set; }
        public string CustomerId { get; set; }
        public string ActionKey { get; set; }
        public string ActionStatus { get; set; }
        public DateTime ActionStatusDate { get; set; }
        public DateTime UpdateDate { get; set; }
        public DateTime CreateDate { get; set; }
    }
}
