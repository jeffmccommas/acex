﻿using System;
using System.Collections.Generic;

namespace CE.InsightDataAccess.InsightModels
{
    public partial class PortalConfigType
    {
        public PortalConfigType()
        {
            PortalConfig = new HashSet<PortalConfig>();
        }

        public int PortalConfigTypeId { get; set; }
        public string PortalConfigTypeName { get; set; }
        public bool IsEnabled { get; set; }

        public virtual ICollection<PortalConfig> PortalConfig { get; set; }
    }
}
