﻿using System;
using System.Collections.Generic;

namespace CE.InsightDataAccess.InsightModels
{
    public partial class Currency
    {
        public Currency()
        {
            BillBenchmark = new HashSet<BillBenchmark>();
        }

        public byte CurrencyId { get; set; }
        public string Name { get; set; }

        public virtual ICollection<BillBenchmark> BillBenchmark { get; set; }
    }
}
