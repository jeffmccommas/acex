﻿using System;
using System.Collections.Generic;

namespace CE.InsightDataAccess.InsightModels
{
    public partial class OauthScopes
    {
        public string Scope { get; set; }
        public bool? IsDefault { get; set; }
    }
}
