﻿using System;
using System.Collections.Generic;

namespace CE.InsightDataAccess.InsightModels
{
    public partial class ConfigAreaClient
    {
        public byte ConfigAreaId { get; set; }
        public int ClientId { get; set; }
        public string ConfigData { get; set; }

        public virtual ConfigArea ConfigArea { get; set; }
    }
}
