﻿using System;
using System.Collections.Generic;

namespace CE.InsightDataAccess.InsightModels
{
    public partial class AuthType
    {
        public byte AuthTypeId { get; set; }
        public string AuthName { get; set; }
        public string AuthDescription { get; set; }
    }
}
