﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace CE.InsightDataAccess.InsightsMetaDataModels
{
    public partial class InsightsMetaDataContext : DbContext
    {
        public virtual DbSet<ClientConfiguration> ClientConfiguration { get; set; }

        //protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        //{
        //    //#warning To protect potentially sensitive information in your connection string, you should move it out of source code. See http://go.microsoft.com/fwlink/?LinkId=723263 for guidance on storing connection strings.
        //    //optionsBuilder.UseSqlServer(@"Server=tcp:acedsql1c0.database.windows.net,1433;Database=InsightsMetaData;User ID=Aclweb@acedsql1c0;Password=Acl@r@393;Trusted_Connection=False;Encrypt=True");
        //}

        public InsightsMetaDataContext(DbContextOptions<InsightsMetaDataContext> options) : base(options)
        {
        }

        public InsightsMetaDataContext() : base()
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<ClientConfiguration>(entity =>
            {
                entity.ToTable("ClientConfiguration", "cm");

                entity.HasIndex(e => new { e.ClientId, e.CategoryKey, e.EnvironmentKey })
                    .HasName("IX_ClientConfiguration_ClientID_CategoryKey_EnvironmentKey");

                entity.HasIndex(e => new { e.ClientId, e.EnvironmentKey, e.ConfigurationKey })
                    .HasName("IX_ClientConfiguration")
                    .IsUnique();

                entity.Property(e => e.ClientConfigurationId).HasColumnName("ClientConfigurationID");

                entity.Property(e => e.CategoryKey)
                    .IsRequired()
                    .HasColumnType("varchar(256)");

                entity.Property(e => e.ClientId).HasColumnName("ClientID");

                entity.Property(e => e.ConfigurationKey)
                    .IsRequired()
                    .HasColumnType("varchar(256)");

                entity.Property(e => e.DateUpdated)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("getdate()");

                entity.Property(e => e.Description)
                    .IsRequired()
                    .HasColumnType("varchar(4000)");

                entity.Property(e => e.EnvironmentKey)
                    .IsRequired()
                    .HasColumnType("varchar(256)");

                entity.Property(e => e.Value)
                    .IsRequired()
                    .HasColumnType("varchar(256)");
            });

            modelBuilder.HasSequence<int>("CountBy1");
        }
    }
}