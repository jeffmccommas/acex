﻿using System;
using System.Collections.Generic;

namespace CE.InsightDataAccess.InsightsMetaDataModels
{
    public partial class ClientConfiguration
    {
        public int ClientConfigurationId { get; set; }
        public int ClientId { get; set; }
        public string EnvironmentKey { get; set; }
        public string ConfigurationKey { get; set; }
        public string CategoryKey { get; set; }
        public string Description { get; set; }
        public string Value { get; set; }
        public DateTime DateUpdated { get; set; }
    }
}
