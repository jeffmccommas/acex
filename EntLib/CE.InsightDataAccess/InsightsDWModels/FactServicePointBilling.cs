﻿using System;
using System.Collections.Generic;

namespace CE.InsightDataAccess.InsightsDWModels
{
    public partial class FactServicePointBilling
    {
        public int ServiceContractKey { get; set; }
        public int BillPeriodStartDateKey { get; set; }
        public int BillPeriodEndDateKey { get; set; }
        public int Uomkey { get; set; }
        public int BillPeriodTypeKey { get; set; }
        public int ClientId { get; set; }
        public string PremiseId { get; set; }
        public string AccountId { get; set; }
        public int CommodityId { get; set; }
        public int BillPeriodTypeId { get; set; }
        public int Uomid { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public decimal TotalUsage { get; set; }
        public decimal CostOfUsage { get; set; }
        public decimal? OtherCost { get; set; }
        public int? RateClassKey1 { get; set; }
        public int? RateClassKey2 { get; set; }
        public DateTime? DueDate { get; set; }
        public DateTime? NextReadDate { get; set; }
        public DateTime? ReadDate { get; set; }
        public DateTime? CreateDate { get; set; }
        public DateTime? UpdateDate { get; set; }
        public string BillCycleScheduleId { get; set; }
        public int BillDays { get; set; }
        public string ReadQuality { get; set; }
        public int EtlLogId { get; set; }
        public string TrackingId { get; set; }
        public DateTime TrackingDate { get; set; }
        public int SourceKey { get; set; }
        public int SourceId { get; set; }
        public bool IsFault { get; set; }
        public Guid? BillingCostDetailsKey { get; set; }
        public Guid? ServiceCostDetailsKey { get; set; }
        public string UtilityBillRecordId { get; set; }
        public string CanceledUtilityBillRecordId { get; set; }
    }
}
