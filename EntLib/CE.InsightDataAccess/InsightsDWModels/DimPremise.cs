﻿using System;
using System.Collections.Generic;

namespace CE.InsightDataAccess.InsightsDWModels
{
    public partial class DimPremise
    {
        public int PremiseKey { get; set; }
        public int PostalCodeKey { get; set; }
        public int CityKey { get; set; }
        public int SourceKey { get; set; }
        public string AccountId { get; set; }
        public string PremiseId { get; set; }
        public string Street1 { get; set; }
        public string Street2 { get; set; }
        public string City { get; set; }
        public string StateProvince { get; set; }
        public string Country { get; set; }
        public string PostalCode { get; set; }
        public int? GasService { get; set; }
        public int? ElectricService { get; set; }
        public int? WaterService { get; set; }
        public DateTime? CreateDate { get; set; }
        public DateTime? UpdateDate { get; set; }
        public int ClientId { get; set; }
        public int? SourceId { get; set; }
        public int? EtlLogId { get; set; }
        public byte? IsInferred { get; set; }
        public string TrackingId { get; set; }
        public DateTime? TrackingDate { get; set; }
    }
}
