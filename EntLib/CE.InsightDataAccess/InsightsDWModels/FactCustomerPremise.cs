﻿using System;
using System.Collections.Generic;

namespace CE.InsightDataAccess.InsightsDWModels
{
    public partial class FactCustomerPremise
    {
        public int CustomerKey { get; set; }
        public int PremiseKey { get; set; }
        public int? EtlLogId { get; set; }
        public DateTime? DateCreated { get; set; }
        public int? ClientId { get; set; }
    }
}
