﻿using System;
using System.Collections.Generic;

namespace CE.InsightDataAccess.InsightsDWModels
{
    public partial class DimMeter
    {
        public int MeterKey { get; set; }
        public int ServicePointKey { get; set; }
        public int ClientId { get; set; }
        public int CommodityKey { get; set; }
        public string MeterId { get; set; }
        public int? MeterTypeKey { get; set; }
        public string ReplacedMeterId { get; set; }
        public string IsDeleted { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime? UpdateDate { get; set; }
        public int SourceId { get; set; }
        public string TrackingId { get; set; }
        public DateTime TrackingDate { get; set; }
        public bool? IsInferred { get; set; }

        public virtual DimServicePoint ServicePointKeyNavigation { get; set; }
    }
}
