﻿using System;
using System.Collections.Generic;

namespace CE.InsightDataAccess.InsightsDWModels
{
    public partial class DimServicePoint
    {
        public DimServicePoint()
        {
            DimMeter = new HashSet<DimMeter>();
            FactServicePoint = new HashSet<FactServicePoint>();
        }

        public int ServicePointKey { get; set; }
        public int PremiseKey { get; set; }
        public string ServicePointId { get; set; }
        public int? ClientId { get; set; }
        public string PremiseId { get; set; }
        public int? SourceId { get; set; }
        public string TrackingId { get; set; }
        public DateTime? TrackingDate { get; set; }
        public bool? IsInferred { get; set; }

        public virtual ICollection<DimMeter> DimMeter { get; set; }
        public virtual ICollection<FactServicePoint> FactServicePoint { get; set; }
    }
}
