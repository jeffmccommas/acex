﻿using System;
using System.Collections.Generic;

namespace CE.InsightDataAccess.InsightsDWModels
{
    public partial class EspmMapDetails
    {
        public int? MapDetailId { get; set; }
        public int? MapId { get; set; }
        public string MapAccountId { get; set; }

        public string CustomerId { get; set; }
        public string MapPremiseId { get; set; }
        public bool Enabled { get; set; } = false;
        public int UpdatedBy { get; set; }
        public DateTime UpdatedDate { get; set; }
    }
}
