﻿using System;
using System.Collections.Generic;

namespace CE.InsightDataAccess.InsightsDWModels
{
    public partial class MeterBillingData
    {
        public int UsageId { get; set; }
        public string MeterId { get; set; }
        public string Usage { get; set; }
        public string Cost { get; set; }
        public DateTime? Startdate { get; set; }
        public DateTime? Enddate { get; set; }
        public DateTime UpdatedDate { get; set; }
        public int UpdatedBy { get; set; }
    }
}
