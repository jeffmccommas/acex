﻿using System;

namespace CE.InsightDataAccess.InsightsDWModels
{
    public partial class EspmMetersWhiteList
    {
        public int? ListId { get; set; }
        public string MeterId { get; set; }
        public string MeterType { get; set; }
        public string Reason { get; set; }
        public int UpdatedBy { get; set; }
        public DateTime UpdatedDate { get; set; }
    }
}
