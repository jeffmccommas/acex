﻿using System;
using System.Collections.Generic;

namespace CE.InsightDataAccess.InsightsDWModels
{
    public partial class Espmmap
    {
        public int? MapId { get; set; }
        public string AccountId { get; set; }
        public string PremiseId { get; set; }
        public string MeterId { get; set; }
        public string MeterType { get; set; }
        public int UpdatedBy { get; set; }
        public DateTime UpdatedDate { get; set; }
        public virtual ICollection<EspmMapDetails> EspmMapDetails{ get; set; }
    }
}
