﻿using System;
using System.Collections.Generic;

namespace CE.InsightDataAccess.InsightsDWModels
{
    public partial class DimCustomer
    {
        public int CustomerKey { get; set; }
        public int ClientKey { get; set; }
        public int ClientId { get; set; }
        public int PostalCodeKey { get; set; }
        public int CustomerAuthenticationTypeKey { get; set; }
        public int? SourceKey { get; set; }
        public int CityKey { get; set; }
        public string CustomerId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Street1 { get; set; }
        public string Street2 { get; set; }
        public string City { get; set; }
        public string StateProvince { get; set; }
        public string Country { get; set; }
        public string PostalCode { get; set; }
        public string PhoneNumber { get; set; }
        public string MobilePhoneNumber { get; set; }
        public string EmailAddress { get; set; }
        public string AlternateEmailAddress { get; set; }
        public DateTime? CreateDate { get; set; }
        public DateTime? UpdateDate { get; set; }
        public int? SourceId { get; set; }
        public int? AuthenticationTypeId { get; set; }
        public string Language { get; set; }
        public int? EtlLogId { get; set; }
        public byte? IsInferred { get; set; }
        public string TrackingId { get; set; }
        public DateTime? TrackingDate { get; set; }
    }
}
