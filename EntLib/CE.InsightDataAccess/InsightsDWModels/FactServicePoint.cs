﻿using System;
using System.Collections.Generic;

namespace CE.InsightDataAccess.InsightsDWModels
{
    public partial class FactServicePoint
    {
        public int FactServicePointKey { get; set; }
        public int ServiceContractKey { get; set; }
        public int ServicePointKey { get; set; }
        public int? ClientId { get; set; }

        public virtual DimServiceContract ServiceContractKeyNavigation { get; set; }
        public virtual DimServicePoint ServicePointKeyNavigation { get; set; }
    }
}
