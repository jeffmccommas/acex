﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CE.InsightDataAccess.InsightsDWModels
{
    public partial class FactServicePointBilling_BillCostDetails
    {
        public Guid? BillingCostDetailsKey { get; set; }
        public string BillingCostDetailName { get; set; }
        public string BillingCostDetailValue { get; set; }

    }
}
