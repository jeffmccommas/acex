﻿using System;
using System.Collections.Generic;

namespace CE.InsightDataAccess.InsightsDWModels
{
    public partial class DimServiceContract
    {
        public DimServiceContract()
        {
            FactServicePoint = new HashSet<FactServicePoint>();
        }

        public int ServiceContractKey { get; set; }
        public int PremiseKey { get; set; }
        public int CommodityKey { get; set; }
        public int ClientId { get; set; }
        public string ServiceContractId { get; set; }
        public int? ServiceContractTypeKey { get; set; }
        public string ServiceContractDescription { get; set; }
        public DateTime? ActiveDate { get; set; }
        public DateTime? InActiveDate { get; set; }
        public byte? IsInferred { get; set; }
        public string TrackingId { get; set; }
        public DateTime? TrackingDate { get; set; }
        public int? BudgetBillingIndicator { get; set; }

        public virtual ICollection<FactServicePoint> FactServicePoint { get; set; }
    }
}
