﻿using System;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure.Internal;
using Microsoft.EntityFrameworkCore.Metadata;

namespace CE.InsightDataAccess.InsightsDWModels
{
    public partial class InsightsDWContext : DbContext
    {
        public virtual DbSet<DimCustomer> DimCustomer { get; set; }
        public virtual DbSet<DimMeter> DimMeter { get; set; }
        public virtual DbSet<DimPremise> DimPremise { get; set; }
        public virtual DbSet<DimServiceContract> DimServiceContract { get; set; }
        public virtual DbSet<DimServicePoint> DimServicePoint { get; set; }
        public virtual DbSet<FactCustomerPremise> FactCustomerPremise { get; set; }
        public virtual DbSet<FactServicePoint> FactServicePoint { get; set; }
        public virtual DbSet<Espmmap> Espmmap { get; set; }
        public virtual DbSet<EspmMapDetails> EspmMapDetails { get; set; }
        public virtual DbSet<MeterBillingData> MeterBillingData { get; set; }

        public virtual DbSet<EspmMetersWhiteList> EspmMetersWhiteList { get; set; }
        public virtual DbSet<FactServicePointBilling> FactServicePointBilling { get; set; }

        public virtual DbSet<FactServicePointBilling_BillCostDetails> FactServicePointBilling_BillCostDetails { get; set; }

        private readonly string _connectionString;

        public string shardDbName { get; set; }
        public InsightsDWContext(DbContextOptions<InsightsDWContext> options) : base(options)
        {
            _connectionString = ((SqlServerOptionsExtension)options.Extensions.Last()).ConnectionString;
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            base.OnConfiguring(optionsBuilder);
            var connectionStringconfig = String.Format(_connectionString, shardDbName);
            optionsBuilder.UseSqlServer(connectionStringconfig);
        }

        public InsightsDWContext() : base()
        {
        }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<DimCustomer>(entity =>
            {
                entity.HasKey(e => new { e.ClientId, e.CustomerId })
                    .HasName("PK_DimCustomer");

                entity.HasIndex(e => e.CustomerId)
                    .HasName("IX_DimCustomer_CustomerId");

                entity.HasIndex(e => e.CustomerKey)
                    .HasName("IX_DimCustomer_CustomerKey")
                    .IsUnique();

                entity.HasIndex(e => new { e.CustomerKey, e.ClientKey })
                    .HasName("IX_DimCustomer_ClientKey");

                entity.Property(e => e.CustomerId).HasColumnType("varchar(50)");

                entity.Property(e => e.AlternateEmailAddress).HasColumnType("varchar(255)");

                entity.Property(e => e.City).HasColumnType("varchar(50)");

                entity.Property(e => e.Country).HasColumnType("varchar(3)");

                entity.Property(e => e.CreateDate).HasColumnType("datetime");

                entity.Property(e => e.CustomerKey).ValueGeneratedOnAdd();

                entity.Property(e => e.EmailAddress).HasColumnType("varchar(255)");

                entity.Property(e => e.EtlLogId).HasColumnName("ETL_LogId");

                entity.Property(e => e.FirstName).HasColumnType("varchar(50)");

                entity.Property(e => e.Language).HasColumnType("varchar(12)");

                entity.Property(e => e.LastName).HasColumnType("varchar(50)");

                entity.Property(e => e.MobilePhoneNumber).HasColumnType("varchar(15)");

                entity.Property(e => e.PhoneNumber).HasColumnType("varchar(15)");

                entity.Property(e => e.PostalCode).HasColumnType("varchar(15)");

                entity.Property(e => e.StateProvince).HasColumnType("varchar(3)");

                entity.Property(e => e.Street1).HasColumnType("varchar(200)");

                entity.Property(e => e.Street2).HasColumnType("varchar(200)");

                entity.Property(e => e.TrackingDate).HasColumnType("datetime");

                entity.Property(e => e.TrackingId).HasColumnType("varchar(50)");

                entity.Property(e => e.UpdateDate).HasColumnType("datetime");
            });

            modelBuilder.Entity<DimMeter>(entity =>
            {
                entity.HasKey(e => e.MeterKey)
                    .HasName("PK_DimMeter_1");

                entity.HasIndex(e => e.ServicePointKey)
                    .HasName("IX_DimMeter_ServicePointKey");

                entity.HasIndex(e => new { e.ServicePointKey, e.CommodityKey, e.MeterId })
                    .HasName("IX_DimMeter_Commodity_Meter");

                entity.HasIndex(e => new { e.CommodityKey, e.MeterId, e.MeterKey, e.ServicePointKey, e.ClientId })
                    .HasName("IX_DimMeter_Client");

                entity.Property(e => e.CreateDate).HasColumnType("datetime");

                entity.Property(e => e.IsDeleted).HasColumnType("varchar(2)");

                entity.Property(e => e.MeterId)
                    .IsRequired()
                    .HasColumnType("varchar(64)");

                entity.Property(e => e.ReplacedMeterId).HasColumnType("varchar(64)");

                entity.Property(e => e.TrackingDate).HasColumnType("datetime");

                entity.Property(e => e.TrackingId).HasColumnType("varchar(50)");

                entity.Property(e => e.UpdateDate).HasColumnType("datetime");

                entity.HasOne(d => d.ServicePointKeyNavigation)
                    .WithMany(p => p.DimMeter)
                    .HasForeignKey(d => d.ServicePointKey)
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_DimMeter_DimServicePoint");
            });

            modelBuilder.Entity<DimPremise>(entity =>
            {
                entity.HasKey(e => e.PremiseKey)
                    .HasName("PK_DimPremise");

                entity.HasIndex(e => new { e.PremiseId, e.AccountId })
                    .HasName("IX_DimPremise");

                entity.Property(e => e.AccountId)
                    .IsRequired()
                    .HasColumnType("varchar(50)");

                entity.Property(e => e.City).HasColumnType("varchar(50)");

                entity.Property(e => e.Country).HasColumnType("varchar(3)");

                entity.Property(e => e.CreateDate).HasColumnType("datetime");

                entity.Property(e => e.EtlLogId).HasColumnName("ETL_LogId");

                entity.Property(e => e.PostalCode).HasColumnType("varchar(15)");

                entity.Property(e => e.PremiseId)
                    .IsRequired()
                    .HasColumnType("varchar(50)");

                entity.Property(e => e.StateProvince).HasColumnType("varchar(3)");

                entity.Property(e => e.Street1).HasColumnType("varchar(200)");

                entity.Property(e => e.Street2).HasColumnType("varchar(200)");

                entity.Property(e => e.TrackingDate).HasColumnType("datetime");

                entity.Property(e => e.TrackingId).HasColumnType("varchar(50)");

                entity.Property(e => e.UpdateDate).HasColumnType("datetime");
            });

            modelBuilder.Entity<DimServiceContract>(entity =>
            {
                entity.HasKey(e => e.ServiceContractKey)
                    .HasName("PK_FactServiceContract");

                entity.HasIndex(e => new { e.CommodityKey, e.PremiseKey, e.ServiceContractId, e.ServiceContractKey, e.ClientId })
                    .HasName("IX_DimServiceContract_ClientId");

                entity.HasIndex(e => new { e.TrackingDate, e.PremiseKey, e.ClientId, e.CommodityKey, e.ServiceContractId })
                    .HasName("IX_DimServiceContract_PrmkeyClntIDCommKeySvcContrID");

                entity.HasIndex(e => new { e.CommodityKey, e.PremiseKey, e.ServiceContractId, e.ServiceContractKey, e.TrackingDate, e.ClientId })
                    .HasName("IX_DimServiceContract_Client_Ext");

                entity.Property(e => e.ActiveDate).HasColumnType("datetime");

                entity.Property(e => e.InActiveDate).HasColumnType("datetime");

                entity.Property(e => e.ServiceContractDescription).HasColumnType("varchar(100)");

                entity.Property(e => e.ServiceContractId)
                    .IsRequired()
                    .HasColumnType("varchar(50)");

                entity.Property(e => e.TrackingDate).HasColumnType("datetime");

                entity.Property(e => e.TrackingId).HasColumnType("varchar(50)");
            });

            modelBuilder.Entity<DimServicePoint>(entity =>
            {
                entity.HasKey(e => e.ServicePointKey)
                    .HasName("PK_DimServicePoint");

                entity.HasIndex(e => new { e.PremiseKey, e.ServicePointId })
                    .HasName("IX_DimServicePoint2");

                entity.HasIndex(e => new { e.ServicePointId, e.PremiseKey })
                    .HasName("IX_DimServicePoint");

                entity.HasIndex(e => new { e.PremiseKey, e.ServicePointId, e.ServicePointKey, e.ClientId })
                    .HasName("IX_DimServicePoint_Client");

                entity.HasIndex(e => new { e.ServicePointId, e.ServicePointKey, e.PremiseKey, e.ClientId })
                    .HasName("IX_DimServicePoint_Premise_Client");

                entity.Property(e => e.PremiseId).HasColumnType("varchar(50)");

                entity.Property(e => e.ServicePointId)
                    .IsRequired()
                    .HasColumnType("varchar(50)");

                entity.Property(e => e.TrackingDate).HasColumnType("datetime");

                entity.Property(e => e.TrackingId).HasColumnType("varchar(50)");
            });

            modelBuilder.Entity<FactCustomerPremise>(entity =>
            {
                entity.HasKey(e => new { e.CustomerKey, e.PremiseKey })
                    .HasName("PK_FactCustomerPremise");

                entity.HasIndex(e => new { e.CustomerKey, e.DateCreated, e.PremiseKey })
                    .HasName("NonClusteredIndex-20140916-164849");

                entity.HasIndex(e => new { e.DateCreated, e.PremiseKey, e.CustomerKey })
                    .HasName("NonClusteredIndex-20141030-091713");

                entity.Property(e => e.DateCreated).HasColumnType("datetime");

                entity.Property(e => e.EtlLogId).HasColumnName("ETL_LogId");
            });

            modelBuilder.Entity<FactServicePoint>(entity =>
            {
                entity.HasKey(e => e.FactServicePointKey)
                    .HasName("PK_FactServicePoint");

                entity.HasIndex(e => new { e.ServiceContractKey, e.ServicePointKey })
                    .HasName("IX_FactServicePoint_ServicePointKey");

                entity.HasIndex(e => new { e.FactServicePointKey, e.ServiceContractKey, e.ServicePointKey })
                    .HasName("IX_FactServicePoint_ServiceContract_ServicePoint");

                entity.HasOne(d => d.ServiceContractKeyNavigation)
                    .WithMany(p => p.FactServicePoint)
                    .HasForeignKey(d => d.ServiceContractKey)
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_FactServicePoint_FactServiceContract");

                entity.HasOne(d => d.ServicePointKeyNavigation)
                    .WithMany(p => p.FactServicePoint)
                    .HasForeignKey(d => d.ServicePointKey)
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_FactServicePoint_DimServicePoint");
            });

            modelBuilder.Entity<EspmMapDetails>(entity =>
            {
                entity.HasKey(e => e.MapDetailId)
                    .HasName("PK_EspmMapDetails");

                entity.ToTable("EspmMapDetails", "portal");

                entity.Property(e => e.MapAccountId)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.CustomerId).HasMaxLength(50);

                entity.Property(e => e.MapPremiseId)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.UpdatedDate).HasColumnType("datetime");
            });

            modelBuilder.Entity<Espmmap>(entity =>
            {
                entity.HasKey(e => e.MapId)
                    .HasName("PK_ESPMMap");

                entity.ToTable("ESPMMap", "portal");

                entity.Property(e => e.AccountId)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.MeterId).HasMaxLength(200);

                entity.Property(e => e.MeterType).HasMaxLength(20);

                entity.Property(e => e.PremiseId).HasMaxLength(50);

                entity.Property(e => e.UpdatedDate).HasColumnType("date");
            });

            modelBuilder.Entity<MeterBillingData>(entity =>
            {
                entity.HasKey(e => e.UsageId)
                    .HasName("PK_portal.MeterConsumptionData");

                entity.ToTable("MeterBillingData", "portal");

                entity.Property(e => e.Cost).HasColumnType("varchar(50)");

                entity.Property(e => e.Enddate).HasColumnType("date");

                entity.Property(e => e.MeterId)
                    .IsRequired()
                    .HasColumnType("varchar(50)");

                entity.Property(e => e.Startdate).HasColumnType("date");

                entity.Property(e => e.UpdatedDate).HasColumnType("date");

                entity.Property(e => e.Usage).HasColumnType("varchar(50)");
            });

            modelBuilder.Entity<EspmMetersWhiteList>(entity =>
            {
                entity.HasKey(e => e.ListId)
                    .HasName("PK_ESPMMetersWhiteList");

                entity.ToTable("ESPMMetersWhiteList", "portal");

                entity.Property(e => e.MeterId)
                    .IsRequired()
                    .HasColumnType("varchar(200)");

                entity.Property(e => e.MeterType).HasColumnType("varchar(20)");
                entity.Property(e => e.Reason).HasColumnType("varchar(255)");

                entity.Property(e => e.UpdatedDate).HasColumnType("date");

                
            });

            modelBuilder.Entity<FactServicePointBilling>(entity =>
            {
                entity.HasKey(e => new { e.ServiceContractKey, e.BillPeriodStartDateKey, e.BillPeriodEndDateKey, e.CommodityId, e.Uomid })
                    .HasName("PK_FactServicePointBilling");

                entity.HasIndex(e => e.UtilityBillRecordId)
                    .HasName("nci_wi_FactServicePointBilling_5076DB02E8535282EEA0E913A85F127F");

                entity.HasIndex(e => new { e.BillDays, e.EndDate, e.ServiceContractKey, e.ClientId })
                    .HasName("IX_FactServicePointBilling_ClientId");

                entity.HasIndex(e => new { e.ClientId, e.PremiseId, e.AccountId, e.SourceId, e.TrackingId, e.TrackingDate, e.UpdateDate })
                    .HasName("IX_FactServicePointBilling_UpdateDate");

                entity.HasIndex(e => new { e.ServiceContractKey, e.CommodityId, e.Uomid, e.StartDate, e.TotalUsage, e.CostOfUsage, e.EndDate })
                    .HasName("IX_FactBilling_Endate");

                entity.HasIndex(e => new { e.ClientId, e.PremiseId, e.AccountId, e.StartDate, e.EndDate, e.CommodityId, e.BillPeriodTypeId, e.Uomid })
                    .HasName("IX_FactServicePointBilling");

                entity.Property(e => e.Uomid).HasColumnName("UOMId");

                entity.Property(e => e.AccountId)
                    .IsRequired()
                    .HasColumnType("varchar(50)");

                entity.Property(e => e.BillCycleScheduleId).HasColumnType("varchar(50)");

                entity.Property(e => e.BillPeriodTypeKey).HasDefaultValueSql("1");

                entity.Property(e => e.CanceledUtilityBillRecordId).HasColumnType("varchar(64)");

                entity.Property(e => e.CostOfUsage).HasColumnType("decimal");

                entity.Property(e => e.CreateDate).HasColumnType("datetime");

                entity.Property(e => e.DueDate).HasColumnType("date");

                entity.Property(e => e.EndDate).HasColumnType("datetime");

                entity.Property(e => e.EtlLogId).HasColumnName("ETL_LogId");

                entity.Property(e => e.IsFault).HasDefaultValueSql("0");

                entity.Property(e => e.NextReadDate).HasColumnType("date");

                entity.Property(e => e.OtherCost).HasColumnType("decimal");

                entity.Property(e => e.PremiseId)
                    .IsRequired()
                    .HasColumnType("varchar(50)");

                entity.Property(e => e.ReadDate).HasColumnType("datetime");

                entity.Property(e => e.ReadQuality).HasColumnType("varchar(50)");

                entity.Property(e => e.StartDate).HasColumnType("datetime");

                entity.Property(e => e.TotalUsage).HasColumnType("decimal");

                entity.Property(e => e.TrackingDate).HasColumnType("datetime");

                entity.Property(e => e.TrackingId)
                    .IsRequired()
                    .HasColumnType("varchar(50)");

                entity.Property(e => e.Uomkey).HasColumnName("UOMKey");

                entity.Property(e => e.UpdateDate).HasColumnType("datetime");

                entity.Property(e => e.UtilityBillRecordId).HasColumnType("varchar(64)");
            });


            modelBuilder.Entity<FactServicePointBilling_BillCostDetails>(entity =>
            {
                entity.HasKey(e => e.BillingCostDetailsKey)
                    .HasName("PK_BillingCostDetailsKey_1");

                entity.Property(e => e.BillingCostDetailName).HasColumnName("BillingCostDetailName");
                entity.Property(e => e.BillingCostDetailValue).HasColumnName("BillingCostDetailValue");
                entity.Property(e => e.BillingCostDetailsKey).HasColumnName("BillingCostDetailsKey");

            });
        }
    }
}