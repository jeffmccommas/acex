﻿using System;
using System.Collections.Generic;

namespace CE.InsightDataAccess.Models
{
    public partial class Billing
    {
        public int ClientId { get; set; }
        public string CustomerId { get; set; }
        public string AccountId { get; set; }
        public string PremiseId { get; set; }
        public string ServiceContractId { get; set; }
        public string ServicePointId { get; set; }
        public DateTime BillStartDate { get; set; }
        public DateTime BillEndDate { get; set; }
        public int BillDays { get; set; }
        public decimal TotalUsage { get; set; }
        public decimal CostOfUsage { get; set; }
        public int CommodityId { get; set; }
        public int BillPeriodTypeId { get; set; }
        public int Uomid { get; set; }
        public DateTime? AmistartDate { get; set; }
        public DateTime? AmiendDate { get; set; }
        public string MeterId { get; set; }
        public string MeterType { get; set; }
        public string ReplacedMeterId { get; set; }
        public string RateClass { get; set; }
        public DateTime? DueDate { get; set; }
        public DateTime? ReadDate { get; set; }
        public string ReadQuality { get; set; }
        public string BillCycleScheduleId { get; set; }
        public int SourceId { get; set; }
    }
}
