﻿using System;
using System.Collections.Generic;

namespace CE.InsightDataAccess.Models
{
    public partial class UserPortal
    {
        public int UserPortalId { get; set; }
        public int UserId { get; set; }
        public int PortalId { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public bool IsEnabled { get; set; }
        public string ModifiedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public bool IsDefault { get; set; }

        public virtual Portals Portal { get; set; }
    }
}
