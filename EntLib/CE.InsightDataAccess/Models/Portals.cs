﻿using System;
using System.Collections.Generic;

namespace CE.InsightDataAccess.Models
{
    public partial class Portals
    {
        public Portals()
        {
            UserPortal = new HashSet<UserPortal>();
        }

        public int PortalId { get; set; }
        public string PortalName { get; set; }
        public bool IsEnabled { get; set; }
        public string PortalComponent { get; set; }

        public virtual ICollection<UserPortal> UserPortal { get; set; }
    }
}
