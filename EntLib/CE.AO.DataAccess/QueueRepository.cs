﻿using System;
using Microsoft.WindowsAzure.Storage.Queue;
using System.Threading.Tasks;
using System.Configuration;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.RetryPolicies;

namespace CE.AO.DataAccess
{
    public class QueueRepository : IQueueRepository
    {
        private readonly CloudQueueClient _queueClient;

        public QueueRepository()
        {
            var connectionString = ConfigurationManager.AppSettings.Get(StaticConfig.AzureStorageConnectionString);
            var storageAccount = CloudStorageAccount.Parse(connectionString);
            _queueClient = storageAccount.CreateCloudQueueClient();
            _queueClient.DefaultRequestOptions.MaximumExecutionTime = TimeSpan.FromHours(24);
            _queueClient.DefaultRequestOptions.ServerTimeout = TimeSpan.FromHours(24);
            _queueClient.DefaultRequestOptions.RetryPolicy = new ExponentialRetry(TimeSpan.FromSeconds(5), 3);
        }

        /// <summary>
        /// Creates the queue with the given queuename if it does not exists
        /// </summary>
        /// <param name="queueName"></param>
        /// <returns></returns>
        public async Task<bool> CreateQueueAsync(string queueName)
        {
            // Create the queue if it doesn't exist.
            var queue = _queueClient.GetQueueReference(queueName);
            return await queue.CreateIfNotExistsAsync();
        }

        /// <summary>
        /// Deletes the given queue if exists
        /// </summary>
        /// <returns></returns>
        public async Task DeleteQueueAsync(string queueName)
        {
            // Delete the queue 
            var queue = _queueClient.GetQueueReference(queueName);
            await queue.DeleteIfExistsAsync();
        }

        public async Task AddQueueAsync(string queueName, string message)
        {
            // Add the queue 
            var queue = _queueClient.GetQueueReference(queueName);
            var cloudQueueMessage = new CloudQueueMessage(message);
            await queue.AddMessageAsync(cloudQueueMessage).ConfigureAwait(false);
        }
    }
}
