﻿using System.Collections.Generic;
using System.Linq;

namespace CE.AO.DataAccess.Helper
{
    public static class ReflectionPropertyMatcher
    {
        public static T SetPropertyValues<T>(object updateObject, object retrievedObject, List<string> propertiesToUpdate)
        {
            if (updateObject == null || retrievedObject == null)  return default(T);

            var firstType = updateObject.GetType();
            if (retrievedObject.GetType() != firstType) return default(T);

            foreach (var propertyInfo in firstType.GetProperties())
            {
                if (!propertyInfo.CanRead) continue;
                var firstValue = propertyInfo.GetValue(updateObject, null);
                var secondValue = propertyInfo.GetValue(retrievedObject, null);
                if (Equals(firstValue, secondValue)) continue;
                if (propertiesToUpdate == null ||
                    propertiesToUpdate.Count(p => p.Contains(propertyInfo.Name)) != 0)
                    propertyInfo.SetValue(retrievedObject, firstValue);
            }
            return (T) retrievedObject;
        }
    }
}
