﻿using System;
using CE.AO.DataAccess.Helper;
using Microsoft.WindowsAzure.Storage.Table;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.Configuration;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.RetryPolicies;

namespace CE.AO.DataAccess
{
    public class TableRepository : ITableRepository
    {
        private readonly CloudTableClient _tableClient;
        
        public TableRepository()
        {
            var connectionString = ConfigurationManager.AppSettings.Get(StaticConfig.AzureStorageConnectionString);
            var storageAccount = CloudStorageAccount.Parse(connectionString);
            _tableClient = storageAccount.CreateCloudTableClient();
            _tableClient.DefaultRequestOptions.MaximumExecutionTime = TimeSpan.FromHours(24);
            _tableClient.DefaultRequestOptions.RetryPolicy = new ExponentialRetry(TimeSpan.FromSeconds(5), 3);
        }

        public List<DynamicTableEntity> GetScheduleMessage(string tableName, int maxEntitiesToFetch, string filterString, ref TableContinuationToken token)
        {

            var table = _tableClient.GetTableReference(tableName);
            var query = new TableQuery().Take(maxEntitiesToFetch);
            query.FilterString = filterString;
            var queryResult = table.ExecuteQuerySegmented(query, token);
            token = queryResult.ContinuationToken;
            return queryResult.Results;

        }


        /// <summary>
        /// Creates the table with the given tablename if it does not exists
        /// </summary>
        /// <param name="tableName"></param>
        /// <returns></returns>
        public async Task<bool> CreateTableAsync(string tableName)
        {
            // Create the table if it doesn't exist.
            var table = _tableClient.GetTableReference(tableName);
            return await table.CreateIfNotExistsAsync();
        }

        /// <summary>
        /// Deletes the given table if exists
        /// </summary>
        /// <returns></returns>
        public async Task DeleteTableAsync(string tableName)
        {
            // Delete the table 
            var table = _tableClient.GetTableReference(tableName);
            await table.DeleteIfExistsAsync();
        }

        /// <summary>
        /// inserts the given T to the given tablename
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="tableModel"></param>
        /// <param name="tableName"></param>
        /// <returns></returns>
        public async Task<bool> InsertAsync<T>(T tableModel, string tableName)
        {
            // Create the table if it doesn't exist.
            var table = _tableClient.GetTableReference(tableName);
            var insertOperation = TableOperation.Insert(tableModel as ITableEntity);
            var result = await table.ExecuteAsync(insertOperation);
            return HttpStatusCodeHelper.GetResultFromTableResult(result);
        }

        /// <summary>
        /// Inserts batch records of type T to the given table name
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="tableModel"></param>
        /// <param name="tableName"></param>
        /// <returns></returns>
        public async Task<bool> InsertBatchAsync<T>(List<T> tableModel, string tableName)
        {
            //declare TableBatchOperation
            var tableBatchOperation = new TableBatchOperation();
            // Create the table if it doesn't exist.
            var table = _tableClient.GetTableReference(tableName);

            foreach (var item in tableModel)
            {
                tableBatchOperation.Insert(item as ITableEntity);
            }
            var result = await table.ExecuteBatchAsync(tableBatchOperation);
            return HttpStatusCodeHelper.GetResultFromTableListResult(result);
        }

        /// <summary>
        /// Inserts or Updates batch records of type T to the given table name
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="tableModel"></param>
        /// <param name="tableName"></param>
        /// <returns></returns>
        public async Task<bool> InsertOrMergeBatchAsync<T>(List<T> tableModel, string tableName)
        {
            //declare TableBatchOperation
            var tableBatchOperation = new TableBatchOperation();
            // Create the table if it doesn't exist.
            var table = _tableClient.GetTableReference(tableName);

            foreach (var item in tableModel)
            {
                tableBatchOperation.InsertOrMerge(item as ITableEntity);
            }
            var result = await table.ExecuteBatchAsync(tableBatchOperation);
            return HttpStatusCodeHelper.GetResultFromTableListResult(result);
        }

        /// <summary>
        /// Gets all records matching to the filter criteria
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="tableName"></param>
        /// <param name="filter"></param>
        /// <returns>TableQuery.GenerateFilterCondition("PartitionKey", QueryComparisons.Equal, "something")</returns>
        public IEnumerable<T> GetAllAsync<T>(string tableName, string filter)
            where T : ITableEntity, new()
        {
            var table = _tableClient.GetTableReference(tableName);
            var query = new TableQuery<T>().Where(filter);
            var result = table.ExecuteQuery(query);
            return result;
        }

        public IEnumerable<T> GetAllAsync<T>(string tableName, TableQuery<T> query)
            where T : ITableEntity, new()
        {
            var table = _tableClient.GetTableReference(tableName);
            var result = table.ExecuteQuery(query);
            return result;
        }

        /// <summary>
        /// Gets the single object with the given criteria
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="tableName"></param>
        /// <param name="partitionKey"></param>
        /// <param name="rowKey"></param>
        /// <returns></returns>
        public async Task<T> GetSingleAsync<T>(string tableName, string partitionKey, string rowKey)
            where T : ITableEntity, new()
        {
            var table = _tableClient.GetTableReference(tableName);
            var retrieveOperation = TableOperation.Retrieve<T>(partitionKey, rowKey);
            var retrievedResult = await table.ExecuteAsync(retrieveOperation);
            if (retrievedResult.Result != null)
                return (T)retrievedResult.Result;
            return default(T);
        }

        /// <summary>
        /// Gets the single object with the given criteria
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="tableName"></param>
        /// <param name="partitionKey"></param>
        /// <param name="rowKey"></param>
        /// <returns></returns>
        public T GetSingle<T>(string tableName, string partitionKey, string rowKey)
            where T : ITableEntity, new()
        {
            var table = _tableClient.GetTableReference(tableName);
            var retrieveOperation = TableOperation.Retrieve<T>(partitionKey, rowKey);
            var retrievedResult = table.Execute(retrieveOperation);
            if (retrievedResult.Result != null)
                return (T)retrievedResult.Result;
            return default(T);
        }

        /// <summary>
        /// Updates an entity with the given credentials on update data
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="tableName"></param>
        /// <param name="partitionKey"></param>
        /// <param name="rowKey"></param>
        /// <param name="updateData"></param>
        /// <param name="propertiesToUpdate"></param>
        /// <returns></returns>
        public async Task<bool> ReplaceSingleAsync<T>(string tableName, string partitionKey, string rowKey, T updateData, List<string> propertiesToUpdate)
            where T : ITableEntity, new()
        {
            var table = _tableClient.GetTableReference(tableName);
            var retrieveOperation = TableOperation.Retrieve<T>(partitionKey, rowKey);
            // Execute the retrieve operation.
            var retrievedResult = await table.ExecuteAsync(retrieveOperation);

            var resultObject = (T)retrievedResult.Result;

            //Update
            if (resultObject != null)
            {
                resultObject = ReflectionPropertyMatcher.SetPropertyValues<T>(updateData, resultObject, propertiesToUpdate);
                // Create the InsertOrReplace TableOperation
                var updateOperation = TableOperation.Replace(resultObject);
                // Execute the operation.
                var returnData = await table.ExecuteAsync(updateOperation);
                return HttpStatusCodeHelper.GetResultFromTableResult(returnData);
            }
            return false;
        }

        /// <summary>
        /// carries out InsertOrMerge for the given object
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="tableName"></param>
        /// <param name="updateData">
        /// </param>
        /// <returns></returns>
        public async Task<bool> InsertOrMergeSingleAsync<T>(string tableName, T updateData)
            where T : ITableEntity, new()
        {
            // Create the CloudTable object that represents the "tableName" table.
            var table = _tableClient.GetTableReference(tableName);

            // Create the InsertOrReplace TableOperation
            var insertOrMerge = TableOperation.InsertOrMerge(updateData);
            // Execute the operation.
            var returnData = await table.ExecuteAsync(insertOrMerge);
            return HttpStatusCodeHelper.GetResultFromTableResult(returnData);
        }

        /// <summary>
        /// carries out InsertOrMerge for the given object
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="tableName"></param>
        /// <param name="updateData">
        /// </param>
        /// <returns></returns>
        public bool InsertOrMergeSingle<T>(string tableName, T updateData)
            where T : ITableEntity, new()
        {
            // Create the CloudTable object that represents the "tableName" table.
            var table = _tableClient.GetTableReference(tableName);

            // Create the InsertOrReplace TableOperation
            var insertOrMerge = TableOperation.InsertOrMerge(updateData);
            // Execute the operation.
            var returnData = table.Execute(insertOrMerge);
            return HttpStatusCodeHelper.GetResultFromTableResult(returnData);
        }

        /// <summary>
        /// carries out InsertOrReplace for the given object
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="tableName"></param>
        /// <param name="updateData">
        /// </param>
        /// <returns></returns>
        public async Task<bool> InsertOrReplaceSingleAsync<T>(string tableName, T updateData)
            where T : ITableEntity, new()
        {
            // Create the CloudTable object that represents the "tableName" table.
            var table = _tableClient.GetTableReference(tableName);

            // Create the InsertOrReplace TableOperation
            var insertOrReplaceOperation = TableOperation.InsertOrReplace(updateData);
            // Execute the operation.
            var returnData = await table.ExecuteAsync(insertOrReplaceOperation);
            return HttpStatusCodeHelper.GetResultFromTableResult(returnData);
        }

        /// <summary>
        /// Delete the record for the given parameters
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="tableName"></param>
        /// <param name="partitionKey"></param>
        /// <param name="rowKey"></param>
        /// <returns></returns>
        public async Task<bool> DeleteSingleAsync<T>(string tableName, string partitionKey, string rowKey)
            where T : ITableEntity, new()
        {
            // Create the CloudTable object that represents the "tableName" table.
            var table = _tableClient.GetTableReference(tableName);

            // Create a retrieve operation that takes a "tableName" entity.
            var retrieveOperation = TableOperation.Retrieve<T>(partitionKey, rowKey);

            // Execute the retrieve operation.
            var retrievedResult = await table.ExecuteAsync(retrieveOperation);

            if (retrievedResult.Result == null) return false;
            var data = (T)retrievedResult.Result;
            var dOperation = TableOperation.Delete(data);
            var result = await table.ExecuteAsync(dOperation);
            return HttpStatusCodeHelper.GetResultFromTableResult(result);
        }

        /// <summary>
        /// Delete the record for the given parameters
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="tableName"></param>
        /// <param name="partitionKey"></param>
        /// <param name="rowKey"></param>
        /// <returns></returns>
        public bool DeleteSingle<T>(string tableName, string partitionKey, string rowKey)
            where T : ITableEntity, new()
        {
            // Create the CloudTable object that represents the "tableName" table.
            var table = _tableClient.GetTableReference(tableName);

            // Create a retrieve operation that takes a "tableName" entity.
            var retrieveOperation = TableOperation.Retrieve<T>(partitionKey, rowKey);

            // Execute the retrieve operation.
            var retrievedResult = table.Execute(retrieveOperation);

            if (retrievedResult.Result == null) return false;
            var data = (T)retrievedResult.Result;
            var dOperation = TableOperation.Delete(data);
            var result = table.Execute(dOperation);
            return HttpStatusCodeHelper.GetResultFromTableResult(result);
        }

        /// <summary>
        /// Gets all records matching to the filter criteria
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="tableName"></param>
        /// <param name="filter"></param>
        /// <param name="maxEntitiesToFetch"></param>
        /// <param name="token">ContinuationToken token</param>
        /// <returns>List<DynamicTableEntity/></returns>
        public List<DynamicTableEntity> GetAllAsync<T>(string tableName, string filter, int maxEntitiesToFetch, ref TableContinuationToken token)
            where T : ITableEntity, new()
        {
            var table = _tableClient.GetTableReference(tableName);
            var query = new TableQuery().Take(maxEntitiesToFetch);
            query.FilterString = filter;

            var queryResult = table.ExecuteQuerySegmented(query, token);
            token = queryResult.ContinuationToken;
            return queryResult.Results;
        }
    }
}
