﻿using Microsoft.WindowsAzure.Storage.Table;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CE.AO.DataAccess
{
    public interface ITableRepository
    {
        Task<bool> CreateTableAsync(string tableName);
        Task DeleteTableAsync(string tableName);
        Task<bool> InsertAsync<T>(T tableModel, string tableName);
        Task<bool> InsertBatchAsync<T>(List<T> tableModel, string tableName);
        Task<bool> InsertOrMergeBatchAsync<T>(List<T> tableModel, string tableName);
        IEnumerable<T> GetAllAsync<T>(string tableName, string filter)
            where T : ITableEntity, new();
        IEnumerable<T> GetAllAsync<T>(string tableName, TableQuery<T> query)
            where T : ITableEntity, new();
        Task<T> GetSingleAsync<T>(string tableName, string partitionKey, string rowKey)
            where T : ITableEntity, new();
        bool DeleteSingle<T>(string tableName, string partitionKey, string rowKey)
            where T : ITableEntity, new();
        T GetSingle<T>(string tableName, string partitionKey, string rowKey)
            where T : ITableEntity, new();
        Task<bool> ReplaceSingleAsync<T>(string tableName, string partitionKey, string rowKey, T updateData, List<string> propertiesToUpdate)
            where T : ITableEntity, new();
        Task<bool> InsertOrReplaceSingleAsync<T>(string tableName, T updateData)
            where T : ITableEntity, new();
        Task<bool> InsertOrMergeSingleAsync<T>(string tableName, T updateData)
            where T : ITableEntity, new();

        bool InsertOrMergeSingle<T>(string tableName, T updateData)
            where T : ITableEntity, new();
        Task<bool> DeleteSingleAsync<T>(string tableName, string partitionKey, string rowKey)
            where T : ITableEntity, new();
        
        List<DynamicTableEntity> GetScheduleMessage(string tableName, int maxEntitiesToFetch, string filterString, ref TableContinuationToken token);
        List<DynamicTableEntity> GetAllAsync<T>(string tableName, string filter, int maxEntitiesToFetch, ref TableContinuationToken token) where T : ITableEntity, new();
    }
}
