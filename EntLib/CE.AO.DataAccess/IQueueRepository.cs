﻿using System.Threading.Tasks;

namespace CE.AO.DataAccess
{
    public interface IQueueRepository
    {
        Task<bool> CreateQueueAsync(string queueName);
        Task DeleteQueueAsync(string queueName);
        Task AddQueueAsync(string queueName, string message);
    }
}
