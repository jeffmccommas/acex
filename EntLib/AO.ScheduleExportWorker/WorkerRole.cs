using System;
using System.Diagnostics;
using System.Net;
using System.Threading;
using Microsoft.Azure.WebJobs;
using Microsoft.WindowsAzure.ServiceRuntime;
using System.Configuration;
using AO.Registrar;
using CE.AO.Utilities;
using Microsoft.Practices.Unity;

namespace AO.ScheduleExportWorker
{
    public class WorkerRole : RoleEntryPoint
    {
        private readonly CancellationTokenSource _cancellationTokenSource = new CancellationTokenSource();
        private readonly ManualResetEvent _runCompleteEvent = new ManualResetEvent(false);

        public override void Run()
        {
            Trace.TraceInformation("AO.ScheduleExportWorker is running");

            try
            {
                if (!_cancellationTokenSource.Token.IsCancellationRequested)
                {
                    new DataStorageRegistrar().Initialize<TransientLifetimeManager>();
                    new CE.ContentModel.AutoMapperBootStrapper().BootStrap();
                    var storageConn =
                        ConfigurationManager.ConnectionStrings[StaticConfig.AclaraOneStorageConnectionString].ToString();
                    var config = new JobHostConfiguration { NameResolver = new QueueNameResolver() };

                    config.Queues.BatchSize = 8; //(default is 16).
                    config.Queues.MaxDequeueCount = 5; //(default is 5).
                    config.Queues.MaxPollingInterval = TimeSpan.FromSeconds(15); //(default is 1 minute).
                    config.StorageConnectionString = storageConn;
                    config.DashboardConnectionString = storageConn;

                    var host = new JobHost(config);
                    host.RunAndBlock();
                }

                //Wait for shutdown to be called, else the role will recycle
                _runCompleteEvent.WaitOne();
            }
            finally
            {
                _runCompleteEvent.Set();
            }
        }

        public override bool OnStart()
        {
            // Set the maximum number of concurrent connections
            ServicePointManager.DefaultConnectionLimit = 12;

            // For information on handling configuration changes
            // see the MSDN topic at http://go.microsoft.com/fwlink/?LinkId=166357.

            //// Installs Simba driver
            //string appRoot = Environment.GetEnvironmentVariable("RoleRoot");
            //var installerFilePath = Path.Combine(appRoot + @"\", @"approot\SimbaSparkODBC32bitDriver\SimbaSparkODBC32.msi");            
            //var installerProcess = Process.Start(installerFilePath, "/a SimbaSparkODBC32.msi");
            //while (installerProcess != null && installerProcess.HasExited == false)
            //{
            //    Thread.Sleep(250);
            //}

            bool result = base.OnStart();

            Trace.TraceInformation("AO.ScheduleExportWorker has been started");

            return result;
        }

        public override void OnStop()
        {
            Trace.TraceInformation("AO.ScheduleExportWorker is stopping");

            _cancellationTokenSource.Cancel();
            _runCompleteEvent.WaitOne();

            base.OnStop();

            Trace.TraceInformation("AO.ScheduleExportWorker has stopped");
        }
    }
}
