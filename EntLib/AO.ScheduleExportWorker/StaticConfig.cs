﻿namespace AO.ScheduleExportWorker
{
    public static class StaticConfig
    {
        public const string AclaraOneStorageConnectionString = "AclaraOneStorageConnectionString";
        public const string ScheduleExportProcessQueue = "aoscheduleexportprocessqueue";
    }
}
