﻿using System;
using System.IO;
using System.Linq;
using System.Xml;
using System.Xml.Linq;
using AO.BusinessContracts;
using AO.Registrar;
using CE.AO.Logging;
using CE.AO.Models;
using Microsoft.Azure.WebJobs;
using Microsoft.Practices.Unity;
using Enums = CE.AO.Utilities.Enums;

namespace AO.ScheduleExportWorker
{
    public class Functions
    {
        // This function will get triggered/executed when a new message is written 
        // on an Azure Queue called queue.
        public static LogModel LogModel { get; set; }

        private static readonly IUnityContainer UContainer = new UnityContainer();

        /// <summary>
        /// Trigger the export of table data.
        /// </summary>
        /// <param name="message">Queue message with format [clientid]:[filtername]</param>
        /// <param name="log">Text wirter log</param>
        public static void ScheduleExport([QueueTrigger(StaticConfig.ScheduleExportProcessQueue)] string message,
            TextWriter log)
        {
            message = GetMessageFromStorageQueueMessage(message);

            string[] parameter = message.Split(new[] { ":" }, StringSplitOptions.None).ToArray();
            var clientId = parameter[0];
            var filterName = parameter[1];

            LogModel logModel = new LogModel();
            if (LogModel == null)
            {
                logModel.Module = Enums.Module.ExportProcess;
                logModel.DisableLog = false;
                logModel.ClientId = clientId;
            }
            else
            {
                logModel = LogModel;
            }

            try
            {
                Console.WriteLine("start export");

                (new DataStorageRegistrar()).Initialize<TransientLifetimeManager>(UContainer);

                var clientConfigFacadeManager = UContainer.Resolve<IClientConfigFacade>();

                var clientSetting = clientConfigFacadeManager.GetClientSettingExportFilter(Convert.ToInt32(clientId),
                    filterName);

                string filename = clientSetting.ExportLocation + "" + clientSetting.OutputFileName + "." +
                                  DateTime.UtcNow.ToString("yyyy-MM-dd-hh_mm_ss") + ".csv";

                var iExport = UContainer.Resolve<IExportFacade>(new ParameterOverride("logModel", logModel));

                iExport.ExportData(clientSetting.BlobConnectionString, clientSetting.ContainerName, filename, clientSetting.FilterQuery, filterName);

                #region previous logic

                //ISchedulerFactory sf = new StdSchedulerFactory();
                //var sched = sf.GetScheduler();
                //ITrigger trigger;
                //var job = DailySchedule.DailyMainCall(out trigger, "DailyScheduledCall");
                //sched.ScheduleJob(job, trigger);
                //sched.Start();

                #endregion

                Console.WriteLine("export schedule done");
            }
            catch (Exception ex)
            {
                Console.WriteLine("exception occured : " + ex.Message + "   :  StackTrace is :  " + ex.StackTrace);
                Logger.Error(ex, logModel);
            }
        }

        /// <summary>
        /// Get the message content from StorageQueueMessage XML.
        /// </summary>
        /// <param name="value">StorageQueueMessage message value</param>
        /// <returns></returns>
        private static string GetMessageFromStorageQueueMessage(string value)
        {
            try
            {
                if (string.IsNullOrEmpty(value) == false)
                {
                    XElement xle = XElement.Parse(value);
                    var msg = xle.Element("Message");
                    if (msg != null) return msg.Value;
                }
                return value;
            }
            catch (XmlException)
            {
                return value;
            }
        }
    }
}
