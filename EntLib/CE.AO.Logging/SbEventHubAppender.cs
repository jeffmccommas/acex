﻿using System;
using System.Linq;
using System.Text;
using CE.AO.Models;
using log4net.Appender;
using log4net;
using log4net.Core;
using Microsoft.ServiceBus.Messaging;
using Newtonsoft.Json;

namespace CE.AO.Logging
{
    public class SbEventHubAppender : AppenderSkeleton
    {
        /// <summary>
        /// Apend using LogViewModel 
        /// </summary>
        /// <param name="loggingEvent"></param>
        protected override void Append(LoggingEvent loggingEvent)
        {
            DateTimeOffset utcDate = DateTime.UtcNow;
            var logViewModel = new LogViewModel
            {
                Message = loggingEvent.RenderedMessage,
                StackTrace = loggingEvent.ExceptionObject != null ? $"{loggingEvent.GetExceptionString()}" : string.Empty,
                Level = loggingEvent.Level.Name,
                TimestampUtc = utcDate,
                Module = "",
                ClientId = ""
            };

            bool? disableLog = false;
            var moduleDescription = "";
            if (ThreadContext.Properties["LogModel"] != null)
            {
                var logModel = (LogModel)ThreadContext.Properties["LogModel"];
                disableLog = logModel.DisableLog;
                logViewModel.ClientId = logModel.ClientId;
                logViewModel.CustomerId = logModel.CustomerId;
                logViewModel.PremiseId = logModel.PremiseId;
                logViewModel.ServiceContractId = logModel.ServiceContractId;
                logViewModel.MeterId = logModel.MeterId;
                logViewModel.AccountId = logModel.AccountId;
                logViewModel.Metadata = logModel.Metadata;
                logViewModel.Source = logModel.Source;
                logViewModel.ProcessingEventCount = logModel.ProcessingEventCount;
                logViewModel.Module = Convert.ToString(logModel.Module);
                moduleDescription = Convert.ToString(logModel.Module);
                logViewModel.ProcessingType = Convert.ToString(logModel.ProcessingType);
                logViewModel.RowIndex = logModel.RowIndex;
            }
            SetPartitionKeyAndRowKey(moduleDescription, utcDate, logViewModel);

            //Do not change this condition as disableLog can be null. Only in the case where explicitly disableLog == true do not process the log
            if (disableLog != true && ValidateEvent(logViewModel))
            {
                var serializedEventData = JsonConvert.SerializeObject(logViewModel);
                var data = new EventData(Encoding.UTF8.GetBytes(serializedEventData));

                var client = Utilities.EventHubSender.GetEventHubClient(StaticConfig.EventHubSendConnectionString, StaticConfig.EventHubName);

                try
                {
                    client.Send(data);
                }
                catch (TimeoutException)
                {
                    client.Send(data);
                }
            }
        }

        /// <summary>
        /// Sets partition key and rowkey for the event to be inserted
        /// </summary>
        private static void SetPartitionKeyAndRowKey(string moduleDescription, DateTimeOffset utcDate, LogViewModel logViewModel)
        {
            logViewModel.PartitionKey = $"{moduleDescription}_{utcDate:yyyy-MM-dd}";
            logViewModel.RowKey = $"{logViewModel.Level}_{utcDate:HH:mm:ss.fff}";
        }

        /// <summary>
        /// Checks if event data is valid
        /// </summary>        
        private static bool ValidateEvent(LogViewModel logEntity)
        {
            var isValid = true;
            var properties = logEntity.GetType().GetProperties().ToList();
            foreach (var property in properties)
            {
                var provider = System.CodeDom.Compiler.CodeDomProvider.CreateProvider("C#");
                if (!string.IsNullOrWhiteSpace(property.Name) && provider.IsValidIdentifier(property.Name) &&
                    property.Name.Length <= 255)
                {
                    // Valid
                    if (!string.Equals(property.Name, "PartitionKey") && !string.Equals(property.Name, "RowKey"))
                        continue;
                    // Valid
                    var propertyValue = Convert.ToString(property.GetValue(logEntity));
                    if (!string.IsNullOrWhiteSpace(propertyValue) && !propertyValue.Any(char.IsControl) &&
                        !propertyValue.Contains(@"\") && !propertyValue.Contains("/") && !propertyValue.Contains("#") &&
                        !propertyValue.Contains("?"))
                        continue;
                    // Not valid
                    isValid = false;
                    break;
                }
                // Not valid
                isValid = false;
                break;
            }

            return isValid;
        }
    }
}
