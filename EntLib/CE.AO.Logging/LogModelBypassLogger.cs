using System;
using CE.AO.Models;
using log4net;

namespace CE.AO.Logging
{
    /// <summary>
    /// Concrete IAoLogger implementation that allows bypassing the legacey CE.AO.Logging logger implementation and ignores LogModel argument.
    /// </summary>
    public class LogModelBypassLogger : IAoLogger
    {
        private readonly ILog _logger = LogManager.GetLogger(typeof (LogModelBypassLogger));

        public void Error(object msg, LogModel logModel)
        {
            _logger.Error(msg);
        }

        public void Error(object msg, Exception ex, LogModel logModel)
        {
            _logger.Error(msg, ex);
        }

        public void Error(Exception ex, LogModel logModel)
        {
            _logger.Error(ex.Message);
        }

        public void Warn(object msg, LogModel logModel)
        {
            _logger.Warn(msg);
        }

        public void Warn(object msg, Exception ex, LogModel logModel)
        {
            _logger.Warn(msg);
        }

        public void Warn(Exception ex, LogModel logModel)
        {
            _logger.Warn(ex.Message);
        }

        public void Info(object msg, LogModel logModel)
        {
            _logger.Info(msg);
        }

        public void Info(object msg, Exception ex, LogModel logModel)
        {
            _logger.Info(msg, ex);
        }

        public void Info(Exception ex, LogModel logModel)
        {
            _logger.Info(ex.Message, ex);
        }

        public void Fatal(object msg, LogModel logModel)
        {
            _logger.Fatal(msg);
        }

        public void Fatal(object msg, Exception ex, LogModel logModel)
        {
            _logger.Fatal(msg, ex);
        }

        public void Fatal(Exception ex, LogModel logModel)
        {
            _logger.Fatal(ex.Message, ex);
        }
    }
}

