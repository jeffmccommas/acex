﻿using System;
using CE.AO.Models;
using log4net;
using log4net.Config;

namespace CE.AO.Logging
{
    public static class Logger
    {
        private static ILog Log { get; }

        static Logger()
        {
            XmlConfigurator.Configure();
            Log = LogManager.GetLogger(typeof(Logger));            
        }

        public static void Error(object msg, LogModel logModel)
        {
            ThreadContext.Properties["LogModel"] = logModel;
            Log.Error(msg);
        }

        public static void Error(object msg, Exception ex, LogModel logModel)
        {
            ThreadContext.Properties["LogModel"] = logModel;
            Log.Error(msg, ex);
        }

        public static void Error(Exception ex, LogModel logModel)
        {
            ThreadContext.Properties["LogModel"] = logModel;
            Log.Error(ex.Message, ex);
        }

        public static void Warn(object msg, LogModel logModel)
        {
            ThreadContext.Properties["LogModel"] = logModel;
            Log.Warn(msg);
        }

        public static void Warn(object msg, Exception ex, LogModel logModel)
        {
            ThreadContext.Properties["LogModel"] = logModel;
            Log.Warn(msg, ex);
        }

        public static void Warn(Exception ex, LogModel logModel)
        {
            ThreadContext.Properties["LogModel"] = logModel;
            Log.Warn(ex.Message, ex);
        }

        public static void Info(object msg, LogModel logModel)
        {
            ThreadContext.Properties["LogModel"] = logModel;
            Log.Info(msg);
        }

        public static void Info(object msg, Exception ex, LogModel logModel)
        {
            ThreadContext.Properties["LogModel"] = logModel;
            Log.Info(msg, ex);
        }

        public static void Info(Exception ex, LogModel logModel)
        {
            ThreadContext.Properties["LogModel"] = logModel;
            Log.Info(ex.Message, ex);
        }

        public static void Fatal(object msg, LogModel logModel)
        {
            ThreadContext.Properties["LogModel"] = logModel;
            Log.Fatal(msg);
        }
        public static void Fatal(object msg, string info1,string info2)
        {
            ThreadContext.Properties["Info1"] = info1;
            ThreadContext.Properties["Info2"] = info2 ?? "n/a";
            Log.Fatal(msg);
        }

        public static void Fatal(object msg, Exception ex, LogModel logModel)
        {
            ThreadContext.Properties["LogModel"] = logModel;
            Log.Fatal(msg, ex);
        }

        public static void Fatal(Exception ex, LogModel logModel)
        {
            ThreadContext.Properties["LogModel"] = logModel;
            Log.Fatal(ex.Message, ex);
        }
    }
}
