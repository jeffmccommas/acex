using System;
using CE.AO.Models;

namespace CE.AO.Logging
{
    /// <summary>
    /// Concrete implementation that delegates to legacy CE.AO.Logger.
    /// </summary>
    public class AoLogger : IAoLogger
    {
        public void Error(object msg, LogModel logModel)
        {
            Logger.Error(msg, logModel);
        }

        public void Error(object msg, Exception ex, LogModel logModel)
        {
            Logger.Error(msg, ex, logModel);
        }

        public void Error(Exception ex, LogModel logModel)
        {
            Logger.Error(ex.Message, ex, logModel);
        }

        public void Warn(object msg, LogModel logModel)
        {
            Logger.Warn(msg, logModel);
        }

        public void Warn(object msg, Exception ex, LogModel logModel)
        {
            Logger.Warn(msg, ex, logModel);
        }

        public void Warn(Exception ex, LogModel logModel)
        {
            Logger.Warn(ex.Message, ex, logModel);
        }

        public void Info(object msg, LogModel logModel)
        {
            Logger.Info(msg, logModel);
        }

        public void Info(object msg, Exception ex, LogModel logModel)
        {
            Logger.Info(msg, ex, logModel);
        }

        public void Info(Exception ex, LogModel logModel)
        {
            Logger.Info(ex.Message, ex, logModel);
        }

        public void Fatal(object msg, LogModel logModel)
        {
            Logger.Fatal(msg, logModel);
        }

        public void Fatal(object msg, Exception ex, LogModel logModel)
        {
            Logger.Fatal(msg, ex, logModel);
        }

        public void Fatal(Exception ex, LogModel logModel)
        {
            Logger.Fatal(ex.Message, ex, logModel);
        }
    }
}

