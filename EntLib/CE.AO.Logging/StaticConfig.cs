﻿using Microsoft.Azure;

namespace CE.AO.Logging
{
    /// <summary>
    /// Static class to handle confiuration settings.
    /// </summary>
    public static class StaticConfig
    {
        /// <summary>
        /// Static constructor to set readonly fields from runtime configuraiton.
        /// </summary>
        static StaticConfig()
        {
            ConnectionString = CloudConfigurationManager.GetSetting(AzureStorageLogTableConnectionStringKey);

            EventHubName = CloudConfigurationManager.GetSetting(EventHubNameKey);

            EventHubSendConnectionString = CloudConfigurationManager.GetSetting(EventHubSendConnectionStringKey);
        }

        /// <summary>
        /// Connection string for the log table on Azure table storage.
        /// </summary>
        internal static readonly string ConnectionString;

        /// <summary>
        /// The name of the Event Hub to use in logging.
        /// </summary>
        internal static readonly string EventHubName;

        /// <summary>
        /// The Send Connection string of the Event Hub to use in logging.
        /// </summary>
        internal static readonly string EventHubSendConnectionString;

        /// <summary>
        /// The app settings key for the connection string associated with the Azure storage account used
        /// for log4net table storage. This app setting is required.
        /// </summary>
        public const string AzureStorageLogTableConnectionStringKey = "AzureStorageLogTableConnectionString";

        /// <summary>
        /// The app settings key for the name of the Azure storage table to us in logging. If the app setting does
        /// not exist a default of LogTable will be used.
        /// </summary>
        public const string AzureStorageLogTableNameKey = "AzureStorageLogTableName";

        /// <summary>
        /// Event Hub Key Name
        /// </summary>
        public const string EventHubNameKey = "AzureTableLoggingEventHubPath";

        /// <summary>
        /// Event Hub Send Connection string Key Name
        /// </summary>
        public const string EventHubSendConnectionStringKey = "AzureTableLoggingEventHubSendConnectionString";
    }
}
