﻿using System;
using CE.AO.Models;

namespace CE.AO.Logging
{
    public interface IAoLogger
    {
        void Error(object msg, LogModel logModel);
        void Error(object msg, Exception ex, LogModel logModel);
        void Error(Exception ex, LogModel logModel);
        void Warn(object msg, LogModel logModel);
        void Warn(object msg, Exception ex, LogModel logModel);
        void Warn(Exception ex, LogModel logModel);
        void Info(object msg, LogModel logModel);
        void Info(object msg, Exception ex, LogModel logModel);
        void Info(Exception ex, LogModel logModel);
        void Fatal(object msg, LogModel logModel);
        void Fatal(object msg, Exception ex, LogModel logModel);
        void Fatal(Exception ex, LogModel logModel);
    }
}