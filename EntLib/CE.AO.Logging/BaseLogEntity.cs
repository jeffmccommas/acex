﻿using System;
using CE.AO.Models;
using log4net;
using log4net.Core;
using Microsoft.WindowsAzure.Storage.Table;

namespace CE.AO.Logging
{
    /// <summary>
    /// Class to be used as base for all Azure storage log table entity classes. Default for constructors is 
    /// to initialize PartitionKey to a string representing the current month and the RowKey to string that's 
    /// a GUID appended to UTC now.
    /// </summary>
    public class BaseLogEntity : TableEntity
    {
        public BaseLogEntity()
        {
            var utc = DateTime.UtcNow;
            PartitionKey = $"{utc:yyyy.MM}";
            RowKey = $"{utc:yyyy.MM.dd HH:mm:ss.fff}-{Guid.NewGuid()}";            
        }
        public bool? DisableLog { get; }

        // ReSharper disable once UnusedParameter.Local
        public BaseLogEntity(LoggingEvent loggingEvent) : this()
        {
            if (ThreadContext.Properties["LogModel"] != null)
            {
                var logModel = (LogModel)ThreadContext.Properties["LogModel"];                
                DisableLog = logModel.DisableLog;
            }
        }
        public virtual void AfterInsert(TableResult tableResult) { }
    }
}
