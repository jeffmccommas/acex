﻿
namespace CE.GreenButton
{
    public class ReportSummary : ReportBase
    {
        private const string TemplateName = "Summary";

        public override string ReadTemplate()
        {
            return TemplateName;//base.LoadDocument(@"C:\acltfs\EnergyPrism\12.03\EntLib\GreenButton\Aclara.GreenButton\Summary.xml");
        }

        public override string ReplaceToken(string token)
        {
            string value;

            switch (token)
            {
                case "%SummaryTitle%":
                    value = SharedObject["SummaryTitle"].ToString();
                    break;
                
                case "%Multiplier%":
                    value = SharedObject["PowerOfTen"].ToString();
                    break;

                case "%StatusTime%":
                    value = SharedObject["StartDateInt"].ToString();
                    break;

                case "%UOM%":
                    value = SharedObject["UOM"].ToString();
                    break;

                case "%TotalUsage%":
                    value = SharedObject["TotalUsage"].ToString();
                    break;
               
                case "%StartDate%":
                    value = SharedObject["StartDate"].ToString();
                    break;

                case "%LINK_SELF%":
                    value = PreLink + "/UsagePoint/01/ElectricPowerUsageSummary/01";
                    break;

                case "%LINK_PARENT%":
                    value = PreLink + "/UsagePoint/01/ElectricPowerUsageSummary";
                    break;
                
                default:
                    value = base.ReplaceToken(token);
                    break;
            }

            return value;
        }
    }
}
