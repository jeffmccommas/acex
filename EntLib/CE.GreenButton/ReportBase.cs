﻿using CE.Xml.Transformation;
using System;
using System.Collections;

namespace CE.GreenButton
{
    public class ReportBase : IXmlPart
    {
        protected int FuelId;
        protected string PreLink;

        protected bool EnableLinks;

        protected IdGenerator IdGenerator;

        public Hashtable SharedObject
        {
            get;
            set;
        }

        public string NewGuid
        {
            get { return "urn:uuid:" + Guid.NewGuid().ToString(); }
        }

        public System.Xml.XmlDocument LoadDocument(string path)
        {
            System.Xml.XmlDocument doc = new System.Xml.XmlDocument();

            doc.Load(path);

            return doc;
        }

        public void CreateLink(string relation, string value, System.Xml.XmlWriter writer)
        {
            if (EnableLinks)
            {
                writer.WriteStartElement("link");
                writer.WriteAttributeString("rel", relation);
                writer.WriteAttributeString("href", value);
                writer.WriteEndElement();
            }
        }

        public virtual void Fill()
        {
            IdGenerator = (IdGenerator)SharedObject["IDGenerator"];

            FuelId = int.Parse(SharedObject["Fuel"].ToString());
            EnableLinks = bool.Parse(SharedObject["EnableLinks"].ToString());

            PreLink = "User/" + IdGenerator.LinkId;
        }

        public virtual string ReadTemplate()
        {
            return string.Empty;
        }

        public virtual string ReplaceToken(string token)
        {
            if (token.Equals("%GUID%"))
                return NewGuid;
            else
                return string.Empty;
        }

        public virtual void GenerateXml(System.Xml.XmlWriter writer)
        {

        }

        // CR 47718 april 2014 - bill xml green button
        public virtual void GenerateMeterBillXml(System.Xml.XmlWriter writer)
        {

        }

    }

}
