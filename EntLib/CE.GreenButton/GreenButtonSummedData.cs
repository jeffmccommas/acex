﻿using System;

namespace CE.GreenButton
{
    public class GreenButtonSummedData
    {
        private int _databaseMeterId;

        public int DatabaseMeterId
        {
            get { return _databaseMeterId; }
            set { _databaseMeterId = value; }
        }

        private DateTime _startDate;

        public DateTime StartDate
        {
            get { return _startDate; }
            set { _startDate = value; }
        }

        private DateTime _endDate;

        public DateTime EndDate
        {
            get { return _endDate; }
            set { _endDate = value; }
        }

        private float _cost;

        public float Cost
        {
            get { return _cost; }
            set { _cost = value; }
        }

        private float _usage;

        public float Usage
        {
            get { return _usage; }
            set { _usage = value; }
        }
        
        
        
    }
}
