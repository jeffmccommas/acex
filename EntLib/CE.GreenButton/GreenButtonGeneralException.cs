﻿using System;
using System.Runtime.Serialization;

namespace CE.GreenButton
{
    [Serializable]
    public class GreenButtonGeneralException : Exception
    {
         #region "Public Constructors"

        public GreenButtonGeneralException()
        {
        }

        public GreenButtonGeneralException(string message)
            : base(message)
        {
        }

        public GreenButtonGeneralException(string message, Exception inner)
            : base(message, inner)
        {
        }

        #endregion

        #region "Protected Constructors"

        protected GreenButtonGeneralException(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
        }

        #endregion
    }
}
