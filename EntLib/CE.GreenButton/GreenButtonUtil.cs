﻿using CE.AO.Utilities;
using CE.Models;
using CE.Models.Insights;
using CE.Xml.Transformation;
using System;
using System.Collections.Generic;
using System.Globalization;
//using System.IO;
using System.Linq;
//using System.Security.AccessControl;
//using System.Security.AccessControl;
//using System.Text;
//using System.IO;
//using System.Text;
using System.Threading.Tasks;
//using CE.Infrastructure;
using CE.Models.Insights.Types;

namespace CE.GreenButton
{
    public class GreenButtonUtil
    {
        #region Private Constants

        //private const int UomidNotApplicable = 0;
        private const int UomidCubicFeet = 119;
        private const int UomidTherm = 169;
        private const int UomidWattHours = 72;
        private const int UomidCubicMeter = 42;
        private const int UomidGal = 128;

        private const string CurrencyIdNotApplicable = "0";
        private const string CurrencyIdUsd = "840";
        private const string CurrencyIdCad = "124";

        private const int KindElectric = 0;
        private const int KindGas = 1;
        private const int KindWater = 2;

        #endregion
        private int _clientId;

        private GreenButtonConfig _config;

        //private IUnityContainer container;

        private const string ReplaceTokenResourceKey = "{%resource%}";

        public static IInsightsEFRepository InsightsEfRepository = new InsightsEfRepository();

        public GreenButtonUtil(int clientId, GreenButtonConfig config)
        {
            _config = config;
            _clientId = clientId;
        }

        /// <summary>
        /// Testability, passed in repository so not relying on data access through sql server
        /// </summary>
        /// <param name="clientId"></param>
        /// <param name="config"></param>
        /// <param name="insightsEfRepository"></param>
        public GreenButtonUtil(int clientId, GreenButtonConfig config, IInsightsEFRepository insightsEfRepository) : this(clientId,config)
        {
            if (insightsEfRepository == null)
            {
                // ReSharper disable once NotResolvedInText
                throw new ArgumentNullException("InsightsRepository is null!");
            }
            InsightsEfRepository = insightsEfRepository;

        }

        public string OlysonTimeZoneKey { get; set; }

        //IsDay
        static public bool IsDay(int inDuration)
        {
            // one day is 86400.  Adding +/- an hour to allow for DST (86400 +/- 3600)
            if (inDuration >= 82800 & inDuration <= 90000)
                return true;
            else
                return false;
        }

        /// <summary>
        /// Convert duration to Resolution Type
        /// </summary>
        /// <param name="duration"></param>
        /// <returns></returns>
        public static ResolutionType ConvertGreenButtonDurationToResolution(int duration)
        {
            ResolutionType retval;
            switch (duration)
            {
                case 300:
                    retval = ResolutionType.five;
                    break;
                case 900:
                    retval = ResolutionType.fifteen;
                    break;
                case 1800:
                    retval = ResolutionType.thirty;
                    break;
                case 3600:
                    retval = ResolutionType.hour;
                    break;
                case 7200:
                    retval = ResolutionType.twohour;
                    break;
                case 14400:
                    retval = ResolutionType.fourhour;
                    break;
                case 21600:
                    retval = ResolutionType.sixhour;
                    break;
                case 43200:
                    retval = ResolutionType.twelvehour;
                    break;
                default:
                    if (duration > 43200)
                    {
                        retval = ResolutionType.bill;
                    }
                    else
                        retval = ResolutionType.none;
                    break;

            }
            return retval;

        }

        /// <summary>
        /// Generate Ami Xml
        /// </summary>
        /// <param name="customerId"></param>
        /// <param name="accountId"></param>
        /// <param name="premiseId"></param>
        ///// <param name="serviceId"></param>
        ///// <param name="meterId"></param>
        /// <param name="meterConsumptionList"></param>
        /// <param name="startDate"></param>
        /// <param name="endDate"></param>
        ///// <param name="commodityType"></param>
        ///// <param name="servicePoint"></param>
        ///// <param name="intervalConsumptionDataList"></param>
        ///// <param name="convertUom"></param>
        ///// <param name="defaultConversionFactor"></param>
        ///// <param name="useDefaultConversionFactor"></param>
        ///// <param name="uom"></param>
        /// <param name="outputFormat"></param>
        /// <param name="premiseAddress"></param>
        /// <returns></returns>
        public ExportResult GenerateAmiXml(string customerId,
                                       string accountId,
                                       string premiseId,
                                       List<MeterConsumption > meterConsumptionList,
                                       //string serviceId,
                                       //string meterId,
                                       DateTime startDate,
                                       DateTime endDate,
                                       //Enums.CommodityType commodityType,
                                       //string servicePoint,
                                       //DateTime amiStartDate,
                                       //DateTime amiEndDate,
                                       //List<IntervalConsumptionData> intervalConsumptionDataList,
                                       //bool convertUom,
                                       //double defaultConversionFactor,
                                       //bool useDefaultConversionFactor,
                                       //string uom,
                                       OutputFormatType outputFormat,
                                       //int appId,
                                       //int csrAppId,
                                       //string csrAppUserName,
                                       //List<ReplacedMeter> replacedMeterList,
                                       //string actualConversionFactor = "",
                                       string premiseAddress = "")
        {
            ExportResult result;
            //EntAMRResultCollection amrResult = null;

            try
            {
                var greenButtonList = new List<GreenButtonXml>();
                foreach (var meterConsumption in meterConsumptionList)
                {
                    var completed = false;
                    var commodityType = (Enums.CommodityType) meterConsumption.CommodityId;
                    var greenButtonInfo = new GreenButtonInfo()
                    {
                        ClientId = _clientId,
                        CustomerId = customerId,
                        AccountId = accountId,
                        //ServiceId = meterConsumption.ServiceId,
                        PremiseId = premiseId,
                        MeterId = meterConsumption.MeterId,
                        FuelType = commodityType,
                        StartDate = startDate,
                        EndDate = endDate,
                        //ServicePoint = meterConsumption.ServicePointId,
                        Format = ReportFormat.Xml,
                        Multiplier = meterConsumption.DefaultConversioniFactor.ToString(CultureInfo.InvariantCulture),
                        DataType = ReportDataType.Amr,
                        Request = RequestType.ManualDownload,
                        //ReplacedMeters = replacedMeterList
                    };

                    var timeout = (_config.MaxLoadingTime == -1 ? -1 : _config.MaxLoadingTime * 1000);
                    var task = new Task<GreenButtonXml>(() =>
                    {
                        var fuelSupported = CheckIfFuelSupported(commodityType);

                        if(fuelSupported)
                        {
                            var uomKey = meterConsumption.UomKey;
                            if (meterConsumption.ConvertUom)
                            {
                                if (commodityType == Enums.CommodityType.Gas)
                                    uomKey = _config.GasDisplayUom;
                            }

                            var intervalName = Common.GetResolutionKey(meterConsumption.IntervalConsumptionList);
                            if (intervalName != string.Empty)
                            {
                                var filename = GenerateExportFileName(commodityType,
                                                                          intervalName,
                                                                          startDate,
                                                                          endDate);

                                var greenButtonXml = GenerateAmiXml(greenButtonInfo,
                                                                                    intervalName,
                                                                                    meterConsumption.IntervalConsumptionList,
                                                                                    commodityType,
                                                                                    ReportFormat.Xml,
                                                                                    filename,
                                                                                    meterConsumption.ConvertUom,
                                                                                    uomKey,
                                                                                    premiseAddress);

                                //var greenButtonHistoryResult = new GreenButtonHistory();
                                //greenButtonHistoryResult.FileName = greenButtonFile.FileName;
                                //greenButtonHistoryResult.Info = greenButtonInfo;
                                //greenButtonHistoryResult.FileMemory = greenButtonFile.File;
                                //greenButtonHistoryResult.Xml = greenButtonFile.Xml;
                                //greenButtonHistoryResult.CreationDate = DateTime.UtcNow;
                                //greenButtonHistoryResult.UpdationDate = DateTime.UtcNow;
                                return greenButtonXml;
                            }
                            else
                            {
                                throw new Exception("No Usage exists for this Meter/Date Range");
                            }
                        }
                        else
                        {
                            return null;
                        }


                    });

                    task.Start();
                    task.Wait(timeout);

                    if (task.Status == TaskStatus.RanToCompletion)
                    {
                        completed = true;
                    }

                    if (completed)
                    {
                        var greenButtonXml = task.Result;
                        if(greenButtonXml != null)
                        {
                            greenButtonList.Add(greenButtonXml);
                        }

                        //result.FilePath = _config.SharedDrivePath + greenButtonHistory.SharedDrivePointer;
                        //result.FileName = greenButtonHistory.FileName;

                        
                    }
                    else
                    {
                        throw new Exception(_config.BillXmlTimeoutError);
                    }
                }

                if (greenButtonList.Any())
                {
                    result = new ExportResult();
                    if (outputFormat == OutputFormatType.Zip)
                    {
                        if (_config.ProcessorInfoFileName.Length > 0)
                        {
                            if (_config.ProcessInfoFile.Length > 0)
                            {
                                var xsltInfo = new GreenButtonXml
                                {
                                    FileName = _config.ProcessorInfoFileName,
                                    Xml = _config.ProcessInfoFile
                                }
                                    ;
                                greenButtonList.Add(xsltInfo);
                            }
                        }
                        //var fileId = Guid.NewGuid().ToString();
                        var zippedStream = ZipUtility.CompressFilesInMemory(greenButtonList,
                            greenButtonList[0].FileName);
                        var filename = GenerateExportZipFileName(startDate, endDate);

                        result.FileName = filename + ".zip";
                        result.File = zippedStream;
                    }
                    else
                    {
                        result.FileName = greenButtonList[0].FileName;
                        result.FileXml = greenButtonList[0].Xml;
                    }
                    result.FileType = outputFormat.ToString().ToLower();
                }
                else
                {

                    return null;

                }
                

            }
            catch (Exception ex)
            {
                result = new ExportResult
                {
                    Failure = true,
                    ErrorMessage = ex.Message
                };
            }


            return result;
        }

        /// <summary>
        /// generate bill xml green button export
        /// cr 47718 april 2014
        /// </summary>
        /// <param name="customerId"></param>
        /// <param name="accountId"></param>
        /// <param name="premiseId"></param>
        /// <param name="startDate"></param>
        /// <param name="endDate"></param>
        /// <param name="outputFormat"></param>
        /// <param name="premiseAddress"></param>
        /// <param name="serviceId"></param>
        /// <returns></returns>
        public ExportResult GenerateBillXml(string customerId,
                                       string accountId,
                                       string premiseId,
                                       string serviceId,
                                       DateTime startDate,
                                       DateTime endDate,
                                       OutputFormatType outputFormat,
                                       //NXEnums.AccountType accountType,
                                       //Enums.CommodityType resource,
                                       //int appId,
                                       //int csrAppId,
                                       //string csrAppUserName,
                                       string premiseAddress = "")
        {
            ExportResult result = null;
            GreenButtonHistory greenButtonHistory;
            var completed = false;
            try
            {


                var greenButtonInfo = new GreenButtonInfo()
                {
                    ClientId = _clientId,
                    CustomerId = customerId,
                    AccountId = accountId,
                    PremiseId = premiseId,
                    //FuelType = resource,
                    StartDate = startDate,
                    EndDate = endDate,
                    Format = ReportFormat.Xml,
                    DataType = ReportDataType.Bill,
                    //AppId = appId,
                    //CsrAppId = csrAppId,
                    //CsrAppUsername = csrAppUserName,
                    //PageId = pageId,
                    Request = RequestType.ManualDownload,
                    ServiceId = serviceId
                };
                

                var timeout = (_config.MaxLoadingTime == -1 ? -1 : _config.MaxLoadingTime * 1000);
                var task = new Task<GreenButtonHistory>(() =>
                {
                    var filename = GenerateBillXmlExportFileName(startDate, endDate);
                    var accountBillData = GetBillData(greenButtonInfo);

                    if (accountBillData != null && accountBillData.Any())
                    {
                        var greenButtonFile = GenerateBillXml(greenButtonInfo, accountBillData, ReportFormat.Xml,
                            filename, outputFormat, premiseAddress);


                        var greenButtonHistoryResult = new GreenButtonHistory();
                        //greenButtonHistoryResult.SharedDrivePointer = sharedDrivePointer;
                        greenButtonHistoryResult.FileName = filename.Replace(ReplaceTokenResourceKey, "");
                        greenButtonHistoryResult.Info = greenButtonInfo;
                        greenButtonHistoryResult.FileMemory = greenButtonFile.File;
                        greenButtonHistoryResult.Xml = greenButtonFile.Xml;
                        greenButtonHistoryResult.CreationDate = DateTime.UtcNow;
                        greenButtonHistoryResult.UpdationDate = DateTime.UtcNow;
                        return greenButtonHistoryResult;
                    }
                    else
                        return null;
                });

                task.Start();
                task.Wait(timeout);

                if (task.Status == TaskStatus.RanToCompletion)
                {
                    completed = true;
                }

                if (completed)
                {
                    greenButtonHistory = task.Result;

                    if(greenButtonHistory != null)
                    {
                        result = new ExportResult();
                        result.FileName = greenButtonHistory.FileName;

                        if (outputFormat == OutputFormatType.Zip)
                        {
                            result.File = greenButtonHistory.FileMemory;
                            result.FileType = "zip";
                        }
                        else
                        {
                            result.FileXml = greenButtonHistory.Xml;
                            result.FileType = "xml";
                        }
                    }
                    
                }
                else
                {
                    throw new Exception(_config.BillXmlTimeoutError);
                }

            }
            catch (Exception ex)
            {
                if (result != null)
                {
                    result.Failure = true;
                    result.ErrorMessage = ex.Message;
                }
                else
                {

                    result = new ExportResult
                    {
                        Failure = true,
                        ErrorMessage = ex.Message
                    };
                }
            }
            

            return result;
        }

        /// <summary>
        /// get bills for all services
        /// </summary>
        /// <param name="info"></param>
        /// <returns></returns>
        private List<BillAccount> GetBillData(GreenButtonInfo info)
        {
            List<BillAccount> filterAccounts = null;
            var billRequest = new BillRequest
            {
                CustomerId = info.CustomerId,
                StartDate = info.StartDate,
                EndDate = info.EndDate,
                Count = _config.BillMonths // default to 24 to be consistant with bill history
            };

            var customerBills = InsightsEfRepository.GetBills(info.ClientId, billRequest);

            if(customerBills.Customer != null)
            {
                var accounts = customerBills.Customer.Accounts.Select(a => a.Id).Distinct().ToList();

                if (!string.IsNullOrEmpty(info.AccountId))
                {
                    accounts = new List<string> { info.AccountId };
                }

                // filter the bills by the account id/premise id/serivce id
                // otherwise return list of bills per service
                filterAccounts = new List<BillAccount>();
                foreach (var id in accounts)
                {
                    var account = customerBills.Customer.Accounts.Find(a => a.Id == id);

                    var services =
                        account.Bills.SelectMany(b => b.Premises).SelectMany(p => p.Service).Select(s => s.Id).Distinct();

                    foreach (var service in services)
                    {
                        var filterBills = new List<Bill>();
                        foreach (var bill in account.Bills)
                        {
                            var filterPremises = new List<BillPremise>();
                            foreach (var premise in bill.Premises)
                            {
                                if ((!string.IsNullOrEmpty(info.PremiseId) && premise.Id == info.PremiseId) || string.IsNullOrEmpty(info.PremiseId))
                                {
                                    if ((!string.IsNullOrEmpty(info.ServiceId) && service == info.ServiceId) ||
                                        string.IsNullOrEmpty(info.ServiceId))
                                    {
                                        var filteredService = premise.Service.Where(s => s.Id == service).ToList();
                                        if (filteredService.Any())
                                        {
                                            var filterPremise = new BillPremise
                                            {
                                                Id = premise.Id,
                                                Addr1 = premise.Addr1,
                                                Addr2 = premise.Addr2,
                                                City = premise.City,
                                                State = premise.State,
                                                Zip = premise.Zip,
                                                Service = filteredService
                                            };

                                            filterPremises.Add(filterPremise);
                                        }
                                    }
                                }

                            }

                            if (filterPremises.Any())
                            {
                                var filterBill = new Bill
                                {
                                    AdditionalBillCost = bill.AdditionalBillCost,
                                    BillDate = bill.BillDate,
                                    BillDueDate = bill.BillDueDate,
                                    BillFrequency = bill.BillFrequency,
                                    TotalAmount = bill.TotalAmount,
                                    Premises = filterPremises
                                };

                                filterBills.Add(filterBill);
                            }
                        }
                        //var serviceBills = account.Bills.Where(b => b.Premises.Any(p => p.Service.Find(s => s.Id == service))).ToList();

                        if (filterBills.Any())
                        {
                            var filterAccount = new BillAccount
                            {
                                Id = id,
                                Bills = filterBills
                            };
                            filterAccounts.Add(filterAccount);
                        }

                    }
                }
            }
            

            return filterAccounts;
        }

        /// <summary>
        /// Setup the transform to convert ami data into xml
        /// </summary>
        /// <param name="info"></param>
        /// <param name="interval"></param>
        /// <param name="intervalConsumptionDataList"></param>
        /// <param name="resourceType"></param>
        /// <param name="format"></param>
        /// <param name="reportFileName"></param>
        /// <param name="convertUom"></param>
        /// <param name="uom"></param>
        /// <param name="premiseAddress"></param>
        /// <returns></returns>
        private GreenButtonXml GenerateAmiXml(GreenButtonInfo info, string interval, List<IntervalConsumptionData> intervalConsumptionDataList,
                                   Enums.CommodityType resourceType,
                                   ReportFormat format,
                                   string reportFileName,
                                   bool convertUom,
                                   string uom, 
                                   string premiseAddress = "")
        {
            const int kindElectric = 0;
            const int kindGas = 1;

            var powerOfTen = 0;
            int uomId = 0;
            //string result;
            //GreenButtonFile greenButtonBillResult;

            //_activityMonitor.AddActivityDetail(Activity.TransformationStart);

            int fuelId = Common.GetFuelId(resourceType);

            //var fileId = Guid.NewGuid().ToString();
            //var filename = fileId + "." + format.ToString().ToLower();

            var filename = reportFileName + "." + format.ToString().ToLower();

            TimeSpan offset;
            var startDate = GetDateTimeInClientTimeZone(out offset);

            var idGenerator = new IdGenerator(info.ClientId, info.AccountId, info.MeterId);
            var xmlTransformer = new XmlTransformer(filename, _config.XmlTemplates);

            if (uom != string.Empty)
                uomId = ConvertUOMNameToUomId(uom);

            //get from config
            xmlTransformer.ProcessorInfoFileName = _config.ProcessorInfoFileName;
            xmlTransformer.GreenButtonDataType = "amr";

            xmlTransformer.SharedObject.Add("Title", _config.ReportTitle);
            xmlTransformer.SharedObject.Add("SubTitle", _config.UsagePointTitle);
            xmlTransformer.SharedObject.Add("UsageTitle", _config.IntervalBlockTitle);
            xmlTransformer.SharedObject.Add("SummaryTitle", _config.UsageSummaryTitle);
            xmlTransformer.SharedObject.Add("AxisLabelsShifting", _config.AxisLabelsShifting);
            xmlTransformer.SharedObject.Add("EnableLinks", _config.EnableLinks);

            xmlTransformer.SharedObject.Add("Fuel", fuelId);
            xmlTransformer.SharedObject.Add("MeterId", info.MeterId);
            xmlTransformer.SharedObject.Add("AccountId", info.AccountId);
            xmlTransformer.SharedObject.Add("ReferrerId", info.ClientId);
            //if (_config.TZ_Offset_Value != "0")
            //    xmlTransformer.SharedObject.Add("StartDateInt", Common.ConvertDateTime(startDate, _config.TZ_Offset_Value));
            //else
            xmlTransformer.SharedObject.Add("StartDateInt", Common.ConvertDateTime(startDate));

            xmlTransformer.SharedObject.Add("StartDate",
                startDate.ToString("yyyy-MM-ddTHH:mm:ss") +
                Common.GetTimeZoneOffsetString(offset));
            xmlTransformer.SharedObject.Add("TimeAttribute", Common.GetTimeAttribute(interval));
            xmlTransformer.SharedObject.Add("PremiseAddress", premiseAddress);
            xmlTransformer.SharedObject.Add("FlowDirection", "1");
            xmlTransformer.SharedObject.Add("IDGenerator", idGenerator);
            var amrInterval = (ResolutionType)Enum.Parse(typeof(ResolutionType), interval);
               
            xmlTransformer.SharedObject.Add("IntervalLength", Common.GetResolutionDuration(amrInterval));

            if (resourceType == Enums.CommodityType.Electric)
            {
                if (uom == string.Empty)
                    uomId = UomidWattHours;
                xmlTransformer.SharedObject.Add("UOM", uomId);
                xmlTransformer.SharedObject.Add("Kind", kindElectric);

                // Multiplier is now used to convert the reading usage, if no conversion, multiplier will be 1
                if (convertUom)
                {
                    if (Convert.ToInt32(xmlTransformer.SharedObject["UOM"]) == UomidWattHours)
                    {
                        // convert kwh to watthours
                        xmlTransformer.SharedObject.Add("Multiplier", Convert.ToDouble(info.Multiplier) * 1000);
                    }
                    else
                    {
                        xmlTransformer.SharedObject.Add("Multiplier", (Convert.ToDouble(info.Multiplier) * 1000));
                        powerOfTen = -3;
                    }
                }
                else
                {
                    if (Convert.ToInt32(xmlTransformer.SharedObject["UOM"]) == UomidWattHours)
                    {
                        // convert kwh to watthours
                        xmlTransformer.SharedObject.Add("Multiplier", 1000);
                    }
                    else
                    {
                        xmlTransformer.SharedObject.Add("Multiplier", (Convert.ToDouble(info.Multiplier)));
                    }
                }
                xmlTransformer.SharedObject.Add("PowerOfTen", powerOfTen);

            }
            else if (resourceType == Enums.CommodityType.Gas)
            {
                if (uom == string.Empty)
                    uomId = UomidCubicFeet;
                xmlTransformer.SharedObject.Add("UOM", uomId);
                xmlTransformer.SharedObject.Add("Kind", kindGas);
                // Multiplier is now used to convert the reading usage, if no conversion, multiplier will be 1
                if (convertUom)
                {
                    if (Convert.ToInt32(xmlTransformer.SharedObject["UOM"]) == UomidCubicFeet)
                    {
                        // convert CCF to cubicfeet
                        xmlTransformer.SharedObject.Add("Multiplier", (Convert.ToDouble(info.Multiplier) * 100));
                    }
                    else if (Convert.ToInt32(xmlTransformer.SharedObject["UOM"]) == UomidTherm)
                    {
                        // convert therm to 1000 therm, then set powerOfTen -3
                        xmlTransformer.SharedObject.Add("Multiplier", (Convert.ToDouble(info.Multiplier) * 1000));
                        powerOfTen = -3;
                    }
                    else
                    {
                        xmlTransformer.SharedObject.Add("Multiplier", (Convert.ToDouble(info.Multiplier) * 1000));
                        powerOfTen = -3;
                    }
                }
                else
                {
                    if (Convert.ToInt32(xmlTransformer.SharedObject["UOM"]) == UomidCubicFeet)
                    {
                        // convert CCF to cubicfeet
                        xmlTransformer.SharedObject.Add("Multiplier", 100);
                    }
                    else if (Convert.ToInt32(xmlTransformer.SharedObject["UOM"]) == UomidTherm)
                    {
                        // convert therm to 1000 therm, then set powerOfTen -3
                        xmlTransformer.SharedObject.Add("Multiplier", 1000);
                        powerOfTen = -3;
                    }
                    else
                    {
                        xmlTransformer.SharedObject.Add("Multiplier", (Convert.ToDouble(info.Multiplier)));
                    }
                }

                xmlTransformer.SharedObject.Add("PowerOfTen", powerOfTen);
            }

            var element = new XmlElement("feed");

            element.Attributes.Add(new XmlAttribute("xmlns", "http://www.w3.org/2005/Atom"));
            element.Attributes.Add(new XmlAttribute("xsi:schemaLocation", "http://naesb.org/espi espi.xsd"));
            element.Attributes.Add(new XmlAttribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance"));

            xmlTransformer.XmlElements.Add(element);

            xmlTransformer.XmlParts.Add(new ReportHeader());
            xmlTransformer.XmlParts.Add(new ReportContent(intervalConsumptionDataList));
            xmlTransformer.XmlParts.Add(new ReportSummary());

            if (_config.EnableLinks == false)
                xmlTransformer.ExcludeElements.Add("link");

            // CR 47718 april 2014 - the following elemnts are only for bill xml green button download
            xmlTransformer.ExcludeElements.Add("currency");
            xmlTransformer.ExcludeElements.Add("ServiceDeliveryPoint");
            xmlTransformer.ExcludeElements.Add("tariffProfile");

            //CR 55405
            xmlTransformer.SharedObject.Add("DST_END_RULE", "B40E2000");
            xmlTransformer.SharedObject.Add("DST_OFFSET_VALUE", "3600");
            xmlTransformer.SharedObject.Add("DST_START_RULE", "360E2000");
            //Need to get the data from Authoring DB.
            xmlTransformer.SharedObject.Add("TZ_OFFSET_VALUE", _config.TzOffsetValue);

            var xml = xmlTransformer.GenerateXmlInMemory();

            var greenButtonXmlInfo = new GreenButtonXml
            {
                FileName = filename,
                Xml = xml
            };


            
            //greenButtonBillResult = new GreenButtonFile
            //{
            //    FileName = reportFileName + "." + format.ToString().ToLower(),
            //    Xml = greenButtonXmlInfo.Xml
            //};
            return greenButtonXmlInfo;
        }

        private GreenButtonFile GenerateBillXml(GreenButtonInfo info,
                           List<BillAccount> simpleBillsList,
                           ReportFormat format,
                           string reportFileName,
                           OutputFormatType outputFormat,
                           string premiseAddress = "")
        {


            string result = string.Empty;

            //_activityMonitor.AddActivityDetail(Activity.TransformationStart);

            var filenamePrefix = reportFileName;

            List<GreenButtonXml> greenButtonXmlList = new List<GreenButtonXml>();
            //List<string> zipFileList = new List<string>();
            //List<MemoryStream> xmlMemoryList = new List<MemoryStream>();
            var index = 0;
            foreach (var simpleBills in simpleBillsList)
            {
                var fileId = Guid.NewGuid().ToString();
                string filename;
                if (index == 0)
                {
                    filename = fileId + "." + format.ToString().ToLower();
                    result = fileId;
                    reportFileName = filenamePrefix + "_" + fileId;
                }
                else
                {
                    filename = filenamePrefix + "_" + fileId + "." + format.ToString().ToLower();
                }

                TimeSpan offset;
                var startDate = GetDateTimeInClientTimeZone(out offset);

                // instead of meterid, use serviceID
                var generatorMeterId = info.MeterId;
                var generatorAccountId = info.AccountId;
                if (simpleBills.Bills.Count > 0)
                {
                    generatorAccountId = simpleBills.Id; // account id
                    var serviceId = string.Empty;
                    if (simpleBills.Bills[0].Premises.Count > 0)
                    {
                        if (simpleBills.Bills[0].Premises[0].Service.Count > 0)
                        {
                            serviceId = simpleBills.Bills[0].Premises[0].Service[0].Id;
                            //if (serviceId == string.Empty)
                            //{
                            //    if (simpleBills.Bills[0].Premises[0].Service[0].Meters.Count > 0)
                            //    {
                            //        serviceId = generatorMeterID = simpleBills[0].SimplePremises[0].SimpleServices[0].Meters[0].MeterId;
                            //    }
                            //}
                        }
                        // Bug 56832 , Update address for each Meter
                        premiseAddress = GetPremiseAddress(simpleBills.Bills[0].Premises[0]);
                    }
                    if (serviceId != string.Empty)
                        generatorMeterId = serviceId;
                }
                var fuelType = Enums.CommodityType.Electric;
                var uom = string.Empty;
                var rateclass = string.Empty;
                int uomId = 0;
                var powerOfTen = 0;
                if (simpleBills.Bills.Count > 0)
                {
                    if (simpleBills.Bills[0].Premises.Count > 0)
                    {
                        if (simpleBills.Bills[0].Premises[0].Service.Count > 0)
                        {
                            fuelType = GetResourceCommodityType(simpleBills.Bills[0].Premises[0].Service[0].CommodityKey);
                            uom = simpleBills.Bills[0].Premises[0].Service[0].UOMKey;
                            rateclass = simpleBills.Bills[0].Premises[0].Service[0].RateClass;
                            //if (simpleBills.Bills[0].Premises[0].Service[0].Meters.Count > 0)
                            //{
                            //    rateclass = simpleBills[0].SimplePremises[0].SimpleServices[0].Meters.GetRateClassWithLastestStartDate();
                            //}
                        }
                    }
                }

                if (index == 0)
                {
                    reportFileName = reportFileName.Replace(ReplaceTokenResourceKey, "_" + fuelType.ToString());
                }
                else
                {
                    filename = filename.Replace(ReplaceTokenResourceKey, "_" + fuelType.ToString());
                }
                var idGenerator = new IdGenerator(info.ClientId, generatorAccountId, generatorMeterId);
                var xmlTransformer = new XmlTransformer(filename, _config.XmlTemplates);

                //get from config
                xmlTransformer.ProcessorInfoFileName = _config.ProcessorInfoFileName;
                xmlTransformer.SharedObject.Add("Title", _config.ReportTitle);
                xmlTransformer.SharedObject.Add("SubTitle", _config.UsagePointTitle);
                xmlTransformer.SharedObject.Add("Currency", GetCurrencyId(_config.Country));
                xmlTransformer.SharedObject.Add("EnableLinks", _config.EnableLinks);
                xmlTransformer.GreenButtonDataType = "bill";

                //if (_config.TZ_Offset_Value != "0")
                //    xmlTransformer.SharedObject.Add("StartDateInt", Common.ConvertDateTime(startDate, _config.TZ_Offset_Value));
                //else
                xmlTransformer.SharedObject.Add("StartDateInt", Common.ConvertDateTime(startDate));

                xmlTransformer.SharedObject.Add("StartDate",
                    startDate.ToString("yyyy-MM-ddTHH:mm:ss") +
                    Common.GetTimeZoneOffsetString(offset));
                xmlTransformer.SharedObject.Add("PremiseAddress", premiseAddress);
                xmlTransformer.SharedObject.Add("FlowDirection", "0");
                xmlTransformer.SharedObject.Add("TimeAttribute", "32");
                xmlTransformer.SharedObject.Add("IntervalLength", "2592000");
                xmlTransformer.SharedObject.Add("IDGenerator", idGenerator);

                //CR 55405
                xmlTransformer.SharedObject.Add("DST_END_RULE", "B40E2000");
                xmlTransformer.SharedObject.Add("DST_OFFSET_VALUE", "3600");
                xmlTransformer.SharedObject.Add("DST_START_RULE", "360E2000");
                //Need to get the data from Authoring DB.
                xmlTransformer.SharedObject.Add("TZ_OFFSET_VALUE", _config.TzOffsetValue);

                int fuelId = Common.GetFuelId(fuelType);
                xmlTransformer.SharedObject.Add("Fuel", fuelId);
                xmlTransformer.SharedObject.Add("RateClass", rateclass);

                bool useDefaultConversionFactor = false;
                info.Multiplier = "1";
                switch (fuelType)
                {
                    case Enums.CommodityType.Gas:
                        if (_config.ConvertGasUom)
                        {
                            uom = string.IsNullOrEmpty(_config.GasDisplayUom) ? uom : _config.GasDisplayUom;
                            useDefaultConversionFactor = _config.UseDefaultGasConversionFactor;
                            info.Multiplier = _config.DefaultGasConversionFactor.ToString(CultureInfo.InvariantCulture);
                        }
                        break;
                    case Enums.CommodityType.Water:
                        if (_config.ConvertWaterUom)
                        {
                            uom = string.IsNullOrEmpty(_config.WaterDisplayUom) ? uom : _config.WaterDisplayUom;
                            useDefaultConversionFactor = _config.UseDefaultWaterConversionFactor;
                            info.Multiplier = _config.DefaultWaterConversionFactor.ToString(CultureInfo.InvariantCulture);
                        }
                        break;
                    default:
                        uom = string.IsNullOrEmpty(uom) ?  _config.ElectricDisplayUom : uom;
                        break;
                }

                xmlTransformer.SharedObject.Add("UseDefaultConversionFactor", useDefaultConversionFactor.ToString());

                if (uom != string.Empty)
                    uomId = ConvertUOMNameToUomId(uom);

                if (fuelType == Enums.CommodityType.Electric)
                {

                    if (uom == string.Empty)
                        uomId = UomidWattHours;
                    xmlTransformer.SharedObject.Add("UOM", uomId);
                    xmlTransformer.SharedObject.Add("Kind", KindElectric);

                    if (Convert.ToInt32(xmlTransformer.SharedObject["UOM"]) == UomidWattHours)
                    {
                        // convert kwh to watthours
                        xmlTransformer.SharedObject.Add("Multiplier", 1000 * (Convert.ToDouble(info.Multiplier)));
                    }
                    else
                    {
                        xmlTransformer.SharedObject.Add("Multiplier", (Convert.ToDouble(info.Multiplier)));
                    }

                    xmlTransformer.SharedObject.Add("PowerOfTen", powerOfTen);

                }
                else if (fuelType == Enums.CommodityType.Gas)
                {
                    if (uom == string.Empty)
                        uomId = UomidCubicFeet;
                    xmlTransformer.SharedObject.Add("UOM", uomId);
                    xmlTransformer.SharedObject.Add("Kind", KindGas);

                    if (Convert.ToInt32(xmlTransformer.SharedObject["UOM"]) == UomidCubicFeet)
                    {
                        // convert CCF to CF
                        xmlTransformer.SharedObject.Add("Multiplier", 100 * (Convert.ToDouble(info.Multiplier)));
                    }
                    else if (Convert.ToInt32(xmlTransformer.SharedObject["UOM"]) == UomidTherm)
                    {
                        xmlTransformer.SharedObject.Add("Multiplier", 1000 * (Convert.ToDouble(info.Multiplier)));
                        powerOfTen = -3;
                    }
                    else
                    {
                        xmlTransformer.SharedObject.Add("Multiplier", (Convert.ToDouble(info.Multiplier)));
                    }

                    xmlTransformer.SharedObject.Add("PowerOfTen", powerOfTen);
                }
                else if (fuelType == Enums.CommodityType.Water)
                {
                    if (uom == string.Empty)
                        uomId = UomidCubicFeet;
                    xmlTransformer.SharedObject.Add("UOM", uomId);
                    xmlTransformer.SharedObject.Add("Kind", KindWater);

                    if (uomId == UomidCubicMeter)
                    {
                        xmlTransformer.SharedObject.Add("Multiplier", 1000 * (Convert.ToDouble(info.Multiplier)));
                        powerOfTen = -3;
                    }
                    else
                    {
                        if (uom.ToUpper() == "GAL")
                        {
                            xmlTransformer.SharedObject.Add("Multiplier", 1000 * (Convert.ToDouble(info.Multiplier)));
                            powerOfTen = -3;
                        }
                        else if (uom.ToUpper() == "KGAL")
                        {
                            xmlTransformer.SharedObject.Add("Multiplier", 1000 * (Convert.ToDouble(info.Multiplier)));
                        }
                        else if (uom.ToUpper() == "CGAL")
                        {
                            xmlTransformer.SharedObject.Add("Multiplier", 100 * (Convert.ToDouble(info.Multiplier)));
                        }
                        else
                        {
                            xmlTransformer.SharedObject.Add("Multiplier", (Convert.ToDouble(info.Multiplier)));
                        }
                    }

                    xmlTransformer.SharedObject.Add("PowerOfTen", powerOfTen);
                }

                var element = new XmlElement("feed");

                element.Attributes.Add(new XmlAttribute("xmlns", "http://www.w3.org/2005/Atom"));
                element.Attributes.Add(new XmlAttribute("xsi:schemaLocation", "http://naesb.org/espi espi.xsd"));
                element.Attributes.Add(new XmlAttribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance"));

                xmlTransformer.XmlElements.Add(element);

                xmlTransformer.XmlParts.Add(new ReportHeader());
                xmlTransformer.XmlParts.Add(new ReportContent(simpleBills.Bills));
                //******************************* Below Code added to fix BUG - 59564 & 59808 **************************************
                xmlTransformer.XmlParts.Add(new ReportSummary());
                xmlTransformer.SharedObject.Add("SummaryTitle", _config.UsageSummaryTitle);
                xmlTransformer.SharedObject.Add("TotalUsage", 0);
                // ****** Above added code for Report Summary is just to pass the Green Button Test, Values are not correct *******

                if (_config.EnableLinks == false)
                    xmlTransformer.ExcludeElements.Add("link");

                var xml = xmlTransformer.GenerateXmlInMemory();

                var greenButtonXmlInfo = new GreenButtonXml
                {
                    FileName = filename,
                    Xml = xml
                };

                greenButtonXmlList.Add(greenButtonXmlInfo);
                //zipFileList.Add(filename);
                index++;

            }

            GreenButtonFile greenButtonBillResult;
            
            if (outputFormat == OutputFormatType.Zip)
            {

                if (_config.ProcessorInfoFileName.Length > 0)
                {
                    if (_config.ProcessInfoFile.Length > 0)
                    {
                        var xsltInfo = new GreenButtonXml
                        {
                            FileName = _config.ProcessorInfoFileName,
                            Xml = _config.ProcessInfoFile
                        }
                        ;
                        greenButtonXmlList.Add(xsltInfo);
                    }
                }


                var zippedStream = ZipUtility.CompressFilesInMemory(greenButtonXmlList,
                    reportFileName + "." + format.ToString().ToLower());
                result = result + ".zip";
                greenButtonBillResult = new GreenButtonFile
                {
                    FileName = result,
                    File = zippedStream
                };
            }
            else
            {
                greenButtonBillResult = new GreenButtonFile
                {
                    FileName = result,
                    Xml = greenButtonXmlList[0].Xml
                };
            }

            return greenButtonBillResult;
        }

        
       
        // Generate the file name for ami xml
        private string GenerateExportFileName(Enums.CommodityType commodityType, string usageInterval, DateTime startDate, DateTime endDate)
        {
            var fileName = _config.ReportPreName + "_" + commodityType.ToString() + "_" + Common.GetFileNameInterval(usageInterval) + "_" + startDate.ToString("MM-dd-yyyy") + "_" + endDate.ToString("MM-dd-yyyy") + "_" + DateTime.Now.ToString("yyyyMMddHHmmssfff");

            return fileName;
        }

        // Generate the file name for ami zip
        private string GenerateExportZipFileName(DateTime startDate, DateTime endDate)
        {
            var fileName = _config.ReportPreName + "_" +  startDate.ToString("MM-dd-yyyy") + "_" + endDate.ToString("MM-dd-yyyy") + "_" + DateTime.Now.ToString("yyyyMMddHHmmssfff");

            return fileName;
        }

        private string GenerateBillXmlExportFileName(DateTime startDate, DateTime endDate)
        {
            var fileName = _config.ReportPreName + ReplaceTokenResourceKey + "_Billing_" + startDate.ToString("MM-dd-yyyy") + "_" + endDate.ToString("MM-dd-yyyy") + "_" + DateTime.Now.ToString("yyyyMMddHHmmssfff");

            return fileName;
        }

        protected string GetPremiseAddress(BillPremise entAddress)
        {
            var address = string.Empty;
            if (!string.IsNullOrEmpty(entAddress.Addr1))
                address = entAddress.Addr1;

            if (! string.IsNullOrEmpty(entAddress.Addr2))
            {
                if(!string.IsNullOrEmpty(address))
                    address = address + ", " + entAddress.Addr2;
                else
                    address = entAddress.Addr2;
            }

            if (!string.IsNullOrEmpty(entAddress.City))
            {
                if (!string.IsNullOrEmpty(address))
                    address = address + ", " + entAddress.City;
                else
                    address = entAddress.City;
            }
            if (!string.IsNullOrEmpty(entAddress.State) || !string.IsNullOrEmpty(entAddress.Zip))
            {
                if (!string.IsNullOrEmpty(address))
                    address = address + ", " + entAddress.State + " " + entAddress.Zip;
                else
                    address = entAddress.State + " " + entAddress.Zip;
            }
            
            return address;
        }

        private Enums.CommodityType GetResourceCommodityType(string commodityKey)
        {
            Enums.CommodityType commodityType;
            switch (commodityKey.ToLower())
            {
                case "electric":
                case "electricity":
                    commodityType = Enums.CommodityType.Electric;
                    break;
                case "gas":
                    commodityType = Enums.CommodityType.Gas;
                    break;
                case "oil":
                    commodityType = Enums.CommodityType.Oil;
                    break;
                case "propane":
                    commodityType = Enums.CommodityType.Propane;
                    break;
                case "water":
                    commodityType = Enums.CommodityType.Water;
                    break;
                case "sewer":
                    commodityType = Enums.CommodityType.Sewer;
                    break;
                default:
                    commodityType = Enums.CommodityType.Undefined;
                    break;
            }

            return commodityType;
        }

        private string GetCurrencyId(string country)
        {

            string result;

            switch (country.ToUpper())
            {
                case "USA":
                    result = CurrencyIdUsd;
                    break;
                case "CANADA":
                    result = CurrencyIdCad;
                    break;
                default:
                    result = CurrencyIdNotApplicable;
                    break;
            }

            return result;
        }

        /// <summary>
        /// Convert uom name to uom identifier.
        /// </summary>
        /// <param name="uom"></param>
        /// <returns></returns>
        private int ConvertUOMNameToUomId(string uom)
        {

            int result;

            switch (uom.ToUpper())
            {
                case "CCF":
                    result = UomidCubicFeet;
                    break;
                case "THERMS":
                case "THERM":
                    result = UomidTherm;
                    break;
                case "KWH":
                    result = UomidWattHours;
                    break;
                case "CUBICMETER":
                case "CUBIC METER":
                    result = UomidCubicMeter;
                    break;
                case "GAL":
                case "KGAL":
                case "CGAL":
                    result = UomidGal;
                    break;
                default:
                    result = 0;
                    break;
            }

            return result;
        }

        private DateTime GetDateTimeInClientTimeZone(out TimeSpan offset)
        {
            var dateTime = DateTime.Now;

            var offsetTzInfo = TimeZoneInfo.Local;

            if (!String.IsNullOrEmpty(OlysonTimeZoneKey))
            {
                offsetTzInfo = Common.ConvertOlsonTimeZoneToTimeZoneInfo(OlysonTimeZoneKey);

                dateTime = TimeZoneInfo.ConvertTime(dateTime, TimeZoneInfo.Local, offsetTzInfo);
            }

            offset = offsetTzInfo.GetUtcOffset(dateTime);

            return dateTime;
        }

        // check if fuel type is supported for ami green  button; only support gas and electric
        private bool CheckIfFuelSupported(Enums.CommodityType commodityType)
        {
            var fuelSupported = true;
            if (!(commodityType == Enums.CommodityType.Electric || commodityType == Enums.CommodityType.Gas))
                fuelSupported = false;
            //throw new Exception(commodityType.ToString() + " is not supported.");
            return fuelSupported;
        }
    }
}
