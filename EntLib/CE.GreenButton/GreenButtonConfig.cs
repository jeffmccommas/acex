﻿//using Nexus.ServiceClient.MeterData;

using System;

namespace CE.GreenButton
{
    [Serializable()]
    public class GreenButtonConfig
    {
        private bool _enableLinks;
        private bool _enableChunking;

        private bool _enableThrottling;
        private bool _enablePartialData;
        private bool _axisLabelsShifting;

        private string _reportTitle = string.Empty;
        private string _usagePointTitle = string.Empty;
        private string _intervalBlockTitle = string.Empty;
        private string _usageSummaryTitle = string.Empty;

        private string _xmlTemplates = string.Empty;
        private string _reportPreName = string.Empty;
        //private string _sharedDrivePath = string.Empty;

        private string _amrTimeOutText = string.Empty;
        private string _amrDownloadErrorText = string.Empty;

        private int _maxLoadingTime = 300;
        private int _amiDownloadWindow = 60;
        //private int _sharedDataValidationDuration = 1;
        private int _maxRequestPeriod = 60;

        private DateTime _maxDate = DateTime.Now;
        private DateTime _minDate = DateTime.Now.AddDays(-59);
        private string _processorInfoFilePath = string.Empty;
        private string _processorInfoFileName = string.Empty;

        //private Enums.dataScenario _eDataScenario = Enums.dataScenario.Working;

        public string ReportTitle
        {
            get { return _reportTitle; }
            set { _reportTitle = value; }
        }

        public string UsagePointTitle
        {
            get { return _usagePointTitle; }
            set { _usagePointTitle = value; }
        }

        public string IntervalBlockTitle
        {
            get { return _intervalBlockTitle; }
            set { _intervalBlockTitle = value; }
        }

        public string UsageSummaryTitle
        {
            get { return _usageSummaryTitle; }
            set { _usageSummaryTitle = value; }
        }

        public string XmlTemplates
        {
            get { return _xmlTemplates; }
            set { _xmlTemplates = value; }
        }

        public string ReportPreName
        {
            get { return _reportPreName; }
            set { _reportPreName = value; }
        }

        //public string SharedDrivePath
        //{
        //    get { return _sharedDrivePath; }
        //    set { _sharedDrivePath = value; }
        //}

        public string AmrTimeOutText
        {
            get { return _amrTimeOutText; }
            set { _amrTimeOutText = value; }
        }

        public string AmrDownloadErrorText
        {
            get { return _amrDownloadErrorText; }
            set { _amrDownloadErrorText = value; }
        }

        public int MaxLoadingTime
        {
            get { return _maxLoadingTime; }
            set { _maxLoadingTime = value; }
        }

        public int AmiDownloadWindow
        {
            get { return _amiDownloadWindow; }
            set { _amiDownloadWindow = value; }
        }

        public bool EnableLinks
        {
            get { return _enableLinks; }
            set { _enableLinks = value; }
        }

        public bool EnableChunking
        {
            get { return _enableChunking; }
            set { _enableChunking = value; }
        }

        public bool EnableThrottling
        {
            get { return _enableThrottling; }
            set { _enableThrottling = value; }
        }

        public bool EnablePartialData
        {
            get { return _enablePartialData; }
            set { _enablePartialData = value; }
        }

        public bool AxisLabelsShifting
        {
            get { return _axisLabelsShifting; }
            set { _axisLabelsShifting = value; }
        }

        //public int SharedDataValidationDuration
        //{
        //    get { return _sharedDataValidationDuration; }
        //    set { _sharedDataValidationDuration = value; }
        //}

        public int MaxRequestPeriod
        {
            get { return _maxRequestPeriod; }
            set { _maxRequestPeriod = value; }
        }

        public DateTime MaxDate
        {
            get { return _maxDate; }
            set { _maxDate = value; }
        }

        public DateTime MinDate
        {
            get { return _minDate; }
            set { _minDate = value; }
        }

        // TBD - currenlty no data scenario in aclara one ami
        //public Enums.dataScenario DataScenario
        //{
        //    get { return _eDataScenario; }
        //    set { _eDataScenario = value; }
        //}


        public string ProcessorInfoFileName
        {
            get { return _processorInfoFileName; }
            set { _processorInfoFileName = value; }
        }

        public string ProcessorInfoFilePath
        {
            get { return _processorInfoFilePath; }
            set { _processorInfoFilePath = value; }
        }

        //// CR 27420 July 2012 - csv export config
        //public string NameLabel { get; set; }
        //public string AddressLabel { get; set; }
        //public string AccountNumberLabel { get; set; }
        //public string DisclaimerLabel { get; set; }
        //public string DisclaimerDescriptionLabel { get; set; }
        //public string TitleLabel { get; set; }
        //public string MeterNumberLabel { get; set; }
        //public string StartDateLabel { get; set; }
        //public string TotalDurationLabel { get; set; }
        //public string UnitOfMeasureLabel { get; set; }
        //public string IntervalTypeLabel { get; set; }
        //public string FlowDirectionLabel { get; set; }
        //public string DateLabel { get; set; }
        //public string TimeLabel { get; set; }
        //public string DurationLabel { get; set; }
        //public string ReadingLabel { get; set; }
        //public string EditCodeLabel { get; set; }
        //public string CommodityLabel { get; set; }
        //public string MultiplierLabel { get; set; }
        //public string MinuteLabel { get; set; }
        //public string DateFormat { get; set; }
        //public string TimeFormat { get; set; }
        //public string ElectricLabel { get; set; }
        //public string GasLabel { get; set; }
        //public string WaterLabel { get; set; }
        public string ElectricDisplayUom { get; set; }
        public string GasDisplayUom { get; set; }
        public string WaterDisplayUom { get; set; }
        ////CR 53322 , TOU Label fro CSV Export
        //public string TouLabel { get; set; }

        // 13.03 bug 35557 - EndDate and Total Usage
        public string EndDateLabel { get; set; }
        public string TotalUsageLabel { get; set; }

        // cr 47718 april 2014 - country
        public string Country { get; set; }
        public string BillXmlTimeoutError { get; set; }
        //public string UnathenticateUserError { get; set; }
        //public string NewServiceError { get; set; }
        public bool ConvertGasUom { get; set; }
        public bool UseDefaultGasConversionFactor { get; set; }
        public double DefaultGasConversionFactor { get; set; }
        public bool ConvertWaterUom { get; set; }
        public bool UseDefaultWaterConversionFactor { get; set; }
        public double DefaultWaterConversionFactor { get; set; }

        // CR 55405 May 2015 - Adding new Configuration for Local Time Parameter
        public string TzOffsetValue { get; set; }

        public string ProcessInfoFile { get; set; }
        public int BillMonths { get; set; }
    }

    public enum ReportFormat
    {
        Xml = 1,
        Csv = 2
    }

    public enum ReportDataType
    {
        Amr = 1,
        Bill = 2,
        EventData = 3
    }

    public enum RequestType
    {
        ManualDownload = 1,
        ManualUpload = 2
    }

    public enum OutputFormatType
    {
        Zip = 1, 
        Xml = 2
    }
}
