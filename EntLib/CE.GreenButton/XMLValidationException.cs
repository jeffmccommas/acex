﻿using System;
using System.Runtime.Serialization;

namespace CE.GreenButton
{
    [Serializable]
    public class XmlValidationException : Exception
    {
         #region "Public Constructors"

        public XmlValidationException()
        {
        }

        public XmlValidationException(string message)
            : base(message)
        {
        }

        public XmlValidationException(string message, Exception inner)
            : base(message, inner)
        {
        }

        #endregion

        #region "Protected Constructors"

        protected XmlValidationException(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
        }

        #endregion
    }
}
