﻿using CE.AO.Utilities;
using System;
using System.Collections.Generic;
using System.Globalization;
//using System.Linq;
using CE.Models.Insights;
using CE.Models.Insights.Types;

namespace CE.GreenButton
{
    public static class Common
    {

        public static string ConvertDateTime(string dateTime)
        {
            return ConvertDateTime(DateTime.Parse(dateTime));
        }

        /// <summary>
        /// This function is used to convert the Interval time from local time to UTC time
        /// </summary>
        /// <param name="dateTime">Local Date time</param>
        /// <param name="tzOffsetValue">time zone Offset Value</param>
        /// <returns></returns>
        public static string ConvertDateTime(DateTime dateTime, string tzOffsetValue)
        {
            string value;
            double tzValue = Convert.ToDouble(tzOffsetValue);

            value = Math.Truncate(dateTime.Subtract(DateTime.Parse("1/1/1970")).TotalSeconds + tzValue).ToString(CultureInfo.InvariantCulture);

            return value;
        }

        public static string ConvertDateTime(DateTime dateTime) 
        {
            string value;

            value = Math.Truncate(dateTime.Subtract(DateTime.Parse("1/1/1970")).TotalSeconds).ToString(CultureInfo.InvariantCulture);
            
            return value;
        }

        /// <summary>
        /// Get the duration for resolution in second
        /// </summary>
        /// <param name="resolution"></param>
        /// <returns></returns>
        public static string GetResolutionDuration(ResolutionType resolution) 
        {
            string duration = "";

            switch (resolution) 
            {
                case ResolutionType.five:
                    duration = (5 * 60).ToString();
                    break;

                case ResolutionType.fifteen:
                    duration = (15 * 60).ToString();
                    break;

                case ResolutionType.thirty:
                    duration = (30 * 60).ToString();
                    break;

                case ResolutionType.hour:
                    duration = (60 * 60).ToString();
                    break;

                case ResolutionType.sixhour:
                    duration = (6 * 60 * 60).ToString();
                    break;

                case ResolutionType.day:
                    duration = (24 * 60 * 60).ToString();
                    break;
            }

            return duration;
        }

        /// <summary>
        /// get resolution duration in mintues
        /// </summary>
        /// <param name="resolution"></param>
        /// <returns></returns>
        public static string GetResolutionDurationInMinutes(ResolutionType resolution)
        {
            string duration = "";

            switch (resolution)
            {
                case ResolutionType.five:
                    duration = 5.ToString();
                    break;
                case ResolutionType.fifteen:
                    duration = 15.ToString();
                    break;
                case ResolutionType.thirty:
                    duration = 30.ToString();
                    break;
                case ResolutionType.hour:
                    duration = 60.ToString();
                    break;
                case ResolutionType.twohour:
                    duration = (2 * 60).ToString();
                    break;
                case ResolutionType.fourhour:
                    duration = (4 * 60).ToString();
                    break;
                case ResolutionType.sixhour:
                    duration = (6 * 60).ToString();
                    break;
                case ResolutionType.twelvehour:
                    duration = (12 * 60).ToString();
                    break;
                case ResolutionType.bill:
                    duration = (24 * 60).ToString();
                    break;
            }

            return duration;
        }
                
        /// <summary>
        /// return the duration between 2 dates for bill
        /// Cr 47718 april 2014
        /// </summary>
        /// <param name="startDate"></param>
        /// <param name="endDate"></param>
        /// <returns></returns>
        public static string GetBillDuration(DateTime startDate, DateTime endDate)
        {
            string duration;

            TimeSpan span = endDate - startDate;

            duration = span.TotalSeconds.ToString(CultureInfo.InvariantCulture);

            return duration;
        }

        /// <summary>
        /// get the duration for days input
        /// cr 47718 april 2014
        /// </summary>
        /// <param name="days"></param>
        /// <returns></returns>
        public static string GetDaysDuration(int days)
        {
            string duration;
            
            duration = (days * 24 * 60 * 60).ToString();

            return duration;
        }

        public static int GetFuelId(Enums.CommodityType resource)
        {
            int fuelId = 0;

            if (resource == Enums.CommodityType.Electric)
                fuelId = 0;
            else if (resource == Enums.CommodityType.Gas)
                fuelId = 1;
            else if (resource == Enums.CommodityType.Water)
                fuelId = 10;

            return fuelId;
        }


        /// <summary>
        /// Get the resolution type name in the list
        /// use the one with the most consumption data
        /// </summary>
        /// <param name="intervalConsumptionDataList"></param>
        /// <returns></returns>
        public static string GetResolutionKey(List<IntervalConsumptionData> intervalConsumptionDataList)
        {
            int readingCount = 0;
            string resolutionKey = string.Empty;

            foreach (var consumptionDataList in intervalConsumptionDataList)
            {
                if (consumptionDataList != null && consumptionDataList.ConsumptionDataList.Count > 0)
                {
                    bool recordInterval = false;

                    if (resolutionKey == string.Empty)
                        recordInterval = true;
                    else if (consumptionDataList.ConsumptionDataList.Count > readingCount)
                        recordInterval = true;

                    if (recordInterval == true)
                    {
                        readingCount = consumptionDataList.ConsumptionDataList.Count;
                        resolutionKey = consumptionDataList.ResolutionKey;
                    }
                }
            }

            return resolutionKey;
        }
        
        /// <summary>
        /// Get time attribute for ami green button based on interval/resolution
        /// </summary>
        /// <param name="intervalName"></param>
        /// <returns></returns>
        public static int GetTimeAttribute(string intervalName) 
        {
            int value = 0;

            switch (intervalName) 
            {
                case "day":
                    value = 11;
                    break;

                case "fifteen":
                    value = 2;
                    break;

                case "five":
                    value = 0;
                    break;

                case "thirty":
                    value = 5;
                    break;

                case "hour":
                    value = 7;
                    break;

                case "sixhour":
                    value = 0;
                    break;
            }

            return value;
        }

        /// <summary>
        /// Get the file name based on the interval/resolution
        /// </summary>
        /// <param name="intervalName"></param>
        /// <returns></returns>
        public static string GetFileNameInterval(string intervalName) 
        {
            string value = "";

            switch (intervalName)
            {
                case "day":
                    value = "Daily";
                    break;

                case "fifteen":
                    value = "15_Minute";
                    break;

                case "five":
                    value = "5_Minute";
                    break;

                case "thirty":
                    value = "30_Minute";
                    break;

                case "hour":
                    value = "60_Minute";
                    break;

                case "sixhour":
                    value = "360_Minute";
                    break;
            }

            return value;
        }

        public static string GetTimeZoneOffsetString(TimeSpan offset)
        {
            string sOffset;

            sOffset = offset.CompareTo(TimeSpan.Parse("00:00")) < 0 ? "-" : "+";
            sOffset = sOffset + offset.ToString("hh");
            sOffset = sOffset + ":";
            sOffset = sOffset + offset.ToString("mm");

            return sOffset;
        }

        public static TimeZoneInfo ConvertOlsonTimeZoneToTimeZoneInfo(string olsonTimeZoneId)
        {
            var olsonWindowsTimes = new Dictionary<string, string>()
                                        {
                                            {"Africa/Cairo", "Egypt Standard Time"},
                                            {"Africa/Casablanca", "Morocco Standard Time"},
                                            {"Africa/Johannesburg", "South Africa Standard Time"},
                                            {"Africa/Lagos", "W. Central Africa Standard Time"},
                                            {"Africa/Nairobi", "E. Africa Standard Time"},
                                            {"Africa/Windhoek", "Namibia Standard Time"},
                                            {"America/Anchorage", "Alaskan Standard Time"},
                                            {"America/Asuncion", "Paraguay Standard Time"},
                                            {"America/Bogota", "SA Pacific Standard Time"},
                                            {"America/Buenos_Aires", "Argentina Standard Time"},
                                            {"America/Caracas", "Venezuela Standard Time"},
                                            {"America/Cayenne", "SA Eastern Standard Time"},
                                            {"America/Chicago", "Central Standard Time"},
                                            {"America/Chihuahua", "Mountain Standard Time (Mexico)"},
                                            {"America/Cuiaba", "Central Brazilian Standard Time"},
                                            {"America/Denver", "Mountain Standard Time"},
                                            {"America/Godthab", "Greenland Standard Time"},
                                            {"America/Guatemala", "Central America Standard Time"},
                                            {"America/Halifax", "Atlantic Standard Time"},
                                            {"America/Indianapolis", "US Eastern Standard Time"},
                                            {"America/La_Paz", "SA Western Standard Time"},
                                            {"America/Los_Angeles", "Pacific Standard Time"},
                                            {"America/Mexico_City", "Mexico Standard Time"},
                                            {"America/Montevideo", "Montevideo Standard Time"},
                                            {"America/New_York", "Eastern Standard Time"},
                                            {"America/Phoenix", "US Mountain Standard Time"},
                                            {"America/Regina", "Canada Central Standard Time"},
                                            {"America/Santa_Isabel", "Pacific Standard Time (Mexico)"},
                                            {"America/Santiago", "Pacific SA Standard Time"},
                                            {"America/Sao_Paulo", "E. South America Standard Time"},
                                            {"America/St_Johns", "Newfoundland Standard Time"},
                                            {"Asia/Almaty", "Central Asia Standard Time"},
                                            {"Asia/Amman", "Jordan Standard Time"},
                                            {"Asia/Baghdad", "Arabic Standard Time"},
                                            {"Asia/Baku", "Azerbaijan Standard Time"},
                                            {"Asia/Bangkok", "SE Asia Standard Time"},
                                            {"Asia/Beirut", "Middle East Standard Time"},
                                            {"Asia/Calcutta", "India Standard Time"},
                                            {"Asia/Colombo", "Sri Lanka Standard Time"},
                                            {"Asia/Damascus", "Syria Standard Time"},
                                            {"Asia/Dhaka", "Bangladesh Standard Time"},
                                            {"Asia/Dubai", "Arabian Standard Time"},
                                            {"Asia/Irkutsk", "North Asia East Standard Time"},
                                            {"Asia/Jerusalem", "Israel Standard Time"},
                                            {"Asia/Kabul", "Afghanistan Standard Time"},
                                            {"Asia/Kamchatka", "Kamchatka Standard Time"},
                                            {"Asia/Karachi", "Pakistan Standard Time"},
                                            {"Asia/Katmandu", "Nepal Standard Time"},
                                            {"Asia/Kolkata","India Standard Time"},
                                            {"Asia/Krasnoyarsk", "North Asia Standard Time"},
                                            {"Asia/Magadan", "Magadan Standard Time"},
                                            {"Asia/Novosibirsk", "N. Central Asia Standard Time"},
                                            {"Asia/Rangoon", "Myanmar Standard Time"},
                                            {"Asia/Riyadh", "Arab Standard Time"},
                                            {"Asia/Seoul", "Korea Standard Time"},
                                            {"Asia/Shanghai", "China Standard Time"},
                                            {"Asia/Singapore", "Singapore Standard Time"},
                                            {"Asia/Taipei", "Taipei Standard Time"},
                                            {"Asia/Tashkent", "West Asia Standard Time"},
                                            {"Asia/Tbilisi", "Georgian Standard Time"},
                                            {"Asia/Tehran", "Iran Standard Time"},
                                            {"Asia/Tokyo", "Tokyo Standard Time"},
                                            {"Asia/Ulaanbaatar", "Ulaanbaatar Standard Time"},
                                            {"Asia/Vladivostok", "Vladivostok Standard Time"},
                                            {"Asia/Yakutsk", "Yakutsk Standard Time"},
                                            {"Asia/Yekaterinburg", "Ekaterinburg Standard Time"},
                                            {"Asia/Yerevan", "Armenian Standard Time"},
                                            {"Atlantic/Azores", "Azores Standard Time"},
                                            {"Atlantic/Cape_Verde", "Cape Verde Standard Time"},
                                            {"Atlantic/Reykjavik", "Greenwich Standard Time"},
                                            {"Australia/Adelaide", "Cen. Australia Standard Time"},
                                            {"Australia/Brisbane", "E. Australia Standard Time"},
                                            {"Australia/Darwin", "AUS Central Standard Time"},
                                            {"Australia/Hobart", "Tasmania Standard Time"},
                                            {"Australia/Perth", "W. Australia Standard Time"},
                                            {"Australia/Sydney", "AUS Eastern Standard Time"},
                                            {"Etc/GMT", "UTC"},
                                            {"Etc/GMT+11", "UTC-11"},
                                            {"Etc/GMT+12", "Dateline Standard Time"},
                                            {"Etc/GMT+2", "UTC-02"},
                                            {"Etc/GMT-12", "UTC+12"},
                                            {"Europe/Berlin", "W. Europe Standard Time"},
                                            {"Europe/Budapest", "Central Europe Standard Time"},
                                            {"Europe/Istanbul", "GTB Standard Time"},
                                            {"Europe/Kiev", "FLE Standard Time"},
                                            {"Europe/London", "GMT Standard Time"},
                                            {"Europe/Minsk", "E. Europe Standard Time"},
                                            {"Europe/Moscow", "Russian Standard Time"},
                                            {"Europe/Paris", "Romance Standard Time"},
                                            {"Europe/Warsaw", "Central European Standard Time"},
                                            {"Indian/Mauritius", "Mauritius Standard Time"},
                                            {"Pacific/Apia", "Samoa Standard Time"},
                                            {"Pacific/Auckland", "New Zealand Standard Time"},
                                            {"Pacific/Fiji", "Fiji Standard Time"},
                                            {"Pacific/Guadalcanal", "Central Pacific Standard Time"},
                                            {"Pacific/Honolulu", "Hawaiian Standard Time"},
                                            {"Pacific/Port_Moresby", "West Pacific Standard Time"},
                                            {"Pacific/Tongatapu", "Tonga Standard Time"}
                                        };

            string windowsTimeZoneId;
            TimeZoneInfo windowsTimeZone;
            
            if (olsonWindowsTimes.TryGetValue(olsonTimeZoneId, out windowsTimeZoneId))
            {
                windowsTimeZone = TimeZoneInfo.FindSystemTimeZoneById(windowsTimeZoneId);
            }
            else
            {
                throw new Exception("No mapping exists for olson timezone: " + olsonTimeZoneId);
            }

            return windowsTimeZone;
        }
    }

    
}
