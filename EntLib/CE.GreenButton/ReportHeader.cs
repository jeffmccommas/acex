﻿
namespace CE.GreenButton
{
    public class ReportHeader : ReportBase
    {
        private const string TemplateName = "Header";

        public override string ReadTemplate()
        {
            return TemplateName;
        }

        public override string ReplaceToken(string token)
        {
            string value;

            switch (token) 
            {
                case "%MeterId%":
                    value = SharedObject["MeterId"].ToString();
                    break;

                case "%Title%":
                    value = SharedObject["Title"].ToString();
                    break;

                case "%StartDate%":
                    value = SharedObject["StartDate"].ToString();
                    break;

                case "%SubTitle%":
                    value = SharedObject["SubTitle"].ToString();
                    break;

                case "%Behaviour%":
                    value = "4";
                    break;

                case "%Commodity%":
                    switch (FuelId)
                    {
                        case 1:
                            value = "7";
                            break;
                        case 10:
                            value = "9";
                            break;
                        default:
                            value = "1";
                            break;
                    }
                    break;

                case "%DataQualifier%":
                    value = "0";
                    break;

                case "%FlowDirection%":
                    value = SharedObject["FlowDirection"].ToString();
                    break;

                case "%Kind%":
                    value = SharedObject["Kind"].ToString();
                    break;

                case "%Phase%":
                    value = "0";
                    break;

                case "%Multiplier%":
                    value = SharedObject["PowerOfTen"].ToString();
                    break;

                case "%TimeAttribute%":
                    value = SharedObject["TimeAttribute"].ToString();
                    break;

                case "%UOM%":
                    value = SharedObject["UOM"].ToString();
                    break;

                case "%LINK_SELF%":
                    value = PreLink + "/UsagePoint/01";
                    break;

                case "%LINK_PARENT%":
                    value = PreLink + "/UsagePoint";
                    break;

                case "%LINK_RELATED%":
                    value = PreLink + "/UsagePoint/01/MeterReading";
                    break;

                case "%LINK_RELATED_SUMMARY%":
                    value = PreLink + "/UsagePoint/01/ElectricPowerUsageSummary";
                    break;

                case "%UsagePointId%":
                    value = IdGenerator.UsagePointId;
                    break;

                case "%ReadingTypeId%":
                    value = IdGenerator.ReadingTypeId;
                    break;

                case "%LocalTimeParameterTypeId%":
                    value = IdGenerator.LocalTimeParameterId;
                    break;

                case "%LINK_RELATED_LOCALTIMEPARAM%":
                    value = "/LocalTimeParameters/01";
                    break;

                case "%LocalTimeSubTitle%":
                    value = "Time period";
                    break;

                case "%DST_END_RULE%":
                    value = SharedObject["DST_END_RULE"].ToString();
                    break;
                case "%DST_OFFSET_VALUE%":
                    value = SharedObject["DST_OFFSET_VALUE"].ToString();
                    break;
                case "%DST_START_RULE%":
                    value = SharedObject["DST_START_RULE"].ToString();
                    break;
                case "%TZ_OFFSET_VALUE%":
                    value = SharedObject["TZ_OFFSET_VALUE"].ToString();
                    break;

                case "%PremiseAddress%":
                    value = SharedObject["PremiseAddress"].ToString();
                    break;
                case "%Currency%":
                    value = SharedObject["Currency"].ToString();
                    break;
                case "%RateClass%":
                    value = SharedObject["RateClass"].ToString();
                    break;
                case "%IntervalLength%":
                    value = SharedObject["IntervalLength"].ToString();
                    break;
                default:
                    value = base.ReplaceToken(token);
                    break;
            }

            return value;
        }
    }
}
