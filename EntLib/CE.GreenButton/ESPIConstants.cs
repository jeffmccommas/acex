﻿
namespace CE.GreenButton
{
    // ReSharper disable once InconsistentNaming
    public class EspiConstants
    {
        public const string IntervalReading = "IntervalReading";
        public const string TimePeriod = "timePeriod";
        public const string Duration = "duration";
        public const string Start = "start";
        public const string Interval = "interval";
        public const int ThirtyThreeDays = 2851200;
        public const int TwentyEightDays = 2419200;
        public const int TwentyDays = 1728000;
    }

    public class AtomConstants
    {
        // ReSharper disable once InconsistentNaming
        public const string link = "link";
        // ReSharper disable once InconsistentNaming
        public const string href = "href";
        // ReSharper disable once InconsistentNaming
        public const string rel = "rel";
        // ReSharper disable once InconsistentNaming
        public const string entry = "entry";
    }




}
