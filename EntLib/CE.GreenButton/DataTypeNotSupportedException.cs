﻿using System;
using System.Runtime.Serialization;

namespace CE.GreenButton
{
    [Serializable]
    public class DataTypeNotSupportedException : Exception
    {
        #region "Public Constructors"

        public DataTypeNotSupportedException()
        {
        }

        public DataTypeNotSupportedException(string message)
            : base(message)
        {
        }

        public DataTypeNotSupportedException(string message, Exception inner)
            : base(message, inner)
        {
        }

        #endregion

        #region "Protected Constructors"

        protected DataTypeNotSupportedException(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
        }

        #endregion
    }
}
