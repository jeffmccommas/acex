﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using ICSharpCode.SharpZipLib.Core;
using ICSharpCode.SharpZipLib.Zip;

namespace CE.GreenButton
{
    public static class ZipUtility
    {
        public static string CompressFile(string sourceFile, string entryName = "")
        {
            string zippedFile = sourceFile.Replace(".xml", ".zip");

            FileStream stream = File.Create(zippedFile);
            ZipOutputStream zStream = new ZipOutputStream(stream);

            zStream.SetLevel(3);

            CompressFile(sourceFile, zStream, entryName);

            zStream.IsStreamOwner = true; // Makes the Close also Close the underlying stream
            zStream.Close();

            File.Delete(sourceFile);

            return zippedFile;
        }

        public static string CompressCsvFile(string sourceFile, string entryName = "")
        {
            string zippedFile = sourceFile.Replace(".csv", ".zip");

            FileStream stream = File.Create(zippedFile);
            ZipOutputStream zStream = new ZipOutputStream(stream);

            zStream.SetLevel(3);

            CompressFile(sourceFile, zStream, entryName);

            zStream.IsStreamOwner = true; // Makes the Close also Close the underlying stream
            zStream.Close();

            File.Delete(sourceFile);

            return zippedFile;
        }

        //private static void CompressFiles(string path, ZipOutputStream zipStream, int folderOffset)
        //{
        //    var files = Directory.GetFiles(path).Where(p => Path.GetExtension(p) != ".zip");

        //    foreach (string filename in files)
        //        CompressFile(filename, zipStream);
        //}

        public static string CompressFiles(List<string> zipFileList, string entryName = "")
        {
            string zippedFile = zipFileList[0].Replace(".xml", ".zip");

            FileStream stream = File.Create(zippedFile);
            ZipOutputStream zStream = new ZipOutputStream(stream);

            zStream.SetLevel(3);
            int count = 0;

            foreach (string fileName in zipFileList)
            {
                if (count == 0)
                {
                    CompressFile(fileName, zStream, entryName);
                    count = count + 1;
                }
                else { CompressFile(fileName, zStream); }
            }

            zStream.IsStreamOwner = true; // Makes the Close also Close the underlying stream
            zStream.Close();

            var index = 0;
            foreach( var file in zipFileList)
            {
                if(file.Contains(".xml"))
                {
                    File.Delete(zipFileList[index]);
                }
                index = index + 1;
            }

            return zippedFile;
        }


        public static MemoryStream CompressFilesInMemory(List<GreenButtonXml>greenButtonXmlList , string entryName = "")
        {

            MemoryStream outputMemoryStream = new MemoryStream();
            ZipOutputStream zipStream = new ZipOutputStream(outputMemoryStream);

            zipStream.SetLevel(3);
            int count = 0;

            foreach (var greenButtonXml in greenButtonXmlList)
            {
                if (count == 0)
                {
                    CompressFileInMemory(greenButtonXml, zipStream, entryName);
                    count = count + 1;
                }
                else { CompressFileInMemory(greenButtonXml, zipStream); }
            }

            zipStream.IsStreamOwner = false;
            zipStream.Close();

            return outputMemoryStream;
        }


        private static void CompressFileInMemory(GreenButtonXml greenButtonXmlInfo, ZipOutputStream zipStream, string optEntryName = "")
        {
            //FileInfo fi = new FileInfo(filename);
            
            string entryName = (optEntryName == "" ? greenButtonXmlInfo.FileName : optEntryName); // Makes the name in zip based on the folder
            entryName = ZipEntry.CleanName(entryName); // Removes drive from name and fixes slash direction
            ZipEntry newEntry = new ZipEntry(entryName);
            newEntry.DateTime = DateTime.Now; //fi.LastWriteTime; // Note the zip format stores 2 second granularity
            
            //newEntry.Size = fi.Length;

            zipStream.PutNextEntry(newEntry);

            // Zip the file in buffered chunks
            // the "using" will close the stream even if an exception occurs
            byte[] buffer = new byte[4096];

            // convert string to stream
            byte[] byteArray = Encoding.UTF8.GetBytes(greenButtonXmlInfo.Xml);
            //byte[] byteArray = Encoding.ASCII.GetBytes(contents);
            MemoryStream stream = new MemoryStream(byteArray);

            StreamUtils.Copy(stream, zipStream, buffer);
            stream.Close();
            zipStream.CloseEntry();
        }


        private static void CompressFile(string filename, ZipOutputStream zipStream, string optEntryName = "")
        {
            FileInfo fi = new FileInfo(filename);

            string entryName = (optEntryName == "" ? fi.Name : optEntryName); // Makes the name in zip based on the folder
            entryName = ZipEntry.CleanName(entryName); // Removes drive from name and fixes slash direction
            ZipEntry newEntry = new ZipEntry(entryName);
            newEntry.DateTime = fi.LastWriteTime; // Note the zip format stores 2 second granularity

            // Specifying the AESKeySize triggers AES encryption. Allowable values are 0 (off), 128 or 256.
            //   newEntry.AESKeySize = 256;

            // To permit the zip to be unpacked by built-in extractor in WinXP and Server2003, WinZip 8, Java, and other older code,
            // you need to do one of the following: Specify UseZip64.Off, or set the Size.
            // If the file may be bigger than 4GB, or you do not need WinXP built-in compatibility, you do not need either,
            // but the zip will be in Zip64 format which not all utilities can understand.
            //   zipStream.UseZip64 = UseZip64.Off;
            newEntry.Size = fi.Length;

            zipStream.PutNextEntry(newEntry);

            // Zip the file in buffered chunks
            // the "using" will close the stream even if an exception occurs
            byte[] buffer = new byte[4096];
            using (FileStream streamReader = File.OpenRead(filename))
            {
                StreamUtils.Copy(streamReader, zipStream, buffer);
            }
            zipStream.CloseEntry();
        }


    }
}
