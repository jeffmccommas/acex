﻿//using Nexus.AMR;
//using Nexus.Core.Entities;

using System;
using System.Collections.Generic;
using System.Globalization;
//using System.Globalization;
using System.Linq;
using CE.Models.Insights;
using CE.Models.Insights.Types;

namespace CE.GreenButton
{
    public class ReportContent : ReportBase
    {
        private string _title;
        // ReSharper disable once NotAccessedField.Local
        private string _subtitle;
        private decimal _conversion;
        // ReSharper disable once NotAccessedField.Local
        private bool _useDefaultConversionFactor;

        private bool _axisLabelsShifting;
        
        private List<IntervalConsumptionData> _intervalConsumptionDataList;
        // green button for bill export
        private readonly List<Bill> _simpleBillCol;

        /// <summary>
        /// Ami Green Button report content
        /// </summary>
        /// <param name="intervalConsumptionDataList"></param>
        public ReportContent(List<IntervalConsumptionData> intervalConsumptionDataList)
        {
            _intervalConsumptionDataList = intervalConsumptionDataList;
        }

        /// <summary>
        /// Bill Green Button report content
        /// </summary>
        /// <param name="simpleBillCol"></param>
        public ReportContent(List<Bill> simpleBillCol)
        {
            _simpleBillCol = simpleBillCol;
        }

        public override void Fill()
        {
            base.Fill();

            _subtitle = SharedObject["SubTitle"].ToString();
            _conversion = decimal.Parse(SharedObject["Multiplier"].ToString());
            // AMI
            if (_intervalConsumptionDataList != null)
            {
                _title = SharedObject["UsageTitle"].ToString();
                _axisLabelsShifting = bool.Parse(SharedObject["AxisLabelsShifting"].ToString());
            }
            else
            {
                _useDefaultConversionFactor = Boolean.Parse(SharedObject["UseDefaultConversionFactor"].ToString());
            }
        }

        /// <summary>
        /// Generate Ami Xml
        /// </summary>
        /// <param name="writer"></param>
        public override void GenerateXml(System.Xml.XmlWriter writer)
        {
            try
            {
                GenerateMeterReadings(writer);
                GenerateUsageIntervals(writer);
            }
                // ReSharper disable once RedundantCatchClause
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Generate bill xml green button
        /// </summary>
        /// <param name="writer"></param>
        public override void GenerateMeterBillXml(System.Xml.XmlWriter writer)
        {
            try
            {
                GenerateBillXmlMeterReadings(writer);
                GenerateBillXmlIntervalBlocks(writer);
            }
                // ReSharper disable once RedundantCatchClause
            catch (Exception )
            {
                throw;
            }
        }


        /// <summary>
        /// Create the meter readings block for Ami
        /// </summary>
        /// <param name="writer"></param>
        private void GenerateMeterReadings(System.Xml.XmlWriter writer)
        {
            int index = 1;

            // create meter reading block per interval
            foreach (var consumptionData in _intervalConsumptionDataList)
            {
                if (consumptionData != null && consumptionData.ConsumptionDataList.Count > 0)
                {
                    string appender = index.ToString("D2");

                    writer.WriteStartElement("entry");
                    writer.WriteElementString("id", IdGenerator.MeterReadingId);

                    CreateLink("self", PreLink + "/UsagePoint/01/MeterReading/" + appender, writer);
                    // Bug 58008 - Added MeterReading into the up link to resolve Greenbutton Testcase D023
                    CreateLink("up", PreLink + "/UsagePoint/01/MeterReading", writer);
                    CreateLink("related", PreLink + "/UsagePoint/01/MeterReading/" + appender + "/IntervalBlock", writer);
                    CreateLink("related", "/ReadingType/01", writer);

                    // Bug 58008 - Added the title tag to fix the Greenbutton Testcase D019 
                    writer.WriteElementString("title", "Energy Delivered");
                    writer.WriteStartElement("content");
                    writer.WriteStartElement("MeterReading");
                    writer.WriteAttributeString("xmlns", "http://naesb.org/espi");
                    writer.WriteEndElement();
                    writer.WriteEndElement();

                    // Bug 58008 - Added published tag to fix Greenbutton Testcase D040, D029
                    writer.WriteElementString("published", SharedObject["StartDate"].ToString());
                    writer.WriteElementString("updated", SharedObject["StartDate"].ToString());

                    writer.WriteEndElement();


                    index += 1;

                }
            }

            

        }



        private void GenerateBillXmlMeterReadings(System.Xml.XmlWriter writer)
        {
            writer.WriteStartElement("entry");
            writer.WriteElementString("id", IdGenerator.MeterReadingId);
            CreateLink("self", PreLink + "/UsagePoint/01/MeterReading/01", writer);
            CreateLink("up", PreLink + "/UsagePoint/01/MeterReading", writer);
            CreateLink("related", PreLink + "/UsagePoint/01/MeterReading/01/IntervalBlock", writer);
            CreateLink("related", "/ReadingType/01", writer);

            writer.WriteStartElement("title");
            writer.WriteEndElement(); // end title

            writer.WriteStartElement("content");
            writer.WriteStartElement("MeterReading");
            writer.WriteAttributeString("xmlns", "http://naesb.org/espi");
            writer.WriteEndElement();
            writer.WriteEndElement();
            writer.WriteElementString("published", SharedObject["StartDate"].ToString());
            writer.WriteElementString("updated", SharedObject["StartDate"].ToString());
            writer.WriteEndElement();
        }



        private void GenerateBillXmlIntervalBlocks(System.Xml.XmlWriter writer)
        {
            writer.WriteStartElement("entry");
            // use daily for now to generate the id, once we have bill period for ami interval, we'll change it to bill period
            writer.WriteElementString("id", IdGenerator.GetIntervalBlockId(ResolutionType.day.ToString(), _simpleBillCol[_simpleBillCol.Count - 1].Premises.SelectMany(s => s.Service).Min(m => Convert.ToDateTime(m.BillStartDate))));
            CreateLink("self", PreLink + "/UsagePoint/01/MeterReading/01/IntervalBlock/01", writer);
            CreateLink("up", PreLink + "/UsagePoint/01/MeterReading/01/IntervalBlock", writer);

            writer.WriteStartElement("title");
            writer.WriteEndElement(); // end title
            writer.WriteStartElement("content");

            writer.WriteStartElement("IntervalBlock");
            writer.WriteAttributeString("xmlns", "http://naesb.org/espi");

            writer.WriteStartElement("interval");
            var billCount = _simpleBillCol.Count;
            var startDate = _simpleBillCol[billCount - 1].Premises.SelectMany(s => s.Service).Min(m => Convert.ToDateTime(m.BillStartDate));
            if (startDate == DateTime.MaxValue)
                startDate = Convert.ToDateTime(_simpleBillCol[billCount - 1].BillDate);
            var endDate = _simpleBillCol[0].Premises.SelectMany(s => s.Service).Max(m => Convert.ToDateTime(m.BillEndDate));
            if (endDate == DateTime.MinValue)
                endDate = Convert.ToDateTime(_simpleBillCol[0].BillDate);
            writer.WriteElementString("duration", Common.GetBillDuration(startDate, endDate));
            
            if (SharedObject["TZ_OFFSET_VALUE"].ToString() != "0")
                writer.WriteElementString("start", Common.ConvertDateTime(startDate, SharedObject["TZ_OFFSET_VALUE"].ToString()));
            else
                writer.WriteElementString("start", Common.ConvertDateTime(startDate));

            writer.WriteEndElement(); // end interval

            GenerateBillXmlIntervalReadings(writer);

            writer.WriteEndElement(); // end intervalblock
            writer.WriteEndElement(); // end content

            writer.WriteElementString("published", SharedObject["StartDate"].ToString());
            writer.WriteElementString("updated", SharedObject["StartDate"].ToString());
            writer.WriteEndElement(); // end entry

        }

        private void GenerateBillXmlIntervalReadings(System.Xml.XmlWriter writer)
        {
            foreach (Bill simpleBill in _simpleBillCol)
            {
                foreach (BillPremise simplePremise in simpleBill.Premises)
                {
                    foreach (BillService simpleService in simplePremise.Service)
                    {

                        writer.WriteStartElement("IntervalReading");
                        writer.WriteElementString("cost", Convert.ToDouble(simpleService.CostofUsage * 100000).ToString(CultureInfo.InvariantCulture));
                        writer.WriteStartElement("timePeriod");
                        //if (simpleService. > 1)
                        //{
                        //    var startDate = simpleService.Meters.GetEarliestStartDate();
                        //    var endDate = simpleService.Meters.GetLatestEndDate();
                        //    writer.WriteElementString("duration", Common.GetBillDuration(startDate, endDate));
                        //    if (SharedObject["TZ_OFFSET_VALUE"].ToString() != "0")
                        //        writer.WriteElementString("start", Common.ConvertDateTime(startDate, SharedObject["TZ_OFFSET_VALUE"].ToString()));
                        //    else
                        //        writer.WriteElementString("start", Common.ConvertDateTime(startDate));
                        //}
                        //else
                        //{
                            writer.WriteElementString("duration", Common.GetDaysDuration(simpleService.BillDays));
                            if (SharedObject["TZ_OFFSET_VALUE"].ToString() != "0")
                                writer.WriteElementString("start", Common.ConvertDateTime(Convert.ToDateTime(simpleService.BillStartDate), SharedObject["TZ_OFFSET_VALUE"].ToString()));
                            else
                                writer.WriteElementString("start", Common.ConvertDateTime(Convert.ToDateTime(simpleService.BillStartDate)));
                        //}
                        writer.WriteEndElement(); // end time period
                        // ACE bill doesn't have gas conversion rate - TBD?
                        //if (simpleService.CommodityKey == Nexus.Core.Constants.NXEnums.Fuel.Gas && !_useDefaultConversionFactor)
                        //{
                        //    var gasConversionFactor = simpleService.GasConversionRate;
                        //    writer.WriteElementString("value", Math.Floor(simpleService.TotalServiceUse * gasConversionFactor).ToString());
                        //}
                        //else
                        //{
                            writer.WriteElementString("value", Math.Floor(simpleService.TotalServiceUse * _conversion).ToString(CultureInfo.InvariantCulture));
                        //}
                        writer.WriteEndElement(); // end interval reading
                    }
                }
            }
        }

        /// <summary>
        /// Generate the Usage Intervals block for Ami
        /// </summary>
        /// <param name="writer"></param>
        private void GenerateUsageIntervals(System.Xml.XmlWriter writer)
        {
            int index = 1;
            long totalUsage = 0;

            foreach (var consumptionDataList in _intervalConsumptionDataList)
            {
                // only use the NoTou readings for ami; consumption.Value is either the noTou or the total of all tous
                if (consumptionDataList != null && consumptionDataList.ConsumptionDataList.Count > 0)
                {
                    bool hasStartSegment = false;

                    string appender = index.ToString("D2");
                    var resolutionType =
                        (ResolutionType) Enum.Parse(typeof (ResolutionType), consumptionDataList.ResolutionKey);
                    string duration = Common.GetResolutionDuration(resolutionType);

                    DateTime currentDate = consumptionDataList.ConsumptionDataList[0].DateTime;

                    foreach (var consumptionData in consumptionDataList.ConsumptionDataList)
                    {
                        if (consumptionData.DateTime.Day != currentDate.Day)
                        {
                            if (hasStartSegment)
                                GenerateEndSegment(writer);

                            hasStartSegment = false;

                            currentDate = consumptionData.DateTime;
                        }
                        if (!hasStartSegment)
                        {
                            hasStartSegment = true;
                            GenerateStartSegment(consumptionDataList.ResolutionKey, currentDate, appender, writer);
                        }

                        var usageValue = Math.Round(consumptionData.Value * _conversion);
                        var usageTimestamp = consumptionData.DateTime;

                        if (_axisLabelsShifting && resolutionType == ResolutionType.hour)
                            usageTimestamp = consumptionData.DateTime.AddHours(1);

                        writer.WriteStartElement("IntervalReading");
                        writer.WriteStartElement("timePeriod");
                        writer.WriteElementString("duration", duration);
                        if (SharedObject["TZ_OFFSET_VALUE"].ToString() != "0")
                            writer.WriteElementString("start", Common.ConvertDateTime(usageTimestamp, SharedObject["TZ_OFFSET_VALUE"].ToString()));
                        else
                            writer.WriteElementString("start", Common.ConvertDateTime(usageTimestamp));
                        writer.WriteEndElement();
                        writer.WriteElementString("value", usageValue.ToString(CultureInfo.InvariantCulture));
                        writer.WriteEndElement();

                        totalUsage += (long)Convert.ToDouble(usageValue);
                    }

                    if (hasStartSegment)
                        GenerateEndSegment(writer);
                }

                index += 1;
            }

            SharedObject.Add("TotalUsage", totalUsage);
        }

        /// <summary>
        /// generate the start element for ami reading
        /// </summary>
        /// <param name="resolution"></param>
        /// <param name="startDate"></param>
        /// <param name="parentIndex"></param>
        /// <param name="writer"></param>
        private void GenerateStartSegment(string resolution, DateTime startDate, string parentIndex, System.Xml.XmlWriter writer)
        {
            writer.WriteStartElement("entry");
            writer.WriteElementString("id", IdGenerator.GetIntervalBlockId(resolution, startDate));

            CreateLink("self", PreLink + "/UsagePoint/01/MeterReading/" + parentIndex + "/IntervalBlock/" + startDate.Year.ToString() + startDate.DayOfYear.ToString(), writer);
            // Bug 58008 - Add the IntervalBlock in the up link to resolve Greenbutton Testcase D025, D037
            CreateLink("up", PreLink + "/UsagePoint/01/MeterReading/" + parentIndex + "/IntervalBlock", writer);

            writer.WriteElementString("title", _title);
            writer.WriteStartElement("content");

            writer.WriteStartElement("IntervalBlock");
            writer.WriteAttributeString("xmlns", "http://naesb.org/espi");
            writer.WriteStartElement("interval");
            writer.WriteElementString("duration", "86400");
            if (SharedObject["TZ_OFFSET_VALUE"].ToString() != "0")
                writer.WriteElementString("start", Common.ConvertDateTime(startDate, SharedObject["TZ_OFFSET_VALUE"].ToString()));
            else
                writer.WriteElementString("start", Common.ConvertDateTime(startDate));
            writer.WriteEndElement();
        }

        /// <summary>
        /// generate the end element for ami reading
        /// </summary>
        /// <param name="writer"></param>
        private void GenerateEndSegment(System.Xml.XmlWriter writer)
        {
            writer.WriteEndElement();
            writer.WriteEndElement();
            //Bug 58008 - Added the published tag to fix greenbutton Testcase D040, D029
            writer.WriteElementString("published", SharedObject["StartDate"].ToString());
            writer.WriteElementString("updated", SharedObject["StartDate"].ToString());
            writer.WriteEndElement();
        }
    }
}
