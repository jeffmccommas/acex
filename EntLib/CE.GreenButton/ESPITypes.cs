﻿
namespace CE.GreenButton
{
    public class EspiTypes
    {
        public enum GreenButtonUom
        {
            WattHours = 72,
            NotApplicable = 0,
            Therms = 169,
            CubicFeet = 119,
            CubicMeter = 42,
            UsGal = 128
        }

        public enum GreenButtonCommodity
        {
            NotApplicable = 0,
            Elec = 1,
            Gas = 7,
            Water = 9
        }

        public enum BillPeriod
        {
            BillPeriod = 0,
            Daily = 1

        }

    }
}
