﻿
namespace CE.GreenButton
{
    public class GreenButtonStatusInfo
    {
        private int _databaseMeterId;

        public int DatabaseMeterId
        {
            get { return _databaseMeterId; }
            set { _databaseMeterId = value; }
        }
        private int _fuel;

        public int Fuel
        {
            get { return _fuel; }
            set { _fuel = value; }
        }

        private int _errorCode;

        public int ErrorCode
        {
            get { return _errorCode; }
            set { _errorCode = value; }
        }
        
        
        
     
    }
}
