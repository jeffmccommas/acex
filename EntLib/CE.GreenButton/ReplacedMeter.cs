﻿using System;

namespace CE.GreenButton
{ // replaced meter for green button
    public class ReplacedMeter
    {
        public string MeterId;
        public DateTime StartDate;
        public DateTime EndDate;
        public DateTime AmiStartDate;
        public DateTime AmiEndDate;
    }
}
