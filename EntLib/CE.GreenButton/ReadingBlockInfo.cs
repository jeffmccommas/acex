﻿using CE.AO.Utilities;

//using Nexus.AMR;

namespace CE.GreenButton
{
    public class ReadingBlockInfo
    {
        private double _powerOfTenMultiplier;

        public double PowerOfTenMultiplier
        {
            get { return _powerOfTenMultiplier; }
            set { _powerOfTenMultiplier = value; }
        }

        private EspiTypes.GreenButtonUom _uom;

        public EspiTypes.GreenButtonUom Uom
        {
            get { return _uom; }
            set { _uom = value; }
        }

        private EspiTypes.GreenButtonCommodity _commodity;

        public EspiTypes.GreenButtonCommodity Commodity
        {
            get { return _commodity; }
            set { _commodity = value; }
        }

        private Enums.UomType _convertToUom;

        public Enums.UomType ConvertToUom
        {
            get { return _convertToUom; }
            set { _convertToUom = value; }
        }
        
        
        
    }
}
