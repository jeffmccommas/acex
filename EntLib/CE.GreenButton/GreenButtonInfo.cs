﻿using CE.AO.Utilities;
using System;
using System.Collections.Generic;
using System.IO;

namespace CE.GreenButton
{
    public class GreenButtonInfo
    {
        public int ClientId;
        public string MeterId;
        public string AccountId;
        public string ServiceId;
        public string PremiseId;
        public string CustomerId;
        public DateTime StartDate;
        public DateTime EndDate;
        public ReportFormat Format;
        public Enums.CommodityType FuelType;
        // CR 21353 April 2012 - service point, ami start and end date
        public string ServicePoint;
        public DateTime AmiStartDate;
        public DateTime AmiEndDate;
        // CR 27420 July 2012 - CSV export
        public string Name;
        public string Multiplier;
        public string FlowDirection;
        // CR 47718 April 2014 - Green button reporting
        //public int CsrAppId;
        //public string CsrAppUsername;
        //public int AppId;
        //public int PageId;
        public ReportDataType DataType;
        public RequestType Request;
        public List<ReplacedMeter> ReplacedMeters;  // support replaced meters for green button
    }


    public class GreenButtonXml
    {
        public string FileName { get; set; }
        public string Xml { get; set; }
    }

    public class GreenButtonFile
    {
        public string FileName { get; set; }
        public MemoryStream File { get; set; }
        public string Xml { get; set; }
    }
}
