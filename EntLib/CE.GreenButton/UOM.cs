﻿
namespace CE.GreenButton
{

    public class Uom
    {
        public static double ConvertGreenButtonUnitToAclaraUnit(EspiTypes.GreenButtonUom uom, EspiTypes.GreenButtonCommodity fuel,double inValue)
        {
            double retval = 0;
            switch (uom)
	{
                case EspiTypes.GreenButtonUom.CubicFeet:
                    if (fuel == EspiTypes.GreenButtonCommodity.Gas)
                    {
                        retval = inValue * .0102;
                    }
                    if (fuel == EspiTypes.GreenButtonCommodity.Water)
                    {
                        retval = inValue * 7.48;
                    }
                    break;
                case EspiTypes.GreenButtonUom.WattHours:
                    retval = inValue * .001;
                    break;
                case EspiTypes.GreenButtonUom.CubicMeter:
                    if (fuel == EspiTypes.GreenButtonCommodity.Gas)
                    {
                        retval = inValue * .3615; 
                    }
                    if (fuel == EspiTypes.GreenButtonCommodity.Water)
                    {
                        retval = inValue * 264;
                    }
                    break;


		default:
                        retval = inValue;
                        break;
	}
            return retval;
        }
    }
}
