﻿
using System.IO;

namespace CE.GreenButton
{
    public class ExportResult
    {
        public bool Failure { get; set; } = false;

        public bool PartialData { get; set; } = false;

        public string FileName { get; set; }

        public string FilePath { get; set; }

        public string FileType { get; set; }
        public MemoryStream File { get; set; }
        public string FileXml { get; set; }
        public string ErrorMessage { get; set; }

        public string DownloadInstructions { get; set; }
    }
}
