﻿using System;
using System.IO;

namespace CE.GreenButton
{
    public class GreenButtonHistory
    {
        public int Id = -1;
        //private readonly int _historyValidityDuration;

        public bool PartialData;

        public string FileName;
        public string ServicePoint;
        //private readonly string _sharedDrivePath;
        public string SharedDrivePointer;
        public MemoryStream FileMemory;
        public string Xml;


        public DateTime CreationDate;
        public DateTime UpdationDate;

        private GreenButtonInfo _info;

        public GreenButtonHistory()
        {
            _info = new GreenButtonInfo();
            //_sharedDrivePath = sharedDrivePath;
            //_historyValidityDuration = historyValidityDuration;
        }

        public GreenButtonInfo Info
        {
            get { return _info; }
            set { _info = value; }
        }

        //public bool ValidateHistory()
        //{
        //    bool valid = false;

        //    if (Id != -1)
        //    {
        //        if (DateTime.Now.Subtract(UpdationDate).Days < _historyValidityDuration)
        //        {
        //            if (System.IO.File.Exists(_sharedDrivePath + SharedDrivePointer))
        //                valid = true;
        //        }
        //    }

        //    return valid;
        //}
    }
    
}
