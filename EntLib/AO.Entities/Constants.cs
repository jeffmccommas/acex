﻿namespace AO.Entities
{
    public static class Constants
    {


        public static class CassandraTableNames
        {
            public const string Ami = "ami";
            public const string ConsumptionByMeter = "consumption_by_meter";
	        public const string VeeStagedClientMeterByDate = "vee_staged_client_meter_by_date";
			public const string MeterHistoryByMeter = "meter_history_by_meter";
			public const string AmiReading = "meter_reading";
            public const string Billing = "billing";
            public const string Logging = "log";
            public const string Calculation = "calculation";
            public const string Insight = "insight";
            public const string BillCycleSchedule = "bill_cycle_schedule";
            public const string AccountUpdates = "account_updates";
            public const string Subscription = "subscription";
            public const string SubscriptionReport = "subscription_rpt";
            public const string ClientAccount = "client_account";
            public const string ClientAccountReport = "client_account_rpt";
            public const string Notification = "notification";
            public const string NotificationReport = "notification_rpt";
            public const string SmsTemplate = "sms_template";
            public const string TrumpiaRequestDetail = "trumpia_request_detail";
            public const string TrumpiaRequestDetailReport = "trumpia_request_detail_rpt";
            public const string TrumpiaKeywordConfiguration = "trumpia_keyword_configuration";
            public const string Customer = "customer";
            public const string CustomerReport = "customer_rpt";
            public const string Premise = "premise";
            public const string ClientMeter = "client_meter";
            public const string CustomerMobileNumber = "customer_mobile_number";
            public const string MeterHistoryByAccount = "meter_history_by_account";
            public const string MtuTypeMapping = "mtu_type";
            public const string ConsumptionByAccount = "consumption_by_account";
            public const string DcuAlarm = "dcu_alarms";
            public const string DcuAlarmHistory = "dcu_alarms_history";
            public const string DcuAlarmByCode = "dcu_alarms_by_code";
            public const string DcuAlarmByCodeHistory = "dcu_alarms_by_code_history";
            public const string DcuAlarmType = "dcu_alarm_types";
            public const string DcuProvisioning = "dcu_provisioning";
            public const string DcuCallDataHistory = "dcu_calldata_history";
            public const string Alarm = "alarms";
            public const string AlarmType = "alarm_types";
            public const string MeterType = "meter_type";
            public const string ConsumptionAggByAccount = "consumption_agg_by_account";
            public const string EmailRequestDetail = "email_request_detail";
            public const string EmailRequestDetailReport = "email_request_detail_rpt";
            public const string LogTableName = "log";
			public const string VeeConsumptionByMeter = "vee_consumption_by_meter";
            public const string AmiClientSpecificTablenames = "AMITableName";
            public const string AmiClientSpecificFilter = "AMIFilterBy";
            public const string AmiFilterByAccountID = "AccountId";
            public const string AmiFilterByServicePointId = "ServicePointId";
            public const string AmiFilterByMeterId = "MeterId";
        }

        public static class QueueNames
        {
            public const string AoCalculationQueue = "aocalculationqueue";
            public const string AoEvaluationQueue = "aoevaluationqueue";
            public const string AoImportBatchProcessQueue = "aoimportbatchprocessqueue";
            public const string AoScheduleCalculationQueue = "aoschedulecalculationqueue";
            public const string AoProcessScheduleCalculationQueue = "aoprocessschedulecalculationqueue";
            public const string AoNotificationQueue = "aonotificationqueue";
            public const string AoInsightReportQueue = "aoinsightreportqueue";
            public const string AoNotificationReportQueue = "aonotificationreportqueue";
            public const string AoScheduleExportProcessQueue = "aoscheduleexportprocessqueue";
        }
    }
}
