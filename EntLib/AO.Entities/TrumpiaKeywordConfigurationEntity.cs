﻿namespace AO.Entities
{
    public class TrumpiaKeywordConfigurationEntity
    {
        public string ClientId { get; set; }
        public string TrumpiaKeyword { get; set; }
    }
}
