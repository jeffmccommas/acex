﻿namespace AO.Entities
{
public class DcuAlarmTypeEntity
	{
        /// <summary>
        /// Hard coded DCU Alarm Type
        /// </summary>
        public string TableName { get; set; }

        /// <summary>
        /// Alarm Code for Mapping
        /// </summary>
        public string AlarmCode { get; set; }

        /// <summary>
        /// Description for Alarm Code
        /// </summary>
        public string UserFriendlyDescription { get; set; }

        /// <summary>
        ///  Severity
        /// </summary>
        public int? Severity { get; set; }

        /// <summary>
        /// Notification Type
        /// </summary>
        public string NotificationType { get; set; }

        /// <summary>
        /// Component Type
        /// </summary>
        public string ComponentType { get; set; }
	}
}
