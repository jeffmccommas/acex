﻿using System;

namespace AO.Entities
{
    public class ClientAccountReportEntity
    {
        public string RowCreateDate { get; set; }
        public int ClientId { get; set; }
        public string AccountId { get; set; }
        public string CustomerId { get; set; }

        /// <summary>
        /// Indicates the last time this notification record was
        /// created or updated. 
        /// </summary>
        public DateTime LastModifiedDate { get; set; }
    }
}
