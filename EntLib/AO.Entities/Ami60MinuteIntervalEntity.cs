﻿using System;

namespace AO.Entities
{
    public class Ami60MinuteIntervalEntity
    {
        public int ClientId { get; set; }
        public string AccountNumber { get; set; }
        public string MeterId { get; set; }
        public string ServicePointId { get; set; }
        public int CommodityId { get; set; }
        // ReSharper disable once InconsistentNaming
        public int UOMId { get; set; }
        public double? VolumeFactor { get; set; }
        public int? Direction { get; set; }
        public DateTime? ProjectedReadDate { get; set; }
        public string Timezone { get; set; }
        public DateTime AmiTimeStamp { get; set; }
        public int IntervalType { get; set; }
        public double? IntValue0000 { get; set; }
        public double? IntValue0100 { get; set; }
        public double? IntValue0200 { get; set; }
        public double? IntValue0300 { get; set; }
        public double? IntValue0400 { get; set; }
        public double? IntValue0500 { get; set; }
        public double? IntValue0600 { get; set; }
        public double? IntValue0700 { get; set; }
        public double? IntValue0800 { get; set; }
        public double? IntValue0900 { get; set; }
        public double? IntValue1000 { get; set; }
        public double? IntValue1100 { get; set; }
        public double? IntValue1200 { get; set; }
        public double? IntValue1300 { get; set; }
        public double? IntValue1400 { get; set; }
        public double? IntValue1500 { get; set; }
        public double? IntValue1600 { get; set; }
        public double? IntValue1700 { get; set; }
        public double? IntValue1800 { get; set; }
        public double? IntValue1900 { get; set; }
        public double? IntValue2000 { get; set; }
        public double? IntValue2100 { get; set; }
        public double? IntValue2200 { get; set; }
        public double? IntValue2300 { get; set; }
    }
}
