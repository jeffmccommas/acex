﻿using System;

namespace AO.Entities
{
    /// <summary>
    /// DcuProvisioning entity
    /// </summary>
    public class DcuProvisioningEntity
    {
        /// <summary>
        /// Client Identifier
        /// </summary>
        public int ClientId { get; set; }

        /// <summary>        
        /// Dcu Identifier
        /// </summary>
        public int DcuId { get; set; }

        /// <summary>
        /// Date that DCU was installed on Star network
        /// </summary>
        public string InstalledDate { get; set; }

        /// <summary>
        /// The DCU longitude coordinate.
        /// </summary>
        public string Longitude { get; set; }

        /// <summary>
        /// The DCU latitude coordinate.
        /// </summary>
        public string Latitude { get; set; }

        /// <summary>
        /// Indicates the name of DCU
        /// </summary>
        public string Name { get; set; }
    }
}