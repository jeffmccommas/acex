﻿namespace AO.Entities
{
    public class MtuTypeEntity
    {
        /// <summary>
        /// Id for this MTU type mapping
        /// </summary>
        public int MtuTypeId { get; set; }

        /// <summary>
        /// User friendly MTU Type
        /// </summary>
        public string MtuType { get; set; }

        /// <summary>
        /// Description for MTU type mapping
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Partition key.  Same value for every row as
        /// this entity is a lookup table.
        /// </summary>
        public string TableName { get; set; }
    }
}
