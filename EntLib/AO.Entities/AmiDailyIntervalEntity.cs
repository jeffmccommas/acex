﻿using System;

namespace AO.Entities
{
    public class AmiDailyIntervalEntity
    {
        public int ClientId { get; set; }
        public string AccountNumber { get; set; }
        public string MeterId { get; set; }
        public string ServicePointId { get; set; }
        public int CommodityId { get; set; }
        // ReSharper disable once InconsistentNaming
        public int UOMId { get; set; }
        public double? VolumeFactor { get; set; }
        public int? Direction { get; set; }
        public DateTime? ProjectedReadDate { get; set; }
        public string Timezone { get; set; }
        public DateTime AmiTimeStamp { get; set; }
        public int IntervalType { get; set; }
        public double? IntValue0000 { get; set; }
    }
}
