﻿using System;
// ReSharper disable InconsistentNaming
#pragma warning disable S125 

namespace AO.Entities
{
    public class ConsumptionByMeterEntity
    {
        public int ClientId { get; set; }
        public string AccountNumber { get; set; }
        public string MeterId { get; set; }
        public string ServicePointId { get; set; }
        public int CommodityId { get; set; }
        public int UOMId { get; set; }
        /// <summary>
        /// Standard Unit of Measure to which all the reads will be converted
        /// </summary>
        public int? StandardUOMId { get; set; }
        public double? VolumeFactor { get; set; }
        public int? Direction { get; set; }
        public DateTime? ProjectedReadDate { get; set; }
        public string Timezone { get; set; }
        public DateTime AmiTimeStamp { get; set; }
        public double? Consumption { get; set; }
        /// <summary>
        /// Consumption converted to Standard Unit of Measure
        /// </summary>
        public double? StandardUOMConsumption { get; set; }
        public string BatteryVoltage { get; set; }
        public string TOUBin { get; set; }
        public string TransponderId { get; set; }
        public string TransponderPort { get; set; }
        public string DialRead { get; set; }
        public string QualityCode { get; set; }
        public string Scenario { get; set; }
        public string ReadingType { get; set; }
        public string CustomerId { get; set; }
        public int IntervalType { get; set; }
        public double? TOU_Regular { get; set; }
        public double? TOU_OnPeak { get; set; }
        public double? TOU_OffPeak { get; set; }
        public double? TOU_Shoulder1 { get; set; }
        public double? TOU_Shoulder2 { get; set; }
        public double? TOU_CriticalPeak { get; set; }
    }

    public class ConsumptionByMeterEntityLight {
        public int ClientId { get; set; }
        public string AccountNumber { get; set; }
        public string MeterId { get; set; }
        public string ServicePointId { get; set; }
        public int CommodityId { get; set; }
        public int UOMId { get; set; }
        /// <summary>
        /// Standard Unit of Measure to which all the reads will be converted
        /// </summary>
        public int? StandardUOMId { get; set; }
        public double? VolumeFactor { get; set; }
        //public DateTime? ProjectedReadDate { get; set; }
        public string Timezone { get; set; }
        public DateTime AmiTimeStamp { get; set; }
        public double? Consumption { get; set; }
        /// <summary>
        /// Consumption converted to Standard Unit of Measure
        /// </summary>
        public double? StandardUOMConsumption { get; set; }
        public int IntervalType { get; set; }
        public double? TOU_Regular { get; set; }
        public double? TOU_OnPeak { get; set; }
        public double? TOU_OffPeak { get; set; }
        public double? TOU_Shoulder1 { get; set; }
        public double? TOU_Shoulder2 { get; set; }
        public double? TOU_CriticalPeak { get; set; }
    }
}
