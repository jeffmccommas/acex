﻿namespace AO.Entities
{
    public class CustomerMobileNumberEntity
    {
        public int ClientId { get; set; }
        public string CustomerId { get; set; }
        public string AccountId { get; set; }
        public string MobileNumber { get; set; }
    }
}
