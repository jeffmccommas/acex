﻿using System;

namespace AO.Entities
{
	/// <summary>
	/// MeterHistoryByMeterEntity entity
	/// </summary>
	public class MeterHistoryByMeterEntity
	{
		/// <summary>
		/// Client Id 
		/// </summary>
		public int ClientId { get; set; }

		/// <summary>
		/// Uniquely defineds the association between an Account, Meter, 
		/// and MTU in NCC head end system. 
		/// </summary>
		public int CrossReferenceId { get; set; }

		/// <summary>
		/// Account Number 
		/// </summary>
		public string AccountNumber { get; set; }

		/// <summary>
		/// Meter Identifier
		/// </summary>
		public int MeterId { get; set; }

		/// <summary>
		/// MTU Identifier
		/// </summary>
		public int MtuId { get; set; }

		/// <summary>
		/// Type of MTU associated with this meter and account.
		/// </summary>
		public int MtuTypeId { get; set; }

		/// <summary>
		/// User friendly string representation of MTU type
		/// </summary>
		public string MtuType { get; set; }

		/// <summary>
		/// Detailed description of this MTU type.
		/// </summary>
		public object MtuTypeDescription { get; set; }

		/// <summary>
		/// Date that MTU was installed on Star network
		/// </summary>
		public DateTime MtuInstallDate { get; set; }

		/// <summary>
		/// Date that Meter was installed on Star network
		/// </summary>
		public DateTime MeterInstallDate { get; set; }

		/// <summary>
		/// Indicates if this Account and Meter/MTU combination 
		/// is currently Active.  Example: An account has a meter replaced
		/// and the old Account/Meter combination is placed in inactive state
		/// while the new Account/Meter combination is placed in active state.
		/// </summary>
		public bool Active { get; set; }

		/// <summary>
		/// The MTU longitude coordinate.
		/// </summary>
		public string Longitude { get; set; }

		/// <summary>
		/// The MTU latitude coordinate.
		/// </summary>
		public string Latitude { get; set; }

		/// <summary>
		/// Indicates the type of Meter associated with this account.
		/// </summary>
		public int MeterTypeId { get; set; }

		/// <summary>
		/// Indicates the Meter type Description
		/// </summary>
		public string MeterTypeDescripton { get; set; }

		/// <summary>
		/// Serial Number associated with this meter
		/// </summary>
		public string MeterSerialNumber { get; set; }
	}
}
