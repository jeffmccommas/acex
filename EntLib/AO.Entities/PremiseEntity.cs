﻿using System;

namespace AO.Entities
{
    public class PremiseEntity
    {
        public string Country { get; set; }
        public int ClientId { get; set; }
        public string CustomerId { get; set; }
        public string PremiseId { get; set; }
        public string AccountId { get; set; }
        public string AddressLine1 { get; set; }
        public string AddressLine2 { get; set; }
        public string AddressLine3 { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string PostalCode { get; set; }

        /// <summary>
        /// Indicates the last time this notification record was
        /// created or updated. 
        /// </summary>
        public DateTime LastModifiedDate { get; set; }
    }
}
