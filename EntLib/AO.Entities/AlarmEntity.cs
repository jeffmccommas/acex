﻿using System;

namespace AO.Entities
{
    /// <summary>
    /// Alarm Class which will be interacting with Database
    /// </summary>
    public class AlarmEntity
    {
        /// <summary>
        /// Client Id 
        /// </summary>
        public int ClientId { get; set; }

        /// <summary>
        /// Alarm Source Type Id
        /// </summary>
        public int AlarmSourceTypeId { get; set; }

        /// <summary>
        /// alarm Time
        /// </summary>
        public DateTime AlarmTime { get; set; }

        /// <summary>
        /// Alarm Group Id
        /// </summary>
        public int AlarmGroupId { get; set; }

        /// <summary>
        /// Alarm Type Id
        /// </summary>
        public int AlarmTypeId { get; set; }

        /// <summary>
        /// Alarm_Id
        /// </summary>
        public int AlarmId { get; set; }

        /// <summary>
        /// Alarm Group Name
        /// </summary>
        public string AlarmGroupName { get; set; }

        /// <summary>
        /// Alarm Source Id
        /// </summary>
        public int AlarmSourceId { get; set; }

        /// <summary>
        /// Alarm Source Type
        /// </summary>
        public string AlarmSourceType { get; set; }

        /// <summary>
        /// Alarm Type Name
        /// </summary>
        public string AlarmTypeName { get; set; }

        /// <summary>
        /// Details
        /// </summary>
        public string Details { get; set; }
    }
}
