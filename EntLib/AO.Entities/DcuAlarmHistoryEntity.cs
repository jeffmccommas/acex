﻿using System;

namespace AO.Entities
{
public 	class DcuAlarmHistoryEntity
	{
        /// <summary>
        /// Data Collector Unit 
        /// </summary>
		public int DcuId { get; set; }

        /// <summary>
        /// Alarm Code for Mapping
        /// </summary>
		public string AlarmCode { get; set; }

        /// <summary>
        /// Alarm Time
        /// </summary>
		public DateTime AlarmTime { get; set; }

        /// <summary>
        /// Client Id 
        /// </summary>
		public int ClientId { get; set; }

        /// <summary>
        /// Description for Alarm Code
        /// </summary>
		public string UserFriendlyDescription { get; set; }

        /// <summary>
        ///  Severity
        /// </summary>
		public int? Severity { get; set; }

        /// <summary>
        /// Notification Type
        /// </summary>
		public string NotificationType { get; set; }

        /// <summary>
        /// Component Type
        /// </summary>
		public string ComponentType { get; set; }
	}
}
