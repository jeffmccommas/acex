﻿using System;

namespace AO.Entities
{
    public class MeterReadingEntity
    {
        public int ClientId { get; set; }
        public string AccountNumber { get; set; }
        public int CommodityId { get; set; }
        public string MeterId { get; set; }
        public int TransponderId { get; set; }
        public int TransponderPort { get; set; }
        public string CustomerId { get; set; }
        public double ReadingValue { get; set; }
        public int UomId { get; set; }
        public DateTime AmiTimeStamp { get; set; }
        public string Timezone { get; set; }
        public string BatteryVoltage { get; set; }
    }
}
