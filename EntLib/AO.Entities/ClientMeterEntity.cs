﻿namespace AO.Entities
{
    public class ClientMeterEntity
    {
        public int ClientId { get; set; }
        public string AccountId { get; set; }
        public string MeterId { get; set; }
        public bool? IsActive { get; set; }
        public string ServiceContractId { get; set; }
        public string CustomerId { get; set; }
        public bool IsServiceContractCreatedBySystem { get; set; }
    }
}
