﻿namespace AO.Entities
{
    public class BillCycleScheduleEntity
    {
        public string ClientId { get; set; }
        public string BillCycleScheduleId { get; set; }
        public string BillCycleScheduleName { get; set; }
        //changed as table in storage is string 
        public string  BeginDate { get; set; }
        //changed as table in storage is string 
        public string EndDate { get; set; }
        public string Description { get; set; }
    }
}
