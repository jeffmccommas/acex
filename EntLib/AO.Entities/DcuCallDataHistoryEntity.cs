﻿using System;

namespace AO.Entities
{
 public class DcuCallDataHistoryEntity
    {
        /// <summary>
        /// Client Id 
        /// </summary>
		public int ClientId { get; set; }

        /// <summary>
        /// Data Collector Unit 
        /// </summary>
		public int DcuId { get; set; }        

        /// <summary>
        /// Time when the call started
        /// </summary>
		public DateTime CallStart { get; set; }

        /// <summary>
        /// Time when the call ended
        /// </summary>
		public DateTime CallEnd { get; set; }

        /// <summary>
        ///  Type of Call
        /// </summary>
		public string CallType { get; set; }

        /// <summary>
        /// Number of bytes received
        /// </summary>
		public int Received { get; set; }

        /// <summary>
        /// Number of bytes expected
        /// </summary>
		public int Expected { get; set; }

        /// <summary>
        /// Is the call hung up
        /// </summary>
		public bool HungUp { get; set; }

        /// <summary>
        /// Is the dcu's memory cleared
        /// </summary>
		public bool Cleared { get; set; }
    }
}
