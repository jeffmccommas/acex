﻿using System;

namespace AO.Entities
{
    public class TrumpiaRequestDetailEntity
    {
        public string MessageId { get; set; }
        public string RequestId { get; set; }
        public string Description { get; set; }
        public string StatusCode { get; set; }
        public string ErrorMessage { get; set; }
        public string SmsBody { get; set; }

        /// <summary>
        /// Indicates the last time this notification record was
        /// created or updated. 
        /// </summary>
        public DateTime LastModifiedDate { get; set; } = DateTime.UtcNow;
    }
}
