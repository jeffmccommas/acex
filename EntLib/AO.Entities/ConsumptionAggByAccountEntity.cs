﻿using System;

namespace AO.Entities
{
    /// <summary>
    /// ConsumptionAggByAccount Entity
    /// </summary>
    public class ConsumptionAggByAccountEntity
    {
        /// <summary>
        /// Client Identifier 
        /// </summary>
        public int ClientId { get; set; }

        /// <summary>
        /// Account Number 
        /// </summary>
        public string AccountNumber { get; set; }

        /// <summary>
        /// AggregationType (day/hour)
        /// </summary>
        public string AggregationType { get; set; }

        /// <summary>
        /// Aggregation End Datetime
        /// </summary>
        public DateTime AggregationEndDateTime { get; set; }

        /// <summary>
        /// Commodity Identifier
        /// </summary>
        public int CommodityId { get; set; }

        /// <summary>
        /// Meter AMI consumption value
        /// </summary>
        // ReSharper disable once InconsistentNaming
        public double StandardUOMConsumption { get; set; }

        /// <summary>
        /// Timezone where the reading was taken
        /// </summary>
        public string Timezone { get; set; }

        /// <summary>
        /// Unit Of Measure
        /// </summary>
        // ReSharper disable once InconsistentNaming
        public int StandardUOMId { get; set; }
    }
}