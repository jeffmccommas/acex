﻿using System;
using System.ComponentModel.DataAnnotations;

namespace AO.Entities
{
    /// <summary>
    /// LogEntity is used to fetch the log data from the database
    /// </summary>
    public class LogEntity
    {
        [Key]
        public int MonthYear { get; set; }
        public string Level { get; set; }
        public string Message { get; set; }
        public string StackTrace { get; set; }
        public DateTimeOffset TimestampUtc { get; set; }
        public string ClientId { get; set; }
        public string CustomerId { get; set; }
        public string PremiseId { get; set; }
        public string ServiceContractId { get; set; }
        public string MeterId { get; set; }
        public string AccountId { get; set; }
        public string Metadata { get; set; }
        public string Source { get; set; }
        public int? ProcessingEventCount { get; set; }
        public string Module { get; set; }
        public string ProcessingType { get; set; }
        public string RowIndex { get; set; }
    }
}
