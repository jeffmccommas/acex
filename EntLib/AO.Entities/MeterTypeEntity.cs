﻿namespace AO.Entities
{
    /// <summary>
    /// MeterType entity
    /// </summary>
    public class MeterTypeEntity
    {
        /// <summary>
        /// TableName
        /// </summary>
        public string TableName { get; set; }

        /// <summary>
        /// Indicates the type of Meter associated with this account.
        /// </summary>
        public int MeterTypeId { get; set; }

        /// <summary>
        /// Indicates whether Meter type is Active or not
        /// </summary>
        public bool Active { get; set; }

        /// <summary>
        /// Indicates the Description
        /// </summary>
        public string Descripton { get; set; }

        /// <summary>
        /// Indicates the MeterReadingUnit
        /// </summary>
        public string MeterReadingUnit { get; set; }

        /// <summary>
        /// Indicates the MeterReadingUtility
        /// </summary>
        public string MeterReadingUtility { get; set; }

        /// <summary>
        /// Indicates the Model
        /// </summary>
        public string Model { get; set; }

        /// <summary>
        /// Indicates the PrisonDisplay
        /// </summary>
        public string PsionDisplay { get; set; }

        /// <summary>
        /// Indicates the Registercount
        /// </summary>
        public short Registercount { get; set; }

        /// <summary>
        /// Indicates the UnitEnumerator
        /// </summary>
        public int UnitEnumerator { get; set; }

        /// <summary>
        /// Indicates the UnitScalar
        /// </summary>
        public float? UnitScalar { get; set; }

        /// <summary>
        /// Indicates the Vendor
        /// </summary>
        public string  Vendor { get; set; }
    }
}
