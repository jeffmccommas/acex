﻿using System;

namespace AO.Entities
{
    /// <summary>
    /// MeterRead model
    /// </summary>
    public class ConsumptionByAccountEntity
    {
        /// <summary>
        /// Client Identifier 
        /// </summary>
        public int ClientId { get; set; }

        /// <summary>
        /// Meter Identifier 
        /// </summary>
        public string MeterId { get; set; }

        /// <summary>
        /// Transponder Identifier
        /// </summary>        
        public string TransponderId { get; set; }

        /// <summary>
        /// Transponder Port
        /// </summary>
        public string TransponderPort { get; set; }

        /// <summary>
        /// Customer Identifier
        /// </summary>
        public string CustomerId { get; set; }

        /// <summary>
        /// Meter AMI consumption value
        /// </summary>
        public double Consumption { get; set; }

        /// <summary>
        /// Consumption converted to Standard Unit of Measure
        /// </summary>
        // ReSharper disable once InconsistentNaming
        public double StandardUOMConsumption { get; set; }

        /// <summary>
        /// Standard Unit of Measure to which all the reads will be converted
        /// </summary>
        // ReSharper disable once InconsistentNaming
        public int StandardUOMId { get; set; }

        /// <summary>
        /// Unit Of Measure
        /// </summary>
        // ReSharper disable once InconsistentNaming
        public int UOMId { get; set; }

        /// <summary>
        /// Indicates what time the reading was taken
        /// </summary>
        public DateTime AmiTimeStamp { get; set; }

        /// <summary>
        /// Timezone where the reading was taken
        /// </summary>
        public string Timezone { get; set; }

        /// <summary>
        /// Voltage of the battery
        /// </summary>
        public string BatteryVoltage { get; set; }

        /// <summary>
        /// Commodity Identifier
        /// </summary>
        public int CommodityId { get; set; }

        /// <summary>
        /// Account Number 
        /// </summary>
        public string AccountNumber { get; set; }

        /// <summary>
        /// Interval of the Read
        /// </summary>
        public int IntervalType { get; set; }
    }
}