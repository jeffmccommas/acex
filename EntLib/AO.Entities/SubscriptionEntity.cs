﻿using System;

namespace AO.Entities
{
    public class SubscriptionEntity
    {
        public int ClientId { get; set; }
        public string CustomerId { get; set; }
        public string AccountId { get; set; }
        public string ProgramName { get; set; }
        public string InsightTypeName { get; set; }
        public string ServiceContractId { get; set; }
        public bool? IsEmailOptInCompleted { get; set; }
        public bool? IsSmsOptInCompleted { get; set; }
        public bool IsSelected { get; set; }
        public string Channel { get; set; }
        public double? SubscriberThresholdValue { get; set; }
        public int? EmailConfirmationCount { get; set; }
        public int? SmsConfirmationCount { get; set; }
        public DateTime? SmsOptInCompletedDate { get; set; }
        public DateTime? EmailOptInCompletedDate { get; set; }
        public DateTime? SubscriptionDate { get; set; }
        public DateTime? OptOutDate { get; set; }
        public int? UomId { get; set; }
        public string Type { get; set; }
        public bool IsLatest { get; set; }

        /// <summary>
        /// Indicates the last time this notification record was
        /// created or updated. 
        /// </summary>
        public DateTime LastModifiedDate { get; set; }
    }
}
