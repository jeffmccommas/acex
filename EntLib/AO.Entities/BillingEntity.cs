﻿using System;

namespace AO.Entities
{
    public class BillingEntity
    {
        public int ClientId { get; set; }
        public string CustomerId { get; set; }
        public string AccountId { get; set; }
        public string PremiseId { get; set; }
        public DateTime? AmiStartDate { get; set; }
        public DateTime? AmiEndDate { get; set; }
        public string BillCycleScheduleId { get; set; }
        public string RateClass { get; set; }
        public string ServicePointId { get; set; }
        public string Source { get; set; }
        public string MeterType { get; set; }
        // ReSharper disable once InconsistentNaming
        public int? UOMId { get; set; }
        public double? TotalUsage { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public int BillDays { get; set; }
        public string ReadQuality { get; set; }
        public double TotalCost { get; set; }
        public int CommodityId { get; set; }
        public string MeterId { get; set; }
        public int BillPeriodType { get; set; }
        public string ReplacedMeterId { get; set; }
        public DateTime? ReadDate { get; set; }
        public string ServiceContractId { get; set; }
        public bool? IsFault { get; set; }
        public bool IsLatest { get; set; }
        
        public static string GetServiceContractId(string meterId, string accountId, string premiseId, int commodityId)
        {
            return !string.IsNullOrWhiteSpace(meterId)
                ? $"{accountId}_{premiseId}_{commodityId}_{meterId}"
                : $"{accountId}_{premiseId}_{commodityId}";
        }
    }
}
