﻿using System;

namespace AO.Domain
{
    /// <summary>A class to represent a registered client of the Aclara One application.</summary>
    public class Client
    {
        public int ClientID { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public int RateCompanyID { get; set; }

        public int ReferrerID { get; set; }

        public DateTime NewDate { get; set; }

        public DateTime UpdDate { get; set; }

        public byte AuthType { get; set; }

        public bool EnableInd { get; set; }
    }
}
