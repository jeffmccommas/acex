﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AO.Domain
{
    /// <summary>A class that associates an Azure tenant with an Aclara One client.</summary>
    public class Tenant
    {
        public string TenantId { get; set; }

        public int ClientId { get; set; }

        public Client Client { get; set; }
    }
}
