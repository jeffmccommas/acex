﻿using System;
using System.Collections.Generic;

namespace CE.NotificationManager.Entities
{
    public class EventInfo
    {
        public string EventId { get; set; }
        public EventType EventType { get; set; }
        public string Target { get; set; }
        public DateTime StartTime { get; set; }
        public DateTime StartUtcTime { get; set; }
        public DateTime EndTime { get; set; }
        public DateTime EndUtcTime { get; set; }
        public DateTime NotifyTime { get; set; }
        public DateTime NotifyUtcTime { get; set; }
        public List <RegistrationInfo> RegistrationInfoList { get; set; }
        public string Zone { get; set; }
        public LeadTimeType LeadTime { get; set; }
        public EventStatus Status { get; set; }
        public string EventXml { get; set; }
        public string TeamName { get; set; }

    }

    public class EventsCollection
    {
        public string Xml { get; set; }

        public List<EventInfo> Events { get; set; }

        public EventsCollection()
        {
            Events = new List<EventInfo>();
        }
    }
    public class RegistrationInfo
    {
        public string RegistrationId { get; set; }
        public string Zone { get; set; }
        public string TeamName { get; set; }
        public bool IsHoneywellDlc { get; set; }
        public string Target { get; set; }
    }

    public enum EventStatus
    {
        Scheduled,
        Active,
        Completed,
        Canceled,
        None
    }

    public enum EventType
    {
        LoadManagement,
        RealTimeDispatch,
        SynchronizedReserved,
        ZonalTest,
        ZonalRetest,
        None
    }

    public enum LeadTimeType
    {
        Quick30,
        Short60,
        Long120
    }
    
}
