using System;
using System.Diagnostics;
using System.Net;
using System.Threading;
using AO.BusinessContracts;
using AO.Registrar;
using Microsoft.Practices.Unity;
using Microsoft.WindowsAzure.ServiceRuntime;

namespace AO.ValidationEstimation.EventProcessWorker 
{
    public class WorkerRole : RoleEntryPoint, IDisposable
    {
        private CancellationTokenSource _cancellationTokenSource = new CancellationTokenSource();
        private ManualResetEvent _runCompleteEvent = new ManualResetEvent(false);
		private ITriggerSubscriber _validationEstimationTrigger;
		public static IUnityContainer UnityContainer { get; set; }
		
        public override void Run()
        {
            Trace.TraceInformation("AO.ValidationEstimation.EventProcessWorker is running");
            try
            {
                if (!_cancellationTokenSource.Token.IsCancellationRequested)
                {
                    UnityContainer = new UnityContainer();
					new DataStorageRegistrar().Initialize<TransientLifetimeManager>(UnityContainer);
					_validationEstimationTrigger = UnityContainer.Resolve<ITriggerSubscriber>("ValidationEstimationTriggerSubscriber");
					_validationEstimationTrigger.Subscribe();
				}

                //Wait for shutdown to be called, else the role will recycle
                _runCompleteEvent.WaitOne();
            }
            finally
            {
                _runCompleteEvent.Set();
            }
        }

        public override bool OnStart()
        {
            // Set the maximum number of concurrent connections
            ServicePointManager.DefaultConnectionLimit = 12;

            // For information on handling configuration changes
            // see the MSDN topic at http://go.microsoft.com/fwlink/?LinkId=166357.

            bool result = base.OnStart();

            Trace.TraceInformation("AO.ValidationEstimation.EventProcessWorker has been started");

            return result;
        }

        public override void OnStop()
        {
            Trace.TraceInformation("AO.ValidationEstimation.EventProcessWorker is stopping");

            _cancellationTokenSource.Cancel();
            _runCompleteEvent.WaitOne();

            try
            {
				//Unregister the event processor so other instances will handle the partitions
				_validationEstimationTrigger.UnSubscribe();
			}
            catch (Exception oops)
            {
                Trace.TraceError(oops.Message);
            }

            base.OnStop();

            Trace.TraceInformation("AO.ValidationEstimation.EventProcessWorker has stopped");
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (_cancellationTokenSource != null)
            {
                _cancellationTokenSource.Cancel();
                _cancellationTokenSource.Dispose();
                _cancellationTokenSource = null;
            }
            if (_runCompleteEvent != null)
            {
                _runCompleteEvent.Dispose();
                _runCompleteEvent = null;
            }
        }
    }
}
