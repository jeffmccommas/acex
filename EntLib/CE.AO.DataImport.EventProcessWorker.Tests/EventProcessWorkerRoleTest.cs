﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace CE.AO.DataImport.EventProcessWorker.Tests
{
    [TestClass]
   public class EventProcessWorkerRoleTest
    {
        private Mock<WorkerRole> _workerRoleMock;
        private WorkerRole _workerRole;
        /// <summary>
        /// Test Case Init
        /// </summary>
        [TestInitialize]
        public void Init()
        {
            _workerRoleMock = new Mock<WorkerRole>();
            _workerRole = new WorkerRole();
        }

        /// <summary>
        /// Test case for OnStart method of EventProcess Worker Rule
        /// </summary>
        [TestMethod]
        public void OnStart()
        {
            _workerRoleMock.Setup(w => w.OnStart()).Returns(true).Verifiable();
            var result = _workerRole.OnStart();
            Assert.AreEqual(result, true);
        }
    }
}

