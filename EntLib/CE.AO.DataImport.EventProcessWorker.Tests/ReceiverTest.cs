﻿//using System;
//using Microsoft.VisualStudio.TestTools.UnitTesting;
//using Microsoft.ServiceBus;
//using Microsoft.ServiceBus.Messaging;

//namespace CE.AO.DataImport.EventProcessWorker.Tests
//{
//    /// <summary>
//    /// Test case for Event Process Worker class
//    /// </summary>
//    [TestClass]
//    public class ReceiverTest
//    {
//        /// <summary>
//        /// Test case for RegisterEventProcessor method
//        /// </summary>
//        [TestMethod]
//        public void RegisterEventProcessor()
//        {
//            try
//            {
//                var consumer = new ConsumerGroupDescription("aclaoazuretableloggingehdev", "Receiver");
//                var builder =
//                    new ServiceBusConnectionStringBuilder(
//                        "Endpoint=sb://aclaoaclaraonesbnsdev.servicebus.windows.net/;SharedAccessKeyName=Manage;SharedAccessKey=gwp1cTMyUcJ2t3tP21AgbKdvw+efteMwecwjafl7oBI=")
//                    {
//                        TransportType = TransportType.Amqp
//                    };
//                var a = builder.ToString();
//                var r = new Receiver("Test", a);
//                r.RegisterEventProcessor(consumer,
//                    "DefaultEndpointsProtocol=https;AccountName=aclaoacaraonestgdev;AccountKey=5Kv12Tw2/OaHqfy9yrWf9b0uQM5xa0K9559guWhsb6ugUmS/u5FN86qCS8LtZfAYsDqtOvi8VisNaaRnXQ==",
//                    "Receive");
//            }
//            catch (Exception ex)
//            {
//                Console.WriteLine(ex);
//            }
//        }

//        /// <summary>
//        /// Test case for UnregisterEventProcessor method
//        /// </summary>
//        [TestMethod]
//        public void UnregisterEventProcessor()
//        {
//            try
//            {
//                var builder =
//                     new ServiceBusConnectionStringBuilder(
//                         "Endpoint=sb://aclaoaclaraonesbnsdev.servicebus.windows.net/;SharedAccessKeyName=Manage;SharedAccessKey=gwp1cTMyUcJ2t3tP21AgbKdvw+efteMwecwjafl7oBI=")
//                     {
//                         TransportType = TransportType.Amqp
//                     };
//                var a = builder.ToString();
//                var r = new Receiver("Test", a);
//                r.UnregisterEventProcessor();
//            }
//            catch (Exception ex)
//            {
//                Console.WriteLine(ex);
//            }
//        }
//    }
//}