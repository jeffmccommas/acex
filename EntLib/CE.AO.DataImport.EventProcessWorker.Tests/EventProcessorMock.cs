﻿using System.Threading.Tasks;
using Microsoft.ServiceBus.Messaging;
using System.Collections.Generic;

namespace CE.AO.DataImport.EventProcessWorker.Tests
{
    /// <summary>
    /// Mock class of EventProcessWorker
    /// </summary>
    public class EventProcessorMock : IEventProcessor
    {
        /// <summary>
        /// Mock method of CloseAsync from EventProcessor
        /// </summary>
        /// <param name="context"></param>
        /// <param name="reason"></param>
        /// <returns></returns>
        public Task CloseAsync(PartitionContext context, CloseReason reason)
        {
           return Task.FromResult(true);
        }

        /// <summary>
        /// Mock method of OpenAsync from EventProcessor
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>
        public Task OpenAsync(PartitionContext context)
        {
            return Task.FromResult(true);
        }

        /// <summary>
        /// Mock method of ProcessEventsAsync from EventProcessor
        /// </summary>
        /// <param name="context"></param>
        /// <param name="messages"></param>
        /// <returns></returns>
        public Task ProcessEventsAsync(PartitionContext context, IEnumerable<EventData> messages)
        {
            return Task.FromResult(true);
        }
    }
}
