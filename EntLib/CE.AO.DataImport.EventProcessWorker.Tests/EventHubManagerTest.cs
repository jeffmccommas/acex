﻿using CE.AO.Utilities;
using Microsoft.ServiceBus;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace CE.AO.DataImport.EventProcessWorker.Tests
{
    /// <summary>
    /// Test cases for Event Hub Manager class
    /// </summary>
    [TestClass]
    public class EventHubManagerTest
    {
        /// <summary>
        /// Test cases for GetServiceBusConnectionString method
        /// </summary>
        [TestMethod]
        public void GetServiceBusConnectionString()
        {
            EventHubManager.GetNamespaceManager("Endpoint=sb://aclaoaclaraonesbnsdev.servicebus.windows.net/;SharedAccessKeyName=Send;SharedAccessKey=q530n4AmCK3MSYSh5j6299pO8vsKt4pP6zLALMy6Tv8=;TransportType=Amqp");
            Assert.AreEqual(true, true);
        }
        
        /// <summary>
        /// Test cases for GetNamespaceManager method
        /// </summary>
        [TestMethod]
        public void GetNamespaceManager()
        {
            var manager = EventHubManager.GetNamespaceManager("Endpoint=sb://aclaoaclaraonesbnsdev.servicebus.windows.net/;SharedAccessKeyName=Send;SharedAccessKey=q530n4AmCK3MSYSh5j6299pO8vsKt4pP6zLALMy6Tv8=;TransportType=Amqp");
            Assert.AreEqual(manager.Address.AbsoluteUri, "sb://aclaoaclaraonesbnsdev.servicebus.windows.net/");
            Assert.IsNotNull(manager);
        }

        /// <summary>
        /// Test cases for CreateEventHubIfNotExists method
        /// </summary>
        [TestMethod]
        public void CreateEventHubIfNotExists()
        {
               EventHubManager.CreateEventHubIfNotExists("TestCase", 32, new NamespaceManager("sb://aclaoaclaraonesbnsdev.servicebus.window.net/"));
               Assert.AreEqual(true, true);
            
        }


    }
}
