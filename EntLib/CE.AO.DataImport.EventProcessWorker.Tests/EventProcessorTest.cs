﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Runtime.Remoting.Messaging;
//using System.Text;
//using CE.AO.Business;
//using CE.AO.Entities.TableStorageEntities;
//using CE.AO.Logging;
//using CE.AO.Models;
//using CE.AO.Models.RawFileModels;
//using Microsoft.Practices.Unity;
//using Microsoft.VisualStudio.TestTools.UnitTesting;
//using Newtonsoft.Json;
//using Microsoft.ServiceBus.Messaging;

//namespace CE.AO.DataImport.EventProcessWorker.Tests
//{
//    /// <summary>
//    /// Test Case Class for Event Processor Worker Role(Function.cs)
//    /// </summary>
//    [TestClass]
//    public class EventProcessorTest
//    {
//        /// <summary>
//        /// Test Case Init
//        /// </summary>
//        [TestInitialize]
//        public void Init()
//        {
//            AutoMapperConfig.AutoMapperMappings();
//        }

//        /// <summary>
//        /// Test Case for OpenAsync Method
//        /// </summary>
//        [TestMethod]
//        public void OpenAsync()
//        {
//            var partitionContext = new PartitionContext();
//            var lease = new Lease();
//            partitionContext.Lease = lease;
//            partitionContext.Lease.Offset = "Lease";
//            EventProcessor.LogModel = new LogModel
//            {
//                DisableLog = false
//            };
//            var eventProc = new EventProcessor();

//            var result = eventProc.OpenAsync(partitionContext);
//            Assert.AreEqual(result.Exception, null);
//        }

//        /// <summary>
//        /// Test Case for Ami 30 Minutes
//        /// </summary>
//        [TestMethod]
//        public void ProcessEventsAsync_Ami30()
//        {
//            var record = new RawAmi30MinuteIntervalModel
//            {
//                ClientId = "87",
//                AccountNumber = "Accn0700",
//                MeterId = "Meter0700",
//                ServicePointId = "SP0700",
//                CommodityId = "2",
//                UOMId = "1",
//                VolumeFactor = "",
//                Direction = "",
//                ProjectedReadDate = "6/15/2016",
//                TimeStamp = "6/24/2016",
//                Timezone = "EST",
//                IntValue0000 = "1"
//            };
//            var serializedEventData = JsonConvert.SerializeObject(record);
//            var eventData = new EventData(Encoding.UTF8.GetBytes(serializedEventData));
//            eventData.Properties.Add("EventType", 1);
//            eventData.Properties.Add("IntervalType", 30);
//            eventData.Properties.Add("ClientId", "87");
//            eventData.Properties.Add("ProcessingType", Enums.ProcessingType.File);
//            eventData.Properties.Add("Source", "S");
//            eventData.Properties.Add("RowIndex", 1);
//            var context = new PartitionContext();
//            var lease = new Lease();
//            context.Lease = lease;
//            context.Lease.Offset = "Lease";
//            context.Lease.PartitionId = "PartitionId";
//            EventProcessor.LogModel = new LogModel
//            {
//                DisableLog = false
//            };
//            var eventProc = new EventProcessor();

//            var eventDataList = new List<EventData> { eventData };
//            eventProc.OpenAsync(context);
//            var result = eventProc.ProcessEventsAsync(context, eventDataList.AsEnumerable());
//            Assert.AreEqual(result.Exception, null);
//        }

//        /// <summary>
//        /// Test Case for Ami 15 Minutes
//        /// </summary>
//        [TestMethod]
//        public void ProcessEventsAsync_Ami15()
//        {
//            var record = new RawAmi15MinuteIntervalModel
//            {
//                ClientId = "87",
//                AccountNumber = "Accn0700",
//                MeterId = "Meter0700",
//                ServicePointId = "SP0700",
//                CommodityId = "2",
//                UOMId = "1",
//                VolumeFactor = "",
//                Direction = "",
//                ProjectedReadDate = "6/15/2016",
//                TimeStamp = "6/24/2016",
//                Timezone = "EST",
//                IntValue0000 = "1"
//            };
//            var serializedEventData = JsonConvert.SerializeObject(record);
//            var eventData = new EventData(Encoding.UTF8.GetBytes(serializedEventData));
//            eventData.Properties.Add("EventType", 1);
//            eventData.Properties.Add("IntervalType", 30);
//            eventData.Properties.Add("ClientId", "87");
//            eventData.Properties.Add("ProcessingType", Enums.ProcessingType.File);
//            eventData.Properties.Add("Source", "S");
//            eventData.Properties.Add("RowIndex", 1);
//            var context = new PartitionContext();
//            var lease = new Lease();
//            context.Lease = lease;
//            context.Lease.Offset = "Lease";
//            context.Lease.PartitionId = "PartitionId";
//            EventProcessor.LogModel = new LogModel
//            {
//                DisableLog = false
//            };
//            var eventProc = new EventProcessor();

//            var eventDataList = new List<EventData> { eventData };
//            eventProc.OpenAsync(context);
//            var result = eventProc.ProcessEventsAsync(context, eventDataList.AsEnumerable());
//            Assert.AreEqual(result.Exception, null);
//        }

//        /// <summary>
//        /// Test Case for Ami 60 Minutes
//        /// </summary>
//        [TestMethod]
//        public void ProcessEventsAsync_Ami60()
//        {
//            var record = new RawAmi60MinuteIntervalModel
//            {
//                ClientId = "87",
//                AccountNumber = "Accn0700",
//                MeterId = "Meter0700",
//                ServicePointId = "SP0700",
//                CommodityId = "2",
//                UOMId = "1",
//                VolumeFactor = "",
//                Direction = "",
//                ProjectedReadDate = "6/15/2016",
//                TimeStamp = "6/24/2016",
//                Timezone = "EST",
//                IntValue0000 = "1"
//            };
//            var serializedEventData = JsonConvert.SerializeObject(record);
//            var eventData = new EventData(Encoding.UTF8.GetBytes(serializedEventData));
//            eventData.Properties.Add("EventType", 1);
//            eventData.Properties.Add("IntervalType", 30);
//            eventData.Properties.Add("ClientId", "87");
//            eventData.Properties.Add("ProcessingType", Enums.ProcessingType.File);
//            eventData.Properties.Add("Source", "S");
//            eventData.Properties.Add("RowIndex", 1);
//            var context = new PartitionContext();
//            var lease = new Lease();
//            context.Lease = lease;
//            context.Lease.Offset = "Lease";
//            context.Lease.PartitionId = "PartitionId";
//            EventProcessor.LogModel = new LogModel
//            {
//                DisableLog = false
//            };
//            var eventProc = new EventProcessor();

//            var eventDataList = new List<EventData> { eventData };
//            eventProc.OpenAsync(context);
//            var result = eventProc.ProcessEventsAsync(context, eventDataList.AsEnumerable());
//            Assert.AreEqual(result.Exception, null);
//        }

//        /// <summary>
//        /// Test Case for Billing
//        /// </summary>
//        [TestMethod]
//        public void ProcessEventsAsync_Billing()
//        {
//            var record = new RawBillingModel
//            {
//                ClientId = "87",
//                customer_id = "C0700",
//                mail_address_line_1 = "67",
//                mail_address_line_2 = "Test",
//                mail_address_line_3 = "Test",
//                mail_city = "KOP",
//                mail_state = "MH",
//                mail_zip_code = "416001",
//                first_name = "James",
//                last_name = "Bond",
//                phone_1 = "9503937707",
//                phone_2 = "9503937708",
//                email = "krishnakant.datkhile@saviantconsulting.com",
//                customer_type = "Residential",
//                account_id = "Accn0700",
//                active_date = "",
//                inactive_date = "",
//                read_cycle = "Cycle03",
//                rate_code = "binTierM",
//                service_point_id = "SP0700",
//                site_addressline1 = "Site_44",
//                site_addressline2 = "Site_LOL",
//                site_addressline3 = "Site_L",
//                site_city = "Site_KOP",
//                site_state = "Site_MH",
//                site_zip_code = "Site_416001",
//                meter_type = "ami",
//                meter_units = "1",
//                bldg_sq_foot = "",
//                year_built = "",
//                bedrooms = "",
//                assess_value = "510",
//                usage_value = "607",
//                bill_enddate = "2016-06-12T00:00:00",
//                bill_days = "22",
//                is_estimate = "",
//                usage_charge = "148",
//                service_commodity = "2",
//                account_structure_type = "A",
//                meter_id = "Meter0700",
//                billperiod_type = "1",
//                meter_replaces_meterid = "2",
//                service_read_date = "2016-06-15T00:00:00",
//                Service_contract = "SC0700",
//                Programs = "A"
//            };
//            var serializedEventData = JsonConvert.SerializeObject(record);
//            var eventData = new EventData(Encoding.UTF8.GetBytes(serializedEventData));
//            eventData.Properties.Add("EventType", 1);
//            eventData.Properties.Add("IntervalType", 30);
//            eventData.Properties.Add("ClientId", "87");
//            eventData.Properties.Add("ProcessingType", Enums.ProcessingType.File);
//            eventData.Properties.Add("Source", "S");
//            eventData.Properties.Add("RowIndex", 1);
//            var context = new PartitionContext();
//            var lease = new Lease();
//            context.Lease = lease;
//            context.Lease.Offset = "Lease";
//            context.Lease.PartitionId = "PartitionId";
//            EventProcessor.LogModel = new LogModel
//            {
//                DisableLog = false
//            };
//            var eventProc = new EventProcessor();

//            var eventDataList = new List<EventData> { eventData };
//            eventProc.OpenAsync(context);
//            var result = eventProc.ProcessEventsAsync(context, eventDataList.AsEnumerable());
//            Assert.AreEqual(result.Exception, null);
//        }

//        /// <summary>
//        /// Test Case for Subscription
//        /// </summary>
//        [TestMethod]
//        public void ProcessEventsAsync_Subscription()
//        {
//            var record = new RawSubscriptionModel
//            {
//                ClientId = "87",
//                CustomerId = "C0700",
//                AccountId = "Accn0700",
//                ProgramName = "Program2",
//                InsightTypeName = "ServiceLevelUsageThreshold",
//                ServiceContractId = "SC0700",
//                IsEmailOptInCompleted = "",
//                IsSmsOptInCompleted = "TRUE",
//                IsSelected = "True",
//                Channel = "Email",
//                SubscriberThresholdValue = "300"
//            };
//            var serializedEventData = JsonConvert.SerializeObject(record);
//            var eventData = new EventData(Encoding.UTF8.GetBytes(serializedEventData));
//            eventData.Properties.Add("EventType", 1);
//            eventData.Properties.Add("IntervalType", 30);
//            eventData.Properties.Add("ClientId", "87");
//            eventData.Properties.Add("ProcessingType", Enums.ProcessingType.File);
//            eventData.Properties.Add("Source", "S");
//            eventData.Properties.Add("RowIndex", 1);
//            var context = new PartitionContext();
//            var lease = new Lease();
//            context.Lease = lease;
//            context.Lease.Offset = "Lease";
//            context.Lease.PartitionId = "PartitionId";
//            EventProcessor.LogModel = new LogModel
//            {
//                DisableLog = false
//            };
//            var eventProc = new EventProcessor();

//            var eventDataList = new List<EventData> { eventData };
//            eventProc.OpenAsync(context);
//            var result = eventProc.ProcessEventsAsync(context, eventDataList.AsEnumerable());
//            Assert.AreEqual(result.Exception, null);
//        }

//        /// <summary>
//        /// Test Case for Subscription SendSms
//        /// </summary>
//        [TestMethod]
//        public void ProcessEventsAsync_Subscription_SendSMS()
//        {
//            var subscriptionEntity = new SubscriptionEntity
//            {
//                ClientId = 87,
//                CustomerId = "500",
//                AccountId = "Ac9898",
//                InsightTypeName = "Insight",
//                ServiceContractId = "SC56",
//                ProgramName = "Program1",
//                IsEmailOptInCompleted = true,
//                IsSmsOptInCompleted = true,
//                IsSelected = true,
//                Channel = "A",
//                SubscriberThresholdValue = 200,
//                UomId = 1,
//            };
//            var clientSetting = new ClientSettings
//            {
//                OptInSmsConfirmationTemplateId = "Conf45T"
//            };
//            var customerModel = new CustomerModel
//            {
//                TrumpiaSubscriptionId = "TrimpId"
//            };
//            var subPorcevent = new SubscriptionProcessEvent(new LogModel())
//            {
//                UnityContainer = new UnityContainer()
//            };
//            // ReSharper disable once UnusedVariable
//            var result = subPorcevent.SendSms(subscriptionEntity, clientSetting, customerModel);
//            Assert.AreEqual(true, true);
//        }

//        /// <summary>
//        /// Test Case for Subscription SendEmails
//        /// </summary>
//        [TestMethod]
//        public void ProcessEventsAsync_Subscription_SendMail()
//        {
//            var subscriptionEntity = new SubscriptionEntity
//            {
//                ClientId = 87,
//                CustomerId = "500",
//                AccountId = "Ac9898",
//                InsightTypeName = "Insight",
//                ServiceContractId = "SC56",
//                SubscriptionDate = DateTime.Now,
//                ProgramName = "Program1"
//            };
//            var clientSetting = new ClientSettings
//            {
//                OptInEmailUserName = "UserName",
//                OptInEmailPassword = "Password",
//                OptInEmailFrom = "From@et.com",
//                UnmaskedAccountIdEndingDigit = 1,
//                OptInEmailConfirmationTemplateId = "24"
//            };
//            var customerModel = new CustomerModel
//            {
//                TrumpiaSubscriptionId = "TrimpId",
//                EmailAddress = "sample@t.com",
//                AddressLine3 = "Address3",
//                AddressLine2 = "AddressLine2",
//                AddressLine1 = "AddressLine1"
//            };
//            var subPorcevent = new SubscriptionProcessEvent(new LogModel())
//            {
//                UnityContainer = new UnityContainer()
//            };
//            subPorcevent.SendMail(subscriptionEntity, clientSetting, customerModel);
//            Assert.AreEqual(true, true);
//        }

//        /// <summary>
//        /// Test Case for AccountUpdate
//        /// </summary>
//        [TestMethod]
//        public void ProcessEventsAsync_AccountUpdate()
//        {
//            var record = new RawAccountUpdatesModel
//            {
//                ClientId = "87",
//                CustomerId = "C0700",
//                AccountId = "Accn0700",
//                PremiseId = "112",
//                ServiceContractId = "SC0700",
//                MeterId = "52",
//                UpdateType = "A",
//                OldValue = "B",
//                NewValue = "C",
//                UpdateSentDate = ""
//            };
//            var serializedEventData = JsonConvert.SerializeObject(record);
//            var eventData = new EventData(Encoding.UTF8.GetBytes(serializedEventData));
//            eventData.Properties.Add("EventType", 1);
//            eventData.Properties.Add("IntervalType", 30);
//            eventData.Properties.Add("ClientId", "87");
//            eventData.Properties.Add("ProcessingType", Enums.ProcessingType.File);
//            eventData.Properties.Add("Source", "S");
//            eventData.Properties.Add("RowIndex", 1);
//            var context = new PartitionContext();
//            var lease = new Lease();
//            context.Lease = lease;
//            context.Lease.Offset = "Lease";
//            context.Lease.PartitionId = "PartitionId";
//            EventProcessor.LogModel = new LogModel
//            {
//                DisableLog = false
//            };
//            var eventProc = new EventProcessor();

//            var eventDataList = new List<EventData> { eventData };
//            eventProc.OpenAsync(context);
//            var result = eventProc.ProcessEventsAsync(context, eventDataList.AsEnumerable());
//            Assert.AreEqual(result.Exception, null);
//        }

//        /// <summary>
//        /// Test Case for NccAmiReading
//        /// </summary>
//        [TestMethod]
//        public void ProcessEventsAsync_NccAmiReading()
//        {
//            var record = new RawNccAmiReadingModel
//            {
//                METER_ID = "1",
//                TRANSPONDER_ID = "2",
//                TRANSPONDER_PORT = "3",
//                CUSTOMER_ID = "4",
//                READING_VALUE = "5",
//                UNIT_OF_MEASURE = "A",
//                READING_DATETIME = "12",
//                TIMEZONE = "IST",
//                BATTERY_VOLTAGE = "5",
//                COMMODITY_ID = "6"
//            };
//            var serializedEventData = JsonConvert.SerializeObject(record);
//            var eventData = new EventData(Encoding.UTF8.GetBytes(serializedEventData));
//            eventData.Properties.Add("EventType", 1);
//            eventData.Properties.Add("IntervalType", 30);
//            eventData.Properties.Add("ClientId", "87");
//            eventData.Properties.Add("ProcessingType", Enums.ProcessingType.File);
//            eventData.Properties.Add("Source", "S");
//            eventData.Properties.Add("RowIndex", 1);
//            var context = new PartitionContext();
//            var lease = new Lease();
//            context.Lease = lease;
//            context.Lease.Offset = "Lease";
//            context.Lease.PartitionId = "PartitionId";
//            EventProcessor.LogModel = new LogModel
//            {
//                DisableLog = false
//            };
//            var eventProc = new EventProcessor();

//            var eventDataList = new List<EventData> { eventData };
//            eventProc.OpenAsync(context);
//            var result = eventProc.ProcessEventsAsync(context, eventDataList.AsEnumerable());
//            Assert.AreEqual(result.Exception, null);
//        }

//        /// <summary>
//        /// Test Case for NccAmiCalculations
//        /// </summary>
//        [TestMethod]
//        public void ProcessEventsAsync_NccAmiCalculations()
//        {
//            var record = new RawNccAmiReadingModel
//            {
//                METER_ID = "1",
//                TRANSPONDER_ID = "2",
//                TRANSPONDER_PORT = "3",
//                CUSTOMER_ID = "4",
//                READING_VALUE = "5",
//                UNIT_OF_MEASURE = "A",
//                READING_DATETIME = "12",
//                TIMEZONE = "IST",
//                BATTERY_VOLTAGE = "5",
//                COMMODITY_ID = "6"
//            };
//            var serializedEventData = JsonConvert.SerializeObject(record);
//            var eventData = new EventData(Encoding.UTF8.GetBytes(serializedEventData));
//            eventData.Properties.Add("EventType", 1);
//            eventData.Properties.Add("IntervalType", 30);
//            eventData.Properties.Add("ClientId", "87");
//            eventData.Properties.Add("ProcessingType", Enums.ProcessingType.File);
//            eventData.Properties.Add("Source", "S");
//            eventData.Properties.Add("RowIndex", 1);
//            var context = new PartitionContext();
//            var lease = new Lease();
//            context.Lease = lease;
//            context.Lease.Offset = "Lease";
//            context.Lease.PartitionId = "PartitionId";
//            EventProcessor.LogModel = new LogModel
//            {
//                DisableLog = false
//            };
//            var eventProc = new EventProcessor();

//            var eventDataList = new List<EventData> { eventData };
//            eventProc.OpenAsync(context);
//            var result = eventProc.ProcessEventsAsync(context, eventDataList.AsEnumerable());
//            Assert.AreEqual(result.Exception, null);
//        }

//        /// <summary>
//        /// Test Case for NccAmiConsumption
//        /// </summary>
//        [TestMethod]
//        public void ProcessEventsAsync_NccAmiConsumption()
//        {
//            var record = new RawNccAmiReadingModel
//            {
//                METER_ID = "1",
//                TRANSPONDER_ID = "2",
//                TRANSPONDER_PORT = "3",
//                CUSTOMER_ID = "4",
//                READING_VALUE = "5",
//                UNIT_OF_MEASURE = "A",
//                READING_DATETIME = "12",
//                TIMEZONE = "IST",
//                BATTERY_VOLTAGE = "5",
//                COMMODITY_ID = "6"
//            };
//            var serializedEventData = JsonConvert.SerializeObject(record);
//            var eventData = new EventData(Encoding.UTF8.GetBytes(serializedEventData));
//            eventData.Properties.Add("EventType", 1);
//            eventData.Properties.Add("IntervalType", 30);
//            eventData.Properties.Add("ClientId", "87");
//            eventData.Properties.Add("ProcessingType", Enums.ProcessingType.File);
//            eventData.Properties.Add("Source", "S");
//            eventData.Properties.Add("RowIndex", 1);
//            var context = new PartitionContext();
//            var lease = new Lease();
//            context.Lease = lease;
//            context.Lease.Offset = "Lease";
//            context.Lease.PartitionId = "PartitionId";
//            EventProcessor.LogModel = new LogModel
//            {
//                DisableLog = false
//            };
//            var eventProc = new EventProcessor();

//            var eventDataList = new List<EventData> { eventData };
//            eventProc.OpenAsync(context);
//            var result = eventProc.ProcessEventsAsync(context, eventDataList.AsEnumerable());
//            Assert.AreEqual(result.Exception, null);
//        }

//        /// <summary>
//        /// Test Case for CloseAsync Method
//        /// </summary>
//        [TestMethod]
//        public void CloseAsync()
//        {
//            var context = new PartitionContext();
//            var lease = new Lease();
//            context.Lease = lease;
//            context.Lease.Offset = "Lease";
//            context.Lease.PartitionId = "PartitionId";
//            var eventProc = new EventProcessor();
//            eventProc.OpenAsync(context);
//            var task = eventProc.CloseAsync(context, CloseReason.Shutdown);
//            // ReSharper disable once UnusedVariable
//            var taskId = task.Id.ToString();
//            Assert.AreEqual(true, true);
//        }

//        /// <summary>
//        /// Cleanup Method To Release Log4Net Data Resources hold by Any Test Method
//        /// </summary>
//        [TestCleanup]
//        public void CleanUpMethod()
//        {
//            CallContext.FreeNamedDataSlot("log4net.Util.LogicalThreadContextProperties");
//        }
//    }
//}
