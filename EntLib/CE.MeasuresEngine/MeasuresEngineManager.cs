﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text.RegularExpressions;
using CE.BillDisagg;
using CE.ContentModel;
using CE.ContentModel.Entities;
using CE.EnergyModel;
using CE.MeasuresEngine.DataAccess;
using NCalc;

namespace CE.MeasuresEngine
{
    public class MeasuresEngineManager : IMeasuresEngine
    {
        public static string ExceptionMessageAppSettingsReadError { get; } = "Could not read from app settings.";
        private const string ConditionFormulaTemplate = "if([P:{0}] {1} {2},1,0)";
        private const string InConditionFormulaTemplate = "in([P:{0}], {2})";
        private const string NotInConditionFormulaTemplate = "notin([P:{0}], {2})";
        private const string MatchPa = @"\[P:(.*?)\]"; //*P*rofile attribute
        private const string MatchCa = @"\[F:(.*?)\]"; //calculation *F*actor
        private readonly int _clientId;
        private readonly string _overrideConnStringInsightsDw = string.Empty;
        private readonly string _overrideConnStringInsightsMetadata = string.Empty;
        private readonly string _overrideConnStringInsights = string.Empty;
        private readonly bool _overridingConnStrings;

        public DataAccessLayer AccessLayer { get; private set; }

        private IContentProvider _contentProvider;
        private MeasuresConfiguration _config;
        private const int CacheTimeoutInMinutes = 20;

        public MeasuresConfiguration Configuration => _config;

        public MeasuresEngineManager(int clientId)
        {
            _clientId = clientId;
            InitDataAccess();
            InitContentProvider();
            LoadConfiguration();
        }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="clientId">The client Id</param>
        /// <param name="csInsightsDw">Connection string to InsightsDW</param>
        /// <param name="csInsightsMetadata">Connection string to InsightsMetadata</param>
        /// <param name="csInsights">Connection string to Insights</param>
        public MeasuresEngineManager(int clientId, string csInsightsDw, string csInsightsMetadata, string csInsights)
        {
            _clientId = clientId;
            InitDataAccess(csInsights);
            InitContentProvider(csInsightsMetadata);
            _overridingConnStrings = true;
            _overrideConnStringInsightsDw = csInsightsDw;
            _overrideConnStringInsightsMetadata = csInsightsMetadata;
            _overrideConnStringInsights = csInsights;
            LoadConfiguration();
        }

        /// <summary>
        /// Execute the logic to read and create measures (aka. Action Savings)
        /// </summary>
        /// <param name="customerId">The Customer Id</param>
        /// <param name="accountId">The Account Id</param>
        /// <param name="premiseId">The Premise Id</param>
        /// <returns>A Measures Result</returns>
        public MeasuresResult ExecuteMeasures(string customerId, string accountId, string premiseId)
        {
            var measuresInputParams = new MeasuresInputParams
            {
                CustomerId = customerId,
                AccountId = accountId,
                PremiseId = premiseId
            };

            return MainExecuteMeasures(measuresInputParams);
        }


        /// <summary>
        /// Execute the logic to read and create measures (aka. Action Savings)
        /// </summary>
        /// <param name="inputParams">Measures Input Parameters</param>
        /// <returns>A Measures Result</returns>
        public MeasuresResult ExecuteMeasures(MeasuresInputParams inputParams)
        {
            return MainExecuteMeasures(inputParams);
        }


        /// <summary>
        /// Main private function that contains logic to read actions setup in content and actually determine measures.
        /// </summary>
        /// <param name="inputParams">Measures Input Parameters</param>
        /// <returns>A Measures Result</returns>
        private MeasuresResult MainExecuteMeasures(MeasuresInputParams inputParams)
        {
            EnergyModelResult emResult;
            IEnergyModel emmgr = null;

            var result = new MeasuresResult
            {
                CustomerId = inputParams.CustomerId,
                AccountId = inputParams.AccountId,
                PremiseId = inputParams.PremiseId
            };

            // MAIN ENERGY MODEL EXECUTION HERE
            // if no energy model provided, then execute the model for the provided customer, since the initial EM result is useful for many inputs
            if (inputParams.EMResultAsInput == null)
            {
                var factory = new EnergyModelManagerFactory();
                if (!_overridingConnStrings)
                {
                    emmgr = factory.CreateEnergyModelManager(_clientId);
                }
                else
                {
                    emmgr = factory.CreateEnergyModelManager(_clientId, _overrideConnStringInsightsDw,
                        _overrideConnStringInsightsMetadata, _overrideConnStringInsights);
                }

                var idParams = new IDParams
                {
                    CustomerId = inputParams.CustomerId,
                    AccountId = inputParams.AccountId,
                    PremiseId = inputParams.PremiseId
                };

                emResult = emmgr.ExecuteEnergyModel(idParams);
            }
            else
            {
                emResult = inputParams.EMResultAsInput;
            }

            // MAIN BILL DISAGG EXECUTION HERE
            // Execute initial Bill Disagg using energy model as an input; note that this means the factory only has to use the simple clientid constructor below
            IBillDisagg bdmgr;

            var bdInputParams = new DisaggInputParams
            {
                CustomerId = inputParams.CustomerId,
                AccountId = inputParams.AccountId,
                PremiseId = inputParams.PremiseId,
                EMResultAsInput = emResult,
                Bills = inputParams.Bills //bills likely null most of the time
            };

            var bdf = new BillDisaggManagerFactory();

            if (!_overridingConnStrings)
            {
                bdmgr = bdf.CreateBillDisaggManager(_clientId);
            }
            else
            {
                bdmgr = bdf.CreateBillDisaggManager(_clientId, _overrideConnStringInsightsDw,
                    _overrideConnStringInsightsMetadata, _overrideConnStringInsights);
            }

            var bdResult = bdmgr.ExecuteBillDisagg(bdInputParams);


            // MEASURES WORK HERE (aka. Actions)
            var customerMeasures = DetermineCustomerMeasures(emResult.ProfileAttributes);

            // Calculate costs & rebates for the collection of measures
            CalculateCostsAndRebates(customerMeasures, emResult.ProfileAttributes);

            // Calculate savings for the measures, using different savings types
            CalculateSavings(customerMeasures, bdResult, emResult, emmgr, bdmgr);

            // Assign measures to the result
            result.Measures = customerMeasures;

            // Assign original bill disagg flat table result to measures output; some callers may use this
            result.BillDisaggFlatTableResult = bdResult.FlatResultTable;

            // Finally, create a DataTable for some callers to easily persist the results from Measures
            PopulateFlatTable(result, inputParams);

            return result;
        }

        /// <summary>
        /// Calculate Savings
        /// </summary>
        /// <param name="measures"></param>
        /// <param name="bdResult"></param>
        /// <param name="emResult"></param>
        /// <param name="emmgr"></param>
        /// <param name="bdmgr"></param>
        private void CalculateSavings(MeasureCollection measures, BillDisaggResult bdResult, EnergyModelResult emResult,
            IEnergyModel emmgr, IBillDisagg bdmgr)
        {
            foreach (var measure in measures)
            {

                if (!string.IsNullOrWhiteSpace(measure.SavingsCalcMethod))
                {
                    //FOUR methods to determine savings
                    switch (measure.SavingsCalcMethod.ToLower())
                    {
                        case "none":
                            measure.AnnualSavingsEstimate = 0.0m;
                            break;

                        case "fixed":
                            measure.AnnualSavingsEstimate = measure.SavingsAmount;
                            break;

                        case "percentage":
                            CalculatePercentSavingsFromAssociatedAppliances(measure, bdResult);
                            break;

                        case "whatif":
                            CalculateWhatIfSavings(measure, bdResult, emResult, emmgr, bdmgr);
                            break;
                    }

                }

                // TFS 4274 April 2016 
                // If max savings populated and calculated savings > max savings then use max savings.
                // If min savings populated and calculated savings < min savings then use min savings
                if (measure.MinimumSavingsAmount != 0.0m && measure.MaximumSavingsAmount != 0.0m)
                {
                    if (measure.AnnualSavingsEstimate > measure.MaximumSavingsAmount)
                    {
                        measure.AnnualSavingsEstimate = measure.MaximumSavingsAmount;
                    }
                    else if (measure.AnnualSavingsEstimate < measure.MinimumSavingsAmount)
                    {
                        measure.AnnualSavingsEstimate = measure.MinimumSavingsAmount;
                    }
                }
                else if (measure.MinimumSavingsAmount != 0.0m && measure.MaximumSavingsAmount == 0.0m)
                {
                    if (measure.AnnualSavingsEstimate < measure.MinimumSavingsAmount)
                    {
                        measure.AnnualSavingsEstimate = measure.MinimumSavingsAmount;
                    }
                }
                else if (measure.MinimumSavingsAmount == 0.0m && measure.MaximumSavingsAmount != 0.0m)
                {
                    if (measure.AnnualSavingsEstimate > measure.MaximumSavingsAmount)
                    {
                        measure.AnnualSavingsEstimate = measure.MaximumSavingsAmount;
                    }
                }

                // the annual savings, and payback in months
                if (measure.AnnualSavingsEstimate > 0.0m)
                {
                    measure.Roi = measure.AnnualSavingsEstimate*10 - measure.UpfrontCost;
                    measure.Payback = measure.UpfrontCost/measure.AnnualSavingsEstimate*12.0m;
                }

            }

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="m"></param>
        /// <param name="bdResult"></param>
        /// <param name="emResult"></param>
        /// <param name="emMgr"></param>
        /// <param name="bdMgr"></param>
        private void CalculateWhatIfSavings(Measure m, BillDisaggResult bdResult, EnergyModelResult emResult,
            IEnergyModel emMgr, IBillDisagg bdMgr)
        {
            if (!string.IsNullOrWhiteSpace(m.WhatIfKeys))
            {
                var pas = new ProfileAttributeCollection();

                var wiKeys = m.WhatIfKeys.Split(',').ToList();

                foreach (var wikey in wiKeys)
                {
                    var wi = _config.ClientWhatIfs[wikey];

                    if (wi == null)
                    {
                        continue;
                    }
                    var paKey = wi.ProfileAttributeKey;

                    var lookupKey = wi.ValueExpressionKey;
                    var theExpression = _config.ClientExpressions[lookupKey];

                    if (theExpression == null)
                    {
                        continue;
                    }

                    var paValue = theExpression.Formula;

                    // FIRST: A few attribute values are actually expressions that need evaluating
                    if (FormulaContainsExpressionWork(paValue))
                    {
                        // Process the value, since it is an expression
                        paValue = PreprocessFormula(theExpression.Formula, emResult.ProfileAttributes, true);

                        if (!paValue.Equals(theExpression.Formula, StringComparison.InvariantCultureIgnoreCase))
                        {
                            // set the expression to ignore case
                            var options = EvaluateOptions.IgnoreCase;
                            var evalResult = new Expression(paValue, options).Evaluate();

                            if (!(evalResult is string))
                            {
                                // the output is a number, set it as string to be treated as numeric later down in code to keep same code path
                                paValue = Convert.ToString(evalResult);
                            }
                        }
                    }

                    // SECOND: simpler attribute values can now be processed
                    paValue = paValue.Replace("'", "");
                    // replace all single quotes with no character, since some are authored with single quotes

                    // must make decisions on how the provided data is authored; Profile Attributes must be provided exactly right
                    decimal n;
                    var isNumeric = decimal.TryParse(paValue, out n);

                    string optionKey;
                    string optionValue;
                    if (isNumeric)
                    {
                        if (paKey.Contains(".value"))
                        {
                            optionKey = paKey;
                        }
                        else
                        {
                            optionKey = paKey + ".value";
                        }

                        optionValue = paValue;
                    }
                    else if (paValue.Contains(".yes"))
                    {
                        optionKey = paValue;
                        optionValue = "1";
                    }
                    else if (paValue.Contains(".no"))
                    {
                        optionKey = paValue;
                        optionValue = "0";
                    }
                    else
                    {
                        optionKey = paValue;

                        // need to lookup the value for the OptionKey
                        decimal value;
                        optionValue = _config.ProfileOptions.TryGetValue(optionKey, out value)
                            ? value.ToString(CultureInfo.InvariantCulture)
                            : "0";

                    }

                    var pa = new ProfileAttribute
                    {
                        Key = paKey,
                        OptionKey = optionKey,
                        OptionValue = optionValue,
                        Source = "U"
                    };

                    pas.Add(pa);
                }

                // call the energy model again here with the PA overrides!
                var regionParams = new RegionParams
                {
                    ApplianceRegionId = emResult.RegionParams.ApplianceRegionId,
                    TemperatureRegionId = emResult.RegionParams.TemperatureRegionId,
                    SolarRegionId = emResult.RegionParams.SolarRegionId,
                    State = emResult.RegionParams.State,
                    StationId = emResult.RegionParams.StationId,
                    ClimateId = emResult.RegionParams.ClimateId,
                    DefaultProfileName = emResult.RegionParams.DefaultProfileName
                };

                var idParams = new IDParams
                {
                    CustomerId = emResult.IDParams.CustomerId,
                    AccountId = emResult.IDParams.AccountId,
                    PremiseId = emResult.IDParams.PremiseId,
                    Zipcode = emResult.IDParams.Zipcode
                };

                // CORE SAVINGS LOGIC STARTS HERE FOR WHATIF
                var thisEmWhatIfResult = emMgr.ExecuteEnergyModel(regionParams, idParams, pas, null);
                var bdWhatIfResult = bdMgr.GenerateBillDisaggFromPreviousResults(bdResult, thisEmWhatIfResult);
                // thisEMWhatIfResult is modified!

                CalculateSavingsAsDeltaOfAppliances(m, bdResult, bdWhatIfResult); // this modifies the measure m
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="m"></param>
        /// <param name="bdResult"></param>
        private static void CalculatePercentSavingsFromAssociatedAppliances(Measure m, BillDisaggResult bdResult)
        {
            var totalApplicableCost = 0.0m;
            var totElecCost = 0.0m;
            var totGasCost = 0.0m;
            var totWaterCost = 0.0m;

            var gasUomKey = string.Empty;
            var waterUomKey = string.Empty;
            //var gasUom = EnergyModel.Enums.UnitOfMeasureType.Unspecified;
            //var waterUom = EnergyModel.Enums.UnitOfMeasureType.Unspecified;

            if (string.IsNullOrWhiteSpace(m.ApplianceKeys))
            {
                return;
            }
            var applianceKeys = m.ApplianceKeys.Split(',').ToList();

            // get the billdisagg result for each appliance, and sum the (cost * percent savings) for applicable commodities
            foreach (var key in applianceKeys)
            {
                var a = bdResult.Appliances.Find(p => p.Key == key.ToLower());

                if (a == null)
                {
                    continue;
                }

                foreach (var energyuse in a.EnergyUses)
                {
                    if (energyuse.CommodityKey == m.CommodityKey ||
                        m.CommodityKey.Equals("all", StringComparison.InvariantCultureIgnoreCase))
                    {
                        totalApplicableCost = totalApplicableCost + energyuse.Cost;

                        if (energyuse.CommodityKey.Equals("electric", StringComparison.InvariantCultureIgnoreCase))
                        {
                            totElecCost = totElecCost + energyuse.Cost;
                        }
                        if (energyuse.CommodityKey.Equals("gas", StringComparison.InvariantCultureIgnoreCase))
                        {
                            totGasCost = totGasCost + energyuse.Cost;
                            gasUomKey = energyuse.UomKey;
                        }
                        if (energyuse.CommodityKey.Equals("water", StringComparison.InvariantCultureIgnoreCase))
                        {
                            totWaterCost = totWaterCost + energyuse.Cost;
                            waterUomKey = energyuse.UomKey;
                        }

                    }
                }
            }

            // if there are no costs, leave the annual savings estimate unchanged in the measure (it is set to a default initially from content.)
            if (totalApplicableCost > 0.0m)
            {
                // this is the overall savings; m.SavingsAmount here is a percent authored with the action
                m.AnnualSavingsEstimate = totalApplicableCost*m.SavingsAmount;
            }

            // individual savings are added to details for commodities that applied
            if (totElecCost > 0.0m)
            {
                var md = new MeasureDetail
                {
                    CommodityKey = "electric",
                    SavingsEstimateCurrencyKey = "usd",
                    SavingsEstimate = totElecCost*m.SavingsAmount,
                    UsageSavingsEstimateUomKey = "kwh",
                    UsageSavingsEstimate = 0m, // There is no usage multiplier for this                     
                };
                m.MeasuresDetails.Add(md);
            }

            if (totGasCost > 0.0m)
            {
                var md = new MeasureDetail
                {
                    CommodityKey = "gas",
                    SavingsEstimateCurrencyKey = "usd",
                    SavingsEstimate = totGasCost*m.SavingsAmount,
                    UsageSavingsEstimateUomKey = gasUomKey,
                    UsageSavingsEstimate = 0m, // There is no usage multiplier for this                     
                };
                m.MeasuresDetails.Add(md);
            }

            if (totWaterCost > 0.0m)
            {
                var md = new MeasureDetail
                {
                    CommodityKey = "water",
                    SavingsEstimateCurrencyKey = "usd",
                    SavingsEstimate = totWaterCost*m.SavingsAmount,
                    UsageSavingsEstimateUomKey = waterUomKey,
                    UsageSavingsEstimate = 0m, // There is no usage multiplier for this                     
                };
                m.MeasuresDetails.Add(md);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="m"></param>
        /// <param name="bdResult"></param>
        /// <param name="bdWhatIfResult"></param>
        private static void CalculateSavingsAsDeltaOfAppliances(Measure m, BillDisaggResult bdResult,
            BillDisaggResult bdWhatIfResult)
        {
            var totalApplicableCost = 0.0m;
            var totElecCost = 0.0m;
            var totElecUsage = 0.0m;
            var totGasCost = 0.0m;
            var totGasUsage = 0.0m;
            var totWaterCost = 0.0m;
            var totWaterUsage = 0.0m;

            var totalWIApplicableCost = 0.0m;
            var totWIElecCost = 0.0m;
            var totWIElecUsage = 0.0m;
            var totWIGasCost = 0.0m;
            var totWIGasUsage = 0.0m;
            var totWIWaterCost = 0.0m;
            var totWIWaterUsage = 0.0m;

            var gasUomKey = string.Empty;
            var waterUomKey = string.Empty;
            //var gasUom = EnergyModel.Enums.UnitOfMeasureType.Unspecified;
            //var waterUom = EnergyModel.Enums.UnitOfMeasureType.Unspecified;

            if (!string.IsNullOrWhiteSpace(m.ApplianceKeys))
            {
                var applianceKeys = m.ApplianceKeys.Split(',').ToList();

                // get the billdisagg result for each appliance, and sum the (cost * percent savings) for applicable commodities
                foreach (var key in applianceKeys)
                {
                    // use primary bill disagg
                    var a = bdResult.Appliances.Find(p => p.Key == key.ToLower());

                    if (a != null)
                    {
                        foreach (var energyuse in a.EnergyUses)
                        {
                            if (energyuse.CommodityKey == m.CommodityKey || m.CommodityKey == "all")
                            {
                                totalApplicableCost = totalApplicableCost + energyuse.Cost;

                                if (energyuse.CommodityKey == "electric")
                                {
                                    totElecCost = totElecCost + energyuse.Cost;
                                    totElecUsage = totElecUsage + energyuse.Quantity;
                                }
                                if (energyuse.CommodityKey == "gas")
                                {
                                    totGasCost = totGasCost + energyuse.Cost;
                                    totGasUsage = totGasUsage + energyuse.Quantity;
                                    gasUomKey = energyuse.UomKey;
                                    //gasUom = energyuse.UnitOfMeasure;
                                }
                                if (energyuse.CommodityKey == "water")
                                {
                                    totWaterCost = totWaterCost + energyuse.Cost;
                                    totWaterUsage = totWaterUsage + energyuse.Quantity;
                                    waterUomKey = energyuse.UomKey;
                                    //waterUom = energyuse.UnitOfMeasure;
                                }

                            }
                        }
                    }

                    // use whatif bill disagg
                    a = bdWhatIfResult.Appliances.Find(p => p.Key == key.ToLower());

                    if (a != null)
                    {
                        foreach (var energyuse in a.EnergyUses)
                        {
                            if (energyuse.CommodityKey == m.CommodityKey || m.CommodityKey == "all")
                            {
                                totalWIApplicableCost = totalWIApplicableCost + energyuse.Cost;

                                if (energyuse.CommodityKey == "electric")
                                {
                                    totWIElecCost = totWIElecCost + energyuse.Cost;
                                    totWIElecUsage = totWIElecUsage + energyuse.Quantity;
                                }
                                if (energyuse.CommodityKey == "gas")
                                {
                                    totWIGasCost = totWIGasCost + energyuse.Cost;
                                    totWIGasUsage = totWIGasUsage + energyuse.Quantity;
                                    gasUomKey = energyuse.UomKey;
                                    //gasUom = energyuse.UnitOfMeasure;
                                }
                                if (energyuse.CommodityKey == "water")
                                {
                                    totWIWaterCost = totWIWaterCost + energyuse.Cost;
                                    totWIWaterUsage = totWIWaterUsage + energyuse.Quantity;
                                    waterUomKey = energyuse.UomKey;
                                    //waterUom = energyuse.UnitOfMeasure;
                                }

                            }
                        }
                    }
                }

                // if there are no costs, leave the annual savings estimate unchanged in the measure (it is set to a default initially from content.)
                if (totalApplicableCost > 0.0m && totalWIApplicableCost > 0.0m)
                {
                    // Overall savings is the difference (the delta);  negative savings means it was an increase in cost
                    m.AnnualSavingsEstimate = totalApplicableCost - totalWIApplicableCost;
                }

                // individual savings are added to details for commodities that applied
                if (totElecCost > 0.0m || totWIElecCost > 0.0m)
                {
                    var md = new MeasureDetail()
                    {
                        CommodityKey = "electric",
                        SavingsEstimateCurrencyKey = "usd",
                        SavingsEstimate = totElecCost - totWIElecCost,
                        UsageSavingsEstimateUomKey = "kwh",
                        UsageSavingsEstimate = totElecUsage - totWIElecUsage
                    };
                    m.MeasuresDetails.Add(md);
                }

                if (totGasCost > 0.0m || totWIGasCost > 0.0m)
                {
                    var md = new MeasureDetail()
                    {
                        CommodityKey = "gas",
                        SavingsEstimateCurrencyKey = "usd",
                        SavingsEstimate = totGasCost - totWIGasCost,
                        UsageSavingsEstimateUomKey = gasUomKey,
                        UsageSavingsEstimate = totGasUsage - totWIGasUsage
                    };
                    m.MeasuresDetails.Add(md);
                }

                if (totWaterCost > 0.0m || totWIWaterCost > 0.0m)
                {
                    var md = new MeasureDetail()
                    {
                        CommodityKey = "water",
                        SavingsEstimateCurrencyKey = "usd",
                        SavingsEstimate = totWaterCost - totWIWaterCost,
                        UsageSavingsEstimateUomKey = waterUomKey,
                        UsageSavingsEstimate = totWaterUsage - totWIWaterUsage
                    };
                    m.MeasuresDetails.Add(md);
                }

            }

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="measures"></param>
        /// <param name="pas"></param>
        private void CalculateCostsAndRebates(MeasureCollection measures, ProfileAttributeCollection pas)
        {

            // iterate the list of measures and get cost and rebate expressions from the root action
            foreach (var m in measures)
            {

                // process any COST EXPRESSIONS
                if (!string.IsNullOrWhiteSpace(m.CostExpression))
                {
                    var exp = _config.ClientExpressions[m.CostExpression];

                    if (exp != null)
                    {
                        try
                        {
                            var formula = exp.Formula;

                            // Preprocess formula to replace profile attribute values, and client factors if present
                            formula = PreprocessFormula(formula, pas, true);

                            // evaluate with NCalc
                            // set the expression to ignore case
                            var options = EvaluateOptions.IgnoreCase;
                            var evalResult = new Expression(formula, options).Evaluate();

                            if (!(evalResult is string))
                            {
                                var res = Convert.ToDecimal(evalResult);
                                m.UpfrontCost = res;
                            }
                        }
                        catch
                        {
                            //just skip
                        }
                    }


                }

                // process any REBATE EXPRESSIONS
                if (!string.IsNullOrWhiteSpace(m.RebateExpression))
                {
                    var exp = _config.ClientExpressions[m.RebateExpression];

                    if (exp != null)
                    {
                        try
                        {
                            var formula = exp.Formula;

                        // Preprocess formula to replace profile attribute values!  
                        // Use the same logic used for conditions when preprocessing profile attributes
                        formula = PreprocessFormula(formula, pas, true);

                        // evaluate with NCalc
                        // set the expression to ignore case
                        var options = EvaluateOptions.IgnoreCase;
                        var evalResult = new Expression(formula, options).Evaluate();

                        if (!(evalResult is string))
                        {
                            var res = Convert.ToDecimal(evalResult);
                            m.RebateAmount = res;
                            m.RebateAvailable = true;
                        }
                    }
                    catch
                    {
                        //just skip
                    }

                }

            }

            }


        }

        /// <summary>
        /// Determine the customers measures
        /// </summary>
        /// <param name="profileAttributes">A collection of profile attributes for the current user</param>
        /// <returns>A collection of measures for the current user</returns>
        private MeasureCollection DetermineCustomerMeasures(ProfileAttributeCollection profileAttributes)
        {
            var customerMeasures = new MeasureCollection();

            // iterate the list of actions available from the client
            foreach (var item in _config.ClientActions)
            {
                // for each action, evaluate the conditions, and if evaluation passes, add to the customer measures collection
                var action = item.Value;
                var rawFormula = string.Empty;
                var evaluatedFormula = string.Empty;

                if (IncludeMeasure(action, profileAttributes, ref rawFormula, ref evaluatedFormula))
                {
                    var measure = new Measure
                    {
                        Key = action.Key,
                        ActionType = action.Type,
                        AnnualCost = action.DefaultAnnualCost,
                        UpfrontCost = action.DefaultUpfrontCost,
                        AnnualCostVariancePercent = action.DefaultCostVariancePercent,
                        AnnualSavingsEstimate = action.DefaultAnnualSavingsEstimate,
                        AnnualSavingsEstimateCurrencyKey = "usd",
                        Priority = action.ActionPriority,
                        CostExpression = action.CostExpression,
                        RebateExpression = action.RebateExpression,
                        RebateAvailable = action.RebateAvailable,
                        RebateAmount = action.RebateAmount,
                        ConditionsRawFormula = rawFormula,
                        ConditionsEvaluatedFormula = evaluatedFormula,
                        SavingsCalcMethod = action.SavingsCalcMethod,
                        ApplianceKeys = action.ApplianceKeys,
                        WhatIfKeys = action.WhatIfDataKeys,
                        SavingsAmount = action.SavingsAmount,
                        CommodityKey = action.CommodityKey,
                        DifficultyKey = action.Difficulty,
                        HabitIntervalKey = action.HabitInterval,
                        MinimumSavingsAmount = action.MinimumSavingsAmt,
                        MaximumSavingsAmount = action.MaximumSavingsAmt
                    };

                    // Add the measure that had no conditions or met all conditions
                    customerMeasures.Add(measure);
                }

            }

            return customerMeasures;
        }

        /// <summary>
        /// Include Measure
        /// </summary>
        /// <param name="action"></param>
        /// <param name="pas"></param>
        /// <param name="rawFormula"></param>
        /// <param name="evaluatedFormula"></param>
        /// <returns></returns>
        private bool IncludeMeasure(ClientAction action, ProfileAttributeCollection pas, ref string rawFormula,
            ref string evaluatedFormula)
        {
            if (rawFormula == null)
            {
                throw new ArgumentNullException(nameof(rawFormula));
            }

            var include = false;
            var formula = string.Empty;
            var preFormula = string.Empty;

            var avoidedActionTypes = new List<string> { "tbd"};

            // The actions can be of several types; Avoid actuo
            if (!avoidedActionTypes.Contains(action.Type.ToLower()))
            {
                // evaluate the conditions of the action to see if all conditions are satisfied
                if (string.IsNullOrWhiteSpace(action.ConditionKeys))
                {
                    include = true;
                }
                else
                {
                    // all present conditions must evaluate to be included
                    var conditions = action.ConditionKeys.Split(',').ToList();
                    formula = string.Empty;
                    var count = 0;

                    foreach (var item in conditions)
                    {
                        //grab the condition from configuration and continue to build the logic expression
                        if (count > 0)
                        {
                            formula = formula + " && ";
                        }

                        // Errors have occured because of Caps versus small letters.
                        var condition = _config.ClientConditions.FirstOrDefault(m => m.Key.Equals(item, StringComparison.InvariantCultureIgnoreCase)).Value;

                        //only profile conditions are processed
                        if (condition.Category.Equals("profile", StringComparison.InvariantCultureIgnoreCase))
                        {
                            // Determine the formula to use.
                            if (condition.Operator.Equals("in", StringComparison.InvariantCultureIgnoreCase))
                            {
                                formula = formula + InConditionFormulaTemplate; // "in([P:{0}], {2})";
                            }
                            else if (condition.Operator.Equals("notin", StringComparison.InvariantCultureIgnoreCase))
                            {
                                formula = formula + NotInConditionFormulaTemplate; // "notin([P:{0}], {2})";
                            }
                            else
                            {
                                formula = formula + ConditionFormulaTemplate; //"if([P:{0}] {1} {2},1,0)";
                            }

                            // Determine the values for comparison.
                            if (condition.ProfileOptionKey.Contains(".value"))
                            {
                                formula = string.Format(formula, condition.ProfileAttributeKey,
                                    condition.Operator, condition.Value);
                            }
                            else if (condition.ProfileOptionKey.Contains(".no"))
                            {
                                formula = string.Format(formula, condition.ProfileAttributeKey,
                                    condition.Operator, -1); // special, set to -1 so that it will not match
                            }
                            else if (condition.ProfileOptionKey.Contains(".yes"))
                            {
                                formula = string.Format(formula, condition.ProfileAttributeKey,
                                    condition.Operator, 1);
                            }
                            else if (condition.ProfileOptionKey.Equals("dummyprofileoption",
                                StringComparison.InvariantCultureIgnoreCase))
                                // This handles Contentful ENUM values for "in" and "notin" operators
                            {
                                var value = condition.Value.Split(',')
                                    .Aggregate(string.Empty,
                                        (current, val) =>
                                            current + ",'" + condition.ProfileAttributeKey + "." + val + "'")
                                    .TrimStart(',');

                                formula = string.Format(formula, condition.ProfileAttributeKey,
                                    condition.Operator, value);
                            }
                            else
                            {
                                formula = string.Format(formula, condition.ProfileAttributeKey,
                                    condition.Operator,
                                    "'" + condition.ProfileOptionKey + "'");
                            }
                        }
                        else
                        {
                            formula = string.Empty;
                        }

                        count++;
                    }

                    // evaluate the formula if it exists
                    if (!string.IsNullOrWhiteSpace(formula))
                    {
                        // save the preFormula to send back to caller
                        preFormula = formula;

                        // preprocess formula to replace profile attribute values!
                        formula = PreprocessFormula(formula, pas, false);

                        // evaluate with NCalc
                        // set the expression to ignore case
                        const EvaluateOptions options = EvaluateOptions.IgnoreCase;
                        var evalResult = new Expression(formula, options).Evaluate();

                        if (!(evalResult is string))
                        {
                            var res = Convert.ToBoolean(Convert.ToInt32(evalResult));
                            include = res;
                        }

                    }
                }

            }

            rawFormula = preFormula;
            evaluatedFormula = formula;

            return include;
        }

        /// <summary>
        /// Pre-Process the Formula
        /// </summary>
        /// <param name="input"></param>
        /// <param name="pas"></param>
        /// <param name="processFactors"></param>
        /// <returns></returns>
        private string PreprocessFormula(string input, ProfileAttributeCollection pas, bool processFactors)
        {
            string output = input;
            string pattern;
            MatchCollection matches;

            //Find and replace all Profile attributes [P:xxxxxx]
            if (pas != null)
            {
                pattern = MatchPa;
                matches = Regex.Matches(output, pattern, RegexOptions.IgnoreCase);
                if (matches.Count > 0)
                {
                    foreach (Match match in matches)
                    {
                        var pa = pas.Find(p => p.Key == match.Groups[1].Value);

                        if (pa != null)
                        {
                            // special
                            if (pa.OptionKey.Contains(".value"))
                            {
                                decimal n;
                                var isNumeric = decimal.TryParse(pa.OptionValue, out n);

                                output = isNumeric
                                    ? output.Replace(match.Groups[0].Value, pa.OptionValue)
                                    : output.Replace(match.Groups[0].Value, "'" + pa.OptionValue + "'");
                            }
                            else if (pa.OptionKey.Contains(".no"))
                            {
                                output = output.Replace(match.Groups[0].Value, "0");
                            }
                            else if (pa.OptionKey.Contains(".yes"))
                            {
                                output = output.Replace(match.Groups[0].Value, "1");
                            }
                            else
                            {
                                output = output.Replace(match.Groups[0].Value, "'" + pa.OptionKey + "'");
                            }
                        }
                        else
                        {
                            // the profile attribute does not exist;  possible that there is no default attribute, which could happen with promo authoring
                            output = output.Replace(match.Groups[0].Value, "'unknown'");
                        }

                    }
                }
            }

            //Find and replace all client configurable calculation Factors [F:xxxxxx]
            if (processFactors)
            {
                pattern = MatchCa;
                matches = Regex.Matches(output, pattern, RegexOptions.IgnoreCase);
                if (matches.Count > 0)
                {
                    foreach (Match match in matches)
                    {
                        string ca;
                        if (_config.CalculationFactors.TryGetValue(match.Groups[1].Value, out ca))
                        {
                            output = output.Replace(match.Groups[0].Value, ca);
                        }

                    }
                }
            }

            return output;
        }

        /// <summary>
        /// Check to see if the Formula contains an Expression.
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        private static bool FormulaContainsExpressionWork(string input)
        {
            var isMatches = false;

            var paMatches = Regex.IsMatch(input, MatchPa, RegexOptions.IgnoreCase);
            var caMatches = Regex.IsMatch(input, MatchCa, RegexOptions.IgnoreCase);

            if (paMatches || caMatches)
            {
                isMatches = true;
            }

            return isMatches;
        }


        /// <summary>
        /// Create a flattened data table representation of appliance billdisagg data for this result.
        /// </summary>
        /// <param name="result"></param>
        /// <param name="ip"></param>
        private void PopulateFlatTable(MeasuresResult result, MeasuresInputParams ip)
        {
            var df = new DataFactory();
            var table = df.CreateMeasureDataTable();

            //iterate measures and create a data table (the measure details are tbd)
            foreach (var m in result.Measures)
            {
                decimal? elecSavEst = null;
                decimal? elecUsgSavEst = null;
                string elecUsgSavEstUomKey = null;
                string elecSavEstCurrencyKey = null;
                decimal? gasSavEst = null;
                decimal? gasUsgSavEst = null;
                string gasUsgSavEstUomKey = null;
                string gasSavEstCurrencyKey = null;
                decimal? waterSavEst = null;
                decimal? waterUsgSavEst = null;
                string waterUsgSavEstUomKey = null;
                string waterSavEstCurrencyKey = null;

                foreach (var d in m.MeasuresDetails)
                {
                    switch (d.CommodityKey)
                    {

                        case "electric":
                            elecSavEst = d.SavingsEstimate;
                            elecSavEstCurrencyKey = d.SavingsEstimateCurrencyKey;
                            elecUsgSavEst = d.UsageSavingsEstimate;
                            elecUsgSavEstUomKey = d.UsageSavingsEstimateUomKey;
                            break;

                        case "gas":
                            gasSavEst = d.SavingsEstimate;
                            gasSavEstCurrencyKey = d.SavingsEstimateCurrencyKey;
                            gasUsgSavEst = d.UsageSavingsEstimate;
                            gasUsgSavEstUomKey = d.UsageSavingsEstimateUomKey;
                            break;

                        case "water":
                            waterSavEst = d.SavingsEstimate;
                            waterSavEstCurrencyKey = d.SavingsEstimateCurrencyKey;
                            waterUsgSavEst = d.UsageSavingsEstimate;
                            waterUsgSavEstUomKey = d.UsageSavingsEstimateUomKey;
                            break;
                    }

                }

                table.Rows.Add(_clientId,
                    ip.CustomerId,
                    ip.AccountId,
                    ip.PremiseId,
                    m.Key,
                    m.ActionType,
                    m.AnnualCost,
                    m.AnnualCostVariancePercent,
                    m.AnnualSavingsEstimate,
                    m.AnnualSavingsEstimateCurrencyKey,
                    m.Roi,
                    m.Payback, m.UpfrontCost,
                    m.CommodityKey,
                    elecSavEst,
                    elecSavEstCurrencyKey,
                    elecUsgSavEst,
                    elecUsgSavEstUomKey,
                    gasSavEst,
                    gasSavEstCurrencyKey,
                    gasUsgSavEst,
                    gasUsgSavEstUomKey,
                    waterSavEst,
                    waterSavEstCurrencyKey,
                    waterUsgSavEst,
                    waterUsgSavEstUomKey,
                    m.Priority,
                    DateTime.UtcNow,
                    m.RebateAmount); // 15.12 Bug 993 - set up rebate amount for flat table

            }

            result.FlatResultTable = table;
        }



        /// <summary>
        /// Data Access init.
        /// </summary>
        private void InitDataAccess()
        {
            AccessLayer = new DataAccessLayer();
        }


        /// <summary>
        /// Data Access Init with connection string overrides.
        /// </summary>
        /// <param name="csInsights"></param>
        private void InitDataAccess(string csInsights)
        {
            AccessLayer = new DataAccessLayer(csInsights);
        }


        /// <summary>
        /// Initialize content provider to be used to populate default attributes, appliances, and client calc factors, when necessary.
        /// </summary>
        private void InitContentProvider(string csInsightsMetadata)
        {
            // init the content provider
            var factory = new ContentModelFactoryContentful();
            _contentProvider = factory.CreateContentProvider(_clientId, ContentProviderType.Contentful,
                csInsightsMetadata);
        }

        /// <summary>
        /// Initialize content provider to be used to populate action information.
        /// </summary>
        private void InitContentProvider()
        {
            // init the content provider
            var factory = new ContentModelFactoryContentful();
            _contentProvider = factory.CreateContentProvider(_clientId);
        }

        /// <summary>
        /// Load the Measure Configuration
        /// </summary>
        private void LoadConfiguration()
        {
            if (_config != null)
            {
                return;
            }

            var cacheKey = "measuresConfig-" + _clientId;

            var cacheItem = EnergyModel.Cache.CacheLayer.Get<MeasuresConfiguration>(cacheKey);

            if (cacheItem == null)
            {
                _config = new MeasuresConfiguration();

                // ACTIONS
                var actionList = _contentProvider.GetContentAction(string.Empty);

                foreach (var item in actionList)
                {
                    _config.ClientActions.Add(item.Key, item);
                }

                //CONDITIONS
                var conditions = _contentProvider.GetContentCondition(string.Empty, string.Empty);

                foreach (var item in conditions)
                {
                    _config.ClientConditions.Add(item.Key, item);
                }

                //WHATIFS
                var whatifs = _contentProvider.GetContentWhatIfData(string.Empty);

                foreach (var item in whatifs)
                {
                    _config.ClientWhatIfs.Add(item.Key, item);
                }

                // EXPRESSIONS
                var expressions = _contentProvider.GetContentExpression(string.Empty);

                foreach (var item in expressions)
                {
                    _config.ClientExpressions.Add(item.Key, item);
                }

                //ENDUSES (for filtering, used by external configuration needs)
                var enduses = _contentProvider.GetContentEnduse(string.Empty);

                foreach (var item in enduses)
                {
                    if (item.Key != "enduse.house") //exclude the placeholder for house
                    {
                        var key = item.Key.Replace("enduse.", ""); //strip the prefix!
                        item.Key = key;
                        _config.ClientEndUses.Add(key, item);
                    }
                }

                // CALCULATION FACTORS (category of energy model, but used by other callers)
                var factorList = _contentProvider.GetContentConfiguration("energymodel", string.Empty);

                foreach (var item in factorList)
                {
                    // an errant category is getting into list, sanity check here
                    if (item.Category.Equals("energymodel", StringComparison.InvariantCultureIgnoreCase))
                    {
                        _config.CalculationFactors.Add(item.Key, item.Value);
                    }
                }

                // PROFILE OPTIONS, used by whatif logic
                var optionList = _contentProvider.GetContentProfileOption(string.Empty);

                foreach (var item in optionList)
                {
                    // limit the items by exclusing those that are essentially boolean or straight-up value; only want lists like (ex: something.usage.rarely, something.usage.always, etc.)
                    if (!item.Key.EndsWith(".no") && !item.Key.EndsWith(".yes") && !item.Key.EndsWith(".value"))
                    {
                        _config.ProfileOptions.Add(item.Key, item.ProfileOptionValue);
                    }
                }


                // ** ASSIGN the configuration to the cache **
                BillDisagg.Cache.CacheLayer.Add(_config, cacheKey, CacheTimeoutInMinutes);

            }
            else
            {
                _config = cacheItem;
            }
        }


    }

}
