﻿
namespace CE.MeasuresEngine
{
    public class Measure
    {
        public string Key { get; set; }
        public decimal AnnualCost { get; set; }
        public decimal UpfrontCost { get; set; }
        public decimal AnnualCostVariancePercent { get; set; }
        public decimal AnnualSavingsEstimate { get; set; }
        public string AnnualSavingsEstimateCurrencyKey { get; set; }
        public MeasureDetailCollection MeasuresDetails { get; set; }
        public string ActionType { get; set; }
        public int Priority { get; set; }
        public decimal Roi { get; set; }                            // 10 year savings
        public decimal Payback { get; set; }                        // how many months for savings to equal cost  

        public string ConditionsRawFormula { get; set; }            //set as an output for possible debugging
        public string ConditionsEvaluatedFormula { get; set; }      //set as an output for possible debugging   


        public string CostExpression { get; set; }                  // from original action
        public string RebateExpression { get; set; }                // from original action
        public bool RebateAvailable { get; set; }                   // from original action
        public decimal RebateAmount { get; set; }                   // from original action
        public string ApplianceKeys { get; set; }                   // from original action, use this for percentage savings when doing savings calculation
        public string WhatIfKeys { get; set; }                      // from original action, use this for the more advanced Energy Model and partial Bill Disagg to calc savings
        public string SavingsCalcMethod { get; set; }               // from original action, use this for savings method
        public decimal SavingsAmount { get; set; }                  // from original action, use this for savings amount (fixed or percentage)

        public string CommodityKey { get; set; }                    // from original action
        public string DifficultyKey { get; set; }                   // from original action
        public string HabitIntervalKey { get; set; }                // from original action
        public decimal MinimumSavingsAmount { get; set; }
        public decimal MaximumSavingsAmount { get; set; }

        public Measure()
        {
            MeasuresDetails = new MeasureDetailCollection();
        }

    }
}
