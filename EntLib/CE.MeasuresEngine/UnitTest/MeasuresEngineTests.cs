﻿using CE.BillDisagg;
using CE.ContentModel;
using CE.MeasuresEngine;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Data;
using System.Diagnostics;

namespace UnitTest
{
    [TestClass]
    public class MeasuresEngineTests
    {
        [TestInitialize]
        public void TestInit()
        {
            // This initializes the automapper!
            new AutoMapperBootStrapper().BootStrap();
        }

        [TestCleanup]
        public void TestClean()
        {
            //noop
        }

        [TestMethod]
        public void Content_Sandbox()
        {
            IContentProvider cp;

            int clientId = 256;

            var cf = new ContentModelFactoryContentful();
            cp = cf.CreateContentProvider(clientId);

            // ACTIONS
            var actionList = cp.GetContentAction(string.Empty);

            foreach (var item in actionList)
            {

            }

            // EXPRESSIONS
            var expressions = cp.GetContentExpression(string.Empty);

            foreach (var item in expressions)
            {

            }

            //CONDITIONS (ClientCondition)
            var conditions = cp.GetContentCondition(string.Empty, string.Empty);

            foreach (var item in conditions)
            {

            }

            //WHATIFS
            var whatifs = cp.GetContentWhatIfData(string.Empty);

            foreach (var item in whatifs)
            {

            }


        }

        [TestMethod]
        public void Measures_Execute_PassConnStrs()
        {
            MeasuresEngineManagerFactory factory = null;
            IMeasuresEngine manager = null;
            MeasuresResult result2 = null;

            // NOTE that the direct use of connection string requires the clientid specific database (use _Test for 87 and 256); 
            int clientId = 256;
            string customerId = "GUL47191";
            string accountId = "ZUW26449";
            string premiseId = "RIV9449";
            string csMeta = @"Server=tcp:acedsql1c0.database.windows.net,1433;Database=InsightsMetaData;User ID=AclaraCEAdmin@acedsql1c0;Password=Acl@r@282;Encrypt=True;TrustServerCertificate=False;Connection Timeout=30;";
            string csDW = @"Server=tcp:acedsql1c0.database.windows.net,1433;Database=InsightsDW_Test;User ID=AclaraCEAdmin@acedsql1c0;Password=Acl@r@282;Encrypt=True;TrustServerCertificate=False;Connection Timeout=30;";

            factory = new MeasuresEngineManagerFactory();
            manager = factory.CreateMeasuresEngineManager(clientId, csDW, csMeta, string.Empty);    //purposely leave out the Insights String so it only uses DW for PAs

            result2 = manager.ExecuteMeasures(customerId, accountId, premiseId);

            //attempt save (test save and retrieve)
            //var dal = new CE.MeasuresEngine.DataAccess.DataAccessLayer();
            //dal.SaveMeasures(clientId, customerId, accountId, premiseId, result.FlatResultTable);
            //var dt = dal.GetMeasures(clientId, customerId, accountId, premiseId, false);

        }

        [TestMethod]
        public void Measures_Execute_PassConnStrs_101()
        {
            MeasuresEngineManagerFactory factory = null;
            IMeasuresEngine manager = null;
            MeasuresResult result2 = null;

            // NOTE that the direct use of connection string requires the clientid specific database (use _Test for 87 and 256); 
            int clientId = 101;
            string customerId = "100001460";
            string accountId = "144208945";
            string premiseId = "144208900";
            string csMeta = @"Server=tcp:acedsql1c0.database.windows.net,1433;Database=InsightsMetaData;User ID=AclaraCEAdmin@acedsql1c0;Password=Acl@r@282;Encrypt=True;TrustServerCertificate=False;Connection Timeout=30;";
            string csDW = @"Server=tcp:acedsql1c0.database.windows.net,1433;Database=InsightsDW_101;User ID=AclaraCEAdmin@acedsql1c0;Password=Acl@r@282;Encrypt=True;TrustServerCertificate=False;Connection Timeout=30;";

            factory = new MeasuresEngineManagerFactory();
            manager = factory.CreateMeasuresEngineManager(clientId, csDW, csMeta, string.Empty);    //purposely leave out the Insights String so it only uses DW for PAs

            result2 = manager.ExecuteMeasures(customerId, accountId, premiseId);

            foreach (DataRow row in result2.FlatResultTable.Rows)
            {
                Debug.WriteLine(string.Join(",", row.ItemArray));
            }
        }

        [TestMethod]
        public void Measures_Execute_PassConnStrs_224()
        {
            MeasuresEngineManagerFactory factory = null;
            IMeasuresEngine manager = null;
            MeasuresResult result2 = null;

            // NOTE that the direct use of connection string requires the clientid specific database (use _Test for 87 and 256); 
            int clientId = 224;
            string customerId = "01000000392224";
            string accountId = "01000000392224";
            string premiseId = "1220002337020";
            string csMeta = @"Server=tcp:aceusql1c0.database.windows.net,1433;Database=InsightsMetaData;User ID=AclaraCEAdmin@aceusql1c0;Password=Acl@r@282;Encrypt=True;TrustServerCertificate=False;Connection Timeout=30;";
            string csDW = @"Server=tcp:aceusql1c0.database.windows.net,1433;Database=InsightsDW_224;User ID=AclaraCEAdmin@aceusql1c0;Password=Acl@r@282;Encrypt=True;TrustServerCertificate=False;Connection Timeout=30;";

            factory = new MeasuresEngineManagerFactory();
            manager = factory.CreateMeasuresEngineManager(clientId, csDW, csMeta, string.Empty);    //purposely leave out the Insights String so it only uses DW for PAs

            result2 = manager.ExecuteMeasures(customerId, accountId, premiseId);

            foreach (DataRow row in result2.FlatResultTable.Rows)
            {
                Debug.WriteLine(string.Join(",", row.ItemArray));
            }
        }

        [TestMethod]
        public void Measures_Execute_PassConnStrs_224_BUG_3658()
        {
            MeasuresEngineManagerFactory factory = null;
            IMeasuresEngine manager = null;
            MeasuresResult result2 = null;

            // NOTE that the direct use of connection string requires the clientid specific database (use _Test for 87 and 256); 
            int clientId = 224;
            string customerId = "01000005533400";
            string accountId = "01000005533400";
            string premiseId = "00000003000000006327";
            string csMeta = @"Server=tcp:acepsql1c0.database.windows.net,1433;Database=InsightsMetaData;User ID=AclaraCEAdmin@acepsql1c0;Password=0S3att!30;Encrypt=True;TrustServerCertificate=False;Connection Timeout=30;Min Pool Size=5;";
            string csDW = @"Server=tcp:acepsql1c0.database.windows.net,1433;Database=InsightsDW_224;User ID=AclaraCEAdmin@acepsql1c0;Password=0S3att!30;Encrypt=True;TrustServerCertificate=False;Connection Timeout=30;Min Pool Size=5;";

            factory = new MeasuresEngineManagerFactory();
            manager = factory.CreateMeasuresEngineManager(clientId, csDW, csMeta, string.Empty);    //purposely leave out the Insights String so it only uses DW for PAs

            result2 = manager.ExecuteMeasures(customerId, accountId, premiseId);

            foreach (DataRow row in result2.FlatResultTable.Rows)
            {
                Debug.WriteLine(string.Join(",", row.ItemArray));
            }
        }

        [TestMethod]
        public void Measures_Execute_101Test()
        {
            MeasuresEngineManagerFactory factory = null;
            IMeasuresEngine manager = null;
            MeasuresResult result = null;

            int clientId = 101;
            string customerId = "100001460";
            string accountId = "144208945";
            string premiseId = "144208900";

            factory = new MeasuresEngineManagerFactory();
            manager = factory.CreateMeasuresEngineManager(clientId);

            result = manager.ExecuteMeasures(customerId, accountId, premiseId);

            foreach (DataRow row in result.FlatResultTable.Rows)
            {
                Debug.WriteLine(string.Join(",", row.ItemArray));
            }

        }

        [TestMethod]
        public void Measures_Execute_276Test()
        {
            MeasuresEngineManagerFactory factory = null;
            IMeasuresEngine manager = null;
            MeasuresResult result = null;

            int clientId = 276;
            string customerId = "993774751";
            string accountId = "1218337093";
            string premiseId = "1218337000";

            factory = new MeasuresEngineManagerFactory();
            manager = factory.CreateMeasuresEngineManager(clientId);

            result = manager.ExecuteMeasures(customerId, accountId, premiseId);

            foreach (DataRow row in result.FlatResultTable.Rows)
            {
                Debug.WriteLine(string.Join(",", row.ItemArray));
            }

            //attempt save (test save and retrieve)
            //var dal = new CE.MeasuresEngine.DataAccess.DataAccessLayer();
            //dal.SaveMeasures(clientId, customerId, accountId, premiseId, result.FlatResultTable);
            //var dt = dal.GetMeasures(clientId, customerId, accountId, premiseId, false);

        }

        [TestMethod]
        public void Measures_Execute_224Test()
        {
            MeasuresEngineManagerFactory factory = null;
            IMeasuresEngine manager = null;
            MeasuresResult result = null;

            int clientId = 224;     // NOTE THAT THIS IS FOR A 224 test for UIL
            string customerId = "01000001880979";
            string accountId = "01000001880979";
            string premiseId = "1160002960020";

            factory = new MeasuresEngineManagerFactory();
            manager = factory.CreateMeasuresEngineManager(clientId);

            result = manager.ExecuteMeasures(customerId, accountId, premiseId);

            foreach (DataRow row in result.FlatResultTable.Rows)
            {
                Debug.WriteLine(string.Join(",", row.ItemArray));
            }

            //attempt save (test save and retrieve)
            //var dal = new CE.MeasuresEngine.DataAccess.DataAccessLayer();
            //dal.SaveMeasures(clientId, customerId, accountId, premiseId, result.FlatResultTable);
            //var dt = dal.GetMeasures(clientId, customerId, accountId, premiseId, false);

        }




        [TestMethod]
        public void Measures_BillDisagg_Compare_Test1()
        {
            BillDisaggManagerFactory bdf = null;
            IBillDisagg bdmgr = null;
            BillDisaggResult bdresult = null;

            MeasuresEngineManagerFactory factory = null;
            IMeasuresEngine manager = null;
            MeasuresResult result = null;

            int clientId = 256;
            string customerId = "GUL47191";
            string accountId = "ZUW26449";
            string premiseId = "RIV9449";

            bdf = new BillDisaggManagerFactory();
            bdmgr = bdf.CreateBillDisaggManager(clientId);
            bdresult = bdmgr.ExecuteBillDisagg(customerId, accountId, premiseId);

            factory = new MeasuresEngineManagerFactory();
            manager = factory.CreateMeasuresEngineManager(clientId);
            result = manager.ExecuteMeasures(customerId, accountId, premiseId);

            // are these equal; direct and via measures calc


            foreach (DataRow row in bdresult.FlatResultTable.Rows)
            {
                Debug.WriteLine(string.Join(",", row.ItemArray));
            }

            Debug.WriteLine("********************************************************");
            Debug.WriteLine("********************************************************");
            Debug.WriteLine("********************************************************");

            foreach (DataRow row in result.BillDisaggFlatTableResult.Rows)
            {
                Debug.WriteLine(string.Join(",", row.ItemArray));
            }


            Assert.IsTrue(IsTableSame(bdresult.FlatResultTable, result.BillDisaggFlatTableResult));


        }



        [TestMethod]
        public void Measures_Execute_87Test()
        {
            MeasuresEngineManagerFactory factory = null;
            IMeasuresEngine manager = null;
            MeasuresResult result = null;

            int clientId = 87;
            string customerId = "993774751";
            string accountId = "1218337093";
            string premiseId = "1218337000";

            factory = new MeasuresEngineManagerFactory();
            manager = factory.CreateMeasuresEngineManager(clientId);

            result = manager.ExecuteMeasures(customerId, accountId, premiseId);

            foreach (DataRow row in result.FlatResultTable.Rows)
            {
                Debug.WriteLine(string.Join(",", row.ItemArray));
            }

            //attempt save (test save and retrieve)
            //var dal = new CE.MeasuresEngine.DataAccess.DataAccessLayer();
            //dal.SaveMeasures(clientId, customerId, accountId, premiseId, result.FlatResultTable);
            //var dt = dal.GetMeasures(clientId, customerId, accountId, premiseId, false);

        }

        [TestMethod]
        public void BusMeasures_Execute_87Test1()
        {
            MeasuresEngineManagerFactory factory = null;
            IMeasuresEngine manager = null;
            MeasuresResult result = null;

            int clientId = 87;
            string customerId = "busbakery1";
            string accountId = "busbakery1";
            string premiseId = "busbakery1";

            factory = new MeasuresEngineManagerFactory();
            manager = factory.CreateMeasuresEngineManager(clientId);

            result = manager.ExecuteMeasures(customerId, accountId, premiseId);

            foreach (DataRow row in result.FlatResultTable.Rows)
            {
                Debug.WriteLine(string.Join(",", row.ItemArray));
            }

            //attempt save (test save and retrieve)
            //var dal = new CE.MeasuresEngine.DataAccess.DataAccessLayer();
            //dal.SaveMeasures(clientId, customerId, accountId, premiseId, result.FlatResultTable);
            //var dt = dal.GetMeasures(clientId, customerId, accountId, premiseId, false);

        }




        [TestMethod]
        public void Measures_Execute_DW_101_Test1()
        {
            MeasuresEngineManagerFactory factory = null;
            IMeasuresEngine manager = null;
            MeasuresResult result = null;

            int clientId = 101;
            string customerId = "100001460";
            string accountId = "144208945";
            string premiseId = "144208900";

            factory = new MeasuresEngineManagerFactory();

            var csMeta = @"Data Source=AZUDSQL001.stg.aclarace.com;Initial Catalog=InsightsMetadata;Persist Security Info=True;User ID=cesqluser;Password=Acl@r@694;Packet Size=32767;";
            var csDW = @"Data Source=AZUDSQL001.stg.aclarace.com;Initial Catalog=InsightsDW;Persist Security Info=True;User ID=cesqluser;Password=Acl@r@694;Packet Size=32767;";
            //var csInsights = ConfigurationManager.ConnectionStrings["InsightsConn"].ConnectionString;

            manager = factory.CreateMeasuresEngineManager(clientId, csDW, csMeta, string.Empty);    //purposely leave out the Insights String so it only uses DW for PAs

            result = manager.ExecuteMeasures(customerId, accountId, premiseId);

            foreach (DataRow row in result.FlatResultTable.Rows)
            {
                Debug.WriteLine(string.Join(",", row.ItemArray));
            }

            //attempt save (test save and retrieve)
            //var dal = new CE.MeasuresEngine.DataAccess.DataAccessLayer();
            //dal.SaveMeasures(clientId, customerId, accountId, premiseId, result.FlatResultTable);
            //var dt = dal.GetMeasures(clientId, customerId, accountId, premiseId, false);

        }


        private bool IsTableSame(DataTable table1, DataTable table2)
        {
            if (table1 == null)
                return (false);

            if (table2 == null)
                return (false);

            if (table1.Rows.Count != table2.Rows.Count)
                return (false);

            if (table1.Columns.Count != table2.Columns.Count)
                return (false);

            // counts are same, do a row by row comparison
            for (var j = 0; j < table1.Rows.Count; j++)
            {
                var array1 = table1.Rows[j].ItemArray;
                var array2 = table2.Rows[j].ItemArray;

                // loop all but last item, since it is a datestamp that changes
                for (var i = 0; i < array1.Length - 1; i++)
                {
                    if (!array1[i].Equals(array2[i]))
                    {
                        return (false);
                    }
                }

            }

            return (true);
        }

        [TestMethod]
        public void BusMeasures_Execute_87Test()
        {
            MeasuresEngineManagerFactory factory = null;
            IMeasuresEngine manager = null;
            MeasuresResult result = null;

            int clientId = 87;
            string customerId = "busbakery1";
            string accountId = "busbakery1";
            string premiseId = "busbakery1";

            factory = new MeasuresEngineManagerFactory();
            manager = factory.CreateMeasuresEngineManager(clientId);

            result = manager.ExecuteMeasures(customerId, accountId, premiseId);

            foreach (DataRow row in result.FlatResultTable.Rows)
            {
                Debug.WriteLine(string.Join(",", row.ItemArray));
            }

            //attempt save (test save and retrieve)
            //var dal = new CE.MeasuresEngine.DataAccess.DataAccessLayer();
            //dal.SaveMeasures(clientId, customerId, accountId, premiseId, result.FlatResultTable);
            //var dt = dal.GetMeasures(clientId, customerId, accountId, premiseId, false);

        }




    }
}
