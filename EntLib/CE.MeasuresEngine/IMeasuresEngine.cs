﻿
namespace CE.MeasuresEngine
{
    public interface IMeasuresEngine
    {
        MeasuresConfiguration Configuration { get; }
        MeasuresResult ExecuteMeasures(string customerId, string accountId, string premiseId);
        MeasuresResult ExecuteMeasures(MeasuresInputParams inputParams);

    }
}
