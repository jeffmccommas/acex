﻿
namespace CE.MeasuresEngine
{
    public class MeasuresEngineManagerFactory
    {
        /// <summary>
        /// Create manager that implements IMeasuresEngine. 
        /// </summary>
        /// <returns></returns>
        public IMeasuresEngine CreateMeasuresEngineManager(int clientId)
        {
            return new MeasuresEngineManager(clientId);
        }

        /// <summary>
        /// Create manager that implements IMeasuresEngine, passing in connection strings directly for special callers.
        /// </summary>
        /// <returns></returns>
        public IMeasuresEngine CreateMeasuresEngineManager(int clientId, string connStringInsightsDW,
            string connStringInsightsMetadata, string connStringInsights)
        {
            return new MeasuresEngineManager(clientId, connStringInsightsDW, connStringInsightsMetadata,
                connStringInsights);
        }

    }
}
