﻿using System;
using System.Data;

namespace CE.MeasuresEngine
{
    public class DataFactory
    {

        public DataFactory()
        {

        }

        /// <summary>
        /// Create a table for Measure data with specific named columns that match TVP and Table. 
        /// </summary>
        /// <returns></returns>
        public DataTable CreateMeasureDataTable()
        {
            var table = new DataTable();

            table.Columns.Add("ClientId", typeof(int));         //PK
            table.Columns.Add("CustomerId", typeof(string));    //PK
            table.Columns.Add("AccountId", typeof(string));     //PK
            table.Columns.Add("PremiseId", typeof(string));     //PK
            table.Columns.Add("ActionKey", typeof(string));
            table.Columns.Add("ActionTypeKey", typeof(string));
            table.Columns.Add("AnnualCost", typeof(decimal));
            table.Columns.Add("AnnualCostVariancePercent", typeof(decimal));
            table.Columns.Add("AnnualSavingsEstimate", typeof(decimal));
            table.Columns.Add("AnnualSavingsEstimateCurrencyKey", typeof(string));
            table.Columns.Add("Roi", typeof(decimal));
            table.Columns.Add("Payback", typeof(decimal));
            table.Columns.Add("UpfrontCost", typeof(decimal));
            table.Columns.Add("CommodityKey", typeof(string));
            table.Columns.Add("ElecSavEst", typeof(decimal));
            table.Columns.Add("ElecSavEstCurrencyKey", typeof(string));
            table.Columns.Add("ElecUsgSavEst", typeof(decimal));
            table.Columns.Add("ElecUsgSavEstUomKey", typeof(string));
            table.Columns.Add("GasSavEst", typeof(decimal));
            table.Columns.Add("GasSavEstCurrencyKey", typeof(string));
            table.Columns.Add("GasUsgSavEst", typeof(decimal));
            table.Columns.Add("GasUsgSavEstUomKey", typeof(string));
            table.Columns.Add("WaterSavEst", typeof(decimal));
            table.Columns.Add("WaterSavEstCurrencyKey", typeof(string));
            table.Columns.Add("WaterUsgSavEst", typeof(decimal));
            table.Columns.Add("WaterUsgSavEstUomKey", typeof(string));
            table.Columns.Add("Priority", typeof(int));
            table.Columns.Add("NewDate", typeof(DateTime));
            table.Columns.Add("RebateAmount", typeof(decimal)); // 15.12 Bug 993 - set up rebate amount for measure table

            return (table);
        }

    }

}
