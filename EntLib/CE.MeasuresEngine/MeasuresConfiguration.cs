﻿using System.Collections.Generic;
using CE.ContentModel.Entities;

namespace CE.MeasuresEngine
{
    public class MeasuresConfiguration
    {
        public Dictionary<string, string> CalculationFactors { get; }

        public Dictionary<string, ClientAction> ClientActions { get; }

        public Dictionary<string, ClientCondition> ClientConditions { get; }

        public Dictionary<string, ClientWhatIfData> ClientWhatIfs { get; }

        public Dictionary<string, ClientExpression> ClientExpressions { get; }

        public Dictionary<string, ClientEndUse> ClientEndUses { get; }

        public Dictionary<string, decimal> ProfileOptions { get; }

        public MeasuresConfiguration()
        {
            ClientActions = new Dictionary<string, ClientAction>();
            ClientConditions = new Dictionary<string, ClientCondition>();
            ClientWhatIfs = new Dictionary<string, ClientWhatIfData>();
            ClientExpressions = new Dictionary<string, ClientExpression>();
            ClientEndUses = new Dictionary<string, ClientEndUse>();
            CalculationFactors = new Dictionary<string, string>();
            ProfileOptions = new Dictionary<string, decimal>();
        }

    }
}
