﻿using System.Data;

namespace CE.MeasuresEngine
{
    public class MeasuresResult
    {
        public string CustomerId { get; set; }

        public string AccountId { get; set; }

        public string PremiseId { get; set; }

        public MeasureCollection Measures { get; set; }

        public DataTable FlatResultTable { get; set; }

        public DataTable BillDisaggFlatTableResult { get; set; }        // the initial bill disagg result is also sent out here, so it can be saved to database by some callers.


    }
}
