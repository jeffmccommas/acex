﻿using System.Collections.Generic;
using CE.BillDisagg;
using CE.EnergyModel;

namespace CE.MeasuresEngine
{
    public class MeasuresInputParams
    {
        /// <summary>
        /// CustomerId is required.
        /// </summary>
        public string CustomerId { get; set; }

        /// <summary>
        /// AccountId is required.
        /// </summary>
        public string AccountId { get; set; }

        /// <summary>
        /// PremiseId is required.
        /// </summary>
        public string PremiseId { get; set; }

        /// <summary>
        /// Bills are optional.  Used with EM API for testing.
        /// </summary>
        public List<Bill> Bills { get; set; }

        /// <summary>
        /// Energy Model result as input is optional.  Used with EM API for testing.
        /// </summary>
        public EnergyModelResult EMResultAsInput { get; set; }

        public MeasuresInputParams()
        {
            Bills = null;
            EMResultAsInput = null;
        }

    }
}
