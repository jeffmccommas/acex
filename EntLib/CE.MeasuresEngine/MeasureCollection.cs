﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace CE.MeasuresEngine
{
    public class MeasureCollection : Collection<Measure>
    {

        public MeasureCollection()
            : base(new List<Measure>())
        {

        }
        /*
        public Measure Find(Predicate<Measure> match)
        {
            List<Measure> items = (List<Measure>)Items;
            return (items.Find(match));
        }

        public bool Exists(Predicate<Measure> match)
        {
            List<Measure> items = (List<Measure>)Items;
            return (items.Exists(match));
        }

        public void AddRange(MeasureCollection measures)
        {
            List<Measure> items = (List<Measure>)Items;
            items.AddRange(measures);
        }
        */
    }
}
