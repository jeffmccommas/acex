﻿using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;

namespace CE.MeasuresEngine.DataAccess
{
    public class DataAccessLayer
    {
        private const string InsightsConnName = "InsightsConn";
        private readonly string _insightsConnString;

        public DataAccessLayer()
        {
            _insightsConnString = ConfigurationManager.ConnectionStrings[InsightsConnName].ConnectionString;
        }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="csInsights">Connection string to InsightsDW</param>
        public DataAccessLayer(string csInsights)
        {
            _insightsConnString = csInsights;
        }

        /// <summary>
        /// Get the connection string to InsightsDW
        /// </summary>
        /// <returns>The connection string</returns>
        private string GetConnectionStringInsights()
        {
            return _insightsConnString;
        }


        /// <summary>
        /// Get the measures for a specific customer and account by client and premise
        /// </summary>
        /// <param name="clientId">The client id</param>
        /// <param name="customerId">The customer id</param>
        /// <param name="accountId">The account id</param>
        /// <param name="premiseId">The premise id</param>
        /// <param name="saveToInsights">True, if want to save to insights.</param>
        /// <returns></returns>
        public DataTable GetMeasures(int clientId, string customerId, string accountId, string premiseId, bool saveToInsights)
        {
            DataTable measuresTable = null;

            const string storedProcedureNameInsights = "[wh].[uspMEGetMeasures]";      // within Insights

            if (string.IsNullOrEmpty(GetConnectionStringInsights()))
            {
                return measuresTable;
            }

            using (var connection = new SqlConnection(GetConnectionStringInsights()))
            {
                using (var cmd = new SqlCommand(storedProcedureNameInsights, connection))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@ClientId", typeof(int)) { Value = clientId });
                    cmd.Parameters.Add(new SqlParameter("@CustomerId", typeof(string)) { Value = customerId });
                    cmd.Parameters.Add(new SqlParameter("@AccountId", typeof(string)) { Value = accountId });
                    cmd.Parameters.Add(new SqlParameter("@PremiseId", typeof(string)) { Value = premiseId });

                    connection.Open();

                    using (var reader = cmd.ExecuteReader())
                    {
                        if (!reader.HasRows)
                        {
                            return measuresTable;
                        }

                        var df = new DataFactory();
                        measuresTable = df.CreateMeasureDataTable();

                        // User with client & user properties
                        while (reader.Read())
                        {
                            var actionKey = reader["ActionKey"] as string;
                            var actionTypeKey = reader["ActionTypeKey"] as string;
                            decimal? annualCost = reader["AnnualCost"] as decimal? ?? default(decimal);
                            decimal? annualCostVariancePercent = reader["AnnualCostVariancePercent"] as decimal? ?? default(decimal);
                            decimal? annualSavingsEstimate = reader["AnnualSavingsEstimate"] as decimal? ?? default(decimal);
                            var annualSavingsEstimateCurrencyKey = reader["AnnualSavingsEstimateCurrencyKey"] as string;
                            decimal? roi = reader["Roi"] as decimal? ?? default(decimal);
                            decimal? payback = reader["Payback"] as decimal? ?? default(decimal);
                            decimal? upfrontCost = reader["UpfrontCost"] as decimal? ?? default(decimal);
                            var commodityKey = reader["CommodityKey"] as string;

                            decimal? elecSavEst = reader["ElecSavEst"] as decimal? ?? default(decimal);
                            var elecSavEstCurrencyKey = reader["ElecSavEstCurrencyKey"] as string;
                            decimal? elecUsgSavEst = reader["ElecUsgSavEst"] as decimal? ?? default(decimal);
                            var elecUsgSavEstUomKey = reader["ElecUsgSavEstUomKey"] as string;

                            decimal? gasSavEst = reader["GasSavEst"] as decimal? ?? default(decimal);
                            var gasSavEstCurrencyKey = reader["GasSavEstCurrencyKey"] as string;
                            decimal? gasUsgSavEst = reader["GasUsgSavEst"] as decimal? ?? default(decimal);
                            var gasUsgSavEstUomKey = reader["GasUsgSavEstUomKey"] as string;

                            decimal? waterSavEst = reader["WaterSavEst"] as decimal? ?? default(decimal);
                            var waterSavEstCurrencyKey = reader["WaterSavEstCurrencyKey"] as string;
                            decimal? waterUsgSavEst = reader["WaterUsgSavEst"] as decimal? ?? default(decimal);
                            var waterUsgSavEstUomKey = reader["WaterUsgSavEstUomKey"] as string;

                            int? priority = reader["Priority"] as int? ?? default(int);
                            DateTime? newDate = reader["NewDate"] as DateTime? ?? default(DateTime);
                            decimal? rebateAmount = reader["RebateAmount"] as decimal? ?? default(decimal); // 15.12 Bug 993 - rebate amount

                            measuresTable.Rows.Add(clientId,
                                customerId,
                                accountId,
                                premiseId,
                                actionKey,
                                actionTypeKey,
                                annualCost,
                                annualCostVariancePercent,
                                annualSavingsEstimate,
                                annualSavingsEstimateCurrencyKey,
                                roi,
                                payback,
                                upfrontCost,
                                commodityKey,

                                elecSavEst,
                                elecSavEstCurrencyKey,
                                elecUsgSavEst,
                                elecUsgSavEstUomKey,

                                gasSavEst,
                                gasSavEstCurrencyKey,
                                gasUsgSavEst,
                                gasUsgSavEstUomKey,

                                waterSavEst,
                                waterSavEstCurrencyKey,
                                waterUsgSavEst,
                                waterUsgSavEstUomKey,
                                priority,
                                newDate,
                                rebateAmount);

                        }
                    }
                }
            }
            return measuresTable;
        }

        /// <summary>
        /// Save the measures to the database
        /// </summary>
        /// <param name="clientId">The client id</param>
        /// <param name="customerId">The customer id</param>
        /// <param name="accountId">The account id</param>
        /// <param name="premiseId">The premise id</param>
        /// <param name="measuresTable">The datatable holding the current measures.</param>
        /// <returns>True, if measures saved</returns>
        public bool SaveMeasures(int clientId, string customerId, string accountId, string premiseId, DataTable measuresTable)
        {

            if (measuresTable == null)
            {
                return true;
            }

            const string storedProcedureName = "[wh].[uspMESaveMeasures]";

            using (var connection = new SqlConnection(GetConnectionStringInsights()))
            {
                using (var cmd = new SqlCommand(storedProcedureName, connection))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@ClientID", typeof(int)) { Value = clientId });
                    cmd.Parameters.Add(new SqlParameter("@CustomerID", typeof(string)) { Value = customerId });
                    cmd.Parameters.Add(new SqlParameter("@AccountID", typeof(string)) { Value = accountId });
                    cmd.Parameters.Add(new SqlParameter("@PremiseID", typeof(string)) { Value = premiseId });
                    cmd.Parameters.Add(new SqlParameter("@MeasureTable", SqlDbType.Structured) { Value = measuresTable, TypeName = "wh.MeasureTableType" });

                    connection.Open();
                    cmd.ExecuteNonQuery();
                }
            }

            return true;
        }


/*
        /// <summary>
        /// Quick conversion of List to datatable.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="data"></param>
        /// <returns></returns>
        private DataTable ConvertToDataTable<T>(IEnumerable<T> data)
        {
            var properties = TypeDescriptor.GetProperties(typeof(T));
            var table = new DataTable();

            foreach (PropertyDescriptor prop in properties)
            {
                table.Columns.Add(prop.Name, Nullable.GetUnderlyingType(prop.PropertyType) ?? prop.PropertyType);
            }

            foreach (var item in data)
            {
                var row = table.NewRow();
                foreach (PropertyDescriptor prop in properties)
                {
                    row[prop.Name] = prop.GetValue(item) ?? DBNull.Value;
                }

                table.Rows.Add(row);
            }

            return table;
        }
*/



    }

}
