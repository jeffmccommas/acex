﻿
namespace CE.MeasuresEngine
{
    public class MeasureDetail
    {
        public string CommodityKey { get; set; }
        public decimal SavingsEstimate { get; set; }
        public string SavingsEstimateCurrencyKey { get; set; }
        public decimal UsageSavingsEstimate { get; set; }
        public string UsageSavingsEstimateUomKey { get; set; }

        public MeasureDetail()
        {

        }

    }
}
