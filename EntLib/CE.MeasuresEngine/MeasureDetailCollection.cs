﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace CE.MeasuresEngine
{
    public class MeasureDetailCollection : Collection<MeasureDetail>
    {

        public MeasureDetailCollection()
            : base(new List<MeasureDetail>())
        {

        }
        /*
        public MeasureDetail Find(Predicate<MeasureDetail> match)
        {
            List<MeasureDetail> items = (List<MeasureDetail>)Items;
            return (items.Find(match));
        }

        public bool Exists(Predicate<MeasureDetail> match)
        {
            List<MeasureDetail> items = (List<MeasureDetail>)Items;
            return (items.Exists(match));
        }
        */

    }
}
