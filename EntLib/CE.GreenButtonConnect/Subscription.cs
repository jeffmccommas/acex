﻿using System.Collections.Generic;

namespace CE.GreenButtonConnect
{
    public class Subscription : GreenButtonConnectBase
    {
        public List<UsagePointEntry> UsagePoints { get; set; }
        public ApplicationInformationEntry ApplicationInformation { get; set; }
        public List<AuthorizationEntry> Authorizations { get; set; }
    }
}
