﻿using System;
using System.Collections.Generic;
using System.Xml.Serialization;

namespace CE.GreenButtonConnect
{
    public class IntervalBlockEntry : GreenButtonConnectBase
    {
        public int UsagePointId { get; set; }
        public int ReadingTypeId { get; set; }
        public string MeterId { get; set; }
        public IntervalBlock IntervalBlock { get; set; }
    }
    [Serializable]
    [XmlType(Namespace = "http://naesb.org/espi")]
    [XmlRoot(Namespace = "http://naesb.org/espi", IsNullable = false)]
    public class IntervalBlock
    {
        [XmlElement]
        // ReSharper disable once InconsistentNaming
        public DateTimeInterval interval { get; set; }
        [XmlElement]
        public List<IntervalReading> IntervalReading { get; set; }
    }

        
    [Serializable]
    public class IntervalReading
    {
        // ReSharper disable once InconsistentNaming
        public long? cost { get; set; }
        [XmlIgnore]
        // ReSharper disable once InconsistentNaming
        public bool costSpecified => cost != null;
        // ReSharper disable once InconsistentNaming
        public DateTimeInterval timePeriod { get; set; }

        // ReSharper disable once InconsistentNaming
        public long value { get; set; }

    }
    

    public class DateTimeInterval
    {
        // ReSharper disable once InconsistentNaming
        public long? duration { get; set; }
        [XmlIgnore]
        // ReSharper disable once InconsistentNaming
        public bool durationSpecified => duration != null;
        // ReSharper disable once InconsistentNaming
        public long? start { get; set; }
        [XmlIgnore]
        // ReSharper disable once InconsistentNaming
        public bool startSpecified => start != null;
    }
    
}
