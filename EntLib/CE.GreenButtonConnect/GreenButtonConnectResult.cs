﻿using System.IO;

namespace CE.GreenButtonConnect
{
    public class GreenButtonConnectResult
    {
        public string FileName { get; set; }
        public MemoryStream ZipStream { get; set; }
        public string GreenButtonXml { get; set; }
    }
}
