﻿using System;
using System.Xml.Serialization;

namespace CE.GreenButtonConnect
{
    public class ReadingTypeEntry : GreenButtonConnectBase
    {
        public ReadingType ReadingType { get; set; }
    }

    [Serializable]
    [XmlType(Namespace = "http://naesb.org/espi")]
    [XmlRoot(Namespace = "http://naesb.org/espi", IsNullable = false)]
    public class ReadingType
    {
        // ReSharper disable once InconsistentNaming
        public int accumulationBehaviour { get; set; }
        // ReSharper disable once InconsistentNaming
        public int commodity { get; set; }
        // ReSharper disable once InconsistentNaming
        public int? consumptionTier { get; set; }
        [XmlIgnore]
        // ReSharper disable once InconsistentNaming
        public bool consumptionTierSpecified => consumptionTier != null;
        // ReSharper disable once InconsistentNaming
        public int? currency { get; set; }
        [XmlIgnore]
        // ReSharper disable once InconsistentNaming
        public bool currencySpecified => currency != null;
        // ReSharper disable once InconsistentNaming
        public int dataQualifier { get; set; }
        // ReSharper disable once InconsistentNaming
        public int? DefaultQuality { get; set; }
        [XmlIgnore]
        // ReSharper disable once InconsistentNaming
        public bool DefaultQualitySpecified => DefaultQuality != null;
        // ReSharper disable once InconsistentNaming
        public int flowDirection { get; set; }
        // ReSharper disable once InconsistentNaming
        public int intervalLength { get; set; }
        // ReSharper disable once InconsistentNaming
        public int kind { get; set; }
        // ReSharper disable once InconsistentNaming
        public int phase { get; set; }
        // ReSharper disable once InconsistentNaming
        public int powerOfTenMultiplier { get; set; }
        // ReSharper disable once InconsistentNaming
        public int timeAttribute { get; set; }
        // ReSharper disable once InconsistentNaming
        public int? tou { get; set; }
        [XmlIgnore]
        // ReSharper disable once InconsistentNaming
        public bool touSpecified => tou != null;
        // ReSharper disable once InconsistentNaming
        public int uom { get; set; }
        // ReSharper disable once InconsistentNaming
        public int? aggregate { get; set; }
        [XmlIgnore]
        // ReSharper disable once InconsistentNaming
        public bool aggregateSpecified => aggregate != null;
        // ReSharper disable once InconsistentNaming
        public RationalNumber argument { get; set; }
        [XmlIgnore]
        // ReSharper disable once InconsistentNaming
        public bool argumentSpecified => argument != null && argument.denominatorSpecified && argument.numeratorSpecified;
        // ReSharper disable once InconsistentNaming
        public int? cpp { get; set; }
        [XmlIgnore]
        // ReSharper disable once InconsistentNaming
        public bool cppSpecified => cpp != null;
        // ReSharper disable once InconsistentNaming
        public ReadingInterharmonic interharmonic { get; set; }
        [XmlIgnore]
        // ReSharper disable once InconsistentNaming
        public bool interharmonicSpecified => interharmonic != null && interharmonic.numeratorSpecified && interharmonic.denominatorSpecified;
        // ReSharper disable once InconsistentNaming
        public int? measuringPeriod { get; set; }
        [XmlIgnore]
        // ReSharper disable once InconsistentNaming
        public bool measuringPeriodSpecified => measuringPeriod != null;
    }

    public class ReadingInterharmonic
    {
        // ReSharper disable once InconsistentNaming
        public decimal? denominator { get; set; }
        [XmlIgnore]
        // ReSharper disable once InconsistentNaming
        public bool denominatorSpecified => denominator != null;
        // ReSharper disable once InconsistentNaming
        public decimal? numerator { get; set; }
        [XmlIgnore]
        // ReSharper disable once InconsistentNaming
        public bool numeratorSpecified => numerator != null;
    }

    public class RationalNumber
    {
        // ReSharper disable once InconsistentNaming
        public decimal? denominator { get; set; }
        [XmlIgnore]
        // ReSharper disable once InconsistentNaming
        public bool denominatorSpecified => denominator != null;
        // ReSharper disable once InconsistentNaming
        public decimal? numerator { get; set; }
        [XmlIgnore]
        // ReSharper disable once InconsistentNaming
        public bool numeratorSpecified => numerator != null;
    }
}
