﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel.Syndication;
using System.Xml;
using System.Xml.Linq;
using System.Xml.Serialization;
using CE.Xml.Transformation;

namespace CE.GreenButtonConnect
{
    public class GreenButtonConnectManager : IGreenButtonConnect
    {
        private const string EspiPrefix = "espi";
        private const string EspiUrl = "http://naesb.org/espi";
        private const string XmlSchemaPrefix = "xsi";
        private const string XmlSchemaUrl = "http://www.w3.org/2001/XMLSchema-instance";
        
        private readonly string _greenButtonDomain;


        private readonly int _clientId;
        private readonly XmlTransformer _xmlTransformer = new XmlTransformer("");

        public GreenButtonConnectManager(int clientId, string greenButtonDomain)
        {
            _clientId = clientId;
            _greenButtonDomain = greenButtonDomain;
        }

        public GreenButtonConnectResult GenerateGreenButtonConnect(Subscription subscription, GreenButtonType greenButtonType)
        {
            var greenButtonXml = GenerateGreenButtonXml(subscription, greenButtonType);
            var result = new GreenButtonConnectResult
            {
                GreenButtonXml = greenButtonXml
            };

            return result;
        }

        public GreenButtonConnectResult GenerateGreenButtonServiceStatus(bool isValidServiceStatus)
        {
            var serviceStatusXml = GenerateServiceStatusXml(isValidServiceStatus);
            var result = new GreenButtonConnectResult
            {
                GreenButtonXml = serviceStatusXml
            };

            return result;

        }


        public SyndicationFeed GetSubscription(Subscription subscription)
        {
            return GenerateSubscription(subscription);
        }

        public List<SyndicationItem> GetUsagePoints(List<UsagePointEntry> usagePoints)
        {
            if (usagePoints.Count > 0)
                return usagePoints.Select(GenerateUsagePointEntry).ToList();
            return new List<SyndicationItem>();
        }

        public List<SyndicationItem> GetMeterReadings(List<MeterReadingEntry> meterReadings)
        {
            if(meterReadings.Count > 0)
                return meterReadings.Select(GenerateMeterReading).ToList();
            return new List<SyndicationItem>();
        }

        public List<SyndicationItem> GetReadingTypes(List<ReadingTypeEntry> readingTypes)
        {
            if(readingTypes.Count > 0)
                return readingTypes.Select(GenerateReadingType).ToList();
            return new List<SyndicationItem>();
        }

        public List<SyndicationItem> GetIntervalBlocks(List<IntervalBlockEntry> intervalBlocks)
        {
            if(intervalBlocks.Count > 0)
                return intervalBlocks.Select(GenerateIntervalBlock).ToList();
            return new List<SyndicationItem>();
        }

        public List<SyndicationItem> GetLocalTimeParams(List<LocalTimeParameterEntry> localTimeParams)
        {
            if(localTimeParams.Count > 0)
                return localTimeParams.Select(GenerateLocalTimeParameter).ToList();
            return new List<SyndicationItem>();
        }

        public SyndicationItem GetApplicationInfo(ApplicationInformationEntry applicationInfoEntry)
        {
            if (applicationInfoEntry != null)
                return GenerateApplicationInfo(applicationInfoEntry);
            return null;
        }

        public SyndicationItem GetAuthorization(AuthorizationEntry authorizationEntry)
        {
            if (authorizationEntry != null)
                return GenerateAuthorization(authorizationEntry);
            return null;
        }



        private ServiceStatus GenerateServiceStatus(bool isClientAccessToken)
        {
            var serviceStatus = new ServiceStatus
            {
                currentStatus = isClientAccessToken ? 1 : 0
            };

            return serviceStatus;

        }

        private List<SyndicationItem> GetAllEntries(Subscription subscription)
        {
            var entries = new List<SyndicationItem>();

            // get usage point
            var usagePoints = GetUsagePoints(subscription.UsagePoints);
            entries.AddRange(usagePoints);

            // Get meter reading
            var meterReadingEntries = subscription.UsagePoints.SelectMany(u => u.MeterReading).ToList();
            var meterReadings = GetMeterReadings(meterReadingEntries);
            entries.AddRange(meterReadings);

            // Get reading type
            var readingTypeEntries = subscription.UsagePoints.SelectMany(u => u.MeterReading).Select(m => m.ReadingType).ToList();
            var readingTypes = GetReadingTypes(readingTypeEntries);
            entries.AddRange(readingTypes);

            // Get interval block
            var intervalBlockEntries = subscription.UsagePoints.SelectMany(u => u.MeterReading).SelectMany(m => m.IntervalBlock ?? new List<IntervalBlockEntry>()).ToList();
            var intervalBlocks = GetIntervalBlocks(intervalBlockEntries);
            entries.AddRange(intervalBlocks);

            // Get local time param
            var localTimeParamEntries = subscription.UsagePoints.Select(u => u.TimeConfiguration).GroupBy(t => t.Id).Select(t => t.FirstOrDefault()).ToList();
            var localTimeParamas = GetLocalTimeParams(localTimeParamEntries);
            entries.AddRange(localTimeParamas);

            return entries;
        }

        private string GenerateGreenButtonXml(Subscription subscription, GreenButtonType greenButtonType)
        {
            var feed = GetSubscription(subscription);

            List<SyndicationItem> entries;
            switch (greenButtonType)
            {
                case GreenButtonType.UsagePoint:
                    entries = GetUsagePoints(subscription.UsagePoints);
                    break;
                case GreenButtonType.MeterReading:
                    var meterReadings = subscription.UsagePoints.SelectMany(u => u.MeterReading).ToList();
                    entries = GetMeterReadings(meterReadings);
                    break;
                case GreenButtonType.ReadingType:
                    var readingTypes = subscription.UsagePoints.SelectMany(u => u.MeterReading).Select(m => m.ReadingType).ToList();
                    entries = GetReadingTypes(readingTypes);
                    break;
                case GreenButtonType.IntervalBlock:
                    var intervalBlocks = subscription.UsagePoints.SelectMany(u => u.MeterReading).SelectMany(m => m.IntervalBlock ?? new List<IntervalBlockEntry>()).ToList();
                    entries = GetIntervalBlocks(intervalBlocks);
                    break;
                case GreenButtonType.LocalTimeParameters:
                    var localTimeParams = subscription.UsagePoints.Select(u => u.TimeConfiguration).GroupBy(t => t.Id).Select(t => t.FirstOrDefault()).ToList();
                    entries = GetLocalTimeParams(localTimeParams);
                    break;
                case GreenButtonType.ApplicationInfo:
                    entries = new List<SyndicationItem>();
                    var entry = GetApplicationInfo(subscription.ApplicationInformation);
                    if(entry != null)
                        entries.Add(entry);
                    break;
                case GreenButtonType.Authorzation:
                    entries = new List<SyndicationItem>();

                    if (subscription.Authorizations.Any())
                    {
                        entries.AddRange(subscription.Authorizations.Select(GetAuthorization));
                    }
                    break;
                default:
                    entries = GetAllEntries(subscription);
                    break;
            }

            feed.Items = entries;

            if (entries.Count == 0)
                return string.Empty;
            return _xmlTransformer.GenerateXmlInMemory(feed);
        }

        private string GenerateServiceStatusXml(bool isValidServiceStatus)
        {
            var content = GenerateServiceStatus(isValidServiceStatus);

            return _xmlTransformer.GenerateXmlInMemory(content);
        }

        private SyndicationFeed GenerateSubscription(Subscription subscription)
        {
            var feed = new SyndicationFeed
            {
                Id = subscription.GuidId,
                LastUpdatedTime = DateTime.SpecifyKind(subscription.UpdatedDate, DateTimeKind.Utc),
                Title = SyndicationContent.CreatePlaintextContent(subscription.Title)
            };
            AddFeedNamespace(feed);
            foreach (var link in subscription.Links)
            {
                CreateLink(feed, link.HyperLinkRel, link.HyperLinkRef);
            }

            return feed;
        }

        private SyndicationItem GenerateIntervalBlock(IntervalBlockEntry intervalBlockEntry)
        {
            var idGeneration = new IdGenerator(_clientId, intervalBlockEntry.MeterId);
            
            XmlSerializer s = new XmlSerializer(typeof (IntervalBlock));

            var content = SyndicationContent.CreateXmlContent(intervalBlockEntry.IntervalBlock, s);
            
            var entry = new SyndicationItem
            {
                Id = idGeneration.MeterReadingId(intervalBlockEntry.ReadingTypeId),
                PublishDate = DateTime.SpecifyKind(intervalBlockEntry.PublishedDate, DateTimeKind.Utc),
                LastUpdatedTime = DateTime.SpecifyKind(intervalBlockEntry.UpdatedDate, DateTimeKind.Utc),
                Title = SyndicationContent.CreatePlaintextContent(intervalBlockEntry.Title),
                Content = content
            };
            AddEntryNamespace(entry);
            foreach (var link in intervalBlockEntry.Links)
            {
                CreateLink(entry, link.HyperLinkRel, link.HyperLinkRef);
            }
            return entry;

        }


        private SyndicationItem GenerateReadingType(ReadingTypeEntry readingTypeEntry)
        {
            XmlSerializer s = new XmlSerializer(typeof(ReadingType));

            var content = SyndicationContent.CreateXmlContent(readingTypeEntry.ReadingType, s);

            var entry = new SyndicationItem
            {
                Id = readingTypeEntry.GuidId,
                PublishDate = DateTime.SpecifyKind(readingTypeEntry.PublishedDate, DateTimeKind.Utc),
                LastUpdatedTime = DateTime.SpecifyKind(readingTypeEntry.UpdatedDate, DateTimeKind.Utc),
                Content = content
            };
            AddEntryNamespace(entry);
            foreach (var link in readingTypeEntry.Links)
            {
                CreateLink(entry, link.HyperLinkRel, link.HyperLinkRef);
            }
            return entry;

        }

        private SyndicationItem GenerateMeterReading(MeterReadingEntry meterReadingEntry)
        {
            var idGeneration = new IdGenerator(_clientId, meterReadingEntry.MeterId);

            XmlSerializer s = new XmlSerializer(typeof(MeterReading));

            var content = SyndicationContent.CreateXmlContent(meterReadingEntry.MeterReading, s);

            var entry = new SyndicationItem
            {
                Id = idGeneration.MeterReadingId(meterReadingEntry.ReadingTypeId),
                PublishDate = DateTime.SpecifyKind(meterReadingEntry.PublishedDate, DateTimeKind.Utc),
                LastUpdatedTime = DateTime.SpecifyKind(meterReadingEntry.UpdatedDate, DateTimeKind.Utc),
                Content = content
            };
            AddEntryNamespace(entry);
            foreach (var link in meterReadingEntry.Links)
            {
                CreateLink(entry, link.HyperLinkRel, link.HyperLinkRef);
            }
            return entry;

        }

        private SyndicationItem GenerateUsagePointEntry(UsagePointEntry usagePointEntry)
        {
            XmlSerializer s = new XmlSerializer(typeof(UsagePoint));
            var content = SyndicationContent.CreateXmlContent(usagePointEntry.UsagePoint, s);

            var entry = new SyndicationItem
            {
                Id = usagePointEntry.GuidId,
                PublishDate = DateTime.SpecifyKind(usagePointEntry.PublishedDate, DateTimeKind.Utc),
                LastUpdatedTime = DateTime.SpecifyKind(usagePointEntry.UpdatedDate, DateTimeKind.Utc),
                Title = SyndicationContent.CreatePlaintextContent(usagePointEntry.Title),
                Content = content
            };
            AddEntryNamespace(entry);

            foreach (var link in usagePointEntry.Links)
            {
                CreateLink(entry, link.HyperLinkRel, link.HyperLinkRef);
            }

            return entry;
        }


        private SyndicationItem GenerateLocalTimeParameter(LocalTimeParameterEntry localTimeParamEntry)
        {

            XmlSerializer s = new XmlSerializer(typeof(LocalTimeParameters));

            var content = SyndicationContent.CreateXmlContent(localTimeParamEntry.LocalTimeParameters, s);
            var entry = new SyndicationItem
            {
                Id = localTimeParamEntry.GuidId,
                PublishDate = DateTime.SpecifyKind(localTimeParamEntry.PublishedDate, DateTimeKind.Utc),
                LastUpdatedTime = DateTime.SpecifyKind(localTimeParamEntry.UpdatedDate, DateTimeKind.Utc),
                Content = content
            };
            AddEntryNamespace(entry);

            foreach (var link in localTimeParamEntry.Links)
            {
                CreateLink(entry, link.HyperLinkRel, link.HyperLinkRef);
            }

            return entry;
        }

        private SyndicationItem GenerateApplicationInfo(ApplicationInformationEntry applicationInformationEntry)
        {

            XmlSerializer s = new XmlSerializer(typeof(ApplicationInformation));

            var content = SyndicationContent.CreateXmlContent(applicationInformationEntry.ApplicationInformation, s);
            var entry = new SyndicationItem
            {
                Id = applicationInformationEntry.GuidId,
                PublishDate = DateTime.SpecifyKind(applicationInformationEntry.PublishedDate, DateTimeKind.Utc),
                LastUpdatedTime = DateTime.SpecifyKind(applicationInformationEntry.UpdatedDate, DateTimeKind.Utc),
                Title = SyndicationContent.CreatePlaintextContent(applicationInformationEntry.Title),
                Content = content
            };
            AddEntryNamespace(entry);

            foreach (var link in applicationInformationEntry.Links)
            {
                CreateLink(entry, link.HyperLinkRel, link.HyperLinkRef);
            }

            return entry;
        }

        private SyndicationItem GenerateAuthorization(AuthorizationEntry authorizationEntry)
        {

            XmlSerializer s = new XmlSerializer(typeof(Authorization));

            var content = SyndicationContent.CreateXmlContent(authorizationEntry.Authorization, s);
            var entry = new SyndicationItem
            {
                Id = authorizationEntry.GuidId,
                PublishDate = DateTime.SpecifyKind(authorizationEntry.PublishedDate, DateTimeKind.Utc),
                LastUpdatedTime = DateTime.SpecifyKind(authorizationEntry.UpdatedDate, DateTimeKind.Utc),
                Title = SyndicationContent.CreatePlaintextContent(authorizationEntry.Title),
                Content = content
            };
            AddEntryNamespace(entry);

            foreach (var link in authorizationEntry.Links)
            {
                CreateLink(entry, link.HyperLinkRel, link.HyperLinkRef);
            }

            return entry;
        }

        private void CreateLink(SyndicationItem entry, string relation, string url)
        {
            var link = new SyndicationLink(new Uri($"{_greenButtonDomain}{url}"), relation, null, null, 0);
            entry.Links.Add(link);
        }

        private void CreateLink(SyndicationFeed feed, string relation, string url)
        {
            var link = new SyndicationLink(new Uri($"{_greenButtonDomain}{url}"), relation, null, null, 0);
            feed.Links.Add(link);
        }

        private void AddFeedNamespace(SyndicationFeed feed)
        {
            feed.AttributeExtensions.Add(
                new XmlQualifiedName(XmlSchemaPrefix, XNamespace.Xmlns.ToString()),
                XmlSchemaUrl);
        }

        private void AddEntryNamespace(SyndicationItem entry)
        {
            entry.AttributeExtensions.Add(new XmlQualifiedName(EspiPrefix, XNamespace.Xmlns.ToString()), EspiUrl);
        }
        
    }
}
