﻿using System;
using System.Collections.Generic;
using System.Xml.Serialization;

namespace CE.GreenButtonConnect
{
    public class UsagePointEntry :GreenButtonConnectBase
    {
        public UsagePoint UsagePoint { get; set; }
        public LocalTimeParameterEntry TimeConfiguration { get; set; }
        public List<MeterReadingEntry> MeterReading { get; set; }
    }
    [Serializable]
    [XmlType(Namespace = "http://naesb.org/espi")]
    [XmlRoot(Namespace = "http://naesb.org/espi", IsNullable = false)]
    public class UsagePoint
    {
        public ServiceCategory ServiceCategory { get; set; }
    }


    public class ServiceCategory
    {
        // ReSharper disable once InconsistentNaming
        public int kind { get; set; }
    }
    
}
