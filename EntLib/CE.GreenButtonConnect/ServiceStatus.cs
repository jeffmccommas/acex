﻿using System;
using System.Xml.Serialization;

namespace CE.GreenButtonConnect
{
    [Serializable]
    [XmlType(Namespace = "http://naesb.org/espi")]
    [XmlRoot(Namespace = "http://naesb.org/espi", IsNullable = false)]
    public class ServiceStatus
    {
        [XmlAttribute("schemaLocation", Namespace = "http://www.w3.org/2001/XMLSchema-instance")]
        // ReSharper disable once InconsistentNaming
        public string schemaLocation { get; set; } = "http://naesb.org/espi espiDerived.xsd";
        // ReSharper disable once InconsistentNaming
        public int currentStatus { get; set; }
    }
}