﻿using System;
using System.Collections.Generic;
using System.Xml.Serialization;

namespace CE.GreenButtonConnect
{
    public class MeterReadingEntry : GreenButtonConnectBase
    {
        public int UsagePointId { get; set; }
        public int ReadingTypeId { get; set; }
        public string MeterId { get; set; }
        public MeterReading MeterReading { get; set; } 
        public List<IntervalBlockEntry> IntervalBlock { get; set; }
        public ReadingTypeEntry ReadingType { get; set; }

    }
    [Serializable]
    [XmlType(Namespace = "http://naesb.org/espi")]
    [XmlRoot(Namespace = "http://naesb.org/espi", IsNullable = false)]
    public class MeterReading
    {
    }
}
