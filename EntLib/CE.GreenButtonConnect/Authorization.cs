﻿using System;
using System.Xml.Serialization;

namespace CE.GreenButtonConnect
{

    public class AuthorizationEntry : GreenButtonConnectBase
    {
        public Authorization Authorization { get; set; }
    }
    [Serializable]
    [XmlType(Namespace = "http://naesb.org/espi")]
    [XmlRoot(Namespace = "http://naesb.org/espi", IsNullable = false)]
    public class Authorization
    {
        // ReSharper disable once InconsistentNaming
        public DateTimeInterval authorizedPeriod { get; set; }
        [XmlIgnore]
        // ReSharper disable once InconsistentNaming
        public bool authorizedPeriodSpecified => authorizedPeriod != null && authorizedPeriod.durationSpecified && authorizedPeriod.duration > 0 && authorizedPeriod.startSpecified && authorizedPeriod.start > 0;
        // ReSharper disable once InconsistentNaming
        public DateTimeInterval publishedPeriod { get; set; }
        [XmlIgnore]
        // ReSharper disable once InconsistentNaming
        public bool publishedPeriodSpecified => authorizedPeriod != null && publishedPeriod.durationSpecified && publishedPeriod.duration > 0 && publishedPeriod.startSpecified && publishedPeriod.start > 0;
        // ReSharper disable once InconsistentNaming
        public int status { get; set; }
        // ReSharper disable once InconsistentNaming
        public long? expires_at { get; set; }
        // ReSharper disable once InconsistentNaming
        public string grant_type { get; set; }
        // ReSharper disable once InconsistentNaming
        public string scope { get; set; }
        // ReSharper disable once InconsistentNaming
        public string token_type { get; set; }
        // ReSharper disable once InconsistentNaming
        public string error { get; set; }
        [XmlIgnore]
        // ReSharper disable once InconsistentNaming
        public bool errorSpecified => !string.IsNullOrEmpty(error);
        // ReSharper disable once InconsistentNaming
        public string error_description { get; set; }
        [XmlIgnore]
        // ReSharper disable once InconsistentNaming
        public bool error_descriptionSpecified => !string.IsNullOrEmpty(error_description);
        // ReSharper disable once InconsistentNaming
        public string error_uri { get; set; }
        [XmlIgnore]
        // ReSharper disable once InconsistentNaming
        public bool error_uriSpecified => !string.IsNullOrEmpty(error_uri);
        // ReSharper disable once InconsistentNaming
        public string resourceURI { get; set; }
        // ReSharper disable once InconsistentNaming
        public string authorizationURI { get; set; }
    }
    
}
