﻿using System;
using System.Xml.Serialization;

namespace CE.GreenButtonConnect
{
    public class LocalTimeParameterEntry : GreenButtonConnectBase
    {
        public LocalTimeParameters LocalTimeParameters { get; set; }
    }
    [Serializable]
    [XmlType(Namespace = "http://naesb.org/espi")]
    [XmlRoot(Namespace = "http://naesb.org/espi", IsNullable = false)]
    public class LocalTimeParameters
    {
        // ReSharper disable once InconsistentNaming
        public string dstEndRule { get; set; }
        // ReSharper disable once InconsistentNaming
        public long dstOffset { get; set; }

        // ReSharper disable once InconsistentNaming
        public string dstStartRule { get; set; }

        // ReSharper disable once InconsistentNaming
        public long tzOffset { get; set; }
    }
}
