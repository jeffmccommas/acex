﻿namespace CE.GreenButtonConnect
{
    public class Link
    {
        public string HyperLinkRef { get; set; }
        public string HyperLinkRel { get; set; }
    }
}
