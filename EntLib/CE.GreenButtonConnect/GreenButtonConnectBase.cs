﻿using System;
using System.Collections.Generic;

namespace CE.GreenButtonConnect
{
    public class GreenButtonConnectBase
    {
        public long Id { get; set; }
        public string GuidId { get; set; }
        public List<Link> Links { get; set; }
        public string Title { get; set; }
        public DateTime PublishedDate { get; set; }
        public DateTime UpdatedDate { get; set; }
    }
}
