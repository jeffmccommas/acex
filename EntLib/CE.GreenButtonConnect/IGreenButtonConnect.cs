﻿using System.Collections.Generic;
using System.ServiceModel.Syndication;

namespace CE.GreenButtonConnect
{
    public interface IGreenButtonConnect
    {
        GreenButtonConnectResult GenerateGreenButtonConnect(Subscription subscription, GreenButtonType greenButtonType);
        GreenButtonConnectResult GenerateGreenButtonServiceStatus(bool isValidServiceStatus);
        SyndicationFeed GetSubscription(Subscription subscription);
        List<SyndicationItem> GetUsagePoints(List<UsagePointEntry> usagePoints);
        List<SyndicationItem> GetMeterReadings(List<MeterReadingEntry> meterReadings);
        List<SyndicationItem> GetReadingTypes(List<ReadingTypeEntry> readingTypes);
        List<SyndicationItem> GetIntervalBlocks(List<IntervalBlockEntry> intervalBlocks);
        List<SyndicationItem> GetLocalTimeParams(List<LocalTimeParameterEntry> localTimeParams);
    }
}
