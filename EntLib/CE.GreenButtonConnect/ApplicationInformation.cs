﻿namespace CE.GreenButtonConnect
{
    public class ApplicationInformationEntry : GreenButtonConnectBase
    {
        public ApplicationInformation ApplicationInformation { get; set; }
    }

    public class ApplicationInformation
    {
        // ReSharper disable once InconsistentNaming
        public string dataCustodianId { get; set; }
        // ReSharper disable once InconsistentNaming
        public int dataCustodianApplicationStatus { get; set; }
        // ReSharper disable once InconsistentNaming
        public string dataCustodianBulkRequestURI { get; set; }
        // ReSharper disable once InconsistentNaming
        public string dataCustodianResourceEndpoint { get; set; }
        // ReSharper disable once InconsistentNaming
        public string thirdPartyNotifyUri { get; set; }
        // ReSharper disable once InconsistentNaming
        public string thirdPartyScopeSelectionScreenURI { get; set; }
        // ReSharper disable once InconsistentNaming
        public string thirdPartyApplicationName { get; set; }
        // ReSharper disable once InconsistentNaming
        public string client_secret { get; set; }
        // ReSharper disable once InconsistentNaming
        public string client_id { get; set; }
        // ReSharper disable once InconsistentNaming
        public string redirect_uri { get; set; }
        // ReSharper disable once InconsistentNaming
        public string scope { get; set; }
    }
}
