﻿namespace CE.GreenButtonConnect
{
    public enum GreenButtonType
    {
        All,
        IntervalBlock,
        LocalTimeParameters,
        MeterReading,
        ReadingType,
        UsagePoint,
        ApplicationInfo,
        Authorzation
    }
}
