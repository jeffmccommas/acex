﻿namespace CE.GreenButtonConnect
{
    public class GreenButtonConnectFactory
    {
        public IGreenButtonConnect CreateGreenButtonManager(int clientId, string greenButtonDomain) => new GreenButtonConnectManager(clientId, greenButtonDomain);
    }
}
