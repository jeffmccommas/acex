﻿using NLog;
using System;

namespace CE.NLogger
{
    public class ASyncLogger
    {
        private static readonly Logger Logger = LogManager.GetLogger("database");
        private static readonly Logger EmailLogger = LogManager.GetLogger("email");
        private string _environment;
        private readonly int _clientId;

        private const string PropCategory = "Category";
        private const string PropRequestData = "RequestData";
        private const string PropResponseData = "ResponseData";
        private const string PropMessage = "Message";
        private const string PropClientId = "ClientId";
        private const string PropAdditionalInfo = "AdditionalInfo";
        private const string PropSource = "Source";
        private const string PropEnvironmentName = "EnvironmentName";
        private const string PropSeverity = "Severity";

        public string Envirnoment { get { return _environment; } set { _environment = value; } }

        public ASyncLogger(int clientId, string environment)
        {
            _environment = environment;
            _clientId = clientId;
        }

        //public void emailTest(LogLevel logLevel, string category, string requestData, string responseData, string message)
        //{
        //    LogEventInfo theEvent = new LogEventInfo(logLevel, "", message);
        //    theEvent.Properties[propCategory] = category;
        //    theEvent.Properties[propRequestData] = requestData;
        //    theEvent.Properties[propResponseData] = responseData;
        //    theEvent.Properties[propMessage] = message;
        //    theEvent.Properties[propClientId] = _clientId;
        //    theEvent.Properties[propAdditionalInfo] = string.Empty;
        //    theEvent.Properties[propSource] = "test";
        //    theEvent.Properties[propEnvironmentName] = "localdev";
        //    theEvent.Properties[propSeverity] = "Test";
        //    _emailLogger.Log(theEvent);
        //}


        //private void LogEvent(LogLevel logLevel, string category, string requestData, string responseData, string message)
        //{
        //    LogEventInfo theEvent = new LogEventInfo(logLevel, "", message);
        //    theEvent.Properties[propCategory] = category;
        //    theEvent.Properties[propRequestData] = requestData;
        //    theEvent.Properties[propResponseData] = responseData;
        //    theEvent.Properties[propMessage] = message;
        //    theEvent.Properties[propClientId] = _clientId;
        //    theEvent.Properties[propAdditionalInfo] = string.Empty;
        //    theEvent.Properties[propSource] = "test";
        //    theEvent.Properties[propEnvironmentName] = "localdev";
        //    theEvent.Properties[propSeverity] = "Test";
        //    _logger.Log(theEvent);
        //}

        private void LogEvent(LogLevel logLevel, string category, string severity, string source, string requestData, string responseData, string message, string addiontionalInfo, string environment, bool email = false)
        {
            LogEventInfo theEvent = new LogEventInfo(logLevel, "", message);
            theEvent.Properties[PropClientId] = _clientId;
            theEvent.Properties[PropAdditionalInfo] = addiontionalInfo;
            theEvent.Properties[PropSource] = source;
            theEvent.Properties[PropEnvironmentName] = environment;
            theEvent.Properties[PropSeverity] = severity;
            theEvent.Properties[PropCategory] = category;
            theEvent.Properties[PropRequestData] = requestData;
            theEvent.Properties[PropResponseData] = responseData;
            theEvent.Properties[PropMessage] = message;
            if (email)
                EmailLogger.Log(theEvent);
            else
                Logger.Log(theEvent);
        }

        private void LogEventException(LogLevel logLevel, string category, string severity, string source, Exception ex, bool email = false)
        {
            LogEventInfo theEvent = new LogEventInfo(logLevel, "", ex.ToString()) {Exception = ex};
            theEvent.Properties[PropSeverity] = severity;
            theEvent.Properties[PropClientId] = _clientId;
            theEvent.Properties[PropAdditionalInfo] = ex.StackTrace;
            theEvent.Properties[PropCategory] = category;
            theEvent.Properties[PropSource] = source;
            theEvent.Properties[PropEnvironmentName] = _environment;
            theEvent.Properties[PropMessage] = ex.Message;
            if (email)
                EmailLogger.Log(theEvent);
            else
                Logger.Log(theEvent);
        }

        public void SendMail(string subject, string message)
        {
            LogEventInfo theEvent = new LogEventInfo(LogLevel.Info, "", message);
            
            theEvent.Properties["subject"] = subject;
            theEvent.Properties["message"] = message;

            EmailLogger.Log(theEvent);
        }

        public void Info(string category, string severity, string source, string requestData, string responseData, string message, string addiontionalInfo)
        {
            LogEvent(LogLevel.Info, category, severity, source, requestData, responseData, message, addiontionalInfo, _environment);
        }
        public void Info(string category, string severity, string source, string requestData, string responseData, string message)
        {
            LogEvent(LogLevel.Info, category, severity, source, requestData, responseData, message, string.Empty, _environment);
        }
        public void Info(string category, string severity, string source, string message, string addiontionalInfo)
        {
            LogEvent(LogLevel.Info, category, severity, source, string.Empty, string.Empty, message, addiontionalInfo, _environment);
        }

        public void Info(string category, string severity, string source, string message)
        {
            LogEvent(LogLevel.Info, category, severity, source, string.Empty, string.Empty, message, string.Empty, _environment);
        }

        //public void Info(string category, string requestData, string responseData, string message)
        //{
        //    LogEvent(LogLevel.Info,category, requestData, responseData, message);
        //}

        //public void Warn(int clientId, string category, string severity, string source, string requestData, string responseData, string message, string addiontionalInfo, string environment)
        //{
        //    LogEvent(LogLevel.Info, clientId, category, severity, source, requestData, responseData, message, addiontionalInfo, environment);
        //}

        //public void Warn(int clientId, string category, string severity, string source, string message, string addiontionalInfo, string environment)
        //{
        //    LogEvent(LogLevel.Info, clientId, category, severity, source, string.Empty, string.Empty, message, addiontionalInfo, environment);
        //}


        //public void Warn(string category, string requestData, string responseData, string message)
        //{
        //    LogEvent(LogLevel.Warn,category, requestData, responseData, message);
        //}

        public void Debug(string trackingid, string message)
        {
            throw new NotImplementedException();
        }

        public void Error(string category, string severity, string source, string requestData, string responseData, string message, string addiontionalInfo)
        {
            LogEvent(LogLevel.Error, category, severity, source, requestData, responseData, message, addiontionalInfo, _environment);
        }

        public void Error(string category, string severity, string source, string requestData, string responseData, string message)
        {
            LogEvent(LogLevel.Error, category, severity, source, requestData, responseData, message, string.Empty, _environment);
        }
        public void Error(string category, string severity, string source, string message, string addiontionalInfo)
        {
            LogEvent(LogLevel.Error, category, severity, source, string.Empty, string.Empty, message, addiontionalInfo, _environment);
        }

        public void Error(string category, string severity, string source, string message)
        {
            LogEvent(LogLevel.Error, category, severity, source, string.Empty, string.Empty, message, string.Empty, _environment);
        }

        //public void Error(string category,string requestData, string responseData, string message)
        //{
        //    LogEvent(LogLevel.Error, category, requestData, responseData, message);
        //}

        public void ErrorException(string category, string severity, string source, Exception ex)
        {
            LogEventException(LogLevel.Error, category, severity, source, ex);
        }

        public void EmailError(string category, string severity, string source, string requestData, string responseData, string message, string addiontionalInfo)
        {
            LogEvent(LogLevel.Error, category, severity, source, requestData, responseData, message, addiontionalInfo, _environment,true);
        }

        public void EmailError(string category, string severity, string source, string requestData, string responseData, string message)
        {
            LogEvent(LogLevel.Error, category, severity, source, requestData, responseData, message, string.Empty, _environment,true);
        }
        public void EmailError(string category, string severity, string source, string message, string addiontionalInfo)
        {
            LogEvent(LogLevel.Error, category, severity, source, string.Empty, string.Empty, message, addiontionalInfo, _environment,true);
        }

        public void EmailError(string category, string severity, string source, string message)
        {
            LogEvent(LogLevel.Error, category, severity, source, string.Empty, string.Empty, message, string.Empty, _environment,true);
        }

        public void EmailErrorException(string category, string severity, string source, Exception ex)
        {
            LogEventException(LogLevel.Error, category, severity, source, ex,true);
        }

        public void Fatal(string trackingid, string message)
        {
            throw new NotImplementedException();
        }
    }
}
