﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestApiCall.models
{
    public class ConsumptionData
    {
        public string DateTime { get; set; }
        public double Value { get; set; }
    }

    public class Commodity
    {
        public string key { get; set; }
        public string namekey { get; set; }
    }

    public class Configuration
    {
        public string key { get; set; }
        public string category { get; set; }
        public string value { get; set; }
    }

    public class TextContent
    {
        public string key { get; set; }
        public string category { get; set; }
        public string shorttext { get; set; }
        public string mediumtext { get; set; }
        public string longtext { get; set; }
        public bool requirevariablesubstitution { get; set; }
    }

    public class UOM
    {
        public string key { get; set; }
        public string namekey { get; set; }
    }

    public class Content
    {
        public List<Commodity> Commodity { get; set; }
        public List<Configuration> Configuration { get; set; }
        public List<object> Enumeration { get; set; }
        public List<TextContent> TextContent { get; set; }
        public List<UOM> UOM { get; set; }
    }

    public class RootObject
    {
        public int ClientId { get; set; }
        public string CustomerId { get; set; }
        public string AccountId { get; set; }
        public string PremiseId { get; set; }
        public string ResolutionKey { get; set; }
        public string ResolutionsAvailable { get; set; }
        public string CommodityKey { get; set; }
        public string UOMKey { get; set; }
        public int PageSize { get; set; }
        public int PageIndex { get; set; }
        public List<ConsumptionData> Data { get; set; }
        public Content Content { get; set; }
    }
}
