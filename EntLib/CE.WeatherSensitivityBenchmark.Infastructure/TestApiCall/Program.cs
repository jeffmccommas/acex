﻿using System;
using System.IO;
using System.Net;
using System.Text;
using TestApiCall.models;

namespace TestApiCall
{
    class Program
    {
        static void Main()
        {
            callInsightsApi();

            Console.WriteLine("Press any key to close this window...");
            Console.ReadLine();
        }

        public static void callInsightsApi()
        {
            string url = "https://aceapiqa.aclarax.com/api/v1/consumption?CustomerId=100000&AccountId=100000&PremiseId=100000&ServicePointId=100000&StartDate=2014-09-01&EndDate=2014-09-17&ResolutionKey=day&CommodityKey=gas&PageIndex=1&includecontent=true&includeresolutions=false&MeterIds=some-meterId";
            //string accessKey = "1792974231d37543f2e8894043069e51c88b"; //read this from DB

            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
            //string _auth = string.Format("{0}:{1}", "myUser", accessKey);
            string _cred = "Basic NzMyMEQwNDQ4MUE5NDlCOEFCNEJCMDRDREQxQTMwN0U6RGVtb0Jhc2ljS2V5ODc=";
            //string _enc = Convert.ToBase64String(Encoding.ASCII.GetBytes(_auth));
            //string _cred = string.Format("{0} {1}", "Basic", _auth);
            request.Headers["Authorization"] = _cred;
            request.Headers["X-CE-AccessKeyId"] = "7320D04481A949B8AB4BB04CDD1A307E";
            try
            {
                WebResponse response = request.GetResponse();
                using (Stream responseStream = response.GetResponseStream())
                {
                    StreamReader reader = new StreamReader(responseStream, Encoding.UTF8);
                    String responseJsonString = reader.ReadToEnd();

                    var consumptionData = Newtonsoft.Json.JsonConvert.DeserializeObject<RootObject>(responseJsonString);
                    Console.WriteLine("Account ID: " + consumptionData.AccountId);
                    Console.WriteLine("Client ID: " + consumptionData.ClientId);

                }
            }
            catch (WebException ex)
            {
                WebResponse errorResponse = ex.Response;
                using (Stream responseStream = errorResponse.GetResponseStream())
                {
                    StreamReader reader = new StreamReader(responseStream, Encoding.GetEncoding("utf-8"));
                    string errorText = reader.ReadToEnd();

                    // log errorText
                }
                throw;
            }
        }

    }
}
