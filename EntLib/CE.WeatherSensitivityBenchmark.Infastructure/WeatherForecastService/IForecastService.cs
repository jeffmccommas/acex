﻿using System;
using System.Collections.Generic;
using WeatherForecastService.models;

namespace WeatherForecastService
{
    public interface IForecastService
    {
        List<WeatherForecastService.models.RootObject> GetForecast(List<string> zipCodeList);
        WeatherForecastService.models.RootObject GetForecast(string longitude, string latitude);
        WeatherForecastService.models.Daily GetHotDayExtremeForecast(string longitude, string latitude);

        WeatherForecastService.models.Daily GetHotDayExtremeForecast(WeatherForecastService.models.Daily dailyForecast);

        WeatherForecastService.models.Daily GetColdDayExtremeForecast(string longitude, string latitude);

        WeatherForecastService.models.Daily GetColdDayExtremeForecast(WeatherForecastService.models.Daily dailyForecast);
        string GetWeatherServiceUrl();
        string GetWeatherServiceUrlKey();
        int GetHotDayThreshold();
        int GetColdDayThreshold();

        DateTime UnixTimeStampToDateTime(double unixTimeStamp);

        // Returns JSON string
        string GetWeatherData(string url);

        WeatherSensitivityAttribute GetWeatherSensitivityAttribute();

    }
}