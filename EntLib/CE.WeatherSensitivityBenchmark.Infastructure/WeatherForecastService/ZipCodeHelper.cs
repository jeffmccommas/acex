﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Reflection;
using log4net;
using Microsoft.VisualBasic.FileIO;
using WeatherForecastService.models;

namespace WeatherForecastService
{
    public class ZipCodeHelper
    {
        private string _zipCodeFilePath = "data/ZipCodeList.txt";
        private readonly ILog _logger;
        public ZipCodeHelper()
        {
            _logger = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);
            // _configurationSection = ConfigurationManager.GetSection("ApplicationSettings");
        }
        public ZipCodeHelper(ILog logger)
        {
            _logger = logger;
        }


        /* public List<string> LoadParticipantZipCodes()
         {
             try
             {
                 var appSettings = _configurationSection as NameValueCollection;
                 if (appSettings != null)
                 {
                     var zipCodeList = appSettings["ParticipantZipCodes"].ToString().Split(',');
                     return new List<string>(zipCodeList);
                 }
                 else
                 {
                     _logger.Fatal("Error reading zip codes from config file.");
                     return null;
                 }
             }
             catch (Exception ex)
             {
                 _logger.Fatal("Error reading zip codes from config file.",ex);
                 return null;
             }

         }*/
        public int GetZipCodeCount()
        {
            if (File.Exists("data/ZipCodeList.txt"))
            {
                var count = File.ReadLines(_zipCodeFilePath).Count();
                return count;
            }
            else
            {
                _logger.Fatal("Zip code file not found");
                return -1;
            }
        }

        public ZipCodeModel GetZipCodeCoordinates(string zipCode)
        {
            ZipCodeModel result = null;
            if (File.Exists("data/ZipCodeList.txt"))
            {
                try
                {
                    using (TextFieldParser csvParser = new TextFieldParser(_zipCodeFilePath))
                    {
                        csvParser.CommentTokens = new string[] { "#" };
                        csvParser.SetDelimiters(new string[] { "," });
                        // Skip the row with the column names
                        csvParser.ReadLine();

                        while (!csvParser.EndOfData)
                        {
                            // Read current line fields, pointer moves to the next line.
                            string[] fields = csvParser.ReadFields();
                            var zipCodeModel = new ZipCodeModel
                            {
                                zip = fields[0],
                                name = fields[1],
                                latitude = fields[2],
                                longitude = fields[3]
                            };

                            if (zipCodeModel.zip == zipCode)
                            {
                                result = zipCodeModel;
                                break;
                            }

                        }
                    }
                    if (result == null)
                        _logger.Warn("zip code not found.");


                    return result;
                }
                catch (Exception ex)
                {
                    _logger.Fatal("Error reading zip coordinates.", ex);
                    return null;
                }
            }
            else
            {
                _logger.Fatal("Zip code file not found");
                return null;
            }

        }
        public ZipCodeModel GetZipCode(string latitude, string longitude)
        {
            ZipCodeModel result = null;
            if (File.Exists("data/ZipCodeList.txt"))
            {
                try
                {
                    using (TextFieldParser csvParser = new TextFieldParser(_zipCodeFilePath))
                    {
                        csvParser.CommentTokens = new string[] { "#" };
                        csvParser.SetDelimiters(new string[] { "," });
                        // Skip the row with the column names
                        csvParser.ReadLine();

                        while (!csvParser.EndOfData)
                        {
                            // Read current line fields, pointer moves to the next line.
                            string[] fields = csvParser.ReadFields();
                            var zipCodeModel = new ZipCodeModel
                            {
                                zip = fields[0],
                                name = fields[1],
                                latitude = fields[2],
                                longitude = fields[3]
                            };

                            if (zipCodeModel.latitude == latitude && zipCodeModel.longitude == longitude)
                            {
                                result = zipCodeModel;
                                break;
                            }

                        }
                    }
                    if (result == null)
                        _logger.Warn("zip code not found.");


                    return result;
                }
                catch (Exception ex)
                {
                    _logger.Fatal("Error reading zipcode data.", ex);
                    return null;
                }
            }
            else
            {
                _logger.Fatal("Zip code file not found");
                return null;
            }
        }

    }


}
