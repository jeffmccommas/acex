﻿using System.Collections.Generic;
using WeatherForecastService.models;

namespace WeatherForecastService
{
    interface IZipCodeHelper
    {
        List<string> LoadParticipantZipCodes();
        int GetZipCodeCount();
        ZipCodeModel GetZipCodeCoordinates(string zipCode);
        ZipCodeModel GetZipCode(string latitude, string longitude);


    }


}
