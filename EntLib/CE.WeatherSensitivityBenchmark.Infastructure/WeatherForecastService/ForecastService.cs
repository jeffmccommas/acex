﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Text;
using CE.WeatherSensitivity.Common.Models;
using log4net;
using WeatherForecastService.models;

namespace WeatherForecastService
{
    public class ForecastService : IForecastService
    {
        
        private readonly ForecastReportSettings _forecastReportSettings;
        private readonly ILog _logger;
        public ForecastService(ForecastReportSettings forecastReportSettings, ILog logger)
        {
            _logger = logger;
            _forecastReportSettings = forecastReportSettings;
        }

        public ForecastService()
        {
            _logger = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);
            _forecastReportSettings = null;
        }

        public List<WeatherForecastService.models.RootObject> GetForecast(List<string> zipCodeList)
        {
            var result = new List<WeatherForecastService.models.RootObject>();
            _logger.Debug("Start GetForecast");
            foreach (var zipCode in zipCodeList)
            {
                var zipCodeModel = new ZipCodeHelper().GetZipCodeCoordinates(zipCode.Trim());
                if (zipCodeModel == null)
                {
                    _logger.Error("Zipcode not found :" + zipCode);
                    return null;
                }

                string url = GetWeatherServiceUrl() + GetWeatherServiceUrlKey() + "/" + zipCodeModel.latitude + "," +
                             zipCodeModel.longitude +
                             "?exclude=currently,minutely,hourly";

                var jsonInput = GetWeatherData(url);
                var forecastData = Newtonsoft.Json.JsonConvert.DeserializeObject<RootObject>(jsonInput);
                _logger.Debug("End GetForecast - > " + forecastData.daily.data.First().summary);
                result.Add(forecastData);
            }
            return result;
        }

        public WeatherForecastService.models.RootObject GetForecast(string longitude, string latitude)
        {
            var result = new WeatherForecastService.models.Daily();
            _logger.Debug("Start GetForecast");
                string url = GetWeatherServiceUrl() + GetWeatherServiceUrlKey() + "/" + latitude + "," + longitude +
                             "?exclude=currently,minutely,hourly";

                var jsonInput = GetWeatherData(url);
                var forecastData = Newtonsoft.Json.JsonConvert.DeserializeObject<RootObject>(jsonInput);

            _logger.Debug("End GetForecast - > " + forecastData.daily.data.First().summary);
            return forecastData;

        }

        public WeatherForecastService.models.Daily GetColdDayExtremeForecast(string longitude, string latitude)
        {
            var result = new WeatherForecastService.models.Daily();
            _logger.Debug("Start GetColdDayExtremeForecast");
            string url = GetWeatherServiceUrl() + GetWeatherServiceUrlKey() + "/" + latitude + "," + longitude +
                         "?exclude=currently,minutely,hourly";

            var jsonInput = GetWeatherData(url);
            var forecastData = Newtonsoft.Json.JsonConvert.DeserializeObject<RootObject>(jsonInput);

            result.data = new List<Data>();
            foreach (var day in forecastData.daily.data)
            {
                if (day.temperatureMin <= GetColdDayThreshold())
                {
                    result.data.Add(day);
                }


            }
            _logger.Debug("End GetColdDayExtremeForecast - > " + forecastData.daily.data.First().summary);
            return result;


        }
        public WeatherForecastService.models.Daily GetHotDayExtremeForecast(string longitude, string latitude)
        {
            var result = new WeatherForecastService.models.Daily();
            _logger.Debug("Start GetHotDayExtremeForecast");
                string url = GetWeatherServiceUrl() + GetWeatherServiceUrlKey() + "/" + latitude + "," + longitude +
                             "?exclude=currently,minutely,hourly";

                var jsonInput = GetWeatherData(url);
                var forecastData = Newtonsoft.Json.JsonConvert.DeserializeObject<RootObject>(jsonInput);

            result.data = new List<Data>();
            foreach (var day in forecastData.daily.data)
            {
                if (day.temperatureMax > GetHotDayThreshold())
                {
                    result.data.Add(day);
                }


            }
            _logger.Debug("End GetHotDayExtremeForecast - > " + forecastData.daily.data.First().summary);
            return result;
            

        }
        public WeatherForecastService.models.Daily GetHotDayExtremeForecast(WeatherForecastService.models.Daily dailyForecast)
        {
            var result = new WeatherForecastService.models.Daily();
            _logger.Debug("Start GetHotDayExtremeForecast");
         
            result.data = new List<Data>();
            foreach (var day in dailyForecast.data)
            {
                //Console.WriteLine("temp = " + Convert.ToInt32(day.temperatureMax));
                //Console.WriteLine("threshold = " + GetHotDayThreshold());
                //Console.WriteLine("----------------------------------------------");
                if (day.temperatureMax >= GetHotDayThreshold())
                {
                    result.data.Add(day);
                }


            }
            _logger.Debug("End GetHotDayExtremeForecast - > " + dailyForecast.data.First().summary);
            return result;


        }

        public WeatherForecastService.models.Daily GetColdDayExtremeForecast(WeatherForecastService.models.Daily dailyForecast)
        {
            var result = new WeatherForecastService.models.Daily();
            _logger.Debug("Start GetColdDayExtremeForecast");

            result.data = new List<Data>();
            foreach (var day in dailyForecast.data)
            {
                if (day.temperatureMin <= GetColdDayThreshold())
                {
                    result.data.Add(day);
                }


            }
            _logger.Debug("End GetColdDayExtremeForecast - > " + dailyForecast.data.First().summary);
            return result;


        }
        public string GetWeatherServiceUrl()
        {
            try
            {
                return _forecastReportSettings.ForecastServiceUrl;
                
            }
            catch (Exception ex)
            {
                _logger.Fatal("Error reading URL from config.", ex);
                return null;
            }

        }
        public string GetWeatherServiceUrlKey()
        {
            try
            {
                return _forecastReportSettings.ForecastServiceKey;
            }
            catch (Exception ex)
            {
                _logger.Fatal("Error reading key from config.", ex);
                return null;
            }

        }
        public virtual int GetHotDayThreshold()
        {
            try
            {
                return Convert.ToInt32(_forecastReportSettings.ExtremeHotTempThreshold);
            }
            catch (Exception ex)
            {
                _logger.Fatal("Error reading threshold from config.", ex);
                return -100;
            }

        }
        public virtual int GetColdDayThreshold()
        {
            try
            {
                return Convert.ToInt32(_forecastReportSettings.ExtremeColdTempThreshold);
            }
            catch (Exception ex)
            {
                _logger.Fatal("Error reading threshold from config.", ex);
                return -100;
            }

        }

        public WeatherSensitivityAttribute GetWeatherSensitivityAttribute()
        {
            try
            {
                return _forecastReportSettings.TemperatureType.ToLower() == "hot" ? WeatherSensitivityAttribute.HOT_WEATHER : WeatherSensitivityAttribute.HOT_WEATHER;
            }
            catch (Exception ex)
            {
                _logger.Fatal("Error reading threshold from config.", ex);
                return WeatherSensitivityAttribute.HOT_WEATHER;//set default to hot weather
            }

        }
        //public WeatherForecastService.models.Daily GetForecast(string darkSkyKey, string longitude, string latitude)
        //{
        //    logger.Debug("Start GetForecast");
        //    var forecastData = new WeatherForecastService.models.RootObject();
        //    string url = "https://api.darksky.net/forecast/" + darkSkyKey + "/" + latitude + "," + longitude +
        //               "?exclude=currently,minutely,hourly";

        //    var jsonInput = GetWeatherData(url);
        //    forecastData = Newtonsoft.Json.JsonConvert.DeserializeObject<RootObject>(jsonInput);

        //    foreach (var day in forecastData.daily.data)
        //    {
        //        var time = UnixTimeStampToDateTime(Convert.ToDouble(day.time));

        //    }
        //    logger.Debug("End GetForecast - > " + forecastData.daily.data.First().summary);
        //    return forecastData.daily;

        //}


        public DateTime UnixTimeStampToDateTime(double unixTimeStamp)
        {
            //double unixTimeStamp = Convert.ToDouble(unixTimeStampstring);
            // Unix timestamp is seconds past epoch
            System.DateTime dtDateTime = new DateTime(1970, 1, 1, 0, 0, 0, 0, System.DateTimeKind.Utc);
            dtDateTime = dtDateTime.AddSeconds(unixTimeStamp).ToLocalTime();
            return dtDateTime;
        }

        // Returns JSON string
        public virtual string GetWeatherData(string url)
        {
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
            try
            {
                WebResponse response = request.GetResponse();
                using (Stream responseStream = response.GetResponseStream())
                {
                    StreamReader reader = new StreamReader(responseStream, Encoding.UTF8);
                    return reader.ReadToEnd();
                }
            }
            catch (WebException ex)
            {
                WebResponse errorResponse = ex.Response;
                using (Stream responseStream = errorResponse.GetResponseStream())
                {
                    StreamReader reader = new StreamReader(responseStream, Encoding.GetEncoding("utf-8"));
                    string errorText = reader.ReadToEnd();
                    return string.Empty;
                    // log errorText
                }
            }
        }
    }
}