﻿using System.Collections.Generic;

namespace WeatherForecastService.models
{
    public enum WeatherSensitivityAttribute
    {
        HOT_WEATHER,
        COLD_WEATHER
    }
    public class Data
    {
        public int time { get; set; }
        public string summary { get; set; }
        public string icon { get; set; }
        public int sunriseTime { get; set; }
        public int sunsetTime { get; set; }
        public double moonPhase { get; set; }
        public double precipIntensity { get; set; }
        public double precipIntensityMax { get; set; }
        public int precipIntensityMaxTime { get; set; }
        public double precipProbability { get; set; }
        public string precipType { get; set; }
        public double precipAccumulation { get; set; }
        public double temperatureMin { get; set; }
        public int temperatureMinTime { get; set; }
        public double temperatureMax { get; set; }
        public int temperatureMaxTime { get; set; }
        public double apparentTemperatureMin { get; set; }
        public int apparentTemperatureMinTime { get; set; }
        public double apparentTemperatureMax { get; set; }
        public int apparentTemperatureMaxTime { get; set; }
        public double dewPoint { get; set; }
        public double humidity { get; set; }
        public double windSpeed { get; set; }
        public int windBearing { get; set; }
        public double visibility { get; set; }
        public double cloudCover { get; set; }
        public double pressure { get; set; }
        public double ozone { get; set; }
    }

    public class Daily
    {
        public string summary { get; set; }
        public string icon { get; set; }
        public List<Data> data { get; set; }
    }

    public class Flags
    {
        public List<string> sources { get; set; }
        public List<string> __invalid_name__lamp_stations { get; set; }
        public List<string> __invalid_name__isd_stations { get; set; }
        public List<string> __invalid_name__madis_stations { get; set; }
        public string units { get; set; }
    }

    public class RootObject
    {
        public double latitude { get; set; }
        public double longitude { get; set; }
        public string timezone { get; set; }
        public int offset { get; set; }
        public Daily daily { get; set; }
        public Flags flags { get; set; }
    }
}
