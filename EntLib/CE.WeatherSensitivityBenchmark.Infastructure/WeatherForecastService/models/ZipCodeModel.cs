﻿using System.Data.Entity;
using Microsoft.VisualBasic.FileIO;

namespace WeatherForecastService.models
{
    public class ZipCodeModel
    {
        public int id { get; set; }
        public string name { get; set; }
        public string zip { get; set; }
        public string latitude { get; set; }
        public string longitude { get; set; }

    }
    ////code first
    //public class ZipCodeContext : DbContext
    //{


    //    static ZipCodeContext() //: base("WSInternalDb")
    //    {
    //        Database.SetInitializer(new DbInitializer());
    //        using (var db = new ZipCodeContext())
    //            db.Database.Initialize(false);
    //    }

    //    public virtual DbSet<ZipCodeModel> ZipCodeModel { get; set; }

       
    //}
    //class DbInitializer : CreateDatabaseIfNotExists<ZipCodeContext>
    //{
    //    protected override void Seed(ZipCodeContext context)
    //    {
    //        int counter = 0;
    //        var path = @"data\ZipCodeList.txt";
    //        using (TextFieldParser csvParser = new TextFieldParser(path))
    //        {
    //            csvParser.CommentTokens = new string[] { "#" };
    //            csvParser.SetDelimiters(new string[] { "," });
    //            // Skip the row with the column names
    //            csvParser.ReadLine();

    //            while (!csvParser.EndOfData)
    //            {
    //                // Read current line fields, pointer moves to the next line.
    //                string[] fields = csvParser.ReadFields();
    //                var zipCodeModel = new ZipCodeModel
    //                {
    //                    zip = fields[0],
    //                    name = fields[1],
    //                    latitude = fields[2],
    //                    longitude = fields[3]
    //                };
    //                counter++;
    //                context.ZipCodeModel.Add(zipCodeModel);
    //            }
    //        }
    //        base.Seed(context);
    //    }
    //}
}
