//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace InsightsDW.DataModel
{
    using System;
    
    public partial class WSRetrieveStationIdsAndPostalCodes_Result
    {
        public string StationIdDaily { get; set; }
        public string PostalCodes { get; set; }
    }
}
