﻿using System.Data.Entity;
using System.Data.Entity.Core.EntityClient;
using System.Data.SqlClient;

namespace InsightsDW.DataModel.DataAccess
{
    /// <summary>
    /// Modify the connection string for Entity Framework usage.
    /// </summary>
    public static class ConnectionTools
    {
        /// <summary>
        /// Change the Initial Catalog to the desired Shard.
        /// </summary>
        /// <param name="source"></param>
        /// <param name="initialCatalog"></param>
        /// <param name="commandTimeout">The time out for the command</param>
        public static void ChangeDatabase(this DbContext source, string initialCatalog = "", int commandTimeout = 600)
        {
            // get the connection string name.
            var configNameEf = source.GetType().Name;

            // add a reference to System.Configuration
            var entityCnxStringBuilder =
                new EntityConnectionStringBuilder(
                    System.Configuration.ConfigurationManager.ConnectionStrings[configNameEf].ConnectionString);

            // init the sqlbuilder with the full EF connectionstring cargo
            var sqlCnxStringBuilder = new SqlConnectionStringBuilder
                (entityCnxStringBuilder.ProviderConnectionString);

            // only populate parameters with values if added
            if (!string.IsNullOrEmpty(initialCatalog))
            {
                sqlCnxStringBuilder.InitialCatalog = initialCatalog;
            }
            source.Database.CommandTimeout = commandTimeout;
            // now flip the properties that were changed
            source.Database.Connection.ConnectionString
                = sqlCnxStringBuilder.ConnectionString;
        }
    }
}
