﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace InsightsDW.DataModel.DataAccess
{
    public class InsightsDataWarehouse : IInsightsDWAccess
    {

        private readonly string _shardName;
        private readonly int _commandTimeout;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="shardName"></param>
        /// <param name="commandTimeout">The command timeout for the connection string</param>
        public InsightsDataWarehouse(string shardName = "InsightsDW_Test", int commandTimeout = 0)
        {
            _commandTimeout = commandTimeout;
            _shardName = shardName;
        }

        /// <summary>
        /// Covert the SQL Server connection string to the proper Shard.
        /// </summary>
        /// <returns></returns>
        private InsightsDW_Entities GetInsightsDW_Entities()
        {

            var insightsDwEntities = new InsightsDW_Entities();
            insightsDwEntities.ChangeDatabase(_shardName, _commandTimeout);
            return insightsDwEntities;
        }

        /// <summary>
        /// Determine the name the profile for the report to be acted upon.
        ///  Based on [Export].[usp_HE_Report_RunReport]
        /// </summary>
        /// <param name="reportAlias">The report's alias</param>
        /// <param name="clientId">The clientId</param>
        /// <param name="reportRequestDate">The datetime the report is being run.</param>
        /// <returns></returns>
        public string GetProfileName(string reportAlias, int clientId, DateTime reportRequestDate)
        {
            var dwEntites = GetInsightsDW_Entities();

            //					-- effective start date for a report profile
            //                    select

            //                        [ProfileName] as [profile_name],
            //                        cast([Value] as date) as [effective_date]
            //        from
            //                            [export].[HomeEnergyReportSetting]
            //        where
            //                            [Name] = 'effective_start' and
            //                            [ClientId] = @clientId and
            //                            [ProfileName] like '%' + @commodity + '%'
            var startDateProfileNames =
                dwEntites.WSReportSettings.Where(
                        m => m.Name.Equals("effective_start", StringComparison.InvariantCultureIgnoreCase) &&
                             m.ClientId == clientId).ToList()
                    .Where(n => DateTime.Parse(n.Value).Subtract(reportRequestDate) <= TimeSpan.Zero).ToList();

            if (!startDateProfileNames.Any())
            {
                return string.Empty;
            }

            //					-- effective end date for a report profile
            //                    select

            //                        [ProfileName] as [profile_name],
            //                        cast([Value] as date) as [effective_date]
            //        from
            //                            [export].[HomeEnergyReportSetting]
            //        where
            //                            [Name] = 'effective_end' and
            //                            [ClientId] = @clientId and
            //                            [ProfileName] like '%' + @commodity + '%'
            //				) as [profile_end]
            //        on
            //                    [profile_start].[profile_name] = [profile_end].[profile_name]
            var endDateProfileNames =
                dwEntites.WSReportSettings.Where(
                        m => m.Name.Equals("effective_end", StringComparison.InvariantCultureIgnoreCase) &&
                             m.ClientId == clientId).ToList()
                    .Where(n => DateTime.Parse(n.Value).Subtract(reportRequestDate) >= TimeSpan.Zero).ToList();

            var validProfileNames = startDateProfileNames.SelectMany(s => endDateProfileNames, (s, e) => new {s, e})
                .Where(t => t.e.ProfileName.Equals(t.s.ProfileName, StringComparison.InvariantCultureIgnoreCase))
                .Select(t => t.e).ToList();

            if (!validProfileNames.Any())
            {
                return string.Empty;
            }



            //[ProfileName] as [profile_name]
            //                        from
            //                            [export].[HomeEnergyReportSetting]
            //                        where
            //                            [Name] = 'report_alias' and
            //[Value] = @reportAlias and
            //[ClientId] = @clientId and
            //[ProfileName] like '%' + @commodity + '%'
            //				) as [profile_alias]
            //        on
            //                    [profile_alias].[profile_name] = [profile_end].[profile_name]

            var reportAliasProfileNames = dwEntites.WSReportSettings.Where(
                    m => m.Name.Equals("report_alias", StringComparison.InvariantCultureIgnoreCase) &&
                         m.ClientId == clientId &&
                         m.Value.Equals(reportAlias, StringComparison.InvariantCultureIgnoreCase)).ToList()
                .Where(m => validProfileNames.Select(n => n.ProfileName).Contains(m.ProfileName)).ToList();

            return !reportAliasProfileNames.Any() ? string.Empty : reportAliasProfileNames[0].ProfileName;
        }

        /// <summary>
        /// Get miscellaneous report settings for a profile.
        /// </summary>
        /// <param name="profileName">The profile name</param>
        /// <param name="clientId">The client Id</param>
        /// <returns></returns>
        public IEnumerable<WSReportSetting> GetReportSettingInfo(string profileName, int clientId)
        {
            return GetInsightsDW_Entities().WSReportSettings.Where(
                m => m.ProfileName.Equals(profileName, StringComparison.InvariantCultureIgnoreCase) &&
                     m.ClientId == clientId).ToList();
        }

        /// <summary>
        /// Get miscellaneous report profile settings for a profile. 
        /// </summary>
        /// <param name="profileName">The profile name</param>
        /// <param name="clientId">The client Id</param>
        /// <returns>A WSReportProfile object.</returns>
        public WSReportProfile GetMiscReportProfilesInfo(string profileName, int clientId)
        {
            return GetInsightsDW_Entities().WSReportProfiles.First(
                m => m.ProfileName.Equals(profileName, StringComparison.InvariantCultureIgnoreCase) &&
                     m.ClientId == clientId);
        }

        /// <summary>
        /// Get miscellaneous report settings for a profile.
        /// </summary>
        /// <param name="appName">The application name</param>
        /// <param name="clientId">The client Id</param>
        /// <returns></returns>
        public IEnumerable<WSApplicationSetting> GetApplicationSettings(string appName, int clientId)
        {
            return GetInsightsDW_Entities().WSApplicationSettings.Where(
                m => m.AppName.Equals(appName, StringComparison.InvariantCultureIgnoreCase) &&
                     m.ClientId == clientId).ToList();
        }

        /// <summary>
        /// get data from WSBenchmarkResults table based on the params
        /// </summary>
        /// <param name="clientId">The clientId</param>
        /// <param name="profileName">The name of the report profile</param>
        /// <returns></returns>
        public IEnumerable<WSBenchmarkResult> GetWsBenchmarkResults(int clientId, string profileName)
        {
            // return the profile records associated with that date.
            return GetInsightsDW_Entities().WSBenchmarkResults.Where(
                m => m.ClientId == clientId &&
                     m.ReportProfileName.Equals(profileName, StringComparison.InvariantCultureIgnoreCase) &&
                     m.IsActive).ToList();
        }

        /// <summary>
        /// Get a list of the GroupIds to be processed.
        /// </summary>
        /// <param name="clientId">The clientId</param> 
        /// <returns>A list of Group Ids</returns>
        public IEnumerable<int> GetWsBenchmarkGroupIds(int clientId)
        {
            var returnList = GetInsightsDW_Entities()
                .WSBenchmarkGroups.Where(m => m.ClientID == clientId)
                .Select(m => (int) m.GroupID).Distinct().ToList();

            returnList.Sort();
            return returnList;
        }

        /// <summary>
        /// Get a list of the GroupIds to be processed.
        /// </summary>
        /// <param name="clientId">The clientId</param>
        /// <param name="groupId">The groupId</param>
        /// <returns>AThe membership count of the groupId</returns>
        public int GetWsBenchmarkGroupMemberCount(int clientId, int groupId)
        {
            var theCount = GetInsightsDW_Entities()
                .WSBenchmarkGroups
                .Count(m => m.ClientID == clientId && m.GroupID == groupId);

            return theCount;
        }

        /// <summary>
        /// Get the Premise Keys that are part of the Benchmark Groups
        /// </summary>
        /// <param name="clientId">The clientId</param> 
        /// <returns>A list of Premise Keys</returns>
        public IEnumerable<WSBenchmarkGroup> GetWsBenchmarkGroups(int clientId)
        {
            return GetInsightsDW_Entities()
                .WSBenchmarkGroups.Where(m => m.ClientID == clientId).ToList();
        }


        /// <summary>
        /// Retrieve the parameters needed to for the Consumption API 
        /// </summary>
        /// <param name="clientId">The clientId</param>
        /// <param name="groupId">The groupdId</param>
        /// <param name="commodity">The commodity</param>
        /// <returns></returns>
        public IEnumerable<WSRetrieveConsumptionAPIParameters_Result> GetConsumptionApiParameters(int clientId,
            int groupId, string commodity)
        {
            return GetInsightsDW_Entities().WSRetrieveConsumptionAPIParameters(clientId, groupId, commodity).ToList();
        }

        /// <summary>
        /// Get the extreme days (hottest or coldest) for a specified date range at specified weather stations.
        /// </summary>
        /// <param name="stationIds">The weather station Ids to look over</param>
        /// <param name="temperatureType">The temperature type (hot or cold)</param>
        /// <param name="numberOfExtremeDays">The number of extreme days desired</param>
        /// <param name="startDateTime">The start date for the observation</param>
        /// <param name="endDateTime">The end date for the observation</param>
        /// <returns></returns>
        public IEnumerable<EMDailyWeatherDetail> GetDailyWeatherDetails(IEnumerable<string> stationIds,
            string temperatureType, int numberOfExtremeDays, DateTime startDateTime, DateTime endDateTime)
        {

            var returnList = new List<EMDailyWeatherDetail>();

            // Start the query
            var theQuery = GetInsightsDW_Entities()
                .EMDailyWeatherDetails.Where(
                    m =>
                        m.WeatherReadingDate >= startDateTime && m.WeatherReadingDate <= endDateTime &&
                        stationIds.Contains(m.StationID));

            // Add temperature type 
            theQuery = temperatureType.Equals("hot", StringComparison.InvariantCultureIgnoreCase)
                ? theQuery.OrderByDescending(m => m.MaxTemp)
                : theQuery.OrderByDescending(m => m.MinTemp);

            var datesChosen = new List<DateTime>();
            foreach (var weatherDataItem in theQuery.ToList())
            {
                if (!datesChosen.Contains(weatherDataItem.WeatherReadingDate))
                {
                    datesChosen.Add(weatherDataItem.WeatherReadingDate);
                    returnList.Add(weatherDataItem);

                    if (returnList.Count.Equals(numberOfExtremeDays))
                    {
                        break;
                    }
                }
            }

            // return the data 
            return returnList;
        }

        /// <summary>
        /// Save the benchmark results to the database.
        /// </summary>
        /// <param name="benchmarkResults">The benchmark results to save.</param>
        /// <param name="runDateTime">The benchmark run datetime</param>
        public void SaveBenchmarks(List<WSBenchmarkResult> benchmarkResults, DateTime runDateTime)
        {

            // Set old records inactive.
            using (var context = GetInsightsDW_Entities())
            {
                var theProfileName = benchmarkResults[0].ReportProfileName;
                var theGroupId = benchmarkResults[0].GroupId;

                context.WSBenchmarkResults.Where(
                        m => m.ReportProfileName.Equals(theProfileName, StringComparison.InvariantCultureIgnoreCase) &&
                             m.GroupId == theGroupId && m.IsActive).ToList()
                    .ForEach(c => c.IsActive = false);
                context.SaveChanges();
            }

            // Set the create date and IsActive flag
            benchmarkResults.ForEach(c =>
            {
                c.CreateDate = runDateTime;
                c.IsActive = true;
            });


            // Save the data.
            using (var context = GetInsightsDW_Entities())
            {
                context.WSBenchmarkResults.AddRange(benchmarkResults);
                context.SaveChanges();
            }
        }

        /// <summary>
        /// Retrieve the weather stationIds and associated ZipCodes 
        /// </summary>
        /// <param name="clientId">The clientId</param>
        /// <param name="groupId">The groupdId (OPTIONAL)</param>
        /// <returns>List of postal codes grouped by station ids</returns>
        public IEnumerable<WSRetrieveStationIdsAndPostalCodes_Result> GetStationIdsAndPostalCodes(int clientId,
            int? groupId)
        {
            return GetInsightsDW_Entities().WSRetrieveStationIdsAndPostalCodes(clientId, groupId).ToList();
        }

        /// <summary>
        /// Determine if the customer participates in home auditing or HES
        /// </summary>
        /// <param name="premiseId">The premiseId</param>
        /// <param name="accountId">The accountId</param>
        /// <returns></returns>
        public int GetIsHesParticipant(string premiseId, string accountId)
        {

            var hesParticipants = GetInsightsDW_Entities().factActions
                .Where(f => f.AccountID.Equals(accountId, StringComparison.InvariantCultureIgnoreCase)
                            && f.PremiseID.Equals(premiseId, StringComparison.InvariantCultureIgnoreCase)
                            && f.StatusKey == 2 &&
                            f.ActionKey.Equals("audithome", StringComparison.InvariantCultureIgnoreCase))
                .GroupBy(m => new {m.AccountID, m.PremiseID, m.StatusKey}).ToList()
                .SelectMany(g => g.OrderByDescending(y => y.CreateDate)
                    .Select((x, i) => new {Value = 1, Rank = i + 1}));


            return hesParticipants.Where(z => z.Rank == 1).Select(m => m.Value).FirstOrDefault();
        }

        /// <summary>
        /// Load the Measures Staging table.
        /// </summary>
        /// <param name="reportProfileName">The report profile name</param>
        /// <param name="reportYear">The report year</param>
        /// <param name="reportMonth">The report month</param>
        /// <param name="reportNumber">The report number</param>
        /// <param name="treatmentGroups">The treatment groups for the report.</param>
        /// <param name="programGroupNumber">The program group number associated with the report.</param>
        /// <param name="programEnrollmentStatus">The programEnrollmentStatus string associated with the report.</param>
        /// <param name="programChannel">The programChannel (i.e. email) string associated with the report.</param>
        /// <param name="programEnrollmentStatusEnrolled">The programEnrollmentStatusEnrolled string associated with the report.</param>
        /// <param name="reportChannel">The report channel (i.e. email) of the report.</param>
        /// <param name="runId">A GUID representing the runId of this report.</param>
        public void LoadAllStagingColumns(string reportProfileName, string reportYear, string reportMonth, string reportNumber, string treatmentGroups,
            string programGroupNumber, string programEnrollmentStatus, string programChannel, string programEnrollmentStatusEnrolled, string reportChannel, Guid runId)
        {
  
            GetInsightsDW_Entities().WS_Measures_CreateStagingAllCols(
                reportProfileName, reportYear, reportMonth, reportNumber, treatmentGroups,
                    programGroupNumber, programEnrollmentStatus, programChannel, programEnrollmentStatusEnrolled,
                    reportChannel, runId.ToString());


        }

        /// <summary>
        /// Run the measures using Stored procedures in the InsightsDW Shard
        /// </summary>
        /// <param name="reportProfileName">The report profile name</param>
        /// <param name="reportYear">The report year</param>
        /// <param name="reportMonth">The report month</param>
        /// <param name="runId">A GUID representing the runId of this report.</param>
        /// <param name="isTrialRun">If True, doing a trial run.</param>
        public void RunTheMeasures(string reportProfileName, string reportYear, string reportMonth, Guid runId, bool isTrialRun)
        {
            GetInsightsDW_Entities().WS_Measures(reportProfileName, reportYear, reportMonth, runId, isTrialRun);
        }

        /// <summary>
        /// Get the current contents of the Staging table.
        /// </summary>
        /// <returns>A list of measure results</returns>
        public List<WS_Measures_StagingAllColumns> GetMeasureResults()
        {
            return GetInsightsDW_Entities().WS_Measures_StagingAllColumns.Select(m => m).ToList();
        }

        /// <summary>
        /// Create a Daily and/or a Releasde candidate snapshot.
        /// </summary>
        /// <param name="reportAlias">The reportAlias</param>
        /// <param name="profileName">The profile name</param>
        /// <param name="reportYear">The report year.</param>
        /// <param name="reportMonth">The report month</param>
        /// <param name="reportNumber">The report number</param>
        /// <param name="runId">The runId for logging</param>
        /// <param name="isTrialRun">True, if a trial run.</param>
        public void CreateMeasuresSnapshot(string reportAlias, string profileName, string reportYear, string reportMonth, int reportNumber, Guid runId, bool isTrialRun)
        {
            GetInsightsDW_Entities()
                .WS_Measures_CreateSnapshot(reportAlias, profileName, reportYear, reportMonth, reportNumber, runId,
                    isTrialRun);
        }
    }
}
