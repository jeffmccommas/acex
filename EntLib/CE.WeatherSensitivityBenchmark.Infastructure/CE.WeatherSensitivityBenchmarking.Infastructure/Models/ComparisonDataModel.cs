﻿using System.Collections.Generic;
using CE.WeatherSensitivity.Common.Models;

namespace CE.WeatherSensitivityBenchmarking.Infastructure.Models
{
    public class ComparisonDataModel
    {

        /// <summary>
        /// The group Id
        /// </summary>
        public int GroupId { get; set; }

        /// <summary>
        /// The CustomerId
        /// </summary>
        public string CustomerId { get; set; }

        /// <summary>
        /// The Account Id
        /// </summary>
        public string AccountId { get; set; }

        /// <summary>
        /// The Premise Id
        /// </summary>
        public string PremiseId { get; set; }

        /// <summary>
        /// The Commondity Desc
        /// </summary>
        public string CommodityKey { get; set; }

        /// <summary>
        /// The UOM Desc
        /// </summary>
        public string UomKey { get; set; }

        /// <summary>
        /// The Consumption Data.
        /// </summary>
        public List<ConsumptionApiModels.ConsumptionData> Data { get; set; }

        /// <summary>
        /// The home usage on extreme days
        /// </summary>
        public decimal MyUsageExtremeDay { get; set; }

        /// <summary>
        /// The home usage on noal days
        /// </summary>
        public decimal MyUsageNormalDay { get; set; }

    }
}
