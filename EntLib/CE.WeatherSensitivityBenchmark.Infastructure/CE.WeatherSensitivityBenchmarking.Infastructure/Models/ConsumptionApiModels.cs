﻿using System.Collections.Generic;

namespace CE.WeatherSensitivityBenchmarking.Infastructure.Models
{
    public class ConsumptionApiModels
    {
        /// <summary>
        /// The ConsumptionData
        /// </summary>
        public class ConsumptionData
        {
            public string DateTime { get; set; }
            public double Value { get; set; }
        }

        /// <summary>
        /// The RootObject
        /// </summary>
        public class RootObject
        {
            public int ClientId { get; set; }

            public string CustomerId { get; set; }

            public string AccountId { get; set; }

            public string PremiseId { get; set; }

            public string ResolutionKey { get; set; }

            public string ResolutionsAvailable { get; set; }

            public string CommodityKey { get; set; }

            public string UOMKey { get; set; }

            public int PageSize { get; set; }

            public int PageIndex { get; set; }

            public List<ConsumptionData> Data { get; set; }
        }


    }
}
