﻿namespace CE.WeatherSensitivityBenchmarking.Infastructure.Models
{
    /// <summary>
    /// Valid enviornments for this application.
    /// </summary>
    public enum Endpoints
    {
        Echo = 1,
        Webtoken = 12,
        Consumption = 17
    }
}
