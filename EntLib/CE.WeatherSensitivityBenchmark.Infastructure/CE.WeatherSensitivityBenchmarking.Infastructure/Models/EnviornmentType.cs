﻿namespace CE.WeatherSensitivityBenchmarking.Infastructure.Models
{
    /// <summary>
    /// Valid enviornments for this application.
    /// </summary>
    public enum EnvironmentType
    {
        prod,
        uat,
        qa,
        dev,
        localdev
    }
}
