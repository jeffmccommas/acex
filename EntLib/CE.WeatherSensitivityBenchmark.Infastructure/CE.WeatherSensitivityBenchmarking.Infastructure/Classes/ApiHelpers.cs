﻿using System;
using System.Configuration;
using System.IO;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using CE.WeatherSensitivity.Common.Classes;
using CE.WeatherSensitivityBenchmarking.Infastructure.Models;
using Insights.DataModel.Models;

namespace CE.WeatherSensitivityBenchmarking.Infastructure.Classes
{
    public class ApiHelpers
    {
        // Constants
        private static EnvironmentType _environment = EnvironmentType.prod;    // default = 0
        private static Endpoints _endpoint = Endpoints.Consumption;
       
        //list of headers here with their respective checkboxes that if selected will be included in the request
        public const string DateTime = "X-DateTime";

        // this will always be included for now on rest client api request- on 87 CE authentication
        public const string UsernameHeader = "X-CE-AccessKeyId";
        public const string MessageIdHeader = "X-CE-MessageId";
        public const string ChannelHeader = "X-CE-Channel";
        public const string LocaleHeader = "X-CE-Locale";
        public const string MetaHeader = "X-CE-Meta";  ////on end also include "#cecp:1#" to simulate control panel caller so record would be excluded in reported results

        /// <summary>
        /// Get the current enviornment 
        /// </summary>
        /// <returns></returns>
        public static byte GetCurrentEnvironment()
        {
            var theEnviornment = ConfigurationHelper.GetCurrentEnvironment();

            if (!string.IsNullOrEmpty(theEnviornment))
            {
                _environment = (EnvironmentType)Enum.Parse(typeof(EnvironmentType), theEnviornment, true);
            }
            return (byte)_environment;
        }

        /// <summary>
        /// Gets the API base url.
        /// </summary>
        /// <returns>The base url</returns>
        public static string GetCEInsightsAPI_BaseUrl()
        {
            var baseUrl = ConfigurationHelper.GetCeInsightsApiBaseUrl();

            if (string.IsNullOrEmpty(baseUrl))
            {
                throw new ApplicationException("Missing API Base URL");
            }

            return baseUrl;
        }

        /// <summary>
        /// Gets the API endpoint.
        /// </summary>
        /// <returns>The API endpoint</returns>
        public static int GetCEInsightsAPI_Endpoint()
        {
            var theEndpoint = ConfigurationHelper.GetCeInsightsApiEndPoint();

            if (!string.IsNullOrEmpty(theEndpoint))
            {
                _endpoint = (Endpoints)Enum.Parse(typeof(Endpoints), theEndpoint, true);
            }
            return (int)_endpoint;
        }


        /// <summary>
        /// Gets the API base url.
        /// </summary>
        /// <returns>The base url</returns>
        public static bool GetCEInsightsAPI_Encrypt()
        {
            var encrypt = ConfigurationHelper.GetCeInsightsApiEncrypt();

            bool returnValue;
            if(bool.TryParse(encrypt, out returnValue))
            {
                returnValue = false;
            }

            return returnValue;
        }

        /// <summary>
        /// Encrypt the API rest call parameters
        /// </summary>
        /// <param name="queryString">The parameters</param>
        /// <param name="clientId">The clientId</param>
        /// <param name="userInfo">The users information</param>
        /// <returns>An encryoted parameter string</returns>
        public static string EncryptQueryApi(string queryString, int clientId, UserData userInfo)
        {
            var key = userInfo.CeSecretAccessKey;

            byte[] iv = { 55, 34, 87, 64, 87, 195, 54, 21 };

            var encryptKey = Encoding.UTF8.GetBytes(key.Substring(0, 8));

            var des = new DESCryptoServiceProvider();

            var inputByte = Encoding.UTF8.GetBytes(queryString);

            var mStream = new MemoryStream();

            var cStream = new CryptoStream(mStream, des.CreateEncryptor(encryptKey, iv), CryptoStreamMode.Write);

            cStream.Write(inputByte, 0, inputByte.Length);

            cStream.FlushFinalBlock();

            return HttpUtility.UrlEncode(Convert.ToBase64String(mStream.ToArray()));
        }

        /// <summary>
        /// Do Base64 Encoding of a string
        /// </summary>
        /// <param name="plainText">The string to encode</param>
        /// <returns>The encoded string</returns>
        public static string Base64Encode(string plainText)
        {
            var plainTextBytes = Encoding.UTF8.GetBytes(plainText);
            return Convert.ToBase64String(plainTextBytes);
        }
    }
}
