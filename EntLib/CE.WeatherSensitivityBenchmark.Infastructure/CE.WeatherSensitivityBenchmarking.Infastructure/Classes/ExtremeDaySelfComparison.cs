﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Reflection;
using CE.WeatherSensitivity.Common.Logging;
using CE.WeatherSensitivity.Common.Models;
using CE.WeatherSensitivityBenchmarking.Infastructure.Models;
using InsightsDW.DataModel;
using InsightsDW.DataModel.DataAccess;
using log4net;

namespace CE.WeatherSensitivityBenchmarking.Infastructure.Classes
{



    // Process Flow
    //  For each participant:
    //  1.	Find the 5 (configurable) days with the most extreme(hottest or coldest) daily temperature for that time period.
    //
    //  2.	Log the dates and temperatures.
    //
    //  3.	Calculate MyExtremeDayUse = Average of the customer’s daily usage for the five most extreme days.
    //
    //  4.	Calculate MyNormalDayUse = Average of the customer’s daily usage for the rest of the days in the time period.
    //
    //  5.	Calculate PcntSelfBenchmark and PcntSelfBenchmarkText by comparing extreme day usage to normal day usage:
    //
    //      a.If ExtremeDayUse/NormalDayUse >= 2 then round it up to the closest half number.Append “X” for the text field.
    //              Sample message – You used nearly 2.5x as much on a hot day compared to a normal day.
    //
    //      b.If ExtremeDayUse/NormalDayUse< 2 and then subtract 1 and round it to the nearest ten percent.Append “ percent” for 
    //              the text field.Sample message: You used 30% more on a hot day compared to a normal day.
    //
    //      c.If AMI or weather data is missing and ExtremeDayUse or NormalDayUse cannot be calculated, PcntSelfBenchmark and text = null



    public class ExtremeDaySelfComparison
    {
        private readonly IEnumerable<EMDailyWeatherDetail> _extremeDailyWeatherDetails;
        private static readonly ILog Logger = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        /// <summary>
        /// A stubbed out constructor for Unit Testing
        /// </summary>
        /// <param name="mockDailyWeatherDetails">Mock Weather Data</param>
        public ExtremeDaySelfComparison(IEnumerable<EMDailyWeatherDetail> mockDailyWeatherDetails)
        {
            _extremeDailyWeatherDetails = mockDailyWeatherDetails;
        }

        /// <summary>
        /// Get the self daily weather details.
        /// </summary>
        /// <param name="processingOptions"></param>
        /// <param name="groupId">The current GroupId</param>
        /// <param name="weatherDetailsCount">The number of weather details found.</param>
        /// <param name="amiStartDate">The start date for the consumption data</param>
        /// <param name="amiEndDate">he end date for the consumption data</param> 
        public ExtremeDaySelfComparison(ProcessingOptions processingOptions, int groupId, DateTime amiStartDate, DateTime amiEndDate, out int weatherDetailsCount)
        {
            try
            {
                // Get the Warehouse connection
                var insightsDataWarehouse = new InsightsDataWarehouse(processingOptions.ShardName);

                // Get the weather station Ids
                var stationIds = insightsDataWarehouse.GetStationIdsAndPostalCodes(processingOptions.ClientId, groupId).Select(m => m.StationIdDaily).Distinct().ToList();

                Logger.InfoFormat("Using the weather station Ids - {0}", string.Join(", ", stationIds));

                // Get the temperature type (hot or cold)
                var temperatureType =
                    processingOptions.ReportSettings.First(
                        m => m.Name.Equals("temperature_type", StringComparison.InvariantCultureIgnoreCase)).Value;

                // Get the number of extreme days desired.
                var numberOfExtremeDays = int.Parse(processingOptions.ReportSettings.First(
                        m => m.Name.Equals("self_number_of_extreme_days", StringComparison.InvariantCultureIgnoreCase))
                    .Value);

                // Get the extreme weather details.
                _extremeDailyWeatherDetails = insightsDataWarehouse.GetDailyWeatherDetails(stationIds, temperatureType,
                    numberOfExtremeDays, amiStartDate, amiEndDate).ToList();

                // Set the out variable.
                weatherDetailsCount = _extremeDailyWeatherDetails.Count();

                var datesChosen = string.Join(", ",
                    _extremeDailyWeatherDetails.Select(m => m.WeatherReadingDate.ToString("g")));

                Logger.InfoFormat(
                    "Retrieved {0} weather details for temperature type {1} from {2} to {3} - specifically the dates {4}",
                    weatherDetailsCount, temperatureType, amiStartDate, amiEndDate, datesChosen);
            }
            catch (Exception eX)
            {
                if (
                    !(eX is ApplicationException &&
                      eX.Message.Equals("Handled", StringComparison.InvariantCultureIgnoreCase)))
                {
                    Logger.ErrorFormat("Error encountered during ExtremeDaySelfComparison setup - {0} - {1}", eX.Message,
                        eX);
                    throw new ApplicationException("Handled");
                }

                weatherDetailsCount = 0;
            }
        }



        /// <summary>
        /// Calculate the benchmarks
        /// </summary>
        /// <param name="comparisonDataForGroup"></param>
        /// <param name="benchmarkResults"></param>
        /// <param name="processingOptions"></param>
        public void ProcessSelfBenchmarks(List<ComparisonDataModel> comparisonDataForGroup,
            List<WSBenchmarkResult> benchmarkResults, ProcessingOptions processingOptions)
        {

            try
            {
                // Get the minimum number of extreme day AMI data required
                var extremeDayAmiDataNeeded = int.Parse(processingOptions.ReportSettings.First(
                            m =>
                                m.Name.Equals("ami_min_extreme_data_points",
                                    StringComparison.InvariantCultureIgnoreCase))
                        .Value);

                // Calculate the MyHomeUse
                foreach (var comparisonData in comparisonDataForGroup)
                {
                    // Find the AMI data for the extreme days
                    var extremeCompData = comparisonData.Data.Where(
                        m =>
                            _extremeDailyWeatherDetails.Select(n => n.WeatherReadingDate.ToShortDateString())
                                .ToList()
                                .Contains(DateTime.Parse(m.DateTime).ToShortDateString())).ToList();

                    // Find the AMI data for the normal days
                    var normalCompData = comparisonData.Data.Where(
                        m =>
                            !_extremeDailyWeatherDetails.Select(n => n.WeatherReadingDate.ToShortDateString())
                                .ToList()
                                .Contains(DateTime.Parse(m.DateTime).ToShortDateString())).ToList();

                    // Calculate the MyHomeUse
                    if (extremeCompData.Count >= extremeDayAmiDataNeeded)
                    {
                        comparisonData.MyUsageExtremeDay =
                            decimal.Parse(extremeCompData.Average(m => m.Value).ToString(CultureInfo.InvariantCulture));
    }
                    else
                    {
                        Logger.InfoFormat("Missing extreme day comparison data for {0} in groupId {1}",
                            comparisonData.CustomerId, comparisonData.GroupId);
                    }

                    // Calculate the Normal usage
                    if (normalCompData.Any())
                    {
                        comparisonData.MyUsageNormalDay =
                            decimal.Parse(normalCompData.Average(m => m.Value).ToString(CultureInfo.InvariantCulture));
                    }
                    else
                    {
                        Logger.InfoFormat("Missing nomral day comparison data for {0} in groupId {1}",
                            comparisonData.CustomerId, comparisonData.GroupId);
                    }

                    // Get the proper Benchmark results object
                    var benchmarkResult = benchmarkResults.First(m => m.ClientId == processingOptions.ClientId &&
                                                                      m.CustomerId.Equals(comparisonData.CustomerId,
                                                                          StringComparison.InvariantCultureIgnoreCase) &&
                                                                      m.AccountId.Equals(comparisonData.AccountId,
                                                                          StringComparison.InvariantCultureIgnoreCase) &&
                                                                      m.PremiseId.Equals(comparisonData.PremiseId,
                                                                          StringComparison.InvariantCultureIgnoreCase) &&
                                                                      m.GroupId == comparisonData.GroupId &&
                                                                      string.IsNullOrEmpty(m.Self_PcntBenchmarkText));

                    // Calculate the SelfBenchmark
                    RunCalculations(benchmarkResult, comparisonData);
                    ////var compareOne = decimal.Zero;
                    ////if (comparisonData.MyUsageExtremeDay != decimal.Zero &&
                    ////    comparisonData.MyUsageNormalDay != decimal.Zero)
                    ////{
                    ////    compareOne = comparisonData.MyUsageExtremeDay / comparisonData.MyUsageNormalDay;
                    ////}

                    ////// Add peer benchmark value
                    ////if (compareOne == decimal.Zero && comparisonData.MyUsageExtremeDay != decimal.Zero)
                    ////{
                    ////    benchmarkResult.Self_PcntBenchmarkValue = "0";
                    ////    benchmarkResult.Self_PcntBenchmarkText = "percent";
                    ////}
                    ////else if (compareOne >= 2)
                    ////{
                    ////    benchmarkResult.Self_PcntBenchmarkValue =
                    ////        Math.Round((Math.Round(compareOne * 2, MidpointRounding.AwayFromZero) / 2), 1,
                    ////            MidpointRounding.AwayFromZero).ToString(
                    ////            CultureInfo.InvariantCulture);
                    ////    benchmarkResult.Self_PcntBenchmarkText = "x";
                    ////}
                    ////else if (compareOne < 2)
                    ////{
                    ////    benchmarkResult.Self_PcntBenchmarkValue =
                    ////        (Math.Round(Math.Round((compareOne - 1) * 100, 0, MidpointRounding.AwayFromZero) / 10) * 10).ToString(
                    ////            CultureInfo.InvariantCulture);
                    ////    benchmarkResult.Self_PcntBenchmarkText = "percent";
                    ////}

                    ////// Add misc data to benchmark results
                    ////benchmarkResult.Self_MyNormalDayUse = Math.Round(comparisonData.MyUsageNormalDay, 2,
                    ////    MidpointRounding.AwayFromZero);
                    ////benchmarkResult.Self_MyExtremeDayUse = Math.Round(comparisonData.MyUsageExtremeDay, 2,
                    ////    MidpointRounding.AwayFromZero);

                    Logger.DebugFormat("Self for CustomerId= {0}, PremiseId= {1}, and AccountId= {2} -> My Usage Normal day= {3} "
                            + ": Max Usage on Extreme day= {4} "
                            + ": Peer Benchmark Value= {5} : Peer Benchmark Text= {6}",
                        benchmarkResult.CustomerId, benchmarkResult.PremiseId, benchmarkResult.AccountId,
                        comparisonData.MyUsageNormalDay, comparisonData.MyUsageExtremeDay,
                        benchmarkResult.Self_PcntBenchmarkValue,
                        benchmarkResult.Self_PcntBenchmarkText);
                }

                InitalizeLog4Net.FlushBuffers();
            }
            catch (Exception eX)
            {
                if (
                    !(eX is ApplicationException &&
                      eX.Message.Equals("Handled", StringComparison.InvariantCultureIgnoreCase)))
                {
                    Logger.ErrorFormat("Error encountered during ProcessSelfBenchmarks - {0} - {1}", eX.Message,
                        eX);
                    throw new ApplicationException("Handled");
                }
            }
        }

        /// <summary>
        /// Run the Self Benchmark Results comparison calculations.
        /// </summary>
        /// <param name="benchmarkResult">The current benchmark results object.</param>
        /// <param name="comparisonData">The current comparison data object.</param>
        public void RunCalculations(WSBenchmarkResult benchmarkResult, ComparisonDataModel comparisonData)
        {
            // Calculate the SelfBenchmark
            var compareOne = decimal.Zero;
            if (comparisonData.MyUsageExtremeDay != decimal.Zero &&
                comparisonData.MyUsageNormalDay != decimal.Zero)
            {
                compareOne = comparisonData.MyUsageExtremeDay / comparisonData.MyUsageNormalDay;
            }

            // Add peer benchmark value
            if (compareOne == decimal.Zero && comparisonData.MyUsageExtremeDay != decimal.Zero)
            {
                benchmarkResult.Self_PcntBenchmarkValue = "0";
                benchmarkResult.Self_PcntBenchmarkText = "percent";
            }
            else if (compareOne >= 2)
            {
                benchmarkResult.Self_PcntBenchmarkValue =
                    Math.Round((Math.Round(compareOne * 2, MidpointRounding.AwayFromZero) / 2), 1,
                        MidpointRounding.AwayFromZero).ToString(
                        CultureInfo.InvariantCulture);
                benchmarkResult.Self_PcntBenchmarkText = "X";
            }
            else if (compareOne < 2)
            {
                benchmarkResult.Self_PcntBenchmarkValue =
                    (Math.Round(Math.Round((compareOne - 1) * 100, 2, MidpointRounding.AwayFromZero) / 10, MidpointRounding.AwayFromZero) * 10)
                    .ToString(CultureInfo.InvariantCulture);
                benchmarkResult.Self_PcntBenchmarkText = "percent";
            }

            // Add misc data to benchmark results
            benchmarkResult.Self_MyNormalDayUse = Math.Round(comparisonData.MyUsageNormalDay, 2,
                MidpointRounding.AwayFromZero);
            benchmarkResult.Self_MyExtremeDayUse = Math.Round(comparisonData.MyUsageExtremeDay, 2,
                MidpointRounding.AwayFromZero);
        }
    }
}
