﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Text;
using System.Threading;
using CE.WeatherSensitivity.Common.Logging;
using CE.WeatherSensitivity.Common.Models;
using CE.WeatherSensitivityBenchmarking.Infastructure.Models;
using InsightsDW.DataModel;
using InsightsDW.DataModel.DataAccess;
using log4net;
using Newtonsoft.Json;
using RestSharp;

namespace CE.WeatherSensitivityBenchmarking.Infastructure.Classes
{

    public class ConsumptionDataGathering
    {
        private static readonly ILog Logger = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        /// <summary>
        /// Perform the API calls to get Meter data.
        /// </summary>
        /// <param name="processingOptions">The processing options</param>
        /// <param name="groupId">The group Id</param>
        /// <param name="amiStartDate">The start date for the consumption data</param>
        /// <param name="amiEndDate">he end date for the consumption data</param>
        /// <param name="comparisonDataForGroup">The consumption data for the Group</param>
        /// <param name="minNumberOfAmiDataNeeded">The minimum number of AMI data points needed</param>
        public void GetConsumptionData(ProcessingOptions processingOptions, int groupId, DateTime amiStartDate,
            DateTime amiEndDate, List<ComparisonDataModel> comparisonDataForGroup, int minNumberOfAmiDataNeeded)
        {

            try
            {
                // Get the Warehouse connection
                var insightsDataWarehouse = new InsightsDataWarehouse(processingOptions.ShardName);

                var consumptionStopwatch = new Stopwatch();
                consumptionStopwatch.Start();

                var memberCount = insightsDataWarehouse.GetWsBenchmarkGroupMemberCount(processingOptions.ClientId, groupId);
                Logger.InfoFormat("Start Consumption Data Gathering for Group {0} - Members {1} via InsightsAPI", groupId, memberCount);

                // Get the consumption API parameters for requesting AMI data.
                var commodity =
                    processingOptions.ReportSettings.First(
                        m => m.Name.Equals("commodity", StringComparison.InvariantCultureIgnoreCase)).Value;


                var apiStopWatch = new Stopwatch();
                apiStopWatch.Start();

                var consumptionApiParameters =
                    insightsDataWarehouse.GetConsumptionApiParameters(processingOptions.ClientId, groupId, commodity)
                        .ToList();

                apiStopWatch.Stop();
                Logger.InfoFormat(
                    "Time Elapsed {0} to get GetConsumptionApiParameters for API calls for GroupId {1} - found data for {2}",
                    apiStopWatch.Elapsed, groupId, consumptionApiParameters.Count);

                // Get consumption data. 
                var apiParamCount = 0;
                var notEnoughConsumptionData = 0;
                var noComsumptionData = 0;
                foreach (var consumptionApiParameterCol in consumptionApiParameters)
                {
                    apiParamCount++;

                    // Add 0 to front of meterIds
                    AddMeterIdWithLeadingZero(consumptionApiParameterCol);

                    string responseContent; // Used for unit tests
                    var consumptionData = CallInsightsApi(consumptionApiParameterCol, processingOptions.ClientId,
                        amiStartDate, amiEndDate, commodity, out responseContent);


                    if (consumptionData?.Data == null)
                    {
                        if (consumptionData != null)
                        {
                            Logger.DebugFormat(
                                "No AMI data found for AccountId={0}, CustomerId={1}, PremiseId={2}, and MeterIds={3}",
                                consumptionApiParameterCol.AccountId, consumptionApiParameterCol.CustomerId,
                                consumptionApiParameterCol.PremiseId, consumptionApiParameterCol.MeterIds);
                        }

                        noComsumptionData++;
                    }
                    else if (consumptionData.Data.Count <= minNumberOfAmiDataNeeded)
                    {
                        Logger.DebugFormat(
                            "Not enough AMI data found for AccountId={0}, CustomerId={1}, PremiseId={2}, and MeterIds={3} - found {4} data points",
                            consumptionData.AccountId, consumptionData.CustomerId, consumptionData.PremiseId,
                            consumptionApiParameterCol.MeterIds, consumptionData.Data.Count);

                        notEnoughConsumptionData++;
                    }

                    else
                    {
                        var comparisonData = new ComparisonDataModel
                        {
                            AccountId = consumptionData.AccountId,
                            CustomerId = consumptionData.CustomerId,
                            PremiseId = consumptionData.PremiseId,
                            CommodityKey = consumptionData.CommodityKey,
                            UomKey = consumptionData.UOMKey,
                            GroupId = groupId,
                            Data = consumptionData.Data
                        };


                        comparisonDataForGroup.Add(comparisonData);
                    }
                }

                InitalizeLog4Net.FlushBuffers();

                consumptionStopwatch.Stop();
                Logger.InfoFormat(
                    "Time Elapsed {0} to get Consumption Data via InsightsAPI calls for GroupId {1} - {2} members - valid data for {3} - no data for {4} - not enough data for {5}",
                    consumptionStopwatch.Elapsed, groupId, apiParamCount, comparisonDataForGroup.Count,
                    noComsumptionData, notEnoughConsumptionData);
            }
            catch (Exception eX)
            {
                if (
                    !(eX is ApplicationException &&
                      eX.Message.Equals("Handled", StringComparison.InvariantCultureIgnoreCase)))
                {
                    Logger.ErrorFormat("Error encountered during GetConsumptionData - {0} - {1}", eX.Message, eX);
                    throw new ApplicationException("Handled");
                }

            }
        }

        /// <summary>
        /// Sometimes the meterIds are lead by a zero that has been removed in the bills.
        /// Therefore, sql server does not match cassandra.
        /// </summary>
        /// <param name="parameters">An instance of ConsumptionApiParameters.</param>
        private void AddMeterIdWithLeadingZero(WSRetrieveConsumptionAPIParameters_Result parameters)
        {
            var allMeterIds = string.Empty;
            foreach (var meterId in parameters.MeterIds.Trim().Split(','))
            {
                int intMeterId;
                if (int.TryParse(meterId, out intMeterId))
                {
                    if (!string.IsNullOrEmpty(allMeterIds))
                    {
                        allMeterIds = allMeterIds + ",";
                    }
                    allMeterIds = allMeterIds + meterId.Trim() + ",0" + meterId.Trim();
                }
            }

            parameters.MeterIds = allMeterIds;
        }

        /// <summary>
        /// Call the Consumption API can get the data.
        /// </summary>
        /// <param name="consumptionApiParameterCol">The parameters needed for the Consumption API</param>
        /// <param name="clientId">The clientId</param>
        /// <param name="amiStartDateTime">The start date for the AMI data.</param>
        /// <param name="amiEndDateTime">The end data for the AMI data.</param>
        /// <param name="commodity">The commodity</param>
        /// <param name="responseContent">USED FOR UNIT TESTS - the content of the response</param>
        /// <returns>The Consumption data</returns>
        public ConsumptionApiModels.RootObject CallInsightsApi(
            WSRetrieveConsumptionAPIParameters_Result consumptionApiParameterCol, int clientId, DateTime amiStartDateTime,
            DateTime amiEndDateTime, string commodity, out string responseContent)
        {

            // {{ url}}/api/v2/consumption? 
            // CustomerId = 993774751 & 
            // AccountId = 1218337093 & 
            // PremiseId = 1218337000 & 
            // MeterIds = M89354704,M189354705,M1218337000 & 
            // StartDate = 10 / 12 / 2015  12:00:00 AM & 
            // EndDate = 10 / 11 / 2016  12:00:00 AM & 
            // CommodityKey = gas & 
            // Count = 31 & 
            // ResolutionKey = day

            // Get the current users information
            var insights = new Insights.DataModel.DataAccess.Insights();
            var userInfo = insights.GetUserInfo(clientId, ApiHelpers.GetCurrentEnvironment(),
                ApiHelpers.GetCEInsightsAPI_Endpoint());

            // Create a request using a URL that can receive a post. 
            var client = new RestClient(ApiHelpers.GetCEInsightsAPI_BaseUrl());

            // Set the Method property of the request to POST.
            // Set the endpoint
            var request = new RestRequest(Method.GET) {Resource = userInfo.Endpoint};

            // Set the authorization
            var authHeader = $"Basic {ApiHelpers.Base64Encode(userInfo.CeAccessKeyId + ":" + userInfo.BasicKey)}";
            request.AddParameter("Authorization", authHeader, ParameterType.HttpHeader);
            request.AddParameter("X-CE-AccessKeyId", userInfo.CeAccessKeyId, ParameterType.HttpHeader);

            // Remove all spaces from meters
            var meters = string.Join("",
                consumptionApiParameterCol.MeterIds.Split(default(string[]), StringSplitOptions.RemoveEmptyEntries));

            // Build the Query string
            request.AddParameter("CustomerId", consumptionApiParameterCol.CustomerId, ParameterType.GetOrPost);
            request.AddParameter("AccountId", consumptionApiParameterCol.AccountId, ParameterType.GetOrPost);
            request.AddParameter("PremiseId", consumptionApiParameterCol.PremiseId, ParameterType.GetOrPost);
            request.AddParameter("CommodityKey", commodity.Trim(), ParameterType.GetOrPost);
            request.AddParameter("MeterIds", meters, ParameterType.GetOrPost);
            request.AddParameter("StartDate", amiStartDateTime.ToString("g"), ParameterType.GetOrPost);
            request.AddParameter("EndDate", amiEndDateTime.ToString("g"), ParameterType.GetOrPost);
            request.AddParameter("ResolutionKey", "day", ParameterType.GetOrPost);
            request.AddParameter("Count", "31", ParameterType.GetOrPost);

            try
            {

                responseContent = string.Empty;

                for (var count = 0; count < 4; count = count + 1)
                {

                    // execute response
                    var jsonResponse = client.Execute(request);
                    responseContent = jsonResponse.Content;

                    if (!responseContent.Contains("No data found"))
                    {
                        break;
                    }

                    Thread.Sleep(2000);
                }

                return JsonConvert.DeserializeObject<ConsumptionApiModels.RootObject>(responseContent);
            }
            catch (WebException eX)
            {
                var errorText = eX.Message;
                using (var responseStream = eX.Response.GetResponseStream())
                {
                    if (responseStream != null)
                    {
                        var reader = new StreamReader(responseStream, Encoding.GetEncoding("utf-8"));
                        var tempErrorMsg = reader.ReadToEnd();

                        if (!string.IsNullOrEmpty(tempErrorMsg))
                        {
                            errorText = tempErrorMsg;
                        }
                    }

                    Logger.ErrorFormat("Error encountered during CallInsightsApi - {0} - {1}", errorText, eX);
                    throw new ApplicationException("Handled");
                }
            }
        }
    }
}
