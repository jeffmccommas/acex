﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Reflection;
using CE.WeatherSensitivity.Common.Logging;
using CE.WeatherSensitivity.Common.Models;
using CE.WeatherSensitivityBenchmarking.Infastructure.Models;
using InsightsDW.DataModel;
using InsightsDW.DataModel.DataAccess;
using log4net;

namespace CE.WeatherSensitivityBenchmarking.Infastructure.Classes
{

    //    Process Flow
    //      1.	Input a start date and end date
    //      2.	Find the 5 (configurable) days with the most extreme(hot or cold) daily temperature for that time period.
    //
    //      For each benchmark group:
    //      3.	Calculate MyHomeUse = Average each customer’s daily usage for the extreme days.
    //      4.	Calculate the AverageHomeUse = Average of the 45th to 55th percentile MyHomeUse values for the benchmark group. (The percentiles are configurable).
    //      5.	Calculate the EfficientHomeUse = The MyHomeUse value at the 15th percentile for the benchmark group. (The percentile is configurable).
    //      6.	Calculate PcntPeerBenchmark:
    //          a.Above average use: If MyHomeUse > AverageHomeUse then:
    //              i.If MyHomeUse/AverageHomeUse >= 2 then round it up to the closest half number.Append “X” for the text field.
    //              ii.If MyHomeUse/AverageHomeUse< 2 and then subtract 1 and round it to the nearest ten percent.Append “ percent” for the text field.
    //          b.Below average use: If MyHomeUse <= AverageHomeUse then:
    //              i.If MyHomeUse/EfficientHomeUse >= 2 then round it up to the closest half number.Append “X” for the text field.
    //              ii.If MyHomeUse/EfficienctHomeUse< 2 and then subtract 1 and round it to the nearest ten percent.Append “ percent” for the text field.

    public class ExtremeDayPeerComparison
    {
        private readonly IEnumerable<EMDailyWeatherDetail> _extremeDailyWeatherDetails;
        private static readonly ILog Logger = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        /// <summary>
        /// A stubbed out constructor for Unit Testing
        /// </summary>
        /// <param name="mockDailyWeatherDetails">Mock Weather Data</param>
        public ExtremeDayPeerComparison(IEnumerable<EMDailyWeatherDetail> mockDailyWeatherDetails)
        {
            _extremeDailyWeatherDetails = mockDailyWeatherDetails;
        }

        /// <summary>
        /// Get the extreme daily weather details.
        /// </summary>
        /// <param name="processingOptions">The processing options</param>
        /// <param name="groupId">The current GroupId</param>
        /// <param name="weatherDetailsCount">The number of weather details found.</param>
        /// <param name="amiStartDate">The start date for the consumption data</param>
        /// <param name="amiEndDate">he end date for the consumption data</param>
        public ExtremeDayPeerComparison(ProcessingOptions processingOptions, int groupId, DateTime amiStartDate, DateTime amiEndDate,  out int weatherDetailsCount)
        {
            try
            {
                // Get the Warehouse connection
                var insightsDataWarehouse = new InsightsDataWarehouse(processingOptions.ShardName);

                // Get the weather station Ids
                var stationIds =
                    insightsDataWarehouse.GetStationIdsAndPostalCodes(processingOptions.ClientId, groupId)
                        .Select(m => m.StationIdDaily).Distinct()
                        .ToList();

                Logger.InfoFormat("Using the weather station Ids - {0}", string.Join(", ", stationIds));

                // Get the temperature type (hot or cold)
                var temperatureType =
                    processingOptions.ReportSettings.First(
                        m => m.Name.Equals("temperature_type", StringComparison.InvariantCultureIgnoreCase)).Value;

                // Get the number of extreme days desired.
                var numberOfExtremeDays = int.Parse(processingOptions.ReportSettings.First(
                    m => m.Name.Equals("extreme_number_of_days", StringComparison.InvariantCultureIgnoreCase)).Value);

                // Get the extreme weather details.
                _extremeDailyWeatherDetails =
                    insightsDataWarehouse.GetDailyWeatherDetails(stationIds, temperatureType, numberOfExtremeDays,
                        amiStartDate, amiEndDate).ToList();

                // Set the out variable.
                weatherDetailsCount = _extremeDailyWeatherDetails.Count();

                var datesChosen = string.Join(", ",
                    _extremeDailyWeatherDetails.Select(m => m.WeatherReadingDate.ToString("g")));

                Logger.InfoFormat(
                    "Retrieved {0} weather details for temperature type {1} from {2} to {3} - specifically the dates {4}",
                    weatherDetailsCount, temperatureType, amiStartDate, amiEndDate, datesChosen);
            }
            catch (Exception eX)
            {
                if (
                    !(eX is ApplicationException &&
                      eX.Message.Equals("Handled", StringComparison.InvariantCultureIgnoreCase)))
                {
                    Logger.ErrorFormat("Error encountered during ExtremeDayPeerComparison setup - {0} - {1}", eX.Message,
                        eX);
                    throw new ApplicationException("Handled");
                }

                weatherDetailsCount = 0;
            }
        }



        /// <summary>
        /// Calculate the benchmarks
        /// </summary>
        /// <param name="comparisonDataForGroup"></param>
        /// <param name="benchmarkResults"></param>
        /// <param name="processingOptions"></param>
        public void ProcessExtremeDayBenchmarks(List<ComparisonDataModel> comparisonDataForGroup,
            List<WSBenchmarkResult> benchmarkResults, ProcessingOptions processingOptions)
        {

            try
            {
                // Get the minimum number of extreme day AMI data required
                var extremeDayAmiDataNeeded = int.Parse(processingOptions.ReportSettings.First(
                            m =>
                                m.Name.Equals("ami_min_extreme_data_points",
                                    StringComparison.InvariantCultureIgnoreCase))
                        .Value);

                // Get the max average percentile border
                var maxAverageHomeUsePercentile =
                    decimal.Parse(processingOptions.ReportSettings.First(
                            m =>
                                m.Name.Equals("extreme_max_average_percentile",
                                    StringComparison.InvariantCultureIgnoreCase))
                        .Value) / 100;

                // Get the min average percentile border
                var minAverageHomeUsePercentile =
                    decimal.Parse(processingOptions.ReportSettings.First(
                            m =>
                                m.Name.Equals("extreme_min_average_percentile",
                                    StringComparison.InvariantCultureIgnoreCase))
                        .Value) / 100;

                // Get the efficent home use percentage
                var efficentHomeUsePercentile =
                    decimal.Parse(processingOptions.ReportSettings.First(
                            m =>
                                m.Name.Equals("extreme_efficent_home_use_percentile",
                                    StringComparison.InvariantCultureIgnoreCase))
                        .Value) / 100;

                // Calculate the MyHomeUse
                var validComparisonDataForGroup = new List<ComparisonDataModel>();
                var groupId = 0;
                foreach (var comparisonData in comparisonDataForGroup)
                {
                    groupId = comparisonData.GroupId;

                    // Find the AMI data for the extreme days
                    var extremeCompData = comparisonData.Data.Where(
                        m =>
                            _extremeDailyWeatherDetails.Select(n => n.WeatherReadingDate.ToShortDateString())
                                .ToList()
                                .Contains(DateTime.Parse(m.DateTime).ToShortDateString())).ToList();



                    if (extremeCompData.Count >= extremeDayAmiDataNeeded)
                    {
                        // Calculate the MyHomeUse
                        comparisonData.MyUsageExtremeDay =
                            decimal.Parse(extremeCompData.Average(m => m.Value).ToString(CultureInfo.InvariantCulture));

                        validComparisonDataForGroup.Add(comparisonData);
                    }
                    else
                    {
                        Logger.DebugFormat("Missing extreme day comparison data for {0} in groupId {1}",
                            comparisonData.CustomerId, comparisonData.GroupId);
                    }
                }
                Logger.InfoFormat("Valid Comparison Data {0} for groupId {1}", validComparisonDataForGroup.Count,
                    groupId);

                if (!validComparisonDataForGroup.Any())
                {
                    return;
                }

                // Get the Max Usage on Extreme day
                var maxUsage = validComparisonDataForGroup.Max(m => m.MyUsageExtremeDay);

                // Get the Min Usage on Extreme day
                var minUsage = validComparisonDataForGroup.Min(m => m.MyUsageExtremeDay);

                // Get the max percentile border
                var maxAverageHomeUsePercentileBorder = (int) minUsage +
                                                        ((maxUsage - minUsage) * maxAverageHomeUsePercentile);

                // Get the min percentile border
                var minAverageHomeUsePercentileBorder = (int) minUsage +
                                                        ((maxUsage - minUsage) * minAverageHomeUsePercentile);

                // Calculate the efficent home usage.
                var efficentHomeUse = minUsage + (maxUsage - minUsage) * efficentHomeUsePercentile;

                // Calculate the average home use
                var averageHomeUseDatum = validComparisonDataForGroup.Where(
                    m =>
                        m.MyUsageExtremeDay >= minAverageHomeUsePercentileBorder &&
                        m.MyUsageExtremeDay <= maxAverageHomeUsePercentileBorder).ToList();

                var averageHomeUse = decimal.Zero;
                if (averageHomeUseDatum.Any())
                {
                    averageHomeUse = averageHomeUseDatum.Average(m => m.MyUsageExtremeDay);
                }
                else
                {
                    Logger.InfoFormat("No Average home use data for groupId {0}",
                        validComparisonDataForGroup[0].GroupId);
                }

                // Calculate the PeerPercentage
                foreach (var comparisonData in validComparisonDataForGroup)
                {
                    // Get the proper Benchmark results object
                    var benchmarkResult = benchmarkResults.First(m => m.ClientId == processingOptions.ClientId &&
                                                                      m.CustomerId.Equals(comparisonData.CustomerId,
                                                                          StringComparison.InvariantCultureIgnoreCase) &&
                                                                      m.AccountId.Equals(comparisonData.AccountId,
                                                                          StringComparison.InvariantCultureIgnoreCase) &&
                                                                      m.PremiseId.Equals(comparisonData.PremiseId,
                                                                          StringComparison.InvariantCultureIgnoreCase) &&
                                                                      m.GroupId == comparisonData.GroupId &&
                                                                      string.IsNullOrEmpty(
                                                                          m.ExtremeDay_PcntPeerBenchmarkText));

                    // Run the calculations
                    RunCalculations(benchmarkResult, comparisonData, averageHomeUse, efficentHomeUse);

                    Logger.DebugFormat(
                        "Peer For CustomerId= {0}, PremiseId= {1}, and AccountId= {2} -> My Usage Extreme day= {3} "
                        +
                        ": Max Usage on Extreme day= {4} : Min Usage on Extreme day= {5} : Max Average Home Use percentile border= {6} "
                        +
                        ": Min Average Home Use percentile border= {7} : Efficent home usage= {8} : Average home use=  {9} "
                        + ": Peer Benchmark Value=  {10} : Peer Benchmark Text= {11}",
                        benchmarkResult.CustomerId, benchmarkResult.PremiseId, benchmarkResult.AccountId,
                        comparisonData.MyUsageExtremeDay, maxUsage, minUsage, maxAverageHomeUsePercentileBorder,
                        maxAverageHomeUsePercentileBorder, efficentHomeUse, averageHomeUse,
                        benchmarkResult.ExtremeDay_PcntPeerBenchmarkValue,
                        benchmarkResult.ExtremeDay_PcntPeerBenchmarkText);

                }

                InitalizeLog4Net.FlushBuffers();
            }
            catch (Exception eX)
            {
                if (
                    !(eX is ApplicationException &&
                      eX.Message.Equals("Handled", StringComparison.InvariantCultureIgnoreCase)))
                {
                    Logger.ErrorFormat("Error encountered during ProcessExtremeDayBenchmarks - {0} - {1}", eX.Message,
                        eX);
                    throw new ApplicationException("Handled");
                }
            }
        }

        /// <summary>
        /// Run the Peer Benchmark Results comparison calculations.
        /// </summary>
        /// <param name="benchmarkResult">The current benchmark results object.</param>
        /// <param name="comparisonData">The current comparison data object.</param>
        /// <param name="averageHomeUse">The average home usage</param>
        /// <param name="efficentHomeUse">The efficent home usage</param>
        public void RunCalculations(WSBenchmarkResult benchmarkResult, ComparisonDataModel comparisonData,
            decimal averageHomeUse, decimal efficentHomeUse)
        {

            var compareOne = decimal.Zero;
            if (comparisonData.MyUsageExtremeDay != decimal.Zero)
            {
                if (comparisonData.MyUsageExtremeDay > averageHomeUse)
                {
                    if (averageHomeUse != decimal.Zero)
                    {
                        compareOne = comparisonData.MyUsageExtremeDay / averageHomeUse;
                    }
                }
                else
                {
                    if (efficentHomeUse != decimal.Zero)
                    {
                        compareOne = comparisonData.MyUsageExtremeDay / efficentHomeUse;
                    }
                }
            }

            // Add peer benchmark value
            if (compareOne == decimal.Zero && comparisonData.MyUsageExtremeDay != decimal.Zero)
            {
                benchmarkResult.ExtremeDay_PcntPeerBenchmarkValue = "0";
                benchmarkResult.ExtremeDay_PcntPeerBenchmarkText = "percent";
            }
            else if (compareOne >= 2)
            {
                benchmarkResult.ExtremeDay_PcntPeerBenchmarkValue =
                    Math.Round((Math.Round(compareOne * 2, MidpointRounding.AwayFromZero) / 2), 1,
                        MidpointRounding.AwayFromZero).ToString(
                        CultureInfo.InvariantCulture);
                benchmarkResult.ExtremeDay_PcntPeerBenchmarkText = "X";
            }
            else if (compareOne < 2)
            {
                benchmarkResult.ExtremeDay_PcntPeerBenchmarkValue =
                    (Math.Round(Math.Round((compareOne - 1) * 100, 0, MidpointRounding.AwayFromZero) / 10) * 10)
                    .ToString(
                        CultureInfo.InvariantCulture);
                benchmarkResult.ExtremeDay_PcntPeerBenchmarkText = "percent";
            }

            // Add misc data to benchmark results
            benchmarkResult.ExtremeDay_AverageHomeUse = Math.Round(averageHomeUse, 2,
                MidpointRounding.AwayFromZero);
            benchmarkResult.ExtremeDay_EfficientHomeUse = Math.Round(efficentHomeUse, 2,
                MidpointRounding.AwayFromZero);
            benchmarkResult.ExtremeDay_MyUsage = Math.Round(comparisonData.MyUsageExtremeDay, 2,
                MidpointRounding.AwayFromZero);
        }
    }
}
