﻿using System;
using System.Collections.Generic;
using System.Linq;
using CE.WeatherSensitivity.Common.Models;
using CE.WeatherSensitivity.Measures.Classes;
using InsightsDW.DataModel;
using InsightsDW.DataModel.DataAccess;
using WSWorkflowDataAccess.models;
using log4net;

namespace WSWorkflowDataAccess
{
    public class WeatherSensitivityDataAccess
    {
        private readonly InsightsDataWarehouse _warehouse;
        private readonly ProcessingOptions _processingOptions;
        private readonly ILog _logger;


        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="processingOptions">The processing options</param>
        /// <param name="logger">The current log</param>
        public WeatherSensitivityDataAccess(ProcessingOptions processingOptions, ILog logger)
        {
            _warehouse = new InsightsDataWarehouse(processingOptions.ShardName);
            _processingOptions = processingOptions;

            _logger = logger;

        }

        /// <summary>
        /// Get the Report Profile Name
        /// </summary>
        /// <returns>The report profile name</returns>
        public string GetProfileName()
        {
            _processingOptions.ProfileName = _warehouse.GetProfileName(_processingOptions.ReportAlias,
                _processingOptions.ClientId, _processingOptions.ReportRequestDate);


            // If no profile found exit.
            return string.IsNullOrEmpty(_processingOptions.ProfileName) ? null : _processingOptions.ProfileName;
        }

        /// <summary>
        /// Determine if a customer participates in audits (HES).
        /// </summary>
        /// <param name="premiseId">The customers premiseId</param>
        /// <param name="accountId">The customers accountId</param>
        /// <returns></returns>
        public int GetIsHesParticipant(string premiseId, string accountId)
        {
            return _warehouse.GetIsHesParticipant(premiseId, accountId);
        }

        /// <summary>
        /// Get the AMI start date.
        /// </summary>
        /// <returns>The ami start date.</returns>
        private DateTime? GetAmiStartDate()
        {
            DateTime? startdate = null;
            var effectiveStartDate = _processingOptions.ReportSettings.FirstOrDefault(x => x.Name.ToLower() == "ami_start_date");
            if (effectiveStartDate != null)
            {
                startdate = Convert.ToDateTime(effectiveStartDate.Value);
            }

            return startdate;
        }

        /// <summary>
        /// Get the ami end date
        /// </summary>
        /// <returns>The ami end date</returns>
        private DateTime GetAmiEndDate()
        {
            try
            {
                var effectiveEndDate = _processingOptions.ReportSettings.First(x => x.Name.ToLower() == "ami_end_date"); //will throw an exception if its empty in settings

                var endDate = Convert.ToDateTime(effectiveEndDate.Value);

                return endDate;
            }
            catch (Exception)
            {
                _logger.Fatal("[effective_end] not found or is null in WSReportSettings");
                throw;
            }

        }

        /// <summary>
        /// Get the report number
        /// </summary>
        /// <returns></returns>
        private string GetReportNumber()
        {
            var reportNumber = _processingOptions.ReportSettings.FirstOrDefault(x => x.Name.ToLower() == "report_number");

            if (reportNumber != null)
            {
                return reportNumber.Value;
            }

            _logger.Error("[report_number] not found or is null in WSReportSettings");
            return string.Empty;
        }

        /// <summary>
        /// /Get the report month
        /// </summary>
        /// <returns></returns>
        private string GetReportMonth()
        {
            var reportNumber = _processingOptions.ReportSettings.FirstOrDefault(x => x.Name.ToLower() == "report_month");

            if (reportNumber != null)
            {
                return reportNumber.Value;
            }

            _logger.Error("[report_number] not found or is null in WSReportSettings");
            return string.Empty;
        }

        /// <summary>
        /// Get the report year.
        /// </summary>
        /// <returns></returns>
        private string GetReportYear()
        {
            var reportNumber = _processingOptions.ReportSettings.FirstOrDefault(x => x.Name.ToLower() == "report_year");

            if (reportNumber != null)
            {
                return reportNumber.Value;
            }

            _logger.Error("[report_number] not found or is null in WSReportSettings");
            return string.Empty;
        }

        /// <summary>
        /// Get the email report. This includes measures and benchmarks.
        /// </summary>
        /// <returns>A list of WSEmailReportModel</returns>
        public List<WsEmailReportModel> GetEmailReport()
        {
            // Get the benchmarks
            var benchmarkReport = GetBenchmarkReport(GetProfileName()).ToList();

            // All records with “0” values for MyHomeUse, AverageHomeUse and EfficientHomeUse should be excluded from the Print report
            benchmarkReport =
                benchmarkReport.Where(
                    m =>
                        m.ExtremeDay_AverageHomeUse != 0 && m.ExtremeDay_EfficientHomeUse != 0 &&
                        m.ExtremeDay_MyUsage != 0).ToList();

            // Run the measures
            var runMeasures = new RunMeasures();
            var measureResults = runMeasures.RunTheMeasures(benchmarkReport, _processingOptions);

            // Generate the email report
            var emailReport = ReportMapper.MapBenchmarkResultToEmailReportModel(
                GetReportMonth(),
                GetReportYear(),
                GetAmiStartDate(),
                GetAmiEndDate(),
                measureResults,
                benchmarkReport);

            // return the final email job.
            return emailReport;
        }

        /// <summary>
        /// Get the benchmark results for a specific report profile.
        /// </summary>
        /// <param name="profileName">The report profile name</param>
        /// <returns></returns>
        private IEnumerable<WSBenchmarkResult> GetBenchmarkReport(string profileName)
        {
            return _warehouse.GetWsBenchmarkResults(_processingOptions.ClientId, profileName);
        }

        /// <summary>
        /// Get the print report. This includes measures and benchmarks.
        /// </summary>
        /// <returns>A list of WSPrintReportModel</returns>
        public List<WsPrintReportModel> GetPrintReport()
        {
            // Setup the return print report
            var result = new List<WsPrintReportModel>();

            // Get the benchmarks
            var benchmarkReport = GetBenchmarkReport(GetProfileName()).ToList();

            // All records with “0” values for MyHomeUse, AverageHomeUse and EfficientHomeUse should be excluded from the Print report
            benchmarkReport =
                benchmarkReport.Where(
                    m =>
                        m.ExtremeDay_AverageHomeUse != 0 && m.ExtremeDay_EfficientHomeUse != 0 &&
                        m.ExtremeDay_MyUsage != 0).ToList();


            // Run the measures
            var runMeasures = new RunMeasures();
            var measureResults = runMeasures.RunTheMeasures(benchmarkReport, _processingOptions);

            // Generate the print report       
            var printReport = ReportMapper.MapBenchmarkResultToPrintReportModel(
                GetReportMonth(),
                GetReportYear(),
                GetAmiStartDate(), 
                GetAmiEndDate(), 
                measureResults, 
                benchmarkReport);

          
            return printReport;
        }

    }
}
