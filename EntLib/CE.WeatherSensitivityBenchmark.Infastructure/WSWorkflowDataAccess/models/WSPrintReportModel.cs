﻿using System;

namespace WSWorkflowDataAccess.models
{
    public class WsPrintReportModel
    {
        public string AccountNumber { get; set; }

        public string CustomerId { get; set; }

        public string PremiseId { get; set; }

        public string ReferrerId { get; set; }

        public string CustomerName { get; set; }

        public string MailingAddress { get; set; }

        public string MailingAddress2 { get; set; }

        public string MailingCity { get; set; }

        public string MailingState { get; set; }

        public string MailingZip { get; set; }

        public string ProgramReportNumber { get; set; }

        public string SegmentId { get; set; }

        public string HesParticipant { get; set; }

        public string ReportMonth { get; set; }

        public string ReportYear { get; set; }

        public DateTime? ReportPeriodStart { get; set; }

        public DateTime ReportPeriodEnd { get; set; }

        public string MaskedAccountNumber { get; set; }

        public string MyHomeUse { get; set; }

        public string AverageHomeUse { get; set; }

        public string EfficientHomeUse { get; set; }

        public string PcntPeerBenchmark { get; set; }

        public string PcntPeerBenchmarkText { get; set; }

        public string MeasureName1 { get; set; }

        public string MeasureSavings1 { get; set; }

        public string MeasureName2 { get; set; }

        public string MeasureSavings2 { get; set; }

        public string MeasureName3 { get; set; }

        public string MeasureSavings3 { get; set; }

        public string MyNormalDayUse { get; set; }

        public string MyExtremeDayUse { get; set; }

        public string PcntSelfBenchmark { get; set; }

        public string PcntSelfBenchmarkText { get; set; }
    }
}
