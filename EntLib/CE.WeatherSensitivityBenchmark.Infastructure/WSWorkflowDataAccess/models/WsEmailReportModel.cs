﻿using System;
using System.Collections.Generic;

namespace WSWorkflowDataAccess.models
{
    public class WsEmailReportModel
    {
        public string SubscriberKey { get; set; }

        public string AccountNumber { get; set; }

        public string CustomerId { get; set; }

        public string PremiseId { get; set; }

        public string ReferrerId { get; set; }

        public string Email { get; set; }

        public string ProgramReportNumber { get; set; }

        public string SegmentId { get; set; }

        public string HesParticipant { get; set; }

        public string ReportMonth { get; set; }

        public string ReportYear { get; set; }

        public DateTime? ReportPeriodStart { get; set; }

        public DateTime ReportPeriodEnd { get; set; }

        public string MaskedAccountNumber { get; set; }

        public string MyHomeUse { get; set; }

        public string AverageHomeUse { get; set; }

        public string EfficientHomeUse { get; set; }

        public string PcntPeerBenchmark { get; set; }

        public string PcntPeerBenchmarkText { get; set; }

        public string MeasureName1 { get; set; }

        public string MeasureSavings1 { get; set; }

        public string MeasureName2 { get; set; }

        public string MeasureSavings2 { get; set; }

        public string MeasureName3 { get; set; }

        public string MeasureSavings3 { get; set; }

        
    }

    public class WsForecastModel
    {
        public DateTime TodaysDate { get; set; }
        public string WeatherAttribute { get; set; }
        public string ThresholdValue { get; set; }
        public List<WsZoneForecastModel> ForecastByZone { get; set; }
        
    }
    public class WsZoneForecastModel
    {
        public string ZipCode { get; set; }
        public string Name { get; set; }
        public List<string> Forecast { get; set; }

    }


}
