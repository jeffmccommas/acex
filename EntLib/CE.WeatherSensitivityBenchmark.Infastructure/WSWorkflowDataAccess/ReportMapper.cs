﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using InsightsDW.DataModel;
using WSWorkflowDataAccess.models;

namespace WSWorkflowDataAccess
{
    public static class ReportMapper
    {
        /// <summary>
        /// Load the email reports
        /// </summary>
        /// <param name="reportMonth">The report month</param>
        /// <param name="reportYear">The report year</param>
        /// <param name="amiStartDate">The report period start date</param>
        /// <param name="amiEndDate">The report period end date</param>
        /// <param name="wsMeasuresStaging">The measure staging results</param>
        /// <param name="benchmarkResults">The benchmark results</param>
        /// <returns></returns>
        public static List<WsEmailReportModel> MapBenchmarkResultToEmailReportModel(
            string reportMonth,
            string reportYear,
            DateTime? amiStartDate,
            DateTime amiEndDate,
            IEnumerable<WS_Measures_StagingAllColumns> wsMeasuresStaging,
            List<WSBenchmarkResult> benchmarkResults)
        {
            var result = new List<WsEmailReportModel>();

            foreach (var measureResult in wsMeasuresStaging)
            {

                var benchmarkResult = benchmarkResults
                    .FirstOrDefault(
                        m =>
                            m.CustomerId.Equals(measureResult.CustomerID, StringComparison.InvariantCultureIgnoreCase) &&
                            m.AccountId.Equals(measureResult.Account_Number, StringComparison.InvariantCultureIgnoreCase) &&
                            m.PremiseId.Equals(measureResult.PremiseID, StringComparison.InvariantCultureIgnoreCase));

                if (benchmarkResult != null)
                {

                    result.Add(new WsEmailReportModel
                    {
                        SubscriberKey = measureResult.Subscriber_Key,
                        AccountNumber = measureResult.Account_Number,
                        CustomerId = measureResult.CustomerID,
                        PremiseId = measureResult.PremiseID,
                        ReferrerId = measureResult.ReferrerID,
                        Email = measureResult.Email,
                        ProgramReportNumber = measureResult.ProgramReportNumber,
                        ReportMonth = reportMonth,
                        ReportYear = reportYear,
                        ReportPeriodStart = amiStartDate, //ami data
                        ReportPeriodEnd = amiEndDate, //ami data
                        SegmentId = measureResult.GroupID,
                        HesParticipant = measureResult.HESParticipant,
                        MaskedAccountNumber = measureResult.MaskedAccountNumber,
                        MyHomeUse =
                            Math.Round(benchmarkResult.ExtremeDay_MyUsage, MidpointRounding.AwayFromZero)
                                .ToString(CultureInfo.InvariantCulture),
                        AverageHomeUse =
                            Math.Round(benchmarkResult.ExtremeDay_AverageHomeUse, MidpointRounding.AwayFromZero)
                                .ToString(CultureInfo.InvariantCulture),
                        EfficientHomeUse =
                            Math.Round(benchmarkResult.ExtremeDay_EfficientHomeUse, MidpointRounding.AwayFromZero)
                                .ToString(CultureInfo.InvariantCulture),
                        PcntPeerBenchmark = benchmarkResult.ExtremeDay_PcntPeerBenchmarkValue,
                        PcntPeerBenchmarkText = benchmarkResult.ExtremeDay_PcntPeerBenchmarkText,
                        MeasureName1 = measureResult.MID1,
                        MeasureSavings1 = measureResult.MSavings1,
                        MeasureName2 = measureResult.MID2,
                        MeasureSavings2 = measureResult.MSavings2,
                        MeasureName3 = measureResult.MID3,
                        MeasureSavings3 = measureResult.MSavings3
                    });
                }


            }
            return result;
        }

        /// <summary>
        /// Load the print reports
        /// </summary>
        /// <param name="reportMonth">The report month</param>
        /// <param name="reportYear">The report year</param>
        /// <param name="amiStartDate">The report period start date</param>
        /// <param name="amiEndDate">The report period end date</param>
        /// <param name="wsMeasuresStaging">The measure staging results</param>
        /// <param name="benchmarkResults">The benchmark results</param>
        /// <returns></returns>
        public static List<WsPrintReportModel> MapBenchmarkResultToPrintReportModel(
            string reportMonth,
            string reportYear,
            DateTime? amiStartDate,
            DateTime amiEndDate,
            IEnumerable<WS_Measures_StagingAllColumns> wsMeasuresStaging,
            List<WSBenchmarkResult> benchmarkResults)
        {
            var result = new List<WsPrintReportModel>();

            foreach (var measureResult in wsMeasuresStaging)
            {

                var benchmarkResult = benchmarkResults
                    .FirstOrDefault(
                        m =>
                            m.CustomerId.Equals(measureResult.CustomerID, StringComparison.InvariantCultureIgnoreCase) &&
                            m.AccountId.Equals(measureResult.Account_Number, StringComparison.InvariantCultureIgnoreCase) &&
                            m.PremiseId.Equals(measureResult.PremiseID, StringComparison.InvariantCultureIgnoreCase));

                if (benchmarkResult != null)
                {
                    result.Add(new WsPrintReportModel
                    {
                        AccountNumber = measureResult.Account_Number,
                        CustomerId = measureResult.CustomerID,
                        PremiseId = measureResult.PremiseID,
                        ReferrerId = measureResult.ReferrerID,
                        CustomerName = measureResult.Mailing_Name,
                        MailingAddress = measureResult.Mailing_Address,
                        MailingAddress2 = measureResult.Mailing_Address_2,
                        MailingCity = measureResult.Mailing_City,
                        MailingState = measureResult.Mailing_State,
                        MailingZip = measureResult.Mailing_Zip,
                        ProgramReportNumber = measureResult.ProgramReportNumber,
                        SegmentId = measureResult.GroupID,
                        HesParticipant = measureResult.HESParticipant,
                        ReportMonth = reportMonth,
                        ReportYear = reportYear,
                        ReportPeriodStart = amiStartDate, //ami data
                        ReportPeriodEnd = amiEndDate, //ami data
                        MaskedAccountNumber = measureResult.MaskedAccountNumber,
                        MyHomeUse =
                            Math.Round(benchmarkResult.ExtremeDay_MyUsage, MidpointRounding.AwayFromZero)
                                .ToString(CultureInfo.InvariantCulture),
                        AverageHomeUse =
                            Math.Round(benchmarkResult.ExtremeDay_AverageHomeUse, MidpointRounding.AwayFromZero)
                                .ToString(CultureInfo.InvariantCulture),
                        EfficientHomeUse =
                            Math.Round(benchmarkResult.ExtremeDay_EfficientHomeUse, MidpointRounding.AwayFromZero)
                                .ToString(CultureInfo.InvariantCulture),
                        PcntPeerBenchmark = benchmarkResult.ExtremeDay_PcntPeerBenchmarkValue,
                        PcntPeerBenchmarkText = benchmarkResult.ExtremeDay_PcntPeerBenchmarkText,
                        MeasureName1 = measureResult.MID1,
                        MeasureSavings1 = measureResult.MSavings1,
                        MeasureName2 = measureResult.MID2,
                        MeasureSavings2 = measureResult.MSavings2,
                        MeasureName3 = measureResult.MID3,
                        MeasureSavings3 = measureResult.MSavings3,
                        MyNormalDayUse =
                            Math.Round(benchmarkResult.Self_MyNormalDayUse, MidpointRounding.AwayFromZero)
                                .ToString(CultureInfo.InvariantCulture),
                        MyExtremeDayUse =
                            Math.Round(benchmarkResult.Self_MyExtremeDayUse, MidpointRounding.AwayFromZero)
                                .ToString(CultureInfo.InvariantCulture),
                        PcntSelfBenchmark = benchmarkResult.Self_PcntBenchmarkValue,
                        PcntSelfBenchmarkText = benchmarkResult.Self_PcntBenchmarkText,

                    });
                }


            }
            return result;
        }
    }
}
