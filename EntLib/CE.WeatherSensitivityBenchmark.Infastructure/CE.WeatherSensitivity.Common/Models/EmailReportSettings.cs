﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CE.WeatherSensitivity.Common.Models
{
    public class ForecastReportSettings
    {
        public string ZipCodeList { get; set; }
        public string EmailSubject { get; set; }
        public string ToEmailList { get; set; }
        public string ExtremeHotTempThreshold { get; set; }
        public string ExtremeColdTempThreshold { get; set; }
        public string TemperatureType { get; set; }
        public string ForecastTotalDays { get; set; }
        public string ForecastStartPeriod { get; set; }
        public string ForecastEndPeriod { get; set; }
        public string ForecastServiceUrl { get; set; }
        public string ForecastServiceKey { get; set; }

        public string ForecastEmailSmtpServer { get; set; }
        public string ForecastEmailSmtpPort { get; set; }
        public string ForecastEmailSmtpAuth { get; set; }
        public string ForecastEmailSmtpUser { get; set; }
        public string ForecastEmailSmtpPassword { get; set; }
        public string ForecastEmailSmtpSsl { get; set; }
        public string ForecastEmailFrom { get; set; }
        public string ForecastEmailFromName { get; set; }

        public string StorageAccountKey { get; set; }
        public string StorageAccountName { get; set; }
        public string StorageContainerName { get; set; }







    }
}
