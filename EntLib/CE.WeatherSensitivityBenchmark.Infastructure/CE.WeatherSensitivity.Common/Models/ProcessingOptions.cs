﻿using System;
using System.Collections.Generic;
using CommandLine;
using InsightsDW.DataModel;

namespace CE.WeatherSensitivity.Common.Models
{
    public class ProcessingOptions
    {

        /// <summary>
        /// The current clientId
        /// </summary>
        [Option('c', "clientId", Required = true, HelpText = "The ClientId.")]
        public int ClientId { get; set; }

        /// <summary>
        /// The shard to be used as the data repository
        /// </summary>
        public string ShardName { get; set; }

        /// <summary>
        /// The name of the profile being used for the report.
        /// </summary>
        public string ProfileName { get; set; }

        /// <summary>
        /// The alias for the report to be run.
        /// </summary>
        [Option('r', "reportalias", Required = true, HelpText = "The Report Alias.")]
        public string ReportAlias { get; set; }

        /// <summary>
        /// The time the report is running.
        /// </summary>
        [Option('d', "date", Required = true, HelpText = "The date the Report is requested.")]
        public DateTime ReportRequestDate { get; set; }

        public string IsTrialRun { get; set; }

        /// <summary>
        /// The report settings.
        /// </summary>
        public IEnumerable<WSReportSetting> ReportSettings { get; set; }
    }
}
