﻿using System.Data.SqlClient;
using System.Linq;
using log4net;
using log4net.Appender;
using log4net.Repository;
using log4net.Repository.Hierarchy;

namespace CE.WeatherSensitivity.Common.Logging
{
    public static class InitalizeLog4Net
    {
        /// <summary>
        /// Initalize the logger and change the connection string.
        /// </summary>
        /// <param name="initialCatalog">The initial catalog to be used in the connection string</param>
        public static void Initalize(string initialCatalog = "InsightsDW_Test")
        {
            //initialCatalog = "InsightsDW_224_2017-02-14T21-44Z";

            // Initialize log4net.
            log4net.Config.XmlConfigurator.Configure();

            //update connection string for log4net dynamically
            var hier = LogManager.GetRepository() as Hierarchy;

            if (hier != null)
            {
                var adoNetAppenders = hier.GetAppenders().OfType<AdoNetAppender>();
                foreach (var adoNetAppender in adoNetAppenders)
                {

                    // init the sqlbuilder with the full EF connectionstring cargo
                    var sqlCnxStringBuilder = new SqlConnectionStringBuilder
                        (adoNetAppender.ConnectionString) {InitialCatalog = initialCatalog};


                    adoNetAppender.ConnectionString = sqlCnxStringBuilder.ConnectionString;
                    adoNetAppender.ActivateOptions();
                }
            }
        }

        /// <summary>
        /// Get the Log4Net logger
        /// </summary>
        /// <param name="declaringType">The declaring type for the logger.</param>
        /// <returns></returns>
        public static ILog GetTheLogger(System.Type declaringType)
        {
            return LogManager.GetLogger(declaringType);
        }

        /// <summary>
        /// Flush all buffers.
        /// </summary>
        public static void FlushBuffers()
        {
            var rep = LogManager.GetRepository();
            foreach (var appender in rep.GetAppenders())
            {
                var buffered = appender as BufferingAppenderSkeleton;
                buffered?.Flush();
            }
        }
    }
}
