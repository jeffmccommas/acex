﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CE.WeatherSensitivity.Common.Classes
{
    public static class ReportHelper
    {
        public static string MaskTextData(string data, int visibleLength)
        {
            if (data.Length > visibleLength)
                return data.Substring(data.Length - visibleLength).PadLeft(data.Length, '*');
            else
            {
                return data;
            }

        }
    }
}
