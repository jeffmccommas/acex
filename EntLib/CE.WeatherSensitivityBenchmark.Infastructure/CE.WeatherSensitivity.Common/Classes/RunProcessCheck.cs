﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using CE.WeatherSensitivity.Common.Models;
using InsightsDW.DataModel;
using InsightsDW.DataModel.DataAccess;
using log4net;


namespace CE.WeatherSensitivity.Common.Classes
{
    /// <summary>
    /// Determine if the process should run. Based on [Export].[usp_HE_Report_RunReport]
    /// </summary>
    public static class RunProcessCheck
    {
        private static readonly ILog Logger = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        /// <summary>
        /// Determine the name the profile for the report to be acted upon.
        /// </summary>
        /// <param name="processingOptions">A Processing Option object</param>
        /// <returns></returns>
        public static void RunCheck(ProcessingOptions processingOptions)
        {
            try
            {
                // Get the warehouse entities.
                var warehouse = new InsightsDataWarehouse(processingOptions.ShardName,ConfigurationHelper.GetCommandTimeout());

                // Get the profile name.
                processingOptions.ProfileName = warehouse.GetProfileName(processingOptions.ReportAlias,
                    processingOptions.ClientId, processingOptions.ReportRequestDate);


                // If no profile found exit.
                if (string.IsNullOrEmpty(processingOptions.ProfileName))
                {
                    // Holla bloody murder
                    Logger.ErrorFormat("Error encountered during RunCheck - Profile name is empty. Check effective-end-date, client id and profile name");
                    throw new ApplicationException("Handled");
                }


                // Get the report settings.
                processingOptions.ReportSettings = warehouse.GetReportSettingInfo(processingOptions.ProfileName,
                    processingOptions.ClientId);
            }
            catch (Exception eX)
            {
                Logger.ErrorFormat("Error encountered during RunCheck - {0} - {1}", eX.Message, eX);
                throw new ApplicationException("Handled");
            }
        }

        public static List<WSApplicationSetting> RunApplicationCheck(string appName, string shardName, int clientId)
        {
            try
            {
                // Get the warehouse entities.
                var warehouse = new InsightsDataWarehouse(shardName, ConfigurationHelper.GetCommandTimeout());

                // Get the report settings.
                return warehouse.GetApplicationSettings(appName, clientId).ToList();

            }
            catch (Exception eX)
            {
                Logger.ErrorFormat("Error encountered during RunCheck - {0} - {1}", eX.Message, eX);
                throw new ApplicationException("Handled");
            }
        }

        public static List<WSRetrieveStationIdsAndPostalCodes_Result> CheckPostalCodes(string shardName, int clientId)
        {
            try
            {
                // Get the warehouse entities.
                var warehouse = new InsightsDataWarehouse(shardName, ConfigurationHelper.GetCommandTimeout());

                // Get the report settings.
                return warehouse.GetStationIdsAndPostalCodes(clientId,null).ToList();

            }
            catch (Exception eX)
            {
                Logger.ErrorFormat("Error encountered during CheckPostalCodes - {0} - {1}", eX.Message, eX);
                throw new ApplicationException("Handled");
            }
        }
    }
}
