﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CE.WeatherSensitivity.Common.Classes
{
    public class ConfigurationHelper
    {
        // Constants
        private const string AppSettingCeEnvironment = "CEEnvironment";
        private const string CeInsightsApiBaseUrl = "CEInsightsAPI_BaseURL";
        private const string CeInsightsApiEndPoint = "CEInsightsAPI_EndPoint";
        private const string CeInsightsApiEncrypt = "CEInsightsAPI_Encrypt";
        
        private const string ProcessingEnabled = "ProcessingEnabled";
        private const string ClientIdForDailyWebJob = "ClientId";
        private const string ApplicationName = "ApplicationName";
        private const string CommandTimeout = "CommandTimeout";


        public static int GetCommandTimeout()
        {
            return AppSettings.Get<int>(CommandTimeout);
        }
        /// <summary>
        /// Get the current enviornment 
        /// </summary>
        /// <returns></returns>
        public static string GetCurrentEnvironment()
        {
            return AppSettings.Get<string>(AppSettingCeEnvironment);
        }
        public static string GetCeInsightsApiBaseUrl()
        {
            return AppSettings.Get<string>(CeInsightsApiBaseUrl);
        }
        public static string GetCeInsightsApiEndPoint()
        {
            return AppSettings.Get<string>(CeInsightsApiEndPoint);
        }
        public static string GetCeInsightsApiEncrypt()
        {
            return AppSettings.Get<string>(CeInsightsApiEncrypt);
        }
        public static bool GetProcessingEnabled()
        {
            return AppSettings.Get<bool>(ProcessingEnabled);
        }
        public static int GetClientIdForDailyWebJob()
        {
            return AppSettings.Get<int>(ClientIdForDailyWebJob);
        }
        public static string GetClientIdListForDailyWebJob()
        {
            return AppSettings.Get<string>(ClientIdForDailyWebJob);
        }
        public static string GetApplicationName()
        {
            return AppSettings.Get<string>(ApplicationName);
        }
    }
    /// <summary>
    /// Type safe app settings class.
    /// </summary>
    public static class AppSettings
    {
        public static T Get<T>(string key)
        {
            var appSetting = ConfigurationManager.AppSettings[key];
            if (string.IsNullOrWhiteSpace(appSetting)) throw new SettingsPropertyNotFoundException(key);

            var converter = TypeDescriptor.GetConverter(typeof(T));
            return (T)(converter.ConvertFromInvariantString(appSetting));
        }
    }
}
