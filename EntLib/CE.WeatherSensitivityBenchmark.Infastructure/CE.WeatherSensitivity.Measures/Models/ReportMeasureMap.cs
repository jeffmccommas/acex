﻿namespace CE.WeatherSensitivity.Measures.Models
{

//  declare @rpt_measure_map    table
//  (
//      [sort_idx]              [int],
//      [rpt_profile_name]      [varchar](100), 
//      [measure_number]        [int],
//      [measure_profile_name]  [varchar](100)
//  );

    public class ReportMeasureMap
    {
        public string MeasureProfileName { get; set; }

        public int MeasureNumber { get; set; }
    }
}
