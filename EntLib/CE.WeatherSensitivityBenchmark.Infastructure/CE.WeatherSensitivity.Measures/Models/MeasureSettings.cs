﻿using System;
using System.Collections.Generic;

namespace CE.WeatherSensitivity.Measures.Models
{
    public class MeasureSettings
    {
        public Guid RunId { get; set; }

        public string ReportProfileName { get; set; }

        public string ReportYear { get; set; }

        public string ReportMonth { get; set; }

        public string ReportNumber { get; set; }

        public string ReportChannel { get; set; }

        public int ClientId { get; set; }

        public string FuelType { get; set; }

        public string EnergyObjectCategoryList { get; set; }

        public string Period { get; set; }

        public string SeasonList { get; set; }

        public int MeasureCount { get; set; }
            
        public string OrderedMeasureList { get; set; }

        //public string DefaultOptions { get; set; }

        public string TreatmentGroups { get; set; }

        public string ProgramGroupNumber { get; set; }

        public string ProgramEnrollmentStatus { get; set; }

        public string ProgramEnrollmentStatusEnrolled { get; set; }

        public string ProgramChannel { get; set; }

        public bool IsTrialRun { get; set; }

        public List<ReportMeasureMap> ReportMeasureMaps { get; set; }
    }
}
