﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using CE.WeatherSensitivity.Common.Classes;
using CE.WeatherSensitivity.Common.Models;
using CE.WeatherSensitivity.Measures.Models;
using InsightsDW.DataModel;
using InsightsDW.DataModel.DataAccess;
using log4net;

namespace CE.WeatherSensitivity.Measures.Classes
{
    public class RunMeasures
    {
        private static readonly ILog Logger = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);
        private static InsightsDataWarehouse _warehouse;
        private static readonly DateTime RunDateTime = DateTime.Now;

        /// <summary>
        /// Run the measures for the report.
        /// </summary>
        /// <param name="benchmarkResults">The overall benchmark results</param>
        /// <param name="processingOptions">The processing options.</param>
        public List<WS_Measures_StagingAllColumns> RunTheMeasures(IEnumerable<WSBenchmarkResult> benchmarkResults, ProcessingOptions processingOptions)
        {
            var wholeProgStopwatch = new Stopwatch();
            wholeProgStopwatch.Start();
            Logger.InfoFormat("Starting Measures run for ClientId {0}, ReportAlias {1}, ReportRequestDate {2}",
                       processingOptions.ClientId, processingOptions.ReportAlias, processingOptions.ReportRequestDate);

            // Get the warehouse entities.
            _warehouse = new InsightsDataWarehouse(processingOptions.ShardName, ConfigurationHelper.GetCommandTimeout());

            // Get the needed Report Settings
            var measureSettings = GetMeasureSettings(processingOptions);

            // Get the Measure Profile List.
            GetMeasureProfileLists(measureSettings, processingOptions);

            // Load the All Staging Columns table
            LoadAllStagingColumns(measureSettings);

            // Run the Measures.
            RunTheMeasures(measureSettings);

            // Return the measures
            var measureResults = GetMeasureResults();


            // Create  a snapshot of the measures results
            CreateSnapshot(measureSettings, processingOptions);


            // Remove all records with no Customer data
            measureResults = measureResults.Where(n => !string.IsNullOrEmpty(n.Email) &&
                                                       !string.IsNullOrEmpty(n.Mailing_Name)).ToList();


            // Log end of program run.
            wholeProgStopwatch.Stop();
            Logger.InfoFormat(
                "Finished Measures run in {0} for ClientId {1}, ReportAlias {2}, ReportRequestDate {3} - RunDateTime = {4}",
                    wholeProgStopwatch.Elapsed, processingOptions.ClientId, processingOptions.ReportAlias, processingOptions.ReportRequestDate, RunDateTime);

            return measureResults;
        }

        /// <summary>
        /// Create and load the Measure Settings.
        /// </summary>
        /// <param name="processingOptions">The processing options.</param>
        /// <returns>The Measure Settings.</returns>
        private MeasureSettings GetMeasureSettings(ProcessingOptions processingOptions)
        {
            // Get the report profile information.
            var reportProfileSettings = _warehouse.GetMiscReportProfilesInfo(processingOptions.ProfileName,
                processingOptions.ClientId);

            var newGuid = Guid.NewGuid();

            // Create the measure settings 
            var measureSettings = new MeasureSettings
            {
                RunId = newGuid,
                ReportProfileName = processingOptions.ProfileName,

                ReportYear = processingOptions.ReportSettings.First(
                        m => m.Name.Equals("report_year", StringComparison.InvariantCultureIgnoreCase)).Value,

                ReportMonth = processingOptions.ReportSettings.First(
                        m => m.Name.Equals("report_month", StringComparison.InvariantCultureIgnoreCase)).Value,

                ReportNumber = processingOptions.ReportSettings.First(
                        m => m.Name.Equals("report_number", StringComparison.InvariantCultureIgnoreCase)).Value,

                ReportChannel = processingOptions.ReportSettings.First(
                        m => m.Name.Equals("measure_reportchannel", StringComparison.InvariantCultureIgnoreCase)).Value,

                ClientId = processingOptions.ClientId,
                EnergyObjectCategoryList = reportProfileSettings.EnergyObjectCategoryList,
                FuelType = reportProfileSettings.FuelType,
                Period = reportProfileSettings.Period,
                SeasonList = reportProfileSettings.SeasonList,

                MeasureCount = int.Parse(processingOptions.ReportSettings.First(
                    m => m.Name.Equals("measure_count", StringComparison.InvariantCultureIgnoreCase)).Value),

                OrderedMeasureList = processingOptions.ReportSettings.First(
                    m => m.Name.Equals("measure_order_list", StringComparison.InvariantCultureIgnoreCase)).Value,

                TreatmentGroups = processingOptions.ReportSettings.First(
                    m => m.Name.Equals("measure_segments", StringComparison.InvariantCultureIgnoreCase)).Value,

                ProgramGroupNumber = processingOptions.ReportSettings.First(
                    m => m.Name.Equals("measure_programgroupnumber", StringComparison.InvariantCultureIgnoreCase)).Value,

                ProgramEnrollmentStatus = processingOptions.ReportSettings.First(
                    m => m.Name.Equals("measure_programenrollmentstatus", StringComparison.InvariantCultureIgnoreCase)).Value,

                ProgramEnrollmentStatusEnrolled = processingOptions.ReportSettings.First(
                    m => m.Name.Equals("measure_programenrollmentstatusenrolled", StringComparison.InvariantCultureIgnoreCase)).Value,

                ProgramChannel = processingOptions.ReportSettings.First(
                    m => m.Name.Equals("measure_programchannel", StringComparison.InvariantCultureIgnoreCase)).Value,

                IsTrialRun = (processingOptions.IsTrialRun == "1" || processingOptions.IsTrialRun == "true")

            };

            // return the measure settings.
            return measureSettings;

        }

        /// <summary>
        ///  Get the report measure maps
        /// </summary>
        /// <param name="measureSettings">The measure settings.</param>
        /// <param name="processingOptions">The processing options.</param>
        private void GetMeasureProfileLists(MeasureSettings measureSettings, ProcessingOptions processingOptions)
        {
            // Initialize the list.
            var reportMeasureMaps = new List<ReportMeasureMap>();

            // Get the report measure maps.
            const string settingName = "measure_profile_list_";
            for (var measureNumber = 1; measureNumber <= measureSettings.MeasureCount; measureNumber++)
            {
                var theMeasureProfileList =
                    processingOptions.ReportSettings.First(
                            m => m.Name.Equals(settingName + measureNumber, StringComparison.InvariantCultureIgnoreCase))
                        .Value;


                reportMeasureMaps.AddRange(
                    theMeasureProfileList.Split(',').Select(measureProfile => new ReportMeasureMap
                    {
                        MeasureNumber = measureNumber,
                        MeasureProfileName = measureProfile
                    }));
            }

            // Set the measure settings variable.
            measureSettings.ReportMeasureMaps = reportMeasureMaps;
        }


        /// <summary>
        /// Do the initial load of the Measures Staging table.
        /// </summary>
        /// <param name="measureSettings">The measure settings.</param>
        private void LoadAllStagingColumns(MeasureSettings measureSettings)
        {
            _warehouse.LoadAllStagingColumns(measureSettings.ReportProfileName,
                measureSettings.ReportYear,
                measureSettings.ReportMonth,
                measureSettings.ReportNumber,
                measureSettings.TreatmentGroups,
                measureSettings.ProgramGroupNumber,
                measureSettings.ProgramEnrollmentStatus,
                measureSettings.ProgramChannel,
                measureSettings.ProgramEnrollmentStatusEnrolled,
                measureSettings.ReportChannel,
                measureSettings.RunId);
        }

        /// <summary>
        /// Run the measures using Stored procedures in the InsightsDW Shard
        /// </summary>
        /// <param name="measureSettings">The measure settings.</param>
        private void RunTheMeasures(MeasureSettings measureSettings)
        {
            _warehouse.RunTheMeasures(measureSettings.ReportProfileName,
                measureSettings.ReportYear,
                measureSettings.ReportMonth,
                measureSettings.RunId,
                measureSettings.IsTrialRun);

        }

        /// <summary>
        /// Get the current contents of the Staging table.
        /// </summary>
        /// <returns>A list of measure results</returns>
        private List<WS_Measures_StagingAllColumns> GetMeasureResults()
        {
            return _warehouse.GetMeasureResults();
        }


        /// <summary>
        /// Create a Daily and/or a Releasde candidate snapshot.
        /// </summary>
        /// <param name="measureSettings">The measure settings.</param>
        /// <param name="processingOptions">The processing options.</param>
        private void CreateSnapshot(MeasureSettings measureSettings, ProcessingOptions processingOptions)
        {
            _warehouse.CreateMeasuresSnapshot(processingOptions.ReportAlias, measureSettings.ReportProfileName,
                measureSettings.ReportYear, measureSettings.ReportMonth,  int.Parse(measureSettings.ReportNumber),
                measureSettings.RunId, measureSettings.IsTrialRun);

        }


    }


}
