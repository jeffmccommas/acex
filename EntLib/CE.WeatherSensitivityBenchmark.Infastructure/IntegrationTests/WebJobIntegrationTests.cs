﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CE.WeatherSensitivity.Common.Classes;
using CE.WeatherSensitivity.Common.Models;
using log4net;
using Moq;
using NUnit.Framework;
using ReportOutputLib;
using WSWorkflowDataAccess;

namespace IntegrationTests
{
    [TestFixture]
    public class WebJobIntegrationTests
    {
        private string _shardName;
        private string _clientId;
        private string _accountid;
        private string _premiseid;
        [OneTimeSetUp]
        public void TestSetup()
        {
            _shardName = "InsightsDW_224";
            _clientId = "224";

            _accountid = "01000004400551";//valid for dev
            _premiseid = "2130088054031";//valid for dev

        }
        [Test]
        // [Ignore("Ignore Integration Tests")]
        public void GetZipCodes_test()
        {
            var expectedResult = RunProcessCheck.CheckPostalCodes(_shardName, Convert.ToInt32(_clientId));
            Console.WriteLine(expectedResult.Any() ? "Found (" + expectedResult.Count + ") stations" : "'0' zip codes found");
            int count = 1;
            foreach (var station in expectedResult)
            {
                Console.WriteLine(expectedResult.Any() ? "Found (" + station.PostalCodes.Length + ") zip codes in station#"+count : "'0' zip codes found");
                count++;
            }
            Assert.That(expectedResult.Any,Is.True);

        }
        [Test]
        public void GetHESParticipant_test()
        {
            Mock<ILog> logMockObject = new Mock<ILog>();
            ProcessingOptions options = new ProcessingOptions();
            options.ShardName = _shardName;
            options.ClientId = Convert.ToInt32(_clientId);

            WeatherSensitivityDataAccess dataAccess = new WeatherSensitivityDataAccess(options, logMockObject.Object);
            var expectedResult = dataAccess.GetIsHesParticipant(_premiseid,_accountid);
            Console.WriteLine("Expected Result="+ expectedResult);
            Assert.That(expectedResult,Is.EqualTo(1));

        }
    }
}
