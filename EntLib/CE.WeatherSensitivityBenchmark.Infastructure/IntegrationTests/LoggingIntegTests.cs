﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using CE.WeatherSensitivity.Common.Logging;
using log4net;
using log4net.Config;
using Moq;
using NUnit.Framework;

namespace IntegrationTests
{
    public class LogRecord
    {
        public int Id { get; set; }
        public DateTime LogDateTime { get; set; }
        public string ThreadName { get; set; }
        public string Level { get; set; }
        public string Logger { get; set; }
        public string Message { get; set; }
        public string Exception { get; set; }
    }
    [TestFixture]
    [Ignore("Ignore Integration Tests")]
    public class LoggingIntegTests
    {
        protected ILog _sut; //LogManager.GetLogger(typeof(LoggingIntegTests));
        private string _connectionString = "";

        [OneTimeSetUp]
        public void TestSetup()
        {
            InitalizeLog4Net.Initalize();
            _sut = InitalizeLog4Net.GetTheLogger(typeof(LoggingIntegTests));
            //log4net.Config.XmlConfigurator.Configure();

            //BasicConfigurator.Configure();
            //_sut = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);
            _connectionString = "data source = acedsql1c0.database.windows.net; initial catalog = InsightsDW_Test; integrated security = false; persist security info = True; User ID = Aclweb@acedsql1c0; Password = Acl@r@393";
        }

        [Test]
        public void LogInfo_test()
        {
            _sut.Info("LogInfo Test");
            Assert.That(GetLatestLog().Message, Is.EqualTo("LogInfo Test"));
            Assert.That(GetLatestLog().LogDateTime.ToString("f"), Is.EqualTo(DateTime.Now.ToString("f")));
        }

        [Test]
       // [Ignore("Ignore Integration Tests")]
        public void LogDebug_test()
        {
            _sut.Debug("LogDebug Test");
            Assert.That(GetLatestLog().Message, Is.EqualTo("LogDebug Test"));
            Assert.That(GetLatestLog().LogDateTime.ToString("f"), Is.EqualTo(DateTime.Now.ToString("f")));

        }

        [Test]
        // [Ignore("Ignore Integration Tests")]
        public void LogWarning_test()
        {
            _sut.Warn("LogWarning Test");
            Assert.That(GetLatestLog().Message, Is.EqualTo("LogWarning Test"));
            Assert.That(GetLatestLog().LogDateTime.ToString("f"), Is.EqualTo(DateTime.Now.ToString("f")));

        }

        [Test]
        // [Ignore("Ignore Integration Tests")]
        public void LogError_test()
        {
            _sut.Error("LogError Test");
            Assert.That(GetLatestLog().Message, Is.EqualTo("LogError Test"));
            Assert.That(GetLatestLog().LogDateTime.ToString("f"), Is.EqualTo(DateTime.Now.ToString("f")));

        }

        private LogRecord GetLatestLog()
        {
            var logRecord = new LogRecord();
            using (SqlConnection connection =
               new SqlConnection(_connectionString))
            {
                // Open the connection in a try/catch block. 
                // Create and execute the DataReader, writing the result
                try
                {
                    connection.Open();

                    string queryString =
                        "SELECT * FROM  audit.WSLog WHERE ID = (SELECT MAX(ID)  FROM audit.WSLog)";

                    // create connection and command

                    using (SqlCommand cmd = new SqlCommand(queryString, connection))
                    {
                        SqlDataReader reader = cmd.ExecuteReader();

                        // Call Read before accessing data.
                        while (reader.Read())
                        {
                            logRecord.Id = (int) reader[0];
                            logRecord.LogDateTime = (DateTime)reader[1];
                            logRecord.ThreadName = (string)reader[2];
                            logRecord.Level = (string)reader[3];
                            logRecord.Logger = (string)reader[4];
                            logRecord.Message = (string)reader[5];
                            logRecord.Exception = (string)reader[6];

                        }

                        // Call Close when done reading.
                        reader.Close();
                    }

                    connection.Close();
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    return null;
                }

                return logRecord;
            }
        }
    }
}
