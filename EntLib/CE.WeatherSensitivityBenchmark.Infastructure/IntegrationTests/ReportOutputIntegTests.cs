﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using CE.WeatherSensitivity.Common.Classes;
using CE.WeatherSensitivity.Common.Models;
using log4net;
using Microsoft.VisualBasic.FileIO;
using Moq;
using NUnit.Framework;
using ReportOutputLib;
using WeatherForecastService;
using WeatherForecastService.models;
using WSWorkflowDataAccess.models;

namespace IntegrationTests
{
    [TestFixture]
    //[Ignore("Ignore Integration Tests")]
    public class ReportOutputIntegTests
    {
        ForecastReportSettings _mockConfig;//
        IReportOutput _sut;
        Notification _sutNotification;
        IFileService _fileService = new FileService();
        [OneTimeSetUp]
        public void TestSetup()
        {

            _mockConfig = new ForecastReportSettings
            {
                ExtremeColdTempThreshold = "20",
                ExtremeHotTempThreshold = "40",
                ForecastTotalDays = "7",
                ForecastStartPeriod = "2",//2 days from now
                ForecastServiceKey = "58f2ab2e4de021586a561e5cfc558262",
                ForecastServiceUrl = "https://api.darksky.net/forecast/",
                ZipCodeList = "06231,06232",
                TemperatureType = "hot",
                ToEmailList = "muazzam.ali@gmail.com",
                EmailSubject = "Weather Sensitivity Program - Daily Forecast",
                ForecastEndPeriod = "5",//5 days from now
                ForecastEmailFrom = "ACE_Dev@aclarax.com",
                ForecastEmailFromName = Environment.UserName,
                ForecastEmailSmtpAuth = "Basic",
                ForecastEmailSmtpPort = "587",
                ForecastEmailSmtpPassword = "Xaclaradev2311X",
                ForecastEmailSmtpServer = "smtp.sendgrid.net",
                ForecastEmailSmtpSsl = "true",
                ForecastEmailSmtpUser = "aclaradev"


            };
            

           // _mockConfig.Add("emailToName", "Muazzam");

            Directory.CreateDirectory("data");
            File.WriteAllText("data/ZipCodeList.txt", "ZIP,NAME,LAT,LNG\n06231, AMSTON, 41.62, -72.37\n06232, ANDOVER, 41.73, -72.36\n06401, ANSONIA, 41.34, -73.06\n06278, ASHFORD, 41.86, -72.11");

            Mock<ILog> logMockObject = new Mock<ILog>();
            _sutNotification = new Notification(_mockConfig, logMockObject.Object);

            _sut = new ReportOutput(_fileService);
        }
        [TearDown]
        public void Cleanup()
        {
           /* if(File.Exists("data/ZipCodeList.txt"))
                File.Delete("data/ZipCodeList.txt");
            if (File.Exists("data/ZipCodeDatabase.sdf"))
                File.Delete("data/ZipCodeDatabase.sdf");
            //if (File.Exists("sampleEmail.csv"))
            //    File.Delete("sampleEmail.csv");

            //if (File.Exists("samplePrint.csv"))
            //    File.Delete("samplePrint.csv");
            if (Directory.Exists("data"))
                Directory.Delete("data");*/
        }
        [Test]
        [Ignore("Ignore Integration Tests")]
        public void WriteEmailReportToCSV_test()
        {
            var data = new List<WsEmailReportModel>();
        }

        [Test]
        public void GenerateSummaryEmail_test_MOCK()
        {
            var data = new WsForecastModel();

            data.ThresholdValue = "50";
            data.TodaysDate = DateTime.Now;
            data.WeatherAttribute = "HotTemp";
            data.ForecastByZone = new List<WsZoneForecastModel>();

            data.ForecastByZone.Add(new WsZoneForecastModel
            {
                Name = "Wellesley, MA",
                ZipCode = "01177",
                Forecast = new List<string>
                {
                   "Number of days with temperature above threshold [ 1 day(s) ]",
                   "[Date : "+DateTime.Now.ToString("d")+"]",
                   "Summary : Mostly cloudy throughout the day.",
                   "Temperature : 49.35",
                   "Date : "+DateTime.Now.AddDays(1).ToString("d"),
                   "Summary : Light rain until afternoon, starting again overnight.",
                   "Temperature : 55.00 <---"

                }

            });

            data.ForecastByZone.Add(new WsZoneForecastModel
            {
                Name = "Natick, MA",
                ZipCode = "01747",
                Forecast = new List<string>
                {
                   "Number of days with temperature above threshold [ 1 day(s) ]",
                   "[Date : "+DateTime.Now.ToString("d")+"]",
                   "Summary : Partly cloudy starting in the evening.",
                   "Temperature : 30.35",
                   "Date : "+DateTime.Now.AddDays(1).ToString("d"),
                   "Summary : Mostly cloudy throughout the day.",
                   "Temperature : 60.00 <---"
                }

            });

            var result = _sut.GenerateSummaryEmail(data);
            Assert.That(result.Contains("Date : " + DateTime.Now.ToString("d")), Is.True);
            Assert.That(result.Contains("Date : " + DateTime.Now.AddDays(1).ToString("d")), Is.True);
            Console.WriteLine(result);

        }

        [Test]
        //[Ignore("")]
        public void GenerateSummaryEmail_test()
        {

            Mock<ILog> logMockObject = new Mock<ILog>();

            ForecastService forecastService = new ForecastService(_mockConfig, logMockObject.Object);
            var zipcodelist = new List<string>();
            zipcodelist.AddRange(_mockConfig.ZipCodeList.Split(','));
            var weatherData = forecastService.GetForecast(zipcodelist);//new List <string> { "06231", "06232" });

            var result = _sut.GenerateSummaryEmail(ForecastMapper.MapToEmailSummary(weatherData, WeatherSensitivityAttribute.HOT_WEATHER, forecastService));
            Assert.That(result.Contains("[Date : " + DateTime.Now.AddDays(1).ToString("d")), Is.True);
            Assert.That(result.Contains("[Date : " + DateTime.Now.AddDays(2).ToString("d")), Is.True);
            Assert.That(result.Contains("[Date : " + DateTime.Now.AddDays(3).ToString("d")), Is.True);
            Assert.That(result.Contains("[Date : " + DateTime.Now.AddDays(4).ToString("d")), Is.True);

            Console.WriteLine(result);

        }

        [Test]
        [Ignore("")]
        public void SendSummaryEmail_test()
        {
           
            Mock<ILog> logMockObject = new Mock<ILog>();

            ForecastService forecastService = new ForecastService(_mockConfig, logMockObject.Object);
            var zipcodelist = new List<string>();
            zipcodelist.AddRange(_mockConfig.ZipCodeList.Split(','));

            var weatherData = forecastService.GetForecast(zipcodelist);//new List<string> { "06231", "06232" });

            var result = _sut.GenerateSummaryEmail(ForecastMapper.MapToEmailSummary(weatherData, WeatherSensitivityAttribute.HOT_WEATHER, forecastService));
            Assert.That(result.Contains("[Date : " + DateTime.Now.AddDays(1).ToString("d")), Is.True);
            Assert.That(result.Contains("[Date : " + DateTime.Now.AddDays(2).ToString("d")), Is.True);
            Assert.That(result.Contains("[Date : " + DateTime.Now.AddDays(3).ToString("d")), Is.True);

            var emailSent = _sutNotification.SendEmail(result);
            Assert.That(emailSent, Is.True);

            Console.WriteLine(result);

        }

        [Test]
//        [Ignore("")]
        public void GenerateEmailReportCsv_test()
        {
           

            Mock<ILog> logMockObject = new Mock<ILog>();

            ForecastService forecastService = new ForecastService(_mockConfig, logMockObject.Object);
            var zipcodelist = new List<string>();
            zipcodelist.AddRange(_mockConfig.ZipCodeList.Split(','));
            var weatherData = forecastService.GetForecast(zipcodelist);

            _sut.EmailReportOutputFile = @"c:\temp\sampleEmail.csv";
            _sut.WriteEmailReportToCSV(PopulateEmailReportModel(), null);
            
            Assert.That(File.Exists(@"c:\temp\sampleEmail.csv"), Is.True);
            Console.WriteLine(Directory.GetCurrentDirectory());


        }

        [Test]
        public void GeneratePrintReportCsv_test()
        {
            _sut.PrintReportOutputFile = @"c:\temp\samplePrint.csv";
            _sut.WritePrintReportToCSV(PopulatePrintReportModel());
            Assert.That(File.Exists(@"c:\temp\samplePrint.csv"), Is.True);
        }

        private List<WsEmailReportModel> PopulateEmailReportModel()
        {
            var result = new List<WsEmailReportModel>();

            using (TextFieldParser csvParser = new TextFieldParser(@"c:\temp\sampleEmailReportData.csv"))
            {
                csvParser.CommentTokens = new string[] { "#" };
                csvParser.SetDelimiters(new string[] { "," });
                // Skip the row with the column names
                csvParser.ReadLine();

                while (!csvParser.EndOfData)
                {
                    // Read current line fields, pointer moves to the next line.
                    string[] fields = csvParser.ReadFields();
                    var wsEmailReportModel = new WsEmailReportModel
                    {
                        SubscriberKey = fields[0],
                        AccountNumber = fields[1],
                        CustomerId = fields[2],
                        PremiseId = fields[3],

                        ReferrerId = fields[4],
                        Email = fields[5],
                        ProgramReportNumber = fields[6],
                        ReportMonth = fields[7],
                        ReportYear = fields[8],
                        ReportPeriodStart = Convert.ToDateTime(DateTime.ParseExact(fields[9], "yyyyMMdd", null)),
                        ReportPeriodEnd = Convert.ToDateTime(DateTime.ParseExact(fields[10], "yyyyMMdd", null)),
                        SegmentId = fields[11],
                        HesParticipant = fields[12],
                        MaskedAccountNumber = ReportHelper.MaskTextData(fields[1], 4),

                        MyHomeUse = fields[14],
                        AverageHomeUse = fields[15],
                        EfficientHomeUse = fields[16],
                        PcntPeerBenchmark = fields[17],//will be calculated
                        PcntPeerBenchmarkText = fields[18],//will be calculated

                        MeasureName1 = fields[19],
                        MeasureSavings1 = fields[20],
                        MeasureName2 = fields[21],
                        MeasureSavings2 = fields[22],
                        MeasureName3 = fields[23],
                        MeasureSavings3 = fields[24]
                    };

                    result.Add(wsEmailReportModel);
                }
            }

            return result;
            
        }


        private List<WsPrintReportModel> PopulatePrintReportModel()
        {
            var result = new List<WsPrintReportModel>();

            using (TextFieldParser csvParser = new TextFieldParser(@"c:\temp\samplePrintReportData.csv"))
            {
                csvParser.CommentTokens = new string[] { "#" };
                csvParser.SetDelimiters(new string[] { "," });
                // Skip the row with the column names
                csvParser.ReadLine();

                while (!csvParser.EndOfData)
                {
                    // Read current line fields, pointer moves to the next line.
                    string[] fields = csvParser.ReadFields();
                    var wsPrintReportModel = new WsPrintReportModel
                    {
                        AccountNumber = fields[0],
                        CustomerId = fields[1],
                        PremiseId = fields[2],
                        ReferrerId = fields[3],
                        CustomerName = fields[4],
                        MailingAddress = fields[5],
                        MailingAddress2 = fields[6],
                        MailingCity = fields[7],
                        MailingState = fields[8],
                        MailingZip = fields[9],

                        ProgramReportNumber = fields[10],
                        SegmentId = fields[11],
                        HesParticipant = fields[12],
                        ReportMonth = fields[13],
                        ReportYear = fields[14],
                        ReportPeriodStart = Convert.ToDateTime(DateTime.ParseExact(fields[15], "yyyyMMdd", null)),
                        ReportPeriodEnd = Convert.ToDateTime(DateTime.ParseExact(fields[16], "yyyyMMdd", null)),
                        MaskedAccountNumber = ReportHelper.MaskTextData(fields[0], 4),
                        MyHomeUse = fields[18],
                        AverageHomeUse = fields[19],
                        EfficientHomeUse = fields[20],
                        PcntPeerBenchmark = fields[21],
                        PcntPeerBenchmarkText = fields[22],


                        MeasureName1 = fields[23],
                        MeasureSavings1 = fields[24],
                        MeasureName2 = fields[25],
                        MeasureSavings2 = fields[26],
                        MeasureName3 = fields[27],
                        MeasureSavings3 = fields[28],
                        MyNormalDayUse = fields[29],
                        MyExtremeDayUse = fields[30],
                        PcntSelfBenchmark = fields[31],
                        PcntSelfBenchmarkText = fields[32],
                    };

                    result.Add(wsPrintReportModel);
                }
            }

            /* result.Add(new WsPrintReportModel
             {
                 AccountNumber = "1088811977070",
                 AverageHomeUse = "183",
                 CustomerId = "108881977070",
                 EfficientHomeUse = "120",
                 HesParticipant = "0",
                 MaskedAccountNumber = ReportHelper.MaskTextData("1088811977070", 4),
                 MeasureName1 = "installprogramthermostat",
                 MeasureName2 = "caulkandweatherize",
                 MeasureName3 = "UIInsulation",
                 MeasureSavings1 = "300",
                 MeasureSavings2 = "800",
                 MeasureSavings3 = "0",
                 MyHomeUse = "162",
                 PcntPeerBenchmark = "40 %",
                 PcntPeerBenchmarkText = "40 Percent",
                 PremiseId = "3007890000021",
                 ProgramReportNumber = "2",
                 ReferrerId = "224",
                 ReportMonth = "05",
                 ReportPeriodEnd = DateTime.Now.AddDays(30).ToString("yyyyMMdd"),
                 ReportPeriodStart = DateTime.Now.ToString("yyyyMMdd"),
                 ReportYear = "2017",
                 SegmentId = "1088811977070",
                 CustomerName = "ANTHONY P RICCIO JR",
                 MailingAddress = "22 ENRICA RITA WAY",
                 MailingAddress2 = "",
                 MailingCity = "STRATFORD",
                 MailingState = "CT",
                 MailingZip = "06614",
                 MyExtremeDayUse = "32",
                 MyNormalDayUse = "12",
                 PcntSelfBenchmark = "2.6",
                 PcntSelfBenchmarkText = "2.6X"

             });

             result.Add(new WsPrintReportModel
             {
                 AccountNumber = "1088811977071",
                 AverageHomeUse = "183",
                 CustomerId = "108881977071",
                 EfficientHomeUse = "120",
                 HesParticipant = "0",
                 MaskedAccountNumber = ReportHelper.MaskTextData("1088811977072", 4),
                 MeasureName1 = "installprogramthermostat",
                 MeasureName2 = "caulkandweatherize",
                 MeasureName3 = "UIInsulation",
                 MeasureSavings1 = "300",
                 MeasureSavings2 = "800",
                 MeasureSavings3 = "0",
                 MyHomeUse = "162",
                 PcntPeerBenchmark = "40 %",
                 PcntPeerBenchmarkText = "40 Percent",
                 PremiseId = "4007890000021",
                 ProgramReportNumber = "2",
                 ReferrerId = "224",
                 ReportMonth = "05",
                 ReportPeriodEnd = DateTime.Now.AddDays(30).ToString("yyyyMMdd"),
                 ReportPeriodStart = DateTime.Now.ToString("yyyyMMdd"),
                 ReportYear = "2017",
                 SegmentId = "1088811977071",
                 CustomerName = "MARK JAMES",
                 MailingAddress = "23 SAMPLE WAY",
                 MailingAddress2 = "",
                 MailingCity = "STRATFORD",
                 MailingState = "CT",
                 MailingZip = "06414",
                 MyExtremeDayUse = "32",
                 MyNormalDayUse = "12",
                 PcntSelfBenchmark = "2.6",
                 PcntSelfBenchmarkText = "2.6X"

             });

             result.Add(new WsPrintReportModel
             {
                 AccountNumber = "1088811977072",
                 AverageHomeUse = "183",
                 CustomerId = "108881977072",
                 EfficientHomeUse = "120",
                 HesParticipant = "0",
                 MaskedAccountNumber = ReportHelper.MaskTextData("1088811977072", 4),
                 MeasureName1 = "installprogramthermostat",
                 MeasureName2 = "caulkandweatherize",
                 MeasureName3 = "UIInsulation",
                 MeasureSavings1 = "300",
                 MeasureSavings2 = "800",
                 MeasureSavings3 = "0",
                 MyHomeUse = "162",
                 PcntPeerBenchmark = "40 %",
                 PcntPeerBenchmarkText = "40 Percent",
                 PremiseId = "5007890000021",
                 ProgramReportNumber = "2",
                 ReferrerId = "224",
                 ReportMonth = "05",
                 ReportPeriodEnd = DateTime.Now.AddDays(30).ToString("yyyyMMdd"),
                 ReportPeriodStart = DateTime.Now.ToString("yyyyMMdd"),
                 ReportYear = "2017",
                 SegmentId = "1088811977072",
                 CustomerName = "JOHN Q PUBLIC",
                 MailingAddress = "1 AMERICA ST",
                 MailingAddress2 = "",
                 MailingCity = "STRATFORD",
                 MailingState = "CT",
                 MailingZip = "06514",
                 MyExtremeDayUse = "32",
                 MyNormalDayUse = "12",
                 PcntSelfBenchmark = "2.6",
                 PcntSelfBenchmarkText = "2.6X"

             });*/

            return result;

        }
    }
}
