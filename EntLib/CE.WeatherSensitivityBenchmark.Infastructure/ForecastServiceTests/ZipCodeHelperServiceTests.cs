﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Data.Entity;
using System.IO;
using System.Linq;
using log4net;
using Moq;
using NUnit.Framework;
using WeatherForecastService;
using WeatherForecastService.models;

namespace ForecastServiceTests
{
    [TestFixture]
    public class ZipCodeHelperServiceTests
    {
        private Mock<ILog> _mockLogger;

        ZipCodeHelper sut;
        [OneTimeSetUp]
        public void TestSetup()
        {
            _mockLogger = new Mock<ILog>();

            /* var mockSet = new Mock<DbSet<ZipCodeModel>>();

             var data = new List<ZipCodeModel>
             {
                 new ZipCodeModel { id = 1,name = "HARTFORD",latitude = "73",longitude ="42",zip = "01464"},
                 new ZipCodeModel { id = 2,name = "WEATHERSFIELD",latitude = "74",longitude ="43",zip = "01465"},
                 new ZipCodeModel { id = 3,name = "CITY",latitude = "75",longitude ="44",zip = "01466"},
             }.AsQueryable();

             mockSet.As<IQueryable<ZipCodeModel>>().Setup(m => m.Provider).Returns(data.Provider);
             mockSet.As<IQueryable<ZipCodeModel>>().Setup(m => m.Expression).Returns(data.Expression);
             mockSet.As<IQueryable<ZipCodeModel>>().Setup(m => m.ElementType).Returns(data.ElementType);
             mockSet.As<IQueryable<ZipCodeModel>>().Setup(m => m.GetEnumerator()).Returns(data.GetEnumerator());*/
            Directory.CreateDirectory("data");
            File.WriteAllText("data/ZipCodeList.txt", "ZIP,NAME,LAT,LNG\n06231, AMSTON, 41.62, -72.37\n06232, ANDOVER, 41.73, -72.36\n06401, ANSONIA, 41.34, -73.06\n06278, ASHFORD, 41.86, -72.11");


            sut = new ZipCodeHelper(_mockLogger.Object);

        }
        [Test]
        public void GetZipCodeCount_test()
        {
            int expectedResult = sut.GetZipCodeCount();
            Assert.That(expectedResult, Is.EqualTo(5));
            //logMockObject.Verify(moqLog => moqLog.Info("do stuff got called"), Times.AtLeastOnce());
        }
        [Test]
        public void GetZipCodeCount_exception_test()
        {
            if (File.Exists("data/ZipCodeList.txt"))
                File.Delete("data/ZipCodeList.txt");

            
            int expectedResult = sut.GetZipCodeCount();

            Assert.That(expectedResult, Is.EqualTo(-1));
            _mockLogger.Verify(moqLog => moqLog.Fatal(It.IsRegex("Zip code file not found")), Times.AtLeastOnce());//check if log was called

            File.WriteAllText("data/ZipCodeList.txt", "ZIP,NAME,LAT,LNG\n06231, AMSTON, 41.62, -72.37\n06232, ANDOVER, 41.73, -72.36\n06401, ANSONIA, 41.34, -73.06\n06278, ASHFORD, 41.86, -72.11");

        }

        [Test]
        public void GetZipCodeCoordinates_test()
        {
            var expectedResult = sut.GetZipCodeCoordinates("06231");
            Assert.That(expectedResult.zip, Is.EqualTo("06231"));
            
        }

        [Test]
        public void GetZipCodeCoordinates_null_test()
        {
           var expectedResult = sut.GetZipCodeCoordinates("0000");
            Assert.That(expectedResult, Is.Null);
            _mockLogger.Verify(moqLog => moqLog.Warn(It.IsRegex("zip code not found.")), Times.AtLeastOnce);//check if log was *NOT* called
        }

        [Test]
        public void GetZipCodeCoordinates_unexpected_exception_test()
        {

            if (File.Exists("data/ZipCodeList.txt"))
                File.Delete("data/ZipCodeList.txt");

            //generate a bad file
            File.WriteAllText("data/ZipCodeList.txt", "ZIP-NAME-LAT-LNG\n06231-AMSTON- 41.62-72.37\n06232, ANDOVER, 41.73, -\n06401, ANSONIA, 41.34, -73.06\n06278, ASHFORD, 41.86, -72.11");//invalid string in zip file

            var expectedResult = sut.GetZipCodeCoordinates("0000");

            Assert.That(expectedResult, Is.Null);

            _mockLogger.Verify(moqLog => moqLog.Fatal(It.IsAny<string>(), It.IsAny<Exception>()), Times.AtLeastOnce());//check if log was called

            if (File.Exists("data/ZipCodeList.txt"))
                File.Delete("data/ZipCodeList.txt");

            sut.GetZipCodeCoordinates("0000");
            _mockLogger.Verify(moqLog => moqLog.Fatal(It.IsRegex("Zip code file not found")), Times.AtLeastOnce());//check if log was called
            //make sure file is available for other tests
            File.WriteAllText("data/ZipCodeList.txt", "ZIP,NAME,LAT,LNG\n06231, AMSTON, 41.62, -72.37\n06232, ANDOVER, 41.73, -72.36\n06401, ANSONIA, 41.34, -73.06\n06278, ASHFORD, 41.86, -72.11");
        }
        [Test]
        public void GetZipCode_null_test()
        {

            if (File.Exists("data/ZipCodeList.txt"))
                File.Delete("data/ZipCodeList.txt");

            File.WriteAllText("data/ZipCodeList.txt", "ZIP,NAME,LAT,LNG\n06231, AMSTON, 41.62, -72.37\n06232, ANDOVER, 41.73, -72.36\n06401, ANSONIA, 41.34, -73.06\n06278, ASHFORD, 41.86, -72.11");

            var expectedResult = sut.GetZipCode("41.73","-7");
            Assert.That(expectedResult, Is.Null);
            _mockLogger.Verify(moqLog => moqLog.Warn(It.IsRegex("zip code not found.")), Times.AtLeastOnce);//check if log was *NOT* called
        }
        [Test]
        public void GetZipCode_exception_test()
        {

            if (File.Exists("data/ZipCodeList.txt"))
                File.Delete("data/ZipCodeList.txt");

            //generate a bad file
            File.WriteAllText("data/ZipCodeList.txt", "ZIP-NAME-LAT-LNG\n06231-AMSTON- 41.62-72.37\n06232, ANDOVER, 41.73, -\n06401, ANSONIA, 41.34, -73.06\n06278, ASHFORD, 41.86, -72.11");//invalid string in zip file

            var expectedResult = sut.GetZipCode("41.73", "-72.36");

            Assert.That(expectedResult, Is.Null);

            _mockLogger.Verify(moqLog => moqLog.Fatal(It.IsAny<string>(), It.IsAny<Exception>()), Times.AtLeastOnce());//check if log was called

            if (File.Exists("data/ZipCodeList.txt"))
                File.Delete("data/ZipCodeList.txt");

            sut.GetZipCode("00","00");
            _mockLogger.Verify(moqLog => moqLog.Fatal(It.IsRegex("Zip code file not found")), Times.AtLeastOnce());//check if log was called
            //make sure file is available for other tests
            File.WriteAllText("data/ZipCodeList.txt", "ZIP,NAME,LAT,LNG\n06231, AMSTON, 41.62, -72.37\n06232, ANDOVER, 41.73, -72.36\n06401, ANSONIA, 41.34, -73.06\n06278, ASHFORD, 41.86, -72.11");

        }
        [TearDown]
        public void Cleanup()
        {
         /*   if (File.Exists("data/ZipCodeList.txt"))
                File.Delete("data/ZipCodeList.txt");
            if (File.Exists("data/ZipCodeDatabase.sdf"))
                File.Delete("data/ZipCodeDatabase.sdf");
            //if (File.Exists("sampleEmail.csv"))
            //    File.Delete("sampleEmail.csv");

            //if (File.Exists("samplePrint.csv"))
            //    File.Delete("samplePrint.csv");
            if (Directory.Exists("data"))
                Directory.Delete("data");*/
        }

    }
}
