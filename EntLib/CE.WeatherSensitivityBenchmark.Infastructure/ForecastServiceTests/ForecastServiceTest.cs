﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.Text;
using CE.WeatherSensitivity.Common.Models;
using log4net;
using Moq;
using NUnit.Framework;
using WeatherForecastService;
using WeatherForecastService.models;
using WSEmailReportWorkflowWebJob;

namespace ForecastServiceTests
{
    [TestFixture]
    public class ForecastServiceTest
    {
        Mock<ILog> _mockLogger = new Mock<ILog>();

        ForecastService sut;
        [OneTimeSetUp]
        public void TestSetup()
        {
            var mockConfig = new ForecastReportSettings
            {
                ExtremeColdTempThreshold = "20",
                ExtremeHotTempThreshold = "80",
                ForecastTotalDays = "7",
                ForecastStartPeriod = "2",//2 days from now
                ForecastServiceKey = "testkey",
                ForecastServiceUrl = "testurl",
                ZipCodeList = "01474,01464",
                TemperatureType = "hot",
                ToEmailList = "testlist",
                EmailSubject = "test",
                ForecastEndPeriod = "5"//5 days from now

            };

            Mock<ILog> logMockObject = new Mock<ILog>();

            sut = new ForecastService(mockConfig, logMockObject.Object);

        }
        [Test]
        public void GetWeatherServiceUrlKey_test()
        {
            string expectedResult = sut.GetWeatherServiceUrlKey();
            Assert.That(expectedResult, Is.EqualTo("testkey"));
            //logMockObject.Verify(moqLog => moqLog.Info("do stuff got called"), Times.AtLeastOnce());
        }
        [Test]
        public void GetWeatherServiceUrlKey_exception_test()
        {
            Mock<ILog> logMockObject = new Mock<ILog>();
            var forecastService = new ForecastService(null, logMockObject.Object);
            string expectedResult = forecastService.GetWeatherServiceUrlKey();
            Assert.That(expectedResult, Is.Null);
        }
        [Test]
        public void GetWeatherServiceUrl_test()
        {
            string expectedResult = sut.GetWeatherServiceUrl();
            Assert.That(expectedResult, Is.EqualTo("testurl"));
            
        }
        [Test]
        public void GetWeatherServiceUrl_exception_test()
        {
            Mock<ILog> logMockObject = new Mock<ILog>();
            var forecastService = new ForecastService(null, logMockObject.Object);
            string expectedResult = forecastService.GetWeatherServiceUrl();
            Assert.That(expectedResult, Is.Null);
            //check if log statement was invoked
            logMockObject.Verify(moqLog => moqLog.Fatal(It.IsAny<string>(), It.IsAny<Exception>()), Times.AtLeastOnce());
        }
        [Test]
        public void GetHotDayThreshold_test()
        {
            int expectedResult = sut.GetHotDayThreshold();
            Assert.That(expectedResult, Is.EqualTo(80));

        }
        [Test]
        public void GetHotDayThreshold_exception_test()
        {
            Mock<ILog> logMockObject = new Mock<ILog>();
            var forecastService = new ForecastService(null, logMockObject.Object);
            int expectedResult = forecastService.GetHotDayThreshold();
            Assert.That(expectedResult, Is.EqualTo(-100));
            //check if log statement was invoked
            logMockObject.Verify(moqLog => moqLog.Fatal(It.IsAny<string>(), It.IsAny<Exception>()), Times.AtLeastOnce());

        }
        [Test]
        public void GetColdDayThreshold_test()
        {
            int expectedResult = sut.GetColdDayThreshold();
            Assert.That(expectedResult, Is.EqualTo(20));

        }
        [Test]
        public void GetColdDayThreshold_exception_test()
        {
            Mock<ILog> logMockObject = new Mock<ILog>();
            var forecastService = new ForecastService(null, logMockObject.Object);
            int expectedResult = forecastService.GetHotDayThreshold();
            Assert.That(expectedResult, Is.EqualTo(-100));
            //check if log statement was invoked
            logMockObject.Verify(moqLog => moqLog.Fatal(It.IsAny<string>(), It.IsAny<Exception>()), Times.AtLeastOnce());

        }
        [Test]
        public void UnixTimeStampToDateTime_test()
        {
            var expectedResult = sut.UnixTimeStampToDateTime(1486011600);
            Assert.That(expectedResult.Day, Is.EqualTo(2));
            Assert.That(expectedResult.Month, Is.EqualTo(2));
            Assert.That(expectedResult.Year, Is.EqualTo(2017));

        }

        [Test]
        public void GetHotDayExtremeForecast_test()
        {
            var mockForecast = File.ReadAllText(AppDomain.CurrentDomain.BaseDirectory + @"\MockData\ForecastJsonMockData.txt");
            var moq = new Mock<ForecastService>();

            moq.Setup(x => x.GetWeatherData(It.IsAny<string>())).Returns(mockForecast);

            var expectedResult = moq.Object.GetHotDayExtremeForecast("", "");
            Assert.That(expectedResult.data.Count, Is.EqualTo(8));

        }

        [Test]
        public void GetColdDayExtremeForecast_by_coordinates_test()
        {
            var mockForecast = File.ReadAllText(AppDomain.CurrentDomain.BaseDirectory + @"\MockData\ForecastJsonMockData.txt");
            var moq = new Mock<ForecastService>();

            moq.Setup(x => x.GetWeatherData(It.IsAny<string>())).Returns(mockForecast);
            moq.Setup(x => x.GetColdDayThreshold()).Returns(20);

            var expectedResult = moq.Object.GetColdDayExtremeForecast("", "");
            Assert.That(expectedResult.data.Count, Is.EqualTo(1));

        }
        [Test]
        public void GetColdDayExtremeForecast_by_obj_test()
        {
            WeatherForecastService.models.Daily dailyForecast = new Daily
            {
                data = new List<Data>
                {
                    new Data
                    {
                        temperatureMin = 10,
                        temperatureMax = 10
                    }
                }

            };

            var mockForecast = File.ReadAllText(AppDomain.CurrentDomain.BaseDirectory + @"\MockData\ForecastJsonMockData.txt");
            var moq = new Mock<ForecastService>();

            moq.Setup(x => x.GetWeatherData(It.IsAny<string>())).Returns(mockForecast);
            moq.Setup(x => x.GetColdDayThreshold()).Returns(25);

            var expectedResult = moq.Object.GetColdDayExtremeForecast(dailyForecast);
            Assert.That(expectedResult.data.Count, Is.EqualTo(1));

        }

        [Test]
        public void CanRunWeatherSensitivityEmailReport_ThresholdNotMet_test()
        {
            if(!Directory.Exists("data"))
                Directory.CreateDirectory("data");
            File.WriteAllText("data/ZipCodeList.txt", "ZIP,NAME,LAT,LNG\n06231, AMSTON, 41.62, -72.37\n06232, ANDOVER, 41.73, -72.36\n06401, ANSONIA, 41.34, -73.06\n06278, ASHFORD, 41.86, -72.11");


            var forecastReportSettings = new ForecastReportSettings();
            forecastReportSettings.EmailSubject = "UNIT TEST";
            forecastReportSettings.ForecastServiceUrl = "test";
            forecastReportSettings.ForecastServiceKey = "test";
            forecastReportSettings.ExtremeHotTempThreshold = "40";
            forecastReportSettings.TemperatureType = "hot";
            forecastReportSettings.ZipCodeList = "06232";
            forecastReportSettings.ForecastStartPeriod = "2";//two days from now
            forecastReportSettings.ForecastEndPeriod = "5";//5 days from now

            //Mock forecast service and result
            var mockForecast = File.ReadAllText(AppDomain.CurrentDomain.BaseDirectory + @"\MockData\ForecastJsonMockData.txt");
            mockForecast = UpdateTimeRelativeToToday(mockForecast);
            var moq = new Mock<ForecastService>(forecastReportSettings, _mockLogger.Object);

            moq.Setup(x => x.GetHotDayThreshold()).Returns(40);
            moq.Setup(x => x.GetWeatherData(It.IsAny<string>())).Returns(mockForecast);

            Program.Logger = _mockLogger.Object;
            var expectedResult = Program.CanRunWeatherSensitivityEmailReport(forecastReportSettings, moq.Object);
            Assert.That(expectedResult,Is.False);
        }

        [Test]
        public void CanRunWeatherSensitivityEmailReport_ThresholdMet_test()
        {
            if (!Directory.Exists("data"))
                Directory.CreateDirectory("data");
            File.WriteAllText("data/ZipCodeList.txt", "ZIP,NAME,LAT,LNG\n06231, AMSTON, 41.62, -72.37\n06232, ANDOVER, 41.73, -72.36\n06401, ANSONIA, 41.34, -73.06\n06278, ASHFORD, 41.86, -72.11");


            var forecastReportSettings = new ForecastReportSettings();
            forecastReportSettings.EmailSubject = "UNIT TEST";
            forecastReportSettings.ForecastServiceUrl = "test";
            forecastReportSettings.ForecastServiceKey = "test";
            forecastReportSettings.ExtremeHotTempThreshold = "40";
            forecastReportSettings.TemperatureType = "hot";
            forecastReportSettings.ZipCodeList = "06232";
            forecastReportSettings.ForecastStartPeriod = "2";//two days from now
            forecastReportSettings.ForecastEndPeriod = "5";//5 days from now

            //Mock forecast service and result
            var mockForecast = File.ReadAllText(AppDomain.CurrentDomain.BaseDirectory + @"\MockData\ForecastJsonMockDataThresholdMet.txt");
            mockForecast = UpdateTimeRelativeToToday(mockForecast);
            var moq = new Mock<ForecastService>(forecastReportSettings, _mockLogger.Object);

            moq.Setup(x => x.GetHotDayThreshold()).Returns(40);
            moq.Setup(x => x.GetWeatherData(It.IsAny<string>())).Returns(mockForecast);

            Program.Logger = _mockLogger.Object;
            var expectedResult = Program.CanRunWeatherSensitivityEmailReport(forecastReportSettings, moq.Object);
            Assert.That(expectedResult, Is.True);
        }

        private string UpdateTimeRelativeToToday(string forecast)
        {
            
            bool flag = true;
            int startIndex = 0;
            int loopCounter = 0;
            while (flag)
            {
                if(startIndex>=forecast.Length)
                {
                    flag = false;
                    break;
                }

                var indexOf = forecast.IndexOf(@"""time"":", startIndex, StringComparison.Ordinal);
                if (indexOf == -1)
                {
                    flag = false;
                    break;
                }
                else
                {
                    string replaceString = forecast.Substring(indexOf + 8, 10);
                    

                    var time = DateTimeToUnixTimestamp(DateTime.Now.AddDays(loopCounter));

                    forecast = forecast.Replace(replaceString, time.ToString());

                    loopCounter++;
                    startIndex = indexOf + 5;
                }

            }

            return forecast;



        }
        private static long DateTimeToUnixTimestamp(DateTime dateTime)
        {
            var timeSpan = (dateTime.ToUniversalTime() - new DateTime(1970, 1, 1, 0, 0, 0));
            return (long)timeSpan.TotalSeconds;
        }

    }
    }
