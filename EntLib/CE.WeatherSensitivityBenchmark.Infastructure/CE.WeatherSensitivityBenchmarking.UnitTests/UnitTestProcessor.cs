﻿using CE.WeatherSensitivityBenchmarking.Infastructure.Classes;
using CE.WeatherSensitivity.Common.Classes;
using System;
using System.Linq;
using CE.WeatherSensitivityBenchmarking.Infastructure.Models;
using InsightsDW.DataModel;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using InsightsDW.DataModel.DataAccess;
using WSBenchmarksWebJob;


namespace CE.WeatherSensitivityBenchmarking.UnitTests
{
    [TestClass]
    public class UnitTestProcessor
    {
        [TestInitialize]
        public void FixEfProviderServicesProblem()
        {
            //The Entity Framework provider type 'System.Data.Entity.SqlServer.SqlProviderServices, EntityFramework.SqlServer'
            //for the 'System.Data.SqlClient' ADO.NET provider could not be loaded. 
            //Make sure the provider assembly is available to the running application. 
            //See http://go.microsoft.com/fwlink/?LinkId=260882 for more information.

            // ReSharper disable once UnusedVariable
            var instance = System.Data.Entity.SqlServer.SqlProviderServices.Instance;
        }

        /// <summary>
        /// Test the Run Check that determines if the report should be executed.
        /// </summary>
        [TestMethod] // TODO Mock this
        public void PositiveRunCheckTest()
        {
            // Create a Processing Options // TODO Mock this
            var mockProcessingOptions = MockDataCreation.SetupMockProcessingOptions();

            // Run the Process check. // TODO Mock this
            RunProcessCheck.RunCheck(mockProcessingOptions);

            // Confirm the profile name was found
            Assert.IsTrue(!string.IsNullOrEmpty(mockProcessingOptions.ProfileName));

            // Confirm report settings were found.
            Assert.IsTrue(mockProcessingOptions.ReportSettings.Any());
        }

        /// <summary>
        /// Make sure the GroupIds to be processed can be determined.
        /// </summary>
        [TestMethod]
        public void GetWsBenchmarkGroupIdsTest()
        {

            // Create a Processing Options
            var mockProcessingOptions = MockDataCreation.SetupMockProcessingOptions();

            // Get the Warehouse connection
            var insightsDataWarehouse = new InsightsDataWarehouse(mockProcessingOptions.ShardName);

            // Check the assertion
            Assert.IsTrue(insightsDataWarehouse.GetWsBenchmarkGroupIds(mockProcessingOptions.ClientId).ToList().Any());
        }


        /// <summary>
        /// Test retrieveing Ami Data for API calls.
        /// </summary>
        [TestMethod]
        public void GetAmiLookupInfoTest()
        {

            // Create a Processing Options
            int numberToProcess;
            var mockProcessingOptions = MockDataCreation.SetupMockAmiInformation(out numberToProcess);

            // Get the Ami Lookup Data
            DateTime amiStartDate;
            DateTime amiEndDate;
            int minNumberOfAmiDataNeeded;
            Program.GetAmiLookupInfo(mockProcessingOptions, out amiStartDate, out amiEndDate, out minNumberOfAmiDataNeeded);

            // Check the assertions
            Assert.IsTrue(amiStartDate.CompareTo(amiEndDate) == 0);

            Assert.IsTrue(numberToProcess == minNumberOfAmiDataNeeded);
        }

        /// <summary>
        /// Test GetConsumptionApiParameters. Unable to Mock becuase it is uing a Stored Procedure.
        /// </summary>
        [TestMethod] 
        public void GetConsumptionApiParametersTest()
        {

            // Create a Processing Options
            var mockProcessingOptions = MockDataCreation.SetupMockProcessingOptions();

            // Get the Warehouse connection 
            var insightsDataWarehouse = new InsightsDataWarehouse(mockProcessingOptions.ShardName);

            // Check the assertion
            Assert.IsTrue(insightsDataWarehouse.GetConsumptionApiParameters(mockProcessingOptions.ClientId, 11, "electric").ToList().Any());
        }

        /// <summary>
        /// Test removing of duplicates in the API parameters
        /// </summary>
        [TestMethod]
        public void RemoveDuplicatesTest()
        {
            // Create the API parameter data 
            var mockApiData = MockDataCreation.SetupMockApiParameters();


            // Check the assertions
            Assert.IsTrue(mockApiData.Count == 2);

            Assert.IsTrue(
                mockApiData.Any(
                    m => m.CustomerId.Equals("Cust_FreakyThing001", StringComparison.InvariantCultureIgnoreCase) &&
                         m.MeterIds.Equals("MeterId_FreakyThing003,MeterId_FreakyThing001,MeterId_FreakThing002",
                             StringComparison.InvariantCultureIgnoreCase)));
        }

        /// <summary>
        /// Test the Insights API call.
        /// </summary>
        [TestMethod]
        public void CallInsightsApiTest()
        {
            // Create a Processing Options
            int numberToProcess;
            var mockProcessingOptions = MockDataCreation.SetupMockAmiInformation(out numberToProcess);

            // Create the API parameter data 
            var mockApiData = MockDataCreation.SetupMockApiParameters();

            // Get the Ami Lookup Data
            DateTime amiStartDate;
            DateTime amiEndDate;
            int minNumberOfAmiDataNeeded;
            Program.GetAmiLookupInfo(mockProcessingOptions, out amiStartDate, out amiEndDate, out minNumberOfAmiDataNeeded);

            // Make a false InsightsApi call.
            string responseContent;
            var consumptionDataGathering = new ConsumptionDataGathering();
            var apiResponseOjb = consumptionDataGathering.CallInsightsApi(mockApiData[0], mockProcessingOptions.ClientId, amiStartDate, amiEndDate,
                "electric", out responseContent);

            // Check the assertions
            Assert.IsTrue(responseContent.ToLower().Contains("no data found"));

            Assert.IsTrue(string.IsNullOrEmpty(apiResponseOjb.AccountId) && string.IsNullOrEmpty(apiResponseOjb.CustomerId)
                            && apiResponseOjb.Data == null);
        }

        /// <summary>
        /// Test the ExtremeDayPeerComparison.
        /// </summary>
        [TestMethod]
        public void ExtremeDayPeerComparisonCreationTest()
        {
            // Setup the mock data
            var mockDailyWeatherDetails = MockDataCreation.SetupMockWeatherDetails();
            var mockComparisonDataForGroup = MockDataCreation.SetupMockComparisonData();
            var mockBenchMarkResults = MockDataCreation.SetupMockBenchmarkResults();
            var mockProcessingOptions = MockDataCreation.SetupMockExtremePeerCompInformation();

            // Trigger the class
            var extremeDayPeerComp = new ExtremeDayPeerComparison(mockDailyWeatherDetails);

            // Run the calculations
            extremeDayPeerComp.ProcessExtremeDayBenchmarks(mockComparisonDataForGroup,
                mockBenchMarkResults, mockProcessingOptions);

            // Check asserts
            Assert.IsTrue(mockBenchMarkResults.Count(m => m.ExtremeDay_AverageHomeUse == decimal.Zero) == 0);
            Assert.IsTrue(mockBenchMarkResults.Count(m => m.ExtremeDay_EfficientHomeUse == decimal.Zero) == 0);
            Assert.IsTrue(mockBenchMarkResults.Count(m => m.ExtremeDay_MyUsage == decimal.Zero) == 0);

            // Other asserts are possible 
        }

        /// <summary>
        /// Test the ExtremeDaySelfComparison.
        /// </summary>
        [TestMethod]
        public void ProcessExtremeDaySelfBenchmarksTest()
        {
            // Setup the mock data
            var mockDailyWeatherDetails = MockDataCreation.SetupMockWeatherDetails();
            var mockComparisonDataForGroup = MockDataCreation.SetupMockComparisonData();
            var mockBenchMarkResults = MockDataCreation.SetupMockBenchmarkResults();
            var mockProcessingOptions = MockDataCreation.SetupMockProcessingOptions();

            // Trigger the class
            var extremeDaySelfComp = new ExtremeDaySelfComparison(mockDailyWeatherDetails);

            // Run the calculations
            extremeDaySelfComp.ProcessSelfBenchmarks(mockComparisonDataForGroup,
                mockBenchMarkResults, mockProcessingOptions);

            // Check asserts
            Assert.IsTrue(mockBenchMarkResults.Count(m => m.Self_MyExtremeDayUse == decimal.Zero) == 0);
            Assert.IsTrue(mockBenchMarkResults.Count(m => m.Self_MyNormalDayUse == decimal.Zero) == 0);
        }


        /// <summary>
        /// Test the ExtremeDaySelfComparison.
        /// </summary>
        [TestMethod]
        public void RunExtremeDaySelfBenchmarksCalculations()
        {
            // Setup the mock data
            var mockDailyWeatherDetails = MockDataCreation.SetupMockWeatherDetails();
            var mockBenchMarkResults = new WSBenchmarkResult();
            var mockComparisonData = new ComparisonDataModel
            {
                MyUsageNormalDay = 150,
                MyUsageExtremeDay = 150
            };

            // Trigger the class
            var extremeDaySelfComp = new ExtremeDaySelfComparison(mockDailyWeatherDetails);
            extremeDaySelfComp.RunCalculations(mockBenchMarkResults, mockComparisonData);


            // Check asserts
            Assert.IsTrue(int.Parse(mockBenchMarkResults.Self_PcntBenchmarkValue) == 0);
            Assert.IsTrue(mockBenchMarkResults.Self_PcntBenchmarkText.Equals("percent", StringComparison.InvariantCultureIgnoreCase));
        }


        /// <summary>
        /// Test the ExtremeDaySelfComparison.
        /// </summary>
        [TestMethod]
        public void RunExtremeDayPeerBenchmarksCalculations()
        {
            // Setup the mock data
            var mockDailyWeatherDetails = MockDataCreation.SetupMockWeatherDetails();
            var mockBenchMarkResult = new WSBenchmarkResult();
            var mockComparisonData = new ComparisonDataModel
            {
                MyUsageNormalDay = 150,
                MyUsageExtremeDay = 150
            };

            var averageHomeUse = 100;
            var efficentHomeUse = 100;

            // Trigger the class
            var extremeDaySelfComp = new ExtremeDayPeerComparison(mockDailyWeatherDetails);
            extremeDaySelfComp.RunCalculations(mockBenchMarkResult, mockComparisonData, averageHomeUse, efficentHomeUse);


            // Check asserts
            Assert.IsTrue(int.Parse(mockBenchMarkResult.ExtremeDay_PcntPeerBenchmarkValue) == 50);
            Assert.IsTrue(mockBenchMarkResult.ExtremeDay_PcntPeerBenchmarkText.Equals("percent", StringComparison.InvariantCultureIgnoreCase));
        }
    }
}
