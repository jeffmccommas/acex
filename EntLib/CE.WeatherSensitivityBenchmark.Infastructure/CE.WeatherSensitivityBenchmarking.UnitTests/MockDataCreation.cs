﻿using System;
using System.Collections.Generic;
using CE.WeatherSensitivity.Common.Models;
using CE.WeatherSensitivityBenchmarking.Infastructure.Models;
using InsightsDW.DataModel;

namespace CE.WeatherSensitivityBenchmarking.UnitTests
{
    public static class MockDataCreation
    {
        /// <summary>
        /// Setup mock Processing Options (with some database additions)
        /// </summary>
        /// <returns>Mock processing options</returns>
        public static ProcessingOptions SetupMockProcessingOptions()
        {
            // Create a Processing Options
            var processingOptions = new ProcessingOptions
            {
                ClientId = 224,
                ReportAlias = "ws",
                ReportRequestDate = new DateTime(2017, 2, 15),
                ShardName = "InsightsDW_224_2017-02-14T21-44Z"
            };

            // Get the Shard Name
            return processingOptions;
        }

        /// <summary>
        /// Setup Mock Ami Information
        /// </summary>
        /// <returns>Mock processing options with AMI information</returns>
        public static ProcessingOptions SetupMockAmiInformation(out int numberToProcess)
        {
            var processingOptions = SetupMockProcessingOptions();

            // Add the AMI information
            var optionsToAdd = new List<WSReportSetting>();

            var amiStartDateObj = new WSReportSetting
            {
                Name = "ami_start_date",
                Value = "1/1/2017"
            };
            optionsToAdd.Add(amiStartDateObj);


            var amiEndDateObj = new WSReportSetting
            {
                Name = "ami_end_date",
                Value = "1/1/2017"
            };
            optionsToAdd.Add(amiEndDateObj);


            var amiTimezoneObj = new WSReportSetting
            {
                Name = "ami_data_timezone",
                Value = "Eastern Standard Time"
            };
            optionsToAdd.Add(amiTimezoneObj);

            numberToProcess = 20;
            var amiDataPointsObj = new WSReportSetting
            {
                Name = "ami_min_data_points",
                Value = numberToProcess.ToString()
            };
            optionsToAdd.Add(amiDataPointsObj);

            processingOptions.ReportSettings = processingOptions.ReportSettings = optionsToAdd;

            return processingOptions;
        }

        /// <summary>
        /// Create Mock API Parameters
        /// </summary>
        /// <returns>Mock Api Parameters</returns>
        public static List<WSRetrieveConsumptionAPIParameters_Result> SetupMockApiParameters()
        {
            var theData = new List<WSRetrieveConsumptionAPIParameters_Result>();

            var datumOne = new WSRetrieveConsumptionAPIParameters_Result
            {
                AccountId = "Acct_FreakyThing001",
                CustomerId = "Cust_FreakyThing001",
                MeterIds = "MeterId_FreakyThing003",
                PremiseId = "Prem_FreakyThing001"
            };
            theData.Add(datumOne);

            var datumTwo = new WSRetrieveConsumptionAPIParameters_Result
            {
                AccountId = "Acct_FreakyThing001",
                CustomerId = "Cust_FreakyThing001",
                MeterIds = "MeterId_FreakyThing001,MeterId_FreakThing002",
                PremiseId = "Prem_FreakyThing001"
            };
            theData.Add(datumTwo);

            var datumThree = new WSRetrieveConsumptionAPIParameters_Result
            {
                AccountId = "Acct_FreakyThing002",
                CustomerId = "Cust_FreakyThing002",
                MeterIds = "MeterId_FreakyThing002",
                PremiseId = "Prem_FreakyThing002"
            };
            theData.Add(datumThree);

            return theData;
        }

        /// <summary>
        /// Generate five days of Mock weather data
        /// </summary>
        /// <returns>A list of Mock Weather data</returns>
        public static List<EMDailyWeatherDetail> SetupMockWeatherDetails()
        {
            var theData = new List<EMDailyWeatherDetail>();

            //StationID WeatherReadingDate      MinTemp MaxTemp AvgTemp AvgWetBulb  HeatingDegreeDays CoolingDegreeDays   DataIsDerived Comment
            //54788     2016 - 08 - 13 00:00:00    75   97      86      77          0                   21                  NULL        NULL
            var datumOne = new EMDailyWeatherDetail
            {
                AvgTemp = 86,
                AvgWetBulb = 77,
                Comment = "Mock Weather Datum One",
                CoolingDegreeDays = 21,
                DataIsDerived = null,
                HeatingDegreeDays = 0,
                MaxTemp = 97,
                MinTemp = 75,
                StationID = "54788",
                WeatherReadingDate = DateTime.Parse("2016 - 08 - 13 00:00:00")
            };

            theData.Add(datumOne);

            //StationID WeatherReadingDate      MinTemp MaxTemp AvgTemp AvgWetBulb  HeatingDegreeDays CoolingDegreeDays   DataIsDerived Comment
            //94702     2016 - 07 - 23 00:00:00    76   97      87      81          0                   22                  NULL        ,avgwetbulb derived
            var datumTwo = new EMDailyWeatherDetail
            {
                AvgTemp = 87,
                AvgWetBulb = 81,
                Comment = "Mock Weather Datum Two",
                CoolingDegreeDays = 22,
                DataIsDerived = null,
                HeatingDegreeDays = 0,
                MaxTemp = 97,
                MinTemp = 76,
                StationID = "94702",
                WeatherReadingDate = DateTime.Parse("2016 - 07 - 23 00:00:00")
            };

            theData.Add(datumTwo);

            //StationID WeatherReadingDate      MinTemp MaxTemp AvgTemp AvgWetBulb  HeatingDegreeDays CoolingDegreeDays   DataIsDerived Comment
            //54788     2016 - 07 - 22 00:00:00    67   96      82      73          0                   17                  NULL        NULL
            var datumThree = new EMDailyWeatherDetail
            {
                AvgTemp = 82,
                AvgWetBulb = 73,
                Comment = "Mock Weather Datum Three",
                CoolingDegreeDays = 17,
                DataIsDerived = null,
                HeatingDegreeDays = 0,
                MaxTemp = 96,
                MinTemp = 67,
                StationID = "54788",
                WeatherReadingDate = DateTime.Parse("2016 - 07 - 22 00:00:00")
            };

            theData.Add(datumThree);

            //StationID WeatherReadingDate      MinTemp MaxTemp AvgTemp AvgWetBulb  HeatingDegreeDays CoolingDegreeDays   DataIsDerived Comment
            //54788     2016 - 07 - 23 00:00:00    70   95      83      70          0                   18                  NULL        NULL
            var datumFour = new EMDailyWeatherDetail
            {
                AvgTemp = 83,
                AvgWetBulb = 70,
                Comment = "Mock Weather Datum Four",
                CoolingDegreeDays = 18,
                DataIsDerived = null,
                HeatingDegreeDays = 0,
                MaxTemp = 95,
                MinTemp = 70,
                StationID = "54788",
                WeatherReadingDate = DateTime.Parse("2016 - 07 - 23 00:00:00")
            };

            theData.Add(datumFour);

            //StationID WeatherReadingDate      MinTemp MaxTemp AvgTemp AvgWetBulb  HeatingDegreeDays CoolingDegreeDays   DataIsDerived Comment
            //54788     2016 - 07 - 28 00:00:00    66   95      81      71          0                   16                  NULL        NULL
            var datumFive = new EMDailyWeatherDetail
            {
                AvgTemp = 81,
                AvgWetBulb = 71,
                Comment = "Mock Weather Datum Five",
                CoolingDegreeDays = 16,
                DataIsDerived = null,
                HeatingDegreeDays = 0,
                MaxTemp = 95,
                MinTemp = 66,
                StationID = "54788",
                WeatherReadingDate = DateTime.Parse("2016 - 07 - 28 00:00:00")
            };

            theData.Add(datumFive);

            // Return the Mock data.
            return theData;
        }

        /// <summary>
        /// Generate the Comparison Data
        /// </summary>
        /// <returns>A list of Mock Comparison data</returns>
        public static List<ComparisonDataModel> SetupMockComparisonData()
        {
            var theData = new List<ComparisonDataModel>();

            // Generate Comparison Data One
            var datumOne = new ComparisonDataModel
            {
                AccountId = "Acct_FreakyThing001",
                CustomerId = "Cust_FreakyThing001",
                PremiseId = "Prem_FreakyThing001",
                CommodityKey = "electric",
                GroupId = 1,
                Data = SetupMockConsumptionData(1)
            };
            theData.Add(datumOne);

            var datumTwo = new ComparisonDataModel
            {
                AccountId = "Acct_FreakyThing002",
                CustomerId = "Cust_FreakyThing002",
                PremiseId = "Prem_FreakyThing002",
                CommodityKey = "electric",
                GroupId = 1,
                Data = SetupMockConsumptionData(1)
            };
            theData.Add(datumTwo);


            var datumThree = new ComparisonDataModel
            {
                AccountId = "Acct_FreakyThing003",
                CustomerId = "Cust_FreakyThing003",
                PremiseId = "Prem_FreakyThing003",
                CommodityKey = "electric",
                GroupId = 1,
                Data = SetupMockConsumptionData(2.5)
            };
            theData.Add(datumThree);


            var datumFour = new ComparisonDataModel
            {
                AccountId = "Acct_FreakyThing004",
                CustomerId = "Cust_FreakyThing004",
                PremiseId = "Prem_FreakyThing004",
                CommodityKey = "electric",
                GroupId = 1,
                Data = SetupMockConsumptionData(1.5)
            };
            theData.Add(datumFour);


            var datumFive = new ComparisonDataModel
            {
                AccountId = "Acct_FreakyThing005",
                CustomerId = "Cust_FreakyThing005",
                PremiseId = "Prem_FreakyThing005",
                CommodityKey = "electric",
                GroupId = 1,
                Data = SetupMockConsumptionData(1.25)
            };
            theData.Add(datumFive);



            // Return the Mock weather data.
            return theData;
        }

        /// <summary>
        /// Generate the AMI data points
        /// </summary>
        /// <param name="increment">The increment to increase the data</param>
        /// <returns>A list of Mock Consumption data</returns>
        private static List<ConsumptionApiModels.ConsumptionData> SetupMockConsumptionData(double increment)
        {
            var theData = new List<ConsumptionApiModels.ConsumptionData>();
            var data1 = new ConsumptionApiModels.ConsumptionData
            {
                DateTime = "2016 - 08 - 13 00:00:00", // use same dates as used in WSDailyWeatherResults mock (see above)
                Value = 10 * increment
            };
            theData.Add(data1);

            var data2 = new ConsumptionApiModels.ConsumptionData
            {
                DateTime = "2016 - 08 - 13 00:00:00",
                Value = 10 * increment
            };
            theData.Add(data2);


            var data3 = new ConsumptionApiModels.ConsumptionData
            {
                DateTime = "2016 - 07 - 22 00:00:00",
                Value = 10 * increment
            };
            theData.Add(data3);


            var data4 = new ConsumptionApiModels.ConsumptionData
            {
                DateTime = "2016 - 07 - 22 00:00:00",
                Value = 10 * increment
            };
            theData.Add(data4);

            var data5 = new ConsumptionApiModels.ConsumptionData
            {
                DateTime = "2016 - 07 - 23 00:00:00",
                Value = 10 * increment
            };
            theData.Add(data5);


            var data6 = new ConsumptionApiModels.ConsumptionData
            {
                DateTime = "2016 - 07 - 23 00:00:00",
                Value = 10 * increment
            };
            theData.Add(data6);


            var data7 = new ConsumptionApiModels.ConsumptionData
            {
                DateTime = "2016 - 07 - 22 00:00:00",
                Value = 10 * increment
            };
            theData.Add(data7);


            var data8 = new ConsumptionApiModels.ConsumptionData
            {
                DateTime = "2016 - 08 - 13 00:00:00",
                Value = 10 * increment
            };
            theData.Add(data8);


            var data9 = new ConsumptionApiModels.ConsumptionData
            {
                DateTime = "2016 - 08 - 13 00:00:00",
                Value = 10 * increment
            };
            theData.Add(data9);

            var data10 = new ConsumptionApiModels.ConsumptionData
            {
                DateTime = "2016 - 08 - 21 00:00:00",
                Value = 10 * increment
            };
            theData.Add(data10);

            // Return mock data
            return theData;
        }

        /// <summary>
        /// Generate Benchmark results
        /// </summary>
        /// <returns>A list of Mock Benchmark result objects</returns>
        public static List<WSBenchmarkResult> SetupMockBenchmarkResults()
        {
            var theData = new List<WSBenchmarkResult>();

            var datumOne = new WSBenchmarkResult
            {
                AccountId = "Acct_FreakyThing001",
                CustomerId = "Cust_FreakyThing001",
                PremiseId = "Prem_FreakyThing001",
                GroupId = 1,
                ClientId = 224
            };
            theData.Add(datumOne);

            var datumTwo = new WSBenchmarkResult
            {
                AccountId = "Acct_FreakyThing002",
                CustomerId = "Cust_FreakyThing002",
                PremiseId = "Prem_FreakyThing002",
                GroupId = 1,
                ClientId = 224
            };
            theData.Add(datumTwo);

            var datumThree = new WSBenchmarkResult
            {
                AccountId = "Acct_FreakyThing003",
                CustomerId = "Cust_FreakyThing003",
                PremiseId = "Prem_FreakyThing003",
                GroupId = 1,
                ClientId = 224
            };
            theData.Add(datumThree);

            var datumFour = new WSBenchmarkResult
            {
                AccountId = "Acct_FreakyThing004",
                CustomerId = "Cust_FreakyThing004",
                PremiseId = "Prem_FreakyThing004",
                GroupId = 1,
                ClientId = 224
            };
            theData.Add(datumFour);

            var datumFive = new WSBenchmarkResult
            {
                AccountId = "Acct_FreakyThing005",
                CustomerId = "Cust_FreakyThing005",
                PremiseId = "Prem_FreakyThing005",
                GroupId = 1,
                ClientId = 224
            };
            theData.Add(datumFive);


            // Return mock data
            return theData;
        }

        /// <summary>
        /// Setup the report settings for the Extreme Day Peer Comparison
        /// </summary>
        /// <returns>A Mock Processing Options</returns>
        public static ProcessingOptions SetupMockExtremePeerCompInformation()
        {
            // Setup the initial items
            var theData = SetupMockProcessingOptions();

            // Create the report settings
            //extreme_max_average_percentile
            //extreme_min_average_percentile
            //extreme_efficent_home_use_percentile
            var optionsToAdd = new List<WSReportSetting>();

            var reportSettingOne = new WSReportSetting
            {
                Name = "extreme_max_average_percentile",
                Value = "55"
            };
            optionsToAdd.Add(reportSettingOne);


            var reportSettingTwo = new WSReportSetting
            {
                Name = "extreme_min_average_percentile",
                Value = "15"
            };
            optionsToAdd.Add(reportSettingTwo);


            var reportSettingThree = new WSReportSetting
            {
                Name = "extreme_efficent_home_use_percentile",
                Value = "15"
            };
            optionsToAdd.Add(reportSettingThree);

            theData.ReportSettings = theData.ReportSettings = optionsToAdd;

            // return the mock data
            return theData;
        }

    }
}
