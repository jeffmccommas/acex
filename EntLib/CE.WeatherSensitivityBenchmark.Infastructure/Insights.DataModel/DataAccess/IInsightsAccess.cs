﻿namespace Insights.DataModel.DataAccess
{
    public interface IInsightsAccess
    {

        string GetCeAccessKey(int clientId);
    }
}
