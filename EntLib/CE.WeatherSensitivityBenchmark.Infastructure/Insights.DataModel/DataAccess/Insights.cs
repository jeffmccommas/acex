﻿using System;
using System.Linq;
using Insights.DataModel.Models;

namespace Insights.DataModel.DataAccess
{
    public class Insights
    {
        /// <summary>
        /// Get the CEAccessKey and ClientId for API calls.
        /// </summary>
        /// <param name="clientId">The current client Id</param>
        /// <param name="enviornmentId"></param>
        /// <param name="endpointId"></param>
        /// <returns>The CEAccessKey</returns>
        public UserData GetUserInfo(int clientId, byte enviornmentId, int endpointId)
        {
            var db = new InsightsEntities();

            var userData = db.Users.First(m => m.ActorName.Equals("BIUILUser", StringComparison.InvariantCultureIgnoreCase) &&
                                       m.ClientID == clientId);

            var userEndPoints = userData.UserEndpoints.First(m => m.EndpointID == endpointId && m.EnableInd);

            var userEnviornment = userData.UserEnvironments.First(m => m.EnvID == enviornmentId);

            var returnModel = new UserData
            {
                CeAccessKeyId = userData.CEAccessKeyID,
                AuthType = userData.Client.AuthType,
                CeSecretAccessKey = userEnviornment.CESecretAccessKey,
                Endpoint = userEndPoints.Endpoint.Name,
                BasicKey = userEnviornment.BasicKey
            };


            return returnModel;
        }


    }
}
