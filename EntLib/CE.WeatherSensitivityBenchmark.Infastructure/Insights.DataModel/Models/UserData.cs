﻿namespace Insights.DataModel.Models
{
    public class UserData
    {
        /// <summary>
        /// The Access Key for the User
        /// </summary>
        public string CeAccessKeyId { get; set; }

        /// <summary>
        /// The Auth type the User
        /// </summary>
        public int AuthType { get; set; }

        /// <summary>
        /// The clients secret accessKey
        /// </summary>
        public string CeSecretAccessKey { get; set; }

        /// <summary>
        /// The Endpoint for the API call
        /// </summary>
        public string Endpoint { get; set; }


        /// <summary>
        /// The clients basic key
        /// </summary>
        public string BasicKey { get; set; }

    }
}
