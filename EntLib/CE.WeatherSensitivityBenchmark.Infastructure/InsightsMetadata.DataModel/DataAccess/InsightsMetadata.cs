﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;

// ReSharper disable once CheckNamespace
namespace InsightsMetadata.DataModel.DataAccess
{
    public class InsightsMetadata : IMetaDataAccess
    {
        /// <summary>
        /// Get the shard name for a specific client.
        /// </summary>
        /// <param name="clientId">The client Id </param>
        /// <param name="theEnvironment">current environment</param>
        public string GetShardName(int clientId, string theEnvironment)
        {
            var db = new InsightsMetadataEntities();

            if (string.IsNullOrEmpty(theEnvironment))
            {
                theEnvironment = "prod"; //set default value
            }

            var firstOrDefault = db.ClientConfigurations.FirstOrDefault(
                c =>
                    c.ConfigurationKey.Equals("weather.sensitivity.shardname",
                        StringComparison.InvariantCultureIgnoreCase) &&
                    c.ClientID == clientId &&
                    c.CategoryKey.Equals("wsbenchmark", StringComparison.InvariantCultureIgnoreCase) &&
                    c.EnvironmentKey.Equals(theEnvironment, StringComparison.InvariantCultureIgnoreCase));


            if (firstOrDefault == null)
            {
                return db.ClientConfigurations.First(
                    c =>
                        c.ConfigurationKey.Equals("weather.sensitivity.shardname",
                            StringComparison.InvariantCultureIgnoreCase) &&
                        c.ClientID == clientId &&
                        c.CategoryKey.Equals("wsbenchmark", StringComparison.InvariantCultureIgnoreCase) &&
                        c.EnvironmentKey.Equals("prod", StringComparison.InvariantCultureIgnoreCase)).Value;
            }

            return firstOrDefault.Value;
        }
    }
}
