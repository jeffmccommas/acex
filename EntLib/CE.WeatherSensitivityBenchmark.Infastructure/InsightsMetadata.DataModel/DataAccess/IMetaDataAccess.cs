﻿
namespace InsightsMetadata.DataModel.DataAccess
{
    public interface IMetaDataAccess
    {
        string GetShardName(int clientId, string theEnvironment);
    }
}
