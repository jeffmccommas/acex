﻿using System;
using System.Net.Mail;
using System.Net.Mime;
using CE.WeatherSensitivity.Common.Models;
using log4net;

namespace ReportOutputLib
{
    public class Notification
    {
        //private readonly object _configurationSection;
        private readonly ILog _logger;
        private readonly ForecastReportSettings _forecastReportSettings;
        
        public Notification(ForecastReportSettings forecastReportSettings, ILog logger)
        {
            _logger = logger;
            _forecastReportSettings = forecastReportSettings;
        }

        public bool SendEmail(string message)
        {
            try
            {
                MailMessage mailMsg = new MailMessage();

                // To
                foreach (var emailAddress in _forecastReportSettings.ToEmailList.Split(','))
                {
                    mailMsg.To.Add(new MailAddress(emailAddress));
                }
                
                // From
                mailMsg.From = new MailAddress(_forecastReportSettings.ForecastEmailFrom, _forecastReportSettings.ForecastEmailFromName);

                // Subject and multipart/alternative Body
                mailMsg.Subject = _forecastReportSettings.EmailSubject;
                string text = message;
                //string html = @"<h1>html body</h1>";
                mailMsg.AlternateViews.Add(AlternateView.CreateAlternateViewFromString(text, null,
                    MediaTypeNames.Text.Plain));
                //mailMsg.AlternateViews.Add(AlternateView.CreateAlternateViewFromString(html, null, MediaTypeNames.Text.Html));

                // Init SmtpClient and send
                SmtpClient smtpClient = new SmtpClient(_forecastReportSettings.ForecastEmailSmtpServer, Convert.ToInt32(_forecastReportSettings.ForecastEmailSmtpPort));

                System.Net.NetworkCredential credentials = new System.Net.NetworkCredential(_forecastReportSettings.ForecastEmailSmtpUser,
                    _forecastReportSettings.ForecastEmailSmtpPassword);
                smtpClient.Credentials = credentials;
                smtpClient.EnableSsl = Convert.ToBoolean(_forecastReportSettings.ForecastEmailSmtpSsl);

                smtpClient.Send(mailMsg);

            }
            catch (Exception ex)
            {
                _logger.Error("SendEmail: ", ex);
                return false;
            }

            return true;

        }
      
    }
}
