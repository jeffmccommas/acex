﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WeatherForecastService;
using WeatherForecastService.models;
using WSWorkflowDataAccess.models;

namespace ReportOutputLib
{
    public static class ForecastMapper
    {
        public static WsForecastModel MapToEmailSummary(List<WeatherForecastService.models.RootObject> dailyForecastByZones, 
            WeatherSensitivityAttribute weatherSensitivityAttribute,IForecastService forecastService)
        {
            var weatherForecastByZone = new List<WsZoneForecastModel>();
            foreach (var zone in dailyForecastByZones)
            {
                var wsZoneForecastModel = new WsZoneForecastModel
                {
                    Name = new ZipCodeHelper().GetZipCode(zone.latitude.ToString(),zone.longitude.ToString()).name,
                    Forecast = ConvertForecastToStringFormat(zone.daily,forecastService),
                    ZipCode = new ZipCodeHelper().GetZipCode(zone.latitude.ToString(), zone.longitude.ToString()).zip
                };
                weatherForecastByZone.Add(wsZoneForecastModel);
            }
            

            var forecastModel = new WsForecastModel
            {
                TodaysDate = DateTime.Now,
                ThresholdValue = weatherSensitivityAttribute == WeatherSensitivityAttribute.HOT_WEATHER
                        ? forecastService.GetHotDayThreshold().ToString()
                        : forecastService.GetColdDayThreshold().ToString(),
                
                WeatherAttribute =
                    weatherSensitivityAttribute == WeatherSensitivityAttribute.COLD_WEATHER
                        ? "ColdDayTemp"
                        : "HotDayTemp",
                ForecastByZone = weatherForecastByZone

            };


            return forecastModel;
        }

        private static List<string> ConvertForecastToStringFormat(WeatherForecastService.models.Daily forecast, IForecastService forecastService)
        {
            List<string> forecastInStringFormat =  new List<string>();
            string extremeDayCount = forecastService.GetHotDayExtremeForecast(forecast).data.Count().ToString();

            forecastInStringFormat.Add($"Number of days with temperature above threshold [ {extremeDayCount} day(s) ]");

            foreach (var dailyForecast in forecast.data)
            {
                forecastInStringFormat.Add("[Date : " + forecastService.UnixTimeStampToDateTime(dailyForecast.time).ToString("d")+"]");
                forecastInStringFormat.Add("Summary: " + dailyForecast.summary);
                string tempInfo = "";
                if (forecastService.GetWeatherSensitivityAttribute() == WeatherSensitivityAttribute.HOT_WEATHER)
                {
                    tempInfo = "High Temperature: " + dailyForecast.temperatureMax;//need to confirm requirements
                    if (dailyForecast.temperatureMax >= forecastService.GetHotDayThreshold())
                        tempInfo += "    <--";
                }
                else
                {
                    tempInfo ="Low Temperature: " + dailyForecast.temperatureMin;//need to confirm requirements
                    if (dailyForecast.temperatureMin <= forecastService.GetColdDayThreshold())
                        tempInfo += "    <--";

                }

                forecastInStringFormat.Add("Humidity: " + dailyForecast.humidity);

                forecastInStringFormat.Add(tempInfo);
            }

            forecastInStringFormat.Add("");
            return forecastInStringFormat;
        }
    }
}
