﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using CE.WeatherSensitivity.Common.Classes;
using WSWorkflowDataAccess.models;

namespace ReportOutputLib
{
    public class ReportOutput : IReportOutput
    {
        private IFileService _fileService;
        public ReportOutput(IFileService fileService)
        {
            _fileService = fileService;
        }

        public void WriteEmailReportToCSV(List<WsEmailReportModel> emailReportModel, WsForecastModel forecastModel, bool overwrite)
        {
            StringBuilder reportBuilder = new StringBuilder();
            var colHeaders = GetEmailCsvColumnNames();
            reportBuilder.AppendLine(colHeaders);

            foreach (var item in emailReportModel)
            {
                reportBuilder.AppendLine(GetReportCsvLine(item));
            }

            _fileService.WriteAllText(EmailReportOutputFile, reportBuilder.ToString(), overwrite);
        }

        private string GetReportCsvLine(WsEmailReportModel reportModel)
        {
            if (!string.IsNullOrEmpty(reportModel.PcntPeerBenchmark))
            {

                string pcntPeerBenchmark = reportModel.PcntPeerBenchmark;
                reportModel.PcntPeerBenchmark = reportModel.PcntPeerBenchmarkText.ToLower() == "percent"
                    ? reportModel.PcntPeerBenchmark = (Convert.ToDouble(reportModel.PcntPeerBenchmark)).ToString() + "%"
                    : reportModel.PcntPeerBenchmark + reportModel.PcntPeerBenchmarkText;

                reportModel.PcntPeerBenchmarkText = reportModel.PcntPeerBenchmarkText.ToLower() == "x"
                    ? pcntPeerBenchmark + reportModel.PcntPeerBenchmarkText
                    : (Convert.ToDouble(pcntPeerBenchmark)).ToString() + " " + reportModel.PcntPeerBenchmarkText;
            }
            // reportModel.PcntPeerBenchmark = ReportHelper.CalculatePcntPeerBenchmark(reportModel.MyHomeUse,
            //  reportModel.AverageHomeUse, reportModel.EfficientHomeUse);
            //reportModel.PcntPeerBenchmarkText = ReportHelper.PcntPeerBenchmarkToText(reportModel.PcntPeerBenchmark);

            var colValueList = new List<string>
            {
                reportModel.SubscriberKey,
                reportModel.AccountNumber,
                reportModel.CustomerId,
                reportModel.PremiseId.ToString(),
                reportModel.ReferrerId,
                reportModel.Email,
                reportModel.ProgramReportNumber,
                reportModel.ReportMonth,
                reportModel.ReportYear,
                reportModel.ReportPeriodStart==null?"": reportModel.ReportPeriodStart.GetValueOrDefault().ToString("yyyyMMdd"),
                reportModel.ReportPeriodEnd.ToString("yyyyMMdd"),
                reportModel.SegmentId,
                reportModel.HesParticipant,
                reportModel.MaskedAccountNumber,
                reportModel.MyHomeUse,
                reportModel.AverageHomeUse,
                reportModel.EfficientHomeUse,
                reportModel.PcntPeerBenchmark,
                reportModel.PcntPeerBenchmarkText,
                reportModel.MeasureName1,
                reportModel.MeasureSavings1,
                reportModel.MeasureName2,
                reportModel.MeasureSavings2,
                reportModel.MeasureName3,
                reportModel.MeasureSavings3
            };

            return string.Join(",", colValueList);
        }

        private string GetReportCsvLine(WsPrintReportModel reportModel)
        {

            if (!string.IsNullOrEmpty(reportModel.PcntPeerBenchmark))
            {
                
                string pcntPeerBenchmark = reportModel.PcntPeerBenchmark;
                reportModel.PcntPeerBenchmark = reportModel.PcntPeerBenchmarkText.ToLower() == "percent"
                    ? reportModel.PcntPeerBenchmark = (Convert.ToDouble(pcntPeerBenchmark)).ToString() + "%"
                    : reportModel.PcntPeerBenchmark + reportModel.PcntPeerBenchmarkText;

                reportModel.PcntPeerBenchmarkText = reportModel.PcntPeerBenchmarkText.ToLower() == "x" 
                    ? pcntPeerBenchmark + reportModel.PcntPeerBenchmarkText 
                    : (Convert.ToDouble(pcntPeerBenchmark)).ToString() + " " + reportModel.PcntPeerBenchmarkText;
            }

            if (!string.IsNullOrEmpty(reportModel.PcntSelfBenchmark))
            {
                if (Convert.ToDouble(reportModel.MyExtremeDayUse) == 0 ||
                    string.IsNullOrEmpty(reportModel.MyExtremeDayUse) ||
                    Convert.ToDouble(reportModel.MyNormalDayUse) == 0 ||
                    string.IsNullOrEmpty(reportModel.MyNormalDayUse)
                )
                {
                    reportModel.PcntSelfBenchmark = "Null";
                    reportModel.PcntSelfBenchmarkText = "null";
                }
                else
                {
                    string pcntSelfBenchmark = reportModel.PcntSelfBenchmark;
                    reportModel.PcntSelfBenchmark = reportModel.PcntSelfBenchmarkText.ToLower() == "percent"
                        ? (Convert.ToDouble(pcntSelfBenchmark)).ToString() + "%"
                        : reportModel.PcntSelfBenchmark + reportModel.PcntSelfBenchmarkText;


                    reportModel.PcntSelfBenchmarkText = reportModel.PcntSelfBenchmarkText.ToLower() == "x"
                        ? pcntSelfBenchmark + reportModel.PcntSelfBenchmarkText
                        : (Convert.ToDouble(pcntSelfBenchmark)).ToString() + " " + reportModel.PcntSelfBenchmarkText;
                }
            }


            var colValueList = new List<string>
            {
                reportModel.AccountNumber,
                reportModel.CustomerId,
                reportModel.PremiseId,
                reportModel.ReferrerId,
                string.IsNullOrEmpty(reportModel.CustomerName)?"": reportModel.CustomerName.Contains("{}")? ("\""+reportModel.CustomerName.Replace("{}",",")+"\"") :reportModel.CustomerName,
                string.IsNullOrEmpty(reportModel.MailingAddress)?"": reportModel.MailingAddress.Contains("{}")? ("\""+reportModel.MailingAddress.Replace("{}",",")+"\"") :reportModel.MailingAddress,
                string.IsNullOrEmpty(reportModel.MailingAddress2)?"": reportModel.MailingAddress2.Contains("{}")? ("\""+reportModel.MailingAddress2.Replace("{}",",")+"\"") :reportModel.MailingAddress2,
                //reportModel.MailingAddress,
                //reportModel.MailingAddress2,
                reportModel.MailingCity,
                reportModel.MailingState,
                reportModel.MailingZip,
                reportModel.ProgramReportNumber,
                reportModel.SegmentId,
                reportModel.HesParticipant,
                reportModel.ReportMonth,
                reportModel.ReportYear,
                reportModel.ReportPeriodStart==null?"": reportModel.ReportPeriodStart.GetValueOrDefault().ToString("yyyyMMdd"),
                reportModel.ReportPeriodEnd.ToString("yyyyMMdd"),
                reportModel.MaskedAccountNumber,
                reportModel.MyHomeUse,
                reportModel.AverageHomeUse,
                reportModel.EfficientHomeUse,
                reportModel.PcntPeerBenchmark,
                reportModel.PcntPeerBenchmarkText,
                string.IsNullOrEmpty(reportModel.MeasureName1)?"": reportModel.MeasureName1.Contains("{}")? ("\""+reportModel.MeasureName1.Replace("{}",",")+"\"") :reportModel.MeasureName1,
               
                reportModel.MeasureSavings1,
                string.IsNullOrEmpty(reportModel.MeasureName2)?"": reportModel.MeasureName2.Contains("{}")? ("\""+reportModel.MeasureName2.Replace("{}",",")+"\"") :reportModel.MeasureName2,
                reportModel.MeasureSavings2,
                string.IsNullOrEmpty(reportModel.MeasureName3)?"": reportModel.MeasureName3.Contains("{}")? ("\""+reportModel.MeasureName3.Replace("{}",",")+"\"") :reportModel.MeasureName3,
                reportModel.MeasureSavings3,
                reportModel.MyNormalDayUse,
                reportModel.MyExtremeDayUse,
                reportModel.PcntSelfBenchmark,
                reportModel.PcntSelfBenchmarkText
            };

            return string.Join(",", colValueList);

           // return csvline.Replace("{}", "\",\"");
        }

        public string GenerateSummaryEmail(WsForecastModel forecastModel)
        {
            var emailHeader = GetEmailHeader(forecastModel);
            var emailContent = GetEmailContent(forecastModel);
            return emailHeader + "\n\n\n" + emailContent;

        }

        private string GetEmailHeader(WsForecastModel forecastModel)
        {
            var header = new StringBuilder();

            //make text configurable

            header.AppendLine("********************************************************");
            header.AppendLine("Today's Date: " + forecastModel.TodaysDate.ToString("d"));//d: 6/15/2008
            header.AppendLine("Weather Attribute: DailyHighTemp");
            header.AppendLine("Threshold: " + forecastModel.ThresholdValue);
            header.AppendLine("********************************************************");

            return header.ToString();
        }
        private string GetEmailContent(WsForecastModel forecastModel)
        {
            StringBuilder content = new StringBuilder();
            int counter = 1;
            foreach (var zone in forecastModel.ForecastByZone)
            {
                content.AppendLine($"{counter}.  {zone.Name} ({zone.ZipCode}):");
                counter = counter + 1;
                foreach (var dailyForecast in zone.Forecast)
                {
                    content.AppendLine(dailyForecast);
                }
                content.AppendLine("");
                content.AppendLine("");

            }
            return content.ToString();
        }
        private string GetEmailCsvColumnNames()
        {
            var colHeaderList = new List<string>
            {
                "SubscriberKey",
                "AccountNumber",
                "CustomerId",
                "PremiseId",
                "ReferrerId",
                "Email",
                "ProgramReportNumber",
                "ReportMonth",
                "ReportYear",
                "ReportPeriodStart",
                "ReportPeriodEnd",
                "SegmentId",
                "HESParticipant",
                "MaskedAccountNumber",
                "MyHomeUse",
                "AverageHomeUse",
                "EfficientHomeUse",
                "PcntPeerBenchmark",
                "PcntPeerBenchmarkText",
                "MeasureName1",
                "MeasureSavings1",
                "MeasureName2",
                "MeasureSavings2",
                "MeasureName3",
                "MeasureSavings3"
            };


            return string.Join(",", colHeaderList);
        }

        private string GetPrintCsvColumnNames()
        {
            var colHeaderList = new List<string>
            {
                "AccountNumber",
                "CustomerId",
                "PremiseId",
                "ReferrerId",
                "CustomerName",
                "MailingAddress",
                "MailingAddress2",
                "MailingCity",
                "MailingState",
                "MailingZip",
                "ProgramReportNumber",
                "SegmentId",
                "HESParticipant",
                "ReportMonth",
                "ReportYear",
                "ReportPeriodStart",
                "ReportPeriodEnd",
                "MaskedAccountNumber",
                "MyHomeUse",
                "AverageHomeUse",
                "EfficientHomeUse",
                "PcntPeerBenchmark",
                "PcntPeerBenchmarkText",
                "MeasureName1",
                "MeasureSavings1",
                "MeasureName2",
                "MeasureSavings2",
                "MeasureName3",
                "MeasureSavings3",
                "MyNormalDayUse",
                "MyExtremeDayUse",
                "PcntSelfBenchmark",
                "PcntSelfBenchmarkText"
            };


            return string.Join(",", colHeaderList);
        }
        public void WritePrintReportToCSV(List<WsPrintReportModel> printReportModel, bool overwrite)
        {
            StringBuilder reportBuilder = new StringBuilder();
            //if (!_fileService.Exists(PrintReportOutputFile))
            //{
                //var header = GetEmailHeader(forecastModel);
                var colHeaders = GetPrintCsvColumnNames();
                //foreach (var line in header)
                //{
                //   reportBuilder.AppendLine(line);
                //}
                reportBuilder.AppendLine(colHeaders);
            //}

            foreach (var item in printReportModel)
            {
                reportBuilder.AppendLine(GetReportCsvLine(item));
            }

            _fileService.WriteAllText(PrintReportOutputFile, reportBuilder.ToString(),overwrite);
        }

        public string EmailReportOutputFile { get; set; }
        public string PrintReportOutputFile { get; set; }
    }
}
