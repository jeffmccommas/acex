﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.WindowsAzure.Storage.File.Protocol;

namespace ReportOutputLib
{
    public interface IFileService
    {
        string StorageName { get; set; }
        string StorageKey { get; set; }
        string ContainerName { get; set; }
        bool Exists(string path);
        void Delete(string path);
        void WriteAllText(string path, string text, bool overwrite);

        List<FileInfo> ListFilesInContainer();

        bool Save(string file, string saveas);
    }
}
