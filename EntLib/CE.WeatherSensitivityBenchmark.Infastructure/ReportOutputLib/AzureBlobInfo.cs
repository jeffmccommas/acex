﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReportOutputLib
{
    public class AzureBlobInfo
    {
        public string ContentType { get; set; }
        public string BlobLength { get; set; }
        public MemoryStream DataStream { get; set; }
    }
}
