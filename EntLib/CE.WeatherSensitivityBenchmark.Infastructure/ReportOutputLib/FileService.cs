﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReportOutputLib
{
    public class FileService:IFileService
    {
        public string StorageName { get; set; }
        public string StorageKey { get; set; }
        public string ContainerName { get; set; }
        public bool Exists(string path)
        {
            return File.Exists(path);
        }

        public void Delete(string path)
        {
            File.Delete(path);
        }

        public void WriteAllText(string path, string text, bool overwrite)
        {
            File.WriteAllText(path, text);
        }

        public List<FileInfo> ListFilesInContainer()
        {
            throw new NotImplementedException();
        }
        public bool Save(string file, string saveas)
        {
            throw new NotImplementedException();
        }

    }
}
