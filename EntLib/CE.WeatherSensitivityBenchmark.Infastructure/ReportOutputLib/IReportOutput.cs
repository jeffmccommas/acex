﻿using System.Collections.Generic;
using WSWorkflowDataAccess.models;

namespace ReportOutputLib
{
    public interface IReportOutput
    {
        void WriteEmailReportToCSV(List<WsEmailReportModel> emailReportModel, WsForecastModel forecastModel, bool overwrite=false);
        void WritePrintReportToCSV(List<WsPrintReportModel> printReportModel, bool overwrite=false);
        string GenerateSummaryEmail(WsForecastModel forecastModel);
        string EmailReportOutputFile { get; set; }
        string PrintReportOutputFile { get; set; }
      
    }
}
