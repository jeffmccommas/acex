﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReportOutputLib
{
    public class FileInfo: IEquatable<FileInfo>, IComparable<FileInfo>
    {
        public string Name { get; set; }
        public string Path { get; set; }
        public string Directory { get; set; }
        public string CreateDateTime { get; set; }

        public bool Equals(FileInfo other)
        {
            if (other == null) return false;
            return (this.Name.Equals(other.Name));
        }

        public int CompareTo(FileInfo compareFileInfo)
        {
            // A null value means that this object is greater.
            if (compareFileInfo == null)
                return 1;

            else
                return String.Compare(this.CreateDateTime, compareFileInfo.CreateDateTime, StringComparison.Ordinal);
        }
    }
}
