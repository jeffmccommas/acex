﻿namespace AO.DTO
{
    /// <summary>Class for containing security information necessary to access Tableau.</summary>
    public class TableauSecurityToken
    {
        /// <summary>The security token that will allow temporary access to the requested resource.</summary>
        public string Token { get; set; }
    }
}