﻿using System;

namespace AO.DTO
{
    public class LogHistoryRequestDTO
    {
        public string ClientID { get; set; }
        public DateTime EndDateTime { get; set; }
        public string[] Levels { get; set; }
        public string[] Modules { get; set; }
        public DateTime StartDateTime { get; set; }
    }
}