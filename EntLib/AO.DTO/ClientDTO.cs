﻿namespace AO.DTO
{
    public class ClientDTO
    {
        public int ClientID { get; set; }

        public string Name { get; set; }
    }
}