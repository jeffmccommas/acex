﻿using Microsoft.Practices.Unity;
using Microsoft.ServiceBus.Messaging;

namespace AO.EventProcessors
{
    /// <summary>
    /// Class implementing IEventProcessorFactory use for dependency injection.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class EventProcessorFactory<T> : IEventProcessorFactory where T : IEventProcessor
    {
        public IEventProcessor CreateEventProcessor(PartitionContext context)
        {
            var unityContainer = new UnityContainer();            
            new EventProcessorRegistrar().Initialize<TransientLifetimeManager>(unityContainer);
            return unityContainer.Resolve<T>();
        }
    }
}