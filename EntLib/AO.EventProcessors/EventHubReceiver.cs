﻿using System;
using Microsoft.ServiceBus.Messaging;

namespace AO.EventProcessors
{
    /// <summary>
    /// Event Hub events receiver
    /// </summary>
    public class EventHubReceiver
    {
        private readonly string _eventHubName;
        private readonly string _eventHubConnectionString;
        private EventProcessorHost _eventProcessorHost;

        public EventHubReceiver(string eventHubName, string eventHubConnectionString)
        {
            _eventHubConnectionString = eventHubConnectionString;
            _eventHubName = eventHubName;
        }

        /// <summary>
        /// Registers Event Processor for receiving the events
        /// </summary>
        /// <param name="group">Consumer Group Description</param>
        /// <param name="blobConnectionString">Blob Connection string</param>
        /// <param name="hostName">The name of the EventProcessorHost instance</param>
        /// <param name="eventProcessorFactory">EventProcessorFactory instance to register event processor host</param>
        public void RegisterEventProcessor<T>(ConsumerGroupDescription group, string blobConnectionString, string hostName, EventProcessorFactory<T> eventProcessorFactory) where T : IEventProcessor
        {
            var eventHubClient = EventHubClient.CreateFromConnectionString(_eventHubConnectionString, _eventHubName);

            if (null == group)
            {
                //Use default consumer group
                var defaultConsumerGroup = eventHubClient.GetDefaultConsumerGroup();
                _eventProcessorHost = new EventProcessorHost(hostName, eventHubClient.Path,
                    defaultConsumerGroup.GroupName, _eventHubConnectionString, blobConnectionString);
            }
            else
            {
                //Use custom consumer group
                _eventProcessorHost = new EventProcessorHost(hostName, eventHubClient.Path, group.Name, _eventHubConnectionString, blobConnectionString);
            }

            _eventProcessorHost.PartitionManagerOptions = new PartitionManagerOptions
            {
                AcquireInterval = TimeSpan.FromMilliseconds(10), // Default is 10 seconds 
                RenewInterval = TimeSpan.FromSeconds(30), // Default is 10 seconds 
                LeaseInterval = TimeSpan.FromMinutes(1) // Default value is 30 seconds                        
            };

            var eventProcessorOptions = new EventProcessorOptions
            {
                InvokeProcessorAfterReceiveTimeout = true,
                MaxBatchSize = 100,
                PrefetchCount = 100,
                ReceiveTimeOut = TimeSpan.FromSeconds(30)
            };

            _eventProcessorHost.RegisterEventProcessorFactoryAsync(eventProcessorFactory, eventProcessorOptions).Wait();
        }

        /// <summary>
        /// Unregister Event Processor
        /// </summary>
        public void UnregisterEventProcessor()
        {
            _eventProcessorHost.UnregisterEventProcessorAsync().Wait();
        }
    }
}
