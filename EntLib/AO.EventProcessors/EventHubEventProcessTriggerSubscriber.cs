﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using System.Threading.Tasks;
using AO.BusinessContracts;
using CE.AO.Models;
using CE.AO.Utilities;
using Microsoft.ServiceBus.Messaging;
using System.Linq;
using AO.BusinessContracts.EventProcessesContracts;
using Enums = CE.AO.Utilities.Enums;
using CE.AO.Logging;
using Microsoft.Azure;

namespace AO.EventProcessors
{
    public class EventHubEventProcessTriggerSubscriber : ITriggerSubscriber, IEventProcessor
    {
        private EventHubReceiver _eventHubReceiver;
        private PartitionContext _partitionContext;
        private Lazy<EventProcessorFactory<EventHubEventProcessTriggerSubscriber>> EventProcessorFactory { get; }
        public const string EventProcessEventHubName = "ImportDataEventHubName";
        public const string ConsumerGroupNameForLoggingEventHub = "ConsumerGroupNameForImportDataEventHub";
        public const string AclaraOneStorageConnectionString = "AclaraOneStorageConnectionString";
        public const string NumberOfPartitionsForLoggingEventHub = "NumberOfPartitionsForImportDataEventHub";
        public const string EventProcessEventHubManageConnectionString = "ImportDataEventHubManageConnectionString";
        private LogModel LogModel { get; }
        private Lazy<IAzureQueue> AzureQueue { get; }
        private IProcessEvent[] EventProcessors { get; }

        public EventHubEventProcessTriggerSubscriber(LogModel logModel, Lazy<EventProcessorFactory<EventHubEventProcessTriggerSubscriber>> eventProcessorFactory,
           IProcessEvent[] eventProcessors, Lazy<IAzureQueue> azureQueue)
        {
            EventProcessorFactory = eventProcessorFactory;
            EventProcessors = eventProcessors;
            AzureQueue = azureQueue;
            LogModel = logModel;
        }


        /// <summary>
        /// Subscribes to event hub trigerring mechanism for doing EventProcess
        /// </summary>
        public void Subscribe()
        {
            //Event Hub
            //Get settings from configuration
            var importDataEventHubName = CloudConfigurationManager.GetSetting(EventProcessEventHubName); //eventHubName
            var consumerGroupNameForImportDataEventHub = CloudConfigurationManager.GetSetting(ConsumerGroupNameForLoggingEventHub);
            var numberOfPartitionsForImportDataEventHub = int.Parse(CloudConfigurationManager.GetSetting(NumberOfPartitionsForLoggingEventHub));
            var aclaraOneStorageConnectionString = CloudConfigurationManager.GetSetting(AclaraOneStorageConnectionString); // Required for checkpoint/state

            //Get AMQP connection string
            var connectionString = EventHubManager.GetServiceBusConnectionString(EventProcessEventHubManageConnectionString);

            //Create event hub if it does not exist
            var namespaceManager = EventHubManager.GetNamespaceManager(connectionString);
            EventHubManager.CreateEventHubIfNotExists(importDataEventHubName, numberOfPartitionsForImportDataEventHub, namespaceManager);

            //Create consumer group if it does not exist
            var group = string.IsNullOrWhiteSpace(consumerGroupNameForImportDataEventHub) ? null : namespaceManager.CreateConsumerGroupIfNotExists(importDataEventHubName, consumerGroupNameForImportDataEventHub);

            //Start processing messages
            _eventHubReceiver = new EventHubReceiver(importDataEventHubName, connectionString);

            //Get host name of worker role instance.  This is used for each environment to obtain
            //a lease, and to renew the same lease in case of a restart.
            _eventHubReceiver.RegisterEventProcessor(group, aclaraOneStorageConnectionString, Guid.NewGuid().ToString(), EventProcessorFactory.Value);
        }

        /// <summary>
        /// Unsubscribes from event hub trigerring mechanism for doing Event Process
        /// </summary>
        public void UnSubscribe()
        {
            //Unregister the event processor so other instances will handle the partitions
            _eventHubReceiver.UnregisterEventProcessor();
        }

        public Task OpenAsync(PartitionContext context)
        {
            Trace.TraceInformation(
                $"EventProcessor OpenAsync.  Partition: '{context.Lease.PartitionId}', Offset: '{context.Lease.Offset}'");
            _partitionContext = context;
            return Task.FromResult<object>(null);
        }

        /// <summary>
        /// To process the event in the event hub
        /// </summary>
        /// <param name="context">Context of the partition</param>
        /// <param name="events">List of EventData present in event hub</param>
        /// <returns>Void</returns>
        public async Task ProcessEventsAsync(PartitionContext context, IEnumerable<EventData> events)
        {
            try
            {
                foreach (var eventData in events)
                {
                    EventHubTriggerSubscriberHelper.CleanAllValues(LogModel);

                    try
                    {
                        KeyValuePair<string, object> eventType = eventData.Properties.FirstOrDefault(p => p.Key == "EventType");
                        KeyValuePair<string, object> intervalType = eventData.Properties.FirstOrDefault(p => p.Key == "IntervalType");
                        KeyValuePair<string, object> clientId = eventData.Properties.FirstOrDefault(p => p.Key == "ClientId");
                        KeyValuePair<string, object> source = eventData.Properties.FirstOrDefault(p => p.Key == "Source");
                        KeyValuePair<string, object> processingType = eventData.Properties.FirstOrDefault(p => p.Key == "ProcessingType");
                        KeyValuePair<string, object> rowIndex = eventData.Properties.FirstOrDefault(p => p.Key == "RowIndex");

                        LogModel.ClientId = Convert.ToString(clientId.Value);
                        LogModel.Source = Convert.ToString(source.Value);
                        LogModel.ProcessingType = processingType.Value != null
                            ? (Enums.ProcessingType)
                                Enum.Parse(typeof(Enums.ProcessingType), Convert.ToString(processingType.Value))
                            : Enums.ProcessingType.None;
                        var data = Encoding.UTF8.GetString(eventData.GetBytes());
                        LogModel.Metadata = data;
                        LogModel.RowIndex = Convert.ToString(rowIndex.Value);
                        LogModel.Module = Enums.Module.EventProcessing;

                        KeyValuePair<string, object> lastEventOfFile = eventData.Properties.FirstOrDefault(p => p.Key == "LastEventOfFile");
                        KeyValuePair<string, object> firstEventOfFile = eventData.Properties.FirstOrDefault(p => p.Key == "FirstEventOfFile");

                        if (firstEventOfFile.Value != null)
                        {
                            Logger.Info($@"Event Processing of file ""{LogModel.Source}"" started.", LogModel);
                        }
                        else if (lastEventOfFile.Value != null)
                        {
                            Logger.Info($@"Event Processing of file ""{LogModel.Source}"" finished.", LogModel);
                            var eventTypeEnum = (Enums.EventType)Enum.Parse(typeof(Enums.EventType), Convert.ToString(eventType.Value));
                            if (eventTypeEnum == Enums.EventType.NccAmiReading)
                            {
                                KeyValuePair<string, object> blobContainer = eventData.Properties.FirstOrDefault(p => p.Key == "BlobContainer");
                                var message = $"NccCalcConsumption{source.Value}:{blobContainer.Value}:{clientId.Value}";
                                AzureQueue.Value.AddMessageToBatchQueue(message).Wait();
                            }
                        }
                        else if (eventType.Value != null)
                        {
                            var eventTypeEnum = (Enums.EventType)Enum.Parse(typeof(Enums.EventType), Convert.ToString(eventType.Value));
                            IProcessEvent processEvent = EventProcessors.SingleOrDefault(p => p.EventType == eventTypeEnum);

                            if (processEvent != null)
                            {
                                LogModel.Module = processEvent.Module;
                                await processEvent.Process(eventData, clientId.Value.ToString());
                            }
                            else
                            {
                                var intervalText = intervalType.Value != null
                                    ? $@" and Interval Type ""{intervalType.Value}"""
                                    : "";
                                Logger.Fatal($@"Event Type ""{eventType.Value}""{intervalText} is not supported for event process.", LogModel);
                            }
                        }
                        else
                        {
                            Logger.Fatal("Event Type is not specified.", LogModel);
                        }
                    }
                    catch (Exception ex)
                    {
                        Logger.Error(ex, LogModel);
                    }
                }
                await context.CheckpointAsync().ConfigureAwait(false);
            }
            catch (Exception ex)
            {
                Trace.TraceError("Error in processing: " + ex.Message);
            }
        }

        public async Task CloseAsync(PartitionContext context, CloseReason reason)
        {
            Trace.TraceWarning(
                $"EventProcessor CloseAsync.  Partition '{_partitionContext.Lease.PartitionId}', Reason: '{reason}'.");
            if (reason == CloseReason.Shutdown)
            {
                await context.CheckpointAsync().ConfigureAwait(false);
            }
        }
    }
}
