﻿using AO.Business;
using AO.DataAccess;
using AO.Entities;
using Cassandra.Mapping;
using Microsoft.Azure;
using Microsoft.Practices.Unity;
using StaticConfig = AO.DataAccess.StaticConfig;

namespace AO.EventProcessors
{
    /// <summary>
    /// Dependency Injection Registrar for Event Processors
    /// </summary>
    public class EventProcessorRegistrar
    {
        private static bool CassandraSessionInitialized { get; set; }

        /// <summary>
        /// Will Initialize dependency registrations and return a constructed IUnityContainer.
        /// </summary>
        /// <typeparam name="TLifetime"></typeparam>
        public IUnityContainer Initialize<TLifetime>() where TLifetime : LifetimeManager, new()
        {
            var container = new UnityContainer();
            Initialize<TLifetime>(container);
            return container;
        }

        /// <summary>
        /// Initialize DI registrations.
        /// </summary>
        /// <typeparam name="TLifetime"></typeparam>
        /// <param name="container"></param>
        public void Initialize<TLifetime>(IUnityContainer container) where TLifetime : LifetimeManager, new()
        {
            var storageConnectionSting = CloudConfigurationManager.GetSetting(StaticConfig.AzureStorageConnectionString);

            AutoMapperConfig.AutoMapperMappings();

            //VEE.Business.AutoMapperConfig.AutoMapperMappings();

            //creates azure queues if does not exist                
            if (!string.IsNullOrWhiteSpace(storageConnectionSting))
                CE.AO.Entities.Init.AzureQueueInit().Wait();

            InitializeCassandraMappings();

            InitializeCassandraSession();

            RegisterTypesForUnity.RegisterTypes<TLifetime>(container);
        }

        /// <summary>
        /// Make sure Cassandra Mappings are only initiated once per App Domain.
        /// </summary>
        private void InitializeCassandraMappings()
        {
            if (!CassandraMapperConfig.IsInitialized)
            {
                MappingConfiguration.Global.Define<CassandraMapperConfig>();
            }
        }

        /// <summary>
        /// Make sure Cassandra Session is created once per App Domain.
        /// </summary>
        private void InitializeCassandraSession()
        {
            if (CassandraSessionInitialized) return;
            var nodesValues = System.Configuration.ConfigurationManager.AppSettings.Get(StaticConfig.CassandraNodes);
            if (nodesValues != null)
                CassandraSession.GetSession();
            CassandraSessionInitialized = true;
        }
    }
}