﻿using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using CE.AO.Models;

namespace AO.EventProcessors
{
    static class EventHubTriggerSubscriberHelper
    {
        /// <summary>
        /// Get count of null columns
        /// </summary>
        /// <typeparam name="T">dyanamic type</typeparam>
        /// <param name="model">dyanamic model to be checked</param>
        internal static void CleanAllValues<T>(T model) where T : LogModel
        {
            List<PropertyInfo> properties = model.GetType().GetProperties().ToList();
            properties
                .ForEach(property => property.SetValue(model, null));
        }
    }
}
