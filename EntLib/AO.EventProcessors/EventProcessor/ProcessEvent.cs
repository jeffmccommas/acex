﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using AO.BusinessContracts.EventProcessesContracts;
using CE.AO.Models;
using Microsoft.ServiceBus.Messaging;

namespace AO.EventProcessors.EventProcessor
{
    /// <summary>
    /// ProcessEvent class is used to process the the events from event hub
    /// </summary>
    public abstract class ProcessEvent : IProcessEvent
    {
        protected ProcessEvent(LogModel logModel)
        {
            LogModel = logModel;
        }
        public LogModel LogModel { get; set; }
        public string ClientId { get; set; }

        public abstract Task<bool> Process(EventData eventData, string clientId);

        public abstract CE.AO.Utilities.Enums.Module Module { get; }

        /// <summary>
        /// The EventType this processor is responsible for processing.
        /// </summary>
        public abstract CE.AO.Utilities.Enums.EventType EventType { get; }

        /// <summary>
        /// Get count of null columns
        /// </summary>
        /// <typeparam name="T">dyanamic type</typeparam>
        /// <param name="model">dyanamic model to be checked</param>
        /// <param name="filterColumnNames">column names to be checked for null</param>
        /// <returns>count of column which have null value</returns>
        public static int GetCountOfNullColumns<T>(T model, string filterColumnNames)
        {
            List<PropertyInfo> properties = string.IsNullOrWhiteSpace(filterColumnNames)
                ? model.GetType().GetProperties().ToList()
                : model.GetType().GetProperties().Where(p => p.Name.Contains(filterColumnNames)).ToList();

            var nullColumns = 0;
            properties
                .ForEach(property => { nullColumns = nullColumns + (property.GetValue(model) == null ? 1 : 0); });
            return nullColumns;
        }

        /// <summary>
        /// Get count of not null columns
        /// </summary>
        /// <typeparam name="T">dyanamic type</typeparam>
        /// <param name="model">dyanamic model to be checked</param>
        /// <param name="filterColumnNames">column names to be checked for not null</param>
        /// <returns>count of column which have value</returns>
        public static int GetCountOfNotNullColumns<T>(T model, string filterColumnNames)
        {
            List<PropertyInfo> properties = string.IsNullOrWhiteSpace(filterColumnNames)
                ? model.GetType().GetProperties().ToList()
                : model.GetType().GetProperties().Where(p => p.Name.Contains(filterColumnNames)).ToList();

            var notNullColumns = 0;
            properties
                .ForEach(property => { notNullColumns = notNullColumns + (property.GetValue(model) != null ? 1 : 0); });
            return notNullColumns;
        }

        /// <summary>
        /// validate the model
        /// </summary>
        /// <typeparam name="T">dynamic type</typeparam>
        /// <param name="model">model to be validated</param>
        /// <param name="validationResult">gives list of validation results if any</param>
        /// <returns>bool</returns>
        public virtual bool Validate<T>(T model, out List<ValidationResult> validationResult)
        {
            return Utilities.Validate(model, out validationResult);
        }

        /// <summary>
        /// create service contract id if not present in file
        /// </summary>
        /// <param name="meterId">current meterId to create service contract id</param>
        /// <param name="accountId">current accountId to create service contract id</param>
        /// <param name="premiseId">current premiseId to create service contract id</param>
        /// <param name="commodityId">current commodityId to create service contract id</param>
        /// <returns>ServiceContractId</returns>
        public static string CreateServiceContractId(string meterId, string accountId, string premiseId, int commodityId)
        {
            return !string.IsNullOrWhiteSpace(meterId)
                ? $"{accountId}_{premiseId}_{commodityId}_{meterId}"
                : $"{accountId}_{premiseId}_{commodityId}";
        }
    }
}
