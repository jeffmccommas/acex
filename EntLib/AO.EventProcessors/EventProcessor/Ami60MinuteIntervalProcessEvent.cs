﻿using Microsoft.ServiceBus.Messaging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AO.BusinessContracts;
using AO.BusinessContracts.EventProcessesContracts;
using AO.BusinessContracts.Preprocessing;
using AutoMapper;
using CE.AO.Models.RawFileModels;
using CE.AO.Logging;
using CE.AO.Models;
using Enums = CE.AO.Utilities.Enums;

namespace AO.EventProcessors.EventProcessor
{
    /// <summary>
    /// Ami60MinuteIntervalProcessEvent class is used to process the event for ami 60 min interval event
    /// </summary>
    public class Ami60MinuteIntervalProcessEvent : ProcessEvent, IAmiProcessEvent
    {

        public Enums.IntervalType IntervalType { get; } = Enums.IntervalType.Sixty;

        public override Enums.Module Module { get; } = Enums.Module.AMIEventProcess;

        public override Enums.EventType EventType { get; } = Enums.EventType.Ami;

        private Lazy<IClientConfigFacade> ClientConfigFacade { get; }
        private Lazy<ITallAMI> TallAmi { get; }
        private Lazy<IAzureQueue> AzureQueue { get; }
        private Lazy<IAmiProcessEventHelper> BaseAmiProcess { get; }

        public Ami60MinuteIntervalProcessEvent(LogModel logModel, Lazy<IClientConfigFacade> clientConfigFacade,
            Lazy<ITallAMI> tallAmi, Lazy<IAzureQueue> azureQueue, Lazy<IAmiProcessEventHelper> baseAmiProcess) : base(logModel)
        {
            ClientConfigFacade = clientConfigFacade;
            TallAmi = tallAmi;
            AzureQueue = azureQueue;
            BaseAmiProcess = baseAmiProcess;
        }

        /// <summary>
        /// To process the event which is related to the ami 60 min interval event
        /// </summary>
        /// <param name="eventData">Event data to be processed</param>
        /// <param name="clientId"></param>
        /// <returns>Boolean result</returns>
        public override async Task<bool> Process(EventData eventData, string clientId)
        {
            ClientId = clientId;

            var data = Encoding.UTF8.GetString(eventData.GetBytes());
            var rawModel = JsonConvert.DeserializeObject<RawAmi60MinuteIntervalModel>(data);
            LogModel.AccountId = rawModel.AccountNumber;
            LogModel.MeterId = rawModel.MeterId;
            LogModel.ClientId = clientId;

            //Map Raw Model to Model
            var model = Mapper.Map<RawAmi60MinuteIntervalModel, Ami60MinuteIntervalModel>(rawModel);
            model.IntervalType = Convert.ToInt32(Enums.IntervalType.Sixty);

            //Validate Model
            List<ValidationResult> validationResults;
            if (Validate(model, out validationResults))
            {
                return await ShiftAndInsert(model);
            }
            var errorString = string.Empty;
            if (validationResults.Count > 0)
                errorString = $"Error: {JsonConvert.SerializeObject(validationResults)}.";
            Logger.Fatal($@"Event of type ""AMI - 60 min"" is not valid.{errorString}", LogModel);

            return false;
        }

        /// <summary>
        /// To insert the Ami60MinuteIntervalModel model after changing the timezone 
        /// </summary>
        /// <param name="ami60MinuteIntervalModel">Ami60MinuteIntervalModel model values to be shifted and inserted to the database</param>
        /// <returns>Boolean result</returns>
        private async Task<bool> ShiftAndInsert(Ami60MinuteIntervalModel ami60MinuteIntervalModel)
        {
            var clientSettings = ClientConfigFacade.Value.GetClientSettings(ami60MinuteIntervalModel.ClientId);
            if (clientSettings == null)
            {
                Logger.Fatal(
                    "Client Settings are not configured for this client. Cannot proceed with futher ingestion.",
                    LogModel);
                return false;
            }

            if (string.IsNullOrWhiteSpace(clientSettings.TimeZone))
            {
                Logger.Fatal(
                    "Client Settings for TimeZone is not configured for this client. Cannot proceed with futher ingestion.",
                    LogModel);
                return false;
            }

            var isIntervalEnd = !clientSettings.IsAmiIntervalStart; 
            var readingTimezoneString = clientSettings.TimeZone; 
            if (!string.IsNullOrWhiteSpace(ami60MinuteIntervalModel.Timezone))
            {
                readingTimezoneString = ami60MinuteIntervalModel.Timezone;
            }
            var readingTimezoneEnum =
                (Enums.Timezone)Enum.Parse(typeof(Enums.Timezone), Convert.ToString(readingTimezoneString));
            var readingTimezone = TimeZoneInfo.FindSystemTimeZoneById(readingTimezoneEnum.GetDescription());
            var defaultTimezoneString = clientSettings.TimeZone; //Set from Settings
            var defaultTimezoneEnum =
                (Enums.Timezone)Enum.Parse(typeof(Enums.Timezone), Convert.ToString(defaultTimezoneString));
            var defaultTimeZone = TimeZoneInfo.FindSystemTimeZoneById(defaultTimezoneEnum.GetDescription());
            var clientHandlesDst = clientSettings.IsDstHandled; //Set from Settings

            ami60MinuteIntervalModel.Timezone = readingTimezoneString;
            var refAmi60MinuteIntervalModel =
                Mapper.Map<Ami60MinuteIntervalModel, Ami60MinuteIntervalModel>(ami60MinuteIntervalModel);

            var amiDateTime = refAmi60MinuteIntervalModel.AmiTimeStamp;

            if (readingTimezoneEnum == Enums.Timezone.UTC || readingTimezoneEnum == Enums.Timezone.GMT)
                amiDateTime = TimeZoneInfo.ConvertTimeFromUtc(refAmi60MinuteIntervalModel.AmiTimeStamp, defaultTimeZone);

            ami60MinuteIntervalModel.AmiTimeStamp = amiDateTime;

            BaseAmiProcess.Value.CleanAllIntValues(ami60MinuteIntervalModel, null);

            var tallAmiList = new List<TallAmiModel>();

            //Convert to Readings
            IEnumerable<AmiReading> amiReadingsList = BaseAmiProcess.Value.GetReadings(refAmi60MinuteIntervalModel);
            foreach (var amiReading in amiReadingsList)
            {
                BaseAmiProcess.Value.SetValueFromReading(amiReading, isIntervalEnd, clientHandlesDst, 60, readingTimezoneEnum,
                    readingTimezone, readingTimezoneString, defaultTimeZone, defaultTimezoneString,
                    ami60MinuteIntervalModel, tallAmiList);
            }

            var todaysDateInReadingTimeZone = TimeZoneInfo.ConvertTimeFromUtc(DateTime.UtcNow, readingTimezone);
            var tasks = new List<Task<bool>>();
            if (tallAmiList.Count > 0)
            {
                IEnumerable<DateTime> distinctPartitions = tallAmiList.Select(d => d.AmiTimeStamp.Date).Distinct();
                tasks.AddRange(
                    distinctPartitions.Select(
                        partition =>
                           TallAmi.Value.InsertOrMergeBatchAsync(
                                tallAmiList.Where(d => d.AmiTimeStamp.Date == partition).ToList())));
            }

            var taskResults = await Task.WhenAll(tasks);
            if (!LogModel.Source.ToLower().Contains("batch"))
            {
                var queueMessage =
                    $"{ami60MinuteIntervalModel.ClientId}^^{ami60MinuteIntervalModel.MeterId}^^{todaysDateInReadingTimeZone.Date.ToString("MM/dd/yyyy")}^^{LogModel.Source}^^{LogModel.RowIndex}^^{LogModel.Metadata}^^{Convert.ToInt32(LogModel.ProcessingType)}";
                AzureQueue.Value.AddMessageToCalculationQueue(queueMessage).Wait();
            }
            return !taskResults.Any(result => false);
        }

        /// <summary>
        /// Validate the data in the Ami60MinuteIntervalModel
        /// </summary>
        /// <param name="model">The Ami60MinuteIntervalModel to validate</param>
        /// <param name="validationResuts">The validation results messages</param>
        /// <returns>True, if valid.</returns>
        private bool Validate(Ami60MinuteIntervalModel model, out List<ValidationResult> validationResuts)
        {
            // Check against the base
            if (!base.Validate(model, out validationResuts))
            {
                return false;
            }

            // Check the ClientId
            if (model.ClientId != Convert.ToInt32(ClientId))
            {
                validationResuts.Add(new ValidationResult("ClientId is not valid", new List<string> { "ClientId" }));
                return false;
            }

            // Check the CommodityType
            if (!Enum.IsDefined(typeof(Enums.CommodityType), model.CommodityId))
            {
                validationResuts.Add(new ValidationResult("CommodityId is not valid.", new List<string> { "CommodityId" }));
                return false;
            }

            if (model.CommodityId != Convert.ToInt32(Enums.CommodityType.Gas) && model.VolumeFactor != null)
            {
                validationResuts.Add(new ValidationResult("VolumeFactor can be entered only for Gas Meters.",
                    new List<string> { "VolumeFactor" }));
                return false;

            }

            if (model.CommodityId != Convert.ToInt32(Enums.CommodityType.Electric) && model.Direction != null)
            {
                validationResuts.Add(new ValidationResult("Direction can be entered only for Electric Meters.",
                    new List<string> { "Direction" }));
                return false;
            }

            // Check the UOM
            if (!Enum.IsDefined(typeof(Enums.UomType), model.UOMId))
            {
                validationResuts.Add(new ValidationResult("UOMId is not valid.", new List<string> { "UOMId" }));
                return false;
            }

            // If all checks pass return true.
            return true;
        }
    }
}
