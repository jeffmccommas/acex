﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AO.BusinessContracts;
using AO.BusinessContracts.EventProcessesContracts;
using AutoMapper;
using CE.AO.Logging;
using CE.AO.Models;
using CE.AO.Models.RawFileModels;
using Microsoft.ServiceBus.Messaging;
using Newtonsoft.Json;
using Enums = CE.AO.Utilities.Enums;

namespace AO.EventProcessors.EventProcessor
{
    /// <summary>
    /// NccAmiConsumptionProcessEvent class is used to process the event for NccAmiConsumption event
    /// </summary>
    public class NccAmiConsumptionProcessEvent : ProcessEvent
    {
        public override Enums.Module Module { get; } = Enums.Module.NccConsumptionEventProcess;
        public override Enums.EventType EventType { get; } = Enums.EventType.NccAmiConsumption;

        private Lazy<IAmiReading> AmiReading { get; set; }
        private Lazy<IClientConfigFacade> ClientConfigFacade { get; set; }
        private Lazy<ITallAMI> TallAmi { get; set; }
        private Lazy<IAmiProcessEventHelper> BaseAmiProcess { get; set; }

        public NccAmiConsumptionProcessEvent(LogModel logModel, Lazy<IAmiReading> amiReading, Lazy<IClientConfigFacade> clientConfigFacade,
            Lazy<ITallAMI> tallAmi, Lazy<IAmiProcessEventHelper> baseAmiProcess) : base(logModel)
        {
            AmiReading = amiReading;
            ClientConfigFacade = clientConfigFacade;
            TallAmi = tallAmi;
            BaseAmiProcess = baseAmiProcess;
        }

        /// <summary>
        /// To process the event which is related to the NccAmiConsumption event
        /// </summary>
        /// <param name="eventData">Event data to be processed</param>
        /// <param name="clientId">Unique Client Identifier</param>
        /// <returns>Boolean result</returns>
        public override async Task<bool> Process(EventData eventData, string clientId)
        {
            ClientId = clientId;

            var data = Encoding.UTF8.GetString(eventData.GetBytes());
            var rawModel = JsonConvert.DeserializeObject<RawNccAmiReadingModel>(data);

            LogModel.MeterId = rawModel.METER_ID;
            LogModel.CustomerId = rawModel.CUSTOMER_ID;
            LogModel.ClientId = clientId;

            var validationResults = new List<ValidationResult>();
            var uomIsValid = true;
            try
            {
                var rawModelUomId = (int)Enums.GetValueFromDescription<Enums.UomType>(rawModel.UNIT_OF_MEASURE);
                rawModel.UNIT_OF_MEASURE = rawModelUomId.ToString();

                // Check the UOM
                if (!Enum.IsDefined(typeof(Enums.UomType), rawModelUomId))
                {
                    uomIsValid = false;
                }
            }
            catch (Exception)
            {
                uomIsValid = false;
            }

            if (!uomIsValid)
            {
                validationResults.Add(new ValidationResult("UNIT_OF_MEASURE is not valid.",
                    new List<string> { "UNIT_OF_MEASURE" }));
            }

            if (validationResults.Count == 0)
            {
                //Map Raw Model to Model
                var model = Mapper.Map<RawNccAmiReadingModel, NccAmiReadingModel>(rawModel);
                model.ClientId = Convert.ToInt32(ClientId);

                //Validate Model
                if (Validate(model, out validationResults))
                {
                    var previousNccAmiReadingModel = await AmiReading.Value.GetAmiReading(model.ClientId, model.MeterId, model.AmiTimeStamp.AddHours(-1));
                    if (previousNccAmiReadingModel != null)
                    {
                        var amiReading = Mapper.Map<NccAmiReadingModel, BusinessContracts.Preprocessing.AmiReading>(model);
                        amiReading.Consumption = model.ReadingValue > previousNccAmiReadingModel.ReadingValue
                            ? model.ReadingValue - previousNccAmiReadingModel.ReadingValue
                            : 0;
                        amiReading.AmiTimeStamp = amiReading.AmiTimeStamp.AddHours(-1);
                        return await ShiftAndInsert(amiReading);
                    }

                    return true;
                }
            }
            return false;
        }

        private async Task<bool> ShiftAndInsert(BusinessContracts.Preprocessing.AmiReading amiReading)
        {
            var clientSettings = ClientConfigFacade.Value.GetClientSettings(amiReading.ClientId);
            if (clientSettings == null)
            {
                Logger.Fatal("Client Settings are not configured for this client. Cannot proceed with futher ingestion.", LogModel);
                return false;
            }

            if (string.IsNullOrWhiteSpace(clientSettings.TimeZone))
            {
                Logger.Fatal("Client Settings for TimeZone is not configured for this client. Cannot proceed with futher ingestion.", LogModel);
                return false;
            }

            var isIntervalEnd = !clientSettings.IsAmiIntervalStart;
            var readingTimezoneString = clientSettings.TimeZone;
            if (!string.IsNullOrWhiteSpace(amiReading.Timezone))
            {
                readingTimezoneString = amiReading.Timezone;
            }
            var readingTimezoneEnum = (Enums.Timezone)Enum.Parse(typeof(Enums.Timezone), Convert.ToString(readingTimezoneString));
            var readingTimezone = TimeZoneInfo.FindSystemTimeZoneById(readingTimezoneEnum.GetDescription());
            var defaultTimezoneString = clientSettings.TimeZone; //Set from Settings
            var defaultTimezoneEnum = (Enums.Timezone)Enum.Parse(typeof(Enums.Timezone), Convert.ToString(defaultTimezoneString));
            var defaultTimeZone = TimeZoneInfo.FindSystemTimeZoneById(defaultTimezoneEnum.GetDescription());
            var clientHandlesDst = clientSettings.IsDstHandled; //Set from Settings

            amiReading.Timezone = readingTimezoneString;
            amiReading.IntervalType = 60;

            var refAmi60MinuteIntervalModel =
                Mapper.Map<BusinessContracts.Preprocessing.AmiReading, Ami60MinuteIntervalModel>(amiReading);
            refAmi60MinuteIntervalModel.AmiTimeStamp = amiReading.AmiTimeStamp.Date;
            refAmi60MinuteIntervalModel.IntervalType = 60;

            var amiDateTime = refAmi60MinuteIntervalModel.AmiTimeStamp;

            if (readingTimezoneEnum == Enums.Timezone.UTC || readingTimezoneEnum == Enums.Timezone.GMT)
                amiDateTime = TimeZoneInfo.ConvertTimeFromUtc(refAmi60MinuteIntervalModel.AmiTimeStamp, defaultTimeZone);

            var ami60MinuteIntervalModel =
                Mapper.Map<Ami60MinuteIntervalModel, Ami60MinuteIntervalModel>(refAmi60MinuteIntervalModel);
            ami60MinuteIntervalModel.AmiTimeStamp = amiDateTime;

            BaseAmiProcess.Value.CleanAllIntValues(ami60MinuteIntervalModel, null);

            var tallAmiList = new List<TallAmiModel>();

            BaseAmiProcess.Value.SetValueFromReading(amiReading, isIntervalEnd, clientHandlesDst, 60, readingTimezoneEnum,
                readingTimezone, readingTimezoneString, defaultTimeZone, defaultTimezoneString, ami60MinuteIntervalModel, tallAmiList, true);

            var tasks = new List<Task<bool>>();
            if (tallAmiList.Count > 0)
            {
                var distinctPartitions = tallAmiList.Select(d => d.AmiTimeStamp.Date).Distinct();
                tasks.AddRange(distinctPartitions.Select(partition => TallAmi.Value.InsertOrMergeBatchAsync(tallAmiList.Where(d => d.AmiTimeStamp.Date == partition).ToList())));
            }

            bool[] taskResults = await Task.WhenAll(tasks);
            return !taskResults.Any(result => false);
        }

        /// <summary>
        /// To insert the NccAmiReadingModel model after changing the timezone 
        /// </summary>
        /// <returns>Boolean result</returns>
        private bool Validate(NccAmiReadingModel model, out List<ValidationResult> validationResuts)
        {
            // Check against the base
            if (!base.Validate(model, out validationResuts))
            {
                return false;
            }

            //Check the ClientId
            if (model.ClientId != Convert.ToInt32(ClientId))
            {
                validationResuts.Add(new ValidationResult("ClientId is not valid", new List<string> { "ClientId" }));
                return false;
            }

            // Check the CommodityType
            if (!Enum.IsDefined(typeof(Enums.CommodityType), model.CommodityId))
            {
                validationResuts.Add(new ValidationResult("CommodityId is not valid.", new List<string> { "CommodityId" }));
                return false;
            }

            return true;
        }
    }
}
