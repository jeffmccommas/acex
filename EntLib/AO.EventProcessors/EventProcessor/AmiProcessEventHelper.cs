﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using AO.BusinessContracts;
using AO.BusinessContracts.EventProcessesContracts;
using AutoMapper;
using CE.AO.Models;
using Enums = CE.AO.Utilities.Enums;

namespace AO.EventProcessors.EventProcessor
{
    /// <summary>
    /// All Ami related functions
    /// </summary>
    public class AmiProcessEventHelper : IAmiProcessEventHelper
    {
        private Lazy<IAmiReading> AmiReading { get; }

        public AmiProcessEventHelper(Lazy<IAmiReading> amiReading)
        {
            AmiReading = amiReading;
        }

        /// <summary>
        /// set the value from reading
        /// </summary>
        /// <param name="amiReading">amireading of the data to be fetched</param>
        /// <param name="isIntervalEnd">cehck isIntervalEnd of the data to be fetched</param>
        /// <param name="clientHandlesDst">clientHandlesDst of the data to be fetched</param>
        /// <param name="intervalValue">intervalValue of the data to be fetched</param>
        /// <param name="readingTimezoneEnum">readingTimezoneEnum of the data to be fetched</param>
        /// <param name="readingTimezone">readingTimezone of the data to be fetched</param>
        /// <param name="readingTimezoneString">readingTimezoneString of the data to be fetched</param>
        /// <param name="defaultTimeZone">defaultTimeZone of the data to be fetched</param>
        /// <param name="defaultTimezoneString">defaultTimezoneString of the data to be fetched</param>
        /// <param name="amiModel">amiModel of the data to be fetched</param>
        /// <param name="tallAmiList">tallAmiList of the data to be fetched</param>
        /// <param name="isNccReading">check isNccReading of the data to be fetched</param>
        /// <returns>bool</returns>        
        public bool SetValueFromReading(BusinessContracts.Preprocessing.AmiReading amiReading, bool isIntervalEnd, bool clientHandlesDst, int intervalValue,
            Enums.Timezone readingTimezoneEnum, TimeZoneInfo readingTimezone, string readingTimezoneString, TimeZoneInfo defaultTimeZone,
            string defaultTimezoneString, dynamic amiModel, List<TallAmiModel> tallAmiList, bool isNccReading = false)
        {
            if (amiModel.GetType() != typeof(Ami15MinuteIntervalModel) && amiModel.GetType() != typeof(Ami30MinuteIntervalModel)
                && amiModel.GetType() != typeof(Ami60MinuteIntervalModel) && amiModel.GetType() != typeof(AmiDailyIntervalModel))
            {
                throw new Exception($"{amiModel.GetType()} model is not supported.");
            }

            if (isIntervalEnd)
                amiReading.AmiTimeStamp = amiReading.AmiTimeStamp.AddMinutes(-1 * intervalValue);

            var dstStartDateTime = DateTime.MinValue;
            var dstEndDateTime = DateTime.MinValue;

            if (readingTimezoneEnum != Enums.Timezone.UTC && readingTimezoneEnum != Enums.Timezone.GMT)
            {
                var timezoneSupportsDaylightSavingTime = readingTimezone.SupportsDaylightSavingTime;
                if (timezoneSupportsDaylightSavingTime)
                {
                    GetDstStartEndDateTime(readingTimezone, out dstStartDateTime, out dstEndDateTime,
                        amiReading.AmiTimeStamp);
                }

                if (timezoneSupportsDaylightSavingTime && IsShiftNeeded(amiReading.AmiTimeStamp, clientHandlesDst,
                dstStartDateTime, dstEndDateTime))
                {
                    amiReading.AmiTimeStamp = amiReading.AmiTimeStamp.AddHours(1);

                    if (IsSumNeeded(amiReading.AmiTimeStamp, dstEndDateTime))
                    {
                        var readingMinusHour = amiReading.AmiTimeStamp.AddHours(-1);
                        var readingMinusHourPropertyName =
                            $"IntValue{Convert.ToString(readingMinusHour.Hour).PadLeft(2, '0')}{Convert.ToString(readingMinusHour.Minute).PadLeft(2, '0')}";
                        var property =
                            ((PropertyInfo[])amiModel.GetType()
                                .GetProperties())
                                .FirstOrDefault(p => p.Name == readingMinusHourPropertyName);
                        double consumption;
                        if (isNccReading)
                        {
                            //Get NCC Readings, calculate usage and then add usage
                            consumption = CalculateConsumptionFromReading(amiReading.ClientId, amiReading.MeterId,
                                amiReading.AmiTimeStamp);
                        }
                        else
                        {
                            consumption =
                                Convert.ToDouble(property != null ? (property.GetValue(amiModel) ?? 0) : 0);
                        }

                        consumption = consumption + (amiReading.Consumption ?? 0);
                        property?.SetValue(amiModel, consumption);
                        amiReading.AmiTimeStamp = amiReading.AmiTimeStamp.AddHours(-1);
                        amiReading.Consumption = consumption;
                    }
                }

                if (!readingTimezone.IsInvalidTime(amiReading.AmiTimeStamp))
                {
                    //Convert to UTC and Insert into Tall format
                    var tallAmiModel = Mapper.Map<BusinessContracts.Preprocessing.AmiReading, TallAmiModel>(amiReading);
                    tallAmiModel.AmiTimeStamp =
                        TimeZoneInfo.ConvertTime(
                            new DateTime(amiReading.AmiTimeStamp.Year, amiReading.AmiTimeStamp.Month,
                                amiReading.AmiTimeStamp.Day, amiReading.AmiTimeStamp.Hour,
                                amiReading.AmiTimeStamp.Minute, amiReading.AmiTimeStamp.Second), readingTimezone,
                            TimeZoneInfo.Utc);
                    tallAmiModel.Timezone = readingTimezoneString;
                    tallAmiModel = SetTouValuesUsingTouId(tallAmiModel, "Consumption");
                    var tallAmiExists = tallAmiList.FirstOrDefault(l => l.AmiTimeStamp == tallAmiModel.AmiTimeStamp);
                    if (tallAmiExists != null)
                        tallAmiExists.Consumption = tallAmiModel.Consumption;
                    else
                        tallAmiList.Add(tallAmiModel);
                }
            }
            else
            {
                //Insert into Tall format
                var tallAmiModel = Mapper.Map<BusinessContracts.Preprocessing.AmiReading, TallAmiModel>(amiReading);
                tallAmiModel.Timezone = defaultTimezoneString;
                tallAmiModel = SetTouValuesUsingTouId(tallAmiModel, "Consumption");
                var tallAmiExists = tallAmiList.FirstOrDefault(l => l.AmiTimeStamp == tallAmiModel.AmiTimeStamp);
                if (tallAmiExists != null)
                    tallAmiExists.Consumption = tallAmiModel.Consumption;
                else
                    tallAmiList.Add(tallAmiModel);
            }

            return true;
        }

        /// <summary>
        /// calculate consumption from the reading
        /// </summary>
        /// <param name="clientId">clientId of the data to be fetched</param>
        /// <param name="meterId">meterId of the data to be fetched</param>
        /// <param name="amiTimestamp">amiTimestamp of the data to be fetched</param>
        /// <returns>consumption</returns>
        private double CalculateConsumptionFromReading(int clientId, string meterId, DateTime amiTimestamp)
        {
            var reading = AmiReading.Value.GetAmiReading(clientId, meterId, amiTimestamp).Result;
            var nextReading = AmiReading.Value.GetAmiReading(clientId, meterId, amiTimestamp.AddHours(1)).Result;

            if (reading != null && nextReading != null)
            {
                return nextReading.ReadingValue > reading.ReadingValue
                    ? nextReading.ReadingValue - reading.ReadingValue
                    : 0;
            }
            return 0;
        }

        /// <summary>
        /// Check whether sum needed or not
        /// </summary>
        /// <param name="amiTimeStamp">amiTimeStamp to check</param>
        /// <param name="dstEndDateTime">dstEndDateTime to check</param>
        /// <returns>bool</returns>
        private bool IsSumNeeded(DateTime amiTimeStamp, DateTime dstEndDateTime)
        {
            return amiTimeStamp >= dstEndDateTime &&
                               amiTimeStamp < dstEndDateTime.AddHours(1);
        }

        /// <summary>
        /// get the start and end date of DST
        /// </summary>
        /// <param name="timezone">timezone of the data to be fetched</param>
        /// <param name="startDateTime">startDateTime of the data to be fetched</param>
        /// <param name="endDateTime">endDateTime of the data to be fetched</param>
        /// <param name="transitionDateTime">transitionDateTime of the data to be fetched</param>
        private void GetDstStartEndDateTime(TimeZoneInfo timezone, out DateTime startDateTime, out DateTime endDateTime, DateTime transitionDateTime)
        {
            var rule = timezone
                .GetAdjustmentRules()
                .FirstOrDefault(r => transitionDateTime.Date >= r.DateStart.Date && transitionDateTime.Date <= r.DateEnd.Date);

            startDateTime = DateTime.MinValue;
            endDateTime = DateTime.MinValue;

            if (rule != null)
            {
                if (rule.DaylightTransitionStart.IsFixedDateRule)
                {
                    startDateTime = new DateTime(transitionDateTime.Year, rule.DaylightTransitionStart.Month, rule.DaylightTransitionStart.Day);
                }
                else
                {
                    var startDay = GetDateByWeek(transitionDateTime.Year, rule.DaylightTransitionStart.Month,
                        rule.DaylightTransitionStart.Week, (int)rule.DaylightTransitionStart.DayOfWeek);

                    startDateTime = new DateTime(transitionDateTime.Year, rule.DaylightTransitionStart.Month, startDay);
                }

                startDateTime = startDateTime.AddHours(rule.DaylightTransitionStart.TimeOfDay.Hour);

                if (rule.DaylightTransitionEnd.IsFixedDateRule)
                {
                    endDateTime = new DateTime(transitionDateTime.Year, rule.DaylightTransitionEnd.Month, rule.DaylightTransitionEnd.Day);
                }
                else
                {
                    var endDay = GetDateByWeek(transitionDateTime.Year, rule.DaylightTransitionEnd.Month,
                      rule.DaylightTransitionEnd.Week, (int)rule.DaylightTransitionEnd.DayOfWeek);

                    endDateTime = new DateTime(transitionDateTime.Year, rule.DaylightTransitionEnd.Month, endDay);
                    endDateTime = endDateTime.AddHours(rule.DaylightTransitionEnd.TimeOfDay.Hour);
                }
            }
        }

        /// <summary>
        /// check whether datetime shifting is needed or not 
        /// </summary>
        /// <param name="amiTimeStamp">amiTimeStamp of the data to be checked</param>
        /// <param name="clientHandlesDst">clientHandlesDst of the data to be checked</param>
        /// <param name="dstStartDateTime">dstStartDateTime of the data to be checked</param>
        /// <param name="dstEndDateTime">dstEndDateTime of the data to be checked</param>
        /// <returns>bool</returns>
        private bool IsShiftNeeded(DateTime amiTimeStamp, bool clientHandlesDst, DateTime dstStartDateTime, DateTime dstEndDateTime)
        {
            if (!clientHandlesDst)
                return amiTimeStamp >= dstStartDateTime && amiTimeStamp < dstEndDateTime;

            return false;
        }

        /// <summary>
        /// get the day from week
        /// </summary>
        /// <param name="year">specifies year of the date</param>
        /// <param name="month">specifies month of the date</param>
        /// <param name="weekNumOfMonth">specifies occurrence of week in month</param>
        /// <param name="dayOfWeek">specifies day of the date</param>
        /// <returns>day</returns>
        private int GetDateByWeek(int year, int month, int weekNumOfMonth, int dayOfWeek)
        {
            //weekNumOfMonth: specifies occurrence eg second
            //dayOfWeek: specifies day eg Sat, Sun, etc.

            var dt = new DateTime(year, month, 1);
            var occurrence = 0;
            while (true)
            {
                if (dayOfWeek == (int)dt.DayOfWeek)
                {
                    occurrence += 1;
                }

                if (occurrence == weekNumOfMonth)
                    break;

                dt = dt.AddDays(1);
            }
            return dt.Day;
        }

        /// <summary>
        /// get the list of ami readings
        /// </summary>
        /// <typeparam name="T">dynamic type</typeparam>
        /// <param name="amiRow">row of the data to be fetched</param>
        /// <returns>List of AmiReading</returns>
        public IEnumerable<BusinessContracts.Preprocessing.AmiReading> GetReadings<T>(T amiRow)
        {
            var amiReadingList = new List<BusinessContracts.Preprocessing.AmiReading>();
            List<PropertyInfo> propertyList = amiRow.GetType()
                .GetProperties().Where(p => p.Name.Contains("IntValue")).ToList();

            var amiTimezoneProperty = amiRow.GetType().GetProperties().FirstOrDefault(p => p.Name == "Timezone");
            var amiTimezone = amiTimezoneProperty != null ? Convert.ToString(amiTimezoneProperty.GetValue(amiRow)) : "";

            var amiTimeStampProperty = amiRow.GetType().GetProperties().FirstOrDefault(p => p.Name == "AmiTimeStamp");

            if (amiTimeStampProperty != null)
            {
                var amiTimeStamp = Convert.ToDateTime(amiTimeStampProperty.GetValue(amiRow));

                foreach (var property in propertyList)
                {
                    BusinessContracts.Preprocessing.AmiReading amiReading = Mapper.Map<T, BusinessContracts.Preprocessing.AmiReading>(amiRow);

                    var hh = Convert.ToInt32(property.Name.Replace("IntValue", "").Substring(0, 2));
                    var mm = Convert.ToInt32(property.Name.Replace("IntValue", "").Substring(2, 2));
                    if (hh >= 24) continue;
                    var timestamp = new DateTime(amiTimeStamp.Year, amiTimeStamp.Month,
                        amiTimeStamp.Day, hh, mm, 0);

                    amiReading.AmiTimeStamp = timestamp;
                    amiReading.Timezone = amiTimezone;
                    var consumption = property.GetValue(amiRow);
                    amiReading.Consumption = consumption != null ? Convert.ToDouble(consumption) : (double?)null;
                    amiReadingList.Add(amiReading);
                }
            }
            return amiReadingList;
        }

        /// <summary>
        /// clean all the interval values
        /// </summary>
        /// <typeparam name="T">dynamic type</typeparam>
        /// <param name="amiModel">amiModel for which to clean values</param>
        /// <param name="otherColumnNames">otherColumnNames for which to clean values</param>
        public void CleanAllIntValues<T>(T amiModel, string[] otherColumnNames)
        {
            List<PropertyInfo> propertyList;
            if (otherColumnNames != null && otherColumnNames.Any())
            {
                propertyList = amiModel.GetType()
                .GetProperties().Where(p => p.Name.Contains("IntValue") || otherColumnNames.Contains(p.Name)).ToList();
            }
            else
                propertyList = amiModel.GetType()
                    .GetProperties().Where(p => p.Name.Contains("IntValue")).ToList();

            foreach (var property in propertyList)
            {
                property.SetValue(amiModel, null);
            }
        }

        public T SetTouValuesUsingTouId<T>(T amiModel, string readingColumn)
        {
            var touProperty = amiModel.GetType().GetProperty("TOUID");
            var touId = Convert.ToInt32(touProperty.GetValue(amiModel));

            switch (touId)
            {
                case 0:
                    amiModel = SetTouValues(amiModel, "TOU_Regular", readingColumn);
                    break;
                case 1:
                    amiModel = SetTouValues(amiModel, "TOU_OnPeak", readingColumn);
                    break;
                case 2:
                    amiModel = SetTouValues(amiModel, "TOU_OffPeak", readingColumn);
                    break;
                case 3:
                    amiModel = SetTouValues(amiModel, "TOU_Shoulder1", readingColumn);
                    break;
                case 4:
                    amiModel = SetTouValues(amiModel, "TOU_Shoulder2", readingColumn);
                    break;
                case 5:
                    amiModel = SetTouValues(amiModel, "TOU_CriticalPeak", readingColumn);
                    break;
            }
            return amiModel;
        }

        private T SetTouValues<T>(T amiModel, string filterColumnName, string readingColumn)
        {
            var consumptionProperty = amiModel.GetType().GetProperty(readingColumn);
            var consumption = Convert.ToDouble(consumptionProperty.GetValue(amiModel));

            var property = amiModel.GetType().GetProperty(filterColumnName);
            property.SetValue(amiModel, consumption);
            return amiModel;
        }
    }
}
