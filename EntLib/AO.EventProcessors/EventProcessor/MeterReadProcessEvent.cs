﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;
using System.Threading.Tasks;
using AO.BusinessContracts;
using AutoMapper;
using CE.AO.Logging;
using CE.AO.Models;
using CE.AO.Models.RawFileModels;
using Microsoft.ServiceBus.Messaging;
using Newtonsoft.Json;
using Enums = CE.AO.Utilities.Enums;

namespace AO.EventProcessors.EventProcessor
{
    /// <summary>
    /// Class for processing MeterRead Event
    /// </summary>
    public class MeterReadProcessEvent : ProcessEvent
    {
        public override Enums.Module Module { get; } = Enums.Module.MeterReadEventProcess;
        public override Enums.EventType EventType { get; } = Enums.EventType.MeterRead;
        private Lazy<IMeterReadFacade> MeterReadFacade { set; get; }
        private Lazy<IUomConversion> UomConversion { set; get; }
        private Lazy<IClientConfigFacade> ClientConfigFacade { get; set; }
        private Lazy<IProcessTrigger<AccountPreAggCalculationRequestModel>> EventHubPreAggCalculationTrigger { get; set; }

        /// <summary>
        /// Parameterized contructor
        /// </summary>
        /// <param name="logModel">Used for logging details in case of any error</param>
        /// <param name="meterReadFacade"></param>
        /// <param name="veeFacade"></param>
        /// <param name="uomConversion"></param>
        /// <param name="clientConfigFacade"></param>
        /// <param name="eventHubPreAggCalculationTrigger"></param>
        public MeterReadProcessEvent(LogModel logModel, Lazy<IMeterReadFacade> meterReadFacade, Lazy<IVeeFacade> veeFacade,
            Lazy<IUomConversion> uomConversion, Lazy<IClientConfigFacade> clientConfigFacade, Lazy<IProcessTrigger<AccountPreAggCalculationRequestModel>> eventHubPreAggCalculationTrigger) : base(logModel)
        {
            MeterReadFacade = meterReadFacade;
            VeeFacade = veeFacade;
            UomConversion = uomConversion;
            ClientConfigFacade = clientConfigFacade;
            EventHubPreAggCalculationTrigger = eventHubPreAggCalculationTrigger;
        }

        /// <summary>
        /// Method to process MeterRead Event
        /// </summary>
        /// <param name="eventData">MeterRead event data</param>
        /// <param name="clientId">clientId to be validated</param>
        /// <returns></returns>
        public override async Task<bool> Process(EventData eventData, string clientId)
        {
            ClientId = clientId;
            var data = Encoding.UTF8.GetString(eventData.GetBytes());
            var rawModel = JsonConvert.DeserializeObject<RawMeterReadModel>(data);
            LogModel.ClientId = clientId;
            LogModel.AccountId = rawModel.ACCOUNT_NUMBER;
            LogModel.MeterId = rawModel.METER_ID;
            LogModel.CustomerId = rawModel.CUSTOMER_ID;

            var validationResults = new List<ValidationResult>();
            var uomIsValid = true;

            try
            {
                var rawModelUomId = (int)Enums.GetValueFromDescription<Enums.UomType>(rawModel.UNIT_OF_MEASURE);
                rawModel.UNIT_OF_MEASURE = rawModelUomId.ToString();
            }
            catch (Exception)
            {
                uomIsValid = false;
            }

            if (!uomIsValid)
            {
                validationResults.Add(new ValidationResult("UNIT_OF_MEASURE is not valid.",
                    new List<string> { "UNIT_OF_MEASURE" }));
            }

            if (validationResults.Count == 0)
            {
                //Map Raw Model to Model
                var model = Mapper.Map<RawMeterReadModel, MeterReadModel>(rawModel);

                //Validate Model
                if (Validate(model, out validationResults))
                {
                    var standardUnitsofMeasure = ClientConfigFacade.Value.GetStandardUom();
                    if (standardUnitsofMeasure == null)
                    {
                        Logger.Fatal("Standard Units of Measures are not found in Database.", LogModel);
                        return false;
                    }
                    try
                    {

                        switch (model.CommodityId)
                        {
                            case (int)Enums.CommodityType.Water:
                                model.StandardUOMId =
                                    (int)Enum.Parse(typeof(Enums.UomType), standardUnitsofMeasure.Water);
                                break;
                            case (int)Enums.CommodityType.Electric:
                                model.StandardUOMId =
                                    (int)Enum.Parse(typeof(Enums.UomType), standardUnitsofMeasure.Electric);
                                break;
                            case (int)Enums.CommodityType.Gas:
                                model.StandardUOMId =
                                    (int)Enum.Parse(typeof(Enums.UomType), standardUnitsofMeasure.Gas);
                                break;
                            default:
                                Logger.Fatal(
                                    "Standard Unit of Measure for " + (Enums.CommodityType)model.CommodityId +
                                    " Commodity is not defined.", LogModel);
                                return false;
                        }
                    }
                    catch (Exception ex)
                    {
                        Logger.Fatal(
                                    "Standard Unit of Measure: " + ex.Message, LogModel);
                        return false;
                    }
                    if (model.UOMId != model.StandardUOMId)
                    {
                        bool conversionNotFound;
                        model.StandardUOMConsumption = UomConversion.Value.Convert((Enums.UomType)model.UOMId,
                            (Enums.UomType)model.StandardUOMId, model.Consumption, out conversionNotFound);
                        if (conversionNotFound)
                        {
                            Logger.Fatal(
                                "Standard Unit of Measure conversion from " + (Enums.UomType)model.UOMId + " to " +
                                (Enums.UomType)model.StandardUOMId + " is not defined.", LogModel);
                            return false;
                        }
                    }
                    else if (model.UOMId == model.StandardUOMId)
                    {
                        model.StandardUOMConsumption = model.Consumption;
                    }

                    //Received Ami Timestamp are expected to be Interval End (i.e. time is set for the end of interval). So shifting it forward by substracting interval type from timestamp
                    model.AmiTimeStamp = model.AmiTimeStamp.AddMinutes(-1 * model.IntervalType);
                    var isSuccess = await MeterReadFacade.Value.InsertOrMergeMeterReadsAsync(model);

                    if (isSuccess)
                    {
                        // NOTE:  Commenting out this behavior as it shouldn't be active for the CE 16.10 release.  In fact, it should be configurable anyway.
                        //    var accountPreAggCalculationRequestModel = Mapper.Map<MeterReadModel, AccountPreAggCalculationRequestModel>(model)
                        //    EventHubPreAggCalculationTrigger.Value.Trigger(accountPreAggCalculationRequestModel)

                        //    var veeStagedClientMeterByDateDto = Mapper.Map<MeterReadModel, VeeStagedClientMeterByDateDto>(model)
                        //    VeeFacade.Value.InsertOrMergeVeeStagedDataAsync(veeStagedClientMeterByDateDto)
                        return true;
                    }
                }
            }

            var errorString = string.Empty;
            if (validationResults.Count > 0)
                errorString = $"Error: {JsonConvert.SerializeObject(validationResults)}.";
            Logger.Fatal($@"Event of type ""{Enums.EventType.MeterAccountMtu}"" is not valid.{errorString}", LogModel);
            return false;
        }

        /// <summary>
        /// Method to validate MeterReadModel model
        /// </summary>
        /// <param name="model">MeterReadModel model</param>
        /// <param name="validationResuts">list of validation results as out param</param>
        /// <returns></returns>
        private bool Validate(MeterReadModel model, out List<ValidationResult> validationResuts)
        {
            var isValid = base.Validate(model, out validationResuts);
            if (model.ClientId != Convert.ToInt32(ClientId))
            {
                isValid = false;
                validationResuts.Add(new ValidationResult("ClientId is not valid.", new List<string> { "ClientId" }));
            }
            return isValid;
        }
    }
}