﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;
using System.Threading.Tasks;
using AO.BusinessContracts;
using AutoMapper;
using CE.AO.Logging;
using CE.AO.Models;
using CE.AO.Models.RawFileModels;
using Microsoft.ServiceBus.Messaging;
using Newtonsoft.Json;
using Enums = CE.AO.Utilities.Enums;

namespace AO.EventProcessors.EventProcessor
{
    /// <summary>
    /// Class for processing MeterAccountMtu Event
    /// </summary>
    public class MeterAccountMtuProcessEvent : ProcessEvent
    {
        public override Enums.Module Module { get; } = Enums.Module.MeterAccountMtuEventProcess;
        public override Enums.EventType EventType { get; } = Enums.EventType.MeterAccountMtu;

        private Lazy<IMeterAccountMtuFacade> MeterAccountMtuFacade { get; set; }

        /// <summary>
        /// Parameterized contructor
        /// </summary>
        /// <param name="logModel">Used for logging details in case of any error</param>
        /// <param name="meterAccountMtuFacade"></param>
        public MeterAccountMtuProcessEvent(LogModel logModel, Lazy<IMeterAccountMtuFacade> meterAccountMtuFacade)
            : base(logModel)
        {
            MeterAccountMtuFacade = meterAccountMtuFacade;
        }

        /// <summary>
        /// Method to process MeterAccountMtu Event
        /// </summary>
        /// <param name="eventData">MeterAccountMtu event data</param>
        /// <param name="clientId">Unique Client Identifier</param>
        /// <returns></returns>
        public override async Task<bool> Process(EventData eventData, string clientId)
        {
            ClientId = clientId;

            var data = Encoding.UTF8.GetString(eventData.GetBytes());
            var rawModel = JsonConvert.DeserializeObject<RawMeterAccountMtuModel>(data);
            LogModel.ClientId = clientId;
            LogModel.AccountId = rawModel.ACCOUNT_NUMBER;
            LogModel.MeterId = rawModel.METER_ID;

            var model = Mapper.Map<RawMeterAccountMtuModel, MeterAccountMtuModel>(rawModel);
            //Validate Model
            List<ValidationResult> validationResults;
            if (Validate(model, out validationResults))
            {
                return await MeterAccountMtuFacade.Value.InsertOrMergeMeterAccountAsync(model);
            }

            var errorString = string.Empty;
            if (validationResults.Count > 0)
                errorString = $"Error: {JsonConvert.SerializeObject(validationResults)}.";
            Logger.Fatal($@"Event of type ""{Enums.EventType.MeterAccountMtu}"" is not valid.{errorString}", LogModel);
            return false;
        }

        /// <summary>
        /// Method to validate MeterAccountMtuModel model
        /// </summary>
        /// <param name="model">MeterAccountMtuModel model</param>
        /// <param name="validationResuts">list of validation results as out param</param>
        /// <returns></returns>
        private bool Validate(MeterAccountMtuModel model, out List<ValidationResult> validationResuts)
        {
            var isValid = base.Validate(model, out validationResuts);
            if (model.ClientId != Convert.ToInt32(ClientId))
            {
                isValid = false;
                validationResuts.Add(new ValidationResult("ClientId is not valid.", new List<string> { "ClientId" }));
            }
            return isValid;
        }
    }
}