﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AO.BusinessContracts;
using AO.BusinessContracts.EventProcessesContracts;
using AutoMapper;
using CE.AO.Logging;
using CE.AO.Models;
using CE.AO.Models.RawFileModels;
using Microsoft.ServiceBus.Messaging;
using Newtonsoft.Json;
using Enums = CE.AO.Utilities.Enums;

namespace AO.EventProcessors.EventProcessor
{
    /// <summary>
    /// AmiDailyIntervalProcessEvent class is used to process the event for ami daily event
    /// </summary>
    public class AmiDailyIntervalProcessEvent : ProcessEvent, IAmiProcessEvent
    {

        public Enums.IntervalType IntervalType { get; } = Enums.IntervalType.Daily;

        public override Enums.Module Module { get; } = Enums.Module.AMIEventProcess;

        public override Enums.EventType EventType { get; } = Enums.EventType.Ami;

        private Lazy<IClientConfigFacade> ClientConfigFacade { get; }
        private Lazy<ITallAMI> TallAmi { get; }
        private Lazy<IAzureQueue> AzureQueue { get; }
        private Lazy<IAmiProcessEventHelper> BaseAmiProcess { get; }

        public AmiDailyIntervalProcessEvent(LogModel logModel, Lazy<IClientConfigFacade> clientConfigFacade,
            Lazy<ITallAMI> tallAmi, Lazy<IAzureQueue> azureQueue, Lazy<IAmiProcessEventHelper> baseAmiProcess) : base(logModel)
        {
            ClientConfigFacade = clientConfigFacade;
            TallAmi = tallAmi;
            AzureQueue = azureQueue;
            BaseAmiProcess = baseAmiProcess;
        }
        

        /// <summary>
        /// To process the event which is related to the Daily Ami event
        /// </summary>
        /// <param name="eventData">Event data to be processed</param>
        /// <param name="clientId"></param>
        /// <returns>Boolean result</returns>
        public override async Task<bool> Process(EventData eventData, string clientId)
        {
            ClientId = clientId;

            var data = Encoding.UTF8.GetString(eventData.GetBytes());
            var rawModel = JsonConvert.DeserializeObject<RawAmiDailyIntervalModel>(data);
            LogModel.AccountId = rawModel.AccountNumber;
            LogModel.MeterId = rawModel.MeterId;
            LogModel.ClientId = clientId;

            //Map Raw Model to Model
            var model = Mapper.Map<RawAmiDailyIntervalModel, AmiDailyIntervalModel>(rawModel);
            model.IntervalType = Convert.ToInt32(Enums.IntervalType.Daily);
            model = BaseAmiProcess.Value.SetTouValuesUsingTouId(model, "IntValue0000");
            return await ShiftAndInsert(model);
        }

        /// <summary>
        /// To process the event which is related to the Daily Ami event
        /// </summary>
        /// <param name="rawModel"></param>
        /// <param name="clientId"></param>
        /// <returns>Boolean result</returns>
        //public async Task<bool> Process2(RawAmiDailyIntervalModel rawModel, string clientId) {
            public async Task<bool> Process2(RawAmiDailyIntervalModel rawModel, string clientId)
            {
                ClientId = clientId;
                LogModel.AccountId = rawModel.AccountNumber;
                LogModel.MeterId = rawModel.MeterId;
                LogModel.ClientId = clientId;

                //Map Raw Model to Model
                var model = Mapper.Map<RawAmiDailyIntervalModel, AmiDailyIntervalModel>(rawModel);
                model.IntervalType = Convert.ToInt32(Enums.IntervalType.Daily);
                model = BaseAmiProcess.Value.SetTouValuesUsingTouId(model, "IntValue0000");
                return await ShiftAndInsert(model).ConfigureAwait(false);
            }

        /// <summary>
        /// To process the event which is related to the Daily Ami event
        /// </summary>
        /// <param name="rawModel"></param>
        /// <param name="clientId"></param>
        /// <returns>Boolean result</returns>
        //public async Task<bool> Process2(RawAmiDailyIntervalModel rawModel, string clientId) {
        public bool Process2Sync(RawAmiDailyIntervalModel rawModel, string clientId) {
            ClientId = clientId;
            LogModel.AccountId = rawModel.AccountNumber;
            LogModel.MeterId = rawModel.MeterId;
            LogModel.ClientId = clientId;

            //Map Raw Model to Model
            var model = Mapper.Map<RawAmiDailyIntervalModel, AmiDailyIntervalModel>(rawModel);
            model.IntervalType = Convert.ToInt32(Enums.IntervalType.Daily);
            model = BaseAmiProcess.Value.SetTouValuesUsingTouId(model, "IntValue0000");
            return ShiftAndInsert(model).Result;
        }
        /// <summary>
        /// To insert the AmiDailyIntervalModel model after changing the timezone 
        /// </summary>
        /// <param name="amiDailyIntervalModel">AmiDailyIntervalModel model values to be shifted and inserted to the database</param>
        /// <returns>Boolean result</returns>
        private async Task<bool> ShiftAndInsert(AmiDailyIntervalModel amiDailyIntervalModel)
        {
            ////Get Client Settings
            var clientSettings = ClientConfigFacade.Value.GetClientSettings(amiDailyIntervalModel.ClientId);
            if (clientSettings == null)
            {
                Logger.Fatal(
                    "Client Settings are not configured for this client. Cannot proceed with futher ingestion.",
                    LogModel);
                return false;
            }

            if (string.IsNullOrWhiteSpace(clientSettings.TimeZone))
            {
                Logger.Fatal(
                    "Client Settings for TimeZone is not configured for this client. Cannot proceed with futher ingestion.",
                    LogModel);
                return false;
            }
            var clientHandlesDst = clientSettings.IsDstHandled; //Set from Settings
            var readingTimezoneString = clientSettings.TimeZone; //Set from Settings
            if (!string.IsNullOrWhiteSpace(amiDailyIntervalModel.Timezone))
            {
                readingTimezoneString = amiDailyIntervalModel.Timezone;
            }
            var readingTimezoneEnum =
                (Enums.Timezone)Enum.Parse(typeof(Enums.Timezone), Convert.ToString(readingTimezoneString));

            var readingTimezone = TimeZoneInfo.FindSystemTimeZoneById(readingTimezoneEnum.GetDescription());
            var defaultTimezoneString = clientSettings.TimeZone; //Set from Settings
            var defaultTimezoneEnum =
                (Enums.Timezone)Enum.Parse(typeof(Enums.Timezone), Convert.ToString(defaultTimezoneString));
            var defaultTimeZone = TimeZoneInfo.FindSystemTimeZoneById(defaultTimezoneEnum.GetDescription());

            amiDailyIntervalModel.Timezone = readingTimezoneString;
            var refAmiDailyIntervalModel =
                Mapper.Map<AmiDailyIntervalModel, AmiDailyIntervalModel>(amiDailyIntervalModel);

            var tallAmiList = new List<TallAmiModel>();

            //Convert to Readings
            IEnumerable<BusinessContracts.Preprocessing.AmiReading> amiReadingsList = BaseAmiProcess.Value.GetReadings(refAmiDailyIntervalModel);
            foreach (var amiReading in amiReadingsList)
            {
                BaseAmiProcess.Value.SetValueFromReading(amiReading, false, clientHandlesDst, 24, readingTimezoneEnum,
                    readingTimezone, readingTimezoneString, defaultTimeZone, defaultTimezoneString,
                    amiDailyIntervalModel, tallAmiList);
            }
            var todaysDateInReadingTimeZone = TimeZoneInfo.ConvertTimeFromUtc(DateTime.UtcNow, readingTimezone);
            var tasks = new List<Task<bool>>();
            if (tallAmiList.Count > 0)
            {
                IEnumerable<DateTime> distinctPartitions = tallAmiList.Select(d => d.AmiTimeStamp.Date).Distinct();
                tasks.AddRange(
                    distinctPartitions.Select(
                        partition =>
                           TallAmi.Value.InsertOrMergeDailyAmiAsync(
                                tallAmiList.Where(d => d.AmiTimeStamp.Date == partition).ToList())));
            }

            bool[] taskResults = await Task.WhenAll(tasks);
            if (!LogModel.Source.ToLower().Contains("batch"))
            {
                var queueMessage =
                    $"{amiDailyIntervalModel.ClientId}^^{amiDailyIntervalModel.MeterId}^^{todaysDateInReadingTimeZone.Date.ToString("MM/dd/yyyy")}^^{LogModel.Source}^^{LogModel.RowIndex}^^{LogModel.Metadata}^^{Convert.ToInt32(LogModel.ProcessingType)}";
                AzureQueue.Value.AddMessageToCalculationQueue(queueMessage).Wait();

            }
            return !taskResults.Any(result => false);
        }
    }
}
