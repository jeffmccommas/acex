﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;
using System.Threading.Tasks;
using AO.BusinessContracts;
using AO.BusinessContracts.EventProcessesContracts;
using AutoMapper;
using CE.AO.Logging;
using CE.AO.Models;
using CE.AO.Models.RawFileModels;
using Microsoft.ServiceBus.Messaging;
using Newtonsoft.Json;
using Enums = CE.AO.Utilities.Enums;

namespace AO.EventProcessors.EventProcessor
{
    /// <summary>
    /// DcuAlarmProcessEvent class is used to process the event for DcuAlarm event
    /// </summary>
    public class DcuAlarmProcessEvent : ProcessEvent
    {
        public override Enums.Module Module { get; } = Enums.Module.DcuAlarmEventProcess;
        public override Enums.EventType EventType { get; } = Enums.EventType.DcuAlarm;

        private readonly Lazy<IDcuAlarmFacade> _dcuAlarmFacade;
        public DcuAlarmProcessEvent(LogModel logModel, Lazy<IDcuAlarmFacade> dcuAlarmFacade) : base(logModel)
        {
            _dcuAlarmFacade = dcuAlarmFacade;
        }

        /// <summary>
        /// Method to process DcuAlarm Event
        /// </summary>
        /// <param name="eventData">DcuAlarm event data</param>
        /// <param name="clientId">Unique Client Identifier</param>
        /// <returns></returns>
        public override async Task<bool> Process(EventData eventData, string clientId)
        {
            ClientId = clientId;

            var data = Encoding.UTF8.GetString(eventData.GetBytes());
            var rawModel = JsonConvert.DeserializeObject<RawDcuAlarmModel>(data);
            LogModel.ClientId = clientId;

            var model = Mapper.Map<RawDcuAlarmModel, DcuAlarmModel>(rawModel);

            //Validate Model
            List<ValidationResult> validationResults;

            if (Validate(model, out validationResults))
            {
                return await _dcuAlarmFacade.Value.InsertOrMergeDcuAlarmAsync(model);
            }

            var errorString = string.Empty;
            if (validationResults.Count > 0)
                errorString = $"Error: {JsonConvert.SerializeObject(validationResults)}.";
            Logger.Fatal($@"Event of type ""{Enums.EventType.MeterAccountMtu}"" is not valid.{errorString}", LogModel);
            return false;
        }

        /// <summary>
        /// Method to validate DcuAlarmModel model
        /// </summary>
        /// <param name="model">DcuAlarmModel model</param>
        /// <param name="validationResuts">list of validation results as out param</param>
        /// <returns></returns>
        private bool Validate(DcuAlarmModel model, out List<ValidationResult> validationResuts)
        {
            var isValid = base.Validate(model, out validationResuts);
            if (model.ClientId != Convert.ToInt32(ClientId))
            {
                isValid = false;
                validationResuts.Add(new ValidationResult("ClientId is not valid.", new List<string> { "ClientId" }));
            }
            return isValid;
        }
    }
}