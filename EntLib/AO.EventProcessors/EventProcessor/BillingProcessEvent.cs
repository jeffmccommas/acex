﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;
using System.Threading.Tasks;
using AO.BusinessContracts;
using AutoMapper;
using CE.AO.Logging;
using CE.AO.Models;
using CE.AO.Models.RawFileModels;
using Microsoft.ServiceBus.Messaging;
using Newtonsoft.Json;
using Enums = CE.AO.Utilities.Enums;

namespace AO.EventProcessors.EventProcessor
{
    /// <summary>
    /// BillingProcessEvent class is used to process the event of billing file
    /// </summary>
    public class BillingProcessEvent : ProcessEvent
    {
        private Lazy<IBillingFacade> BillingFacade { get; }

        public override Enums.Module Module { get; } = Enums.Module.BillingEventProcess;

        public override Enums.EventType EventType { get; } = Enums.EventType.Bill;


        public BillingProcessEvent(LogModel logModel, Lazy<IBillingFacade> billingFacade) : base(logModel)
        {
            BillingFacade = billingFacade;
        }

        /// <summary>
        /// Process the AclaraOne bill data.
        /// </summary>
        /// <param name="eventData">The event data</param>
        /// <param name="clientId">Unique Client Identifier</param>
        /// <returns>True, if valid data.</returns>
        public override async Task<bool> Process(EventData eventData, string clientId)
        {
            ClientId = clientId;

            var data = Encoding.UTF8.GetString(eventData.GetBytes());
            var rawModel = JsonConvert.DeserializeObject<RawBillingModel>(data);

            LogModel.AccountId = rawModel.account_id;
            LogModel.MeterId = rawModel.meter_id;
            LogModel.ClientId = clientId;
            LogModel.CustomerId = rawModel.customer_id;
            LogModel.PremiseId = rawModel.premise_id;
            LogModel.ServiceContractId = rawModel.Service_contract;

            var model = Mapper.Map<RawBillingModel, BillingCustomerPremiseModel>(rawModel);

            //Validate Model
            List<ValidationResult> validationResults;
            if (Validate(model, out validationResults))
            {
                return await BillingFacade.Value.InsertOrMergeBillingAsync(model);
            }

            var errorString = string.Empty;
            if (validationResults.Count > 0)
                errorString = $"Error: {JsonConvert.SerializeObject(validationResults)}.";
            Logger.Fatal($@"Event of type ""Billing"" is not valid.{errorString}", LogModel);
            return false;
        }

        /// <summary>
        /// Validate the data in the BillingCustomerPremiseModel
        /// </summary>
        /// <param name="model">The BillingCustomerPremiseModel to validate</param>
        /// <param name="validationResuts">The validation results messages</param>
        /// <returns>True, if valid.</returns>
        private bool Validate(BillingCustomerPremiseModel model, out List<ValidationResult> validationResuts)
        {
            // Check against the base
            if (!base.Validate(model, out validationResuts))
            {
                return false;
            }

            //Check the ClientId
            if (model.ClientId != Convert.ToInt32(ClientId))
            {
                validationResuts.Add(new ValidationResult("ClientId is not valid.", new List<string> { "ClientId" }));
                return false;
            }

            // Check the meter type; if nonmetered, meter id should be empty
            var meterTypeEnum =
                (Enums.MeterType)Enum.Parse(typeof(Enums.MeterType), Convert.ToString(model.meter_type));
            if (meterTypeEnum == Enums.MeterType.nonmetered && !string.IsNullOrEmpty(model.meter_id))
            {
                validationResuts.Add(new ValidationResult("Invalid Row .", new List<string> { "meter_id" }));
                return false;
            }

            if (DateTime.Now.Subtract(model.bill_enddate).Days >= 0) return true;
            validationResuts.Add(new ValidationResult("Bill End Date is in the future .", new List<string> { "bill_enddate" }));
            return false;
        }
    }
}
