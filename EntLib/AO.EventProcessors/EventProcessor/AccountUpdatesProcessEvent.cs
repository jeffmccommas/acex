﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;
using System.Threading.Tasks;
using AO.BusinessContracts;
using AutoMapper;
using CE.AO.Logging;
using CE.AO.Models;
using CE.AO.Models.RawFileModels;
using Microsoft.ServiceBus.Messaging;
using Newtonsoft.Json;

namespace AO.EventProcessors.EventProcessor
{
    /// <summary>
    /// AccountUpdatesProcessEvent class is used to process the event for account updates event
    /// </summary>
    public class AccountUpdatesProcessEvent : ProcessEvent
    {
        public override CE.AO.Utilities.Enums.Module Module { get; } = CE.AO.Utilities.Enums.Module.AccountUpdatesEventProcess;
        public override CE.AO.Utilities.Enums.EventType EventType { get; } = CE.AO.Utilities.Enums.EventType.AccountUpdate;
    
        private Lazy<IAccountUpdatesFacade> AccountUpdatesFacade { get; set; }
        public AccountUpdatesProcessEvent(LogModel logModel, Lazy<IAccountUpdatesFacade> accountUpdatesFacade)
            : base(logModel)
        {
            AccountUpdatesFacade = accountUpdatesFacade;
        }

        /// <summary>
        /// To process the event which is related to the account updates event
        /// </summary>
        /// <param name="eventData">Event data to be processed</param>
        /// <param name="clientId"></param>
        /// <returns>Boolean result</returns>
        public override async Task<bool> Process(EventData eventData, string clientId)
        {
            ClientId = clientId;
            var data = Encoding.UTF8.GetString(eventData.GetBytes());
            var rawModel = JsonConvert.DeserializeObject<RawAccountUpdatesModel>(data);
	        LogModel.ClientId = clientId;
	        LogModel.AccountId = rawModel.AccountId;
	        LogModel.CustomerId = rawModel.CustomerId;
	        LogModel.PremiseId = rawModel.PremiseId;
	        LogModel.ServiceContractId = rawModel.ServiceContractId;
	        var model = Mapper.Map<RawAccountUpdatesModel, AccountUpdatesModel>(rawModel);
            //Validate Model
            List<ValidationResult> validationResults;
            if (Validate(model, out validationResults))
            {
                return await AccountUpdatesFacade.Value.InsertOrMergeAccountUpdateAsync(model);
            }

            var errorString = string.Empty;
            if (validationResults.Count > 0)
                errorString = $"Error: {JsonConvert.SerializeObject(validationResults)}.";
            Logger.Fatal($@"Event of type ""Billing"" is not valid.{errorString}", LogModel);
            return false;
        }

        /// <summary>
        /// Validate the data in the AccountUpdatesModel
        /// </summary>
        /// <param name="model">The AccountUpdatesModel to validate</param>
        /// <param name="validationResuts">The validation results messages</param>
        /// <returns>True, if valid.</returns>
        private bool Validate(AccountUpdatesModel model, out List<ValidationResult> validationResuts)
        {
            // Check against the base
            if (!base.Validate(model, out validationResuts))
            {
                return false;
            }

            // Check the ClientId
            if (model.ClientId != Convert.ToInt32(ClientId))
            {
                validationResuts.Add(new ValidationResult("ClientId is not valid.", new List<string> { "ClientId" }));
                return false;
            }

            // If all checks pass return true.
            return true;
        }
    }
}