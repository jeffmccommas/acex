﻿using Microsoft.ServiceBus.Messaging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;
using System.Threading.Tasks;
using AO.BusinessContracts;
using CE.AO.Models;
using AutoMapper;
using CE.AO.Logging;
using CE.AO.Models.RawFileModels;
using Enums = CE.AO.Utilities.Enums;
namespace AO.EventProcessors.EventProcessor
{
    /// <summary>
    /// Alarm Event Process Method
    /// </summary>
    public class AlarmProcessEvent : ProcessEvent
    {
        public override Enums.Module Module { get; } = Enums.Module.AlarmEventProcess;
        public override Enums.EventType EventType { get; } = Enums.EventType.Alarm;

        public Lazy<IAlarmFacade> AlarmFacade { get; set; }

        public AlarmProcessEvent(LogModel logModel, Lazy<IAlarmFacade> alarmFacade) : base(logModel)
        {
            AlarmFacade = alarmFacade;
        }

        /// <summary>
        /// Method to process Alarm Event
        /// </summary>
        /// <param name="eventData">Alarm event data</param>
        /// <param name="clientId">Unique Client Identifier</param>
        /// <returns></returns>
        public override async Task<bool> Process(EventData eventData, string clientId)
        {
            ClientId = clientId;
            var data = Encoding.UTF8.GetString(eventData.GetBytes());
            var rawModel = JsonConvert.DeserializeObject<RawAlarmModel>(data);
            LogModel.ClientId = clientId;

            var alarmModel = Mapper.Map<RawAlarmModel, AlarmModel>(rawModel);

            //Validate Model
            List<ValidationResult> validationResults;

            if (Validate(alarmModel, out validationResults))
            {
                return await AlarmFacade.Value.InsertOrMergeAlarmAsync(alarmModel);
            }

            var errorString = string.Empty;
            if (validationResults.Count > 0)
                errorString = $"Error: {JsonConvert.SerializeObject(validationResults)}.";
            Logger.Fatal($@"Event of type ""{Enums.EventType.Alarm}"" is not valid.{errorString}", LogModel);
            return false;
        }

        /// <summary>
        /// Method to validate AlarmModel model
        /// </summary>
        /// <param name="model">AlarmModel model</param>
        /// <param name="validationResuts">list of validation results as out param</param>
        /// <returns></returns>
        private bool Validate(AlarmModel model, out List<ValidationResult> validationResuts)
        {
            var isValid = base.Validate(model, out validationResuts);
            if (model.ClientId != Convert.ToInt32(ClientId))
            {
                isValid = false;
                validationResuts.Add(new ValidationResult("ClientId is not valid.", new List<string> { "ClientId" }));
            }
            return isValid;
        }
    }
}
