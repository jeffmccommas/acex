﻿using System;
using System.Linq;
using System.Threading.Tasks;
using AO.BusinessContracts.EventProcessesContracts;
using CE.AO.Models;
using CE.AO.Utilities;
using Microsoft.ServiceBus.Messaging;

namespace AO.EventProcessors.EventProcessor
{
    /// <summary>
    /// Root AMI processor which is ultimately responsible for just delegating to the appropriate interval specific processor which is injected as a list via DI.
    /// </summary>
    public class AmiProcessEvent : ProcessEvent
    {
        public override Enums.Module Module { get; } = Enums.Module.AMIEventProcess;
        public override Enums.EventType EventType { get; } = Enums.EventType.Ami;

        private IAmiProcessEvent[] AmiEventProcessors { get; }

        public AmiProcessEvent(LogModel logModel, IAmiProcessEvent[] amiEventProcessors ) : base(logModel)
        {
            AmiEventProcessors = amiEventProcessors;
        }

        public override Task<bool> Process(EventData eventData, string clientId)
        {
            ClientId = clientId;

            var intervalTypeString = eventData.Properties.Single(p => p.Key == "IntervalType").Value.ToString();

            var intervalTypeEnum = (Enums.IntervalType) Enum.Parse(typeof (Enums.IntervalType), intervalTypeString);

            // Grab the correct processor.
            IAmiProcessEvent processor = AmiEventProcessors.Single(p => p.IntervalType == intervalTypeEnum);

            // Delegate processing to the correct interval specific processor.
            return processor.Process(eventData, clientId);
        }
    }
}