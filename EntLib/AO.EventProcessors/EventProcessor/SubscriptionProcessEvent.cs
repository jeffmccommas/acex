﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using AO.BusinessContracts;
using AutoMapper;
using CE.AO.Logging;
using CE.AO.Models;
using CE.AO.Models.RawFileModels;
using Microsoft.ServiceBus.Messaging;
using Newtonsoft.Json;
using Enums = CE.AO.Utilities.Enums;

namespace AO.EventProcessors.EventProcessor
{
    /// <summary>
    /// SubscriptionProcessEvent class is used to process the event of subscription file
    /// </summary>
    public class SubscriptionProcessEvent : ProcessEvent
    {
        private const string CustomKeySeperator = ";";
        public override Enums.Module Module { get; } = Enums.Module.SubscriptionEventProcess;

        public override Enums.EventType EventType { get; } = Enums.EventType.Subscription;

        private Lazy<IClientConfigFacade> ClientConfigFacade { get; set; }

        private Lazy<IBilling> Billing { get; set; }

        private Lazy<ISubscription> Subscription { get; set; }

        private Lazy<ICustomer> Customer { get; set; }

        private Lazy<INotification> Notification { get; set; }

        private Lazy<ISendSms> Sendsms { get; set; }

        private Lazy<ISendEmail> SendEmail { get; set; }


        public SubscriptionProcessEvent(LogModel logModel, Lazy<IClientConfigFacade> clientConfigFacade, Lazy<IBilling> billing, Lazy<ISubscription> subscription, Lazy<ICustomer> customer, Lazy<INotification> notification, Lazy<ISendSms> sendSms, Lazy<ISendEmail> sendEmail) : base(logModel)
        {
            ClientConfigFacade = clientConfigFacade;
            Billing = billing;
            Subscription = subscription;
            Customer = customer;
            Notification = notification;
            SendEmail = sendEmail;
            Sendsms = sendSms;
        }

        public override async Task<bool> Process(EventData eventData, string clientId)
        {
            ClientId = clientId;

            var data = Encoding.UTF8.GetString(eventData.GetBytes());
            var rawModel = JsonConvert.DeserializeObject<RawSubscriptionModel>(data);

            LogModel.AccountId = rawModel.AccountId;
            LogModel.ClientId = clientId;
            LogModel.CustomerId = rawModel.CustomerId;
            LogModel.ServiceContractId = rawModel.ServiceContractId;
            var addEmailConfirmationCount = false;
            var addSmsConfirmationCount = false;

            //Modify rawModel.ServiceContractId WHEN rawModel.InsightTypeName is NOT provided but rawModel.ServiceContractId is provided
            //Convert to program level only by blanking the ServiceContractID
            if (string.IsNullOrWhiteSpace(rawModel.InsightTypeName) && !string.IsNullOrWhiteSpace(rawModel.ServiceContractId))
            {
                rawModel.ServiceContractId = string.Empty;
            }
            else if (!string.IsNullOrWhiteSpace(rawModel.InsightTypeName) && !string.IsNullOrWhiteSpace(rawModel.ProgramName))
            {
                // force all insights' double optin complete to true regardless of what's being sent since we only send out opt in email at program level
                rawModel.IsEmailOptInCompleted = "TRUE";
                rawModel.IsSmsOptInCompleted = "TRUE";
            }

            //Map Raw Model to Model
            var model = Mapper.Map<RawSubscriptionModel, SubscriptionModel>(rawModel);

            //Validate Model
            List<ValidationResult> validationResults;
            if (Validate(model, out validationResults))
            {
                var clientSetting = ClientConfigFacade.Value.GetClientSettings(model.ClientId);
                var programSettings =
                    clientSetting.Programs.Find(item => item.ProgramName.Equals(model.ProgramName));

                // if it's insight level
                if (!string.IsNullOrWhiteSpace(model.ProgramName) && !string.IsNullOrWhiteSpace(model.InsightTypeName))
                {
                    var insightSetting = programSettings.Insights.Find(
                        item =>
                            item.InsightName.Equals(model.InsightTypeName, StringComparison.InvariantCultureIgnoreCase));
                    if (insightSetting != null && (!string.IsNullOrWhiteSpace(model.ServiceContractId) &&
                                                   !string.IsNullOrWhiteSpace(insightSetting.DefaultValue) &&
                                                   insightSetting.ThresholdType.Equals("usage",
                                                       StringComparison.InvariantCultureIgnoreCase)))
                    {
                        // get uom from bill
                        var service =
                            await
                                Billing.Value.GetServiceDetailsFromBillAsync(model.ClientId, model.CustomerId,
                                    model.AccountId,
                                    model.ServiceContractId);

                        if (service != null)
                        {
                            model.UomId = service.UOMId;
                        }
                    }
                }
                var result = await Subscription.Value.InsertSubscriptionAsync(model, false, false);

                if (result && model.IsSelected)
                {
                    var subscriptionModel =
                        await Subscription.Value.GetSubscriptionDetailsAsync(model.ClientId, model.CustomerId,
                            model.AccountId, model.ProgramName, model.ServiceContractId, model.InsightTypeName);

                    if (subscriptionModel != null && string.IsNullOrWhiteSpace(subscriptionModel.InsightTypeName))
                    {

                        var customerModel = await Customer.Value.GetCustomerAsync(subscriptionModel.ClientId,
                            subscriptionModel.CustomerId);

                        if (!Convert.ToBoolean(subscriptionModel.IsEmailOptInCompleted))
                        {
                            if (programSettings.DoubleOptInRequired)
                            {
                                if (customerModel != null)
                                {
                                    await SendMail(subscriptionModel, clientSetting, customerModel).ConfigureAwait(false);
                                    if (!string.IsNullOrEmpty(customerModel.EmailAddress))
                                        addEmailConfirmationCount = true;

                                    // set email opt in completed to true after the subcription email is sent since we only support single opt-in
                                    subscriptionModel.IsEmailOptInCompleted = true;
                                }
                                else
                                    Logger.Fatal(@"Customer not found while Sending Email.", LogModel);
                            }
                            else
                            {
                                subscriptionModel.IsEmailOptInCompleted = true;
                            }
                        }

                        if (!Convert.ToBoolean(subscriptionModel.IsSmsOptInCompleted))
                        {
                            if (programSettings.DoubleOptInRequired)
                            {
                                if (customerModel != null)
                                {
                                    // sms settings                                                                        
                                    var smsApiKey = clientSetting.TrumpiaApiKey;
                                    var smsUserName = clientSetting.TrumpiaUserName;
                                    var smsContactList = clientSetting.TrumpiaContactList;

                                    if (string.IsNullOrEmpty(customerModel.TrumpiaSubscriptionId))
                                    {
                                        if (string.IsNullOrEmpty(customerModel.Phone1))
                                        {
                                            Logger.Fatal(@"Phone number not specified in Customer information.",
                                                LogModel);
                                        }
                                        else
                                        {
                                            Customer.Value.CreateSubscriptionAndUpdateCustomer(customerModel, smsApiKey,
                                                smsUserName, smsContactList);
                                        }
                                    }
                                    else
                                    {
                                        if (string.IsNullOrEmpty(customerModel.Phone1))
                                        {
                                            Logger.Fatal(@"Phone number not specified in Customer information.",
                                                LogModel);
                                        }
                                        else
                                        {
                                            // check if the customer is opt-out by text, if so, re-register 
                                            var reOptin = false;
                                            Customer.Value.SmsSubscirptionCheckAndUpdate(customerModel, smsApiKey,
                                                smsUserName, smsContactList, ref reOptin);
                                        }
                                    }

                                    if (!string.IsNullOrEmpty(customerModel.TrumpiaSubscriptionId) && customerModel.TrumpiaSubscriptionId != "0")
                                    {
                                        await SendSms(subscriptionModel, clientSetting, customerModel).ConfigureAwait(false);
                                        addSmsConfirmationCount = true;

                                        // set sms opt in completed to true after the subcription sms is sent since we only support single opt-in
                                        subscriptionModel.IsSmsOptInCompleted = true;
                                    }
                                }
                                else
                                    Logger.Fatal(@"Customer not found while Sending SMS.", LogModel);
                            }
                            else
                            {
                                subscriptionModel.IsSmsOptInCompleted = true;
                            }
                        }
                        await
                            Subscription.Value.InsertSubscriptionAsync(subscriptionModel, addEmailConfirmationCount,
                                addSmsConfirmationCount);
                    }
                }
                return result;
            }
            var errorString = string.Empty;
            if (validationResults.Count > 0)
                errorString = $"Error: {JsonConvert.SerializeObject(validationResults)}.";
            Logger.Fatal($@"Event of type ""Subscription"" is not valid.{errorString}", LogModel);
            return false;
        }

        /// <summary>
        /// Send sms with custome key as subscription details
        /// </summary>
        /// <param name="subscriptionModel">subscriptionEntity details to be send</param>
        /// <param name="clientSetting">clientSetting details to be send</param>
        /// <param name="customerModel">customerModel to fetch data and send deatils</param>
        /// <returns>task result</returns>
        public async Task SendSms(SubscriptionModel subscriptionModel, ClientSettings clientSetting, CustomerModel customerModel)
        {
            var settings = ClientConfigFacade.Value.GetClientSettings(subscriptionModel.ClientId);
            var smsApiKey = settings.TrumpiaApiKey;
            var smsUserName = settings.TrumpiaUserName;

            var smsTemplateModel =
                await
                Notification.Value.GetSmsTemplateFromDB(subscriptionModel.ClientId,
                        "OptInConfirmation",
                        clientSetting.OptInSmsConfirmationTemplateId);
            var substitutes = Regex.Matches(smsTemplateModel.Body, "[-][a-zA-Z]+[-]");
            smsTemplateModel.Body = Notification.Value.GetReplacedSmsTemplateForSubscriptionDoubleOptInConfirmation(smsTemplateModel.Body,
                subscriptionModel, clientSetting, customerModel, substitutes);

            var subscriptionPrimaryKey = subscriptionModel.ClientId + CustomKeySeperator + subscriptionModel.CustomerId +
                                         CustomKeySeperator + subscriptionModel.AccountId + CustomKeySeperator +
                                         subscriptionModel.Type + CustomKeySeperator + subscriptionModel.ProgramName +
                                         CustomKeySeperator + subscriptionModel.InsightTypeName + CustomKeySeperator +
                                         subscriptionModel.ServiceContractId + CustomKeySeperator +
                                         subscriptionModel.SubscriptionDate + CustomKeySeperator +
                                         subscriptionModel.IsLatest;
            var phone = customerModel.Phone1 ?? customerModel.Phone2;
            await
                Sendsms.Value.Send(smsApiKey, smsUserName, smsTemplateModel.Body, phone, customerModel.TrumpiaSubscriptionId,
                    $"s|{subscriptionPrimaryKey}", subscriptionModel.ClientId,
                    subscriptionModel.AccountId, subscriptionModel.ProgramName, subscriptionModel.ServiceContractId,
                    subscriptionModel.InsightTypeName, DateTime.UtcNow);
        }

        /// <summary>
        ///  Send email with custome key as a combinationof notification & subscription details
        /// </summary>
        /// <param name="subscriptionModel">subscriptionEntity details to be send</param>
        /// <param name="clientSetting">clientSetting details to be send</param>
        /// <param name="customerModel">customerModel to fetch data and send deatils</param>
        public async Task SendMail(SubscriptionModel subscriptionModel, ClientSettings clientSetting, CustomerModel customerModel)
        {
            // email settings
            var clientEmailUsername = clientSetting.OptInEmailUserName;
            var clientEmailPassword = clientSetting.OptInEmailPassword;
            var sender = clientSetting.OptInEmailFrom;
            if (string.IsNullOrEmpty(customerModel.EmailAddress))
            {
                Logger.Fatal(@" Email address not specified in Customer information.", LogModel);
                return;
            }
            var address = string.Empty;

            if (!string.IsNullOrEmpty(customerModel.AddressLine1))
                address = address + customerModel.AddressLine1;

            if (!string.IsNullOrEmpty(customerModel.AddressLine2))
            {
                address = !string.IsNullOrEmpty(address)
                    ? address + ", " + customerModel.AddressLine2
                    : address + customerModel.AddressLine2;
            }

            if (!string.IsNullOrEmpty(customerModel.AddressLine3))
            {
                address = !string.IsNullOrEmpty(address)
                    ? address + ", " + customerModel.AddressLine3
                    : address + customerModel.AddressLine3;
            }
            var accountNumber = subscriptionModel.AccountId;
            if (clientSetting.UnmaskedAccountIdEndingDigit > 0)
            {
                accountNumber = AccountLookupModel.GetMaskedAccountId(accountNumber, clientSetting.UnmaskedAccountIdEndingDigit);
            }

            List<string> emailAddress = new List<string> { customerModel.EmailAddress };
            string description = $@"{subscriptionModel.ClientId}|{subscriptionModel.CustomerId}|{subscriptionModel.AccountId}|{subscriptionModel.ProgramName}";
            var encodedDescription = Encoding.UTF8.GetBytes(description);
            var identifiers = new Dictionary<string, List<string>>
            {
                {"-FirstName-", new List<string> {customerModel.FirstName}},
                {"-LastName-", new List<string> {customerModel.LastName}},
                {"-ConfirmationLink-" , new List<string> { $"description={Convert.ToBase64String(encodedDescription)}" } },
                {"-ProgramName-" , new List<string> { subscriptionModel.ProgramName } },
                { "-AccountAddress-",new List<string>{address}},
                { "-AccountNumber-", new List<string> {accountNumber}}
            };

            var templateId = clientSetting.OptInEmailConfirmationTemplateId;

            var notificationPrimaryKey = subscriptionModel.ClientId + CustomKeySeperator + subscriptionModel.CustomerId +
                                         CustomKeySeperator + subscriptionModel.AccountId + CustomKeySeperator +
                                         subscriptionModel.ProgramName + CustomKeySeperator + null + CustomKeySeperator +
                                         null + CustomKeySeperator + Enums.SubscriptionChannel.Email.ToString() +
                                         CustomKeySeperator + false;  // changed false to true as previously false refers to latest record now true refers latest record (IsLatest property)
            var subscriptionPrimaryKey = subscriptionModel.ClientId + CustomKeySeperator + subscriptionModel.CustomerId +
                                         CustomKeySeperator + subscriptionModel.AccountId + CustomKeySeperator +
                                         subscriptionModel.ProgramName + CustomKeySeperator +
                                         subscriptionModel.InsightTypeName + CustomKeySeperator +
                                         subscriptionModel.ServiceContractId + CustomKeySeperator +
                                         subscriptionModel.SubscriptionDate + CustomKeySeperator +
                                         subscriptionModel.IsLatest;
            var customKey = string.Format(notificationPrimaryKey + "|" + subscriptionPrimaryKey);
            await SendEmail.Value.SendEmailAsync(sender, emailAddress, identifiers, templateId, clientEmailUsername, clientEmailPassword, customKey, null).ConfigureAwait(false);
        }

        /// <summary>
        /// Validate the data in the SubscriptionModel
        /// </summary>
        /// <param name="model">The SubscriptionModel to validate</param>
        /// <param name="validationResuts">The validation results messages</param>
        /// <returns>True, if valid.</returns>
        private bool Validate(SubscriptionModel model, out List<ValidationResult> validationResuts)
        {
            // Check against the base
            if (!base.Validate(model, out validationResuts))
            {
                return false;
            }

            //Check the ClientId
            if (model.ClientId != Convert.ToInt32(ClientId))
            {
                validationResuts.Add(new ValidationResult("ClientId is not valid", new List<string> { "ClientId" }));
                return false;
            }

            return true;
        }
    }
}
