﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;
using System.Threading.Tasks;
using AO.BusinessContracts;
using AutoMapper;
using CE.AO.Logging;
using CE.AO.Models;
using CE.AO.Models.RawFileModels;
using Microsoft.ServiceBus.Messaging;
using Newtonsoft.Json;
using Enums = CE.AO.Utilities.Enums;

namespace AO.EventProcessors.EventProcessor
{
    /// <summary>
    /// Class for processing DcuProvisioning Event
    /// </summary>
    public class DcuProvisioningProcessEvent : ProcessEvent
    {
        public override Enums.Module Module { get; } = Enums.Module.DcuProvisioningEventProcess;
        public override Enums.EventType EventType { get; } = Enums.EventType.DcuProvisioningData;

        private Lazy<IDcuProvisioning> DcuProvisioning { get; set; }

        /// <summary>
        /// Parameterized contructor
        /// </summary>
        /// <param name="logModel">Used for logging details in case of any error</param>
        /// <param name="dcuProvisioning"></param>
        public DcuProvisioningProcessEvent(LogModel logModel, Lazy<IDcuProvisioning> dcuProvisioning) : base(logModel)
        {
            DcuProvisioning = dcuProvisioning;
        }

        /// <summary>
        /// Method to process DcuProvisioning Event
        /// </summary>
        /// <param name="eventData">DcuProvisioning event data</param>
        /// <param name="clientId">Unique Client Identifier</param>
        /// <returns></returns>
        public override async Task<bool> Process(EventData eventData, string clientId)
        {
            ClientId = clientId;

            var data = Encoding.UTF8.GetString(eventData.GetBytes());
            var rawModel = JsonConvert.DeserializeObject<RawDcuProvisioningModel>(data);
			LogModel.ClientId = clientId;

	        var model = Mapper.Map<RawDcuProvisioningModel, DcuProvisioningModel>(rawModel);

            //Validate Model
            List<ValidationResult> validationResults;
            if (Validate(model, out validationResults))
            {
                await DcuProvisioning.Value.InsertOrMergeDcuProvisioningAsync(model).ConfigureAwait(false);
                return true;
            }

            var errorString = string.Empty;
            if (validationResults.Count > 0)
                errorString = $"Error: {JsonConvert.SerializeObject(validationResults)}.";
            Logger.Fatal($@"Event of type ""{Enums.EventType.DcuProvisioningData}"" is not valid.{errorString}", LogModel);
            return false;
        }

        /// <summary>
        /// Method to validate DcuProvisioningModel model
        /// </summary>
        /// <param name="model">DcuProvisioningModel model</param>
        /// <param name="validationResuts">list of validation results as out param</param>
        /// <returns></returns>
        private bool Validate(DcuProvisioningModel model, out List<ValidationResult> validationResuts)
        {
            var isValid = base.Validate(model, out validationResuts);
            if (model.ClientId != Convert.ToInt32(ClientId))
            {
                isValid = false;
                validationResuts.Add(new ValidationResult("ClientId is not valid.", new List<string> { "ClientId" }));
            }
            return isValid;
        }
    }
}