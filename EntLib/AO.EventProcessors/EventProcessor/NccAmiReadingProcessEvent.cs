﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;
using System.Threading.Tasks;
using AO.BusinessContracts;
using AutoMapper;
using CE.AO.Logging;
using CE.AO.Models;
using CE.AO.Models.RawFileModels;
using Microsoft.ServiceBus.Messaging;
using Newtonsoft.Json;
using Enums = CE.AO.Utilities.Enums;

namespace AO.EventProcessors.EventProcessor
{
    /// <summary>
    /// NccAmiReadingProcessEvent class is used to process the event for ncc ami event
    /// </summary>
    public class NccAmiReadingProcessEvent : ProcessEvent
    {
        public override Enums.Module Module { get; } = Enums.Module.NccEventProcess;
        public override Enums.EventType EventType { get; } = Enums.EventType.NccAmiReading;

        private Lazy<IAmiReading> AmiReading { get; set; }

        public NccAmiReadingProcessEvent(LogModel logModel, Lazy<IAmiReading> amiReading) : base(logModel)
        {
            AmiReading = amiReading;
        }

        /// <summary>
        /// To process the event which is related to the ncc ami event
        /// </summary>
        /// <param name="eventData">Event data to be processed</param>
        /// <param name="clientId">Unique Client Identifier</param>
        /// <returns>Boolean result</returns>
        public override async Task<bool> Process(EventData eventData, string clientId)
        {
            ClientId = clientId;

            var data = Encoding.UTF8.GetString(eventData.GetBytes());
            var rawModel = JsonConvert.DeserializeObject<RawNccAmiReadingModel>(data);

            LogModel.MeterId = rawModel.METER_ID;
            LogModel.CustomerId = rawModel.CUSTOMER_ID;
            LogModel.ClientId = clientId;

            var validationResults = new List<ValidationResult>();
            var uomIsValid = true;
            try
            {
                var rawModelUomId = (int)Enums.GetValueFromDescription<Enums.UomType>(rawModel.UNIT_OF_MEASURE);
                rawModel.UNIT_OF_MEASURE = rawModelUomId.ToString();

                // Check the UOM
                if (!Enum.IsDefined(typeof(Enums.UomType), rawModelUomId))
                {
                    uomIsValid = false;
                }
            }
            catch (Exception)
            {
                uomIsValid = false;
            }

            if (!uomIsValid)
            {
                validationResults.Add(new ValidationResult("UNIT_OF_MEASURE is not valid.",
                    new List<string> { "UNIT_OF_MEASURE" }));
            }

            if (validationResults.Count == 0)
            {
                //Map Raw Model to Model
                var model = Mapper.Map<RawNccAmiReadingModel, NccAmiReadingModel>(rawModel);
                model.ClientId = Convert.ToInt32(ClientId);

                //Validate Model
                if (Validate(model, out validationResults))
                {
                    return await AmiReading.Value.InsertOrMergeAmiReadingAsync(model);
                }
            }

            var errorString = string.Empty;
            if (validationResults.Count > 0)
                errorString = $"Error: {JsonConvert.SerializeObject(validationResults)}.";
            Logger.Fatal($@"Event of type ""NCC AMI - 60 min"" is not valid.{errorString}", LogModel);
            return false;
        }

        /// <summary>
        /// Validate the data in the NccAmiReadingModel
        /// </summary>
        /// <param name="model">The NccAmiReadingModel to validate</param>
        /// <param name="validationResuts">The validation results messages</param>
        /// <returns>True, if valid.</returns>
        private bool Validate(NccAmiReadingModel model, out List<ValidationResult> validationResuts)
        {
            // Check against the base
            if (!base.Validate(model, out validationResuts))
            {
                return false;
            }

            //Check the ClientId
            if (model.ClientId != Convert.ToInt32(ClientId))
            {
                validationResuts.Add(new ValidationResult("ClientId is not valid", new List<string> { "ClientId" }));
                return false;
            }

            // Check the CommodityType
            if (!Enum.IsDefined(typeof(Enums.CommodityType), model.CommodityId))
            {
                validationResuts.Add(new ValidationResult("CommodityId is not valid.", new List<string> { "CommodityId" }));
                return false;
            }

            return true;
        }
    }
}
