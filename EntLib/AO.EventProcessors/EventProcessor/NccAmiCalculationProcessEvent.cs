﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;
using System.Threading.Tasks;
using AO.BusinessContracts;
using AutoMapper;
using CE.AO.Models;
using CE.AO.Models.RawFileModels;
using Microsoft.ServiceBus.Messaging;
using Newtonsoft.Json;
using Enums = CE.AO.Utilities.Enums;

namespace AO.EventProcessors.EventProcessor
{
    /// <summary>
    /// NccAmiCalculationProcessEvent class is used to process the event for ncc ami calculation event
    /// </summary>
    public class NccAmiCalculationProcessEvent : ProcessEvent
    {
        public override Enums.Module Module { get; } = Enums.Module.NccAmiCalculationEventProcess;
        public override Enums.EventType EventType { get; } = Enums.EventType.NccAmiCalculation;

        private Lazy<IAzureQueue> AzureQueue { get; set; }

        public NccAmiCalculationProcessEvent(LogModel logModel, Lazy<IAzureQueue> azureQueue) : base(logModel)
        {
            AzureQueue = azureQueue;
        }

        /// <summary>
        /// To process the event which is related to the ncc ami calculation event
        /// </summary>
        /// <param name="eventData">Event data to be processed</param>
        /// <param name="clientId">Unique Client Identifier</param>
        /// <returns>Boolean result</returns>
        public override async Task<bool> Process(EventData eventData, string clientId)
        {
            ClientId = clientId;

            var data = Encoding.UTF8.GetString(eventData.GetBytes());
            var rawModel = JsonConvert.DeserializeObject<RawBaseReadModel>(data);

            LogModel.MeterId = rawModel.METER_ID;
            LogModel.CustomerId = rawModel.CUSTOMER_ID;
            LogModel.ClientId = clientId;

            var validationResults = new List<ValidationResult>();
            var uomIsValid = true;
            try
            {
                var rawModelUomId = (int)Enums.GetValueFromDescription<Enums.UomType>(rawModel.UNIT_OF_MEASURE);
                rawModel.UNIT_OF_MEASURE = rawModelUomId.ToString();

                // Check the UOM
                if (!Enum.IsDefined(typeof(Enums.UomType), rawModelUomId))
                {
                    uomIsValid = false;
                }
            }
            catch (Exception)
            {
                uomIsValid = false;
            }

            if (!uomIsValid)
            {
                validationResults.Add(new ValidationResult("UNIT_OF_MEASURE is not valid.",
                    new List<string> { "UNIT_OF_MEASURE" }));
            }

            if (validationResults.Count == 0)
            {
                //Map Raw Model to Model
                var model = Mapper.Map<RawBaseReadModel, NccAmiReadingModel>(rawModel);
                model.ClientId = Convert.ToInt32(ClientId);

                //Validate Model
                if (Validate(model, out validationResults))
                {
                    var queueMessage =
                        $"{model.ClientId}^^{model.MeterId}^^{model.AmiTimeStamp.Date.ToString("MM/dd/yyyy")}^^{LogModel.Source}^^{LogModel.RowIndex}^^{LogModel.Metadata}^^{Convert.ToInt32(LogModel.ProcessingType)}";
                    await AzureQueue.Value.AddMessageToCalculationQueue(queueMessage).ConfigureAwait(false);
                    return true;
                }
            }
            return false;
        }

        /// <summary>
        /// Validate the data in the NccAmiReadingModel
        /// </summary>
        /// <param name="model">The NccAmiReadingModel to validate</param>
        /// <param name="validationResuts">The validation results messages</param>
        /// <returns>True, if valid.</returns>
        private bool Validate(NccAmiReadingModel model, out List<ValidationResult> validationResuts)
        {
            // Check against the base
            if (!base.Validate(model, out validationResuts))
            {
                return false;
            }

            //Check the ClientId
            if (model.ClientId != Convert.ToInt32(ClientId))
            {
                validationResuts.Add(new ValidationResult("ClientId is not valid", new List<string> { "ClientId" }));
                return false;
            }

            // Check the CommodityType
            if (!Enum.IsDefined(typeof(Enums.CommodityType), model.CommodityId))
            {
                validationResuts.Add(new ValidationResult("CommodityId is not valid.", new List<string> { "CommodityId" }));
                return false;
            }

            return true;
        }
    }
}