﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;
using System.Threading.Tasks;
using AO.BusinessContracts;
using AutoMapper;
using CE.AO.Logging;
using CE.AO.Models;
using CE.AO.Models.RawFileModels;
using Microsoft.ServiceBus.Messaging;
using Newtonsoft.Json;
using Enums = CE.AO.Utilities.Enums;

namespace AO.EventProcessors.EventProcessor
{
    /// <summary>
    /// Class for trigerring calculation after processing MeterRead Event
    /// </summary>
    public class MeterReadTriggerCalculationProcessEvent : ProcessEvent
    {

        public override Enums.Module Module { get; } = Enums.Module.MeterReadCalculationEventProcess;
        public override Enums.EventType EventType { get; } = Enums.EventType.MeterReadTriggerCalculation;

        private Lazy<IAzureQueue> AzureQueue { get; set; }

        /// <summary>
        /// Parameterized contructor
        /// </summary>
        /// <param name="logModel">Used for logging details in case of any error</param>
        /// <param name="azureQueue"></param>
        public MeterReadTriggerCalculationProcessEvent(LogModel logModel, Lazy<IAzureQueue> azureQueue) : base(logModel)
        {
            AzureQueue = azureQueue;
        }

        /// <summary>
        /// Method to process MeterRead Event
        /// </summary>
        /// <param name="eventData">MeterRead event data</param>
        /// <param name="clientId">Unique Client Identifier</param>
        /// <returns></returns>
        public override async Task<bool> Process(EventData eventData, string clientId)
        {
            ClientId = clientId;

            var data = Encoding.UTF8.GetString(eventData.GetBytes());
            var rawModel = JsonConvert.DeserializeObject<RawBaseReadModel>(data);
            LogModel.MeterId = rawModel.METER_ID;
            LogModel.CustomerId = rawModel.CUSTOMER_ID;
            LogModel.ClientId = clientId;

            var validationResults = new List<ValidationResult>();
            var uomIsValid = true;
            try
            {
                var rawModelUomId = (int)Enums.GetValueFromDescription<Enums.UomType>(rawModel.UNIT_OF_MEASURE);
                rawModel.UNIT_OF_MEASURE = rawModelUomId.ToString();
            }
            catch (Exception)
            {
                uomIsValid = false;
            }

            if (uomIsValid && validationResults.Count == 0)
            {
                //Map Raw Model to Model
                var model = Mapper.Map<RawBaseReadModel, MeterReadModel>(rawModel);
                model.AccountNumber = "Not Needed";
                //Validate Model
                if (Validate(model, out validationResults))
                {
                    var readingTimezoneEnum =
                        (Enums.Timezone)Enum.Parse(typeof(Enums.Timezone), Convert.ToString(model.Timezone));
                    var readingTimezone = TimeZoneInfo.FindSystemTimeZoneById(readingTimezoneEnum.GetDescription());
                    var todaysDateInReadingTimeZone = TimeZoneInfo.ConvertTimeFromUtc(DateTime.UtcNow, readingTimezone);
                    var queueMessage =
                        $"{model.ClientId}^^{model.MeterId}^^{todaysDateInReadingTimeZone.Date.ToString("MM/dd/yyyy")}^^{LogModel.Source}^^{LogModel.RowIndex}^^{LogModel.Metadata}^^{Convert.ToInt32(LogModel.ProcessingType)}";
                    await AzureQueue.Value.AddMessageToCalculationQueue(queueMessage).ConfigureAwait(false);
                    return true;
                }
            }

            return false;
        }

        /// <summary>
        /// Method to validate MeterReadModel model
        /// </summary>
        /// <param name="model">MeterReadModel model</param>
        /// <param name="validationResuts">list of validation results as out param</param>
        /// <returns></returns>
        private bool Validate(MeterReadModel model, out List<ValidationResult> validationResuts)
        {
            return base.Validate(model, out validationResuts);
        }
    }
}