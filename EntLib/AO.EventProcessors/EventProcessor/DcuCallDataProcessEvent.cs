﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;
using System.Threading.Tasks;
using AO.BusinessContracts;
using AutoMapper;
using CE.AO.Logging;
using CE.AO.Models;
using CE.AO.Models.RawFileModels;
using Microsoft.ServiceBus.Messaging;
using Newtonsoft.Json;
using Enums = CE.AO.Utilities.Enums;

namespace AO.EventProcessors.EventProcessor
{
    /// <summary>
    /// Class for processing DcuCallData Event
    /// </summary>
    public class DcuCallDataProcessEvent : ProcessEvent
    {

        public override Enums.Module Module { get; } = Enums.Module.DcuCallDataEventProcess;
        public override Enums.EventType EventType { get; } = Enums.EventType.DcuCallData;

        private readonly Lazy<IDcuCallData> _dcuCallData;

        /// <summary>
        /// Parameterized contructor
        /// </summary>
        /// <param name="logModel">Used for logging details in case of any error</param>
        /// <param name="dcuCallData"></param>
        public DcuCallDataProcessEvent(LogModel logModel, Lazy<IDcuCallData> dcuCallData) : base(logModel)
        {
            _dcuCallData = dcuCallData;
        }

        /// <summary>
        /// Method to process DcuCallData Event
        /// </summary>
        /// <param name="eventData">DcuCallData event data</param>
        /// <param name="clientId">Unique Client Identifier</param>
        /// <returns></returns>
        public override async Task<bool> Process(EventData eventData, string clientId)
        {
            ClientId = clientId;

            var data = Encoding.UTF8.GetString(eventData.GetBytes());
            var rawModel = JsonConvert.DeserializeObject<RawDcuCallDataModel>(data);
			LogModel.ClientId = clientId;

	        var model = Mapper.Map<RawDcuCallDataModel, DcuCallDataModel>(rawModel);
            //Validate Model
            List<ValidationResult> validationResults;
            if (Validate(model, out validationResults))
            {
                await _dcuCallData.Value.InsertOrMergeDcuCallDataAsync(model).ConfigureAwait(false);
                return true;
            }

            var errorString = string.Empty;
            if (validationResults.Count > 0)
                errorString = $"Error: {JsonConvert.SerializeObject(validationResults)}.";
            Logger.Fatal($@"Event of type ""{Enums.EventType.DcuCallData}"" is not valid.{errorString}", LogModel);
            return false;
        }

        /// <summary>
        /// Method to validate DcuCallData model
        /// </summary>
        /// <param name="model">DcuCallData model</param>
        /// <param name="validationResuts">list of validation results as out param</param>
        /// <returns></returns>
        private bool Validate(DcuCallDataModel model, out List<ValidationResult> validationResuts)
        {
            var isValid = base.Validate(model, out validationResuts);
            if (model.ClientId != Convert.ToInt32(ClientId))
            {
                isValid = false;
                validationResuts.Add(new ValidationResult("ClientId is not valid.", new List<string> { "ClientId" }));
            }
            return isValid;
        }
    }
}