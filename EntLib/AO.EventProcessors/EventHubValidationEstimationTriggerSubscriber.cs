﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using System.Threading.Tasks;
using AO.BusinessContracts;
using CE.AO.Models;
using CE.AO.Utilities;
using Microsoft.Azure;
using Microsoft.ServiceBus.Messaging;
using Newtonsoft.Json;

namespace AO.EventProcessors
{
    /// <summary>
    /// To process the events in the event hub
    /// </summary>
    public class EventHubValidationEstimationTriggerSubscriber : ITriggerSubscriber, IEventProcessor
    {
        private EventHubReceiver _eventHubReceiver;
        private PartitionContext _partitionContext;
        private Lazy<EventProcessorFactory<EventHubValidationEstimationTriggerSubscriber>> EventProcessorFactory { get; }
        private LogModel LogModel { get; }

        public const string ValidationEstimationEventHubName = "ValidationEstimationEventHubName";
        public const string ConsumerGroupNameForValidationEstimationEventHub = "ConsumerGroupNameForValidationEstimationEventHub";
        public const string AclaraOneStorageConnectionString = "AclaraOneStorageConnectionString";
        public const string NumberOfPartitionsForValidationEstimationEventHub = "NumberOfPartitionsForValidationEstimationEventHub";
        public const string ValidationEstimationEventHubManageConnectionString = "ValidationEstimationEventHubManageConnectionString";

        /// <summary>
        /// Subscribes to event hub trigerring mechanism for validation and estimation process
        /// </summary>
        public void Subscribe()
        {
            //Event Hub
            //Get settings from configuration
            var importDataEventHubName = CloudConfigurationManager.GetSetting(ValidationEstimationEventHubName); //eventHubName
            var consumerGroupNameForImportDataEventHub = CloudConfigurationManager.GetSetting(ConsumerGroupNameForValidationEstimationEventHub);
            var numberOfPartitionsForValidationEstimationEventHub = int.Parse(CloudConfigurationManager.GetSetting(NumberOfPartitionsForValidationEstimationEventHub));
            var aclaraOneStorageConnectionString = CloudConfigurationManager.GetSetting(AclaraOneStorageConnectionString); // Required for checkpoint/state

            //Get AMQP connection string
            var connectionString = EventHubManager.GetServiceBusConnectionString(ValidationEstimationEventHubManageConnectionString);

            //Create event hub if it does not exist
            var namespaceManager = EventHubManager.GetNamespaceManager(connectionString);
            EventHubManager.CreateEventHubIfNotExists(importDataEventHubName, numberOfPartitionsForValidationEstimationEventHub, namespaceManager);

            //Create consumer group if it does not exist
            var group = string.IsNullOrWhiteSpace(consumerGroupNameForImportDataEventHub) ? null : namespaceManager.CreateConsumerGroupIfNotExists(importDataEventHubName, consumerGroupNameForImportDataEventHub);

            //Start processing messages
            _eventHubReceiver = new EventHubReceiver(importDataEventHubName, connectionString);

            //Get host name of worker role instance.  This is used for each environment to obtain
            //a lease, and to renew the same lease in case of a restart.            
            _eventHubReceiver.RegisterEventProcessor(group, aclaraOneStorageConnectionString, Guid.NewGuid().ToString(), EventProcessorFactory.Value);
        }

        /// <summary>
        /// Unsubscribes from event hub trigerring mechanism for validation and estimation process
        /// </summary>
        public void UnSubscribe()
        {
            _eventHubReceiver.UnregisterEventProcessor();
        }

        /// <summary>
        /// This method is called when Event Processor of a partition is opened
        /// </summary>
        /// <param name="context">Instance of Partition context</param>
        /// <returns>Void</returns>
        public async Task OpenAsync(PartitionContext context)
        {
            Trace.TraceInformation(
                $"EventProcessor OpenAsync.  Partition: '{context.Lease.PartitionId}', Offset: '{context.Lease.Offset}'");
            _partitionContext = context;
            await Task.CompletedTask.ConfigureAwait(false);
        }

        /// <summary>
        /// This method is called when Event Processor of a partition is closed
        /// </summary>
        /// <param name="context">Instance of Partition context</param>
        /// <param name="reason">Reason to close the connection</param>
        /// <returns>Void</returns>
        public async Task CloseAsync(PartitionContext context, CloseReason reason)
        {
            Trace.TraceWarning(
                $"EventProcessor CloseAsync.  Partition '{_partitionContext.Lease.PartitionId}', Reason: '{reason}'.");
            if (reason == CloseReason.Shutdown)
            {
                await context.CheckpointAsync().ConfigureAwait(false);
            }
        }

        /// <summary>
        /// To process the events in the event hub
        /// </summary>
        /// <param name="context">Context of the partition</param>
        /// <param name="events">List of EventData present in event hub</param>
        /// <returns>Void</returns>
        public async Task ProcessEventsAsync(PartitionContext context, IEnumerable<EventData> events)
        {
            try
            {
                foreach (var eventData in events)
                {
                    try
                    {
                        EventHubTriggerSubscriberHelper.CleanAllValues(LogModel);

                        var data = Encoding.UTF8.GetString(eventData.GetBytes());
                        var veeValidationEstimationTriggerDto = JsonConvert.DeserializeObject<VeeValidationEstimationTriggerDto>(data);

                        VeeFacade.Value.ExecuteValidationsAndEstimations(veeValidationEstimationTriggerDto);
                    }
                    catch (Exception ex)
                    {
                        Trace.TraceError("Error in processing: " + ex.Message);
                    }
                }
                await context.CheckpointAsync().ConfigureAwait(false);
            }
            catch (Exception ex)
            {
                Trace.TraceError("Error in processing: " + ex.Message);
            }
        }
    }
}
