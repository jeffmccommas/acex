﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AO.BusinessContracts;
using CE.AO.Logging;
using CE.AO.Models;
using CE.AO.Utilities;
using Microsoft.Azure;
using Enums = CE.AO.Utilities.Enums;
using Microsoft.ServiceBus.Messaging;
using Newtonsoft.Json;


namespace AO.EventProcessors
{
    /// <summary>
    /// This class represents the Event Hub subscriber for doing pre aggregations
    /// </summary>
    public class EventHubPreAggCalculationTriggerSubscriber : ITriggerSubscriber, IEventProcessor
    {
        private PartitionContext _partitionContext;
        private EventHubReceiver _eventHubReceiver;
        private const string PreAggCalculationEventHubName = "PreAggCalculationEventHubName";
        private const string ConsumerGroupNameForPreAggCalculationEventHub = "ConsumerGroupNameForPreAggCalculationEventHub";
        private const string NumberOfPartitionsForPreAggCalculationEventHub = "NumberOfPartitionsForPreAggCalculationEventHub";
        private const string PreAggCalculationEventHubManageConnectionString = "PreAggCalculationEventHubManageConnectionString";
        private const string AclaraOneStorageConnectionString = "AclaraOneStorageConnectionString";
        private Lazy<IPreAggCalculationFacade> PreAggCalculationFacade { get; }
        private Lazy<EventProcessorFactory<EventHubPreAggCalculationTriggerSubscriber>> EventProcessorFactory { get; }
        private LogModel LogModel { get; }

        public EventHubPreAggCalculationTriggerSubscriber(LogModel logModel, Lazy<IPreAggCalculationFacade> preAggCalculationFacade, Lazy<EventProcessorFactory<EventHubPreAggCalculationTriggerSubscriber>> eventProcessorFactory)
        {
            PreAggCalculationFacade = preAggCalculationFacade;
            EventProcessorFactory = eventProcessorFactory;
            LogModel = logModel;
        }

        /// <summary>
        /// Subscribes to event hub trigerring mechanism for doing pre aggregations
        /// </summary>
        public void Subscribe()
        {
            //Event Hub
            //Get settings from configuration
            var importDataEventHubName = CloudConfigurationManager.GetSetting(PreAggCalculationEventHubName); //eventHubName
            var consumerGroupNameForImportDataEventHub = CloudConfigurationManager.GetSetting(ConsumerGroupNameForPreAggCalculationEventHub);
            var numberOfPartitionsForImportDataEventHub =
                int.Parse(CloudConfigurationManager.GetSetting(NumberOfPartitionsForPreAggCalculationEventHub));
            var aclaraOneStorageConnectionString = CloudConfigurationManager.GetSetting(AclaraOneStorageConnectionString); // Required for checkpoint/state            
                                                                                                                           //Get AMQP connection string
            var connectionString = EventHubManager.GetServiceBusConnectionString(PreAggCalculationEventHubManageConnectionString);

            var group = EventHubManager.CreateEventHubIfNotExists(importDataEventHubName,
                numberOfPartitionsForImportDataEventHub, PreAggCalculationEventHubManageConnectionString,
                consumerGroupNameForImportDataEventHub);

            //Start processing messages
            _eventHubReceiver = new EventHubReceiver(importDataEventHubName, connectionString);

            //Get host name of worker role instance.  This is used for each environment to obtain
            //a lease, and to renew the same lease in case of a restart.            
            _eventHubReceiver.RegisterEventProcessor(group, aclaraOneStorageConnectionString, Guid.NewGuid().ToString(), EventProcessorFactory.Value);
        }

        /// <summary>
        /// Unsubscribes from event hub trigerring mechanism for doing pre aggregations
        /// </summary>
        public void UnSubscribe()
        {
            //Unregister the event processor so other instances will handle the partitions
            _eventHubReceiver.UnregisterEventProcessor();
        }

        /// <summary>
        /// This method is called when Event Processor of a partition is opened
        /// </summary>
        /// <param name="context">Instance of Partition context</param>
        /// <returns>Void</returns>
        public async Task OpenAsync(PartitionContext context)
        {
            Trace.TraceInformation(
                $"EventProcessor OpenAsync.  Partition: '{context.Lease.PartitionId}', Offset: '{context.Lease.Offset}'");
            _partitionContext = context;
            await Task.CompletedTask.ConfigureAwait(false);
        }

        /// <summary>
        /// This method is called when Event Processor of a partition is closed
        /// </summary>
        /// <param name="context">Instance of Partition context</param>
        /// <param name="reason">Reason to close the connection</param>
        /// <returns>Void</returns>
        public async Task CloseAsync(PartitionContext context, CloseReason reason)
        {
            Trace.TraceWarning(
                $"EventProcessor CloseAsync.  Partition '{_partitionContext.Lease.PartitionId}', Reason: '{reason}'.");
            if (reason == CloseReason.Shutdown)
            {
                await context.CheckpointAsync().ConfigureAwait(false);
            }
        }

        /// <summary>
        /// To process the event in the event hub
        /// </summary>
        /// <param name="context">Context of the partition</param>
        /// <param name="events">List of EventData present in event hub</param>
        /// <returns>Void</returns>
        public async Task ProcessEventsAsync(PartitionContext context, IEnumerable<EventData> events)
        {
            try
            {
                foreach (var eventData in events)
                {
                    try
                    {
                        EventHubTriggerSubscriberHelper.CleanAllValues(LogModel);
                        PopulateLogModel(eventData, LogModel);

                        var model = JsonConvert.DeserializeObject<AccountPreAggCalculationRequestModel>(LogModel.Metadata);
                        LogModel.ClientId = Convert.ToString(model.ClientId);
                        LogModel.AccountId = model.AccountId;

                        //Validate Model
                        List<ValidationResult> validationResults;

                        if (Validate(model, out validationResults))
                        {
                            await PreAggCalculationFacade.Value.CalculateAccountPreAgg(model);
                            break;
                        }

                        var errorString = string.Empty;
                        if (validationResults.Count > 0)
                            errorString = $"Error: {JsonConvert.SerializeObject(validationResults)}.";
                        Logger.Fatal($@"Pre aggregations cannot be performed.{errorString}", LogModel);

                    }
                    catch (Exception ex)
                    {
                        Logger.Error(ex, LogModel);
                    }
                }
                await context.CheckpointAsync().ConfigureAwait(false);
            }
            catch (Exception ex)
            {
                Logger.Error("Error in processing: " + ex.Message, ex, LogModel);
            }
        }

        /// <summary>
        /// Populate Log Model for Pre aggregation calculation
        /// </summary>        
        private void PopulateLogModel(EventData eventData, LogModel logModel)
        {
            KeyValuePair<string, object> source = eventData.Properties.FirstOrDefault(p => p.Key == "Source");
            KeyValuePair<string, object> processingType = eventData.Properties.FirstOrDefault(p => p.Key == "ProcessingType");
            KeyValuePair<string, object> rowIndex = eventData.Properties.FirstOrDefault(p => p.Key == "RowIndex");

            logModel.Source = Convert.ToString(source.Value);
            logModel.ProcessingType = processingType.Value != null
                ? (Enums.ProcessingType)
                    Enum.Parse(typeof(Enums.ProcessingType), Convert.ToString(processingType.Value))
                : Enums.ProcessingType.None;
            var data = Encoding.UTF8.GetString(eventData.GetBytes());
            logModel.Metadata = data;
            logModel.RowIndex = Convert.ToString(rowIndex.Value);
            logModel.Module = Enums.Module.PreAggCalculation;
        }

        /// <summary>
        /// Method to validate AccountPreAggModel model
        /// </summary>
        /// <param name="calculationRequestModel">AccountPreAggModel model</param>
        /// <param name="validationResuts">list of validation results as out param</param>
        /// <returns></returns>
        private bool Validate(AccountPreAggCalculationRequestModel calculationRequestModel, out List<ValidationResult> validationResuts)
        {
            var isValid = Utilities.Validate(calculationRequestModel, out validationResuts);
            return isValid;
        }
    }
}