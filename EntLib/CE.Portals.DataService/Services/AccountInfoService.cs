﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CE.Portals.DataService.Interfaces;
using CE.Portals.DataService.Models;

namespace CE.Portals.DataService.Services
{
    public class AccountInfoService : IAccountInfoService
    {
        public List<InsightAccountInfo> FindAccount(string accountId, string premiseId = "")
        {
            var retVal = new List<InsightAccountInfo>
            {
                new InsightAccountInfo
                {
                    CustomerName = "test customer1",
                    InsightPremiseId = "1234567",
                    InsightAccountId = "98765432"
                },
                new InsightAccountInfo
                {
                    CustomerName = "test customer2",
                    InsightPremiseId = "44554455",
                    InsightAccountId = "3334455677"
                }
            };

            return retVal;
        }

        public bool AddMap(InsightAccountInfo insightAccountInfo, string accountId, string premiseId = "")
        {
            throw new NotImplementedException();
        }

        public bool RemoveMap(InsightAccountInfo insightAccountInfo, string accountId, string premiseId = "")
        {
            throw new NotImplementedException();
        }
    }
}
