﻿using System.Collections.Generic;
using CE.Portals.DataService.Interfaces;
using System.Linq;
using System;
using CE.InsightDataAccess.InsightModels;

namespace CE.Portals.DataService.Services
{
    public class EnergyStarServices : IEnergyStarServices
    {
        private readonly InsightsContext _insightsContext;
        public EnergyStarServices(InsightsContext insightsContext)
        {
            _insightsContext = insightsContext;

        }

        public List<AcceptedMeters> GetAcceptedMeterIds(int ClientId)
        {
           var acceptedMeters = (from u in _insightsContext.AcceptedMeters
                                  where u.ClientiD == ClientId
                                  where u.IsValid == true
                                  select new { u.MeterId}).ToArray();

            var acceptedMeterIds = new List<AcceptedMeters>();

            foreach (var meter in acceptedMeters)
            {
                acceptedMeterIds.Add(new AcceptedMeters() { MeterId = meter.MeterId});
            }
            return acceptedMeterIds;

        }

        public void AddAcceptedMeters(AcceptedMeters meter)
        {
            try
            {
                AcceptedMeters metertoInsert = new AcceptedMeters()
                {
                    MeterId = meter.MeterId,
                    ClientiD = meter.ClientiD,
                    IsValid = meter.IsValid,
                    ModifiedBy = meter.ModifiedBy,
                    ModifiedDate = meter.ModifiedDate,
                    UpdateBy = meter.UpdateBy,
                    UpdatedDate = meter.UpdatedDate
                };

                _insightsContext.AcceptedMeters.Add(metertoInsert);
                _insightsContext.SaveChanges();
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
        }
    }
}
