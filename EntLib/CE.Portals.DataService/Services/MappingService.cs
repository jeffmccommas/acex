﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection.Metadata.Ecma335;
using CE.InsightDataAccess.InsightModels;
using CE.InsightDataAccess.InsightsDWModels;
using CE.Portals.DataService.Interfaces;
using CE.Portals.DataService.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using Microsoft.EntityFrameworkCore;

namespace CE.Portals.DataService.Services
{
    public class MappingService : IMappingService
    {

        private readonly InsightsContext _insightContext;
        private readonly InsightsDWContext _insightDwContext;

        public MappingService(InsightsContext insightsContext, InsightsDWContext insightsDwContext)
        {
            _insightContext = insightsContext;
            _insightDwContext = insightsDwContext;

        }

        public string GetMappedData(string sharDbName)
        {

            _insightDwContext.shardDbName = sharDbName;
            var value = from u in _insightDwContext.DimCustomer
                select u.LastName;

            return value.ToString();

        }

        public MappedCustomer GetMappedCustomerDetails(string shardDbName, string accountId, string premiseId)
        {

            _insightDwContext.shardDbName = shardDbName;
            string firstName = String.Empty, lastName = String.Empty;
            var valueAddrrss = (from c in _insightDwContext.DimCustomer
                join fcp in _insightDwContext.FactCustomerPremise on c.CustomerKey equals fcp.CustomerKey
                join p in _insightDwContext.DimPremise on fcp.PremiseKey equals p.PremiseKey
                where p.AccountId == accountId
                select new
                {
                    c.FirstName,
                    c.LastName,
                    //c.Street1,
                    //c.Street2,
                    //c.City,
                    //c.StateProvince,
                    //c.Country,
                    //c.PostalCode
                }).FirstOrDefault();

            if (valueAddrrss != null)
            {
                firstName = valueAddrrss.FirstName;
                lastName = valueAddrrss.LastName;
            }

            if (!string.IsNullOrEmpty(premiseId))
            {
                var childName = (from fspbd in _insightDwContext.FactServicePointBilling_BillCostDetails
                    join fsp in _insightDwContext.FactServicePointBilling
                    on fspbd.BillingCostDetailsKey equals fsp.BillingCostDetailsKey
                    where fsp.AccountId == accountId
                          && fsp.PremiseId == premiseId
                          && (fspbd.BillingCostDetailName == "ChildFirstName" ||
                              fspbd.BillingCostDetailName == "ChildLastName")
                    select new
                    {
                        fspbd.BillingCostDetailName,
                        fspbd.BillingCostDetailValue    
                    });

                if (childName.Count() > 1)
                {
                    if (childName.Any(x => x.BillingCostDetailName == "ChildFirstName"))
                    {
                        firstName = childName.Where(x => x.BillingCostDetailName == "ChildFirstName")
                            .Select(a => a.BillingCostDetailValue ?? "").First();
                        lastName = "";
                    }
                    if (childName.Any(x => x.BillingCostDetailName == "ChildLastName"))
                    {
                        lastName = childName.Where(x => x.BillingCostDetailName == "ChildLastName")
                            .Select(a => a.BillingCostDetailValue ?? "").First();
                    }
                }

                var value = (from p in _insightDwContext.DimPremise

                    where p.PremiseId == premiseId && p.AccountId == accountId
                    select new
                    {
                        p.Street1,
                        p.Street2,
                        p.City,
                        p.StateProvince,
                        p.Country,
                        p.PostalCode,

                    }).FirstOrDefault();
                if (value != null)
                {
                    return new MappedCustomer()
                    {
                        
                        FirstName = firstName,
                        LastName = lastName,
                        Street1 = value.Street1,
                        Street2 = value.Street2,
                        City = value.City,
                        Country = value.Country,
                        PostalCode = value.PostalCode,
                        StateProvince = value.StateProvince

                    };
                }
            }
            else if (valueAddrrss != null)
            {
                return new MappedCustomer()
                {
                    FirstName = valueAddrrss.FirstName,
                    LastName = valueAddrrss.LastName
                };
            }
            return new MappedCustomer() { Error = true };

        }

        public Boolean CheckMapping(string shardDbName, string accountId, string premiseId)
        {
            _insightDwContext.shardDbName = shardDbName;
            bool mappingExists = _insightDwContext.DimPremise
                .Any(map => map.AccountId.Equals(accountId)
                            && map.PremiseId.Equals((premiseId.Length > 0) ? premiseId : map.PremiseId));
            return mappingExists;

        }

        public void AddMapping(string shardDbName, Espmmap espmMap)
        {
            _insightDwContext.shardDbName = shardDbName;
            using (var dbContextTransaction = _insightDwContext.Database.BeginTransaction())
            {
                List<EspmMapDetails> espmMapDetailsInsert = new List<EspmMapDetails>();
                List<EspmMapDetails> espmMapDetailsUpdates = new List<EspmMapDetails>();
                bool newMap = false;
                try
                {
                    if (espmMap.MapId == null && espmMap.EspmMapDetails.Count > 0)
                    {
                        _insightDwContext.Espmmap.Add(espmMap);
                        _insightDwContext.SaveChanges();
                        newMap = true;
                    }
                    else
                    {
                        _insightDwContext.Espmmap.Update(espmMap);
                        _insightDwContext.SaveChanges();
                    }

                    
                    if (espmMap.EspmMapDetails.Count >= 1)
                    {

                        foreach (EspmMapDetails mapDetails in espmMap.EspmMapDetails)
                        {
                            mapDetails.MapId = espmMap.MapId;
                            if (_insightDwContext.EspmMapDetails.Any(x => x.MapDetailId == mapDetails.MapDetailId))
                            {
                                espmMapDetailsUpdates.Add(mapDetails);
                            }

                            else
                            {
                                mapDetails.MapDetailId = null;
                                espmMapDetailsInsert.Add(mapDetails);
                            }
                        }
                        if (!newMap)
                        {
                            var deleteMappingDetails = (from a in _insightDwContext.EspmMapDetails
                                where a.MapId == espmMap.MapId
                                select a);
                            _insightDwContext.EspmMapDetails.RemoveRange(deleteMappingDetails);
                        }
                        _insightDwContext.EspmMapDetails.AddRange(espmMapDetailsInsert);
                        _insightDwContext.EspmMapDetails.UpdateRange(espmMapDetailsUpdates);
                    }
                    else
                    {
                        var deleteMappingDetails = (from a in _insightDwContext.EspmMapDetails
                            where a.MapId == espmMap.MapId
                            select a);
                        _insightDwContext.EspmMapDetails.RemoveRange(deleteMappingDetails);

                    }
                    _insightDwContext.SaveChanges();
                    dbContextTransaction.Commit();
                }
                catch (Exception ex)
                {
                    dbContextTransaction.Rollback();
                    throw;
                }

            }

        }

        public bool IsMapped(string shardDbName, string meterId, string accountId, string premiseId, string meterType, int loginId)
        {
            _insightDwContext.shardDbName = shardDbName;
            Boolean isMappedInEspm = false;
            Boolean isNewMap = false;
            var isMappedInPremise = _insightDwContext.DimPremise.Any(p => p.AccountId == accountId &&
                                                                          p.PremiseId ==
                                                                          (premiseId ?? p.PremiseId));
            var esPmMapDetail = _insightDwContext.Espmmap.LastOrDefault(p => p.MeterId == meterId);
            if (esPmMapDetail != null)
            {
                isMappedInEspm =
                    _insightDwContext.EspmMapDetails.Any(p => p.MapId == esPmMapDetail.MapId);

                isNewMap = (esPmMapDetail.AccountId != accountId) || (esPmMapDetail.PremiseId != premiseId);
            }
            if (isNewMap)
            {
                _insightDwContext.EspmMapDetails.RemoveRange();
            }
            
            if (isMappedInPremise && !isMappedInEspm && esPmMapDetail == null)
            {
                var espmMapNew = new Espmmap()
                {
                    AccountId = accountId,
                    PremiseId = premiseId,
                    MeterId = meterId,
                    UpdatedDate = DateTime.Now,
                    UpdatedBy = loginId,
                    MeterType = meterType
                };
                _insightDwContext.Espmmap.Add(espmMapNew);

                _insightDwContext.EspmMapDetails.Add(new EspmMapDetails()
                {
                    MapId = espmMapNew.MapId,
                    MapPremiseId = premiseId,
                    MapAccountId = accountId,
                    Enabled = true,
                    UpdatedDate = DateTime.Now,
                    UpdatedBy = loginId
                });
                _insightDwContext.SaveChanges();
                isMappedInEspm = true;
            }
            return isMappedInEspm;
        }



        public Espmmap GetMappedMeters(string shardDbName, string accountId, string premiseId, string meterId)
        {
            _insightDwContext.shardDbName = shardDbName;
            //if (accountId != null)
            //{
                var espmMap = (from map in _insightDwContext.Espmmap
                    where map.MeterId == meterId
                    select map).LastOrDefault();

                if (espmMap != null)
                {
                    espmMap.EspmMapDetails = (from mapDetails in _insightDwContext.EspmMapDetails
                        where mapDetails.MapId == espmMap.MapId
                        select mapDetails).ToList();

                    foreach (var espmMapEspmMapDetail in espmMap.EspmMapDetails)
                    {
                        if (espmMapEspmMapDetail.MapPremiseId == null)
                        {
                            espmMapEspmMapDetail.MapPremiseId = "";}
                    }
                }
                else
                {

                    return GetMappedMetersFromDimPremise(shardDbName, accountId, premiseId);
                }

                return espmMap;
            //}
            //else
            //{
            //    return new Espmmap();
            //}




        }

        public Espmmap GetMappedMetersFromDimPremise(string shardDbName, string accountId, string premiseId)
        {
            _insightDwContext.shardDbName = shardDbName;
            var mappedData = new Espmmap
            {
                AccountId = accountId,
                PremiseId = premiseId,
                EspmMapDetails = new List<EspmMapDetails>()
            };
            var espmMap = (from map in _insightDwContext.DimPremise
                where map.AccountId == accountId
                      && map.PremiseId == (premiseId ?? map.PremiseId)
                select map);
            if (!espmMap.Any()) return mappedData;

            foreach (DimPremise map in espmMap)
            {
                mappedData.EspmMapDetails.Add(
                    new EspmMapDetails() { MapAccountId = map.AccountId, MapPremiseId = map.PremiseId });
            }

            return mappedData;


        }

        public Boolean UpdateMeterMappingonAccept(string shardDbName, string accountId, string premiseId, int userId)
        {
            try
            {
                _insightDwContext.shardDbName = shardDbName;
                var espmmap = (from map in _insightDwContext.Espmmap
                    where map.AccountId == accountId && map.PremiseId == (premiseId ?? map.PremiseId)
                    select map).FirstOrDefault();
                var espmapdetails = (from m in _insightDwContext.EspmMapDetails
                    where m.MapId == espmmap.MapId
                    select m).ToList();
                foreach (EspmMapDetails map in espmapdetails)
                {
                    if (!_insightDwContext.DimPremise.Any(
                        p => p.AccountId == map.MapAccountId &&
                             p.PremiseId == (map.MapPremiseId ?? p.PremiseId))) continue;
                    map.Enabled = true;
                    map.UpdatedDate = DateTime.Now;
                    map.UpdatedBy = userId;
                    //espmmap.UpdatedBy = userId;
                    //espmmap.UpdatedDate = DateTime.Now;
                }
                //_insightDwContext.Espmmap.Update(espmmap);
                _insightDwContext.EspmMapDetails.UpdateRange(espmapdetails);
                _insightDwContext.SaveChanges();
                return true;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }


        public Boolean UpdateCustomerId(string shardDbName, string meterId)
        {

            try
            {
                _insightDwContext.shardDbName = shardDbName;


                var espmMapDetails = (from mapDetails in _insightDwContext.EspmMapDetails
                    join map in _insightDwContext.Espmmap
                    on mapDetails.MapId equals map.MapId
                    where map.MeterId == meterId
                    select mapDetails).ToList();
                if (espmMapDetails.Any())
                {
                    foreach (EspmMapDetails mapdetails in espmMapDetails)
                    {
                        mapdetails.CustomerId = (from c in _insightDwContext.DimCustomer
                            join fcp in _insightDwContext.FactCustomerPremise on c.CustomerKey equals fcp.CustomerKey
                            join p in _insightDwContext.DimPremise on fcp.PremiseKey equals p.PremiseKey
                            where p.AccountId == mapdetails.MapAccountId
                            select c.CustomerId).FirstOrDefault();

                        mapdetails.UpdatedDate = DateTime.Now;
                    }


                    _insightDwContext.SaveChanges();
                }

                return true;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }

        }

        public List<BillingMeterMaps> GetMappedAccountForMeters(string shardDbName, string meterId)
        {
            try
            {
                _insightDwContext.shardDbName = shardDbName;
                List<BillingMeterMaps> billingMeterMaps = new List<BillingMeterMaps>();

                var espmMapDetails = (from mapDetails in _insightDwContext.EspmMapDetails
                                      join map in _insightDwContext.Espmmap
                                      on mapDetails.MapId equals map.MapId
                                      where map.MeterId == meterId
                                      select mapDetails).ToList();

                var meterType = (from map in _insightDwContext.Espmmap
                    where map.MeterId == meterId
                    select map.MeterType).LastOrDefault();
                espmMapDetails.ForEach(x =>
                    {
                        billingMeterMaps.Add(new BillingMeterMaps()
                        {
                            MeterType = meterType,
                            MapAccountId = x.MapAccountId,
                            MapPremiseId = x.MapPremiseId,
                            CustomerId = x.CustomerId,
                            Enabled = x.Enabled,
                            MapDetailId = x.MapDetailId,
                            MapId = x.MapId,
                            UpdatedBy = x.UpdatedBy,
                            UpdatedDate = x.UpdatedDate
                        });
                    })
                ;
                return billingMeterMaps;

            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }

        }

        public Boolean AddMeterBillignData(string shardDbName, List<MeterBillingData> meterBillignData)
        {
            _insightDwContext.shardDbName = shardDbName;
            using (var dbContextTransaction = _insightDwContext.Database.BeginTransaction())
            {
                try
                {
                   
                    _insightDwContext.MeterBillingData.AddRange(meterBillignData);
                    _insightDwContext.SaveChanges();
                    dbContextTransaction.Commit();

                    return true;
                }
                catch (Exception e)
                {
                    Console.WriteLine(e);
                    dbContextTransaction.Rollback();
                    throw;
                }
            }
        }


        public Boolean DeleteBillingDataWebJob(string shardDbName)
        {
            _insightDwContext.shardDbName = shardDbName;
            using (var dbContextTransaction = _insightDwContext.Database.BeginTransaction())
            {
                try
                {

                    _insightDwContext.Database.ExecuteSqlCommand("truncate table portal.MeterBillingData");
                    _insightDwContext.SaveChanges();
                    dbContextTransaction.Commit();

                    return true;
                }
                catch (Exception e)
                {
                    Console.WriteLine(e);
                    dbContextTransaction.Rollback();
                    throw;
                }
            }
        }

        public List<MeterBillingData> GetAllMeterBillingforMeter(string shardDbName, string meterId, string startdate, string endDate)
        {
            _insightDwContext.shardDbName = shardDbName;
                try { 
                    return (from p in _insightDwContext.MeterBillingData
                            where p.Enddate > DateTime.Parse(startdate) 
                            && p.Enddate <= DateTime.Parse(endDate)
                            && p.MeterId == meterId select p).OrderByDescending(x => x.Enddate).ToList();
                    }
                catch (Exception e)
                {
                    Console.WriteLine(e);
                    throw;
                }
            

        }

        public List<string> GetAllMeterIds(string shardDbName)
        {
            _insightDwContext.shardDbName = shardDbName;
            try
            {
                return _insightDwContext.MeterBillingData.Select(x => x.MeterId).Distinct().ToList();
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }


        }

        public Boolean DoesWhiteListHasAnyRecords(string shardDbName)
        {
            _insightDwContext.shardDbName = shardDbName;
            try
            {
                return _insightDwContext.EspmMetersWhiteList.Any();

            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
            
        }
        public Boolean CheckIfMeterIsInWhitelist(string shardDbName, string meterId)
        {
            _insightDwContext.shardDbName = shardDbName;
            try
            {
                return _insightDwContext.EspmMetersWhiteList.Any(x => x.MeterId == meterId);

            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }


        }


    }

}