﻿using System.Linq;
using CE.Portals.DataService.Interfaces;
using CE.InsightDataAccess.InsightModels;
using CE.InsightDataAccess.InsightsMetaDataModels;

namespace CE.Portals.DataService.Services
{
    public class ConfigurationService : IConfigurationService
    {
        private readonly InsightsContext _insightsContext;
        private readonly InsightsMetaDataContext _insightsMetaDataContext;
        public ConfigurationService(InsightsContext insightsContext, InsightsMetaDataContext insightsMetaDataContext)
        {
            _insightsContext = insightsContext;
            _insightsMetaDataContext = insightsMetaDataContext;

        }

        public string GetDwDatabaseConfigValue(int clientId, string environmentKey, string configurationKey)
        {
            var dwDdatabaseName = (from config in _insightsMetaDataContext.ClientConfiguration
                where config.ClientId == clientId
                      && config.EnvironmentKey == environmentKey
                      && config.ConfigurationKey == configurationKey
                select config.Value).FirstOrDefault();

            return dwDdatabaseName;
        }

        
    }
}
