﻿using CE.Portals.DataService.Interfaces;
using System;
using CE.Portals.Integrations.Security;
using System.Linq;
using System.Collections.Generic;
using CE.InsightDataAccess.InsightModels;
using CE.Portals.DataService.Models;
using CE.Portals.Integrations.Common;
using Microsoft.EntityFrameworkCore;

namespace CE.Portals.DataService.Services
{
    public class LoginService : ILoginService
    {
        private readonly InsightsContext _insightsContext;
        public LoginService(InsightsContext insightsContext)
        {
            _insightsContext = insightsContext;

        }
        //public User GetUsersecretKeys(string clientId, string environment)
        //{
        //    throw new NotImplementedException();
        //}
        private void UpdateFailedAttemptCount(int userId, int failedLoginAttemptCount)
        {
            var loginUsers = (from u in _insightsContext.PortalLogin
                              where u.UserId == userId
                              select u).SingleOrDefault();
            loginUsers.FailedLoginCount = failedLoginAttemptCount;
            int maxInvalidLogins = int.Parse(GetClientAppsettings(loginUsers.ClientId.ToString(), ApplicationConstants.MAXINCORRECTLOGINS));
            if (failedLoginAttemptCount >= maxInvalidLogins)
            {
                loginUsers.Enabled = false;
            }
            //else
            //{
            //    loginUsers.Enabled = loginUsers.Enabled == false ? false : true;
            //}
            _insightsContext.SaveChanges();
        }

        public string CheckDbConnection()
        {
            try
            {
                var connection = _insightsContext.Database.GetDbConnection();
                connection.Open();
                connection.Close();
                return "Connection Successful";
            }

            catch (System.Data.SqlClient.SqlException sqlExp)
            {
                return sqlExp.Message;
            }
        }
        public UserDetails ValidateESPMUser(string userName, string password)
        {


            string decodedUserName = System.Text.Encoding.UTF8.GetString(Convert.FromBase64String(userName));
            string decodedPassword = System.Text.Encoding.UTF8.GetString(Convert.FromBase64String(password));

            var loginUsers = (from u in _insightsContext.PortalLogin
                              where u.Username == decodedUserName
                              select new
                              {
                                  u.Username,
                                  u.PasswordSalt,
                                  u.PasswordHash,
                                  u.UserId,
                                  u.ClientId,
                                  u.FailedLoginCount,
                                  u.Enabled,
                                  u.FirstName,
                                  u.LastName,
                                  u.EnvId
                              }).FirstOrDefault();

            if (loginUsers == null) return null;
            var encryptor = new Encryptor(decodedPassword);

            if (!encryptor.VerifyHash(decodedPassword, loginUsers.PasswordSalt, loginUsers.PasswordHash))
            {
                UpdateFailedAttemptCount(loginUsers.UserId, loginUsers.FailedLoginCount + 1);
                return new UserDetails()
                {
                    FailedLoginAttempts = loginUsers.FailedLoginCount,
                    Enabled = loginUsers.Enabled ?? false,
                    ClientId = loginUsers.ClientId.ToString()
                };
            }

            else if (loginUsers.Enabled == false)
            {
                return new UserDetails()
                {
                    FailedLoginAttempts = loginUsers.FailedLoginCount,
                    Enabled = loginUsers.Enabled ?? false,
                    ClientId = loginUsers.ClientId.ToString()
                };
            }
            else
            {
                UpdateFailedAttemptCount(loginUsers.UserId, 0);
                return new UserDetails()
                {
                    ClientId = loginUsers.ClientId.ToString(),
                    UserId = loginUsers.UserId.ToString(),
                    FailedLoginAttempts = 0,
                    UserName = loginUsers.Username,
                    FirstName = loginUsers.FirstName,
                    LastName = loginUsers.LastName

                };
            }
        }

        public UserDetails GetInsightsUserCreds(int clientId, string apiUserType, int envId)
        {
            var loginUsers = (from ue in _insightsContext.UserEnvironment
                              join u in _insightsContext.User on ue.UserId equals u.UserId
                              where u.ClientId == clientId && u.ActorName == apiUserType && ue.EnvId == envId
                              select new { ue.CesecretAccessKey, u.CeaccessKeyId }).FirstOrDefault();

            if (loginUsers == null) return null;
            else
                return new UserDetails() { ClientId = clientId.ToString(), CeAccessKeyId = loginUsers.CeaccessKeyId.ToString(), CeSecretAccessKey = loginUsers.CesecretAccessKey.ToString() };
        }


        //public List<UserPortalList> GetAllPortals(string userId, string clientId)
        //{

        //    var portalList = (from p in _insightsContext.Portals
        //                      join up in _insightsContext.UserPortal on p.PortalId equals up.PortalId
        //                      join u in _insightsContext.PortalLogin on up.UserId equals u.UserId
        //                      where up.UserId == Int32.Parse(userId)
        //                      where u.ClientId == Int32.Parse(clientId)
        //                      where u.Enabled == true
        //                      select new { p.PortalId, p.PortalName, p.PortalComponent, up.IsDefault });
        //    var userPortalList = new List<UserPortalList>();

        //    foreach (var portal in portalList)
        //    {
        //        userPortalList.Add(new UserPortalList() { PortalId = portal.PortalId, PortalName = portal.PortalName, IsDefault = portal.IsDefault, PortalComponent = portal.PortalComponent });
        //    }
        //    //userPortalList.Add(new UserPortalList() { IsDefault = true, PortalComponent = "BenchMark", PortalName = "Energy Star", PortalId = 1 });
        //    return userPortalList;
        //}

        public string GetClientESPMSecretKeys(string clientId)
        {
            var secretKey = (from cpc in _insightsContext.ClientPortalConfig
                             join pc in _insightsContext.PortalConfig on cpc.PortalConfigId equals pc.PortalConfigId
                             where pc.IsEnabled == true
                             where cpc.IsEnabled == true
                             where pc.PortalConfigName == ApplicationConstants.ESPMSecterKey
                             where cpc.ClientId == Int32.Parse(clientId)
                             select new { cpc.PortalConfigValue });
            if (secretKey.Any())
            {
                return secretKey.FirstOrDefault().PortalConfigValue;
            }
            else
            { return null; }
        }

        public string GetClientAppsettings(string clientId, string appSettingKey)
        {
            var secretKey = (from cpc in _insightsContext.ClientPortalConfig
                             join pc in _insightsContext.PortalConfig on cpc.PortalConfigId equals pc.PortalConfigId
                             where pc.IsEnabled == true
                             where cpc.IsEnabled == true
                             where pc.PortalConfigName == appSettingKey
                             where cpc.ClientId == Int32.Parse(clientId)
                             select new { cpc.PortalConfigValue });
            if (secretKey.Any())
            {
                return secretKey.FirstOrDefault().PortalConfigValue;
            }
            else
            { return null; }
        }

        public UserDetails GetUserDetails(int userId)
        {
            var userDetails = (from p in _insightsContext.PortalLogin
                               join c in _insightsContext.Client on p.ClientId equals c.ClientId
                               where p.UserId == userId
                               select new
                               {
                                   p.FirstName,
                                   p.LastName,
                                   p.UserType,
                                   p.Enabled,
                                   c.Description,
                                   c.Name,
                                   p.Username,
                                   p.FailedLoginCount,
                                   p.UserId,
                                   p.ClientId
                               }).FirstOrDefault();

            return new UserDetails()
            {
                FirstName = userDetails.FirstName,
                LastName = userDetails.LastName,
                IsAdminUser = (userDetails.UserType == "admin"),
                Enabled = userDetails.Enabled ?? false,
                ClientDescription = userDetails.Description,
                ClientName = userDetails.Name,
                FailedLoginAttempts = userDetails.FailedLoginCount,
                UserName = userDetails.Username,
                UserId = userDetails.UserId.ToString(),
                ClientId = userDetails.ClientId.ToString()

            };

        }

        public void AddNewUser(UserDetails userDetails, int enVironmentId)
        {
            var encryptor = new Encryptor(userDetails.PassWord);
            var salt = encryptor.GenerateSalt();

            PortalLogin portalLogin = new PortalLogin()
            {
                ClientId = int.Parse(userDetails.ClientId),
                Username = userDetails.UserName,
                FirstName = userDetails.FirstName,
                LastName = userDetails.LastName,
                PasswordSalt = salt,
                PasswordHash = encryptor.GenerateHashWithSalt(userDetails.PassWord, salt),
                UpdateDate = DateTime.UtcNow,
                EnvId = Byte.Parse(enVironmentId.ToString()),
                UserType = userDetails.IsAdminUser == true ? "admin" : "std",
                Enabled = userDetails.Enabled,
                FailedLoginCount = 0

            };

            _insightsContext.PortalLogin.Add(portalLogin);
            _insightsContext.SaveChanges();


        }

        public Boolean CheckIfUserExists(string userName)
        {
            var loggedinuserDetails = (from p in _insightsContext.PortalLogin
                                       where p.Username == userName
                                       select new { p }).FirstOrDefault();

            return loggedinuserDetails != null;
        }

        public void UpdateUser(UserDetails userDetails, Boolean status)
        {
            var loginUsers = (from u in _insightsContext.PortalLogin
                              where u.UserId == int.Parse(userDetails.UserId)
                              select u).SingleOrDefault();
            if (!status)
            {



                loginUsers.FirstName = userDetails.FirstName;
                loginUsers.LastName = userDetails.LastName;
                loginUsers.Username = userDetails.UserName;
                if (userDetails.PassWord != null)
                {
                    var salt = new Encryptor(userDetails.PassWord).GenerateSalt();
                    loginUsers.PasswordSalt = salt;
                    loginUsers.PasswordHash = new Encryptor(userDetails.PassWord).GenerateHashWithSalt(userDetails.PassWord, salt);
                }
                loginUsers.UpdateDate = DateTime.UtcNow;
                loginUsers.UserType = userDetails.IsAdminUser == true ? "admin" : "std";
                loginUsers.Enabled = userDetails.Enabled;
                loginUsers.FailedLoginCount = userDetails.FailedLoginAttempts;


            }
            else
            {
                loginUsers.Enabled = userDetails.Enabled;
                loginUsers.FailedLoginCount = userDetails.FailedLoginAttempts;
            }
            _insightsContext.PortalLogin.Update(loginUsers);
            _insightsContext.SaveChanges();
        }



        public UserDetails GetUserProfile(int userId)
        {
            var userDetails = (from p in _insightsContext.PortalLogin
                               join c in _insightsContext.Client on p.ClientId equals c.ClientId
                               where p.UserId == userId
                               select new { p.FirstName, p.LastName, p.UserType, p.Username, p.Enabled, c.Description, c.Name, p.FailedLoginCount }).FirstOrDefault();

            return new UserDetails()
            {
                FirstName = userDetails.FirstName,
                LastName = userDetails.LastName,
                IsAdminUser = (userDetails.UserType == "admin"),
                Enabled = userDetails.Enabled ?? false,
                ClientDescription = userDetails.Description,
                ClientName = userDetails.Name,
                FailedLoginAttempts = userDetails.FailedLoginCount,
                UserName = userDetails.Username
            };

        }

        public List<UserDetails> GetUserList(int clientId)
        {
            List<UserDetails> userDetails = new List<UserDetails>();

            var users = from p in _insightsContext.PortalLogin where p.UserType != "Web"
                        where p.ClientId == clientId select new { p.FirstName, p.LastName, p.UserType, p.UserId };

            foreach (var user in users)
            {
                userDetails.Add(new UserDetails()
                {
                    FirstName = user.FirstName,
                    LastName = user.LastName,
                    IsAdminUser = user.UserType == "admin" ? true : false,
                    DisplayName = user.LastName + "," + user.FirstName,
                    UserId = user.UserId.ToString()
                });
            }

            return userDetails;
        }
    }
}
