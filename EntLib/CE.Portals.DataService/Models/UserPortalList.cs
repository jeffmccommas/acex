﻿
namespace CE.Portals.DataService.Models
{
    public partial class UserPortalList
    {
        public int PortalId { get; set; }
        public string PortalName { get; set; }
        public bool IsDefault { get; set; }
        public string PortalComponent { get; set; }

    }
}
