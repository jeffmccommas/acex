﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CE.Portals.DataService.Models
{
    public class MappedCustomer
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Street1 { get; set; }
        public string Street2 { get; set; }
        public string City { get; set; }
        public string StateProvince { get; set; }
        public string Country { get; set; }
        public string PostalCode { get; set; }
        public bool Error { get; set; }

    }
    
}
