﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CE.Portals.DataService.Models
{
    public class MeterMap
    {
        public string ESPMAccountId { get; set; }
        public string ESPMPremiseId { get; set; }
        //public List<string> InsightAccountId { get; set; }
        //public List<string> InsightPremiseId { get; set; }

        public List<InsightAccountInfo> InsightAccountInfoList { get; set; }
        

    }
}
