﻿using System;


namespace CE.Portals.DataService.Models
{
    public class LoginUser
    {
        public string AccessToken { get; set; }
        public String ExpiresIn { get; set; }
        public UserDetails user { get; set; }
    }
    
    public class UserDetails
    {
        public string UserId { get; set; }
        public string ClientId { get; set; }
        public string CeSecretAccessKey { get; set; }
        public string CeAccessKeyId { get; set; }
        public int FailedLoginAttempts { get; set; }
        public Boolean Enabled { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public Boolean IsAdminUser { get; set; }
        public String UserName { get; set; }
        public String ClientName { get; set; }
        public String ClientDescription { get; set; }
        public String PassWord { get; set; }
        public String DisplayName{ get; set; }
        public Boolean IsLoggedin { get; set; } = false;
    }
}
