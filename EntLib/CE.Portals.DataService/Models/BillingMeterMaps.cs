﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CE.Portals.DataService.Models
{
    public class BillingMeterMaps
    {

        public int? MapDetailId { get; set; }
        public int? MapId { get; set; }
        public string MapAccountId { get; set; }

        public string CustomerId { get; set; }
        public string MapPremiseId { get; set; }
        public bool Enabled { get; set; } = false;
        public int UpdatedBy { get; set; }
        public DateTime UpdatedDate { get; set; }

        public string MeterType { get; set; }
    }
}
