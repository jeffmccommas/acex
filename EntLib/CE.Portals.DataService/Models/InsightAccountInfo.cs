﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CE.Portals.DataService.Models
{
    public class InsightAccountInfo
    {
        public string CustomerName { get; set; }
        public string InsightAccountId { get; set; }
        public string InsightPremiseId { get; set; }
    }
}
