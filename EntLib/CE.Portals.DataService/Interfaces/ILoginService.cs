﻿using System.Collections.Generic;
using CE.Portals.DataService.Models;
using System;

namespace CE.Portals.DataService.Interfaces
{
    public interface ILoginService
    {
        UserDetails ValidateESPMUser(string UserName, string password);
        string GetClientESPMSecretKeys(string clientId);
        string GetClientAppsettings(string clientId, string appsettingKey);
        //List<UserPortalList> GetAllPortals(string userId, string clientId);
        UserDetails GetInsightsUserCreds(int clientId, string apiUserType, int envId);
        UserDetails GetUserDetails(int userId);
        UserDetails GetUserProfile(int userId);
        void AddNewUser(UserDetails userDetails, int enVironmentId);
        void UpdateUser(UserDetails userDetails, Boolean status);
        Boolean CheckIfUserExists(string userName);
        List<UserDetails> GetUserList(int clientId);
        string CheckDbConnection();





    }
}
