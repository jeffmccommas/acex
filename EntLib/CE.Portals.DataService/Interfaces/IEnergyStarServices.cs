﻿using System.Collections.Generic;
using CE.InsightDataAccess.InsightModels;


namespace CE.Portals.DataService.Interfaces
{
    public interface IEnergyStarServices
    {
        List<AcceptedMeters> GetAcceptedMeterIds(int ClientId);

        void AddAcceptedMeters(AcceptedMeters meter);
    }
}