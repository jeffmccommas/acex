﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CE.Portals.DataService.Interfaces
{
    public interface  IConfigurationService
    {
        string GetDwDatabaseConfigValue(int clientId, string environmentKey, string configurationKey);
    }
}
