﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Text;
using CE.InsightDataAccess.InsightsDWModels;
using CE.Portals.DataService.Models;

namespace CE.Portals.DataService.Interfaces
{
    public interface IMappingService
    {
        string GetMappedData(string connectionString);

        MappedCustomer GetMappedCustomerDetails(string shardDBName, string accountId, string premiseId);

        void AddMapping(string shardDnName, Espmmap espmMapping);

        Boolean CheckMapping(string shardDbName, string accountId, string premiseId);

        Espmmap GetMappedMeters(string shardDbName, string accountId, string premiseId, string meterId);

        Espmmap GetMappedMetersFromDimPremise(string shardDbName, string accountId, string premiseId);

        Boolean UpdateMeterMappingonAccept(string shardDbName, string accountId, string premiseId, int userId);

        Boolean IsMapped(string shardDbName, string meterId, string accountId, string premiseId, string meterType, int loginId);

        Boolean UpdateCustomerId(string shardDbName, string meterId);

        List<BillingMeterMaps> GetMappedAccountForMeters(string shardDbName, string meterId);

        Boolean AddMeterBillignData(string shardDbName, List<MeterBillingData> meterBillignData);

        Boolean DeleteBillingDataWebJob(string shardDbName);

        List<MeterBillingData> GetAllMeterBillingforMeter(string shardDbName, string meterId, string startdate, string endDate);

        List<string> GetAllMeterIds(string shardDbName);

        Boolean CheckIfMeterIsInWhitelist(string shardDbName, string meterId);
        Boolean DoesWhiteListHasAnyRecords(string shardDbName);

    }
}