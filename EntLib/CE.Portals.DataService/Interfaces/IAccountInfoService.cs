﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CE.Portals.DataService.Models;

namespace CE.Portals.DataService.Interfaces
{
    public interface IAccountInfoService
    {
        List<InsightAccountInfo> FindAccount(string accountId, string premiseId = "");
        bool AddMap(InsightAccountInfo insightAccountInfo, string accountId, string premiseId = "");
        bool RemoveMap(InsightAccountInfo insightAccountInfo, string accountId, string premiseId = "");

    }
}
