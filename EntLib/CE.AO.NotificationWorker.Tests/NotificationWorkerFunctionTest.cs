﻿using System.IO;
using System.Runtime.Remoting.Messaging;
using CE.AO.Logging;
using CE.AO.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace CE.AO.NotificationWorker.Tests
{
    /// <summary>
    /// Test Case Class for Notification Worker Role(Function.cs)
    /// </summary>
    [TestClass]
    public class NotificationWorkerFunctionTest
    {
        /// <summary>
        /// Test Case for Process Notification
        /// </summary>
        [TestMethod]
        public void ProcessNotification()
        {
            var log = TextWriter.Null;
            Functions.LogModel = new LogModel
            {
                DisableLog = false,
                Module = Utilities.Enums.Module.Notification
            };
            Functions.ProcessNotification("87^^600^^AC76^^096756^^Program1^^Source^^89^^Meta7890^^None^^34", log);
            Assert.AreEqual(true, true);
        }

        /// <summary>
        /// Test Case for Process Notification
        /// </summary>
        [TestMethod]
        public void ProcessNotification_Else()
        {
            var log = TextWriter.Null;

            Functions.LogModel = new LogModel
            {
                DisableLog = false,
                Module = Utilities.Enums.Module.Notification
            };
            Functions.ProcessNotification("87^^600^^AC76^^096756^^Program1^^Source^^89^^Meta7890^^None", log);
            Assert.AreEqual(true, true);
        }

        /// <summary>
        /// Test Case for Process Notification For Catch
        /// </summary>
        [TestMethod]
        public void ProcessNotification_Catch()
        {
            var log = TextWriter.Null;

            Functions.LogModel = new LogModel
            {
                DisableLog = false,
                Module = Utilities.Enums.Module.Notification
            };
            try
            {
                Functions.ProcessNotification(null, log);
            }
            catch
            {
                Assert.AreEqual(true, true);
            }
        }

        /// <summary>
        /// Test Case for Process Notification For Catch
        /// </summary>
        [TestMethod]
        public void ProcessNotificationReport()
        {
            var log = TextWriter.Null;
            Functions.LogModel = new LogModel
            {
                DisableLog = false,
                Module = Utilities.Enums.Module.InsightEvaluation
            };
            Functions.ProcessNotificationReport("87^^09/24/2016^^2", log);
            Assert.AreEqual(true, true);
        }

        /// <summary>
        /// Test Case for Process Notification For Catch
        /// </summary>
        [TestMethod]
        public void ProcessNotificationReport_Eles()
        {
            var log = TextWriter.Null;

            Functions.LogModel = new LogModel
            {
                DisableLog = false,
                Module = Utilities.Enums.Module.InsightEvaluation
            };
            Functions.ProcessNotificationReport("87^^09/24/2016^^2^^34", log);
            Assert.AreEqual(true, true);
        }

        /// <summary>
        /// Test Case for Process Notification For Catch
        /// </summary>
        [TestMethod]
        public void ProcessNotificationReport_Catch()
        {
            var log = TextWriter.Null;

            Functions.LogModel = new LogModel
            {
                DisableLog = false,
                Module = Utilities.Enums.Module.InsightEvaluation
            };
            try
            {
                Functions.ProcessNotificationReport("87^^2^^09/24/2016", log);
            }
            catch
            {
                Assert.AreEqual(true, true);
            }
        }

        /// <summary>
        /// Cleanup Method To Release Log4Net Data Resources hold by Any Test Method
        /// </summary>
        [TestCleanup]
        public void CleanUpMethod()
        {
            CallContext.FreeNamedDataSlot("log4net.Util.LogicalThreadContextProperties");
        }
    }
}