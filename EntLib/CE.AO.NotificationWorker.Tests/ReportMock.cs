﻿using AO.BusinessContracts;
using CE.AO.Logging;
using System;
using System.Threading.Tasks;
using CE.AO.Models;

namespace CE.AO.NotificationWorker.Tests
{
    /// <summary>
    /// Report Mock Class
    /// </summary>
    /// <seealso>
    ///     <cref>AO.BusinessContracts.IReport</cref>
    /// </seealso>
    public class ReportMock : IReport
    {
        /// <summary>
        /// Gets or sets the log model.
        /// </summary>
        /// <value>
        /// The log model.
        /// </value>
        public LogModel LogModel { get; set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="ReportMock"/> class.
        /// </summary>
        public ReportMock()
        {
            LogModel = new LogModel { DisableLog = false };
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ReportMock"/> class.
        /// </summary>
        /// <param name="logModel">The log model.</param>
        public ReportMock(LogModel logModel)
        {
            LogModel = logModel;
        }

        /// <summary>
        /// Exports the insights.
        /// </summary>
        /// <param name="clientId">The client identifier.</param>
        /// <param name="asOfDate">As of date.</param>
        /// <returns></returns>
        public Task<bool> ExportInsights(int clientId, DateTime asOfDate)
        {
            return Task.FromResult(true);
        }

        /// <summary>
        /// Exports the notifications.
        /// </summary>
        /// <param name="clientId">The client identifier.</param>
        /// <param name="asOfDate">As of date.</param>
        /// <returns></returns>
        public Task<bool> ExportNotifications(int clientId, DateTime asOfDate)
        {
            return Task.FromResult(true);
        }
    }
}