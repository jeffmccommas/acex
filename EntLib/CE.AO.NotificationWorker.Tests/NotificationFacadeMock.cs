﻿using AO.BusinessContracts;
using CE.AO.Logging;
using CE.AO.Models;

namespace CE.AO.NotificationWorker.Tests
{
    /// <summary>
    /// Notification Facade Mock Class
    /// </summary>
    /// <seealso>
    ///     <cref>AO.BusinessContracts.INotificationFacade</cref>
    /// </seealso>
    public class NotificationFacadeMock : INotificationFacade
    {
        /// <summary>
        /// Gets or sets the log model.
        /// </summary>
        /// <value>
        /// The log model.
        /// </value>
        public LogModel LogModel { get; set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="NotificationFacadeMock"/> class.
        /// </summary>
        public NotificationFacadeMock()
        {
            LogModel = new LogModel { DisableLog = false };
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="NotificationFacadeMock"/> class.
        /// </summary>
        /// <param name="logModel">The log model.</param>
        public NotificationFacadeMock(LogModel logModel)
        {
            LogModel = logModel;
        }

        /// <summary>
        /// Processes the notification.
        /// </summary>
        /// <param name="clientId">The client identifier.</param>
        /// <param name="customerId">The customer identifier.</param>
        /// <param name="accountId">The account identifier.</param>
        /// <param name="serviceContractId">The service contract identifier.</param>
        /// <param name="programName">Name of the program.</param>
        /// <param name="evaluationTickCount">The evaluation tick count.</param>
        public void ProcessNotification(int clientId, string customerId, string accountId, string serviceContractId, string programName, long? evaluationTickCount)
        {
        }
    }
}