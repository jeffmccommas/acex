﻿CREATE TABLE [dbo].[DimAction] (
    [ActionKey]    INT            NOT NULL,
    [ActionId]     INT            NOT NULL,
    [CMSActionKey] VARBINARY (50) NOT NULL,
    CONSTRAINT [PK_DimAction] PRIMARY KEY CLUSTERED ([ActionKey] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IX_DimAction]
    ON [dbo].[DimAction]([ActionId] ASC);

