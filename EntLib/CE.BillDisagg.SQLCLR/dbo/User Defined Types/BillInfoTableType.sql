﻿CREATE TYPE [dbo].[BillInfoTableType] AS TABLE (
    [StartDateKey]      INT     NOT NULL,
    [EndDateKey]        INT     NOT NULL,
    [FuelKey]           TINYINT NOT NULL,
    [BillPeriodTypeKey] TINYINT NOT NULL);

