using System;
using System.Data;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using Microsoft.SqlServer.Server;


public partial class StoredProcedures
{
    [Microsoft.SqlServer.Server.SqlProcedure]
    public static void EchoTest(SqlInt32 clientId, SqlString customerId, SqlString accountId, SqlString premiseId)
    {
        SqlPipe sp = SqlContext.Pipe;
        //BillDisaggManagerFactory factory = null;
        //IBillDisagg manager = null;
        //BillDisaggResult result = null;

        //factory = new BillDisaggManagerFactory();
        //manager = factory.CreateBillDisaggManager(clientId.Value);

        //result = manager.ExecuteBillDisagg(customerId.Value, accountId.Value, premiseId.Value);

        // Setup 3 columns, and 4 rows
        SqlMetaData[] meta = new SqlMetaData[] { new SqlMetaData("Column1", SqlDbType.VarChar, 50), new SqlMetaData("Column2", SqlDbType.VarChar, 50), new SqlMetaData("Column3", SqlDbType.VarChar, 50) };
        SqlDataRecord[] records = new SqlDataRecord[4];

        // fill 4 rows
        records[0] = new SqlDataRecord(meta); records[0].SetString(0, customerId.Value); records[0].SetString(1, accountId.Value); records[0].SetString(2, premiseId.Value);
        records[1] = new SqlDataRecord(meta); records[1].SetString(0, customerId.Value); records[1].SetString(1, accountId.Value); records[1].SetString(2, premiseId.Value);
        records[2] = new SqlDataRecord(meta); records[2].SetString(0, customerId.Value); records[2].SetString(1, accountId.Value); records[2].SetString(2, premiseId.Value);
        records[3] = new SqlDataRecord(meta); records[3].SetString(0, customerId.Value); records[3].SetString(1, accountId.Value); records[3].SetString(2, premiseId.Value);

        // send it to caller as recordset
        sp.SendResultsStart(new SqlDataRecord(meta));
        foreach (SqlDataRecord r in records)
        {
            SqlContext.Pipe.SendResultsRow(r);
        }
        sp.SendResultsEnd();

    }
}
