﻿namespace AO.VEE.DTO
{
	/// <summary>
	/// ValidatorSettings Dto
	/// </summary>
	public class ValidatorSettingsDto
	{
		public bool Required { get; set; }
		public int IntervalThreshold { get; set; }
		public double ConsumptionThreshold { get; set; }
	}
}
