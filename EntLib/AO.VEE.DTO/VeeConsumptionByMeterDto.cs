﻿using System;
// ReSharper disable InconsistentNaming

namespace AO.VEE.DTO
{
	/// <summary>
	/// VeeConsumptionByMeter dto
	/// </summary>
	public class VeeConsumptionByMeterDto
	{
		public int ClientId { get; set; }
		public string MeterId { get; set; }
		public DateTime AmiTimeStamp { get; set; }
		public string AccountNumber { get; set; }
		public int CommodityId { get; set; }
		public double? Consumption { get; set; }
		public int? Direction { get; set; }
		public double? EstimatedConsumption { get; set; }
		public double? EditedConsumption { get; set; }
		public double? RawConsumption { get; set; }
		public double? StandardUOMConsumption { get; set; }
		public int EstimationMethod { get; set; }
		public bool? IsApproved { get; set; }
		public bool? IsEdited { get; set; }
		public bool? IsEstimated { get; set; }
		public bool? IsMetered { get; set; }
		public bool? IsMissing { get; set; }
		public bool? IsNegative { get; set; }
		public bool? IsZero { get; set; }
		public bool? IsStatic { get; set; }
		public bool? IsSpike { get; set; }
		public string Timezone { get; set; }
		public string TransponderId { get; set; }
		public string TransponderPort { get; set; }
		public int UOMId { get; set; }
		public int StandardUOMId { get; set; }
		public int IntervalType { get; set; }
	}
}
