﻿namespace AO.VEE.DTO
{
	/// <summary>
	/// VeeConfigurations dto
	/// </summary>
	public class VeeConfigurationsDto
	{
		public ValidatorSettingsDto MissingIntervalCheck { get; set; }
		public ValidatorSettingsDto NegativeValueCheck { get; set; }
		public ValidatorSettingsDto ZeroCheck { get; set; }
		public ValidatorSettingsDto StaticCheck { get; set; }
		public ValidatorSettingsDto SpikeCheck { get; set; }
	}
}
