﻿using System;
using AO.DTO;

namespace AO.VEE.DTO
{
    public class VeeValidationEstimationTriggerDto : TriggerDto
    {
        public int ClientId { get; set; }

        public string MeterId { get; set; }

        public DateTime? AmiDate { get; set; }

		public bool IsActive { get; set; }
	}
}
