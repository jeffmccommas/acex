﻿using System;

namespace AO.VEE.DTO
{
	/// <summary>
	/// VeeStagedClientMeterByDate dto
	/// </summary>
	public class VeeStagedClientMeterByDateDto
	{
		public int ClientId { get; set; }
		public string MeterId { get; set; }
		public DateTime AmiDate { get; set; }
	}
}
