﻿using System.Security.Cryptography.X509Certificates;

namespace CE.InsightsIntegration.Models
{
    public partial class QueryToolForm
    {
        public string selectedCEAccessKeyID { get; set; }
        public string selectedAuthName { get; set; }
        public int UserId { get; set; }
        public string BaseUrl { get; set; }
        public string SelectedClientUserName { get; set; }
        public string selectedEndpoint { get; set; }
        public string Parameters { get; set; }
        public string SelectedMethodApi { get; set; }

        //encrypt the form fields if checked when sending to remote api request
        public bool incEnc { get; set; }
        public string XDateTimeUTCHeader { get; set; }
        public string MetaHeaderEntry { get; set; }
        public bool MetaHeaderEntryInc { get; set; }
        public string ResponseQueryAPI { get; set; }
        public string BasicKey { get; set; }
        public int AuthType { get; set; }
        public string Environment { get; set; }
        public X509CertificateCollection ClientCert { get; set; }
        public string Thumbprint { get; set; }
        public string ApiVersion { get; set; }
    }
}