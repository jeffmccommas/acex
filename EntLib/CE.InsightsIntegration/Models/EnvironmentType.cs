﻿
namespace CE.InsightsIntegration.Models
{
    public enum EnvironmentType
    {
        prod,
        perf,
        uat,
        qa,
        dev,
        localdev
    }
}