﻿
namespace CE.InsightsIntegration.Models
{
    public class Response
    {
        public string Content { get; set; }
        public int StatusCode { get; set; }
        public string StatusDescription { get; set; }
        public string FileName { get; set; }
        public byte[] RawBytes { get; set; }
    }
}
