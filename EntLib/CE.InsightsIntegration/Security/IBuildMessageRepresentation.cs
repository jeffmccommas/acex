﻿using RestSharp;

namespace CE.InsightsIntegration.Common
{
    public interface IBuildMessageRepresentation
    {
        string BuildRequestRepresentation(RestRequest requestMessage, RestClient client, string querystring);
    }
}