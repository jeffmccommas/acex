﻿namespace CE.InsightsIntegration.Common
{
    //some differences from infrastructure
    public interface ICalculteSignature
    {
        string Signature(string secret, string value);
    }
}