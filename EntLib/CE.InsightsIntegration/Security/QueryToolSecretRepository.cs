﻿using System;
using System.Security.Cryptography;
using System.Text;

namespace CE.InsightsIntegration.Common
{
    public class QueryToolSecretRepository : ISecretRepository
    {
        private string ComputeHash(string inputData, HashAlgorithm algorithm)
        {
            byte[] inputBytes = Encoding.UTF8.GetBytes(inputData);
            byte[] hashed = algorithm.ComputeHash(inputBytes);
            return Convert.ToBase64String(hashed);
        }
    }
}