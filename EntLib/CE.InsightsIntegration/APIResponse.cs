﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using CE.InsightsIntegration.Common;
using CE.InsightsIntegration.Models;
using Newtonsoft.Json;
using RestSharp;

namespace CE.InsightsIntegration
{
    public class APIResponse
    {
        public Response GetAPIResponse(QueryTool query)
        {
            // setup rest client
            var client = new RestClient(query.BaseUrl);

            var user = query.selectedCEAccessKeyID;

            //method
            Method method;
            switch (query.SelectedMethodApi.ToUpper())
            {
                case Helpers.GET:
                    method = Method.GET;
                    break;
                case Helpers.POST:
                    method = Method.POST;
                    break;
                case Helpers.PUT:
                    method = Method.PUT;
                    break;
                case Helpers.DELETE:
                    method=Method.DELETE;
                    break;
                default:
                    method = Method.GET;
                    break;
            }

            //request
            var request = new RestRequest(method);
            //resource at endpoint
            if (!string.IsNullOrEmpty(query.selectedEndpoint))
            {
                request.Resource = query.selectedEndpoint;
            }

            var queryString = query.Parameters;
            var encQueryString = string.Empty;

            if (queryString.Length > 0)
            {
                switch (method)
                {
                    case Method.GET:
                        encQueryString = HttpUtility.UrlEncode(queryString);
                        break;
                    case Method.POST:
                    case Method.PUT:
                        encQueryString = queryString;
                        break;
                }
            }
            //if encrypt is turned on (checked) then include the whole query 
            //otherwise parse through the query and add to parameters on the restclient request
            if (query.incEnc)
            {
                if (queryString.Length > 0)
                {
                    encQueryString = Helpers.EncryptQueryAPI(queryString, user);
                    request.AddParameter("enc", encQueryString, ParameterType.GetOrPost);
                }
            }
            else
            {
                if (!string.IsNullOrEmpty(queryString))
                {
                    //get after the ? on the first character
                    var querystringParsed = queryString.Substring(queryString.IndexOf('?') + 1);
                    //split into key values
                    {
                        var p = querystringParsed.Split('&');
                        foreach (var temp in from s in p where s.IndexOf('=') > -1 select s.Split('='))
                        {
                            request.AddParameter(temp[0], temp[1], ParameterType.GetOrPost);
                        }
                    }
                }
            }

            //headers - three custom headers being used
            //signing
            var signingHandler = new HmacSigningHandler(new QueryToolSecretRepository(),
                new CanonicalRepresentationBuilder(),
                new HmacSignatureCalculator(),
                query)
            {
                Username = user,
                MessageIdHeader = query.MessageIdHeader,
                ChannelHeader = query.ChannelHeader,
                LocaleHeader = query.LocaleHeader,
                MetaHeader = query.MetaHeader,
                Date = query.DateTime,
                BasicKey = query.BasicKey
            };


            //MD5
            if (queryString.Length > 0)
            {
                switch (method)
                {
                    case Method.GET:
                        signingHandler.QueryString = encQueryString;
                        break;
                    case Method.POST:
                    case Method.PUT:
                        signingHandler.Body = encQueryString;
                        break;
                }
            }

            var signature = signingHandler.Execute(request, client);

            if (query.FileDetailList != null && query.FileDetailList.Count > 0)
            {
                foreach (var file in query.FileDetailList)
                {
                    byte[] data = new byte[file.FileStream.Length];
                    file.FileStream.Read(data, 0, (int)file.FileStream.Length);

                    request.AddFile(file.ParameterName, data, file.FileName, file.ContentType);
                }

                request.AddHeader(Helpers.ContentType, "multipart/form-data");
            }

            //authentication
            if (query.AuthType == 0)
            {
                client.Authenticator = new ACLAuthenticator(user, signature, "CE");
            }
            else
            {
                var basicKey = query.BasicKey;
                var authHeader = $"{"Basic"} {Base64Encode(user + ":" + basicKey)}";
                request.AddParameter("Authorization", authHeader, ParameterType.HttpHeader);
            }

            //Client Cert
            client.ClientCertificates = query.ClientCert;

            // execute response
            var response = client.Execute(request);

            var cdHeader = response.Headers.Where(x => x.Name == "Content-Disposition").Select(x => x.Value);
            var fileName = string.Empty;
            foreach (var n in cdHeader)
            {
                fileName = n.ToString().Split(new string[] { "filename=" }, StringSplitOptions.None)[1];
            }
            var r = new Response
            {
                Content = response.Content,
                StatusCode = (int)response.StatusCode,
                StatusDescription = response.StatusDescription,
                FileName = fileName,
                RawBytes = response.RawBytes
            };
            //return JsonConvert.SerializeObject(response);
            return r;
        }

        private static string Base64Encode(string plainText)
        {
            var plainTextBytes = Encoding.UTF8.GetBytes(plainText);
            return Convert.ToBase64String(plainTextBytes);
        }

        public Response PostEvents(QueryTool query)
        {
            // setup rest client          
            var key = Encoding.ASCII.GetBytes(query.BasicKey);
            var uri = "https:" + query.BaseUrl;
            var expireInSeconds = DateTime.UtcNow.ToUniversalTime().Subtract(new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc)).TotalSeconds;
            var utfBytes = Encoding.UTF8.GetBytes(HttpUtility.UrlEncode(uri) + "\n" + expireInSeconds);
            var plainSignature = Encoding.UTF8.GetString(utfBytes);
            var token = "SharedAccessSignature sr=" + HttpUtility.UrlEncode(uri) + "&sig=" + HttpUtility.UrlEncode(Encode(plainSignature, key)) + "&se=" + expireInSeconds + "&skn=Send";
            //Client
            var client = new RestClient("https:" + query.BaseUrl + "/messages/?timeout=60");

            //request
            var request = new RestRequest(Method.POST);
            request.AddHeader("Content-Type", "application/json");
            request.AddHeader("Authorization", token);

            var eventData = (Dictionary<string, string>)JsonConvert.DeserializeObject(query.Parameters, typeof(Dictionary<string, string>));
            request.AddJsonBody(eventData);
            request.RequestFormat = DataFormat.Json;
            //execute response
            var r = new Response();
            if (ValidateEventData(eventData))
            {
                var response = client.Execute(request);
                r.Content = response.Content;
                r.StatusCode = (int)response.StatusCode;
                r.StatusDescription = response.StatusDescription;
            }
            else
            {
                r.StatusCode = 400;
                r.StatusDescription = "Event data malformed.";
            }
            return r;
        }

        private static string Encode(string input, byte[] key)
        {
            var myhmacsha256 = new HMACSHA256(key);
            var byteArray = Encoding.ASCII.GetBytes(input);
            var stream = new MemoryStream(byteArray);
            var hash = myhmacsha256.ComputeHash(stream);
            return Convert.ToBase64String(hash);
        }

        private static bool ValidateEventData(Dictionary<string, string> eventData)
        {
            var provider = System.CodeDom.Compiler.CodeDomProvider.CreateProvider("C#");
            foreach (var entry in eventData)
            {
                if (entry.Key == "PartitionKey" || entry.Key == "RowKey")
                {
                    if (!string.IsNullOrWhiteSpace(entry.Value)
                        && entry.Value.Any(c => !char.IsControl(c))
                        && !entry.Value.Contains(@"\")
                        && !entry.Value.Contains("/")
                        && !entry.Value.Contains("#")
                        && !entry.Value.Contains("?"))
                    {
                        // Valid
                    }
                    else
                    {
                        return false;
                    }
                }
                else
                {
                    if (!string.IsNullOrWhiteSpace(entry.Key) && provider.IsValidIdentifier(entry.Key) &&
                        entry.Key.Length <= 255)
                    {
                        // Valid
                    }
                    else
                    {
                        return false;
                    }
                }
            }
            return true;
        }
    }
}
