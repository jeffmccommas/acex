﻿using System;
using System.Security.Cryptography.X509Certificates;
using CE.InsightsIntegration.Common;
using CE.InsightsIntegration.Models;

namespace CE.InsightsIntegration
{
    public class API
    {
        public static Response GetEchoResponse(QueryTool query)
        {
            query.SelectedMethodApi = Helpers.GET;
            query.selectedEndpoint = "/api/v1/echo";

            if (query.Thumbprint != null)
            {
                var certificate = GetCertificateByThumbprint(query.Thumbprint);
                var coll = new X509Certificate2Collection {certificate};
                query.ClientCert = coll;
            }

            var t = new APIResponse();
            var r = t.GetAPIResponse(query);
            return r;
        }

        public static Response GetContentResponse(QueryTool query)
        {
            query.SelectedMethodApi = Helpers.GET;
            query.selectedEndpoint = "/api/v1/content";

            if (query.Thumbprint != null)
            {
                var certificate = GetCertificateByThumbprint(query.Thumbprint);
                var coll = new X509Certificate2Collection { certificate };
                query.ClientCert = coll;
            }
            var t = new APIResponse();
            var r = t.GetAPIResponse(query);
            return r;
        }

        public static Response GetConsumptionResponse(QueryTool query)
        {
            query.SelectedMethodApi = Helpers.GET;
            query.selectedEndpoint = "/api/v2/consumption";

            if (query.Thumbprint != null)
            {
                var certificate = GetCertificateByThumbprint(query.Thumbprint);
                var coll = new X509Certificate2Collection { certificate };
                query.ClientCert = coll;
            }
            var t = new APIResponse();
            var r = t.GetAPIResponse(query);
            return r;
        }

        public static Response GetBillDisaggResponse(QueryTool query)
        {
            query.SelectedMethodApi = Helpers.GET;
            query.selectedEndpoint = "/api/v1/billdisagg";

            if (query.Thumbprint != null)
            {
                var certificate = GetCertificateByThumbprint(query.Thumbprint);
                var coll = new X509Certificate2Collection { certificate };
                query.ClientCert = coll;
            }
            var t = new APIResponse();
            var r = t.GetAPIResponse(query);
            return r;
        }

        public static Response ProfileResponse(QueryTool query)
        {
            query.selectedEndpoint = "/api/v1/profile";

            if (query.Thumbprint != null)
            {
                var certificate = GetCertificateByThumbprint(query.Thumbprint);
                var coll = new X509Certificate2Collection { certificate };
                query.ClientCert = coll;
            }
            var t = new APIResponse();
            var r = t.GetAPIResponse(query);
            return r;
        }

        public static Response WebTokenResponse(QueryTool query)
        {
            query.selectedEndpoint = "/api/"+ query.ApiVersion +"/webtoken";

            if (query.Thumbprint != null)
            {
                var certificate = GetCertificateByThumbprint(query.Thumbprint);
                var coll = new X509Certificate2Collection { certificate };
                query.ClientCert = coll;
            }
            var t = new APIResponse();
            var r = t.GetAPIResponse(query);
            return r;
        }

        public static Response ActionResponse(QueryTool query)
        {
            query.SelectedMethodApi = Helpers.GET;
            query.selectedEndpoint = "/api/v1/action";

            if (query.Thumbprint != null)
            {
                var certificate = GetCertificateByThumbprint(query.Thumbprint);
                var coll = new X509Certificate2Collection { certificate };
                query.ClientCert = coll;
            }
            var t = new APIResponse();
            var r = t.GetAPIResponse(query);
            return r;
        }

        public static Response ActionPlanResponse(QueryTool query)
        {
            query.selectedEndpoint = "/api/v1/actionplan";

            if (query.Thumbprint != null)
            {
                var certificate = GetCertificateByThumbprint(query.Thumbprint);
                var coll = new X509Certificate2Collection { certificate };
                query.ClientCert = coll;
            }
            var t = new APIResponse();
            var r = t.GetAPIResponse(query);
            return r;
        }

        public static Response BenchmarkResponse(QueryTool query)
        {
            query.SelectedMethodApi = Helpers.GET;
            query.selectedEndpoint = "/api/v1/benchmark";

            if (query.Thumbprint != null)
            {
                var certificate = GetCertificateByThumbprint(query.Thumbprint);
                var coll = new X509Certificate2Collection { certificate };
                query.ClientCert = coll;
            }
            var t = new APIResponse();
            var r = t.GetAPIResponse(query);
            return r;
        }

        public static Response BillResponse(QueryTool query)
        {
            query.SelectedMethodApi = Helpers.GET;
            query.selectedEndpoint = "/api/" + query.ApiVersion + "/bill";

            if (query.Thumbprint != null)
            {
                var certificate = GetCertificateByThumbprint(query.Thumbprint);
                var coll = new X509Certificate2Collection { certificate };
                query.ClientCert = coll;
            }
            var t = new APIResponse();
            var r = t.GetAPIResponse(query);
            return r;
        }

        public static Response GetBillToDateResponse(QueryTool query)
        {
            query.SelectedMethodApi = Helpers.GET;
            query.selectedEndpoint = "/api/v2/billtodate";

            if (query.Thumbprint != null)
            {
                var certificate = GetCertificateByThumbprint(query.Thumbprint);
                var coll = new X509Certificate2Collection { certificate };
                query.ClientCert = coll;
            }
            var t = new APIResponse();
            var r = t.GetAPIResponse(query);
            return r;
        }

        public static Response GetCustomerInfoResponse(QueryTool query)
        {
            query.SelectedMethodApi = Helpers.GET;
            query.selectedEndpoint = "/api/v1/customerinfo";

            if (query.Thumbprint != null)
            {
                var certificate = GetCertificateByThumbprint(query.Thumbprint);
                var coll = new X509Certificate2Collection { certificate };
                query.ClientCert = coll;
            }
            var t = new APIResponse();
            var r = t.GetAPIResponse(query);
            return r;
        }

        public static Response RateResponse(QueryTool query)
        {
            query.SelectedMethodApi = Helpers.GET;
            query.selectedEndpoint = "/api/" + query.ApiVersion + "/rate";

            if (query.Thumbprint != null)
            {
                var certificate = GetCertificateByThumbprint(query.Thumbprint);
                var coll = new X509Certificate2Collection { certificate };
                query.ClientCert = coll;
            }
            var t = new APIResponse();
            var r = t.GetAPIResponse(query);
            return r;
        }

        public static Response SubscriptionResponse(QueryTool query)
        {
            query.selectedEndpoint = "/api/" + query.ApiVersion + "/subscription";

            if (query.Thumbprint != null)
            {
                var certificate = GetCertificateByThumbprint(query.Thumbprint);
                var coll = new X509Certificate2Collection { certificate };
                query.ClientCert = coll;
            }
            var t = new APIResponse();
            var r = t.GetAPIResponse(query);
            return r;
        }
        
        public static Response EventTrackingResponse(QueryTool query)
        {            
            var t = new APIResponse();
            var r = t.PostEvents(query);
            return r;
        }

        public static Response ModelResponse(QueryTool query)
        {
            query.selectedEndpoint = "/api/v1/energymodel";

            if (query.Thumbprint != null)
            {
                var certificate = GetCertificateByThumbprint(query.Thumbprint);
                var coll = new X509Certificate2Collection { certificate };
                query.ClientCert = coll;
            }
            var t = new APIResponse();
            var r = t.GetAPIResponse(query);
            return r;
        }

        public static Response GreenButtonBillResponse(QueryTool query)
        {
            query.SelectedMethodApi = Helpers.GET;
            query.selectedEndpoint = "/api/v1/greenbuttonbill";

            if (query.Thumbprint != null)
            {
                var certificate = GetCertificateByThumbprint(query.Thumbprint);
                var coll = new X509Certificate2Collection { certificate };
                query.ClientCert = coll;
            }
            var t = new APIResponse();
            var r = t.GetAPIResponse(query);
            return r;
        }
        public static Response GreenButtonAmiResponse(QueryTool query)
        {
            query.SelectedMethodApi = Helpers.GET;
            query.selectedEndpoint = "/api/v1/greenbuttonami";

            if (query.Thumbprint != null)
            {
                var certificate = GetCertificateByThumbprint(query.Thumbprint);
                var coll = new X509Certificate2Collection { certificate };
                query.ClientCert = coll;
            }
            var t = new APIResponse();
            var r = t.GetAPIResponse(query);
            return r;
        }

        public static Response PdfReportsResponse(QueryTool query)
        {
            query.SelectedMethodApi = Helpers.GET;
            query.selectedEndpoint = "/api/v1/file";

            if (query.Thumbprint != null)
            {
                var certificate = GetCertificateByThumbprint(query.Thumbprint);
                var coll = new X509Certificate2Collection { certificate };
                query.ClientCert = coll;
            }
            var t = new APIResponse();
            var r = t.GetAPIResponse(query);
            return r;
        }

        public static Response PdfReportResponse(QueryTool query)
        {
            query.SelectedMethodApi = Helpers.GET;
            query.selectedEndpoint = "/api/v1/file";

            if (query.Thumbprint != null)
            {
                var certificate = GetCertificateByThumbprint(query.Thumbprint);
                var coll = new X509Certificate2Collection { certificate };
                query.ClientCert = coll;
            }
            var t = new APIResponse();
            var r = t.GetAPIResponse(query);
            return r;
        }

        public static Response PdfDeleteReportResponse(QueryTool query)
        {
            query.SelectedMethodApi = Helpers.DELETE;
            query.selectedEndpoint = "/api/v1/file";

            if (query.Thumbprint != null)
            {
                var certificate = GetCertificateByThumbprint(query.Thumbprint);
                var coll = new X509Certificate2Collection { certificate };
                query.ClientCert = coll;
            }
            var t = new APIResponse();
            var r = t.GetAPIResponse(query);
            return r;
        }
        public static Response FileResponse(QueryTool query)
        {
            query.SelectedMethodApi = Helpers.POST;
            query.selectedEndpoint = "/api/v1/File";

            if (query.Thumbprint != null)
            {
                var certificate = GetCertificateByThumbprint(query.Thumbprint);
                var coll = new X509Certificate2Collection { certificate };
                query.ClientCert = coll;
            }
            var t = new APIResponse();
            var r = t.GetAPIResponse(query);
            return r;
        }

        //Returns a certificate by searching through all likely places
        private static X509Certificate2 GetCertificateByThumbprint(string thumbprint)
        {
            //foreach likely certificate store name
            foreach (var name in new[] { StoreName.My, StoreName.Root })
            {
                //foreach store location
                foreach (var location in new[] { StoreLocation.LocalMachine })
                {
                    //see if the certificate is in this store name and location
                    var certificate = FindThumbprintInStore(thumbprint, name, location);
                    if (certificate != null)
                    {
                        //return the resulting certificate
                        return certificate;
                    }
                }
            }
            //certificate was not found
            throw new Exception($"The certificate with thumbprint {thumbprint} was not found");
        }

        private static X509Certificate2 FindThumbprintInStore(string thumbprint,
                                                              StoreName name, StoreLocation location)
        {
            //creates the store based on the input name and location e.g. name=My
            var certStore = new X509Store(name, location);
            certStore.Open(OpenFlags.ReadOnly);
            //finds the certificate in question in this store
            var certCollection = certStore.Certificates.Find(X509FindType.FindByThumbprint,
                                                             thumbprint, false);
            certStore.Close();

            if (certCollection.Count > 0)
            {
                //if it is found return
                return certCollection[0];
            }
            //if the certificate was not found return null
            return null;
        }
    }
}
