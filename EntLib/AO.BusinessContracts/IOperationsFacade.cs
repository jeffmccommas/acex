﻿using System.Threading.Tasks;
using System.Collections.Generic;
using AO.DTO;
using CE.AO.Models;

namespace AO.BusinessContracts
{
    /// <summary>
    /// Interface for log class
    /// </summary>
    public interface IOperationsFacade
    {
        LogModel LogModel { get; set; }

        /// <summary>
        /// To insert/update the LogViewModel in Log table.
        /// </summary>
        /// <param name="model">LogViewModel which is to be inserted/updated</param>
        /// <returns>True</returns>
        Task<bool> InsertOrMergeLogAsync(LogViewModel model);

        /// <summary>
        /// To fetch the list of logs from the database
        /// </summary>
        /// <param name="filterParams">Filter params for logs to be fetched</param>
        /// <returns>List of LogViewModel model</returns>
        List<LogViewModel> GetLogHistory(LogHistoryRequestDTO filterParams);
    }
}