﻿using System.Threading.Tasks;

namespace AO.BusinessContracts
{
    public interface ICacheProvider
    {
        /// <summary>
        /// Store object in cache.  If the object already exists then 
        /// the value will be overwritten regardless of type.
        /// </summary>
        /// <param name="key"></param>
        /// <param name="value"></param>
        /// <param name="database"></param>       
        void Set(string key, object value, int database);

        /// <summary>
        /// Retrieve type T object from cache.
        /// </summary>
        /// <param name="key"></param>
        /// <param name="database"></param>
        /// <returns></returns>
        T Get<T>(string key, int database);

        /// <summary>
        /// Remove key from cache.
        /// </summary>
        /// <param name="key"></param>
        /// <param name="database"></param>
        /// <returns></returns>
        bool Remove(string key, int database);

        /// <summary>
        /// Removes all keys from all the databases
        /// </summary>        
        /// <returns></returns>
        Task FlushAllKeys();

        /// <summary>
        /// Removes all keys from all the databases for the connection string provided
        /// </summary>        
        /// <returns></returns>
        Task FlushAllKeys(string connectionString);

        /// <summary>
        /// Removes all keys from the database specified
        /// </summary>        
        /// <returns></returns>
        Task FlushAllKeys(int database);

        /// <summary>
        /// Removes all keys from the database specified and for the connection string provided
        /// </summary>        
        /// <returns></returns>
        Task FlushAllKeys(int database, string connectionString);
    }
}
