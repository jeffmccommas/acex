﻿using System;
using CE.AO.Models;

namespace AO.BusinessContracts
{
    /// <summary>
    /// INotificationFacade interface is used to fetch/insert/update the notifiaction details from/to the database and send the notifications through email & sms.
    /// </summary>
    public interface INotificationFacade
    {
        LogModel LogModel { get; set; }

		/// <summary>
		/// Fetch/insert/update the notifiaction details from/to the database and send the notifications through email & sms
		/// </summary>
		/// <param name="clientId">clientId of the data to be fetched</param>
		/// <param name="customerId">customerId of the data to be fetched</param>
		/// <param name="accountId">accountId of the data to be fetched</param>
		/// <param name="serviceContractId">serviceContractId of the data to be fetched</param>
		/// <param name="programName">programName of the data to be fetched</param>
		/// <param name="asOfEvaluationId">unique evaluation id</param>
		void ProcessNotification(int clientId, string customerId, string accountId, string serviceContractId, string programName, Guid? asOfEvaluationId);
    }
}
