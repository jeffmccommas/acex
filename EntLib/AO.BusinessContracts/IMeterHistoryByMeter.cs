﻿using System.Collections.Generic;
using System.Threading.Tasks;
using CE.AO.Models;

namespace AO.BusinessContracts
{
	public interface IMeterHistoryByMeter
	{
		LogModel LogModel { get; set; }

		/// <summary>
		/// To insert/update the MeterHistoryByMeterModel to the database
		/// </summary>
		/// <param name="model">MeterHistoryByMeterModel model which is to be inserted/updated</param>
		/// <returns>True</returns>
		Task<bool> InsertOrMergeMeterHistoryByMeterAsync(MeterHistoryByMeterModel model);

		/// <summary>
		/// To fetch the MeterHistoryByMeter details from datatbase
		/// </summary>
		/// <param name="pageSize"></param>
		/// <param name="token"></param>
		/// <param name="clientId"></param>
		/// <returns>List of MeterHistoryByMeterModel</returns>
		IEnumerable<MeterHistoryByMeterModel> GetMeterHistoryByMeterList(int pageSize, ref byte[] token, int clientId);

		/// <summary>
		/// To delete the MeterHistoryByMeter data in batch to the database
		/// </summary>
		/// <param name="meterHistoryByMeter">MeterHistoryByMeter data which is to be deleted</param>
		Task DeleteMeterHistoryByMeterAsync(MeterHistoryByMeterModel meterHistoryByMeter);
	}
}
