﻿using System.Threading.Tasks;
using CE.AO.Logging;
using CE.AO.Models;

namespace AO.BusinessContracts
{
    /// <summary>
    /// ITrumpiaRequestDetail interface is used to insert/update the trumpia request details to the database
    /// </summary>
    public interface ITrumpiaRequestDetail
    {
        /// <summary>
        /// To fetch the trumpia request detail from the database
        /// </summary>
        /// <param name="requestId">requestId of the data to be fetched</param>
        /// <returns>TrumpiaRequestDetailEntity</returns>
        TrumpiaRequestDetailModel GetTrumpiaRequestDetail(string requestId);

        /// <summary>
        /// To fetch the trumpia request detail from the database
        /// </summary>
        /// <param name="requestId">requestId of the data to be fetched</param>
        /// <returns>TrumpiaRequestDetailEntity</returns>
        Task<TrumpiaRequestDetailModel> GetTrumpiaRequestDetailAsync(string requestId);

        /// <summary>
        /// To insert or update TrumpiaRequestDetailEntity to database
        /// </summary>
        /// <param name="entity">entity to be inserted</param>
        /// <returns>bool</returns>
        bool InsertOrMergeTrumpiaRequestDetail(TrumpiaRequestDetailModel entity);

        /// <summary>
        /// To insert or update TrumpiaRequestDetailEntity to database
        /// </summary>
        /// <param name="entity">entity to be inserted</param>
        /// <returns>bool</returns>
        Task<bool> InsertOrMergeTrumpiaRequestDetailAsync(TrumpiaRequestDetailModel entity);
        Task<bool> DeleteTrumpiaReqeustnAsync(string request);
    }
}
