﻿using AO.DTO;
using CE.AO.Models;

namespace AO.BusinessContracts
{
    /// <summary>
    /// This is generic interface.It can be used for triggers. Currently it is used for VeeTrigger.
    /// </summary>
    /// <typeparam name="TriggerType"></typeparam>
    public interface IProcessTrigger<in TriggerType> where TriggerType : TriggerDto
    {
        LogModel LogModel { get; set; }
        void Trigger(TriggerType triggerDto);
    }
}
