﻿using System;
using System.Threading.Tasks;
using CE.AO.Logging;
using CE.AO.Models;

namespace AO.BusinessContracts
{
    /// <summary>
    /// IAmiReading interface is used to insert/update/fetch ami reading data to/from the database
    /// </summary>
    public interface IAmiReading
    {
        LogModel LogModel { get; set; }

        /// <summary>
        /// To insert/update the NccAmiReadingModel to the database
        /// </summary>
        /// <param name="model">NccAmiReadingModel model to be inserted/updated</param>
        /// <returns>True</returns>
        Task<bool> InsertOrMergeAmiReadingAsync(NccAmiReadingModel model);

        /// <summary>
        /// To fetch the NccAmiReadingModel model from the database
        /// </summary>
        /// <param name="clientId">Client Id of the model to be fetched</param>
        /// <param name="meterId">Meter Id of the model to be fetched</param>
        /// <param name="amiTimeStamp">Ami time stamp of the model to be fetched</param>
        /// <returns>NccAmiReadingModel model</returns>
        Task<NccAmiReadingModel> GetAmiReading(int clientId, string meterId, DateTime amiTimeStamp);

        /// <summary>
        /// Delete Ami Reading
        /// </summary>
        /// <param name="clientId"></param>
        /// <param name="meterId"></param>
        /// <returns></returns>
        Task<bool> DeleteAmiReadingAsync(int clientId, string meterId);
    }
}
