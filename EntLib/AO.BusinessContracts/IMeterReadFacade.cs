﻿using System.Threading.Tasks;
using CE.AO.Logging;
using CE.AO.Models;

namespace AO.BusinessContracts
{
    /// <summary>
    /// IMeterReadFacade interface is used to insert/update the meter reads to the database
    /// </summary>
    public interface IMeterReadFacade
    {
       /// <summary>
        /// To insert or update Meter Reads to database
        /// </summary>
        /// <param name="model">model to be inserted</param>
        /// <returns>true</returns>
        Task<bool> InsertOrMergeMeterReadsAsync(MeterReadModel model);
    }
}
