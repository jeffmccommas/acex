﻿using System.Threading.Tasks;
using CE.AO.Logging;
using CE.AO.Models;

namespace AO.BusinessContracts
{
    public interface IEmailRequestDetail
    {
        LogModel LogModel { get; set; }

        EmailRequestDetailModel GetEmailRequestDetail(int clientId, string accountId, string programName,
            string serviceContractId, string insightName, string defaultCommunicationChannel, string date,
            string eventType);

        Task<EmailRequestDetailModel> GetEmailRequestDetailAsync(int clientId, string accountId, string programName,
            string serviceContractId, string insightName, string defaultCommunicationChannel, string date);

        bool InsertOrMergeEmailRequestDetail(EmailRequestDetailModel model);
        Task<bool> InsertOrMergeEmailRequestDetailAsync(EmailRequestDetailModel model);
    }
}
