﻿using System;
using System.Threading.Tasks;
using CE.AO.Models;

namespace AO.BusinessContracts
{
    /// <summary>
    /// IReport interface is used to export the reports
    /// </summary>
    public interface IReport
    {
        LogModel LogModel { get; set; }

        /// <summary>
        /// Export the insight report to the blob
        /// </summary>
        /// <param name="clientId">Client id for which report is to be exported</param>
        /// <param name="asOfDate">Date for which report is to be exported</param>
        /// <returns></returns>
        Task<bool> ExportInsights(int clientId, DateTime asOfDate);

        /// <summary>
        /// Export the notifications report to the blob
        /// </summary>
        /// <param name="clientId">Client id for which report is to be exported</param>
        /// <param name="asOfDate">Date for which report is to be exported</param>
        /// <returns></returns>
        Task<bool> ExportNotifications(int clientId,  DateTime asOfDate);
    }
}