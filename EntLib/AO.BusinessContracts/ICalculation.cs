﻿using System.Threading.Tasks;
using CE.AO.Logging;
using CE.AO.Models;

namespace AO.BusinessContracts
{
    /// <summary>
    /// ICalculation interface is used to insert/fetch the BTD and CTD calculation to/from the database
    /// </summary>
    public interface ICalculation
    { 
        LogModel LogModel { get; set; }

        /// <summary>
        /// To insert the CTD calculation model to the database
        /// </summary>
        /// <param name="model">CalculationModel model to be inserted</param>
        /// <returns>True</returns>
        Task InsertCtdCalculationAsync(CalculationModel model);

        /// <summary>
        /// To insert th BTD calculation model to the database
        /// </summary>
        /// <param name="model">CalculationModel model to be inserted</param>
        /// <returns>True</returns>
        Task InsertBtdCalculationAsync(CalculationModel model);

        /// <summary>
        /// To fetch BTD calculation model from the database
        /// </summary>
        /// <param name="clientId">Client id of the CalculationModel model</param>
        /// <param name="accountId">Account id of the CalculationModel model</param>
        /// <returns>CalculationModel</returns>
        Task<CalculationModel> GetBtdCalculationAsync(int clientId, string accountId);

        /// <summary>
        /// To fetch CTD calculation model from the database
        /// </summary>
        /// <param name="clientId">Client id of the CalculationModel model</param>
        /// <param name="accountId">Account id of the CalculationModel model</param>
        /// <param name="serviceContractId">Service contract id of the CalculationModel model</param>
        /// <returns>CalculationModel</returns>
        Task<CalculationModel> GetCtdCalculationAsync(int clientId, string accountId, string serviceContractId);

        /// <summary>
        /// Delete ctd Data from Calculation Table
        /// </summary>
        /// <param name="clientId"></param>
        /// <param name="accountId"></param>
        /// <returns></returns>
        Task DeleteCtdCalculationAsyc(int clientId, string accountId);

        /// <summary>
        /// Delete btd Data from Calculation Table
        /// </summary>
        /// <param name="clientId"></param>
        /// <param name="accountId"></param>
        /// <returns></returns>
        Task DeleteBtdCalculationAsyc(int clientId, string accountId);
    }
}
