﻿using System.Collections.Generic;
using CE.AO.Models;

namespace AO.BusinessContracts
{
    /// <summary>
    /// IClientWidgetConfiguration interfaceis used to fetch the data for ClientWidgetConfigurationModel model
    /// </summary>
    public interface IClientWidgetConfiguration
    {
        /// <summary>
        /// To fetch list of ClientWidgetConfigurationModel model
        /// </summary>
        /// <param name="clientId">Client id for which data to be fetched</param>
        /// <returns></returns>
        IEnumerable<ClientWidgetConfigurationModel> GetClientWidgetConfiguration(int clientId);
    }
}
