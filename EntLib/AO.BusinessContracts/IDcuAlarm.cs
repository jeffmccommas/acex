﻿using System.Threading.Tasks;
using CE.AO.Logging;
using CE.AO.Models;
namespace AO.BusinessContracts
{
    public interface IDcuAlarm
    {
        /// <summary>
        /// To insert/update the DcuAlarmModel in dcu_alarms table.
        /// </summary>
        /// <param name="model">DcuAlarmModel which is to be inserted/updated</param>
        /// <returns>True</returns>
        Task InsertOrMergeDcuAlarmAsync(DcuAlarmModel model);

        /// <summary>
        /// To delete the DcuAlarmModel in dcu_alarms table.
        /// </summary>
        /// <param name="model">DcuAlarmModel which is to be deleted</param>
        /// <returns>True</returns>
        Task DeleteDcuAlarmAsync(DcuAlarmModel model);
    }
}
