﻿using System;
using System.Threading.Tasks;
using CE.AO.Logging;
using CE.AO.Models;

namespace AO.BusinessContracts
{
    /// <summary>
    /// IBillingCycleSchedule interface is used to fetch the data for BillCycleScheduleModel model
    /// </summary>
    public interface IBillingCycleSchedule
    {
        LogModel LogModel { get; set; }

        /// <summary>
        /// To fetch billing cycle schedule by date
        /// </summary>
        /// <param name="clientId">Client Id of model to be fetched</param>
        /// <param name="billCycleId">Bill cycle id of model to be fetched</param>
        /// <param name="date">Date of model to be fetched</param>
        /// <returns></returns>
        BillCycleScheduleModel GetBillingCycleScheduleByDateAsync(int clientId, string billCycleId,
            DateTime date);
        Task<bool> InsertOrMergeBillingCycleScheduleAsync(BillCycleScheduleModel billCycleScheduleModel);

        Task<bool> DeleteHistoryBillingCycleScheduleAsyc(string clientId, string billCycleScheduleId, string beginDate,
            string endDate);
    }
}
