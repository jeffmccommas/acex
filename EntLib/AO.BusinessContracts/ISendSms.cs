﻿using System;
using System.Threading.Tasks;
using CE.AO.Models;

namespace AO.BusinessContracts
{
    public interface ISendSms
    {
        Task<string> Send(string apiKey, string username, string smsBody, string phone, string subscriptionId, string customKey,
            int clientId, string accountId, string programName, string serviceContractId, string insightTypeName,
            DateTime requestDate, DateTime? notifyDateTime = null); 

        string CreateNewTrumpiaSubscription(CustomerModel customerModel, string apiKey, string username,
            string contactList);

        string GetSubscriptionId(CustomerModel customerModel, string requestId, string apiKey, string username);

        Task<SmsStatsModel> GetSmsStatics(string requestId, string apiKey, string username);

        SubscriptionInfoResultModel GetSubscriptionInfo(string subscriptionId, string apiKey, string username);

        string UpdateSubscriptionInfo(CustomerModel customerModel, string subscriptionId, string apiKey, string username,
            string contactList);
    }
}
