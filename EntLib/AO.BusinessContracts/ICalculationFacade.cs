﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using CE.AO.Logging;
using CE.AO.Models;

namespace AO.BusinessContracts
{
    /// <summary>
    /// ICalculationFacade interface is used to calculate cost to date, calculate bill to date, schedule message and to fetch client accounts.
    /// </summary>
    public interface ICalculationFacade
    {
        LogModel LogModel { get; set; }

        /// <summary>
        /// To calculate the cost for given date
        /// </summary>
        /// <param name="clientId">Client Id for calculating cost</param>
        /// <param name="meterId">Meter Id for calculating cost</param>
        /// <param name="asOfDate">Date for which cost is to be calculated</param>
        /// <returns>Void</returns>
        Task CalculateCostToDate(int clientId, string meterId, DateTime asOfDate);

        /// <summary>
        /// To calculate bill to date, cost to date and insert into the database
        /// </summary>
        /// <param name="clientId">Client id for which bill is to be calculated</param>
        /// <param name="accountId">Account id for which bill is to be calculated</param>
        /// <param name="customerId">customer id for which bill is to be calculated</param>
        /// <param name="asOfDate">date for which bill is to be calculated</param>
        /// <returns>Void</returns>
        Task CalculateBillToDate(int clientId, string accountId, string customerId, DateTime asOfDate);

        /// <summary>
        /// To fetch the ClientAccountModels
        /// </summary>
        /// <param name="clientId">Client id of the ClientAccountModel</param>
        /// <returns>List of ClientAccountModel model</returns>
        IEnumerable<ClientAccountModel> GetClientAccount(int clientId);

        /// <summary>
        /// To send scheduled messages
        /// </summary>
        /// <param name="clientId">Client id for which message to be sent</param>
        /// <param name="asOfDate">Date for which message to be sent</param>
        void ScheduledCalculation(int clientId, string  asOfDate);
    }
}
