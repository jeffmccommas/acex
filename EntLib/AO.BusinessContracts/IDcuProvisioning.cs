﻿using System.Threading.Tasks;
using CE.AO.Models;

namespace AO.BusinessContracts
{
    /// <summary>
    /// IDcuProvisioning interface is used to insert/update the DcuProvisioningModel to the database
    /// </summary>
    public interface IDcuProvisioning
    {
        /// <summary>
        /// To insert/update the DcuProvisioningModel to the database
        /// </summary>
        /// <param name="model">DcuProvisioningModel which is to be inserted/updated</param>
        /// <returns>True</returns>
        Task InsertOrMergeDcuProvisioningAsync(DcuProvisioningModel model);

        /// <summary>
        /// To delete the DcuProvisioningModel from the database
        /// </summary>
        /// <param name="model">DcuProvisioningModel which is to be deleted</param>
        /// <returns>True</returns>
        Task DeleteDcuProvisioningAsync(DcuProvisioningModel model);
    }
}