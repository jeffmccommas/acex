﻿using System;
using CE.AO.Models;

namespace AO.BusinessContracts
{
    /// <summary>
    /// IEvaluationFacade interface is used for evaluation
    /// </summary>
    public interface IEvaluationFacade
    {
        LogModel LogModel { get; set; }

        /// <summary>
        /// To process the evaluation
        /// </summary>
        /// <param name="clientId">Client id to be processed</param>
        /// <param name="customerId">Customer id to be processed</param>
        /// <param name="accountId">Accout id to be processed</param>
        /// <param name="serviceContractId">Service contract id to be processed</param>
        /// <param name="asOfDate">Date for which record to be processed</param>
        void ProcessEvaluation(int clientId, string customerId, string accountId, string serviceContractId,DateTime asOfDate);
    }
}
