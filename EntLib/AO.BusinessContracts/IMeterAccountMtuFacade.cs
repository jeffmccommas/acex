﻿using System.Threading.Tasks;
using CE.AO.Models;

namespace AO.BusinessContracts
{
	/// <summary>
	/// Interface for MeterAccountMtuFacade
	/// </summary>
	public interface IMeterAccountMtuFacade
	{
		/// <summary>
		/// To insert in Alarms by fetching details from Alarmtypes
		/// </summary>
		/// <param name="model">AlarmModel to be inserted</param>
		/// <returns>true</returns>
		Task<bool> InsertOrMergeMeterAccountAsync(MeterAccountMtuModel model);
	}
}
