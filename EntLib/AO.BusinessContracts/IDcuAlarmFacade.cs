﻿using System.Threading.Tasks;
using CE.AO.Logging;
using CE.AO.Models;

namespace AO.BusinessContracts
{
    public interface IDcuAlarmFacade
    {
        LogModel LogModel { get; set; }

        /// <summary>
        /// To insert in all 4 tables ie dcu_alarms, dcu_alarms_by_code, dcu_alarms_history, dcu_alarms_by_code_history by fetching Mapping related information from dcu_alarms_mapping
        /// </summary>
        /// <param name="model">DcuAlarmModel to be inserted</param>
        /// <returns>true</returns>
        Task<bool> InsertOrMergeDcuAlarmAsync(DcuAlarmModel model);
    }
}
