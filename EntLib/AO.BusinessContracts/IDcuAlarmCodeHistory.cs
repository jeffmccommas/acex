﻿using System.Threading.Tasks;
using CE.AO.Logging;
using CE.AO.Models;
namespace AO.BusinessContracts
{
    public interface IDcuAlarmCodeHistory
    {
        LogModel LogModel { get; set; }

        /// <summary>
        /// To insert/update the DcuAlarmModel in dcu_alarms_by_code_history table
        /// </summary>
        /// <param name="model">DcuAlarmModel which is to be inserted/updated</param>
        /// <returns>True</returns>
        Task<bool> InsertOrMergeDcuAlarmCodeHistoryAsync(DcuAlarmModel model);

        /// <summary>
        /// To delete the DcuAlarmModel in dcu_alarms_by_code_history table
        /// </summary>
        /// <param name="model">DcuAlarmModel which is to be deleted</param>
        /// <returns>True</returns>
        Task<bool> DeleteDcuAlarmCodeHistoryAsync(DcuAlarmModel model);
        
        /// <summary>
        /// To delete the DcuAlarmModel with Partition in dcu_alarms_by_code_history table
        /// </summary>
        /// <param name="model">DcuAlarmModel which is to be deleted</param>
        /// <returns>True</returns>
        Task<bool> DeleteDcuAlarmCodeWithPartitionHistoryAsync(DcuAlarmModel model);


    }
}
