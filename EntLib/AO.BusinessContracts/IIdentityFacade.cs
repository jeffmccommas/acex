﻿using System;
using System.Collections.Generic;
using CE.AO.Models;

namespace AO.BusinessContracts
{
    /// <summary>
    /// IIdentityFacade is an interface that provides AD identity information about the current user.
    /// </summary>
    public interface IIdentityFacade
    {
        /// <summary>
        /// Return the current user's Tenant ID from the HttpContext.
        /// </summary>
        int GetClientIdForTenant(string tenantId);
    }
}