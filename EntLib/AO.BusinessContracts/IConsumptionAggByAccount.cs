﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using CE.AO.Logging;
using CE.AO.Models;

namespace AO.BusinessContracts
{
    /// <summary>
    /// IConsumptionAggByAccount interface is used to insert/update the ConsumptionAggByAccountModel to the database
    /// </summary>
    public interface IConsumptionAggByAccount
    {
        LogModel LogModel { get; set; }

        /// <summary>
        /// To insert/update the ConsumptionAggByAccountModel to database
        /// </summary>
        /// <param name="model">ConsumptionAggByAccountModel which is to be inserted/updated</param>
        /// <returns>True</returns>
        Task<bool> InsertOrMergeConsumptionAggByAccountAsync(ConsumptionAggByAccountModel model);

        /// <summary>
        /// To insert/update the ConsumptionAggByAccountModel to database in batch
        /// </summary>
        /// <param name="models">List of ConsumptionAggByAccountModel which are to be inserted/updated</param>        
        /// <returns>True</returns>
        Task<bool> InsertOrMergeConsumptionAggByAccountAsync(List<ConsumptionAggByAccountModel> models);

        /// <summary>
        /// To get the ConsumptionAggByAccountModels from database
        /// </summary>        
        /// <param name="clientId"></param>
        /// <param name="aggregationEndDateTime"></param>
        /// <param name="accountNumber"></param>        
        Task<IList<ConsumptionAggByAccountModel>> GetConsumptionAggByAccountAsync(int clientId,
            DateTime aggregationEndDateTime, string accountNumber);

        /// <summary>
        /// To delete the ConsumptionAggByAccountModel to the database
        /// </summary>
        /// <param name="model">ConsumptionAggByAccountModel which is to be deleted</param>
        /// <returns>True</returns>
        Task DeleteConsumptionAggByAccountAsync(ConsumptionAggByAccountModel model);

        /// <summary>
        /// To delete the ConsumptionAggByAccountModel to the database by Partition
        /// </summary>
        /// <param name="model">ConsumptionAggByAccountModel which is to be deleted</param>
        /// <returns>True</returns>
        Task DeleteConsumptionAggByAccountUsingPartionAsync(ConsumptionAggByAccountModel model);

    }
}
