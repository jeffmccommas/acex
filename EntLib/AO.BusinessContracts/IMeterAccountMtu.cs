﻿using System.Threading.Tasks;
using CE.AO.Logging;
using CE.AO.Models;

namespace AO.BusinessContracts
{
    /// <summary>
    /// IMeterAccountMtu interface is used to insert/update the MeterAccountMtuModel to the database
    /// </summary>
    public interface IMeterAccountMtu
    {
        LogModel LogModel { get; set; }

        /// <summary>
        /// To insert/update the MeterAccountMtuModel to the database
        /// </summary>
        /// <param name="model">MeterAccountMtuModel which is to be inserted/updated</param>
        /// <returns>True</returns>
        Task<bool> InsertOrMergeMeterAccountMtuAsync(MeterAccountMtuModel model);

        /// <summary>
        /// To delete the MeterAccountMtuModel from the database
        /// </summary>
        /// <param name="model">MeterAccountMtuModel which is to be deleted</param>
        /// <returns>True</returns>
        Task<bool> DeleteMeterAccountMtuAsync(MeterAccountMtuModel model);
    }
}