﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using CE.AO.Models;

namespace AO.BusinessContracts
{
    /// <summary>
    /// ISubscription interface is used to fetch/insert/update the subscription details from/to the database
    /// </summary>
    public interface ISubscription
    {
        /// <summary>
        ///  To insert or update SubscriptionModel to database
        /// </summary>
        /// <param name="model">model to be inserted</param>
        /// <param name="addEmailConfirmationCount">Used to set EmailConfirmationCount</param>
        /// <param name="addSmsConfirmationCount">Used to set SmsConfirmationCount</param>
        /// <returns>bool</returns>
        Task<bool> InsertSubscriptionAsync(SubscriptionModel model, bool addEmailConfirmationCount, bool addSmsConfirmationCount);

        /// <summary>
        /// Map SubscriptionModel to SubscriptionEntity. If subscription already present then delete it and then insert new subscription.
        /// </summary>
        /// <param name="model">model to be inserted</param>
        /// <returns>true</returns>
        bool InsertSubscription(SubscriptionModel model);

        /// <summary>
        /// To fetch the subscription for the customer from the database
        /// </summary>
        /// <param name="clientId">clientId of the data to be fetched</param>
        /// <param name="customerId">customerId of the data to be fetched</param>
        /// <param name="accountId">accountId of the data to be fetched</param>
        /// <returns>List of SubscriptionModel</returns>
        IEnumerable<SubscriptionModel> GetCustomerSubscriptions(int clientId, string customerId, string accountId);

        /// <summary>
        /// To fetch the latest subscription from the database
        /// </summary>
        /// <param name="clientId">clientId of the data to be fetched</param>
        /// <param name="customerId">customerId of the data to be fetched</param>
        /// <param name="accountId">accountId of the data to be fetched</param>
        /// <param name="programName">programName of the data to be fetched</param>
        /// <param name="serviceContractId">serviceContractId of the data to be fetched</param>
        /// <param name="insightTypeName">insightTypeName of the data to be fetched</param>
        /// <returns>SubscriptionEntity</returns>
        Task<SubscriptionModel> GetSubscriptionDetailsAsync(int clientId, string customerId,
            string accountId, string programName, string serviceContractId, string insightTypeName);

        /// <summary>
        /// To fetch the latest subscription from the database
        /// </summary>
        /// <param name="clientId">clientId of the data to be fetched</param>
        /// <param name="customerId">customerId of the data to be fetched</param>
        /// <param name="accountId">accountId of the data to be fetched</param>
        /// <param name="programName">programName of the data to be fetched</param>
        /// <param name="serviceContractId">serviceContractId of the data to be fetched</param>
        /// <param name="insightTypeName">insightTypeName of the data to be fetched</param>
        /// <returns>SubscriptionEntity</returns>
        SubscriptionModel GetSubscriptionDetails(int clientId, string customerId, string accountId, string programName, string serviceContractId,
        string insightTypeName);

        /// <summary>
        /// Merge default subscriptions with other subscribed subscription
        /// </summary>
        /// <param name="subscriptions">List of subscribed subscriptions</param>
        /// <param name="defaultSubscriptions">List of default subscriptions</param>
        /// <returns>List of SubscriptionModel</returns>
        List<SubscriptionModel> MergeDefaultSubscriptionListWithSubscribedPrograms(List<SubscriptionModel> subscriptions, List<SubscriptionModel> defaultSubscriptions);

        /// <summary>
        /// Get all account level subscription within the specified date range
        /// </summary>
        /// <param name="clientId">clientId of the data to be fetched</param>
        /// <param name="startDate">start date for date range</param>
        /// <param name="endDate">end date for date range</param>
        /// <param name="program">subscription program</param>
        /// <returns>List of SubscriptionEntity</returns>
        IEnumerable<SubscriptionModel> GetSubscription(int clientId, DateTime startDate, DateTime endDate, string program);
        Task<bool> DeleteSubscriptionAsync(int clientId, string type, string customerId, string accountId);

        /// <summary>
        /// If customer opted out for sms then insert or update subscription with same in database
        /// </summary>
        /// <param name="billingModel"></param>
        /// <param name="subscriptions"></param>
        /// <param name="clientId">clientId of the data to be fetched</param>
        /// <param name="customerId">customerId of the data to be fetched</param>
        /// <param name="accountId">accountId of the data to be fetched</param>
        /// <param name="serviceContractId">serviceContractId of the data to be fetched</param>
        /// <param name="programName">programName of the data to be fetched</param>
        /// <param name="insightName">insightName of the data to be fetched</param>
        /// <returns>bool</returns>
        bool InsertorMergeForSmsOptOut(BillingModel billingModel, List<SubscriptionModel> subscriptions, int clientId, string customerId, string accountId, string serviceContractId, string programName, string insightName);
    }
}
