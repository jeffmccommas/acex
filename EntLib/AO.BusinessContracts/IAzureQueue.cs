﻿using System.Threading.Tasks;

namespace AO.BusinessContracts
{
    public interface IAzureQueue
    {
        /// <summary>
        /// Adds queue message to batch process queue
        /// </summary>        
        Task AddMessageToBatchQueue(string message);

        /// <summary>
        /// Adds queue message to calculation process queue
        /// </summary>
        Task AddMessageToCalculationQueue(string message);
    }
}
