﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using CE.AO.Models;
using CE.RateModel;

namespace AO.BusinessContracts
{
    /// <summary>
    /// INotification interface is used to fetch/insert/update the notifiaction details from/to the database and send the notifications through email & sms.
    /// </summary>
    public interface INotification
    {
        /// <summary>
        /// Send notifications using email/sms
        /// </summary>
        /// <param name="subscriptions">List of subscription to send notification</param>
        /// <param name="evaluation">Insight model</param>
        /// <param name="clientSettings">Client settings</param>
        /// <param name="customerModel">Customer model</param>
        /// <param name="billingModel">Billing model</param>
        /// <param name="tierBoundariesWithServiceContractId">Dictionary of list of TierBoundary</param>
        /// <param name="calculations">List of CalculationModel model</param>
        /// <returns></returns>
        Task ProcessMessage(List<SubscriptionModel> subscriptions, InsightModel evaluation, ClientSettings clientSettings,
            CustomerModel customerModel, BillingModel billingModel, Dictionary<string, List<TierBoundary>> tierBoundariesWithServiceContractId, List<CalculationModel> calculations);

        /// <summary>
        /// To fetch all the notification details of specific date from the database
        /// </summary>
        /// <param name="clientId">clientId of the data to be fetched</param>
        /// <param name="customerId">customerId of the data to be fetched</param>
        /// <param name="accountId">accountId of the data to be fetched</param>
        /// <param name="asOfDate">date  of the data to be fetched</param>
        /// <returns>List of NotificationModel</returns>
        IEnumerable<NotificationModel> GetDayNotifications(int clientId, string customerId, string accountId, DateTime asOfDate);

        /// <summary>
        /// To fetch the notification detail from the database
        /// </summary>
        /// <param name="clientId">clientId of the data to be fetched</param>
        /// <param name="customerId">customerId of the data to be fetched</param>
        /// <param name="accountId">accountId of the data to be fetched</param>
        /// <param name="programName">programName of the data to be fetched</param>
        /// <param name="serviceContractId">serviceContractId of the data to be fetched</param>
        /// <param name="insightName">insightTypeName of the data to be fetched</param>
        /// <param name="defaultCommunicationChannel">default communication channel of the data to be fetched</param>
        /// <param name="isLatest">whether it is latest or history record</param>
        /// <param name="date">history record date</param>
        /// <returns>NotificationModel</returns>
        NotificationModel GetNotificationSync(int clientId, string customerId, string accountId, string programName,
            string serviceContractId, string insightName, string defaultCommunicationChannel, bool isLatest, string date);

        /// <summary>
        /// Retrieves fully hydrated notification based on the supplied notification model.
        /// </summary>
        /// <param name="notification">Notification model</param>
        /// <returns>Fully hydrated notification model</returns>
        NotificationModel GetNotificationSync(NotificationModel notification);

        /// <summary>
        /// To fetch the sms template from database
        /// </summary>
        /// <param name="clientId">clientId of the data to be fetched</param>
        /// <param name="insightTypeName">insight type of the data to be fetched</param>
        /// <param name="smsTemplateId">smsTemplateId of the template to be fetched</param>
        /// <returns>SmsTemplateModel</returns>
        Task<SmsTemplateModel> GetSmsTemplateFromDB(int clientId, string insightTypeName, string smsTemplateId);

        /// <summary>
        /// To fetch the sms template for subscription double opt in confirmation with replaced substitutions
        /// </summary>
        /// <param name="smsBody">body of the sms</param>
        /// <param name="subscriptionModel">subscription for double opt in confirmation</param>
        /// <param name="clientSetting">client setting for double opt in confirmation</param>
        /// <param name="customerModel"></param>
        /// <param name="substitutes">substitutes needs to replaced with actual value</param>
        /// <returns>sms body</returns>
        string GetReplacedSmsTemplateForSubscriptionDoubleOptInConfirmation(string smsBody,
            SubscriptionModel subscriptionModel, ClientSettings clientSetting, CustomerModel customerModel,
            MatchCollection substitutes);

        /// <summary>
        /// To fetch sms report for the user
        /// </summary>
        /// <param name="billingModel">billingModel for report to be fetched</param>
        /// <param name="subscriptions">subscriptions for report to be fetched</param>
        /// <param name="notificationModel">notificationModel for report to be fetched</param>
        /// <param name="insightModel">insightModel for report to be fetched</param>
        /// <param name="customerModel">customerModel for report to be fetched</param>
        /// <param name="subscriptionId">subscriptionId for report to be fetched</param>
        /// <param name="apiKey">apiKey for report to be fetched</param>
        /// <param name="username">user name for report to be fetched</param>
        /// <param name="requestedDate">request date for report to be fetched</param>
        /// <param name="retry">count to retry</param>
        /// <returns>task</returns>
        Task GetSmsReport(BillingModel billingModel, List<SubscriptionModel> subscriptions, NotificationModel notificationModel, InsightModel insightModel, CustomerModel customerModel,
            string subscriptionId, string apiKey, string username, DateTime requestedDate, int? retry = 0);

        /// <summary>
        /// Get notification for client within provided date range
        /// </summary>
        /// <param name="clientId">clientId of the data to be fetched</param>
        /// <param name="startDate">start date for date range</param>
        /// <param name="endDate">end date for date range</param>
        /// <returns>List of NotificationModel</returns>
        IEnumerable<NotificationModel> GetNotifications(int clientId, DateTime startDate, DateTime endDate);
        
        /// <summary>
        /// To insert or update latest NotificationModel to database
        /// </summary>
        /// <param name="notificationModel">model to be inserted</param>
        /// <param name="forceLatest">Forces the notification to be saved with IsLatest flag set to true</param>
        /// <returns>bool</returns>
        void SaveNotification(NotificationModel notificationModel, bool forceLatest = false);

        Task DeleteNotificationAsync(int clientId, string customerId, string accountId);

        /// <summary>
        /// Retrieves all scheduled notifications for all clients
        /// </summary>
        IList<NotificationModel> GetScheduledNotifications();
    }
}
