﻿using System.Threading.Tasks;
using CE.AO.Models;
namespace AO.BusinessContracts
{
   public interface IDcuAlarmType
	{
        /// <summary>
        /// Get DcuAlarmMapping information of the specific code from dcu_alarms_mapping table
        /// </summary>
        /// <param name="alarmCode"> alarmCode from which Mapping is fetched </param>
        /// <returns>True</returns>
		DcuAlarmTypeModel GetDcuAlarmType(string alarmCode);

        /// <summary>
        /// To insert/update the DcuAlarmMappingModel to database
        /// </summary>
        /// <param name="model">DcuAlarmMappingModel which is to be inserted/updated</param>
        /// <returns>True</returns>
        /// 
        Task InsertOrMergeDcuAlarmTypeAsync(DcuAlarmTypeModel model);

        /// <summary>
        /// To delete the DcuAlarmMappingModel from database
        /// </summary>
        /// <param name="model">DcuAlarmMappingModel which is to be deleted</param>
        /// <returns>True</returns>
        Task<bool> DeleteDcuAlarmTypeAsync(DcuAlarmTypeModel model);
    }
}
