﻿using System.Threading.Tasks;
using CE.AO.Logging;
using CE.AO.Models;


namespace AO.BusinessContracts
{
    /// <summary>
    /// Interface declaration for Alarm
    /// </summary>
    public interface IAlarm 
    {
        /// <summary>
        /// To insert/update the AlarmModel in alarms table.
        /// </summary>
        /// <param name="model">AlarmModel which is to be inserted/updated</param>
        /// <returns>True</returns>
        Task<bool> InsertOrMergeAlarmAsync(AlarmModel model);

        /// <summary>
        /// To delete the AlarmModel in alarms table.
        /// </summary>
        /// <param name="model">AlarmModel which is to be deleted</param>
        /// <returns>True</returns>
        Task<bool> DeleteAlarmAsync(AlarmModel model);

    }
}
