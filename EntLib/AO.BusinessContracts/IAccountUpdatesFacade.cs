﻿using System.Threading.Tasks;
using CE.AO.Logging;
using CE.AO.Models;

namespace AO.BusinessContracts
{
    /// <summary>
    /// IAccountUpdatesFacade interface is used to insert/update the AccountUpdatesModel model to the database
    /// </summary>
    public interface IAccountUpdatesFacade
    {
        LogModel LogModel { get; set; }

        /// <summary>
        /// To insert/update AccountUpdatesModel model to the database 
        /// </summary>
        /// <param name="model">AccountUpdatesModel model to be inserted/updated</param>
        /// <returns>Insertion/Updation is successful or not</returns>
        Task<bool> InsertOrMergeAccountUpdateAsync(AccountUpdatesModel model);

        /// <summary>
        /// To insert/update AccountUpdatesModel model to the database 
        /// </summary>
        /// <param name="clientId">Client id of the model</param>
        /// <param name="customerId">Customer Id of the model</param>
        /// <param name="accountId">Account Id of the model</param>
        /// <param name="premiseId">Premise Id of the model</param>
        /// <param name="serviceContractId">Service Contract Id of the model</param>
        /// <param name="meterId">Meter Id of the model</param>
        /// <param name="newValue">New value of the model</param>
        /// <param name="replacedValue">Replaced value of the model</param>
        /// <param name="accountUpdateType">Account update type value of the model</param>
        /// <returns></returns>
        Task<bool> InsertOrMergeAccountUpdateAsync(int clientId, string customerId, string accountId, string premiseId,
            string serviceContractId, string meterId, string newValue, string replacedValue, string accountUpdateType);
    }
}
