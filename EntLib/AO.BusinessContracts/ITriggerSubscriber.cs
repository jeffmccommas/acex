﻿namespace AO.BusinessContracts
{
    /// <summary>
    /// Interface to Trigger Subscribers
    /// </summary>
    public interface ITriggerSubscriber
    {
        /// <summary>
        /// Subscribes to Generic trigerring mechanism eg: LogToCassandra TriggerSubscribers  and PreAggregation TriggerSubscribers
        /// </summary>
        void Subscribe();

        /// <summary>
        /// Unsubscribes to Generic trigerring mechanism eg: LogToCassandra TriggerSubscribers  and PreAggregation TriggerSubscribers
        /// </summary>
        void UnSubscribe();
    }
}
