﻿using System.Collections.Generic;
using CE.AO.Models;

namespace AO.BusinessContracts
{
    /// <summary>
    /// ISubscriptionFacade interface is used to fetch/insert/update the subscription details from/to the database
    /// </summary>
    public interface ISubscriptionFacade
    {
        /// <summary>
        /// Validate a subscription insights list
        /// </summary>
        /// <param name="clientId">The current client Id.</param>
        /// <param name="insights">The  list of subscription insights</param>
        /// <returns>A list of invalid insights.</returns>
        List<SubscriptionModel> ValidateInsightList(int clientId, List<SubscriptionModel> insights);

        /// <summary>
        /// Validate a subscription insights list
        /// </summary>
        /// <param name="clientId">The current client Id.</param>
        /// <param name="insights">The  list of subscription insights</param>
        /// <param name="custEmail">The Email Address of the Customer</param>
        /// <param name="custPhone">The Phone number of the Customer</param>
        /// <param name="insightErrMsg">The specific error found with the Insights.</param>
        /// <returns>A list of invalid insights.</returns>
        List<SubscriptionModel> ValidateInsightList(int clientId, List<SubscriptionModel> insights, string custEmail,
            string custPhone, ref string insightErrMsg);

        /// <summary>
        /// To fetch the all subscriptions those are subscribed from the database
        /// </summary>
        /// <param name="clientId">clientId of the data to be fetched</param>
        /// <param name="customerId">customerId of the data to be fetched</param>
        /// <param name="accountId">accountId of the data to be fetched</param>
        /// <returns>List of SubscriptionModel</returns>
        List<SubscriptionModel> GetDefaultListWithSubscribedPrograms(int clientId, string customerId, string accountId);

        /// <summary>
        /// Get Subscribe List
        /// </summary>
        /// <param name="clientId">The current client Id</param>
        /// <param name="customerId">The id of customer</param>
        /// <param name="accountId">The account id of customer</param>
        /// <param name="insights">The  list of subscription insights</param>
        /// <returns>List of SubscriptionModel</returns>
        List<SubscriptionModel> GetSubscribeList(int clientId, string customerId, string accountId, List<SubscriptionModel> insights);

        /// <summary>
        /// Get Subscribe List
        /// </summary>
        /// <param name="clientId">The current client Id</param>
        /// <param name="customerId">The id of the customer</param>
        /// <param name="accountId">The account id of the customer</param>
        /// <param name="insights">The  list of subscription insights</param>
        /// <param name="insightErrMsg">The specific error found with the Insights.</param>
        /// <returns>List of SubscriptionModel</returns>
        List<SubscriptionModel> GetSubscribeList(int clientId, string customerId, string accountId, List<SubscriptionModel> insights, ref string insightErrMsg);

        /// <summary>
        /// Opt in for email and sms
        /// </summary>
        /// <param name="subscription">Opt in for the subscription</param>
        TrumpiaRequestDetailModel DoubleOptin(SubscriptionModel subscription);

        /// <summary>
        /// check if the subscription in trumpia is blocked or deleted
        /// if so, update trumpia request detail with the info
        /// </summary>
        /// <returns></returns>
        TrumpiaRequestDetailModel TrumpiaSubscriptionCheck(string requestId, string subscriptionId, string apiKey,
            string username);
    }
}
