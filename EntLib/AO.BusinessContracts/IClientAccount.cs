﻿using System.Collections.Generic;
using System.Threading.Tasks;
using CE.AO.Logging;
using CE.AO.Models;
using Microsoft.WindowsAzure.Storage.Table;

namespace AO.BusinessContracts
{
    /// <summary>
    /// IClientAccount interface is used to fetch/insert/update the clientaccount details from/to the database
    /// </summary>
    public interface IClientAccount
    {
        LogModel LogModel { get; set; }

        /// <summary>
        /// To insert or update ClientAccountModel to database
        /// </summary>
        /// <param name="model">model to be inserted</param>
        /// <returns>true</returns>
        Task<bool> InsertOrMergeClientAccount(ClientAccountModel model);

        /// <summary>
        /// To fetch the clientaccount details from the database
        /// </summary>
        /// <param name="clientId">clientId of the data to be fetched</param>
        /// <returns>List of ClientAccountModel</returns>
        IEnumerable<ClientAccountModel> GetClientAccounts(int clientId);

        /// <summary>
        /// To schedule calculation add message to queue
        /// </summary>
        /// <param name="clientId">clientId to be send in queue</param>
        /// <param name="asOfDate">asOfDate to be send in queue</param>
        void SendScheduleMessage(int clientId, string asOfDate);

        /// <summary>
        /// To fetch the data for client account
        /// </summary>
        /// <param name="clientId">Client id for which data to be fetched</param>
        /// <param name="maxEntitiesToFetch">Number of records to be fetched</param>
        /// <param name="token">Table continuation token</param>
        /// <returns>List of ClientAccountModel model</returns>
        IEnumerable<ClientAccountModel> GetClientAccounts(int clientId, int maxEntitiesToFetch, ref TableContinuationToken token);

        /// <summary>
        /// To fetch the clientaccounts details from the database
        /// </summary>
        /// <param name="clientId">clientId of the data to be fetched</param>
        /// <param name="maxEntitiesToFetch">max number of entities to be fetched</param>
        /// <param name="token">token to be used</param>
        /// <returns>List of ClientAccountModel</returns>
        IEnumerable<ClientAccountModel> GetClientAccounts(int clientId, int maxEntitiesToFetch, ref byte[] token);

        /// <summary>
        /// Delete btd Data from Calculation Table
        /// </summary>
        /// <param name="clientId"></param>
        /// <param name="accountId"></param>
        /// <returns></returns>
        Task<bool> DeleteClientAccountAsyc(int clientId, string accountId);
    }
}