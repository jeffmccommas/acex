﻿using System;

// ReSharper disable InconsistentNaming

namespace AO.BusinessContracts.Preprocessing
{
    public class AmiReading
    {
        public int ClientId { get; set; }
        public string AccountNumber { get; set; }
        public string MeterId { get; set; }
        public string ServicePointId { get; set; }
        public int CommodityId { get; set; }
        public int UOMId { get; set; }
        public double? VolumeFactor { get; set; }
        public int? Direction { get; set; }
        public DateTime? ProjectedReadDate { get; set; }
        public string Timezone { get; set; }
        public DateTime AmiTimeStamp { get; set; }
        public double? Consumption { get; set; }
        public string BatteryVoltage { get; set; }
        // ReSharper disable once InconsistentNaming
        public string TOUBin { get; set; }
        public int TOUID { get; set; }
        public string TransponderId { get; set; }
        public string TransponderPort { get; set; }
        public string DialRead { get; set; }
        public string QualityCode { get; set; }
        public string Scenario { get; set; }
        public string ReadingType { get; set; }

        /// <summary>
        /// Customer Identifier
        /// </summary>
        public string CustomerId { get; set; }

        /// <summary>
        /// Interval of the Read
        /// </summary>
        public int IntervalType { get; set; }
    }
}
