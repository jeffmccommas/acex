﻿using CE.AO.Logging;
using CE.AO.Models;

namespace AO.BusinessContracts
{
    /// <summary>
    /// ITrumpiaKeywordConfiguration interface is used to fetch the trumpia keyword configuration details from the database
    /// </summary>
    public interface ITrumpiaKeywordConfiguration
    {
        /// <summary>
        ///  To fetch the trumpia keyword configuration detail from the database
        /// </summary>
        /// <param name="trumpiaKeyword">trumpiaKeyword of the data to be fetched</param>
        /// <returns>TrumpiaKeywordConfigurationModel</returns>
        TrumpiaKeywordConfigurationModel GetTrumpiaKeywordConfiguration(string trumpiaKeyword);
    }
}