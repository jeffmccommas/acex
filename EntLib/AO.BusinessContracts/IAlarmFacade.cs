﻿using System.Threading.Tasks;
using CE.AO.Logging;
using CE.AO.Models;

namespace AO.BusinessContracts
{
    /// <summary>
    /// Interface for AlarmFacade 
    /// </summary>
    public interface IAlarmFacade
    {
        /// <summary>
        /// To insert in Alarms by fetching details from Alarmtypes
        /// </summary>
        /// <param name="model">AlarmModel to be inserted</param>
        /// <returns>true</returns>
        Task<bool> InsertOrMergeAlarmAsync(AlarmModel model);
    }
}
