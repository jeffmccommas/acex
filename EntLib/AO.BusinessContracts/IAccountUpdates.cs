﻿using System.Threading.Tasks;
using CE.AO.Logging;
using CE.AO.Models;

namespace AO.BusinessContracts
{
    /// <summary>
    /// IAccountUpdates interface is used to fetch/insert/update the AccountUpdatesModel from/to the database
    /// </summary>
    public interface IAccountUpdates
    {
        LogModel LogModel { get; set; }

        /// <summary>
        /// To insert/update the AccountUpdatesModel to the database
        /// </summary>
        /// <param name="model">AccountUpdatesModel which is to be inserted/updated</param>
        /// <param name="history">Boolean which tells whether the AccountUpdatesModel is latest or not</param>
        /// <returns>True</returns>
        Task<bool> InsertOrMergeAccountUpdateAsync(AccountUpdatesModel model, bool history);

        /// <summary>
        /// To fetch the AccountUpdatesModel from the database
        /// </summary>
        /// <param name="client">Client Id of the model</param>
        /// <param name="customerId">Customer Id of the model</param>
        /// <param name="updateType">Update Type of the model</param>
        /// <param name="serviceContractId">Service Contract Id of the model</param>
        /// <param name="history">Boolean value which tells model to be fetched is latest or not</param>
        /// <returns>AccountUpdatesModel model</returns>
        Task<AccountUpdatesModel> GetAccountUpdate(int client, string customerId, string updateType, string serviceContractId,
            bool history);

        /// <summary>
        /// To delete the record from the database
        /// </summary>
        /// <param name="clientId">Client Id of the data to be delted</param>
        /// <param name="customerId">Customer Id of the data to be delted</param>
        /// <param name="updateType">Update Type of the data to be delted</param>
        /// <param name="serviceContractId">Service Contract Id of the data to be delted</param>
        /// <param name="isLatest">IsLatest of the data to be delted</param>
        /// <returns>True</returns>
        Task<bool> DeleteHistoryAccountUpdateAsyc(int clientId, string customerId, string updateType,
            string serviceContractId, bool isLatest);
    }
}
