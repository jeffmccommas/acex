﻿ // ReSharper disable once CheckNamespace
namespace AO.BusinessContracts
{
    public static partial class Enums
    {
        public enum CacheDatabases
        {
            DcuAlarmTypesDatabaseId = 0,
            AlarmTypesDatabaseId = 1,
            MtuTypesDatabaseId = 2,
            MeterType = 3
        }
    }
}
