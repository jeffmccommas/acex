﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using CE.AO.Models;
using CE.RateModel;

namespace AO.BusinessContracts
{
    /// <summary>
    /// ITallAMI interface is used to fetch/insert/update data from/to the database
    /// </summary>
    // ReSharper disable once InconsistentNaming
    public interface ITallAMI
    {
        LogModel LogModel { get; set; }

        /// <summary>
        /// To insert/update the TallAmiModel model to the database
        /// </summary>
        /// <param name="model">TallAmiModel model which is to be inserted/updated</param>
        /// <returns>True</returns>
        Task<bool> InsertOrMergeAmiAsync(TallAmiModel model);

        /// <summary>
        /// To insert/update the TallAmiModel model in a batch to the database
        /// </summary>
        /// <param name="tallAmiModels">List of TallAmiModel models which is to be inserted/updated</param>
        /// <returns>True</returns>
        Task<bool> InsertOrMergeBatchAsync(List<TallAmiModel> tallAmiModels);

        /// <summary>
        /// To insert/update the TallAmiModel model with touvalues in DB
        /// </summary>
        /// <param name="tallAmiModels">List of TallAmiModel models which is to be inserted/updated</param>
        /// <returns></returns>
        Task<bool> InsertOrMergeDailyAmiAsync(List<TallAmiModel> tallAmiModels);

        /// <summary>
        /// This method is used to fetch the Tall Ami details.
        /// The date input is expected as UTC.
        /// </summary>
        /// <param name="date">date of the data to be fetched</param>
        /// <param name="meterId">Meter Id of the data to be fetched</param>
        /// <param name="accountId">Account Id of the data to be fetched</param>
        /// <param name="clientId">Client Id of the data to be fetched</param>
        /// <returns>List of TallAmiModel</returns>
        IEnumerable<TallAmiModel> GetAmi(DateTime date, string meterId, string accountId, int clientId);

        /// <summary>
        /// This method is used to fetch the Tall Ami details based on start date and end date.
        /// The startDate and endDate are expected as UTC.
        /// </summary>
        /// <param name="startDate">Start date of the data to be fetched</param>
        /// <param name="endDate">End date of the data to be fetched</param>
        /// <param name="meterId">Meter Id of the data to be fetched</param>
        /// <param name="clientId">Client Id of the data to</param>
        /// <returns>Returns null if no AMI readings found. </returns>
        IEnumerable<TallAmiModel> GetAmiList(DateTime startDate, DateTime endDate, string meterId, int clientId);

        /// <summary>
        /// This method is used to fetch the Tall Ami details based on start date and end date.
        /// The startDate and endDate are expected as UTC.
        /// </summary>
        /// <param name="startDate">Start date of the data to be fetched</param>
        /// <param name="endDate">End date of the data to be fetched</param>
        /// <param name="meterId">Meter Id of the data to be fetched</param>
        /// <param name="accountId">Account Id of the data to be fetched</param>
        /// <param name="clientId">Client Id of the data to be fetched</param>
        /// <returns>List of TallAmiModel</returns>
        IEnumerable<TallAmiModel> GetAmiList(DateTime startDate, DateTime endDate, string meterId, string accountId,
            int clientId);

        /// <summary>
        /// This method is used to fetch the Tall Ami details based on start date and end date.
        /// The startDate and endDate are expected as UTC.
        /// </summary>
        /// <param name="startDate">Start date of the data to be fetched</param>
        /// <param name="endDate">End date of the data to be fetched</param>
        /// <param name="meterId">Meter Id of the data to be fetched</param>
        /// <param name="accountId">Account Id of the data to be fetched</param>
        /// <param name="clientId">Client Id of the data to be fetched</param>
        /// <param name="filterBYvalues">filterBYvalues of the data to be fetched</param>
        /// <returns>List of TallAmiModel</returns>
        IEnumerable<TallAmiModel> GetAmiList(DateTime startDate, DateTime endDate, string meterId,
            int clientId, IDictionary<string,string> filterBYvalues);


        /// <summary>
        /// To convert from ami list to Reading list
        /// 
        /// Note, not appropriate for a AO business object to return CE specific domain objects.  Re-factor this out of
        /// AO business object into a CE specific business object.
        /// </summary>
        /// <param name="amiList">List of model from which readings to be converted</param>
        /// <param name="noOfDays">Total number of days of which reading is returned</param>
        /// <param name="convertToLocalTimeZone">indicate if return data should be converted into local time zone</param>
        /// <returns>List of Reading</returns>
        List<Reading> GetReadings(IEnumerable<TallAmiModel> amiList, out int noOfDays, bool convertToLocalTimeZone = true);

        List<ReadingModel> GetReadings(IEnumerable<TallAmiModel> amiList);

        Task<bool> DeleteReadingAsync(int clientId, string meterId);
    }
}
