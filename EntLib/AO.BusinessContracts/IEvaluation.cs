﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using CE.AO.Models;
using CE.RateModel;

namespace AO.BusinessContracts
{
    /// <summary>
    /// IEvaluation interface is used to fetch/insert/update the evaluation from/to the database
    /// </summary>
    public interface IEvaluation
    {
        /// <summary>
        /// To evaluate insight, to save insight using subscription, calculation, ami, billing model
        /// </summary>
        /// <param name="subscriptions">List of SubscriptionModel model</param>
        /// <param name="calculations">List of CalculationModle model</param>
        /// <param name="clientSettings">Client settings</param>
        /// <param name="amiList">List of AMI</param>
        /// <param name="tierBoundaries">List of TierBoundary</param>
        /// <param name="asOfDate">Date for which evaluation should be done</param>
        /// <param name="billingmodel">BillingModel model</param>
        /// <returns></returns>
        Task EvaluateInsight(IEnumerable<SubscriptionModel> subscriptions, IEnumerable<CalculationModel> calculations,
            ClientSettings clientSettings, List<TallAmiModel> amiList, List<TierBoundary> tierBoundaries, DateTime asOfDate, BillingModel billingmodel);

        /// <summary>
        /// To fetch service level evaluation
        /// </summary>
        /// <param name="clientId">Client id for service level evaluation to be fetched</param>
        /// <param name="customerId">customer id for service level evaluation to be fetched</param>
        /// <param name="accountId">Account id for service level evaluation to be fetched</param>
        /// <param name="programName">Program name for service level evaluation to be fetched</param>
        /// <param name="serviceContractId">Service contract id for service level evaluation to be fetched</param>
        /// <returns>InsightModel model</returns>
        Task<InsightModel> GetServiceLevelEvaluationsAsync(int clientId, string customerId, string accountId, string programName, string serviceContractId);

        /// <summary>
        /// To fetch account level evaluation
        /// </summary>
        /// <param name="clientId">Client id for account level evaluation to be fetched</param>
        /// <param name="customerId">customer id for account level evaluation to be fetched</param>
        /// <param name="accountId">Account id for account level evaluation to be fetched</param>
        /// <param name="programName">Program name for account level evaluation to be fetched</param>
        /// <returns>InsightModel model</returns>
        Task<InsightModel> GetAccountLevelEvaluationsAsync(int clientId, string customerId, string accountId, string programName);

        /// <summary>
        /// To fetch list of all InsightModel model in a day
        /// </summary>
        /// <param name="clientId">Client id for which list to be fetched</param>
        /// <param name="customerId">customer id for which list to be fetched</param>
        /// <param name="accountId">Account id for which list to be fetched</param>
        /// <param name="asOfDate">Date for which list to be fetched</param>
        /// <returns>List of InsightModel model</returns>
        IEnumerable<InsightModel> GetDayInsights(int clientId, string customerId, string accountId,DateTime asOfDate);
    }
}
