﻿using System.Threading.Tasks;
using CE.AO.Models;
namespace AO.BusinessContracts
{
    /// <summary>
    /// Interface declaration for AlarmTypes
    /// </summary>
    public interface IAlarmTypes
    {
        /// <summary>
        /// Get AlarmType information from alarms table
        /// </summary>
        /// <param name="alarmSourceTypeId"> ALarm Source type Id </param>
        /// <param name="alarmGroupId"> Alarm Group Id </param>
        /// <param name="alarmTypeId"> Alarm Type Id </param>
        /// <returns>True</returns>
        AlarmTypeModel GetAlarmType(int alarmSourceTypeId, int alarmGroupId, int alarmTypeId);

        /// <summary>
        /// To insert/update the AlarmTypeModel to database
        /// </summary>
        /// <param name="model">AlarmTypeModel which is to be inserted/updated</param>
        /// <returns>True</returns>
        /// 
        Task<bool> InsertOrMergeAlarmTypeAsync(AlarmTypeModel model);

        /// <summary>
        /// To delete the AlarmTypeModel from database
        /// </summary>
        /// <param name="model">AlarmTypeModel which is to be deleted</param>
        /// <returns>True</returns>
        Task<bool> DeleteAlarmTypeAsync(AlarmTypeModel model);
    }
}
