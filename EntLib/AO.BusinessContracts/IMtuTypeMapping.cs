﻿using CE.AO.Models;
namespace AO.BusinessContracts
{
   public interface IMtuTypeMapping
	{
		/// <summary>
		/// Get MtuTypeMapping information of the specific mtu type from mtu_type table
		/// </summary>
		/// <param name="mtuTypeId"> mtuTypeId from which Mapping is fetched </param>
		/// <returns>True</returns>
		MtuTypeModel GetMtuTypeMapping(int mtuTypeId);
	}
}
