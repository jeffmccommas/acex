﻿namespace AO.BusinessContracts
{
    public interface IExportFacade
    {
        /// <summary>
        /// To export table from database to blob storage.
        /// </summary>
        /// <param name="blobConnectionString">Conncetion string for blob storage</param>
        /// <param name="containerName">Container name for the blob storage.</param>
        /// <param name="fileName">filename of export CSV file in blob storage.</param>
        /// <param name="filterQuery">Generalize SQL expression with filter condition, which will execute on database.</param>
        /// <param name="filterName">Name of the filter.</param>
        void ExportData(string blobConnectionString, string containerName,string fileName, string filterQuery, string filterName);
    }
}
