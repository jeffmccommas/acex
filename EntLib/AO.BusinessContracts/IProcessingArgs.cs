using System;
using System.Collections.Specialized;
using System.Xml;

namespace AO.BusinessContracts
{
    public interface IProcessingArgs
    {
        /// <summary>
        /// possible flesh out some missing parameters
        /// </summary>
        /// <param name="parsstg"></param>
        void AddIfMissing(string parsstg, bool overWrite = false);

        /// <summary>
        /// possible add and/override parameters
        /// </summary>
        /// <param name="parsstg"></param>
        void Override(string parsstg);

        /// <summary>
        /// possible add and/override parameters
        /// </summary>
        /// <param name="parsstg"></param>
        void Override(NameValueCollection parsstg);

        void ApplyCustomConfig();
        string GetCustomConfigFile();

        /// <summary>
        /// overwrite/create the root directory custom config file
        /// </summary>
        /// <param name="doc"></param>
        /// <returns></returns>
        string SetCustomConfig(XmlDocument doc);

        bool GetCustomConfig(bool checkIt, ref XmlDocument dataDoc, string pathFrag);
        
        string AsJson(bool justArgs = false, bool secure = false);

        /// <summary>
        /// make this smarter
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        bool IsAsserted(string key);

        string this[string key] { get; set; }
        string GetControlVal(string parName, string defVal);

        /// <summary>
        /// Get the control val for specified client
        /// </summary>
        /// <param name="clientId"></param>
        /// <param name="parName"></param>
        /// <param name="defVal"></param>
        /// <returns></returns>
        string GetClientControlVal(string clientId, string parName, string defVal);

        int GetControlVal(string parName, int defVal);
        DateTime GetControlVal(string parName, DateTime defVal);
    }
}