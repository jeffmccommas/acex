﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using CE.AO.Logging;
using CE.AO.Models;
using CE.RateModel;

namespace AO.BusinessContracts
{
    /// <summary>
    /// IAMI interface is used to fetch/insert/update the ami readings from/to the database
    /// </summary>
    public interface IAmi
    {
        LogModel LogModel { get; set; }

        /// <summary>
        /// To insert the Ami15MinuteIntervalModel model to the database
        /// </summary>
        /// <param name="model">Ami15MinuteIntervalModel model which is to be inserted</param>
        /// <returns>True</returns>
        Task<bool> InsertAmiAsync(Ami15MinuteIntervalModel model);

        /// <summary>
        /// To insert the Ami30MinuteIntervalModel model to the database
        /// </summary>
        /// <param name="model">Ami30MinuteIntervalModel model which is to be inserted</param>
        /// <returns>True</returns>
        Task<bool> InsertAmiAsync(Ami30MinuteIntervalModel model);

        /// <summary>
        /// To insert the Ami60MinuteIntervalModel model to the database
        /// </summary>
        /// <param name="model">Ami60MinuteIntervalModel model which is to be inserted</param>
        /// <returns>True</returns>
        Task<bool> InsertAmiAsync(Ami60MinuteIntervalModel model);

        /// <summary>
        /// To insert/update the model to the database
        /// </summary>
        /// <typeparam name="T">Type of model which is to be inserted/updated</typeparam>
        /// <param name="updateData">Model which is to be inserted/updated</param>
        /// <param name="interval">Interval of the data of the model</param>
        /// <returns>True</returns>
        Task<bool> InsertOrMergeAmiAsync<T>(T updateData, CE.AO.Utilities.Enums.IntervalType interval) where T : new();

        /// <summary>
        /// To fetch the list of models from the database
        /// </summary>
        /// <param name="startDate">Start date of the data to be fetched</param>
        /// <param name="endDate">End date of the data to be fetched</param>
        /// <param name="meterId">Meter Id of the data to be fetched</param>
        /// <param name="clientId">Client Id of the data to be fetched</param>
        /// <returns>List of model</returns>
        IEnumerable<dynamic> GetAmiList(DateTime startDate, DateTime endDate, string meterId, int clientId);


        IEnumerable<dynamic> GetAmiListByInterval(DateTime startDate, DateTime endDate, string meterId, int clientId, int intervalType);

        /// <summary>
        /// To fetch the single model from the database
        /// </summary>
        /// <param name="date">Date of the data to be fetched</param>
        /// <param name="meterId">Meter Id of the data to be fetched</param>
        /// <param name="clientId">Client Id of the data to be fetched</param>
        /// <returns>Model</returns>
        Task<dynamic> GetAmi(DateTime date, string meterId, int clientId);

        /// <summary>
        /// To convert from ami list to Reading list
        /// </summary>
        /// <param name="amiList">List of model from which readings to be converted</param>
        /// <param name="noOfDays">Total number of days of which reading is returned</param>
        /// <returns>List of Reading</returns>
        List<CE.RateModel.Reading> GetReadings(IEnumerable<dynamic> amiList, out int noOfDays);

        /// <summary>
        /// To convert from ami list to ReadingModel
        /// </summary>
        /// <param name="amiList">List of model from which readings to be converted</param>
        /// <param name="intervalType"></param>
        /// <returns>List of ReadingModel model</returns>
        List<CE.RateModel.Reading> GetDailyReadings(IEnumerable<dynamic> amiList, int intervalType);

        /// <summary>
        /// To get the list of daily Reading model
        /// </summary>
        /// <param name="amiList">List of ami from which reading is to be fetched</param>
        /// <param name="intervalType"></param>
        /// <returns>List of Reading model</returns>
        List<CE.RateModel.Reading> GetMonthlyReadings(IEnumerable<dynamic> amiList, int intervalType);

        /// <summary>
        /// To get the list of monthly Reading model
        /// </summary>
        /// <param name="amiList">List of ami from which reading is to be fetched</param>
        /// <returns>List of Reading model</returns>
        List<ReadingModel> GetReadings(IEnumerable<dynamic> amiList);

        List<Reading> GetAmiReadings(int clientId, string meterId, DateTime startDate, DateTime endDate,
            string requestedResolution, out int intervalType, out int amiUomId, out int amiCommodityId);
    }
}
