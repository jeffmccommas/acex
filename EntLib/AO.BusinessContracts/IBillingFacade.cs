﻿using System.Threading.Tasks;
using CE.AO.Logging;
using CE.AO.Models;

namespace AO.BusinessContracts
{
    /// <summary>
    /// IBillingFacade interface is used to insert/update the billing details to the database
    /// </summary>
    public interface IBillingFacade
    {
        /// <summary>
        /// To insert or update BillingCustomerPremiseModel to database
        /// </summary>
        /// <param name="model">model to be inserted</param>
        /// <returns>true</returns>
        Task<bool> InsertOrMergeBillingAsync(BillingCustomerPremiseModel model);
    }
}
