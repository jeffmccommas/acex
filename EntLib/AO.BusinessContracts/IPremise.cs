﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using CE.AO.Logging;
using CE.AO.Models;

namespace AO.BusinessContracts
{
    /// <summary>
    /// IPremise interface is used to fetch/insert/update the premise details from/to the database
    /// </summary>
    public interface IPremise
    {
        /// <summary>
        /// To insert or update PremiseModel to database
        /// </summary>
        /// <param name="model">model to be inserted</param>
        /// <returns>true</returns>
        Task<bool> InsertOrMergePremiseAsync(PremiseModel model);

        /// <summary>
        /// To fetch the all premise details from the database
        /// </summary>
        /// <param name="clientId">clientId of the data to be fetched</param>
        /// <param name="customerId">customerId of the data to be fetched</param>
        /// <returns>List of PremiseModel</returns>
        IEnumerable<PremiseModel> GetPremises(int clientId, string customerId);
        Task<bool> DeletePremisesAsync(int clientId, string customerId, string premiseId);
    }
}
