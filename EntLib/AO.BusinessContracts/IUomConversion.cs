﻿namespace AO.BusinessContracts
{
    /// <summary>
    /// IUomConversion interface is used to declare methods for Conversions of unit of measures to standard unit of measures
    /// </summary>
    public interface IUomConversion
    {
        /// <summary>
        /// This method declaration is for conversion of unit of measure <paramref name="fromUom"/> to standard unit of measure <paramref name="toUom"/>
        /// </summary>
        /// <param name="fromUom"></param>
        /// <param name="toUom"></param>
        /// <param name="consumtion"></param>
        /// <param name="conversionNotFound"></param>
        /// <returns></returns>
        double Convert(CE.AO.Utilities.Enums.UomType fromUom, CE.AO.Utilities.Enums.UomType toUom, double consumtion, out bool conversionNotFound);
    }
}
