﻿using System.Threading.Tasks;
using Microsoft.ServiceBus.Messaging;

namespace AO.BusinessContracts.EventProcessesContracts
{
    /// <summary>
    ///  Base contract for Aclara One Event Processors.
    /// </summary>
    public interface IProcessEvent
    {
        /// <summary>
        /// Processes the provided event data.
        /// </summary>
        /// <param name="eventData"></param>
        /// <param name="clientId">The client ID this event is applicable to.</param>
        /// <returns></returns>
        Task<bool> Process(EventData eventData, string clientId);

        /// <summary>
        /// The processing module this event processor is associated with.
        /// </summary>
        CE.AO.Utilities.Enums.Module Module { get; }

        /// <summary>
        /// The EventType this processor is responsible for processing.
        /// </summary>
        CE.AO.Utilities.Enums.EventType EventType { get; }
    }
}
