﻿namespace AO.BusinessContracts.EventProcessesContracts
{
    public interface IAmiProcessEvent : IProcessEvent
    {
        /// <summary>
        /// The IntervalType this AMI processor is responsible for processing.
        /// </summary>
        CE.AO.Utilities.Enums.IntervalType IntervalType { get; }
    }
}
