﻿using System;
using System.Collections.Generic;
using AO.BusinessContracts.Preprocessing;
using CE.AO.Models;

namespace AO.BusinessContracts.EventProcessesContracts
{
    /// <summary>
    /// Helper methods common accross AmiProcessEvents.  Common base maybe better option.
    /// </summary>
    public interface IAmiProcessEventHelper
    {
        bool SetValueFromReading(AmiReading amiReading, bool isIntervalEnd, bool clientHandlesDst,
            int intervalValue, CE.AO.Utilities.Enums.Timezone readingTimezoneEnum, TimeZoneInfo readingTimezone,
            string readingTimezoneString,
            TimeZoneInfo defaultTimeZone,
            string defaultTimezoneString, dynamic amiModel, List<TallAmiModel> tallAmiList, bool isNccReading = false);

        IEnumerable<AmiReading> GetReadings<T>(T amiRow);

        void CleanAllIntValues<T>(T amiModel, string[] otherColumnNames);

        T SetTouValuesUsingTouId<T>(T amiModel, string readingColumn);
    }
}
