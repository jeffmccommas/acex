﻿namespace AO.BusinessContracts
{
    public  interface ILogtoCassandraTrigger
    {
        /// <summary>
        /// Subscribes to a trigerring mechanism for doing Log to Cassandra
        /// </summary>
        void Subscribe();

        /// <summary>
        /// Unsubscribes from the trigerring mechanism for doing Log to Cassandrea
        /// </summary>
        void UnSubscribe();
    }
}
