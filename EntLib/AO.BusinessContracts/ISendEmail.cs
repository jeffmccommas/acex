﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace AO.BusinessContracts
{
    public interface ISendEmail
    {
        /// <summary>
        /// Send Email
        /// </summary>
        /// <param name="sender">The sender</param>
        /// <param name="recipients">The message recipients</param>
        /// <param name="identifiers">This replacement key must exist in the message body. There should be one value for each recipient in the To list</param>
        /// <param name="templateId">The active sendgrid Template Id</param>
        /// <param name="username">sendgrid username</param>
        /// <param name="password">sendgrid password</param>
        /// <param name="customKey">Combination of partionkey and rowkey</param>
        /// <param name="notifyDateTime">Send at time</param>
        /// <returns></returns>
        Task SendEmailAsync(string sender, IEnumerable<string> recipients,
            Dictionary<string, List<string>> identifiers, string templateId, string username, string password,
            string customKey, DateTime? notifyDateTime);

        /// <summary>
        /// Send Email
        /// </summary>
        /// <param name="sender">The sender</param>
        /// <param name="recipients">The message recipients</param>
        /// <param name="identifiers">This replacement key must exist in the message body. There should be one value for each recipient in the To list</param>
        /// <param name="body">Email body</param>
        /// <param name="subject">Email Subject</param>
        /// <param name="username">sendgrid username</param>
        /// <param name="password">sendgrid password</param>
        /// <param name="customKey">Combination of partionkey and rowkey</param>
        /// <param name="notifyDateTime">Send at time</param>
        /// <returns></returns>
        Task SendEmailAsync(string sender, IEnumerable<string> recipients,
            Dictionary<string, List<string>> identifiers, string body, string subject, string username, string password,
            string customKey, DateTime? notifyDateTime);
    }
}
