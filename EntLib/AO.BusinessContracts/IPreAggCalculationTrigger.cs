﻿namespace AO.BusinessContracts
{
    public interface IPreAggCalculationTrigger
    {
        /// <summary>
        /// Subscribes to a trigerring mechanism for doing pre aggregations
        /// </summary>
        void Subscribe();

        /// <summary>
        /// Unsubscribes from the trigerring mechanism for doing pre aggregations
        /// </summary>
        void UnSubscribe();
    }
}
