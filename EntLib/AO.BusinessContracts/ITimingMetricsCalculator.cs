﻿using CE.AO.Models;

namespace AO.BusinessContracts
{
    /// <summary>
    /// Interface to calculate timing of operations performed
    /// </summary>
    public interface ITimingMetricsCalculator
    {
        /// <summary>
        /// Start calculating time to perform operation
        /// </summary>
        void Start(int clientId, Enums.Metrics metric);

        /// <summary>
        /// Stop calculating time to perform operation. Call this when operation is performed.
        /// </summary>
        void Stop(int clientId, Enums.Metrics metric, LogModel logModel);
    }
}