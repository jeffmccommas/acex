﻿using System.Collections.Generic;
using AO.Domain;

namespace AO.BusinessContracts
{
    /// <summary>
    /// Interface for class TenantFacade
    /// </summary>
    public interface ITenantFacade
    {
        /// <summary>
        /// Method is used to return list of client id and name in key value pair
        /// </summary>
        /// <returns></returns>
        List<Client> GetClients();
    }
}