﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using CE.AO.Logging;
using CE.AO.Models;

namespace AO.BusinessContracts
{
    /// <summary>
    /// IConsumptionByAccount interface is used to do database operations on ConsumptionByAccount table
    /// </summary>
    public interface IConsumptionByAccount
    {
        LogModel LogModel { get; set; }

        /// <summary>
        /// To get the list of ConsumptionByAccountModels from the database
        /// </summary>
        Task<IList<ConsumptionByAccountModel>> GetConsumptionByAccountByDateRangeAsync(int clientId, string accountId,
            DateTime startDateTime, DateTime endDateTime);

        /// <summary>
        /// To insert/update the ConsumptionByAccountModel to the database
        /// </summary>
        /// <param name="model">ConsumptionByAccountModel which is to be inserted/updated</param>
        /// <returns>True</returns>
        Task<bool> InsertOrMergeConsumptionByAccountAsync(ConsumptionByAccountModel model);

        /// <summary>
        /// To delete the ConsumptionByAccountModel to the database
        /// </summary>
        /// <param name="model">ConsumptionByAccountModel which is to be deleted</param>
        /// <returns>True</returns>
        Task<bool> DeleteConsumptionByAccountAsync(ConsumptionByAccountModel model);
    }
}