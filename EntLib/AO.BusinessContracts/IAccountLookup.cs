﻿using System.Collections.Generic;
using System.Threading.Tasks;
using CE.AO.Models;

namespace AO.BusinessContracts
{
    /// <summary>
    /// IAccountLookup class is used to fetch/insert/update the client meter records from/to the database
    /// </summary>
    public interface IAccountLookup
    {      
        LogModel LogModel { get; set; }

        /// <summary>
        ///To fetch the active account meter details from the database
        /// </summary>
        /// <param name="clientId">clientId of the data to be fetched</param>
        /// <param name="meterId">meterId of the data to be fetched</param>
        /// <returns>AccountLookupModel</returns>
        List<AccountLookupModel> GetAccountMeterDetails(int clientId, string meterId);

        /// <summary>
        /// To insert or update AccountLookupModel to database
        /// </summary>
        /// <param name="model">AccountLookupModel to be inserted</param>
        /// <returns>true</returns>
        Task<bool> InsertOrMergeAccountLookupAsync(AccountLookupModel model);

        /// <summary>
        /// To fetch the account meter details for customer from the database
        /// </summary>
        /// <param name="clientId">clientId of the data to be fetched</param>
        /// <param name="meterId">meterId of the data to be fetched</param>
        /// <param name="customerId">customerId of the data to be fetched</param>
        /// <returns>AccountLookupModel</returns>
        Task<AccountLookupModel> GetAccountMeterDetails(int clientId, string meterId, string customerId);

        /// <summary>
        /// Delete Client Meter Data form Client Meter Table
        /// </summary>
        /// <param name="clientId"></param>
        /// <param name="meterid"></param>
        /// <param name="customerid"></param>
        /// <returns></returns>
        Task<bool> DeleteHistoryAccountLookupAsyc(int clientId, string meterid, string customerid);
    }
}
