﻿using System.Collections.Generic;
using CE.AO.Models;
using CE.BillToDate;
using CE.ContentModel.Entities;

namespace AO.BusinessContracts
{
    /// <summary>
    /// IClientModuleConfigFacade is an interface used to return specific settings and configuration information regarding the current client.
    /// </summary>
    public interface IClientConfigFacade
    {
        /// <summary>
        /// Return all configured portal modules for the current client.
        /// </summary>
        DynamicAppModuleConfig GetDynamicPortalModules(int clientId);

        /// <summary>
        /// Returns Client Settings for a particular client
        /// </summary>
        ClientSettings GetClientSettings(int clientId);

        /// <summary>
        /// Returns BillToDate Settings for a particular client
        /// </summary>
        BillToDateSettings GetBillToDateSettings(int clientId);

        /// <summary>
        /// Returns Client Settings for a particular client using Configuration Bulk Key.
        /// </summary>
        ClientSettingsExport GetClientSettingsExport(int clientId, string bulkKey);

        /// <summary>
        /// Returns particular filter details of client settings. 
        /// </summary>
        /// <param name="clientid">client id of client.</param>
        /// <param name="filtername">filter name to select from the list of filters</param>
        /// <returns></returns>
        Filter GetClientSettingExportFilter(int clientid, string filtername);

        /// <summary>
        /// Retrieves units of measure configuration based on supplied clientId.
        /// </summary>
        /// <param name="clientId">Unique identifier of the client that units of measure should be retrieved.</param>
        /// <returns>list of units of measure.</returns>
        IList<ClientUOM> GetClientUom(int clientId);

        /// <summary>
        /// Retrieves client text content settings based on supplied clientId.
        /// </summary>
        /// <param name="clientId">Unique identifier of the client that text content should be retrieved.</param>
        /// <param name="locale">Local of the text.  Example "en-US"</param>
        /// <param name="category">Comma seperated list of categories</param>
        /// <returns></returns>
        List<ClientTextContent> GetTextContents(int clientId, string locale, string category);

        /// <summary>
        /// Retrieves Standard Unit of Measure from Insight Metadata DB
        /// </summary>
        /// <returns></returns>
        StandardUomModel GetStandardUom();
        
    }
}