﻿using System.Threading.Tasks;
using CE.AO.Models;
namespace AO.BusinessContracts
{
    public interface IDcuCallData
    {
        /// <summary>
        /// To insert/update the DcuCallDataModel in dcu_calldata_history table.
        /// </summary>
        /// <param name="model">DcuCallDataModel which is to be inserted/updated</param>
        /// <returns>True</returns>
        Task InsertOrMergeDcuCallDataAsync(DcuCallDataModel model);

        /// <summary>
        /// To delete the DcuCallDataModel from dcu_calldata_history table.
        /// </summary>
        /// <param name="model">DcuCallDataModel which is to be deleted</param>
        /// <returns>True</returns>
        Task DeleteDcuCallDataAsync(DcuCallDataModel model);
    }
}
