﻿using System.Threading.Tasks;
using CE.AO.Logging;
using CE.AO.Models;

namespace AO.BusinessContracts
{
    /// <summary>
    /// ICustomer interface is used to fetch/insert/update the customer details from/to the database
    /// </summary>
    public interface ICustomer
    {
        LogModel LogModel { get; set; }

        /// <summary>
        /// To fetch the customer detail from the database
        /// </summary>
        /// <param name="clientId">clientId of the data to be fetched</param>
        /// <param name="customerId">customerId of the data to be fetched</param>
        /// <returns>CustomerModel</returns>
        Task<CustomerModel> GetCustomerAsync(int clientId, string customerId);

        /// <summary>
        /// To insert or update CustomerModel to database
        /// </summary>
        /// <param name="model">model to be inserted</param>
        /// <returns>true</returns>
        Task<bool> InsertOrMergeCustomerAsync(CustomerModel model);

        /// <summary>
        /// Create subscription and then update customer with newly created trumpia subscription id
        /// </summary>
        /// <param name="customerModel">customerModel of the trumpia subscription to be created</param>
        /// <param name="apiKey">apiKey of the trumpia subscription to be created</param>
        /// <param name="username">username of the trumpia subscription to be created</param>
        /// <param name="contactList">contactList of the trumpia subscription to be created</param>
        /// <returns>bool</returns>
        bool CreateSubscriptionAndUpdateCustomer(CustomerModel customerModel, string apiKey, string username, string contactList);
        Task<bool> DeleteCustomerAsync(int clientId, string customerId);

        /// <summary>
        /// Check whether trumpia subscription is created or not, if not then create subscription and then update customer with newly created trumpia subscription id
        /// </summary>
        /// <param name="customerModel">customerModel of the trumpia subscription to be fetched/created</param>
        /// <param name="apiKey">apiKey of the trumpia subscription to be fetched/created</param>
        /// <param name="username">username of the trumpia subscription to be fetched/created</param>
        /// <param name="contactList">contactList of the trumpia subscription to be fetched/created</param>
        /// <param name="isReOptIn">if the customer is opt-out by text, then to re-register set isReOptIn</param>
        /// <returns>bool</returns>
        bool SmsSubscirptionCheckAndUpdate(CustomerModel customerModel, string apiKey, string username,
            string contactList, ref bool isReOptIn);
    }
}
