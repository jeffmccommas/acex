﻿using System.Threading.Tasks;
using CE.AO.Logging;
using CE.AO.Models;

namespace AO.BusinessContracts
{
    public interface IDcuAlarmCode
    {
        /// <summary>
        /// To insert/update the DcuAlarmModel in dcu_alarms_by_code table
        /// </summary>
        /// <param name="model">DcuAlarmModel which is to be inserted/updated</param>
        /// <returns>True</returns>
        Task<bool> InsertOrMergeDcuAlarmCodeAsync(DcuAlarmModel model);

        /// <summary>
        /// To delete the DcuAlarmModel in dcu_alarms_by_code table
        /// </summary>
        /// <param name="model">DcuAlarmModel which is to be deleted</param>
        /// <returns>True</returns>
        Task<bool> DeleteDcuAlarmCodeAsync(DcuAlarmModel model);

        /// <summary>
        /// To delete the DcuAlarmModel in dcu_alarms_by_code table by using Partition
        /// </summary>
        /// <param name="model">DcuAlarmModel which is to be deleted</param>
        /// <returns>True</returns>
        Task<bool> DeleteDcuAlarmCodeWithPartionAsync(DcuAlarmModel model);
    }
}
