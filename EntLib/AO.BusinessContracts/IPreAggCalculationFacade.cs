﻿using System.Threading.Tasks;
using CE.AO.Models;

namespace AO.BusinessContracts
{
    /// <summary>
    /// IPreAggCalculationFacade interface is used to do pre aggregations
    /// </summary>
    public interface IPreAggCalculationFacade
    {
        Task<bool> CalculateAccountPreAgg(AccountPreAggCalculationRequestModel calculationRequestModel);
    }
}
