﻿using System.Collections.Generic;
using System.Threading.Tasks;
using CE.AO.Logging;
using CE.AO.Models;

namespace AO.BusinessContracts
{
    /// <summary>
    /// ICustomerMobileNumberLookup interface is used to fetch/insert/update the customermobilenumberlookup details from/to the database
    /// </summary>
    public interface ICustomerMobileNumberLookup
    {       
        LogModel LogModel { get; set; }

        /// <summary>
        /// To fetch the customermobilenumberlookup detail from the database
        /// </summary>
        /// <param name="clientId">clientId of the data to be fetched</param>
        /// <param name="mobileNumber">mobileNumber of the data to be fetched</param>
        /// <param name="accountId">accountId of the data to be fetched</param>
        /// <returns>CustomerMobileNumberLookupModel</returns>
        CustomerMobileNumberLookupModel GetCustomerMobileNumberLookup(int clientId, string mobileNumber, string accountId);

        /// <summary>
        /// To fetch the customermobilenumberlookup details from the database
        /// </summary>
        /// <param name="clientId">clientId of the data to be fetched</param>
        /// <param name="mobileNumber">mobileNumber of the data to be fetched</param>
        /// <returns>List of CustomerMobileNumberLookupModel</returns>
        List<CustomerMobileNumberLookupModel> GetAllCustomerMobileNumberLookup(int clientId,
            string mobileNumber);

        /// <summary>
        /// To insert or update CustomerMobileNumberLookupModel to database
        /// </summary>
        /// <param name="model">model to be inserted</param>
        /// <returns>true</returns>
        Task<bool> InsertOrMergeCustomerMobileNumberLookupAsync(CustomerMobileNumberLookupModel model);

		/// <summary>
		/// To delete CustomerMobileNumberEntity from the database
		/// </summary>
		/// <param name="clientId">Client id of entity to be deleted</param>
		/// <param name="mobileNumber">Mobile number of entity to be deleted</param>
		/// <param name="accountId">Account Id of entity to be deleted</param>
		/// <returns>true</returns>
		Task<bool> DeleteCustomerMobileNumberLookupAsync(int clientId, string mobileNumber, string accountId);

    }
}
