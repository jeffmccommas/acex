﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using CE.AO.Logging;
using CE.AO.Models;

namespace AO.BusinessContracts
{
    /// <summary>
    /// IBilling interface is used to fetch/insert/update the billing details from/to the database
    /// </summary>
    public interface IBilling
    {
        LogModel LogModel { get; set; }

        /// <summary>
        /// To fetch the latest bill from the database
        /// </summary>
        /// <param name="clientId">clientId of the data to be fetched</param>
        /// <param name="customerId">customerId of the data to be fetched</param>
        /// <param name="accountId">accountId of the data to be fetched</param>
        /// <param name="serviceContractId">serviceContractId of the data to be fetched</param>
        /// <returns>BillingModel</returns>
        Task<BillingModel> GetServiceDetailsFromBillAsync(int clientId, string customerId, string accountId, string serviceContractId);

        /// <summary>
        /// To fetch the all latest bills from the database
        /// </summary>
        /// <param name="clientId">clientId of the data to be fetched</param>
        /// <param name="customerId">customerId of the data to be fetched</param>
        /// <param name="accountId">accountId of the data to be fetched</param>
        /// <returns>List of BillingModel</returns>
        IEnumerable<BillingModel> GetAllServicesFromLastBill(int clientId, string customerId, string accountId);

        /// <summary>
        /// To fetch the all bills of specified serviceContractId only from the database
        /// </summary>
        /// <param name="clientId">clientId of the data to be fetched</param>
        /// <param name="customerId">customerId of the data to be fetched</param>
        /// <param name="accountId">accountId of the data to be fetched</param>
        /// <param name="serviceContractId">serviceContractId of the data to be fetched</param>
        /// <param name="startDate">startDate of the data to be fetched</param>
        /// <returns>List of BillingModel</returns>
        IEnumerable<BillingModel> GetBillsForService(int clientId, string customerId, string accountId, string serviceContractId, DateTime startDate);

        /// <summary>
        /// To fetch the all history bills within the date range only from the database
        /// </summary>
        /// <param name="clientId">clientId of the data to be fetched</param>
        /// <param name="customerId">customerId of the data to be fetched</param>
        /// <param name="accountId">accountId of the data to be fetched</param>
        /// <param name="serviceContractId">serviceContractId of the data to be fetched</param>
        /// <param name="startDate">startDate of the data to be fetched</param>
        /// <param name="endDate">endDate of the data to be fetched</param>
        /// <returns>List of BillingModel</returns>
        IEnumerable<BillingModel> GetBillsForService(int clientId, string customerId, string accountId, string serviceContractId, DateTime startDate, DateTime endDate);

        /// <summary>
        /// To insert or update latest BillingModel to database
        /// </summary>
        /// <param name="model">model to be inserted</param>
        /// <returns>true</returns>
        Task<bool> InsertOrMergeLatestBillAsync(BillingModel model);

        /// <summary>
        /// To insert or update history BillingModel to database
        /// </summary>
        /// <param name="model">model to be inserted</param>
        /// <returns>true</returns>
        Task<bool> InsertOrMergeHistoryBillAsync(BillingModel model);

        /// <summary>
        /// To delete latest bill from database
        /// </summary>
        /// <param name="clientId">clientId of the data to be deleted</param>
        /// <param name="customerId">customerId of the data to be deleted</param>
        /// <param name="accountId">accountId of the data to be deleted</param>
        /// <param name="serviceContractId">serviceContractId of the data to be deleted</param>
        /// <returns>true</returns>
        Task<bool> DeleteLatestServiceDetailsAsync(int clientId, string customerId, string accountId,
             string serviceContractId);

        /// <summary>
        /// To delete history bill from database
        /// </summary>
        /// <param name="clientId">clientId of the data to be deleted</param>
        /// <param name="customerId">customerId of the data to be deleted</param>
        /// <param name="accountId">accountId of the data to be deleted</param>
        /// <param name="serviceContractId">serviceContractId of the data to be deleted</param>
        /// <param name="endDate">endDate of the data to be deleted</param>
        /// <returns>true</returns>
        Task<bool> DeleteHistoryServiceDetailsAsync(int clientId, string customerId, string accountId,
            string serviceContractId, DateTime endDate);
    }
}
