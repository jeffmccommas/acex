﻿using System.Threading.Tasks;
using CE.AO.Models;

namespace AO.BusinessContracts
{
    /// <summary>
    /// Meter Type Interface
    /// </summary>
    public interface IMeterType
    {
        /// <summary>
        /// Get MeterType information from alarms table
        /// </summary>
        /// <param name="meterTypeId"> Meter TypeId</param>
        /// <returns>True</returns>
        MeterTypeModel GetMeterType(int meterTypeId);

        /// <summary>
        /// To insert/update the MeterTypeModel to database
        /// </summary>
        /// <param name="model">MeterTypeModel which is to be inserted/updated</param>
        /// <returns>True</returns>
        /// 
        Task InsertOrMergeMeterTypeAsync(MeterTypeModel model);

        /// <summary>
        /// To delete the MeterTypeModel from database
        /// </summary>
        /// <param name="model">MeterTypeModel which is to be deleted</param>
        /// <returns>True</returns>
        Task DeleteMeterTypeAsync(MeterTypeModel model);

    }
}
