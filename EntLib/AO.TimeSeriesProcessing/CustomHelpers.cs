﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Web;

namespace AO.TimeSeriesProcessing {
    class CustomHelpers {
    }


    public static class MappingCustomHelpers {

        private static Dictionary<string, string> _commodityMap;
        private static Dictionary<string, string> _uomMap;

        private static string _commodityMapProcLock = "CommodityMapProcLock";
        private static string _uomMapProcLock = "_uomMapProcLock";


        public static string CommodityMapping(ProcessingArgs pa, string clientId, string commodity) {
            string retVal = commodity;

            if (_commodityMap == null) {
                lock (_commodityMapProcLock) {
                    if (_commodityMap == null) {
                        _commodityMap = new Dictionary<string, string>();
                        string commodities = pa.GetControlVal($"{clientId}commodityMap", ""); // if blank is probably bad.
                        string[] commoditiesMaps = commodities.Split(new[] { ';' }, StringSplitOptions.RemoveEmptyEntries);
                        foreach (string c in commoditiesMaps) {
                            string[] commodityKey = c.Split(new[] { '=' }, StringSplitOptions.RemoveEmptyEntries);
                            if (commodityKey.GetUpperBound(0) > 0) {
                                _commodityMap.Add(commodityKey[0], commodityKey[1]);
                            }
                        }
                    }
                }
            }

            if (_commodityMap.Any()) {
                if (_commodityMap.ContainsKey(commodity)) {
                    retVal = _commodityMap[commodity];
                }
            }

            return retVal;
        }

        public static string UomMapping(ProcessingArgs pa, string uom) {
            string retVal = uom;
            if (_uomMap == null) {
                lock (_uomMapProcLock) {
                    if (_uomMap == null) {
                        _uomMap = new Dictionary<string, string>();
                        string uoms = pa.GetControlVal("uomMap", ""); // if blank is probably bad.
                        string[] uomMaps = uoms.Split(new[] {';'}, StringSplitOptions.RemoveEmptyEntries);
                        foreach (string c in uomMaps) {
                            string[] uomKey = c.Split(new[] {'='}, StringSplitOptions.RemoveEmptyEntries);
                            if (uomKey.GetUpperBound(0) > 0) {
                                _uomMap.Add(uomKey[0], uomKey[1]);
                            }
                        }
                    }
                }
            }

            if (_uomMap.Any()) {
                if (_uomMap.ContainsKey(uom.ToLower())) {
                    retVal = _uomMap[uom.ToLower()];
                }
            }
            return retVal;
        }
    }
    public static class AlertsCustomHelpers {
        public static IHtmlString Create_Label(string Content) {
            string LableStr = $"<label style=\"background-color:gray;color:yellow;font-size:24px\">{Content}</label>";
            return new HtmlString(LableStr);
        }


        public static IHtmlString ColorResultsString(string Content) {
            string coloredString = "";
            if (Content.Contains("**analMiss**")) {
                coloredString = $"<div style=\"background-white:gray;color:red\">{Content}</div>";
            }
            else if (Content.Contains("**analHit**")) {
                coloredString = $"<div style=\"background-white:gray;color:green\">{Content}</div>";
            }
            else if (Content.Contains("**analComplete**")) {
                coloredString = $"<div style=\"background-color:green;color:white\">{Content}</div>";
            }
            else if (Content.Contains("**miss**")) {
                coloredString = $"<div style=\"background-color:yellow;color:black\">{Content}</div>";
            }
            else if (Content.Contains("**missed**")) {
                coloredString = $"<div style=\"background-color:red;color:white\">{Content}</div>";
            }
            else if (Content.Contains("**hits**")) {
                coloredString = $"<div style=\"background-color:green;color:white\">{Content}</div>";
            }
            else {
                coloredString = $"<div style=\"background-color:white;color:black\">{Content}</div>";
            }
            return new HtmlString(coloredString);
        }
        public static IHtmlString StatusResultsString(string Content) {
            string coloredString = "";
            if (Content.Contains("**analMiss**")) {
                coloredString = $"<div style=\"background-white:gray;color:red\">{Content}</div>";
            }
            else if (Content.Contains("**analComplete**")) {
                coloredString = $"<div style=\"background-color:green;color:white\">{Content}</div>";
            }
            else if (Content.Contains("**miss**")) {
                coloredString = $"<div style=\"background-color:yellow;color:black\">{Content}</div>";
            }
            else if (Content.Contains("**missed**")) {
                coloredString = $"<div style=\"background-color:red;color:white\">{Content}</div>";
            }
            else if (Content.Contains("**hits**")) {
                coloredString = $"<div style=\"background-color:green;color:white\">{Content}</div>";
            }
            else {
                coloredString = $"";
            }
            return new HtmlString(coloredString);
        }

    }
}
