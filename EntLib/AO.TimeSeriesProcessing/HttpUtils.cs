﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Security.Cryptography.X509Certificates;
using System.Xml;
using Newtonsoft.Json;

namespace AO.TimeSeriesProcessing {
    public class HttpUtils {




        public static StringContent GetExceptionResponse(string whereFrom, ProcessingArgs pa, string[] sArgs, Exception exc) {
            XmlDocument xDoc = XmlDocUtils.GetXMLDocument(whereFrom, sArgs, exc);
            bool isJson = !string.IsNullOrEmpty(pa["contentType"]) && (pa["contentType"].ToLower() == "application/json");
            if (isJson) {
                return new StringContent(JsonConvert.SerializeXmlNode(xDoc));
            }
            return new StringContent(xDoc.OuterXml);
        }

        public class AcceptAllCertificatePolicy : ICertificatePolicy {
            public AcceptAllCertificatePolicy() {
            }

            public bool CheckValidationResult(ServicePoint sPoint,
               X509Certificate cert, WebRequest wRequest, int certProb) {
                //// Always accept
                //return true;

                // something a little better than accept anything
                string smsg;
                bool noprob = false;
                switch ((uint)certProb) {
                    case 0x800B0101:
                        smsg = "CertEXPIRED";
                        break;
                    case 0x800B0102:
                        smsg = "CertVALIDITYPERIODNESTING";
                        break;
                    case 0x800B0103:
                        smsg = "CertROLE";
                        break;
                    case 0x800B0104:
                        smsg = "CertPATHLENCONST";
                        break;
                    case 0x800B0105:
                        smsg = "CertCRITICAL";
                        break;
                    case 0x800B0106:
                        smsg = "CertPURPOSE";
                        break;
                    case 0x800B0107:
                        smsg = "CertISSUERCHAINING";
                        break;
                    case 0x800B0108:
                        smsg = "CertMALFORMED";
                        break;
                    case 0x800B0109:
                        smsg = "CertUNTRUSTEDROOT";
                        break;
                    case 0x800B010A:
                        smsg = "CertCHAINING";
                        break;
                    case 0x800B010C:
                        smsg = "CertREVOKED";
                        break;
                    case 0x800B010D:
                        smsg = "CertUNTRUSTEDTESTROOT";
                        break;
                    case 0x800B010E:
                        smsg = "CertREVOCATION_FAILURE";
                        break;
                    case 0x800B010F:
                        smsg = "CertCN_NO_MATCH";
                        break;
                    case 0x800B0110:
                        smsg = "CertWRONG_USAGE";
                        break;
                    case 0x800B0112:
                        smsg = "CertUNTRUSTEDCA";
                        break;
                    default:
                        smsg = string.Format("NA certp={0}", certProb);
                        noprob = true;
                        break;
                }
                Console.WriteLine("CertCheck:" + smsg);
                if (!noprob) {
                    string defOKProbs = "CertUNTRUSTEDROOT";
                    string cfgLoc = "System/Configuration/CertPolicy/Overrides";
                    //defOKProbs = SubsystemConfig.GetConfigSetting(cfgLoc, defOKProbs);
                    string[] accepts = defOKProbs.Split(';');
                    foreach (string s in accepts) {
                        if (s.Equals(smsg)) {
                            noprob = true;
                            Console.WriteLine(string.Format("CertCheck:overriden('{0}'='{1}'):{2}", cfgLoc, defOKProbs, smsg));
                            break;
                        }
                    }
                    if (!noprob) {
                        Console.WriteLine(string.Format("CertCheck:noverriden('{0}'='{1}')", cfgLoc, defOKProbs));

                    }
                }

                return noprob;


                //bool certMatch = false; // Assume failure
                //byte[] certHash = cert.GetCertHash();
                //if (certHash.Length == apiCertHash.Length) {
                //    certMatch = true; // Now assume success.
                //    for (int idx = 0; idx < certHash.Length; idx++) {
                //        if (certHash[idx] != apiCertHash[idx]) {
                //            certMatch = false; // No match
                //            break;
                //        }
                //    }
                //}

                //// Return true => allow unauthenticated server,
                ////        false => disallow unauthenticated server.
                //return certMatch;
            }
        }



        #region webProcessing

        public static void FinalizeResponseHeaders(ProcessingArgs pa, HttpResponseMessage response, HttpRequestMessage request, ContextData context, string contentType = null) {
            ManageOrigin(pa, request, response);
            IEnumerable<string> values;
            if (!string.IsNullOrEmpty(pa["cassbuiltqry"])) {
                response.Headers.Add("cassbuiltqry", pa["cassbuiltqry"]);
            }
            response.Content.Headers.ContentType = new MediaTypeHeaderValue(contentType ?? "application/xml");
            if (request.Headers.TryGetValues("X-BIO-timeStamp", out values)) {
                response.Headers.Add("X-BIO-timeStamp", values.FirstOrDefault());
                response.Headers.Add("X-BIO-timeStampStop", DateTime.Now.ToString());
            }
            if (!string.IsNullOrEmpty(pa["start"])) response.Headers.Add("X-BIO-start_page", pa["start"]);
            if (!string.IsNullOrEmpty(pa["next"])) response.Headers.Add("X-BIO-next_page", pa["next"]);
            if (!string.IsNullOrEmpty(pa["per_page"])) response.Headers.Add("X-BIO-per_page", pa["per_page"]);
            if (!string.IsNullOrEmpty(pa["pages"])) response.Headers.Add("X-BIO-pages", pa["pages"]);
            if (!string.IsNullOrEmpty(pa["recCount"])) response.Headers.Add("X-BIO-records", pa["recCount"]);
            if (!string.IsNullOrEmpty(pa["X-BIO-etag"])) response.Headers.Add("X-BIO-etag", pa["X-BIO-etag"]);
            if (!string.IsNullOrEmpty(pa["filtDisp"])) response.Headers.Add("X-BIO-filtDisp", pa["filtDisp"]);
            if (!string.IsNullOrEmpty(pa["filtTotal"])) response.Headers.Add("X-BIO-filtTotal", pa["filtTotal"]);
            response.Headers.Add("cntr_noFix", context.Add2Counter("noFix", 0).ToString());
            response.Headers.Add("cntr_autoFix", context.Add2Counter("autoFix", 0).ToString());
            response.Headers.Add("cntr_refReadRows", context.Add2Counter("RefReadRows", 0).ToString());
            response.Headers.Add("cntr_errors", context.Add2Counter("errors", 0).ToString());
            response.Headers.Add("cntr_createUnzipMove", context.Add2Counter("createUnzipMove", 0).ToString());
            response.Headers.Add("cntr_deleteUnzipDest", context.Add2Counter("deleteUnzipDest", 0).ToString());
            response.Headers.Add("cntr_deleteUnzipMove", context.Add2Counter("deleteUnzipMove", 0).ToString());
            response.Headers.Add("cntr_batchSubmit", context.Add2Counter("batchSubmit", 0).ToString());
            response.Headers.Add("cntr_dobatch", context.Add2Counter("dobatch", 0).ToString());
        }

        public static void ManageOrigin(ProcessingArgs pa, HttpRequestMessage request, HttpResponseMessage response) {
            string reqHost = request.Headers.Host;
            IEnumerable<string> values;
            if (pa["OriginHosts"] != null) {
                // we're dealing with multiple sites as well as nginx altered routes so indicate allowed sites 
                string[] allowedHosts = pa["OriginHosts"].Split(',');
                string allowHost = "http://localhost"; // default if nothing implicit or explicit
                if (allowedHosts.Contains(reqHost)) {
                    string useScheme = request.RequestUri.Scheme;
                    if (request.Headers.TryGetValues("X-http-scheme", out values)) {
                        useScheme = values.FirstOrDefault();
                    }
                    allowHost = $"{useScheme}://{reqHost}";
                    if (request.Headers.Referrer != null) {
                        // nginx forwarded or some other override
                        allowHost = $"{request.Headers.Referrer.Scheme}://{request.Headers.Referrer.Authority}";
                    }
                }
                response.Headers.Add("Access-Control-Allow-Origin", allowHost);
                response.Headers.Add("X-ACAO", allowHost);
                // so we can see what it did since ACAO doesnt make it through to the chrome debugger
            }
        }

        #endregion

    }
}
