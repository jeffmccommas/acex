﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.Data;
using System.IO;
using System.IO.MemoryMappedFiles;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Xml;

namespace AO.TimeSeriesProcessing {

    public class TransCsvData {
        private Dictionary<int, StringDictionary> colMapData;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="transVals"> somehting like "6:1=11,=2;"   for 2 mappings for column 6 values where 1->11 and blank->2 and anything else stays the same</param>
        public TransCsvData(string transVals) {
            if (!string.IsNullOrEmpty(transVals)) {
                colMapData = new Dictionary<int, StringDictionary>();
                string[] maps = transVals.Split(new[] { ';' }, StringSplitOptions.RemoveEmptyEntries);
                foreach (string cmap in maps) {
                    string[] mMap = cmap.Split(':');
                    int col = 0;
                    if (int.TryParse(mMap[0], out col) && mMap.Length > 1) {
                        string[] colMaps = mMap[1].Split(',');
                        StringDictionary mapD = new StringDictionary();
                        foreach (string mapVs in colMaps) {
                            string[] mapV = mapVs.Split('=');
                            mapD.Add(mapV[0], mapV[1]);
                        }
                        colMapData[col] = mapD;
                    }
                }
            }
        }

        public string MapValues(string csvStg) {
            if (HaveMapData) {
                string[] csvVals = csvStg.Split(',');
                csvVals = MapValues(csvVals);
                return string.Join(",", csvVals);
            }
            return csvStg;
        }


        public string[] MapValues(string[] vals) {
            if (HaveMapData) {
                foreach (KeyValuePair<int, StringDictionary> columnMapedValues in colMapData) {
                    StringDictionary mvPairs = columnMapedValues.Value;   // mapping of from-to strings for the column columnMapedValues.Key  
                    if (mvPairs.ContainsKey(vals[columnMapedValues.Key])) {  // if the mapping pairs contains something for the data value in the current column
                        vals[columnMapedValues.Key] = mvPairs[vals[columnMapedValues.Key]];
                    }
                }
            }
            return vals;
        }

        public bool HaveMapData => colMapData != null;
    }


    public class BatchUtils {
        public const string UtcTimeFormat = "yyyy-MM-ddTHH:mm:ss";





        /// <summary>
        /// do validation analysis (clean this up -- extracted method, so put vals in pa)
        /// </summary>
        /// <param name="contextData"></param>
        /// <param name="pa"></param>
        /// <param name="request"></param>
        /// <param name="response"></param>
        public static void AnalysisBatch(ContextData contextData, ProcessingArgs pa, HttpRequestMessage request, HttpResponseMessage response) {
            if (request == null) request = new HttpRequestMessage();
            if (response == null) response = new HttpResponseMessage();

            contextData.ContextWrite("AnalysisBatch", "begin");
            List<DataTable> queryResultTables = new List<DataTable> { };
            contextData.ContextWrite("AnalysisBatch", "1");

            List<string> batchQueries = new List<string>();

            List<string> autoFixes = new List<string>();
            List<string> noFixes = new List<string>();

            contextData.ContextWrite("AnalysisBatch", "2");
            CassConn1 cc1 = new CassConn1();
            cc1.Connect(pa);
            contextData.ContextWrite("AnalysisBatch", "3");

            string _myEtag = (!string.IsNullOrEmpty(pa["myEtag"])) ? pa["myEtag"] : "";


            if (!string.IsNullOrEmpty(pa["procResp"]) && (pa["procResp"].ToLower() != "0")) { // process data and write
                contextData.ContextWrite("webPath", pa["outputWeb"]);
                contextData.ContextWrite("etag", _myEtag);
                contextData.ContextWrite("RequestUri", request?.RequestUri?.ToString());
                contextData.ContextWrite("RequestHeaders", request?.Headers);
                contextData.ContextWriteBulk("RequestContent", pa["reqContent"]);
            }
            int expectRows = 1;
            if (pa["expectRows"] != null) int.TryParse(pa["expectRows"], out expectRows);
            string referenceSourceFile = @"c:\_Test\AclValidateSrc.csv";
            string importContent = pa.GetControlVal("dataContent", "");
            try {
                if (string.IsNullOrEmpty(importContent)) {
                    if (!String.IsNullOrEmpty(pa[$"fiOper0"]?.Trim())) {
                        contextData.ContextWrite("AnalysisBatch", "4");
                        referenceSourceFile = pa["fiOper0"].Trim();
                        FileAttributes attr = File.GetAttributes(referenceSourceFile);
                        if (attr.HasFlag(FileAttributes.Directory)) {
                            // directory == get list of CSV files
                            string srcFile = BatchUtils.ConsolidateFiles(contextData, ref referenceSourceFile, pa);
                            if (pa.GetControlVal("useConsolidatedData", "no").Equals("yes") && !string.IsNullOrEmpty(srcFile)) {
                                queryResultTables.Add(AnalysisUtils.GetDataTableFromCsv(srcFile, true));
                            }
                            else {
                                return;
                            }
                        }
                    }
                }
                else {
                    if (!String.IsNullOrEmpty(pa[$"fiOper0"]?.Trim())) {
                        referenceSourceFile = pa["fiOper0"].Trim();
                    }
                }
            }
            catch (Exception exc) {
                contextData.ContextWrite("AnalysisBatch", $"Exception i {exc.Message}, {exc.InnerException?.Message}");
                contextData.Add2Counter("errors", 1);
            }
            string matchType = "exact";
            if (!String.IsNullOrEmpty(pa["matchType"]?.Trim())) {
                matchType = pa["matchType"].Trim();
            }
            contextData.ContextWrite("AnalysisBatch", "5");
            DateTime dtStart = DateTime.Now;
            contextData?.ContextWrite("analSource", $"using {referenceSourceFile} rows ");
            contextData?.ContextWrite("analStart", $"expecting {expectRows} rows ", dtStart.ToString());

            EstablishIntervalEnvironment(pa, referenceSourceFile);

            //if (referenceSourceFile.ToLower().IndexOf("daily") >= 0) {
            //    if (testEnv.Equals("perfh")) {
            //        pa["aclenv"] = "perfd";
            //    }
            //}
            //if (referenceSourceFile.ToLower().IndexOf("60min") >= 0) {
            //    if (testEnv.Equals("perfd")) {
            //        pa["aclenv"] = "perfh";
            //    }
            //}

            int matchMiss = 0;
            int matchFound = 0;
            int readRows = 0;
            int missOutputLimit = 50;
            int missAbortLimit = 50000;

            int ctxLines = 25;
            int ctxLinesMax = ctxLines;
            int tryLines;
            if (pa["contextRecs"] != null) {
                if (int.TryParse(pa["contextRecs"], out tryLines)) {
                    ctxLines = tryLines;
                    ctxLinesMax = ctxLines;
                }
            }

            int batchTimeout = GetConfigInt(pa, "batchTimeout", 100);
            int batchSleepMs = GetConfigInt(pa, "batchSleepMs", 10);
            int procLimit = GetConfigInt(pa, "procLimit", 200);
            int batchLimit = GetConfigInt(pa, "batchLimit", 200);
            int batchDataLimit = GetConfigInt(pa, "batchDataLimit", 4600);
            int skipRefRows = GetConfigInt(pa, "skipRefRows", 0);
            int taskListMax = GetConfigInt(pa, "taskListMax", 30);
            bool doAsyncBatch = GetConfigInt(pa, "doAsyncBatch", 1) == 1;
            bool doFilterIngestion = GetConfigInt(pa, "doFilterIngestion", 1) == 1;
            bool newFilterIngestion = GetConfigInt(pa, "newFilterIngestion", 1) == 1;
            bool trackIgnored = GetConfigInt(pa, "trackIgnored", 0) == 1;
            bool trackImported = GetConfigInt(pa, "trackImported", 0) == 1;
            bool trackParams = GetConfigInt(pa, "trackParams", 0) == 1;
            string getQRows = pa.GetControlVal("qRows", "*");
            bool doCassandraImport = GetConfigInt(pa, "doCassandraImport", 1) == 1;
            bool trackAlternate = GetConfigInt(pa, "trackAlternate", 0) == 1;
            string trackAlternateFile = pa.GetControlVal("trackAlternateFile", "");
            bool stripCsvQuotes = string.Equals(pa.GetControlVal("stripCsvQ", "no"), "yes");
            string clientId = pa.GetControlVal("clientId", "87");

            string procEnvTag = pa.GetControlVal("aclenv", "work");
            string intervalTag = pa.GetControlVal($"{procEnvTag}intervalTag", "60");

            // TBD -- lots of ameren specific in this TZ handling.  only hourly and 60 (tz done in cvt2tall).  partil remediation of this.
            TimeZoneInfo desTimeZoneInfo = null;
            int timeOffsetColumn = GetConfigInt(pa, "timeOffsetColumn", 10);
            var cvtTimeKey = pa.GetControlVal($"{clientId}cvtTimeKey", "cvtTime");
            bool doTimeOffset = GetConfigInt(pa, $"{procEnvTag}{cvtTimeKey}", 0) == 1;

            Dictionary<string, string> dtLookup = null;

            if (doTimeOffset) {
                var destTimeZoneString = pa.GetControlVal("destTimeZoneString", "Central Standard Time");
                if (!string.IsNullOrEmpty(destTimeZoneString)) {
                    desTimeZoneInfo = TimeZoneInfo.FindSystemTimeZoneById(destTimeZoneString);
                    // dtLookup = GetDailyDateTimeLookup(desTimeZoneInfo, TimeZoneInfo.Utc,1);
                }
                else {
                    doTimeOffset = false;
                }
            }

            //int timeOffsetHours = GetConfigInt(pa, "timeOffsetHours", -6);
            //string destTimeZoneString = pa.GetControlVal("destTimeZoneString", "Central Standard Time");
            //TimeZoneInfo desTimeZoneInfo = TimeZoneInfo.FindSystemTimeZoneById(destTimeZoneString);
            //bool doTimeOffset = GetConfigInt(pa, "doTimeOffset", 1) == 1;
            //if (doTimeOffset) {
            //    if (!intervalTag.Equals("24")) {  // TBD: ameren specific as only hourly and daily and hourly convert did offset
            //        doTimeOffset = false;
            //    }
            //}

            FileStream trackAlternateStream = null;
            MeterFilter mf = null;

            if (doFilterIngestion) {
                mf = new MeterFilter();
                contextData.ContextWrite("AnalysisBatch", $" historical import initilizing with new={newFilterIngestion} with {mf.SubscriptionSummary()} ");
                mf.InitializeFiltering(contextData, newFilterIngestion);
                contextData.ContextWrite("AnalysisBatch", $" historical import request for want new={newFilterIngestion} with {mf.SubscriptionSummary()} ");

                if (newFilterIngestion) {
                    if (mf.SubscriptionCount("new") == 0) {
                        if (!pa.GetControlVal("scanWithNoNew", "0").Equals("1")) {
                            contextData.ContextWrite("AnalysisBatch", $"**terminating** due to historical import request with no new subscriptions. (set 'scanWithNoNew=1' to override) ");
                            return;
                        }
                    }
                }
            }
            if (trackParams) {
                contextData.NoFixes.AddLine(pa.AsJson());
            }
            string sqlMeterMgtDatabase = pa.GetControlVal($"{clientId}sqlMeterMgtDatabasename", "InsightsDW_Test");
            if (pa["sqlMeterMgtCxnStg"] == null) {
                string sqlMeterMgt = ConfigurationManager.AppSettings.Get("sqlMeterMgt");
                pa["sqlMeterMgtCxnStg"] = String.Format(sqlMeterMgt, sqlMeterMgtDatabase);
            }

            StreamReader referenceStream = null;
            DateTime dtAbStart = DateTime.Now;
            try {

                if ((pa["qSel0"] != null) && (pa["qSel0"].Trim() != "*")) {
                    string csvNames = "nada";
                    var tasks = new List<Task>();
                    if (!string.IsNullOrEmpty(importContent)) {
                        contextData.ContextWrite("AnalysisBatch", "importContent");
                        referenceStream = new StreamReader(StreamUtils.ToStream(importContent));
                    }
                    else {
                        contextData.ContextWrite("AnalysisBatch", referenceSourceFile);
                        referenceStream = new StreamReader(referenceSourceFile);
                    }
                    {
                        bool hasHeaders = pa.GetControlVal("hasheaders", 1) == 1;

                        // used to be using
                        if (hasHeaders) {
                            csvNames = referenceStream.ReadLine();
                        }

                        string transCsvMap = pa.GetClientControlVal(clientId, "transcsvMap", "transcsv");
                        string dTransCsv = pa.GetControlVal($"{procEnvTag}{transCsvMap}", "");
                        //string dTransCsv = pa["dTransCsv"];

                        TransCsvData translationData = null;
                        if (!string.IsNullOrEmpty(dTransCsv)) {
                            translationData = new TransCsvData(dTransCsv);
                        }

                        string qryFmt = pa["qSel0"];
                        if (string.IsNullOrEmpty(qryFmt)) {
                            qryFmt = pa["cassquery"];
                        }
                        if (!string.IsNullOrEmpty(qryFmt)) {
                            qryFmt = qryFmt.Replace("##IntervalTag##", intervalTag);
                            int directionCol = pa.GetControlVal($"{procEnvTag}DirectionCol", 7);

                            if (contextData != null) {
                                contextData.ContextWrite("val_file", contextData.Validation.FileName);
                                contextData.ContextWrite("val_web", pa["outputWeb"] + Path.GetFileName(contextData.Validation.FileName));
                            }
                            int batchDataLength = 0;
                            int exceptionCount = 0;
                            int importExceptionLimit = pa.GetControlVal("importExceptionLimit", 5000);
                            try {
                                if (trackAlternate && !string.IsNullOrEmpty(trackAlternateFile)) {
                                    string trackDIr = Path.GetDirectoryName(trackAlternateFile);
                                    if (!Directory.Exists(trackDIr)) {
                                        contextData.ContextWrite("trackAlternate creating track directory", trackDIr);
                                        Directory.CreateDirectory(trackDIr);
                                    }
                                    trackAlternateStream = new FileStream(trackAlternateFile, FileMode.Append);
                                    contextData.ContextWrite("** AnalysisBatch ==REDIRECT to", trackAlternateFile);
                                }
                            }
                            catch (Exception exc) {
                                string excInfo = $"trackAlternate Exception on {trackAlternateFile} {exc.Message}, {exc.InnerException?.Message}, disabling trackAlternate";
                                trackAlternate = false;
                                if (!doCassandraImport) excInfo += " reenabling cassandra import";
                                doCassandraImport = true;
                                contextData.ContextWrite("** ERROR AnalysisBatch", excInfo);
                            }
                            string lastSrcTimeString = "";
                            string lastDestTimeString = "";
                            int lookupFailCount = 0;
                            while (!referenceStream.EndOfStream && (procLimit-- > 0) && (exceptionCount < importExceptionLimit)) {
                                string csvVals = referenceStream.ReadLine();
                                string[] csvCols = null;

                                string mfRowHashKey = "";
                                try {
                                    mfRowHashKey = "";  // reset in case of exception
                                    if (stripCsvQuotes) {
                                        csvVals = csvVals.Replace("\"", "");
                                    }
                                    readRows++;
                                    contextData.IncrementCounter("RefReadRows");
                                    if (string.IsNullOrEmpty(csvVals)) {
                                        continue;
                                    }
                                    if (doFilterIngestion) {
                                        mfRowHashKey = mf.Has(csvVals, newFilterIngestion);
                                        if (string.IsNullOrEmpty(mfRowHashKey)) {
                                            contextData.IncrementCounter("impIgnore");
                                            if (trackIgnored) {
                                                contextData.AutoFixes.AddLine($"{csvVals}");
                                            }
                                            continue;
                                        }
                                        // passed initial existance.  now filter out non forward (blank/0 == forward, anything else is rejected
                                        csvCols = csvVals.Split(',');
                                        if (!string.IsNullOrEmpty(csvCols[directionCol])) {
                                            if (csvCols[directionCol] != "0") { // hack! forward = 0
                                                contextData.IncrementCounter("impReject");
                                                mf.Increment(mfRowHashKey, newFilterIngestion, false);
                                                contextData.AutoFixes.AddLine($"{csvVals}");
                                                continue;
                                            }
                                        }
                                        mf.Increment(mfRowHashKey, newFilterIngestion);
                                        if (trackImported) {
                                            contextData.NoFixes.AddLine($"{csvVals}");
                                        }
                                        contextData.IncrementCounter("impProcess");
                                    }
                                    if (doTimeOffset) {
                                        if (csvCols == null) {
                                            csvCols = csvVals.Split(',');
                                        }
                                        if (dtLookup != null) {  // POC for dt lookup to minimize churn and speed up processing
                                            if (lookupFailCount < 50) {
                                                // likely not hourly which this is currently used to address clr heap GC failure 
                                                if (!csvCols[timeOffsetColumn].Equals(lastSrcTimeString)) {
                                                    // lots of dups, so avoid even conversion lookup if possible
                                                    lastSrcTimeString = csvCols[timeOffsetColumn];
                                                    DateTime recTime = DateTime.Parse(lastSrcTimeString);
                                                    string dtStg = $"{recTime.Year}/{recTime.Month}/{recTime.Day}";
                                                    if (dtLookup.ContainsKey(dtStg)) {
                                                        lastDestTimeString = dtLookup[dtStg];
                                                    }
                                                    else {
                                                        lookupFailCount++;
                                                        DateTime cvtTime = TimeZoneInfo.ConvertTime(recTime,
                                                            desTimeZoneInfo, TimeZoneInfo.Utc);
                                                        lastDestTimeString = cvtTime.ToString(UtcTimeFormat);
                                                    }
                                                }
                                                csvCols[timeOffsetColumn] = lastDestTimeString;
                                            }
                                            else {
                                                DateTime recTime = DateTime.Parse(csvCols[timeOffsetColumn]);
                                                DateTime cvtTime = TimeZoneInfo.ConvertTime(recTime, desTimeZoneInfo, TimeZoneInfo.Utc);
                                                csvCols[timeOffsetColumn] = cvtTime.ToString(UtcTimeFormat);
                                            }
                                        }
                                        else {
                                            DateTime recTime = DateTime.Parse(csvCols[timeOffsetColumn]);
                                            DateTime cvtTime = TimeZoneInfo.ConvertTime(recTime, desTimeZoneInfo, TimeZoneInfo.Utc);
                                            csvCols[timeOffsetColumn] = cvtTime.ToString(UtcTimeFormat);
                                        }
                                    }
                                    try {
                                        if (trackAlternate && trackAlternateStream != null) {
                                            string nstg = null;
                                            if (csvCols != null) { //TBD: just a test -- remove
                                                nstg = csvCols.Aggregate((current, next) => current + "," + next) + Environment.NewLine;
                                            }
                                            else {
                                                nstg = csvVals + Environment.NewLine;
                                            }
                                            trackAlternateStream.Write(Encoding.UTF8.GetBytes(nstg), 0, Encoding.UTF8.GetByteCount(nstg));
                                            contextData.IncrementCounter("trackAlternate");
                                        }
                                    }
                                    catch (Exception exc) {
                                        string excInfo = $"trackAlternate write Exception on {trackAlternateFile} {exc.Message}, {exc.InnerException?.Message}, disabling trackAlternate";
                                        trackAlternate = false;
                                        if (!doCassandraImport) excInfo += " reenabling cassandra import";
                                        doCassandraImport = true;
                                        contextData.ContextWrite("** ERROR AnalysisBatch", excInfo);
                                    }

                                    if (doCassandraImport) {

                                        ValidateContexts ctxs = ValidateContexts.GetQuerySet(pa, qryFmt, (csvCols != null) ? csvCols : new string[] { csvVals }, translationData);
                                        foreach (ValidateContext ctx in ctxs) {
                                            pa["qSel0"] = ctx.QryString;
                                            pa["qRows"] = getQRows; // at some point be selective
                                            string cassandraQuery = cc1.GetCasQuery(pa);
                                            batchQueries.Add(cassandraQuery);
                                            contextData.Validation.AddLine(cassandraQuery);
                                            batchDataLength += cassandraQuery.Length;
                                        }
                                        if ((batchQueries.Count > batchLimit) || (batchDataLength >= batchDataLimit)) {
                                            if (doAsyncBatch) {
                                                tasks.Add(cc1.PerformCasQueryBatchAsynch(pa, batchQueries));
                                                if (tasks.Count > taskListMax) {
                                                    int waitLimit = (batchTimeout * 1000) / batchSleepMs;
                                                    Task tAll = Task.WhenAll(tasks);
                                                    while (!tAll.IsCompleted && (waitLimit-- > 0)) {
                                                        Thread.Sleep(10);
                                                    }
                                                    if (!tAll.IsCompleted && (waitLimit < 0)) {
                                                        // whoops -- batch not completed
                                                        contextData.ContextWrite("ERROR batch timeout", $">{batchTimeout}secs for {tasks.Count}");
                                                    }
                                                    tasks.Clear();
                                                }
                                            }
                                            else {
                                                cc1.PerformCasQueryBatch(pa, batchQueries);
                                            }
                                            contextData.IncrementCounter("dobatch");
                                            batchQueries.Clear();
                                            batchDataLength = 0;
                                        }
                                    }
                                    // maybe skipping some lines (e.g., alternating in some of the 224 subscription files)
                                    if (skipRefRows > 0) {
                                        for (int skip = 0; skip < skipRefRows && !referenceStream.EndOfStream; skip++) {
                                            var discard = referenceStream.ReadLine();
                                        }
                                    }
                                    if (referenceStream.EndOfStream) break;
                                }
                                catch (Exception exc) {
                                    string excInfo = $"process record {exceptionCount} Exception {exc.Message}, {exc.InnerException?.Message}";
                                    mf?.Error(mfRowHashKey, excInfo, newFilterIngestion);
                                    exceptionCount++;
                                    contextData.Add2Counter("errors", 1);
                                    contextData.ContextWrite("AnalysisBatch", excInfo);
                                    contextData.IncrementCounter("impException");
                                    contextData.AutoFixes.AddLine($"{csvVals} : {excInfo}");

                                }
                            }
                            if (doAsyncBatch) {
                                if (tasks.Count > 0) {  // process any outstanding tasks
                                    int waitLimit = (batchTimeout * 1000) / batchSleepMs;
                                    Task tAll = Task.WhenAll(tasks);
                                    while (!tAll.IsCompleted && (waitLimit-- > 0)) {
                                        Thread.Sleep(10);
                                    }
                                    if (!tAll.IsCompleted && (waitLimit < 0)) {
                                        // whoops -- batch not completed
                                        contextData.ContextWrite("finish batch timeout", $">{batchTimeout}secs for {tasks.Count}");
                                    }
                                    tasks.Clear();
                                }
                            }

                            if (batchQueries.Count > 0) {  // process any outstanding queries (which would not have been added to async yet)
                                cc1.PerformCasQueryBatch(pa, batchQueries);
                                batchQueries.Clear();
                                batchDataLength = 0;
                                contextData.IncrementCounter("dobatch");
                            }
                            if (trackAlternate && trackAlternateStream != null) {
                                trackAlternateStream.Close();
                                trackAlternateStream = null;

                            }

                            pa["X-BIO-etag"] = _myEtag;
                            DateTime dtNow = DateTime.Now;
                            contextData?.ContextWrite("analComplete",
                                $" at {dtNow}, delta = {dtNow.Subtract(dtStart).TotalMilliseconds} ms.");
                            contextData?.Add2Counter("analysisTotal", (long)dtNow.Subtract(dtStart).TotalMilliseconds);
                            if (contextData != null) {
                                contextData.ContextWrite("miss", $"{matchMiss} total not having {pa["matchType"]} count of {expectRows} ");
                                contextData.ContextWrite("hits", $"{matchFound} total     having {pa["matchType"]} count of {expectRows} ");
                                contextData.ContextWrite("read", $"{readRows} rows read");
                                if (autoFixes.Count > 0) {
                                    DateTime dateTime = DateTime.Now;
                                    contextData.ContextWriteSeg("AutoFix", true, dateTime);
                                    string fixFileName = pa["fixFileName"];
                                    string metaString = "";
                                    if (!String.IsNullOrEmpty(pa["useFixMeta"])) {
                                        string[] metaStrings = pa["useFixMeta"].Trim().Split(',');
                                        foreach (string stg in metaStrings) {
                                            metaString += stg + ",";
                                        }
                                    }
                                    ctxLines = ctxLinesMax;
                                    contextData.AutoFixes.AddLine(metaString + csvNames);
                                    foreach (string autoFix in autoFixes.Distinct()) {
                                        if (ctxLines-- > 0) contextData.ContextWrite(autoFix);
                                        contextData.AutoFixes.AddLine(autoFix);
                                        contextData.IncrementCounter("autoFixDistinct");
                                    }
                                    contextData.ContextWriteSeg("AutoFix", false, dateTime);
                                    contextData.ContextWrite("fix_file", fixFileName);
                                    contextData.ContextWrite("fix_web", pa["outputWeb"] + Path.GetFileName(fixFileName));
                                }
                                if (noFixes.Count > 0) {
                                    DateTime dateTime = DateTime.Now;
                                    contextData.ContextWriteSeg("NoFix", true, dateTime);
                                    string metaString = "";
                                    if (!string.IsNullOrEmpty(pa["useFixMeta"])) {
                                        string[] metaStrings = pa["useFixMeta"].Trim().Split(',');
                                        foreach (string stg in metaStrings) {
                                            metaString += stg + ",";
                                        }
                                    }
                                    ctxLines = ctxLinesMax;
                                    contextData.AutoFixes.AddLine(metaString + csvNames);
                                    foreach (string autoFix in noFixes.Distinct()) {
                                        if (ctxLines-- > 0) contextData.ContextWrite(autoFix);
                                        contextData.AutoFixes.AddLine(autoFix);
                                        contextData.IncrementCounter("noFixDistinct");
                                    }
                                    contextData.ContextWriteSeg("NoFix", false, dateTime);
                                }

                            }
                        }
                    }
                }
                else {
                    contextData.ContextWrite("AnalysisBatch", "nothing to do");
                }

                if (!String.IsNullOrEmpty(pa["procResp"]) && (pa["procResp"].ToLower() != "0")) {
                    // process data and write
                    DataUtils1.PrepareResponseInfo(queryResultTables, pa, request, response, pa["resultsRawFileName"] + "_", contextData);
                    contextData.ContextWrite("ResponseHeaders", response?.Headers);
                    string contextReturn = pa["contextRet"] ?? "0";
                    if (contextReturn.Equals("1") && response != null) {
                        response.Content = new StringContent(contextData.Snag());
                    }
                }
                else {
                    // process data no write (maybe merge with above, but this is simpler)
                    DataUtils1.PrepareResponseInfo(queryResultTables, pa, request, response, null, null);
                }
            }
            catch (Exception exc) {
                contextData.ContextWrite("AnalysisBatch", $"Exception o {exc.Message}, {exc.InnerException?.Message}");
                contextData.Add2Counter("errors", 1);
                NameValueCollection nv = new NameValueCollection();
                nv.Add("n", "v");
                XmlDocument doc = XmlDocUtils.GetXMLExceptionDocument("putAnalysis", nv, exc);
                DataUtils1.PrepareResponseInfo(queryResultTables, pa, request, response, null, contextData);
                contextData.ContextWrite("AnalysisBatch", doc.OuterXml);
                if (response != null) {
                    response.Content = new StringContent(doc.OuterXml);
                }
            }
            finally {
                if (mf != null) {
                    mf.SaveSubsTable();
                }
                contextData.ContextWrite("AnalysisBatch", "complete");
                if (referenceStream != null) {
                    referenceStream.Close();
                }
                if (trackAlternate && trackAlternateStream != null) {
                    trackAlternateStream.Close();
                    trackAlternateStream = null;
                }
            }

            long totalRows = contextData.Add2Counter("RefReadRows", 0);
            double totalMsecs = DateTime.Now.Subtract(dtAbStart).TotalMilliseconds;
            double perSec = (totalMsecs > 0 ? totalRows / totalMsecs : -1) * 1000;
            string lStg = $"AnalysisBatch >>>>>> recs/sec {perSec} for readrows={totalRows}, msecs={totalMsecs}";
            contextData.ContextWrite("AnalysisBatch ", lStg);
            if (pa.GetControlVal("requireLogForce", "yes").Equals("yes")) {
                Console.WriteLine(lStg);
            }
            contextData.ContextWrite("AnalysisBatch", "end");
        }

        public static void EstablishIntervalEnvironment(ProcessingArgs pa, string referenceSourceFile) {
            // maybe adjust type of env based on file type.  if set to perfh or perfd and doesnt' match file name swap it.  Bit of a hack for ameren configs
            string dataFileEnvMaps = pa.GetControlVal("dataFileEnvMaps", ""); // if blank is probably bad.
            string[] fileMaps = dataFileEnvMaps.Split(new[] { ';' }, StringSplitOptions.RemoveEmptyEntries);
            string useEnv = "";
            string refileName = Path.GetFileName(referenceSourceFile).ToLower();
            for (int i = 0; i < fileMaps.Length; i++) {
                string[] keyEnv = fileMaps[i].Split(new[] { '=' }, StringSplitOptions.RemoveEmptyEntries);
                if (refileName.IndexOf(keyEnv[0]) >= 0) {
                    useEnv = keyEnv[1];
                    break;
                }
            }
            if (string.IsNullOrEmpty(useEnv)) {
                useEnv = fileMaps[fileMaps.Length - 1].Split(new[] { '=' }, StringSplitOptions.RemoveEmptyEntries)[1];
            }
            string[] nameParts = refileName.Split(new[] { '_' }, StringSplitOptions.RemoveEmptyEntries);

            string clientId = pa["clientId"];
            if (string.IsNullOrEmpty(clientId)) {
                pa["clientId"] = nameParts[0];
            }
            pa["aclenv"] = useEnv;
        }

        /// <summary>
        /// get config val.  default, overriden by config, overriden by pa
        /// </summary>
        /// <param name="pa"></param>
        /// <param name="parName"></param>
        /// <param name="defVal"></param>
        /// <returns></returns>
        public static int GetConfigInt(ProcessingArgs pa, string parName, int defVal) {
            int rVal = defVal;
            int tryLines;
            string tempStg = ConfigurationManager.AppSettings.Get(parName);
            if (!string.IsNullOrEmpty(tempStg)) { }
            if (int.TryParse(tempStg, out tryLines)) {
                rVal = tryLines;
            }
            if (pa[parName] != null) {
                if (int.TryParse(pa[parName], out tryLines)) {
                    rVal = tryLines;
                }
            }
            return rVal;
        }


        public static string ConsolidateFiles(ContextData contextData, ref string referenceSourceFileIn, ProcessingArgs pa) {
            string consolidateFilter = pa.GetControlVal("consFilter", "*.csv");
            IEnumerable<string> files = Directory.EnumerateFiles(referenceSourceFileIn, consolidateFilter);
            string[] fileNames = files.ToArray();
            if (fileNames.Length <= 0) return "";
            string teeDestFile = Path.Combine(referenceSourceFileIn, "CombinedFiles.merged_csv.tee.csv");
            string referenceSourceFileOut = Path.Combine(referenceSourceFileIn, "CombinedFiles.merged_csv");
            pa["mergedRefSource"] = referenceSourceFileOut;
            int[] teeCols = null;
            StreamWriter tw = null;  // to tee of csv if asked to do so
            int totalRowsCount = 0;
            int totalFilesCount = 0;
            bool hasHeaders = pa.GetControlVal("hasheaders", 1) == 1;

            try {
                if (pa != null) {
                    string teeColsStg = pa.GetControlVal("teeCols", "");
                    if (!string.IsNullOrEmpty(teeColsStg)) {
                        string[] teeColStgs = teeColsStg.Split(',');
                        teeCols = new int[teeColStgs.Length];
                        for (int i = 0; i < teeColStgs.Length; i++) {
                            teeCols[i] = int.Parse(teeColStgs[i]);
                        }
                        tw = new StreamWriter(teeDestFile);
                    }
                }
                try {
                    using (StreamWriter sw = new StreamWriter(referenceSourceFileOut)) {
                        string headerData = null;
                        if (hasHeaders) {
                            using (StreamReader sr = new StreamReader(fileNames[0])) {
                                // get header line from first file.
                                headerData = sr.ReadLine();
                                sw.WriteLine(headerData);
                                if (tw != null) {  // sending some cols to another stream
                                    string teeHdrsStg = pa.GetControlVal("teeHdrs", "");
                                    string[] inCols = (!string.IsNullOrEmpty(teeHdrsStg)) ? teeHdrsStg.Split(',') : headerData.Split(',');
                                    string teeStg = "";
                                    for (int j = 0; j < teeCols.Length; j++) {
                                        teeStg += inCols[j] + ",";
                                    }
                                    teeStg = teeStg.Trim(',');
                                    tw.WriteLine(teeStg);
                                }
                                contextData.ContextWrite("consolidate write", $" file '{fileNames[0]}',header='{headerData}'");
                                sr.Close();
                            }
                        }

                        for (int i = 0; i < fileNames.Length; i++) {
                            using (StreamReader sr = new StreamReader(fileNames[i])) {
                                if (hasHeaders) {
                                    headerData = sr.ReadLine(); // eat names
                                }
                                int rowsCount = 0;
                                while (!sr.EndOfStream) {
                                    string input = sr.ReadLine();
                                    if (!String.IsNullOrEmpty(input)) {
                                        // maybe someone just concanteted a bunch of files with headers
                                        if (hasHeaders && !string.IsNullOrEmpty(headerData) && input.Equals(headerData)) {
                                            contextData.ContextWrite("consolidate discard", $" file '{fileNames[i]}', '{input}' ");
                                        }
                                        else {
                                            sw.WriteLine(input);
                                            if (teeCols != null) { // sending some cols to another stream
                                                string[] inCols = input.Split(',');
                                                string teeStg = "";
                                                for (int j = 0; j < teeCols.Length; j++) {
                                                    teeStg += inCols[teeCols[j]] + ",";
                                                }
                                                teeStg = teeStg.Trim(',');
                                                tw.WriteLine(teeStg);
                                            }
                                            if (rowsCount == 0) {
                                                contextData.ContextWrite("consolidate write",
                                                    $" file '{fileNames[i]}', total {totalRowsCount} rows {input} ");
                                            }
                                            rowsCount++;
                                            contextData.IncrementCounter($"{fileNames[i]}.rows");
                                        }
                                    }
                                    else {
                                        contextData.IncrementCounter($"{fileNames[i]}.ignores");
                                    }
                                }
                                totalRowsCount += rowsCount;
                                contextData.ContextWrite("consolidate", $" file '{fileNames[i]}', {rowsCount} rows ");
                                sr.Close();
                            }
                            totalFilesCount++;
                        }
                    }
                }
                catch (Exception exc) {
                    contextData.ContextWrite("error ", $" ConsolidateFiles processing {exc.Message} , {exc.InnerException?.Message} ");
                    contextData.Add2Counter("errors", 1);
                }
                finally {
                    tw?.Close();
                }
            }
            catch (Exception exc) {
                contextData.ContextWrite("error ", $" ConsolidateFiles setup {exc.Message} , {exc.InnerException?.Message} ");
                contextData.Add2Counter("errors", 1);
            }
            contextData.ContextWrite("consolidated", $" {totalFilesCount} files, {totalRowsCount} rows ");
            return referenceSourceFileOut;
        }

    }
}
