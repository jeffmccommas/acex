﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Serialization;

namespace AO.TimeSeriesProcessing {

    public delegate string ProcessDroppedFile(FilesProcessor src, string pathName);

    public class FilesProcessor {

        private List<FileSystemWatcher> _watchersList;
        private string _watchersListPadlock = "_watchersList";
        volatile int _changeHits;
        private ProcessDroppedFile _postMoveProcessor;
        public bool RequestTerminate { get; set; }
        public bool RequestPause { get; set; }
        public int FileRetries { get; set; }
        public int FileOpenRetries { get; set; }
        public int FileConfigRetries { get; set; }

        public int FilesDropped { get; set; }
        public int ConfigDropped { get; set; }
        public int FilesRelocated { get; set; }
        public int FilesIgnored { get; set; }



        public string ConfigChangeData { get; set; }

        public ProcessDroppedFile SetMoveProcessor(ProcessDroppedFile proc) {
            ProcessDroppedFile oldProc = _postMoveProcessor;
            _postMoveProcessor = proc;
            return oldProc;
        }

        public List<string> QueuedList { get; } = new List<string>();
        private string _queuedListPadlock = "_watchersList";
        public List<string> WatchingDoneList { get; } = new List<string>();
        private string _watchingPath = "[unitialized]";
        private string _destinationPath = "[unitialized]";

        public int Count() {
            return WatchingDoneList.Count;
        }
        public string Status() {
            return $" FilesProcessor.watching {WatchingDoneList.Count} in '{_watchingPath}', dest='{_destinationPath}' changehits={_changeHits} counts: cfg={ConfigDropped} drop={FilesDropped} relocate={FilesRelocated} *ignored={FilesIgnored} retries: file={FileRetries}  cfgr={FileConfigRetries} dropr={FileOpenRetries};";
        }


        /// <summary>
        /// hide default constructor
        /// </summary>
        private FilesProcessor() {
        }

        private ProcessingArgs myPa = null;
        public ProcessingArgs FpArgs { get; set; }
        public ContextData FpContext { get; set; }

        public FilesProcessor(ContextData context, ProcessingArgs pa, string srcPath, string destPath) {
            FpContext = context;
            FpArgs = pa;
            WatchFileChanges(pa, srcPath, destPath);
        }

        public void ResetWatchers(ProcessingArgs pa) {
            WatchFileChanges(pa, "", "", true);
        }

        public List<string>[] RelocateChanges(List<string> toRelocateList, string relocateDir = null) {
            lock (_watchersListPadlock) {
                string movetoDir = _destinationPath;

                List<string>[] results = {
                    new List<string>(), new List<string>(), new List<string>(), new List<string>(), new List<string>(),
                    new List<string>()
                };
                if (relocateDir != null & Directory.Exists(relocateDir)) {
                    movetoDir = relocateDir;
                }
                foreach (string s in toRelocateList) {
                    if (WatchingDoneList.Contains(s)) {
                        if (File.Exists(s)) {
                            try {
                                results[1].Add(s);
                                string destFileName = Path.Combine(movetoDir, Path.GetFileName(s));
                                if (File.Exists(destFileName)) {
                                    string newFileName = destFileName + "." + DateTime.Now.Ticks;
                                    File.Move(destFileName, newFileName);
                                    results[5].Add($"FilesProcessor.RelocateChanges moved existing '{destFileName}' to '{newFileName}'");
                                }
                                File.Move(s, destFileName);
                                WatchingDoneList.Remove(s);
                            }
                            catch (Exception exc) {
                                results[5].Add($"FilesProcessor.RelocateChanges issue with {s} exception {exc.Message},{exc.InnerException?.Message}");
                            }
                        }
                        else {
                            results[2].Add(s);
                        }
                    }
                    else {
                        results[3].Add(s);
                    }
                    results[0] = new List<string>(WatchingDoneList);
                    List<string> unaccountedFor = Directory.GetFiles(_watchingPath).ToList();
                    foreach (string s1 in new List<string>(unaccountedFor)) {
                        if (WatchingDoneList.Contains(s1)) {
                            unaccountedFor.Remove(s1);
                        }
                    }
                    results[4] = unaccountedFor;
                }
                return results;
            }
        }


        public List<string> GetChangeList() {
            lock (_watchersListPadlock) {
                return new List<string>(WatchingDoneList);
            }
        }


        int _fileRetriesCount = 20;
        int _fileRetriesSleep = 50;

        private static DateTime _lastConfigChangeTime;

        /// <summary>
        /// Event indicating that some external configuration has changed and our cache may need a refresh
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        private void OnChanged(object source, FileSystemEventArgs e) {
            _changeHits += 1;
            if (e.FullPath.Contains("PoisonTerminate")) {
                Console.WriteLine($" **** OnChanged PoisonTerminate {e.FullPath} {Status()} ");
                if (e.FullPath.Contains("Forced")) {
                    RequestTerminate = true;
                    Process process = Process.GetCurrentProcess();
                    process.Kill();
                }
                else if (e.FullPath.Contains("Graceful")) {
                    RequestTerminate = true;
                }
                else {
                    Console.WriteLine($" **** OnChanged PoisonTerminate no terminate ");
                }
            }
            else if (e.FullPath.Contains("PauseProcessing")) {
                Console.WriteLine($" **** OnChanged PauseProcessing {e.FullPath}  ");
                RequestPause = true;
            }
            else if (e.FullPath.Contains("ResumeProcessing")) {
                Console.WriteLine($" **** OnChanged ResumeProcessing {e.FullPath}  ");
                RequestPause = false;
            }
            else if (e.FullPath.Contains("ConfigurationChange")) {
                Console.WriteLine($" **** OnChanged ConfigurationChange {e.FullPath}  ");
                if (DateTime.Now.Subtract(_lastConfigChangeTime).TotalSeconds > 30) {  // to suppress churn
                    try {
                        _lastConfigChangeTime = DateTime.Now;  
                        int tries = TryOpenFile(e);
                        if (tries != _fileRetriesCount) {
                            FileConfigRetries += 1;
                            string addlInfo = "none";
                            if (tries <= 0) {
                                addlInfo = "possible config file open issue in future";
                            }
                            Console.WriteLine(
                                $" **** OnChanged config file '{e.FullPath}' drop ***RETRIED needed {_fileRetriesCount - tries} retires of {_fileRetriesCount} max to open file. addlInfo={addlInfo} ");
                        }
                        Dictionary<string, string> replPars = new Dictionary<string, string>();

                        string cfgRepl = FpArgs.GetControlVal("replPars", "");
                       try {
                            if (!string.IsNullOrEmpty(cfgRepl)) {
                                string[] replArgs = cfgRepl.Split(new[] {" "}, StringSplitOptions.RemoveEmptyEntries);
                                foreach (string replArg in replArgs) {
                                    string[] nvPair = replArg.Split(new[] {"@"}, StringSplitOptions.RemoveEmptyEntries);
                                    replPars.Add(nvPair[0],(nvPair.Length>1)?nvPair[1]:"");
                                }
                            }
                        }
                        catch (Exception exc) {
                            Console.WriteLine(
                                $" **** OnChanged ConfigurationChange replPars '{cfgRepl}' exception {e.FullPath} = {exc.Message} , {exc.InnerException?.Message} ");
                        }

                        Thread.Sleep(200); // little delay since sometimes the file is not fully committed yet.
                        using (StreamReader sr = new StreamReader(e.FullPath)) {
                            int maxCfgCnt = 100;
                            string configData = "";
                            while (!sr.EndOfStream && maxCfgCnt-- > 0) {
                                string configLine = sr.ReadLine();
                                if (!string.IsNullOrEmpty(configLine)) {
                                    configLine = configLine.Trim();
                                    if (!string.IsNullOrEmpty(configLine)) {
                                        if (replPars.Count > 0) {
                                            foreach (KeyValuePair<string, string> kvp in replPars) {
                                                configLine = configLine.Replace(kvp.Key, kvp.Value);
                                            }
                                        }
                                        configData += configLine + Environment.NewLine;
                                    }
                                    Console.WriteLine($" **** OnChanged ConfigurationChange data: '{configLine}'  ");
                                    FpContext.ContextWrite("FileProcessor",
                                        $" **** OnChanged ConfigurationChange data: '{configLine}'  ");
                                }
                            }
                            if (!string.IsNullOrEmpty(configData)) {
                                ConfigChangeData = configData.Trim();
                                ConfigDropped++;
                            }
                            else {
                                Console.WriteLine($" **** OnChanged ConfigurationChange data: '{configData}'  ");
                            }
                        }
                    }
                    catch (Exception exc) {
                        Console.WriteLine(
                            $" **** OnChanged ConfigurationChange exception {e.FullPath} = {exc.Message} , {exc.InnerException?.Message} ");
                    }
                }
            }
            else {
                if (!RequestTerminate) {
                    lock (_watchersListPadlock) {
                        if (!WatchingDoneList.Contains(e.FullPath)) {
                            int tries = TryOpenFile(e);
                            if (tries != _fileRetriesCount) {
                                FileOpenRetries += 1;
                                string addlInfo = "none";
                                if (tries <= 0) {
                                    addlInfo = "possible file open issue in future";
                                }
                                Console.WriteLine($" **** OnChanged file '{e.FullPath}' drop ***RETRIED needed {_fileRetriesCount - tries} retires of {_fileRetriesCount} max to open file. addlInfo={addlInfo} ");
                            }
                            AddProcessFile(e.FullPath);
                            Console.WriteLine($" **** OnChanged file '{e.FullPath}' drop ");
                        }
                    }
                }
                else {
                    Console.WriteLine($" ****OnChanged requestTerminate so not queueing {e.FullPath} ");
                }
            }

        }

        private int TryOpenFile(FileSystemEventArgs e) {
            int tries = _fileRetriesCount;
            if (File.Exists(e.FullPath)) {
                Thread.Sleep(1);  // we often get exception 1'st time, give things a chanch to settle
                while (tries > 0) {
                    try {
                        using (FileStream stream = File.Open(e.FullPath, FileMode.Open, FileAccess.Read)) {
                            byte[] buf = new byte[2];
                            int len = stream.Read(buf, 0, 1);
                            if (len > 0) { // just testing read
                            }
                            break;
                        }
                    }
                    catch (IOException) {
                        FileRetries++;
                        Thread.Sleep(_fileRetriesSleep);
                        tries--;
                    }
                }
            }
            return tries;
        }

        private void AddProcessFile(string fullPath) {
            if (_postMoveProcessor != null) {
                lock (_queuedListPadlock) {
                    if (!QueuedList.Contains(fullPath)) {
                        QueuedList.Add(fullPath);
                        FilesDropped++;
                    }
                    else {
                        FilesIgnored++;
                    }
                }
            }
        }

        public int DoProcessOne() {
            int status = -1;
            if (QueuedList.Count > 0) {
                string one;
                lock (_queuedListPadlock) {
                    one = QueuedList[0];
                    QueuedList.RemoveAt(0);
                }
                if (!string.IsNullOrEmpty(one)) {
                    status = DoProcessFile(one);
                }
            }
            return status;
        }

        private int DoProcessFile(string fullPath) {
            int errorCount = 0;
            if (_postMoveProcessor != null) {
                if (fullPath.Contains(" ")) {
                    string oldPath = fullPath;
                    fullPath = fullPath.Replace(" ", "_");
                    File.Move(oldPath, fullPath);
                }
                string tVal = _postMoveProcessor(this, fullPath);
                int.TryParse(tVal, out errorCount);
            }
            if (errorCount == 0) {  // don't record file if any errors appeared
                WatchingDoneList.Add(fullPath);
            }
            return errorCount;
        }

        public void FlushPending() {
            List<string> unaccountedFor = Directory.GetFiles(_watchingPath).ToList();
            foreach (string s1 in new List<string>(unaccountedFor)) {
                if (WatchingDoneList.Contains(s1)) {
                    unaccountedFor.Remove(s1);
                }
                else {
                    AddProcessFile(s1);
                }
            }

        }


        /// <summary>
        /// Set up, if necessary, the watching of files in the path specified (or a part of) the call param.
        /// note: this is is set up for more but only handles a single so only do ONCE until _watchingPath is made a collection.
        /// </summary>
        /// <param name="pa"></param>
        /// <param name="path2Watch">path name for files to watch (can be a filename -- we will extract the path)</param>
        /// <param name="destinationPath"></param>
        /// <param name="forceInit"></param>
        public void WatchFileChanges(ProcessingArgs pa, string path2Watch, string destinationPath, bool forceInit = false) {

            if (_watchersList != null) {
                if (!forceInit) {
                    Console.WriteLine("FilesProcessor.WatchFileChanges: not null and not forced");
                    return;
                }
                else {
                    Console.WriteLine("FilesProcessor.WatchFileChanges force reinit: ");
                    foreach (FileSystemWatcher fileSystemWatcher in _watchersList) {
                        fileSystemWatcher.EnableRaisingEvents = false;
                    }
                    _watchersList = null;   // watch out -- this will leak a little (but shouldn't be called much if at all)
                }
            }

            _fileRetriesCount = pa.GetControlVal("fileRetriesCount", _fileRetriesCount);
            _fileRetriesSleep = pa.GetControlVal("fileRetriesSleep", _fileRetriesSleep);
            if (string.IsNullOrEmpty(path2Watch)) path2Watch = pa.GetControlVal("dropLocation", "");
            if (!Directory.Exists(path2Watch)) {
                Console.WriteLine($"FilesProcessor.WatchFileChanges: {path2Watch} does not exist");
                Directory.CreateDirectory(path2Watch);
                Console.WriteLine($"FilesProcessor.WatchFileChanges: created destination '{path2Watch}' ");
            }
            string path = path2Watch;
            if (_watchersList == null) {
                lock (_watchersListPadlock) {
                    // check again under lock
                    if (_watchersList == null) {
                        // this is how we see if the config file changes
                        _watchersList = new List<FileSystemWatcher>();
                    }
                }
            }

            if (File.Exists(path2Watch)) {
                // if a file get the path where the file exists
                path = Path.GetDirectoryName(path2Watch);
            }
            if (!Directory.Exists(path)) {
                Console.WriteLine($"FilesProcessor.WatchFileChanges: nonexistant watch path '{path}' ");
                return;
            }
            _watchingPath = path;
            if (string.IsNullOrEmpty(destinationPath)) destinationPath = pa.GetControlVal("dropRelocate", "");
            if (!string.IsNullOrEmpty(destinationPath)) {
                try {
                    if (!Directory.Exists(destinationPath)) {
                        Directory.CreateDirectory(destinationPath);
                        Console.WriteLine($"FilesProcessor.WatchFileChanges: created destination '{destinationPath}' ");
                    }
                    _destinationPath = destinationPath;
                }
                catch (Exception exc) {
                    Console.WriteLine($"FilesProcessor.WatchFileChanges: exception -- cannot use supplied path '{destinationPath}' {exc.Message}, {exc.InnerException?.Message} ");
                }
            }
            else {
                _destinationPath = Path.Combine(_watchingPath, pa.GetControlVal("dropLocationRelocate", "relocated"));
            }
            if (!Directory.Exists(_destinationPath)) {
                Directory.CreateDirectory(_destinationPath);
                Console.WriteLine($"FilesProcessor.WatchFileChanges: created destination '{_destinationPath}' ");
            }

            bool watching = false;
            foreach (FileSystemWatcher watcher in _watchersList) {
                if (watcher.Path == path) {
                    watching = true;
                    break;
                }
            }
            if (!watching) {
                lock (_watchersList) {
                    // check again under lock
                    foreach (FileSystemWatcher watcher in _watchersList) {
                        if (watcher.Path == path) {
                            watching = true;
                            break;
                        }
                    }
                    if (!watching) {
                        string watchFileTemplates = pa.GetControlVal("dropFiles2Watch", "*.csv");
                        string[] watchingFiles = watchFileTemplates.Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                        foreach (string ft in watchingFiles) {
                            FileSystemWatcher configFileWatcher = new FileSystemWatcher();
                            configFileWatcher.Path = path;
                            //configFileWatcher.NotifyFilter = NotifyFilters.LastWrite;
                            configFileWatcher.NotifyFilter = NotifyFilters.LastAccess | NotifyFilters.LastWrite
                                                             | NotifyFilters.FileName | NotifyFilters.DirectoryName;
                            configFileWatcher.Filter = ft;
                            //configFileWatcher.Created += OnChanged;
                            configFileWatcher.Changed += OnChanged;
                            configFileWatcher.Created += OnChanged;
                            configFileWatcher.EnableRaisingEvents = true;
                            _watchersList.Add(configFileWatcher);
                        }
                    }
                }
            }
        }


    }


    public class ZipManager {

        /// <summary>
        /// hide empty constructor
        /// </summary>
        private ZipManager() {
        }


        public void ExtractAndDrop(string zipSrc, string inDropLocation) {
            if (string.IsNullOrEmpty(inDropLocation)) {
                inDropLocation = pa.GetControlVal("dropLocation", @"c:\junk\t114");
            }
            try {
                bool failed = false;
                string errorInfo = "";
                try {
                    FileAttributes attr = File.GetAttributes(zipSrc);
                    int dropCounter = 0;  // used to load balance files across drop locations
                    if ((attr & FileAttributes.Directory) == FileAttributes.Directory) {
                        string consolidateFilter = pa.GetControlVal("consFilter", "*.zip");
                        IEnumerable<string> fnames = Directory.EnumerateFiles(zipSrc, consolidateFilter);
                        foreach (string fname in fnames) {
                            context.ContextWrite("ziptest process", fname);
                            dropCounter = this.TestFileExtract(fname, inDropLocation, dropCounter);
                            context.IncrementCounter("zipsProcessed");
                        }
                    }

                    else {
                        TestFileExtract(zipSrc, inDropLocation, 0);
                        context.IncrementCounter("zipsProcessed");
                    }
                }
                catch (Exception exc) {
                    failed = true;
                    errorInfo = $"ziptest exception {exc.Message}, {exc.InnerException?.Message}";
                    context.ContextWrite("ziptest exception", errorInfo);
                }
                if (failed) {
                    string errorTxt =
                        $"ziptest {errorInfo}, needs a zipSrc input file/directory and dropLocation path.  '{zipSrc}'  and '{inDropLocation}' .";
                    Console.WriteLine(errorTxt);
                    context.IncrementCounter("errors");
                }
            }
            catch (Exception exc) {
                context.IncrementCounter("ZipManagerErrors");
                context.IncrementCounter("errors");
                context.ContextWrite($"ZipManager.ExtractAndDrop",
                    $" exception {exc.Message}, {exc.InnerException?.Message}");
            }
        }

        ProcessingArgs pa;
        private ContextData context;
        public ZipManager(ProcessingArgs ipa, ContextData icontext) {
            pa = ipa;
            context = icontext;
        }

        public const string StandardTimeFormatBaseTemp = "yyyy-MM-ddTHH:mm:ss{0}";

        public void Convert2TallAmiFs(string zipFile, string destPath) {
            if (string.IsNullOrEmpty(destPath)) {
                destPath = pa.GetControlVal("dropLocation", @"c:\junk\t114");
            }
            try {
                if (!Directory.Exists(destPath)) {
                    Directory.CreateDirectory(destPath);
                    context.ContextWrite("ZipManager.Convert2TallAmi", $" create drop location {destPath} ");
                    context.IncrementCounter("zipDropCreate");
                }
                string useHeaders = pa.GetControlVal("useHeaders", @"ClientId,MeterId,AccountNumber,ServicePointId,CommodityId,UOMId,VolumeFactor,Direction,TOUID,ProjectedReadDate,TimeStamp,Timezone,IntValue0000");
                string csvColTemp = pa.GetControlVal("csvColTemp", @"-,-,-,-,-,-,-,-,0,-,-,-,-");
                string csvColMap = pa.GetControlVal("csvColMap", @"0,1,2,3,4,5,6,7,-1,8,9,10,-1");
                int csvWideTimestampCol = pa.GetControlVal("csvTallTimestampCol", 9);
                int csvTallTimestampCol = pa.GetControlVal("csvTallTimestampCol", 10);
                int csvWideValCol = pa.GetControlVal("csvTallValCol", 11);
                int csvTallValCol = pa.GetControlVal("csvTallValCol", 12);
                string tsTimeZoneInfo = pa.GetControlVal("tsTimeZoneInfo", "Z");
                string standardTimeFormatBase = string.Format(StandardTimeFormatBaseTemp, tsTimeZoneInfo);
                string[] csvColArray = csvColMap.Split(',');
                int[] csvIntMap = new int[csvColArray.Length];
                for (int i = 0; i < csvIntMap.Length; i++) {
                    csvIntMap[i] = int.Parse(csvColArray[i]);
                }

                bool retainTallSrc = pa.GetControlVal("retainTallSrc", "yes").Equals("yes");
                bool haveHeaders = pa.GetControlVal("haveCsvHeaders", "yes").Equals("yes");
                bool allowOverwrite = pa.GetControlVal("overwriteDrop", "no").Equals("yes");
                int procLimit = pa.GetControlVal("procLimit", 100);
                bool stripCsvQuotes = string.Equals(pa.GetControlVal("stripCsvQ", "no"), "yes");
                bool addCsvQuotes = string.Equals(pa.GetControlVal("addCsvQuotes", "yes"), "yes");
                using (ZipArchive archive = ZipFile.OpenRead(zipFile)) {
                    string tallZipFileName;
                    foreach (ZipArchiveEntry entry in archive.Entries) {
                        string tallFileName = $"{entry.FullName}.tall.csv";
                        string tallFileZipName = $"{entry.FullName}.tall.csv.zip";
                        tallZipFileName = Path.Combine(destPath, tallFileZipName);
                        string destFileName = Path.Combine(destPath, tallFileName);
                        if (File.Exists(destFileName)) {
                            if (allowOverwrite) {
                                File.Delete(destFileName);
                                context.IncrementCounter("deleteUnzipDest");
                                context.ContextWrite("ziptest extract overwrite", destFileName);
                            }
                            else {
                                string err = $" Error: ZipManager.Convert2TallAmi overwrite not allowed for {destFileName}";
                                context.ContextWrite(err);
                                throw new Exception(err);
                            }
                        }
                        string tempFilePath = Path.Combine(destPath, "temp");
                        if (!Directory.Exists(tempFilePath)) {
                            Directory.CreateDirectory(tempFilePath);
                            context.ContextWrite("ZipManager.Convert2TallAmi", $" create tempFilePath {tempFilePath} ");
                            context.IncrementCounter("zipTempCreate");
                        }
                        string tallFileTempName = Path.Combine(tempFilePath, tallFileName);
                        using (StreamWriter sw = new StreamWriter(tallFileTempName)) {
                            var lineCount = 0;
                            string headers = "";
                            using (var stream = entry.Open()) {
                                using (StreamReader sr = new StreamReader(stream)) {
                                    if (haveHeaders) {
                                        if (string.IsNullOrEmpty(headers)) {
                                            headers = sr.ReadLine(); // remove and save headers
                                            if (!string.IsNullOrEmpty(useHeaders)) {
                                                sw.WriteLine(useHeaders);
                                            }
                                            else {
                                                sw.WriteLine(headers);
                                            }
                                        }
                                        else {
                                            sr.ReadLine(); // remove headers
                                        }
                                    }
                                    while (!sr.EndOfStream) {
                                        string rec = sr.ReadLine();
                                        //sw.WriteLine($" will expand {rec}");
                                        if (stripCsvQuotes) {
                                            rec = rec.Replace("\"", "");
                                        }
                                        string[] cols = rec.Split(',');
                                        DateTime dt = DateTime.Parse(cols[csvWideTimestampCol]);
                                        int startSrcValsCol = csvWideValCol;
                                        context.IncrementCounter("totalrows");
                                        for (int i = 0; i < 24; i++) {
                                            string[] newCsvVals = csvColTemp.Split(',');
                                            for (int cm = 0; cm < newCsvVals.Length; cm++) {
                                                if (csvIntMap[cm] >= 0) {
                                                    newCsvVals[cm] = cols[csvIntMap[cm]];
                                                }
                                            }
                                            newCsvVals[csvTallTimestampCol] = dt.ToString(standardTimeFormatBase);
                                            newCsvVals[csvTallValCol] = cols[startSrcValsCol + i];
                                            if (addCsvQuotes) {
                                                for (int cm = 0; cm < newCsvVals.Length; cm++) {
                                                    //newCsvVals[cm] = $"\"{newCsvVals[cm]}\"";
                                                    //newCsvVals[cm] = string.Format("\"{0}\"", newCsvVals[cm]);
                                                    newCsvVals[cm] = '\"' + newCsvVals[cm] + '\"';  // significantly faster than others 1.5 (1.0 for none, 2.0 for interp)
                                                }
                                            }

                                            string newCsv = newCsvVals.Aggregate((current, next) => current + ',' + next);
                                            sw.WriteLine(newCsv);
                                            dt = dt.AddHours(1);
                                        }
                                        context.Add2Counter("unzip2TallRecCnt", 24);

                                        lineCount++;
                                        if (lineCount >= procLimit) {
                                            break;
                                        }
                                    }
                                }
                            }
                            if (lineCount > 0) {
                                context.Add2Counter("unzipCvtRecCnt", lineCount);
                            }
                        }
                        context.ContextWrite("ZipManager.Convert2TallAmi", $"generated {tallZipFileName} from {tallFileName}");
                        using (ZipArchive narchive = ZipFile.Open(tallZipFileName, ZipArchiveMode.Update)) {
                            narchive.CreateEntryFromFile(tallFileTempName, tallFileName);
                            context.ContextWrite("ZipManager.Convert2TallAmi", $"compressed {tallZipFileName} from {tallFileName}");
                        }
                        FileInfo fit = new FileInfo(tallFileTempName);
                        FileInfo fiz = new FileInfo(tallZipFileName);
                        context.Add2Counter("unzipCvtRecTallSize", fit.Length);
                        context.Add2Counter("unzipCvtRecZipSize", fiz.Length);
                        context.ContextWrite("ZipManager.Convert2TallAmi", $"zipsize {fiz.Length} tallsize {fit.Length}");
                        if (!retainTallSrc) {
                            File.Delete(tallFileTempName);
                            context.IncrementCounter("unzipCvtRecDelSrcCnt");
                        }
                    }
                }
                string moveToPath = Path.Combine(Path.GetDirectoryName(zipFile), "extracted");
                if (!Directory.Exists(moveToPath)) {
                    Directory.CreateDirectory(moveToPath);
                    context.IncrementCounter("unzipCvtMove");
                }
                string destFile = Path.Combine(moveToPath, Path.GetFileName(zipFile));
                if (File.Exists(destFile) && pa.GetControlVal("overwriteDrop", "no").Equals("yes")) {
                    File.Delete(destFile);
                    context.IncrementCounter("unzipCvtDelete");
                    context.ContextWrite("ZipManager.Convert2TallAmi done overwrite", destFile);
                }
                File.Move(zipFile, destFile);
            }
            catch (Exception exc) {
                context.IncrementCounter("errors");
                context.ContextWrite($"ZipManager.Convert2TallAmi", $" exception {exc.Message}, {exc.InnerException?.Message}");
            }

        }



        //for DST conversions below:  BUGBUG -- short-short and short-mid projects -- only a few years here.  Generalize fix.
        //DST 2015 in Massachusetts began at 2:00 AM on Sunday, March 8
        //and ended at 2:00 AM on Sunday, November 1
        //All times are in Eastern Time.

        //    DST 2016 in Massachusetts began at 2:00 AM on Sunday, March 13
        //and ended at 2:00 AM on Sunday, November 6
        //All times are in Eastern Time.

        //    DST 2017 begins at 2:00 AM on Sunday, March 12
        //and ends at 2:00 AM on Sunday, November 5


        //DST 2018 in Massachusetts will begin at 2:00 AM on Sunday, March 11
        //and ends at 2:00 AM on Sunday, November 4
        //All times are in Eastern Time.



        private static int[][] shiftDays = new int[6][];
        private static bool isShiftDaysInitialized = false;

        // {2017,3,11,1},{2017,11,4,-1},{2016,3,13,1},{2016,11,6,-1},{2015,3,8,1},{2015,11,1,-1},
        //
        static void InitShiftDays() {
            shiftDays[0] = new int[] { 2017, 3, 11, -1 };
            shiftDays[1] = new int[] { 2017, 11, 4, 1 };
            shiftDays[2] = new int[] { 2016, 3, 13, -1 };
            shiftDays[3] = new int[] { 2016, 11, 6, 1 };
            shiftDays[4] = new int[] { 2015, 3, 8, -1 };
            shiftDays[5] = new int[] { 2015, 11, 1, 1 };
            isShiftDaysInitialized = true;
        }

        static int GetDTShift(DateTimeOffset dto) {
            if (!isShiftDaysInitialized) {
                InitShiftDays();
            }
            int rval = 0;
            for (int i = 0; i < shiftDays.Length; i++) {
                if (
                    (dto.Year == shiftDays[i][0]) &&
                    (dto.Month == shiftDays[i][1]) &&
                    (dto.Day == shiftDays[i][2])) {
                    rval = shiftDays[i][3];
                    if (dto.Hour == 2) {
                        rval *= 2;
                    }
                }
            }
            return rval;
        }

        /// <summary>
        /// generate UTC tall files from LOCAL wide files.
        /// Note: several dev varients and code regarding doTimeOffset should to be purged (it doesn't matter any more)
        /// </summary>
        /// <param name="zipFile"></param>
        /// <param name="destPath"></param>
        public void Convert2TallAmiOrignal(string zipFile, string destPath) {
            if (string.IsNullOrEmpty(destPath)) {
                destPath = pa.GetControlVal("dropLocation", @"c:\junk\t114");
            }
            try {
                if (!Directory.Exists(destPath)) {
                    Directory.CreateDirectory(destPath);
                    context.ContextWrite("ZipManager.Convert2TallAmi", $" create drop location {destPath} ");
                    context.IncrementCounter("zipDropCreate");
                }
                string useHeaders = pa.GetControlVal("useHeaders", @"ClientId,MeterId,AccountNumber,ServicePointId,CommodityId,UOMId,VolumeFactor,Direction,TOUID,ProjectedReadDate,TimeStamp,Timezone,IntValue0000");
                string csvColTemp = pa.GetControlVal("csvColTemp", @"-,-,-,-,-,-,-,-,0,-,-,-,-");
                string csvColMap = pa.GetControlVal("csvColMap", @"0,1,2,3,4,5,6,7,-1,8,9,10,-1");
                int csvWideTimestampCol = pa.GetControlVal("csvTallTimestampCol", 9);
                int csvTallTimestampCol = pa.GetControlVal("csvTallTimestampCol", 10);
                int csvWideValCol = pa.GetControlVal("csvTallValCol", 11);
                int csvTallValCol = pa.GetControlVal("csvTallValCol", 12);
                string tsTimeZoneInfo = pa.GetControlVal("tsTimeZoneInfo", "Z");
                string standardTimeFormatBase = string.Format(StandardTimeFormatBaseTemp, tsTimeZoneInfo);
                string[] csvColArray = csvColMap.Split(',');
                int[] csvIntMap = new int[csvColArray.Length];
                for (int i = 0; i < csvIntMap.Length; i++) {
                    csvIntMap[i] = int.Parse(csvColArray[i]);
                }

                bool retainTallSrc = pa.GetControlVal("retainTallSrc", "yes").Equals("yes");
                bool haveHeaders = pa.GetControlVal("haveCsvHeaders", "yes").Equals("yes");
                bool allowOverwrite = pa.GetControlVal("overwriteDrop", "no").Equals("yes");
                int procLimit = pa.GetControlVal("procLimit", 100);
                bool stripCsvQuotes = string.Equals(pa.GetControlVal("stripCsvQ", "no"), "yes");
                bool addCsvQuotes = string.Equals(pa.GetControlVal("addCsvQuotes", "yes"), "yes");


                TimeZoneInfo desTimeZoneInfo = null;
                bool doTimeOffset = BatchUtils.GetConfigInt(pa, "doTimeOffset", 1) == 1;
                string destTimeZoneString = pa.GetControlVal("destTimeZoneString", "Central Standard Time");
                if (doTimeOffset) {
                    if (!string.IsNullOrEmpty(destTimeZoneString)) {
                        desTimeZoneInfo = TimeZoneInfo.FindSystemTimeZoneById(destTimeZoneString);
                    }
                    else {
                        doTimeOffset = false;
                    }
                }
                


                using (ZipArchive archive = ZipFile.OpenRead(zipFile)) {
                    string tallZipFileName;
                    foreach (ZipArchiveEntry entry in archive.Entries) {
                        string tallFileName = $"{entry.FullName}.tall.csv";
                        string tallFileZipName = $"{entry.FullName}.tall.csv.zip";
                        tallZipFileName = Path.Combine(destPath, tallFileZipName);

                        string destFileName = Path.Combine(destPath, tallFileName);
                        if (File.Exists(destFileName)) {
                            if (allowOverwrite) {
                                File.Delete(destFileName);
                                context.IncrementCounter("deleteUnzipDest");
                                context.ContextWrite("ziptest extract overwrite", destFileName);
                            }
                            else {
                                string err = $" Error: ZipManager.Convert2TallAmi overwrite not allowed for {destFileName}";
                                context.ContextWrite(err);
                                throw new Exception(err);
                            }
                        }
                        using (var sw = new MemoryStream()) {
                            var lineCount = 0;
                            string headers = "";
                            using (var stream = entry.Open()) {
                                using (StreamReader sr = new StreamReader(stream)) {
                                    if (haveHeaders) {
                                        if (string.IsNullOrEmpty(headers)) {
                                            headers = sr.ReadLine(); // remove and save headers
                                            if (!string.IsNullOrEmpty(useHeaders)) {
                                                byte[] bytes = Encoding.UTF8.GetBytes(useHeaders + "\n");
                                                sw.Write(bytes, 0, bytes.Length);
                                                //sw.WriteLine(useHeaders);
                                            }
                                            else {
                                                byte[] bytes = Encoding.UTF8.GetBytes(headers + "\n");
                                                sw.Write(bytes, 0, bytes.Length);
                                                //sw.WriteLine(headers);
                                            }
                                        }
                                        else {
                                            sr.ReadLine(); // remove headers
                                        }
                                    }
                                    DateTimeFormatInfo  dtFormat = new DateTimeFormatInfo();
                                    while (!sr.EndOfStream) {
                                        string rec = sr.ReadLine();
                                        //sw.WriteLine($" will expand {rec}");
                                        if (stripCsvQuotes) {
                                            rec = rec.Replace("\"", "");
                                        }
                                        string[] cols = rec.Split(',');

                                        TimeZoneInfo tzi = TimeZoneInfo.FindSystemTimeZoneById(destTimeZoneString);
                                        DateTime dtSttq = DateTime.Parse(cols[csvWideTimestampCol]);
                                        DateTimeOffset dt = new DateTimeOffset(dtSttq.Year, dtSttq.Month, dtSttq.Day, dtSttq.Hour, dtSttq.Minute, dtSttq.Second, tzi.GetUtcOffset(dtSttq));
                                        TimeSpan tsZero = new TimeSpan();

                                        int startSrcValsCol = csvWideValCol;
                                        for (int i = 0; i < 24; i++) {
                                            int dtOfsettCode = GetDTShift(dt);
                                            if (dtOfsettCode < -1) {
                                                dt = dt.AddHours(1);
                                                continue;
                                            }
                                            TimeSpan tzOffset = tzi.GetUtcOffset(dt);
                                            DateTimeOffset dtos = dt.AddHours(-tzOffset.Hours);
                                            DateTimeOffset dtoUTC = new DateTimeOffset(dtos.Year, dtos.Month, dtos.Day, dtos.Hour, dtos.Minute, dtos.Second, tsZero);

                                            string[] newCsvVals = csvColTemp.Split(',');
                                            for (int cm = 0; cm < newCsvVals.Length; cm++) {
                                                if (csvIntMap[cm] >= 0) {
                                                    newCsvVals[cm] = cols[csvIntMap[cm]];
                                                }
                                            }

                                            newCsvVals[csvTallTimestampCol] = dtoUTC.ToString(standardTimeFormatBase);
                                            newCsvVals[csvTallValCol] = cols[startSrcValsCol + i];
                                            if (addCsvQuotes) {
                                                for (int cm = 0; cm < newCsvVals.Length; cm++) {
                                                    //newCsvVals[cm] = $"\"{newCsvVals[cm]}\"";
                                                    //newCsvVals[cm] = string.Format("\"{0}\"", newCsvVals[cm]);
                                                    newCsvVals[cm] =
                                                        '\"' + newCsvVals[cm] +
                                                        '\"'; // significantly faster than others 1.5 (1.0 for none, 2.0 for interp)
                                                }
                                            }

                                            string newCsv =
                                                newCsvVals.Aggregate((current, next) => current + ',' + next);
                                            byte[] bytes = Encoding.UTF8.GetBytes(newCsv + "\n");
                                            sw.Write(bytes, 0, bytes.Length);
                                            //sw.WriteLine(newCsv);
                                            dt = dt.AddHours(1);
                                        }
                                        context.Add2Counter("unzip2TallRecCnt", 24);

                                        lineCount++;
                                        if (lineCount >= procLimit) {
                                            break;
                                        }
                                    }
                                }
                            }
                            if (lineCount > 0) {
                                context.Add2Counter("unzipCvtRecCnt", lineCount);
                            }
                            using (ZipArchive narchive = ZipFile.Open(tallZipFileName, ZipArchiveMode.Update)) {
                                sw.Flush();
                                sw.Position = 0;
                                ZipArchiveEntry nentry = narchive.CreateEntry(tallFileName, CompressionLevel.Fastest);
                                using (Stream entryStream = nentry.Open()) {
                                    sw.CopyTo(entryStream);
                                }
                                //narchive.CreateEntryFromFile(tallFileTempName, tallFileName);
                                context.ContextWrite("ZipManager.Convert2TallAmi", $"generated {tallZipFileName} from {tallFileName}");
                            }
                            //FileInfo fit = new FileInfo(tallFileTempName);
                            FileInfo fiz = new FileInfo(tallZipFileName);
                            context.Add2Counter("unzipCvtRecTallSize", sw.Length);
                            context.Add2Counter("unzipCvtRecZipSize", fiz.Length);
                            context.ContextWrite("ZipManager.Convert2TallAmi", $"zipsize {fiz.Length} tallsize {sw.Length}");
                            //if (!retainTallSrc) {
                            //    File.Delete(tallFileTempName);
                            //    context.IncrementCounter("unzipCvtRecDelSrcCnt");
                            //}
                        }
                    }
                }
                string moveToPath = Path.Combine(Path.GetDirectoryName(zipFile), "extracted");
                if (!Directory.Exists(moveToPath)) {
                    Directory.CreateDirectory(moveToPath);
                    context.IncrementCounter("unzipCvtMove");
                }
                string destFile = Path.Combine(moveToPath, Path.GetFileName(zipFile));
                if (File.Exists(destFile) && pa.GetControlVal("overwriteDrop", "no").Equals("yes")) {
                    File.Delete(destFile);
                    context.IncrementCounter("unzipCvtDelete");
                    context.ContextWrite("ZipManager.Convert2TallAmi done overwrite", destFile);
                }
                File.Move(zipFile, destFile);
            }
            catch (Exception exc) {
                context.IncrementCounter("errors");
                context.ContextWrite($"ZipManager.Convert2TallAmi", $" exception {exc.Message}, {exc.InnerException?.Message}");
            }

        }

        /// <summary>
        /// generate UTC tall files from LOCAL wide files.
        /// Note: several dev varients and code regarding doTimeOffset should to be purged (it doesn't matter any more)
        /// </summary>
        /// <param name="zipFile"></param>
        /// <param name="destPath"></param>
        public void Convert2TallAmi(string zipFile, string destPath) {
            if (string.IsNullOrEmpty(destPath)) {
                destPath = pa.GetControlVal("dropLocation", @"c:\junk\t114");
            }
            try {
                if (!Directory.Exists(destPath)) {
                    Directory.CreateDirectory(destPath);
                    context.ContextWrite("ZipManager.Convert2TallAmi", $" create drop location {destPath} ");
                    context.IncrementCounter("zipDropCreate");
                }
                string csvColMap = pa.GetControlVal("csvColMap", @"0,1,2,3,4,5,6,7,-1,8,9,10,-1");
                string[] csvColArray = csvColMap.Split(',');
                int[] csvIntMap = new int[csvColArray.Length];
                for (int i = 0; i < csvIntMap.Length; i++) {
                    csvIntMap[i] = int.Parse(csvColArray[i]);
                }

                bool allowOverwrite = pa.GetControlVal("overwriteDrop", "no").Equals("yes");
                
                using (ZipArchive archive = ZipFile.OpenRead(zipFile)) {
                    string tallZipFileName;
                    foreach (ZipArchiveEntry entry in archive.Entries) {
                        string tallFileName = $"{entry.FullName}.tall.csv";
                        string tallFileZipName = $"{entry.FullName}.tall.csv.zip";
                        tallZipFileName = Path.Combine(destPath, tallFileZipName);

                        string destFileName = Path.Combine(destPath, tallFileName);
                        if (File.Exists(destFileName)) {
                            if (allowOverwrite) {
                                File.Delete(destFileName);
                                context.IncrementCounter("deleteUnzipDest");
                                context.ContextWrite("ziptest extract overwrite", destFileName);
                            }
                            else {
                                string err = $" Error: ZipManager.Convert2TallAmi overwrite not allowed for {destFileName}";
                                context.ContextWrite(err);
                                throw new Exception(err);
                            }
                        }
                        using (var sw = new MemoryStream()) {
                            var lineCount = 0;
                            string headers = "";
                            using (var stream = entry.Open()) {
                                lineCount = ConvertStreamToTallAmi(stream, sw);
                            }
                            if (lineCount > 0) {
                                context.Add2Counter("unzipCvtRecCnt", lineCount);
                            }
                            using (ZipArchive narchive = ZipFile.Open(tallZipFileName, ZipArchiveMode.Update)) {
                                sw.Flush();
                                sw.Position = 0;
                                ZipArchiveEntry nentry = narchive.CreateEntry(tallFileName, CompressionLevel.Fastest);
                                using (Stream entryStream = nentry.Open()) {
                                    sw.CopyTo(entryStream);
                                }
                                //narchive.CreateEntryFromFile(tallFileTempName, tallFileName);
                                context.ContextWrite("ZipManager.Convert2TallAmi", $"generated {tallZipFileName} from {tallFileName}");
                            }
                            //FileInfo fit = new FileInfo(tallFileTempName);
                            FileInfo fiz = new FileInfo(tallZipFileName);
                            context.Add2Counter("unzipCvtRecTallSize", sw.Length);
                            context.Add2Counter("unzipCvtRecZipSize", fiz.Length);
                            context.ContextWrite("ZipManager.Convert2TallAmi", $"zipsize {fiz.Length} tallsize {sw.Length}");
                            //if (!retainTallSrc) {
                            //    File.Delete(tallFileTempName);
                            //    context.IncrementCounter("unzipCvtRecDelSrcCnt");
                            //}
                        }
                    }
                }
                string moveToPath = Path.Combine(Path.GetDirectoryName(zipFile), "extracted");
                if (!Directory.Exists(moveToPath)) {
                    Directory.CreateDirectory(moveToPath);
                    context.IncrementCounter("unzipCvtMove");
                }
                string destFile = Path.Combine(moveToPath, Path.GetFileName(zipFile));
                if (File.Exists(destFile) && pa.GetControlVal("overwriteDrop", "no").Equals("yes")) {
                    File.Delete(destFile);
                    context.IncrementCounter("unzipCvtDelete");
                    context.ContextWrite("ZipManager.Convert2TallAmi done overwrite", destFile);
                }
                File.Move(zipFile, destFile);
            }
            catch (Exception exc) {
                context.IncrementCounter("errors");
                context.ContextWrite($"ZipManager.Convert2TallAmi", $" exception {exc.Message}, {exc.InnerException?.Message}");
            }

        }

        /// <summary>
        /// Convert ami stream data to tall format
        /// </summary>
        /// <param name="stream"></param>
        /// <param name="sw"></param>
        /// <returns></returns>
        public int ConvertStreamToTallAmi(Stream stream, MemoryStream sw) {

            string headers = "";
            var lineCount = 0;
            string useHeaders = pa.GetControlVal("useHeaders", @"ClientId,MeterId,AccountNumber,ServicePointId,CommodityId,UOMId,VolumeFactor,Direction,TOUID,ProjectedReadDate,TimeStamp,Timezone,IntValue0000");
            string csvColTemp = pa.GetControlVal("csvColTemp", @"-,-,-,-,-,-,-,-,0,-,-,-,-");
            string csvColMap = pa.GetControlVal("csvColMap", @"0,1,2,3,4,5,6,7,-1,8,9,10,-1");
            int csvWideTimestampCol = pa.GetControlVal("csvTallTimestampCol", 9);
            int csvTallTimestampCol = pa.GetControlVal("csvTallTimestampCol", 10);
            int csvWideValCol = pa.GetControlVal("csvTallValCol", 11);
            int csvTallValCol = pa.GetControlVal("csvTallValCol", 12);
            string tsTimeZoneInfo = pa.GetControlVal("tsTimeZoneInfo", "Z");
            string standardTimeFormatBase = string.Format(StandardTimeFormatBaseTemp, tsTimeZoneInfo);
            string[] csvColArray = csvColMap.Split(',');
            int[] csvIntMap = new int[csvColArray.Length];
            for (int i = 0; i < csvIntMap.Length; i++) {
                csvIntMap[i] = int.Parse(csvColArray[i]);
            }

            bool haveHeaders = pa.GetControlVal("haveCsvHeaders", "yes").Equals("yes");
            int procLimit = pa.GetControlVal("procLimit", 100);
            bool stripCsvQuotes = string.Equals(pa.GetControlVal("stripCsvQ", "no"), "yes");
            bool addCsvQuotes = string.Equals(pa.GetControlVal("addCsvQuotes", "yes"), "yes");
            //string destTimeZoneString = pa.GetControlVal("destTimeZoneString", "Central Standard Time");
            string destTimeZoneString = pa.GetControlVal("destTimeZoneString", "Central Standard Time");

            using (StreamReader sr = new StreamReader(stream)) {
                if (haveHeaders) {
                    if (string.IsNullOrEmpty(headers)) {
                        headers = sr.ReadLine(); // remove and save headers
                        if (!string.IsNullOrEmpty(useHeaders)) {
                            byte[] bytes = Encoding.UTF8.GetBytes(useHeaders + "\n");
                            sw.Write(bytes, 0, bytes.Length);
                            //sw.WriteLine(useHeaders);
                        }
                        else {
                            byte[] bytes = Encoding.UTF8.GetBytes(headers + "\n");
                            sw.Write(bytes, 0, bytes.Length);
                            //sw.WriteLine(headers);
                        }
                    }
                    else {
                        sr.ReadLine(); // remove headers
                    }
                }
                while (!sr.EndOfStream) {
                    string rec = sr.ReadLine();
                    //sw.WriteLine($" will expand {rec}");
                    if (stripCsvQuotes) {
                        rec = rec?.Replace("\"", "");
                    }
                    string[] cols = rec?.Split(',');

                    TimeZoneInfo tzi = TimeZoneInfo.FindSystemTimeZoneById(destTimeZoneString);
                    DateTime dtSttq = DateTime.Parse(cols?[csvWideTimestampCol]);
                    DateTimeOffset dt = new DateTimeOffset(dtSttq.Year, dtSttq.Month, dtSttq.Day, dtSttq.Hour, dtSttq.Minute, dtSttq.Second, tzi.GetUtcOffset(dtSttq));
                    TimeSpan tsZero = new TimeSpan();

                    int startSrcValsCol = csvWideValCol;
                    for (int i = 0; i < 24; i++) {
                        int dtOfsettCode = GetDTShift(dt);
                        if (dtOfsettCode < -1) {
                            dt = dt.AddHours(1);
                            continue;
                        }
                        TimeSpan tzOffset = tzi.GetUtcOffset(dt);
                        DateTimeOffset dtos = dt.AddHours(-tzOffset.Hours);
                        DateTimeOffset dtoUtc = new DateTimeOffset(dtos.Year, dtos.Month, dtos.Day, dtos.Hour, dtos.Minute, dtos.Second, tsZero);

                        string[] newCsvVals = csvColTemp.Split(',');
                        for (int cm = 0; cm < newCsvVals.Length; cm++) {
                            if (csvIntMap[cm] >= 0) {
                                newCsvVals[cm] = cols?[csvIntMap[cm]];
                            }
                        }

                        newCsvVals[csvTallTimestampCol] = dtoUtc.ToString(standardTimeFormatBase);
                        newCsvVals[csvTallValCol] = cols?[startSrcValsCol + i];
                        if (addCsvQuotes) {
                            for (int cm = 0; cm < newCsvVals.Length; cm++) {
                                //newCsvVals[cm] = $"\"{newCsvVals[cm]}\"";
                                //newCsvVals[cm] = string.Format("\"{0}\"", newCsvVals[cm]);
                                newCsvVals[cm] =
                                    '\"' + newCsvVals[cm] +
                                    '\"'; // significantly faster than others 1.5 (1.0 for none, 2.0 for interp)
                            }
                        }

                        string newCsv =
                            newCsvVals.Aggregate((current, next) => current + ',' + next);
                        byte[] bytes = Encoding.UTF8.GetBytes(newCsv + "\n");
                        sw.Write(bytes, 0, bytes.Length);
                        //sw.WriteLine(newCsv);
                        dt = dt.AddHours(1);
                    }
                    context.Add2Counter("unzip2TallRecCnt", 24);

                    lineCount++;
                    if (lineCount >= procLimit) {
                        break;
                    }
                }
            }

            return lineCount;
        }

        public int TestFileExtract(string zipFile, string inDropLocation, int dropCounter) {


            string[] dropLocations = inDropLocation.Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries);

            try {

                using (ZipArchive archive = ZipFile.OpenRead(zipFile)) {
                    foreach (ZipArchiveEntry entry in archive.Entries) {
                        if (dropCounter >= dropLocations.Length) {
                            dropCounter = 0;
                        }
                        string dropLocation = dropLocations[dropCounter++];
                        string tmpLocation = Path.Combine(dropLocation, "zipTmp");
                        tmpLocation = pa.GetControlVal("zipTmpLocation", tmpLocation); // maybe override
                        if (!Directory.Exists(tmpLocation)) {
                            Directory.CreateDirectory(tmpLocation);
                            context.ContextWrite("ZipManager.ExtractAndDrop.TestFileExtract", $" create tmp location {tmpLocation} ");
                            context.IncrementCounter("zipTmpCreate");
                        }
                        if (!Directory.Exists(dropLocation)) {
                            Directory.CreateDirectory(dropLocation);
                            context.ContextWrite("ZipManager.ExtractAndDrop.TestFileExtract", $" create drop location {dropLocation} ");
                            context.IncrementCounter("zipDropCreate");
                        }

                        string destFileName = Path.Combine(dropLocation, entry.FullName);
                        if (File.Exists(destFileName) && pa.GetControlVal("overwriteDrop", "no").Equals("yes")) {
                            File.Delete(destFileName);
                            context.IncrementCounter("deleteUnzipDest");
                            context.ContextWrite("ziptest extract overwrite", destFileName);
                        }
                        var lineCount = 0;
                        if (pa.GetControlVal("cntUnzipRecs", "yes").Equals("yes")) {
                            DateTime dtSttCnt = DateTime.Now;
                            using (var stream = entry.Open()) {
                                using (StreamReader sr = new StreamReader(stream)) {
                                    while (!sr.EndOfStream) {
                                        sr.ReadLine();
                                        lineCount++;
                                    }
                                }
                            }
                            DateTime dtStPCnt = DateTime.Now;
                            context.Add2Counter("zipCntTime", (long)dtStPCnt.Subtract(dtSttCnt).TotalMilliseconds);
                        }
                        if (lineCount > 0) {
                            context.Add2Counter("unzipRecCnt", lineCount - 1);
                        }
                        if (pa.GetControlVal("useZipTmp", "yes").Equals("yes")) {
                            string tmpFile = Path.Combine(tmpLocation, "unzip.tmp");
                            DateTime dtSttUnzip = DateTime.Now;
                            entry.ExtractToFile(tmpFile, true); // watcher tries to grab viles while unzipping, so shorten window so can keep a short timeout
                            DateTime dtStpUnzip = DateTime.Now;
                            File.Move(tmpFile, destFileName);
                            DateTime dtStpMove = DateTime.Now;
                            context.Add2Counter("zipUnzipTime", (long)dtStpUnzip.Subtract(dtSttUnzip).TotalMilliseconds);
                            context.Add2Counter("zipMoveTime", (long)dtStpMove.Subtract(dtStpUnzip).TotalMilliseconds);
                            string info = $" w/ tmp extract {lineCount} lines from '{entry.Name}' to '{destFileName}'";
                            context.ContextWrite("ZipManager.TestFileExtract", info);
                            Console.WriteLine($"ZipManager.TestFileExtract {info}");
                        }
                        else {
                            DateTime dtSttUnzip = DateTime.Now;
                            entry.ExtractToFile(destFileName, true);
                            DateTime dtStpUnzip = DateTime.Now;
                            context.Add2Counter("zipUnzipTime", (long)dtStpUnzip.Subtract(dtSttUnzip).TotalMilliseconds);
                            string info = $" w/o tmp extract {lineCount} lines from '{entry.Name}' to '{destFileName}'";
                            context.ContextWrite("ZipManager.TestFileExtract", info);
                        }
                    }
                }
                if (pa.GetControlVal("moveAfterZip", "yes").Equals("yes")) {
                    string moveToPath = Path.Combine(Path.GetDirectoryName(zipFile), "extracted");
                    moveToPath = pa.GetControlVal("zipRelocatePath", moveToPath);
                    if (!Directory.Exists(moveToPath)) {
                        Directory.CreateDirectory(moveToPath);
                        context.IncrementCounter("createUnzipMove");
                        context.ContextWrite("createUnzipMove", moveToPath);
                    }
                    string destFile = Path.Combine(moveToPath, Path.GetFileName(zipFile));
                    if (File.Exists(destFile) && pa.GetControlVal("overwriteDrop", "no").Equals("yes")) {
                        File.Delete(destFile);
                        context.IncrementCounter("deleteUnzipMove");
                        context.ContextWrite("ziptest done overwrite", destFile);
                    }
                    File.Move(zipFile, destFile);
                    context.ContextWrite("createUnzipMove", $" from '{zipFile}' to '{destFile}'");
                }
            }
            catch (Exception exc) {
                context.IncrementCounter("errors");
                context.ContextWrite($"ZipManager.TestFileExtract", $" exception {exc.Message}, {exc.InnerException?.Message} src='{zipFile}', topath='{inDropLocation}'");
            }
            return dropCounter;
        }

        private void Read2Memory(string path) {
            using (var file = File.OpenRead(path))
            using (var zip = new ZipArchive(file, ZipArchiveMode.Read)) {
                foreach (var entry in zip.Entries) {
                    using (StreamReader sr = new StreamReader(entry.Open())) {
                        while (!sr.EndOfStream) {

                        }
                    }
                }
            }
        }


    }


    class ToolingRequest {
        //private string defToolRoot = "https://localhost/DevWt1/api/Data/consumption_by_meter/prod/4/validate?allowFilter=1&maxRows=300&procLimit=100&sortCol=ami_time_stamp&procResp=1&outputTag=test1&userPath=anon2&contextRecs=25&contextRet=1&expectRows=1&matchtype=get";  // localhost with standalone service.
        private string defToolRoot = "https://localhost/DevWt1/api/Data/consumption_by_meter/prod/4/validate?allowFilter=1&sortCol=ami_time_stamp&procResp=1&outputTag=test1&userPath=anon2&contextRecs=25&contextRet=1&expectRows=1&matchtype=get";  // localhost with standalone service.
        public const string StandardTimeFormat = "yyyy-MM-ddTHH:mm:ss.fffzz00";



        //private string _amiReq ="/api/current/ami/{0}?dtStt={1}&dtStp={2}&bucketSizeHours={3}&wantMetadata=0&wantCassinfo=0&zmetaQualName=estInd&metaQualVal=yes&requireLogForce=no&seriesSort=desc&seriesValFmt=F3&logCassQueries=1&retdatapa=yes&zspecCassandraKeyspace=not-there&zallowcachecas=no&zresetcache=yes";

        //private const string CfgReq = "/api/current/config?custom=nyes&oversecj=2216";

        private void InitWebCall(WebClient client) {
            client.Headers["X-BIO-DiagUser"] = "AclaraDiag";
            client.Headers["Accept"] = "application/json";
            client.Headers["Content-Type"] = "text/plain";
        }

        public async Task<string> SubmitBulkDataRequests(ProcessingArgs paIn, ContextData context) {

            Task<string> resTask = null;
            string requestBody =

                "#C where client_id={0} and meter_id = '{1}' and ami_time_stamp>'2015-01-01 00:00:00' and ami_time_stamp<'2017-07-01 00:00:00'\n" +
                @"#F C:\_Test\uil_prod\files\uil_distinct_meters_20170529.csv.000.input.csv" + "\n" +
                "#P csv 1 meter_id,ami_time_stamp,consumption 1,DT,,yyyy-MM-ddTHH:mm:ss.fffzz00;\n";

            requestBody = paIn.GetControlVal("bulkReqBody", requestBody);


            string bodyOverrideFile = paIn.GetControlVal("bulkReqBodyFile", @".\data\BulkCassExtractBody.txt");
            if (!string.IsNullOrEmpty(bodyOverrideFile) && File.Exists(bodyOverrideFile)) {
                try {
                    using (StreamReader sr = new StreamReader(bodyOverrideFile)) {
                        requestBody = sr.ReadToEnd();
                        context.ContextWrite($"using query body file {bodyOverrideFile} is \n{requestBody}\n");
                    }
                }
                catch (Exception exc) {
                    context.ContextWrite(
                        $"exception getting body file {bodyOverrideFile} is {exc.Message}, inner={exc.InnerException?.Message}");
                }
            }


            //reqMsg.Headers.Add("Content-Type","text/plain");
            //string srcFileName = @"C:\_Test\uil_prod\files\uil_distinct_meters_20170529.csv.000.input.csv";
            string srcFileName = paIn.GetControlVal("srcfile", @"C:\_Test\uil_prod\files\uil_distinct_meters_20170529.csv.000.input.csv");
            System.Collections.Concurrent.ConcurrentBag<string> srcFiles = new System.Collections.Concurrent.ConcurrentBag<string>(new[] { srcFileName });  // start off assuming a file.

            FileAttributes attr = File.GetAttributes(srcFileName);
            if (attr.HasFlag(FileAttributes.Directory)) { // directory == get list of CSV files
                string consolidateFilter = paIn.GetControlVal("consFilter", "*.csv");
                IEnumerable<string> fnames = Directory.EnumerateFiles(srcFileName, consolidateFilter);
                srcFiles = new System.Collections.Concurrent.ConcurrentBag<string>();
                foreach (string fname in fnames) {
                    srcFiles.Add(fname);
                }
            }
            else {
                srcFiles.Add(srcFileName);
            }

            bool doParallel = paIn.GetControlVal("doParallelVal", "no").Equals("yes");
            if (doParallel) {
                Parallel.ForEach(srcFiles, srcFil => {
                    ProcessingArgs pa = new ProcessingArgs(paIn);
                    pa["useRefFile"] = srcFil;
                    Thread.Sleep(1000); // let things stabilize -- seems to be a little race somewhere.
                    resTask = PerformBulkRequestSegment(pa, srcFil, requestBody);
                });
            }
            else {
                foreach (string srcFil in srcFiles) {
                    ProcessingArgs pa = new ProcessingArgs(paIn);
                    //Thread.Sleep(1000); // let things stabilize -- seems to be a little race somewhere.
                    resTask = PerformBulkRequestSegment(pa, srcFil, requestBody);
                }
            }
            return resTask.Result;
        }

        private static string _consolePadlock = "ProcDataCachePadlock";


        private async Task<string> PerformBulkRequestSegment(ProcessingArgs pa, string srcFile, string requestBodyStg) {
            HttpRequestMessage reqMsg = new HttpRequestMessage();
            reqMsg.Headers.Add("X-BIO-DiagUser", "AclaraDiag");
            reqMsg.Headers.Add("Accept", "*/*");
            string[] requestBody = requestBodyStg.Split('\n');
            for (int i = 0; i < requestBody.Length; i++) {
                requestBody[i] = requestBody[i].Trim();
            }

            if (requestBody.Length > 1) {
                requestBody[1] = $"#F {srcFile}";
            }

            string srcDir = Path.GetDirectoryName(srcFile);

            string destFilename = srcFile + ".results.csv";
            if (File.Exists(destFilename)) {
                bool overwriteFiles = pa.GetControlVal("overwriteFiles", 0) > 0;
                if (overwriteFiles) {
                    Console.WriteLine($" SubmitBulkDataRequests '{destFilename}' exists, overwriteFiles={overwriteFiles}");
                    File.Delete(destFilename);
                }
                else {
                    string cTxt = $" ***** SubmitBulkDataRequests '{destFilename}' exists, overwriteFiles={overwriteFiles} so cancelling. ";
                    Console.WriteLine(cTxt);
                    return cTxt;
                }
            }

            string reqBody = requestBody.Aggregate((current, next) => current + Environment.NewLine + next);
            reqMsg.Content = new StringContent(reqBody);
            string excCmd = pa.GetControlVal("excCmd", "");
            string validateRestRoot = pa.GetControlVal("validateRestRoot", "");
            if (!string.IsNullOrEmpty(excCmd) && !string.IsNullOrEmpty(validateRestRoot)) {
                string validateRestPath = pa.GetControlVal("validateRestPath", "");
                string[] reqPars = excCmd.Split(new[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
                string reqParamsStg = reqPars.Aggregate((current, next) => current + "&" + next);
                string uriPath = string.Format(validateRestPath, pa.GetControlVal("reqVsrc", "consumption_by_meter"), pa.GetControlVal("aclenv", "perfd"), pa.GetControlVal("reqVval", "88"));
                string uriStg = $"{validateRestRoot}/{uriPath}?{reqParamsStg}";
                reqMsg.RequestUri = new Uri(uriStg);
            }
            else {
                reqMsg.RequestUri = new Uri(defToolRoot);
            }
            string useRefFile = pa.GetControlVal("useRefFile",""); // because of way command is passed down already filled in, we loose the parallel element
            HttpResponseMessage respMsg = new HttpResponseMessage();

            ContextData.InitContextStrings(pa["reqVsrc"], pa["aclenv"], pa["reqVval"], pa, reqMsg);
            pa["alreadyExtractedVars"] = "no";
            ProcessingArgs.AccumulateRequestPars(reqMsg, pa["reqVsrc"], pa["aclenv"], pa["reqVval"], pa);
            pa["bodyOverride"] = "yes";
            pa["useRefFile"] = useRefFile;
            DateTime dtStt = DateTime.Now;
            ContextData contextData = new ContextData(pa);
            string result = "";
            try {

                if (pa.GetControlVal("showdetail", 0) > 0) {
                    contextData.ContextWriteSeg("Tooling request", true, dtStt);
                    contextData.ContextWriteBulk("config", pa.AsJson());
                }
                string doRemoteValidate = pa.GetControlVal("doRemoteValidate", "no");
                if (doRemoteValidate.Equals("yes")) {  // TODO: do this later
                    AnalysisUtils.AnalysisValidate(contextData, pa, reqMsg, respMsg);
                }
                else {
                    AnalysisUtils.AnalysisValidate(contextData, pa, reqMsg, respMsg);
                }

                Byte[] byteArray = await respMsg.Content.ReadAsByteArrayAsync();
                result = Encoding.UTF8.GetString(byteArray);
                string ctxOutputName = contextData.MainStream.FileName;
                bool overwriteFiles = pa.GetControlVal("overwriteFiles", 0) > 0;

                if (pa.GetControlVal("doMisFilesCopies", "yes").Equals("yes")) {
                    string csvP0Name = ctxOutputName + "_raw___P0_csv.csv";
                    csvP0Name = pa.GetControlVal("p0filename", csvP0Name);
                    if (File.Exists(destFilename)) {
                        Console.WriteLine(
                            $" SubmitBulkDataRequests '{destFilename}' exists, overwriteFiles={overwriteFiles}");
                        if (overwriteFiles) {
                            File.Delete(destFilename);
                        }
                    }
                    if (pa.GetControlVal("showdetail", 0) > 0) {
                        contextData.ContextWriteSeg("Tooling request", false, dtStt);
                    }
                    File.Copy(csvP0Name, destFilename);

                    string srcDirName = Path.GetDirectoryName(ctxOutputName);
                    string logDirName = Path.GetFileName(ctxOutputName);
                    string searchPat = $"{logDirName}*_pproc.csv";
                    IEnumerable<string> fnames = Directory.EnumerateFiles(srcDirName, searchPat, SearchOption.AllDirectories);
                    //List<String> srcFiles = new List<string>();
                    foreach (string fname in fnames) {
                        //srcFiles.Add(fname);
                        contextData.ContextWrite("found file name", fname);
                        string tName = Path.GetFileName(fname);
                        string tFrag = tName.Substring(logDirName.Length);
                        string newFileName = srcFile + tFrag;
                        contextData.ContextWrite("will use", newFileName);
                        if (overwriteFiles) {
                            File.Delete(newFileName);
                        }
                        File.Copy(fname, newFileName);
                    }
                }
            }
            catch (Exception exc) {
                contextData.ContextWrite($" exception in Tooling request  {exc.Message}; inner={exc.InnerException?.Message}");
            }
            string ctxFile = contextData.MainStream.FileName;
            contextData.End();

            lock (_consolePadlock) {
                Console.WriteLine($" SubmitBulkDataRequests response\n{result}\n");
            }
            StringBuilder sb = new StringBuilder();
            sb.Append(ctxFile);
            sb.AppendLine();
            sb.Append(result);

            return sb.ToString();
        }
    }



    public class MeterFilterMaintenance {
        public Dictionary<string, DataRow> NewMetersRows = new Dictionary<string, DataRow>();
        public Dictionary<string, DataRow> AllMetersRows = new Dictionary<string, DataRow>();

        public List<string> NewMetersHash = new List<string>();
        public List<string> AllMetersHash = new List<string>();

    }

    public class MeterFilter {
        private DataTable _subscriptionsTable;
        private string _subscriptionsFileName;
        private ContextData context;

        private MeterFilterMaintenance mfm = new MeterFilterMaintenance();

        public int SubscriptionCount(string selVal = "all") {
            switch (selVal) {
                case "new":
                    return mfm.NewMetersHash.Count;
                    break;
                case "old":
                    return mfm.AllMetersHash.Count - mfm.NewMetersHash.Count;
                    break;
                case "all":
                    return mfm.AllMetersHash.Count;
                    break;
            }
            return mfm.NewMetersHash.Count;
        }

        public string SubscriptionSummary() {
            return $" Subscriptions: new={SubscriptionCount("new")}, old={SubscriptionCount("old")}, all={SubscriptionCount("all")} ";
        }

        public void InitializeFiltering(ContextData inContext, bool newFilterIngestion = false) {
            context = inContext;
            ProcessingArgs pa = context.CtxArgs;
            XmlSerializer xmlSerializer = XmlSerializer.FromTypes(new[] {typeof(DataTable)})[0];

            //gbcmeterdatafile =  doModRows = true 

            _subscriptionsFileName = pa.GetControlVal("gbcmeterdatafile", "C:/junk/t122/_cjh_gbcm2.dat");
            if (File.Exists(_subscriptionsFileName)) {
                var fileStream = new FileStream(_subscriptionsFileName, FileMode.Open);
                try {
                    DataTable dataTable;
                    using (XmlTextReader reader = new XmlTextReader(fileStream)) {
                        dataTable = (DataTable) xmlSerializer.Deserialize(reader);
                    }
                    fileStream.Close();
                    context.ContextWrite("InitializeFiltering", $"subsFile {_subscriptionsFileName} ");
                    DataView dv = dataTable.DefaultView;
                    dv.Sort = "MeterId,AccountId asc";
                    _subscriptionsTable = dv.ToTable();
                    CreateMetersHashs();

                    CheckTableThings(pa.GetControlVal("newFilterIngestion", 1) == 1);
                }
                catch (Exception ex) {
                    context.ContextWrite("**ERROR** InitializeFiltering",
                        $"InitializeFiltering {_subscriptionsFileName}  Exception: {ex.Message} InnerException: {ex.InnerException?.Message}");
                }
                finally {
                }
            }
        }

        /// <summary>
        /// Process for initializing new records during daily ingestion which occurs *after* historical ingestion
        ///Historical import sets historical date but does *not* flip new->existing
        ///This allows all historical data to flow in.
        ///Daily import checks each record and for those with a recent historical import and still new flips new->active.
        ///This resets the record for the next historical import (avoiding multiple imports) while allowing the daily data in.
        /// </summary>
        /// <param name="newFilterIngestion"></param>
        private void CheckTableThings(bool newFilterIngestion = false) {
            ProcessingArgs pa = context.CtxArgs;
            Random rand = new Random();
            IEnumerable<DataRow> rows = _subscriptionsTable.AsEnumerable();
            string csvOutFile = _subscriptionsFileName + ".consout.csv";
            csvOutFile = pa.GetControlVal("csvOutFile", csvOutFile);
            bool doModRows = pa.GetControlVal("doModRows", "false").ToLower().Equals("true");
            bool moddedTable = false;
            using (StreamWriter sw = new StreamWriter(csvOutFile)) {
                foreach (DataRow row in rows) {
                    string newCsv = (string) (row.ItemArray.Aggregate((current, next) => current.ToString() + ',' + next.ToString()));
                    sw.WriteLine(newCsv.Trim(','));
                    if (!newFilterIngestion) {
                        bool stillNew = row["Status"].Equals("new");
                        if (stillNew) {
                            DateTime histDate = (DateTime)row["HistoricalLoadDate"];
                            bool haveHistorical = DateTime.Now.Subtract(histDate).TotalHours < 10000;
                            if (haveHistorical) {  // and stil new means reset before next historical run
                                row["Status"] = "existing";
                                newCsv = (string)(row.ItemArray.Aggregate((current, next) => current.ToString() + ',' + next.ToString()));
                                sw.WriteLine(newCsv.Trim(',') + " ***reset from new***");
                                moddedTable = true;
                            }
                        }
                    }

                    if (doModRows) {
                        if (rand.Next(100) > 50) {
                            row["status"] = "changed";
                            row["Updated"] = DateTime.Now;
                        }
                        else {
                            row["status"] = "not_changed";
                        }
                        moddedTable = true;
                    }
                }
            }
            if (moddedTable) {
                SaveSubsTable();
            }
        }

        public void SaveSubsTable() {
            ProcessingArgs pa = context.CtxArgs;
            XmlSerializer xmlSerializer = XmlSerializer.FromTypes(new[] {typeof(DataTable)})[0];
            string modifiedDataFileName = _subscriptionsFileName + ".mod.dat";
            modifiedDataFileName = pa.GetControlVal("modDataFileName", modifiedDataFileName);
            if (File.Exists(modifiedDataFileName)) {
                File.Delete(modifiedDataFileName);
            }
            FileStream fileStream = new FileStream(modifiedDataFileName, FileMode.OpenOrCreate);
            try {
                using (XmlTextWriter writer = new XmlTextWriter(fileStream, new UTF8Encoding(false))) {
                    writer.Formatting = Formatting.Indented;
                    xmlSerializer.Serialize(writer, _subscriptionsTable);
                    writer.Flush();
                    context.ContextWrite("SaveSubsTable", $"File {modifiedDataFileName} is written");
                }
            }
            catch (Exception ex) {
                context.ContextWrite("Writefile - ERROR",
                    $"Fail to write file {modifiedDataFileName}  Exception: {ex.Message} InnerException: {ex.InnerException?.Message}");
            }
            finally {
                fileStream.Close();
            }
        }


        void CreateMetersHashs() {
            GetNewMetersHashs(true,"Status='new' ");
            GetNewMetersHashs();
        }

        /// <summary>
        /// create hash list of items to be extracted
        /// </summary>
        /// <returns></returns>
        void GetNewMetersHashs(bool doNew = false, string query = "") {
            DateTime dtStt = DateTime.Now;
            context.ContextWriteSeg($"GetNewMetersHashs '{query}'",true, dtStt);
            DataRow[] resRows = _subscriptionsTable.Select(query);
            List<string> useList = doNew ? mfm.NewMetersHash : mfm.AllMetersHash;
            Dictionary<string, DataRow> useRowCache = doNew ? mfm.NewMetersRows : mfm.AllMetersRows;
            foreach (DataRow resRow in resRows) {
                string rowHash = resRow["ClientId"] + ","+ resRow["MeterId"] + "," + resRow["AccountId"];
                useList.Add(rowHash);
                useRowCache.Add(rowHash,resRow);
            }
            //Console.WriteLine($"\n '{query}' hashs");
            foreach (string s in useList) {
                context.ContextWrite("filterhash",s);
                //Console.WriteLine(s);
            }
            context.ContextWriteSeg($"GetNewMetersHashs '{query}'", false, dtStt);
        }

        private static readonly int[] FilterProps = {1,2};
        private const int numFilterSeps = 3;
        private const char sepChar = ',';

        /// <summary>
        /// check if props refer to something that has not yet been processed
        /// TBD: this is dump for a few props and needs to be generalized if it is to be used outside of short-short
        /// </summary>
        /// <param name="props"></param>
        /// <returns></returns>
        public string Has(string csvLine, bool inNew = true) {
            int hashEnd = 0;
            int sepCount = 0;
            for (; hashEnd < csvLine.Length && sepCount < numFilterSeps; hashEnd++) {
                if (csvLine[hashEnd].Equals(sepChar)) {
                    sepCount++;
                }
            }
            hashEnd -= 1;
            if (hashEnd < 1) return "";
            string hash = csvLine.Substring(0, hashEnd);
            if (inNew) {
                if (mfm.NewMetersHash.Contains(hash)) {
                    return hash;
                }
            }
            else {
                if (mfm.AllMetersHash.Contains(hash)) {
                    return hash;
                }
            }
            return "";
        }

        public int Increment(string hash, bool inNew = true, bool isGood = true) {
            int rVal = -1;
            if (!string.IsNullOrEmpty(hash)) {
                if (inNew) {
                    if (mfm.NewMetersHash.Contains(hash)) {
                        if (isGood) {
                            rVal = (int)mfm.NewMetersRows[hash]["IngestedReadingsCount"];
                            rVal += 1;
                            mfm.NewMetersRows[hash]["IngestedReadingsCount"] = rVal;
                            mfm.NewMetersRows[hash]["HistoricalLoadDate"] = DateTime.Now;
                            //mfm.NewMetersRows[hash]["Status"] = "existing";
                        }
                        else {
                            rVal = (int)mfm.NewMetersRows[hash]["RejectedReadingsCount"];
                            rVal += 1;
                            mfm.NewMetersRows[hash]["RejectedReadingsCount"] = rVal;
                        }
                    }
                }
                else {
                    if (mfm.AllMetersHash.Contains(hash)) {
                        if (isGood) {
                            rVal = (int)mfm.AllMetersRows[hash]["IngestedReadingsCount"];
                            rVal += 1;
                            mfm.AllMetersRows[hash]["IngestedReadingsCount"] = rVal;
                            mfm.AllMetersRows[hash]["Updated"] = DateTime.Now;
                        }
                        else {
                            rVal = (int)mfm.AllMetersRows[hash]["RejectedReadingsCount"];
                            rVal += 1;
                            mfm.AllMetersRows[hash]["RejectedReadingsCount"] = rVal;
                        }
                    }
                }
            }
            return rVal;
        }
        public int Error(string hash, string errMsg, bool inNew = true) {
            int rVal = -1;

            if (!string.IsNullOrEmpty(hash)) {
                if (inNew) {
                    if (mfm.NewMetersHash.Contains(hash)) {
                        rVal = (int)mfm.NewMetersRows[hash]["ErrorCount"];
                        rVal += 1;
                        mfm.NewMetersRows[hash]["ErrorCount"] = rVal;
                    }
                }
                else {
                    if (mfm.AllMetersHash.Contains(hash)) {
                        rVal = (int)mfm.AllMetersRows[hash]["ErrorCount"];
                        rVal += 1;
                        mfm.AllMetersRows[hash]["ErrorCount"] = rVal;
                    }
                }
            }
            return rVal;
        }





    }




}
