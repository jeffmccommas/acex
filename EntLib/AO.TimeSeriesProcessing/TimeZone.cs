﻿using System;
using System.Globalization;
using System.Linq;

namespace AO.TimeSeriesProcessing
{
    public static class TimeZone
    {
        public static DaylightTime GetDaylightChanges(TimeZoneInfo currentTimezone, int year)
        {
            var currentRules =
                currentTimezone.GetAdjustmentRules().FirstOrDefault(rule =>
                    rule.DateStart <= DateTime.Today &&
                    rule.DateEnd >= DateTime.Today);

            if (currentRules != null)
            {
                var daylightStart =
                    GetTransitionDate(currentRules.DaylightTransitionStart, year);

                var daylightEnd =
                    GetTransitionDate(currentRules.DaylightTransitionEnd, year);

                return new DaylightTime(daylightStart, daylightEnd,
                    currentRules.DaylightDelta);
            }

            return null;
        }

        public static string GetTimeZoneDescription(string timezone)
        {
            string timezoneDes;

            switch (timezone.ToUpper())
            {
                case "ADT":
                case "AST":
                    timezoneDes = "Atlantic Standard Time";
                    break;
                case "AKDT":
                case "AKST":
                    timezoneDes = "Alaskan Standard Time";
                    break;
                case "CDT":
                case "CST":
                    timezoneDes = "Central Standard Time";
                    break;
                case "EDT":
                case "EST":
                    timezoneDes = "Eastern Standard Time";
                    break;
                case "GMT":
                    timezoneDes = "Greenwich Standard Time";
                    break;
                case "HADT":
                case "HAST":
                    timezoneDes = "Hawaiian Standard Time";
                    break;
                case "MDT":
                case "MST":
                    timezoneDes = "Mountain Standard Time";
                    break;
                case "PDT":
                case "PST":
                    timezoneDes = "Pacific Standard Time";
                    break;
                default:
                    timezoneDes = "Central Standard Time";
                    break;

            }

            return timezoneDes;
        }

        private static DateTime GetTransitionDate(TimeZoneInfo.TransitionTime transition,
            int year)
        {
            return (transition.IsFixedDateRule)
                ? new DateTime(year, transition.Month, transition.Day,
                    transition.TimeOfDay.Hour, transition.TimeOfDay.Minute,
                    transition.TimeOfDay.Second)
                : GetNonFixedTransitionDate(transition, year);
        }

        private static DateTime GetNonFixedTransitionDate(
            TimeZoneInfo.TransitionTime transition, int year)
        {
            var calendar = CultureInfo.CurrentCulture.Calendar;
            int startOfWeek = transition.Week * 7 - 6;
            int firstDayOfWeek = (int)calendar.GetDayOfWeek(new DateTime(year,
                transition.Month, 1));

            int changeDayOfWeek = (int)transition.DayOfWeek;

            int transitionDay = (firstDayOfWeek <= changeDayOfWeek)
                ? startOfWeek + (changeDayOfWeek - firstDayOfWeek)
                : startOfWeek + (7 - firstDayOfWeek + changeDayOfWeek);

            if (transitionDay > calendar.GetDaysInMonth(year, transition.Month))
                transitionDay -= 7;

            return new DateTime(year, transition.Month, transitionDay,
                transition.TimeOfDay.Hour, transition.TimeOfDay.Minute,
                transition.TimeOfDay.Second);
        }
    }
}