﻿using System;
using System.Collections.Specialized;
using System.Data;
using System.IO;
using System.Text;
using System.Xml;
using System.Xml.Serialization;

namespace AO.TimeSeriesProcessing {


    class XmlUtils1 {

    }



    public class XmlDocUtils {
        public static XmlDocument GetXMLExceptionDocument(string docType, NameValueCollection pars, Exception exc = null) {
            XmlDocument xDoc = new XmlDocument();
            XmlNode topNode = xDoc.CreateElement("Errors");
            xDoc.AppendChild(topNode);
            XmlNode bodyNode = xDoc.CreateElement(docType?.Replace(" ", "_") ?? "unknown");
            topNode.AppendChild(bodyNode);
            if (pars != null) {
                string[] keys = pars.AllKeys;
                foreach (string stg in keys) {
                    XmlNode xn = xDoc.CreateElement(stg);
                    xn.InnerText = pars[stg] ?? "NULL";
                    bodyNode.AppendChild(xn);
                }
            }
            if (exc != null) {
                AddExcInfo(exc, xDoc);
            }
            return xDoc;
        }

        /// <summary>
        /// Create an XML document from a bunch of strings. Nodes wil be par0, par1, ... parN.
        /// </summary>
        /// <param name="docType">root node name [unknown].  Might clean up to make them xml path friendly</param>
        /// <param name="pars">values (to be attribute value)  [NULL] </param>
        /// <param name="exc"></param>
        /// <returns></returns>
        public static XmlDocument GetXMLDocument(string docType, string[] pars, Exception exc = null) {
            XmlDocument xDoc = new XmlDocument();
            XmlNode topNode = xDoc.CreateElement(docType?.Replace(" ", "_") ?? "unknown");
            XmlNode bodyNode = xDoc.AppendChild(topNode);
            if (pars != null) {
                AddParsInfo(pars, xDoc, bodyNode);
            }
            if (exc != null) {
                AddExcInfo(exc, xDoc);
            }
            return xDoc;
        }

        private static void AddParsInfo(string[] pars, XmlDocument xDoc, XmlNode bodyNode) {
            for (int i = 0; i < pars.Length; i++) {
                XmlNode xn = xDoc.CreateElement("par" + i);
                xn.InnerText = pars[i] ?? "NULL";
                bodyNode.AppendChild(xn);
            }
        }

        private static void AddExcInfo(Exception exc, XmlDocument xDoc) {
            XmlNode xn = xDoc.CreateElement("exception");
            xn.AppendChild(xDoc.CreateElement("msg")).InnerText = exc.Message;
            if (exc.InnerException != null) {
                xn.AppendChild(xDoc.CreateElement("inner")).InnerText = exc.InnerException.Message;
            }
            if (exc.StackTrace != null) {
                xn.AppendChild(xDoc.CreateElement("stack")).InnerText = exc.StackTrace;
            }
            xDoc.FirstChild.AppendChild(xn);
        }


        public static string DatasetToXml(DataSet ds) {
            using (var memoryStream = new MemoryStream()) {
                using (TextWriter streamWriter = new StreamWriter(memoryStream)) {
                    var xmlSerializer = new XmlSerializer(typeof(DataSet));
                    xmlSerializer.Serialize(streamWriter, ds);
                    return Encoding.UTF8.GetString(memoryStream.ToArray());
                }
            }
        }
    }





}
