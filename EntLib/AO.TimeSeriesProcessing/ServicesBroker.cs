﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Configuration;
using System.Data;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Xml;
using Newtonsoft.Json;
using Microsoft.ServiceBus.Messaging;

// change for a git-tfs sync check
// above worked.
// another change in a clond of the original repo used for the above.

namespace AO.TimeSeriesProcessing {

    public delegate int ProcessArgsDelegate(string[] args, ContextData context);

    public class ServicesBroker {

        /// <summary>
        /// putting this processing delegate here to allow hosting environment to take over functions we have not (or will not) migrated down .
        /// </summary>
        public static ProcessArgsDelegate optionalProcessor = null;
        private static List<string> optionalServices = null;

        static bool _done;
        private static int _infoTimeSecs = 30;
        private static int _infoTimeMins = 30;


        public static ProcessArgsDelegate SetProcessDelegate(ProcessArgsDelegate newProcessor, List<string> newOptionalServices) {
            ProcessArgsDelegate oldProcessor = optionalProcessor;
            optionalProcessor = newProcessor;
            if (newOptionalServices != null) {
                optionalServices = new List<string>(newOptionalServices);
            }
            return oldProcessor;
        }

        public static string[] TestProcess(string[] args, string dataContent = null) {
            ProcessingArgs pa = new ProcessingArgs(args);
            return TestProcess(pa, dataContent);
        }



        public static string[] TestProcess(ProcessingArgs pa, string dataContent) {
            if (!string.IsNullOrEmpty(dataContent)) {
                pa["dataContent"] = dataContent;
                if (char.IsDigit(dataContent[0])) {
                    // assume no header row
                    pa["hasheaders"] = "0";
                }
            }
            return TestProcess(pa);
        }

        private const string timeSeriesTable = "value_series_meta";
        private static int recurseCount = 0;
        private const int maxRecurseCount = 3;

        /// <summary>
        /// Entry point to support executing an operation against the series management subsystem including unit tests and validation
        /// </summary>
        /// <param name="pa"></param>
        /// <returns></returns>
        public static string[] TestProcess(ProcessingArgs pa) {
            string[] rVal = { "initRval" };
            try {
                DateTime dtStt = DateTime.Now;
                Console.WriteLine($"Starting at {dtStt}");
                string vsrc = pa.GetControlVal("reqVsrc", timeSeriesTable);
                ContextData.InitContextStrings(vsrc, pa["aclenv"], "none", pa, null);
                ProcessingArgs.AccumulateRequestPars(vsrc, pa["aclenv"], "none", pa);
                string clientId = pa.GetControlVal("clientId", "");
                string sqlMeterMgtDatabase = pa.GetControlVal($"{clientId}sqlMeterMgtDatabasename", "InsightsDW_Test");
                if (pa["sqlMeterMgtCxnStg"] == null) {
                    string sqlMeterMgt = ConfigurationManager.AppSettings.Get("sqlMeterMgt");
                    pa["sqlMeterMgtCxnStg"] = String.Format(sqlMeterMgt, sqlMeterMgtDatabase);
                }
                ContextData context = new ContextData(pa);
                bool excepted = false;
                try {
                    DateTime dt = DateTime.Now;
                    context.ContextWrite("ProcessOptions invoke", "Begin");
                    bool processed = false;
                    if (optionalProcessor != null) {
                        if (recurseCount < maxRecurseCount) {
                            try {
                                recurseCount++;
                                context.CtxArgs = pa;
                                int redirVal = 0;
                                if (optionalServices != null &&
                                    optionalServices.Contains(pa.GetControlVal("oper", ""))) { // selective intercept
                                    redirVal = optionalProcessor(new[] { "nada" }, context);
                                    processed = true;
                                }
                                else { // global intercept
                                    redirVal = optionalProcessor(new[] { "nada" }, context);
                                    processed = true;
                                }
                                context.ContextWrite("ServicesProker.TestProcess", $"recursion result={redirVal}");
                            }
                            catch (Exception exc) {
                                context.ContextWrite("ServicesProker.TestProcess", $" !!! exception={exc.Message}, {exc.InnerException?.Message}");
                            }
                            finally {
                                recurseCount--;
                            }
                        }
                        else {
                            context.ContextWrite("ServicesProker.TestProcess", "exceeded recursion count");
                        }
                    }
                    if (!processed) {
                        ProcessOptions(pa, context, dtStt);
                    }
                    context.ContextWrite("ProcessOptions invoke", "End", dt);
                }
                catch (Exception exc) {
                    excepted = true;
                    rVal = new[] { $"Exception in TestProcess {exc.Message}, {exc.InnerException?.Message}", };
                }
                finally {
                    if (!excepted) {
                        long totalRows = context.Add2Counter("RefReadRows", 0);
                        double totalMsecs = DateTime.Now.Subtract(dtStt).TotalMilliseconds;
                        double perSec = (totalMsecs > 0 ? totalRows / totalMsecs : -1) * 1000;
                        string lStg = $"TestProcess >>>>>> recs/sec {perSec} for readrows={totalRows}, msecs={totalMsecs}";
                        context.ContextWrite("TestProcess ", lStg);
                        if (pa.GetControlVal("requireLogForce", "yes").Equals("yes")) {
                            Console.WriteLine(lStg);
                        }
                        Console.WriteLine(context.Snag());
                        if (pa.GetControlVal("retdatapa", "yes").Equals("yes") && (pa["retdatatxt"] != null)) {
                            rVal = new[] {
                                context.ContexFileNamePath, pa.GetControlVal("validateResult", context.Validation.FileName), context.AutoFixes.FileName,
                                context.NoFixes.FileName, context.Snag(), pa["retdatatxt"]
                            };
                        }
                        else {
                            rVal = new[] {
                                context.ContexFileNamePath, pa.GetControlVal("validateResult", context.Validation.FileName), context.AutoFixes.FileName,
                                context.NoFixes.FileName, context.Snag()
                            };
                        }
                    }
                    context.End();
                }
            }
            catch (Exception exc) {
                rVal = new[] { $"Exception outer in TestProcess {exc.Message}, {exc.InnerException?.Message}", };
            }
            return rVal;
        }


        public static string[] ImportProcess(string[] args, ref string retContent, string dataContent = null) {
            string[] rVal = { "initRval" };
            try {
                ProcessingArgs pa = new ProcessingArgs(args);
                DateTime dtStt = DateTime.Now;
                Console.WriteLine($"Starting at {dtStt}");

                string vsrc = pa.GetControlVal("reqVsrc", timeSeriesTable);

                ContextData.InitContextStrings(vsrc, pa["aclenv"], "none", pa, null);
                ProcessingArgs.AccumulateRequestPars(vsrc, pa["aclenv"], "none", pa);
                string clientId = pa.GetControlVal("clientId", "87");
                string sqlMeterMgtDatabase = pa.GetControlVal($"{clientId}sqlMeterMgtDatabasename", "InsightsDW_Test");
                if (pa["sqlMeterMgtCxnStg"] == null) {
                    string sqlMeterMgt = ConfigurationManager.AppSettings.Get("sqlMeterMgt");
                    pa["sqlMeterMgtCxnStg"] = String.Format(sqlMeterMgt, sqlMeterMgtDatabase);
                }
                if (dataContent != null) {
                    pa["dataContent"] = dataContent;
                }
                pa["retContent"] = retContent;
                ContextData context = new ContextData(pa);
                bool excepted = false;
                try {
                    DateTime dt = DateTime.Now;
                    context.ContextWrite("ImportProcess.ProcessOptions", "Begin");
                    ProcessOptions(pa, context, dtStt);
                    context.ContextWrite("ImportProcess.ProcessOptions", "End", dt);
                    if (retContent.Equals("retContent")) {
                        retContent = pa["retContent"];
                    }
                }
                catch (Exception exc) {
                    excepted = true;
                    rVal = new[] { $"Exception in ImportProcess {exc.Message}, {exc.InnerException?.Message}", };
                }
                finally {
                    if (!excepted) {
                        Console.WriteLine(context.Snag());
                        rVal = new[] {
                            context.ContexFileNamePath, context.Validation.FileName, context.AutoFixes.FileName, context.NoFixes.FileName,
                            context.Snag()
                        };
                    }
                    context.End();
                }
            }
            catch (Exception exc) {
                rVal = new[] { $"Exception outer in ImportProcess {exc.Message}, {exc.InnerException?.Message}", };
            }
            return rVal;
        }

        public static string DeleteOlderLogs(ProcessingArgs pa, ContextData context = null) {
            pa.GetControlVal("purgeShowOnly", "no");
            int sizeInMb = pa.GetControlVal("purgeSizeThreshold", 10000); // default to 10 GB
            int daysOld = pa.GetControlVal("purgeLogAgeDays", 14);

            DateTime oldTime = DateTime.Now.Subtract(TimeSpan.FromDays(daysOld));  // anything older then this is target for deletion
            string purgeDirectory = pa.GetControlVal("purgeDirectory", "getcurrentlog");
            if (purgeDirectory.Equals("getcurrentlog")) {
                if (context != null) {
                    purgeDirectory = Path.GetDirectoryName(context.ContexFileNamePath);
                    pa["purgeDirectory"] = purgeDirectory;
                }
                else {
                    pa["purgeDirectory"] = "NoConfiguredDirectoryForDefaultLogPath";
                }
            }
            if (!string.IsNullOrEmpty(purgeDirectory)) {
                FileAttributes attr = File.GetAttributes(purgeDirectory);
                //detect whether its a directory or file
                if ((attr & FileAttributes.Directory) == FileAttributes.Directory) {
                }
                else {
                    purgeDirectory = Path.GetDirectoryName(purgeDirectory);
                }
            }

            StringBuilder sb = new StringBuilder();
            long purgedLength = 0;
            long purgedCount = 0;
            long leftLength = 0;
            long leftCount = 0;
            string modOp = pa.GetControlVal("justshow", "no");
            const int maxDisplayLeft = 200;
            if (Directory.Exists(purgeDirectory)) {
                IEnumerable<string> fnames = Directory.EnumerateFiles(purgeDirectory);
                try {
                    foreach (string fname in fnames) {
                        FileInfo fi = new FileInfo(fname);
                        if ((fi.CreationTime < oldTime) || ((fi.Length / 1000000) > sizeInMb)) {
                            purgedLength += fi.Length;
                            purgedCount++;
                            sb.AppendLine($" #1 SB DeleteOlderLogs {modOp} delete log file {fname},{fi.CreationTime},{fi.Length}");
                            if (!modOp.Equals("justshow")) File.Delete(fname);
                        }
                        else {
                            leftLength += fi.Length;
                            leftCount++;
                            if (leftCount < maxDisplayLeft) {
                                sb.AppendLine($" #1 SB DeleteOlderLogs {modOp} leave log file {fname},{fi.CreationTime},{fi.Length}");
                            }
                            else if (leftCount == maxDisplayLeft) {
                                sb.AppendLine($" #1 SB DeleteOlderLogs stopping recording");
                            }
                        }
                    }
                }
                catch (Exception exc) {
                    string msg = $" SB DeleteOlderLogs, exception {exc.Message}, {exc.InnerException?.Message}";
                    sb.AppendLine(msg);
                    Debug.WriteLine(msg);
                }
            }
            else {
                sb.AppendLine($"SB DeleteOlderLogs nodirectory {purgeDirectory}");
            }
            return
                $"(purged={purgedCount} items for total length {(double)purgedLength / (1024 * 1024)}MB, left={leftCount} items for total length {leftLength / (1024 * 1024)}MB)\n" +
                sb;
        }

        /// <summary>
        /// process the operation indicated in the request.
        /// </summary>
        /// <param name="pa"></param>
        /// <param name="context"></param>
        /// <param name="dtStt"></param>
        public static string ProcessOptions(ProcessingArgs pa, ContextData context, DateTime dtStt) {
            string rVal = "";
            DateTime startTime = DateTime.Now;
            context.ContextWrite("ProcessOptions", "Begin");
            if (pa.GetControlVal("disablelog", "no").Equals("yes")) {
                context.ContextWrite("ProcessOptions", "disable logging requested", dtStt);
                AnalysisOutputFileStream.LoggingDisabled = true;
            }
            string clientId = pa.GetControlVal("clientId", "87");
            string sqlMeterMgtCxnString = pa.GetControlVal("sqlMeterMgtCxnStg", "");
            string sqlMeterMgtDatabase = pa.GetControlVal($"{clientId}sqlMeterMgtDatabasename", "InsightsDW_Test");
            if (string.IsNullOrEmpty(sqlMeterMgtCxnString)) {
                string sqlMeterMgt = pa.GetControlVal("sqlMeterMgt", "");
                pa["sqlMeterMgtCxnStg"] = String.Format(sqlMeterMgt, sqlMeterMgtDatabase);
            }
            if (pa.GetControlVal("restRoot", "nada").Equals("api") && pa.GetControlVal("restVersion", "nada").Equals("current")) {
                // using the new restful URI format
                string resourceName = pa.GetControlVal("restResource", "nada");
                string resourceVerb = pa.GetControlVal("restVerb", "nada").ToLower();
                string resourceId = pa.GetControlVal("restResId", "nada");
                switch (resourceName) {
                    case "ami": {
                        pa["collid"] = resourceId;
                    }
                        break;
                }
                switch (resourceVerb) {
                    case "get": {
                        switch (resourceName) {
                            case "ami": {
                                pa["oper"] = "recallseries";
                            }
                                break;
                            case "meter": {
                                pa["oper"] = "seemeters";
                            }
                                break;
                            case "statistics": {
                                pa["oper"] = "seestats";
                            }
                                break;
                            case "config": {
                                pa["oper"] = "getconfig";
                            }
                                break;
                        }
                    }
                        break;
                    case "put":
                    case "post": {
                        switch (resourceName) {
                            case "ami": {
                                pa["oper"] = "recallseries";  // tentative as we do not currently allow post
                            }
                                break;
                            case "config": {
                                pa["oper"] = "setconfig";
                            }
                                break;
                        }
                        if (pa.GetControlVal("bodyOverride", "nope").Equals("yes")) {  // initialize params from the body payload.
                            if (pa["reqContent"] != null) {
                                string varStg;
                                if (pa["reqContent"].Length > 8192) {
                                    varStg = pa["reqContent"].Substring(0, 8192);
                                    pa.Override(varStg);
                                }
                                else {
                                    pa.Override(pa["reqContent"]);
                                }
                            }
                        }
                    }
                        break;
                }

            }
            string todo = pa["oper"] ?? "nada";
            pa["retdatapa"] = pa["retdatapa"] ?? "yes";
            pa["aclenv"] = pa["aclenv"] ?? ConfigurationManager.AppSettings["AMiCassandraKey"] ?? "work";
            try {
                switch (todo) {
                    case "validator": {
                        string validateResult = ValidateData(context, pa);
                        pa["validateResult"] = validateResult;
                    }
                        break;
                    case "multicsv": {
                        ToolingRequest tr = new ToolingRequest();
                        tr.SubmitBulkDataRequests(pa, context);
                    }
                        break;

                    case "splitcsv": {
                        string srcFile = pa.GetControlVal("srcfile", "nada");
                        int maxLines = pa.GetControlVal("maxlines", 10000);
                        using (StreamReader sr = new StreamReader(srcFile)) {
                            string csvLine = sr.ReadLine();
                            int destIdx = 0;
                            int lineCnt = 0;
                            string destFile = $"{srcFile}.{destIdx:D3}.input.csv";
                            StreamWriter sw = new StreamWriter(destFile);
                            sw.WriteLine(csvLine);
                            while (!sr.EndOfStream) {
                                string line = sr.ReadLine();
                                sw.WriteLine(line);
                                if (++lineCnt >= maxLines) {
                                    sw.Flush();
                                    sw.Close();
                                    lineCnt = 0;
                                    destIdx++;
                                    destFile = $"{srcFile}.{destIdx:D3}.input.csv";
                                    sw = new StreamWriter(destFile);
                                    sw.WriteLine(csvLine);
                                }
                            }
                            sw.Close();

                        }
                    }
                        break;

                    case "cvtCsv2Tall": {
                        ConvertAmiCsvToTall(context, dtStt, pa);
                        rVal = pa["retContent"];
                    }
                        break;
                    case "cvt2Tall": {
                        ZipManager zm = new ZipManager(pa, context);
                        string zipSrc = pa.GetControlVal("zipSrc", "");
                        string dropLocation = pa.GetControlVal("dropLocation", "");
                        FileAttributes attr = File.GetAttributes(zipSrc);
                        if ((attr & FileAttributes.Directory) == FileAttributes.Directory) {
                            Console.WriteLine($"cvt2Tall {zipSrc} -> {dropLocation}");
                            context.ContextWrite("cvt2Tall", $" {zipSrc} -> {dropLocation}");
                            string consolidateFilter = pa.GetControlVal("consFilter", "*.zip");
                            IEnumerable<string> fnames = Directory.EnumerateFiles(zipSrc, consolidateFilter);
                            foreach (string fname in fnames) {
                                Console.WriteLine($"cvt2Tall {fname} -> {dropLocation}");
                                context.ContextWrite("cvt2Tall", $" {fname} -> {dropLocation}");
                                zm.Convert2TallAmi(fname, dropLocation);
                            }

                        }
                        else {
                            zm.Convert2TallAmi(zipSrc, dropLocation);
                        }
                    }
                        break;
                    case "ziptest": {
                        ZipManager zm = new ZipManager(pa, context);
                        string zipSrc = pa.GetControlVal("zipSrc", "");
                        string dropLocation = pa.GetControlVal("dropLocation", "");
                        zm.ExtractAndDrop(zipSrc, dropLocation);
                    }
                        break;

                    case "recallseries": {
                        try {

                            string timezone = pa.GetControlVal("timezone", "EST");
                            bool adjustTimezone = Convert.ToBoolean(pa.GetControlVal("adjusttimezone", "false"));
                            SeriesCollectionSpecification collSpec = new SeriesCollectionSpecification(pa);
                            collSpec.InitializeCache(pa, pa.GetControlVal("resetcache", "no").Equals("yes"), context);
                            SeriesSpecification seriesSpec = new SeriesSpecification(pa, null as DataRow);

                            string commodity = pa.GetControlVal("commodity", "0");
                            string uom = pa.GetControlVal("uom", string.Empty);
                            string seriesColInfoSpec = pa.GetControlVal($"{clientId}SeriesColInfoSpec{commodity}", $"0SeriesColInfoSpec{commodity}");

                            string spec = $"Commodity:{commodity}";
                            if (!string.IsNullOrEmpty(uom)) {
                                spec = $"uom:{uom.ToLower()}|{spec}";
                            }
                            else if (string.IsNullOrEmpty(uom) && commodity == "1") {
                                spec = $"uom:kwh|{spec}";
                            }

                            // direction and tou if configured
                            if (!string.IsNullOrEmpty(seriesColInfoSpec)) {
                                string[] specPairs = seriesColInfoSpec.Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                                foreach (string specPair in specPairs) {
                                    string[] s = specPair.Split(new[] { ':' }, StringSplitOptions.RemoveEmptyEntries);
                                    // if there's direction setup in series col info spec, retrieve based on direction passed in arg; otherwise default to forward(1);
                                    // forward =1, reverse = 19
                                    if (s[0].ToLower().Contains("direction")) {
                                        string direction = pa.GetControlVal("direction", "1");
                                        spec = string.IsNullOrEmpty(spec) ? $"{s[0]}:{direction}" : $"{spec}|{s[0]}:{direction}";
                                    }
                                    // if there's tou setup in series col info spec, retrieve based on tou passed in arg; otherwise default to noTou (0)
                                    // noTou = 0, onpeak = 1, offpeak = 2, shoulder1 = 3, shoulder2 = 4, critical peak = 5
                                    if (s[0].ToLower().Contains("tou")) {
                                        string tou = pa.GetControlVal("tou", "0");
                                        spec = string.IsNullOrEmpty(spec) ? $"{s[0]}:{tou}" : $"{spec}|{s[0]}:{tou}";
                                    }
                                }
                            }

                            string seriesId = string.IsNullOrEmpty(spec) ? pa["collid"] : $"{pa["collid"]}|{spec}";

                            // if force the above should do it seriesSpec.InitializeCache(pa, pa.GetControlVal("resetcache", "no").Equals("yes"));
                            DataTable dt = collSpec.GetSeriesData(pa, context, seriesId, seriesSpec);

                            // convert time zone
                            if (adjustTimezone)
                                ConvertTimeZone(dt, timezone);

                            int retRows = 0;
                            if (dt != null) {
                                //int seriesBucketSizeSeconds = pa.GetControlVal("seriesBucketSizeSeconds", -1);
                                AmiAccumulator amiAccum = new AmiAccumulator(pa);
                                if (amiAccum.RequestBucketize(pa)) {
                                    dt = amiAccum.BucketizeAmiData(context, pa, dt);
                                }
                                else {
                                    bool doMeta = pa.GetControlVal("wantMetadata", -1) > 0;
                                    bool wantCassinfo = pa.GetControlVal("wantCassinfo", -1) > 0;
                                    if (!wantCassinfo) {
                                        dt.Columns.Remove(AmiAccumulator.TsColId);
                                        dt.Columns.Remove(AmiAccumulator.TsColDid);
                                    }
                                    if (!doMeta) {
                                        dt.Columns.Remove(AmiAccumulator.TsColMeta);
                                    }
                                    if (pa.GetControlVal("seriesSort", "asc").Equals("asc")) {
                                        AmiAccumulator.MakeAsc(ref dt);
                                    }
                                }
                                string dataOut = DataUtils1.FormatResultsData(pa, dt);
                                context.NoFixes.AddLine(dataOut);
                                if (pa.GetControlVal("retdatapa", "yes").Equals("yes")) {
                                    pa["retdatatxt"] = dataOut;
                                }
                                retRows = dt.Rows.Count;
                            }
                            if (retRows > 0) {
                                context.ContextWrite("tseries returned data", $" rows={retRows}");
                                context.Add2Counter("retrows", retRows);
                            }
                            else {
                                context.ContextWrite("warn_tseries", " no returned data");
                            }
                        }
                        catch (Exception exc) {
                            pa["retdatapa"] = pa["retdatapa"] ?? "yes";
                            context.ContextWrite("error_tseries", $" exception {exc.Message},{exc.InnerException?.Message} ");
                        }
                    }
                        break;
                    case "tseries": {
                        SeriesSpecification.Initialize(pa, context);
                        //samples:
                        //oper=genmeters nummeters=10 numseries=3 
                        DateTime stest = TimingHelper.RunningTimes(context, dtStt, null, "seestats", "start");
                        string commodity = pa.GetControlVal("commodity", "0");
                        string seriesColInfoSpec = pa.GetControlVal($"{clientId}SeriesColInfoSpec{commodity}", $"0SeriesColInfoSpec{commodity}");
                        string uom = pa.GetControlVal("uom", string.Empty);
                        string spec = $"Commodity:{commodity}";
                        if (!string.IsNullOrEmpty(uom)) {
                            spec = $"uom:{uom.ToLower()}|{spec}";
                        }
                        else if (string.IsNullOrEmpty(uom) && commodity == "1") {
                            spec = $"uom:kwh|{spec}";
                        }
                        // direction and tou if configured
                        if (!string.IsNullOrEmpty(seriesColInfoSpec)) {
                            string[] specPairs = seriesColInfoSpec.Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                            foreach (string specPair in specPairs) {
                                string[] s = specPair.Split(new[] { ':' }, StringSplitOptions.RemoveEmptyEntries);
                                // if there's direction setup in series col info spec, retrieve based on direction passed in arg; otherwise default to forward(1);
                                // forward =1, reverse = 19
                                if (s[0].ToLower().Contains("direction")) {
                                    string direction = pa.GetControlVal("direction", "1");
                                    spec = string.IsNullOrEmpty(spec) ? $"{s[0]}:{direction}" : $"{spec}|{s[0]}:{direction}";
                                }
                                // if there's tou setup in series col info spec, retrieve based on tou passed in arg; otherwise default to noTou (0)
                                // noTou = 0, onpeak = 1, offpeak = 2, shoulder1 = 3, shoulder2 = 4, critical peak = 5
                                if (s[0].ToLower().Contains("tou")) {
                                    string tou = pa.GetControlVal("tou", "0");
                                    spec = string.IsNullOrEmpty(spec) ? $"{s[0]}:{tou}" : $"{spec}|{s[0]}:{tou}";
                                }
                            }
                        }


                        SeriesCollectionSpecification collSpec = new SeriesCollectionSpecification(pa);
                        collSpec.InitializeCache(pa, false, context);
                        SeriesSpecification seriesSpec = new SeriesSpecification(pa, null as DataRow);
                        seriesSpec.InitializeCache(pa);
                        stest = TimingHelper.RunningTimes(context, dtStt, stest, "tseries", "initcache");
                        string colidstg = pa["collid"];
                        string[] colids = new[] { colidstg };
                        if (colidstg.IndexOf(",", StringComparison.InvariantCultureIgnoreCase) > 0) {
                            colids = colidstg.Split(',');
                        }
                        else {
                            // maybe another real recall test
                            if (colidstg != "collectionStarterId") {
                                try {
                                    string seriesId = string.IsNullOrEmpty(spec) ? colidstg : $"{colidstg}|{spec}";

                                    DataTable dt = collSpec.GetSeriesData(pa, context, seriesId, seriesSpec);
                                    int retRows = dt.Rows.Count;
                                    if (retRows > 0) {
                                        context.ContextWrite("tseries returned data", $" rows={retRows}");
                                    }
                                    else {
                                        context.ContextWrite("error_tseries", " no returned data");
                                    }
                                }
                                catch (Exception exc) {
                                    context.ContextWrite("error_tseries", $" exception {exc.Message},{exc.InnerException?.Message} ");
                                }
                            }
                        }
                        foreach (string colid in colids) {
                            string res = collSpec.TestGetData(pa, colid);
                            stest = TimingHelper.RunningTimes(context, dtStt, stest, "tseries", "getone");
                            Console.WriteLine(res);
                        }
                        foreach (string colid in colids) {
                            string seriesId = string.IsNullOrEmpty(spec) ? colid : $"{colid}|{spec}";

                            string id = collSpec.GetDataSeriesKey(pa, seriesId);
                            Console.WriteLine(id);
                            string res = seriesSpec.GetData(pa, id);
                            stest = TimingHelper.RunningTimes(context, dtStt, stest, "tseries", "getseriesspec");
                            Console.WriteLine(res);
                        }
                        DateTime testStt = pa.GetControlVal("dtstt", DateTime.Parse("1/1/2017"));
                        DateTime testStp = pa.GetControlVal("dtstp", DateTime.Parse("1/2/2017"));
                        List<string> qryStgs = new List<string>();
                        foreach (string colid in colids) {
                            string seriesId = string.IsNullOrEmpty(spec) ? colid : $"{colid}|{spec}";
                            string id = collSpec.GetDataSeriesKey(pa, seriesId);
                            Console.WriteLine(id);
                            List<string> queryStgs = seriesSpec.GetQuery(pa, id, testStt, testStp);
                            foreach (string queryStg in queryStgs) {
                                qryStgs.Add(queryStg);
                            }
                            stest = TimingHelper.RunningTimes(context, dtStt, stest, "tseries", "getseriesspec");
                        }
                        stest = TimingHelper.RunningTimes(context, dtStt, stest, "tseries", "end");
                        if (qryStgs.Count > 0) {
                            //int retRowsCnt = 0;
                            SeriesStorePersistenceInfo cc1 = SeriesStorePersistenceInfo.GetCacheCxn(pa);
                            //cc1.Connect(pa);
                            stest = TimingHelper.RunningTimes(context, dtStt, stest, "tseries", "connected");
                            BatchExecutor batch = new BatchExecutor();
                            string res = batch.ExecuteGet(pa, context, qryStgs, cc1);
                            stest = TimingHelper.RunningTimes(context, dtStt, stest, "query 3", $"end, res={res}");
                            DataTable dt = batch.ExecuteGetTable(pa, context, qryStgs, cc1);
                            TimingHelper.RunningTimes(context, dtStt, stest, "query 1", $"end table, rows={dt.Rows.Count}");

                            // cassandra map doesn't come into XML cleanly
                            //TODO: http://theburningmonk.com/2010/05/net-tips-xml-serialize-or-deserialize-dictionary-in-csharp/

                            string jsonString = JsonConvert.SerializeObject(dt);
                            context.NoFixes.AddLine(jsonString);
                        }
                    }
                        break;
                    case "purgelogweek": {
                        pa["purgeLogAgeDays"] = pa.GetControlVal("purgeLogAgeDays", "8");
                        PurgeLogOperation(pa, context);
                    }
                        break;

                    case "purgelogday": {
                        pa["purgeLogAgeDays"] = pa.GetControlVal("purgeLogAgeDays", "2");
                        PurgeLogOperation(pa, context);
                    }
                        break;

                    case "time":
                        SomeTests.TimeTests();
                        break;
                    case "see_events":
                        EventsProcessing(pa);
                        break;

                    case "avpreproc": {
                        //samples:
                        string cvtType = pa.GetControlVal("cvtType", "avista");
                        switch (cvtType) {
                            case "avista":
                                pa["clientId"] = pa.GetControlVal("clientId", "290");
                                break;
                            case "ameren":
                                pa["clientId"] = pa.GetControlVal("clientId", "174");
                                break;
                        }
                        AvPreProc(context, dtStt, pa);
                        rVal = pa["retContent"];
                    }
                        break;

                    case "avfullprocess": {
                        StringBuilder sb = new StringBuilder();
                        StreamReader referenceStream = new StreamReader(pa.GetControlVal("importFile", null));
                        while (!referenceStream.EndOfStream) {
                            string inpd = referenceStream.ReadLine();
                            if (inpd == null)
                                throw new Exception("input file stream is null");
                            if (!string.IsNullOrEmpty(inpd.Trim())) {
                                sb.Append(inpd + "\n");
                            }
                        }
                        string dataBody = sb.ToString().Trim();
                        Debug.WriteLine($" 8.0  AmiAdvancedConnector.ImportAmiDataValidate : for\n{dataBody}");
                        string cvtData = ConvertCassandraDataTestl(dataBody, pa);
                        Debug.WriteLine($" 8.0.1  AmiAdvancedConnector.ImportAmiDataValidate : datlen = {cvtData.Length}  ");
                        InsertCassandraDataTestl(cvtData, pa);
                        Debug.WriteLine($" 8.0.1  AmiAdvancedConnector.ImportAmiDataValidate : ");
                    }
                        break;
                    case "amfullprocess": {
                        StringBuilder sb = new StringBuilder();
                        string filename = pa.GetControlVal("importFile", null);
                        StreamReader referenceStream =
                            new StreamReader(filename);
                        while (!referenceStream.EndOfStream) {
                            string inpd = referenceStream.ReadLine();
                            if (inpd == null)
                                throw new Exception("input file stream is null");
                            if (!string.IsNullOrEmpty(inpd.Trim())) {
                                sb.Append(inpd + "\n");
                            }
                        }
                        string dataBody = sb.ToString().Trim();

                        Debug.WriteLine(
                            $" 8.0  AmiMidTermConnector.ImportAmiDataValidate : for\n{dataBody}");
                        if (!filename.ToLower().Contains("daily")) {
                            dataBody = ConvertCassandraDataWideToTall(dataBody, pa);
                            //pa["altQueryPath"] = "new";
                        }
                        Debug.WriteLine(
                            $" 8.0.1  AmiMidTermConnector.ImportAmiDataValidate : datlen = {dataBody.Length}  ");
                        InsertCassandraDataTestl(dataBody, pa);
                        Debug.WriteLine($" 8.0.1  AmiMidTermConnector.ImportAmiDataValidate : ");
                    }
                        break;

                    case "seemeters": {
                        //samples:
                        //oper=seemeters quantity=100
                        DateTime stest = TimingHelper.RunningTimes(context, dtStt, null, "seemeters", "start");
                        DataTable dt = new SeriesCollectionSpecification(pa).GetSome(pa);
                        TimingHelper.RunningTimes(context, dtStt, stest, "seemeters", "end");
                        StringWriter sw = new StringWriter();
                        dt.WriteXml(sw, true);
                        string retMedia = pa.GetControlVal("Accept", "text/plain").ToLower();
                        if (pa.GetControlVal("retdatapa", "yes").Equals("yes")) {
                            switch (retMedia) {
                                case "application/xml": {
                                    pa["retdatatxt"] = sw.ToString();
                                }
                                    break;
                                case "application/json": {
                                    XmlDocument doc = new XmlDocument();
                                    doc.LoadXml(sw.ToString());
                                    pa["retdatatxt"] = JsonConvert.SerializeObject(doc);
                                    //Console.WriteLine(pa["retdatatxt"]);
                                }
                                    break;
                                default: {
                                    pa["retdatatxt"] = sw.ToString();
                                }
                                    break;
                            }
                        }
                    }
                        break;
                    case "getconfig": {
                        if (pa.GetControlVal("custom", "nope").Equals("yes")) {
                            string result = pa.GetCustomConfigFile();
                            pa["retdatatxt"] = result;
                            pa["Accept"] = "application/xml";
                        }
                        else {
                            string retMedia = pa.GetControlVal("Accept", "text/plain").ToLower();
                            switch (retMedia) {
                                case "application/xml": {
                                    XmlDocument doc = JsonConvert.DeserializeXmlNode(pa.AsJson(), "config");
                                    pa["retdatatxt"] = doc.OuterXml;
                                }
                                    break;
                                //case "application/json":
                                default: {
                                    pa["retdatatxt"] = pa.AsJson();
                                }
                                    break;
                            }
                        }
                    }
                        break;
                    case "setconfig": {
                        string setResult = "noSetResult";
                        if (pa["reqContent"] != null) {
                            try {
                                XmlDocument doc = new XmlDocument();
                                doc.LoadXml(pa["reqContent"]);  // check content structure
                                setResult = pa.SetCustomConfig(doc);
                            }
                            catch (Exception exc) {
                                setResult = $"setconfig exception : {exc.Message}, {exc.InnerException?.Message}";
                            }
                        }
                        pa["retdatatxt"] = setResult;
                        pa["Accept"] = "text/plain";
                    }
                        break;
                    case "seeseries": {
                        //samples:
                        //oper=seeseries quantity=100
                        DateTime stest = TimingHelper.RunningTimes(context, dtStt, null, "seeseries", "start");
                        DataTable dt = new SeriesSpecification(pa).GetSome(pa);
                        TimingHelper.RunningTimes(context, dtStt, stest, "seeseries", "end");
                        StringWriter sw = new StringWriter();
                        dt.WriteXml(sw, true);
                        if (pa.GetControlVal("usejson", "no") == "yes") {
                            XmlDocument doc = new XmlDocument();
                            doc.LoadXml(sw.ToString());
                            Console.WriteLine(JsonConvert.SerializeObject(doc));
                        }
                        else {
                            Console.WriteLine(sw.ToString());
                        }
                    }
                        break;

                    case "seestats": {
                        //samples:
                        //oper=seestats 
                        DateTime stest = TimingHelper.RunningTimes(context, dtStt, null, "seestats", "start");
                        SeriesCollectionSpecification col = new SeriesCollectionSpecification(pa);
                        string cinfo = col.Statistics(pa);
                        SeriesSpecification ser = new SeriesSpecification(pa);
                        string sinfo = ser.Statistics(pa);
                        TimingHelper.RunningTimes(context, dtStt, stest, "seestats", "end");
                        Console.WriteLine($" colls:{cinfo}, series:{sinfo}");
                    }
                        break;

                    case "genseries": {
                        //samples:
                        //oper=genseries nummeters=10 numseries=3 
                        SeriesSpecification spec = new SeriesSpecification(pa);
                        spec.Generate(pa);
                    }
                        break;
                    case "genmeters": {
                        SeriesSpecification.Initialize(pa, context);
                        //samples:
                        //oper=genmeters nummeters=10 numseries=3 
                        SeriesCollectionSpecification spec = new SeriesCollectionSpecification(pa);
                        spec.Generate(pa);
                    }
                        break;
                    case "genmetersfrombilling": {
                        string cfgMeterCsvCols = ConfigurationManager.AppSettings["AmiDistinctMeterImportCols"];
                        if (string.IsNullOrEmpty(cfgMeterCsvCols)) {
                            cfgMeterCsvCols = "meter=0,spoint=0,client=-1,cust=-1,acct=-1";
                        }
                        pa["namecolidx"] = cfgMeterCsvCols;
                        SeriesSpecification.Initialize(pa, context);
                        //samples:
                        //oper=genmeters nummeters=10 numseries=3 
                        DateTime stest = TimingHelper.RunningTimes(context, dtStt, null, "seestats", "start");
                        SeriesCollectionSpecification spec = new SeriesCollectionSpecification(pa);
                        spec.InitializeCache(pa, pa.GetControlVal("resetcache", "no").Equals("yes"), context);
                        spec.GenerateFromCsvData(pa, context, spec);
                        spec.InitializeCache(pa, true, context);
                        TimingHelper.RunningTimes(context, dtStt, stest, "seestats", "end");
                    }
                        break;
                    case "genmetersfromami": {
                        pa["namecolidx"] = "client=0,cust=-1,meter=1,spoint=3,acct=2";
                        SeriesSpecification.Initialize(pa, context);
                        //samples:
                        //oper=genmeters nummeters=10 numseries=3 
                        DateTime stest = TimingHelper.RunningTimes(context, dtStt, null, "seestats", "start");
                        SeriesCollectionSpecification spec = new SeriesCollectionSpecification(pa);
                        spec.InitializeCache(pa, pa.GetControlVal("resetcache", "no").Equals("yes"), context);
                        spec.GenerateFromCsvData(pa, context, spec);
                        spec.InitializeCache(pa, true, context);
                        TimingHelper.RunningTimes(context, dtStt, stest, "seestats", "end");
                    }
                        break;
                    case "genseriesdata": {
                        //samples:
                        //oper=genseriesdata minutes=60 numseries=10000 seriesstart=100 dtstt=2/1/2017  dtstp=2/7/2017 qualreps=1 gendata=10000
                        DateTime stest = TimingHelper.RunningTimes(context, dtStt, null, "genseriesdata", "start");
                        //SeriesSpecification spec = new SeriesSpecification(pa);
                        SeriesCollectionSpecification spec = new SeriesCollectionSpecification(pa);
                        spec.InitializeCache(pa, pa.GetControlVal("resetcache", "no").Equals("yes"), context);
                        spec.GenerateDataFromSeries(pa, context);
                        TimingHelper.RunningTimes(context, dtStt, stest, "genseriesdata", "end");
                    }
                        break;
                    case "cassimport": {
                        context.ContextWrite("cassimport", "begin");
                        // samples
                        // oper=cassimport procLimit=50 importfile=C:\junk\DevWt1\seriesvalues_168_5_100.csv
                        //  oper=cassimport minutes=60 numseries=10000 seriesstart=100 dtstt=2/1/2017  dtstp=2/2/2017 qualreps=1 gendata=100 procLimit=50
                        string procableTag = pa.GetControlVal("reqVsrc", "value_series_meta");
                        string procEnvTag = pa.GetControlVal("aclenv", "work");
                        string nvpairsStg =
                            $"procTableTag={procableTag} procEnvTag={procEnvTag} isInsert=1 maxRows=30 procLimit=500000 allowFilter=0 sortCol=last_modified_date procResp=1 outputTag=test1 userPath=anon2 contextRecs=25 contextRet=1 expectRows=0 matchtype=get";
                        pa.AddIfMissing(nvpairsStg);
                        DateTime stest = TimingHelper.RunningTimes(context, dtStt, null, "cassimport", "start");
                        CassImportProcessing(context, pa);
                        TimingHelper.RunningTimes(context, dtStt, stest, "cassimport", "end");
                        context.ContextWrite("cassimport", "end", startTime);
                    }
                        break;
                    case "cassvalidate": {
                        context.ContextWrite("cassvalidate", "begin");
                        // samples
                        // oper=cassimport procLimit=50 importfile=C:\junk\DevWt1\seriesvalues_168_5_100.csv
                        //  oper=cassimport minutes=60 numseries=10000 seriesstart=100 dtstt=2/1/2017  dtstp=2/2/2017 qualreps=1 gendata=100 procLimit=50
                        string procableTag = pa.GetControlVal("reqVsrc", "value_series_meta");
                        string procEnvTag = pa.GetControlVal("aclenv", "work");
                        string procQuery = pa.GetControlVal($"{procEnvTag}qSel0", "");
                        string procRows = pa.GetControlVal($"{procEnvTag}qRows", "*");
                        string nvpairsStg =
                            $"procTableTag={procableTag} procEnvTag={procEnvTag} skipRefRows=99 maxRows=30 procLimit=500000 allowFilter=0 sortCol=last_modified_date procResp=1 outputTag=test1 userPath=anon2 contextRecs=25 contextRet=1 expectRows=1 matchtype=get batchLimit=0";
                        pa.AddIfMissing(nvpairsStg);
                        pa["qSel0"] = procQuery;
                        pa["qRows"] = procRows;
                        pa["allowFilter"] = "ALLOW FILTERING";
                        DateTime stest = TimingHelper.RunningTimes(context, dtStt, null, "cassvalidate", "start");
                        CassImportProcessing(context, pa);
                        TimingHelper.RunningTimes(context, dtStt, stest, "cassvalidate", "end");
                        context.ContextWrite("cassvalidate", "end", startTime);
                    }
                        break;
                    case "gseries":
                        GenTemplateSeriesData(pa);
                        break;
                    case "OldFilesProcessing":
                        FilesProcessingOld(pa);
                        break;
                }
            }
            catch (Exception exc) {
                if (pa.GetControlVal("disablelog", "no").Equals("yes")) {
                    AnalysisOutputFileStream.LoggingDisabled = false;
                    context.ContextWrite("ProcessOptions", "temp enable logging", dtStt);
                    context.ContextWrite($"ProcessOptions.{todo}, Exception {exc.Message}, inner={exc.InnerException?.Message}");
                    Console.WriteLine($"ProcessOptions.{todo}, Exception {exc.Message}, inner={exc.InnerException?.Message}");
                    AnalysisOutputFileStream.LoggingDisabled = true;
                }
                else {
                    context.ContextWrite($"ProcessOptions.{todo}, Exception {exc.Message}, inner={exc.InnerException?.Message}");
                    Console.WriteLine($"ProcessOptions.{todo}, Exception {exc.Message}, inner={exc.InnerException?.Message}");
                }
            }
            finally {
                DateTime dtStp = DateTime.Now;
                Console.WriteLine($"total time = {dtStp.Subtract(dtStt).TotalMilliseconds} ms at {DateTime.Now}");
                Console.WriteLine($"context data from {context.ContexFileNamePath}");
            }
            if (pa.GetControlVal("disablelog", "no").Equals("yes")) {
                AnalysisOutputFileStream.LoggingDisabled = false;
                context.ContextWrite("ProcessOptions", "enabling logging", dtStt);
            }
            context.ContextWrite("counters", context.Counters());
            context.ContextWrite("ProcessOptions", "End", startTime);
            return rVal;
        }


        private static string ValidateData(ContextData context, ProcessingArgs pa) {
            List<string> resultsRecs = new List<string>();

            string srcDataRef = pa.GetControlVal("useRefFile", "");
            FileAttributes attr = File.GetAttributes(srcDataRef);
            List<string> srcFiles = new List<string>();
            bool serializeValidation = pa.GetControlVal("serializeValidation", "no").Equals("yes");
            if (serializeValidation && (attr & FileAttributes.Directory) == FileAttributes.Directory) {
                int maxFiles = pa.GetControlVal("maxFiles", 20);
                bool justListData = pa.GetControlVal("justListData", "no").Equals("yes");
                int skipFiles = pa.GetControlVal("skipFiles", 0);
                string consolidateFilter = pa.GetControlVal("consFilter", "*.csv");
                IEnumerable<string> fnames = Directory.EnumerateFiles(srcDataRef, consolidateFilter)
                    .OrderBy(fileName => fileName);
                int doSkip = skipFiles; // want to snag the first one
                long totalRecsToProcess = 0;
                long totalFilesToProcess = 0;
                foreach (string fname in fnames) {
                    if (doSkip >= skipFiles) {
                        if (justListData) {
                            totalRecsToProcess += ShowProposedProcessInfo(fname, pa);
                            totalFilesToProcess++;
                        }
                        else {
                            srcFiles.Add(fname);
                            totalFilesToProcess++;
                        }
                        doSkip = 0;
                    }
                    else {
                        doSkip++;
                    }
                    if (totalFilesToProcess >= maxFiles) break;
                }
                if (justListData) {
                    int lowEstimateRate = pa.GetControlVal("lowEstimateRate", 200);
                    int higEstimateRate = pa.GetControlVal("higEstimateRate", 400);
                    long lowRateSecs = totalRecsToProcess / lowEstimateRate;
                    long higRateSecs = totalRecsToProcess / higEstimateRate;
                    Console.WriteLine(
                        $"*** would process {totalFilesToProcess} with {totalRecsToProcess} total records, estimated time of {higRateSecs} to {lowRateSecs} seconds.");
                    context.Add2Counter("cntr_lowRateSecs", lowRateSecs);
                    context.Add2Counter("cntr_higRateSecs", higRateSecs);
                    context.Add2Counter("cntr_totalFilesToProcess", totalFilesToProcess);
                    context.Add2Counter("cntr_totalRecsToProcess", totalRecsToProcess);
                }
            }
            else {
                srcFiles.Add(srcDataRef);
            }

            string validateFile = string.Empty;
            foreach (string fname in srcFiles) {
                pa["useRefFile"] = fname;
                string res = ValidateDroppedFile(context, pa);
                resultsRecs.Add(res);
                context.ContextWrite("Validate Results", "Begin");
                Console.WriteLine($"\n\n ***validateresults*** {res}");
                string[] resRow = res.Split(new[] { Environment.NewLine }, StringSplitOptions.None);

                context.ContextWrite("**validateResultFile**");
                foreach (string row in resRow) {
                    context.ContextWrite(row);
                }
                validateFile = resRow[0];
                context.ContextWrite("Validate Results", "End");
            }

            Console.WriteLine($"\n\n **composite results*** ");
            foreach (string resLine in resultsRecs) {
                Console.WriteLine($" **compositevalidateresults*** {resLine}");
            }

            return validateFile;
        }

        public static string ValidateDroppedFile(ContextData context, ProcessingArgs pa) {
            string result = "0";

            // set environment config setting
            importFileName = pa.GetControlVal("importfile", importFileName);
            BatchUtils.EstablishIntervalEnvironment(pa, importFileName);

            int procLimit = pa.GetControlVal("procLimit", 50);
            string useEnv = pa.GetControlVal("aclEnv", "perfd");
            string clientId = pa.GetControlVal("clientId", "87");
            string valQuery = pa.GetClientControlVal(clientId, "valQueryMap", "valQry");
            string overrideQry = pa.GetControlVal("overrideQry", $"{useEnv}{valQuery}");//"perfdvalQry");
            string useRefFile = pa.GetControlVal("useRefFile", @"C:\_Test\ameren\t104\174_ami_daily_20150703_001.csv");
            int skipRows = pa.GetControlVal("skipRows", 1000);
            string reqVsrc = pa.GetControlVal("reqVsrc", "consumption_by_meter");
            string processCommand = pa.GetControlVal("validateFileCmd", "");
            string allowFilter = pa.GetControlVal("allowFilter", "1");
            int expectRows = pa.GetControlVal("expectRows", 1);
            string matchtype = pa.GetControlVal("matchtype", "min");

            if (!string.IsNullOrEmpty(processCommand) && !string.IsNullOrEmpty(useRefFile)) {// && File.Exists(useRefFile)) {
                string execCmd = string.Format(processCommand, useRefFile, useEnv, procLimit, overrideQry, skipRows, allowFilter, expectRows, matchtype, reqVsrc);
                pa.Override(execCmd);
                pa["excCmd"] = execCmd;
                // few vars for the older validate pipeline
                pa["srcfile"] = useRefFile;
                pa["bulkReqBody"] = "";
                pa["bulkReqBodyFile"] = "";
                pa["reqVenv"] = useEnv;
                pa["reqVval"] = pa.GetControlVal("reqVval", "99");

                Console.WriteLine($"  submit cmd '{execCmd}'");
                try {
                    ToolingRequest tr = new ToolingRequest();
                    Task<string> t = tr.SubmitBulkDataRequests(pa, context);
                    result = t.Result;

                    //string[] procArgs = execCmd.Split(new[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
                    //int rVal = MainProcess(procArgs);
                    //result = rVal.ToString();
                }
                catch (Exception exc) {
                    Console.WriteLine($"ValidateDroppedFile.ProcessDroppedFile: exception is {exc.Message}, inner={exc.InnerException?.Message},  stack={exc.StackTrace}");
                }
            }
            else {

            }
            string[] resRow = result.Split(new[] { Environment.NewLine }, StringSplitOptions.None);
            foreach (string s in resRow) {
                if (s.IndexOf("**SnagCounters**") > 0) {
                    return $"{resRow[0]}{Environment.NewLine}**referencedFile**{useRefFile}{Environment.NewLine}{s}";
                }
            }
            return result;
        }


        private static long ShowProposedProcessInfo(string fileName, ProcessingArgs pa) {
            long lineCount = 0;
            using (FileStream stream = File.OpenRead(fileName)) {
                using (StreamReader sr = new StreamReader(stream)) {
                    while (!sr.EndOfStream) {
                        sr.ReadLine();
                        lineCount++;
                    }
                }
            }
            long origProcLines = lineCount - 1;
            long procLines = origProcLines;
            int procLimit = pa.GetControlVal("procLimit", 5000000);
            int skipRows = pa.GetControlVal("skipRows", 0);
            if (procLines > procLimit) {
                procLines = procLimit;
            }
            if (skipRows > 0) {
                procLines /= (skipRows + 1);
            }
            Console.WriteLine($"{fileName} , {procLines} of {lineCount - 1} records  would be processed. Skipping {skipRows} rows after processing one.  Possible {origProcLines} rows clamped at {procLimit} ");
            return procLines;
        }

        public static void ConvertTimeZone(DataTable dt, string timezone) {
            var timezoneDesc = TimeZone.GetTimeZoneDescription(timezone);
            var timezoneId = TimeZoneInfo.FindSystemTimeZoneById(timezoneDesc);

            foreach (DataRow row in dt.Rows) {
                var timeStamp = (DateTimeOffset)row["ts"];
                var convertedTs = TimeZoneInfo.ConvertTime(timeStamp, timezoneId);

                row["ts"] = convertedTs;

            }
        }
        private static void PurgeLogOperation(ProcessingArgs pa, ContextData context) {
            //string justShow = pa.GetControlVal("purgeShowOnly", null);
            string purgeDirectory = pa.GetControlVal("purgeDirectory", null);
            pa["purgeLogAgeDays"] = pa.GetControlVal("purgeLogAgeDays", "14");

            context.ContextWrite("PurgeLogOperation", $" daysold={pa["purgeLogAgeDays"] }, purgeDirectory='{purgeDirectory}'");
            if (purgeDirectory.Equals("getcurrentlog")) {
                purgeDirectory = Path.GetDirectoryName(context.ContexFileNamePath);
                pa["purgeDirectory"] = purgeDirectory;
            }
            if (!string.IsNullOrEmpty(purgeDirectory)) {
                FileAttributes attr = File.GetAttributes(purgeDirectory);
                //detect whether its a directory or file
                if ((attr & FileAttributes.Directory) == FileAttributes.Directory) {
                }
                else {
                    purgeDirectory = Path.GetDirectoryName(purgeDirectory);
                }
                context.ContextWrite("PurgeLogOperation", $" actual purgeDirectory='{purgeDirectory}");
                string results = DeleteOlderLogs(pa, context);
                context.ContextWrite("PurgeLogOperation", results);
                Console.WriteLine(results);
                Debug.WriteLine(results);
            }
        }


        private static void GenTemplateSeriesData(ProcessingArgs pa) {
            int counter = 0;
            int procLimit = pa.GetControlVal("procLimit", 10);
            int rseed = pa.GetControlVal("rseed", 999);
            double minutes = pa.GetControlVal("minutes", 5);
            DateTime dtStt = pa.GetControlVal("dtstt", DateTime.Parse("1/1/2017"));
            DateTime dtStp = pa.GetControlVal("dtstp", DateTime.Parse("1/2/2017"));
            string spid = pa.GetControlVal("spid", "36023832");
            string outfile = pa.GetControlVal("outfile", ".\\seriesoutdata.csv");
            string listfile = pa.GetControlVal("listfile", ".\\CollidList.csv");
            Random rand = new Random(rseed);
            string CsvSeriesDataHeaderCols =
                "CIS_EXT_SA_ID,D1_SP_ID,CIS_EXT_SP_ID,D1_UOM_CD,SVC_QTY_IDNTFR_CD,SECONDS_PER_INTERVAL,MSRMT_VAL,MSRMT_DTM_UTC,ESTIMATION_IND_FLG,DESCR100,COMMODITY";
            //"Service_Agreement_ID,MDM_SP_ID,Service_Point_ID,UOM,SQI,Seconds per interval,Measurement Value,DateTime,Estimate Description,Estimate Indicator,Commodity";
            //string seriesRowTemplate = $"90290762,{0},90658614,KWH,,300,{1},2/23/2017,00.05.00,D2NO,No";
            string seriesRowTemplate = "{0},{0},{0},KWH,,{3},{1},{2},D2NO,No,1";
            string outputRecord;
            using (StreamWriter sw = new StreamWriter(outfile)) {
                sw.WriteLine(CsvSeriesDataHeaderCols);
                using (StreamReader sr = new StreamReader(listfile)) {
                    sr.ReadLine();
                    while (!sr.EndOfStream && (procLimit-- > 0)) {
                        var readLine = sr.ReadLine();
                        if (readLine != null) spid = readLine.Trim();
                        DateTime dtOper = dtStt;
                        while (dtOper.CompareTo(dtStp) < 0) {
                            //for (int i = 1; i < 3; i++) {
                            var val = rand.Next(-1000, 1000) / 10.0;
                            //var val = rand.Next(0, 1000) / 10.0;
                            string dateTime = dtOper.ToString("yyyy-MM-ddTHH:mm:ss.fffzz00");
                            outputRecord = string.Format(seriesRowTemplate, spid, val, dateTime, 60 * minutes);
                            if ((counter % 10000) == 0) Console.WriteLine($"{counter}, {outputRecord}");
                            sw.WriteLine(outputRecord);
                            //}
                            dtOper = dtOper.AddMinutes(minutes);
                            counter++;
                        }
                    }
                }
            }


        }

        private static void InsertCassandraDataTestl(string dat, ProcessingArgs pa) {
            string importfile = pa.GetControlVal("importfile", string.Empty);
            if (string.IsNullOrEmpty(importfile)) {
                string msg = "Input file is not provided";
                Debug.WriteLine(msg);
                throw new Exception(msg);
            }
            string clientId = pa.GetControlVal("clientId", "87");
            string procLimit = pa.GetControlVal("procLimit", "5000000");
            procLimit = pa.GetControlVal("insertLimit", procLimit);
            string env = pa.GetControlVal("aclenv", "work");
            string reqVsrc = pa.GetControlVal("reqVsrc", "value_series_meta");
            string altQueryPath = pa.GetClientControlVal(clientId, "consumptionQuery", "importquery");
            //string stg = $"oper=cassimport procLimit=5000000 importfile={tmpFile} doAsyncBatch=1 aclenv=work";
            Debug.WriteLine($" ins 1 AmiAdvancedConnector.InsertCassandraDataTest impContent length={dat?.Length}");
            string stg = $"oper=cassimport clientId={clientId} procLimit={procLimit} importLength={dat?.Length} doAsyncBatch=1 aclenv={env} reqVsrc={reqVsrc} importfile={importfile} doFilterIngestion=0";

            if (!string.IsNullOrEmpty(altQueryPath)) {
                stg = stg + $" altQueryPath={altQueryPath}";
            }
            string[] rVals = TestProcess(stg.Split(' '), dat);
            string sval = "";
            foreach (string rVal in rVals) {
                sval += rVal + "; ";
            }
            Debug.WriteLine($" ins 2 AmiAdvancedConnector.InsertCassandraDataTest impContent rval={sval}");
        }

        private static string ConvertCassandraDataTestl(string dat, ProcessingArgs pa) {
            string clientId = pa.GetControlVal("clientId", "87");
            string procLimit = pa.GetControlVal("procLimit", "5000000");
            procLimit = pa.GetControlVal("convertLimit", procLimit);
            //string stg = $"oper=cassimport procLimit=5000000 importfile={tmpFile} doAsyncBatch=1 aclenv=work";
            Debug.WriteLine($" cvt 1 AmiAdvancedConnector.ConvertCassandraDataTest impContent length={dat?.Length}");
            string stg = $"oper=avpreproc clientId={clientId} procLimit={procLimit} importLength={dat?.Length} doAsyncBatch=1 aclenv=work retContent=retContent";
            string retData = "retContent";
            string[] rVals = ImportProcess(stg.Split(' '), ref retData, dat);
            string sval = "";
            foreach (string rVal in rVals) {
                sval += rVal + "; ";
            }
            Debug.WriteLine($" cvt 2 AmiAdvancedConnector.ConvertCassandraDataTest impContent rval={sval}");
            return retData;
        }

        /// <summary>
        /// convert data from wide format to tall 
        /// </summary>
        /// <param name="dat"></param>
        /// <param name="pa"></param>
        /// <returns></returns>
        private static string ConvertCassandraDataWideToTall(string dat, ProcessingArgs pa) {
            string clientId = pa.GetControlVal("clientId", "87");
            string procLimit = pa.GetControlVal("procLimit", "5000000");
            string aclenv = pa.GetControlVal("aclenv", "work");
            string reqVsrc = pa.GetControlVal("reqVsrc", "value_series_meta");
            procLimit = pa.GetControlVal("convertLimit", procLimit);

            //string stg = $"oper=cassimport procLimit=5000000 importfile={tmpFile} doAsyncBatch=1 aclenv=work";
            Debug.WriteLine($" cvt 1 AmiMidTermConnector.ConvertAmerenCassandraData impContent length={dat?.Length}");
            string stg = $"oper=cvtCsv2Tall clientId={clientId} procLimit={procLimit} importLength={dat?.Length} doAsyncBatch=1 aclenv={aclenv} retContent=retContent  reqVsrc={reqVsrc}";
            string retData = "retContent";
            string[] rVals = ImportProcess(stg.Split(' '), ref retData, dat);
            string sval = "";
            foreach (string rVal in rVals) {
                sval += rVal + "; ";
            }
            Debug.WriteLine($" cvt 2 AmiMidTermConnector.ConvertAmerenCassandraData impContent rval={sval}");
            return retData;
        }

        private static string ConvertCassandraDataTest2(string dat, ProcessingArgs pa) {
            string clientId = pa.GetControlVal("clientId", "87");
            string procLimit = pa.GetControlVal("procLimit", "5000000");
            procLimit = pa.GetControlVal("convertLimit", procLimit);
            //string stg = $"oper=cassimport procLimit=5000000 importfile={tmpFile} doAsyncBatch=1 aclenv=work";
            Debug.WriteLine($" cvt 1 AmiAdvancedConnector.ConvertCassandraDataTest impContent length={dat?.Length}");
            string stg = $"oper=avpreproc cvtType=ameren clientId={clientId} procLimit={procLimit} importLength={dat?.Length} doAsyncBatch=1 aclenv=work retContent=retContent";
            string retData = "retContent";
            string[] rVals = ImportProcess(stg.Split(' '), ref retData, dat);
            string sval = "";
            foreach (string rVal in rVals) {
                sval += rVal + "; ";
            }
            Debug.WriteLine($" cvt 2 AmiAdvancedConnector.ConvertCassandraDataTest impContent rval={sval}");
            return retData;
        }

        /// <summary>
        /// Convert ami csv file to tall format
        /// </summary>
        /// <param name="context"></param>
        /// <param name="dtStt"></param>
        /// <param name="pa"></param>
        private static void ConvertAmiCsvToTall(ContextData context, DateTime dtStt, ProcessingArgs pa) {
            //oper=avpreproc importfile=C:/_Test/Avista/Avista5minSample.csv cvtType=ameren
            ZipManager zm = new ZipManager(pa, context);
            DateTime stest = TimingHelper.RunningTimes(context, dtStt, null, "ampreproc", "start");

            TimingHelper.RunningTimes(context, dtStt, stest, "ampreproc", "initialize");

            Stream referenceStream;
            string referenceSourceFile = pa.GetControlVal("importfile", @"c:\_Test2\SimpleBilling.csv");
            string importContent = pa.GetControlVal("dataContent", "");

            context.ContextWrite("inputFile", referenceSourceFile);
            string outputFile = referenceSourceFile + ".out.csv";
            context.ContextWrite("outputFile", outputFile);

            if (!string.IsNullOrEmpty(importContent)) {
                context.ContextWrite("AmPreProc", "importContent");
                referenceStream = importContent.ToStream();
            }
            else {
                context.ContextWrite("AmPreProc", referenceSourceFile);
                referenceStream = File.OpenRead(referenceSourceFile);
            }

            using (var ms = new MemoryStream()) {
                var referenceLength = referenceStream.Length;
                zm.ConvertStreamToTallAmi(referenceStream, ms);
                context.Add2Counter("cvtRecTallSize", ms.Length);
                context.Add2Counter("cvtRecZipSize", referenceLength);
                context.ContextWrite("ZipManager.ConvertStreamToTallAmi", $"originalsize {referenceLength} tallsize {ms.Length}");

                ms.Flush();
                if ("retContent".Equals(pa.GetControlVal("retContent", "no"))) {
                    pa["retContent"] = Encoding.UTF8.GetString(ms.ToArray(), 0, (int)ms.Length);
                }
                else {
                    FileStream file = new FileStream(outputFile, FileMode.Create, FileAccess.Write);
                    ms.Position = 0;
                    ms.CopyTo(file);
                    file.Close();
                }
            }

            TimingHelper.RunningTimes(context, dtStt, stest, "ampreproc", "end");
        }

        private static void AvPreProc(ContextData context, DateTime dtStt, ProcessingArgs pa) {
            //oper=avpreproc importfile=C:/_Test/Avista/Avista5minSample.csv
            DateTime stest = TimingHelper.RunningTimes(context, dtStt, null, "avpreproc", "start");
            SeriesCollectionSpecification collSpec = new SeriesCollectionSpecification(pa);
            TimingHelper.RunningTimes(context, dtStt, stest, "avpreproc", "initialize");
            collSpec.Transform2SeriesImport(pa, context);
            TimingHelper.RunningTimes(context, dtStt, stest, "avpreproc", "end");
        }


        private static string importFileName = "C:\\junk\\DevWt1\\seriesvalues_144_5_10000_0201.csv";
        private static string cassImportQuery = "(id, did, ts, val, meta)   values({0},{1},'{2}',{3},{5})";


        static void CassImportProcessing(ContextData contextData, ProcessingArgs pa) {
            contextData.ContextWrite("CassImportProcessing", "begin");

            // set environment config setting
            importFileName = pa.GetControlVal("importfile", importFileName);
            BatchUtils.EstablishIntervalEnvironment(pa, importFileName);
            pa["procEnvTag"] = pa.GetControlVal("aclenv", "work");
            string procTableTag = pa["procTableTag"];
            string procEnvTag = pa["procEnvTag"];
            string procAltTag = "5";
            pa["reqVanal"] = "batch";
            string usenv = pa.GetControlVal("aclenv", "work");
            string tSel = pa.GetControlVal("qSel0", "");
            string clientId = pa.GetControlVal("clientId", "87");
            if (string.IsNullOrEmpty(tSel)) {
                string altQryPath = pa.GetClientControlVal(clientId, "consumptionQuery", "importquery");
                pa["qSel"] = pa.GetControlVal($"{usenv}{altQryPath}", cassImportQuery);
                pa["qSel0"] = pa["qSel"];
            }
            else {
                pa["qSel"] = tSel;
            }

            string transCsvMap = pa.GetClientControlVal(clientId, "transcsvMap", "transcsv");
            pa["dTransCsv"] = pa.GetControlVal($"{usenv}{transCsvMap}", "");
            pa["cassquery"] = pa.GetControlVal("importquery", cassImportQuery);
            pa["fiOper0"] = importFileName;

            ProcessingArgs.AccumulateRequestPars(procTableTag, procEnvTag, procAltTag, pa);

            BatchUtils.AnalysisBatch(contextData, pa, null, null);
            contextData.ContextWrite("CassImportProcessing", "end");
        }



        private static void FilesProcessingOld(ProcessingArgs pa) {
            int counter = 1;
            double val;
            string dataPath = pa["outputPath"];
            Random rand = new Random(100);
            string headerCols = "id,ds,ts,val,qual,meta";


            double minutes;
            if (!Double.TryParse(pa.GetControlVal("minutes", 5).ToString(), out minutes)) {
                throw new Exception("Unable to convert control value minutes to double");
            }
            int qualControl = pa.GetControlVal("qualcontrol", -1);
            int numSeries = pa.GetControlVal("numseries", 1000);
            int seriesStart = pa.GetControlVal("seriesstart", 1);
            int qualReps = pa.GetControlVal("qualreps", 1);

            DateTime dtStt = pa.GetControlVal("dtstt", DateTime.Parse("1/1/2017"));
            DateTime dtStp = pa.GetControlVal("dtstp", DateTime.Parse("1/2/2017"));

            double hours = dtStp.Subtract(dtStt).TotalHours;

            DateTime dtOper = dtStt;
            string meta = "(*( 'm1':'{0}' (*)'m2':'{1}' )*) ";
            string fileName = $"seriesvalues_{hours}_{minutes}_{numSeries}_{dtStt:yyyyMMdd}_{dtStp:yyyyMMdd}.csv";
            string outputFile = Path.Combine(dataPath, fileName);
            using (StreamWriter sw = new StreamWriter(outputFile)) {
                sw.WriteLine(headerCols);
                for (int seriesId = seriesStart; seriesId < (seriesStart + numSeries); seriesId++) {
                    dtOper = dtStt;
                    while (dtOper.CompareTo(dtStp) < 0) {
                        val = rand.Next(0, 1000) / 10.0;
                        string tstg = dtOper.ToString(ContextData.StandardTimeFormat);
                        for (int qr = 1; qr <= qualReps; qr++) {
                            string metaVal = string.Format(meta, seriesId, qr);
                            int qualifier = counter;
                            if (qualControl >= 0) {
                                qualifier *= qr;
                            }
                            string outputRecord = $"{seriesId},{tstg},{tstg},{val},{qualifier},{metaVal}";
                            if ((counter % 1000) == 0) Console.WriteLine(outputRecord);
                            sw.WriteLine(outputRecord);
                        }
                        dtOper = dtOper.AddMinutes(minutes);
                        counter++;
                    }
                }
            }

            Console.WriteLine($" count={counter} last={dtOper.ToString(CultureInfo.InvariantCulture)}");
        }


        private static void EventsProcessing(ProcessingArgs pa) {
            string env = pa["env"] ?? "work";
            SimpleEventProcessor.Init(pa, env);
            Console.CancelKeyPress += myHandler;
            if (pa["infoTimeSecs"] != null) int.TryParse(pa["infoTimeSecs"], out _infoTimeSecs);
            if (pa["infoTimeMins"] != null) int.TryParse(pa["infoTimeMins"], out _infoTimeMins);


            string eventHubConnectionString = pa[env + ".hubCxn"];
            string eventHubName = "aclaoimportdataeh" + env;
            string storageConnectionString = pa[env + ".storeCxn"];
            //string storageAccountName = "aclaoaclaraonestg" + env;
            //string storageAccountKey = pa[env + ".storeAcctKey"];
            //string storageConnectionString = $"DefaultEndpointsProtocol=https;AccountName={storageAccountName};AccountKey={storageAccountKey}";

            //SimpleEventProcessor.LogStg2($"env={env}, eventHubName={eventHubName}, storageAccountName={storageAccountName} ");
            SimpleEventProcessor.LogStg2($"env={env}, eventHubName={eventHubName} ");

            string eventProcessorHostName = Guid.NewGuid().ToString();
            EventProcessorHost eventProcessorHost = new EventProcessorHost(eventProcessorHostName, eventHubName,
                EventHubConsumerGroup.DefaultGroupName, eventHubConnectionString, storageConnectionString);
            SimpleEventProcessor.LogStg2("Registering EventProcessor...");
            var options = new EventProcessorOptions();
            options.ExceptionReceived += (sender, e) => { Console.WriteLine(e.Exception); };
            eventProcessorHost.RegisterEventProcessorAsync<SimpleEventProcessor>(options).Wait();

            SimpleEventProcessor.LogStg2("Receiving. cntl-break to end");
            DateTime beginTime = DateTime.Now;
            DateTime nextInfoTime = DateTime.Now.AddSeconds(_infoTimeSecs);
            DateTime nextLogTime = DateTime.Now.AddMinutes(_infoTimeMins);
            while (!_done) {
                Thread.Sleep(1000);
                if (DateTime.Now > nextInfoTime) {
                    nextInfoTime = DateTime.Now.AddSeconds(_infoTimeSecs);
                    SimpleEventProcessor.ShowInfo(
                        $"EventsConsole.running env={env}, for secs={DateTime.Now.Subtract(beginTime).TotalSeconds}");
                }
                if (DateTime.Now > nextLogTime) {
                    nextLogTime = DateTime.Now.AddMinutes(_infoTimeMins);
                    SimpleEventProcessor.ShowLog();
                }
            }
            SimpleEventProcessor.LogStg2("\n Shutting down... \n");
            SimpleEventProcessor.ShowLog();
            eventProcessorHost.UnregisterEventProcessorAsync().Wait();
            SimpleEventProcessor.ShowLog();
            SimpleEventProcessor.FlushData();
            Thread.Sleep(2000);
            SimpleEventProcessor.FlushData();
        }


        protected static void myHandler(object sender, ConsoleCancelEventArgs args) {
            Console.WriteLine("  Key pressed: {0}", args.SpecialKey);
            if (args.SpecialKey == ConsoleSpecialKey.ControlBreak) {
                _done = true;
                args.Cancel = true;
            }
            else {
                Console.WriteLine("press cntl-break to quit");
                SimpleEventProcessor.ShowLog();
                args.Cancel = true;
            }
        }


        //private static void MeterGenerator(ProcessingArgs pa, StreamWriter sw, DataRow dataRow) {

        //}

        //private static void SeriesGenerator(ProcessingArgs pa, StreamWriter sw, DataRow dataRow) {

        //}


    }


    internal class SimpleEventProcessor : IEventProcessor {

        public static string Mode { get; set; }
        private static string _partitionOperationsLog = "";
        private static int _partitionsOpenedCount;
        private static int _partitionsClosedCount;
        private static StreamWriter _logStream;

        private static int _consFull = 100000;
        private static int _consInfo = 10000;
        private static bool _doCheckpoint;

        // watch it --= assumes 32
        private static readonly int[] PartMessages = {
            0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
            0, 0, 0, 0, 0, 0, 0
        };

        private static string _environ = "na";

        /// <summary>
        /// DO THIS BEFORE USE -- static initialization of logging, etc.
        /// </summary>
        /// <param name="pa"></param>
        /// <param name="env"></param>
        public static void Init(ProcessingArgs pa, string env) {
            _environ = env;
            if (pa["consFull"] != null) int.TryParse(pa["consFull"], out _consFull);
            if (pa["consInfo"] != null) int.TryParse(pa["consInfo"], out _consInfo);
            if (pa["doCheckpoint"] != null) _doCheckpoint = pa["doCheckpoint"].Equals("1");

            string fName =
                $"evConsLog_{DateTime.Now.Month}_{DateTime.Now.Day}_{DateTime.Now.Hour}_{DateTime.Now.Minute}_{DateTime.Now.Second}_{_environ}.txt";
            _logStream = new StreamWriter(fName);
            LogStg2($"SimpleEventProcessor, now={DateTime.Now}, name={fName}");
        }

        private static void LogStg(string stg) {
            lock (_logStream) {
                _logStream.WriteLine(stg);
            }
        }

        public static void LogStg2(string stg) {
            LogStg(stg);
            Console.WriteLine(stg);
        }

        public static void FlushData() {
            lock (_logStream) {
                _logStream.Flush();
            }
        }

        public static void ShowInfo(string info) {
            LogStg2($"at '{info}', total messages ={_totalMsgsCounter}  total rx Size = {_totalMsgsSize}");

        }


        public static void ShowLog() {
            LogStg2(
                $"\npartitions opened={_partitionsOpenedCount} closed={_partitionsClosedCount},  checkpoint={_doCheckpoint} now={DateTime.Now}");
            LogStg2("\n partitionsLog: \n" + _partitionOperationsLog);
            for (int i = 0; i < PartMessages.Length; i++) {
                LogStg2($"partitions[{i}]={PartMessages[i]}");
            }
            LogStg2($"total messages ={_totalMsgsCounter}");
            FlushData();
        }

        Stopwatch _checkpointStopWatch;

        async Task IEventProcessor.CloseAsync(PartitionContext context, CloseReason reason) {
            string stg =
                $"Processor Shutting Down. Partition: '{context.Lease.PartitionId}', Reason: '{reason}'', time={DateTime.Now}  checkpoint={_doCheckpoint}";
            _partitionOperationsLog += stg + "\n";
            LogStg2(stg);
            _partitionsClosedCount++;
            if (reason == CloseReason.Shutdown) {
                if (_doCheckpoint) {
                    await context.CheckpointAsync();
                }
            }
        }

        Task IEventProcessor.OpenAsync(PartitionContext context) {
            string stg =
                $"SimpleEventProcessor initialized.  Partition: '{context.Lease.PartitionId}', Offset: '{context.Lease.Offset}',  checkpoint={_doCheckpoint} time={DateTime.Now}";
            _partitionOperationsLog += stg + "\n";
            LogStg2(stg);
            _partitionsOpenedCount++;
            _checkpointStopWatch = new Stopwatch();
            _checkpointStopWatch.Start();
            return Task.FromResult<object>(null);
        }

        static int _totalMsgsCounter;
        static long _totalMsgsSize;

        async Task IEventProcessor.ProcessEventsAsync(PartitionContext context, IEnumerable<EventData> messages) {
            foreach (EventData eventData in messages) {
                PartMessages[int.Parse(context.Lease.PartitionId)] += 1;
                _totalMsgsSize += eventData.SerializedSizeInBytes;
                LogStg(
                    $"Message received. Count {_totalMsgsCounter} totalsize {_totalMsgsSize} Partition: '{context.Lease.PartitionId}', Data: '{Encoding.UTF8.GetString(eventData.GetBytes())}'");
                if (_totalMsgsCounter % _consFull == 0) {
                    string data = Encoding.UTF8.GetString(eventData.GetBytes());
                    Console.WriteLine(
                        $"Message received. Count {_totalMsgsCounter}  Partition: '{context.Lease.PartitionId}', Data: '{data}'");
                }
                else {
                    if (_totalMsgsCounter % _consInfo == 0) {
                        Console.WriteLine($"[{_totalMsgsCounter},{context.Lease.PartitionId},{eventData.SerializedSizeInBytes}]");
                    }
                }
                _totalMsgsCounter++;
            }

            //Call checkpoint every 5 minutes, so that worker can resume processing from 5 minutes back if it restarts.
            if (_checkpointStopWatch.Elapsed > TimeSpan.FromMinutes(5)) {
                string stg =
                    $"SimpleEventProcessor CheckpointAsync.  Partition: '{context.Lease.PartitionId}', time={DateTime.Now} checkpoint={_doCheckpoint}";
                _partitionOperationsLog += stg + "\n";
                LogStg2(stg);
                if (_doCheckpoint) {
                    await context.CheckpointAsync();
                }
                _checkpointStopWatch.Restart();
            }
        }
    }

    public delegate void DLogMessage(string msg);

    public abstract class SimpleWebServer {
        private static DLogMessage _logFunction;
        public virtual string WhoamiStg() {
            return "[I am no SimpleWebServer]";
        }

        private static void LocalLogMsg(String msg) {
            _logFunction?.Invoke(msg);
        }

        public virtual void Logit(string msg) {
            Console.WriteLine(msg);
        }

        public SimpleWebServer(ProcessingArgs pa) {
            Begin(pa);
        }

        public void Begin(ProcessingArgs pa) {
            //CheckEndpoints(pa);
        }

        public void SetLogFn(DLogMessage logger) {
            _logFunction = logger;
        }

        public delegate string[] SendResponseFn(HttpListenerRequest request);


        /// <summary>
        /// Spin up the web endpoint
        /// </summary>
        /// <param name="pa"></param>
        /// <param name="handler"></param>
        //public string InitializeServer(ProcessingArgs pa, SendResponseFn handler = null) {
        public string InitializeServer(ProcessingArgs pa, System.Func<System.Net.HttpListenerRequest, string[]> handler = null) {
            string hostIp = "127.0.0.1";
            string port = "4443";
            string prefixesUrl = "nada";
            hostIp = pa.GetControlVal("hostIp", hostIp);
            port = pa.GetControlVal("port", port);
            if (!HttpListener.IsSupported) {
                Logit(
                    $"Event Process Worker NOT SUPPORTED a SimpleWebServer.InitializeServer NOT SUPPORTED for IP:{hostIp}, Port:{port}");
            }
            else {
                Logit($"Event Process Worker SimpleWebServer.InitializeServer starting at public IP:{hostIp}, Port:{port}");
                prefixesUrl = $"https://{hostIp}:{port}/";
                Logit($"Event Process Worker SimpleWebServer.InitializeServer encoding prefix {prefixesUrl}");
                string overrideAmiPrefix = pa.GetControlVal("overrideAMIPrefix", "");
                if (!string.IsNullOrEmpty(overrideAmiPrefix)) {
                    Logit($"Event Process Worker SimpleWebServer.InitializeServer override prefix with {overrideAmiPrefix}");
                    prefixesUrl = overrideAmiPrefix;
                }
                Console.WriteLine($"Event Process Worker SimpleWebServer.InitializeServer adding prefix {prefixesUrl}", null);
                _listener = new HttpListener();
                _listener.Prefixes.Add(prefixesUrl);
                _responderMethod = handler ?? SendResponse;
                //_responderMethod = SendResponse;
                _listener.Start();
                StartWebServer();
                Logit($"Event Process Worker SimpleWebServer.InitializeServer started");

                //ws.Stop();
            }
            return prefixesUrl;
        }



        private HttpListener _listener;
        private Func<HttpListenerRequest, string[]> _responderMethod;

        Thread _webServerThread;
        private string myTestId = "weblisten";

        public void StartWebServer() {
            _webServerThread = new Thread(ThreadRoutine);
            _webServerThread.Name = "TestThread:" + myTestId;
            _webServerThread.IsBackground = true;
            _webServerThread.Start();
        }


        public void ThreadRoutine() {
            try {
                Run();
            }
            catch (ThreadAbortException exc) {
                int dbgBreak = 1;
                if (dbgBreak > 1) {
                    Console.WriteLine(exc.Message);
                }
            }
            catch (Exception exc) {
                Debug.WriteLine($"TestThread ThreadRoutine exception {exc.Message}, {exc.InnerException?.Message},\n{exc.StackTrace}");
            }
        }

        //pa["reqVsrc"] = vsrc;
        //    pa["reqVenv"] = venv;
        //    pa["reqVval"] = vval;

        public static string[] SendResponse(HttpListenerRequest request) {
            //string respBody = "nada";
            string reqBody = "nothing=nada";
            ProcessingArgs pa = new ProcessingArgs();
            string vsrc = pa.GetControlVal("reqVsrc", "evc");
            ProcessingArgs.AccumulateRequestPars(vsrc, pa["aclenv"], "none", pa);
            //pa["requireLogForce"] = "yes";  // start off assuming we don't want logging unless forced (better performance this way)
            string[] respArr = new string[2];
            respArr[0] = "nothing=nada";
            respArr[1] = "text/csv";
            try {
                string baseUrl = request.RawUrl;
                if (baseUrl.IndexOf('?') >= 0) {
                    baseUrl = baseUrl.Substring(0, baseUrl.IndexOf('?'));
                }
                string[] urlParts = baseUrl.Split(new[] { '/' }, StringSplitOptions.RemoveEmptyEntries);

                pa["restVerb"] = request.HttpMethod;
                pa["restContType"] = request.ContentType;
                for (int i = 0; i < urlParts.Length; i++) {
                    switch (i) {
                        case 0:
                            pa["reqVsrc"] = urlParts[i];
                            pa["restRoot"] = urlParts[i];
                            break;
                        case 1:
                            pa["reqVenv"] = urlParts[i];
                            pa["restVersion"] = urlParts[i];
                            break;
                        case 2:
                            pa["reqVval"] = urlParts[i];
                            pa["restResource"] = urlParts[i];
                            break;
                        case 3:
                            pa[$"reqVPar{i}"] = urlParts[i];
                            pa["restResId"] = urlParts[i];
                            break;
                        default:
                            pa[$"reqVPar{i}"] = urlParts[i];
                            break;
                    }
                }
                ProcessingArgs.AccumulateRequestPars(request, pa);
                if (pa.GetControlVal("requireLogForce", "nope").Equals("nope")) { // if not overridden via config
                    pa["requireLogForce"] = "yes"; // start off assuming we don't want logging unless forced (better performance this way)
                }

                if (pa.GetControlVal("reqVPar3", "nope").Equals("body") || pa.GetControlVal("allowBodyOverride", "nope").Equals("yes")) {  // initialize params from the body payload.
                    if (pa["reqContent"] != null) {
                        string varStg;
                        if (pa["reqContent"].Length > 8192) {
                            varStg = pa["reqContent"].Substring(0, 8192);
                            pa.Override(varStg);
                        }
                        else {
                            pa.Override(pa["reqContent"]);
                        }
                    }
                }
                string[] rVals = ServicesBroker.TestProcess(pa);
                respArr[0] = "no data back from ServicesBroker";
                if (rVals.Length > 0) {
                    respArr[0] = rVals[rVals.Length - 1];
                    //if (pa.GetControlVal("Accept", "nada").ToLower().Equals("application/json")) {
                    //    respArr[1] = "application/json";
                    //}
                    respArr[1] = pa.GetControlVal("Accept", "text/plain");
                }
                //LocalLogMsg($" got request {reqBody}");
            }
            catch (Exception exc) {
                respArr[0] = $"exception for '{reqBody}' SimpleWebServer SendResponse {exc.Message}, {exc.InnerException?.Message}";
                LocalLogMsg($" {respArr[0]}");
            }
            return respArr;
        }

        public void Run() {
            ThreadPool.QueueUserWorkItem((o) => {
                Console.WriteLine("Webserver running...");
                while (_listener.IsListening) {
                    try {
                        ThreadPool.QueueUserWorkItem((c) => {
                            var ctx = c as HttpListenerContext;

                            if (ctx == null)
                                throw new Exception("HttpListenerContext is null");
                            string[] rstr = _responderMethod(ctx.Request);
                            if (!string.IsNullOrEmpty(rstr[0])) {
                                byte[] buf = Encoding.UTF8.GetBytes(rstr[0]);
                                ctx.Response.ContentType = rstr[1];
                                ctx.Response.ContentLength64 = buf.Length;
                                ctx.Response.OutputStream.Write(buf, 0, buf.Length);
                            }
                            else {
                                byte[] buf = Encoding.UTF8.GetBytes(" null response from web responder ");
                                ctx.Response.ContentLength64 = buf.Length;
                                ctx.Response.OutputStream.Write(buf, 0, buf.Length);
                            }
                        }, _listener.GetContext());
                    }
                    catch (Exception exc) {
                        Trace.WriteLine($" SimpleWebServer.Run exception {exc.Message}, {exc.InnerException?.Message}");
                    }
                }

            });
        }

        public void Stop() {
            _listener.Stop();
            _listener.Close();
        }
    }


    class SomeTests {

        public static void TimeTests() {
            DateTime thisDate = new DateTime(2007, 3, 10, 0, 0, 0);
            DateTime dstDate = new DateTime(2007, 6, 10, 0, 0, 0);
            DateTimeOffset thisTime;

            thisTime = new DateTimeOffset(dstDate, new TimeSpan(-7, 0, 0));
            ShowPossibleTimeZones(thisTime);

            thisTime = new DateTimeOffset(thisDate, new TimeSpan(-6, 0, 0));
            ShowPossibleTimeZones(thisTime);

            thisTime = new DateTimeOffset(thisDate, new TimeSpan(+1, 0, 0));
            ShowPossibleTimeZones(thisTime);
        }

        private static void ShowPossibleTimeZones(DateTimeOffset offsetTime) {
            TimeSpan offset = offsetTime.Offset;
            ReadOnlyCollection<TimeZoneInfo> timeZones;

            Console.WriteLine("{0} could belong to the following time zones:",
                offsetTime.ToString());
            // Get all time zones defined on local system
            timeZones = TimeZoneInfo.GetSystemTimeZones();
            // Iterate time zones 
            foreach (TimeZoneInfo timeZone in timeZones) {
                // Compare offset with offset for that date in that time zone
                if (timeZone.GetUtcOffset(offsetTime.DateTime).Equals(offset))
                    Console.WriteLine("   {0}", timeZone.DisplayName);
            }
            Console.WriteLine();
        }
    }


}
