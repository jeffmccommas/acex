﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Xml;
using Cassandra;
using Newtonsoft.Json;
using Microsoft.VisualBasic.FileIO;

namespace AO.TimeSeriesProcessing {


    public class AnalysisUtils {

        public const string StandardTimeFormatBaseTemp = "yyyy-MM-ddTHH:mm:ss{0}";


        public static DataTable GetDataTableFromCsv(string csv_file_path, bool hasHeaders) {
            DataTable csvData = new DataTable();

            try {

                using (TextFieldParser csvReader = new TextFieldParser(csv_file_path)) {
                    csvReader.SetDelimiters(new string[] { "," });
                    csvReader.HasFieldsEnclosedInQuotes = false;
                    string[] colFields = csvReader.ReadFields();
                    foreach (string column in colFields) {
                        DataColumn datecolumn = new DataColumn(column);
                        datecolumn.AllowDBNull = true;
                        csvData.Columns.Add(datecolumn);
                    }

                    while (!csvReader.EndOfData) {
                        string[] fieldData = csvReader.ReadFields();
                        //Making empty value as null
                        for (int i = 0; i < fieldData.Length; i++) {
                            if (fieldData[i] == "") {
                                fieldData[i] = null;
                            }
                        }
                        csvData.Rows.Add(fieldData);
                    }
                }
            }
            catch (Exception ex) {
            }
            return csvData;
        }



        /// <summary>
        /// do validation analysis 
        /// </summary>
        /// <param name="contextData"></param>
        /// <param name="pa"></param>
        /// <param name="request"></param>
        /// <param name="response"></param>
        public static void AnalysisValidate(ContextData contextData, ProcessingArgs pa, HttpRequestMessage request, HttpResponseMessage response) {
            //string qryTemplate = "where client_id={0} and type in ('A','S') and customer_id = '{1}' and account_id = '{2}' ";
            List<DataTable> queryResultTables = new List<DataTable> { };
            DataUtils1.LoadQueryData(contextData, pa, queryResultTables, false);

            string _myEtag = (!string.IsNullOrEmpty(pa["myEtag"])) ? pa["myEtag"] : "";

            if (!string.IsNullOrEmpty(pa["procResp"]) && (pa["procResp"].ToLower() != "0")) { // process data and write
                contextData.ContextWrite("webPath", pa["outputWeb"]);
                contextData.ContextWrite("etag", _myEtag);
                contextData.ContextWrite("RequestUri", request.RequestUri.ToString());
                contextData.ContextWrite("RequestHeaders", request.Headers);
                contextData.ContextWriteBulk("RequestContent", pa["reqContent"]);
            }
            int expectRows = 1;
            if (pa["expectRows"] != null) int.TryParse(pa["expectRows"], out expectRows);
            string referenceSourceFile = @"c:\_Test\AclValidateSrc.csv";
            referenceSourceFile = pa.GetControlVal("fiOper0", referenceSourceFile);
            referenceSourceFile = pa.GetControlVal("useRefFile", referenceSourceFile); // maybe pass in an override to the script
            referenceSourceFile = referenceSourceFile.Trim();
            BatchUtils.EstablishIntervalEnvironment(pa, referenceSourceFile);
            string useEnv = pa.GetControlVal("aclEnv", "perfd");
            bool doParallel = pa.GetControlVal("doParallelVal", "no").Equals("yes");
            if (!string.IsNullOrEmpty(referenceSourceFile)) {
                FileAttributes attr = File.GetAttributes(referenceSourceFile);
                if (!doParallel && attr.HasFlag(FileAttributes.Directory)) { // directory == get list of CSV files
                    string srcFile = BatchUtils.ConsolidateFiles(contextData, ref referenceSourceFile, pa);
                    if (pa.GetControlVal("useConsolidatedData", "no").Equals("yes") && !string.IsNullOrEmpty(srcFile)) {
                        queryResultTables.Add(GetDataTableFromCsv(srcFile, true));
                    }
                    else {
                        return;
                    }
                }
            }
            if (string.IsNullOrEmpty(pa["reqVsrc"]) || referenceSourceFile.Contains("ami")) {  // dealing with the bias with AMI
                pa["reqVsrc"] = pa.GetClientControlVal(pa.GetControlVal("clientId", "87"), "consumptionTableName", "");
            }
            //referenceSourceFile = pa.GetControlVal("useRefFile", referenceSourceFile); // maybe pass in an override to the script
            string matchType = "exact";
            if (!string.IsNullOrEmpty(pa["matchType"]?.Trim())) {
                matchType = pa["matchType"].Trim();
            }
            CassConn1 cc1 = new CassConn1();
            cc1.Connect(pa);


            DateTime dtStart = DateTime.Now;
            contextData?.ContextWrite("analSource", $"using {referenceSourceFile} rows ");
            contextData?.ContextWrite("analStart", $"expecting {expectRows} rows ", dtStart.ToString());
            int matchMiss = 0;
            int matchFound = 0;
            int readRows = 0;
            int missOutputLimit = pa.GetControlVal("missOutputLimit", 5000);
            int missAbortLimit = pa.GetControlVal("missAbortLimit", 50000);

            int ctxLines = 25;
            int ctxLinesMax = ctxLines;
            int tryLines;
            if (pa["contextRecs"] != null) {
                if (int.TryParse(pa["contextRecs"], out tryLines)) {
                    ctxLines = tryLines;
                    ctxLinesMax = ctxLines;
                }
            }
            int procLimit = 100;
            if (pa["procLimit"] != null) {
                if (int.TryParse(pa["procLimit"], out tryLines)) {
                    procLimit = tryLines;
                }
            }


            int inputLimit = procLimit;
            if (pa["inputLimit"] != null) {
                if (int.TryParse(pa["inputLimit"], out tryLines)) {
                    inputLimit = tryLines;
                }
            }

            bool stripCsvQuotes = string.Equals(pa.GetControlVal("stripCsvQ", "no"), "yes");


            int skipRefRows = 0;
            if (pa["skipRows"] != null) {
                if (int.TryParse(pa["skipRows"], out tryLines)) {
                    skipRefRows = tryLines;
                }
            }
            string clientId = pa.GetControlVal("clientId", "");
            string sqlMeterMgtDatabase = pa.GetControlVal($"{clientId}sqlMeterMgtDatabasename", "InsightsDW_Test");
            if (pa["sqlMeterMgtCxnStg"] == null) {
                string sqlMeterMgt = ConfigurationManager.AppSettings.Get("sqlMeterMgt");
                pa["sqlMeterMgtCxnStg"] = String.Format(sqlMeterMgt, sqlMeterMgtDatabase);
            }

            if (pa["dodbtest"] != null) {

                string sqlMeterMgt = String.Format(ConfigurationManager.AppSettings.Get("sqlMeterMgt"), sqlMeterMgtDatabase);

                using (SqlConnection con = new SqlConnection(sqlMeterMgt)) {
                    con.Open();
                    using (SqlCommand command = new SqlCommand("SELECT TOP 2 * FROM ZTestTable1", con)) {
                        using (SqlDataReader reader = command.ExecuteReader()) {
                            while (reader.Read()) {
                                Console.WriteLine("{0} {1} {2}",
                                    reader.GetInt32(0), reader.GetString(1), reader.GetString(2));
                            }
                        }
                    }
                }

                NameValueCollection nv = new NameValueCollection();
                nv.Add("noData", "expected");
                XmlDocument doc = XmlDocUtils.GetXMLExceptionDocument("DataTest", nv);
                DataUtils1.PrepareResponseInfo(queryResultTables, pa, request, response, null, contextData);
                response.Content = new StringContent(doc.OuterXml);
            }


            try {
                contextData.Add2Counter("autoFix", 0);  // make sure shows up even if no data
                contextData.Add2Counter("noFix", 0);  // make sure shows up even if no data
                string mapAccumDataCols = pa.GetControlVal("mapAccumDataCols", "");
                int[] mapDataIndices = null;
                if (!string.IsNullOrEmpty(mapAccumDataCols)) {
                    string[] mapCols = mapAccumDataCols.Split(new[] { ',' }, StringSplitOptions.None);
                    mapDataIndices = new int[mapCols.Length];
                    for (int i = 0; i < mapDataIndices.Length; i++) {
                        mapDataIndices[i] = int.Parse(mapCols[i]);
                    }
                }
                bool startedOutput = false;
                bool storingData = false;
                bool storedOriginalTable = false;
                if (File.Exists(referenceSourceFile)) using (StreamReader referenceStream = new StreamReader(referenceSourceFile)) {
                    //string csvNames = referenceStream.ReadLine();
                    //contextData.NoFixes.AddLine(csvNames);
                    //contextData.AutoFixes.AddLine(csvNames);
                    int maxNofixRows = pa.GetControlVal("maxNofixRows", 1000);
                    string overrideQry = pa.GetControlVal("overrideQry", "");
                    if (!string.IsNullOrEmpty(overrideQry)) {
                        overrideQry = pa.GetControlVal(overrideQry, "");
                        if (!string.IsNullOrEmpty(overrideQry)) {
                            pa["qSel0"] = overrideQry;
                        }
                    }
                    string overrideHelper = pa.GetControlVal("overrideHelper", "");
                    if (!string.IsNullOrEmpty(overrideHelper)) {
                        pa["heOper0"] = overrideHelper;
                    }

                    string qryFmtSave = pa["qSel0"];  // we need at least one but this is just a minimal proposition.  A different query might be selected to replace it
                    if (!string.IsNullOrEmpty(qryFmtSave)) {

                        if (contextData != null) {
                            contextData.ContextWrite("val_file", contextData.Validation.FileName);
                            contextData.ContextWrite("val_web", pa["outputWeb"] + Path.GetFileName(contextData.Validation.FileName));
                        }
                        DataTable accumulatorTable = new DataTable("getCassdata");
                        string destTimeZoneString = pa.GetControlVal("destTimeZoneString", "Central Standard Time");
                        TimeZoneInfo tzi = TimeZoneInfo.FindSystemTimeZoneById(destTimeZoneString);
                        int validateTimeShiftCol = -1;
                        if (referenceSourceFile.Contains("ami")) {  // assuming that only AMI has time shifted data.  
                            var validateTimeShiftColKey = pa.GetControlVal($"{clientId}validateTimeShiftColKey", "validateTimeShiftCol");
                            validateTimeShiftCol = pa.GetControlVal($"{useEnv}{validateTimeShiftColKey}", -1);
                        }
                        string tsTimeZoneInfo = pa.GetControlVal("tsTimeZoneInfo", "Z");
                        string standardTimeFormatBase = string.Format(StandardTimeFormatBaseTemp, tsTimeZoneInfo);
                        TimeSpan tsZero = new TimeSpan();

                        if ((pa["qSel0"] != null) && (pa["qSel0"].Trim() != "*")) {
                            while (!referenceStream.EndOfStream && (inputLimit-- > 0)) {
                                string csvStg = referenceStream.ReadLine();
                                if (!char.IsDigit(csvStg[0])) {
                                    contextData.NoFixes.AddLine(csvStg);
                                    contextData.AutoFixes.AddLine(csvStg);
                                }
                                if (stripCsvQuotes) {
                                    csvStg = csvStg.Replace("\"", "");
                                }

                                contextData.IncrementCounter("RefReadRows");
                                try {
                                    string[] csvVals = csvStg.Split(',');
                                    string qryFmt = qryFmtSave; // reset
                                    if (!string.IsNullOrEmpty(pa["inOper0"])) {
                                        // figure out if we need to replace the base query.
                                        string[] indirectPars = pa["inOper0"].Trim().Split(' ');
                                        if (indirectPars.Length < 3) break; // src is form "1 has col1=aa"  or "1 not col1=,"
                                        string[] compVars = indirectPars[2].Split('=');
                                        string rValue = ""; // assume blank for rValue

                                        string lValue = csvVals[0]; // assume blank for rValue

                                        if (compVars.Length > 1) {
                                            rValue = compVars[1];
                                        }
                                        int tVal = 0;
                                        if (int.TryParse(compVars[0], out tVal)) {
                                            lValue = csvVals[tVal];
                                        }
                                        if (lValue.Equals(rValue)) {
                                            // add additional operators here
                                            if (int.TryParse(indirectPars[0], out tVal)) {
                                                string qSelector = $"qSel{tVal}";
                                                if (!string.IsNullOrEmpty(pa[qSelector])) {
                                                    // selected a different query than the default
                                                    qryFmt = pa[qSelector];
                                                }
                                            }
                                        }
                                    }

                                    if (validateTimeShiftCol >= 0) {
                                        DateTime dtSttq = DateTime.Parse(csvVals[validateTimeShiftCol]);
                                        DateTimeOffset dt = new DateTimeOffset(dtSttq.Year, dtSttq.Month, dtSttq.Day, dtSttq.Hour, dtSttq.Minute, dtSttq.Second, tzi.GetUtcOffset(dtSttq));
                                        TimeSpan tzOffset = tzi.GetUtcOffset(dt);
                                        DateTimeOffset dtos = dt.AddHours(-tzOffset.Hours);
                                        DateTimeOffset dtoUtc = new DateTimeOffset(dtos.Year, dtos.Month, dtos.Day, dtos.Hour, dtos.Minute, dtos.Second, tsZero);
                                        csvVals[validateTimeShiftCol] = dtoUtc.ToString(standardTimeFormatBase);
                                    }

                                    ValidateContexts ctxs = ValidateContexts.GetQuerySet(pa, qryFmt, csvVals);
                                    foreach (ValidateContext ctx in ctxs) {
                                        pa["qSel0"] = ctx.QryString;
                                        string getRows = pa.GetControlVal("qRows", "*");
                                        if (getRows.Equals("*")) pa["qRows"] = "*"; // at some point be selective
                                        string cassandraQuery = "";
                                        RowSet rs = cc1.PerformCasQuery(pa, ref cassandraQuery);

                                        int retRowsCnt = rs.GetAvailableWithoutFetching();
                                        string sqlResults = "";
                                        if (pa.GetControlVal("qryFrag", 0) == 1) {
                                            int sqlCount = CassConn1.SqlCount(contextData, pa, rs, 1);
                                            if (sqlCount != retRowsCnt) {
                                                sqlResults = $"[sql={retRowsCnt}->{sqlCount}]";
                                            }
                                            retRowsCnt = sqlCount;
                                        }

                                        bool fail = false;
                                        switch (matchType) {
                                            case "get": // results being stored.
                                                storingData = true;
                                                break;
                                            case "min":
                                                if (retRowsCnt < expectRows) fail = true;
                                                break;
                                            case "max":
                                                if (retRowsCnt > expectRows) fail = true;
                                                break;
                                            case "exact":
                                                if (retRowsCnt != expectRows) fail = true;
                                                break;
                                            default:
                                                if (retRowsCnt != expectRows) fail = true;
                                                break;
                                        }
                                        if (!storingData) {
                                            contextData.Validation.AddLine($"{retRowsCnt} rows for : {cassandraQuery}");
                                            if (fail) {
                                                Row row = null;
                                                string rowData = "";
                                                if (retRowsCnt > 0) {
                                                    //rs.FetchMoreResults();
                                                    foreach (Row r in rs) {
                                                        foreach (object val in r) {
                                                            rowData += val?.ToString().Replace(",", "\\,") + ",";
                                                        }
                                                    }
                                                }
                                                matchMiss++;
                                                string missText = $"analMiss {retRowsCnt} rows ({matchType}={expectRows} {sqlResults}) {cassandraQuery}";
                                                contextData.Validation.AddLine(missText);
                                                contextData.IncrementCounter("autoFix");
                                                if (row != null) {
                                                    contextData.Validation.AddLine($"analMisData {rowData}");
                                                }
                                                string addlFixInfo = "";
                                                if (validateTimeShiftCol >= 0) {
                                                    addlFixInfo = $" -- src data='{csvStg}'";
                                                    contextData.Validation.AddLine($"autoFix '{ctx.AutoFix ?? "no ctx.AutoFix"}' {addlFixInfo}");
                                                    contextData.AutoFixes.AddLine(csvStg);
                                                }
                                                else {
                                                    if (!string.IsNullOrEmpty(ctx.AutoFix)) {
                                                        contextData.Validation.AddLine($"autoFix '{ctx.AutoFix}' -- {addlFixInfo}");
                                                        contextData.AutoFixes.AddLine($"{ctx.AutoFix}{addlFixInfo}");
                                                    }
                                                    else {
                                                        contextData.Validation.AddLine($"autoFix {addlFixInfo}");
                                                        contextData.AutoFixes.AddLine(csvStg);
                                                    }
                                                }
                                                if (matchMiss < missOutputLimit) {
                                                    contextData?.ContextWrite(missText);
                                                    if (!string.IsNullOrEmpty(ctx.AutoFix))
                                                        contextData?.ContextWrite("autoFix", ctx.AutoFix);
                                                    if (row != null) contextData.ContextWrite("analMisData", rowData);
                                                }
                                                else if (matchMiss == missOutputLimit) {
                                                    contextData?.ContextWrite("analMiss", $" {matchMiss} = to many misses. Tuncating output");
                                                }
                                                else if (matchMiss >= missAbortLimit) {
                                                    contextData?.ContextWrite("analMiss", $" {matchMiss} = to many misses.  Aborting");
                                                    break;
                                                }
                                            }
                                            else {
                                                matchFound++;
                                                contextData.IncrementCounter("noFix");
                                                if (maxNofixRows-- > 0) {
                                                    contextData.NoFixes.AddLine(ctx.AutoFix);
                                                }
                                                if (ctxLines-- > 0) {
                                                    contextData?.ContextWrite("analHit",
                                                        $" {retRowsCnt} rows ({matchType}={expectRows}  {sqlResults}) for : {cassandraQuery}");
                                                }
                                            }
                                        }
                                        else {
                                            if (retRowsCnt > 0) {
                                                foreach (Row row in rs) {
                                                    if (!startedOutput) {
                                                        CqlColumn[] cols = rs.Columns;
                                                        string colNames = "";
                                                        if (mapDataIndices != null) {
                                                            for (int i = 0; i < mapDataIndices.Length; i++) {
                                                                accumulatorTable.Columns.Add(rs.Columns[i].Name, rs.Columns[i].Type);
                                                                colNames += rs.Columns[i].Name + ",";
                                                            }
                                                        }
                                                        else {
                                                            foreach (CqlColumn ccol in rs.Columns) {
                                                                accumulatorTable.Columns.Add(ccol.Name, ccol.Type);
                                                                colNames += ccol.Name + ",";
                                                            }
                                                        }
                                                        contextData.Validation.AddLine(colNames.Trim(','));
                                                        startedOutput = true;
                                                    }


                                                    if (mapDataIndices != null) {
                                                        object[] trow = new object[mapDataIndices.Length];
                                                        for (int i = 0; i < mapDataIndices.Length; i++) {
                                                            if (rs.Columns[mapDataIndices[i]].Type.FullName == "System.DateTimeOffset") {
                                                                if (row[mapDataIndices[i]] != null) {
                                                                    DateTimeOffset dto = row.GetValue<DateTimeOffset>(mapDataIndices[i]);
                                                                    trow[i] = new DateTimeOffset(dto.Year, dto.Month, dto.Day, 0, 0, 0, tsZero);
                                                                    //string ts = $"{dto.Year}-{dto.Month}-{dto.Day}";
                                                                    //trow[i] = ts;
                                                                }
                                                                else {  // null datetimes 
                                                                    trow[i] = new DateTimeOffset();
                                                                }
                                                            }
                                                            else {
                                                                trow[i] = row[mapDataIndices[i]];
                                                            }
                                                        }
                                                        accumulatorTable.Rows.Add(trow);
                                                    }
                                                    else {
                                                        accumulatorTable.Rows.Add(row.ToArray());
                                                    }

                                                    string rowData = "";
                                                    foreach (object val in row) {
                                                        rowData += val?.ToString().Replace(",", "\\,") + ",";
                                                    }
                                                    contextData.Validation.AddLine(rowData.Trim(','));
                                                }
                                                if (!storedOriginalTable) {
                                                    queryResultTables.Add(accumulatorTable);
                                                    storedOriginalTable = true;
                                                }
                                            }
                                        }
                                    }
                                    readRows++;
                                }
                                catch (Exception exc) {
                                    contextData.ContextWrite("Exception", $" {exc.Message}, {exc.InnerException?.Message}, with '{csvStg}'");
                                }
                                // maybe skipping some lines (e.g., alternating in some of the 224 subscription files)
                                if (skipRefRows > 0) {
                                    for (int skip = 0; skip < skipRefRows && !referenceStream.EndOfStream; skip++) {
                                        var discard = referenceStream.ReadLine();
                                    }
                                }
                                if (referenceStream.EndOfStream) break;
                            }
                        }

                        pa["X-BIO-etag"] = _myEtag;
                        DateTime dtNow = DateTime.Now;
                        contextData?.ContextWrite("analComplete", $" at {dtNow}, delta = {dtNow.Subtract(dtStart).TotalMilliseconds} ms.");
                        contextData?.Add2Counter("analysisTotal", (long)dtNow.Subtract(dtStart).TotalMilliseconds);
                        if (contextData != null) {
                            if (matchMiss > 0) contextData.ContextWrite("missed", $"{matchMiss} total not having {pa["matchType"]} count of {expectRows} ");
                            contextData.ContextWrite("miss", $"{matchMiss} total not having {pa["matchType"]} count of {expectRows} ");
                            contextData.ContextWrite("hits", $"{matchFound} total     having {pa["matchType"]} count of {expectRows} ");
                            contextData.ContextWrite("read", $"{readRows} rows read");
                        }
                    }
                    else {
                        contextData.ContextWrite("val_noCQuery", " doing nothing");
                    }
                }

                DataTable _lastResultTable = null;// from older 
                if (!string.IsNullOrEmpty(pa["procResp"]) && (pa["procResp"].ToLower() != "0")) { // process data and write
                    if (_lastResultTable != null) {
                        contextData.ContextWrite("rawData", $" row count {_lastResultTable.Rows.Count}");
                    }
                    DataUtils1.PrepareResponseInfo(queryResultTables, pa, request, response, pa["resultsRawFileName"] + "_", contextData);
                    contextData.ContextWrite("ResponseHeaders", response.Headers);
                    //string contexFileNamePath = contextData?.End();
                    string contextReturn = pa["contextRet"] ?? "0";
                    if (contextReturn.Equals("1") && response != null) {
                        response.Content = new StringContent(contextData.Snag());
                    }
                }
                else { // process data no write (maybe merge with above, but this is simpler)
                    DataUtils1.PrepareResponseInfo(queryResultTables, pa, request, response, null, contextData);
                }
            }
            catch (Exception exc) {
                contextData?.ContextWrite("AO.TimeSeriesProcessing.AnalysisUtils.AnalysisValidate exception", $"{exc.Message}, {exc.InnerException?.Message}");
                NameValueCollection nv = new NameValueCollection();
                nv.Add("n", "v");
                XmlDocument doc = XmlDocUtils.GetXMLExceptionDocument("putAnalysis", nv, exc);
                DataUtils1.PrepareResponseInfo(queryResultTables, pa, request, response, null, contextData);
                response.Content = new StringContent(doc.OuterXml);
            }
        }


        /// <summary>
        /// do basic analysis (clean this up -- extracted method, so put vals in pa)
        /// </summary>
        /// <param name="context"></param>
        /// <param name="pa"></param>
        /// <param name="request"></param>
        /// <param name="response"></param>
        public static void AnalysisBasic(ContextData context, ProcessingArgs pa, HttpRequestMessage request, HttpResponseMessage response) {
            string clientId = pa.GetControlVal("clientId", "");
            switch (pa.GetControlVal("oper", "nada")) {
                case "recallseries": {
                    try {
                        string sqlMeterMgtDatabase = pa.GetControlVal($"{clientId}sqlMeterMgtDatabasename", "InsightsDW_Test");
                        if (pa["sqlMeterMgtCxnStg"] == null) {
                            string sqlMeterMgt = ConfigurationManager.AppSettings.Get("sqlMeterMgt");
                            pa["sqlMeterMgtCxnStg"] = String.Format(sqlMeterMgt, sqlMeterMgtDatabase);
                        }
                        SeriesCollectionSpecification collSpec = new SeriesCollectionSpecification(pa);
                        collSpec.InitializeCache(pa, false, context);
                        SeriesSpecification seriesSpec = new SeriesSpecification(pa, null as DataRow);
                        seriesSpec.InitializeCache(pa);
                        string commodity = pa.GetControlVal("commodity", "0");
                        string seriesColInfoSpec = pa.GetControlVal($"{clientId}SeriesColInfoSpec{commodity}", $"0SeriesColInfoSpec{commodity}");
                        string uom = pa.GetControlVal("uom", string.Empty);
                        string spec = $"Commodity:{commodity}";
                        if (!string.IsNullOrEmpty(uom)) {
                            spec = $"uom:{uom.ToLower()}|{spec}";
                        }
                        else if (string.IsNullOrEmpty(uom) && commodity == "1") {
                            spec = $"uom:kwh|{spec}";
                        }
                        // direction and tou if configured
                        if (!string.IsNullOrEmpty(seriesColInfoSpec)) {
                            string[] specPairs = seriesColInfoSpec.Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                            foreach (string specPair in specPairs) {
                                string[] s = specPair.Split(new[] { ':' }, StringSplitOptions.RemoveEmptyEntries);
                                // if there's direction setup in series col info spec, retrieve based on direction passed in arg; otherwise default to forward(1);
                                // forward =1, reverse = 19
                                if (s[0].ToLower().Contains("direction")) {
                                    string direction = pa.GetControlVal("direction", "1");
                                    spec = string.IsNullOrEmpty(spec) ? $"{s[0]}:{direction}" : $"{spec}|{s[0]}:{direction}";
                                }
                                // if there's tou setup in series col info spec, retrieve based on tou passed in arg; otherwise default to noTou (0)
                                // noTou = 0, onpeak = 1, offpeak = 2, shoulder1 = 3, shoulder2 = 4, critical peak = 5
                                if (s[0].ToLower().Contains("tou")) {
                                    string tou = pa.GetControlVal("tou", "0");
                                    spec = string.IsNullOrEmpty(spec) ? $"{s[0]}:{tou}" : $"{spec}|{s[0]}:{tou}";
                                }
                            }
                        }

                        string seriesId = string.IsNullOrEmpty(spec) ? pa["collid"] : $"{pa["collid"]}|{spec}";

                        DataTable dt = collSpec.GetSeriesData2(pa, context, seriesId, seriesSpec);
                        DataUtils1.PrepareResponseInfo(dt, pa, request, response, null, context);
                        context.ContextWrite("ResponseHeaders", response.Headers);
                        string contextReturn = pa["contextRet"] ?? "0";
                        if (contextReturn.Equals("1") && response != null) {
                            response.Content = new StringContent(context.Snag());
                            Newtonsoft.Json.Formatting fmt = pa.GetControlVal("jsonfmt", "1").Equals("1")
                                ? Newtonsoft.Json.Formatting.Indented
                                : Newtonsoft.Json.Formatting.None;
                            string dataOut = JsonConvert.SerializeObject(dt, fmt);
                            context.NoFixes.AddLine(dataOut);
                        }
                        int retRows = dt.Rows.Count;
                        if (retRows > 0) {
                            context.ContextWrite("tseries returned data", $" rows={retRows}");
                            context.Add2Counter("retrows", retRows);
                        }
                        else {
                            context.ContextWrite("warn_tseries", " no returned data");
                        }
                    }
                    catch (Exception exc) {
                        context.ContextWrite("error_tseries", $" exception {exc.Message},{exc.InnerException?.Message} ");
                    }
                }
                    break;

                case "recallmeters": {
                    try {
                        string sqlMeterMgtDatabase = pa.GetControlVal($"{clientId}sqlMeterMgtDatabasename", "InsightsDW_Test");
                        if (pa["sqlMeterMgtCxnStg"] == null) {
                            string sqlMeterMgt = ConfigurationManager.AppSettings.Get("sqlMeterMgt");
                            pa["sqlMeterMgtCxnStg"] = String.Format(sqlMeterMgt, sqlMeterMgtDatabase);
                        }
                        DataTable dt = new SeriesCollectionSpecification(pa).GetSome(pa);
                        DataUtils1.PrepareResponseInfo(dt, pa, request, response, null, context);
                        context.ContextWrite("ResponseHeaders", response.Headers);
                        string contextReturn = pa["contextRet"] ?? "0";
                        if (contextReturn.Equals("1") && response != null) {
                            response.Content = new StringContent(context.Snag());
                            Newtonsoft.Json.Formatting fmt = pa.GetControlVal("jsonfmt", "1").Equals("1")
                                ? Newtonsoft.Json.Formatting.Indented
                                : Newtonsoft.Json.Formatting.None;
                            string dataOut = JsonConvert.SerializeObject(dt, fmt);
                            context.NoFixes.AddLine(dataOut);
                        }
                        int retRows = dt.Rows.Count;
                        if (retRows > 0) {
                            context.ContextWrite("tseries returned data", $" rows={retRows}");
                        }
                        else {
                            context.ContextWrite("warn_tseries", " no returned data");
                        }
                    }
                    catch (Exception exc) {
                        context.ContextWrite("error_tseries", $" exception {exc.Message},{exc.InnerException?.Message} ");
                    }
                }
                    break;

                default: {
                    List<DataTable> queryResultTables = new List<DataTable> { };
                    DataUtils1.LoadQueryData(context, pa, queryResultTables, true);

                    string _myEtag = (!string.IsNullOrEmpty(pa["myEtag"])) ? pa["myEtag"] : "";
                    if (!string.IsNullOrEmpty(pa["procResp"]) && (pa["procResp"].ToLower() != "0")) { // process data and write
                        context.ContextWrite("webPath", pa["outputWeb"]);
                        context.ContextWrite("etag", _myEtag);
                        context.ContextWrite("RequestUri", request.RequestUri.ToString());
                        context.ContextWrite("RequestHeaders", request.Headers);
                        context.ContextWriteBulk("RequestContent", pa["reqContent"]);
                        DataUtils1.PrepareResponseInfo(queryResultTables, pa, request, response, pa["resultsRawFileName"] + "_", context);
                        for (int i = 0; i < queryResultTables.Count; i++) {
                            if (queryResultTables[i] != null) context.ContextWrite("rawData", $" table {i} row count {queryResultTables[i].Rows.Count}");
                        }
                        context.ContextWrite("ResponseHeaders", response.Headers);
                        string contextReturn = pa["contextRet"] ?? "0";
                        if (contextReturn.Equals("1") && response != null) {
                            response.Content = new StringContent(context.Snag());
                        }
                    }
                    else { // process data no write (maybe merge with above, but this is simpler)
                        DataUtils1.PrepareResponseInfo(queryResultTables, pa, request, response, null, context);
                    }
                }
                    break;
            }

        }
    }
}
