﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;



namespace AO.TimeSeriesProcessing {


    public class CollSpecIdentityCache : IDisposable {
        private DataTable SystemCache { get; set; }
        private string _cache_padlock = "CollSpecIdentityCachePadlock";
        private Dictionary<string, List<DataRow>> _hashedCache;

        private string TableName;
        private string CollectionId;
        const string SelSomeMinFormat = "SELECT TOP 5000000 * FROM {0}";
        private bool _initializingCache;

        public int Length { get { return _hashedCache?.Count ?? -1; } }

        public CollSpecIdentityCache(ProcessingArgs pa, string collId, string tablerName, bool force = false) {
            CollectionId = collId;
            TableName = tablerName;
            InitializeCache(pa, force);
        }

        /// <summary>
        /// initialize cache prior to an AMI ingestion.  Note that we will have separated out the creat series infor
        /// so that for the duration of a bulk import the sql data will be stable.
        /// A simultaneous other import may update the sql data and start a bulk import without impacting a current import.
        /// </summary>
        /// <param name="pa"></param>
        /// <param name="force"></param>
        public void InitializeCache(ProcessingArgs pa, bool force = false) {
            if (SystemCache == null | force) {
                lock (_cache_padlock) {
                    if (SystemCache == null | force) {
                        _initializingCache = true;
                        if (SystemCache != null) {
                            SystemCache.Dispose();
                            SystemCache = null;
                        }
                        string sqlMeterMgtCxnStg = pa["sqlMeterMgtCxnStg"];
                        using (SqlConnection con = new SqlConnection(sqlMeterMgtCxnStg)) {
                            con.Open();
                            string cmdTxt = string.Format(SelSomeMinFormat, TableName);
                            DataSet ds = SqlUtils.ExecuteDataset(con, CommandType.Text, cmdTxt, null);
                            if (ds.Tables.Count == 0) {
                                throw new Exception("InitializeCache: no SeriesCollectionInfo table in DB");
                            }
                            if (ds.Tables[0].Rows.Count == 0) {
                                throw new Exception("InitializeCache: no SeriesCollectionInfo data in DB");
                            }
                            SystemCache = ds.Tables[0];
                            _hashedCache = new Dictionary<string, List<DataRow>>();
                            string skey = CollectionId;
                            // ReSharper disable once RedundantAssignment
                            var key = CollectionId;
                            foreach (DataRow row in SystemCache.Rows) {
                                if (CollectionId.IndexOf('|') >= 0) {
                                    string[] ids = CollectionId.Split(new[] { '|' }, StringSplitOptions.RemoveEmptyEntries);
                                    key = "" + row[ids[0]];
                                    if (ids.Length > 1) {
                                        // hash key contains value from one or more columns
                                        for (int i = 1; i < ids.Length; i++) {
                                            key += "|" + row[ids[i]];
                                        }
                                    }
                                }
                                else {
                                    key = row[skey].ToString();
                                }
                                if (_hashedCache.ContainsKey(key)) {
                                    _hashedCache[key].Add(row);
                                }
                                else {
                                    _hashedCache[key] = new List<DataRow>();
                                    _hashedCache[key].Add(row);
                                }
                            }
                        }
                        _initializingCache = false;
                    }
                }
            }
        }


        public DataRow[] Select(string id) {
            int delay = 60000;
            while (_initializingCache && (delay-- > 0)) {
                Thread.Sleep(10);
            }
            if (delay <= 0) { // in the off chanch that this gets stuck
                Trace.WriteLine($"CollSpecIdentityCache initiaize timeout -> resetting.");
                _initializingCache = false;
            }
            return (_hashedCache.ContainsKey(id)) ? _hashedCache[id].ToArray() : null;
        }


        public void Dispose() {
            if (SystemCache != null) {
                SystemCache.Dispose();
            }
        }
    }


    /// <summary>
    /// Class for the new cassandra time series management.
    /// See design docs.
    /// </summary>
    public class SeriesCollectionSpecification : ISeriesCollectionSpecification {
        public SeriesSpecification CacheSeriesSpec;

        private static CollSpecIdentityCache _idCache;
        private static string _cache_padlock = "SeriesCollectionSpecificationPadlock";
        private static DateTime _cacheInitTime;
        private static int _cacheMaxAgeSeconds = 600;

        /// <summary>
        /// initialize cache prior to an AMI ingestion.  Note that we will have separated out the create series info
        /// so that for the duration of a bulk import the sql data will be stable.
        /// A simultaneous other import may update the sql data and start a bulk import without impacting a current import.
        /// </summary>
        /// <param name="pa"></param>
        /// <param name="force"></param>
        /// <param name="context"></param>
        public void InitializeCache(ProcessingArgs pa, bool force = false, ContextData context = null) {
            _cacheMaxAgeSeconds = pa.GetControlVal("cacheMaxAgeSeconds", _cacheMaxAgeSeconds);
            double howOld = DateTime.Now.Subtract(_cacheInitTime).TotalSeconds;
            if (_idCache == null || force || (howOld > _cacheMaxAgeSeconds)) {
                DateTime stt = DateTime.Now;
                lock (_cache_padlock) {
                    if (_idCache == null || force || (howOld > _cacheMaxAgeSeconds)) {
                        if (_idCache != null) {
                            try {
                                _idCache.Dispose();
                            }
                            catch (Exception exc) {
                                Trace.WriteLine($"SeriesCollectionSpecification.InitializeCache exception {exc.Message}, {exc.InnerException?.Message}");
                            }
                            _idCache = null;
                        }

                        string clientId = pa.GetControlVal("clientId", "87");
                        string cacheColumns = pa.GetControlVal($"{clientId}collHashCols", HashKeyColumns);
                        _idCache = new CollSpecIdentityCache(pa, cacheColumns, TableName);
                        CacheSeriesSpec = new SeriesSpecification(pa);
                        CacheSeriesSpec.InitializeCache(pa, force || (howOld > _cacheMaxAgeSeconds));
                        _cacheInitTime = DateTime.Now;
                        pa["MeterSpecInitTime"] = _cacheInitTime.ToString(CultureInfo.InvariantCulture);
                    }
                }
                context?.Add2Counter("initializeCacheTime", (long)DateTime.Now.Subtract(stt).TotalMilliseconds);
                context?.Add2Counter("initializeCacheLength", _idCache.Length);
            }
        }

        public static void InitializeCacheStatic(ProcessingArgs pa, bool force = false) {

        }


        private SeriesStorePersistenceInfo _cacheCasCxn;
        //private static string cacheCasCxnPadlock = "cacheCasCxnPadlock";

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pa"></param>
        /// <param name="context"></param>
        /// <param name="colid"></param>
        /// <param name="seriesSpec"></param>
        /// <returns></returns>
        public DataTable GetSeriesData(ProcessingArgs pa, ContextData context, string colid, SeriesSpecification seriesSpec) {
            DataTable retTable;
            InitializeCache(pa); // just in case
            string id = GetDataSeriesKey(pa, colid);
            if (string.IsNullOrEmpty(id)) {
                return null;
            }
            if (seriesSpec == null) {
                seriesSpec = new SeriesSpecification(pa, null as DataRow);
                seriesSpec.InitializeCache(pa);
            }
            string useCacheCas = pa.GetControlVal("allowcachecas", "no");
            DateTimeOffset stt = DateTimeOffset.Parse(pa["dtStt"] ?? "2017-01-01", null, DateTimeStyles.AssumeUniversal);
            DateTimeOffset stp = DateTimeOffset.Parse(pa["dtStp"] ?? "2017-02-01", null, DateTimeStyles.AssumeUniversal);
            List<string> qryStgs = seriesSpec.GetQuery(pa, id, stt, stp);
            DateTime dtStt = DateTime.Now;
            if (pa.GetControlVal("ctxlog", 1) > 3) {
                context.ContextWrite($"GetSeriesData", $"{qryStgs.Count} queries");
            }
            BatchExecutor batch = new BatchExecutor();
            if (useCacheCas.Equals("reset") || useCacheCas.Equals("no")) {
                SeriesStorePersistenceInfo.Discard(pa);
            }
            _cacheCasCxn = SeriesStorePersistenceInfo.GetCacheCxn(pa);
            if (pa.GetControlVal("gasync", "1").Equals("1")) {
                retTable = batch.ExecuteGetTable(pa, context, qryStgs, _cacheCasCxn);
            }
            else {
                retTable = batch.ExecuteGetTable3(pa, context, qryStgs, _cacheCasCxn);
            }
            if (pa.GetControlVal("ctxlog", 1) > 3) {
                context.ContextWrite($"GetSeriesData", $"{qryStgs.Count} queries", dtStt);
            }
            return retTable;
        }


        public DataTable GetSeriesData2(ProcessingArgs pa, ContextData context, string colid, SeriesSpecification seriesSpec,
            SeriesStorePersistenceInfo cassCon = null) {
            DataTable retTable;
            InitializeCache(pa); // just in case
            string id = GetDataSeriesKey(pa, colid);
            if (seriesSpec == null) {
                seriesSpec = new SeriesSpecification(pa, null as DataRow);
                seriesSpec.InitializeCache(pa);
            }
            DateTimeOffset stt = DateTimeOffset.Parse(pa["dtStt"], null, DateTimeStyles.AssumeUniversal);
            DateTimeOffset stp = DateTimeOffset.Parse(pa["dtStp"], null, DateTimeStyles.AssumeUniversal);
            List<string> qryStgs = seriesSpec.GetQuery(pa, id, stt, stp);
            BatchExecutor batch = new BatchExecutor();
            if (cassCon == null) {
                using (SeriesStorePersistenceInfo casCxn = new SeriesStorePersistenceInfo(pa)) {
                    retTable = batch.ExecuteGetTable3(pa, context, qryStgs, casCxn);
                }
            }
            else {
                retTable = batch.ExecuteGetTable3(pa, context, qryStgs, cassCon);
            }
            return retTable;
        }

        /// <summary>
        /// Get the cassandra series to store some data
        /// this is dev/POC -- will be caching this stuff
        /// </summary>
        /// <param name="pa"></param>
        /// <param name="collid"></param>
        /// <param name="seriesSpec"></param>
        /// <returns></returns>
        public static SeriesSpecification GetSeriesSpec(ProcessingArgs pa, string collid, string seriesSpec) {
            string sqlMeterMgtCxnStg = pa["sqlMeterMgtCxnStg"];
            SeriesSpecification ss;
            using (SqlConnection con = new SqlConnection(sqlMeterMgtCxnStg)) {
                con.Open();
                string cmdTxt = string.Format(SpecQueryFormat, collid);
                DataSet ds = SqlUtils.ExecuteDataset(con, CommandType.Text, cmdTxt, null);
                if (ds.Tables.Count == 0) {
                    throw new Exception("SeriesCollectionSpecification.GetSeriesSpec: no SeriesCollectionInfo table in DB");
                }
                DataTable dt = ds.Tables[0];
                if (dt.Rows.Count == 0) {
                    throw new Exception($"SeriesCollectionSpecification.GetSeriesSpec: invalid collectionId {collid}");
                }
                SeriesCollectionSpecification scs = new SeriesCollectionSpecification(dt.Rows[0]);
                string[] timeSeries = scs.TimeSeries.Split('|');
                ss = SeriesSpecification.GetSeriesSpec(pa, timeSeries[0], seriesSpec, con);
            }
            return ss;
        }


        /// <summary>
        /// get the data series key for a particular collection.
        /// Initial use case is to support a single collection so only return the first (we have tested for many).
        /// </summary>
        /// <param name="pa"></param>
        /// <param name="id"></param>
        /// <returns></returns>
        public string GetDataSeriesKey(ProcessingArgs pa, string id) {
            string resp = "";
            InitializeCache(pa);
            if (_idCache == null)
                throw new Exception("cache is still null after initialization");

            DataRow[] rows = _idCache.Select(id);
            if (rows?.Length > 0) {
                string dat = rows[0]["TimeSeries"] as string;
                if (dat == null)
                    throw new Exception("TimeSeries is null");
                string[] tss = dat.Split(new[] { '|' }, StringSplitOptions.RemoveEmptyEntries);
                return tss[0];
            }
            return resp;
        }


        /// <summary>
        /// Main use of SQL server is as a resting place for series management data stores.  In memory is where the bulk of the usage takes place.
        /// we want to avoid lock in to sql server, so staying strait TSql for most of the interactions with the DB (e.g., avoid stored procedures, etc.).
        /// This is relatively simple storage and processing so we are embedding the 'DAL' in the class.
        /// </summary>

        private const string TableName = "SeriesCollectionInfo";
        private const string HashKeyColumns = "CollectionId"; // columns to be used for cache hash key

        private static string SpecQueryFormat = $"SELECT * FROM {TableName} where CollectionId='{{0}}' and State='active' order by id asc";
        private static readonly string FirstSpecQuery = $"SELECT TOP 1 * FROM {TableName} order by id asc";

        private static readonly string SpecSaveFormat =
            $"INSERT INTO [dbo].[{TableName}] ([Specs],[TimeSeries],[CollectionId],[AltCollectionId],[TClid],[TCustid],[TAcctid],[State],[Archive]) VALUES('{{0}}','{{1}}','{{2}}','{{3}}','{{4}}','{{5}}','{{6}}','{{7}}','{{8}}') SET @ID = SCOPE_IDENTITY();";

        private static readonly string SpecUpdateFormat =
            $"UPDATE [dbo].[{TableName}] set Specs=@pSpecs,TimeSeries=@pTimeSeries,CollectionId=@pCollectionId,AltCollectionId=@pAltCollectionId,TClid=@pTClid,TCustid=@pTCustid,TAcctid=@pTAcctid,State=@pState,Archive=@pArchive where id = @pId";

        private static readonly string SelSomeFormat = $"SELECT TOP {{0}} * FROM {TableName}";

        private static readonly string SelCountFormat = $"SELECT count(*) FROM {TableName}";

        private static string CsvSeriesDataHeaderCols =
            "id,Specs,TimeSeries,CollectionId,AltCollectionId,TClid,TCustid,TAcctid,State,Archive";

        public string FormattedSaveStg() {
            return string.Format(SpecSaveFormat, Specs, TimeSeries, CollectionId, AltCollectionId, TClid, TCustid, TAcctid, State, Archive);
        }

        private readonly string[] _paramsForUpdate = {
            "@pId", "@pSpecs", "@pTimeSeries", "@pCollectionId", "@pAltCollectionId", "@pTClid",
            "@pTCustid", "@pTAcctid", "@pState", "@pArchive"
        };

        object[] GetObjs() {
            object[] objs = { Id, Specs, TimeSeries, CollectionId, AltCollectionId, TClid, TCustid, TAcctid, State, Archive ?? "" };
            return objs;
        }


        private DataRow storeData;

        /// <summary>
        /// constructors of various forms
        /// </summary>
        /// <param name="pa"></param>
        /// <param name="source"></param>
        public SeriesCollectionSpecification(ProcessingArgs pa, object source = null) {
            if (source == null) {
                storeData = (DataRow)Starter(pa);
            }
            else {
                storeData = (DataRow)source;
            }
        }

        public SeriesCollectionSpecification(DataRow source) {
            storeData = source;
        }

        public SeriesCollectionSpecification(IDataSpecification source) {
            storeData = ((SeriesCollectionSpecification)source).storeData.Table.NewRow();
            storeData.ItemArray = (object[])((SeriesCollectionSpecification)source).storeData.ItemArray.Clone();
        }

        /// <summary>
        /// formatted content for logging, viewing
        /// </summary>
        /// <returns></returns>
        public string Show() {
            return string.Format($"id={Id},CollectionId='{CollectionId}',Specs='{Specs}'");
        }

        /// <summary>
        /// These should be made private and potentially eliminated as they are just implemented for quickly developing the initial implementation.
        /// Ideally should not be creating code couplings to the schema/store
        /// </summary>

        public long Id {
            get { return (long)storeData["id"]; }
            set { storeData["id"] = value; }
        }

        public string CollectionId {
            get { return (string)storeData["CollectionId"]; }
            set { storeData["CollectionId"] = value; }
        }

        public string Specs {
            get { return storeData["Specs"] as string; }
            set { storeData["Specs"] = value; }
        }

        public string Statistics(ProcessingArgs pa) {
            string sqlMeterMgtCxnStg = pa["sqlMeterMgtCxnStg"];
            int count;
            using (SqlConnection con = new SqlConnection(sqlMeterMgtCxnStg)) {
                con.Open();
                SqlCommand cmd = new SqlCommand(SelCountFormat, con);
                count = (int)cmd.ExecuteScalar();
            }
            return $" count={count}";
        }


        // these are just for dev/debug convenience and should be deprecated later
        public string TimeSeries {
            get { return storeData["TimeSeries"] as string; }
            //get { return (storeData["TimeSeries"] == null) ? "" : storeData["TimeSeries"] as string; }
            set { storeData["TimeSeries"] = value; }
        }

        public string State {
            get { return storeData["State"] as string; }
            set { storeData["State"] = value; }
        }

        public string Archive {
            get { return storeData["Archive"] as string; }
            set { storeData["Archive"] = value; }
        }

        public string AltCollectionId {
            get { return storeData["AltCollectionId"] as string; }
            set { storeData["AltCollectionId"] = value; }
        }

        // ReSharper disable once InconsistentNaming
        public string TClid {
            get { return storeData["TClid"] as string; }
            set { storeData["TClid"] = value; }
        }

        // ReSharper disable once InconsistentNaming
        public string TCustid {
            get { return storeData["TCustid"] as string; }
            set { storeData["TCustid"] = value; }
        }

        // ReSharper disable once InconsistentNaming
        public string TAcctid {
            get { return storeData["TAcctid"] as string; }
            set { storeData["TAcctid"] = value; }
        }

        private static DataRow _starter; // initialization template to avoid persistance hits when generating instances
        private static readonly string _padlock = "MeterSpec.Starter.padlock";

        /// <summary>
        /// initialization of statics for this component
        /// </summary>
        /// <param name="pa"></param>
        /// <param name="source"></param>
        public void Initialize(ProcessingArgs pa, object source = null) {
            if (_starter == null) {
                lock (_padlock) {
                    if (_starter == null) {
                        try {
                            if (source != null) {
                                DataSet ds = SqlUtils.ExecuteDataset((SqlConnection)source, CommandType.Text, FirstSpecQuery, null);
                                if (ds.Tables.Count > 0) {
                                    _starter = ds.Tables[0].NewRow();
                                    if (ds.Tables[0].Rows.Count > 0) {
                                        _starter.ItemArray = (object[])ds.Tables[0].Rows[0].ItemArray.Clone();
                                        _starter["TimeSeries"] = "";
                                        _starter["Specs"] = "";
                                    }
                                }
                            }
                            else {
                                string sqlMeterMgtCxnStg = pa["sqlMeterMgtCxnStg"];
                                using (SqlConnection conn = new SqlConnection(sqlMeterMgtCxnStg)) {
                                    DataSet ds = SqlUtils.ExecuteDataset(conn, CommandType.Text, FirstSpecQuery, null);
                                    if (ds.Tables.Count > 0) {
                                        _starter = ds.Tables[0].NewRow();
                                        if (ds.Tables[0].Rows.Count > 0) {
                                            _starter.ItemArray = (object[])ds.Tables[0].Rows[0].ItemArray.Clone();
                                            _starter["TimeSeries"] = "";
                                            _starter["Specs"] = "";
                                        }
                                    }
                                }
                            }
                        }
                        catch (Exception exc) {
                            Debug.WriteLine($"MeterSpec.Starter {exc.Message}, {exc.InnerException?.Message}, {exc.StackTrace}");
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Spin up this puppy with it's seed and make sure we are ready to go with this component.
        /// </summary>
        /// <param name="pa"></param>
        /// <param name="source"></param>
        /// <returns></returns>
        public object Starter(ProcessingArgs pa, object source = null) {
            DataRow rVal = null;
            if (_starter == null) {
                Initialize(pa, source);
            }
            if (_starter != null) {
                rVal = _starter.Table.NewRow();
                rVal.ItemArray = (object[])_starter.ItemArray.Clone();
            }
            return rVal;
        }


        public DataTable GetSome(ProcessingArgs pa, SqlConnection cxn = null) {
            int quantity = pa.GetControlVal("nummeters", -1);
            if (quantity < 0) {
                quantity = pa.GetControlVal("quantity", 1000);
            }
            quantity = pa.GetControlVal("per_page", quantity);
            int page = pa.GetControlVal("page", 0);
            string sqlMeterMgtCxnStg = pa["sqlMeterMgtCxnStg"];
            DataTable dt;
            string cmdTxt = string.Format(SelSomeFormat, quantity);
            string resourceId = pa.GetControlVal("restResId", "nada");
            int skip = 0;
            string filter;
            if (page > 0) {
                skip = quantity * page;
            }
            int offset = pa.GetControlVal("page_start", 0);
            skip += offset;
            if (!resourceId.Equals("nada")) {
                filter = $" CollectionId='{resourceId}'";
            }
            else {
                filter = pa.GetControlVal("where", "");
            }
            if (skip > 0) {
                string toSkip = $" id > {skip} ";
                if (!string.IsNullOrEmpty(filter)) {
                    filter += $" and {toSkip} ";
                }
                else {
                    filter = $" {toSkip} ";
                }
            }

            if (!string.IsNullOrEmpty(filter)) {
                filter = SqlUtils.SafeSqlStgSegment(filter);
                cmdTxt += " where " + filter;
            }

            if (cxn == null) {
                using (SqlConnection con = new SqlConnection(sqlMeterMgtCxnStg)) {
                    con.Open();
                    DataSet ds = SqlUtils.ExecuteDataset(con, CommandType.Text, cmdTxt, null);
                    if (ds.Tables.Count == 0) {
                        throw new Exception("GetSome: no SeriesCollectionInfo table in DB");
                    }
                    dt = ds.Tables[0];
                }
            }
            else {
                DataSet ds = SqlUtils.ExecuteDataset(cxn, CommandType.Text, cmdTxt, null);
                if (ds.Tables.Count == 0) {
                    throw new Exception("GetSome: no SeriesCollectionInfo table in DB");
                }
                dt = ds.Tables[0];
            }
            return dt;
        }


        //private string namecolidx = "client=0,cust=1,meter=40,spoint=20,acct=15";

        /// <summary>
        /// Populate series collections and series from billing data.
        /// This is to facilitate generating/initializing advanced AMI with existing custoemr interaction artifacts that also are ingested into
        /// the alerts system general.
        /// </summary>
        /// <param name="pa"></param>
        /// <param name="context"></param>
        /// <param name="spec"></param>
        public void GenerateFromCsvData(ProcessingArgs pa, ContextData context, SeriesCollectionSpecification spec) {
            int procLimit = pa.GetControlVal("proclimit", 100);
            int numSeries = pa.GetControlVal("numSeries", 1);
            var tasks = new List<Task>();
            int maxTasks = 50;
            InitializeCache(pa);
            // ReSharper disable once UnusedVariable
            SeriesCollectionSpecification serColl = new SeriesCollectionSpecification(pa);

            StringDictionary newCollection = new StringDictionary();

            string sqlMeterMgtCxnStg = pa["sqlMeterMgtCxnStg"];
            context.ContextWrite("GenerateFromCsvData", "start");
            using (SqlConnection con = new SqlConnection(sqlMeterMgtCxnStg)) {
                con.Open();
                PrepareForeAdditions(pa, 1, con);
                string referenceSourceFile = pa.GetControlVal("importfile", @"c:\_Test2\SimpleBilling.csv");
                SqlCommand cmd = new SqlCommand {
                    CommandType = CommandType.Text,
                    Connection = con
                };
                cmd.Parameters.Add("@ID", SqlDbType.BigInt, 8).Direction = ParameterDirection.Output;
                bool doAsyncBatch = pa.GetControlVal("doAsyncBatch", 0) == 1;
                // quick init of temporary data in billing file until UL integration is done.
                var colIdxMap = new Dictionary<string, int>();
                string namecolidx = pa["namecolidx"];
                string[] nvpairs = namecolidx.Trim().Split(',');
                foreach (string nvpair in nvpairs) {
                    string[] nv = nvpair.Split('=');
                    colIdxMap[nv[0]] = int.Parse(nv[1]);
                }
                using (StreamReader referenceStream = new StreamReader(referenceSourceFile)) {
                    referenceStream.ReadLine();
                    while (!referenceStream.EndOfStream && (procLimit-- > 0)) {
                        var csvVals = referenceStream.ReadLine();
                        context.IncrementCounter("importRecs");
                        if (csvVals == null)
                            throw new Exception("csv stream value is null");

                        string[] colData = csvVals.Split(',');
                        for (int i = 0; i < colData.Length; i++) colData[i] = colData[i].Trim();

                        DataRow[] rows = _idCache.Select(colData[colIdxMap["meter"]]);
                        if (rows?.Length > 0) {
                            context.Add2Counter("collIdAlreadyRegistered", 1);
                            continue;
                        }
                        context.Add2Counter("collIdToBeRegistered", 1);
                        //string specs = "";
                        //foreach (KeyValuePair<string, int> keyValuePair in colIdxMap) {
                        //    specs += $"{keyValuePair.Key}:{colData[keyValuePair.Value]}|";
                        //}
                        //specs = specs.Trim('|');
                        string meId = (colIdxMap["meter"] >= 0) ? colData[colIdxMap["meter"]] : "";
                        string spId = (colIdxMap["spoint"] >= 0) ? colData[colIdxMap["spoint"]] : "";
                        string clId = (colIdxMap["client"] >= 0) ? colData[colIdxMap["client"]] : "";
                        string cuId = (colIdxMap["cust"] >= 0) ? colData[colIdxMap["cust"]] : "";
                        string acId = (colIdxMap["acct"] >= 0) ? colData[colIdxMap["acct"]] : "";
                        if (string.IsNullOrEmpty(spId)) {
                            if (string.IsNullOrEmpty(meId)) {
                                continue;
                            }
                            spId = meId;
                        }
                        else if (string.IsNullOrEmpty(meId)) {
                            if (string.IsNullOrEmpty(spId)) {
                                continue;
                            }
                            meId = spId;
                        }
                        string colId = spId;

                        string commodity = colIdxMap.ContainsKey("Commodity") ? colData[colIdxMap["Commodity"]] : "0";
                        string uom = colIdxMap.ContainsKey("UOM") ? colData[colIdxMap["UOM"]] : string.Empty;
                        string seriesColInfoSpec = pa.GetControlVal($"{clId}SeriesColInfoSpec{commodity}", $"0SeriesColInfoSpec{commodity}");

                        Dictionary<string, string> direction = new Dictionary<string, string>();
                        Dictionary<string, string> tou = new Dictionary<string, string>();
                        string specFormat = $"Commodity:{commodity}";
                        if (!string.IsNullOrEmpty(uom)) {
                            specFormat = $"uom:{uom.ToLower()}|{spec}";
                        }
                        if (!string.IsNullOrEmpty(seriesColInfoSpec)) {
                            string[] specPairs = seriesColInfoSpec.Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                            foreach (string specPair in specPairs) {
                                string[] s = specPair.Split(new[] { ':' }, StringSplitOptions.RemoveEmptyEntries);

                                specFormat = $"{specFormat}|%{s[0]}%";
                                if (s[0].ToLower() == "direction") {
                                    direction.Add(s[0], $"{s[0]}:1");
                                    direction.Add(s[0], $"{s[0]}:19");
                                }
                                if (s[0].ToLower() == "tou") {
                                    tou.Add(s[0], $"{s[0]}:0");
                                    tou.Add(s[0], $"{s[0]}:1");
                                    tou.Add(s[0], $"{s[0]}:2");
                                    tou.Add(s[0], $"{s[0]}:3");
                                    tou.Add(s[0], $"{s[0]}:4");
                                    tou.Add(s[0], $"{s[0]}:5");
                                }
                            }
                        }

                        List<string> specSeries = new List<string>();

                        if (tou.Any() && direction.Any()) {
                            foreach (KeyValuePair<string, string> t in tou) {
                                var replaceTKey = $"%{t.Key}%";
                                foreach (var d in direction) {
                                    var replaceDKey = $"%{d.Key}%";
                                    specSeries.Add(specFormat.Replace(replaceTKey, t.Value).Replace(replaceDKey, d.Value));
                                }
                            }
                        }
                        else if (direction.Any()) {
                            foreach (KeyValuePair<string, string> d in direction) {
                                var replaceDKey = $"%{d.Key}%";
                                specSeries.Add(specFormat.Replace(replaceDKey, d.Value));
                            }
                        }
                        else if (tou.Any()) {
                            foreach (KeyValuePair<string, string> t in tou) {
                                var replaceTKey = $"%{t.Key}%";
                                specSeries.Add(specFormat.Replace(replaceTKey, t.Value));
                            }
                        }

                        if (!specSeries.Any()) {
                            specSeries.Add("");
                        }
                        foreach (string specSerie in specSeries) {
                            string series = string.IsNullOrEmpty(specSerie) ? spId : $"{spId}|{specSeries}";
                            if (string.IsNullOrEmpty(spec.GetDataSeriesKey(pa, series)) && newCollection[series] == null) {
                                newCollection[series] = series;
                                string specs = specSerie;
                                SeriesCollectionSpecification newCollSpec =
                                    (SeriesCollectionSpecification)AddSingle(pa, new SeriesCollectionSpecification(_starter) {
                                        CollectionId = colId,
                                        AltCollectionId = spId,
                                        TClid = clId,
                                        TCustid = cuId,
                                        TAcctid = acId,
                                        Specs = specs
                                    }, 0, cmd);
                                if (doAsyncBatch) {
                                    tasks.Add(AddSeriesToCollectionAsync(pa, con, numSeries, newCollSpec, cmd));
                                    if (tasks.Count > maxTasks) {
                                        Task.WhenAll(tasks);
                                        tasks.Clear();
                                        context.IncrementCounter("batchSubmit");
                                    }
                                }
                                else {
                                    SeriesSpecification newSeriesSpec = null;
                                    AddSeriesToCollection(pa, con, numSeries, newCollSpec, cmd, ref newSeriesSpec);
                                }
                            }
                        }

                    }
                }
                if (tasks.Count > 0) {
                    Task.WhenAll(tasks);
                    tasks.Clear();
                    context.IncrementCounter("batchSubmit");
                }
            }
            context.ContextWrite("GenerateFromCsvData", "end");

        }




        //private string namecolidxAvista =
        //    "MDM_SP_ID=1,Service_Point_ID=0,Measuerment_Value=6,Date=7,time=8,Estimate_Description=9,Estimate_Indicator=10";
        //private string namecolidxAvista =
        //    "MDM_SP_ID=1,Service_Point_ID=0,Measuerment_Value=6,DateTime=7,Estimate_Description=8,Estimate_Indicator=9";

        public void Transform2SeriesImport(ProcessingArgs pa, ContextData context) {
            DateTime dtSttInit = DateTime.Now;
            InitializeCache(pa, true); // BUGBUG: for initial pass -- wicked inefficient!!!!!
            DateTime dtStpInit = DateTime.Now;
            context.ContextWrite("Transform2SeriesImport", $" InitializeCache={dtStpInit.Subtract(dtSttInit).TotalMilliseconds}ms");
            int procLimit = pa.GetControlVal("proclimit", 100);
            int numSeries = pa.GetControlVal("numSeries", 1);
            string clientId = pa.GetControlVal("clientId", "87");
            bool hasHeaders = pa.GetControlVal("hasheaders", 1) == 1;
            int errorThreshold = pa.GetControlVal("preProcErrorThreshold", 1000);
            int errorCount = 0;
            string collectionId = pa.GetControlVal($"{clientId}collectionId", "MDM_SP_ID");

            // ReSharper disable once CollectionNeverUpdated.Local
            var tasks = new List<Task>();
            InitializeCache(pa);
            string referenceSourceFile = pa.GetControlVal("importfile", @"c:\_Test2\SimpleBilling.csv");
            //bool doAsyncBatch = pa.GetControlVal("doAsyncBatch", 0) == 1;
            DateTime dtStt = DateTime.Now;

            string sqlMeterMgtCxnStg = pa["sqlMeterMgtCxnStg"];
            // quick init of temporary data in billing file until UL integration is done.
            var colIdxMap = new Dictionary<string, int>();

            string mapType = pa.GetControlVal("cvtType", "avista");
            string nvMapString = pa.GetControlVal($"{mapType.ToLower()}NamecolidxMap", "");
            string[] nvpairs = nvMapString.Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
            //string[] nvpairs = namecolidxAvista.Trim().Split(',');
            foreach (string nvpair in nvpairs) {
                string[] nv = nvpair.Split('=');
                colIdxMap[nv[0]] = int.Parse(nv[1]);
            }
            context.ContextWrite("inputFile", referenceSourceFile);
            string outputFile = referenceSourceFile + ".out.csv";
            context.ContextWrite("outputFile", outputFile);
            using (SqlConnection con = new SqlConnection(sqlMeterMgtCxnStg)) {
                con.Open();
                PrepareForeAdditions(pa, 1, con);
                SqlCommand cmd = new SqlCommand {
                    CommandType = CommandType.Text,
                    Connection = con
                };
                cmd.Parameters.Add("@ID", SqlDbType.BigInt, 8).Direction = ParameterDirection.Output;
                DateTime stest = TimingHelper.RunningTimes(context, dtStt, null, "Transform2SeriesImport", "starting");
                string csvNames = "";
                StreamWriter sw;
                MemoryStream memoryStream = null;
                bool swIsOutputFile = false;
                if ("retContent".Equals(pa.GetControlVal("retContent", "no"))) {
                    memoryStream = new MemoryStream();
                    sw = new StreamWriter(memoryStream);
                }
                else {
                    sw = new StreamWriter(outputFile);
                    swIsOutputFile = true;
                }

                StreamReader referenceStream;
                string importContent = pa.GetControlVal("dataContent", "");
                if (!string.IsNullOrEmpty(importContent)) {
                    context.ContextWrite("Transform2SeriesImport", "importContent");
                    referenceStream = new StreamReader(StreamUtils.ToStream(importContent));
                }
                else {
                    context.ContextWrite("Transform2SeriesImport", referenceSourceFile);
                    referenceStream = new StreamReader(referenceSourceFile);
                }

                bool stripQuotes = pa.GetControlVal("stripCsvQ", "yes").Equals("yes");
                string metaMapInit = pa.GetControlVal($"{mapType.ToLower()}AdvAMiMetaMaps", "Estimate_Description=estDesc,Estimate_Indicator=estInd");
                string[] metaMapInits = metaMapInit.Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                List<KeyValuePair<string, string>> nameMetaMaps = new List<KeyValuePair<string, string>>();
                foreach (string mapInit in metaMapInits) {
                    string[] maps = mapInit.Split(new[] { '=' }, StringSplitOptions.RemoveEmptyEntries);
                    if (maps.Length > 1) {
                        nameMetaMaps.Add(new KeyValuePair<string, string>(maps[0], maps[1]));
                    }
                }

                { // old using()
                    {  //using (StreamReader referenceStream = new StreamReader(referenceSourceFile)) 
                        if (hasHeaders) {
                            csvNames = referenceStream.ReadLine();
                        }
                        if (!string.IsNullOrEmpty(csvNames)) {
                            context.AutoFixes.AddLine(csvNames);
                            context.NoFixes.AddLine(csvNames);
                        }
                        stest = TimingHelper.RunningTimes(context, dtStt, stest, "Transform2SeriesImport", "begin");
                        int newSeriesCount = 0;
                        Dictionary<string, SeriesSpecification> newSeriesSpecs = new Dictionary<string, SeriesSpecification>();
                        SeriesSpecification root = new SeriesSpecification(pa, null as DataRow);
                        while (!referenceStream.EndOfStream && (procLimit-- > 0)) {
                            string csvVals = referenceStream.ReadLine();
                            if (stripQuotes) {
                                csvVals = csvVals.Replace("\"", "");
                            }
                            try {
                                SeriesSpecification ss = null;
                                context.IncrementCounter("totalrows");
                                string[] colData = csvVals.Split(',');
                                string spIdForColl = colData[colIdxMap[collectionId]];
                                string seriesId = spIdForColl;
                                string commodity = colIdxMap.ContainsKey("Commodity") ? MappingCustomHelpers.CommodityMapping(pa,clientId, colData[colIdxMap["Commodity"]]) : string.Empty;
                                if (commodity == string.Empty)
                                    throw new Exception("Commodity is missing");
                                string uom = colIdxMap.ContainsKey("UOM") ? MappingCustomHelpers.UomMapping(pa, colData[colIdxMap["UOM"]]) : string.Empty;
                                string seriesColInfoSpec = pa.GetControlVal($"{clientId}SeriesColInfoSpec{commodity}", $"0SeriesColInfoSpec{commodity}");
                                string specs = $"Commodity:{commodity}";
                                if (!string.IsNullOrEmpty(uom)) {
                                    specs = $"uom:{uom.ToLower()}|{specs}";
                                }
                                if (!string.IsNullOrEmpty(seriesColInfoSpec)) {
                                    string[] specPairs = seriesColInfoSpec.Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                                    foreach (string specPair in specPairs) {
                                        string[] spec = specPair.Split(new[] { ':' }, StringSplitOptions.RemoveEmptyEntries);
                                        if (spec.GetUpperBound(0) > 0) {
                                            if (colIdxMap.ContainsKey(spec[0])) {
                                                specs = $"{specs}|{spec[0]}:{colData[colIdxMap[spec[0]]]}";
                                            }
                                            else {
                                                specs = $"{specs}|{specPair}";
                                            }
                                        }

                                    }
                                    seriesId = !string.IsNullOrEmpty(specs) ? $"{seriesId}|{specs}" : seriesId;
                                }

                                string seriesKey = GetDataSeriesKey(pa, seriesId);
                                if (string.IsNullOrEmpty(seriesKey)) {
                                    if (!newSeriesSpecs.ContainsKey(seriesId)) {
                                        SeriesCollectionSpecification newCollSpec =
                                            (SeriesCollectionSpecification)AddSingle(pa,
                                                new SeriesCollectionSpecification(_starter) {
                                                    CollectionId = spIdForColl,
                                                    AltCollectionId = spIdForColl,
                                                    TClid = clientId,
                                                    TCustid = colData[colIdxMap["Service_Point_ID"]],
                                                    TAcctid = "na",
                                                    Specs = specs
                                                }, 0, cmd);
                                        AddSeriesToCollection(pa, con, numSeries, newCollSpec, cmd, ref ss);
                                        seriesKey = ss.Id.ToString();
                                        //InitializeCache(pa, true); // BUGBUG: for initial pass -- wicked inefficient!!!!!
                                        //seriesKey = GetDataSeriesKey(pa, spIdForColl);
                                        newSeriesCount++;
                                        newSeriesSpecs.Add(seriesId, ss);
                                        context.IncrementCounter("addedSeries");
                                    }
                                    else {
                                        ss = newSeriesSpecs[seriesId];
                                    }
                                }
                                if (ss == null) {
                                    ss = root.GetDataSeriesKey(pa, seriesKey);
                                }

                                double val;
                                double.TryParse(colData[colIdxMap["Measurement_Value"]], out val);
                                var datetime = DateTimeOffset.Parse(colData[colIdxMap["DateTime"]], null, DateTimeStyles.AssumeUniversal);
                                List<KeyValuePair<string, string>> metaData = new List<KeyValuePair<string, string>>();
                                foreach (KeyValuePair<string, string> nmm in nameMetaMaps) {
                                    if ((colIdxMap.ContainsKey(nmm.Key) && colIdxMap[nmm.Key] >= 0)) {
                                        metaData.Add(new KeyValuePair<string, string>(nmm.Value, colData[colIdxMap[nmm.Key]]));
                                    }
                                }
                                ss.CsvGenerator(pa, sw, ss, val, datetime.UtcDateTime, metaData);
                                context.IncrementCounter("xformRows");
                                context.NoFixes.AddLine(csvVals);
                            }
                            catch (Exception exc) {
                                context.AutoFixes.AddLine(csvVals + $" -- {exc.Message},{exc.InnerException?.Message}");
                                context.IncrementCounter("errors");
                                errorCount++;
                                if (errorCount > errorThreshold && pa.GetControlVal("OverwritePreProcErrorThreshold", 0) == 1) {
                                    context.ContextWrite("Transform2SeriesImport", $"Exception - Exceeds PreProceErrorThreshold {errorThreshold}, process aborted");
                                }
                            }
                        }
                        
                        TimingHelper.RunningTimes(context, dtStt, stest, "Transform2SeriesImport", "end");
                        if (newSeriesCount > 0) {
                            context.ContextWrite("Transform2SeriesImport", $"added {newSeriesCount} new records so reinit cache ");
                            InitializeCache(pa, true);
                            TimingHelper.RunningTimes(context, dtStt, stest, "Transform2SeriesImport", "cache reset");
                        }
                    }
                }
                if (tasks.Count > 0) {
                    Task.WhenAll(tasks);
                    tasks.Clear();
                }
                if (swIsOutputFile) {
                    sw.Close();
                }
                else {
                    sw.Flush();
                    pa["retContent"] = Encoding.UTF8.GetString(memoryStream.ToArray(), 0, (int)memoryStream.Length);  // BUGBUG?:  constraint of 4GB
                }

            }
        }


        /// <summary>
        /// get ready to start inserting data
        /// </summary>
        /// <param name="pa"></param>
        /// <param name="quantity"></param>
        /// <param name="con"></param>
        /// <returns></returns>
        private DataSet PrepareForeAdditions(ProcessingArgs pa, int quantity, SqlConnection con) {
            string cmdTxt = string.Format(SelSomeFormat, quantity + 1);
            DataSet ds = SqlUtils.ExecuteDataset(con, CommandType.Text, cmdTxt, null);
            if (ds.Tables.Count == 0) {
                throw new Exception("Generate: no SeriesCollectionInfo table in DB");
            }
            if (ds.Tables[0].Rows.Count == 0) {
                // first is the seed (should be done in DB create script, but handle here if not)
                SqlCommand cmd = new SqlCommand {
                    CommandType = CommandType.Text,
                    Connection = con
                };
                SeriesCollectionSpecification seed = new SeriesCollectionSpecification(_starter) {
                    Specs = "seedMeterSpecs",
                    CollectionId = "InitialCollectionId"
                };
                AddSingle(pa, seed, 0, cmd);
                ds = SqlUtils.ExecuteDataset(con, CommandType.Text, string.Format(SelSomeFormat, quantity + 1), null);
            }
            return ds;
        }

        public void AddMultiple(ProcessingArgs pa, DataTable dt, SqlConnection con, int quantity) {
            AddMultiple(pa, dt, con, quantity, 0);
        }

        /// <summary>
        /// Add some meters for testing data gen
        /// </summary>
        /// <param name="pa"></param>
        /// <param name="dt"></param>
        /// <param name="con"></param>
        /// <param name="quantity"></param>
        /// <param name="numSeries"></param>
        public void AddMultiple(ProcessingArgs pa, DataTable dt, SqlConnection con, int quantity, int numSeries) {
            List<string> colNamesTypes = dt.Columns.Cast<DataColumn>().Select(c => c.ColumnName + "," + c.DataType).ToList();
            foreach (string colNamesType in colNamesTypes) {
                Console.WriteLine($"{colNamesType}");
            }
            SqlCommand cmd = new SqlCommand {
                CommandType = CommandType.Text,
                Connection = con
            };
            cmd.Parameters.Add("@ID", SqlDbType.BigInt, 8).Direction = ParameterDirection.Output;

            var tasks = new List<Task>();
            int maxTasks = 20;

            while (quantity-- > 0) {
                long mid = (quantity + 1) * 1000; // to give us a sequence
                SeriesCollectionSpecification ms =
                    (SeriesCollectionSpecification)
                    AddSingle(pa, new SeriesCollectionSpecification(_starter) { CollectionId = mid.ToString(), Specs = "newspec:" + mid }, mid,
                        cmd);
                if (numSeries > 0) {
                    tasks.Add(AddSeriesToCollectionAsync(pa, con, numSeries, ms, cmd));
                    if (tasks.Count > maxTasks) {
                        Task.WhenAll(tasks);
                        tasks.Clear();
                    }
                }
            }
            if (tasks.Count > 0) {
                Task.WhenAll(tasks);
                tasks.Clear();
            }

        }


        /// <summary>
        /// Greate a new series and add to collection
        /// </summary>
        /// <param name="pa"></param>
        /// <param name="con"></param>
        /// <param name="numSeries"></param>
        /// <param name="ms"></param>
        /// <param name="cmdOrig"></param>
        /// <param name="newSeriesSpec"></param>
        void AddSeriesToCollection(ProcessingArgs pa, SqlConnection con, int numSeries, SeriesCollectionSpecification ms, SqlCommand cmdOrig, ref SeriesSpecification newSeriesSpec) {
            string seriesList = ms.TimeSeries;
            SeriesSpecification sps = new SeriesSpecification(pa, null as DataRow);
            SqlCommand cmd = new SqlCommand {
                CommandType = CommandType.Text,
                Connection = cmdOrig.Connection
            };
            cmd.Parameters.Add("@ID", SqlDbType.BigInt, 8).Direction = ParameterDirection.Output;
            for (int i = 0; i < numSeries; i++) {
                SeriesSpecification nsp = (SeriesSpecification)sps.AddSingle(pa, sps, ms.Id, cmd);
                seriesList += $"|{nsp.Id}";
                if (newSeriesSpec == null) newSeriesSpec = nsp;
            }
            ms.TimeSeries = seriesList;
            SqlCommand cmd2 = new SqlCommand {
                CommandType = CommandType.Text,
                Connection = con
            };
            ms.UpdateSingle(cmd2);
        }


        async Task<bool> AddSeriesToCollectionAsync(ProcessingArgs pa, SqlConnection con, int numSeries, SeriesCollectionSpecification ms,
            SqlCommand cmdOrig) {
            string seriesList = ms.TimeSeries;
            SeriesSpecification sps = new SeriesSpecification(pa, null as DataRow);
            SqlCommand cmd = new SqlCommand {
                CommandType = CommandType.Text,
                Connection = cmdOrig.Connection
            };
            cmd.Parameters.Add("@ID", SqlDbType.BigInt, 8).Direction = ParameterDirection.Output;
            for (int i = 0; i < numSeries; i++) {
                SeriesSpecification nsp = (SeriesSpecification)sps.AddSingle(pa, sps, ms.Id, cmd);
                seriesList += $"|{nsp.Id}";
            }
            ms.TimeSeries = seriesList;
            SqlCommand cmd2 = new SqlCommand {
                CommandType = CommandType.Text,
                Connection = con
            };
            ms.UpdateSingle(cmd2);
            await Task.Run(() => true);
            return true;

        }

        /// <summary>
        /// add a single series spec to the store
        /// </summary>
        /// <param name="pa"></param>
        /// <param name="src"></param>
        /// <param name="key"></param>
        /// <param name="cmd"></param>
        /// <returns></returns>
        public IDataSpecification AddSingle(ProcessingArgs pa, IDataSpecification src, long key, SqlCommand cmd) {
            SeriesCollectionSpecification spec = new SeriesCollectionSpecification(src);
            cmd.CommandText = spec.FormattedSaveStg();
            cmd.ExecuteNonQuery();
            spec.Id = (long)cmd.Parameters["@ID"].Value;
            return spec;
        }



        /// <summary>
        /// update a single series spec
        /// </summary>
        /// <param name="cmd"></param>
        /// <returns></returns>
        public IDataSpecification UpdateSingle(SqlCommand cmd) {
            cmd.CommandText = SpecUpdateFormat;
            object[] parValues = GetObjs();
            for (int i = 0; i < parValues.Length; i++) {
                cmd.Parameters.AddWithValue(_paramsForUpdate[i], parValues[i]);
            }
            cmd.ExecuteNonQuery();
            return this;
        }


        #region  testDataGeneration for test/QA/populating



        public string TestGetData(ProcessingArgs pa, string id) {
            string resp = "";
            if (_idCache == null) {
                InitializeCache(pa);
            }
            if (_idCache != null) {
                DataRow[] rows = _idCache.Select(id);
                if (rows.Length > 0) {
                    DataColumnCollection colNames = rows[0].Table.Columns;
                    DataTable dt = new DataTable("ActionTable");
                    foreach (DataColumn column in colNames) {
                        dt.Columns.Add(new DataColumn() { ColumnName = column.ColumnName, DataType = column.DataType });
                    }
                    foreach (DataRow dataRow in rows) {
                        dt.ImportRow(dataRow);
                    }
                    var objType =
                        JArray.FromObject(dt,
                                JsonSerializer.CreateDefault(new JsonSerializerSettings { NullValueHandling = NullValueHandling.Ignore }))
                            .FirstOrDefault(); // Get the first row            
                    if (objType != null) resp = objType.ToString();
                }
            }
            return resp;
        }

        /// <summary>
        /// populate some data
        /// </summary>
        /// <param name="pa"></param>
        public void Generate(ProcessingArgs pa) {
            int quantity = pa.GetControlVal("nummeters", -1);
            int numSeries = pa.GetControlVal("numSeries", -1);
            if (quantity < 0) {
                quantity = pa.GetControlVal("quantity", 1000);
            }
            string sqlMeterMgtCxnStg = pa["sqlMeterMgtCxnStg"];
            using (SqlConnection con = new SqlConnection(sqlMeterMgtCxnStg)) {
                con.Open();
                var ds = PrepareForeAdditions(pa, quantity, con);
                DataTable dt = ds.Tables[0];
                if (dt.Rows.Count < quantity + 1) {
                    // first is the seed 
                    AddMultiple(pa, dt, con, (quantity + 1) - dt.Rows.Count, numSeries);
                }
            }
        }


        /// <summary>
        /// Place to generate information that goes into Cassandra for this artifact.
        /// Meter is in SQL but might have some Cassandra footprint.
        /// currently generating from billing data but might update/reuse this method for disconnected series collections.
        /// </summary>
        /// <param name="pa"></param>
        public void GenerateData(ProcessingArgs pa) {
            //List<SeriesCollectionSpecification> specs = new List<SeriesCollectionSpecification>();
            string sqlMeterMgtCxnStg = pa["sqlMeterMgtCxnStg"];
            int quantity = pa.GetControlVal("nummeters", -1);
            if (quantity < 0) {
                quantity = pa.GetControlVal("quantity", 1000);
            }

            using (SqlConnection con = new SqlConnection(sqlMeterMgtCxnStg)) {
                con.Open();
                string cmdTxt = string.Format(SelSomeFormat, quantity + 1);
                DataSet ds = SqlUtils.ExecuteDataset(con, CommandType.Text, cmdTxt, null);
                if (ds.Tables.Count == 0) {
                    throw new Exception("GenerateData: no SeriesCollectionInfo table in DB");
                }
                ds = SqlUtils.ExecuteDataset(con, CommandType.Text, string.Format(SelSomeFormat, quantity + 1), null);
                DataTable dt = ds.Tables[0];
                bool first = true;
                foreach (DataRow dataRow in dt.Rows) {
                    if (first) {
                        // first one addeed was the starter template
                        first = false;
                        continue;
                    }
                    SeriesCollectionSpecification unused = new SeriesCollectionSpecification(dataRow);
                    //specs.Add(new SeriesCollectionSpecification(dataRow));
                }
            }
        }


        /// <summary>
        /// Place to generate information that goes into Cassandra for this artifact.
        /// Initially just generate for the first of each series spec in the collection (this is the classic singularly supported case)
        /// </summary>
        /// <param name="pa"></param>
        /// <param name="context"></param>
        public void GenerateDataFromSeries(ProcessingArgs pa, ContextData context) {
            List<SeriesCollectionSpecification> specs = new List<SeriesCollectionSpecification>();
            string dataPath = DataUtils1.GetStorePath(pa);
            int gendata = pa.GetControlVal("gendata", 0);
            string sqlMeterMgtCxnStg = pa["sqlMeterMgtCxnStg"];
            int numSeries = pa.GetControlVal("numseries", 1000);
            double minutes = pa.GetControlVal("minutes", 5);
            DateTime dtStt = pa.GetControlVal("dtstt", DateTime.Parse("1/1/2017"));
            DateTime dtStp = pa.GetControlVal("dtstp", DateTime.Parse("1/2/2017"));
            double hours = dtStp.Subtract(dtStt).TotalHours;

            using (SqlConnection con = new SqlConnection(sqlMeterMgtCxnStg)) {
                con.Open();
                string cmdTxt = string.Format(SelSomeFormat, numSeries + 1);
                DataSet ds = SqlUtils.ExecuteDataset(con, CommandType.Text, cmdTxt, null);
                if (ds.Tables.Count == 0) {
                    throw new Exception("no SeriesCollectionInfo table in DB");
                }
                string cmdText = string.Format(SelSomeFormat, numSeries + 1);
                ds = SqlUtils.ExecuteDataset(con, CommandType.Text, cmdText, null);

                DataTable dt = ds.Tables[0];
                bool first = true;
                foreach (DataRow dataRow in dt.Rows) {
                    if (first) {
                        // first one addeed was the starter template
                        first = false;
                        continue;
                    }
                    specs.Add(new SeriesCollectionSpecification(pa, dataRow));
                }
                if (specs.Count > 0) {
                    string fileName = $"seriesvalues_{hours}_{minutes}_{numSeries}_{dtStt:yyyyMMdd}_{dtStp:yyyyMMdd}.csv";
                    string outputFile = Path.Combine(dataPath, fileName);
                    context.ContextWrite($"outputFile = {outputFile}");
                    using (StreamWriter sw = new StreamWriter(outputFile)) {
                        sw.WriteLine(CsvSeriesDataHeaderCols);
                        foreach (var seriesSpec in specs) {
                            try {
                                string[] timeSeries = seriesSpec.TimeSeries.Trim().Split(new[] { '|' }, StringSplitOptions.RemoveEmptyEntries);
                                SeriesSpecification ss = SeriesSpecification.GetSeriesSpec(pa, seriesSpec.Id.ToString(), timeSeries[0], con);

                                ss.DataGenerator(pa, sw, ss);
                                if (--gendata <= 0) break;
                            }
                            catch (Exception) {
                                // eat it -- some invalid populated values during initial dev and testing
                            }
                        }
                    }
                }
            }
        }


        #endregion



    }




    /// <summary>
    /// Specification for a series
    /// </summary>
    public class SeriesSpecification : ISeriespecification {
        /// <summary>
        /// Get the cassandra series to store some data
        /// this is dev POC -- will be caching this stuff
        /// </summary>
        /// <param name="pa"></param>
        /// <param name="collid"></param>
        /// <param name="seriesSpec"></param>
        /// <param name="con"></param>
        /// <returns></returns>
        public static SeriesSpecification GetSeriesSpec(ProcessingArgs pa, string collid, string seriesSpec, SqlConnection con = null) {
            string sqlMeterMgtCxnStg = pa["sqlMeterMgtCxnStg"];
            DataTable dt;
            string cmdTxt = string.Format(SpecQueryFormat, collid);
            DataSet ds;
            if (con == null) {
                using (con = new SqlConnection(sqlMeterMgtCxnStg)) {
                    con.Open();
                    ds = SqlUtils.ExecuteDataset(con, CommandType.Text, cmdTxt, null);
                }
            }
            else {
                ds = SqlUtils.ExecuteDataset(con, CommandType.Text, cmdTxt, null);
            }
            if (ds.Tables.Count == 0) {
                throw new Exception("SeriesSpecification.GetSeriesSpec: no SeriesCollectionInfo table in DB");
            }
            dt = ds.Tables[0];
            if (dt.Rows.Count == 0) {
                throw new Exception($"SeriesSpecification.GetSeriesSpec: invalid collectionId {collid}");
            }
            SeriesSpecification scs = new SeriesSpecification(pa, dt.Rows[0]);
            return scs;
        }


        //private static DataTable _systemCache;
        private static CollSpecIdentityCache _idCache;
        private static string _cache_padlock = "SeriesSpecificationCachePadlock";

        public void InitializeCache(ProcessingArgs pa, bool force = false) {
            if (_idCache == null | force) {
                lock (_cache_padlock) {
                    if (_idCache == null | force) {
                        if (_idCache != null) {
                            try {
                                _idCache.Dispose();
                            }
                            catch (Exception exc) {
                                Trace.WriteLine($"SeriesSpecification.InitializeCache exception {exc.Message}, {exc.InnerException?.Message}");
                            }
                            _idCache = null;
                        }
                        _idCache = new CollSpecIdentityCache(pa, HashKeyColumns, TableName);
                    }
                }
            }
        }



        /// <summary>
        /// get the series specification for a particular series (used to prepare for encoding data for storage).
        /// </summary>
        /// <param name="pa"></param>
        /// <param name="id"></param>
        /// <returns></returns>
        public SeriesSpecification GetDataSeriesKey(ProcessingArgs pa, string id) {
            if (_idCache == null) {
                InitializeCache(pa);
            }

            if (_idCache == null)
                throw new Exception("cache is still null after initialization");

            //long tval = 0;
            //long.TryParse(id, out tval);
            DataRow[] rows = _idCache.Select(id);
            SeriesSpecification sspec = null;
            if (rows?.Length > 0) {
                sspec = new SeriesSpecification(pa, rows[0]);
            }
            return sspec;
        }


        //public RowSet GetSeriesDataold(ProcessingArgs pa, ContextData context, string id, CassConn1 casCxn) {
        //    RowSet rs = null;
        //    string qryLine = "";
        //    DataRow[] rows = idCache.Select($"id={id}");
        //    if (rows.Length > 0) {
        //        DateTime stt = DateTime.Parse(pa["dtStt"]);
        //        DateTime stp = DateTime.Parse(pa["dtStp"]);
        //        string stts = stt.ToString((string) rows[0]["TSSpec"]);
        //        string stps = stp.ToString((string) rows[0]["TSSpec"]);

        //        List<string> queryStgs = GetQuery(pa, id, stt, stp);

        //        string timeSelect = $" ts> '{stts}' and ts<'{stps}' ";
        //        string dids = stt.ToString((string) rows[0]["DidSpec"]);
        //        string parSelect = $" id={rows[0]["id"]} and did={dids} ";
        //        pa["qSel"] = $"where {parSelect} and {timeSelect}";
        //        pa["reqVsrc"] = "value_series_meta";
        //        pa["qRows"] = "*";
        //        return casCxn.PerformCasQuery(pa, ref qryLine);
        //    }
        //    return rs;
        //}

        /// <summary>
        /// Probe for returning series specification data
        /// </summary>
        /// <param name="pa"></param>
        /// <param name="id"></param>
        /// <returns></returns>
        public string GetData(ProcessingArgs pa, string id) {
            string resp = "nada";
            if (_idCache == null) {
                InitializeCache(pa);
            }
            //long tval = 0;
            //long.TryParse(id, out tval);
            if (_idCache == null)
                throw new Exception("cache is null after initizlation");
            DataRow[] rows = _idCache.Select(id);
            if (rows?.Length > 0) {
                DataColumnCollection colNames = rows[0].Table.Columns;
                DataTable dt = new DataTable("ActionTable");
                foreach (DataColumn column in colNames) {
                    dt.Columns.Add(new DataColumn() { ColumnName = column.ColumnName, DataType = column.DataType });
                }
                foreach (DataRow dataRow in rows) {
                    dt.ImportRow(dataRow);
                }
                var objType =
                    JArray.FromObject(dt,
                            JsonSerializer.CreateDefault(new JsonSerializerSettings { NullValueHandling = NullValueHandling.Ignore }))
                        .FirstOrDefault(); // Get the first row            
                if (objType == null)
                    throw new Exception("data table is empty");
                resp = objType.ToString();
            }
            return resp;
        }




        /// <summary>
        /// return the query for getting data pertaining to a particular series spec.
        /// Should generate and return multiple queries if required (e.g., crossing Cassandra buckets).
        /// To do this generate multiple queries such that 'did' covers the partition key from the start to the end.
        /// </summary>
        /// <param name="pa"></param>
        /// <param name="id"></param>
        /// <param name="dtStt"></param>
        /// <param name="dtStp"></param>
        /// <returns></returns>
        public List<string> GetQuery(ProcessingArgs pa, string id, DateTime dtStt, DateTime dtStp) {
            List<string> qryList = new List<string>();
            if (_idCache == null) {
                InitializeCache(pa);
            }

            if (_idCache == null)
                throw new Exception("cache is still null after initialization");
            //long tval = 0;
            //long.TryParse(id, out tval);
            DataRow[] rows = _idCache.Select(id);
            if (rows?.Length > 0) {
                SeriesSpecification sspec = new SeriesSpecification(null, rows[0]);
                string qry = $"select * from {CassTableName} where id={sspec.Id} and did={dtStt.ToString(sspec.DidSpec)} and ts>='{dtStt.ToString(sspec.TsSpec)}' and ts<='{dtStp.ToString(sspec.TsSpec)}'; ";
                qryList.Add(qry);
                switch (sspec.DidSpec) {
                    case "yyyyMM": { // monthly buckets.
                        DateTime operDateTime = dtStt;
                        int clamp = 48; // allow up to 4 year span in query
                        while ((operDateTime.Year != dtStp.Year) || (operDateTime.Month != dtStp.Month)) {
                            operDateTime = operDateTime.AddMonths(1);
                            qry = $"select * from {CassTableName} where id={sspec.Id} and did={operDateTime.ToString(sspec.DidSpec)} and ts>='{dtStt.ToString(sspec.TsSpec)}' and ts<='{dtStp.ToString(sspec.TsSpec)}'; ";
                            qryList.Add(qry);
                            if (clamp-- <= 0) break;
                        }
                    }
                        break;
                }
            }
            return qryList;
        }

        /// <summary>
        /// return the query for getting data pertaining to a particular series spec.
        /// Should generate and return multiple queries if required (e.g., crossing Cassandra buckets).
        /// To do this generate multiple queries such that 'did' covers the partition key from the start to the end.
        /// </summary>
        /// <param name="pa"></param>
        /// <param name="id"></param>
        /// <param name="dtStt"></param>
        /// <param name="dtStp"></param>
        /// <returns></returns>
        public List<string> GetQuery(ProcessingArgs pa, string id, DateTimeOffset dtStt, DateTimeOffset dtStp) {
            List<string> qryList = new List<string>();
            if (_idCache == null) {
                InitializeCache(pa);
            }
            if (_idCache == null)
                throw new Exception("cache is still null after initialization");

            //long tval = 0;
            //long.TryParse(id, out tval);
            DataRow[] rows = _idCache.Select(id);
            if (rows?.Length > 0) {
                string metaCondition = "";
                string metaName = pa.GetControlVal("metaQualName", "");
                string metaVal = pa.GetControlVal("metaQualVal", "");
                if (!string.IsNullOrEmpty(metaName) && !string.IsNullOrEmpty(metaVal)) {
                    metaCondition = $" and meta['{metaName}']='{metaVal}' ";
                }
                SeriesSpecification sspec = new SeriesSpecification(null, rows[0]);
                string qry = $"select * from {CassTableName} where id={sspec.Id} and did={dtStt.UtcDateTime.ToString(sspec.DidSpec)} and ts>='{dtStt.UtcDateTime.ToString(sspec.TsSpec)}' and ts<='{dtStp.UtcDateTime.ToString(sspec.TsSpec)}' {metaCondition}; ";
                qryList.Add(qry);
                switch (sspec.DidSpec) {
                    case "yyyyMM": { // monthly buckets.
                        DateTimeOffset operDateTime = dtStt;
                        int clamp = 48; // allow up to 4 year span in query
                        while ((operDateTime.Year != dtStp.Year) || (operDateTime.Month != dtStp.Month)) {
                            operDateTime = operDateTime.AddMonths(1);
                            qry = $"select * from {CassTableName} where id={sspec.Id} and did={operDateTime.UtcDateTime.ToString(sspec.DidSpec)} and ts>='{dtStt.UtcDateTime.ToString(sspec.TsSpec)}' and ts<='{dtStp.UtcDateTime.ToString(sspec.TsSpec)}' {metaCondition}; ";
                            qryList.Add(qry);
                            if (clamp-- <= 0) break;
                        }
                    }
                        break;
                }
            }
            return qryList;
        }


        public const string DefaultDidSpec = "yyyyMM";
        public const string DefaultTsSpec = "yyyy-MM-dd HH:mm:ssZ";
        private const string TableName = "SeriesInfo";
        private const string CassTableName = "value_series_meta";
        private const string HashKeyColumns = "id";  // for cache -- columns to be used

        /// <summary>
        /// Main use of SQL server is as a resting place for series management data stores.  In memory is where the bulk of the usage takes place.
        /// we want to avoid lock in to sql server, so staying strait TSql for most of the interactions with the DB (e.g., avoid stored procedures, etc.).
        /// This is relatively simple storage and processing so we are embedding the 'DAL' in the class.
        /// </summary>

        private static string SpecQueryFormat = $"SELECT TOP 1 * FROM {TableName} where CollectionKey='{{0}}' order by id asc";

        private static string FirstSpecQuery = $"SELECT TOP 1 * FROM {TableName} order by id asc";

        private static string SpecSaveFormat =
            $"INSERT INTO [dbo].[{TableName}] ([Specs],[CollectionKey],[DidSpec],[TsSpec]) VALUES('{{0}}','{{1}}','{{2}}','{{3}}') SET @ID = SCOPE_IDENTITY();";

        private static string SpecUpdateFormat =
            $"UPDATE [dbo].[{TableName}] set Specs=@pSpecs,CollectionKey=@pCollectionKey,DidSpec=@pDidSpec,TSSpec=@pTSSpec where id = @pId";

        private static string SelSomeFormat = $"SELECT TOP {{0}} * FROM {TableName}";
        /*
                private static string _selSomeMinFormat = $"SELECT TOP {{0}} [id],[CollectionKey],[DidSpec],[TsSpec] FROM {TableName}";
        */
        private static string SelCountFormat = $"SELECT count(*) FROM {TableName}";
        private static string CsvSeriesDataHeaderCols = "id,ds,ts,val,qual,meta";


        private readonly string[] _paramsForUpdate = { "@pId", "@pSpecs", "@pCollectionKey", "@pDidSpec", "@pTsSpec" };

        object[] GetObjs() {
            object[] objs = { Id, Specs, CollectionKey, DidSpec, TsSpec };
            return objs;
        }

        // helper for constructing query
        public string FormattedSaveStg() {
            return string.Format(SpecSaveFormat, Specs, CollectionKey, DidSpec, TsSpec);
        }

        /// <summary>
        /// the data which is stored for us
        /// </summary>
        private DataRow storeData;


        /// <summary>
        /// constructors of various forms
        /// </summary>
        /// <param name="pa"></param>
        /// <param name="source"></param>
        public SeriesSpecification(ProcessingArgs pa, object source = null) {
            if (source == null) {
                // ReSharper disable once ExpressionIsAlwaysNull
                storeData = (DataRow)Starter(pa, source);
            }
            else {
                storeData = (DataRow)source;
            }
        }

        public SeriesSpecification(ProcessingArgs pa, DataRow source) {
            storeData = source;
            if (storeData == null) {
                storeData = (DataRow)Starter(pa, source);
            }
        }

        public SeriesSpecification(ProcessingArgs pa, IDataSpecification source) {
            if (((SeriesSpecification)source).storeData == null) {
                // a null one from the outside.  Seed it.
                source = new SeriesSpecification(pa, _starter);
            }
            storeData = ((SeriesSpecification)source).storeData.Table.NewRow();
            storeData.ItemArray = (object[])((SeriesSpecification)source).storeData.ItemArray.Clone();
        }

        /// <summary>
        /// formatted content for logging, viewing
        /// </summary>
        /// <returns></returns>
        public string Show() {
            return string.Format($"id={Id},CollectionKey='{CollectionKey}',Specs='{Specs}',DidSpec='{DidSpec}',TsSpec='{TsSpec}'");
        }

        /// <summary>
        /// These should be made private and potentially eliminated as they are just implemented for quickly developing the initial implementation.
        /// Ideally should not be creating code couplings to the schema/store
        /// </summary>
        public string DidSpec {
            get { return (string)storeData["DidSpec"]; }
            set { storeData["DidSpec"] = value; }
        }

        public string TsSpec {
            get { return (string)storeData["TSSpec"]; }
            set { storeData["TSSpec"] = value; }
        }

        public long Id {
            get { return (long)storeData["id"]; }
            set { storeData["id"] = value; }
        }

        public long CollectionKey {
            get { return (long)storeData["CollectionKey"]; }
            set { storeData["CollectionKey"] = value; }
        }

        public string Specs {
            get { return (string)storeData["Specs"]; }
            set { storeData["Specs"] = value; }
        }

        public string Statistics(ProcessingArgs pa) {
            string sqlMeterMgtCxnStg = pa["sqlMeterMgtCxnStg"];
            int count;
            using (SqlConnection con = new SqlConnection(sqlMeterMgtCxnStg)) {
                con.Open();
                SqlCommand cmd = new SqlCommand(SelCountFormat, con);
                count = (int)cmd.ExecuteScalar();
            }
            return $" count={count}";
        }


        private static DataRow _starter; // initialization template to avoid persistance hits when generating instances
        private static readonly string _padlock = "SeriesSpec.Starter.padlock";

        /// <summary>
        /// initialization of statics for this component
        /// </summary>
        /// <param name="pa"></param>
        /// <param name="context"></param>
        /// <param name="source"></param>
        public static void Initialize(ProcessingArgs pa, ContextData context, object source = null) {
            if (_starter == null) {
                lock (_padlock) {
                    if (_starter == null) {
                        try {
                            if (source != null) {
                                DataSet ds = SqlUtils.ExecuteDataset((SqlConnection)source, CommandType.Text, FirstSpecQuery, null);
                                if (ds.Tables.Count > 0) {
                                    _starter = ds.Tables[0].NewRow();
                                    if (ds.Tables[0].Rows.Count > 0) {
                                        _starter.ItemArray = (object[])ds.Tables[0].Rows[0].ItemArray.Clone();
                                    }
                                }
                            }
                            else {
                                string sqlMeterMgtCxnStg = pa["sqlMeterMgtCxnStg"];
                                using (SqlConnection conn = new SqlConnection(sqlMeterMgtCxnStg)) {
                                    DataSet ds = SqlUtils.ExecuteDataset(conn, CommandType.Text, FirstSpecQuery, null);
                                    if (ds.Tables.Count > 0) {
                                        _starter = ds.Tables[0].NewRow();
                                        if (ds.Tables[0].Rows.Count > 0) {
                                            _starter.ItemArray = (object[])ds.Tables[0].Rows[0].ItemArray.Clone();
                                        }
                                    }
                                }
                            }
                        }
                        catch (Exception exc) {
                            Debug.WriteLine($"SeriesSpec.Starter {exc.Message}, {exc.InnerException?.Message}, {exc.StackTrace}");
                            context?.ContextWrite("error_exception",
                                $"SeriesSpecificiation.Initialize ={exc.Message}, {exc.InnerException?.Message}");
                        }
                    }
                }
            }

        }

        /// <summary>
        /// Spin up this puppy with it's seed and make sure we are ready to go with this component.
        /// </summary>
        /// <param name="pa"></param>
        /// <param name="source"></param>
        /// <returns></returns>
        public object Starter(ProcessingArgs pa, object source = null) {
            DataRow rVal = null;
            if (_starter == null) {
                Initialize(pa, null, source);
            }
            if (_starter != null) {
                rVal = _starter.Table.NewRow();
                rVal.ItemArray = (object[])_starter.ItemArray.Clone();
            }
            return rVal;
        }




        /// <summary>
        /// Return 0 or more results from our data store
        /// </summary>
        /// <param name="pa"></param>
        /// <param name="cxn"></param>
        /// <returns></returns>
        public DataTable GetSome(ProcessingArgs pa, SqlConnection cxn = null) {
            int quantity = pa.GetControlVal("nummeters", -1);
            if (quantity < 0) {
                quantity = pa.GetControlVal("quantity", 1000);
            }
            DataTable dt;
            string cmdTxt = string.Format(SelSomeFormat, quantity + 1);

            if (cxn == null) {
                string sqlMeterMgtCxnStg = pa["sqlMeterMgtCxnStg"];
                using (SqlConnection con = new SqlConnection(sqlMeterMgtCxnStg)) {
                    con.Open();
                    DataSet ds = SqlUtils.ExecuteDataset(con, CommandType.Text, cmdTxt, null);
                    if (ds.Tables.Count == 0) {
                        throw new Exception("GetSome: no SeriesCollectionInfo table in DB");
                    }
                    dt = ds.Tables[0];
                }
            }
            else {
                DataSet ds = SqlUtils.ExecuteDataset(cxn, CommandType.Text, cmdTxt, null);
                if (ds.Tables.Count == 0) {
                    throw new Exception("GetSome: no SeriesCollectionInfo table in DB");
                }
                dt = ds.Tables[0];
            }
            return dt;
        }


        /// <summary>
        /// Add some series specifications for testing 
        /// </summary>
        /// <param name="pa"></param>
        /// <param name="dt"></param>
        /// <param name="con"></param>
        /// <param name="quantity"></param>
        public void AddMultiple(ProcessingArgs pa, DataTable dt, SqlConnection con, int quantity) {
            List<string> colNamesTypes = dt.Columns.Cast<DataColumn>().Select(c => c.ColumnName + "," + c.DataType).ToList();
            foreach (string colNamesType in colNamesTypes) {
                Console.WriteLine($"{colNamesType}");
            }
            SqlCommand cmd = new SqlCommand {
                CommandType = CommandType.Text,
                Connection = con
            };

            while (quantity-- > 0) {
                AddSingle(pa, new SeriesSpecification(pa, _starter), (quantity + 1) * 1000, cmd);
            }
        }

        /// <summary>
        /// Persist a single se ries
        /// </summary>
        /// <param name="collectionKey"></param>
        /// <param name="cmd"></param>
        /// <param name="pa"></param>
        /// <param name="src"></param>
        public IDataSpecification AddSingle(ProcessingArgs pa, IDataSpecification src, long collectionKey, SqlCommand cmd) {
            SeriesSpecification spec = new SeriesSpecification(pa, src);
            spec.Specs += $":{collectionKey}";
            spec.CollectionKey = collectionKey;
            cmd.CommandText = spec.FormattedSaveStg();
            cmd.ExecuteNonQuery();
            spec.Id = (long)cmd.Parameters["@ID"].Value;
            return spec;
        }



        public IDataSpecification UpdateSingle(SqlCommand cmd) {
            cmd.CommandText = SpecUpdateFormat;
            object[] parValues = GetObjs();
            for (int i = 0; i < parValues.Length; i++) {
                cmd.Parameters.AddWithValue(_paramsForUpdate[i], parValues[i]);
            }
            cmd.ExecuteNonQuery();
            return this;
        }


        #region  testDataGeneration for test/QA

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pa"></param>
        public void Generate(ProcessingArgs pa) {
            string sqlMeterMgtCxnStg = pa["sqlMeterMgtCxnStg"];
            int quantity = pa.GetControlVal("numseries", -1);
            if (quantity < 0) {
                quantity = pa.GetControlVal("quantity", 1000);
            }

            using (SqlConnection con = new SqlConnection(sqlMeterMgtCxnStg)) {
                con.Open();
                string cmdTxt = string.Format(SelSomeFormat, quantity + 1);
                DataSet ds = SqlUtils.ExecuteDataset(con, CommandType.Text, cmdTxt, null);
                if (ds.Tables.Count == 0) {
                    throw new Exception("Generate :no SeriesInfo table in DB");
                }
                if (ds.Tables[0].Rows.Count == 0) {
                    // first is the seed 
                    SqlCommand cmd = new SqlCommand {
                        CommandType = CommandType.Text,
                        Connection = con
                    };
                    AddSingle(pa,
                        new SeriesSpecification(pa, _starter) {
                            DidSpec = DefaultDidSpec,
                            TsSpec = DefaultTsSpec,
                            Specs = "seriesSeedSpecs",
                            CollectionKey = 0
                        }, 0, cmd);
                    ds = SqlUtils.ExecuteDataset(con, CommandType.Text, string.Format(SelSomeFormat, quantity + 1), null);
                }
                DataTable dt = ds.Tables[0];
                if (dt.Rows.Count < quantity + 1) {
                    // first is the seed 
                    AddMultiple(pa, dt, con, (quantity + 1) - dt.Rows.Count);
                }
            }
        }

        /// <summary>
        /// Place to generate information that goes into Cassandra for this artifact.
        /// </summary>
        /// <param name="pa"></param>
        public void GenerateData(ProcessingArgs pa) {
            List<SeriesSpecification> specs = new List<SeriesSpecification>();
            string dataPath = DataUtils1.GetStorePath(pa);
            int gendata = pa.GetControlVal("gendata", 0);
            string sqlMeterMgtCxnStg = pa["sqlMeterMgtCxnStg"];
            int numSeries = pa.GetControlVal("numseries", 1000);
            double minutes = pa.GetControlVal("minutes", 5);
            DateTime dtStt = pa.GetControlVal("dtstt", DateTime.Parse("1/1/2017"));
            DateTime dtStp = pa.GetControlVal("dtstp", DateTime.Parse("1/2/2017"));
            double hours = dtStp.Subtract(dtStt).TotalHours;

            using (SqlConnection con = new SqlConnection(sqlMeterMgtCxnStg)) {
                con.Open();
                string cmdTxt = string.Format(SelSomeFormat, numSeries + 1);
                DataSet ds = SqlUtils.ExecuteDataset(con, CommandType.Text, cmdTxt, null);
                if (ds.Tables.Count == 0) {
                    throw new Exception("no SeriesInfo table in DB");
                }
                ds = SqlUtils.ExecuteDataset(con, CommandType.Text, $"SELECT TOP {numSeries + 1} * FROM SeriesInfo", null);
                DataTable dt = ds.Tables[0];
                bool first = true;
                foreach (DataRow dataRow in dt.Rows) {
                    if (first) {
                        // first one addeed was the starter template
                        first = false;
                        continue;
                    }
                    specs.Add(new SeriesSpecification(pa, dataRow));
                }
            }
            if (specs.Count > 0) {

                string fileName = $"seriesvalues_{hours}_{minutes}_{numSeries}_{dtStt:yyyyMMdd}_{dtStp:yyyyMMdd}.csv";

                string outputFile = Path.Combine(dataPath, fileName);
                using (StreamWriter sw = new StreamWriter(outputFile)) {
                    sw.WriteLine(CsvSeriesDataHeaderCols);
                    foreach (var seriesSpec in specs) {
                        DataGenerator(pa, sw, seriesSpec);
                        if (--gendata <= 0) break;
                    }
                }
            }
        }


        /// <summary>
        /// Generate csv test data covering a particular timespan with a particular granularity.
        /// </summary>
        /// <param name="pa"></param>
        /// <param name="sw"></param>
        /// <param name="seriesSpec"></param>
        public void DataGenerator(ProcessingArgs pa, StreamWriter sw, ISeriespecification seriesSpec) {
            int counter = 0;
            Random rand = new Random((int)seriesSpec.Id);
            double minutes = pa.GetControlVal("minutes", 5);
            DateTime dtStt = pa.GetControlVal("dtstt", DateTime.Parse("1/1/2017"));
            DateTime dtStp = pa.GetControlVal("dtstp", DateTime.Parse("1/2/2017"));

            string meta = "(*( 'm1':'{0}' (*)'m2':'{1}' )*) "; // test data
            DateTime dtOper = dtStt;
            string outputRecord = "";
            while (dtOper.CompareTo(dtStp) < 0) {
                var val = rand.Next(0, 1000) / 10.0;
                string metaVal = string.Format(meta, seriesSpec.Id, counter);
                outputRecord =
                    $"{seriesSpec.Id},{dtOper.ToString(seriesSpec.DidSpec)},{dtOper.ToString(seriesSpec.TsSpec)},{val},{counter},{metaVal}";
                if ((counter % 10000) == 0) Console.WriteLine($"{counter}, {outputRecord}");
                sw.WriteLine(outputRecord);
                dtOper = dtOper.AddMinutes(minutes);
                counter++;
            }
            Console.WriteLine($" count={counter} last={dtOper} is {outputRecord}");
        }




        //public void CsvGenerator(ProcessingArgs pa, StreamWriter sw, ISeriespecification seriesSpec, double val, DateTime dtOper,
        //    string estDesc, string estInd) {
        //    string meta = "(*( 'estDesc':'{0}' (*)'estInd':'{1}' )*) "; // test data
        //    string outputRecord;
        //    string metaVal = string.Format(meta, estDesc, estInd);
        //    outputRecord = $"{seriesSpec.Id},{dtOper.ToString(seriesSpec.DidSpec)},{dtOper.ToString(seriesSpec.TsSpec)},{val},0,{metaVal}";
        //    sw.WriteLine(outputRecord);
        //}

        /// <summary>
        /// make a 'csv' string with meta data we put in a orm that will bass through csv processing until it gets fixed near cassandra
        /// </summary>
        /// <param name="pa"></param>
        /// <param name="sw"></param>
        /// <param name="seriesSpec"></param>
        /// <param name="val"></param>
        /// <param name="dtOper"></param>
        /// <param name="metaData"></param>
        public void CsvGenerator(ProcessingArgs pa, StreamWriter sw, ISeriespecification seriesSpec, double val, DateTime dtOper, List<KeyValuePair<string, string>> metaData) {
            string metaString = "";
            foreach (KeyValuePair<string, string> kvp in metaData) {
                if (!string.IsNullOrEmpty(metaString)) {
                    metaString += " (*)";
                }
                metaString += $" '{kvp.Key}':'{kvp.Value}'";
            }
            string metaVal = $"(*( {metaString} )*) ";
            string outputRecord;
            outputRecord = $"{seriesSpec.Id},{dtOper.ToString(seriesSpec.DidSpec)},{dtOper.ToString(seriesSpec.TsSpec)},{val},0,{metaVal}";
            sw.WriteLine(outputRecord);
        }

        #endregion

    }
}


