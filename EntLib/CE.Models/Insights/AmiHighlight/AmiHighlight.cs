﻿using System;
using System.Runtime.Serialization;

// ReSharper disable once CheckNamespace
namespace CE.Models.Insights
{
    public class AmiHighlight
    {
        /// <summary>
        /// Identifier of the action from the content management system.
        /// </summary>
        [DataMember(Order = 0)]
        public string ActionKey { get; set; }

        /// <summary>
        /// Percentage of impact.
        /// </summary>
        [DataMember(Order = 1)]
        public decimal PercentageImpact { get; set; }

        /// <summary>
        /// Type of commodity. 
        /// </summary>
        [DataMember(Order = 2)]
        public string Commoditykey { get; set; }

        /// <summary>
        /// Service Id (Service Contract Id) of the highlight.
        /// </summary>
        [DataMember(Order = 3)]
        public string ServiceId { get; set; }


        /// <summary>
        /// Meter Id of the highlight.
        /// </summary>
        [DataMember(Order = 4)]
        public string MeterId { get; set; }

        /// <summary>
        /// Cost of impact.
        /// </summary>
        [DataMember(Order = 5)]
        public decimal CostImpact { get; set; }


        /// <summary>
        /// Cost of impact.
        /// </summary>
        [DataMember(Order = 6)]
        public DateTime? TimeStamp { get; set; }
    }
}
