﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace CE.Models.Insights
{
    /// <summary>
    /// Data and analysis for a utility account.
    /// </summary>
    [DataContract]
    public class Account
    {
        public Account()
        {
            Premises = new List<Premise>();
        }

        /// <summary>
        /// The ID of the utility customer's account
        /// </summary>
        [DataMember(Order = 0)]
        public string Id { get; set; }
        /// <summary>
        /// The start date of the date range, typically the beginning of a billing period.
        /// Format is YYYY-MM-DD.
        /// </summary>
        [DataMember(Order = 1)]
        public DateTime? BillStartDate { get; set; }

        /// <summary>
        /// The end date of the date range, typically the end of a bill period. 
        /// Format is YYYY-MM-DD
        /// </summary>
        [DataMember(Order = 2)]
        public DateTime? BillEndDate { get; set; }

        /// <summary>
        /// A list of premises included in the account, and premise-specific analysis.
        /// </summary>
        [DataMember(Order = 3)]
        public List<Premise> Premises { get; set; }
    }
}
