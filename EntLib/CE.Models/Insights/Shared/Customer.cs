﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace CE.Models.Insights
{
    /// <summary>
    /// Data and analysis for a utility customer.
    /// </summary>
    [DataContract]
    public class Customer
    {
        /// <summary>
        /// The utility customer ID and the customer's accounts.
        /// </summary>
        public Customer()
        {
            Accounts = new List<Account>();
        }

        /// <summary>
        /// The utility customer's ID
        /// </summary>
        [DataMember(Order = 0)]
        public string Id { get; set; }

        /// <summary>
        /// A list of accounts that belong to the utility customer
        /// </summary>
        [DataMember(Order = 1)]
        public List<Account> Accounts;
    }
}
