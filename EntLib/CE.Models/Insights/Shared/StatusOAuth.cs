﻿using System.Runtime.Serialization;

namespace CE.Models.Insights
{
    /// <summary>
    /// Base class for serializable main objects.
    /// </summary>
    [DataContract]
    public class StatusOAuth
    {
        /// <summary>
        ///Required. Must be one of a set of predefined error codes. See the specification for the codes and their meaning.
        /// </summary>
        [DataMember(Order = 0)]
        public string Error { get; set; }
        /// <summary>
        ///Optional. A human-readable UTF-8 encoded text describing the error. Intended for a developer, not an end user.
        /// </summary>
        [DataMember(Order = 1)]
        public string Error_Description { get; set; }

        /// <summary>
        ///Optional. A URI pointing to a human-readable web page with information about the error.
        /// </summary>
        [DataMember(Order = 2)]
        public string Error_Uri { get; set; }

        /// <summary>
        /// Required, if present in authorization request. The same value as sent in the state parameter in the request.
        /// </summary>
        [DataMember(Order = 3)]
        public string State { get; set; }

    }
}
