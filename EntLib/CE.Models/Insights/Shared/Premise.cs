﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace CE.Models.Insights
{
    /// <summary>
    /// Data and analysis for a customer account's premise. 
    /// </summary>
    [DataContract]
    public class Premise
    {
        public Premise()
        {
            EndUses = new List<EndUse>();
        }

        /// <summary>
        /// Id of the premise
        /// </summary>
        [DataMember(Order = 0)]
        public string Id { get; set; }
        

        [DataMember(Order = 1)]
        public List<DisaggStatus> DisaggStatuses { get; set; }


        /// <summary>
        /// The BillDisagg endpoint populates this with usage and costs for specific end uses at the premise.  Not used in Benchmark response.
        /// </summary>
        [DataMember(Order = 2)]
        public List<EndUse> EndUses;

        /// <summary>
        /// The Benchmark endpoint populates this with benchmark comparison data for the premise.  Not used in BillDisagg response.
        /// </summary>
        [DataMember(Order = 3)]
        public List<Benchmark> Benchmarks;
    }

    [DataContract]
    public class DisaggStatus
    {

        [DataMember(Order = 0)]
        public string CommodityKey { get; set; }
        [DataMember(Order = 1)]
        public string Status { get; set; }
    }
}
