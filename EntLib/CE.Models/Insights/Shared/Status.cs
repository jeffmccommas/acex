﻿using System.Runtime.Serialization;
using CE.Models.Insights.Types;

namespace CE.Models.Insights
{
    /// <summary>
    /// Base class for serializable main objects.
    /// </summary>
    [DataContract]
    public class Status
    {
        /// <summary>
        /// Message.
        /// </summary>
        [DataMember(Order = 0)]
        public string Message { get; set; }
        public StatusType StatusType { get; set; }

    }
}
