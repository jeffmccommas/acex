﻿using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

// ReSharper disable once CheckNamespace
namespace CE.Models.Insights
{
    /// <summary>
    /// Details about the bill, including costs, measurements, units and timestamps.
    /// </summary>
    public class BillDetail
    {
        /// <summary>
        /// Name of the detail.
        /// </summary>
        [DataMember(Order = 0)]
        [Required]
        public string Name { get; set; }

        /// <summary>
        /// Value of the detail.
        /// </summary>
        [DataMember(Order = 0)]
        [Required]
        public string Value { get; set; }
    }
}
