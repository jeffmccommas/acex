﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace CE.Models.Insights
{
    /// <summary> Billing data for customer, account and premise.</summary>
    ///
    [DataContract]
    public class BillPremise
    {
        /// <summary>
        /// Id of the utility premise.
        /// </summary>
        [DataMember(Order = 0)]
        [Required]
        public string Id { get; set; }


        /// <summary>
        /// Premise address line one.
        /// </summary>
        [DataMember(Order = 1)]
        [StringLength(100)]
        [Required]
        public string Addr1 { get; set; }

        /// <summary>
        /// Premise address line two.
        /// </summary>
        [DataMember(Order = 2)]
        [StringLength(100)]
        public string Addr2 { get; set; }

        /// <summary>
        /// Premise city.
        /// </summary>
        [DataMember(Order = 3)]
        [StringLength(30)]
        [Required]
        public string City { get; set; }

        /// <summary>
        /// Premise state.
        /// </summary>
        [DataMember(Order = 4)]
        [StringLength(2)]
        [Required]
        public string State { get; set; }

        /// <summary>
        /// Premise zip code.
        /// </summary>
        [DataMember(Order = 5)]
        [StringLength(10)]
        [Required]
        public string Zip { get; set; }


        /// <summary>
        /// Service-level billing information. A premise can have one or more services.
        /// </summary>
        [DataMember(Order = 6)]
        public List<BillService> Service { get; set; }
    }
}
