﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

// ReSharper disable once CheckNamespace
namespace CE.Models.Insights
{
    /// <summary>
    /// Summary of a utility bill.
    /// </summary>
    [DataContract]
    public class Bill
    {
        /// <summary>
        /// The total amount billed.
        /// </summary>
        [DataMember(Order = 0)]
        [Required]
        public decimal TotalAmount { get; set; }

        /// <summary>
        /// Any billed amount included in the total amount that was not billed under a service. This may include taxes or bill charges.
        /// </summary>
        [DataMember(Order = 1)]
        [Required]
        public decimal AdditionalBillCost { get; set; }

        /// <summary>
        /// The date that this bill was created.
        /// </summary>
        [DataMember(Order = 2)]
        [Required]
        public string BillDate { get; set; }

        /// <summary>
        /// The date that payment is due.
        /// </summary>
        [DataMember(Order = 3)]
        public string BillDueDate { get; set; }

        /// <summary>
        /// The bill frequency, such as monthly.
        /// </summary>
        [DataMember(Order = 4)]
        [StringLength(50)]
        public string BillFrequency { get; set; }

        /// <summary>
        /// List of cost details for the bill.
        /// </summary>
        [DataMember(Order = 5)]
        public List<BillDetail> CostDetails { get; set; }

        /// <summary>
        /// Billing details for the premises that belong to the billed account.
        /// </summary>
        [DataMember(Order = 6)]
        public List<BillPremise> Premises { get; set; }
    }
}
