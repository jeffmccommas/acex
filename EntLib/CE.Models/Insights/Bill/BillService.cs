﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

// ReSharper disable once CheckNamespace
namespace CE.Models.Insights
{
    /// <summary>
    /// Billing data for a specific service belonging to a Customer, Account and Premise.
    /// </summary>
    [DataContract]
    public class BillService
    {
        /// <summary>
        /// Id of the utility service.
        /// </summary>
        [DataMember(Order = 0)]
        [Required]
        public string Id { get; set; }

        /// <summary>
        /// Total commodity usage that was billed for the service.
        /// </summary>
        [DataMember(Order = 1)]
        [Required]
        public decimal TotalServiceUse { get; set; }

        /// <summary>
        /// The amount billed for the commodity usage.
        /// </summary>
        [DataMember(Order = 2)]
        [Required]
        public decimal CostofUsage { get; set; }

        /// <summary>
        /// Any other costs for this service that are not directly related to usage, such as a service charge.
        /// </summary>
        [DataMember(Order = 3)]
        public decimal AdditionalServiceCost { get; set; }

        /// <summary>
        /// The key value for the commodity provided by this service.
        /// </summary>
        [DataMember(Order = 4)]
        [StringLength(10)]
        [Required]
        public string CommodityKey { get; set; }

        /// <summary>
        /// The key value for the unit of measure.
        /// </summary>
        [DataMember(Order = 5)]
        [StringLength(10)]
        // ReSharper disable once InconsistentNaming
        public string UOMKey { get; set; }

        /// <summary>
        /// The start date of the period of time for which this service was billed.
        /// </summary>
        [DataMember(Order = 6)]
        public string BillStartDate { get; set; }

        /// <summary>
        /// The end date of the period of time for which this service was billed.
        /// </summary>
        [DataMember(Order = 7)]
        [Required]
        public string BillEndDate { get; set; }

        /// <summary>
        /// The number of days in the billing period for this service.
        /// </summary>
        [DataMember(Order = 8)]
        [Range(0, 31)]
        [Required]
        public int BillDays { get; set; }

        /// <summary>
        /// The average temperature for the period of time defined by BillStartDate and BillEndDate.
        /// </summary>
        [DataMember(Order = 9)]
        public int AverageTemperature { get; set; }

        /// <summary>
        /// Rate class for the service
        /// </summary>
        [DataMember(Order = 10)]
        public string RateClass { get; set; }

        /// <summary>
        /// List of cost details for the service.
        /// </summary>
        [DataMember(Order = 11)]
        public List<BillDetail> CostDetails { get; set; }
    }
}
