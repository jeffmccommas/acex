﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;


namespace CE.Models.Insights
{
    /// <summary>
    /// Action statuses for the premise.
    /// </summary>
    [DataContract]
    public class ActionPlanPremise
    {
        /// <summary>
        /// Id of the utility premise.
        /// </summary>
        [Required]
        [DataMember(Order = 1)]
        [MaxLength(50)]
        public string Id { get; set; }

        /// <summary>
        /// Action statuses for the premise.
        /// </summary>
        [Required]
        [DataMember(Order = 2)]
        public List<ActionStatus> ActionStatuses { get; set; }
    }
}