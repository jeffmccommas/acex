﻿using System.Runtime.Serialization;

namespace CE.Models.Insights
{
    /// <summary>
    /// Additional information about an action status, captured in key:value pairs.
    /// </summary>
    [DataContract]
    public class ActionAdditionalInfo
    {
        /// <summary>
        /// The name of the field.      
        /// </summary>
        [DataMember(Order = 0)]
        public string Key { get; set; }

        /// <summary>
        /// The value of the field.
        /// </summary>
        [DataMember(Order = 1)]
        public string Value { get; set; }
        
    }
}
