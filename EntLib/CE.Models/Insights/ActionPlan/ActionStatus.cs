﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace CE.Models.Insights
{
    /// <summary>
    /// Status of a specific action for a customer, account and premise. 
    /// Note that only one ActionStatus can be POSTed for the same ActionKey and SubActionKey combination at a time. 
    /// For example, you must submit two separate POST requests to store that an action was selected and later completed.
    /// </summary>
    [DataContract]
    public class ActionStatus
    {
        /// <summary>
        /// Identifier of the action in the content management system. Should be all lower-case.
        /// If the POST validation is set to True then it validates that the action key is defined in the CMS, 
        /// and this validation is case-sensitive.
        /// </summary>
        [Required]
        [DataMember(Order = 1)]
        [MaxLength(64)]
        public string ActionKey { get; set; }

        /// <summary>
        /// Use this identifier when you want to capture different instances of the same action. 
        /// For example, it could contain the project number for multiple window replacement projects
        /// or the year of an annual furnace maintenance. 
        /// </summary>       
        [DataMember(Order = 2)]
        [MaxLength(64)]
        public string SubActionKey { get; set; }

        /// <summary>
        /// Status of the action for the customer, account and premise. Should be all lower-case.
        /// If the POST validation is set to True then it validates that the status is defined in the CMS, 
        /// and this validation is case-sensitive.
        /// </summary>
        [Required]
        [DataMember(Order = 3)]
        public string Status { get; set; }

        /// <summary>
        ///Date that the status changed. This will default to the current date.
        ///The date will be in GMT time formatted as YYYY-MM-DDTHH:II:SS.Ms
        /// </summary>
        [DataMember(Order = 4)]
        public DateTime StatusDate { get; set; }

        /// <summary>
        ///Key value for the source of the profile data. Should be all lower-case.
        ///If the POST validation is set to True then it validates this value against the source enumerated values in the CMS,
        ///and this validation is case-sensitive.
        /// </summary>
        [Required]
        [DataMember(Order = 5)]
        public string SourceKey { get; set; }

        /// <summary>
        ///Additional information about the action status. This is a configurable key:value data structure. 
        /// </summary>
        [DataMember(Order = 6)]
        public List<ActionAdditionalInfo> AdditionalInfo { get; set; }
    }
}
