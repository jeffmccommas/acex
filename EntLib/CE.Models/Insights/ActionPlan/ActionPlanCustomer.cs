﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace CE.Models.Insights
{
    /// <summary>
    /// Action statuses for the customer.
    /// </summary>
    [DataContract]
    public class ActionPlanCustomer
    {
        /// <summary>
        /// Id of the utility customer.
        /// </summary>
        [Required]
        [DataMember(Order = 1)]
        [MaxLength(50)]
        public string Id { get; set; }


        /// <summary>
        /// A list of accounts that belong to the utility customer.
        /// </summary>
        [Required]
        [DataMember(Order = 2)]
        public List<ActionPlanAccount> Accounts { get; set; }
    }
}
