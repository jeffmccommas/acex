﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace CE.Models.Insights
{
    /// <summary>
    /// Action statuses for the account.
    /// </summary>
    [DataContract]
    public class ActionPlanAccount
    {
        /// <summary>
        /// Id of the utility account.
        /// </summary>
        [Required]
        [DataMember(Order = 1)]
        [MaxLength(50)]
        public string Id { get; set; }

        /// <summary>
        /// A list of premises that belong to the account.
        /// </summary>
        [Required]
        [DataMember(Order = 2)]
        public List<ActionPlanPremise> Premises { get; set; }
    }
}
