﻿// ReSharper disable once CheckNamespace

using System.Collections.Generic;
using CE.GreenButtonConnect;
using CE.Models.Insights;
using CE.Models.Insights.GreenButtonConnect;
using CE.Models.Insights.Types;

// ReSharper disable once CheckNamespace
namespace CE.Models
{
    public interface IGreenButtonModel
    {
        GreenButtonResult GenerateGreenButton(GreenButtonSubscription subscription, GreenButtonEntryType entryType);
        Subscription GreenButtonSubscriptionMapping(GreenButtonSubscription subscription);
        GreenButtonResult GenerateServiceStatus(bool isValidServiceStatus);
        GreenButtonResult GenerateApplicationInfo(GreenButtonApplicationInfo applicationInfo);
        GreenButtonResult GenerateAuthorization(List<GreenButtonAuthorization> authorizations);
    }
}
