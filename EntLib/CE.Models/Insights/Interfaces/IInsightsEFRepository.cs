﻿using CE.Models.Insights;
using System;
using System.Collections.Generic;

// ReSharper disable once CheckNamespace
namespace CE.Models
{
    /// <summary>
    /// IInsightsEFRepository
    /// </summary>
    // ReSharper disable once InconsistentNaming
    public interface IInsightsEFRepository : IDisposable
    {
        /// <summary>
        /// GetEndpointTracking
        /// </summary>
        /// <param name="clientId"></param>
        /// <param name="request"></param>
        /// <returns></returns>
        EndpointTrackingResponse GetEndpointTracking(int clientId, EndpointTrackingRequest request);

        /// <summary>
        /// GetBenchmark
        /// </summary>
        /// <param name="clientId"></param>
        /// <param name="request"></param>
        /// <returns></returns>
        BenchmarkResponse GetBenchmark(int clientId, BenchmarkRequest request);

        /// <summary>
        /// GetProfile
        /// </summary>
        /// <param name="clientId"></param>
        /// <param name="request"></param>
        /// <returns></returns>
        ProfileResponse GetProfile(int clientId, ProfileRequest request);

        /// <summary>
        /// PostProfile
        /// </summary>
        /// <param name="clientId"></param>
        /// <param name="request"></param>
        /// <returns></returns>
        ProfilePostResponse PostProfile(int clientId, ProfilePostRequest request);

        /// <summary>
        /// GetActions
        /// </summary>
        /// <param name="clientId"></param>
        /// <param name="locale"></param>
        /// <param name="request"></param>
        /// <returns></returns>
        ActionResponse GetActions(int clientId, string locale, ActionRequest request);

        /// <summary>
        /// GetActions
        /// </summary>
        /// <param name="clientId"></param>
        /// <param name="request"></param>
        /// <returns></returns>
        ActionPlanResponse GetActionPlan(int clientId, ActionPlanRequest request);

        /// <summary>
        /// PostActionPlan
        /// </summary>
        /// <param name="clientId"></param>
        /// <param name="request"></param>
        /// <returns></returns>
        ActionPlanPostResponse PostActionPlan(int clientId, ActionPlanPostRequest request);

        /// <summary>
        /// GetBills
        /// </summary>
        /// <param name="clientId"></param>
        /// <param name="request"></param>
        /// <returns></returns>
        BillResponse GetBills(int clientId, BillRequest request);
        
        /// <summary>
        /// GetConsumption
        /// </summary>
        /// <param name="clientId"></param>
        /// <param name="request"></param>
        /// <returns></returns>
        ConsumptionResponse GetConsumption(int clientId, ConsumptionRequest request);

        /// <summary>
        /// KeepAlive
        /// </summary>
        /// <param name="clientId"></param>        
        /// <returns></returns>
        void KeepAlive(int clientId);       

        /// <summary>
        /// GetWeather
        /// </summary>
        /// <param name="clientId"></param>
        /// <param name="request"></param>
        /// <returns></returns>
        List<WeatherDb> GetWeather(int clientId, WeatherRequest request);

        /// <summary>
        /// GetCustomer
        /// </summary>
        /// <param name="clientId"></param>
        /// <param name="request"></param>
        /// <returns></returns>
        CustomerInfoResponse GetCustomerInfo(int clientId, CustomerInfoRequest request);

        /// <summary>
        /// GetWeatherInfo for bill 
        /// </summary>
        /// <param name="accounts"></param>
        /// <returns></returns>
        List<ServiceDb> GetWeatherInfoForBill(List<BillAccount> accounts);

        /// <summary>
        /// GetGroupId
        /// </summary>
        /// <param name="groupName"></param>
        /// <returns></returns>
        int? GetGroupId(string groupName);

        /// <summary>
        /// GetRoleId
        /// </summary>
        /// <param name="roleName"></param>
        /// <returns></returns>
        int? GetRoleId(string roleName);


        /// <summary>
        /// Get file metadata
        /// </summary>
        /// <param name="clientId"></param>
        /// <param name="fileId"></param>
        /// <returns></returns>
        FileDetail GetFile(int clientId, int fileId);

        /// <summary>
        /// Post File Metadata details
        /// </summary>
        /// <param name="clientId"></param>
        /// <param name="request"></param>
        /// <returns></returns>
        FilePostResponse PostFile(int clientId, FilePostRequest request);


        /// <summary>
        /// Delete file Metadata details
        /// </summary>
        /// <param name="fileId"></param>
        void DeleteFile(int fileId);

        /// <summary>
        /// Get files for Client,Customer,  Account and Premise
        /// </summary>
        /// <param name="clientId"></param>
        /// <param name="request"></param>
        /// <returns></returns>
        List<FileDetail> GetFiles(int clientId, FilesRequest request);


        /// <summary>
        /// OAuth2 -- Validate Client Authentication
        /// </summary>
        /// <param name="context"></param>
        /// <param name="authorizationHeader"></param>
        /// <param name="baseUrl"></param>
        /// <returns></returns>
        TokenPostResponse ValidateClientAuthentication(TokenPostRequest context, string authorizationHeader, string baseUrl);

        /// <summary>
        /// Get subscription info for the subscription id
        /// </summary>
        /// <param name="subscriptionId"></param>
        /// <returns></returns>
        GreenButtonSubscription GetGreenButtonAmiSubscription(long subscriptionId);

        /// <summary>
        /// Get all usage points by the subscription id
        /// </summary>
        /// <param name="subscriptionId"></param>
        /// <param name="scopes"></param>
        /// <returns></returns>
        List<GreenButtonUsagePoint> GetGreenButtonAmiUsagePoints(long subscriptionId, GreenButtonScope scopes);
        
        /// <summary>
        /// Get usage point by the subscription id and encrypted meter id
        /// </summary>
        /// <param name="subscriptionId"></param>
        /// <param name="meterId"></param>
        /// <returns></returns>
        GreenButtonUsagePoint GetGreenButtonAmiUsagePoint(long subscriptionId,string meterId);

        /// <summary>
        /// Update local time configuration information for usage point
        /// </summary>
        /// <param name="usagePointId"></param>
        /// <param name="timeZoneInfo"></param>
        /// <returns></returns>
        int UpdateLocalTimeConfiguration(long usagePointId, TimeZoneInfo timeZoneInfo);

        /// <summary>
        /// Get all related links for the specified usage point id
        /// </summary>
        /// <param name="usagePoint"></param>
        /// <returns></returns>
        List<GreenButtonLink> GetGreenButtonAmiUsagePointRelatedLinks(GreenButtonUsagePoint usagePoint);
        
        /// <summary>
        /// Get all reading types for meter id
        /// </summary>
        /// <param name="meterId"></param>
        /// <returns></returns>
        List<GreenButtonAmiReadingType> GetGreenButtonAmiReadingTypes(string meterId);

        /// <summary>
        /// Insert reading Types into database
        /// </summary>
        /// <param name="clientId"></param>
        /// <param name="meterId"></param>
        /// <param name="readingTypes"></param>
        void InsertGreenButtonAmiReadingTypes(int clientId, string meterId, List<GreenButtonAmiReadingType> readingTypes);
        
        /// <summary>
        /// get local time parameter info based on time configarution id
        /// </summary>
        /// <param name="configurationId"></param>
        /// <returns></returns>
        GreenButtonTimeParameter GetGreenButtonTimeParam(long configurationId);

        /// <summary>
        /// Get Green Button Authorization based on client id and secret
        /// </summary>
        /// <param name="clientId"></param>
        /// <param name="secret"></param>
        /// <returns></returns>
        List<GreenButtonAuthorization> GetGreenButtonAuthorizations(string clientId, string secret);

        /// <summary>
        /// Get Green Button Ami Reading Type
        /// </summary>
        /// <param name="subscriptionid"></param>
        /// <param name="meterId"></param>
        /// <param name="encryptedReadingTypeId"></param>
        /// <returns></returns>
        GreenButtonAmiReadingType GetGreenButtonAmiReadingType(long subscriptionid, string meterId, string encryptedReadingTypeId);
        /// <summary>
        /// Get Green button Application Info by application Info id
        /// </summary>
        /// <param name="applicationInfoId"></param>
        /// <returns></returns>
        GreenButtonApplicationInfo GetGreenButtonApplicationInformation(long applicationInfoId);
        /// <summary>
        /// Get Meter List for a Customer from InsightsDW
        /// </summary>
        /// <param name="clientId"></param>
        /// <param name="customerId"></param>
        /// <param name="accountId"></param>
        /// <param name="premiseId"></param>
        /// <param name="scopes"></param>
        /// <returns></returns>
        List<string> GetMeterList(int clientId, string customerId, string accountId, string premiseId, GreenButtonScope scopes);


    }
}
