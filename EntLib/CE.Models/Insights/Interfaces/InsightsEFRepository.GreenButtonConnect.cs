﻿using System.Linq;
using AutoMapper;
using CE.Models.Insights;
using CE.Models.Insights.EF;
using System;
using System.Collections.Generic;
using System.Text;
using CE.Models.Insights.Types;
using CE.Models.Utils;
using CE.GreenButtonConnect;
using System.Data.Entity;

// ReSharper disable once CheckNamespace
namespace CE.Models
{
    public partial class InsightsEfRepository
    {
        private const string Est = "Eastern Standard Time";
        private const string Cst = "Central Standard Time";
        private const string Mst = "Mountain Standard Time";
        private const string Pst = "Pacific Standard Time";
        private const string Akst = "Alaskan Standard Time";
        private const string Hst = "Hawaiian Standard Time";

        private const string SelfLink = "self";

        public List<GreenButtonUsagePoint> GetGreenButtonAmiUsagePoints(long subscriptionId, GreenButtonScope scopes)
        {
            var usagePoints = new List<GreenButtonUsagePoint>();
            var usagePointEntities = GetUsagePoints(subscriptionId);

            //**** meter exchange, check for new meters ****
            // lookup customer info by subscriptionId
            var customer = (from rc in db.retail_customers
                            join sub in db.subscription on rc.id equals sub.retail_customer_id
                            where sub.id == subscriptionId
                            select rc).FirstOrDefault();

            if (customer != null)
            {
                var meterList = GetMeterList(customer.data_custodian_client_id, customer.data_custodian_customer_id,
                    customer.data_custodian_account_id,
                    customer.data_custodian_premise_id, scopes);

                //new meters, not yet in gbc usage point table
                var meters = meterList?.Where(m => !usagePointEntities.Any(i => i.meterid == m)).ToList();

                //create entry in usage point table
                if (meters?.Count > 0)
                {

                    foreach (var m in meters)
                    {
                        var genUsage = new IdGenerator(Convert.ToInt32(customer.data_custodian_client_id), customer.data_custodian_account_id, m);
                        var up = new usage_points
                        {
                            uuid = genUsage.UsagePointId.Split(':')[2],
                            self_link_href = "/Datacustodian/espi/1_1/resource/Subscription/" + subscriptionId + "/UsagePoint/" + Helper.ConvertStringToHex(m, Encoding.Unicode),
                            self_link_rel = "self",
                            up_link_href = "/Datacustodian/espi/1_1/resource/Subscription/" + subscriptionId + "/UsagePoint",
                            up_link_rel = "up",
                            meterid = m,
                            published = DateTime.UtcNow,
                            updated = DateTime.UtcNow
                        };

                        db.Entry(up).State = EntityState.Added;
                        db.usage_points.Add(up);
                        db.SaveChanges();

                        var sup = new subscription_usage_points
                        {
                            subscription_id = subscriptionId,
                            usage_point_id = up.id
                        };

                        db.Entry(sup).State = EntityState.Added;
                        db.subscription_usage_points.Add(sup);

                        //add new usage point to the list so it is returned to the caller
                        usagePointEntities.Add(up);
                    }
                    db.SaveChanges();

                }
            }

            foreach (var usagePointEntity in usagePointEntities)
            {
                var links = new List<GreenButtonLink>();
                links.Add(new GreenButtonLink
                {
                    Href = usagePointEntity.self_link_href,
                    Rel = usagePointEntity.self_link_rel
                });
                links.Add(new GreenButtonLink
                {
                    Href = usagePointEntity.up_link_href,
                    Rel = usagePointEntity.up_link_rel
                });

                var usagePoint = Mapper.Map<usage_points, GreenButtonUsagePoint>(usagePointEntity);

                usagePoint.Links = links;
                usagePoints.Add(usagePoint);
            }

            return usagePoints;
        }

        public GreenButtonUsagePoint GetGreenButtonAmiUsagePoint(long subscriptionId, string meterId)
        {
            var usagePointEntity = GetUsagePoint(subscriptionId, meterId);

            var usagePoint = Mapper.Map<usage_points, GreenButtonUsagePoint>(usagePointEntity);

            if (usagePointEntity != null)
            {
                var links = new List<GreenButtonLink>();

                links.Add(new GreenButtonLink
                {
                    Href = usagePointEntity.self_link_href,
                    Rel = usagePointEntity.self_link_rel
                });

                links.Add(new GreenButtonLink
                {
                    Href = usagePointEntity.up_link_href,
                    Rel = usagePointEntity.up_link_rel
                });

                usagePoint.Links = links;
            }
            return usagePoint;
        }

        public int UpdateLocalTimeConfiguration(long usagePointId, TimeZoneInfo timeZoneInfo)
        {
            var localTimeId = UpdateLocalTimeConfig(usagePointId, timeZoneInfo);
            return localTimeId;
        }

        public List<GreenButtonLink> GetGreenButtonAmiUsagePointRelatedLinks(GreenButtonUsagePoint usagePoint)
        {
            var usagePointRelatedLinksEntities = GetUsagePointRelatedLinks(usagePoint);

            List<GreenButtonLink> relatedLinks;

            if (usagePointRelatedLinksEntities.Count > 0)
            {
                relatedLinks =
                    Mapper.Map<List<usage_point_related_links>, List<GreenButtonLink>>(usagePointRelatedLinksEntities);
            }
            else
            {
                var timeId = usagePoint.TimeConfigurationId ?? 1;
                relatedLinks = GenerateGreenButtonAmiUsagePointRelatedLinks(usagePoint.Links.Find(l => l.Rel == SelfLink).Href, timeId);

                // insert into database
                InsertGreenButtonAmiUsagePointRelatedLinks(relatedLinks, usagePoint.Id);
            }

            return relatedLinks;

        }

        public GreenButtonSubscription GetGreenButtonAmiSubscription(long subscriptionId)
        {
            var subscriptionEntity = GetSubscription(subscriptionId);
            var subscription = Mapper.Map<subscription, GreenButtonSubscription>(subscriptionEntity);

            if (subscriptionEntity != null)
            {
                var accountId = GetAccoutId(subscriptionEntity.retail_customer_id);

                subscription.AccountId = accountId;

                var links = new List<GreenButtonLink>();

                links.Add(new GreenButtonLink
                {
                    Href = subscriptionEntity.self_link_href,
                    Rel = subscriptionEntity.self_link_rel
                });

                subscription.Links = links;
            }
            return subscription;
        }

        public bool IsSubscriptionValid(long subscriptionId)
        {
            var subscription = GetSubscription(subscriptionId);

            if (subscription != null)
                return true;
            return false;
        }

        public bool IsSubscriptionValid(long subscriptionId, string meterId)
        {
            var subscription = GetGreenButtonAmiUsagePoint(subscriptionId, meterId);

            if (subscription != null)
                return true;
            return false;
        }

        public List<GreenButtonAmiReadingType> GetGreenButtonAmiReadingTypes(string meterId)
        {
            var readingTypes = new List<GreenButtonAmiReadingType>();

            List<reading_types> readingTypesEntities = GetReadingTypes(meterId);

            if (readingTypesEntities.Count > 0)
            {
                foreach (var readingTypeEntity in readingTypesEntities)
                {
                    var links = new List<GreenButtonLink>();
                    links.Add(new GreenButtonLink
                    {
                        Href = readingTypeEntity.self_link_href,
                        Rel = readingTypeEntity.self_link_rel
                    });
                    links.Add(new GreenButtonLink
                    {
                        Href = readingTypeEntity.up_link_href,
                        Rel = readingTypeEntity.up_link_ref
                    });

                    var readingType = Mapper.Map<reading_types, GreenButtonAmiReadingType>(readingTypeEntity);
                    readingType.Links = links;

                    readingTypes.Add(readingType);
                }
            }
            return readingTypes;
        }


        public GreenButtonAmiReadingType GetGreenButtonAmiReadingType(long subscriptionid, string meterId, string encryptedReadingTypeId)
        {
            var readingTypeId = Helper.ConvertHexToString(encryptedReadingTypeId, Encoding.Unicode);

            var readingTypeEntity = GetReadingType(subscriptionid, meterId, Convert.ToInt64(readingTypeId));

            if (readingTypeEntity == null) return null;
            var links = new List<GreenButtonLink>
            {
                new GreenButtonLink
                {
                    Href = readingTypeEntity.self_link_href,
                    Rel = readingTypeEntity.self_link_rel
                },
                new GreenButtonLink
                {
                    Href = readingTypeEntity.up_link_href,
                    Rel = readingTypeEntity.up_link_ref
                }
            };

            var readingType = Mapper.Map<reading_types, GreenButtonAmiReadingType>(readingTypeEntity);
            readingType.Links = links;
            return readingType;
        }

        public GreenButtonTimeParameter GetGreenButtonTimeParam(long configurationId)
        {
            var timeConfig = GetLocatlTimeConfiguration(configurationId);

            var timeParam = Mapper.Map<time_configurations, GreenButtonTimeParameter>(timeConfig);

            if (timeConfig != null)
            {
                var links = new List<GreenButtonLink>();

                links.Add(new GreenButtonLink
                {
                    Href = timeConfig.self_link_href,
                    Rel = timeConfig.self_link_rel
                });

                links.Add(new GreenButtonLink
                {
                    Href = timeConfig.up_link_href,
                    Rel = timeConfig.up_link_rel
                });

                timeParam.Links = links;
            }

            return timeParam;
        }

        public void InsertGreenButtonAmiReadingTypes(int clientId, string meterId, List<GreenButtonAmiReadingType> readingTypes)
        {
            foreach (var readingType in readingTypes)
            {
                IdGenerator idGenerator = new IdGenerator(clientId, meterId);
                var guidId = idGenerator.ReadingTypeId(readingType.IntervalLength.ToString());
                readingType.Uuid = guidId;

                var readingTypeEntity = Mapper.Map<GreenButtonAmiReadingType, reading_types>(readingType);

                var self = readingType.Links.Find(l => l.Rel == GreenButtonLinkRelationshipType.self.ToString());
                var up = readingType.Links.Find(l => l.Rel == GreenButtonLinkRelationshipType.up.ToString());

                readingTypeEntity.self_link_href = self.Href;
                readingTypeEntity.self_link_rel = self.Rel;
                readingTypeEntity.up_link_href = up.Href;
                readingTypeEntity.up_link_ref = up.Rel;

                var id = InsertReadingType(readingTypeEntity);
                readingType.Id = id;

                var hexId = Helper.ConvertStringToHex(readingType.Id.ToString(), Encoding.Unicode);
                self.Href = $"{self.Href}/{hexId}";
            }
        }

        public List<GreenButtonAuthorization> GetGreenButtonAuthorizations(string clientId, string secret)
        {
            return GetAuthorizations(clientId, secret);
        }

        public GreenButtonApplicationInfo GetGreenButtonApplicationInformation(long applicationInfoId)
        {
            return GetApplicationInfo(applicationInfoId);

        }


        private List<GreenButtonAuthorization> GetAuthorizations(string clientId, string secret)
        {
            List<GreenButtonAuthorization> authorizations;
            //declare the transaction options
            //set it to read uncommited
            var transactionOptions = new System.Transactions.TransactionOptions
            {
                IsolationLevel = System.Transactions.IsolationLevel.ReadUncommitted
            };

            using (var transScopeAction = new System.Transactions.TransactionScope(System.Transactions.TransactionScopeOption.Required, transactionOptions))
            {
                authorizations =
                    db.authorizations.Join(db.application_information, a => a.application_information_id,
                        ai => ai.application_information_id,
                        (a, ai) => new { a, ai })
                        .Where(j => j.ai.client_id == clientId && j.ai.client_secret == secret)
                        .ToList()
                        .Select(j => new GreenButtonAuthorization
                        {
                            ApplicationInformationId = j.ai.application_information_id,
                            AuthorizationId = j.a.id,
                            UuId = j.a.uuid,
                            ClientId = Helper.GetAclaraClientIdFromDataCustodianId(j.ai.dataCustodianId),
                            SubscriptionId = j.a.subscription_id ?? 0,
                            Scope = GetScopeSettings(j.a.scope),
                            ScopeString = j.a.scope,
                            AuthorizationScope = j.a.scope,
                            AccessTokenType = GetAccessTokenType(j.a.third_party),
                            AccessToken = j.a.access_token,
                            EndDate = j.a.end_date,
                            Active = j.a.status == 1,
                            Status = j.a.status == 1 ? 1 : 0,
                            PusblishedTimePeriod = new TimePeriod
                            {
                                Duration = j.a.pp_duration,
                                Start = j.a.pp_start
                            },
                            AuthorizationTimePeriod = new TimePeriod
                            {
                                Duration = j.a.ap_duration,
                                Start = j.a.ap_start
                            },
                            ExpiresAt = j.a.expiresin,
                            GrantType = GetAuthorizationGrantType(j.a.refresh_token, j.a.third_party),
                            Error = j.a.error,
                            ErrorDescription = j.a.error_description,
                            ErrorUri = j.a.error_uri,
                            ResourceUri = j.a.resourceURI,
                            AuthorizationUri = j.a.authorization_uri,
                            Links = new List<GreenButtonLink>
                                {
                                    new GreenButtonLink
                                    {
                                        Href = j.a.self_link_href,
                                        Rel = j.a.self_link_rel
                                    },
                                    new GreenButtonLink
                                    {
                                        Href = j.a.up_link_href,
                                        Rel = j.a.up_link_rel
                                    }
                                },
                            UpdatedDate = j.a.updated,
                            PublishedDate = j.a.published
                        }).ToList();

                transScopeAction.Complete();
            }

            return authorizations;
        }

        private GreenButtonApplicationInfo GetApplicationInfo(long applicationInfoId)
        {
            GreenButtonApplicationInfo applicationInfo = null;
            //declare the transaction options
            //set it to read uncommited
            var transactionOptions = new System.Transactions.TransactionOptions
            {
                IsolationLevel = System.Transactions.IsolationLevel.ReadUncommitted
            };

            using (var transScopeAction = new System.Transactions.TransactionScope(System.Transactions.TransactionScopeOption.Required, transactionOptions))
            {

                var applicationInfoEntity =
                    db.application_information.Join(db.application_information_scopes, a => a.application_information_id, ai => ai.application_information_id,
                        (a, ai) => new { a, ai }).FirstOrDefault(j => j.a.application_information_id == applicationInfoId);

                if (applicationInfoEntity != null)
                {
                    applicationInfo = Mapper.Map<application_information, GreenButtonApplicationInfo>(applicationInfoEntity.a);

                    var links = new List<GreenButtonLink>
                    {
                        new GreenButtonLink
                        {
                            Href = applicationInfoEntity.a.self_link_href,
                            Rel = applicationInfoEntity.a.self_link_rel
                        },
                        new GreenButtonLink
                        {
                            Href = applicationInfoEntity.a.up_link_href,
                            Rel = applicationInfoEntity.a.up_link_rel
                        }
                    };


                    applicationInfo.Links = links;
                    applicationInfo.Scope = applicationInfoEntity.ai.scope;
                }


                transScopeAction.Complete();
            }

            return applicationInfo;
        }

        private string GetAuthorizationGrantType(string refreshToken, string thirdParty)
        {
            var accessTokenType = GetAccessTokenType(thirdParty);
            if (accessTokenType == GreenButtonAccessTokenType.Client)
                return GrantTypeClientAccessToken;
            if (refreshToken == null)
                return GrantTypeAuthorizationCode;
            return GrantTypeRefreshToken;
        }
        private GreenButtonScope GetScopeSettings(string scope)
        {
            var greenButtonScope = new GreenButtonScope();
            if (!string.IsNullOrEmpty(scope))
            {
                var scopeBlocks = scope.Split(' ');

                foreach (var block in scopeBlocks)
                {
                    var scopeSettings = block.Split(';');
                    foreach (var setting in scopeSettings)
                    {
                        var keyValuePair = setting.Split('=');
                        if (keyValuePair.GetUpperBound(0) == 1)
                        {
                            // function block
                            if (keyValuePair[0] == "FB")
                            {
                                greenButtonScope.FunctionBlocks = ConvertScopeFunctionBlocks(keyValuePair[1]);
                            }

                            // interval Duration
                            if (keyValuePair[0] == "IntervalDuration")
                            {
                                greenButtonScope.IntervalDurations = ConvertScopeIntervalDuration(keyValuePair[1]);
                            }

                            // block duration
                            if (keyValuePair[0] == "BlockDuration")
                            {
                                GreenButtonDurationType blockDuration;
                                Enum.TryParse(keyValuePair[1], out blockDuration);
                                greenButtonScope.BlockDuration = blockDuration;
                            }

                            // Account collection
                            if (keyValuePair[0] == "AccountCollection")
                            {
                                greenButtonScope.AccountCollection = Convert.ToInt32(keyValuePair[1]);
                            }

                            // HistoryLength
                            if (keyValuePair[0] == "HistoryLength")
                            {
                                greenButtonScope.HistoryLength = (int)Math.Round(Convert.ToDecimal(keyValuePair[1]) / 86400);
                            }

                            // SubscriptionFrequency
                            if (keyValuePair[0] == "SubscriptionFrequency")
                            {
                                GreenButtonSubscriptionFrequencyType subscriptionFrequency;
                                Enum.TryParse(keyValuePair[1], out subscriptionFrequency);
                                greenButtonScope.SubscriptionFrequency = subscriptionFrequency;
                            }
                        }
                    }
                }
            }
            return greenButtonScope;
        }

        private GreenButtonAccessTokenType GetAccessTokenType(string thirdParty)
        {
            GreenButtonAccessTokenType accessToken;
            switch (thirdParty.ToLower())
            {
                case "registration_third_party":
                    accessToken = GreenButtonAccessTokenType.Registration;
                    break;
                case "third_party_admin":
                    accessToken = GreenButtonAccessTokenType.Client;
                    break;
                default:
                    accessToken = GreenButtonAccessTokenType.Customer;
                    break;
            }

            return accessToken;
        }

        private time_configurations GetLocatlTimeConfiguration(long timeConfigId)
        {
            time_configurations timeConfig;

            //declare the transaction options
            var transactionOptions = new System.Transactions.TransactionOptions();
            //set it to read uncommited
            transactionOptions.IsolationLevel = System.Transactions.IsolationLevel.ReadUncommitted;

            using (
                var transScopeAction =
                    new System.Transactions.TransactionScope(System.Transactions.TransactionScopeOption.Required,
                        transactionOptions))
            {
                timeConfig = db.time_configurations.FirstOrDefault(t => t.id == timeConfigId);
                transScopeAction.Complete();
            }


            return timeConfig;
        }

        private List<GreenButtonFunctionBlockType> ConvertScopeFunctionBlocks(string functionBlockVales)
        {
            var functionBlockArray = functionBlockVales.Split('_');

            return functionBlockArray.Select(functionBlock => (GreenButtonFunctionBlockType)Convert.ToInt32(functionBlock)).ToList();
        }

        private List<GreenButtonDurationType> ConvertScopeIntervalDuration(string intervalDurationValue)
        {
            var intervalDurationArray = intervalDurationValue.Split('_');

            return intervalDurationArray.Select(intervalDuration => (GreenButtonDurationType)Convert.ToInt32(intervalDuration)).ToList();
        }

        private List<reading_types> GetReadingTypes(string meterId)
        {
            List<reading_types> readingTypesEntities;

            //declare the transaction options
            var transactionOptions = new System.Transactions.TransactionOptions();
            //set it to read uncommited
            transactionOptions.IsolationLevel = System.Transactions.IsolationLevel.ReadUncommitted;

            using (var transScopeAction = new System.Transactions.TransactionScope(System.Transactions.TransactionScopeOption.Required, transactionOptions))
            {

                readingTypesEntities =
                    db.reading_types.Where(u => u.meterid == meterId).ToList();
                transScopeAction.Complete();
            }
            return readingTypesEntities;
        }

        private reading_types GetReadingType(long subscriptionId, string meterId, long readingTypeId)
        {
            reading_types readingTypeEntity;

            //declare the transaction options
            var transactionOptions = new System.Transactions.TransactionOptions();
            //set it to read uncommited
            transactionOptions.IsolationLevel = System.Transactions.IsolationLevel.ReadUncommitted;

            using (var transScopeAction = new System.Transactions.TransactionScope(System.Transactions.TransactionScopeOption.Required, transactionOptions))
            {
                readingTypeEntity =
                    db.reading_types.Join(db.usage_points, rt => rt.meterid, up => up.meterid, (rt, up) => new { rt, up })
                        .Join(db.subscription_usage_points, rup => rup.up.id, sup => sup.usage_point_id,
                            (rup, sup) => new { rup, sup })
                        .Join(db.subscription, jrup => jrup.sup.subscription_id, s => s.id, (jrup, s) => new { jrup, s })
                        .Where(
                            u =>
                                u.jrup.rup.rt.id == readingTypeId && u.jrup.rup.up.meterid == meterId &&
                                u.s.id == subscriptionId)
                        .Select(u => u.jrup.rup.rt)
                        .FirstOrDefault();
                transScopeAction.Complete();
            }
            return readingTypeEntity;
        }

        private long InsertReadingType(reading_types readingType)
        {
            long readingTypeId;

            //declare the transaction options
            var transactionOptions = new System.Transactions.TransactionOptions();
            //set it to read uncommited
            transactionOptions.IsolationLevel = System.Transactions.IsolationLevel.ReadUncommitted;

            using (
                var transScopeAction =
                    new System.Transactions.TransactionScope(System.Transactions.TransactionScopeOption.Required,
                        transactionOptions))
            {
                db.reading_types.Add(readingType);
                db.SaveChanges();
                readingTypeId = readingType.id;
                var hexId = Helper.ConvertStringToHex(readingType.id.ToString(), Encoding.Unicode);
                readingType.self_link_href = $"{readingType.self_link_href}/{hexId}";
                db.SaveChanges();
                transScopeAction.Complete();
            }
            return readingTypeId;
        }
        private subscription GetSubscription(long subscriptionId)
        {
            subscription subscriptionEntity;

            //declare the transaction options
            //set it to read uncommited
            var transactionOptions = new System.Transactions.TransactionOptions
            {
                IsolationLevel = System.Transactions.IsolationLevel.ReadUncommitted
            };

            using (var transScopeAction = new System.Transactions.TransactionScope(System.Transactions.TransactionScopeOption.Required, transactionOptions))
            {

                subscriptionEntity =
                    db.subscription.FirstOrDefault(u => u.id == subscriptionId);
                transScopeAction.Complete();
            }
            return subscriptionEntity;
        }

        private string GetAccoutId(long? retailCustomerId)
        {
            var accountId = string.Empty;
            //declare the transaction options
            //set it to read uncommited
            if (retailCustomerId.HasValue)
            {
                var transactionOptions = new System.Transactions.TransactionOptions
                {
                    IsolationLevel = System.Transactions.IsolationLevel.ReadUncommitted
                };

                using (var transScopeAction = new System.Transactions.TransactionScope(System.Transactions.TransactionScopeOption.Required, transactionOptions))
                {

                    var retailCustomers = db.retail_customers.FirstOrDefault(u => u.id == retailCustomerId);
                    if (retailCustomers != null)
                        accountId =
                            retailCustomers.data_custodian_account_id;
                    transScopeAction.Complete();
                }
            }

            return accountId;
        }
        private List<usage_points> GetUsagePoints(long subscriptionId)
        {
            var usagePointEntities = new List<usage_points>();

            //declare the transaction options
            var transactionOptions = new System.Transactions.TransactionOptions();
            //set it to read uncommited
            transactionOptions.IsolationLevel = System.Transactions.IsolationLevel.ReadUncommitted;

            using (var transScopeAction = new System.Transactions.TransactionScope(System.Transactions.TransactionScopeOption.Required, transactionOptions))
            {

                var usagepointList =
                    db.subscription_usage_points.Where(u => u.subscription_id == subscriptionId).ToList();

                foreach (var usagePoint in usagepointList)
                {
                    var usagePointEntity = db.usage_points.FirstOrDefault(u => u.id == usagePoint.usage_point_id);
                    usagePointEntities.Add(usagePointEntity);
                }
                transScopeAction.Complete();
            }
            return usagePointEntities;
        }

        private usage_points GetUsagePoint(long subscriptionId, string meterId)
        {
            usage_points usagePointEntity;

            //declare the transaction options
            var transactionOptions = new System.Transactions.TransactionOptions();
            //set it to read uncommited
            transactionOptions.IsolationLevel = System.Transactions.IsolationLevel.ReadUncommitted;

            using (
                var transScopeAction =
                    new System.Transactions.TransactionScope(System.Transactions.TransactionScopeOption.Required,
                        transactionOptions))
            {
                usagePointEntity =
                    db.usage_points.Join(db.subscription_usage_points, up => up.id, sup => sup.usage_point_id,
                        (up, sup) => new { up, sup })
                        .Where(m => m.sup.subscription_id == subscriptionId && m.up.meterid == meterId)
                        .Select(m => m.up).FirstOrDefault();
                transScopeAction.Complete();
            }

            return usagePointEntity;
        }

        private int UpdateLocalTimeConfig(long usagePointId, TimeZoneInfo timeZoneInfo)
        {
            var localTimeId = 1;
            switch (timeZoneInfo.Id)
            {
                case Est:
                    localTimeId = 1;
                    break;
                case Cst:
                    localTimeId = 2;
                    break;
                case Mst:
                    localTimeId = 3;
                    break;
                case Pst:
                    localTimeId = 4;
                    break;
                case Akst:
                    localTimeId = 5;
                    break;
                case Hst:
                    localTimeId = 6;
                    break;
            }

            //declare the transaction options
            var transactionOptions = new System.Transactions.TransactionOptions();
            //set it to read uncommited
            transactionOptions.IsolationLevel = System.Transactions.IsolationLevel.ReadUncommitted;

            // update time configuration id in database
            using (
                var transScopeAction =
                    new System.Transactions.TransactionScope(System.Transactions.TransactionScopeOption.Required,
                        transactionOptions))
            {
                var usagePointEntity = db.usage_points.FirstOrDefault(u => u.id == usagePointId);
                if (usagePointEntity != null)
                {
                    usagePointEntity.local_time_parameters_id = localTimeId;
                    db.SaveChanges();
                }
                transScopeAction.Complete();
            }

            return localTimeId;
        }

        private List<GreenButtonLink> GenerateGreenButtonAmiUsagePointRelatedLinks(string usagePointLink, int localTimeConfigId)
        {
            var relatedLinks = new List<GreenButtonLink>();
            // Genterate meter reading 
            var link = GreenButton.GenerateGreenButtonLink($"{usagePointLink}/{GreenButtonEntryType.MeterReading.ToString()}", GreenButtonLinkRelationshipType.related.ToString());
            relatedLinks.Add(link);
            // Local Time Configuration
            var localTimeConfigRelateLink =
                GreenButton.GenerateGreenButtonLink(
                    $"{GreenButtonLinkPrefix}/{GreenButtonEntryType.LocalTimeParameters.ToString()}/{localTimeConfigId}",
                    GreenButtonLinkRelationshipType.related.ToString());
            relatedLinks.Add(localTimeConfigRelateLink);

            return relatedLinks;
        }

        private List<usage_point_related_links> GetUsagePointRelatedLinks(GreenButtonUsagePoint usagePoint)
        {
            List<usage_point_related_links> usagePointRelatedLinksEntities;

            //declare the transaction options
            var transactionOptions = new System.Transactions.TransactionOptions();
            //set it to read uncommited
            transactionOptions.IsolationLevel = System.Transactions.IsolationLevel.ReadUncommitted;

            using (var transScopeAction = new System.Transactions.TransactionScope(System.Transactions.TransactionScopeOption.Required, transactionOptions))
            {
                usagePointRelatedLinksEntities =
                    db.usage_point_related_links.Where(l => l.usage_point_id == usagePoint.Id).ToList();
                transScopeAction.Complete();
            }


            return usagePointRelatedLinksEntities;

        }

        private void InsertGreenButtonAmiUsagePointRelatedLinks(List<GreenButtonLink> usagePointRelatedLinks, long usagePointId)
        {
            var usagePointRelatedLinksEntities =
                Mapper.Map<List<GreenButtonLink>, List<usage_point_related_links>>(usagePointRelatedLinks);

            //declare the transaction options
            var transactionOptions = new System.Transactions.TransactionOptions();
            //set it to read uncommited
            transactionOptions.IsolationLevel = System.Transactions.IsolationLevel.ReadUncommitted;

            using (
                var transScopeAction =
                    new System.Transactions.TransactionScope(System.Transactions.TransactionScopeOption.Required,
                        transactionOptions))
            {
                foreach (var usagePointRelatedLink in usagePointRelatedLinksEntities)
                {
                    usagePointRelatedLink.usage_point_id = usagePointId;
                }

                db.usage_point_related_links.AddRange(usagePointRelatedLinksEntities);
                db.SaveChanges();
                transScopeAction.Complete();
            }
        }


        private void InitializeAutoMapper()
        {
            Mapper.CreateMap<subscription, GreenButtonSubscription>()
               .ForMember(dest => dest.Id, opt => opt.MapFrom(src => src.id))
               .ForMember(dest => dest.UuId, opt => opt.MapFrom(src => src.uuid))
               .ForMember(dest => dest.ApplicationInformationId, opt => opt.MapFrom(src => src.application_information_id))
               .ForMember(dest => dest.AuthorizationId, opt => opt.MapFrom(src => src.authorization_id))
               .ForMember(dest => dest.RetailCustomerId, opt => opt.MapFrom(src => src.retail_customer_id))
               .ForMember(dest => dest.PublishedDate, opt => opt.MapFrom(src => src.published))
               .ForMember(dest => dest.UpdatedDate, opt => opt.MapFrom(src => src.updated))
               .ForAllUnmappedMembers(o => o.Ignore());

            Mapper.CreateMap<usage_points, GreenButtonUsagePoint>()
               .ForMember(dest => dest.Id, opt => opt.MapFrom(src => src.id))
               .ForMember(dest => dest.UuId, opt => opt.MapFrom(src => src.uuid))
               .ForMember(dest => dest.TimeConfigurationId, opt => opt.MapFrom(src => src.local_time_parameters_id != null ? Convert.ToInt32(src.local_time_parameters_id) : (int?)null))
               .ForMember(dest => dest.MeterId, opt => opt.MapFrom(src => src.meterid))
               .ForMember(dest => dest.PublishedDate, opt => opt.MapFrom(src => src.published))
               .ForMember(dest => dest.UpdatedDate, opt => opt.MapFrom(src => src.updated))
               .ForAllUnmappedMembers(o => o.Ignore());

            Mapper.CreateMap<usage_point_related_links, GreenButtonLink>()
                .ForMember(dest => dest.Href, opt => opt.MapFrom(src => src.href))
                .ForMember(dest => dest.Rel, opt => opt.MapFrom(src => src.rel))
                .ForAllUnmappedMembers(o => o.Ignore());

            Mapper.CreateMap<GreenButtonLink, usage_point_related_links>()
                .ForMember(dest => dest.href, opt => opt.MapFrom(src => src.Href))
                .ForMember(dest => dest.rel, opt => opt.MapFrom(src => src.Rel))
                .ForAllUnmappedMembers(o => o.Ignore());

            Mapper.CreateMap<reading_types, GreenButtonAmiReadingType>()
                .ForMember(dest => dest.AccumulationBehavior, opt => opt.MapFrom(src => Convert.ToInt32(src.accumulationBehaviour)))
                .ForMember(dest => dest.Commodity, opt => opt.MapFrom(src => Convert.ToInt32(src.commodity)))
                .ForMember(dest => dest.ConsumptionTier, opt => opt.MapFrom(src => String.IsNullOrEmpty(src.consumptionsTier) ? (int?)null : Convert.ToInt32(src.consumptionsTier)))
                .ForMember(dest => dest.Currency, opt => opt.MapFrom(src => String.IsNullOrEmpty(src.currency) ? (int?)null : Convert.ToInt32(src.currency)))
                .ForMember(dest => dest.DataQualifier, opt => opt.MapFrom(src => Convert.ToInt32(src.dateQualifier)))
                .ForMember(dest => dest.Aggregate, opt => opt.MapFrom(src => String.IsNullOrEmpty(src.aggregate) ? (int?)null : Convert.ToInt32(src.aggregate)))
                .ForMember(dest => dest.Cpp, opt => opt.MapFrom(src => String.IsNullOrEmpty(src.cpp) ? (int?)null : Convert.ToInt32(src.cpp)))
                .ForMember(dest => dest.FlowDirection, opt => opt.MapFrom(src => Convert.ToInt32(src.flowDirection)))
                .ForMember(dest => dest.InterharMonicDenominator, opt => opt.MapFrom(src => src.interharmonic_denominator))
                .ForMember(dest => dest.InterharmonicNumerator, opt => opt.MapFrom(src => src.interharmonic_numerator))
                .ForMember(dest => dest.IntervalLength, opt => opt.MapFrom(src => Convert.ToInt32(src.intervalLength)))
                .ForMember(dest => dest.Kind, opt => opt.MapFrom(src => Convert.ToInt32(src.kind)))
                .ForMember(dest => dest.MeasuringPeriod, opt => opt.MapFrom(src => String.IsNullOrEmpty(src.measuringPeriod) ? (int?)null : Convert.ToInt32(src.measuringPeriod)))
                .ForMember(dest => dest.Phase, opt => opt.MapFrom(src => Convert.ToInt32(src.phase)))
                .ForMember(dest => dest.PowerOfTenMultiplier, opt => opt.MapFrom(src => Convert.ToInt32(src.powerOfTenMultiplier)))
                .ForMember(dest => dest.RationalDenomiator, opt => opt.MapFrom(src => src.rational_denomiator))
                .ForMember(dest => dest.RationalNumerator, opt => opt.MapFrom(src => src.rational_numerator))
                .ForMember(dest => dest.TimeAttribute, opt => opt.MapFrom(src => Convert.ToInt32(src.timeAttribute)))
                .ForMember(dest => dest.Tou, opt => opt.MapFrom(src => String.IsNullOrEmpty(src.tou) ? (int?)null : Convert.ToInt32(src.tou)))
                .ForMember(dest => dest.Uom, opt => opt.MapFrom(src => Convert.ToInt32(src.uom)))
                .ForMember(dest => dest.UpdatedDate, opt => opt.MapFrom(src => src.updated))
                .ForMember(dest => dest.PublishedDate, opt => opt.MapFrom(src => src.published))
                .ForMember(dest => dest.Uuid, opt => opt.MapFrom(src => src.uuid))
                .ForMember(dest => dest.MeterId, opt => opt.MapFrom(src => src.meterid))
                .ForMember(dest => dest.Id, opt => opt.MapFrom(src => src.id))
                .ForAllUnmappedMembers(o => o.Ignore());

            Mapper.CreateMap<GreenButtonAmiReadingType, reading_types>()
                .ForMember(dest => dest.accumulationBehaviour, opt => opt.MapFrom(src => Convert.ToString(src.AccumulationBehavior)))
                .ForMember(dest => dest.commodity, opt => opt.MapFrom(src => Convert.ToString(src.Commodity)))
                .ForMember(dest => dest.consumptionsTier, opt => opt.MapFrom(src => src.ConsumptionTier == null ? null : Convert.ToString(src.ConsumptionTier)))
                .ForMember(dest => dest.currency, opt => opt.MapFrom(src => src.ConsumptionTier == null ? null : Convert.ToString(src.Currency)))
                .ForMember(dest => dest.dateQualifier, opt => opt.MapFrom(src => Convert.ToString(src.DataQualifier)))
                .ForMember(dest => dest.aggregate, opt => opt.MapFrom(src => src.Aggregate == null ? null : Convert.ToString(src.Aggregate)))
                .ForMember(dest => dest.cpp, opt => opt.MapFrom(src => src.Cpp == null ? null : Convert.ToString(src.Cpp)))
                .ForMember(dest => dest.flowDirection, opt => opt.MapFrom(src => Convert.ToString(src.FlowDirection)))
                .ForMember(dest => dest.interharmonic_denominator, opt => opt.MapFrom(src => src.InterharMonicDenominator))
                .ForMember(dest => dest.interharmonic_numerator, opt => opt.MapFrom(src => src.InterharmonicNumerator))
                .ForMember(dest => dest.intervalLength, opt => opt.MapFrom(src => Convert.ToInt64(src.IntervalLength)))
                .ForMember(dest => dest.kind, opt => opt.MapFrom(src => Convert.ToString(src.Kind)))
                .ForMember(dest => dest.measuringPeriod, opt => opt.MapFrom(src => src.MeasuringPeriod == null ? null : Convert.ToString(src.MeasuringPeriod)))
                .ForMember(dest => dest.phase, opt => opt.MapFrom(src => Convert.ToString(src.Phase)))
                .ForMember(dest => dest.powerOfTenMultiplier, opt => opt.MapFrom(src => Convert.ToString(src.PowerOfTenMultiplier)))
                .ForMember(dest => dest.rational_denomiator, opt => opt.MapFrom(src => src.RationalDenomiator))
                .ForMember(dest => dest.rational_numerator, opt => opt.MapFrom(src => src.RationalNumerator))
                .ForMember(dest => dest.timeAttribute, opt => opt.MapFrom(src => Convert.ToString(src.TimeAttribute)))
                .ForMember(dest => dest.tou, opt => opt.MapFrom(src => src.Tou == null ? null : Convert.ToString(src.Tou)))
                .ForMember(dest => dest.uom, opt => opt.MapFrom(src => Convert.ToString(src.Uom)))
                .ForMember(dest => dest.updated, opt => opt.MapFrom(src => src.UpdatedDate))
                .ForMember(dest => dest.published, opt => opt.MapFrom(src => src.PublishedDate))
                .ForMember(dest => dest.uuid, opt => opt.MapFrom(src => src.Uuid))
                .ForMember(dest => dest.meterid, opt => opt.MapFrom(src => src.MeterId))
                .ForAllUnmappedMembers(o => o.Ignore());

            Mapper.CreateMap<time_configurations, GreenButtonTimeParameter>()
                .ForMember(dest => dest.Id, opt => opt.MapFrom(src => src.id))
                .ForMember(dest => dest.GuidId, opt => opt.MapFrom(src => src.uuid))
                .ForMember(dest => dest.DstEndRule, opt => opt.MapFrom(src => src.dstEndRule))
                .ForMember(dest => dest.DstStartRule, opt => opt.MapFrom(src => src.dstStartRule))
                .ForMember(dest => dest.DstOffset, opt => opt.MapFrom(src => src.dstOffset))
                .ForMember(dest => dest.TzOffset, opt => opt.MapFrom(src => src.tzOffset))
                .ForMember(dest => dest.PublishedDate, opt => opt.MapFrom(src => src.published))
                .ForMember(dest => dest.UpdatedDate, opt => opt.MapFrom(src => src.updated))
                .ForAllUnmappedMembers(o => o.Ignore());

            Mapper.CreateMap<application_information, GreenButtonApplicationInfo>()
                .ForMember(dest => dest.Id, opt => opt.MapFrom(src => src.application_information_id))
                .ForMember(dest => dest.UuId, opt => opt.MapFrom(src => src.uuid))
                .ForMember(dest => dest.DataCustodianApplicationStatus, opt => opt.MapFrom(src => Convert.ToInt32(src.dataCustodianApplicationStatus)))
                .ForMember(dest => dest.DataCustodianId, opt => opt.MapFrom(src => src.dataCustodianId))
                .ForMember(dest => dest.DataCustodianBulkRequestUri, opt => opt.MapFrom(src => src.dataCustodianBulkRequestURI))
                .ForMember(dest => dest.DataCustodianResourceEndpoint, opt => opt.MapFrom(src => src.dataCustodianResourceEndpoint))
                .ForMember(dest => dest.ThirdPartyApplicationName, opt => opt.MapFrom(src => src.client_name))
                .ForMember(dest => dest.ThirdPartyNotifyUri, opt => opt.MapFrom(src => src.thirdPartyNotifyUri))
                .ForMember(dest => dest.ThirdPartyScopeSelectionScreenUri, opt => opt.MapFrom(src => src.thirdPartyScopeSelectionScreenURI))
                .ForMember(dest => dest.ClientSecret, opt => opt.MapFrom(src => src.client_secret))
                .ForMember(dest => dest.ClientId, opt => opt.MapFrom(src => src.client_id))
                .ForMember(dest => dest.RedirectUri, opt => opt.MapFrom(src => src.redirect_uri))
                .ForMember(dest => dest.PublishedDate, opt => opt.MapFrom(src => src.published))
                .ForMember(dest => dest.UpdatedDate, opt => opt.MapFrom(src => src.updated))
                .ForAllUnmappedMembers(o => o.Ignore());
        }
    }
}
