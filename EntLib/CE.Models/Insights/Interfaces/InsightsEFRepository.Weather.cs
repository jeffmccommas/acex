﻿using System;
using System.Collections.Generic;
//using System.Data;
using System.Linq;
using CE.Models.Insights;
using CE.Models.Insights.EF;
using AutoMapper;
using CE.Models.Utils;

// ReSharper disable once CheckNamespace
namespace CE.Models
{
    // ReSharper disable once InconsistentNaming
    // ReSharper disable once RedundantExtendsListEntry
    public partial class InsightsEfRepository : IInsightsEFRepository
    {

        private readonly InsightsMetadataEntities _insightsMetadataEntities = new InsightsMetadataEntities();

        /// <summary>
        /// Testability: pass in test db, ends dependency on database for unit tests
        /// </summary>
        /// <param name="context">test db context used</param>
        public InsightsEfRepository(InsightsMetadataEntities context)
        {
            _insightsMetadataEntities = context;
        }

        /// <summary>
        /// GetBills
        /// </summary>
        /// <param name="clientId"></param>
        /// <param name="request"></param>
        /// <returns></returns>
        public List<WeatherDb> GetWeather(int clientId, WeatherRequest request)
        {
            var weatherDataList = GetWeatherSummary(clientId, request);
            return weatherDataList;
        }

        /// <summary>
        /// helper
        /// </summary>
        private List<WeatherDb> GetWeatherSummary(int clientid, WeatherRequest request)
        {
            Mapper.CreateMap<EMDailyWeatherDetail, WeatherDb>()
                .ForAllUnmappedMembers(o => o.Ignore());

            List<WeatherDb> weatherDataList;

            //declare the transaction options            //set it to read uncommited
            var transactionOptions = new System.Transactions.TransactionOptions
            {
                IsolationLevel = System.Transactions.IsolationLevel.ReadUncommitted
            };

            using (
                var transScope =
                    new System.Transactions.TransactionScope(System.Transactions.TransactionScopeOption.Required,
                        transactionOptions))
            {
                using (_insightsMetadataEntities)
                {
                    //ent.Database.Connection.Open();
                    var envList = (from d in _insightsMetadataEntities.EMDailyWeatherDetails
                                   join z in _insightsMetadataEntities.EMZipcodes on d.StationID equals z.StationIdDaily
                                   where
                                       (z.ZipCode == request.ZipCode && (z.ClientID == 0 || z.ClientID == clientid)) &&
                                       d.WeatherReadingDate >= request.StartDate &&
                                       d.WeatherReadingDate <= request.EndDate
                                   select d).ToList();

                    // Get only those for the station if it is passed in.
                    if (!string.IsNullOrEmpty(request.StationId))
                    {
                        envList = envList.Where(w => w.StationID == request.StationId).ToList();
                    }

                    weatherDataList = Mapper.Map<List<EMDailyWeatherDetail>, List<WeatherDb>>(envList);
                }
                transScope.Complete();
            }

            return weatherDataList;
        }
    }

    /// <summary>
    /// Weather data from database
    /// </summary>
    public class WeatherDb
    {
        public DateTime WeatherReadingDate { get; set; }
        public int AvgTemp { get; set; }
        public int MinTemp { get; set; }
        public int MaxTemp { get; set; }
        public int AvgWetBulb { get; set; }
        public int HeatingDegreeDays { get; set; }
        public int CoolingDegreeDays { get; set; }
    }

}
