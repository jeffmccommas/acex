﻿using System;
using System.Data.Entity;

namespace CE.Models.Insights.EF
{
    public interface IInsightsEntities: IDisposable
    {
      DbSet<Endpoint> Endpoints { get; set; }
      DbSet<EndpointTracking> EndpointTrackings { get; set; }

      DbSet<ProfileAccountAttribute> ProfileAccountAttributes { get; set; }

      DbSet<ProfilePremiseAttribute> ProfilePremiseAttributes { get; set; }

      DbSet<ProfileCustomerAccount> ProfileCustomerAccounts { get; set; }

      DbSet<ProfileCustomerAttribute> ProfileCustomerAttributes { get; set; }
      DbSet<ActionCustomerAccount> ActionCustomerAccount { get; set; }
      DbSet<ActionPremisePlan> ActionPremisePlan { get; set; }

      DbSet<Client> Clients { get; set; }
      DbSet<User> Users { get; set; }
    }
}
