﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity.Infrastructure;
using System.Data.SqlClient;
using System.Linq;
using AutoMapper;
using CE.Models.Insights;
using CE.Models.Insights.EF;
using CE.Models.Utils;
using CE.Models.Insights.Types;

namespace CE.Models
{
    public partial class InsightsEfRepository : IInsightsEFRepository, IDisposable
    {
        /// <summary>
        /// GetCustomer
        /// </summary>
        /// <param name="clientId"></param>
        /// <param name="request"></param>
        /// <returns></returns>
        public CustomerInfoResponse GetCustomerInfo(int clientId, CustomerInfoRequest request)
        {
            var response = new CustomerInfoResponse();
            response.ClientId = clientId;
            response.Customer = GetCustomer(clientId, request);
            if (response.Customer == null)
            {
                response.Message = "No data found";
            }
            return (response);
        }

        /// <summary>
        /// helper
        /// </summary>
        private CustomerInfo GetCustomer(int clientid, CustomerInfoRequest request)
        {
            using (var ent = new InsightsDWEntities(ConnStringBuilder.GetEFConnectionStringInsightsDW(clientid)))
            {
                ent.Configuration.AutoDetectChangesEnabled = false;
                ent.Configuration.ValidateOnSaveEnabled = false;
                ent.Database.Initialize(force: false);

                var cmd = ent.Database.Connection.CreateCommand();
                cmd.CommandText = "[dbo].[GetCustomerInfo]";
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.Add(new SqlParameter("@ClientID", typeof(int)) { Value = clientid });
                cmd.Parameters.Add(new SqlParameter("@CustomerID", typeof(string)) { Value = request.CustomerId });

                try
                {
                    ent.Database.Connection.Open();
                    var reader = cmd.ExecuteReader();
                    var cim = new CustomerInfoMerge();

                    // Read the first result set -- Customer
                    var cic = ((IObjectContextAdapter)ent).ObjectContext.Translate<CustomerInfoDB>(reader).FirstOrDefault();
                    cim.Customer = cic;

                    // Move to second result set -- Account
                    reader.NextResult();
                    List<CustomerInfoAccountDB> cia = ((IObjectContextAdapter)ent).ObjectContext.Translate<CustomerInfoAccountDB>(reader).ToList();
                    cim.Accounts = cia;

                    // Move to third result set -- Premise
                    reader.NextResult();
                    List<CustomerInfoPremiseDB> cip = ((IObjectContextAdapter)ent).ObjectContext.Translate<CustomerInfoPremiseDB>(reader).ToList();
                    cim.Premises = cip;

                    // Move to forth result set -- Service
                    reader.NextResult();
                    List<CustomerInfoServiceDB> cis = ((IObjectContextAdapter)ent).ObjectContext.Translate<CustomerInfoServiceDB>(reader).ToList();
                    cim.Services = cis;

                    // Move to fifth result set -- Service Point
                    reader.NextResult();
                    List<CustomerInfoServicePointDB> cisp = ((IObjectContextAdapter)ent).ObjectContext.Translate<CustomerInfoServicePointDB>(reader).ToList();
                    cim.ServicePoints = cisp;

                    // Move to sixth result set -- Service Point
                    reader.NextResult();
                    List<CustomerInfoMeterDB> cimeter = ((IObjectContextAdapter)ent).ObjectContext.Translate<CustomerInfoMeterDB>(reader).ToList();
                    cim.Meters = cimeter;

                    Mapper.CreateMap<CustomerInfoMerge, CustomerInfo>().ConvertUsing(new CustomerInfoConverter());
                    var c = new CustomerInfo();
                    if (cic == null)
                    {
                        return null;
                    }
                    else
                    {
                        return Mapper.Map(cim, c);
                    }
                }
                finally
                {
                    ent.Database.Connection.Close();
                }
            }
        }

        public virtual List<string> GetMeterList(int clientid, string customerId, string accountId, string premiseId, GreenButtonScope scopes)
        {
            var meterList = new List<string>();
            //get customer info so we can check for current meters in dw (non-gbc)      
            var request = new CustomerInfoRequest();
            request.CustomerId = customerId;
            var customerInfo = GetCustomerInfo(clientid, request);
            if (customerInfo != null)
            {

                //compare customerInfo meters to gbc meters, insert any new ones into gbc
                var comList = new List<string>();

                if (scopes.FunctionBlocks.Exists(fb => fb == GreenButtonFunctionBlockType.IntervalElecMetering))
                {
                    comList.Add("electric");
                }
                if (scopes.FunctionBlocks.Exists(fb => fb == GreenButtonFunctionBlockType.Gas))
                {
                    comList.Add("gas");
                }
                if (scopes.FunctionBlocks.Exists(fb => fb == GreenButtonFunctionBlockType.Water))
                {
                    comList.Add("water");
                }

                meterList = customerInfo.Customer.Accounts.Where(x => x.Id == accountId)
                                    .SelectMany(c => c.Premises)
                                    .Where(p => p.Id == premiseId)
                                    .SelectMany(s => s.Service)
                                    .Where(c => comList.Contains(c.CommodityKey))
                                    .SelectMany(c => c.ServicePoints)
                                    .SelectMany(c => c.Meters).Select(x => x.Id).ToList();

            }
                return meterList;
        }
    }


    public class CustomerInfoConverter : ITypeConverter<CustomerInfoMerge, CustomerInfo>
    {
        public CustomerInfo Convert(ResolutionContext context)
        {
            var sourceCollection = (CustomerInfoMerge)context.SourceValue;

            Mapper.CreateMap<CustomerInfoDB, CustomerInfo>()
                       .ForMember(dest => dest.Id, opt => opt.MapFrom(src => src.CustomerId))
                       .ForAllUnmappedMembers(o => o.Ignore());

            //foreach (var memberName in typeMap.GetUnmappedPropertyNames())
            //    mapping.ForMember(memberName, memberOptions);
            var cust = new CustomerInfo();
            Mapper.Map(sourceCollection.Customer, cust);

            Mapper.CreateMap<CustomerInfoAccountDB, CustomerInfoAccount>()
                    .ForMember(dest => dest.Id, opt => opt.MapFrom(src => src.AccountId))
                    .ForAllUnmappedMembers(o => o.Ignore());

            Mapper.CreateMap<CustomerInfoPremiseDB, CustomerInfoPremise>()
                   .ForMember(dest => dest.Id, opt => opt.MapFrom(src => src.PremiseId))
                   .ForAllUnmappedMembers(o => o.Ignore()); 

            Mapper.CreateMap<CustomerInfoServiceDB, CustomerInfoService>()
                  .ForMember(dest => dest.Id, opt => opt.MapFrom(src => src.ServiceContractId))
                  .ForAllUnmappedMembers(o => o.Ignore());

            Mapper.CreateMap<CustomerInfoServicePointDB, CustomerInfoServicePoint>()
                  .ForMember(dest => dest.Id, opt => opt.MapFrom(src => src.ServicePointId))
                  .ForAllUnmappedMembers(o => o.Ignore());

            Mapper.CreateMap<CustomerInfoMeterDB, CustomerInfoMeter>()
                   .ForMember(dest => dest.Id, opt => opt.MapFrom(src => src.MeterId))
                   .ForAllUnmappedMembers(o => o.Ignore());

            var accounts = sourceCollection.Accounts.Select(x => x.AccountId).ToList();
            var acctList = new List<CustomerInfoAccount>();
            foreach (var a in accounts)
            {
                var acct = new CustomerInfoAccount();
                var cipl = new List<CustomerInfoPremise>();
                acct.Id = a;
                var premises = sourceCollection.Premises.Where(x => x.AccountId == a).ToList();

                foreach (var p in premises)
                {
                    var cip = new CustomerInfoPremise();
                    var cisl = new List<CustomerInfoService>();
                    var services = sourceCollection.Services.Where(x => x.PremiseId == p.PremiseId).ToList();

                    foreach (var r in services)
                    {
                        var spl = new List<CustomerInfoServicePoint>();
                        var cis = new CustomerInfoService();
                        var sps = sourceCollection.ServicePoints.Where(x => x.ServiceContractId == r.ServiceContractId).ToList();
                       
                        foreach (var sp in sps)
                        {
                            var cisp = new CustomerInfoServicePoint();
                            var ml = new List<CustomerInfoMeter>();
                            var ms = sourceCollection.Meters.Where(x => x.ServicePointId == sp.ServicePointId).ToList();
                            foreach (var m in ms)
                            {
                                ml.Add(EntityMapper.Map<CustomerInfoMeter>(m));
                            }
                            Mapper.Map(sp, cisp);
                            cisp.Meters = ml;
                            spl.Add(cisp);
                        }
                        Mapper.Map(r, cis);
                        cis.ServicePoints = spl;
                        cisl.Add(cis);
                    }
                    Mapper.Map(p, cip);
                    cip.Service = cisl;
                    cipl.Add(cip);
                }
                acct.Premises = cipl;
                acctList.Add(acct);
            }
            cust.Accounts = acctList;
            return cust;
        }
    }

    public class CustomerInfoMerge
    {
        public CustomerInfoDB Customer { get; set; }
        public List<CustomerInfoAccountDB> Accounts { get; set; }
        public List<CustomerInfoPremiseDB> Premises { get; set; }
        public List<CustomerInfoServiceDB> Services { get; set; }
        public List<CustomerInfoServicePointDB> ServicePoints { get; set; }
        public List<CustomerInfoMeterDB> Meters { get; set; }
    }

    public class CustomerInfoDB
    {
        public string CustomerId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Addr1 { get; set; }
        public string Addr2 { get; set; }
        public string City { get; set; }
        public string StateProvince { get; set; }
        public string PostalCode { get; set; }
        public string Country { get; set; }
        public string PhoneNumber { get; set; }
        public string MobilePhoneNumber { get; set; }
        public string EmailAddress { get; set; }
        public string AlternateEmailAddress { get; set; }
        public string Language { get; set; }
    }

    public class CustomerInfoAccountDB
    {
        public string CustomerId { get; set; }
        public string AccountId { get; set; }
    }

    public class CustomerInfoPremiseDB
    {
        public string AccountId { get; set; }
        public string PremiseId { get; set; }
        public string Addr1 { get; set; }
        public string Addr2 { get; set; }
        public string City { get; set; }
        public string StateProvince { get; set; }
        public string PostalCode { get; set; }
        public string Country { get; set; }
    }

    public class CustomerInfoServiceDB
    {
        public string ServiceContractId { get; set; }
        public string PremiseId { get; set; }
        public string Type { get; set; }
        public string Description { get; set; }
        public string ActiveDate { get; set; }
        public string InActiveDate { get; set; }
        public string CommodityKey { get; set; }
        public string BudgetBillingInd { get; set; }
    }

    public class CustomerInfoServicePointDB
    {
        public string ServicePointId { get; set; }
        public string ServiceContractId { get; set; }
    }

    public class CustomerInfoMeterDB
    {
        public string MeterId { get; set; }
        public string ServicePointId { get; set; }
        public string IsDeleted { get; set; }
        public string ReplacedMeterId { get; set; }
    }
}



