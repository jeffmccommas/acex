﻿using System;
using System.Collections.Generic;
using System.Linq;
using CE.Models.Insights.EF;
using LinqKit;

namespace CE.Models
{
    /// <summary>
    /// InsightsEFRepository
    /// </summary>
    public partial class InsightsEfRepository : IInsightsEFRepository, IDisposable
    {
        /// <summary>
        /// This KeepAlive grabs customer accounts and takes top one
        /// </summary>
        /// <param name="clientId"></param>
        public void KeepAlive(int clientId)
        {
            //declare the transaction options
            var transactionOptions = new System.Transactions.TransactionOptions();
            //set it to read uncommited
            transactionOptions.IsolationLevel = System.Transactions.IsolationLevel.ReadUncommitted;

            List<ProfileCustomerAccount> profileCustomerAccounts;

            var predicateCustomerAccount = PredicateBuilder.True<ProfileCustomerAccount>();
            predicateCustomerAccount = predicateCustomerAccount.And(m => m.ClientID == clientId);

            using (var transScope = new System.Transactions.TransactionScope(System.Transactions.TransactionScopeOption.Required, transactionOptions))
            {
                using (var ent = new InsightsEntities())
                {
                    ent.Database.Connection.Open();
                    profileCustomerAccounts = (from pca in ent.ProfileCustomerAccounts.AsExpandable().Where(predicateCustomerAccount)
                                               select pca).Take(1).ToList();
                }
                transScope.Complete();
            }
        }
    }
}
