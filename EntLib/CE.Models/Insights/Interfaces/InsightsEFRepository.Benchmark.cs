﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity.Infrastructure;
using System.Data.SqlClient;
using System.Linq;
using AutoMapper;
using CE.Models.Insights;
using CE.Models.Insights.EF;
using CE.Models.Utils;

namespace CE.Models
{
    public partial class InsightsEfRepository : IInsightsEFRepository, IDisposable
    {
        /// <summary>
        /// GetBenchmark
        /// </summary>
        /// <param name="clientId"></param>
        /// <param name="request"></param>
        /// <returns></returns>
        public BenchmarkResponse GetBenchmark(int clientId, BenchmarkRequest request)
        {
            var response = new BenchmarkResponse();
            response.ClientId = clientId;
            response.Customer = GetPeerBenchmarks(clientId, request);
            if (response.Customer == null)
            {
                response.Message = "No data found";
            }
            return (response);
        }

        /// <summary>
        /// helper
        /// </summary>
        private Customer GetPeerBenchmarks(int clientid, BenchmarkRequest request)
        {
            using (var ent = new InsightsDWEntities(ConnStringBuilder.GetEFConnectionStringInsightsDW(clientid)))
            {
                // If using Code First we need to make sure the model is built before we open the connection 
                // This isn't required for models created with the EF Designer 
                ent.Database.Initialize(force: false);

                var cmd = ent.Database.Connection.CreateCommand();
                cmd.CommandText = "[dbo].[GetBenchmarks]";
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.Add(new SqlParameter("@ClientID", typeof(int)) { Value = clientid });
                cmd.Parameters.Add(new SqlParameter("@CustomerID", typeof(string)) { Value = request.CustomerId });
                cmd.Parameters.Add(new SqlParameter("@StartDate", typeof(DateTime)) { Value = request.StartDate });
                cmd.Parameters.Add(new SqlParameter("@EndDate", typeof(DateTime)) { Value = request.EndDate });
                cmd.Parameters.Add(new SqlParameter("@Count", typeof(int)) { Value = (request.Count == 0 ? 1 : request.Count) });
                cmd.Parameters.Add(new SqlParameter("@Groups", typeof(string)) { Value = (request.GroupKeys == null ? "default" : request.GroupKeys) });
                cmd.Parameters.Add(new SqlParameter("@Measurements", typeof(string)) { Value = request.MeasurementKeys });
                if (!string.IsNullOrWhiteSpace(request.AccountId))
                {
                    cmd.Parameters.Add(new SqlParameter("@AccountID", typeof(string)) { Value = request.AccountId });
                }
                if (!string.IsNullOrWhiteSpace(request.PremiseId))
                {
                    cmd.Parameters.Add(new SqlParameter("@PremiseID", typeof(string)) { Value = request.PremiseId });
                }

                try
                {
                    ent.Database.Connection.Open();
                    var reader = cmd.ExecuteReader();
                    PeerMerge pm = new PeerMerge();

                    // Read the first result set 
                    List<Peer> p = ((IObjectContextAdapter)ent).ObjectContext.Translate<Peer>(reader).ToList();
                    pm.Peer = p;

                    // Move to second result set 
                    if (request.MeasurementKeys != null)
                    {
                        reader.NextResult();
                        List<PeerData> pd = ((IObjectContextAdapter)ent).ObjectContext.Translate<PeerData>(reader).ToList();
                        pm.PeerData = pd;
                    }

                    Mapper.CreateMap<PeerMerge, Customer>().ConvertUsing(new Converter());
                    var c = new Customer();
                    if (p.Count == 0)
                    {
                        return null;
                    }
                    else
                    {
                        return Mapper.Map(pm, c);
                    }
                }
                finally
                {
                    ent.Database.Connection.Close();
                }
            }

        }

    }

    public class Converter : ITypeConverter<PeerMerge, Customer>
    {
        public Customer Convert(ResolutionContext context)
        {
            //var destCollection = (Customer)context.DestinationValue;
            var sourceCollection = (PeerMerge)context.SourceValue;
            var cust = new Customer();
            var accts = new List<Account>();

            Mapper.CreateMap<Peer, Benchmark>().ForAllUnmappedMembers(o => o.Ignore());
            Mapper.CreateMap<PeerData, BenchmarkMeasurement>().ForAllUnmappedMembers(o => o.Ignore());
            Mapper.CreateMap<List<PeerData>, Benchmark>()
                     .ForMember(dest => dest.Measurements,
                                opt => opt.MapFrom(
                                    src => Mapper.Map<List<PeerData>, List<BenchmarkMeasurement>>(src)))
                                    .ForAllUnmappedMembers(o => o.Ignore());

            cust.Id = sourceCollection.Peer.First().CustomerId;
            var accounts = sourceCollection.Peer.Select(x => x.AccountId).Distinct();

            foreach (var a in accounts)
            {
                var acct = new Account();
                acct.Id = a;
                var premises = sourceCollection.Peer.Where(x => x.AccountId == a).Select(x => x.PremiseId).Distinct();

                foreach (var r in premises)
                {
                    var bm = new List<Benchmark>();
                    var p = sourceCollection.Peer.Where(x => x.PremiseId == r).ToList();

                    var premise = new Premise();
                    premise.Id = p.First().PremiseId;

                    foreach (var source in p)
                    {
                        if (sourceCollection.PeerData != null)
                        {
                            var pd = sourceCollection.PeerData.Where(d => d.RowIdentifier == source.RowIdentifier).ToList();
                            bm.Add(EntityMapper.Map<Benchmark>(source, pd));
                        }
                        else
                        {
                            bm.Add(EntityMapper.Map<Benchmark>(source));
                        }
                    }
                    premise.Benchmarks = bm;
                    premise.EndUses = null;
                    acct.Premises.Add(premise);
                }
                accts.Add(acct);
            }
            cust.Accounts = accts;
            return cust;
        }
    }

    public class Peer
    {
        public Int64 RowIdentifier { get; set; }
        public int ClientId { get; set; }
        public string CustomerId { get; set; }
        public string AccountId { get; set; }
        public string PremiseId { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public string GroupKey { get; set; }
        public string CommodityKey { get; set; }
        public string Key { get; set; }
        public decimal MyUsage { get; set; }
        public decimal MyCost { get; set; }
        public decimal AverageUsage { get; set; }
        public decimal AverageCost { get; set; }
        public decimal EfficientUsage { get; set; }
        public decimal EfficientCost { get; set; }
        public string CostCurrencyKey { get; set; }
        public string UOMKey { get; set; }
        public int GroupCount { get; set; }
    }

    public class PeerData
    {
        public Int64 RowIdentifier { get; set; }
        //public int ClientId { get; set; }
        //public string CustomerId { get; set; }
        //public string AccountId { get; set; }
        //public string PremiseId { get; set; }
        //public string GroupKey { get; set; }
        //public int FuelKey { get; set; }
        //public DateTime EndDate { get; set; }
        public string Key { get; set; }
        public decimal MyQuantity { get; set; }
        public decimal AverageQuantity { get; set; }
        public decimal EfficientQuantity { get; set; }
        public string UOMKey { get; set; }
    }

    public class PeerMerge
    {
        public List<Peer> Peer { get; set; }
        public List<PeerData> PeerData { get; set; }
    }
}
