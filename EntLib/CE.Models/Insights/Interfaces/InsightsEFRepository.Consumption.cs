﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity.Infrastructure;
using System.Data.SqlClient;
using System.Linq;
using AutoMapper;
using CE.Models.Insights;
using CE.Models.Insights.EF;
using CE.Models.Utils;

namespace CE.Models
{
    public partial class InsightsEfRepository : IInsightsEFRepository, IDisposable
    {
        /// <summary>
        /// GetConsumption
        /// </summary>
        /// <param name="clientId"></param>
        /// <param name="request"></param>
        /// <returns></returns>
        public ConsumptionResponse GetConsumption(int clientId, ConsumptionRequest request)
        {
            var response = new ConsumptionResponse();
            var resp = GetConsumptionSummary(clientId, request);
            if (resp == null)
            {
                response.Message = "No data found";
            }
            else
            {
                response = resp;
            }
            return (response);
        }

        /// <summary>
        /// helper
        /// </summary>
        private ConsumptionResponse GetConsumptionSummary(int clientid, ConsumptionRequest request)
        {
            using (var ent = new InsightsDWEntities(ConnStringBuilder.GetEFConnectionStringInsightsDW(clientid)))
            {
                ent.Configuration.AutoDetectChangesEnabled = false;
                ent.Configuration.ValidateOnSaveEnabled = false;

                ent.Database.Initialize(force: false);

                var cmd = ent.Database.Connection.CreateCommand();
                cmd.CommandText = "[dbo].[GetConsumption]";
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.Add(new SqlParameter("@ClientID", typeof(int)) { Value = clientid });
                cmd.Parameters.Add(new SqlParameter("@CustomerID", typeof(string)) { Value = request.CustomerId });
                cmd.Parameters.Add(new SqlParameter("@AccountID", typeof(string)) { Value = request.AccountId });
                cmd.Parameters.Add(new SqlParameter("@PremiseID", typeof(string)) { Value = request.PremiseId });
                cmd.Parameters.Add(new SqlParameter("@ServicePointID", typeof(string)) { Value = request.ServicePointId });
                cmd.Parameters.Add(new SqlParameter("@StartDate", typeof(DateTime)) { Value = request.StartDate });
                cmd.Parameters.Add(new SqlParameter("@EndDate", typeof(DateTime)) { Value = request.EndDate });
                if (!string.IsNullOrWhiteSpace(request.ResolutionKey))
                {
                    cmd.Parameters.Add(new SqlParameter("@ResolutionKey", typeof(int)) { Value = request.ResolutionKey });
                }
                if (!string.IsNullOrWhiteSpace(request.CommodityKey))
                {
                    cmd.Parameters.Add(new SqlParameter("@CommodityKey", typeof(int)) { Value = request.CommodityKey });
                }
                if (request.PageIndex > 0)
                {
                    cmd.Parameters.Add(new SqlParameter("@Page", typeof(int)) { Value = request.PageIndex });
                }
                try
                {
                    ent.Database.Connection.Open();
                    var reader = cmd.ExecuteReader();
                    ConsumptionResponse cr = new ConsumptionResponse();
                    ConsumptionMerge cm = new ConsumptionMerge();

                    Mapper.CreateMap<ConsumptionMerge, ConsumptionResponse>().ForAllUnmappedMembers(o => o.Ignore());
                    Mapper.CreateMap<ConsumptionDataDB, ConsumptionData>()
                        .ForMember(dest => dest.DateTime, opt => opt.MapFrom(src => src.DateStamp))
                        .ForMember(dest => dest.Value, opt => opt.MapFrom(src => src.Quantity))
                        .ForAllUnmappedMembers(o => o.Ignore());

                    Mapper.CreateMap<List<ConsumptionDataDB>, ConsumptionResponse>()
                    .ForMember(dest => dest.Data,
                               opt => opt.MapFrom(
                                   src => Mapper.Map<List<ConsumptionDataDB>, List<ConsumptionData>>(src)))
                                   .ForAllUnmappedMembers(o => o.Ignore());

                    // Read the first result set
                    cm = ((IObjectContextAdapter)ent).ObjectContext.Translate<ConsumptionMerge>(reader).FirstOrDefault();

                    // Move to second result set 
                    reader.NextResult();
                    List<ConsumptionDataDB> cd = ((IObjectContextAdapter)ent).ObjectContext.Translate<ConsumptionDataDB>(reader).ToList();

                    if (cm == null) { return null; }
                    cr = EntityMapper.Map<ConsumptionResponse>(cm, cd);

                    return cr;
                }
                finally
                {
                    ent.Database.Connection.Close();
                }
            }

        }
    }

    public class ConsumptionMerge
    {
        public int ClientId { get; set; }
        public string CustomerId { get; set; }
        public string AccountId { get; set; }
        public string PremiseId { get; set; }
        public string ServicePointId { get; set; }
        public string ResolutionKey { get; set; }
        public string ResolutionsAvailable { get; set; }
        public string CommodityKey { get; set; }
        public string UOMKey { get; set; }
        public Int32 PageSize { get; set; }
        public Int32 PageIndex { get; set; }
    }

    public class ConsumptionDataDB
    {
        public DateTime DateStamp { get; set; }
        public decimal Quantity { get; set; }
    }
}
