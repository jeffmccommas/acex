﻿using System;
using System.Collections.Generic;
using System.Linq;
using CE.Models.Insights.EF;
using LinqKit;

namespace CE.Models
{
    /// <summary>
    /// InsightsEFRepository
    /// </summary>
    public partial class InsightsEfRepository : IInsightsEFRepository, IDisposable
    {
        /// <summary>
        /// This gets the group id.
        /// </summary>
        /// <param name="groupName"></param>
        public int? GetGroupId(string groupName)
        {
            if (string.IsNullOrEmpty(groupName))
            {
                return null;
            }
            var transactionOptions = new System.Transactions.TransactionOptions
            {
                IsolationLevel = System.Transactions.IsolationLevel.ReadUncommitted
            };
            int? groupId = null;
            using (var transScope = new System.Transactions.TransactionScope(System.Transactions.TransactionScopeOption.Required, transactionOptions))
            {
                using (var ent = new InsightsEntities())
                {
                    ent.Database.Connection.Open();

                    var grp = (from g in ent.Group
                               where g.GroupName == groupName
                               select new { g.GroupID }).FirstOrDefault();

                    if (grp != null)
                    {
                        groupId = grp.GroupID;
                    }
                }
                transScope.Complete();
            }
            return groupId;
        }

        /// <summary>
        /// This gets the role id.
        /// </summary>
        /// <param name="roleName"></param>
        public int? GetRoleId(string roleName)
        {
            if (string.IsNullOrEmpty(roleName))
            {
                return null;
            }
            var transactionOptions = new System.Transactions.TransactionOptions
            {
                IsolationLevel = System.Transactions.IsolationLevel.ReadUncommitted
            };
            int? roleId= null;
            using (var transScope = new System.Transactions.TransactionScope(System.Transactions.TransactionScopeOption.Required, transactionOptions))
            {
                using (var ent = new InsightsEntities())
                {
                    ent.Database.Connection.Open();

                    var role = (from r in ent.Role
                              where r.RoleName == roleName
                              select new { r.RoleID }).FirstOrDefault();
                    if (role != null)
                    {
                        roleId = role.RoleID;
                    }
                }
                transScope.Complete();
            }
            return roleId;
        }
    }
}
