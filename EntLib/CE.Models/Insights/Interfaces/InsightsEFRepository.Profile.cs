﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity.Infrastructure;
using System.Data.SqlClient;
using System.Linq;
using CE.Models.Insights;
using CE.Models.Insights.EF;
using LinqKit;

// ReSharper disable once CheckNamespace
namespace CE.Models
{
    /// <summary>
    /// InsightsEFRepository
    /// </summary>
    public partial class InsightsEfRepository
    {
        public ProfileResponse GetProfile(int clientId, ProfileRequest request)
        {
            //declare the transaction options            
            //set it to read uncommited
            var transactionOptions =
                new System.Transactions.TransactionOptions
                {
                    IsolationLevel = System.Transactions.IsolationLevel.ReadUncommitted
                };


            var profResponse = new ProfileResponse();

            List<ProfileCustomerAccount> profileCustomerAccounts;
            var accountList = new List<ProfileAccount>();

            var predicateCustomerAccount = PredicateBuilder.True<ProfileCustomerAccount>();
            predicateCustomerAccount = predicateCustomerAccount.And(m => m.ClientID == clientId && m.CustomerID == request.CustomerId);
            if (!string.IsNullOrEmpty(request.AccountId))
            {
                predicateCustomerAccount = predicateCustomerAccount.And(m => m.AccountID == request.AccountId);
            }

            using (var transScope = new System.Transactions.TransactionScope(System.Transactions.TransactionScopeOption.Required, transactionOptions))
            {
                using (var ent = new InsightsEntities())
                {
                    ent.Database.Connection.Open();
                    profileCustomerAccounts = (from pca in ent.ProfileCustomerAccounts.AsExpandable().Where(predicateCustomerAccount)
                                               select pca).ToList();
                }
                transScope.Complete();
            }

            if (profileCustomerAccounts.Count == 0)
            {
                GetNewCustomerFromBi(clientId, request);
            }

            //create the transaction scope, passing our options in            
            using (var transactionScope = new System.Transactions.TransactionScope(System.Transactions.TransactionScopeOption.Required, transactionOptions))
            {
                using (var ent = new InsightsEntities())
                {
                    ent.Database.Connection.Open();
                    if (profileCustomerAccounts.Count == 0)
                    {
                        profileCustomerAccounts = (from pca in ent.ProfileCustomerAccounts.AsExpandable().Where(predicateCustomerAccount)
                                                   select pca).ToList();
                    }
                    foreach (var acct in profileCustomerAccounts)
                    {
                        //Account
                        var predicateAccount = PredicateBuilder.True<ProfileAccountAttribute>();
                        predicateAccount = predicateAccount.And(m => m.ClientID == clientId);
                        predicateAccount = predicateAccount.And(m => m.AccountID == acct.AccountID);
                        if (request.StartDate != DateTime.MinValue)
                        {
                            predicateAccount = predicateAccount.And(m => (m.EffectiveDate >= request.StartDate));
                        }
                        if (request.EndDate != DateTime.MinValue)
                        {
                            predicateAccount = predicateAccount.And(m => (m.EffectiveDate <= request.EndDate));
                        }
                        if (!string.IsNullOrEmpty(request.ProfileAttributeKeys))
                        {
                            var inner = PredicateBuilder.False<ProfileAccountAttribute>();
                            foreach (var attr in request.ProfileAttributeKeys.Split(','))
                            {
                                inner = inner.Or(m => m.AttributeKey.Contains(attr));
                            }
                            predicateAccount = predicateAccount.And(inner.Expand());
                        }
                        if (!string.IsNullOrEmpty(request.SourceKeys))
                        {
                            var inner = PredicateBuilder.False<ProfileAccountAttribute>();
                            foreach (var src in request.SourceKeys.Split(','))
                            {
                                inner = inner.Or(m => m.Source.Contains(src));
                            }
                            predicateAccount = predicateAccount.And(inner.Expand());
                        }
                        var accountAttributes = (from x in ent.ProfileAccountAttributes.AsExpandable().Where(predicateAccount)
                                                 select new ProfileAttribute
                                                 {
                                                     AttributeKey = x.AttributeKey,
                                                     AttributeValue = x.AttributeValue,
                                                     SourceKey = x.Source,
                                                     EffectiveDate = x.EffectiveDate
                                                 }).ToList();


                        var account = new ProfileAccount
                        {
                            Id = acct.AccountID,
                            Attributes = accountAttributes
                        };

                        //premise
                        var predicatePremise = PredicateBuilder.True<ProfilePremiseAttribute>();
                        predicatePremise = predicatePremise.And(m => m.ClientID == clientId && m.AccountID == acct.AccountID);
                        if (!string.IsNullOrEmpty(request.PremiseId))
                        {
                            predicatePremise = predicatePremise.And(m => m.PremiseID == request.PremiseId);
                        }

                        var profilePremises = (from x in ent.ProfilePremiseAttributes.AsExpandable().Where(predicatePremise)
                                               group x by new { x.ClientID, x.AccountID, x.PremiseID } into g
                                               select g).ToList();

                        var premiseList = new List<ProfilePremise>();
                        foreach (var prem in profilePremises)
                        {
                            var predicate = PredicateBuilder.True<ProfilePremiseAttribute>();
                            predicate = predicate.And(m => m.ClientID == clientId);

                            if (request.StartDate != DateTime.MinValue)
                            {
                                predicate = predicate.And(m => (m.EffectiveDate >= request.StartDate));
                            }
                            if (request.EndDate != DateTime.MinValue)
                            {
                                predicate = predicate.And(m => (m.EffectiveDate <= request.EndDate));
                            }
                            predicate = predicate.And(m => m.AccountID == acct.AccountID);
                            predicate = predicate.And(m => m.PremiseID == prem.Key.PremiseID);
                            if (!string.IsNullOrEmpty(request.ProfileAttributeKeys))
                            {
                                var inner = PredicateBuilder.False<ProfilePremiseAttribute>();
                                foreach (var attr in request.ProfileAttributeKeys.Split(','))
                                {
                                    inner = inner.Or(m => m.AttributeKey.Contains(attr));
                                }
                                predicate = predicate.And(inner.Expand());
                            }

                            if (!string.IsNullOrEmpty(request.SourceKeys))
                            {
                                var inner = PredicateBuilder.False<ProfilePremiseAttribute>();
                                foreach (var src in request.SourceKeys.Split(','))
                                {
                                    inner = inner.Or(m => m.Source.Contains(src));
                                }
                                predicate = predicate.And(inner.Expand());
                            }
                            var premiseAttributes = (from x in ent.ProfilePremiseAttributes.AsExpandable().Where(predicate)
                                                     select new ProfileAttribute
                                                     {
                                                         AttributeKey = x.AttributeKey,
                                                         AttributeValue = x.AttributeValue,
                                                         SourceKey = x.Source,
                                                         EffectiveDate = x.EffectiveDate
                                                     }).ToList();


                            var premise = new ProfilePremise
                            {
                                Id = prem.Key.PremiseID,
                                Attributes = premiseAttributes
                            };

                            premiseList.Add(premise);
                        }
                        account.Premises = premiseList;

                        accountList.Add(account);
                    }

                    //Customer               
                    var predicateCustomer = PredicateBuilder.True<ProfileCustomerAttribute>();
                    predicateCustomer = predicateCustomer.And(m => m.ClientID == clientId);

                    if (!string.IsNullOrEmpty(request.CustomerId))
                    {
                        predicateCustomer = predicateCustomer.And(m => m.CustomerID == request.CustomerId);
                    }
                    if (request.StartDate != DateTime.MinValue)
                    {
                        predicateCustomer = predicateCustomer.And(m => (m.EffectiveDate >= request.StartDate));
                    }
                    if (request.EndDate != DateTime.MinValue)
                    {
                        predicateCustomer = predicateCustomer.And(m => (m.EffectiveDate <= request.EndDate));
                    }
                    if (!string.IsNullOrEmpty(request.ProfileAttributeKeys))
                    {
                        var inner = PredicateBuilder.False<ProfileCustomerAttribute>();
                        foreach (var attr in request.ProfileAttributeKeys.Split(','))
                        {
                            inner = inner.Or(m => m.AttributeKey.Contains(attr));
                        }
                        predicateCustomer = predicateCustomer.And(inner.Expand());
                    }
                    if (!string.IsNullOrEmpty(request.SourceKeys))
                    {
                        var inner = PredicateBuilder.False<ProfileCustomerAttribute>();
                        foreach (var src in request.SourceKeys.Split(','))
                        {
                            inner = inner.Or(m => m.Source.Contains(src));
                        }
                        predicateCustomer = predicateCustomer.And(inner.Expand());
                    }

                    var customerAttributes = (from x in ent.ProfileCustomerAttributes.AsExpandable().Where(predicateCustomer)
                                              select new ProfileAttribute
                                              {
                                                  AttributeKey = x.AttributeKey,
                                                  AttributeValue = x.AttributeValue,
                                                  SourceKey = x.Source,
                                                  EffectiveDate = x.EffectiveDate
                                              }).ToList();

                    var customer = new ProfileCustomer
                    {
                        Id = request.CustomerId,
                        Attributes = customerAttributes,
                        Accounts = accountList
                    };
                    profResponse.Customer = customer;
                    transactionScope.Complete();
                }
            }
            profResponse.ClientId = clientId;
            return profResponse;
        }

        public ProfilePostResponse PostProfile(int clientId, ProfilePostRequest request)
        {
            var profResponse = new ProfilePostResponse();
            //declare the transaction options            //set it to read uncommited
            var transactionOptions =
                new System.Transactions.TransactionOptions
                {
                    IsolationLevel = System.Transactions.IsolationLevel.ReadUncommitted
                };

            //create the transaction scope, passing our options in            
            using (var transactionScope = new System.Transactions.TransactionScope(System.Transactions.TransactionScopeOption.Required, transactionOptions))
            {
                using (var ent = new InsightsEntities())
                {
                    using (var dbContextTransaction = ent.Database.BeginTransaction())
                    {
                        try
                        {
                            ent.Configuration.AutoDetectChangesEnabled = false;
                            ent.Configuration.ValidateOnSaveEnabled = false;
                            var accts = request.Customer.Accounts.Select(x => x.Id).ToList();
                            var attrCount = 0;
                            //Check each acct if it exists                        
                            foreach (var acct in accts)
                            {
                                List<ProfileCustomerAccount> profileCustomerAccounts = (from pca in ent.ProfileCustomerAccounts
                                                                                        where pca.ClientID == clientId && pca.CustomerID == request.Customer.Id && pca.AccountID == acct
                                                                                        select pca).ToList();

                                if (profileCustomerAccounts.Count == 0)
                                {
                                    //Save Content                        

                                    //ProfileCustomerAccount
                                    ProfileCustomerAccount pca = new ProfileCustomerAccount
                                    {
                                        ClientID = clientId,
                                        CustomerID = request.Customer.Id,
                                        AccountID = acct
                                    };
                                    ent.Entry(pca).State = System.Data.Entity.EntityState.Added;
                                    ent.ProfileCustomerAccounts.Add(pca);

                                    //ProfileCustomerAttributes
                                    var customerExists = (from c in ent.ProfileCustomerAttributes
                                        where c.ClientID == clientId && c.CustomerID == request.Customer.Id
                                        select c).ToList();

                                    if (customerExists.Count == 0)
                                    {
                                        var profileCustomerAttrs = request.Customer.Attributes.ToList();
                                        if (!ent.ProfileCustomerAttributes.Local
                                            .Any()) // Insert customer attr once -- for multiple accounts
                                        {
                                            foreach (var attr in profileCustomerAttrs)
                                            {
                                                var pcattr = new ProfileCustomerAttribute
                                                {
                                                    ClientID = clientId,
                                                    CustomerID = request.Customer.Id,
                                                    AttributeKey = attr.AttributeKey,
                                                    AttributeValue = attr.AttributeValue,
                                                    Source = attr.SourceKey,
                                                    EffectiveDate = SetupEffectiveDate(attr.EffectiveDate)
                                                };
                                                ent.Entry(pcattr).State = System.Data.Entity.EntityState.Added;
                                                ent.ProfileCustomerAttributes.Add(pcattr);
                                                attrCount += 1;
                                            }
                                        }
                                    }

                                    //ProfileAccountAttributes
                                    var firstOrDefault = request.Customer.Accounts.FirstOrDefault(x => x.Id == acct);
                                    if (firstOrDefault != null)
                                    {
                                        var profileAccountAttrs = firstOrDefault.Attributes.ToList();
                                        foreach (var attr in profileAccountAttrs)
                                        {
                                            var paattr = new ProfileAccountAttribute
                                            {
                                                ClientID = clientId,
                                                AccountID = acct,
                                                AttributeKey = attr.AttributeKey,
                                                AttributeValue = attr.AttributeValue,
                                                Source = attr.SourceKey,
                                                EffectiveDate = SetupEffectiveDate(attr.EffectiveDate)
                                            };
                                            ent.Entry(paattr).State = System.Data.Entity.EntityState.Added;
                                            ent.ProfileAccountAttributes.Add(paattr);
                                            attrCount += 1;
                                        }
                                    }

                                    //ProfilePremiseAttributes
                                    var profileAccount = request.Customer.Accounts.FirstOrDefault(x => x.Id == acct);
                                    if (profileAccount != null)
                                    {
                                        var profilePremises = profileAccount.Premises.ToList();
                                        foreach (var premise in profilePremises)
                                        {
                                            var profilePremiseAttrs = premise.Attributes.ToList();
                                            foreach (var pattr in profilePremiseAttrs)
                                            {
                                                var ppattr = new ProfilePremiseAttribute
                                                {
                                                    ClientID = clientId,
                                                    AccountID = acct,
                                                    PremiseID = premise.Id,
                                                    AttributeKey = pattr.AttributeKey,
                                                    AttributeValue = pattr.AttributeValue,
                                                    Source = pattr.SourceKey,
                                                    EffectiveDate = SetupEffectiveDate(pattr.EffectiveDate)
                                                };
                                                ent.Entry(ppattr).State = System.Data.Entity.EntityState.Added;
                                                ent.ProfilePremiseAttributes.Add(ppattr);
                                                attrCount += 1;
                                            }
                                        }
                                    }
                                }
                                else
                                {
                                    //Update Content

                                    //ProfileCustomerAttributes
                                    var attributes = request.Customer.Attributes.Select(c => c.AttributeKey);
                                    var attributesInDb = ent.ProfileCustomerAttributes
                                        .Where(c => c.ClientID == clientId && c.CustomerID == request.Customer.Id &&
                                                    attributes.Contains(c.AttributeKey))
                                        .ToList(); // single DB query

                                    foreach (var attr in request.Customer.Attributes)
                                    {
                                        var attrInDb =
                                            attributesInDb.SingleOrDefault(
                                                c => c.AttributeKey ==
                                                     attr.AttributeKey); // runs in memory                           
                                        if (attrInDb != null)
                                        {
                                            if (attrInDb.AttributeValue != attr.AttributeValue ||
                                                attrInDb.Source != attr.SourceKey)
                                            {
                                                attrInDb.AttributeValue = attr.AttributeValue;
                                                attrInDb.EffectiveDate = SetupEffectiveDate(attr.EffectiveDate);
                                                attrInDb.Source = attr.SourceKey;
                                                ent.Entry(attrInDb).State = System.Data.Entity.EntityState.Modified;
                                                attrCount += 1;
                                            }
                                        }
                                        else
                                        {
                                            var pcattr = new ProfileCustomerAttribute
                                            {
                                                ClientID = clientId,
                                                CustomerID = request.Customer.Id,
                                                AttributeKey = attr.AttributeKey,
                                                AttributeValue = attr.AttributeValue,
                                                Source = attr.SourceKey,
                                                EffectiveDate = SetupEffectiveDate(attr.EffectiveDate)
                                            };
                                            ent.Entry(pcattr).State = System.Data.Entity.EntityState.Added;
                                            ent.ProfileCustomerAttributes.Add(pcattr);
                                            attrCount += 1;
                                        }
                                    }

                                    //ProfileAccountAttributes
                                    var firstOrDefault = request.Customer.Accounts.FirstOrDefault(x => x.Id == acct);
                                    if (firstOrDefault != null)
                                    {
                                        var accountAttributes = firstOrDefault.Attributes.Select(c => c.AttributeKey);
                                        var acountattributesInDb =
                                            ent.ProfileAccountAttributes
                                                .Where(c => c.ClientID == clientId && c.AccountID == acct &&
                                                            accountAttributes.Contains(c.AttributeKey))
                                                .ToList(); // single DB query
                                        foreach (var acctAttr in firstOrDefault.Attributes)
                                        {
                                            var attrInDb =
                                                acountattributesInDb.SingleOrDefault(
                                                    c => c.AttributeKey == acctAttr.AttributeKey); // runs in memory
                                            if (attrInDb != null)
                                            {
                                                if (attrInDb.AttributeValue != acctAttr.AttributeValue ||
                                                    attrInDb.Source != acctAttr.SourceKey)
                                                {
                                                    attrInDb.AttributeValue = acctAttr.AttributeValue;
                                                    attrInDb.EffectiveDate = SetupEffectiveDate(acctAttr.EffectiveDate);
                                                    attrInDb.Source = acctAttr.SourceKey;
                                                    ent.Entry(attrInDb).State = System.Data.Entity.EntityState.Modified;
                                                    attrCount += 1;
                                                }
                                            }
                                            else
                                            {
                                                var paattr = new ProfileAccountAttribute
                                                {
                                                    ClientID = clientId,
                                                    AccountID = acct,
                                                    AttributeKey = acctAttr.AttributeKey,
                                                    AttributeValue = acctAttr.AttributeValue,
                                                    Source = acctAttr.SourceKey,
                                                    EffectiveDate = SetupEffectiveDate(acctAttr.EffectiveDate)
                                                };
                                                ent.Entry(paattr).State = System.Data.Entity.EntityState.Added;
                                                ent.ProfileAccountAttributes.Add(paattr);
                                                attrCount += 1;
                                            }
                                        }
                                    }

                                    //ProfilePremiseAttributes
                                    var profileAccount = request.Customer.Accounts.FirstOrDefault(x => x.Id == acct);
                                    if (profileAccount != null)
                                    {
                                        var profilePremises = profileAccount.Premises.ToList();
                                        foreach (var premise in profilePremises)
                                        {
                                            var profilePremise =
                                                profileAccount.Premises.FirstOrDefault(x => x.Id == premise.Id);
                                            if (profilePremise != null)
                                            {
                                                var premiseAttributes =
                                                    profilePremise.Attributes.Select(c => c.AttributeKey);
                                                var premiseattributesInDb =
                                                    ent.ProfilePremiseAttributes
                                                        .Where(
                                                            c => c.ClientID == clientId && c.AccountID == acct &&
                                                                 c.PremiseID == premise.Id &&
                                                                 premiseAttributes.Contains(c.AttributeKey))
                                                        .ToList(); // single DB query
                                                foreach (var premAttr in profilePremise.Attributes)
                                                {
                                                    var attrInDb =
                                                        premiseattributesInDb.SingleOrDefault(
                                                            c => c.AttributeKey ==
                                                                 premAttr.AttributeKey); // runs in memory    

                                                    if (attrInDb != null)
                                                    {
                                                        if (attrInDb.AttributeValue != premAttr.AttributeValue ||
                                                            attrInDb.Source != premAttr.SourceKey)
                                                        {
                                                            attrInDb.AttributeValue = premAttr.AttributeValue;
                                                            attrInDb.EffectiveDate =
                                                                SetupEffectiveDate(premAttr.EffectiveDate);
                                                            attrInDb.Source = premAttr.SourceKey;
                                                            ent.Entry(attrInDb).State = System.Data.Entity.EntityState
                                                                .Modified;
                                                            attrCount += 1;
                                                        }
                                                    }
                                                    else
                                                    {
                                                        var ppattr = new ProfilePremiseAttribute
                                                        {
                                                            ClientID = clientId,
                                                            AccountID = acct,
                                                            PremiseID = premise.Id,
                                                            AttributeKey = premAttr.AttributeKey,
                                                            AttributeValue = premAttr.AttributeValue,
                                                            Source = premAttr.SourceKey,
                                                            EffectiveDate = SetupEffectiveDate(premAttr.EffectiveDate)
                                                        };
                                                        ent.Entry(ppattr).State = System.Data.Entity.EntityState.Added;
                                                        ent.ProfilePremiseAttributes.Add(ppattr);
                                                        attrCount += 1;
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }

                                if (attrCount >= 50)
                                {
                                    attrCount = 0;
                                    ent.SaveChanges();
                                }

                            }
                            ent.SaveChanges();
                            dbContextTransaction.Commit();
                        }
                        catch (Exception)
                        {
                            dbContextTransaction.Rollback();
                        }
                    }
                }
                transactionScope.Complete();
            }

            return profResponse;
        }

        private DateTime SetupEffectiveDate(DateTime? effectiveDate)
        {
            if (effectiveDate == null) { return DateTime.Now.ToUniversalTime(); }

            if (effectiveDate == DateTime.MinValue)
            { return DateTime.Now.ToUniversalTime(); }
            else { return Convert.ToDateTime(effectiveDate); }
        }

        private static void GetNewCustomerFromBi(int clientId, ProfileRequest request)
        {
            List<string> accounts;
            List<PremiseAttributes> pa = null;

            //declare the transaction options            //set it to read uncommited
            var transactionOptions =
                new System.Transactions.TransactionOptions
                {
                    IsolationLevel = System.Transactions.IsolationLevel.ReadUncommitted
                };


            using (var entDw = new InsightsDWEntities(ConnStringBuilder.GetEFConnectionStringInsightsDW(clientId)))
            {
                entDw.Configuration.AutoDetectChangesEnabled = false;
                entDw.Configuration.ValidateOnSaveEnabled = false;

                entDw.Database.Initialize(force: false);

                var cmd = entDw.Database.Connection.CreateCommand();
                cmd.CommandText = "[dbo].[GetProfilePremiseAttributes]";
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.Add(new SqlParameter("@ClientID", typeof(int)) { Value = clientId });
                cmd.Parameters.Add(new SqlParameter("@CustomerID", typeof(string)) { Value = request.CustomerId });

                try
                {
                    entDw.Database.Connection.Open();
                    var reader = cmd.ExecuteReader();

                    // Read the first result set 
                    accounts = ((IObjectContextAdapter)entDw).ObjectContext.Translate<string>(reader).ToList();

                    // Move to second result set 
                    reader.NextResult();
                    pa = ((IObjectContextAdapter)entDw).ObjectContext.Translate<PremiseAttributes>(reader).ToList();
                }
                catch (Exception)
                {
                    accounts = null;
                }
                finally
                {
                    entDw.Database.Connection.Close();
                }
            }


            if (accounts == null) return;
            try
            {
                using (var transScope =
                    new System.Transactions.TransactionScope(System.Transactions.TransactionScopeOption.Required,
                        transactionOptions))
                {
                    using (var ent = new InsightsEntities())
                    {
                        ent.Database.Connection.Open();
                        //using (var dbContextTransaction = ent.Database.BeginTransaction())
                        //{

                        ent.Configuration.AutoDetectChangesEnabled = false;
                        ent.Configuration.ValidateOnSaveEnabled = false;

                        var attrCount = 0;
                        //Check each acct if it exists                        

                        foreach (var acct in accounts)
                        {
                            var profCustAccts = ent.ProfileCustomerAccounts.Where(
                                pca => pca.ClientID == clientId && pca.CustomerID == request.CustomerId &&
                                       pca.AccountID == acct).ToList();


                            if (profCustAccts.Count == 0)
                            {
                                //Save Content                        

                                //ProfileCustomerAccount
                                var pca = new ProfileCustomerAccount
                                {
                                    ClientID = clientId,
                                    CustomerID = request.CustomerId,
                                    AccountID = acct
                                };

                                ent.Entry(pca).State = System.Data.Entity.EntityState.Added;
                                ent.ProfileCustomerAccounts.Add(pca);

                                //ProfilePremiseAttributes
                                var paByAcct = pa.Where(x => x.AccountId == acct).ToList();
                                foreach (var pattr in paByAcct)
                                {
                                    var ppattr = new ProfilePremiseAttribute
                                    {
                                        ClientID = clientId,
                                        AccountID = acct,
                                        PremiseID = pattr.PremiseId,
                                        AttributeKey = pattr.AttributeKey,
                                        AttributeValue = pattr.Value,
                                        Source = pattr.Source,
                                        EffectiveDate = pattr.EffectiveDate
                                    };

                                    ent.Entry(ppattr).State = System.Data.Entity.EntityState.Added;
                                    ent.ProfilePremiseAttributes.Add(ppattr);
                                    attrCount += 1;
                                }
                            }
                            if (attrCount >= 50)
                            {
                                attrCount = 0;
                                ent.SaveChanges();
                            }
                        }
                        ent.SaveChanges();
                        //dbContextTransaction.Commit();

                        //}
                        ent.Database.Connection.Close();
                    }
                    transScope.Complete();

                }
            }
            catch (Exception)
            {
                //dbContextTransaction.Rollback();
            }
        }
    }

    public class PremiseAttributes
    {
        public string ClientId { get; set; }
        public string AccountId { get; set; }
        public string PremiseId { get; set; }
        public string AttributeKey { get; set; }
        public string Value { get; set; }
        public string Source { get; set; }
        public DateTime EffectiveDate { get; set; }
    }
}
