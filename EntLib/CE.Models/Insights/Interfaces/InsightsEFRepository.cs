﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.Entity.Infrastructure;
using System.Data.SqlClient;
using System.Linq;
using AutoMapper;
using CE.Models.Insights;
using CE.Models.Insights.EF;
using CE.Models.Insights.Types;
using CE.Models.Utils;
using LinqKit;

namespace CE.Models
{
    /// <summary>
    /// InsightsEFRepository
    /// </summary>
    public partial class InsightsEfRepository : IInsightsEFRepository, IDisposable
    {
        private InsightsEntities db = new InsightsEntities();

        private const string GreenButtonLinkPrefix = "/Datacustodian/espi/1_1/resource";

        /// <summary>
        /// default
        /// </summary>
        public InsightsEfRepository()
        {
            InitializeAutoMapper();
        }
        /// <summary>
        /// Testability: pass in test db, ends dependency on database for unit tests
        /// </summary>
        /// <param name="context">test db context used</param>
        public InsightsEfRepository(InsightsEntities context)
        {
            db = context;
            InitializeAutoMapper();
        }
        public EndpointTrackingResponse GetEndpointTracking(int clientId, EndpointTrackingRequest request)
        {
            var eptResponse = new EndpointTrackingResponse();
            //declare the transaction options
            var transactionOptions = new System.Transactions.TransactionOptions();
            //set it to read uncommited
            transactionOptions.IsolationLevel = System.Transactions.IsolationLevel.ReadUncommitted;
            //create the transaction scope, passing our options in            
            using (var transactionScope = new System.Transactions.TransactionScope(System.Transactions.TransactionScopeOption.Required, transactionOptions))
            {
                var predicate = PredicateBuilder.True<EndpointTracking>();
                predicate = predicate.And(m => m.ClientID == clientId);
                predicate = predicate.And(m => (m.LogDate >= request.StartDate && m.LogDate <= request.EndDate && m.IncludeInd == true));
                if (request.UserId > 0)
                {
                    predicate = predicate.And(m => m.UserID == request.UserId || m.UserID == -1);
                }
                if (!string.IsNullOrEmpty(request.Verb))
                {
                    predicate = predicate.And(m => m.Verb == request.Verb);
                }
                if (request.EndpointId > 0)
                {
                    predicate = predicate.And(m => m.EndpointID == request.EndpointId || request.EndpointId == 0);
                }
                if (!string.IsNullOrEmpty(request.MessageId))
                {
                    predicate = predicate.And(m => m.XCEMessageId == request.MessageId);
                }

                var endpoints = (from x in db.EndpointTrackings.AsExpandable().Where(predicate)
                                 group x by x.ResourceName into g
                                 orderby g.Key
                                 select new
                                 {
                                     ResourceName = g.Key,
                                     Verbs = from x in g
                                             group x by x.Verb into gg
                                             select new
                                             {
                                                 Name = gg.Key,
                                                 Users = from x in gg
                                                         group x by x.UserID into ggg
                                                         select new
                                                         {
                                                             Id = ggg.Key,
                                                             ThirdPartyClientId = from x in ggg
                                                                 group x by x.ThirdPartyClientId into gggg
                                                                 select new {gggg.Key },
                                                             Count = ggg.Count()
                                                         }
                                             },
                                     Count = g.Count()
                                 }).ToList();


                var endpointList = new List<EndpointTrackDetails>();
                var totalCount = 0;
                foreach (var e in endpoints)
                {
                    var epTracking = new EndpointTrackDetails();
                    epTracking.EndpointName = e.ResourceName;
                    var verbList = new List<EndpointVerb>();
                    foreach (var v in e.Verbs)
                    {
                        var verb = new EndpointVerb();
                        verb.Name = v.Name;
                        var userList = new List<EndpointUser>();
                        foreach (var u in v.Users)
                        {
                            if (u.Id != -1)
                            {
                                var user = new EndpointUser();
                                user.UserId = Convert.ToInt32(u.Id);
                                user.UserName =
                                    db.Users.Where(x => x.UserID == user.UserId || x.UserID == -1)
                                        .Select(x => x.ActorName)
                                        .First();
                                var countByUser = u.Count;
                                user.Total = countByUser;
                                totalCount += countByUser;
                                userList.Add(user);
                            }
                            else
                            {
                                foreach (var t in u.ThirdPartyClientId)
                                {
                                    var user = new EndpointUser();
                                    user.UserId = Convert.ToInt32(u.Id);
                                    user.UserName = t.Key;
                                    var countByUser = u.Count;
                                    user.Total = countByUser;
                                    totalCount += countByUser;
                                    userList.Add(user);
                                }
                            }
                        }
                        verb.Users = userList;
                        verbList.Add(verb);
                    }
                    epTracking.Verbs = verbList;
                    epTracking.Total = e.Count;
                    endpointList.Add(epTracking);
                }

                eptResponse.ClientId = clientId;
                eptResponse.ClientName = db.Clients.Where(x => x.ClientID == clientId).Select(x => x.Name).First();
                eptResponse.StartDate = request.StartDate;
                eptResponse.EndDate = request.EndDate;
                eptResponse.IncludeDetails = request.IncludeDetails;
                eptResponse.UserId = request.UserId.ToString();
                eptResponse.Verb = request.Verb;
                eptResponse.EndpointId = request.EndpointId;
                eptResponse.MessageId = request.MessageId;
                eptResponse.EndpointTotals = endpointList;
                eptResponse.TotalCalls = totalCount;

                if (request.IncludeDetails)
                {
                    List<EndpointTracking> endpointTracking;
                    List<EndpointTrackingDetails> endpointTrackingDetails = new List<EndpointTrackingDetails>();
                    var pageIndex = request.PageIndex;
                    var totalRecords = db.EndpointTrackings.AsExpandable().Where(predicate).Count();
                    var pageSize = 500;
                    if (pageIndex <= 0)
                        pageIndex = 1;
                    int skipRows = (pageIndex - 1) * pageSize;
                    bool desc = true;
                    if (desc)
                        endpointTracking = db.EndpointTrackings.AsExpandable().Where(predicate).OrderByDescending(x => x.LogDate).Skip(skipRows).Take(pageSize).ToList();
                    else
                        endpointTracking = db.EndpointTrackings.AsExpandable().Where(predicate).OrderBy(x => x.LogDate).Skip(skipRows).Take(pageSize).ToList();

                    Mapper.CreateMap<EndpointTracking, EndpointTrackingDetails>()
                        .ForMember(dest => dest.UserID,
                                    opt => opt.MapFrom(
                                        src => src.UserID == -1 ? src.ThirdPartyClientId : src.UserID.ToString()))
                        .ForAllUnmappedMembers(o => o.Ignore());
                    endpointTrackingDetails = Mapper.Map<List<EndpointTracking>, List<EndpointTrackingDetails>>(endpointTracking);
                    

                    eptResponse.TrackingDetails = endpointTrackingDetails;
                    eptResponse.PageIndex = pageIndex;
                    eptResponse.PageSize = pageSize;
                }
                transactionScope.Complete();
            }
            //EFAutoMapper();
            //GetPeerBenchmarks();
            return eptResponse;
        }

        /// <summary>
        /// Auto mapper
        /// </summary>
        public void EFAutoMapper()
        {
            using (var ent = new InsightsEntities())
            {
                // If using Code First we need to make sure the model is built before we open the connection 
                // This isn't required for models created with the EF Designer 
                ent.Database.Initialize(force: false);

                // Create a SQL command to execute the sproc 
                var cmd = ent.Database.Connection.CreateCommand();
                cmd.CommandText = "[dbo].[GetClientUserByAccessKey]";
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@CEAccessKeyID", typeof(string)) { Value = "7320D04481A949B8AB4BB04CDD1A307E" });
                cmd.Parameters.Add(new SqlParameter("@EnvID", typeof(int)) { Value = 3 });

                try
                {
                    ent.Database.Connection.Open();
                    var reader = cmd.ExecuteReader();

                    // Read the first result set 
                    Data b = ((IObjectContextAdapter)ent).ObjectContext.Translate<Data>(reader).FirstOrDefault();


                    // Move to second result set 
                    reader.NextResult();
                    List<DataEndPoint> p = ((IObjectContextAdapter)ent)
                        .ObjectContext
                        .Translate<DataEndPoint>(reader).ToList();
                    //.Translate<DataEndPoint>(reader, "P", MergeOption.AppendOnly);

                    foreach (var item in p)
                    {
                        var n = item.Name;
                    }

                    Mapper.CreateMap<Data, DataJoined>()
                    .ForMember(x => x.Dat, opt => opt.MapFrom(model => model))
                    .ForAllUnmappedMembers(o => o.Ignore());

                    Mapper.CreateMap<List<DataEndPoint>, DataJoined>()
                         .ForMember(dest => dest.DatList,
                                    opt => opt.MapFrom(
                                        src => Mapper.Map<List<DataEndPoint>, List<DataEndPoint>>(src)))
                                        .ForAllUnmappedMembers(o => o.Ignore());

                    var jj = EntityMapper.Map<DataJoined>(b, p);

                    //Product
                    Mapper.CreateMap<ProductModel, ProductDto>()
                        .ForAllUnmappedMembers(o => o.Ignore());

                    Mapper.CreateMap<List<ProductModel>, ProductListDto>()
                        .ForMember(dest => dest.Products,
                                   opt => opt.MapFrom(
                                       src => Mapper.Map<List<ProductModel>,
                                                         List<ProductDto>>(src)))
                                                         .ForAllUnmappedMembers(o => o.Ignore());

                    var products = new List<ProductModel>
                    {
                        new ProductModel
                            {
                                Id = 1,
                                Name = "StackOverflow Rocks",
                                SerialNumber = "1234"
                            },
                        new ProductModel
                            {
                                Id = 2,
                                Name = "I Also Rock",
                                SerialNumber = "4321"
                            }
                    };

                    var productsDto = Mapper.Map<List<ProductModel>, ProductListDto>(products);
                    //var dto = Mapper.Map<List<ProductModel>, ProductListDto>((List<ProductModel>)model);


                    //Multiple objects
                    PersonTasks _personTasks = new PersonTasks();

                    var personId = 23;

                    var person = _personTasks.GetPerson(personId);
                    var address = _personTasks.GetAddress(personId);
                    var comment = _personTasks.GetComment(personId);

                    Mapper.CreateMap<Person, PersonViewModel>()
                        .ForAllUnmappedMembers(o => o.Ignore());

                    Mapper.CreateMap<Address, PersonViewModel>()
                        .ForMember(x => x.Id, opt => opt.Ignore());

                    Mapper.CreateMap<Comment, PersonViewModel>().ForMember(
                        x => x.Comment, opt => opt.MapFrom(source => source.Text))
                        .ForAllUnmappedMembers(o => o.Ignore());

                    var personViewModel = EntityMapper.Map<PersonViewModel>(person, address, comment);
                }
                finally
                {
                    ent.Database.Connection.Close();
                }
            }

        }


        /// <summary>
        /// Dispose
        /// </summary>
        public void Dispose()
        {

        }


    }




    public class ProductModel
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public string SerialNumber { get; set; }
    }

    public class ProductDto
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public string SerialNumber { get; set; }
    }

    public class ProductListDto : List<ProductDto>
    {
        public List<ProductDto> Products;

        public ProductListDto()
        {
            Products = new List<ProductDto>();
        }
    }


    public class Data
    {
        public int ClientID { get; set; }
        public int UserID { get; set; }
        public int RateCompanyID { get; set; }
        public string ActorName { get; set; }
        public string CEAccessKeyID { get; set; }
        public string CESecretAccessKey { get; set; }
        public string BasicKey { get; set; }
        public byte AuthType { get; set; }
    }

    public class DataEndPoint
    {
        public int EndpointID { get; set; }
        public string Name { get; set; }
        public string ShortName { get; set; }
    }

    public class DataJoined
    {
        public Data Dat { get; set; }
        public List<DataEndPoint> DatList { get; set; }
    }


    public class Address
    {
        public int Id { get; set; }
        public string AddressLine1 { get; set; }
        public string AddressLine2 { get; set; }
        public string Country { get; set; }
    }

    public class Comment
    {
        public string Text { get; set; }
        public DateTime Created { get; set; }
    }

    public class Person
    {
        public int Id { get; set; }
        public string Firstname { get; set; }
        public string Surname { get; set; }
    }

    public class PersonTasks
    {
        public Person GetPerson(int personId)
        {
            return new Person { Id = personId, Firstname = "John", Surname = "Doe" };
        }

        public Address GetAddress(int personId)
        {
            return new Address { Id = 325, AddressLine1 = "101 The Street", AddressLine2 = "The Town", Country = "UK" };
        }

        public Comment GetComment(int personId)
        {
            return new Comment { Text = "Some comment", Created = DateTime.Now };
        }
    }

    public class PersonViewModel
    {
        public int Id { get; set; }

        [DisplayName("Firstname")]
        public string Firstname { get; set; }

        [DisplayName("Surname")]
        public string Surname { get; set; }

        [DisplayName("Address Line 1")]
        public string AddressLine1 { get; set; }

        [DisplayName("Address Line 2")]
        public string AddressLine2 { get; set; }

        [DisplayName("Country Of Residence")]
        public string Country { get; set; }

        [DisplayName("Admin Comment")]
        public string Comment { get; set; }
    }

}
