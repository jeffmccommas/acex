﻿using AutoMapper;
using CE.Models.Insights;
using CE.Models.Insights.EF;
using CE.Models.Utils;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity.Infrastructure;
using System.Data.SqlClient;
using System.Linq;

// ReSharper disable once CheckNamespace
namespace CE.Models
{
    public partial class InsightsEfRepository
    {
        /// <summary>
        /// GetBills
        /// </summary>
        /// <param name="clientId"></param>
        /// <param name="request"></param>
        /// <returns></returns>
        public BillResponse GetBills(int clientId, BillRequest request)
        {
            DateTime startTime = DateTime.Now;
            string miscInfo = string.Empty;
            var response = new BillResponse
            {
                ClientId = clientId,
                Customer = GetBillSummary(clientId, request)
            };
            var performanceMs = DateTime.Now.Subtract(startTime).TotalMilliseconds;
            miscInfo += $"GetBills.GetBillSummary {performanceMs} ms";

            if (response.Customer == null)
            {
                response.Message = "No data found";
            }

            response.MiscInfo = miscInfo;

            return response;
        }

        /// <summary>
        /// get weather info for bill
        /// used by Bill V2
        /// </summary>
        /// <param name="accounts"></param>
        /// <returns></returns>
        public List<ServiceDb> GetWeatherInfoForBill(List<BillAccount> accounts)
        {
            //Create TVP from Services to get the weather data updated in the service list
            var f = new EfDataFactory();
            var table = f.CreateAccountBillAmountsTable();
            foreach (var account in accounts)
            {
                foreach(var bill in account.Bills)
                {
                    foreach(var premise in bill.Premises)
                    {
                        foreach(var service in premise.Service)
                        {
                            table.Rows.Add(account.Id, premise.Id, service.Id, bill.BillDate, service.BillStartDate,
                                service.BillEndDate, service.CommodityKey, service.UOMKey, service.BillDays, service.TotalServiceUse,
                                service.CostofUsage, service.AdditionalServiceCost, premise.Zip, service.AverageTemperature);
                        }
                    }
                }
            }

            var billWeatherDetails = GetDailyWeatherDetailForBills(table);
            return billWeatherDetails;
        }

        public IEnumerable<GetBillToDate_Result> GetBillToDate(int clientId, string accountId, string customerId, string serviceContractId = null, DateTime? startDate = null)
        {
            using (var ent = new InsightsDWEntities(ConnStringBuilder.GetEFConnectionStringInsightsDW(clientId)))
            {
                return ent.GetBillToDate(clientId, accountId, customerId, serviceContractId, startDate).ToList();
            }
        }

        /// <summary>
        /// helper
        /// </summary>
        private BillCustomer GetBillSummary(int clientid, BillRequest request)
        {
            using (var ent = new InsightsDWEntities(ConnStringBuilder.GetEFConnectionStringInsightsDW(clientid)))
            {
                ent.Configuration.AutoDetectChangesEnabled = false;
                ent.Configuration.ValidateOnSaveEnabled = false;

                ent.Database.Initialize(false);

                var cmd = ent.Database.Connection.CreateCommand();
                cmd.CommandText = "[dbo].[GetBillSummary]";
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.Add(new SqlParameter("@ClientID", typeof(int)) { Value = clientid });
                cmd.Parameters.Add(new SqlParameter("@CustomerID", typeof(string)) { Value = request.CustomerId });
                cmd.Parameters.Add(new SqlParameter("@StartDate", typeof(DateTime)) { Value = request.StartDate });
                cmd.Parameters.Add(new SqlParameter("@EndDate", typeof(DateTime)) { Value = request.EndDate });
                cmd.Parameters.Add(new SqlParameter("@Count", typeof(int)) { Value = (request.Count == 0 ? 1 : request.Count) });

                if (!string.IsNullOrWhiteSpace(request.AccountId))
                {
                    cmd.Parameters.Add(new SqlParameter("@AccountID", typeof(string)) { Value = request.AccountId });
                }

                try
                {
                    ent.Database.Connection.Open();
                    var reader = cmd.ExecuteReader();
                    var bm = new BillMerge();

                    // Read the first result set -- Customer
                    var bc = ((IObjectContextAdapter)ent).ObjectContext.Translate<BillCustomer>(reader).FirstOrDefault();
                    bm.Customer = bc;

                    // Move to second result set -- Account
                    reader.NextResult();
                    var ba = ((IObjectContextAdapter)ent).ObjectContext.Translate<BillAccount>(reader).ToList();
                    bm.Accounts = ba;

                    // Move to third result set -- Bill
                    reader.NextResult();
                    var b = ((IObjectContextAdapter)ent).ObjectContext.Translate<BillDb>(reader).ToList();
                    bm.Bills = b;

                    // Move to forth result set -- Premise
                    reader.NextResult();
                    var p = ((IObjectContextAdapter)ent).ObjectContext.Translate<PremiseDb>(reader).ToList();
                    bm.Premises = p;

                    // Move to fifth result set -- Service
                    reader.NextResult();
                    var s = ((IObjectContextAdapter)ent).ObjectContext.Translate<ServiceDb>(reader).ToList();

                    //Create TVP from Services to get the weather data updated in the service list
                    var f = new EfDataFactory();
                    var table = f.CreateAccountBillAmountsTable();
                    foreach (var li in s)
                    {
                        table.Rows.Add(li.AccountId, li.PremiseId, li.ServiceId, li.BillDate, li.BillStartDate,
                                        li.BillEndDate, li.CommodityKey, li.UomKey, li.BillDays, li.TotalServiceUse,
                                        li.CostofUsage, li.AdditionalServiceCost, li.ZipCode, li.AvgTemp, li.RateClass, li.ServiceCostDetailsKey);
                    }
                    var s2 = GetDailyWeatherDetailForBills(table);
                    bm.Services = s2;

                    // Move to sixth result set - Service Cost Detail
                    reader.NextResult();
                    var scd = ((IObjectContextAdapter)ent).ObjectContext.Translate<ServiceCostDetailDb>(reader).ToList();
                    bm.ServiceCostDetails = scd;

                    // Move to seventh result set - bill Cost Detail
                    reader.NextResult();
                    var bcd = ((IObjectContextAdapter)ent).ObjectContext.Translate<BillCostDetailDb>(reader).ToList();
                    bm.BillCostDetails = bcd;

                    Mapper.CreateMap<BillMerge, BillCustomer>().ConvertUsing(new BillConverter());
                    var c = new BillCustomer();
                    return bc == null ? null : Mapper.Map(bm, c);
                }
                finally
                {
                    ent.Database.Connection.Close();
                }
            }

        }

        /// <summary>
        /// Separate call with TVP to get avg temperature for each bill
        /// </summary>
        /// <param name="table"></param>
        /// <returns></returns>
        private List<ServiceDb> GetDailyWeatherDetailForBills(DataTable table)
        {
            List<ServiceDb> items = null;
            const string storedProcedureName = "[cm].[uspEMSelectDailyWeatherDetailForBills]";
            var dal = new Insights.DataAccess.DataAccessLayer();

            using (var connection = new SqlConnection(dal.GetConnectionMetaData()))
            {
                using (var cmd = new SqlCommand(storedProcedureName, connection))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@AccountBills", SqlDbType.Structured) { Value = table, TypeName = "[cm].[AccountBillAmountsTable]" });

                    connection.Open();

                    using (var reader = cmd.ExecuteReader())
                    {
                        if (reader.HasRows)
                        {
                            items = new List<ServiceDb>();

                            // User with client & user properties
                            while (reader.Read())
                            {
                                var accountid = reader["AccountId"] as string;
                                var premiseid = reader["PremiseId"] as string;
                                var serviceid = reader["ServiceId"] as string;
                                var billdate = reader["BillDate"] as string;
                                var billstartdate = reader["BillStartDate"] as string;
                                var billenddate = reader["BillEndDate"] as string;
                                var commoditykey = reader["CommodityKey"] as string;
                                var uomkey = reader["UOMKey"] as string;
                                int? billdays = reader["BillDays"] as int? ?? default(int);
                                decimal? totalserviceuse = reader["TotalServiceUse"] as decimal? ?? default(decimal);
                                decimal? costofusage = reader["CostofUsage"] as decimal? ?? default(decimal);
                                decimal? additionalservicecost = reader["AdditionalServiceCost"] as decimal? ?? default(decimal);
                                var zipcode = reader["ZipCode"] as string;
                                int? avgtemp = reader["AvgTemp"] as int? ?? default(int);
                                var rateClass = reader["RateClass"] as string;
                                Guid? serviceCostDetailsKey = reader["ServiceCostDetailsKey"] as Guid? ?? default(Guid);

                                var item = new ServiceDb
                                {
                                    AccountId = accountid,
                                    PremiseId = premiseid,
                                    ServiceId = serviceid,
                                    BillDate = billdate,
                                    BillStartDate = billstartdate,
                                    BillEndDate = billenddate,
                                    CommodityKey = commoditykey,
                                    UomKey = uomkey,
                                    BillDays = billdays.Value,
                                    TotalServiceUse = totalserviceuse.Value,
                                    CostofUsage = costofusage.Value,
                                    AdditionalServiceCost = additionalservicecost.Value,
                                    ZipCode = zipcode,
                                    AvgTemp = avgtemp.Value,
                                    RateClass = rateClass,
                                    ServiceCostDetailsKey = serviceCostDetailsKey.Value
                                };

                                items.Add(item);
                            }

                        }

                    }

                }

            }

            return items;
        }

    }


    public class BillConverter : ITypeConverter<BillMerge, BillCustomer>
    {
        public BillCustomer Convert(ResolutionContext context)
        {
            var sourceCollection = (BillMerge)context.SourceValue;
            Mapper.CreateMap<BillDb, Bill>().ForAllUnmappedMembers(o => o.Ignore());
            Mapper.CreateMap<PremiseDb, BillPremise>()
                .ForMember(dest => dest.Id, opt => opt.MapFrom(src => src.PremiseId))
                .ForAllUnmappedMembers(o => o.Ignore());
            Mapper.CreateMap<ServiceDb, BillService>()
                .ForMember(dest => dest.Id, opt => opt.MapFrom(src => src.ServiceId))
                .ForMember(dest => dest.AverageTemperature, opt => opt.MapFrom(src => src.AvgTemp))
                .ForAllUnmappedMembers(o => o.Ignore());
            Mapper.CreateMap<BillCostDetailDb, BillDetail>()
                .ForMember(dest => dest.Name, opt => opt.MapFrom(src => src.BillingCostDetailName))
                .ForMember(dest => dest.Value, opt => opt.MapFrom(src => src.BillingCostDetailValue.ToLower()))
                .ForAllUnmappedMembers(o => o.Ignore());

            Mapper.CreateMap<ServiceCostDetailDb, BillDetail>()
                .ForMember(dest => dest.Name, opt => opt.MapFrom(src => src.ServiceCostDetailName))
                .ForMember(dest => dest.Value, opt => opt.MapFrom(src => src.ServiceCostDetailValue.ToLower()))
                .ForAllUnmappedMembers(o => o.Ignore());

            Mapper.CreateMap<List<BillDetail>, BillService>()
                    .ForMember(dest => dest.CostDetails,
                               opt => opt.MapFrom(
                                   src => Mapper.Map<List<BillDetail>, List<BillDetail>>(src)))
                    .ForAllUnmappedMembers(o => o.Ignore());
            Mapper.CreateMap<List<BillService>, BillPremise>()
                    .ForMember(dest => dest.Service,
                               opt => opt.MapFrom(
                                   src => Mapper.Map<List<BillService>, List<BillService>>(src)))
                    .ForAllUnmappedMembers(o => o.Ignore());

            Mapper.CreateMap<List<BillPremise>, Bill>()
                   .ForMember(dest => dest.Premises,
                              opt => opt.MapFrom(
                                  src => Mapper.Map<List<BillPremise>, List<BillPremise>>(src)))
                   .ForAllUnmappedMembers(o => o.Ignore());
            Mapper.CreateMap<List<BillDetail>, Bill>()
                   .ForMember(dest => dest.CostDetails,
                              opt => opt.MapFrom(
                                  src => Mapper.Map<List<BillDetail>, List<BillDetail>>(src)))
                   .ForAllUnmappedMembers(o => o.Ignore());
            Mapper.CreateMap<List<Bill>, BillAccount>()
                   .ForMember(dest => dest.Bills,
                              opt => opt.MapFrom(
                                  src => Mapper.Map<List<Bill>, List<Bill>>(src)))
                   .ForAllUnmappedMembers(o => o.Ignore());

            var cust = EntityMapper.Map<BillCustomer>(sourceCollection.Customer);

            var accounts = sourceCollection.Accounts.Select(x => x.Id).ToList();
            var acctList = new List<BillAccount>();
            foreach (var a in accounts)
            {
                var acct = new BillAccount();
                var billsList = new List<Bill>();
                acct.Id = a;
                var bills = sourceCollection.Bills.Where(x => x.AccountId == a).ToList();

                foreach (var b in bills)
                {
                    // Bill Cost Details
                    var billCostDetails =
                        sourceCollection.BillCostDetails.Where(bcd => b.BillDate == bcd.BillDate && b.AccountId == bcd.AccountId).ToList();
                    var bcds = billCostDetails.Select(billCostDetailDb => EntityMapper.Map<BillDetail>(billCostDetailDb)).ToList();


                    var p = new List<BillPremise>();
                    var premises = sourceCollection.Premises.Where(x => x.AccountId == a && x.PremiseId == b.PremiseId).ToList();

                    foreach (var r in premises)
                    {
                        var s = sourceCollection.Services.Where(x => x.AccountId == a && x.PremiseId == r.PremiseId && x.BillDate == b.BillDate).ToList();

                        var bs = (from serviceDb in s
                                  let serviceCostDetails =
                                      sourceCollection.ServiceCostDetails.Where(
                                          scd =>
                                              scd.BillDate == serviceDb.BillDate &&
                                              scd.ServiceId == serviceDb.ServiceId &&
                                              scd.CommodityKey == serviceDb.CommodityKey &&
                                              scd.PremiseId == serviceDb.PremiseId &&
                                              scd.AccountId == serviceDb.AccountId &&
                                              scd.ServiceCostDetailsKey == serviceDb.ServiceCostDetailsKey)
                                  let scds =
                                      serviceCostDetails.Select(
                                          serviceCostDetailDb => EntityMapper.Map<BillDetail>(serviceCostDetailDb))
                                          .ToList()
                                  select EntityMapper.Map<BillService>(scds, serviceDb)).ToList();

                        p.Add(EntityMapper.Map<BillPremise>(bs, r));
                    }
                    billsList.Add(EntityMapper.Map<Bill>(p, b, bcds));
                }
                acct.Bills = billsList;
                acctList.Add(acct);
            }
            cust.Accounts = acctList;
            return cust;
        }
    }

    public class BillMerge
    {
        public BillCustomer Customer { get; set; }
        public List<BillAccount> Accounts { get; set; }
        public List<BillDb> Bills { get; set; }
        public List<PremiseDb> Premises { get; set; }
        public List<ServiceDb> Services { get; set; }
        public List<ServiceCostDetailDb> ServiceCostDetails { get; set; }
        public List<BillCostDetailDb> BillCostDetails { get; set; }
    }

    public class BillDb
    {
        public string AccountId { get; set; }
        public string PremiseId { get; set; }
        public string ServiceId { get; set; }
        public string BillDate { get; set; }
        public decimal TotalAmount { get; set; }
        public string BillFrequency { get; set; }
        public decimal AdditionalBillCost { get; set; }
        public string BillDueDate { get; set; }
        public long RowIdentifier { get; set; }
        public long RowIndex { get; set; }
    }

    public class PremiseDb
    {
        public string AccountId { get; set; }
        public string PremiseId { get; set; }
        public string Addr1 { get; set; }
        public string Addr2 { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Zip { get; set; }
    }

    public class ServiceDb
    {
        public string AccountId { get; set; }
        public string PremiseId { get; set; }
        public string ServiceId { get; set; }
        public string BillDate { get; set; }
        public string BillStartDate { get; set; }
        public string BillEndDate { get; set; }
        public string CommodityKey { get; set; }
        public string UomKey { get; set; }
        public int BillDays { get; set; }
        public decimal TotalServiceUse { get; set; }
        public decimal CostofUsage { get; set; }
        public decimal AdditionalServiceCost { get; set; }
        public string ZipCode { get; set; }
        public int AvgTemp { get; set; }
        public string RateClass { get; set; }
        public Guid? ServiceCostDetailsKey { get; set; }
    }
    public class ServiceCostDetailDb
    {
        public string AccountId { get; set; }
        public string PremiseId { get; set; }
        public string ServiceId { get; set; }
        public string BillDate { get; set; }
        public string CommodityKey { get; set; }
        public string ServiceCostDetailName { get; set; }
        public string ServiceCostDetailValue { get; set; }
        public Guid ServiceCostDetailsKey { get; set; }
    }

    public class BillCostDetailDb
    {
        public string AccountId { get; set; }
        public string BillDate { get; set; }
        public string BillingCostDetailName { get; set; }
        public string BillingCostDetailValue { get; set; }
    }

    public class EfDataFactory
    {

        /// <summary>
        /// Create a table for BillDisagg data with specific named columns that match TVP and Table. 
        /// </summary>
        /// <returns></returns>
        public DataTable CreateAccountBillAmountsTable()
        {
            var table = new DataTable();

            table.Columns.Add("AccountId", typeof(string));
            table.Columns.Add("PremiseId", typeof(string));
            table.Columns.Add("ServiceId", typeof(string));
            table.Columns.Add("BillDate", typeof(string));
            table.Columns.Add("BillStartDate", typeof(string));
            table.Columns.Add("BillEndDate", typeof(string));
            table.Columns.Add("CommodityKey", typeof(string));
            table.Columns.Add("UOMKey", typeof(string));
            table.Columns.Add("BillDays", typeof(int));
            table.Columns.Add("TotalServiceUse", typeof(decimal));
            table.Columns.Add("CostofUsage", typeof(decimal));
            table.Columns.Add("AdditionalServiceCost", typeof(decimal));
            table.Columns.Add("ZipCode", typeof(string));
            table.Columns.Add("AvgTemp", typeof(int));
            table.Columns.Add("RateClass", typeof(string));
            table.Columns.Add("ServiceCostDetailsKey", typeof(Guid));

            return table;
        }

    }



}
