﻿using System;
using System.Collections.Generic;
using System.Linq;
using CE.Models.Insights;
using CE.Models.Insights.EF;
using AutoMapper;
using CE.Models.Utils;
using LinqKit;
using FileDetail = CE.Models.Insights.FileDetail;

//using CE.a;

namespace CE.Models
{
    public partial class InsightsEfRepository : IInsightsEFRepository, IDisposable
    {
        /// <summary>
        ///  Get file metadata details
        /// </summary>
        /// <param name="clientId"></param>
        /// <param name="fileId"></param>
        /// <returns></returns>
        public FileDetail GetFile(int clientId, int fileId)
        {
            FileDetail outputDetail;
            Mapper.CreateMap<Insights.EF.FileDetail, FileDetail>()
                .ForAllUnmappedMembers(o => o.Ignore());
            var transactionOptions = new System.Transactions.TransactionOptions
            {
                IsolationLevel = System.Transactions.IsolationLevel.ReadUncommitted
            };

            using (var transactionScope = new System.Transactions.TransactionScope(System.Transactions.TransactionScopeOption.Required, transactionOptions))
            {
              
                using (var ent = new InsightsEntities())
                {
                    //int fileid = Convert.ToInt32(fileId);
                    var fileDetail = ent.FileDetail.SingleOrDefault(a => a.ID == fileId && a.ClientId == clientId);

                    outputDetail = Mapper.Map<Insights.EF.FileDetail,FileDetail>(fileDetail);
                }
                transactionScope.Complete();
            }
            return outputDetail;
        }

        
        /// <summary>
        /// Delete file metadata from Database
        /// </summary>
        /// <param name="fileid"></param>
        public void DeleteFile(int fileid)
        {
            var transactionOptions = new System.Transactions.TransactionOptions();
            //set it to read uncommited
            transactionOptions.IsolationLevel = System.Transactions.IsolationLevel.ReadUncommitted;
            //create the transaction scope, passing our options in 
            
            using (var transactionScope = new System.Transactions.TransactionScope(System.Transactions.TransactionScopeOption.Required, transactionOptions))
            {

                using (var ent = new InsightsEntities())
                {
                    using (var dbContextTransaction = ent.Database.BeginTransaction())
                    {
                        try
                        {
                            var filedetail = new Insights.EF.FileDetail() { ID = fileid };
                            ent.FileDetail.Attach(filedetail);
                            ent.FileDetail.Remove(filedetail);
                            ent.SaveChanges();
                            dbContextTransaction.Commit();
                        }
                        catch 
                        {
                            dbContextTransaction.Rollback();
                        }
                    }
                }
                transactionScope.Complete();
            }
        }

        /// <summary>
        /// Save File Metadata details
        /// </summary>
        /// <param name="clientId"></param>
        /// <param name="request"></param>
        /// <returns></returns>
        public FilePostResponse PostFile(int clientId, FilePostRequest request)
        {
            var fileResponse = new FilePostResponse();

            // Create Database entry
            //declare the transaction options
            var transactionOptions = new System.Transactions.TransactionOptions();
            //set it to read uncommited
            transactionOptions.IsolationLevel = System.Transactions.IsolationLevel.ReadUncommitted;
            //create the transaction scope, passing our options in 

            using (var transactionScope = new System.Transactions.TransactionScope(System.Transactions.TransactionScopeOption.Required, transactionOptions))
            {

                using (var ent = new InsightsEntities())
                {
                    using (var dbContextTransaction = ent.Database.BeginTransaction())
                    {
                        try
                        {
                            var fileDetail = new Insights.EF.FileDetail
                            {
                                ClientId = clientId,
                                CustomerId = request.Filedetail.CustomerId,
                                AccountId = request.Filedetail.AccountId,
                                PremiseId = request.Filedetail.PremiseId,
                                UserId = request.Filedetail.UserId,
                                RoleId = request.Filedetail.RoleId,
                                Description = request.Filedetail.Description,
                                TypeId = request.Filedetail.TypeId,
                                Title = request.Filedetail.Title,
                                Name = request.Filedetail.Name,
                                FileSourceId = request.Filedetail.FileSourceId,
                                CreateDate = DateTime.Now.ToUniversalTime()
                            };

                            ent.FileDetail.Add(fileDetail);
                            ent.SaveChanges();
                            dbContextTransaction.Commit();
                            request.Filedetail.Id = fileDetail.ID;
                        }
                        catch
                        {
                            dbContextTransaction.Rollback();
                            transactionScope.Complete();
                            throw;
                        }
                    }
                }
                transactionScope.Complete();
            }
            return fileResponse;
        }


        /// <summary>
        /// Get files for Client,Customer,  Account and Premise
        /// </summary>
        /// <param name="clientId"></param>
        /// <param name="request"></param>
        /// <returns></returns>
        public List<FileDetail> GetFiles(int clientId, FilesRequest request)
        {
            List<FileDetail> outputDetail;
            Mapper.CreateMap<Insights.EF.FileDetail, FileDetail>()
                .ForAllUnmappedMembers(o => o.Ignore());
            var transactionOptions = new System.Transactions.TransactionOptions
            {
                IsolationLevel = System.Transactions.IsolationLevel.ReadUncommitted
            };

            var predicateFileDetails = PredicateBuilder.True<Insights.EF.FileDetail>();
            predicateFileDetails = predicateFileDetails.And(m => m.CustomerId == request.CustomerId && m.AccountId == request.AccountId && m.PremiseId == request.PremiseId );

            if (request.UserRole > 0 || request.UserId == null)
            {
                predicateFileDetails = predicateFileDetails.And(m => m.UserId == request.UserId || m.UserId ==null);
                predicateFileDetails = predicateFileDetails.And(m => m.RoleId == request.UserRole || m.RoleId == 0);
            }
            else
            {
                predicateFileDetails = predicateFileDetails.And(m => m.RoleId == 0);
                predicateFileDetails = predicateFileDetails.And(m => m.UserId == null);
            }

            using (var transactionScope = new System.Transactions.TransactionScope(System.Transactions.TransactionScopeOption.Required, transactionOptions))
            {

                using (var ent = new InsightsEntities())
                {
                    ent.Database.Connection.Open();
                    var fileList = (from d in ent.FileDetail.AsExpandable().Where(predicateFileDetails)
                                    select d).ToList();

                    outputDetail = Mapper.Map<List<Insights.EF.FileDetail>, List<FileDetail>>(fileList);
                }
                transactionScope.Complete();
            }
            return outputDetail;

        }


    }
}
