﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity.Infrastructure;
using System.Data.SqlClient;
using System.Linq;
using AutoMapper;
using CE.Models.Insights;
using CE.Models.Insights.EF;
using CE.Models.Utils;
using Action = CE.Models.Insights.Action;

namespace CE.Models
{
    /// <summary>
    /// InsightsEFRepository
    /// </summary>
    public partial class InsightsEfRepository
    {
        /// <summary>
        /// Get Actions from the repositiory
        /// </summary>
        /// <param name="clientId">Teh current ClientId</param>
        /// <param name="locale">The current locale</param>
        /// <param name="request">The action request</param>
        /// <returns>An action response</returns>
        public ActionResponse GetActions(int clientId, string locale, ActionRequest request)
        {
            var response = new ActionResponse
            {
                ClientId = clientId,
                Customer = GetRecommendedActions(clientId, request)
            };

            if (response.Customer == null)
            {
                response.Message = "No data found";
            }
            return response;
        }

        /// <summary>
        /// Get Recommended Actions
        /// </summary>
        private ActionCustomer GetRecommendedActions(int clientid, ActionRequest request)
        {
            using (var ent = new InsightsDWEntities(ConnStringBuilder.GetEFConnectionStringInsightsDW(clientid)))
            {
                ent.Configuration.AutoDetectChangesEnabled = false;
                ent.Configuration.ValidateOnSaveEnabled = false;

                ent.Database.Initialize(false);

                var cmd = ent.Database.Connection.CreateCommand();
                cmd.CommandText = "[dbo].[GetRecommendedActions]";
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.Add(new SqlParameter("@ClientID", typeof(int)) { Value = clientid });
                if (!string.IsNullOrWhiteSpace(request.CustomerId))
                {
                    cmd.Parameters.Add(new SqlParameter("@CustomerID", typeof(string)) { Value = request.CustomerId });
                }
                if (!string.IsNullOrWhiteSpace(request.AccountId))
                {
                    cmd.Parameters.Add(new SqlParameter("@AccountID", typeof(string)) { Value = request.AccountId });
                }
                if (!string.IsNullOrWhiteSpace(request.PremiseId))
                {
                    cmd.Parameters.Add(new SqlParameter("@PremiseID", typeof(string)) { Value = request.PremiseId });
                }
                if (!string.IsNullOrWhiteSpace(request.CommodityKeys))
                {
                    cmd.Parameters.Add(new SqlParameter("@CommodityKeys", typeof(string)) { Value = request.CommodityKeys });
                }
                if (!string.IsNullOrWhiteSpace(request.Types))
                {
                    cmd.Parameters.Add(new SqlParameter("@Types", typeof(string)) { Value = request.Types });
                }
                if (!string.IsNullOrWhiteSpace(request.EndUseKeys))
                {
                    cmd.Parameters.Add(new SqlParameter("@EndUseKeys", typeof(string)) { Value = request.EndUseKeys });
                }
                if (!string.IsNullOrWhiteSpace(request.ApplianceKeys))
                {
                    cmd.Parameters.Add(new SqlParameter("@ApplianceKeys", typeof(string)) { Value = request.ApplianceKeys });
                }
                try
                {
                    ent.Database.Connection.Open();
                    var reader = cmd.ExecuteReader();
                    var am = new ActionMerge();

                    // Read the first result set -- Actions
                    List<ActionDB> a = ((IObjectContextAdapter)ent).ObjectContext.Translate<ActionDB>(reader).ToList();
                    am.Actions = a;

                    if (a.Count == 0)
                    {
                        return null;
                    }

                    Mapper.CreateMap<ActionMerge, ActionCustomer>().ConvertUsing(new ActionConverter());
                    var c = new ActionCustomer();
                    return Mapper.Map(am, c);
                }
                finally
                {
                    ent.Database.Connection.Close();
                }
            }

        }
    }

    /// <summary>
    /// Action Converter class
    /// </summary>
    public class ActionConverter : ITypeConverter<ActionMerge, ActionCustomer>
    {
        /// <summary>
        /// Convert
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>
        public ActionCustomer Convert(ResolutionContext context)
        {
            var sourceCollection = (ActionMerge)context.SourceValue;
            var cust = new ActionCustomer();
            var accts = new List<ActionAccount>();

            Mapper.CreateMap<ActionDB, Action>().ForAllUnmappedMembers(o => o.Ignore());
            Mapper.CreateMap<MeasurementDB, ActionMeasurement>().ForAllUnmappedMembers(o => o.Ignore());

            Mapper.CreateMap<List<MeasurementDB>, Action>()
                     .ForMember(dest => dest.AdditionalMeasurements,
                                opt => opt.MapFrom(
                                    src => Mapper.Map<List<MeasurementDB>, List<ActionMeasurement>>(src)))
                                    .ForAllUnmappedMembers(o => o.Ignore());

            Mapper.CreateMap<MeasurementDB, ActionMeasurement>()
                     .ForMember(dest => dest.Type, opt => opt.MapFrom(src => src.MeasurementType))
                     .ForMember(dest => dest.Quantity, opt => opt.MapFrom(src => src.MeasurementQuantity))
                     .ForMember(dest => dest.Units, opt => opt.MapFrom(src => src.MeasurementKey))
                     .ForAllUnmappedMembers(o => o.Ignore());

            cust.Id = sourceCollection.Actions.First().CustomerId;
            var accounts = sourceCollection.Actions.Select(x => x.AccountId).Distinct();

            foreach (var a in accounts)
            {
                var premiseList = new List<ActionPremise>();
                var acct = new ActionAccount {Id = a};
                var premises = sourceCollection.Actions.Where(x => x.AccountId == a).Select(x => x.PremiseId).Distinct();

                foreach (var r in premises)
                {
                    var action = new List<Action>();
                    var p = sourceCollection.Actions.Where(x => x.PremiseId == r).ToList();

                    var premise = new ActionPremise {Id = p.First().PremiseId};

                    foreach (var source in p)
                    {
                        if (sourceCollection.Measurements != null)
                        {
                            var pd = sourceCollection.Measurements.Where(d => d.ClientActionSavingsID == source.ClientActionSavingsID).ToList();
                            action.Add(EntityMapper.Map<Action>(source, pd));
                        }
                        else
                        {
                            action.Add(EntityMapper.Map<Action>(source));
                        }
                    }
                    premise.Actions = action;

                    premiseList.Add(premise);
                }
                acct.Premises = premiseList;
                accts.Add(acct);
            }
            cust.Accounts = accts;
            return cust;
        }
    }

    /// <summary>
    /// ActionMerge class
    /// </summary>
    public class ActionMerge
    {
        public List<ActionDB> Actions { get; set; }
        public List<MeasurementDB> Measurements { get; set; }
    }

    /// <summary>
    /// ActionDb class
    /// </summary>
    public class ActionDB
    {
        public string CustomerId { get; set; }

        public string AccountId { get; set; }

        public string PremiseId { get; set; }

        public string PremiseKey { get; set; }

        public int ClientActionSavingsID { get; set; }

        public string Key { get; set; }

        public string CommodityKey { get; set; }

        public int Rank { get; set; }

        public decimal UpfrontCost { get; set; }

        public decimal AnnualCost { get; set; }

        public decimal CostVariancePercent { get; set; }

        public decimal AnnualSavingsEstimate { get; set; }

        public decimal AnnualUsageSavingsEstimate { get; set; }

        public string AnnualUsageSavingsUomKey { get; set; }

        public string SavingsEstimateQuality { get; set; }

        public decimal PayBackTime { get; set; }

        public decimal ROI { get; set; }

        public string Popularity { get; set; }
    }

    /// <summary>
    /// MeasureDb class
    /// </summary>
    public class MeasurementDB
    {
        public string PremiseKey { get; set; }

        public int ClientActionSavingsID { get; set; }

        public string Key { get; set; }

        public string MeasurementType { get; set; }

        public decimal MeasurementQuantity { get; set; }

        public string MeasurementKey { get; set; }
    }
}
