﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity.Infrastructure;
using System.Data.SqlClient;
using System.Linq;
using CE.Models.Insights;
using CE.Models.Insights.EF;
using LinqKit;

namespace CE.Models
{
    /// <summary>
    /// InsightsEFRepository
    /// </summary>
    public partial class InsightsEfRepository : IInsightsEFRepository, IDisposable
    {
        public ActionPlanResponse GetActionPlan(int clientId, ActionPlanRequest request)
        {
            var apResponse = new ActionPlanResponse();
            //declare the transaction options
            var transactionOptions = new System.Transactions.TransactionOptions();
            //set it to read uncommited
            transactionOptions.IsolationLevel = System.Transactions.IsolationLevel.ReadUncommitted;

            List<ActionCustomerAccount> actionCustomerAccounts;
            var accountList = new List<ActionPlanAccount>();

            var predicateCustomerAccount = PredicateBuilder.True<ActionCustomerAccount>();
            predicateCustomerAccount = predicateCustomerAccount.And(m => m.ClientID == clientId && m.CustomerID == request.CustomerId);
            if (!string.IsNullOrEmpty(request.AccountId))
            {
                predicateCustomerAccount = predicateCustomerAccount.And(m => m.AccountID == request.AccountId);
            }

            using (var transScopeAction = new System.Transactions.TransactionScope(System.Transactions.TransactionScopeOption.Required, transactionOptions))
            {
                using (var ent = new InsightsEntities())
                {
                    ent.Database.Connection.Open();
                    actionCustomerAccounts = (from pca in ent.ActionCustomerAccount.AsExpandable().Where(predicateCustomerAccount)
                                              select pca).ToList();
                }
                transScopeAction.Complete();
            }

            if (actionCustomerAccounts.Count == 0)
            {
                GetNewActionPlanCustomerFromBI(clientId, request);
            }

            //create the transaction scope, passing our options in            
            using (var transactionScopeAction = new System.Transactions.TransactionScope(System.Transactions.TransactionScopeOption.Required, transactionOptions))
            {
                using (var ent = new InsightsEntities())
                {
                    ent.Database.Connection.Open();
                    if (actionCustomerAccounts.Count == 0)
                    {
                        actionCustomerAccounts = (from pca in ent.ActionCustomerAccount.AsExpandable().Where(predicateCustomerAccount)
                                                  select pca).ToList();
                    }

                    foreach (var acct in actionCustomerAccounts)
                    {
                        //Account
                        var account = new ActionPlanAccount();
                        account.Id = acct.AccountID;

                        //premise
                        var predicatePremise = PredicateBuilder.True<ActionPremisePlan>();
                        predicatePremise = predicatePremise.And(m => m.ClientID == clientId && m.AccountID == acct.AccountID);
                        if (!string.IsNullOrEmpty(request.PremiseId))
                        {
                            predicatePremise = predicatePremise.And(m => m.PremiseID == request.PremiseId);
                        }

                        var actionPremises = (from x in ent.ActionPremisePlan.AsExpandable().Where(predicatePremise)
                                              group x by new { x.ClientID, x.AccountID, x.PremiseID } into g
                                              select g).ToList();

                        var premiseList = new List<ActionPlanPremise>();
                        foreach (var prem in actionPremises)
                        {
                            var predicate = PredicateBuilder.True<ActionPremisePlan>();
                            predicate = predicate.And(m => m.ClientID == clientId);
                            predicate = predicate.And(m => m.AccountID == acct.AccountID);
                            predicate = predicate.And(m => m.PremiseID == prem.Key.PremiseID);

                            var actionPremiseList = (from x in ent.ActionPremisePlan.AsExpandable().Where(predicate)
                                                     group x by new { x.ActionKey, x.ActionStatus, x.ActionStatusDate, x.SourceKey, x.AdditionalInfo, x.SubActionKey } into g
                                                     select g).ToList();

                            var premiseStatuses = new List<ActionStatus>();

                            foreach (var i in actionPremiseList)
                            {
                                ActionStatus a = new ActionStatus();
                                a.ActionKey = i.Key.ActionKey;
                                a.Status = i.Key.ActionStatus;
                                a.StatusDate = i.Key.ActionStatusDate;
                                a.SourceKey = i.Key.SourceKey;
                                a.SubActionKey = (i.Key.SubActionKey == string.Empty) ? null : i.Key.SubActionKey;

                                if (i.Key.AdditionalInfo != null)
                                {
                                    var aiPair = i.Key.AdditionalInfo.Split(';');
                                    var ail = new List<ActionAdditionalInfo>();
                                    foreach (var p in aiPair)
                                    {
                                        var ai = new ActionAdditionalInfo();
                                        var aiInfo = p.Split(':');
                                        if (p.Length > 0)
                                        {
                                            ai.Key = aiInfo[0];
                                            ai.Value = aiInfo[1];
                                            ail.Add(ai);
                                        }
                                    }
                                    a.AdditionalInfo = ail;
                                }
                                premiseStatuses.Add(a);
                            }

                            var premise = new ActionPlanPremise();
                            premise.Id = prem.Key.PremiseID;
                            premise.ActionStatuses = premiseStatuses;

                            premiseList.Add(premise);
                        }
                        account.Premises = premiseList;
                        accountList.Add(account);
                    }

                    //Customer
                    var customer = new ActionPlanCustomer();
                    customer.Id = request.CustomerId;
                    customer.Accounts = accountList;
                    apResponse.Customer = customer;
                    transactionScopeAction.Complete();
                }
            }
            apResponse.ClientId = clientId;
            return apResponse;
        }

        public ActionPlanPostResponse PostActionPlan(int clientId, ActionPlanPostRequest request)
        {
            var apResponse = new ActionPlanPostResponse();
            //declare the transaction options
            var transactionOptions = new System.Transactions.TransactionOptions();
            //set it to read uncommited
            transactionOptions.IsolationLevel = System.Transactions.IsolationLevel.ReadUncommitted;
            //create the transaction scope, passing our options in            
            using (var transactionScope = new System.Transactions.TransactionScope(System.Transactions.TransactionScopeOption.Required, transactionOptions))
            {
                using (var ent = new InsightsEntities())
                {
                    using (var dbContextTransaction = ent.Database.BeginTransaction())
                    {
                        try
                        {
                            ent.Configuration.AutoDetectChangesEnabled = false;
                            ent.Configuration.ValidateOnSaveEnabled = false;

                            var accts = request.Customer.Accounts.Select(x => x.Id).ToList();
                            DateTime createdDate = DateTime.Now.ToUniversalTime();

                            var attrCount = 0;

                            //Check each acct if it exists                        
                            foreach (var acct in accts)
                            {
                                List<ActionCustomerAccount> actionCustomerAccounts = (from aca in ent.ActionCustomerAccount
                                                                                      where aca.ClientID == clientId && aca.CustomerID == request.Customer.Id && aca.AccountID == acct
                                                                                      select aca).ToList();

                                if (actionCustomerAccounts.Count == 0)
                                {
                                    //Save Content                        

                                    //CustomerAccount
                                    ActionCustomerAccount aca = new ActionCustomerAccount
                                    {
                                        ClientID = clientId,
                                        CustomerID = request.Customer.Id,
                                        AccountID = acct
                                    };
                                    ent.Entry(aca).State = System.Data.Entity.EntityState.Added;
                                    ent.ActionCustomerAccount.Add(aca);

                                    //Premise
                                    var actionPremises = request.Customer.Accounts.Where(x => x.Id == acct).FirstOrDefault().Premises.ToList();
                                    foreach (var premise in actionPremises)
                                    {
                                        var actionPremiseStats = premise.ActionStatuses.ToList();
                                        foreach (var pstat in actionPremiseStats)
                                        {
                                            pstat.StatusDate = SetupStatusDate(pstat.StatusDate);

                                            var apstat = new ActionPremisePlan
                                            {
                                                ClientID = clientId,
                                                AccountID = acct,
                                                PremiseID = premise.Id,
                                                ActionKey = pstat.ActionKey,
                                                ActionStatus = pstat.Status.ToLower(),
                                                ActionStatusDate = pstat.StatusDate,
                                                CreateDate = createdDate,
                                                UpdateDate = createdDate,
                                                SourceKey = pstat.SourceKey,
                                                SubActionKey = (pstat.SubActionKey == null) ? string.Empty : pstat.SubActionKey
                                            };

                                            var aiString = string.Empty;
                                            if (pstat.AdditionalInfo != null)
                                            {
                                                foreach (var a in pstat.AdditionalInfo)
                                                {
                                                    if (aiString.Length == 0)
                                                    {
                                                        aiString += a.Key + ":" + a.Value;
                                                    }
                                                    else
                                                    {
                                                        aiString += ";" + a.Key + ":" + a.Value;
                                                    }
                                                }
                                                apstat.AdditionalInfo = aiString;
                                            }

                                            ent.Entry(apstat).State = System.Data.Entity.EntityState.Added;
                                            ent.ActionPremisePlan.Add(apstat);
                                            attrCount += 1;
                                        }
                                    }
                                }
                                else
                                {
                                    //Update Content

                                    //Premise
                                    var actionPremises = request.Customer.Accounts.Where(x => x.Id == acct).FirstOrDefault().Premises.ToList();
                                    foreach (var premise in actionPremises)
                                    {
                                        var actionStatuses = request.Customer.Accounts.Where(x => x.Id == acct).FirstOrDefault().Premises.Where(x => x.Id == premise.Id).FirstOrDefault().ActionStatuses.Select(c => c.ActionKey);
                                        var premiseattributesInDb = ent.ActionPremisePlan.Where(c => c.ClientID == clientId && c.AccountID == acct && c.PremiseID == premise.Id && actionStatuses.Contains(c.ActionKey)).ToList(); // single DB query
                                        foreach (var premStat in request.Customer.Accounts.Where(c => c.Id == acct).FirstOrDefault().Premises.Where(x => x.Id == premise.Id).FirstOrDefault().ActionStatuses)
                                        {
                                            foreach (var subAction in request.Customer.Accounts.Where(x => x.Id == acct).FirstOrDefault().Premises.Where(x => x.Id == premise.Id).FirstOrDefault().ActionStatuses.Where(x => x.ActionKey == premStat.ActionKey))
                                            {
                                                var sa = (subAction.SubActionKey == null) ? string.Empty : subAction.SubActionKey;
                                                var attrInDb = premiseattributesInDb.Where(c => c.ActionKey == premStat.ActionKey && c.SubActionKey == sa).SingleOrDefault(); // runs in memory    

                                                premStat.StatusDate = SetupStatusDate(premStat.StatusDate);

                                                if (attrInDb != null)
                                                {
                                                    attrInDb.ActionKey = premStat.ActionKey;
                                                    attrInDb.UpdateDate = createdDate;
                                                    attrInDb.ActionStatus = premStat.Status.ToLower();
                                                    attrInDb.ActionStatusDate = premStat.StatusDate;
                                                    attrInDb.SourceKey = premStat.SourceKey;

                                                    var aiString = string.Empty;
                                                    if (premStat.AdditionalInfo != null)
                                                    {
                                                        foreach (var a in premStat.AdditionalInfo)
                                                        {
                                                            if (aiString.Length == 0)
                                                            {
                                                                aiString += a.Key + ":" + a.Value;
                                                            }
                                                            else
                                                            {
                                                                aiString += ";" + a.Key + ":" + a.Value;
                                                            }
                                                        }
                                                        attrInDb.AdditionalInfo = aiString;
                                                    }
                                                    ent.Entry(attrInDb).State = System.Data.Entity.EntityState.Modified;
                                                    attrCount += 1;
                                                }
                                                else
                                                {
                                                    var ppStat = new ActionPremisePlan
                                                    {
                                                        ClientID = clientId,
                                                        AccountID = acct,
                                                        PremiseID = premise.Id,
                                                        ActionKey = premStat.ActionKey,
                                                        ActionStatus = premStat.Status.ToLower(),
                                                        ActionStatusDate = premStat.StatusDate,
                                                        CreateDate = createdDate,
                                                        UpdateDate = createdDate,
                                                        SourceKey = premStat.SourceKey,
                                                        SubActionKey = (premStat.SubActionKey == null) ? string.Empty : premStat.SubActionKey
                                                    };
                                                    var aiString = string.Empty;
                                                    if (premStat.AdditionalInfo != null)
                                                    {
                                                        foreach (var a in premStat.AdditionalInfo)
                                                        {
                                                            if (aiString.Length == 0)
                                                            {
                                                                aiString += a.Key + ":" + a.Value;
                                                            }
                                                            else
                                                            {
                                                                aiString += ";" + a.Key + ":" + a.Value;
                                                            }
                                                        }
                                                        ppStat.AdditionalInfo = aiString;
                                                    }
                                                    ent.Entry(ppStat).State = System.Data.Entity.EntityState.Added;
                                                    ent.ActionPremisePlan.Add(ppStat);
                                                    attrCount += 1;
                                                }
                                            }
                                        }
                                    }
                                }

                                if (attrCount >= 50)
                                {
                                    attrCount = 0;
                                    ent.SaveChanges();
                                }
                            }
                            ent.SaveChanges();
                            dbContextTransaction.Commit();
                        }
                        catch (Exception)
                        {
                            dbContextTransaction.Rollback();
                        }
                    }
                }
                transactionScope.Complete();
            }
            return apResponse;
        }

        private DateTime SetupStatusDate(DateTime statusDate)
        {
            if (statusDate == null) { return System.DateTime.Now.ToUniversalTime(); }

            if (statusDate == DateTime.MinValue)
            { return DateTime.Now.ToUniversalTime(); }
            else { return Convert.ToDateTime(statusDate); }
        }


        private void GetNewActionPlanCustomerFromBI(int clientId, ActionPlanRequest request)
        {
            List<string> accounts;
            List<ActionPlan> ap = null;

            //declare the transaction options
            var transactionOptions = new System.Transactions.TransactionOptions();
            //set it to read uncommited
            transactionOptions.IsolationLevel = System.Transactions.IsolationLevel.ReadUncommitted;
            DateTime createdDate = DateTime.Now.ToUniversalTime();

            using (var entDW = new InsightsDWEntities(ConnStringBuilder.GetEFConnectionStringInsightsDW(clientId)))
            {
                entDW.Configuration.AutoDetectChangesEnabled = false;
                entDW.Configuration.ValidateOnSaveEnabled = false;

                entDW.Database.Initialize(force: false);

                var cmd = entDW.Database.Connection.CreateCommand();
                cmd.CommandText = "[dbo].[GetPremiseActions]";
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.Add(new SqlParameter("@ClientID", typeof(int)) { Value = clientId });
                cmd.Parameters.Add(new SqlParameter("@CustomerID", typeof(string)) { Value = request.CustomerId });

                try
                {
                    entDW.Database.Connection.Open();
                    var reader = cmd.ExecuteReader();

                    // Read the first result set 
                    accounts = ((IObjectContextAdapter)entDW).ObjectContext.Translate<string>(reader).ToList();

                    // Move to second result set 
                    reader.NextResult();
                    ap = ((IObjectContextAdapter)entDW).ObjectContext.Translate<ActionPlan>(reader).ToList();
                }
                catch (Exception)
                {
                    accounts = null;
                }
                finally
                {
                    entDW.Database.Connection.Close();
                }
            }

            using (var transScope = new System.Transactions.TransactionScope(System.Transactions.TransactionScopeOption.Required, transactionOptions))
            {
                using (var ent = new InsightsEntities())
                {
                    ent.Database.Connection.Open();
                    //using (var dbContextTransaction = ent.Database.BeginTransaction())
                    //{
                    //    try
                    //    {
                            ent.Configuration.AutoDetectChangesEnabled = false;
                            ent.Configuration.ValidateOnSaveEnabled = false;

                            var apCount = 0;
                            //Check each acct if it exists                        
                            foreach (var acct in accounts)
                            {
                                List<ActionCustomerAccount> actCustAccts;
                                actCustAccts = (from pca in ent.ActionCustomerAccount
                                                where pca.ClientID == clientId && pca.CustomerID == request.CustomerId && pca.AccountID == acct
                                                select pca).ToList();

                                if (actCustAccts.Count == 0)
                                {
                                    //Save Content                        

                                    //ActionCustomerAccount
                                    ActionCustomerAccount aca = new ActionCustomerAccount
                                    {
                                        ClientID = clientId,
                                        CustomerID = request.CustomerId,
                                        AccountID = acct
                                    };
                                    ent.Entry(aca).State = System.Data.Entity.EntityState.Added;
                                    ent.ActionCustomerAccount.Add(aca);

                                    //ActionPremisePlan
                                    var apByAcct = ap.Where(x => x.AccountId == acct).ToList();
                                    foreach (var apPlan in apByAcct)
                                    {
                                        var applan = new ActionPremisePlan
                                        {
                                            ClientID = clientId,
                                            AccountID = acct,
                                            PremiseID = apPlan.PremiseId,
                                            ActionKey = apPlan.ActionKey,
                                            ActionStatus = apPlan.ActionStatus,
                                            ActionStatusDate = apPlan.ActionStatusDate,
                                            CreateDate = createdDate,
                                            UpdateDate = createdDate,
                                            SourceKey = apPlan.SourceKey,
                                            AdditionalInfo = apPlan.AdditionalInfo,
                                            SubActionKey = (apPlan.SubActionKey == null) ? string.Empty : apPlan.SubActionKey
                                        };
                                        ent.Entry(applan).State = System.Data.Entity.EntityState.Added;
                                        ent.ActionPremisePlan.Add(applan);
                                        apCount += 1;

                                    }
                                }
                                if (apCount >= 50)
                                {
                                    apCount = 0;
                                    ent.SaveChanges();
                                }
                            }
                            ent.SaveChanges();
                    //        dbContextTransaction.Commit();
                    //    }
                    //    catch (Exception)
                    //    {
                    //        dbContextTransaction.Rollback();
                    //    }
                    //}
                }
                transScope.Complete();
            }
        }
    }

    public class ActionPlan
    {
        public string ClientId { get; set; }
        public string AccountId { get; set; }
        public string PremiseId { get; set; }
        public string ActionKey { get; set; }
        public string ActionStatus { get; set; }
        public DateTime ActionStatusDate { get; set; }
        public string SourceKey { get; set; }
        public string AdditionalInfo { get; set; }
        public string SubActionKey { get; set; }
    }
}
