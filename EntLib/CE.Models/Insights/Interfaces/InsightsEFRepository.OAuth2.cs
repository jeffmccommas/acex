﻿using CE.GreenButtonConnect;
using CE.Models.Insights;
using CE.Models.Insights.EF;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Globalization;
using System.Linq;
using System.Transactions;

namespace CE.Models
{
    /// <summary>
    /// InsightsEFRepository
    /// </summary>
    public partial class InsightsEfRepository
    {
        private const string GrantTypeAuthorizationCode = "authorization_code";
        private const string GrantTypeRefreshToken = "refresh_token";
        private const string GrantTypeClientAccessToken = "client_credentials";
        private const string ErrorCodeUnsupportedResponseType = "unsupported_response_type";
        private const string ErrorDescUnsupportedResponseType = "The authorization server does not support obtaining a token using this method.";
        private const string ErrorCodeInvalidClient = "invalid_clientId";
        private const string ErrorDescInvalidClient = "Client {%client_id%} is not registered in the system.";
        private const string ErrorDescInactiveClient = "Client is inactive.";
        private const string ErrorCodeUnsupportedGrantType = "unsupported_grant_type";
        private const string ErrorCodeAccessDenied = "access_denied";
        private const string ErrorDescInvalidClientAuthorization = "Client authorization code is invalid.";
        private const string ErrorDescInvalidClientSecret = "Client secret is invalid.";
        private const string ErrorCodeInvalidRefreshToken = "invalid_refresh_token";
        private const string ErrorDescInvalidRefreshToken = "The provided refresh token is invalid.";
        private const string ThirdParty = "third_party";

        public TokenPostResponse ValidateClientAuthentication(TokenPostRequest context, string authorizationHeader, string baseUrl)
        {
            var tpr = new TokenPostResponse();

            if (context.grant_type != GrantTypeAuthorizationCode && context.grant_type != GrantTypeRefreshToken && context.grant_type != GrantTypeClientAccessToken)
            {
                tpr.Error = ErrorCodeUnsupportedGrantType;
                tpr.Error_Description = ErrorDescUnsupportedResponseType;
                return tpr;
            }

            //declare the transaction options
            var transactionOptions = new TransactionOptions
            {
                IsolationLevel = IsolationLevel.ReadUncommitted
            };
            var clientId = Helper.Base64Decode(authorizationHeader).Split(':')[0];
            var secret = Helper.Base64Decode(authorizationHeader).Split(':')[1];
            var dataCustodianId = string.Empty;
            List<application_information> client;
            var authCodes = new authorizations();
            var refreshTokens = new List<authorizations>();
            long applicationInfoId = 0;
            using (var transScope = new TransactionScope(TransactionScopeOption.Required, transactionOptions))
            {
                using (var ent = new InsightsEntities())
                {
                    ent.Database.Connection.Open();

                    client = (from ai in ent.application_information
                              where ai.client_id == clientId && ai.client_secret == secret && ai.enabled == true
                              select ai).ToList();

                    if (client.Count != 0 && context.grant_type != GrantTypeClientAccessToken)
                    {
                        applicationInfoId = client.FirstOrDefault().application_information_id;
                        dataCustodianId = client.FirstOrDefault().dataCustodianId;
                        
                        if (context.code == null)
                        {
                            if (context.grant_type == GrantTypeRefreshToken)
                            {
                                refreshTokens = (from oc in ent.authorizations
                                                 where oc.application_information_id == applicationInfoId && oc.refresh_token == context.refresh_token
                                                 && oc.third_party == ThirdParty && oc.status == (int)Helper.AuthorizationStatus.Active
                                                 select oc).ToList();

                                authCodes = refreshTokens.FirstOrDefault();
                            }
                        }
                        else
                        {
                            authCodes = (from oc in ent.authorizations
                                         where oc.application_information_id == applicationInfoId && oc.code == context.code 
                                         && oc.third_party == ThirdParty && oc.status == (int)Helper.AuthorizationStatus.Active
                                         select oc).FirstOrDefault();
                        }
                    }
                    else if (client.Count != 0 && context.grant_type == GrantTypeClientAccessToken)
                    {
                        applicationInfoId = client.FirstOrDefault().application_information_id;
                        dataCustodianId = client.FirstOrDefault().dataCustodianId;
                    }
                }
                transScope.Complete();
            }

            if (client.Count == 0)
            {
                tpr.Error = ErrorCodeInvalidClient;
                tpr.Error_Description = ErrorDescInvalidClient.Replace("{%client_id%}", clientId);
                return tpr;
            }
            var b = !client.FirstOrDefault().enabled;
            if (b != null && (bool)b)
            {
                tpr.Error = ErrorCodeInvalidClient;
                tpr.Error_Description = ErrorDescInactiveClient;
                return tpr;
            }

            switch (context.grant_type)
            {
                case GrantTypeAuthorizationCode:

                    if (authCodes == null)
                    {
                        tpr.Error = ErrorCodeAccessDenied;
                        tpr.Error_Description = ErrorDescInvalidClientAuthorization;
                        return tpr;
                    }
                    var timeValid = authCodes.authCodeExpiresIn;
                    if (timeValid > Helper.ConvertToUnixTimestamp(DateTime.UtcNow))
                    {
                        if (secret != client.FirstOrDefault().client_secret)
                        {
                            tpr.Error = ErrorCodeAccessDenied;
                            tpr.Error_Description = ErrorDescInvalidClientSecret;
                            return tpr;
                        }

                        if (context.code != authCodes.code)
                        {
                            tpr.Error = ErrorCodeAccessDenied;
                            tpr.Error_Description = ErrorDescInvalidClientAuthorization;
                            return tpr;
                        }
                    }
                    else
                    {
                        tpr.Error = ErrorCodeAccessDenied;
                        tpr.Error_Description = ErrorDescInvalidClientAuthorization;
                        return tpr;
                    }
                    break;
                case GrantTypeRefreshToken:
                    if (refreshTokens.Count == 0)
                    {
                        tpr.Error = ErrorCodeInvalidRefreshToken;
                        tpr.Error_Description = ErrorDescInvalidRefreshToken;
                        return tpr;
                    }

                    if (context.refresh_token != refreshTokens.FirstOrDefault().refresh_token)
                    {
                        tpr.Error = ErrorCodeInvalidRefreshToken;
                        tpr.Error_Description = ErrorDescInvalidRefreshToken;
                        return tpr;
                    }

                    var timeValidRefresh = refreshTokens.FirstOrDefault().refreshTokenExpiresIn;
                    if (timeValidRefresh <= Helper.ConvertToUnixTimestamp(DateTime.UtcNow))
                    {
                        tpr.Error = ErrorCodeAccessDenied;
                        tpr.Error_Description = ErrorDescInvalidRefreshToken;
                        return tpr;
                    }
                    break;
                case GrantTypeClientAccessToken:
                    if (secret != client.FirstOrDefault().client_secret)
                    {
                        tpr.Error = ErrorCodeAccessDenied;
                        tpr.Error_Description = ErrorDescInvalidClientSecret;
                        return tpr;
                    }
                    break;
            }
            //Save access token
            using (var transScope = new TransactionScope(TransactionScopeOption.Required, transactionOptions))
            {
                using (var ent = new InsightsEntities())
                {
                    ent.Database.Connection.Open();
                    using (var dbContextTransaction = ent.Database.BeginTransaction())
                    {
                        try
                        {
                            ent.Configuration.AutoDetectChangesEnabled = false;
                            ent.Configuration.ValidateOnSaveEnabled = false;

                            if (context.grant_type == GrantTypeAuthorizationCode || context.grant_type == GrantTypeRefreshToken)
                            {
                                var refreshToken = string.Empty;
                                switch (context.grant_type)
                                {
                                    case GrantTypeAuthorizationCode:
                                        {
                                            if (authCodes.refresh_token == null)
                                            {
                                                // generate refresh token when authcode is generated
                                                refreshToken = GenerateToken(clientId, secret, authCodes.retail_customer_id.ToString(), dataCustodianId);
                                                tpr.refresh_token = refreshToken;
                                            }
                                        }
                                        break;
                                    case GrantTypeRefreshToken:
                                        {

                                        }
                                        break;
                                }

                                authCodes.access_token = GenerateToken(clientId, secret, authCodes.retail_customer_id.ToString(), dataCustodianId);
                                authCodes.expiresin = Helper.ConvertToUnixTimestamp(DateTime.UtcNow.AddHours(1));
                                if (refreshToken.Length > 0)
                                {
                                    authCodes.refresh_token = refreshToken;
                                    authCodes.refreshTokenExpiresIn = Helper.ConvertToUnixTimestamp(DateTime.UtcNow.AddYears(2));
                                }
                                authCodes.updated = DateTime.UtcNow;
                                ent.Entry(authCodes).State = EntityState.Modified;
                                ent.SaveChanges();

                                tpr.token_type = "Bearer";
                                tpr.access_token = authCodes.access_token;
                                tpr.expires_in = 3600;
                                tpr.scope = authCodes.scope;
                                tpr.status = authCodes.status;
                                tpr.authorizationURI = baseUrl + "/api/v1" + authCodes.authorization_uri;
                                tpr.resourceURI = baseUrl + "/api/v1" + authCodes.resourceURI;
                            }
                            else if (context.grant_type == GrantTypeClientAccessToken)
                            {
                                //check if client token exists
                                var clientAccessToken = (from oc in ent.authorizations
                                                         where oc.application_information_id == applicationInfoId && oc.third_party == "third_party_admin"
                                                         select oc).FirstOrDefault();

                                if (clientAccessToken == null)
                                {
                                    var token = GenerateToken(clientId, secret, authCodes.retail_customer_id.ToString(), dataCustodianId);
                                    //create a record in authorization table -- third party admin token
                                    var a = new authorizations
                                    {
                                        published = DateTime.UtcNow,
                                        self_link_rel = "self",
                                        up_link_rel = "up",
                                        updated = DateTime.UtcNow,
                                        uuid = "",
                                        access_token = token,
                                        authorization_uri = "/Datacustodian/espi/1_1/resource/Authorization/",
                                        ap_duration = Helper.GetDuration(4),
                                        ap_start = Helper.ConvertToUnixTimestamp(DateTime.UtcNow.AddYears(-2)),
                                        expiresin = Helper.ConvertToUnixTimestamp(DateTime.UtcNow.AddHours(1)),
                                        grant_type = "client_credentials",
                                        pp_duration = 0,
                                        pp_start = 0,
                                        resourceURI = "/Datacustodian/espi/1_1/resource/ReadServiceStatus",
                                        scope = "FB=34_35",
                                        application_information_id = applicationInfoId,
                                        retail_customer_id = 0,
                                        third_party = "third_party_admin",
                                        status = (int)Helper.AuthorizationStatus.Active
                                    };

                                    ent.Entry(a).State = EntityState.Added;
                                    ent.authorizations.Add(a);
                                    ent.SaveChanges();

                                    //create a subscription record
                                    var s = new subscription
                                    {
                                        self_link_rel = "self",
                                        up_link_rel = "up",
                                        uuid = "",
                                        application_information_id = applicationInfoId,
                                        retail_customer_id = 0,
                                        authorization_id = a.id,
                                        published = DateTime.UtcNow,
                                        updated = DateTime.UtcNow
                                    };

                                    ent.Entry(s).State = EntityState.Added;
                                    ent.subscription.Add(s);
                                    ent.SaveChanges();

                                    var dcId = Convert.ToInt32(Helper.GetAclaraClientIdFromDataCustodianId(dataCustodianId));

                                    var genSubs = new IdGenerator(dcId, s.id);
                                    s.uuid = genSubs.SubscriptionIdClientAccess;
                                    ent.Entry(s).State = EntityState.Modified;
                                    ent.SaveChanges();

                                    //Update record with authorization uri, subscription id
                                    var genAuth = new IdGenerator(dcId, a.id);
                                    a.uuid = genAuth.AuthorizationIdClientAccess;
                                    a.authorization_uri = "/Datacustodian/espi/1_1/resource/Authorization/" + a.id;
                                    a.subscription_id = s.id;
                                    a.updated = DateTime.UtcNow;
                                    ent.Entry(a).State = EntityState.Modified;
                                    ent.SaveChanges();

                                    tpr.token_type = "Bearer";
                                    tpr.access_token = a.access_token;
                                    tpr.expires_in = 3600;
                                    tpr.scope = a.scope;
                                    tpr.status = a.status;
                                    tpr.authorizationURI = baseUrl + "/api/v1" + a.authorization_uri;
                                    tpr.resourceURI = baseUrl + "/api/v1" + a.resourceURI;
                                }
                                else
                                {
                                    if (clientAccessToken.expiresin <= Helper.ConvertToUnixTimestamp(DateTime.UtcNow))
                                    {
                                        clientAccessToken.expiresin = Helper.ConvertToUnixTimestamp(DateTime.UtcNow.AddHours(1));
                                        clientAccessToken.updated = DateTime.UtcNow;
                                        ent.Entry(clientAccessToken).State = EntityState.Modified;
                                        ent.SaveChanges();
                                    }

                                    tpr.token_type = "Bearer";
                                    tpr.access_token = clientAccessToken.access_token;
                                    tpr.expires_in = 3600;
                                    tpr.scope = clientAccessToken.scope;
                                    tpr.status = clientAccessToken.status;
                                    tpr.authorizationURI = baseUrl + "/api/v1" + clientAccessToken.authorization_uri;
                                    tpr.resourceURI = baseUrl + "/api/v1" + clientAccessToken.resourceURI;
                                }
                            }
                            dbContextTransaction.Commit();
                        }
                        catch (Exception)
                        {
                            dbContextTransaction.Rollback();
                        }
                    }
                }
                transScope.Complete();
            }
            return tpr;
        }

        #region Helper            

        private static string GenerateToken(string clientId, string secret, string retailCustomerId, string dataCustodianId)
        {
            var randomNumber = DateTime.UtcNow.ToString("yyyy-MM-dd HH:mm:ss.fff", CultureInfo.InvariantCulture);
            var keySource = clientId + "," + secret + "," + retailCustomerId + "," + dataCustodianId + "," + randomNumber;
            return Helper.Base64Encode(Helper.Encrypt(keySource));
        }       

        #endregion
    }
}
