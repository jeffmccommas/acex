﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;
using Newtonsoft.Json;

namespace CE.Models.Insights
{
    /// <summary>
    /// Information about a utility service agreement.
    /// </summary>
    [DataContract]
    public class CustomerInfoService
    {
        /// <summary>
        /// Id of the utility service.
        /// </summary>
        [DataMember(Order = 0)]
        [Required]
        public string Id { get; set; }

        /// <summary>
        /// The service type.
        /// </summary>
        [DataMember(Order = 1)]
        [StringLength(50)]
        public string Type { get; set; }

        /// <summary>
        /// The service description.
        /// </summary>
        [DataMember(Order = 2)]
        [StringLength(50)]
        public string Description { get; set; }

        /// <summary>
        /// The service active date.
        /// </summary>
        [DataMember(Order = 3)]
        public string ActiveDate { get; set; }

        /// <summary>
        /// The service inactive date.
        /// </summary>
        [DataMember(Order = 4)]
        public string InActiveDate { get; set; }

        /// <summary>
        /// The key value for the commodity provided by this service.
        /// </summary>
        [DataMember(Order = 5)]
        public string CommodityKey { get; set; }

        /// <summary>
        /// The service budget billing indicator.
        /// </summary>
        [DataMember(Order = 6)]
        public string BudgetBillingInd { get; set; }

        /// <summary>
        /// Service points for this service agreement.
        /// </summary>
        [DataMember(Order = 7)]
        public List<CustomerInfoServicePoint> ServicePoints { get; set; }
    }
}
