﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace CE.Models.Insights
{
    /// <summary>
    /// Information about a service point.
    /// </summary>
    [DataContract]
    public class CustomerInfoServicePoint
    {
        /// <summary>
        /// Id of the service point.
        /// </summary>
        [DataMember(Order = 0)]
        [Required]
        public string Id { get; set; }

        /// <summary>
        /// Meters.
        /// </summary>
        [DataMember(Order = 1)]
        public List<CustomerInfoMeter> Meters { get; set; }
    }
}
