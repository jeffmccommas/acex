﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace CE.Models.Insights
{
    /// <summary>
    /// A customer’s utility account.
    /// </summary>
    [DataContract]
    public class CustomerInfoAccount
    {
        /// <summary>
        /// Id of the utility account.
        /// </summary>
        [DataMember(Order = 0)]
        [Required]
        public string Id { get; set; }

        /// <summary>
        /// A list of premises that belong to the account.
        /// </summary>
        [DataMember(Order = 1)]
        public List<CustomerInfoPremise> Premises { get; set; }

    }
}
