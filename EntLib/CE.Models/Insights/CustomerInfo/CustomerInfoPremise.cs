﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace CE.Models.Insights
{
    /// <summary> Information about a premise.</summary>
    ///
    [DataContract]
    public class CustomerInfoPremise
    {
        /// <summary>
        /// Id of the utility premise.
        /// </summary>
        [DataMember(Order = 0)]
        [Required]
        public string Id { get; set; }

        /// <summary>
        /// Premise address Street one.
        /// </summary>
        [DataMember(Order = 1)]
        [StringLength(100)]
        [Required]
        public string Addr1 { get; set; }

        /// <summary>
        /// Premise address street two.
        /// </summary>
        [DataMember(Order = 2)]
        [StringLength(100)]
        public string Addr2 { get; set; }

        /// <summary>
        /// Premise city.
        /// </summary>
        [DataMember(Order = 3)]
        [StringLength(30)]
        [Required]
        public string City { get; set; }

        /// <summary>
        /// Premise state.
        /// </summary>
        [DataMember(Order = 4)]
        [StringLength(2)]
        [Required]
        public string StateProvince { get; set; }

        /// <summary>
        /// Premise postal code.
        /// </summary>
        [DataMember(Order = 5)]
        [StringLength(10)]
        [Required]
        public string PostalCode { get; set; }

        /// <summary>
        /// The customer's county.
        /// </summary>
        [DataMember(Order = 6)]
        [StringLength(50)]
        public string Country { get; set; }

        /// <summary>
        /// Service.
        /// </summary>
        [DataMember(Order = 7)]
        public List<CustomerInfoService> Service { get; set; }
    }
}
