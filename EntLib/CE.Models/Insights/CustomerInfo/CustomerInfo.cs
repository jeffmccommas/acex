﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace CE.Models.Insights
{
    /// <summary>
    /// Billing data for the Customer.
    /// </summary>
    [DataContract]
    public class CustomerInfo
    {
        /// <summary>
        /// Id of the utility customer
        /// </summary>
        [DataMember(Order = 0)]
        public string Id { get; set; }

        /// <summary>
        /// The customer's first name.
        /// </summary>
        [DataMember(Order = 1)]
        [StringLength(50)]
        public string FirstName { get; set; }

        /// <summary>
        /// The customer's last name.
        /// </summary>
        [DataMember(Order = 2)]
        [StringLength(50)]
        public string LastName { get; set; }

        /// <summary>
        /// The customer's address street 1.
        /// </summary>
        [DataMember(Order = 3)]
        [StringLength(50)]
        public string Addr1 { get; set; }

        /// <summary>
        /// The customer's address street 2.
        /// </summary>
        [DataMember(Order = 4)]
        [StringLength(50)]
        public string Addr2 { get; set; }

        /// <summary>
        /// The customer's address city.
        /// </summary>
        [DataMember(Order = 5)]
        [StringLength(50)]
        public string City { get; set; }

        /// <summary>
        /// The customer's address state.
        /// </summary>
        [DataMember(Order = 6)]
        [StringLength(50)]
        public string StateProvince { get; set; }

        /// <summary>
        /// The customer's address postal code.
        /// </summary>
        [DataMember(Order = 7)]
        [StringLength(50)]
        public string PostalCode { get; set; }

        /// <summary>
        /// The customer's county.
        /// </summary>
        [DataMember(Order = 8)]
        [StringLength(50)]
        public string Country { get; set; }

        /// <summary>
        /// The customer's phone number.
        /// </summary>
        [DataMember(Order = 9)]
        [StringLength(10)]
        public string PhoneNumber { get; set; }

        /// <summary>
        /// The customer's mobile phone number.
        /// </summary>
        [DataMember(Order = 10)]
        [StringLength(10)]
        public string MobilePhoneNumber { get; set; }

        /// <summary>
        /// The customer's email address.
        /// </summary>
        [DataMember(Order = 11)]
        [StringLength(50)]
        [RegularExpression(@"\b[A-Z0-9._%-]+@[A-Z0-9.-]+\.[A-Z]{2,4}\b", ErrorMessage = "Invalid Mail id")]
        public string EmailAddress { get; set; }

        /// <summary>
        /// The customer's alternate email address.
        /// </summary>
        [DataMember(Order = 12)]
        [StringLength(50)]
        [RegularExpression(@"\b[A-Z0-9._%-]+@[A-Z0-9.-]+\.[A-Z]{2,4}\b", ErrorMessage = "Invalid Mail id")]
        public string AlternateEmailAddress { get; set; }

        /// <summary>
        /// The customer's language.
        /// </summary>
        [DataMember(Order = 13)]
        [StringLength(50)]
        public string Language { get; set; }

        /// <summary>
        /// A list of accounts that belong to the utility customer.
        /// </summary>
        [DataMember(Order = 14)]
        public List<CustomerInfoAccount> Accounts { get; set; }
    }
}
