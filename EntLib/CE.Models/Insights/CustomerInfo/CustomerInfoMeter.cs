﻿using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace CE.Models.Insights
{
    /// <summary>
    /// Information about a meter.
    /// </summary>
    [DataContract]
    public class CustomerInfoMeter
    {
        /// <summary>
        /// Id of the meter.
        /// </summary>
        [DataMember(Order = 0)]
        [Required]
        public string Id { get; set; }

        /// <summary>
        /// Indicates that the meter has been deleted. If null then the meter has not been deleted. 
        /// </summary>
        [DataMember(Order = 1)]
        public string IsDeleted { get; set; }

        /// <summary>
        /// The Id of the meter that was replaced by this meter.
        /// </summary>
        [DataMember(Order = 2)]
        [Required]
        public string ReplacedMeterId { get; set; }
    }
}
