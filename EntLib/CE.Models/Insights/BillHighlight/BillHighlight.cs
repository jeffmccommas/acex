﻿using System.Runtime.Serialization;

namespace CE.Models.Insights
{
    /// <summary>
    /// Highlights displaying to a customer highlights on their current versus their previous bills.
    /// </summary>
    [DataContract]
    public class BillHighlight
    {
        /// <summary>
        /// Identifier of the action from the content management system.
        /// </summary>
        [DataMember(Order = 0)]
        public string ActionKey { get; set; }

        /// <summary>
        /// Cost of impact.
        /// </summary>
        [DataMember(Order = 1)]
        public decimal CostImpact { get; set; }

        /// <summary>
        /// Percentage of impact.
        /// </summary>
        [DataMember(Order = 2)]
        public decimal PercentageImpact { get; set; }

        /// <summary>
        /// Type of commodity. 
        /// </summary>
        [DataMember(Order = 3)]
        public string Commoditykey { get; set; }

        /// <summary>
        /// Service Id (Service Contract Id) of the highlight.
        /// </summary>
        [DataMember(Order = 4)]
        public string ServiceId { get; set; }
    }
}
