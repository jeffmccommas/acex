﻿using System.Runtime.Serialization;

namespace CE.Models.Insights
{
    public class EMProfileAttribute
    {
        [DataMember(Order = 0)]
        public string Key { get; set; }

        [DataMember(Order = 1)]
        public string OptionKey { get; set; }

        [DataMember(Order = 2)]
        public string OptionValue { get; set; }

        [DataMember(Order = 3)]
        public string Kind { get; set; }
    }


}
