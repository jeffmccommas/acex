﻿using System;
using System.Runtime.Serialization;

namespace CE.Models.Insights
{
    public class BDBill
    {
        /// <summary>
        /// End Date of Bill.
        /// </summary>
        [DataMember(Order = 0)]
        public DateTime EndDate { get; set; }

        /// <summary>
        /// Start Date of Bill.
        /// </summary>
        [DataMember(Order = 1)]
        public DateTime StartDate { get; set; }

        /// <summary>
        /// Commodity Key.
        /// </summary>
        [DataMember(Order = 2)]
        public string CommodityKey { get; set; }

        /// <summary>
        ///  Unit of Measure Key.
        /// </summary>
        [DataMember(Order = 3)]
        public string UomKey { get; set; }

        /// <summary>
        ///  Total Usage.
        /// </summary>
        [DataMember(Order = 4)]
        public decimal Usage { get; set; }

        /// <summary>
        ///  Total Usage.
        /// </summary>
        [DataMember(Order = 5)]
        public decimal Cost { get; set; }

        /// <summary>
        ///  Total Usage.
        /// </summary>
        [DataMember(Order = 6)]
        public int BillDays { get; set; }

    }

}