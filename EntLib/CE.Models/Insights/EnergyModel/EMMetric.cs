﻿using System.Runtime.Serialization;

namespace CE.Models.Insights
{
    public class EMMetric
    {
        [DataMember(Order = 0)]
        public string Name { get; set; }

        [DataMember(Order = 1)]
        public long Duration { get; set; }

    }

}
