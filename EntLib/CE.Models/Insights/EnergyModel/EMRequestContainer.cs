﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace CE.Models.Insights
{
    public class EMRequestContainer
    {
        /// <summary>
        /// Customer input parameters.
        /// </summary>
        [DataMember(Order = 0)]
        public EMCustomerParameters CustomerParameters { get; set; }

        /// <summary>
        /// Region input parameters.
        /// </summary>
        [DataMember(Order = 1)]
        public EMRegionParameters RegionParameters { get; set; }

        /// <summary>
        /// Additional Profile Attributes to add/replace.  Can be stand-alone collection.
        /// </summary>
        [DataMember(Order = 2)]
        public List<EMProfileAttribute> ProfileAttributes { get; set; }

        /// <summary>
        /// Additional Appliances to add/replace.  Can be stand-alone collection.
        /// </summary>
        [DataMember(Order = 3)]
        public List<EMAppliance> Appliances { get; set; }

        /// <summary>
        /// Additional EndUses to add/replace.  Can be stand-alone collection.
        /// </summary>
        [DataMember(Order = 4)]
        public List<EMEndUse> EndUses { get; set; }

        [DataMember(Order = 5)]
        public bool? OutputMonthly { get; set; }

        [DataMember(Order = 6)]
        public bool? StandAloneAppliances { get; set; }

        [DataMember(Order = 7)]
        public bool? StandAloneEndUses { get; set; }

        [DataMember(Order = 8)]
        public bool? StandAloneProfileAttributes { get; set; }

        [DataMember(Order = 9)]
        public bool? ReturnProfileAttributes { get; set; }

        [DataMember(Order = 10)]
        public bool? ReturnClientFactors { get; set; }

        [DataMember(Order = 11)]
        public bool? OutputTrace { get; set; }

        [DataMember(Order = 12)]
        public int? ChunkSize { get; set; }

        [DataMember(Order = 13)]
        public bool? ExecuteBillDisagg { get; set; }

        /// <summary>
        /// Bills as direct input for bill disagg use only.
        /// </summary>
        [DataMember(Order = 14)]
        public List<BDBill> Bills { get; set; }

    }
}
