﻿
using System.Runtime.InteropServices;
using System.Runtime.Serialization;


namespace CE.Models.Insights
{
    public class EMRegionParameters
    {

        /// <summary>
        /// Id for a temperature region.
        /// </summary>
        [DataMember(Order = 0)]
        public int TemperatureRegionId { get; set; }

        /// <summary>
        /// Id for a solar region.
        /// </summary>
        [DataMember(Order = 1)]
        public int SolarRegionId { get; set; }

        /// <summary>
        /// Id for an appliance region.  This is used to select a default profile attribute collection.
        /// </summary>
        [DataMember(Order = 2)]
        public int ApplianceRegionId { get; set; }

        /// <summary>
        /// State.
        /// </summary>
        [DataMember(Order = 3)]
        public string State { get; set; }

        /// <summary>
        /// Id for a climate.  Optional
        /// </summary>
        [DataMember(Order = 4)]
        public int ClimateId { get; set; }
    }
}
