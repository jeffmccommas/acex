﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace CE.Models.Insights
{
    public class EMResponseContainer
    {
        /// <summary>
        /// ClientId in response.
        /// </summary>
        [DataMember(Order = 0)]
        public int ClientId { get; set; }

        /// <summary>
        /// Customer input parameters.  Replayed if applicable.
        /// </summary>
        [DataMember(Order = 1)]
        public EMCustomerParameters CustomerParameters { get; set; }

        /// <summary>
        /// Region input parameters.  Replayed if applicable, or selected from customer information.
        /// </summary>
        [DataMember(Order = 2)]
        public EMRegionParameters RegionParameters { get; set; }

        /// <summary>
        /// Confidence average of all appliances.
        /// </summary>
        [DataMember(Order = 3)]
        public int Confidence { get; set; }

        /// <summary>
        /// Totals for energy results.
        /// </summary>
        [DataMember(Order = 4)]
        public List<EMEnergyUse> TotalEnergyUses { get; set; }

        /// <summary>
        /// Appliance results.
        /// </summary>
        [DataMember(Order = 5)]
        public List<EMAppliance> Appliances { get; set; }

        /// <summary>
        /// End Use results.
        /// </summary>
        [DataMember(Order = 6)]
        public List<EMEndUse> EndUses { get; set; }

        /// <summary>
        /// Profile attributes.
        /// </summary>
        [DataMember(Order = 7)]
        public List<EMProfileAttribute> ProfileAttributes { get; set; }

        /// <summary>
        /// Actual Bills.
        /// </summary>
        [DataMember(Order = 8)]
        public List<BDBill> Bills { get; set; }

        /// <summary>
        /// Computed Bills.
        /// </summary>
        [DataMember(Order = 9)]
        public List<BDBill> ComputedBills { get; set; }

        /// <summary>
        /// Disagg Status Name.
        /// </summary>
        [DataMember(Order = 10)]
        public List<BDDisaggStatus> DisaggStatuses { get; set; }

        /// <summary>
        /// Metrics of results.
        /// </summary>
        [DataMember(Order = 11)]
        public List<EMMetric> Metrics { get; set; }

        [DataMember(Order = 12)]
        public string ModelType { get; set; }

    }
    
    public class BDDisaggStatus
    {

        [DataMember(Order = 0)]
        public string CommodityKey { get; set; }
        [DataMember(Order = 1)]
        public string Status { get; set; }
    }
}
