﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace CE.Models.Insights
{
    [DataContract]
    public class EMAppliance
    {
        /// <summary>
        /// Name of appliance.
        /// </summary>
        [DataMember(Order = 0)]
        public string Name { get; set; }
        /// <summary>
        /// Confidence in the results.
        /// </summary>
        [DataMember(Order = 1)]
        public int Confidence { get; set; }
        /// <summary>
        /// Energy Uses.
        /// </summary>
        [DataMember(Order = 2)]
        public List<EMEnergyUse> EnergyUses { get; set; }

    }


}
