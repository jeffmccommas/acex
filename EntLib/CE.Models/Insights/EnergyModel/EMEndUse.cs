﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace CE.Models.Insights
{
    public class EMEndUse
    {
        /// <summary>
        /// Name of EndUse.
        /// </summary>
        [DataMember(Order = 0)]
        public string Name { get; set; }
        /// <summary>
        /// Name of Category.
        /// </summary>
        [DataMember(Order = 1)]
        public string Category { get; set; }
        /// <summary>
        /// Confidence in the results.
        /// </summary>
        [DataMember(Order = 2)]
        public int Confidence { get; set; }
        /// <summary>
        /// EnergyUses.
        /// </summary>
        [DataMember(Order = 3)]
        public List<EMEnergyUse> EnergyUses { get; set; }
        /// <summary>
        /// Appliance names.
        /// </summary>
        [DataMember(Order = 4)]
        public List<string> ApplianceNames { get; set; }
    }

}
