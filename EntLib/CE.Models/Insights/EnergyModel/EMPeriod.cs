﻿using System;
using System.Runtime.Serialization;
using Newtonsoft.Json;

namespace CE.Models.Insights
{
    public class EMPeriod
    {

        [DataMember(Order = 0)]
        public int Number { get; set; }

        [DataMember(Order = 1)]
        public string Name { get; set; }

        [IgnoreDataMember]
        public decimal Quantity { get; set; }

        [DataMember(Order = 2)]
        [JsonProperty(PropertyName = "Quantity")] //must use JsonProperty here to get the Name as desired on serialization
        public decimal OutputQuantity
        {
            get { return Math.Round(Quantity, 2); }
        }

        [DataMember(Order = 3)]
        public DateTime? Start { get; set; }

        [DataMember(Order = 4)]
        public DateTime? End { get; set; }

    }

}
