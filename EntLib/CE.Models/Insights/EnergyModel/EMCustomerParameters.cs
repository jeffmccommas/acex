﻿
using System.Runtime.Serialization;

namespace CE.Models.Insights
{
    public class EMCustomerParameters
    {
        /// <summary>
        /// Id for the customer. This will be used with account and premise to select the customer bill information and find a zipcode for the user.
        /// </summary>
        [DataMember(Order = 0)]
        public string CustomerId { get; set; }

        /// <summary>
        /// Id for the account. This will be used with customer and premise to select the customer bill information and find a zipcode for the user.
        /// </summary>
        [DataMember(Order = 1)]
        public string AccountId { get; set; }

        /// <summary>
        /// Id for the premise. This will be used with customer and account to select the customer bill information and find a zipcode for the user.
        /// </summary>
        [DataMember(Order = 2)]
        public string PremiseId { get; set; }

        /// <summary>
        /// Zipcode.  If just this is provided, it will be used to select region parameters.
        /// </summary>
        [DataMember(Order = 3)]
        public string Zipcode { get; set; }

    }
}
