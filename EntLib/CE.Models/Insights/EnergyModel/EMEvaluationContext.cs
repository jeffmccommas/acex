﻿using System.Runtime.Serialization;

namespace CE.Models.Insights
{
    public class EMEvaluationContext
    {

        [DataMember(Order = 0)]
        public string Expression { get; set; }

        [DataMember(Order = 1)]
        public string ContextName { get; set; }

        public EMEvaluationContext()
        {
            Expression = string.Empty;
            ContextName = "Pending";
        }

    }
}
