﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using Newtonsoft.Json;

namespace CE.Models.Insights
{
    public class EMEnergyUse
    {
        [DataMember(Order = 0)]
        public string CommodityName { get; set; }

        [DataMember(Order = 1)]
        public EMEvaluationContext EvaluationContext { get; set; }

        [IgnoreDataMember]
        public decimal Quantity { get; set; }

        [DataMember(Order = 2)]
        [JsonProperty(PropertyName = "Quantity")] //must use JsonProperty here to get the Name as desired on serialization
        public decimal OutputQuantity
        {
            get { return Math.Round(Quantity, 2); }
        }

        [IgnoreDataMember]
        public decimal Cost { get; set; }

        [DataMember(Order = 3)]
        [JsonProperty(PropertyName = "Cost")] //must use JsonProperty here to get the Name as desired on serialization
        public decimal OutputCost
        {
            get { return Math.Round(Cost, 2); }
        }

        [IgnoreDataMember]
        public decimal RecRatio { get; set; }

        [DataMember(Order = 4)]
        [JsonProperty(PropertyName = "RecRatio")] //must use JsonProperty here to get the Name as desired on serialization
        public decimal OutputReconRatio
        {
            get { return Math.Round(RecRatio, 4); }
        }

        [IgnoreDataMember]
        public decimal WeatherAdjustmentFactor { get; set; }

        [DataMember(Order = 5)]
        [JsonProperty(PropertyName = "WeatherFactor")] //must use JsonProperty here to get the Name as desired on serialization
        public decimal OutputWeatherAdjustmentFactor
        {
            get { return Math.Round(WeatherAdjustmentFactor, 4); }
        }

        [DataMember(Order = 6)]
        public string UnitOfMeasureName { get; set; }

        [DataMember(Order = 7)]
        public List<EMPeriod> Periods { get; set; }

    }


}
