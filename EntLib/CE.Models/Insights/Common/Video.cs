﻿using System;
using System.Runtime.Serialization;

namespace CE.Models.Insights
{
    /// <summary>
    /// Image
    /// </summary>
    [DataContract]
    public class Video
    {
        /// <summary>
        /// Url.
        /// </summary>
        [DataMember(Order = 0)]
        public String Url { get; set; }

        /// <summary>
        /// AltText.
        /// </summary>
        [DataMember(Order = 1)]
        public string AltText { get; set; }

        /// <summary>
        /// Description.
        /// </summary>
        [DataMember(Order = 2)]
        public string Description { get; set; }

    }
}
