﻿using CE.Models.Insights.Types;

namespace CE.Models.Insights
{
    /// <summary>
    /// Measurements other than the price or quantity. 
    /// </summary>
    public class Measurement
    {        
        /// <summary>
        /// Identifier of the measurement. This is a key value for retrieving measurement-specific content from the CMS.
        /// </summary>
        public string Key { get; set; }

        /// <summary>
        /// Quantity that was measured or calculated
        /// </summary>
        public decimal Quantity { get; set; }
        
        /// <summary>
        /// Units of the measurement 
        /// </summary>
        public MeasurementUnitType Units { get; set; }
    }
}
