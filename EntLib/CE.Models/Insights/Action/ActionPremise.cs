﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace CE.Models.Insights
{
    ///
    /// Actions for the premise
    ///
    [DataContract]
    public class ActionPremise
    {
        public ActionPremise()
        {
            Actions = new List<Action>();
            BillHighlights = new List<BillHighlight>();
            AmiHighlights = new List<AmiHighlight>();
        }

        /// <summary>
        /// Id of the utility premise.
        /// </summary>
        [DataMember(Order = 0)]
        public string Id { get; set; }

        /// <summary>
        /// Actions for the premise.
        /// </summary>
        [DataMember(Order = 1)]
        public List<Action> Actions { get; set; }

        /// <summary>
        /// Bill Highlights for the premise.
        /// </summary>
        [DataMember(EmitDefaultValue = false)]
        public List<BillHighlight> BillHighlights { get; set; }

        /// <summary>
        /// amo Highlights for the premise.
        /// </summary>
        [DataMember(EmitDefaultValue = false)]
        public List<AmiHighlight> AmiHighlights { get; set; }
    }
}
