﻿using System.Collections.Generic;
using System.Runtime.Serialization;


namespace CE.Models.Insights
{
    /// <summary>
    /// Information about an action recommended to a customer, including estimated costs and savings. 
    /// </summary>
    [DataContract]
    public class Action
    {
        /// <summary>
        /// Identifier of the action from the content management system.
        /// </summary>
        [DataMember(Order = 0)]
        public string Key { get; set; }

        /// <summary>
        /// Identifier of the action type from the content management system.
        /// </summary>
        [DataMember(Order = 1)]
        public string ActionTypeKey { get; set; }

        /// <summary>
        /// How much money the action will cost annually after the upfront cost.
        /// </summary>
        [DataMember(Order = 2)]
        public decimal AnnualCost { get; set; }

        /// <summary>
        /// How much money the action will cost upfront.
        /// </summary>
        [DataMember(Order = 3)]
        public decimal UpfrontCost { get; set; }

        /// <summary>
        /// Percentage plus or minus that the cost may vary.
        /// </summary>
        [DataMember(Order = 4)]
        public decimal CostVariancePercent { get; set; }

        /// <summary>
        /// How much money the action will save the customer per year. 
        /// </summary>
        [DataMember(Order = 5)]
        public decimal AnnualSavingsEstimate { get; set; }


        /// <summary>
        /// How much money the action will save the customer per year. 
        /// </summary>
        [DataMember(Order = 6)]
        public string AnnualSavingsEstimateCurrencyKey { get; set; }

        /// <summary>
        /// Quality of the savings estimate. 
        /// This reflects whether the savings are from a reputable study or model or simply rough estimates. 
        /// </summary>
        [DataMember(Order = 7)]
        public string SavingsEstimateQuality { get; set; }

        /// <summary>
        /// How much of the commodity the action will save per year. 
        /// </summary>
        [DataMember(Order = 8)]
        public List<ActionDetail> ActionDetails { get; set; }

        /// <summary>
        /// How many months it would take for the actions savings to equal the cost. 
        /// </summary>
        [DataMember(Order = 9)]
        public decimal PaybackTime { get; set; }

        /// <summary>
        /// Estimated financial savings over ten years. 
        /// </summary>
        [DataMember(Order = 10)]
        public decimal ROI { get; set; }

        /// <summary>
        /// Description of the popularity of the action.
        /// </summary>
        [DataMember(Order = 11)]
        public string Popularity { get; set; }

        /// <summary>
        /// Priority of the action.
        /// </summary>
        [DataMember(Order = 12)]
        public int Priority { get; set; }

        /// <summary>
        /// Additional measurements of the impact of the action. 
        /// </summary>
        [DataMember(Order = 13)]
        public List<ActionMeasurement> AdditionalMeasurements { get; set; }

        /// <summary>
        /// Rebate Amount for Action
        /// </summary>
        [DataMember(Order = 14)]
        public decimal RebateAmount { get; set; }

    }
}
