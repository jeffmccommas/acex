﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace CE.Models.Insights
{
    /// <summary>
    /// Actions for the Customer
    /// </summary>
    [DataContract]
    public class ActionCustomer
    {
        public ActionCustomer()
        {
            Accounts = new List<ActionAccount>();
        }


        /// <summary>
        /// Id of the utility customer
        /// </summary>
        [DataMember(Order = 0)]
        public string Id { get; set; }

        /// <summary>
        /// A list of accounts that belong to the utility customer.
        /// </summary>
        [DataMember(Order = 1)]
        public List<ActionAccount> Accounts { get; set; }
    }
}
