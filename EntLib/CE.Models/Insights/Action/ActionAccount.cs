﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace CE.Models.Insights
{
    /// <summary>
    /// Actions returned for the account.
    /// </summary>
    [DataContract]
    public class ActionAccount
    {
        public ActionAccount()
        {
            Premises = new List<ActionPremise>();
        }

        /// <summary>
        /// Id of the utility account.
        /// </summary>
        [DataMember(Order = 0)]
        public string Id { get; set; }


        /// <summary>
        /// A list of premises that belong to the account.
        /// </summary>
        [DataMember(Order = 2)]
        public List<ActionPremise> Premises { get; set; }

    }
}
