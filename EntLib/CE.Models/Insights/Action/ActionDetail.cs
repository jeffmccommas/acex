﻿using System.Runtime.Serialization;

namespace CE.Models.Insights
{
    /// <summary>
    /// Information about an action recommended to a customer, including estimated costs and savings. 
    /// </summary>
    public class ActionDetail
    {
        /// <summary>
        /// Identifier of the commodity for this detail from the content management system.
        /// </summary>
        [DataMember(Order = 0)]
        public string CommodityKey { get; set; }

        /// <summary>
        /// How much money the action will save the customer per year for this commodity. 
        /// </summary>
        [DataMember(Order = 1)]
        public decimal SavingsEstimate { get; set; }

        /// <summary>
        /// Key to the unit of measure for the currency.
        /// </summary>
        [DataMember(Order = 2)]
        public string SavingsEstimateCurrencyKey { get; set; }

        /// <summary>
        /// How much of the commodity the action will save per year. 
        /// </summary>
        [DataMember(Order = 3)]
        public decimal UsageSavingsEstimate { get; set; }

        /// <summary>
        /// Key to the unit of measure for the commodity usage.
        /// </summary>
        [DataMember(Order = 4)]
        public string UsageSavingsEstimateUomKey { get; set; }

    }
}
