﻿namespace CE.Models.Insights
{
    /// <summary>
    /// Action Measurements. 
    /// </summary>
    public class ActionMeasurement
    {
        public string Key { get; set; }

        public string Type { get; set; }

        public decimal Quantity { get; set; }

        public string Units { get; set; }
    }
}
