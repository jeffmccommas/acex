﻿using System;
using System.Runtime.Serialization;
using System.ComponentModel.DataAnnotations;
using System.IO;

namespace CE.Models.Insights
{
    /// <summary>
    /// File Metadata and content
    /// </summary>
    [DataContract]
    public class FileDetail
    {
        /// <summary>
        /// Id of the report
        ///</summary>
        [DataMember(Order = 0)]
        public int Id { get; set; }

        /// <summary>
        /// The customer's Id.
        /// </summary>
        [DataMember(Order = 1)]
        [StringLength(50)]
        public string CustomerId { get; set; }

        /// <summary>
        /// The customer's AccountId.
        /// </summary>
        [DataMember(Order = 2)]
        [StringLength(50)]
        public string AccountId { get; set; }

        /// <summary>
        /// The customer's Premise.
        /// </summary>
        [DataMember(Order = 3)]
        [StringLength(50)]
        public string PremiseId { get; set; }

        /// <summary>
        /// The Role of User saving the report
        /// </summary>
        [DataMember(Order = 4)]
        public byte RoleId{ get; set; }

        /// <summary>
        /// User ID.
        /// </summary>
        [DataMember(Order = 5)]
        [StringLength(50)]
        public string UserId { get; set; }

        /// <summary>
        /// File upload type ID.
        /// </summary>
        [DataMember(Order = 6)]
        public int TypeId { get; set; }

     
        /// <summary>
        /// File Title
        /// </summary>
        [DataMember(Order = 7)]
        [StringLength(50)]
        public string Title { get; set; }

        /// <summary>
        /// File description
        /// </summary>
        [DataMember(Order = 8)]
        [StringLength(500)]
        public string Description { get; set; }

       

        /// <summary>
        /// Name of File
        /// </summary>
        [DataMember(Order = 9)]
        [StringLength(150)]
        public string Name { get; set; }


        /// <summary>
        /// File upload Source ID.
        /// </summary>
        [DataMember(Order = 10)]
        public int FileSourceId { get; set; }

        /// <summary>
        /// Blob Name for Azure storage
        /// </summary>
        [DataMember(Order = 11)]
        [StringLength(150)]
        public string BlobName { get; set; }


        /// <summary>
        /// File content 
        /// </summary>
        [DataMember(Order = 12)]
        [StringLength(150)]
        public MemoryStream FileContent { get; set; }

        [DataMember(Order = 13)]
        public DateTime CreateDate { get; set; }
    }


    /// <summary>
    /// Type of File
    /// </summary>
    public enum FileType
    {
        Unknown,
        Pdf,
        Zip,
        Csv
    }

    /// <summary>
    /// Source of File
    /// </summary>
    public enum FileSource
    {
        Unknown,
        ReportCreate,
        Utility,
        ThirdParty
    }
}
