﻿using System.Configuration;
using System.Linq;

namespace CE.Models.Insights.DataAccess
{
    public partial class DataAccessLayer
    {
        private const string InsightsConnName = "InsightsConn";
        private const string InsightsDwConnName = "InsightsDWConn";
        private const string InsightsMetaDataConnName = "InsightsMetaDataConn";
        private const string AppSetting_PaaSOnly = "PaaSOnly";
        private const string AppSetting_PaaSDedicatedClients = "PaaSDedicatedClients";
        private readonly string _dataSourceConnString = string.Empty;
        private readonly string _dataSourceConnStringDw = string.Empty;
        private readonly string _dataSourceConnStringMetaData = string.Empty;

        public DataAccessLayer()
        {
            _dataSourceConnString = ConfigurationManager.ConnectionStrings[InsightsConnName].ConnectionString;
            _dataSourceConnStringDw = ConfigurationManager.ConnectionStrings[InsightsDwConnName].ConnectionString;
            _dataSourceConnStringMetaData = ConfigurationManager.ConnectionStrings[InsightsMetaDataConnName].ConnectionString;
        }

        private string GetConnection()
        {
            return (_dataSourceConnString);
        }
        public string GetConnectionMetaData()
        {
            return (_dataSourceConnStringMetaData);
        }

        private string GetConnectionDw()
        {
            return (_dataSourceConnStringDw);
        }

        private string GetConnectionDw(int clientID)
        {
            string connString = string.Empty;

            //Check if PaaS, and modify connection string
            string sPaaS = ConfigurationManager.AppSettings[AppSetting_PaaSOnly];
            if (!string.IsNullOrEmpty(sPaaS))
            {
                if (System.Convert.ToBoolean(sPaaS))
                {
                    string sPaaSDedicatedClients = ConfigurationManager.AppSettings[AppSetting_PaaSDedicatedClients];
                    if (!string.IsNullOrEmpty(sPaaSDedicatedClients))
                    {
                        var cid = clientID.ToString();
                        if (sPaaSDedicatedClients.Split(',').ToList().Contains(cid))
                        {
                            connString = _dataSourceConnStringDw.Replace("InsightsDW;", "InsightsDW_" + cid + ";");
                        }
                        else
                        {
                            connString = _dataSourceConnStringDw.Replace("InsightsDW;", "InsightsDW_Test;");
                        }

                    }
                    else
                    {
                        connString = _dataSourceConnStringDw.Replace("InsightsDW;", "InsightsDW_Test;");
                    }
                }
                else
                {
                    connString = _dataSourceConnStringDw;
                }
            }
            else
            {
                connString = _dataSourceConnStringDw;
            }

            return (connString);
        }

    }
}
