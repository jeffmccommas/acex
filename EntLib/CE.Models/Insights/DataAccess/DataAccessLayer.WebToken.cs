﻿using System;
using System.Data;
using System.Data.SqlClient;

namespace CE.Models.Insights.DataAccess
{
    public partial class DataAccessLayer
    {

        private const string ParamClientId = "@clientID";
        private const string ParamCustomerId = "@customerID";
        private const string ParamWebToken = "@webToken";
        private const string ParamAdditionalInfo = "@additionalInfo";
        private const string ParamUtcDateTimeCreated = "@utcDateTimeCreated";
        private const string ParamUserId = "@userID";
        private const string ParamGroupId = "@groupID";
        private const string ParamRoleId = "@roleID";

        private const string ParamEmailAddress = "@EmailAddress";
        private const string ParamPostalCode = "@PostalCode";

        /// <summary>
        /// Merges Customer Web Token in insights database.
        /// </summary>
        /// <param name="clientId"></param>
        /// <param name="customerId"></param>
        /// <param name="webToken"></param>
        /// <param name="additionalInfo"></param>
        /// <param name="utcDateTimeCreated"></param>
        /// <param name="userId"></param>
        /// <param name="groupId"></param>
        /// <param name="roleId"></param>
        public void MergeCustomerWebTokenInfo(int clientId, string customerId, string webToken, string additionalInfo, DateTime utcDateTimeCreated, string userId, int? groupId, int? roleId)
        {
            const string storedProcedureName = "[dbo].[uspMergeCustomerWebToken]";

            using (var connection = new SqlConnection(GetConnection()))
            {
                using (var cmd = new SqlCommand(storedProcedureName, connection))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter(ParamClientId, DbType.Int32) { Value = clientId });
                    cmd.Parameters.Add(new SqlParameter(ParamCustomerId, DbType.String) { Value = customerId });
                    cmd.Parameters.Add(new SqlParameter(ParamWebToken, DbType.String) { Value = webToken });
                    cmd.Parameters.Add(new SqlParameter(ParamAdditionalInfo, DbType.Int32) { Value = additionalInfo });
                    cmd.Parameters.Add(new SqlParameter(ParamUtcDateTimeCreated, typeof(DateTime)) { Value = utcDateTimeCreated });
                    cmd.Parameters.Add(new SqlParameter(ParamUserId, DbType.String) { Value = userId });
                    cmd.Parameters.Add(new SqlParameter(ParamGroupId, DbType.Int32) { Value = groupId });
                    cmd.Parameters.Add(new SqlParameter(ParamRoleId, DbType.Int32) { Value = roleId });

                    connection.Open();

                    cmd.ExecuteNonQuery();
                }
            }
        }

        /// <summary>
        /// Insert Customer Web Token in insights database.
        /// </summary>
        /// <param name="clientId"></param>
        /// <param name="customerId"></param>
        /// <param name="webToken"></param>
        /// <param name="additionalInfo"></param>
        /// <param name="utcDateTimeCreated"></param>
        /// <param name="userId"></param>
        /// <param name="groupId"></param>
        /// <param name="roleId"></param>
        public virtual void InsertCustomerWebTokenInfo(int clientId, string customerId, string webToken, string additionalInfo, DateTime utcDateTimeCreated, string userId, int? groupId, int? roleId)
        {
            const string storedProcedureName = "[dbo].[uspInsertCustomerWebToken]";

            using (var connection = new SqlConnection(GetConnection()))
            {
                using (var cmd = new SqlCommand(storedProcedureName, connection))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter(ParamClientId, DbType.Int32) { Value = clientId });
                    cmd.Parameters.Add(new SqlParameter(ParamCustomerId, DbType.String) { Value = customerId });
                    cmd.Parameters.Add(new SqlParameter(ParamWebToken, DbType.String) { Value = webToken });
                    cmd.Parameters.Add(new SqlParameter(ParamAdditionalInfo, DbType.Int32) { Value = additionalInfo });
                    cmd.Parameters.Add(new SqlParameter(ParamUtcDateTimeCreated, typeof(DateTime)) { Value = utcDateTimeCreated });
                    cmd.Parameters.Add(new SqlParameter(ParamUserId, DbType.String) { Value = userId });
                    cmd.Parameters.Add(new SqlParameter(ParamGroupId, DbType.Int32) { Value = groupId });
                    cmd.Parameters.Add(new SqlParameter(ParamRoleId, DbType.Int32) { Value = roleId });

                    connection.Open();

                    cmd.ExecuteNonQuery();
                }
            }
        }

        /// <summary>
        /// Gets Customer Web Token information.
        /// </summary>
        /// <param name="clientId"></param>
        /// <param name="customerId"></param>
        /// <param name="webToken"></param>
        /// <param name="webTokenTimeout"></param>
        /// <param name="isActive"></param>
        /// <param name="additionalInfo"></param>
        /// <param name="utcDateTimeCreated"></param>
        /// <param name="userId"></param>
        /// <param name="groupId"></param>
        /// <param name="roleId"></param>
        /// <returns>
        /// Returns True if Web Token found in database.
        /// Sets IsActive if creatd datetime is within webtoken timeout limit
        /// Includes additional info based on includeAdditionalInfo input param
        /// </returns>
        public bool GetCustomerWebTokenInfo(int clientId, string customerId, string webToken, int webTokenTimeout, out bool isActive, out string additionalInfo, out DateTime utcDateTimeCreated, string userId, int? groupId, int? roleId)
        {
            const string storedProcedureName = "[dbo].[uspGetCustomerWebToken]";
            const string stringWebToken = "WebToken";
            const string stringUtcDateTimeUpdated = "UtcDateTimeUpdated";
            const string stringAdditionalInfo = "AdditionalInfo";

            //Assign default values
            var result = false;
            isActive = false;
            additionalInfo = null;
            utcDateTimeCreated = new DateTime();

            using (var connection = new SqlConnection(GetConnection()))
            {
                using (var cmd = new SqlCommand(storedProcedureName, connection))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter(ParamClientId, DbType.Int32) { Value = clientId });
                    cmd.Parameters.Add(new SqlParameter(ParamCustomerId, DbType.String) { Value = customerId });
                    cmd.Parameters.Add(new SqlParameter(ParamUserId, DbType.String) { Value = userId });
                    cmd.Parameters.Add(new SqlParameter(ParamGroupId, DbType.Int32) { Value = groupId });
                    cmd.Parameters.Add(new SqlParameter(ParamRoleId, DbType.Int32) { Value = roleId });

                    connection.Open();

                    var reader = cmd.ExecuteReader();

                    if (reader.HasRows)
                    {
                        while (reader.Read())
                        {
                            if (reader[stringWebToken].ToString() != webToken) continue;
                            result = true;
                            utcDateTimeCreated = Convert.ToDateTime(reader[stringUtcDateTimeUpdated].ToString());
                            if (utcDateTimeCreated.AddMinutes(webTokenTimeout) > DateTime.UtcNow)
                            {
                                isActive = true;
                            }
                            additionalInfo = reader[stringAdditionalInfo].ToString();
                        }
                    }
                }
            }

            return result;
        }

        /// <summary>
        /// Deletes Customer Web Token in insights database.
        /// </summary>
        /// <param name="clientId"></param>
        /// <param name="customerId"></param>
        /// <param name="webToken"></param>
        /// <returns>Number of records affected.</returns>
        public int DeleteCustomerWebTokenInfo(int clientId, string customerId, string webToken)
        {
            const string storedProcedureName = "[dbo].[uspDeleteCustomerWebToken]";

            int result;

            using (var connection = new SqlConnection(GetConnection()))
            {
                using (var cmd = new SqlCommand(storedProcedureName, connection))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter(ParamClientId, DbType.Int32) { Value = clientId });
                    cmd.Parameters.Add(new SqlParameter(ParamCustomerId, DbType.String) { Value = customerId });
                    cmd.Parameters.Add(new SqlParameter(ParamWebToken, DbType.String) { Value = webToken });

                    connection.Open();

                    result = cmd.ExecuteNonQuery();
                }
            }

            return result;
        }

        /// <summary>
        /// Checks if CustomerId exist in datawarehouse or not.
        /// </summary>
        /// <param name="clientId"></param>
        /// <param name="customerId"></param>
        /// <returns>Returns 1 if CustomerId exitst. If not, returns 0.</returns>
        public int ValidateCustomerIdInDataWarehouse(int clientId, string customerId)
        {
            const string storedProcedureName = "[dbo].[CheckForCustomer]";

            int result;

            using (var connection = new SqlConnection(GetConnectionDw(clientId)))
            {
                using (var cmd = new SqlCommand(storedProcedureName, connection))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter(ParamClientId, DbType.Int32) { Value = clientId });
                    cmd.Parameters.Add(new SqlParameter(ParamCustomerId, DbType.String) { Value = customerId });

                    connection.Open();

                    result = (int)cmd.ExecuteScalar();
                }
            }

            return result;
        }

        /// <summary>
        /// Checks if anonymous customer id exist in datawarehouse or not by email address or
        /// Create a new one and return new customer id
        /// </summary>
        /// <param name="clientId"></param>
        /// <param name="emailAddress"></param>
        /// <param name="postalCode"></param>
        /// <param name="customerId"></param>
        /// <param name="accountId"></param>
        /// <param name="premiseId"></param>
        /// <returns>Returns TRUE/FALSE and anonymous user's customer id, account id and premise id </returns>
        public virtual bool GetAnonymousUser(int clientId, string emailAddress, string postalCode, out string customerId, out string accountId, out string premiseId)
        {
            const string storedProcedureName = "[dbo].[getAnonymousUser]";

            const string stringCustomerID = "CustomerId";

            bool result = false;
            customerId = string.Empty;
            accountId = string.Empty;
            premiseId = string.Empty;

            using (var connection = new SqlConnection(GetConnectionDw(clientId)))
            {
                using (var cmd = new SqlCommand(storedProcedureName, connection))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter(ParamClientId, DbType.Int32) { Value = clientId });
                    cmd.Parameters.Add(new SqlParameter(ParamEmailAddress, DbType.String) { Value = emailAddress });
                    cmd.Parameters.Add(new SqlParameter(ParamPostalCode, DbType.String) { Value = postalCode });

                    connection.Open();

                    var reader = cmd.ExecuteReader();
                    if (reader.HasRows)
                    {
                        while (reader.Read())
                        {
                            result = true;
                            customerId = reader[stringCustomerID].ToString();

                            accountId = customerId;
                            premiseId = customerId;

                        }
                    }
                }
            }
            return result;            
        }

    }
}
