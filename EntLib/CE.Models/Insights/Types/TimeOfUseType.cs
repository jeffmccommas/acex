﻿
namespace CE.Models.Insights.Types
{
    public enum TimeOfUseType
    {
        NoTou = 0,
        OnPeak,
        OffPeak,
        Shoulder1,
        Shoulder2,
        CriticalPeak

    }
}
