﻿namespace CE.Models.Insights.Types
{
    public enum GreenButtonEntryType
    {
        All,
        IntervalBlock,
        LocalTimeParameters,
        MeterReading,
        ReadingType,
        Subscription,
        UsagePoint
    }
}
