﻿
namespace CE.Models.Insights.Types
{
    /// <summary>
    /// Status types.
    /// </summary>
    public enum StatusType
    {
        Undefined,
        ParameterErrors,
        ModelErrors
    }
}
