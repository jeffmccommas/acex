﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CE.Models.Insights.Types
{
    public enum GreenButtonFunctionBlockType
    {
        CommonDataCustodian = 1,
        ConnectMyData = 3,
        IntervalMetering = 4,
        IntervalElecMetering = 5,
        Gas = 10,
        Water = 11,
        SecurityPrivacy = 13,
        AuthorizationAuthentication = 14,
        MulitpleUsagePoints = 18,
        QueryParmeters = 37
    }
}
