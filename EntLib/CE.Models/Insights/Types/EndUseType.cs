﻿
namespace CE.Models.Insights.Types
{
    /// <summary>
    /// End Uses.
    /// </summary>
    public enum EndUseType
    {
        Cooking,
        Cooling,
        HotWater,
        Other,
        Lighting,
        Heating,
        Pool,
        FoodStorage,
        Water,
        IndoorWater,
        OutdoorWater
    }
}
