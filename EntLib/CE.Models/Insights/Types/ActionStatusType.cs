﻿
namespace CE.Models.Insights.Types
{
    /// <summary>
    /// Action status type
    /// </summary>
    public enum ActionStatusType
    {
        None,
        Selected,
        Completed
    }
}
