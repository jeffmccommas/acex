﻿
using System.ComponentModel;

namespace CE.Models.Insights.Types
{
    public enum GreenButtonErrorType
    {
        [Description("No Error")]
        NoError = 0,
        [Description("Interval metering is not supported.")]
        IntervalMeteringNotSupportErr,
        [Description("Multiple Usage Points is not supported.")]
        MultipleUsageNotSupportErr,
        [Description("Commodity type or interval type is not supported.")]
        CommodityOrIntervalNotSupportErr,
        [Description("Query parameter is not supported.")]
        QueryParameterNotSupportErr,
        [Description("Subscription is invalid.")]
        InvalidSubscriptionErr,
        [Description("Usage Point is invalid for the subscription.")]
        InvalidSusbscriptionUsagePointErr,
        [Description("Access token is invalid for the subscription.")]
        InvalidSubscriptionAccessTokenErr,
        [Description("There's no meter reading.")]
        NoMeterReadingErr,
        [Description("There's no reading type.")]
        NoReadingTypeErr,
        [Description("The authorization is invalid.")]
        InvalidAuthorization,
        [Description("There's no ami data.")]
        NoAmiErr,
        [Description("The requested local time parameter is invalid.")]
        InvalidLocalTimeParamErr,
        [Description("The requested application information is invalid.")]
        InvalidApplicationInfoErr,
        [Description("The requested interval block id is invalid.")]
        InvalidIntervalBlockErr,
        [Description("The requested authorization is invalid.")]
        InvalidRequestAuthorization
    }
}
