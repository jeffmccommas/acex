﻿
namespace CE.Models.Insights.Types
{
    public enum MeasurementUnitType
    {
        lbs,
        pts
    }
}
