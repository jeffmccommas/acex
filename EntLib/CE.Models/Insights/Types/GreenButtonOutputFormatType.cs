﻿namespace CE.Models.Insights.Types
{
    public enum GreenButtonOutputFormatType
    {
        Zip = 1,
        Xml = 2
    }
}
