﻿
namespace CE.Models.Insights.Types
{
    public enum UnitOfMeasureType
    {
        kWh = 1,
        CCF = 2,
        Therms = 3,
        kW = 4,
        Gal = 5,
        USGL = 6,
        CGal = 7,
        CF = 8,
        KGAL = 9,
        hgal = 10,
        DKTherms = 11,
        kVA = 12,
        kVAH = 13,
        kVAR = 14,
        kVARh = 15,
        HCF = 16,
        lb = 17,
        klb = 18,
        CubicMeter = 19,
        CubicFoot = 20,
        Liter = 21, 
        MCF = 22,
        Other = 99
    }
}
