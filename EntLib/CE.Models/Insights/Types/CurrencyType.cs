﻿
namespace CE.Models.Insights.Types
{
    public enum CurrencyType
    {
        USD = 1,
        CAD = 2
    }
}
