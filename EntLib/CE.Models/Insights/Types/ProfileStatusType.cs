﻿
namespace CE.Models.Insights.Types
{
    public enum ProfileStatusType
    {
        Unspecified,
        Partial,
        Complete
    }
}
