﻿namespace CE.Models.Insights.Types
{
    public enum GreenButtonLinkRelationshipType
    {
        self,
        up,
        related
    }
}
