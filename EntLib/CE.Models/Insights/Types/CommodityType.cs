﻿
namespace CE.Models.Insights.Types
{
    public enum CommodityType
    {
        Electric = 1,
        Gas = 2,
        Oil = 3,
        Propane = 4,
        Water = 5
    }
}
