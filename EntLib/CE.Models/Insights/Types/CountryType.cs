﻿
namespace CE.Models.Insights.Types
{
    public enum CountryType
    {
        // ReSharper disable once InconsistentNaming
        US,
        // ReSharper disable once InconsistentNaming
        CA
    }
}
