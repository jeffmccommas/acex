﻿namespace CE.Models.Insights.Types
{
    public enum GreenButtonAccessTokenType
    {
        Customer,
        Client,
        Registration
    }
}