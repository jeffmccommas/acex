﻿namespace CE.Models.Insights.Types
{
    public enum GreenButtonSubscriptionFrequencyType
    {
        Daily = 1,
        Weekly = 7,
        Monthly = 31
    }
}