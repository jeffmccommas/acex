﻿namespace CE.Models.Insights.Types
{
    public enum ResolutionType
    {
        bill = 0, // use by green button for now
        month = 1,
        day = 24,
        five = 5, // use by green button for now
        fifteen = 15,
        thirty = 30,
        hour = 60,
        twohour = 120, // used by green button for now
        fourhour = 240, // used by green button for now
        sixhour = 360, // used by green button for now
        twelvehour = 720, // used by green button for now
        none
    }
}
