﻿
namespace CE.Models.Insights.Types
{
    public enum ApplianceType
    {
        /// <summary>
        /// The whole house
        /// </summary>
        House,
        AtticFan,
        CeilingFan,
        CentralAC,
        ClothesWasher,
        Computer,
        Cooktop,
        Dehumidifier,
        Dishwasher,
        Dryer,
        Fireplace,
        Freezer,
        Freezer2,
        WoodStove,
        HotTub,
        HouseFan,
        Humidifier,
        Lighting,
        Microwave,
        Miscellaneous,
        Oven,
        Pool,
        Refrigerator,
        Refrigerator2,
        RoomAC,
        RoomAC2,
        RoomAC3,
        SpaceHeater,
        SumpPump,
        TV,
        Waterbed,
        WaterHeater,
        WellPump,
        SecondaryHeat,
        HeatSystem,
        OutSideWater,
        TVFlat,
        TVRear,
        Sinks,
        Toilets,
        Showers,
        Vehicle
    }

}
