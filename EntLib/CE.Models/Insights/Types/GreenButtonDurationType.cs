﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CE.Models.Insights.Types
{
    public enum GreenButtonDurationType
    {
        Fifteen = 900,
        Thirty = 1800,
        Hour = 3600,
        Daily = 86400
    }
}
