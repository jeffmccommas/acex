﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;
using CE.Models.Insights.Types;

// ReSharper disable once CheckNamespace
namespace CE.Models.Insights
{
    /// <summary>
    /// 
    /// </summary>
    [DataContract]
    public class GreenButtonAmiReadingType
    {

        /// <summary>
        /// 
        /// </summary>
        [DataMember(Order = 0)]
        public long Id { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [DataMember(Order = 1)]
        [Required]
        public string Uuid { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [DataMember(Order = 2)]
        [Required]
        public string MeterId { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [DataMember(Order = 3)]
        [Required]
        public int AccumulationBehavior { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [DataMember(Order = 4)]
        [Required]
        public int Commodity { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [DataMember(Order = 5)]
        public int? ConsumptionTier { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [DataMember(Order = 6)]
        public int? Currency { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [DataMember(Order = 7)]
        public int DataQualifier { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [DataMember(Order = 8)]
        public int? DefaultQuality { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [DataMember(Order = 9)]
        public int FlowDirection { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [DataMember(Order = 10)]
        public int IntervalLength { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [DataMember(Order = 11)]
        public int Kind { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [DataMember(Order = 12)]
        public int Phase { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [DataMember(Order = 13)]
        public int PowerOfTenMultiplier { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [DataMember(Order = 14)]
        public int TimeAttribute { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [DataMember(Order = 15)]
        public int? Tou { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [DataMember(Order = 16)]
        public int Uom { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [DataMember(Order = 17)]
        public int? Aggregate { get; set; }


        /// <summary>
        /// 
        /// </summary>
        [DataMember(Order = 18)]
        public decimal? RationalDenomiator { get; set; }


        /// <summary>
        /// 
        /// </summary>
        [DataMember(Order = 19)]
        public decimal? RationalNumerator { get; set; }
        
        /// <summary>
        /// 
        /// </summary>
        [DataMember(Order = 20)]
        public int? Cpp { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [DataMember(Order = 21)]
        public decimal? InterharMonicDenominator { get; set; }
        
        /// <summary>
        /// 
        /// </summary>
        [DataMember(Order = 22)]
        public decimal? InterharmonicNumerator { get; set; }
        
        /// <summary>
        /// 
        /// </summary>
        [DataMember(Order = 23)]
        public int? MeasuringPeriod { get; set; }


        /// <summary>
        /// 
        /// </summary>
        [DataMember(Order = 24)]
        public DateTime PublishedDate { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [DataMember(Order = 25)]
        public DateTime UpdatedDate { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [DataMember(Order = 26)]
        public List<GreenButtonLink> Links { get; set; }

        [DataMember(Order = 27)]
        public IntervalConsumptionData ConsumptionList { get; set; }

        public GreenButtonDurationType BlockDuratioin { get; set; }
    }
}
