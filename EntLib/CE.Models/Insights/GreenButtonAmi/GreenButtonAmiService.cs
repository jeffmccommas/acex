﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;
using Newtonsoft.Json;

namespace CE.Models.Insights
{
    /// <summary>
    /// Billing data for a specific service belonging to a Customer, Account and Premise.
    /// </summary>
    [DataContract]
    public class GreenButtonAmiService
    {
        /// <summary>
        /// Id of the utility service.
        /// </summary>
        [DataMember(Order = 0)]
        [Required]
        public List<GreenButtonAmiMeterReading> MeterReadings { get; set; }

    }
}
