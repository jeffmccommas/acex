﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;
using Newtonsoft.Json;

namespace CE.Models.Insights
{
    /// <summary>
    /// Bill data for an account.
    /// </summary>
    [DataContract]
    public class GreenButtonAmiAccount
    {
        /// <summary>
        /// Id of the utility account.
        /// </summary>
        [DataMember(Order = 0)]
        [Required]
        public string Id { get; set; }

        /// <summary>
        /// A list of bills that belong to the account.
        /// </summary>
        [DataMember(Order = 1)]
        public List<GreenButtonAmiPremise> Premises { get; set; }

    }
}
