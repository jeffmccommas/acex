﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace CE.Models.Insights
{
    /// <summary>
    /// Billing data for the Customer.
    /// </summary>
    [DataContract]
    public class GreenButtonAmiCustomer
    {
        /// <summary>
        /// Id of the utility customer
        /// </summary>
        [DataMember(Order = 0)]
        public string Id { get; set; }

        /// <summary>
        /// The customer's first name.
        /// </summary>
        [DataMember(Order = 1)]
        [StringLength(50)]
        public string FirstName { get; set; }

        /// <summary>
        /// The customer's last name.
        /// </summary>
        [DataMember(Order = 2)]
        [StringLength(50)]
        public string LastName { get; set; }


        /// <summary>
        /// Bill data for the accounts that belong to the utility customer.
        /// </summary>
        [DataMember(Order = 3)]
        public List<GreenButtonAmiAccount> Accounts { get; set; }
    }
}
