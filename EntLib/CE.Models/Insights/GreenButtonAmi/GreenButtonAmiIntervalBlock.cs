﻿using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;
using Newtonsoft.Json;

namespace CE.Models.Insights
{
    /// <summary>
    /// 
    /// </summary>
    [DataContract]
    public class GreenButtonAmiIntervalBlock
    {
        /// <summary>
        /// 
        /// </summary>
        [DataMember(Order = 0)]
        [Required]
        public int Start { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [DataMember(Order = 1)]
        [Required]
        public int Duration { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [DataMember(Order = 2)]
        [Required]
        public decimal Value { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [DataMember(Order = 3)]
        public decimal Cost { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [DataMember(Order = 4)]
        public string Quality { get; set; }
    }
}
