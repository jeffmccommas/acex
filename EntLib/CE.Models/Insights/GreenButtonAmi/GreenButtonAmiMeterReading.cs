﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace CE.Models.Insights
{
    /// <summary>
    /// Summary of a utility bill.
    /// </summary>
    [DataContract]
    public class GreenButtonAmiMeterReading
    {

        /// <summary>
        /// 
        /// </summary>
        [DataMember(Order = 4)]
        public List<GreenButtonAmiReadingType> ReadingType { get; set; }

    }
}
