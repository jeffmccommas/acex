﻿using System.Runtime.Serialization;

// ReSharper disable once CheckNamespace
namespace CE.Models.Insights
{
    public class ConsumptionTouData
    {
        /// <summary>
        /// TOU of the Consumption value.
        /// </summary>
        [DataMember(Order = 1)]
        public string TouKey { get; set; }
        /// <summary>
        /// Consumption value.
        /// </summary>
        [DataMember(Order = 2)]
        public decimal Value { get; set; }

    }
}
