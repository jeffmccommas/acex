﻿using System;
using System.Runtime.Serialization;

// ReSharper disable once CheckNamespace
namespace CE.Models.Insights
{
    public class ConsumptionWeather
    {
        /// <summary>
        /// Date.
        /// </summary>
        [DataMember(Order = 1)]
        public DateTime Date { get; set; }


        /// <summary>
        /// Average temperature.
        /// </summary>
        [DataMember(Order = 2)]
        public int AvgTemp { get; set; }
    }
}
