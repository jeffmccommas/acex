﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

// ReSharper disable once CheckNamespace
namespace CE.Models.Insights
{
    /// <summary>
    /// Consumption data.
    /// </summary>
    [DataContract]
    public class ConsumptionData
    {
        /// <summary>
        /// Date and time.
        /// </summary>
        [DataMember(Order = 1)]
        public DateTime DateTime { get; set; }


        /// <summary>
        /// Consumption value.
        /// No Tou/total of all the tou consumptions.
        /// </summary>
        [DataMember(Order = 2)]
        public decimal Value { get; set; }

        /// <summary>
        /// List of all available Tou Consumption Data
        /// </summary>
        [DataMember(Order = 3, EmitDefaultValue = false)]
        public List<ConsumptionTouData> TouConsumptions{ get; set; }
    }
}
