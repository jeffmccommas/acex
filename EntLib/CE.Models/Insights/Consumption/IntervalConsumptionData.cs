﻿using System.Collections.Generic;
using System.Runtime.Serialization;

// ReSharper disable once CheckNamespace
namespace CE.Models.Insights
{
    public class IntervalConsumptionData
    {
        /// <summary>
        /// List of consumption data.
        /// </summary>
        [DataMember(Order = 1)]
        public List<ConsumptionData> ConsumptionDataList { get; set; }
        /// <summary>
        ///  Key value for the interval resolution of the list of consumption data
        /// </summary>
        [DataMember(Order = 2)]
        public string ResolutionKey { get; set; }
    }

    public class MeterConsumption
    {
        public string MeterId { get; set; }
        public int CommodityId { get; set; }
        public string UomKey { get; set; }

        public bool ConvertUom { get; set; }
        public bool UseDefaultConversionFactor { get; set; }
        public double DefaultConversioniFactor { get; set; }
        public List<IntervalConsumptionData> IntervalConsumptionList { get; set; }
    }
}
