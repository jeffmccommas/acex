﻿using System;
using System.Runtime.Serialization;

namespace CE.Models.Insights
{
    /// <summary>
    /// A current profile value.
    /// </summary>
    [DataContract]
    public class ProfileAttribute
    {
        /// <summary>
        /// Key to the profile attribute used in the content management system. Should be all lower-case.
        /// The POST process validates that the attribute key is defined in the CMS, and this validation is case-sensitive.
        /// </summary>
        [DataMember(Order = 0)]
        public string AttributeKey { get; set; }

        /// <summary>
        /// Profile attribute value. This will either be a key to a profile option or a specific value. 
        /// If it is a key to a profile option then it should be all lower-case.
        /// If the POST validation is set to True then it validates that specific values are the correct type of data 
        /// and that option keys are defined in the CMS. Note that option key validation is case-sensitive.
        /// </summary>
        [DataMember(Order = 1)]
        public string AttributeValue { get; set; }

        /// <summary>
        /// Key values for conditions that make this attribute not applicable. This is not used by the POST process.
        /// </summary>
        public string ConditionKeys { get; set; }       //NOTE: was Criteria

        /// <summary>
        /// Date that the profile value was last updated. The date will be in GMT time formatted as YYYY-MM-DDTHH:II:SS.Ms
        /// </summary>
        [DataMember(Order = 2)]
        public DateTime? EffectiveDate { get; set; }

        /// <summary>
        /// Key value of the source of the profile value. Should be all lower-case.
        /// If the POST validation is set to True then it validates that the source key is defined in the CMS, 
        /// and this validation is case-sensitive.
        /// </summary>
        [DataMember(Order = 4)]
        public string SourceKey { get; set; }
    }
}
