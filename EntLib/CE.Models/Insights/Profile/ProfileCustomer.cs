﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace CE.Models.Insights
{
    /// <summary>
    /// Profile data for the customer, accounts and premises.
    /// </summary>
    [DataContract]
    public class ProfileCustomer
    {
        /// <summary>
        /// Id of the utility customer.
        /// </summary>
        [DataMember(Order = 0)]
        public string Id { get; set; }

        /// <summary>
        /// Profile values for the customer.
        /// </summary>
        [DataMember(Order = 1)]
        public List<ProfileAttribute> Attributes { get; set; }

        /// <summary>
        /// A list of accounts that belong to the utility customer.
        /// </summary>
        [DataMember(Order = 2)]
        public List<ProfileAccount> Accounts { get; set; }

    }
}
