﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace CE.Models.Insights
{
    /// <summary>
    /// Profile data for the account and premises.
    /// </summary>
    [DataContract]
    public class ProfileAccount
    {
        /// <summary>
        /// Id for the utility account.
        /// </summary>
        [DataMember(Order = 0)]
        public string Id { get; set; }

        /// <summary>
        /// Profile values for the account.
        /// </summary>
        [DataMember(Order = 1)]
        public List<ProfileAttribute> Attributes { get; set; }

        /// <summary>
        /// A list of premises that belong to the utility customer/account.
        /// </summary>
        [DataMember(Order = 2)]
        public List<ProfilePremise> Premises { get; set; }

    }
}
