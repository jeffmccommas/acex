﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace CE.Models.Insights
{
    /// <summary>
    /// Profile data for the premise.
    /// </summary>
    [DataContract]
    public class ProfilePremise
    {
        /// <summary>
        /// Id of the utility premise.
        /// </summary>
        [DataMember(Order = 0)]
        public string Id { get; set; }

        /// <summary>
        /// Profile values for the premise.
        /// </summary>
        [DataMember(Order = 1)]
        public List<ProfileAttribute> Attributes { get; set; }


    }
}