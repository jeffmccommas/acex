﻿using CE.ContentModel.Entities;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace CE.Models.Insights
{
    /// <summary>
    ///  Bill to Date Post Request
    /// </summary>
    public class BillToDatePostRequest
    {

        /// <summary>
        /// The ID of the utility customer.
        /// </summary>      
        [MaxLength(50)]
        [Required]
        public string CustomerId { get; set; }

        /// <summary>
        /// The ID of the utility customer's account. 
        /// </summary>
        [MaxLength(50)]
        [Required]
        public string AccountId { get; set; }

        /// <summary>
        /// Id of the utility client used in the rate engine.
        /// </summary>
        [Required]
        public int RateCompanyId { get; set; }

        /// <summary>
        /// Metered services to include in the calculation.
        /// </summary>
        public List<BillToDateMeterService> Services {get; set;}

        /// <summary>
        /// Non-metered services to include in the calculation.
        /// </summary>
        public List<BillToDateNonMeter> NonMeteredServices { get; set; }

        /// <summary>
        /// Configuration settings to use in the calculation.
        /// </summary>
        public List<ClientConfiguration> Configurations { get; set; }

    }
}
