﻿using System.Runtime.Serialization;

namespace CE.Models.Insights
{
    /// <summary>
    /// A collection of actions, in descending order by annual savings estimate
    /// </summary>
    [DataContract]
    public class ActionResponse : Status
    {
        /// <summary>
        /// The id of the Client, typically a utility, who is using the API.
        /// </summary>
        [DataMember(Order = 0)]
        public int? ClientId { get; set; }

        /// <summary>
        /// Actions selected for the customer.
        /// </summary>
        [DataMember(Order = 1)]
        public ActionCustomer Customer { get; set; }

        /// <summary>
        /// Action-related content data.
        /// </summary>     
        [DataMember(Order = 2)]
        public Content Content { get; set; }

    }
}