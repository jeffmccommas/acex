﻿using System.ComponentModel.DataAnnotations;

// ReSharper disable once CheckNamespace
namespace CE.Models.Insights
{
    /// <summary>
    /// Green Button Bill Request
    /// </summary>
    public class GreenButtonBillRequest
    { 
        /// <summary>
        ///  The ID of the utility customer.
        ///  </summary>      
        [MaxLength(50)]
        [Required]
        public string CustomerId { get; set; }
        /// <summary>
        /// The ID of the utility customer's account. 
        /// </summary>
        [MaxLength(50)]
        public string AccountId { get; set; }

        /// <summary>
        /// The ID of the utility customer's premise. 
        /// </summary>
        [MaxLength(50)]
        public string PremiseId { get; set; }

        /// <summary>
        /// The id of the utility customer’s service contract.       
        /// </summary>
        [MaxLength(50)]
        public string ServiceContractId { get; set; }
        /// <summary>
        /// Indicate whether to output a Zip file containing the XML file and stylesheet or to output XML for a single Service. 
        /// If not provided, default to Zip if Account/Premise/Service Id is not available; otherwise, Xml
        /// Valid values: Zip or Xml
        /// </summary>
        [MaxLength(3)]
        [EnumDataType(typeof(Types.GreenButtonOutputFormatType))]
        public string OutputFormat { get; set; }

    }
}
