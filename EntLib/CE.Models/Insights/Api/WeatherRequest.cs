﻿using System;
using System.ComponentModel.DataAnnotations;

// ReSharper disable once CheckNamespace
namespace CE.Models.Insights
{
    public class WeatherRequest
    {
        /// <summary>
        /// Start date of the period to return. 
        /// Format is YYYY-DD-MMThh:mm:ssTZD, where TZD is a time zone designator (Z, +hh:mm or -hh:mm)       
        /// </summary>
        [Required]
        public DateTime StartDate { get; set; }

        /// <summary>
        /// End date of the period to return. 
        /// Format is YYYY-DD-MMThh:mm:ssTZD, where TZD is a time zone designator (Z, +hh:mm or -hh:mm)        
        /// </summary>
        [Required]
        public DateTime EndDate { get; set; }

        /// <summary>
        /// The Zip Code for the Weather Data
        /// Required if IncludeWeather is true
        /// </summary>
        public string ZipCode { get; set; }

        /// <summary>
        /// TheStationId for the Weather Data
        /// </summary>
        public string StationId { get; set; }
    }
}
