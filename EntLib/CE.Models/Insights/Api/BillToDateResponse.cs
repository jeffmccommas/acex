﻿using System.Runtime.Serialization;

namespace CE.Models.Insights
{
    /// <summary>
    ///  Bill to Date Response
    /// </summary>
    [DataContract]
    public class BillToDateResponse : Status
    {
        /// <summary>
        /// The id of the Client, typically a utility, who is using the API.
        /// </summary>
        [DataMember(Order = 0)]
        public int? ClientId { get; set; }

        /// <summary>
        /// Bill to Date result for an account
        /// </summary>
        [DataMember(Order = 1)]
        public BillToDate BillToDate { get; set; }

        /// <summary>
        /// Bill to Date related content data.
        /// </summary>     
        [DataMember(Order = 2)]
        public Content Content { get; set; }
    }
}
