﻿using System.Runtime.Serialization;

namespace CE.Models.Insights
{
    /// <summary>
    /// The BillDisagg response shows how a bill's usage quantities and costs
    /// are distributed across appliances and end uses.
    /// </summary>
    [DataContract]
    public class BillDisaggResponse : Status
    {
        /// <summary>
        /// The id of the Client, typically a utility, who is using the API.
        /// </summary>
        [DataMember(Order = 0)]
        public int? ClientId { get; set; }
        /// <summary>
        /// Disaggregated bill data for the customer.
        /// </summary>
        [DataMember(Order = 1)]
        public Customer Customer { get; set; }
        /// <summary>
        /// Related content blocks.
        /// </summary>
        [DataMember(Order = 2)]
        public Content Content { get; set; }

    }
}
