﻿using System;
using System.Runtime.Serialization;

// ReSharper disable once CheckNamespace
namespace CE.Models.Insights
{
    /// <inheritdoc />
    /// <summary>
    /// The WebToken Post response includes current WebToken data for a customer.
    /// </summary>
    [DataContract]
    public class WebTokenPostResponse : Status
    {
        /// <summary>
        /// WebToken GUID for the customer. 
        /// </summary>
        [DataMember(Order = 0)]
        public string WebToken { get; set; }
        /// <summary>
        /// The date and time that the WebToken was created.
        /// </summary>
        [DataMember(Order = 1)]
        public DateTime? CreatedUtcDateTime { get; set; }
        /// <summary>
        /// If customer doesn't exist in data warehouse then this will contain the details.
        /// </summary>
        [DataMember(Order = 3)]
        public string Status { get; set; }
        /// <summary>
        /// Anonymous user -- Customer Id
        /// </summary>
        [DataMember(Order = 4)]
        public string CustomerId { get; set; }
        /// <summary>
        /// Anonymous user -- Account Id
        /// </summary>
        [DataMember(Order = 5)]
        public string AccountId { get; set; }
        /// <summary>
        /// Anonymous user -- Premise Id
        /// </summary>
        [DataMember(Order = 6)]
        public string PremiseId { get; set; }
        /// <summary>
        /// Anonymous user -- Service Point Id
        /// </summary>
        [DataMember(Order = 7)]
        public string ServicePointId { get; set; }
    }
}