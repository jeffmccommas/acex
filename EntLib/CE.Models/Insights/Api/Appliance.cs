﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace CE.Models.Insights
{
    /// <summary>
    /// Contains data and analysis for a specific appliance
    /// </summary>
    [DataContract]
    public class Appliance
    {
        /// <summary>
        /// Default Constructor
        /// </summary>
        public Appliance()
        {
            DisaggDetails = new List<DisaggDetail>();
        }

        /// <summary>
        /// Identifier of the appliance. This is a key value that is used to retrieve appliance-related content from the CMS.
        /// </summary>
        [DataMember(Order = 0)]
        public string Key { get; set; }

        /// <summary>
        /// Disaggregation details (usage, costs and other measurements) for this appliance.
        /// </summary>
        [DataMember(Order = 2)]
        public List<DisaggDetail> DisaggDetails;

    }
}
