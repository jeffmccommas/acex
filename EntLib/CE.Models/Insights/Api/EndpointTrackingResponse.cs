﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using Newtonsoft.Json;

namespace CE.Models.Insights
{
    /// <summary>
    /// The EndpointTracking reponse includes the request parameters, endpoint tracking totals and (optionally) details of each API call.
    /// </summary>
    [DataContract]
    public class EndpointTrackingResponse : Status
    {
        /// <summary>
        /// The internal id of the client whose tracking details are returned.
        /// </summary>
        [DataMember(Order = 0)]
        public int? ClientId { get; set; }
        /// <summary>
        /// The name of the client whose tracking details are returned.
        /// </summary>
        [DataMember(Order = 1)]
        public string ClientName { get; set; }
        /// <summary>
        /// Requested start date and time of the tracking data.
        /// </summary>
        [DataMember(Order = 2)]
        public DateTime StartDate { get; set; }
        /// <summary>
        /// Requested end date and time of the tracking data.
        /// </summary>
        [DataMember(Order = 3)]
        public DateTime EndDate { get; set; }
        /// <summary>
        /// From the request: indicates whether or not to return details of each API call.
        /// </summary>
        [DataMember(Order = 4)]
        public bool IncludeDetails { get; set; }
        /// <summary>
        /// From the request: if specified, the API will return only API calls from this user.
        /// </summary>
        [DataMember(Order = 5)]
        public string UserId { get; set; }
        /// <summary>
        /// From the request: if specified, the API will return only API calls with this verb.
        /// </summary>
        [DataMember(Order = 6)]
        public string Verb { get; set; }
        /// <summary>
        /// From the request: if specified, the API will return only API calls to this endpoint.
        /// </summary>
        [DataMember(Order = 7)]
        public int EndpointId { get; set; }
        /// <summary>
        /// From the request: if specified, the API will return only API calls with this message id. 
        /// </summary>
        [DataMember(Order = 8)]
        public string MessageId { get; set; }
        /// <summary>
        /// From the request: indicates what "page" of response data to return, when the amount of data is too large to return in one call.
        /// </summary>
        [DataMember(Order = 9, EmitDefaultValue = false)]
        public int? PageIndex { get; set; }
        /// <summary>
        /// From the request: indicates what page size of response data to return.
        /// </summary>
        [DataMember(Order = 10, EmitDefaultValue = false)]
        public int? PageSize { get; set; }
        /// <summary>
        /// The total number of Insights API calls for this client.
        /// </summary>
        [DataMember(Order = 11)]
        public int TotalCalls { get; set; }
        /// <summary>
        /// A collection of totals broken down by endpoint, verb and user.
        /// </summary>
        [DataMember(Order = 12)]
        public List<EndpointTrackDetails> EndpointTotals { get; set; }
        /// <summary>
        /// Details of each API call, in reverse chronological order. This will be returned only if the request specified IncludeDetails = true. 
        /// </summary>
        [DataMember(Order = 13)]
        public List<EndpointTrackingDetails> TrackingDetails { get; set; }
    }

    /// <summary>
    /// Details of a single Insights API call. 
    /// </summary>
    [DataContract]
    public class EndpointTrackingDetails
    {
        [DataMember(Order = 0)]
        [JsonIgnore]
        public int EndpointTrackingID { get; set; }
        [DataMember(Order = 1)]
        [JsonIgnore]
        public int ClientID { get; set; }
        /// <summary>
        /// The id of the Insights API user
        /// </summary>
        [DataMember(Order = 2)]
        public string UserID { get; set; }
        /// <summary>
        /// The verb: GET, POST, PUT, or DELETE
        /// </summary>
        [DataMember(Order = 3)]
        public string Verb { get; set; }
        /// <summary>
        /// The name of the resource, including endpoint name and version.
        /// </summary>
        [DataMember(Order = 4)]
        public string ResourceName { get; set; }
        /// <summary>
        /// The internal id of the endpoint
        /// </summary>
        [DataMember(Order = 5)]
        public Nullable<int> EndpointID { get; set; }
        /// <summary>
        /// User Agent passed in the User-Agent header
        /// </summary>
        [DataMember(Order = 6)]
        public string UserAgent { get; set; }
        /// <summary>
        /// Source IP address
        /// </summary>
        [DataMember(Order = 7)]
        public string SourceIPAddress { get; set; }
        /// <summary>
        /// The message id of the request that was passed in the X-CE-MessageId custom header
        /// </summary>
        [DataMember(Order = 8)]
        public string XCEMessageId { get; set; }
        /// <summary>
        /// Information about the channel that was passed in the X-CE-Channel custom header
        /// </summary>
        [DataMember(Order = 9)]
        public string XCEChannel { get; set; }
        /// <summary>
        /// Information about the locale that was passed in the X-CE-Locale custom header
        /// </summary>
        [DataMember(Order = 10)]
        public string XCELocale { get; set; }
        [DataMember(Order = 11)]
        [JsonIgnore]
        public string XCEMeta { get; set; }
        /// <summary>
        /// The request parameters
        /// </summary>
        [DataMember(Order = 12)]
        public string Query { get; set; }
        [DataMember(Order = 13)]
        [JsonIgnore]
        public string MachineName { get; set; }
        [DataMember(Order = 14)]
        [JsonIgnore]
        public Nullable<bool> IncludeInd { get; set; }
        /// <summary>
        /// The date the call was logged.
        /// </summary>
        [DataMember(Order = 15)]
        public Nullable<DateTime> LogDate { get; set; }
        [DataMember(Order = 16)]
        [JsonIgnore]
        public DateTime NewDate { get; set; }
        [DataMember(Order = 17)]
        [JsonIgnore]
        public byte PartitionKey { get; set; }
    }
}
