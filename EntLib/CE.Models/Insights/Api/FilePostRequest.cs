﻿

namespace CE.Models.Insights
{
    public class FilePostRequest
    {

        /// <summary>
        /// File data to be posted.
        /// </summary>
        public FileDetail Filedetail { get; set; }
    }
}
