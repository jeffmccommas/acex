﻿using System;
using System.ComponentModel.DataAnnotations;

// ReSharper disable once CheckNamespace
namespace CE.Models.Insights
{
    /// <summary>
    /// BillRequest class.
    /// </summary>    
    public class GreenButtonAmiRequest
    {
        /// <summary>
        /// The ID of the utility customer.
        /// </summary>      
        [MaxLength(50)]
        [Required]
        public string CustomerId { get; set; }

        /// <summary>
        /// The ID of the utility customer's account. 
        /// If not provided then data for all of the customer's accounts will be returned.
        /// </summary>
        [MaxLength(50)]
        [Required]
        public string AccountId { get; set; }

        /// <summary>
        /// The ID of the utility customer's premise.
        /// </summary>
        [MaxLength(50)]
        [Required]
        public string PremiseId { get; set; }

        /// <summary>
        /// Comma-separated list of meter Ids  
        /// </summary>
        [Required]
        public string MeterIds { get; set; }

        /// <summary>
        /// Start date of the period to be exported in green button. Format is YYYY-MM-DD.      
        /// </summary>
        [Required]
        public DateTime StartDate { get; set; }
        /// <summary>
        /// End date of the period to be exported in green button. Format is YYYY-MM-DD.       
        /// </summary>
        [Required]
        public DateTime EndDate { get; set; }

        ///// <summary>
        ///// The commodity key of the meter.
        ///// </summary>
        //[MaxLength(20)]
        //public string CommodityKey { get; set; }

        ///// <summary>
        ///// The uom key of the meter.
        ///// </summary>
        //[MaxLength(10)]
        //public string UomKey { get; set; }

        /// <summary>
        /// Indicate whether to output a Zip file containing the XML file and stylesheet or to output XML for the meter. 
        /// Default to Zip if not provided
        /// Valid values: Zip or Xml
        /// </summary>
        [MaxLength(3)]
        [EnumDataType(typeof(Types.GreenButtonOutputFormatType))]
        public string OutputFormat { get; set; }
    }
}
