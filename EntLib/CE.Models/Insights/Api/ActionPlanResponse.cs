﻿using System.Runtime.Serialization;

namespace CE.Models.Insights
{
    /// <summary>
    /// The ActionPlan response.
    /// </summary>    
    [DataContract]
    public class ActionPlanResponse : Status
    {
        /// <summary>
        /// The id of the Client, typically a utility, who is using the API.
        /// </summary>
        //[JsonProperty(Order = 0)]       
        [DataMember(Order = 0)]
        public int? ClientId { get; set; }

        /// <summary>
        /// Action statuses for the customer.
        /// </summary>             
        [DataMember(Order = 1)]
        public ActionPlanCustomer Customer { get; set; }

        /// <summary>
        /// Content that is related to actions.
        /// </summary>                    
        [DataMember(Order = 2)]
        public Content Content { get; set; }
    }
}