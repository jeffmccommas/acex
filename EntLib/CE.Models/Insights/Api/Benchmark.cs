﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace CE.Models.Insights
{
    /// <summary>
    /// Cost, usage and other measurements for a commodity and benchmark group. Also includes the number of members in the group.
    /// </summary>
    [DataContract]
    public class Benchmark
    {
        /// <summary>
        /// default constructor
        /// </summary>
        public Benchmark()
        {
        }

        /// <summary>
        /// The start date of the date range.
        /// Format is YYYY-MM-DD.
        /// </summary>
        [DataMember(Order = 0)]
        public DateTime? StartDate { get; set; }

        /// <summary>
        /// The end date of the date range.
        /// Format is YYYY-MM-DD
        /// </summary>
        [DataMember(Order = 1)]
        public DateTime? EndDate { get; set; }

        /// <summary>
        /// Key for the commodity used, as defined in the content management system.
        /// </summary>
        [DataMember(Order = 2)]
        public string CommodityKey { get; set; }

        /// <summary>
        /// Key for the benchmark group, as defined in the content management system.
        /// </summary>
        [DataMember(Order = 3)]
        public string GroupKey { get; set; }

        /// <summary>
        /// The number of members in the benchmark group
        /// </summary>
        [DataMember(Order = 4)]
        public int GroupCount { get; set; }

        /// <summary>
        /// The usage for the specified premise.
        /// </summary>
        [DataMember(Order = 5)]
        public decimal MyUsage { get; set; }

        /// <summary>
        /// Average quantity used by all premises in the group.
        /// </summary>
        [DataMember(Order = 6)]
        public decimal AverageUsage { get; set; }

        /// <summary>
        /// Usage for efficient members of the group.
        /// </summary>
        [DataMember(Order = 7)]
        public decimal EfficientUsage { get; set; }

        /// <summary>
        /// Key for the UOM of the quantity, as defined in the content management system.
        /// </summary>
        [DataMember(Order = 8)]
        public string UOMKey { get; set; }

        /// <summary>
        /// How much the utility billed the customer for their premise's usage.
        /// </summary>
        [DataMember(Order = 9)]
        public decimal MyCost { get; set; }

        /// <summary>
        /// Average cost for all members of the group
        /// </summary>
        [DataMember(Order = 10)]
        public decimal AverageCost { get; set; }

        /// <summary>
        /// Cost for the efficient members of the group.
        /// </summary>
        [DataMember(Order = 11)]
        public decimal EfficientCost { get; set; }

        /// <summary>
        /// The key for the currency of the cost, as defined in the content management system.
        /// </summary>
        [DataMember(Order = 12)]
        public string CostCurrencyKey { get; set; }

        /// <summary>
        /// Other measurements (other than standard cost and usage) for the group.  
        /// </summary>
        [DataMember(Order = 13)]
        public List<BenchmarkMeasurement> Measurements;

    }
}
