﻿using System;
using System.Runtime.Serialization;

namespace CE.Models.Insights
{
    /// <summary>
    /// The WebToken response includes current WebToken data for a customer.
    /// </summary>
    [DataContract]
    public class WebTokenResponse : Status
    {
        /// <summary>
        /// Whether or not the WebToken is active. 
        /// </summary>
        [DataMember(Order = 0)]
        public bool IsActive { get; set; }
        /// <summary>
        /// The date and time that the WebToken was created.
        /// </summary>
        [DataMember(Order = 1)]
        public DateTime? CreatedUtcDateTime { get; set; }
        /// <summary>
        /// Additional Information that was stored with the WebToken.
        /// </summary>
        [DataMember(Order = 2)]
        public string AdditionalInfo { get; set; }

    }
}