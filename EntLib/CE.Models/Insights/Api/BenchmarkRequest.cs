﻿using System;
using System.ComponentModel.DataAnnotations;

namespace CE.Models.Insights
{
    /// <summary>
    /// Use this to request benchmark comparisons of usage, costs and other measurements.
    /// </summary>
    public class BenchmarkRequest
    {
        /// <summary>
        /// The ID of the utility customer.
        /// </summary>
        [Required]
        [MaxLength(50)]
        public string CustomerId { get; set; }

        /// <summary>
        /// Start date of the period to be analyzed. Format is YYYY-MM-DD.
        /// The API will return all benchmarks that overlap the specified period.
        /// </summary>
        [Required]
        public DateTime StartDate { get; set; }

        /// <summary>
        /// End date of the period to be analyzed. Format is YYYY-MM-DD.
        /// The API will return all benchmarks that overlap the specified period.
        /// </summary>
        [Required]
        public DateTime EndDate { get; set; }

        /// <summary>
        /// The ID of the utility customer's account.
        /// If this is not provided then the API will return benchmarks for all of the customer's accounts. 
        /// </summary>
        [MaxLength(50)]
        public string AccountId { get; set; }

        /// <summary>
        /// The ID of the utility customer's premise.
        /// If this is not provided then the API will return benchmarks for all of the premises for the account.
        /// </summary>
        [MaxLength(50)]
        public string PremiseId { get; set; }

        /// <summary>
        /// If specified, the response will return up to this number of the most recent benchmarks. 
        /// Defaults to 1 if not specified.
        /// </summary>
        [Range(0, 24)]
        public int Count { get; set; }

        /// <summary>
        /// A comma-separated list of keys for benchmark groups to compare. For example: default, zipcode.
        /// Defaults to "default" if not specified. 
        /// </summary>       
        [MaxLength(200)]
        public string GroupKeys { get; set; }

        /// <summary>
        /// A comma-separated list of keys for additional measurements to return.
        /// For example: CO2, Points
        /// </summary>
        [MaxLength(30)]
        public string MeasurementKeys { get; set; }

        /// <summary>
        /// Indicate whether or not to retrieve content from the content management system.
        /// Default is False if not specified.
        /// </summary>
        public bool IncludeContent { get; set; }
    }
}
