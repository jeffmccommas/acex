﻿using System.Runtime.Serialization;

namespace CE.Models.Insights
{
    /// <summary>
    /// Subscription Post Request
    /// </summary>
    [DataContract]
    public class SubscriptionPostRequest
    {
        /// <summary>
        /// Subscribed Insights for the customer
        /// </summary>
        [DataMember]
        public SubscribedCustomerInsight Customer { get; set; }
    }
}
