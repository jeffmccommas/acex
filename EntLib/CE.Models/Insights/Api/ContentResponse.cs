﻿using System.Runtime.Serialization;

namespace CE.Models.Insights
{
    /// <summary>
    /// The Content response includes content data and a message indicating whether a refresh was performed. 
    /// </summary>
    [DataContract]
    public class ContentResponse : Status
    {
        /// <summary>
        /// Content data
        /// </summary>
        [DataMember(Order = 1)]
        public Content Content { get; set; }
    }
}