﻿using System.ComponentModel.DataAnnotations;

namespace CE.Models.Insights
{
    /// <summary>
    /// Actionplan Request class.
    /// </summary>
    public class ActionPlanRequest
    {
        /// <summary>
        /// The ID of the utility customer.
        /// </summary>   
        [Required]
        [MaxLength(50)]
        public string CustomerId { get; set; }

        /// <summary>
        /// The ID of the utility customer's account.
        /// </summary>
        [MaxLength(50)]
        public string AccountId { get; set; }

        /// <summary>
        /// The ID of the utility customer's premise.
        /// </summary>
        [MaxLength(50)]
        public string PremiseId { get; set; }


    }
}
