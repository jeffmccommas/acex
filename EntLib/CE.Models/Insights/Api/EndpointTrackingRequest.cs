﻿using System;
using System.ComponentModel.DataAnnotations;

namespace CE.Models.Insights
{
    /// <summary>
    /// Details of a single API call
    /// </summary>
    public class EndpointTrackingRequest
    {
        /// <summary>
        /// Start date and time of the tracking data to return. Format is YYYY-DD-MMThh:mm:ssTZD, 
        /// where TZD is a time zone designator (Z, +hh:mm or -hh:mm). For example, use MM-DD-YYYYT00:00:00-05:00
        /// to retrieve data starting from the beginning of the day in eastern standard time, not shifted for DST. 
        /// If the time is omitted then it will be defaulted to the beginning of the day in the UTC time zone. 
        /// </summary>
        [Required]
        public DateTime StartDate { get; set; }
        /// <summary>
        /// Inclusive end date and time of the tracking data to return. Format is YYYY-DD-MMThh:mm:ssTZD, 
        /// where TZD is a time zone designator (Z, +hh:mm or -hh:mm). For example, use MM-DD-YYYYT23:59:59-05:00 
        /// to retrieve data up until the end of the day in eastern standard time, not shifted for DST. 
        /// If the time is omitted then it will be defaulted to the beginning of the day in the UTC time zone.
        /// </summary>
        [Required]
        public DateTime EndDate { get; set; }
        /// <summary>
        /// Indicates whether or not to return details of each API call. If true then the API will return totals and details. 
        /// If false then it will return totals only. Defaults to false if not specified.
        /// </summary>
        public bool IncludeDetails { get; set; }
        /// <summary>
        /// If specified, the response will include only API calls from this Insights API user.
        /// </summary>
        [Range(0, 50)]
        public int UserId { get; set; }
        /// <summary>
        /// If specified, the response will include only API calls with this verb (GET, PUT, POST or DELETE).
        /// </summary>
        [MaxLength(6)]
        public string Verb { get; set; }
        /// <summary>
        /// If specified, the response will include only API calls to this endpoint.
        /// </summary>
        [Range(0, 50)]
        public int EndpointId { get; set; }
        /// <summary>
        /// If specified, the resonse will include only API calls with this message id.
        /// </summary>
        [MaxLength(50)]
        public string MessageId { get; set; }
        /// <summary>
        /// When the amount of data retrieved is very large, the API will return it in chunks, or "pages" of data. 
        /// Use this parameter to indicates what page of response data to return. If not specified then it will default to 1. 
        /// </summary>
        [Range(0, 1000)]
        public int PageIndex { get; set; }
    }
}
