﻿using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace CE.Models.Insights
{
    public class FilesRequest
    {
        /// <summary>
        /// The ID of the utility customer.
        /// </summary>      
        [MaxLength(50)]
        [Required]
        [DataMember(Order = 1)]
        public string CustomerId { get; set; }

        /// <summary>
        /// The ID of the utility customer's account. 
        /// If not provided then bill data for all of the customer's accounts will be returned.
        /// </summary>
        [MaxLength(50)]
        [Required]
        [DataMember(Order = 2)]
        public string AccountId { get; set; }

        [MaxLength(50)]
        [Required]
        [DataMember(Order = 3)]
        [StringLength(50)]
        public string PremiseId { get; set; }

        /// <summary>
        /// The Role of User saving the report
        /// </summary>
        [DataMember(Order = 4)]
        public byte UserRole { get; set; }

        /// <summary>
        /// User ID.
        /// </summary>
        [DataMember(Order = 5)]
        public string UserId { get; set; }

    }
}
