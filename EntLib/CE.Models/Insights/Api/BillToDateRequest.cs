﻿using System.ComponentModel.DataAnnotations;

namespace CE.Models.Insights
{
    /// <summary>
    /// Bill to Date Request
    /// </summary>
    public class BillToDateRequest
    {/// <summary>
     /// The ID of the utility customer.
     /// </summary>      
        [MaxLength(50)]
        [Required]
        public string CustomerId { get; set; }

        /// <summary>
        /// The ID of the utility customer's account. 
        /// </summary>
        [MaxLength(50)]
        [Required]
        public string AccountId { get; set; }

        ///// <summary>
        ///// The ID of the utility customer's premise. 
        ///// </summary>
        //[MaxLength(50)]
        //[Required]
        //public string PremiseId { get; set; }

        /// <summary>
        /// Indicate whether or not to retrieve bill-to-date-related content from the content management system.
        /// Default is False if not specified.
        /// </summary>
        public bool IncludeContent { get; set; }

        /// <summary>
        /// If set to true then the bill-to-date will be re-calculated; 
        /// if false then the bill-to-date will be calculated only if there is no up-to-date calculation already available. 
        /// Defaults to false if not specified.
        /// </summary>
        public bool ForceCalc { get; set; }
        /// <summary>
        /// If set to true and ForceCalc is true, then calculation log will be return.
        /// Defaults to false if not specified.
        /// </summary>
        public bool ShowLog { get; set; }
    }
}
