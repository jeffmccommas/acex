﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Diagnostics;
using Newtonsoft.Json;

namespace CE.Models.Insights
{
    public class BillDisaggRequest
    {
        /// <summary>
        /// The ID of the utility customer.
        /// </summary>
        [Required]
        [MaxLength(50)]
        public string CustomerId { get; set; }
        /// <summary>
        /// The start date of the period to be analyzed. Format is YYYY-MM-DD.
        /// </summary>
        [Required]
        public DateTime StartDate { get; set; }
        /// <summary>
        /// The end date of the period to be analyzed. Format is YYYY-MM-DD.
        /// </summary>
        [Required]
        public DateTime EndDate { get; set; }
        /// <summary>
        /// The ID of the utility customer's account.
        /// If this is not specified then all of the customer's accounts will be included.
        /// </summary>
        [Required]
        [MaxLength(50)]
        public string AccountId { get; set; }
        /// <summary>
        /// The ID of the utility customer's premise.
        /// If this is not specified then all of the customer's premises will be included.
        /// </summary>
        [Required]
        [MaxLength(50)]
        public string PremiseId { get; set; }
        /// <summary>
        /// Comma-separated list of additional measurement keys to return.  
        /// If not specified then no additional measurements will be returned.
        /// </summary>
        [MaxLength(15)]
        public string MeasurementKeys { get; set; }
        /// <summary>
        /// Indicates that a calculation shall be made, regardless whether a previous result was cached.
        /// </summary>
        public bool ForceCalc { get; set; }
        /// <summary>
        /// Indicates that the customer premise shall be assumed to be a business premise.
        /// A building category and total area shall be retrieved, and those will be default values if no business profile attributes exist for this customer.
        /// </summary>
        public bool ForceBusiness { get; set; }
        /// <summary>
        /// Indicate whether or not to retrieve content from the content management system.
        /// Default is False if not specified.
        /// </summary>
        public bool IncludeContent { get; set; }
    }

}
