﻿using System;
using System.Runtime.Serialization;

namespace CE.Models.Insights
{
    /// <summary>
    /// The WebToken response includes current WebToken data for a customer.
    /// </summary>
    [DataContract]
    public class RegisterPostResponse : StatusOAuth
    {
        /// <summary>
        /// The client application's id.
        /// </summary>
        [DataMember(Order = 0)]
        public long client_id { get; set; }
        /// <summary>
        /// client secret
        /// </summary>
        [DataMember(Order = 1)]
        public string client_secret { get; set; }
        /// <summary>
        /// If client is active.
        /// </summary>
        [DataMember(Order = 2)]
        public bool active { get; set; }

    }
}

