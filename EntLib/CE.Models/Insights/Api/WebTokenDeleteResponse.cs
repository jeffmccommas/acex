﻿using System.Runtime.Serialization;

namespace CE.Models.Insights
{
    /// <summary>
    /// The WebToken response includes current WebToken data for a customer.
    /// </summary>
    [DataContract]
    public class WebTokenDeleteResponse : Status
    {
        /// <summary>
        /// Whether or not the WebToken is active. 
        /// </summary>
        [DataMember(Order = 0)]
        public bool Success { get; set; }

    }
}