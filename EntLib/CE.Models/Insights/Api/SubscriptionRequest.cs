﻿using System;
using System.ComponentModel.DataAnnotations;

namespace CE.Models.Insights
{
    /// <summary>
    /// SubscriptionRequest Class
    /// </summary>
    public class SubscriptionRequest
    {
        /// <summary>
        /// The ID of the utility customer.
        /// </summary>      
        [MaxLength(50)]
        [Required]
        public string CustomerId { get; set; }

        /// <summary>
        /// The ID of the utility customer's account. 
        /// </summary>
        [MaxLength(50)]
        [Required]
        public string AccountId { get; set; }

        /// <summary>
        /// Indicate whether or not to retrieve subscription-related content from the content management system.
        /// Default is False if not specified.
        /// </summary>
        public bool IncludeContent { get; set; }
    }
}
