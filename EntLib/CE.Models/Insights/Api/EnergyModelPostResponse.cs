﻿using System.Runtime.Serialization;

namespace CE.Models.Insights
{
    [DataContract]
    public class EnergyModelPostResponse : Status
    {
        /// <summary>
        /// Response parameters.
        /// </summary>
        [DataMember(Order = 0, Name = "Data")]
        public EMResponseContainer Data { get; set; }

    }
}
