﻿using System.ComponentModel.DataAnnotations;

// ReSharper disable once CheckNamespace
namespace CE.Models.Insights
{
    /// <summary>
    /// WebToken Post Request class.
    /// </summary>
    public class WebTokenPostRequest
    {
        /// <summary>
        /// The ID of the utility customer.
        /// </summary>
        [MaxLength(50)]
        public string CustomerId { get; set; }

        /// <summary>
        /// Key:Value pairs that may be used to pass additional Information for application use. 
        /// For example, pre-selected account or premise ids.
        /// </summary>
        public string AdditionalInfo { get; set; }

        /// <summary>
        /// The identifier of the user. This is required only if the user is accessing role-specific functionality, such as CSR screens. 
        /// </summary>
        public string UserId { get; set; }

        /// <summary>
        /// The name of the group to which the user belongs. This is required if the user is accessing role-specific functionality, such as CSR screens. 
        /// If the group is missing or invalid, the user will see the default customer-facing website. 
        /// </summary>
        public string GroupName { get; set; }

        /// <summary>
        /// The name of the user’s role within their group. This is required if the user is accessing role-specific functionality, such as CSR screens. 
        /// If the role is missing or invalid, the user will see the default customer-facing website.
        /// </summary>
        public string RoleName { get; set; }

        /// <summary>
        /// Required for anonymous user signup and authentication. It must be in a valid email format.
        /// </summary>
        public string EmailAddress { get; set; }

        /// <summary>
        /// Zipcode is used to create a new anonymous user. It has to be in a valid US and Canadian zip code format and must be within client's service area.
        /// It must be blank for anonymous user authentication and populated for new anonymous user signup.
        /// </summary>
        public string ZipCode { get; set; }
       
    }
}
