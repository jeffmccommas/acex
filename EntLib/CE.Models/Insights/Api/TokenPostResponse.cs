﻿using System;
using System.Runtime.Serialization;

namespace CE.Models.Insights
{
    /// <summary>
    /// The WebToken response includes current WebToken data for a customer.
    /// </summary>
    [DataContract]
    public class TokenPostResponse : StatusOAuth
    {
        /// <summary>
        /// Access token as assigned by the authorization server.
        /// </summary>
        [DataMember(Order = 0)]
        public string access_token { get; set; }
        /// <summary>
        /// Type of token assigned by the authorization server.
        /// </summary>
        [DataMember(Order = 1)]
        public string token_type { get; set; }
        /// <summary>
        /// Is a number of seconds after which the access token expires, and is no longer valid. Expiration of access tokens is optional.
        /// </summary>
        [DataMember(Order = 2)]
        public long? expires_in { get; set; }
        /// <summary>
        ///Contains a refresh token in case the access token can expire. The refresh token is used to obtain a new access token once the one returned in this response is no longer valid.
        /// </summary>
        [DataMember(Order = 3)]
        public string refresh_token { get; set; }
        /// <summary>
        ///Contains a refresh token in case the access token can expire. The refresh token is used to obtain a new access token once the one returned in this response is no longer valid.
        /// </summary>
        [DataMember(Order = 4)]
        public string scope { get; set; }
        /// <summary>
        ///Contains resource url
        /// </summary>
        [DataMember(Order = 5)]
        public string resourceURI { get; set; }
        /// <summary>
        ///Contains authorization url
        /// </summary>
        [DataMember(Order = 6)]
        public string authorizationURI { get; set; }
        /// <summary>
        ///Status of access token
        /// </summary>
        [DataMember(Order = 7)]
        public byte? status { get; set; }

    }
}

