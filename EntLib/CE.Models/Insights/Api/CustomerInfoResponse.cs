﻿using System.Runtime.Serialization;

namespace CE.Models.Insights
{
    /// <summary>
    /// Consumption data for the utility customer
    /// </summary>
    [DataContract]
    public class CustomerInfoResponse : Status
    {
        /// <summary>
        /// The id of the Client, typically a utility, who is using the API.
        /// </summary>
        [DataMember(Order = 0)]
        public int? ClientId { get; set; }
       
        /// <summary>
        /// Customer Info.
        /// </summary>
        [DataMember(Order = 1)]
        public CustomerInfo Customer { get; set; }

        /// <summary>
        ///Content data.
        /// </summary>     
        [DataMember(Order = 2)]
        public Content Content { get; set; }

    }
}