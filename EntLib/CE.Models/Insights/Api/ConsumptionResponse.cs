﻿using System.Collections.Generic;
using System.Runtime.Serialization;

// ReSharper disable once CheckNamespace
namespace CE.Models.Insights
{
    /// <summary>
    /// Consumption data for the utility customer
    /// </summary>
    [DataContract]
    public class ConsumptionResponse : Status
    {
        /// <summary>
        /// The id of the Client, typically a utility, who is using the API.
        /// </summary>
        [DataMember(Order = 0)]
        public int? ClientId { get; set; }

        /// <summary>
        /// Id of the utility customer.
        /// </summary>
        [DataMember(Order = 1)]
        public string CustomerId { get; set; }

        /// <summary>
        /// Id of the utility account.
        /// </summary>
        [DataMember(Order = 2)]
        public string AccountId { get; set; }

        /// <summary>
        /// Id of the utility premise.
        /// </summary>
        [DataMember(Order = 3)]
        public string PremiseId { get; set; }

        /// <summary>
        /// the Meter Ids returned
        /// </summary>
        [DataMember(Order = 4)]
        public string MeterIds { get; set; }

        /// <summary>
        /// Key value for the interval resolution of the consumption data returned.  
        /// </summary>
        [DataMember(Order = 5)]
        public string ResolutionKey { get; set; }

        /// <summary>
        /// Key value for the interval resolution of the consumption data returned.  
        /// </summary>
        [DataMember(Order = 6)]
        public string ResolutionsAvailable { get; set; }

        /// <summary>
        /// Key value for the commodity of the consumption data returned.
        /// </summary>
        [DataMember(Order = 7)]
        public string CommodityKey { get; set; }

        /// <summary>
        /// Key value for the unit of measure for the consumption data. 
        /// </summary>
        [DataMember(Order = 8)]
        // ReSharper disable once InconsistentNaming
        public string UOMKey { get; set; }

        /// <summary>
        ///Consumption data.
        /// </summary>
        [DataMember(Order = 9, EmitDefaultValue = false)]
        public decimal? AverageUsage { get; set; }

        /// <summary>
        /// Indicates the page size of response data returned. 
        /// If the size of the data returned equals this page size then there is more data available via pagination. 
        /// To retrieve the next set of data, issue a new request with PageIndex set to the next page. 
        /// </summary>
        [DataMember(Order = 10, EmitDefaultValue = false)]
        public int? PageSize { get; set; }

        /// <summary>
        /// From the request: indicates what "page" of response data to return
        /// when the amount of data is too large to return in one call.
        /// </summary>
        [DataMember(Order = 11, EmitDefaultValue = false)]
        public int? PageIndex { get; set; }

        /// <summary>
        ///Consumption data.
        /// </summary>
        [DataMember(Order = 12)]
        public List<ConsumptionData> Data { get; set; }


        /// <summary>
        ///Consumption data.
        /// </summary>
        [DataMember(Order = 13, EmitDefaultValue = false)]
        public List<ConsumptionWeather> Weather { get; set; }


        /// <summary>
        ///Content data.
        /// </summary>     
        [DataMember(Order = 14)]
        public Content Content { get; set; }

        [DataMember(Order = 15)]
        public string MiscInfo { get; set; }

    }
}