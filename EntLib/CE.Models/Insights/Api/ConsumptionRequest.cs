﻿using System;
using System.ComponentModel.DataAnnotations;

// ReSharper disable once CheckNamespace
namespace CE.Models.Insights
{
    /// <summary>
    /// ConsumptionRequest class.
    /// </summary>
    public class ConsumptionRequest
    {
        /// <summary>
        /// The ID of the utility customer.
        /// </summary>      
        [MaxLength(50)]
        [Required]
        public string CustomerId { get; set; }

        /// <summary>
        /// The ID of the utility account. 
        /// </summary>
        [MaxLength(50)]
        [Required]
        public string AccountId { get; set; }

        /// <summary>
        /// The ID of the utility premise. 
        /// </summary>
        [MaxLength(50)]
        [Required]
        public string PremiseId { get; set; }

        /// <summary>
        /// The ID of the utility service point.        
        /// </summary>
        //[MaxLength(50)]
        //Removed Max Length as we might now have multiple comma seperated service point ids 
        public string ServicePointId { get; set; }

        /// <summary>
        /// Comma-seperated list of meter Ids  
        /// </summary>
        //[Required]
        //Changed to optional as some clients will be sending in ServicePointIds instead of Meter Ids
        public string MeterIds { get; set; }

        /// <summary>
        /// Start date of the period to return. 
        /// Format is YYYY-DD-MMThh:mm:ssTZD, where TZD is a time zone designator (Z, +hh:mm or -hh:mm)   
        /// Date should be pass in UTC time    
        /// </summary>
        [Required]
        public DateTime StartDate { get; set; }

        /// <summary>
        /// End date of the period to return, and it's not inclusive
        /// Format is YYYY-DD-MMThh:mm:ssTZD, where TZD is a time zone designator (Z, +hh:mm or -hh:mm)     
        /// Date should be pass in UTC time   
        /// </summary>
        [Required]
        public DateTime EndDate { get; set; }

        /// <summary>
        /// Number of intervals to return - will return the most recent intervals availabe. If not specified then will return all intervals in the date range.
        /// </summary>
        public int Count { get; set; }

        /// <summary>
        /// Key value for the interval resolution to return. 
        /// If the data is stored at a smaller resolution then it will be summed and returned in the requested resolution. 
        /// If the data is stored at a larger resolution then there will be an error response.
        /// If not specified then it will return the data in the smallest resolution available. 
        /// </summary>       
        public string ResolutionKey { get; set; }

        /// <summary>
        /// Indicate whether or not to retrieve consumption-related content from the content management system.
        /// Default is Gas if not specified.
        /// </summary>
        /// 
        public string CommodityKey { get; set; }

        /// <summary>
        /// Indicate whether or not to return a list of available resolutions.  
        /// Default to false if not specified.
        /// </summary>
        public bool IncludeResolutions { get; set; }

        /// <summary>
        /// Indicate whether or not to retrieve consumption-related content from the content management system.
        /// Default is Gas if not specified.
        /// </summary>
        public bool IncludeContent { get; set; }

        /// <summary>
        /// When the amount of data retrieved is very large, the API will return it in chunks, or "pages" of data. 
        /// Use this parameter to indicates what page of response data to return. If not specified then it will default to 1. 
        /// </summary>
        [Range(0, 1000)]
        public int PageIndex { get; set; }

        /// <summary>
        /// Indicate whether or not to return weather data.
        /// Default to false if not specified.
        /// </summary>
        public bool IncludeWeather { get; set; }

        /// <summary>
        /// The Zip Code for the Weather Data.
        /// Required if IncludeWeather is true.
        /// </summary>
        public string ZipCode { get; set; }
        /// <summary>
        /// The Country for the zip code.
        /// Required if IncludeWeather is true.
        /// Valid values: US or CA
        /// </summary>
        public string Country { get; set; }

        /// <summary>
        /// Indicate whether or not to return average usage.
        /// Default to false if not specified.
        /// </summary>
        public bool IncludeAverageUsage { get; set; }

        public string AdvAmi { get; set; }

        public string advAmiRoot { get; set; }

        public string adjusttimezone { get; set; }

        public string timezone { get; set; }

        public string buckettype { get; set; }
        public string bucketsize { get; set; }


    }
}
