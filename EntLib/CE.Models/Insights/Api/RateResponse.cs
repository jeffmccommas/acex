﻿using System.Runtime.Serialization;

namespace CE.Models.Insights
{
    /// <summary>
    /// Rate information
    /// </summary>
    [DataContract]
    public class RateResponse : Status
    {
        /// <summary>
        /// The id of the Client, typically a utility, who is using the API.
        /// </summary>
        [DataMember(Order = 0)]
        public int? ClientId { get; set; }

        /// <summary>
        /// The rate class.
        /// </summary>
        [DataMember(Order = 1)]
        public RateClass RateClass { get; set; }


        /// <summary>
        /// Rate class details including use charges, service charges, tier boundaries, time-of-use boundaries, season boundaries, 
        /// minimum charges and tax charges.
        /// </summary>
        [DataMember(Order = 2)]
        public RateDetails Details { get; set; }

        /// <summary>
        ///Content data
        /// </summary>     
        [DataMember(Order = 3)]
        public Content Content { get; set; }
    }
}
