﻿using System.Runtime.Serialization;

namespace CE.Models.Insights
{
    /// <summary>
    /// Bill data for the utility customer
    /// </summary>
    [DataContract]
    public class GreenButtonAmiResponse : Status
    {
        /// <summary>
        /// The id of the Client, typically a utility, who is using the API.
        /// </summary>
        [DataMember(Order = 0)]
        public int? ClientId { get; set; }

        /// <summary>
        /// Billing data for the customer.
        /// </summary>
        [DataMember(Order = 1)]
        public GreenButtonAmiCustomer Customer { get; set; }

        /// <summary>
        /// Bill-related content data.
        /// </summary>     
        [DataMember(Order = 2)]
        public Content Content { get; set; }

    }
}