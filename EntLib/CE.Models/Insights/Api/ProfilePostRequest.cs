﻿

namespace CE.Models.Insights
{
    public class ProfilePostRequest
    {
        /// <summary>
        /// Profile data for the customer and their accounts and premises.
        /// </summary>
        public ProfileCustomer Customer { get; set; }
        /// <summary>
        /// If set to True then the POST will validate the profile attribute key, profile attribute value, and source key. 
        /// If False then it will not validate these values beyond that they are required. Defaults to False. 
        /// </summary>
        public bool Validate { get; set; }
    }
}
