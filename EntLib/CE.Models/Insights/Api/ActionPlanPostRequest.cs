﻿

namespace CE.Models.Insights
{
    public class ActionPlanPostRequest
    {
        /// <summary>
        /// Actions status information for the customer.
        /// </summary>
        public ActionPlanCustomer Customer { get; set; }
        /// <summary>
        /// If set to True then the POST will validate the ActionKey, Status, and SourceKey values. 
        /// If False then they will not be validated, other than that they are required. 
        /// Defaults to false. 
        /// </summary>
        public bool Validate { get; set; }
    }
}
