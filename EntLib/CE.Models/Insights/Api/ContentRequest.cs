﻿using System.ComponentModel.DataAnnotations;

// ReSharper disable once CheckNamespace
namespace CE.Models.Insights
{
    /// <summary>
    /// Content request.
    /// </summary>
    public class ContentRequest
    {
        /// <summary>
        /// If set to true, this value will trigger a content refresh into the database and clear cached content.
        /// If false then this refresh does not happen. 
        /// </summary>
        [Required]
        public bool Refresh { get; set; }

        /// <summary>
        /// If specified, will only return content with this type.
        /// If Category or Keys are specified then Type must also be specified. 
        /// </summary>
        [MaxLength(50)]
        public string Type { get; set; }

        /// <summary>
        /// If specified, will only return content with this category and the "common" category.
        /// If Category is specified then Type must also be specified. 
        /// </summary>
        [MaxLength(50)]
        public string Category { get; set; }

        /// <summary>
        /// A comma-separated list of content keys. Do not put a space after the comma. If specified, will return only content with these keys.
        /// If keys are specified then the Type must also be specified.
        /// </summary>       
        [MaxLength(200)]
        public string Keys { get; set; }

        /// <summary>
        /// Indicate whether or not to retrieve content from the content management system.
        /// Default is False if not specified.
        /// </summary>
        public bool IncludeContent { get; set; }

        /// <summary>
        /// if specified, it will return all the profile defaults with the collection name merged with aclara's
        /// sprint 4
        /// </summary>
        [MaxLength(256)]
        public string ProfileCollectionKey { get; set; }

    }
}
