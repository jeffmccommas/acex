﻿using System.Runtime.Serialization;

namespace CE.Models.Insights
{
    /// <summary>
    /// The Echo response outputs the value passed in the "Say" parameter.
    /// </summary>
    [DataContract]
    public class EchoResponse : Status
    {
        /// <summary>
        /// The value passed in the Say parameter
        /// </summary>
        [DataMember(Order = 0)]
        public string Echo { get; set; }
    }
}