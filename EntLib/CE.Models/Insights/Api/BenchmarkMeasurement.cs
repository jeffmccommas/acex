﻿using System.Runtime.Serialization;

namespace CE.Models.Insights
{
    /// <summary>
    /// Measurements other than the price or quantity. 
    /// </summary>
    [DataContract]
    public class BenchmarkMeasurement
    {
        /// <summary>
        /// Identifier of the benchmark measurement. This is a key value for retrieving benchmark-measurement-specific content from the CMS.
        /// </summary>
        [DataMember(Order = 0)]
        public string Key { get; set; }

        /// <summary>
        /// Quantity that was measured or calculated for the specified premise.
        /// </summary>
        [DataMember(Order = 1)]
        public decimal MyQuantity { get; set; }

        /// <summary>
        /// Average quantity for all premises in the group.
        /// </summary>
        [DataMember(Order = 2)]
        public decimal AverageQuantity { get; set; }

        /// <summary>
        /// Quantity for efficient members of the group. 
        /// </summary>
        [DataMember(Order = 3)]
        public decimal EfficientQuantity { get; set; }

        /// <summary>
        /// Key for the measurement units as defined in the content management system.
        /// </summary>
        [DataMember(Order = 4)]
        public string UOMKey { get; set; }
    }
}