﻿using System;
using System.ComponentModel.DataAnnotations;

namespace CE.Models.Insights
{
    /// <summary>
    /// BillRequest class.
    /// </summary>
    public class BillRequest
    {
        /// <summary>
        /// The ID of the utility customer.
        /// </summary>      
        [MaxLength(50)]
        [Required]
        public string CustomerId { get; set; }

        /// <summary>
        /// The ID of the utility customer's account. 
        /// If not provided then bill data for all of the customer's accounts will be returned.
        /// </summary>
        [MaxLength(50)]
        public string AccountId { get; set; }

        /// <summary>
        /// Start date of the period to be analyzed. Format is YYYY-MM-DD.      
        /// </summary>
        [Required]
        public DateTime StartDate { get; set; }
        /// <summary>
        /// End date of the period to be analyzed. Format is YYYY-MM-DD.       
        /// </summary>
        [Required]
        public DateTime EndDate { get; set; }

        /// <summary>
        /// If specified, the response will return up to this number of the most recent bills. 
        /// Defaults to 1 if not specified.
        /// </summary>
        [Range(0, 24)]
        public int Count { get; set; }

        /// <summary>
        /// Indicate whether or not to retrieve bill-related content from the content management system.
        /// Default is False if not specified.
        /// </summary>
        public bool IncludeContent { get; set; }
    }
}
