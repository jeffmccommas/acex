﻿using System.Runtime.Serialization;

namespace CE.Models.Insights
{
    /// <summary>
    /// The Profile response includes current profile data for a customer, account and/or premise.
    /// </summary>
    [DataContract]
    public class ProfileResponse : Status
    {
        /// <summary>
        /// The id of the Client, typically a utility, who is using the API.
        /// </summary>
        [DataMember(Order = 0)]
        public int? ClientId { get; set; }

        /// <summary>
        /// Profile data for the customer, their accounts and their premises.
        /// </summary>
        [DataMember(Order = 1)]
        public ProfileCustomer Customer { get; set; }

        /// <summary>
        /// Profile-related content from the content management system.
        /// </summary>
        [DataMember(Order = 2)]
        public Content Content { get; set; }

    }
}