﻿using System.ComponentModel.DataAnnotations;

namespace CE.Models.Insights
{
    /// <summary>
    /// WebToken Request class.
    /// </summary>
    public class RegisterPostRequest
    {
        /// <summary>
        /// The client application's id.
        /// </summary>
        public long client_id { get; set; }

        /// <summary>
        /// The client name.
        /// </summary>
        [Required]
        [MaxLength(255)]
        public string client_name { get; set; }

        /// <summary>
        /// The client description.
        /// </summary>
        [MaxLength(2000)]
        public string client_description { get; set; }

        /// <summary>
        ///  Must be set to authorization_code.
        /// </summary> 
        [Required]
        [MaxLength(80)]
        public string grant_type { get; set; }

        /// <summary>
        /// The possible scope of the request.
        /// </summary>
        [Required]
        [MaxLength(2000)]
        public string scope { get; set; }

        /// <summary>
        /// Required, If the request URI was included in the authorization request.Must be identical then.
        /// </summary>
        [Required]
        [MaxLength(2000)]
        public string redirect_uri { get; set; }

        /// <summary>
        /// If client is active.
        /// </summary>
        public bool active { get; set; }


        [Required]
        [MaxLength(2080)]
        public string logo_uri { get; set; }
        [Required]
        [MaxLength(2080)]
        public string client_uri { get; set; }
        [Required]
        [MaxLength(2080)]
        public string tos_uri { get; set; }
        [Required]
        [MaxLength(2080)]
        public string policy_uri { get; set; }
        [Required]
        [MaxLength(50)]
        public string software_id { get; set; }
        [Required]
        [MaxLength(50)]
        public string software_version { get; set; }
        [Required]
        [MaxLength(500)]
        public string contacts { get; set; }
        [Required]
        [MaxLength(50)]
        public string token_endpoint_auth_method { get; set; }
        [Required]
        [MaxLength(250)]
        public string grant_types { get; set; }
        [Required]
        [MaxLength(250)]
        public string response_types { get; set; }
        [Required]
        [MaxLength(1000)]
        public string third_party_application_description { get; set; }
        [Required]
        [MaxLength(2)]
        public string third_party_application_status { get; set; }
        [Required]
        [MaxLength(2)]
        public string third_party_application_type { get; set; }
        [Required]
        [MaxLength(2)]
        public string third_party_application_use { get; set; }
        [Required]
        [MaxLength(16)]
        public string third_party_phone { get; set; }
        [Required]
        [MaxLength(2080)]
        public string third_party_notify_uri { get; set; }
        [Required]
        [MaxLength(2080)]
        public string third_party_data_custodian_selection_screen_uri { get; set; }
        [Required]
        [MaxLength(2080)]
        public string third_party_login_screen_uri { get; set; }
        [Required]
        [MaxLength(2080)]
        public string third_party_scope_selection_screen_uri { get; set; }
        [Required]
        [MaxLength(2080)]
        public string third_party_user_portal_screen_uri { get; set; }

    }
}

