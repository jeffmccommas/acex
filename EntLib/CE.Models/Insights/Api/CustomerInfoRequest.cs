﻿using System.ComponentModel.DataAnnotations;

namespace CE.Models.Insights
{
    /// <summary>
    /// ConsumptionRequest class.
    /// </summary>
    public class CustomerInfoRequest
    {
        /// <summary>
        /// The ID of the utility customer.
        /// </summary>      
        [MaxLength(50)]
        [Required]
        public string CustomerId { get; set; }

        /// <summary>
        /// Indicate whether or not to retrieve customer-related content from the content management system. 
        /// Default is False if not specified.
        /// </summary>
        public bool IncludeContent { get; set; }
    }
}
