﻿using System.ComponentModel.DataAnnotations;

// ReSharper disable once CheckNamespace
namespace CE.Models.Insights
{
    /// <summary>
    /// Request entity to Delete file
    /// </summary>
    public class FileRequest
    {

         /// <summary>
        /// The ID of the File.
        /// </summary>
        [Required]
        public int FileId { get; set; }



    }
}
