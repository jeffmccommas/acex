﻿using System;
using System.ComponentModel.DataAnnotations;

namespace CE.Models.Insights
{
    /// <summary>
    /// ActionRequest class.
    /// </summary>
    public class ActionRequest
    {
        /// <summary>
        /// The ID of the utility customer.
        /// </summary>      
        [MaxLength(50)]
        [Required]
        public string CustomerId { get; set; }

        /// <summary>
        /// The ID of the utility customer's account.
        /// </summary>
        [MaxLength(50)]
        [Required]
        public string AccountId { get; set; }

        /// <summary>
        /// The ID of the utility customer's premise.
        /// </summary>
        [MaxLength(50)]
        [Required]
        public string PremiseId { get; set; }

        /// <summary>
        /// Comma-separated list of commodity keys. If specified then only actions related to these commodites will be returned. 
        /// If not specified then all commodities will be included. 
        /// </summary>
        [MaxLength(200)]
        public string CommodityKeys { get; set; }

        /// <summary>
        /// Comma-separated list of action types to return.
        /// If not specified then all types will be included. 
        /// </summary>
        [MaxLength(200)]
        public string Types { get; set; }

        /// <summary>
        /// Comma-separated list of end use keys. If specified then only actions related to these end uses will be returned.
        /// If not specified then all end uses will be included.
        /// </summary>
        [MaxLength(200)]
        public string EndUseKeys { get; set; }

        /// <summary>
        /// Comma-separated list of appliances keys. If specified then only actions related to these appliances will be returned.
        /// If not specified then all appliances will be included.
        /// </summary>
        [MaxLength(200)]
        public string ApplianceKeys { get; set; }

        /// <summary>
        /// Indicates that measure calculations shall be made, regardless whether a previous result was cached.
        /// </summary>
        public bool ForceCalc { get; set; }

        /// <summary>
        /// Indicate whether or not to retrieve action-related content from the content management system.
        /// Default is False if not specified.
        /// </summary>
        public bool IncludeContent { get; set; }

        /// <summary>
        /// Latest Billdate to compare again . Format is YYYY-MM-DD.
        /// </summary>
        public DateTime LatestBillDate { get; set; }


        /// <summary>
        /// Past Billdate to compare again. Format is YYYY-MM-DD.
        /// </summary>
        public DateTime PreviousBillDate { get; set; }
    }

}
