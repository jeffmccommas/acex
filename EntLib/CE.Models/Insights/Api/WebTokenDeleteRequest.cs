﻿using System.ComponentModel.DataAnnotations;

namespace CE.Models.Insights
{
    /// <summary>
    /// WebToken Request class.
    /// </summary>
    public class WebTokenDeleteRequest
    {
        /// <summary>
        /// The ID of the utility customer.
        /// </summary>
        [Required]
        [MaxLength(50)]
        public string CustomerId { get; set; }

        /// <summary>
        /// WebToken data for the customer for validation.
        /// </summary>
        [Required]
        [MaxLength(50)]
        public string WebToken { get; set; }

    }
}
