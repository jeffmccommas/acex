﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace CE.Models.Insights
{
    /// <summary>
    /// Data and analysis for the End Use
    /// </summary>
    [DataContract]
    public class EndUse
    {
        /// <summary>
        /// Default Constructor
        /// </summary>
        public EndUse()
        {
            Appliances = new List<Appliance>();
        }

        /// <summary>
        /// Identifier of the end use. This is a key value that is used to retrieve end use-related content from the CMS.
        /// </summary>
        [DataMember(Order = 0)]
        public string Key { get; set; }

        /// <summary>
        /// Cost, usage and other measurements for the end use.
        /// </summary>
        [DataMember(Order = 1)]
        public List<DisaggDetail> DisaggDetails;

        /// <summary>
        /// Cost, usage and other measurements for each appliance that is mapped to this end use.
        /// </summary>
        [DataMember(Order = 2)]
        public List<Appliance> Appliances;

    }

}
