﻿using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace CE.Models.Insights
{
    /// <summary>
    /// WebToken Request class.
    /// </summary>
    public class WebTokenRequest
    {
        /// <summary>
        /// The ID of the utility customer.
        /// </summary>
        [Required]
        [MaxLength(50)]
        public string CustomerId { get; set; }

        /// <summary>
        /// WebToken for the customer.
        /// </summary>
        [Required]
        [MaxLength(50)]
        public string WebToken { get; set; }

        /// <summary>
        /// If set to True, the API will return any Additional Information key:value pairs that were stored with the WebToken.
        /// If set to False, the API will not return any additional information.
        /// </summary>
        public bool IncludeAdditionalInfo { get; set; }

        /// <summary>
        /// The identifier of the user. This is required if the user is accessing role-specific functionality, such as CSR screens. 
        /// </summary>
        public string UserId { get; set; }

        /// <summary>
        /// The name of the group to which the user belongs. This is required if the user is accessing role-specific functionality, such as CSR screens. 
        /// If the group is missing or invalid, the user will see the default customer-facing website. 
        /// </summary>
        public string GroupName { get; set; }

        /// <summary>
        /// The name of the user’s role within their group. This is required if the user is accessing role-specific functionality, such as CSR screens. 
        /// If the role is missing or invalid, the user will see the default customer-facing website.
        /// </summary>
        public string RoleName { get; set; }
    }
}
