﻿using System.Runtime.Serialization;

// ReSharper disable once CheckNamespace
namespace CE.Models.Insights
{
    [DataContract]
    public class GreenButtonConnectRequest
    {
        /// <summary>
        /// Not used.
        /// </summary>
        [DataMember(Name = "published-max")]
        public string PublishedMax { get; set; }
        /// <summary>
        /// Not used.
        /// </summary>
        [DataMember(Name = "published-min")]
        public string PublishedMin { get; set; }
        /// <summary>
        /// Not used.
        /// </summary>
        [DataMember(Name = "updated-max")]
        public string UpdatedMax { get; set; }
        /// <summary>
        /// Not used.
        /// </summary>
        [DataMember(Name = "updated-min")]
        public string UpdatedMin { get; set; }
        /// <summary>
        /// Not used.
        /// </summary>
        [DataMember(Name = "max-result")]
        public string MaxResult { get; set; }
        /// <summary>
        /// Not used.
        /// </summary>
        [DataMember(Name = "start-index")]
        public string StartIndex { get; set; }
        /// <summary>
        /// Not used.
        /// </summary>
        [DataMember(Name = "depth")]
        public string Depth { get; set; }
    }
}
