﻿using System.Runtime.Serialization;
namespace CE.Models.Insights
{
    /// <summary>
    /// Subscription Post Response
    /// </summary>
    public class SubscriptionPostResponse : Status
    {

        /// <summary>
        /// Sms warning code.
        /// </summary>     
        [DataMember(Order = 0)]
        public string SmsWarningCode { get; set; }
    }
}
