﻿using System.ComponentModel.DataAnnotations;

namespace CE.Models.Insights
{
    /// <summary>
    /// WebToken Request class.
    /// </summary>
    public class TokenPostRequest
    {
        ///// <summary>
        ///// The client application's id.
        ///// </summary>
        //[Required]
        //public string client_id { get; set; }

        ///// <summary>
        ///// The client application's client secret.
        ///// </summary>
        //[Required]
        //[MaxLength(80)]
        //public string client_secret { get; set; }

        /// <summary>
        ///  Must be set to authorization_code or refresh_token.
        /// </summary>
        [Required]
        [MaxLength(80)]
        public string grant_type { get; set; }

        /// <summary>
        /// The authorization code received by the authorization server.
        /// </summary>
        [MaxLength(50)]
        public string code { get; set; }

        /// <summary>
        /// Required, If the request URI was included in the authorization request.Must be identical then.
        /// </summary>
        [MaxLength(2000)]
        public string redirect_uri { get; set; }

        /// <summary>
        /// The refresh token received by the authorization server.
        /// </summary>
        [MaxLength(550)]
        public string refresh_token { get; set; }
    }
}
