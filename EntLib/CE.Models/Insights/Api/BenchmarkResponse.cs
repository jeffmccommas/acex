﻿using System.Runtime.Serialization;

namespace CE.Models.Insights
{
    /// <summary>
    /// The Benchmark response contains comparisons of cost, usage and other measurements for the specified benchmark groups.
    /// </summary>
    [DataContract]
    public class BenchmarkResponse : Status
    {
        /// <summary>
        /// The id of the Client, typically a utility, who is using the API.
        /// </summary>
        [DataMember(Order = 0)]
        public int? ClientId { get; set; }
        /// <summary>
        /// Benchmark data for the customer.
        /// </summary>
        [DataMember(Order = 1)]
        public Customer Customer { get; set; }
        /// <summary>
        /// Related content blocks.
        /// </summary>
        [DataMember(Order = 2)]
        public Content Content { get; set; }
    }
}