﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace CE.Models.Insights
{
    /// <summary>
    /// Usage, cost and other measurement details
    /// </summary>
    [DataContract]
    public class DisaggDetail
    {
        public DisaggDetail()
        {
        }

        /// <summary>
        /// Identifier of the commodity. This is a key value for retrieving commodity-related content from the CMS.
        /// </summary>
        [DataMember(Order = 0)]
        public string CommodityKey { get; set; }

        /// <summary>
        /// The usage quantity
        /// </summary>
        [DataMember(Order = 1)]
        public decimal UsageQuantity { get; set; }

        /// <summary>
        /// The UOM for the usage. This is a key value for retrieving UOM-related content from the CMS.
        /// </summary>
        [DataMember(Order = 2)]
        public string UsageUOMKey { get; set; }

        /// <summary>
        /// The amount charged on the bill.
        /// </summary>
        [DataMember(Order = 3)]
        public decimal CostAmount { get; set; }

        /// <summary>
        /// The currency for the charge amount. This is a key value for retrieving currency-related content from the CMS.
        /// </summary>
        [DataMember(Order = 4)]
        public string CostCurrencyKey { get; set; }

        /// <summary>
        /// The confidence value.  This is an integer representing percent confidence in the result, based upon number of user attributes used in model calculations (0 to 100).
        /// </summary>
        [DataMember(Order = 5)]
        public int Confidence { get; set; }

        /// <summary>
        /// The Reconciliation ratio.  This factor represents the amount bills were adjusted within bill disaggregation for computed or actual bills.  This is a decimal quantity from 0 to 1.
        /// </summary>
        [DataMember(Order = 6)]
        public decimal ReconciliationRatio { get; set; }

        /// <summary>
        /// Additional measurements (other than the standard usage and cost)
        /// </summary>
        [DataMember(Order = 7)]
        public List<Measurement> Measurements;

    }
}
