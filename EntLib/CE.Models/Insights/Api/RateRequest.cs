﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace CE.Models.Insights
{
    /// <summary>
    /// Rate Request
    /// </summary>
    public class RateRequest
    {
        /// <summary>
        /// The rate class. 
        /// </summary>
        [Required]
        public string RateClass { get; set; }

        /// <summary>
        /// The API will return the rate definition that is in effect on this date.
        /// </summary>
        [Required]
        public DateTime Date { get; set; }

        /// <summary>
        /// The start date of the billing period. 
        /// If provided, this is  used to finalize the tier boundary information for the rate definition, 
        /// End date is required if start date is provided
        /// </summary>
        public DateTime StartDate { get; set; }

        /// <summary>
        /// The end date of the billing period. 
        /// If provided, this is used to finalize the tier boundary information for the rate definition, 
        /// Start date is required if end date is provided,
        /// </summary>
        public DateTime EndDate { get; set; }

        /// <summary>
        /// The projected end date to finalize the tier boundaries. 
        /// Start date and end date are required if projected end date is provided.
        /// </summary>
        public DateTime ProjectedEndDate { get; set; }

        /// <summary>
        /// Set to true if using the projected end date to finalize the tier boundaries, else false. 
        /// When IncludeFinalizedTierBoundariesis true,  start date, end date and projected end date are also required. 
        /// Default is False if not specified.
        /// </summary>
        public bool UseProjectedEndDateToFinalizeTier { get; set; }

        /// <summary>
        /// Set to true if the response should include rate details, else false. Default is False if not specified. 
        /// </summary>
        public bool IncludeRateDetails { get; set; }


        /// <summary>
        /// Set to true if the response should include finalized tier boundaries, else false. Default is False if not specified.
        /// </summary>
        public bool IncludeFinalizedTierBoundaries { get; set; }

        /// <summary>
        /// Indicate whether or not to retrieve rate-related content from the content management system. 
        /// Default is False if not specified.
        /// </summary>
        public bool IncludeContent { get; set; }

    }
}
