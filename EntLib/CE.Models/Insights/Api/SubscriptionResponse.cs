﻿using System.Runtime.Serialization;

// ReSharper disable once CheckNamespace
namespace CE.Models.Insights
{
    public class SubscriptionResponse : Status
    {
        /// <summary>
        /// The id of the Client, typically a utility, who is using the API.
        /// </summary>
        [DataMember(Order = 0)]
        public int? ClientId { get; set; }

        /// <summary>
        /// Sms warning code.
        /// </summary>     
        [DataMember(Order = 1)]
        public string SmsWarningCode { get; set; }

        /// <summary>
        /// List of insights available, along with the customer’s subscription status. 
        /// </summary>
        [DataMember(Order = 2)]
        public CustomerInsight Customer { get; set; }

        /// <summary>
        /// subscription-related content data.
        /// </summary>     
        [DataMember(Order = 3)]
        public Content Content { get; set; }
    }
}
