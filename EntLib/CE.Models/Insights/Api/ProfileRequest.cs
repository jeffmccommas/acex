﻿using System;
using System.ComponentModel.DataAnnotations;

namespace CE.Models.Insights
{
    /// <summary>
    /// ProfileRequest class.
    /// </summary>
    public class ProfileRequest
    {
        /// <summary>
        /// The ID of the utility customer.
        /// </summary>
        [Required]
        [MaxLength(50)]
        public string CustomerId { get; set; }

        /// <summary>
        /// The ID of the utility customer's account.
        /// </summary>
        [MaxLength(50)]
        public string AccountId { get; set; }

        /// <summary>
        /// The ID of the utility customer's premise.
        /// </summary>
        [MaxLength(50)]
        public string PremiseId { get; set; }

        /// <summary>
        /// Comma-separated list of keys for profile attributes. Do not leave a space after the comma.
        /// If specified, will only return values for these attributes. 
        /// </summary>
        [MaxLength(200)]
        public string ProfileAttributeKeys { get; set; }

        /// <summary>
        /// Comma-separated list of keys for profile value sources. Do not leave a space after the comma. 
        /// If specified, will only return values with the specified sources. 
        /// If you specify "missing" then you should also select "IncludeMissing" = True.
        /// </summary>
        [MaxLength(64)]
        public string SourceKeys { get; set; }

        /// <summary>
        /// Comma-separated list of keys for appliances. Do not leave a space after the comma.
        /// If specified then the API will return only attributes that are linked to the these appliances.
        /// If not specified then attributes for all appliances will be included.
        /// </summary>
        [MaxLength(200)]
        public string ApplianceKeys { get; set; }

        /// <summary>
        /// Comma-separated list of keys for end uses. Do not leave a space after the comma.
        /// If specified then the API will return only attributes that are linked to appliances in these end uses.  
        /// If not specified then attributes for all end uses will be included. 
        /// </summary>
        [MaxLength(200)]
        public string EndUseKeys { get; set; }

        /// <summary>
        /// If populated, will return only current profile values last updated on or after this date.
        /// Format is YYYY-MM-DD.
        /// </summary>
        public DateTime StartDate { get; set; }

        /// <summary>
        /// If populated, will return only current profile values updated on or before this date.
        /// Format is YYYY-MM-DD.
        /// </summary>
        public DateTime EndDate { get; set; }

        /// <summary>
        /// If set to True, the API will return attributes where there is no value set. 
        /// Will default to false if not specified.
        /// </summary>
        public bool IncludeMissing { get; set; }

        /// <summary>
        /// If set to True, the API will return profile-related content from the content management system.
        /// Will default to false if not specified.
        /// </summary>
        public bool IncludeContent { get; set; }

        /// <summary>
        /// Comma-separated list of entity types for which you want profile data: customer, account, or premise
        /// Do not leave a space after the comma. If not specified then all types will be returned. 
        /// </summary>
        [MaxLength(64)]
        public string EntityLevel { get; set; }
    }
}
