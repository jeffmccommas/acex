﻿using System.Runtime.Serialization;

namespace CE.Models.Insights
{


    /// <summary>
    /// Response of File
    /// </summary>
    [DataContract]
    public class FileResponse: Status
    {
        /// <summary>
        /// Metadata fo file with content
        /// </summary>
        [DataMember]
        public FileDetail File { get; set; }
    }
}
