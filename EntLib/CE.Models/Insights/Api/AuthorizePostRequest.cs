﻿using System.ComponentModel.DataAnnotations;

namespace CE.Models.Insights
{
    /// <summary>
    /// WebToken Request class.
    /// </summary>
    public class AuthorizePostRequest
    {
        /// <summary>
        /// Must be set to "code"
        /// </summary>
        [Required]
        [MaxLength(4)]
        public string response_type { get; set; }

        /// <summary>
        /// The client identifier as assigned by the authorization server, when the client was registered.
        /// </summary>
        [Required]
        public long client_id { get; set; }

        /// <summary>
        /// The redirect URI registered by the client.
        /// </summary>
        [MaxLength(2000)]
        public string redirect_uri { get; set; }

        /// <summary>
        /// The possible scope of the request.
        /// </summary>
        [MaxLength(2000)]
        public string scope { get; set; }

        /// <summary>
        /// Any client state that needs to be passed on to the client request URI.
        /// </summary>
        public string state { get; set; }
    }
}
