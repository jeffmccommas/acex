﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace CE.Models.Insights
{
    /// <summary>
    /// Collection of Files Metadata
    /// </summary>
    [DataContract]
    public class FilesResponse : Status
    {

        [DataMember(Order = 0)]
        public List<FileDetail> Files { get; set; }
    }
}
