﻿using System;
using System.Runtime.Serialization;

namespace CE.Models.Insights
{
    /// <summary>
    /// The WebToken response includes current WebToken data for a customer.
    /// </summary>
    [DataContract]
    public class AuthorizePostResponse : StatusOAuth
    {
        /// <summary>
        /// The authorization code. 
        /// </summary>
        [DataMember(Order = 0)]
        public string code { get; set; }
        /// <summary>
        /// The same value as sent by the client in the state parameter, if any.
        /// </summary>
        [DataMember(Order = 1)]
        public string state { get; set; }
       
    }
}