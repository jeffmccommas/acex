﻿using System.ComponentModel.DataAnnotations;

namespace CE.Models.Insights
{
    /// <summary>
    /// EchoRequest.
    /// </summary>
    public class EchoRequest
    {
        /// <summary>
        /// This value will be echoed. Try hello world.
        /// </summary>
        [Required]
        public string Say { get; set; }

        /// <summary>
        /// This value is required, but is not returned.
        /// </summary>
        [Required]
        public string Name { get; set; }

        /// <summary>
        /// This field is required, but is not returned.
        /// </summary>
        [Required]
        [Range(0, 5)]
        public int? Number { get; set; }
        /// <summary>
        /// Defaults to false. If set to true on a GET Echo, it will keep the application "alive" by refreshing the entity framework 
        /// and content cache. If set to false it will not refresh the entity framework or content cache.
        /// </summary>

        public bool KeepAlive { get; set; }
    }
}
