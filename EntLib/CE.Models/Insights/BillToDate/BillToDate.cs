﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace CE.Models.Insights
{
    /// <summary>
    /// Bill data for the current bill period calculated using the latest AMI data available. 
    /// </summary>
    [DataContract]
    public class BillToDate
    {
        /// <summary>
        /// The start date of the current bill period.
        /// </summary>
        [DataMember(Order = 0)]
        public string BillStartDate { get; set; }

        /// <summary>
        /// The projected end date of the current bill period.
        /// </summary>
        [DataMember(Order = 1)]
        public string BillEndDate { get; set; }

        /// <summary>
        /// Number of days in the current bill period from start to projected end.
        /// </summary>
        [DataMember(Order = 2)]
        public double BillDays { get; set; }

        /// <summary>
        /// The cost so far for current bill period.
        /// </summary>
        [DataMember(Order = 3)]
        public decimal TotalCost { get; set; }

        /// <summary>
        /// The projected cost for current bill period to projected end date.
        /// </summary>
        [DataMember(Order = 4)]
        public decimal TotalProjectedCost { get; set; }

        /// <summary>
        /// The average daily cost.
        /// </summary>
        [DataMember(Order = 5)]
        public decimal AverageDailyCost { get; set; }

        /// <summary>
        /// A list of service-level results for the bill-to-date calculation.
        /// </summary>
        [DataMember(Order = 6)]
        public List<BillToDateService> Services { get; set; }

        /// <summary>
        /// Returns true if there were any errors with the bill-to-date calculation, else false. 
        /// </summary>
        [DataMember(Order = 7)]
        [Required]
        public bool HasError { get; set; }

        /// <summary>
        /// Error message for the bill-to-date calculation result.
        /// </summary>
        [DataMember(Order = 8)]
        public string ErrorMessage { get; set; }

        /// <summary>
        /// Error message for the bill-to-date calculation result.
        /// </summary>
        [DataMember(Order = 10)]
        public List<string> LogEntries { get; set; }

    }
}
