﻿using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace CE.Models.Insights
{
    /// <summary>
    /// Meter reads to include in the bill-to-date calculation.
    /// </summary>
    public class BillToDateReading
    {
        /// <summary>
        /// The date and time of the reading.
        /// </summary>
        [DataMember(Order = 0)]
        [Required]
        public string Timestamp { get; set; }

        /// <summary>
        /// The edit code for the reading, if applicable.
        /// </summary>
        [DataMember(Order = 1)]
        [Required]
        public string EditCode { get; set; }

        /// <summary>
        /// The Time-of-Use bin for the reading, if applicable.
        /// </summary>
        [DataMember(Order = 2)]
        [Required]
        public string TouBin { get; set; }

        /// <summary>
        /// The reading value.
        /// </summary>
        [DataMember(Order = 3)]
        [Required]
        public double Reading { get; set; }

        /// <summary>
        /// The direction for the reading, if applicable.
        /// </summary>
        [DataMember(Order = 4)]
        public string Direction { get; set; }
    }
}
