﻿
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace CE.Models.Insights
{
    /// <summary>
    /// Non-metered service information to use in the bill-to-date calculation.
    /// </summary>
    public class BillToDateNonMeter
    {
        /// <summary>
        /// Id of the non-metered service.
        /// </summary>
        [DataMember(Order = 0)]
        [Required]
        public string Id { get; set; }

        /// <summary>
        /// Key value for the commodity provided by the service.
        /// </summary>
        [DataMember(Order = 1)]
        [Required]
        public string CommodityKey { get; set; }

        /// <summary>
        /// The start date of the bill period.
        /// </summary>
        [DataMember(Order = 2)]
        [Required]
        public string StartDate { get; set; }

        /// <summary>
        /// The end date to include in the calculation.
        /// </summary>
        [DataMember(Order = 3)]
        [Required]
        public string EndDate { get; set; }

        /// <summary>
        /// The projected end date of the bill period.
        /// </summary>
        [DataMember(Order = 4)]
        public string ProjectedEndDate { get; set; }

        /// <summary>
        /// The non-metered service quantity. 
        /// </summary>
        [DataMember(Order = 5)]
        [Required]
        public double Usage { get; set; }

        /// <summary>
        /// The non-metered service cost.
        /// </summary>
        [DataMember(Order = 6)]
        [Required]
        public double Cost { get; set; }

        /// <summary>
        /// The unit of measure for the service.
        /// </summary>
        [DataMember(Order = 7)]
        [Required]
        public string UomKey { get; set; }
    }
}
