﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace CE.Models.Insights
{
    /// <summary>
    /// Metered services to include in the Bill-to-Date calculation.
    /// </summary>
    public class BillToDateMeterService
    {
        /// <summary>
        /// Id of the metered service.
        /// </summary>
        [DataMember(Order = 0)]
        [Required]
        public string Id { get; set; }

        /// <summary>
        /// Key value for the commodity provided by this service.
        /// </summary>
        [DataMember(Order = 1)]
        [Required]
        public string CommodityKey { get; set; }

        /// <summary>
        /// Meters included in the service. 
        /// </summary>
        [DataMember(Order = 2)]
        [Required]
        public List<BillToDateMeter> Meters { get; set; }
    }
}
