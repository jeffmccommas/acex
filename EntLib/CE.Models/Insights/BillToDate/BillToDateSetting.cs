﻿using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace CE.Models.Insights.BillToDate
{
    public class BillToDateSetting
    {       

        [DataMember(Order = 0)]
        public string MiniumChargeType { get; set; }

        [DataMember(Order = 1)]
        public bool UseConversionFactorForGas { get; set; }

        [DataMember(Order = 2)]
        public bool UseConversionFactorForWater { get; set; }

        [DataMember(Order = 3)]
        public int ProjectedNumberOfDays { get; set; }

        [DataMember(Order = 4)]
        public bool SmoothTiers { get; set; }

        [DataMember(Order = 5)]
        public bool AllowMultipleMetersPerService { get; set; }

        [DataMember(Order = 6)]
        public string DstEndDate { get; set; }

        [DataMember(Order = 7)]
        public bool AllowRebateCalculations { get; set; }

        [DataMember(Order = 8)]
        public bool AllowBaseLineCalculations { get; set;  }

        [DataMember(Order = 9)]
        public string DailyTierBillDaysType { get; set; }

        [DataMember(Order = 10)]
        public bool UseProjectedNumDaysForbillToDate { get; set; }

        [DataMember(Order = 11)]
        public bool SupportDailyDemand { get; set; }

        [DataMember(Order = 12)]
        public bool HandleMixedIntervalReading { get; set; }

        [DataMember(Order = 13)]
        public bool CheckMaxMissingDays { get; set; }

        [DataMember(Order = 14)]
        public bool CheckDaysInFullMonth { get; set; }

        [DataMember(Order = 15)]
        public bool CheckMinBillDays { get; set; }

        [DataMember(Order = 16)]
        public bool CheckMinUsage { get; set; }

        [DataMember(Order = 17)]
        public int MaxMissingDaysThreshold { get; set; }

        [DataMember(Order = 18)]
        public int DaysInFullMonth { get; set; }

        [DataMember(Order = 19)]
        public int MinBillDaysElectricThreshold { get; set; }

        [DataMember(Order = 20)]
        public int MinBillDaysGasThreshold { get; set; }

        [DataMember(Order = 21)]
        public int MinBillDaysWaterThreshold { get; set; }

        [DataMember(Order = 22)]
        public int MinUsageThreshold { get; set; }

        [DataMember(Order = 23)]
        public bool ProrateMonthlyServiceCharges { get; set; }

        [DataMember(Order = 24)]
        public bool ProrateNonMeteredCharges { get; set; }

        [DataMember(Order = 25)]
        public bool PorateDemandUsageDeterminants { get; set; }

        [DataMember(Order = 26)]
        public bool ProrateSewerMaxMonthUsageDeterminants { get; set; }

        [DataMember(Order = 27)]
        public bool UseSewerMaxMonthsUsageForResidential { get; set; }

        [DataMember(Order = 28)]
        public bool UseSewerMaxMonthsUsageForCommercial { get; set; }

        [DataMember(Order = 29)]
        public int sewerMaxMonthsUsageForResidential { get; set; }

        [DataMember(Order = 30)]
        public int SewerMaxMonthsUsageForCommercial
    }
}
