﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace CE.Models.Insights
{
    /// <summary>
    /// Meters to include in the Bill-to-Date calculation.
    /// </summary>
    public class BillToDateMeter
    {
        /// <summary>
        /// The meter Id.
        /// </summary>
        [DataMember(Order = 0)]
        [Required]
        public string Id { get; set; }

        /// <summary>
        /// The start date of the bill period.
        /// </summary>
        [DataMember(Order = 1)]
        [Required]
        public string StartDate { get; set; }

        /// <summary>
        /// The end date to include in the calculation.
        /// </summary>
        [DataMember(Order = 2)]
        [Required]
        public string EndDate { get; set; }

        /// <summary>
        /// The projected end date of the bill period.
        /// </summary>
        [DataMember(Order = 3)]
        public string ProjectedEndDate { get; set; }

        /// <summary>
        /// The rate class to use for this meter.
        /// </summary>
        [DataMember(Order = 4)]
        [Required]
        public string RateClass { get; set; }

        /// <summary>
        /// The sewer rate class to use, if applicable.
        /// </summary>
        [DataMember(Order = 5)]
        public string SewerRateClass { get; set; }

        /// <summary>
        /// The sewer maximum derived usage, if applicable.
        /// </summary>
        [DataMember(Order = 6)]
        public int SewerMaxDerivedUsage { get; set; }

        /// <summary>
        /// The key value for the unit of measure for this meter.
        /// </summary>
        [DataMember(Order = 7)]
        [Required]
        public string UomKey { get; set; }

        /// <summary>
        /// Meter readings to use in the bill-to-date calculation.
        /// </summary>
        [DataMember(Order = 8)]
        public List<BillToDateReading> Readings { get; set; }

    }
}
