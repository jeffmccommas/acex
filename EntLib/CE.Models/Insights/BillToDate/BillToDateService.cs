﻿using Newtonsoft.Json;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace CE.Models.Insights
{
    /// <summary>
    /// Service-level bill-to-date information.
    /// </summary>
    [DataContract]
    public class BillToDateService
    {
        /// <summary>
        /// Id of the utility service.
        /// </summary>
        [DataMember(Order = 0)]
        [Required]
        public string Id { get; set; }

        /// <summary>
        /// The total current usage to-date.
        /// </summary>
        [DataMember(Order = 1)]
        [Required]
        public decimal UseToDate { get; set; }

        /// <summary>
        /// The projected usage for the projected end of current bill period.
        /// </summary>
        [DataMember(Order = 2)]
        [Required]
        public decimal UseProjected { get; set; }

        /// <summary>
        /// The cost for current usage to-date.
        /// </summary>
        [DataMember(Order = 3)]
        [Required]
        public decimal CostToDate { get; set; }

        /// <summary>
        /// The projected cost for the projected end of current bill period.
        /// </summary>
        [DataMember(Order = 4)]
        [Required]
        public decimal CostProjected { get; set; }
        
        /// <summary>
        /// The key value for the commodity provided by this service.
        /// </summary>
        [DataMember(Order = 5)]
        [StringLength(10)]
        [Required]
        public string CommodityKey { get; set; }

        /// <summary>
        /// The start date of current bill period.
        /// </summary>
        [DataMember(Order = 6)]
        [Required]
        public string StartDate { get; set; }

        /// <summary>
        /// The projected end date of current bill period.
        /// </summary>
        [DataMember(Order = 7)]
        [Required]
        public string EndDate { get; set; }

        /// <summary>
        /// The read date of current bill period.
        /// </summary>
        [DataMember(Order = 8)]
        [Required]
        public string ReadDate { get; set; }

        /// <summary>
        /// The number of days into current bill period.
        /// </summary>
        [DataMember(Order = 9)]
        [Required]
        public int DaysIntoCycle { get; set; }


        /// <summary>
        /// The key value for the unit of measure of this service.
        /// </summary>
        [DataMember(Order = 10)]
        [Required]
        public string UomKey { get; set; }


        /// <summary>
        /// The rate class of this service.
        /// </summary>
        [DataMember(Order = 11)]
        public string RateClass { get; set; }
    }
}
