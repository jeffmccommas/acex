﻿using System;
using System.Collections.Generic;

// ReSharper disable once CheckNamespace
namespace CE.Models.Insights
{
    public class GreenButtonApplicationInfo
    {
        public long Id { get; set; }
        public string UuId { get; set; }
        public List<GreenButtonLink> Links { get; set; }
        public string DataCustodianId { get; set; }
        public int DataCustodianApplicationStatus { get; set; }
        public string DataCustodianBulkRequestUri { get; set; }
        public string DataCustodianResourceEndpoint { get; set; }
        public string ThirdPartyNotifyUri { get; set; }
        public string ThirdPartyScopeSelectionScreenUri { get; set; }
        public string ThirdPartyApplicationName { get; set; }
        public string ClientSecret { get; set; }
        public string ClientId { get; set; }
        public string RedirectUri { get; set; }
        public string Scope { get; set; }
        public DateTime PublishedDate { get; set; }
        public DateTime UpdatedDate { get; set; }
    }
}