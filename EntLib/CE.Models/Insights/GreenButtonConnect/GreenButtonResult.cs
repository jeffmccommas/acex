﻿
using System.IO;
using CE.Models.Insights.Types;

namespace CE.Models.Insights.GreenButtonConnect
{
    public class GreenButtonResult
    {
        public string FileName { get; set; }
        public MemoryStream ZipStream { get; set; }
        public string GreenButtonXml { get; set; }
        public GreenButtonErrorType Status { get; set; } = GreenButtonErrorType.NoError;
        
    }
}
