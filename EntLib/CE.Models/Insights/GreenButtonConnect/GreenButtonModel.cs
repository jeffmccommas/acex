﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CE.GreenButtonConnect;
using CE.Models.Insights.Types;
using AutoMapper;
using CE.Models.Insights.GreenButtonConnect;
using CE.Models.Utils;

// ReSharper disable once CheckNamespace
namespace CE.Models.Insights
{
    public class GreenButtonModel : IGreenButtonModel
    {
        private readonly GreenButtonConfig _greenButtonSettings;
        private static IInsightsEFRepository _insightsEfRepository = new InsightsEfRepository();
        private const string GreenButtonLinkPrefix = "/Datacustodian/espi/1_1/resource";
        private const string GreenButtonApplicationInfoTitle = "DataCustodian Application";
        private const string GreenButtonAuthorizationTitle = "DataCustodian Authorization";

        private const int UomidCubicFeet = 119;
        private const int UomidWattHours = 72;

        private readonly int _clientId;
        private readonly IGreenButtonConnect _greenButtonConnect;
        private readonly string _greenButtonDomain;
        
        /// <summary>
        /// Green Button  Model
        /// </summary>
        /// <param name="clientId"></param>
        /// <param name="greenButtonDomain"></param>
        /// <param name="config"></param>
        /// <param name="insightsEfRepository"></param>
        public GreenButtonModel(int clientId, string greenButtonDomain, GreenButtonConfig config, IInsightsEFRepository insightsEfRepository)
        {
            InitializeAutoMapper();
            _clientId = clientId;
            _greenButtonSettings = config;
            _insightsEfRepository = insightsEfRepository;
            var gbFactor = new GreenButtonConnectFactory();
            _greenButtonDomain = greenButtonDomain;
            _greenButtonConnect = gbFactor.CreateGreenButtonManager(_clientId, _greenButtonDomain);

        }

        /// <summary>
        /// used by unit test
        /// </summary>
        /// <param name="clientId"></param>
        /// <param name="greenButtonDomain"></param>
        /// <param name="config"></param>
        /// <param name="insightsEfRepository"></param>
        /// <param name="greenButtonConnect"></param>
        public GreenButtonModel(int clientId, string greenButtonDomain, GreenButtonConfig config, IInsightsEFRepository insightsEfRepository, IGreenButtonConnect greenButtonConnect) : this(clientId, greenButtonDomain, config, insightsEfRepository)
        {
            _greenButtonConnect = greenButtonConnect;
        }
        public GreenButtonResult GenerateGreenButton(GreenButtonSubscription subscription, GreenButtonEntryType entryType)
        {
            var subscriptionEntry = GreenButtonSubscriptionMapping(subscription);


            if (subscriptionEntry != null)
            {

                GreenButtonConnectResult gbConnectresult;
                Link selfLink;
                switch (entryType)
                {
                    case GreenButtonEntryType.IntervalBlock:
                        selfLink = subscriptionEntry.Links.Find(l => l.HyperLinkRel == "self");
                        selfLink.HyperLinkRef = subscriptionEntry.UsagePoints.FirstOrDefault()?.MeterReading.FirstOrDefault()?.IntervalBlock?.FirstOrDefault()?.Links.Find(l => l.HyperLinkRel == "up").HyperLinkRef;
                        gbConnectresult = _greenButtonConnect.GenerateGreenButtonConnect(subscriptionEntry, GreenButtonType.IntervalBlock);
                        break;
                    case GreenButtonEntryType.UsagePoint:
                        gbConnectresult = _greenButtonConnect.GenerateGreenButtonConnect(subscriptionEntry, GreenButtonType.UsagePoint);
                        break;
                    case GreenButtonEntryType.ReadingType:
                        selfLink = subscriptionEntry.Links.Find(l => l.HyperLinkRel == "self");
                        selfLink.HyperLinkRef = subscriptionEntry.UsagePoints.FirstOrDefault()?.MeterReading.FirstOrDefault()?.ReadingType.Links.Find(l => l.HyperLinkRel == "up").HyperLinkRef;
                        gbConnectresult = _greenButtonConnect.GenerateGreenButtonConnect(subscriptionEntry, GreenButtonType.ReadingType);
                        break;
                    case GreenButtonEntryType.LocalTimeParameters:
                        selfLink = subscriptionEntry.Links.Find(l => l.HyperLinkRel == "self");
                        selfLink.HyperLinkRef = subscriptionEntry.UsagePoints.FirstOrDefault()?.TimeConfiguration.Links.Find(l => l.HyperLinkRel == "up").HyperLinkRef;

                        gbConnectresult = _greenButtonConnect.GenerateGreenButtonConnect(subscriptionEntry, GreenButtonType.LocalTimeParameters);
                        break;
                    case GreenButtonEntryType.MeterReading:
                        selfLink = subscriptionEntry.Links.Find(l => l.HyperLinkRel == "self");
                        selfLink.HyperLinkRef = subscriptionEntry.UsagePoints.FirstOrDefault()?.MeterReading.FirstOrDefault()?.Links.Find(l => l.HyperLinkRel == "up").HyperLinkRef;

                        gbConnectresult = _greenButtonConnect.GenerateGreenButtonConnect(subscriptionEntry, GreenButtonType.MeterReading);
                        break;
                    default:
                        gbConnectresult = _greenButtonConnect.GenerateGreenButtonConnect(subscriptionEntry, GreenButtonType.All);
                        break;
                }
                return Mapper.Map<GreenButtonConnectResult, GreenButtonResult>(gbConnectresult);
            }

            return null;
        }

        public GreenButtonResult GenerateServiceStatus(bool isValidServiceStatus)
        {
            var gbConnectresult = _greenButtonConnect.GenerateGreenButtonServiceStatus(isValidServiceStatus);
            return Mapper.Map<GreenButtonConnectResult, GreenButtonResult>(gbConnectresult);
        }

        public GreenButtonResult GenerateApplicationInfo(GreenButtonApplicationInfo applicationInfo)
        {
            var applicationInfoEntry = GreenButtonApplicationInfoMapping(applicationInfo);
            var subscription = new Subscription
            {
                GuidId = "urn:uuid:" + Guid.NewGuid().ToString(),
                Title = _greenButtonSettings.ReportTitle,
                Links = new List<Link>
                {
                    new Link
                    {
                        HyperLinkRel = "self",
                        HyperLinkRef = applicationInfoEntry?.Links.Find(l => l.HyperLinkRel == "up").HyperLinkRef
                    }
                },
                ApplicationInformation = applicationInfoEntry
            };

            var gbConnectresult = _greenButtonConnect.GenerateGreenButtonConnect(subscription, GreenButtonType.ApplicationInfo);
            return Mapper.Map<GreenButtonConnectResult, GreenButtonResult>(gbConnectresult);
        }

        public GreenButtonResult GenerateAuthorization(List<GreenButtonAuthorization> authorizations)
        {
            if(authorizations.Any())
            {
                var authorizationEntries = GreenButtonAuthorizationMapping(authorizations);
                var subscription = new Subscription
                {
                    GuidId = "urn:uuid:" + Guid.NewGuid().ToString(),
                    Title = _greenButtonSettings.ReportTitle,
                    Links = new List<Link>
                {
                    new Link
                    {
                        HyperLinkRel = "self",
                        HyperLinkRef = authorizationEntries[0]?.Links.Find(l => l.HyperLinkRel == "up").HyperLinkRef
                    }
                },
                    Authorizations = authorizationEntries
                };

                var gbConnectresult = _greenButtonConnect.GenerateGreenButtonConnect(subscription, GreenButtonType.Authorzation);
                return Mapper.Map<GreenButtonConnectResult, GreenButtonResult>(gbConnectresult);
            }
            return new GreenButtonResult();
        }

        public Subscription GreenButtonSubscriptionMapping(GreenButtonSubscription subscription)
        {
            var subscriptionEntry = Mapper.Map<GreenButtonSubscription, Subscription>(subscription);
            subscriptionEntry.Title = _greenButtonSettings.ReportTitle;
            // subscriptionEntry.Links - certification link TBD!!!!!!!!
            subscriptionEntry.UsagePoints = new List<UsagePointEntry>();
            foreach (var usagePoint in subscription.UsagePoints)
            {
                subscriptionEntry.UsagePoints.Add(GreenButtonUsagePointMapping(subscription.Id,usagePoint));
            }

            return subscriptionEntry;
        }

        private ApplicationInformationEntry GreenButtonApplicationInfoMapping(GreenButtonApplicationInfo applicationInfo)
        {
            var applicationInfoEntry =
                Mapper.Map<GreenButtonApplicationInfo, ApplicationInformationEntry>(applicationInfo);
            applicationInfoEntry.Title = GreenButtonApplicationInfoTitle;
            return applicationInfoEntry;
        }

        private List<AuthorizationEntry> GreenButtonAuthorizationMapping(List<GreenButtonAuthorization> authorization)
        {
            var applicationInfoEntry =
                Mapper.Map<List<GreenButtonAuthorization>, List<AuthorizationEntry>>(authorization);
            return applicationInfoEntry;
        }

        private UsagePointEntry GreenButtonUsagePointMapping(long subscriptionId, GreenButtonUsagePoint usagePoint)
        {
            var usagePointEntry = Mapper.Map<GreenButtonUsagePoint, UsagePointEntry>(usagePoint);
            usagePointEntry.Title = _greenButtonSettings.UsagePointTitle;
            usagePointEntry.MeterReading = new List<MeterReadingEntry>();
            usagePointEntry.UsagePoint = new UsagePoint
            {
                ServiceCategory =  new ServiceCategory()
            };

            // Meter readings
            foreach (var readingType in usagePoint.ReadingTypes)
            {
                usagePointEntry.MeterReading.Add(GreenButtonMeterReadingMapping(subscriptionId, usagePoint.Id, readingType));
                usagePointEntry.UsagePoint.ServiceCategory.kind = readingType.Kind;
            }

            // time config
            usagePointEntry.TimeConfiguration = GreenButtonLocalTimeParamMapping(usagePoint.TimeConfigurationId ?? 1);
            
            return usagePointEntry;
        }

        private MeterReadingEntry GreenButtonMeterReadingMapping(long subscriptionId, long usagePointId, GreenButtonAmiReadingType amiReadingType)
        {
            var idGenerator = new IdGenerator(_clientId,amiReadingType.MeterId);
            var meterReading  = new MeterReadingEntry
            {
                GuidId = idGenerator.MeterReadingId(amiReadingType.Id),
                Links = GenerateMeterReadingLinks(subscriptionId,amiReadingType.Id.ToString(),amiReadingType.MeterId),
                MeterId = amiReadingType.MeterId,
                PublishedDate = amiReadingType.PublishedDate,
                UpdatedDate = amiReadingType.UpdatedDate,
                ReadingTypeId = Convert.ToInt32(amiReadingType.Id),
                UsagePointId = Convert.ToInt32(usagePointId),
                MeterReading = new MeterReading()
            };

            // reading Type
            meterReading.ReadingType = GreenButtonReadingTypeMapping(amiReadingType);

            // interval block
            if (amiReadingType.ConsumptionList != null)
            {
                var conversionFactor = 1m;
                if (meterReading.ReadingType?.ReadingType.commodity == 1 && _greenButtonSettings.ConvertGasUom)
                    conversionFactor = Convert.ToDecimal(_greenButtonSettings.DefaultGasConversionFactor);
                // interval blocks
                if (meterReading.ReadingType != null)
                    meterReading.IntervalBlock = GreenButtonIntervalBlockMapping(subscriptionId, usagePointId,
                        amiReadingType.Id, amiReadingType.MeterId, amiReadingType.ConsumptionList,
                        amiReadingType.BlockDuratioin, meterReading.ReadingType.ReadingType.commodity,
                        meterReading.ReadingType.ReadingType.uom, conversionFactor);
            }

            return meterReading;
        }
        private ReadingTypeEntry GreenButtonReadingTypeMapping(GreenButtonAmiReadingType amiReadingType)
        {
            var readingType = Mapper.Map<GreenButtonAmiReadingType, ReadingTypeEntry>(amiReadingType);

            return readingType;
        }
        private List<IntervalBlockEntry> GreenButtonIntervalBlockMapping(long subscriptionId, long usagePointId, long readingTypeId, string meterid, IntervalConsumptionData intervalConsumption, GreenButtonDurationType blockDuration, int commodity, int uomId, decimal conversionFactor) 
        {
            var intervalBlocks = new List<IntervalBlockEntry>();

            var multipler = GetConsumptionMultiplier(commodity, uomId);

            // Daily for current scope
            if (blockDuration == GreenButtonDurationType.Daily) 
            {
                intervalBlocks = MapDailyIntervalBlocks(subscriptionId, usagePointId, readingTypeId, meterid,
                    intervalConsumption, blockDuration, multipler, conversionFactor);
            }

            return intervalBlocks;
        }

        private LocalTimeParameterEntry GreenButtonLocalTimeParamMapping(long timeConfigId)
        {
            var timeParams = _insightsEfRepository.GetGreenButtonTimeParam(timeConfigId);

            return Mapper.Map<GreenButtonTimeParameter, LocalTimeParameterEntry>(timeParams);
        }
        private List<IntervalBlockEntry> MapDailyIntervalBlocks(long subscriptionId, long usagePointId, long readingTypeId, string meterid, IntervalConsumptionData intervalConsumption,
            GreenButtonDurationType blockDuration, decimal multipler, decimal conversionFactor) 
        {
            var idGenerator = new IdGenerator(_clientId,meterid);

            var intervalBlockEntries = new List<IntervalBlockEntry>();
            var date = intervalConsumption.ConsumptionDataList[0].DateTime;
            
            ResolutionType resolution;
            Enum.TryParse(intervalConsumption.ResolutionKey, out resolution);
            
            var intervalBlockEntry = new IntervalBlockEntry
            {
                GuidId = idGenerator.GetIntervalBlockId(resolution.ToString(),date),
                UpdatedDate = date,
                PublishedDate = date,
                UsagePointId =  Convert.ToInt32(usagePointId),
                MeterId = meterid,
                ReadingTypeId = Convert.ToInt32(readingTypeId),
                Links = GenerateIntervalBlockLinks(subscriptionId, readingTypeId.ToString(), meterid,date),
                Title = _greenButtonSettings.IntervalBlockTitle,
                IntervalBlock = new IntervalBlock { 
                interval = new DateTimeInterval
                {
                    duration = Convert.ToInt64(blockDuration),
                    start = ConvertDateTime(date)
                }}
            };
            var intervalReadings = new  List<IntervalReading>();
            foreach (var consumption in intervalConsumption.ConsumptionDataList)
            {
                if (consumption.DateTime.Day != date.Day)
                {
                    // add the previous reading into block
                    intervalBlockEntry.IntervalBlock.IntervalReading = intervalReadings;
                    intervalBlockEntries.Add(intervalBlockEntry);

                    // reset block for new readings
                    date = consumption.DateTime;
                    intervalReadings = new List<IntervalReading>();
                    intervalBlockEntry = new IntervalBlockEntry
                    {
                        GuidId = idGenerator.GetIntervalBlockId(resolution.ToString(), date),
                        UpdatedDate = date,
                        PublishedDate = date,
                        UsagePointId = Convert.ToInt32(usagePointId),
                        MeterId = meterid,
                        ReadingTypeId = Convert.ToInt32(readingTypeId),
                        Links = GenerateIntervalBlockLinks(subscriptionId, readingTypeId.ToString(), meterid, date),
                        IntervalBlock = new IntervalBlock
                        {
                            interval = new DateTimeInterval
                            {
                                duration = Convert.ToInt64(blockDuration),
                                start = ConvertDateTime(date)
                            }
                        }
                    };
                }

                var intervalReading = new IntervalReading
                {
                    value = Convert.ToInt64(Math.Round(consumption.Value * conversionFactor * multipler, MidpointRounding.AwayFromZero)),
                    timePeriod = new DateTimeInterval
                    {
                        duration = GreenButton.GetResolutionDuration(resolution),
                        start = ConvertDateTime(consumption.DateTime)
                    }
                };

                intervalReadings.Add(intervalReading);

            }

            // add the previous reading into block
            intervalBlockEntry.IntervalBlock.IntervalReading = intervalReadings;
            intervalBlockEntries.Add(intervalBlockEntry);

            return intervalBlockEntries;
        }


        private void InitializeAutoMapper()
        {
            Mapper.CreateMap<GreenButtonAmiReadingType, ReadingType>()
                .ForMember(dest => dest.commodity, opt => opt.MapFrom(src => src.Commodity))
                .ForMember(dest => dest.DefaultQuality, opt => opt.MapFrom(src => src.DefaultQuality))
                .ForMember(dest => dest.accumulationBehaviour, opt => opt.MapFrom(src => src.AccumulationBehavior))
                .ForMember(dest => dest.aggregate, opt => opt.MapFrom(src => src.Aggregate))
                .ForMember(dest => dest.argument,
                    opt => opt.MapFrom(src => Mapper.Map<GreenButtonAmiReadingType, RationalNumber>(src)))
                .ForMember(dest => dest.consumptionTier, opt => opt.MapFrom(src => src.ConsumptionTier))
                .ForMember(dest => dest.cpp, opt => opt.MapFrom(src => src.Cpp))
                .ForMember(dest => dest.currency, opt => opt.MapFrom(src => src.Currency))
                .ForMember(dest => dest.dataQualifier, opt => opt.MapFrom(src => src.DataQualifier))
                .ForMember(dest => dest.flowDirection, opt => opt.MapFrom(src => src.FlowDirection))
                .ForMember(dest => dest.interharmonic,
                    opt => opt.MapFrom(src => Mapper.Map<GreenButtonAmiReadingType, ReadingInterharmonic>(src)))
                .ForMember(dest => dest.intervalLength, opt => opt.MapFrom(src => src.IntervalLength))
                .ForMember(dest => dest.kind, opt => opt.MapFrom(src => src.Kind))
                .ForMember(dest => dest.measuringPeriod, opt => opt.MapFrom(src => src.MeasuringPeriod))
                .ForMember(dest => dest.phase, opt => opt.MapFrom(src => src.Phase))
                .ForMember(dest => dest.powerOfTenMultiplier, opt => opt.MapFrom(src => src.PowerOfTenMultiplier))
                .ForAllUnmappedMembers(o => o.Ignore());

            Mapper.CreateMap<GreenButtonAmiReadingType, ReadingInterharmonic>()
                .ForMember(dest => dest.denominator, opt => opt.MapFrom(src => src.InterharMonicDenominator))
                .ForMember(dest => dest.numerator, opt => opt.MapFrom(src => src.InterharmonicNumerator))
               .ForAllUnmappedMembers(o => o.Ignore());

            Mapper.CreateMap<GreenButtonAmiReadingType, RationalNumber>()
                .ForMember(dest => dest.denominator, opt => opt.MapFrom(src => src.RationalDenomiator))
                .ForMember(dest => dest.numerator, opt => opt.MapFrom(src => src.RationalNumerator))
                .ForAllUnmappedMembers(o => o.Ignore());

            Mapper.CreateMap<GreenButtonLink, Link>()
                .ForMember(dest => dest.HyperLinkRef, opt => opt.MapFrom(src => src.Href))
                .ForMember(dest => dest.HyperLinkRel, opt => opt.MapFrom(src => src.Rel));

            Mapper.CreateMap<GreenButtonAmiReadingType, ReadingTypeEntry>()
                .ForMember(dest => dest.PublishedDate, opt => opt.MapFrom(src => src.PublishedDate))
                .ForMember(dest => dest.GuidId, opt => opt.MapFrom(src => src.Uuid))
                .ForMember(dest => dest.Id, opt => opt.MapFrom(src => src.Id))
                .ForMember(dest => dest.UpdatedDate, opt => opt.MapFrom(src => src.UpdatedDate))
                .ForMember(dest => dest.ReadingType, opt => opt.MapFrom(src => Mapper.Map<GreenButtonAmiReadingType, ReadingType>(src)))
                .ForMember(dest => dest.Links, opt => opt.MapFrom(src => Mapper.Map<List<GreenButtonLink>, List<Link>>(src.Links)))
                .ForAllUnmappedMembers(o => o.Ignore());
            
            Mapper.CreateMap<GreenButtonUsagePoint, UsagePointEntry>()
                .ForMember(dest => dest.GuidId, opt => opt.MapFrom(src => src.UuId))
                .ForMember(dest => dest.Id, opt => opt.MapFrom(src => src.Id))
                .ForMember(dest => dest.Links, opt => opt.MapFrom(src => Mapper.Map<List<GreenButtonLink>, List<Link>>(src.Links)))
                .ForMember(dest => dest.PublishedDate, opt => opt.MapFrom(src => src.PublishedDate))
                .ForMember(dest => dest.UpdatedDate, opt => opt.MapFrom(src => src.UpdatedDate))
                .ForAllUnmappedMembers(o => o.Ignore());

            Mapper.CreateMap<GreenButtonTimeParameter, LocalTimeParameters>()
                .ForMember(dest => dest.dstEndRule, opt => opt.MapFrom(src => src.DstEndRule))
                .ForMember(dest => dest.dstStartRule, opt => opt.MapFrom(src => src.DstStartRule))
                .ForMember(dest => dest.dstOffset, opt => opt.MapFrom(src => src.DstOffset))
                .ForMember(dest => dest.tzOffset, opt => opt.MapFrom(src => src.TzOffset))
                .ForAllUnmappedMembers(o => o.Ignore());

            Mapper.CreateMap<GreenButtonTimeParameter, LocalTimeParameterEntry>()
                .ForMember(dest => dest.GuidId, opt => opt.MapFrom(src => src.GuidId))
                .ForMember(dest => dest.Id, opt => opt.MapFrom(src => src.Id))
                .ForMember(dest => dest.Links, opt => opt.MapFrom(src => Mapper.Map<List<GreenButtonLink>, List<Link>>(src.Links)))
                .ForMember(dest => dest.PublishedDate, opt => opt.MapFrom(src => src.PublishedDate))
                .ForMember(dest => dest.UpdatedDate, opt => opt.MapFrom(src => src.UpdatedDate))
                .ForMember(dest => dest.LocalTimeParameters, opt => opt.MapFrom(src => Mapper.Map<GreenButtonTimeParameter, LocalTimeParameters>(src)))
                .ForAllUnmappedMembers(o => o.Ignore());

            Mapper.CreateMap<GreenButtonSubscription, Subscription>()
                .ForMember(dest => dest.GuidId, opt => opt.MapFrom(src => src.UuId))
                .ForMember(dest => dest.Id, opt => opt.MapFrom(src => src.Id))
                .ForMember(dest => dest.Links, opt => opt.MapFrom(src => Mapper.Map<List<GreenButtonLink>, List<Link>>(src.Links)))
                .ForMember(dest => dest.PublishedDate, opt => opt.MapFrom(src => src.PublishedDate))
                .ForMember(dest => dest.UpdatedDate, opt => opt.MapFrom(src => src.UpdatedDate))
                .ForAllUnmappedMembers(o => o.Ignore());

            Mapper.CreateMap<GreenButtonApplicationInfo, ApplicationInformation>()
                .ForMember(dest => dest.scope, opt => opt.MapFrom(src => src.Scope))
                .ForMember(dest => dest.client_id, opt => opt.MapFrom(src => src.ClientId))
                .ForMember(dest => dest.client_secret, opt => opt.MapFrom(src => src.ClientSecret))
                .ForMember(dest => dest.dataCustodianApplicationStatus,
                    opt => opt.MapFrom(src => src.DataCustodianApplicationStatus))
                .ForMember(dest => dest.dataCustodianBulkRequestURI,
                    opt => opt.MapFrom(src => src.DataCustodianBulkRequestUri))
                .ForMember(dest => dest.dataCustodianId, opt => opt.MapFrom(src => src.DataCustodianId))
                .ForMember(dest => dest.dataCustodianResourceEndpoint,
                    opt => opt.MapFrom(src => src.DataCustodianResourceEndpoint))
                .ForMember(dest => dest.redirect_uri, opt => opt.MapFrom(src => src.RedirectUri))
                .ForMember(dest => dest.thirdPartyApplicationName,
                    opt => opt.MapFrom(src => src.ThirdPartyApplicationName))
                .ForMember(dest => dest.thirdPartyNotifyUri, opt => opt.MapFrom(src => src.ThirdPartyNotifyUri))
                .ForMember(dest => dest.thirdPartyScopeSelectionScreenURI,
                    opt => opt.MapFrom(src => src.ThirdPartyScopeSelectionScreenUri));

            Mapper.CreateMap<GreenButtonApplicationInfo, ApplicationInformationEntry>()
                .ForMember(dest => dest.GuidId, opt => opt.MapFrom(src => src.UuId))
                .ForMember(dest => dest.Id, opt => opt.MapFrom(src => src.Id))
                .ForMember(dest => dest.Links, opt => opt.MapFrom(src => Mapper.Map<List<GreenButtonLink>, List<Link>>(src.Links)))
                .ForMember(dest => dest.ApplicationInformation, opt => opt.MapFrom(src => Mapper.Map<GreenButtonApplicationInfo, ApplicationInformation>(src)))
                .ForMember(dest => dest.PublishedDate, opt => opt.MapFrom(src => src.PublishedDate))
                .ForMember(dest => dest.UpdatedDate, opt => opt.MapFrom(src => src.UpdatedDate))
                .ForAllUnmappedMembers(o => o.Ignore());

            Mapper.CreateMap<TimePeriod, DateTimeInterval>()
               .ForMember(dest => dest.duration, opt => opt.MapFrom(src => src.Duration))
               .ForMember(dest => dest.start, opt => opt.MapFrom(src => src.Start))
              .ForAllUnmappedMembers(o => o.Ignore());

            Mapper.CreateMap<GreenButtonAuthorization, Authorization>()
                .ForMember(dest => dest.authorizedPeriod, opt => opt.MapFrom(src => Mapper.Map<TimePeriod, DateTimeInterval>(src.AuthorizationTimePeriod)))
                .ForMember(dest => dest.publishedPeriod, opt => opt.MapFrom(src => Mapper.Map<TimePeriod, DateTimeInterval>(src.PusblishedTimePeriod)))
                .ForMember(dest => dest.scope, opt => opt.MapFrom(src => src.ScopeString))
                .ForMember(dest => dest.status, opt => opt.MapFrom(src => src.Status))
                .ForMember(dest => dest.expires_at, opt => opt.MapFrom(src => src.ExpiresAt))
                .ForMember(dest => dest.grant_type,
                    opt => opt.MapFrom(src => src.GrantType))
                .ForMember(dest => dest.token_type,
                    opt => opt.MapFrom(src => src.TokenType))
                .ForMember(dest => dest.error, opt => opt.MapFrom(src => src.Error))
                .ForMember(dest => dest.error_description,
                    opt => opt.MapFrom(src => src.ErrorDescription))
                .ForMember(dest => dest.error_uri, opt => opt.MapFrom(src => src.ErrorUri))
                .ForMember(dest => dest.resourceURI,
                    opt => opt.MapFrom(src => _greenButtonDomain + src.ResourceUri))
                .ForMember(dest => dest.authorizationURI, opt => opt.MapFrom(src => _greenButtonDomain + src.AuthorizationUri))
                .ForAllUnmappedMembers(o => o.Ignore());

            Mapper.CreateMap<GreenButtonAuthorization, AuthorizationEntry>()
                .ForMember(dest => dest.Title, opt => opt.UseValue(GreenButtonAuthorizationTitle))
                .ForMember(dest => dest.GuidId, opt => opt.MapFrom(src => src.UuId))
                .ForMember(dest => dest.Id, opt => opt.MapFrom(src => src.AuthorizationId))
                .ForMember(dest => dest.Links, opt => opt.MapFrom(src => Mapper.Map<List<GreenButtonLink>, List<Link>>(src.Links)))
                .ForMember(dest => dest.Authorization, opt => opt.MapFrom(src => Mapper.Map<GreenButtonAuthorization, Authorization>(src)))
                .ForMember(dest => dest.PublishedDate, opt => opt.MapFrom(src => src.PublishedDate))
                .ForMember(dest => dest.UpdatedDate, opt => opt.MapFrom(src => src.UpdatedDate))
                .ForAllUnmappedMembers(o => o.Ignore());


            Mapper.CreateMap<GreenButtonConnectResult, GreenButtonResult>()
                .ForMember(dest => dest.ZipStream, opt => opt.MapFrom(src => src.ZipStream))
                .ForMember(dest => dest.FileName, opt => opt.MapFrom(src => src.FileName))
                .ForMember(dest => dest.GreenButtonXml, opt => opt.MapFrom(src => src.GreenButtonXml))
                .ForAllUnmappedMembers(o => o.Ignore());
        }

        private List<Link> GenerateIntervalBlockLinks(long subscriptionId, string readingTypeId, string meterId, DateTime date)
        {
            var encryptedMeterId = Helper.ConvertStringToHex(meterId, Encoding.Unicode);
            var encryptedReadingTypeId = Helper.ConvertStringToHex(readingTypeId, Encoding.Unicode);

            var links = new List<Link>();

            // self link
            var link = $"{GreenButtonLinkPrefix}/{GreenButtonEntryType.Subscription}/{subscriptionId}/{GreenButtonEntryType.UsagePoint}/{encryptedMeterId}/{GreenButtonEntryType.MeterReading}/{encryptedReadingTypeId}/{GreenButtonEntryType.IntervalBlock}";
            var intervalBlockId = Helper.ConvertStringToHex($"{readingTypeId}_{date.ToShortDateString()}", Encoding.Unicode);
            var selfLink =
                GenerateGreenButtonLink($"{link}/{intervalBlockId}",
                    GreenButtonLinkRelationshipType.self.ToString());
            links.Add(selfLink);
            // up link
            var upLink = GenerateGreenButtonLink($"{link}", GreenButtonLinkRelationshipType.up.ToString());
            links.Add(upLink);

            return links;
        }

        private List<Link> GenerateMeterReadingLinks(long subscriptionId, string readingTypeId, string meterId)
        {
            var encryptedMeterId = Helper.ConvertStringToHex(meterId, Encoding.Unicode);
            var encryptedReadingTypeId = Helper.ConvertStringToHex(readingTypeId, Encoding.Unicode);

            var links = new List<Link>();

            // self link
            var link = $"{GreenButtonLinkPrefix}/{GreenButtonEntryType.Subscription}/{subscriptionId}/{GreenButtonEntryType.UsagePoint}/{encryptedMeterId}/{GreenButtonEntryType.MeterReading}";
            var selfLink =
                GenerateGreenButtonLink($"{link}/{encryptedReadingTypeId}",
                    GreenButtonLinkRelationshipType.self.ToString());
            links.Add(selfLink);
            // up link
            var upLink = GenerateGreenButtonLink($"{link}", GreenButtonLinkRelationshipType.up.ToString());
            links.Add(upLink);

            return links;
        }

        private decimal GetConsumptionMultiplier(int commodity, int uomId)
        {
            // 0 - electric
            // 1- gas
            // 10 - water
            if (commodity == 0 && uomId == UomidWattHours)
                return 1000;
            if (commodity == 1) // gas
            {
                if (uomId == UomidCubicFeet)
                    return 100;
                return 1000;
            }
            return 1;
        }
        private long ConvertDateTime(DateTime dateTime)
        {
            var value = Convert.ToInt64(Math.Truncate(dateTime.Subtract(DateTime.Parse("1/1/1970")).TotalSeconds));

            return value;
        }

        private Link GenerateGreenButtonLink(string href, string rel)
        {
            var link = new Link
            {
                HyperLinkRef = href,
                HyperLinkRel = rel
            };

            return link;
        }

    }

    public static class GreenButton
    {
        public static long GetResolutionDuration(ResolutionType resolution)
        {
            long duration = 0;

            switch (resolution)
            {
                case ResolutionType.five:
                    duration = (5 * 60);
                    break;

                case ResolutionType.fifteen:
                    duration = (15 * 60);
                    break;

                case ResolutionType.thirty:
                    duration = (30 * 60);
                    break;

                case ResolutionType.hour:
                    duration = (60 * 60);
                    break;

                case ResolutionType.sixhour:
                    duration = (6 * 60 * 60);
                    break;

                case ResolutionType.day:
                    duration = (24 * 60 * 60);
                    break;
            }

            return duration;
        }

        public static GreenButtonLink GenerateGreenButtonLink(string href, string rel)
        {
            var link = new GreenButtonLink
            {
                Href = href,
                Rel = rel
            };

            return link;
        }
    }

}
