﻿using System;
using System.Collections.Generic;
// ReSharper disable once CheckNamespace
namespace CE.Models.Insights
{
    public class GreenButtonUsagePoint
    {
        public long Id { get; set; }
        public String UuId { get; set; }
        public List<GreenButtonLink> Links { get; set; }
        public string MeterId { get; set; }
        public int? TimeConfigurationId { get; set; }

        public List<GreenButtonAmiReadingType> ReadingTypes { get; set; } = new List<GreenButtonAmiReadingType>();
        public DateTime PublishedDate { get; set; }
        public DateTime UpdatedDate { get; set; }

    }
}
