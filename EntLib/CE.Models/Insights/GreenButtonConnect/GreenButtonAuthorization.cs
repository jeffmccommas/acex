﻿using System;
using System.Collections.Generic;
using CE.Models.Insights.Types;

// ReSharper disable once CheckNamespace
namespace CE.Models.Insights
{
    public class GreenButtonAuthorization
    {
        public long AuthorizationId { get; set; }
        public long ApplicationInformationId { get; set; }
        public string UuId { get; set; }
        public List<GreenButtonLink> Links { get; set; }
        public string ClientId { get; set; }

        public long SubscriptionId { get; set; }
        public string ScopeString { get; set; }
        public GreenButtonScope Scope { get; set; }
        public string AccessToken { get; set; }
        public GreenButtonAccessTokenType AccessTokenType { get; set; }
        public DateTime? EndDate { get; set; }
        public bool Active { get; set; }
        public TimePeriod PusblishedTimePeriod { get; set; }
        public TimePeriod AuthorizationTimePeriod { get; set; }
        public int Status { get; set; }
        public long? ExpiresAt { get; set; }
        public string GrantType { get; set; }
        public string TokenType { get; set; } = "Bearer";
        public string Error { get; set; }
        public string AuthorizationScope { get; set; }
        public string ErrorDescription { get; set; }
        public string ErrorUri { get; set; }
        public string ResourceUri { get; set; }
        public string AuthorizationUri { get; set; }
        public DateTime PublishedDate { get; set; }
        public DateTime UpdatedDate { get; set; }
    }

    public class TimePeriod
    {
        public long? Start { get; set; }
        public long? Duration { get; set; }
    }
}
