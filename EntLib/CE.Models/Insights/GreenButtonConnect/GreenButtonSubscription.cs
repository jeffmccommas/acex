﻿using System;
using System.Collections.Generic;
using CE.Models.Insights.Types;

// ReSharper disable once CheckNamespace
namespace CE.Models.Insights
{
    public class GreenButtonSubscription
    {
        public long Id { get; set; }
        public string UuId { get; set; }
        public List<GreenButtonLink> Links { get; set; }
        public long? ApplicationInformationId { get; set; }
        public long? AuthorizationId { get; set; }
        public long? RetailCustomerId { get; set; }
        public string AccountId { get; set; }
        public List<GreenButtonUsagePoint> UsagePoints { get; set; }
        public DateTime PublishedDate { get; set; }
        public DateTime UpdatedDate { get; set; }
    }
}
