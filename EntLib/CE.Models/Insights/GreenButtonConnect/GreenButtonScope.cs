﻿using System.Collections.Generic;
using CE.Models.Insights.Types;

// ReSharper disable once CheckNamespace
namespace CE.Models.Insights
{
    public class GreenButtonScope
    {
        public List<GreenButtonFunctionBlockType> FunctionBlocks { get; set; }
        public List<GreenButtonDurationType>  IntervalDurations { get; set; }
        public GreenButtonDurationType? BlockDuration { get; set; }
        public int AccountCollection { get; set; }
        public int HistoryLength { get; set; }
        public GreenButtonSubscriptionFrequencyType SubscriptionFrequency { get; set; }
        
    }
    
}
