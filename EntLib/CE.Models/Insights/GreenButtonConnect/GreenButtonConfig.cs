﻿// ReSharper disable once CheckNamespace
namespace CE.Models.Insights
{
    public class GreenButtonConfig
    {
        public string ReportTitle { get; set; } = string.Empty;
        public string UsagePointTitle { get; set; } = string.Empty;
        public string IntervalBlockTitle { get; set; } = string.Empty;
        public string UsageSummaryTitle { get; set; } = string.Empty;
        public string ReportPreName { get; set; } = string.Empty;
        public int MaxLoadingTime { get; set; } = 300;
        public bool AxisLabelsShifting { get; set; }
        public string ProcessorInfoFileName { get; set; } = string.Empty;
        public string ElectricDisplayUom { get; set; }
        public string GasDisplayUom { get; set; }
        public string WaterDisplayUom { get; set; }
        public string Country { get; set; }
        public string BillXmlTimeoutError { get; set; }
        public bool ConvertGasUom { get; set; }
        public bool UseDefaultGasConversionFactor { get; set; }
        public double DefaultGasConversionFactor { get; set; }
        public bool ConvertWaterUom { get; set; }
        public bool UseDefaultWaterConversionFactor { get; set; }
        public double DefaultWaterConversionFactor { get; set; }
        public string TzOffsetValue { get; set; }
        public string ProcessInfoFile { get; set; }
        public int BillMonths { get; set; }
        public int MaxQueryDateRange { get; set; } = 1;
    }

    public enum ReportFormat
    {
        Xml = 1,
        Csv = 2
    }

    public enum ReportDataType
    {
        Amr = 1,
        Bill = 2,
        EventData = 3
    }

    public enum RequestType
    {
        ManualDownload = 1,
        ManualUpload = 2
    }
    
}

