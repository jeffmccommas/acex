﻿// ReSharper disable once CheckNamespace
namespace CE.Models.Insights
{
    public class GreenButtonLink
    {
        public string Href { get; set; }
        public string Rel { get; set; }
    }
}
