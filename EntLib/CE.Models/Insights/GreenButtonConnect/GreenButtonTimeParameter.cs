﻿using System;
using System.Collections.Generic;

// ReSharper disable once CheckNamespace
namespace CE.Models.Insights
{
    public class GreenButtonTimeParameter
    {
        public long Id { get; set; }
        public string GuidId { get; set; }
        public List<GreenButtonLink> Links { get; set; } 
        public DateTime PublishedDate { get; set; }
        public DateTime UpdatedDate { get; set; }
        public string DstEndRule { get; set; }
        public string DstStartRule { get; set; }
        public long DstOffset { get; set; }
        public long TzOffset { get; set; }

    }
}
