//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace CE.Models.Insights.EF
{
    using System;
    using System.Collections.Generic;
    
    public partial class ClientProperty
    {
        public int ClientID { get; set; }
        public byte EnvID { get; set; }
        public int PropertyID { get; set; }
        public string PropValue { get; set; }
    
        public virtual Client Client { get; set; }
    }
}
