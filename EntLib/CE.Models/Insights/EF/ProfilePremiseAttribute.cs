//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace CE.Models.Insights.EF
{
    using System;
    using System.Collections.Generic;
    
    public partial class ProfilePremiseAttribute
    {
        public int ClientID { get; set; }
        public string AccountID { get; set; }
        public string PremiseID { get; set; }
        public string AttributeKey { get; set; }
        public string AttributeValue { get; set; }
        public string Source { get; set; }
        public string Criteria { get; set; }
        public System.DateTime EffectiveDate { get; set; }
    }
}
