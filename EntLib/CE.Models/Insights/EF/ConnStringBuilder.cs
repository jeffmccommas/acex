﻿using System.Configuration;
using System.Linq;

namespace CE.Models.Insights.EF
{
    static public class ConnStringBuilder
    {
        private const string AppSetting_PaaSOnly = "PaaSOnly";
        private const string AppSetting_PaaSDedicatedClients = "PaaSDedicatedClients";
        private const string ConnString_InsightsDWEntities = "InsightsDWEntities";

        static public string GetEFConnectionStringInsightsDW(int clientID)
        {
            string connString = string.Empty;
            var dwConnString = ConfigurationManager.ConnectionStrings[ConnString_InsightsDWEntities].ConnectionString;

            //Check if PaaS, and modify connection string
            string sPaaS = ConfigurationManager.AppSettings[AppSetting_PaaSOnly];
            if (!string.IsNullOrEmpty(sPaaS))
            {
                if (System.Convert.ToBoolean(sPaaS))
                {
                    string sPaaSDedicatedClients = ConfigurationManager.AppSettings[AppSetting_PaaSDedicatedClients];
                    if (!string.IsNullOrEmpty(sPaaSDedicatedClients))
                    {
                        var cid = clientID.ToString();
                        if (sPaaSDedicatedClients.Split(',').ToList().Contains(cid))
                        {
                            connString = dwConnString.Replace("InsightsDW;", "InsightsDW_" + cid + ";");
                        }
                        else
                        {
                            connString = dwConnString.Replace("InsightsDW;", "InsightsDW_Test;");
                        }

                    }
                    else
                    {
                        connString = dwConnString.Replace("InsightsDW;", "InsightsDW_Test;");
                    }
                }
                else
                {
                    connString = dwConnString;
                }
            }
            else
            {
                connString = dwConnString;
            }

            return (connString);
        }

    }
}
