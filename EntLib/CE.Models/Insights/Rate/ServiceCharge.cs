﻿using System.Runtime.Serialization;

namespace CE.Models.Insights
{
    [DataContract]
    public class ServiceCharge
    {
        /// <summary>
        /// Calculation type for the service charge, such as Daily or Monthly.
        /// </summary>
        [DataMember(Order = 0)]
        public string CalculationKey { get; set; }

        /// <summary>
        /// Service step for the service charge.
        /// </summary>
        [DataMember(Order = 1)]
        public string StepKey { get; set; }

        /// <summary>
        /// Season for the service charge.
        /// </summary>
        [DataMember(Order = 2)]
        public string SeasonKey { get; set; }

        /// <summary>
        /// Rebate class for the service charge.
        /// </summary>
        [DataMember(Order = 3)]
        public string RebateClassKey { get; set; }

        /// <summary>
        /// Cost type for the service charge.
        /// </summary>
        [DataMember(Order = 4)]
        public string CostKey { get; set; }

        /// <summary>
        /// Cost per unit value for the service charge.
        /// </summary>
        [DataMember(Order = 5)]
        public decimal ChargeValue { get; set; }
    }
}
