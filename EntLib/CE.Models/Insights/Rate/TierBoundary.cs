﻿using System.Runtime.Serialization;


namespace CE.Models.Insights
{
    [DataContract]
    public class TierBoundary
    {
        /// <summary>
        /// Base or tier key for the tier.
        /// </summary>
        [DataMember(Order = 0)]
        public string BaseOrTierKey { get; set; }

        /// <summary>
        /// Time Of Use for the tier.
        /// </summary>
        [DataMember]
        public string TimeOfUseKey { get; set; }

        /// <summary>
        /// Season for the tier.
        /// </summary>
        [DataMember]
        public string SeasonKey { get; set; }

        /// <summary>
        /// Number of days into the season, based on the input date range.
        /// </summary>
        [DataMember(EmitDefaultValue = false)]
        public int DaysIntoSeason { get; set; }

        /// <summary>
        /// The season factor, based on the input date range.
        /// </summary>
        [DataMember(EmitDefaultValue = false)]
        public decimal SeasonFactor { get; set; }
        
        /// <summary>
        /// The threshold for the tier.
        /// </summary>
        [DataMember]
        public decimal Threshold { get; set; }

        /// <summary>
        /// Secondary threshold for the tier.
        /// </summary>
        [DataMember(EmitDefaultValue = false)]
        public decimal SecondaryThreshold { get; set; }

    }
}
