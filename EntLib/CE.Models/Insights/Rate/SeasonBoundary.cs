﻿using System.Runtime.Serialization;

namespace CE.Models.Insights
{
    [DataContract]
    public class SeasonBoundary
    {
        /// <summary>
        /// Season key.
        /// </summary>
        [DataMember(Order = 0)]
        public string SeasonKey { get; set; }

        /// <summary>
        /// Start day of the season boundary.
        /// </summary>
        [DataMember(Order = 1)]
        public string StartDay { get; set; }

        /// <summary>
        /// End day of the season boundary.
        /// </summary>
        [DataMember(Order = 2)]
        public string EndDay { get; set; }
    }
}
