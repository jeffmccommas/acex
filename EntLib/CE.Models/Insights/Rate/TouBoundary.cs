﻿using System.Runtime.Serialization;

namespace CE.Models.Insights
{
    [DataContract]
    public class TouBoundary
    {
        /// <summary>
        /// Time of use key.
        /// </summary>
        [DataMember(Order = 0)]
        public string TimeOfUseKey { get; set; }

        /// <summary>
        /// Season for the TOU boundary.
        /// </summary>
        [DataMember(Order = 1)]
        public string SeasonKey { get; set; }

        /// <summary>
        /// Day type for the TOU boundary.
        /// </summary>
        [DataMember(Order = 2)]
        public string DayKey { get; set; }

        /// <summary>
        /// Start time of the TOU boundary.
        /// </summary>
        [DataMember(Order = 3)]
        public string StartTime { get; set; }

        /// <summary>
        /// End time of the TOU boundary.
        /// </summary>
        [DataMember(Order = 4)]
        public string EndTime { get; set; }
    }
}
