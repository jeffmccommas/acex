﻿using System.Runtime.Serialization;

namespace CE.Models.Insights
{
    [DataContract]
    public class RateClass
    {
        /// <summary>
        /// Rate class id.
        /// </summary>
        [DataMember(Order = 0)]
        public string RateClassID { get; set; }

        /// <summary>
        ///  Rate class name.
        /// </summary>
        [DataMember(Order = 1)]
        public string Name { get; set; }

        /// <summary>
        /// Rate class description.
        /// </summary>
        [DataMember(Order = 2)]
        public string Description { get; set; }

        /// <summary>
        ///  Commodity for the rate class.
        /// </summary>
        [DataMember(Order = 3)]
        public string CommodityKey { get; set; }

        /// <summary>
        /// Customer Type: Business/Residential/Farm.
        /// </summary>
        [DataMember(Order = 4)]
        public string CustomerType { get; set; }

        /// <summary>
        /// Returns true if the rate class includes a Peak Time Rebate, else returns false.
        /// </summary>
        [DataMember(Order = 5)]
        public bool ContainsPTR { get; set; }

        /// <summary>
        /// Returns true if the rate class includes tiered prices, else returns false.
        /// </summary>
        [DataMember(Order = 6)]
        public bool IsTiered { get; set; }

        /// <summary>
        /// Returns true if the rate class includes time of use rates, else returns false.
        /// </summary>
        [DataMember(Order = 7)]
        public bool IsTimeOfUse { get; set; }

        /// <summary>
        ///  Returns true if the rate class includes seasonal rates, else returns false. 
        /// </summary>
        [DataMember(Order = 8)]
        public bool IsSeasonal { get; set; }

        /// <summary>
        ///  The unit of measure authored in rate engine.
        /// </summary>
        [DataMember(Order = 9)]
        public string UomKey { get; set; }
    }
}
