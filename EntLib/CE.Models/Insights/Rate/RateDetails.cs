﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace CE.Models.Insights
{
    [DataContract]
    public class RateDetails
    {
        /// <summary>
        ///  Use charges included in the rate definition.
        /// </summary>
        [DataMember(Order = 0)]
        public List<UseCharge> UseCharges { get; set; }

        /// <summary>
        /// Service charges included in the rate definition.
        /// </summary>
        [DataMember(EmitDefaultValue = false, Order = 1)]
        public List<ServiceCharge> ServiceCharges { get; set; }

        /// <summary>
        /// Tier boundaries included in the rate definition.
        /// </summary>
        [DataMember(EmitDefaultValue = false, Order = 2)]
        public List<TierBoundary> TierBoundaries { get; set; }

        /// <summary>
        /// Finalized tier boundaries calculated based on the dates in the request.
        /// </summary>
        [DataMember(EmitDefaultValue = false, Order = 3)]
        public List<TierBoundary> FinalizedTierBoundaries { get; set; }

        /// <summary>
        ///  Time-of-use boundaries included in the rate definition.
        /// </summary>
        [DataMember(EmitDefaultValue = false, Order = 4)]
        public List<TouBoundary> TouBoundaries { get; set; }

        /// <summary>
        ///  Season boundaries included in the rate definition.
        /// </summary>
        [DataMember(EmitDefaultValue = false, Order = 5)]
        public List<SeasonBoundary> SeasonBoundaries { get; set; }

        /// <summary>
        /// Minimum charges included in the rate definition.
        /// </summary>
        [DataMember(EmitDefaultValue = false, Order = 6)]
        public  List<MinimumCharge> MinimumCharges { get; set; }

        /// <summary>
        /// Tax charges included in the rate definition.
        /// </summary>
        [DataMember(EmitDefaultValue = false, Order = 7)]
        public List<TaxCharge> TaxCharges { get; set; }
    }
}
