﻿using System.Runtime.Serialization;

namespace CE.Models.Insights
{
    [DataContract]
    public class UseCharge
    {
        /// <summary>
        /// Base or tier for the use charge.
        /// </summary>
        [DataMember(Order = 0)]
        public string BaseOrTierKey { get; set; }

        /// <summary>
        /// Time of use period for the use charge.
        /// </summary>
        [DataMember(Order = 1)]
        public string TimeOfUseKey { get; set; }

        /// <summary>
        /// Season for the use charge.
        /// </summary>
        [DataMember(Order = 2)]
        public string SeasonKey { get; set; }

        /// <summary>
        /// Part type for the use charge, such as “Primary” or “Secondary”.
        /// </summary>
        [DataMember(Order = 3)]
        public string PartKey { get; set; }

        /// <summary>
        /// Cost type for the use charge.
        /// </summary>
        [DataMember(Order = 4)]
        public string CostKey { get; set; }

        /// <summary>
        /// Returns True if the use charge is fuel cost recovery, else False.
        /// </summary>
        [DataMember(Order = 5)]
        public bool IsFuelCostRecovery { get; set; }

        /// <summary>
        /// Fuel Cost Recovery group for the use charge
        /// </summary>
        [DataMember(EmitDefaultValue = false, Order =6)]
        public int FcrGroup { get; set; }

        /// <summary>
        /// Returns True if the use charge is real time pricing, else False.
        /// </summary>
        [DataMember(Order = 7)]
        public bool IsRealTimePricing { get; set; }

        /// <summary>
        /// Real Time Pricing group for the use charge.
        /// </summary>
        [DataMember(EmitDefaultValue = false, Order = 8)]
        public int RtpGroup { get; set; }

        /// <summary>
        /// Cost per unit value for the use charge.
        /// </summary>
        [DataMember(Order = 9)]
        public decimal ChargeValue { get; set; }
    }
}
