﻿using System.Runtime.Serialization;

namespace CE.Models.Insights
{
    [DataContract]
    public class TaxCharge
    {
        /// <summary>
        /// Cost type for the charge.
        /// </summary>
        [DataMember(Order = 0)]
        public string CostKey { get; set; }

        /// <summary>
        /// Cost per unit value for the charge.
        /// </summary>
        [DataMember(Order = 1)]
        public decimal ChargeValue { get; set; }
    }
}
