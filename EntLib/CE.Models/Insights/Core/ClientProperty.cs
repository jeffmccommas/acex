﻿
namespace CE.Models
{
    public class ClientProperties
    {
        public ClientProperties(PropertyType propertyId, string name)
        {
            Id = propertyId;
            Name = name;
        }

        public PropertyType Id { get; set; }
        public string Name { get; set; }
    }
}
