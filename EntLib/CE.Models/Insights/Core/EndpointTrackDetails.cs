﻿using System.Collections.Generic;
using CE.Models.Insights;

namespace CE.Models
{
    /// <summary>
    /// Total number of calls to each endpoint.
    /// </summary>
    public class EndpointTrackDetails 
    {        
        /// <summary>
        /// The name of the endpoint.
        /// </summary>
        public string EndpointName { get; set; }
        /// <summary>
        /// Total number of calls to the endpoint split out by verb (GET, POST, PUT and DELETE)
        /// </summary>
        public List<EndpointVerb> Verbs { get; set; }
        /// <summary>
        /// Total number of calls to the endpoint.
        /// </summary>
        public int Total { get; set; }
    }
}
