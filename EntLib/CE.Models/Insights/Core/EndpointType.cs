﻿
namespace CE.Models
{
    /// <summary>
    /// Existing endpoints supported in code.  
    /// </summary>
    public enum EndpointType
    {
        unspecified,
        echo,
        billdisagg,
        benchmark,
        endpointtracking,
        profile,
        content,
        action,
        actionplan,
        bill,
        greenbuttonami,
        consumption,
        webtoken,
        energymodel,
        insights,
        billtodate,
        customerinfo,
        rate,
        subscription
    }
}
