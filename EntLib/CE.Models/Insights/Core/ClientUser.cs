﻿using System.Collections.Generic;

namespace CE.Models
{
    /// <summary>
    /// Client's User information.
    /// </summary>
    public class ClientUser
    {
        public int ClientID { get; set; }
        public int UserID { get; set; }
        public int RateCompanyID { get; set; }
        public int ReferrerID { get; set; }
        public string CEAccessKeyID { get; set; }
        public string CESecretAccessKey { get; set; }
        public string BasicKey { get; set; }
        public AuthType ClientAuthType { get; set; }
        public EnvironmentType Environment { get; set; }
        public bool Enabled { get; set; }
        public List<Endpoint> Endpoints { get; set; }
        public List<Role> Roles { get; set; }
        public List<ClientProperties> ClientProperties { get; set; }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="propertyId"></param>
        /// <returns></returns>
        public string GetClientPropertyNameValue(PropertyType propertyId)
        {
            string result = null;
            var clientProperty = this.ClientProperties.Find(p => p.Id == PropertyType.CMS_Space);
            if ((clientProperty != null) && (!string.IsNullOrEmpty(clientProperty.Name)))
            {
                result = clientProperty.Name;
            }
            return result;
        }
    }

}
