﻿
namespace CE.Models
{
    public enum AuthType
    {
        CE,
        Basic,
        OAuth,
        OAuth2,
        Bearer
    }
}
