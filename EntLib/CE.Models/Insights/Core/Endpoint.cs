﻿
namespace CE.Models
{
    public class Endpoint
    {
        public Endpoint(EndpointType endpointId, string name, string shortName)
        {
            Id = endpointId;
            Name = name;
            ShortName = shortName;
        }

        public EndpointType Id { get; set; }
        public string Name { get; set; }
        public string ShortName { get; set; }
    }
}
