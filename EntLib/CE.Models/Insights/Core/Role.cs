﻿
namespace CE.Models
{
    public class Role
    {
        public Role(RoleType roleId, string name)
        {
            Id = roleId;
            Name = name;
        }

        public RoleType Id { get; set; }
        public string Name { get; set; }
    }
}
