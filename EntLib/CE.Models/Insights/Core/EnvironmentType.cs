﻿
namespace CE.Models
{
    public enum EnvironmentType
    {
        prod,
        uat,
        qa,
        dev,
        localdev,
        perf
    }
}
