﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace CE.Models.Insights
{
    /// <summary>
    /// The number of calls to a specific endpoint and verb, plus totals broken out by user.
    /// </summary>
    [DataContract]
    public class EndpointVerb
    {
        /// <summary>
        /// Name of the endpoint verb
        /// </summary>
        [DataMember(Order = 0)]
        public string Name { get; set; }
        /// <summary>
        /// Collection of totals by user.
        /// </summary>
        [DataMember(Order = 1)]
        public List<EndpointUser> Users;
    }
}
