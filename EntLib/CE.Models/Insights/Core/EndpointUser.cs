﻿namespace CE.Models
{
    /// <summary>
    /// The total number of Insights API calls for a specific endpoint and verb, broken down by user.
    /// </summary>
    public class EndpointUser
    {
        /// <summary>
        /// Id of the Insights API user
        /// </summary>
        public int UserId { get; set; }
        /// <summary>
        /// Name of the Insights API user
        /// </summary>
        public string UserName { get; set; }
        /// <summary>
        /// Total number of Insights API calls for this user
        /// </summary>
        public int Total { get; set; }
    }
}
