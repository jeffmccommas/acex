﻿
namespace CE.Models
{
    public class EndpointTrack
    {
        public int ClientId { get; set; }
        public int UserId { get; set; }
        public string ThirdPartyClientId { get; set; }
        public string Verb { get; set; }
        public string ResourceName { get; set; }
        public int? EndpointId { get; set; }
        public string UserAgent { get; set; }
        public string SourceIP { get; set; }
        public string XCEMessageId { get; set; }
        public string XCEChannel { get; set; }
        public string XCELocale { get; set; }
        public string XCEMeta { get; set; }
        public string QueryString { get; set; }
        public bool Include { get; set; }

    }
}
