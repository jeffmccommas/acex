﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace CE.Models.Insights
{
    /// <summary>
    /// Insights subscription information for the premise.
    /// </summary>
    [DataContract]
    public class PremiseInsight
    {
        /// <summary>
        /// Id of the utility premise.
        /// </summary>
        [DataMember(Order = 0)]
        [Required]
        public string Id { get; set; }

        /// <summary>
        /// List of insights available for the premise, along with subscription status.
        /// </summary>
        [DataMember(Order = 2, EmitDefaultValue = false)]
        public List<Insight> Insight { get; set; }

        /// <summary>
        /// Insight subscription information for services under this premise.
        /// </summary>
        [DataMember(Order = 3)]
        public List<ServiceInsight> Services { get; set; }
        
    }
}
