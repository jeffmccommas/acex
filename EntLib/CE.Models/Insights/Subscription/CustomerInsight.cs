﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace CE.Models.Insights
{
    /// <summary>
    /// Insights subscription information for the customer.
    /// </summary>
    [DataContract]
    public class CustomerInsight
    { 
        /// <summary>
        /// Id of the utility customer.
        /// </summary>
        [DataMember(Order = 0)]
        [Required]
        public string Id { get; set; }
        /// <summary>
        /// Email address of the utility customer. 
        /// </summary>
        [DataMember(Order = 1)]
        public string Email { get; set; }
        /// <summary>
        /// Phone number of the utility customer.
        /// </summary>
        [DataMember(Order = 2)]
        public string Phone { get; set; }

        /// <summary>
        /// List of insights available for the customer, along with subscription status.
        /// </summary>
        [DataMember(Order = 3, EmitDefaultValue = false)]
        public List<Insight> Insights { get; set; }

        /// <summary>
        /// Insight subscription information for accounts under this customer
        /// </summary>
        [DataMember(Order = 4)]
        public List<AccountInsight> Accounts { get; set; }
    }
}
