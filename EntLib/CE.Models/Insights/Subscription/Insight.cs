﻿using System.Runtime.Serialization;

namespace CE.Models.Insights
{
    /// <summary>
    /// Subscription details for a customer for a specific insight/alert.
    /// </summary>
    [DataContract]
    public class Insight
    {
        /// <summary>
        /// Insight Name - ProgramName.InsightName
        /// </summary>
        [DataMember(EmitDefaultValue = false)]
        public string Name { get; set; }

        /// <summary>
        /// Subscription level for the insight. Valid values are “account” and “service”. 
        /// Account-level insights apply to total costs for the account, where service-level insights apply to costs or usage for a specific service contract. 
        /// </summary>
        [DataMember(EmitDefaultValue = false)]
        public string Level { get; set; }

        /// <summary>
        /// Equals “true” if the customer is subscribed to the insight, else “false”.
        /// </summary>
        [DataMember]
        public bool IsSelected { get; set; }

        /// <summary>
        /// All the avaliable channels are allowed to use for the insight.
        /// </summary>
        [DataMember(EmitDefaultValue = false)]
        public string AllowedChannels { get; set; }

        /// <summary>
        /// The channel to deliver the insight.
        /// </summary>
        [DataMember(EmitDefaultValue = false)]
        public string Channel { get; set; }

        /// <summary>
        /// The threshold type for this insight, cost, usage or percent.
        /// </summary>
        [DataMember(EmitDefaultValue = false)]
        public string ThresholdType { get; set; }

        /// <summary>
        /// Threshold value for the insight. Thresholds can be usage quantities or costs. Note that not all insights require thresholds. 
        /// </summary>
        [DataMember(EmitDefaultValue = false)]
        public decimal? Threshold { get; set; }

        /// <summary>
        /// Min threshold for validation on the widget
        /// </summary>
        [DataMember(EmitDefaultValue = false)]
        public decimal? ThresholdMin { get; set; }

        /// <summary>
        /// Max threshold for validation on the widget</summary>
        [DataMember(EmitDefaultValue = false)]
        public decimal? ThresholdMax { get; set; }


        /// <summary>
        /// The uom key for this insight.
        /// </summary>
        [DataMember(EmitDefaultValue = false)]
        public string UomKey { get; set; }

        /// <summary>
        /// Is the email double opt-in process completed
        /// If this is not provided then it will default to false. 
        /// </summary>
        [DataMember(EmitDefaultValue = false)]
        public bool? IsEmailOptInCompleted { get; set; }

        /// <summary>
        /// Is the sms double opt-in process completed
        /// If this is not provided then it will default to false. 
        /// </summary>
        [DataMember(EmitDefaultValue = false)]
        public bool? IsSmsOptInCompleted { get; set; }
    }
}
