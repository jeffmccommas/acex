﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace CE.Models.Insights
{
    /// <summary>
    /// Insights subscription information for the service.
    /// </summary>
    [DataContract]
    public class ServiceInsight 
    {
        /// <summary>
        /// Id of the utility service.
        /// </summary>
        [DataMember(Order = 0)]
        [Required]
        public string Id { get; set; }

        ///// <summary>
        ///// Unique service contract Id of the utility service.
        ///// </summary>
        //[DataMember(Order = 1)]
        //[Required]
        //public string ContractId { get; set; }

        /// <summary>
        /// The commodity to which the insight applies.
        /// </summary>
        [DataMember(Order = 2)]
        public string CommodityKey { get; set; }

        /// <summary>
        /// List of insights available for the service, along with subscription status. 
        /// </summary>
        [DataMember(Order = 3 , EmitDefaultValue = false)]
        public List<Insight> Insights { get; set; }
    }
}
