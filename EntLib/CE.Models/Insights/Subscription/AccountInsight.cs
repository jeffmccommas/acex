﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace CE.Models.Insights
{
    /// <summary>
    /// Insights subscription information for the account.
    /// </summary>
    [DataContract]
    public class AccountInsight 
    {
        /// <summary>
        /// Id of the utility account.
        /// </summary>
        [DataMember(Order = 0)]
        [Required]
        public string Id { get; set; }

        /// <summary>
        /// List of insights available for the account, along with subscription status. 
        /// </summary>
        [DataMember(Order = 2, EmitDefaultValue = false)]
        public List<Insight> Insights { get; set; }

        /// <summary>
        /// Insight subscription information for premises under this account.
        /// </summary>
        [DataMember(Order = 1)]
        public List<PremiseInsight> Premises { get; set; }
    }
}
