﻿using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

// ReSharper disable once CheckNamespace
namespace CE.Models.Insights
{
    /// <summary>
    /// The customer's subscription status for a specific insight.
    /// </summary>
    [DataContract]
    public class SubscribedInsight
    {
        /// <summary>
        /// Subscribed Insight Name - ProgramName.InsightName
        /// </summary>
        [DataMember(EmitDefaultValue = false)]
        public string Name { get; set; }

        /// <summary>
        /// Utility account id
        /// </summary>
        [DataMember]
        [Required]
        public string AccountId { get; set; }

        /// <summary>
        /// Utility premise id. This is required for service-level insights. 
        /// </summary>
        [DataMember]
        public string PremiseId { get; set; }

        /// <summary>
        /// Utility service id (service contract id). This is required for service-level insights. 
        /// </summary>
        [DataMember]
        public string ServiceId { get; set; }

        ///// <summary>
        ///// Unique Service contract id. This is required for service-level insights. 
        ///// </summary>
        //[DataMember]
        //public string ServiceContractId { get; set; }

        /// <summary>
        /// Indicates whether the insight is evaluated for the account or for a specific service. Valid values are Account or Service.
        /// </summary>
        [DataMember(EmitDefaultValue = false)]
        public string Level { get; set; }

        /// <summary>
        /// The status of the subscription. This will be True if the subscription is active, else it will be False.
        /// </summary>
        [DataMember(EmitDefaultValue = false)]
        public bool IsSelected { get; set; }
        
        /// <summary>
        /// Threshold value for the insight. 
        /// If this is provided then it will be validated against the minimum and maximum values configured for the insight. 
        /// If it is not provided, and the insight uses a threshold, the configured default value will be used. 
        /// </summary>
        [DataMember(EmitDefaultValue = false)]
        public decimal? Threshold { get; set; }

        /// <summary>
        /// The communication channel for the insight. 
        /// If this is not provided then it will default to the configured default communication channel. 
        /// </summary>
        [DataMember(EmitDefaultValue = false)]
        public string Channel { get; set; }

        ///// <summary>
        ///// Is the email single opt-in process completed
        ///// If this is not provided then it will default to false. 
        ///// </summary>
        //[DataMember(EmitDefaultValue = false)]
        //public bool? IsEmailOptInCompleted { get; set; }

        ///// <summary>
        ///// Is the sms single opt-in process completed
        ///// If this is not provided then it will default to false. 
        ///// </summary>
        //[DataMember(EmitDefaultValue = false)]
        //public bool? IsSmsOptInCompleted { get; set; }

    }
}
