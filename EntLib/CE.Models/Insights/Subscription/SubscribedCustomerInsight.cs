﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

// ReSharper disable once CheckNamespace
namespace CE.Models.Insights
{
    /// <summary>
    /// Subscribed Insights for the customer
    /// </summary>
    public class SubscribedCustomerInsight
    { 
        /// <summary>
        /// Id of the utility customer.
        /// </summary>
        [DataMember(Order = 0)]
        [Required]
        public string Id { get; set; }
        /// <summary>
        /// Email address of the utility customer. 
        /// </summary>
        [DataMember(Order = 1, EmitDefaultValue = false)]
        public string Email { get; set; }
        /// <summary>
        /// Phone number of the utility customer.
        /// </summary>
        [DataMember(Order = 2, EmitDefaultValue = false)]
        public string Phone { get; set; }

        /// <summary>
        /// List of Subscribed Insights
        /// </summary>
        [DataMember(Order = 3, EmitDefaultValue = false)]
        public List<SubscribedInsight> Insights { get; set; }
    }
}
