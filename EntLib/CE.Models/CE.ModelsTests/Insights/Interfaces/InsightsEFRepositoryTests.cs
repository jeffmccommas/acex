﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Text;
using CE.Models.Insights;
using CE.Models.Insights.Types;
using CE.ModelsTests.Insights.Helpers;
using Moq;

// ReSharper disable once CheckNamespace
namespace CE.Models.Tests
{
    [TestClass()]
    [DeploymentItem("EntityFramework.SqlServer.dll")]
    public class InsightsEfRepositoryTests : InsightsEntitiesMock
    {
        private MockInsightMetadataEntities _context;
        public TestContext TestContext { get; set; }

        [TestInitialize]
        public void TestInit()
        {
            _context = new MockInsightMetadataEntities();
        }

        [TestMethod]
        public void GetWeatherTest()
        {

            var clientId = 87;
            var zipcode = "02481";
            var startDate = Convert.ToDateTime("6/5/2016");
            var endDate = Convert.ToDateTime("6/10/2016");

            var request = new WeatherRequest
            {
                ZipCode = zipcode,
                StartDate = startDate,
                EndDate = endDate
            };

            var insightRepository = new InsightsEfRepository(_context);

            var result = insightRepository.GetWeather(clientId, request);

            Assert.IsNotNull(result);
            Assert.AreEqual(6, result.Count);
        }


        [TestMethod]
        [DeploymentItem("Insights\\TestData\\GetGreenButtonAmiSubscriptionTestData.xml")]
        [DataSource("Microsoft.VisualStudio.TestTools.DataSource.XML", "|DataDirectory|\\GetGreenButtonAmiSubscriptionTestData.xml", "Subscription", DataAccessMethod.Sequential)]
        public void GetGreenButtonAmiSubscriptionTest()
        {
            try
            {
                var subscriptionId = Convert.ToInt64(TestContext.DataRow["Id"].ToString());
                var expectedUuId = TestContext.DataRow["ExpectedUuId"].ToString();
                var expected = Convert.ToBoolean(TestContext.DataRow["result"].ToString());


                var insightRepository = new InsightsEfRepository(MockInsightsEntities.Object);
                var result = insightRepository.GetGreenButtonAmiSubscription(subscriptionId);

                if (expected)
                {
                    Assert.IsNotNull(result);
                    Assert.AreEqual(expectedUuId, result.UuId);
                }
                else
                {
                    Assert.IsNull(result);
                }
            }
            catch (Exception ex)
            {
                Assert.Fail(ex.Message);
            }


        }

        [TestMethod]
        [DeploymentItem("Insights\\TestData\\GreenButtonSubscriptionValidation.xml")]
        [DataSource("Microsoft.VisualStudio.TestTools.DataSource.XML",
            "|DataDirectory|\\GreenButtonSubscriptionValidation.xml", "Subscription", DataAccessMethod.Sequential)]
        public void IsValidSubscriptionTest()
        {
            try
            {
                var subscriptionId = Convert.ToInt64(TestContext.DataRow["Id"].ToString());
                var meterId = TestContext.DataRow["MeterId"].ToString();
                var expected = Convert.ToBoolean(TestContext.DataRow["result"].ToString());

                var insightRepository = new InsightsEfRepository(MockInsightsEntities.Object);

                var result = string.IsNullOrEmpty(meterId)
                    ? insightRepository.IsSubscriptionValid(subscriptionId)
                    : insightRepository.IsSubscriptionValid(subscriptionId, meterId);

                Assert.IsNotNull(result);
                Assert.AreEqual(expected, result);
            }
            catch (Exception ex)
            {
                Assert.Fail(ex.Message);
            }
        }

        [TestMethod]
        [DeploymentItem("Insights\\TestData\\GetUsagePointTestData.xml")]
        [DataSource("Microsoft.VisualStudio.TestTools.DataSource.XML", "|DataDirectory|\\GetUsagePointTestData.xml", "UsagePoint", DataAccessMethod.Sequential)]
        public void GetGreenButtonAmiUsagePointTest()
        {
            try
            {
                var subscriptionId = Convert.ToInt32(TestContext.DataRow["SubscirptionId"].ToString());
                var meterId = TestContext.DataRow["MeterId"].ToString();
                var expected = Convert.ToBoolean(TestContext.DataRow["result"].ToString());


                var insightRepository = new InsightsEfRepository(MockInsightsEntities.Object);
                var result = insightRepository.GetGreenButtonAmiUsagePoint(subscriptionId, meterId);

                if (expected)
                {
                    Assert.IsNotNull(result);
                }
                else
                {
                    Assert.IsNull(result);
                }
            }
            catch (Exception ex)
            {
                Assert.Fail(ex.Message);
            }


        }

        [TestMethod]
        [DeploymentItem("Insights\\TestData\\GetUsagePointsTestData.xml")]
        [DataSource("Microsoft.VisualStudio.TestTools.DataSource.XML", "|DataDirectory|\\GetUsagePointsTestData.xml", "Subscription", DataAccessMethod.Sequential)]
        public void GetGreenButtonAmiUsagePointsTest() {
            try {
                var subscriptionId = Convert.ToInt32(TestContext.DataRow["SubscirptionId"].ToString());
                var expected = Convert.ToInt32(TestContext.DataRow["result"].ToString());

                var scope = new GreenButtonScope {
                    FunctionBlocks = new List<GreenButtonFunctionBlockType> { GreenButtonFunctionBlockType.IntervalElecMetering }
                };

                var insightRepository = new InsightsEfRepository(MockInsightsEntities.Object);
                var result = insightRepository.GetGreenButtonAmiUsagePoints(subscriptionId, scope);

                Assert.IsNotNull(result);
                Assert.AreEqual(expected, result.Count);
            }
            catch (Exception ex) {
                Assert.Fail(ex.Message);
            }


        }


        [TestMethod]
        [DeploymentItem("Insights\\TestData\\UpdateLocalTimeConfigurationTestData.xml")]
        [DataSource("Microsoft.VisualStudio.TestTools.DataSource.XML", "|DataDirectory|\\UpdateLocalTimeConfigurationTestData.xml", "TimeConfiguration", DataAccessMethod.Sequential)]
        public void UpdateLocalTimeConfigurationTest()
        {
            try
            {
                var usagePointId = Convert.ToInt32(TestContext.DataRow["UsagePointId"].ToString());
                var timeZone = TestContext.DataRow["TimeZone"].ToString();
                TimeZoneInfo tzInfo = TimeZoneInfo.FindSystemTimeZoneById(timeZone);
                var expected = Convert.ToInt32(TestContext.DataRow["TimeConfigurationId"].ToString());

                MockInsightsEntities.Setup(i => i.SaveChanges()).Verifiable();


                var insightRepository = new InsightsEfRepository(MockInsightsEntities.Object);
                
                var result = insightRepository.UpdateLocalTimeConfiguration(usagePointId, tzInfo);
                
                MockInsightsEntities.Verify(i => i.SaveChanges(), Times.Once);
                
                Assert.IsNotNull(result);
                Assert.AreEqual(expected, result);
                
            }
            catch (Exception ex)
            {
                Assert.Fail(ex.Message);
            }
        }

        [TestMethod]
        [DeploymentItem("Insights\\TestData\\GetUsagePointRelatedLinksTestData.xml")]
        [DataSource("Microsoft.VisualStudio.TestTools.DataSource.XML", "|DataDirectory|\\GetUsagePointRelatedLinksTestData.xml", "RelatedLink", DataAccessMethod.Sequential)]
        public void GetGreenButtonAmiUsagePointRelatedLinksTest()
        {
            try
            {
                var usagePointId = Convert.ToInt32(TestContext.DataRow["UsagePointId"].ToString());
                var selfLink = TestContext.DataRow["SelfLink"].ToString();
                var timeConfigId = Convert.ToInt32(TestContext.DataRow["TimeConfigurationId"].ToString());
                var saveChange = Convert.ToBoolean(TestContext.DataRow["SaveChange"].ToString());
                var expected = Convert.ToInt32(TestContext.DataRow["result"].ToString());


                var usagePoint = new GreenButtonUsagePoint
                {
                    Id = usagePointId,
                    TimeConfigurationId = timeConfigId,
                    Links = new List<GreenButtonLink> {new GreenButtonLink {Href = selfLink, Rel = "self"}}
                };

                MockInsightsEntities.Setup(i => i.SaveChanges()).Verifiable();

                var insightRepository = new InsightsEfRepository(MockInsightsEntities.Object);

                var result = insightRepository.GetGreenButtonAmiUsagePointRelatedLinks(usagePoint);

                Assert.IsNotNull(result);
                if (saveChange)
                    MockInsightsEntities.Verify(i => i.SaveChanges(), Times.Once);
                else
                {
                    MockInsightsEntities.Verify(i => i.SaveChanges(), Times.Never);
                }
                Assert.AreEqual(expected, result.Count);

            }
            catch (Exception ex)
            {
                Assert.Fail(ex.Message);
            }
        }

        [TestMethod]
        [DeploymentItem("Insights\\TestData\\GetGreenButtonAmiReadingTypesTestData.xml")]
        [DataSource("Microsoft.VisualStudio.TestTools.DataSource.XML", "|DataDirectory|\\GetGreenButtonAmiReadingTypesTestData.xml", "ReadingType", DataAccessMethod.Sequential)]
        public void GetGreenButtonAmiReadingTypesTest()
        {
            try
            {
                var meterId = TestContext.DataRow["MeterId"].ToString();
                var expected = Convert.ToInt32(TestContext.DataRow["result"].ToString());
                
                var insightRepository = new InsightsEfRepository(MockInsightsEntities.Object);

                var result = insightRepository.GetGreenButtonAmiReadingTypes(meterId);

                Assert.IsNotNull(result);
                Assert.AreEqual(expected, result.Count);

            }
            catch (Exception ex)
            {
                Assert.Fail(ex.Message);
            }
        }

        [TestMethod]
        [DeploymentItem("Insights\\TestData\\GetGreenButtonAmiReadignTypeTestData.xml")]
        [DataSource("Microsoft.VisualStudio.TestTools.DataSource.XML", "|DataDirectory|\\GetGreenButtonAmiReadignTypeTestData.xml", "ReadingType", DataAccessMethod.Sequential)]
        public void GetGreenButtonAmiReadingTypeTest()
        {
            try
            {
                var subscriptionId = Convert.ToInt64(TestContext.DataRow["SubscriptionId"].ToString());
                var meterId = TestContext.DataRow["meterId"].ToString();
                var readingTypeId = TestContext.DataRow["ReadingTypeId"].ToString();
                var hexedReadingTypeId = GreenButtonConnect.Helper.ConvertStringToHex(readingTypeId, Encoding.Unicode);
                var expected = Convert.ToBoolean(TestContext.DataRow["hasReadingType"].ToString());

                var insightRepository = new InsightsEfRepository(MockInsightsEntities.Object);

                var result = insightRepository.GetGreenButtonAmiReadingType(subscriptionId,meterId, hexedReadingTypeId);

                if(expected)
                    Assert.IsNotNull(result);
                else
                    Assert.IsNull(result);

            }
            catch (Exception ex)
            {
                Assert.Fail(ex.Message);
            }
        }

        [TestMethod]
        [DeploymentItem("Insights\\TestData\\GetGreenButtonAmiReadingTypesTestData.xml")]
        [DataSource("Microsoft.VisualStudio.TestTools.DataSource.XML", "|DataDirectory|\\GetGreenButtonAmiReadingTypesTestData.xml", "ReadingType", DataAccessMethod.Sequential)]
        public void InsertGreenButtonAmiReadingTypesTest()
        {
            try
            {
                var clientId = Convert.ToInt32(TestContext.DataRow["ClientId"].ToString());
                var meterId = TestContext.DataRow["MeterId"].ToString();

                MockInsightsEntities.Setup(i => i.SaveChanges()).Verifiable();
                var insightRepository = new InsightsEfRepository(MockInsightsEntities.Object);

                var readingTypes = insightRepository.GetGreenButtonAmiReadingTypes(meterId);
                if (readingTypes.Count > 0)
                {
                    insightRepository.InsertGreenButtonAmiReadingTypes(clientId,meterId,readingTypes);
                    MockInsightsEntities.Verify(i => i.SaveChanges(), Times.AtLeastOnce);
                }
            }
            catch (Exception ex)
            {
                Assert.Fail(ex.Message);
            }
        }
        

        [TestMethod]
        [DeploymentItem("Insights\\TestData\\GetLocalTimeConfigurationTestData.xml")]
        [DataSource("Microsoft.VisualStudio.TestTools.DataSource.XML", "|DataDirectory|\\GetLocalTimeConfigurationTestData.xml", "TimeConfiguration", DataAccessMethod.Sequential)]
        public void GetLocalTimeParamTest()
        {
            try
            {
                var timeConfigId = Convert.ToInt32(TestContext.DataRow["TimeConfigurationId"].ToString());
                var dstEndRule = TestContext.DataRow["dstEndRule"].ToString();
                var dstStartRule = TestContext.DataRow["dstStartRule"].ToString();
                var dstOffSet = Convert.ToInt64(TestContext.DataRow["dstOffSet"].ToString());
                var tzOffSet = Convert.ToInt64(TestContext.DataRow["tzOffSet"].ToString());

                MockInsightsEntities.Setup(i => i.SaveChanges()).Verifiable();


                var insightRepository = new InsightsEfRepository(MockInsightsEntities.Object);

                var result = insightRepository.GetGreenButtonTimeParam(timeConfigId);
                
                Assert.IsNotNull(result);
                Assert.IsNotNull(result.Links);
                Assert.AreEqual(2, result.Links.Count);
                Assert.AreEqual(dstEndRule, result.DstEndRule);
                Assert.AreEqual(dstStartRule, result.DstStartRule);
                Assert.AreEqual(dstOffSet, result.DstOffset);
                Assert.AreEqual(tzOffSet, result.TzOffset);

            }
            catch (Exception ex)
            {
                Assert.Fail(ex.Message);
            }
        }

        [TestMethod]
        [DeploymentItem("Insights\\TestData\\GetApplicationInfoTestData.xml")]
        [DataSource("Microsoft.VisualStudio.TestTools.DataSource.XML", "|DataDirectory|\\GetApplicationInfoTestData.xml", "ApplicationInfo", DataAccessMethod.Sequential)]
        public void GetApplicationInfoTest()
        {
            try
            {
                var applicationId = Convert.ToInt64(TestContext.DataRow["applicationId"].ToString());
                var expected = Convert.ToBoolean(TestContext.DataRow["result"].ToString());
                


                var insightRepository = new InsightsEfRepository(MockInsightsEntities.Object);

                var result = insightRepository.GetGreenButtonApplicationInformation(applicationId);

                if (expected)
                    Assert.IsNotNull(result);
                else
                    Assert.IsNull(result);

            }
            catch (Exception ex)
            {
                Assert.Fail(ex.Message);
            }
        }

        [TestMethod]
        [DeploymentItem("Insights\\TestData\\GetAuthorizationTestData.xml")]
        [DataSource("Microsoft.VisualStudio.TestTools.DataSource.XML", "|DataDirectory|\\GetAuthorizationTestData.xml", "AuthorizationByApplicationId", DataAccessMethod.Sequential)]
        public void GetAuthorizationByApplicationIdTest()
        {
            try
            {
                var clientId = TestContext.DataRow["client"].ToString();
                var clientSecretCode = TestContext.DataRow["clientSecretCode"].ToString();
               
                var expected = Convert.ToInt32(TestContext.DataRow["Result"].ToString());



                var insightRepository = new InsightsEfRepository(MockInsightsEntities.Object);

                var result = insightRepository.GetGreenButtonAuthorizations(clientId,clientSecretCode);

                Assert.IsNotNull(result);
                Assert.AreEqual(expected, result.Count);

            }
            catch (Exception ex)
            {
                Assert.Fail(ex.Message);
            }
        }

    }
    
}