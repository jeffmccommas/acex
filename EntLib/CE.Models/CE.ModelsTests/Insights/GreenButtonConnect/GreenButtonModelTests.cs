﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using CE.Models.Insights;
using System;
using System.Collections.Generic;
using System.Linq;
using CE.GreenButtonConnect;
using CE.Models.Insights.Types;
using CE.ModelsTests.Insights.Helpers;
using Moq;
using Helper = CE.ModelsTests.Insights.Helpers.Helper;

// ReSharper disable once CheckNamespace
namespace CE.Models.Tests
{
    [TestClass()]
    [DeploymentItem("EntityFramework.SqlServer.dll")]
    public class GreenButtonModelTests : InsightsEntitiesMock
    {
        private string _greenButtonDomain = "http://www.greenbutton.com";
        private Mock<IGreenButtonConnect> _greenButtonConnect;
        public TestContext TestContext { get; set; }

        [TestInitialize]
        public void TestInit()
        {
            _greenButtonConnect = new Mock<IGreenButtonConnect>();

        }

        [TestMethod]
        [DeploymentItem("Insights\\TestData\\SubscriptionMappingTestData.xml")]
        [DataSource("Microsoft.VisualStudio.TestTools.DataSource.XML",
            "|DataDirectory|\\SubscriptionMappingTestData.xml", "Subscription", DataAccessMethod.Sequential)]
        public void GreenButtonSubscriptionMappingTest()
        {
            try
            {

                var clientId = Convert.ToInt32(TestContext.DataRow["clientId"].ToString());
                var usagePointCount = Convert.ToInt32(TestContext.DataRow["UsagePointCount"].ToString());
                var readingTypeCount = Convert.ToInt32(TestContext.DataRow["ReadingTypeCount"].ToString());
                var intervalId = Convert.ToInt32(TestContext.DataRow["IntervalId"].ToString());
                var commodityId = Convert.ToInt32(TestContext.DataRow["CommodityId"].ToString());
                var uomKey = Convert.ToString(TestContext.DataRow["UomKey"].ToString());
                var convertGas = Convert.ToBoolean(TestContext.DataRow["ConvertGas"].ToString());
                var conversionFactor = Convert.ToDouble(TestContext.DataRow["conversionFactor"].ToString());
                var hasConsumption = Convert.ToBoolean(TestContext.DataRow["HasConsumption"].ToString());
                var blockDuration = TestContext.DataRow["blockDuration"].ToString();
                GreenButtonDurationType blockDurationType;
                Enum.TryParse(blockDuration, out blockDurationType);
                var start = TestContext.DataRow["StartDate"].ToString();
                var end = TestContext.DataRow["EndDate"].ToString();
                var consumptionVal = Convert.ToDecimal(TestContext.DataRow["Consumption"].ToString());
                var expectedConsumptionVal = Convert.ToDecimal(TestContext.DataRow["ExpectedConsumption"].ToString());
                var expectedUsagePointCount = Convert.ToInt32(TestContext.DataRow["ExpectedUsagePointCount"].ToString());
                var expectedMeterReadingCount = Convert.ToInt32(TestContext.DataRow["ExpectedMeterReadingCount"].ToString());
                var expectedReadingTypeCount = Convert.ToInt32(TestContext.DataRow["ExpectedReadinTypeCount"].ToString());
                var expectedIntervalBlockCount = Convert.ToInt32(TestContext.DataRow["ExpectedIntervalBlockCount"].ToString());
                DateTime startDate = !string.IsNullOrEmpty(start) ? Convert.ToDateTime(start) : DateTime.UtcNow.Date.AddDays(-1);
                DateTime? endDate = !string.IsNullOrEmpty(end) ? Convert.ToDateTime(end) : (DateTime?)null;
                var config = new GreenButtonConfig
                {
                    ConvertGasUom = convertGas,
                    DefaultGasConversionFactor = conversionFactor
                };
                var insightRepository = new InsightsEfRepository(MockInsightsEntities.Object);
                var subscriptionMock = Helper.MockSubscription(usagePointCount, readingTypeCount, intervalId,
                    commodityId, uomKey, convertGas, hasConsumption, startDate, endDate, blockDurationType, consumptionVal);
                IGreenButtonModel model = new GreenButtonModel(clientId, _greenButtonDomain,config, insightRepository, _greenButtonConnect.Object);
                
                var result = model.GreenButtonSubscriptionMapping(subscriptionMock);


                Assert.IsNotNull(result);
                Assert.AreEqual(expectedUsagePointCount, result.UsagePoints.Count);
                Assert.AreEqual(expectedMeterReadingCount, result.UsagePoints.SelectMany(u => u.MeterReading).Count());
                Assert.AreEqual(expectedReadingTypeCount, result.UsagePoints.SelectMany(u => u.MeterReading).Select(m => m.ReadingType).Count());
                var intervalBlocks =
                    result.UsagePoints.SelectMany(u => u.MeterReading)
                        .SelectMany(m => m.IntervalBlock ?? new List<IntervalBlockEntry>())
                        .ToList();
                Assert.AreEqual(expectedIntervalBlockCount, intervalBlocks.Count());
                if (expectedIntervalBlockCount > 0)
                    Assert.AreEqual(expectedConsumptionVal, Convert.ToDecimal(intervalBlocks[0].IntervalBlock.IntervalReading[0].value));

            }
            catch (Exception ex)
            {
                Assert.Fail(ex.Message);
            }
        }


        [TestMethod]
        [DeploymentItem("Insights\\TestData\\GenerateGreenButtonTestData.xml")]
        [DataSource("Microsoft.VisualStudio.TestTools.DataSource.XML",
            "|DataDirectory|\\GenerateGreenButtonTestData.xml", "GreenButton", DataAccessMethod.Sequential)]
        public void GenerateGreenButtonTest()
        {
            try
            {

                var clientId = Convert.ToInt32(TestContext.DataRow["clientId"].ToString());
                var usagePointCount = Convert.ToInt32(TestContext.DataRow["UsagePointCount"].ToString());
                var readingTypeCount = Convert.ToInt32(TestContext.DataRow["ReadingTypeCount"].ToString());
                var intervalId = Convert.ToInt32(TestContext.DataRow["IntervalId"].ToString());
                var commodityId = Convert.ToInt32(TestContext.DataRow["CommodityId"].ToString());
                var uomKey = Convert.ToString(TestContext.DataRow["UomKey"].ToString());
                var convertGas = Convert.ToBoolean(TestContext.DataRow["ConvertGas"].ToString());
                var conversionFactor = Convert.ToDouble(TestContext.DataRow["conversionFactor"].ToString());
                var hasConsumption = Convert.ToBoolean(TestContext.DataRow["HasConsumption"].ToString());
                var blockDuration = TestContext.DataRow["blockDuration"].ToString();
                GreenButtonDurationType blockDurationType;
                Enum.TryParse(blockDuration, out blockDurationType);
                var start = TestContext.DataRow["StartDate"].ToString();
                var end = TestContext.DataRow["EndDate"].ToString();
                DateTime startDate = !string.IsNullOrEmpty(start) ? Convert.ToDateTime(start) : DateTime.UtcNow.Date.AddDays(-1);
                DateTime? endDate = !string.IsNullOrEmpty(end) ? Convert.ToDateTime(end) : (DateTime?)null;
                var hasResult = Convert.ToBoolean(TestContext.DataRow["hasResult"].ToString());
                GreenButtonEntryType greenButtonType;
                Enum.TryParse(TestContext.DataRow["GreenButtonType"].ToString(), out greenButtonType);
                GreenButtonType type;
                Enum.TryParse(TestContext.DataRow["GreenButtonType"].ToString(), out type);

                var config = new GreenButtonConfig
                {
                    ConvertGasUom = convertGas,
                    DefaultGasConversionFactor = conversionFactor
                };
                var insightRepository = new InsightsEfRepository(MockInsightsEntities.Object);
                var subscriptionMock = Helper.MockSubscription(usagePointCount, readingTypeCount, intervalId,
                    commodityId, uomKey, convertGas, hasConsumption, startDate, endDate, blockDurationType, 1m);

                if (hasResult)
                {
                    _greenButtonConnect.Setup(
                        gb => gb.GenerateGreenButtonConnect(It.IsAny<Subscription>(), type))
                        .Returns(new GreenButtonConnectResult {GreenButtonXml = "result"})
                        .Verifiable();
                }
                else
                    _greenButtonConnect.Setup(
                        gb => gb.GenerateGreenButtonConnect(It.IsAny<Subscription>(), type))
                        .Returns(new GreenButtonConnectResult { GreenButtonXml = String.Empty })
                        .Verifiable();

                IGreenButtonModel model = new GreenButtonModel(clientId, _greenButtonDomain,config, insightRepository, _greenButtonConnect.Object);

                var result = model.GenerateGreenButton(subscriptionMock, greenButtonType);


                Assert.IsNotNull(result);
                _greenButtonConnect.Verify(gb => gb.GenerateGreenButtonConnect(It.IsAny<Subscription>(), type), Times.Once);
                if (hasResult)
                    Assert.AreNotEqual(string.Empty, result.GreenButtonXml);
                else
                    Assert.AreEqual(string.Empty, result.GreenButtonXml);

            }
            catch (Exception ex)
            {
                Assert.Fail(ex.Message);
            }
        }

        [TestMethod]
        [DeploymentItem("Insights\\TestData\\GenerateServiceStatusTestData.xml")]
        [DataSource("Microsoft.VisualStudio.TestTools.DataSource.XML",
    "|DataDirectory|\\GenerateServiceStatusTestData.xml", "ServiceStatus", DataAccessMethod.Sequential)]
        public void GenerateServiceStatusTest()
        {
            try
            {

                var clientId = Convert.ToInt32(TestContext.DataRow["clientId"].ToString());
                var isRegistrationAccessToken = Convert.ToBoolean(TestContext.DataRow["isRegistrationAccessToken"].ToString());

                var config = new GreenButtonConfig
                {
                    ConvertGasUom = false,
                    DefaultGasConversionFactor = 1
                };
                var insightRepository = new InsightsEfRepository(MockInsightsEntities.Object);

                _greenButtonConnect.Setup(
                       gb => gb.GenerateGreenButtonServiceStatus(It.IsAny<bool>()))
                       .Returns(new GreenButtonConnectResult { GreenButtonXml = "result" })
                       .Verifiable();

                IGreenButtonModel model = new GreenButtonModel(clientId, _greenButtonDomain, config, insightRepository, _greenButtonConnect.Object);

                var result = model.GenerateServiceStatus(isRegistrationAccessToken);


                Assert.IsNotNull(result);
                _greenButtonConnect.Verify(gb => gb.GenerateGreenButtonServiceStatus(It.IsAny<bool>()), Times.Once);

                Assert.AreNotEqual(string.Empty, result.GreenButtonXml);

            }
            catch (Exception ex)
            {
                Assert.Fail(ex.Message);
            }
        }

        [TestMethod]
        public void GenerateApplicationInfoTest()
        {
            try
            {

                var clientId = 87;

                var config = new GreenButtonConfig
                {
                    ConvertGasUom = false,
                    DefaultGasConversionFactor = 1
                };
                var insightRepository = new InsightsEfRepository(MockInsightsEntities.Object);

                var applicationInfoMock = Helper.MockApplicationInfo(clientId.ToString(), DateTime.UtcNow.Date);

                _greenButtonConnect.Setup(
                       gb => gb.GenerateGreenButtonConnect(It.IsAny<Subscription>(), GreenButtonType.ApplicationInfo))
                       .Returns(new GreenButtonConnectResult { GreenButtonXml = "result" })
                       .Verifiable();

                IGreenButtonModel model = new GreenButtonModel(clientId, _greenButtonDomain, config, insightRepository, _greenButtonConnect.Object);

                var result = model.GenerateApplicationInfo(applicationInfoMock);


                Assert.IsNotNull(result);
                Assert.AreNotEqual(string.Empty, result.GreenButtonXml);

                _greenButtonConnect.Setup(
                      gb => gb.GenerateGreenButtonConnect(It.IsAny<Subscription>(), GreenButtonType.ApplicationInfo))
                      .Returns(new GreenButtonConnectResult { GreenButtonXml = string.Empty })
                      .Verifiable();
                
                result = model.GenerateApplicationInfo(applicationInfoMock);


                Assert.IsNotNull(result);
                Assert.AreEqual(string.Empty, result.GreenButtonXml);


                _greenButtonConnect.Verify(gb => gb.GenerateGreenButtonConnect(It.IsAny<Subscription>(), GreenButtonType.ApplicationInfo), Times.AtMost(2));


            }
            catch (Exception ex)
            {
                Assert.Fail(ex.Message);
            }
        }

        [TestMethod]
        [DeploymentItem("Insights\\TestData\\GetAuthorizationTestData.xml")]
        [DataSource("Microsoft.VisualStudio.TestTools.DataSource.XML", "|DataDirectory|\\GetAuthorizationTestData.xml", "AuthorizationByApplicationId", DataAccessMethod.Sequential)]
        public void GenerateAuthorizationTest()
        {
            try
            {

                var clientId = 87;

                var config = new GreenButtonConfig
                {
                    ConvertGasUom = false,
                    DefaultGasConversionFactor = 1
                };



                var client = TestContext.DataRow["client"].ToString();
                var clientSecretCode = TestContext.DataRow["clientSecretCode"].ToString();
                var expected = Convert.ToInt32(TestContext.DataRow["Result"].ToString());



                var insightRepository = new InsightsEfRepository(MockInsightsEntities.Object);

                var authorizations = insightRepository.GetGreenButtonAuthorizations(client, clientSecretCode);


                if (expected > 0)
                   _greenButtonConnect.Setup(
                      gb => gb.GenerateGreenButtonConnect(It.IsAny<Subscription>(), GreenButtonType.Authorzation))
                      .Returns(new GreenButtonConnectResult { GreenButtonXml = "result" })
                      .Verifiable();

                IGreenButtonModel model = new GreenButtonModel(clientId, _greenButtonDomain, config, insightRepository, _greenButtonConnect.Object);

                var result = model.GenerateAuthorization(authorizations);


                Assert.IsNotNull(result);
                if (expected == 0)
                    Assert.IsNull(result.GreenButtonXml);
                else
                    Assert.AreNotEqual(string.Empty, result.GreenButtonXml);


                _greenButtonConnect.Verify();


            }
            catch (Exception ex)
            {
                Assert.Fail(ex.Message);
            }
        }


    }
}