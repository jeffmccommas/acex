﻿using System;
using System.Data.Entity;
using System.Linq;
using CE.Models.Insights.EF;

namespace CE.ModelsTests.Insights.Helpers
{
    public class MockInsightMetadataEntities : InsightsMetadataEntities
    {
        public MockInsightMetadataEntities()
        {
            EMDailyWeatherDetails = MockDailyWeatherDetails();
            EMZipcodes = MockZipCodes();
        }

        public sealed override DbSet<EMDailyWeatherDetail> EMDailyWeatherDetails { get; set; }
        public override DbSet<EMDegreeDay> EMDegreeDays { get; set; }
        public override DbSet<EMEvaporation> EMEvaporations { get; set; }
        public override DbSet<EMSolar> EMSolars { get; set; }
        public override DbSet<EMTemperatureProfile> EMTemperatureProfiles { get; set; }
        public override DbSet<EMTemperatureRegion> EMTemperatureRegions { get; set; }
        public sealed override DbSet<EMZipcode> EMZipcodes { get; set; }
        public override DbSet<EMApplianceRegion> EMApplianceRegions { get; set; }

        private MockEmDailyWeatherDetailDbSet MockDailyWeatherDetails()
        {
            var emDailyWeatherDetails = new MockEmDailyWeatherDetailDbSet();

            emDailyWeatherDetails.Add(new EMDailyWeatherDetail { StationID = "14702", WeatherReadingDate = Convert.ToDateTime("6/1/2016"), AvgWetBulb = 67, AvgTemp = 74, CoolingDegreeDays = 9, HeatingDegreeDays = 0,MaxTemp = 85,MinTemp = 62});
            emDailyWeatherDetails.Add(new EMDailyWeatherDetail { StationID = "14702", WeatherReadingDate = Convert.ToDateTime("6/2/2016"), AvgWetBulb = 54, AvgTemp = 63, CoolingDegreeDays = 0, HeatingDegreeDays = 2, MaxTemp = 72, MinTemp = 54 });
            emDailyWeatherDetails.Add(new EMDailyWeatherDetail { StationID = "14702", WeatherReadingDate = Convert.ToDateTime("6/3/2016"), AvgWetBulb = 49, AvgTemp = 59, CoolingDegreeDays = 0, HeatingDegreeDays = 6, MaxTemp = 70, MinTemp = 47 });
            emDailyWeatherDetails.Add(new EMDailyWeatherDetail { StationID = "14702", WeatherReadingDate = Convert.ToDateTime("6/4/2016"), AvgWetBulb = 49, AvgTemp = 56, CoolingDegreeDays = 0, HeatingDegreeDays = 9, MaxTemp = 69, MinTemp = 42 });
            emDailyWeatherDetails.Add(new EMDailyWeatherDetail { StationID = "14702", WeatherReadingDate = Convert.ToDateTime("6/5/2016"), AvgWetBulb = 52, AvgTemp = 56, CoolingDegreeDays = 0, HeatingDegreeDays = 9, MaxTemp = 72, MinTemp = 39 });
            emDailyWeatherDetails.Add(new EMDailyWeatherDetail { StationID = "14702", WeatherReadingDate = Convert.ToDateTime("6/6/2016"), AvgWetBulb = 58, AvgTemp = 63, CoolingDegreeDays = 0, HeatingDegreeDays = 2, MaxTemp = 78, MinTemp = 47 });
            emDailyWeatherDetails.Add(new EMDailyWeatherDetail { StationID = "14702", WeatherReadingDate = Convert.ToDateTime("6/7/2016"), AvgWetBulb = 61, AvgTemp = 70, CoolingDegreeDays = 5, HeatingDegreeDays = 0, MaxTemp = 88, MinTemp = 51 });
            emDailyWeatherDetails.Add(new EMDailyWeatherDetail { StationID = "14702", WeatherReadingDate = Convert.ToDateTime("6/8/2016"), AvgWetBulb = 68, AvgTemp = 74, CoolingDegreeDays = 9, HeatingDegreeDays = 0, MaxTemp = 90, MinTemp = 58 });
            emDailyWeatherDetails.Add(new EMDailyWeatherDetail { StationID = "14702", WeatherReadingDate = Convert.ToDateTime("6/9/2016"), AvgWetBulb = 69, AvgTemp = 77, CoolingDegreeDays = 12, HeatingDegreeDays = 0, MaxTemp = 88, MinTemp = 66 });
            emDailyWeatherDetails.Add(new EMDailyWeatherDetail { StationID = "14702", WeatherReadingDate = Convert.ToDateTime("6/10/2016"), AvgWetBulb = 64, AvgTemp = 69, CoolingDegreeDays = 4, HeatingDegreeDays = 0, MaxTemp = 80, MinTemp = 58 });
            emDailyWeatherDetails.Add(new EMDailyWeatherDetail { StationID = "14702", WeatherReadingDate = Convert.ToDateTime("6/11/2016"), AvgWetBulb = 55, AvgTemp = 58, CoolingDegreeDays = 0, HeatingDegreeDays = 7, MaxTemp = 63, MinTemp = 53, Comment = ",avgwetbulb derived" });
            emDailyWeatherDetails.Add(new EMDailyWeatherDetail { StationID = "14702", WeatherReadingDate = Convert.ToDateTime("6/12/2016"), AvgWetBulb = 53, AvgTemp = 55, CoolingDegreeDays = 0, HeatingDegreeDays = 10, MaxTemp = 58, MinTemp = 51, Comment = ",avgwetbulb derived" });
            emDailyWeatherDetails.Add(new EMDailyWeatherDetail { StationID = "14702", WeatherReadingDate = Convert.ToDateTime("6/13/2016"), AvgWetBulb = 57, AvgTemp = 61, CoolingDegreeDays = 0, HeatingDegreeDays = 4, MaxTemp = 69, MinTemp = 53, Comment = ",avgwetbulb derived" });
            emDailyWeatherDetails.Add(new EMDailyWeatherDetail { StationID = "14702", WeatherReadingDate = Convert.ToDateTime("6/14/2016"), AvgWetBulb = 54, AvgTemp = 57, CoolingDegreeDays = 0, HeatingDegreeDays = 8, MaxTemp = 61, MinTemp = 52, Comment = ",avgwetbulb derived" });
            emDailyWeatherDetails.Add(new EMDailyWeatherDetail { StationID = "14702", WeatherReadingDate = Convert.ToDateTime("6/15/2016"), AvgWetBulb = 55, AvgTemp = 64, CoolingDegreeDays = 0, HeatingDegreeDays = 1, MaxTemp = 77, MinTemp = 50 });
            emDailyWeatherDetails.Add(new EMDailyWeatherDetail { StationID = "14702", WeatherReadingDate = Convert.ToDateTime("6/16/2016"), AvgWetBulb = 62, AvgTemp = 71, CoolingDegreeDays = 6, HeatingDegreeDays = 0, MaxTemp = 87, MinTemp = 54 });
            emDailyWeatherDetails.Add(new EMDailyWeatherDetail { StationID = "14702", WeatherReadingDate = Convert.ToDateTime("6/17/2016"), AvgWetBulb = 64, AvgTemp = 68, CoolingDegreeDays = 3, HeatingDegreeDays = 0, MaxTemp = 75, MinTemp = 61 });
            emDailyWeatherDetails.Add(new EMDailyWeatherDetail { StationID = "14702", WeatherReadingDate = Convert.ToDateTime("6/18/2016"), AvgWetBulb = 65, AvgTemp = 71, CoolingDegreeDays = 6, HeatingDegreeDays = 0, MaxTemp = 82, MinTemp = 60 });
            emDailyWeatherDetails.Add(new EMDailyWeatherDetail { StationID = "14702", WeatherReadingDate = Convert.ToDateTime("6/19/2016"), AvgWetBulb = 57, AvgTemp = 65, CoolingDegreeDays = 0, HeatingDegreeDays = 0, MaxTemp = 78, MinTemp = 51 });
            emDailyWeatherDetails.Add(new EMDailyWeatherDetail { StationID = "14702", WeatherReadingDate = Convert.ToDateTime("6/20/2016"), AvgWetBulb = 58, AvgTemp = 64, CoolingDegreeDays = 0, HeatingDegreeDays = 1, MaxTemp = 81, MinTemp = 47 });
            emDailyWeatherDetails.Add(new EMDailyWeatherDetail { StationID = "14702", WeatherReadingDate = Convert.ToDateTime("6/21/2016"), AvgWetBulb = 61, AvgTemp = 68, CoolingDegreeDays = 3, HeatingDegreeDays = 0, MaxTemp = 84, MinTemp = 52 });
            emDailyWeatherDetails.Add(new EMDailyWeatherDetail { StationID = "14702", WeatherReadingDate = Convert.ToDateTime("6/22/2016"), AvgWetBulb = 65, AvgTemp = 67, CoolingDegreeDays = 2, HeatingDegreeDays = 0, MaxTemp = 71, MinTemp = 63 });
            emDailyWeatherDetails.Add(new EMDailyWeatherDetail { StationID = "14702", WeatherReadingDate = Convert.ToDateTime("6/23/2016"), AvgWetBulb = 61, AvgTemp = 63, CoolingDegreeDays = 0, HeatingDegreeDays = 2, MaxTemp = 65, MinTemp = 60 });
            emDailyWeatherDetails.Add(new EMDailyWeatherDetail { StationID = "14702", WeatherReadingDate = Convert.ToDateTime("6/24/2016"), AvgWetBulb = 58, AvgTemp = 58, CoolingDegreeDays = 0, HeatingDegreeDays = 7, MaxTemp = 62, MinTemp = 54 });
            emDailyWeatherDetails.Add(new EMDailyWeatherDetail { StationID = "14702", WeatherReadingDate = Convert.ToDateTime("6/25/2016"), AvgWetBulb = 58, AvgTemp = 62, CoolingDegreeDays = 0, HeatingDegreeDays = 3, MaxTemp = 70, MinTemp = 53 });
            emDailyWeatherDetails.Add(new EMDailyWeatherDetail { StationID = "14702", WeatherReadingDate = Convert.ToDateTime("6/26/2016"), AvgWetBulb = 65, AvgTemp = 70, CoolingDegreeDays = 5, HeatingDegreeDays = 0, MaxTemp = 80, MinTemp = 60 });
            emDailyWeatherDetails.Add(new EMDailyWeatherDetail { StationID = "14702", WeatherReadingDate = Convert.ToDateTime("6/27/2016"), AvgWetBulb = 66, AvgTemp = 71, CoolingDegreeDays = 6, HeatingDegreeDays = 0, MaxTemp = 83, MinTemp = 58 });
            emDailyWeatherDetails.Add(new EMDailyWeatherDetail { StationID = "14702", WeatherReadingDate = Convert.ToDateTime("6/28/2016"), AvgWetBulb = 67, AvgTemp = 73, CoolingDegreeDays = 8, HeatingDegreeDays = 0, MaxTemp = 84, MinTemp = 62 });
            emDailyWeatherDetails.Add(new EMDailyWeatherDetail { StationID = "14702", WeatherReadingDate = Convert.ToDateTime("6/29/2016"), AvgWetBulb = 66, AvgTemp = 74, CoolingDegreeDays = 9, HeatingDegreeDays = 0, MaxTemp = 83, MinTemp = 64 });
            emDailyWeatherDetails.Add(new EMDailyWeatherDetail { StationID = "14702", WeatherReadingDate = Convert.ToDateTime("6/30/2016"), AvgWetBulb = 61, AvgTemp = 69, CoolingDegreeDays = 4, HeatingDegreeDays = 0, MaxTemp = 81, MinTemp = 56 });
            return emDailyWeatherDetails;
        }

        private MockEmZipcodeDbSet MockZipCodes()
        {
            var zipCodes = new MockEmZipcodeDbSet();
            zipCodes.Add(new EMZipcode
            {
                ZipCode = "02481",
                TemperatureRegionId = 134,
                ApplianceRegionId = 2,
                SolarRegionId = 2,
                State = "MA",
                City = "WELLESLEY HILLS",
                CountryCode = "US",
                ClientID = 0,
                StationIdDaily = "14702"
            });
            zipCodes.Add(new EMZipcode
            {
                ZipCode = "02482",
                TemperatureRegionId = 134,
                ApplianceRegionId = 2,
                SolarRegionId = 2,
                State = "MA",
                City = "WELLESLEY",
                CountryCode = "US",
                ClientID = 0,
                StationIdDaily = "14702"
            });
            zipCodes.Add(new EMZipcode
            {
                ZipCode = "02483",
                TemperatureRegionId = 134,
                ApplianceRegionId = 2,
                SolarRegionId = 2,
                State = "MA",
                City = "WESTON",
                CountryCode = "US",
                ClientID = 0,
                StationIdDaily = "14702"
            });

            zipCodes.Add(new EMZipcode
            {
                ZipCode = "02484",
                TemperatureRegionId = 134,
                ApplianceRegionId = 2,
                SolarRegionId = 2,
                State = "MA",
                City = "Test",
                CountryCode = "US",
                ClientID = 2,
                StationIdDaily = "14702"
            });

            return zipCodes;
        }
    }

    class MockEmDailyWeatherDetailDbSet : MockDbSet<EMDailyWeatherDetail>
    {
        public override EMDailyWeatherDetail Find(params object[] keyValues)
        {
            return this.SingleOrDefault(w => w.StationID == (string)keyValues.Single());
        }

    }

    class MockEmZipcodeDbSet : MockDbSet<EMZipcode>
    {
        public override EMZipcode Find(params object[] keyValues)
        {
            return this.SingleOrDefault(z => z.ZipCode == (string)keyValues.Single());
        }

    }
}
