﻿using System;
using System.Collections.Generic;
using CE.Models.Insights;
using CE.Models.Insights.Types;

namespace CE.ModelsTests.Insights.Helpers
{
    public static class Helper
    {

        private const int UomidCubicFeet = 119;
        private const int UomidTherm = 169;
        private const int UomidWattHours = 72;
        private const int UomidCubicMeter = 42;
        private const int UomidGal = 128;

        public static GreenButtonSubscription MockSubscription(int numOfUsagePoints, int numOfReadingTypes, int intervalId,
     int commodityId, string uomKey, bool? convertGas, bool? createConsumption, DateTime startDate,
     DateTime? endDate, GreenButtonDurationType blockDuration, decimal consumptionValue) 
        {
            var usagePoints = MockUsagePoints(numOfUsagePoints, numOfReadingTypes, intervalId, commodityId, uomKey,
                convertGas, createConsumption, startDate, endDate, blockDuration, consumptionValue);
            var links = MockLinks();
            var subsription = new GreenButtonSubscription
            {
                Id = 1,
                UuId = "SubscriptionUuid1",
                Links = links,
                PublishedDate = DateTime.UtcNow.Date,
                UpdatedDate = DateTime.UtcNow.Date,
                UsagePoints = usagePoints
            };
            return subsription;
        }

        public static GreenButtonApplicationInfo MockApplicationInfo(string clientId, DateTime startDate)
        {
            var links = MockLinks();
            var applicationInfo = new GreenButtonApplicationInfo
            {
                Id = 1,
                UuId = "applicationguuid1",
                Links = links,
                UpdatedDate = startDate,
                ClientId = clientId,
                DataCustodianApplicationStatus = 1
            };
            return applicationInfo;

        }

        public static List<GreenButtonUsagePoint> MockUsagePoints(int numOfUsagePoints, int numOfReadingTypes, int intervalId, int commodityId, string uomKey, bool? convertGas, bool? createConsumption, DateTime startDate, DateTime? endDate, GreenButtonDurationType blockDuration, decimal consumptionValue) 
        {
            var usagePoints = new List<GreenButtonUsagePoint>();

            for (var i = 1; i <= numOfUsagePoints; i++) 
            {
                var usagePoint = MockUsagePoint(i, numOfReadingTypes, intervalId, commodityId, uomKey, convertGas,
                    createConsumption, startDate, endDate, blockDuration, consumptionValue);
                usagePoints.Add(usagePoint);
            }

            return usagePoints;
        }

        public static GreenButtonUsagePoint MockUsagePoint(int id, int numOfReadingTypes, int intervalId, int commodityId, string uomKey, bool? convertGas, bool? createConsumption, DateTime startDate, DateTime? endDate, GreenButtonDurationType blockDuration, decimal consumptionValue)
        {
            var meterId = $"meter{id}";
            var links = MockLinks();

            var readingTypes = new List<GreenButtonAmiReadingType>();
            for (int i = 0; i < numOfReadingTypes; i++)
            {
                var readingTypeId = Convert.ToInt32($"{id}{i}");
                int generateInterval = intervalId;
                if (numOfReadingTypes > 1)
                {
                    if (i == 1)
                    {
                        generateInterval = intervalId == 30 ? 60 : 30;
                    }
                    else if (i == 2)
                    {
                        generateInterval = intervalId == 15 ? 60 : 15;
                    }
                    else if (i == 3)
                    {
                        generateInterval = intervalId == 5 ? 60 : 5;
                    }
                    else if (i == 4)
                    {
                        generateInterval = intervalId == 24 ? 60 : 24;
                    }
                    else
                    {
                        generateInterval = intervalId;
                    }
                }
                var readingType = MockReadingType(id, readingTypeId, meterId, commodityId, generateInterval, uomKey, convertGas,
                    createConsumption, startDate, endDate, blockDuration, consumptionValue);

                readingTypes.Add(readingType);
            }
            var usagePoint = new GreenButtonUsagePoint
            {
                Id = id,
                UuId = $"UsagePointUuid{id}",
                PublishedDate = DateTime.UtcNow.Date,
                UpdatedDate = DateTime.UtcNow.Date,
                Links = links,
                ReadingTypes = readingTypes
            };

            return usagePoint;

        }

        private static List<GreenButtonLink> MockLinks()
        {
            var links = new List<GreenButtonLink>
            {
                new GreenButtonLink {Href = "/selflink", Rel = "self"},
                new GreenButtonLink {Href = "/uplink", Rel = "up"},
                new GreenButtonLink {Href = "/otherlink", Rel = "other"}
            };

            return links;
        }
        
        public static GreenButtonAmiReadingType MockReadingType(int usagePointId, int readingTypeId, string meterid, int commondityId,
            int intervalId, string uomKey, bool? convertGas, bool? createConsumption, DateTime startDate,
            DateTime? endDate, GreenButtonDurationType blockDuration, decimal consumptionValue) 
        {
            var consumption = createConsumption ?? false;
            var readingType = MockReadingType(readingTypeId, meterid, commondityId, intervalId, uomKey, convertGas,
                startDate, blockDuration);

            if (consumption) 
            {
                var end = endDate ?? DateTime.UtcNow.AddDays(-1).Date;
                readingType.ConsumptionList = MockIntervalConsumptionData(intervalId, startDate, end, consumptionValue);
            }

            return readingType;
        }

        private static GreenButtonAmiReadingType MockReadingType(int readingTypeId, string meterid, int commondityId, int intervalId, string uomKey, bool? convertGas, DateTime startDate, GreenButtonDurationType blockDuration)
        {
            int kind, commondity, powerOfTen = 0;
            var uomId = ConvertUomNameToUomId(uomKey);
            var convert = convertGas ?? false;
            if (commondityId == 2) // gas
            {
                if (convert)
                {
                    if (uomId != UomidCubicFeet)
                    {
                        powerOfTen = -3;
                    }
                }
                else
                {
                    if (uomId == UomidTherm)
                    {
                        powerOfTen = -3;
                    }
                }
                kind = 1;
                commondity = 1;
            }
            else if (commondityId == 3) // water
            {
                powerOfTen = 0;
                kind = 2;
                commondity = 10;
            }
            else // electric
            {
                powerOfTen = 0;
                kind = 0;
                commondity = 0;
            }
            var intervalLength = ConvertIntervalIdToIntervalDurationLength(intervalId);
            var timeAttribute = ConvertIntervalIdToTimeAttributeId(intervalId);

            var links = MockLinks();
            var readType = new GreenButtonAmiReadingType
            {
                 AccumulationBehavior= 4,
                Aggregate = 1,
                Commodity = commondity,
                DataQualifier = 0,
                FlowDirection = 1,
                Kind = kind,
                IntervalLength = intervalLength,
                TimeAttribute = timeAttribute,
                Uom = uomId,
                PowerOfTenMultiplier = powerOfTen,
                Links = links,
                Id = readingTypeId,
                Uuid = $"ReadingTypeUuid{readingTypeId}",
                PublishedDate = startDate,
                UpdatedDate = startDate,
                MeterId = meterid, 
                BlockDuratioin = blockDuration
            };


            return readType;
        }

        private static int ConvertUomNameToUomId(string uom)
        {

            int result;

            switch (uom.ToUpper())
            {
                case "CCF":
                    result = UomidCubicFeet;
                    break;
                case "THERMS":
                case "THERM":
                    result = UomidTherm;
                    break;
                case "KWH":
                    result = UomidWattHours;
                    break;
                case "CUBICMETER":
                case "CUBIC METER":
                    result = UomidCubicMeter;
                    break;
                case "GAL":
                case "KGAL":
                case "CGAL":
                    result = UomidGal;
                    break;
                default:
                    result = 0;
                    break;
            }

            return result;
        }

        private static int ConvertIntervalIdToIntervalDurationLength(int intervalId)
        {
            int duration = 0;
            switch (intervalId)
            {
                case 5:
                case 15:
                case 30:
                case 60:
                case 360:
                    duration = intervalId * 60;
                    break;
                case 24:
                    duration = intervalId * 60 * 60;
                    break;
            }

            return duration;
        }

        private static int ConvertIntervalIdToTimeAttributeId(int intervalId)
        {
            int value;
            switch (intervalId)
            {
                case 24:
                    value = 11;
                    break;
                case 5:
                    value = 0;
                    break;
                case 15:
                    value = 2;
                    break;
                case 30:
                    value = 5;
                    break;
                case 60:
                    value = 7;
                    break;
                default:
                    value = 0;
                    break;
            }
            return value;
        }

        public static IntervalConsumptionData MockIntervalConsumptionData(int intervalId, DateTime startDate, DateTime endDate, decimal consumptionValue)
        {
            var hour = 1;
            if (intervalId == 15)
                hour = 4;
            else if (intervalId == 30)
                hour = 2;
            else if (intervalId == 5)
                hour = 12;

            var daysOfData = (endDate - startDate).TotalDays + 1;

            var consumptionDataList = new List<ConsumptionData>();
            for (int j = 0; j < daysOfData; j++)
            {
                var date = startDate.AddDays(j);
                if (intervalId == 24)
                {
                    consumptionDataList.Add(new ConsumptionData {DateTime = date, Value = 1m});
                }
                else
                {
                    for (var i = 0; i < 24; i++)
                    {
                        for (var x = 0; x < hour; x++)
                        {
                            var min = (intervalId * x) + (60 * i);
                            var timestamp = date.AddMinutes(min);
                            consumptionDataList.Add(new ConsumptionData { DateTime = timestamp, Value = consumptionValue });
                        }
                    }

                }
            }

            var intervalData = new IntervalConsumptionData
            {
                ConsumptionDataList = consumptionDataList,
                ResolutionKey = ((ResolutionType)intervalId).ToString()
            };

            return intervalData;

        }

    }
}
