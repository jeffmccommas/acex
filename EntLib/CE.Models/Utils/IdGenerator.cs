﻿//using CE.AO.Utilities;
using System;
using System.Security.Cryptography;
using System.Text;

namespace CE.Models.Utils
{
    public class IdGenerator
    {
        private readonly int _referrerId;
        private readonly string _accountId;
        private readonly string _meterId;
        private readonly long _tableId;

        private readonly MD5 _md5;

        public IdGenerator(int referrerId, string accountId, string meterId)
        {
            _referrerId = referrerId;
            _accountId = accountId;
            _meterId = meterId;

            _md5 = MD5.Create();
        }

        public IdGenerator(int referrerId, string accountId, long tableId)
        {
            _referrerId = referrerId;
            _accountId = accountId;
            _tableId = tableId;

            _md5 = MD5.Create();
        }

        public string ApplicationInfoId => GetMd5Hash(_md5, _referrerId + _accountId + _tableId + "ApplicationInfo");

        public string AuthorizationId => GetMd5Hash(_md5, _referrerId + _accountId + _tableId + "ApplicationInfo");

        public string SubscriptionId => GetMd5Hash(_md5, _referrerId + _accountId + _tableId + "Subscription");

        public string LinkId => GetMd5Hash(_md5, _referrerId + _accountId + _meterId);

        public string UsagePointId => "urn:uuid:" + GetMd5Hash(_md5, _referrerId + _accountId + _meterId + "UsagePoint");

        public string ReadingTypeId => "urn:uuid:" + GetMd5Hash(_md5, _referrerId + _accountId + _meterId + "ReadingType");

        public string MeterReadingId => "urn:uuid:" + GetMd5Hash(_md5, _referrerId + _accountId + _meterId + "MeterReading");

        public string GetIntervalBlockId(string resolution, DateTime date) => "urn:uuid:" + GetMd5Hash(_md5, _referrerId + _accountId + _meterId + "IntervalBlock" + resolution + date.ToShortDateString());

        private static string GetMd5Hash(MD5 md5Hash, string input)
        {

            // Convert the input string to a byte array and compute the hash.
            var data = md5Hash.ComputeHash(Encoding.UTF8.GetBytes(input));

            // Create a new Stringbuilder to collect the bytes
            // and create a string.
            var sBuilder = new StringBuilder();

            // Loop through each byte of the hashed data 
            // and format each one as a hexadecimal string.
            for (var i = 0; i < data.Length; i++)
            {
                sBuilder.Append(data[i].ToString("x2"));
            }

            // Return the hexadecimal string.
            return sBuilder.ToString();
        }

        //Added this for use with Green Button input.  The MD5 hash is generated during the create, so no need to pass it
        public string GetMd5Hash( string input)
        {

            // Convert the input string to a byte array and compute the hash.
            var data = _md5.ComputeHash(Encoding.UTF8.GetBytes(input));

            // Create a new Stringbuilder to collect the bytes
            // and create a string.
            var sBuilder = new StringBuilder();

            // Loop through each byte of the hashed data 
            // and format each one as a hexadecimal string.
            for (var i = 0; i < data.Length; i++)
            {
                sBuilder.Append(data[i].ToString("x2"));
            }

            // Return the hexadecimal string.
            return sBuilder.ToString();
        }

        ///CR 55405 
        public string LocalTimeParameterId => "urn:uuid:" + GetMd5Hash(_md5, _referrerId.ToString() + _accountId + _meterId + "LocalTimeParameterType");
    }
    
}
