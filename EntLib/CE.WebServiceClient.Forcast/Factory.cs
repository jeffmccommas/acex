﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CE.WebServiceClient.Forcast
{
    public static class Factory
    {
        public static IForcast CreateForcast(int clientId,int maxRetry = 3, int retryWaitDuration = 250)
        {
            return new ForcastClient(clientId) { MaxRetry = maxRetry, RetryWaitDuration = retryWaitDuration };
        }
    }
}
