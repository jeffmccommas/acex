﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CE.WebServiceClient.Forcast
{
    public class WeatherSenseConfig
    {
        public int DayTotal { get; set; }
        public int DayStart { get; set; }
        public int DayEnd { get; set; }
        public int ThresholdValue { get; set; }

        public int MaxRetry { get; set; }
        public int RetryWaitDuration { get; set; }

        public bool Enabled { get; set; }

        public string EmailFooter { get; set; }

        public IList<GeoLocation> Locations { get; set; }

        public ThresholdCompare ThresholdCompare { get; set; }
        public ThresholdAttribute ThresholdAttribute { get; set; }

        public WeatherSenseConfig()
        {
            this.Locations = new List<GeoLocation>();
        }

        public static ThresholdCompare ParseThresholdCompare(string value)
        {
            switch (value.ToLower())
            {
                case "lte":
                    return ThresholdCompare.LessThanEqual;
                    break;

                case "gte":
                    return ThresholdCompare.GreaterThanEqual;
                    break;

                case "e":
                    return ThresholdCompare.Equal;
                    break;

                default:
                    throw new Exception("Value for ThresholdCompare is not supported:" + value);
            }
        }

        public static ThresholdAttribute ParseThresholdAttribute(string value)
        {
            switch (value.ToLower())
            {
                case "dailylowtemp":
                    return ThresholdAttribute.DailyLowTemp;
                    break;

                case "dailyhightemp":
                    return ThresholdAttribute.DailyHighTemp;
                    break;

                default:
                    throw new Exception("Value for ThresholdAttribute is not supported:" + value);
            }
        }
    }

    public class GeoLocation
    {
        public string Location { get; set; }
        public float Latitude { get; set; }
        public float Longitude { get; set; }

        public GeoLocation(float latitude, float longitude)
        {
            this.Latitude = latitude;
            this.Longitude = longitude;
        }

        public GeoLocation(string latitude, string longitude)
            : this(float.Parse(latitude), float.Parse(longitude))
        {

        }

        public GeoLocation(string location, string latitude, string longitude)
            : this(latitude, longitude)
        {
            this.Location = location;
        }

        public static GeoLocation Parse(string value)
        {
            try
            {
                var values = value.Split(new char[] { ';' }, StringSplitOptions.RemoveEmptyEntries);

                var coordinates = values[values.Length - 1].Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);

                if (values.Length == 1)
                    return new GeoLocation(coordinates[0].Trim(), coordinates[1].Trim());
                else
                    return new GeoLocation(values[0], coordinates[0].Trim(), coordinates[1].Trim());
            }
            catch (Exception)
            {
                throw new Exception("Unable to Parse geo location:" + value);
            }
        }
    }

    public class DailyForcast
    {
        public DateTime Time { get; set; }
        public float TemperatureMin { get; set; }
        public float TemperatureMax { get; set; }

        public float GetTemperature(ThresholdAttribute attribute)
        {
            if (attribute == ThresholdAttribute.DailyHighTemp)
                return TemperatureMax;
            else
                return TemperatureMin;
        }
    }

    public class WeatherForcast
    {
        public GeoLocation Location { get; private set; }
        public IList<DailyForcast> DailyForcastResult { get; private set; }

        public WeatherForcast(GeoLocation location)
        {
            this.Location = location;
            this.DailyForcastResult = new List<DailyForcast>();
        }

        public void AddDailyForcast(long pythonTime, float minTemp, float maxTemp)
        {
            this.DailyForcastResult.Add(new DailyForcast()
            {
                Time = new DateTime(1970, 1, 1).Add(TimeSpan.FromSeconds(pythonTime)),
                TemperatureMax = maxTemp,
                TemperatureMin = minTemp
            });
        }
    }

    public enum ThresholdCompare { LessThanEqual, GreaterThanEqual, Equal };
    public enum ThresholdAttribute { DailyLowTemp, DailyHighTemp };
}
