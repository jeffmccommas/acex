﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CE.WebServiceClient.Forcast
{
    public interface IForcast
    {
        int MaxRetry { get; set; }
        int RetryWaitDuration { get; set; }

        WeatherForcast GetDailyForcast(string latitude, string longitude);
        WeatherForcast GetDailyForcast(GeoLocation location);
        IList<WeatherForcast> GetDailyForcast(IList<GeoLocation> locations);
    }
}
