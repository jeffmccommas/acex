﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CE.WebServiceClient.Forcast
{
    internal class ForcastClient:IForcast
    {
        private int _clientId;

        private const string _apiKey = "998555956ba077fec745916b595710cf";

        public int MaxRetry { get; set; }
        public int RetryWaitDuration { get; set; }

        public ForcastClient(int clientId)
        {
            _clientId = clientId;
        }

        public IList<WeatherForcast> GetDailyForcast(IList<GeoLocation> locations)
        {
            var result = new List<WeatherForcast>();

            foreach (var location in locations)
            {
                result.Add(GetDailyForcast(location));
            }

            return result;
        }

        public WeatherForcast GetDailyForcast(string latitude, string longitude)
        {
            return GetDailyForcast(new GeoLocation(latitude, longitude));
        }

        public WeatherForcast GetDailyForcast(GeoLocation location)
        {
            var retryCount = 1;

            do
            {
                try
                {
                    var request = new ForecastIO.ForecastIORequest(_apiKey, location.Latitude, location.Longitude, ForecastIO.Unit.us);

                    var response = request.Get();

                    return MapForcastResult(location, response.daily.data);
                }
                catch (Exception)
                {
                    if (retryCount >= MaxRetry)
                        throw;

                    Task.Delay(RetryWaitDuration);

                    retryCount += 1;
                }
            } while (true);

        }

        private WeatherForcast MapForcastResult(GeoLocation location, List<ForecastIO.DailyForecast> dailyForcast)
        {
            var result = new WeatherForcast(location);

            foreach (var forcast in dailyForcast)
            {
                result.AddDailyForcast(forcast.time, forcast.temperatureMin, forcast.temperatureMax);
            }

            return result;
        }
    }
}
