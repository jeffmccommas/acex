﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using AO.DataAccess.Contracts;
using AO.DTO;
using AO.Entities;
using CE.AO.Utilities;

namespace AO.DataAccess
{
    /// <summary>
    /// LogRepository class is used to retrieve logs from the database
    /// </summary>
    public class LogRepository : ILogRepository
    {
        private Lazy<ICassandraRepository> CassandraRepository { get; }

        public LogRepository(Lazy<ICassandraRepository> cassandraRepository)
        {
            CassandraRepository = cassandraRepository;
        }

        /// <summary>
        /// Insert LogViewModel to Log table
        /// </summary>
        /// <param name="logEntity">LogEntity to be inserted</param>
        /// <returns></returns>  
        public async Task<bool> InsertOrMergeLogAsync(LogEntity logEntity)
        {
            logEntity.MonthYear = Convert.ToInt32(logEntity.TimestampUtc.Month + "" + logEntity.TimestampUtc.Year);
            await CassandraRepository.Value.InsertOrUpdateAsync(logEntity, Constants.CassandraTableNames.LogTableName).ConfigureAwait(false);
            return true;
        }

        /// <summary>
        /// To fetch the list of logs from the database
        /// </summary>
        /// <param name="filterParams">Filter params for logs to be fetched</param>
        /// <returns>List of LogEntity</returns>
        public List<LogEntity> GetLogHistory(LogHistoryRequestDTO filterParams)
        {
            var lstLogEntities = new List<LogEntity>();

            if (string.IsNullOrEmpty(filterParams.ClientID))
            {
                return lstLogEntities;
            }
            // assigning Levels 
            var arrayLevel = filterParams.Levels.Length == 0 ? (from object level in Enum.GetValues(typeof(Enums.Severity)) select level.ToString().ToUpper(CultureInfo.InvariantCulture)).ToArray() : filterParams.Levels;
            // assigning MonthYear
            var monthYear = new[] { Convert.ToInt32(filterParams.StartDateTime.Month + "" + filterParams.StartDateTime.Year) };
            if (filterParams.StartDateTime.Month != filterParams.EndDateTime.Month)
                monthYear = new[] { Convert.ToInt32(filterParams.StartDateTime.Month + "" + filterParams.StartDateTime.Year), Convert.ToInt32(filterParams.EndDateTime.Month + "" + filterParams.EndDateTime.Year) };
            // assigning Modules
            var arrayModules = filterParams.Modules.Length == 0 ? (from object module in Enum.GetValues(typeof(Enums.Module)) select module.ToString()).ToArray() : filterParams.Modules;

            lstLogEntities.AddRange(CassandraRepository.Value.Get<LogEntity>(Constants.CassandraTableNames.Logging,
                    a => monthYear.Contains(a.MonthYear) && a.ClientId == filterParams.ClientID && arrayModules.Contains(a.Module) && a.TimestampUtc >= filterParams.StartDateTime && a.TimestampUtc < filterParams.EndDateTime
                    && arrayLevel.Contains(a.Level)
                ));
            return lstLogEntities;
        }

        /// <summary>
        /// Delete LogViewModel to Log table
        /// </summary>
        /// <param name="logEntity">LogEntity to be Deleted</param>
        /// <returns></returns>
        public async Task<bool> DeleteLogAsync(LogEntity logEntity)
        {
            await
                CassandraRepository.Value.DeleteAsync<LogEntity>(Constants.CassandraTableNames.LogTableName,
                    a =>
                        a.MonthYear == logEntity.MonthYear && a.ClientId == logEntity.ClientId &&
                        a.Module == logEntity.Module.ToString() &&
                        a.Level == logEntity.Level && a.TimestampUtc == logEntity.TimestampUtc).ConfigureAwait(false);

            return await Task.Factory.StartNew(() => true);
        }
    }
}