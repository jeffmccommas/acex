﻿using System;
using System.Collections.Generic;
using System.Data.Odbc;
using System.Diagnostics;
using AO.DataAccess.Contracts;

namespace AO.DataAccess
{
    /// <summary>
    /// OdbcRepository class use to fetch data from cassandra database using ODBC drivers.
    /// </summary>
    public class OdbcRepository : IOdbcRepository
    {
        /// <summary>
        /// Gets the rows and column list from database
        /// </summary>
        /// <param name="logDetails">out parameter with log details.</param>
        /// <param name="columnslList">out parameter for List of columns</param>
        /// <param name="filterQuery">generalized sql query</param>
        /// <returns></returns>
        public List<object[]> Get(out string logDetails,out List<string> columnslList, string filterQuery)
        {
            var rows = new List<object[]>();
            var columns = new List<string>();
            string logMsg   ;
            using (OdbcConnection dbConnection = new OdbcConnection(System.Configuration.ConfigurationManager.AppSettings.Get(StaticConfig.OdbcConnectionString)))
            {
                dbConnection.Open();
                using (OdbcCommand dbCommand = dbConnection.CreateCommand())
                {
                    dbCommand.CommandText = filterQuery;
                    Stopwatch sw = new Stopwatch();
                    sw.Start();
                    using (OdbcDataReader dbReader = dbCommand.ExecuteReader())
                    {
                        sw.Stop();
                        logMsg = " Time Elapsed to execute the query: " + sw.Elapsed +".";
                        var fCount = dbReader.FieldCount;
                        for (var i = 0; i < fCount; i++)
                        {
                            columns.Add(dbReader.GetName(i));
                        }
                        while (dbReader.Read())
                        {
                            var row = new object[fCount];
                            for (var i = 0; i < fCount; i++)
                            {
                                row[i] = Convert.ToString(dbReader.GetValue(i));
                            }
                            rows.Add(row);
                        }
                    }
                }
            }
            columnslList = columns;
            logDetails = logMsg;
            return rows;



        }
    }
}
