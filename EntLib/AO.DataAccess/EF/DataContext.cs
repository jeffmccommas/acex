using System;
using System.Collections;
using System.Configuration;
using System.Data.Entity;
using System.Data.Entity.Core.Objects;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.Validation;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AO.DataAccess.Contracts;
using log4net;

namespace AO.DataAccess.EF
{
    public class DataContext : DbContext, IDataContext
    {
        private static readonly ILog Log = LogManager.GetLogger(typeof(DataContext));
        private const string CommandTimeoutKey = "CommandTimeoutInSeconds";
        private bool _disposed;

        public DataContext(string nameOrConnectionString) : base(nameOrConnectionString)
        {
            InstanceId = Guid.NewGuid();
            Configuration.LazyLoadingEnabled = false;
            Configuration.ProxyCreationEnabled = false;

            // If the application has defined command timeout then use it, otherwsie go with the default of 30 seconds.
            if(!string.IsNullOrEmpty(ConfigurationManager.AppSettings[CommandTimeoutKey]))
                Database.CommandTimeout = int.Parse(ConfigurationManager.AppSettings[CommandTimeoutKey]);
            
            if (Log.IsDebugEnabled)
                Database.Log = s => Log.Debug(s.TrimEnd('\n'));
        }

        public Guid InstanceId { get; }

        /// <summary>
        ///     Saves all changes made in this context to the underlying database.
        /// </summary>
        /// <returns>The number of objects written to the underlying database.</returns>
        public override int SaveChanges()
        {
            try
            {
                return base.SaveChanges();
            }
            catch (DbEntityValidationException ex)
            {
                // Retrieve details about validation exception
                var errorMessages = ex.EntityValidationErrors.SelectMany(x => x.ValidationErrors).Select(x => x.ErrorMessage);
                var fullErrorMessage = string.Join("; ", errorMessages);
                var exceptionMessage = string.Concat(ex.Message, " The validation errors are: ", fullErrorMessage);

                // Throw a new DbEntityValidationException with the improved exception message.
                throw new DbEntityValidationException(exceptionMessage, ex.EntityValidationErrors);
            }
        }

        /// <summary>
        ///     Asynchronously saves all changes made in this context to the underlying database.
        /// </summary>
        /// <returns>A task that represents the asynchronous save operation.  The 
        ///     <see cref="Task">Task.Result</see> contains the number of 
        ///     objects written to the underlying database.</returns>
        public override async Task<int> SaveChangesAsync()
        {
            return await SaveChangesAsync(CancellationToken.None);
        }

        /// <summary>
        ///     Asynchronously saves all changes made in this context to the underlying database.
        /// </summary>
        /// <returns>A task that represents the asynchronous save operation.  The 
        ///     <see cref="Task">Task.Result</see> contains the number of 
        ///     objects written to the underlying database.</returns>
        public override async Task<int> SaveChangesAsync(CancellationToken cancellationToken)
        {
            return await base.SaveChangesAsync(cancellationToken);
        }

        public void Refresh()
        {
            var refreshableObjects = ChangeTracker.Entries().Select(c => c.Entity).ToList();

            var context = ((IObjectContextAdapter)this).ObjectContext;
            context.Refresh(RefreshMode.StoreWins, refreshableObjects);
        }

        /// <summary>
        /// Force a refresh of the context with specified data.
        /// </summary>
        public void Refresh(IEnumerable refreshableObjects)
        {
            var context = ((IObjectContextAdapter)this).ObjectContext;
            context.Refresh(RefreshMode.StoreWins, refreshableObjects);
        }


        protected override void Dispose(bool disposing)
        {
            if (!_disposed)
            {
                if (disposing)
                {
                    // free other managed objects that implement
                    // IDisposable only
                }

                _disposed = true;
            }

            base.Dispose(disposing);
        }
    }
}
