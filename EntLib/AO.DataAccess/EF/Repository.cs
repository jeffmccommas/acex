﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Threading;
using System.Threading.Tasks;
using AO.DataAccess.Contracts;
using LinqKit;

namespace AO.DataAccess.EF
{
    public class Repository<TEntity> : IRepository<TEntity> where TEntity : class
    {
        protected readonly IDataContext Context;
        protected readonly DbContext DbContext;
        protected readonly DbSet<TEntity> DbSet;

        public Repository(IDataContext context)
        {
            Context = context;
            DbContext = context as DbContext;

            if (DbContext != null)
            {
                DbSet = DbContext.Set<TEntity>();
            }
            else
            {
                var fakeContext = context as FakeDbContext;

                if (fakeContext != null)
                {
                    DbSet = fakeContext.Set<TEntity>();
                }
            }
        }

        public virtual TEntity Find(params object[] keyValues)
        {
            return DbSet.Find(keyValues);
        }

        public virtual void Refresh()
        {
            var refreshableObjects = DbSet.ToList();
            Context.Refresh(refreshableObjects);
        }

        public virtual void ExecuteCommand(string query, params object[] parameters)
        {
            DbContext?.Database.ExecuteSqlCommand(query, parameters);
        }

        public virtual void Insert(TEntity entity)
        {
            DbSet.Add(entity);
        }

        public virtual void InsertRange(IEnumerable<TEntity> entities)
        {
            DbSet.AddRange(entities);
        }

        public virtual void Update(TEntity entity)
        {
            DbSet.Attach(entity);
            if (DbContext != null) DbContext.Entry(entity).State = EntityState.Modified;
        }

        public virtual void Delete(object id)
        {
            var entity = DbSet.Find(id);
            Delete(entity);
        }

        public virtual void Delete(TEntity entity)
        {
            DbSet.Remove(entity);
        }        

        public IQuery<TEntity> Query()
        {
            return new Query<TEntity>(this);
        }

        public virtual IQuery<TEntity> Query(Expression<Func<TEntity, bool>> query)
        {
            return new Query<TEntity>(this, query);
        }

        public virtual async Task<TEntity> FindAsync(params object[] keyValues)
        {
            return await DbSet.FindAsync(keyValues);
        }

        public virtual async Task<TEntity> FindAsync(CancellationToken cancellationToken, params object[] keyValues)
        {
            return await DbSet.FindAsync(cancellationToken, keyValues);
        }

        public virtual async Task<bool> DeleteAsync(params object[] keyValues)
        {
            return await DeleteAsync(CancellationToken.None, keyValues);
        }

        public virtual async Task<bool> DeleteAsync(CancellationToken cancellationToken, params object[] keyValues)
        {
            var entity = await FindAsync(cancellationToken, keyValues);

            if (entity == null)
            {
                return false;
            }

            DbSet.Attach(entity);

            return true;
        }

        public IQueryable<TEntity> Queryable()
        {
            return DbSet;
        }

        public void SaveChanges()
        {
            Context.SaveChanges();
        }

        /// <summary>
        /// Attaches an object to the context.
        /// </summary>
        /// <param name="entity">Entity to attach.</param>
        /// <param name="state">State of the entity after it is added to the context.</param>
        protected void AttachEntity(TEntity entity, EntityState state = EntityState.Unchanged)
        {
            if (DbContext != null)
            {
                var entry = DbContext.Entry(entity);
                entry.State = state;
            }
        }

        public virtual IQueryable<TEntity> SelectQuery(string query, params object[] parameters)
        {
            return DbSet.SqlQuery(query, parameters).AsQueryable();
        }

        internal IQueryable<TEntity> Select(
                    Expression<Func<TEntity, bool>> filter = null,
                    Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> orderBy = null,
                    List<Expression<Func<TEntity, object>>> includes = null,
                    int? page = null,
                    int? pageSize = null)
        {
            IQueryable<TEntity> query = DbSet;

            if (includes != null)
            {
                try
                {
                    query = includes.Aggregate(query, (current, include) => current.Include(include));
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.ToString());
                }
            }
            if (orderBy != null)
            {
                query = orderBy(query);
            }
            if (filter != null)
            {
                query = query.AsExpandable().Where(filter);
            }
            if (page != null && pageSize != null)
            {
                query = query.Skip((page.Value - 1) * pageSize.Value).Take(pageSize.Value);
            }
            return query;
        }

        internal async Task<IEnumerable<TEntity>> SelectAsync(
            Expression<Func<TEntity, bool>> filter = null,
            Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> orderBy = null,
            List<Expression<Func<TEntity, object>>> includes = null,
            int? page = null,
            int? pageSize = null)
        {
            return await Select(filter, orderBy, includes, page, pageSize).ToListAsync();
        }
    }
}