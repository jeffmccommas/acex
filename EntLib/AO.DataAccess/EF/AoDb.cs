﻿using System.Data.Entity;
using AO.DataAccess.Contracts;
using AO.Domain;
using AO.DataAccess.Configurations;


namespace AO.DataAccess.EF
{
    public class AoDb : DataContext, IAoDb
    {
        public AoDb() : base("InsightsMetaDataConn")
        {
            Configuration.LazyLoadingEnabled = true;
            Configuration.ProxyCreationEnabled = true;
            Database.SetInitializer<AoDb>(null);
        }

        public virtual IDbSet<Tenant> Tenants { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.Configurations.Add(new TenantConfiguration());
            modelBuilder.Configurations.Add(new ClientConfiguration());
        }
    }
}
