﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using AO.DataAccess.Contracts;
using AO.Entities;
using CE.AO.Models;

namespace AO.DataAccess
{
    /// <summary>
    /// Responsible for Notification CRUD operations in cassandra
    /// </summary>
    public class NotificationRepository : INotificationRepository
    {
        private readonly ICassandraRepository _cassandraRepository;

        public NotificationRepository(ICassandraRepository cassandraRepository)
        {
            _cassandraRepository = cassandraRepository;
        }

        /// <summary>
        /// Inserts a new notification into data store.  If the notification alreadys exists
        /// then a merge is performed.
        /// </summary>
        /// <param name="notification">Notification which is to be inserted/merged</param>
        public async Task InsertOrMergeNotification(NotificationEntity notification)
        {
            notification.LastModifiedDate = DateTime.UtcNow;
            await _cassandraRepository.InsertOrUpdateAsync(notification, Constants.CassandraTableNames.Notification,'A');
        }

        public async Task InsertOrMergeNotificationReport(NotificationReportEntity notificationReport)
        {
            notificationReport.LastModifiedDate = DateTime.UtcNow;
            notificationReport.RowCreateDate = DateTime.UtcNow.ToString("yyyyMMdd");
            await _cassandraRepository.InsertOrUpdateAsync(notificationReport, Constants.CassandraTableNames.NotificationReport, 'A');
        }

        /// <summary>
        /// Deletes a notification from the data store. 
        /// </summary>
        /// <param name="clientId">Client Id of the notification that should be deleted</param>
        /// <param name="customerId">Customer Id of the notification that should be deleted</param>
        /// <param name="accountId">Acount Id of the notification that should be deleted</param>
        /// <returns></returns>
        public async Task DeleteNotification(int clientId, string customerId, string accountId)
        {
            await
                _cassandraRepository.DeleteAsync<NotificationEntity>(Constants.CassandraTableNames.Notification,
                    a => a.ClientId == clientId && a.AccountId == accountId && a.CustomerId == customerId).ConfigureAwait(false);
        }

        /// <summary>
        /// Delete notification based on the supplied notification model
        /// </summary>
        /// <param name="notification">notification</param>
        public async Task DeleteNotification(NotificationEntity notification)
        {
            await
                _cassandraRepository.DeleteAsync<NotificationEntity>(Constants.CassandraTableNames.Notification,
                    n =>
                        n.ClientId == notification.ClientId && n.CustomerId == notification.CustomerId &&
                        n.AccountId == notification.AccountId &&
                        n.ProgramName == notification.ProgramName &&
                        n.ServiceContractId == notification.ServiceContractId &&
                        n.Insight == notification.Insight &&
                        n.Channel == notification.Channel && n.IsLatest == notification.IsLatest &&
                        n.NotifiedDateTime == notification.NotifiedDateTime);
        }

        /// <summary>
        /// Retrieves fully hydrated Notification based on the supplied InsightModel and InsightSettings values.
        /// </summary>
        /// <param name="evaluation">InsightModel</param>
        /// <param name="insightSettings">InsightSettings</param>
        public async Task<NotificationEntity> GetNotification(InsightModel evaluation, InsightSettings insightSettings)
        {
            return await
                    _cassandraRepository.GetSingleAsync<NotificationEntity>(
                        Constants.CassandraTableNames.Notification,
                        n =>
                            n.ClientId == evaluation.ClientId && n.AccountId == evaluation.AccountId && n.CustomerId == evaluation.CustomerId &&
                            n.ProgramName == evaluation.ProgramName &&
                            n.ServiceContractId == evaluation.ServiceContractId &&
                            n.Insight == insightSettings.InsightName &&
                            // ReSharper disable once RedundantBoolCompare we need to explicitly specify IsLatest == true, otherwise it gives error. To execute cassandra query required to provide all values.
                            n.Channel == insightSettings.DefaultCommunicationChannel && n.IsLatest == true);
        }

        /// <summary>
        /// Retrieves fully hydrated Notification based on the supplied NotificationEntity;
        /// </summary>
        /// <param name="notification"></param>
        public async Task<NotificationEntity> GetNotification(NotificationEntity notification)
        {
            return await
                _cassandraRepository.GetSingleAsync<NotificationEntity>(Constants.CassandraTableNames.Notification,
                    n =>
                        n.ClientId == notification.ClientId && n.CustomerId == notification.CustomerId && n.AccountId == notification.AccountId &&
                        n.ProgramName == notification.ProgramName &&
                        n.ServiceContractId == notification.ServiceContractId &&
                        n.Insight == notification.Insight &&
                        n.Channel == notification.Channel && n.IsLatest == notification.IsLatest &&
                        n.NotifiedDateTime == notification.NotifiedDateTime).ConfigureAwait(false);
        }

        /// <summary>
        /// Retrieves all notifications in the data store based on the supplied <paramref name="clientId"/> 
        /// and <paramref name="accountId"/>
        /// </summary>
        /// <param name="clientId">ClientId that notifications should be retrieved for.</param>
        /// <param name="customerId">CustomerId that notifications should be retrieved for.</param>
        /// <param name="accountId">AccountId that notifications should be retrieved for.</param>
        /// <returns></returns>
        public async Task<IList<NotificationEntity>> GetNotifications(int clientId, string customerId, string accountId)
        {
            return await
                _cassandraRepository.GetAsync<NotificationEntity>(Constants.CassandraTableNames.Notification,
                    n => n.ClientId == clientId && n.AccountId == accountId && n.CustomerId == customerId)
                    .ConfigureAwait(false);
        }

        /// <summary>
        /// Retreives all notifications for a specified client that fall within the <paramref name="startDate"/> 
        /// and <paramref name="endDate"/>
        /// </summary>
        /// <param name="clientId">Client Id that notifications should be returned for</param>
        /// <param name="startDate">Start date range for the notification search</param>
        /// <param name="endDate">End date range for the notification search</param>
        /// <returns></returns>
        public async Task<IList<NotificationEntity>> GetNotifications(int clientId, DateTime startDate, DateTime endDate)
        {
            return await
                _cassandraRepository.GetAsync<NotificationEntity>(Constants.CassandraTableNames.Notification,
                    n => n.ClientId == clientId && n.NotifiedDateTime >= startDate && n.NotifiedDateTime <= endDate)
                    .ConfigureAwait(false);
        }

        public async Task<IList<NotificationEntity>> GetNotifications()
        {
            return await
                _cassandraRepository.GetAsync<NotificationEntity>(Constants.CassandraTableNames.Notification);
        }

    }
}