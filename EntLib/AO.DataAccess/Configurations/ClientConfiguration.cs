﻿using System.Data.Entity.ModelConfiguration;
using AO.Domain;

namespace AO.DataAccess.Configurations
{
    internal class ClientConfiguration : EntityTypeConfiguration<Client>
    {
        public ClientConfiguration()
        {
            ToTable("dbo.Client");
        }
    }
}
