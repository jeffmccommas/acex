﻿using System.Data.Entity.ModelConfiguration;
using AO.Domain;

namespace AO.DataAccess.Configurations
{
    internal class TenantConfiguration : EntityTypeConfiguration<Tenant>
    {
        public TenantConfiguration()
        {
            ToTable("ao.Tenant");

            HasRequired(t => t.Client)
                .WithMany()
                .HasForeignKey(t => t.ClientId);
        }
    }
}
