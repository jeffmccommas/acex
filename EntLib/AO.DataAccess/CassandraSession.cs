﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.Net;
using System.Net.Sockets;
using System.Text;
using Cassandra;

namespace AO.DataAccess
{
    public static class CassandraSession
    {
        private static Cluster _cluster;
        private static ISession _session;
        private static string _keyspace;

        private static string GetKeyspace()
        {
                if (_keyspace == null)
                {
                    _keyspace = ConfigurationManager.AppSettings.Get(StaticConfig.CassandraKeyspace);

                    if (_keyspace == null)
                    {
                        throw new ApplicationException($"[{StaticConfig.CassandraKeyspace}] not found in app configuration settings.");
                    }
                }

                return _keyspace;
        }

        public static ISession GetSession()
        {
            if (_cluster == null || _session.IsDisposed)
            {
                SetCluster();
                if (_cluster != null)
                    _session = _cluster.Connect();
            }
            else if (_session == null)
            {
                _session = _cluster.Connect();
            }

            _session.ChangeKeyspace(GetKeyspace());
            return _session;
        }

        private static void SetCluster()
        {
            StringBuilder errorAccumulator = new StringBuilder();  // cannot depend on error logging yet so save info to log later.
            string nodesList = ConfigurationManager.AppSettings.Get(StaticConfig.CassandraNodes);  // cjh -- fixfix need better config paradigm
            var foundNodes = CheckNetworkAccessibility(nodesList, errorAccumulator);
            if (foundNodes.Count == 0) {  // can't do anything. This would normally be a big problem.
                throw new Exception($"CassandraSession.SetCluster: no valid nodes found for '{nodesList}' ");
            }
            string[] nodes = foundNodes.ToArray();
            // now back to the pre-existing code with a vetted set of addresses.  Previous code was crashing on discrepencies.
            var queryOptions = new QueryOptions()
                .SetConsistencyLevel(ConsistencyLevel.One)
                .SetRetryOnTimeout(true);

            // cjh -- fixfix: especially for debugging, need to make timeout settings configurable
            var clusterBuilder = Cluster.Builder()
                .WithSocketOptions(new SocketOptions().SetConnectTimeoutMillis(20000))
                .AddContactPoints(nodes)
                .WithPoolingOptions(new PoolingOptions().SetHeartBeatInterval(50000))
                .WithQueryOptions(queryOptions)
                .WithRetryPolicy(new DefaultRetryPolicy())
                .WithLoadBalancingPolicy(new TokenAwarePolicy(new DCAwareRoundRobinPolicy()))
                .WithQueryTimeout(20000);

            var username = ConfigurationManager.AppSettings.Get(StaticConfig.CassandraNodeUsername);
            var password = ConfigurationManager.AppSettings.Get(StaticConfig.CassandraNodePassword);
            if (!string.IsNullOrWhiteSpace(username) && !string.IsNullOrWhiteSpace(password))
            {
                clusterBuilder = clusterBuilder.WithCredentials(username, password);
            }

            var cluster = clusterBuilder.Build();

            _cluster = cluster;

            if (errorAccumulator.Length > 0) {  // Saved until now because current logger goes to cassandra and if it is not there we'd fail this
                Logger logger = new Logger(typeof(string));
                logger.Error(errorAccumulator.ToString(), new object[] { nodesList });
            }

        }

        /// <summary>
        /// check the accessibility of nodes on network.  Old code was crasshing if a network error occurred.
        /// Minimal check initially but can be expanded.
        /// Public to support unit test (vstudio requirement)
        /// </summary>
        /// <param name="nodesList">csv string of nodes</param>
        /// <param name="errorAccumulator">place to put error information</param>
        /// <returns></returns>
        public static List<string> CheckNetworkAccessibility(string nodesList, StringBuilder errorAccumulator) {
            string[] provisionalNodes = nodesList.Split(',');
            List<string> foundNodes = new List<string>();
            foreach (string tNode in provisionalNodes) {
                // check each node for acccessibility and discard if not.
                try {
                    IPHostEntry hostEntry = Dns.GetHostEntry(tNode);
                    if (hostEntry != null) {
                        foundNodes.Add(tNode);
                    }
                }
                catch (SocketException exc) {
                    string erinfo = $"Socket Exception: CassandraSession.SetCluster addr='{tNode}' = {exc.Message}";
                    Debug.WriteLine(erinfo);
                    errorAccumulator.Append(erinfo + Environment.NewLine);
                }
                catch (Exception exc) {
                    string erinfo = $"Exception: CassandraSession.SetCluster addr='{tNode}' = {exc.Message}";
                    Debug.WriteLine(erinfo);
                    errorAccumulator.Append(erinfo + Environment.NewLine);
                }
            }
            return foundNodes;
        }
    }
}
