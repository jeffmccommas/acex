﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using AO.DataAccess.EF;
using AO.DataAccess.Contracts;
using AO.Domain;
using log4net;

namespace AO.DataAccess
{
    /// <summary>
    /// Repository for the Tenant
    /// </summary>
    /// <seealso>
    ///     <cref>AO.DataAccess.Repository{AO.Domain.Tenant}</cref>
    /// </seealso>
    public class TenantRepository : Repository<Tenant>, ITenantRepository
    {
        internal static readonly ILog Log = LogManager.GetLogger(typeof(TenantRepository));

        /// <summary>
        /// Initializes a new instance of the <see cref="TenantRepository"/> class.
        /// </summary>
        /// <param name="context">The context.</param>
        public TenantRepository(IAoDb context) : base(context)
        {

        }

        /// <summary>
        /// Gets the Tenant by its Tenant ID.
        /// </summary>
        /// <param name="tenantId">The ID of the tenant.</param>
        /// <returns></returns>
        public Tenant GetByTenantId(string tenantId)
        {
            var tenant =
                Queryable()
                    .Include(t => t.Client)
                    .SingleOrDefault(t => t.TenantId == tenantId);

            return tenant;
        }

        /// <summary>
        /// Gets the list of Clients
        /// </summary>
        /// <returns></returns>
        public List<Client> GetClients()
        {
            var clients = Queryable().Select(t => t.Client).ToList();
            return clients;
        }
    }
}