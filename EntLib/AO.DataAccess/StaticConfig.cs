﻿namespace AO.DataAccess
{
    public static class StaticConfig
    {        
        public const string CassandraNodes = "CassandraNodes";
        public const string CassandraNodeUsername = "CassandraNodeUsername";
        public const string CassandraNodePassword = "CassandraNodePassword";
        public const string CassandraKeyspace = "CassandraKeyspace";
        public const string AzureStorageConnectionString = "AzureStorageConnectionString";
        public const string OdbcConnectionString = "OdbcConnectionString";
    }
}
