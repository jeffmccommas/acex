﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading;
using System.Threading.Tasks;
using Cassandra.Data.Linq;
using Cassandra.Mapping;
using AO.DataAccess.Contracts;
using Cassandra;

namespace AO.DataAccess
{
    public class CassandraRepository : ICassandraRepository
    {
        /// <summary>
        /// Perform a simple paged CQL query against cassandra and invoke the provided delegate Action for reach row in the paged resultset.
        /// </summary>
        /// <param name="cqlQuery">The CQL query expression to invoke.</param>
        /// <param name="aoRowAction">The user provided delegate to invoke for each row.</param>
        /// <param name="pageSize">The number of rows to batch in each page request.</param>
        public void ExecutePagedQuery(string cqlQuery, AoRowAction aoRowAction, int pageSize = 1000)
        {
            var session = CassandraSession.GetSession();
            var statement = new SimpleStatement(cqlQuery);

            statement.SetPageSize(pageSize);

            var rs = session.Execute(statement);

            var columnNames = rs.Columns.Select(c => c.Name).ToArray();

            long rowNumber = 1;

            foreach (var row in rs)
            {
                var columnValues = row.Select(c => c?.ToString() ?? "").ToArray();

                // Invoke user provided delegate to perform the desired action for each row.
                aoRowAction(rowNumber, columnValues, columnNames);

                rowNumber++;
            }
        }
		
        private static CqlQuery<T> CreateCqlQuery<T>(string tableName, Expression<Func<T, bool>> filter = null) where T : class
        {
            var session = CassandraSession.GetSession();
            CqlQuery<T> query = new Table<T>(session, MappingConfiguration.Global, tableName);
            if (filter != null)
            {
                query = query.Where(filter);
            }
            return query;
        }

        public bool InsertOrUpdateBatch<T>(List<T> tableModel, string tableName) where T : class
        {
            var session = CassandraSession.GetSession();
            var batch = session.CreateBatch();

            var query = new Table<T>(session, MappingConfiguration.Global, tableName);
            foreach (var entity in tableModel)
            {
                batch.Append(query.Insert(entity));
            }
            batch.Execute();
            return true;
        }

        public async Task InsertOrUpdateBatchAsync<T>(List<T> tableModel, string tableName) where T : class
        {
            var session = CassandraSession.GetSession();
            var batch = session.CreateBatch();
            var query = new Table<T>(session, MappingConfiguration.Global, tableName);
            foreach (var entity in tableModel)
            {
                batch.Append(query.Insert(entity));
            }
            await batch.ExecuteAsync().ConfigureAwait(false);
        }

        public bool InsertOrUpdate<T>(T tableModel, string tableName) where T : class {
            return InsertOrUpdate(tableModel, tableName, 'A');
#if false
            var session = CassandraSession.GetSession();
            var query = new Table<T>(session, MappingConfiguration.Global, tableName);
            query.Insert(tableModel).Execute();
            return true;
#endif
        }
        public bool InsertOrUpdate<T>(T tableModel, string tableName, char consistency) where T : class
        {
            var session = CassandraSession.GetSession();
            var query = new Table<T>(session, MappingConfiguration.Global, tableName);
            switch (consistency)
            {
                case '0':
                    query.SetConsistencyLevel(ConsistencyLevel.Any);
                    break;
                case '1':
                    query.SetConsistencyLevel(ConsistencyLevel.One);
                    break;
                case 'q':
                    query.SetConsistencyLevel(ConsistencyLevel.Quorum);
                    break;
                default:
                    query.SetConsistencyLevel(ConsistencyLevel.All);
                    break;
            }
            query.Insert(tableModel).Execute();
            return true;
        }

        public async Task InsertOrUpdateAsync<T>(T tableModel, string tableName) where T : class
        {
            await InsertOrUpdateAsync(tableModel, tableName, 'A');
#if false
            var session = CassandraSession.GetSession();
            var query = new Table<T>(session, MappingConfiguration.Global, tableName);
            await query.Insert(tableModel).ExecuteAsync().ConfigureAwait(false);
#endif
        }

#pragma warning disable 1998
        public async Task InsertOrUpdateAsync<T>(T tableModel, string tableName, char consistency) where T : class
        {
            var session = CassandraSession.GetSession();
            var query = new Table<T>(session, MappingConfiguration.Global, tableName);
            switch (consistency)
            {
                case '0':
                    query.SetConsistencyLevel(ConsistencyLevel.Any);
                    break;
                case '1':
                    query.SetConsistencyLevel(ConsistencyLevel.One);
                    break;
                case 'q':
                    query.SetConsistencyLevel(ConsistencyLevel.Quorum);
                    break;
                default:
                    query.SetConsistencyLevel(ConsistencyLevel.All);
                    break;
            }
            Task t = query.Insert(tableModel).ExecuteAsync();
            int maxMsecsWait = 10000;  // clamp how long will wait for completion.  
            while (!t.IsCompleted && (maxMsecsWait-- > 0)) {
                //Debug.Write("I")
                Thread.Sleep(1);
            }
        }

#pragma warning restore 1998

        public bool InsertOrUpdate<T>(T tableModel, string tableName, int ttlInSeconds) where T : class
        {
            var session = CassandraSession.GetSession();
            var query = new Table<T>(session, MappingConfiguration.Global, tableName);
            query.SetConsistencyLevel(ConsistencyLevel.All);
            query.Insert(tableModel).SetTTL(ttlInSeconds).Execute();
            return true;
        }

        public async Task InsertOrUpdateAsync<T>(T tableModel, string tableName, int ttlInSeconds) where T : class
        {
            var session = CassandraSession.GetSession();
            var query = new Table<T>(session, MappingConfiguration.Global, tableName);
            query.SetConsistencyLevel(ConsistencyLevel.All);
            await query.Insert(tableModel).SetTTL(ttlInSeconds).ExecuteAsync().ConfigureAwait(false);
        }

        public IList<T> Get<T>(string tableName, Expression<Func<T, bool>> filter = null) where T : class
        {
            var session = CassandraSession.GetSession();
            CqlQuery<T> query = new Table<T>(session, MappingConfiguration.Global, tableName);
            if (filter != null)
            {
                query = query.Where(filter);
            }

            return query.Execute().ToList();
        }

        public IList<T> Get<T>(string tableName, char consistency, Expression<Func<T, bool>> filter = null) where T : class
        {
            var session = CassandraSession.GetSession();
            CqlQuery<T> query = new Table<T>(session, MappingConfiguration.Global, tableName);
            if (filter != null)
            {
                query = query.Where(filter);
            }

            switch (consistency)
            {
                case '0':
                    query.SetConsistencyLevel(ConsistencyLevel.Any);
                    break;
                case '1':
                    query.SetConsistencyLevel(ConsistencyLevel.One);
                    break;
                case 'q':
                    query.SetConsistencyLevel(ConsistencyLevel.Quorum);
                    break;
                default:
                    query.SetConsistencyLevel(ConsistencyLevel.All);
                    break;
            }

            return query.Execute().ToList();
        }

        public IList<TResult> Get<T, TResult>(string tableName, Expression<Func<T, TResult>> selection, Expression<Func<T, bool>> filter = null) where T : class
        {
            var session = CassandraSession.GetSession();
            CqlQuery<T> query = new Table<T>(session, MappingConfiguration.Global, tableName);
            if (filter != null)
            {
                query = query.Where(filter);
            }

            CqlQuery<TResult> selectorQuery = query.Select(selection);
            return selectorQuery.Execute().ToList();
        }

        public IList<T> Get<T>(string tableName, int pageSize, ref byte[] token, Expression<Func<T, bool>> filter = null) where T : class
        {
            CqlQuery<T> query = CreateCqlQuery(tableName, filter);
            query.SetPageSize(pageSize);
            if (token != null)
            {
                query.SetPagingState(token);
            }
            IPage<T> p = query.ExecutePaged();
            token = p.PagingState;
            return p.ToList();
        }

        public async Task<IList<T>> GetAsync<T>(string tableName, Expression<Func<T, bool>> filter = null) where T : class
        {
            var session = CassandraSession.GetSession();
            CqlQuery<T> query = new Table<T>(session, MappingConfiguration.Global, tableName);
            if (filter != null)
            {
                query = query.Where(filter);
            }

            return (await query.ExecuteAsync().ConfigureAwait(false)).ToList();
        }

        public async Task<IList<T>> GetAsync<T>(string tableName, char consistency, Expression<Func<T, bool>> filter = null) where T : class
        {
            var session = CassandraSession.GetSession();
            CqlQuery<T> query = new Table<T>(session, MappingConfiguration.Global, tableName);
            if (filter != null)
            {
                query = query.Where(filter);
            }
            switch (consistency)
            {
                case '0':
                    query.SetConsistencyLevel(ConsistencyLevel.Any);
                    break;
                case '1':
                    query.SetConsistencyLevel(ConsistencyLevel.One);
                    break;
                case 'q':
                    query.SetConsistencyLevel(ConsistencyLevel.Quorum);
                    break;
                default:
                    query.SetConsistencyLevel(ConsistencyLevel.All);
                    break;
            }

            if (query.ConsistencyLevel == ConsistencyLevel.All)
            {
                var t = query.ExecuteAsync();
                while (!t.IsCompleted)
                {
                    //Debug.Write("D")
                    Thread.Sleep(5);
                }
                return t.Result.ToList();
            }
            return (await query.ExecuteAsync().ConfigureAwait(false)).ToList();
        }

        public async Task<IList<TResult>> GetAsync<T, TResult>(string tableName, Expression<Func<T, TResult>> selection, Expression<Func<T, bool>> filter = null) where T : class
        {
            var session = CassandraSession.GetSession();
            CqlQuery<T> query = new Table<T>(session, MappingConfiguration.Global, tableName);
            if (filter != null)
            {
                query = query.Where(filter);
            }

            CqlQuery<TResult> selectorQuery = query.Select(selection);
            return (await selectorQuery.ExecuteAsync().ConfigureAwait(false)).ToList();
        }

        public T GetSingle<T>(string tableName, Expression<Func<T, bool>> filter = null) where T : class
        {
            var session = CassandraSession.GetSession();
            CqlQuery<T> query = new Table<T>(session, MappingConfiguration.Global, tableName);
            if (filter != null)
            {
                query = query.Where(filter);
            }

            return query.FirstOrDefault().Execute();
        }

        public TResult GetSingle<T, TResult>(string tableName, Expression<Func<T, TResult>> selection, Expression<Func<T, bool>> filter = null) where T : class
        {
            var session = CassandraSession.GetSession();
            CqlQuery<T> query = new Table<T>(session, MappingConfiguration.Global, tableName);
            if (filter != null)
            {
                query = query.Where(filter);
            }

            CqlQuery<TResult> selectorQuery = query.Select(selection);
            return selectorQuery.FirstOrDefault().Execute();
        }

        public async Task<T> GetSingleAsync<T>(string tableName, Expression<Func<T, bool>> filter = null) where T : class
        {
            var session = CassandraSession.GetSession();
            CqlQuery<T> query = new Table<T>(session, MappingConfiguration.Global, tableName);
            if (filter != null)
            {
                query = query.Where(filter);
            }

            return await query.FirstOrDefault().ExecuteAsync().ConfigureAwait(false);
        }

        public async Task<T> GetSingleAsync<T>(string tableName, char consistency, Expression<Func<T, bool>> filter = null) where T : class
        {
            var session = CassandraSession.GetSession();
            CqlQuery<T> query = new Table<T>(session, MappingConfiguration.Global, tableName);
            if (filter != null)
            {
                query = query.Where(filter);
            }
            
            switch (consistency)
            {
                case '0':
                    query.SetConsistencyLevel(ConsistencyLevel.Any);
                    break;
                case '1':
                    query.SetConsistencyLevel(ConsistencyLevel.One);
                    break;
                case 'q':
                    query.SetConsistencyLevel(ConsistencyLevel.Quorum);
                    break;
                default:
                    query.SetConsistencyLevel(ConsistencyLevel.All);
                    break;
            }

            if (query.ConsistencyLevel == ConsistencyLevel.All)
            {
                Task<T> t = query.FirstOrDefault().ExecuteAsync();
                while (!t.IsCompleted)
                {
                    //Debug.Write("D")
                    Thread.Sleep(5);
                }

                return t.Result;
            }
            return await query.FirstOrDefault().ExecuteAsync().ConfigureAwait(false);
            
        }

        public async Task<TResult> GetSingleAsync<T, TResult>(string tableName, Expression<Func<T, TResult>> selection, Expression<Func<T, bool>> filter = null) where T : class
        {
            var session = CassandraSession.GetSession();
            CqlQuery<T> query = new Table<T>(session, MappingConfiguration.Global, tableName);
            if (filter != null)
            {
                query = query.Where(filter);
            }

            CqlQuery<TResult> selectorQuery = query.Select(selection);
            return await selectorQuery.FirstOrDefault().ExecuteAsync().ConfigureAwait(false);
        }

        public TResult ExecuteSelectQuery<T, TResult>(CqlQuery<T> query, Expression<Func<T, TResult>> selection) where T : class
        {
            CqlQuery<TResult> q = query.Select(selection);
            return q.FirstOrDefault().Execute();
        }

        public void ExecuteUpdateQuery(string updateQuery, params object [] properties)
        {
            var session = CassandraSession.GetSession();
            var statement = session.Prepare(updateQuery);
            var boundStatement = statement.Bind(properties);
            session.Execute(boundStatement);
        }

        public CqlQuery<T> CreateQuery<T>(string tableName, Expression<Func<T, bool>> filter = null) where T : class
        {
            var session = CassandraSession.GetSession();
            CqlQuery<T> query = new Table<T>(session, MappingConfiguration.Global, tableName);

            if (filter != null)
            {
                query = query.Where(filter);
            }

            return query;
        }

        public async Task DeleteAsync<T>(string tableName, Expression<Func<T, bool>> filter) where T : class {
            await DeleteAsync(tableName, filter, 'A');
#if false
            var session = CassandraSession.GetSession();
            CqlQuery<T> query = new Table<T>(session, MappingConfiguration.Global, tableName);
            query = query.Where(filter);
            await query.Delete().ExecuteAsync().ConfigureAwait(false);
#endif
        }

#pragma warning disable 1998
        public async Task DeleteAsync<T>(string tableName, Expression<Func<T, bool>> filter, char consistency) where T : class
        {
            var session = CassandraSession.GetSession();
            CqlQuery<T> query = new Table<T>(session, MappingConfiguration.Global, tableName);
            query = query.Where(filter);
            switch (consistency)
            {
                case '0':
                    query.SetConsistencyLevel(ConsistencyLevel.Any);
                    break;
                case '1':
                    query.SetConsistencyLevel(ConsistencyLevel.One);
                    break;
                case 'q':
                    query.SetConsistencyLevel(ConsistencyLevel.Quorum);
                    break;
                default:
                    query.SetConsistencyLevel(ConsistencyLevel.All);
                    break;
            }
            //await query.Delete().ExecuteAsync().ConfigureAwait(false) 

            Task t = query.Delete().ExecuteAsync();
            while (!t.IsCompleted)
            {
                //Debug.Write("D")
                Thread.Sleep(5);
            }
        }
#pragma warning restore 1998

        public void Delete<T>(string tableName, Expression<Func<T, bool>> filter) where T : class
        {
            var session = CassandraSession.GetSession();
            CqlQuery<T> query = new Table<T>(session, MappingConfiguration.Global, tableName);
            query = query.Where(filter);
            query.Delete().Execute();
        }

        public void Delete<T>(string tableName, Expression<Func<T, bool>> filter, char consistency) where T : class
        {
            var session = CassandraSession.GetSession();
            CqlQuery<T> query = new Table<T>(session, MappingConfiguration.Global, tableName);
            query = query.Where(filter);
            switch (consistency)
            {
                case '0':
                    query.SetConsistencyLevel(ConsistencyLevel.Any);
                    break;
                case '1':
                    query.SetConsistencyLevel(ConsistencyLevel.One);
                    break;
                case 'q':
                    query.SetConsistencyLevel(ConsistencyLevel.Quorum);
                    break;
                default:
                    query.SetConsistencyLevel(ConsistencyLevel.All);
                    break;
            }

            query.Delete().Execute();
        }


    }
}
