﻿
namespace CE.BillDisagg.Enums
{
    public enum DisaggStatusType
    {
        Unspecified,
        ActualBills,
        ComputedBills,
        ModelOnly,
        Failed
    }
}
