﻿
namespace CE.BillDisagg.Enums
{
    public enum CurrencyType
    {
        Unspecified,
        USD,
        CAD,
        EUR
    }
}
