﻿using System;
using System.Collections.Generic;
using System.Linq;
using CE.BillDisagg.DataAccess;
using CE.ContentModel;
using CE.EnergyModel;
using CE.EnergyModel.Enums;
using CE.EnergyModel.Helper;

namespace CE.BillDisagg
{
    public partial class BillDisaggManager
    {
        private readonly int _clientId;
        private readonly string _overrideConnStringInsightsDw = string.Empty;
        private readonly string _overrideConnStringInsightsMetadata = string.Empty;
        private readonly string _overrideConnStringInsights = string.Empty;

        private readonly bool _overridingConnStrings;
        private DataAccessLayer _dataAccessLayer;
        private BillDisaggConfiguration _config;

        private IContentProvider _contentProvider;
        private int _cacheTimeoutInMinutes = 20;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="clientId">A client Id.</param>
        public BillDisaggManager(int clientId)
        {
            _clientId = clientId;
            InitDataAccess();
            InitContentProvider();
            LoadConfiguration();
        }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="clientId">A client Id</param>
        /// <param name="csInsightsDw">The connection string for InsightsDW</param>
        /// <param name="csInsightsMetadata">The connection string for InsightMetadata</param>
        /// <param name="csInsights">The connection string for Insights</param>
        public BillDisaggManager(int clientId, string csInsightsDw, string csInsightsMetadata, string csInsights)
        {
            _clientId = clientId;

            InitDataAccess(csInsightsMetadata, csInsightsDw, csInsights);
            InitContentProvider(csInsightsMetadata);

            _overridingConnStrings = true;
            _overrideConnStringInsightsDw = csInsightsDw;
            _overrideConnStringInsightsMetadata = csInsightsMetadata;
            _overrideConnStringInsights = csInsights;

            LoadConfiguration();
        }


        /// <summary>
        /// Execute bill disaggregation.  Main entry point for DW callers.
        /// </summary>
        /// <param name="customerId"></param>
        /// <param name="accountId"></param>
        /// <param name="premiseId"></param>
        /// <param name="startDate"></param>
        /// <param name="endDate"></param>
        /// <returns></returns>
        public BillDisaggResult ExecuteBillDisagg(string customerId, string accountId, string premiseId,
            DateTime startDate, DateTime endDate)
        {
            var p = new DisaggInputParams()
            {
                CustomerId = customerId,
                AccountId = accountId,
                PremiseId = premiseId,
                StartDate = startDate,
                EndDate = endDate
            };

            return ExecuteBillDisagg(p);
        }

        /// <summary>
        /// Execute bill disaggregation.
        /// </summary>
        /// <param name="customerId"></param>
        /// <param name="accountId"></param>
        /// <param name="premiseId"></param>
        /// <returns></returns>
        public BillDisaggResult ExecuteBillDisagg(string customerId, string accountId, string premiseId)
        {
            var p = new DisaggInputParams()
            {
                CustomerId = customerId,
                AccountId = accountId,
                PremiseId = premiseId
            };

            return ExecuteBillDisagg(p);
        }

        /// <summary>
        /// Execute bill disaggregation.
        /// </summary>
        /// <param name="customerId"></param>
        /// <param name="accountId"></param>
        /// <param name="premiseId"></param>
        /// <param name="isBusiness"></param>
        /// <returns></returns>
        public BillDisaggResult ExecuteBillDisagg(string customerId, string accountId, string premiseId, bool isBusiness)
        {
            var p = new DisaggInputParams()
            {
                CustomerId = customerId,
                AccountId = accountId,
                PremiseId = premiseId,
                IsBusiness = isBusiness
            };

            return (ExecuteBillDisagg(p));
        }

        /// <summary>
        /// Execute bill disaggregation.
        /// </summary>
        /// <param name="inputParams"></param>
        /// <returns></returns>
        public BillDisaggResult ExecuteBillDisagg(DisaggInputParams inputParams)
        {
            // if no bill collection exists, since one was not passed in, populate it here
            if (inputParams?.CustomerId != null && inputParams.AccountId != null && inputParams.PremiseId != null)
            {
                if (inputParams.Bills == null || (inputParams.Bills != null && inputParams.Bills.Count == 0))
                {
                    var bills = _dataAccessLayer.GetBills(_clientId, inputParams.CustomerId, inputParams.AccountId,
                        inputParams.PremiseId);

                    inputParams.Bills = bills;
                }
            }

            return MainExecuteBillDisagg(inputParams);
        }


        /// <summary>
        /// used by weather highlight to get the updated energy model result after bill disagg
        /// </summary>
        /// <param name="inputParams"></param>
        /// <param name="commodities"></param>
        /// <returns></returns>
        public EnergyModelResult UpdateEnergyModelWithBillDisagg(DisaggInputParams inputParams, List<CommodityType> commodities)
        {
            List<Bill> computedBills;
            List<DisaggStatus> disaggStatuses;

            if (inputParams?.CustomerId != null && inputParams.AccountId != null && inputParams.PremiseId != null)
            {
                if (inputParams.Bills == null || (inputParams.Bills != null && inputParams.Bills.Count == 0))
                {
                    var bills = _dataAccessLayer.GetBills(_clientId, inputParams.CustomerId, inputParams.AccountId,
                        inputParams.PremiseId);

                    inputParams.Bills = bills;
                }
            }

            var emResult = UpdateEnergyModelWithBillDisagg(inputParams, commodities, out computedBills, out disaggStatuses);
            return emResult;
        }

        

        /// <summary>
        /// Main private entry point for logic.
        /// </summary>
        /// <param name="inputParams"></param>
        /// <returns></returns>
        private BillDisaggResult MainExecuteBillDisagg(DisaggInputParams inputParams)
        {
            var actualBills = inputParams.Bills;

            List<Bill> computedBills;
            List<DisaggStatus> disaggStatuses;
            var emResult = UpdateEnergyModelWithBillDisagg(inputParams,null, out computedBills, out disaggStatuses);

            //Assign results to BillDisaggResult
            var result = new BillDisaggResult
            {
                CustomerId = inputParams.CustomerId,
                AccountId = inputParams.AccountId,
                PremiseId = inputParams.PremiseId,
                EndUses = emResult.EndUses,
                Appliances = emResult.Appliances,
                ActualBills = actualBills,
                ComputedBills = computedBills,
                DisaggStatuses = disaggStatuses
            };

            // set date range to newest and back annual
            if (actualBills?.Any() == true)
            {
                var newestBill =
                    inputParams.Bills.FirstOrDefault(b => b.EndDate == inputParams.Bills.Max(x => x.EndDate));
                if (newestBill != null)
                {
                    var startDate = newestBill.EndDate.AddYears(-1);
                    result.EndDate = newestBill.EndDate;
                    result.StartDate = startDate;
                }
            }
            else
            {
                var now = DateTime.Now;
                var date = new DateTime(now.Year, now.Month, 1).Date;
                result.EndDate = date;
                result.StartDate = date.AddYears(-1);
            }

            // Finally, create a DataTable for some callers to easily persist the results from BillDisagg
            PopulateFlatTable(result, inputParams);

            return result;
        }

        private EnergyModelResult UpdateEnergyModelWithBillDisagg(DisaggInputParams inputParams, List<CommodityType> commodityList, out List<Bill> computedBills, out List<DisaggStatus> disaggStatusList)
        {
            EnergyModelResult emResult;
            var disaggStatuses = new List<DisaggStatus>();
            List<CommodityType> commodities;

            var valid = false;

            // if no energy model provided, then execute the model for the provided customer
            if (inputParams.EMResultAsInput == null)
            {
                var factory = new EnergyModelManagerFactory();

                var mgr = !_overridingConnStrings
                    ? factory.CreateEnergyModelManager(_clientId)
                    : factory.CreateEnergyModelManager(_clientId, _overrideConnStringInsightsDw,
                        _overrideConnStringInsightsMetadata, _overrideConnStringInsights);

                var idParams = new IDParams()
                {
                    CustomerId = inputParams.CustomerId,
                    AccountId = inputParams.AccountId,
                    PremiseId = inputParams.PremiseId
                };

                emResult = mgr.ExecuteEnergyModel(idParams, new ExtraParams { IsBusiness = inputParams.IsBusiness }, null,
                    null, null);
            }
            else
            {
                emResult = inputParams.EMResultAsInput;
            }

            // remove non bill disagg enduses and cleanup addons
            var billdisaggCategory = "billdisagg";
            if (emResult.ModelType == ModelType.Business)
            {
                billdisaggCategory = "business";
            }

            emResult.EndUses.RemoveAll(a => a.Category.ToLower() != billdisaggCategory);
            emResult.EndUses.RemoveAddOnEnergyUses();
            emResult.Appliances.RemoveAddOnEnergyUses();
            emResult.Appliances.RemoveEmpty();
            emResult.Appliances.SetOriginalQuantities();
            //This is in case we need to revert to model usages because of bill disagg failure
            emResult.TotalEnergyUses.Clear(); //Totals are no longer valid so clear this collection

            var actualBills = inputParams.Bills;

            if (actualBills?.Any() == true)
            {
                // check actual bills usage units of measure and convert into Energy Model required units of measure; model requires kwh, therms, & gallons
                var conversionInfo = ModifyActualBillUnitOfMeasure(actualBills);

                //valid commodities are those with two or more bills
                commodities = commodityList ?? GetValidCommodities(actualBills);
                if (emResult.ModelType == ModelType.Business)
                {
                    commodities.Remove(CommodityType.Water); //business does not model water currently
                }

                // create disagg status for all the commodities with bills
                commodities.ForEach(
                    c => disaggStatuses.Add(new DisaggStatus { Commodity = c, Status = Enums.DisaggStatusType.ModelOnly }));

                //create computed bills from actual bills if possible; bill retrieval already occured, or bills were passed
                computedBills = CreateComputedBills(ref valid, actualBills, commodities, emResult);

                //one or more commodities have valid positive usage computed bills for disaggregation logic
                if (valid && computedBills?.Any() == true)
                {
                    // update bill disagg status to computed bills
                    disaggStatuses.ForEach(s => s.Status = Enums.DisaggStatusType.ComputedBills);

                    //Adjust modeled energy for heating and cooling appliances
                    var bills = actualBills.OrderByDescending(d => d.EndDate.AddDays(-1)).Take(1).ToList();

                    var endDate = inputParams.EndDate ?? bills[0].EndDate.AddDays(-1);
                    var startDate = inputParams.StartDate ?? endDate.AddYears(-1);

                    var weatherCalc = new WeatherCalc();
                    weatherCalc.AdjustModeledEnergyUsingActualWeather(startDate, endDate, commodities, emResult);

                    //Reconcile USAGE and COST
                    ReconcileModeledUsageAndCost(computedBills, actualBills, commodities, emResult, disaggStatuses);

                    //Check appliance results for anomolies and reset to model if disagg results are bad
                    var resultsValid = CheckResultsForValidity(computedBills, commodities, emResult);
                    if (!resultsValid)
                    {
                        emResult.Appliances.ResetQuantities();

                        ResetModeledCost(emResult);

                        // reset disagg status to model only for all commodities with bill
                        disaggStatuses.ForEach(s => s.Status = Enums.DisaggStatusType.ModelOnly);
                    }

                    //Recalculate EndUses after appliance adjustments
                    UpdateEndUses(emResult);

                }
                else  // if not valid, update usage and cost using model
                {
                    disaggStatuses = new List<DisaggStatus>();

                    //Adjust modeled energy for heating and cooling appliances
                    // set start date and end date to be beginning of next month of last year to beginning of current month for no bill
                    var now = DateTime.Now;
                    var endDate = inputParams.EndDate ?? new DateTime(now.Year, now.Month, 1).Date;
                    var startDate = inputParams.StartDate ?? endDate.AddMonths(1).AddYears(-1);

                    var weatherCalc = new WeatherCalc();
                    weatherCalc.AdjustModeledEnergyUsingActualWeather(startDate, endDate, commodities, emResult);

                    //Reconcile USAGE and COST
                    ReconcileModeledUsageAndCost(computedBills, null, new List<CommodityType>(), emResult, disaggStatuses);
                }

                // all results are in the model units of measure.  Iterate the Energy Model results and convert back to the actual bills unit of measure, 
                // then the output data will be in the units of measure specified in bills.
                if (conversionInfo?.Any() == true)
                {
                    ConvertBackToUnitsOfMeasure(conversionInfo, emResult, actualBills);
                }

            }
            else // no bill
            {
                // get all the commdities for no bill
                commodities = new List<CommodityType>();

                var commoditiesWithoutBills = commodityList ?? GetCommoditiesWithoutBills(commodities);

                // create computed bills using model
                computedBills = CreateComputedBills(ref valid, null, commoditiesWithoutBills, emResult);

                //Adjust modeled energy for heating and cooling appliances
                // set start date and end date to be beginning of next month of last year to beginning of current month for no bill
                var now = DateTime.Now;
                var endDate = inputParams.EndDate ?? new DateTime(now.Year, now.Month, 1).Date;
                var startDate = inputParams.StartDate ?? endDate.AddMonths(1).AddYears(-1);

                var weatherCalc = new WeatherCalc();
                weatherCalc.AdjustModeledEnergyUsingActualWeather(startDate, endDate, commoditiesWithoutBills, emResult);

                //Reconcile USAGE and COST
                ReconcileModeledUsageAndCost(computedBills, null, commodities, emResult, disaggStatuses);
            }

            disaggStatusList = disaggStatuses;
            return emResult;
        }


        private void ConvertBackToUnitsOfMeasure(List<BillConversionInfo> conversionInfo, EnergyModelResult em,
            List<Bill> actualBills)
        {
            var gasConversionFactor = 1.0m;
            var waterConversionFactor = 1.0m;
            var gasUomKey = "ccf";
            var waterUomKey = "gallons";
            var gasUom = UnitOfMeasureType.Unspecified;
            var waterUom = UnitOfMeasureType.Unspecified;

            var cinfo = conversionInfo.Find(c => c.CommodityKey == "gas");
            if (cinfo != null)
            {
                gasConversionFactor = cinfo.ConversionFactor;
                gasUomKey = cinfo.UomKey;
                gasUom = cinfo.Uom;
            }

            cinfo = conversionInfo.Find(c => c.CommodityKey == "water");
            if (cinfo != null)
            {
                waterConversionFactor = cinfo.ConversionFactor;
                waterUomKey = cinfo.UomKey;
                waterUom = cinfo.Uom;
            }

            // update all applicable **appliances** with converted quantity and unit of measure string to match what was used in bill data.
            foreach (var a in em.Appliances)
            {
                foreach (var e in a.EnergyUses)
                {
                    switch (e.CommodityKey.ToLower())
                    {
                        case "gas":
                            // if unspecified, there was no info for this commodity so do not process this
                            if (gasUom != UnitOfMeasureType.Unspecified)
                            {
                                e.Quantity = e.Quantity * (1.0m / gasConversionFactor);
                                e.OriginalQuantity = e.OriginalQuantity * (1.0m / gasConversionFactor);
                                e.UomKey = gasUomKey;
                                e.UnitOfMeasure = gasUom;
                            }
                            break;

                        case "water":
                            // if unspecified, there was no info for this commodity so do not process this
                            if (waterUom != UnitOfMeasureType.Unspecified)
                            {
                                e.Quantity = e.Quantity * (1.0m / waterConversionFactor);
                                e.OriginalQuantity = e.OriginalQuantity * (1.0m / waterConversionFactor);
                                e.UomKey = waterUomKey;
                                e.UnitOfMeasure = waterUom;
                            }
                            break;
                    }
                }
            }

            // update all applicable **end uses** with converted quantity and unit of measure string to match what was used in bill data.
            foreach (var eu in em.EndUses)
            {
                foreach (var e in eu.EnergyUses)
                {
                    switch (e.CommodityKey.ToLower())
                    {

                        case "gas":
                            // if unspecified, there was no info for this commodity so do not process this
                            if (gasUom != UnitOfMeasureType.Unspecified)
                            {
                                e.Quantity = e.Quantity * (1.0m / gasConversionFactor);
                                e.UomKey = gasUomKey;
                                e.UnitOfMeasure = gasUom;
                            }
                            break;

                        case "water":
                            // if unspecified, there was no info for this commodity so do not process this
                            if (waterUom != UnitOfMeasureType.Unspecified)
                            {
                                e.Quantity = e.Quantity * (1.0m / waterConversionFactor);
                                e.UomKey = waterUomKey;
                                e.UnitOfMeasure = waterUom;
                            }
                            break;
                    }
                }
            }

            //actual bills **get back the original actual bills**
            if (actualBills != null && actualBills.Any())
            {
                foreach (var bill in actualBills)
                {
                    switch (bill.CommodityKey.ToLower())
                    {

                        case "gas":
                            if (gasUom != UnitOfMeasureType.Unspecified)
                            {
                                bill.UomKey = gasUomKey;
                                bill.TotalServiceUse = bill.TotalServiceUse * (1.0m / gasConversionFactor);
                            }
                            break;

                        case "water":
                            if (gasUom != UnitOfMeasureType.Unspecified)
                            {
                                bill.UomKey = waterUomKey;
                                bill.TotalServiceUse = bill.TotalServiceUse * (1.0m / waterConversionFactor);
                            }
                            break;

                    }

                }
            }

        }


        private void UpdateEndUses(EnergyModelResult em)
        {

            // new feature to use all primary commodities here; electric, water, gas
            var allCommodities = GetAllCommodities();

            // appliances may have updated quanties; need to update the output end uses of EnergyModelResult
            foreach (var commodity in allCommodities)
            {
                // first zero the energy uses as long as the commodity is valid
                foreach (var endUse in em.EndUses)
                {
                    foreach (var energyUse in endUse.EnergyUses)
                    {
                        if (energyUse.Commodity == commodity)
                        {
                            energyUse.OriginalQuantity = 0m;
                            energyUse.Quantity = 0m;
                            energyUse.Cost = 0m;
                        }
                    }
                }

                //now update the end uses quantities using appliances belonging to the end use
                foreach (var endUse in em.EndUses)
                {
                    var energyUseOfEndUse = endUse.EnergyUses.Find(x => x.Commodity == commodity);

                    if (energyUseOfEndUse != null)
                    {
                        foreach (var name in endUse.ApplianceNames)
                        {
                            var a = em.Appliances.Find(p => p.Name == name); //find the appliance from all appliances

                            var energyUseOfAppliance = a?.EnergyUses.Find(x => x.Commodity == commodity);

                            if (energyUseOfAppliance != null)
                            {
                                energyUseOfEndUse.Quantity = energyUseOfEndUse.Quantity + energyUseOfAppliance.Quantity;
                                energyUseOfEndUse.Cost = energyUseOfEndUse.Cost + energyUseOfAppliance.Cost;
                                energyUseOfEndUse.RecRatio = energyUseOfAppliance.RecRatio;
                                energyUseOfEndUse.OriginalQuantity = energyUseOfEndUse.OriginalQuantity +
                                                                     energyUseOfAppliance.OriginalQuantity;
                            }
                        }

                    }

                }
            }
        }

        private void ReconcileModeledUsageAndCost(List<Bill> computedBills, List<Bill> actualBills,
            List<CommodityType> commodities, EnergyModelResult em, List<DisaggStatus> disaggStatuses)
        {
            decimal avgCost;

            // iterate valid commodities
            foreach (var commodity in commodities)
            {
                var defaultAvgCost = 0.0m;
                switch (commodity)
                {
                    case CommodityType.Electric:
                        defaultAvgCost = _config.GetCommonFactor<decimal>("defaultelectricrate.kwh");
                        break;
                    case CommodityType.Gas:
                        defaultAvgCost = _config.GetCommonFactor<decimal>("defaultgasrate.ccf");
                        break;
                    case CommodityType.Water:
                        defaultAvgCost = _config.GetCommonFactor<decimal>("defaultwaterrate.gal");
                        break;
                }

                var totalModelUsage = em.Appliances.GetCommoditySum(commodity);

                var cbills = computedBills.FindAll(p => p.CommodityKey == MapToCommodityName(commodity)).ToList();

                //get avg cost per unit for use below
                var abills = actualBills.FindAll(p => p.CommodityKey == MapToCommodityName(commodity)).ToList();
                var actualBillsTotalCost = abills.Sum(p => p.TotalServiceCost);
                var actualBillsTotalUsage = abills.Sum(p => p.TotalServiceUse);
                if (actualBillsTotalUsage <= 0) actualBillsTotalUsage = 1;

                avgCost = actualBillsTotalCost / actualBillsTotalUsage;

                // if avg cost is twice as much as the default rate, then use the default rate for avg cost
                if ((defaultAvgCost * 2) < avgCost)
                {
                    avgCost = defaultAvgCost;
                    disaggStatuses.Find(s => s.Commodity == commodity).Status = Enums.DisaggStatusType.ModelOnly;
                }

                // usage
                var totalBilledUsage = cbills.Sum(c => c.TotalServiceUse);
                if (totalBilledUsage == 0) totalBilledUsage = totalModelUsage;

                //Reconciliation ratio for the commodity
                decimal reconciliationRatio = totalModelUsage / totalBilledUsage;
                if (reconciliationRatio == 0.0m) reconciliationRatio = 1.0m;

                //iterate through all appliances and reconcile the usage using the ratio of model usage to computed bill usage
                foreach (var a in em.Appliances)
                {
                    foreach (var energyUse in a.EnergyUses)
                    {
                        if (energyUse.Commodity == commodity)
                        {
                            // RECONCILED APPLIANCE USAGE; adjust the modeled usage 
                            energyUse.Quantity = energyUse.Quantity * (1.0m / reconciliationRatio);
                            energyUse.RecRatio = reconciliationRatio;

                            // RECONCILED APPLIANCE COST
                            energyUse.Cost = energyUse.Quantity * avgCost;
                            energyUse.UnitCost = avgCost;
                        }
                    }
                }
            }

            // Iterate commodities that we do not have any bills for and assign costs using a default rate
            var commoditiesWithoutBills = GetCommoditiesWithoutBills(commodities);

            foreach (var commodity in commoditiesWithoutBills)
            {

                // set up disagg status as model only for all commodities without bill
                disaggStatuses.Add(new DisaggStatus {Commodity = commodity, Status = Enums.DisaggStatusType.ModelOnly});

                foreach (var a in em.Appliances)
                {
                    foreach (var energyUse in a.EnergyUses)
                    {
                        if (energyUse.Commodity == commodity)
                        {
                            energyUse.RecRatio = 1.0m;

                            // calculate an appliance cost using a default rate for the commodity; note that the UOM may need to be variable in the gas and water cases
                            avgCost = 0.0m;
                            switch (commodity)
                            {
                                case CommodityType.Electric:
                                    avgCost = _config.GetCommonFactor<decimal>("defaultelectricrate.kwh");
                                    break;
                                case CommodityType.Gas:
                                    avgCost = _config.GetCommonFactor<decimal>("defaultgasrate.ccf");
                                    break;
                                case CommodityType.Water:
                                    avgCost = _config.GetCommonFactor<decimal>("defaultwaterrate.gal");
                                    break;
                            }

                            // RECONCILED APPLIANCE COST
                            energyUse.Cost = energyUse.Quantity * avgCost;
                            energyUse.UnitCost = avgCost;
                        }
                    }
                }

            }




        }

        private void ResetModeledCost(EnergyModelResult em)
        {
            //foreach (var commodity in commodities)
            //{
                
                foreach (var a in em.Appliances)
                {
                    foreach (var energyUse in a.EnergyUses)
                    {
                        //if (energyUse.Commodity == commodity)
                        //{
                            energyUse.RecRatio = 1.0m;

                            // calculate an appliance cost using a default rate for the commodity; note that the UOM may need to be variable in the gas and water cases
                            var avgCost = 0.0m;
                            switch (energyUse.Commodity)
                            {
                                case CommodityType.Electric:
                                    avgCost = _config.GetCommonFactor<decimal>("defaultelectricrate.kwh");
                                    break;
                                case CommodityType.Gas:
                                    avgCost = _config.GetCommonFactor<decimal>("defaultgasrate.ccf");
                                    break;
                                case CommodityType.Water:
                                    avgCost = _config.GetCommonFactor<decimal>("defaultwaterrate.gal");
                                    break;
                            }

                            // RECONCILED APPLIANCE COST
                            energyUse.Cost = energyUse.Quantity * avgCost;
                            energyUse.UnitCost = avgCost;
                        //}
                    }
                }

            //}
        }

        private bool CheckResultsForValidity(List<Bill> computedBills, List<CommodityType> commodities,
            EnergyModelResult em)
        {
            var valid = true;

            // check commodities for specific minimums in computedBills, if any single commodity is too low, the results are *invalid overall*
            foreach (var commodity in commodities)
            {
                var cbills = computedBills.FindAll(p => p.CommodityKey == MapToCommodityName(commodity)).ToList();

                var totalBilledUsage = cbills.Sum(c => c.TotalServiceUse);

                decimal minUsage = 0.0m;
                switch (commodity)
                {
                    case CommodityType.Electric:
                        minUsage = _config.GetCommonFactor<decimal>("computedbills.minusageelectric");
                        break;
                    case CommodityType.Gas:
                        minUsage = _config.GetCommonFactor<decimal>("computedbills.minusagegas");
                        break;
                    case CommodityType.Water:
                        minUsage = _config.GetCommonFactor<decimal>("computedbills.minusagewater");
                        break;
                }

                if (totalBilledUsage < minUsage)
                {
                    valid = false;
                    break;
                }
            }


            //Any negative costs or usages in bills shall invalidate results.
            if (computedBills.Any(b => b.TotalServiceUse < 0m || b.TotalServiceCost < 0m))
            {
                valid = false;
            }

            if (em.Appliances.DoesNegativeEnergyUseExist())
            {
                valid = false;
            }

            return (valid);
        }


        /// <summary>
        /// Create a flattened data table representation of appliance billdisagg data for this result.
        /// </summary>
        /// <param name="result"></param>
        /// <param name="disaggInputParams"></param>
        private void PopulateFlatTable(BillDisaggResult result, DisaggInputParams disaggInputParams)
        {
            var df = new DataFactory();
            var table = df.CreateBillDisaggDataTable();

                //iterate end uses and add appliances that are listed in the end use; each energy use in the appliance
                foreach (var eu in result.EndUses)
                {
                    foreach (var name in eu.ApplianceNames)
                    {
                        var a = result.Appliances.Find(p => p.Name == name);
                        if (a == null)
                        {
                            continue;
                        }

                        foreach (var eng in a.EnergyUses)
                        {
                            var disaggStatus = result.DisaggStatuses.Find(s => s.Commodity == eng.Commodity).Status;

                            table.Rows.Add(_clientId,
                                disaggInputParams.CustomerId,
                                disaggInputParams.AccountId,
                                disaggInputParams.PremiseId,
                                result.EndDate,
                                eu.Key,
                                a.Key,
                                eng.CommodityKey,
                                (int) result.DisaggPeriod,
                                eng.Quantity,
                                eng.Cost,
                                eng.UomKey,
                                eng.CurrencyKey,
                                a.Confidence,
                                eng.RecRatio,
                                (int) disaggStatus,
                                DateTime.UtcNow);
                        }
                    }
                }

            result.FlatResultTable = table;

        }



        /// <summary>
        /// Create a NEW bill disagg result by partial processing of a new energy model result and a previous bill disagg result.  Used by measures!
        /// </summary>
        /// <param name="bd"></param>
        /// <param name="em"></param>
        /// <returns></returns>
        public BillDisaggResult GenerateBillDisaggFromPreviousResults(BillDisaggResult bd, EnergyModelResult em)
        {
            List<Bill> computedBills = null;
            List<DisaggStatus> disaggStatuses;

            // remove non bill disagg enduses and cleanup addons
            var billdisaggCategory = "billdisagg";
            if (em.ModelType == ModelType.Business) billdisaggCategory = "business";

            em.EndUses.RemoveAll(a => a.Category.ToLower() != billdisaggCategory);
            em.EndUses.RemoveAddOnEnergyUses();
            em.Appliances.RemoveAddOnEnergyUses();
            em.Appliances.RemoveEmpty();
            em.Appliances.SetOriginalQuantities();    //This is in case we need to revert to model usages because of bill disagg failure
            em.TotalEnergyUses.Clear();               //Totals are no longer valid so clear this collection

            var actualBills = bd.ActualBills;

            if (actualBills != null && actualBills.Count > 0)
            {
                // check actual bills usage units of measure and convert into Energy Model required units of measure; model requires kwh, therms, & gallons
                var conversionInfo = ModifyActualBillUnitOfMeasure(actualBills);

                //valid commodities are those with two or more bills
                var commodities = GetValidCommodities(actualBills);
                if (em.ModelType == ModelType.Business) commodities.Remove(CommodityType.Water);  //business does not model water currently

                disaggStatuses = new List<DisaggStatus>();
                commodities.ForEach(c => disaggStatuses.Add(new DisaggStatus { Commodity = c, Status = Enums.DisaggStatusType.ModelOnly }));

                if (bd.ComputedBills != null) computedBills = bd.ComputedBills;

                //one or more commodities have valid positive usage computed bills for disaggregation logic
                if (computedBills != null && computedBills.Count > 0)
                {
                    //bd.DisaggStatuses.ForEach(s => disaggStatuses.Find(ds => ds.Commodity == s.Commodity).Status = s.Status);
                    foreach (var s in bd.DisaggStatuses)
                    {
                        foreach (var ds in disaggStatuses)
                        {
                            if (ds.Commodity == s.Commodity)
                            {
                                ds.Status = s.Status;
                            }
                        }
                    }

                    //Reconcile usage and cost from previous bill disagg results per appliance (includes weather factor, recon ratio, and unit cost)
                    ReconcileResults(em, bd);

                    //Check appliance results for anomolies and reset to model if disagg results are bad
                    var resultsValid = CheckResultsForValidity(computedBills, commodities, em);
                    if (!resultsValid)
                    {
                        em.Appliances.ResetQuantities();

                        ResetModeledCost(em);

                        commodities.ForEach(c => disaggStatuses.Find(s => s.Commodity == c).Status = Enums.DisaggStatusType.ModelOnly);
                    }

                    //Recalculate EndUses after appliance adjustments
                    UpdateEndUses(em);
                }

                // all results are in the model units of measure.  Iterate the Energy Model results and convert back to the actual bills unit of measure, 
                // then the output data will be in the units of measure specified in bills.
                if (conversionInfo != null && conversionInfo.Count > 0)
                {
                    ConvertBackToUnitsOfMeasure(conversionInfo, em, actualBills);
                }
            }
            else            // no bill
            {
                // get all the commdities for no bill
                if (bd.ComputedBills != null) computedBills = bd.ComputedBills;
                ReconcileResults(em, bd);
                disaggStatuses = bd.DisaggStatuses;

            }

            //Assign new result to new bill disagg output
            var result = new BillDisaggResult()
            {
                CustomerId = bd.CustomerId,
                AccountId = bd.AccountId,
                PremiseId = bd.PremiseId,
                DisaggStatuses = disaggStatuses,
                EndUses = em.EndUses,
                Appliances = em.Appliances,
                ActualBills = actualBills,
                ComputedBills = computedBills,
                EndDate = bd.EndDate,
                StartDate = bd.StartDate
            };

            return (result);
        }


        /// <summary>
        /// Reconcile results for special case where we have energy model and previous bill disagg result.
        /// </summary>
        /// <param name="em"></param>
        /// <param name="bd"></param>
        private void ReconcileResults(EnergyModelResult em, BillDisaggResult bd)
        {
            // new feature to use all commodities here
            var allCommodities = GetAllCommodities();

            // iterate valid commodities
            foreach (var commodity in allCommodities)
            {
                //iterate through all appliances and reconcile the usage and cost using the previous bill disagg result
                foreach (var a in em.Appliances)
                {
                    var bdAppliance = bd.Appliances.Find(p => p.Name.ToLower() == a.Name.ToLower());

                    if (bdAppliance != null)
                    {
                        foreach (var energyUse in a.EnergyUses)
                        {
                            if (energyUse.Commodity == commodity)
                            {
                                var bdEnergyUse = bdAppliance.EnergyUses.Find(c => c.Commodity == commodity);

                                if (bdEnergyUse != null)
                                {
                                    // RECONCILED APPLIANCE USAGE and COST from previous execution results 
                                    var weatherAdjustmentFactor = bdEnergyUse.WeatherAdjustmentFactor;
                                    var reconciliationRatio = bdEnergyUse.RecRatio;
                                    var avgCost = bdEnergyUse.UnitCost;

                                    energyUse.Quantity = energyUse.Quantity * weatherAdjustmentFactor * (1.0m / reconciliationRatio);
                                    energyUse.RecRatio = reconciliationRatio;

                                    energyUse.Cost = energyUse.Quantity * avgCost;
                                    energyUse.UnitCost = avgCost;
                                }

                            }
                        }

                    }

                }

            }

        }



        /// <summary>
        /// Data Access init.
        /// </summary>
        private void InitDataAccess()
        {
                _dataAccessLayer = new DataAccessLayer();
        }


        /// <summary>
        /// Data Access Init with connection string overrides.
        /// </summary>
        /// <param name="csInsightsMetadata"></param>
        /// <param name="csInsightsDw"></param>
        /// <param name="csInsights"></param>
        private void InitDataAccess(string csInsightsMetadata, string csInsightsDw, string csInsights)
        {
                _dataAccessLayer = new DataAccessLayer(csInsightsMetadata, csInsightsDw, csInsights);
        }



        /// <summary>
        /// Initialize content provider to be used to populate common factors.
        /// </summary>
        private void InitContentProvider()
        {
            // init the content provider
            var factory = new ContentModelFactoryContentful();
            _contentProvider = factory.CreateContentProvider(_clientId);
        }


        /// <summary>
        /// Initialize content provider to be used to populate common factors.  Supported override of connection string.
        /// </summary>
        private void InitContentProvider(string csInsightsMetadata)
        {
            // init the content provider
            var factory = new ContentModelFactoryContentful();
            _contentProvider = factory.CreateContentProvider(_clientId, ContentProviderType.Contentful,
                csInsightsMetadata);
        }


        private void LoadConfiguration()
        {
            if (_config == null)
            {
                var cacheKey = "disaggConfig";

                var cacheItem = EnergyModel.Cache.CacheLayer.Get<BillDisaggConfiguration>(cacheKey);

                if (cacheItem == null)
                {
                    _config = new BillDisaggConfiguration();

                    // COMMON FACTORS
                    // get the configuration information for common and billdisagg, AND only get conversion, computedbill, and default rate values
                    var commonFactorList = _contentProvider.GetContentConfiguration("common,billdisagg", "conversion.ccftogal,conversion.ccftotherm,conversion.mcftotherm,defaultelectricrate.kwh,defaultgasrate.ccf,defaultgasrate.therms,defaultwaterrate.gal,defaultwaterrate.ccf,computedbills.minusageelectric,computedbills.minusagegas,computedbills.minusagewater");

                    foreach (var item in commonFactorList)
                    {
                        _config.CommonFactors.Add(item.Key, item.Value);
                    }

                    // APPLIANCES - MONTHLY LOAD FACTORS - STATIC FROM ORIGINAL DISAGG LOGIC
                    // add the 'known' ApplianceMonthlyLoadFactors
                    _config.ApplianceMonthlyLoadFactors.Add("aquarium", new [] { 1m, 1m, 1m, 1m, 1m, 1m, 1m, 1m, 1m, 1m, 1m, 1m });
                    _config.ApplianceMonthlyLoadFactors.Add("airpurifier", new [] { 1m, 1m, 1m, 1m, 1m, 1m, 1m, 1m, 1m, 1m, 1m, 1m });
                    _config.ApplianceMonthlyLoadFactors.Add("atticfan", new [] { 0.009108653m, 0.02081978m, 0.02342225m, 0.05009759m, 0.06310996m, 0.1145088m, 0.1906311m, 0.2055953m, 0.1763175m, 0.1054001m, 0.02927781m, 0.01171113m });
                    _config.ApplianceMonthlyLoadFactors.Add("blockheater", new [] { 1m, 1m, 1m, 1m, 1m, 1m, 1m, 1m, 1m, 1m, 1m, 1m });
                    _config.ApplianceMonthlyLoadFactors.Add("ceilingfan", new [] { 0.009108653m, 0.02081978m, 0.02342225m, 0.05009759m, 0.06310996m, 0.1145088m, 0.1906311m, 0.2055953m, 0.1763175m, 0.1054001m, 0.02927781m, 0.01171113m });
                    _config.ApplianceMonthlyLoadFactors.Add("centralac", new [] { 0m, 0m, 0.000463899m, 0.03971553m, 0.0828571m, 0.1535674m, 0.2623256m, 0.2535548m, 0.1656455m, 0.04187025m, 0m, 0m });
                    _config.ApplianceMonthlyLoadFactors.Add("clotheswasher", new [] { 32m, 32m, 32m, 31m, 31m, 31m, 30m, 30m, 30m, 30m, 30m, 30m });
                    _config.ApplianceMonthlyLoadFactors.Add("coffeemaker", new [] { 1m, 1m, 1m, 1m, 1m, 1m, 1m, 1m, 1m, 1m, 1m, 1m });
                    _config.ApplianceMonthlyLoadFactors.Add("compactor", new [] { 1m, 1m, 1m, 1m, 1m, 1m, 1m, 1m, 1m, 1m, 1m, 1m });
                    _config.ApplianceMonthlyLoadFactors.Add("computer", new [] { 1m, 1m, 1m, 1m, 1m, 1m, 1m, 1m, 1m, 1m, 1m, 1m });
                    _config.ApplianceMonthlyLoadFactors.Add("cooktop", new [] { 23m, 23m, 23m, 22m, 21m, 20m, 20m, 20m, 20m, 21m, 22m, 23m });
                    _config.ApplianceMonthlyLoadFactors.Add("copier", new [] { 1m, 1m, 1m, 1m, 1m, 1m, 1m, 1m, 1m, 1m, 1m, 1m });
                    _config.ApplianceMonthlyLoadFactors.Add("dehumidifier", new [] { 0.009108653m, 0.02081978m, 0.02342225m, 0.05009759m, 0.06310996m, 0.1145088m, 0.1906311m, 0.2055953m, 0.1763175m, 0.1054001m, 0.02927781m, 0.01171113m });
                    _config.ApplianceMonthlyLoadFactors.Add("dishwasher", new [] { 32m, 32m, 32m, 31m, 31m, 31m, 30m, 30m, 30m, 30m, 30m, 30m });
                    _config.ApplianceMonthlyLoadFactors.Add("disposal", new [] { 1m, 1m, 1m, 1m, 1m, 1m, 1m, 1m, 1m, 1m, 1m, 1m });
                    _config.ApplianceMonthlyLoadFactors.Add("dryer", new [] { 32m, 32m, 32m, 31m, 31m, 31m, 30m, 30m, 30m, 30m, 30m, 30m });
                    _config.ApplianceMonthlyLoadFactors.Add("electricblanket", new [] { 1m, 1m, 1m, 1m, 1m, 1m, 1m, 1m, 1m, 1m, 1m, 1m });
                    _config.ApplianceMonthlyLoadFactors.Add("fax", new [] { 1m, 1m, 1m, 1m, 1m, 1m, 1m, 1m, 1m, 1m, 1m, 1m });
                    _config.ApplianceMonthlyLoadFactors.Add("fireplace", new [] { 0.1652994m, 0.1275636m, 0.1246924m, 0.09721083m, 0.07301067m, 0.04183757m, 0.01722724m, 0.0147662m, 0.02912223m, 0.0442986m, 0.1033634m, 0.1616079m });
                    _config.ApplianceMonthlyLoadFactors.Add("freezer", new [] { 22m, 23m, 24m, 25m, 26m, 27m, 28m, 27m, 26m, 25m, 24m, 23m });
                    _config.ApplianceMonthlyLoadFactors.Add("freezer2", new [] { 22m, 23m, 24m, 25m, 26m, 27m, 28m, 27m, 26m, 25m, 24m, 23m });
                    _config.ApplianceMonthlyLoadFactors.Add("growlight", new [] { 1m, 1m, 1m, 1m, 1m, 1m, 1m, 1m, 1m, 1m, 1m, 1m });
                    _config.ApplianceMonthlyLoadFactors.Add("gutterheater", new [] { 1m, 1m, 1m, 1m, 1m, 1m, 1m, 1m, 1m, 1m, 1m, 1m });
                    _config.ApplianceMonthlyLoadFactors.Add("heatsystem", new [] { 0.2383304m, 0.1700638m, 0.1413937m, 0.05652884m, 0.01017542m, 0m, 0m, 0m, 0m, 0.03126654m, 0.1216386m, 0.2306027m });
                    _config.ApplianceMonthlyLoadFactors.Add("hottub", new [] { 1m, 1m, 1m, 1m, 1m, 1m, 1m, 1m, 1m, 1m, 1m, 1m });
                    _config.ApplianceMonthlyLoadFactors.Add("hotwaterdispenser", new [] { 1m, 1m, 1m, 1m, 1m, 1m, 1m, 1m, 1m, 1m, 1m, 1m });
                    _config.ApplianceMonthlyLoadFactors.Add("housefan", new [] { 0.009108653m, 0.02081978m, 0.02342225m, 0.05009759m, 0.06310996m, 0.1145088m, 0.1906311m, 0.2055953m, 0.1763175m, 0.1054001m, 0.02927781m, 0.01171113m });
                    _config.ApplianceMonthlyLoadFactors.Add("humidifier", new [] { 0.1652994m, 0.1275636m, 0.1246924m, 0.09721083m, 0.07301067m, 0.04183757m, 0.01722724m, 0.0147662m, 0.02912223m, 0.0442986m, 0.1033634m, 0.1616079m });
                    _config.ApplianceMonthlyLoadFactors.Add("kiln", new [] { 1m, 1m, 1m, 1m, 1m, 1m, 1m, 1m, 1m, 1m, 1m, 1m });
                    _config.ApplianceMonthlyLoadFactors.Add("lighting", new [] { 10m, 10m, 10m, 9m, 9m, 9m, 8m, 8m, 8m, 9m, 9m, 9m });
                    _config.ApplianceMonthlyLoadFactors.Add("microwave", new [] { 23m, 23m, 23m, 22m, 21m, 20m, 20m, 20m, 20m, 21m, 22m, 23m });
                    _config.ApplianceMonthlyLoadFactors.Add("miscellaneous", new [] { 1m, 1m, 1m, 1m, 1m, 1m, 1m, 1m, 1m, 1m, 1m, 1m });
                    _config.ApplianceMonthlyLoadFactors.Add("o2concentrator", new [] { 1m, 1m, 1m, 1m, 1m, 1m, 1m, 1m, 1m, 1m, 1m, 1m });
                    _config.ApplianceMonthlyLoadFactors.Add("outsidewater", new [] { 1m, 1m, 1m, 1m, 1m, 1m, 1m, 1m, 1m, 1m, 1m, 1m });
                    _config.ApplianceMonthlyLoadFactors.Add("oven", new [] { 23m, 23m, 23m, 22m, 21m, 20m, 20m, 20m, 20m, 21m, 22m, 23m });
                    _config.ApplianceMonthlyLoadFactors.Add("pool", new [] { 0.13m, 0.13m, 0.1m, 0.08m, 0.07m, 0.05m, 0.04m, 0.04m, 0.05m, 0.08m, 0.1m, 0.13m });
                    _config.ApplianceMonthlyLoadFactors.Add("refrigerator", new [] { 22m, 23m, 24m, 25m, 26m, 27m, 28m, 27m, 26m, 25m, 24m, 23m });
                    _config.ApplianceMonthlyLoadFactors.Add("refrigerator2", new [] { 22m, 23m, 24m, 25m, 26m, 27m, 28m, 27m, 26m, 25m, 24m, 23m });
                    _config.ApplianceMonthlyLoadFactors.Add("roomac", new [] { 0.009108653m, 0.02081978m, 0.02342225m, 0.05009759m, 0.06310996m, 0.1145088m, 0.1906311m, 0.2055953m, 0.1763175m, 0.1054001m, 0.02927781m, 0.01171113m });
                    _config.ApplianceMonthlyLoadFactors.Add("roomac2", new [] { 0.009108653m, 0.02081978m, 0.02342225m, 0.05009759m, 0.06310996m, 0.1145088m, 0.1906311m, 0.2055953m, 0.1763175m, 0.1054001m, 0.02927781m, 0.01171113m });
                    _config.ApplianceMonthlyLoadFactors.Add("roomac3", new [] { 0.009108653m, 0.02081978m, 0.02342225m, 0.05009759m, 0.06310996m, 0.1145088m, 0.1906311m, 0.2055953m, 0.1763175m, 0.1054001m, 0.02927781m, 0.01171113m });
                    _config.ApplianceMonthlyLoadFactors.Add("sauna", new [] { 1m, 1m, 1m, 1m, 1m, 1m, 1m, 1m, 1m, 1m, 1m, 1m });
                    _config.ApplianceMonthlyLoadFactors.Add("secondaryheating", new [] { 0.2383304m, 0.1700638m, 0.1413937m, 0.05652884m, 0.01017542m, 0m, 0m, 0m, 0m, 0.03126654m, 0.1216386m, 0.2306027m });
                    _config.ApplianceMonthlyLoadFactors.Add("shower", new [] { 32m, 32m, 32m, 31m, 31m, 31m, 30m, 30m, 30m, 30m, 30m, 30m });
                    _config.ApplianceMonthlyLoadFactors.Add("sink", new [] { 32m, 32m, 32m, 31m, 31m, 31m, 30m, 30m, 30m, 30m, 30m, 30m });
                    _config.ApplianceMonthlyLoadFactors.Add("spaceheater", new [] { 0.1652994m, 0.1275636m, 0.1246924m, 0.09721083m, 0.07301067m, 0.04183757m, 0.01722724m, 0.0147662m, 0.02912223m, 0.0442986m, 0.1033634m, 0.1616079m });
                    _config.ApplianceMonthlyLoadFactors.Add("steamroom", new [] { 1m, 1m, 1m, 1m, 1m, 1m, 1m, 1m, 1m, 1m, 1m, 1m });
                    _config.ApplianceMonthlyLoadFactors.Add("sumppump", new [] { 10m, 10m, 10m, 11m, 11m, 11m, 10m, 10m, 10m, 10m, 10m, 10m });
                    _config.ApplianceMonthlyLoadFactors.Add("toilet", new [] { 32m, 32m, 32m, 31m, 31m, 31m, 30m, 30m, 30m, 30m, 30m, 30m });
                    _config.ApplianceMonthlyLoadFactors.Add("towelbar", new [] { 1m, 1m, 1m, 1m, 1m, 1m, 1m, 1m, 1m, 1m, 1m, 1m });
                    _config.ApplianceMonthlyLoadFactors.Add("tv", new [] { 1m, 1m, 1m, 1m, 1m, 1m, 1m, 1m, 1m, 1m, 1m, 1m });
                    _config.ApplianceMonthlyLoadFactors.Add("tvflat", new [] { 1m, 1m, 1m, 1m, 1m, 1m, 1m, 1m, 1m, 1m, 1m, 1m });
                    _config.ApplianceMonthlyLoadFactors.Add("tvrear", new [] { 1m, 1m, 1m, 1m, 1m, 1m, 1m, 1m, 1m, 1m, 1m, 1m });
                    _config.ApplianceMonthlyLoadFactors.Add("vehicle", new [] { 1m, 1m, 1m, 1m, 1m, 1m, 1m, 1m, 1m, 1m, 1m, 1m });
                    _config.ApplianceMonthlyLoadFactors.Add("vehicle2", new [] { 1m, 1m, 1m, 1m, 1m, 1m, 1m, 1m, 1m, 1m, 1m, 1m });
                    _config.ApplianceMonthlyLoadFactors.Add("waterbed", new [] { 11m, 10m, 10m, 9m, 9m, 8m, 8m, 8m, 9m, 9m, 10m, 10m });
                    _config.ApplianceMonthlyLoadFactors.Add("waterheater", new [] { 1m, 1m, 1m, 1m, 1m, 1m, 1m, 1m, 1m, 1m, 1m, 1m });
                    _config.ApplianceMonthlyLoadFactors.Add("wellpump", new [] { 1m, 1m, 1m, 1m, 1m, 1m, 1m, 1m, 1m, 1m, 1m, 1m });
                    _config.ApplianceMonthlyLoadFactors.Add("waterdispenser", new [] { 1m, 1m, 1m, 1m, 1m, 1m, 1m, 1m, 1m, 1m, 1m, 1m });
                    _config.ApplianceMonthlyLoadFactors.Add("winechiller", new [] { 1m, 1m, 1m, 1m, 1m, 1m, 1m, 1m, 1m, 1m, 1m, 1m });
                    _config.ApplianceMonthlyLoadFactors.Add("woodstove", new [] { 19m, 16m, 14m, 9m, 4m, 1m, 0m, 0m, 2m, 7m, 11m, 17m });

                    _config.ApplianceMonthlyLoadFactors.Add("businesscomputers", new [] { 1m, 1m, 1m, 1m, 1m, 1m, 1m, 1m, 1m, 1m, 1m, 1m });
                    _config.ApplianceMonthlyLoadFactors.Add("businesscooling", new [] { 1m, 1m, 1m, 1m, 1m, 1m, 1m, 1m, 1m, 1m, 1m, 1m });
                    _config.ApplianceMonthlyLoadFactors.Add("businessheating", new [] { 1m, 1m, 1m, 1m, 1m, 1m, 1m, 1m, 1m, 1m, 1m, 1m });
                    _config.ApplianceMonthlyLoadFactors.Add("businesswaterheating", new [] { 1m, 1m, 1m, 1m, 1m, 1m, 1m, 1m, 1m, 1m, 1m, 1m });
                    _config.ApplianceMonthlyLoadFactors.Add("businesscooking", new [] { 1m, 1m, 1m, 1m, 1m, 1m, 1m, 1m, 1m, 1m, 1m, 1m });
                    _config.ApplianceMonthlyLoadFactors.Add("businesslaundry", new [] { 1m, 1m, 1m, 1m, 1m, 1m, 1m, 1m, 1m, 1m, 1m, 1m });
                    _config.ApplianceMonthlyLoadFactors.Add("businessrefrigeration", new [] { 1m, 1m, 1m, 1m, 1m, 1m, 1m, 1m, 1m, 1m, 1m, 1m });
                    _config.ApplianceMonthlyLoadFactors.Add("businesslighting", new [] { 1m, 1m, 1m, 1m, 1m, 1m, 1m, 1m, 1m, 1m, 1m, 1m });
                    _config.ApplianceMonthlyLoadFactors.Add("businessmisc", new [] { 1m, 1m, 1m, 1m, 1m, 1m, 1m, 1m, 1m, 1m, 1m, 1m });
                    _config.ApplianceMonthlyLoadFactors.Add("businessofficeequipment", new [] { 1m, 1m, 1m, 1m, 1m, 1m, 1m, 1m, 1m, 1m, 1m, 1m });
                    _config.ApplianceMonthlyLoadFactors.Add("businessventilation", new [] { 1m, 1m, 1m, 1m, 1m, 1m, 1m, 1m, 1m, 1m, 1m, 1m });

                    // APPLIANCES - ANNUAL LOAD FACTORS as SUM of Monthly from above
                    _config.ApplianceAnnualLoadFactors.Add("aquarium", _config.ApplianceMonthlyLoadFactors["aquarium"].Sum());
                    _config.ApplianceAnnualLoadFactors.Add("airpurifier", _config.ApplianceMonthlyLoadFactors["airpurifier"].Sum());
                    _config.ApplianceAnnualLoadFactors.Add("atticfan", _config.ApplianceMonthlyLoadFactors["atticfan"].Sum());
                    _config.ApplianceAnnualLoadFactors.Add("blockheater", _config.ApplianceMonthlyLoadFactors["blockheater"].Sum());
                    _config.ApplianceAnnualLoadFactors.Add("ceilingfan", _config.ApplianceMonthlyLoadFactors["ceilingfan"].Sum());
                    _config.ApplianceAnnualLoadFactors.Add("centralac", _config.ApplianceMonthlyLoadFactors["centralac"].Sum());
                    _config.ApplianceAnnualLoadFactors.Add("clotheswasher", _config.ApplianceMonthlyLoadFactors["clotheswasher"].Sum());
                    _config.ApplianceAnnualLoadFactors.Add("coffeemaker", _config.ApplianceMonthlyLoadFactors["coffeemaker"].Sum());
                    _config.ApplianceAnnualLoadFactors.Add("compactor", _config.ApplianceMonthlyLoadFactors["compactor"].Sum());
                    _config.ApplianceAnnualLoadFactors.Add("computer", _config.ApplianceMonthlyLoadFactors["computer"].Sum());
                    _config.ApplianceAnnualLoadFactors.Add("cooktop", _config.ApplianceMonthlyLoadFactors["cooktop"].Sum());
                    _config.ApplianceAnnualLoadFactors.Add("copier", _config.ApplianceMonthlyLoadFactors["copier"].Sum());
                    _config.ApplianceAnnualLoadFactors.Add("dehumidifier", _config.ApplianceMonthlyLoadFactors["dehumidifier"].Sum());
                    _config.ApplianceAnnualLoadFactors.Add("dishwasher", _config.ApplianceMonthlyLoadFactors["dishwasher"].Sum());
                    _config.ApplianceAnnualLoadFactors.Add("disposal", _config.ApplianceMonthlyLoadFactors["disposal"].Sum());
                    _config.ApplianceAnnualLoadFactors.Add("dryer", _config.ApplianceMonthlyLoadFactors["dryer"].Sum());
                    _config.ApplianceAnnualLoadFactors.Add("electricblanket", _config.ApplianceMonthlyLoadFactors["electricblanket"].Sum());
                    _config.ApplianceAnnualLoadFactors.Add("fax", _config.ApplianceMonthlyLoadFactors["fax"].Sum());
                    _config.ApplianceAnnualLoadFactors.Add("fireplace", _config.ApplianceMonthlyLoadFactors["fireplace"].Sum());
                    _config.ApplianceAnnualLoadFactors.Add("freezer", _config.ApplianceMonthlyLoadFactors["freezer"].Sum());
                    _config.ApplianceAnnualLoadFactors.Add("freezer2", _config.ApplianceMonthlyLoadFactors["freezer2"].Sum());
                    _config.ApplianceAnnualLoadFactors.Add("growlight", _config.ApplianceMonthlyLoadFactors["growlight"].Sum());
                    _config.ApplianceAnnualLoadFactors.Add("gutterheater", _config.ApplianceMonthlyLoadFactors["gutterheater"].Sum());
                    _config.ApplianceAnnualLoadFactors.Add("heatsystem", _config.ApplianceMonthlyLoadFactors["heatsystem"].Sum());
                    _config.ApplianceAnnualLoadFactors.Add("hottub", _config.ApplianceMonthlyLoadFactors["hottub"].Sum());
                    _config.ApplianceAnnualLoadFactors.Add("hotwaterdispenser", _config.ApplianceMonthlyLoadFactors["hotwaterdispenser"].Sum());
                    _config.ApplianceAnnualLoadFactors.Add("housefan", _config.ApplianceMonthlyLoadFactors["housefan"].Sum());
                    _config.ApplianceAnnualLoadFactors.Add("humidifier", _config.ApplianceMonthlyLoadFactors["humidifier"].Sum());
                    _config.ApplianceAnnualLoadFactors.Add("kiln", _config.ApplianceMonthlyLoadFactors["kiln"].Sum());
                    _config.ApplianceAnnualLoadFactors.Add("lighting", _config.ApplianceMonthlyLoadFactors["lighting"].Sum());
                    _config.ApplianceAnnualLoadFactors.Add("microwave", _config.ApplianceMonthlyLoadFactors["microwave"].Sum());
                    _config.ApplianceAnnualLoadFactors.Add("miscellaneous", _config.ApplianceMonthlyLoadFactors["miscellaneous"].Sum());
                    _config.ApplianceAnnualLoadFactors.Add("o2concentrator", _config.ApplianceMonthlyLoadFactors["o2concentrator"].Sum());
                    _config.ApplianceAnnualLoadFactors.Add("outsidewater", _config.ApplianceMonthlyLoadFactors["outsidewater"].Sum());
                    _config.ApplianceAnnualLoadFactors.Add("oven", _config.ApplianceMonthlyLoadFactors["oven"].Sum());
                    _config.ApplianceAnnualLoadFactors.Add("pool", _config.ApplianceMonthlyLoadFactors["pool"].Sum());
                    _config.ApplianceAnnualLoadFactors.Add("refrigerator", _config.ApplianceMonthlyLoadFactors["refrigerator"].Sum());
                    _config.ApplianceAnnualLoadFactors.Add("refrigerator2", _config.ApplianceMonthlyLoadFactors["refrigerator2"].Sum());
                    _config.ApplianceAnnualLoadFactors.Add("roomac", _config.ApplianceMonthlyLoadFactors["roomac"].Sum());
                    _config.ApplianceAnnualLoadFactors.Add("roomac2", _config.ApplianceMonthlyLoadFactors["roomac2"].Sum());
                    _config.ApplianceAnnualLoadFactors.Add("roomac3", _config.ApplianceMonthlyLoadFactors["roomac3"].Sum());
                    _config.ApplianceAnnualLoadFactors.Add("sauna", _config.ApplianceMonthlyLoadFactors["sauna"].Sum());
                    _config.ApplianceAnnualLoadFactors.Add("secondaryheating", _config.ApplianceMonthlyLoadFactors["secondaryheating"].Sum());
                    _config.ApplianceAnnualLoadFactors.Add("shower", _config.ApplianceMonthlyLoadFactors["shower"].Sum());
                    _config.ApplianceAnnualLoadFactors.Add("sink", _config.ApplianceMonthlyLoadFactors["sink"].Sum());
                    _config.ApplianceAnnualLoadFactors.Add("spaceheater", _config.ApplianceMonthlyLoadFactors["spaceheater"].Sum());
                    _config.ApplianceAnnualLoadFactors.Add("steamroom", _config.ApplianceMonthlyLoadFactors["steamroom"].Sum());
                    _config.ApplianceAnnualLoadFactors.Add("sumppump", _config.ApplianceMonthlyLoadFactors["sumppump"].Sum());
                    _config.ApplianceAnnualLoadFactors.Add("toilet", _config.ApplianceMonthlyLoadFactors["toilet"].Sum());
                    _config.ApplianceAnnualLoadFactors.Add("towelbar", _config.ApplianceMonthlyLoadFactors["towelbar"].Sum());
                    _config.ApplianceAnnualLoadFactors.Add("tv", _config.ApplianceMonthlyLoadFactors["tv"].Sum());
                    _config.ApplianceAnnualLoadFactors.Add("tvflat", _config.ApplianceMonthlyLoadFactors["tvflat"].Sum());
                    _config.ApplianceAnnualLoadFactors.Add("tvrear", _config.ApplianceMonthlyLoadFactors["tvrear"].Sum());
                    _config.ApplianceAnnualLoadFactors.Add("vehicle", _config.ApplianceMonthlyLoadFactors["vehicle"].Sum());
                    _config.ApplianceAnnualLoadFactors.Add("vehicle2", _config.ApplianceMonthlyLoadFactors["vehicle2"].Sum());
                    _config.ApplianceAnnualLoadFactors.Add("waterbed", _config.ApplianceMonthlyLoadFactors["waterbed"].Sum());
                    _config.ApplianceAnnualLoadFactors.Add("waterheater", _config.ApplianceMonthlyLoadFactors["waterheater"].Sum());
                    _config.ApplianceAnnualLoadFactors.Add("wellpump", _config.ApplianceMonthlyLoadFactors["wellpump"].Sum());
                    _config.ApplianceAnnualLoadFactors.Add("waterdispenser", _config.ApplianceMonthlyLoadFactors["waterdispenser"].Sum());
                    _config.ApplianceAnnualLoadFactors.Add("winechiller", _config.ApplianceMonthlyLoadFactors["winechiller"].Sum());
                    _config.ApplianceAnnualLoadFactors.Add("woodstove", _config.ApplianceMonthlyLoadFactors["woodstove"].Sum());

                    _config.ApplianceAnnualLoadFactors.Add("businesscomputers", _config.ApplianceMonthlyLoadFactors["businesscomputers"].Sum());
                    _config.ApplianceAnnualLoadFactors.Add("businesscooling", _config.ApplianceMonthlyLoadFactors["businesscooling"].Sum());
                    _config.ApplianceAnnualLoadFactors.Add("businessheating", _config.ApplianceMonthlyLoadFactors["businessheating"].Sum());
                    _config.ApplianceAnnualLoadFactors.Add("businesswaterheating", _config.ApplianceMonthlyLoadFactors["businesswaterheating"].Sum());
                    _config.ApplianceAnnualLoadFactors.Add("businesscooking", _config.ApplianceMonthlyLoadFactors["businesscooking"].Sum());
                    _config.ApplianceAnnualLoadFactors.Add("businesslaundry", _config.ApplianceMonthlyLoadFactors["businesslaundry"].Sum());
                    _config.ApplianceAnnualLoadFactors.Add("businessrefrigeration", _config.ApplianceMonthlyLoadFactors["businessrefrigeration"].Sum());
                    _config.ApplianceAnnualLoadFactors.Add("businesslighting", _config.ApplianceMonthlyLoadFactors["businesslighting"].Sum());
                    _config.ApplianceAnnualLoadFactors.Add("businessmisc", _config.ApplianceMonthlyLoadFactors["businessmisc"].Sum());
                    _config.ApplianceAnnualLoadFactors.Add("businessofficeequipment", _config.ApplianceMonthlyLoadFactors["businessofficeequipment"].Sum());
                    _config.ApplianceAnnualLoadFactors.Add("businessventilation", _config.ApplianceMonthlyLoadFactors["businessventilation"].Sum());

                    // ** FINALLY add the configuration to the cache **
                    Cache.CacheLayer.Add(_config, cacheKey, _cacheTimeoutInMinutes);

                }
                else
                {
                    _config = cacheItem;
                }

            }
        }

    }
}

