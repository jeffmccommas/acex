﻿using System;
using System.Runtime.Serialization;

namespace CE.BillDisagg.Exceptions
{
    [Serializable]
    public class ClientCommonFactorNotFoundException : Exception
    {
        //public ClientCommonFactorNotFoundException(){ }
        public ClientCommonFactorNotFoundException(string message)
            : base(message)
        { }

        //public ClientCommonFactorNotFoundException(string message, Exception innerException): base(message, innerException){ }

        //protected ClientCommonFactorNotFoundException(SerializationInfo info, StreamingContext context): base(info, context){ }

    }
}
