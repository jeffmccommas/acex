﻿using CE.BillDisagg;
using CE.ContentModel;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.Text;

namespace UnitTest
{
    [TestClass]
    public class BillDisaggTests
    {
        [TestInitialize]
        public void BillDisaggTestInit()
        {
            // This initializes the automapper!
            new AutoMapperBootStrapper().BootStrap();
        }

        [TestCleanup]
        public void BillDisaggTestClean()
        {
            //noop
        }

        [TestMethod]
        public void BillDisagg_Execute_Bills_AGas_MinUsage()
        {
            BillDisaggManagerFactory factory = null;
            IBillDisagg manager = null;
            BillDisaggResult result = null;

            int clientId = 256;
            string customerId = "GUL47191";
            string accountId = "ZUW26449";
            string premiseId = "RIV9449";

            factory = new BillDisaggManagerFactory();
            manager = factory.CreateBillDisaggManager(clientId);

            List<Bill> bills = new List<Bill>();

            // bills for gas
            bills.Add(new Bill()
            {
                StartDate = DateTime.Parse("2/5/2015"),
                EndDate = DateTime.Parse("3/4/2015"),
                BillDays = (DateTime.Parse("3/4/2015") - DateTime.Parse("2/5/2015")).Days,
                CommodityKey = "gas",
                UomKey = "therms",
                TotalServiceUse = 1m,
                TotalServiceCost = 1m
            });
            bills.Add(new Bill()
            {
                StartDate = DateTime.Parse("3/4/2015"),
                EndDate = DateTime.Parse("4/4/2015"),
                BillDays = (DateTime.Parse("4/4/2015") - DateTime.Parse("3/4/2015")).Days,
                CommodityKey = "gas",
                UomKey = "therms",
                TotalServiceUse = 1m,
                TotalServiceCost = 1m
            });


            // Sending in the bills
            var inputParams = new DisaggInputParams()
            {
                AccountId = accountId,
                CustomerId = customerId,
                PremiseId = premiseId,
                Bills = bills,
                EMResultAsInput = null
            };

            //execute bill disagg here
            result = manager.ExecuteBillDisagg(inputParams);

            //let us see the result
            WriteOutputToDebugWindow(result);

        }


        [TestMethod]
        public void BillDisagg_Execute_Bills_AGas()
        {
            BillDisaggManagerFactory factory = null;
            IBillDisagg manager = null;
            BillDisaggResult result = null;

            int clientId = 256;
            string customerId = "GUL47191";
            string accountId = "ZUW26449";
            string premiseId = "RIV9449";

            factory = new BillDisaggManagerFactory();
            manager = factory.CreateBillDisaggManager(clientId);

            List<Bill> bills = new List<Bill>();

            // bills for gas
            bills.Add(new Bill()
            {
                StartDate = DateTime.Parse("2/5/2015"),
                EndDate = DateTime.Parse("3/4/2015"),
                BillDays = (DateTime.Parse("3/4/2015") - DateTime.Parse("2/5/2015")).Days,
                CommodityKey = "gas",
                UomKey = "therms",
                TotalServiceUse = 120.0m,
                TotalServiceCost = 40.0m
            });
            bills.Add(new Bill()
            {
                StartDate = DateTime.Parse("3/4/2015"),
                EndDate = DateTime.Parse("4/4/2015"),
                BillDays = (DateTime.Parse("4/4/2015") - DateTime.Parse("3/4/2015")).Days,
                CommodityKey = "gas",
                UomKey = "therms",
                TotalServiceUse = 150.5m,
                TotalServiceCost = 41.28m
            });


            // Sending in the bills
            var inputParams = new DisaggInputParams()
            {
                AccountId = accountId,
                CustomerId = customerId,
                PremiseId = premiseId,
                Bills = bills,
                EMResultAsInput = null
            };

            //execute bill disagg here
            result = manager.ExecuteBillDisagg(inputParams);

            //let us see the result
            WriteOutputToDebugWindow(result);

        }

        [TestMethod]
        public void BillDisagg_Execute_Bills_AElec()
        {
            BillDisaggManagerFactory factory = null;
            IBillDisagg manager = null;
            BillDisaggResult result = null;

            int clientId = 256;
            string customerId = "GUL47191";
            string accountId = "ZUW26449";
            string premiseId = "RIV9449";

            factory = new BillDisaggManagerFactory();
            manager = factory.CreateBillDisaggManager(clientId);

            List<Bill> bills = new List<Bill>();

            // bills for elec
            bills.Add(new Bill()
            {
                StartDate = DateTime.Parse("2/5/2015"),
                EndDate = DateTime.Parse("3/4/2015"),
                BillDays = (DateTime.Parse("3/4/2015") - DateTime.Parse("2/5/2015")).Days,
                CommodityKey = "electric",
                UomKey = "kwh",
                TotalServiceUse = 480.0m,
                TotalServiceCost = 68.0m
            });
            bills.Add(new Bill()
            {
                StartDate = DateTime.Parse("3/4/2015"),
                EndDate = DateTime.Parse("4/4/2015"),
                BillDays = (DateTime.Parse("4/4/2015") - DateTime.Parse("3/4/2015")).Days,
                CommodityKey = "electric",
                UomKey = "kwh",
                TotalServiceUse = 500.0m,
                TotalServiceCost = 72.77m
            });


            // Sending in the bills
            var inputParams = new DisaggInputParams()
            {
                AccountId = accountId,
                CustomerId = customerId,
                PremiseId = premiseId,
                Bills = bills,
                EMResultAsInput = null
            };

            //execute bill disagg here
            result = manager.ExecuteBillDisagg(inputParams);

            //let us see the result
            WriteOutputToDebugWindow(result);

        }

        [TestMethod]
        public void BillDisagg_Execute_Bills_AGasElec()
        {
            BillDisaggManagerFactory factory = null;
            IBillDisagg manager = null;
            BillDisaggResult result = null;

            int clientId = 256;
            string customerId = "GUL47191";
            string accountId = "ZUW26449";
            string premiseId = "RIV9449";

            factory = new BillDisaggManagerFactory();
            manager = factory.CreateBillDisaggManager(clientId);

            List<Bill> bills = new List<Bill>();

            // bills for elec
            bills.Add(new Bill()
            {
                StartDate = DateTime.Parse("2/5/2015"),
                EndDate = DateTime.Parse("3/4/2015"),
                BillDays = (DateTime.Parse("3/4/2015") - DateTime.Parse("2/5/2015")).Days,
                CommodityKey = "electric",
                UomKey = "kwh",
                TotalServiceUse = 480.0m,
                TotalServiceCost = 68.0m
            });
            bills.Add(new Bill()
            {
                StartDate = DateTime.Parse("3/4/2015"),
                EndDate = DateTime.Parse("4/4/2015"),
                BillDays = (DateTime.Parse("4/4/2015") - DateTime.Parse("3/4/2015")).Days,
                CommodityKey = "electric",
                UomKey = "kwh",
                TotalServiceUse = 500.0m,
                TotalServiceCost = 72.77m
            });


            // bills for gas
            bills.Add(new Bill()
            {
                StartDate = DateTime.Parse("2/5/2015"),
                EndDate = DateTime.Parse("3/4/2015"),
                BillDays = (DateTime.Parse("3/4/2015") - DateTime.Parse("2/5/2015")).Days,
                CommodityKey = "gas",
                UomKey = "therms",
                TotalServiceUse = 120.0m,
                TotalServiceCost = 40.0m
            });
            bills.Add(new Bill()
            {
                StartDate = DateTime.Parse("3/4/2015"),
                EndDate = DateTime.Parse("4/4/2015"),
                BillDays = (DateTime.Parse("4/4/2015") - DateTime.Parse("3/4/2015")).Days,
                CommodityKey = "gas",
                UomKey = "therms",
                TotalServiceUse = 150.5m,
                TotalServiceCost = 41.28m
            });


            // Sending in the bills
            var inputParams = new DisaggInputParams()
            {
                AccountId = accountId,
                CustomerId = customerId,
                PremiseId = premiseId,
                Bills = bills,
                EMResultAsInput = null
            };

            //execute bill disagg here
            result = manager.ExecuteBillDisagg(inputParams);

            //let us see the result
            WriteOutputToDebugWindow(result);

        }

        [TestMethod]
        public void BillDisagg_Execute_Bills_AGasElecWater()
        {
            BillDisaggManagerFactory factory = null;
            IBillDisagg manager = null;
            BillDisaggResult result = null;

            int clientId = 256;
            string customerId = "GUL47191";
            string accountId = "ZUW26449";
            string premiseId = "RIV9449";

            factory = new BillDisaggManagerFactory();
            manager = factory.CreateBillDisaggManager(clientId);

            List<Bill> bills = new List<Bill>();

            // bills for ELEC
            bills.Add(new Bill()
            {
                StartDate = DateTime.Parse("2/5/2015"),
                EndDate = DateTime.Parse("3/4/2015"),
                BillDays = (DateTime.Parse("3/4/2015") - DateTime.Parse("2/5/2015")).Days,
                CommodityKey = "electric",
                UomKey = "kwh",
                TotalServiceUse = 480.0m,
                TotalServiceCost = 68.0m
            });
            bills.Add(new Bill()
            {
                StartDate = DateTime.Parse("3/4/2015"),
                EndDate = DateTime.Parse("4/4/2015"),
                BillDays = (DateTime.Parse("4/4/2015") - DateTime.Parse("3/4/2015")).Days,
                CommodityKey = "electric",
                UomKey = "kwh",
                TotalServiceUse = 500.0m,
                TotalServiceCost = 72.77m
            });


            // bills for GAS
            bills.Add(new Bill()
            {
                StartDate = DateTime.Parse("2/5/2015"),
                EndDate = DateTime.Parse("3/4/2015"),
                BillDays = (DateTime.Parse("3/4/2015") - DateTime.Parse("2/5/2015")).Days,
                CommodityKey = "gas",
                UomKey = "therms",
                TotalServiceUse = 120.0m,
                TotalServiceCost = 40.0m
            });
            bills.Add(new Bill()
            {
                StartDate = DateTime.Parse("3/4/2015"),
                EndDate = DateTime.Parse("4/4/2015"),
                BillDays = (DateTime.Parse("4/4/2015") - DateTime.Parse("3/4/2015")).Days,
                CommodityKey = "gas",
                UomKey = "therms",
                TotalServiceUse = 150.5m,
                TotalServiceCost = 41.28m
            });


            // bills for WATER
            bills.Add(new Bill()
            {
                StartDate = DateTime.Parse("2/5/2015"),
                EndDate = DateTime.Parse("3/4/2015"),
                BillDays = (DateTime.Parse("3/4/2015") - DateTime.Parse("2/5/2015")).Days,
                CommodityKey = "water",
                UomKey = "gallons",
                TotalServiceUse = 8000m,
                TotalServiceCost = 40m
            });
            bills.Add(new Bill()
            {
                StartDate = DateTime.Parse("3/4/2015"),
                EndDate = DateTime.Parse("4/4/2015"),
                BillDays = (DateTime.Parse("4/4/2015") - DateTime.Parse("3/4/2015")).Days,
                CommodityKey = "water",
                UomKey = "gallons",
                TotalServiceUse = 9500m,
                TotalServiceCost = 47.50m
            });

            // Sending in the bills
            var inputParams = new DisaggInputParams()
            {
                AccountId = accountId,
                CustomerId = customerId,
                PremiseId = premiseId,
                Bills = bills,
                EMResultAsInput = null
            };

            //execute bill disagg here
            result = manager.ExecuteBillDisagg(inputParams);

            //let us see the result
            WriteOutputToDebugWindow(result);

        }


        [TestMethod]
        public void BillDisagg_Execute_Bills_12()
        {
            BillDisaggManagerFactory factory = null;
            IBillDisagg manager = null;
            BillDisaggResult result = null;

            int clientId = 256;
            string customerId = "GUL47191";
            string accountId = "ZUW26449";
            string premiseId = "RIV9449";

            factory = new BillDisaggManagerFactory();
            manager = factory.CreateBillDisaggManager(clientId);

            List<Bill> bills = new List<Bill>();

            // bills for electric
            DateTime startDate = DateTime.Parse("8/1/2015");
            DateTime endDate = DateTime.Now;

            for (int i = 0; i < 12; i++)
            {
                startDate = startDate.AddMonths(-1);
                endDate = startDate.AddDays(DateTime.DaysInMonth(startDate.Year, startDate.Month));

                Bill elecBill = new Bill()
                {
                    StartDate = startDate,
                    EndDate = endDate,
                    BillDays = (endDate - startDate).Days,
                    CommodityKey = "electric",
                    UomKey = "kwh",
                    TotalServiceUse = 1000.0m,
                    TotalServiceCost = 200.0m
                };

                bills.Add(elecBill);
            }


            // bills for gas
            startDate = DateTime.Parse("8/1/2015");
            endDate = DateTime.Now;

            for (int i = 0; i < 12; i++)
            {
                startDate = startDate.AddMonths(-1);
                endDate = startDate.AddDays(DateTime.DaysInMonth(startDate.Year, startDate.Month));

                Bill gasBill = new Bill()
                {
                    StartDate = startDate,
                    EndDate = endDate,
                    BillDays = (endDate - startDate).Days,
                    CommodityKey = "gas",
                    UomKey = "ccf",              //typically therms, use ccf or mcf to test test conversion
                    TotalServiceUse = 120.0m,
                    TotalServiceCost = 40.0m
                };

                bills.Add(gasBill);
            }

            // bills for water
            startDate = DateTime.Parse("8/1/2015");
            endDate = DateTime.Now;

            for (int i = 0; i < 12; i++)
            {
                startDate = startDate.AddMonths(-1);
                endDate = startDate.AddDays(DateTime.DaysInMonth(startDate.Year, startDate.Month));

                Bill waterBill = new Bill()
                {
                    StartDate = startDate,
                    EndDate = endDate,
                    BillDays = (endDate - startDate).Days,
                    CommodityKey = "water",
                    UomKey = "gallons",
                    TotalServiceUse = 9500m,
                    TotalServiceCost = 51.0m
                };

                bills.Add(waterBill);
            }

            // Sending in some bills
            var inputParams = new DisaggInputParams()
            {
                AccountId = accountId,
                CustomerId = customerId,
                PremiseId = premiseId,
                Bills = bills,
                EMResultAsInput = null
            };

            //execute bill disagg here
            result = manager.ExecuteBillDisagg(inputParams);

            //let us see the result
            WriteOutputToDebugWindow(result);

        }


        [TestMethod]
        public void BillDisagg_Execute()
        {
            BillDisaggManagerFactory factory = null;
            IBillDisagg manager = null;
            BillDisaggResult result = null;

            int clientId = 256;
            string customerId = "GUL47191";
            string accountId = "ZUW26449";
            string premiseId = "RIV9449";

            factory = new BillDisaggManagerFactory();
            manager = factory.CreateBillDisaggManager(clientId);

            result = manager.ExecuteBillDisagg(customerId, accountId, premiseId);

            //let us see the result
            WriteOutputToDebugWindow(result);

            //attempt save (test save and retrieve)
            //var dal = new CE.BillDisagg.DataAccess.DataAccessLayer();
            //dal.SaveBillDisagg(clientId, customerId, accountId, premiseId, result.FlatResultTable);
            //var dt = dal.GetBillDisagg(clientId, customerId, System.DateTime.Now, accountId, premiseId, false);
        }

        [TestMethod]
        [ExpectedException(typeof(System.Collections.Generic.KeyNotFoundException))]
        public void BillDisagg_ExceptionTest()
        {
            BillDisaggManagerFactory factory = null;
            IBillDisagg manager = null;
           

            int clientId = 256;
           
            factory = new BillDisaggManagerFactory();
            manager = factory.CreateBillDisaggManager(clientId);
            var config = new BillDisaggConfiguration();
            var convert_ccf2therm = config.GetCommonFactor<decimal>("");
            
        }

        [TestMethod]
        public void BillDisagg_Execute_NewDefRate()
        {
            BillDisaggManagerFactory factory = null;
            IBillDisagg manager = null;
            BillDisaggResult result = null;

            int clientId = 224;     // NOTE THAT THIS IS FOR A 224 test for UIL
            string customerId = "01000001880979";
            string accountId = "01000001880979";
            string premiseId = "1160002960020";

            factory = new BillDisaggManagerFactory();
            manager = factory.CreateBillDisaggManager(clientId);

            result = manager.ExecuteBillDisagg(customerId, accountId, premiseId);

            //let us see the result
            WriteOutputToDebugWindow(result);

            //attempt save (test save and retrieve)
            //var dal = new CE.BillDisagg.DataAccess.DataAccessLayer();
            //dal.SaveBillDisagg(clientId, customerId, accountId, premiseId, result.FlatResultTable);
            //var dt = dal.GetBillDisagg(clientId, customerId, System.DateTime.Now, accountId, premiseId, false);
        }

        [TestMethod]
        public void BillDisagg_Execute_PassConnStrs()
        {
            BillDisaggManagerFactory factory = null;
            IBillDisagg manager = null;
            BillDisaggResult result = null;

            int clientId = 256;
            string customerId = "GUL47191";
            string accountId = "ZUW26449";
            string premiseId = "RIV9449";

            factory = new BillDisaggManagerFactory();

            var csMeta = @"Data Source=AZUDSQL001.stg.aclarace.com;Initial Catalog=InsightsMetadata;Persist Security Info=True;User ID=cesqluser;Password=Acl@r@694;Packet Size=32767;";    //ConfigurationManager.ConnectionStrings["InsightsMetaDataConn"].ConnectionString;
            var csDW = @"Data Source=AZUDSQL001.stg.aclarace.com;Initial Catalog=InsightsDW;Persist Security Info=True;User ID=cesqluser;Password=Acl@r@694;Packet Size=32767;"; //ConfigurationManager.ConnectionStrings["InsightsDWConn"].ConnectionString;
            //var csInsights = ConfigurationManager.ConnectionStrings["InsightsConn"].ConnectionString;

            manager = factory.CreateBillDisaggManager(clientId, csDW, csMeta, string.Empty);    //purposely leave out the Insights String so it only uses DW for PAs

            result = manager.ExecuteBillDisagg(customerId, accountId, premiseId);

            //let us see the result
            WriteOutputToDebugWindow(result);

        }

        [TestMethod]
        public void BillDisagg_TestConString()
        {
            var dataAccess= new CE.BillDisagg.DataAccess.DataAccessLayer();
            var result = dataAccess.TestConnInsightsMetadata();
            Assert.IsTrue(result);
        }



        /// <summary>
        /// Writes resulting collections and flat table to output debug window.
        /// </summary>
        /// <param name="result"></param>
        private static void WriteOutputToDebugWindow(BillDisaggResult result)
        {

            //output the per enduse usages and costs per commodity (executing this in Debug will write to TEST output result window)
            Debug.WriteLine("----------"); Debug.WriteLine("END USES");
            foreach (var enduse in result.EndUses)
            {
                Debug.WriteLine("EndUse = {0}{1} {2}; ({3})", "", enduse.Name, enduse.Key, String.Join(",", enduse.ApplianceNames.Select(x => x.ToString()).ToArray()));

                foreach (var eng in enduse.EnergyUses)
                {
                    Debug.WriteLine("  Commodity = {0}, Usage = {1:0.####} {2}, Cost = {3:0.###} US Dollars, commKey={4}, uomkey={5}, currKey={6}, recRatio={7:0.###}", eng.Commodity.ToString(), eng.Quantity, eng.UnitOfMeasure, eng.Cost, eng.CommodityKey, eng.UomKey, eng.CurrencyKey, eng.RecRatio);
                }

            }

            //output the per appliance usages and costs per commodity (executing this in Debug will write to TEST output result window)
            Debug.WriteLine("----------"); Debug.WriteLine("APPLIANCES");
            foreach (var appl in result.Appliances)
            {
                Debug.WriteLine("Appliance = {0}{1} {2},", "", appl.Name, appl.Key);

                foreach (var eng in appl.EnergyUses)
                {
                    Debug.WriteLine("  Commodity = {0}, Usage = {1:0.####} {2}, Cost = {3:0.###} US Dollars, commKey={4}, uomkey={5}, currKey={6}, recRatio={7:0.###}", eng.Commodity.ToString(), eng.Quantity, eng.UnitOfMeasure, eng.Cost, eng.CommodityKey, eng.UomKey, eng.CurrencyKey, eng.RecRatio);
                }

            }

            //table.Columns.Add("ClientId", typeof(int));
            //table.Columns.Add("CustomerId", typeof(string));
            //table.Columns.Add("AccountId", typeof(string));
            //table.Columns.Add("PremiseId", typeof(string));
            //table.Columns.Add("BillDate", typeof(DateTime));
            //table.Columns.Add("EndUseKey", typeof(string));
            //table.Columns.Add("ApplianceKey", typeof(string));
            //table.Columns.Add("CommodityKey", typeof(string));
            //table.Columns.Add("PeriodId", typeof(int));
            //table.Columns.Add("Usage", typeof(decimal));
            //table.Columns.Add("Cost", typeof(decimal));
            //table.Columns.Add("UomKey", typeof(string));
            //table.Columns.Add("CurrencyKey", typeof(string));
            //table.Columns.Add("Confidence", typeof(int));
            //table.Columns.Add("ReconciliationRatio", typeof(decimal));
            //table.Columns.Add("StatusId", typeof(int));
            //table.Columns.Add("NewDate", typeof(DateTime));

            //output data table
            Debug.WriteLine("----------"); Debug.WriteLine("FLAT DATA TABLE");
            StringBuilder output = new StringBuilder();
            foreach (DataRow row in result.FlatResultTable.Rows)
            {
                foreach (DataColumn col in result.FlatResultTable.Columns)
                {
                    output.AppendFormat("{0} ", row[col]);
                }

                output.AppendLine();
            }
            Debug.WriteLine(output.ToString());


        }


    }
}
