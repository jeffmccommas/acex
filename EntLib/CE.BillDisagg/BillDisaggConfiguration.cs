﻿using System.Collections.Generic;
using System.ComponentModel;
using CE.BillDisagg.Exceptions;

namespace CE.BillDisagg
{
    public class BillDisaggConfiguration
    {
        private Dictionary<string, string> _commonFactors;
        private Dictionary<string, decimal[]> _applianceMonthlyLoadFactors;
        private Dictionary<string, decimal> _applianceAnnualLoadFactors;

        public BillDisaggConfiguration()
        {
            _commonFactors = new Dictionary<string, string>();
            _applianceMonthlyLoadFactors = new Dictionary<string, decimal[]>();
            _applianceAnnualLoadFactors = new Dictionary<string, decimal>();
        }

        public Dictionary<string, string> CommonFactors
        {
            get
            {
                return (this._commonFactors);
            }

        }

        public Dictionary<string, decimal[]> ApplianceMonthlyLoadFactors
        {
            get
            {
                return (this._applianceMonthlyLoadFactors);
            }

        }

        public Dictionary<string, decimal> ApplianceAnnualLoadFactors
        {
            get
            {
                return (this._applianceAnnualLoadFactors);
            }

        }


        /// <summary>
        /// Allow a type convertor to more easily convert a factor setting to the appropriate type.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="key"></param>
        /// <returns></returns>
        public T GetCommonFactor<T>(string key)
        {
            var factor = this._commonFactors[key];
            if (string.IsNullOrWhiteSpace(factor)) throw new ClientCommonFactorNotFoundException(key);

            var converter = TypeDescriptor.GetConverter(typeof(T));
            return (T)(converter.ConvertFromInvariantString(factor));
        }

    }


}
