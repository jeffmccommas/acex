﻿using System;
using System.Data;

namespace CE.BillDisagg
{
    public class DataFactory
    {

        public DataFactory()
        {

        }

        /// <summary>
        /// Create a table for BillDisagg data with specific named columns that match TVP and Table. 
        /// </summary>
        /// <returns></returns>
        public DataTable CreateBillDisaggDataTable()
        {
            var table = new DataTable();

            table.Columns.Add("ClientId", typeof(int));
            table.Columns.Add("CustomerId", typeof(string));
            table.Columns.Add("AccountId", typeof(string));
            table.Columns.Add("PremiseId", typeof(string));
            table.Columns.Add("BillDate", typeof(DateTime));
            table.Columns.Add("EndUseKey", typeof(string));
            table.Columns.Add("ApplianceKey", typeof(string));
            table.Columns.Add("CommodityKey", typeof(string));
            table.Columns.Add("PeriodId", typeof(int));
            table.Columns.Add("Usage", typeof(decimal));
            table.Columns.Add("Cost", typeof(decimal));
            table.Columns.Add("UomKey", typeof(string));
            table.Columns.Add("CurrencyKey", typeof(string));
            table.Columns.Add("Confidence", typeof(int));
            table.Columns.Add("ReconciliationRatio", typeof(decimal));
            table.Columns.Add("StatusId", typeof(int));
            table.Columns.Add("NewDate", typeof(DateTime));

            return (table);
        }


        /// <summary>
        /// Create a table for Bill data with specific named columns that match TVP and Table. 
        /// </summary>
        /// <returns></returns>
        public DataTable CreateBillDataTable()
        {
            var table = new DataTable();

            table.Columns.Add("ClientId", typeof(int));
            table.Columns.Add("CustomerID", typeof(string));
            table.Columns.Add("AccountID", typeof(string));
            table.Columns.Add("PremiseID", typeof(string));
            table.Columns.Add("StartDate", typeof(DateTime));
            table.Columns.Add("EndDate", typeof(DateTime));
            table.Columns.Add("CommodityKey", typeof(string));
            table.Columns.Add("TotalUnits", typeof(decimal));
            table.Columns.Add("TotalCost", typeof(decimal));
            table.Columns.Add("UomKey", typeof(string));
            table.Columns.Add("BillDays", typeof(int));

            return (table);
        }

    }
}
