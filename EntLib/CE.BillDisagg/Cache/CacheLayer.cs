﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Caching;

namespace CE.BillDisagg.Cache
{
    /// <summary>
    /// Caching common project for bill disagg.
    /// </summary>
    public class CacheLayer
    {
        static readonly ObjectCache Cache = MemoryCache.Default;
        /*
        /// <summary>
        /// Retrieve cached item.
        /// </summary>
        /// <typeparam name="T">Type of cached item</typeparam>
        /// <param name="key">Name of cached item</param>
        /// <returns>Cached item as type</returns>
        public static T Get<T>(string key) where T : class
        {
            try
            {
                return (T)Cache[key];
            }
            catch
            {
                return null;
            }
        }

        /// <summary>
        /// Insert value into the cache using appropriate name/value pairs.
        /// </summary>
        /// <typeparam name="T">Type of cached item</typeparam>
        /// <param name="objectToCache">Item to be cached</param>
        /// <param name="key">Name of item</param>
        public static void Add<T>(T objectToCache, string key) where T : class
        {
            Cache.Add(key, objectToCache, DateTime.Now.AddMinutes(10));
        }
        */
        /// <summary>
        /// Insert value into the cache using appropriate name/value pairs.  Specify the timeout in minutes
        /// </summary>
        /// <typeparam name="T">Type of cached item</typeparam>
        /// <param name="objectToCache">Item to be cached</param>
        /// <param name="key">Name of item</param>
        /// <param name="timeoutInMinutes">Timeout in minutes</param>
        public static void Add<T>(T objectToCache, string key, double timeoutInMinutes) where T : class
        {
            CacheItemPolicy policy = new CacheItemPolicy();
            policy.Priority = CacheItemPriority.Default;
            policy.AbsoluteExpiration = DateTimeOffset.Now.AddMinutes(timeoutInMinutes);
            Cache.Add(key, objectToCache, policy);
        }

        /*
        /// <summary>
        /// Insert value into the cache using appropriate name/value pairs.
        /// </summary>
        /// <param name="objectToCache">Item to be cached</param>
        /// <param name="key">Name of item</param>
        public static void Add(object objectToCache, string key)
        {
            Cache.Add(key, objectToCache, DateTime.Now.AddMinutes(10));
        }

        /// <summary>
        /// Remove item from cache.
        /// </summary>
        /// <param name="key">Name of cached item</param>
        public static void Clear(string key)
        {
            Cache.Remove(key);
        }

        /// <summary>
        /// Check for item in cache.
        /// </summary>
        /// <param name="key">Name of cached item</param>
        /// <returns></returns>
        public static bool Exists(string key)
        {
            return Cache.Get(key) != null;
        }

        /// <summary>
        /// Gets all cached items as a list by their key.
        /// </summary>
        /// <returns></returns>
        public static List<string> GetAll()
        {
            return Cache.Select(keyValuePair => keyValuePair.Key).ToList();
        }
        */

    }

}
