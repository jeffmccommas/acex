﻿
namespace CE.BillDisagg
{
    public class DailyUsage
    {
        public bool IsDerivedFromActualBill { get; set; }
        public decimal Usage { get; set; }
        public decimal ModeledUsage { get; set; }

        public DailyUsage()
        {
            IsDerivedFromActualBill = false;
            Usage = 0.0m;
            ModeledUsage = 0.0m;
        }

    }
}
