﻿using System;
using System.Collections.Generic;
using CE.EnergyModel;

namespace CE.BillDisagg
{
    /// <summary>
    /// Full set of input parameters for a bill disaggregation.  Many are optional
    /// </summary>
    public class DisaggInputParams
    {
        /// <summary>
        /// CustomerId is required.
        /// </summary>
        public string CustomerId { get; set; }
        /// <summary>
        /// AccountId is required.
        /// </summary>
        public string AccountId { get; set; }
        /// <summary>
        /// PremiseId is required.
        /// </summary>
        public string PremiseId { get; set; }
        /// <summary>
        /// StartDate is optional.
        /// </summary>
        public DateTime? StartDate { get; set; }
        /// <summary>
        /// EndDate is optional.
        /// </summary>
        public DateTime? EndDate { get; set; }
        /// <summary>
        /// Bills are optional.
        /// </summary>
        public List<Bill> Bills { get; set; }
        /// <summary>
        /// Energy Model result as input is optional.
        /// </summary>
        public EnergyModelResult EMResultAsInput { get; set; }
        /// <summary>
        /// Is Business can be specified to force the Business Energy Model be executed.  Optional.
        /// </summary>
        public bool IsBusiness { get; set; }

        public DisaggInputParams()
        {
            Bills = null;
            EMResultAsInput = null;
        }

    }
}
