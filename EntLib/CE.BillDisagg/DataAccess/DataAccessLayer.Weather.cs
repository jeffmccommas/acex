﻿//using System;
//using System.Data;
//using System.Data.SqlClient;
//using CE.BillDisagg.Entities;

//namespace CE.BillDisagg.DataAccess
//{
//    public partial class DataAccessLayer
//    {

//        #region ActualDailyWeather

//        /// <summary>
//        /// This gets the HDD and CDD from Actual Daily weather.  Sum over the time period.
//        /// </summary>
//        /// <param name="stationId"></param>
//        /// <param name="startDate"></param>
//        /// <param name="endDate"></param>
//        /// <returns></returns>
//        public ActualWeather GetActualWeather(string stationId, DateTime startDate, DateTime endDate)
//        {
//            ActualWeather actual = new ActualWeather();
//            const string storedProcedureName = "[cm].[uspEMSelectDailyWeatherTotal]";

//            using (SqlConnection connection = new SqlConnection(GetConnectionStringMetaData()))
//            {
//                using (var cmd = new SqlCommand(storedProcedureName, connection))
//                {
//                    cmd.CommandType = CommandType.StoredProcedure;
//                    cmd.Parameters.Add(new SqlParameter("@StationId", typeof(string)) { Value = stationId });
//                    cmd.Parameters.Add(new SqlParameter("@StartDate", typeof(DateTime)) { Value = startDate });
//                    cmd.Parameters.Add(new SqlParameter("@EndDate", typeof(DateTime)) { Value = endDate });

//                    connection.Open();

//                    using (var reader = cmd.ExecuteReader())
//                    {
//                        if (reader.HasRows)
//                        {
//                            int? hdd = 0;
//                            int? cdd = 0;

//                            // User with client & user properties
//                            while (reader.Read())
//                            {
//                                hdd = reader["HDD"] as int? ?? default(int);
//                                cdd = reader["CDD"] as int? ?? default(int);
//                            }

//                            actual.TotalHDD = hdd.Value;
//                            actual.TotalCDD = cdd.Value;
//                        }

//                    }

//                }

//            }

//            return (actual);
//        }

//        #endregion


//    }
//}
