﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;

namespace CE.BillDisagg.DataAccess
{
    public class DataAccessLayer
    {
        private const string InsightsMetaDataConnName = "InsightsMetaDataConn";
        private const string InsightsDwConnName = "InsightsDWConn";
        private const string InsightsConnName = "InsightsConn";
        private const string AppSettingPaaSOnly = "PaaSOnly";
        private const string AppSettingPaaSDedicatedClients = "PaaSDedicatedClients";

        private readonly string _metaDataConnString;
        private readonly string _dwConnString;
        private readonly string _insightsConnString;

        private const string StoredProcedureNameInsights = "[wh].[uspBDGetBills]";      // within Insights
        private const string StoredProcedureNameInsightsDw = "[dbo].[uspBDGetBills]";   // within InsightsDW

        /// <summary>
        /// Constuctor
        /// </summary>
        public DataAccessLayer()
        {
            _metaDataConnString = ConfigurationManager.ConnectionStrings[InsightsMetaDataConnName].ConnectionString;
            _dwConnString = ConfigurationManager.ConnectionStrings[InsightsDwConnName].ConnectionString;
            _insightsConnString = ConfigurationManager.ConnectionStrings[InsightsConnName].ConnectionString;
        }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="csInsightsDw">The connection string for InsightsDW</param>
        /// <param name="csInsightsMetadata">The connection string for InsightMetadata</param>
        /// <param name="csInsights">The connection string for Insights</param>
        public DataAccessLayer(string csInsightsMetadata, string csInsightsDw, string csInsights)
        {
            _metaDataConnString = csInsightsMetadata;
            _dwConnString = csInsightsDw;
            _insightsConnString = csInsights;
        }

        /// <summary>
        /// Change the Initial Catalog to the proper one for the ClientId being run.
        /// </summary>
        /// <param name="clientId">The client Id</param>
        /// <returns>A InsightsDw connection string.</returns>
        private string GetConnectionStringInsightsDw(int clientId)
        {
            string connString;

            //Check if PaaS, and modify connection string
            var sPaaS = ConfigurationManager.AppSettings[AppSettingPaaSOnly];

            if (!string.IsNullOrEmpty(sPaaS))
            {
                if (Convert.ToBoolean(sPaaS))
                {
                    var sPaaSDedicatedClients = ConfigurationManager.AppSettings[AppSettingPaaSDedicatedClients];

                    if (!string.IsNullOrEmpty(sPaaSDedicatedClients))
                    {
                        var cid = clientId.ToString();
                        connString = sPaaSDedicatedClients.Split(',').ToList().Contains(cid)
                            ? _dwConnString.Replace("InsightsDW;", "InsightsDW_" + cid + ";")
                            : _dwConnString.Replace("InsightsDW;", "InsightsDW_Test;");
                    }
                    else
                    {
                        connString = _dwConnString.Replace("InsightsDW;", "InsightsDW_Test;");
                    }
                }
                else
                {
                    connString = _dwConnString;
                }
            }
            else
            {
                connString = _dwConnString;
            }

            return connString;
        }

        /// <summary>
        /// Get the InsightsMetadata connection string.
        /// </summary>
        /// <returns>The InsightsMetadata connection string.</returns>
        private string GetConnectionStringMetaData()
        {
            return _metaDataConnString;
        }

        /// <summary>
        /// Get the Insights connection string.
        /// </summary>
        /// <returns>The Insights connection string.</returns>
        private string GetConnectionStringInsights()
        {
            return _insightsConnString;
        }

        /// <summary>
        /// Get bills to use for bill disagg adjustments.
        /// </summary>
        /// <param name="clientId">A client Id</param>
        /// <param name="customerId">A customer Id</param>
        /// <param name="accountId">An account Id </param>
        /// <param name="premiseId">A premise Id</param>
        /// <returns>A list of Bills</returns>
        public List<Bill> GetBills(int clientId, string customerId, string accountId, string premiseId)
        {
            // Initialize list.
            var bills = new List<Bill>();

            // Look in the Insights database for bills.
            if (!string.IsNullOrEmpty(GetConnectionStringInsights()))
            {

                using (var connection = new SqlConnection(GetConnectionStringInsights()))
                {
                    using (var cmd = new SqlCommand(StoredProcedureNameInsights, connection))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Add(new SqlParameter("@ClientID", typeof(int)) {Value = clientId});
                        cmd.Parameters.Add(new SqlParameter("@CustomerID", typeof(string)) {Value = customerId});
                        cmd.Parameters.Add(new SqlParameter("@AccountID", typeof(string)) {Value = accountId});
                        cmd.Parameters.Add(new SqlParameter("@PremiseID", typeof(string)) {Value = premiseId});

                        connection.Open();

                        using (var reader = cmd.ExecuteReader())
                        {
                            if (reader.HasRows)
                            {
                                bills = new List<Bill>();

                                // User with client & user properties
                                while (reader.Read())
                                {
                                    DateTime? startDate = reader["startdate"] as DateTime? ?? default(DateTime);
                                    DateTime? endDate = reader["enddate"] as DateTime? ?? default(DateTime);
                                    decimal? usage = reader["totalunits"] as decimal? ?? default(decimal);
                                    decimal? cost = reader["totalcost"] as decimal? ?? default(decimal);
                                    var commoditykey = reader["commoditykey"] as string;
                                    var uomkey = reader["uomkey"] as string;
                                    int? billdays = reader["BillDays"] as int? ?? default(int);

                                    var bill = new Bill
                                    {
                                        StartDate = startDate.Value,
                                        EndDate = endDate.Value,
                                        TotalServiceUse = usage.Value,
                                        TotalServiceCost = cost.Value,
                                        CommodityKey = commoditykey,
                                        UomKey = uomkey,
                                        BillDays = billdays.Value
                                    };

                                    bills.Add(bill);
                                }

                            }

                        }

                    }

                }

            }

            // If no bills in Insights database look in InsightsDW for bills and save them into Insights.
            if (bills?.Any() == false)
            {
                using (var connection = new SqlConnection(GetConnectionStringInsightsDw(clientId)))
                {
                    using (var cmd = new SqlCommand(StoredProcedureNameInsightsDw, connection))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Add(new SqlParameter("@ClientID", typeof(int)) {Value = clientId});
                        cmd.Parameters.Add(new SqlParameter("@CustomerID", typeof(string)) {Value = customerId});
                        cmd.Parameters.Add(new SqlParameter("@AccountID", typeof(string)) {Value = accountId});
                        cmd.Parameters.Add(new SqlParameter("@PremiseID", typeof(string)) {Value = premiseId});

                        connection.Open();

                        using (var reader = cmd.ExecuteReader())
                        {
                            if (reader.HasRows)
                            {
                                bills = new List<Bill>();

                                // User with client & user properties
                                while (reader.Read())
                                {
                                    DateTime? startDate = reader["startdate"] as DateTime? ?? default(DateTime);
                                    DateTime? endDate = reader["enddate"] as DateTime? ?? default(DateTime);
                                    decimal? usage = reader["totalunits"] as decimal? ?? default(decimal);
                                    decimal? cost = reader["totalcost"] as decimal? ?? default(decimal);
                                    var commoditykey = reader["commoditykey"] as string;
                                    var uomkey = reader["uomkey"] as string;
                                    int? billdays = reader["BillDays"] as int? ?? default(int);

                                    var bill = new Bill
                                    {
                                        StartDate = startDate.Value,
                                        EndDate = endDate.Value,
                                        TotalServiceUse = usage.Value,
                                        TotalServiceCost = cost.Value,
                                        CommodityKey = commoditykey,
                                        UomKey = uomkey,
                                        BillDays = billdays.Value
                                    };

                                    bills.Add(bill);
                                }

                            }

                        }

                    }

                    //we retrieved bills from InsightsDW, we shall save these into db Insights, if we have a connection string for db Insights!
                    if (bills.Any() && !string.IsNullOrEmpty(GetConnectionStringInsights()))
                    {
                        SaveBills(clientId, customerId, accountId, premiseId, bills);
                    }
                }

            }

            return bills;
        }


        /// <summary>
        /// Save bills to insights using table value parameter.
        /// </summary>
        /// <param name="clientId"></param>
        /// <param name="customerId"></param>
        /// <param name="accountId"></param>
        /// <param name="premiseId"></param>
        /// <param name="bills"></param>
        /// <returns></returns>
        private void SaveBills(int clientId, string customerId, string accountId, string premiseId, IEnumerable<Bill> bills)
        {
            const string storedProcedureName = "[wh].[uspBDSaveBills]";

            var f = new DataFactory();
            var table = f.CreateBillDataTable();

            foreach (var b in bills)
            {
                table.Rows.Add(clientId, customerId, accountId, premiseId, b.StartDate, b.EndDate, b.CommodityKey, b.TotalServiceUse, b.TotalServiceCost, b.UomKey, b.BillDays);
            }

            using (var connection = new SqlConnection(GetConnectionStringInsights()))
            {
                using (var cmd = new SqlCommand(storedProcedureName, connection))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@ClientID", typeof(int)) {Value = clientId});
                    cmd.Parameters.Add(new SqlParameter("@CustomerID", typeof(string)) {Value = customerId});
                    cmd.Parameters.Add(new SqlParameter("@AccountID", typeof(string)) {Value = accountId});
                    cmd.Parameters.Add(new SqlParameter("@PremiseID", typeof(string)) {Value = premiseId});
                    cmd.Parameters.Add(new SqlParameter("@Bills", SqlDbType.Structured)
                    {
                        Value = table,
                        TypeName = "wh.BillTableType"
                    });

                    connection.Open();
                    cmd.ExecuteNonQuery();

                }

            }
        }


        /// <summary>
        /// Get data table of bill disagg.
        /// </summary>
        /// <param name="clientId"></param>
        /// <param name="customerId"></param>
        /// <param name="billDate"></param>
        /// <param name="accountId"></param>
        /// <param name="premiseId"></param>
        /// <param name="saveToInsights"></param>
        /// <returns></returns>
        public DataTable GetBillDisagg(int clientId, string customerId, DateTime billDate, string accountId, string premiseId, bool saveToInsights)
        {
            DataTable disaggTable;

            const string storedProcedureNameInsights = "[wh].[uspBDGetBillDisagg]";      // within Insights

            if (string.IsNullOrEmpty(GetConnectionStringInsights()))
            {
                return null;
            }

            using (var connection = new SqlConnection(GetConnectionStringInsights()))
            {
                using (var cmd = new SqlCommand(storedProcedureNameInsights, connection))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@ClientId", typeof(int)) { Value = clientId });
                    cmd.Parameters.Add(new SqlParameter("@CustomerId", typeof(string)) { Value = customerId });
                    cmd.Parameters.Add(new SqlParameter("@BillDate", typeof(DateTime)) { Value = billDate });
                    cmd.Parameters.Add(new SqlParameter("@AccountId", typeof(string)) { Value = accountId });
                    cmd.Parameters.Add(new SqlParameter("@PremiseId", typeof(string)) { Value = premiseId });

                    connection.Open();

                    using (var reader = cmd.ExecuteReader())
                    {
                        if (!reader.HasRows)
                        {
                            return null;
                        }

                        var df = new DataFactory();
                        disaggTable = df.CreateBillDisaggDataTable();

                        // User with client & user properties
                        while (reader.Read())
                        {
                            var enduseKey = reader["EndUseKey"] as string;
                            var applianceKey = reader["ApplianceKey"] as string;
                            var commodityKey = reader["CommodityKey"] as string;
                            byte? periodId = reader["PeriodId"] as byte? ?? default(byte);
                            decimal? usage = reader["Usage"] as decimal? ?? default(decimal);
                            decimal? cost = reader["Cost"] as decimal? ?? default(decimal);
                            var uomKey = reader["UomKey"] as string;
                            var currencyKey = reader["CurrencyKey"] as string;
                            int? confidence = reader["Confidence"] as int? ?? default(int);
                            decimal? recRatio = reader["ReconciliationRatio"] as decimal? ?? default(decimal);
                            byte? statusId = reader["StatusId"] as byte? ?? default(byte);
                            DateTime? newDate = reader["NewDate"] as DateTime? ?? default(DateTime);

                            disaggTable.Rows.Add(clientId,
                                customerId,
                                accountId,
                                premiseId,
                                billDate,
                                enduseKey,
                                applianceKey,
                                commodityKey,
                                periodId,
                                usage,
                                cost,
                                uomKey,
                                currencyKey,
                                confidence,
                                recRatio,
                                statusId,
                                newDate);

                        }
                    }

                }

            }

       
            return disaggTable;
        }


        /// <summary>
        /// Save a billdisagg data table to Insights.
        /// </summary>
        /// <param name="premiseId"></param>
        /// <param name="disaggTable"></param>
        /// <param name="clientId"></param>
        /// <param name="customerId"></param>
        /// <param name="accountId"></param>
        /// <returns></returns>
        public bool SaveBillDisagg(int clientId, string customerId, string accountId, string premiseId,
            DataTable disaggTable)
        {
            const string storedProcedureName = "[wh].[uspBDSaveBillDisagg]";

            if (disaggTable != null)
            {

                using (SqlConnection connection = new SqlConnection(GetConnectionStringInsights()))
                {
                    using (var cmd = new SqlCommand(storedProcedureName, connection))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Add(new SqlParameter("@ClientID", typeof(int)) {Value = clientId});
                        cmd.Parameters.Add(new SqlParameter("@CustomerID", typeof(string)) {Value = customerId});
                        cmd.Parameters.Add(new SqlParameter("@AccountID", typeof(string)) {Value = accountId});
                        cmd.Parameters.Add(new SqlParameter("@PremiseID", typeof(string)) {Value = premiseId});
                        cmd.Parameters.Add(new SqlParameter("@BillDisaggTable", SqlDbType.Structured)
                        {
                            Value = disaggTable,
                            TypeName = "wh.BillDisaggTableType"
                        });

                        connection.Open();
                        cmd.ExecuteNonQuery();
                    }

                }

            }

            return true;
        }



        public bool TestConnInsightsMetadata()
        {
            var retval = false;
            const string sp = "[cm].[uspEMSelectSolar]";

            if (!string.IsNullOrEmpty(GetConnectionStringMetaData()))
            {
                using (var connection = new SqlConnection(GetConnectionStringMetaData()))
                {
                    using (var cmd = new SqlCommand(sp, connection))
                    {
                        connection.Open();

                        using (var reader = cmd.ExecuteReader())
                        {
                            retval = true;

                            if (reader.HasRows)
                            {
                                // rows or not, we still connected and execute the SP okay.
                            }

                        }

                    }

                }

            }

            return retval;
        }
    }
}
