﻿using System;
using System.Collections.Generic;
using System.Linq;
using CE.EnergyModel;
using CE.EnergyModel.Enums;

namespace CE.BillDisagg
{
    public partial class BillDisaggManager : IBillDisagg
    {
        private const int MinimumBillsCountToTriggerComputedBills = 2;
        private const int MonthsOfComputedBills = 12;


        private List<BillConversionInfo> ModifyActualBillUnitOfMeasure(List<Bill> actualBills)
        {
            List<BillConversionInfo> conversionInfo = null;

            // if any bills are using ccf or mcf, loop and convert those to therms when gas and gallons when water
            if (actualBills.Exists(p => p.UomKey.ToLower() == "ccf" || p.UomKey.ToLower() == "mcf"))
            {
                conversionInfo = new List<BillConversionInfo>();

                var convertCcf2Therm = 1.0m;
                var convertMcf2Therm = 1.0m;
                var convertCcf2Gal = 1.0m;

                try
                {
                    convertCcf2Therm = _config.GetCommonFactor<decimal>("conversion.ccftotherm");
                    convertMcf2Therm = _config.GetCommonFactor<decimal>("conversion.mcftotherm");
                    convertCcf2Gal = _config.GetCommonFactor<decimal>("conversion.ccftogal");
                }
                catch
                {
                    //conversions not setup or present
                }

                // gas init conversion info
                var gasBill = actualBills.Find(p => p.CommodityKey.ToLower() == "gas");
                if (gasBill != null)
                {
                    if (gasBill.UomKey.ToLower() == "ccf")
                    {
                        conversionInfo.Add(new BillConversionInfo() { CommodityKey = "gas", ConversionFactor = convertCcf2Therm, UomKey = "ccf", Uom = UnitOfMeasureType.CCF });        // from uom
                    }
                    else if (gasBill.UomKey.ToLower() == "mcf")
                    {
                        conversionInfo.Add(new BillConversionInfo() { CommodityKey = "gas", ConversionFactor = convertMcf2Therm, UomKey = "mcf", Uom = UnitOfMeasureType.MCF });        // from uom
                    }
                }

                // water init conversion info
                var waterBill = actualBills.Find(p => p.CommodityKey.ToLower() == "water");
                if (waterBill != null)
                {
                    if (waterBill.UomKey.ToLower() == "ccf")
                    {
                        conversionInfo.Add(new BillConversionInfo() { CommodityKey = "water", ConversionFactor = convertCcf2Gal, UomKey = "ccf", Uom = UnitOfMeasureType.CCF });        // from uom
                    }
                }

                // process the bills
                foreach (var bill in actualBills)
                {
                    switch (bill.CommodityKey.ToLower())
                    {

                        case "gas":

                            switch (bill.UomKey.ToLower())
                            {
                                case "ccf":
                                    bill.UomKey = "therms";
                                    bill.TotalServiceUse = bill.TotalServiceUse * convertCcf2Therm;
                                    break;

                                case "mcf":
                                    bill.UomKey = "therms";
                                    bill.TotalServiceUse = bill.TotalServiceUse * convertMcf2Therm;
                                    break;
                            }

                            break;

                        case "water":

                            switch (bill.UomKey.ToLower())
                            {
                                case "ccf":
                                    bill.UomKey = "gallons";
                                    bill.TotalServiceUse = bill.TotalServiceUse * convertCcf2Gal;
                                    break;

                            }

                            break;

                    }

                }

            }

            return conversionInfo;
        }


        /// <summary>
        /// Create 12 months of calendar bills per commodity using actual bills and energy model.
        /// NOTE that a bills endDate is NOT inclusive of date range of StartDate to EndDate of a bill -->  [SD, ED) ----- This could change however.
        /// </summary>
        /// <param name="valid"></param>
        /// <param name="actualBills"></param>
        /// <param name="commodities"></param>
        /// <param name="em"></param>
        /// <returns></returns>
        // ReSharper disable once RedundantAssignment
        public List<Bill> CreateComputedBills(ref bool valid, List<Bill> actualBills, List<CommodityType> commodities, EnergyModelResult em)
        {
            valid = true;

            var computedBills = new List<Bill>();

            // iterate commodities that have two or more bills 
            foreach (var commodity in commodities)
            {
                var daysInYear = 365;
                var daysInMonth = new[] { 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 };
                var readYear = new int[MonthsOfComputedBills];
                var monthlyLoad = new decimal[MonthsOfComputedBills];
                var loadForDayOfMonth = new decimal[MonthsOfComputedBills];
                decimal annualCorrectionFactor;

                var dailyUsages = InitializeArray<DailyUsage>(366);

                if (actualBills != null)
                {
                    // get max 12 most recent bills for the commodity, sorted descending by end date.  May only get a few.
                    var bills =
                        actualBills.FindAll(p => p.CommodityKey == MapToCommodityName(commodity))
                            .Distinct()
                            .OrderByDescending(d => d.EndDate.AddDays(-1))
                            .Take(12)
                            .ToList();

                    SetComputedBillYears(readYear, bills);
                    LeapYearAdjustment(ref daysInYear, daysInMonth, readYear);

                    //spread usage information for each actual monthly bill onto list of DailyUsage for the year
                    foreach (var bill in bills)
                    {
                        if (bill.BillDays <= 0)
                        {
                            continue;
                        }

                        var lastDayOfYearOfBill = bill.EndDate.AddDays(-1).DayOfYear;

                        if (lastDayOfYearOfBill > daysInYear)
                        {
                            lastDayOfYearOfBill = daysInYear;
                        }

                        var firstDayOfYearOfBill = bill.StartDate.DayOfYear;

                        var dailyUse = bill.TotalServiceUse / bill.BillDays;

                        for (var dayOfYear = lastDayOfYearOfBill; dayOfYear > firstDayOfYearOfBill; dayOfYear--)
                        {
                            var actualDayOfYear = dayOfYear;

                            if (dayOfYear <= 0)
                            {
                                actualDayOfYear = daysInYear + dayOfYear;
                                //the day of year is negative here when rollover end of year, this makes adjustment 
                            }

                            // Subtract one to handle the zero based list.
                            dailyUsages[actualDayOfYear - 1].Usage = dailyUse;

                            // each day gets the appropriate fraction of the total;
                            dailyUsages[actualDayOfYear - 1].IsDerivedFromActualBill = true;

                        }
                    }
                }
                else
                {

                    SetComputedBillYearsForNoBill(readYear);
                    LeapYearAdjustment(ref daysInYear, daysInMonth, readYear);
                }

                //Get modeled monthly load using static appliance load shapes and modeled energy output
                GetModeledMonthlyLoad(monthlyLoad, commodity, em);
                GetModeledMonthlyLoadModified(loadForDayOfMonth, monthlyLoad, daysInMonth);

                //Pass 1: Fill calendar days in computed year with modeled usage
                var totalModeledUsage = 0m;
                var totalUsage = 0m;
                var dayInYear = 0;

                for (var m = 0; m < 12; m++)
                {
                    var days = daysInMonth[m];

                    for (var d = 0; d < days; d++)
                    {
                        var modeledUsage = loadForDayOfMonth[m];
                        dailyUsages[dayInYear].ModeledUsage = modeledUsage;

                        if (dailyUsages[dayInYear].IsDerivedFromActualBill)
                        {
                            totalModeledUsage = totalModeledUsage + modeledUsage;
                            totalUsage = totalUsage + dailyUsages[dayInYear].Usage;
                        }
                        dayInYear++;
                    }
                }

                //Determine annual correction factor as ratio of usage over modeled usage
                if (totalModeledUsage > 0m)
                {
                    annualCorrectionFactor = totalUsage / totalModeledUsage;
                }
                else
                {
                    //modeled usage is insufficient, must abort
                    if (actualBills != null)
                    {
                        valid = false;
                        break;
                    }

                    annualCorrectionFactor = 1m;
                }

                //Pass 2: Create the computed bills!
                dayInYear = 0;
                var totalComputedUsage = 0m;

                for (var m = 0; m < 12; m++)
                {
                    var totalMonthActualUsage = 0m;
                    var daysWithData = 0;
                    var daysWithoutData = 0;
                    var days = daysInMonth[m];

                    for (var d = 0; d < days; d++)
                    {
                        if (dailyUsages[dayInYear].IsDerivedFromActualBill)
                        {
                            totalMonthActualUsage = totalMonthActualUsage + dailyUsages[dayInYear].Usage;
                            daysWithData++;
                        }
                        else
                        {
                            daysWithoutData++;
                        }
                        dayInYear++;
                    }

                    //for the month, adjustments based on how much actual daya we initially have per calendar month
                    if (daysWithoutData == 0)
                    {
                        //month is full of actual usage
                    }
                    else if (daysWithData > 7)
                    {
                        //extrapolate total for month from actual usage
                        var scaleFactor = (decimal)days / daysWithData;
                        totalMonthActualUsage = totalMonthActualUsage * scaleFactor;
                    }
                    else
                    {
                        //very little actual usage in month, adjust using modeled load for the month
                        totalMonthActualUsage = days * loadForDayOfMonth[m] * annualCorrectionFactor;
                    }

                    var startDate = new DateTime(readYear[m], m + 1, 1);
                    var endDate = new DateTime(readYear[m], m + 1, daysInMonth[m]).AddDays(1); //end date is not inclusive (this could change), so end date will be first of next month

                    //ADD THE COMPUTED BILL HERE
                    var computedBill = new Bill
                    {
                        TotalServiceUse = totalMonthActualUsage,
                        CommodityKey = MapToCommodityName(commodity),
                        DaysInMonth = daysInMonth[m],
                        BillDays = daysInMonth[m],
                        StartDate = startDate,
                        EndDate = endDate,
                        Month = m + 1,
                        Year = readYear[m],
                        TotalServiceCost = 0.0m,
                        UomKey = MapToUomByCommodity(commodity)
                    };

                    computedBills.Add(computedBill);

                    totalComputedUsage = totalComputedUsage + computedBill.TotalServiceUse;
                }
            }

            //if any computed bills have negative usage, we must invalidate the computed bills 
            if (computedBills.Exists(p => p.TotalServiceUse < 0.0m))
            {
                valid = false;
            }

            return computedBills;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="loadForDayOfMonth"></param>
        /// <param name="monthlyLoad"></param>
        /// <param name="daysInMonth"></param>
        private void GetModeledMonthlyLoadModified(IList<decimal> loadForDayOfMonth, IReadOnlyList<decimal> monthlyLoad, IReadOnlyList<int> daysInMonth)
        {
            for (var m = 0; m < 12; m++)
            {
                loadForDayOfMonth[m] = monthlyLoad[m] / daysInMonth[m];
            }
        }

        /// <summary>
        /// This is monthly load based on appliance and end use, using the modeled usage as input as well.
        /// </summary>
        private void GetModeledMonthlyLoad(decimal[] monthlyLoad, CommodityType commodity, EnergyModelResult em)
        {
            foreach (var a in em.Appliances)
            {
                for (var m = 0; m < 12; m++)
                {
                    var applianceName = a.Name.ToLower();
                    var monthlyFraction = _config.ApplianceMonthlyLoadFactors[applianceName][m] / _config.ApplianceAnnualLoadFactors[applianceName];

                    var energyUse = a.EnergyUses.Find(p => p.Commodity == commodity);

                    if (energyUse != null)
                    {
                        var energyOutput = energyUse.Quantity * monthlyFraction;
                        monthlyLoad[m] = monthlyLoad[m] + energyOutput;
                    }

                }
            }

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="readYear"></param>
        /// <param name="bills"></param>
        private void SetComputedBillYears(int[] readYear, IReadOnlyList<Bill> bills)
        {
            if (readYear == null)
            {
                throw new ArgumentNullException(nameof(readYear));
            }
            var actualMonth = 0;

            // initialize the year array with rolling window into year
            var recentYear = bills[0].EndDate.AddDays(-1).Year;
            var recentMonth = bills[0].EndDate.AddDays(-1).Month;
            for (var i = 0; i < MonthsOfComputedBills; i++)
            {
                if (i > (recentMonth - 1))
                {
                    readYear[i] = recentYear - 1;
                }
                else
                {
                    readYear[i] = recentYear;
                }
            }

            // set actual year for actual bills that we have
            var counter = 0;
            foreach (var bill in bills)
            {
                if (counter == 0)
                {
                    actualMonth = bill.EndDate.AddDays(-1).Month;
                }
                else
                {
                    actualMonth = actualMonth - 1;
                }

                if (actualMonth == 0)
                {
                    actualMonth = 12;
                }

                var billYear = bill.EndDate.AddDays(-1).Year;
                readYear[actualMonth - 1] = billYear;

                counter = counter + 1;
            }

        }

        /// <summary>
        /// 15.12 TFS 1610 - set computed bill years for no bill
        /// </summary>

        /// <param name="readYear"></param>
        private void SetComputedBillYearsForNoBill(IList<int> readYear)
        {
            // initialize the year array with rolling window into year
            var recentYear = DateTime.Today.Year;
            var recentMonth = DateTime.Today.Month;

            for (var i = 0; i < MonthsOfComputedBills; i++)
            {
                if (i > recentMonth - 1)
                {
                    readYear[i] = recentYear - 1;
                }
                else
                {
                    readYear[i] = recentYear;
                }
            }

        }

        /// <summary>
        /// 15.12 TFS 1610 - adjust leap year for no bill
        /// </summary>
        /// <param name="daysInYear"></param>
        /// <param name="daysInMonth"></param>
        /// <param name="readYear"></param>
        private void LeapYearAdjustment(ref int daysInYear, IList<int> daysInMonth, IReadOnlyList<int> readYear)
        {
            if (DateTime.IsLeapYear(readYear[1]))        // if we have a february in the leap year we need to adjust
            {
                daysInYear = 366;
                daysInMonth[1] = 29;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        private IEnumerable<CommodityType> GetAllCommodities()
        {
            return new List<CommodityType> {CommodityType.Electric, CommodityType.Gas, CommodityType.Water};
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="actualBills"></param>
        /// <returns></returns>
        private List<CommodityType> GetValidCommodities(List<Bill> actualBills)
        {
            var commodities = new List<CommodityType>();

            if (actualBills.Count(p => p.CommodityKey == "electric") >= MinimumBillsCountToTriggerComputedBills)
            {
                commodities.Add(CommodityType.Electric);
            }

            if (actualBills.Count(p => p.CommodityKey == "gas") >= MinimumBillsCountToTriggerComputedBills)
            {
                commodities.Add(CommodityType.Gas);
            }

            if (actualBills.Count(p => p.CommodityKey == "water") >= MinimumBillsCountToTriggerComputedBills)
            {
                commodities.Add(CommodityType.Water);
            }

            return commodities;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="existingCommodities"></param>
        /// <returns></returns>
        private List<CommodityType> GetCommoditiesWithoutBills(List<CommodityType> existingCommodities)
        {
            var commodities = new List<CommodityType>();

            var electricExists = existingCommodities.Exists(p => p == CommodityType.Electric);
            var gasExists = existingCommodities.Exists(p => p == CommodityType.Gas);
            var waterExists = existingCommodities.Exists(p => p == CommodityType.Water);

            if (!electricExists)
            {
                commodities.Add(CommodityType.Electric);
            }

            if (!gasExists)
            {
                commodities.Add(CommodityType.Gas);
            }

            if (!waterExists)
            {
                commodities.Add(CommodityType.Water);
            }

            return commodities;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="commodity"></param>
        /// <returns></returns>
        private string MapToCommodityName(CommodityType commodity)
        {
            string name;

            switch (commodity)
            {
                case CommodityType.Electric:
                    name = "electric";
                    break;

                case CommodityType.Gas:
                    name = "gas";
                    break;

                case CommodityType.Water:
                    name = "water";
                    break;

                default:
                    throw new Exception("Commodity not matched for commodity name mapping");
            }

            return name;

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="commodity"></param>
        /// <returns></returns>
        private string MapToUomByCommodity(CommodityType commodity)
        {
            string name;

            switch (commodity)
            {
                case CommodityType.Electric:
                    name = "kwh";
                    break;

                case CommodityType.Gas:
                    name = "therms";
                    break;

                case CommodityType.Water:
                    name = "gal";
                    break;

                default:
                    throw new Exception("Commodity not matched for uom mapping");

            }

            return name;

        }

        /// <summary>
        /// Extension method for array initialization.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="length"></param>
        /// <returns></returns>
        T[] InitializeArray<T>(int length) where T : new()
        {
            var array = new T[length];
            for (var i = 0; i < length; ++i)
            {
                array[i] = new T();
            }

            return array;
        }

    }

}
