﻿
namespace CE.BillDisagg
{
    public class BillDisaggManagerFactory
    {
        public BillDisaggManagerFactory()
        {

        }

        /// <summary>
        /// Create BillDisaggManager that implements IBillDisagg. 
        /// </summary>
        /// <returns></returns>
        public IBillDisagg CreateBillDisaggManager(int clientId)
        {
            return (new BillDisaggManager(clientId));
        }


        public IBillDisagg CreateBillDisaggManager(int clientId, string connStringInsightsDW, string connStringInsightsMetadata, string connStringInsights)
        {
            return (new BillDisaggManager(clientId, connStringInsightsDW, connStringInsightsMetadata, connStringInsights));
        }

    }

}
