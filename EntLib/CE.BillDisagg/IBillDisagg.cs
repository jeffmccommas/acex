﻿using System;
using System.Collections.Generic;
using CE.EnergyModel;
using CE.EnergyModel.Enums;

namespace CE.BillDisagg
{
    public interface IBillDisagg
    {
        BillDisaggResult ExecuteBillDisagg(string customerId, string accountId, string premiseId, DateTime startDate, DateTime endDate);
        BillDisaggResult ExecuteBillDisagg(string customerId, string accountId, string premiseId, bool isBusiness);
        BillDisaggResult ExecuteBillDisagg(string customerId, string accountId, string premiseId);
        BillDisaggResult ExecuteBillDisagg(DisaggInputParams inputParams);
        BillDisaggResult GenerateBillDisaggFromPreviousResults(BillDisaggResult bd, EnergyModelResult em);
        EnergyModelResult UpdateEnergyModelWithBillDisagg(DisaggInputParams inputParams,
            List<CommodityType> commodities);
    }

}
