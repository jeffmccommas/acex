﻿using System;

namespace CE.BillDisagg
{
    public class Bill
    {
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public string CommodityKey { get; set; }
        public string UomKey { get; set; }
        public decimal TotalServiceUse { get; set; }
        public decimal TotalServiceCost { get; set; }
        public int BillDays { get; set; }

        public int Month { get; set; }          //optionally assigned
        public int Year { get; set; }           //optionally assigned
        public int DaysInMonth { get; set; }    //optionally assigned


        public Bill()
        {

        }

    }

    /// <summary>
    /// This is used to help convert from cf to therms, mcf to therms, ccf to gallons, etc.
    /// </summary>
    public class BillConversionInfo
    {
        public string CommodityKey { get; set; }
        public string UomKey { get; set; }                              //original key
        public decimal ConversionFactor { get; set; }
        public CE.EnergyModel.Enums.UnitOfMeasureType Uom { get; set; } //original uomtype

    }

}
