﻿using System;
using System.Collections.Generic;
using System.Data;
using CE.BillDisagg.Enums;
using CE.EnergyModel;
using CE.EnergyModel.Enums;

namespace CE.BillDisagg
{
    /// <summary>
    /// Results for bill disagg.  The End Use Collection shall be only those that are category billDisagg.
    /// </summary>
    public class BillDisaggResult
    {
        public string CustomerId { get; set; }
        public string AccountId { get; set; }
        public string PremiseId { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public List<DisaggStatus> DisaggStatuses { get; set; }
        public DisaggPeriodType DisaggPeriod { get; set; }

        public EndUseCollection EndUses { get; set; }
        public ApplianceCollection Appliances { get; set; }
        public DataTable FlatResultTable { get; set; }

        public List<Bill> ActualBills { get; set; }
        public List<Bill> ComputedBills { get; set; }


        public BillDisaggResult()
        {
            DisaggPeriod = DisaggPeriodType.Annual;
        }

    }
    public class DisaggStatus
    {
        public DisaggStatusType Status { get; set; }
        public CommodityType Commodity { get; set; }
    }

}
