//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace CE.InsightsBulk.Model
{
    using System;
    using System.Collections.Generic;
    
    public partial class ActionItem
    {
        public int ClientId { get; set; }
        public string CustomerId { get; set; }
        public string AccountID { get; set; }
        public string PremiseId { get; set; }
        public int StatusId { get; set; }
        public System.DateTime StatusDate { get; set; }
        public string ActionKey { get; set; }
        public string SubActionKey { get; set; }
        public string ActionData { get; set; }
        public string ActionDataValue { get; set; }
        public string TrackingID { get; set; }
        public System.DateTime TrackingDate { get; set; }
        public int SourceId { get; set; }
    }
}
