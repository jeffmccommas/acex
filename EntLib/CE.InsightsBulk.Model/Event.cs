//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace CE.InsightsBulk.Model
{
    using System;
    using System.Collections.Generic;
    
    public partial class Event
    {
        public int ClientID { get; set; }
        public string CustomerID { get; set; }
        public string AccountID { get; set; }
        public string PremiseID { get; set; }
        public string EventClass { get; set; }
        public string EventAction { get; set; }
        public System.DateTime EventDateTime { get; set; }
        public string EventPrimaryInfo { get; set; }
        public string EventAdditionalInfo { get; set; }
        public string EventChannel { get; set; }
        public string EventOrigin { get; set; }
        public int EventOrder { get; set; }
        public string SessionID { get; set; }
    }
}
