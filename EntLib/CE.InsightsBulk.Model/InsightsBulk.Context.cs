﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace CE.InsightsBulk.Model
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Infrastructure;
    
    public partial class InsightsBulkEntities : DbContext
    {
        public InsightsBulkEntities()
            : base("name=InsightsBulkEntities")
        {
        }
    
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            throw new UnintentionalCodeFirstException();
        }
    
        public virtual DbSet<Customer> Customers { get; set; }
        public virtual DbSet<Event> Events { get; set; }
        public virtual DbSet<Premise> Premises { get; set; }
        public virtual DbSet<Billing> Billings { get; set; }
        public virtual DbSet<PremiseAttribute> PremiseAttributes { get; set; }
        public virtual DbSet<ActionItem> ActionItems { get; set; }
    }
}
