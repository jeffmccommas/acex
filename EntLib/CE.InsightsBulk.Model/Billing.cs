//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace CE.InsightsBulk.Model
{
    using System;
    using System.Collections.Generic;
    
    public partial class Billing
    {
        public int ClientId { get; set; }
        public string CustomerId { get; set; }
        public string AccountId { get; set; }
        public string PremiseId { get; set; }
        public System.DateTime BillStartDate { get; set; }
        public System.DateTime BillEndDate { get; set; }
        public int BillDays { get; set; }
        public decimal TotalUnits { get; set; }
        public decimal TotalCost { get; set; }
        public int CommodityId { get; set; }
        public int BillPeriodTypeId { get; set; }
        public string BillCycleScheduleId { get; set; }
        public int UOMId { get; set; }
        public int SourceId { get; set; }
        public string TrackingId { get; set; }
        public System.DateTime TrackingDate { get; set; }
        public int BillingRowId { get; set; }
        public string ServicePointId { get; set; }
        public string ServiceContractId { get; set; }
        public Nullable<System.DateTime> DueDate { get; set; }
        public Nullable<System.DateTime> AMIStartDate { get; set; }
        public Nullable<System.DateTime> AMIEndDate { get; set; }
        public string MeterId { get; set; }
        public string RateClass { get; set; }
        public string MeterType { get; set; }
        public string ReplacedMeterId { get; set; }
        public string ReadQuality { get; set; }
        public Nullable<System.DateTime> ReadDate { get; set; }
    }
}
