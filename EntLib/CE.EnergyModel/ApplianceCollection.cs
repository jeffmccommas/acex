﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace CE.EnergyModel
{
    public class ApplianceCollection : Collection<Appliance>
    {
        public bool IsStandAlone { get; set; }

        public ApplianceCollection()
            : base(new List<Appliance>())
        {

        }

        public ApplianceCollection(List<Appliance> appliances)
            : base(appliances)
        {

        }

        public Appliance Find(Predicate<Appliance> match)
        {
            List<Appliance> items = (List<Appliance>)Items;
            return (items.Find(match));
        }

        public List<string> GetStandAloneApplianceNames()
        {
            List<Appliance> items = (List<Appliance>)Items;
            List<string> output = new List<string>();

            var findList = items.FindAll(p => p.EnergyUses.Count == 0);
            if (findList != null)
            {
                foreach (var x in findList)
                {
                    output.Add(x.Name.ToLower());   //make the appliance names lower case when extracting
                }
            }

            return (output);
        }

        /// <summary>
        /// Sum up all commodities with one pass through all appliance energy uses.  Index the decimal[] using CommodityType.
        /// </summary>
        /// <returns></returns>
        public decimal[] GetCommoditySums()
        {
            List<Appliance> items = (List<Appliance>)Items;
            decimal[] res = { 0m, 0m, 0m, 0m, 0m, 0m, 0m, 0m, 0m, 0m, 0m, 0m }; // indexed by commodity enum

            if (items != null)
            {
                foreach (var a in items)
                {
                    foreach (var e in a.EnergyUses)
                    {
                        // do not sum unit of measure output that is BTU, or if it is wood (since wood must be btu)
                        if (e.UnitOfMeasure != Enums.UnitOfMeasureType.BTU || e.Commodity == Enums.CommodityType.Wood)
                        {
                            res[(int)e.Commodity] = res[(int)e.Commodity] + e.Quantity;
                        }
                    }

                }
            }

            return (res);
        }

        /// <summary>
        /// Get Sum of energy use quantity of all appliances given the commodity  
        /// </summary>
        /// <param name="c"></param>
        /// <returns></returns>
        public decimal GetCommoditySum(Enums.CommodityType c)
        {
            decimal output = 0m;
            List<Appliance> items = (List<Appliance>)Items;

            if (items != null)
            {
                foreach (var a in items)
                {
                    foreach (var e in a.EnergyUses)
                    {
                        // sum all non-provided commodities (typicall everything)
                        if (e.Commodity == c && e.EvaluationContext != null && e.EvaluationContext.EvaluationType != Enums.EvaluationType.Provided)
                        {
                            output = output + e.Quantity;
                        }
                    }

                }
            }

            return (output);
        }

        /// <summary>
        /// Remove appliance energy uses that are extraneous. Use during bill disagg to keep only Elec, Gas, Water uses.
        /// </summary>
        public void RemoveEmpty()
        {
            List<Appliance> items = (List<Appliance>)Items;
            items.RemoveAll(p => p.EnergyUses.Count == 0);
        }


        /// <summary>
        /// Remove end use energy uses that are extraneous. Use during bill disagg to keep only Elec, Gas, Water uses.
        /// Also remove extraneous btu.
        /// </summary>
        public void RemoveAddOnEnergyUses()
        {
            List<Appliance> items = (List<Appliance>)Items;

            for (int i = 0; i < this.Count; i++)
            {
                items[i].EnergyUses.RemoveAll(p => p.Commodity == Enums.CommodityType.Unspecified
                                                || p.Commodity == Enums.CommodityType.Oil
                                                || p.Commodity == Enums.CommodityType.Propane
                                                || p.Commodity == Enums.CommodityType.Wood
                                                || p.Commodity == Enums.CommodityType.Coal
                                                || p.Commodity == Enums.CommodityType.HotWater
                                                || p.Commodity == Enums.CommodityType.OtherWater
                                                || p.Commodity == Enums.CommodityType.ThermoFlow
                                                || p.UnitOfMeasure == Enums.UnitOfMeasureType.BTU);
            }

            //Unspecified,
            //Electric,
            //Gas,
            //Water,
            //Oil,
            //Propane,
            //Wood,
            //Coal,
            //HotWater,
            //OtherWater,
            //ThermoFlow

        }

        /// <summary>
        /// Set the original quantities for the energy uses of all appliances.
        /// </summary>
        public void SetOriginalQuantities()
        {
            List<Appliance> items = (List<Appliance>)Items;

            for (int i = 0; i < this.Count; i++)
            {
                items[i].EnergyUses.SetOriginalQuantities();
            }

        }

        /// <summary>
        /// Reset the Quantities to the Original Quantities for the energy uses of all appliances.
        /// </summary>
        public void ResetQuantities()
        {
            List<Appliance> items = (List<Appliance>)Items;

            for (int i = 0; i < this.Count; i++)
            {
                items[i].EnergyUses.ResetQuantities();
            }

        }


        public bool Exists(Predicate<Appliance> match)
        {
            List<Appliance> items = (List<Appliance>)Items;
            return (items.Exists(match));
        }

        public void AddRange(ApplianceCollection appliances)
        {
            List<Appliance> items = (List<Appliance>)Items;
            items.AddRange(appliances);
        }

        public void Sort(Comparison<Appliance> compare)
        {
            List<Appliance> items = (List<Appliance>)Items;
            items.Sort(compare);
        }


        public bool DoesNegativeEnergyUseExist()
        {
            bool negEnergyExists = false;
            List<Appliance> items = (List<Appliance>)Items;

            if (items != null)
            {
                foreach (var a in items)
                {
                    foreach (var e in a.EnergyUses)
                    {
                        // sum all non-provided commodities (typicall everything)
                        if (e.EvaluationContext != null && e.EvaluationContext.EvaluationType != Enums.EvaluationType.Provided)
                        {
                            if (e.Quantity < 0m || e.Cost < 0m)
                            {
                                negEnergyExists = true;
                                break;
                            }
                        }
                    }

                    if (negEnergyExists)
                    {
                        break;
                    }
                }
            }

            return (negEnergyExists);
        }


    }

}