﻿using System.Collections.Generic;

namespace CE.EnergyModel
{
    /// <summary>
    /// EndUse.
    /// </summary>
    public class EndUse
    {
        public string Name { get; set; }
        public string Key { get; set; }
        public string Category { get; set; }
        public List<string> ApplianceNames { get; set; }
        public EnergyUseCollection EnergyUses { get; set; }
        public int Confidence { get; set; }

        public EndUse()
        {
            EnergyUses = new EnergyUseCollection();
        }

        public EndUse Clone()
        {
            var eu = new EndUse();

            eu.Name = this.Name;
            eu.Key = this.Key;
            eu.Category = this.Category;
            eu.Confidence = this.Confidence;
            foreach (var e in this.EnergyUses)
            {
                var newEnegyUse = e.Clone();
                eu.EnergyUses.Add(newEnegyUse);
            }

            if (this.ApplianceNames != null)
            {
                eu.ApplianceNames = new List<string>();
                foreach (var item in this.ApplianceNames)
                {
                    eu.ApplianceNames.Add(item);
                }
            }

            return (eu);
        }

    }
}
