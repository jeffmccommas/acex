﻿
using CE.EnergyModel.Enums;

namespace CE.EnergyModel
{
    /// <summary>
    /// Energy Model Result.
    /// </summary>
    public class EnergyModelResult
    {
        public EnergyUseCollection TotalEnergyUses { get; set; }

        public EndUseCollection EndUses { get; set; }

        public ApplianceCollection Appliances { get; set; }

        public RegionParams RegionParams { get; set; }      //copied to output

        public IDParams IDParams { get; set; }              //copied to output, there may not be any customer parameters if not provided initially

        public int Confidence { get; set; }                 //average confidence of overall calculations

        public ModelType ModelType { get; set; }            //the model is Residential or Business (determined automatically from profile attribute named PremiseType)

        public int AnnualHDD { get; set; }                  //copied to output

        public int AnnualCDD { get; set; }                  //copied to output

        public EnergyHighlightsCollection Highlights { get; set; }

        public MetricCollection Metrics { get; set; }

        public StatusList StatusList { get; set; }

        public ProfileAttributeCollection ProfileAttributes { get; set; }       //optional in the result, only filled if an argument ReturnProfileAttributes was true on input

        public EnergyModelResult()
        {
            TotalEnergyUses = new EnergyUseCollection();
            EndUses = new EndUseCollection();
            Appliances = new ApplianceCollection();
            RegionParams = new RegionParams();
            Metrics = new MetricCollection();
            StatusList = new StatusList();
            Highlights = new EnergyHighlightsCollection();
            ProfileAttributes = null;
        }


    }
}
