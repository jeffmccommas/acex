﻿using System;

namespace CE.EnergyModel
{
    /// <summary>
    /// Partial.
    /// </summary>
    public partial class ApplianceEvaluator
    {
        decimal baseheatgain { get; set; }
        decimal gainpersqft { get; set; }
        decimal gainperperson { get; set; }
        decimal slabfactor { get; set; }
        decimal seer { get; set; }
        decimal sealleaksfactor { get; set; }
        decimal usefanmore { get; set; }
        int centralacage { get; set; }

        private void UpdateCentralACInternalSetup(ProfileAttributeCollection pa, EnergyModelConfiguration config)
        {
            string basement = pa.Find(p => p.Key == "house.basementstyle").OptionKey;

            switch (basement)
            {
                case "house.basementstyle.conditioned":
                    slabfactor = 0.9m;
                    break;
                case "house.basementstyle.unconditioned":
                    slabfactor = 1m;
                    break;
                case "house.basementstyle.slabongrade":
                    slabfactor = 1m;
                    break;
                case "house.basementstyle.unventedcrawl":
                    slabfactor = 1m;
                    break;
                case "house.basementstyle.ventedcrawl":
                    slabfactor = 1m;
                    break;
                default:    // nothing found
                    slabfactor = 1m;
                    break;
            }

            string efficiency = pa.Find(p => p.Key == "centralac.efficiency").OptionKey;
            int age = centralacage;

            switch (efficiency)
            {
                //efficiency
                case "centralac.efficiency.excellent":
                case "centralac.efficiency.good":
                    switch (age)
                    {
                        case 0:
                            seer = 15;
                            break;
                        case 1:
                        case 2:
                        case 3:
                        case 4:
                        case 5:
                            seer = 14;
                            break;
                        case 6:
                        case 7:
                        case 8:
                        case 9:
                        case 10:
                            seer = 14;
                            break;
                        case 11:
                        case 12:
                        case 13:
                        case 14:
                        case 15:
                            seer = 13;
                            break;
                        default:
                            if (age > 15)
                            {
                                seer = 12;
                            }
                            break;
                    }
                    break;
                case "centralac.efficiency.average":
                case "centralac.efficiency.poor":
                    switch (age)
                    {
                        case 0:
                            seer = 11;
                            break;
                        case 1:
                        case 2:
                        case 3:
                        case 4:
                        case 5:
                            seer = 11;
                            break;
                        case 6:
                        case 7:
                        case 8:
                        case 9:
                        case 10:
                            seer = 11;
                            break;
                        case 11:
                        case 12:
                        case 13:
                        case 14:
                        case 15:
                            seer = 11;
                            break;
                        default:
                            if (age > 15)
                            {
                                seer = 11;
                            }
                            break;
                    }
                    break;
            }   // if seer = 0, there is a problem

            //override seer here when applicable
            if (pa.Exists(p => p.Key == "factoroverride.seer")) seer = Convert.ToDecimal(pa.Find(p => p.Key == "factoroverride.seer").OptionValue);

            string sealleaks = pa.Find(p => p.Key == "centralac.sealleaks").OptionKey;

            if (sealleaks == "centralac.sealleaks.yes")
            {
                sealleaksfactor = config.GetCalculationFactor<decimal>("centralac.sealleaksfactor_sealed");
            }
            else
            {
                sealleaksfactor = config.GetCalculationFactor<decimal>("centralac.sealleaksfactor_unsealed");
            }

        }

        private decimal GetCentralACTempDiff(ProfileAttributeCollection pa, EnergyModelConfiguration config)
        {
            decimal evtemp = 0;
            decimal daytemp = 0;
            decimal nitetemp = 0;
            short i = 0;
            short j = 0;
            short days = 0;
            decimal tempdiff_hour = 0;
            decimal tempdiff_month = 0;
            decimal niteseting = 0;
            decimal evseting = 0;
            decimal dayseting = 0;
            decimal shading = 0;
            decimal low_e_window = 0;
            decimal closedrapes = 0;
            decimal areawest = 0;
            decimal areaeast = 0;
            decimal areasouth = 0;
            decimal areanorth = 0;
            decimal nval = 0;
            decimal solarload = 0;
            decimal wetbulbload = 0;
            decimal airvolumechange = 0;
            decimal nwindowsarea = 0;
            decimal shadingkoeff = 0;
            decimal[] nmonthlyload = new decimal[13];
            int houseage = 0;
            int internalheatgain = 0;
            decimal outsidehumidityratio = 0;
            decimal insidehumidityratio = 0;
            short twb = 0;
            short tdb = 0;
            short twbi = 0;
            short tdbi = 0;
            float sp = 0;

            decimal infiltrationheatgain = 0;
            decimal solarscreen = 0;
            decimal nhousetotalarea = 0;
            decimal naveragegain = 0;
            decimal nhourlygainfactor = 0;

            decimal[,] solarwinter = new decimal[5, 25];
            decimal[,] solarsummer = new decimal[5, 25];
            decimal[,] solartable = new decimal[5, 25];
            decimal[,] temptable = new decimal[14, 25];
            decimal[,] wettemptable = new decimal[14, 25];

            sp = (float)_input.WeatherInfo.StandardPressure;

            int people = (int)Convert.ToDecimal(pa.Find(p => p.Key == "house.people").OptionValue);

            int year = (int)GetSpecialDecimalFromValueOrList(pa, "house.yearbuilt", "house.yearbuiltrange");

            int age = DateTime.Now.Year - year;

            houseage = age;
            nhousetotalarea = GetSpecialDecimalFromValueOrList(pa, "house.totalarea", "house.totalarearange");

            HouseUpdateInternalOutput(pa, config);

            nwindowsarea = windowarea; // Convert.ToDecimal(pa.Find(p => p.Key == "house.windowarea").OptionValue);

            string closedrapesstring = pa.Find(p => p.Key == "house.closedrapes").OptionKey;

            if (closedrapesstring == "house.closedrapes.yes")
            {
                closedrapes = 0.07m;
            }
            else
            {
                closedrapes = 0m;
            }

            string windowslowe = pa.Find(p => p.Key == "house.windowslowe").OptionKey;

            if (windowslowe == "house.windowslowe.yes")
            {
                low_e_window = 0.1m;
            }
            else
            {
                low_e_window = 0;
            }

            shadingkoeff = Convert.ToDecimal(pa.Find(p => p.Key == "house.shadingcoefficient").OptionValue);

            baseheatgain = config.GetCalculationFactor<decimal>("centralac.averagegain_base");
            gainpersqft = config.GetCalculationFactor<decimal>("centralac.averagegain_sqft");
            gainperperson = config.GetCalculationFactor<decimal>("centralac.averagegain_person");

            decimal sensiblegain = 0;
            decimal latentgain = 0;
            sensiblegain = baseheatgain + (nhousetotalarea * gainpersqft) + gainperperson * people;
            latentgain = 68 + 0.07m * nhousetotalarea + 41 * people;
            naveragegain = sensiblegain + latentgain;

            string solarscreenanswer = pa.Find(p => p.Key == "house.solarscreen").OptionKey;

            if (solarscreenanswer == "house.solarscreen.yes")
            {
                solarscreen = 0.4m;
            }
            else
            {
                solarscreen = 0;
            }

            shading = shadingkoeff - low_e_window - closedrapes - solarscreen;
            areawest = Convert.ToDecimal(shading * 0.25m * nwindowsarea);
            areaeast = Convert.ToDecimal(shading * 0.25m * nwindowsarea);
            areanorth = Convert.ToDecimal(shading * 0.25m * nwindowsarea);
            areasouth = Convert.ToDecimal(shading * 0.25m * nwindowsarea);

            string stemp = null;
            Boolean drafty = false;
            drafty = Convert.ToBoolean(System.Convert.ToInt32(pa.Find(p => p.Key == "house.drafty").OptionValue));

            if (houseage < 10)
            {
                stemp = "centralac.airvolumechange_lt10";
            }
            else if (houseage >= 10 && houseage <= 20)
            {
                stemp = "centralac.airvolumechange_10to20";
            }
            else if (houseage > 20)
            {
                stemp = "centralac.airvolumechange_gt20";
            }

            if (drafty)
            {
                stemp += "drafty";
            }

            airvolumechange = config.GetCalculationFactor<decimal>(stemp);
            if (pa.Exists(p => p.Key == "factoroverride." + stemp)) airvolumechange = Convert.ToDecimal(pa.Find(p => p.Key == "factoroverride." + stemp).OptionValue);

            int zones = Convert.ToInt32(pa.Find(p => p.Key == "house.zones").OptionValue);
            int daylivngcooling = Convert.ToInt32(pa.Find(p => p.Key == "house.daylivngcooling").OptionValue);
            int evelivngcooling = Convert.ToInt32(pa.Find(p => p.Key == "house.evelivngcooling").OptionValue);
            int nitelivngcooling = Convert.ToInt32(pa.Find(p => p.Key == "house.nitelivngcooling").OptionValue);
            string daylivngcooloff = Convert.ToString(pa.Find(p => p.Key == "house.daylivngcooloff").OptionKey);
            string evelivngcooloff = Convert.ToString(pa.Find(p => p.Key == "house.evelivngcooloff").OptionKey);
            string nitelivngcooloff = Convert.ToString(pa.Find(p => p.Key == "house.nitelivngcooloff").OptionKey);
            int daysleepcooling = Convert.ToInt32(pa.Find(p => p.Key == "house.daysleepcooling").OptionValue);
            int evesleepcooling = Convert.ToInt32(pa.Find(p => p.Key == "house.evesleepcooling").OptionValue);
            int nitesleepcooling = Convert.ToInt32(pa.Find(p => p.Key == "house.nitesleepcooling").OptionValue);
            string daysleepcooloff = Convert.ToString(pa.Find(p => p.Key == "house.daysleepcooloff").OptionKey);
            string evesleepcooloff = Convert.ToString(pa.Find(p => p.Key == "house.evesleepcooloff").OptionKey);
            string nitesleepcooloff = Convert.ToString(pa.Find(p => p.Key == "house.nitesleepcooloff").OptionKey);

            //get average temperature
            switch (zones)
            {
                case 1:
                    daytemp = daylivngcooling;
                    evtemp = evelivngcooling;
                    nitetemp = nitelivngcooling;
                    //day time shut off
                    if (daylivngcooloff == "house.daylivngcooloff.yes")
                    {
                        dayseting = 0;
                    }
                    else
                    {
                        dayseting = 1;
                    }
                    //day time shut off
                    if (evelivngcooloff == "house.evelivngcooloff.yes")
                    {
                        evseting = 0;
                    }
                    else
                    {
                        evseting = 1;
                    }
                    //day time shut off
                    if (nitelivngcooloff == "house.nitelivngcooloff.yes")
                    {
                        niteseting = 0;
                    }
                    else
                    {
                        niteseting = 1;
                    }
                    break;
                default:
                    daytemp = (daylivngcooling + daysleepcooling) / 2;
                    evtemp = (evelivngcooling + evesleepcooling) / 2;
                    nitetemp = (nitelivngcooling + nitesleepcooling) / 2;
                    //day time shut off
                    if (daylivngcooloff == "house.daylivngcooloff.yes" && daysleepcooloff == "house.daysleepcooloff.yes")
                    {
                        dayseting = 0;
                    }
                    else if ((daylivngcooloff == "house.daylivngcooloff.yes" && daysleepcooloff == "house.daylivngcooloff.no") ||
                        (daylivngcooloff == "house.daylivngcooloff.no" && daysleepcooloff == "house.daylivngcooloff.yes"))
                    {
                        dayseting = 0.7m;
                    }
                    else
                    {
                        dayseting = 1;
                    }
                    //Eve time shut off
                    if (evelivngcooloff == "house.evelivngcooloff.yes" & evesleepcooloff == "house.evesleepcooloff.yes")
                    {
                        evseting = 0;
                    }
                    else if ((evelivngcooloff == "house.evelivngcooloff.yes" && evesleepcooloff == "house.evesleepcooloff.no") ||
                        (evelivngcooloff == "house.evelivngcooloff.no" && evesleepcooloff == "house.evesleepcooloff.yes"))
                    {
                        evseting = 0.7m;
                    }
                    else
                    {
                        evseting = 1;
                    }
                    //Nite time shut off
                    if (nitelivngcooloff == "house.nitelivngcooloff.yes" && nitesleepcooloff == "house.nitesleepcooloff.yes")
                    {
                        niteseting = 0;
                    }
                    else if ((nitelivngcooloff == "house.nitelivngcooloff.yes" && nitesleepcooloff == "house.nitesleepcooloff.no") ||
                        (nitelivngcooloff == "house.nitelivngcooloff.no" & nitesleepcooloff == "house.nitesleepcooloff.yes"))
                    {
                        niteseting = 0.7m;
                    }
                    else
                    {
                        niteseting = 1;
                    }
                    break;
            }

            //[season 0 = winter, 1 = summar][direction = n, e, s, w][hour = 1 - 24]
            for (i = 0; i < 24; i++)
            {
                solarsummer[0, i] = _input.WeatherInfo.SolarFactors[1, 0, i];   //north
                solarsummer[1, i] = _input.WeatherInfo.SolarFactors[1, 1, i];   //east
                solarsummer[2, i] = _input.WeatherInfo.SolarFactors[1, 2, i];   //south
                solarsummer[3, i] = _input.WeatherInfo.SolarFactors[1, 3, i];   //west
            }

            for (i = 0; i < 24; i++)
            {
                solarwinter[0, i] = _input.WeatherInfo.SolarFactors[0, 0, i];   //north
                solarwinter[1, i] = _input.WeatherInfo.SolarFactors[0, 1, i];   //east
                solarwinter[2, i] = _input.WeatherInfo.SolarFactors[0, 2, i];   //south
                solarwinter[3, i] = _input.WeatherInfo.SolarFactors[0, 3, i];   //west
            }

            //[type 0 = regular, 1 = wet bulb][month = 1 - 12][hour = 1 - 24]
            for (i = 0; i < 24; i++) // regular
            {
                temptable[1, i + 1] = _input.WeatherInfo.TemperatureFactors[0, 0, i];
                temptable[2, i + 1] = _input.WeatherInfo.TemperatureFactors[0, 1, i];
                temptable[3, i + 1] = _input.WeatherInfo.TemperatureFactors[0, 2, i];
                temptable[4, i + 1] = _input.WeatherInfo.TemperatureFactors[0, 3, i];
                temptable[5, i + 1] = _input.WeatherInfo.TemperatureFactors[0, 4, i];
                temptable[6, i + 1] = _input.WeatherInfo.TemperatureFactors[0, 5, i];
                temptable[7, i + 1] = _input.WeatherInfo.TemperatureFactors[0, 6, i];
                temptable[8, i + 1] = _input.WeatherInfo.TemperatureFactors[0, 7, i];
                temptable[9, i + 1] = _input.WeatherInfo.TemperatureFactors[0, 8, i];
                temptable[10, i + 1] = _input.WeatherInfo.TemperatureFactors[0, 9, i];
                temptable[11, i + 1] = _input.WeatherInfo.TemperatureFactors[0, 10, i];
                temptable[12, i + 1] = _input.WeatherInfo.TemperatureFactors[0, 11, i];
            }

            //[type 0 = regular, 1 = wet bulb][month = 1 - 12][hour = 1 - 24]
            for (i = 0; i < 24; i++) // wet
            {
                wettemptable[1, i + 1] = _input.WeatherInfo.TemperatureFactors[1, 0, i];
                wettemptable[2, i + 1] = _input.WeatherInfo.TemperatureFactors[1, 1, i];
                wettemptable[3, i + 1] = _input.WeatherInfo.TemperatureFactors[1, 2, i];
                wettemptable[4, i + 1] = _input.WeatherInfo.TemperatureFactors[1, 3, i];
                wettemptable[5, i + 1] = _input.WeatherInfo.TemperatureFactors[1, 4, i];
                wettemptable[6, i + 1] = _input.WeatherInfo.TemperatureFactors[1, 5, i];
                wettemptable[7, i + 1] = _input.WeatherInfo.TemperatureFactors[1, 6, i];
                wettemptable[8, i + 1] = _input.WeatherInfo.TemperatureFactors[1, 7, i];
                wettemptable[9, i + 1] = _input.WeatherInfo.TemperatureFactors[1, 8, i];
                wettemptable[10, i + 1] = _input.WeatherInfo.TemperatureFactors[1, 9, i];
                wettemptable[11, i + 1] = _input.WeatherInfo.TemperatureFactors[1, 10, i];
                wettemptable[12, i + 1] = _input.WeatherInfo.TemperatureFactors[1, 11, i];
            }

            for (i = 1; i <= 12; i++)
            {
                switch (i)
                {
                    case 1:
                    case 3:
                    case 5:
                    case 7:
                    case 8:
                    case 10:
                    case 12:
                        days = 31;
                        break;
                    case 2:
                        days = 28;
                        break;
                    case 4:
                    case 6:
                    case 9:
                    case 11:
                        days = 30;
                        break;
                }
                switch (i)
                {
                    case 12:
                    case 10:
                    case 11:
                    case 1:
                    case 2:
                    case 3:
                        solartable = solarwinter;
                        break;
                    case 9:
                    case 4:
                    case 5:
                    case 6:
                    case 7:
                    case 8:
                        solartable = solarsummer;
                        break;
                }
                tempdiff_hour = 0;


                for (j = 0; j <= 23; j++)
                {
                    solarload = areanorth * solartable[0, j] + areaeast * solartable[1, j] + areasouth * solartable[2, j] + areawest * solartable[3, j];

                    twb = Convert.ToInt16(wettemptable[i, j + 1]);
                    tdb = Convert.ToInt16(temptable[i, j + 1]);
                    outsidehumidityratio = GetOutsideHumidityRatio(ref twb, ref tdb, ref sp);

                    switch (j)
                    {
                        case 6:
                        case 7:
                        case 8:
                        case 9:
                        case 10:
                        case 11:
                        case 12:
                        case 13:
                        case 14:
                        case 15:
                        case 16:
                            nhourlygainfactor = 1;
                            internalheatgain = Convert.ToInt32(naveragegain * nhourlygainfactor);
                            tdbi = Convert.ToInt16(daytemp);
                            twbi = Convert.ToInt16(0.83 * tdbi + 0.38);
                            insidehumidityratio = GetInsideHumidityRatio(ref twbi, ref tdbi, ref sp);
                            infiltrationheatgain = Convert.ToDecimal(1.08m * airvolumechange * nhousetotalarea * 9 / 60 * (tdb - daytemp));
                            wetbulbload = 4840 * (outsidehumidityratio - insidehumidityratio) * airvolumechange * nhousetotalarea * 9 / 60;
                            if (wetbulbload < 0)
                                wetbulbload = 0;
                            nval = Convert.ToDecimal(dayseting * ((kfactorhouse * (tdb - daytemp)) + internalheatgain + solarload + wetbulbload + infiltrationheatgain));
                            break;
                        case 17:
                        case 18:
                        case 19:
                        case 20:
                        case 21:
                        case 22:
                            nhourlygainfactor = 1.3m;
                            internalheatgain = Convert.ToInt32(naveragegain * nhourlygainfactor);
                            tdbi = Convert.ToInt16(evtemp);
                            twbi = Convert.ToInt16(0.83 * tdbi + 0.38);
                            insidehumidityratio = GetInsideHumidityRatio(ref twbi, ref tdbi, ref sp);
                            infiltrationheatgain = Convert.ToDecimal(1.08m * airvolumechange * nhousetotalarea * 9 / 60 * (tdb - evtemp));
                            wetbulbload = 4840 * (outsidehumidityratio - insidehumidityratio) * airvolumechange * nhousetotalarea * 9 / 60;
                            if (wetbulbload < 0)
                                wetbulbload = 0;
                            nval = Convert.ToDecimal(evseting * ((kfactorhouse * (tdb - evtemp)) + internalheatgain + solarload + wetbulbload + infiltrationheatgain));
                            break;
                        case 23:
                        case 0:
                        case 1:
                        case 2:
                        case 3:
                        case 4:
                        case 5:
                            nhourlygainfactor = 0.64m;
                            internalheatgain = Convert.ToInt32(naveragegain * nhourlygainfactor);
                            tdbi = Convert.ToInt16(nitetemp);
                            twbi = Convert.ToInt16(0.83 * tdbi + 0.38);
                            insidehumidityratio = GetInsideHumidityRatio(ref twbi, ref tdbi, ref sp);
                            infiltrationheatgain = Convert.ToDecimal(1.08m * airvolumechange * nhousetotalarea * 9 / 60 * (tdb - nitetemp));
                            wetbulbload = 4840 * (outsidehumidityratio - insidehumidityratio) * airvolumechange * nhousetotalarea * 9 / 60;
                            if (wetbulbload < 0)
                                wetbulbload = 0;
                            nval = Convert.ToDecimal(niteseting * ((kfactorhouse * (tdb - nitetemp)) + internalheatgain + solarload + wetbulbload + infiltrationheatgain));
                            break;
                    }

                    if (nval > 0)
                        tempdiff_hour = tempdiff_hour + nval;

                }

                nmonthlyload[i] = Convert.ToDecimal(tempdiff_hour * days);
                tempdiff_month = tempdiff_month + tempdiff_hour * days;
                tempdiff_hour = 0;
            }

            return tempdiff_month;
        }



        private decimal GetOutsideHumidityRatio(ref short twb, ref short tdb, ref float p)
        {
            return Convert.ToDecimal(Convert.ToSingle(0.62197 * (6.112 * Math.Pow(10, (7.5 * ((twb - 32) / 1.8) / (237.7 + ((twb - 32) / 1.8)))) - (0.00066 * (1 + 0.00155 * ((twb - 32) / 1.8)) * p * 68.948 * (tdb - twb) / 1.8)) / (p * 68.948 - (6.112 * Math.Pow(10, (7.5 * (twb - 32) / 1.8 / (237.7 + (twb - 32) / 1.8))) - (0.00066 * (1 + 0.00155 * (twb - 32) / 1.8) * p * 68.948 * (tdb - twb) / 1.8)))));
        }
        private decimal GetInsideHumidityRatio(ref short twbi, ref short tdbi, ref float p)
        {
            return Convert.ToDecimal(Convert.ToSingle(0.62197 * (6.112 * Math.Pow(10, (7.5 * ((twbi - 32) / 1.8) / (237.7 + ((twbi - 32) / 1.8)))) - (0.00066 * (1 + 0.00155 * ((twbi - 32) / 1.8)) * p * 68.948 * (tdbi - twbi) / 1.8)) / (p * 68.948 - (6.112 * Math.Pow(10, (7.5 * (twbi - 32) / 1.8 / (237.7 + (twbi - 32) / 1.8))) - (0.00066 * (1 + 0.00155 * (twbi - 32) / 1.8) * p * 68.948 * (tdbi - twbi) / 1.8)))));
        }

    }
}
