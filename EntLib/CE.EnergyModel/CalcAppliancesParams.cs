﻿
namespace CE.EnergyModel
{
    public class CalcAppliancesParams
    {
        public ApplianceCollection Appliances { get; set; }
        public EnergyModelInputParams Input { get; set; }

        public CalcAppliancesParams()
        {
            Appliances = new ApplianceCollection();
        }
    }
}
