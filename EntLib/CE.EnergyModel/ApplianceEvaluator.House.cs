﻿using System;

namespace CE.EnergyModel
{
    /// <summary>
    /// Partial ApplianceEvaluator
    /// </summary>
    public partial class ApplianceEvaluator
    {
        decimal kfactorhouse { get; set; }
        decimal kfactorfloor { get; set; }
        decimal kfactorhousecal { get; set; }
        decimal kfactorfloorcal { get; set; }
        decimal housethermoflow { get; set; }
        decimal windowarea { get; set; }

        private void HouseUpdateInternalOutput(ProfileAttributeCollection pa, EnergyModelConfiguration config)
        {
            decimal u_roof = 0;
            decimal u_wall = 0;
            decimal u_window = 0;
            decimal windowpercent = 0;
            decimal build_length = 0;

            string stemp = null;

            string style = pa.Find(p => p.Key == "house.style").OptionKey;
            string test = pa.Find(p => p.Key == "house.style").OptionValue;
            decimal styleenum = Convert.ToDecimal(pa.Find(p => p.Key == "house.style").OptionValue);

            switch (style)
            {
                //style
                case "house.style.singlefamily":
                case "house.style.raisedranch":
                    stemp = "house.windowpercent_single";
                    break;
                case "house.style.townhouse":
                    stemp = "house.windowpercent_town";
                    break;
                case "house.style.duplex":
                    stemp = "house.windowpercent_duplex";
                    break;
                case "house.style.apartment":
                    stemp = "house.windowpercent_apt";
                    break;
                case "house.style.multifamily":
                    stemp = "house.windowpercent_multi";
                    break;
                case "house.style.mobile":
                    stemp = "house.windowpercent_mobile";
                    break;
                default:    // nothing found
                    stemp = "house.windowpercent_single";
                    break;
            }

            windowpercent = config.GetCalculationFactor<decimal>(stemp);
            if (pa.Exists(p => p.Key == "factoroverride." + stemp)) windowpercent = Convert.ToDecimal(pa.Find(p => p.Key == "factoroverride." + stemp).OptionValue);

            decimal totalarea = GetSpecialDecimalFromValueOrList(pa, "house.totalarea", "house.totalarearange");
            decimal levels = Convert.ToDecimal(pa.Find(p => p.Key == "house.levels").OptionValue);

            decimal roofarea = totalarea / levels;
            decimal basementwallarea = 0;
            decimal perimeter = 0;
            windowarea = totalarea * windowpercent;

            //pa.Find(p => p.Key == "house.windowarea").OptionValue = Convert.ToString(windowarea);

            build_length = Convert.ToDecimal(System.Math.Sqrt((double)roofarea));
            //if attic=finished the subtract .5 level but not less than one
            decimal wallarea = (4 * build_length * 9 * levels) - windowarea;
            if (styleenum < 3 || style == "house.style.raisedranch")
            {
                basementwallarea = Convert.ToDecimal(7.5m * 4 * build_length);
                perimeter = 4 * build_length;
            }
            else
            {
                basementwallarea = 0;
                perimeter = 0;
            }

            decimal atticinsulation = Convert.ToDecimal(pa.Find(p => p.Key == "house.atticinsulationinches").OptionValue);
            decimal addatticins = Convert.ToDecimal(pa.Find(p => p.Key == "house.addatticins").OptionValue);

            if (atticinsulation + addatticins > 0)
            {
                u_roof = 1 / (atticinsulation + addatticins);
            }
            else
            {
                u_roof = 1 / 6;
            }

            decimal wallinsulation = Convert.ToDecimal(pa.Find(p => p.Key == "house.wallinsulationinches").OptionValue);
            decimal addwallins = Convert.ToDecimal(pa.Find(p => p.Key == "house.addwallins").OptionValue);

            if ((wallinsulation + addwallins) > 0)
            {
                u_wall = 1 / (wallinsulation + addwallins);
            }
            else
            {
                u_wall = 1 / 6;
            }

            decimal windowsdblpanepct = Convert.ToDecimal(pa.Find(p => p.Key == "house.windowsdblpanepct").OptionValue);
            decimal windowsdblpaneqlt = Convert.ToDecimal(pa.Find(p => p.Key == "house.windowsdblpaneqlt").OptionValue);
            decimal windowsstormpct = Convert.ToDecimal(pa.Find(p => p.Key == "house.windowsstormpct").OptionValue);

            u_window = Convert.ToDecimal(1 / ((1 - windowsdblpanepct) + windowsdblpanepct * windowsdblpaneqlt + windowsstormpct * 0.8m));

            GetHeatLossCoeff(u_wall, wallarea, u_roof, roofarea, u_window, windowarea, pa, config);

            housethermoflow = kfactorhouse + kfactorfloor;
        }

        private void GetHeatLossCoeff(decimal uwall, decimal awall, decimal uroof, decimal aroof, decimal uwindow, decimal awindow,
            ProfileAttributeCollection pa, EnergyModelConfiguration config)
        {
            decimal uslab = 0;
            decimal nconstructionfactor = 0;
            decimal nlocationfactor = 0;
            decimal ninsulationfactor = 0;
            decimal uunventedcrawl = 0;
            decimal ufloor = 0;
            decimal ubasementwalls = 0;
            decimal ubasementfloor = 0;
            decimal afloor = 0;
            decimal pslab = 0;
            decimal aventedcrawl = 0;
            decimal aunventedcrawl = 0;
            decimal abasementwalls = 0;
            decimal abasementfloor = 0;

            string basement = pa.Find(p => p.Key == "house.basementstyle").OptionKey;
            string basementconstructiontype = pa.Find(p => p.Key == "house.basementconstructiontype").OptionKey;
            string foundinsulation = pa.Find(p => p.Key == "house.foundinsulation").OptionKey;

            if (basement != "house.basementstyle.slabongrade")
            {
                uslab = 0;
            }
            else
            {
                switch (basementconstructiontype)
                {
                    case "house.basementconstruction.heavy":
                        nconstructionfactor = 0.8m;
                        break;
                    case "house.basementconstruction.medium":
                        nconstructionfactor = 1;
                        break;
                    case "house.basementconstruction.light":
                        nconstructionfactor = 1.4m;
                        break;
                    case "house.basementconstruction.heatedslab":
                        nconstructionfactor = 2.6m;
                        break;
                    default:    // nothing found
                        nconstructionfactor = 1;
                        break;
                }

                decimal hdd = (decimal)_input.WeatherInfo.AnnualHDD;
                if (hdd < 4000)
                    nlocationfactor = 0.9m;
                else if (hdd > 4001 && hdd < 6400)
                    nlocationfactor = 1m;
                else if (hdd > 6400)
                    nlocationfactor = 1.1m;

                switch (basementconstructiontype)
                {
                    case "house.basementconstruction.heavy":
                        if (foundinsulation == "house.foundinsulation.yes")
                        {
                            ninsulationfactor = 0.76m;
                        }
                        else
                        {
                            ninsulationfactor = 1;
                        }
                        break;
                    case "house.basementconstruction.medium":
                        if (foundinsulation == "house.foundinsulation.yes")
                        {
                            ninsulationfactor = 0.58m;
                        }
                        else
                        {
                            ninsulationfactor = 1;
                        }
                        break;
                    case "house.basementconstruction.light":
                        if (foundinsulation == "house.foundinsulation.yes")
                        {
                            ninsulationfactor = 0.44m;
                        }
                        else
                        {
                            ninsulationfactor = 1;
                        }
                        break;
                    case "house.basementconstruction.heatedslab":
                        if (foundinsulation == "house.foundinsulation.yes")
                        {
                            ninsulationfactor = 0.34m;
                        }
                        else
                        {
                            ninsulationfactor = 1;
                        }
                        break;
                    default:    // nothing found
                        if (foundinsulation == "house.foundinsulation.yes")
                        {
                            ninsulationfactor = 0.58m;
                        }
                        else
                        {
                            ninsulationfactor = 1;
                        }
                        break;
                }

                decimal meanslabloss = Convert.ToDecimal(pa.Find(p => p.Key == "house.meanslabloss").OptionValue);

                uslab = meanslabloss * nconstructionfactor * nlocationfactor * ninsulationfactor;
            }

            string crawlinsulation = pa.Find(p => p.Key == "house.floorinsulation").OptionKey;
            string floorinsulation = pa.Find(p => p.Key == "house.floorinsulation").OptionKey;
            string basementinsulation = pa.Find(p => p.Key == "house.basementinsulation").OptionKey;

            if (basement != "house.basementstyle.unventedcrawl")
            {
                uunventedcrawl = 0m;
            }
            else
            {
                uunventedcrawl = 0.25m;
            }

            if (basement != "house.basementstyle.unconditioned")
            {
                ufloor = 0;
            }
            else
            {
                if (floorinsulation == "house.foundinsulation.yes")
                {
                    ufloor = 0.33m;
                }
                else
                {
                    ufloor = 0.062m;
                }
            }

            if (basement != "house.basementstyle.conditioned")
            {
                ubasementwalls = 0;
            }
            else
            {
                if (basementinsulation == "house.basementinsulation.no")
                {
                    ubasementwalls = 0.08m;
                }
                else
                {
                    ubasementwalls = 0.03m;
                }
            }

            if (basement != "house.basementstyle.conditioned")
            {
                ubasementfloor = 0;
            }
            else
            {
                ubasementfloor = 0.015m;
            }

            decimal totalarea = GetSpecialDecimalFromValueOrList(pa, "house.totalarea", "house.totalarearange");
            decimal levels = Convert.ToDecimal(pa.Find(p => p.Key == "house.levels").OptionValue);
            string style = pa.Find(p => p.Key == "house.style").OptionKey;

            afloor = totalarea / levels;
            aventedcrawl = totalarea / levels;
            aunventedcrawl = totalarea / levels;

            double basementwallheight;
            switch (basement)
            {
                case "house.basementstyle.conditioned":
                case "house.basementstyle.unconditioned":
                    basementwallheight = 8;
                    break;
                case "house.basementstyle.ventedcrawl":
                case "house.basementstyle.unventedcrawl":
                    basementwallheight = 4;
                    break;
                default:
                    basementwallheight = 0;
                    break;
            }
            abasementwalls = Convert.ToDecimal(4 * System.Math.Sqrt((double)(totalarea / levels)) * basementwallheight);

            abasementfloor = totalarea / levels;

            switch (style)
            {
                case "house.style.apartment":
                case "house.style.duplex":
                case "house.style.mobile":
                case "house.style.multifamily":
                    pslab = 0;
                    break;
                default:
                    pslab = Convert.ToDecimal(4 * System.Math.Sqrt((double)(totalarea / levels)));
                    break;
            }

            kfactorhouse = uwall * awall + uroof * aroof + uwindow * awindow + ubasementwalls * abasementwalls + ubasementfloor * abasementfloor + uunventedcrawl * aunventedcrawl;

            kfactorfloor = ufloor * afloor;

            if (kfactorhousecal == 0)
            {
                kfactorhousecal = config.GetCalculationFactor<decimal>("house.kfactorhousecal");
            }
            if (kfactorfloorcal == 0)
            {
                kfactorfloorcal = config.GetCalculationFactor<decimal>("house.kfactorfloorcal");
            }

            kfactorhouse = kfactorhouse * kfactorhousecal;
            kfactorfloor = kfactorfloor * kfactorfloorcal;

        }

    }
}
