﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace CE.EnergyModel
{
    /// <summary>
    /// Timing information.
    /// </summary>
    public class Metric
    {
        public string Name { get; set; }            //name used when unspecified type.
        public long Duration { get; set; }

        public Metric()
        {
            Name = string.Empty;
            Duration = 0;
        }

        public Metric(string name, long duration)
        {
            Name = name;
            Duration = duration;
        }


    }

    /// <summary>
    /// Metric collection.
    /// </summary>
    public class MetricCollection : Collection<Metric>
    {

        public MetricCollection()
            : base(new List<Metric>())
        {
        }

        protected new void Add(Metric item)
        {
            base.Add(item);
        }

        protected override void InsertItem(int index, Metric item)
        {
            base.InsertItem(index, item);
        }

        public Metric Find(Predicate<Metric> match)
        {
            List<Metric> items = (List<Metric>)Items;

            return (items.Find(match));
        }

    }



}
