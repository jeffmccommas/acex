﻿using System;

namespace CE.EnergyModel
{
    /// <summary>
    /// partial ApplianceEvaluator
    /// </summary>
    public partial class ApplianceEvaluator
    {

        decimal auxuse { get; set; }
        decimal oilbtufactor { get; set; }
        decimal propanebtufactor { get; set; }
        decimal basewinterheatgain { get; set; }
        decimal winterheatgainpersqft { get; set; }
        decimal winterheatgainperperson { get; set; }
        decimal secondsystemfactor { get; set; }
        string waterheaterfuel { get; set; }
        decimal heatefficiency { get; set; }
        decimal deliveryfactor { get; set; }
        decimal ignfactor { get; set; }
        decimal closedroomfactor { get; set; }
        decimal woodfactor { get; set; }
        decimal woodbtus { get; set; }
        int heatsystemage { get; set; }
        Boolean heatsystemelectrignition { get; set; }

        private void ResetHeatDefaults(ProfileAttributeCollection pa, EnergyModelConfiguration config)
        {
            auxuse = config.GetCalculationFactor<decimal>("heatsystem.auxiliaryusefactor_kwh");
            oilbtufactor = config.GetCalculationFactor<decimal>("heatsystem.oilbtufactor");
            propanebtufactor = config.GetCalculationFactor<decimal>("heatsystem.propanebtufactor");
            basewinterheatgain = config.GetCalculationFactor<decimal>("heatsystem.averagegain_base");
            winterheatgainpersqft = config.GetCalculationFactor<decimal>("heatsystem.averagegain_sqft");
            winterheatgainperperson = config.GetCalculationFactor<decimal>("heatsystem.averagegain_person");

            // reset electric ignition since if has not been answered yet
            string ignsource = pa.Find(p => p.Key == "heatsystem.electrignition").Source;
            if (ignsource == "D")
            {
                string fuel = pa.Find(p => p.Key == "heatsystem.fuel").OptionKey;
                string style = pa.Find(p => p.Key == "heatsystem.style").OptionKey;
                int age = heatsystemage;

                switch (fuel)
                {
                    case "heatsystem.fuel.electric":
                        heatsystemelectrignition = true;
                        break;
                    case "heatsystem.fuel.gas":
                    case "heatsystem.fuel.propane":
                        if (style == "heatsystem.style.freestandingstove" || style == "heatsystem.style.basicfireplace" ||
                            style == "heatsystem.style.heatcircfireplace" || style == "heatsystem.style.FireplaceInsert" ||
                            style == "heatsystem.style.waterboiler")
                        {
                            heatsystemelectrignition = true;
                        }
                        else if (style == "heatsystem.style.forcedairfurnace" && age < 5)
                        {
                            heatsystemelectrignition = true;
                        }
                        else
                        {
                            heatsystemelectrignition = false;
                        }
                        break;
                    case "heatsystem.fuel.oil":
                        heatsystemelectrignition = true;
                        break;
                    default:    // nothing found
                        heatsystemelectrignition = true;
                        break;
                }
            }

        }

        private void UpdateHeatSystemInternalSetup(ProfileAttributeCollection pa, EnergyModelConfiguration config)
        {
            decimal secondsystemservicearea = 0.0m;
            decimal secondsystemuse = 0.0m;
            string secondsystemfuel = string.Empty;
            int roomsinhouse = 0;
            decimal heatpumpcopwxcoor = 0.0m;

            ResetHeatDefaults(pa, config);

            string style = pa.Find(p => p.Key == "heatsystem.style").OptionKey;
            switch (style)
            {
                case "heatsytem.style.freestandingstove":
                case "heatsytem.style.basicfireplace":
                case "heatsytem.style.fireplaceinsert":
                    auxuse = 0.0m;
                    break;
                default:    // any other style, leave as is
                    break;
            }

            decimal count = System.Convert.ToDecimal(pa.Find(p => p.Key == "secondaryheating.count").OptionValue);
            if (count == 0)
            {
                secondsystemfuel = "secondaryheating.fuel.unspecified";
                secondsystemfactor = 0.0m;
            }
            else
            {
                secondsystemfuel = pa.Find(p => p.Key == "secondaryheating.fuel").OptionKey;
                string SecondUsage = pa.Find(p => p.Key == "secondaryheating.usage").OptionKey;
                switch (SecondUsage)
                {
                    case "secondaryheating.usage.often":
                        secondsystemuse = 0.75m;
                        break;
                    case "secondaryheating.usage.occasionally":
                        secondsystemuse = 0.5m;
                        break;
                    case "secondaryheating.usage.rarely":
                        secondsystemuse = 0.25m;
                        break;
                    default:
                        secondsystemuse = 0.0m;
                        break;
                }

                secondsystemservicearea = System.Convert.ToDecimal(pa.Find(p => p.Key == "secondaryheating.servicearea").OptionValue);

                secondsystemfactor = secondsystemuse * secondsystemservicearea;
            }

            roomsinhouse = System.Convert.ToInt32(pa.Find(p => p.Key == "house.rooms").OptionValue);
            waterheaterfuel = pa.Find(p => p.Key == "waterheater.fuel").OptionKey;
            int age = heatsystemage;
            string fuel = pa.Find(p => p.Key == "heatsystem.fuel").OptionKey;

            heatefficiency = GetHeatEfficiency(pa, age);

            if (pa.Find(p => p.Key == "heatsystem.oilnozzlereplace").OptionKey == "heatsystem.oilnozzlereplace.yes")
            {
                heatefficiency = heatefficiency + 0.03m;
            }
            if (pa.Find(p => p.Key == "heatsystem.altercirclfan").OptionKey == "heatsystem.altercirclfan.yes")
            {
                heatefficiency = heatefficiency + 0.02m;
            }
            if (pa.Find(p => p.Key == "heatsystem.replaceburner").OptionKey == "heatsystem.replaceburner.yes")
            {
                heatefficiency = heatefficiency + 0.03m;
            }

            if (pa.Find(p => p.Key == "heatsystem.aquastatmodified").OptionKey == "heatsystem.aquastatmodified.yes")
            {
                heatefficiency = heatefficiency * 1.07m;
            }

            if (style == "heatsystem.style.airsourceheatpump" || style == "heatsystem.style.airsourceheatpumpgasbackup" ||
                style == "heatsystem.style.airsourceheatpumppropanebackup" || style == "heatsystem.style.airsourceheatpumpoilbackup")
            {
                decimal a = 0;
                decimal b = 0;
                decimal c = 0;
                decimal d = 0;
                int HeatDesignTemp = 0;
                HeatDesignTemp = _input.WeatherInfo.HeatDesignTemp;

                a = heatefficiency < 2.49m ? 0.1392m : 0.1041m;
                b = heatefficiency < 2.49m ? -0.00846m : -0.008862m;
                c = heatefficiency < 2.49m ? -0.0001074m : -0.0001153m;
                d = heatefficiency < 2.49m ? 0.077816m : 0.096144m;

                heatpumpcopwxcoor = 1 - (a + b * HeatDesignTemp + c * Convert.ToDecimal(Math.Pow(HeatDesignTemp, 2)) + d * heatefficiency);
                heatefficiency = heatefficiency * heatpumpcopwxcoor;
            }

            deliveryfactor = GetDeliveryFactor(pa, config);

            if (pa.Find(p => p.Key == "heatsystem.pipesinsulated").OptionKey == "heatsystem.pipesinsulated.yes")
            {
                deliveryfactor = deliveryfactor + 0.05m;
            }

            //system has electronic ignition
            if (heatsystemelectrignition == true)
            {
                ignfactor = 0;
            }
            else
            {
                switch (fuel)
                {
                    case "heatsystem.fuel.gas":
                    case "heatsystem.fuel.gasandwood":
                    case "heatsystem.fuel.propane":
                        ignfactor = config.GetCalculationFactor<decimal>("heatsystem.ignfactor") * -1;
                        break;
                    default:
                        ignfactor = 0;
                        break;
                }
            }


            int RoomClosed = Convert.ToInt32(pa.Find(p => p.Key == "heatsystem.roomclosed").OptionValue);
            closedroomfactor = Convert.ToDecimal(1 - RoomClosed * 0.5 / (roomsinhouse + 2));

        }

        private decimal GetHeatEfficiency(ProfileAttributeCollection pa, int age)
        {
            decimal heatefficiency = 1;
            string fuel = pa.Find(p => p.Key == "heatsystem.fuel").OptionKey;
            string style = pa.Find(p => p.Key == "heatsystem.style").OptionKey;

            switch (fuel)
            {
                case "heatsystem.fuel.electric":
                    switch (age)
                    {
                        case 0:
                            switch (style)
                            {
                                case "heatsystem.style.baseboardresistance":
                                    heatefficiency = .98m;
                                    break;
                                case "heatsystem.style.forcedairfurnace":
                                    heatefficiency = .98m;
                                    break;
                                case "heatsystem.style.airsourceheatpump":
                                    heatefficiency = 2.3m;
                                    break;
                                case "heatsystem.style.groundsourceheatpump":
                                    heatefficiency = 3.5m;
                                    break;
                                case "heatsystem.style.radiant":
                                    heatefficiency = 1m;
                                    break;
                                case "heatsystem.style.airsourceheatpumpgasbackup":
                                    heatefficiency = 2.3m;
                                    break;
                                case "heatsystem.style.airsourceheatpumppropanebackup":
                                    heatefficiency = 2.3m;
                                    break;
                                case "heatsystem.style.airsourceheatpumpoilbackup":
                                    heatefficiency = 2.3m;
                                    break;
                            }
                            break;
                        case 1:
                        case 2:
                        case 3:
                        case 4:
                        case 5:
                            switch (style)
                            {
                                case "heatsystem.style.baseboardresistance":
                                    heatefficiency = .98m;
                                    break;
                                case "heatsystem.style.forcedairfurnace":
                                    heatefficiency = .98m;
                                    break;
                                case "heatsystem.style.airsourceheatpump":
                                    heatefficiency = 2.3m;
                                    break;
                                case "heatsystem.style.groundsourceheatpump":
                                    heatefficiency = 3.5m;
                                    break;
                                case "heatsystem.style.radiant":
                                    heatefficiency = 1m;
                                    break;
                                case "heatsystem.style.airsourceheatpumpgasbackup":
                                    heatefficiency = 2.3m;
                                    break;
                                case "heatsystem.style.airsourceheatpumppropanebackup":
                                    heatefficiency = 2.3m;
                                    break;
                                case "heatsystem.style.airsourceheatpumpoilbackup":
                                    heatefficiency = 2.3m;
                                    break;
                            }
                            break;
                        case 6:
                        case 7:
                        case 8:
                        case 9:
                        case 10:
                            switch (style)
                            {
                                case "heatsystem.style.baseboardresistance":
                                    heatefficiency = .98m;
                                    break;
                                case "heatsystem.style.forcedairfurnace":
                                    heatefficiency = .98m;
                                    break;
                                case "heatsystem.style.airsourceheatpump":
                                    heatefficiency = 2.2m;
                                    break;
                                case "heatsystem.style.groundsourceheatpump":
                                    heatefficiency = 3.2m;
                                    break;
                                case "heatsystem.style.radiant":
                                    heatefficiency = 1m;
                                    break;
                                case "heatsystem.style.airsourceheatpumpgasbackup":
                                    heatefficiency = 2.2m;
                                    break;
                                case "heatsystem.style.airsourceheatpumppropanebackup":
                                    heatefficiency = 2.2m;
                                    break;
                                case "heatsystem.style.airsourceheatpumpoilbackup":
                                    heatefficiency = 2.2m;
                                    break;
                            }
                            break;
                        case 11:
                        case 12:
                        case 13:
                        case 14:
                        case 15:
                            switch (style)
                            {
                                case "heatsystem.style.baseboardresistance":
                                    heatefficiency = .98m;
                                    break;
                                case "heatsystem.style.forcedairfurnace:":
                                    heatefficiency = .98m;
                                    break;
                                case "heatsystem.style.airsourceheatpump":
                                    heatefficiency = 2.2m;
                                    break;
                                case "heatsystem.style.groundsourceheatpump":
                                    heatefficiency = 3.0m;
                                    break;
                                case "heatsystem.style.radiant":
                                    heatefficiency = 1m;
                                    break;
                                case "heatsystem.style.airsourceheatpumpgasbackup":
                                    heatefficiency = 2.2m;
                                    break;
                                case "heatsystem.style.airsourceheatpumppropanebackup":
                                    heatefficiency = 2.2m;
                                    break;
                                case "heatsystem.style.airsourceheatpumpoilbackup":
                                    heatefficiency = 2.2m;
                                    break;
                            }
                            break;
                        default:
                            if (age > 15)
                            {
                                switch (style)
                                {
                                    case "heatsystem.style.baseboardresistance":
                                        heatefficiency = .98m;
                                        break;
                                    case "heatsystem.style.forcedairfurnace":
                                        heatefficiency = .98m;
                                        break;
                                    case "heatsystem.style.airsourceheatpump":
                                        heatefficiency = 2.1m;
                                        break;
                                    case "heatsystem.style.groundsourceheatpump":
                                        heatefficiency = 2.6m;
                                        break;
                                    case "heatsystem.style.radiant":
                                        heatefficiency = 1m;
                                        break;
                                    case "heatsystem.style.airsourceheatpumpgasbackup":
                                        heatefficiency = 2.1m;
                                        break;
                                    case "heatsystem.style.airsourceheatpumppropanebackup":
                                        heatefficiency = 2.1m;
                                        break;
                                    case "heatsystem.style.airsourceheatpumpoilbackup":
                                        heatefficiency = 2.1m;
                                        break;
                                }
                                break;
                            }
                            break;
                    }
                    break;
                case "heatsystem.fuel.gas":
                    switch (age)
                    {
                        case 0:
                            switch (style)
                            {
                                case "heatsystem.style.waterboiler":
                                    heatefficiency = 0.87m;
                                    break;
                                case "heatsystem.style.forcedairfurnace":
                                    heatefficiency = 0.89m;
                                    break;
                                case "heatsystem.style.steamboiler":
                                    heatefficiency = 0.82m;
                                    break;
                                case "heatsystem.style.freestandingstove":
                                    heatefficiency = 0.7m;
                                    break;
                                case "heatsystem.style.basicfireplace":
                                    heatefficiency = 0.7m;
                                    break;
                                case "heatsystem.style.heatcircfireplace":
                                    heatefficiency = 0.65m;
                                    break;
                                case "heatsystem.style.fireplaceinsert":
                                    heatefficiency = 0.7m;
                                    break;
                                case "heatsystem.style.hotwaterradiant":
                                    heatefficiency = 0.88m;
                                    break;
                            }
                            break;
                        case 1:
                        case 2:
                        case 3:
                        case 4:
                        case 5:
                            switch (style)
                            {
                                case "heatsystem.style.waterboiler":
                                    heatefficiency = 0.86m;
                                    break;
                                case "heatsystem.style.forcedairfurnace":
                                    heatefficiency = 0.88m;
                                    break;
                                case "heatsystem.style.steamboiler":
                                    heatefficiency = 0.81m;
                                    break;
                                case "heatsystem.style.freestandingstove":
                                    heatefficiency = 0.68m;
                                    break;
                                case "heatsystem.style.basicfireplace":
                                    heatefficiency = 0.68m;
                                    break;
                                case "heatsystem.style.heatcircfireplace":
                                    heatefficiency = 0.6m;
                                    break;
                                case "heatsystem.style.fireplaceinsert":
                                    heatefficiency = 0.68m;
                                    break;
                                case "heatsystem.style.hotwaterradiant":
                                    heatefficiency = 0.8m;
                                    break;
                            }
                            break;
                        case 6:
                        case 7:
                        case 8:
                        case 9:
                        case 10:
                            switch (style)
                            {
                                case "heatsystem.style.waterboiler":
                                    heatefficiency = 0.82m;
                                    break;
                                case "heatsystem.style.forcedairfurnace":
                                    heatefficiency = 0.84m;
                                    break;
                                case "heatsystem.style.steamboiler":
                                    heatefficiency = 0.79m;
                                    break;
                                case "heatsystem.style.freestandingstove":
                                    heatefficiency = 0.66m;
                                    break;
                                case "heatsystem.style.basicfireplace":
                                    heatefficiency = 0.66m;
                                    break;
                                case "heatsystem.style.heatcircfireplace":
                                    heatefficiency = 0.55m;
                                    break;
                                case "heatsystem.style.fireplaceinsert":
                                    heatefficiency = 0.66m;
                                    break;
                                case "heatsystem.style.hotwaterradiant":
                                    heatefficiency = 0.8m;
                                    break;
                            }
                            break;
                        case 11:
                        case 12:
                        case 13:
                        case 14:
                        case 15:
                            switch (style)
                            {
                                case "heatsystem.style.waterboiler":
                                    heatefficiency = 0.82m;
                                    break;
                                case "heatsystem.style.forcedairfurnace":
                                    heatefficiency = 0.83m;
                                    break;
                                case "heatsystem.style.steamboiler":
                                    heatefficiency = 0.76m;
                                    break;
                                case "heatsystem.style.freestandingstove":
                                    heatefficiency = 0.63m;
                                    break;
                                case "heatsystem.style.basicfireplace":
                                    heatefficiency = 0.64m;
                                    break;
                                case "heatsystem.style.heatcircfireplace":
                                    heatefficiency = 0.5m;
                                    break;
                                case "heatsystem.style.fireplaceinsert":
                                    heatefficiency = 0.64m;
                                    break;
                                case "heatsystem.style.hotwaterradiant":
                                    heatefficiency = 0.78m;
                                    break;
                            }
                            break;
                        case 16:
                        case 17:
                        case 18:
                        case 19:
                        case 20:
                        case 21:
                        case 22:
                        case 23:
                        case 24:
                        case 25:
                        case 26:
                        case 27:
                        case 28:
                        case 29:
                        case 30:
                            switch (style)
                            {
                                case "heatsystem.style.waterboiler":
                                    heatefficiency = 0.81m;
                                    break;
                                case "heatsystem.style.forcedairfurnace":
                                    heatefficiency = 0.82m;
                                    break;
                                case "heatsystem.style.steamboiler":
                                    heatefficiency = 0.75m;
                                    break;
                                case "heatsystem.style.freestandingstove":
                                    heatefficiency = 0.62m;
                                    break;
                                case "heatsystem.style.basicfireplace":
                                    heatefficiency = 0.62m;
                                    break;
                                case "heatsystem.style.heatcircfireplace":
                                    heatefficiency = 0.45m;
                                    break;
                                case "heatsystem.style.fireplaceinsert":
                                    heatefficiency = 0.62m;
                                    break;
                                case "heatsystem.style.hotwaterradiant":
                                    heatefficiency = 0.74m;
                                    break;
                            }
                            break;
                        default:
                            if (age > 30)
                            {
                                switch (style)
                                {
                                    case "heatsystem.style.waterboiler":
                                        heatefficiency = 0.65m;
                                        break;
                                    case "heatsystem.style.forcedairfurnace":
                                        heatefficiency = 0.65m;
                                        break;
                                    case "heatsystem.style.steamboiler":
                                        heatefficiency = 0.65m;
                                        break;
                                    case "heatsystem.style.freestandingstove":
                                        heatefficiency = 0.6m;
                                        break;
                                    case "heatsystem.style.basicfireplace":
                                        heatefficiency = 0.62m;
                                        break;
                                    case "heatsystem.style.heatcircfireplace":
                                        heatefficiency = 0.45m;
                                        break;
                                    case "heatsystem.style.fireplaceinsert":
                                        heatefficiency = 0.62m;
                                        break;
                                    case "heatsystem.style.hotwaterradiant":
                                        heatefficiency = 0.74m;
                                        break;
                                }
                                break;
                            }
                            break;
                    }
                    break;
                case "heatsystem.fuel.propane":
                    switch (age)
                    {
                        case 0:
                            switch (style)
                            {
                                case "heatsystem.style.waterboiler":
                                    heatefficiency = 0.82m;
                                    break;
                                case "heatsystem.style.forcedairfurnace":
                                    heatefficiency = 0.85m;
                                    break;
                                case "heatsystem.style.steamboiler":
                                    heatefficiency = 0.79m;
                                    break;
                                case "heatsystem.style.freestandingstove":
                                    heatefficiency = 0.7m;
                                    break;
                                case "heatsystem.style.basicfireplace":
                                    heatefficiency = 0.7m;
                                    break;
                                case "heatsystem.style.heatcircfireplace":
                                    heatefficiency = 0.65m;
                                    break;
                                case "heatsystem.style.fireplaceinsert":
                                    heatefficiency = 0.7m;
                                    break;
                                case "heatsystem.style.hotwaterradiant":
                                    heatefficiency = 0.88m;
                                    break;
                            }
                            break;
                        case 1:
                        case 2:
                        case 3:
                        case 4:
                        case 5:
                            switch (style)
                            {
                                case "heatsystem.style.waterboiler":
                                    heatefficiency = 0.82m;
                                    break;
                                case "heatsystem.style.forcedairfurnace":
                                    heatefficiency = 0.85m;
                                    break;
                                case "heatsystem.style.steamboiler":
                                    heatefficiency = 0.78m;
                                    break;
                                case "heatsystem.style.freestandingstove":
                                    heatefficiency = 0.68m;
                                    break;
                                case "heatsystem.style.basicfireplace":
                                    heatefficiency = 0.68m;
                                    break;
                                case "heatsystem.style.heatcircfireplace":
                                    heatefficiency = 0.6m;
                                    break;
                                case "heatsystem.style.fireplaceinsert":
                                    heatefficiency = 0.68m;
                                    break;
                                case "heatsystem.style.hotwaterradiant":
                                    heatefficiency = 0.8m;
                                    break;
                            }
                            break;
                        case 6:
                        case 7:
                        case 8:
                        case 9:
                        case 10:
                            switch (style)
                            {
                                case "heatsystem.style.waterboiler":
                                    heatefficiency = 0.80m;
                                    break;
                                case "heatsystem.style.forcedairfurnace":
                                    heatefficiency = 0.84m;
                                    break;
                                case "heatsystem.style.steamboiler":
                                    heatefficiency = 0.76m;
                                    break;
                                case "heatsystem.style.freestandingstove":
                                    heatefficiency = 0.66m;
                                    break;
                                case "heatsystem.style.basicfireplace":
                                    heatefficiency = 0.66m;
                                    break;
                                case "heatsystem.style.heatcircfireplace":
                                    heatefficiency = 0.55m;
                                    break;
                                case "heatsystem.style.fireplaceinsert":
                                    heatefficiency = 0.66m;
                                    break;
                                case "heatsystem.style.hotwaterradiant":
                                    heatefficiency = 0.8m;
                                    break;
                            }
                            break;
                        case 11:
                        case 12:
                        case 13:
                        case 14:
                        case 15:
                            switch (style)
                            {
                                case "heatsystem.style.waterboiler":
                                    heatefficiency = 0.80m;
                                    break;
                                case "heatsystem.style.forcedairfurnace":
                                    heatefficiency = 0.83m;
                                    break;
                                case "heatsystem.style.steamboiler":
                                    heatefficiency = 0.74m;
                                    break;
                                case "heatsystem.style.freestandingstove":
                                    heatefficiency = 0.64m;
                                    break;
                                case "heatsystem.style.basicfireplace":
                                    heatefficiency = 0.64m;
                                    break;
                                case "heatsystem.style.heatcircfireplace":
                                    heatefficiency = 0.5m;
                                    break;
                                case "heatsystem.style.fireplaceinsert":
                                    heatefficiency = 0.64m;
                                    break;
                                case "heatsystem.style.hotwaterradiant":
                                    heatefficiency = 0.78m;
                                    break;
                            }
                            break;
                        default:
                            if (age > 15)
                            {
                                switch (style)
                                {
                                    case "heatsystem.style.waterboiler":
                                        heatefficiency = 0.80m;
                                        break;
                                    case "heatsystem.style.forcedairfurnace":
                                        heatefficiency = 0.80m;
                                        break;
                                    case "heatsystem.style.steamboiler":
                                        heatefficiency = 0.72m;
                                        break;
                                    case "heatsystem.style.freestandingstove":
                                        heatefficiency = 0.62m;
                                        break;
                                    case "heatsystem.style.basicfireplace":
                                        heatefficiency = 0.62m;
                                        break;
                                    case "heatsystem.style.heatcircfireplace":
                                        heatefficiency = 0.45m;
                                        break;
                                    case "heatsystem.style.fireplaceinsert":
                                        heatefficiency = 0.62m;
                                        break;
                                    case "heatsystem.style.hotwaterradiant":
                                        heatefficiency = 0.74m;
                                        break;
                                }
                                break;
                            }
                            break;
                    }
                    break;
                case "heatsystem.fuel.oil":
                    switch (age)
                    {
                        case 0:
                            switch (style)
                            {
                                case "heatsystem.style.waterboiler":
                                    heatefficiency = 0.84m;
                                    break;
                                case "heatsystem.style.forcedairfurnace":
                                    heatefficiency = 0.83m;
                                    break;
                                case "heatsystem.style.steamboiler":
                                    heatefficiency = 0.77m;
                                    break;
                                case "heatsystem.style.hotwaterradiant":
                                    heatefficiency = 0.85m;
                                    break;
                            }
                            break;
                        case 1:
                        case 2:
                        case 3:
                        case 4:
                        case 5:
                            switch (style)
                            {
                                case "heatsystem.style.waterboiler":
                                    heatefficiency = 0.84m;
                                    break;
                                case "heatsystem.style.forcedairfurnace":
                                    heatefficiency = 0.81m;
                                    break;
                                case "heatsystem.style.steamboiler":
                                    heatefficiency = 0.76m;
                                    break;
                                case "heatsystem.style.hotwaterradiant":
                                    heatefficiency = 0.8m;
                                    break;
                            }
                            break;
                        case 6:
                        case 7:
                        case 8:
                        case 9:
                        case 10:
                            switch (style)
                            {
                                case "heatsystem.style.waterboiler":
                                    heatefficiency = 0.80m;
                                    break;
                                case "heatsystem.style.forcedairfurnace":
                                    heatefficiency = 0.81m;
                                    break;
                                case "heatsystem.style.steamboiler":
                                    heatefficiency = 0.74m;
                                    break;
                                case "heatsystem.style.hotwaterradiant":
                                    heatefficiency = 0.8m;
                                    break;
                            }
                            break;
                        case 11:
                        case 12:
                        case 13:
                        case 14:
                        case 15:
                            switch (style)
                            {
                                case "heatsystem.style.waterboiler":
                                    heatefficiency = 0.78m;
                                    break;
                                case "heatsystem.style.forcedairfurnace":
                                    heatefficiency = 0.81m;
                                    break;
                                case "heatsystem.style.steamboiler":
                                    heatefficiency = 0.72m;
                                    break;
                                case "heatsystem.style.hotwaterradiant":
                                    heatefficiency = 0.78m;
                                    break;
                            }
                            break;
                        default:
                            if (age > 15)
                            {
                                switch (style)
                                {
                                    case "heatsystem.style.waterboiler":
                                        heatefficiency = 0.74m;
                                        break;
                                    case "heatsystem.style.forcedairfurnace":
                                        heatefficiency = 0.78m;
                                        break;
                                    case "heatsystem.style.steamboiler":
                                        heatefficiency = 0.70m;
                                        break;
                                    case "heatsystem.style.HotWaterRadiant":
                                        heatefficiency = 0.74m;
                                        break;
                                }
                                break;
                            }
                            break;
                    }
                    break;
                case "heatsystem.fuel.wood":
                    switch (age)
                    {
                        case 0:
                            heatefficiency = 0.75m;
                            break;
                        case 1:
                        case 2:
                        case 3:
                        case 4:
                            heatefficiency = 0.68m;
                            break;
                        case 5:
                        case 6:
                        case 7:
                        case 8:
                        case 9:
                            heatefficiency = 0.6m;
                            break;
                        case 10:
                        case 11:
                        case 12:
                        case 13:
                        case 14:
                        case 15:
                            heatefficiency = 0.48m;
                            break;
                        default:
                            if (age > 15)
                            {
                                heatefficiency = 0.42m;
                            }
                            break;
                    }
                    break;
            }

            if (heatefficiency == 0)    // this would be a problem
            {
                heatefficiency = 1;
            }

            //override efficiency
            if (pa.Exists(p => p.Key == "factoroverride.heatefficiency")) heatefficiency = Convert.ToDecimal(pa.Find(p => p.Key == "factoroverride.heatefficiency").OptionValue);

            // TFS 1966 Dec 2015 - use provided efficiency value if existed
            if (pa.Exists(p => p.Key == "heatsystem.efficiencyvalue") && Convert.ToDecimal(pa.Find(p => p.Key == "heatsystem.efficiencyvalue").OptionValue) > 0m) heatefficiency = Convert.ToDecimal(pa.Find(p => p.Key == "heatsystem.efficiencyvalue").OptionValue);

            return (heatefficiency);
        }

        private decimal GetDeliveryFactor(ProfileAttributeCollection pa, EnergyModelConfiguration config)
        {
            string stemp = null;
            string style = pa.Find(p => p.Key == "heatsystem.style").OptionKey;

            stemp = "heatsystem.deliveryfactor_";

            switch (style)
            {
                case "heatsystem.style.baseboardresistance":
                    stemp = stemp + "bboard_";
                    break;
                case "heatsystem.style.forcedairfurnace":
                    stemp = stemp + "furn_";
                    break;
                case "heatsystem.style.airsourceheatpump":
                case "heatsystem.style.airsourceheatpumpgasbackup":
                case "heatsystem.style.airsourceheatpumppropanebackup":
                case "heatsystem.style.airsourceheatpumpoilbackup":
                    stemp = stemp + "hp_";
                    break;
                case "heatsystem.style.groundsourceheatpump":
                    stemp = stemp + "gndhp_";
                    break;
                case "heatsystem.style.WaterBoiler":
                    stemp = stemp + "wblr_";
                    break;
                case "heatsystem.style.steamboiler":
                    stemp = stemp + "stblr_";
                    break;
                case "heatsystem.style.radiant":
                    stemp = stemp + "radiantpanel_";
                    break;
                case "heatsystem.style.hotwaterradiant":
                    stemp = stemp + "wradiant_";
                    break;
                case "heatsystem.style.basicfireplace":
                case "heatsystem.style.heatcircfireplace":
                case "heatsystem.style.fireplaceinsert":
                    stemp = stemp + "fireplace_";
                    break;
                case "heatsystem.style.freestandingstove":
                    stemp = stemp + "freestandingstove_";
                    break;
                default:    //catch all those factors that are unaccounted for (like conventionalgas)
                    stemp = stemp + "radiantpanel_";
                    break;
            }

            if (pa.Find(p => p.Key == "heatsystem.sealleaks").OptionKey == "heatsystem.sealleaks.yes")
            {
                stemp = stemp + "sealed";
            }
            else
            {
                stemp = stemp + "unsealed";
            }

            decimal DeliveryFactor = 0;

            DeliveryFactor = config.GetCalculationFactor<decimal>(stemp);

            if (DeliveryFactor == 0)
            {
                DeliveryFactor = 1;
            }

            return DeliveryFactor;

        }

        private decimal GetHeatTempDiff(ProfileAttributeCollection pa, EnergyModelConfiguration config)
        {
            decimal evtemp = 0;
            decimal daytemp = 0;
            decimal nitetemp = 0;
            short i = 0;
            short j = 0;
            short days = 0;
            decimal tempdiff_hour = 0;
            decimal tempdiff_month = 0;
            decimal niteseting = 0;
            decimal evseting = 0;
            decimal dayseting = 0;
            decimal shading = 0;
            decimal low_e_window = 0;
            decimal areawest = 0;
            decimal areaeast = 0;
            decimal areasouth = 0;
            decimal areanorth = 0;
            decimal nval = 0;
            decimal solarload = 0;
            decimal[] nmonthlyload = new decimal[13];
            decimal internalheatgain = 0;
            decimal infiltrationfactor = 0;
            decimal airvolumechange = 0;
            decimal[,] solarwinter = new decimal[5, 25];
            decimal[,] solarsummer = new decimal[5, 25];
            decimal[,] solartable = new decimal[5, 25];
            decimal[,] temptable = new decimal[14, 25];
            decimal basementwinter = 0;
            decimal naveragegain = 0;

            // get kfactors
            HouseUpdateInternalOutput(pa, config);

            basementwinter = Convert.ToDecimal(pa.Find(p => p.Key == "house.basementwintertemp").OptionValue);

            if (pa.Find(p => p.Key == "house.windowslowe").OptionKey == "yes")
            {
                low_e_window = 0.1m;
            }
            else
            {
                low_e_window = 0;
            }

            int totalarea = (int)GetSpecialDecimalFromValueOrList(pa, "house.totalarea", "house.totalarearange");
            int people = (int)Convert.ToDecimal(pa.Find(p => p.Key == "house.people").OptionValue);

            decimal sensiblegain = 0;
            decimal latentgain = 0;
            sensiblegain = basewinterheatgain + (totalarea * winterheatgainpersqft) + winterheatgainperperson * people;
            latentgain = 68 + 0.07m * totalarea + 41 * people;
            naveragegain = sensiblegain + latentgain;

            string sTemp = null;

            sTemp = "heatsystem.airvolumechange_";

            int year = (int)GetSpecialDecimalFromValueOrList(pa, "house.yearbuilt", "house.yearbuiltrange");
            int age = DateTime.Now.Year - year;

            Boolean drafty = false;
            drafty = Convert.ToBoolean(System.Convert.ToInt32(pa.Find(p => p.Key == "house.drafty").OptionValue));

            switch (age)
            {
                case 0:
                case 1:
                case 2:
                case 3:
                case 4:
                case 5:
                case 6:
                case 7:
                case 8:
                case 9:
                    sTemp = sTemp + "lt10";
                    break;
                case 10:
                case 11:
                case 12:
                case 13:
                case 14:
                case 15:
                case 16:
                case 17:
                case 18:
                case 19:
                case 20:
                    sTemp = sTemp + "10to20";
                    break;
                default:
                    sTemp = sTemp + "gt20";
                    break;
            }

            if (drafty)
            {
                sTemp += "drafty";
            }

            airvolumechange = config.GetCalculationFactor<decimal>(sTemp);
            if (pa.Exists(p => p.Key == "factoroverride." + sTemp)) airvolumechange = Convert.ToDecimal(pa.Find(p => p.Key == "factoroverride." + sTemp).OptionValue);

            infiltrationfactor = Convert.ToDecimal(1.08m * airvolumechange * totalarea * 9 / 60);

            decimal shadingkoeff = Convert.ToDecimal(pa.Find(p => p.Key == "house.shadingcoefficient").OptionValue);
            //decimal windowarea = Convert.ToDecimal(pa.Find(p => p.Key == "house.windowarea").OptionValue);
            shading = shadingkoeff - low_e_window;
            areawest = Convert.ToDecimal(shading * 0.25m * windowarea);
            areaeast = areawest;
            areanorth = areawest;
            areasouth = areawest;

            //get Average temperature
            int zones = Convert.ToInt32(pa.Find(p => p.Key == "house.zones").OptionValue);
            int daylivngheating = Convert.ToInt32(pa.Find(p => p.Key == "house.daylivngheating").OptionValue);
            int evelivngheating = Convert.ToInt32(pa.Find(p => p.Key == "house.evelivngheating").OptionValue);
            int nitelivngheating = Convert.ToInt32(pa.Find(p => p.Key == "house.nitelivngheating").OptionValue);
            string daylivngheatoff = Convert.ToString(pa.Find(p => p.Key == "house.daylivngheatoff").OptionKey);
            string evelivngheatoff = Convert.ToString(pa.Find(p => p.Key == "house.evelivngheatoff").OptionKey);
            string nitelivngheatoff = Convert.ToString(pa.Find(p => p.Key == "house.nitelivngheatoff").OptionKey);
            int daysleepheating = Convert.ToInt32(pa.Find(p => p.Key == "house.daysleepheating").OptionValue);
            int evesleepheating = Convert.ToInt32(pa.Find(p => p.Key == "house.evesleepheating").OptionValue);
            int nitesleepheating = Convert.ToInt32(pa.Find(p => p.Key == "house.nitesleepheating").OptionValue);
            string daysleepheatoff = Convert.ToString(pa.Find(p => p.Key == "house.daysleepheatoff").OptionKey);
            string evesleepheatoff = Convert.ToString(pa.Find(p => p.Key == "house.evesleepheatoff").OptionKey);
            string nitesleepheatoff = Convert.ToString(pa.Find(p => p.Key == "house.nitesleepheatoff").OptionKey);

            //get hourly gain overrides
            decimal hourlyGainFactorNight = 0.64m;
            decimal hourlyGainFactorDay = 1.0m;
            decimal hourlyGainFactorEvening = 1.3m;
            if (pa.Exists(p => p.Key == "factoroverride.hourlygainfactornight")) hourlyGainFactorNight = Convert.ToDecimal(pa.Find(p => p.Key == "factoroverride.hourlygainfactornight").OptionValue);
            if (pa.Exists(p => p.Key == "factoroverride.hourlygainfactorday")) hourlyGainFactorDay = Convert.ToDecimal(pa.Find(p => p.Key == "factoroverride.hourlygainfactorday").OptionValue);
            if (pa.Exists(p => p.Key == "factoroverride.hourlygainfactorevening")) hourlyGainFactorEvening = Convert.ToDecimal(pa.Find(p => p.Key == "factoroverride.hourlygainfactorevening").OptionValue);

            switch (zones)
            {
                case 1:
                    daytemp = daylivngheating;
                    evtemp = evelivngheating;
                    nitetemp = nitelivngheating;
                    //day time shut off
                    if (daylivngheatoff == "house.daylivngheatoff.yes")
                    {
                        dayseting = 0;
                    }
                    else
                    {
                        dayseting = 1;
                    }
                    //day time shut off
                    if (evelivngheatoff == "house.evelivngheatoff.yes")
                    {
                        evseting = 0;
                    }
                    else
                    {
                        evseting = 1;
                    }
                    //day time shut off
                    if (nitelivngheatoff == "house.nitelivngheatoff.yes")
                    {
                        niteseting = 0;
                    }
                    else
                    {
                        niteseting = 1;
                    }
                    break;
                default:

                    daytemp = (daylivngheating + daysleepheating) / 2;
                    evtemp = (evelivngheating + evesleepheating) / 2;
                    nitetemp = (nitelivngheating + nitesleepheating) / 2;
                    //day time shut off
                    if (daylivngheatoff == "house.daylivngheatoff.yes" && daysleepheatoff == "house.daysleepheatoff.yes")
                    {
                        dayseting = 0;
                    }
                    else if ((daylivngheatoff == "house.daylivngheatoff.yes" && daysleepheatoff == "house.daysleepheatoff.yes") ||
                        (daylivngheatoff == "house.daylivngheatoff.yes" && daysleepheatoff == "house.daysleepheatoff.yes"))
                    {
                        dayseting = 0.7m;
                    }
                    else
                    {
                        dayseting = 1;
                    }
                    //Eve time shut off
                    if (evelivngheatoff == "houseevelivngheatoff.yes" && evesleepheatoff == "house.evesleepheatoff.yes")
                    {
                        evseting = 0;
                    }
                    else if ((evelivngheatoff == "house.evelivngheatoff.yes" && evesleepheatoff == "house.evesleepheatoff.yes") ||
                        (evelivngheatoff == "house.evelivngheatoff.yes" && evesleepheatoff == "house.evesleepheatoff.yes"))
                    {
                        evseting = 0.7m;
                    }
                    else
                    {
                        evseting = 1;
                    }
                    //Nite time shut off
                    if (nitelivngheatoff == "house.nitelivngheatoff.yes" && nitesleepheatoff == "house.nitesleepheatoff.yes")
                    {
                        niteseting = 0;
                    }
                    else if ((nitelivngheatoff == "house.nitelivngheatoff.yes" && nitesleepheatoff == "house.nitesleepheatoff.yes") ||
                        (nitelivngheatoff == "house.nitelivngheatoff.yes" && nitesleepheatoff == "house.nitesleepheatoff.yes"))
                    {
                        niteseting = 0.7m;
                    }
                    else
                    {
                        niteseting = 1;
                    }
                    break;
            }

            //[season 0 = winter, 1 = summar][direction = n, e, s, w][hour = 1 - 24]
            for (i = 0; i < 24; i++)
            {
                solarsummer[0, i] = _input.WeatherInfo.SolarFactors[1, 0, i];   //north
                solarsummer[1, i] = _input.WeatherInfo.SolarFactors[1, 1, i];   //east
                solarsummer[2, i] = _input.WeatherInfo.SolarFactors[1, 2, i];   //south
                solarsummer[3, i] = _input.WeatherInfo.SolarFactors[1, 3, i];   //west
            }

            for (i = 0; i < 24; i++)
            {
                solarwinter[0, i] = _input.WeatherInfo.SolarFactors[0, 0, i];   //north
                solarwinter[1, i] = _input.WeatherInfo.SolarFactors[0, 1, i];   //east
                solarwinter[2, i] = _input.WeatherInfo.SolarFactors[0, 2, i];   //south
                solarwinter[3, i] = _input.WeatherInfo.SolarFactors[0, 3, i];   //west
            }

            //[type 0 = regular, 1 = wet bulb][month = 1 - 12][hour = 1 - 24]
            for (i = 0; i < 24; i++)
            {
                temptable[1, i + 1] = _input.WeatherInfo.TemperatureFactors[0, 0, i];
                temptable[2, i + 1] = _input.WeatherInfo.TemperatureFactors[0, 1, i];
                temptable[3, i + 1] = _input.WeatherInfo.TemperatureFactors[0, 2, i];
                temptable[4, i + 1] = _input.WeatherInfo.TemperatureFactors[0, 3, i];
                temptable[5, i + 1] = _input.WeatherInfo.TemperatureFactors[0, 4, i];
                temptable[6, i + 1] = _input.WeatherInfo.TemperatureFactors[0, 5, i];
                temptable[7, i + 1] = _input.WeatherInfo.TemperatureFactors[0, 6, i];
                temptable[8, i + 1] = _input.WeatherInfo.TemperatureFactors[0, 7, i];
                temptable[9, i + 1] = _input.WeatherInfo.TemperatureFactors[0, 8, i];
                temptable[10, i + 1] = _input.WeatherInfo.TemperatureFactors[0, 9, i];
                temptable[11, i + 1] = _input.WeatherInfo.TemperatureFactors[0, 10, i];
                temptable[12, i + 1] = _input.WeatherInfo.TemperatureFactors[0, 11, i];
            }

            for (i = 1; i <= 12; i++)
            {
                switch (i)
                {
                    case 1:
                    case 3:
                    case 5:
                    case 7:
                    case 8:
                    case 10:
                    case 12:
                        days = 31;
                        break;
                    case 2:
                        days = 28;
                        break;
                    case 4:
                    case 6:
                    case 9:
                    case 11:
                        days = 30;
                        break;
                }
                switch (i)
                {
                    case 12:
                    case 10:
                    case 11:
                    case 1:
                    case 2:
                    case 3:
                        solartable = solarwinter;
                        break;
                    case 9:
                    case 4:
                    case 5:
                    case 6:
                    case 7:
                    case 8:
                        solartable = solarsummer;
                        break;
                }
                tempdiff_hour = 0;


                for (j = 0; j <= 23; j++)
                {
                    solarload = areanorth * solartable[0, j] + areaeast * solartable[1, j] + areasouth * solartable[2, j] + areawest * solartable[3, j];

                    switch (j)
                    {
                        case 6:
                        case 7:
                        case 8:
                        case 9:
                        case 10:
                        case 11:
                        case 12:
                        case 13:
                        case 14:
                        case 15:
                        case 16:
                            internalheatgain = naveragegain * hourlyGainFactorDay;
                            nval = dayseting * ((kfactorhouse + infiltrationfactor) * (daytemp - temptable[i, j + 1]) + kfactorfloor * (daytemp - basementwinter) - internalheatgain - solarload);
                            if (nval > 0)
                                tempdiff_hour = tempdiff_hour + nval;

                            break;
                        case 17:
                        case 18:
                        case 19:
                        case 20:
                        case 21:
                        case 22:
                            internalheatgain = naveragegain * hourlyGainFactorEvening;
                            nval = evseting * ((kfactorhouse + infiltrationfactor) * (evtemp - temptable[i, j + 1]) + kfactorfloor * (evtemp - basementwinter) - internalheatgain - solarload);
                            if (nval > 0)
                                tempdiff_hour = tempdiff_hour + nval;

                            break;
                        case 23:
                        case 0:
                        case 1:
                        case 2:
                        case 3:
                        case 4:
                        case 5:
                            internalheatgain = naveragegain * hourlyGainFactorNight;
                            nval = niteseting * ((kfactorhouse + infiltrationfactor) * (nitetemp - temptable[i, j + 1]) + kfactorfloor * (nitetemp - basementwinter) - internalheatgain - solarload);
                            if (nval > 0)
                                tempdiff_hour = tempdiff_hour + nval;

                            break;
                    }
                }

                nmonthlyload[i] = Convert.ToDecimal(tempdiff_hour * days);
                tempdiff_month = tempdiff_month + (tempdiff_hour * days);
                tempdiff_hour = 0;
            }

            return tempdiff_month;
        }

    }
}
