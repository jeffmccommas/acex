﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace CE.EnergyModel
{
    public class EndUseCollection : Collection<EndUse>
    {
        public bool IsStandAlone { get; set; }

        public EndUseCollection()
            : base(new List<EndUse>())
        {

        }

        public EndUse Find(Predicate<EndUse> match)
        {
            List<EndUse> items = (List<EndUse>)Items;
            return (items.Find(match));
        }

        public bool Exists(Predicate<EndUse> match)
        {
            List<EndUse> items = (List<EndUse>)Items;
            return (items.Exists(match));
        }

        public int RemoveAll(Predicate<EndUse> match)
        {
            List<EndUse> items = (List<EndUse>)Items;
            return (items.RemoveAll(match));
        }

        /// <summary>
        /// Remove end use energy uses that are extraneous. Use during bill disagg to keep only Elec, Gas, Water uses.
        /// Also remove extraneous btu
        /// </summary>
        public void RemoveAddOnEnergyUses()
        {
            List<EndUse> items = (List<EndUse>)Items;

            for (int i = 0; i < this.Count; i++)
            {
                items[i].EnergyUses.RemoveAll(p => p.Commodity == Enums.CommodityType.Unspecified
                                                || p.Commodity == Enums.CommodityType.Oil
                                                || p.Commodity == Enums.CommodityType.Propane
                                                || p.Commodity == Enums.CommodityType.Wood
                                                || p.Commodity == Enums.CommodityType.Coal
                                                || p.Commodity == Enums.CommodityType.HotWater
                                                || p.Commodity == Enums.CommodityType.OtherWater
                                                || p.Commodity == Enums.CommodityType.ThermoFlow
                                                || p.UnitOfMeasure == Enums.UnitOfMeasureType.BTU);
            }

            //Unspecified,
            //Electric,
            //Gas,
            //Water,
            //Oil,
            //Propane,
            //Wood,
            //Coal,
            //HotWater,
            //OtherWater,
            //ThermoFlow

        }

    }

}
