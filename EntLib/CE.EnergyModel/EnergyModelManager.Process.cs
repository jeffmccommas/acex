﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using CE.EnergyModel.Enums;
using NCalc;

namespace CE.EnergyModel
{
    public partial class EnergyModelManager : IEnergyModel
    {

        /// <summary>
        /// CoreCalculationsAsync.
        /// </summary>
        /// <param name="fullResult"></param>
        /// <param name="input"></param>
        /// <returns></returns>
        private async Task CoreCalculationsAsync(EnergyModelResult fullResult, EnergyModelInputParams input)
        {

            // Make a list MonthCalcParams.
            List<CalcAppliancesParams> calcAppliancesParams = SetupCalcAppliancesParams(input);

            // Create a query that, when executed, returns a collection of tasks.
            IEnumerable<Task<CalcAppliancesResult>> processCalcAppliancesTasksQuery =
                from calcParams in calcAppliancesParams select ProcessCalcAppliancesAsync(calcParams);

            // Use ToList to execute the query and start the tasks. 
            List<Task<CalcAppliancesResult>> processCalcAppliancesTasks = processCalcAppliancesTasksQuery.ToList();

            // In the loop, process the tasks one at a time until none remain. 
            while (processCalcAppliancesTasks.Count > 0)
            {
                // Identify a task that completes.
                Task<CalcAppliancesResult> finishedTask = await Task.WhenAny(processCalcAppliancesTasks).ConfigureAwait(false);

                // Remove the selected task from the list so that it is not processed more than once.
                processCalcAppliancesTasks.Remove(finishedTask);

                // Await the completed task. 
                CalcAppliancesResult calcAppliancesResult = await finishedTask.ConfigureAwait(false);

                // Add the calculated appliances to the fullResults as they complete.
                fullResult.Appliances.AddRange(calcAppliancesResult.Appliances);
            }

        }

        /// <summary>
        /// ProcessCalcAppliancesAsync.  Modularized medium and high complexity appliances as well as simple expression-based appliances.
        /// </summary>
        /// <param name="calcParams"></param>
        /// <returns></returns>
        private async Task<CalcAppliancesResult> ProcessCalcAppliancesAsync(CalcAppliancesParams calcParams)
        {
            var calcResult = new CalcAppliancesResult();

            foreach (var appliance in calcParams.Appliances)
            {
                // For appliances that have no Energy Uses defined, those are automatically "complex"
                if (appliance.EnergyUses.Count() == 0)
                {
                    var e = new ApplianceEvaluator(appliance, calcParams.Input);

                    try
                    {
                        await Task.Factory.StartNew(() => e.Evaluate()).ConfigureAwait(false);

                        if (e.Appliance is Appliance)
                        {
                            // TODO: note that the appliance here should also have monthly energy use periods if that was specified
                            // Each monthly appliance will need to do this on its own. 

                            // And, add appliances to output that have energy use OR failed, since we need to see failed appliances, 
                            // we do not want to hide failed appliances from output.
                            if (calcParams.Appliances.IsStandAlone || (e.Appliance.HasNonZeroEnergyUse() || e.Appliance.HasFailedEnergyUseContext()))
                            {
                                calcResult.Appliances.Add(e.Appliance);
                            }

                        }
                    }
                    catch (Exception ex)
                    {
                        var errEU = new EnergyUse(EvaluationType.Failed);
                        errEU.Quantity = 0.0m;
                        errEU.EvaluationContext.Status = new Status(new Exception(Exception_Message_ApplianceFailed, ex), StatusType.Error);
                        appliance.EnergyUses.Add(errEU);
                        calcResult.Appliances.Add(appliance);
                    }

                }
                else
                {
                    // Expression evaluations here per commodity for those appliances that are not indicated as complex
                    // This MAY typically be simple single energy appliances. 
                    foreach (var energyUse in appliance.EnergyUses)
                    {
                        if (energyUse.EvaluationContext.EvaluationType == EvaluationType.Pending)
                        {
                            try
                            {
                                //First, process the expression for replacement of inline parameters
                                //For example =>  [P:atticfan.count] * [P:atticfan.kwhperuse] * [P:atticfan.usage] * ([D:CDD]/125)
                                energyUse.EvaluationContext.Expression = PreprocessExpression(energyUse.EvaluationContext.Expression, calcParams);

                                // get expression here; note that function delegates can be used
                                // set the expression to ignore case
                                var options = EvaluateOptions.IgnoreCase;
                                var e = new Expression(energyUse.EvaluationContext.Expression, options);

                                //Execute an expression evaluation here
                                var evalResult = await Task.Factory.StartNew(() => e.Evaluate()).ConfigureAwait(false);

                                if (!(evalResult is string))
                                {
                                    energyUse.Quantity = System.Convert.ToDecimal(evalResult);
                                    energyUse.EvaluationContext.EvaluationType = EvaluationType.Completed;
                                }

                            }
                            catch (Exception ex)
                            {
                                //Individual expression failures are caught here with status
                                energyUse.Quantity = 0.0m;
                                energyUse.EvaluationContext.EvaluationType = EvaluationType.Failed;
                                energyUse.EvaluationContext.Status = new Status(new Exception(Exception_Message_ApplianceFailed, ex), StatusType.Error);
                            }

                        }
                        else if (energyUse.EvaluationContext.EvaluationType == EvaluationType.Provided)
                        {
                            //quantity will contain result already
                        }

                    }

                    // TODO: appliances that are provided, or have expressions evaluated, can now have their monthly energy use determined
                    // This is not happening yet! 

                    // TODO: review this - should the appliance be cloned here before adding?
                    // And, add appliances to output that have energy use OR failed, since we need to see failed appliances, 
                    // we do not want to hide failed appliances from output.
                    if (calcParams.Appliances.IsStandAlone || (appliance.HasNonZeroEnergyUse() || appliance.HasFailedEnergyUseContext()))
                    {
                        calcResult.Appliances.Add(appliance);
                    }

                }

            }

            return (calcResult);
        }

        /// <summary>
        /// SetupCalcAppliancesParams.
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        private List<CalcAppliancesParams> SetupCalcAppliancesParams(EnergyModelInputParams input)
        {
            var p = new List<CalcAppliancesParams>();
            var chunkSize = input.ExtraParams.ChunkSize;
            if (chunkSize < 2) { chunkSize = 2; } else if (chunkSize > 100) { chunkSize = 100; }

            // Chunk the processing using groups of appliances
            int i = 0;
            foreach (var applianceGroup in input.Appliances.GroupBy(s => i++ / chunkSize))
            {
                var acp = new CalcAppliancesParams();
                acp.Input = input;
                acp.Appliances.IsStandAlone = input.Appliances.IsStandAlone;

                // iterate the grouped list
                foreach (var a in applianceGroup)
                {

                    var appliance = new Appliance
                    {
                        Name = a.Name,
                        Key = a.Key
                    };

                    foreach (var u in a.EnergyUses)
                    {
                        var energyUse = new EnergyUse();
                        energyUse.Commodity = u.Commodity;
                        energyUse.UnitOfMeasure = u.UnitOfMeasure;
                        energyUse.Quantity = u.Quantity;

                        if (u.EvaluationContext != null)
                        {
                            energyUse.EvaluationContext = new EvaluationContext
                            {
                                EvaluationType = u.EvaluationContext.EvaluationType,
                                Expression = u.EvaluationContext.Expression
                            };
                        }
                        else
                        {
                            energyUse.EvaluationContext = new EvaluationContext
                            {
                                EvaluationType = Enums.EvaluationType.Provided,
                                Expression = string.Empty
                            };
                        }

                        appliance.EnergyUses.Add(energyUse);
                    }

                    acp.Appliances.Add(appliance);

                }

                p.Add(acp);

            }

            return (p);
        }


        /// <summary>
        /// Find and replace tokens in an expression.
        /// </summary>
        /// <param name="input"></param>
        /// <param name="calcParams"></param>
        /// <returns></returns>
        private string PreprocessExpression(string input, CalcAppliancesParams calcParams)
        {
            string output = input;
            string pattern = string.Empty;
            MatchCollection matches = null;
            const string Match_PA = @"\[P:(.*?)\]"; //*P*rofile attribute
            const string Match_CA = @"\[F:(.*?)\]"; //calculation *F*actor
            const string Match_DV = @"\[D:(.*?)\]"; //*D*erived value
            const string Derived_HDD = "HDD";
            const string Derived_CDD = "CDD";

            //Find and replace all Profile attributes [P:xxxxxx]
            if (calcParams.Input != null && calcParams.Input.ProfileAttributes != null)
            {
                pattern = Match_PA;
                matches = Regex.Matches(output, pattern, RegexOptions.IgnoreCase);
                if (matches.Count > 0)
                {
                    foreach (Match match in matches)
                    {
                        var pa = calcParams.Input.ProfileAttributes.Find(p => p.Key == match.Groups[1].Value);
                        if (pa.OptionValue != null)
                        {
                            output = output.Replace(match.Groups[0].Value, pa.OptionValue);
                        }
                    }
                }
            }

            //Find and replace all client configurable calculation Factors [F:xxxxxx]
            if (calcParams.Input != null && calcParams.Input.Configuration != null && calcParams.Input.Configuration.CalculationFactors != null)
            {
                pattern = Match_CA;
                matches = Regex.Matches(output, pattern, RegexOptions.IgnoreCase);
                if (matches.Count > 0)
                {
                    foreach (Match match in matches)
                    {
                        string ca;
                        if (calcParams.Input.Configuration.CalculationFactors.TryGetValue(match.Groups[1].Value, out ca))
                        {
                            output = output.Replace(match.Groups[0].Value, ca);
                        }

                    }
                }
            }

            //Find and replace all Derived values [D:xxx]; supported are: HDD, CDD
            if (calcParams.Input != null)
            {
                pattern = Match_DV;
                matches = Regex.Matches(output, pattern, RegexOptions.IgnoreCase);
                if (matches.Count > 0)
                {
                    foreach (Match match in matches)
                    {
                        switch (match.Groups[1].Value.ToUpper())
                        {
                            case Derived_HDD:
                                output = output.Replace(match.Groups[0].Value, calcParams.Input.WeatherInfo.AnnualHDD.ToString());
                                break;

                            case Derived_CDD:
                                output = output.Replace(match.Groups[0].Value, calcParams.Input.WeatherInfo.AnnualCDD.ToString());
                                break;

                            default:
                                break;
                        }

                    }
                }
            }


            return (output);
        }
    }
}
