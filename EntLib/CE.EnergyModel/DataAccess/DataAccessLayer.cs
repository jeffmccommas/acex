﻿using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using CE.EnergyModel.Cache;
using System.Collections.Generic;
using CE.EnergyModel.Entities;

namespace CE.EnergyModel.DataAccess
{
    public partial class DataAccessLayer
    {
        private const string InsightsMetaDataConnName = "InsightsMetaDataConn";
        private const string InsightsDWConnName = "InsightsDWConn";
        private const string InsightsConnName = "InsightsConn";
        private const string AppSetting_PaaSOnly = "PaaSOnly";
        private const string AppSetting_PaaSDedicatedClients = "PaaSDedicatedClients";
        private readonly string _metaDataConnString;
        private readonly string _dwConnString;
        private readonly string _insightsConnString;
        private int _cacheTimeoutInMinutes = 20;

        public DataAccessLayer()
        {
            _metaDataConnString = ConfigurationManager.ConnectionStrings[InsightsMetaDataConnName].ConnectionString;
            _dwConnString = ConfigurationManager.ConnectionStrings[InsightsDWConnName].ConnectionString;
            _insightsConnString = ConfigurationManager.ConnectionStrings[InsightsConnName].ConnectionString;
        }

        public DataAccessLayer(string csMetadata, string csDW, string csInsights)
        {
            _metaDataConnString = csMetadata;
            _dwConnString = csDW;
            _insightsConnString = csInsights;
        }




        private string GetConnectionStringInsightsDW(int clientID)
        {
            string connString;

            //Check if PaaS, and modify connection string
            string sPaaS = ConfigurationManager.AppSettings[AppSetting_PaaSOnly];
            if (!string.IsNullOrEmpty(sPaaS))
            {
                if (System.Convert.ToBoolean(sPaaS))
                {
                    string sPaaSDedicatedClients = ConfigurationManager.AppSettings[AppSetting_PaaSDedicatedClients];
                    if (!string.IsNullOrEmpty(sPaaSDedicatedClients))
                    {
                        var cid = clientID.ToString();
                        connString = sPaaSDedicatedClients.Split(',').ToList().Contains(cid) ? 
                            _dwConnString.Replace("InsightsDW;", "InsightsDW_" + cid + ";") : 
                            _dwConnString.Replace("InsightsDW;", "InsightsDW_Test;");
                    }
                    else
                    {
                        connString = _dwConnString.Replace("InsightsDW;", "InsightsDW_Test;");
                    }
                }
                else
                {
                    connString = _dwConnString;
                }
            }
            else
            {
                connString = _dwConnString;
            }

            return (connString);
        }

        //private string GetConnectionStringInsightsDW()
        //{
        //    return (_dwConnString);
        //}

        private string GetConnectionStringInsights()
        {
            return (_insightsConnString);
        }

        private string GetConnectionStringMetaData()
        {
            return (_metaDataConnString);
        }

        /// <summary>
        /// Get Region Parameters from EMZipcode using Zipcode to select.
        /// </summary>
        /// <returns></returns>
        public RegionParams GetRegionParametersByClientZipcode(int clientID, string zipcode)
        {
            RegionParams regionParams = null;
            const string storedProcedureName = "[cm].[uspEMSelectRegionParamsByClientZipcode]";

            using (var connection = new SqlConnection(GetConnectionStringMetaData()))
            {
                using (var cmd = new SqlCommand(storedProcedureName, connection))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@ClientID", typeof(int)) { Value = clientID });
                    cmd.Parameters.Add(new SqlParameter("@Zipcode", typeof(string)) { Value = zipcode });

                    connection.Open();

                    using (var reader = cmd.ExecuteReader())
                    {
                        if (reader.HasRows)
                        {
                            regionParams = new RegionParams();

                            // User with client & user properties
                            while (reader.Read())
                            {
                                regionParams.TemperatureRegionId = reader["TemperatureRegionId"] as int? ?? default(int);
                                regionParams.ApplianceRegionId = reader["ApplianceRegionId"] as int? ?? default(int);
                                regionParams.SolarRegionId = reader["SolarRegionId"] as int? ?? default(int);
                                regionParams.State = reader["State"] as string;
                                regionParams.StationId = reader["StationIdDaily"] as string;
                                regionParams.ClimateId = reader["ClimateId"] as int? ?? default(int);
                                regionParams.CountryCode = reader["CountryCode"] as string;
                            }
                        }

                    }

                }
            }

            return regionParams;
        }


        /// <summary>
        /// Get zipcode via a customer lookup in the data warehouse.
        /// </summary>
        /// <param name="clientId"></param>
        /// <param name="customerId"></param>
        /// <param name="accountId"></param>
        /// <param name="premiseId"></param>
        /// <returns></returns>
        public string GetZipcodeByCustomer(int clientId, string customerId, string accountId, string premiseId)
        {
            string zipcode = string.Empty;
            const string storedProcedureName = "[dbo].[GetCustomerBasic]";

            using (SqlConnection connection = new SqlConnection(GetConnectionStringInsightsDW(clientId)))
            {
                using (var cmd = new SqlCommand(storedProcedureName, connection))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@ClientID", typeof(int)) { Value = clientId });
                    cmd.Parameters.Add(new SqlParameter("@CustomerID", typeof(string)) { Value = customerId });
                    cmd.Parameters.Add(new SqlParameter("@AccountID", typeof(string)) { Value = accountId });
                    cmd.Parameters.Add(new SqlParameter("@PremiseID", typeof(string)) { Value = premiseId });

                    connection.Open();

                    using (var reader = cmd.ExecuteReader())
                    {
                        if (reader.HasRows)
                        {
                            var customerZipcode = string.Empty;
                            var premiseZipcode = string.Empty;

                            // User with client & user properties
                            while (reader.Read())
                            {
                                customerZipcode = reader["CustomerPostalCode"] as string;
                                premiseZipcode = reader["PremisePostalCode"] as string;
                            }

                            if (!string.IsNullOrEmpty(premiseZipcode) && premiseZipcode.Length >= 5)
                            {
                                zipcode = premiseZipcode.Substring(0, 5);
                            }
                            else if (!string.IsNullOrEmpty(customerZipcode) && customerZipcode.Length >= 5)
                            {
                                zipcode = customerZipcode.Substring(0, 5);
                            }

                        }

                    }

                }
            }

            return (zipcode);
        }



        #region GetDefaultProfileName

        public string GetDefaultProfileName(int clientId, int applianceRegionId)
        {
            return (GetDefaultProfileName_Cache(clientId, applianceRegionId));
            //return (GetDefaultProfileName_Database());  //use this when debugging the database retrieval
        }

        private string GetDefaultProfileName_Cache(int clientId, int applianceRegionId)
        {
            const string cacheKeyTemplate = "DefProfileName-{0}";

            var cacheKeySB = new StringBuilder();

            var cacheKey = cacheKeySB.AppendFormat(cacheKeyTemplate, applianceRegionId).ToString();

            var x = CacheLayer.Get<string>(cacheKey);

            if (x == null)
            {
                x = GetDefaultProfileName_Database(clientId, applianceRegionId);

                if (x != null)
                {
                    CacheLayer.Add(x, cacheKey, _cacheTimeoutInMinutes);
                }
            }

            return x;
        }



        /// <summary>
        /// Get Default Profile Name to use for Profile attributes retrieval.
        /// </summary>
        /// <param name="clientId"></param>
        /// <param name="applianceRegionId"></param>
        /// <returns></returns>
        private string GetDefaultProfileName_Database(int clientId, int applianceRegionId)
        {
            string name = string.Empty;
            const string storedProcedureName = "[cm].[uspEMSelectDefaultProfileName]";

            using (SqlConnection connection = new SqlConnection(GetConnectionStringMetaData()))
            {
                using (var cmd = new SqlCommand(storedProcedureName, connection))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@ClientID", typeof(int)) { Value = clientId });
                    cmd.Parameters.Add(new SqlParameter("@ApplianceRegionID", typeof(int)) { Value = applianceRegionId });

                    connection.Open();

                    using (var reader = cmd.ExecuteReader())
                    {
                        if (reader.HasRows)
                        {
                            // User with client & user properties
                            while (reader.Read())
                            {
                                name = reader["ProfileCollectionName"] as string;
                            }

                        }

                    }

                }
            }

            return (name);
        }

        #endregion



        #region GetProfileAttributes

        public ProfileAttributeCollection GetProfileAttributes(int clientId, string customerId, string accountId,
            string premiseId)
        {
            ProfileAttributeCollection pas = null;
            const string storedProcedureNameInsights = "[wh].[uspEMGetProfileAttributes]"; // within Insights
            const string storedProcedureNameInsightsDW = "[dbo].[uspEMGetProfilePremiseAttributes]";
            // within InsightsDW

            if (!string.IsNullOrEmpty(GetConnectionStringInsights()))
            {
                // try and get attributes from Insights first
                using (SqlConnection connection = new SqlConnection(GetConnectionStringInsights())) //Insights
                {
                    using (var cmd = new SqlCommand(storedProcedureNameInsights, connection)) //Insights
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Add(new SqlParameter("@ClientID", typeof(int)) { Value = clientId });
                        cmd.Parameters.Add(new SqlParameter("@CustomerID", typeof(string)) { Value = customerId });
                        cmd.Parameters.Add(new SqlParameter("@AccountID", typeof(string)) { Value = accountId });
                        cmd.Parameters.Add(new SqlParameter("@PremiseID", typeof(string)) { Value = premiseId });

                        connection.Open();

                        using (var reader = cmd.ExecuteReader())
                        {
                            if (reader.HasRows)
                            {
                                pas = new ProfileAttributeCollection();

                                while (reader.Read())
                                {
                                    var pa = new ProfileAttribute
                                    {
                                        Key = reader["AttributeKey"] as string,
                                        OptionKey = reader["Value"] as string,
                                        OptionValue = reader["Value"] as string
                                    };

                                    //If the OptionKey is null, then the data we have is bad, and we must skip this profile attribute
                                    if (pa.OptionKey != null)
                                    {

                                        if (pa.OptionKey.Contains(pa.Key))
                                        {
                                            pa.OptionValue = string.Empty;
                                            if (pa.Key + ".no" == pa.OptionKey)
                                            {
                                                pa.OptionValue = "0"; //FALSE
                                            }
                                            else if (pa.Key + ".yes" == pa.OptionKey)
                                            {
                                                pa.OptionValue = "1"; //TRUE
                                            }
                                        }
                                        else
                                        {
                                            pa.OptionKey = pa.Key + ".value";
                                        }

                                        var tempsrc = reader["Source"] as string;
                                        if (tempsrc != null &&
                                            (tempsrc.ToLower().Equals("customer") || tempsrc.ToLower().Equals("user")))
                                        {
                                            pa.Source = "U"; //the user
                                        }
                                        else
                                        {
                                            pa.Source = "U"; //all from here are User for now
                                            //pa.Source = "C";    //the client or utility
                                        }

                                        pas.Add(pa);

                                    }

                                }

                            }

                        }

                    }

                }
            }

            if (pas == null)
            {
                // now try instead to get attributes from InsightsDW
                using (SqlConnection connection = new SqlConnection(GetConnectionStringInsightsDW(clientId)))
                //InsightsDW
                {
                    using (var cmd = new SqlCommand(storedProcedureNameInsightsDW, connection)) //InsightsDW
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Add(new SqlParameter("@ClientID", typeof(int)) { Value = clientId });
                        cmd.Parameters.Add(new SqlParameter("@CustomerID", typeof(string)) { Value = customerId });
                        cmd.Parameters.Add(new SqlParameter("@AccountID", typeof(string)) { Value = accountId });
                        cmd.Parameters.Add(new SqlParameter("@PremiseID", typeof(string)) { Value = premiseId });

                        connection.Open();

                        using (var reader = cmd.ExecuteReader())
                        {
                            if (reader.HasRows)
                            {
                                pas = new ProfileAttributeCollection();

                                while (reader.Read())
                                {
                                    var pa = new ProfileAttribute
                                    {
                                        Key = reader["AttributeKey"] as string,
                                        OptionKey = reader["Value"] as string,
                                        OptionValue = reader["Value"] as string
                                    };
                                    if (pa.OptionKey.Contains(pa.Key))
                                    {
                                        pa.OptionValue = string.Empty;
                                        if (pa.Key + ".no" == pa.OptionKey)
                                        {
                                            pa.OptionValue = "0"; //FALSE
                                        }
                                        else if (pa.Key + ".yes" == pa.OptionKey)
                                        {
                                            pa.OptionValue = "1"; //TRUE
                                        }

                                    }
                                    else
                                    {
                                        pa.OptionKey = pa.Key + ".value";
                                    }

                                    var tempsrc = reader["Source"] as string;
                                    if (tempsrc != null &&
                                        (tempsrc.ToLower().Equals("customer") || tempsrc.ToLower().Equals("user")))
                                    {
                                        pa.Source = "U"; //the user
                                    }
                                    else
                                    {
                                        pa.Source = "U"; //all from here are User for now
                                        //pa.Source = "C";    //the client or utility
                                    }

                                    pas.Add(pa);
                                }

                            }

                        }

                    }

                }

            }

            return (pas);
        }


        #endregion


        #region GetBuildingEnergyUseIntensityList


        public List<BuildingEnergyUseIntensity> GetBuildingEnergyUseIntensityList(int clientId)
        {
            List<BuildingEnergyUseIntensity> euiList = null;
            const string storedProcedureName = "[cm].[uspBEMSelectBuildingEUIs]";

            using (SqlConnection connection = new SqlConnection(GetConnectionStringMetaData()))
            {
                using (var cmd = new SqlCommand(storedProcedureName, connection))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@ClientID", typeof(int)) { Value = clientId });

                    connection.Open();

                    using (var reader = cmd.ExecuteReader())
                    {
                        if (reader.HasRows)
                        {
                            // create list
                            euiList = new List<BuildingEnergyUseIntensity>();

                            while (reader.Read())
                            {
                                //fill list
                                var euiItem = new BuildingEnergyUseIntensity()
                                {
                                    CategoryId = System.Convert.ToInt32(reader["BuildingCategoryId"]),
                                    SizeId = System.Convert.ToInt32(reader["BuildingSizeId"]),
                                    CommodityId = System.Convert.ToInt32(reader["CommodityId"]),
                                    ClimateId = System.Convert.ToInt32(reader["ClimateId"]),
                                    EuiHeating = System.Convert.ToDecimal(reader["EUIHeating"]),
                                    EuiCooling = System.Convert.ToDecimal(reader["EUICooling"]),
                                    EuiComputers = System.Convert.ToDecimal(reader["EUIComputers"]),
                                    EuiCooking = System.Convert.ToDecimal(reader["EUICooking"]),
                                    EuiLaundry = System.Convert.ToDecimal(reader["EUILaundry"]),
                                    EuiLighting = System.Convert.ToDecimal(reader["EUILighting"]),
                                    EuiMiscellaneous = System.Convert.ToDecimal(reader["EUIMiscellaneous"]),
                                    EuiOfficeEquipment = System.Convert.ToDecimal(reader["EUIOfficeEquipment"]),
                                    EuiRefrigeration = System.Convert.ToDecimal(reader["EUIRefrigeration"]),
                                    EuiVentilation = System.Convert.ToDecimal(reader["EUIVentilation"]),
                                    EuiWaterHeating = System.Convert.ToDecimal(reader["EUIWaterHeating"]),
                                    CategoryName = System.Convert.ToString(reader["BuildingCategoryName"])
                                };

                                euiList.Add(euiItem);
                            }

                        }

                    }

                }

            }

            return (euiList);
        }


        #endregion

    }

}
