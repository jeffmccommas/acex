﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using CE.EnergyModel.Cache;
using CE.EnergyModel.Entities;

namespace CE.EnergyModel.DataAccess
{
    public partial class DataAccessLayer
    {

        #region DegreeDaysLUT

        public DegreeDaysCollection GetDegreeDaysLUT()
        {
            return (GetDegreeDaysLUT_Cache());
            //return (GetAnnualCDDLUT_Database());  //use this when debugging the database retrieval
        }

        private DegreeDaysCollection GetDegreeDaysLUT_Cache()
        {
            DegreeDaysCollection x;
            const string cacheKeyTemplate = "DegreeDays-{0}";
            string someparam = "ALL";

            StringBuilder cacheKeySB = new StringBuilder();
            string cacheKey;

            cacheKey = cacheKeySB.AppendFormat(cacheKeyTemplate, someparam).ToString();

            x = CacheLayer.Get<DegreeDaysCollection>(cacheKey);

            if (x == null)
            {
                x = GetDegreeDaysLUT_Database();

                if (x != null)
                {
                    CacheLayer.Add(x, cacheKey, _cacheTimeoutInMinutes);
                }
                else
                {
                    x = null;
                }
            }

            return (x);
        }

        private DegreeDaysCollection GetDegreeDaysLUT_Database()
        {
            DegreeDaysCollection ddc = null;
            const string storedProcedureName = "[cm].[uspEMSelectDegreeDays]";

            using (SqlConnection connection = new SqlConnection(GetConnectionStringMetaData()))
            {
                using (var cmd = new SqlCommand(storedProcedureName, connection))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    connection.Open();

                    using (var reader = cmd.ExecuteReader())
                    {
                        if (reader.HasRows)
                        {
                            ddc = new DegreeDaysCollection();

                            // User with client & user properties
                            while (reader.Read())
                            {

                                ddc.Add(new DegreeDays(Convert.ToInt32(reader["TemperatureRegionId"]), Convert.ToInt32(reader["HDD_ANN"]), Convert.ToInt32(reader["CDD_ANN"]),
                                    new int[] { 0, Convert.ToInt32(reader["HDD_JAN"]), Convert.ToInt32(reader["HDD_FEB"]), Convert.ToInt32(reader["HDD_MAR"]), Convert.ToInt32(reader["HDD_APR"]), Convert.ToInt32(reader["HDD_MAY"]), Convert.ToInt32(reader["HDD_JUN"]), Convert.ToInt32(reader["HDD_JUL"]), Convert.ToInt32(reader["HDD_AUG"]), Convert.ToInt32(reader["HDD_SEP"]), Convert.ToInt32(reader["HDD_OCT"]), Convert.ToInt32(reader["HDD_NOV"]), Convert.ToInt32(reader["HDD_DEC"]) },
                                    new int[] { 0, Convert.ToInt32(reader["CDD_JAN"]), Convert.ToInt32(reader["CDD_FEB"]), Convert.ToInt32(reader["CDD_MAR"]), Convert.ToInt32(reader["CDD_APR"]), Convert.ToInt32(reader["CDD_MAY"]), Convert.ToInt32(reader["CDD_JUN"]), Convert.ToInt32(reader["CDD_JUL"]), Convert.ToInt32(reader["CDD_AUG"]), Convert.ToInt32(reader["CDD_SEP"]), Convert.ToInt32(reader["CDD_OCT"]), Convert.ToInt32(reader["CDD_NOV"]), Convert.ToInt32(reader["CDD_DEC"]) }));

                            }
                        }

                    }

                }
            }

            return (ddc);
        }

        #endregion


        #region EvaporationLUT

        public EvaporationCollection GetEvaporationLUT()
        {
            return (GetEvaporationLUT_Cache());
            //return (GetAnnualCDDLUT_Database());  //use this when debugging the database retrieval
        }

        private EvaporationCollection GetEvaporationLUT_Cache()
        {
            EvaporationCollection x;
            const string cacheKeyTemplate = "Evaporation-{0}";
            string someparam = "ALL";

            StringBuilder cacheKeySB = new StringBuilder();
            string cacheKey;

            cacheKey = cacheKeySB.AppendFormat(cacheKeyTemplate, someparam).ToString();

            x = CacheLayer.Get<EvaporationCollection>(cacheKey);

            if (x == null)
            {
                x = GetEvaporationLUT_Database();

                if (x != null)
                {
                    CacheLayer.Add(x, cacheKey, _cacheTimeoutInMinutes);
                }
                else
                {
                    x = null;
                }
            }

            return (x);
        }

        private EvaporationCollection GetEvaporationLUT_Database()
        {
            EvaporationCollection c = null;
            const string storedProcedureName = "[cm].[uspEMSelectEvaporation]";

            using (SqlConnection connection = new SqlConnection(GetConnectionStringMetaData()))
            {
                using (var cmd = new SqlCommand(storedProcedureName, connection))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    connection.Open();

                    using (var reader = cmd.ExecuteReader())
                    {
                        if (reader.HasRows)
                        {
                            c = new EvaporationCollection();

                            while (reader.Read())
                            {

                                c.Add(new Evaporation(reader["State"].ToString(), Convert.ToDouble(reader["ANN"]),
                                    new double[] { 0, Convert.ToDouble(reader["JAN"]), Convert.ToDouble(reader["FEB"]), Convert.ToDouble(reader["MAR"]), 
                                        Convert.ToDouble(reader["APR"]), Convert.ToDouble(reader["MAY"]), Convert.ToDouble(reader["JUN"]),
                                        Convert.ToDouble(reader["JUL"]), Convert.ToDouble(reader["AUG"]), Convert.ToDouble(reader["SEP"]), 
                                        Convert.ToDouble(reader["OCT"]), Convert.ToDouble(reader["NOV"]), Convert.ToDouble(reader["DEC"]) }));

                            }
                        }

                    }

                }
            }

            return (c);
        }

        #endregion


        #region SolarLUT

        public SolarCollection GetSolarLUT()
        {
            return (GetSolarLUT_Cache());
            //return (GetSolarLUT_Database());  //use this when debugging the database retrieval
        }

        private SolarCollection GetSolarLUT_Cache()
        {
            SolarCollection x;
            const string cacheKeyTemplate = "Solar-{0}";
            string someparam = "ALL";

            StringBuilder cacheKeySB = new StringBuilder();
            string cacheKey;

            cacheKey = cacheKeySB.AppendFormat(cacheKeyTemplate, someparam).ToString();

            x = CacheLayer.Get<SolarCollection>(cacheKey);

            if (x == null)
            {
                x = GetSolarLUT_Database();

                if (x != null)
                {
                    CacheLayer.Add(x, cacheKey, _cacheTimeoutInMinutes);
                }
                else
                {
                    x = null;
                }
            }

            return (x);
        }

        private SolarCollection GetSolarLUT_Database()
        {
            SolarCollection c = null;
            const string storedProcedureName = "[cm].[uspEMSelectSolar]";

            using (SqlConnection connection = new SqlConnection(GetConnectionStringMetaData()))
            {
                using (var cmd = new SqlCommand(storedProcedureName, connection))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    connection.Open();

                    using (var reader = cmd.ExecuteReader())
                    {
                        if (reader.HasRows)
                        {
                            c = new SolarCollection();

                            int solarRegionId = -1;
                            int lastSolarRegionId = -1;
                            int season = 0;
                            int hour = 0;
                            bool firstTime = true;
                            Solar s = null;

                            while (reader.Read())
                            {
                                solarRegionId = Convert.ToInt32(reader["SolarRegionId"]);
                                if (solarRegionId != lastSolarRegionId)
                                {
                                    if (firstTime)
                                    {
                                        firstTime = false;
                                    }
                                    else
                                    {
                                        c.Add(s);
                                    }
                                    s = new Solar() { SolarRegionId = solarRegionId };
                                    lastSolarRegionId = solarRegionId;
                                }
                                season = Convert.ToInt32(reader["Season"]);
                                hour = Convert.ToInt32(reader["Hour"]);
                                s.Factor[season, 0, hour - 1] = Convert.ToDouble(reader["North"]);
                                s.Factor[season, 1, hour - 1] = Convert.ToDouble(reader["East"]);
                                s.Factor[season, 2, hour - 1] = Convert.ToDouble(reader["South"]);
                                s.Factor[season, 3, hour - 1] = Convert.ToDouble(reader["West"]);
                            }
                            c.Add(s);   // add the final item

                        }

                    }

                }
            }

            return (c);
        }

        #endregion


        #region TemperatureRegionLUT

        public TemperatureRegionCollection GetTemperatureRegionLUT()
        {
            return (GetTemperatureRegionLUT_Cache());
            //return (GetTemperatureRegionLUT_Database());  //use this when debugging the database retrieval
        }

        private TemperatureRegionCollection GetTemperatureRegionLUT_Cache()
        {
            TemperatureRegionCollection x;
            const string cacheKeyTemplate = "TemperatureRegion-{0}";
            string someparam = "ALL";

            StringBuilder cacheKeySB = new StringBuilder();
            string cacheKey;

            cacheKey = cacheKeySB.AppendFormat(cacheKeyTemplate, someparam).ToString();

            x = CacheLayer.Get<TemperatureRegionCollection>(cacheKey);

            if (x == null)
            {
                x = GetTemperatureRegionLUT_Database();

                if (x != null)
                {
                    CacheLayer.Add(x, cacheKey, _cacheTimeoutInMinutes);
                }
                else
                {
                    x = null;
                }
            }

            return (x);
        }

        private TemperatureRegionCollection GetTemperatureRegionLUT_Database()
        {
            TemperatureRegionCollection c = null;
            const string storedProcedureName = "[cm].[uspEMSelectTemperatureRegion]";

            using (SqlConnection connection = new SqlConnection(GetConnectionStringMetaData()))
            {
                using (var cmd = new SqlCommand(storedProcedureName, connection))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    connection.Open();

                    using (var reader = cmd.ExecuteReader())
                    {
                        if (reader.HasRows)
                        {
                            c = new TemperatureRegionCollection();

                            while (reader.Read())
                            {

                                c.Add(new TemperatureRegion(Convert.ToInt32(reader["TemperatureRegionId"]),
                                    Convert.ToString(reader["State"]), Convert.ToInt32(reader["GroundWaterTemp"]), Convert.ToInt32(reader["HeatDesignTemp"]),
                                    Convert.ToInt32(reader["CoolDesignTemp"]), Convert.ToInt32(reader["CoolDesignWetBulb"]), Convert.ToDecimal(reader["StandardPressure"])));

                            }
                        }

                    }

                }
            }

            return (c);
        }

        #endregion


        #region TemperatureProfileLUT (dry,wet)

        public TemperatureProfileCollection GetTemperatureProfileLUT()
        {
            return (GetTemperatureProfileLUT_Cache());
            //return (GetSolarLUT_Database());  //use this when debugging the database retrieval
        }

        private TemperatureProfileCollection GetTemperatureProfileLUT_Cache()
        {
            TemperatureProfileCollection x;
            const string cacheKeyTemplate = "TemperatureProfile-{0}";
            string someparam = "ALL";

            StringBuilder cacheKeySB = new StringBuilder();
            string cacheKey;

            cacheKey = cacheKeySB.AppendFormat(cacheKeyTemplate, someparam).ToString();

            x = CacheLayer.Get<TemperatureProfileCollection>(cacheKey);

            if (x == null)
            {
                x = GetTemperatureProfileLUT_Database();

                if (x != null)
                {
                    CacheLayer.Add(x, cacheKey, _cacheTimeoutInMinutes);
                }
                else
                {
                    x = null;
                }
            }

            return (x);
        }

        private TemperatureProfileCollection GetTemperatureProfileLUT_Database()
        {
            TemperatureProfileCollection c = null;
            const string storedProcedureName = "[cm].[uspEMSelectTemperatureProfile]";

            using (SqlConnection connection = new SqlConnection(GetConnectionStringMetaData()))
            {
                using (var cmd = new SqlCommand(storedProcedureName, connection))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    connection.Open();

                    using (var reader = cmd.ExecuteReader())
                    {
                        if (reader.HasRows)
                        {
                            c = new TemperatureProfileCollection();

                            int temperatureRegionId = -1;
                            int lastTemperatureRegionId = -1;
                            int dryOrWet = 0;
                            int hour = 0;
                            bool firstTime = true;
                            TemperatureProfile s = null;

                            while (reader.Read())
                            {
                                temperatureRegionId = Convert.ToInt32(reader["TemperatureRegionId"]);
                                if (temperatureRegionId != lastTemperatureRegionId)
                                {
                                    if (firstTime)
                                    {
                                        firstTime = false;
                                    }
                                    else
                                    {
                                        c.Add(s);
                                    }
                                    s = new TemperatureProfile() { TemperatureRegionId = temperatureRegionId };
                                    lastTemperatureRegionId = temperatureRegionId;
                                }
                                dryOrWet = Convert.ToInt32(reader["Type"]);
                                hour = Convert.ToInt32(reader["Hour"]);
                                s.Factor[dryOrWet, 0, hour - 1] = Convert.ToDecimal(reader["jan"]);
                                s.Factor[dryOrWet, 1, hour - 1] = Convert.ToDecimal(reader["feb"]);
                                s.Factor[dryOrWet, 2, hour - 1] = Convert.ToDecimal(reader["mar"]);
                                s.Factor[dryOrWet, 3, hour - 1] = Convert.ToDecimal(reader["apr"]);
                                s.Factor[dryOrWet, 4, hour - 1] = Convert.ToDecimal(reader["may"]);
                                s.Factor[dryOrWet, 5, hour - 1] = Convert.ToDecimal(reader["jun"]);
                                s.Factor[dryOrWet, 6, hour - 1] = Convert.ToDecimal(reader["jul"]);
                                s.Factor[dryOrWet, 7, hour - 1] = Convert.ToDecimal(reader["aug"]);
                                s.Factor[dryOrWet, 8, hour - 1] = Convert.ToDecimal(reader["sep"]);
                                s.Factor[dryOrWet, 9, hour - 1] = Convert.ToDecimal(reader["oct"]);
                                s.Factor[dryOrWet, 10, hour - 1] = Convert.ToDecimal(reader["nov"]);
                                s.Factor[dryOrWet, 11, hour - 1] = Convert.ToDecimal(reader["dec"]);

                            }
                            c.Add(s);   // add the final item

                        }

                    }

                }
            }

            return (c);
        }

        #endregion


        #region ActualDailyWeather

        /// <summary>
        /// This gets the HDD and CDD from Actual Daily weather.  Sum over the time period.
        /// </summary>
        /// <param name="stationId">The weather station Id</param>
        /// <param name="startDate">The start date</param>
        /// <param name="endDate">The end date</param>
        /// <returns>An ActualWeather object</returns>
        public ActualWeather GetActualWeather(string stationId, DateTime startDate, DateTime endDate)
        {
            ActualWeather actual = new ActualWeather();
            const string storedProcedureName = "[cm].[uspEMSelectDailyWeatherTotal]";

            using (SqlConnection connection = new SqlConnection(GetConnectionStringMetaData()))
            {
                using (var cmd = new SqlCommand(storedProcedureName, connection))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@StationId", typeof(string)) { Value = stationId });
                    cmd.Parameters.Add(new SqlParameter("@StartDate", typeof(DateTime)) { Value = startDate });
                    cmd.Parameters.Add(new SqlParameter("@EndDate", typeof(DateTime)) { Value = endDate });

                    connection.Open();

                    using (var reader = cmd.ExecuteReader())
                    {
                        if (reader.HasRows)
                        {
                            int? hdd = 0;
                            int? cdd = 0;

                            // User with client & user properties
                            while (reader.Read())
                            {
                                hdd = reader["HDD"] as int? ?? default(int);
                                cdd = reader["CDD"] as int? ?? default(int);
                            }

                            actual.TotalHDD = hdd.Value;
                            actual.TotalCDD = cdd.Value;
                        }

                    }

                }

            }

            return (actual);
        }

        #endregion


    }
}
