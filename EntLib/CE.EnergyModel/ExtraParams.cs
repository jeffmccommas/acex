﻿
namespace CE.EnergyModel
{
    public class ExtraParams
    {
        private const int _defaultChunkSize = 100; //make this 15 to group appliances together for async task based calculations

        /// <summary>
        /// To trigger monthly energy use for each appliance.
        /// </summary>
        public bool OutputMonthlyEnergyUse { get; set; }

        /// <summary>
        /// To trigger debug details with the calculations.
        /// </summary>
        public bool OutputTraceDetails { get; set; }

        /// <summary>
        /// Chunk size for processing.
        /// </summary>
        public int ChunkSize { get; set; }

        /// <summary>
        /// The calculation shall be a business calculation rather than residential.
        /// </summary>
        public bool IsBusiness { get; set; }

        /// <summary>
        /// To trigger a bill disaggregation that encompasses an energy model calculation.  Uses full model.
        /// </summary>
        public bool ExecuteBillDisagg { get; set; }

        public ExtraParams()
        {
            OutputMonthlyEnergyUse = false;
            OutputTraceDetails = false;
            ChunkSize = _defaultChunkSize;
            ExecuteBillDisagg = false;
        }

    }
}
