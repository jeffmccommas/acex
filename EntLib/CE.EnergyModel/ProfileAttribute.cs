﻿
namespace CE.EnergyModel
{
    /// <summary>
    /// ProfileAttribute
    /// Ex:
    /// Key=atticfan.kwhperuse 
    /// OptionKey=atticfan.kwhperuse.value 
    /// OptionValue=10.0
    /// Source= D|C|U|P (aka: Default, Client, User, Passed)
    /// 
    /// </summary>
    public class ProfileAttribute
    {
        public string Key { get; set; }
        public string OptionKey { get; set; }
        public string OptionValue { get; set; }
        public string Source { get; set; }

        public ProfileAttribute()
        {
        }

        public ProfileAttribute Clone()
        {
            var pa = new ProfileAttribute();

            pa.Source = this.Source;
            pa.Key = this.Key;
            pa.OptionKey = this.OptionKey;
            pa.OptionValue = this.OptionValue;

            return (pa);
        }

    }
}
