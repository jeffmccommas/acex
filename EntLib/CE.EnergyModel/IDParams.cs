﻿
namespace CE.EnergyModel
{
    public class IDParams
    {
        public string CustomerId { get; set; }
        public string AccountId { get; set; }
        public string PremiseId { get; set; }
        public string Zipcode { get; set; }

        public IDParams()
        {

        }

        public IDParams( string zipcode ) : this()
        {
            Zipcode = zipcode;
        }

    }
}
