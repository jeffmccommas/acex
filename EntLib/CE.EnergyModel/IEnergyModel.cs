﻿
namespace CE.EnergyModel
{
    public interface IEnergyModel
    {

        EnergyModelResult ExecuteEnergyModel(string zipcode);
        EnergyModelResult ExecuteEnergyModel(IDParams idParams);
        EnergyModelResult ExecuteEnergyModel(IDParams idParams, ProfileAttributeCollection profileAttributes, ApplianceCollection appliances);
        EnergyModelResult ExecuteEnergyModel(IDParams idParams, ProfileAttributeCollection profileAttributes, ApplianceCollection appliances, EndUseCollection endUses);
        EnergyModelResult ExecuteEnergyModel(IDParams idParams, ExtraParams extraParams, ProfileAttributeCollection profileAttributes, ApplianceCollection appliances, EndUseCollection endUses);

        EnergyModelResult ExecuteEnergyModel(RegionParams regionParams);
        EnergyModelResult ExecuteEnergyModel(RegionParams regionParams, IDParams idParams);
        EnergyModelResult ExecuteEnergyModel(RegionParams regionParams, IDParams idParams, ProfileAttributeCollection profileAttributes, ApplianceCollection appliances);
        EnergyModelResult ExecuteEnergyModel(RegionParams regionParams, IDParams idParams, ProfileAttributeCollection profileAttributes, ApplianceCollection appliances, EndUseCollection endUses);
        EnergyModelResult ExecuteEnergyModel(RegionParams regionParams, IDParams idParams, ExtraParams extraParams, ProfileAttributeCollection profileAttributes, ApplianceCollection appliances, EndUseCollection endUses);

    }

}
