﻿using System;
using System.Collections.Generic;
using CE.EnergyModel.DataAccess;
using CE.Models;
using CE.Models.Insights;
using CE.Models.Insights.Types;
using CommodityTypeEnergy = CE.EnergyModel.Enums.CommodityType;

namespace CE.EnergyModel.Helper
{
    public class WeatherCalc
    {
        /// <summary>
        /// Adjust the Energy Model results by Weather and calculate Highlights
        /// </summary>
        /// <param name="startDate">The start date</param>
        /// <param name="endDate">The end date</param>
        /// <param name="commodities">A list of commodities to limit usage.</param>
        /// <param name="emResult">An EnergyModel Result object.</param>
        public void AdjustModeledEnergyUsingActualWeather(DateTime startDate, DateTime endDate,
            List<CommodityTypeEnergy> commodities, EnergyModelResult emResult)
        {
            var dataAccessLayer = new DataAccessLayer();
            var actualWeather = dataAccessLayer.GetActualWeather(emResult.RegionParams.StationId, startDate, endDate);

            decimal actualHDDFinal = actualWeather.TotalHDD;
            if (actualHDDFinal < 120)
            {
                actualHDDFinal = 0;
            }

            decimal actualCDDFinal = actualWeather.TotalCDD;
            if (actualCDDFinal < 60)
            {
                actualCDDFinal = 0;
            }

            // Calc the weatherAdjustmentFactors
            var weatherAdjustmentFactorHDD = actualHDDFinal/emResult.AnnualHDD;
            var weatherAdjustmentFactorCDD = actualCDDFinal/emResult.AnnualCDD;

            // Add the factors to the highlights.
            emResult.Highlights.WeatherAdjustmentFactorHDD = weatherAdjustmentFactorHDD;
            emResult.Highlights.WeatherAdjustmentFactorCDD = weatherAdjustmentFactorCDD;

            // Start the usage counters.
            emResult.Highlights.BaseFuelLoad = 0;
            emResult.Highlights.TotalCoolLoad = 0;
            emResult.Highlights.TotalHeatLoad = 0;

            var heatingEndUse =
                emResult.EndUses.Find(p => p.Name.Equals("heating", StringComparison.InvariantCultureIgnoreCase) ||
                                           p.Name.Equals("businessheating", StringComparison.InvariantCultureIgnoreCase));

            var coolingEndUse =
                emResult.EndUses.Find(p => p.Name.Equals("cooling", StringComparison.InvariantCultureIgnoreCase) ||
                                           p.Name.Equals("businesscooling", StringComparison.InvariantCultureIgnoreCase));

            var baseEndUse =
                emResult.EndUses.Find(p => !p.Name.Equals("heating", StringComparison.InvariantCultureIgnoreCase) &&
                                           !p.Name.Equals("businessheating", StringComparison.InvariantCultureIgnoreCase) &&
                                           !p.Name.Equals("cooling", StringComparison.InvariantCultureIgnoreCase) &&
                                           !p.Name.Equals("businesscooling", StringComparison.InvariantCultureIgnoreCase));

            // iterate the commodities within energy model that have two or more bills; in this case appliances in heating and cooling end uses!
            foreach (var commodity in commodities)
            {
                //heating, adjust quantity based on weather 
                if (heatingEndUse != null)
                {
                    foreach (var name in heatingEndUse.ApplianceNames)
                    {
                        var heatingAppliance = emResult.Appliances.Find(p => p.Name == name);
                        var energyuse = heatingAppliance?.EnergyUses.Find(p => p.Commodity == commodity);

                        if (energyuse != null)
                        {
                            emResult.Highlights.TotalHeatLoad = emResult.Highlights.TotalHeatLoad + energyuse.Quantity;
                            energyuse.Quantity = energyuse.Quantity*weatherAdjustmentFactorHDD;
                            energyuse.WeatherAdjustmentFactor = weatherAdjustmentFactorHDD;
                        }
                    }
                }

                //cooling, adjust quantity based on weather 
                if (coolingEndUse != null)
                {
                    foreach (var name in coolingEndUse.ApplianceNames)
                    {
                        var coolingAppliance = emResult.Appliances.Find(p => p.Name == name);
                        var energyuse = coolingAppliance?.EnergyUses.Find(p => p.Commodity == commodity);

                        if (energyuse != null)
                        {
                            emResult.Highlights.TotalCoolLoad = emResult.Highlights.TotalCoolLoad + energyuse.Quantity;
                            energyuse.Quantity = energyuse.Quantity*(weatherAdjustmentFactorCDD);
                            energyuse.WeatherAdjustmentFactor = weatherAdjustmentFactorCDD;
                        }
                    }
                }

                //base use
                foreach (var endUse in emResult.EndUses)
                {
                    if (!endUse.Name.Equals("heating", StringComparison.InvariantCultureIgnoreCase) &&
                        !endUse.Name.Equals("businessheating", StringComparison.InvariantCultureIgnoreCase) &&
                        !endUse.Name.Equals("cooling", StringComparison.InvariantCultureIgnoreCase) &&
                        !endUse.Name.Equals("businesscooling", StringComparison.InvariantCultureIgnoreCase))
                    {
                        foreach (var name in endUse.ApplianceNames)
                        {
                            var baseAppliance = emResult.Appliances.Find(p => p.Name == name);
                            var energyuse = baseAppliance?.EnergyUses.Find(p => p.Commodity == commodity);

                            if (energyuse != null)
                            {
                                emResult.Highlights.BaseFuelLoad = emResult.Highlights.BaseFuelLoad + energyuse.Quantity;
                            }
                        }
                    }
                }

            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="clientId"></param>
        /// <param name="startDate"></param>
        /// <param name="endDate"></param>
        /// <param name="zipCode"></param>
        /// <param name="countryCode"></param>
        /// <param name="stationId"></param>
        /// <param name="insightsEfRepository"></param>
        /// <returns></returns>
        public List<WeatherDb> GetDailyWeather(int clientId, DateTime startDate, DateTime endDate, string zipCode, string countryCode, string stationId, InsightsEfRepository insightsEfRepository)
        {

            if (zipCode.Length > 5 && countryCode.Equals(CountryType.US.ToString(), StringComparison.InvariantCultureIgnoreCase))
            {
                zipCode = zipCode.Substring(0, 5);
            }
            else if (zipCode.Length > 6 && countryCode.Equals(CountryType.CA.ToString(), StringComparison.InvariantCultureIgnoreCase))
            {
                zipCode = zipCode.Substring(0, 6);
            }

            var weatherRequest = new WeatherRequest
            {
                ZipCode = zipCode,
                StartDate = startDate,
                EndDate = endDate,
                StationId = stationId
            };

            if (insightsEfRepository == null)
            {
                insightsEfRepository = new InsightsEfRepository();
            }

            var weatherDbList = insightsEfRepository.GetWeather(clientId, weatherRequest);

            return weatherDbList;
        }
    }
}

