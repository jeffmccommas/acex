﻿using System;
using System.Collections.Generic;
using System.Linq;
using CE.EnergyModel.Enums;

namespace CE.EnergyModel
{
    /// <summary>
    /// Status class.
    /// </summary>
    public class Status
    {
        Exception _exception;
        StatusType _statusType;


        #region "Public Properties"

        /// <summary>
        /// Property: Exception
        /// </summary>
        public Exception Exception
        {
            get { return _exception; }
            set { _exception = value; }
        }

        /// <summary>
        /// Property: Status type.
        /// </summary>
        public StatusType StatusType
        {
            get { return _statusType; }
            set { _statusType = value; }
        }

        #endregion


        /// <summary>
        /// Default constructor.
        /// </summary>
        public Status()
        {
            _exception = null;
            _statusType = StatusType.Unspecified;
        }

        /// <summary>
        /// Constructor.
        /// </summary>
        public Status(Exception exception, StatusType statusType)
        {
            _exception = exception;
            _statusType = statusType;
        }

    }


    /// <summary>
    /// StatusList class.
    /// </summary>
    public class StatusList : List<Status>
    {

        /// <summary>
        /// Quick determiniation to see if status list has any errors.
        /// </summary>
        /// <returns></returns>
        public bool HasNoErrors()
        {
            return (!HasStatusSeverity(StatusType.Error));
        }


        /// <summary>
        /// Determine if status list containts specified status type.
        /// </summary>
        /// <returns></returns>
        public bool HasStatusSeverity(StatusType statusType)
        {
            bool result = false;
            Status status = null;

            try
            {
                //Find status with matching status type.
                status = this.Find(statusToFind => statusToFind.StatusType == statusType);

                //Status with matching status type found.
                if (status != null)
                {
                    result = true;
                }
            }
            catch (Exception)
            {
                throw;
            }

            return result;
        }


        /// <summary>
        /// Determine count of specified status type.
        /// </summary>
        /// <returns></returns>
        public int CountOfStatusSeverity(StatusType statusType)
        {
            List<Status> statusList = null;
            var count = 0;

            try
            {
                //Find status with matching status severity.
                statusList = this.FindAll(statusToFind => statusToFind.StatusType == statusType);

                //Status with matching status type found.
                if (statusList != null)
                {
                    count = statusList.Count();
                }
            }
            catch (Exception)
            {
                throw;
            }

            return (count);
        }

        /// <summary>
        /// Determine if status list containts specified type.
        /// </summary>
        /// <returns></returns>
        public bool HasException(System.Type comparisonType)
        {
            bool result = false;
            Status status = null;

            try
            {

                //Find status with matching exception subtype.
                status = this.Find(statusToFind => statusToFind.Exception.GetType() == comparisonType);

                //Status with matching exception subtype found.
                if (status != null)
                {
                    result = true;
                }

            }
            catch (Exception)
            {
                throw;
            }

            return result;

        }

        /// <summary>
        /// Return status list with all status objects that match specified status type.
        /// If no statuses match then empty status list is returned.
        /// </summary>
        /// <param name="statusType"></param>
        /// <returns></returns>
        public StatusList GetStatusListByStatusSeverity(StatusType statusType)
        {
            StatusList result = null;
            List<Status> matchingStatuslist = null;

            matchingStatuslist = this.FindAll(statusToFind => statusToFind.StatusType == statusType);

            result = new StatusList();

            result.AddRange(matchingStatuslist);

            return result;
        }

    }


}