﻿using System;

namespace CE.EnergyModel
{
    /// <summary>
    /// Applance Evaluator for medium and complex appliances.  Initialize evaluator with appliance.  Call Evaluate.  Partial.
    /// </summary>
    public partial class ApplianceEvaluator
    {
        private const string ExceptionMessageApplianceUnknown = "Unknown appliance.";
        private Appliance _appliance;
        private readonly EnergyModelInputParams _input;

        public ApplianceEvaluator(Appliance appliance, EnergyModelInputParams input)
        {
            _appliance = appliance;
            _input = input;
        }

        public Appliance Appliance
        {
            get { return _appliance; }
            set { _appliance = value; }
        }

        /// <summary>
        /// Evaluate the given appliance.
        /// </summary>
        /// <returns></returns>
        public void Evaluate()
        {
            switch (_appliance.Name.ToLower())
            {
                case Constants.ApplianceComplex_HeatSystem:
                    Model_heatsystem();
                    break;

                case Constants.ApplianceComplex_CentralAC:
                    Model_centralac();
                    break;

                case Constants.ApplianceComplex_SecondaryHeating:
                    Model_secondaryheating();
                    break;

                case Constants.ApplianceComplex_Lighting:
                    Model_lighting();
                    break;

                case Constants.ApplianceComplex_Freezer:
                    Model_freezer();
                    break;

                case Constants.ApplianceComplex_Freezer2:
                    Model_freezer2();
                    break;

                case Constants.ApplianceComplex_WellPump:
                    Model_wellpump();
                    break;

                case Constants.ApplianceComplex_SumpPump:
                    Model_sumppump();
                    break;

                case Constants.ApplianceComplex_Microwave:
                    Model_microwave();
                    break;

                case Constants.ApplianceComplex_WaterBed:
                    Model_waterbed();
                    break;

                case Constants.ApplianceComplex_Vehicle:
                    Model_vehicle();
                    break;

                case Constants.ApplianceComplex_Vehicle2:
                    Model_vehicle2();
                    break;

                case Constants.ApplianceComplex_SpaceHeater:
                    Model_spaceheater();
                    break;

                case Constants.ApplianceComplex_Refrigerator:
                    Model_refrigerator();
                    break;

                case Constants.ApplianceComplex_Refrigerator2:
                    Model_refrigerator2();
                    break;

                case Constants.ApplianceComplex_Oven:
                    Model_oven();
                    break;

                case Constants.ApplianceComplex_RoomAC:
                    Model_roomac();
                    break;

                case Constants.ApplianceComplex_RoomAC2:
                    Model_roomac2();
                    break;

                case Constants.ApplianceComplex_RoomAC3:
                    Model_roomac3();
                    break;

                case Constants.ApplianceComplex_Fireplace:
                    Model_fireplace();
                    break;

                case Constants.ApplianceComplex_HotTub:
                    Model_hottub();
                    break;

                case Constants.ApplianceComplex_ClothesWasher:
                    Model_clotheswasher();
                    break;

                case Constants.ApplianceComplex_Cooktop:
                    Model_cooktop();
                    break;

                case Constants.ApplianceComplex_DishWasher:
                    Model_dishwasher();
                    break;

                case Constants.ApplianceComplex_Dryer:
                    Model_dryer();
                    break;

                case Constants.ApplianceComplex_OutsideWater:
                    Model_outsidewater();
                    break;

                case Constants.ApplianceComplex_Shower:
                case Constants.ApplianceComplex_Shower_Alt:
                    Model_shower();
                    break;

                case Constants.ApplianceComplex_Sink:
                case Constants.ApplianceComplex_Sink_Alt:
                    Model_sink();
                    break;

                case Constants.ApplianceComplex_WaterHeater:
                    Model_waterheater();
                    break;

                case Constants.ApplianceComplex_Pool:
                    Model_pool();
                    break;

                case Constants.ApplianceComplex_WoodStove:
                    Model_woodstove();
                    break;



                case Constants.ApplianceBusinessComputers:
                    Model_BusinessComputers("EuiComputers");
                    break;

                case Constants.ApplianceBusinessCooking:
                    Model_BusinessCooking("EuiCooking");
                    break;

                case Constants.ApplianceBusinessCooling:
                    Model_BusinessCooling("EuiCooling");
                    break;

                case Constants.ApplianceBusinessHeating:
                    Model_BusinessHeating("EuiHeating");
                    break;

                case Constants.ApplianceBusinessLaundry:
                    Model_BusinessLaundry("EuiLaundry");
                    break;

                case Constants.ApplianceBusinessLighting:
                    Model_BusinessLighting("EuiLighting");
                    break;

                case Constants.ApplianceBusinessMiscellaneous:
                    Model_BusinessMiscellaneous("EuiMiscellaneous");
                    break;

                case Constants.ApplianceBusinessOfficeEquipment:
                    Model_BusinessOfficeEquipment("EuiOfficeEquipment");
                    break;

                case Constants.ApplianceBusinessRefrigeration:
                    Model_BusinessRefrigeration("EuiRefrigeration");
                    break;

                case Constants.ApplianceBusinessVentilation:
                    Model_BusinessVentilation("EuiVentilation");
                    break;

                case Constants.ApplianceBusinessWaterHeating:
                    Model_BusinessWaterHeating("EuiWaterHeating");
                    break;



                default:
                    Model_notfound();
                    break;
            }
        }


        /// <summary>
        /// Model for an unknown appliance.
        /// </summary>
        private void Model_notfound()
        {
            var errEu = new EnergyUse(Enums.EvaluationType.Failed)
            {
                Quantity = 0.0m,
                EvaluationContext =
                {
                    Status = new Status(new Exception(ExceptionMessageApplianceUnknown), Enums.StatusType.Error)
                }
            };
            _appliance.EnergyUses.Add(errEu);
        }

        /// <summary>
        /// Model for a freezer.
        /// </summary>
        private void Model_freezer()
        {
            decimal frostFreeFactor;


            var pa = _input.ProfileAttributes;
            var config = _input.Configuration;

            var age = ResetDefaultYear(pa, "freezer.year");

            // use the style to get the frostFreeFactor
            var style = pa.Find(p => p.Key == "freezer.style").OptionKey;

            //is frost free
            var frostfree = Convert.ToBoolean(Convert.ToInt32(pa.Find(p => p.Key == "freezer.frostfree").OptionValue));

            var sTemp = "freezer.energyuse";
            if (style == "freezer.style.chest")
            {
                sTemp = sTemp + "_chest";
                frostFreeFactor = config.GetCalculationFactor<decimal>("freezer.frostfreefactor_chest");
            }
            else
            {
                sTemp = sTemp + "_upright";
                frostFreeFactor = config.GetCalculationFactor<decimal>("freezer.frostfreefactor_upright");
            }

            // get size from profile attribute option value
            var size = Convert.ToDecimal(pa.Find(p => p.Key == "freezer.size").OptionValue);
            if (size > 24)
            {
                sTemp = sTemp + "_sizegt24";
            }
            else if (size >= 22)
            {
                sTemp = sTemp + "_size13to15";
            }
            else if (size >= 16)
            {
                sTemp = sTemp + "_size16to21";
            }
            else if (size >= 13)
            {
                sTemp = sTemp + "_size22to24";
            }
            else
            {
                sTemp = sTemp + "_sizelt13";
            }

            //get energy use factor based upon style and size; ex: freezer.energyuse_upright_size13to15
            var energyUse = config.GetCalculationFactor<decimal>(sTemp);

            var ageEnerygUseFactorString = "freezer.ageuse_factor_";
            if (age > 20)
            {
                ageEnerygUseFactorString = ageEnerygUseFactorString + "gt20";
            }
            else if (age > 16)
            {
                ageEnerygUseFactorString = ageEnerygUseFactorString + "16to20";
            }
            else if (age > 11)
            {
                ageEnerygUseFactorString = ageEnerygUseFactorString + "11to15";
            }
            else if (age > 6)
            {
                ageEnerygUseFactorString = ageEnerygUseFactorString + "6to10";
            }
            else if (age > 2)
            {
                ageEnerygUseFactorString = ageEnerygUseFactorString + "2to5";
            }
            else
            {
                ageEnerygUseFactorString = ageEnerygUseFactorString + "lt2";
            }

            //get energy use factor based upon age
            var ageEnergyUseFactor = config.GetCalculationFactor<decimal>(ageEnerygUseFactorString);

            energyUse = energyUse*ageEnergyUseFactor;

            // get frostfree boolean profile attribute
            if (frostfree)
            {
                energyUse = energyUse*frostFreeFactor;
            }

            // get maintainRegularly boolean profile attribute
            var defrostFactor = 0.0m;
            bool maintainRegularly =
                Convert.ToBoolean(Convert.ToInt32(pa.Find(p => p.Key == "freezer.maintainregularly").OptionValue));
            if (!maintainRegularly && !frostfree)
            {
                defrostFactor = config.GetCalculationFactor<decimal>("freezer.defrostfactor");
            }

            // get defrostRegularly boolean profile attribute
            decimal ageMaintenanceFactor;
            var defrostRegularly =
                Convert.ToBoolean(Convert.ToInt32(pa.Find(p => p.Key == "freezer.defrostregularly").OptionValue));
            if (defrostRegularly)
            {
                ageMaintenanceFactor = 1.0m;
            }
            else
            {
                string ageMaintFactorString;
                if (age > 15)
                {
                    ageMaintFactorString = "freezer.agemaint_factor_gt15";
                }
                else if (age > 10)
                {
                    ageMaintFactorString = "freezer.agemaint_factor_11to15";
                }
                else if (age > 5)
                {
                    ageMaintFactorString = "freezer.agemaint_factor_6to10";
                }
                else if (age > 1)
                {
                    ageMaintFactorString = "freezer.agemaint_factor_1to5";
                }
                else
                {
                    ageMaintFactorString = "freezer.agemaint_factor_0";
                }

                //get maint factor based upon age
                ageMaintenanceFactor = config.GetCalculationFactor<decimal>(ageMaintFactorString);
            }

            // temperature setting factor from profile attribute
            var temperatureFactor = Convert.ToDecimal(pa.Find(p => p.Key == "freezer.temperature").OptionValue);

            // Degree day ratio
            var ddr = _input.WeatherInfo.AnnualHDD/(decimal) _input.WeatherInfo.AnnualCDD;

            //factor string
            string locationFactorString;

            // location option key;  ex: freezer.location.pantry
            var location = pa.Find(p => p.Key == "freezer.location").OptionKey;

            if (ddr > 3.5m)
            {
                switch (location)
                {
                    case "freezer.location.kitchen":
                        locationFactorString = "freezer.loc_kitchen_ddr_gt35";
                        break;
                    case "freezer.location.pantry":
                        locationFactorString = "freezer.loc_pantry_ddr_gt35";
                        break;
                    case "freezer.location.basement":
                        locationFactorString = "freezer.loc_basement_ddr_gt35";
                        break;
                    case "freezer.location.garage":
                        locationFactorString = "freezer.loc_garage_ddr_gt35";
                        break;
                    default: //freezer.location.other
                        locationFactorString = "freezer.loc_other_ddr_gt35";
                        break;
                }
            }
            else if (ddr > 2.5m)
            {
                switch (location)
                {
                    case "freezer.location.kitchen":
                        locationFactorString = "freezer.loc_kitchen_ddr_25to35";
                        break;
                    case "freezer.location.pantry":
                        locationFactorString = "freezer.loc_pantry_ddr_25to35";
                        break;
                    case "freezer.location.basement":
                        locationFactorString = "freezer.loc_basement_ddr_25to35";
                        break;
                    case "freezer.location.garage":
                        locationFactorString = "freezer.loc_garage_ddr_25to35";
                        break;
                    default: //freezer.location.other
                        locationFactorString = "freezer.loc_other_ddr_25to35";
                        break;
                }
            }
            else
            {
                switch (location)
                {
                    case "freezer.location.kitchen":
                        locationFactorString = "freezer.loc_kitchen_ddr_lt25";
                        break;
                    case "freezer.location.pantry":
                        locationFactorString = "freezer.loc_pantry_ddr_lt25";
                        break;
                    case "freezer.location.basement":
                        locationFactorString = "freezer.loc_basement_ddr_lt25";
                        break;
                    case "freezer.location.garage":
                        locationFactorString = "freezer.loc_garage_ddr_lt25";
                        break;
                    default: //freezer.location.other
                        locationFactorString = "freezer.loc_other_ddr_lt25";
                        break;
                }
            }

            //get location factor based degree day ratio and location of freezer in house
            var locationFactor = config.GetCalculationFactor<decimal>(locationFactorString);

            // count
            var count = Convert.ToDecimal(pa.Find(p => p.Key == "freezer.count").OptionValue);

            // months off
            var monthsOff = Convert.ToDecimal(pa.Find(p => p.Key == "freezer.monthsoff").OptionValue);

            //FINAL
            var outputkWh = (count*((12 - monthsOff)/12)*(energyUse + defrostFactor)*temperatureFactor*
                             ageMaintenanceFactor*locationFactor);

            // add energy use        
            _appliance.EnergyUses.Add(new EnergyUse(true)
            {
                Commodity = Enums.CommodityType.Electric,
                Quantity = outputkWh,
                UnitOfMeasure = Enums.UnitOfMeasureType.kWh
            });
        }


        /// <summary>
        /// Model for a second freezer
        /// </summary>
        private void Model_freezer2()
        {
            decimal frostFreeFactor;

            var pa = _input.ProfileAttributes;
            var config = _input.Configuration;

            var age = ResetDefaultYear(pa, "freezer2.year");

            // use the style to get the frostFreeFactor
            var style = pa.Find(p => p.Key == "freezer2.style").OptionKey;

            //is frost free
            var frostfree = Convert.ToBoolean(Convert.ToInt32(pa.Find(p => p.Key == "freezer2.frostfree").OptionValue));

            var sTemp = "freezer.energyuse";
            if (style == "freezer.style.chest")
            {
                sTemp = sTemp + "_chest";
                frostFreeFactor = config.GetCalculationFactor<decimal>("freezer.frostfreefactor_chest");
            }
            else
            {
                sTemp = sTemp + "_upright";
                frostFreeFactor = config.GetCalculationFactor<decimal>("freezer.frostfreefactor_upright");
            }

            // get size from profile attribute option value
            var size = Convert.ToDecimal(pa.Find(p => p.Key == "freezer2.size").OptionValue);
            if (size > 24)
            {
                sTemp = sTemp + "_sizegt24";
            }
            else if (size >= 22)
            {
                sTemp = sTemp + "_size13to15";
            }
            else if (size >= 16)
            {
                sTemp = sTemp + "_size16to21";
            }
            else if (size >= 13)
            {
                sTemp = sTemp + "_size22to24";
            }
            else
            {
                sTemp = sTemp + "_sizelt13";
            }

            //get energy use factor based upon style and size; ex: freezer.energyuse_upright_size13to15
            var energyUse = config.GetCalculationFactor<decimal>(sTemp);

            string ageEnerygUseFactorString;
            if (age > 20)
            {
                ageEnerygUseFactorString = "freezer.ageuse_factor_gt20";
            }
            else if (age > 16)
            {
                ageEnerygUseFactorString = "freezer.ageuse_factor_16to20";
            }
            else if (age > 11)
            {
                ageEnerygUseFactorString = "freezer.ageuse_factor_11to15";
            }
            else if (age > 6)
            {
                ageEnerygUseFactorString = "freezer.ageuse_factor_6to10";
            }
            else if (age > 2)
            {
                ageEnerygUseFactorString = "freezer.ageuse_factor_2to5";
            }
            else
            {
                ageEnerygUseFactorString = "freezer.ageuse_factor_lt2";
            }

            //get energy use factor based upon age
            var ageEnergyUseFactor = config.GetCalculationFactor<decimal>(ageEnerygUseFactorString);

            energyUse = energyUse*ageEnergyUseFactor;

            // get frostfree boolean profile attribute
            if (frostfree)
            {
                energyUse = energyUse*frostFreeFactor;
            }


            // get maintainRegularly boolean profile attribute
            var defrostFactor = 0.0m;
            bool maintainRegularly =
                Convert.ToBoolean(Convert.ToInt32(pa.Find(p => p.Key == "freezer2.maintainregularly").OptionValue));
            if (!maintainRegularly && !frostfree)
            {
                defrostFactor = config.GetCalculationFactor<decimal>("freezer.defrostfactor");
            }

            // get defrostRegularly boolean profile attribute
            decimal ageMaintenanceFactor;
            bool defrostRegularly =
                Convert.ToBoolean(Convert.ToInt32(pa.Find(p => p.Key == "freezer2.defrostregularly").OptionValue));
            if (defrostRegularly)
            {
                ageMaintenanceFactor = 1.0m;
            }
            else
            {
                string ageMaintFactorString;
                if (age > 15)
                {
                    ageMaintFactorString = "freezer.agemaint_factor_gt15";
                }
                else if (age > 10)
                {
                    ageMaintFactorString = "freezer.agemaint_factor_11to15";
                }
                else if (age > 5)
                {
                    ageMaintFactorString = "freezer.agemaint_factor_6to10";
                }
                else if (age > 1)
                {
                    ageMaintFactorString = "freezer.agemaint_factor_1to5";
                }
                else
                {
                    ageMaintFactorString = "freezer.agemaint_factor_0";
                }

                //get maint factor based upon age
                ageMaintenanceFactor = config.GetCalculationFactor<decimal>(ageMaintFactorString);
            }

            // temperature setting factor from profile attribute
            var temperatureFactor = Convert.ToDecimal(pa.Find(p => p.Key == "freezer2.temperature").OptionValue);

            // Degree day ratio
            var ddr = _input.WeatherInfo.AnnualHDD/(decimal) _input.WeatherInfo.AnnualCDD;

            //factor string
            string locationFactorString;

            // location option key;  ex: freezer.location.pantry
            string location = pa.Find(p => p.Key == "freezer2.location").OptionKey;

            if (ddr > 3.5m)
            {
                switch (location)
                {
                    case "freezer.location.kitchen":
                        locationFactorString = "freezer.loc_kitchen_ddr_gt35";
                        break;
                    case "freezer.location.pantry":
                        locationFactorString = "freezer.loc_pantry_ddr_gt35";
                        break;
                    case "freezer.location.basement":
                        locationFactorString = "freezer.loc_basement_ddr_gt35";
                        break;
                    case "freezer.location.garage":
                        locationFactorString = "freezer.loc_garage_ddr_gt35";
                        break;
                    default: //freezer.location.other
                        locationFactorString = "freezer.loc_other_ddr_gt35";
                        break;
                }
            }
            else if (ddr > 2.5m)
            {
                switch (location)
                {
                    case "freezer.location.kitchen":
                        locationFactorString = "freezer.loc_kitchen_ddr_25to35";
                        break;
                    case "freezer.location.pantry":
                        locationFactorString = "freezer.loc_pantry_ddr_25to35";
                        break;
                    case "freezer.location.basement":
                        locationFactorString = "freezer.loc_basement_ddr_25to35";
                        break;
                    case "freezer.location.garage":
                        locationFactorString = "freezer.loc_garage_ddr_25to35";
                        break;
                    default: //freezer.location.other
                        locationFactorString = "freezer.loc_other_ddr_25to35";
                        break;
                }
            }
            else
            {
                switch (location)
                {
                    case "freezer.location.kitchen":
                        locationFactorString = "freezer.loc_kitchen_ddr_lt25";
                        break;
                    case "freezer.location.pantry":
                        locationFactorString = "freezer.loc_pantry_ddr_lt25";
                        break;
                    case "freezer.location.basement":
                        locationFactorString = "freezer.loc_basement_ddr_lt25";
                        break;
                    case "freezer.location.garage":
                        locationFactorString = "freezer.loc_garage_ddr_lt25";
                        break;
                    default: //freezer.location.other
                        locationFactorString = "freezer.loc_other_ddr_lt25";
                        break;
                }
            }

            //get location factor based degree day ratio and location of freezer in house
            var locationFactor = config.GetCalculationFactor<decimal>(locationFactorString);

            // count
            var count = Convert.ToDecimal(pa.Find(p => p.Key == "freezer2.count").OptionValue);

            // months off
            var monthsOff = Convert.ToDecimal(pa.Find(p => p.Key == "freezer2.monthsoff").OptionValue);

            //FINAL
            var outputkWh = (count*((12 - monthsOff)/12)*(energyUse + defrostFactor)*temperatureFactor*
                             ageMaintenanceFactor*locationFactor);

            // add energy use        
            _appliance.EnergyUses.Add(new EnergyUse(true)
            {
                Commodity = Enums.CommodityType.Electric,
                Quantity = outputkWh,
                UnitOfMeasure = Enums.UnitOfMeasureType.kWh
            });
        }


        /// <summary>
        /// Model for Lighting
        /// </summary>
        private void Model_lighting()
        {
            var pa = _input.ProfileAttributes;
            var config = _input.Configuration;

            //get factors
            var baseUsagePerDay = config.GetCalculationFactor<decimal>("lighting.lighthours_base");
            var personUsagePerDay = config.GetCalculationFactor<decimal>("lighting.lighthours_person");
            var people = (int) Convert.ToDecimal(pa.Find(p => p.Key == "house.people").OptionValue);
            var peopleFactor = baseUsagePerDay + (people*personUsagePerDay);

            // Usage----------------------------------
            // Interior Lighting
            var sTemp = "lighting.usage_factor";
            var insideUsageHours = pa.Find(p => p.Key == "lighting.insideusagehrs").OptionKey;

            switch (insideUsageHours)
            {
                case "lighting.insideusagehrs.high":
                    sTemp = sTemp + "_gt6";
                    break;
                case "lighting.insideusagehrs.average":
                    sTemp = sTemp + "_4to6";
                    break;
                case "lighting.insideusagehrs.low":
                    sTemp = sTemp + "_1to3";
                    break;
                default:
                    sTemp = sTemp + "_0";
                    break;
            }

            var usageFactor = config.GetCalculationFactor<decimal>(sTemp);
            var insideUsageFactor = usageFactor*peopleFactor;

            var wallQty = Convert.ToInt32(pa.Find(p => p.Key == "lighting.wallqty").OptionValue);
            var wallWattage = Convert.ToDecimal(pa.Find(p => p.Key == "lighting.wallwattage").OptionValue);
            var floorWattage = Convert.ToDecimal(pa.Find(p => p.Key == "lighting.floorwattage").OptionValue);
            var floorQty = Convert.ToInt32(pa.Find(p => p.Key == "lighting.floorqty").OptionValue);
            var spotWattage = Convert.ToDecimal(pa.Find(p => p.Key == "lighting.spotwattage").OptionValue);
            var spotQty = Convert.ToInt32(pa.Find(p => p.Key == "lighting.spotqty").OptionValue);

            // Exterior Lighting
            var outsideWattage = Convert.ToDecimal(pa.Find(p => p.Key == "lighting.outsidewattage").OptionValue);
            var outsideQty = Convert.ToInt32(pa.Find(p => p.Key == "lighting.outsideqty").OptionValue);
            var outsideUsageHrs = pa.Find(p => p.Key == "lighting.outsideusagehrs").OptionKey;

            int outsideUsageHours;
            switch (outsideUsageHrs)
            {
                case "lighting.outsideusagehrs.evening":
                    outsideUsageHours = 2;
                    break;
                case "lighting.outsideusagehrs.overnight":
                    outsideUsageHours = 4;
                    break;
                case "lighting.outsideusagehrs.rarely":
                    outsideUsageHours = 1;
                    break;
                default:
                    outsideUsageHours = 0;
                    break;
            }

            // Halogen Usage
            var halogenWattage = Convert.ToDecimal(pa.Find(p => p.Key == "lighting.halogenwattage").OptionValue);
            var halogenQty = Convert.ToInt32(pa.Find(p => p.Key == "lighting.halogenqty").OptionValue);
            const int halogenUsageHrs = 5;
                // Convert.ToDecimal(pa.Find(p => p.Key == "lighting.halogenusagehrs").OptionValue);

            var usage = (wallQty*wallWattage + floorQty*floorWattage + spotQty*spotWattage)*insideUsageFactor +
                        outsideQty*outsideWattage*outsideUsageHours +
                        halogenUsageHrs*halogenWattage*halogenQty;

            //FINAL
            var count = Convert.ToDecimal(pa.Find(p => p.Key == "lighting.count").OptionValue);
            var outputkWh = count*0.365m*usage;

            // add energy use          
            _appliance.EnergyUses.Add(new EnergyUse(true)
            {
                Commodity = Enums.CommodityType.Electric,
                Quantity = outputkWh,
                UnitOfMeasure = Enums.UnitOfMeasureType.kWh
            });
        }

        /// <summary>
        /// Model for Well Pump
        /// </summary>
        private void Model_wellpump()
        {
            decimal outputkWh;

            var pa = _input.ProfileAttributes;
            var config = _input.Configuration;

            var age = ResetDefaultYear(pa, "wellpump.year");

            //get factors
            var energyUse = config.GetCalculationFactor<decimal>("wellpump.energyuse");

            //get people quantity
            var people = (int) Convert.ToDecimal(pa.Find(p => p.Key == "house.people").OptionValue);

            // get efficiency factor based upon age
            var sTemp = "wellpump.agepumpeff_factor";

            if (age > 20)
            {
                sTemp = sTemp + "_gt20";
            }
            else if (age >= 15)
            {
                sTemp = sTemp + "_15to20";
            }
            else if (age >= 10)
            {
                sTemp = sTemp + "_10to15";
            }
            else if (age >= 5)
            {
                sTemp = sTemp + "_5to10";
            }
            else
            {
                sTemp = sTemp + "_lt5";
            }

            var ageEffFactor = config.GetCalculationFactor<decimal>(sTemp);

            //get the default or count of well pump
            var count = Convert.ToDecimal(pa.Find(p => p.Key == "wellpump.count").OptionValue);

            if (count >= 1.0m)
            {
                //FINAL
                outputkWh = energyUse*(people/ageEffFactor)*(1 + (count - 1)*0.05m);
            }
            else
            {
                //FINAL
                outputkWh = energyUse*(people/ageEffFactor)*count;
            }

            // add energy use           
            _appliance.EnergyUses.Add(new EnergyUse(true)
            {
                Commodity = Enums.CommodityType.Electric,
                Quantity = outputkWh,
                UnitOfMeasure = Enums.UnitOfMeasureType.kWh
            });
        }

        /// <summary>
        /// Model for the Sump Pump
        /// </summary>
        private void Model_sumppump()
        {
            decimal outputkWh;

            var pa = _input.ProfileAttributes;
            var config = _input.Configuration;

            var age = ResetDefaultYear(pa, "sumppump.year");

            //get factors
            var energyUse = config.GetCalculationFactor<decimal>("sumppump.energy"); //energy rather than energyuse

            // get efficiency factor based upon age
            var sTemp = "sumppump.agepumpeff_factor";

            if (age > 15)
            {
                sTemp = sTemp + "_gt15";
            }
            else if (age >= 10)
            {
                sTemp = sTemp + "_10to15";
            }
            else if (age >= 5)
            {
                sTemp = sTemp + "_5to10";
            }
            else
            {
                sTemp = sTemp + "_lt5";
            }

            var ageEffFactor = config.GetCalculationFactor<decimal>(sTemp);

            //get the default or count of sump pump
            var count = Convert.ToDecimal(pa.Find(p => p.Key == "sumppump.count").OptionValue);

            //get the sumppump.basementwater level option value
            var basementWaterFactor = Convert.ToDecimal(pa.Find(p => p.Key == "sumppump.basementwater").OptionValue);

            if (count >= 1.0m)
            {
                //FINAL
                outputkWh = energyUse*basementWaterFactor/ageEffFactor*(1 + (count - 1)*0.05m);
            }
            else
            {
                //FINAL
                outputkWh = ((energyUse*basementWaterFactor)/ageEffFactor)*count;
            }

            // add energy use           
            _appliance.EnergyUses.Add(new EnergyUse(true)
            {
                Commodity = Enums.CommodityType.Electric,
                Quantity = outputkWh,
                UnitOfMeasure = Enums.UnitOfMeasureType.kWh
            });
        }

        /// <summary>
        /// Model for the Microwave.
        /// </summary>
        private void Model_microwave()
        {
            decimal outputkWh;

            var pa = _input.ProfileAttributes;
            var config = _input.Configuration;

            //get factors
            var energyUseForMeals = config.GetCalculationFactor<decimal>("microwave.useformeals");
            //energy use for meals
            var energyUseForSnacks = config.GetCalculationFactor<decimal>("microwave.useforsnacks");
            //energy use for snacks

            //get people quantity
            var people = (int) Convert.ToDecimal(pa.Find(p => p.Key == "house.people").OptionValue);

            // get people factor based upon age
            var sTemp = "microwave.people_factor";
            if (people >= 6)
            {
                sTemp = sTemp + "_6";
            }
            else if (people >= 5)
            {
                sTemp = sTemp + "_5";
            }
            else if (people >= 4)
            {
                sTemp = sTemp + "_4";
            }
            else if (people >= 3)
            {
                sTemp = sTemp + "_3";
            }
            else if (people >= 2)
            {
                sTemp = sTemp + "_2";
            }
            else if (people >= 1)
            {
                sTemp = sTemp + "_1";
            }
            else
            {
                sTemp = sTemp + "_0";
            }

            var peopleFactor = config.GetCalculationFactor<decimal>(sTemp);

            //get the default or count of microwave
            var count = Convert.ToDecimal(pa.Find(p => p.Key == "microwave.count").OptionValue);

            //usageformeals factor
            var usageForMealsFactor = Convert.ToDecimal(pa.Find(p => p.Key == "microwave.usageformeals").OptionValue);
            //usageforother factor
            var usageForOtherFactor = Convert.ToDecimal(pa.Find(p => p.Key == "microwave.usageforother").OptionValue);
            //breakfasts factor
            var breakfastsWkly = Convert.ToDecimal(pa.Find(p => p.Key == "microwave.breakfastswkly").OptionValue);
            //lunches factor
            var lunchesWkly = Convert.ToDecimal(pa.Find(p => p.Key == "microwave.luncheswkly").OptionValue);
            //dinners factor
            var dinnersWkly = Convert.ToDecimal(pa.Find(p => p.Key == "microwave.dinnerswkly").OptionValue);

            energyUseForMeals = energyUseForMeals*usageForMealsFactor;
            energyUseForSnacks = energyUseForSnacks*usageForOtherFactor;

            var hotMealFactor = 1 - (14 - (0.5m*breakfastsWkly + lunchesWkly + 2.0m*dinnersWkly))/14;

            if (count >= 1.0m)
            {
                //FINAL
                outputkWh = (energyUseForSnacks + (energyUseForMeals*hotMealFactor))*peopleFactor*
                            (1 + 0.05m*(count - 1));
            }
            else
            {
                //FINAL
                outputkWh = (energyUseForSnacks + energyUseForMeals*hotMealFactor)*peopleFactor*count;
            }

            // add energy use         
            _appliance.EnergyUses.Add(new EnergyUse(true)
            {
                Commodity = Enums.CommodityType.Electric,
                Quantity = outputkWh,
                UnitOfMeasure = Enums.UnitOfMeasureType.kWh
            });
        }

        /// <summary>
        /// Model for a waterbed
        /// </summary>
        private void Model_waterbed()
        {
            var pa = _input.ProfileAttributes;
            var config = _input.Configuration;

            //get the default or count
            var count = Convert.ToDecimal(pa.Find(p => p.Key == "waterbed.count").OptionValue);

            //get energy use factor from config
            var energyUse = config.GetCalculationFactor<decimal>("waterbed.energyuse");

            //get the size factor and the size type string
            var sizefactor = Convert.ToDecimal(pa.Find(p => p.Key == "waterbed.size").OptionValue);

            //is soft sided
            var isSoftSided = Convert.ToBoolean(Convert.ToInt32(pa.Find(p => p.Key == "waterbed.softsided").OptionValue));

            //is make bed
            var isMakeBed = Convert.ToBoolean(Convert.ToInt32(pa.Find(p => p.Key == "waterbed.makethebed").OptionValue));

            //is side insulated
            var isSideInsulated =
                Convert.ToBoolean(Convert.ToInt32(pa.Find(p => p.Key == "waterbed.sideinsulated").OptionValue));

            //temperature
            var temperature = Convert.ToInt32(pa.Find(p => p.Key == "waterbed.temperature").OptionValue);

            //months heated
            var monthsHeated = Convert.ToInt32(pa.Find(p => p.Key == "waterbed.monthsheated").OptionValue);

            // various other factors and attributes
            var sizetype = pa.Find(p => p.Key == "waterbed.size").OptionKey;
            string styleFactorString;
            string slopeFactorString;
            string insulationFactorString;

            if (isSoftSided)
            {
                styleFactorString = "waterbed.stylefactorsoft";

                switch (sizetype)
                {
                    case "waterbed.size.twin":
                        slopeFactorString = "waterbed.softfactortween";
                        break;
                    case "waterbed.size.double":
                        slopeFactorString = "waterbed.softfactordouble";
                        break;
                    case "waterbed.size.queen":
                        slopeFactorString = "waterbed.softfactorqueen";
                        break;
                    case "waterbed.size.king":
                        slopeFactorString = "waterbed.softfactorking";
                        break;
                    default:
                        slopeFactorString = "waterbed.softfactortween";
                        break;
                }

                if (isMakeBed)
                {
                    insulationFactorString = isSideInsulated ? "waterbed.insulfactor0" : "waterbed.insulfactor1";
                }
                else
                {
                    insulationFactorString = isSideInsulated ? "waterbed.insulfactor2" : "waterbed.insulfactor3";
                }

            }
            else
            {
                styleFactorString = "waterbed.stylefactorhard";

                switch (sizetype)
                {
                    case "waterbed.size.twin":
                        slopeFactorString = "waterbed.hardfactortween";
                        break;
                    case "waterbed.size.double":
                        slopeFactorString = "waterbed.hardfactordouble";
                        break;
                    case "waterbed.size.queen":
                        slopeFactorString = "waterbed.hardfactorqueen";
                        break;
                    case "waterbed.size.king":
                        slopeFactorString = "waterbed.hardfactorking";
                        break;
                    default:
                        slopeFactorString = "waterbed.hardfactortween";
                        break;
                }

                if (isMakeBed)
                {
                    insulationFactorString = isSideInsulated ? "waterbed.insulfactor4" : "waterbed.insulfactor5";
                }
                else
                {
                    insulationFactorString = isSideInsulated ? "waterbed.insulfactor6" : "waterbed.insulfactor7";
                }
            }

            var styleFactor = config.GetCalculationFactor<decimal>(styleFactorString);
            var slopeFactor = config.GetCalculationFactor<decimal>(slopeFactorString);
            var insulationFactor = config.GetCalculationFactor<decimal>(insulationFactorString);

            var outputkWh = count*(energyUse*sizefactor*styleFactor + (temperature - 70)*slopeFactor)*insulationFactor*
                            (monthsHeated/12.0m);

            // add energy use        
            _appliance.EnergyUses.Add(new EnergyUse(true)
            {
                Commodity = Enums.CommodityType.Electric,
                Quantity = outputkWh,
                UnitOfMeasure = Enums.UnitOfMeasureType.kWh
            });
        }

        /// <summary>
        /// Model for Electric Vehicle
        /// </summary>
        private void Model_vehicle()
        {
            var fuel1AnnualUse = 0.0m;
            var fuel2AnnualUse = 0.0m;

            var pa = _input.ProfileAttributes;
            var config = _input.Configuration;

            var vehiclekWhConversion = config.GetCalculationFactor<decimal>("vehicle.kwhconversion");
            var vehicleThermConversion = config.GetCalculationFactor<decimal>("vehicle.thermconversion");

            var count = Convert.ToDecimal(pa.Find(p => p.Key == "vehicle.count").OptionValue);
            var mileage = Convert.ToDecimal(pa.Find(p => p.Key == "vehicle.mileage").OptionValue);
            var frequency = Convert.ToDecimal(pa.Find(p => p.Key == "vehicle.frequency").OptionValue);
            var avgMilePerGallon = Convert.ToDecimal(pa.Find(p => p.Key == "vehicle.averagempg").OptionValue);
            var avgMilePerGallonBackup = Convert.ToDecimal(pa.Find(p => p.Key == "vehicle.averagempgbackup").OptionValue);
            var gasBackup = Convert.ToDecimal(pa.Find(p => p.Key == "vehicle.gasbackup").OptionValue);
            var partOfUtilityBill =
                Convert.ToDecimal(Convert.ToInt32(pa.Find(p => p.Key == "vehicle.partofutilitybill").OptionValue));
            var isSeperateMeter =
                Convert.ToBoolean(Convert.ToInt32(pa.Find(p => p.Key == "vehicle.separatemeter").OptionValue));

            var actualMileage = mileage*frequency;

            if (avgMilePerGallon > 0)
            {
                fuel1AnnualUse = count*actualMileage*(1 - gasBackup)/avgMilePerGallon;
            }

            if (avgMilePerGallonBackup > 0)
            {
                fuel2AnnualUse = actualMileage*gasBackup/avgMilePerGallonBackup;
            }


            fuel1AnnualUse = fuel1AnnualUse*partOfUtilityBill;

            var fueltype = pa.Find(p => p.Key == "vehicle.fuel").OptionKey;

            if (!isSeperateMeter)
            {
                // This means the usage is part of overall model, so include the energy output

                decimal conversionFactor;
                switch (fueltype)
                {
                    case "vehicle.fuel.electric":
                        conversionFactor = vehiclekWhConversion;
                        break;
                    case "vehicle.fuel.cng":
                    case "vehicle.fuel.lng":
                    case "vehicle.fuel.lpg":
                        conversionFactor = vehicleThermConversion;
                        break;
                    default:
                        conversionFactor = 1.0m;
                        break;
                }

                decimal outputkWh;
                switch (fueltype)
                {
                    case "vehicle.fuel.electric":
                        outputkWh = fuel1AnnualUse*conversionFactor;
                        _appliance.EnergyUses.Add(new EnergyUse(true)
                        {
                            Commodity = Enums.CommodityType.Electric,
                            Quantity = outputkWh,
                            UnitOfMeasure = Enums.UnitOfMeasureType.kWh
                        });

                        var outputTherms = fuel2AnnualUse*vehicleThermConversion;
                        _appliance.EnergyUses.Add(new EnergyUse(true)
                        {
                            Commodity = Enums.CommodityType.Gas,
                            Quantity = outputTherms,
                            UnitOfMeasure = Enums.UnitOfMeasureType.Therms
                        });
                        break;
                    case "vehicle.fuel.cng":
                    case "vehicle.fuel.lng":
                        outputkWh = fuel1AnnualUse*conversionFactor;
                        _appliance.EnergyUses.Add(new EnergyUse(true)
                        {
                            Commodity = Enums.CommodityType.Electric,
                            Quantity = outputkWh,
                            UnitOfMeasure = Enums.UnitOfMeasureType.kWh
                        });
                        break;
                    case "vehicle.fuel.lpg":
                        var outputPropane = fuel1AnnualUse*conversionFactor;
                        _appliance.EnergyUses.Add(new EnergyUse(true)
                        {
                            Commodity = Enums.CommodityType.Propane,
                            Quantity = outputPropane,
                            UnitOfMeasure = Enums.UnitOfMeasureType.Gal
                        });
                        break;
                    default:
                        // other fuels are not supported. There is no energy output.
                        _appliance.EnergyUses.Add(new EnergyUse(true)
                        {
                            Commodity = Enums.CommodityType.Electric,
                            Quantity = 0.0m,
                            UnitOfMeasure = Enums.UnitOfMeasureType.kWh
                        });
                        break;
                }
            }
            else
            {
                // actual vehicle load type usage is not supported directly in the model. There is no energy output.
                _appliance.EnergyUses.Add(new EnergyUse(true)
                {
                    Commodity = Enums.CommodityType.Electric,
                    Quantity = 0.0m,
                    UnitOfMeasure = Enums.UnitOfMeasureType.kWh
                });

            }
        }

        /// <summary>
        /// Model for a Electrical Vehicle - 2
        /// </summary>
        private void Model_vehicle2()
        {
            var fuel1AnnualUse = 0.0m;
            var fuel2AnnualUse = 0.0m;

            var pa = _input.ProfileAttributes;
            var config = _input.Configuration;

            var vehiclekWhConversion = config.GetCalculationFactor<decimal>("vehicle.kwhconversion");
            var vehicleThermConversion = config.GetCalculationFactor<decimal>("vehicle.thermconversion");

            var count = Convert.ToDecimal(pa.Find(p => p.Key == "vehicle2.count").OptionValue);
            var mileage = Convert.ToDecimal(pa.Find(p => p.Key == "vehicle2.mileage").OptionValue);
            var frequency = Convert.ToDecimal(pa.Find(p => p.Key == "vehicle2.frequency").OptionValue);
            var avgMilePerGallon = Convert.ToDecimal(pa.Find(p => p.Key == "vehicle2.averagempg").OptionValue);
            var avgMilePerGallonBackup =
                Convert.ToDecimal(pa.Find(p => p.Key == "vehicle2.averagempgbackup").OptionValue);
            var gasBackup = Convert.ToDecimal(pa.Find(p => p.Key == "vehicle2.gasbackup").OptionValue);
            var partOfUtilityBill =
                Convert.ToDecimal(Convert.ToInt32(pa.Find(p => p.Key == "vehicle2.partofutilitybill").OptionValue));
            var isSeperateMeter =
                Convert.ToBoolean(Convert.ToInt32(pa.Find(p => p.Key == "vehicle2.separatemeter").OptionValue));

            var actualMileage = mileage*frequency;

            if (avgMilePerGallon > 0)
            {
                fuel1AnnualUse = count*actualMileage*(1 - gasBackup)/avgMilePerGallon;
            }

            if (avgMilePerGallonBackup > 0)
            {
                fuel2AnnualUse = actualMileage*gasBackup/avgMilePerGallonBackup;
            }


            fuel1AnnualUse = fuel1AnnualUse*partOfUtilityBill;

            var fueltype = pa.Find(p => p.Key == "vehicle2.fuel").OptionKey;

            if (!isSeperateMeter)
            {
                // This means the usage is part of overall model, so include the energy output

                decimal conversionFactor;
                switch (fueltype)
                {
                    case "vehicle2.fuel.electric":
                        conversionFactor = vehiclekWhConversion;
                        break;
                    case "vehicle2.fuel.cng":
                    case "vehicle2.fuel.lng":
                    case "vehicle2.fuel.lpg":
                        conversionFactor = vehicleThermConversion;
                        break;
                    default:
                        conversionFactor = 1.0m;
                        break;
                }

                decimal outputkWh;
                switch (fueltype)
                {
                    case "vehicle2.fuel.electric":
                        outputkWh = fuel1AnnualUse*conversionFactor;
                        _appliance.EnergyUses.Add(new EnergyUse(true)
                        {
                            Commodity = Enums.CommodityType.Electric,
                            Quantity = outputkWh,
                            UnitOfMeasure = Enums.UnitOfMeasureType.kWh
                        });

                        var outputTherms = fuel2AnnualUse*vehicleThermConversion;
                        _appliance.EnergyUses.Add(new EnergyUse(true)
                        {
                            Commodity = Enums.CommodityType.Gas,
                            Quantity = outputTherms,
                            UnitOfMeasure = Enums.UnitOfMeasureType.Therms
                        });
                        break;
                    case "vehicle2.fuel.cng":
                    case "vehicle2.fuel.lng":
                        outputkWh = fuel1AnnualUse*conversionFactor;
                        _appliance.EnergyUses.Add(new EnergyUse(true)
                        {
                            Commodity = Enums.CommodityType.Electric,
                            Quantity = outputkWh,
                            UnitOfMeasure = Enums.UnitOfMeasureType.kWh
                        });
                        break;
                    case "vehicle2.fuel.lpg":
                        var outputPropane = fuel1AnnualUse*conversionFactor;
                        _appliance.EnergyUses.Add(new EnergyUse(true)
                        {
                            Commodity = Enums.CommodityType.Propane,
                            Quantity = outputPropane,
                            UnitOfMeasure = Enums.UnitOfMeasureType.Gal
                        });
                        break;
                    default:
                        // other fuels are not supported. There is no energy output.
                        _appliance.EnergyUses.Add(new EnergyUse(true)
                        {
                            Commodity = Enums.CommodityType.Electric,
                            Quantity = 0.0m,
                            UnitOfMeasure = Enums.UnitOfMeasureType.kWh
                        });
                        break;
                }
            }
            else
            {
                // actual vehicle load type usage is not supported directly in the model. There is no energy output.
                _appliance.EnergyUses.Add(new EnergyUse(true)
                {
                    Commodity = Enums.CommodityType.Electric,
                    Quantity = 0.0m,
                    UnitOfMeasure = Enums.UnitOfMeasureType.kWh
                });

            }
        }

        /// <summary>
        /// Model for a space heater
        /// </summary>
        private void Model_spaceheater()
        {
            var pa = _input.ProfileAttributes;
            var config = _input.Configuration;

            var heaterBTU = config.GetCalculationFactor<decimal>("spaceheater.heaterbtu");
            var heatingWeeks = config.GetCalculationFactor<decimal>("spaceheater.heatingweeks");
            var usageMultiplier = config.GetCalculationFactor<decimal>("spaceheater.usagefactormultiplier");

            var count = Convert.ToDecimal(pa.Find(p => p.Key == "spaceheater.count").OptionValue);
            var usage = Convert.ToDecimal(pa.Find(p => p.Key == "spaceheater.usage").OptionValue);
            var fueltype = pa.Find(p => p.Key == "spaceheater.fuel").OptionKey;

            var btus = heaterBTU*count*usageMultiplier*usage*heatingWeeks;

            switch (fueltype)
            {
                case "spaceheater.fuel.electric":
                    var outputkWh = btus/3413.0m;
                    _appliance.EnergyUses.Add(new EnergyUse(true)
                    {
                        Commodity = Enums.CommodityType.Electric,
                        Quantity = outputkWh,
                        UnitOfMeasure = Enums.UnitOfMeasureType.kWh
                    });
                    _appliance.EnergyUses.Add(new EnergyUse(true)
                    {
                        Commodity = Enums.CommodityType.Electric,
                        Quantity = btus,
                        UnitOfMeasure = Enums.UnitOfMeasureType.BTU
                    });
                    break;
                case "spaceheater.fuel.propane":
                    var outputGal = btus/92000.0m;
                    _appliance.EnergyUses.Add(new EnergyUse(true)
                    {
                        Commodity = Enums.CommodityType.Propane,
                        Quantity = outputGal,
                        UnitOfMeasure = Enums.UnitOfMeasureType.Gal
                    });
                    _appliance.EnergyUses.Add(new EnergyUse(true)
                    {
                        Commodity = Enums.CommodityType.Propane,
                        Quantity = btus,
                        UnitOfMeasure = Enums.UnitOfMeasureType.BTU
                    });
                    break;
                default:
                    _appliance.EnergyUses.Add(new EnergyUse(true)
                    {
                        Commodity = Enums.CommodityType.Electric,
                        Quantity = 0.0m,
                        UnitOfMeasure = Enums.UnitOfMeasureType.kWh
                    });
                    break;
            }

        }

        /// <summary>
        /// Model for a refigerator
        /// </summary>
        private void Model_refrigerator()
        {
            var builUpLoss = 0.0m;
            decimal maintFactor;
            decimal moistureSave;

            var pa = _input.ProfileAttributes;
            var config = _input.Configuration;

            var age = ResetDefaultYear(pa, "refrigerator.year");

            //frost free
            var frostfree =
                Convert.ToBoolean(Convert.ToInt32(pa.Find(p => p.Key == "refrigerator.frostfree").OptionValue));

            if (!frostfree)
            {
                var defrostRegularly =
                    Convert.ToBoolean(Convert.ToInt32(pa.Find(p => p.Key == "refrigerator.defrostregularly").OptionValue));
                if (!defrostRegularly)
                {
                    builUpLoss = config.GetCalculationFactor<decimal>("refrigerator.builduploss");
                }
            }

            var maintainRegularly =
                Convert.ToBoolean(Convert.ToInt32(pa.Find(p => p.Key == "refrigerator.maintainregularly").OptionValue));
            if (maintainRegularly)
            {
                maintFactor = 1.0m;
            }
            else
            {
                string ageMaintFactorString;

                if (age > 15)
                {
                    ageMaintFactorString = "refrigerator.agemaint_factor_gt15";
                }
                else if (age > 10)
                {
                    ageMaintFactorString = "refrigerator.agemaint_factor_10to15";
                }
                else if (age > 5)
                {
                    ageMaintFactorString = "refrigerator.agemaint_factor_5to10";
                }
                else if (age > 1)
                {
                    ageMaintFactorString = "refrigerator.agemaint_factor_1to5";
                }
                else
                {
                    ageMaintFactorString = "refrigerator.agemaint_factor_0";
                }

                //get maint factor based upon age
                maintFactor = config.GetCalculationFactor<decimal>(ageMaintFactorString);
            }

            // temperature setting factor from profile attribute
            var temperatureFactor = Convert.ToDecimal(pa.Find(p => p.Key == "refrigerator.temperatureset").OptionValue);

            // Degree day ratio
            var ddr = _input.WeatherInfo.AnnualHDD/(decimal) _input.WeatherInfo.AnnualCDD;


            // location option key;  ex: refrigerator.location.pantry
            string locationFactorString;
            var location = pa.Find(p => p.Key == "refrigerator.location").OptionKey;

            if (ddr > 3.5m)
            {
                switch (location)
                {
                    case "refrigerator.location.kitchen":
                        locationFactorString = "refrigerator.loc_kitchen_ddr_gt35";
                        break;
                    case "refrigerator.location.pantry":
                        locationFactorString = "refrigerator.loc_pantry_ddr_gt35";
                        break;
                    case "refrigerator.location.basement":
                        locationFactorString = "refrigerator.loc_basement_ddr_gt35";
                        break;
                    case "refrigerator.location.garage":
                        locationFactorString = "refrigerator.loc_garage_ddr_gt35";
                        break;
                    default: //refrigerator.location.other
                        locationFactorString = "refrigerator.loc_other_ddr_gt35";
                        break;
                }
            }
            else if (ddr > 2.5m)
            {
                switch (location)
                {
                    case "refrigerator.location.kitchen":
                        locationFactorString = "refrigerator.loc_kitchen_ddr_25to35";
                        break;
                    case "refrigerator.location.pantry":
                        locationFactorString = "refrigerator.loc_pantry_ddr_25to35";
                        break;
                    case "refrigerator.location.basement":
                        locationFactorString = "refrigerator.loc_basement_ddr_25to35";
                        break;
                    case "refrigerator.location.garage":
                        locationFactorString = "refrigerator.loc_garage_ddr_25to35";
                        break;
                    default: //refrigerator.location.other
                        locationFactorString = "refrigerator.loc_other_ddr_25to35";
                        break;
                }
            }
            else
            {
                switch (location)
                {
                    case "refrigerator.location.kitchen":
                        locationFactorString = "refrigerator.loc_kitchen_ddr_lt25";
                        break;
                    case "refrigerator.location.pantry":
                        locationFactorString = "refrigerator.loc_pantry_ddr_lt25";
                        break;
                    case "refrigerator.location.basement":
                        locationFactorString = "refrigerator.loc_basement_ddr_lt25";
                        break;
                    case "refrigerator.location.garage":
                        locationFactorString = "refrigerator.loc_garage_ddr_lt25";
                        break;
                    default: //refrigerator.location.other
                        locationFactorString = "refrigerator.loc_other_ddr_lt25";
                        break;
                }
            }

            //get location factor based degree day ratio and location of freezer in house
            var locationFactor = config.GetCalculationFactor<decimal>(locationFactorString);

            var style = pa.Find(p => p.Key == "refrigerator.style").OptionKey;

            var sTemp = "refrigerator.energyusebase_";
            switch (style)
            {
                case "refrigerator.style.topfreezer":
                    sTemp = sTemp + "topfrz_";
                    break;
                case "refrigerator.style.bottomfreezer":
                    sTemp = sTemp + "botfrz_";
                    break;
                case "refrigerator.style.sidebyside":
                    sTemp = sTemp + "side_";
                    break;
                case "refrigerator.style.singledoor":
                    sTemp = sTemp + "1door_";
                    break;
                default:
                    sTemp = sTemp + "topfrz_";
                    break;
            }

            var size = Convert.ToDecimal(pa.Find(p => p.Key == "refrigerator.size").OptionValue);
            if (size > 24)
            {
                sTemp = sTemp + "gt24";
            }
            else if (size >= 22)
            {
                sTemp = sTemp + "22to24";
            }
            else if (size >= 16)
            {
                sTemp = sTemp + "16to21";
            }
            else if (size >= 13)
            {
                sTemp = sTemp + "13to15";
            }
            else
            {
                sTemp = sTemp + "lt13";
            }

            var energyUseBase = config.GetCalculationFactor<decimal>(sTemp);

            if (!frostfree)
            {
                var frostFreeFactor = config.GetCalculationFactor<decimal>("refrigerator.manualdefrostfactor");
                energyUseBase = energyUseBase + frostFreeFactor;
            }

            var thruDoorIce =
                Convert.ToBoolean(Convert.ToInt32(pa.Find(p => p.Key == "refrigerator.thrudoorice").OptionValue));

            if (thruDoorIce)
            {
                var thruDoorIceFactor = config.GetCalculationFactor<decimal>("refrigerator.thrudooricefactor");
                energyUseBase = energyUseBase + thruDoorIceFactor;
            }

            // age factor
            string ageEnerygUseFactorString;

            if (age > 20)
            {
                ageEnerygUseFactorString = "refrigerator.ageuse_factor_gt20";
            }
            else if (age > 16)
            {
                ageEnerygUseFactorString = "refrigerator.ageuse_factor_16to20";
            }
            else if (age > 11)
            {
                ageEnerygUseFactorString = "refrigerator.ageuse_factor_11to15";
            }
            else if (age > 6)
            {
                ageEnerygUseFactorString = "refrigerator.ageuse_factor_6to10";
            }
            else if (age > 2)
            {
                ageEnerygUseFactorString = "refrigerator.ageuse_factor_2to5";
            }
            else
            {
                ageEnerygUseFactorString = "refrigerator.ageuse_factor_lt2";
            }

            //get energy use factor based upon age
            var ageEnergyUseFactor = config.GetCalculationFactor<decimal>(ageEnerygUseFactorString);

            var energyUse = ageEnergyUseFactor*energyUseBase;

            //last part
            var moistureSwitch =
                Convert.ToBoolean(Convert.ToInt32(pa.Find(p => p.Key == "refrigerator.moistureswitch").OptionValue));
            if (moistureSwitch)
            {
                var monthsMoistOff = Convert.ToDecimal(pa.Find(p => p.Key == "refrigerator.monthsmoistoff").OptionValue);
                moistureSave = 8.3m*(monthsMoistOff - 6);
            }
            else
            {
                moistureSave = 0.0m;
            }

            // count
            var count = Convert.ToDecimal(pa.Find(p => p.Key == "refrigerator.count").OptionValue);

            // months off
            var monthsOff = Convert.ToDecimal(pa.Find(p => p.Key == "refrigerator.monthsoff").OptionValue);

            //FINAL
            var outputkWh = count*(energyUse + builUpLoss)*maintFactor*temperatureFactor*locationFactor*
                            ((12 - monthsOff)/12) - moistureSave;

            // add energy use        
            _appliance.EnergyUses.Add(new EnergyUse(true)
            {
                Commodity = Enums.CommodityType.Electric,
                Quantity = outputkWh,
                UnitOfMeasure = Enums.UnitOfMeasureType.kWh
            });
        }

        /// <summary>
        /// Model for a second refrigerator
        /// </summary>
        private void Model_refrigerator2()
        {
            var builUpLoss = 0.0m;
            decimal maintFactor;
            decimal moistureSave;

            var pa = _input.ProfileAttributes;
            var config = _input.Configuration;

            var age = ResetDefaultYear(pa, "refrigerator2.year");

            //frost free
            var frostfree =
                Convert.ToBoolean(Convert.ToInt32(pa.Find(p => p.Key == "refrigerator2.frostfree").OptionValue));

            if (!frostfree)
            {
                var defrostRegularly =
                    Convert.ToBoolean(
                        Convert.ToInt32(pa.Find(p => p.Key == "refrigerator2.defrostregularly").OptionValue));
                if (!defrostRegularly)
                {
                    builUpLoss = config.GetCalculationFactor<decimal>("refrigerator.builduploss");
                }
            }

            var maintainRegularly =
                Convert.ToBoolean(Convert.ToInt32(pa.Find(p => p.Key == "refrigerator.maintainregularly").OptionValue));
            if (maintainRegularly)
            {
                maintFactor = 1.0m;
            }
            else
            {
                string ageMaintFactorString;

                if (age > 15)
                {
                    ageMaintFactorString = "refrigerator.agemaint_factor_gt15";
                }
                else if (age > 10)
                {
                    ageMaintFactorString = "refrigerator.agemaint_factor_10to15";
                }
                else if (age > 5)
                {
                    ageMaintFactorString = "refrigerator.agemaint_factor_5to10";
                }
                else if (age > 1)
                {
                    ageMaintFactorString = "refrigerator.agemaint_factor_1to5";
                }
                else
                {
                    ageMaintFactorString = "refrigerator.agemaint_factor_0";
                }

                //get maint factor based upon age
                maintFactor = config.GetCalculationFactor<decimal>(ageMaintFactorString);
            }

            // temperature setting factor from profile attribute
            var temperatureFactor = Convert.ToDecimal(pa.Find(p => p.Key == "refrigerator2.temperatureset").OptionValue);

            // Degree day ratio
            var ddr = _input.WeatherInfo.AnnualHDD/(decimal) _input.WeatherInfo.AnnualCDD;


            // location option key;  ex: refrigerator.location.pantry
            string locationFactorString;
            string location = pa.Find(p => p.Key == "refrigerator2.location").OptionKey;

            if (ddr > 3.5m)
            {
                switch (location)
                {
                    case "refrigerator.location.kitchen":
                        locationFactorString = "refrigerator.loc_kitchen_ddr_gt35";
                        break;
                    case "refrigerator.location.pantry":
                        locationFactorString = "refrigerator.loc_pantry_ddr_gt35";
                        break;
                    case "refrigerator.location.basement":
                        locationFactorString = "refrigerator.loc_basement_ddr_gt35";
                        break;
                    case "refrigerator.location.garage":
                        locationFactorString = "refrigerator.loc_garage_ddr_gt35";
                        break;
                    default: //refrigerator.location.other
                        locationFactorString = "refrigerator.loc_other_ddr_gt35";
                        break;
                }
            }
            else if (ddr > 2.5m)
            {
                switch (location)
                {
                    case "refrigerator.location.kitchen":
                        locationFactorString = "refrigerator.loc_kitchen_ddr_25to35";
                        break;
                    case "refrigerator.location.pantry":
                        locationFactorString = "refrigerator.loc_pantry_ddr_25to35";
                        break;
                    case "refrigerator.location.basement":
                        locationFactorString = "refrigerator.loc_basement_ddr_25to35";
                        break;
                    case "refrigerator.location.garage":
                        locationFactorString = "refrigerator.loc_garage_ddr_25to35";
                        break;
                    default: //refrigerator.location.other
                        locationFactorString = "refrigerator.loc_other_ddr_25to35";
                        break;
                }
            }
            else
            {
                switch (location)
                {
                    case "refrigerator.location.kitchen":
                        locationFactorString = "refrigerator.loc_kitchen_ddr_lt25";
                        break;
                    case "refrigerator.location.pantry":
                        locationFactorString = "refrigerator.loc_pantry_ddr_lt25";
                        break;
                    case "refrigerator.location.basement":
                        locationFactorString = "refrigerator.loc_basement_ddr_lt25";
                        break;
                    case "refrigerator.location.garage":
                        locationFactorString = "refrigerator.loc_garage_ddr_lt25";
                        break;
                    default: //refrigerator.location.other
                        locationFactorString = "refrigerator.loc_other_ddr_lt25";
                        break;
                }
            }

            //get location factor based degree day ratio and location of freezer in house
            var locationFactor = config.GetCalculationFactor<decimal>(locationFactorString);

            var style = pa.Find(p => p.Key == "refrigerator2.style").OptionKey;

            var sTemp = "refrigerator.energyusebase_";
            switch (style)
            {
                case "refrigerator.style.topfreezer":
                    sTemp = sTemp + "topfrz_";
                    break;
                case "refrigerator.style.bottomfreezer":
                    sTemp = sTemp + "botfrz_";
                    break;
                case "refrigerator.style.sidebyside":
                    sTemp = sTemp + "side_";
                    break;
                case "refrigerator.style.singledoor":
                    sTemp = sTemp + "1door_";
                    break;
                default:
                    sTemp = sTemp + "topfrz_";
                    break;
            }

            var size = Convert.ToDecimal(pa.Find(p => p.Key == "refrigerator2.size").OptionValue);
            if (size > 24)
            {
                sTemp = sTemp + "gt24";
            }
            else if (size >= 22)
            {
                sTemp = sTemp + "22to24";
            }
            else if (size >= 16)
            {
                sTemp = sTemp + "16to21";
            }
            else if (size >= 13)
            {
                sTemp = sTemp + "13to15";
            }
            else
            {
                sTemp = sTemp + "lt13";
            }

            var energyUseBase = config.GetCalculationFactor<decimal>(sTemp);

            if (!frostfree)
            {
                var frostFreeFactor = config.GetCalculationFactor<decimal>("refrigerator.manualdefrostfactor");
                energyUseBase = energyUseBase + frostFreeFactor;
            }

            var thruDoorIce =
                Convert.ToBoolean(Convert.ToInt32(pa.Find(p => p.Key == "refrigerator2.thrudoorice").OptionValue));

            if (thruDoorIce)
            {
                var thruDoorIceFactor = config.GetCalculationFactor<decimal>("refrigerator.thrudooricefactor");
                energyUseBase = energyUseBase + thruDoorIceFactor;
            }

            // age factor
            string ageEnerygUseFactorString;

            if (age > 20)
            {
                ageEnerygUseFactorString = "refrigerator.ageuse_factor_gt20";
            }
            else if (age > 16)
            {
                ageEnerygUseFactorString = "refrigerator.ageuse_factor_16to20";
            }
            else if (age > 11)
            {
                ageEnerygUseFactorString = "refrigerator.ageuse_factor_11to15";
            }
            else if (age > 6)
            {
                ageEnerygUseFactorString = "refrigerator.ageuse_factor_6to10";
            }
            else if (age > 2)
            {
                ageEnerygUseFactorString = "refrigerator.ageuse_factor_2to5";
            }
            else
            {
                ageEnerygUseFactorString = "refrigerator.ageuse_factor_lt2";
            }

            //get energy use factor based upon age
            var ageEnergyUseFactor = config.GetCalculationFactor<decimal>(ageEnerygUseFactorString);

            var energyUse = ageEnergyUseFactor*energyUseBase;

            //last part
            var moistureSwitch =
                Convert.ToBoolean(Convert.ToInt32(pa.Find(p => p.Key == "refrigerator2.moistureswitch").OptionValue));
            if (moistureSwitch)
            {
                var monthsMoistOff = Convert.ToDecimal(pa.Find(p => p.Key == "refrigerator2.monthsmoistoff").OptionValue);
                moistureSave = 8.3m*(monthsMoistOff - 6);
            }
            else
            {
                moistureSave = 0.0m;
            }

            // count
            var count = Convert.ToDecimal(pa.Find(p => p.Key == "refrigerator2.count").OptionValue);

            // months off
            var monthsOff = Convert.ToDecimal(pa.Find(p => p.Key == "refrigerator2.monthsoff").OptionValue);

            //FINAL
            var outputkWh = count*(energyUse + builUpLoss)*maintFactor*temperatureFactor*locationFactor*
                            ((12 - monthsOff)/12) - moistureSave;

            // add energy use        
            _appliance.EnergyUses.Add(new EnergyUse(true)
            {
                Commodity = Enums.CommodityType.Electric,
                Quantity = outputkWh,
                UnitOfMeasure = Enums.UnitOfMeasureType.kWh
            });
        }

        /// <summary>
        /// Model for an oven
        /// </summary>
        private void Model_oven()
        {
            decimal output;
            decimal energyUse;

            var pa = _input.ProfileAttributes;
            var config = _input.Configuration;

            //for the oven, get the adjustment factor of having a microwave that is used for meals
            decimal microwavefactor;
            var microwaveMealUsageKey = pa.Find(p => p.Key == "microwave.usageformeals").OptionKey;

            switch (microwaveMealUsageKey)
            {
                case "microwave.usageformeals.often":
                    microwavefactor = config.GetCalculationFactor<decimal>("oven.microwaveusefactor_often");
                    break;
                case "microwave.usageformeals.occasionally":
                    microwavefactor = config.GetCalculationFactor<decimal>("oven.microwaveusefactor_occ");
                    break;
                default:
                    microwavefactor = 1.0m;
                    break;
            }

            //get people quantity
            var people = (int) Convert.ToDecimal(pa.Find(p => p.Key == "house.people").OptionValue);

            // get people factor based upon number of people
            var sTemp = "oven.people_factor";
            if (people >= 6)
            {
                sTemp = sTemp + "_6";
            }
            else if (people >= 5)
            {
                sTemp = sTemp + "_5";
            }
            else if (people >= 4)
            {
                sTemp = sTemp + "_4";
            }
            else if (people >= 3)
            {
                sTemp = sTemp + "_3";
            }
            else if (people >= 2)
            {
                sTemp = sTemp + "_2";
            }
            else if (people >= 1)
            {
                sTemp = sTemp + "_1";
            }
            else
            {
                sTemp = sTemp + "_0";
            }

            var peopleFactor = config.GetCalculationFactor<decimal>(sTemp);

            var usageFactor = Convert.ToDecimal(pa.Find(p => p.Key == "oven.usage").OptionValue);

            var count = Convert.ToDecimal(pa.Find(p => p.Key == "oven.count").OptionValue);

            var isConvection =
                Convert.ToBoolean(Convert.ToInt32(pa.Find(p => p.Key == "oven.convectionoven").OptionValue));
            var convectionFactor = isConvection ? config.GetCalculationFactor<decimal>("oven.convectionfactor") : 1.0m;

            //breakfasts factor
            var breakfastsWkly = Convert.ToDecimal(pa.Find(p => p.Key == "oven.breakfastswkly").OptionValue);
            //lunches factor
            var lunchesWkly = Convert.ToDecimal(pa.Find(p => p.Key == "oven.luncheswkly").OptionValue);
            //dinners factor
            var dinnersWkly = Convert.ToDecimal(pa.Find(p => p.Key == "oven.dinnerswkly").OptionValue);

            //HotMealFactor
            var hotMealFactor = 1 - ((14 - (0.5m*breakfastsWkly + lunchesWkly + 2.0m*dinnersWkly))/14);

            var fueltype = pa.Find(p => p.Key == "oven.fuel").OptionKey;
            switch (fueltype)
            {
                case "oven.fuel.electric":
                    energyUse = config.GetCalculationFactor<decimal>("oven.energyuse_elec");

                    break;
                case "oven.fuel.gas":
                {
                    energyUse = config.GetCalculationFactor<decimal>("oven.energyuse_gas");
                    var isPilot =
                        Convert.ToBoolean(Convert.ToInt32(pa.Find(p => p.Key == "oven.pilotlight").OptionValue));
                    if (isPilot)
                    {
                        var pilotAdd = config.GetCalculationFactor<decimal>("oven.pilotuse");
                        energyUse = energyUse + pilotAdd;
                    }
                    break;
                }
                case "oven.fuel.propane":
                    energyUse = config.GetCalculationFactor<decimal>("oven.energyuse_prop");
                    break;
                default:
                    energyUse = 0.0m;
                    break;
            }

            //BTU
            var btus = 1000000.0m*convectionFactor*energyUse*microwavefactor*peopleFactor*hotMealFactor*usageFactor*
                       (1 + (count - 1.0m)*0.05m);

            switch (fueltype)
            {
                case "oven.fuel.electric":
                    output = btus/3413m*count;
                    _appliance.EnergyUses.Add(new EnergyUse(true)
                    {
                        Commodity = Enums.CommodityType.Electric,
                        Quantity = output,
                        UnitOfMeasure = Enums.UnitOfMeasureType.kWh
                    });
                    break;
                case "oven.fuel.gas":
                    output = btus/100000m*count;
                    _appliance.EnergyUses.Add(new EnergyUse(true)
                    {
                        Commodity = Enums.CommodityType.Gas,
                        Quantity = output,
                        UnitOfMeasure = Enums.UnitOfMeasureType.Therms
                    });
                    break;
                case "oven.fuel.propane":
                    output = btus/92000m*count;
                    _appliance.EnergyUses.Add(new EnergyUse(true)
                    {
                        Commodity = Enums.CommodityType.Propane,
                        Quantity = output,
                        UnitOfMeasure = Enums.UnitOfMeasureType.Gal
                    });
                    break;
                default:
                    output = 0.0m;
                    _appliance.EnergyUses.Add(new EnergyUse(true)
                    {
                        Commodity = Enums.CommodityType.Electric,
                        Quantity = output,
                        UnitOfMeasure = Enums.UnitOfMeasureType.kWh
                    });
                    break;
            }

        }

        /// <summary>
        /// Model for a room vacumn
        /// </summary>
        private void Model_roomac()
        {
            decimal outputkWh;
            var maintFactor = 1.0m;
            var wiredThermstatFactor = 1.0m;

            var pa = _input.ProfileAttributes;
            var config = _input.Configuration;

            var age = ResetDefaultYear(pa, "roomac.year");

            string eerString;
            var efficiency = pa.Find(p => p.Key == "roomac.efficiency").OptionKey;
            switch (efficiency)
            {
                case "roomac.efficiency.excellent":
                    eerString = "roomac.eer_excellent";
                    break;
                case "roomac.efficiency.good":
                    eerString = "roomac.eer_good";
                    break;
                case "roomac.efficiency.average":
                    eerString = "roomac.eer_average";
                    break;
                default:
                    eerString = "roomac.eer_poor";
                    break;
            }

            var eer = config.GetCalculationFactor<decimal>(eerString);

            var serviceFrequencyKey = pa.Find(p => p.Key == "roomac.servicefrequency").OptionKey;
            if (serviceFrequencyKey != "roomac.servicefrequency.regularly")
            {
                string maintFactorString;
                if (age > 15)
                {
                    maintFactorString = "roomac.agemaint_factor1";
                }
                else if (age >= 10)
                {
                    maintFactorString = "roomac.agemaint_factor2";
                }
                else if (age >= 5)
                {
                    maintFactorString = "roomac.agemaint_factor3";
                }
                else
                {
                    maintFactorString = "roomac.agemaint_factor4";
                }

                maintFactor = config.GetCalculationFactor<decimal>(maintFactorString);
            }

            var dayUse = Convert.ToDecimal(pa.Find(p => p.Key == "roomac.dayusage").OptionValue);
            var eveningUse = Convert.ToDecimal(pa.Find(p => p.Key == "roomac.eveningusage").OptionValue);
            var nightUse = Convert.ToDecimal(pa.Find(p => p.Key == "roomac.nightusage").OptionValue);

            var useFactor = 7*(dayUse + eveningUse + nightUse);

            //temperature
            var temperatureFactor = Convert.ToDecimal(pa.Find(p => p.Key == "roomac.temperatureset").OptionValue);


            var sizeInBtu = Convert.ToDecimal(pa.Find(p => p.Key == "roomac.btu").OptionValue);
            string adjustmentString;
            string runningString;
            if (sizeInBtu > 12000)
            {
                adjustmentString = "roomac.sizeadjustgt12k";
                runningString = "roomac.timerungt12k";
            }
            else if (sizeInBtu >= 8000)
            {
                adjustmentString = "roomac.sizeadjust8to12k";
                runningString = "roomac.timerunt8to12k";
            }
            else if (sizeInBtu >= 5000)
            {
                adjustmentString = "roomac.sizeadjust5to8k";
                runningString = "roomac.timerunt5to8k";
            }
            else
            {
                adjustmentString = "roomac.sizeadjustlt5k";
                runningString = "roomac.timerunlt5k";
            }

            var adjFactor = config.GetCalculationFactor<decimal>(adjustmentString);
            var runFactor = config.GetCalculationFactor<decimal>(runningString);

            var isWiredThermostat =
                Convert.ToBoolean(Convert.ToInt32(pa.Find(p => p.Key == "roomac.wiredthermostat").OptionValue));
            if (isWiredThermostat)
            {
                wiredThermstatFactor = config.GetCalculationFactor<decimal>("roomac.wiredthermostatfactor");
            }

            var coolingWeeks = config.GetCalculationFactor<decimal>("roomac.coolingweeks");

            var roomsCooled = Convert.ToDecimal(pa.Find(p => p.Key == "roomac.roomscooled").OptionValue);

            //count
            var count = Convert.ToDecimal(pa.Find(p => p.Key == "roomac.count").OptionValue);

            if (eer > 0)
            {
                outputkWh = count*temperatureFactor*(0.8m + 0.2m*roomsCooled)*wiredThermstatFactor*useFactor*runFactor*
                            adjFactor*coolingWeeks/(eer*maintFactor*1000m);
            }
            else
            {
                outputkWh = 0.0m;
            }

            // add energy use           
            _appliance.EnergyUses.Add(new EnergyUse(true)
            {
                Commodity = Enums.CommodityType.Electric,
                Quantity = outputkWh,
                UnitOfMeasure = Enums.UnitOfMeasureType.kWh
            });
        }

        /// <summary>
        /// Model for room vacumn - 2
        /// </summary>
        private void Model_roomac2()
        {
            decimal outputkWh;
            var maintFactor = 1.0m;
            var wiredThermstatFactor = 1.0m;

            var pa = _input.ProfileAttributes;
            var config = _input.Configuration;

            var age = ResetDefaultYear(pa, "roomac2.year");

            string eerString;
            var efficiency = pa.Find(p => p.Key == "roomac2.efficiency").OptionKey;
            switch (efficiency)
            {
                case "roomac.efficiency.excellent":
                    eerString = "roomac.eer_excellent";
                    break;
                case "roomac.efficiency.good":
                    eerString = "roomac.eer_good";
                    break;
                case "roomac.efficiency.average":
                    eerString = "roomac.eer_average";
                    break;
                default:
                    eerString = "roomac.eer_poor";
                    break;
            }

            var eer = config.GetCalculationFactor<decimal>(eerString);

            var serviceFrequencyKey = pa.Find(p => p.Key == "roomac2.servicefrequency").OptionKey;
            if (serviceFrequencyKey != "roomac.servicefrequency.regularly")
            {
                string maintFactorString;
                if (age > 15)
                {
                    maintFactorString = "roomac.agemaint_factor1";
                }
                else if (age >= 10)
                {
                    maintFactorString = "roomac.agemaint_factor2";
                }
                else if (age >= 5)
                {
                    maintFactorString = "roomac.agemaint_factor3";
                }
                else
                {
                    maintFactorString = "roomac.agemaint_factor4";
                }

                maintFactor = config.GetCalculationFactor<decimal>(maintFactorString);
            }

            var dayUse = Convert.ToDecimal(pa.Find(p => p.Key == "roomac2.dayusage").OptionValue);
            var eveningUse = Convert.ToDecimal(pa.Find(p => p.Key == "roomac2.eveningusage").OptionValue);
            var nightUse = Convert.ToDecimal(pa.Find(p => p.Key == "roomac2.nightusage").OptionValue);

            var useFactor = 7*(dayUse + eveningUse + nightUse);

            //temperature
            var temperatureFactor = Convert.ToDecimal(pa.Find(p => p.Key == "roomac2.temperatureset").OptionValue);


            var sizeInBtu = Convert.ToDecimal(pa.Find(p => p.Key == "roomac2.btu").OptionValue);
            string adjustmentString;
            string runningString;
            if (sizeInBtu > 12000)
            {
                adjustmentString = "roomac.sizeadjustgt12k";
                runningString = "roomac.timerungt12k";
            }
            else if (sizeInBtu >= 8000)
            {
                adjustmentString = "roomac.sizeadjust8to12k";
                runningString = "roomac.timerunt8to12k";
            }
            else if (sizeInBtu >= 5000)
            {
                adjustmentString = "roomac.sizeadjust5to8k";
                runningString = "roomac.timerunt5to8k";
            }
            else
            {
                adjustmentString = "roomac.sizeadjustlt5k";
                runningString = "roomac.timerunlt5k";
            }

            var adjFactor = config.GetCalculationFactor<decimal>(adjustmentString);
            var runFactor = config.GetCalculationFactor<decimal>(runningString);

            var isWiredThermostat =
                Convert.ToBoolean(Convert.ToInt32(pa.Find(p => p.Key == "roomac2.wiredthermostat").OptionValue));
            if (isWiredThermostat)
            {
                wiredThermstatFactor = config.GetCalculationFactor<decimal>("roomac.wiredthermostatfactor");
            }

            var coolingWeeks = config.GetCalculationFactor<decimal>("roomac.coolingweeks");

            var roomsCooled = Convert.ToDecimal(pa.Find(p => p.Key == "roomac2.roomscooled").OptionValue);

            //count
            var count = Convert.ToDecimal(pa.Find(p => p.Key == "roomac2.count").OptionValue);

            if (eer > 0)
            {
                outputkWh = count*temperatureFactor*(0.8m + 0.2m*roomsCooled)*wiredThermstatFactor*useFactor*runFactor*
                            adjFactor*coolingWeeks/(eer*maintFactor*1000m);
            }
            else
            {
                outputkWh = 0.0m;
            }

            // add energy use           
            _appliance.EnergyUses.Add(new EnergyUse(true)
            {
                Commodity = Enums.CommodityType.Electric,
                Quantity = outputkWh,
                UnitOfMeasure = Enums.UnitOfMeasureType.kWh
            });
        }

        /// <summary>
        /// Model for room vacumn - 3
        /// </summary>
        private void Model_roomac3()
        {
            decimal outputkWh;
            var maintFactor = 1.0m;
            var wiredThermstatFactor = 1.0m;

            var pa = _input.ProfileAttributes;
            var config = _input.Configuration;

            var age = ResetDefaultYear(pa, "roomac3.year");

            string eerString;
            var efficiency = pa.Find(p => p.Key == "roomac3.efficiency").OptionKey;
            switch (efficiency)
            {
                case "roomac.efficiency.excellent":
                    eerString = "roomac.eer_excellent";
                    break;
                case "roomac.efficiency.good":
                    eerString = "roomac.eer_good";
                    break;
                case "roomac.efficiency.average":
                    eerString = "roomac.eer_average";
                    break;
                default:
                    eerString = "roomac.eer_poor";
                    break;
            }

            var eer = config.GetCalculationFactor<decimal>(eerString);

            var serviceFrequencyKey = pa.Find(p => p.Key == "roomac3.servicefrequency").OptionKey;
            if (serviceFrequencyKey != "roomac.servicefrequency.regularly")
            {
                string maintFactorString;
                if (age > 15)
                {
                    maintFactorString = "roomac.agemaint_factor1";
                }
                else if (age >= 10)
                {
                    maintFactorString = "roomac.agemaint_factor2";
                }
                else if (age >= 5)
                {
                    maintFactorString = "roomac.agemaint_factor3";
                }
                else
                {
                    maintFactorString = "roomac.agemaint_factor4";
                }

                maintFactor = config.GetCalculationFactor<decimal>(maintFactorString);
            }

            var dayUse = Convert.ToDecimal(pa.Find(p => p.Key == "roomac3.dayusage").OptionValue);
            var eveningUse = Convert.ToDecimal(pa.Find(p => p.Key == "roomac3.eveningusage").OptionValue);
            var nightUse = Convert.ToDecimal(pa.Find(p => p.Key == "roomac3.nightusage").OptionValue);

            var useFactor = 7*(dayUse + eveningUse + nightUse);

            //temperature
            var temperatureFactor = Convert.ToDecimal(pa.Find(p => p.Key == "roomac3.temperatureset").OptionValue);


            var sizeInBtu = Convert.ToDecimal(pa.Find(p => p.Key == "roomac3.btu").OptionValue);
            string adjustmentString;
            string runningString;
            if (sizeInBtu > 12000)
            {
                adjustmentString = "roomac.sizeadjustgt12k";
                runningString = "roomac.timerungt12k";
            }
            else if (sizeInBtu >= 8000)
            {
                adjustmentString = "roomac.sizeadjust8to12k";
                runningString = "roomac.timerunt8to12k";
            }
            else if (sizeInBtu >= 5000)
            {
                adjustmentString = "roomac.sizeadjust5to8k";
                runningString = "roomac.timerunt5to8k";
            }
            else
            {
                adjustmentString = "roomac.sizeadjustlt5k";
                runningString = "roomac.timerunlt5k";
            }

            var adjFactor = config.GetCalculationFactor<decimal>(adjustmentString);
            var runFactor = config.GetCalculationFactor<decimal>(runningString);

            var isWiredThermostat =
                Convert.ToBoolean(Convert.ToInt32(pa.Find(p => p.Key == "roomac3.wiredthermostat").OptionValue));
            if (isWiredThermostat)
            {
                wiredThermstatFactor = config.GetCalculationFactor<decimal>("roomac.wiredthermostatfactor");
            }

            var coolingWeeks = config.GetCalculationFactor<decimal>("roomac.coolingweeks");

            var roomsCooled = Convert.ToDecimal(pa.Find(p => p.Key == "roomac3.roomscooled").OptionValue);

            //count
            var count = Convert.ToDecimal(pa.Find(p => p.Key == "roomac3.count").OptionValue);

            if (eer > 0)
            {
                outputkWh = count*temperatureFactor*(0.8m + 0.2m*roomsCooled)*wiredThermstatFactor*useFactor*runFactor*
                            adjFactor*coolingWeeks/(eer*maintFactor*1000m);
            }
            else
            {
                outputkWh = 0.0m;
            }

            // add energy use           
            _appliance.EnergyUses.Add(new EnergyUse(true)
            {
                Commodity = Enums.CommodityType.Electric,
                Quantity = outputkWh,
                UnitOfMeasure = Enums.UnitOfMeasureType.kWh
            });
        }


        /// <summary>
        /// Model for a fireplace
        /// </summary>
        private void Model_fireplace()
        {
            var pa = _input.ProfileAttributes;
            var config = _input.Configuration;

            var fireBtu = config.GetCalculationFactor<decimal>("fireplace.firebtu");
            var fireEff = config.GetCalculationFactor<decimal>("fireplace.fireeff");
            var btuDivisor = config.GetCalculationFactor<decimal>("fireplace.btudivisor");

            var count = Convert.ToDecimal(pa.Find(p => p.Key == "fireplace.count").OptionValue);
            var usage = Convert.ToDecimal(pa.Find(p => p.Key == "fireplace.usage").OptionValue);
            var fueltype = pa.Find(p => p.Key == "fireplace.fuel").OptionKey;

            var btus = fireBtu*usage*count*(_input.WeatherInfo.AnnualHDD/btuDivisor);

            if (fireEff > 0.0m)
            {
                decimal output;
                switch (fueltype)
                {
                    case "fireplace.fuel.gas":
                        output = btus/(fireEff*100000);
                        _appliance.EnergyUses.Add(new EnergyUse(true)
                        {
                            Commodity = Enums.CommodityType.Gas,
                            Quantity = output,
                            UnitOfMeasure = Enums.UnitOfMeasureType.Therms
                        });
                        _appliance.EnergyUses.Add(new EnergyUse(true)
                        {
                            Commodity = Enums.CommodityType.Gas,
                            Quantity = btus,
                            UnitOfMeasure = Enums.UnitOfMeasureType.BTU
                        });
                        break;
                    case "fireplace.fuel.propane":
                        output = btus/(fireEff*92000);
                        _appliance.EnergyUses.Add(new EnergyUse(true)
                        {
                            Commodity = Enums.CommodityType.Propane,
                            Quantity = output,
                            UnitOfMeasure = Enums.UnitOfMeasureType.Gal
                        });
                        _appliance.EnergyUses.Add(new EnergyUse(true)
                        {
                            Commodity = Enums.CommodityType.Propane,
                            Quantity = btus,
                            UnitOfMeasure = Enums.UnitOfMeasureType.BTU
                        });
                        break;
                    case "fireplace.fuel.gasandwood":
                        output = 0.5m*(btus/(fireEff*100000));
                        _appliance.EnergyUses.Add(new EnergyUse(true)
                        {
                            Commodity = Enums.CommodityType.Gas,
                            Quantity = output,
                            UnitOfMeasure = Enums.UnitOfMeasureType.Therms
                        });
                        _appliance.EnergyUses.Add(new EnergyUse(true)
                        {
                            Commodity = Enums.CommodityType.Gas,
                            Quantity = btus,
                            UnitOfMeasure = Enums.UnitOfMeasureType.BTU
                        });
                        break;
                    default:
                        _appliance.EnergyUses.Add(new EnergyUse(true)
                        {
                            Commodity = Enums.CommodityType.Electric,
                            Quantity = 0.0m,
                            UnitOfMeasure = Enums.UnitOfMeasureType.kWh
                        });
                        break;
                }
            }
            else
            {
                _appliance.EnergyUses.Add(new EnergyUse(true)
                {
                    Commodity = Enums.CommodityType.Electric,
                    Quantity = 0.0m,
                    UnitOfMeasure = Enums.UnitOfMeasureType.kWh
                });
            }

        }

        /// <summary>
        /// Model for a woodstove
        /// </summary>
        private void Model_woodstove()
        {
            var pa = _input.ProfileAttributes;
            var config = _input.Configuration;

            var stoveBtu = config.GetCalculationFactor<decimal>("woodstove.stovebtu");
            var heatingWeeks = config.GetCalculationFactor<decimal>("woodstove.heatingweeks");

            var count = Convert.ToDecimal(pa.Find(p => p.Key == "woodstove.count").OptionValue);
            var usage = Convert.ToDecimal(pa.Find(p => p.Key == "woodstove.usage").OptionValue);

            var btus = stoveBtu*count*usage*heatingWeeks;

            _appliance.EnergyUses.Add(new EnergyUse(true)
            {
                Commodity = Enums.CommodityType.Wood,
                Quantity = btus,
                UnitOfMeasure = Enums.UnitOfMeasureType.BTU
            });

        }

        /// <summary>
        /// Model for a hottub
        /// </summary>
        private void Model_hottub()
        {
            decimal output;
            decimal btus;
            decimal coverFactor;
            decimal effectOfCover;
            decimal energyUse;
            decimal pumpCons;
            decimal waterEvap;
            decimal volume;
            decimal effFactor;

            var pa = _input.ProfileAttributes;
            var config = _input.Configuration;

            var count = Convert.ToDecimal(pa.Find(p => p.Key == "hottub.count").OptionValue);

            var coverUsageKey = pa.Find(p => p.Key == "hottub.coverusage").OptionKey;
            switch (coverUsageKey)
            {
                case "hottub.coverusage.useregularly":
                    coverFactor = config.GetCalculationFactor<decimal>("hottub.coverfactor1");
                    effectOfCover = config.GetCalculationFactor<decimal>("hottub.covereffect1");
                    break;
                case "hottub.coverusage.useirregularly":
                    coverFactor = config.GetCalculationFactor<decimal>("hottub.coverfactor2");
                    effectOfCover = config.GetCalculationFactor<decimal>("hottub.covereffect2");
                    break;
                default:
                    coverFactor = 1.0m;
                    effectOfCover = 1.0m;
                    break;
            }

            var sizeKey = pa.Find(p => p.Key == "hottub.size").OptionKey;
            switch (sizeKey)
            {
                case "hottub.size.small":
                    energyUse = config.GetCalculationFactor<decimal>("hottub.energyuse_sm");
                    pumpCons = config.GetCalculationFactor<decimal>("hottub.pumpcons_sm");
                    waterEvap = config.GetCalculationFactor<decimal>("hottub.waterevap_sm");
                    volume = config.GetCalculationFactor<decimal>("hottub.volume_sm");
                    break;
                case "hottub.size.medium":
                    energyUse = config.GetCalculationFactor<decimal>("hottub.energyuse_md");
                    pumpCons = config.GetCalculationFactor<decimal>("hottub.pumpcons_md");
                    waterEvap = config.GetCalculationFactor<decimal>("hottub.waterevap_md");
                    volume = config.GetCalculationFactor<decimal>("hottub.volume_md");
                    break;
                //case "hottub.size.large":
                default:
                    energyUse = config.GetCalculationFactor<decimal>("hottub.energyuse_lg");
                    pumpCons = config.GetCalculationFactor<decimal>("hottub.pumpcons_lg");
                    waterEvap = config.GetCalculationFactor<decimal>("hottub.waterevap_lg");
                    volume = config.GetCalculationFactor<decimal>("hottub.volume_lg");
                    break;
            }

            var monthsHeated = Convert.ToDecimal(pa.Find(p => p.Key == "hottub.monthsheated").OptionValue);
            var temperature = Convert.ToDecimal(pa.Find(p => p.Key == "hottub.temperature").OptionValue);
            var timesRefilled = Convert.ToDecimal(pa.Find(p => p.Key == "hottub.timesrefilled").OptionValue);
            var isInside = Convert.ToBoolean(Convert.ToInt32(pa.Find(p => p.Key == "hottub.inside").OptionValue));

            var timeUsed = monthsHeated/12;
            var tempFactor = 1 + ((temperature - 102)*0.02m);
            if (isInside)
            {
                tempFactor = tempFactor*config.GetCalculationFactor<decimal>("hottub.insideFactor");
            }

            //water output
            var outputWater = (timesRefilled*volume + timeUsed*waterEvap*effectOfCover)*count;
            _appliance.EnergyUses.Add(new EnergyUse(true)
            {
                Commodity = Enums.CommodityType.Water,
                Quantity = outputWater,
                UnitOfMeasure = Enums.UnitOfMeasureType.Gal
            });

            // fuel output
            var fueltype = pa.Find(p => p.Key == "hottub.fuel").OptionKey;
            switch (fueltype)
            {
                case "hottub.fuel.gas":
                    effFactor = config.GetCalculationFactor<decimal>("hottub.eff_gas");
                    btus = timeUsed*energyUse*tempFactor/(effFactor*coverFactor);
                    output = btus/100000*count;
                    _appliance.EnergyUses.Add(new EnergyUse(true)
                    {
                        Commodity = Enums.CommodityType.Gas,
                        Quantity = output,
                        UnitOfMeasure = Enums.UnitOfMeasureType.Therms
                    });
                    break;

                case "hottub.fuel.propane":
                    effFactor = config.GetCalculationFactor<decimal>("hottub.eff_prop");
                    btus = timeUsed*energyUse*tempFactor/(effFactor*coverFactor);
                    output = btus/92000*count;
                    _appliance.EnergyUses.Add(new EnergyUse(true)
                    {
                        Commodity = Enums.CommodityType.Propane,
                        Quantity = output,
                        UnitOfMeasure = Enums.UnitOfMeasureType.Gal
                    });
                    break;

                case "hottub.fuel.oil":
                    effFactor = config.GetCalculationFactor<decimal>("hottub.eff_oil");
                    btus = timeUsed*energyUse*tempFactor/(effFactor*coverFactor);
                    output = btus/138690*count;
                    _appliance.EnergyUses.Add(new EnergyUse(true)
                    {
                        Commodity = Enums.CommodityType.Oil,
                        Quantity = output,
                        UnitOfMeasure = Enums.UnitOfMeasureType.Gal
                    });
                    break;

                case "hottub.fuel.electric":
                    effFactor = config.GetCalculationFactor<decimal>("hottub.eff_elec");
                    btus = timeUsed*energyUse*tempFactor/(effFactor*coverFactor);
                    output = (btus/3413 + timeUsed*pumpCons)*count;
                    _appliance.EnergyUses.Add(new EnergyUse(true)
                    {
                        Commodity = Enums.CommodityType.Electric,
                        Quantity = output,
                        UnitOfMeasure = Enums.UnitOfMeasureType.kWh
                    });
                    break;

                default:
                    _appliance.EnergyUses.Add(new EnergyUse(true)
                    {
                        Commodity = Enums.CommodityType.Electric,
                        Quantity = 0.0m,
                        UnitOfMeasure = Enums.UnitOfMeasureType.kWh
                    });
                    break;

            }
        }

        /// <summary>
        /// Model for a cooktop
        /// </summary>
        private void Model_cooktop()
        {
            decimal output;
            decimal pilotAdd;
            decimal energyUse;
            bool isPilot;

            var pa = _input.ProfileAttributes;
            var config = _input.Configuration;

            //for the oven, get the adjustment factor of having a microwave that is used for meals
            decimal microwavefactor;
            var microwaveMealUsageKey = pa.Find(p => p.Key == "microwave.usageformeals").OptionKey;

            switch (microwaveMealUsageKey)
            {
                case "microwave.usageformeals.often":
                    microwavefactor = config.GetCalculationFactor<decimal>("cooktop.microwaveusefactor_often");
                    break;
                case "microwave.usageformeals.occasionally":
                    microwavefactor = config.GetCalculationFactor<decimal>("cooktop.microwaveusefactor_occ");
                    break;
                default:
                    microwavefactor = 1.0m;
                    break;
            }

            //get people quantity
            var people = (int) Convert.ToDecimal(pa.Find(p => p.Key == "house.people").OptionValue);

            // get people factor based upon number of people
            var sTemp = "cooktop.people_factor";
            if (people >= 6)
            {
                sTemp = sTemp + "_6";
            }
            else if (people >= 5)
            {
                sTemp = sTemp + "_5";
            }
            else if (people >= 4)
            {
                sTemp = sTemp + "_4";
            }
            else if (people >= 3)
            {
                sTemp = sTemp + "_3";
            }
            else if (people >= 2)
            {
                sTemp = sTemp + "_2";
            }
            else if (people >= 1)
            {
                sTemp = sTemp + "_1";
            }
            else
            {
                sTemp = sTemp + "_0";
            }

            var peopleFactor = config.GetCalculationFactor<decimal>(sTemp);

            var usageFactor = Convert.ToDecimal(pa.Find(p => p.Key == "cooktop.usage").OptionValue);

            var count = Convert.ToDecimal(pa.Find(p => p.Key == "cooktop.count").OptionValue);


            //breakfasts factor
            var breakfastsWkly = Convert.ToDecimal(pa.Find(p => p.Key == "cooktop.breakfastswkly").OptionValue);
            //lunches factor
            var lunchesWkly = Convert.ToDecimal(pa.Find(p => p.Key == "cooktop.luncheswkly").OptionValue);
            //dinners factor
            var dinnersWkly = Convert.ToDecimal(pa.Find(p => p.Key == "cooktop.dinnerswkly").OptionValue);

            //HotMealFactor
            var hotMealFactor = 1 - (14 - (0.5m*breakfastsWkly + lunchesWkly + 2.0m*dinnersWkly))/14;

            var fueltype = pa.Find(p => p.Key == "cooktop.fuel").OptionKey;
            switch (fueltype)
            {
                case "cooktop.fuel.electric":
                    energyUse = config.GetCalculationFactor<decimal>("cooktop.energyuse_elec");
                    break;
                case "cooktop.fuel.gas":
                {
                    energyUse = config.GetCalculationFactor<decimal>("cooktop.energyuse_gas");
                    isPilot = Convert.ToBoolean(Convert.ToInt32(pa.Find(p => p.Key == "cooktop.pilotlight").OptionValue));
                    if (isPilot)
                    {
                        pilotAdd = config.GetCalculationFactor<decimal>("cooktop.pilotuse");
                        energyUse = energyUse + pilotAdd;
                    }
                    break;
                }
                case "cooktop.fuel.propane":
                {
                    energyUse = config.GetCalculationFactor<decimal>("cooktop.energyuse_prop");
                    isPilot = Convert.ToBoolean(Convert.ToInt32(pa.Find(p => p.Key == "cooktop.pilotlight").OptionValue));
                    if (isPilot)
                    {
                        pilotAdd = config.GetCalculationFactor<decimal>("cooktop.pilotuse");
                        energyUse = energyUse + pilotAdd;
                    }
                    break;
                }
                default:
                    energyUse = 0.0m;
                    break;
            }


            //BTU
            var btus = 1000000.0m*energyUse*microwavefactor*peopleFactor*hotMealFactor*usageFactor;

            switch (fueltype)
            {
                case "cooktop.fuel.electric":
                    output = btus/3413m*count;
                    _appliance.EnergyUses.Add(new EnergyUse(true)
                    {
                        Commodity = Enums.CommodityType.Electric,
                        Quantity = output,
                        UnitOfMeasure = Enums.UnitOfMeasureType.kWh
                    });
                    break;
                case "cooktop.fuel.gas":
                    output = btus/100000m*count;
                    _appliance.EnergyUses.Add(new EnergyUse(true)
                    {
                        Commodity = Enums.CommodityType.Gas,
                        Quantity = output,
                        UnitOfMeasure = Enums.UnitOfMeasureType.Therms
                    });
                    break;
                case "cooktop.fuel.propane":
                    output = btus/92000m*count;
                    _appliance.EnergyUses.Add(new EnergyUse(true)
                    {
                        Commodity = Enums.CommodityType.Propane,
                        Quantity = output,
                        UnitOfMeasure = Enums.UnitOfMeasureType.Gal
                    });
                    break;
                default:
                    output = 0.0m;
                    _appliance.EnergyUses.Add(new EnergyUse(true)
                    {
                        Commodity = Enums.CommodityType.Electric,
                        Quantity = output,
                        UnitOfMeasure = Enums.UnitOfMeasureType.kWh
                    });
                    break;
            }
        }

        /// <summary>
        /// Model for a dryer
        /// </summary>
        private void Model_dryer()
        {
            decimal outputkwh;
            decimal outputgas;
            decimal btus;
            decimal tempCount;
            var overDryFactor = 0;
            var fullLoadFactor = 0;

            var pa = _input.ProfileAttributes;
            var config = _input.Configuration;

            var age = ResetDefaultYear(pa, "dryer.year");

            var count = Convert.ToDecimal(pa.Find(p => p.Key == "dryer.count").OptionValue);
            var runFullLoad = Convert.ToBoolean(Convert.ToInt32(pa.Find(p => p.Key == "dryer.runfullload").OptionValue));
            var autoShutOff =
                Convert.ToBoolean(Convert.ToInt32(pa.Find(p => p.Key == "dryer.autoshutoffuse").OptionValue));
            var turnOffWhenDone =
                Convert.ToBoolean(Convert.ToInt32(pa.Find(p => p.Key == "dryer.turnoffwhendone").OptionValue));
            var loadsWkly = Convert.ToDecimal(pa.Find(p => p.Key == "dryer.loadswkly").OptionValue);
            var motorEnergyFactor = config.GetCalculationFactor<decimal>("dryer.motorenergy");

            if (autoShutOff || turnOffWhenDone)
            {
                overDryFactor = 1;
            }

            if (runFullLoad)
            {
                fullLoadFactor = 1;
            }

            var sTemp = "dryer.realsize_";

            var sizetype = pa.Find(p => p.Key == "dryer.size").OptionKey;
            switch (sizetype)
            {
                case "dryer.size.standard":
                    sTemp = sTemp + "std";
                    break;
                case "dryer.size.compact":
                    sTemp = sTemp + "comp";
                    break;
                case "dryer.size.extralarge":
                    sTemp = sTemp + "xl";
                    break;
                default:
                    sTemp = sTemp + "std";
                    break;
            }

            var realsize = config.GetCalculationFactor<decimal>(sTemp);

            var fueltype = pa.Find(p => p.Key == "dryer.fuel").OptionKey;


            sTemp = "dryer.energyfactor_";

            switch (fueltype)
            {
                case "dryer.fuel.electric":
                    sTemp = sTemp + "elec_";
                    break;
                case "dryer.fuel.gas":
                case "dryer.fuel.propane":
                    sTemp = sTemp + "gas_";
                    break;
                default:
                    sTemp = sTemp + "gas_";
                    break;
            }

            if (age > 15)
            {
                sTemp = sTemp + "gt15";
            }
            else if (age >= 10)
            {
                sTemp = sTemp + "10to15";
            }
            else if (age >= 5)
            {
                sTemp = sTemp + "5to9";
            }
            else if (age >= 1)
            {
                sTemp = sTemp + "1to5";
            }
            else
            {
                sTemp = sTemp + "lt1";
            }

            var energyFactor = config.GetCalculationFactor<decimal>(sTemp);


            if (count > 1)
            {
                tempCount = (1 + ((count - 1)*0.05m));
            }
            else
            {
                tempCount = count;
            }

            if (energyFactor > 0)
            {
                btus = 3413m*(realsize/energyFactor - motorEnergyFactor)*loadsWkly*52*(1.08m - 0.15m*overDryFactor)*
                       (1.05m - 0.05m*fullLoadFactor)*tempCount;
            }
            else
            {
                btus = 0.0m;
            }

            var motorkwh = motorEnergyFactor*loadsWkly*52*(1.08m - 0.15m*overDryFactor)*(1.05m - (0.05m*fullLoadFactor))*
                           tempCount;

            switch (fueltype)
            {
                case "dryer.fuel.electric":
                    outputkwh = motorkwh + btus/3413m;
                    _appliance.EnergyUses.Add(new EnergyUse(true)
                    {
                        Commodity = Enums.CommodityType.Electric,
                        Quantity = outputkwh,
                        UnitOfMeasure = Enums.UnitOfMeasureType.kWh
                    });
                    break;
                case "dryer.fuel.gas":
                    outputgas = 0.92m*btus/100000m;
                    _appliance.EnergyUses.Add(new EnergyUse(true)
                    {
                        Commodity = Enums.CommodityType.Gas,
                        Quantity = outputgas,
                        UnitOfMeasure = Enums.UnitOfMeasureType.Therms
                    });
                    _appliance.EnergyUses.Add(new EnergyUse(true)
                    {
                        Commodity = Enums.CommodityType.Electric,
                        Quantity = motorkwh,
                        UnitOfMeasure = Enums.UnitOfMeasureType.kWh
                    });
                    break;
                case "dryer.fuel.propane":
                    outputgas = btus/92000m;
                    _appliance.EnergyUses.Add(new EnergyUse(true)
                    {
                        Commodity = Enums.CommodityType.Propane,
                        Quantity = outputgas,
                        UnitOfMeasure = Enums.UnitOfMeasureType.Gal
                    });
                    _appliance.EnergyUses.Add(new EnergyUse(true)
                    {
                        Commodity = Enums.CommodityType.Electric,
                        Quantity = motorkwh,
                        UnitOfMeasure = Enums.UnitOfMeasureType.kWh
                    });
                    break;
                default:
                    outputkwh = 0.08m*btus/3413m;
                    _appliance.EnergyUses.Add(new EnergyUse(true)
                    {
                        Commodity = Enums.CommodityType.Electric,
                        Quantity = outputkwh,
                        UnitOfMeasure = Enums.UnitOfMeasureType.kWh
                    });
                    break;
            }

        }

        /// <summary>
        /// Model for Clothes Washer
        /// </summary>
        private void Model_clotheswasher()
        {
            decimal outputkWh;
            decimal outputHotWater;

            var pa = _input.ProfileAttributes;
            var config = _input.Configuration;

            var age = ResetDefaultYear(pa, "clotheswasher.yearinstalled");

            var count = Convert.ToDecimal(pa.Find(p => p.Key == "clotheswasher.count").OptionValue);
            var motorEnergyFactor = config.GetCalculationFactor<decimal>("clotheswasher.getmotorkwh");
            var sizeFactor = Convert.ToDecimal(pa.Find(p => p.Key == "clotheswasher.size").OptionValue);
            var loadsUsedFactor = Convert.ToDecimal(pa.Find(p => p.Key == "clotheswasher.loadused").OptionValue);
            var loadsWkly = Convert.ToDecimal(pa.Find(p => p.Key == "clotheswasher.loadswkly").OptionValue);
            var matchWaterLoad =
                Convert.ToBoolean(Convert.ToInt32(pa.Find(p => p.Key == "clotheswasher.matchwaterload").OptionValue));

            //water volume
            var sTemp = "clotheswasher.watervolume_";

            var styletype = pa.Find(p => p.Key == "clotheswasher.style").OptionKey;
            switch (styletype)
            {
                case "clotheswasher.style.toploading":
                    sTemp = sTemp + "vert_";
                    break;
                case "clotheswasher.style.frontloading":
                    sTemp = sTemp + "hori_";
                    break;
                default:
                    sTemp = sTemp + "vert_";
                    break;
            }

            if (age > 15)
            {
                sTemp = sTemp + "gt15";
            }
            else if (age >= 10)
            {
                sTemp = sTemp + "10to15";
            }
            else if (age >= 5)
            {
                sTemp = sTemp + "5to9";
            }
            else if (age >= 1)
            {
                sTemp = sTemp + "1to5";
            }
            else
            {
                sTemp = sTemp + "lt1";
            }

            var waterVolume = config.GetCalculationFactor<decimal>(sTemp);

            //efficiency
            sTemp = "clotheswasher.efficiencyfactor_";
            switch (styletype)
            {
                case "clotheswasher.style.toploading":
                    sTemp = sTemp + "vert_";
                    break;
                case "clotheswasher.style.frontloading":
                    sTemp = sTemp + "hori_";
                    break;
                default:
                    sTemp = sTemp + "vert_";
                    break;
            }

            var efficiencytype = pa.Find(p => p.Key == "clotheswasher.efficiency").OptionKey;
            switch (efficiencytype)
            {
                case "clotheswasher.efficiency.standard":
                    sTemp = sTemp + "std";
                    break;
                case "clotheswasher.efficiency.high":
                    sTemp = sTemp + "high";
                    break;
                default:
                    sTemp = sTemp + "std";
                    break;
            }

            var effFactor = config.GetCalculationFactor<decimal>(sTemp);

            //Water Volume Per Load
            var waterVolumePerLoad = waterVolume*sizeFactor*loadsUsedFactor*effFactor;

            //Hot Water Use 
            var hotWaterUseForWash =
                Convert.ToDecimal(pa.Find(p => p.Key == "clotheswasher.washtemperature").OptionValue);
            var hotWaterUseForRinse =
                Convert.ToDecimal(pa.Find(p => p.Key == "clotheswasher.rinsetemperature").OptionValue);
            var hotWaterUse = (hotWaterUseForWash + hotWaterUseForRinse)/2.0m;

            //Water Use - note that hot water calculation is not included here
            var outputWater = waterVolumePerLoad*52.0m*loadsWkly;

            // final output for kwh
            var matchFactor = matchWaterLoad ? 1.0m : 0.0m;

            if (count >= 1)
            {
                outputWater = outputWater*(1 + (count - 1)*0.05m*(1.03m - 0.06m*matchFactor));
                outputHotWater = outputWater*hotWaterUse;
                outputkWh = loadsWkly*52*motorEnergyFactor*(1 + (count - 1)*0.05m);
            }
            else
            {
                outputWater = outputWater*count*(1.03m - (0.06m*matchFactor));
                outputHotWater = outputWater*hotWaterUse;
                outputkWh = loadsWkly*52*motorEnergyFactor*count;
            }

            _appliance.EnergyUses.Add(new EnergyUse(true)
            {
                Commodity = Enums.CommodityType.Water,
                Quantity = outputWater,
                UnitOfMeasure = Enums.UnitOfMeasureType.Gal
            });
            _appliance.EnergyUses.Add(new EnergyUse(true)
            {
                Commodity = Enums.CommodityType.HotWater,
                Quantity = outputHotWater,
                UnitOfMeasure = Enums.UnitOfMeasureType.Gal
            });
            _appliance.EnergyUses.Add(new EnergyUse(true)
            {
                Commodity = Enums.CommodityType.Electric,
                Quantity = outputkWh,
                UnitOfMeasure = Enums.UnitOfMeasureType.kWh
            });

        }

        /// <summary>
        /// Model for Dishwasher
        /// </summary>
        private void Model_dishwasher()
        {
            decimal outputkWh;
            decimal usageLightLoads;
            decimal usageRegularLoads;
            decimal usageHeavyLoads;
            decimal hwVolumeLight;
            decimal hwVolumeRegular;
            decimal hwVolumeHeavy;

            var pa = _input.ProfileAttributes;
            var config = _input.Configuration;

            var age = ResetDefaultYear(pa, "dishwasher.year");

            var count = Convert.ToDecimal(pa.Find(p => p.Key == "dishwasher.count").OptionValue);
            var washCycleLightLoads = config.GetCalculationFactor<decimal>("dishwasher.washcyclelightloads");
            var washCycleRegularLoads = config.GetCalculationFactor<decimal>("dishwasher.washcycleregularloads");
            var washCycleHeavyLoads = config.GetCalculationFactor<decimal>("dishwasher.washcycleheavyloads");
            var washCycleOtherLoads = config.GetCalculationFactor<decimal>("dishwasher.washcycleotherloads");
            var gallonAdjustmentFactor = config.GetCalculationFactor<decimal>("dishwasher.gallonadjustmentfactor");
            var pumpkw = config.GetCalculationFactor<decimal>("dishwasher.pumpkw");
            var fandrykw = config.GetCalculationFactor<decimal>("dishwasher.fandrykw");
            var isBoosterheater =
                Convert.ToBoolean(Convert.ToInt32(pa.Find(p => p.Key == "dishwasher.boosterheater").OptionValue));
            var isEnergySaving =
                Convert.ToBoolean(Convert.ToInt32(pa.Find(p => p.Key == "dishwasher.energysaving").OptionValue));
            var matchDirtLevel =
                Convert.ToBoolean(Convert.ToInt32(pa.Find(p => p.Key == "dishwasher.matchdirtlevel").OptionValue));
            var loadsWkly = Convert.ToDecimal(pa.Find(p => p.Key == "dishwasher.loadswkly").OptionValue);

            var ncount = count >= 1 ? count : 1.0m;

            var washcycletype = pa.Find(p => p.Key == "dishwasher.washcycle").OptionKey;
            switch (washcycletype)
            {
                case "dishwasher.washcycle.light":
                    usageLightLoads = washCycleLightLoads;
                    usageRegularLoads = washCycleOtherLoads;
                    usageHeavyLoads = washCycleOtherLoads;
                    break;
                case "dishwasher.washcycle.regular":
                    usageLightLoads = washCycleOtherLoads;
                    usageRegularLoads = washCycleRegularLoads;
                    usageHeavyLoads = washCycleOtherLoads;
                    break;
                case "dishwasher.washcycle.heavy":
                    usageLightLoads = washCycleOtherLoads;
                    usageRegularLoads = washCycleOtherLoads;
                    usageHeavyLoads = washCycleHeavyLoads;
                    break;
                default:
                    usageLightLoads = washCycleOtherLoads;
                    usageRegularLoads = washCycleRegularLoads;
                    usageHeavyLoads = washCycleOtherLoads;
                    break;
            }

            var totalLoads = usageLightLoads + usageRegularLoads + usageHeavyLoads;

            usageLightLoads = usageLightLoads/totalLoads;
            usageRegularLoads = usageRegularLoads/totalLoads;
            usageHeavyLoads = usageHeavyLoads/totalLoads;

            // motor usage factor
            var sTemp = "dishwasher.agemotorfactor_";

            if (age > 15)
            {
                sTemp = sTemp + "gt15";
            }
            else if (age >= 10)
            {
                sTemp = sTemp + "10to15";
            }
            else if (age >= 5)
            {
                sTemp = sTemp + "5to10";
            }
            else if (age >= 1)
            {
                sTemp = sTemp + "1to5";
            }
            else
            {
                sTemp = sTemp + "lt1";
            }

            var motorUsageFactor = config.GetCalculationFactor<decimal>(sTemp);

            //size to get other factors;  these are not configurable; replace with a fit curve or simple formula in future
            var sizetype = pa.Find(p => p.Key == "dishwasher.size").OptionKey;
            switch (sizetype)
            {
                case "dishwasher.size.compact":
                    if (age > 15)
                    {
                        hwVolumeLight = 8.0m;
                        hwVolumeRegular = 10.0m;
                        hwVolumeHeavy = 12.0m;
                    }
                    else if (age >= 10)
                    {
                        hwVolumeLight = 6.5m;
                        hwVolumeRegular = 8.0m;
                        hwVolumeHeavy = 10.0m;
                    }
                    else if (age >= 5)
                    {
                        hwVolumeLight = 3.5m;
                        hwVolumeRegular = 4.5m;
                        hwVolumeHeavy = 5.5m;
                    }
                    else if (age >= 1)
                    {
                        hwVolumeLight = 3.5m;
                        hwVolumeRegular = 4.5m;
                        hwVolumeHeavy = 5.5m;
                    }
                    else
                    {
                        hwVolumeLight = 2.5m;
                        hwVolumeRegular = 3.5m;
                        hwVolumeHeavy = 4.5m;
                    }
                    break;
                default:
                    if (age > 15)
                    {
                        hwVolumeLight = 10.0m;
                        hwVolumeRegular = 13.0m;
                        hwVolumeHeavy = 16.0m;
                    }
                    else if (age >= 10)
                    {
                        hwVolumeLight = 8.0m;
                        hwVolumeRegular = 10.5m;
                        hwVolumeHeavy = 13.0m;
                    }
                    else if (age >= 5)
                    {
                        hwVolumeLight = 5.0m;
                        hwVolumeRegular = 6.5m;
                        hwVolumeHeavy = 8.0m;
                    }
                    else if (age >= 1)
                    {
                        hwVolumeLight = 5.0m;
                        hwVolumeRegular = 6.5m;
                        hwVolumeHeavy = 8.0m;
                    }
                    else
                    {
                        hwVolumeLight = 3.5m;
                        hwVolumeRegular = 5.0m;
                        hwVolumeHeavy = 6.5m;
                    }
                    break;
            }

            var lengthOfCycle = 0.33m*usageLightLoads + 0.5m*usageRegularLoads + 0.66m*usageHeavyLoads;

            var boosterkw = isBoosterheater ? config.GetCalculationFactor<decimal>("dishwasher.boosterkw") : 0.0m;

            if (isEnergySaving == false)
            {
                outputkWh = 52*loadsWkly*motorUsageFactor*(((pumpkw + boosterkw)*lengthOfCycle) + fandrykw)*
                            (1 + (0.05m*ncount));
            }
            else
            {
                outputkWh = 52*loadsWkly*(pumpkw + boosterkw)*lengthOfCycle*motorUsageFactor*(1 + (0.05m*ncount));
            }

            var dirtLevelFactor = matchDirtLevel == false
                ? 1.0m
                : config.GetCalculationFactor<decimal>("dishwasher.dirtlevelfactor");

            var hotWaterOutput = 52*loadsWkly*
                                 (hwVolumeLight*usageLightLoads + hwVolumeRegular*usageRegularLoads +
                                  hwVolumeHeavy*usageHeavyLoads)*dirtLevelFactor*(1 + 0.05m*(ncount - 1.0m));
            hotWaterOutput = hotWaterOutput*gallonAdjustmentFactor;
            var waterOutput = hotWaterOutput;

            if (count < 1)
            {
                outputkWh = outputkWh*count;
                hotWaterOutput = hotWaterOutput*count;
                waterOutput = waterOutput*count;
            }

            _appliance.EnergyUses.Add(new EnergyUse(true)
            {
                Commodity = Enums.CommodityType.Water,
                Quantity = waterOutput,
                UnitOfMeasure = Enums.UnitOfMeasureType.Gal
            });
            _appliance.EnergyUses.Add(new EnergyUse(true)
            {
                Commodity = Enums.CommodityType.HotWater,
                Quantity = hotWaterOutput,
                UnitOfMeasure = Enums.UnitOfMeasureType.Gal
            });
            _appliance.EnergyUses.Add(new EnergyUse(true)
            {
                Commodity = Enums.CommodityType.Electric,
                Quantity = outputkWh,
                UnitOfMeasure = Enums.UnitOfMeasureType.kWh
            });

        }

        /// <summary>
        /// Model for Shower
        /// </summary>
        private void Model_shower()
        {
            var pa = _input.ProfileAttributes;
            var config = _input.Configuration;

            var count = Convert.ToDecimal(pa.Find(p => p.Key == "shower.count").OptionValue);
            var showerLeakRate = Convert.ToDecimal(pa.Find(p => p.Key == "shower.showerleakrate").OptionValue);
            var showerHeadFlow = Convert.ToDecimal(pa.Find(p => p.Key == "shower.showerheadflow").OptionValue);
            var leakingShowerQty = Convert.ToDecimal(pa.Find(p => p.Key == "shower.leakingshowerqty").OptionValue);
            var bathsWeekly = Convert.ToDecimal(pa.Find(p => p.Key == "shower.bathswkly").OptionValue);
            var showersWeekly = Convert.ToDecimal(pa.Find(p => p.Key == "shower.showerswkly").OptionValue);
            var showerLength = Convert.ToDecimal(pa.Find(p => p.Key == "shower.showerlength").OptionValue);
            var bathGal = config.GetCalculationFactor<decimal>("house.bath_gal");
            var hotWaterShowerFactor = config.GetCalculationFactor<decimal>("house.shower_water_hot");

            var leakingShowerVolume = 365m*showerLeakRate*leakingShowerQty;
            if (count == 0)
            {
                leakingShowerVolume = 0.0m;
            }

            var showerWater = hotWaterShowerFactor*52*(showerLength*showersWeekly*showerHeadFlow + bathGal*bathsWeekly);
            var outputWater = showerWater + leakingShowerVolume;

            _appliance.EnergyUses.Add(new EnergyUse(true)
            {
                Commodity = Enums.CommodityType.Water,
                Quantity = outputWater,
                UnitOfMeasure = Enums.UnitOfMeasureType.Gal
            });
            _appliance.EnergyUses.Add(new EnergyUse(true)
            {
                Commodity = Enums.CommodityType.OtherWater,
                Quantity = showerWater,
                UnitOfMeasure = Enums.UnitOfMeasureType.Gal
            });

        }

        /// <summary>
        /// Model for Sink
        /// </summary>
        private void Model_sink()
        {
            var pa = _input.ProfileAttributes;
            var config = _input.Configuration;

            var count = Convert.ToDecimal(pa.Find(p => p.Key == "sink.count").OptionValue);
            var faucetLeakRate = Convert.ToDecimal(pa.Find(p => p.Key == "sink.faucetleakrate").OptionValue);
            var sinkFlow = Convert.ToDecimal(pa.Find(p => p.Key == "sink.sinkflow").OptionValue);
            var leakingFaucetsQty = Convert.ToDecimal(pa.Find(p => p.Key == "sink.leakingfaucetsqty").OptionValue);
            var breakfastsWeekly = Convert.ToDecimal(pa.Find(p => p.Key == "cooktop.breakfastswkly").OptionValue);
            var lunchesWeekly = Convert.ToDecimal(pa.Find(p => p.Key == "cooktop.luncheswkly").OptionValue);
            var dinnersWeekly = Convert.ToDecimal(pa.Find(p => p.Key == "cooktop.dinnerswkly").OptionValue);
            var dishwasherCount = Convert.ToDecimal(pa.Find(p => p.Key == "dishwasher.count").OptionValue);
            var faucetMinPerDay = config.GetCalculationFactor<decimal>("house.faucetminperday");
            var people = Convert.ToDecimal(pa.Find(p => p.Key == "house.people").OptionValue);

            if (count == 0)
            {
                leakingFaucetsQty = 0.0m;
            }

            var sinkVolumePerMeal = dishwasherCount > 0
                ? 1m
                : config.GetCalculationFactor<decimal>("sink.volumepermeal");

            var mealCount = breakfastsWeekly + lunchesWeekly + dinnersWeekly;
            var cookingWater = people*sinkFlow*faucetMinPerDay*365 + sinkVolumePerMeal*mealCount*52.0m;
            var leakingFaucetVolume = 365*faucetLeakRate*leakingFaucetsQty;

            var outputWater = cookingWater + leakingFaucetVolume;

            _appliance.EnergyUses.Add(new EnergyUse(true)
            {
                Commodity = Enums.CommodityType.Water,
                Quantity = outputWater,
                UnitOfMeasure = Enums.UnitOfMeasureType.Gal
            });
            _appliance.EnergyUses.Add(new EnergyUse(true)
            {
                Commodity = Enums.CommodityType.OtherWater,
                Quantity = cookingWater,
                UnitOfMeasure = Enums.UnitOfMeasureType.Gal
            });

        }

        /// <summary>
        /// Model for outside water
        /// </summary>
        private void Model_outsidewater()
        {
            var outputWater = 0.0m;
            var lawnWater = 0.0m;
            var gardenWater = 0.0m;

            var pa = _input.ProfileAttributes;
            var config = _input.Configuration;

            var count = Convert.ToDecimal(pa.Find(p => p.Key == "outsidewater.count").OptionValue);

            if (count > 0)
            {
                var lawnWinterMinutes =
                    Convert.ToDecimal(pa.Find(p => p.Key == "outsidewater.lawnwinterminutes").OptionValue);
                var lawnSummerMinutes =
                    Convert.ToDecimal(pa.Find(p => p.Key == "outsidewater.lawnsummerminutes").OptionValue);
                var lawnFallMinutes =
                    Convert.ToDecimal(pa.Find(p => p.Key == "outsidewater.lawnfallminutes").OptionValue);
                var gardenWinterMinutes =
                    Convert.ToDecimal(pa.Find(p => p.Key == "outsidewater.gardenwinterminutes").OptionValue);
                var gardenSummerMinutes =
                    Convert.ToDecimal(pa.Find(p => p.Key == "outsidewater.gardensummerminutes").OptionValue);
                var gardenFallMinutes =
                    Convert.ToDecimal(pa.Find(p => p.Key == "outsidewater.gardenfallminutes").OptionValue);
                var lawnWinterFreq = Convert.ToDecimal(pa.Find(p => p.Key == "outsidewater.lawnwinterfrqcy").OptionValue);
                var lawnSummerFreq = Convert.ToDecimal(pa.Find(p => p.Key == "outsidewater.lawnsummerfrqcy").OptionValue);
                var lawnFallFreq = Convert.ToDecimal(pa.Find(p => p.Key == "outsidewater.lawnfallfrqcy").OptionValue);
                var gardenWinterFreq =
                    Convert.ToDecimal(pa.Find(p => p.Key == "outsidewater.gardenwinterfrqcy").OptionValue);
                var gardenSummerFreq =
                    Convert.ToDecimal(pa.Find(p => p.Key == "outsidewater.gardensummerfrqcy").OptionValue);
                var gardenFallFreq = Convert.ToDecimal(pa.Find(p => p.Key == "outsidewater.gardenfallfrqcy").OptionValue);
                var lawnArea = Convert.ToDecimal(pa.Find(p => p.Key == "outsidewater.lawnarea").OptionValue);
                var gardenArea = Convert.ToDecimal(pa.Find(p => p.Key == "outsidewater.gardenarea").OptionValue);
                var isLawnWatered =
                    Convert.ToBoolean(Convert.ToInt32(pa.Find(p => p.Key == "outsidewater.lawnwatered").OptionValue));
                var isGardenWatered =
                    Convert.ToBoolean(Convert.ToInt32(pa.Find(p => p.Key == "outsidewater.gardenwatered").OptionValue));
                var isLowWaterPlants =
                    Convert.ToBoolean(Convert.ToInt32(pa.Find(p => p.Key == "outsidewater.lowwaterplants").OptionValue));
                var lowWaterFactor = config.GetCalculationFactor<decimal>("outsidewater.lowwaterfactor");

                var gardenWinterHours = gardenWinterFreq*gardenWinterMinutes/60;
                var gardenSummerHours = gardenSummerFreq*gardenSummerMinutes/60;
                var gardenFallHours = gardenFallFreq*gardenFallMinutes/60;
                var lawnWinterHours = lawnWinterFreq*lawnWinterMinutes/60;
                var lawnSummerHours = lawnSummerFreq*lawnSummerMinutes/60;
                var lawnFallHours = lawnFallFreq*lawnFallMinutes/60;

                var sTemp = "outsidewater.sprinklertype.";

                var sprinklertype = pa.Find(p => p.Key == "outsidewater.lawnsprinkler").OptionKey;
                switch (sprinklertype)
                {
                    case "outsidewater.lawnsprinkler.hose":
                        sTemp = sTemp + "hose";
                        break;
                    case "outsidewater.lawnsprinkler.house_end_sprinkler":
                        sTemp = sTemp + "house_end_sprinkler";
                        break;
                    case "outsidewater.lawnsprinkler.soaker_hose":
                        sTemp = sTemp + "soaker_hose";
                        break;
                    case "outsidewater.lawnsprinkler.drip_watering_system":
                        sTemp = sTemp + "drip_watering_system";
                        break;
                    case "outsidewater.lawnsprinkler.steam_rotor":
                        sTemp = sTemp + "steam_rotor";
                        break;
                    case "outsidewater.lawnsprinkler.impact":
                        sTemp = sTemp + "impact";
                        break;
                    case "outsidewater.lawnsprinkler.gear_rotor":
                        sTemp = sTemp + "gear_rotor";
                        break;
                    case "outsidewater.lawnsprinkler.fixed_spray":
                        sTemp = sTemp + "fixed_spray";
                        break;
                    default:
                        sTemp = sTemp + "hose";
                        break;
                }

                var wateringRate = config.GetCalculationFactor<decimal>(sTemp);

                if (isLawnWatered)
                {
                    lawnWater = lawnArea*wateringRate*13.0m*(lawnWinterHours + lawnSummerHours + 2.0m*lawnFallHours);
                }

                sTemp = "outsidewater.sprinklertype.";

                sprinklertype = pa.Find(p => p.Key == "outsidewater.gardensprinkler").OptionKey;
                switch (sprinklertype)
                {
                    case "outsidewater.gardensprinkler.hose":
                        sTemp = sTemp + "hose";
                        break;
                    case "outsidewater.gardensprinkler.house_end_sprinkler":
                        sTemp = sTemp + "house_end_sprinkler";
                        break;
                    case "outsidewater.gardensprinkler.soaker_hose":
                        sTemp = sTemp + "soaker_hose";
                        break;
                    case "outsidewater.gardensprinkler.drip_watering_system":
                        sTemp = sTemp + "drip_watering_system";
                        break;
                    case "outsidewater.gardensprinkler.steam_rotor":
                        sTemp = sTemp + "steam_rotor";
                        break;
                    case "outsidewater.gardensprinkler.impact":
                        sTemp = sTemp + "impact";
                        break;
                    case "outsidewater.gardensprinkler.gear_rotor":
                        sTemp = sTemp + "gear_rotor";
                        break;
                    case "outsidewater.gardensprinkler.fixed_spray":
                        sTemp = sTemp + "fixed_spray";
                        break;
                    default:
                        sTemp = sTemp + "hose";
                        break;
                }

                wateringRate = config.GetCalculationFactor<decimal>(sTemp);

                if (isGardenWatered)
                {
                    gardenWater = gardenArea*wateringRate*
                                  (_input.WeatherInfo.WeeksWinter*gardenWinterHours +
                                   _input.WeatherInfo.WeeksSummer*gardenSummerHours +
                                   _input.WeatherInfo.WeeksFall*gardenFallHours +
                                   _input.WeatherInfo.WeeksSpring*gardenFallHours)*60m;
                    if (isLowWaterPlants) gardenWater = gardenWater*lowWaterFactor;
                }

                outputWater = lawnWater + gardenWater;

            }

            _appliance.EnergyUses.Add(new EnergyUse(true)
            {
                Commodity = Enums.CommodityType.Water,
                Quantity = outputWater,
                UnitOfMeasure = Enums.UnitOfMeasureType.Gal
            });

        }


        /// <summary>
        /// Model Waterheater
        /// </summary>
        private void Model_waterheater()
        {
            // Setup variables
            var btu = 0.0m;
            var showerWater = 0.0m;
            var sinkWater = 0.0m;
            var clothesWasherWater = 0.0m;
            var dishWasherWater = 0.0m;
            var highEffAdjustment = 0.0m;
            var maintFactor = 0.0m;
            var heaterEffFactor = 1.0m;
            var pilotFuel = 0.0m;
            var damperLoss = 0.0m;
            decimal rFactor;

            // Get passed in attributes.
            var pa = _input.ProfileAttributes;
            var config = _input.Configuration;

            // Determine details about the waterheater
            var count = Convert.ToDecimal(pa.Find(p => p.Key == "waterheater.count").OptionValue);

            var age = ResetDefaultYearTwoWayCheck(pa, "waterheater.year", "waterheater.yearrange");
            var fueltype = pa.Find(p => p.Key == "waterheater.fuel").OptionKey;
            var style = pa.Find(p => p.Key == "waterheater.style").OptionKey;
            var servicefrequencytype = pa.Find(p => p.Key == "waterheater.servicefrequency").OptionKey;

            // Water Heater characteristics
            var isHighEfficiency =
                Convert.ToBoolean(Convert.ToInt32(pa.Find(p => p.Key == "waterheater.highefficiency").OptionValue));
            var isPilotLight =
                Convert.ToBoolean(Convert.ToInt32(pa.Find(p => p.Key == "waterheater.pilotlight").OptionValue));
            var isFlueDamper =
                Convert.ToBoolean(Convert.ToInt32(pa.Find(p => p.Key == "waterheater.fluedamper").OptionValue));
            var isTankInsulation =
                Convert.ToBoolean(Convert.ToInt32(pa.Find(p => p.Key == "waterheater.tankinsulation").OptionValue));
            var cookHotWaterPct = config.GetCalculationFactor<decimal>("waterheater.cookwaterhot");
            var isTimer = Convert.ToBoolean(Convert.ToInt32(pa.Find(p => p.Key == "waterheater.timer").OptionValue));
            var isHeatTraps =
                Convert.ToBoolean(Convert.ToInt32(pa.Find(p => p.Key == "waterheater.heattraps").OptionValue));
            var isHeatRecovery =
                Convert.ToBoolean(Convert.ToInt32(pa.Find(p => p.Key == "waterheater.heatrecovery").OptionValue));
            var isSolarActive =
                Convert.ToBoolean(Convert.ToInt32(pa.Find(p => p.Key == "waterheater.solaractive").OptionValue));
            var isPipesInsulation =
                Convert.ToBoolean(Convert.ToInt32(pa.Find(p => p.Key == "waterheater.pipesinsulation").OptionValue));
            var tankSize = Convert.ToDecimal(pa.Find(p => p.Key == "waterheater.tanksize").OptionValue);
            var temperature = Convert.ToDecimal(pa.Find(p => p.Key == "waterheater.temperature").OptionValue);
            var groundwatertemp = Convert.ToDecimal(_input.WeatherInfo.GroundWaterTemp);
            var isSolarHeater =
                Convert.ToBoolean(Convert.ToInt32(pa.Find(p => p.Key == "waterheater.solarheater").OptionValue));

            // Heat pump water heater configuration
            var waterDensity = config.GetCalculationFactor<decimal>("waterheater.waterdensity");
            var waterSpecificHeat = config.GetCalculationFactor<decimal>("waterheater.waterspecificheat");
            var reElectricResistance =
                config.GetCalculationFactor<decimal>("waterheater.recoveryefficiency.electricresistance");
            var reHeatPump = config.GetCalculationFactor<decimal>("waterheater.recoveryefficiency.heatpump");
            var ponHeatPump = config.GetCalculationFactor<decimal>("waterheater.ratedinputpower.heatpump");
            var ponElectricResistance =
                config.GetCalculationFactor<decimal>("waterheater.ratedinputpower.electricresistance");
            var paFactor1 = config.GetCalculationFactor<decimal>("waterheater.performanceadjfac1");
            var paFactor2 = config.GetCalculationFactor<decimal>("waterheater.performanceadjfac2");
            var paFactor3 = config.GetCalculationFactor<decimal>("waterheater.performanceadjfac3");
            var qOut = config.GetCalculationFactor<decimal>("waterheater.waterenergycontentout");

            //must run model for several individual appliances to get *water* output to be used for this water heater;
            //each appliance must be able to be executed without dependencies on other appliances, thus these outputs being used here
            var evalShower = new ApplianceEvaluator(new Appliance {Name = "Shower"}, _input);
            evalShower.Model_shower();
            if (evalShower.Appliance.EnergyUses != null &&
                evalShower.Appliance.EnergyUses.Exists(p => p.Commodity == Enums.CommodityType.OtherWater)) //OtherWater
            {
                showerWater =
                    evalShower.Appliance.EnergyUses.Find(p => p.Commodity == Enums.CommodityType.OtherWater).Quantity;
            }

            var evalSink = new ApplianceEvaluator(new Appliance {Name = "Sink"}, _input);
            evalSink.Model_sink();
            if (evalSink.Appliance.EnergyUses != null &&
                evalSink.Appliance.EnergyUses.Exists(p => p.Commodity == Enums.CommodityType.OtherWater)) //OtherWater
            {
                sinkWater =
                    evalSink.Appliance.EnergyUses.Find(p => p.Commodity == Enums.CommodityType.OtherWater).Quantity;
            }

            var evalClothesWasher = new ApplianceEvaluator(new Appliance {Name = "ClothesWasher"}, _input);
            evalClothesWasher.Model_clotheswasher();
            if (evalClothesWasher.Appliance.EnergyUses != null &&
                evalClothesWasher.Appliance.EnergyUses.Exists(p => p.Commodity == Enums.CommodityType.HotWater))
                //HotWater
            {
                clothesWasherWater =
                    evalClothesWasher.Appliance.EnergyUses.Find(p => p.Commodity == Enums.CommodityType.HotWater)
                        .Quantity;
            }

            var evalDishwasher = new ApplianceEvaluator(new Appliance {Name = "DishWasher"}, _input);
            evalDishwasher.Model_dishwasher();
            if (evalDishwasher.Appliance.EnergyUses != null &&
                evalDishwasher.Appliance.EnergyUses.Exists(p => p.Commodity == Enums.CommodityType.HotWater)) //HotWater
            {
                dishWasherWater =
                    evalDishwasher.Appliance.EnergyUses.Find(p => p.Commodity == Enums.CommodityType.HotWater).Quantity;
            }

            // Annual hot water used
            var waterUsed = clothesWasherWater + dishWasherWater + showerWater + sinkWater*cookHotWaterPct;

            // Determine the high efficency adjustment.
            if (isHighEfficiency)
            {
                switch (fueltype)
                {
                    case "waterheater.fuel.electric":
                        highEffAdjustment = 0m;
                        break;
                    default:
                        highEffAdjustment = 0.04m;
                        break;
                }
            }

            // Change the maintenance and heat efficency factors based on the fuel type and age of the waterheater
            switch (fueltype)
            {
                // Electric
                case "waterheater.fuel.electric":
                {
                    // Determine maintenance factor and heating efficency
                    if (age > 15)
                    {
                        maintFactor = servicefrequencytype == "waterheater.servicefrequency.regularly" ? 0.90m : 1.0m;
                    }
                    else if (age >= 10)
                    {
                        maintFactor = servicefrequencytype == "waterheater.servicefrequency.regularly" ? 0.93m : 1.0m;
                    }
                    else if (age >= 5)
                    {
                        maintFactor = servicefrequencytype == "waterheater.servicefrequency.regularly" ? 0.96m : 1.0m;
                    }
                    else if (age >= 1)
                    {
                        maintFactor = servicefrequencytype == "waterheater.servicefrequency.regularly" ? 0.98m : 1.0m;
                    }
                    else
                    {
                        maintFactor = servicefrequencytype == "waterheater.servicefrequency.regularly" ? 0.99m : 1.0m;
                    }

                    switch (style)
                    {
                        case "waterheater.style.conventionaltank":
                        case "waterheater.style.demand":
                            heaterEffFactor = 0.98m;
                            break;

                        case "waterheater.style.heatpump":
                            heaterEffFactor = 2.5m;
                            break;
                    }
                    break;
                }

                // Gas
                case "waterheater.fuel.gas":
                case "waterheater.fuel.propane":
                {

                    pilotFuel = isPilotLight ? 300 : 0;

                    damperLoss = isFlueDamper ? 50 : 200;

                    // Determine maintenance factor and heating efficency
                    if (age > 15)
                    {
                        maintFactor = servicefrequencytype == "waterheater.servicefrequency.regularly" ? 0.88m : 1.0m;

                        switch (style)
                        {
                            case "waterheater.style.conventionaltank":
                                heaterEffFactor = 0.73m;
                                break;
                            case "waterheater.style.demand":
                                heaterEffFactor = 0.67m;
                                break;
                            case "waterheater.style.tankless":
                                heaterEffFactor = 0.44m;
                                break;
                            case "waterheater.style.indirect":
                                heaterEffFactor = 0.74m;
                                break;
                        }
                    }
                    else if (age >= 10)
                    {
                        maintFactor = servicefrequencytype == "waterheater.servicefrequency.regularly" ? 0.92m : 1.0m;

                        switch (style)
                        {
                            case "waterheater.style.conventionaltank":
                                heaterEffFactor = 0.76m;
                                break;
                            case "waterheater.style.demand":
                                heaterEffFactor = 0.7m;
                                break;
                            case "waterheater.style.tankless":
                                heaterEffFactor = 0.47m;
                                break;
                            case "waterheater.style.indirect":
                                heaterEffFactor = 0.76m;
                                break;
                        }
                    }
                    else if (age >= 5)
                    {
                        maintFactor = servicefrequencytype == "waterheater.servicefrequency.regularly" ? 0.95m : 1.0m;

                        switch (style)
                        {
                            case "waterheater.style.conventionaltank":
                                heaterEffFactor = 0.77m;
                                break;
                            case "waterheater.style.demand":
                                heaterEffFactor = 0.71m;
                                break;
                            case "waterheater.style.tankless":
                                heaterEffFactor = 0.50m;
                                break;
                            case "waterheater.style.indirect":
                                heaterEffFactor = 0.78m;
                                break;
                        }
                    }
                    else if (age >= 1)
                    {
                        maintFactor = servicefrequencytype == "waterheater.servicefrequency.regularly" ? 0.98m : 1.0m;

                        switch (style)
                        {
                            case "waterheater.style.conventionaltank":
                                heaterEffFactor = 0.78m;
                                break;
                            case "waterheater.style.demand":
                                heaterEffFactor = 0.76m;
                                break;
                            case "waterheater.style.tankless":
                                heaterEffFactor = 0.53m;
                                break;
                            case "waterheater.style.indirect":
                                heaterEffFactor = 0.80m;
                                break;
                        }
                    }
                    else
                    {
                        maintFactor = servicefrequencytype == "waterheater.servicefrequency.regularly" ? 0.99m : 1.0m;

                        switch (style)
                        {
                            case "waterheater.style.conventionaltank":
                                heaterEffFactor = 0.80m;
                                break;
                            case "waterheater.style.demand":
                                heaterEffFactor = 0.82m;
                                break;
                            case "waterheater.style.tankless":
                                heaterEffFactor = 0.54m;
                                break;
                            case "waterheater.style.indirect":
                                heaterEffFactor = 0.81m;
                                break;
                        }
                    }

                    break;
                }

                // Oil
                case "waterheater.fuel.oil":

                    damperLoss = isFlueDamper ? 50 : 200;

                    if (age > 15)
                    {
                        maintFactor = servicefrequencytype == "waterheater.servicefrequency.regularly" ? 0.85m : 1.0m;

                        switch (style)
                        {
                            case "waterheater.style.conventionaltank":
                                heaterEffFactor = 0.59m;
                                break;
                            case "waterheater.style.demand":
                                heaterEffFactor = 0.56m;
                                break;
                            case "waterheater.style.tankless":
                                heaterEffFactor = 0.39m;
                                break;
                            case "waterheater.style.indirect":
                                heaterEffFactor = 0.62m;
                                break;
                        }
                    }
                    else if (age >= 10)
                    {
                        maintFactor = servicefrequencytype == "waterheater.servicefrequency.regularly" ? 0.90m : 1.0m;

                        switch (style)
                        {
                            case "waterheater.style.conventionaltank":
                                heaterEffFactor = 0.63m;
                                break;
                            case "waterheater.style.demand":
                                heaterEffFactor = 0.60m;
                                break;
                            case "waterheater.style.tankless":
                                heaterEffFactor = 0.41m;
                                break;
                            case "waterheater.style.indirect":
                                heaterEffFactor = 0.64m;
                                break;
                        }
                    }
                    else if (age >= 5)
                    {
                        maintFactor = servicefrequencytype == "waterheater.servicefrequency.regularly" ? 0.94m : 1.0m;

                        switch (style)
                        {
                            case "waterheater.style.conventionaltank":
                                heaterEffFactor = 0.64m;
                                break;
                            case "waterheater.style.demand":
                                heaterEffFactor = 0.61m;
                                break;
                            case "waterheater.style.tankless":
                                heaterEffFactor = 0.45m;
                                break;
                            case "waterheater.style.indirect":
                                heaterEffFactor = 0.66m;
                                break;
                        }
                    }
                    else if (age >= 1)
                    {
                        maintFactor = servicefrequencytype == "waterheater.servicefrequency.regularly" ? 0.98m : 1.0m;

                        switch (style)
                        {
                            case "waterheater.style.conventionaltank":
                                heaterEffFactor = 0.65m;
                                break;
                            case "waterheater.style.demand":
                                heaterEffFactor = 0.66m;
                                break;
                            case "waterheater.style.tankless":
                                heaterEffFactor = 0.49m;
                                break;
                            case "waterheater.style.indirect":
                                heaterEffFactor = 0.68m;
                                break;
                        }
                    }
                    else
                    {
                        maintFactor = servicefrequencytype == "waterheater.servicefrequency.regularly" ? 0.99m : 1.0m;

                        switch (style)
                        {
                            case "waterheater.style.conventionaltank":
                                heaterEffFactor = 0.67m;
                                break;
                            case "waterheater.style.demand":
                                heaterEffFactor = 0.71m;
                                break;
                            case "waterheater.style.tankless":
                                heaterEffFactor = 0.50m;
                                break;
                            case "waterheater.style.indirect":
                                heaterEffFactor = 0.69m;
                                break;
                        }
                    }
                    break;
            }


            // Determine the R factor based on the age of the heater.
            if (age > 15)
            {
                rFactor = isTankInsulation ? 13m : 7m;
            }
            else if (age >= 10)
            {
                rFactor = isTankInsulation ? 16m : 10m;
            }
            else if (age >= 5)
            {
                rFactor = isTankInsulation ? 19m : 13m;
            }
            else if (age >= 1)
            {
                rFactor = isTankInsulation ? 22m : 16m;
            }
            else
            {
                rFactor = isTankInsulation ? 22.5m : 16.2m;
            }

            //only continue when these are positive
            if (heaterEffFactor <= 0.0m || rFactor <= 0.0m)
            {
                _appliance.EnergyUses.Add(new EnergyUse(true)
                {
                    Commodity = Enums.CommodityType.Water,
                    Quantity = 0.0m,
                    UnitOfMeasure = Enums.UnitOfMeasureType.Gal
                });
                return;
            }

            // If there is a timer on the waterheater
            decimal systemEverOff;
            decimal daysOff;

            if (isTimer)
            {
                systemEverOff = 0.0m;
                daysOff = config.GetCalculationFactor<decimal>("waterheater.timerdaysoff");
            }
            else
            {
                systemEverOff = 1.0m;
                daysOff = 0.0m;
            }

            // If there are heat traps
            var heatTrapsMult = isHeatTraps ? 0.0m : 1.0m;

            // main
            var hoursEffSystemOn = 8760 -
                                   (1 - systemEverOff)*
                                   (24*daysOff -
                                    8.34m*tankSize*rFactor/
                                    (decimal) (12*Math.Exp(0.667*Math.Log(0.134*(double) tankSize))));

            // Determine the heaters BTUs
            switch (style)
            {
                case "waterheater.style.demand":
                case "waterheater.style.tankless":
                {
                    btu = 8.34m*waterUsed*(temperature - groundwatertemp)/
                          ((heaterEffFactor + highEffAdjustment)*maintFactor) + (8760 - 24*daysOff)*pilotFuel;
                    break;
                }
                case "waterheater.style.heatpump":
                {
                    var isLeapYear = false;
                    var daysInYear = 365;
                    if (DateTime.IsLeapYear(DateTime.Now.Year))
                    {
                        isLeapYear = true;
                        daysInYear = 366;
                    }

                    // get average daily hot water used
                    var dailyWaterUsed = waterUsed/daysInYear;
                    var tamb = 0.0m;
                    var indoorTemp = GetIndoorTemperature(pa, config);
                    var location = pa.Find(p => p.Key == "waterheater.location").OptionKey;

                    // get btu per month
                    for (var i = 0; i < 12; i++)
                    {
                        // get ambient temperature based on location 
                        var outdoorTemp = _input.WeatherInfo.AverageTemperature[0, i];
                        switch (location)
                        {
                            case "waterheater.location.basement":
                                tamb = (indoorTemp + outdoorTemp)/2;
                                break;
                            case "waterheater.location.garage":
                                tamb = outdoorTemp;
                                break;
                            case "waterheater.location.indoors":
                                tamb = indoorTemp;
                                break;
                            case "waterheater.location.outdoors":
                                tamb = outdoorTemp;
                                break;
                        }

                        decimal heatPumpOn;
                        if (tamb < 32)
                        {
                            heatPumpOn = 0;
                        }
                        else
                        {
                            heatPumpOn = (100 -
                                          dailyWaterUsed*waterDensity*waterSpecificHeat*(temperature - 55)/9500)/100;
                        }

                        var heatPumpWaterHeaterPerformanceAdj = (paFactor1*(tamb*tamb)) - (paFactor2*tamb) +
                                                                paFactor3;

                        var heatPumpFraction = (heatPumpOn*ponHeatPump*reHeatPump*heatPumpWaterHeaterPerformanceAdj)/
                                               (heatPumpOn*ponHeatPump*reHeatPump*heatPumpWaterHeaterPerformanceAdj +
                                                (1 - heatPumpOn)*ponElectricResistance*reElectricResistance);

                        var heatLossCoeff = (1/(heaterEffFactor*1) - 1/reHeatPump)/
                                            (67.5m*(24m/qOut - 1/(reHeatPump*ponHeatPump)));

                        // main formula for heat pump daily btu
                        var dailyBtu = dailyWaterUsed*waterDensity*waterSpecificHeat*(temperature - 55m)*
                                       (heatPumpFraction/(reHeatPump*heatPumpWaterHeaterPerformanceAdj)
                                        *(1 - heatLossCoeff*(temperature - tamb)/ponHeatPump)
                                        + (1 - heatPumpFraction)/reElectricResistance
                                        *(1 - heatLossCoeff*(temperature - tamb)/ponElectricResistance))
                                       + 24*heatLossCoeff*(temperature - tamb);

                        var days = 0;
                        if (i + 1 == 1 || i + 1 == 3 || i + 1 == 5 || i + 1 == 7 || i + 1 == 8 || i + 1 == 10 ||
                            i + 1 == 12)
                        {
                            days = 31;
                        }
                        else if (i + 1 == 2)
                        {
                            days = isLeapYear ? 29 : 28;
                        }
                        else if (i + 1 == 4 || i + 1 == 6 || i + 1 == 9 || i + 1 == 11)
                        {
                            days = 30;
                        }

                        // add up monthly btu
                        btu = btu + (dailyBtu*days);
                    }
                    break;
                }
                default:
                {
                    btu = 8.34m*waterUsed*(temperature - groundwatertemp)/
                          ((heaterEffFactor + highEffAdjustment)*maintFactor) +
                          hoursEffSystemOn*6*
                          (decimal) Math.Exp(0.667*Math.Log(0.134*(double) tankSize))*
                          (temperature - groundwatertemp)/rFactor + (8760 - 24*daysOff)*pilotFuel +
                          hoursEffSystemOn*damperLoss + 1400000*heatTrapsMult;
                    break;
                }
            }

            // solar adjustments for BTU
            if (isSolarHeater)
            {
                var activeFactor = isSolarActive ? 0.8m : 1.0m;

                var solarOrientationFactor =
                    Convert.ToDecimal(pa.Find(p => p.Key == "waterheater.solarorientation").OptionValue);

                var avgBtu = 0.0m;
                switch (_input.RegionParams.SolarRegionId)
                {
                    case 0:
                        avgBtu = 50;
                        break;
                    case 1:
                        avgBtu = 36;
                        break;
                    case 2:
                        avgBtu = 43;
                        break;
                    case 3:
                        avgBtu = 50;
                        break;
                    case 4:
                        avgBtu = 56;
                        break;
                    case 5:
                        avgBtu = 63;
                        break;
                    case 6:
                        avgBtu = 69;
                        break;
                    case 7:
                        avgBtu = 76;
                        break;
                    case 8:
                        avgBtu = 83;
                        break;
                    case 9:
                        avgBtu = 89;
                        break;
                    case 10:
                        avgBtu = 96;
                        break;
                }

                var solarSize = Convert.ToDecimal(pa.Find(p => p.Key == "waterheater.solarsize").OptionValue);

                var solarEnergy = 4380*activeFactor*solarOrientationFactor*solarSize*avgBtu;

                btu = btu - solarEnergy;

                var standByLosses = hoursEffSystemOn*6*
                                    (decimal) Math.Exp(0.667*Math.Log(0.134*(double) tankSize))*
                                    (temperature - groundwatertemp)/rFactor + (8760 - 24*daysOff)*pilotFuel +
                                    hoursEffSystemOn*damperLoss + 1400000*heatTrapsMult;

                if (btu < standByLosses)
                {
                    btu = standByLosses;
                }
            }

            // If the pipes are insulated adjust the BTU
            if (isPipesInsulation)
            {
                btu = 0.97m*btu;
            }

            // If have a heat recovery system.
            if (isHeatRecovery && style != "waterheater.style.heatpump")
            {
                var coolingSeasonWeeks = 6.75m + _input.WeatherInfo.AnnualCDD/125m;

                var heatsystemtype = pa.Find(p => p.Key == "heatsystem.style").OptionKey;
                if (heatsystemtype == "heatsystem.style.airsourcheatpump")
                {
                    btu = btu*(1 - (0.66m*coolingSeasonWeeks/52m + 0.33m*(52m - coolingSeasonWeeks)/52m));
                }
                else
                {
                    btu = btu*(1 - (0.66m*coolingSeasonWeeks/52m));
                }
            }

            //final output
            // No Water heater - no water usage
            if (style == "waterheater.style.tankless")
            {
                _appliance.EnergyUses.Add(new EnergyUse(true)
                {
                    Commodity = Enums.CommodityType.Electric,
                    Quantity = 0.0m,
                    UnitOfMeasure = Enums.UnitOfMeasureType.kWh
                });
                return;
            }

            // Water heater - water and energy usage.
            _appliance.EnergyUses.Add(new EnergyUse(true)
            {
                Commodity = Enums.CommodityType.HotWater,
                Quantity = waterUsed,
                UnitOfMeasure = Enums.UnitOfMeasureType.Gal
            });

            decimal output;
            switch (fueltype)
            {
                case "waterheater.fuel.electric":
                    output = count*(btu/3412m);
                    _appliance.EnergyUses.Add(new EnergyUse(true)
                    {
                        Commodity = Enums.CommodityType.Electric,
                        Quantity = output,
                        UnitOfMeasure = Enums.UnitOfMeasureType.kWh
                    });
                    break;
                case "waterheater.fuel.gas":
                    output = count*(btu/100000m);
                    _appliance.EnergyUses.Add(new EnergyUse(true)
                    {
                        Commodity = Enums.CommodityType.Gas,
                        Quantity = output,
                        UnitOfMeasure = Enums.UnitOfMeasureType.Therms
                    });
                    break;
                case "waterheater.fuel.propane":
                    output = count*(btu/92000m);
                    _appliance.EnergyUses.Add(new EnergyUse(true)
                    {
                        Commodity = Enums.CommodityType.Propane,
                        Quantity = output,
                        UnitOfMeasure = Enums.UnitOfMeasureType.Gal
                    });
                    break;
                case "waterheater.fuel.oil":
                    output = count*(btu/138690m);
                    _appliance.EnergyUses.Add(new EnergyUse(true)
                    {
                        Commodity = Enums.CommodityType.Oil,
                        Quantity = output,
                        UnitOfMeasure = Enums.UnitOfMeasureType.Gal
                    });
                    break;
                default:
                    _appliance.EnergyUses.Add(new EnergyUse(true)
                    {
                        Commodity = Enums.CommodityType.Electric,
                        Quantity = 0.0m,
                        UnitOfMeasure = Enums.UnitOfMeasureType.kWh
                    });
                    break;
            }
        }

        /// <summary>
        /// Model for a pool
        /// </summary>
        private void Model_pool()
        {
            decimal output;
            decimal waterOutput = 0.0m;
            decimal btu;
            decimal heaterEffFactor;
            decimal coverFactor;
            decimal coverFactorWater;
            decimal sizeFactor;
            decimal frequencyFactor;

            var pa = _input.ProfileAttributes;
            var config = _input.Configuration;

            var count = Convert.ToDecimal(pa.Find(p => p.Key == "pool.count").OptionValue);
            var size = Convert.ToDecimal(pa.Find(p => p.Key == "pool.size").OptionValue);
            //bool isTimer = System.Convert.ToBoolean(Convert.ToInt32(pa.Find(p => p.Key == "pool.timer").OptionValue));
            var isPoolHeater = Convert.ToBoolean(Convert.ToInt32(pa.Find(p => p.Key == "pool.poolheater").OptionValue));
            var isSolarHeater = Convert.ToBoolean(Convert.ToInt32(pa.Find(p => p.Key == "pool.solarheater").OptionValue));
            var isEfficientPump =
                Convert.ToBoolean(Convert.ToInt32(pa.Find(p => p.Key == "pool.efficientpump").OptionValue));
            var isTurnOverOnce =
                Convert.ToBoolean(Convert.ToInt32(pa.Find(p => p.Key == "pool.poolturnoveronce").OptionValue));
            var poolBtu = config.GetCalculationFactor<decimal>("pool.poolbtu");
            var horsePower = Convert.ToDecimal(pa.Find(p => p.Key == "pool.horsepower").OptionValue);
            var pumpUsageHours = Convert.ToDecimal(pa.Find(p => p.Key == "pool.pumpusagehrs").OptionValue);
            var collectorRatio = Convert.ToDecimal(pa.Find(p => p.Key == "pool.collectorratio").OptionValue);
            var siteConditions = Convert.ToDecimal(pa.Find(p => p.Key == "pool.siteconditions").OptionValue);
            var temperature = Convert.ToDecimal(pa.Find(p => p.Key == "pool.temperature").OptionValue);

            var fueltype = pa.Find(p => p.Key == "pool.fuel").OptionKey;

            var frequencyfactortype = pa.Find(p => p.Key == "pool.heatingfrqcy").OptionKey;
            switch (frequencyfactortype)
            {
                case "pool.heatingfrqcy.hardly_ever":
                    frequencyFactor = 0.25m;
                    break;
                case "pool.heatingfrqcy.seldom":
                    frequencyFactor = 0.5m;
                    break;
                case "pool.heatingfrqcy.regulary":
                    frequencyFactor = 1.0m;
                    break;
                case "pool.heatingfrqcy.never":
                    frequencyFactor = 0.0m;
                    break;
                default:
                    frequencyFactor = 0.0m;
                    break;
            }

            var coverusagetype = pa.Find(p => p.Key == "pool.poolcoverusage").OptionKey;
            switch (coverusagetype)
            {
                case "pool.poolcoverusage.useregularly":
                    coverFactor = 4.0m;
                    coverFactorWater = 0.5m;
                    break;
                case "pool.poolcoverusage.useirregularly":
                    coverFactor = 2.0m;
                    coverFactorWater = 0.8m;
                    break;
                case "pool.poolcoverusage.donothave":
                    coverFactor = 0.0m;
                    coverFactorWater = 1.0m;
                    break;
                default:
                    coverFactor = 0.0m;
                    coverFactorWater = 1.0m;
                    break;
            }

            if (isPoolHeater)
            {
                if (isSolarHeater)
                {
                    heaterEffFactor = 8*(0.25m + collectorRatio)*siteConditions;
                }
                else
                {
                    switch (fueltype)
                    {
                        case "pool.fuel.electric":
                            heaterEffFactor = 4.0m;
                            break;
                        case "pool.fuel.gas":
                        case "pool.fuel.propane":
                            heaterEffFactor = 0.8m;
                            break;
                        case "pool.fuel.oil":
                            heaterEffFactor = 0.75m;
                            break;
                        case "pool.fuel.solar":
                            heaterEffFactor = 5.0m;
                            break;
                        default:
                            heaterEffFactor = 4.0m;
                            break;
                    }
                }
            }
            else
            {
                heaterEffFactor = 0.0m;
            }

            var pumpEnergyEffFactor = isEfficientPump ? 0.9m : 1.0m;
            var pumpUseTurnoverFactor = isTurnOverOnce ? 0.8m : 1.0m;


            if (size > 700)
            {
                sizeFactor = 1.3m;
            }
            else if (size >= 400)
            {
                sizeFactor = 1.0m;
            }
            else
            {
                sizeFactor = 0.8m;
            }

            if (heaterEffFactor != 0.0m)
            {
                btu = sizeFactor*frequencyFactor*poolBtu*(temperature - coverFactor - 70)/(82 - 70)*coverFactorWater/
                      heaterEffFactor;
            }
            else
            {
                btu = 0.0m;
            }


            var pumpWeeks =
                config.GetCalculationFactor<decimal>(isPoolHeater ? "pool.pumpweeks_heated" : "pool.pumpweeks_unheated");

            if (horsePower == 0.0m)
            {
                horsePower = 2.0m;
            }
            var horsePowerFactor = 7*(horsePower/1.341m);


            // energy output
            var outputkwh = pumpEnergyEffFactor*pumpUseTurnoverFactor*horsePowerFactor*pumpUsageHours*pumpWeeks;

            switch (fueltype)
            {
                case "pool.fuel.electric":
                    output = count*(outputkwh + (btu/3413m));
                    _appliance.EnergyUses.Add(new EnergyUse(true)
                    {
                        Commodity = Enums.CommodityType.Electric,
                        Quantity = output,
                        UnitOfMeasure = Enums.UnitOfMeasureType.kWh
                    });
                    break;
                case "pool.fuel.gas":
                    output = count*outputkwh;
                    _appliance.EnergyUses.Add(new EnergyUse(true)
                    {
                        Commodity = Enums.CommodityType.Electric,
                        Quantity = output,
                        UnitOfMeasure = Enums.UnitOfMeasureType.kWh
                    });

                    if (isPoolHeater)
                    {
                        output = count*(btu/100000m);
                        _appliance.EnergyUses.Add(new EnergyUse(true)
                        {
                            Commodity = Enums.CommodityType.Gas,
                            Quantity = output,
                            UnitOfMeasure = Enums.UnitOfMeasureType.Therms
                        });
                    }

                    break;
                case "pool.fuel.propane":
                    output = count*outputkwh;
                    _appliance.EnergyUses.Add(new EnergyUse(true)
                    {
                        Commodity = Enums.CommodityType.Electric,
                        Quantity = output,
                        UnitOfMeasure = Enums.UnitOfMeasureType.kWh
                    });

                    if (isPoolHeater)
                    {
                        output = count*(btu/92000m);
                        _appliance.EnergyUses.Add(new EnergyUse(true)
                        {
                            Commodity = Enums.CommodityType.Propane,
                            Quantity = output,
                            UnitOfMeasure = Enums.UnitOfMeasureType.Gal
                        });
                    }

                    break;

                case "pool.fuel.oil":
                    output = count*outputkwh;
                    _appliance.EnergyUses.Add(new EnergyUse(true)
                    {
                        Commodity = Enums.CommodityType.Electric,
                        Quantity = output,
                        UnitOfMeasure = Enums.UnitOfMeasureType.kWh
                    });

                    if (isPoolHeater)
                    {
                        output = count*(btu/138690m);
                        _appliance.EnergyUses.Add(new EnergyUse(true)
                        {
                            Commodity = Enums.CommodityType.Oil,
                            Quantity = output,
                            UnitOfMeasure = Enums.UnitOfMeasureType.Gal
                        });
                    }

                    break;
                default:
                    _appliance.EnergyUses.Add(new EnergyUse(true)
                    {
                        Commodity = Enums.CommodityType.Electric,
                        Quantity = 0.0m,
                        UnitOfMeasure = Enums.UnitOfMeasureType.kWh
                    });
                    break;
            }



            // water output using load factors with evaporation data here
            int[] weeklyThreshold = {0, 48, 44, 35, 27, 22, 12, 1, 5, 10, 18, 30, 40};
            double[] monthlyLoadFactors = {0.13, 0.13, 0.1, 0.08, 0.07, 0.05, 0.04, 0.04, 0.05, 0.08, 0.1, 0.13};
            double[] adjustedMonthlyLoadFactors = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};

            var annual = 0.0;
            for (var i = 0; i < 12; i++)
            {
                if (pumpWeeks >= weeklyThreshold[i + 1])
                {
                    if (isPoolHeater)
                    {
                        adjustedMonthlyLoadFactors[i] = monthlyLoadFactors[i];
                    }
                    else
                    {
                        adjustedMonthlyLoadFactors[i] = 1;
                    }
                }
                else
                {
                    adjustedMonthlyLoadFactors[i] = 0;
                }

                annual = annual + adjustedMonthlyLoadFactors[i];
            }

            for (var i = 0; i < 12; i++)
            {
                adjustedMonthlyLoadFactors[i] = adjustedMonthlyLoadFactors[i]/annual;
            }

            if (count > 0)
            {
                for (var i = 0; i < 12; i++)
                {
                    waterOutput = waterOutput +
                                  (_input.WeatherInfo.MonthlyEvaporationFactors[i + 1]*
                                   (decimal) adjustedMonthlyLoadFactors[i]);
                }
            }

            waterOutput = waterOutput*size*coverFactorWater;

            //water output
            _appliance.EnergyUses.Add(new EnergyUse(true)
            {
                Commodity = Enums.CommodityType.Water,
                Quantity = waterOutput,
                UnitOfMeasure = Enums.UnitOfMeasureType.Gal
            });

        }

        /// <summary>
        /// Model for secondary heating
        /// </summary>
        private void Model_secondaryheating()
        {
            decimal output;
            var thermoFlow = 0.0m;
            decimal ageFactor;

            var pa = _input.ProfileAttributes;

            var age = ResetDefaultYear(pa, "secondaryheating.year");

            var count = Convert.ToDecimal(pa.Find(p => p.Key == "secondaryheating.count").OptionValue);
            var serviceArea = Convert.ToDecimal(pa.Find(p => p.Key == "secondaryheating.servicearea").OptionValue);
            var useFactor = Convert.ToDecimal(pa.Find(p => p.Key == "secondaryheating.usage").OptionValue);

            //must run model for several individual appliances to get *water* output to be used for this water heater;
            //each appliance must be able to be executed without dependencies on other appliances, thus these outputs being used here
            var evalHeatSystem = new ApplianceEvaluator(new Appliance {Name = "HeatSystem"}, _input);
            evalHeatSystem.Model_heatsystem();
            if (evalHeatSystem.Appliance.EnergyUses != null &&
                evalHeatSystem.Appliance.EnergyUses.Exists(p => p.Commodity == Enums.CommodityType.ThermoFlow))
                //ThermoFlow is special!
            {
                thermoFlow =
                    evalHeatSystem.Appliance.EnergyUses.Find(p => p.Commodity == Enums.CommodityType.ThermoFlow)
                        .Quantity;
            }


            if (age > 15)
            {
                ageFactor = 1.06m;
            }
            else if (age >= 10)
            {
                ageFactor = 1.04m;
            }
            else if (age >= 5)
            {
                ageFactor = 1.02m;
            }
            else
            {
                ageFactor = 1.0m;
            }

            var btus = thermoFlow*ageFactor*serviceArea*useFactor;

            var fueltype = pa.Find(p => p.Key == "secondaryheating.fuel").OptionKey;
            switch (fueltype)
            {
                case "secondaryheating.fuel.electric":
                    output = count*(btus/3413m);
                    _appliance.EnergyUses.Add(new EnergyUse(true)
                    {
                        Commodity = Enums.CommodityType.Electric,
                        Quantity = output,
                        UnitOfMeasure = Enums.UnitOfMeasureType.kWh
                    });
                    break;

                case "secondaryheating.fuel.gas":
                    output = count*(btus/100000m);
                    _appliance.EnergyUses.Add(new EnergyUse(true)
                    {
                        Commodity = Enums.CommodityType.Gas,
                        Quantity = output,
                        UnitOfMeasure = Enums.UnitOfMeasureType.Therms
                    });
                    break;

                case "secondaryheating.fuel.propane":
                    output = count*(btus/92000m);
                    _appliance.EnergyUses.Add(new EnergyUse(true)
                    {
                        Commodity = Enums.CommodityType.Propane,
                        Quantity = output,
                        UnitOfMeasure = Enums.UnitOfMeasureType.Gal
                    });
                    break;

                case "secondaryheating.fuel.oil":
                    output = count*(btus/138690m);
                    _appliance.EnergyUses.Add(new EnergyUse(true)
                    {
                        Commodity = Enums.CommodityType.Oil,
                        Quantity = output,
                        UnitOfMeasure = Enums.UnitOfMeasureType.Gal
                    });
                    break;

                case "secondaryheating.fuel.wood":
                    output = count*btus;
                    _appliance.EnergyUses.Add(new EnergyUse(true)
                    {
                        Commodity = Enums.CommodityType.Wood,
                        Quantity = output,
                        UnitOfMeasure = Enums.UnitOfMeasureType.BTU
                    });
                    break;
            }
        }

        /// <summary>
        /// Model for heating system
        /// </summary>
        private void Model_heatsystem()
        {
            decimal propane_gal;
            decimal kwh;
            decimal therms;
            decimal oil_gal;
            var btus = 0.0m;

            var pa = _input.ProfileAttributes;
            var config = _input.Configuration;

            heatsystemage = ResetDefaultYearTwoWayCheck(pa, "heatsystem.yearinstalled", "heatsystem.yearinstalledrange");

            UpdateHeatSystemInternalSetup(pa, config);

            var thermoFlow = GetHeatTempDiff(pa, config);

            var count = Convert.ToDecimal(pa.Find(p => p.Key == "heatsystem.count").OptionValue);
            var style = pa.Find(p => p.Key == "heatsystem.style").OptionKey;
            var fuel = pa.Find(p => p.Key == "heatsystem.fuel").OptionKey;
            var woodtype = pa.Find(p => p.Key == "heatsystem.woodtype").OptionKey;
            var woodqty = Convert.ToInt32(pa.Find(p => p.Key == "heatsystem.woodqty").OptionValue);

            if (
                !(deliveryfactor == 0 || heatefficiency == 0 || count == 0 || oilbtufactor == 0 || propanebtufactor == 0))
            {
                if (fuel == "heatsytem.fuel.wood")
                {
                    switch (woodtype)
                    {
                        case "heatsystem.wood.seasonedhardwood":
                            woodfactor = 27500000;
                            break;
                        case "heatsystem.wood.unseasonedhardwood":
                            woodfactor = 26000000;
                            break;
                        case "heatsystem.wood.seasonedsoftwood":
                            woodfactor = 18000000;
                            break;
                        case "heatsystem.wood.unseasonedsoftwood":
                            woodfactor = 16500000;
                            break;
                        case "heatsystem.wood.woodpellets":
                            woodfactor = 17000000;
                            break;
                        default:
                            woodfactor = 0;
                            break;
                    }
                    btus = woodqty*woodfactor;
                }
                else
                {
                    btus = closedroomfactor*(1 - secondsystemfactor)*thermoFlow/
                           (deliveryfactor*heatefficiency) - ignfactor;
                }
            }

            if (btus < 0)
            {
                btus = 0;
            }

            decimal supplementheatfactorhp = 0;

            if (style == "heatsystem.style.heatpumpgasbackup" || style == "heatsystem.style.heatpumppropanebackup" ||
                style == "heatsystem.style.heatpumpoilbackup")
            {
                supplementheatfactorhp = config.GetCalculationFactor<decimal>("heatsystem.supplementalheatfactor_hp");
            }

            var addonhydro = pa.Find(p => p.Key == "heatsystem.addonhydro").OptionKey;
            var fueltype = pa.Find(p => p.Key == "heatsystem.fuel").OptionKey;
            var electrignition = pa.Find(p => p.Key == "heatsystem.electrignition").OptionKey;

            switch (fueltype)
            {
                case "heatsystem.fuel.electric":
                    kwh = btus/3413;
                    therms = 0;
                    oil_gal = 0;
                    propane_gal = 0;
                    woodbtus = 0;

                    if (style == "heatsystem.style.airsourceheatpump" & addonhydro == "heatsystem.addonhydro.yes")
                    {
                        switch (waterheaterfuel)
                        {
                            case "waterheater.fuel.electric":
                                kwh = Convert.ToDecimal(0.5m*btus/3413);
                                therms = Convert.ToDecimal(1.0625m*btus/100000);
                                break;
                            case "waterheater.fuel.oil":
                                kwh = Convert.ToDecimal(0.5m*btus/3413);
                                oil_gal = Convert.ToDecimal(1.0625m*btus/oilbtufactor);
                                break;
                            case "waterheater.fuel.propane":
                                kwh = Convert.ToDecimal(0.5m*btus/3413);
                                propane_gal = Convert.ToDecimal(1.0625m*btus/propanebtufactor);
                                break;
                        }
                    }

                    if (style == "heatsystem.style.heatpumpgasbackup")
                    {
                        kwh = btus*supplementheatfactorhp/3413;
                        therms = btus*(1 - supplementheatfactorhp)/100000;
                        oil_gal = 0;
                        propane_gal = 0;
                        woodbtus = 0;
                    }

                    if (style == "heatsystem.style.heatpumppropanebackup")
                    {
                        kwh = btus*supplementheatfactorhp/3413;
                        therms = 0;
                        oil_gal = 0;
                        propane_gal = btus*(1 - supplementheatfactorhp)/propanebtufactor;
                        woodbtus = 0;
                    }

                    if (style == "heatsystem.style.heatpumpoilbackup")
                    {
                        kwh = btus*supplementheatfactorhp/3413;
                        therms = 0;
                        oil_gal = btus*(1 - supplementheatfactorhp)/oilbtufactor;
                        propane_gal = 0;
                        woodbtus = 0;
                    }
                    break;

                case "heatsystem.fuel.gas":
                    therms = btus/100000;

                    if (electrignition == "heatsystem.electrignition.no")
                    {
                        kwh = Convert.ToDecimal(auxuse*(btus + ignfactor)/3413);
                        if (kwh < 0)
                            kwh = 0;
                    }
                    else
                    {
                        kwh = Convert.ToDecimal(auxuse*btus/3413);
                    }

                    oil_gal = 0;
                    propane_gal = 0;
                    woodbtus = 0;
                    break;

                case "heatsystem.fuel.propane":
                    propane_gal = btus/propanebtufactor;

                    if (electrignition == "heatsystem.electrignition.no")
                    {
                        kwh = Convert.ToDecimal(auxuse*(btus + ignfactor)/3413);
                        if (kwh < 0)
                            kwh = 0;
                    }
                    else
                    {
                        kwh = Convert.ToDecimal(auxuse*btus/3413);
                    }

                    therms = 0;
                    oil_gal = 0;
                    woodbtus = 0;
                    break;

                case "heatsystem.fuel.oil":
                    oil_gal = btus/oilbtufactor;

                    if (electrignition == "heatsystem.electrignition.no")
                    {
                        kwh = Convert.ToDecimal(auxuse*(btus + ignfactor)/3413);
                        if (kwh < 0)
                            kwh = 0;
                    }
                    else
                    {
                        kwh = Convert.ToDecimal(auxuse*btus/3413);
                    }

                    propane_gal = 0;
                    therms = 0;
                    woodbtus = 0;
                    break;

                case "heatsystem.fuel.wood":
                    oil_gal = 0;
                    kwh = 0;
                    propane_gal = 0;
                    therms = 0;
                    woodbtus = btus;
                    break;

                default:
                    therms = 0;
                    oil_gal = 0;
                    propane_gal = 0;
                    kwh = Convert.ToDecimal(auxuse*btus/3413);
                    woodbtus = 0;
                    break;
            }

            if (kwh > 0)
            {
                _appliance.EnergyUses.Add(new EnergyUse(true)
                {
                    Commodity = Enums.CommodityType.Electric,
                    Quantity = kwh,
                    UnitOfMeasure = Enums.UnitOfMeasureType.kWh
                });
            }
            if (therms > 0)
            {
                _appliance.EnergyUses.Add(new EnergyUse(true)
                {
                    Commodity = Enums.CommodityType.Gas,
                    Quantity = therms,
                    UnitOfMeasure = Enums.UnitOfMeasureType.Therms
                });
            }
            if (oil_gal > 0)
            {
                _appliance.EnergyUses.Add(new EnergyUse(true)
                {
                    Commodity = Enums.CommodityType.Oil,
                    Quantity = oil_gal,
                    UnitOfMeasure = Enums.UnitOfMeasureType.Gal
                });
            }
            if (propane_gal > 0)
            {
                _appliance.EnergyUses.Add(new EnergyUse(true)
                {
                    Commodity = Enums.CommodityType.Propane,
                    Quantity = propane_gal,
                    UnitOfMeasure = Enums.UnitOfMeasureType.Gal
                });
            }
            if (woodbtus > 0)
            {
                _appliance.EnergyUses.Add(new EnergyUse(true)
                {
                    Commodity = Enums.CommodityType.Wood,
                    Quantity = woodbtus,
                    UnitOfMeasure = Enums.UnitOfMeasureType.BTU
                });
            }

            // add the special thermo flow output here (weather btus)
            _appliance.EnergyUses.Add(new EnergyUse(true)
            {
                Commodity = Enums.CommodityType.ThermoFlow,
                Quantity = thermoFlow,
                UnitOfMeasure = Enums.UnitOfMeasureType.BTU
            });

        }

        /// <summary>
        /// Model for central airconditioning
        /// </summary>
        private void Model_centralac()
        {

            var pa = _input.ProfileAttributes;
            var config = _input.Configuration;

            centralacage = ResetDefaultYear(pa, "centralac.year");

            UpdateCentralACInternalSetup(pa, config);

            var aceff = Convert.ToDecimal(1000*seer*(sealleaksfactor));

            var thermoflow = GetCentralACTempDiff(pa, config);

            var coolstyle = pa.Find(p => p.Key == "centralac.style").OptionKey;

            string stemp;
            switch (coolstyle)
            {
                case "centralac.style.evaporativecooling":
                    stemp = "centralac.stylefactor_evap";
                    break;
                case "centralac.style.EvaporativeandStandard":
                    stemp = "centralac.stylefactor_mix";
                    break;
                case "centralac.style.airsourceheatpump":
                    stemp = "centralac.stylefactor_air";
                    break;
                case "centralac.style.groundsourceheatpump":
                    stemp = "centralac.stylefactor_ground";
                    break;
                default:
                    stemp = "centralac.stylefactor_std";
                    break;
            }

            var nstylefactor = config.GetCalculationFactor<decimal>(stemp);

            var count = Convert.ToDecimal(pa.Find(p => p.Key == "centralac.count").OptionValue);

            var output = count*nstylefactor*slabfactor*thermoflow/aceff;

            _appliance.EnergyUses.Add(new EnergyUse(true)
            {
                Commodity = Enums.CommodityType.Electric,
                Quantity = output,
                UnitOfMeasure = Enums.UnitOfMeasureType.kWh
            });

            // add the special thermo flow output here for AC (temperature difference or weather btus)
            _appliance.EnergyUses.Add(new EnergyUse(true)
            {
                Commodity = Enums.CommodityType.ThermoFlow,
                Quantity = thermoflow,
                UnitOfMeasure = Enums.UnitOfMeasureType.BTU
            });

        }

        /// <summary>
        /// Reset to the default year
        /// </summary>
        /// <param name="pa">A ProfileAttributeCollection</param>
        /// <param name="applianceyear">The current appliance year</param>
        /// <returns>The new appliance year</returns>
        private static int ResetDefaultYear(ProfileAttributeCollection pa, string applianceyear)
        {
            var yearsource = pa.Find(p => p.Key == applianceyear).Source;
            var year = (int) Convert.ToDecimal(pa.Find(p => p.Key == applianceyear).OptionValue);
            var houseyear = (int) GetSpecialDecimalFromValueOrList(pa, "house.yearbuilt", "house.yearbuiltrange");
            var age = DateTime.Now.Year - year;
            var houseage = DateTime.Now.Year - houseyear;

            if (yearsource != "D")
            {
                return age;
            }

            if (age > houseage)
            {
                age = houseage;
            }
            return age;
        }

        /// <summary>
        /// Reset the appliance year with a two way check
        /// </summary>
        /// <param name="pas">A ProfileAttributeCollection</param>
        /// <param name="applianceyear">The current appliance year</param>
        /// <param name="applianceyearAlternate">An alternative appliance year</param>
        /// <returns>The new appliance year.</returns>
        private static int ResetDefaultYearTwoWayCheck(ProfileAttributeCollection pas, string applianceyear,
            string applianceyearAlternate)
        {
            var yearsource = "X";
            int year;
            var age = 0;
            var houseage = 0;

            var pa1 = pas.Find(p => p.Key == applianceyear);
            var pa2 = pas.Find(p => p.Key == applianceyearAlternate);
            var houseyear = (int) GetSpecialDecimalFromValueOrList(pas, "house.yearbuilt", "house.yearbuiltrange");

            if (pa1 != null && pa1.Source == "U")
            {
                yearsource = pa1.Source;
                if (!string.IsNullOrWhiteSpace(pa1.OptionValue))
                {
                    year = (int) Convert.ToDecimal(pa1.OptionValue);
                    age = DateTime.Now.Year - year;
                }
                houseage = DateTime.Now.Year - houseyear;
            }
            else if (pa2 != null && pa2.Source == "U")
            {
                yearsource = pa2.Source;
                if (!string.IsNullOrWhiteSpace(pa2.OptionValue))
                {
                    year = (int) Convert.ToDecimal(pa2.OptionValue);
                    age = DateTime.Now.Year - year;
                }
                houseage = DateTime.Now.Year - houseyear;
            }
            else
            {
                if (pa1 != null)
                {
                    yearsource = pa1.Source;
                    if (!string.IsNullOrWhiteSpace(pa1.OptionValue))
                    {
                        year = (int) Convert.ToDecimal(pa1.OptionValue);
                        age = DateTime.Now.Year - year;
                    }
                    houseage = DateTime.Now.Year - houseyear;
                }
            }

            if (yearsource != "D")
            {
                return age;
            }

            if (age > houseage)
            {
                age = houseage;
            }

            return age;
        }

        /// <summary>
        /// Get Special Decimal From Value Or List 
        /// </summary>
        /// <param name="pas"></param>
        /// <param name="paAsValue"></param>
        /// <param name="paAsList"></param>
        /// <returns></returns>
        private static decimal GetSpecialDecimalFromValueOrList(ProfileAttributeCollection pas, string paAsValue,
            string paAsList)
        {
            var outDecimal = 0m;
            var pa1 = pas.Find(p => p.Key == paAsValue);
            var pa2 = pas.Find(p => p.Key == paAsList);

            if (pa2 != null && pa2.Source == "U")
            {
                outDecimal = Convert.ToDecimal(pa2.OptionValue);
            }
            else if (pa1 != null)
            {
                outDecimal = Convert.ToDecimal(pa1.OptionValue);
            }

            return outDecimal;
        }

    }
}

