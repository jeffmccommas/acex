﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CE.EnergyModel
{
    public class EnergyHighlightsCollection
    {
        public decimal TotalCoolLoad { get; set; }

        public decimal WeatherAdjustmentFactorCDD { get; set; }

        public decimal TotalHeatLoad { get; set; }

        public decimal WeatherAdjustmentFactorHDD { get; set; }

        public decimal BaseFuelLoad { get; set; }


    }
}
