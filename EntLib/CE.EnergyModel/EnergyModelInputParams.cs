﻿
namespace CE.EnergyModel
{
    public class EnergyModelInputParams
    {
        public int ClientId { get; set; }           //manager has this stored, it must set this value here
        public RegionParams RegionParams { get; set; }
        public IDParams IdParams { get; set; }
        public ExtraParams ExtraParams { get; set; }
        public ProfileAttributeCollection ProfileAttributes { get; set; }
        public ApplianceCollection Appliances { get; set; }
        public EndUseCollection EndUses { get; set; }
        public EnergyModelConfiguration Configuration { get; set; }
        public DerivedWeather WeatherInfo { get; set; }

        public EnergyModelInputParams()
        {
            WeatherInfo = new DerivedWeather();
        }

    }

    public class DerivedWeather
    {
        public int AnnualHDD { get; set; }
        public int AnnualCDD { get; set; }
        public decimal WeeksWinter { get; set; }
        public decimal WeeksSpring { get; set; }
        public decimal WeeksSummer { get; set; }
        public decimal WeeksFall { get; set; }
        public decimal[] MonthlyEvaporationFactors { get; set; }
        public int GroundWaterTemp { get; set; }
        public int HeatDesignTemp { get; set; }
        public int CoolDesignTemp { get; set; }
        public int CoolDesignWetBulb { get; set; }
        public decimal StandardPressure { get; set; }
        public decimal[, ,] SolarFactors { get; set; }
        public decimal[, ,] TemperatureFactors { get; set; }
        public decimal[,] AverageTemperature { get; set; } // Dec 2015 TFS 1463 - average temperate per month

        public DerivedWeather()
        {
            MonthlyEvaporationFactors = new decimal[] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
            SolarFactors = new decimal[2, 4, 24];
            TemperatureFactors = new decimal[2, 12, 24];
            AverageTemperature = new decimal[2, 12]; // Dec 2015 TFS 1463 - average temperate per month
        }

    }

}
