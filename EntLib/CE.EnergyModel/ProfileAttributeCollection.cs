﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;


namespace CE.EnergyModel
{
    public class ProfileAttributeCollection : Collection<ProfileAttribute>
    {
        public bool IsStandAlone { get; set; }

        public ProfileAttributeCollection()
            : base(new List<ProfileAttribute>())
        {

        }

        public ProfileAttribute Find(Predicate<ProfileAttribute> match)
        {
            List<ProfileAttribute> items = (List<ProfileAttribute>)Items;
            return (items.Find(match));
        }

        public bool Exists(Predicate<ProfileAttribute> match)
        {
            List<ProfileAttribute> items = (List<ProfileAttribute>)Items;
            return (items.Exists(match));
        }

        public void AddRange(ProfileAttributeCollection attributes)
        {
            List<ProfileAttribute> items = (List<ProfileAttribute>)Items;
            items.AddRange(attributes);
        }

        public int GetAttributeCountByApplianceAndDerivedSource(string applianceName)
        {
            int count = 0;
            List<ProfileAttribute> items = (List<ProfileAttribute>)Items;

            var all = items.FindAll(p => p.Key.Contains(applianceName) && p.Source == "U" || p.Source == "P");     //User or Provided profile attributes
            if (all != null)
            {
                count = all.Count;
            }

            return (count);
        }

    }
}
