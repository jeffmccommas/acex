﻿
namespace CE.EnergyModel
{
    public static class Constants
    {
        public const string ApplianceComplex_Freezer = "freezer";
        public const string ApplianceComplex_Freezer2 = "freezer2";
        public const string ApplianceComplex_WellPump = "wellpump";
        public const string ApplianceComplex_Lighting = "lighting";
        public const string ApplianceComplex_SumpPump = "sumppump";
        public const string ApplianceComplex_Microwave = "microwave";
        public const string ApplianceComplex_WaterBed = "waterbed";
        public const string ApplianceComplex_Vehicle = "vehicle";
        public const string ApplianceComplex_Vehicle2 = "vehicle2";
        public const string ApplianceComplex_SpaceHeater = "spaceheater";
        public const string ApplianceComplex_Refrigerator = "refrigerator";
        public const string ApplianceComplex_Refrigerator2 = "refrigerator2";
        public const string ApplianceComplex_Oven = "oven";
        public const string ApplianceComplex_RoomAC = "roomac";
        public const string ApplianceComplex_RoomAC2 = "roomac2";
        public const string ApplianceComplex_RoomAC3 = "roomac3";
        public const string ApplianceComplex_Fireplace = "fireplace";
        public const string ApplianceComplex_HotTub = "hottub";
        public const string ApplianceComplex_ClothesWasher = "clotheswasher";
        public const string ApplianceComplex_Cooktop = "cooktop";
        public const string ApplianceComplex_DishWasher = "dishwasher";
        public const string ApplianceComplex_Dryer = "dryer";
        public const string ApplianceComplex_Shower = "shower"; public const string ApplianceComplex_Shower_Alt = "showers";
        public const string ApplianceComplex_Sink = "sink"; public const string ApplianceComplex_Sink_Alt = "sinks";
        public const string ApplianceComplex_CentralAC = "centralac";
        public const string ApplianceComplex_HeatSystem = "heatsystem";
        public const string ApplianceComplex_OutsideWater = "outsidewater";
        public const string ApplianceComplex_Pool = "pool";
        public const string ApplianceComplex_SecondaryHeating = "secondaryheating";
        public const string ApplianceComplex_WaterHeater = "waterheater";
        public const string ApplianceComplex_WoodStove = "woodstove";

        public const string ApplianceBusinessComputers = "businesscomputers";
        public const string ApplianceBusinessCooking = "businesscooking";
        public const string ApplianceBusinessCooling = "businesscooling";
        public const string ApplianceBusinessHeating = "businessheating";
        public const string ApplianceBusinessLaundry = "businesslaundry";
        public const string ApplianceBusinessLighting = "businesslighting";
        public const string ApplianceBusinessMiscellaneous = "businessmisc";
        public const string ApplianceBusinessOfficeEquipment = "businessofficeequipment";
        public const string ApplianceBusinessRefrigeration = "businessrefrigeration";
        public const string ApplianceBusinessVentilation = "businessventilation";
        public const string ApplianceBusinessWaterHeating = "businesswaterheating";
    }
}
