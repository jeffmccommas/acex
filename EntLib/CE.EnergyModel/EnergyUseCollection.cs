﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using CE.EnergyModel.Enums;

namespace CE.EnergyModel
{
    public class EnergyUseCollection : Collection<EnergyUse>
    {

        public EnergyUseCollection()
            : base(new List<EnergyUse>())
        {

        }

        public EnergyUse Find(Predicate<EnergyUse> match)
        {
            List<EnergyUse> items = (List<EnergyUse>)Items;
            return (items.Find(match));
        }

        public bool Exists(Predicate<EnergyUse> match)
        {
            List<EnergyUse> items = (List<EnergyUse>)Items;
            return (items.Exists(match));
        }

        public int RemoveAll(Predicate<EnergyUse> match)
        {
            List<EnergyUse> items = (List<EnergyUse>)Items;
            return (items.RemoveAll(match));
        }


        /// <summary>
        ///  Find first item by commodity, test function.  Caller could just use the Find directly.
        /// </summary>
        /// <param name="commodity"></param>
        /// <returns></returns>
        public EnergyUse GetEnergyUseByCommodity(CommodityType commodity)
        {
            List<EnergyUse> items = (List<EnergyUse>)Items;
            return (items.Find(p => p.Commodity == commodity));
        }

        /// <summary>
        ///  Find first item by commodity and uom.
        /// </summary>
        /// <param name="commodity"></param>
        /// <param name="uom"></param>
        /// <returns></returns>
        public EnergyUse GetEnergyUseByCommodityAndUOM(CommodityType commodity, UnitOfMeasureType uom)
        {
            List<EnergyUse> items = (List<EnergyUse>)Items;
            return (items.Find(p => p.Commodity == commodity && p.UnitOfMeasure == uom));
        }


        /// <summary>
        /// Set the OriginalQuantity to the value of Quantity.  Used during bill disaggregation.
        /// </summary>
        public void SetOriginalQuantities()
        {
            List<EnergyUse> items = (List<EnergyUse>)Items;

            for (int i = 0; i < this.Count; i++)
            {
                items[i].OriginalQuantity = items[i].Quantity;
            }
        }

        /// <summary>
        /// Set the Quantity to the value of OriginalQuantity.  Used during bill disaggregation when the disaggregation fails, and original model quantities need to be reset.
        /// </summary>
        public void ResetQuantities()
        {
            List<EnergyUse> items = (List<EnergyUse>)Items;

            for (int i = 0; i < this.Count; i++)
            {
                items[i].Quantity = items[i].OriginalQuantity;
                items[i].Cost = 0.0m;
            }
        }

    }

}
