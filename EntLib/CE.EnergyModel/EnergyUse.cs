﻿using CE.EnergyModel.Enums;

namespace CE.EnergyModel
{
    /// <summary>
    /// EnergyUse.
    /// </summary>
    public class EnergyUse
    {
        public CommodityType Commodity { get; set; }
        public EvaluationContext EvaluationContext { get; set; }
        public decimal Quantity { get; set; }
        public decimal Cost { get; set; }
        public UnitOfMeasureType UnitOfMeasure { get; set; }
        public PeriodCollection Periods { get; set; }
        public string CommodityKey { get; set; }                //equivalent to commodity
        public string UomKey { get; set; }                      //equivalent to unitOfMeasure for usage quantity
        public string CurrencyKey { get; set; }                 //equivalent to currency for cost
        public decimal RecRatio { get; set; }                   //reconciliation ratio
        public decimal WeatherAdjustmentFactor { get; set; }    //weather adjustment, if applicable, used during bill disaggregation
        public decimal UnitCost { get; set; }                   //also known as everage cost, set during bill disaggregation
        public decimal OriginalQuantity { get; set; }           //used during bill disaggregation


        public EnergyUse()
        {
            RecRatio = 1.0m;
            WeatherAdjustmentFactor = 1.0m;
        }

        public EnergyUse(bool setContextComplete)
            : this()
        {
            EvaluationContext = new EvaluationContext(EvaluationType.Completed);
        }

        public EnergyUse(EvaluationType evalType)
            : this()
        {
            EvaluationContext = new EvaluationContext(evalType);
        }

        public EnergyUse(CommodityType commodity, EvaluationContext evalContext)
            : this()
        {
            Commodity = commodity;
            EvaluationContext = evalContext;
        }

        public EnergyUse(CommodityType commodity, UnitOfMeasureType uom, EvaluationContext evalContext)
            : this()
        {
            Commodity = commodity;
            UnitOfMeasure = uom;
            EvaluationContext = evalContext;
        }

        public EnergyUse(CommodityType commodity, UnitOfMeasureType uom, decimal quantity)
            : this()
        {
            Commodity = commodity;
            UnitOfMeasure = uom;
            Quantity = quantity;
        }

        public EnergyUse(bool createMonthlyPeriods, CommodityType commodity, UnitOfMeasureType uom, decimal quantity)
            : this()
        {
            Commodity = commodity;
            UnitOfMeasure = uom;
            Quantity = quantity;

            if (createMonthlyPeriods)
            {
                Periods = new PeriodCollection(12);
            }
        }

        public EnergyUse Clone()
        {
            var x = new EnergyUse();

            x.Commodity = this.Commodity;
            x.Quantity = this.Quantity;
            x.Cost = this.Cost;
            x.UnitOfMeasure = this.UnitOfMeasure;
            x.UomKey = this.UomKey;
            x.CommodityKey = this.CommodityKey;
            x.CurrencyKey = this.CurrencyKey;
            x.RecRatio = this.RecRatio;
            x.WeatherAdjustmentFactor = this.WeatherAdjustmentFactor;
            x.OriginalQuantity = this.OriginalQuantity;
            x.UnitCost = this.UnitCost;

            if (this.EvaluationContext != null)
            {
                x.EvaluationContext = new EvaluationContext();
                x.EvaluationContext.EvaluationType = this.EvaluationContext.EvaluationType;
                x.EvaluationContext.Expression = this.EvaluationContext.Expression;
            }

            if (this.Periods != null)
            {
                x.Periods = new PeriodCollection();
                foreach (var item in this.Periods)
                {
                    var newPeriod = item.Clone();
                    x.Periods.Add(newPeriod);
                }
            }

            return (x);
        }
    }
}
