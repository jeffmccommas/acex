﻿using System;

namespace CE.EnergyModel
{
    /// <summary>
    /// Partial.
    /// </summary>
    public partial class ApplianceEvaluator
    {

        private decimal GetIndoorTemperature(ProfileAttributeCollection pa, EnergyModelConfiguration config)
        {
            var indoorTemp = 0.0m;
            var avgCentralAcTemp = 0.0m;
            var avgHeatTemp = 0.0m;
            int zones = Convert.ToInt32(pa.Find(p => p.Key == "house.zones").OptionValue);

            // get central ac avg thermostat temperature
            decimal centralAcCount = System.Convert.ToDecimal(pa.Find(p => p.Key == "centralac.count").OptionValue);

            if (centralAcCount > 0)
            {
                decimal evtemp = 0;
                decimal daytemp = 0;
                decimal nitetemp = 0;

                int daylivngcooling = Convert.ToInt32(pa.Find(p => p.Key == "house.daylivngcooling").OptionValue);
                int evelivngcooling = Convert.ToInt32(pa.Find(p => p.Key == "house.evelivngcooling").OptionValue);
                int nitelivngcooling = Convert.ToInt32(pa.Find(p => p.Key == "house.nitelivngcooling").OptionValue);
                int daysleepcooling = Convert.ToInt32(pa.Find(p => p.Key == "house.daysleepcooling").OptionValue);
                int evesleepcooling = Convert.ToInt32(pa.Find(p => p.Key == "house.evesleepcooling").OptionValue);
                int nitesleepcooling = Convert.ToInt32(pa.Find(p => p.Key == "house.nitesleepcooling").OptionValue);

                //get average temperature
                switch (zones)
                {
                    case 1:
                        daytemp = daylivngcooling;
                        evtemp = evelivngcooling;
                        nitetemp = nitelivngcooling;
                        break;
                    default:
                        daytemp = (daylivngcooling + daysleepcooling) / 2;
                        evtemp = (evelivngcooling + evesleepcooling) / 2;
                        nitetemp = (nitelivngcooling + nitesleepcooling) / 2;
                        break;
                }
                // .46 for daytime setting from 6am to 4pm, .25 for evening setting from 5pm to 11pm, .29 for night setting from midnight to 5
                avgCentralAcTemp = daytemp * 0.46m + evtemp * .25m + nitetemp * .29m;

            }

            // get heat system thermostat avg temperature
            decimal heatSystemCount = System.Convert.ToDecimal(pa.Find(p => p.Key == "heatsystem.count").OptionValue);
            if (heatSystemCount > 0)
            {
                decimal evtemp = 0;
                decimal daytemp = 0;
                decimal nitetemp = 0;

                int daylivngheating = Convert.ToInt32(pa.Find(p => p.Key == "house.daylivngheating").OptionValue);
                int evelivngheating = Convert.ToInt32(pa.Find(p => p.Key == "house.evelivngheating").OptionValue);
                int nitelivngheating = Convert.ToInt32(pa.Find(p => p.Key == "house.nitelivngheating").OptionValue);
                int daysleepheating = Convert.ToInt32(pa.Find(p => p.Key == "house.daysleepheating").OptionValue);
                int evesleepheating = Convert.ToInt32(pa.Find(p => p.Key == "house.evesleepheating").OptionValue);
                int nitesleepheating = Convert.ToInt32(pa.Find(p => p.Key == "house.nitesleepheating").OptionValue);

                //get average temperature
                switch (zones)
                {
                    case 1:
                        daytemp = daylivngheating;
                        evtemp = evelivngheating;
                        nitetemp = nitelivngheating;
                        break;
                    default:
                        daytemp = (daylivngheating + daysleepheating) / 2;
                        evtemp = (evelivngheating + evesleepheating) / 2;
                        nitetemp = (nitelivngheating + nitesleepheating) / 2;
                        break;
                }
                // .46 for daytime setting from 6am to 4pm, .25 for evening setting from 5pm to 11pm, .29 for night setting from midnight to 5
                avgHeatTemp = daytemp * 0.46m + evtemp * .25m + nitetemp * .29m;
            }

            // get avg of heat and central ac thermostat
            if (heatSystemCount > 0 && centralAcCount > 0)
            {
                indoorTemp = (avgCentralAcTemp + avgHeatTemp) / 2;
            }
            else if (heatSystemCount > 0)
            {
                indoorTemp = avgHeatTemp;
            }
            else
            {
                indoorTemp = avgCentralAcTemp;
            }

            return indoorTemp;
        }
    }
}
