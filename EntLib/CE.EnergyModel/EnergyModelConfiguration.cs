﻿using System.Collections.Generic;
using System.ComponentModel;
using CE.EnergyModel.Entities;
using CE.EnergyModel.Exceptions;

namespace CE.EnergyModel
{

    /// <summary>
    /// This class is a container for client configurable calculation factors, attribute total counts, Profile Attribute Options, and Energy Use Intensity values for lookup.
    /// This also stores keys from content for commodities, uom, and currency.
    /// </summary>
    public class EnergyModelConfiguration
    {
        private Dictionary<string, string> _calculationFactors;
        private Dictionary<string, int> _applianceAttributeCounts;
        private Dictionary<string, decimal> _profileOptions;
        private Dictionary<string, string> _commodityKeys;
        private Dictionary<string, string> _uomKeys;
        private Dictionary<string, string> _currencyKeys;

        public List<BuildingEnergyUseIntensity> BuildingEuis { get; set; }


        public Dictionary<string, string> CalculationFactors
        {
            get
            {
                return (this._calculationFactors);
            }

        }

        public Dictionary<string, int> ApplianceAttributeCounts
        {
            get
            {
                return (this._applianceAttributeCounts);
            }

        }

        public Dictionary<string, decimal> ProfileOptions
        {
            get
            {
                return (this._profileOptions);
            }

        }

        public Dictionary<string, string> CommodityKeys
        {
            get
            {
                return (this._commodityKeys);
            }

        }

        public Dictionary<string, string> UomKeys
        {
            get
            {
                return (this._uomKeys);
            }

        }

        public Dictionary<string, string> CurrencyKeys
        {
            get
            {
                return (this._currencyKeys);
            }

        }

        public EnergyModelConfiguration()
        {
            _calculationFactors = new Dictionary<string, string>();
            _applianceAttributeCounts = new Dictionary<string, int>();
            _profileOptions = new Dictionary<string, decimal>();
            _commodityKeys = new Dictionary<string, string>();
            _uomKeys = new Dictionary<string, string>();
            _currencyKeys = new Dictionary<string, string>();
        }

        public EnergyModelConfiguration(Dictionary<string, string> calculationFactors)
        {
            _calculationFactors = calculationFactors;
            _profileOptions = new Dictionary<string, decimal>();
            _applianceAttributeCounts = new Dictionary<string, int>();
            _profileOptions = new Dictionary<string, decimal>();
            _commodityKeys = new Dictionary<string, string>();
            _uomKeys = new Dictionary<string, string>();
            _currencyKeys = new Dictionary<string, string>();
        }

        public EnergyModelConfiguration(Dictionary<string, string> calculationFactors, Dictionary<string, int> applianceAttrCounts)
        {
            _calculationFactors = calculationFactors;
            _applianceAttributeCounts = applianceAttrCounts;
            _profileOptions = new Dictionary<string, decimal>();
            _commodityKeys = new Dictionary<string, string>();
            _uomKeys = new Dictionary<string, string>();
            _currencyKeys = new Dictionary<string, string>();
        }

        public EnergyModelConfiguration(Dictionary<string, string> calculationFactors, Dictionary<string, int> applianceAttrCounts, Dictionary<string, decimal> profileOptions)
        {
            _calculationFactors = calculationFactors;
            _applianceAttributeCounts = applianceAttrCounts;
            _profileOptions = profileOptions;
            _commodityKeys = new Dictionary<string, string>();
            _uomKeys = new Dictionary<string, string>();
            _currencyKeys = new Dictionary<string, string>();
        }

        /// <summary>
        /// Allow a type convertor to more easily convert a factor setting to the appropriate type.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="key"></param>
        /// <returns></returns>
        public T GetCalculationFactor<T>(string key)
        {
            var factor = this._calculationFactors[key];
            if (string.IsNullOrWhiteSpace(factor)) throw new ClientCalculationFactorNotFoundException(key);

            var converter = TypeDescriptor.GetConverter(typeof(T));
            return (T)(converter.ConvertFromInvariantString(factor));
        }

        /// <summary>
        /// Helper for quick OptionValue retrieval.
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public decimal GetProfileOptionValue(string key)
        {
            decimal optionValue = 0m;
            
            decimal value;
            if (this._profileOptions.TryGetValue(key, out value))
            {
                optionValue = value;
            }

            return (optionValue);
        }

    }
}
