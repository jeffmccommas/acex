﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using CE.ContentModel;
using CE.EnergyModel.DataAccess;
using CE.EnergyModel.Entities;
using CE.EnergyModel.Enums;
using CE.EnergyModel.Exceptions;

namespace CE.EnergyModel
{
    /// <summary>
    /// Partial
    /// </summary>
    public partial class EnergyModelManager
    {
        private const string Exception_Message_ApplianceFailed = "Failed to execute energy model for appliance.";
        private const string Exception_Message_Timeout = "Energy Model timeout.";

        private const string Exception_Message_Params1 =
            "Error, cannot determine region parameters using the provided zipcode.";

        private const string Exception_Message_Params3 =
            "Error, cannot determine region parameters using the derived zipcode.";

        private const int MaxWaitEnergyModelInMs = 60000; // 60s
        private readonly int _clientId;

        private EnergyModelConfiguration _config;
        private IContentProvider _contentProvider;
        private Weather _weather;
        private int _cacheTimeoutInMinutes = 20; //typically set as 20, use 1 when testing
        private DataAccessLayer _dataAccessLayer;

        public EnergyModelManager(int clientId)
        {
            _clientId = clientId;
            InitDataAccess();
            InitContentProvider();
            LoadWeatherLookups();
            LoadConfiguration();
        }

        public EnergyModelManager(int clientId, string csInsightsDW, string csInsightsMetadata, string csInsights)
        {
            _clientId = clientId;
            InitDataAccess(csInsightsMetadata, csInsightsDW, csInsights);
            InitContentProvider(csInsightsMetadata);
            LoadWeatherLookups();
            LoadConfiguration();
        }

        public EnergyModelManager(int clientId, EnergyModelConfiguration config)
        {
            _clientId = clientId;
            _config = config;
            InitDataAccess();
            InitContentProvider();
            LoadWeatherLookups();
            LoadConfiguration();
        }

        public EnergyModelManager(int clientId, EnergyModelConfiguration config, Weather weather)
        {
            _clientId = clientId;
            _config = config;
            _weather = weather;
            InitDataAccess();
            InitContentProvider();
            LoadWeatherLookups();
            LoadConfiguration();
        }

        public EnergyModelManager(int clientId, EnergyModelConfiguration config, Weather weather,
            bool useMockContentProvider)
        {
            if (useMockContentProvider)
            {
                _clientId = clientId;
                _config = config;
                _weather = weather;
                InitDataAccess();
                InitContentProvider(ContentProviderType.Mock);
                LoadWeatherLookups();
                LoadConfiguration();
            }
        }

        /// <summary>
        /// Property to get a ClientID. Only the constructor can set this.
        /// </summary>
        public int ClientID => _clientId;

        /// <summary>
        /// Property to gain access to the manager configuration.  Only the constructor populate this.
        /// </summary>
        public EnergyModelConfiguration Configuration => _config;


        /// <summary>
        /// Execute Energy Model.
        /// </summary>
        /// <param name="zipcode">Derives region parameters from zipcode.</param>
        /// <returns></returns>
        public EnergyModelResult ExecuteEnergyModel(string zipcode)
        {
            var idParams = new IDParams(zipcode);
            var regionParams = GetRegionParams(idParams);

            var emiParams = new EnergyModelInputParams
            {
                RegionParams = regionParams,
                IdParams = idParams
            };

            return ExecuteEnergyModel(emiParams);
        }

        /// <summary>
        /// Execute Energy Model.
        /// </summary>
        /// <param name="idParams">Derive zipcode and then region parameters from idParams lookup via bill data.</param>
        /// <returns></returns>
        public EnergyModelResult ExecuteEnergyModel(IDParams idParams)
        {
            var regionParams = GetRegionParams(idParams);

            var emiParams = new EnergyModelInputParams()
            {
                RegionParams = regionParams,
                IdParams = idParams
            };

            return ExecuteEnergyModel(emiParams);
        }

        /// <summary>
        /// Execute Energy Model.
        /// </summary>
        /// <param name="idParams">Derive zipcode and then region parameters from idParams lookup via bill data.</param>
        /// <param name="profileAttributes">Will be merged into default profile attributes and customer profile attributes.</param>
        /// <param name="appliances">Will be merged into retrieved Appliance list.</param>
        /// <returns></returns>
        public EnergyModelResult ExecuteEnergyModel(IDParams idParams, ProfileAttributeCollection profileAttributes,
            ApplianceCollection appliances)
        {
            var regionParams = GetRegionParams(idParams);

            var emiParams = new EnergyModelInputParams()
            {
                RegionParams = regionParams,
                IdParams = idParams,
                ProfileAttributes = profileAttributes,
                Appliances = appliances
            };

            return ExecuteEnergyModel(emiParams);
        }

        /// <summary>
        /// Execute Energy Model.
        /// </summary>
        /// <param name="idParams">Derive zipcode and then region parameters from idParams lookup via bill data.</param>
        /// <param name="profileAttributes">Will be merged into default profile attributes and customer profile attributes.</param>
        /// <param name="appliances">May be merged with client appliance list.</param>
        /// <param name="endUses">May be merged with client enduse list.</param>
        /// <returns></returns>
        public EnergyModelResult ExecuteEnergyModel(IDParams idParams, ProfileAttributeCollection profileAttributes,
            ApplianceCollection appliances, EndUseCollection endUses)
        {
            var regionParams = GetRegionParams(idParams);

            var emiParams = new EnergyModelInputParams
            {
                RegionParams = regionParams,
                IdParams = idParams,
                ProfileAttributes = profileAttributes,
                Appliances = appliances,
                EndUses = endUses
            };

            return ExecuteEnergyModel(emiParams);
        }


        /// <summary>
        /// ExecuteEnergyModel.
        /// </summary>
        /// <param name="idParams"></param>
        /// <param name="extraParams"></param>
        /// <param name="profileAttributes"></param>
        /// <param name="appliances"></param>
        /// <param name="endUses"></param>
        /// <returns></returns>
        public EnergyModelResult ExecuteEnergyModel(IDParams idParams, ExtraParams extraParams,
            ProfileAttributeCollection profileAttributes, ApplianceCollection appliances, EndUseCollection endUses)
        {
            var regionParams = GetRegionParams(idParams);

            var emiParams = new EnergyModelInputParams
            {
                RegionParams = regionParams,
                IdParams = idParams,
                ExtraParams = extraParams,
                ProfileAttributes = profileAttributes,
                Appliances = appliances,
                EndUses = endUses
            };

            return ExecuteEnergyModel(emiParams);
        }


        /// <summary>
        /// Execute Energy Model.
        /// </summary>
        /// <param name="regionParams">Region parameters provided directly.</param>
        /// <returns></returns>
        public EnergyModelResult ExecuteEnergyModel(RegionParams regionParams)
        {
            var emiParams = new EnergyModelInputParams
            {
                RegionParams = regionParams
            };

            return ExecuteEnergyModel(emiParams);
        }

        /// <summary>
        /// Execute Energy Model.
        /// </summary>
        /// <param name="regionParams">Region parameters provided directly..</param>
        /// <param name="idParams">idParams provided so that profile attributes for the customer can be retrieved.</param>
        /// <returns></returns>
        public EnergyModelResult ExecuteEnergyModel(RegionParams regionParams, IDParams idParams)
        {
            var emiParams = new EnergyModelInputParams
            {
                RegionParams = regionParams,
                IdParams = idParams
            };

            return ExecuteEnergyModel(emiParams);
        }

        /// <summary>
        /// Execute Energy Model.
        /// </summary>
        /// <param name="regionParams">Region parameters.</param>
        /// <param name="idParams">Contains boolean that indicates to retrieve customer profile attributes.</param>
        /// <param name="profileAttributes">Will be merged into default profile attributes and customer profile attributes.</param>
        /// <param name="appliances">Will be merged into retrieved Appliance list.</param>
        /// <returns></returns>
        public EnergyModelResult ExecuteEnergyModel(RegionParams regionParams, IDParams idParams,
            ProfileAttributeCollection profileAttributes, ApplianceCollection appliances)
        {
            var emiParams = new EnergyModelInputParams
            {
                RegionParams = regionParams,
                IdParams = idParams,
                ProfileAttributes = profileAttributes,
                Appliances = appliances
            };

            return ExecuteEnergyModel(emiParams);
        }

        /// <summary>
        /// Execute Energy Model.
        /// </summary>
        /// <param name="regionParams">Region parameters.</param>
        /// <param name="idParams">Contains boolean that indicates to retrieve customer profile attributes.</param>
        /// <param name="profileAttributes">Will be merged into default profile attributes and customer profile attributes.</param>
        /// <param name="appliances">Will be merged into retrieved Appliance list.</param>
        /// <param name="endUses">End Uses.</param>
        /// <returns></returns>
        public EnergyModelResult ExecuteEnergyModel(RegionParams regionParams, IDParams idParams,
            ProfileAttributeCollection profileAttributes, ApplianceCollection appliances, EndUseCollection endUses)
        {
            var emiParams = new EnergyModelInputParams
            {
                RegionParams = regionParams,
                IdParams = idParams,
                ProfileAttributes = profileAttributes,
                Appliances = appliances,
                EndUses = endUses
            };

            return ExecuteEnergyModel(emiParams);
        }


        /// <summary>
        /// Execute Energy Model.
        /// </summary>
        /// <param name="regionParams"></param>
        /// <param name="idParams"></param>
        /// <param name="extraParams"></param>
        /// <param name="profileAttributes"></param>
        /// <param name="appliances"></param>
        /// <param name="endUses"></param>
        /// <returns></returns>
        public EnergyModelResult ExecuteEnergyModel(RegionParams regionParams, IDParams idParams,
            ExtraParams extraParams, ProfileAttributeCollection profileAttributes, ApplianceCollection appliances,
            EndUseCollection endUses)
        {

            // get the station id for the region if it's not available by zipcode/default zipcode
            if (regionParams.StationId == null)
            {
                var defaultRegionParams = GetRegionParams(idParams ?? new IDParams(string.Empty));
                regionParams.StationId = defaultRegionParams.StationId;
                regionParams.DefaultProfileName = defaultRegionParams.DefaultProfileName;

            }
            var emiParams = new EnergyModelInputParams
            {
                RegionParams = regionParams,
                IdParams = idParams,
                ExtraParams = extraParams,
                ProfileAttributes = profileAttributes,
                Appliances = appliances,
                EndUses = endUses
            };

            return ExecuteEnergyModel(emiParams);
        }

        /// <summary>
        /// Execute Energy Model based upon input data.
        /// </summary>
        /// <param name="energyModelInputParams">Energy model input parameters</param>
        /// <returns></returns>
        private EnergyModelResult ExecuteEnergyModel(EnergyModelInputParams energyModelInputParams)
        {
            var isBusiness = false;

            energyModelInputParams.ClientId = _clientId;
            energyModelInputParams.Configuration = _config;

            if (energyModelInputParams.ExtraParams == null)
            {
                energyModelInputParams.ExtraParams = new ExtraParams();
            }
            if (energyModelInputParams.Appliances == null)
            {
                energyModelInputParams.Appliances = new ApplianceCollection();
            }
            if (energyModelInputParams.EndUses == null)
            {
                energyModelInputParams.EndUses = new EndUseCollection();
            }
            if (energyModelInputParams.ProfileAttributes == null)
            {
                energyModelInputParams.ProfileAttributes = new ProfileAttributeCollection();
            }

            //Initialize some data to be used by appliance calculations
            SetWeatherParameters(energyModelInputParams);

            //Profile Attributes handling; in stand-alone, the exact profile attributes provided are used as-is;
            if (!energyModelInputParams.ProfileAttributes.IsStandAlone)
            {
                //Since not stand-alone, get default Profile Attributes from content, overlayed automatically for appliance region
                var allProfileAttributes =
                    GetDefaultProfileAttributes(energyModelInputParams.RegionParams.ApplianceRegionId,
                        energyModelInputParams.RegionParams.DefaultProfileName);

                //Since not stand-alone, and IDParams exist, get customer specific Profile attributes;
                var custProfileAttributes = GetCustomerProfileAttributes(energyModelInputParams.IdParams);

                //Merge by replacing matching default profile attributes with customer profile attributes, and then with those passed-in.
                MergeProfileAttributes(energyModelInputParams.ProfileAttributes, allProfileAttributes,
                    custProfileAttributes);
            }


            // special consideration for business modeling; first check and see if it is forced business model calculation 
            // via billDisagg as origin of caller
            if (energyModelInputParams.ExtraParams.IsBusiness)
            {
                isBusiness = true;
            }
            else
            {
                // now check and see if there is a profile attribute that indicates the premisetype (typical way of doing it)
                if (energyModelInputParams.ProfileAttributes.Exists(pa => pa.Key == "premisetype"))
                {
                    var optionKey =
                        energyModelInputParams.ProfileAttributes.Find(pa => pa.Key == "premisetype").OptionKey;
                    if (optionKey == "premisetype.business")
                    {
                        isBusiness = true;
                        energyModelInputParams.ExtraParams.IsBusiness = true;
                    }
                }
            }

            //Appliance handling
            var allAppliances = GetClientAppliances(isBusiness);
            if (energyModelInputParams.Appliances.IsStandAlone && energyModelInputParams.Appliances.Count > 0)
            {
                var applianceNames = energyModelInputParams.Appliances.GetStandAloneApplianceNames();
                allAppliances = GetStandAloneAppliances(applianceNames, allAppliances);
            }
            MergeAppliances(energyModelInputParams.Appliances, allAppliances);


            //EndUse handling; in stand-alone, the exact end uses provided are used as-is;
            if (!energyModelInputParams.EndUses.IsStandAlone)
            {
                var allEndUses = GetClientEndUses(isBusiness);
                MergeEndUses(energyModelInputParams.EndUses, allEndUses);
            }


            return MainCalculateEnergyModel(energyModelInputParams);
        }



        /// <summary>
        /// Private entry point of calculation logic.
        /// </summary>
        /// <param name="inputParams"></param>
        /// <returns></returns>
        private EnergyModelResult MainCalculateEnergyModel(EnergyModelInputParams inputParams)
        {
            var result = new EnergyModelResult
            {
                RegionParams =
                {
                    TemperatureRegionId = inputParams.RegionParams.TemperatureRegionId,
                    SolarRegionId = inputParams.RegionParams.SolarRegionId,
                    ApplianceRegionId = inputParams.RegionParams.ApplianceRegionId,
                    State = inputParams.RegionParams.State,
                    StationId = inputParams.RegionParams.StationId,
                    ClimateId = inputParams.RegionParams.ClimateId,
                    CountryCode = inputParams.RegionParams.CountryCode
                },
                AnnualHDD = inputParams.WeatherInfo.AnnualHDD,
                AnnualCDD = inputParams.WeatherInfo.AnnualCDD
            };

            if (inputParams.IdParams != null)
            {
                if (result.IDParams == null) result.IDParams = new IDParams();
                result.IDParams.CustomerId = inputParams.IdParams.CustomerId;
                result.IDParams.AccountId = inputParams.IdParams.AccountId;
                result.IDParams.PremiseId = inputParams.IdParams.PremiseId;
                result.IDParams.Zipcode = inputParams.IdParams.Zipcode;
            }
            if (inputParams.ExtraParams.IsBusiness) result.ModelType = ModelType.Business;

            var sw = Stopwatch.StartNew();

            var task = MainCalculateEnergyModelAsync(result, inputParams);
            var completedWithinAllotedTime = task.Wait(MaxWaitEnergyModelInMs);
                // hard wait; needed to block somewhere in synchronous code.

            sw.Stop();

            if (!completedWithinAllotedTime)
            {
                result.StatusList.Add(new Status(new EnergyModelTimeoutException(Exception_Message_Timeout),
                    StatusType.Error));
            }

            result.Metrics.Add(new Metric("Calculations", sw.ElapsedMilliseconds));

            return result;
        }


        private async Task MainCalculateEnergyModelAsync(EnergyModelResult result, EnergyModelInputParams input)
        {
            try
            {

                await CoreCalculationsAsync(result, input).ConfigureAwait(false);

                FinalizeResults(result, input);

            }
            catch (Exception ex)
            {
                result.StatusList.Add(new Status(ex, StatusType.Error));
            }
        }

        private void FinalizeResults(EnergyModelResult result, EnergyModelInputParams input)
        {
            result.Appliances.Sort((x, y) => x.Name.CompareTo(y.Name));

            bool includeMonths = input.ExtraParams.OutputMonthlyEnergyUse;

            // Populate Result TotalEnergyUses; sum all energy uses in appliances; dual uom output of btu for several appliances are not summed.
            // FUTURE: monthly energy uses for periods creating placeholder periods, but not summing yet
            var totalSums = result.Appliances.GetCommoditySums();
            if (totalSums[(int) CommodityType.Electric] != 0m)
            {
                result.TotalEnergyUses.Add(new EnergyUse(includeMonths, CommodityType.Electric, UnitOfMeasureType.kWh,
                    totalSums[(int) CommodityType.Electric]));
            }
            if (totalSums[(int) CommodityType.Gas] != 0m)
            {
                result.TotalEnergyUses.Add(new EnergyUse(includeMonths, CommodityType.Gas, UnitOfMeasureType.Therms,
                    totalSums[(int) CommodityType.Gas]));
            }
            if (totalSums[(int) CommodityType.Water] != 0m)
            {
                result.TotalEnergyUses.Add(new EnergyUse(includeMonths, CommodityType.Water, UnitOfMeasureType.Gal,
                    totalSums[(int) CommodityType.Water]));
            }
            if (totalSums[(int) CommodityType.Propane] != 0m)
            {
                result.TotalEnergyUses.Add(new EnergyUse(includeMonths, CommodityType.Propane, UnitOfMeasureType.Gal,
                    totalSums[(int) CommodityType.Propane]));
            }
            if (totalSums[(int) CommodityType.Oil] != 0m)
            {
                result.TotalEnergyUses.Add(new EnergyUse(includeMonths, CommodityType.Oil, UnitOfMeasureType.Gal,
                    totalSums[(int) CommodityType.Oil]));
            }
            if (totalSums[(int) CommodityType.Wood] != 0m)
            {
                result.TotalEnergyUses.Add(new EnergyUse(includeMonths, CommodityType.Wood, UnitOfMeasureType.BTU,
                    totalSums[(int) CommodityType.Wood]));
            }
            if (totalSums[(int) CommodityType.Coal] != 0m)
            {
                result.TotalEnergyUses.Add(new EnergyUse(includeMonths, CommodityType.Coal, UnitOfMeasureType.BTU,
                    totalSums[(int) CommodityType.Coal]));
            }


            // Populate Confidence value in each appliance calculation, and the overall average confidence
            int overallConfSum = 0;
            int overallConfCount = 0;
            foreach (var a in result.Appliances)
            {
                if (
                    a.EnergyUses.Exists(
                        p =>
                            p.EvaluationContext.EvaluationType == EvaluationType.Completed ||
                            p.EvaluationContext.EvaluationType == EvaluationType.Provided))
                {
                    int totalCount = 1;
                    var name = a.Name.ToLower();
                    var res = input.Configuration.ApplianceAttributeCounts.TryGetValue(name, out totalCount);
                    if (!res)
                    {
                        totalCount = 1;
                    }
                    //([0.1 + (0.8 * (userAttr/totalAttr))) * 100;
                    a.Confidence =
                        (int)
                        (((((double) input.ProfileAttributes.GetAttributeCountByApplianceAndDerivedSource(name)/
                            (double) totalCount)*0.8) + 0.1)*100);

                    overallConfCount++;
                    overallConfSum = overallConfSum + a.Confidence;

                }
            }
            if (overallConfCount > 0) result.Confidence = (int) ((double) overallConfSum/(double) overallConfCount);


            // Populate Result EndUses; create end uses in the result with summed quantities via appliances; determine average confidence of end use as well
            foreach (var e in input.EndUses)
            {
                var resultEnduse = new EndUse()
                {
                    Name = e.Name,
                    Key = e.Key,
                    Category = e.Category
                };

                if (e.ApplianceNames != null)
                {
                    resultEnduse.ApplianceNames = new List<string>();
                    foreach (var item in e.ApplianceNames)
                    {
                        resultEnduse.ApplianceNames.Add(item);
                    }
                }

                int confSum = 0;
                int confCount = 0;
                decimal[] energyUseByCommodity = {0m, 0m, 0m, 0m, 0m, 0m, 0m, 0m, 0m, 0m, 0m, 0m};
                    // indexed by commodity enum
                foreach (var name in resultEnduse.ApplianceNames)
                {
                    var a = result.Appliances.Find(p => p.Name == name);
                    if (a != null)
                    {
                        foreach (var eu in a.EnergyUses)
                        {
                            // do not sum unit of measure output that is BTU, or if it is wood (since wood must be btu)
                            if (eu.UnitOfMeasure != Enums.UnitOfMeasureType.BTU ||
                                eu.Commodity == Enums.CommodityType.Wood)
                            {
                                energyUseByCommodity[(int) eu.Commodity] = energyUseByCommodity[(int) eu.Commodity] +
                                                                           eu.Quantity;
                            }

                        }
                        confCount++;
                        confSum = confSum + a.Confidence;
                    }
                }
                if (confCount > 0) resultEnduse.Confidence = (int) ((double) confSum/(double) confCount);

                int existsCount = 0;
                // FUTURE: end use specific monthly energy uses for periods is creating placeholder periods, but not summing yet
                if (energyUseByCommodity[(int) CommodityType.Electric] != 0m)
                {
                    existsCount++;
                    resultEnduse.EnergyUses.Add(new EnergyUse(includeMonths, CommodityType.Electric,
                        UnitOfMeasureType.kWh, energyUseByCommodity[(int) CommodityType.Electric]));
                }
                if (energyUseByCommodity[(int) CommodityType.Gas] != 0m)
                {
                    existsCount++;
                    resultEnduse.EnergyUses.Add(new EnergyUse(includeMonths, CommodityType.Gas, UnitOfMeasureType.Therms,
                        energyUseByCommodity[(int) CommodityType.Gas]));
                }
                if (energyUseByCommodity[(int) CommodityType.Water] != 0m)
                {
                    existsCount++;
                    resultEnduse.EnergyUses.Add(new EnergyUse(includeMonths, CommodityType.Water, UnitOfMeasureType.Gal,
                        energyUseByCommodity[(int) CommodityType.Water]));
                }
                if (energyUseByCommodity[(int) CommodityType.Propane] != 0m)
                {
                    existsCount++;
                    resultEnduse.EnergyUses.Add(new EnergyUse(includeMonths, CommodityType.Propane,
                        UnitOfMeasureType.Gal, energyUseByCommodity[(int) CommodityType.Propane]));
                }
                if (energyUseByCommodity[(int) CommodityType.Oil] != 0m)
                {
                    existsCount++;
                    resultEnduse.EnergyUses.Add(new EnergyUse(includeMonths, CommodityType.Oil, UnitOfMeasureType.Gal,
                        energyUseByCommodity[(int) CommodityType.Oil]));
                }
                if (energyUseByCommodity[(int) CommodityType.Wood] != 0m)
                {
                    existsCount++;
                    resultEnduse.EnergyUses.Add(new EnergyUse(includeMonths, CommodityType.Wood, UnitOfMeasureType.BTU,
                        energyUseByCommodity[(int) CommodityType.Wood]));
                }
                if (energyUseByCommodity[(int) CommodityType.Coal] != 0m)
                {
                    existsCount++;
                    resultEnduse.EnergyUses.Add(new EnergyUse(includeMonths, CommodityType.Coal, UnitOfMeasureType.BTU,
                        energyUseByCommodity[(int) CommodityType.Coal]));
                }

                // only include end use if there are commodities present.  This means the output could have zero end uses in some cases
                if (existsCount > 0)
                {
                    result.EndUses.Add(resultEnduse);
                }

            }

            // Map to enums to CommodityKeys, CurrencyKeys, and EnduseKeys for possible use by callers
            MapEnumsToKeys(result, input);

            // Basically assign the result pointer to profile attributes from input (not a copy), this is in case a calling interface wants to iterate through the attributes used
            result.ProfileAttributes = input.ProfileAttributes;

        }


        /// <summary>
        /// Map to enums to CommodityKeys, CurrencyKeys, and EnduseKeys.
        /// </summary>
        /// <param name="result"></param>
        /// <param name="input"></param>
        private void MapEnumsToKeys(EnergyModelResult result, EnergyModelInputParams input)
        {
            // Now iterate all energy uses within appliances and end uses and assign all CommodityKeys, CurrencyKeys, and EnduseKeys
            foreach (var a in result.Appliances)
            {
                foreach (var eg in a.EnergyUses)
                {
                    string key;
                    if (input.Configuration.CommodityKeys.TryGetValue(eg.Commodity.ToString().ToLower(), out key))
                    {
                        eg.CommodityKey = key;
                    }
                    else
                    {
                        eg.CommodityKey = "unknown";
                    }

                    if (input.Configuration.UomKeys.TryGetValue(eg.UnitOfMeasure.ToString().ToLower(), out key))
                    {
                        eg.UomKey = key;
                    }
                    else
                    {
                        eg.UomKey = "unknown";
                    }

                    if (input.Configuration.CurrencyKeys.TryGetValue("usd", out key))
                    {
                        eg.CurrencyKey = key;
                    }
                    else
                    {
                        eg.CurrencyKey = "unknown";
                    }

                }
            }

            foreach (var eu in result.EndUses)
            {
                foreach (var eg in eu.EnergyUses)
                {
                    string key;
                    if (input.Configuration.CommodityKeys.TryGetValue(eg.Commodity.ToString().ToLower(), out key))
                    {
                        eg.CommodityKey = key;
                    }
                    else
                    {
                        eg.CommodityKey = "unknown";
                    }

                    if (input.Configuration.UomKeys.TryGetValue(eg.UnitOfMeasure.ToString().ToLower(), out key))
                    {
                        eg.UomKey = key;
                    }
                    else
                    {
                        eg.UomKey = "unknown";
                    }

                    if (input.Configuration.CurrencyKeys.TryGetValue("usd", out key))
                    {
                        eg.CurrencyKey = key;
                    }
                    else
                    {
                        eg.CurrencyKey = "unknown";
                    }
                }
            }

        }


        /// <summary>
        /// Get Regional Parameters.
        /// </summary>
        /// <param name="idParams">The ID Parameters</param>
        /// <returns>The Regional Parameters.</returns>
        private RegionParams GetRegionParams(IDParams idParams)
        {
            RegionParams regionParams;
            var config = _config; //factors and special common properties

            if (!string.IsNullOrEmpty(idParams.Zipcode))
            {
                // Use provided zipcode to get region parameters
                regionParams = _dataAccessLayer.GetRegionParametersByClientZipcode(ClientID, idParams.Zipcode);

                if (regionParams == null)
                {
                    throw new Exception(Exception_Message_Params1);
                }
            }
            else
            {
                if (!string.IsNullOrEmpty(idParams.CustomerId) && !string.IsNullOrEmpty(idParams.AccountId) &&
                    !string.IsNullOrEmpty(idParams.PremiseId))
                {
                    // Use customer data to get zipcode and then region parameters
                    var zipcode = _dataAccessLayer.GetZipcodeByCustomer(ClientID, idParams.CustomerId,
                        idParams.AccountId, idParams.PremiseId);

                    if (string.IsNullOrEmpty(zipcode))
                    {
                        zipcode = config.GetCalculationFactor<string>("defaultzipcode");
                    }

                    idParams.Zipcode = zipcode;

                    regionParams = _dataAccessLayer.GetRegionParametersByClientZipcode(ClientID, idParams.Zipcode);
                    if (regionParams == null)
                    {
                        throw new Exception(Exception_Message_Params3);
                    }

                }
                else
                {
                    // Use default zipcode straight away
                    idParams.Zipcode = config.GetCalculationFactor<string>("defaultzipcode");

                    regionParams = _dataAccessLayer.GetRegionParametersByClientZipcode(ClientID, idParams.Zipcode);

                    if (regionParams == null)
                    {
                        throw new Exception(Exception_Message_Params3);
                    }
                }

            }

            // Lookup DefaultProfileName for use when getting default profile attributes
            var dpn = _dataAccessLayer.GetDefaultProfileName(ClientID, regionParams.ApplianceRegionId);
            if (!string.IsNullOrWhiteSpace(dpn))
            {
                regionParams.DefaultProfileName = dpn;
            }


            return regionParams;
        }


        /// <summary>
        /// Get absolute default profile attributes for the appliance region.  
        /// The content engine shall select aclara default and override with applianceRegionId.
        /// </summary>
        /// <returns></returns>
        private ProfileAttributeCollection GetDefaultProfileAttributes(int applianceRegionId,
            string profileCollectionName)
        {
            ProfileAttributeCollection resultAttributes = new ProfileAttributeCollection();

            try
            {
                var cacheKey = "CDefProfileAttr_" + ClientID + "_" + applianceRegionId;

                var att = CE.EnergyModel.Cache.CacheLayer.Get<ProfileAttributeCollection>(cacheKey);

                if (att == null)
                {
                    //Get the client profile default list of profile attributes.  
                    var defaultList = _contentProvider.GetContentProfileDefault(string.Empty, profileCollectionName);

                    resultAttributes = new ProfileAttributeCollection();

                    foreach (var item in defaultList)
                    {
                        var pa = new ProfileAttribute()
                        {
                            Source = item.ContentSource,
                            Key = item.ProfileAttributeKey,
                            OptionKey = item.DefaultKey
                        };

                        if (string.IsNullOrWhiteSpace(item.DefaultValue))
                        {
                            // null values need to be set properly in content, these are typically enumerations that are strings, and have default values such as "1.123"
                            decimal value;
                            if (_config.ProfileOptions.TryGetValue(item.DefaultKey, out value))
                            {
                                pa.OptionValue = value.ToString();
                            }
                            else
                            {
                                pa.OptionValue = "0";
                            }
                        }
                        else if (item.DefaultValue == "TRUE" || item.DefaultValue == "true")
                        {
                            // booleans are strings in caps, the code expects "1" or "0" that is converted to integer and then bool
                            pa.OptionValue = "1";
                        }
                        else if (item.DefaultValue == "FALSE" || item.DefaultValue == "false")
                        {
                            // booleans are strings in caps, the code expects "1" or "0" that is converted to integer and then bool
                            pa.OptionValue = "0";
                        }
                        else
                        {
                            pa.OptionValue = item.DefaultValue;
                        }

                        resultAttributes.Add(pa);
                    }

                    CE.EnergyModel.Cache.CacheLayer.Add(resultAttributes, cacheKey, _cacheTimeoutInMinutes);

                }
                else
                {
                    resultAttributes = att;
                }

            }
            catch
            {
                throw;
            }

            return (resultAttributes);
        }


        /// <summary>
        /// Get a customer's profile attributes if possible.
        /// </summary>
        /// <param name="idParams"></param>
        /// <returns></returns>
        private ProfileAttributeCollection GetCustomerProfileAttributes(IDParams idParams)
        {
            ProfileAttributeCollection resultAttributes = null;

            if (idParams != null)
            {
                if (!string.IsNullOrEmpty(idParams.CustomerId) &&
                    !string.IsNullOrEmpty(idParams.AccountId) &&
                    !string.IsNullOrEmpty(idParams.PremiseId))
                {
                    resultAttributes = this._dataAccessLayer.GetProfileAttributes(this.ClientID, idParams.CustomerId,
                        idParams.AccountId, idParams.PremiseId);

                    if (resultAttributes != null)
                    {
                        //look for ProfileOptions that need to be filled-in from the lookup dictionary
                        foreach (var item in resultAttributes)
                        {
                            // skip straight-up values and booleans
                            if (!item.OptionKey.EndsWith(".value") && !item.OptionKey.EndsWith(".no") &&
                                !item.OptionKey.EndsWith(".yes"))
                            {
                                decimal value;
                                if (_config.ProfileOptions.TryGetValue(item.OptionKey, out value))
                                {
                                    item.OptionValue = value.ToString();
                                }
                            }
                        }
                    }
                }

            }

            return (resultAttributes);
        }


        /// <summary>
        /// Client list of appliances, with caching.
        /// </summary>
        /// <returns></returns>
        private ApplianceCollection GetClientAppliances(bool isBusiness)
        {
            ApplianceCollection resAppliances = null;

            var cacheBusRes = "Res";
            if (isBusiness)
            {
                cacheBusRes = "Bus";
            }

            // then get appliances and add expressions by lookup as needed
            try
            {
                var cacheKey = "CM_Apl_" + ClientID + "_" + cacheBusRes;
                var c = CE.EnergyModel.Cache.CacheLayer.Get<ApplianceCollection>(cacheKey);

                if (c == null)
                {
                    List<CE.ContentModel.Entities.ClientExpression> resExpressions = null;

                    //need expressions to lookup and add to the appliance collection when appropriate below
                    try
                    {
                        var expCacheKey = "CM_Exp_" + ClientID;
                        var e =
                            CE.EnergyModel.Cache.CacheLayer.Get<List<CE.ContentModel.Entities.ClientExpression>>(
                                expCacheKey);

                        if (e == null)
                        {
                            var expressionList = _contentProvider.GetContentExpression(string.Empty);
                            CE.EnergyModel.Cache.CacheLayer.Add(expressionList, expCacheKey, _cacheTimeoutInMinutes);
                            resExpressions = expressionList;
                        }
                        else
                        {
                            resExpressions = e;
                        }

                    }
                    catch
                    {
                        throw;
                    }

                    // only want appliance that are business or residential
                    var defaultList = !isBusiness
                        ? _contentProvider.GetContentAppliance(string.Empty).FindAll(xx => !xx.Key.Contains("business"))
                        : _contentProvider.GetContentAppliance(string.Empty).FindAll(xx => xx.Key.Contains("business"));

                    resAppliances = new ApplianceCollection();

                    foreach (var item in defaultList)
                    {
                        if (item.Key != "house") //exclude the placeholder for house
                        {
                            var a = new Appliance()
                            {
                                Name = item.Key,
                                Key = item.Key
                            };

                            var expressions = item.Expressions;
                            if (!string.IsNullOrWhiteSpace(expressions))
                            {
                                string[] expList = expressions.Split(';');

                                foreach (string exp in expList)
                                {
                                    if (resExpressions.Exists(p => p.Key == exp))
                                    {
                                        var ctx = new EvaluationContext(EvaluationType.Pending);
                                        ctx.Expression = resExpressions.Find(p => p.Key == exp).Formula;

                                        var energyUse = new EnergyUse()
                                        {
                                            EvaluationContext = ctx,
                                            Quantity = 0.0m
                                        };

                                        if (exp.Contains("electric"))
                                        {
                                            energyUse.Commodity = CommodityType.Electric;
                                            energyUse.UnitOfMeasure = UnitOfMeasureType.kWh;
                                        }
                                        else if (exp.Contains("water"))
                                        {
                                            energyUse.Commodity = CommodityType.Water;
                                            energyUse.UnitOfMeasure = UnitOfMeasureType.Gal;
                                        }
                                        else if (exp.Contains("gas"))
                                        {
                                            energyUse.Commodity = CommodityType.Gas;
                                            energyUse.UnitOfMeasure = UnitOfMeasureType.Therms;
                                        }
                                        else if (exp.Contains("propane"))
                                        {
                                            energyUse.Commodity = CommodityType.Propane;
                                            energyUse.UnitOfMeasure = UnitOfMeasureType.Gal;
                                        }
                                        else if (exp.Contains("oil"))
                                        {
                                            energyUse.Commodity = CommodityType.Oil;
                                            energyUse.UnitOfMeasure = UnitOfMeasureType.Gal;
                                        }
                                        else if (exp.Contains("wood"))
                                        {
                                            energyUse.Commodity = CommodityType.Wood;
                                            energyUse.UnitOfMeasure = UnitOfMeasureType.BTU;
                                        }
                                        else if (exp.Contains("coal"))
                                        {
                                            energyUse.Commodity = CommodityType.Coal;
                                            energyUse.UnitOfMeasure = UnitOfMeasureType.BTU;
                                        }

                                        a.EnergyUses.Add(energyUse);
                                    }

                                }
                            }

                            resAppliances.Add(a);
                        }

                    }

                    CE.EnergyModel.Cache.CacheLayer.Add(resAppliances, cacheKey, _cacheTimeoutInMinutes);

                }
                else
                {
                    resAppliances = c;
                }

            }
            catch
            {
                throw;
            }


            return (resAppliances);
        }


        /// <summary>
        /// Get a list of stand-alone appliances from the full list, filtered to include only those named, using a predicate builder.
        /// </summary>
        /// <param name="names"></param>
        /// <param name="allAppliances"></param>
        /// <returns></returns>
        private ApplianceCollection GetStandAloneAppliances(List<string> names, ApplianceCollection allAppliances)
        {
            ApplianceCollection resAppliances = null;
            string filter = string.Empty;

            var predicateAppliance = PredicateBuilder.False<Appliance>();

            if (names == null)
            {
                names = new List<string>();
            }
            foreach (var name in names)
            {
                string keyword = name.ToLower();
                predicateAppliance = predicateAppliance.Or(p => p.Name.ToLower() == keyword);
            }

            var someAppliances = (from aps in allAppliances.AsQueryable().Where(predicateAppliance)
                select aps).ToList();

            resAppliances = new ApplianceCollection(someAppliances);

            return (resAppliances);
        }


        /// <summary>
        /// Merge the main client list of appliances into the input parameter list.
        /// </summary>
        /// <param name="target"></param>
        /// <param name="source"></param>
        private void MergeAppliances(ApplianceCollection target, ApplianceCollection source)
        {
            foreach (var sa in source)
            {
                if (!target.Exists(p => p.Name.ToLower() == sa.Name.ToLower()))
                {
                    //clone and add appliance to target
                    target.Add(sa.Clone());
                }
                else
                {
                    //target already has the appliance; 
                    var targetAppliance = target.Find(p => p.Name.ToLower() == sa.Name.ToLower());

                    //if target does not have one or more Energy Uses, then remove it from the target and add the appliance from the source;
                    if (targetAppliance != null && targetAppliance.EnergyUses.Count == 0)
                    {
                        target.RemoveAt(target.IndexOf(targetAppliance));
                        target.Add(sa.Clone());
                    }

                }
            }

        }


        /// <summary>
        /// Client list of end uses
        /// </summary>
        /// <returns></returns>
        private EndUseCollection GetClientEndUses(bool isBusiness)
        {
            EndUseCollection resEnduses = null;

            var cacheBusRes = "Res";
            if (isBusiness)
            {
                cacheBusRes = "Bus";
            }

            try
            {
                var cacheKey = "CEndUses_" + ClientID + "_" + cacheBusRes;

                var c = CE.EnergyModel.Cache.CacheLayer.Get<EndUseCollection>(cacheKey);

                if (c == null)
                {

                    // only want end uses that are business or residential
                    var defaultList = !isBusiness
                        ? _contentProvider.GetContentEnduse(string.Empty).FindAll(xx => xx.Category != "business")
                        : _contentProvider.GetContentEnduse(string.Empty).FindAll(xx => xx.Category == "business");

                    resEnduses = new EndUseCollection();

                    foreach (var item in defaultList)
                    {
                        if (item.Key != "enduse.house") //exclude the placeholder for house
                        {
                            var e = new EndUse()
                            {
                                Name = item.Key.Replace("enduse.", ""), //NEW, strip the prefix!
                                Key = item.Key,
                                Category = item.Category
                            };

                            var applianceCsv = item.Appliances;
                            if (!string.IsNullOrWhiteSpace(applianceCsv))
                            {
                                e.ApplianceNames = new List<string>();

                                var csvList = applianceCsv.Split(';');

                                foreach (var a in csvList)
                                {
                                    if (a != "house")
                                        //exclude the placeholder for appliance house (not the same as enduse.house above!)
                                    {
                                        e.ApplianceNames.Add(a);
                                    }
                                }
                            }

                            resEnduses.Add(e);
                        }
                    }

                    CE.EnergyModel.Cache.CacheLayer.Add(resEnduses, cacheKey, _cacheTimeoutInMinutes);

                }
                else
                {
                    resEnduses = c;
                }

            }
            catch
            {
                throw;
            }

            return (resEnduses);
        }


        /// <summary>
        /// Merge the client list of end use with anything that may have been passed.
        /// </summary>
        /// <param name="target"></param>
        /// <param name="source"></param>
        private void MergeEndUses(EndUseCollection target, EndUseCollection source)
        {
            foreach (var sa in source)
            {
                if (!target.Exists(p => p.Name.ToLower() == sa.Name.ToLower()))
                {
                    //clone and add end use to target
                    target.Add(sa.Clone());
                }
                else
                {
                    //target already has the end use; 
                    var targetEndUse = target.Find(p => p.Name.ToLower() == sa.Name.ToLower());

                    //add any appliances from the 
                    if (targetEndUse != null)
                    {
                        //if targetEndUse does NOT have a category, then set it to the source category
                        if (string.IsNullOrEmpty(targetEndUse.Category))
                        {
                            targetEndUse.Category = sa.Category;
                        }

                        //check each item from sa, and add those that do not exist in target here.
                        if (sa.ApplianceNames != null)
                        {
                            foreach (var a in sa.ApplianceNames)
                            {
                                if (!targetEndUse.ApplianceNames.Contains(a))
                                {
                                    if (targetEndUse.ApplianceNames == null)
                                    {
                                        targetEndUse.ApplianceNames = new List<string>();
                                    }
                                    targetEndUse.ApplianceNames.Add(a);
                                }
                            }
                        }
                    }
                }
            }

        }


        /// <summary>
        /// Merge the customer profile attributes into the default profile attributes, then merge that into the target, where the target takes precident!
        /// </summary>
        /// <param name="target"></param>
        /// <param name="defaultPAS"></param>
        /// <param name="custPAS"></param>
        private void MergeProfileAttributes(ProfileAttributeCollection target, ProfileAttributeCollection defaultPAS,
            ProfileAttributeCollection custPAS)
        {
            // first merge cust into target, where target takes precident
            if (custPAS != null)
            {
                foreach (var pa in custPAS)
                {
                    if (!target.Exists(p => p.Key.ToLower() == pa.Key.ToLower()))
                    {
                        target.Add(pa.Clone());
                    }
                    else
                    {
                        //target already has the end use, keep the target as-is, ignore this source
                    }
                }
            }

            //  then merge default into target, where target again takes precident
            if (defaultPAS != null)
            {
                foreach (var pa in defaultPAS)
                {
                    if (!target.Exists(p => p.Key.ToLower() == pa.Key.ToLower()))
                    {
                        target.Add(pa.Clone());
                    }
                    else
                    {
                        //target already has the end use, keep the target as-is, ignore this source
                    }
                }
            }
        }


        /// <summary>
        /// Set the AnnualHDD, AnnualCDD, derived weeks per season, evaporation information, ...
        /// </summary>
        /// <param name="p"></param>
        private void SetWeatherParameters(EnergyModelInputParams p)
        {
            // degree days info
            var dd =
                _weather.AnnualDegreeDaysCollection.Find(
                    m => m.TemperatureRegionId == p.RegionParams.TemperatureRegionId);
            if (dd != null)
            {
                // set annual HHD and CDD
                p.WeatherInfo.AnnualHDD = dd.TotalHDD;
                p.WeatherInfo.AnnualCDD = dd.TotalCDD;

                // set weeks per season
                decimal winterCDD = (decimal) (dd.MonthlyCDD[12] + dd.MonthlyCDD[1] + dd.MonthlyCDD[2])/
                                    (decimal) dd.TotalCDD;
                decimal springCDD = (decimal) (dd.MonthlyCDD[3] + dd.MonthlyCDD[4] + dd.MonthlyCDD[5])/
                                    (decimal) dd.TotalCDD;
                decimal summerCDD = (decimal) (dd.MonthlyCDD[6] + dd.MonthlyCDD[7] + dd.MonthlyCDD[8])/
                                    (decimal) dd.TotalCDD;
                decimal fallCDD = (decimal) (dd.MonthlyCDD[9] + dd.MonthlyCDD[10] + dd.MonthlyCDD[11])/
                                  (decimal) dd.TotalCDD;

                if (winterCDD >= 0.25m)
                {
                    p.WeatherInfo.WeeksWinter = 13;
                }
                else
                {
                    p.WeatherInfo.WeeksWinter = 52.0m*winterCDD;
                }
                if (springCDD >= 0.25m)
                {
                    p.WeatherInfo.WeeksSpring = 13;
                }
                else
                {
                    p.WeatherInfo.WeeksSpring = 52.0m*springCDD;
                }
                if (summerCDD >= 0.25m)
                {
                    p.WeatherInfo.WeeksSummer = 13;
                }
                else
                {
                    p.WeatherInfo.WeeksSummer = 52.0m*summerCDD;
                }
                if (fallCDD >= 0.25m)
                {
                    p.WeatherInfo.WeeksFall = 13;
                }
                else
                {
                    p.WeatherInfo.WeeksFall = 52.0m*fallCDD;
                }
            }

            // evaporation info
            var evap = _weather.AnnualEvaporationCollection.Find(m => m.State == p.RegionParams.State);
            if (evap != null)
            {
                if (evap.MonthlyEvap != null && evap.MonthlyEvap.Length >= 0 && evap.MonthlyEvap.Length <= 13)
                {
                    for (int i = 0; i < evap.MonthlyEvap.Length - 1; i++)
                    {
                        p.WeatherInfo.MonthlyEvaporationFactors[i] = (decimal) evap.MonthlyEvap[i];
                    }
                }
            }

            //temperature region specific
            var tempReg =
                _weather.TemperatureRegionCollection.Find(
                    m => m.TemperatureRegionId == p.RegionParams.TemperatureRegionId);
            if (tempReg != null)
            {
                p.WeatherInfo.GroundWaterTemp = tempReg.GroundWaterTemp;
                p.WeatherInfo.CoolDesignTemp = tempReg.CoolDesignTemp;
                p.WeatherInfo.CoolDesignWetBulb = tempReg.CoolDesignWetBulb;
                p.WeatherInfo.HeatDesignTemp = tempReg.HeatDesignTemp;
                p.WeatherInfo.StandardPressure = tempReg.StandardPressure;
            }

            // solar
            var solar = _weather.SolarCollection.Find(m => m.SolarRegionId == p.RegionParams.SolarRegionId);
            if (solar != null)
            {
                for (int i = 0; i < solar.Factor.GetLength(0); i++)
                {
                    for (int j = 0; j < solar.Factor.GetLength(1); j++)
                    {
                        for (int k = 0; k < solar.Factor.GetLength(2); k++)
                        {
                            p.WeatherInfo.SolarFactors[i, j, k] = (decimal) solar.Factor[i, j, k];
                        }

                    }
                }
            }

            //temperature profile (possibly refactor this to just-in-time when executing manager for only when we have heatsystem, centralac, or secondaryheat)
            var tempProfile =
                _weather.TemperatureProfileCollection.Find(
                    m => m.TemperatureRegionId == p.RegionParams.TemperatureRegionId);
            if (tempProfile != null)
            {
                var monthTemp = new decimal[2, 12];
                for (int i = 0; i < tempProfile.Factor.GetLength(0); i++)
                {
                    for (int j = 0; j < tempProfile.Factor.GetLength(1); j++)
                    {
                        for (int k = 0; k < tempProfile.Factor.GetLength(2); k++)
                        {
                            var tempFactor = (decimal) tempProfile.Factor[i, j, k];
                            p.WeatherInfo.TemperatureFactors[i, j, k] = tempFactor;
                            // TFS 1463 Dec 2015
                            // add up the hourly temperature per month
                            monthTemp[i, j] = monthTemp[i, j] + tempFactor;
                        }

                    }
                }

                for (int i = 0; i < 2; i++)
                {
                    for (int j = 0; j < 12; j++)
                    {
                        p.WeatherInfo.AverageTemperature[i, j] = monthTemp[i, j]/24;
                    }
                }
            }

        }

        /// <summary>
        /// Data Access init.
        /// </summary>
        private void InitDataAccess()
        {
            _dataAccessLayer = new DataAccessLayer();
        }




        /// <summary>
        /// Data Access Init with connection string overrides.
        /// </summary>
        /// <param name="csInsightsMetadata"></param>
        /// <param name="csInsightsDW"></param>
        /// <param name="csInsights"></param>
        private void InitDataAccess(string csInsightsMetadata, string csInsightsDW, string csInsights)
        {
            _dataAccessLayer = new DataAccessLayer(csInsightsMetadata, csInsightsDW, csInsights);
        }


        /// <summary>
        /// Initialize content provider to be used to populate default attributes, appliances, and client calc factors, when necessary.
        /// </summary>
        private void InitContentProvider()
        {
            // init the content provider
            var factory = new ContentModelFactoryContentful();
            _contentProvider = factory.CreateContentProvider(_clientId);
        }

        private void InitContentProvider(ContentProviderType type)
        {
            try
            {
                // init the content provider
                var factory = new ContentModelFactoryContentful();
                _contentProvider = factory.CreateContentProvider(_clientId, type);

            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// Initialize content provider to be used to populate default attributes, appliances, and client calc factors, when necessary.
        /// </summary>
        private void InitContentProvider(string csInsightsMetadata)
        {
            try
            {
                // init the content provider
                var factory = new ContentModelFactoryContentful();
                _contentProvider = factory.CreateContentProvider(_clientId, ContentProviderType.Contentful,
                    csInsightsMetadata);

            }
            catch
            {
                throw;
            }
        }


        /// <summary>
        /// Load weather lookup data.
        /// </summary>
        private void LoadWeatherLookups()
        {

            try
            {
                if (_weather == null)
                {
                    _weather = new Weather();
                }

                if (_weather.AnnualDegreeDaysCollection == null)
                {
                    var ddc = this._dataAccessLayer.GetDegreeDaysLUT();
                    _weather.AnnualDegreeDaysCollection = ddc;
                }

                if (_weather.AnnualEvaporationCollection == null)
                {
                    var ec = this._dataAccessLayer.GetEvaporationLUT();
                    _weather.AnnualEvaporationCollection = ec;
                }

                if (_weather.SolarCollection == null)
                {
                    var sc = this._dataAccessLayer.GetSolarLUT();
                    _weather.SolarCollection = sc;
                }

                if (_weather.TemperatureRegionCollection == null)
                {
                    var tr = this._dataAccessLayer.GetTemperatureRegionLUT();
                    _weather.TemperatureRegionCollection = tr;
                }

                if (_weather.TemperatureProfileCollection == null)
                {
                    var tr = this._dataAccessLayer.GetTemperatureProfileLUT();
                    _weather.TemperatureProfileCollection = tr;
                }

            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// Load configuration.
        /// </summary>
        private void LoadConfiguration()
        {
            try
            {
                if (_config == null)
                {
                    var cacheKey = "emc_" + ClientID;

                    var emc = CE.EnergyModel.Cache.CacheLayer.Get<EnergyModelConfiguration>(cacheKey);

                    if (emc == null)
                    {
                        _config = new EnergyModelConfiguration();

                        // CALCULATION FACTORS
                        // The content model should already be initialized.  Get the configuration information for energymodel and common!  There are only a few common.
                        var factorList = _contentProvider.GetContentConfiguration("energymodel,common", string.Empty);

                        foreach (var item in factorList)
                        {
                            _config.CalculationFactors.Add(item.Key, item.Value);
                        }

                        // PROFILE OPTIONS
                        var optionList = _contentProvider.GetContentProfileOption(string.Empty);

                        foreach (var item in optionList)
                        {
                            // limit the items by exclusing those that are essentially boolean or straight-up value; only want lists like (ex: something.usage.rarely, something.usage.always, etc.)
                            if (!item.Key.EndsWith(".no") && !item.Key.EndsWith(".yes") && !item.Key.EndsWith(".value"))
                            {
                                _config.ProfileOptions.Add(item.Key, item.ProfileOptionValue);
                            }
                        }

                        // APPLIANCE ATTRIBUTE COUNTS
                        // now add the 'known' applianceAttributesCounts, this is static right now, used for 'confidence' in finalizing
                        _config.ApplianceAttributeCounts.Add("aquarium", 1);
                        _config.ApplianceAttributeCounts.Add("airpurifier", 1);
                        _config.ApplianceAttributeCounts.Add("atticfan", 2);
                        _config.ApplianceAttributeCounts.Add("blockheater", 1);
                        _config.ApplianceAttributeCounts.Add("ceilingfan", 2);
                        _config.ApplianceAttributeCounts.Add("centralac", 9);
                        _config.ApplianceAttributeCounts.Add("clotheswasher", 12);
                        _config.ApplianceAttributeCounts.Add("coffeemaker", 1);
                        _config.ApplianceAttributeCounts.Add("compactor", 1);
                        _config.ApplianceAttributeCounts.Add("computer", 3);
                        _config.ApplianceAttributeCounts.Add("cooktop", 8);
                        _config.ApplianceAttributeCounts.Add("copier", 1);
                        _config.ApplianceAttributeCounts.Add("dehumidifier", 2);
                        _config.ApplianceAttributeCounts.Add("dishwasher", 8);
                        _config.ApplianceAttributeCounts.Add("disposal", 1);
                        _config.ApplianceAttributeCounts.Add("dryer", 8);
                        _config.ApplianceAttributeCounts.Add("electricblanket", 1);
                        _config.ApplianceAttributeCounts.Add("fax", 1);
                        _config.ApplianceAttributeCounts.Add("fireplace", 3);
                        _config.ApplianceAttributeCounts.Add("freezer", 10);
                        _config.ApplianceAttributeCounts.Add("freezer2", 10);
                        _config.ApplianceAttributeCounts.Add("growlight", 1);
                        _config.ApplianceAttributeCounts.Add("gutterheater", 1);
                        _config.ApplianceAttributeCounts.Add("heatsystem", 25);
                        _config.ApplianceAttributeCounts.Add("hottub", 8);
                        _config.ApplianceAttributeCounts.Add("hotwaterdispenser", 1);
                        _config.ApplianceAttributeCounts.Add("housefan", 2);
                        _config.ApplianceAttributeCounts.Add("humidifier", 2);
                        _config.ApplianceAttributeCounts.Add("kiln", 1);
                        _config.ApplianceAttributeCounts.Add("lighting", 14);
                        _config.ApplianceAttributeCounts.Add("microwave", 6);
                        _config.ApplianceAttributeCounts.Add("miscellaneous", 1);
                        _config.ApplianceAttributeCounts.Add("o2concentrator", 1);
                        _config.ApplianceAttributeCounts.Add("outsidewater", 20);
                        _config.ApplianceAttributeCounts.Add("oven", 9);
                        _config.ApplianceAttributeCounts.Add("pool", 15);
                        _config.ApplianceAttributeCounts.Add("refrigerator", 14);
                        _config.ApplianceAttributeCounts.Add("refrigerator2", 14);
                        _config.ApplianceAttributeCounts.Add("roomac", 11);
                        _config.ApplianceAttributeCounts.Add("roomac2", 11);
                        _config.ApplianceAttributeCounts.Add("roomac3", 11);
                        _config.ApplianceAttributeCounts.Add("sauna", 1);
                        _config.ApplianceAttributeCounts.Add("secondaryheating", 5);
                        _config.ApplianceAttributeCounts.Add("shower", 7);
                        _config.ApplianceAttributeCounts.Add("sink", 4);
                        _config.ApplianceAttributeCounts.Add("spaceheater", 3);
                        _config.ApplianceAttributeCounts.Add("steamroom", 1);
                        _config.ApplianceAttributeCounts.Add("sumppump", 3);
                        _config.ApplianceAttributeCounts.Add("toilet", 3);
                        _config.ApplianceAttributeCounts.Add("towelbar", 1);
                        _config.ApplianceAttributeCounts.Add("tv", 7);
                        _config.ApplianceAttributeCounts.Add("tvflat", 5);
                        _config.ApplianceAttributeCounts.Add("tvrear", 4);
                        _config.ApplianceAttributeCounts.Add("vehicle", 9);
                        _config.ApplianceAttributeCounts.Add("vehicle2", 9);
                        _config.ApplianceAttributeCounts.Add("waterbed", 7);
                        _config.ApplianceAttributeCounts.Add("waterheater", 19);
                        _config.ApplianceAttributeCounts.Add("wellpump", 2);
                        _config.ApplianceAttributeCounts.Add("waterdispenser", 1);
                        _config.ApplianceAttributeCounts.Add("winechiller", 1);
                        _config.ApplianceAttributeCounts.Add("woodstove", 2);

                        // COMMODITY KEYS
                        var commodityList = _contentProvider.GetContentCommodity(string.Empty);

                        foreach (var item in commodityList)
                        {
                            _config.CommodityKeys.Add(item.Key, item.Key);
                        }

                        // UOM KEYS
                        var uomList = _contentProvider.GetContentUom(string.Empty);

                        foreach (var item in uomList)
                        {
                            _config.UomKeys.Add(item.Key, item.Key);
                        }

                        // CURRENCY KEYS
                        var currencyList = _contentProvider.GetContentCurrency(string.Empty);

                        foreach (var item in currencyList)
                        {
                            _config.CurrencyKeys.Add(item.Key, item.Key);
                        }

                        //BUSINESS Energy Use Intensity (EUI) list for Lookup
                        var euis = _dataAccessLayer.GetBuildingEnergyUseIntensityList(ClientID);
                        _config.BuildingEuis = euis;


                        // ** FINALLY add the configuration to the cache **
                        CE.EnergyModel.Cache.CacheLayer.Add(_config, cacheKey, _cacheTimeoutInMinutes);

                    }
                    else
                    {
                        _config = emc;
                    }

                }

            }
            catch
            {
                throw;
            }
        }
    }

}
