﻿using CE.EnergyModel;
using CE.EnergyModel.Entities;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace EnergyModelTests
{
    [TestClass]
    public class SingleTests
    {

        [TestInitialize]
        public void TestInit()
        {
            // This initializes the automapper!
            new CE.ContentModel.AutoMapperBootStrapper().BootStrap();
        }


        [TestMethod]
        public void Aquarium_Test()
        {
            var clientId = 87;
            var factory = new EnergyModelManagerFactory();
            var config = new EnergyModelConfiguration(Utility.CreateCalculationFactors());
            var weather = new Weather(Utility.CreateDegreeDayCollection(), Utility.CreateEvaporationCollection(), Utility.CreateTemperatureRegionCollection(), Utility.CreateSolarCollection(), Utility.CreateTemperatureProfileCollection());

            // Create Manager with config and weather data
            var mgr = factory.CreateEnergyModelManager(clientId, config, weather);

            // Appliance collection
            var appliances = new ApplianceCollection();

            //Appliances - all data passed-in directly; these typically come from client appliances, but CAN be provided directly
            var applianceName = "Aquarium";
            var electricExpression = "([P:aquarium.count] * 1.0 ) + [F:aquarium.energyuse]";
            var appliance = new Appliance() { Name = applianceName };
            var energyUse = new EnergyUse(CE.EnergyModel.Enums.CommodityType.Electric, CE.EnergyModel.Enums.UnitOfMeasureType.kWh, new EvaluationContext() { EvaluationType = CE.EnergyModel.Enums.EvaluationType.Pending, Expression = electricExpression });
            appliance.EnergyUses.Add(energyUse);
            appliances.Add(appliance);

            //Profile Attributes - all data passed-in directly, as if it were populated by overlaying from multiple data sources internally
            var attributes = new ProfileAttributeCollection();
            attributes.Add(new ProfileAttribute() { Key = "aquarium.count", OptionKey = "aquarium.count.value", OptionValue = "1" });

            // use this for overload call below;
            var regionParams = new RegionParams() { ApplianceRegionId = 1, TemperatureRegionId = 31, SolarRegionId = 1, State = "AZ" };

            // Execute Model
            var result = mgr.ExecuteEnergyModel(regionParams, null, attributes, appliances);

        }

        [TestMethod]
        public void AutoBlockHeater_Test()
        {
            var clientId = 87;
            var factory = new EnergyModelManagerFactory();
            var config = new EnergyModelConfiguration(Utility.CreateCalculationFactors());
            var weather = new Weather(Utility.CreateDegreeDayCollection(), Utility.CreateEvaporationCollection(), Utility.CreateTemperatureRegionCollection(), Utility.CreateSolarCollection(), Utility.CreateTemperatureProfileCollection());

            // Create Manager with config and weather data
            var mgr = factory.CreateEnergyModelManager(clientId, config, weather);

            // Appliance collection
            var appliances = new ApplianceCollection();

            //Appliances - all data passed-in directly; these typically come from client appliances, but CAN be provided directly
            var applianceName = "AutoBlockHeater";
            var electricExpression = "([P:autoblockheater.count] * 1.0 ) + [F:autoblockheater.energyuse]";
            var appliance = new Appliance() { Name = applianceName };
            var energyUse = new EnergyUse(CE.EnergyModel.Enums.CommodityType.Electric, CE.EnergyModel.Enums.UnitOfMeasureType.kWh, new EvaluationContext() { EvaluationType = CE.EnergyModel.Enums.EvaluationType.Pending, Expression = electricExpression });
            appliance.EnergyUses.Add(energyUse);
            appliances.Add(appliance);

            //Profile Attributes - all data passed-in directly, as if it were populated by overlaying from multiple data sources internally
            var attributes = new ProfileAttributeCollection();
            attributes.Add(new ProfileAttribute() { Key = "autoblockheater.count", OptionKey = "autoblockheater.count.value", OptionValue = "1" });

            // use this for overload call below;
            var regionParams = new RegionParams() { ApplianceRegionId = 1, TemperatureRegionId = 31, SolarRegionId = 1, State = "AZ" };

            // Execute Model
            var result = mgr.ExecuteEnergyModel(regionParams, null, attributes, appliances);

        }

        [TestMethod]
        public void BottledWaterDispenser_Test()
        {
            var clientId = 87;
            var factory = new EnergyModelManagerFactory();
            var config = new EnergyModelConfiguration(Utility.CreateCalculationFactors());
            var weather = new Weather(Utility.CreateDegreeDayCollection(), Utility.CreateEvaporationCollection(), Utility.CreateTemperatureRegionCollection(), Utility.CreateSolarCollection(), Utility.CreateTemperatureProfileCollection());

            // Create Manager with config and weather data
            var mgr = factory.CreateEnergyModelManager(clientId, config, weather);

            // Appliance collection
            var appliances = new ApplianceCollection();

            //Appliances - all data passed-in directly; these typically come from client appliances, but CAN be provided directly
            var applianceName = "BottledWaterDispenser";
            var electricExpression = "([P:bottledwaterdispenser.count] * 1.0 ) + [F:bottledwaterdispenser.energyuse]";
            var appliance = new Appliance() { Name = applianceName };
            var energyUse = new EnergyUse(CE.EnergyModel.Enums.CommodityType.Electric, CE.EnergyModel.Enums.UnitOfMeasureType.kWh, new EvaluationContext() { EvaluationType = CE.EnergyModel.Enums.EvaluationType.Pending, Expression = electricExpression });
            appliance.EnergyUses.Add(energyUse);
            appliances.Add(appliance);

            //Profile Attributes - all data passed-in directly, as if it were populated by overlaying from multiple data sources internally
            var attributes = new ProfileAttributeCollection();
            attributes.Add(new ProfileAttribute() { Key = "bottledwaterdispenser.count", OptionKey = "bottledwaterdispenser.count.value", OptionValue = "1" });

            // use this for overload call below;
            var regionParams = new RegionParams() { ApplianceRegionId = 1, TemperatureRegionId = 31, SolarRegionId = 1, State = "AZ" };

            // Execute Model
            var result = mgr.ExecuteEnergyModel(regionParams, null, attributes, appliances);

        }

        [TestMethod]
        public void CoffeeMaker_Test()
        {
            var clientId = 87;
            var factory = new EnergyModelManagerFactory();
            var config = new EnergyModelConfiguration(Utility.CreateCalculationFactors());
            var weather = new Weather(Utility.CreateDegreeDayCollection(), Utility.CreateEvaporationCollection(), Utility.CreateTemperatureRegionCollection(), Utility.CreateSolarCollection(), Utility.CreateTemperatureProfileCollection());

            // Create Manager with config and weather data
            var mgr = factory.CreateEnergyModelManager(clientId, config, weather);

            // Appliance collection
            var appliances = new ApplianceCollection();

            //Appliances - all data passed-in directly; these typically come from client appliances, but CAN be provided directly
            var applianceName = "CoffeeMaker";
            var electricExpression = "([P:coffeemaker.count] * 1.0 ) + [F:coffeemaker.energyuse]";
            var appliance = new Appliance() { Name = applianceName };
            var energyUse = new EnergyUse(CE.EnergyModel.Enums.CommodityType.Electric, CE.EnergyModel.Enums.UnitOfMeasureType.kWh, new EvaluationContext() { EvaluationType = CE.EnergyModel.Enums.EvaluationType.Pending, Expression = electricExpression });
            appliance.EnergyUses.Add(energyUse);
            appliances.Add(appliance);

            //Profile Attributes - all data passed-in directly, as if it were populated by overlaying from multiple data sources internally
            var attributes = new ProfileAttributeCollection();
            attributes.Add(new ProfileAttribute() { Key = "coffeemaker.count", OptionKey = "coffeemaker.count.value", OptionValue = "1" });

            // use this for overload call below;
            var regionParams = new RegionParams() { ApplianceRegionId = 1, TemperatureRegionId = 31, SolarRegionId = 1, State = "AZ" };

            // Execute Model
            var result = mgr.ExecuteEnergyModel(regionParams, null, attributes, appliances);

        }

        [TestMethod]
        public void Copier_Test()
        {
            var clientId = 87;
            var factory = new EnergyModelManagerFactory();
            var config = new EnergyModelConfiguration(Utility.CreateCalculationFactors());
            var weather = new Weather(Utility.CreateDegreeDayCollection(), Utility.CreateEvaporationCollection(), Utility.CreateTemperatureRegionCollection(), Utility.CreateSolarCollection(), Utility.CreateTemperatureProfileCollection());

            // Create Manager with config and weather data
            var mgr = factory.CreateEnergyModelManager(clientId, config, weather);

            // Appliance collection
            var appliances = new ApplianceCollection();

            //Appliances - all data passed-in directly; these typically come from client appliances, but CAN be provided directly
            var applianceName = "Copier";
            var electricExpression = "([P:copier.count] * 1.0 ) + [F:copier.energyuse]";
            var appliance = new Appliance() { Name = applianceName };
            var energyUse = new EnergyUse(CE.EnergyModel.Enums.CommodityType.Electric, CE.EnergyModel.Enums.UnitOfMeasureType.kWh, new EvaluationContext() { EvaluationType = CE.EnergyModel.Enums.EvaluationType.Pending, Expression = electricExpression });
            appliance.EnergyUses.Add(energyUse);
            appliances.Add(appliance);

            //Profile Attributes - all data passed-in directly, as if it were populated by overlaying from multiple data sources internally
            var attributes = new ProfileAttributeCollection();
            attributes.Add(new ProfileAttribute() { Key = "copier.count", OptionKey = "copier.count.value", OptionValue = "1" });

            // use this for overload call below;
            var regionParams = new RegionParams() { ApplianceRegionId = 1, TemperatureRegionId = 31, SolarRegionId = 1, State = "AZ" };

            // Execute Model
            var result = mgr.ExecuteEnergyModel(regionParams, null, attributes, appliances);

        }

        [TestMethod]
        public void ElectricBlanket_Test()
        {
            var clientId = 87;
            var factory = new EnergyModelManagerFactory();
            var config = new EnergyModelConfiguration(Utility.CreateCalculationFactors());
            var weather = new Weather(Utility.CreateDegreeDayCollection(), Utility.CreateEvaporationCollection(), Utility.CreateTemperatureRegionCollection(), Utility.CreateSolarCollection(), Utility.CreateTemperatureProfileCollection());

            // Create Manager with config and weather data
            var mgr = factory.CreateEnergyModelManager(clientId, config, weather);

            // Appliance collection
            var appliances = new ApplianceCollection();

            //Appliances - all data passed-in directly; these typically come from client appliances, but CAN be provided directly
            var applianceName = "ElectricBlanket";
            var electricExpression = "([P:electricblanket.count] * 1.0 ) + [F:electricblanket.energyuse]";
            var appliance = new Appliance() { Name = applianceName };
            var energyUse = new EnergyUse(CE.EnergyModel.Enums.CommodityType.Electric, CE.EnergyModel.Enums.UnitOfMeasureType.kWh, new EvaluationContext() { EvaluationType = CE.EnergyModel.Enums.EvaluationType.Pending, Expression = electricExpression });
            appliance.EnergyUses.Add(energyUse);
            appliances.Add(appliance);

            //Profile Attributes - all data passed-in directly, as if it were populated by overlaying from multiple data sources internally
            var attributes = new ProfileAttributeCollection();
            attributes.Add(new ProfileAttribute() { Key = "electricblanket.count", OptionKey = "electricblanket.count.value", OptionValue = "1" });

            // use this for overload call below;
            var regionParams = new RegionParams() { ApplianceRegionId = 1, TemperatureRegionId = 31, SolarRegionId = 1, State = "AZ" };

            // Execute Model
            var result = mgr.ExecuteEnergyModel(regionParams, null, attributes, appliances);

        }

        [TestMethod]
        public void FaxMachine_Test()
        {
            var clientId = 87;
            var factory = new EnergyModelManagerFactory();
            var config = new EnergyModelConfiguration(Utility.CreateCalculationFactors());
            var weather = new Weather(Utility.CreateDegreeDayCollection(), Utility.CreateEvaporationCollection(), Utility.CreateTemperatureRegionCollection(), Utility.CreateSolarCollection(), Utility.CreateTemperatureProfileCollection());

            // Create Manager with config and weather data
            var mgr = factory.CreateEnergyModelManager(clientId, config, weather);

            // Appliance collection
            var appliances = new ApplianceCollection();

            //Appliances - all data passed-in directly; these typically come from client appliances, but CAN be provided directly
            var applianceName = "FaxMachine";
            var electricExpression = "([P:faxmachine.count] * 1.0 ) + [F:faxmachine.energyuse]";
            var appliance = new Appliance() { Name = applianceName };
            var energyUse = new EnergyUse(CE.EnergyModel.Enums.CommodityType.Electric, CE.EnergyModel.Enums.UnitOfMeasureType.kWh, new EvaluationContext() { EvaluationType = CE.EnergyModel.Enums.EvaluationType.Pending, Expression = electricExpression });
            appliance.EnergyUses.Add(energyUse);
            appliances.Add(appliance);

            //Profile Attributes - all data passed-in directly, as if it were populated by overlaying from multiple data sources internally
            var attributes = new ProfileAttributeCollection();
            attributes.Add(new ProfileAttribute() { Key = "faxmachine.count", OptionKey = "faxmachine.count.value", OptionValue = "1" });

            // use this for overload call below;
            var regionParams = new RegionParams() { ApplianceRegionId = 1, TemperatureRegionId = 31, SolarRegionId = 1, State = "AZ" };

            // Execute Model
            var result = mgr.ExecuteEnergyModel(regionParams, null, attributes, appliances);

        }

        [TestMethod]
        public void GutterHeater_Test()
        {
            var clientId = 87;
            var factory = new EnergyModelManagerFactory();
            var config = new EnergyModelConfiguration(Utility.CreateCalculationFactors());
            var weather = new Weather(Utility.CreateDegreeDayCollection(), Utility.CreateEvaporationCollection(), Utility.CreateTemperatureRegionCollection(), Utility.CreateSolarCollection(), Utility.CreateTemperatureProfileCollection());

            // Create Manager with config and weather data
            var mgr = factory.CreateEnergyModelManager(clientId, config, weather);

            // Appliance collection
            var appliances = new ApplianceCollection();

            //Appliances - all data passed-in directly; these typically come from client appliances, but CAN be provided directly
            var applianceName = "GutterHeater";
            var electricExpression = "([P:gutterheater.count] * 1.0 ) + [F:gutterheater.energyuse]";
            var appliance = new Appliance() { Name = applianceName };
            var energyUse = new EnergyUse(CE.EnergyModel.Enums.CommodityType.Electric, CE.EnergyModel.Enums.UnitOfMeasureType.kWh, new EvaluationContext() { EvaluationType = CE.EnergyModel.Enums.EvaluationType.Pending, Expression = electricExpression });
            appliance.EnergyUses.Add(energyUse);
            appliances.Add(appliance);

            //Profile Attributes - all data passed-in directly, as if it were populated by overlaying from multiple data sources internally
            var attributes = new ProfileAttributeCollection();
            attributes.Add(new ProfileAttribute() { Key = "gutterheater.count", OptionKey = "gutterheater.count.value", OptionValue = "1" });

            // use this for overload call below;
            var regionParams = new RegionParams() { ApplianceRegionId = 1, TemperatureRegionId = 31, SolarRegionId = 1, State = "AZ" };

            // Execute Model
            var result = mgr.ExecuteEnergyModel(regionParams, null, attributes, appliances);

        }

        [TestMethod]
        public void GrowLight_Test()
        {
            var clientId = 87;
            var factory = new EnergyModelManagerFactory();
            var config = new EnergyModelConfiguration(Utility.CreateCalculationFactors());
            var weather = new Weather(Utility.CreateDegreeDayCollection(), Utility.CreateEvaporationCollection(), Utility.CreateTemperatureRegionCollection(), Utility.CreateSolarCollection(), Utility.CreateTemperatureProfileCollection());

            // Create Manager with config and weather data
            var mgr = factory.CreateEnergyModelManager(clientId, config, weather);

            // Appliance collection
            var appliances = new ApplianceCollection();

            //Appliances - all data passed-in directly; these typically come from client appliances, but CAN be provided directly
            var applianceName = "GrowLight";
            var electricExpression = "([P:growlight.count] * 1.0 ) + [F:growlight.energyuse]";
            var appliance = new Appliance() { Name = applianceName };
            var energyUse = new EnergyUse(CE.EnergyModel.Enums.CommodityType.Electric, CE.EnergyModel.Enums.UnitOfMeasureType.kWh, new EvaluationContext() { EvaluationType = CE.EnergyModel.Enums.EvaluationType.Pending, Expression = electricExpression });
            appliance.EnergyUses.Add(energyUse);
            appliances.Add(appliance);

            //Profile Attributes - all data passed-in directly, as if it were populated by overlaying from multiple data sources internally
            var attributes = new ProfileAttributeCollection();
            attributes.Add(new ProfileAttribute() { Key = "growlight.count", OptionKey = "growlight.count.value", OptionValue = "1" });

            // use this for overload call below;
            var regionParams = new RegionParams() { ApplianceRegionId = 1, TemperatureRegionId = 31, SolarRegionId = 1, State = "AZ" };

            // Execute Model
            var result = mgr.ExecuteEnergyModel(regionParams, null, attributes, appliances);

        }

        [TestMethod]
        public void HeatedTowelBar_Test()
        {
            var clientId = 87;
            var factory = new EnergyModelManagerFactory();
            var config = new EnergyModelConfiguration(Utility.CreateCalculationFactors());
            var weather = new Weather(Utility.CreateDegreeDayCollection(), Utility.CreateEvaporationCollection(), Utility.CreateTemperatureRegionCollection(), Utility.CreateSolarCollection(), Utility.CreateTemperatureProfileCollection());

            // Create Manager with config and weather data
            var mgr = factory.CreateEnergyModelManager(clientId, config, weather);

            // Appliance collection
            var appliances = new ApplianceCollection();

            //Appliances - all data passed-in directly; these typically come from client appliances, but CAN be provided directly
            var applianceName = "HeatedTowelBar";
            var electricExpression = "([P:heatedtowelbar.count] * 1.0 ) + [F:heatedtowelbar.energyuse]";
            var appliance = new Appliance() { Name = applianceName };
            var energyUse = new EnergyUse(CE.EnergyModel.Enums.CommodityType.Electric, CE.EnergyModel.Enums.UnitOfMeasureType.kWh, new EvaluationContext() { EvaluationType = CE.EnergyModel.Enums.EvaluationType.Pending, Expression = electricExpression });
            appliance.EnergyUses.Add(energyUse);
            appliances.Add(appliance);

            //Profile Attributes - all data passed-in directly, as if it were populated by overlaying from multiple data sources internally
            var attributes = new ProfileAttributeCollection();
            attributes.Add(new ProfileAttribute() { Key = "heatedtowelbar.count", OptionKey = "heatedtowelbar.count.value", OptionValue = "1" });

            // use this for overload call below;
            var regionParams = new RegionParams() { ApplianceRegionId = 1, TemperatureRegionId = 31, SolarRegionId = 1, State = "AZ" };

            // Execute Model
            var result = mgr.ExecuteEnergyModel(regionParams, null, attributes, appliances);

        }

        [TestMethod]
        public void HotWaterDispenser_Test()
        {
            var clientId = 87;
            var factory = new EnergyModelManagerFactory();
            var config = new EnergyModelConfiguration(Utility.CreateCalculationFactors());
            var weather = new Weather(Utility.CreateDegreeDayCollection(), Utility.CreateEvaporationCollection(), Utility.CreateTemperatureRegionCollection(), Utility.CreateSolarCollection(), Utility.CreateTemperatureProfileCollection());

            // Create Manager with config and weather data
            var mgr = factory.CreateEnergyModelManager(clientId, config, weather);

            // Appliance collection
            var appliances = new ApplianceCollection();

            //Appliances - all data passed-in directly; these typically come from client appliances, but CAN be provided directly
            var applianceName = "HotWaterDispenser";
            var electricExpression = "([P:hotwaterdispenser.count] * 1.0 ) + [F:hotwaterdispenser.energyuse]";
            var appliance = new Appliance() { Name = applianceName };
            var energyUse = new EnergyUse(CE.EnergyModel.Enums.CommodityType.Electric, CE.EnergyModel.Enums.UnitOfMeasureType.kWh, new EvaluationContext() { EvaluationType = CE.EnergyModel.Enums.EvaluationType.Pending, Expression = electricExpression });
            appliance.EnergyUses.Add(energyUse);
            appliances.Add(appliance);

            //Profile Attributes - all data passed-in directly, as if it were populated by overlaying from multiple data sources internally
            var attributes = new ProfileAttributeCollection();
            attributes.Add(new ProfileAttribute() { Key = "hotwaterdispenser.count", OptionKey = "hotwaterdispenser.count.value", OptionValue = "1" });

            // use this for overload call below;
            var regionParams = new RegionParams() { ApplianceRegionId = 1, TemperatureRegionId = 31, SolarRegionId = 1, State = "AZ" };

            // Execute Model
            var result = mgr.ExecuteEnergyModel(regionParams, null, attributes, appliances);

        }

        [TestMethod]
        public void Kiln_Test()
        {
            var clientId = 87;
            var factory = new EnergyModelManagerFactory();
            var config = new EnergyModelConfiguration(Utility.CreateCalculationFactors());
            var weather = new Weather(Utility.CreateDegreeDayCollection(), Utility.CreateEvaporationCollection(), Utility.CreateTemperatureRegionCollection(), Utility.CreateSolarCollection(), Utility.CreateTemperatureProfileCollection());

            // Create Manager with config and weather data
            var mgr = factory.CreateEnergyModelManager(clientId, config, weather);

            // Appliance collection
            var appliances = new ApplianceCollection();

            //Appliances - all data passed-in directly; these typically come from client appliances, but CAN be provided directly
            var applianceName = "Kiln";
            var electricExpression = "([P:kiln.count] * 1.0 ) + [F:kiln.energyuse]";
            var appliance = new Appliance() { Name = applianceName };
            var energyUse = new EnergyUse(CE.EnergyModel.Enums.CommodityType.Electric, CE.EnergyModel.Enums.UnitOfMeasureType.kWh, new EvaluationContext() { EvaluationType = CE.EnergyModel.Enums.EvaluationType.Pending, Expression = electricExpression });
            appliance.EnergyUses.Add(energyUse);
            appliances.Add(appliance);

            //Profile Attributes - all data passed-in directly, as if it were populated by overlaying from multiple data sources internally
            var attributes = new ProfileAttributeCollection();
            attributes.Add(new ProfileAttribute() { Key = "kiln.count", OptionKey = "kiln.count.value", OptionValue = "1" });

            // use this for overload call below;
            var regionParams = new RegionParams() { ApplianceRegionId = 1, TemperatureRegionId = 31, SolarRegionId = 1, State = "AZ" };

            // Execute Model
            var result = mgr.ExecuteEnergyModel(regionParams, null, attributes, appliances);

        }

        [TestMethod]
        public void O2Concentrator_Test()
        {
            var clientId = 87;
            var factory = new EnergyModelManagerFactory();
            var config = new EnergyModelConfiguration(Utility.CreateCalculationFactors());
            var weather = new Weather(Utility.CreateDegreeDayCollection(), Utility.CreateEvaporationCollection(), Utility.CreateTemperatureRegionCollection(), Utility.CreateSolarCollection(), Utility.CreateTemperatureProfileCollection());

            // Create Manager with config and weather data
            var mgr = factory.CreateEnergyModelManager(clientId, config, weather);

            // Appliance collection
            var appliances = new ApplianceCollection();

            //Appliances - all data passed-in directly; these typically come from client appliances, but CAN be provided directly
            var applianceName = "O2Concentrator";
            var electricExpression = "([P:o2concentrator.count] * 1.0 ) + [F:o2concentrator.energyuse]";
            var appliance = new Appliance() { Name = applianceName };
            var energyUse = new EnergyUse(CE.EnergyModel.Enums.CommodityType.Electric, CE.EnergyModel.Enums.UnitOfMeasureType.kWh, new EvaluationContext() { EvaluationType = CE.EnergyModel.Enums.EvaluationType.Pending, Expression = electricExpression });
            appliance.EnergyUses.Add(energyUse);
            appliances.Add(appliance);

            //Profile Attributes - all data passed-in directly, as if it were populated by overlaying from multiple data sources internally
            var attributes = new ProfileAttributeCollection();
            attributes.Add(new ProfileAttribute() { Key = "o2concentrator.count", OptionKey = "o2concentrator.count.value", OptionValue = "1" });

            // use this for overload call below;
            var regionParams = new RegionParams() { ApplianceRegionId = 1, TemperatureRegionId = 31, SolarRegionId = 1, State = "AZ" };

            // Execute Model
            var result = mgr.ExecuteEnergyModel(regionParams, null, attributes, appliances);

        }

        [TestMethod]
        public void TrashCompactor_Test()
        {
            var clientId = 87;
            var factory = new EnergyModelManagerFactory();
            var config = new EnergyModelConfiguration(Utility.CreateCalculationFactors());
            var weather = new Weather(Utility.CreateDegreeDayCollection(), Utility.CreateEvaporationCollection(), Utility.CreateTemperatureRegionCollection(), Utility.CreateSolarCollection(), Utility.CreateTemperatureProfileCollection());

            // Create Manager with config and weather data
            var mgr = factory.CreateEnergyModelManager(clientId, config, weather);

            // Appliance collection
            var appliances = new ApplianceCollection();

            //Appliances - all data passed-in directly; these typically come from client appliances, but CAN be provided directly
            var applianceName = "TrashCompactor";
            var electricExpression = "([P:trashcompactor.count] * 1.0 ) + [F:trashcompactor.energyuse]";
            var appliance = new Appliance() { Name = applianceName };
            var energyUse = new EnergyUse(CE.EnergyModel.Enums.CommodityType.Electric, CE.EnergyModel.Enums.UnitOfMeasureType.kWh, new EvaluationContext() { EvaluationType = CE.EnergyModel.Enums.EvaluationType.Pending, Expression = electricExpression });
            appliance.EnergyUses.Add(energyUse);
            appliances.Add(appliance);

            //Profile Attributes - all data passed-in directly, as if it were populated by overlaying from multiple data sources internally
            var attributes = new ProfileAttributeCollection();
            attributes.Add(new ProfileAttribute() { Key = "trashcompactor.count", OptionKey = "trashcompactor.count.value", OptionValue = "1" });

            // use this for overload call below;
            var regionParams = new RegionParams() { ApplianceRegionId = 1, TemperatureRegionId = 31, SolarRegionId = 1, State = "AZ" };

            // Execute Model
            var result = mgr.ExecuteEnergyModel(regionParams, null, attributes, appliances);

        }

        [TestMethod]
        public void Sauna_Test()
        {
            var clientId = 87;
            var factory = new EnergyModelManagerFactory();
            var config = new EnergyModelConfiguration(Utility.CreateCalculationFactors());
            var weather = new Weather(Utility.CreateDegreeDayCollection(), Utility.CreateEvaporationCollection(), Utility.CreateTemperatureRegionCollection(), Utility.CreateSolarCollection(), Utility.CreateTemperatureProfileCollection());

            // Create Manager with config and weather data
            var mgr = factory.CreateEnergyModelManager(clientId, config, weather);

            // Appliance collection
            var appliances = new ApplianceCollection();

            //Appliances - all data passed-in directly; these typically come from client appliances, but CAN be provided directly
            var applianceName = "Sauna";
            var electricExpression = "([P:sauna.count] * 1.0 ) + [F:sauna.energyuse]";
            var appliance = new Appliance() { Name = applianceName };
            var energyUse = new EnergyUse(CE.EnergyModel.Enums.CommodityType.Electric, CE.EnergyModel.Enums.UnitOfMeasureType.kWh, new EvaluationContext() { EvaluationType = CE.EnergyModel.Enums.EvaluationType.Pending, Expression = electricExpression });
            appliance.EnergyUses.Add(energyUse);
            appliances.Add(appliance);

            //Profile Attributes - all data passed-in directly, as if it were populated by overlaying from multiple data sources internally
            var attributes = new ProfileAttributeCollection();
            attributes.Add(new ProfileAttribute() { Key = "sauna.count", OptionKey = "sauna.count.value", OptionValue = "1" });

            // use this for overload call below;
            var regionParams = new RegionParams() { ApplianceRegionId = 1, TemperatureRegionId = 31, SolarRegionId = 1, State = "AZ" };

            // Execute Model
            var result = mgr.ExecuteEnergyModel(regionParams, null, attributes, appliances);

        }

        [TestMethod]
        public void SteamRoom_Test()
        {
            var clientId = 87;
            var factory = new EnergyModelManagerFactory();
            var config = new EnergyModelConfiguration(Utility.CreateCalculationFactors());
            var weather = new Weather(Utility.CreateDegreeDayCollection(), Utility.CreateEvaporationCollection(), Utility.CreateTemperatureRegionCollection(), Utility.CreateSolarCollection(), Utility.CreateTemperatureProfileCollection());

            // Create Manager with config and weather data
            var mgr = factory.CreateEnergyModelManager(clientId, config, weather);

            // Appliance collection
            var appliances = new ApplianceCollection();

            //Appliances - all data passed-in directly; these typically come from client appliances, but CAN be provided directly
            var applianceName = "SteamRoom";
            var electricExpression = "([P:steamroom.count] * 1.0 ) + [F:steamroom.energyuse]";
            var appliance = new Appliance() { Name = applianceName };
            var energyUse = new EnergyUse(CE.EnergyModel.Enums.CommodityType.Electric, CE.EnergyModel.Enums.UnitOfMeasureType.kWh, new EvaluationContext() { EvaluationType = CE.EnergyModel.Enums.EvaluationType.Pending, Expression = electricExpression });
            appliance.EnergyUses.Add(energyUse);
            appliances.Add(appliance);

            //Profile Attributes - all data passed-in directly, as if it were populated by overlaying from multiple data sources internally
            var attributes = new ProfileAttributeCollection();
            attributes.Add(new ProfileAttribute() { Key = "steamroom.count", OptionKey = "steamroom.count.value", OptionValue = "1" });

            // use this for overload call below;
            var regionParams = new RegionParams() { ApplianceRegionId = 1, TemperatureRegionId = 31, SolarRegionId = 1, State = "AZ" };

            // Execute Model
            var result = mgr.ExecuteEnergyModel(regionParams, null, attributes, appliances);

        }

        [TestMethod]
        public void AtticFan_Test()
        {
            var clientId = 87;
            var factory = new EnergyModelManagerFactory();
            var config = new EnergyModelConfiguration(Utility.CreateCalculationFactors());
            var weather = new Weather(Utility.CreateDegreeDayCollection(), Utility.CreateEvaporationCollection(), Utility.CreateTemperatureRegionCollection(), Utility.CreateSolarCollection(), Utility.CreateTemperatureProfileCollection());

            // Create Manager with config and weather data
            var mgr = factory.CreateEnergyModelManager(clientId, config, weather);

            // Appliance collection
            var appliances = new ApplianceCollection();

            //Appliances - all data passed-in directly; these typically come from client appliances, but CAN be provided directly
            var applianceName = "AtticFan";
            var electricExpression = "[P:atticfan.count] * [F:atticfan.fankwh] * [P:atticfan.usage] * ([D:CDD]/125)";
            var appliance = new Appliance() { Name = applianceName };
            var energyUse = new EnergyUse(CE.EnergyModel.Enums.CommodityType.Electric, CE.EnergyModel.Enums.UnitOfMeasureType.kWh, new EvaluationContext() { EvaluationType = CE.EnergyModel.Enums.EvaluationType.Pending, Expression = electricExpression });
            appliance.EnergyUses.Add(energyUse);
            appliances.Add(appliance);

            //Profile Attributes - all data passed-in directly, as if it were populated by overlaying from multiple data sources internally
            var attributes = new ProfileAttributeCollection();
            attributes.Add(new ProfileAttribute() { Key = "atticfan.count", OptionKey = "atticfan.count.value", OptionValue = "0.04" });
            attributes.Add(new ProfileAttribute() { Key = "atticfan.usage", OptionKey = "atticfan.usage.often", OptionValue = "4" });

            // use this for overload call below;
            var regionParams = new RegionParams() { ApplianceRegionId = 1, TemperatureRegionId = 31, SolarRegionId = 1, State = "AZ" };

            // Execute Model
            var result = mgr.ExecuteEnergyModel(regionParams, null, attributes, appliances);

        }

        [TestMethod]
        public void REM_CeilingFan_1()
        {
            var clientId = 87;
            var factory = new EnergyModelManagerFactory();
            var config = new EnergyModelConfiguration(Utility.CreateCalculationFactors());
            var weather = new Weather(Utility.CreateDegreeDayCollection(), Utility.CreateEvaporationCollection(), Utility.CreateTemperatureRegionCollection(), Utility.CreateSolarCollection(), Utility.CreateTemperatureProfileCollection());

            // Create Manager with config and weather data
            var mgr = factory.CreateEnergyModelManager(clientId, config, weather);

            // Appliance collection
            var appliances = new ApplianceCollection()
            {
                IsStandAlone = true
            };

            //Appliances - all data passed-in directly; these typically come from client appliances, but CAN be provided directly
            var applianceName = "CeilingFan";
            var electricExpression = "[P:ceilingfan.count] * [F:ceilingfan.fankwh] * [P:ceilingfan.usage] * (18 + [D:CDD]/125)";
            var appliance = new Appliance() { Name = applianceName };
            var energyUse = new EnergyUse(CE.EnergyModel.Enums.CommodityType.Electric, CE.EnergyModel.Enums.UnitOfMeasureType.kWh, new EvaluationContext() { EvaluationType = CE.EnergyModel.Enums.EvaluationType.Pending, Expression = electricExpression });
            appliance.EnergyUses.Add(energyUse);
            appliances.Add(appliance);

            //Profile Attributes - all data passed-in directly, as if it were populated by overlaying from multiple data sources internally
            var attributes = new ProfileAttributeCollection();
            attributes.Add(new ProfileAttribute() { Key = "ceilingfan.count", OptionKey = "ceilingfan.count.value", OptionValue = "0.65" });
            attributes.Add(new ProfileAttribute() { Key = "ceilingfan.usage", OptionKey = "ceilingfan.usage.often", OptionValue = "40" });

            // use this for overload call below;
            var regionParams = new RegionParams() { ApplianceRegionId = 1, TemperatureRegionId = 31, SolarRegionId = 1, State = "AZ" };

            // Execute Model
            var result = mgr.ExecuteEnergyModel(regionParams, null, attributes, appliances);

        }


        [TestMethod]
        public void Computer_Test()
        {
            var clientId = 87;
            var factory = new EnergyModelManagerFactory();
            var config = new EnergyModelConfiguration(Utility.CreateCalculationFactors());
            var weather = new Weather(Utility.CreateDegreeDayCollection(), Utility.CreateEvaporationCollection(), Utility.CreateTemperatureRegionCollection(), Utility.CreateSolarCollection(), Utility.CreateTemperatureProfileCollection());

            // Create Manager with config and weather data
            var mgr = factory.CreateEnergyModelManager(clientId, config, weather);

            // Appliance collection
            var appliances = new ApplianceCollection();

            //Appliances - all data passed-in directly; these typically come from client appliances, but CAN be provided directly
            var applianceName = "Computer";
            var electricExpression = "365 * [F:computer.computerkwh] * ( [P:computer.usage] * ( [P:computer.count] - [P:computer.leftovernight] ) + ( [P:computer.leftovernight] * 24 ) )";
            var appliance = new Appliance() { Name = applianceName };
            var energyUse = new EnergyUse(CE.EnergyModel.Enums.CommodityType.Electric, CE.EnergyModel.Enums.UnitOfMeasureType.kWh, new EvaluationContext() { EvaluationType = CE.EnergyModel.Enums.EvaluationType.Pending, Expression = electricExpression });
            appliance.EnergyUses.Add(energyUse);
            appliances.Add(appliance);

            //Profile Attributes - all data passed-in directly, as if it were populated by overlaying from multiple data sources internally
            var attributes = new ProfileAttributeCollection();
            attributes.Add(new ProfileAttribute() { Key = "computer.leftovernight", OptionKey = "computer.leftovernight.value", OptionValue = "0" });
            attributes.Add(new ProfileAttribute() { Key = "computer.count", OptionKey = "computer.count.value", OptionValue = "0.62" });
            attributes.Add(new ProfileAttribute() { Key = "computer.usage", OptionKey = "computer.usage.often", OptionValue = "3.0" });

            // use this for overload call below;
            var regionParams = new RegionParams() { ApplianceRegionId = 1, TemperatureRegionId = 31, SolarRegionId = 1, State = "AZ" };

            // Execute Model
            var result = mgr.ExecuteEnergyModel(regionParams, null, attributes, appliances);

        }


        [TestMethod]
        public void Dehumidifier_Test()
        {
            var clientId = 87;
            var factory = new EnergyModelManagerFactory();
            var config = new EnergyModelConfiguration(Utility.CreateCalculationFactors());
            var weather = new Weather(Utility.CreateDegreeDayCollection(), Utility.CreateEvaporationCollection(), Utility.CreateTemperatureRegionCollection(), Utility.CreateSolarCollection(), Utility.CreateTemperatureProfileCollection());

            // Create Manager with config and weather data
            var mgr = factory.CreateEnergyModelManager(clientId, config, weather);

            // Appliance collection
            var appliances = new ApplianceCollection();

            //Appliances - all data passed-in directly; these typically come from client appliances, but CAN be provided directly
            var applianceName = "Dehumidifier";
            var electricExpression = "if( [P:dehumidifier.count] < 1.0, [P:dehumidifier.count] * [F:dehumidifier.energyuse] * [P:dehumidifier.usage], (([P:dehumidifier.count] * 0.5 ) + 0.5) * ([F:dehumidifier.energyuse] * [P:dehumidifier.usage]) )";
            var appliance = new Appliance() { Name = applianceName };
            var energyUse = new EnergyUse(CE.EnergyModel.Enums.CommodityType.Electric, CE.EnergyModel.Enums.UnitOfMeasureType.kWh, new EvaluationContext() { EvaluationType = CE.EnergyModel.Enums.EvaluationType.Pending, Expression = electricExpression });
            appliance.EnergyUses.Add(energyUse);
            appliances.Add(appliance);

            //Profile Attributes - all data passed-in directly, as if it were populated by overlaying from multiple data sources internally
            var attributes = new ProfileAttributeCollection();
            attributes.Add(new ProfileAttribute() { Key = "dehumidifier.count", OptionKey = "dehumidifier.count.value", OptionValue = "1" }); //default value is 0.01
            attributes.Add(new ProfileAttribute() { Key = "dehumidifier.usage", OptionKey = "dehumidifier.usage.often", OptionValue = "1.4" });

            // use this for overload call below;
            var regionParams = new RegionParams() { ApplianceRegionId = 1, TemperatureRegionId = 31, SolarRegionId = 1, State = "AZ" };

            // Execute Model
            var result = mgr.ExecuteEnergyModel(regionParams, null, attributes, appliances);

        }


        [TestMethod]
        public void Freezer_Test()
        {
            var clientId = 87;
            var factory = new EnergyModelManagerFactory();
            var config = new EnergyModelConfiguration(Utility.CreateCalculationFactors());
            var weather = new Weather(Utility.CreateDegreeDayCollection(), Utility.CreateEvaporationCollection(), Utility.CreateTemperatureRegionCollection(), Utility.CreateSolarCollection(), Utility.CreateTemperatureProfileCollection());

            // Create Manager with config and weather data
            var mgr = factory.CreateEnergyModelManager(clientId, config, weather);

            // Appliance collection
            var appliances = new ApplianceCollection();

            //Appliances - all data passed-in directly; these typically come from client appliances, but CAN be provided directly
            var applianceName = "Freezer";
            var appliance = new Appliance() { Name = applianceName };
            appliances.Add(appliance);

            //Profile Attributes - all data passed-in directly, as if it were populated by overlaying from multiple data sources internally
            var attributes = new ProfileAttributeCollection();
            attributes.Add(new ProfileAttribute() { Key = "freezer.age", OptionKey = "freezer.age.value", OptionValue = "12" });
            attributes.Add(new ProfileAttribute() { Key = "freezer.count", OptionKey = "freezer.count.value", OptionValue = "0.23" });
            attributes.Add(new ProfileAttribute() { Key = "freezer.monthsoff", OptionKey = "freezer.monthsoff.value", OptionValue = "0" });
            attributes.Add(new ProfileAttribute() { Key = "freezer.tempsetting", OptionKey = "freezer.tempsetting.verylow", OptionValue = "1.1" });
            attributes.Add(new ProfileAttribute() { Key = "freezer.maintainregularly", OptionKey = "freezer.maintainregularly.value", OptionValue = "1" });
            attributes.Add(new ProfileAttribute() { Key = "freezer.defrostregularly", OptionKey = "freezer.defrostregularly.value", OptionValue = "1" });
            attributes.Add(new ProfileAttribute() { Key = "freezer.location", OptionKey = "freezer.location.kitchen", OptionValue = "0" });
            attributes.Add(new ProfileAttribute() { Key = "freezer.frostfree", OptionKey = "freezer.frostfree.value", OptionValue = "1" });
            attributes.Add(new ProfileAttribute() { Key = "freezer.realmodel", OptionKey = "freezer.realmodel.value", OptionValue = "0" });
            attributes.Add(new ProfileAttribute() { Key = "freezer.size", OptionKey = "freezer.size.value", OptionValue = "19" });
            attributes.Add(new ProfileAttribute() { Key = "freezer.style", OptionKey = "freezer.style.upright", OptionValue = "1" });
            attributes.Add(new ProfileAttribute() { Key = "freezer.actualkwh", OptionKey = "freezer.actualkwh.value", OptionValue = "345" }); //only used when real model

            // use this for overload call below;
            var regionParams = new RegionParams() { ApplianceRegionId = 1, TemperatureRegionId = 31, SolarRegionId = 1, State = "AZ" };

            // Execute Model
            var result = mgr.ExecuteEnergyModel(regionParams, null, attributes, appliances);


        }


        [TestMethod]
        public void AirPurifier_Test()
        {
            var clientId = 87;
            var factory = new EnergyModelManagerFactory();
            var config = new EnergyModelConfiguration(Utility.CreateCalculationFactors());
            var weather = new Weather(Utility.CreateDegreeDayCollection(), Utility.CreateEvaporationCollection(), Utility.CreateTemperatureRegionCollection(), Utility.CreateSolarCollection(), Utility.CreateTemperatureProfileCollection());

            // Create Manager with config and weather data
            var mgr = factory.CreateEnergyModelManager(clientId, config, weather);

            // Appliance collection
            var appliances = new ApplianceCollection();

            //Appliances - all data passed-in directly; these typically come from client appliances, but CAN be provided directly
            var applianceName = "AirPurifier";
            var electricExpression = "([P:airpurifier.count] * 1.0 ) + [F:airpurifier.energyuse]";
            var appliance = new Appliance() { Name = applianceName };
            var energyUse = new EnergyUse(CE.EnergyModel.Enums.CommodityType.Electric, CE.EnergyModel.Enums.UnitOfMeasureType.kWh, new EvaluationContext() { EvaluationType = CE.EnergyModel.Enums.EvaluationType.Pending, Expression = electricExpression });
            appliance.EnergyUses.Add(energyUse);
            appliances.Add(appliance);

            //Profile Attributes - all data passed-in directly, as if it were populated by overlaying from multiple data sources internally
            var attributes = new ProfileAttributeCollection();
            attributes.Add(new ProfileAttribute() { Key = "airpurifier.count", OptionKey = "airpurifier.count.value", OptionValue = "1" });

            // use this for overload call below;
            var regionParams = new RegionParams() { ApplianceRegionId = 1, TemperatureRegionId = 31, SolarRegionId = 1, State = "AZ" };

            // Execute Model
            var result = mgr.ExecuteEnergyModel(regionParams, null, attributes, appliances);

        }


        [TestMethod]
        public void GarbageDisposal_Test()
        {
            var clientId = 87;
            var factory = new EnergyModelManagerFactory();
            var config = new EnergyModelConfiguration(Utility.CreateCalculationFactors());
            var weather = new Weather(Utility.CreateDegreeDayCollection(), Utility.CreateEvaporationCollection(), Utility.CreateTemperatureRegionCollection(), Utility.CreateSolarCollection(), Utility.CreateTemperatureProfileCollection());

            // Create Manager with config and weather data
            var mgr = factory.CreateEnergyModelManager(clientId, config, weather);

            // Appliance collection
            var appliances = new ApplianceCollection();

            //Appliances - all data passed-in directly; these typically come from client appliances, but CAN be provided directly
            var applianceName = "GarbageDisposal";
            var electricExpression = "([P:garbagedisposal.count] * 1.0 ) + [F:garbagedisposal.energyuse]";
            var waterExpression = "((12 * 35) + (12 * 5 * [P:house.people]))";
            var appliance = new Appliance() { Name = applianceName };
            var energyUse = new EnergyUse(CE.EnergyModel.Enums.CommodityType.Electric, new EvaluationContext() { EvaluationType = CE.EnergyModel.Enums.EvaluationType.Pending, Expression = electricExpression });
            var energyUse2 = new EnergyUse(CE.EnergyModel.Enums.CommodityType.Water, CE.EnergyModel.Enums.UnitOfMeasureType.Gal, new EvaluationContext() { EvaluationType = CE.EnergyModel.Enums.EvaluationType.Pending, Expression = waterExpression });
            appliance.EnergyUses.Add(energyUse);
            appliance.EnergyUses.Add(energyUse2);
            appliances.Add(appliance);

            //Profile Attributes - all data passed-in directly, as if it were populated by overlaying from multiple data sources internally
            var attributes = new ProfileAttributeCollection();
            attributes.Add(new ProfileAttribute() { Key = "garbagedisposal.count", OptionKey = "garbagedisposal.count.value", OptionValue = "1" });
            attributes.Add(new ProfileAttribute() { Key = "house.people", OptionKey = "house.people.value", OptionValue = "3" });

            // use this for overload call below;
            var regionParams = new RegionParams() { ApplianceRegionId = 1, TemperatureRegionId = 31, SolarRegionId = 1, State = "AZ" };

            // Execute Model
            var result = mgr.ExecuteEnergyModel(regionParams, null, attributes, appliances);

        }


        [TestMethod]
        public void WineChiller_Test()
        {
            var clientId = 87;
            var factory = new EnergyModelManagerFactory();
            var config = new EnergyModelConfiguration(Utility.CreateCalculationFactors());
            var weather = new Weather(Utility.CreateDegreeDayCollection(), Utility.CreateEvaporationCollection(), Utility.CreateTemperatureRegionCollection(), Utility.CreateSolarCollection(), Utility.CreateTemperatureProfileCollection());

            // Create Manager with config and weather data
            var mgr = factory.CreateEnergyModelManager(clientId, config, weather);

            // Appliance collection
            var appliances = new ApplianceCollection();

            //Appliances - all data passed-in directly; these typically come from client appliances, but CAN be provided directly
            var applianceName = "WineChiller";
            var electricExpression = "([P:winechiller.count] * 1.0 ) + [F:winechiller.energyuse]";
            var appliance = new Appliance() { Name = applianceName };
            var energyUse = new EnergyUse(CE.EnergyModel.Enums.CommodityType.Electric, CE.EnergyModel.Enums.UnitOfMeasureType.kWh, new EvaluationContext() { EvaluationType = CE.EnergyModel.Enums.EvaluationType.Pending, Expression = electricExpression });
            appliance.EnergyUses.Add(energyUse);
            appliances.Add(appliance);

            //Profile Attributes - all data passed-in directly, as if it were populated by overlaying from multiple data sources internally
            var attributes = new ProfileAttributeCollection();
            attributes.Add(new ProfileAttribute() { Key = "winechiller.count", OptionKey = "winechiller.count.value", OptionValue = "1" });

            // use this for overload call below;
            var regionParams = new RegionParams() { ApplianceRegionId = 1, TemperatureRegionId = 31, SolarRegionId = 1, State = "AZ" };

            // Execute Model
            var result = mgr.ExecuteEnergyModel(regionParams, null, attributes, appliances);

        }


        [TestMethod]
        public void WoodStove_Test()
        {
            var clientId = 87;
            var factory = new EnergyModelManagerFactory();
            var config = new EnergyModelConfiguration(Utility.CreateCalculationFactors());
            var weather = new Weather(Utility.CreateDegreeDayCollection(), Utility.CreateEvaporationCollection(), Utility.CreateTemperatureRegionCollection(), Utility.CreateSolarCollection(), Utility.CreateTemperatureProfileCollection());

            // Create Manager with config and weather data
            var mgr = factory.CreateEnergyModelManager(clientId, config, weather);

            // Appliance collection
            var appliances = new ApplianceCollection();

            //Appliances - all data passed-in directly; these typically come from client appliances, but CAN be provided directly
            var applianceName = "WoodStove";
            var btuExpression = "[P:woodstove.count] * [P:woodstove.usage] * [F:woodstove.stovebtu] * [F:woodstove.heatingweeks]";
            var appliance = new Appliance() { Name = applianceName };
            var energyUse = new EnergyUse(CE.EnergyModel.Enums.CommodityType.Wood, CE.EnergyModel.Enums.UnitOfMeasureType.BTU, new EvaluationContext() { EvaluationType = CE.EnergyModel.Enums.EvaluationType.Pending, Expression = btuExpression });
            appliance.EnergyUses.Add(energyUse);
            appliances.Add(appliance);

            //Profile Attributes - all data passed-in directly, as if it were populated by overlaying from multiple data sources internally
            var attributes = new ProfileAttributeCollection();
            attributes.Add(new ProfileAttribute() { Key = "woodstove.count", OptionKey = "airpurifier.count.value", OptionValue = "1" });
            attributes.Add(new ProfileAttribute() { Key = "woodstove.usage", OptionKey = "woodstove.usage.occasionally", OptionValue = "4" });

            // use this for overload call below;
            var regionParams = new RegionParams() { ApplianceRegionId = 1, TemperatureRegionId = 31, SolarRegionId = 1, State = "AZ" };

            // Execute Model
            var result = mgr.ExecuteEnergyModel(regionParams, null, attributes, appliances);

        }

        [TestMethod]
        public void HouseFan_Test()
        {
            var clientId = 87;
            var factory = new EnergyModelManagerFactory();
            var config = new EnergyModelConfiguration(Utility.CreateCalculationFactors());
            var weather = new Weather(Utility.CreateDegreeDayCollection(), Utility.CreateEvaporationCollection(), Utility.CreateTemperatureRegionCollection(), Utility.CreateSolarCollection(), Utility.CreateTemperatureProfileCollection());

            // Create Manager with config and weather data
            var mgr = factory.CreateEnergyModelManager(clientId, config, weather);

            // Appliance collection
            var appliances = new ApplianceCollection();

            //Appliances - all data passed-in directly; these typically come from client appliances, but CAN be provided directly
            var applianceName = "HouseFan";
            var electricExpression = "[P:housefan.count] * [P:housefan.usage] * [F:housefan.fankwh] * [F:housefan.coolingweeks]";
            var appliance = new Appliance() { Name = applianceName };
            var energyUse = new EnergyUse(CE.EnergyModel.Enums.CommodityType.Electric, CE.EnergyModel.Enums.UnitOfMeasureType.kWh, new EvaluationContext() { EvaluationType = CE.EnergyModel.Enums.EvaluationType.Pending, Expression = electricExpression });
            appliance.EnergyUses.Add(energyUse);
            appliances.Add(appliance);

            //Profile Attributes - all data passed-in directly, as if it were populated by overlaying from multiple data sources internally
            var attributes = new ProfileAttributeCollection();
            attributes.Add(new ProfileAttribute() { Key = "housefan.count", OptionKey = "housefan.count.value", OptionValue = "0.04" });
            attributes.Add(new ProfileAttribute() { Key = "housefan.usage", OptionKey = "housefan.usage.rarely", OptionValue = "0.07" });

            // use this for overload call below;
            var regionParams = new RegionParams() { ApplianceRegionId = 1, TemperatureRegionId = 31, SolarRegionId = 1, State = "AZ" };

            // Execute Model
            var result = mgr.ExecuteEnergyModel(regionParams, null, attributes, appliances);

        }


        [TestMethod]
        public void TV_Test()
        {
            var clientId = 87;
            var factory = new EnergyModelManagerFactory();
            var config = new EnergyModelConfiguration(Utility.CreateCalculationFactors());
            var weather = new Weather(Utility.CreateDegreeDayCollection(), Utility.CreateEvaporationCollection(), Utility.CreateTemperatureRegionCollection(), Utility.CreateSolarCollection(), Utility.CreateTemperatureProfileCollection());

            // Create Manager with config and weather data
            var mgr = factory.CreateEnergyModelManager(clientId, config, weather);

            // Appliance collection
            var appliances = new ApplianceCollection();

            //Appliances - all data passed-in directly; these typically come from client appliances, but CAN be provided directly
            var applianceName = "TV";
            var electricExpression = "(365 * [F:tv.tvhours] * ((2+[P:house.people])/5) ) * ( [P:tv.blackwiteqty] * [P:tv.bwusage] * ( [F:tv.tvwattsstdbw] / 1000 ) ) + ( [P:tv.bigscreenqty] * [P:tv.bigscreenusage] * ( [F:tv.tvwattsstdlg] / 1000 ) ) + ( [P:tv.colorqty] * [P:tv.colorusage] * ( [F:tv.tvwattsstdsm] / 1000 ) ) + ( ( [P:tv.blackwiteqty] + [P:tv.bigscreenqty] + [P:tv.colorqty] ) * [F:tv.tvstandbykwhstd] )";
            var appliance = new Appliance() { Name = applianceName };
            var energyUse = new EnergyUse(CE.EnergyModel.Enums.CommodityType.Electric, CE.EnergyModel.Enums.UnitOfMeasureType.kWh, new EvaluationContext() { EvaluationType = CE.EnergyModel.Enums.EvaluationType.Pending, Expression = electricExpression });
            appliance.EnergyUses.Add(energyUse);
            appliances.Add(appliance);

            //Profile Attributes - all data passed-in directly, as if it were populated by overlaying from multiple data sources internally
            var attributes = new ProfileAttributeCollection();
            attributes.Add(new ProfileAttribute() { Key = "tv.count", OptionKey = "tv.count.value", OptionValue = "1" });
            attributes.Add(new ProfileAttribute() { Key = "tv.blackwiteqty", OptionKey = "tv.blackwiteqty.value", OptionValue = "1" });
            attributes.Add(new ProfileAttribute() { Key = "tv.colorqty", OptionKey = "tv.colorqty.value", OptionValue = "3" });
            attributes.Add(new ProfileAttribute() { Key = "tv.bigscreenqty", OptionKey = "tv.bigscreenqty.value", OptionValue = "1" });
            attributes.Add(new ProfileAttribute() { Key = "tv.bwusage", OptionKey = "tv.bwusage.rarely", OptionValue = "0.7" });
            attributes.Add(new ProfileAttribute() { Key = "tv.colorusage", OptionKey = "tv.colorusage.often", OptionValue = "1.3" });
            attributes.Add(new ProfileAttribute() { Key = "tv.bigscreenusage", OptionKey = "tv.bigscreenusage.rarely", OptionValue = "0.7" });
            attributes.Add(new ProfileAttribute() { Key = "house.people", OptionKey = "house.people.value", OptionValue = "3" });

            // use this for overload call below;
            var regionParams = new RegionParams() { ApplianceRegionId = 1, TemperatureRegionId = 31, SolarRegionId = 1, State = "AZ" };

            // Execute Model
            var result = mgr.ExecuteEnergyModel(regionParams, null, attributes, appliances);

        }


        [TestMethod]
        public void TVFlat_Test()
        {
            var clientId = 87;
            var factory = new EnergyModelManagerFactory();
            var config = new EnergyModelConfiguration(Utility.CreateCalculationFactors());
            var weather = new Weather(Utility.CreateDegreeDayCollection(), Utility.CreateEvaporationCollection(), Utility.CreateTemperatureRegionCollection(), Utility.CreateSolarCollection(), Utility.CreateTemperatureProfileCollection());

            // Create Manager with config and weather data
            var mgr = factory.CreateEnergyModelManager(clientId, config, weather);

            // Appliance collection
            var appliances = new ApplianceCollection();

            //Appliances - all data passed-in directly; these typically come from client appliances, but CAN be provided directly
            var applianceName = "TVFlat";
            var electricExpression = "(365 * [F:tv.tvhours] * ((2+[P:house.people])/5) ) * ( [P:tvflat.smallqty] * [P:tvflat.smallusage] * ( [F:tv.tvwattsflatsm] / 1000 ) ) + ( [P:tvflat.largeqty] * [P:tvflat.largeusage] * ( [F:tv.tvwattsflatlg] / 1000 ) ) + ( ( [P:tvflat.smallqty] + [P:tvflat.largeqty] ) * [F:tv.tvstandbykwhflat] )";
            var appliance = new Appliance() { Name = applianceName };
            var energyUse = new EnergyUse(CE.EnergyModel.Enums.CommodityType.Electric, CE.EnergyModel.Enums.UnitOfMeasureType.kWh, new EvaluationContext() { EvaluationType = CE.EnergyModel.Enums.EvaluationType.Pending, Expression = electricExpression });
            appliance.EnergyUses.Add(energyUse);
            appliances.Add(appliance);

            //Profile Attributes - all data passed-in directly, as if it were populated by overlaying from multiple data sources internally
            var attributes = new ProfileAttributeCollection();
            attributes.Add(new ProfileAttribute() { Key = "tvflat.count", OptionKey = "tvflat.count.value", OptionValue = "1" });
            attributes.Add(new ProfileAttribute() { Key = "tvflat.smallqty", OptionKey = "tvflat.smallqty.value", OptionValue = "1" });
            attributes.Add(new ProfileAttribute() { Key = "tvflat.largeqty", OptionKey = "tvflat.largeqty.value", OptionValue = "1" });
            attributes.Add(new ProfileAttribute() { Key = "tvflat.largeusage", OptionKey = "tvflat.largeusage.rarely", OptionValue = "0.7" });
            attributes.Add(new ProfileAttribute() { Key = "tvflat.smallusage", OptionKey = "tvflat.smallusage.rarely", OptionValue = "0.7" });
            attributes.Add(new ProfileAttribute() { Key = "house.people", OptionKey = "house.people.value", OptionValue = "3" });

            // use this for overload call below;
            var regionParams = new RegionParams() { ApplianceRegionId = 1, TemperatureRegionId = 31, SolarRegionId = 1, State = "AZ" };

            // Execute Model
            var result = mgr.ExecuteEnergyModel(regionParams, null, attributes, appliances);

        }



        [TestMethod]
        public void TVRear_Test()
        {
            var clientId = 87;
            var factory = new EnergyModelManagerFactory();
            var config = new EnergyModelConfiguration(Utility.CreateCalculationFactors());
            var weather = new Weather(Utility.CreateDegreeDayCollection(), Utility.CreateEvaporationCollection(), Utility.CreateTemperatureRegionCollection(), Utility.CreateSolarCollection(), Utility.CreateTemperatureProfileCollection());

            // Create Manager with config and weather data
            var mgr = factory.CreateEnergyModelManager(clientId, config, weather);

            // Appliance collection
            var appliances = new ApplianceCollection();

            //Appliances - all data passed-in directly; these typically come from client appliances, but CAN be provided directly
            var applianceName = "TVRear";
            var electricExpression = "(365 * [F:tv.tvhours] * ((2+[P:house.people])/5) ) * ( [P:tvrear.count] * [P:tvrear.usage] * ( [F:tv.tvwattsproj] / 1000 ) ) + ( [P:tvrear.count] * [F:tv.tvstandbyproj] )";
            var appliance = new Appliance() { Name = applianceName };
            var energyUse = new EnergyUse(CE.EnergyModel.Enums.CommodityType.Electric, CE.EnergyModel.Enums.UnitOfMeasureType.kWh, new EvaluationContext() { EvaluationType = CE.EnergyModel.Enums.EvaluationType.Pending, Expression = electricExpression });
            appliance.EnergyUses.Add(energyUse);
            appliances.Add(appliance);

            //Profile Attributes - all data passed-in directly, as if it were populated by overlaying from multiple data sources internally
            var attributes = new ProfileAttributeCollection();
            attributes.Add(new ProfileAttribute() { Key = "tvrear.count", OptionKey = "tvflat.count.value", OptionValue = "1" });
            attributes.Add(new ProfileAttribute() { Key = "tvrear.usage", OptionKey = "tvrear.usage.rarely", OptionValue = "0.7" });
            attributes.Add(new ProfileAttribute() { Key = "house.people", OptionKey = "house.people.value", OptionValue = "3" });

            // use this for overload call below;
            var regionParams = new RegionParams() { ApplianceRegionId = 1, TemperatureRegionId = 31, SolarRegionId = 1, State = "AZ" };

            // Execute Model
            var result = mgr.ExecuteEnergyModel(regionParams, null, attributes, appliances);

        }


        [TestMethod]
        public void Toilet_Test()
        {
            var clientId = 87;
            var factory = new EnergyModelManagerFactory();
            var config = new EnergyModelConfiguration(Utility.CreateCalculationFactors());
            var weather = new Weather(Utility.CreateDegreeDayCollection(), Utility.CreateEvaporationCollection(), Utility.CreateTemperatureRegionCollection(), Utility.CreateSolarCollection(), Utility.CreateTemperatureProfileCollection());

            // Create Manager with config and weather data
            var mgr = factory.CreateEnergyModelManager(clientId, config, weather);

            // Appliance collection
            var appliances = new ApplianceCollection();

            //Appliances - all data passed-in directly; these typically come from client appliances, but CAN be provided directly
            var applianceName = "Toilet";
            var waterExpression = "( [P:toilet.toiletflow] * [F:house.flushesperday] * 365 * [P:house.people]) + ( [P:toilet.leakingtoiletsqty] * [F:house.leakingtoiletvolume] * 365)";
            var appliance = new Appliance() { Name = applianceName };
            var energyUse = new EnergyUse(CE.EnergyModel.Enums.CommodityType.Water, CE.EnergyModel.Enums.UnitOfMeasureType.Gal, new EvaluationContext() { EvaluationType = CE.EnergyModel.Enums.EvaluationType.Pending, Expression = waterExpression });
            appliance.EnergyUses.Add(energyUse);
            appliances.Add(appliance);

            //Profile Attributes - all data passed-in directly, as if it were populated by overlaying from multiple data sources internally
            var attributes = new ProfileAttributeCollection();
            attributes.Add(new ProfileAttribute() { Key = "toilet.toiletflow", OptionKey = "toilet.toiletflow.normal", OptionValue = "5.0" });
            attributes.Add(new ProfileAttribute() { Key = "toilet.leakingtoiletsqty", OptionKey = "toilet.leakingtoiletsqty.value", OptionValue = "1" });
            attributes.Add(new ProfileAttribute() { Key = "house.people", OptionKey = "house.people.value", OptionValue = "3" });

            // use this for overload call below;
            var regionParams = new RegionParams() { ApplianceRegionId = 1, TemperatureRegionId = 31, SolarRegionId = 1, State = "AZ" };

            // Execute Model
            var result = mgr.ExecuteEnergyModel(regionParams, null, attributes, appliances);

        }

        [TestMethod]
        public void Sink_Test()
        {
            var clientId = 87;
            var factory = new EnergyModelManagerFactory();
            var config = new EnergyModelConfiguration(Utility.CreateCalculationFactors());
            var weather = new Weather(Utility.CreateDegreeDayCollection(), Utility.CreateEvaporationCollection(), Utility.CreateTemperatureRegionCollection(), Utility.CreateSolarCollection(), Utility.CreateTemperatureProfileCollection());

            // Create Manager with config and weather data
            var mgr = factory.CreateEnergyModelManager(clientId, config, weather);

            // Appliance collection
            var appliances = new ApplianceCollection();

            //Appliances - all data passed-in directly; these typically come from client appliances, but CAN be provided directly
            var applianceName = "Sink";
            var appliance = new Appliance() { Name = applianceName };
            appliances.Add(appliance);

            //Profile Attributes - all data passed-in directly, as if it were populated by overlaying from multiple data sources internally
            var attributes = new ProfileAttributeCollection();
            attributes.Add(new ProfileAttribute() { Key = "sink.count", OptionKey = "sink.count.value", OptionValue = "2" });
            attributes.Add(new ProfileAttribute() { Key = "sink.faucetleakrate", OptionKey = "sink.faucetleakrate.normal", OptionValue = "8.6" });
            attributes.Add(new ProfileAttribute() { Key = "sink.sinkflow", OptionKey = "sink.sinkflow.normal", OptionValue = "1.14" });
            attributes.Add(new ProfileAttribute() { Key = "sink.leakingfaucetsqty", OptionKey = "sink.leakingfaucetsqty.value", OptionValue = "1" });
            attributes.Add(new ProfileAttribute() { Key = "cooktop.breakfastswkly", OptionKey = "cooktop.breakfastswkly.value", OptionValue = "5" });
            attributes.Add(new ProfileAttribute() { Key = "cooktop.luncheswkly", OptionKey = "cooktop.luncheswkly.value", OptionValue = "5" });
            attributes.Add(new ProfileAttribute() { Key = "cooktop.dinnerswkly", OptionKey = "cooktop.dinnerswkly.value", OptionValue = "5" });
            attributes.Add(new ProfileAttribute() { Key = "dishwasher.count", OptionKey = "dishwasher.count.value", OptionValue = "1" });
            attributes.Add(new ProfileAttribute() { Key = "house.people", OptionKey = "house.people.value", OptionValue = "3" });

            // use this for overload call below;
            var regionParams = new RegionParams() { ApplianceRegionId = 1, TemperatureRegionId = 31, SolarRegionId = 1, State = "AZ" };

            // Execute Model
            var result = mgr.ExecuteEnergyModel(regionParams, null, attributes, appliances);

        }


        [TestMethod]
        public void Shower_Test()
        {
            var clientId = 87;
            var factory = new EnergyModelManagerFactory();
            var config = new EnergyModelConfiguration(Utility.CreateCalculationFactors());
            var weather = new Weather(Utility.CreateDegreeDayCollection(), Utility.CreateEvaporationCollection(), Utility.CreateTemperatureRegionCollection(), Utility.CreateSolarCollection(), Utility.CreateTemperatureProfileCollection());

            // Create Manager with config and weather data
            var mgr = factory.CreateEnergyModelManager(clientId, config, weather);

            // Appliance collection
            var appliances = new ApplianceCollection();

            //Appliances - all data passed-in directly; these typically come from client appliances, but CAN be provided directly
            var applianceName = "Shower";
            var appliance = new Appliance() { Name = applianceName };
            appliances.Add(appliance);

            //Profile Attributes - all data passed-in directly, as if it were populated by overlaying from multiple data sources internally
            var attributes = new ProfileAttributeCollection();
            attributes.Add(new ProfileAttribute() { Key = "shower.count", OptionKey = "shower.count.value", OptionValue = "1" });
            attributes.Add(new ProfileAttribute() { Key = "shower.showerleakrate", OptionKey = "shower.showerleakrate.normal", OptionValue = "8.6" });
            attributes.Add(new ProfileAttribute() { Key = "shower.showerheadflow", OptionKey = "shower.showerheadflow.normal", OptionValue = "2.1" });
            attributes.Add(new ProfileAttribute() { Key = "shower.leakingshowerqty", OptionKey = "shower.leakingshowerqty.value", OptionValue = "1" });
            attributes.Add(new ProfileAttribute() { Key = "shower.showerswkly", OptionKey = "shower.showerswkly.value", OptionValue = "7" });
            attributes.Add(new ProfileAttribute() { Key = "shower.bathswkly", OptionKey = "shower.bathswkly.value", OptionValue = "1" });
            attributes.Add(new ProfileAttribute() { Key = "shower.showerlength", OptionKey = "shower.showerlength.value", OptionValue = "7" });
            attributes.Add(new ProfileAttribute() { Key = "house.people", OptionKey = "house.people.value", OptionValue = "3" });

            // use this for overload call below;
            var regionParams = new RegionParams() { ApplianceRegionId = 1, TemperatureRegionId = 31, SolarRegionId = 1, State = "AZ" };

            // Execute Model
            var result = mgr.ExecuteEnergyModel(regionParams, null, attributes, appliances);

        }


        [TestMethod]
        public void Humidifier_Test()
        {
            var clientId = 87;
            var factory = new EnergyModelManagerFactory();
            var config = new EnergyModelConfiguration(Utility.CreateCalculationFactors());
            var weather = new Weather(Utility.CreateDegreeDayCollection(), Utility.CreateEvaporationCollection(), Utility.CreateTemperatureRegionCollection(), Utility.CreateSolarCollection(), Utility.CreateTemperatureProfileCollection());

            // Create Manager with config and weather data
            var mgr = factory.CreateEnergyModelManager(clientId, config, weather);

            // Appliance collection
            var appliances = new ApplianceCollection();

            //Appliances - all data passed-in directly; these typically come from client appliances, but CAN be provided directly
            var applianceName = "Humidifier";
            var electricExpression = "if( [P:humidifier.count] >= 1, ([P:humidifier.usage] * [F:humidifier.energyuse]) * (0.5 + 0.5 * [P:humidifier.count]), ([P:humidifier.usage] * [F:humidifier.energyuse] * [P:humidifier.count]))";
            var appliance = new Appliance() { Name = applianceName };
            var energyUse = new EnergyUse(CE.EnergyModel.Enums.CommodityType.Electric, CE.EnergyModel.Enums.UnitOfMeasureType.kWh, new EvaluationContext() { EvaluationType = CE.EnergyModel.Enums.EvaluationType.Pending, Expression = electricExpression });
            appliance.EnergyUses.Add(energyUse);
            appliances.Add(appliance);

            //Profile Attributes - all data passed-in directly, as if it were populated by overlaying from multiple data sources internally
            var attributes = new ProfileAttributeCollection();
            attributes.Add(new ProfileAttribute() { Key = "humidifier.count", OptionKey = "dehumidifier.count.value", OptionValue = "1" }); //default value is 0.01
            attributes.Add(new ProfileAttribute() { Key = "humidifier.usage", OptionKey = "dehumidifier.usage.rarely", OptionValue = "0.2" });

            // use this for overload call below;
            var regionParams = new RegionParams() { ApplianceRegionId = 1, TemperatureRegionId = 31, SolarRegionId = 1, State = "AZ" };

            // Execute Model
            var result = mgr.ExecuteEnergyModel(regionParams, null, attributes, appliances);

        }


        [TestMethod]
        public void Lighting_Test()
        {
            var clientId = 87;
            var factory = new EnergyModelManagerFactory();
            var config = new EnergyModelConfiguration(Utility.CreateCalculationFactors());
            var weather = new Weather(Utility.CreateDegreeDayCollection(), Utility.CreateEvaporationCollection(), Utility.CreateTemperatureRegionCollection(), Utility.CreateSolarCollection(), Utility.CreateTemperatureProfileCollection());

            // Create Manager with config and weather data
            var mgr = factory.CreateEnergyModelManager(clientId, config, weather);

            // Appliance collection
            var appliances = new ApplianceCollection();

            //Appliances - all data passed-in directly; these typically come from client appliances, but CAN be provided directly
            var applianceName = "Lighting";
            var appliance = new Appliance() { Name = applianceName };
            appliances.Add(appliance);

            //Profile Attributes - all data passed-in directly, as if it were populated by overlaying from multiple data sources internally
            var attributes = new ProfileAttributeCollection();
            attributes.Add(new ProfileAttribute() { Key = "lighting.count", OptionKey = "lighting.count.value", OptionValue = "1" });
            attributes.Add(new ProfileAttribute() { Key = "lighting.insideusagehrs", OptionKey = "lighting.insideusagehrs.value", OptionValue = "2" });
            attributes.Add(new ProfileAttribute() { Key = "lighting.wallqty", OptionKey = "lighting.wallqty.value", OptionValue = "6" });
            attributes.Add(new ProfileAttribute() { Key = "lighting.wallwattage", OptionKey = "lighting.wallwattage.value", OptionValue = "75" });
            attributes.Add(new ProfileAttribute() { Key = "lighting.floorwattage", OptionKey = "lighting.floorwattage.value", OptionValue = "75" });
            attributes.Add(new ProfileAttribute() { Key = "lighting.floorqty", OptionKey = "lighting.floorqty.value", OptionValue = "3" });
            attributes.Add(new ProfileAttribute() { Key = "lighting.spotwattage", OptionKey = "lighting.spotwattage.value", OptionValue = "75" });
            attributes.Add(new ProfileAttribute() { Key = "lighting.spotqty", OptionKey = "lighting.spotqty.value", OptionValue = "8" });
            attributes.Add(new ProfileAttribute() { Key = "lighting.outsidewattage", OptionKey = "lighting.outsidewattage.value", OptionValue = "100" });
            attributes.Add(new ProfileAttribute() { Key = "lighting.outsideqty", OptionKey = "lighting.outsideqty.value", OptionValue = "3" });
            attributes.Add(new ProfileAttribute() { Key = "lighting.outsideusagehrs", OptionKey = "lighting.outsideusagehrs.value", OptionValue = "2" });
            attributes.Add(new ProfileAttribute() { Key = "lighting.halogenwattage", OptionKey = "lighting.halogenwattage.value", OptionValue = "300" });
            attributes.Add(new ProfileAttribute() { Key = "lighting.halogenqty", OptionKey = "lighting.halogenqty.value", OptionValue = "1" });
            attributes.Add(new ProfileAttribute() { Key = "lighting.halogenusagehrs", OptionKey = "lighting.halogenusagehrs.value", OptionValue = "2" });
            attributes.Add(new ProfileAttribute() { Key = "house.people", OptionKey = "house.people.value", OptionValue = "3" });

            // use this for overload call below;
            var regionParams = new RegionParams() { ApplianceRegionId = 1, TemperatureRegionId = 31, SolarRegionId = 1, State = "AZ" };

            // Execute Model
            var result = mgr.ExecuteEnergyModel(regionParams, null, attributes, appliances);

        }


        [TestMethod]
        public void WellPump_Test()
        {
            var clientId = 87;
            var factory = new EnergyModelManagerFactory();
            var config = new EnergyModelConfiguration(Utility.CreateCalculationFactors());
            var weather = new Weather(Utility.CreateDegreeDayCollection(), Utility.CreateEvaporationCollection(), Utility.CreateTemperatureRegionCollection(), Utility.CreateSolarCollection(), Utility.CreateTemperatureProfileCollection());

            // Create Manager with config and weather data
            var mgr = factory.CreateEnergyModelManager(clientId, config, weather);

            // Appliance collection
            var appliances = new ApplianceCollection();

            //Appliances - all data passed-in directly; these typically come from client appliances, but CAN be provided directly
            var applianceName = "WellPump";
            var appliance = new Appliance() { Name = applianceName };
            appliances.Add(appliance);

            //Profile Attributes - all data passed-in directly, as if it were populated by overlaying from multiple data sources internally
            var attributes = new ProfileAttributeCollection();
            attributes.Add(new ProfileAttribute() { Key = "wellpump.count", OptionKey = "wellpump.count.value", OptionValue = "0.05" });
            attributes.Add(new ProfileAttribute() { Key = "wellpump.age", OptionKey = "wellpump.age.value", OptionValue = "13" });
            attributes.Add(new ProfileAttribute() { Key = "house.people", OptionKey = "house.people.value", OptionValue = "3" });
            attributes.Add(new ProfileAttribute() { Key = "house.age", OptionKey = "house.age.value", OptionValue = "25" });

            // use this for overload call below;
            var regionParams = new RegionParams() { ApplianceRegionId = 1, TemperatureRegionId = 31, SolarRegionId = 1, State = "AZ" };

            // Execute Model
            var result = mgr.ExecuteEnergyModel(regionParams, null, attributes, appliances);

        }


        [TestMethod]
        public void SumpPump_Test()
        {
            var clientId = 87;
            var factory = new EnergyModelManagerFactory();
            var config = new EnergyModelConfiguration(Utility.CreateCalculationFactors());
            var weather = new Weather(Utility.CreateDegreeDayCollection(), Utility.CreateEvaporationCollection(), Utility.CreateTemperatureRegionCollection(), Utility.CreateSolarCollection(), Utility.CreateTemperatureProfileCollection());

            // Create Manager with config and weather data
            var mgr = factory.CreateEnergyModelManager(clientId, config, weather);

            // Appliance collection
            var appliances = new ApplianceCollection();

            //Appliances - all data passed-in directly; these typically come from client appliances, but CAN be provided directly
            var applianceName = "SumpPump";
            var appliance = new Appliance() { Name = applianceName };
            appliances.Add(appliance);

            //Profile Attributes - all data passed-in directly, as if it were populated by overlaying from multiple data sources internally
            var attributes = new ProfileAttributeCollection();
            attributes.Add(new ProfileAttribute() { Key = "sumppump.count", OptionKey = "wellpump.count.value", OptionValue = "0.1" });
            attributes.Add(new ProfileAttribute() { Key = "sumppump.age", OptionKey = "wellpump.age.value", OptionValue = "13" });
            attributes.Add(new ProfileAttribute() { Key = "sumppump.basementwater", OptionKey = "sumppump.basementwater.alot", OptionValue = "2.0" });

            // use this for overload call below;
            var regionParams = new RegionParams() { ApplianceRegionId = 1, TemperatureRegionId = 31, SolarRegionId = 1, State = "AZ" };

            // Execute Model
            var result = mgr.ExecuteEnergyModel(regionParams, null, attributes, appliances);

        }


        [TestMethod]
        public void Microwave_Test()
        {
            var clientId = 87;
            var factory = new EnergyModelManagerFactory();
            var config = new EnergyModelConfiguration(Utility.CreateCalculationFactors());
            var weather = new Weather(Utility.CreateDegreeDayCollection(), Utility.CreateEvaporationCollection(), Utility.CreateTemperatureRegionCollection(), Utility.CreateSolarCollection(), Utility.CreateTemperatureProfileCollection());

            // Create Manager with config and weather data
            var mgr = factory.CreateEnergyModelManager(clientId, config, weather);

            // Appliance collection
            var appliances = new ApplianceCollection();

            //Appliances - all data passed-in directly; these typically come from client appliances, but CAN be provided directly
            var applianceName = "Microwave";
            var appliance = new Appliance() { Name = applianceName };
            appliances.Add(appliance);

            //Profile Attributes - all data passed-in directly, as if it were populated by overlaying from multiple data sources internally
            var attributes = new ProfileAttributeCollection();
            attributes.Add(new ProfileAttribute() { Key = "microwave.count", OptionKey = "wellpump.count.value", OptionValue = "0.83" });
            attributes.Add(new ProfileAttribute() { Key = "microwave.breakfastswkly", OptionKey = "microwave.breakfastswkly.value", OptionValue = "3" });
            attributes.Add(new ProfileAttribute() { Key = "microwave.luncheswkly", OptionKey = "microwave.luncheswkly.value", OptionValue = "3" });
            attributes.Add(new ProfileAttribute() { Key = "microwave.dinnerswkly", OptionKey = "microwave.dinnerswkly.value", OptionValue = "4" });
            attributes.Add(new ProfileAttribute() { Key = "microwave.usageformeals", OptionKey = "microwave.usageformeals.rarely", OptionValue = "0.5" });
            attributes.Add(new ProfileAttribute() { Key = "microwave.usageforother", OptionKey = "microwave.usageforother.occasionally", OptionValue = "1" });
            attributes.Add(new ProfileAttribute() { Key = "house.people", OptionKey = "house.people.value", OptionValue = "3" });

            // use this for overload call below;
            var regionParams = new RegionParams() { ApplianceRegionId = 1, TemperatureRegionId = 31, SolarRegionId = 1, State = "AZ" };

            // Execute Model
            var result = mgr.ExecuteEnergyModel(regionParams, null, attributes, appliances);

        }


        [TestMethod]
        public void WaterBed_Test()
        {
            var clientId = 87;
            var factory = new EnergyModelManagerFactory();
            var config = new EnergyModelConfiguration(Utility.CreateCalculationFactors());
            var weather = new Weather(Utility.CreateDegreeDayCollection(), Utility.CreateEvaporationCollection(), Utility.CreateTemperatureRegionCollection(), Utility.CreateSolarCollection(), Utility.CreateTemperatureProfileCollection());

            // Create Manager with config and weather data
            var mgr = factory.CreateEnergyModelManager(clientId, config, weather);

            // Appliance collection
            var appliances = new ApplianceCollection();

            //Appliances - all data passed-in directly; these typically come from client appliances, but CAN be provided directly
            var applianceName = "WaterBed";
            var appliance = new Appliance() { Name = applianceName };
            appliances.Add(appliance);

            //Profile Attributes - all data passed-in directly, as if it were populated by overlaying from multiple data sources internally
            var attributes = new ProfileAttributeCollection();
            attributes.Add(new ProfileAttribute() { Key = "waterbed.count", OptionKey = "waterbed.count.value", OptionValue = "0.02" });
            attributes.Add(new ProfileAttribute() { Key = "waterbed.size", OptionKey = "waterbed.size.queen", OptionValue = "1.2" });
            attributes.Add(new ProfileAttribute() { Key = "waterbed.softsided", OptionKey = "waterbed.softsided.value", OptionValue = "1" });
            attributes.Add(new ProfileAttribute() { Key = "waterbed.makethebed", OptionKey = "waterbed.makethebed.value", OptionValue = "1" });
            attributes.Add(new ProfileAttribute() { Key = "waterbed.monthsheated", OptionKey = "waterbed.monthsheated.value", OptionValue = "8" });
            attributes.Add(new ProfileAttribute() { Key = "waterbed.sideinsulated", OptionKey = "waterbed.sideinsulated.yes", OptionValue = "1" });
            attributes.Add(new ProfileAttribute() { Key = "waterbed.temperature", OptionKey = "waterbed.temperature.value", OptionValue = "85" });

            // use this for overload call below;
            var regionParams = new RegionParams() { ApplianceRegionId = 1, TemperatureRegionId = 31, SolarRegionId = 1, State = "AZ" };

            // Execute Model
            var result = mgr.ExecuteEnergyModel(regionParams, null, attributes, appliances);

        }


        [TestMethod]
        public void Vehicle_Test()
        {
            var clientId = 87;
            var factory = new EnergyModelManagerFactory();
            var config = new EnergyModelConfiguration(Utility.CreateCalculationFactors());
            var weather = new Weather(Utility.CreateDegreeDayCollection(), Utility.CreateEvaporationCollection(), Utility.CreateTemperatureRegionCollection(), Utility.CreateSolarCollection(), Utility.CreateTemperatureProfileCollection());

            // Create Manager with config and weather data
            var mgr = factory.CreateEnergyModelManager(clientId, config, weather);

            // Appliance collection
            var appliances = new ApplianceCollection();

            //Appliances - all data passed-in directly; these typically come from client appliances, but CAN be provided directly
            var applianceName = "Vehicle";
            var appliance = new Appliance() { Name = applianceName };
            appliances.Add(appliance);

            //Profile Attributes - all data passed-in directly, as if it were populated by overlaying from multiple data sources internally
            var attributes = new ProfileAttributeCollection();
            attributes.Add(new ProfileAttribute() { Key = "vehicle.count", OptionKey = "vehicle.count.value", OptionValue = "1" });
            attributes.Add(new ProfileAttribute() { Key = "vehicle.frequency", OptionKey = "vehicle.frequency.annual", OptionValue = "1" });
            attributes.Add(new ProfileAttribute() { Key = "vehicle.averagempg", OptionKey = "vehicle.averagempg.value", OptionValue = "118" });
            attributes.Add(new ProfileAttribute() { Key = "vehicle.averagempgbackup", OptionKey = "vehicle.averagempgbackup.value", OptionValue = "0" });
            attributes.Add(new ProfileAttribute() { Key = "vehicle.mileage", OptionKey = "vehicle.mileage.value", OptionValue = "12000" });
            attributes.Add(new ProfileAttribute() { Key = "vehicle.gasbackup", OptionKey = "vehicle.gasbackup.value", OptionValue = "0" });
            attributes.Add(new ProfileAttribute() { Key = "vehicle.partofutilitybill", OptionKey = "vehicle.partofutilitybill.value", OptionValue = "1" });
            attributes.Add(new ProfileAttribute() { Key = "vehicle.fuel", OptionKey = "vehicle.fuel.electric", OptionValue = "0" });
            attributes.Add(new ProfileAttribute() { Key = "vehicle.separatemeter", OptionKey = "vehicle.separatemeter.value", OptionValue = "0" });

            // use this for overload call below;
            var regionParams = new RegionParams() { ApplianceRegionId = 1, TemperatureRegionId = 31, SolarRegionId = 1, State = "AZ" };

            // Execute Model
            var result = mgr.ExecuteEnergyModel(regionParams, null, attributes, appliances);

        }


        [TestMethod]
        public void SpaceHeater_Test()
        {
            var clientId = 87;
            var factory = new EnergyModelManagerFactory();
            var config = new EnergyModelConfiguration(Utility.CreateCalculationFactors());
            var weather = new Weather(Utility.CreateDegreeDayCollection(), Utility.CreateEvaporationCollection(), Utility.CreateTemperatureRegionCollection(), Utility.CreateSolarCollection(), Utility.CreateTemperatureProfileCollection());

            // Create Manager with config and weather data
            var mgr = factory.CreateEnergyModelManager(clientId, config, weather);

            // Appliance collection
            var appliances = new ApplianceCollection();

            //Appliances - all data passed-in directly; these typically come from client appliances, but CAN be provided directly
            var applianceName = "SpaceHeater";
            var appliance = new Appliance() { Name = applianceName };
            appliances.Add(appliance);

            //Profile Attributes - all data passed-in directly, as if it were populated by overlaying from multiple data sources internally
            var attributes = new ProfileAttributeCollection();
            attributes.Add(new ProfileAttribute() { Key = "spaceheater.count", OptionKey = "vehicle.count.value", OptionValue = "0.13" });
            attributes.Add(new ProfileAttribute() { Key = "spaceheater.usage", OptionKey = "spaceheater.usage.rarely", OptionValue = "0.24" });
            attributes.Add(new ProfileAttribute() { Key = "spaceheater.fuel", OptionKey = "spaceheater.fuel.electric", OptionValue = "0" });

            // use this for overload call below;
            var regionParams = new RegionParams() { ApplianceRegionId = 1, TemperatureRegionId = 31, SolarRegionId = 1, State = "AZ" };

            // Execute Model
            var result = mgr.ExecuteEnergyModel(regionParams, null, attributes, appliances);

        }


        [TestMethod]
        public void Refrigerator_Test()
        {
            var clientId = 87;
            var factory = new EnergyModelManagerFactory();
            var config = new EnergyModelConfiguration(Utility.CreateCalculationFactors());
            var weather = new Weather(Utility.CreateDegreeDayCollection(), Utility.CreateEvaporationCollection(), Utility.CreateTemperatureRegionCollection(), Utility.CreateSolarCollection(), Utility.CreateTemperatureProfileCollection());

            // Create Manager with config and weather data
            var mgr = factory.CreateEnergyModelManager(clientId, config, weather);

            // Appliance collection
            var appliances = new ApplianceCollection();

            //Appliances - all data passed-in directly; these typically come from client appliances, but CAN be provided directly
            var applianceName = "Refrigerator";
            var appliance = new Appliance() { Name = applianceName };
            appliances.Add(appliance);

            //Profile Attributes - all data passed-in directly, as if it were populated by overlaying from multiple data sources internally
            var attributes = new ProfileAttributeCollection();
            attributes.Add(new ProfileAttribute() { Key = "refrigerator.age", OptionKey = "refrigerator.age.value", OptionValue = "5" });
            attributes.Add(new ProfileAttribute() { Key = "refrigerator.count", OptionKey = "refrigerator.count.value", OptionValue = "1" });
            attributes.Add(new ProfileAttribute() { Key = "refrigerator.monthsoff", OptionKey = "refrigerator.monthsoff.value", OptionValue = "0" });
            attributes.Add(new ProfileAttribute() { Key = "refrigerator.tempsetting", OptionKey = "refrigerator.tempsetting.verylow", OptionValue = "1.1" });
            attributes.Add(new ProfileAttribute() { Key = "refrigerator.maintainregularly", OptionKey = "refrigerator.maintainregularly.value", OptionValue = "0" });
            attributes.Add(new ProfileAttribute() { Key = "refrigerator.defrostregularly", OptionKey = "refrigerator.defrostregularly.value", OptionValue = "1" });
            attributes.Add(new ProfileAttribute() { Key = "refrigerator.location", OptionKey = "refrigerator.location.kitchen", OptionValue = "0" });
            attributes.Add(new ProfileAttribute() { Key = "refrigerator.frostfree", OptionKey = "refrigerator.frostfree.value", OptionValue = "1" });
            attributes.Add(new ProfileAttribute() { Key = "refrigerator.realmodel", OptionKey = "refrigerator.realmodel.value", OptionValue = "0" });
            attributes.Add(new ProfileAttribute() { Key = "refrigerator.size", OptionKey = "refrigerator.size.value", OptionValue = "17.5" });
            attributes.Add(new ProfileAttribute() { Key = "refrigerator.style", OptionKey = "refrigerator.style.topfreezer", OptionValue = "1" });
            attributes.Add(new ProfileAttribute() { Key = "refrigerator.actualkwh", OptionKey = "refrigerator.actualkwh.value", OptionValue = "124" }); //only used when real model
            attributes.Add(new ProfileAttribute() { Key = "refrigerator.thrudoorice", OptionKey = "refrigerator.thrudoorice.value", OptionValue = "0" });
            attributes.Add(new ProfileAttribute() { Key = "refrigerator.moistureswitch", OptionKey = "refrigerator.moistureswitch.value", OptionValue = "1" });
            attributes.Add(new ProfileAttribute() { Key = "refrigerator.monthsmoistoff", OptionKey = "refrigerator.monthsmoistoff.value", OptionValue = "8" });

            // use this for overload call below;
            var regionParams = new RegionParams() { ApplianceRegionId = 1, TemperatureRegionId = 31, SolarRegionId = 1, State = "AZ" };

            // Execute Model
            var result = mgr.ExecuteEnergyModel(regionParams, null, attributes, appliances);

        }


        [TestMethod]
        public void Miscellaneous_Test()
        {
            var clientId = 87;
            var factory = new EnergyModelManagerFactory();
            var config = new EnergyModelConfiguration(Utility.CreateCalculationFactors());
            var weather = new Weather(Utility.CreateDegreeDayCollection(), Utility.CreateEvaporationCollection(), Utility.CreateTemperatureRegionCollection(), Utility.CreateSolarCollection(), Utility.CreateTemperatureProfileCollection());

            // Create Manager with config and weather data
            var mgr = factory.CreateEnergyModelManager(clientId, config, weather);

            // Appliance collection
            var appliances = new ApplianceCollection();

            //Appliances - all data passed-in directly; these typically come from client appliances, but CAN be provided directly
            var applianceName = "Miscellaneous";
            var electricExpression = " ([P:miscellaneous.count] * [F:miscellaneous.energyuse]) + ( [F:miscellaneous.peopleroombase] + (Min(Ceiling([P:house.rooms]/4),5) * [F:miscellaneous.roommultiplier]) + (Min([P:house.people],6) * [F:miscellaneous.peoplemultiplier]) )";
            var appliance = new Appliance() { Name = applianceName };
            var energyUse = new EnergyUse(CE.EnergyModel.Enums.CommodityType.Electric, CE.EnergyModel.Enums.UnitOfMeasureType.kWh, new EvaluationContext() { EvaluationType = CE.EnergyModel.Enums.EvaluationType.Pending, Expression = electricExpression });
            appliance.EnergyUses.Add(energyUse);
            appliances.Add(appliance);

            //Profile Attributes - all data passed-in directly, as if it were populated by overlaying from multiple data sources internally
            var attributes = new ProfileAttributeCollection();
            attributes.Add(new ProfileAttribute() { Key = "miscellaneous.count", OptionKey = "miscellaneous.count.value", OptionValue = "1" });
            attributes.Add(new ProfileAttribute() { Key = "house.people", OptionKey = "house.people.value", OptionValue = "3" });
            attributes.Add(new ProfileAttribute() { Key = "house.rooms", OptionKey = "house.rooms.value", OptionValue = "12" });

            // use this for overload call below;
            var regionParams = new RegionParams() { ApplianceRegionId = 1, TemperatureRegionId = 31, SolarRegionId = 1, State = "AZ" };

            // Execute Model
            var result = mgr.ExecuteEnergyModel(regionParams, null, attributes, appliances);

        }


        [TestMethod]
        public void Oven_Test()
        {
            var clientId = 87;
            var factory = new EnergyModelManagerFactory();
            var config = new EnergyModelConfiguration(Utility.CreateCalculationFactors());
            var weather = new Weather(Utility.CreateDegreeDayCollection(), Utility.CreateEvaporationCollection(), Utility.CreateTemperatureRegionCollection(), Utility.CreateSolarCollection(), Utility.CreateTemperatureProfileCollection());

            // Create Manager with config and weather data
            var mgr = factory.CreateEnergyModelManager(clientId, config, weather);

            // Appliance collection
            var appliances = new ApplianceCollection();

            //Appliances - all data passed-in directly; these typically come from client appliances, but CAN be provided directly
            var applianceName = "Oven";
            var appliance = new Appliance() { Name = applianceName };
            appliances.Add(appliance);

            //Profile Attributes - all data passed-in directly, as if it were populated by overlaying from multiple data sources internally
            var attributes = new ProfileAttributeCollection();
            attributes.Add(new ProfileAttribute() { Key = "oven.count", OptionKey = "oven.count.value", OptionValue = "0.9" });
            attributes.Add(new ProfileAttribute() { Key = "oven.fuel", OptionKey = "oven.fuel.gas", OptionValue = "1" });
            attributes.Add(new ProfileAttribute() { Key = "oven.usage", OptionKey = "oven.usage.often", OptionValue = "1.25" });
            attributes.Add(new ProfileAttribute() { Key = "oven.breakfastswkly", OptionKey = "oven.breakfastswkly.value", OptionValue = "3" });
            attributes.Add(new ProfileAttribute() { Key = "oven.luncheswkly", OptionKey = "oven.luncheswkly.value", OptionValue = "3" });
            attributes.Add(new ProfileAttribute() { Key = "oven.dinnerswkly", OptionKey = "oven.dinnerswkly.value", OptionValue = "4" });
            attributes.Add(new ProfileAttribute() { Key = "oven.pilotlight", OptionKey = "oven.pilotlight.value", OptionValue = "1" });
            attributes.Add(new ProfileAttribute() { Key = "oven.age", OptionKey = "oven.age.value", OptionValue = "15" });
            attributes.Add(new ProfileAttribute() { Key = "oven.convectionoven", OptionKey = "oven.convectionoven.value", OptionValue = "1" });
            attributes.Add(new ProfileAttribute() { Key = "microwave.usageformeals", OptionKey = "microwave.usageformeals.often", OptionValue = "1.22" }); //optionvalue not used here
            attributes.Add(new ProfileAttribute() { Key = "house.people", OptionKey = "house.people.value", OptionValue = "3" });

            // use this for overload call below;
            var regionParams = new RegionParams() { ApplianceRegionId = 1, TemperatureRegionId = 31, SolarRegionId = 1, State = "AZ" };

            // Execute Model
            var result = mgr.ExecuteEnergyModel(regionParams, null, attributes, appliances);

        }



        [TestMethod]
        public void RoomAC_Test()
        {
            var clientId = 87;
            var factory = new EnergyModelManagerFactory();
            var config = new EnergyModelConfiguration(Utility.CreateCalculationFactors());
            var weather = new Weather(Utility.CreateDegreeDayCollection(), Utility.CreateEvaporationCollection(), Utility.CreateTemperatureRegionCollection(), Utility.CreateSolarCollection(), Utility.CreateTemperatureProfileCollection());

            // Create Manager with config and weather data
            var mgr = factory.CreateEnergyModelManager(clientId, config, weather);

            // Appliance collection
            var appliances = new ApplianceCollection();

            //Appliances - all data passed-in directly; these typically come from client appliances, but CAN be provided directly
            var applianceName = "RoomAC";
            var appliance = new Appliance() { Name = applianceName };
            appliances.Add(appliance);

            //Profile Attributes - all data passed-in directly, as if it were populated by overlaying from multiple data sources internally
            var attributes = new ProfileAttributeCollection();
            attributes.Add(new ProfileAttribute() { Key = "roomac.count", OptionKey = "roomac.count.value", OptionValue = "1" });
            attributes.Add(new ProfileAttribute() { Key = "roomac.age", OptionKey = "roomac.age.value", OptionValue = "15" });
            attributes.Add(new ProfileAttribute() { Key = "roomac.size", OptionKey = "roomac.size.value", OptionValue = "15000" });
            attributes.Add(new ProfileAttribute() { Key = "roomac.dayusage", OptionKey = "roomac.eveningusage.often", OptionValue = "6" });
            attributes.Add(new ProfileAttribute() { Key = "roomac.eveningusage", OptionKey = "roomac.eveningusage.often", OptionValue = "6" });
            attributes.Add(new ProfileAttribute() { Key = "roomac.nightusage", OptionKey = "roomac.nightusage.occasionally", OptionValue = "3" });
            attributes.Add(new ProfileAttribute() { Key = "roomac.tempsetting", OptionKey = "roomac.tempsetting.low", OptionValue = "1.0" });
            attributes.Add(new ProfileAttribute() { Key = "roomac.efficiency", OptionKey = "roomac.efficiency.excellent", OptionValue = "0" });
            attributes.Add(new ProfileAttribute() { Key = "roomac.wiredthermostat", OptionKey = "roomac.wiredthermostat.value", OptionValue = "1" });
            attributes.Add(new ProfileAttribute() { Key = "roomac.servicefrequency", OptionKey = "roomac.servicefrequency.regularly", OptionValue = "0" });
            attributes.Add(new ProfileAttribute() { Key = "roomac.roomscooled", OptionKey = "roomac.roomscooled.value", OptionValue = "5" });


            // use this for overload call below;
            var regionParams = new RegionParams() { ApplianceRegionId = 1, TemperatureRegionId = 31, SolarRegionId = 1, State = "AZ" };

            // Execute Model
            var result = mgr.ExecuteEnergyModel(regionParams, null, attributes, appliances);

        }


        [TestMethod]
        public void Fireplace_Test()
        {
            var clientId = 87;
            var factory = new EnergyModelManagerFactory();
            var config = new EnergyModelConfiguration(Utility.CreateCalculationFactors());
            var weather = new Weather(Utility.CreateDegreeDayCollection(), Utility.CreateEvaporationCollection(), Utility.CreateTemperatureRegionCollection(), Utility.CreateSolarCollection(), Utility.CreateTemperatureProfileCollection());

            // Create Manager with config and weather data
            var mgr = factory.CreateEnergyModelManager(clientId, config, weather);

            // Appliance collection
            var appliances = new ApplianceCollection();

            //Appliances - all data passed-in directly; these typically come from client appliances, but CAN be provided directly
            var applianceName = "Fireplace";
            var appliance = new Appliance() { Name = applianceName };
            appliances.Add(appliance);

            //Profile Attributes - all data passed-in directly, as if it were populated by overlaying from multiple data sources internally
            var attributes = new ProfileAttributeCollection();
            attributes.Add(new ProfileAttribute() { Key = "fireplace.count", OptionKey = "fireplace.count.value", OptionValue = "1" });
            attributes.Add(new ProfileAttribute() { Key = "fireplace.usage", OptionKey = "fireplace.usage.often", OptionValue = "1.3" });
            attributes.Add(new ProfileAttribute() { Key = "fireplace.fuel", OptionKey = "fireplace.fuel.gas", OptionValue = "1" });

            // use this for overload call below;
            var regionParams = new RegionParams() { ApplianceRegionId = 1, TemperatureRegionId = 31, SolarRegionId = 1, State = "AZ" };

            // Execute Model
            var result = mgr.ExecuteEnergyModel(regionParams, null, attributes, appliances);

        }


        [TestMethod]
        public void Hottub_Test()
        {
            var clientId = 87;
            var factory = new EnergyModelManagerFactory();
            var config = new EnergyModelConfiguration(Utility.CreateCalculationFactors());
            var weather = new Weather(Utility.CreateDegreeDayCollection(), Utility.CreateEvaporationCollection(), Utility.CreateTemperatureRegionCollection(), Utility.CreateSolarCollection(), Utility.CreateTemperatureProfileCollection());

            // Create Manager with config and weather data
            var mgr = factory.CreateEnergyModelManager(clientId, config, weather);

            // Appliance collection
            var appliances = new ApplianceCollection();

            //Appliances - all data passed-in directly; these typically come from client appliances, but CAN be provided directly
            var applianceName = "Hottub";
            var appliance = new Appliance() { Name = applianceName };
            appliances.Add(appliance);

            //Profile Attributes - all data passed-in directly, as if it were populated by overlaying from multiple data sources internally
            var attributes = new ProfileAttributeCollection();
            attributes.Add(new ProfileAttribute() { Key = "hottub.count", OptionKey = "hottub.count.value", OptionValue = "1" });
            attributes.Add(new ProfileAttribute() { Key = "hottub.coverusage", OptionKey = "hottub.coverusage.useregularly", OptionValue = "1" });
            attributes.Add(new ProfileAttribute() { Key = "hottub.temperature", OptionKey = "hottub.temperature.value", OptionValue = "102" });
            attributes.Add(new ProfileAttribute() { Key = "hottub.size", OptionKey = "hottub.size.medium", OptionValue = "1" });
            attributes.Add(new ProfileAttribute() { Key = "hottub.monthsheated", OptionKey = "hottub.monthsheated.value", OptionValue = "6" });
            attributes.Add(new ProfileAttribute() { Key = "hottub.timesrefilled", OptionKey = "hottub.timesrefilled.value", OptionValue = "2" });
            attributes.Add(new ProfileAttribute() { Key = "hottub.inside", OptionKey = "hottub.inside.no", OptionValue = "0" });
            attributes.Add(new ProfileAttribute() { Key = "hottub.fuel", OptionKey = "hottub.fuel.gas", OptionValue = "1" });

            // use this for overload call below;
            var regionParams = new RegionParams() { ApplianceRegionId = 1, TemperatureRegionId = 31, SolarRegionId = 1, State = "AZ" };

            // Execute Model
            var result = mgr.ExecuteEnergyModel(regionParams, null, attributes, appliances);

        }


        [TestMethod]
        public void Cooktop_Test()
        {
            var clientId = 87;
            var factory = new EnergyModelManagerFactory();
            var config = new EnergyModelConfiguration(Utility.CreateCalculationFactors());
            var weather = new Weather(Utility.CreateDegreeDayCollection(), Utility.CreateEvaporationCollection(), Utility.CreateTemperatureRegionCollection(), Utility.CreateSolarCollection(), Utility.CreateTemperatureProfileCollection());

            // Create Manager with config and weather data
            var mgr = factory.CreateEnergyModelManager(clientId, config, weather);

            // Appliance collection
            var appliances = new ApplianceCollection();

            //Appliances - all data passed-in directly; these typically come from client appliances, but CAN be provided directly
            var applianceName = "Cooktop";
            var appliance = new Appliance() { Name = applianceName };
            appliances.Add(appliance);

            //Profile Attributes - all data passed-in directly, as if it were populated by overlaying from multiple data sources internally
            var attributes = new ProfileAttributeCollection();
            attributes.Add(new ProfileAttribute() { Key = "cooktop.count", OptionKey = "cooktop.count.value", OptionValue = "0.9" });
            attributes.Add(new ProfileAttribute() { Key = "cooktop.fuel", OptionKey = "cooktop.fuel.gas", OptionValue = "1" });
            attributes.Add(new ProfileAttribute() { Key = "cooktop.usage", OptionKey = "cooktop.usage.often", OptionValue = "1.25" });
            attributes.Add(new ProfileAttribute() { Key = "cooktop.breakfastswkly", OptionKey = "cooktop.breakfastswkly.value", OptionValue = "3" });
            attributes.Add(new ProfileAttribute() { Key = "cooktop.luncheswkly", OptionKey = "cooktop.luncheswkly.value", OptionValue = "3" });
            attributes.Add(new ProfileAttribute() { Key = "cooktop.dinnerswkly", OptionKey = "cooktop.dinnerswkly.value", OptionValue = "4" });
            attributes.Add(new ProfileAttribute() { Key = "cooktop.pilotlight", OptionKey = "cooktop.pilotlight.yes", OptionValue = "1" });
            attributes.Add(new ProfileAttribute() { Key = "cooktop.age", OptionKey = "cooktop.age.value", OptionValue = "15" });
            attributes.Add(new ProfileAttribute() { Key = "microwave.usageformeals", OptionKey = "microwave.usageformeals.often", OptionValue = "1.22" }); //optionvalue not used here
            attributes.Add(new ProfileAttribute() { Key = "house.people", OptionKey = "house.people.value", OptionValue = "3" });

            // use this for overload call below;
            var regionParams = new RegionParams() { ApplianceRegionId = 1, TemperatureRegionId = 31, SolarRegionId = 1, State = "AZ" };

            // Execute Model
            var result = mgr.ExecuteEnergyModel(regionParams, null, attributes, appliances);

        }


        [TestMethod]
        public void Dryer_Test()
        {
            var clientId = 87;
            var factory = new EnergyModelManagerFactory();
            var config = new EnergyModelConfiguration(Utility.CreateCalculationFactors());
            var weather = new Weather(Utility.CreateDegreeDayCollection(), Utility.CreateEvaporationCollection(), Utility.CreateTemperatureRegionCollection(), Utility.CreateSolarCollection(), Utility.CreateTemperatureProfileCollection());

            // Create Manager with config and weather data
            var mgr = factory.CreateEnergyModelManager(clientId, config, weather);

            // Appliance collection
            var appliances = new ApplianceCollection();

            //Appliances - all data passed-in directly; these typically come from client appliances, but CAN be provided directly
            var applianceName = "Dryer";
            var appliance = new Appliance() { Name = applianceName };
            appliances.Add(appliance);

            //Profile Attributes - all data passed-in directly, as if it were populated by overlaying from multiple data sources internally
            var attributes = new ProfileAttributeCollection();
            attributes.Add(new ProfileAttribute() { Key = "dryer.count", OptionKey = "dryer.count.value", OptionValue = "0.9" });
            attributes.Add(new ProfileAttribute() { Key = "dryer.age", OptionKey = "dryer.age.value", OptionValue = "15" });
            attributes.Add(new ProfileAttribute() { Key = "dryer.size", OptionKey = "dryer.size.standard", OptionValue = "0" });
            attributes.Add(new ProfileAttribute() { Key = "dryer.fuel", OptionKey = "dryer.fuel.gas", OptionValue = "1" });
            attributes.Add(new ProfileAttribute() { Key = "dryer.loadswkly", OptionKey = "dryer.loadswkly.value", OptionValue = "7" });
            attributes.Add(new ProfileAttribute() { Key = "dryer.autoshuttoffuse", OptionKey = "dryer.autoshuttoffuse.yes", OptionValue = "1" });
            attributes.Add(new ProfileAttribute() { Key = "dryer.turnoffwhendone", OptionKey = "dryer.turnoffwhendone.yes", OptionValue = "1" });
            attributes.Add(new ProfileAttribute() { Key = "dryer.runfullload", OptionKey = "dryer.runfullload.yes", OptionValue = "1" });
            attributes.Add(new ProfileAttribute() { Key = "dryer.realmodel", OptionKey = "dryer.realmodel.no", OptionValue = "0" });
            attributes.Add(new ProfileAttribute() { Key = "dryer.actualuse", OptionKey = "dryer.actualuse.value", OptionValue = "0.0" });
            attributes.Add(new ProfileAttribute() { Key = "house.people", OptionKey = "house.people.value", OptionValue = "3" });

            // use this for overload call below;
            var regionParams = new RegionParams() { ApplianceRegionId = 1, TemperatureRegionId = 31, SolarRegionId = 1, State = "AZ" };

            // Execute Model
            var result = mgr.ExecuteEnergyModel(regionParams, null, attributes, appliances);

        }


        [TestMethod]
        public void ClothesWasher_Test()
        {
            var clientId = 87;
            var factory = new EnergyModelManagerFactory();
            var config = new EnergyModelConfiguration(Utility.CreateCalculationFactors());
            var weather = new Weather(Utility.CreateDegreeDayCollection(), Utility.CreateEvaporationCollection(), Utility.CreateTemperatureRegionCollection(), Utility.CreateSolarCollection(), Utility.CreateTemperatureProfileCollection());

            // Create Manager with config and weather data
            var mgr = factory.CreateEnergyModelManager(clientId, config, weather);

            // Appliance collection
            var appliances = new ApplianceCollection();

            //Appliances - all data passed-in directly; these typically come from client appliances, but CAN be provided directly
            var applianceName = "ClothesWasher";
            var appliance = new Appliance() { Name = applianceName };
            appliances.Add(appliance);

            //Profile Attributes - all data passed-in directly, as if it were populated by overlaying from multiple data sources internally
            var attributes = new ProfileAttributeCollection();
            attributes.Add(new ProfileAttribute() { Key = "clotheswasher.count", OptionKey = "clotheswasher.count.value", OptionValue = "1.0" });
            attributes.Add(new ProfileAttribute() { Key = "clotheswasher.age", OptionKey = "clotheswasher.age.value", OptionValue = "8" });
            attributes.Add(new ProfileAttribute() { Key = "clotheswasher.size", OptionKey = "clotheswasher.size.standard", OptionValue = "1" });
            attributes.Add(new ProfileAttribute() { Key = "clotheswasher.loadswkly", OptionKey = "clotheswasher.loadswkly.value", OptionValue = "5" });
            attributes.Add(new ProfileAttribute() { Key = "clotheswasher.loadsused", OptionKey = "clotheswasher.loadsused.large", OptionValue = "1.0" });
            attributes.Add(new ProfileAttribute() { Key = "clotheswasher.matchwaterload", OptionKey = "clotheswasher.matchwaterload.yes", OptionValue = "1" });
            attributes.Add(new ProfileAttribute() { Key = "clotheswasher.style", OptionKey = "clotheswasher.style.toploading", OptionValue = "0" });
            attributes.Add(new ProfileAttribute() { Key = "clotheswasher.efficiency", OptionKey = "clotheswasher.efficiency.standard", OptionValue = "0" });
            attributes.Add(new ProfileAttribute() { Key = "house.people", OptionKey = "house.people.value", OptionValue = "3" });
            attributes.Add(new ProfileAttribute() { Key = "clotheswasher.waterheaterfuel", OptionKey = "clotheswasher.waterheaterfuel.gas", OptionValue = "1" });
            attributes.Add(new ProfileAttribute() { Key = "clotheswasher.rinsetemperature", OptionKey = "clotheswasher.rinsetemperature.hot", OptionValue = "1.0" });
            attributes.Add(new ProfileAttribute() { Key = "clotheswasher.washtemperature", OptionKey = "clotheswasher.washtemperature.hot", OptionValue = "1.0" });
            attributes.Add(new ProfileAttribute() { Key = "clotheswasher.realmodel", OptionKey = "clotheswasher.realmodel.no", OptionValue = "0" });
            attributes.Add(new ProfileAttribute() { Key = "clotheswasher.energystar", OptionKey = "clotheswasher.energystar.no", OptionValue = "0" });

            // use this for overload call below;
            var regionParams = new RegionParams() { ApplianceRegionId = 1, TemperatureRegionId = 31, SolarRegionId = 1, State = "AZ" };

            // Execute Model
            var result = mgr.ExecuteEnergyModel(regionParams, null, attributes, appliances);

        }


        [TestMethod]
        public void DishWasher_Test()
        {
            var clientId = 87;
            var factory = new EnergyModelManagerFactory();
            var config = new EnergyModelConfiguration(Utility.CreateCalculationFactors());
            var weather = new Weather(Utility.CreateDegreeDayCollection(), Utility.CreateEvaporationCollection(), Utility.CreateTemperatureRegionCollection(), Utility.CreateSolarCollection(), Utility.CreateTemperatureProfileCollection());

            // Create Manager with config and weather data
            var mgr = factory.CreateEnergyModelManager(clientId, config, weather);

            // Appliance collection
            var appliances = new ApplianceCollection();

            //Appliances - all data passed-in directly; these typically come from client appliances, but CAN be provided directly
            var applianceName = "DishWasher";
            var appliance = new Appliance() { Name = applianceName };
            appliances.Add(appliance);

            //Profile Attributes - all data passed-in directly, as if it were populated by overlaying from multiple data sources internally
            var attributes = new ProfileAttributeCollection();
            attributes.Add(new ProfileAttribute() { Key = "dishwasher.count", OptionKey = "dishwasher.count.value", OptionValue = "1.0" });
            attributes.Add(new ProfileAttribute() { Key = "dishwasher.age", OptionKey = "dishwasher.age.value", OptionValue = "8" });
            attributes.Add(new ProfileAttribute() { Key = "dishwasher.size", OptionKey = "dishwasher.size.compact", OptionValue = "0" });
            attributes.Add(new ProfileAttribute() { Key = "dishwasher.loadswkly", OptionKey = "dishwasher.loadswkly.value", OptionValue = "5" });
            attributes.Add(new ProfileAttribute() { Key = "dishwasher.matchdirtlevel", OptionKey = "dishwasher.matchdirtlevel.no", OptionValue = "0" });
            attributes.Add(new ProfileAttribute() { Key = "dishwasher.energysaving", OptionKey = "dishwasher.energysaving.yes", OptionValue = "1" });
            attributes.Add(new ProfileAttribute() { Key = "dishwasher.boosterheater", OptionKey = "dishwasher.boosterheater.yes", OptionValue = "1" });
            attributes.Add(new ProfileAttribute() { Key = "dishwasher.washcycle", OptionKey = "dishwasher.washcycle.regular", OptionValue = "1" });
            attributes.Add(new ProfileAttribute() { Key = "house.people", OptionKey = "house.people.value", OptionValue = "3" });

            // use this for overload call below;
            var regionParams = new RegionParams() { ApplianceRegionId = 1, TemperatureRegionId = 31, SolarRegionId = 1, State = "AZ" };

            // Execute Model
            var result = mgr.ExecuteEnergyModel(regionParams, null, attributes, appliances);

        }


        [TestMethod]
        public void OutsideWater_Test()
        {
            var clientId = 87;
            var factory = new EnergyModelManagerFactory();
            var config = new EnergyModelConfiguration(Utility.CreateCalculationFactors());
            var weather = new Weather(Utility.CreateDegreeDayCollection(), Utility.CreateEvaporationCollection(), Utility.CreateTemperatureRegionCollection(), Utility.CreateSolarCollection(), Utility.CreateTemperatureProfileCollection());

            // Create Manager with config and weather data
            var mgr = factory.CreateEnergyModelManager(clientId, config, weather);

            // Appliance collection
            var appliances = new ApplianceCollection();

            //Appliances - all data passed-in directly; these typically come from client appliances, but CAN be provided directly
            var applianceName = "OutsideWater";
            var appliance = new Appliance() { Name = applianceName };
            appliances.Add(appliance);

            //Profile Attributes - all data passed-in directly, as if it were populated by overlaying from multiple data sources internally
            var attributes = new ProfileAttributeCollection();
            attributes.Add(new ProfileAttribute() { Key = "outsidewater.count", OptionKey = "outsidewater.count.value", OptionValue = "1" });
            attributes.Add(new ProfileAttribute() { Key = "outsidewater.lawnwatered", OptionKey = "outsidewater.lawnwatered.yes", OptionValue = "1" });
            attributes.Add(new ProfileAttribute() { Key = "outsidewater.gardenwatered", OptionKey = "outsidewater.gardenwatered.yes", OptionValue = "1" });
            attributes.Add(new ProfileAttribute() { Key = "outsidewater.lowwaterplants", OptionKey = "outsidewater.lowwaterplants.yes", OptionValue = "1" });
            attributes.Add(new ProfileAttribute() { Key = "outsidewater.lawnsprinkler", OptionKey = "outsidewater.lawnsprinkler.impact", OptionValue = "5" });
            attributes.Add(new ProfileAttribute() { Key = "outsidewater.gardensprinkler", OptionKey = "outsidewater.gardensprinkler.soaker_hose", OptionValue = "2" });
            attributes.Add(new ProfileAttribute() { Key = "outsidewater.lawnwinterminutes", OptionKey = "outsidewater.lawnwinterminutes.value", OptionValue = "0" });
            attributes.Add(new ProfileAttribute() { Key = "outsidewater.lawnsummerminutes", OptionKey = "outsidewater.lawnsummerminutes.value", OptionValue = "60" });
            attributes.Add(new ProfileAttribute() { Key = "outsidewater.lawnfallminutes", OptionKey = "outsidewater.lawnfallminutes.value", OptionValue = "0" });
            attributes.Add(new ProfileAttribute() { Key = "outsidewater.gardenwinterminutes", OptionKey = "outsidewater.gardenwinterminutes.value", OptionValue = "0" });
            attributes.Add(new ProfileAttribute() { Key = "outsidewater.gardensummerminutes", OptionKey = "outsidewater.gardensummerminutes.value", OptionValue = "60" });
            attributes.Add(new ProfileAttribute() { Key = "outsidewater.gardenfallminutes", OptionKey = "outsidewater.gardenfallminutes.value", OptionValue = "0" });
            attributes.Add(new ProfileAttribute() { Key = "outsidewater.lawnwinterfrqcy", OptionKey = "outsidewater.lawnwinterfrqcy.value", OptionValue = "0" });
            attributes.Add(new ProfileAttribute() { Key = "outsidewater.lawnsummerfrqcy", OptionKey = "outsidewater.lawnsummerfrqcy.value", OptionValue = "60" });
            attributes.Add(new ProfileAttribute() { Key = "outsidewater.lawnfallfrqcy", OptionKey = "outsidewater.lawnfallfrqcy.value", OptionValue = "0" });
            attributes.Add(new ProfileAttribute() { Key = "outsidewater.gardenwinterfrqcy", OptionKey = "outsidewater.gardenwinterfrqcy.value", OptionValue = "0" });
            attributes.Add(new ProfileAttribute() { Key = "outsidewater.gardensummerfrqcy", OptionKey = "outsidewater.gardensummerfrqcy.value", OptionValue = "60" });
            attributes.Add(new ProfileAttribute() { Key = "outsidewater.gardenfallfrqcy", OptionKey = "outsidewater.gardenfallfrqcy.value", OptionValue = "0" });
            attributes.Add(new ProfileAttribute() { Key = "outsidewater.lawnarea", OptionKey = "outsidewater.lawnarea.value", OptionValue = "2000" });
            attributes.Add(new ProfileAttribute() { Key = "outsidewater.gardenarea", OptionKey = "outsidewater.gardenarea.value", OptionValue = "300" });

            // use this for overload call below;
            var regionParams = new RegionParams() { ApplianceRegionId = 1, TemperatureRegionId = 31, SolarRegionId = 1, State = "AZ" };

            // Execute Model
            var result = mgr.ExecuteEnergyModel(regionParams, null, attributes, appliances);

        }


        [TestMethod]
        public void WaterHeater_Test()
        {
            var clientId = 87;
            var factory = new EnergyModelManagerFactory();
            var config = new EnergyModelConfiguration(Utility.CreateCalculationFactors());
            var weather = new Weather(Utility.CreateDegreeDayCollection(), Utility.CreateEvaporationCollection(), Utility.CreateTemperatureRegionCollection(), Utility.CreateSolarCollection(), Utility.CreateTemperatureProfileCollection());

            // Create Manager with config and weather data
            var mgr = factory.CreateEnergyModelManager(clientId, config, weather);

            // Appliance collection
            var appliances = new ApplianceCollection();

            //Appliances - all data passed-in directly; these typically come from client appliances, but CAN be provided directly
            var applianceName = "WaterHeater";
            var appliance = new Appliance() { Name = applianceName };
            appliances.Add(appliance);
            appliances.IsStandAlone = true;

            //Profile Attributes - all data passed-in directly, as if it were populated by overlaying from multiple data sources internally
            //Water Heater uses a lot of attributes from other appliances, hence the big list here!
            var attributes = new ProfileAttributeCollection();
            attributes.Add(new ProfileAttribute() { Key = "shower.count", OptionKey = "shower.count.value", OptionValue = "1" });
            attributes.Add(new ProfileAttribute() { Key = "shower.showerleakrate", OptionKey = "shower.showerleakrate.normal", OptionValue = "8.6" });
            attributes.Add(new ProfileAttribute() { Key = "shower.showerheadflow", OptionKey = "shower.showerheadflow.normal", OptionValue = "2.1" });
            attributes.Add(new ProfileAttribute() { Key = "shower.leakingshowerqty", OptionKey = "shower.leakingshowerqty.value", OptionValue = "1" });
            attributes.Add(new ProfileAttribute() { Key = "shower.showerswkly", OptionKey = "shower.showerswkly.value", OptionValue = "7" });
            attributes.Add(new ProfileAttribute() { Key = "shower.bathswkly", OptionKey = "shower.bathswkly.value", OptionValue = "1" });
            attributes.Add(new ProfileAttribute() { Key = "shower.showerlength", OptionKey = "shower.showerlength.value", OptionValue = "7" });
            attributes.Add(new ProfileAttribute() { Key = "house.people", OptionKey = "house.people.value", OptionValue = "3" });

            attributes.Add(new ProfileAttribute() { Key = "sink.count", OptionKey = "sink.count.value", OptionValue = "2" });
            attributes.Add(new ProfileAttribute() { Key = "sink.faucetleakrate", OptionKey = "sink.faucetleakrate.normal", OptionValue = "8.6" });
            attributes.Add(new ProfileAttribute() { Key = "sink.sinkflow", OptionKey = "sink.sinkflow.normal", OptionValue = "1.14" });
            attributes.Add(new ProfileAttribute() { Key = "sink.leakingfaucetsqty", OptionKey = "sink.leakingfaucetsqty.value", OptionValue = "1" });
            attributes.Add(new ProfileAttribute() { Key = "cooktop.breakfastswkly", OptionKey = "cooktop.breakfastswkly.value", OptionValue = "5" });
            attributes.Add(new ProfileAttribute() { Key = "cooktop.luncheswkly", OptionKey = "cooktop.luncheswkly.value", OptionValue = "5" });
            attributes.Add(new ProfileAttribute() { Key = "cooktop.dinnerswkly", OptionKey = "cooktop.dinnerswkly.value", OptionValue = "5" });

            attributes.Add(new ProfileAttribute() { Key = "clotheswasher.count", OptionKey = "clotheswasher.count.value", OptionValue = "1.0" });
            attributes.Add(new ProfileAttribute() { Key = "clotheswasher.age", OptionKey = "clotheswasher.age.value", OptionValue = "8" });
            attributes.Add(new ProfileAttribute() { Key = "clotheswasher.size", OptionKey = "clotheswasher.size.standard", OptionValue = "1" });
            attributes.Add(new ProfileAttribute() { Key = "clotheswasher.loadswkly", OptionKey = "clotheswasher.loadswkly.value", OptionValue = "5" });
            attributes.Add(new ProfileAttribute() { Key = "clotheswasher.loadsused", OptionKey = "clotheswasher.loadsused.large", OptionValue = "1.0" });
            attributes.Add(new ProfileAttribute() { Key = "clotheswasher.matchwaterload", OptionKey = "clotheswasher.matchwaterload.yes", OptionValue = "1" });
            attributes.Add(new ProfileAttribute() { Key = "clotheswasher.style", OptionKey = "clotheswasher.style.toploading", OptionValue = "0" });
            attributes.Add(new ProfileAttribute() { Key = "clotheswasher.efficiency", OptionKey = "clotheswasher.efficiency.standard", OptionValue = "0" });
            attributes.Add(new ProfileAttribute() { Key = "clotheswasher.waterheaterfuel", OptionKey = "clotheswasher.waterheaterfuel.gas", OptionValue = "1" });
            attributes.Add(new ProfileAttribute() { Key = "clotheswasher.rinsetemperature", OptionKey = "clotheswasher.rinsetemperature.hot", OptionValue = "1.0" });
            attributes.Add(new ProfileAttribute() { Key = "clotheswasher.washtemperature", OptionKey = "clotheswasher.washtemperature.hot", OptionValue = "1.0" });
            attributes.Add(new ProfileAttribute() { Key = "clotheswasher.realmodel", OptionKey = "clotheswasher.realmodel.no", OptionValue = "0" });
            attributes.Add(new ProfileAttribute() { Key = "clotheswasher.energystar", OptionKey = "clotheswasher.energystar.no", OptionValue = "0" });

            attributes.Add(new ProfileAttribute() { Key = "dishwasher.count", OptionKey = "dishwasher.count.value", OptionValue = "1.0" });
            attributes.Add(new ProfileAttribute() { Key = "dishwasher.age", OptionKey = "dishwasher.age.value", OptionValue = "8" });
            attributes.Add(new ProfileAttribute() { Key = "dishwasher.size", OptionKey = "dishwasher.size.compact", OptionValue = "0" });
            attributes.Add(new ProfileAttribute() { Key = "dishwasher.loadswkly", OptionKey = "dishwasher.loadswkly.value", OptionValue = "5" });
            attributes.Add(new ProfileAttribute() { Key = "dishwasher.matchdirtlevel", OptionKey = "dishwasher.matchdirtlevel.no", OptionValue = "0" });
            attributes.Add(new ProfileAttribute() { Key = "dishwasher.energysaving", OptionKey = "dishwasher.energysaving.yes", OptionValue = "1" });
            attributes.Add(new ProfileAttribute() { Key = "dishwasher.boosterheater", OptionKey = "dishwasher.boosterheater.yes", OptionValue = "1" });
            attributes.Add(new ProfileAttribute() { Key = "dishwasher.washcycle", OptionKey = "dishwasher.washcycle.regular", OptionValue = "1" });

            attributes.Add(new ProfileAttribute() { Key = "waterheater.count", OptionKey = "waterheater.count.value", OptionValue = "1.0" });
            attributes.Add(new ProfileAttribute() { Key = "waterheater.realmodel", OptionKey = "waterheater.realmodel.no", OptionValue = "0" });
            //attributes.Add(new ProfileAttribute() { Key = "waterheater.count", OptionKey = "waterheater.count.value", OptionValue = "1.0" });
            attributes.Add(new ProfileAttribute() { Key = "waterheater.age", OptionKey = "waterheater.age.value", OptionValue = "12" });
            attributes.Add(new ProfileAttribute() { Key = "waterheater.highefficiency", OptionKey = "waterheater.highefficiency.yes", OptionValue = "1" });
            attributes.Add(new ProfileAttribute() { Key = "waterheater.pilotlight", OptionKey = "waterheater.pilotlight.yes", OptionValue = "1" });
            attributes.Add(new ProfileAttribute() { Key = "waterheater.fluedamper", OptionKey = "waterheater.fluedamper.yes", OptionValue = "1" });
            attributes.Add(new ProfileAttribute() { Key = "waterheater.pipesinsulation", OptionKey = "waterheater.pipesinsulation.yes", OptionValue = "1" });
            attributes.Add(new ProfileAttribute() { Key = "waterheater.tankinsulation", OptionKey = "waterheater.tankinsulation.yes", OptionValue = "1" });
            attributes.Add(new ProfileAttribute() { Key = "waterheater.timer", OptionKey = "waterheater.timer.no", OptionValue = "0" });
            attributes.Add(new ProfileAttribute() { Key = "waterheater.solarheater", OptionKey = "waterheater.solarheater.no", OptionValue = "0" });
            attributes.Add(new ProfileAttribute() { Key = "waterheater.solaractive", OptionKey = "waterheater.solaractive.no", OptionValue = "0" });
            attributes.Add(new ProfileAttribute() { Key = "waterheater.heatrecovery", OptionKey = "waterheater.heatrecovery.yes", OptionValue = "1" });
            attributes.Add(new ProfileAttribute() { Key = "waterheater.heattraps", OptionKey = "waterheater.heattraps.yes", OptionValue = "1" });
            attributes.Add(new ProfileAttribute() { Key = "waterheater.location", OptionKey = "waterheater.location.basement", OptionValue = "0" });

            // elctric heat pump water heater test!!!!
            attributes.Add(new ProfileAttribute() { Key = "waterheater.fuel", OptionKey = "waterheater.fuel.electric", OptionValue = "1.0" });
            attributes.Add(new ProfileAttribute() { Key = "waterheater.style", OptionKey = "waterheater.style.heatpump", OptionValue = "1.0" });
            attributes.Add(new ProfileAttribute() { Key = "waterheater.servicefrequency", OptionKey = "waterheater.servicefrequency.seldom", OptionValue = "1.0" });
            attributes.Add(new ProfileAttribute() { Key = "waterheater.tanksize", OptionKey = "waterheater.tanksize.value", OptionValue = "50" });
            attributes.Add(new ProfileAttribute() { Key = "waterheater.temperature", OptionKey = "waterheater.temperature.value", OptionValue = "120" });
            attributes.Add(new ProfileAttribute() { Key = "waterheater.solarorientation", OptionKey = "waterheater.solarorientation.optimum", OptionValue = "1.0" });
            attributes.Add(new ProfileAttribute() { Key = "heatsystem.style", OptionKey = "heatsystem.style.airsource_heatpump", OptionValue = "1.0" });
            attributes.Add(new ProfileAttribute() { Key = "waterheater.solarsize", OptionKey = "waterheater.solarsize.value", OptionValue = "100" });
            attributes.Add(new ProfileAttribute() { Key = "house.groundwatertemp", OptionKey = "house.groundwatertemp.value", OptionValue = "58" });
            attributes.Add(new ProfileAttribute() { Key = "centralac.count", OptionKey = "centralac.count.value", OptionValue = "1.0" });
            attributes.Add(new ProfileAttribute() { Key = "heatsystem.count", OptionKey = "heatsystem.count.value", OptionValue = "1.0" });

            // use this for overload call below;
            var regionParams = new RegionParams() { ApplianceRegionId = 1, TemperatureRegionId = 31, SolarRegionId = 1, State = "AZ" };

            // Execute Model
            var result = mgr.ExecuteEnergyModel(regionParams, null, attributes, appliances);

        }

        
        /// <summary>
        /// test with CT (colder climate) for waterheater for comparison numbers for electric, water heater heat pump changes
        /// </summary>
        [TestMethod]
        public void WaterHeater_Test_CoolRegion()
        {
            var clientId = 224;
            var factory = new EnergyModelManagerFactory();
            var config = new EnergyModelConfiguration(Utility.CreateCalculationFactors());
            var weather = new Weather(Utility.CreateDegreeDayCollection(), Utility.CreateEvaporationCollection(), Utility.CreateTemperatureRegionCollection(), Utility.CreateSolarCollection(), Utility.CreateTemperatureProfileCollection());

            // Create Manager with config and weather data
            var mgr = factory.CreateEnergyModelManager(clientId, config, weather);

            // Appliance collection
            var appliances = new ApplianceCollection();

            //Appliances - all data passed-in directly; these typically come from client appliances, but CAN be provided directly
            var applianceName = "WaterHeater";
            var appliance = new Appliance() { Name = applianceName };
            appliances.Add(appliance);
            appliances.IsStandAlone = true;

            //Profile Attributes - all data passed-in directly, as if it were populated by overlaying from multiple data sources internally
            //Water Heater uses a lot of attributes from other appliances, hence the big list here!
            var attributes = new ProfileAttributeCollection();
            attributes.Add(new ProfileAttribute() { Key = "shower.count", OptionKey = "shower.count.value", OptionValue = "1" });
            attributes.Add(new ProfileAttribute() { Key = "shower.showerleakrate", OptionKey = "shower.showerleakrate.normal", OptionValue = "8.6" });
            attributes.Add(new ProfileAttribute() { Key = "shower.showerheadflow", OptionKey = "shower.showerheadflow.normal", OptionValue = "2.1" });
            attributes.Add(new ProfileAttribute() { Key = "shower.leakingshowerqty", OptionKey = "shower.leakingshowerqty.value", OptionValue = "1" });
            attributes.Add(new ProfileAttribute() { Key = "shower.showerswkly", OptionKey = "shower.showerswkly.value", OptionValue = "7" });
            attributes.Add(new ProfileAttribute() { Key = "shower.bathswkly", OptionKey = "shower.bathswkly.value", OptionValue = "1" });
            attributes.Add(new ProfileAttribute() { Key = "shower.showerlength", OptionKey = "shower.showerlength.value", OptionValue = "7" });
            attributes.Add(new ProfileAttribute() { Key = "house.people", OptionKey = "house.people.value", OptionValue = "3" });

            attributes.Add(new ProfileAttribute() { Key = "sink.count", OptionKey = "sink.count.value", OptionValue = "2" });
            attributes.Add(new ProfileAttribute() { Key = "sink.faucetleakrate", OptionKey = "sink.faucetleakrate.normal", OptionValue = "8.6" });
            attributes.Add(new ProfileAttribute() { Key = "sink.sinkflow", OptionKey = "sink.sinkflow.normal", OptionValue = "1.14" });
            attributes.Add(new ProfileAttribute() { Key = "sink.leakingfaucetsqty", OptionKey = "sink.leakingfaucetsqty.value", OptionValue = "1" });
            attributes.Add(new ProfileAttribute() { Key = "cooktop.breakfastswkly", OptionKey = "cooktop.breakfastswkly.value", OptionValue = "5" });
            attributes.Add(new ProfileAttribute() { Key = "cooktop.luncheswkly", OptionKey = "cooktop.luncheswkly.value", OptionValue = "5" });
            attributes.Add(new ProfileAttribute() { Key = "cooktop.dinnerswkly", OptionKey = "cooktop.dinnerswkly.value", OptionValue = "5" });

            attributes.Add(new ProfileAttribute() { Key = "clotheswasher.count", OptionKey = "clotheswasher.count.value", OptionValue = "1.0" });
            attributes.Add(new ProfileAttribute() { Key = "clotheswasher.age", OptionKey = "clotheswasher.age.value", OptionValue = "8" });
            attributes.Add(new ProfileAttribute() { Key = "clotheswasher.size", OptionKey = "clotheswasher.size.standard", OptionValue = "1" });
            attributes.Add(new ProfileAttribute() { Key = "clotheswasher.loadswkly", OptionKey = "clotheswasher.loadswkly.value", OptionValue = "5" });
            attributes.Add(new ProfileAttribute() { Key = "clotheswasher.loadsused", OptionKey = "clotheswasher.loadsused.large", OptionValue = "1.0" });
            attributes.Add(new ProfileAttribute() { Key = "clotheswasher.matchwaterload", OptionKey = "clotheswasher.matchwaterload.yes", OptionValue = "1" });
            attributes.Add(new ProfileAttribute() { Key = "clotheswasher.style", OptionKey = "clotheswasher.style.toploading", OptionValue = "0" });
            attributes.Add(new ProfileAttribute() { Key = "clotheswasher.efficiency", OptionKey = "clotheswasher.efficiency.standard", OptionValue = "0" });
            attributes.Add(new ProfileAttribute() { Key = "clotheswasher.waterheaterfuel", OptionKey = "clotheswasher.waterheaterfuel.gas", OptionValue = "1" });
            attributes.Add(new ProfileAttribute() { Key = "clotheswasher.rinsetemperature", OptionKey = "clotheswasher.rinsetemperature.hot", OptionValue = "1.0" });
            attributes.Add(new ProfileAttribute() { Key = "clotheswasher.washtemperature", OptionKey = "clotheswasher.washtemperature.hot", OptionValue = "1.0" });
            attributes.Add(new ProfileAttribute() { Key = "clotheswasher.realmodel", OptionKey = "clotheswasher.realmodel.no", OptionValue = "0" });
            attributes.Add(new ProfileAttribute() { Key = "clotheswasher.energystar", OptionKey = "clotheswasher.energystar.no", OptionValue = "0" });

            attributes.Add(new ProfileAttribute() { Key = "dishwasher.count", OptionKey = "dishwasher.count.value", OptionValue = "1.0" });
            attributes.Add(new ProfileAttribute() { Key = "dishwasher.age", OptionKey = "dishwasher.age.value", OptionValue = "8" });
            attributes.Add(new ProfileAttribute() { Key = "dishwasher.size", OptionKey = "dishwasher.size.compact", OptionValue = "0" });
            attributes.Add(new ProfileAttribute() { Key = "dishwasher.loadswkly", OptionKey = "dishwasher.loadswkly.value", OptionValue = "5" });
            attributes.Add(new ProfileAttribute() { Key = "dishwasher.matchdirtlevel", OptionKey = "dishwasher.matchdirtlevel.no", OptionValue = "0" });
            attributes.Add(new ProfileAttribute() { Key = "dishwasher.energysaving", OptionKey = "dishwasher.energysaving.yes", OptionValue = "1" });
            attributes.Add(new ProfileAttribute() { Key = "dishwasher.boosterheater", OptionKey = "dishwasher.boosterheater.yes", OptionValue = "1" });
            attributes.Add(new ProfileAttribute() { Key = "dishwasher.washcycle", OptionKey = "dishwasher.washcycle.regular", OptionValue = "1" });

            attributes.Add(new ProfileAttribute() { Key = "waterheater.count", OptionKey = "waterheater.count.value", OptionValue = "1.0" });
            attributes.Add(new ProfileAttribute() { Key = "waterheater.realmodel", OptionKey = "waterheater.realmodel.no", OptionValue = "0" });
            //attributes.Add(new ProfileAttribute() { Key = "waterheater.count", OptionKey = "waterheater.count.value", OptionValue = "1.0" });
            attributes.Add(new ProfileAttribute() { Key = "waterheater.age", OptionKey = "waterheater.age.value", OptionValue = "12" });
            attributes.Add(new ProfileAttribute() { Key = "waterheater.highefficiency", OptionKey = "waterheater.highefficiency.yes", OptionValue = "1" });
            attributes.Add(new ProfileAttribute() { Key = "waterheater.pilotlight", OptionKey = "waterheater.pilotlight.yes", OptionValue = "1" });
            attributes.Add(new ProfileAttribute() { Key = "waterheater.fluedamper", OptionKey = "waterheater.fluedamper.yes", OptionValue = "1" });
            attributes.Add(new ProfileAttribute() { Key = "waterheater.pipesinsulation", OptionKey = "waterheater.pipesinsulation.yes", OptionValue = "1" });
            attributes.Add(new ProfileAttribute() { Key = "waterheater.tankinsulation", OptionKey = "waterheater.tankinsulation.yes", OptionValue = "1" });
            attributes.Add(new ProfileAttribute() { Key = "waterheater.timer", OptionKey = "waterheater.timer.no", OptionValue = "0" });
            attributes.Add(new ProfileAttribute() { Key = "waterheater.solarheater", OptionKey = "waterheater.solarheater.no", OptionValue = "0" });
            attributes.Add(new ProfileAttribute() { Key = "waterheater.solaractive", OptionKey = "waterheater.solaractive.no", OptionValue = "0" });
            attributes.Add(new ProfileAttribute() { Key = "waterheater.heatrecovery", OptionKey = "waterheater.heatrecovery.yes", OptionValue = "1" });
            attributes.Add(new ProfileAttribute() { Key = "waterheater.heattraps", OptionKey = "waterheater.heattraps.yes", OptionValue = "1" });
            attributes.Add(new ProfileAttribute() { Key = "waterheater.location", OptionKey = "waterheater.location.basement", OptionValue = "0" });

            // elctric heat pump water heater test!!!!
            attributes.Add(new ProfileAttribute() { Key = "waterheater.fuel", OptionKey = "waterheater.fuel.electric", OptionValue = "1.0" });
            attributes.Add(new ProfileAttribute() { Key = "waterheater.style", OptionKey = "waterheater.style.heatpump", OptionValue = "1.0" });
            attributes.Add(new ProfileAttribute() { Key = "waterheater.servicefrequency", OptionKey = "waterheater.servicefrequency.seldom", OptionValue = "1.0" });
            attributes.Add(new ProfileAttribute() { Key = "waterheater.tanksize", OptionKey = "waterheater.tanksize.value", OptionValue = "50" });
            attributes.Add(new ProfileAttribute() { Key = "waterheater.temperature", OptionKey = "waterheater.temperature.value", OptionValue = "120" });
            attributes.Add(new ProfileAttribute() { Key = "waterheater.solarorientation", OptionKey = "waterheater.solarorientation.optimum", OptionValue = "1.0" });
            attributes.Add(new ProfileAttribute() { Key = "heatsystem.style", OptionKey = "heatsystem.style.airsource_heatpump", OptionValue = "1.0" });
            attributes.Add(new ProfileAttribute() { Key = "waterheater.solarsize", OptionKey = "waterheater.solarsize.value", OptionValue = "100" });
            attributes.Add(new ProfileAttribute() { Key = "house.groundwatertemp", OptionKey = "house.groundwatertemp.value", OptionValue = "58" });
            attributes.Add(new ProfileAttribute() { Key = "centralac.count", OptionKey = "centralac.count.value", OptionValue = "1.0" });
            attributes.Add(new ProfileAttribute() { Key = "heatsystem.count", OptionKey = "heatsystem.count.value", OptionValue = "1.0" });

            // use this for overload call below using region params for CT similar to UIL electric client;
            var regionParams = new RegionParams() { ApplianceRegionId = 2, TemperatureRegionId = 52, SolarRegionId = 2, State = "CT" };

            // Execute Model
            var result = mgr.ExecuteEnergyModel(regionParams, null, attributes, appliances);

        }


        [TestMethod]
        public void Pool_Test()
        {
            var clientId = 87;
            var factory = new EnergyModelManagerFactory();
            var config = new EnergyModelConfiguration(Utility.CreateCalculationFactors());
            var weather = new Weather(Utility.CreateDegreeDayCollection(), Utility.CreateEvaporationCollection(), Utility.CreateTemperatureRegionCollection(), Utility.CreateSolarCollection(), Utility.CreateTemperatureProfileCollection());

            // Create Manager with config and weather data
            var mgr = factory.CreateEnergyModelManager(clientId, config, weather);

            // Appliance collection
            var appliances = new ApplianceCollection();

            //Appliances - all data passed-in directly; these typically come from client appliances, but CAN be provided directly
            var applianceName = "Pool";
            var appliance = new Appliance() { Name = applianceName };
            appliances.Add(appliance);

            //Profile Attributes - all data passed-in directly, as if it were populated by overlaying from multiple data sources internally
            var attributes = new ProfileAttributeCollection();
            attributes.Add(new ProfileAttribute() { Key = "pool.count", OptionKey = "pool.count.value", OptionValue = "1.0" });
            attributes.Add(new ProfileAttribute() { Key = "pool.horsepower", OptionKey = "pool.horsepower.value", OptionValue = "1.5" });
            attributes.Add(new ProfileAttribute() { Key = "pool.size", OptionKey = "pool.size.value", OptionValue = "500" });
            attributes.Add(new ProfileAttribute() { Key = "pool.collectorratio", OptionKey = "pool.collectorratio.value", OptionValue = "1" });
            attributes.Add(new ProfileAttribute() { Key = "pool.siteconditions", OptionKey = "pool.siteconditions.value", OptionValue = "1" });
            attributes.Add(new ProfileAttribute() { Key = "pool.pumpusagehrs", OptionKey = "pool.pumpusagehrs.value", OptionValue = "9" });
            attributes.Add(new ProfileAttribute() { Key = "pool.coverusage", OptionKey = "pool.coverusage.useregularly", OptionValue = "0" });
            attributes.Add(new ProfileAttribute() { Key = "pool.temperature", OptionKey = "pool.temperature.value", OptionValue = "84" });
            attributes.Add(new ProfileAttribute() { Key = "pool.timer", OptionKey = "pool.timer.no", OptionValue = "0" });
            attributes.Add(new ProfileAttribute() { Key = "pool.fuel", OptionKey = "pool.fuel.electric", OptionValue = "0" });
            attributes.Add(new ProfileAttribute() { Key = "pool.heatingfrqcy", OptionKey = "pool.heatingfrqcy.seldom", OptionValue = "0.5" });
            attributes.Add(new ProfileAttribute() { Key = "pool.heater", OptionKey = "pool.heater.no", OptionValue = "0" });
            attributes.Add(new ProfileAttribute() { Key = "pool.solarheater", OptionKey = "pool.solarheater.yes", OptionValue = "1" });
            attributes.Add(new ProfileAttribute() { Key = "pool.efficientpump", OptionKey = "pool.efficientpump.no", OptionValue = "0" });
            attributes.Add(new ProfileAttribute() { Key = "pool.turnoveronce", OptionKey = "pool.turnoveronce.yes", OptionValue = "1" });

            // use this for overload call below;
            var regionParams = new RegionParams() { ApplianceRegionId = 1, TemperatureRegionId = 31, SolarRegionId = 1, State = "AZ" };

            // Execute Model
            var result = mgr.ExecuteEnergyModel(regionParams, null, attributes, appliances);

        }


        [TestMethod]
        public void SecondaryHeating_Test()
        {
            var clientId = 87;
            var factory = new EnergyModelManagerFactory();
            var config = new EnergyModelConfiguration(Utility.CreateCalculationFactors());
            var weather = new Weather(Utility.CreateDegreeDayCollection(), Utility.CreateEvaporationCollection(), Utility.CreateTemperatureRegionCollection(), Utility.CreateSolarCollection(), Utility.CreateTemperatureProfileCollection());

            // Create Manager with config and weather data
            var mgr = factory.CreateEnergyModelManager(clientId, config, weather);

            // Appliance collection
            var appliances = new ApplianceCollection();

            //Appliances - all data passed-in directly; these typically come from client appliances, but CAN be provided directly
            var applianceName = "SecondaryHeating";
            var appliance = new Appliance() { Name = applianceName };
            appliances.Add(appliance);

            //Profile Attributes - all data passed-in directly, as if it were populated by overlaying from multiple data sources internally
            //Water Heater uses a lot of attributes from other appliances, hence the big list here!
            var attributes = new ProfileAttributeCollection();
            attributes.Add(new ProfileAttribute() { Key = "secondaryheating.count", OptionKey = "secondaryheating.count.value", OptionValue = "1" });
            attributes.Add(new ProfileAttribute() { Key = "secondaryheating.age", OptionKey = "secondaryheating.age.value", OptionValue = "12" });
            attributes.Add(new ProfileAttribute() { Key = "secondaryheating.servicearea", OptionKey = "secondaryheating.servicearea.value", OptionValue = "0.25" });
            attributes.Add(new ProfileAttribute() { Key = "secondaryheating.usage", OptionKey = "secondaryheating.usage.occasionally", OptionValue = "0.5" });
            attributes.Add(new ProfileAttribute() { Key = "secondaryheating.fuel", OptionKey = "secondaryheating.fuel.gas", OptionValue = "1" });

            attributes.Add(new ProfileAttribute() { Key = "heatsystem.count", OptionKey = "heatsystem.count.value", OptionValue = "1" });
            attributes.Add(new ProfileAttribute() { Key = "heatsystem.fuel", OptionKey = "heatsystem.fuel.gas", OptionValue = "1" });

            // use this for overload call below;
            var regionParams = new RegionParams() { ApplianceRegionId = 1, TemperatureRegionId = 31, SolarRegionId = 1, State = "AZ" };

            // Execute Model
            var result = mgr.ExecuteEnergyModel(regionParams, null, attributes, appliances);

        }


        [TestMethod]
        public void HeatSystem_Test()
        {
            var clientId = 87;
            var factory = new EnergyModelManagerFactory();
            var config = new EnergyModelConfiguration(Utility.CreateCalculationFactors());
            var weather = new Weather(Utility.CreateDegreeDayCollection(), Utility.CreateEvaporationCollection(), Utility.CreateTemperatureRegionCollection(), Utility.CreateSolarCollection(), Utility.CreateTemperatureProfileCollection());

            // Create Manager with config and weather data
            var mgr = factory.CreateEnergyModelManager(clientId, config, weather);

            // Appliance collection
            var appliances = new ApplianceCollection();

            //Appliances - all data passed-in directly; these typically come from client appliances, but CAN be provided directly
            var applianceName = "HeatSystem";
            var appliance = new Appliance() { Name = applianceName };
            appliances.Add(appliance);

            //Profile Attributes - all data passed-in directly, as if it were populated by overlaying from multiple data sources internally
            //Water Heater uses a lot of attributes from other appliances, hence the big list here!
            var attributes = new ProfileAttributeCollection();
            attributes.Add(new ProfileAttribute() { Key = "heatsystem.count", OptionKey = "heatsystem.count.value", OptionValue = "1" });
            attributes.Add(new ProfileAttribute() { Key = "heatsystem.fuel", OptionKey = "heatsystem.fuel.gas", OptionValue = "1" });

            // use this for overload call below;
            var regionParams = new RegionParams() { ApplianceRegionId = 1, TemperatureRegionId = 31, SolarRegionId = 1, State = "AZ" };

            // Execute Model
            var result = mgr.ExecuteEnergyModel(regionParams, null, attributes, appliances);

        }


        [TestMethod]
        public void CentralAC_Test()
        {
            var clientId = 87;
            var factory = new EnergyModelManagerFactory();
            var config = new EnergyModelConfiguration(Utility.CreateCalculationFactors());
            var weather = new Weather(Utility.CreateDegreeDayCollection(), Utility.CreateEvaporationCollection(), Utility.CreateTemperatureRegionCollection(), Utility.CreateSolarCollection(), Utility.CreateTemperatureProfileCollection());

            // Create Manager with config and weather data
            var mgr = factory.CreateEnergyModelManager(clientId, config, weather);

            // Appliance collection
            var appliances = new ApplianceCollection();

            //Appliances - all data passed-in directly; these typically come from client appliances, but CAN be provided directly
            var applianceName = "CentralAC";
            var appliance = new Appliance() { Name = applianceName };
            appliances.Add(appliance);

            //Profile Attributes - all data passed-in directly, as if it were populated by overlaying from multiple data sources internally
            //Water Heater uses a lot of attributes from other appliances, hence the big list here!
            var attributes = new ProfileAttributeCollection();
            attributes.Add(new ProfileAttribute() { Key = "centralac.count", OptionKey = "centralac.count.value", OptionValue = "1" });

            // use this for overload call below;
            var regionParams = new RegionParams() { ApplianceRegionId = 1, TemperatureRegionId = 31, SolarRegionId = 1, State = "AZ" };

            // Execute Model
            var result = mgr.ExecuteEnergyModel(regionParams, null, attributes, appliances);

        }


    }
}
