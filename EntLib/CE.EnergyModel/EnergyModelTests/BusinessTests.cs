﻿using System;
using System.Diagnostics;
using CE.EnergyModel;
using CE.EnergyModel.Entities;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace EnergyModelTests
{
    [TestClass]
    public class BusinessTests
    {

        [TestInitialize]
        public void TestInit()
        {
            // This initializes the automapper!
            new CE.ContentModel.AutoMapperBootStrapper().BootStrap();
        }

        [TestMethod]
        public void BEM_Business_Model_Default()
        {
            const int clientId = 87;
            var factory = new EnergyModelManagerFactory();

            var mgr = factory.CreateEnergyModelManager(clientId);

            var attributes = new ProfileAttributeCollection()
            {
                new ProfileAttribute() { Key = "premisetype", OptionKey = "premisetype.business", OptionValue = "1" }
            };

            var regionParams = new RegionParams() { ApplianceRegionId = 1, TemperatureRegionId = 31, SolarRegionId = 1, State = "AZ" };

            var result = mgr.ExecuteEnergyModel(regionParams, null, attributes, null);

            CollectionAssert.AllItemsAreNotNull(result.Appliances);
            CollectionAssert.AllItemsAreNotNull(result.EndUses);

        }


        [TestMethod]
        public void BEM_Business_Model_conveniencestore_1()
        {
            const int clientId = 87;
            var mgr = new EnergyModelManagerFactory().CreateEnergyModelManager(clientId);

            var attributes = new ProfileAttributeCollection()
            {
                new ProfileAttribute() { Key = "premisetype", OptionKey = "premisetype.business", OptionValue = "1" },
                new ProfileAttribute() { Key = "business.buildingcategory", OptionKey = "buildingcategory.conveniencestore", OptionValue = "0" },
                new ProfileAttribute() { Key = "business.totalarea", OptionKey = "business.totalarea.value", OptionValue = "2000" }
            };

            var regionParams = new RegionParams() { ApplianceRegionId = 1, TemperatureRegionId = 31, SolarRegionId = 1, State = "AZ" };

            var result = mgr.ExecuteEnergyModel(regionParams, null, attributes, null);

            WriteResultsToConsole(result);

            CollectionAssert.AllItemsAreNotNull(result.Appliances);
            CollectionAssert.AllItemsAreNotNull(result.EndUses);
        }

        [TestMethod]
        public void BEM_Business_Model_collegeuniversity_1()
        {
            const int clientId = 87;
            var mgr = new EnergyModelManagerFactory().CreateEnergyModelManager(clientId);

            var attributes = new ProfileAttributeCollection()
            {
                new ProfileAttribute() { Key = "premisetype", OptionKey = "premisetype.business", OptionValue = "1" },
                new ProfileAttribute() { Key = "business.buildingcategory", OptionKey = "buildingcategory.collegeuniversity", OptionValue = "0" },
                new ProfileAttribute() { Key = "business.totalarea", OptionKey = "business.totalarea.value", OptionValue = "110000" }
            };

            var regionParams = new RegionParams() { ApplianceRegionId = 1, TemperatureRegionId = 31, SolarRegionId = 1, State = "AZ" };

            var result = mgr.ExecuteEnergyModel(regionParams, null, attributes, null);

            WriteResultsToConsole(result);

            CollectionAssert.AllItemsAreNotNull(result.Appliances);
            CollectionAssert.AllItemsAreNotNull(result.EndUses);
        }

        [TestMethod]
        public void BEM_Business_Model_collegeuniversity_businesscomputers()
        {
            const int clientId = 87;
            var mgr = new EnergyModelManagerFactory().CreateEnergyModelManager(clientId);

            var attributes = new ProfileAttributeCollection()
            {
                new ProfileAttribute() { Key = "premisetype", OptionKey = "premisetype.business", OptionValue = "1" },
                new ProfileAttribute() { Key = "business.buildingcategory", OptionKey = "buildingcategory.collegeuniversity", OptionValue = "0" },
                new ProfileAttribute() { Key = "business.totalarea", OptionKey = "business.totalarea.value", OptionValue = "110000" }
            };

            // Appliance collection
            var appliances = new ApplianceCollection { new Appliance() { Name = "businesscomputers" } };
            appliances.IsStandAlone = true;

            var regionParams = new RegionParams() { ApplianceRegionId = 1, TemperatureRegionId = 31, SolarRegionId = 1, State = "AZ" };

            var result = mgr.ExecuteEnergyModel(regionParams, null, attributes, appliances);

            WriteResultsToConsole(result);

            CollectionAssert.AllItemsAreNotNull(result.Appliances);
            CollectionAssert.AllItemsAreNotNull(result.EndUses);
        }

        [TestMethod]
        public void BEM_Business_Model_collegeuniversity_businesscooking()
        {
            const int clientId = 87;
            var mgr = new EnergyModelManagerFactory().CreateEnergyModelManager(clientId);

            var attributes = new ProfileAttributeCollection()
            {
                new ProfileAttribute() { Key = "premisetype", OptionKey = "premisetype.business", OptionValue = "1" },
                new ProfileAttribute() { Key = "business.buildingcategory", OptionKey = "buildingcategory.collegeuniversity", OptionValue = "0" },
                new ProfileAttribute() { Key = "business.totalarea", OptionKey = "business.totalarea.value", OptionValue = "110000" }
            };

            // Appliance collection
            var appliances = new ApplianceCollection { new Appliance() { Name = "businesscooking" } };
            appliances.IsStandAlone = true;

            var regionParams = new RegionParams() { ApplianceRegionId = 1, TemperatureRegionId = 31, SolarRegionId = 1, State = "AZ" };

            var result = mgr.ExecuteEnergyModel(regionParams, null, attributes, appliances);

            WriteResultsToConsole(result);

            CollectionAssert.AllItemsAreNotNull(result.Appliances);
            CollectionAssert.AllItemsAreNotNull(result.EndUses);
        }

        [TestMethod]
        public void BEM_Business_Model_collegeuniversity_businesscooling()
        {
            const int clientId = 87;
            var mgr = new EnergyModelManagerFactory().CreateEnergyModelManager(clientId);

            var attributes = new ProfileAttributeCollection()
            {
                new ProfileAttribute() { Key = "premisetype", OptionKey = "premisetype.business", OptionValue = "1" },
                new ProfileAttribute() { Key = "business.buildingcategory", OptionKey = "buildingcategory.collegeuniversity", OptionValue = "0" },
                new ProfileAttribute() { Key = "business.totalarea", OptionKey = "business.totalarea.value", OptionValue = "110000" }
            };

            // Appliance collection
            var appliances = new ApplianceCollection { new Appliance() { Name = "businesscooling" } };
            appliances.IsStandAlone = true;

            var regionParams = new RegionParams() { ApplianceRegionId = 1, TemperatureRegionId = 31, SolarRegionId = 1, State = "AZ" };

            var result = mgr.ExecuteEnergyModel(regionParams, null, attributes, appliances);

            WriteResultsToConsole(result);

            CollectionAssert.AllItemsAreNotNull(result.Appliances);
            CollectionAssert.AllItemsAreNotNull(result.EndUses);
        }

        [TestMethod]
        public void BEM_Business_Model_collegeuniversity_businessheating()
        {
            const int clientId = 87;
            var mgr = new EnergyModelManagerFactory().CreateEnergyModelManager(clientId);

            var attributes = new ProfileAttributeCollection()
            {
                new ProfileAttribute() { Key = "premisetype", OptionKey = "premisetype.business", OptionValue = "1" },
                new ProfileAttribute() { Key = "business.buildingcategory", OptionKey = "buildingcategory.collegeuniversity", OptionValue = "0" },
                new ProfileAttribute() { Key = "business.totalarea", OptionKey = "business.totalarea.value", OptionValue = "110000" }
            };

            // Appliance collection
            var appliances = new ApplianceCollection { new Appliance() { Name = "businessheating" } };
            appliances.IsStandAlone = true;

            var regionParams = new RegionParams() { ApplianceRegionId = 1, TemperatureRegionId = 31, SolarRegionId = 1, State = "AZ" };

            var result = mgr.ExecuteEnergyModel(regionParams, null, attributes, appliances);

            WriteResultsToConsole(result);

            CollectionAssert.AllItemsAreNotNull(result.Appliances);
            CollectionAssert.AllItemsAreNotNull(result.EndUses);
        }

        [TestMethod]
        public void BEM_Business_Model_collegeuniversity_businesslighting()
        {
            const int clientId = 87;
            var mgr = new EnergyModelManagerFactory().CreateEnergyModelManager(clientId);

            var attributes = new ProfileAttributeCollection()
            {
                new ProfileAttribute() { Key = "premisetype", OptionKey = "premisetype.business", OptionValue = "1" },
                new ProfileAttribute() { Key = "business.buildingcategory", OptionKey = "buildingcategory.collegeuniversity", OptionValue = "0" },
                new ProfileAttribute() { Key = "business.totalarea", OptionKey = "business.totalarea.value", OptionValue = "110000" }
            };

            // Appliance collection
            var appliances = new ApplianceCollection { new Appliance() { Name = "businesslighting" } };
            appliances.IsStandAlone = true;

            var regionParams = new RegionParams() { ApplianceRegionId = 1, TemperatureRegionId = 31, SolarRegionId = 1, State = "AZ" };

            var result = mgr.ExecuteEnergyModel(regionParams, null, attributes, appliances);

            WriteResultsToConsole(result);

            CollectionAssert.AllItemsAreNotNull(result.Appliances);
            CollectionAssert.AllItemsAreNotNull(result.EndUses);
        }


        [TestMethod]
        public void BEM_Business_Model_collegeuniversity_businessmisc()
        {
            const int clientId = 87;
            var mgr = new EnergyModelManagerFactory().CreateEnergyModelManager(clientId);

            var attributes = new ProfileAttributeCollection()
            {
                new ProfileAttribute() { Key = "premisetype", OptionKey = "premisetype.business", OptionValue = "1" },
                new ProfileAttribute() { Key = "business.buildingcategory", OptionKey = "buildingcategory.collegeuniversity", OptionValue = "0" },
                new ProfileAttribute() { Key = "business.totalarea", OptionKey = "business.totalarea.value", OptionValue = "110000" }
            };

            // Appliance collection
            var appliances = new ApplianceCollection { new Appliance() { Name = "businessmisc" } };
            appliances.IsStandAlone = true;

            var regionParams = new RegionParams() { ApplianceRegionId = 1, TemperatureRegionId = 31, SolarRegionId = 1, State = "AZ" };

            var result = mgr.ExecuteEnergyModel(regionParams, null, attributes, appliances);

            WriteResultsToConsole(result);

            CollectionAssert.AllItemsAreNotNull(result.Appliances);
            CollectionAssert.AllItemsAreNotNull(result.EndUses);
        }

        [TestMethod]
        public void BEM_Business_Model_collegeuniversity_businessofficeequipment()
        {
            const int clientId = 87;
            var mgr = new EnergyModelManagerFactory().CreateEnergyModelManager(clientId);

            var attributes = new ProfileAttributeCollection()
            {
                new ProfileAttribute() { Key = "premisetype", OptionKey = "premisetype.business", OptionValue = "1" },
                new ProfileAttribute() { Key = "business.buildingcategory", OptionKey = "buildingcategory.collegeuniversity", OptionValue = "0" },
                new ProfileAttribute() { Key = "business.totalarea", OptionKey = "business.totalarea.value", OptionValue = "110000" }
            };

            // Appliance collection
            var appliances = new ApplianceCollection { new Appliance() { Name = "businessofficeequipment" } };
            appliances.IsStandAlone = true;

            var regionParams = new RegionParams() { ApplianceRegionId = 1, TemperatureRegionId = 31, SolarRegionId = 1, State = "AZ" };

            var result = mgr.ExecuteEnergyModel(regionParams, null, attributes, appliances);

            WriteResultsToConsole(result);

            CollectionAssert.AllItemsAreNotNull(result.Appliances);
            CollectionAssert.AllItemsAreNotNull(result.EndUses);
        }

        [TestMethod]
        public void BEM_Business_Model_collegeuniversity_businessrefrigeration()
        {
            const int clientId = 87;
            var mgr = new EnergyModelManagerFactory().CreateEnergyModelManager(clientId);

            var attributes = new ProfileAttributeCollection()
            {
                new ProfileAttribute() { Key = "premisetype", OptionKey = "premisetype.business", OptionValue = "1" },
                new ProfileAttribute() { Key = "business.buildingcategory", OptionKey = "buildingcategory.collegeuniversity", OptionValue = "0" },
                new ProfileAttribute() { Key = "business.totalarea", OptionKey = "business.totalarea.value", OptionValue = "110000" }
            };

            // Appliance collection
            var appliances = new ApplianceCollection { new Appliance() { Name = "businessrefrigeration" } };
            appliances.IsStandAlone = true;

            var regionParams = new RegionParams() { ApplianceRegionId = 1, TemperatureRegionId = 31, SolarRegionId = 1, State = "AZ" };

            var result = mgr.ExecuteEnergyModel(regionParams, null, attributes, appliances);

            WriteResultsToConsole(result);

            CollectionAssert.AllItemsAreNotNull(result.Appliances);
            CollectionAssert.AllItemsAreNotNull(result.EndUses);
        }

        [TestMethod]
        public void BEM_Business_Model_collegeuniversity_businessventilation()
        {
            const int clientId = 87;
            var mgr = new EnergyModelManagerFactory().CreateEnergyModelManager(clientId);

            var attributes = new ProfileAttributeCollection()
            {
                new ProfileAttribute() { Key = "premisetype", OptionKey = "premisetype.business", OptionValue = "1" },
                new ProfileAttribute() { Key = "business.buildingcategory", OptionKey = "buildingcategory.collegeuniversity", OptionValue = "0" },
                new ProfileAttribute() { Key = "business.totalarea", OptionKey = "business.totalarea.value", OptionValue = "110000" }
            };

            // Appliance collection
            var appliances = new ApplianceCollection { new Appliance() { Name = "businessventilation" } };
            appliances.IsStandAlone = true;

            var regionParams = new RegionParams() { ApplianceRegionId = 1, TemperatureRegionId = 31, SolarRegionId = 1, State = "AZ" };

            var result = mgr.ExecuteEnergyModel(regionParams, null, attributes, appliances);

            WriteResultsToConsole(result);

            CollectionAssert.AllItemsAreNotNull(result.Appliances);
            CollectionAssert.AllItemsAreNotNull(result.EndUses);
        }

        [TestMethod]
        public void BEM_Business_Model_collegeuniversity_businesswaterheating()
        {
            const int clientId = 87;
            var mgr = new EnergyModelManagerFactory().CreateEnergyModelManager(clientId);

            var attributes = new ProfileAttributeCollection()
            {
                new ProfileAttribute() { Key = "premisetype", OptionKey = "premisetype.business", OptionValue = "1" },
                new ProfileAttribute() { Key = "business.buildingcategory", OptionKey = "buildingcategory.collegeuniversity", OptionValue = "0" },
                new ProfileAttribute() { Key = "business.totalarea", OptionKey = "business.totalarea.value", OptionValue = "110000" }
            };

            // Appliance collection
            var appliances = new ApplianceCollection { new Appliance() { Name = "businesswaterheating" } };
            appliances.IsStandAlone = true;

            var regionParams = new RegionParams() { ApplianceRegionId = 1, TemperatureRegionId = 31, SolarRegionId = 1, State = "AZ" };

            var result = mgr.ExecuteEnergyModel(regionParams, null, attributes, appliances);

            WriteResultsToConsole(result);

            CollectionAssert.AllItemsAreNotNull(result.Appliances);
            CollectionAssert.AllItemsAreNotNull(result.EndUses);
        }

        [TestMethod]
        public void BEM_Business_Model_collegeuniversity_businesslaundry()
        {
            const int clientId = 87;
            var mgr = new EnergyModelManagerFactory().CreateEnergyModelManager(clientId);

            var attributes = new ProfileAttributeCollection()
            {
                new ProfileAttribute() { Key = "premisetype", OptionKey = "premisetype.business", OptionValue = "1" },
                new ProfileAttribute() { Key = "business.buildingcategory", OptionKey = "buildingcategory.collegeuniversity", OptionValue = "0" },
                new ProfileAttribute() { Key = "business.totalarea", OptionKey = "business.totalarea.value", OptionValue = "110000" }
            };

            // Appliance collection
            var appliances = new ApplianceCollection { new Appliance() { Name = "businesslaundry" } };
            appliances.IsStandAlone = true;

            var regionParams = new RegionParams() { ApplianceRegionId = 1, TemperatureRegionId = 31, SolarRegionId = 1, State = "AZ" };

            var result = mgr.ExecuteEnergyModel(regionParams, null, attributes, appliances);

            WriteResultsToConsole(result);

            CollectionAssert.AllItemsAreNotNull(result.Appliances);
            CollectionAssert.AllItemsAreNotNull(result.EndUses);
        }

        [TestMethod]
        [ExpectedException(typeof(System.Collections.Generic.KeyNotFoundException))]
        public void BusinessModel_ExceptionTest()
        {
            var EMConfig = new CE.EnergyModel.EnergyModelConfiguration();
            var result = EMConfig.GetCalculationFactor<decimal>("");
           Debug.WriteLine(result);
        }

        /// <summary>
        /// Disconnnected unit tests with all mock data - FUTURE. 
        /// Need to mock the content model completely.  This depends on some appliance and enduse content currently.
        /// Need to mock the EUI values completely.
        /// </summary>
        //[TestMethod, TestCategory("BVT")]
        //[TestMethod]
        //public void BEM_Business_Model_bvt_collegeuniversity_1()
        //{
        //    const int clientId = 87;
        //    var factory = new EnergyModelManagerFactory();
        //    var config = new EnergyModelConfiguration(Utility.CreateCalculationFactors());
        //    var weather = new Weather(Utility.CreateDegreeDayCollection(), Utility.CreateEvaporationCollection(), Utility.CreateTemperatureRegionCollection(), Utility.CreateSolarCollection(), Utility.CreateTemperatureProfileCollection());

        //    // Create Manager with config and weather data
        //    var mgr = factory.CreateEnergyModelManager(clientId, config, weather, true);

        //    var attributes = new ProfileAttributeCollection()
        //    {
        //        new ProfileAttribute() { Key = "premisetype", OptionKey = "premisetype.business", OptionValue = "1" },
        //        new ProfileAttribute() { Key = "business.buildingcategory", OptionKey = "buildingcategory.collegeuniversity", OptionValue = "0" },
        //        new ProfileAttribute() { Key = "business.totalarea", OptionKey = "business.totalarea.value", OptionValue = "110000" }
        //    };
        //    attributes.IsStandAlone = true;

        //    // Appliance collection
        //    var appliances = new ApplianceCollection { new Appliance() { Name = "businesscomputers" } };
        //    appliances.IsStandAlone = true;

        //    // End Use collection
        //    var enduses = new EndUseCollection { new EndUse() { Name = "businesscomputers", ApplianceNames = new System.Collections.Generic.List<string> { "businesscomputers" } } };
        //    enduses.IsStandAlone = true;

        //    var regionParams = new RegionParams() { ApplianceRegionId = 1, TemperatureRegionId = 31, SolarRegionId = 1, State = "AZ" };

        //    var result = mgr.ExecuteEnergyModel(regionParams, null, attributes, appliances, enduses);

        //    WriteResultsToConsole(result);

        //    CollectionAssert.AllItemsAreNotNull(result.Appliances);
        //    CollectionAssert.AllItemsAreNotNull(result.EndUses);
        //}

        [TestMethod]
        public void BEM_Business_Model_multipleappliances_climateid3()
        {
            const int clientId = 87;
            var mgr = new EnergyModelManagerFactory().CreateEnergyModelManager(clientId);

            var attributes = new ProfileAttributeCollection()
            {
                new ProfileAttribute() { Key = "premisetype", OptionKey = "premisetype.business", OptionValue = "0" },
                new ProfileAttribute() { Key = "business.buildingcategory", OptionKey = "buildingcategory.school", OptionValue = "0" },
                new ProfileAttribute() { Key = "business.totalarea", OptionKey = "business.totalarea.value", OptionValue = "13953.2944822347" },
                new ProfileAttribute() { Key = "business.cooking.fuel", OptionKey="business.cooking.fuel.electric",OptionValue="0" },
                new ProfileAttribute() { Key = "business.heating.fuel", OptionKey="business.heating.fuel.electric",OptionValue="0" },
                new ProfileAttribute() { Key = "business.waterheating.fuel", OptionKey="business.waterheating.fuel.electric",OptionValue="0" }
            };

            // Appliance collection
            var appliances = new ApplianceCollection
            {
                new Appliance() { Name = "businessheating" },
                new Appliance() {Name = "businesscooling"},
                new Appliance() {Name ="businesswaterheating" },
                new Appliance() {Name = "businessventilation"},
                new Appliance() {Name = "businesslighting"},
                new Appliance() {Name = "businesscooking"},
                new Appliance() {Name = "businessrefrigeration"},
                new Appliance() {Name = "businessofficeequipment"},
                new Appliance() {Name = "businesscomputers"},
                new Appliance() {Name = "businessmisc"},
            };
            appliances.IsStandAlone = true;

            var regionParams = new RegionParams() { ApplianceRegionId = 6, TemperatureRegionId = 62, SolarRegionId = 3, State = "FL",ClimateId = 3};

            var result = mgr.ExecuteEnergyModel(regionParams, null, attributes, appliances);

            WriteResultsToConsole(result);
            Decimal total = new decimal();
            foreach (var item in result.Appliances)
            {
                total=total + item.EnergyUses[0].Quantity;
            }
            Console.WriteLine("Total == "+total);
            CollectionAssert.AllItemsAreNotNull(result.Appliances);
            CollectionAssert.AllItemsAreNotNull(result.EndUses);
        }

        #region Test Helpers

        /// <summary>
        /// Helper to see output quickly in test console.
        /// </summary>
        /// <param name="result"></param>
        private static void WriteResultsToConsole(EnergyModelResult result)
        {
            foreach (var item in result.Appliances)
            {
                Console.WriteLine("key = " + item.Name
                                + ",Commodity = " + item.EnergyUses[0].Commodity
                                + ", Quantity = " + item.EnergyUses[0].Quantity
                                + ", Uom = " + item.EnergyUses[0].UnitOfMeasure);
            }
        }

        #endregion

    }
}
