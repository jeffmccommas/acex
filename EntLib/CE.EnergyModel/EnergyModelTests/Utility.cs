﻿using CE.EnergyModel.Entities;
using System.Collections.Generic;

namespace EnergyModelTests
{
	public static class Utility
	{

		/// <summary>
		/// Calculation factors for use in unit tests for creating input configuration data.
		/// </summary>
		/// <returns></returns>
		public static Dictionary<string, string> CreateCalculationFactors()
		{
			var factors = new Dictionary<string, string>();

			factors.Add("house.bath_gal", "25");
			factors.Add("house.faucetminperday", "8.4");
			factors.Add("house.flushesperday", "5");
			factors.Add("house.kfactorfloorcal", "1");
			factors.Add("house.kfactorhousecal", "0.9");
			factors.Add("house.leakingtoiletvolume", "40");
			factors.Add("house.shower_water_hot", "0.56");
			factors.Add("house.windowpercent_apt", "0.12");
			factors.Add("house.windowpercent_duplex", "0.15");
			factors.Add("house.windowpercent_mobile", "0.15");
			factors.Add("house.windowpercent_multi", "0.13");
			factors.Add("house.windowpercent_single", "0.16");
			factors.Add("house.windowpercent_town", "0.1");
			factors.Add("atticfan.fankwh", "10");
			factors.Add("ceilingfan.fankwh", "0.25");
			factors.Add("centralac.airvolumechange_lt10", "0.35");
			factors.Add("centralac.airvolumechange_gt20", "0.74");
			factors.Add("centralac.airvolumechange_10to20", "0.5");
			factors.Add("centralac.averagegain_base", "800");
			factors.Add("centralac.averagegain_person", "230");
			factors.Add("centralac.averagegain_sqft", "0.333");
			factors.Add("centralac.coolroofs", "0");
			factors.Add("centralac.coolroofscoolsavingspct", "0.15");
			factors.Add("centralac.sealleaksfactor_sealed", "0.82");
			factors.Add("centralac.sealleaksfactor_unsealed", "0.75");
			factors.Add("centralac.stylefactor_air", "1");
			factors.Add("centralac.stylefactor_evap", "0.1");
			factors.Add("centralac.stylefactor_ground", "1");
			factors.Add("centralac.stylefactor_mix", "0.75");
			factors.Add("centralac.stylefactor_std", "1");
			factors.Add("clotheswasher.efficiencyfactor_hori_high", "0.71");
			factors.Add("clotheswasher.efficiencyfactor_hori_std", "1");
			factors.Add("clotheswasher.efficiencyfactor_vert_high", "0.65");
			factors.Add("clotheswasher.efficiencyfactor_vert_std", "1");
			factors.Add("clotheswasher.front_loading_estar_factor", "1.02");
			factors.Add("clotheswasher.getmotorkwh", "0.27");
			factors.Add("clotheswasher.top_loading_estar_factor", "2.2");
			factors.Add("clotheswasher.watervolume_hori_lt1", "15");
			factors.Add("clotheswasher.watervolume_hori_gt15", "26");
			factors.Add("clotheswasher.watervolume_hori_10to15", "25");
			factors.Add("clotheswasher.watervolume_hori_1to5", "15");
			factors.Add("clotheswasher.watervolume_hori_5to9", "20");
			factors.Add("clotheswasher.watervolume_vert_lt1", "30");
			factors.Add("clotheswasher.watervolume_vert_gt15", "44");
			factors.Add("clotheswasher.watervolume_vert_10to15", "42");
			factors.Add("clotheswasher.watervolume_vert_1to5", "30");
			factors.Add("clotheswasher.watervolume_vert_5to9", "36");
			factors.Add("computer.computerkwh", "0.15");
			factors.Add("cooktop.energyuse_elec", "1.25");
			factors.Add("cooktop.energyuse_gas", "3.3");
			factors.Add("cooktop.energyuse_prop", "3.3");
			factors.Add("cooktop.pilotuse", "1.7");         //changed from 'pilotfactor' to 'pilotuse'
			factors.Add("cooktop.people_factor_0", "0");//added
			factors.Add("cooktop.people_factor_1", "0.7");//added
			factors.Add("cooktop.people_factor_2", "0.9");//added
			factors.Add("cooktop.people_factor_3", "1.0");//added
			factors.Add("cooktop.people_factor_4", "1.05");//added
			factors.Add("cooktop.people_factor_5", "1.1");//added
			factors.Add("cooktop.people_factor_6", "1.2");//added
			factors.Add("cooktop.microwaveusefactor_often", "0.9");//added
			factors.Add("cooktop.microwaveusefactor_occ", "0.97"); //added
			factors.Add("dehumidifier.energyuse", "500");
			factors.Add("dishwasher.boosterkw", "0.8");
			factors.Add("dishwasher.fandrykw", "0.3");
			factors.Add("dishwasher.gallonadjustmentfactor", "1");
			factors.Add("dishwasher.pumpkw", "0.3");
			factors.Add("dishwasher.washcyclelightloads", "10");//added
			factors.Add("dishwasher.washcycleregularloads", "10");//added
			factors.Add("dishwasher.washcycleheavyloads", "10");//added
			factors.Add("dishwasher.washcycleotherloads", "2");//added
			factors.Add("dishwasher.agemotorfactor_lt1", "0.99");//added
			factors.Add("dishwasher.agemotorfactor_1to5", "1.0");//added
			factors.Add("dishwasher.agemotorfactor_5to10", "1.02");//added
			factors.Add("dishwasher.agemotorfactor_10to15", "1.04");//added
			factors.Add("dishwasher.agemotorfactor_gt15", "1.06");//added
			factors.Add("dishwasher.dirtlevelfactor", "0.95");//added
			factors.Add("dryer.energyfactor_elec_lt1", "3.16");
			factors.Add("dryer.energyfactor_elec_gt15", "3");
			factors.Add("dryer.energyfactor_elec_10to15", "3.05");
			factors.Add("dryer.energyfactor_elec_1to5", "3.1");
			factors.Add("dryer.energyfactor_elec_5to9", "3.07");
			factors.Add("dryer.energyfactor_gas_lt1", "3.02");
			factors.Add("dryer.energyfactor_gas_gt15", "2.4");
			factors.Add("dryer.energyfactor_gas_10to15", "2.6");
			factors.Add("dryer.energyfactor_gas_1to5", "2.75");
			factors.Add("dryer.energyfactor_gas_5to9", "2.67");
			factors.Add("dryer.motorenergy", "0.23");
			factors.Add("dryer.realsize_comp", "3");
			factors.Add("dryer.realsize_std", "7");
			factors.Add("dryer.realsize_xl", "8");
			factors.Add("fireplace.firebtu", "10000");
			factors.Add("fireplace.fireeff", "0.2");
			factors.Add("fireplace.btudivisor", "200"); //added
			factors.Add("freezer.energyuse_chest_sizelt13", "298");
			factors.Add("freezer.energyuse_chest_sizegt24", "571");
			factors.Add("freezer.energyuse_chest_size13to15", "400");
			factors.Add("freezer.energyuse_chest_size16to21", "486");
			factors.Add("freezer.energyuse_chest_size22to24", "537");
			factors.Add("freezer.energyuse_upright_sizelt13", "376");
			factors.Add("freezer.energyuse_upright_sizegt24", "585");
			factors.Add("freezer.energyuse_upright_size13to15", "454");
			factors.Add("freezer.energyuse_upright_size16to21", "520");
			factors.Add("freezer.energyuse_upright_size22to24", "559");
			factors.Add("freezer.frostfreefactor_chest", "1");
			factors.Add("freezer.frostfreefactor_upright", "1.3");
			factors.Add("freezer.ageuse_factor_lt2", "0.8"); //added
			factors.Add("freezer.ageuse_factor_2to5", "0.89");//added
			factors.Add("freezer.ageuse_factor_6to10", "0.97");//added
			factors.Add("freezer.ageuse_factor_11to15", "1.0");//added
			factors.Add("freezer.ageuse_factor_16to20", "1.11");//added
			factors.Add("freezer.ageuse_factor_gt20", "1.3");//added
			factors.Add("freezer.agemaint_factor_0", "1.00");//added
			factors.Add("freezer.agemaint_factor_1to5", "1.01");//added
			factors.Add("freezer.agemaint_factor_6to10", "1.03");//added
			factors.Add("freezer.agemaint_factor_11to15", "1.05");//added
			factors.Add("freezer.agemaint_factor_gt15", "1.06");//added
			factors.Add("freezer.loc_kitchen_ddr_gt35", "1.0");//added
			factors.Add("freezer.loc_kitchen_ddr_25to35", "1.0");//added
			factors.Add("freezer.loc_kitchen_ddr_lt25", "1.0");//added
			factors.Add("freezer.loc_pantry_ddr_gt35", "1.0");//added
			factors.Add("freezer.loc_pantry_ddr_25to35", "1.0");//added
			factors.Add("freezer.loc_pantry_ddr_lt25", "1.0");//added
			factors.Add("freezer.loc_basement_ddr_gt35", "0.9");//added
			factors.Add("freezer.loc_basement_ddr_25to35", "0.9");//added
			factors.Add("freezer.loc_basement_ddr_lt25", "0.9");//added
			factors.Add("freezer.loc_garage_ddr_gt35", "0.9");//added
			factors.Add("freezer.loc_garage_ddr_25to35", "1.0");//added
			factors.Add("freezer.loc_garage_ddr_lt25", "1.1");//added
			factors.Add("freezer.loc_other_ddr_gt35", "1.0");//added
			factors.Add("freezer.loc_other_ddr_25to35", "1.0");//added
			factors.Add("freezer.loc_other_ddr_lt25", "1.0");//added
			factors.Add("freezer.defrostfactor", "80");//added
			factors.Add("woodstove.heatingweeks", "26");
			factors.Add("woodstove.stovebtu", "5000");
			factors.Add("hottub.energyuse_lg", "2.5e+007");
			factors.Add("hottub.energyuse_md", "2e+007");
			factors.Add("hottub.energyuse_sm", "1.8e+007");
			factors.Add("hottub.pumpcons_lg", "500");
			factors.Add("hottub.pumpcons_md", "400");
			factors.Add("hottub.pumpcons_sm", "300");
			factors.Add("hottub.volume_lg", "600");
			factors.Add("hottub.volume_md", "400");
			factors.Add("hottub.volume_sm", "200");
			factors.Add("hottub.waterevap_lg", "2000");
			factors.Add("hottub.waterevap_md", "1500");
			factors.Add("hottub.waterevap_sm", "1000");
			factors.Add("hottub.coverfactor1", "0.88");//added
			factors.Add("hottub.coverfactor2", "0.96");//added
			factors.Add("hottub.covereffect1", "0.1");//added
			factors.Add("hottub.covereffect2", "0.5");//added
			factors.Add("hottub.insideFactor", "0.8");//added
			factors.Add("hottub.eff_gas", "0.8");//added
			factors.Add("hottub.eff_prop", "0.8");//added
			factors.Add("hottub.eff_elec", "1.0");//added
			factors.Add("hottub.eff_oil", "0.7");//added
			factors.Add("housefan.coolingweeks", "26");         //was housefan, changed to wholehousefan, changed back!
			factors.Add("housefan.fankwh", "10");               //was housefan, changed to wholehousefan , changed back!
			factors.Add("humidifier.energyuse", "500");
			factors.Add("lighting.lighthours_base", "2");
			factors.Add("lighting.lighthours_person", "0.1");
			factors.Add("lighting.spotreplaceledwattage", "15");
			factors.Add("lighting.wallreplaceledwattage", "10");
			factors.Add("lighting.usage_factor_gt6", "1.1"); //added
			factors.Add("lighting.usage_factor_4to6", "1.0");//added
			factors.Add("lighting.usage_factor_1to3", "0.80");//added
			factors.Add("lighting.usage_factor_0", "0.0");//added
			factors.Add("microwave.useformeals", "100");
			factors.Add("microwave.useforsnacks", "100");
			factors.Add("microwave.people_factor_0", "0.0");//added
			factors.Add("microwave.people_factor_1", "0.9");//added
			factors.Add("microwave.people_factor_2", "0.97");//added
			factors.Add("microwave.people_factor_3", "1.0");//added
			factors.Add("microwave.people_factor_4", "1.03");//added
			factors.Add("microwave.people_factor_5", "1.05");//added
			factors.Add("microwave.people_factor_6", "1.6");//added
			factors.Add("airpurifier.energyuse", "200");    // formerly miscellaneous.xxx
			factors.Add("aquarium.energyuse", "300");    // formerly miscellaneous.xxx
			factors.Add("autoblockheater.energyuse", "400");    // formerly miscellaneous.xxx
			factors.Add("bottledwaterdispenser.energyuse", "300");    // formerly miscellaneous.xxx
			factors.Add("coffeemaker.energyuse", "200");    // formerly miscellaneous.xxx
			factors.Add("copier.energyuse", "240");    // formerly miscellaneous.xxx
			factors.Add("electricblanket.energyuse", "150");    // formerly miscellaneous.xxx
			factors.Add("faxmachine.energyuse", "240");    // formerly miscellaneous.xxx
			factors.Add("garbagedisposal.energyuse", "400");    // formerly miscellaneous.xxx
			factors.Add("growlight.energyuse", "400");    // formerly miscellaneous.xxx
			factors.Add("heatedtowelbar.energyuse", "300");    // formerly miscellaneous.xxx
			factors.Add("hotwaterdispenser.energyuse", "250");    // formerly miscellaneous.xxx )(was instanthotwater)
			factors.Add("kiln.energyuse", "1000");    // formerly miscellaneous.xxx
			factors.Add("o2concentrator.energyuse", "2600");    // formerly miscellaneous.xxx
			factors.Add("pipeandgutterheater.energyuse", "300");    // formerly miscellaneous.xxx
			factors.Add("sauna.energyuse", "1500");    // formerly miscellaneous.xxx
			factors.Add("steamroom.energyuse", "1500");    // formerly miscellaneous.xxx
			factors.Add("trashcompactor.energyuse", "400");    // formerly miscellaneous.xxx
			factors.Add("winechiller.energyuse", "750");    // formerly miscellaneous.xxx
			factors.Add("oven.energyuse_elec", "1.22");
			factors.Add("oven.energyuse_gas", "2");
			factors.Add("oven.energyuse_prop", "2");
			factors.Add("oven.pilotuse", "1.7");//added
			factors.Add("oven.convectionfactor", "0.9");//added
			factors.Add("oven.people_factor_0", "0");//added
			factors.Add("oven.people_factor_1", "0.7");//added
			factors.Add("oven.people_factor_2", "0.9");//added
			factors.Add("oven.people_factor_3", "1.0");//added
			factors.Add("oven.people_factor_4", "1.05");//added
			factors.Add("oven.people_factor_5", "1.1");//added
			factors.Add("oven.people_factor_6", "1.2");//added
			factors.Add("oven.microwaveusefactor_often", "0.9");//added
			factors.Add("oven.microwaveusefactor_occ", "0.97"); //added
			factors.Add("pool.poolbtu", "2e+008");
			factors.Add("pool.pumpweeks_heated", "24");
			factors.Add("pool.pumpweeks_unheated", "17");
			factors.Add("refrigerator.energyusebase_1door_lt13", "402");
			factors.Add("refrigerator.energyusebase_1door_gt24", "568");
			factors.Add("refrigerator.energyusebase_1door_13to15", "454");
			factors.Add("refrigerator.energyusebase_1door_16to21", "506");
			factors.Add("refrigerator.energyusebase_1door_22to24", "547");
			factors.Add("refrigerator.energyusebase_botfrz_lt13", "508");
			factors.Add("refrigerator.energyusebase_botfrz_gt24", "596");
			factors.Add("refrigerator.energyusebase_botfrz_13to15", "536");
			factors.Add("refrigerator.energyusebase_botfrz_16to21", "563");
			factors.Add("refrigerator.energyusebase_botfrz_22to24", "585");
			factors.Add("refrigerator.energyusebase_side_lt13", "563");
			factors.Add("refrigerator.energyusebase_side_gt24", "661");
			factors.Add("refrigerator.energyusebase_side_13to15", "594");
			factors.Add("refrigerator.energyusebase_side_16to21", "625");
			factors.Add("refrigerator.energyusebase_side_22to24", "649");
			factors.Add("refrigerator.energyusebase_topfrz_lt13", "381");
			factors.Add("refrigerator.energyusebase_topfrz_gt24", "567");
			factors.Add("refrigerator.energyusebase_topfrz_13to15", "439");
			factors.Add("refrigerator.energyusebase_topfrz_16to21", "497");
			factors.Add("refrigerator.energyusebase_topfrz_22to24", "544");
			factors.Add("refrigerator.manualdefrostfactor", "0.9");
			factors.Add("refrigerator.thrudooricefactor", "1.15");
			factors.Add("refrigerator.builduploss", "80");  //added
			factors.Add("refrigerator.agemaint_factor_0", "1.00");//added
			factors.Add("refrigerator.agemaint_factor_1to5", "1.01");//added
			factors.Add("refrigerator.agemaint_factor_5to10", "1.03");//added
			factors.Add("refrigerator.agemaint_factor_10to15", "1.05");//added
			factors.Add("refrigerator.agemaint_factor_gt15", "1.06");//added
			factors.Add("refrigerator.loc_kitchen_ddr_gt35", "1.0");//added
			factors.Add("refrigerator.loc_kitchen_ddr_25to35", "1.0");//added
			factors.Add("refrigerator.loc_kitchen_ddr_lt25", "1.0");//added
			factors.Add("refrigerator.loc_pantry_ddr_gt35", "1.0");//added
			factors.Add("refrigerator.loc_pantry_ddr_25to35", "1.0");//added
			factors.Add("refrigerator.loc_pantry_ddr_lt25", "1.0");//added
			factors.Add("refrigerator.loc_basement_ddr_gt35", "0.9");//added
			factors.Add("refrigerator.loc_basement_ddr_25to35", "0.9");//added
			factors.Add("refrigerator.loc_basement_ddr_lt25", "0.9");//added
			factors.Add("refrigerator.loc_garage_ddr_gt35", "0.9");//added
			factors.Add("refrigerator.loc_garage_ddr_25to35", "1.0");//added
			factors.Add("refrigerator.loc_garage_ddr_lt25", "1.1");//added
			factors.Add("refrigerator.loc_other_ddr_gt35", "0.9");//added
			factors.Add("refrigerator.loc_other_ddr_25to35", "0.9");//added
			factors.Add("refrigerator.loc_other_ddr_lt25", "0.9");//added
			factors.Add("refrigerator.ageuse_factor_lto2", "0.8"); //added
			factors.Add("refrigerator.ageuse_factor_2to5", "0.89");//added
			factors.Add("refrigerator.ageuse_factor_6to10", "0.97");//added
			factors.Add("refrigerator.ageuse_factor_11to15", "1.0");//added
			factors.Add("refrigerator.ageuse_factor_16to20", "1.11");//added
			factors.Add("refrigerator.ageuse_factor_gt20", "1.3");//added
			factors.Add("roomac.coolingweeks", "17");
			factors.Add("roomac.eer_excellent", "9.5");
			factors.Add("roomac.eer_good", "8.5");
			factors.Add("roomac.eer_average", "7.5");
			factors.Add("roomac.eer_poor", "6.5");
			factors.Add("roomac.agemaint_factor1", "0.98");//added
			factors.Add("roomac.agemaint_factor2", "0.94");//added
			factors.Add("roomac.agemaint_factor3", "0.88");//added
			factors.Add("roomac.agemaint_factor4", "0.8");//added
			factors.Add("roomac.sizeadjustgt12k", "13000");//added
			factors.Add("roomac.sizeadjust8to12k", "10000");//added
			factors.Add("roomac.sizeadjust5to8k", "6500");//added
			factors.Add("roomac.sizeadjustlt5k", "4500");//added
			factors.Add("roomac.timerungt12k", "0.22");//added
			factors.Add("roomac.timerunt8to12k", "0.27");//added
			factors.Add("roomac.timerunt5to8k", "0.37");//added
			factors.Add("roomac.timerunlt5k", "0.50");//added
			factors.Add("roomac.wiredthermostatfactor", "0.92"); //added
			factors.Add("spaceheater.heaterbtu", "5000");
			factors.Add("spaceheater.heatingweeks", "26");
			factors.Add("spaceheater.usagefactormultiplier", "20");
			factors.Add("sumppump.energy", "180");
			factors.Add("sumppump.agepumpeff_factor_gt15", "0.75"); //added
			factors.Add("sumppump.agepumpeff_factor_10to15", "0.8");//added
			factors.Add("sumppump.agepumpeff_factor_5to10", "0.84");//added
			factors.Add("sumppump.agepumpeff_factor_lt5", "0.87");//added
			factors.Add("tv.tvhours", "4");
			factors.Add("tv.tvstandbykwhflat", "30");
			factors.Add("tv.tvstandbykwhstd", "30");
			factors.Add("tv.tvstandbyproj", "30");
			factors.Add("tv.tvwattsflatlg", "275");
			factors.Add("tv.tvwattsflatsm", "105");
			factors.Add("tv.tvwattsproj", "210");
			factors.Add("tv.tvwattsstdbw", "100");
			factors.Add("tv.tvwattsstdlg", "195");
			factors.Add("tv.tvwattsstdsm", "150");
			factors.Add("waterbed.energyuse", "830");
			factors.Add("waterbed.softfactortween", "7.5");//added
			factors.Add("waterbed.softfactordouble", "10.3");//added
			factors.Add("waterbed.softfactorqueen", "12.6");//added
			factors.Add("waterbed.softfactorking", "13.7");//added
			factors.Add("waterbed.hardfactortween", "15");//added
			factors.Add("waterbed.hardfactordouble", "20.7");//added
			factors.Add("waterbed.hardfactorqueen", "25.3");//added
			factors.Add("waterbed.hardfactorking", "27.3");//added
			factors.Add("waterbed.insulfactor0", "0.85");//added
			factors.Add("waterbed.insulfactor1", "0.87");//added
			factors.Add("waterbed.insulfactor2", "0.92");//added
			factors.Add("waterbed.insulfactor3", "1.0");//added
			factors.Add("waterbed.insulfactor4", "0.7");//added
			factors.Add("waterbed.insulfactor5", "0.78");//added
			factors.Add("waterbed.insulfactor6", "0.84");//added
			factors.Add("waterbed.insulfactor7", "1.0");//added
			factors.Add("waterbed.stylefactorsoft", "0.5");//added
			factors.Add("waterbed.stylefactorhard", "1.0");//added
			factors.Add("waterheater.cookwaterhot", "0.5");
			factors.Add("waterheater.tstatshowerstartup", "0");
			factors.Add("waterheater.water_heater_install_oil", "500");
			factors.Add("waterheater.water_heater_install_std", "275");
			factors.Add("waterheater.timerdaysoff", "152");  //added
            factors.Add("waterheater.waterdensity", "8.29");  //added
            factors.Add("waterheater.waterspecificheat", "1.00074");  //added
            factors.Add("waterheater.recoveryefficiency.electricresistance", "0.98");  //added
            factors.Add("waterheater.recoveryefficiency.heatpump", "2.23");  //added
            factors.Add("waterheater.ratedinputpower.heatpump", "0.5");  //added
            factors.Add("waterheater.ratedinputpower.electricresistance", "4.5");  //added
            factors.Add("waterheater.performanceadjfac1", "0.000197");  //added
            factors.Add("waterheater.performanceadjfac2", "0.01842");  //added
            factors.Add("waterheater.performanceadjfac3", "1.322263");  //added
            factors.Add("waterheater.waterenergycontentout", "41093.7");  //added
            factors.Add("wellpump.energyuse", "125");
			factors.Add("wellpump.agepumpeff_factor_gt20", "0.8"); //added
			factors.Add("wellpump.agepumpeff_factor_15to20", "0.7");//added
			factors.Add("wellpump.agepumpeff_factor_10to15", "0.75");//added
			factors.Add("wellpump.agepumpeff_factor_5to10", "0.8");//added
			factors.Add("wellpump.agepumpeff_factor_lt5", "0.82");//added
			factors.Add("heatsystem.airvolumechange_lt10", "0.2");
			factors.Add("heatsystem.airvolumechange_gt20", "0.42");
			factors.Add("heatsystem.airvolumechange_10to20", "0.28");
			factors.Add("heatsystem.auxiliaryusefactor_kwh", "0.01");
			factors.Add("heatsystem.averagegain_base", "800");
			factors.Add("heatsystem.averagegain_person", "230");
			factors.Add("heatsystem.averagegain_sqft", "0.333");
			factors.Add("heatsystem.combustionsavingspct", "0.01");
			factors.Add("heatsystem.coolroofsheatsavingspct", "-0.07");
			factors.Add("heatsystem.deliveryfactor_bboard_sealed", "0.98");
			factors.Add("heatsystem.deliveryfactor_bboard_unsealed", "0.98");
			factors.Add("heatsystem.deliveryfactor_fireplace_sealed", "1");
			factors.Add("heatsystem.deliveryfactor_fireplace_unsealed", "1");
			factors.Add("heatsystem.deliveryfactor_freestandingstove_sealed", "1");
			factors.Add("heatsystem.deliveryfactor_freestandingstove_unsealed", "1");
			factors.Add("heatsystem.deliveryfactor_furn_sealed", "0.82");
			factors.Add("heatsystem.deliveryfactor_furn_unsealed", "0.75");
			factors.Add("heatsystem.deliveryfactor_gndhp_sealed", "0.82");
			factors.Add("heatsystem.deliveryfactor_gndhp_unsealed", "0.75");
			factors.Add("heatsystem.deliveryfactor_hp_sealed", "0.82");
			factors.Add("heatsystem.deliveryfactor_hp_unsealed", "0.75");
			factors.Add("heatsystem.deliveryfactor_radiantpanel_sealed", "0.98");
			factors.Add("heatsystem.deliveryfactor_radiantpanel_unsealed", "0.98");
			factors.Add("heatsystem.deliveryfactor_stblr_sealed", "0.84");
			factors.Add("heatsystem.deliveryfactor_stblr_unsealed", "0.84");
			factors.Add("heatsystem.deliveryfactor_wblr_sealed", "0.9");
			factors.Add("heatsystem.deliveryfactor_wblr_unsealed", "0.9");
			factors.Add("heatsystem.deliveryfactor_wradiant_sealed", "0.925");
			factors.Add("heatsystem.deliveryfactor_wradiant_unsealed", "0.925");
			factors.Add("heatsystem.ignfactor", "3.6e+006");
			factors.Add("heatsystem.oilbtufactor", "138690");
			factors.Add("heatsystem.propanebtufactor", "92000");
			factors.Add("heatsystem.supplementalheatfactor_hp", "0.15");
			factors.Add("outsidewater.lowwaterfactor", "0.9");
			factors.Add("outsidewater.sprinklertype.drip_watering_system", "0.0006");
			factors.Add("outsidewater.sprinklertype.fixed_spray", "0.01558");
			factors.Add("outsidewater.sprinklertype.gear_rotor", "0.0052");
			factors.Add("outsidewater.sprinklertype.hose", "0.00258");
			factors.Add("outsidewater.sprinklertype.house_end_sprinkler", "0.00258");
			factors.Add("outsidewater.sprinklertype.impact", "0.00831");
			factors.Add("outsidewater.sprinklertype.soaker_hose", "0.00129");
			factors.Add("outsidewater.sprinklertype.steam_rotor", "0.00649");
			factors.Add("vehicle.kwhconversion", "33.7");    //renamed vehiclekwhconversion to kwhconversion
			factors.Add("vehicle.thermconversion", "1.2");   //renamed
			factors.Add("miscellaneous.energyuse", "200");   //added new as a constant for generic appliances
			factors.Add("miscellaneous.roomgroupsize", "4"); //added, made algorithm linear with formula
			factors.Add("miscellaneous.roommultiplier", "20"); //added
			factors.Add("miscellaneous.peoplemultiplier", "36"); //added
			factors.Add("miscellaneous.peopleroombase", "310"); //added
			factors.Add("sink.volumepermeal", "310"); //added

			return (factors);
		}

		// Used this query to quickly create the above logic used to initialize the dictionary with all existing "generic properties" for referrer zero.
		// No cost specific properties included here
		//use esov3static
		//go
		//
		//SELECT TOP 1000 [ReferrerID]
		//      ,[ApplianceEnum]
		//      ,ApplianceList.[Description] As ApplianceName
		//      ,LOWER(REPLACE(REPLACE([ApplianceList].[Description],'>','gt'),'<','lt')) + '.' + LOWER(REPLACE(REPLACE([PropertyName],'>','gt'),'<','lt')) As KeyName
		//      ,'factors.Add("' + LOWER(REPLACE(REPLACE([ApplianceList].[Description],'>','gt'),'<','lt')) + '.' + LOWER(REPLACE(REPLACE([PropertyName],'>','gt'),'<','lt')) + '", "' + CAST([PropertyValue] as varchar(30)) + '");'
		//      ,[PropertyName]
		//      ,[PropertyValue]
		//      ,[GenericProperties].[Description] as PropertyDescription
		//  FROM [esov3static].[dbo].[GenericProperties] INNER JOIN dbo.ApplianceList ON [GenericProperties].ApplianceEnum = ApplianceList.TypeID
		//       where PropertyName NOT LIKE ('%Cost%') and referrerid = 0
		//      order by ReferreriD, ApplianceEnum



		/// <summary>
		/// Create a degree day collection object.
		/// </summary>
		/// <returns></returns>
		public static DegreeDaysCollection CreateDegreeDayCollection()
		{
			// generate a degreeDaysCollection
			var ddc = new DegreeDaysCollection();


			ddc.Add(new DegreeDays(1, 8897, 1, new int[] { 0, 1265, 1025, 1001, 759, 558, 360, 279, 310, 468, 707, 990, 1175 }, new int[] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 }));
			ddc.Add(new DegreeDays(2, 9953, 1, new int[] { 0, 1380, 1145, 1110, 837, 614, 396, 313, 357, 534, 834, 1128, 1305 }, new int[] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 }));
			ddc.Add(new DegreeDays(3, 9733, 1, new int[] { 0, 1128, 1053, 1088, 951, 787, 579, 450, 419, 519, 787, 918, 1054 }, new int[] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 }));
			ddc.Add(new DegreeDays(4, 13807, 1, new int[] { 0, 2192, 1728, 1538, 1029, 660, 354, 236, 357, 636, 1166, 1797, 2114 }, new int[] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 }));
			ddc.Add(new DegreeDays(5, 11606, 1, new int[] { 0, 1708, 1408, 1345, 963, 620, 315, 205, 307, 567, 1048, 1455, 1665 }, new int[] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 }));
			ddc.Add(new DegreeDays(6, 11456, 1, new int[] { 0, 1553, 1406, 1321, 1014, 701, 450, 319, 344, 534, 1011, 1281, 1522 }, new int[] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 }));
			ddc.Add(new DegreeDays(7, 14129, 1, new int[] { 0, 1798, 1711, 1748, 1422, 911, 573, 423, 459, 675, 1147, 1473, 1789 }, new int[] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 }));
			ddc.Add(new DegreeDays(8, 15689, 32, new int[] { 0, 2409, 2072, 1925, 1329, 676, 238, 192, 373, 726, 1442, 2004, 2303 }, new int[] { 0, 0, 0, 0, 0, 0, 5, 18, 9, 0, 0, 0, 0 }));
			ddc.Add(new DegreeDays(9, 20226, 6, new int[] { 0, 2430, 2318, 2483, 2016, 1417, 930, 797, 840, 1035, 1597, 2001, 2362 }, new int[] { 0, 0, 0, 0, 0, 0, 0, 3, 4, 0, 0, 0, 0 }));
			ddc.Add(new DegreeDays(10, 3323, 1651, new int[] { 0, 812, 613, 417, 158, 41, 0, 0, 0, 13, 173, 411, 685 }, new int[] { 0, 0, 0, 11, 32, 147, 324, 434, 412, 229, 56, 6, 0 }));
			ddc.Add(new DegreeDays(11, 2918, 1797, new int[] { 0, 729, 540, 350, 135, 31, 0, 0, 0, 7, 148, 364, 614 }, new int[] { 0, 0, 0, 15, 45, 168, 339, 459, 434, 259, 71, 7, 0 }));
			ddc.Add(new DegreeDays(12, 2918, 1797, new int[] { 0, 729, 540, 350, 135, 31, 0, 0, 0, 7, 148, 364, 614 }, new int[] { 0, 0, 0, 15, 45, 168, 339, 459, 434, 259, 71, 7, 0 }));
			ddc.Add(new DegreeDays(13, 2918, 1797, new int[] { 0, 729, 540, 350, 135, 31, 0, 0, 0, 7, 148, 364, 614 }, new int[] { 0, 0, 0, 15, 45, 168, 339, 459, 434, 259, 71, 7, 0 }));
			ddc.Add(new DegreeDays(14, 2224, 2212, new int[] { 0, 594, 426, 263, 78, 6, 0, 0, 0, 0, 91, 274, 492 }, new int[] { 0, 8, 0, 21, 69, 220, 411, 505, 484, 345, 116, 22, 11 }));
			ddc.Add(new DegreeDays(15, 2224, 2212, new int[] { 0, 594, 426, 263, 78, 6, 0, 0, 0, 0, 91, 274, 492 }, new int[] { 0, 8, 0, 21, 69, 220, 411, 505, 484, 345, 116, 22, 11 }));
			ddc.Add(new DegreeDays(16, 2224, 2212, new int[] { 0, 594, 426, 263, 78, 6, 0, 0, 0, 0, 91, 274, 492 }, new int[] { 0, 8, 0, 21, 69, 220, 411, 505, 484, 345, 116, 22, 11 }));
			ddc.Add(new DegreeDays(17, 1702, 2627, new int[] { 0, 492, 344, 177, 48, 0, 0, 0, 0, 0, 52, 196, 393 }, new int[] { 0, 24, 14, 38, 132, 295, 462, 536, 521, 387, 157, 40, 21 }));
			ddc.Add(new DegreeDays(18, 3478, 1894, new int[] { 0, 871, 647, 426, 151, 36, 0, 0, 0, 13, 136, 432, 766 }, new int[] { 0, 0, 0, 10, 43, 164, 357, 512, 484, 271, 53, 0, 0 }));
			ddc.Add(new DegreeDays(19, 3478, 1894, new int[] { 0, 871, 647, 426, 151, 36, 0, 0, 0, 13, 136, 432, 766 }, new int[] { 0, 0, 0, 10, 43, 164, 357, 512, 484, 271, 53, 0, 0 }));
			ddc.Add(new DegreeDays(20, 3478, 1894, new int[] { 0, 871, 647, 426, 151, 36, 0, 0, 0, 13, 136, 432, 766 }, new int[] { 0, 0, 0, 10, 43, 164, 357, 512, 484, 271, 53, 0, 0 }));
			ddc.Add(new DegreeDays(21, 3478, 1894, new int[] { 0, 871, 647, 426, 151, 36, 0, 0, 0, 13, 136, 432, 766 }, new int[] { 0, 0, 0, 10, 43, 164, 357, 512, 484, 271, 53, 0, 0 }));
			ddc.Add(new DegreeDays(22, 3228, 1916, new int[] { 0, 822, 610, 395, 130, 31, 0, 0, 0, 11, 126, 396, 707 }, new int[] { 0, 0, 0, 13, 58, 177, 375, 508, 465, 260, 60, 0, 0 }));
			ddc.Add(new DegreeDays(23, 3228, 1916, new int[] { 0, 822, 610, 395, 130, 31, 0, 0, 0, 11, 126, 396, 707 }, new int[] { 0, 0, 0, 13, 58, 177, 375, 508, 465, 260, 60, 0, 0 }));
			ddc.Add(new DegreeDays(24, 3155, 1906, new int[] { 0, 803, 599, 384, 133, 25, 0, 0, 0, 8, 128, 387, 688 }, new int[] { 0, 0, 0, 15, 46, 187, 402, 425, 484, 281, 66, 0, 0 }));
			ddc.Add(new DegreeDays(25, 3155, 1906, new int[] { 0, 803, 599, 384, 133, 25, 0, 0, 0, 8, 128, 387, 688 }, new int[] { 0, 0, 0, 15, 46, 187, 402, 425, 484, 281, 66, 0, 0 }));
			ddc.Add(new DegreeDays(26, 3155, 1906, new int[] { 0, 803, 599, 384, 133, 25, 0, 0, 0, 8, 128, 387, 688 }, new int[] { 0, 0, 0, 15, 46, 187, 402, 425, 484, 281, 66, 0, 0 }));
			ddc.Add(new DegreeDays(27, 3001, 2272, new int[] { 0, 659, 517, 390, 203, 38, 0, 0, 0, 5, 107, 392, 690 }, new int[] { 0, 0, 0, 5, 25, 216, 444, 620, 560, 325, 75, 2, 0 }));
			ddc.Add(new DegreeDays(28, 5146, 904, new int[] { 0, 955, 777, 658, 416, 177, 40, 6, 14, 84, 358, 680, 980 }, new int[] { 0, 0, 0, 0, 2, 58, 188, 320, 232, 97, 7, 0, 0 }));
			ddc.Add(new DegreeDays(29, 4052, 1101, new int[] { 0, 809, 657, 546, 349, 99, 5, 0, 1, 22, 230, 533, 801 }, new int[] { 0, 0, 0, 0, 1, 66, 229, 376, 287, 133, 9, 0, 0 }));
			ddc.Add(new DegreeDays(30, 2832, 2267, new int[] { 0, 619, 452, 373, 200, 66, 8, 0, 1, 12, 113, 377, 613 }, new int[] { 0, 0, 1, 18, 69, 200, 426, 569, 516, 347, 116, 7, 0 }));
			ddc.Add(new DegreeDays(31, 1033, 4417, new int[] { 0, 302, 187, 79, 14, 0, 0, 0, 0, 0, 5, 101, 346 }, new int[] { 0, 2, 4, 101, 208, 531, 754, 950, 900, 680, 325, 60, 0 }));
			ddc.Add(new DegreeDays(32, 886, 4884, new int[] { 0, 260, 165, 76, 12, 0, 0, 0, 0, 0, 5, 79, 289 }, new int[] { 0, 3, 8, 110, 254, 583, 826, 958, 897, 753, 392, 99, 0 }));
			ddc.Add(new DegreeDays(33, 1420, 3386, new int[] { 0, 375, 273, 166, 44, 1, 0, 0, 0, 0, 18, 153, 390 }, new int[] { 0, 1, 2, 42, 136, 403, 642, 709, 637, 537, 230, 48, 0 }));
			ddc.Add(new DegreeDays(34, 3016, 65, new int[] { 0, 505, 358, 363, 287, 218, 121, 92, 68, 79, 135, 306, 484 }, new int[] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 35, 30, 0, 0 }));
			ddc.Add(new DegreeDays(35, 2855, 1575, new int[] { 0, 605, 400, 397, 252, 63, 0, 0, 0, 0, 122, 396, 620 }, new int[] { 0, 0, 0, 0, 35, 98, 273, 479, 382, 245, 63, 0, 0 }));
			ddc.Add(new DegreeDays(36, 4310, 1039, new int[] { 0, 843, 633, 564, 361, 140, 16, 0, 0, 51, 265, 594, 843 }, new int[] { 0, 0, 0, 0, 13, 53, 193, 363, 298, 105, 14, 0, 0 }));
			ddc.Add(new DegreeDays(37, 3005, 145, new int[] { 0, 431, 297, 313, 275, 260, 198, 187, 156, 116, 123, 237, 412 }, new int[] { 0, 0, 0, 0, 5, 0, 16, 21, 28, 64, 11, 0, 0 }));
			ddc.Add(new DegreeDays(38, 4496, 1967, new int[] { 0, 527, 440, 484, 444, 378, 285, 248, 223, 234, 319, 399, 515 }, new int[] { 0, 0, 0, 7, 68, 158, 352, 524, 474, 293, 91, 0, 0 }));
			ddc.Add(new DegreeDays(39, 2438, 1537, new int[] { 0, 403, 311, 304, 237, 178, 102, 42, 36, 71, 108, 252, 394 }, new int[] { 0, 14, 32, 36, 77, 97, 176, 293, 316, 271, 162, 45, 18 }));
			ddc.Add(new DegreeDays(40, 2182, 2365, new int[] { 0, 533, 331, 246, 144, 28, 6, 0, 0, 8, 60, 283, 543 }, new int[] { 0, 0, 0, 11, 84, 214, 402, 592, 546, 362, 147, 7, 0 }));
			ddc.Add(new DegreeDays(41, 2182, 2365, new int[] { 0, 533, 331, 246, 144, 28, 6, 0, 0, 8, 60, 283, 543 }, new int[] { 0, 0, 0, 11, 84, 214, 402, 592, 546, 362, 147, 7, 0 }));
			ddc.Add(new DegreeDays(46, 6415, 973, new int[] { 0, 1122, 924, 859, 558, 302, 87, 6, 18, 164, 468, 816, 1091 }, new int[] { 0, 0, 0, 0, 0, 31, 190, 375, 291, 86, 0, 0, 0 }));
			ddc.Add(new DegreeDays(47, 5548, 1183, new int[] { 0, 1240, 854, 670, 389, 132, 13, 0, 0, 55, 332, 738, 1125 }, new int[] { 0, 0, 0, 0, 0, 39, 235, 428, 353, 115, 13, 0, 0 }));
			ddc.Add(new DegreeDays(48, 6020, 679, new int[] { 0, 1094, 885, 806, 504, 253, 71, 0, 0, 144, 429, 780, 1054 }, new int[] { 0, 0, 0, 0, 0, 11, 128, 267, 203, 63, 7, 0, 0 }));
			ddc.Add(new DegreeDays(49, 6020, 679, new int[] { 0, 1094, 885, 806, 504, 253, 71, 0, 0, 144, 429, 780, 1054 }, new int[] { 0, 0, 0, 0, 0, 11, 128, 267, 203, 63, 7, 0, 0 }));
			ddc.Add(new DegreeDays(50, 8749, 62, new int[] { 0, 1559, 1193, 1014, 717, 453, 174, 42, 98, 306, 667, 1053, 1473 }, new int[] { 0, 0, 0, 0, 0, 0, 6, 39, 17, 0, 0, 0, 0 }));
			ddc.Add(new DegreeDays(51, 6151, 677, new int[] { 0, 1252, 1050, 853, 489, 194, 20, 0, 6, 96, 397, 693, 1101 }, new int[] { 0, 0, 0, 0, 0, 27, 125, 270, 210, 45, 0, 0, 0 }));
			ddc.Add(new DegreeDays(52, 6151, 677, new int[] { 0, 1252, 1050, 853, 489, 194, 20, 0, 6, 96, 397, 693, 1101 }, new int[] { 0, 0, 0, 0, 0, 27, 125, 270, 210, 45, 0, 0, 0 }));
			ddc.Add(new DegreeDays(53, 5537, 724, new int[] { 0, 1119, 969, 818, 504, 219, 18, 0, 0, 54, 302, 582, 952 }, new int[] { 0, 0, 0, 0, 0, 15, 96, 270, 251, 81, 11, 0, 0 }));
			ddc.Add(new DegreeDays(54, 5006, 1549, new int[] { 0, 1066, 879, 676, 369, 140, 7, 0, 0, 44, 319, 588, 918 }, new int[] { 0, 0, 0, 0, 9, 104, 318, 465, 419, 203, 31, 0, 0 }));
			ddc.Add(new DegreeDays(55, 4937, 1046, new int[] { 0, 1066, 885, 691, 384, 122, 0, 0, 0, 35, 288, 561, 905 }, new int[] { 0, 0, 0, 0, 0, 44, 199, 353, 310, 125, 15, 0, 0 }));
			ddc.Add(new DegreeDays(56, 4937, 1046, new int[] { 0, 1066, 885, 691, 384, 122, 0, 0, 0, 35, 288, 561, 905 }, new int[] { 0, 0, 0, 0, 0, 44, 199, 353, 310, 125, 15, 0, 0 }));
			ddc.Add(new DegreeDays(57, 1617, 2582, new int[] { 0, 471, 331, 184, 38, 0, 0, 0, 0, 0, 39, 183, 371 }, new int[] { 0, 23, 14, 33, 108, 279, 441, 508, 505, 408, 187, 57, 19 }));
			ddc.Add(new DegreeDays(58, 1434, 2570, new int[] { 0, 421, 296, 169, 37, 0, 0, 0, 0, 0, 31, 149, 331 }, new int[] { 0, 34, 24, 61, 117, 279, 417, 487, 477, 390, 184, 69, 31 }));
			ddc.Add(new DegreeDays(59, 909, 2919, new int[] { 0, 282, 205, 112, 21, 0, 0, 0, 0, 0, 0, 82, 207 }, new int[] { 0, 50, 37, 90, 150, 301, 432, 502, 496, 432, 265, 109, 55 }));
			ddc.Add(new DegreeDays(60, 725, 3381, new int[] { 0, 234, 160, 81, 7, 0, 0, 0, 0, 0, 0, 72, 171 }, new int[] { 0, 70, 58, 117, 191, 369, 483, 536, 543, 480, 316, 144, 74 }));
			ddc.Add(new DegreeDays(61, 418, 3855, new int[] { 0, 153, 108, 32, 0, 0, 0, 0, 0, 0, 0, 25, 100 }, new int[] { 0, 116, 103, 160, 247, 412, 501, 552, 558, 510, 378, 199, 119 }));
			ddc.Add(new DegreeDays(62, 323, 4198, new int[] { 0, 122, 85, 27, 0, 0, 0, 0, 0, 0, 0, 15, 74 }, new int[] { 0, 156, 149, 221, 306, 425, 492, 546, 552, 507, 412, 264, 168 }));
			ddc.Add(new DegreeDays(63, 100, 4798, new int[] { 0, 44, 30, 7, 0, 0, 0, 0, 0, 0, 0, 0, 19 }, new int[] { 0, 196, 184, 279, 360, 484, 543, 601, 598, 549, 465, 318, 221 }));
			ddc.Add(new DegreeDays(64, 2991, 1667, new int[] { 0, 744, 566, 365, 138, 27, 0, 0, 0, 10, 138, 367, 636 }, new int[] { 0, 0, 0, 8, 33, 157, 330, 428, 406, 241, 54, 10, 0 }));
			ddc.Add(new DegreeDays(65, 2991, 1667, new int[] { 0, 744, 566, 365, 138, 27, 0, 0, 0, 10, 138, 367, 636 }, new int[] { 0, 0, 0, 8, 33, 157, 330, 428, 406, 241, 54, 10, 0 }));
			ddc.Add(new DegreeDays(66, 2893, 1709, new int[] { 0, 719, 546, 359, 131, 27, 0, 0, 0, 0, 140, 354, 617 }, new int[] { 0, 0, 0, 6, 29, 161, 342, 453, 419, 240, 53, 6, 0 }));
			ddc.Add(new DegreeDays(67, 2261, 2284, new int[] { 0, 607, 440, 260, 76, 7, 0, 0, 0, 0, 86, 268, 517 }, new int[] { 0, 9, 0, 18, 70, 234, 435, 524, 508, 342, 117, 19, 8 }));
			ddc.Add(new DegreeDays(68, 2334, 2125, new int[] { 0, 615, 454, 267, 82, 7, 0, 0, 0, 5, 106, 287, 511 }, new int[] { 0, 7, 0, 15, 61, 221, 405, 502, 477, 314, 100, 17, 6 }));
			ddc.Add(new DegreeDays(69, 2565, 1948, new int[] { 0, 654, 493, 305, 106, 12, 0, 0, 0, 0, 129, 314, 552 }, new int[] { 0, 0, 0, 11, 37, 188, 375, 490, 456, 285, 92, 14, 0 }));
			ddc.Add(new DegreeDays(70, 2261, 2284, new int[] { 0, 607, 440, 260, 76, 7, 0, 0, 0, 0, 86, 268, 517 }, new int[] { 0, 9, 0, 18, 70, 234, 435, 524, 508, 342, 117, 19, 8 }));
			ddc.Add(new DegreeDays(71, 1847, 2365, new int[] { 0, 516, 378, 204, 47, 0, 0, 0, 0, 0, 63, 213, 426 }, new int[] { 0, 16, 9, 24, 77, 266, 423, 521, 496, 348, 135, 36, 14 }));
			ddc.Add(new DegreeDays(72, 1847, 2365, new int[] { 0, 516, 378, 204, 47, 0, 0, 0, 0, 0, 63, 213, 426 }, new int[] { 0, 16, 9, 24, 77, 266, 423, 521, 496, 348, 135, 36, 14 }));
			ddc.Add(new DegreeDays(73, 0, 3883, new int[] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 }, new int[] { 0, 209, 188, 239, 270, 335, 384, 431, 450, 426, 391, 315, 245 }));
			ddc.Add(new DegreeDays(74, 6893, 907, new int[] { 0, 1466, 1159, 905, 444, 171, 19, 0, 12, 103, 417, 858, 1339 }, new int[] { 0, 0, 0, 0, 0, 65, 193, 332, 254, 55, 8, 0, 0 }));
			ddc.Add(new DegreeDays(75, 6497, 1036, new int[] { 0, 1414, 1128, 859, 428, 165, 10, 0, 11, 71, 372, 780, 1259 }, new int[] { 0, 0, 0, 0, 5, 81, 214, 360, 287, 74, 15, 0, 0 }));
			ddc.Add(new DegreeDays(76, 7327, 702, new int[] { 0, 1522, 1226, 967, 522, 237, 34, 6, 27, 124, 456, 861, 1345 }, new int[] { 0, 0, 0, 0, 0, 55, 150, 261, 194, 34, 8, 0, 0 }));
			ddc.Add(new DegreeDays(77, 6893, 907, new int[] { 0, 1466, 1159, 905, 444, 171, 19, 0, 12, 103, 417, 858, 1339 }, new int[] { 0, 0, 0, 0, 0, 65, 193, 332, 254, 55, 8, 0, 0 }));
			ddc.Add(new DegreeDays(78, 6497, 1036, new int[] { 0, 1414, 1128, 859, 428, 165, 10, 0, 11, 71, 372, 780, 1259 }, new int[] { 0, 0, 0, 0, 5, 81, 214, 360, 287, 74, 15, 0, 0 }));
			ddc.Add(new DegreeDays(79, 7406, 702, new int[] { 0, 1562, 1257, 955, 501, 203, 27, 10, 29, 127, 470, 879, 1386 }, new int[] { 0, 0, 0, 0, 0, 55, 150, 261, 194, 34, 8, 0, 0 }));
			ddc.Add(new DegreeDays(80, 6893, 907, new int[] { 0, 1466, 1159, 905, 444, 171, 19, 0, 12, 103, 417, 858, 1339 }, new int[] { 0, 0, 0, 0, 0, 65, 193, 332, 254, 55, 8, 0, 0 }));
			ddc.Add(new DegreeDays(81, 6497, 1036, new int[] { 0, 1414, 1128, 859, 428, 165, 10, 0, 11, 71, 372, 780, 1259 }, new int[] { 0, 0, 0, 0, 5, 81, 214, 360, 287, 74, 15, 0, 0 }));
			ddc.Add(new DegreeDays(82, 7406, 702, new int[] { 0, 1562, 1257, 955, 501, 203, 27, 10, 29, 127, 470, 879, 1386 }, new int[] { 0, 0, 0, 0, 0, 55, 150, 261, 194, 34, 8, 0, 0 }));
			ddc.Add(new DegreeDays(83, 5270, 814, new int[] { 0, 973, 725, 648, 436, 210, 64, 6, 16, 141, 394, 711, 946 }, new int[] { 0, 0, 0, 0, 0, 6, 121, 288, 285, 114, 0, 0, 0 }));
			ddc.Add(new DegreeDays(84, 5270, 814, new int[] { 0, 973, 725, 648, 436, 210, 64, 6, 16, 141, 394, 711, 946 }, new int[] { 0, 0, 0, 0, 0, 6, 121, 288, 285, 114, 0, 0, 0 }));
			ddc.Add(new DegreeDays(85, 5270, 814, new int[] { 0, 973, 725, 648, 436, 210, 64, 6, 16, 141, 394, 711, 946 }, new int[] { 0, 0, 0, 0, 0, 6, 121, 288, 285, 114, 0, 0, 0 }));
			ddc.Add(new DegreeDays(86, 5861, 754, new int[] { 0, 1116, 815, 701, 477, 242, 75, 6, 20, 160, 414, 753, 1082 }, new int[] { 0, 0, 0, 0, 0, 9, 120, 285, 252, 88, 0, 0, 0 }));
			ddc.Add(new DegreeDays(87, 5861, 754, new int[] { 0, 1116, 815, 701, 477, 242, 75, 6, 20, 160, 414, 753, 1082 }, new int[] { 0, 0, 0, 0, 0, 9, 120, 285, 252, 88, 0, 0, 0 }));
			ddc.Add(new DegreeDays(88, 5861, 754, new int[] { 0, 1116, 815, 701, 477, 242, 75, 6, 20, 160, 414, 753, 1082 }, new int[] { 0, 0, 0, 0, 0, 9, 120, 285, 252, 88, 0, 0, 0 }));
			ddc.Add(new DegreeDays(89, 7180, 421, new int[] { 0, 1293, 1005, 890, 603, 353, 125, 9, 29, 218, 527, 882, 1246 }, new int[] { 0, 0, 0, 0, 0, 0, 56, 183, 144, 38, 0, 0, 0 }));
			ddc.Add(new DegreeDays(90, 5861, 754, new int[] { 0, 1116, 815, 701, 477, 242, 75, 6, 20, 160, 414, 753, 1082 }, new int[] { 0, 0, 0, 0, 0, 9, 120, 285, 252, 88, 0, 0, 0 }));
			ddc.Add(new DegreeDays(91, 7180, 421, new int[] { 0, 1293, 1005, 890, 603, 353, 125, 9, 29, 218, 527, 882, 1246 }, new int[] { 0, 0, 0, 0, 0, 0, 56, 183, 144, 38, 0, 0, 0 }));
			ddc.Add(new DegreeDays(92, 7180, 421, new int[] { 0, 1293, 1005, 890, 603, 353, 125, 9, 29, 218, 527, 882, 1246 }, new int[] { 0, 0, 0, 0, 0, 0, 56, 183, 144, 38, 0, 0, 0 }));
			ddc.Add(new DegreeDays(93, 6474, 702, new int[] { 0, 1398, 1126, 856, 438, 188, 10, 0, 11, 73, 384, 762, 1228 }, new int[] { 0, 0, 0, 0, 0, 56, 138, 259, 198, 42, 9, 0, 0 }));
			ddc.Add(new DegreeDays(94, 6536, 752, new int[] { 0, 1364, 1109, 862, 492, 235, 35, 5, 19, 84, 391, 750, 1190 }, new int[] { 0, 0, 0, 0, 0, 46, 143, 259, 226, 66, 12, 0, 0 }));
			ddc.Add(new DegreeDays(95, 6148, 982, new int[] { 0, 1345, 1084, 806, 408, 183, 11, 0, 9, 54, 356, 714, 1178 }, new int[] { 0, 0, 0, 0, 0, 87, 206, 326, 261, 87, 15, 0, 0 }));
			ddc.Add(new DegreeDays(96, 6148, 982, new int[] { 0, 1345, 1084, 806, 408, 183, 11, 0, 9, 54, 356, 714, 1178 }, new int[] { 0, 0, 0, 0, 0, 87, 206, 326, 261, 87, 15, 0, 0 }));
			ddc.Add(new DegreeDays(97, 6148, 982, new int[] { 0, 1345, 1084, 806, 408, 183, 11, 0, 9, 54, 356, 714, 1178 }, new int[] { 0, 0, 0, 0, 0, 87, 206, 326, 261, 87, 15, 0, 0 }));
			ddc.Add(new DegreeDays(98, 5688, 1141, new int[] { 0, 1265, 1016, 750, 361, 166, 7, 0, 8, 44, 314, 663, 1094 }, new int[] { 0, 0, 0, 0, 7, 120, 244, 357, 278, 113, 22, 0, 0 }));
			ddc.Add(new DegreeDays(99, 5688, 1141, new int[] { 0, 1265, 1016, 750, 361, 166, 7, 0, 8, 44, 314, 663, 1094 }, new int[] { 0, 0, 0, 0, 7, 120, 244, 357, 278, 113, 22, 0, 0 }));
			ddc.Add(new DegreeDays(100, 5688, 1141, new int[] { 0, 1265, 1016, 750, 361, 166, 7, 0, 8, 44, 314, 663, 1094 }, new int[] { 0, 0, 0, 0, 7, 120, 244, 357, 278, 113, 22, 0, 0 }));
			ddc.Add(new DegreeDays(101, 5688, 1141, new int[] { 0, 1265, 1016, 750, 361, 166, 7, 0, 8, 44, 314, 663, 1094 }, new int[] { 0, 0, 0, 0, 7, 120, 244, 357, 278, 113, 22, 0, 0 }));
			ddc.Add(new DegreeDays(102, 6331, 728, new int[] { 0, 1293, 1081, 856, 489, 233, 28, 7, 22, 85, 395, 723, 1119 }, new int[] { 0, 0, 0, 0, 0, 59, 151, 251, 205, 52, 10, 0, 0 }));
			ddc.Add(new DegreeDays(103, 6331, 728, new int[] { 0, 1293, 1081, 856, 489, 233, 28, 7, 22, 85, 395, 723, 1119 }, new int[] { 0, 0, 0, 0, 0, 59, 151, 251, 205, 52, 10, 0, 0 }));
			ddc.Add(new DegreeDays(104, 6273, 824, new int[] { 0, 1305, 1095, 849, 474, 217, 15, 0, 10, 81, 388, 711, 1128 }, new int[] { 0, 0, 0, 0, 0, 68, 171, 279, 215, 78, 13, 0, 0 }));
			ddc.Add(new DegreeDays(105, 5615, 1014, new int[] { 0, 1225, 991, 732, 378, 165, 5, 0, 6, 58, 338, 660, 1057 }, new int[] { 0, 0, 0, 0, 0, 96, 212, 322, 260, 106, 18, 0, 0 }));
			ddc.Add(new DegreeDays(106, 5615, 1014, new int[] { 0, 1225, 991, 732, 378, 165, 5, 0, 6, 58, 338, 660, 1057 }, new int[] { 0, 0, 0, 0, 0, 96, 212, 322, 260, 106, 18, 0, 0 }));
			ddc.Add(new DegreeDays(107, 5615, 1014, new int[] { 0, 1225, 991, 732, 378, 165, 5, 0, 6, 58, 338, 660, 1057 }, new int[] { 0, 0, 0, 0, 0, 96, 212, 322, 260, 106, 18, 0, 0 }));
			ddc.Add(new DegreeDays(108, 4708, 1376, new int[] { 0, 1082, 857, 595, 273, 114, 0, 0, 0, 33, 266, 564, 924 }, new int[] { 0, 0, 0, 0, 9, 130, 294, 415, 344, 159, 25, 0, 0 }));
			ddc.Add(new DegreeDays(109, 4708, 1376, new int[] { 0, 1082, 857, 595, 273, 114, 0, 0, 0, 33, 266, 564, 924 }, new int[] { 0, 0, 0, 0, 9, 130, 294, 415, 344, 159, 25, 0, 0 }));
			ddc.Add(new DegreeDays(110, 4708, 1376, new int[] { 0, 1082, 857, 595, 273, 114, 0, 0, 0, 33, 266, 564, 924 }, new int[] { 0, 0, 0, 0, 9, 130, 294, 415, 344, 159, 25, 0, 0 }));
			ddc.Add(new DegreeDays(111, 5974, 859, new int[] { 0, 1135, 918, 803, 465, 211, 33, 0, 11, 108, 400, 789, 1101 }, new int[] { 0, 0, 0, 0, 0, 25, 162, 329, 265, 78, 0, 0, 0 }));
			ddc.Add(new DegreeDays(112, 5574, 1317, new int[] { 0, 1212, 952, 735, 373, 149, 8, 0, 0, 53, 290, 705, 1097 }, new int[] { 0, 0, 0, 0, 7, 81, 263, 450, 377, 125, 14, 0, 0 }));
			ddc.Add(new DegreeDays(113, 5574, 1317, new int[] { 0, 1212, 952, 735, 373, 149, 8, 0, 0, 53, 290, 705, 1097 }, new int[] { 0, 0, 0, 0, 7, 81, 263, 450, 377, 125, 14, 0, 0 }));
			ddc.Add(new DegreeDays(114, 5974, 859, new int[] { 0, 1135, 918, 803, 465, 211, 33, 0, 11, 108, 400, 789, 1101 }, new int[] { 0, 0, 0, 0, 0, 25, 162, 329, 265, 78, 0, 0, 0 }));
			ddc.Add(new DegreeDays(115, 4791, 1628, new int[] { 0, 1101, 846, 608, 278, 102, 5, 0, 0, 29, 221, 609, 992 }, new int[] { 0, 0, 0, 0, 20, 121, 326, 508, 443, 188, 22, 0, 0 }));
			ddc.Add(new DegreeDays(116, 5265, 1304, new int[] { 0, 1187, 918, 663, 312, 121, 5, 0, 0, 56, 283, 660, 1060 }, new int[] { 0, 0, 0, 0, 9, 109, 263, 419, 351, 140, 13, 0, 0 }));
			ddc.Add(new DegreeDays(117, 5001, 1465, new int[] { 0, 1091, 840, 667, 323, 119, 7, 0, 0, 39, 253, 654, 1008 }, new int[] { 0, 0, 0, 0, 14, 94, 289, 474, 414, 159, 21, 0, 0 }));
			ddc.Add(new DegreeDays(118, 4791, 1628, new int[] { 0, 1101, 846, 608, 278, 102, 5, 0, 0, 29, 221, 609, 992 }, new int[] { 0, 0, 0, 0, 20, 121, 326, 508, 443, 188, 22, 0, 0 }));
			ddc.Add(new DegreeDays(119, 4791, 1628, new int[] { 0, 1101, 846, 608, 278, 102, 5, 0, 0, 29, 221, 609, 992 }, new int[] { 0, 0, 0, 0, 20, 121, 326, 508, 443, 188, 22, 0, 0 }));
			ddc.Add(new DegreeDays(120, 4279, 1475, new int[] { 0, 1004, 787, 550, 231, 83, 0, 0, 0, 24, 228, 513, 859 }, new int[] { 0, 0, 0, 8, 18, 136, 306, 428, 366, 180, 33, 0, 0 }));
			ddc.Add(new DegreeDays(121, 4514, 1288, new int[] { 0, 1032, 820, 580, 273, 105, 6, 0, 0, 36, 254, 537, 871 }, new int[] { 0, 0, 0, 0, 12, 115, 252, 378, 335, 171, 25, 0, 0 }));
			ddc.Add(new DegreeDays(122, 5248, 1140, new int[] { 0, 1144, 930, 682, 354, 151, 11, 0, 0, 51, 327, 621, 977 }, new int[] { 0, 0, 0, 0, 6, 104, 221, 335, 301, 143, 30, 0, 0 }));
			ddc.Add(new DegreeDays(123, 4393, 1033, new int[] { 0, 998, 798, 549, 262, 125, 0, 0, 0, 38, 264, 519, 840 }, new int[] { 0, 0, 0, 0, 10, 103, 202, 298, 273, 125, 22, 0, 0 }));
			ddc.Add(new DegreeDays(124, 2264, 2368, new int[] { 0, 623, 448, 262, 69, 0, 0, 0, 0, 0, 63, 264, 535 }, new int[] { 0, 6, 8, 30, 87, 239, 432, 549, 533, 351, 110, 15, 8 }));
			ddc.Add(new DegreeDays(125, 2264, 2368, new int[] { 0, 623, 448, 262, 69, 0, 0, 0, 0, 0, 63, 264, 535 }, new int[] { 0, 6, 8, 30, 87, 239, 432, 549, 533, 351, 110, 15, 8 }));
			ddc.Add(new DegreeDays(126, 2264, 2368, new int[] { 0, 623, 448, 262, 69, 0, 0, 0, 0, 0, 63, 264, 535 }, new int[] { 0, 6, 8, 30, 87, 239, 432, 549, 533, 351, 110, 15, 8 }));
			ddc.Add(new DegreeDays(127, 2264, 2368, new int[] { 0, 623, 448, 262, 69, 0, 0, 0, 0, 0, 63, 264, 535 }, new int[] { 0, 6, 8, 30, 87, 239, 432, 549, 533, 351, 110, 15, 8 }));
			ddc.Add(new DegreeDays(128, 1616, 2650, new int[] { 0, 467, 328, 178, 40, 0, 0, 0, 0, 0, 35, 193, 375 }, new int[] { 0, 15, 12, 45, 139, 307, 459, 533, 524, 381, 162, 55, 18 }));
			ddc.Add(new DegreeDays(129, 1669, 2690, new int[] { 0, 490, 345, 170, 27, 0, 0, 0, 0, 0, 48, 197, 392 }, new int[] { 0, 19, 12, 55, 144, 319, 465, 536, 524, 393, 159, 47, 17 }));
			ddc.Add(new DegreeDays(130, 1616, 2650, new int[] { 0, 467, 328, 178, 40, 0, 0, 0, 0, 0, 35, 193, 375 }, new int[] { 0, 15, 12, 45, 139, 307, 459, 533, 524, 381, 162, 55, 18 }));
			ddc.Add(new DegreeDays(131, 1616, 2650, new int[] { 0, 467, 328, 178, 40, 0, 0, 0, 0, 0, 35, 193, 375 }, new int[] { 0, 15, 12, 45, 139, 307, 459, 533, 524, 381, 162, 55, 18 }));
			ddc.Add(new DegreeDays(132, 1513, 2655, new int[] { 0, 450, 316, 162, 28, 0, 0, 0, 0, 0, 30, 178, 349 }, new int[] { 0, 25, 17, 56, 133, 304, 450, 524, 512, 393, 157, 61, 23 }));
			ddc.Add(new DegreeDays(133, 6398, 434, new int[] { 0, 1231, 1056, 905, 573, 265, 54, 0, 11, 116, 406, 699, 1082 }, new int[] { 0, 0, 0, 0, 0, 0, 63, 194, 154, 23, 0, 0, 0 }));
			ddc.Add(new DegreeDays(134, 6979, 333, new int[] { 0, 1308, 1126, 970, 618, 301, 69, 6, 25, 157, 465, 768, 1166 }, new int[] { 0, 0, 0, 0, 0, 6, 45, 151, 118, 13, 0, 0, 0 }));
			ddc.Add(new DegreeDays(135, 5641, 678, new int[] { 0, 1128, 972, 818, 507, 221, 32, 0, 6, 72, 321, 591, 973 }, new int[] { 0, 0, 0, 0, 0, 10, 113, 264, 220, 66, 5, 0, 0 }));
			ddc.Add(new DegreeDays(136, 4707, 1137, new int[] { 0, 1029, 846, 648, 348, 108, 0, 0, 0, 29, 276, 546, 877 }, new int[] { 0, 0, 0, 0, 0, 59, 227, 372, 329, 134, 16, 0, 0 }));
			ddc.Add(new DegreeDays(137, 4707, 1137, new int[] { 0, 1029, 846, 648, 348, 108, 0, 0, 0, 29, 276, 546, 877 }, new int[] { 0, 0, 0, 0, 0, 59, 227, 372, 329, 134, 16, 0, 0 }));
			ddc.Add(new DegreeDays(138, 4707, 1137, new int[] { 0, 1029, 846, 648, 348, 108, 0, 0, 0, 29, 276, 546, 877 }, new int[] { 0, 0, 0, 0, 0, 59, 227, 372, 329, 134, 16, 0, 0 }));
			ddc.Add(new DegreeDays(139, 4707, 1137, new int[] { 0, 1029, 846, 648, 348, 108, 0, 0, 0, 29, 276, 546, 877 }, new int[] { 0, 0, 0, 0, 0, 59, 227, 372, 329, 134, 16, 0, 0 }));
			ddc.Add(new DegreeDays(140, 4707, 1137, new int[] { 0, 1029, 846, 648, 348, 108, 0, 0, 0, 29, 276, 546, 877 }, new int[] { 0, 0, 0, 0, 0, 59, 227, 372, 329, 134, 16, 0, 0 }));
			ddc.Add(new DegreeDays(141, 4707, 1137, new int[] { 0, 1029, 846, 648, 348, 108, 0, 0, 0, 29, 276, 546, 877 }, new int[] { 0, 0, 0, 0, 0, 59, 227, 372, 329, 134, 16, 0, 0 }));
			ddc.Add(new DegreeDays(142, 4707, 1137, new int[] { 0, 1029, 846, 648, 348, 108, 0, 0, 0, 29, 276, 546, 877 }, new int[] { 0, 0, 0, 0, 0, 59, 227, 372, 329, 134, 16, 0, 0 }));
			ddc.Add(new DegreeDays(143, 4707, 1137, new int[] { 0, 1029, 846, 648, 348, 108, 0, 0, 0, 29, 276, 546, 877 }, new int[] { 0, 0, 0, 0, 0, 59, 227, 372, 329, 134, 16, 0, 0 }));
			ddc.Add(new DegreeDays(144, 9651, 131, new int[] { 0, 1739, 1487, 1252, 813, 437, 143, 61, 115, 343, 676, 1029, 1556 }, new int[] { 0, 0, 0, 0, 0, 0, 8, 76, 47, 0, 0, 0, 0 }));
			ddc.Add(new DegreeDays(145, 9651, 131, new int[] { 0, 1739, 1487, 1252, 813, 437, 143, 61, 115, 343, 676, 1029, 1556 }, new int[] { 0, 0, 0, 0, 0, 0, 8, 76, 47, 0, 0, 0, 0 }));
			ddc.Add(new DegreeDays(146, 7378, 268, new int[] { 0, 1370, 1168, 992, 651, 363, 100, 11, 39, 189, 512, 789, 1194 }, new int[] { 0, 0, 0, 0, 0, 0, 22, 123, 111, 12, 0, 0, 0 }));
			ddc.Add(new DegreeDays(147, 9567, 155, new int[] { 0, 1646, 1428, 1283, 831, 471, 193, 74, 122, 330, 663, 1047, 1479 }, new int[] { 0, 0, 0, 0, 0, 13, 19, 78, 45, 0, 0, 0, 0 }));
			ddc.Add(new DegreeDays(148, 9316, 131, new int[] { 0, 1615, 1428, 1271, 804, 457, 224, 92, 128, 300, 611, 960, 1426 }, new int[] { 0, 0, 0, 0, 0, 8, 14, 55, 54, 0, 0, 0, 0 }));
			ddc.Add(new DegreeDays(149, 8218, 256, new int[] { 0, 1491, 1310, 1141, 684, 358, 120, 35, 70, 234, 567, 894, 1314 }, new int[] { 0, 0, 0, 0, 0, 20, 51, 112, 73, 0, 0, 0, 0 }));
			ddc.Add(new DegreeDays(150, 8284, 231, new int[] { 0, 1469, 1308, 1147, 723, 411, 146, 43, 80, 237, 561, 879, 1280 }, new int[] { 0, 0, 0, 0, 0, 8, 35, 108, 74, 6, 0, 0, 0 }));
			ddc.Add(new DegreeDays(151, 6924, 431, new int[] { 0, 1293, 1137, 983, 600, 309, 78, 10, 25, 133, 456, 768, 1132 }, new int[] { 0, 0, 0, 0, 0, 27, 78, 174, 133, 19, 0, 0, 0 }));
			ddc.Add(new DegreeDays(152, 6924, 431, new int[] { 0, 1293, 1137, 983, 600, 309, 78, 10, 25, 133, 456, 768, 1132 }, new int[] { 0, 0, 0, 0, 0, 27, 78, 174, 133, 19, 0, 0, 0 }));
			ddc.Add(new DegreeDays(153, 6979, 483, new int[] { 0, 1349, 1162, 961, 570, 292, 64, 11, 25, 137, 468, 768, 1172 }, new int[] { 0, 0, 0, 0, 0, 37, 91, 184, 136, 29, 6, 0, 0 }));
			ddc.Add(new DegreeDays(154, 6973, 534, new int[] { 0, 1339, 1156, 958, 570, 273, 50, 0, 18, 139, 485, 810, 1175 }, new int[] { 0, 0, 0, 0, 0, 40, 110, 208, 157, 19, 0, 0, 0 }));
			ddc.Add(new DegreeDays(155, 7101, 490, new int[] { 0, 1367, 1179, 973, 573, 283, 60, 8, 26, 147, 487, 801, 1197 }, new int[] { 0, 0, 0, 0, 0, 35, 102, 188, 141, 24, 0, 0, 0 }));
			ddc.Add(new DegreeDays(156, 6569, 626, new int[] { 0, 1305, 1109, 908, 531, 243, 38, 0, 16, 102, 435, 744, 1138 }, new int[] { 0, 0, 0, 0, 0, 38, 116, 231, 186, 48, 7, 0, 0 }));
			ddc.Add(new DegreeDays(157, 10487, 249, new int[] { 0, 1984, 1604, 1330, 780, 418, 156, 55, 116, 348, 701, 1203, 1792 }, new int[] { 0, 0, 0, 0, 0, 18, 48, 107, 76, 0, 0, 0, 0 }));
			ddc.Add(new DegreeDays(158, 10487, 249, new int[] { 0, 1984, 1604, 1330, 780, 418, 156, 55, 116, 348, 701, 1203, 1792 }, new int[] { 0, 0, 0, 0, 0, 18, 48, 107, 76, 0, 0, 0, 0 }));
			ddc.Add(new DegreeDays(159, 9818, 180, new int[] { 0, 1798, 1476, 1259, 792, 445, 170, 60, 113, 329, 660, 1098, 1618 }, new int[] { 0, 0, 0, 0, 0, 0, 14, 94, 72, 0, 0, 0, 0 }));
			ddc.Add(new DegreeDays(160, 7981, 682, new int[] { 0, 1649, 1319, 1054, 558, 244, 41, 11, 22, 167, 502, 954, 1460 }, new int[] { 0, 0, 0, 0, 0, 43, 137, 278, 192, 32, 0, 0, 0 }));
			ddc.Add(new DegreeDays(161, 8928, 415, new int[] { 0, 1764, 1420, 1159, 645, 307, 87, 23, 54, 240, 592, 1059, 1578 }, new int[] { 0, 0, 0, 0, 0, 25, 81, 181, 119, 9, 0, 0, 0 }));
			ddc.Add(new DegreeDays(162, 7981, 682, new int[] { 0, 1649, 1319, 1054, 558, 244, 41, 11, 22, 167, 502, 954, 1460 }, new int[] { 0, 0, 0, 0, 0, 43, 137, 278, 192, 32, 0, 0, 0 }));
			ddc.Add(new DegreeDays(163, 8250, 472, new int[] { 0, 1659, 1341, 1091, 603, 282, 56, 16, 33, 185, 530, 972, 1482 }, new int[] { 0, 0, 0, 0, 0, 28, 101, 199, 133, 11, 0, 0, 0 }));
			ddc.Add(new DegreeDays(164, 8250, 472, new int[] { 0, 1659, 1341, 1091, 603, 282, 56, 16, 33, 185, 530, 972, 1482 }, new int[] { 0, 0, 0, 0, 0, 28, 101, 199, 133, 11, 0, 0, 0 }));
			ddc.Add(new DegreeDays(165, 8250, 472, new int[] { 0, 1659, 1341, 1091, 603, 282, 56, 16, 33, 185, 530, 972, 1482 }, new int[] { 0, 0, 0, 0, 0, 28, 101, 199, 133, 11, 0, 0, 0 }));
			ddc.Add(new DegreeDays(166, 5393, 1288, new int[] { 0, 1218, 946, 691, 325, 135, 7, 0, 6, 56, 279, 657, 1073 }, new int[] { 0, 0, 0, 0, 10, 107, 253, 419, 350, 131, 18, 0, 0 }));
			ddc.Add(new DegreeDays(167, 4758, 1534, new int[] { 0, 1107, 871, 617, 266, 111, 0, 0, 0, 21, 237, 564, 964 }, new int[] { 0, 0, 0, 0, 17, 145, 312, 459, 391, 177, 33, 0, 0 }));
			ddc.Add(new DegreeDays(168, 5212, 1189, new int[] { 0, 1159, 921, 676, 316, 137, 10, 0, 0, 52, 285, 627, 1029 }, new int[] { 0, 0, 0, 0, 7, 94, 223, 384, 321, 139, 21, 0, 0 }));
			ddc.Add(new DegreeDays(169, 4638, 1320, new int[] { 0, 1051, 820, 589, 280, 110, 5, 0, 0, 43, 249, 570, 921 }, new int[] { 0, 0, 0, 0, 10, 98, 251, 406, 366, 163, 26, 0, 0 }));
			ddc.Add(new DegreeDays(170, 4638, 1320, new int[] { 0, 1051, 820, 589, 280, 110, 5, 0, 0, 43, 249, 570, 921 }, new int[] { 0, 0, 0, 0, 10, 98, 251, 406, 366, 163, 26, 0, 0 }));
			ddc.Add(new DegreeDays(171, 4638, 1320, new int[] { 0, 1051, 820, 589, 280, 110, 5, 0, 0, 43, 249, 570, 921 }, new int[] { 0, 0, 0, 0, 10, 98, 251, 406, 366, 163, 26, 0, 0 }));
			ddc.Add(new DegreeDays(172, 3079, 1908, new int[] { 0, 778, 580, 365, 130, 27, 0, 0, 0, 9, 144, 379, 667 }, new int[] { 0, 0, 0, 15, 52, 188, 366, 484, 453, 279, 64, 7, 0 }));
			ddc.Add(new DegreeDays(173, 3079, 1908, new int[] { 0, 778, 580, 365, 130, 27, 0, 0, 0, 9, 144, 379, 667 }, new int[] { 0, 0, 0, 15, 52, 188, 366, 484, 453, 279, 64, 7, 0 }));
			ddc.Add(new DegreeDays(174, 3079, 1908, new int[] { 0, 778, 580, 365, 130, 27, 0, 0, 0, 9, 144, 379, 667 }, new int[] { 0, 0, 0, 15, 52, 188, 366, 484, 453, 279, 64, 7, 0 }));
			ddc.Add(new DegreeDays(175, 2467, 2215, new int[] { 0, 656, 485, 285, 87, 7, 0, 0, 0, 6, 104, 295, 542 }, new int[] { 0, 8, 6, 28, 75, 224, 414, 512, 493, 333, 94, 19, 9 }));
			ddc.Add(new DegreeDays(176, 2467, 2215, new int[] { 0, 656, 485, 285, 87, 7, 0, 0, 0, 6, 104, 295, 542 }, new int[] { 0, 8, 6, 28, 75, 224, 414, 512, 493, 333, 94, 19, 9 }));
			ddc.Add(new DegreeDays(177, 2467, 2215, new int[] { 0, 656, 485, 285, 87, 7, 0, 0, 0, 6, 104, 295, 542 }, new int[] { 0, 8, 6, 28, 75, 224, 414, 512, 493, 333, 94, 19, 9 }));
			ddc.Add(new DegreeDays(178, 2444, 2138, new int[] { 0, 628, 456, 292, 99, 14, 0, 0, 0, 6, 123, 301, 525 }, new int[] { 0, 8, 5, 31, 72, 210, 393, 496, 484, 318, 95, 16, 10 }));
			ddc.Add(new DegreeDays(179, 2444, 2138, new int[] { 0, 628, 456, 292, 99, 14, 0, 0, 0, 6, 123, 301, 525 }, new int[] { 0, 8, 5, 31, 72, 210, 393, 496, 484, 318, 95, 16, 10 }));
			ddc.Add(new DegreeDays(180, 2444, 2138, new int[] { 0, 628, 456, 292, 99, 14, 0, 0, 0, 6, 123, 301, 525 }, new int[] { 0, 8, 5, 31, 72, 210, 393, 496, 484, 318, 95, 16, 10 }));
			ddc.Add(new DegreeDays(181, 2444, 2138, new int[] { 0, 628, 456, 292, 99, 14, 0, 0, 0, 6, 123, 301, 525 }, new int[] { 0, 8, 5, 31, 72, 210, 393, 496, 484, 318, 95, 16, 10 }));
			ddc.Add(new DegreeDays(182, 8378, 280, new int[] { 0, 1380, 1075, 970, 654, 425, 235, 97, 128, 367, 716, 1020, 1311 }, new int[] { 0, 0, 0, 0, 0, 0, 29, 121, 106, 24, 0, 0, 0 }));
			ddc.Add(new DegreeDays(183, 8031, 386, new int[] { 0, 1407, 1081, 973, 648, 388, 137, 34, 65, 321, 617, 1002, 1358 }, new int[] { 0, 0, 0, 0, 0, 0, 50, 164, 139, 33, 0, 0, 0 }));
			ddc.Add(new DegreeDays(184, 7741, 388, new int[] { 0, 1358, 1053, 983, 642, 372, 156, 37, 91, 299, 543, 933, 1274 }, new int[] { 0, 0, 0, 0, 0, 0, 54, 137, 150, 47, 0, 0, 0 }));
			ddc.Add(new DegreeDays(185, 8031, 386, new int[] { 0, 1407, 1081, 973, 648, 388, 137, 34, 65, 321, 617, 1002, 1358 }, new int[] { 0, 0, 0, 0, 0, 0, 50, 164, 139, 33, 0, 0, 0 }));
			ddc.Add(new DegreeDays(186, 7164, 652, new int[] { 0, 1308, 1008, 915, 582, 316, 119, 12, 42, 242, 498, 897, 1225 }, new int[] { 0, 0, 0, 0, 0, 6, 113, 244, 219, 65, 5, 0, 0 }));
			ddc.Add(new DegreeDays(187, 8745, 558, new int[] { 0, 1686, 1330, 1104, 621, 322, 121, 19, 71, 267, 586, 1074, 1544 }, new int[] { 0, 0, 0, 0, 0, 12, 103, 199, 208, 36, 0, 0, 0 }));
			ddc.Add(new DegreeDays(188, 7164, 652, new int[] { 0, 1308, 1008, 915, 582, 316, 119, 12, 42, 242, 498, 897, 1225 }, new int[] { 0, 0, 0, 0, 0, 6, 113, 244, 219, 65, 5, 0, 0 }));
			ddc.Add(new DegreeDays(372, 9844, 144, new int[] { 0, 1719, 1479, 1287, 886, 526, 209, 71, 112, 352, 700, 1020, 1483 }, new int[] { 0, 0, 0, 0, 0, 2, 28, 63, 46, 5, 0, 0, 0 }));
			ddc.Add(new DegreeDays(373, 9237, 220, new int[] { 0, 1702, 1464, 1223, 783, 404, 145, 47, 82, 296, 650, 977, 1464 }, new int[] { 0, 0, 0, 0, 0, 7, 44, 94, 64, 11, 0, 0, 0 }));
			ddc.Add(new DegreeDays(374, 8650, 186, new int[] { 0, 1503, 1322, 1167, 801, 455, 181, 48, 73, 280, 608, 899, 1313 }, new int[] { 0, 0, 0, 0, 0, 3, 25, 81, 66, 10, 0, 0, 0 }));
			ddc.Add(new DegreeDays(375, 8558, 66, new int[] { 0, 1459, 1287, 1145, 780, 479, 223, 79, 89, 286, 597, 866, 1268 }, new int[] { 0, 0, 0, 0, 0, 0, 7, 28, 28, 3, 0, 0, 0 }));
			ddc.Add(new DegreeDays(376, 8551, 257, new int[] { 0, 1557, 1333, 1141, 739, 390, 139, 35, 62, 277, 612, 913, 1354 }, new int[] { 0, 0, 0, 0, 0, 5, 42, 109, 86, 14, 0, 0, 0 }));
			ddc.Add(new DegreeDays(377, 8946, 260, new int[] { 0, 1599, 1378, 1189, 805, 449, 163, 41, 67, 284, 636, 946, 1389 }, new int[] { 0, 0, 0, 0, 0, 5, 48, 110, 83, 13, 0, 0, 0 }));
			ddc.Add(new DegreeDays(189, 4308, 907, new int[] { 0, 908, 736, 546, 298, 117, 18, 5, 5, 64, 295, 525, 791 }, new int[] { 0, 0, 0, 0, 0, 55, 150, 247, 222, 100, 133, 0, 0 }));
			ddc.Add(new DegreeDays(190, 3865, 1253, new int[] { 0, 877, 700, 502, 235, 78, 6, 0, 0, 16, 226, 465, 760 }, new int[] { 0, 0, 0, 0, 13, 106, 249, 369, 332, 157, 27, 0, 0 }));
			ddc.Add(new DegreeDays(191, 3865, 1253, new int[] { 0, 877, 700, 502, 235, 78, 6, 0, 0, 16, 226, 465, 760 }, new int[] { 0, 0, 0, 0, 13, 106, 249, 369, 332, 157, 27, 0, 0 }));
			ddc.Add(new DegreeDays(192, 3457, 1417, new int[] { 0, 809, 644, 458, 193, 47, 0, 0, 0, 9, 189, 414, 694 }, new int[] { 0, 0, 0, 6, 13, 109, 279, 406, 375, 192, 37, 0, 0 }));
			ddc.Add(new DegreeDays(193, 3341, 1572, new int[] { 0, 797, 630, 437, 183, 42, 0, 0, 0, 6, 161, 391, 694 }, new int[] { 0, 0, 0, 0, 5, 116, 321, 443, 412, 228, 47, 0, 0 }));
			ddc.Add(new DegreeDays(194, 2470, 1926, new int[] { 0, 631, 496, 338, 115, 10, 0, 0, 0, 0, 102, 259, 519 }, new int[] { 0, 8, 0, 10, 34, 168, 345, 468, 446, 309, 111, 19, 8 }));
			ddc.Add(new DegreeDays(195, 2698, 1638, new int[] { 0, 639, 546, 421, 196, 38, 0, 0, 0, 0, 90, 265, 503 }, new int[] { 0, 6, 0, 6, 13, 100, 276, 412, 415, 276, 102, 25, 7 }));
			ddc.Add(new DegreeDays(196, 2698, 1638, new int[] { 0, 639, 546, 421, 196, 38, 0, 0, 0, 0, 90, 265, 503 }, new int[] { 0, 6, 0, 6, 13, 100, 276, 412, 415, 276, 102, 25, 7 }));
			ddc.Add(new DegreeDays(197, 9090, 548, new int[] { 0, 1739, 1369, 1135, 654, 316, 115, 26, 76, 294, 626, 1134, 1606 }, new int[] { 0, 0, 0, 0, 0, 15, 106, 203, 191, 33, 0, 0, 0 }));
			ddc.Add(new DegreeDays(198, 9090, 548, new int[] { 0, 1739, 1369, 1135, 654, 316, 115, 26, 76, 294, 626, 1134, 1606 }, new int[] { 0, 0, 0, 0, 0, 15, 106, 203, 191, 33, 0, 0, 0 }));
			ddc.Add(new DegreeDays(199, 9090, 548, new int[] { 0, 1739, 1369, 1135, 654, 316, 115, 26, 76, 294, 626, 1134, 1606 }, new int[] { 0, 0, 0, 0, 0, 15, 106, 203, 191, 33, 0, 0, 0 }));
			ddc.Add(new DegreeDays(200, 9254, 537, new int[] { 0, 1832, 1484, 1212, 660, 307, 93, 19, 48, 239, 598, 1107, 1655 }, new int[] { 0, 0, 0, 0, 0, 35, 108, 209, 165, 20, 0, 0, 0 }));
			ddc.Add(new DegreeDays(201, 9254, 537, new int[] { 0, 1832, 1484, 1212, 660, 307, 93, 19, 48, 239, 598, 1107, 1655 }, new int[] { 0, 0, 0, 0, 0, 35, 108, 209, 165, 20, 0, 0, 0 }));
			ddc.Add(new DegreeDays(202, 9254, 537, new int[] { 0, 1832, 1484, 1212, 660, 307, 93, 19, 48, 239, 598, 1107, 1655 }, new int[] { 0, 0, 0, 0, 0, 35, 108, 209, 165, 20, 0, 0, 0 }));
			ddc.Add(new DegreeDays(203, 8968, 488, new int[] { 0, 1730, 1380, 1141, 660, 324, 116, 15, 69, 262, 598, 1092, 1581 }, new int[] { 0, 0, 0, 0, 0, 14, 98, 183, 171, 22, 0, 0, 0 }));
			ddc.Add(new DegreeDays(204, 8968, 488, new int[] { 0, 1730, 1380, 1141, 660, 324, 116, 15, 69, 262, 598, 1092, 1581 }, new int[] { 0, 0, 0, 0, 0, 14, 98, 183, 171, 22, 0, 0, 0 }));
			ddc.Add(new DegreeDays(205, 8968, 488, new int[] { 0, 1730, 1380, 1141, 660, 324, 116, 15, 69, 262, 598, 1092, 1581 }, new int[] { 0, 0, 0, 0, 0, 14, 98, 183, 171, 22, 0, 0, 0 }));
			ddc.Add(new DegreeDays(206, 6729, 713, new int[] { 0, 1243, 974, 893, 555, 278, 61, 0, 9, 175, 477, 861, 1203 }, new int[] { 0, 0, 0, 0, 0, 12, 130, 290, 214, 67, 0, 0, 0 }));
			ddc.Add(new DegreeDays(207, 7282, 752, new int[] { 0, 1407, 1123, 970, 555, 248, 49, 0, 16, 167, 496, 927, 1324 }, new int[] { 0, 0, 0, 0, 0, 19, 139, 305, 236, 53, 0, 0, 0 }));
			ddc.Add(new DegreeDays(208, 6873, 877, new int[] { 0, 1426, 1137, 908, 459, 183, 22, 0, 16, 124, 426, 864, 1308 }, new int[] { 0, 0, 0, 0, 0, 56, 181, 322, 251, 67, 0, 0, 0 }));
			ddc.Add(new DegreeDays(209, 6421, 997, new int[] { 0, 1336, 1056, 846, 430, 171, 18, 0, 12, 107, 395, 822, 1228 }, new int[] { 0, 0, 0, 0, 0, 59, 213, 363, 288, 74, 0, 0, 0 }));
			ddc.Add(new DegreeDays(210, 6413, 1072, new int[] { 0, 1380, 1095, 846, 414, 155, 11, 0, 11, 90, 363, 789, 1259 }, new int[] { 0, 0, 0, 0, 5, 84, 227, 369, 292, 83, 12, 0, 0 }));
			ddc.Add(new DegreeDays(211, 6859, 713, new int[] { 0, 1345, 1047, 884, 504, 234, 49, 0, 11, 158, 474, 888, 1265 }, new int[] { 0, 0, 0, 0, 0, 23, 136, 282, 222, 50, 0, 0, 0 }));
			ddc.Add(new DegreeDays(212, 6421, 997, new int[] { 0, 1336, 1056, 846, 430, 171, 18, 0, 12, 107, 395, 822, 1228 }, new int[] { 0, 0, 0, 0, 0, 59, 213, 363, 288, 74, 0, 0, 0 }));
			ddc.Add(new DegreeDays(213, 6278, 1134, new int[] { 0, 1355, 1075, 818, 399, 161, 11, 0, 9, 81, 362, 786, 1221 }, new int[] { 0, 0, 0, 0, 0, 72, 236, 409, 319, 90, 8, 0, 0 }));
			ddc.Add(new DegreeDays(214, 14015, 1, new int[] { 0, 1894, 1680, 1618, 1278, 936, 639, 518, 564, 741, 1076, 1332, 1739 }, new int[] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 }));
			ddc.Add(new DegreeDays(215, 7554, 328, new int[] { 0, 1438, 1210, 1011, 633, 312, 70, 13, 39, 196, 533, 837, 1262 }, new int[] { 0, 0, 0, 0, 0, 8, 46, 153, 111, 10, 0, 0, 0 }));
			ddc.Add(new DegreeDays(216, 4888, 1201, new int[] { 0, 1066, 896, 710, 381, 127, 0, 0, 0, 26, 252, 531, 899 }, new int[] { 0, 0, 0, 0, 0, 59, 232, 397, 353, 140, 20, 0, 0 }));
			ddc.Add(new DegreeDays(217, 4888, 1201, new int[] { 0, 1066, 896, 710, 381, 127, 0, 0, 0, 26, 252, 531, 899 }, new int[] { 0, 0, 0, 0, 0, 59, 232, 397, 353, 140, 20, 0, 0 }));
			ddc.Add(new DegreeDays(218, 5169, 826, new int[] { 0, 1057, 896, 729, 450, 167, 12, 0, 0, 53, 324, 576, 905 }, new int[] { 0, 0, 0, 0, 0, 25, 144, 301, 260, 86, 10, 0, 0 }));
			ddc.Add(new DegreeDays(219, 5064, 772, new int[] { 0, 980, 795, 691, 401, 169, 22, 0, 0, 73, 325, 666, 942 }, new int[] { 0, 0, 0, 0, 0, 24, 160, 288, 227, 64, 9, 0, 0 }));
			ddc.Add(new DegreeDays(220, 5064, 772, new int[] { 0, 980, 795, 691, 401, 169, 22, 0, 0, 73, 325, 666, 942 }, new int[] { 0, 0, 0, 0, 0, 24, 160, 288, 227, 64, 9, 0, 0 }));
			ddc.Add(new DegreeDays(221, 5064, 772, new int[] { 0, 980, 795, 691, 401, 169, 22, 0, 0, 73, 325, 666, 942 }, new int[] { 0, 0, 0, 0, 0, 24, 160, 288, 227, 64, 9, 0, 0 }));
			ddc.Add(new DegreeDays(222, 4425, 1244, new int[] { 0, 955, 700, 561, 301, 89, 0, 0, 0, 18, 259, 621, 921 }, new int[] { 0, 0, 0, 0, 7, 64, 279, 419, 338, 126, 11, 0, 0 }));
			ddc.Add(new DegreeDays(223, 4425, 1244, new int[] { 0, 955, 700, 561, 301, 89, 0, 0, 0, 18, 259, 621, 921 }, new int[] { 0, 0, 0, 0, 7, 64, 279, 419, 338, 126, 11, 0, 0 }));
			ddc.Add(new DegreeDays(224, 4425, 1244, new int[] { 0, 955, 700, 561, 301, 89, 0, 0, 0, 18, 259, 621, 921 }, new int[] { 0, 0, 0, 0, 7, 64, 279, 419, 338, 126, 11, 0, 0 }));
			ddc.Add(new DegreeDays(225, 3267, 1776, new int[] { 0, 791, 574, 409, 162, 19, 0, 0, 0, 8, 122, 432, 750 }, new int[] { 0, 0, 0, 9, 42, 165, 387, 487, 415, 236, 35, 0, 0 }));
			ddc.Add(new DegreeDays(226, 3267, 1776, new int[] { 0, 791, 574, 409, 162, 19, 0, 0, 0, 8, 122, 432, 750 }, new int[] { 0, 0, 0, 9, 42, 165, 387, 487, 415, 236, 35, 0, 0 }));
			ddc.Add(new DegreeDays(227, 5674, 538, new int[] { 0, 995, 756, 688, 492, 274, 76, 13, 20, 178, 440, 741, 1001 }, new int[] { 0, 0, 0, 0, 0, 9, 89, 229, 173, 38, 0, 0, 0 }));
			ddc.Add(new DegreeDays(228, 7077, 457, new int[] { 0, 1237, 938, 849, 621, 369, 138, 18, 42, 235, 536, 876, 1218 }, new int[] { 0, 0, 0, 0, 0, 0, 60, 194, 157, 46, 0, 0, 0 }));
			ddc.Add(new DegreeDays(229, 2407, 3201, new int[] { 0, 605, 389, 292, 143, 14, 0, 0, 0, 0, 62, 304, 598 }, new int[] { 0, 0, 0, 22, 116, 293, 597, 809, 735, 465, 164, 0, 0 }));
			ddc.Add(new DegreeDays(230, 2407, 3201, new int[] { 0, 605, 389, 292, 143, 14, 0, 0, 0, 0, 62, 304, 598 }, new int[] { 0, 0, 0, 22, 116, 293, 597, 809, 735, 465, 164, 0, 0 }));
			ddc.Add(new DegreeDays(231, 7273, 337, new int[] { 0, 1361, 1184, 1004, 618, 305, 73, 16, 35, 180, 502, 801, 1194 }, new int[] { 0, 0, 0, 0, 0, 20, 49, 147, 103, 18, 0, 0, 0 }));
			ddc.Add(new DegreeDays(232, 7273, 337, new int[] { 0, 1361, 1184, 1004, 618, 305, 73, 16, 35, 180, 502, 801, 1194 }, new int[] { 0, 0, 0, 0, 0, 20, 49, 147, 103, 18, 0, 0, 0 }));
			ddc.Add(new DegreeDays(233, 6834, 438, new int[] { 0, 1321, 1148, 964, 579, 268, 61, 10, 28, 139, 443, 735, 1138 }, new int[] { 0, 0, 0, 0, 0, 23, 70, 178, 133, 34, 0, 0, 0 }));
			ddc.Add(new DegreeDays(234, 5027, 921, new int[] { 0, 1048, 899, 744, 435, 178, 9, 0, 0, 39, 261, 534, 880 }, new int[] { 0, 0, 0, 0, 0, 23, 141, 326, 301, 117, 13, 0, 0 }));
			ddc.Add(new DegreeDays(235, 6894, 507, new int[] { 0, 1376, 1162, 952, 558, 247, 34, 0, 12, 141, 459, 759, 1194 }, new int[] { 0, 0, 0, 0, 0, 18, 91, 213, 155, 30, 0, 0, 0 }));
			ddc.Add(new DegreeDays(236, 6834, 438, new int[] { 0, 1321, 1148, 964, 579, 268, 61, 10, 28, 139, 443, 735, 1138 }, new int[] { 0, 0, 0, 0, 0, 23, 70, 178, 133, 34, 0, 0, 0 }));
			ddc.Add(new DegreeDays(237, 6834, 438, new int[] { 0, 1321, 1148, 964, 579, 268, 61, 10, 28, 139, 443, 735, 1138 }, new int[] { 0, 0, 0, 0, 0, 23, 70, 178, 133, 34, 0, 0, 0 }));
			ddc.Add(new DegreeDays(238, 6834, 438, new int[] { 0, 1321, 1148, 964, 579, 268, 61, 10, 28, 139, 443, 735, 1138 }, new int[] { 0, 0, 0, 0, 0, 23, 70, 178, 133, 34, 0, 0, 0 }));
			ddc.Add(new DegreeDays(239, 6734, 477, new int[] { 0, 1283, 1131, 952, 573, 270, 62, 10, 33, 137, 435, 735, 1113 }, new int[] { 0, 0, 0, 0, 0, 19, 86, 194, 141, 37, 0, 0, 0 }));
			ddc.Add(new DegreeDays(240, 6834, 438, new int[] { 0, 1321, 1148, 964, 579, 268, 61, 10, 28, 139, 443, 735, 1138 }, new int[] { 0, 0, 0, 0, 0, 23, 70, 178, 133, 34, 0, 0, 0 }));
			ddc.Add(new DegreeDays(241, 6579, 610, new int[] { 0, 1318, 1114, 893, 522, 238, 33, 0, 16, 109, 436, 750, 1150 }, new int[] { 0, 0, 0, 0, 0, 40, 120, 225, 168, 49, 8, 0, 0 }));
			ddc.Add(new DegreeDays(242, 6160, 625, new int[] { 0, 1246, 1053, 837, 495, 224, 37, 0, 13, 103, 399, 690, 1063 }, new int[] { 0, 0, 0, 0, 0, 38, 121, 217, 177, 64, 8, 0, 0 }));
			ddc.Add(new DegreeDays(243, 6201, 621, new int[] { 0, 1246, 1058, 859, 522, 250, 40, 0, 11, 99, 387, 672, 1057 }, new int[] { 0, 0, 0, 0, 0, 33, 118, 218, 178, 66, 8, 0, 0 }));
			ddc.Add(new DegreeDays(244, 5708, 797, new int[] { 0, 1197, 991, 747, 420, 187, 23, 0, 12, 81, 361, 663, 1026 }, new int[] { 0, 0, 0, 0, 0, 69, 149, 258, 214, 96, 11, 0, 0 }));
			ddc.Add(new DegreeDays(245, 5708, 797, new int[] { 0, 1197, 991, 747, 420, 187, 23, 0, 12, 81, 361, 663, 1026 }, new int[] { 0, 0, 0, 0, 0, 69, 149, 258, 214, 96, 11, 0, 0 }));
			ddc.Add(new DegreeDays(246, 6258, 666, new int[] { 0, 1256, 1064, 849, 498, 229, 43, 0, 20, 110, 396, 702, 1091 }, new int[] { 0, 0, 0, 0, 0, 37, 127, 225, 185, 77, 15, 0, 0 }));
			ddc.Add(new DegreeDays(247, 6544, 497, new int[] { 0, 1283, 1095, 890, 531, 268, 62, 8, 22, 129, 435, 720, 1101 }, new int[] { 0, 0, 0, 0, 0, 35, 98, 172, 140, 45, 7, 0, 0 }));
			ddc.Add(new DegreeDays(248, 5708, 886, new int[] { 0, 1209, 997, 760, 414, 185, 11, 0, 6, 73, 355, 663, 1035 }, new int[] { 0, 0, 0, 0, 0, 83, 173, 285, 230, 97, 18, 0, 0 }));
			ddc.Add(new DegreeDays(249, 5708, 886, new int[] { 0, 1209, 997, 760, 414, 185, 11, 0, 6, 73, 355, 663, 1035 }, new int[] { 0, 0, 0, 0, 0, 83, 173, 285, 230, 97, 18, 0, 0 }));
			ddc.Add(new DegreeDays(250, 5708, 797, new int[] { 0, 1197, 991, 747, 420, 187, 23, 0, 12, 81, 361, 663, 1026 }, new int[] { 0, 0, 0, 0, 0, 69, 149, 258, 214, 96, 11, 0, 0 }));
			ddc.Add(new DegreeDays(251, 3691, 2017, new int[] { 0, 924, 692, 457, 151, 41, 0, 0, 0, 20, 144, 453, 809 }, new int[] { 0, 0, 0, 11, 46, 174, 381, 567, 512, 269, 57, 0, 0 }));
			ddc.Add(new DegreeDays(252, 3691, 2017, new int[] { 0, 924, 692, 457, 151, 41, 0, 0, 0, 20, 144, 453, 809 }, new int[] { 0, 0, 0, 11, 46, 174, 381, 567, 512, 269, 57, 0, 0 }));
			ddc.Add(new DegreeDays(253, 3691, 2017, new int[] { 0, 924, 692, 457, 151, 41, 0, 0, 0, 20, 144, 453, 809 }, new int[] { 0, 0, 0, 11, 46, 174, 381, 567, 512, 269, 57, 0, 0 }));
			ddc.Add(new DegreeDays(254, 3659, 1859, new int[] { 0, 902, 675, 464, 176, 31, 0, 0, 0, 15, 137, 462, 797 }, new int[] { 0, 0, 0, 9, 38, 136, 351, 527, 499, 255, 44, 0, 0 }));
			ddc.Add(new DegreeDays(255, 3659, 1859, new int[] { 0, 902, 675, 464, 176, 31, 0, 0, 0, 15, 137, 462, 797 }, new int[] { 0, 0, 0, 9, 38, 136, 351, 527, 499, 255, 44, 0, 0 }));
			ddc.Add(new DegreeDays(256, 3659, 1859, new int[] { 0, 902, 675, 464, 176, 31, 0, 0, 0, 15, 137, 462, 797 }, new int[] { 0, 0, 0, 9, 38, 136, 351, 527, 499, 255, 44, 0, 0 }));
			ddc.Add(new DegreeDays(257, 3659, 1859, new int[] { 0, 902, 675, 464, 176, 31, 0, 0, 0, 15, 137, 462, 797 }, new int[] { 0, 0, 0, 9, 38, 136, 351, 527, 499, 255, 44, 0, 0 }));
			ddc.Add(new DegreeDays(258, 3659, 1859, new int[] { 0, 902, 675, 464, 176, 31, 0, 0, 0, 15, 137, 462, 797 }, new int[] { 0, 0, 0, 9, 38, 136, 351, 527, 499, 255, 44, 0, 0 }));
			ddc.Add(new DegreeDays(259, 3659, 1859, new int[] { 0, 902, 675, 464, 176, 31, 0, 0, 0, 15, 137, 462, 797 }, new int[] { 0, 0, 0, 9, 38, 136, 351, 527, 499, 255, 44, 0, 0 }));
			ddc.Add(new DegreeDays(367, 7318, 454, new int[] { 0, 1355, 1192, 1029, 636, 309, 89, 16, 32, 185, 509, 802, 1165 }, new int[] { 0, 0, 0, 0, 2, 22, 80, 174, 135, 40, 2, 0, 0 }));
			ddc.Add(new DegreeDays(368, 8284, 440, new int[] { 0, 1605, 1360, 1138, 664, 279, 74, 13, 39, 224, 572, 919, 1398 }, new int[] { 0, 0, 0, 0, 1, 24, 88, 177, 123, 26, 1, 0, 0 }));
			ddc.Add(new DegreeDays(369, 9531, 215, new int[] { 0, 1731, 1469, 1273, 796, 391, 156, 56, 98, 327, 675, 1046, 1514 }, new int[] { 0, 0, 0, 0, 1, 11, 44, 87, 60, 12, 0, 0, 0 }));
			ddc.Add(new DegreeDays(370, 10292, 126, new int[] { 0, 1832, 1528, 1309, 818, 480, 229, 80, 122, 382, 727, 1134, 1650 }, new int[] { 0, 0, 0, 0, 0, 3, 13, 58, 45, 6, 0, 0, 0 }));
			ddc.Add(new DegreeDays(371, 6345, 759, new int[] { 0, 1256, 1078, 892, 533, 217, 40, 3, 9, 109, 399, 722, 1085 }, new int[] { 0, 0, 0, 0, 5, 44, 151, 264, 210, 78, 6, 0, 0 }));
			ddc.Add(new DegreeDays(260, 5158, 8, new int[] { 0, 716, 582, 598, 504, 388, 243, 157, 141, 201, 384, 543, 701 }, new int[] { 0, 0, 0, 0, 0, 0, 0, 0, 8, 0, 0, 0, 0 }));
			ddc.Add(new DegreeDays(261, 4546, 300, new int[] { 0, 750, 582, 546, 432, 285, 121, 38, 30, 116, 338, 567, 741 }, new int[] { 0, 0, 0, 0, 0, 0, 31, 109, 110, 50, 0, 0, 0 }));
			ddc.Add(new DegreeDays(262, 4611, 725, new int[] { 0, 834, 622, 558, 416, 219, 60, 8, 11, 93, 320, 627, 843 }, new int[] { 0, 0, 0, 0, 0, 8, 105, 253, 244, 108, 7, 0, 0 }));
			ddc.Add(new DegreeDays(263, 4546, 300, new int[] { 0, 750, 582, 546, 432, 285, 121, 38, 30, 116, 338, 567, 741 }, new int[] { 0, 0, 0, 0, 0, 0, 31, 109, 110, 50, 0, 0, 0 }));
			ddc.Add(new DegreeDays(264, 4611, 725, new int[] { 0, 834, 622, 558, 416, 219, 60, 8, 11, 93, 320, 627, 843 }, new int[] { 0, 0, 0, 0, 0, 8, 105, 253, 244, 108, 7, 0, 0 }));
			ddc.Add(new DegreeDays(265, 5294, 701, new int[] { 0, 977, 722, 626, 441, 226, 71, 15, 23, 145, 391, 705, 952 }, new int[] { 0, 0, 0, 0, 0, 6, 107, 260, 240, 88, 0, 0, 0 }));
			ddc.Add(new DegreeDays(266, 7785, 202, new int[] { 0, 1293, 997, 890, 666, 440, 224, 58, 103, 315, 620, 942, 1237 }, new int[] { 0, 0, 0, 0, 0, 0, 14, 95, 78, 15, 0, 0, 0 }));
			ddc.Add(new DegreeDays(267, 5294, 701, new int[] { 0, 977, 722, 626, 441, 226, 71, 15, 23, 145, 391, 705, 952 }, new int[] { 0, 0, 0, 0, 0, 6, 107, 260, 240, 88, 0, 0, 0 }));
			ddc.Add(new DegreeDays(268, 7785, 202, new int[] { 0, 1293, 997, 890, 666, 440, 224, 58, 103, 315, 620, 942, 1237 }, new int[] { 0, 0, 0, 0, 0, 0, 14, 95, 78, 15, 0, 0, 0 }));
			ddc.Add(new DegreeDays(269, 5785, 773, new int[] { 0, 1190, 1000, 794, 459, 183, 10, 0, 8, 82, 373, 657, 1029 }, new int[] { 0, 0, 0, 0, 0, 38, 142, 282, 231, 73, 7, 0, 0 }));
			ddc.Add(new DegreeDays(270, 5785, 773, new int[] { 0, 1190, 1000, 794, 459, 183, 10, 0, 8, 82, 373, 657, 1029 }, new int[] { 0, 0, 0, 0, 0, 38, 142, 282, 231, 73, 7, 0, 0 }));
			ddc.Add(new DegreeDays(271, 4954, 1101, new int[] { 0, 1073, 896, 701, 378, 123, 5, 0, 0, 32, 283, 558, 905 }, new int[] { 0, 0, 0, 0, 0, 58, 209, 363, 326, 128, 17, 0, 0 }));
			ddc.Add(new DegreeDays(272, 4954, 1101, new int[] { 0, 1073, 896, 701, 378, 123, 5, 0, 0, 32, 283, 558, 905 }, new int[] { 0, 0, 0, 0, 0, 58, 209, 363, 326, 128, 17, 0, 0 }));
			ddc.Add(new DegreeDays(273, 5347, 962, new int[] { 0, 1128, 944, 738, 402, 147, 9, 0, 0, 59, 329, 618, 973 }, new int[] { 0, 0, 0, 0, 0, 48, 186, 332, 285, 101, 10, 0, 0 }));
			ddc.Add(new DegreeDays(274, 5347, 962, new int[] { 0, 1128, 944, 738, 402, 147, 9, 0, 0, 59, 329, 618, 973 }, new int[] { 0, 0, 0, 0, 0, 48, 186, 332, 285, 101, 10, 0, 0 }));
			ddc.Add(new DegreeDays(275, 6291, 539, new int[] { 0, 1249, 1070, 871, 510, 217, 38, 0, 11, 109, 423, 708, 1085 }, new int[] { 0, 0, 0, 0, 0, 25, 104, 210, 163, 37, 0, 0, 0 }));
			ddc.Add(new DegreeDays(276, 6087, 622, new int[] { 0, 1234, 1036, 834, 477, 200, 30, 0, 9, 106, 408, 696, 1057 }, new int[] { 0, 0, 0, 0, 0, 29, 114, 229, 189, 61, 0, 0, 0 }));
			ddc.Add(new DegreeDays(277, 5968, 654, new int[] { 0, 1206, 1016, 794, 462, 214, 36, 6, 14, 100, 400, 681, 1039 }, new int[] { 0, 0, 0, 0, 0, 44, 123, 227, 184, 67, 9, 0, 0 }));
			ddc.Add(new DegreeDays(278, 6279, 550, new int[] { 0, 1228, 1098, 902, 567, 268, 52, 0, 12, 89, 370, 654, 1039 }, new int[] { 0, 0, 0, 0, 0, 17, 97, 199, 171, 59, 7, 0, 0 }));
			ddc.Add(new DegreeDays(279, 0, 5558, new int[] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 }, new int[] { 0, 372, 339, 403, 432, 493, 519, 546, 549, 525, 524, 450, 406 }));
			ddc.Add(new DegreeDays(280, 0, 5558, new int[] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 }, new int[] { 0, 372, 339, 403, 432, 493, 519, 546, 549, 525, 524, 450, 406 }));
			ddc.Add(new DegreeDays(281, 0, 5558, new int[] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 }, new int[] { 0, 372, 339, 403, 432, 493, 519, 546, 549, 525, 524, 450, 406 }));
			ddc.Add(new DegreeDays(282, 0, 5558, new int[] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 }, new int[] { 0, 372, 339, 403, 432, 493, 519, 546, 549, 525, 524, 450, 406 }));
			ddc.Add(new DegreeDays(283, 0, 5558, new int[] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 }, new int[] { 0, 372, 339, 403, 432, 493, 519, 546, 549, 525, 524, 450, 406 }));
			ddc.Add(new DegreeDays(284, 0, 5558, new int[] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 }, new int[] { 0, 372, 339, 403, 432, 493, 519, 546, 549, 525, 524, 450, 406 }));
			ddc.Add(new DegreeDays(285, 5884, 606, new int[] { 0, 1150, 988, 856, 528, 246, 31, 0, 8, 90, 359, 630, 998 }, new int[] { 0, 0, 0, 0, 0, 7, 88, 239, 203, 63, 6, 0, 0 }));
			ddc.Add(new DegreeDays(286, 3272, 1473, new int[] { 0, 772, 605, 412, 177, 49, 0, 0, 0, 7, 175, 402, 673 }, new int[] { 0, 0, 0, 0, 21, 139, 300, 409, 375, 193, 36, 0, 0 }));
			ddc.Add(new DegreeDays(287, 3272, 1473, new int[] { 0, 772, 605, 412, 177, 49, 0, 0, 0, 7, 175, 402, 673 }, new int[] { 0, 0, 0, 0, 21, 139, 300, 409, 375, 193, 36, 0, 0 }));
			ddc.Add(new DegreeDays(288, 3272, 1473, new int[] { 0, 772, 605, 412, 177, 49, 0, 0, 0, 7, 175, 402, 673 }, new int[] { 0, 0, 0, 0, 21, 139, 300, 409, 375, 193, 36, 0, 0 }));
			ddc.Add(new DegreeDays(289, 2649, 1966, new int[] { 0, 665, 514, 317, 108, 18, 0, 0, 0, 5, 138, 323, 561 }, new int[] { 0, 8, 0, 13, 48, 201, 372, 490, 456, 281, 86, 11, 0 }));
			ddc.Add(new DegreeDays(290, 2649, 1966, new int[] { 0, 665, 514, 317, 108, 18, 0, 0, 0, 5, 138, 323, 561 }, new int[] { 0, 8, 0, 13, 48, 201, 372, 490, 456, 281, 86, 11, 0 }));
			ddc.Add(new DegreeDays(291, 2649, 1966, new int[] { 0, 665, 514, 317, 108, 18, 0, 0, 0, 5, 138, 323, 561 }, new int[] { 0, 8, 0, 13, 48, 201, 372, 490, 456, 281, 86, 11, 0 }));
			ddc.Add(new DegreeDays(292, 1866, 2313, new int[] { 0, 525, 399, 238, 57, 0, 0, 0, 0, 0, 46, 194, 407 }, new int[] { 0, 11, 7, 18, 69, 242, 414, 521, 496, 354, 136, 35, 10 }));
			ddc.Add(new DegreeDays(293, 8446, 633, new int[] { 0, 1702, 1352, 1091, 594, 269, 68, 15, 29, 195, 549, 1041, 1541 }, new int[] { 0, 0, 0, 0, 0, 24, 116, 257, 203, 33, 0, 0, 0 }));
			ddc.Add(new DegreeDays(294, 8446, 633, new int[] { 0, 1702, 1352, 1091, 594, 269, 68, 15, 29, 195, 549, 1041, 1541 }, new int[] { 0, 0, 0, 0, 0, 24, 116, 257, 203, 33, 0, 0, 0 }));
			ddc.Add(new DegreeDays(295, 8446, 633, new int[] { 0, 1702, 1352, 1091, 594, 269, 68, 15, 29, 195, 549, 1041, 1541 }, new int[] { 0, 0, 0, 0, 0, 24, 116, 257, 203, 33, 0, 0, 0 }));
			ddc.Add(new DegreeDays(296, 7301, 611, new int[] { 0, 1324, 1072, 958, 597, 311, 103, 18, 31, 211, 505, 906, 1265 }, new int[] { 0, 0, 0, 0, 0, 7, 97, 241, 205, 61, 0, 0, 0 }));
			ddc.Add(new DegreeDays(297, 7301, 611, new int[] { 0, 1324, 1072, 958, 597, 311, 103, 18, 31, 211, 505, 906, 1265 }, new int[] { 0, 0, 0, 0, 0, 7, 97, 241, 205, 61, 0, 0, 0 }));
			ddc.Add(new DegreeDays(298, 7301, 611, new int[] { 0, 1324, 1072, 958, 597, 311, 103, 18, 31, 211, 505, 906, 1265 }, new int[] { 0, 0, 0, 0, 0, 7, 97, 241, 205, 61, 0, 0, 0 }));
			ddc.Add(new DegreeDays(299, 7923, 743, new int[] { 0, 1606, 1277, 1026, 561, 256, 48, 10, 22, 173, 512, 978, 1454 }, new int[] { 0, 0, 0, 0, 0, 27, 135, 302, 232, 47, 0, 0, 0 }));
			ddc.Add(new DegreeDays(300, 7809, 744, new int[] { 0, 1587, 1268, 1008, 543, 240, 50, 10, 22, 165, 508, 960, 1448 }, new int[] { 0, 0, 0, 0, 0, 35, 149, 298, 220, 42, 0, 0, 0 }));
			ddc.Add(new DegreeDays(301, 7809, 744, new int[] { 0, 1587, 1268, 1008, 543, 240, 50, 10, 22, 165, 508, 960, 1448 }, new int[] { 0, 0, 0, 0, 0, 35, 149, 298, 220, 42, 0, 0, 0 }));
			ddc.Add(new DegreeDays(302, 3937, 1266, new int[] { 0, 899, 697, 496, 234, 94, 0, 0, 0, 18, 238, 489, 772 }, new int[] { 0, 0, 0, 0, 9, 106, 246, 360, 341, 171, 33, 0, 0 }));
			ddc.Add(new DegreeDays(303, 4183, 1156, new int[] { 0, 930, 734, 530, 252, 96, 0, 0, 0, 32, 260, 531, 818 }, new int[] { 0, 0, 0, 0, 6, 93, 226, 335, 316, 155, 25, 0, 0 }));
			ddc.Add(new DegreeDays(304, 3729, 1616, new int[] { 0, 893, 689, 469, 193, 59, 0, 0, 0, 21, 195, 450, 760 }, new int[] { 0, 0, 0, 10, 19, 143, 318, 443, 406, 225, 52, 0, 0 }));
			ddc.Add(new DegreeDays(305, 3082, 2118, new int[] { 0, 784, 582, 383, 127, 25, 0, 0, 0, 10, 131, 380, 660 }, new int[] { 0, 0, 0, 14, 64, 217, 423, 546, 496, 286, 72, 0, 0 }));
			ddc.Add(new DegreeDays(306, 3431, 1689, new int[] { 0, 812, 613, 437, 161, 25, 0, 0, 0, 17, 154, 456, 756 }, new int[] { 0, 0, 0, 10, 44, 162, 366, 465, 400, 200, 42, 0, 0 }));
			ddc.Add(new DegreeDays(307, 2584, 2451, new int[] { 0, 688, 493, 299, 94, 7, 0, 0, 0, 6, 75, 317, 605 }, new int[] { 0, 0, 0, 23, 106, 245, 456, 589, 564, 336, 118, 14, 0 }));
			ddc.Add(new DegreeDays(308, 2407, 2603, new int[] { 0, 670, 484, 286, 75, 0, 0, 0, 0, 0, 51, 275, 566 }, new int[] { 0, 0, 5, 29, 90, 246, 480, 629, 617, 372, 119, 11, 5 }));
			ddc.Add(new DegreeDays(309, 2407, 2603, new int[] { 0, 670, 484, 286, 75, 0, 0, 0, 0, 0, 51, 275, 566 }, new int[] { 0, 0, 5, 29, 90, 246, 480, 629, 617, 372, 119, 11, 5 }));
			ddc.Add(new DegreeDays(310, 2708, 2094, new int[] { 0, 688, 473, 316, 110, 7, 0, 0, 0, 0, 88, 378, 648 }, new int[] { 0, 0, 0, 10, 62, 217, 462, 536, 468, 282, 57, 0, 0 }));
			ddc.Add(new DegreeDays(311, 1692, 3016, new int[] { 0, 514, 353, 175, 28, 2, 2, 0, 0, 0, 17, 175, 426 }, new int[] { 0, 12, 11, 66, 166, 329, 489, 605, 614, 456, 206, 52, 10 }));
			ddc.Add(new DegreeDays(312, 1644, 2996, new int[] { 0, 494, 332, 167, 32, 0, 0, 0, 0, 0, 30, 180, 409 }, new int[] { 0, 8, 10, 64, 161, 326, 516, 620, 617, 429, 191, 42, 12 }));
			ddc.Add(new DegreeDays(313, 1499, 2764, new int[] { 0, 450, 307, 155, 32, 0, 0, 0, 0, 0, 26, 177, 352 }, new int[] { 0, 13, 10, 43, 149, 316, 471, 552, 546, 408, 172, 63, 21 }));
			ddc.Add(new DegreeDays(314, 1506, 3142, new int[] { 0, 465, 291, 139, 14, 0, 0, 0, 0, 0, 19, 171, 407 }, new int[] { 0, 6, 14, 87, 203, 381, 531, 626, 614, 444, 196, 33, 7 }));
			ddc.Add(new DegreeDays(315, 635, 3888, new int[] { 0, 232, 135, 41, 0, 0, 0, 0, 0, 0, 0, 50, 177 }, new int[] { 0, 59, 62, 159, 312, 462, 540, 605, 605, 504, 332, 161, 87 }));
			ddc.Add(new DegreeDays(316, 5765, 1047, new int[] { 0, 1150, 865, 719, 464, 215, 51, 0, 0, 108, 373, 726, 1094 }, new int[] { 0, 0, 0, 0, 0, 23, 174, 400, 329, 114, 7, 0, 0 }));
			ddc.Add(new DegreeDays(317, 5765, 1047, new int[] { 0, 1150, 865, 719, 464, 215, 51, 0, 0, 108, 373, 726, 1094 }, new int[] { 0, 0, 0, 0, 0, 23, 174, 400, 329, 114, 7, 0, 0 }));
			ddc.Add(new DegreeDays(318, 5765, 1047, new int[] { 0, 1150, 865, 719, 464, 215, 51, 0, 0, 108, 373, 726, 1094 }, new int[] { 0, 0, 0, 0, 0, 23, 174, 400, 329, 114, 7, 0, 0 }));
			ddc.Add(new DegreeDays(319, 5765, 1047, new int[] { 0, 1150, 865, 719, 464, 215, 51, 0, 0, 108, 373, 726, 1094 }, new int[] { 0, 0, 0, 0, 0, 23, 174, 400, 329, 114, 7, 0, 0 }));
			ddc.Add(new DegreeDays(320, 5765, 1047, new int[] { 0, 1150, 865, 719, 464, 215, 51, 0, 0, 108, 373, 726, 1094 }, new int[] { 0, 0, 0, 0, 0, 23, 174, 400, 329, 114, 7, 0, 0 }));
			ddc.Add(new DegreeDays(321, 5765, 1047, new int[] { 0, 1150, 865, 719, 464, 215, 51, 0, 0, 108, 373, 726, 1094 }, new int[] { 0, 0, 0, 0, 0, 23, 174, 400, 329, 114, 7, 0, 0 }));
			ddc.Add(new DegreeDays(322, 5765, 1047, new int[] { 0, 1150, 865, 719, 464, 215, 51, 0, 0, 108, 373, 726, 1094 }, new int[] { 0, 0, 0, 0, 0, 23, 174, 400, 329, 114, 7, 0, 0 }));
			ddc.Add(new DegreeDays(323, 3495, 1422, new int[] { 0, 803, 672, 508, 249, 51, 0, 0, 0, 11, 164, 380, 657 }, new int[] { 0, 0, 0, 0, 9, 85, 277, 409, 378, 218, 46, 0, 0 }));
			ddc.Add(new DegreeDays(324, 3963, 1348, new int[] { 0, 908, 736, 527, 241, 61, 0, 0, 0, 23, 233, 462, 772 }, new int[] { 0, 0, 0, 0, 10, 92, 270, 403, 366, 173, 34, 0, 0 }));
			ddc.Add(new DegreeDays(325, 4261, 1048, new int[] { 0, 918, 778, 623, 351, 120, 6, 0, 0, 21, 219, 462, 763 }, new int[] { 0, 0, 0, 0, 0, 67, 207, 329, 298, 125, 22, 0, 0 }));
			ddc.Add(new DegreeDays(326, 4340, 1096, new int[] { 0, 955, 778, 577, 283, 95, 6, 0, 0, 32, 270, 519, 825 }, new int[] { 0, 0, 0, 0, 0, 43, 192, 341, 322, 165, 33, 0, 0 }));
			ddc.Add(new DegreeDays(327, 4360, 1052, new int[] { 0, 946, 776, 564, 289, 105, 5, 0, 0, 39, 283, 525, 828 }, new int[] { 0, 0, 0, 0, 7, 78, 200, 329, 298, 120, 20, 0, 0 }));
			ddc.Add(new DegreeDays(328, 4360, 1052, new int[] { 0, 946, 776, 564, 289, 105, 5, 0, 0, 39, 283, 525, 828 }, new int[] { 0, 0, 0, 0, 7, 78, 200, 329, 298, 120, 20, 0, 0 }));
			ddc.Add(new DegreeDays(329, 7771, 388, new int[] { 0, 1510, 1310, 1063, 633, 282, 58, 6, 29, 199, 533, 846, 1302 }, new int[] { 0, 0, 0, 0, 0, 13, 64, 176, 119, 16, 0, 0, 0 }));
			ddc.Add(new DegreeDays(330, 7771, 388, new int[] { 0, 1510, 1310, 1063, 633, 282, 58, 6, 29, 199, 533, 846, 1302 }, new int[] { 0, 0, 0, 0, 0, 13, 64, 176, 119, 16, 0, 0, 0 }));
			ddc.Add(new DegreeDays(331, 7771, 388, new int[] { 0, 1510, 1310, 1063, 633, 282, 58, 6, 29, 199, 533, 846, 1302 }, new int[] { 0, 0, 0, 0, 0, 13, 64, 176, 119, 16, 0, 0, 0 }));
			ddc.Add(new DegreeDays(332, 5655, 101, new int[] { 0, 837, 669, 657, 525, 366, 201, 96, 102, 222, 474, 675, 831 }, new int[] { 0, 0, 0, 0, 0, 0, 12, 31, 49, 9, 0, 0, 0 }));
			ddc.Add(new DegreeDays(333, 4611, 167, new int[] { 0, 735, 580, 570, 438, 276, 123, 44, 58, 144, 357, 561, 725 }, new int[] { 0, 0, 0, 0, 0, 0, 15, 54, 80, 18, 0, 0, 0 }));
			ddc.Add(new DegreeDays(334, 4908, 190, new int[] { 0, 772, 602, 601, 474, 307, 144, 58, 65, 156, 378, 591, 760 }, new int[] { 0, 0, 0, 0, 0, 0, 21, 64, 81, 24, 0, 0, 0 }));
			ddc.Add(new DegreeDays(335, 5655, 101, new int[] { 0, 837, 669, 657, 525, 366, 201, 96, 102, 222, 474, 675, 831 }, new int[] { 0, 0, 0, 0, 0, 0, 12, 31, 49, 9, 0, 0, 0 }));
			ddc.Add(new DegreeDays(336, 5868, 6, new int[] { 0, 788, 644, 673, 564, 437, 288, 192, 180, 256, 453, 627, 766 }, new int[] { 0, 0, 0, 0, 0, 0, 0, 0, 6, 0, 0, 0, 0 }));
			ddc.Add(new DegreeDays(337, 5868, 6, new int[] { 0, 788, 644, 673, 564, 437, 288, 192, 180, 256, 453, 627, 766 }, new int[] { 0, 0, 0, 0, 0, 0, 0, 0, 6, 0, 0, 0, 0 }));
			ddc.Add(new DegreeDays(338, 6842, 398, new int[] { 0, 1175, 888, 815, 573, 344, 139, 30, 56, 223, 549, 897, 1153 }, new int[] { 0, 0, 0, 0, 0, 0, 49, 148, 161, 40, 0, 0, 0 }));
			ddc.Add(new DegreeDays(339, 5967, 458, new int[] { 0, 1094, 801, 682, 468, 255, 90, 19, 38, 169, 468, 792, 1091 }, new int[] { 0, 0, 0, 0, 0, 7, 78, 171, 162, 40, 0, 0, 0 }));
			ddc.Add(new DegreeDays(340, 6842, 398, new int[] { 0, 1175, 888, 815, 573, 344, 139, 30, 56, 223, 549, 897, 1153 }, new int[] { 0, 0, 0, 0, 0, 0, 49, 148, 161, 40, 0, 0, 0 }));
			ddc.Add(new DegreeDays(341, 5967, 458, new int[] { 0, 1094, 801, 682, 468, 255, 90, 19, 38, 169, 468, 792, 1091 }, new int[] { 0, 0, 0, 0, 0, 7, 78, 171, 162, 40, 0, 0, 0 }));
			ddc.Add(new DegreeDays(342, 8089, 381, new int[] { 0, 1572, 1308, 1085, 630, 318, 93, 17, 43, 189, 527, 918, 1389 }, new int[] { 0, 0, 0, 0, 0, 23, 78, 163, 108, 9, 0, 0, 0 }));
			ddc.Add(new DegreeDays(343, 8089, 381, new int[] { 0, 1572, 1308, 1085, 630, 318, 93, 17, 43, 189, 527, 918, 1389 }, new int[] { 0, 0, 0, 0, 0, 23, 78, 163, 108, 9, 0, 0, 0 }));
			ddc.Add(new DegreeDays(344, 8089, 381, new int[] { 0, 1572, 1308, 1085, 630, 318, 93, 17, 43, 189, 527, 918, 1389 }, new int[] { 0, 0, 0, 0, 0, 23, 78, 163, 108, 9, 0, 0, 0 }));
			ddc.Add(new DegreeDays(345, 7491, 692, new int[] { 0, 1569, 1263, 998, 519, 220, 36, 7, 16, 131, 464, 882, 1386 }, new int[] { 0, 0, 0, 0, 0, 50, 141, 270, 196, 35, 0, 0, 0 }));
			ddc.Add(new DegreeDays(346, 7491, 692, new int[] { 0, 1569, 1263, 998, 519, 220, 36, 7, 16, 131, 464, 882, 1386 }, new int[] { 0, 0, 0, 0, 0, 50, 141, 270, 196, 35, 0, 0, 0 }));
			ddc.Add(new DegreeDays(347, 8089, 381, new int[] { 0, 1572, 1308, 1085, 630, 318, 93, 17, 43, 189, 527, 918, 1389 }, new int[] { 0, 0, 0, 0, 0, 23, 78, 163, 108, 9, 0, 0, 0 }));
			ddc.Add(new DegreeDays(348, 7673, 485, new int[] { 0, 1519, 1243, 1014, 588, 294, 68, 12, 38, 168, 499, 888, 1342 }, new int[] { 0, 0, 0, 0, 0, 30, 104, 198, 141, 12, 0, 0, 0 }));
			ddc.Add(new DegreeDays(349, 7673, 485, new int[] { 0, 1519, 1243, 1014, 588, 294, 68, 12, 38, 168, 499, 888, 1342 }, new int[] { 0, 0, 0, 0, 0, 30, 104, 198, 141, 12, 0, 0, 0 }));
			ddc.Add(new DegreeDays(350, 7324, 479, new int[] { 0, 1429, 1176, 983, 618, 338, 82, 14, 27, 123, 456, 819, 1259 }, new int[] { 0, 0, 0, 0, 0, 16, 82, 197, 160, 24, 0, 0, 0 }));
			ddc.Add(new DegreeDays(351, 6120, 346, new int[] { 0, 1175, 986, 787, 504, 249, 62, 11, 23, 131, 458, 711, 1023 }, new int[] { 0, 0, 0, 0, 0, 13, 56, 126, 110, 35, 6, 0, 0 }));
			ddc.Add(new DegreeDays(352, 6120, 346, new int[] { 0, 1175, 986, 787, 504, 249, 62, 11, 23, 131, 458, 711, 1023 }, new int[] { 0, 0, 0, 0, 0, 13, 56, 126, 110, 35, 6, 0, 0 }));
			ddc.Add(new DegreeDays(353, 4665, 1031, new int[] { 0, 1023, 837, 589, 298, 121, 11, 0, 7, 53, 300, 555, 871 }, new int[] { 0, 0, 0, 0, 6, 83, 202, 313, 276, 125, 26, 0, 0 }));
			ddc.Add(new DegreeDays(354, 5558, 463, new int[] { 0, 1110, 921, 713, 414, 198, 47, 10, 19, 117, 400, 648, 961 }, new int[] { 0, 0, 0, 0, 0, 28, 83, 156, 136, 51, 9, 0, 0 }));
			ddc.Add(new DegreeDays(355, 5558, 463, new int[] { 0, 1110, 921, 713, 414, 198, 47, 10, 19, 117, 400, 648, 961 }, new int[] { 0, 0, 0, 0, 0, 28, 83, 156, 136, 51, 9, 0, 0 }));
			ddc.Add(new DegreeDays(356, 6120, 346, new int[] { 0, 1175, 986, 787, 504, 249, 62, 11, 23, 131, 458, 711, 1023 }, new int[] { 0, 0, 0, 0, 0, 13, 56, 126, 110, 35, 6, 0, 0 }));
			ddc.Add(new DegreeDays(357, 7804, 439, new int[] { 0, 1370, 1081, 964, 636, 381, 156, 28, 63, 269, 567, 975, 1314 }, new int[] { 0, 0, 0, 0, 0, 0, 69, 170, 168, 32, 0, 0, 0 }));
			ddc.Add(new DegreeDays(358, 7889, 479, new int[] { 0, 1411, 1109, 964, 651, 376, 137, 9, 38, 246, 564, 1014, 1370 }, new int[] { 0, 0, 0, 0, 0, 0, 80, 195, 159, 45, 0, 0, 0 }));
			ddc.Add(new DegreeDays(359, 7682, 285, new int[] { 0, 1321, 1078, 977, 666, 394, 139, 10, 36, 250, 577, 957, 1277 }, new int[] { 0, 0, 0, 0, 0, 0, 39, 136, 87, 23, 0, 0, 0 }));
			ddc.Add(new DegreeDays(360, 7804, 439, new int[] { 0, 1370, 1081, 964, 636, 381, 156, 28, 63, 269, 567, 975, 1314 }, new int[] { 0, 0, 0, 0, 0, 0, 69, 170, 168, 32, 0, 0, 0 }));
			ddc.Add(new DegreeDays(361, 7804, 439, new int[] { 0, 1370, 1081, 964, 636, 381, 156, 28, 63, 269, 567, 975, 1314 }, new int[] { 0, 0, 0, 0, 0, 0, 69, 170, 168, 32, 0, 0, 0 }));
			ddc.Add(new DegreeDays(362, 7804, 439, new int[] { 0, 1370, 1081, 964, 636, 381, 156, 28, 63, 269, 567, 975, 1314 }, new int[] { 0, 0, 0, 0, 0, 0, 69, 170, 168, 32, 0, 0, 0 }));
			ddc.Add(new DegreeDays(363, 7889, 479, new int[] { 0, 1411, 1109, 964, 651, 376, 137, 9, 38, 246, 564, 1014, 1370 }, new int[] { 0, 0, 0, 0, 0, 0, 80, 195, 159, 45, 0, 0, 0 }));
			ddc.Add(new DegreeDays(364, 7326, 285, new int[] { 0, 1194, 1000, 973, 675, 403, 150, 31, 44, 251, 558, 894, 1153 }, new int[] { 0, 0, 0, 0, 0, 0, 39, 136, 87, 23, 0, 0, 0 }));
			ddc.Add(new DegreeDays(365, 7889, 479, new int[] { 0, 1411, 1109, 964, 651, 376, 137, 9, 38, 246, 564, 1014, 1370 }, new int[] { 0, 0, 0, 0, 0, 0, 80, 195, 159, 45, 0, 0, 0 }));
			ddc.Add(new DegreeDays(366, 7682, 445, new int[] { 0, 1321, 1078, 977, 666, 394, 139, 10, 36, 250, 577, 957, 1277 }, new int[] { 0, 0, 0, 0, 0, 0, 70, 190, 151, 34, 0, 0, 0 }));

			return (ddc);
		}

		// SELECT TOP 1000 [STATE]
		//    ,[DIV]
		//    ,[TemperatureRegion]
		//    ,[HDD]
		//    ,[CDD]
		//    ,'ddc.Add(new DegreeDays(' + CAST([TemperatureRegion] as varchar(30)) + ', ' + CAST([HDD] as varchar(30)) + ', ' + CAST([CDD] as varchar(30)) + '));'
		//FROM [esov3static].[dbo].[HDD_CDD]

		/*
		 * 
		 * NEWER
		 * 
		 * 
		  SELECT    dbo.HDD.TemperatureRegion, dbo.HDD.ANN AS HddAnn, dbo.CDD.ANN AS CddAnn, dbo.HDD_CDD.HDD AS AnnualHDD, 
					  dbo.HDD_CDD.CDD AS AnnualCDD, dbo.HDD.JAN, dbo.HDD.FEB, dbo.HDD.MAR, dbo.HDD.APR, dbo.HDD.MAY, dbo.HDD.JUN, dbo.HDD.JUL, dbo.HDD.AUG, 
					  dbo.HDD.SEP, dbo.HDD.OCT, dbo.HDD.NOV, dbo.HDD.DEC,
					  'ddc.Add(new DegreeDays(' + CAST(dbo.HDD.TemperatureRegion as varchar(30)) + ', ' + CAST(dbo.HDD.ANN as varchar(30))
					  + ', ' + CAST(dbo.CDD.ANN as varchar(30))
					  + ', new int[] { 0' 
					  + ',' +  CAST(dbo.HDD.Jan as varchar(30)) 
					  + ',' +  CAST(dbo.HDD.Feb as varchar(30)) 
					  + ',' +  CAST(dbo.HDD.Mar as varchar(30)) 
					  + ',' +  CAST(dbo.HDD.Apr as varchar(30)) 
					  + ',' +  CAST(dbo.HDD.May as varchar(30)) 
					  + ',' +  CAST(dbo.HDD.JUN as varchar(30)) 
					  + ',' +  CAST(dbo.HDD.Jul as varchar(30)) 
					  + ',' +  CAST(dbo.HDD.Aug as varchar(30)) 
					  + ',' +  CAST(dbo.HDD.Sep as varchar(30)) 
					  + ',' +  CAST(dbo.HDD.Oct as varchar(30)) 
					  + ',' +  CAST(dbo.HDD.Nov as varchar(30)) 
					  + ',' +  CAST(dbo.HDD.Dec as varchar(30)) 
					  + '} , new int[] { 0' 
					  + ',' +  CAST(dbo.CDD.Jan as varchar(30)) 
					  + ',' +  CAST(dbo.CDD.Feb as varchar(30)) 
					  + ',' +  CAST(dbo.CDD.Mar as varchar(30)) 
					  + ',' +  CAST(dbo.CDD.Apr as varchar(30)) 
					  + ',' +  CAST(dbo.CDD.May as varchar(30)) 
					  + ',' +  CAST(dbo.CDD.JUN as varchar(30)) 
					  + ',' +  CAST(dbo.CDD.Jul as varchar(30)) 
					  + ',' +  CAST(dbo.CDD.Aug as varchar(30)) 
					  + ',' +  CAST(dbo.CDD.Sep as varchar(30)) 
					  + ',' +  CAST(dbo.CDD.Oct as varchar(30)) 
					  + ',' +  CAST(dbo.CDD.Nov as varchar(30)) 
					  + ',' +  CAST(dbo.CDD.Dec as varchar(30)) 
					  + ' } ));'
					  
			FROM         dbo.HDD INNER JOIN
					  dbo.CDD ON dbo.HDD.TemperatureRegion = dbo.CDD.TemperatureRegion INNER JOIN
					  dbo.HDD_CDD ON dbo.HDD.TemperatureRegion = dbo.HDD_CDD.TemperatureRegion
		 * 
		 * */


		/// <summary>
		/// Evaporation collection by state
		/// </summary>
		/// <returns></returns>
		public static EvaporationCollection CreateEvaporationCollection()
		{
			// generate a EvaporationCollection
			var ec = new EvaporationCollection();

			ec.Add(new Evaporation("AK", 0.37, new double[] { 0, 0.06, 0.31, 0.64, 1.83, 2.84, 4.18, 4.49, 2.61, 1.61, 0.48, 0.09, 0.05 }));
			ec.Add(new Evaporation("AL", 2.33002, new double[] { 0, 1.02055, 1.30947, 2.0644, 2.70282, 4.17656, 4.15909, 4.08919, 3.86783, 3.2795, 2.56302, 1.40267, 1.09045 }));
			ec.Add(new Evaporation("AR", 1.93858, new double[] { 0, 0.876088, 1.06249, 1.84538, 2.55836, 3.94938, 4.36296, 4.67169, 4.33966, 3.19795, 2.38827, 1.20695, 3.68609 }));
			ec.Add(new Evaporation("AZ", 3.51833, new double[] { 0, 1.62635, 1.89198, 2.95447, 4.21734, 6.90851, 7.88129, 7.38034, 6.36678, 5.45807, 4.12414, 2.07372, 1.51917 }));
			ec.Add(new Evaporation("CA", 3.22, new double[] { 0, 1.44, 1.78, 2.4, 2.98, 4.42, 4.67, 5.19, 4.86, 4.11, 3.14, 1.71, 1.34 }));
			ec.Add(new Evaporation("CO", 2.07838, new double[] { 0, 0.983268, 1.09511, 1.86868, 2.93583, 5.00954, 6.06388, 6.32018, 5.60952, 4.19986, 2.99408, 1.30481, 0.964628 }));
			ec.Add(new Evaporation("CT", 1.29083, new double[] { 0, 0.605805, 0.685026, 1.16501, 1.8547, 3.05815, 3.3727, 3.57076, 3.26203, 2.36497, 1.79994, 0.894728, 0.596485 }));
			ec.Add(new Evaporation("DC", 1.66363, new double[] { 0, 0.759587, 0.904048, 1.53781, 2.2741, 3.65231, 4.19986, 4.40956, 3.90278, 2.86592, 2.07372, 1.12307, 0.782887 }));
			ec.Add(new Evaporation("DE", 1.50519, new double[] { 0, 0.694346, 0.810847, 1.40267, 2.02246, 3.22708, 3.72803, 3.72803, 3.44843, 2.70282, 1.96304, 1.02521, 0.708326 }));
			ec.Add(new Evaporation("FL", 3.60221, new double[] { 0, 1.67295, 1.92926, 2.75874, 3.50901, 4.82897, 4.42121, 4.53771, 4.23481, 3.59406, 3.2562, 2.0411, 1.71489 }));
			ec.Add(new Evaporation("GA", 2.30672, new double[] { 0, 1.01589, 1.29083, 2.0411, 2.76806, 4.07171, 4.10666, 4.03093, 3.83288, 3.12223, 2.49312, 1.37937, 1.08579 }));
			ec.Add(new Evaporation("HI", 4.66, new double[] { 0, 2.12, 2.54, 2.95, 3.28, 3.61, 3.78, 3.84, 3.77, 3.46, 2.9, 2.34, 2.05 }));
			ec.Add(new Evaporation("IA", 0.890068, new double[] { 0, 0.382123, 0.507944, 1.08579, 2.0877, 3.66396, 4.22899, 4.22899, 3.56493, 2.50477, 2.06207, 0.810847, 0.433384 }));
			ec.Add(new Evaporation("ID", 1.30015, new double[] { 0, 0.596485, 0.703666, 1.65897, 2.32536, 4.29889, 5.27167, 6.92598, 5.59787, 3.80958, 2.30672, 0.824827, 0.489304 }));
			ec.Add(new Evaporation("IL", 1.02521, new double[] { 0, 0.442704, 0.582505, 1.18365, 2.09236, 3.83871, 4.42121, 4.46199, 3.77463, 2.79602, 2.08537, 0.857447, 0.489304 }));
			ec.Add(new Evaporation("IN", 1.07647, new double[] { 0, 0.470664, 0.605805, 1.14637, 2.00848, 3.59406, 4.20569, 4.20569, 3.72803, 2.72612, 1.96887, 0.815507, 0.489304 }));
			ec.Add(new Evaporation("KS", 1.81276, new double[] { 0, 0.876088, 0.936668, 1.7615, 2.70748, 4.35714, 5.14352, 5.6736, 5.23672, 3.55911, 2.7436, 1.19763, 0.852787 }));
			ec.Add(new Evaporation("KY", 1.32345, new double[] { 0, 0.619785, 0.703666, 1.42597, 2.2741, 3.63483, 3.97268, 4.07171, 3.87366, 2.94748, 2.14944, 1.00191, 0.703666 }));
			ec.Add(new Evaporation("LA", 2.39526, new double[] { 0, 1.08579, 1.30947, 2.03178, 2.52108, 4.03093, 4.22899, 4.20569, 3.90861, 3.30863, 2.76107, 1.44927, 1.07181 }));
			ec.Add(new Evaporation("MA", 1.47723, new double[] { 0, 0.699006, 0.778227, 1.20229, 1.82674, 3.163, 3.39018, 3.54163, 3.11058, 2.27177, 1.80577, 0.932008, 0.722306 }));
			ec.Add(new Evaporation("MD", 1.66363, new double[] { 0, 0.759587, 0.904048, 1.53781, 2.2741, 3.65231, 4.19986, 4.40956, 3.90278, 2.86592, 2.07372, 1.12307, 0.782887 }));
			ec.Add(new Evaporation("ME", 0.904048, new double[] { 0, 0.424064, 0.479984, 0.927348, 1.44461, 2.77272, 3.1164, 3.32028, 2.90087, 1.92809, 1.33394, 0.554545, 0.414744 }));
			ec.Add(new Evaporation("MI", 0.792207, new double[] { 0, 0.349503, 0.442704, 0.890068, 1.63567, 3.12805, 3.72803, 3.93773, 3.29115, 2.19604, 1.54946, 0.671046, 0.377463 }));
			ec.Add(new Evaporation("MN", 0.615125, new double[] { 0, 0.260962, 0.354163, 0.787547, 1.66829, 3.21543, 3.70473, 4.01346, 3.28533, 2.04459, 1.56111, 0.517264, 0.265622 }));
			ec.Add(new Evaporation("MO", 1.51451, new double[] { 0, 0.671046, 0.843467, 1.56111, 2.4838, 4.00763, 4.31054, 4.76489, 4.40956, 3.16883, 2.44652, 1.09977, 0.712986 }));
			ec.Add(new Evaporation("MS", 2.0411, new double[] { 0, 0.890068, 1.15103, 1.9013, 2.57234, 3.92608, 4.18821, 4.12996, 3.73386, 3.11058, 2.37662, 1.25821, 0.922688 }));
			ec.Add(new Evaporation("MT", 1.16501, new double[] { 0, 0.498624, 0.666386, 1.15569, 1.8547, 3.47755, 4.08336, 5.67942, 4.84062, 2.94748, 1.95139, 0.801527, 0.601145 }));
			ec.Add(new Evaporation("NB", 1.106, new double[] { 0, 0.518796, 0.587208, 1.13451, 1.76733, 3.39213, 3.81258, 4.062, 3.5489, 2.35881, 1.63193, 0.678425, 0.507394 }));
			ec.Add(new Evaporation("NC", 2.07372, new double[] { 0, 0.941328, 1.13239, 1.86402, 2.71214, 3.97851, 4.12996, 4.20569, 3.73968, 3.00573, 2.23099, 1.29549, 0.987929 }));
			ec.Add(new Evaporation("ND", 0.535905, new double[] { 0, 0.163101, 0.372803, 0.792207, 1.75218, 3.61153, 4.03093, 4.93964, 4.45034, 2.72612, 1.78247, 0.535905, 0.349503 }));
			ec.Add(new Evaporation("NE", 1.30015, new double[] { 0, 0.573185, 0.726966, 1.39335, 2.4139, 4.01928, 4.82897, 5.24837, 4.66004, 3.13388, 2.3941, 1.00191, 0.699006 }));
			ec.Add(new Evaporation("NH", 0.806187, new double[] { 0, 0.363483, 0.442704, 0.876088, 1.46791, 2.80767, 3.0465, 3.24455, 2.8135, 1.78829, 1.28734, 0.531245, 0.382123 }));
			ec.Add(new Evaporation("NJ", 1.59839, new double[] { 0, 0.754927, 0.843467, 1.43063, 2.10634, 3.4659, 3.90278, 3.99598, 3.59988, 2.75525, 2.02129, 1.07181, 0.745606 }));
			ec.Add(new Evaporation("NM", 2.94515, new double[] { 0, 1.23957, 1.70557, 2.72612, 3.90045, 6.93181, 7.70654, 6.34348, 6.05223, 4.81149, 3.51833, 1.71489, 1.25355 }));
			ec.Add(new Evaporation("NV", 1.89198, new double[] { 0, 0.834147, 1.05783, 1.89664, 2.71214, 4.90469, 6.0231, 7.01919, 6.12213, 4.39209, 2.85427, 1.18365, 0.787547 }));
			ec.Add(new Evaporation("NY", 1.00657, new double[] { 0, 0.470664, 0.535905, 0.955308, 1.71955, 3.05233, 3.69891, 3.90278, 3.3261, 2.35332, 1.66014, 0.778227, 0.489304 }));
			ec.Add(new Evaporation("OH", 0.992589, new double[] { 0, 0.452024, 0.540565, 1.05317, 1.79412, 3.28533, 3.83871, 3.87366, 3.43095, 2.48147, 1.83489, 0.815507, 0.484644 }));
			ec.Add(new Evaporation("OK", 2.05042, new double[] { 0, 0.913368, 1.13705, 1.98518, 2.84728, 4.11831, 4.77654, 5.5804, 5.24255, 3.58241, 2.83097, 1.42597, 1.02521 }));
			ec.Add(new Evaporation("ON", 1.053, new double[] { 0, 0.493, 0.56, 1, 1.799, 3.193, 3.869, 4.083, 3.479, 2.462, 1.736, 0.814, 0.512 }));
			ec.Add(new Evaporation("OR", 1.09511, new double[] { 0, 0.447364, 0.647746, 1.11375, 1.61237, 2.93, 3.79793, 4.87557, 4.07754, 2.73777, 1.40384, 0.559205, 0.400763 }));
			ec.Add(new Evaporation("PA", 1.20229, new double[] { 0, 0.559205, 0.643086, 1.14637, 1.90596, 3.19795, 3.69308, 3.80958, 3.33775, 2.3941, 1.75334, 0.857447, 0.577845 }));
			ec.Add(new Evaporation("RI", 1.46791, new double[] { 0, 0.694346, 0.773567, 1.31879, 2.02712, 3.3494, 3.59406, 3.79211, 3.36105, 2.42322, 1.79994, 0.955308, 0.703666 }));
			ec.Add(new Evaporation("SC", 2.29274, new double[] { 0, 1.02055, 1.27219, 2.09236, 2.91252, 4.15909, 4.18821, 4.27559, 3.92026, 3.08145, 2.34167, 1.37471, 1.09511 }));
			ec.Add(new Evaporation("SD", 0.950648, new double[] { 0, 0.433384, 0.517264, 1.11375, 2.111, 3.73968, 4.48529, 5.17264, 4.69499, 3.11058, 2.27177, 0.862107, 0.507944 }));
			ec.Add(new Evaporation("TN", 1.63101, new double[] { 0, 0.717646, 0.913368, 1.67295, 2.50244, 3.83288, 3.97851, 4.06589, 3.76298, 2.95913, 2.16109, 1.04385, 0.768907 }));
			ec.Add(new Evaporation("TX", 3.01971, new double[] { 0, 1.39801, 1.62169, 2.64224, 3.36455, 5.15517, 5.88913, 6.31435, 5.7901, 4.25811, 3.37853, 1.82674, 1.42597 }));
			ec.Add(new Evaporation("UT", 1.33277, new double[] { 0, 0.531245, 0.801527, 1.64965, 2.50244, 5.00954, 6.15125, 7.77644, 6.52988, 4.43869, 2.63875, 0.932008, 0.470664 }));
			ec.Add(new Evaporation("VA", 1.80344, new double[] { 0, 0.834147, 0.969288, 1.66829, 2.44652, 3.68143, 4.01928, 4.01346, 3.56493, 2.73195, 1.97469, 1.17899, 0.848127 }));
			ec.Add(new Evaporation("VT", 0.736286, new double[] { 0, 0.316883, 0.419404, 0.754927, 1.42597, 2.65622, 3.29115, 3.47173, 3.01155, 1.83489, 1.28151, 0.563865, 0.340183 }));
			ec.Add(new Evaporation("WA", 1.08113, new double[] { 0, 0.442704, 0.638425, 1.06249, 1.57975, 2.93583, 3.43095, 4.22316, 3.43678, 2.27759, 1.25821, 0.540565, 0.368143 }));
			ec.Add(new Evaporation("WI", 0.796867, new double[] { 0, 0.340183, 0.456684, 0.894728, 1.78946, 3.23873, 3.85618, 4.07754, 3.37853, 2.17274, 1.61936, 0.633765, 0.354163 }));
			ec.Add(new Evaporation("WV", 1.22093, new double[] { 0, 0.549885, 0.671046, 1.21627, 1.7615, 2.85427, 2.93583, 2.75525, 2.62127, 2.05042, 1.46209, 0.694346, 0.563865 }));
			ec.Add(new Evaporation("WY", 1.54713, new double[] { 0, 0.736286, 0.810847, 1.34209, 2.12032, 3.77463, 4.58431, 5.59205, 5.14934, 3.35523, 2.4407, 0.978608, 0.768907 }));

			return (ec);
		}

		/*
		 * 
		 * Use esov3static
				GO

				SELECT TOP 1000 [State]
					  ,[Jan]
					  ,[Feb]
					  ,[Mar]
					  ,[Apr]
					  ,[May]
					  ,[Jun]
					  ,[Jul]
					  ,[Aug]
					  ,[Sep]
					  ,[Oct]
					  ,[Nov]
					  ,[Dec]
					  ,'ddc.Add(new Evaporation(' + CAST(dbo.Evaporation.State as varchar(30)) + ', ' + CAST((dbo.Evaporation.Jan + dbo.Evaporation.Feb) as varchar(30))
									  + ', new double[] { 0' 
									  + ',' +  CAST(dbo.Evaporation.Jan as varchar(30)) 
									  + ',' +  CAST(dbo.Evaporation.Feb as varchar(30)) 
									  + ',' +  CAST(dbo.Evaporation.Mar as varchar(30)) 
									  + ',' +  CAST(dbo.Evaporation.Apr as varchar(30)) 
									  + ',' +  CAST(dbo.Evaporation.May as varchar(30)) 
									  + ',' +  CAST(dbo.Evaporation.JUN as varchar(30)) 
									  + ',' +  CAST(dbo.Evaporation.Jul as varchar(30)) 
									  + ',' +  CAST(dbo.Evaporation.Aug as varchar(30)) 
									  + ',' +  CAST(dbo.Evaporation.Sep as varchar(30)) 
									  + ',' +  CAST(dbo.Evaporation.Oct as varchar(30)) 
									  + ',' +  CAST(dbo.Evaporation.Nov as varchar(30)) 
									  + ',' +  CAST(dbo.Evaporation.Dec as varchar(30)) 
									  + ' } ));'
				  FROM [esov3static].[dbo].[Evaporation]
		 */


		/// <summary>
		/// Temperature regions for access to several temperature and standard pressure data.
		/// </summary>
		/// <returns></returns>
		public static TemperatureRegionCollection CreateTemperatureRegionCollection()
		{
			// generate a EvaporationCollection
			var trc = new TemperatureRegionCollection();

			trc.Add(new TemperatureRegion(1, "AK", 55, 7, 69, 58, 14.683m));
			trc.Add(new TemperatureRegion(2, "AK", 55, 12, 65, 56, 14.636m));
			trc.Add(new TemperatureRegion(3, "AK", 55, 23, 57, 53, 14.688m));
			trc.Add(new TemperatureRegion(4, "AK", 55, -39, 73, 56, 13.877m));
			trc.Add(new TemperatureRegion(5, "AK", 55, -9, 68, 57, 14.626m));
			trc.Add(new TemperatureRegion(6, "AK", 55, -13, 66, 56, 14.645m));
			trc.Add(new TemperatureRegion(7, "AK", 55, -26, 65, 55, 14.683m));
			trc.Add(new TemperatureRegion(8, "AK", 55, -41, 77, 59, 14.457m));
			trc.Add(new TemperatureRegion(9, "AK", 55, -36, 52, 49, 14.688m));
			trc.Add(new TemperatureRegion(10, "AL", 70, 20, 92, 74, 14.357m));
			trc.Add(new TemperatureRegion(11, "AL", 70, 23, 92, 75, 14.364m));
			trc.Add(new TemperatureRegion(12, "AL", 70, 24, 94, 77, 14.605m));
			trc.Add(new TemperatureRegion(13, "AL", 70, 24, 93, 76, 14.374m));
			trc.Add(new TemperatureRegion(14, "AL", 70, 18, 93, 73, 14.3411m));
			trc.Add(new TemperatureRegion(15, "AL", 70, 27, 93, 76, 14.588m));
			trc.Add(new TemperatureRegion(16, "AL", 70, 32, 93, 76, 14.484m));
			trc.Add(new TemperatureRegion(17, "AL", 70, 30, 92, 76, 14.579m));
			trc.Add(new TemperatureRegion(18, "AR", 65, 13, 93, 75, 14.044m));
			trc.Add(new TemperatureRegion(19, "AR", 65, 18, 95, 77, 14.56m));
			trc.Add(new TemperatureRegion(20, "AR", 65, 18, 95, 77, 14.56m));
			trc.Add(new TemperatureRegion(21, "AR", 65, 19, 96, 76, 14.451m));
			trc.Add(new TemperatureRegion(22, "AR", 65, 21, 95, 77, 14.531m));
			trc.Add(new TemperatureRegion(23, "AR", 65, 21, 95, 77, 14.531m));
			trc.Add(new TemperatureRegion(24, "AR", 65, 25, 95, 77, 14.489m));
			trc.Add(new TemperatureRegion(25, "AR", 65, 18, 96, 76, 14.634m));
			trc.Add(new TemperatureRegion(26, "AR", 65, 16, 97, 77, 14.5662m));
			trc.Add(new TemperatureRegion(27, "AZ", 70, 27, 97, 63, 12.983m));
			trc.Add(new TemperatureRegion(28, "AZ", 70, 8, 83, 55, 11.335m));
			trc.Add(new TemperatureRegion(29, "AZ", 70, 20, 91, 60, 12.208m));
			trc.Add(new TemperatureRegion(30, "AZ", 70, 37, 108, 70, 14.118m));
			trc.Add(new TemperatureRegion(31, "AZ", 70, 44, 109, 72, 14.586m));
			trc.Add(new TemperatureRegion(32, "AZ", 70, 38, 107, 71, 14.126m));
			trc.Add(new TemperatureRegion(33, "AZ", 70, 34, 102, 65, 13.388m));
			trc.Add(new TemperatureRegion(34, "CA", 57, 31, 65, 59, 14.6754m));
			trc.Add(new TemperatureRegion(35, "CA", 57, 33, 97, 69, 14.683m));
			trc.Add(new TemperatureRegion(36, "CA", 57, 24, 81, 57, 12.097m));
			trc.Add(new TemperatureRegion(37, "CA", 60, 39, 78, 62, 14.687m));
			trc.Add(new TemperatureRegion(38, "CA", 60, 32, 97, 68, 14.681m));
			trc.Add(new TemperatureRegion(39, "CA", 60, 46, 81, 67, 14.68m));
			trc.Add(new TemperatureRegion(40, "CA", 63, 32, 105, 67, 13.701m));
			trc.Add(new TemperatureRegion(41, "CA", 63, 33, 97, 69, 14.683m));
			trc.Add(new TemperatureRegion(42, "CA", 63, 45, 81, 64, 14.64m));
			trc.Add(new TemperatureRegion(43, "CA", 63, 45, 81, 64, 14.64m));
			trc.Add(new TemperatureRegion(44, "CA", 63, 45, 81, 64, 14.64m));
			trc.Add(new TemperatureRegion(45, "CA", 63, 45, 81, 64, 14.64m));
			trc.Add(new TemperatureRegion(46, "CO", 60, 5, 94, 62, 12.355m));
			trc.Add(new TemperatureRegion(47, "CO", 60, -7, 86, 57, 11.539m));
			trc.Add(new TemperatureRegion(48, "CO", 60, -7, 93, 62, 12.696m));
			trc.Add(new TemperatureRegion(49, "CO", 60, 3, 90, 59, 12.076m));
			trc.Add(new TemperatureRegion(50, "CO", 60, -11, 82, 55, 11.108m));
			trc.Add(new TemperatureRegion(51, "CT", 55, -4, 85, 71, 14.2439m));
			trc.Add(new TemperatureRegion(52, "CT", 55, 6, 88, 72, 14.685m));
			trc.Add(new TemperatureRegion(53, "CT", 55, 12, 84, 72, 14.687m));
			trc.Add(new TemperatureRegion(54, "DC", 60, 20, 92, 76, 14.661m));
			trc.Add(new TemperatureRegion(55, "DE", 60, 14, 89, 74, 14.654m));
			trc.Add(new TemperatureRegion(56, "DE", 60, 18, 89, 75, 14.68m));
			trc.Add(new TemperatureRegion(57, "FL", 70, 28, 93, 76, 14.659m));
			trc.Add(new TemperatureRegion(58, "FL", 70, 33, 92, 77, 14.615m));
			trc.Add(new TemperatureRegion(59, "FL", 70, 42, 93, 76, 14.64m));
			trc.Add(new TemperatureRegion(60, "FL", 70, 40, 91, 77, 14.69m));
			trc.Add(new TemperatureRegion(61, "FL", 70, 47, 93, 77, 14.687m));
			trc.Add(new TemperatureRegion(62, "FL", 70, 50, 90, 77, 14.688m));
			trc.Add(new TemperatureRegion(63, "FL", 70, 58, 89, 79, 14.685m));
			trc.Add(new TemperatureRegion(64, "GA", 70, 21, 94, 74, 14.357m));
			trc.Add(new TemperatureRegion(65, "GA", 70, 20, 91, 74, 14.155m));
			trc.Add(new TemperatureRegion(66, "GA", 70, 24, 93, 77, 14.6699m));
			trc.Add(new TemperatureRegion(67, "GA", 70, 27, 94, 76, 14.572m));
			trc.Add(new TemperatureRegion(68, "GA", 70, 27, 94, 75, 14.505m));
			trc.Add(new TemperatureRegion(69, "GA", 70, 25, 94, 76, 14.617m));
			trc.Add(new TemperatureRegion(70, "GA", 70, 30, 95, 76, 14.593m));
			trc.Add(new TemperatureRegion(71, "GA", 70, 34, 94, 77, 14.572m));
			trc.Add(new TemperatureRegion(72, "GA", 70, 29, 93, 76, 14.669m));
			trc.Add(new TemperatureRegion(73, "HI", 70, 63, 88, 73, 14.687m));
			trc.Add(new TemperatureRegion(74, "IA", 55, -6, 90, 74, 14.119m));
			trc.Add(new TemperatureRegion(75, "IA", 55, -10, 88, 73, 14.062m));
			trc.Add(new TemperatureRegion(76, "IA", 55, -9, 88, 73, 14.234m));
			trc.Add(new TemperatureRegion(77, "IA", 55, -6, 90, 74, 14.119m));
			trc.Add(new TemperatureRegion(78, "IA", 55, -4, 90, 74, 14.19m));
			trc.Add(new TemperatureRegion(79, "IA", 55, -5, 89, 74, 14.24m));
			trc.Add(new TemperatureRegion(80, "IA", 55, -8, 91, 75, 14.051m));
			trc.Add(new TemperatureRegion(81, "IA", 55, 0, 92, 75, 14.251m));
			trc.Add(new TemperatureRegion(82, "IA", 55, 1, 91, 76, 14.328m));
			trc.Add(new TemperatureRegion(83, "ID", 55, -8, 86, 61, 13.1605m));
			trc.Add(new TemperatureRegion(84, "ID", 55, 15, 93, 64, 13.948m));
			trc.Add(new TemperatureRegion(85, "ID", 55, 15, 93, 64, 13.948m));
			trc.Add(new TemperatureRegion(86, "ID", 55, 5, 96, 62, 13.173m));
			trc.Add(new TemperatureRegion(87, "ID", 55, 9, 94, 63, 13.235m));
			trc.Add(new TemperatureRegion(88, "ID", 55, -3, 95, 61, 12.5969m));
			trc.Add(new TemperatureRegion(89, "ID", 55, 2, 90, 62, 12.621m));
			trc.Add(new TemperatureRegion(90, "ID", 55, -6, 89, 60, 12.346m));
			trc.Add(new TemperatureRegion(91, "ID", 55, 0, 90, 60, 12.468m));
			trc.Add(new TemperatureRegion(92, "ID", 55, 0, 90, 60, 12.468m));
			trc.Add(new TemperatureRegion(93, "IL", 60, -3, 90, 74, 14.383m));
			trc.Add(new TemperatureRegion(94, "IL", 60, -1, 88, 73, 14.342m));
			trc.Add(new TemperatureRegion(95, "IL", 60, 2, 91, 75, 14.292m));
			trc.Add(new TemperatureRegion(96, "IL", 60, 3, 91, 75, 14.337m));
			trc.Add(new TemperatureRegion(97, "IL", 60, -4, 90, 74, 14.3225m));
			trc.Add(new TemperatureRegion(98, "IL", 60, 2, 91, 75, 14.373m));
			trc.Add(new TemperatureRegion(99, "IL", 60, 3, 91, 75, 14.337m));
			trc.Add(new TemperatureRegion(100, "IL", 60, 10, 93, 77, 14.457m));
			trc.Add(new TemperatureRegion(101, "IL", 60, 0, 92, 75, 14.4379m));
			trc.Add(new TemperatureRegion(102, "IN", 60, 2, 88, 73, 14.262m));
			trc.Add(new TemperatureRegion(103, "IN", 60, 3, 87, 72, 14.289m));
			trc.Add(new TemperatureRegion(104, "IN", 60, 2, 88, 73, 14.262m));
			trc.Add(new TemperatureRegion(105, "IN", 60, 3, 90, 75, 14.376m));
			trc.Add(new TemperatureRegion(106, "IN", 60, 3, 88, 74, 14.272m));
			trc.Add(new TemperatureRegion(107, "IN", 60, -2, 90, 74, 14.0871m));
			trc.Add(new TemperatureRegion(108, "IN", 60, 9, 92, 76, 14.491m));
			trc.Add(new TemperatureRegion(109, "IN", 60, 0, 92, 75, 14.2418m));
			trc.Add(new TemperatureRegion(110, "IN", 60, 5, 93, 74, 14.4508m));
			trc.Add(new TemperatureRegion(111, "KS", 60, 2, 94, 66, 12.84m));
			trc.Add(new TemperatureRegion(112, "KS", 60, 4, 97, 73, 14.032m));
			trc.Add(new TemperatureRegion(113, "KS", 60, -2, 93, 76, 14.1901m));
			trc.Add(new TemperatureRegion(114, "KS", 60, 4, 97, 69, 13.224m));
			trc.Add(new TemperatureRegion(115, "KS", 60, 3, 96, 72, 13.732m));
			trc.Add(new TemperatureRegion(116, "KS", 60, 5, 96, 74, 14.138m));
			trc.Add(new TemperatureRegion(117, "KS", 60, 6, 97, 70, 13.37m));
			trc.Add(new TemperatureRegion(118, "KS", 60, 8, 97, 73, 13.998m));
			trc.Add(new TemperatureRegion(119, "KS", 60, 3, 97, 74, 14.1711m));
			trc.Add(new TemperatureRegion(120, "KY", 60, 13, 93, 76, 14.477m));
			trc.Add(new TemperatureRegion(121, "KY", 60, 14, 91, 75, 14.407m));
			trc.Add(new TemperatureRegion(122, "KY", 60, 10, 89, 73, 14.179m));
			trc.Add(new TemperatureRegion(123, "KY", 60, 14, 87, 73, 13.977m));
			trc.Add(new TemperatureRegion(124, "LA", 65, 26, 95, 77, 14.558m));
			trc.Add(new TemperatureRegion(125, "LA", 65, 27, 94, 78, 14.654m));
			trc.Add(new TemperatureRegion(126, "LA", 65, 27, 94, 78, 14.654m));
			trc.Add(new TemperatureRegion(127, "LA", 65, 30, 94, 76, 14.522m));
			trc.Add(new TemperatureRegion(128, "LA", 65, 30, 94, 78, 14.648m));
			trc.Add(new TemperatureRegion(129, "LA", 65, 30, 92, 77, 14.659m));
			trc.Add(new TemperatureRegion(130, "LA", 65, 32, 91, 78, 14.678m));
			trc.Add(new TemperatureRegion(131, "LA", 65, 32, 93, 78, 14.673m));
			trc.Add(new TemperatureRegion(132, "LA", 65, 34, 92, 78, 14.68m));
			trc.Add(new TemperatureRegion(133, "MA", 55, -8, 84, 70, 14.626m));
			trc.Add(new TemperatureRegion(134, "MA", 55, 5, 83, 69, 14.167m));
			trc.Add(new TemperatureRegion(135, "MA", 55, 12, 87, 71, 14.68m));
			trc.Add(new TemperatureRegion(136, "MD", 65, 18, 90, 76, 14.668m));
			trc.Add(new TemperatureRegion(137, "MD", 65, 18, 90, 76, 14.668m));
			trc.Add(new TemperatureRegion(138, "MD", 65, 15, 91, 74, 14.614m));
			trc.Add(new TemperatureRegion(139, "MD", 65, 15, 91, 74, 14.614m));
			trc.Add(new TemperatureRegion(140, "MD", 65, 15, 91, 74, 14.614m));
			trc.Add(new TemperatureRegion(141, "MD", 65, 8, 91, 75, 14.5272m));
			trc.Add(new TemperatureRegion(142, "MD", 65, 6, 89, 74, 14.272m));
			trc.Add(new TemperatureRegion(143, "MD", 65, 6, 89, 74, 14.272m));
			trc.Add(new TemperatureRegion(144, "ME", 55, -10, 82, 67, 14.367m));
			trc.Add(new TemperatureRegion(145, "ME", 55, 1, 84, 69, 14.51m));
			trc.Add(new TemperatureRegion(146, "ME", 55, 2, 83, 70, 14.662m));
			trc.Add(new TemperatureRegion(147, "MI", 55, -8, 82, 67, 13.955m));
			trc.Add(new TemperatureRegion(148, "MI", 55, -7, 80, 68, 14.314m));
			trc.Add(new TemperatureRegion(149, "MI", 55, 2, 86, 70, 14.367m));
			trc.Add(new TemperatureRegion(150, "MI", 55, -1, 84, 69, 14.332m));
			trc.Add(new TemperatureRegion(151, "MI", 55, 7, 83, 70, 14.362m));
			trc.Add(new TemperatureRegion(152, "MI", 55, 0, 87, 72, 14.2688m));
			trc.Add(new TemperatureRegion(153, "MI", 55, 4, 87, 72, 14.343m));
			trc.Add(new TemperatureRegion(154, "MI", 55, 5, 86, 71, 14.274m));
			trc.Add(new TemperatureRegion(155, "MI", 55, 2, 86, 72, 14.238m));
			trc.Add(new TemperatureRegion(156, "MI", 55, 5, 87, 72, 14.347m));
			trc.Add(new TemperatureRegion(157, "MN", 55, -31, 85, 69, 13.9579m));
			trc.Add(new TemperatureRegion(158, "MN", 55, -23, 83, 67, 14.077m));
			trc.Add(new TemperatureRegion(159, "MN", 55, -16, 81, 67, 13.958m));
			trc.Add(new TemperatureRegion(160, "MN", 55, -15, 86, 70, 13.955m));
			trc.Add(new TemperatureRegion(161, "MN", 55, -14, 88, 71, 14.16m));
			trc.Add(new TemperatureRegion(162, "MN", 55, -11, 88, 71, 14.257m));
			trc.Add(new TemperatureRegion(163, "MN", 55, -12, 88, 72, 14.16m));
			trc.Add(new TemperatureRegion(164, "MN", 55, -17, 88, 72, 14.159m));
			trc.Add(new TemperatureRegion(165, "MN", 55, -12, 85, 71, 14.008m));
			trc.Add(new TemperatureRegion(166, "MO", 60, 4, 93, 75, 14.16m));
			trc.Add(new TemperatureRegion(167, "MO", 60, 8, 93, 75, 14.398m));
			trc.Add(new TemperatureRegion(168, "MO", 60, 7, 93, 76, 14.24m));
			trc.Add(new TemperatureRegion(169, "MO", 60, 9, 92, 74, 14.034m));
			trc.Add(new TemperatureRegion(170, "MO", 60, 13, 94, 77, 14.515m));
			trc.Add(new TemperatureRegion(171, "MO", 60, 13, 92, 76, 14.443m));
			trc.Add(new TemperatureRegion(172, "MS", 65, 14, 94, 77, 14.6003m));
			trc.Add(new TemperatureRegion(173, "MS", 65, 25, 94, 76, 14.532m));
			trc.Add(new TemperatureRegion(174, "MS", 65, 22, 94, 76, 14.505m));
			trc.Add(new TemperatureRegion(175, "MS", 65, 24, 94, 78, 14.614m));
			trc.Add(new TemperatureRegion(176, "MS", 65, 25, 93, 76, 14.52m));
			trc.Add(new TemperatureRegion(177, "MS", 65, 25, 94, 76, 14.579m));
			trc.Add(new TemperatureRegion(178, "MS", 65, 23, 94, 78, 14.5911m));
			trc.Add(new TemperatureRegion(179, "MS", 65, 28, 92, 76, 14.477m));
			trc.Add(new TemperatureRegion(180, "MS", 65, 25, 94, 76, 14.532m));
			trc.Add(new TemperatureRegion(181, "MS", 65, 35, 91, 78, 14.678m));
			trc.Add(new TemperatureRegion(182, "MT", 55, -3, 86, 61, 13.184m));
			trc.Add(new TemperatureRegion(183, "MT", 55, -14, 84, 56, 11.98m));
			trc.Add(new TemperatureRegion(184, "MT", 55, -13, 88, 60, 12.854m));
			trc.Add(new TemperatureRegion(185, "MT", 55, -10, 87, 59, 12.74m));
			trc.Add(new TemperatureRegion(186, "MT", 55, -7, 90, 62, 12.896m));
			trc.Add(new TemperatureRegion(187, "MT", 55, -17, 90, 63, 13.516m));
			trc.Add(new TemperatureRegion(188, "MT", 55, -13, 93, 65, 13.352m));
			trc.Add(new TemperatureRegion(372, "NB", 55, -10, 79, 66, 14.629m));
			trc.Add(new TemperatureRegion(373, "NB", 55, -10, 76, 67, 14.557m));
			trc.Add(new TemperatureRegion(374, "NB", 55, -5, 80, 67, 14.572m));
			trc.Add(new TemperatureRegion(375, "NB", 55, -4, 75, 64, 14.507m));
			trc.Add(new TemperatureRegion(376, "NB", 55, -7, 83, 68, 14.661m));
			trc.Add(new TemperatureRegion(377, "NB", 55, -7, 83, 67, 14.639m));
			trc.Add(new TemperatureRegion(189, "NC", 65, 16, 85, 71, 13.58m));
			trc.Add(new TemperatureRegion(190, "NC", 65, 23, 89, 74, 14.187m));
			trc.Add(new TemperatureRegion(191, "NC", 65, 19, 90, 74, 14.231m));
			trc.Add(new TemperatureRegion(192, "NC", 65, 20, 90, 75, 14.463m));
			trc.Add(new TemperatureRegion(193, "NC", 65, 23, 91, 74, 14.292m));
			trc.Add(new TemperatureRegion(194, "NC", 65, 27, 94, 76, 14.567m));
			trc.Add(new TemperatureRegion(195, "NC", 65, 27, 94, 76, 14.638m));
			trc.Add(new TemperatureRegion(196, "NC", 65, 29, 86, 77, 14.69m));
			trc.Add(new TemperatureRegion(197, "ND", 55, -18, 92, 66, 13.711m));
			trc.Add(new TemperatureRegion(198, "ND", 55, -16, 88, 66, 13.807m));
			trc.Add(new TemperatureRegion(199, "ND", 55, -16, 88, 69, 14.217m));
			trc.Add(new TemperatureRegion(200, "ND", 55, -21, 90, 66, 13.3511m));
			trc.Add(new TemperatureRegion(201, "ND", 55, -22, 90, 69, 13.9046m));
			trc.Add(new TemperatureRegion(202, "ND", 55, -17, 88, 70, 14.224m));
			trc.Add(new TemperatureRegion(203, "ND", 55, -21, 90, 66, 13.3511m));
			trc.Add(new TemperatureRegion(204, "ND", 55, -16, 90, 67, 13.835m));
			trc.Add(new TemperatureRegion(205, "ND", 55, -22, 90, 69, 13.9046m));
			trc.Add(new TemperatureRegion(206, "NE", 60, -3, 92, 64, 12.712m));
			trc.Add(new TemperatureRegion(207, "NE", 60, -8, 94, 67, 13.367m));
			trc.Add(new TemperatureRegion(208, "NE", 60, -5, 92, 72, 13.89m));
			trc.Add(new TemperatureRegion(209, "NE", 60, -2, 93, 72, 13.736m));
			trc.Add(new TemperatureRegion(210, "NE", 60, -2, 92, 75, 14.182m));
			trc.Add(new TemperatureRegion(211, "NE", 60, -4, 92, 69, 13.275m));
			trc.Add(new TemperatureRegion(212, "NE", 60, -7, 94, 71, 13.6679m));
			trc.Add(new TemperatureRegion(213, "NE", 60, -5, 95, 74, 14.038m));
			trc.Add(new TemperatureRegion(214, "NH", 55, -14, 84, 69, 14.1033m));
			trc.Add(new TemperatureRegion(215, "NH", 55, -2, 87, 70, 14.513m));
			trc.Add(new TemperatureRegion(216, "NJ", 60, 14, 90, 73, 14.68m));
			trc.Add(new TemperatureRegion(217, "NJ", 60, 15, 90, 74, 14.624m));
			trc.Add(new TemperatureRegion(218, "NJ", 60, 13, 88, 73, 14.661m));
			trc.Add(new TemperatureRegion(219, "NM", 65, 13, 92, 60, 11.999m));
			trc.Add(new TemperatureRegion(220, "NM", 65, 6, 88, 61, 11.6266m));
			trc.Add(new TemperatureRegion(221, "NM", 65, 15, 95, 65, 12.661m));
			trc.Add(new TemperatureRegion(222, "NM", 65, -1, 88, 58, 11.5333m));
			trc.Add(new TemperatureRegion(223, "NM", 65, 18, 93, 60, 12.084m));
			trc.Add(new TemperatureRegion(224, "NM", 65, 18, 93, 60, 12.084m));
			trc.Add(new TemperatureRegion(225, "NM", 65, 23, 98, 66, 13.028m));
			trc.Add(new TemperatureRegion(226, "NM", 65, 23, 96, 63, 12.647m));
			trc.Add(new TemperatureRegion(227, "NV", 65, 13, 92, 60, 12.505m));
			trc.Add(new TemperatureRegion(228, "NV", 65, 1, 92, 59, 12.166m));
			trc.Add(new TemperatureRegion(229, "NV", 65, 13, 92, 57, 12.033m));
			trc.Add(new TemperatureRegion(230, "NV", 65, 30, 106, 66, 13.575m));
			trc.Add(new TemperatureRegion(231, "NY", 60, 3, 87, 71, 14.195m));
			trc.Add(new TemperatureRegion(232, "NY", 60, 2, 82, 69, 13.85m));
			trc.Add(new TemperatureRegion(233, "NY", 55, -6, 83, 70, 14.524m));
			trc.Add(new TemperatureRegion(234, "NY", 60, 15, 88, 72, 14.683m));
			trc.Add(new TemperatureRegion(235, "NY", 60, -2, 86, 70, 14.541m));
			trc.Add(new TemperatureRegion(236, "NY", 55, -12, 85, 71, 14.3124m));
			trc.Add(new TemperatureRegion(237, "NY", 55, -4, 83, 69, 14.57m));
			trc.Add(new TemperatureRegion(238, "NY", 55, -10, 84, 71, 14.583m));
			trc.Add(new TemperatureRegion(239, "NY", 55, 5, 86, 71, 14.403m));
			trc.Add(new TemperatureRegion(240, "NY", 55, 2, 85, 71, 14.481m));
			trc.Add(new TemperatureRegion(241, "OH", 60, 4, 87, 72, 14.27m));
			trc.Add(new TemperatureRegion(242, "OH", 60, -3, 88, 73, 14.3731m));
			trc.Add(new TemperatureRegion(243, "OH", 60, 6, 86, 72, 14.274m));
			trc.Add(new TemperatureRegion(244, "OH", 60, -1, 89, 73, 14.1338m));
			trc.Add(new TemperatureRegion(245, "OH", 60, 6, 88, 73, 14.267m));
			trc.Add(new TemperatureRegion(246, "OH", 60, 4, 85, 72, 14.02m));
			trc.Add(new TemperatureRegion(247, "OH", 60, 5, 85, 71, 14.05m));
			trc.Add(new TemperatureRegion(248, "OH", 60, 12, 90, 75, 14.441m));
			trc.Add(new TemperatureRegion(249, "OH", 60, 0, 92, 74, 14.3518m));
			trc.Add(new TemperatureRegion(250, "OH", 60, 9, 88, 73, 14.224m));
			trc.Add(new TemperatureRegion(251, "OK", 60, 6, 97, 73, 13.5611m));
			trc.Add(new TemperatureRegion(252, "OK", 60, 12, 98, 74, 14.015m));
			trc.Add(new TemperatureRegion(253, "OK", 60, 14, 97, 76, 14.34m));
			trc.Add(new TemperatureRegion(254, "OK", 60, 10, 98, 74, 14.1164m));
			trc.Add(new TemperatureRegion(255, "OK", 60, 15, 96, 74, 14.017m));
			trc.Add(new TemperatureRegion(256, "OK", 60, 17, 96, 76, 14.291m));
			trc.Add(new TemperatureRegion(257, "OK", 60, 19, 100, 73, 13.978m));
			trc.Add(new TemperatureRegion(258, "OK", 60, 10, 97, 74, 14.1532m));
			trc.Add(new TemperatureRegion(259, "OK", 60, 17, 96, 76, 14.291m));
			trc.Add(new TemperatureRegion(367, "ON", 55, 6, 84, 70, 14.397m));
			trc.Add(new TemperatureRegion(368, "ON", 55, -3, 83, 69, 14.498m));
			trc.Add(new TemperatureRegion(369, "ON", 55, -8, 78, 66, 14.06m));
			trc.Add(new TemperatureRegion(370, "ON", 55, -13, 80, 66, 14.352m));
			trc.Add(new TemperatureRegion(371, "ON", 55, 6, 86, 71, 14.367m));
			trc.Add(new TemperatureRegion(260, "OR", 60, 29, 72, 62, 14.683m));
			trc.Add(new TemperatureRegion(261, "OR", 60, 25, 87, 66, 14.589m));
			trc.Add(new TemperatureRegion(262, "OR", 60, 24, 95, 66, 14.003m));
			trc.Add(new TemperatureRegion(263, "OR", 60, 25, 87, 66, 14.589m));
			trc.Add(new TemperatureRegion(264, "OR", 60, 10, 87, 62, 12.649m));
			trc.Add(new TemperatureRegion(265, "OR", 60, 11, 93, 63, 13.918m));
			trc.Add(new TemperatureRegion(266, "OR", 60, 10, 87, 62, 12.649m));
			trc.Add(new TemperatureRegion(267, "OR", 60, 0, 84, 58, 12.666m));
			trc.Add(new TemperatureRegion(268, "OR", 60, -1, 89, 61, 12.9664m));
			trc.Add(new TemperatureRegion(269, "PA", 60, 7, 85, 70, 14.199m));
			trc.Add(new TemperatureRegion(270, "PA", 60, 10, 88, 72, 14.493m));
			trc.Add(new TemperatureRegion(271, "PA", 60, 15, 89, 74, 14.68m));
			trc.Add(new TemperatureRegion(272, "PA", 60, 13, 89, 73, 14.532m));
			trc.Add(new TemperatureRegion(273, "PA", 60, 7, 87, 71, 14.419m));
			trc.Add(new TemperatureRegion(274, "PA", 60, 7, 87, 71, 14.419m));
			trc.Add(new TemperatureRegion(275, "PA", 60, 5, 84, 69, 13.756m));
			trc.Add(new TemperatureRegion(276, "PA", 60, 10, 86, 70, 13.915m));
			trc.Add(new TemperatureRegion(277, "PA", 60, 7, 86, 70, 14.057m));
			trc.Add(new TemperatureRegion(278, "PA", 60, 7, 83, 70, 14.308m));
			trc.Add(new TemperatureRegion(279, "PR", 70, 67, 88, 80, 14.667m));
			trc.Add(new TemperatureRegion(280, "PR", 70, 67, 88, 80, 14.667m));
			trc.Add(new TemperatureRegion(281, "PR", 70, 67, 88, 80, 14.667m));
			trc.Add(new TemperatureRegion(282, "PR", 70, 67, 88, 80, 14.667m));
			trc.Add(new TemperatureRegion(283, "PR", 70, 67, 88, 80, 14.667m));
			trc.Add(new TemperatureRegion(284, "PR", 70, 67, 88, 80, 14.667m));
			trc.Add(new TemperatureRegion(285, "RI", 60, 10, 86, 71, 14.662m));
			trc.Add(new TemperatureRegion(286, "SC", 65, 23, 91, 74, 14.187m));
			trc.Add(new TemperatureRegion(287, "SC", 65, 23, 91, 74, 14.187m));
			trc.Add(new TemperatureRegion(288, "SC", 65, 19, 94, 74, 14.4427m));
			trc.Add(new TemperatureRegion(289, "SC", 65, 27, 94, 76, 14.617m));
			trc.Add(new TemperatureRegion(290, "SC", 65, 18, 93, 74, 14.3625m));
			trc.Add(new TemperatureRegion(291, "SC", 65, 24, 94, 75, 14.576m));
			trc.Add(new TemperatureRegion(292, "SC", 65, 28, 92, 77, 14.669m));
			trc.Add(new TemperatureRegion(293, "SD", 60, -5, 91, 65, 13.089m));
			trc.Add(new TemperatureRegion(294, "SD", 60, -9, 95, 69, 13.794m));
			trc.Add(new TemperatureRegion(295, "SD", 60, -19, 91, 72, 14.0062m));
			trc.Add(new TemperatureRegion(296, "SD", 60, -5, 91, 65, 13.089m));
			trc.Add(new TemperatureRegion(297, "SD", 60, -5, 91, 65, 13.089m));
			trc.Add(new TemperatureRegion(298, "SD", 60, -9, 95, 69, 13.794m));
			trc.Add(new TemperatureRegion(299, "SD", 60, -12, 91, 71, 14.024m));
			trc.Add(new TemperatureRegion(300, "SD", 60, -7, 94, 71, 13.795m));
			trc.Add(new TemperatureRegion(301, "SD", 60, -11, 90, 72, 13.953m));
			trc.Add(new TemperatureRegion(302, "TN", 65, 19, 90, 74, 14.182m));
			trc.Add(new TemperatureRegion(303, "TN", 65, 15, 87, 72, 13.724m));
			trc.Add(new TemperatureRegion(304, "TN", 65, 16, 92, 75, 14.385m));
			trc.Add(new TemperatureRegion(305, "TN", 65, 21, 94, 77, 14.544m));
			trc.Add(new TemperatureRegion(306, "TX", 70, 12, 94, 66, 12.879m));
			trc.Add(new TemperatureRegion(307, "TX", 70, 22, 97, 71, 13.769m));
			trc.Add(new TemperatureRegion(308, "TX", 70, 24, 98, 74, 14.381m));
			trc.Add(new TemperatureRegion(309, "TX", 70, 27, 95, 77, 14.543m));
			trc.Add(new TemperatureRegion(310, "TX", 70, 25, 98, 64, 12.731m));
			trc.Add(new TemperatureRegion(311, "TX", 70, 24, 97, 70, 13.709m));
			trc.Add(new TemperatureRegion(312, "TX", 70, 30, 96, 74, 14.369m));
			trc.Add(new TemperatureRegion(313, "TX", 70, 31, 94, 77, 14.638m));
			trc.Add(new TemperatureRegion(314, "TX", 70, 36, 101, 74, 14.427m));
			trc.Add(new TemperatureRegion(315, "TX", 70, 40, 94, 77, 14.685m));
			trc.Add(new TemperatureRegion(316, "UT", 63, 8, 91, 59, 11.945m));
			trc.Add(new TemperatureRegion(317, "UT", 63, 14, 101, 65, 13.1958m));
			trc.Add(new TemperatureRegion(318, "UT", 63, 11, 94, 62, 12.586m));
			trc.Add(new TemperatureRegion(319, "UT", 63, 8, 91, 59, 11.945m));
			trc.Add(new TemperatureRegion(320, "UT", 63, 11, 90, 60, 12.325m));
			trc.Add(new TemperatureRegion(321, "UT", 63, -5, 89, 60, 12.0789m));
			trc.Add(new TemperatureRegion(322, "UT", 63, 6, 98, 60, 12.6838m));
			trc.Add(new TemperatureRegion(323, "VA", 63, 24, 91, 77, 14.69m));
			trc.Add(new TemperatureRegion(324, "VA", 63, 18, 92, 75, 14.602m));
			trc.Add(new TemperatureRegion(325, "VA", 63, 17, 90, 74, 14.204m));
			trc.Add(new TemperatureRegion(326, "VA", 63, 14, 90, 74, 14.525m));
			trc.Add(new TemperatureRegion(327, "VA", 63, 17, 89, 72, 14.082m));
			trc.Add(new TemperatureRegion(328, "VA", 63, 17, 89, 72, 14.082m));
			trc.Add(new TemperatureRegion(329, "VT", 55, -6, 83, 68, 14.087m));
			trc.Add(new TemperatureRegion(330, "VT", 55, -6, 84, 69, 14.515m));
			trc.Add(new TemperatureRegion(331, "VT", 55, -13, 84, 70, 14.3625m));
			trc.Add(new TemperatureRegion(332, "WA", 52, 27, 74, 61, 14.588m));
			trc.Add(new TemperatureRegion(333, "WA", 52, 21, 76, 64, 14.612m));
			trc.Add(new TemperatureRegion(334, "WA", 52, 28, 81, 64, 14.458m));
			trc.Add(new TemperatureRegion(335, "WA", 52, 19, 85, 67, 14.6906m));
			trc.Add(new TemperatureRegion(336, "WA", 52, 10, 74, 56, 12.708m));
			trc.Add(new TemperatureRegion(337, "WA", 52, 2, 91, 64, 13.7796m));
			trc.Add(new TemperatureRegion(338, "WA", 52, 12, 96, 65, 14.311m));
			trc.Add(new TemperatureRegion(339, "WA", 52, 11, 92, 64, 14.138m));
			trc.Add(new TemperatureRegion(340, "WA", 52, 7, 89, 61, 13.435m));
			trc.Add(new TemperatureRegion(341, "WA", 52, 12, 95, 65, 14.067m));
			trc.Add(new TemperatureRegion(342, "WI", 55, -21, 82, 68, 14.3465m));
			trc.Add(new TemperatureRegion(343, "WI", 55, -9, 85, 70, 14.069m));
			trc.Add(new TemperatureRegion(344, "WI", 55, -15, 84, 71, 14.3705m));
			trc.Add(new TemperatureRegion(345, "WI", 55, -13, 87, 71, 14.221m));
			trc.Add(new TemperatureRegion(346, "WI", 55, -15, 89, 73, 14.1196m));
			trc.Add(new TemperatureRegion(347, "WI", 55, -8, 85, 72, 14.326m));
			trc.Add(new TemperatureRegion(348, "WI", 55, -8, 88, 73, 14.347m));
			trc.Add(new TemperatureRegion(349, "WI", 55, -6, 87, 72, 14.241m));
			trc.Add(new TemperatureRegion(350, "WI", 55, -2, 86, 72, 14.332m));
			trc.Add(new TemperatureRegion(351, "WV", 63, 11, 88, 72, 14.245m));
			trc.Add(new TemperatureRegion(352, "WV", 63, 11, 87, 71, 14.045m));
			trc.Add(new TemperatureRegion(353, "WV", 63, 11, 88, 73, 14.182m));
			trc.Add(new TemperatureRegion(354, "WV", 63, 5, 83, 70, 13.665m));
			trc.Add(new TemperatureRegion(355, "WV", 63, 12, 83, 69, 13.24m));
			trc.Add(new TemperatureRegion(356, "WV", 63, 14, 91, 73, 14.402m));
			trc.Add(new TemperatureRegion(357, "WY", 63, -7, 87, 58, 12.184m));
			trc.Add(new TemperatureRegion(358, "WY", 63, -7, 87, 58, 12.184m));
			trc.Add(new TemperatureRegion(359, "WY", 63, -2, 84, 54, 11.444m));
			trc.Add(new TemperatureRegion(360, "WY", 63, -7, 87, 58, 12.184m));
			trc.Add(new TemperatureRegion(361, "WY", 63, -8, 90, 61, 12.708m));
			trc.Add(new TemperatureRegion(362, "WY", 63, -7, 91, 61, 12.675m));
			trc.Add(new TemperatureRegion(363, "WY", 63, -5, 89, 58, 12.096m));
			trc.Add(new TemperatureRegion(364, "WY", 63, 0, 85, 57, 11.714m));
			trc.Add(new TemperatureRegion(365, "WY", 63, -7, 87, 58, 11.974m));
			trc.Add(new TemperatureRegion(366, "WY", 63, -14, 81, 56, 11.2196m));


			return (trc);
		}

		/*
			use esov3static
			go

			SELECT [TemperatureRegion]
				  ,[State]
				  ,[GroundWaterTemp]
				  ,[HeatDesignTemp]
				  ,[CoolDesignTemp]
				  ,[CoolDesignWetBulb]
				  ,[StandardPressure]
				  ,'trc.Add(new TemperatureRegion(' + CAST(dbo.TemperatureRegions.TemperatureRegion as varchar(30)) 
				  + ',"' + CAST(dbo.TemperatureRegions.State as varchar(30)) 
				  + '", ' + CAST(dbo.TemperatureRegions.GroundWaterTemp as varchar(30))
				  + ', ' + CAST(dbo.TemperatureRegions.HeatDesignTemp as varchar(30))
				  + ', ' + CAST(dbo.TemperatureRegions.CoolDesignTemp as varchar(30))
				  + ', ' + CAST(dbo.TemperatureRegions.CoolDesignWetBulb as varchar(30))
				  + ', ' + CAST(dbo.TemperatureRegions.StandardPressure as varchar(30))
				  + 'm));'
			  FROM [esov3static].[dbo].[TemperatureRegions]
		 * */


		/// <summary>
		/// Solar collection for summer/winter, directional, hourly
		/// </summary>
		/// <returns></returns>
		public static SolarCollection CreateSolarCollection()
		{
			// generate a SolarCollection; double {season,direction,hours] ; 0|1, n|e|s|w, 0|1|...23 
			var sc = new SolarCollection();

			sc.Add(new Solar(1, new double[,,] {
			{ 
				{ .25, .25, .25, .75, .75, .75, 4.75, 4.75, 4.75, 8, 8, 8, 8.75, 8.75, 8.75, 6, 6, 6, 1.75, 1.75, 1.75, 0.75, 0.75, 0.75}, 
				{ .5,  .5, .5, 3.75, 3.75, 3.75, 37.75, 37.75, 37.75, 28,28,28, 13.75, 13.75, 13.75, 7.5,7.5,7.5, 2.5,2.5,2.5, 1, 1, 1 }, 
				{ .5,  .5, .5, .5,.5,.5, 6.25,6.25,6.25, 20.5,20.5,20.5, 22,22,22, 9.75,9.75,9.75, 3,3,3, 1, 1, 1 },
				{ 1.5,1.5,1.5, .75, .75, .75, 4.75, 4.75, 4.75, 8.25,8.25,8.25, 26.5,26.5,26.5, 39,39,39, 10,10,10, 3.5,3.5,3.5}
			},
			{ 
				{ .25, .25, .25, .75, .75, .75, 4.75, 4.75, 4.75, 8, 8, 8, 8.75, 8.75, 8.75, 6, 6, 6, 1.75, 1.75, 1.75, 0.75, 0.75, 0.75}, 
				{ .5,  .5, .5, 3.75, 3.75, 3.75, 37.75, 37.75, 37.75, 28,28,28, 13.75, 13.75, 13.75, 7.5,7.5,7.5, 2.5,2.5,2.5, 1, 1, 1 }, 
				{ .5,  .5, .5, .5,.5,.5, 6.25,6.25,6.25, 20.5,20.5,20.5, 22,22,22, 9.75,9.75,9.75, 3,3,3, 1, 1, 1 },
				{ 1.5,1.5,1.5, .75, .75, .75, 4.75, 4.75, 4.75, 8.25,8.25,8.25, 26.5,26.5,26.5, 39,39,39, 10,10,10, 3.5,3.5,3.5}
			}        
			}));

			sc.Add(new Solar(2, new double[,,] {
			{ 
				{ .25, .25, .25, .75, .75, .75, 4.75, 4.75, 4.75, 8, 8, 8, 8.75, 8.75, 8.75, 6, 6, 6, 1.75, 1.75, 1.75, 0.75, 0.75, 0.75}, 
				{ .5,  .5, .5, 3.75, 3.75, 3.75, 37.75, 37.75, 37.75, 28,28,28, 13.75, 13.75, 13.75, 7.5,7.5,7.5, 2.5,2.5,2.5, 1, 1, 1 }, 
				{ .5,  .5, .5, .5,.5,.5, 6.25,6.25,6.25, 20.5,20.5,20.5, 22,22,22, 9.75,9.75,9.75, 3,3,3, 1, 1, 1 },
				{ 1.5,1.5,1.5, .75, .75, .75, 4.75, 4.75, 4.75, 8.25,8.25,8.25, 26.5,26.5,26.5, 39,39,39, 10,10,10, 3.5,3.5,3.5}
			},
			{ 
				{ .25, .25, .25, .75, .75, .75, 4.75, 4.75, 4.75, 8, 8, 8, 8.75, 8.75, 8.75, 6, 6, 6, 1.75, 1.75, 1.75, 0.75, 0.75, 0.75}, 
				{ .5,  .5, .5, 3.75, 3.75, 3.75, 37.75, 37.75, 37.75, 28,28,28, 13.75, 13.75, 13.75, 7.5,7.5,7.5, 2.5,2.5,2.5, 1, 1, 1 }, 
				{ .5,  .5, .5, .5,.5,.5, 6.25,6.25,6.25, 20.5,20.5,20.5, 22,22,22, 9.75,9.75,9.75, 3,3,3, 1, 1, 1 },
				{ 1.5,1.5,1.5, .75, .75, .75, 4.75, 4.75, 4.75, 8.25,8.25,8.25, 26.5,26.5,26.5, 39,39,39, 10,10,10, 3.5,3.5,3.5}
			}        
			}));

			sc.Add(new Solar(3, new double[,,] {
			{ 
				{ .25, .25, .25, .75, .75, .75, 4.75, 4.75, 4.75, 8, 8, 8, 8.75, 8.75, 8.75, 6, 6, 6, 1.75, 1.75, 1.75, 0.75, 0.75, 0.75}, 
				{ .5,  .5, .5, 3.75, 3.75, 3.75, 37.75, 37.75, 37.75, 28,28,28, 13.75, 13.75, 13.75, 7.5,7.5,7.5, 2.5,2.5,2.5, 1, 1, 1 }, 
				{ .5,  .5, .5, .5,.5,.5, 6.25,6.25,6.25, 20.5,20.5,20.5, 22,22,22, 9.75,9.75,9.75, 3,3,3, 1, 1, 1 },
				{ 1.5,1.5,1.5, .75, .75, .75, 4.75, 4.75, 4.75, 8.25,8.25,8.25, 26.5,26.5,26.5, 39,39,39, 10,10,10, 3.5,3.5,3.5}
			},
			{ 
				{ .25, .25, .25, .75, .75, .75, 4.75, 4.75, 4.75, 8, 8, 8, 8.75, 8.75, 8.75, 6, 6, 6, 1.75, 1.75, 1.75, 0.75, 0.75, 0.75}, 
				{ .5,  .5, .5, 3.75, 3.75, 3.75, 37.75, 37.75, 37.75, 28,28,28, 13.75, 13.75, 13.75, 7.5,7.5,7.5, 2.5,2.5,2.5, 1, 1, 1 }, 
				{ .5,  .5, .5, .5,.5,.5, 6.25,6.25,6.25, 20.5,20.5,20.5, 22,22,22, 9.75,9.75,9.75, 3,3,3, 1, 1, 1 },
				{ 1.5,1.5,1.5, .75, .75, .75, 4.75, 4.75, 4.75, 8.25,8.25,8.25, 26.5,26.5,26.5, 39,39,39, 10,10,10, 3.5,3.5,3.5}
			}        
			}));

			sc.Add(new Solar(4, new double[,,] {
			{ 
				{ .25, .25, .25, .75, .75, .75, 4.75, 4.75, 4.75, 8, 8, 8, 8.75, 8.75, 8.75, 6, 6, 6, 1.75, 1.75, 1.75, 0.75, 0.75, 0.75}, 
				{ .5,  .5, .5, 3.75, 3.75, 3.75, 37.75, 37.75, 37.75, 28,28,28, 13.75, 13.75, 13.75, 7.5,7.5,7.5, 2.5,2.5,2.5, 1, 1, 1 }, 
				{ .5,  .5, .5, .5,.5,.5, 6.25,6.25,6.25, 20.5,20.5,20.5, 22,22,22, 9.75,9.75,9.75, 3,3,3, 1, 1, 1 },
				{ 1.5,1.5,1.5, .75, .75, .75, 4.75, 4.75, 4.75, 8.25,8.25,8.25, 26.5,26.5,26.5, 39,39,39, 10,10,10, 3.5,3.5,3.5}
			},
			{ 
				{ .25, .25, .25, .75, .75, .75, 4.75, 4.75, 4.75, 8, 8, 8, 8.75, 8.75, 8.75, 6, 6, 6, 1.75, 1.75, 1.75, 0.75, 0.75, 0.75}, 
				{ .5,  .5, .5, 3.75, 3.75, 3.75, 37.75, 37.75, 37.75, 28,28,28, 13.75, 13.75, 13.75, 7.5,7.5,7.5, 2.5,2.5,2.5, 1, 1, 1 }, 
				{ .5,  .5, .5, .5,.5,.5, 6.25,6.25,6.25, 20.5,20.5,20.5, 22,22,22, 9.75,9.75,9.75, 3,3,3, 1, 1, 1 },
				{ 1.5,1.5,1.5, .75, .75, .75, 4.75, 4.75, 4.75, 8.25,8.25,8.25, 26.5,26.5,26.5, 39,39,39, 10,10,10, 3.5,3.5,3.5}
			}        
			}));

			sc.Add(new Solar(5, new double[,,] {
			{ 
				{ .25, .25, .25, .75, .75, .75, 4.75, 4.75, 4.75, 8, 8, 8, 8.75, 8.75, 8.75, 6, 6, 6, 1.75, 1.75, 1.75, 0.75, 0.75, 0.75}, 
				{ .5,  .5, .5, 3.75, 3.75, 3.75, 37.75, 37.75, 37.75, 28,28,28, 13.75, 13.75, 13.75, 7.5,7.5,7.5, 2.5,2.5,2.5, 1, 1, 1 }, 
				{ .5,  .5, .5, .5,.5,.5, 6.25,6.25,6.25, 20.5,20.5,20.5, 22,22,22, 9.75,9.75,9.75, 3,3,3, 1, 1, 1 },
				{ 1.5,1.5,1.5, .75, .75, .75, 4.75, 4.75, 4.75, 8.25,8.25,8.25, 26.5,26.5,26.5, 39,39,39, 10,10,10, 3.5,3.5,3.5}
			},
			{ 
				{ .25, .25, .25, .75, .75, .75, 4.75, 4.75, 4.75, 8, 8, 8, 8.75, 8.75, 8.75, 6, 6, 6, 1.75, 1.75, 1.75, 0.75, 0.75, 0.75}, 
				{ .5,  .5, .5, 3.75, 3.75, 3.75, 37.75, 37.75, 37.75, 28,28,28, 13.75, 13.75, 13.75, 7.5,7.5,7.5, 2.5,2.5,2.5, 1, 1, 1 }, 
				{ .5,  .5, .5, .5,.5,.5, 6.25,6.25,6.25, 20.5,20.5,20.5, 22,22,22, 9.75,9.75,9.75, 3,3,3, 1, 1, 1 },
				{ 1.5,1.5,1.5, .75, .75, .75, 4.75, 4.75, 4.75, 8.25,8.25,8.25, 26.5,26.5,26.5, 39,39,39, 10,10,10, 3.5,3.5,3.5}
			}        
			}));

			sc.Add(new Solar(6, new double[,,] {
			{ 
				{ .25, .25, .25, .75, .75, .75, 4.75, 4.75, 4.75, 8, 8, 8, 8.75, 8.75, 8.75, 6, 6, 6, 1.75, 1.75, 1.75, 0.75, 0.75, 0.75}, 
				{ .5,  .5, .5, 3.75, 3.75, 3.75, 37.75, 37.75, 37.75, 28,28,28, 13.75, 13.75, 13.75, 7.5,7.5,7.5, 2.5,2.5,2.5, 1, 1, 1 }, 
				{ .5,  .5, .5, .5,.5,.5, 6.25,6.25,6.25, 20.5,20.5,20.5, 22,22,22, 9.75,9.75,9.75, 3,3,3, 1, 1, 1 },
				{ 1.5,1.5,1.5, .75, .75, .75, 4.75, 4.75, 4.75, 8.25,8.25,8.25, 26.5,26.5,26.5, 39,39,39, 10,10,10, 3.5,3.5,3.5}
			},
			{ 
				{ .25, .25, .25, .75, .75, .75, 4.75, 4.75, 4.75, 8, 8, 8, 8.75, 8.75, 8.75, 6, 6, 6, 1.75, 1.75, 1.75, 0.75, 0.75, 0.75}, 
				{ .5,  .5, .5, 3.75, 3.75, 3.75, 37.75, 37.75, 37.75, 28,28,28, 13.75, 13.75, 13.75, 7.5,7.5,7.5, 2.5,2.5,2.5, 1, 1, 1 }, 
				{ .5,  .5, .5, .5,.5,.5, 6.25,6.25,6.25, 20.5,20.5,20.5, 22,22,22, 9.75,9.75,9.75, 3,3,3, 1, 1, 1 },
				{ 1.5,1.5,1.5, .75, .75, .75, 4.75, 4.75, 4.75, 8.25,8.25,8.25, 26.5,26.5,26.5, 39,39,39, 10,10,10, 3.5,3.5,3.5}
			}        
			}));

			sc.Add(new Solar(7, new double[,,] {
			{ 
				{ .25, .25, .25, .75, .75, .75, 4.75, 4.75, 4.75, 8, 8, 8, 8.75, 8.75, 8.75, 6, 6, 6, 1.75, 1.75, 1.75, 0.75, 0.75, 0.75}, 
				{ .5,  .5, .5, 3.75, 3.75, 3.75, 37.75, 37.75, 37.75, 28,28,28, 13.75, 13.75, 13.75, 7.5,7.5,7.5, 2.5,2.5,2.5, 1, 1, 1 }, 
				{ .5,  .5, .5, .5,.5,.5, 6.25,6.25,6.25, 20.5,20.5,20.5, 22,22,22, 9.75,9.75,9.75, 3,3,3, 1, 1, 1 },
				{ 1.5,1.5,1.5, .75, .75, .75, 4.75, 4.75, 4.75, 8.25,8.25,8.25, 26.5,26.5,26.5, 39,39,39, 10,10,10, 3.5,3.5,3.5}
			},
			{ 
				{ .25, .25, .25, .75, .75, .75, 4.75, 4.75, 4.75, 8, 8, 8, 8.75, 8.75, 8.75, 6, 6, 6, 1.75, 1.75, 1.75, 0.75, 0.75, 0.75}, 
				{ .5,  .5, .5, 3.75, 3.75, 3.75, 37.75, 37.75, 37.75, 28,28,28, 13.75, 13.75, 13.75, 7.5,7.5,7.5, 2.5,2.5,2.5, 1, 1, 1 }, 
				{ .5,  .5, .5, .5,.5,.5, 6.25,6.25,6.25, 20.5,20.5,20.5, 22,22,22, 9.75,9.75,9.75, 3,3,3, 1, 1, 1 },
				{ 1.5,1.5,1.5, .75, .75, .75, 4.75, 4.75, 4.75, 8.25,8.25,8.25, 26.5,26.5,26.5, 39,39,39, 10,10,10, 3.5,3.5,3.5}
			}        
			}));

			sc.Add(new Solar(8, new double[,,] {
			{ 
				{ .25, .25, .25, .75, .75, .75, 4.75, 4.75, 4.75, 8, 8, 8, 8.75, 8.75, 8.75, 6, 6, 6, 1.75, 1.75, 1.75, 0.75, 0.75, 0.75}, 
				{ .5,  .5, .5, 3.75, 3.75, 3.75, 37.75, 37.75, 37.75, 28,28,28, 13.75, 13.75, 13.75, 7.5,7.5,7.5, 2.5,2.5,2.5, 1, 1, 1 }, 
				{ .5,  .5, .5, .5,.5,.5, 6.25,6.25,6.25, 20.5,20.5,20.5, 22,22,22, 9.75,9.75,9.75, 3,3,3, 1, 1, 1 },
				{ 1.5,1.5,1.5, .75, .75, .75, 4.75, 4.75, 4.75, 8.25,8.25,8.25, 26.5,26.5,26.5, 39,39,39, 10,10,10, 3.5,3.5,3.5}
			},
			{ 
				{ .25, .25, .25, .75, .75, .75, 4.75, 4.75, 4.75, 8, 8, 8, 8.75, 8.75, 8.75, 6, 6, 6, 1.75, 1.75, 1.75, 0.75, 0.75, 0.75}, 
				{ .5,  .5, .5, 3.75, 3.75, 3.75, 37.75, 37.75, 37.75, 28,28,28, 13.75, 13.75, 13.75, 7.5,7.5,7.5, 2.5,2.5,2.5, 1, 1, 1 }, 
				{ .5,  .5, .5, .5,.5,.5, 6.25,6.25,6.25, 20.5,20.5,20.5, 22,22,22, 9.75,9.75,9.75, 3,3,3, 1, 1, 1 },
				{ 1.5,1.5,1.5, .75, .75, .75, 4.75, 4.75, 4.75, 8.25,8.25,8.25, 26.5,26.5,26.5, 39,39,39, 10,10,10, 3.5,3.5,3.5}
			}        
			}));

			sc.Add(new Solar(9, new double[,,] {
			{ 
				{ .25, .25, .25, .75, .75, .75, 4.75, 4.75, 4.75, 8, 8, 8, 8.75, 8.75, 8.75, 6, 6, 6, 1.75, 1.75, 1.75, 0.75, 0.75, 0.75}, 
				{ .5,  .5, .5, 3.75, 3.75, 3.75, 37.75, 37.75, 37.75, 28,28,28, 13.75, 13.75, 13.75, 7.5,7.5,7.5, 2.5,2.5,2.5, 1, 1, 1 }, 
				{ .5,  .5, .5, .5,.5,.5, 6.25,6.25,6.25, 20.5,20.5,20.5, 22,22,22, 9.75,9.75,9.75, 3,3,3, 1, 1, 1 },
				{ 1.5,1.5,1.5, .75, .75, .75, 4.75, 4.75, 4.75, 8.25,8.25,8.25, 26.5,26.5,26.5, 39,39,39, 10,10,10, 3.5,3.5,3.5}
			},
			{ 
				{ .25, .25, .25, .75, .75, .75, 4.75, 4.75, 4.75, 8, 8, 8, 8.75, 8.75, 8.75, 6, 6, 6, 1.75, 1.75, 1.75, 0.75, 0.75, 0.75}, 
				{ .5,  .5, .5, 3.75, 3.75, 3.75, 37.75, 37.75, 37.75, 28,28,28, 13.75, 13.75, 13.75, 7.5,7.5,7.5, 2.5,2.5,2.5, 1, 1, 1 }, 
				{ .5,  .5, .5, .5,.5,.5, 6.25,6.25,6.25, 20.5,20.5,20.5, 22,22,22, 9.75,9.75,9.75, 3,3,3, 1, 1, 1 },
				{ 1.5,1.5,1.5, .75, .75, .75, 4.75, 4.75, 4.75, 8.25,8.25,8.25, 26.5,26.5,26.5, 39,39,39, 10,10,10, 3.5,3.5,3.5}
			}        
			}));

			sc.Add(new Solar(10, new double[,,] {
			{ 
				{ .25, .25, .25, .75, .75, .75, 4.75, 4.75, 4.75, 8, 8, 8, 8.75, 8.75, 8.75, 6, 6, 6, 1.75, 1.75, 1.75, 0.75, 0.75, 0.75}, 
				{ .5,  .5, .5, 3.75, 3.75, 3.75, 37.75, 37.75, 37.75, 28,28,28, 13.75, 13.75, 13.75, 7.5,7.5,7.5, 2.5,2.5,2.5, 1, 1, 1 }, 
				{ .5,  .5, .5, .5,.5,.5, 6.25,6.25,6.25, 20.5,20.5,20.5, 22,22,22, 9.75,9.75,9.75, 3,3,3, 1, 1, 1 },
				{ 1.5,1.5,1.5, .75, .75, .75, 4.75, 4.75, 4.75, 8.25,8.25,8.25, 26.5,26.5,26.5, 39,39,39, 10,10,10, 3.5,3.5,3.5}
			},
			{ 
				{ .25, .25, .25, .75, .75, .75, 4.75, 4.75, 4.75, 8, 8, 8, 8.75, 8.75, 8.75, 6, 6, 6, 1.75, 1.75, 1.75, 0.75, 0.75, 0.75}, 
				{ .5,  .5, .5, 3.75, 3.75, 3.75, 37.75, 37.75, 37.75, 28,28,28, 13.75, 13.75, 13.75, 7.5,7.5,7.5, 2.5,2.5,2.5, 1, 1, 1 }, 
				{ .5,  .5, .5, .5,.5,.5, 6.25,6.25,6.25, 20.5,20.5,20.5, 22,22,22, 9.75,9.75,9.75, 3,3,3, 1, 1, 1 },
				{ 1.5,1.5,1.5, .75, .75, .75, 4.75, 4.75, 4.75, 8.25,8.25,8.25, 26.5,26.5,26.5, 39,39,39, 10,10,10, 3.5,3.5,3.5}
			}        
			}));

			return (sc);
		}


		/// <summary>
		/// Temperature Profile
		/// </summary>
		/// <returns></returns>
		public static TemperatureProfileCollection CreateTemperatureProfileCollection()
		{
			var tpc = new TemperatureProfileCollection();

			for (var i = 1; i <= 377; i++)
			{

				tpc.Add(new TemperatureProfile(i, new decimal[,,] {
				{ 
					{ 51m,51m,51m,51m,49m,49m,49m,54m,54m,54m,57m,57m,57m,56m,56m,56m,54m,54m,54m,51m,51m,51m,51m,51m}, 
					{ 51m,51m,51m,51m,49m,49m,49m,54m,54m,54m,57m,57m,57m,56m,56m,56m,54m,54m,54m,51m,51m,51m,51m,51m}, 
					{ 51m,51m,51m,51m,49m,49m,49m,54m,54m,54m,57m,57m,57m,56m,56m,56m,54m,54m,54m,51m,51m,51m,51m,51m}, 
					{ 51m,51m,51m,51m,49m,49m,49m,54m,54m,54m,57m,57m,57m,56m,56m,56m,54m,54m,54m,51m,51m,51m,51m,51m}, 
					{ 51m,51m,51m,51m,49m,49m,49m,54m,54m,54m,57m,57m,57m,56m,56m,56m,54m,54m,54m,51m,51m,51m,51m,51m}, 
					{ 51m,51m,51m,51m,49m,49m,49m,54m,54m,54m,57m,57m,57m,56m,56m,56m,54m,54m,54m,51m,51m,51m,51m,51m}, 
					{ 51m,51m,51m,51m,49m,49m,49m,54m,54m,54m,57m,57m,57m,56m,56m,56m,54m,54m,54m,51m,51m,51m,51m,51m}, 
					{ 51m,51m,51m,51m,49m,49m,49m,54m,54m,54m,57m,57m,57m,56m,56m,56m,54m,54m,54m,51m,51m,51m,51m,51m},
					{ 51m,51m,51m,51m,49m,49m,49m,54m,54m,54m,57m,57m,57m,56m,56m,56m,54m,54m,54m,51m,51m,51m,51m,51m}, 
					{ 51m,51m,51m,51m,49m,49m,49m,54m,54m,54m,57m,57m,57m,56m,56m,56m,54m,54m,54m,51m,51m,51m,51m,51m}, 
					{ 51m,51m,51m,51m,49m,49m,49m,54m,54m,54m,57m,57m,57m,56m,56m,56m,54m,54m,54m,51m,51m,51m,51m,51m}, 
					{ 51m,51m,51m,51m,49m,49m,49m,54m,54m,54m,57m,57m,57m,56m,56m,56m,54m,54m,54m,51m,51m,51m,51m,51m}
				},
				{ 
					{ 39.7m,39.7m,39.7m,40.7m,40.7m,40.7m,45.3m,45.3m,45.3m,48.1m,48.1m,48.1m,48.5m,48.5m,48.5m,45.6m,45.6m,45.6m,43.1m,43.1m,43.1m,41.2m,41.2m,41.2m}, 
					{ 39.7m,39.7m,39.7m,40.7m,40.7m,40.7m,45.3m,45.3m,45.3m,48.1m,48.1m,48.1m,48.5m,48.5m,48.5m,45.6m,45.6m,45.6m,43.1m,43.1m,43.1m,41.2m,41.2m,41.2m}, 
					{ 39.7m,39.7m,39.7m,40.7m,40.7m,40.7m,45.3m,45.3m,45.3m,48.1m,48.1m,48.1m,48.5m,48.5m,48.5m,45.6m,45.6m,45.6m,43.1m,43.1m,43.1m,41.2m,41.2m,41.2m}, 
					{ 39.7m,39.7m,39.7m,40.7m,40.7m,40.7m,45.3m,45.3m,45.3m,48.1m,48.1m,48.1m,48.5m,48.5m,48.5m,45.6m,45.6m,45.6m,43.1m,43.1m,43.1m,41.2m,41.2m,41.2m}, 
					{ 39.7m,39.7m,39.7m,40.7m,40.7m,40.7m,45.3m,45.3m,45.3m,48.1m,48.1m,48.1m,48.5m,48.5m,48.5m,45.6m,45.6m,45.6m,43.1m,43.1m,43.1m,41.2m,41.2m,41.2m}, 
					{ 39.7m,39.7m,39.7m,40.7m,40.7m,40.7m,45.3m,45.3m,45.3m,48.1m,48.1m,48.1m,48.5m,48.5m,48.5m,45.6m,45.6m,45.6m,43.1m,43.1m,43.1m,41.2m,41.2m,41.2m}, 
					{ 39.7m,39.7m,39.7m,40.7m,40.7m,40.7m,45.3m,45.3m,45.3m,48.1m,48.1m,48.1m,48.5m,48.5m,48.5m,45.6m,45.6m,45.6m,43.1m,43.1m,43.1m,41.2m,41.2m,41.2m}, 
					{ 39.7m,39.7m,39.7m,40.7m,40.7m,40.7m,45.3m,45.3m,45.3m,48.1m,48.1m,48.1m,48.5m,48.5m,48.5m,45.6m,45.6m,45.6m,43.1m,43.1m,43.1m,41.2m,41.2m,41.2m}, 
					{ 39.7m,39.7m,39.7m,40.7m,40.7m,40.7m,45.3m,45.3m,45.3m,48.1m,48.1m,48.1m,48.5m,48.5m,48.5m,45.6m,45.6m,45.6m,43.1m,43.1m,43.1m,41.2m,41.2m,41.2m}, 
					{ 39.7m,39.7m,39.7m,40.7m,40.7m,40.7m,45.3m,45.3m,45.3m,48.1m,48.1m,48.1m,48.5m,48.5m,48.5m,45.6m,45.6m,45.6m,43.1m,43.1m,43.1m,41.2m,41.2m,41.2m}, 
					{ 39.7m,39.7m,39.7m,40.7m,40.7m,40.7m,45.3m,45.3m,45.3m,48.1m,48.1m,48.1m,48.5m,48.5m,48.5m,45.6m,45.6m,45.6m,43.1m,43.1m,43.1m,41.2m,41.2m,41.2m}, 
					{ 39.7m,39.7m,39.7m,40.7m,40.7m,40.7m,45.3m,45.3m,45.3m,48.1m,48.1m,48.1m,48.5m,48.5m,48.5m,45.6m,45.6m,45.6m,43.1m,43.1m,43.1m,41.2m,41.2m,41.2m}   
				}
				}));

			}



			return (tpc);
		}



	}
}
