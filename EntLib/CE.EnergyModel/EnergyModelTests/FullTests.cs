﻿using CE.EnergyModel;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace EnergyModelTests
{
    [TestClass()]
    public class FullTests
    {
        [TestInitialize]
        public void TestInit()
        {
            // This initializes the automapper!
            new CE.ContentModel.AutoMapperBootStrapper().BootStrap();
        }

        /// <summary>
        /// This test creates a manager with minimal arguments.  Config and weather grabbed from content.
        /// </summary>
        [TestMethod]
        public void RAW_Content_AtticFan_Test()
        {
            var clientId = 256;
            var factory = new EnergyModelManagerFactory();

            // Create Manager with config and weather data
            var mgr = factory.CreateEnergyModelManager(clientId);

            // Appliance collection
            var appliances = new ApplianceCollection();

            //Appliances - all data passed-in directly; these typically come from client appliances, but CAN be provided directly
            var applianceName = "AtticFan";
            var electricExpression = "[P:atticfan.count] * [F:atticfan.fankwh] * [P:atticfan.usage] * ([D:CDD]/125)";
            var appliance = new Appliance() { Name = applianceName };
            var energyUse = new EnergyUse(CE.EnergyModel.Enums.CommodityType.Electric, CE.EnergyModel.Enums.UnitOfMeasureType.kWh, new EvaluationContext() { EvaluationType = CE.EnergyModel.Enums.EvaluationType.Pending, Expression = electricExpression });
            appliance.EnergyUses.Add(energyUse);
            appliances.Add(appliance);

            //Profile Attributes - all data passed-in directly, as if it were populated by overlaying from multiple data sources internally
            var attributes = new ProfileAttributeCollection();
            attributes.Add(new ProfileAttribute() { Key = "atticfan.count", OptionKey = "atticfan.count.value", OptionValue = "0.04" });
            attributes.Add(new ProfileAttribute() { Key = "atticfan.usage", OptionKey = "atticfan.usage.often", OptionValue = "4" });

            // use this for overload call below;
            var regionParams = new RegionParams() { ApplianceRegionId = 1, TemperatureRegionId = 31, SolarRegionId = 1, State = "AZ" };

            // Execute Model
            var result = mgr.ExecuteEnergyModel(regionParams, null, attributes, appliances);

        }


        [TestMethod]
        public void DB_Aquarium_Test()
        {
            var clientId = 87;
            var factory = new EnergyModelManagerFactory();
            var config = new EnergyModelConfiguration(Utility.CreateCalculationFactors());

            // Create Manager with config and weather data
            var mgr = factory.CreateEnergyModelManager(clientId, config);

            // Appliance collection
            var appliances = new ApplianceCollection();

            //Appliances - all data passed-in directly; these typically come from client appliances, but CAN be provided directly
            var applianceName = "Aquarium";
            var electricExpression = "([P:aquarium.count] * 1.0 ) + [F:aquarium.energyuse]";
            var appliance = new Appliance() { Name = applianceName };
            var energyUse = new EnergyUse(CE.EnergyModel.Enums.CommodityType.Electric, CE.EnergyModel.Enums.UnitOfMeasureType.kWh, new EvaluationContext() { EvaluationType = CE.EnergyModel.Enums.EvaluationType.Pending, Expression = electricExpression });
            appliance.EnergyUses.Add(energyUse);
            appliances.Add(appliance);

            //Profile Attributes - all data passed-in directly, as if it were populated by overlaying from multiple data sources internally
            var attributes = new ProfileAttributeCollection();
            attributes.Add(new ProfileAttribute() { Key = "aquarium.count", OptionKey = "aquarium.count.value", OptionValue = "1" });

            // use this for overload call below;
            var regionParams = new RegionParams() { ApplianceRegionId = 1, TemperatureRegionId = 31, SolarRegionId = 1, State = "AZ" };

            // Execute Model
            var result = mgr.ExecuteEnergyModel(regionParams, null, attributes, appliances);

        }


        [TestMethod]
        public void DB_Aquarium_Minimal()
        {
            var clientId = 87;
            var factory = new EnergyModelManagerFactory();
            var config = new EnergyModelConfiguration(Utility.CreateCalculationFactors());

            // Create Manager with config and weather data
            var mgr = factory.CreateEnergyModelManager(clientId, config);

            // Appliance collection
            var appliances = new ApplianceCollection();

            //Appliances - all data passed-in directly; these typically come from client appliances, but CAN be provided directly
            var applianceName = "Aquarium";
            var appliance = new Appliance() { Name = applianceName };
            appliances.Add(appliance);

            //Profile Attributes - all data passed-in directly, as if it were populated by overlaying from multiple data sources internally
            var attributes = new ProfileAttributeCollection();
            attributes.Add(new ProfileAttribute() { Key = "aquarium.count", OptionKey = "aquarium.count.value", OptionValue = "1" });

            // use this for overload call below;
            var regionParams = new RegionParams() { ApplianceRegionId = 1, TemperatureRegionId = 31, SolarRegionId = 1, State = "AZ" };

            // Execute Model
            var result = mgr.ExecuteEnergyModel(regionParams, null, attributes, appliances);

        }

        [TestMethod]
        public void WaterBed_Test_B()
        {
            var clientId = 87;
            var factory = new EnergyModelManagerFactory();
            var mgr = factory.CreateEnergyModelManager(clientId);

            // Appliance collection
            var appliances = new ApplianceCollection();

            //Appliances - all data passed-in directly; these typically come from client appliances, but CAN be provided directly
            var applianceName = "WaterBed";
            var appliance = new Appliance() { Name = applianceName };
            appliances.Add(appliance);
            appliances.IsStandAlone = true;

            //Profile Attributes - all data passed-in directly, as if it were populated by overlaying from multiple data sources internally
            var pas = new ProfileAttributeCollection();
            pas.Add(new ProfileAttribute() { Key = "waterbed.count", OptionKey = "waterbed.count.value", OptionValue = "1" });
            pas.Add(new ProfileAttribute() { Key = "waterbed.size", OptionKey = "waterbed.size.queen", OptionValue = "1.2" });
            pas.Add(new ProfileAttribute() { Key = "waterbed.softsided", OptionKey = "waterbed.softsided.value", OptionValue = "1" });
            pas.Add(new ProfileAttribute() { Key = "waterbed.makethebed", OptionKey = "waterbed.makethebed.value", OptionValue = "1" });
            pas.Add(new ProfileAttribute() { Key = "waterbed.monthsheated", OptionKey = "waterbed.monthsheated.value", OptionValue = "8" });
            pas.Add(new ProfileAttribute() { Key = "waterbed.sideinsulated", OptionKey = "waterbed.sideinsulated.yes", OptionValue = "1" });
            pas.Add(new ProfileAttribute() { Key = "waterbed.temperature", OptionKey = "waterbed.temperature.value", OptionValue = "85" });
            pas.IsStandAlone = false;

            // use this for overload call below;
            var regionParams = new RegionParams() { ApplianceRegionId = 1, TemperatureRegionId = 31, SolarRegionId = 1, State = "AZ" };

            // Execute Model
            var result = mgr.ExecuteEnergyModel(regionParams, null, pas, appliances);

        }


        [TestMethod()]
        public void FullModel_Zipcode_Test()
        {
            var clientId = 87;
            var zipcode = "02062";
            var factory = new EnergyModelManagerFactory();

            // Create Manager
            var mgr = factory.CreateEnergyModelManager(clientId);

            // Execute Model
            var result = mgr.ExecuteEnergyModel(zipcode);
        }

        [TestMethod()]
        public void FullModel_Zipcode_IdParams_Test()
        {
            var clientId = 87;
            var zipcode = "02062";
            var idParams = new IDParams()
            {
                CustomerId = "something",
                AccountId = "something",
                PremiseId = "something",
                Zipcode = zipcode
            };

            var factory = new EnergyModelManagerFactory();

            // Create Manager
            var mgr = factory.CreateEnergyModelManager(clientId);

            // Execute Model
            var result = mgr.ExecuteEnergyModel(idParams);
        }

    }

}
