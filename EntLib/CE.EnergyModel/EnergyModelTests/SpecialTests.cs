﻿using CE.EnergyModel;
using CE.EnergyModel.Entities;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace EnergyModelTests
{
    [TestClass]
    public class SpecialTests
    {

        [TestInitialize]
        public void TestInit()
        {
            // This initializes the automapper!
            new CE.ContentModel.AutoMapperBootStrapper().BootStrap();
        }

        /// <summary>
        /// Provide own appliance to model.
        /// </summary>
        [TestMethod]
        public void UserSpecified_Tivo_Test()
        {
            var clientId = 87;
            var factory = new EnergyModelManagerFactory();
            var config = new EnergyModelConfiguration(Utility.CreateCalculationFactors());
            var weather = new Weather(Utility.CreateDegreeDayCollection(), Utility.CreateEvaporationCollection(), Utility.CreateTemperatureRegionCollection(), Utility.CreateSolarCollection(), Utility.CreateTemperatureProfileCollection());

            // Create Manager with config and weather data
            var mgr = factory.CreateEnergyModelManager(clientId, config, weather);

            // Appliance collection
            var appliances = new ApplianceCollection();

            //Appliances - all data passed-in directly; these typically come from client appliances, this example passes a direct usage for special appliance
            var applianceName = "Tivo";
            var electricExpression = "";
            var appliance = new Appliance() { Name = applianceName };
            var energyUse = new EnergyUse(CE.EnergyModel.Enums.CommodityType.Electric, new EvaluationContext() { EvaluationType = CE.EnergyModel.Enums.EvaluationType.Provided, Expression = electricExpression });
            energyUse.Quantity = 220m;  //already calculated and passed through here!
            appliance.EnergyUses.Add(energyUse);
            appliances.Add(appliance);

            // use this for overload call below;
            var regionParams = new RegionParams() { ApplianceRegionId = 1, TemperatureRegionId = 20, SolarRegionId = 1, State = "AZ" };

            // Execute Model
            var result = mgr.ExecuteEnergyModel(regionParams, null, null, appliances);
        }




    }
}
