﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace CE.EnergyModel
{
    public class PeriodCollection : Collection<Period>
    {

        public PeriodCollection()
            : base(new List<Period>())
        {

        }

        public PeriodCollection(int periodCount)
            : base(new List<Period>())
        {
            for (var i = 0; i < periodCount; i++)
            {
                List<Period> items = (List<Period>)Items;
                var p = new Period()
                {
                    Number = i + 1,
                    Name = "x",
                    Quantity = 0m
                };
                items.Add(p);
            }
        }

        public Period Find(Predicate<Period> match)
        {
            List<Period> items = (List<Period>)Items;
            return (items.Find(match));
        }

        public bool Exists(Predicate<Period> match)
        {
            List<Period> items = (List<Period>)Items;
            return (items.Exists(match));
        }

        public void AddRange(PeriodCollection periods)
        {
            List<Period> items = (List<Period>)Items;
            items.AddRange(periods);
        }


    }

}
