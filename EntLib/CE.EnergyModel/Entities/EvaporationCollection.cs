﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace CE.EnergyModel.Entities
{
    public class EvaporationCollection : Collection<Evaporation>
    {

        public EvaporationCollection()
            : base(new List<Evaporation>())
        {

        }

        public Evaporation Find(Predicate<Evaporation> match)
        {
            List<Evaporation> items = (List<Evaporation>)Items;
            return (items.Find(match));
        }

        public bool Exists(Predicate<Evaporation> match)
        {
            List<Evaporation> items = (List<Evaporation>)Items;
            return (items.Exists(match));
        }

    }

}