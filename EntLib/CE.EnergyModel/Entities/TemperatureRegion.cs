﻿
namespace CE.EnergyModel.Entities
{
    public class TemperatureRegion
    {
        public int TemperatureRegionId { get; set; }
        public string State { get; set; }
        public int GroundWaterTemp { get; set; }
        public int HeatDesignTemp { get; set; }
        public int CoolDesignTemp { get; set; }
        public int CoolDesignWetBulb { get; set; }
        public decimal StandardPressure { get; set; }

        public TemperatureRegion()
        {
        }

        public TemperatureRegion(int temperatureRegionId, string state, int groundWaterTemp, int heatDesignTemp, int coolDesignTemp, int coolDesignWetBulb, decimal standardPressure)
            : this()
        {
            TemperatureRegionId = temperatureRegionId;
            State = state;
            GroundWaterTemp = groundWaterTemp;
            HeatDesignTemp = heatDesignTemp;
            CoolDesignTemp = coolDesignTemp;
            CoolDesignWetBulb = coolDesignWetBulb;
            StandardPressure = standardPressure;
        }

    }

}

