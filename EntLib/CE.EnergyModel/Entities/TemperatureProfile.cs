﻿namespace CE.EnergyModel.Entities
{
    public class TemperatureProfile
    {
        public int TemperatureRegionId { get; set; }
        public decimal[, ,] Factor { get; set; }     // matrix[regular or wet,month,hour]

        public TemperatureProfile()
        {
            Factor = new decimal[2, 12, 24];
        }

        public TemperatureProfile(int temperatureRegionId)
            : this()
        {
            TemperatureRegionId = temperatureRegionId;
        }

        public TemperatureProfile(int temperatureRegionId, decimal[, ,] arrayFactor)
            : this(temperatureRegionId)
        {

            if (arrayFactor != null)
            {
                for (int i = 0; i < arrayFactor.GetLength(0); i++)
                {
                    for (int j = 0; j < arrayFactor.GetLength(1); j++)
                    {
                        for (int k = 0; k < arrayFactor.GetLength(2); k++)
                        {
                            Factor[i, j, k] = arrayFactor[i, j, k];
                        }

                    }
                }
            }

        }

    }
}