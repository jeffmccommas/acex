﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace CE.EnergyModel.Entities
{
    public class TemperatureRegionCollection : Collection<TemperatureRegion>
    {

        public TemperatureRegionCollection()
            : base(new List<TemperatureRegion>(400))
        {

        }

        public TemperatureRegion Find(Predicate<TemperatureRegion> match)
        {
            List<TemperatureRegion> items = (List<TemperatureRegion>)Items;
            return (items.Find(match));
        }

        public bool Exists(Predicate<TemperatureRegion> match)
        {
            List<TemperatureRegion> items = (List<TemperatureRegion>)Items;
            return (items.Exists(match));
        }

    }

}