﻿
namespace CE.EnergyModel.Entities
{
    public class DegreeDays
    {
        public int TemperatureRegionId { get; set; }
        public int TotalHDD { get; set; }
        public int TotalCDD { get; set; }
        public int[] MonthlyHDD { get; set; }
        public int[] MonthlyCDD { get; set; }

        public DegreeDays()
        {
            MonthlyHDD = new int[13];   //index using 1 through 12 to match month numbers
            MonthlyCDD = new int[13];   //index using 1 through 12 to match month numbers
        }

        public DegreeDays(int temperatureRegionId, int annualHDD, int annualCDD)
            : this()
        {
            TemperatureRegionId = temperatureRegionId;
            TotalHDD = annualHDD;
            TotalCDD = annualCDD;
        }

        public DegreeDays(int temperatureRegion, int annualHDD, int annualCDD, int[] arrayHDD, int[] arrayCDD)
            : this(temperatureRegion, annualHDD, annualCDD)
        {

            if (arrayHDD != null && arrayHDD.Length >= 0 && arrayHDD.Length <= 13)
            {
                for (int i = 0; i < arrayHDD.Length; i++)
                {
                    MonthlyHDD[i] = arrayHDD[i];
                }
            }

            if (arrayCDD != null && arrayCDD.Length >= 0 && arrayCDD.Length <= 13)
            {
                for (int i = 0; i < arrayCDD.Length; i++)
                {
                    MonthlyCDD[i] = arrayCDD[i];
                }
            }
        }

    }
}
