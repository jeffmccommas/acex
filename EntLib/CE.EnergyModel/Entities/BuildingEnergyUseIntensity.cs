﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CE.EnergyModel.Entities
{
    public class BuildingEnergyUseIntensity
    {
        public int CategoryId { get; set; }     //select with this
        public int SizeId { get; set; }         //select with this
        public int CommodityId { get; set; }    //select with this 
        public int ClimateId { get; set; }     //select with this 
        public string CategoryName { get; set; }
        public decimal EuiHeating { get; set; }
        public decimal EuiCooling { get; set; }
        public decimal EuiVentilation { get; set; }
        public decimal EuiWaterHeating { get; set; }
        public decimal EuiLighting { get; set; }
        public decimal EuiCooking { get; set; }
        public decimal EuiRefrigeration { get; set; }
        public decimal EuiOfficeEquipment { get; set; }
        public decimal EuiComputers { get; set; }
        public decimal EuiMiscellaneous { get; set; }
        public decimal EuiLaundry { get; set; }

        public BuildingEnergyUseIntensity()
        {

        }

        // to allow the retrieval of properties by name for callers
        public object this[string propertyName]
        {
            get { return this.GetType().GetProperty(propertyName).GetValue(this, null); }
            set { this.GetType().GetProperty(propertyName).SetValue(this, value, null); }
        }

    }

}
