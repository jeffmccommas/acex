﻿namespace CE.EnergyModel.Entities
{
    public class Evaporation
    {
        public string State { get; set; }
        public double TotalEvap { get; set; }
        public double[] MonthlyEvap { get; set; }

        public Evaporation()
        {
            MonthlyEvap = new double[13];   //index using 1 through 12 to match month numbers
        }

        public Evaporation(string state, double annualEvap)
            : this()
        {
            State = state;
            TotalEvap = annualEvap;
        }

        public Evaporation(string state, double annualEvap, double[] arrayEvap)
            : this(state, annualEvap)
        {

            if (arrayEvap != null && arrayEvap.Length >= 0 && arrayEvap.Length <= 13)
            {
                for (int i = 0; i < arrayEvap.Length; i++)
                {
                    MonthlyEvap[i] = arrayEvap[i];
                }
            }

        }

    }
}
