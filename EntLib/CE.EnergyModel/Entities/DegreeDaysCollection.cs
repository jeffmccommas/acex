﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace CE.EnergyModel.Entities
{
    public class DegreeDaysCollection : Collection<DegreeDays>
    {

        public DegreeDaysCollection()
            : base(new List<DegreeDays>())
        {

        }

        public DegreeDays Find(Predicate<DegreeDays> match)
        {
            List<DegreeDays> items = (List<DegreeDays>)Items;
            return (items.Find(match));
        }

        public bool Exists(Predicate<DegreeDays> match)
        {
            List<DegreeDays> items = (List<DegreeDays>)Items;
            return (items.Exists(match));
        }

    }

}
