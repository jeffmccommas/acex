﻿
namespace CE.EnergyModel.Entities
{
    public class Weather
    {
        public DegreeDaysCollection AnnualDegreeDaysCollection { get; set; }
        public EvaporationCollection AnnualEvaporationCollection { get; set; }
        public TemperatureRegionCollection TemperatureRegionCollection { get; set; }
        public SolarCollection SolarCollection { get; set; }
        public TemperatureProfileCollection TemperatureProfileCollection { get; set; }

        public Weather()
        {

        }

        public Weather(DegreeDaysCollection degreeDaysCollection,
                        EvaporationCollection evaporationCollection,
                        TemperatureRegionCollection temperatureRegionCollection,
                        SolarCollection solarCollection,
                        TemperatureProfileCollection temperatureProfileCollection)
        {
            AnnualDegreeDaysCollection = degreeDaysCollection;
            AnnualEvaporationCollection = evaporationCollection;
            TemperatureRegionCollection = temperatureRegionCollection;
            SolarCollection = solarCollection;
            TemperatureProfileCollection = temperatureProfileCollection;
        }


    }
}
