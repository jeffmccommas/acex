﻿namespace CE.EnergyModel.Entities
{
    /// <summary>
    /// Actual Weather totals.
    /// </summary>
    public class ActualWeather
    {
        public int TotalHDD { get; set; }
        public int TotalCDD { get; set; }
    }
}
