﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace CE.EnergyModel.Entities
{
    public class TemperatureProfileCollection : Collection<TemperatureProfile>
    {

        public TemperatureProfileCollection()
            : base(new List<TemperatureProfile>(400))
        {

        }

        public TemperatureProfile Find(Predicate<TemperatureProfile> match)
        {
            List<TemperatureProfile> items = (List<TemperatureProfile>)Items;
            return (items.Find(match));
        }

        public bool Exists(Predicate<TemperatureProfile> match)
        {
            List<TemperatureProfile> items = (List<TemperatureProfile>)Items;
            return (items.Exists(match));
        }

    }

}