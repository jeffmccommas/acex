﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace CE.EnergyModel.Entities
{
    public class SolarCollection : Collection<Solar>
    {

        public SolarCollection()
            : base(new List<Solar>())
        {

        }

        public Solar Find(Predicate<Solar> match)
        {
            List<Solar> items = (List<Solar>)Items;
            return (items.Find(match));
        }

        public bool Exists(Predicate<Solar> match)
        {
            List<Solar> items = (List<Solar>)Items;
            return (items.Exists(match));
        }

    }

}