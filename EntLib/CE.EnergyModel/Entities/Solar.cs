﻿namespace CE.EnergyModel.Entities
{
    public class Solar
    {
        public int SolarRegionId { get; set; }
        public double[, ,] Factor { get; set; }     // matrix[season,direction,hour]

        public Solar()
        {
            Factor = new double[2, 4, 24];   //season,direction,hour
        }

        public Solar(int solarRegionId)
            : this()
        {
            SolarRegionId = solarRegionId;
        }

        public Solar(int solarRegionId, double[, ,] arrayFactor)
            : this(solarRegionId)
        {

            if (arrayFactor != null)
            {
                // 3 dimensions of length 0,1,2
                for (int i = 0; i < arrayFactor.GetLength(0); i++)
                {
                    for (int j = 0; j < arrayFactor.GetLength(1); j++)
                    {
                        for (int k = 0; k < arrayFactor.GetLength(2); k++)
                        {
                            Factor[i, j, k] = arrayFactor[i, j, k];
                        }

                    }
                }
            }

        }

    }
}