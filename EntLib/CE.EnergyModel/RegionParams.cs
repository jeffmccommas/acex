﻿
namespace CE.EnergyModel
{
    public class RegionParams
    {
        private const string _defaultProfileName = "aclara";

        public int ApplianceRegionId { get; set; }          //Were/are 24 appliance regions.  Maps to a DefaultProfileName (aka. ProfileCollectionName) as well.

        public int TemperatureRegionId { get; set; }        //Provided or gets populated. 

        public int SolarRegionId { get; set; }              //Provided or gets populated. 

        public string State { get; set; }                   //State or Province

        public string StationId { get; set; }               // For daily weather data lookup, used for disaggregation

        public int ClimateId { get; set; }

        public  string CountryCode { get; set; }

        public string DefaultProfileName { get; set; }      //Mapped by ApplianceRegionId

        public RegionParams()
        {
            DefaultProfileName = _defaultProfileName;
            ClimateId = 0;
        }

    }
}
