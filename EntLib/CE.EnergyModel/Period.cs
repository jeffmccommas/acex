﻿using System;

namespace CE.EnergyModel
{
    /// <summary>
    /// Period.  Typically would be period of a month.
    /// </summary>
    public class Period
    {
        public int Number { get; set; }
        public string Name { get; set; }
        public decimal Quantity { get; set; }
        public decimal Cost { get; set; }
        public DateTime? Start { get; set; }
        public DateTime? End { get; set; }

        public Period()
        {
        }

        public Period Clone()
        {
            var p = new Period();

            p.Number = this.Number;
            p.Name = this.Name;
            p.Quantity = this.Quantity;
            p.Cost = this.Cost;
            p.Start = this.Start;
            p.End = this.End;

            return (p);
        }

    }
}
