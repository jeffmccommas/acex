﻿
namespace CE.EnergyModel.Extensions
{

    public static class EnergyModelExtensions
    {
        //This is for any extension methods that may be needed.
        //Since we are using collections derived from Collection<T> and instantiated with base class List<T>, we will not need to extend List<T>
    }
}
