﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CE.EnergyModel
{
    /// <summary>
    /// Small class to save strings as part of a quick local trace.
    /// </summary>
    public class QuickTrace
    {
        private List<String> _traceData = new List<string>();

        /// <summary>
        /// Get the list of strings that may have been logged/traced.
        /// </summary>
        public List<String> TraceData
        {
            get { return _traceData; }
        }

        /// <summary>
        /// Save a message in the list of strings. (quick trace it!)
        /// </summary>
        /// <param name="msg"></param>
        /// <param name="title"></param>
        public void TraceMessage(string msg, string title)
        {
            StringBuilder sb = new StringBuilder();

            sb.Append("[" + DateTime.Now.ToString() + "] ");
            sb.Append("[" + title + "] ");
            sb.Append(msg);
            _traceData.Add(sb.ToString());
        }

        /// <summary>
        /// Clear trace data.
        /// </summary>
        public void ClearTraceData()
        {
            _traceData.Clear();
        }

        /// <summary>
        /// Output trace to console.
        /// </summary>
        public void OutputTrace()
        {
            Console.WriteLine("===== OUTPUT TRACE DATA =====\n");
            foreach (string s in _traceData)
            {
                Console.WriteLine(s + "\n");
            }
        }

    }
}
