﻿
namespace CE.EnergyModel
{
    /// <summary>
    /// Appliance.
    /// </summary>
    public class Appliance
    {
        public string Name { get; set; }
        public string Key { get; set; }
        public EnergyUseCollection EnergyUses { get; set; }
        public int Confidence { get; set; }

        public Appliance()
        {
            EnergyUses = new EnergyUseCollection();
        }

        public Appliance(string name)
            : this()
        {
            Name = name;
        }

        public Appliance(string name, string key)
            : this()
        {
            Name = name;
            Key = key;
        }

        public bool HasFailedEnergyUseContext()
        {
            bool ret = false;

            foreach (var e in this.EnergyUses)
            {
                if (e.EvaluationContext != null && e.EvaluationContext.EvaluationType == Enums.EvaluationType.Failed)
                {
                    ret = true;
                    break;
                }
            }

            return (ret);
        }

        public bool HasNonZeroEnergyUse()
        {
            bool ret = false;

            foreach (var e in this.EnergyUses)
            {
                if (e.Quantity != 0m)
                {
                    ret = true;
                    break;
                }
            }

            return (ret);
        }


        public Appliance Clone()
        {
            var a = new Appliance();

            a.Name = this.Name;
            a.Key = this.Key;
            a.Confidence = this.Confidence;

            foreach (var e in this.EnergyUses)
            {
                var newEnegyUse = e.Clone();
                a.EnergyUses.Add(newEnegyUse);
            }

            return (a);
        }


    }


}
