﻿
namespace CE.EnergyModel
{
    public class CalcAppliancesResult
    {
        public ApplianceCollection Appliances { get; set; }

        public CalcAppliancesResult()
        {
            Appliances = new ApplianceCollection();
        }
    }
}
