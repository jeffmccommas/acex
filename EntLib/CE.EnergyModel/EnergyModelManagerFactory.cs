﻿using CE.EnergyModel.Entities;

namespace CE.EnergyModel
{
    public class EnergyModelManagerFactory
    {

        /// <summary>
        /// Create EnergyModelManager that implements IEnergyModel.
        /// </summary>
        /// <returns></returns>
        public IEnergyModel CreateEnergyModelManager(int clientId)
        {
            return (new EnergyModelManager(clientId));
        }

        /// <summary>
        /// Create EnergyModelManager that implements IEnergyModel, some callers need to provide connection strings directly.
        /// </summary>
        /// <returns></returns>
        public IEnergyModel CreateEnergyModelManager(int clientId, string connStringInsightsDW, string connStringInsightsMetadata, string connStringInsights)
        {
            return (new EnergyModelManager(clientId, connStringInsightsDW, connStringInsightsMetadata, connStringInsights));
        }

        /// <summary>
        /// Create EnergyModelManager that implements IEnergyModel.  Set Configuration with this method.
        /// </summary>
        /// <param name="clientId"></param>
        /// <param name="config"></param>
        /// <returns></returns>
        public IEnergyModel CreateEnergyModelManager(int clientId, EnergyModelConfiguration config)
        {
            return (new EnergyModelManager(clientId, config));
        }

        /// <summary>
        /// Create EnergyModelManager that implements IEnergyModel.  Set Configuration and Weather with this method.
        /// </summary>
        /// <param name="clientId"></param>
        /// <param name="config"></param>
        /// <param name="weather"></param>
        /// <returns></returns>
        public IEnergyModel CreateEnergyModelManager(int clientId, EnergyModelConfiguration config, Weather weather)
        {
            return (new EnergyModelManager(clientId, config, weather));
        }

        /// <summary>
        /// Create EnergyModelManager that implements IEnergyModel.  Set Configuration and Weather with this method.  Specify Mock content provider.
        /// </summary>
        /// <param name="clientId"></param>
        /// <param name="config"></param>
        /// <param name="weather"></param>
        /// <param name="useMockContentProvider"></param>
        /// <returns></returns>
        public IEnergyModel CreateEnergyModelManager(int clientId, EnergyModelConfiguration config, Weather weather, bool useMockContentProvider)
        {
            return (new EnergyModelManager(clientId, config, weather, useMockContentProvider));
        }

    }
}
