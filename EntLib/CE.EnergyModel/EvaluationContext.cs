﻿using CE.EnergyModel.Enums;

namespace CE.EnergyModel
{
    public class EvaluationContext : QuickTrace
    {
        public string Expression { get; set; }
        public EvaluationType EvaluationType { get; set; }
        public Status Status { get; set; }

        public EvaluationContext()
        {
            Expression = string.Empty;
            EvaluationType = Enums.EvaluationType.Pending;
            Status = new Status();
        }

        public EvaluationContext(Enums.EvaluationType evalType)
        {
            Expression = string.Empty;
            EvaluationType = evalType;
            Status = new Status();
        }

    }
}
