﻿using System;
using System.Runtime.Serialization;

namespace CE.EnergyModel.Exceptions
{
    [Serializable]
    public class EnergyModelException : Exception
    {
        /*
          public EnergyModelException()
        { }

        public EnergyModelException(string message)
            : base(message)
        { }

        public EnergyModelException(string message, Exception innerException)
            : base(message, innerException)
        { }

        protected EnergyModelException(SerializationInfo info, StreamingContext context)
            : base(info, context)
        { }
        */
    }
}
