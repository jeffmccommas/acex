﻿using System;
using System.Runtime.Serialization;

namespace CE.EnergyModel.Exceptions
{
    [Serializable]
    public class EnergyModelTimeoutException : Exception
    {
        /*
        public EnergyModelTimeoutException()
        { }
        */
        public EnergyModelTimeoutException(string message)
            : base(message)
        { }
        /*
        public EnergyModelTimeoutException(string message, Exception innerException)
            : base(message, innerException)
        { }

        protected EnergyModelTimeoutException(SerializationInfo info, StreamingContext context)
            : base(info, context)
        { }
        */
    }
}
