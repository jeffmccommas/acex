﻿using System;
using System.Runtime.Serialization;

namespace CE.EnergyModel.Exceptions
{
    [Serializable]
    public class ClientCalculationFactorNotFoundException : Exception
    {
        /*
        public ClientCalculationFactorNotFoundException()
        { }
        */
        public ClientCalculationFactorNotFoundException(string message)
            : base(message)
        { }
        /*
        public ClientCalculationFactorNotFoundException(string message, Exception innerException)
            : base(message, innerException)
        { }

        protected ClientCalculationFactorNotFoundException(SerializationInfo info, StreamingContext context)
            : base(info, context)
        { }
        */
    }
}
