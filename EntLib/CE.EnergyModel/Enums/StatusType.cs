﻿
namespace CE.EnergyModel.Enums
{
    /// <summary>
    /// Used with status of operations with exception information as well as evaluation context.
    /// </summary>
    public enum StatusType
    {
        Unspecified = 0,
        Error = 1,
        Warning = 2,
        Informational = 3
    }
}
