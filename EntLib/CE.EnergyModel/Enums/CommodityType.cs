﻿
namespace CE.EnergyModel.Enums
{
    public enum CommodityType
    {
        Unspecified,
        Electric,
        Gas,
        Water,
        Oil,
        Propane,
        Wood,
        Coal,
        HotWater,
        OtherWater,
        ThermoFlow
    }
}
