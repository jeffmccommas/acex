﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CE.EnergyModel.Enums
{
    public enum ModelType
    {
        Residential = 0,
        Business = 1
    }
}
