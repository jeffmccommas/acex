﻿
namespace CE.EnergyModel.Enums
{
    public enum EvaluationType
    {
        Incomplete,
        Pending,
        Completed,
        Provided,
        Failed
    }
}
