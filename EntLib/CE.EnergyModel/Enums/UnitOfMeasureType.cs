﻿
namespace CE.EnergyModel.Enums
{
    public enum UnitOfMeasureType
    {
        Unspecified,
        kWh,
        Therms,
        Gal,            // water gallons, propane gallons, oil gallons
        BTU,            // wood btus, coal btus
        CCF,            // not used internally, but may be converted to this uom after coming out of energy model, and may indeed appear in api output
        MCF,             // not used internally, but may be converted to this uom after coming out of energy model, and may indeed appear in api output
        HCF
    }

}
