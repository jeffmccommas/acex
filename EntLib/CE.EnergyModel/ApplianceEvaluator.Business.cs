﻿using System;

namespace CE.EnergyModel
{
    /// <summary>
    /// Applance Evaluator for business appliance.  Each business appliance is a placeholder for the correspondng business enduse.
    /// </summary>
    public partial class ApplianceEvaluator
    {
        private const decimal ConvertBtuToKwh = 0.000293071m;
        private const decimal ConvertBtuToTherms = 0.00001m;
        private const decimal ConvertBtuToGallonsOfPropane = 0.00001086956m;
        private const decimal ConvertBtuToGallonsOfOil = 0.00000721032m;

        private void Model_BusinessCooling(string euiColumnName)
        {
            var primaryFuel = GetOptionKey("business.cooling.fuel");
            var buildingSqFt = GetBuildingSquareFeet();
            var buildingCategoryKey = GetBusinessCategoryKey();
            var euiInThousandsOfBtu = GetEnergyUseIntensity(primaryFuel, buildingCategoryKey, _input.RegionParams.ClimateId, buildingSqFt, euiColumnName);

            FinalizeEnergyUseForAppliance(primaryFuel, euiInThousandsOfBtu,buildingSqFt);
        }

        private void Model_BusinessHeating(string euiColumnName)
        {
            var primaryFuel = GetOptionKey("business.heating.fuel");
            var buildingSqFt = GetBuildingSquareFeet();
            var buildingCategoryKey = GetBusinessCategoryKey();
            var euiInThousandsOfBtu = GetEnergyUseIntensity(primaryFuel, buildingCategoryKey, _input.RegionParams.ClimateId, buildingSqFt, euiColumnName);

            FinalizeEnergyUseForAppliance(primaryFuel, euiInThousandsOfBtu, buildingSqFt);
        }

        private void Model_BusinessWaterHeating(string euiColumnName)
        {
            var primaryFuel = GetOptionKey("business.waterheating.fuel");
            var buildingSqFt = GetBuildingSquareFeet();
            var buildingCategoryKey = GetBusinessCategoryKey();
            var euiInThousandsOfBtu = GetEnergyUseIntensity(primaryFuel, buildingCategoryKey, _input.RegionParams.ClimateId, buildingSqFt, euiColumnName);

            FinalizeEnergyUseForAppliance(primaryFuel, euiInThousandsOfBtu, buildingSqFt);
        }

        private void Model_BusinessCooking(string euiColumnName)
        {
            var primaryFuel = GetOptionKey("business.cooking.fuel");
            var buildingSqFt = GetBuildingSquareFeet();
            var buildingCategoryKey = GetBusinessCategoryKey();
            var euiInThousandsOfBtu = GetEnergyUseIntensity(primaryFuel, buildingCategoryKey, _input.RegionParams.ClimateId, buildingSqFt, euiColumnName);

            FinalizeEnergyUseForAppliance(primaryFuel, euiInThousandsOfBtu, buildingSqFt);
        }

        private void Model_BusinessLaundry(string euiColumnName)
        {
            var primaryFuel = GetOptionKey("business.laundry.fuel");
            var buildingSqFt = GetBuildingSquareFeet();
            var buildingCategoryKey = GetBusinessCategoryKey();
            var euiInThousandsOfBtu = GetEnergyUseIntensity(primaryFuel, buildingCategoryKey, buildingSqFt, euiColumnName);

            FinalizeEnergyUseForAppliance(primaryFuel, euiInThousandsOfBtu, buildingSqFt);
        }

        private void Model_BusinessRefrigeration(string euiColumnName)
        {
            var primaryFuel = GetOptionKey("business.refrigeration.fuel");
            var buildingSqFt = GetBuildingSquareFeet();
            var buildingCategoryKey = GetBusinessCategoryKey();
            var euiInThousandsOfBtu = GetEnergyUseIntensity(primaryFuel, buildingCategoryKey, _input.RegionParams.ClimateId, buildingSqFt, euiColumnName);

            FinalizeEnergyUseForAppliance(primaryFuel, euiInThousandsOfBtu, buildingSqFt);
        }

        private void Model_BusinessComputers(string euiColumnName)
        {
            var primaryFuel = "electric";
            var buildingSqFt = GetBuildingSquareFeet();
            var buildingCategoryKey = GetBusinessCategoryKey();
            var euiInThousandsOfBtu = GetEnergyUseIntensity(primaryFuel, buildingCategoryKey, _input.RegionParams.ClimateId, buildingSqFt, euiColumnName);

            FinalizeEnergyUseForAppliance(primaryFuel, euiInThousandsOfBtu, buildingSqFt);
        }

        private void Model_BusinessLighting(string euiColumnName)
        {
            var primaryFuel = "electric";
            var buildingSqFt = GetBuildingSquareFeet();
            var buildingCategoryKey = GetBusinessCategoryKey();
            var euiInThousandsOfBtu = GetEnergyUseIntensity(primaryFuel, buildingCategoryKey, _input.RegionParams.ClimateId, buildingSqFt, euiColumnName);

            FinalizeEnergyUseForAppliance(primaryFuel, euiInThousandsOfBtu, buildingSqFt);
        }

        private void Model_BusinessMiscellaneous(string euiColumnName)
        {
            var primaryFuel = "electric";
            var buildingSqFt = GetBuildingSquareFeet();
            var buildingCategoryKey = GetBusinessCategoryKey();
            var euiInThousandsOfBtu = GetEnergyUseIntensity(primaryFuel, buildingCategoryKey, _input.RegionParams.ClimateId, buildingSqFt, euiColumnName);

            FinalizeEnergyUseForAppliance(primaryFuel, euiInThousandsOfBtu, buildingSqFt);
        }

        private void Model_BusinessOfficeEquipment(string euiColumnName)
        {
            var primaryFuel = "electric";
            var buildingSqFt = GetBuildingSquareFeet();
            var buildingCategoryKey = GetBusinessCategoryKey();
            var euiInThousandsOfBtu = GetEnergyUseIntensity(primaryFuel, buildingCategoryKey, _input.RegionParams.ClimateId, buildingSqFt, euiColumnName);

            FinalizeEnergyUseForAppliance(primaryFuel, euiInThousandsOfBtu, buildingSqFt);
        }

        private void Model_BusinessVentilation(string euiColumnName)
        {
            var primaryFuel = "electric";
            var buildingSqFt = GetBuildingSquareFeet();
            var buildingCategoryKey = GetBusinessCategoryKey();
            var euiInThousandsOfBtu = GetEnergyUseIntensity(primaryFuel, buildingCategoryKey, _input.RegionParams.ClimateId, buildingSqFt, euiColumnName);

            FinalizeEnergyUseForAppliance(primaryFuel, euiInThousandsOfBtu, buildingSqFt);
        }



        #region Helpers

        /// <summary>
        /// Get the optionKey for the profile attribute and return empty string if not present.
        /// </summary>
        /// <param name="profileAttribute"></param>
        /// <returns></returns>
        private string GetOptionKey(string profileAttribute)
        {
            string result;

            try
            {
                result = _input.ProfileAttributes.Find(p => p.Key == profileAttribute).OptionKey;
            }
            catch (Exception)
            {
                result = string.Empty;
            }

            return (result);
        }

        /// <summary>
        /// Get business total area.
        /// </summary>
        /// <returns></returns>
        private decimal GetBuildingSquareFeet()
        {
            decimal result;

            try
            {
                // Get building area profile attribute
                result = Convert.ToDecimal(_input.ProfileAttributes.Find(p => p.Key == "business.totalarea").OptionValue);
            }
            catch (Exception)
            {
                result = 0.0m;
            }

            return (result);
        }

        /// <summary>
        /// Get building category off the business.
        /// </summary>
        /// <returns></returns>
        private string GetBusinessCategoryKey()
        {
            string result;

            try
            {
                // Get business building category profile attribute (ex: buildingcategory.office)
                result = _input.ProfileAttributes.Find(p => p.Key == "business.buildingcategory").OptionKey;
            }
            catch (Exception)
            {
                result = string.Empty;
            }

            return (result);
        }


        /// <summary>
        /// GetEnergyUseIntensity
        /// </summary>
        /// <param name="commodityName">The name of the Commodity</param>
        /// <param name="buildingCategoryKey">The Building Category Key</param>
        /// <param name="climateId">The Id of the Climate</param>
        /// <param name="sizeSqFt">The Buildings Sq Ft</param>
        /// <param name="euiColumnName"></param>
        /// <returns></returns>
        private decimal GetEnergyUseIntensity(string commodityName, string buildingCategoryKey, int climateId,
            decimal sizeSqFt, string euiColumnName)
        {
            var euiFactor = 0.0m;

            // detemine commodityId
            var commodityId = 1; //default is electric; 1 = electric, 2 = gas, 4 = oil, 5 = propane
            if (commodityName.ToLower().Contains("electric"))
            {
                commodityId = 1;
            }
            if (commodityName.ToLower().Contains("gas"))
            {
                commodityId = 2;
            }
            if (commodityName.ToLower().Contains("oil"))
            {
                commodityId = 4;
            }
            if (commodityName.ToLower().Contains("propane"))
            {
                commodityId = 5;
            }

            //determine buildingCategoryId
            // These buildingCategoryId MUST match the Id in the •	cm.BEMBuildingCategory • table of InsightMetaData
            int buildingCategoryId;
            //buildingCategoryKey = "business." + buildingCategoryKey;
            switch (buildingCategoryKey)
            {
                case "business.buildingcategory.office":
                    buildingCategoryId = 0;
                    break;

                case "business.buildingcategory.outpatientnondiagnostic":
                    buildingCategoryId = 1;
                    break;

                case "business.buildingcategory.gasstation":
                    buildingCategoryId = 2;
                    break;

                case "business.buildingcategory.retail":
                    buildingCategoryId = 3;
                    break;

                case "business.buildingcategory.printcopy":
                    buildingCategoryId = 4;
                    break;

                case "business.buildingcategory.smallwarehouse":
                    buildingCategoryId = 5;
                    break;

                case "business.buildingcategory.school":
                    buildingCategoryId = 6;
                    break;

                case "business.buildingcategory.collegeuniversity":
                    buildingCategoryId = 7;
                    break;

                case "business.buildingcategory.bakery":
                    buildingCategoryId = 8;
                    break;

                case "business.buildingcategory.publicassembly":
                    buildingCategoryId = 9;
                    break;

                case "business.buildingcategory.nursinghome":
                    buildingCategoryId = 10;
                    break;

                case "business.buildingcategory.barberbeauty":
                    buildingCategoryId = 11;
                    break;

                case "business.buildingcategory.healthclub":
                    buildingCategoryId = 12;
                    break;

                case "business.buildingcategory.fastfood":
                    buildingCategoryId = 13;
                    break;

                case "business.buildingcategory.outpatientoffice":
                    buildingCategoryId = 14;
                    break;

                case "business.buildingcategory.autosalesrepair":
                    buildingCategoryId = 15;
                    break;

                case "business.buildingcategory.laundrydrycleaner":
                    buildingCategoryId = 16;
                    break;

                case "business.buildingcategory.restaurant":
                    buildingCategoryId = 17;
                    break;

                case "business.buildingcategory.hotelmotel":
                    buildingCategoryId = 18;
                    break;

                case "business.buildingcategory.conveniencestore":
                    buildingCategoryId = 19;
                    break;

                case "business.buildingcategory.conveniencegas":
                    buildingCategoryId = 20;
                    break;

                case "business.buildingcategory.grocery":
                    buildingCategoryId = 21;
                    break;

                case "business.buildingcategory.apartmentcomplex":
                    buildingCategoryId = 22;
                    break;

                default:
                    buildingCategoryId = 0;
                    break;
            }

            //determine the sizeId
            var sizeId = 0;
            if (sizeSqFt > 100000)
            {
                sizeId = 6;
            }
            else if (sizeSqFt > 50000)
            {
                sizeId = 5;
            }
            else if (sizeSqFt > 25000)
            {
                sizeId = 4;
            }
            else if (sizeSqFt > 10000)
            {
                sizeId = 3;
            }
            else if (sizeSqFt > 5000)
            {
                sizeId = 2;
            }
            else if (sizeSqFt > 1000)
            {
                sizeId = 1;
            }

            // get the Building Factor Entity from the list if entities
            var foundEui =
                _input.Configuration.BuildingEuis.Find(
                    i =>
                        i.CategoryId == buildingCategoryId && i.SizeId == sizeId && i.CommodityId == commodityId &&
                        i.ClimateId == climateId);

            // retry with smaller sizeId since some factors do not exist for larger buildings (could be typical doing testing)
            if (foundEui == null)
            {
                for (var retry = 1; retry < 4; retry++)
                {
                    var tempSizeId = sizeId - retry;
                    if (tempSizeId < 0)
                    {
                        tempSizeId = 0;
                    }

                    foundEui =
                        _input.Configuration.BuildingEuis.Find(
                            i =>
                                i.CategoryId == buildingCategoryId && i.SizeId == tempSizeId &&
                                i.CommodityId == commodityId && i.ClimateId == climateId);

                    if (foundEui != null)
                    {
                        break;
                    }
                }
            }

            //and get the actual specific end use factor by specified column name of euiColumnName
            if (foundEui != null)
            {
                euiFactor = (decimal) foundEui[euiColumnName];
            }

            return euiFactor;
        }

        /// <summary>
        /// Helper to lookup specific EUI from the list of entities in config.
        /// </summary>
        /// <param name="commodityName"></param>
        /// <param name="buildingCategoryKey"></param>
        /// <param name="sizeSqFt"></param>
        /// <param name="euiColumnName"></param>
        /// <returns></returns>
        private decimal GetEnergyUseIntensity(string commodityName, string buildingCategoryKey, decimal sizeSqFt, string euiColumnName)
        {
            return (GetEnergyUseIntensity(commodityName, buildingCategoryKey,0, sizeSqFt, euiColumnName));
        }


        /// <summary>
        /// Convert BTUs to appropriate fuel uom and add the energy use to the appropriate collection of energy uses in the inclusive appliance.
        /// </summary>
        /// <param name="primaryFuel"></param>
        /// <param name="kbtus"></param>
        /// <param name="sqft"></param>/// 
        private void FinalizeEnergyUseForAppliance(string primaryFuel, decimal kbtus, decimal sqft)
        {

            if (primaryFuel.Contains("electric"))
            {
                Appliance.EnergyUses.Add(new EnergyUse(true) { Commodity = Enums.CommodityType.Electric, Quantity = kbtus * 1000 * sqft * ConvertBtuToKwh, UnitOfMeasure = Enums.UnitOfMeasureType.kWh });
                return;
            }

            if (primaryFuel.Contains("gas"))
            {
                Appliance.EnergyUses.Add(new EnergyUse(true) { Commodity = Enums.CommodityType.Gas, Quantity = kbtus * 1000 * sqft * ConvertBtuToTherms, UnitOfMeasure = Enums.UnitOfMeasureType.Therms });
                return;
            }

            if (primaryFuel.Contains("propane"))
            {
                Appliance.EnergyUses.Add(new EnergyUse(true) { Commodity = Enums.CommodityType.Propane, Quantity = kbtus * 1000 * sqft * ConvertBtuToGallonsOfPropane, UnitOfMeasure = Enums.UnitOfMeasureType.Gal });
                return;
            }

            if (primaryFuel.Contains("oil"))
            {
                Appliance.EnergyUses.Add(new EnergyUse(true) { Commodity = Enums.CommodityType.Oil, Quantity = kbtus * 1000 * sqft * ConvertBtuToGallonsOfOil, UnitOfMeasure = Enums.UnitOfMeasureType.Gal });
            }

        }

        #endregion


    }

}
