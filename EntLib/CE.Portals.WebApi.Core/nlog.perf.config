﻿<?xml version="1.0" encoding="utf-8" ?>
<nlog xmlns="http://www.nlog-project.org/schemas/NLog.xsd"
      xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
      autoReload="true"
      throwExceptions="false"
      internalLogLevel="Warn"
      internalLogFile="NLogInternal.log">

  <!-- Load the ASP.NET Core plugin -->
  <extensions>

    <add assembly="NLog.Web.AspNetCore"/>
  </extensions>

  <!-- the targets to write to -->
  <targets>

    <target name="filelog" xsi:type="File" fileName="basedir/${date:format=yyyy-MM-dd}-webapi.log" />
    <target name="asyncDbWrapperTarget" xsi:type="AsyncWrapper" queueLimit="10000" timeToSleepBetweenBatches="50" batchSize="100" overflowAction="Block">
      <target name="dblog" xsi:type="Database">
        <dbProvider>sqlserver</dbProvider>
        <connectionString>${var:dbConnectionstring}</connectionString>
        <commandText>
          exec portal.LogInsert @UserID, @Level, @Logger, @MachineName, @Message, @Exception, @LogDate
        </commandText>

        <parameter name="@UserID" layout="${aspnet-session:Variable=UserId:EvaluateAsNestedProperties=true}" />
        <parameter name="@Level" layout="${level}" />
        <parameter name="@Logger" layout="${logger}" />
        <parameter name="@MachineName" layout="${machinename}" />
        <parameter name="@Message" layout="${message:whenEmpty=(no message)}" />
        <parameter name="@Exception" layout="${exception:format=ToString}" />
        <parameter name="@LogDate" layout="${date}" />
      </target>
    </target>
    <!--<target name="asyncDebugWrapperTarget" xsi:type="AsyncWrapper" queueLimit="10000" timeToSleepBetweenBatches="50" batchSize="100" overflowAction="Block">
      <target name="debugTarget" xsi:type="Debugger" layout="${time}|${level:uppercase=true}|${logger}|${message}|${aspnet-request:serverVariable=HTTP_USER_AGENT}" />
    </target>-->
    <target name="asyncDbWrapperET" xsi:type="AsyncWrapper" queueLimit="10000" timeToSleepBetweenBatches="50" batchSize="100" overflowAction="Block">
      <target name="dbEndpointTracking" type="Database">

        <dbProvider>sqlserver</dbProvider>
        <connectionString>${var:dbConnectionstring}</connectionString>
        <commandText>
          exec portal.EndpointTrackingInsert @ClientID, @UserID, @Verb, @ResourceName, @EndpointID, @UserAgent, @SourceIPAddress, @XCEMessageID, @XCEChannel, @XCELocale, @XCEMeta, @Query, @MachineName, @IncludeInd, @LogDate, @ThirdPartyClientId
        </commandText>

        <parameter name="@ClientID" layout="${event-context:item=et_clientid}" />
        <parameter name="@UserID" layout="${event-context:item=et_userid}" />
        <parameter name="@Verb" layout="${event-context:item=et_verb}" />
        <parameter name="@ResourceName" layout="${event-context:item=et_resourcename}" />
        <parameter name="@EndpointID" layout="${event-context:item=et_endpointid}" />
        <parameter name="@UserAgent" layout="${event-context:item=et_useragent}" />
        <parameter name="@SourceIPAddress" layout="${event-context:item=et_sourceip}" />
        <parameter name="@XCEMessageID" layout="${event-context:item=et_xcemessageid}" />
        <parameter name="@XCEChannel" layout="${event-context:item=et_xcechannel}" />
        <parameter name="@XCELocale" layout="${event-context:item=et_xcelocale}" />
        <parameter name="@XCEMeta" layout="${event-context:item=et_xcemeta}" />
        <parameter name="@Query" layout="${event-context:item=et_query}" />
        <parameter name="@MachineName" layout="${machinename}" />
        <parameter name="@IncludeInd" layout="${event-context:item=et_include}" />
        <parameter name="@LogDate" layout="${date}" />
        <parameter name="@ThirdPartyClientId" layout="${event-context:item=et_thirdpartyclientid}" />
      </target>

    </target>
    <target xsi:type="Null" name="blackhole" />
  </targets>
  <!-- Levels: Off, Trace, Debug, Info, Warn, Error, Fatal -->
  <!-- rules to map from logger name to target -->
  <rules>
    <logger name="Microsoft.*" minlevel="Trace" writeTo="blackhole" final="true" />
    <logger name="System.Web.Http.*" minlevel="Off" writeTo="filelog" />
    <logger name="System.Web.Http.*" minlevel="Off" writeTo="asyncDbWrapperTarget" />
    <logger name="EndpointTracker" minlevel="Info" writeTo="asyncDbWrapperET" />
    <logger name="CE.Portals.*" minlevel="Info" writeTo="asyncDbWrapperTarget" />

  </rules>
</nlog>