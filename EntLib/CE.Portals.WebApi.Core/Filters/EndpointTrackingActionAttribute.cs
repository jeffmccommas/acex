﻿using System;
using CE.Portals.WebApi.Core.logwriter;
using Microsoft.AspNetCore.Mvc.Filters;
using System.Net;
using System.Text;
using Microsoft.AspNetCore.Http.Features;

namespace CE.Portals.WebApi.Core.Filters
{
    public class EndpointTrackingActionAttribute : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext actionContext)
        {
            //if (actionContext.HttpContext.Request.Path != "/api/PortalsApi/UserAuth")
            //{
            string userId = "99";
            string clientId = "99";
            string query = WebUtility.UrlDecode((actionContext.HttpContext.Request.Headers["WebToken"]));
            string accessToken = (actionContext.HttpContext.Request.Headers["Authorization"]);
            if (query != null)
            {
                if (query.Contains("\""))
                {
                    query = query.Replace("\"", "");
                }
                string incoming = query.Replace('_', '/').Replace('-', '+');
                switch (query.Length % 4)
                {
                    case 2:
                        incoming += "==";
                        break;
                    case 3:
                        incoming += "=";
                        break;
                }

                byte[] cryptBytes = Convert.FromBase64String(incoming);

                byte[] passBytes = Encoding.UTF8.GetBytes(FilterHelper.KEY);

                query = Encoding.UTF8.GetString(FilterHelper.AESDecryptBytes(cryptBytes, passBytes, FilterHelper.SALT));
                userId = query.Split(',')[0].Split(':')[1];
                clientId = query.Split(',')[1].Split(':')[1];
            }

            var tracker = new NLogEndpointTrackingWriter();
            var record = new EndpointTrack();
            record.ClientId = Convert.ToInt32(clientId.Replace("\"", ""));
            record.UserId = Convert.ToInt32(userId.Replace("\"", ""));
            record.UserAgent = ((Microsoft.AspNetCore.Server.Kestrel.Internal.Http.FrameRequestHeaders)actionContext.HttpContext.Request.Headers).HeaderUserAgent;
            record.Verb = actionContext.HttpContext.Request.Method;
            record.ResourceName = actionContext.HttpContext.Request.Path;
            record.QueryString = actionContext.HttpContext.Request.QueryString.Value;
            record.Include = true;
            //record.EndpointId = ;
            record.XCEMessageId = ((Microsoft.AspNetCore.Http.DefaultHttpContext)actionContext.HttpContext).TraceIdentifier;
            record.XCEChannel = "";
            record.XCELocale = "";
            record.XCEMeta = "";
            //record.SourceIP = ((Microsoft.AspNetCore.Server.Kestrel.Internal.Http.Frame)actionContext.HttpContext.Features).RemoteIpAddress.ToString();
            record.SourceIP = actionContext.HttpContext.Features.Get<IHttpConnectionFeature>()?.RemoteIpAddress?.ToString();
            //record.SourceIP = actionContext.HttpContext.GetFeature<IHttpConnectionFeature>()?.RemoteIpAddress;

            tracker.Track(record);
            
        }

      
    }
}
