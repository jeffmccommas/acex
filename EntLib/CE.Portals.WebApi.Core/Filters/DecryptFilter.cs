﻿using Microsoft.AspNetCore.Mvc.Filters;
using System;
using System.Net;
using System.Text;
using CE.Portals.Integrations.Common;

namespace CE.Portals.WebApi.Core.Filters
{
    public class DecryptFilterAttribute : ActionFilterAttribute
    {
       
        public DecryptFilterAttribute()
        {
           // _KEY = purposeKey;
        }
        public override void OnActionExecuting(ActionExecutingContext context)
        {
            string query = WebUtility.UrlDecode((context.HttpContext.Request.Headers["WebToken"]));
            string accessToken = (context.HttpContext.Request.Headers["Authorization"]);
            if (query != null)
            {
                if (query.Contains("\""))
                {
                    query = query.Replace("\"", "");
                }
                string incoming = query.Replace('_', '/').Replace('-', '+');
                switch (query.Length % 4)
                {
                    case 2: incoming += "=="; break;
                    case 3: incoming += "="; break;
                }

                byte[] cryptBytes = Convert.FromBase64String(incoming);

                byte[] passBytes = Encoding.UTF8.GetBytes(FilterHelper.KEY);

                query = Encoding.UTF8.GetString(FilterHelper.AESDecryptBytes(cryptBytes, passBytes, FilterHelper.SALT));
                context.HttpContext.Items.Add(ApplicationConstants.WEBTOKEN, query);
                if (accessToken != null)
                {
                    context.HttpContext.Items.Add("Authorization", query);
                }
            }
            
            base.OnActionExecuting(context);
           
        }
    }
}
