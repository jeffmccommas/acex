﻿using Microsoft.AspNetCore.Mvc;
using CE.Portals.WebApi.Core.Models;
using System.Net.Http;
using Microsoft.Extensions.Options;
using CE.Portals.WebApi.Core.Filters;
using CE.Portals.WebApi.Core.Helpers;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using Newtonsoft.Json;
using System.Linq;
using CE.Portals.DataService.Interfaces;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.AspNetCore.Http;

namespace CE.Portals.WebApi.Core.Controllers
{
    [Route("api/[controller]")]
    public class InsightsApiController : Controller
    {
        private readonly IMemoryCache _memoryCache;
        private readonly AppSettings _appSettings;
        private static int _serverCacheTimeOut = 1800;
        private HttpMessageHandler _httpMessagehandler;
        private readonly ILogger _logger;
        const string category = "espm";
        const string categoryreport = "espmreport";
        private readonly ILoginService _loginService;
        public InsightsApiController(IOptions<AppSettings> appsettings, ILogger<InsightsApiController> logger, IMemoryCache memoryCache, ILoginService loginService,  HttpMessageHandler httpMessagehandler = null)
        {
            _memoryCache = memoryCache;
            _logger = logger;
            _appSettings = appsettings.Value;
            if (!string.IsNullOrEmpty(_appSettings.ContentsApiServerCacheTimeOutSeconds))
                _serverCacheTimeOut = Convert.ToInt32(_appSettings.ContentsApiServerCacheTimeOutSeconds)/60;
            _httpMessagehandler = httpMessagehandler;
            _loginService = loginService;

        }
        private List<Configuration> GetConfiguration(string clientId, string locale, string userId)
        {
            string servercacheKey = "Configuration-" + clientId;
            List<Configuration> content = null;
            if (this._memoryCache.TryGetValue(servercacheKey, out content))
            {
                //Log(LogLevel.Debug, "GetContent returning cached object", userId);
                return content;
            }
            try
            {
                var webApiResponseModel = new WebAPIResponseModel(_appSettings, _logger, _memoryCache, _loginService);
                webApiResponseModel.clientId = clientId;

                var response =
                    webApiResponseModel.GetContent(
                        $"refresh=false&IncludeContent=true&type=Configuration&category={category}", locale);
                if (response.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    content = JsonConvert.DeserializeObject<RootContentObject>(response.Content).Content.Configuration;
                    this._memoryCache.Set(servercacheKey, content, new MemoryCacheEntryOptions { AbsoluteExpirationRelativeToNow = (DateTime.Now.AddMinutes(_serverCacheTimeOut) - DateTime.Now) });
                    return content;
                }
                else
                {
                    Log(LogLevel.Error, $"GetConfiguration: AcceptConnection returned {response.StatusCode} " + response.Content, userId);
                    return null;
                }

            }
            catch (Exception ex)
            {
                Log(LogLevel.Error, "GetConfiguration returned Error" + ex.Message, "");
                return null;
            }
        }

        private List<ConfigurationBulk> GetConfigurationForReport(string clientId, string locale, string userId)
        {
            string servercacheKey = "Configuration-" + clientId;
            List<ConfigurationBulk> content = null;
            if (this._memoryCache.TryGetValue(servercacheKey, out content))
            {
                //Log(LogLevel.Debug, "GetContent returning cached object", userId);
                return content;
            }
            try
            {
                var webApiResponseModel = new WebAPIResponseModel(_appSettings, _logger, _memoryCache, _loginService);
                webApiResponseModel.clientId = clientId;

                var response =
                    webApiResponseModel.GetContent(
                        $"refresh=true&IncludeContent=true&type=configurationbulk&category={categoryreport}", locale);
                if (response.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    content = JsonConvert.DeserializeObject<RootContentObject>(response.Content).Content.ConfigurationBulk;
                    this._memoryCache.Set(servercacheKey, content, new MemoryCacheEntryOptions { AbsoluteExpirationRelativeToNow = (DateTime.Now.AddMinutes(_serverCacheTimeOut) - DateTime.Now) });
                    return content;
                }
                else
                {
                    Log(LogLevel.Error, $"GetConfiguration: AcceptConnection returned {response.StatusCode} " + response.Content, userId);
                    return null;
                }

            }
            catch (Exception ex)
            {
                Log(LogLevel.Error, "GetConfiguration returned Error" + ex.Message, "");
                return null;
            }
        }

        private Content GetContent(string clientId, string locale, string userId)
        {
            string servercacheKey = "Contents-" + clientId;
            Content content = null;
            if (this._memoryCache.TryGetValue(servercacheKey, out content))
            {
                //Log(LogLevel.Debug, "GetContent returning cached object", userId);
                return content;
            }
            try
            {
                var webApiResponseModel = new WebAPIResponseModel(_appSettings, _logger, _memoryCache, _loginService);
                webApiResponseModel.clientId = clientId;

                var response =
                    webApiResponseModel.GetContent(
                        $"refresh=false&IncludeContent=true&type=TextContent&category={category}", locale);
                if (response.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    content = JsonConvert.DeserializeObject<RootContentObject>(response.Content).Content;
                    this._memoryCache.Set(servercacheKey, content, new MemoryCacheEntryOptions { AbsoluteExpirationRelativeToNow = (DateTime.Now.AddMinutes(_serverCacheTimeOut) - DateTime.Now) });
                    return content;
                }
                else
                {
                    Log(LogLevel.Error, $"GetContent: AcceptConnection returned {response.StatusCode} " + response.Content, userId);
                    return null;
                }

            }
            catch (Exception ex)
            {
                Log(LogLevel.Error, "GetContent returned Error" + ex.Message, "");
                return null;
            }
        }

        [Route("ContactsContent")]
        [HttpGet]
        [DecryptFilter]
        [ResponseCache(CacheProfileName = "Default")]
        public IActionResult GetContactsContent(string locale, string tabKey, string widgetType)
        {
            var loginUser = QueryStringHelper.GetLoginuser(this.HttpContext);
            if (loginUser != null)
            {
                try
                {
                    Contacts contacts = null;
                    Content content = null;

                    content = GetContent(loginUser.ClientId, locale, loginUser.UserId);
                    if (content == null)
                    {
                        Log(LogLevel.Error, "GetContactsContent: AcceptConnection returned HttpStatusCode.BadRequest - " + content, loginUser.UserId);
                        return new StatusCodeResult((int)System.Net.HttpStatusCode.BadRequest);
                    }


                    var contactsTextContent = content.TextContent.ToList();

                    contacts = new Contacts
                    {
                        AcceptButton =
                            CMS.GetWidgetContent(contactsTextContent, tabKey, widgetType, "acceptbutton"),
                        AccountColumn =
                            CMS.GetWidgetContent(contactsTextContent, tabKey, widgetType, "accountcolumn"),
                        AddesssColumn =
                            CMS.GetWidgetContent(contactsTextContent, tabKey, widgetType, "addresscolumn"),
                        CustomField1Column = CMS.GetWidgetContent(contactsTextContent, tabKey, widgetType,
                            "customfield1column"),
                        CustomField2Column = CMS.GetWidgetContent(contactsTextContent, tabKey, widgetType,
                            "customfield2column"),
                        Introtext = CMS.GetWidgetContent(contactsTextContent, tabKey, widgetType, "introtext"),
                        NameColumn = CMS.GetWidgetContent(contactsTextContent, tabKey, widgetType, "namecolumn"),
                        NoDataMessage =
                            CMS.GetWidgetContent(contactsTextContent, tabKey, widgetType, "nodatamessage"),
                        RejectButton =
                            CMS.GetWidgetContent(contactsTextContent, tabKey, widgetType, "rejectbutton"),
                        StatusColumn =
                            CMS.GetWidgetContent(contactsTextContent, tabKey, widgetType, "statuscolumn"),
                        TimeStampColumn =
                            CMS.GetWidgetContent(contactsTextContent, tabKey, widgetType, "timestampcolumn"),
                        Title = CMS.GetWidgetContent(contactsTextContent, tabKey, widgetType, "title"),
                        AcceptedAccounts =
                            CMS.GetWidgetContent(contactsTextContent, tabKey, widgetType, "acceptedaccounts"),
                        PendingAccounts =
                            CMS.GetWidgetContent(contactsTextContent, tabKey, widgetType, "pendingaccounts"),
                        RejectConfirm = CMS.GetWidgetContent(contactsTextContent, tabKey, widgetType,
                            "rejectconfirmationconfirm"),
                        RejectCancel = CMS.GetWidgetContent(contactsTextContent, tabKey, widgetType,
                            "rejectconfirmationcancel"),
                        RejectConfirmMessage = CMS.GetWidgetContent(contactsTextContent, tabKey, widgetType,
                            "rejectconfirmationmessage"),
                        RejectConfirmTitle = CMS.GetWidgetContent(contactsTextContent, tabKey, widgetType,
                            "rejectconfirmationtitle"),
                        Footer = CMS.GetWidgetContent(contactsTextContent, tabKey, widgetType, "footer"),
                        NoRecordsFound =
                            CMS.GetWidgetContent(contactsTextContent, tabKey, widgetType, "norecordsfound"),
                        NoSelection = CMS.GetWidgetContent(contactsTextContent, tabKey, widgetType, "noselection"),
                        Loading = CMS.GetWidgetContent(contactsTextContent, tabKey, widgetType, "loading")
                    };

                    return new OkObjectResult(contacts);

                }
                catch (Exception ex)
                {
                    Log(LogLevel.Error, "GetContactsContent returned error " + ex.Message, loginUser.UserId);
                    return new StatusCodeResult((int)System.Net.HttpStatusCode.BadGateway);

                }

            }

            Log(LogLevel.Error, "GetContactsContent returned HttpStatusCode.Forbidden", "");
            return new StatusCodeResult((int)System.Net.HttpStatusCode.Forbidden);

        }

        [Route("MetersContent")]
        [HttpGet]
        [DecryptFilter]
        [ResponseCache(CacheProfileName = "Default")]
        public IActionResult GetMetersContent(string locale, string tabKey, string widgetType)
        {

            var loginUser = QueryStringHelper.GetLoginuser(this.HttpContext);
            if (loginUser != null)
            {
                //string servercacheKey = "Contents-" + loginUser.ClientId;
                try
                {
                    MeterContent meters = null;
                    Content content = null;

                    content = GetContent(loginUser.ClientId, locale, loginUser.UserId);
                    if (content == null)
                    {
                        Log(LogLevel.Error, "GetMetersContent: AcceptConnection returned HttpStatusCode.BadRequest - " + content, loginUser.UserId);
                        return new StatusCodeResult((int)System.Net.HttpStatusCode.BadRequest);
                    }

                    var contactsTextContent = content.TextContent.ToList();
                    meters = new MeterContent
                    {
                        AcceptedMeters =
                            CMS.GetWidgetContent(contactsTextContent, tabKey, widgetType, "acceptedmeters"),
                        AcceptMeters = CMS.GetWidgetContent(contactsTextContent, tabKey, widgetType, "acceptmeters"),
                        AccountAddress =
                            CMS.GetWidgetContent(contactsTextContent, tabKey, widgetType, "accountaddress"),
                        AccountId = CMS.GetWidgetContent(contactsTextContent, tabKey, widgetType, "accountid"),
                        AccountJobTitle =
                            CMS.GetWidgetContent(contactsTextContent, tabKey, widgetType, "accountjobtitle"),
                        AccountName = CMS.GetWidgetContent(contactsTextContent, tabKey, widgetType, "accountname"),
                        AccountPhone = CMS.GetWidgetContent(contactsTextContent, tabKey, widgetType, "accountphone"),
                        CustomField1 = CMS.GetWidgetContent(contactsTextContent, tabKey, widgetType,
                            "customfield1column"),
                        CustomField2 = CMS.GetWidgetContent(contactsTextContent, tabKey, widgetType,
                            "customfield2column"),
                        FirstBillDate = CMS.GetWidgetContent(contactsTextContent, tabKey, widgetType, "firstbilldate"),
                        Footer = CMS.GetWidgetContent(contactsTextContent, tabKey, widgetType, "footer"),
                        InUse = CMS.GetWidgetContent(contactsTextContent, tabKey, widgetType, "inuse"),
                        Loading = CMS.GetWidgetContent(contactsTextContent, tabKey, widgetType, "loading"),
                        Metered = CMS.GetWidgetContent(contactsTextContent, tabKey, widgetType, "metered"),
                        MeterId = CMS.GetWidgetContent(contactsTextContent, tabKey, widgetType, "meterid"),
                        MeterName = CMS.GetWidgetContent(contactsTextContent, tabKey, widgetType, "metername"),
                        MetersIntroText =
                            CMS.GetWidgetContent(contactsTextContent, tabKey, widgetType, "metersintrotext"),
                        MeterType = CMS.GetWidgetContent(contactsTextContent, tabKey, widgetType, "metertype"),
                        NoRecordFound = CMS.GetWidgetContent(contactsTextContent, tabKey, widgetType, "norecordsfound"),
                        NoSelection = CMS.GetWidgetContent(contactsTextContent, tabKey, widgetType, "noselection"),
                        PendingMeters = CMS.GetWidgetContent(contactsTextContent, tabKey, widgetType, "pendingmeters"),
                        PropertyAddress =
                            CMS.GetWidgetContent(contactsTextContent, tabKey, widgetType, "propertyaddress"),
                        PropertyBuildings =
                            CMS.GetWidgetContent(contactsTextContent, tabKey, widgetType, "propertybuildings"),
                        PropertyConstructionStatus = CMS.GetWidgetContent(contactsTextContent, tabKey, widgetType,
                            "propertyconstructionstatus"),
                        PropertyYearBuilt =
                            CMS.GetWidgetContent(contactsTextContent, tabKey, widgetType, "propertyearbuilt"),
                        PropertyFloorArea =
                            CMS.GetWidgetContent(contactsTextContent, tabKey, widgetType, "propertyfloorarea"),
                        PropertyId = CMS.GetWidgetContent(contactsTextContent, tabKey, widgetType, "propertyid"),
                        PropertyIsFederal =
                            CMS.GetWidgetContent(contactsTextContent, tabKey, widgetType, "propertyisfederal"),
                        PropertyName = CMS.GetWidgetContent(contactsTextContent, tabKey, widgetType, "propertyname"),
                        PropertyOccupancyPercentage = CMS.GetWidgetContent(contactsTextContent, tabKey, widgetType,
                            "propertyoccupancypercent"),
                        PropertyPrimaryFunction = CMS.GetWidgetContent(contactsTextContent, tabKey, widgetType,
                            "propertyprimaryfunction"),
                        RejectCancel = CMS.GetWidgetContent(contactsTextContent, tabKey, widgetType, "rejectcancel"),
                        RejectConfirm = CMS.GetWidgetContent(contactsTextContent, tabKey, widgetType, "rejectconfirm"),
                        RejectConfirmationMessage = CMS.GetWidgetContent(contactsTextContent, tabKey, widgetType,
                            "rejectconfirmationmessage"),
                        RejectConfirmationTitle = CMS.GetWidgetContent(contactsTextContent, tabKey, widgetType,
                            "rejectconfirmationtitle"),
                        TimeStamp = CMS.GetWidgetContent(contactsTextContent, tabKey, widgetType, "timestamp"),
                        UnitOfMeasure = CMS.GetWidgetContent(contactsTextContent, tabKey, widgetType, "unitofmeasure"),
                        RejectMeters = CMS.GetWidgetContent(contactsTextContent, tabKey, widgetType, "rejectmeters"),
                        Title = CMS.GetWidgetContent(contactsTextContent, tabKey, widgetType, "meterstitle"),
                        LastUpdated = CMS.GetWidgetContent(contactsTextContent, tabKey, widgetType, "lastupdated"),
                        LoadingPropertyDetails = CMS.GetWidgetContent(contactsTextContent, tabKey, widgetType, "loadingpropertydetails"),
                        ErrorLoadingPropertyDetails = CMS.GetWidgetContent(contactsTextContent, tabKey, widgetType, "errorloadingpropertydetails"),
                        MetersTab= CMS.GetWidgetContent(contactsTextContent, tabKey, widgetType, "meterstab"),
                        MetricsTab = CMS.GetWidgetContent(contactsTextContent, tabKey, widgetType, "metricstab"),
                        LoadingAccountDetails = CMS.GetWidgetContent(contactsTextContent, tabKey, widgetType, "loadingaccountdetails"),
                        ErrorLoadingAccountDetails = CMS.GetWidgetContent(contactsTextContent, tabKey, widgetType, "errorloadingaccountdetails"),
                        BillData = CMS.GetWidgetContent(contactsTextContent, tabKey, widgetType, "billdata"),
                        ConsumptionRecords = CMS.GetWidgetContent(contactsTextContent, tabKey, widgetType, "consumptionrecords"),
                        StartDate = CMS.GetWidgetContent(contactsTextContent, tabKey, widgetType, "startdate"),
                        EndDate = CMS.GetWidgetContent(contactsTextContent, tabKey, widgetType, "enddate"),
                        Usage = CMS.GetWidgetContent(contactsTextContent, tabKey, widgetType, "usage"),
                        Cost = CMS.GetWidgetContent(contactsTextContent, tabKey, widgetType, "cost"),
                        NoMatchingCustomerFound = CMS.GetWidgetContent(contactsTextContent, tabKey, widgetType, "nomatchingcustomerfound"),
                        ErrorFetchingMatchCcustomer = CMS.GetWidgetContent(contactsTextContent, tabKey, widgetType, "errorfetchingmatchcustomer"),
                        ConsumptionTab = CMS.GetWidgetContent(contactsTextContent, tabKey, widgetType, "consumptiontab"),
                        MappingTab = CMS.GetWidgetContent(contactsTextContent, tabKey, widgetType, "mappingtabtab"),
                        MappedSuccessfully = CMS.GetWidgetContent(contactsTextContent, tabKey, widgetType, "mappedsuccessfully"),
                        MappingNotfound = CMS.GetWidgetContent(contactsTextContent, tabKey, widgetType, "mappingnotfound"),
                        MissingFields = CMS.GetWidgetContent(contactsTextContent, tabKey, widgetType, "mappingmissingfields"),
                        CustomDataMessage = CMS.GetWidgetContent(contactsTextContent, tabKey, widgetType, "customdatamessage"),
                        InvalidMapping = CMS.GetWidgetContent(contactsTextContent, tabKey, widgetType, "invalidmapping"),
                        NoErrorsMessage = CMS.GetWidgetContent(contactsTextContent, tabKey, widgetType, "noerrorsmessage"),
                        PremiseId = CMS.GetWidgetContent(contactsTextContent, tabKey, widgetType, "premiseid"),
                        MappedCustomer = CMS.GetWidgetContent(contactsTextContent, tabKey, widgetType, "mappedcustomer"),
                        AddMapping = CMS.GetWidgetContent(contactsTextContent, tabKey, widgetType, "addmapping"),
                        Save = CMS.GetWidgetContent(contactsTextContent, tabKey, widgetType, "save"),
                        Cancel = CMS.GetWidgetContent(contactsTextContent, tabKey, widgetType, "cancel"),
                        AccountIdNotNull = CMS.GetWidgetContent(contactsTextContent, tabKey, widgetType, "accountidnotnull"),
                        AccountIdUnique = CMS.GetWidgetContent(contactsTextContent, tabKey, widgetType, "accountidunique"),
                        ClearFilters = CMS.GetWidgetContent(contactsTextContent, tabKey, widgetType, "clearfilters"),
                        AccountOrganization = CMS.GetWidgetContent(contactsTextContent, tabKey, widgetType, "organization"),
                        NoAccessToProperty = CMS.GetWidgetContent(contactsTextContent, tabKey, widgetType, "noaccesstoproperty"),
                        NoAccessToAccount = CMS.GetWidgetContent(contactsTextContent, tabKey, widgetType, "noaccesstoaccount")
                    };

                    return new OkObjectResult(meters);


                }
                catch (Exception ex)
                {
                    Log(LogLevel.Error, "GetMetersContent returned error " + ex.Message, loginUser.UserId);
                    return new StatusCodeResult((int)System.Net.HttpStatusCode.BadGateway);

                }

            }
            Log(LogLevel.Error, "GetMetersContent returned HttpStatusCode.Forbidden", "");
            return new StatusCodeResult((int)System.Net.HttpStatusCode.Forbidden);

        }

        [Route("TabContent")]
        [HttpGet]
        [DecryptFilter]
        [ResponseCache(CacheProfileName = "Default")]
        public IActionResult GetTabContent(string locale, string tabKey, string widgetType)
        {
            var loginUser = QueryStringHelper.GetLoginuser(this.HttpContext);
            if (loginUser != null)
            {
                try
                {
                    TabContent tab = null;
                    Content content = null;
                    content = GetContent(loginUser.ClientId, locale, loginUser.UserId);
                    if (content == null)
                    {
                        Log(LogLevel.Error, "GetTabContent: AcceptConnection returned HttpStatusCode.BadRequest - " + content, loginUser.UserId);
                        return new StatusCodeResult((int)System.Net.HttpStatusCode.BadRequest);
                    }


                    var contactsTextContent = content.TextContent.ToList();

                    tab = new TabContent
                    {
                        Contacts = CMS.GetWidgetContent(contactsTextContent, tabKey, widgetType, "contacts"),
                        Meters = CMS.GetWidgetContent(contactsTextContent, tabKey, widgetType, "meters")
                    };


                    return new OkObjectResult(tab);


                }
                catch (Exception ex)
                {
                    Log(LogLevel.Error, "GeTTabContent returned error " + ex.Message, loginUser.UserId);
                    return new StatusCodeResult((int)System.Net.HttpStatusCode.BadGateway);

                }

            }

            Log(LogLevel.Error, "GeTTabContent returned HttpStatusCode.Forbidden", "");
            return new StatusCodeResult((int)System.Net.HttpStatusCode.Forbidden);

        }

        [Route("AddUserContent")]
        [HttpGet]
        [DecryptFilter]
        [ResponseCache(CacheProfileName = "Default")]
        public IActionResult GetAddUserContent(string locale, string tabKey, string widgetType)
        {
            var loginUser = QueryStringHelper.GetLoginuser(this.HttpContext);
            if (loginUser != null)
            {
                try
                {
                    NewUser newUserContent = null;
                    Content content = null;

                    content = GetContent(loginUser.ClientId, locale, loginUser.UserId);
                    if (content == null)
                    {
                        Log(LogLevel.Error, "GetAddUserContent: AcceptConnection returned HttpStatusCode.BadRequest - " + content, loginUser.UserId);
                        return new StatusCodeResult((int)System.Net.HttpStatusCode.BadRequest);
                    }


                    var contactsTextContent = content.TextContent.ToList();

                    newUserContent = new NewUser
                    {
                        AddUserTitle = CMS.GetWidgetContent(contactsTextContent, tabKey, widgetType, "title"),
                        FirstName = CMS.GetWidgetContent(contactsTextContent, tabKey, widgetType, "firstnamelabel"),
                        LastName = CMS.GetWidgetContent(contactsTextContent, tabKey, widgetType, "lastnamelabel"),
                        UserName = CMS.GetWidgetContent(contactsTextContent, tabKey, widgetType, "usernamelabel"),
                        Password = CMS.GetWidgetContent(contactsTextContent, tabKey, widgetType, "passwordlabel"),
                        Administrator = CMS.GetWidgetContent(contactsTextContent, tabKey, widgetType, "adminlabel"),
                        Status = CMS.GetWidgetContent(contactsTextContent, tabKey, widgetType, "statuslabel"),
                        Save = CMS.GetWidgetContent(contactsTextContent, tabKey, widgetType, "savebutton"),
                        FirstNameRequired = CMS.GetWidgetContent(contactsTextContent, tabKey, widgetType, "firstnamerequired"),
                        Cancel = CMS.GetWidgetContent(contactsTextContent, tabKey, widgetType, "cancelbutton"),
                    };


                    return new OkObjectResult(newUserContent);


                }
                catch (Exception ex)
                {
                    Log(LogLevel.Error, "GetAddUserContent returned error " + ex.Message, loginUser.UserId);
                    return new StatusCodeResult((int)System.Net.HttpStatusCode.BadGateway);

                }

            }
            else
            {
                Log(LogLevel.Error, "GetAddUserContent returned HttpStatusCode.Forbidden", "");
                return new StatusCodeResult((int)System.Net.HttpStatusCode.Forbidden);
            }
        }

        [Route("ManageUserContent")]
        [HttpGet]
        [DecryptFilter]
        [ResponseCache(CacheProfileName = "Default")]
        public IActionResult GetManageuserContent(string locale, string tabKey, string widgetType)
        {
            var loginUser = QueryStringHelper.GetLoginuser(this.HttpContext);
            if (loginUser != null)
            {
                try
                {
                    ManageUser manageUserContent = null;
                    Content content = null;

                    content = GetContent(loginUser.ClientId, locale, loginUser.UserId);
                    if (content == null)
                    {
                        Log(LogLevel.Error, "GetManageuserContent: AcceptConnection returned HttpStatusCode.BadRequest - " + content, loginUser.UserId);
                        return new StatusCodeResult((int)System.Net.HttpStatusCode.BadRequest);
                    }

                    var contactsTextContent = content.TextContent.ToList();

                    manageUserContent = new ManageUser();

                    manageUserContent.ManageUserTitle = CMS.GetWidgetContent(contactsTextContent, tabKey, widgetType, "title");
                    manageUserContent.Name = CMS.GetWidgetContent(contactsTextContent, tabKey, widgetType, "namelabel");
                    manageUserContent.UserName = CMS.GetWidgetContent(contactsTextContent, tabKey, widgetType, "usernamelabel");
                    manageUserContent.Password = CMS.GetWidgetContent(contactsTextContent, tabKey, widgetType, "passwordlabel");
                    manageUserContent.Administrator = CMS.GetWidgetContent(contactsTextContent, tabKey, widgetType, "adminlabel");
                    manageUserContent.Status = CMS.GetWidgetContent(contactsTextContent, tabKey, widgetType, "statuslabel");
                    manageUserContent.FailedLoginCount = CMS.GetWidgetContent(contactsTextContent, tabKey, widgetType, "failedlogincount");
                    manageUserContent.Edit = CMS.GetWidgetContent(contactsTextContent, tabKey, widgetType, "editbutton");
                    manageUserContent.Disable = CMS.GetWidgetContent(contactsTextContent, tabKey, widgetType, "disablebutton");
                    manageUserContent.EnableUser = CMS.GetWidgetContent(contactsTextContent, tabKey, widgetType, "enableuser");

                    manageUserContent.DisableUserMessage= CMS.GetWidgetContent(contactsTextContent, tabKey, widgetType, "disableusermessage");
                    manageUserContent.EnableUserMessage = CMS.GetWidgetContent(contactsTextContent, tabKey, widgetType, "enableusermessage");

                    return new OkObjectResult(manageUserContent);


                }
                catch (Exception ex)
                {
                    Log(LogLevel.Error, "GetManageuserContent returned error " + ex.Message, loginUser.UserId);
                    return new StatusCodeResult((int)System.Net.HttpStatusCode.BadGateway);

                }

            }
            else
            {
                Log(LogLevel.Error, "GetManageuserContent returned HttpStatusCode.Forbidden", "");
                return new StatusCodeResult((int)System.Net.HttpStatusCode.Forbidden);
            }
        }

        [Route("EditUserContent")]
        [HttpGet]
        [DecryptFilter]
        [ResponseCache(CacheProfileName = "Default")]
        public IActionResult GetEditUserContent(string locale, string tabKey, string widgetType)
        {
            var loginUser = QueryStringHelper.GetLoginuser(this.HttpContext);
            if (loginUser != null)
            {
                try
                {
                    EditUser editUserContent = null;
                    Content content = null;
                    content = GetContent(loginUser.ClientId, locale, loginUser.UserId);
                    if (content == null)
                    {
                        Log(LogLevel.Error, "GetEditUserContent: AcceptConnection returned HttpStatusCode.BadRequest - " + content, loginUser.UserId);
                        return new StatusCodeResult((int)System.Net.HttpStatusCode.BadRequest);
                    }

                    var contactsTextContent = content.TextContent.ToList();

                    editUserContent = new EditUser();

                    editUserContent.EditUserTitle = CMS.GetWidgetContent(contactsTextContent, tabKey, widgetType, "title");
                    editUserContent.FirstName = CMS.GetWidgetContent(contactsTextContent, tabKey, widgetType, "firstnamelabel");
                    editUserContent.LastName = CMS.GetWidgetContent(contactsTextContent, tabKey, widgetType, "lastnamelabel");
                    editUserContent.UserName = CMS.GetWidgetContent(contactsTextContent, tabKey, widgetType, "usernamelabel");
                    editUserContent.Password = CMS.GetWidgetContent(contactsTextContent, tabKey, widgetType, "passwordlabel");
                    editUserContent.Administrator = CMS.GetWidgetContent(contactsTextContent, tabKey, widgetType, "adminlabel");
                    editUserContent.Status = CMS.GetWidgetContent(contactsTextContent, tabKey, widgetType, "statuslabel");
                    editUserContent.FailedLoginCount = CMS.GetWidgetContent(contactsTextContent, tabKey, widgetType, "failedlogincount");
                    editUserContent.Save = CMS.GetWidgetContent(contactsTextContent, tabKey, widgetType, "savebutton");
                    editUserContent.Cancel = CMS.GetWidgetContent(contactsTextContent, tabKey, widgetType, "cancelbutton");

                    return new OkObjectResult(editUserContent);


                }
                catch (Exception ex)
                {
                    Log(LogLevel.Error, "GetEditUserContent returned error " + ex.Message, loginUser.UserId);
                    return new StatusCodeResult((int)System.Net.HttpStatusCode.BadGateway);

                }

            }
            else
            {
                Log(LogLevel.Error, "GetEditUserContent returned HttpStatusCode.Forbidden", "");
                return new StatusCodeResult((int)System.Net.HttpStatusCode.Forbidden);
            }
        }


        [Route("UserProfileContent")]
        [HttpGet]
        [DecryptFilter]
        [ResponseCache(CacheProfileName = "Default")]
        public IActionResult GetUserProfileContent(string locale, string tabKey, string widgetType)
        {
            var loginUser = QueryStringHelper.GetLoginuser(this.HttpContext);
            if (loginUser != null)
            {
                try
                {
                    UserProfile userProfileContent = null;
                    Content content = null;

                    content = GetContent(loginUser.ClientId, locale, loginUser.UserId);
                    if (content == null)
                    {
                        Log(LogLevel.Error, "GetUserProfileContent: AcceptConnection returned HttpStatusCode.BadRequest - " + content, loginUser.UserId);
                        return new StatusCodeResult((int)System.Net.HttpStatusCode.BadRequest);
                    }

                    var contactsTextContent = content.TextContent.ToList();

                    userProfileContent = new UserProfile();

                    userProfileContent.UserProfileTitle = CMS.GetWidgetContent(contactsTextContent, tabKey, widgetType, "title");
                    userProfileContent.Name = CMS.GetWidgetContent(contactsTextContent, tabKey, widgetType, "namelabel");
                    userProfileContent.UserName = CMS.GetWidgetContent(contactsTextContent, tabKey, widgetType, "usernamelabel");
                    userProfileContent.UserName = CMS.GetWidgetContent(contactsTextContent, tabKey, widgetType, "adminlabel");
                    userProfileContent.Administrator = CMS.GetWidgetContent(contactsTextContent, tabKey, widgetType, "statuslabel");
                    userProfileContent.Status = CMS.GetWidgetContent(contactsTextContent, tabKey, widgetType, "failedlogincount");

                    return new OkObjectResult(userProfileContent);


                }
                catch (Exception ex)
                {
                    Log(LogLevel.Error, "GetUserProfileContent returned error " + ex.Message, loginUser.UserId);
                    return new StatusCodeResult((int)System.Net.HttpStatusCode.BadGateway);

                }

            }
            else
            {
                Log(LogLevel.Error, "GetUserProfileContent returned HttpStatusCode.Forbidden", "");
                return new StatusCodeResult((int)System.Net.HttpStatusCode.Forbidden);
            }
        }


        [Route("UserMenuContent")]
        [HttpGet]
        [DecryptFilter]
        [ResponseCache(CacheProfileName = "Default")]
        public IActionResult GetUserMenuContent(string locale, string tabKey, string widgetType)
        {
            var loginUser = QueryStringHelper.GetLoginuser(this.HttpContext);
            if (loginUser != null)
            {
                try
                {
                    UserMenu userMenuContent = null;
                    Content content = null;
                    content = GetContent(loginUser.ClientId, locale, loginUser.UserId);
                    if (content == null)
                    {
                        Log(LogLevel.Error, "GetUserMenuContent: AcceptConnection returned HttpStatusCode.BadRequest - " + content, loginUser.UserId);
                        return new StatusCodeResult((int)System.Net.HttpStatusCode.BadRequest);
                    }
                    var contactsTextContent = content.TextContent.ToList();

                    userMenuContent = new UserMenu()
                    {
                        UserMenuSalutation = CMS.GetWidgetContent(contactsTextContent, tabKey, widgetType, "salutation"),
                        ViewProfile = CMS.GetWidgetContent(contactsTextContent, tabKey, widgetType, "viewprofile"),
                        ManageUsers = CMS.GetWidgetContent(contactsTextContent, tabKey, widgetType, "manageusers"),
                        Logout = CMS.GetWidgetContent(contactsTextContent, tabKey, widgetType, "logout")
                    };
                    return new OkObjectResult(userMenuContent);


                }
                catch (Exception ex)
                {
                    Log(LogLevel.Error, "GetUserMenuContent returned error " + ex.Message, loginUser.UserId);
                    return new StatusCodeResult((int)System.Net.HttpStatusCode.BadGateway);
                }

            }
            else
            {
                Log(LogLevel.Error, "GetUserMenuContent returned HttpStatusCode.Forbidden", "");
                return new StatusCodeResult((int)System.Net.HttpStatusCode.Forbidden);
            }
        }

        [Route("ConfigurationContent")]
        [HttpGet]
        [DecryptFilter]
        [ResponseCache(CacheProfileName = "Default")]
        public IActionResult GetConfigurationContent(string locale, string tabKey, string widgetType)
        {
            var loginUser = QueryStringHelper.GetLoginuser(this.HttpContext);
            if (loginUser != null)
            {
                try
                {
                    UserMenu userMenuContent = null;
                    List<Configuration> content = null;
                    content = GetConfiguration(loginUser.ClientId, locale, loginUser.UserId);
                    if (content == null)
                    {
                        Log(LogLevel.Error, "ConfigurationContent: AcceptConnection returned HttpStatusCode.BadRequest - " + content, loginUser.UserId);
                        return new StatusCodeResult((int)System.Net.HttpStatusCode.BadRequest);
                    }
                    
                    return new OkObjectResult(content.Select(x => x.value).ToList());


                }
                catch (Exception ex)
                {
                    Log(LogLevel.Error, "ConfigurationContent returned error " + ex.Message, loginUser.UserId);
                    return new StatusCodeResult((int)System.Net.HttpStatusCode.BadGateway);
                }

            }
            else
            {
                Log(LogLevel.Error, "ConfigurationContent returned HttpStatusCode.Forbidden", "");
                return new StatusCodeResult((int)System.Net.HttpStatusCode.Forbidden);
            }
        }

        [Route("ConfigurationContentForReport")]
        [HttpGet]
        [DecryptFilter]
        [ResponseCache(CacheProfileName = "Default")]
        public IActionResult GetConfigurationContentForReport(string locale, string tabKey, string widgetType)
        {
            var loginUser = QueryStringHelper.GetLoginuser(this.HttpContext);
            if (loginUser != null)
            {
                try
                {
                    UserMenu userMenuContent = null;
                    List<ConfigurationBulk> content = null;
                    content = GetConfigurationForReport(loginUser.ClientId, locale, loginUser.UserId);
                    if (content == null)
                    {
                        Log(LogLevel.Error, "ConfigurationContentForReport: AcceptConnection returned HttpStatusCode.BadRequest - " + content, loginUser.UserId);
                        return new StatusCodeResult((int)System.Net.HttpStatusCode.BadRequest);
                    }

                    return new OkObjectResult(content.Select(x => x.jsonConfiguration).ToList());


                }
                catch (Exception ex)
                {
                    Log(LogLevel.Error, "ConfigurationContentForReport returned error " + ex.Message, loginUser.UserId);
                    return new StatusCodeResult((int)System.Net.HttpStatusCode.BadGateway);
                }

            }
            else
            {
                Log(LogLevel.Error, "ConfigurationContentForReport returned HttpStatusCode.Forbidden", "");
                return new StatusCodeResult((int)System.Net.HttpStatusCode.Forbidden);
            }
        }

        private void Log(LogLevel level, string message, string userId)
        {
            HttpContext?.Session.SetString("UserId", userId);
            switch (level)
            {
                case LogLevel.Error: _logger.LogError(message); break;
                case LogLevel.Critical: _logger.LogCritical(message); break;
                case LogLevel.Warning: _logger.LogWarning(message); break;
                case LogLevel.Information: _logger.LogInformation(message); break;
                case LogLevel.Debug: _logger.LogDebug(message); break;
                case LogLevel.Trace: _logger.LogTrace(message); break;
            }

        }
    }
}
