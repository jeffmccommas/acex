﻿using Microsoft.AspNetCore.Mvc;
using CE.Portals.DataService.Interfaces;
using CE.Portals.Integrations.Models;
using CE.Portals.Integrations.Common;
using Newtonsoft.Json;
using System.Net;
using System.Collections.Generic;
using System;
using CE.Portals.WebApi.Core.Models;
using Microsoft.Extensions.Options;
using CE.Portals.WebApi.Core.Constants;
using Microsoft.Extensions.Logging;
using CE.Portals.WebApi.Core.Helpers;
using CE.Portals.WebApi.Core.Filters;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Cors;
using CE.Portals.DataService.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Caching.Memory;

namespace CE.Portals.WebApi.Core.Controllers
{
    [Route("api/[controller]")]
    public class PortalsApiController : Controller
    {
        private static int _serverCacheTimeOut = 1800;
        private readonly IMemoryCache _memoryCache;
        private readonly ILoginService _loginService;
        private readonly AppSettings _appSettings;
        private readonly ILogger _logger;
        private readonly IEnergyStarServices _energyStarService;
        private readonly JWTIssuer _jwtIssuerOptions;
        public PortalsApiController(ILoginService loginService, IOptions<AppSettings> appSettings,
            ILogger<PortalsApiController> logger, IMemoryCache memoryCache, IEnergyStarServices energyStartServices, IOptions<JWTIssuer> jwtOptions)
        {
            _memoryCache = memoryCache;
            _appSettings = appSettings.Value;
            _loginService = loginService;
            _logger = logger;
            _energyStarService = energyStartServices;
            _jwtIssuerOptions = jwtOptions.Value;
            if (!string.IsNullOrEmpty(_appSettings.ContentsApiServerCacheTimeOutSeconds))
                _serverCacheTimeOut = Convert.ToInt32(_appSettings.ContentsApiServerCacheTimeOutSeconds) / 60;
        }

        [Route("ApplicationConfig")]
        [HttpGet]
        public IActionResult ApplicationConfig()
        {
            var response = new
            {
                Environment = _appSettings.CEENvironment,
                APIBaseUrl = _appSettings.APIBaseURL,
                clientcachetimeout = _appSettings.ContentsApiClientCacheTimeOutSeconds,
                servercachetimeout = _appSettings.ContentsApiServerCacheTimeOutSeconds,
                eventtrackinghub = _appSettings.EventTrackingURL.Split('/').Last()


            };
            return new OkObjectResult(response);
        }

        [Route("RefreshToken")]
        [HttpGet]
        [DecryptFilter]
        [ResponseCache(CacheProfileName = "token")]
        public IActionResult RefreshToken()
        {
            JWTIssuer refreshJwtIssuer = new JWTIssuer();
            refreshJwtIssuer.ValidFor = _jwtIssuerOptions.ValidFor;

            var loginUser = QueryStringHelper.GetLoginuser(this.HttpContext);
            if (loginUser != null)
            {
                var claims = new[]
                             {
                                new Claim(JwtRegisteredClaimNames.Sub, loginUser.UserName),
                                new Claim(JwtRegisteredClaimNames.Iat,
                                          JWTHandler.ToUnixEpochDate(refreshJwtIssuer.IssuedAt).ToString(),
                                          ClaimValueTypes.Integer64),
                                 new Claim(JwtRegisteredClaimNames.Nbf, JWTHandler.ToUnixEpochDate(refreshJwtIssuer.NotBefore).ToString(),
                                          ClaimValueTypes.Integer64),
                                  new Claim(JwtRegisteredClaimNames.Exp, JWTHandler.ToUnixEpochDate(refreshJwtIssuer.Expiration).ToString(),
                                          ClaimValueTypes.Integer64)
                                };

                var jwt = new JwtSecurityToken(
                                                      issuer: _jwtIssuerOptions.Issuer,
                                                      audience: _jwtIssuerOptions.Audience,
                                                      claims: claims,
                                                      notBefore: refreshJwtIssuer.NotBefore,
                                                      expires: refreshJwtIssuer.Expiration,
                                                      signingCredentials: refreshJwtIssuer.SigningCredentials
                                                   );

                var encodedJwt = new JwtSecurityTokenHandler().WriteToken(jwt);

                var response = new
                {
                    AccessToken = encodedJwt,
                    ExpiresIn = (int)_jwtIssuerOptions.ValidFor.TotalSeconds,
                };
                return new OkObjectResult(response);
            }
            else
            {
                Log(LogLevel.Error, "Refresh returned HttpStatusCode.UNAuthorized", "");
                return new UnauthorizedResult();
            }
        }

        [Route("AddUser")]
        [HttpPost]
        [DecryptFilter]
        public IActionResult AddNewUser([FromBody] ApplicationUser appUserDetails)
        {
            var loginUser = QueryStringHelper.GetLoginuser(this.HttpContext);
            if (loginUser != null)
            {
                try
                {
                    var userExists = _loginService.CheckIfUserExists(appUserDetails.UserName);
                    if (userExists)
                        return new StatusCodeResult((int)HttpStatusCode.Conflict);
                    else
                    {
                        var userDetails = new UserDetails()
                        {
                            FirstName = appUserDetails.FirstName,
                            LastName = appUserDetails.LastName,
                            PassWord = appUserDetails.Password,
                            IsAdminUser = appUserDetails.Administrator,
                            ClientId = loginUser.ClientId,
                            UserName = appUserDetails.UserName,
                            Enabled = appUserDetails.Enabled
                        };

                        _loginService.AddNewUser(userDetails, (int)Enum.Parse(typeof(EnvironmentType), _appSettings.CEENvironment));
                        Log(LogLevel.Information, "Added new user [ " + appUserDetails.UserName + " ] by [ " + loginUser.UserName + " ]", loginUser.UserId);
                        return new OkResult();
                    }
                }
                catch (Exception ex)
                {
                    Log(LogLevel.Error, "AddNewUser" + ex.Message, loginUser.UserId);
                    return new StatusCodeResult((int)HttpStatusCode.InternalServerError); ;
                }
            }
            else
            {
                Log(LogLevel.Error, "Refresh returned HttpStatusCode.UNAuthorized", "");
                return new UnauthorizedResult();
            }
        }


        [Route("UserList")]
        [HttpGet]
        [DecryptFilter]
        public IActionResult GetuserList()
        {
            var loginUser = QueryStringHelper.GetLoginuser(this.HttpContext);
            if (loginUser != null)
            {
                try
                {
                    var userList = _loginService.GetUserList(Int32.Parse(loginUser.ClientId));
                    return new OkObjectResult(userList);
                }
                catch (Exception ex)
                {
                    Log(LogLevel.Error, "GetuserList" + ex.Message, loginUser.UserId);
                    return new StatusCodeResult((int)HttpStatusCode.InternalServerError); ;
                }
            }
            else
            {
                Log(LogLevel.Error, "Refresh returned HttpStatusCode.UNAuthorized", "");
                return new UnauthorizedResult();
            }
        }

        [Route("UpdateUserStatus")]
        [HttpPost]
        [DecryptFilter]
        public IActionResult UpdateUserStatus([FromBody] ApplicationUser appUserDetails)
        {
            var loginUser = QueryStringHelper.GetLoginuser(this.HttpContext);
            if (loginUser != null)
            {
                try
                {
                    var user = _loginService.GetUserDetails(appUserDetails.UserId);

                    user.Enabled = appUserDetails.Enabled;
                    if (appUserDetails.Enabled)
                        user.FailedLoginAttempts = 0;

                    _loginService.UpdateUser(user, true);
                    Log(LogLevel.Information, "Updated User Status [ " + user.UserName + " ]", loginUser.UserId);
                    return new OkResult();

                }
                catch (Exception ex)
                {
                    Log(LogLevel.Error, "UpdateUserStatus" + ex.Message, loginUser.UserId);
                    return new StatusCodeResult((int)HttpStatusCode.InternalServerError);
                }
            }
            else
            {
                Log(LogLevel.Error, "Refresh returned HttpStatusCode.UNAuthorized", "");
                return new UnauthorizedResult();
            }
        }

        [Route("GetClientDetails")]
        [HttpGet]
        [DecryptFilter]
        public IActionResult GetClientDetails()
        {
            var loginUser = QueryStringHelper.GetLoginuser(this.HttpContext);
            if (loginUser != null)
            {
                try
                {
                    Log(LogLevel.Information, "GetClientDetails", loginUser.UserId);
                    return new OkObjectResult(loginUser.ClientId);

                }
                catch (Exception ex)
                {
                    Log(LogLevel.Error, "GetClientDetails" + ex.Message, loginUser.UserId);
                    return new StatusCodeResult((int)HttpStatusCode.InternalServerError); ;
                }
            }
            else
            {
                Log(LogLevel.Error, "Refresh returned HttpStatusCode.UNAuthorized", "");
                return new UnauthorizedResult();
            }
        }

        [Route("EditUser")]
        [HttpPost]
        [DecryptFilter]
        public IActionResult UpdateUser([FromBody] ApplicationUser appUserDetails)
        {
            var loginUser = QueryStringHelper.GetLoginuser(this.HttpContext);
            if (loginUser != null)
            {
                try
                {
                    var user = _loginService.GetUserDetails(appUserDetails.UserId);

                    user.FirstName = appUserDetails.FirstName;
                    user.LastName = appUserDetails.LastName;
                    user.FirstName = appUserDetails.FirstName;
                    user.UserName = appUserDetails.UserName;
                    user.PassWord = appUserDetails.Password;
                    user.Enabled = appUserDetails.Enabled;
                    if (appUserDetails.Enabled)
                        user.FailedLoginAttempts = 0;
                    user.IsAdminUser = appUserDetails.Administrator;

                    _loginService.UpdateUser(user, false);
                    Log(LogLevel.Information, "User Updated [ " + user.UserName + " ]", loginUser.UserId);
                    return new OkResult();

                }
                catch (Exception ex)
                {
                    Log(LogLevel.Error, "UpdateUser" + ex.Message, loginUser.UserId);
                    return new StatusCodeResult((int)HttpStatusCode.InternalServerError); ;
                }
            }
            else
            {
                Log(LogLevel.Error, "Refresh returned HttpStatusCode.UNAuthorized", "");
                return new UnauthorizedResult();
            }
        }

        [Route("UserAuth")]
        [HttpPost]
        [EnableCors("AllowOnlyESPMWebsite")]
        [ApiExplorerSettings(IgnoreApi = true)]
        public IActionResult GetUserAuth([FromBody] ApplicationUser applicationuser)
        {
            try
            {
                JWTIssuer refreshJwtIssuer = new JWTIssuer();
                refreshJwtIssuer.ValidFor = _jwtIssuerOptions.ValidFor;
                if (applicationuser.UserName == null) //|| password == null)
                {
                    Log(LogLevel.Error, "GetUserAuth returned HttpStatusCode.BadRequest- No user name provided.", "");
                    return new BadRequestResult();
                }

                if (!_loginService.CheckDbConnection().Contains("Connection Successful"))
                {
                    return new StatusCodeResult((int)HttpStatusCode.InternalServerError);
                }

                UserDetails validUser = null;
                validUser = _loginService.ValidateESPMUser(applicationuser.UserName, applicationuser.Password);

                if (validUser == null)
                {
                    Log(LogLevel.Error, "GetUserAuth returned HttpStatusCode.BadRequest- ValidUser=null.", "");
                    return new StatusCodeResult((int)HttpStatusCode.NotFound);
                }
                else if (validUser.UserId == null)
                {
                    int maxLoginAttempts = int.Parse(_loginService.GetClientAppsettings(validUser.ClientId.ToString(),
                        ApplicationConstants.MAXINCORRECTLOGINS));

                    if (validUser.FailedLoginAttempts >= maxLoginAttempts)
                    {
                        Log(LogLevel.Error, "GetUserAuth returned HttpStatusCode.BadRequest- Max login exceeded", "");
                        return new StatusCodeResult((int)HttpStatusCode.Forbidden);
                    }
                    if (!validUser.Enabled)
                    {
                        Log(LogLevel.Error, "GetUserAuth returned HttpStatusCode.BadRequest- User not enabled", "");
                        return new StatusCodeResult((int)HttpStatusCode.NotFound);

                    }

                    else
                    {
                        Log(LogLevel.Error, "GetUserAuth returned HttpStatusCode.UNAuthorised- No user found", "");
                        return new StatusCodeResult((int)HttpStatusCode.Unauthorized);
                    }
                }
                var claims = new[]
                {
                    new Claim(JwtRegisteredClaimNames.Sub, applicationuser.UserName),
                    new Claim(JwtRegisteredClaimNames.Iat,
                        JWTHandler.ToUnixEpochDate(refreshJwtIssuer.IssuedAt).ToString(),
                        ClaimValueTypes.Integer64),
                    new Claim(JwtRegisteredClaimNames.Nbf,
                        JWTHandler.ToUnixEpochDate(refreshJwtIssuer.NotBefore).ToString(),
                        ClaimValueTypes.Integer64),
                    new Claim(JwtRegisteredClaimNames.Exp,
                        JWTHandler.ToUnixEpochDate(refreshJwtIssuer.Expiration).ToString(),
                        ClaimValueTypes.Integer64)
                };

                var jwt = new JwtSecurityToken(
                    issuer: _jwtIssuerOptions.Issuer,
                    audience: _jwtIssuerOptions.Audience,
                    claims: claims,
                    notBefore: refreshJwtIssuer.NotBefore,
                    expires: refreshJwtIssuer.Expiration,
                    signingCredentials: _jwtIssuerOptions.SigningCredentials
                );

                var encodedJwt = new JwtSecurityTokenHandler().WriteToken(jwt);

                var response = new
                {
                    AccessToken = encodedJwt,
                    ExpiresIn = (int)_jwtIssuerOptions.ValidFor.TotalSeconds,
                    user = FilterHelper.Encrypt(JsonConvert.SerializeObject(validUser))
                };


                return new OkObjectResult(JsonConvert.SerializeObject(response));

            }

            catch (Exception ex)
            {

                Log(LogLevel.Debug, "GetAuthKey returned HttpStatusCode.BadRequest" + ex.Message, "");//set to debug for security
                return new BadRequestObjectResult(ex.Message);
            }


        }
        //private JsonResult GetEspmSecretKey([FromQuery] string clientId)
        //{
        //    try
        //    {
        //        var secretKey = _loginService.GetClientESPMSecretKeys(clientId);
        //        if (secretKey == null)
        //        {

        //            //TODO - Move messages to logs                    
        //            var r = new ResponseModel
        //            {
        //                StatusCode = HttpStatusCode.Unauthorized,
        //                StatusDescription = "No Secret Key Found."
        //            };
        //            return new JsonResult(r);
        //        }

        //        var response = new ResponseModel
        //        {
        //            StatusCode = HttpStatusCode.OK,
        //            StatusDescription = "Success",
        //            Content = secretKey
        //        };

        //        return new JsonResult(response);
        //    }
        //    catch (Exception ex)
        //    {
        //        Log(LogLevel.Error, "GetEspmSecretKey - internal error: " + ex.Message, "");
        //        var r = new ResponseModel
        //        {
        //            StatusCode = HttpStatusCode.InternalServerError,
        //            StatusDescription = "An error occurred while processing your request."
        //        };
        //        return new JsonResult(r);
        //    }
        //}
        

        [HttpGet("UserDetails")]
        [DecryptFilter]
        public IActionResult GetUserDetails([FromQuery] string userId)
        {
            var loginUser = QueryStringHelper.GetLoginuser(this.HttpContext);
            if (loginUser != null)
            {
                userId = userId == null ? loginUser.UserId : userId;
                var user = _loginService.GetUserDetails(Int32.Parse(userId));
                if (user.UserId == loginUser.UserId)
                {
                    user.IsLoggedin = true;
                }
                return new OkObjectResult(user);
            }
            else
            {
                Log(LogLevel.Error, "GetAuthKey returned HttpStatusCode.BadRequest - UserInfo is null", "");
                return new BadRequestObjectResult("GetAuthKey returned HttpStatusCode.BadRequest - UserInfo is null");
            }
        }

        [HttpGet("LoggedInUserProfile")]
        [DecryptFilter]
        public IActionResult GetUserProfile()
        {
            var loginUser = QueryStringHelper.GetLoginuser(this.HttpContext);
            if (loginUser != null)
            {
                var user = _loginService.GetUserProfile(Int32.Parse(loginUser.UserId));
                return new OkObjectResult(user);
            }
            else
            {
                Log(LogLevel.Error, "GetAuthKey returned HttpStatusCode.BadRequest", "");
                return new BadRequestObjectResult("GetAuthKey returned HttpStatusCode.BadRequest");
            }
        }


        [HttpPost("PostEvents")]
        [DecryptFilter]
        public IActionResult Post([FromBody] EventData eventData)
        {
            
            Dictionary<string, string> eventInfo = new Dictionary<string, string>();
            var loginUser = QueryStringHelper.GetLoginuser(this.HttpContext);
            if (loginUser != null)
            {
                eventInfo.Add("EventType", eventData.EventType);
                eventInfo.Add("AccountId", eventData.AccountId);
                eventInfo.Add("PremiseId", eventData.PremiseId);
                eventInfo.Add("ClientId", loginUser.ClientId);
                eventInfo.Add("CustomerId", loginUser.UserId);
                eventInfo.Add("PartitionKey",
                    loginUser.ClientId + "_ESPM_" + eventData.EventType + "_" +
                    DateTime.UtcNow.Date.ToString("MM/dd/yyyy").Replace("/", "-"));
                eventInfo.Add("RowKey", loginUser.UserId + "_" + DateTime.UtcNow.Ticks);
                eventInfo.Add("DateCreated", DateTime.UtcNow.Date.ToString("MM/dd/yyyy"));

                // ThreadPool.QueueUserWorkItem(delegate {
                Task.Factory.StartNew(() => PostLogToEventHubAsync(JsonConvert.SerializeObject(eventInfo), loginUser.ClientId,
                    eventData.Locale));
                //});
                //RunOperationAsync();


                return new OkObjectResult("");

            }

            Log(LogLevel.Error, "PostEvent returned HttpStatusCode.BadRequest", "");
            return new BadRequestResult();

        }
      
        private async Task<int> PostLogToEventHubAsync(string eventInfo, string clientId, string locale)
        {
            try
            {
                var rm = new WebAPIResponseModel(_appSettings, _logger, _memoryCache);
                Json(JsonConvert.SerializeObject(rm.PostEvents(eventInfo,
                    locale, int.Parse(clientId))));
            }
            catch (Exception ex)
            {
                Log(LogLevel.Error, "PostLogToEventHub returned exception " + ex.Message, "");
            }
            return 1;
        }

        [HttpPost("TraceLog")]
        [DecryptFilter]
        public IActionResult TraceLog([FromBody] TraceLog traceLog)
        {
            var loginUser = QueryStringHelper.GetLoginuser(this.HttpContext);
            if (loginUser != null)
            {
                switch (traceLog.LogLevel)
                {
                    case "debug": Log(LogLevel.Debug, traceLog.LogMessage, loginUser.UserId); break;
                    case "error": Log(LogLevel.Error, traceLog.LogMessage, loginUser.UserId); break;
                    case "warn": Log(LogLevel.Warning, traceLog.LogMessage, loginUser.UserId); break;
                    case "critical": Log(LogLevel.Critical, traceLog.LogMessage, loginUser.UserId); break;
                    case "info": Log(LogLevel.Information, traceLog.LogMessage, loginUser.UserId); break;
                    case "trace": Log(LogLevel.Trace, traceLog.LogMessage, loginUser.UserId); break;
                }
                return new OkObjectResult(traceLog.LogMessage);
            }
            else
            {
                Log(LogLevel.Error, "TraceLog returned HttpStatusCode.BadRequest", "");
                return new BadRequestResult();
            }
        }

        private void Log(LogLevel level, string message, string userId)
        {
            HttpContext?.Session.SetString("UserId", userId);

            switch (level)
            {
                case LogLevel.Error: _logger.LogError(message); break;
                case LogLevel.Critical: _logger.LogCritical(message); break;
                case LogLevel.Warning: _logger.LogWarning(message); break;
                case LogLevel.Information: _logger.LogInformation(message); break;
                case LogLevel.Debug: _logger.LogDebug(message); break;
                case LogLevel.Trace: _logger.LogTrace(message); break;
            }

        }

    }
}
