﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using CE.InsightDataAccess.InsightsDWModels;
using CE.Portals.DataService.Interfaces;
using CE.Portals.WebApi.Core.Filters;
using CE.Portals.WebApi.Core.Helpers;
using CE.Portals.WebApi.Core.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace CE.Portals.WebApi.Core.Controllers
{
    [Route("api/[controller]")]
    public class PortalsApiMapController : Controller
    {
        private readonly IMemoryCache _memoryCache;
        private readonly AppSettings _appSettings;
        private static int _serverCacheTimeOut = 1800;
        private HttpMessageHandler _httpMessagehandler;
        private readonly ILogger _logger;
        private readonly IConfigurationService _configurationService;
        private readonly IMappingService _mappingService;
        private readonly ILoginService _loginService;
        public PortalsApiMapController(IOptions<AppSettings> appsettings, ILogger<PortalsApiMapController> logger,
            IMemoryCache memoryCache, IConfigurationService configurationService, IMappingService mappingService, ILoginService loginService,
            HttpMessageHandler httpMessagehandler = null)
        {
            _memoryCache = memoryCache;
            _logger = logger;
            _appSettings = appsettings.Value;
            if (!string.IsNullOrEmpty(_appSettings.ContentsApiServerCacheTimeOutSeconds))
                _serverCacheTimeOut = Convert.ToInt32(_appSettings.ContentsApiServerCacheTimeOutSeconds)/60;
            _httpMessagehandler = httpMessagehandler;
            _configurationService = configurationService;
            _mappingService = mappingService;
            _loginService = loginService;
        }

        [Route("FindMap")]
        [HttpGet]
        [DecryptFilter]
        public IActionResult FindMap(string accountId, string premiseId, string meterId)
        {
            try
            {
                var loginUser = QueryStringHelper.GetLoginuser(this.HttpContext);
                if (loginUser != null)
                {
                    var shardName = GetSharedName(Int32.Parse(loginUser.ClientId));
                    var result = _mappingService.GetMappedMeters(shardName, accountId, premiseId, meterId);
                    return new OkObjectResult(result);
                }
                else
                {
                    Log(LogLevel.Error, "FindMap returned HttpStatusCode.UNAuthorized", loginUser.UserId);
                    return new UnauthorizedResult();
                }
            }
            catch (Exception exp)
            {
                Log(LogLevel.Error, "FindMap internal error " + exp.Message, "");
                return StatusCode(500);
            }
        }

        [Route("IsMapped")]
        [HttpGet]
        [DecryptFilter]
        public IActionResult IsMapped(string meterId, string accountId, string premiseId, string meterType)
        {
            var loginUser = QueryStringHelper.GetLoginuser(this.HttpContext);
            if (loginUser != null)
            {
                var shardName = GetSharedName(Int32.Parse(loginUser.ClientId));
                var result = _mappingService.IsMapped(shardName, meterId, accountId, premiseId, meterType,Int32.Parse(loginUser.UserId));
                return new OkObjectResult(result);
            }
            else
            {
                Log(LogLevel.Error, "FindMap returned HttpStatusCode.UNAuthorized", "");
                return new UnauthorizedResult();
            }
        }

        [Route("IsAllMapped")]
        [HttpPost]
        [DecryptFilter]
        public IActionResult IsAllMapped([FromBody] List<Meter> pendingMeters)
        {

            var loginUser = QueryStringHelper.GetLoginuser(this.HttpContext);
            if (loginUser != null)
            {
                var shardName = GetSharedName(Int32.Parse(loginUser.ClientId));
                foreach (var meter in pendingMeters)
                {
                    var result = _mappingService.IsMapped(shardName, meter.meterId, meter.customFieldList.customField[0].Text,
                        meter.customFieldList.customField[1].Text, meter.meterInfo.type,
                        Int32.Parse(loginUser.UserId));
                    if (result)
                    {
                        meter.isMapped = true;
                    }
                    else
                    {
                        meter.isMapped = false;
                    }
                }
                return new OkObjectResult(pendingMeters);
            }
            else
            {
                Log(LogLevel.Error, "FindMap returned HttpStatusCode.UNAuthorized", "");
                return new UnauthorizedResult();
            }
            return new OkObjectResult("");
        }

        [Route("IsMeterObjectMapped")]
        [HttpPost]
        [DecryptFilter]
        public IActionResult IsMeterObjectMapped([FromBody] Meter pendingMeter)
        {
            var loginUser = QueryStringHelper.GetLoginuser(this.HttpContext);
            if (loginUser != null)
            {
                var shardName = GetSharedName(Int32.Parse(loginUser.ClientId));

                var result = _mappingService.IsMapped(shardName, pendingMeter.meterId, pendingMeter.customFieldList.customField[0].Text,
                    pendingMeter.customFieldList.customField[1].Text, pendingMeter.meterInfo.type,
                    Int32.Parse(loginUser.UserId));
                if (result)
                {
                    pendingMeter.isMapped = true;
                }
                else
                {
                    pendingMeter.isMapped = false;
                }

                return new OkObjectResult(pendingMeter);
            }
            else
            {
                Log(LogLevel.Error, "FindMap returned HttpStatusCode.UNAuthorized", "");
                return new UnauthorizedResult();
            }
        }


        private string GetSharedName(int clientId)
        {
            string servercacheKey = "shardname-" + clientId;
            string shardname = string.Empty;
            if (this._memoryCache.TryGetValue(servercacheKey, out shardname))
            {
                return shardname;
            }
            else
            {
                shardname = _configurationService.GetDwDatabaseConfigValue(clientId, _appSettings.CEENvironment,
                    "espmbenchmark.shardname");

                this._memoryCache.Set(servercacheKey, shardname, new MemoryCacheEntryOptions { AbsoluteExpirationRelativeToNow = (DateTime.Now.AddMinutes(_serverCacheTimeOut) - DateTime.Now) });
                return shardname;
            }
        }

        [Route("MappedCustomerInfo")]
        [HttpGet]
        [DecryptFilter]
        public IActionResult GetMappedCustomerInformation(string accountId, string premiseId)
        {
            var loginUser = QueryStringHelper.GetLoginuser(this.HttpContext);
            if (loginUser != null)
            {
                try
                {

                    var shardName = GetSharedName(Int32.Parse(loginUser.ClientId));
                    var mappedCustomer = _mappingService.GetMappedCustomerDetails(shardName, accountId, premiseId);
                    return new OkObjectResult(mappedCustomer);
                }
                catch (Exception ex)
                {
                    Log(LogLevel.Error, "GetMappedCustomerInformation returned HttpStatusCode.InternalServerError- " + ex.Message, loginUser.UserId);
                    return new StatusCodeResult((int)HttpStatusCode.InternalServerError);

                }
            }
            else
            {
                Log(LogLevel.Error, "GetMappedCustomerInformation returned HttpStatusCode.UNAuthorized", "");
                return new UnauthorizedResult();
            }

        }

        [Route("AddMappedMeter")]
        [HttpPost]
        [DecryptFilter]
        public IActionResult AddMappedMeter([FromBody] Espmmap espmMap)
        {
            var loginUser = QueryStringHelper.GetLoginuser(this.HttpContext);
            if (loginUser != null)
            {
                try
                {
                    string mappingdetailsaccountlist = String.Empty;
                    string mappingdetailspremiselist = String.Empty;
                    if (espmMap?.EspmMapDetails != null)
                    {
                        if (espmMap.EspmMapDetails.Any())
                        {
                            mappingdetailsaccountlist =
                                string.Join(",", (from x in espmMap.EspmMapDetails select x.MapAccountId).ToList());
                            mappingdetailspremiselist =
                                string.Join(",", (from x in espmMap.EspmMapDetails select x.MapPremiseId).ToList());
                        }
                    }

                    Log(LogLevel.Information, $"Saving Meter Map changes. AccountId: {espmMap.AccountId} PremiseId: {espmMap.PremiseId} MeterId: {espmMap.MeterId} Details Accounts: {mappingdetailsaccountlist} Details Premises: {mappingdetailspremiselist}", loginUser.UserId);

                    var shardName = GetSharedName(Int32.Parse(loginUser.ClientId));
                    espmMap.UpdatedBy = int.Parse(loginUser.UserId);
                    espmMap.UpdatedDate = DateTime.Now;

                    foreach (EspmMapDetails mapDetails in espmMap.EspmMapDetails)
                    {
                        mapDetails.UpdatedBy = int.Parse(loginUser.UserId);
                        mapDetails.UpdatedDate = DateTime.Now;
                        mapDetails.Enabled = true;
                    }
                    _mappingService.AddMapping(shardName, espmMap);
                    return new OkResult();
                }
                catch (Exception ex)
                {
                    Log(LogLevel.Error, "AddMappedMeter returned HttpStatusCode.InternalServerError- " + ex.Message, loginUser.UserId);
                    return new StatusCodeResult((int)HttpStatusCode.InternalServerError);

                }
            }
            else
            {
                Log(LogLevel.Error, "AddMappedMeter returned HttpStatusCode.UNAuthorized", "");
                return new UnauthorizedResult();
            }

        }


        [Route("UpdateCustomerId")]
        [HttpPost]
        [DecryptFilter]
        public IActionResult UpdateCustomerId([FromBody] string meterId)
        {
            var loginUser = QueryStringHelper.GetLoginuser(this.HttpContext);
            if (loginUser != null)
            {
                try
                {

                    Log(LogLevel.Information, $"UpdateCustomerId called. MeterId {meterId}", loginUser.UserId);

                    var shardName = GetSharedName(Int32.Parse(loginUser.ClientId));
                    var meterIds = meterId.Split('|');
                    Boolean success = false;
                    foreach (var meter in meterIds)
                    {
                        Log(LogLevel.Information, "UpdateCustomerId for meter ID - " + meter, loginUser.UserId);
                        success = _mappingService.UpdateCustomerId(shardName, meter);
                    }
                    if (success)
                        return new OkResult();
                    return new StatusCodeResult((int)HttpStatusCode.InternalServerError);
                }
                catch (Exception ex)
                {
                    Log(LogLevel.Error, "UpdateCustomerId returned HttpStatusCode.InternalServerError- " + ex.Message, loginUser.UserId);
                    return new StatusCodeResult((int)HttpStatusCode.InternalServerError);

                }
            }
            else
            {
                Log(LogLevel.Error, "UpdateCustomerId returned HttpStatusCode.UNAuthorized", "");
                return new UnauthorizedResult();
            }

        }

        [Route("GetBillingData")]
        [HttpGet]
        [DecryptFilter]
        public IActionResult GetBillingdata([FromQuery] string accountId, string customerId, string startdate, string endDate, string premiseId, string count)
        {
            var loginUser = QueryStringHelper.GetLoginuser(this.HttpContext);
            if (loginUser != null)
            {
                try
                {
                    var parameters = string.Empty;
                    Log(LogLevel.Information, $"GetBillingdata called. accountId: {accountId} customerId:{customerId} startdate {startdate} endDate: {endDate}", loginUser.UserId);
                    var webApiResponseModel = new WebAPIResponseModel(_appSettings, _logger, _memoryCache, _loginService);
                    webApiResponseModel.clientId = loginUser.ClientId;
                    //webApiResponseModel.clientId = "87";
                    if (!string.IsNullOrEmpty(accountId))
                    {
                        parameters = parameters + "&AccountId=" + accountId;
                    }

                    if (!string.IsNullOrEmpty(customerId))
                    {
                        parameters = parameters + "&CustomerId=" + customerId;
                    }
                    if (!string.IsNullOrEmpty(startdate))
                    {
                        parameters = parameters + "&StartDate=" + startdate;
                    }
                    if (!string.IsNullOrEmpty(endDate))
                    {
                        parameters = parameters + "&EndDate=" + endDate;
                    }
                    if (!string.IsNullOrEmpty(premiseId))
                    {
                        parameters = parameters + "&PremiseId=" + premiseId;
                    }
                    if (!string.IsNullOrEmpty(count))
                    {
                        parameters = parameters + "&Count=" + count;
                    }
                    var response = webApiResponseModel.GetBillingData(parameters, Integrations.Models.PortalWebAPIType.InsightsApi, "en-US");
                    if (response.StatusCode == System.Net.HttpStatusCode.OK)
                    {
                        return new OkObjectResult(response);
                    }
                    else if (response.StatusCode == System.Net.HttpStatusCode.BadRequest)
                    {
                        Log(LogLevel.Error, "GetBillingdata returned HttpStatusCode.BadRequest - " + response.Content, loginUser.UserId);
                        return new BadRequestObjectResult(response.Content);

                    }
                    else
                    {
                        Log(LogLevel.Error, "GetBillingdata returned HttpStatusCode.Forbidden -" + response.Content, loginUser.UserId);
                        return new StatusCodeResult((int)System.Net.HttpStatusCode.Forbidden);
                    }

                }
                catch (Exception ex)
                {
                    Log(LogLevel.Error, "GetBillingdata returned HttpStatusCode.InternalServerError" + ex.Message, loginUser.UserId);
                    return new StatusCodeResult((int)HttpStatusCode.InternalServerError);

                }
            }
            else
            {
                Log(LogLevel.Error, "GetBillingdata HttpStatusCode.UNAuthorized", "");
                return new UnauthorizedResult();
            }

        }


        [Route("GetMappedAccountsForMeters")]
        [HttpGet]
        [DecryptFilter]
        public IActionResult GetMappedAccountsForMeters([FromQuery] string meterId)
        {
            var loginUser = QueryStringHelper.GetLoginuser(this.HttpContext);
            if (loginUser != null)
            {
                try
                {
                    var shardName = GetSharedName(Int32.Parse(loginUser.ClientId));
                    Log(LogLevel.Information, $"GetMappedAccountsForMeters called. meterId: {meterId}", loginUser.UserId);
                    var espmMapDetails = _mappingService.GetMappedAccountForMeters(shardName, meterId);
                    if (espmMapDetails != null)
                    {
                        return new OkObjectResult(espmMapDetails);
                    }
                    return new StatusCodeResult((int)HttpStatusCode.InternalServerError);

                }
                catch (Exception ex)
                {
                    Log(LogLevel.Error, "GetBillingdata returned HttpStatusCode.InternalServerError" + ex.Message, loginUser.UserId);
                    return new StatusCodeResult((int)HttpStatusCode.InternalServerError);

                }
            }
            else
            {
                Log(LogLevel.Error, "GetMappedAccountsForMeters HttpStatusCode.UNAuthorized", "");
                return new UnauthorizedResult();
            }

        }


        [Route("SaveBillingData")]
        [HttpPost]
        [DecryptFilter]
        public IActionResult SaveBillingData([FromBody] List<MeterBillingData> meterBillignData)
        {
            var loginUser = QueryStringHelper.GetLoginuser(this.HttpContext);
            if (loginUser != null)
            {
                try
                {

                    _logger.LogWarning($"SaveBillingData called.");

                    var shardName = GetSharedName(Int32.Parse(loginUser.ClientId));
                    Log(LogLevel.Information, $"SaveBillingData called. meterBillingData: {meterBillignData}", loginUser.UserId);
                    meterBillignData.ForEach(x => 
                    { x.UpdatedBy = Int32.Parse(loginUser.UserId);
                        x.UpdatedDate = DateTime.Now;
                    });
                    var sucess = _mappingService.AddMeterBillignData(shardName, meterBillignData);
                    if (sucess)
                        return new OkResult();
                    return new StatusCodeResult((int)HttpStatusCode.InternalServerError);
                }
                catch (Exception ex)
                {
                    Log(LogLevel.Error, "SaveBillingData returned HttpStatusCode.InternalServerError" + ex.Message, loginUser.UserId);
                    return new StatusCodeResult((int)HttpStatusCode.InternalServerError);

                }
            }
            else
            {
                Log(LogLevel.Error, "SaveBillingData HttpStatusCode.UNAuthorized", "");
                return new UnauthorizedResult();
            }

        }

        [Route("DeleteBillingDataWebJob")]
        [HttpPost]
        [DecryptFilter]
        public IActionResult DeleteBillingDataWebJob()
        {
            var loginUser = QueryStringHelper.GetLoginuser(this.HttpContext);
            if (loginUser != null)
            {
                try
                {

                    Log(LogLevel.Information, $"DeleteBillingDataWebJob called.", loginUser.UserId);

                    var shardName = GetSharedName(Int32.Parse(loginUser.ClientId));
                    var sucess = _mappingService.DeleteBillingDataWebJob(shardName);
                    if (sucess)
                        return new OkResult();
                    return new StatusCodeResult((int)HttpStatusCode.InternalServerError);
                }
                catch (Exception ex)
                {
                    Log(LogLevel.Error, "DeleteBillingDataWebJob returned HttpStatusCode.InternalServerError- " + ex.Message, loginUser.UserId);
                    return new StatusCodeResult((int)HttpStatusCode.InternalServerError);

                }
            }
            else
            {
                Log(LogLevel.Error, "DeleteBillingDataWebJob returned HttpStatusCode.UNAuthorized", "");

                return new UnauthorizedResult();
            }

        }


        [Route("GetAllMeterBillingForMeter")]
        [HttpGet]
        [DecryptFilter]
        public IActionResult GetAllMeterBillingforMeter([FromQuery] string meterId, string startdate, string endDate)
        {
            var loginUser = QueryStringHelper.GetLoginuser(this.HttpContext);
            if (loginUser != null)
            {
                try
                {

                    Log(LogLevel.Information, $"GetAllMeterBilling called.", loginUser.UserId);

                    var shardName = GetSharedName(Int32.Parse(loginUser.ClientId));
                    var meterBillingData = _mappingService.GetAllMeterBillingforMeter(shardName,meterId, startdate, endDate);
                    Log(LogLevel.Information, "GetAllMeterBilling returned HttpStatusCode.OK- " + JsonConvert.SerializeObject(meterBillingData), loginUser.UserId);
                    return new OkObjectResult(meterBillingData);

                   
                }
                catch (Exception ex)
                {
                    Log(LogLevel.Error, "GetAllMeterBilling returned HttpStatusCode.InternalServerError- " + ex.Message, loginUser.UserId);
                    return new StatusCodeResult((int)HttpStatusCode.InternalServerError);

                }
            }
            else
            {
                Log(LogLevel.Error, "GetAllMeterBilling returned HttpStatusCode.UNAuthorized", "");

                return new UnauthorizedResult();
            }

        }

        [Route("GetAllMeterIds")]
        [HttpGet]
        [DecryptFilter]
        public IActionResult GetAllMeterIds()
        {
            var loginUser = QueryStringHelper.GetLoginuser(this.HttpContext);
            if (loginUser != null)
            {
                try
                {

                    Log(LogLevel.Information, $"GetAllMeterIds called.", loginUser.UserId);

                    var shardName = GetSharedName(Int32.Parse(loginUser.ClientId));
                    var meterIds = _mappingService.GetAllMeterIds(shardName);
                    if (meterIds != null)
                        return new OkObjectResult(meterIds);
                    return new StatusCodeResult((int)HttpStatusCode.InternalServerError);
                }
                catch (Exception ex)
                {
                    Log(LogLevel.Error, "GetAllMeterIds returned HttpStatusCode.InternalServerError- " + ex.Message, loginUser.UserId);
                    return new StatusCodeResult((int)HttpStatusCode.InternalServerError);

                }
            }
            else
            {
                Log(LogLevel.Error, "GetAllMeterIds returned HttpStatusCode.UNAuthorized", "");

                return new UnauthorizedResult();
            }

        }

        private void Log(LogLevel level, string message, string userId)
        {
            HttpContext?.Session.SetString("UserId", userId);

            switch (level)
            {
                case LogLevel.Error: _logger.LogError(message); break;
                case LogLevel.Critical: _logger.LogCritical(message); break;
                case LogLevel.Warning: _logger.LogWarning(message); break;
                case LogLevel.Information: _logger.LogInformation(message); break;
                case LogLevel.Debug: _logger.LogDebug(message); break;
                case LogLevel.Trace: _logger.LogTrace(message); break;
            }

        }
    }
}
