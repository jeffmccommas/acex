﻿using CE.Portals.WebApi.Core.Filters;
using CE.Portals.WebApi.Core.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using System;
using CE.Portals.WebApi.Core.Helpers;
using System.Net.Http;
using Microsoft.Extensions.Logging;
using System.Collections.Generic;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using CE.Portals.DataService.Interfaces;
using System.Linq;
using CE.InsightDataAccess.InsightsDWModels;
using CE.Portals.Integrations.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Caching.Memory;


// For more information on enabling MVC for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace CE.Portals.WebApi.Core.Controllers
{
    [Route("api/[controller]")]
    public class EnergyStarController : Controller
    {
        //private readonly EnergyStarSettings _energyStarSettings;
        private readonly IMemoryCache _memoryCache;
        private static int _serverCacheTimeOut = 1800;
        public readonly AppSettings _appSettings;
        private readonly HttpMessageHandler _httpMessagehandler;
        private readonly ILogger _logger;
        private readonly IEnergyStarServices _energyStarService;
        private readonly IMappingService _mappingService;
        private readonly IConfigurationService _configurationService;
        private readonly ILoginService _loginService;
        public EnergyStarController(IOptions<AppSettings> appsettings, 
            ILogger<EnergyStarController> logger, 
            IEnergyStarServices energyStartServices, IMappingService mappingService,
            IConfigurationService configurationService,
            IMemoryCache memoryCache, ILoginService loginService,
            HttpMessageHandler httpMessagehandler = null)
        {
            _memoryCache = memoryCache;
            _logger = logger;
            _appSettings = appsettings.Value;
            _httpMessagehandler = httpMessagehandler;
            _energyStarService = energyStartServices;
            _mappingService = mappingService;
            _configurationService = configurationService;
            _loginService = loginService;
            _appSettings = appsettings.Value;
            if (!string.IsNullOrEmpty(_appSettings.ContentsApiServerCacheTimeOutSeconds))
                _serverCacheTimeOut = Convert.ToInt32(_appSettings.ContentsApiServerCacheTimeOutSeconds)/60;

        }
        [Route("AcceptedAccountsList")]
        [HttpGet]
        [DecryptFilter]
        public IActionResult GetAcceptedAccounts()
        {
            var loginUser = QueryStringHelper.GetLoginuser(this.HttpContext);
            if (loginUser != null)
            {
                var webApiResponseModel = new WebAPIResponseModel(_appSettings, _logger, _memoryCache, _loginService);
                webApiResponseModel.clientId = loginUser.ClientId;
                string parameters = String.Empty;
                var response = webApiResponseModel.GetAcceptedAccountsList(Integrations.Models.PortalWebAPIType.ESPMApi, "en_US", _httpMessagehandler);
                if (response.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    return new OkObjectResult(response.Content);
                }
                else if (response.StatusCode == System.Net.HttpStatusCode.BadRequest)
                {
                    Log(LogLevel.Error, "GetAcceptedAccounts returned HttpStatusCode.BadRequest- " + response.Content, loginUser.UserId);
                    return new BadRequestObjectResult(response.Content);

                }
                else
                {
                    Log(LogLevel.Error, "GetAcceptedAccounts returned HttpStatusCode.Forbidden- " + response.Content, loginUser.UserId);
                    return new StatusCodeResult((int)System.Net.HttpStatusCode.Forbidden);
                }

            }
            else
            {
                Log(LogLevel.Error, "GetAcceptedAccounts returned HttpStatusCode.Forbidden", "");
                return new StatusCodeResult((int)System.Net.HttpStatusCode.Forbidden);
            }

        }

        [Route("AcceptedAccountDetails")]
        [HttpGet]
        [DecryptFilter]
        [ResponseCache(CacheProfileName = "token")]
        public IActionResult GetAcceptedAccountDetails(string accountId)
        {
            var loginUser = QueryStringHelper.GetLoginuser(this.HttpContext);
            if (loginUser != null)
            {
                var webApiResponseModel = new WebAPIResponseModel(_appSettings,_logger, _memoryCache);
                webApiResponseModel.clientId = loginUser.ClientId;
                string parameters = String.Empty;
                if (!string.IsNullOrEmpty(accountId))
                {
                    parameters = "&accountId=" + accountId;
                }
                var response = webApiResponseModel.GetAcceptedAccountDetails(parameters, Integrations.Models.PortalWebAPIType.ESPMApi, "en_US", _httpMessagehandler);
                if (response.StatusCode == System.Net.HttpStatusCode.OK)
                {

                    return new OkObjectResult(response.Content);
                }
                else if (response.StatusCode == System.Net.HttpStatusCode.BadRequest)
                {
                    Log(LogLevel.Error, "GetAcceptedAccountDetails returned HttpStatusCode.BadRequest- " + response.Content, loginUser.UserId);
                    return new BadRequestObjectResult(response.Content);

                }
                else
                {
                    Log(LogLevel.Error, "GetAcceptedAccountDetails returned HttpStatusCode.Forbidden- " + response.Content, loginUser.UserId);
                    return new StatusCodeResult((int)System.Net.HttpStatusCode.Forbidden);
                }

            }
            else
            {
                Log(LogLevel.Error, "GetAcceptedAccountDetails returned HttpStatusCode.Forbidden","");
                return new StatusCodeResult((int)System.Net.HttpStatusCode.Forbidden);
            }

        }


        [Route("PendingAccounts")]
        [HttpGet]
        [DecryptFilter]
        public IActionResult GetPendingAccounts(string pageNumber = "")
        {
            var loginUser = QueryStringHelper.GetLoginuser(this.HttpContext);
            if (loginUser != null)
            {
                var webApiResponseModel = new WebAPIResponseModel(_appSettings, _logger, _memoryCache, _loginService);
                webApiResponseModel.clientId = loginUser.ClientId;
                string parameters = String.Empty;
                if (!string.IsNullOrEmpty(pageNumber))
                {
                    parameters = "&page=" + pageNumber;
                }
                var response = webApiResponseModel.GetPendingAccounts(parameters, Integrations.Models.PortalWebAPIType.ESPMApi, "en_US", _httpMessagehandler);
                if (response.StatusCode == System.Net.HttpStatusCode.OK)
                {

                    return new OkObjectResult(response.Content);
                }
                else if (response.StatusCode == System.Net.HttpStatusCode.BadRequest)
                {
                    Log(LogLevel.Error, "GetPendingAccounts returned HttpStatusCode.BadRequest- " + response.Content, loginUser.UserId);
                    return new BadRequestObjectResult(response.Content);

                }
                else
                {
                    Log(LogLevel.Error, "GetPendingAccounts returned HttpStatusCode.Forbidden- " + response.Content, loginUser.UserId);
                    return new StatusCodeResult((int)System.Net.HttpStatusCode.Forbidden);
                }

            }
            else
            {
                Log(LogLevel.Error, "GetPendingAccounts returned HttpStatusCode.Forbidden", "");
                return new StatusCodeResult((int)System.Net.HttpStatusCode.Forbidden);
            }

        }

        [Route("AcceptedAccounts")]
        [HttpGet]
        [DecryptFilter]
        public IActionResult GetAccounts(string pageNumber = "")
        {
            var loginUser = QueryStringHelper.GetLoginuser(this.HttpContext);
            if (loginUser != null)
            {
                var webApiResponseModel = new WebAPIResponseModel(_appSettings, _logger, _memoryCache, _loginService);
                webApiResponseModel.clientId = loginUser.ClientId;
                List<Account> accounts = new List<Account>();
                string parameters = String.Empty;
                if (!string.IsNullOrEmpty(pageNumber))
                {
                    parameters = "&page=" + pageNumber;
                }
                var response = webApiResponseModel.GetAcceptedAccountsList(Integrations.Models.PortalWebAPIType.ESPMApi, "en_US", _httpMessagehandler);
                if (response.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    var result1 = JObject.Parse(response.Content);
                    var links = result1["response"]["links"].HasValues == true ? (JObject)result1["response"]["links"] : null;
                    if (links != null)
                    {
                        var token = JToken.Parse(JsonConvert.SerializeObject(links["link"]));
                        if (token is JArray)
                        {
                            foreach (var acceptedaccountlink in links["link"])
                            {
                                var acceptedAccount = acceptedaccountlink.ToObject<AcceptedAccount>();
                                var responseAccountDetails = webApiResponseModel.GetAcceptedAccountDetails("&customerId=" + acceptedAccount.id, Integrations.Models.PortalWebAPIType.ESPMApi,
                                    "en_US", _httpMessagehandler);
                                if (responseAccountDetails.StatusCode == System.Net.HttpStatusCode.OK)
                                {
                                    var user = JObject.Parse(responseAccountDetails.Content).SelectToken("customer").ToString();
                                    var account = JsonConvert.DeserializeObject<Account>(user);
                                    account.accountId = acceptedAccount.id;
                                    account.IsAcceptedAccount = true;
                                    account.connectionAudit = new ConnectionAudit();
                                    accounts.Add(account);
                                }
                                else if (response.StatusCode == System.Net.HttpStatusCode.BadRequest)
                                {
                                    Log(LogLevel.Error, "GetAccounts returned HttpStatusCode.BadRequest- " + responseAccountDetails.Content, loginUser.UserId);
                                    //return new BadRequestObjectResult(response.Content);
                                }
                            }
                        }
                        else if (token is JObject)
                        {
                            var acceptedAccount = result1["response"]["links"]["link"].ToObject<AcceptedAccount>();
                            var responseAccountDetails = webApiResponseModel.GetAcceptedAccountDetails("&customerId=" + acceptedAccount.id, Integrations.Models.PortalWebAPIType.ESPMApi,
                                   "en_US", _httpMessagehandler);
                            if (responseAccountDetails.StatusCode == System.Net.HttpStatusCode.OK)
                            {
                                var user = JObject.Parse(responseAccountDetails.Content).SelectToken("customer").ToString();
                                var account = JsonConvert.DeserializeObject<Account>(user);
                                account.accountId = acceptedAccount.id;
                                account.IsAcceptedAccount = true;
                                account.connectionAudit = new ConnectionAudit();
                                accounts.Add(account);
                            }
                            else if (response.StatusCode == System.Net.HttpStatusCode.BadRequest)
                            {
                                Log(LogLevel.Error, "GetAccounts returned HttpStatusCode.BadRequest- " + responseAccountDetails.Content, loginUser.UserId);
                                //return new BadRequestObjectResult(response.Content);
                            }

                        }
                    }
                    return new OkObjectResult(accounts);

                }

                else if (response.StatusCode == System.Net.HttpStatusCode.BadRequest)
                {
                    Log(LogLevel.Error, "GetAccounts returned HttpStatusCode.BadRequest- " + response.Content, loginUser.UserId);
                    return new BadRequestObjectResult(response.Content);

                }
                else
                {
                    Log(LogLevel.Error, "GetAccounts returned HttpStatusCode.Forbidden- " + response.Content, loginUser.UserId);
                    return new StatusCodeResult((int)System.Net.HttpStatusCode.Forbidden);
                }

            }
            else
            {
                Log(LogLevel.Error, "GetAccounts returned HttpStatusCode.Forbidden", "");
                return new StatusCodeResult((int)System.Net.HttpStatusCode.Forbidden);
            }

        }

        [Route("AcceptCustomersList")]
        [HttpGet]
        [DecryptFilter]
        public IActionResult AcceptedConnectionsList([FromQuery] string skip, string take, string accountId = null)
        {
            var loginUser = QueryStringHelper.GetLoginuser(this.HttpContext);
            if (loginUser != null)
            {
                var webApiResponseModel = new WebAPIResponseModel(_appSettings,_logger, _memoryCache) {clientId = loginUser.ClientId };
                //var webApiResponseModel = new WebAPIResponseModel(_appSettings) { clientId = "224" };
                var response = webApiResponseModel.GetAcceptedAccountsList(Integrations.Models.PortalWebAPIType.ESPMApi, "en_US", _httpMessagehandler);
                if (_httpMessagehandler != null)
                    return new OkObjectResult(response.Content);
                else if (response.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    AcceptedAccountsList accounts = new AcceptedAccountsList();
                    accounts.values = new List<AcceptedAccount>();
                    accounts.count = 0;
                    var result1 = JObject.Parse(response.Content);
                    var links = result1["response"]["links"].HasValues == true ? (JObject)result1["response"]["links"] : null;
                    if (links != null)
                    {
                        var token = JToken.Parse(JsonConvert.SerializeObject(links["link"]));
                        if (token is JArray)
                        {
                            foreach (var acceptedaccountlink in links["link"])
                            {
                                var acceptedAccount = acceptedaccountlink.ToObject<AcceptedAccount>();
                                accounts.values.Add(acceptedAccount);
                            }
                        }
                        else if (token is JObject)
                        {
                            accounts.values.Add(result1["response"]["links"]["link"].ToObject<AcceptedAccount>());
                        }
                        var data = (accountId != null) ?
                                     accounts.values.Where(res => res.id == accountId).Skip(Convert.ToInt32(skip)).OrderBy(res => res.hint).Take(Convert.ToInt32(take))
                                 : accounts.values.Skip(Convert.ToInt32(skip)).Take(Convert.ToInt32(take)).OrderBy(res => res.hint);
                        AcceptedAccountsList pagedaccounts = new AcceptedAccountsList();
                        pagedaccounts.values = data.ToList();
                        pagedaccounts.count = (accountId != null) ? data.Count() : accounts.values.Count;
                        return new OkObjectResult(pagedaccounts);

                    }
                    else
                    {
                        return new OkObjectResult(accounts);
                    }

                }
                else if (response.StatusCode == System.Net.HttpStatusCode.BadRequest)
                {
                    Log(LogLevel.Error, "AcceptedConnectionsList returned HttpStatusCode.BadRequest - " + response.Content, loginUser.UserId);
                    return new BadRequestObjectResult(response.Content);

                }
                else
                {
                    Log(LogLevel.Error, "AcceptedConnectionsList returned HttpStatusCode.Forbidden- " + response.Content, loginUser.UserId);
                    return new StatusCodeResult((int)System.Net.HttpStatusCode.Forbidden);
                }
            }
            else
            {
                Log(LogLevel.Error, "AcceptedConnectionsList returned HttpStatusCode.Forbidden", "");
                return Json(new { status = "error", message = "error accepting connection" });
            }
        }

        [Route("AcceptedPropertyForAccount")]
        [HttpGet]
        [DecryptFilter]
        public IActionResult AcceptedPropertyList([FromQuery] string accountId)
        {
            var loginUser = QueryStringHelper.GetLoginuser(this.HttpContext);
            if (loginUser != null)
            {
                var parameters = String.Empty;
                if (!string.IsNullOrEmpty(accountId))
                {
                    parameters = "&accountId=" + accountId;
                    var webApiResponseModel = new WebAPIResponseModel(_appSettings, _logger, _memoryCache, _loginService) { clientId = loginUser.ClientId };
                    //var webApiResponseModel = new WebAPIResponseModel(_appSettings) { clientId = "224" };
                    var response = webApiResponseModel.AcceptedPropertyList(parameters, Integrations.Models.PortalWebAPIType.ESPMApi, "en_US", _httpMessagehandler);
                    if (_httpMessagehandler != null)
                        return new OkObjectResult(response.Content);
                    else if (response.StatusCode == System.Net.HttpStatusCode.OK)
                    {

                        return new OkObjectResult(response.Content);
                    }
                    else if (response.StatusCode == System.Net.HttpStatusCode.BadRequest)
                    {
                        Log(LogLevel.Error, "AcceptedPropertyList returned HttpStatusCode.BadRequest - " + response.Content, loginUser.UserId);
                        return new BadRequestObjectResult(response.Content);

                    }
                    else
                    {
                        Log(LogLevel.Error, "AcceptedPropertyList returned HttpStatusCode.Forbidden- " + response.Content, loginUser.UserId);
                        return new StatusCodeResult((int)System.Net.HttpStatusCode.Forbidden);
                    }
                }
                else
                {
                    Log(LogLevel.Error, "AcceptedPropertyList returned HttpStatusCode.Forbidden", loginUser.UserId);
                    return Json(new { status = "error", message = "error accepting property list" });
                }
            }
            else
            {
                Log(LogLevel.Error, "AcceptedPropertyList returned HttpStatusCode.Forbidden", "");
                return Json(new { status = "error", message = "error accepting property list" });
            }
        }

        [Route("AcceptedPropertyList")]
        [HttpGet]
        [DecryptFilter]
        public IActionResult AcceptedPropertyList([FromQuery] string skip, string take, string accountId, string propertyId = null)
        {
            var loginUser = QueryStringHelper.GetLoginuser(this.HttpContext);
            if (loginUser != null)
            {
                var parameters = String.Empty;
                if (!string.IsNullOrEmpty(accountId))
                {
                    parameters = "&accountId=" + accountId;
                    var webApiResponseModel = new WebAPIResponseModel(_appSettings,_logger, _memoryCache) { clientId = loginUser.ClientId };
                    //var webApiResponseModel = new WebAPIResponseModel(_appSettings) { clientId = "224" };
                    var response = webApiResponseModel.AcceptedPropertyList(parameters, Integrations.Models.PortalWebAPIType.ESPMApi, "en_US", _httpMessagehandler);
                    if (_httpMessagehandler != null)
                        return new OkObjectResult(response.Content);
                    else if (response.StatusCode == System.Net.HttpStatusCode.OK)
                    {

                        AcceptedPropertiesList properties = new AcceptedPropertiesList();
                        properties.values = new List<AcceptedProperties>();
                        properties.count = 0;
                        var result1 = JObject.Parse(response.Content);
                        var links = result1["response"]["links"].HasValues == true ? (JObject)result1["response"]["links"] : null;

                        if (links != null)
                        {
                            var token = JToken.Parse(JsonConvert.SerializeObject(links["link"]));
                            if (token is JArray)
                            {
                                foreach (var acceptedPropertylink in links["link"])
                                {
                                    var acceptedProperty = acceptedPropertylink.ToObject<AcceptedProperties>();
                                    properties.values.Add(acceptedProperty);
                                }

                            }

                            else if (token is JObject)
                            {
                                properties.values.Add(result1["response"]["links"]["link"].ToObject<AcceptedProperties>());
                            }


                            var data = (propertyId != null) ?
                                        properties.values.Where(res => res.id == propertyId).Skip(Convert.ToInt32(skip)).OrderBy(res => res.hint).Take(Convert.ToInt32(take))
                                        : properties.values.Skip(Convert.ToInt32(skip)).Take(Convert.ToInt32(take)).OrderBy(res => res.hint);

                            AcceptedPropertiesList pagedproperties = new AcceptedPropertiesList();
                            pagedproperties.values = data.ToList();
                            pagedproperties.count = (propertyId != null) ? data.Count() : properties.values.Count;
                            return new OkObjectResult(pagedproperties);

                        }
                        return new OkObjectResult(properties);
                    }
                    else if (response.StatusCode == System.Net.HttpStatusCode.BadRequest)
                    {
                        Log(LogLevel.Error, "AcceptedPropertyList returned HttpStatusCode.BadRequest - " + response.Content, loginUser.UserId);
                        return new BadRequestObjectResult(response.Content);

                    }
                    else
                    {
                        Log(LogLevel.Error, "AcceptedPropertyList returned HttpStatusCode.Forbidden- " + response.Content, loginUser.UserId);
                        return new StatusCodeResult((int)System.Net.HttpStatusCode.Forbidden);
                    }
                }
                else
                {
                    Log(LogLevel.Error, "AcceptedPropertyList returned HttpStatusCode.Forbidden", loginUser.UserId);
                    return Json(new { status = "error", message = "error accepting property list" });
                }
            }
            else
            {
                Log(LogLevel.Error, "AcceptedPropertyList returned HttpStatusCode.Forbidden", "");
                return Json(new { status = "error", message = "error accepting property list" });
            }
        }

        [Route("AcceptedMetersList")]
        [HttpGet]
        [DecryptFilter]
        public IActionResult AcceptedMetersList([FromQuery] string propertyId)
        {
            var loginUser = QueryStringHelper.GetLoginuser(this.HttpContext);
            if (loginUser != null)
            {
                var parameters = String.Empty;
                if (!string.IsNullOrEmpty(propertyId))
                {
                    parameters = "&propertyId=" + propertyId;
                }
                var webApiResponseModel = new WebAPIResponseModel(_appSettings,_logger, _memoryCache) { clientId = loginUser.ClientId };
                var response = webApiResponseModel.AcceptedMeterList(parameters, Integrations.Models.PortalWebAPIType.ESPMApi, "en_US", _httpMessagehandler);
                if (_httpMessagehandler != null)
                    return new OkObjectResult(response.Content);
                else if (response.StatusCode == System.Net.HttpStatusCode.OK)
                {

                    return new OkObjectResult(response.Content);
                }
                else if (response.StatusCode == System.Net.HttpStatusCode.BadRequest)
                {
                    Log(LogLevel.Error, "AcceptedMetersList returned HttpStatusCode.BadRequest - " + response.Content, loginUser.UserId);
                    return new BadRequestObjectResult(response.Content);

                }
                else
                {
                    Log(LogLevel.Error, "AcceptedMetersList returned HttpStatusCode.Forbidden- " + response.Content, loginUser.UserId);
                    return new StatusCodeResult((int)System.Net.HttpStatusCode.Forbidden);
                }
            }
            else
            {
                Log(LogLevel.Error, "AcceptedMetersList returned HttpStatusCode.Forbidden", "");
                return Json(new { status = "error", message = "error accepting connection" });
            }
        }

        [Route("AcceptConnections")]
        [HttpPost]
        [DecryptFilter]
        public IActionResult AcceptConnections([FromBody] string[] accountIds)
        {
            var loginUser = QueryStringHelper.GetLoginuser(this.HttpContext);
            List<Account> processedaccounts = new List<Account>(); ;
            if (loginUser != null)
            {
                foreach (string accountId in accountIds)
                {
                    var response = AcceptConnection(accountId);
                    var content = (((ObjectResult)response));
                    if (content.StatusCode == (int)System.Net.HttpStatusCode.OK)
                    {
                        processedaccounts.Add(new Account() { accountId = accountId, errorMessage = null });
                    }
                    else
                    {
                        Log(LogLevel.Error, "AcceptConnection returned HttpStatusCode.BadRequest - " + content.Value.ToString(), loginUser.UserId);
                        processedaccounts.Add(new Account() { accountId = accountId, errorMessage = content.Value.ToString() });
                    }
                    //processedaccounts.Add(new Accounts() { AccountId = accountId, ErrorMessage = "Error Message" });
                }


                return new OkObjectResult(processedaccounts);
            }
            else
            {
                Log(LogLevel.Error, "AcceptConnections returned HttpStatusCode.Forbidden", loginUser.UserId);
                return Json(new { status = "error", message = "error accepting connection" });
            }
        }

        [Route("RejectConnections")]
        [HttpPost]
        [DecryptFilter]
        public IActionResult RejectConnections([FromBody] string[] accountIds)
        {
            var loginUser = QueryStringHelper.GetLoginuser(this.HttpContext);
            List<Account> processedaccounts = new List<Account>(); ;
            if (loginUser != null)
            {
                foreach (string accountId in accountIds)
                {
                    var response = RejectConnection(accountId);
                    var content = (((ObjectResult)response));
                    if (content.StatusCode == (int)System.Net.HttpStatusCode.OK)
                    {
                        processedaccounts.Add(new Account() { accountId = accountId, errorMessage = null });
                    }
                    else
                    {
                        Log(LogLevel.Error, "RejectConnections returned HttpStatusCode.BadRequest - " + content.Value.ToString(), loginUser.UserId);
                        processedaccounts.Add(new Account() { accountId = accountId, errorMessage = content.Value.ToString() });
                    }
                }
                return new OkObjectResult(processedaccounts);
            }
            else
            {
                Log(LogLevel.Error, "RejectConnections returned HttpStatusCode.Forbidden", "");
                return Json(new { status = "error", message = "error accepting connection" });
            }
        }


        [Route("AcceptConnection/{accountId}")]
        [HttpPost]
        [DecryptFilter]
        public IActionResult AcceptConnection(string accountId)
        {
            var loginUser = QueryStringHelper.GetLoginuser(this.HttpContext);
            if (loginUser != null)
            {
                var webApiResponseModel = new WebAPIResponseModel(_appSettings,_logger, _memoryCache) { clientId = loginUser.ClientId };
                var parameters = string.Empty;
                if (!string.IsNullOrEmpty(accountId))
                {
                    parameters = "&accountId=" + accountId;
                }
                var response = webApiResponseModel.AcceptAccounts(parameters, Integrations.Models.PortalWebAPIType.ESPMApi, "en_US", _httpMessagehandler);
                if (response.StatusCode == System.Net.HttpStatusCode.OK)
                {

                    return new OkObjectResult(response.Content);
                }
                else if (response.StatusCode == System.Net.HttpStatusCode.BadRequest)
                {
                    Log(LogLevel.Error, "AcceptConnection returned HttpStatusCode.BadRequest - " + response.Content, loginUser.UserId);
                    return new BadRequestObjectResult(response.Content);

                }
                else
                {
                    Log(LogLevel.Error, "AcceptConnection returned HttpStatusCode.Forbidden- " + response.Content, loginUser.UserId);
                    return new StatusCodeResult((int)System.Net.HttpStatusCode.Forbidden);
                }

            }
            else
            {
                Log(LogLevel.Error, "AcceptConnection returned HttpStatusCode.Forbidden", "");
                return Json(new { status = "error", message = "error accepting connection" });
            }
        }

        [Route("RejectConnection/{accountId}")]
        [HttpPost]
        [DecryptFilter]
        public IActionResult RejectConnection(string accountId)
        {
            var loginUser = QueryStringHelper.GetLoginuser(this.HttpContext);
            if (loginUser != null)
            {
                var webApiResponseModel = new WebAPIResponseModel(_appSettings, _logger, _memoryCache, _loginService) { clientId = loginUser.ClientId };
                var parameters = string.Empty;
                if (!string.IsNullOrEmpty(accountId))
                {
                    parameters = "&accountId=" + accountId;
                }
                var response = webApiResponseModel.RejectAccounts(parameters, Integrations.Models.PortalWebAPIType.ESPMApi, "en_US", _httpMessagehandler);
                if (response.StatusCode == System.Net.HttpStatusCode.OK)
                {

                    return new OkObjectResult(response.Content);
                }
                else if (response.StatusCode == System.Net.HttpStatusCode.BadRequest)
                {
                    Log(LogLevel.Error, "RejectConnection returned HttpStatusCode.BadRequest- " + response.Content, loginUser.UserId);
                    return new BadRequestObjectResult(response.Content);

                }
                else
                {
                    Log(LogLevel.Error, "RejectConnection returned HttpStatusCode.Forbidden", loginUser.UserId);
                    return new StatusCodeResult((int)System.Net.HttpStatusCode.Forbidden);
                }
            }
            else
            {
                Log(LogLevel.Error, "RejectConnection returned HttpStatusCode.Forbidden", "");
                return new StatusCodeResult((int)System.Net.HttpStatusCode.Forbidden);
            }
        }

        [Route("PendingMeters")]
        [HttpGet]
        [DecryptFilter]
        public IActionResult GetPendingMeters(string pageNumber = "")
        {
            var loginUser = QueryStringHelper.GetLoginuser(this.HttpContext);
            if (loginUser != null)
            {
                var webApiResponseModel = new WebAPIResponseModel(_appSettings,_logger, _memoryCache);
                webApiResponseModel.clientId = loginUser.ClientId;
                string parameters = String.Empty;
                if (!string.IsNullOrEmpty(pageNumber))
                {
                    parameters = "&page=" + pageNumber;
                }
                var response = webApiResponseModel.GetPendingMeters(parameters, Integrations.Models.PortalWebAPIType.ESPMApi, "en_US", _httpMessagehandler);
                if (response.StatusCode == System.Net.HttpStatusCode.OK)
                {

                    return new OkObjectResult(response.Content);
                }
                else if (response.StatusCode == System.Net.HttpStatusCode.BadRequest)
                {
                    Log(LogLevel.Error, "GetPendingMeters returned HttpStatusCode.BadRequest- " + response.Content, loginUser.UserId);
                    return new BadRequestObjectResult(response.Content);

                }
                else
                {
                    Log(LogLevel.Error, "GetPendingMeters returned HttpStatusCode.Forbidden- " + response.Content, loginUser.UserId);
                    return new StatusCodeResult((int)System.Net.HttpStatusCode.Forbidden);
                }

            }
            else
            {
                Log(LogLevel.Error, "GetPendingMeters returned HttpStatusCode.Forbidden", "");
                return new StatusCodeResult((int)System.Net.HttpStatusCode.Forbidden);
            }

        }

        [Route("AcceptMeters")]
        [HttpPost]
        [DecryptFilter]
        public IActionResult AcceptMeters([FromBody] List<MeterMap> meterIds)
        {
            var loginUser = QueryStringHelper.GetLoginuser(this.HttpContext);
            List<Meter> processedMeters = new List<Meter>(); ;
            if (loginUser != null)
            {
                foreach (MeterMap meter in meterIds)
                {
                    var response = AcceptMeter(meter);
                    var content = (((ObjectResult)response));
                    if (content.StatusCode == (int)System.Net.HttpStatusCode.OK)
                    {

                        processedMeters.Add(new Meter() { meterId = meter.Customer, errorMessage = null });
                    }
                    else
                    {
                        Log(LogLevel.Error, "AcceptMeters returned HttpStatusCode.BadRequest - " + content.Value.ToString(), loginUser.UserId);
                        processedMeters.Add(new Meter() { meterId = meter.Customer, errorMessage = content.Value.ToString() });
                    }
                    //processedaccounts.Add(new Accounts() { AccountId = accountId, ErrorMessage = "Error Message" });
                }


                return new OkObjectResult(processedMeters);
            }
            else
            {
                Log(LogLevel.Error, "AcceptMeters returned HttpStatusCode.Forbidden", "");
                return Json(new { status = "error", message = "error accepting connection" });
            }
        }

        [Route("AcceptMeter")]
        [HttpPost]
        [DecryptFilter]
        public IActionResult AcceptMeter([FromBody] MeterMap meter)
        {
            var loginUser = QueryStringHelper.GetLoginuser(this.HttpContext);
            if (loginUser != null)
            {
                
                var webApiResponseModel = new WebAPIResponseModel(_appSettings,_logger, _memoryCache) { clientId = loginUser.ClientId };
                var parameters = string.Empty;
                if (!string.IsNullOrEmpty(meter.Customer))
                {
                    parameters = "&meterId=" + meter.Customer;
                }
                //var response = new ResponseModel() {StatusCode = System.Net.HttpStatusCode.OK};
                var response = webApiResponseModel.AcceptMeters(parameters, Integrations.Models.PortalWebAPIType.ESPMApi, "en_US", _httpMessagehandler);
                if (response.StatusCode == System.Net.HttpStatusCode.OK)
                {

                    return new OkObjectResult(response);
                    //if (meter.AccountId != null)
                    //{   
                    //    var mappedMeters = _mappingService.UpdateMeterMappingonAccept(shardDbName, meter.AccountId,
                    //        meter.PremiseId, int.Parse(loginUser.UserId));
                    //    if (mappedMeters)
                    //    {
                    //        return new OkObjectResult(response);
                    //    }
                    //    else
                    //    {
                    //        Log(LogLevel.Error, "AcceptMeter/{meterId} returned HttpStatusCode.BadRequest while mapping - " + response.Content, loginUser.UserId);
                    //        return new BadRequestObjectResult(response.Content);
                    //    }
                    //}
                    //else
                    //{
                    //    Log(LogLevel.Error, "AcceptMeter/{meterId} - AccountId is null - ", "");
                    //    return new OkObjectResult(response.Content);
                    //}

                }
                else if (response.StatusCode == System.Net.HttpStatusCode.BadRequest)
                {
                    Log(LogLevel.Error, "AcceptMeter/{meterId} returned HttpStatusCode.BadRequest - " + response.Content, loginUser.UserId);
                    return new BadRequestObjectResult(response.Content);

                }
                else
                {
                    Log(LogLevel.Error, "AcceptMeter/{meterId} returned HttpStatusCode.Forbidden- " + response.Content, loginUser.UserId);
                    return new StatusCodeResult((int)System.Net.HttpStatusCode.Forbidden);
                }

            }
            else
            {
                Log(LogLevel.Error, "AcceptMeter/{meterId} returned HttpStatusCode.Forbidden", loginUser.UserId);
                return Json(new { status = "error", message = "error accepting connection" });
            }
        }

        [Route("RejectMeters")]
        [HttpPost]
        [DecryptFilter]
        public IActionResult RejectMeters([FromBody] List<MeterMap> meterIds)
        {
            var loginUser = QueryStringHelper.GetLoginuser(this.HttpContext);
            List<Meter> processedMeters = new List<Meter>(); ;
            if (loginUser != null)
            {
                foreach (MeterMap meter in meterIds)
                {
                    var response = RejectMeter(meter);
                    var content = (((ObjectResult)response));
                    if (content.StatusCode == (int)System.Net.HttpStatusCode.OK)
                    {
                        processedMeters.Add(new Meter() { meterId = meter.Customer, errorMessage = null });
                    }
                    else
                    {
                        Log(LogLevel.Error, "RejectMeters returned HttpStatusCode.BadRequest - " + content.Value.ToString(), loginUser.UserId);
                        processedMeters.Add(new Meter() { meterId = meter.Customer, errorMessage = content.Value.ToString() });
                    }
                    //processedaccounts.Add(new Accounts() { AccountId = accountId, ErrorMessage = "Error Message" });
                }


                return new OkObjectResult(processedMeters);
            }
            else
            {
                Log(LogLevel.Error, "RejectMeters returned HttpStatusCode.Forbidden", "");
                return Json(new { status = "error", message = "error accepting connection" });
            }
        }

        [Route("RejectMeter")]
        [HttpPost]
        [DecryptFilter]
        public IActionResult RejectMeter([FromBody] MeterMap meter)
        {
            var loginUser = QueryStringHelper.GetLoginuser(this.HttpContext);
            if (loginUser != null)
            {
                var webApiResponseModel = new WebAPIResponseModel(_appSettings,_logger, _memoryCache) { clientId = loginUser.ClientId };
                var parameters = string.Empty;
                if (!string.IsNullOrEmpty(meter.Customer))
                {
                    parameters = "&meterId=" + meter.Customer;
                }
                var response = webApiResponseModel.RejectMeters(parameters, Integrations.Models.PortalWebAPIType.ESPMApi, "en_US", _httpMessagehandler);
                if (response.StatusCode == System.Net.HttpStatusCode.OK)
                {

                    return new OkObjectResult(response.Content);
                }
                else if (response.StatusCode == System.Net.HttpStatusCode.BadRequest)
                {
                    Log(LogLevel.Error, "RejectMeter/{meterId} returned HttpStatusCode.BadRequest - " + response.Content, loginUser.UserId);
                    return new BadRequestObjectResult(response.Content);

                }
                else
                {
                    Log(LogLevel.Error, "RejectMeter/{meterId} returned HttpStatusCode.Forbidden- " + response.Content, loginUser.UserId);
                    return new StatusCodeResult((int)System.Net.HttpStatusCode.Forbidden);
                }

            }
            else
            {
                Log(LogLevel.Error, "RejectMeter/{meterId} returned HttpStatusCode.Forbidden", "");
                return Json(new { status = "error", message = "error accepting connection" });
            }
        }

        [Route("MeterDetails")]
        [HttpGet]
        [DecryptFilter]
        public IActionResult GetMeterDetails([FromQuery] string meterId)
        {
            var loginUser = QueryStringHelper.GetLoginuser(this.HttpContext);
            if (loginUser != null)
            {
                var webApiResponseModel = new WebAPIResponseModel(_appSettings, _logger, _memoryCache, _loginService);
                webApiResponseModel.clientId = loginUser.ClientId;
                var parameters = String.Empty;
                if (!string.IsNullOrEmpty(meterId))
                {
                    parameters = "&meterId=" + meterId;
                }
                var response = webApiResponseModel.GetMeterDetails(parameters, Integrations.Models.PortalWebAPIType.ESPMApi, "en_US", _httpMessagehandler);
                if (response.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    return new OkObjectResult(response.Content);
                }
                else if (response.StatusCode == System.Net.HttpStatusCode.BadRequest)
                {
                    Log(LogLevel.Error, "GetMeterDetails returned HttpStatusCode.BadRequest- " + response.Content, loginUser.UserId);
                    return new BadRequestObjectResult(response.Content);

                }
                else
                {
                    Log(LogLevel.Error, "GetMeterDetails returned HttpStatusCode.Forbidden- " + response.Content, loginUser.UserId);
                    return new StatusCodeResult((int)System.Net.HttpStatusCode.Forbidden);
                }

            }
            else
            {
                Log(LogLevel.Error, "GetMeterDetails returned HttpStatusCode.Forbidden", "");
                return new StatusCodeResult((int)System.Net.HttpStatusCode.Forbidden);
            }

        }

        [Route("AcceptedMeterDetailsList")]
        [HttpGet]
        [DecryptFilter]
        [ResponseCache(CacheProfileName = "token")]
        public IActionResult GetMeterDetailsList([FromQuery] string propertyId, string acceptedMeters)
        {
            var loginUser = QueryStringHelper.GetLoginuser(this.HttpContext);
            if (loginUser != null)
            {
                var webApiResponseModel = new WebAPIResponseModel(_appSettings, _logger, _memoryCache, _loginService);
                webApiResponseModel.clientId = loginUser.ClientId;
                //webApiResponseModel.clientId = "224";
                MeterDetailsCollection meterdetailsCollection = new MeterDetailsCollection();
                meterdetailsCollection.values = new List<MeterDetails>();
                meterdetailsCollection.count = 0;
                string parameters = String.Empty;
                if (!string.IsNullOrEmpty(propertyId))
                {
                    parameters = "&propertyId=" + propertyId;
                }
                var response = webApiResponseModel.AcceptedMeterList(parameters, Integrations.Models.PortalWebAPIType.ESPMApi, "en_US", _httpMessagehandler);
                if (response.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    var result1 = JObject.Parse(response.Content);
                    var links = result1["response"]["links"].HasValues == true ? (JObject)result1["response"]["links"] : null;
                    if (links != null)
                    {
                        var token = JToken.Parse(JsonConvert.SerializeObject(links["link"]));
                        if (token is JArray)
                        {
                            foreach (var meter in links["link"])
                            {
                                var meterlist = meter.ToObject<MeterList>();
                                var responseMeterDetails = webApiResponseModel.GetMeterDetails("&meterId=" + meterlist.link.Substring(meterlist.link.LastIndexOf("/") + 1), Integrations.Models.PortalWebAPIType.ESPMApi, "en-US", _httpMessagehandler);

                                if (responseMeterDetails.StatusCode == System.Net.HttpStatusCode.OK)
                                {
                                    var meterdetailsJson = JObject.Parse(responseMeterDetails.Content).SelectToken("meter").ToString();
                                    var meterDetails = JsonConvert.DeserializeObject<MeterDetails>(meterdetailsJson);
                                    meterdetailsCollection.values.Add(meterDetails);
                                }
                                else if (responseMeterDetails.StatusCode == System.Net.HttpStatusCode.BadRequest)
                                {
                                    Log(LogLevel.Error, "GetMeterDetailsList returned HttpStatusCode.BadRequest- " + responseMeterDetails.Content, loginUser.UserId);
                                    //return new BadRequestObjectResult(response.Content);
                                }
                            }
                        }
                        else if (token is JObject)
                        {
                            var meterlist = result1["response"]["links"]["link"].ToObject<MeterList>();
                            var responseMeterDetails = webApiResponseModel.GetMeterDetails("&meterId=" + meterlist.link.Substring(meterlist.link.LastIndexOf("/") + 1), Integrations.Models.PortalWebAPIType.ESPMApi, "en-US", _httpMessagehandler);
                            if (responseMeterDetails.StatusCode == System.Net.HttpStatusCode.OK)
                            {
                                var meterdetailsJson = JObject.Parse(responseMeterDetails.Content).SelectToken("meter").ToString();
                                var meterDetails = JsonConvert.DeserializeObject<MeterDetails>(meterdetailsJson);
                                meterdetailsCollection.values.Add(meterDetails);
                            }
                            else if (responseMeterDetails.StatusCode == System.Net.HttpStatusCode.BadRequest)
                            {
                                Log(LogLevel.Error, "GetMeterDetailsList returned HttpStatusCode.BadRequest- " + responseMeterDetails.Content, loginUser.UserId);
                                //return new BadRequestObjectResult(response.Content);
                            }

                        }
                        meterdetailsCollection.count = meterdetailsCollection.values.Count;

                    }

                    return new OkObjectResult(meterdetailsCollection);
                }
                else if (response.StatusCode == System.Net.HttpStatusCode.BadRequest)
                {
                    Log(LogLevel.Error, "GetMeterDetailsList returned HttpStatusCode.BadRequest- " + response.Content, loginUser.UserId);
                    return new BadRequestObjectResult(response.Content);
                }
                else if (response.StatusCode == System.Net.HttpStatusCode.Forbidden)
                {
                    Log(LogLevel.Error, "GetMeterDetailsList returned HttpStatusCode.BadRequest- " + response.Content, loginUser.UserId);
                    return new OkObjectResult(meterdetailsCollection);

                }
                else
                {
                    Log(LogLevel.Error, "GetMeterDetailsList returned HttpStatusCode.Forbidden- " + response.Content, loginUser.UserId);
                    return new StatusCodeResult((int)System.Net.HttpStatusCode.Forbidden);
                }

            }
            else
            {
                Log(LogLevel.Error, "GetMeterDetailsList returned HttpStatusCode.Forbidden", "");
                return new StatusCodeResult((int)System.Net.HttpStatusCode.Forbidden);
            }
        }

        [Route("PropertyDetails")]
        [HttpGet]
        [DecryptFilter]
        [ResponseCache(CacheProfileName = "token")]
        public IActionResult GetPropertyDetails(string propertyId)
        {
            var loginUser = QueryStringHelper.GetLoginuser(this.HttpContext);
            if (loginUser != null)
            {
                var webApiResponseModel = new WebAPIResponseModel(_appSettings, _logger, _memoryCache, _loginService);
                webApiResponseModel.clientId = loginUser.ClientId;
                string parameters = String.Empty;
                if (!string.IsNullOrEmpty(propertyId))
                {
                    parameters = "&propertyId=" + propertyId;
                }
                var response = webApiResponseModel.GetPropertyDetails(parameters, Integrations.Models.PortalWebAPIType.ESPMApi, "en_US", _httpMessagehandler);
                if (response.StatusCode == System.Net.HttpStatusCode.OK)
                {

                    return new OkObjectResult(response.Content);
                }
                else if (response.StatusCode == System.Net.HttpStatusCode.BadRequest)
                {
                    Log(LogLevel.Error, "GetPropertyDetails returned HttpStatusCode.BadRequest- " + response.Content, loginUser.UserId);
                    return new BadRequestObjectResult(response.Content);

                }
                else
                {
                    Log(LogLevel.Error, "GetPropertyDetails returned HttpStatusCode.Forbidden- " + response.Content, loginUser.UserId);
                    return new StatusCodeResult((int)System.Net.HttpStatusCode.Forbidden);
                }

            }
            else
            {
                Log(LogLevel.Error, "GetPropertyDetails returned HttpStatusCode.Forbidden", "");
                return new StatusCodeResult((int)System.Net.HttpStatusCode.Forbidden);
            }

        }

        [Route("CustomMeterFields")]
        [HttpGet]
        [DecryptFilter]
        public IActionResult GetCustomMeterfields([FromQuery] string meterId)
        {
            var loginUser = QueryStringHelper.GetLoginuser(this.HttpContext);
            if (loginUser != null)
            {
                var webApiResponseModel = new WebAPIResponseModel(_appSettings, _logger, _memoryCache, _loginService);
                webApiResponseModel.clientId = loginUser.ClientId;
                string parameters = String.Empty;
                if (!string.IsNullOrEmpty(meterId))
                {
                    parameters = "&meterId=" + meterId;
                }
                var response = webApiResponseModel.GetCustomFields(parameters, Integrations.Models.PortalWebAPIType.ESPMApi, "en_US", _httpMessagehandler);
                if (response.StatusCode == System.Net.HttpStatusCode.OK)
                {

                    return new OkObjectResult(response.Content);
                }
                else if (response.StatusCode == System.Net.HttpStatusCode.BadRequest)
                {
                    Log(LogLevel.Error, "GetCustomfields returned HttpStatusCode.BadRequest- " + response.Content, loginUser.UserId);
                    return new BadRequestObjectResult(response.Content);

                }
                else
                {
                    Log(LogLevel.Error, "GetCustomfields returned HttpStatusCode.Forbidden- " + response.Content, loginUser.UserId);
                    return new StatusCodeResult((int)System.Net.HttpStatusCode.Forbidden);
                }

            }
            else
            {
                Log(LogLevel.Error, "GetCustomfields returned HttpStatusCode.Forbidden", "");
                return new StatusCodeResult((int)System.Net.HttpStatusCode.Forbidden);
            }

        }

        [Route("MeterConsumptionData")]
        [HttpGet]
        [DecryptFilter]
        public IActionResult GetMeterConsumptionData(string meterId)
        {
            var loginUser = QueryStringHelper.GetLoginuser(this.HttpContext);
            if (loginUser != null)
            {
                var webApiResponseModel = new WebAPIResponseModel(_appSettings, _logger, _memoryCache, _loginService);
                webApiResponseModel.clientId = loginUser.ClientId;
                string parameters = String.Empty;
                if (!string.IsNullOrEmpty(meterId))
                {
                    parameters = "&meterId=" + meterId;

                }
                var response = webApiResponseModel.GetMeterConsumptionData(parameters, Integrations.Models.PortalWebAPIType.ESPMApi, "en_US", _httpMessagehandler);
                if (response.StatusCode == System.Net.HttpStatusCode.OK)
                {

                    return new OkObjectResult(response.Content);
                }
                else if (response.StatusCode == System.Net.HttpStatusCode.BadRequest)
                {
                    Log(LogLevel.Error, "MeterConsumptionData returned HttpStatusCode.BadRequest- " + response.Content, loginUser.UserId);
                    return new BadRequestObjectResult(response.Content);

                }
                else
                {
                    Log(LogLevel.Error, "MeterConsumptionData returned HttpStatusCode.Forbidden- " + response.Content, loginUser.UserId);
                    return new StatusCodeResult((int)System.Net.HttpStatusCode.Forbidden);
                }

            }
            else
            {
                Log(LogLevel.Error, "MeterConsumptionData returned HttpStatusCode.Forbidden", "");
                return new StatusCodeResult((int)System.Net.HttpStatusCode.Forbidden);
            }

        }


        [Route("MeterEndDate")]
        [HttpGet]
        [DecryptFilter]
        public IActionResult GetMeterEndDate(string meterId)
        {
            var loginUser = QueryStringHelper.GetLoginuser(this.HttpContext);
            if (loginUser != null)
            {
                var webApiResponseModel = new WebAPIResponseModel(_appSettings, _logger, _memoryCache, _loginService);
                webApiResponseModel.clientId = loginUser.ClientId;
                string parameters = String.Empty;
                if (!string.IsNullOrEmpty(meterId))
                {
                    parameters = "&meterId=" + meterId;
                }
                var response = webApiResponseModel.GetMeterConsumptionData(parameters, Integrations.Models.PortalWebAPIType.ESPMApi, "en_US", _httpMessagehandler);
                if (response.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    var consumptionData = new List<MeterConsumption>();
                    var result1 = JObject.Parse(response.Content);
                    var meterData = result1["meterData"].HasValues == true ? (JObject)result1["meterData"] : null;
                    
                    if (meterData != null && meterData["meterConsumption"]!= null)
                    {
                       
                        foreach (var consumptiondata in meterData["meterConsumption"])
                            {
                                var data = consumptiondata.ToObject<MeterConsumption>();
                                consumptionData.Add(data);
                            }
                        return new OkObjectResult(consumptionData.Max(date => date.EndDate));
                    }
                    var now = DateTime.Now;
                    return new OkObjectResult(new DateTime(now.Year - 1, now.Month, 1).ToString("yyyy-MM-dd"));
                }
                else if (response.StatusCode == System.Net.HttpStatusCode.NotFound)
                {
                    Log(LogLevel.Error, "GetMeterEndDate returned HttpStatusCode.BadRequest- " + response.Content, loginUser.UserId);
                    return new NotFoundObjectResult(response.Content);

                }
                else if (response.StatusCode == System.Net.HttpStatusCode.BadRequest)
                {
                    Log(LogLevel.Error, "GetMeterEndDate returned HttpStatusCode.BadRequest- " + response.Content, loginUser.UserId);
                    return new BadRequestObjectResult(response.Content);

                }
                else
                {
                    Log(LogLevel.Error, "GetMeterEndDate returned HttpStatusCode.Forbidden- " + response.Content, loginUser.UserId);
                    return new StatusCodeResult((int)System.Net.HttpStatusCode.Forbidden);
                }

            }
            else
            {
                Log(LogLevel.Error, "GetCustomfields returned HttpStatusCode.Forbidden", "");
                return new StatusCodeResult((int)System.Net.HttpStatusCode.Forbidden);
            }

        }

        [Route("PostConsumptionData")]
        [HttpPost]
        [DecryptFilter]
        public IActionResult PostConsumptiondata([FromBody] List<MeterBillingData> meterBillingData)
        {
            var loginUser = QueryStringHelper.GetLoginuser(this.HttpContext);
            List<Account> processedaccounts = new List<Account>(); ;
            if (loginUser != null)
            {
                try
                {
                    string shardname = GetSharedName(Int32.Parse(loginUser.ClientId));
                    if (_mappingService.DoesWhiteListHasAnyRecords(shardname))
                    {
                        if (!_mappingService.CheckIfMeterIsInWhitelist(shardname, meterBillingData[0].MeterId))
                        {
                            Log(LogLevel.Information,"PostConsumptiondata - Data not found in whitelist." + meterBillingData[0].MeterId,
                                loginUser.UserId);
                            return new OkResult();
                        }
                    }
                    var webApiResponseModel = new WebAPIResponseModel(_appSettings, _logger, _memoryCache, _loginService);
                    webApiResponseModel.clientId = loginUser.ClientId;
                        var parameters = "&meterId=" + meterBillingData[0].MeterId;
                        var response =
                            webApiResponseModel.PostMeterConsumptionData(parameters, PortalWebAPIType.ESPMApi, "en-US",
                                meterBillingData);
                        if (response.StatusCode == System.Net.HttpStatusCode.Created)
                        {
                            Log(LogLevel.Information,
                                "PostConsumptiondata returned HttpStatusCode.OK - " + response.Content,
                                loginUser.UserId);
                        }
                        else
                        {
                            Log(LogLevel.Error,
                                "PostConsumptiondata returned HttpStatusCode.BadRequest - " + response.Content,
                                loginUser.UserId);
                        }
                   
                    return new OkResult();
                }
                catch (Exception ex)
                {
                    Log(LogLevel.Error, "PostConsumptiondata returned HttpStatusCode.BadRequest- " + ex.Message, loginUser.UserId);
                    return new StatusCodeResult((int)System.Net.HttpStatusCode.InternalServerError);
                }
            }
            else
            {
                Log(LogLevel.Error, "PostConsumptiondata returned HttpStatusCode.Forbidden", "");
                return Json(new { status = "error", message = "error accepting connection" });
            }
        }


        [Route("BaselineAndTarget")]
        [HttpGet]
        [DecryptFilter]
        public IActionResult GetBaselineAndTarget(string propertyId)
        {
            var loginUser = QueryStringHelper.GetLoginuser(this.HttpContext);
            if (loginUser != null)
            {
                var webApiResponseModel = new WebAPIResponseModel(_appSettings, _logger, _memoryCache, _loginService);
                webApiResponseModel.clientId = loginUser.ClientId;
                string parameters = String.Empty;
                if (!string.IsNullOrEmpty(propertyId))
                {
                    parameters = "&propertyId=" + propertyId;
                }
                var response = webApiResponseModel.GetBaseLineAndTarget(parameters, Integrations.Models.PortalWebAPIType.ESPMApi, "en_US", _httpMessagehandler);
                if (response.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    return new OkObjectResult(response.Content);
                }
                else if (response.StatusCode == System.Net.HttpStatusCode.BadRequest)
                {
                    Log(LogLevel.Error, "GetBaselineAndTarget returned HttpStatusCode.BadRequest- " + response.Content, loginUser.UserId);
                    return new BadRequestObjectResult(response.Content);

                }
                else
                {
                    Log(LogLevel.Error, "GetBaselineAndTarget returned HttpStatusCode.Forbidden- " + response.Content, loginUser.UserId);
                    return new StatusCodeResult((int)System.Net.HttpStatusCode.Forbidden);
                }

            }
            else
            {
                Log(LogLevel.Error, "GetBaselineAndTarget returned HttpStatusCode.Forbidden", "");
                return new StatusCodeResult((int)System.Net.HttpStatusCode.Forbidden);
            }

        }

        [Route("GetPropertyMetrics")]
        [HttpGet]
        [DecryptFilter]
        public IActionResult GetPropertyMetrics([FromQuery] string propertyId, string month, string year, string metrics)
        {
            var loginUser = QueryStringHelper.GetLoginuser(this.HttpContext);
            if (loginUser != null)
            {
                var webApiResponseModel = new WebAPIResponseModel(_appSettings, _logger, _memoryCache, _loginService);
                webApiResponseModel.clientId = loginUser.ClientId;
                string parameters = String.Empty;
                if (!string.IsNullOrEmpty(propertyId))
                {
                    parameters = "&propertyId=" + propertyId;
                }
                if (!string.IsNullOrEmpty(month))
                {
                    parameters += "&month=" + month;
                }
                if (!string.IsNullOrEmpty(year))
                {
                    parameters += "&year=" + year;
                }
                parameters += "&measurementSystem=EPA";
                var response = webApiResponseModel.GetPropertyMetrics(parameters, metrics, Integrations.Models.PortalWebAPIType.ESPMApi, "en_US", _httpMessagehandler);
                if (response.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    return new OkObjectResult(response.Content);
                }
                else if (response.StatusCode == System.Net.HttpStatusCode.BadRequest)
                {
                    Log(LogLevel.Error, "GetPropertyMetrics returned HttpStatusCode.BadRequest- " + response.Content, loginUser.UserId);
                    return new BadRequestObjectResult(response.Content);

                }
                else
                {
                    Log(LogLevel.Error, "GetPropertyMetrics returned HttpStatusCode.Forbidden- " + response.Content, loginUser.UserId);
                    return new StatusCodeResult((int)System.Net.HttpStatusCode.Forbidden);
                }

            }
            else
            {
                Log(LogLevel.Error, "GetPropertyMetrics returned HttpStatusCode.Forbidden", "");
                return new StatusCodeResult((int)System.Net.HttpStatusCode.Forbidden);
            }

        }

        [Route("GetPropertyMetricsNoScore")]
        [HttpGet]
        [DecryptFilter]
        public IActionResult GetPropertyMetricsNoScoreReason([FromQuery] string propertyId, string month, string year)
        {
            var loginUser = QueryStringHelper.GetLoginuser(this.HttpContext);
            if (loginUser != null)
            {
                var webApiResponseModel = new WebAPIResponseModel(_appSettings, _logger, _memoryCache, _loginService);
                webApiResponseModel.clientId = loginUser.ClientId;
                string parameters = String.Empty;
                if (!string.IsNullOrEmpty(propertyId))
                {
                    parameters = "&propertyId=" + propertyId;
                }
                if (!string.IsNullOrEmpty(month))
                {
                    parameters += "&month=" + month;
                }
                if (!string.IsNullOrEmpty(year))
                {
                    parameters += "&year=" + year;
                }
                parameters += "&measurementSystem=EPA";
                var response = webApiResponseModel.GetPropertyMetricsNoScoreReason(parameters, Integrations.Models.PortalWebAPIType.ESPMApi, "en_US", _httpMessagehandler);
                if (response.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    return new OkObjectResult(response.Content);
                }
                else if (response.StatusCode == System.Net.HttpStatusCode.BadRequest)
                {
                    Log(LogLevel.Error, "GetPropertyMetricsNoScoreReason returned HttpStatusCode.BadRequest- " + response.Content, loginUser.UserId);
                    return new BadRequestObjectResult(response.Content);

                }
                else
                {
                    Log(LogLevel.Error, "GetPropertyMetricsNoScoreReason returned HttpStatusCode.Forbidden- " + response.Content, loginUser.UserId);
                    return new StatusCodeResult((int)System.Net.HttpStatusCode.Forbidden);
                }

            }
            else
            {
                Log(LogLevel.Error, "GetPropertyMetricsNoScoreReason returned HttpStatusCode.Forbidden", "");
                return new StatusCodeResult((int)System.Net.HttpStatusCode.Forbidden);
            }

        }
        private string GetSharedName(int clientId)
        {
            string servercacheKey = "shardname-" + clientId;
            string shardname = string.Empty;
            if (this._memoryCache.TryGetValue(servercacheKey, out shardname))
            {
                return shardname;
            }
            else
            {
                shardname = _configurationService.GetDwDatabaseConfigValue(clientId, _appSettings.CEENvironment,
                    "espmbenchmark.shardname");

                this._memoryCache.Set(servercacheKey, shardname, new MemoryCacheEntryOptions { AbsoluteExpirationRelativeToNow = (DateTime.Now.AddMinutes(_serverCacheTimeOut) - DateTime.Now) });
                return shardname;
            }
        }

        private void Log(LogLevel level, string message, string userId)
        {
            HttpContext?.Session.SetString("UserId", userId);
            switch (level)
            {
                case LogLevel.Error: _logger.LogError(message); break;
                case LogLevel.Critical: _logger.LogCritical(message); break;
                case LogLevel.Warning: _logger.LogWarning(message); break;
                case LogLevel.Information: _logger.LogInformation(message); break;
                case LogLevel.Debug: _logger.LogDebug(message); break;
                case LogLevel.Trace: _logger.LogTrace(message); break;
            }

        }
    }
}
