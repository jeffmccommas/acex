﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CE.Portals.WebApi.Core.Helpers;
using CE.Portals.WebApi.Core.Models;
using Microsoft.Extensions.Configuration;

namespace CE.Portals.WebApi.Core
{
    public static class ConfigHelper
    {
        public static string DecodeString()
        {
            return string.Empty;
        }
        public static string EncodeString()
        {
            return string.Empty;
        }

        public static string GetInsightsConnectionString(IConfigurationRoot config)
        {
            string connectionString = config.GetConnectionString("InsightsDatabase"); 
            if (!string.IsNullOrEmpty(connectionString))
            {
                return connectionString;
            }
            else
            {
                return Environment.GetEnvironmentVariable("INSIGHTS_DB_CS");
            }
        }

        public static string GetInsightsDWConnectionString(IConfigurationRoot config)
        {
            string connectionString = config.GetConnectionString("InsightsDWDatabase");
            if (!string.IsNullOrEmpty(connectionString))
            {
                return connectionString;
            }
            else
            {
                return Environment.GetEnvironmentVariable("INSIGHTS_DW_DB_CS"); ;
            }
        }

        public static string GetInsightsMetaDataConnectionString(IConfigurationRoot config)
        {
            string connectionString = config.GetConnectionString("InsightsMetaDataDatabase");
            if (!string.IsNullOrEmpty(connectionString))
            {
                return connectionString;
            }
            else
            {
                return Environment.GetEnvironmentVariable("INSIGHTS_METADATA_DB_CS");
            }
        }

        public static string[] GetAllowedOriginsList(IConfigurationRoot config)
        {
            List<string> allowedOrigins = new List<string>();
            string envSetting = Environment.GetEnvironmentVariable("AllowedOrigins");
            
            if (string.IsNullOrEmpty(envSetting))
            {
                var jwtAppSettingOptions = config.GetSection(nameof(JWTIssuer));
                envSetting = jwtAppSettingOptions[nameof(JWTIssuer.Audience)];
                allowedOrigins.Add(envSetting);
                return allowedOrigins.ToArray();
            }
            else
            {

                return envSetting.Split(',');
            }
        }
    }
}
