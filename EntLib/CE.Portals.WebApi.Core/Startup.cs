﻿using System;
using CE.Portals.DataService.Interfaces;
using CE.Portals.DataService.Services;
using CE.Portals.WebApi.Core.Models;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using DBContext = CE.InsightDataAccess;
using Microsoft.IdentityModel.Tokens;
using System.Text;
using AspNetCoreRateLimit;
using CE.Portals.WebApi.Core.Filters;
using CE.Portals.WebApi.Core.Helpers;
using Microsoft.AspNetCore.ResponseCompression;
using NLog;
using Swashbuckle.AspNetCore.Swagger;
using NLog.Extensions.Logging;
using NLog.Web;

namespace CE.Portals.WebApi.Core
{
    public class Startup
    {
        public Startup(IHostingEnvironment env)
        {
            env.ConfigureNLog($"nlog.{env.EnvironmentName}.config");
            var builder = new ConfigurationBuilder()
                .SetBasePath(env.ContentRootPath)
                .AddJsonFile("appsettings.json")
                .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true);

            if (env.IsDevelopment() || env.IsEnvironment("dev") || env.IsEnvironment("localdev"))
            {
                // This will push telemetry data through Application Insights pipeline faster, allowing you to view results immediately.
                builder.AddApplicationInsightsSettings(developerMode: true);
            }

            builder.AddEnvironmentVariables();
            Configuration = builder.Build();
        }

        public IConfigurationRoot Configuration { get; }


        // This method gets called by the runtime. Use this method to add services to the container
        public void ConfigureServices(IServiceCollection services)
        {

            var pathToDoc = Configuration["Swagger:FileName"];
            services.Configure<AppSettings>(Configuration.GetSection("AppSettings"));

            var endpointTracking = new EndpointTrackingActionAttribute();
            var _signingKey = new SymmetricSecurityKey(Encoding.ASCII.GetBytes(Configuration["AppSettings:PurposeKey"]));
            var jwtAppSettingOptions = Configuration.GetSection(nameof(JWTIssuer));

            services.AddCors(options =>
            {
                options.AddPolicy("AllowAllCorsPolicy",
                    builder => builder.AllowAnyOrigin()
                        .AllowAnyMethod()
                        .AllowAnyHeader()
                        .AllowCredentials());

                options.AddPolicy("AllowOnlyESPMWebsite",
                    builder => builder.WithOrigins(ConfigHelper.GetAllowedOriginsList(Configuration))
                        .AllowAnyMethod()
                        .AllowAnyHeader()
                        .AllowCredentials());
            });
            
            int tokenExpirationMinutes = Convert.ToInt32(jwtAppSettingOptions["JWTTokenExpirationMinutes"]);
            services.Configure<JWTIssuer>(options =>
            {
                options.ValidFor = TimeSpan.FromMinutes(tokenExpirationMinutes);
                options.Issuer = jwtAppSettingOptions[nameof(JWTIssuer.Issuer)];
                options.Audience = jwtAppSettingOptions[nameof(JWTIssuer.Audience)];
                options.SigningCredentials = new SigningCredentials(_signingKey, SecurityAlgorithms.HmacSha256);
            });



            var duration = Configuration["AppSettings:ContentsApiClientCacheTimeOutSeconds"];

            int reponseCacheDuration = 60; //seconds
            if (!string.IsNullOrEmpty(duration))
            {
                reponseCacheDuration = Convert.ToInt32(duration);
            }
            // Add Caching Support
            services.AddMemoryCache();

            //load general configuration from appsettings.json
            services.Configure<IpRateLimitOptions>(Configuration.GetSection("IpRateLimiting"));

            //load ip rules from appsettings.json
            services.Configure<IpRateLimitPolicies>(Configuration.GetSection("IpRateLimitPolicies"));

            // inject counter and rules stores
            services.AddSingleton<IIpPolicyStore, MemoryCacheIpPolicyStore>();
            services.AddSingleton<IRateLimitCounterStore, MemoryCacheRateLimitCounterStore>();


            services.AddMvc(options =>
            {
                options.Filters.Add(endpointTracking);
                options.CacheProfiles.Add("Default",
                    new CacheProfile()
                    {
                        Duration = reponseCacheDuration,
                        Location = ResponseCacheLocation.Client,
                    });
                options.CacheProfiles.Add("token",
                    new CacheProfile()
                    {
                        Duration = (tokenExpirationMinutes - 1) * 60,
                        Location = ResponseCacheLocation.Client
                    });

                options.CacheProfiles.Add("Never",
                    new CacheProfile()
                    {
                        Location = ResponseCacheLocation.None,
                        NoStore = true
                    });

            });
            services.AddApplicationInsightsTelemetry(Configuration);
            services.AddDbContext<DBContext.InsightModels.InsightsContext>(options =>
            options.UseSqlServer(ConfigHelper.GetInsightsConnectionString(Configuration)));

            services.AddDbContext<DBContext.InsightsMetaDataModels.InsightsMetaDataContext>(options =>
                options.UseSqlServer(ConfigHelper.GetInsightsMetaDataConnectionString(Configuration)));

            services.AddDbContext<DBContext.InsightsDWModels.InsightsDWContext>(options =>
                options.UseSqlServer(ConfigHelper.GetInsightsDWConnectionString(Configuration)));
            //services.AddScoped(typeof(IRepository<>), typeof(Repository<>));
            services.AddTransient<ILoginService, LoginService>();
            services.AddTransient<IEnergyStarServices, EnergyStarServices>();
            services.AddTransient<IConfigurationService, ConfigurationService>();
            services.AddTransient<IMappingService, MappingService>();

            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new Info { Title = "PortalsWebApi", Version = "v1" });
            });

            // Adds a default in-memory implementation of IDistributedCache.
            services.AddDistributedMemoryCache();

            services.AddSession(options =>
            {
                // Set a short timeout for easy testing.
                options.IdleTimeout = TimeSpan.FromSeconds(300);
                options.CookieHttpOnly = false;
            });

            services.Configure<GzipCompressionProviderOptions>(options => options.Level = System.IO.Compression.CompressionLevel.Optimal);

            services.AddResponseCompression(options =>
            {
                options.MimeTypes = new[]
                {
                    // Default
                    "text/plain",
                    "text/css",
                    "application/javascript",
                    "text/html",
                    "application/xml",
                    "text/xml",
                    "application/json",
                    "text/json",
                    // Custom
                    "image/svg+xml"
                };
            });

        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
        {
            var jwtAppSettingOptions = Configuration.GetSection(nameof(JWTIssuer));
            app.UseResponseBuffering();//do not want response broken down into chunks
            var tokenValidationParameters = new TokenValidationParameters
            {
                ValidateIssuer = true,
                ValidIssuer = jwtAppSettingOptions[nameof(JWTIssuer.Issuer)],

                ValidateAudience = true,
                ValidAudience = jwtAppSettingOptions[nameof(JWTIssuer.Audience)],

                ValidateIssuerSigningKey = true,
                IssuerSigningKey = new SymmetricSecurityKey(Encoding.ASCII.GetBytes(Configuration["AppSettings:PurposeKey"])),

                RequireExpirationTime = true,
                ValidateLifetime = true,

                ClockSkew = TimeSpan.Zero
            };

            app.UseJwtBearerAuthentication(new JwtBearerOptions
            {
                AutomaticAuthenticate = true,
                AutomaticChallenge = true,
                TokenValidationParameters = tokenValidationParameters
            });

            //loggerFactory.AddConsole(Configuration.GetSection("Logging"));
            //loggerFactory.AddDebug();
            //loggerFactory.AddAzureWebAppDiagnostics();
            app.UseSession();
            loggerFactory.AddNLog();
            //add NLog.Web
            app.AddNLogWeb();

            LogManager.Configuration.Variables["dbConnectionstring"] =
                ConfigHelper.GetInsightsConnectionString(Configuration);

            app.UseIpRateLimiting();

            app.UseResponseCompression();
            // global policy - assign here or on each controller
            //UseCors() has to be called before UseMvc()
            //add this to controller if you want it just for a controller [EnableCors("CorsPolicy")]
            //this line sets it globally
            app.UseCors("AllowAllCorsPolicy");

            app.UseMvc();
            app.UseMvc(routes =>
            {
                routes.MapRoute(name: "API Default", template: "{controller}/{action}");

            });

            app.UseStaticFiles();

            app.UseSwagger();

            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "V1 Docs");
            });

        }
    }
}
