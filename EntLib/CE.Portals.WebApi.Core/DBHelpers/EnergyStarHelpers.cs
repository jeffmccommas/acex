﻿
using CE.Portals.DataService.Interfaces;
using CE.Portals.WebApi.Core.Controllers;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CE.InsightDataAccess.InsightModels;

namespace CE.Portals.WebApi.Core.DBHelpers
{
    public class EnergyStarHelpers
    {
        private readonly IEnergyStarServices _energyStarService;
        private readonly ILogger _logger;
        public EnergyStarHelpers(IEnergyStarServices energyStartServices, ILogger logger)
        {
            _energyStarService = energyStartServices;
            _logger = logger;
        }
        public List<AcceptedMeters> GetAcceptedMeters(int clientId)
        {  try
            {              
                var acceptedMeters = _energyStarService.GetAcceptedMeterIds(clientId);
                return acceptedMeters;

            }
            catch (Exception Ex)
            {
                _logger.LogError("GetAcceptedMeters returned HttpStatusCode.BadRequest- " + Ex.Message);
                throw Ex;
                
            }

        }
    }
}
