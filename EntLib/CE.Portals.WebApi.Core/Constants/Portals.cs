﻿namespace CE.Portals.WebApi.Core.Constants
{
    public class PortalsConstants
    {
        /*
        EnvID       Name    Description     SortOrder
        0	          prod       Prod	        0
        1	          uat        UAT	        1
        2	          qa         QA	            2
        3	          dev        Dev	        3
        4	          localdev   Local	        4
        */

        public const string AppSetting_CEEnvironmentDEV = "dev";
        public const string AppSetting_CEEnvironmentQA = "qa";
        public const string AppSetting_CEEnvironmentUAT = "uat";
        public const string AppSetting_CEEnvironmentPROD = "prod";
        public const string AppSetting_CEEnvironmentLOCALDEV = "localdev";
        public const string AppSetting_CEEnvironmentPERF = "localperf";

    }
}
