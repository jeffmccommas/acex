﻿using CE.Portals.DataService.Models;
using CE.Portals.Integrations.Common;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;

namespace CE.Portals.WebApi.Core.Helpers
{
    public static class QueryStringHelper
    {
        public static UserDetails GetLoginuser(HttpContext httpcontext)
        {
            if (httpcontext.Items.ContainsKey(ApplicationConstants.WEBTOKEN))
            {
                var loginUser = JsonConvert.DeserializeObject<UserDetails>(httpcontext.Items[ApplicationConstants.WEBTOKEN].ToString());
                return loginUser;
            }
            else
            {
                return null;
            }
        }
        
        public static string GetAccessToken(HttpContext httpcontext)
        {
            if (httpcontext.Items.ContainsKey("Authorization"))
            {
                return httpcontext.Items["Authorization"].ToString();
                
            }
            else
            {
                return null;
            }
        }
    }
}
