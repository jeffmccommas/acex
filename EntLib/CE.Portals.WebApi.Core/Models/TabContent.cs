﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CE.Portals.WebApi.Core.Models
{
    public class TabContent
    {
        public string Contacts { get; set; }
        public string Meters { get; set; }
    }
}
