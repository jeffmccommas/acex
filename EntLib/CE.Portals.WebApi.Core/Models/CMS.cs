﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;


namespace CE.Portals.WebApi.Core.Models
{
    public class CMS
    {
        private const string COMMON_CATEGORY = "common";
        private const string FIRST_NAME = "firstname";
        private const string ADDR1 = "addr1";

        public static RootContentObject GetContent(int clientId, string locale, AppSettings settings, ILogger logger)
        {
            var rm = new WebAPIResponseModel(settings, logger, null);
            rm.clientId = clientId.ToString();
            RootContentObject rt;
            var ct = rm.GetContent("refresh=false&IncludeContent=true", locale);
             rt = (RootContentObject)JsonConvert.DeserializeObject(ct.Content, typeof(RootContentObject), new JsonSerializerSettings
                {
                    Error = HandleDeserializationError
                });
           return rt;
        }

        private static void HandleDeserializationError(object sender, ErrorEventArgs errorArgs)
        {
            var currentError = errorArgs.ErrorContext.Error.Message;
            errorArgs.ErrorContext.Handled = true;
            throw new Exception(currentError);
        }

        public static string GetCommonContent(RootContentObject contentRoot, string key, string type = "")
            => type == "config" ? contentRoot.Content.Configuration.Where(x => x.category == COMMON_CATEGORY && x.key == key).Select(x => x.value).FirstOrDefault()
                                : contentRoot.Content.TextContent.Where(x => x.category == COMMON_CATEGORY && x.key == key).Select(x => x.shorttext).FirstOrDefault();

        public static string GetCommonContent(Content content, string key, string type = "")
            => type == "config" ? content.Configuration.Where(x => x.category == COMMON_CATEGORY && x.key == key).Select(x => x.value).FirstOrDefault()
                                : content.TextContent.Where(x => x.category == COMMON_CATEGORY && x.key == key).Select(x => x.shorttext).FirstOrDefault();

        public static string GetWidgetContent(Content content, string tabKey, string widgetType, string item, System.Linq.Expressions.Expression<Func<TextContent, bool>> predicate)
        {
            var k = tabKey + "." + widgetType + '.' + item;
            var cont = (from tc in content.TextContent.AsQueryable().Where(predicate)
                        select tc).ToList().Where(x => x.key == k).Select(x => x.shorttext).FirstOrDefault();
            return cont ?? "";
        }

        public static string GetWidgetConfig(Content content, string tabKey, string widgetType, string item, System.Linq.Expressions.Expression<Func<Configuration, bool>> predicate)
        {
            var k = tabKey + "." + widgetType + '.' + item;
            var config = (from c in content.Configuration.AsQueryable().Where(predicate)
                          select c).ToList().Where(x => x.key == k).Select(x => x.value).FirstOrDefault();
            return config ?? "";
        }
        public static string GetWidgetContent(List<TextContent> content, string tabKey, string widgetType, string item, string instance = "")
        {
            string k;
            if (!string.IsNullOrEmpty(instance))
            {
                k = tabKey + "." + widgetType + '.' + item + '.' + instance;
            }
            else
            {
                k = tabKey + "." + widgetType + '.' + item;
            }
            var cont = content.Where(x => x.key == k).Select(x => x.shorttext).FirstOrDefault();
            return cont ?? "";
        }
        public static string GetWidgetConfig(List<Configuration> config, string tabKey, string widgetType, string item, string instance = "")
        {
            string k;
            if (!string.IsNullOrEmpty(instance))
            {
                k = tabKey + "." + widgetType + '.' + item + '.' + instance;
            }
            else
            {
                k = tabKey + "." + widgetType + '.' + item;
            }
            var conf = config.Where(x => x.key == k).Select(x => x.value).FirstOrDefault();
            return conf ?? "";
        }

        public static string GetCommoditiesName(List<TextContent> textContent, string commodities)
        {
            var commoditiesText = string.Empty;
            foreach (var comNameKey in commodities.Replace(" ", "").Split(',').Select(co => "commodity." + co + ".name"))
            {
                if (string.IsNullOrEmpty(commoditiesText))
                {
                    commoditiesText = textContent.Where(e => e.key == comNameKey).Select(s => s.shorttext).FirstOrDefault();
                }
                else
                {
                    commoditiesText += "," + textContent.Where(e => e.key == comNameKey).Select(s => s.shorttext).FirstOrDefault();
                }
            }

            return commoditiesText;
        }
    }

    public class Action
    {
        public string key { get; set; }
        public string namekey { get; set; }
        public string conditionkeys { get; set; }
        public string descriptionkey { get; set; }
        public bool hide { get; set; }
        public string videokey { get; set; }
        public string imagekey { get; set; }
        public string iconclass { get; set; }
        public string commoditykey { get; set; }
        public string type { get; set; }
        public string appliancekeys { get; set; }
        public double defaultupfrontcost { get; set; }
        public double defaultannualcost { get; set; }
        public double defaultcostvariancepercent { get; set; }
        public string difficulty { get; set; }
        public string habitinterval { get; set; }
        public double defaultannualsavingsestimate { get; set; }
        public double defaultannualusagesavingsestimate { get; set; }
        public string defaultannualusagesavingsuomkey { get; set; }
        public int defaultpaybacktime { get; set; }
        public double defaultroi { get; set; }
        public bool rebateavailable { get; set; }
        public string rebateurl { get; set; }
        public string rebateimagekey { get; set; }
        public double rebateamount { get; set; }
        public string seasonkeys { get; set; }
        public double actionpriority { get; set; }
        public string savingscalcmethod { get; set; }
        public decimal savingsamount { get; set; }
        public string whatifdatakeys { get; set; }
        public string costexpression { get; set; }
        public string rebateexpression { get; set; }
        public string nextsteplink { get; set; }
        public string nextsteplinktext { get; set; }
        public string nextsteplinktype { get; set; }
        public string tags { get; set; }
    }

    public class ActionSaving
    {
        public string key { get; set; }
        public int rank { get; set; }
        public string conditions { get; set; }
        public double cost { get; set; }
        public double annualsavingsestimate { get; set; }
        public double annualusagesavingsestimate { get; set; }
        public string comments { get; set; }
    }

    public class Appliance
    {
        public string key { get; set; }
        public string namekey { get; set; }
        public double averagecostperyear { get; set; }
        public string profileattributes { get; set; }
        public string expressions { get; set; }
    }

    public class BenchmarkGroup
    {
        public string key { get; set; }
        public string namekey { get; set; }
        public string descriptionkey { get; set; }
        public bool hide { get; set; }
    }

    public class Commodity
    {
        public string key { get; set; }
        public string namekey { get; set; }
    }

    public class Condition
    {
        public string key { get; set; }
        public string category { get; set; }
        public string description { get; set; }
        public string profileattributekey { get; set; }
        public string @operator { get; set; }
        public string value { get; set; }
        public string profileoptionkey { get; set; }
    }

    public class Configuration
    {
        public string key { get; set; }
        public string category { get; set; }
        public string value { get; set; }
    }

    public class ConfigurationBulk
    {
        public string key { get; set; }
        public string category { get; set; }
        public string jsonConfiguration { get; set; }
    }

    public class Currency
    {
        public string key { get; set; }
        public string namekey { get; set; }
        public string symbolkey { get; set; }
    }

    public class EndUse
    {
        public string key { get; set; }
        public string namekey { get; set; }
        public string category { get; set; }
        public string appliances { get; set; }
    }

    public class Enumeration
    {
        public string enumerationkey { get; set; }
        public string category { get; set; }
        public string key { get; set; }
        public string namekey { get; set; }
        public int order { get; set; }
    }

    public class Expression
    {
        public string Key { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Formula { get; set; }
    }

    public class FileContent
    {
        public string key { get; set; }
        public string category { get; set; }
        public string smallfile { get; set; }
        public string smallfiletitle { get; set; }
        public string smallfiledescription { get; set; }
        public string smallfilefilename { get; set; }
        public int smallfilesize { get; set; }
        public string mediumfile { get; set; }
        public string mediumfiletitle { get; set; }
        public string mediumfiledescription { get; set; }
        public string mediumfilefilename { get; set; }
        public int mediumfilesize { get; set; }
        public string largefile { get; set; }
        public string largefiletitle { get; set; }
        public string largefiledescription { get; set; }
        public string largefilefilename { get; set; }
        public int largefilesize { get; set; }
    }

    public class Column
    {
        public int Span { get; set; }
        public string Size { get; set; }
        public List<WidgetList> Widgets { get; set; }
    }

    public class WidgetList
    {
        public string Name { get; set; }
        public string Instance { get; set; }
    }

    public class Row
    {
        public List<Column> Columns { get; set; }
    }

    public class Layout2
    {
        public List<Row> Rows { get; set; }
    }

    public class Layoutjson
    {
        public string TabKey { get; set; }
        public string LoadingText { get; set; }
        public string YellowFadeColor { get; set; }
        public string WidgetError { get; set; }
        public Layout2 Layout { get; set; }
        public string WidgetList { get; set; }
        public string ClientId;
        public string CustomerId;
        public string AccountId;
        public string PremiseId;
        public string PremiseType;
        public string ServicePointId;
        public string WebToken;
        public string Locale;
        public string Browser { get; set; }
        public string PartialBrowserSupport { get; set; }
        public string Parameters { get; set; }
        public string DeepLinkData { get; set; }
        public string DeepLinkWidget { get; set; }
        public string DeepLinkWidgetState { get; set; }
        public string DeepLinkType { get; set; }
        public string DeepLinkWidgetTab { get; set; }
        public string DeepLinkWidgetSection { get; set; }
        public string DeepLinkEnum { get; set; }
        public string DeepLinkActionKey { get; set; }
        public string DeepLinkResolution { get; set; }
        public string DeepLinkComparison { get; set; }
        public string Json { get; set; }
        public bool IsCsr { get; set; }
    }

    public class Layout
    {
        public string key { get; set; }
        public string description { get; set; }
        public Layoutjson layoutjson { get; set; }
    }

    public class Measurement
    {
        public string key { get; set; }
        public string namekey { get; set; }
    }

    public class ProfileAttribute
    {
        public string key { get; set; }
        public int minvalue { get; set; }
        public int maxvalue { get; set; }
        public bool donotdefault { get; set; }
        public string optionkeys { get; set; }
        public string type { get; set; }
        public string entitylevel { get; set; }
        public string questiontextkey { get; set; }
        public string disableconditionkeys { get; set; }
    }

    public class ProfileDefault
    {
        public string profileattributekey { get; set; }
        public string defaultprofileoptionkey { get; set; }
        public string defaultvalue { get; set; }
    }

    public class ProfileDefaultCollection
    {
        public string key { get; set; }
        public string namekey { get; set; }
        public string description { get; set; }
    }

    public class ProfileOption
    {
        public string key { get; set; }
        public double value { get; set; }
        public string namekey { get; set; }
        public string description { get; set; }
    }

    public class Season
    {
        public string key { get; set; }
        public string namekey { get; set; }
        public int startmonth { get; set; }
        public int startday { get; set; }
        public int endmonth { get; set; }
        public int endday { get; set; }
    }

    public class Tab
    {
        public string key { get; set; }
        public string name { get; set; }
        public int menuorder { get; set; }
        public string layoutkey { get; set; }
        public string tabtype { get; set; }
        public string namekey { get; set; }
        public string widgetkey { get; set; }
        public string url { get; set; }
        public string conditions { get; set; }
        public string configurations { get; set; }
        public string textcontentlist { get; set; }
        public string profilesections { get; set; }
        public string childtabs { get; set; }
        public bool isurlinternal { get; set; }
        public string tabiconclass { get; set; }
        public string images { get; set; }
        public string actions { get; set; }
        public string tabicontype { get; set; }
    }

    public class TextContent
    {
        public string key { get; set; }
        public string category { get; set; }
        public string shorttext { get; set; }
        public string mediumtext { get; set; }
        public string longtext { get; set; }
    }

    public class UOM
    {
        public string key { get; set; }
        public string namekey { get; set; }
    }

    public class Widget
    {
        public string key { get; set; }
        public string name { get; set; }
        public string tabkey { get; set; }
        public string widgettype { get; set; }
        public int column { get; set; }
        public int row { get; set; }
        public int order { get; set; }
        public string conditions { get; set; }
        public string configurations { get; set; }
        public string textcontentlist { get; set; }
    }

    public class ProfileSection
    {
        public string key { get; set; }
        public string titlekey { get; set; }
        public string subtitlekey { get; set; }
        public string descriptionkey { get; set; }
        public double rank { get; set; }
        public string profileattributes { get; set; }
        public string conditions { get; set; }
        public string images { get; set; }
        public string introtextkey { get; set; }
    }

    public class Content
    {
        public List<Action> Action { get; set; }
        public List<ActionSaving> ActionSavings { get; set; }
        public List<Appliance> Appliance { get; set; }
        public List<BenchmarkGroup> BenchmarkGroup { get; set; }
        public List<Commodity> Commodity { get; set; }
        public List<Condition> Condition { get; set; }
        public List<Configuration> Configuration { get; set; }
        public List<ConfigurationBulk> ConfigurationBulk { get; set; }
        public List<Currency> Currency { get; set; }
        public List<EndUse> EndUse { get; set; }
        public List<Enumeration> Enumeration { get; set; }
        public List<Expression> Expression { get; set; }
        public List<FileContent> FileContent { get; set; }
        public List<Layout> Layout { get; set; }
        public List<Measurement> Measurement { get; set; }
        public List<ProfileAttribute> ProfileAttribute { get; set; }
        public List<ProfileDefault> ProfileDefault { get; set; }
        public List<ProfileDefaultCollection> ProfileDefaultCollection { get; set; }
        public List<ProfileOption> ProfileOption { get; set; }
        public List<Season> Season { get; set; }
        public List<Tab> Tab { get; set; }
        public List<TextContent> TextContent { get; set; }
        public List<UOM> UOM { get; set; }
        public List<Widget> Widget { get; set; }
        public List<ProfileSection> ProfileSection { get; set; }
    }

    public class RootContentObject
    {
        public string Message { get; set; }
        public Content Content { get; set; }
    }

}
