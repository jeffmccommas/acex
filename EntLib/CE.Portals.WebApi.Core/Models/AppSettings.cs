﻿namespace CE.Portals.WebApi.Core.Models
{
    public class AppSettings
    {
        public string ConnectionString { get; set; }
        public string EventTrackingKey { get; set; }
        public string EventTrackingURL { get; set; }
        public string CEENvironment { get; set; }
        public string ESPMBaseURL { get; set; }
        public string APIBaseURL { get; set; }
        public string InsightsAPIBaseURL { get; set; }
        public string PurposeKey { get; set; }
        public string APIUserKey { get; set; }
        public string ContentsApiServerCacheTimeOutSeconds { get; set; }
        public string ContentsApiClientCacheTimeOutSeconds { get; set; }
        public string LogCallTimes { get; set; }


    }
}
