﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CE.Portals.WebApi.Core.Models
{
    public class MeterContent
    {
        public string Title { get; set; }
        public string AcceptedMeters { get; set; }
        public string AcceptMeters { get; set; }
        public string AccountAddress { get; set;}
        public string AccountId { get; set; }
        public string AccountJobTitle { get; set; }
        public string AccountName { get; set; }
        public string AccountPhone { get; set; }
        public string CustomField1 { get; set; }
        public string CustomField2{ get; set; }
        public string FirstBillDate { get; set; }
        public string Footer { get; set; }
        public string InUse { get; set; }
        public string Loading { get; set; }
        public string Metered { get; set; }
        public string MeterId { get; set; }
        public string MeterName { get; set; }
        public string MetersIntroText { get; set; }
        public string MetersTitle { get; set; }
        public string MeterType { get; set; }
        public string NoRecordFound { get; set; }
        public string NoSelection { get; set; }
        public string PendingMeters { get; set; }
        public string PropertyAddress { get; set; }
        public string PropertyBuildings { get; set; }
        public string PropertyConstructionStatus { get; set; }
        public string PropertyYearBuilt { get; set; }
        public string PropertyFloorArea { get; set; }
        public string PropertyId { get; set; }
        public string PropertyIsFederal { get; set; }
        public string PropertyName { get; set; }
        public string PropertyOccupancyPercentage { get; set; }
        public string PropertyPrimaryFunction { get; set; }
        public string RejectCancel { get; set; }
        public string RejectConfirm { get; set; }
        public string RejectConfirmationMessage { get; set; }
        public string RejectConfirmationTitle { get; set; }
        public string RejectMeters { get; set; }
        public string TimeStamp { get; set; }
        public string UnitOfMeasure { get; set; }
        public string LastUpdated { get; set; }
        public string LoadingPropertyDetails { get; set; }
        public string ErrorLoadingPropertyDetails { get; set; }
        public string MetersTab { get; set; }
        public string MetricsTab { get; set; }
        public string LoadingAccountDetails { get; set; }
        public string ErrorLoadingAccountDetails { get; set; }
        public string BillData { get; set; }
        public string ConsumptionRecords { get; set; }
        public string StartDate { get; set; }
        public string EndDate { get; set; }

        public string Usage { get; set; }
        public string Cost { get; set; }

        public string NoMatchingCustomerFound { get; set; }

        public string ErrorFetchingMatchCcustomer { get; set; }
        public string ConsumptionTab { get; set; }
        public string MappingTab { get; set; }
        public string MappedSuccessfully { get; set; }
        public string MappingNotfound { get; set; }
        public string MissingFields { get; set; }
        public string CustomDataMessage { get; set; }
        public string InvalidMapping { get; set; }
        public string NoErrorsMessage { get; set; }
        public string PremiseId { get; set; }
        public string MappedCustomer { get; set; }
        public string AddMapping { get; set; }
        public string Save { get; set; }
        public string Cancel { get; set; }
        public string AccountIdNotNull { get; set; }
        public string AccountIdUnique { get; set; }
        public string ClearFilters { get; set; }
        public string AccountOrganization { get; set; }

        public string NoAccessToProperty { get; set; }

        public string NoAccessToAccount { get; set; }

    }
    
    
}
