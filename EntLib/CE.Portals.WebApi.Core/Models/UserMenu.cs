﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CE.Portals.WebApi.Core.Models
{
    public class UserMenu
    {
        public string UserMenuSalutation { get; set; }
        public string ViewProfile { get; set; }
        public string ManageUsers { get; set; }
        public string Logout { get; set; }

    }
}
