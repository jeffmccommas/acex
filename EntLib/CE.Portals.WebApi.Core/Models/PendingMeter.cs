﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CE.Portals.WebApi.Core.Models
{
    public class PendingMeter
    {
        public string meterId { get; set; }
        public string customField1 { get; set; }
        public string customField2 { get; set; }
        public string type { get; set; }
        public string mappodId { get; set; }
        public string mapaccountId { get; set; }
        public Boolean isMapped { get; set; }
    }
}
