﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CE.Portals.WebApi.Core.Models
{
    public class UserProfile
    {
        public string UserProfileTitle { get; set; }
        public string Name { get; set; }
        public string UserName { get; set; }
        public string Administrator { get; set; }
        public string Status { get; set; }
        public string FailedLoginCount { get; set; }
    }
}
