﻿namespace CE.Portals.WebApi.Core.Models
{
    public class Contacts
    {
        public string AcceptButton { get; set; }
        public string AccountColumn { get; set; }
        public string AddesssColumn { get; set; }
        public string CustomField1Column { get; set; }
        public string CustomField2Column { get; set; }
        public string Introtext { get; set; }
        public string NameColumn { get; set; }
        public string NoDataMessage { get; set; }
        public string RejectButton { get; set; }
        public string StatusColumn { get; set; }
        public string TimeStampColumn { get; set; }
        public string Title { get; set; }
        public string AcceptedAccounts { get; set; }
        public string PendingAccounts { get; set; }
        public string RejectConfirm { get; set; }
        public string RejectCancel { get; set; }
        public string RejectConfirmMessage { get; set; }
        public string RejectConfirmTitle { get; set; }
        public string Footer { get; set; }
        public string NoRecordsFound { get; set; }
        public string NoSelection { get; set; }
        public string Loading { get; set; }
        
    }
}
