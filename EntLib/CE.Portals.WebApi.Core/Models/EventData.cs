﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CE.Portals.WebApi.Core.Models
{
    public class EventData
    {
        public string EventType { get; set; }
        public string AccountId { get; set; }
        public string PremiseId { get; set; }
        public string Locale { get; set; }
    }

    public class TraceLog
    {
        public string LogMessage { get; set; }
        public string LogLevel { get; set; }
      
    }

    
}
