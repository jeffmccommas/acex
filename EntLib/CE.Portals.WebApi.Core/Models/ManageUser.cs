﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CE.Portals.WebApi.Core.Models
{
    public class ManageUser
    {
        public string ManageUserTitle { get; set; }
        public string Name { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public string Administrator { get; set; }
        public string Status { get; set; }
        public string FailedLoginCount { get; set; }
        public string Edit { get; set; }
        public string Disable { get; set; }
        public string AddUser { get; set; }

        public string EnableUser { get; set; }
        public string EnableUserMessage { get; set; }
        public string DisableUserMessage { get; set; }
    }
}
