﻿using Newtonsoft.Json;
namespace CE.Portals.WebApi.Core.Models
{
    public class Account
    {
        public string username { get; set; }
        public string includeTestPropertiesInGraphics { get; set; }
        public AccountInfo accountInfo { get; set; }
        public string accountId { get; set; }

        public string errorMessage { get; set; }
        public ConnectionAudit connectionAudit { get; set; }
        public bool IsAcceptedAccount { get; set; } = false;
    }
    public class AccountInfo
    {
        public Address address { get; set; }
        public string email { get; set; }
        public string firstName { get; set; }
        public string phone { get; set; }
        public string lastName { get; set; }
        public string jobTitle { get; set; }
        public string organization { get; set; }

        public string CustomField1 { get; set; }
        public string CustomField2 { get; set; }
    }
    public class Address
    {
        public string address1 { get; set; }
        public string city { get; set; }
        public string state { get; set; }
        public string postalCode { get; set; }
        public string country { get; set; }
    }

    public class ConnectionAudit
    {
        public string createdBy { get; set; }
        public string createdByAccountId { get; set; }
        public string createdDate { get; set; }
        public string lastUpdatedBy { get; set; }
        public string lastUpdatedByAccountId { get; set; }
        public string lastUpdatedDate { get; set; }
    }
}
