﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CE.Portals.WebApi.Core.Models
{
    public class EditUser
    {
        public string EditUserTitle { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public string Administrator { get; set; }
        public string Status { get; set; }
        public string FailedLoginCount { get; set; }
        public string Save { get; set; }
        public string Cancel { get; set; }
        public string FirstNameRequired { get; set; }
        public string LastNameRequired { get; set; }
        public string UserNameRequired { get; set; }
        public string PasswordRequired { get; set; }
        public string UserAddedMessage { get; set; }
    }
}
