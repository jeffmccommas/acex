﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace CE.Portals.WebApi.Core.Models
{
    public class AcceptedAccountsList
    {
        public List<AcceptedAccount> values { get; set; }

        public int count { get; set; }
    }

    public class AcceptedAccount
    {
        //[JsonProperty(PropertyName = "@id")]
        public string id { get; set; }

        //[JsonProperty(PropertyName = "@hint")]
        public string hint { get; set; }

        public string httpMethod { get; set; }

        public string link { get; set; }

        public string linkDescription { get; set; }

    }
}
