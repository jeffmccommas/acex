﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CE.Portals.WebApi.Core.Models
{
    public class Login
    {
        public string UserName { get; set; }
        public string Password { get; set; }
        public string LoginLabel { get; set; }
        public string InvalidLogin  { get; set; }
        public string UserNameMessage { get; set; }
        public string PasswordMessage { get; set; }
        public string LockedUser { get; set; }
        public string MaxValidLogins { get; set; }

    }
}
