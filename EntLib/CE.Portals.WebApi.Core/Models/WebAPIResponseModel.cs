﻿using CE.Portals.Integrations.Models;
using CE.Portals.Integrations.API;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using Integration = CE.Portals.Integrations.Common;
using System.Net.Http;
using Newtonsoft.Json;
using CE.Portals.DataService.Models;
using System.Text;
using CE.InsightDataAccess.InsightsDWModels;
using CE.Portals.DataService.Interfaces;
using CE.Portals.WebApi.Core.Constants;
using CE.Portals.WebApi.Core.Controllers;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Logging;
using Microsoft.AspNetCore.Http;

namespace CE.Portals.WebApi.Core.Models
{
    public class WebAPIResponseModel
    {
        private readonly IMemoryCache _memoryCache;
        private static int _serverCacheTimeOut = 1800;

        private readonly AppSettings _appsettings;
        private Stopwatch _stopwatch = new Stopwatch();
        private bool _logcalltimes;
        private const string POST = "POST";
        private const string GET = "GET";
        private const string DELETE = "DELETE";
        public string clientId { get; set; }
        private readonly ILogger _logger;
        private readonly ILoginService _loginService;

        public WebAPIResponseModel(AppSettings appsettings, ILogger logger, IMemoryCache memoryCache)
        {
            _memoryCache = memoryCache;

            _logger = logger;
            _logcalltimes = false;
            _appsettings = appsettings;
            if (!string.IsNullOrEmpty(_appsettings.LogCallTimes))
            {
                if (_appsettings.LogCallTimes.ToLower() == "true")
                    _logcalltimes = true;
            }

            if (!string.IsNullOrEmpty(_appsettings.ContentsApiServerCacheTimeOutSeconds))
                _serverCacheTimeOut = Convert.ToInt32(_appsettings.ContentsApiServerCacheTimeOutSeconds) / 60;
        }
        public WebAPIResponseModel(AppSettings appsettings, ILogger logger, IMemoryCache memoryCache, ILoginService loginService)
        {
            _memoryCache = memoryCache;

            _logger = logger;
            _logcalltimes = false;
            _appsettings = appsettings;
            _loginService = loginService;
            if (!string.IsNullOrEmpty(_appsettings.LogCallTimes))
            {
                if (_appsettings.LogCallTimes.ToLower() == "true")
                    _logcalltimes = true;
            }

            if (!string.IsNullOrEmpty(_appsettings.ContentsApiServerCacheTimeOutSeconds))
                _serverCacheTimeOut = Convert.ToInt32(_appsettings.ContentsApiServerCacheTimeOutSeconds) / 60;
        }

        private void StartTimer()
        {
            if (_logcalltimes)
            {
                _stopwatch.Reset();
                _stopwatch.Start();
            }
        }
        private void StopTimer(string methodName)
        {
            if (_logcalltimes)
            {
                _stopwatch.Stop();
                _logger.LogInformation($"Call time for {methodName} : {_stopwatch.Elapsed}");
                _stopwatch.Reset();
            }
        }
        public ResponseModel PostEvents(string parameters, string locale, int clientId)
        {

            var query = new QueryTool
            {
                LocaleHeader = locale,
                Parameters = parameters,
                BasicKey = _appsettings.EventTrackingKey,
                BaseUrl = _appsettings.EventTrackingURL,
            };
            var str = PortalAPI.EventTrackingResponse(query);

            return str;
        }
        public ResponseModel GetAuthKeys(string parameters, string locale, PortalWebAPIType portalType)
        {
            StartTimer();
            var query = new QueryTool { LocaleHeader = locale };
            query = GetCurrentEnvironmentKeys(query, portalType, clientId);
            query.Parameters = parameters;
            query.SelectedMethodApi = GET;
            var response = PortalAPI.GetESPMSecretKeys(query);
            StopTimer("GetAuthKeys");
            return response;
        }
        public ResponseModel GetPendingAccounts(string parameters, PortalWebAPIType portalType, string locale, HttpMessageHandler httpmessageHandler = null)
        {
            StartTimer();
            var query = new QueryTool { LocaleHeader = locale };
            query = GetCurrentEnvironmentKeys(query, portalType, clientId);
            query.Parameters = parameters;
            query.SelectedMethodApi = GET;
            var response = EnergyStarAPI.GetAccountPendingList(query, httpmessageHandler);
            StopTimer("GetPendingAccounts");
            return response;
        }
        public ResponseModel GetAcceptedAccountDetails(string parameters, PortalWebAPIType portalType, string locale, HttpMessageHandler httpmessageHandler = null)
        {
            StartTimer();
            var query = new QueryTool { LocaleHeader = locale };
            query = GetCurrentEnvironmentKeys(query, portalType, clientId);
            query.Parameters = parameters;
            query.SelectedMethodApi = GET;
            var response = EnergyStarAPI.GetAcceptedAccountDetails(query, httpmessageHandler);
            StopTimer("GetAcceptedAccountDetails");
            return response;
        }

        public ResponseModel GetAcceptedAccountsList(PortalWebAPIType portalType, string locale, HttpMessageHandler httpmessageHandler = null)
        {
            StartTimer();
            var query = new QueryTool { LocaleHeader = locale };
            query = GetCurrentEnvironmentKeys(query, portalType, clientId);
            query.SelectedMethodApi = GET;
            var response = EnergyStarAPI.GetAccountAcceptedList(query, httpmessageHandler);
            StopTimer("GetAcceptedAccountsList");
            return response;
        }
        public ResponseModel AcceptAccounts(string parameters, PortalWebAPIType portalType, string locale, HttpMessageHandler httpmessageHandler = null)
        {
            StartTimer();
            var query = new QueryTool { LocaleHeader = locale };
            query = GetCurrentEnvironmentKeys(query, portalType, clientId);
            query.Parameters = parameters;
            query.SelectedMethodApi = GET;
            var response = EnergyStarAPI.AcceptAccounts(query, httpmessageHandler);
            StopTimer("AcceptAccounts");
            return response;
        }

        public ResponseModel AcceptedPropertyList(string parameters, PortalWebAPIType portalType, string locale, HttpMessageHandler httpmessageHandler = null)
        {
            StartTimer();
            var query = new QueryTool { LocaleHeader = locale };
            query = GetCurrentEnvironmentKeys(query, portalType, clientId);
            query.Parameters = parameters;
            query.SelectedMethodApi = GET;
            var response = EnergyStarAPI.AcceptedPropertyList(query, httpmessageHandler);
            StopTimer("AcceptedPropertyList");
            return response;
        }

        public ResponseModel AcceptedMeterList(string parameters, PortalWebAPIType portalType, string locale, HttpMessageHandler httpmessageHandler = null)
        {
            StartTimer();
            var query = new QueryTool { LocaleHeader = locale };
            query = GetCurrentEnvironmentKeys(query, portalType, clientId);
            query.Parameters = parameters;
            query.SelectedMethodApi = GET;
            var response = EnergyStarAPI.AcceptedMeterList(query, httpmessageHandler);
            StopTimer("AcceptedMeterList");
            return response;
        }

        public ResponseModel AcceptMeters(string parameters, PortalWebAPIType portalType, string locale, HttpMessageHandler httpmessageHandler = null)
        {
            StartTimer();
            var query = new QueryTool { LocaleHeader = locale };
            query = GetCurrentEnvironmentKeys(query, portalType, clientId);
            query.Parameters = parameters;
            query.SelectedMethodApi = GET;
            var response = EnergyStarAPI.AcceptMeters(query, httpmessageHandler);
            StopTimer("AcceptMeters");
            return response;
        }

        public ResponseModel RejectMeters(string parameters, PortalWebAPIType portalType, string locale, HttpMessageHandler httpmessageHandler = null)
        {
            StartTimer();
            var query = new QueryTool { LocaleHeader = locale };
            query = GetCurrentEnvironmentKeys(query, portalType, clientId);
            query.Parameters = parameters;
            query.SelectedMethodApi = GET;
            var response = EnergyStarAPI.RejectMeters(query, httpmessageHandler);
            StopTimer("RejectMeters");
            return response;
        }

        public ResponseModel RejectAccounts(string parameters, PortalWebAPIType portalType, string locale, HttpMessageHandler httpmessageHandler = null)
        {
            StartTimer();
            var query = new QueryTool { LocaleHeader = locale };
            query = GetCurrentEnvironmentKeys(query, portalType, clientId);
            query.Parameters = parameters;
            query.SelectedMethodApi = GET;
            var response = EnergyStarAPI.RejectAccounts(query, httpmessageHandler);
            StopTimer("RejectAccounts");
            return response;
        }

        public ResponseModel GetPendingMeters(string parameters, PortalWebAPIType portalType, string locale, HttpMessageHandler httpmessageHandler = null)
        {
            StartTimer();
            var query = new QueryTool { LocaleHeader = locale };
            query = GetCurrentEnvironmentKeys(query, portalType, clientId);
            query.Parameters = parameters;
            query.SelectedMethodApi = GET;
            var response = EnergyStarAPI.GetMetersPendingList(query, httpmessageHandler);
            StopTimer("GetPendingMeters");
            return response;
        }

        public ResponseModel GetMeterDetails(string parameters, PortalWebAPIType portalType, string locale, HttpMessageHandler httpmessageHandler = null)
        {
            StartTimer();
            var query = new QueryTool { LocaleHeader = locale };
            query = GetCurrentEnvironmentKeys(query, portalType, clientId);
            query.Parameters = parameters;
            query.SelectedMethodApi = GET;
            var response = EnergyStarAPI.GetMeterDetails(query, httpmessageHandler);
            StopTimer("GetMeterDetails");
            return response;
        }

        public ResponseModel GetPropertyList(string parameters, PortalWebAPIType portalType, string locale, HttpMessageHandler httpmessageHandler = null)
        {
            StartTimer();
            var query = new QueryTool { LocaleHeader = locale };
            query = GetCurrentEnvironmentKeys(query, portalType, clientId);
            query.Parameters = parameters;
            query.SelectedMethodApi = GET;
            var response = EnergyStarAPI.GetPropertyList(query, httpmessageHandler);
            StopTimer("GetPropertyList");
            return response;
        }

        public ResponseModel GetContent(string parameters, string locale)
        {
            StartTimer();
            var query = new QueryTool { LocaleHeader = locale };
            query = GetCurrentEnvironmentKeys(query, PortalWebAPIType.InsightsApi, clientId.ToString());
            query.Parameters = parameters;
            var str = InsightsAPI.GetContentResponse(query);
            StopTimer("GetContent");
            return str;
        }

        public ResponseModel GetAcceptedMeterList(string parameters, string locale, HttpMessageHandler httpmessageHandler = null)
        {
            StartTimer();
            var query = new QueryTool { LocaleHeader = locale };
            query = GetCurrentEnvironmentKeys(query, PortalWebAPIType.PortalApi, clientId.ToString());
            query.Parameters = parameters;
            var response = PortalAPI.GetAcceptedMeterList(query);
            StopTimer("GetAcceptedMeterList");
            return response;
        }

        public ResponseModel GetPropertyDetails(string parameters, PortalWebAPIType portalType, string locale, HttpMessageHandler httpmessageHandler = null)
        {
            StartTimer();
            var query = new QueryTool { LocaleHeader = locale };
            query = GetCurrentEnvironmentKeys(query, portalType, clientId);
            query.Parameters = parameters;
            query.SelectedMethodApi = GET;
            var response = EnergyStarAPI.GetPropertyDetails(query, httpmessageHandler);
            StopTimer("GetPropertyDetails");
            return response;
        }

        public ResponseModel GetCustomFields(string parameters, PortalWebAPIType portalType, string locale, HttpMessageHandler httpmessageHandler = null)
        {
            StartTimer();
            var query = new QueryTool { LocaleHeader = locale };
            query = GetCurrentEnvironmentKeys(query, portalType, clientId);
            query.Parameters = parameters;
            query.SelectedMethodApi = GET;
            var response = EnergyStarAPI.GetCustomFields(query, httpmessageHandler);
            StopTimer("GetCustomFields");
            return response;
        }
        public ResponseModel GetMeterConsumptionData(string parameters, PortalWebAPIType portalType, string locale, HttpMessageHandler httpmessageHandler = null)
        {
            StartTimer();
            var query = new QueryTool { LocaleHeader = locale };
            query = GetCurrentEnvironmentKeys(query, portalType, clientId);
            query.Parameters = parameters;
            query.SelectedMethodApi = GET;
            var response = EnergyStarAPI.GetMeterConsumptionData(query, httpmessageHandler);
            StopTimer("GetCustomFields");
            return response;
        }
        public ResponseModel PostMeterConsumptionData(string parameters, PortalWebAPIType portalType, string locale, List<MeterBillingData> meterBillingData, HttpMessageHandler httpmessageHandler = null)
        {
            StartTimer();
            var query = new QueryTool { LocaleHeader = locale };
            query = GetCurrentEnvironmentKeys(query, portalType, clientId);
            query.Parameters = parameters;
            query.SelectedMethodApi = POST;
            query.Content = "<meterData>";
            foreach (var meterBill in meterBillingData)
            {
                query.Content +=
                    $"<meterConsumption><usage>{meterBill.Usage}</usage><cost>{meterBill.Cost}</cost><startDate>{Convert.ToDateTime(meterBill.Startdate):yyyy-MM-dd}</startDate><endDate>{Convert.ToDateTime(meterBill.Enddate):yyyy-MM-dd}</endDate></meterConsumption>";
            }
            query.Content += "</meterData>";
            var response = EnergyStarAPI.PostMeterConsumptionData(query, httpmessageHandler);
            StopTimer("PostMeterConsumptionData");
            return response;
        }

        public ResponseModel GetBillingData(string parameters, PortalWebAPIType portalType, string locale, HttpMessageHandler httpmessageHandler = null)
        {
            StartTimer();
            var query = new QueryTool { LocaleHeader = locale };
            query = GetCurrentEnvironmentKeys(query, portalType, clientId);
            query.Parameters = parameters;
            query.SelectedMethodApi = GET;
            var response = InsightsAPI.GetBillingdata(query);
            StopTimer("GetBillingData");
            return response;
        }

        public ResponseModel GetBaseLineAndTarget(string parameters, PortalWebAPIType portalType, string locale, HttpMessageHandler httpmessageHandler = null)
        {
            StartTimer();
            var query = new QueryTool { LocaleHeader = locale };
            query = GetCurrentEnvironmentKeys(query, portalType, clientId);
            query.Parameters = parameters;
            query.SelectedMethodApi = GET;
            var response = EnergyStarAPI.GetBaseLineAndTarget(query);
            StopTimer("GetBaseLineAndTarget");
            return response;
        }
        public ResponseModel GetPropertyMetrics(string parameters, string headers, PortalWebAPIType portalType, string locale, HttpMessageHandler httpmessageHandler = null)
        {
            StartTimer();
            var query = new QueryTool { LocaleHeader = locale };
            query = GetCurrentEnvironmentKeys(query, portalType, clientId);
            query.Parameters = parameters;
            query.SelectedMethodApi = GET;
            query.Headers = new Dictionary<string, string>();
            query.Headers.Add("PM-Metrics", headers);
            var response = EnergyStarAPI.GetPropertyMetrics(query);
            StopTimer("GetPropertyMetrics");
            return response;
        }

        public ResponseModel GetPropertyMetricsNoScoreReason(string parameters, PortalWebAPIType portalType, string locale, HttpMessageHandler httpmessageHandler = null)
        {
            StartTimer();
            var query = new QueryTool { LocaleHeader = locale };
            query = GetCurrentEnvironmentKeys(query, portalType, clientId);
            query.Parameters = parameters;
            query.SelectedMethodApi = GET;
            var response = EnergyStarAPI.GetPropertyMetricsNoScoreReason(query);
            StopTimer("GetPropertyMetrics");
            return response;
        }

        private static string Base64Encode(string plainText)
        {

            var plainTextBytes = Encoding.UTF8.GetBytes(plainText);
            return Convert.ToBase64String(plainTextBytes);
        }
        private QueryTool GetCurrentEnvironmentKeys(QueryTool query, PortalWebAPIType portalType, string clientId)
        {
            var s = _appsettings.CEENvironment;

            if (clientId.Length > 0)
            {
                query.BaseUrl = _appsettings.APIBaseURL;
                switch (portalType)
                {
                    case PortalWebAPIType.ESPMApi:
                       
                        if (_appsettings.APIBaseURL.Length > 0)
                        {

                            string servercacheKey = "espmapi-envkey1-" + clientId;
                            string content = string.Empty;
                            if (!this._memoryCache.TryGetValue(servercacheKey, out content))
                            {
                                
                                    var authKey = GetAuthKey(this.clientId, Integration.ApplicationConstants.ESPMSecterKey);
                                    if (authKey != null)
                                    {
                                        content = authKey;
                                        this._memoryCache.Set(servercacheKey, content,
                                            new MemoryCacheEntryOptions
                                            {
                                                AbsoluteExpirationRelativeToNow =
                                                    (DateTime.Now.AddMinutes(_serverCacheTimeOut) - DateTime.Now)
                                            });
                                    }
                                
                            }

                            query.BasicKey = content;
                        }
                        break;
                    case PortalWebAPIType.InsightsApi:
                        if (_appsettings.APIBaseURL.Length > 0)
                        {
                            string servercacheKey = "espmapi-envkey-headers-" + clientId;
                            string content = string.Empty;
                            if (this._memoryCache != null)
                            {
                                if (this._memoryCache.TryGetValue(servercacheKey, out content))
                                {
                                    string[] headers = content.Split(',');
                                    query.selectedCEAccessKeyID = headers[0];
                                    query.BasicKey = headers[1];

                                }
                            }

                            if (string.IsNullOrEmpty(content))
                            {
                                var authHeaders = GetAuthHeaders(this.clientId);
                                if (authHeaders != null)
                                {
                                    
                                    query.selectedCEAccessKeyID = authHeaders.CeAccessKeyId;
                                    query.BasicKey = $"{Base64Encode(authHeaders.CeAccessKeyId + ":" + authHeaders.CeSecretAccessKey)}";

                                    string cachedata = authHeaders.CeAccessKeyId + "," + $"{Base64Encode(authHeaders.CeAccessKeyId + ":" + authHeaders.CeSecretAccessKey)}";

                                    this._memoryCache.Set(servercacheKey, cachedata, new MemoryCacheEntryOptions
                                    {
                                        AbsoluteExpirationRelativeToNow = (DateTime.Now.AddMinutes(_serverCacheTimeOut) - DateTime.Now)
                                    });

                                }
                            }

                        }
                        break;
                }


            }
            switch (portalType)
            {
                case PortalWebAPIType.ESPMApi: query.BaseUrl = _appsettings.ESPMBaseURL; break;
                case PortalWebAPIType.InsightsApi: query.BaseUrl = _appsettings.InsightsAPIBaseURL; break;
                case PortalWebAPIType.PortalApi: query.BaseUrl = _appsettings.APIBaseURL; break;
            }

            query.MessageIdHeader = "13";
            query.ChannelHeader = "CE";
            query.DateTime = DateTime.UtcNow;
            return query;
        }

        public UserDetails GetAuthHeaders(string clientId)
        {
            try
            {
                var env = 4;//local dev
                switch (_appsettings.CEENvironment)
                {
                    case PortalsConstants.AppSetting_CEEnvironmentDEV:
                        env = 3;
                        break;
                    case PortalsConstants.AppSetting_CEEnvironmentQA:
                        env = 2;
                        break;
                    case PortalsConstants.AppSetting_CEEnvironmentUAT:
                        env = 1;
                        break;
                    case PortalsConstants.AppSetting_CEEnvironmentPROD:
                        env = 0;
                        break;
                    case PortalsConstants.AppSetting_CEEnvironmentPERF:
                        env = 5;
                        break;

                }
                string servercacheKey = "clientdetails-" + clientId;
                string response = string.Empty;
                if (this._memoryCache.TryGetValue(servercacheKey, out response))
                {
                    Log(LogLevel.Debug, "(GetAuthHeaders) Returning client info from cache", "");
                    return JsonConvert.DeserializeObject<UserDetails>(response);
                }
                Log(LogLevel.Debug, "(GetAuthHeaders) Returning client info from DB", "");
                var loginUser = _loginService.GetInsightsUserCreds(Int32.Parse(clientId), _appsettings.APIUserKey, env);
                if (loginUser == null)
                {
                    Log(LogLevel.Error, "Auth Headers returned HttpStatusCode.Forbidden- No Auth keys found - appSettings.APIUserKey = " + _appsettings.APIUserKey, "");
                    
                }
                else
                {
                    response = JsonConvert.SerializeObject(loginUser);
                    this._memoryCache.Set(servercacheKey, response, new MemoryCacheEntryOptions { AbsoluteExpirationRelativeToNow = (DateTime.Now.AddMinutes(_serverCacheTimeOut) - DateTime.Now) });
                   
                }
                return (loginUser);

            }
            catch (Exception ex)
            {
                Log(LogLevel.Error, "GetPendingAccounts returned HttpStatusCode.BadRequest" + ex.Message, "");
                return null;
            }

        }

        private string GetAuthKey(string clientId, string key)
        {
            string secretKey = string.Empty;
            string servercacheKey = "ESPMAuthKey-" + clientId;
            try
            {
                if (!this._memoryCache.TryGetValue(servercacheKey, out secretKey))
                {
                    secretKey = _loginService.GetClientAppsettings(clientId, key);
                    this._memoryCache.Set(servercacheKey, secretKey,
                        new MemoryCacheEntryOptions
                        {
                            AbsoluteExpirationRelativeToNow =
                                (DateTime.Now.AddMinutes(_serverCacheTimeOut) - DateTime.Now)
                        });
                    Log(LogLevel.Debug, "(GetAuthKey) GetAuthKey returned from DB", "");
                }
                else
                {
                    Log(LogLevel.Debug, "(GetAuthKey) GetAuthKey returned from cache", "");
                }
                if (string.IsNullOrEmpty(secretKey))
                {
                    Log(LogLevel.Error, "GetAuthKey returned HttpStatusCode.Forbidden - No Auth keys found", "");
                    return null;
                }

                return secretKey;
            }
            catch (Exception ex)
            {
                Log(LogLevel.Error, "GetAuthKey returned HttpStatusCode.BadRequest" + ex.Message, "");
                return null;
            }

        }

        private void Log(LogLevel level, string message, string userId)
        {
            switch (level)
            {
                case LogLevel.Error: _logger.LogError(message); break;
                case LogLevel.Critical: _logger.LogCritical(message); break;
                case LogLevel.Warning: _logger.LogWarning(message); break;
                case LogLevel.Information: _logger.LogInformation(message); break;
                case LogLevel.Debug: _logger.LogDebug(message); break;
                case LogLevel.Trace: _logger.LogTrace(message); break;
            }

        }
    }
}
