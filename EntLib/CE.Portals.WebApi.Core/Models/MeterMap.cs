﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CE.Portals.WebApi.Core.Models
{
    public class MeterMap
    {
        public string Customer { get; set; }
        public string AccountId { get; set; }
        public string PremiseId { get; set; }
    }
    
}
