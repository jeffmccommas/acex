﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CE.Portals.WebApi.Core.Models
{

    public class AcceptedPropertiesList
    {
        public List<AcceptedProperties> values { get; set; }

        public int count { get; set; }
    }
    public class AcceptedProperties
    {
        //[JsonProperty(PropertyName = "@id")]
        public string id { get; set; }

        //[JsonProperty(PropertyName = "@hint")]
        public string hint { get; set; }

        public string httpMethod { get; set; }

        public string link { get; set; }

        public string linkDescription { get; set; }
        
    }
}
