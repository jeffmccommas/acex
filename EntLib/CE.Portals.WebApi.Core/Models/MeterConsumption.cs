﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CE.Portals.WebApi.Core.Models
{
    public class MeterConsumption
    {
        public string Id { get; set; }
        public Audit Audit { get; set; }
        public string StartDate { get; set; }
        public string EndDate { get; set; }
        public string Usage { get; set; }
        public string _EstimatedValue { get; set; }
    }
}
