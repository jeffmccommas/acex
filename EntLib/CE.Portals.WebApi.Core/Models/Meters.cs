﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CE.Portals.WebApi.Core.Models
{


public class CustomField
{
    public string Text { get; set; }
    public string Value { get; set; }
}

public class CustomFieldList
{
    public List<CustomField> customField { get; set; }
}


public class GrossFloorArea
{
    public string units { get; set; }
    public string temporary { get; set; }
    public string @default { get; set; }
    public string value { get; set; }
}

public class Audit
{
    public string createdBy { get; set; }
    public string createdByAccountId { get; set; }
    public string createdDate { get; set; }
    public string lastUpdatedBy { get; set; }
    public string lastUpdatedByAccountId { get; set; }
    public string lastUpdatedDate { get; set; }
}

public class PropertyInfo
{
    public string name { get; set; }
    public Address address { get; set; }
    public string numberOfBuildings { get; set; }
    public string constructionStatus { get; set; }
    public string primaryFunction { get; set; }
    public string yearBuilt { get; set; }
    public GrossFloorArea grossFloorArea { get; set; }
    public string occupancyPercentage { get; set; }
    public string isFederalProperty { get; set; }
    public Audit audit { get; set; }
}

public class Audit2
{
    public string createdBy { get; set; }
    public string createdByAccountId { get; set; }
    public string createdDate { get; set; }
    public string lastUpdatedBy { get; set; }
    public string lastUpdatedByAccountId { get; set; }
    public string lastUpdatedDate { get; set; }
}

public class MeterInfo
{
    public string name { get; set; }
    public string metered { get; set; }
    public string firstBillDate { get; set; }
    public string inUse { get; set; }
    public Audit2 audit { get; set; }
    public string type { get; set; }
    public string unitOfMeasure { get; set; }
}

public class ShareAudit
{
    public string createdBy { get; set; }
    public string createdByAccountId { get; set; }
    public string createdDate { get; set; }
    public string lastUpdatedBy { get; set; }
    public string lastUpdatedByAccountId { get; set; }
    public string lastUpdatedDate { get; set; }
}

public class Meter
{
    public string meterId { get; set; }
    public string propertyId { get; set; }
    public string accountId { get; set; }
    public CustomFieldList customFieldList { get; set; }
    public string accessLevel { get; set; }
    public PropertyInfo propertyInfo { get; set; }
    public MeterInfo meterInfo { get; set; }
    public ShareAudit shareAudit { get; set; }
    public string errorMessage { get; set; }
    public Boolean isMapped { get; set; }

    public Meter()
    {
    }
}

public class MeterList
    {
        public string linkDescription { get; set; }
        public string link { get; set; }
    }


    public class MeterDetails
    {
        public string id { get; set; }
        public string type { get; set; }
        public string name { get; set; }
        public string metered { get; set; }
        public string unitOfMeasure { get; set; }
        public string firstBillDate { get; set; }
        public string inUse { get; set; }
        public string accessLevel { get; set; }

        public Audit audit { get; set; }
    }

    public class MeterDetailsCollection
    {
        public List<MeterDetails> values;
        public int count;
    }

    
}
