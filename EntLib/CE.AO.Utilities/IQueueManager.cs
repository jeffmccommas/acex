﻿using System.Threading.Tasks;

namespace CE.AO.Utilities
{
    public interface IQueueManager
    {
        Task<bool> CreateQueueAsync(string queueName);
        Task DeleteQueueAsync(string queueName);
        Task AddQueueAsync(string queueName, string message);
    }
}
