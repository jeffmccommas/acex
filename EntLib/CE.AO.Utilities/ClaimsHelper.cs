﻿using System.Diagnostics;
using System.Linq;
using System.Security.Claims;
using System.Web;

namespace CE.AO.Utilities
{
    /// <summary>
    /// Helper class for retrieving specific claims from the current ClaimIdentity.
    /// </summary>
    public static class ClaimHelper
    {
        /// <summary>
        /// Will return the value of the Claim that matches the claimType associated with the users ClaimIdentity.  If claim does not exist
        /// it will return null.
        /// 
        /// Note, if we need to use the helper in a non-web context, then we will have to update this class to retrieve IClaimsIdentity from
        /// the Thread.CurrentPrincipal and make sure that all of our web applications are setting the Thread.CurrentPrincipal based on the
        /// HttpContext.Current.User.
        /// </summary>
        /// <param name="claimType">the type of claim</param>
        public static string GetClaim(string claimType)
        {
            var claimsIdentity = GetClaimsIdentity();

            Claim claim = null;

            if (claimsIdentity != null)
                claim = claimsIdentity.Claims.SingleOrDefault(c => c.Type == claimType);

            if (claim != null)
                return claim.Value;

            return null;
        }

        public static ClaimsIdentity GetClaimsIdentity()
        {
            ClaimsPrincipal claimsPrincipal = null;

            if (HttpContext.Current != null && HttpContext.Current.User != null)
                claimsPrincipal = HttpContext.Current.User as ClaimsPrincipal;

            if (claimsPrincipal != null)
                return ((ClaimsIdentity)claimsPrincipal.Identity);

            return null;
        }

        /// <summary>
        /// Will return an array of claim values matching the claimType requested from the users ClaimIdentity.  If no matching claims 
        /// are found, it will return an empty array.
        /// </summary>
        /// <param name="claimType">the type of claim</param>
        public static string[] GetClaims(string claimType)
        {
            var claimsIdentity = GetClaimsIdentity();

            if (claimsIdentity != null)
                return claimsIdentity.Claims.Where(c => c.Type == claimType).Select(c => c.Value).ToArray();

            return new string[] { };
        }
    }
}
