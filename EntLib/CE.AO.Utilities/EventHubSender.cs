﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.ServiceBus.Messaging;

namespace CE.AO.Utilities
{
    public static class EventHubSender
    {
        /// <summary>
        /// This method checks the size of the event data and based on that it sends the event in batch.
        /// </summary>
        /// <param name="client"></param>
        /// <param name="eventsList"></param>
        public static void SendEventsInBatch(EventHubClient client, List<EventData> eventsList)
        {
            var eventData = eventsList.First();
            var totalSizeOfEvents = eventData.SerializedSizeInBytes * eventsList.Count;
            if (totalSizeOfEvents > 250000) //Events size is greater >  250000 bytes
            {
                var maxEventsCanBeSent = Convert.ToInt32(eventsList.Count * 250000 / totalSizeOfEvents);
                var loopCount = Convert.ToInt32(eventsList.Count / maxEventsCanBeSent);
                var totalEventSent = 0;
                for (var i = 0; i <= loopCount - 1; i++)
                {
                    var events = eventsList.Skip(i * maxEventsCanBeSent).Take(maxEventsCanBeSent).ToList();
                    if (events.Count > 0) client.SendBatch(events);
                    totalEventSent = (i + 1) * maxEventsCanBeSent;
                }
                if (totalEventSent != eventsList.Count)
                {
                    var events = eventsList.Skip(totalEventSent).ToList();
                    if (events.Count > 0) client.SendBatch(events);
                }
            }
            else
            {
                if (eventsList.Count > 0) client.SendBatch(eventsList);
            }
        }

        /// <summary>
        /// Get Event Hub client
        /// </summary>
        /// <param name="connectionString">Event Hub connection string</param>
        /// <param name="path">The path to the Event Hub</param>
        /// <returns>EventHubClient</returns>
        public static EventHubClient GetEventHubClient(string connectionString, string path)
        {
            var messagingFactory = MessagingFactory.CreateFromConnectionString(connectionString);
            messagingFactory.GetSettings().OperationTimeout = TimeSpan.FromHours(1);
            var client = messagingFactory.CreateEventHubClient(path);
            return client;
        }
    }
}