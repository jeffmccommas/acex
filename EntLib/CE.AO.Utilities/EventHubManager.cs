﻿using System;
using System.Diagnostics;
using Microsoft.Azure;
using Microsoft.ServiceBus;
using Microsoft.ServiceBus.Messaging;

namespace CE.AO.Utilities
{
    /// <summary>
    /// This class has the responsibility for managing Event Hub
    /// </summary>
    public static class EventHubManager
    {
        /// <summary>
        /// Get Service Bus Connection string
        /// </summary>
        /// <returns>connection string</returns>
        public static string GetServiceBusConnectionString(string configKeyName)
        {
            var connectionString = CloudConfigurationManager.GetSetting(configKeyName);

            if (string.IsNullOrEmpty(connectionString))
            {
                return string.Empty;
            }

            var builder = new ServiceBusConnectionStringBuilder(connectionString)
            {
                TransportType = TransportType.Amqp
            };
            return builder.ToString();
        }

        /// <summary>
        /// Creates a new instance of ServiceBus.NamespaceManager using a specified connection string
        /// </summary>
        /// <param name="connectionString">Connection string</param>
        /// <returns>A new instance of ServiceBus.NamespaceManager</returns>
        public static NamespaceManager GetNamespaceManager(string connectionString)
        {
            return NamespaceManager.CreateFromConnectionString(connectionString);
        }

        /// <summary>
        /// Creates Event Hub if not exists
        /// </summary>
        /// <param name="eventHubName">Name of Event Hub</param>
        /// <param name="numberOfPartitions">Number of partitions in Event Hub</param>
        /// <param name="manager">Instance of ServiceBus.NamespaceManager</param>
        public static void CreateEventHubIfNotExists(string eventHubName, int numberOfPartitions, NamespaceManager manager)
        {
            try
            {
                // Create the Event Hub
                var ehd = new EventHubDescription(eventHubName) { PartitionCount = numberOfPartitions };
                manager.CreateEventHubIfNotExistsAsync(ehd).Wait();
            }
            catch (AggregateException agexp)
            {
                Trace.WriteLine(agexp.Flatten());
            }
        }

        /// <summary>
        /// Creates Event Hub if not exists
        /// </summary>
        public static ConsumerGroupDescription CreateEventHubIfNotExists(string eventHubName, int numberOfPartitions, string connectionStringKeyName, string consumerGroupName)
        {
            ConsumerGroupDescription consumerGroupDescription = null;
            try
            {
                //Get AMQP connection string
                var connectionString = GetServiceBusConnectionString(connectionStringKeyName);

                //Create event hub if it does not exist
                var namespaceManager = GetNamespaceManager(connectionString);
                CreateEventHubIfNotExists(eventHubName, numberOfPartitions, namespaceManager);

                if (!string.IsNullOrWhiteSpace(consumerGroupName))
                    consumerGroupDescription = namespaceManager.CreateConsumerGroupIfNotExists(eventHubName, consumerGroupName);
            }
            catch (Exception ex)
            {
                Trace.WriteLine(ex.Message);
            }

            return consumerGroupDescription;
        }

        /// <summary>
        /// Method to Delete Event Hub
        /// </summary>
        /// <param name="eventHubName"></param>
        /// <param name="manager"></param>
        public static void DeleteEventHub(string eventHubName, NamespaceManager manager)
        {
            try
            {
                // Delete the Event Hub
                manager.DeleteEventHub(eventHubName);
            }
            catch (Exception exp)
            {
                Trace.WriteLine(exp.Message);
            }
        }
    }
}
