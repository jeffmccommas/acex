﻿namespace CE.AO.Utilities
{
    public static partial class Enums
    {
        public enum Severity
        {
            Info,
            Fatal,
            Warn,
            Error
        }
    }
}