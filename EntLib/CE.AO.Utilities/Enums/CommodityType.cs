﻿namespace CE.AO.Utilities
{
    public static partial class Enums
    {
        public enum CommodityType
        {
            Undefined = 0,
            Electric = 1,
            Gas = 2,
            Water = 3,
            Sewer = 4,
            Refuse = 5,
            Oil = 6,
            Propane = 7,
            Tv = 8,
            Fire = 9,
            Transportation = 10,
            Other = 99
        }
    }
}