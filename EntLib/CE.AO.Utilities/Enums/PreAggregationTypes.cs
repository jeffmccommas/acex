﻿namespace CE.AO.Utilities
{
    public static partial class Enums
    {
        /// <summary>
        /// Enum for defining type of pre aggregations
        /// </summary>
        public enum PreAggregationTypes
        {
            Day,
            Hour
        }
    }
}