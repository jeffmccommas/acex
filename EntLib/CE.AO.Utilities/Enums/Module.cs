﻿using System.ComponentModel;
using System.Diagnostics.CodeAnalysis;

namespace CE.AO.Utilities
{
    public static partial class Enums
    {
        [SuppressMessage("ReSharper", "InconsistentNaming")]
        public enum Module
        {
            [Description("No Module Assigned")]
            NoModuleAssigned,
            [Description("File Processing")]
            FileProcessing,
            [Description("Event Processing")]
            EventProcessing,
            [Description("AMI Batch Process")]
            AMIBatchProcess,
            [Description("Billing Batch Process")]
            BillingBatchProcess,
            [Description("Account Updates Batch Process")]
            AccountUpdatesBatchProcess,
            [Description("Subscription Batch Process")]
            SubscriptionBatchProcess,
            [Description("Ncc Batch Process")]
            NccBatchProcess,
            [Description("Ncc Consumption Batch Process")]
            NccConsumptionBatchProcess,
            [Description("Ncc Ami Calculation Batch Process")]
            NccAmiCalculationBatchProcess,
            [Description("Meter Account Mtu Batch Process")]
            MeterAccountMtuBatchProcess,
            [Description("Meter Read Batch Process")]
            MeterReadBatchProcess,
            [Description("Meter Read Calculation Batch Process")]
            MeterReadCalculationBatchProcess,
            [Description("Dcu Provisioning Batch Process")]
            DcuProvisioningBatchProcess,
            [Description("Dcu Alarm Batch Process")]
            DcuAlarmBatchProcess,
            [Description("Dcu Call Data Batch Process")]
            DcuCallDataBatchProcess,
            [Description("Alarm Batch Process")]
            AlarmBatchProcess,
            [Description("AMI Event Process")]
            AMIEventProcess,
            [Description("Billing Event Process")]
            BillingEventProcess,
            [Description("Account Updates Event Process")]
            AccountUpdatesEventProcess,
            [Description("Subscription Event Process")]
            SubscriptionEventProcess,
            [Description("Ncc Event Process")]
            NccEventProcess,
            [Description("Ncc Consumption Event Process")]
            NccConsumptionEventProcess,
            [Description("Ncc Ami Calculation Event Process")]
            NccAmiCalculationEventProcess,
            [Description("Meter Account Mtu Event Process")]
            MeterAccountMtuEventProcess,
            [Description("Meter Read Event Process")]
            MeterReadEventProcess,
            [Description("Meter Read Calculation Event Process")]
            MeterReadCalculationEventProcess,
            [Description("Dcu Provisioning Event Process")]
            DcuProvisioningEventProcess,
            [Description("Dcu Alarm Event Process")]
            DcuAlarmEventProcess,
            [Description("Dcu Call Data Event Process")]
            DcuCallDataEventProcess,
            [Description("Cost To Date Calculation")]
            CostToDateCalculation,
            [Description("Bill To Date Calculation")]
            BillToDateCalculation,
            [Description("Insight Evaluation")]
            InsightEvaluation,
            [Description("Notification")]
            Notification,
            [Description("Mdm Integration For Lus")]
            MdmIntegrationForLus,
            [Description("Calculation")]
            Calculation,
            [Description("Notification Stats")]
            NotificationStats,
            [Description("Subscription Api")]
            SubscriptionApi,
            [Description("Alarm Event Process")]
            AlarmEventProcess,
            [Description("Export Process")]
            ExportProcess,
            [Description("Pre Agg Calculation")]
            PreAggCalculation,
            [Description("Validation Estimation Process")]
            ValidationEstimationProcess
        }
    }
}