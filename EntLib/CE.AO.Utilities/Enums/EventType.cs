﻿namespace CE.AO.Utilities
{
    public static partial class Enums
    {
        public enum EventType
        {
            None,
            Ami,
            Bill,
            AccountUpdate,
            Subscription,
            NccAmiReading,
            NccAmiConsumption,
            NccAmiCalculation,
            MeterAccountMtu,
            MeterRead,
            MeterReadTriggerCalculation,
            DcuProvisioningData,
            DcuAlarm,
            DcuCallData,
            Alarm
        }
    }
}