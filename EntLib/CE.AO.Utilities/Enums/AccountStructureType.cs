﻿namespace CE.AO.Utilities
{
    public static partial class Enums
    {
        public enum AccountStructureType : byte
        {
            AccountHasOnePremise = 1,
            AccountHasMultiplePremises = 2,
            PremiseHasMultipleAccounts = 3
        }
    }
}