﻿namespace CE.AO.Utilities
{
    public static partial class Enums
    {
        public enum IntervalType
        {
            None,
            Fifteen = 15,
            Thirty = 30,
            Sixty = 60,
            Daily = 24
        }
    }
}