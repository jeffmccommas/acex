﻿using System.Diagnostics.CodeAnalysis;

namespace CE.AO.Utilities
{
    public static partial class Enums
    {
        [SuppressMessage("ReSharper", "InconsistentNaming")]
        public enum MeterType
        {
            ami,
            regular,
            nonmetered
        }
    }
}