﻿namespace CE.AO.Utilities
{
    public static partial class Enums
    {
        public enum BillPeriodType
        {
            Monthly = 1,
            BiMonthly = 2,
            Quarterly = 3
        }
    }
}