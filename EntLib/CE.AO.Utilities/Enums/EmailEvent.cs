﻿using System.Diagnostics.CodeAnalysis;
// ReSharper disable once CheckNamespace
namespace CE.AO.Utilities
{
    public static partial class Enums
    {
        [SuppressMessage("ReSharper", "InconsistentNaming")]
        public enum EmailEvent
        {
            delivered,
            bounce,
            open,
            click,
            unsubscribe,
            dropped,
            processed, // sendgrid action event, but not used
            deferred, // sendgrid action event, but not used
            spamreport, // sendgrid action event, but not used
            group_unsubscribe, // sendgrid action event, but not used
            group_resubscribe // sendgrid action event, but not used
        }
    }
}