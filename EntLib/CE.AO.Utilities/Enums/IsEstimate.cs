﻿namespace CE.AO.Utilities
{
    public static partial class Enums
    {
        public enum IsEstimate : byte
        {
            False = 0,
            True = 1
        }
    }
}