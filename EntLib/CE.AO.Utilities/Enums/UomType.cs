﻿using System.ComponentModel;
using System.Diagnostics.CodeAnalysis;

namespace CE.AO.Utilities
{
    public static partial class Enums
    {
        [SuppressMessage("ReSharper", "InconsistentNaming")]
        public enum UomType
        {
            Other = 99,
            kwh = 0,
            therms = 1,
            ccf = 2,
            [Description("Cu. Ft.")]
            cf = 3,
            cgal = 4,
            hgal = 5,
            [Description("GALS.")]
            gal = 6,
            kgal = 7,
            hcf = 8,
            lbs = 9,
            trees = 10,
            [Description("CU. MTR.")]
            cm = 11,
            mcf = 12
        }
    }
}