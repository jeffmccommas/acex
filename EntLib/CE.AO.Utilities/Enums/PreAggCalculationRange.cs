﻿namespace CE.AO.Utilities
{
    public static partial class Enums
    {
        /// <summary>
        /// Enum for Calulation Ranges while doing pre aggregations
        /// </summary>
        public enum PreAggCalculationRange
        {
            Day,
            Hour
        }
    }
}