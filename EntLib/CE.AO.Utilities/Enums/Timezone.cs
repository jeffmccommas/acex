﻿using System.ComponentModel;
using System.Diagnostics.CodeAnalysis;

namespace CE.AO.Utilities
{
    public static partial class Enums
    {
        [SuppressMessage("ReSharper", "InconsistentNaming")]
        public enum Timezone
        {
            [Description("Atlantic Standard Time")]
            ADT,
            [Description("Atlantic Standard Time")]
            AST,
            [Description("Alaskan Standard Time")]
            AKDT,
            [Description("Alaskan Standard Time")]
            AKST,
            [Description("Central Standard Time")]
            CDT,
            [Description("Central Standard Time")]
            CST,
            [Description("Eastern Standard Time")]
            EDT,
            [Description("Eastern Standard Time")]
            EST,
            [Description("Greenwich Standard Time")]
            GMT,
            [Description("Hawaiian Standard Time")]
            HADT,
            [Description("Hawaiian Standard Time")]
            HAST,
            [Description("Mountain Standard Time")]
            MDT,
            [Description("Mountain Standard Time")]
            MST,
            [Description("Pacific Standard Time")]
            PDT,
            [Description("Pacific Standard Time")]
            PST,
            [Description("UTC")]
            UTC
        }
    }
}