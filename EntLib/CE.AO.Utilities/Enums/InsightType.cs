﻿namespace CE.AO.Utilities
{
    public static partial class Enums
    {
        public enum InsightType
        {
            Cost,
            ProjectedCost,
            Usage,
            AccountLevelCostThreshold,
            ServiceLevelUsageThreshold,
            ServiceLevelCostThreshold,
            DayThreshold,
            ServiceLevelTieredThresholdApproaching,
            ServiceLevelTieredThresholdExceed
        }
    }
}