﻿namespace CE.AO.Utilities
{
    public static partial class Enums
    {
        public enum AccountUpdateType
        {
            RateClass,
            Email,
            Phone1,
            Address1,
            State,
            Zip,
            FirstName,
            LastName,
            MeterReplacement,
            NewMeter,
            BillCycle,
            RemoveMeter,
            RemoveAccount,
            RemoveService,
            RemovePremise,
            ReadStartDate,
            ReadEndDate,
            NewServiceContract,
            NewServicePoint
        }
    }
}