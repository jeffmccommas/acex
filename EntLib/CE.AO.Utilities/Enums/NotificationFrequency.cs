﻿namespace CE.AO.Utilities
{
    public static partial class Enums
    {
        public enum NotificationFrequency
        {
            Daily,
            Weekly,
            Monthly,
            Immediate
        }
    }
}