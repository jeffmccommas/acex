﻿namespace CE.AO.Utilities
{
    public static partial class Enums
    {
        public enum Direction
        {
            Forward = 0,
            Reverse = 1
        }
    }
}