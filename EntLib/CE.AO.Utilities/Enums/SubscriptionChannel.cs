﻿using System.Diagnostics.CodeAnalysis;

namespace CE.AO.Utilities
{
    public static partial class Enums
    {
        [SuppressMessage("ReSharper", "InconsistentNaming")]
        public enum SubscriptionChannel
        {
            Email,
            SMS,
            EmailAndSMS,
            File
        }
    }
}