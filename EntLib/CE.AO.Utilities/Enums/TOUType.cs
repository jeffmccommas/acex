﻿namespace CE.AO.Utilities
{
    public static partial class Enums
    {
        // ReSharper disable once InconsistentNaming
        public enum TOUType
        {
            Regular = 0,
            OnPeak = 1,
            OffPeak = 2,
            Shoulder1 = 3,
            Shoulder2 = 4,
            CriticalPeak = 5
        }
    }
}