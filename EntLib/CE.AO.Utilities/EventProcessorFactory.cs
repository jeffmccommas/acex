﻿using Microsoft.Practices.Unity;
using Microsoft.ServiceBus.Messaging;

namespace CE.AO.Utilities
{
    /// <summary>
    /// Class implementing IEventProcessorFactory use for dependency injection.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class EventProcessorFactory<T> : IEventProcessorFactory where T : IEventProcessor
    {
        private readonly IUnityContainer _unityContainer;

        public EventProcessorFactory(IUnityContainer unityContainer)
        {
            _unityContainer = unityContainer;
        }

        public IEventProcessor CreateEventProcessor(PartitionContext context)
        {
            return _unityContainer.Resolve<T>();
        }

    }
}
