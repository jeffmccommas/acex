﻿using System;
using System.Configuration;
using System.Threading.Tasks;



using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Queue;
using Microsoft.WindowsAzure.Storage.RetryPolicies;

namespace CE.AO.Utilities
{
    public class QueueManager : IQueueManager
    {
        protected readonly CloudQueueClient QueueClient;

        public QueueManager()
        {
            var connectionString = ConfigurationManager.AppSettings.Get("AzureStorageConnectionString");
            var storageAccount = CloudStorageAccount.Parse(connectionString);
            QueueClient = storageAccount.CreateCloudQueueClient();
            QueueClient.DefaultRequestOptions.MaximumExecutionTime = TimeSpan.FromHours(24);
            QueueClient.DefaultRequestOptions.ServerTimeout = TimeSpan.FromHours(24);
            QueueClient.DefaultRequestOptions.RetryPolicy = new ExponentialRetry(TimeSpan.FromSeconds(5), 3);
        }

        /// <summary>
        /// Creates the queue with the given queuename if it does not exists
        /// </summary>
        /// <param name="queueName"></param>
        /// <returns></returns>
        public async Task<bool> CreateQueueAsync(string queueName)
        {
            // Create the queue if it doesn't exist.
            var queue = QueueClient.GetQueueReference(queueName);
            return await queue.CreateIfNotExistsAsync();
        }

        /// <summary>
        /// Deletes the given queue if exists
        /// </summary>
        /// <returns></returns>
        public async Task DeleteQueueAsync(string queueName)
        {
            // Delete the queue 
            var queue = QueueClient.GetQueueReference(queueName);
            await queue.DeleteIfExistsAsync();
        }

        public async Task AddQueueAsync(string queueName, string message)
        {
            // Add the queue 
            var queue = QueueClient.GetQueueReference(queueName);
            var cloudQueueMessage = new CloudQueueMessage(message);
            await queue.AddMessageAsync(cloudQueueMessage);
        }
    }
}
