﻿using System;
using System.Globalization;
using System.Linq;

namespace CE.AO.Utilities
{
    public static class TimezoneHelper
    {
        /// <summary>
        /// Converts given date time from source time zone to destination timezone
        /// </summary>
        public static DateTime ConvertToTimeZone(DateTime timestamp, TimeZoneInfo sourceTimeZoneInfo, TimeZoneInfo destinationTimeZoneInfo)
        {
            return TimeZoneInfo.ConvertTime(timestamp, sourceTimeZoneInfo, destinationTimeZoneInfo);
        }

        /// <summary>
        /// In case where two UTC time represents same local time then this method specifies whether given UTC time is second hour representing same local time.        
        /// <para>
        /// e.g. Timezone EST: During Fall Back period we move our clocks from 2 am back to 1 am, but UTC does not fall back. So there are two hours in UTC but in EST we have the same hour.
        /// UTC : November 1, 2015 5 am = EST : November 1, 2015 1 am        
        /// UTC : November 1, 2015 6 am = EST : November 1, 2015 1 am
        /// </para>
        /// </summary>
        public static bool IsSecondHour(TimeZoneInfo localzone, DateTime localtime, DateTime utcTime)
        {
            if (localzone.IsAmbiguousTime(localtime))
            {
                return utcTime.Add(localzone.GetUtcOffset(localtime)) == localtime;
            }
            return false;
        }
    }
}