﻿using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Blob;

namespace CE.AO.Utilities
{

    /// <summary>
    /// Helper class for performaning typical Blob storage administrative functions.
    /// </summary>
    public static class BlobStorageManager
    {
        public static CloudAppendBlob CreateAppendBlob(string blobConnectionString, string containerName, string fileName)
        {
            CloudBlobContainer blobContainer = GetBlobContainer(blobConnectionString, containerName);

            var blob = blobContainer.GetAppendBlobReference($"{fileName}");

            if (!blob.Exists())
            {
                blob.CreateOrReplace();
                blob.Properties.ContentType = "application/octet-stream";
                blob.SetProperties();
            }

            return blob;
        }

        public static void DeleteBlob(string blobConnectionString, string containerName, string fileName)
        {
            CloudBlobContainer blobContainer = GetBlobContainer(blobConnectionString, containerName);

            CloudBlob blob = blobContainer.GetBlobReference(fileName);
            blob.DeleteIfExists();
        }

        private static CloudBlobContainer GetBlobContainer(string blobConnectionString, string containerName)
        {
            var storageAccount = CloudStorageAccount.Parse(blobConnectionString);

            var blobClient = storageAccount.CreateCloudBlobClient();

            var blobContainer = blobClient.GetContainerReference(containerName);
            blobContainer.CreateIfNotExists();

            return blobContainer;
        }

        public static bool BlobExists(string blobConnectionString, string containerName, string fileName)
        {
            CloudBlobContainer blobContainer = GetBlobContainer(blobConnectionString, containerName);

            CloudBlob blob = blobContainer.GetBlobReference(fileName);

            return blob.Exists();
        }
    }
}
