﻿using CE.AO.Logging;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.IO;
using System.Runtime.Remoting.Messaging;
using CE.AO.Models;

namespace CE.AO.DataImport.BatchProcessWorker.Tests
{
    /// <summary>
    /// Test Case Class for BatchProcess Worker Role(Function.cs)
    /// </summary>
    [TestClass]
    public class BatchProcessWorkerFunctionTest
    {
        /// <summary>
        /// Test Case for Initialization Test
        /// </summary>
        [TestInitialize]
        public void Init()
        {
            AutoMapperConfig.AutoMapperMappings();
        }

        /// <summary>
        /// Test Case for Process Batch for AMi 60 min interval file
        /// </summary>
        [TestMethod]
        public void ProcessBatch_ForAmi60Min()
        {
            try
            {
                var log = TextWriter.Null;
                Functions.LogModel = new LogModel
                {
                    DisableLog = false,
                    Module = Utilities.Enums.Module.FileProcessing,
                    ProcessingType = Utilities.Enums.ProcessingType.File
                };

                Functions.ProcessBatch("AMIEnd_EST_AMI_87_60min-Does not Handles DST1_Test.csv:client87:87", log);
            }
            catch
            {
                Assert.AreEqual(true, true);
            }
        }

        /// <summary>
        /// Test Case for Process Batch for wrong container blob name
        /// </summary>
        [TestMethod]
        public void ProcessBatch_WrongContainerBlobName()
        {
            try
            {
                var log = TextWriter.Null;
                Functions.LogModel = new LogModel
                {
                    DisableLog = false,
                    Module = Utilities.Enums.Module.FileProcessing,
                    ProcessingType = Utilities.Enums.ProcessingType.File
                };

                Functions.ProcessBatch("AMIEnd_EST_AMI_87_60min-Does not Handles DST1_Test.csv:client87", log);
            }
            catch
            {
                Assert.AreEqual(true, true);
            }
        }

        /// <summary>
        /// Test Case for Process Batch for AMi daily interval file
        /// </summary>
        [TestMethod]
        public void ProcessBatch_ForAmiDaily()
        {
            try
            {
                var log = TextWriter.Null;
                Functions.LogModel = new LogModel
                {
                    DisableLog = false,
                    Module = Utilities.Enums.Module.FileProcessing,
                    ProcessingType = Utilities.Enums.ProcessingType.File
                };
                Functions.ProcessBatch("EST_AMI_87_1daily_13June-Test.csv:client87:87", log);
            }
            catch
            {
                Assert.AreEqual(true, true);
            }
        }

        /// <summary>
        /// Test Case for Process Batch AMi 15 min interval file
        /// </summary>
        [TestMethod]
        public void ProcessBatch_ForAmi15Min()
        {
            try
            {
                var log = TextWriter.Null;
                Functions.LogModel = new LogModel
                {
                    DisableLog = false,
                    Module = Utilities.Enums.Module.FileProcessing,
                    ProcessingType = Utilities.Enums.ProcessingType.File
                };
                Functions.ProcessBatch("EST_AMI_87_15min_13June-Test.csv:client87:87", log);
            }
            catch
            {
                Assert.AreEqual(true, true);
            }
        }

        /// <summary>
        /// Test Case for Process Batch Ami 30 mIn Interval File
        /// </summary>
        [TestMethod]
        public void ProcessBatch_ForAmi30Min()
        {
            try
            {
                var log = TextWriter.Null;
                Functions.LogModel = new LogModel
                {
                    DisableLog = false,
                    Module = Utilities.Enums.Module.FileProcessing,
                    ProcessingType = Utilities.Enums.ProcessingType.File
                };
                Functions.ProcessBatch("EST Timezone AMI_87_30min_Valid Data-Test.csv:client87:87", log);
            }
            catch
            {
                Assert.AreEqual(true, true);
            }
        }

        /// <summary>
        /// Test Case for Process Batch
        /// </summary>
        [TestMethod]
        public void ProcessBatch_ForAccountUpdates()
        {
            try
            {
                var log = TextWriter.Null;
                Functions.LogModel = new LogModel
                {
                    DisableLog = false,
                    Module = Utilities.Enums.Module.FileProcessing,
                    ProcessingType = Utilities.Enums.ProcessingType.File
                };
                Functions.ProcessBatch("AccountUpdates_87_UATDay4BCycle007_UnitTest.csv:client87:87", log);
            }
            catch
            {
                Assert.AreEqual(true, true);
            }
        }

        /// <summary>
        /// Test Case for Process Batch Billing File
        /// </summary>
        [TestMethod]
        public void ProcessBatch_ForBilling()
        {
            try
            {
                var log = TextWriter.Null;
                Functions.LogModel = new LogModel
                {
                    DisableLog = false,
                    Module = Utilities.Enums.Module.FileProcessing,
                    ProcessingType = Utilities.Enums.ProcessingType.File
                };
                Functions.ProcessBatch("Bill_87_UATDay41_Test.csv:client87:87", log);
            }
            catch
            {
                Assert.AreEqual(true, true);
            }
        }

        /// <summary>
        /// Test Case for Process Batch Subscription File
        /// </summary>
        [TestMethod]
        public void ProcessBatch_ForSubscription()
        {
            try
            {
                var log = TextWriter.Null;
                Functions.LogModel = new LogModel
                {
                    DisableLog = false,
                    Module = Utilities.Enums.Module.FileProcessing,
                    ProcessingType = Utilities.Enums.ProcessingType.File
                };
                Functions.ProcessBatch("Subscription_87_10Mar_Test.csv:client87:87", log);
            }
            catch
            {
                Assert.AreEqual(true, true);
            }
        }

        /// <summary>
        /// Test Case for Process Batch for NCC AMi reading File
        /// </summary>
        [TestMethod]
        public void ProcessBatch_ForNCC()
        {
            try
            {
                var log = TextWriter.Null;
                Functions.LogModel = new LogModel
                {
                    DisableLog = false,
                    Module = Utilities.Enums.Module.FileProcessing,
                    ProcessingType = Utilities.Enums.ProcessingType.File
                };
                Functions.ProcessBatch("ncc__2016-07-28_101914__2de7bbe0-edf8-48cf-9854-50e6b4c2a80f_Test.csv:client87:87", log);
            }
            catch
            {
                Assert.AreEqual(true, true);
            }
        }

        /// <summary>
        /// Test Case for Process Batch for NCC AMi Consumption File
        /// </summary>
        [TestMethod]
        public void ProcessBatch_ForNCCAmiConsumption()
        {
            try
            {
                var log = TextWriter.Null;
                Functions.LogModel = new LogModel
                {
                    DisableLog = false,
                    Module = Utilities.Enums.Module.FileProcessing,
                    ProcessingType = Utilities.Enums.ProcessingType.File
                };
                Functions.ProcessBatch("NccCalcConsumptionncc_UnitTest.csv:client87:87", log);
            }
            catch
            {
                Assert.AreEqual(true, true);
            }
        }
        
        /// <summary>
        /// Cleanup Method To Release Log4Net Data Resources hold by Any Test Method
        /// </summary>
        [TestCleanup]
        public void CleanUpMethod()
        {
            CallContext.FreeNamedDataSlot("log4net.Util.LogicalThreadContextProperties");
        }
    }
}