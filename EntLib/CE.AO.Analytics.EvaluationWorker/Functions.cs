﻿using System;
using System.IO;
using CE.AO.Logging;
using System.Linq;
using Microsoft.Azure.WebJobs;
using Microsoft.Practices.Unity;
using AO.BusinessContracts;
using AO.Registrar;
using CE.AO.Models;
using Enums = CE.AO.Utilities.Enums;

namespace CE.AO.Analytics.EvaluationWorker
{
    /// <summary>
    /// Functions class is used to process the queue message in the Evaluation queue
    /// </summary>
    public static class Functions
    {
        public static LogModel LogModel
        {
            get; set;
        }
        public static IUnityContainer UnityContainer { get; set; }

        /// <summary>
        /// To process the queue message in the AOEvaluationQueue queue
        /// </summary>
        /// <param name="data">Queue message</param>
        /// <param name="log">TextWriter object</param>
        public static void ProcessEvaluation([QueueTrigger("%AOEvaluationQueue%")] string data, TextWriter log)
        {
            IUnityContainer unityContainer = new UnityContainer();
            if (UnityContainer != null)
                unityContainer = UnityContainer;
            else
                (new DataStorageRegistrar()).Initialize<TransientLifetimeManager>(unityContainer);

            LogModel logModel = new LogModel();
            if (LogModel == null)
            {
                logModel.Module = Enums.Module.InsightEvaluation;
            }
            else
            {
                logModel = LogModel;
            }

            try
            {
                string[] parameter = data.Split(new[] { "^^" }, StringSplitOptions.None).ToArray();
                if (parameter.Length == 9)
                {
                    var clientId = Convert.ToInt32(parameter[0]);
                    var customerId = parameter[1];
                    var accountId = parameter[2];
                    var serviceContractId = parameter[3];
                    var date = parameter[4];
                    var source = parameter[5];
                    var rowIndex = parameter[6];
                    var metadata = parameter[7];
                    var processingType = parameter[8];

                    logModel.ClientId = clientId.ToString();
                    logModel.CustomerId = customerId;
                    logModel.AccountId = accountId;
                    logModel.ServiceContractId = serviceContractId;
                    logModel.Source = source;
                    logModel.RowIndex = rowIndex;
                    logModel.Metadata = metadata;
                    logModel.ProcessingType = (Enums.ProcessingType)Convert.ToInt32(processingType);

                    var evaluationFacade = unityContainer.Resolve<IEvaluationFacade>(
                        new ParameterOverride("logModel", logModel));
                    var asOfDate = DateTime.Now;
                    if (!string.IsNullOrEmpty(date))
                    {
                        asOfDate = Convert.ToDateTime(date);
                        ////Logger.Info($"Evaluation started for client {clientId} customer {customerId} account {accountId} serviceContract {serviceContractId} asOfDate {asOfDate}",
                        ////    logModel);
                    }
                    evaluationFacade.ProcessEvaluation(clientId, customerId, accountId, serviceContractId, asOfDate);
                }
                else
                {
                    Logger.Fatal(
                        $@"Queue message ""{data}"" received for evaluation is not in proper format. Format should be ""ClientId:CusotmerId:AccountId:ServiceContractId:Date:FileName:RowIndex:Metadata:ProcessingType"".",
                        LogModel);
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, logModel);
                throw;
            }
        }

        /// <summary>
        /// To process the queue message in the AOInsightReportQueue queue
        /// </summary>
        /// <param name="data">Queue Message</param>
        /// <param name="log">TextWriter object</param>
        public static void ProcessInsightReport([QueueTrigger("%AOInsightReportQueue%")] string data, TextWriter log)
        {
            IUnityContainer unityContainer = new UnityContainer();
            if (UnityContainer != null)
                unityContainer = UnityContainer;
            else
                (new DataStorageRegistrar()).Initialize<TransientLifetimeManager>(unityContainer);

            LogModel logModel = new LogModel();
            if (LogModel == null)
            {
                logModel.Module = Enums.Module.InsightEvaluation;
            }
            else
            {
                logModel = LogModel;
            }

            try
            {
                string[] parameter = data.Split(new[] { "^^" }, StringSplitOptions.None).ToArray();
                if (parameter.Length == 3)
                {
                    var clientId = Convert.ToInt32(parameter[0]);
                    var date = parameter[1];
                    var processingType = parameter[2];

                    logModel.ClientId = clientId.ToString();
                    logModel.ProcessingType = (Enums.ProcessingType)Convert.ToInt32(processingType);

                    var asOfDate = DateTime.UtcNow.Date;
                    if (!string.IsNullOrEmpty(date))
                        asOfDate = (Convert.ToDateTime(date)).Date;
                    var insightReport = unityContainer.Resolve<IReport>(new ParameterOverride("logModel", logModel));

                    Logger.Info($"Insight report started for client {clientId}", logModel);

                    insightReport.ExportInsights(clientId, asOfDate);
                }
                else
                {
                    Logger.Fatal(
                        $@"Queue message ""{data
                            }"" received for insight report is not in proper format. Format should be ""ClientId:Date:ProcessingType"".",
                        logModel);
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, logModel);
                throw;
            }
        }
    }
}
