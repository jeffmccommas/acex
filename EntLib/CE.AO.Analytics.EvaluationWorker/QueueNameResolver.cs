﻿using Microsoft.Azure;
using Microsoft.Azure.WebJobs;

namespace CE.AO.Analytics.EvaluationWorker
{
    class QueueNameResolver : INameResolver
    {
        public string Resolve(string name)
        {
            return CloudConfigurationManager.GetSetting(name);
        }
    }
}
