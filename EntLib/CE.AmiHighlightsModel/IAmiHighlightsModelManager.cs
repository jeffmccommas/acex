using System.Collections.Generic;
using CE.Models;
using CE.Models.Insights;

namespace CE.AmiHighlightsModel
{
    public interface IAmiHighlightsModelManager
    {
        /// <summary>
        /// Execute bill highlight evaluations
        /// </summary>
        /// <param name="customerId">A Customer Id</param>
        /// <param name="accountId">An Account Id</param>
        /// <param name="premiseId">A Premise Id</param>
        /// <param name="requestCommodityKeys">The commodity keys passed in via the Action request</param>
        List<AmiHighlightModel> ExecuteAmiHighlightModel(string customerId,
            string accountId, string premiseId,
            string requestCommodityKeys);

        /// <summary>
        /// Calculate AMI Seven Day Average Highlights
        /// </summary>
        /// <param name="billAccount">A CustomerInfoAccount object</param>
        /// <param name="accountBills">An BillAccount object</param>
        /// <param name="premiseId">A Premise Id</param>
        /// <param name="customerId">A Customer Id</param>
        /// <param name="accountId">An Account Id</param>
        /// <param name="requestCommodityKeys">The commodity keys passed in via the Action request</param>
        /// <param name="highlights">A refrenence to a list of AmiHighlight objects.</param>
        void CalcHighlights(CustomerInfoAccount billAccount, BillAccount accountBills, string premiseId, string customerId,
            string accountId, string requestCommodityKeys, ref List<AmiHighlightModel> highlights);

        /// <summary>
        /// In the formulas below, 
        /// �	Percent impact = (usage for recent day � average usage for prev week)/average usage for prev week
        /// �	Cost Impact = (usage for recent day � average usage for prev week) * Cost of Usage.
        /// </summary>
        /// <param name="consumptionData">The AMI consumption data associated with the most recent bill.</param>
        /// <param name="recentBillService">The service associated with the most recent bill.</param>
        /// <param name="accountId">An Account Id</param>
        /// <param name="premiseId">A Premise Id</param>
        /// <param name="customerId">A Customer Id</param>
        /// <param name="serviceId">A Service Id</param>
        /// <param name="servicePointId">A Service Id</param>
        /// <param name="meterId">A Meter Id</param>
        List<AmiHighlightModel> CalcAmiSevenDayAverageHighlight(List<RateModel.Reading> consumptionData, BillService recentBillService,
            string accountId, string premiseId,
            string customerId, string serviceId, string servicePointId, string meterId);

        /// <summary>
        /// Get the CustomerInfo Account
        /// </summary>
        /// <param name="customerId">A Customer Id</param>
        /// <param name="accountId">An Account Id</param>
        CustomerInfoAccount GetCustomerInfoAccountWithMeters(string customerId, string accountId);

        /// <summary>
        /// Get the consumption data for the most recent bill using Version2 Consumption API
        /// </summary>
        /// <param name="clientUser">A Client User</param>
        /// <param name="recentBillService">The service associated with the most recent bill.</param>
        /// <param name="accountId">An Account Id</param>
        /// <param name="premiseId">A Premise Id</param>
        /// <param name="customerId">A Customer Id</param>
        /// <param name="serviceId">A Service Id</param>
        /// <param name="servicePointId">A Service Id</param>
        /// <param name="meterId">A Meter Id</param>
        /// <param name="dailyReadings"></param>
        /// <param name="intervalReadings"></param>
        /// <param name="interval"></param>
        void GetAmiReadings(ClientUser clientUser, BillService recentBillService,
            string accountId, string premiseId, string customerId, string serviceId, string servicePointId,
            string meterId, out List<RateModel.Reading> dailyReadings, out List<RateModel.Reading> intervalReadings, out int interval);
    }
}