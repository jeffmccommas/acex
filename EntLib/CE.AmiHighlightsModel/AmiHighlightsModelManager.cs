﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AO.BusinessContracts;
using CE.AO.Models;
using CE.ContentModel;
using CE.Models;
using CE.Models.Insights;
using CE.RateModel;
using Microsoft.Practices.Unity;
using ResolutionTypes = CE.Models.Insights.Types.ResolutionType;

namespace CE.AmiHighlightsModel
{
    public class AmiHighlightsModelManager : IAmiHighlightsModelManager
    {
        #region Private Constants

        private const int NumOfBills = 24; // default to 24 to be consistant with bill history
        private const int IntervalDays = 7;

        #endregion

        #region Member Variables

        public static IInsightsEFRepository InsightsEfRepository = new InsightsEfRepository();
        private readonly ClientUser _clientUser;
        private readonly int _clientId;
        private readonly IUnityContainer _container;

        #endregion

        #region Constructors

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="clientUser">A Client User</param>
        private AmiHighlightsModelManager(ClientUser clientUser)
        {
            _clientUser = clientUser;
            _clientId = clientUser.ClientID;
            InsightsEfRepository = new InsightsEfRepository();
        }

        public AmiHighlightsModelManager(ClientUser clientUser, IUnityContainer unityContainer)
            : this(clientUser) {
            _container = unityContainer;

        }

        /// <inheritdoc />
        /// <summary>
        /// Construtor to initialize AMI Highlight Model Manager with Unitycontainer
        /// 
        /// </summary>
        /// <param name="clientUser">A Client User</param>
        /// <param name="unityContainer"></param>
        /// <param name="insightsEfRepository"></param>
        public AmiHighlightsModelManager(ClientUser clientUser, IUnityContainer unityContainer, IInsightsEFRepository insightsEfRepository)
            : this(clientUser)
        {
            _container = unityContainer;
            InsightsEfRepository = insightsEfRepository;
        }
        #endregion Constructors

        #region ExecuteAmiHighlightModel

        /// <inheritdoc />
        /// <summary>
        /// Execute bill highlight evaluations
        /// </summary>
        /// <param name="customerId">A Customer Id</param>
        /// <param name="accountId">An Account Id</param>
        /// <param name="premiseId">A Premise Id</param>
        /// <param name="requestCommodityKeys">The commodity keys passed in via the Action request</param>
        public List<AmiHighlightModel> ExecuteAmiHighlightModel(string customerId,
            string accountId, string premiseId,
            string requestCommodityKeys)
        {
            var highlights = new List<AmiHighlightModel>();

            // Get the Account's Bills 
            var accountBills = GetAccountBills(customerId, accountId);

            if (accountBills?.Bills == null || !accountBills.Bills.Any())
            {
                return highlights;
            }

            // get the customer info account for retrieving meter ids
            var billAccount = GetCustomerInfoAccountWithMeters(customerId, accountId);

            if (billAccount?.Premises == null || !billAccount.Premises.Any())
            {
                return highlights;
            }

            // Calc the Highlights
            CalcHighlights(billAccount, accountBills, premiseId, customerId, accountId,
                requestCommodityKeys, ref highlights);

            // Return the highlights
            return highlights.Distinct().Where(x => x.CostImpact != 0 || x.PercentageImpact != 0).ToList();
        }

        #endregion ExecuteAmiHighlightModel

        #region CalcHighlights

        /// <inheritdoc />
        /// <summary>
        /// Calculate AMI Seven Day Average Highlights
        /// </summary>
        /// <param name="billAccount">A CustomerInfoAccount object</param>
        /// <param name="accountBills">An BillAccount object</param>
        /// <param name="premiseId">A Premise Id</param>
        /// <param name="customerId">A Customer Id</param>
        /// <param name="accountId">An Account Id</param>
        /// <param name="requestCommodityKeys">The commodity keys passed in via the Action request</param>
        /// <param name="highlights">A refrenence to a list of AmiHighlight objects.</param>
        public void CalcHighlights(CustomerInfoAccount billAccount, BillAccount accountBills, string premiseId, string customerId,
            string accountId, string requestCommodityKeys, ref List<AmiHighlightModel> highlights)
        {
            if (billAccount?.Premises == null || !billAccount.Premises.Any())
            {
                return;
            }

            if (accountBills?.Bills == null || !accountBills.Bills.Any())
            {
                return;
            }

            // get premise bills
            var premiseBills = accountBills.Bills.Where(b => b.Premises.All(p => p.Id == premiseId)).ToList();

            if (!premiseBills.Any())
            {
                return;
            }

            // get latest bill at premise level
            var recentBill = premiseBills[0];

            // get service level highlight for each service under the premise
            foreach (var recentBillService in recentBill.Premises[0].Service)
            {
                if (string.IsNullOrEmpty(requestCommodityKeys))
                {
                    continue;
                }

                if (!requestCommodityKeys.ToLower().Contains(recentBillService.CommodityKey.ToLower()))
                {
                    continue;
                }

                var billAccountPremises = billAccount.Premises.Find(p => p.Id == recentBill.Premises[0].Id);

                if (billAccountPremises == null)
                {
                    continue;
                }

                var billAccountServices = billAccountPremises.Service.Find(p => p.Id == recentBillService.Id);

                foreach (var servicePoint in billAccountServices.ServicePoints)
                {
                    foreach (var meter in servicePoint.Meters)
                    {
                        // get the consumption data

                        GetAmiReadings(_clientUser, recentBillService, accountId, premiseId,
                           customerId, recentBillService.Id, servicePoint.Id, meter.Id, out var dailyReadings, out var intervalReadings, out var interval);

                        // calc seven day highlight
                        var amiSevenDayAverageHighlightTask = GetAmiSevenDayAverageHighlightAsync(dailyReadings, recentBillService,
                            accountId, premiseId, customerId, servicePoint.Id, meter.Id);
                        var amiSevenDayAverageHighlights = amiSevenDayAverageHighlightTask.Result;
                        highlights.AddRange(amiSevenDayAverageHighlights);

                        // max consumption highlight
                        var maxConsumptionHighlightTask = GetMaxConsumptionHighlightAsync(intervalReadings,
                            recentBillService, accountId, premiseId, meter.Id, interval);
                        var maxConsumptionHighlights = maxConsumptionHighlightTask.Result;
                        highlights.AddRange(maxConsumptionHighlights);
                    }
                }
            }
        }

        #endregion CalcHighlights

        private async Task<List<AmiHighlightModel>> GetMaxConsumptionHighlightAsync(List<Reading> consumptionData,
            BillService recentBillService, string accountId, string premiseId, string meterId, int interval)
        {
            var task = await Task.Factory.StartNew(() => CalcMaxConsumptionHighlight(consumptionData, recentBillService, accountId,
                            premiseId, recentBillService.Id, meterId, interval)).ConfigureAwait(false);
            return task;
        }

        private async Task<List<AmiHighlightModel>> GetAmiSevenDayAverageHighlightAsync(List<Reading> consumptionData, BillService recentBillService,
                            string accountId, string premiseId, string customerId, string servicePointId, string meterId)
        {
            var task = await Task.Factory.StartNew(() => CalcAmiSevenDayAverageHighlight(consumptionData, recentBillService, accountId, premiseId,
                                 customerId, recentBillService.Id, servicePointId, meterId)).ConfigureAwait(false);
            return task;
        }

        # region CalcAmiSevenDayAverageHighlight

        /// <inheritdoc />
        /// <summary>
        /// In the formulas below, 
        /// •	Percent impact = (usage for recent day – average usage for prev week)/average usage for prev week
        /// •	Cost Impact = (usage for recent day – average usage for prev week) * Cost of Usage.
        /// </summary>
        /// <param name="consumptionData">The AMI consumption data associated with the most recent bill.</param>
        /// <param name="recentBillService">The service associated with the most recent bill.</param>
        /// <param name="accountId">An Account Id</param>
        /// <param name="premiseId">A Premise Id</param>
        /// <param name="customerId">A Customer Id</param>
        /// <param name="serviceId">A Service Id</param>
        /// <param name="servicePointId">A Service Id</param>
        /// <param name="meterId">A Meter Id</param>
        public List<AmiHighlightModel> CalcAmiSevenDayAverageHighlight(List<Reading> consumptionData, BillService recentBillService,
            string accountId, string premiseId,
            string customerId, string serviceId, string servicePointId, string meterId)
        {
            var highlights = new List<AmiHighlightModel>();
            if (consumptionData == null || !consumptionData.Any())
            {
                return new List<AmiHighlightModel>();
            }

            // Retrieve the most recent, plus the previous seven days
            var weeksData = consumptionData.Take(8).ToList();
            var previousDays = weeksData.Count() - 1;
            var recentDay = weeksData.Take(1).SingleOrDefault();

            // Retrieve the usage for the previous seven days (not including the most recent day)
            var priorSevenDays = weeksData.Take(previousDays).ToList();

            // make sure we have recent day data and full seven days for previous week
            if (recentDay == null || previousDays < 7)
            {
                return new List<AmiHighlightModel>();
            }

            var recentUse = Convert.ToDecimal(recentDay.Quantity);
            var totalUse = Convert.ToDecimal(priorSevenDays.Sum(d => d.Quantity));
            var averageUse = totalUse / previousDays;

            if (averageUse == 0)
            {
                return new List<AmiHighlightModel>();
            }

            var percentImpact = (recentUse - averageUse) / averageUse;
            var costImpact = percentImpact * recentBillService.CostofUsage;

            // Check if "Impact" is positive or negative
            if (percentImpact > 0)
            {
                highlights.Add(new AmiHighlightModel
                {
                    AccountId = accountId,
                    ActionKey = "insight.sevendayaverageincrease",
                    Commoditykey = recentBillService.CommodityKey,
                    MeterId = meterId,
                    CostImpact = costImpact,
                    PercentageImpact = percentImpact,
                    PremiseId = premiseId,
                    ServiceId = serviceId
                });

                //return zero impact for sevendayaveragedecrease
                highlights.Add(new AmiHighlightModel
                {
                    AccountId = accountId,
                    ActionKey = "insight.sevendayaveragedecrease",
                    Commoditykey = recentBillService.CommodityKey,
                    MeterId = meterId,
                    CostImpact = 0M,
                    PercentageImpact = 0M,
                    PremiseId = premiseId,
                    ServiceId = serviceId
                });
            }
            else if (percentImpact < 0)
            {
                highlights.Add(new AmiHighlightModel
                {
                    AccountId = accountId,
                    ActionKey = "insight.sevendayaveragedecrease",
                    Commoditykey = recentBillService.CommodityKey,
                    MeterId = meterId,
                    CostImpact = Math.Abs(costImpact),
                    PercentageImpact = Math.Abs(percentImpact),
                    PremiseId = premiseId,
                    ServiceId = serviceId
                });

                //return zero impact for sevendayaverageincrease
                highlights.Add(new AmiHighlightModel
                {
                    AccountId = accountId,
                    ActionKey = "insight.sevendayaverageincrease",
                    Commoditykey = recentBillService.CommodityKey,
                    MeterId = meterId,
                    CostImpact = 0M,
                    PercentageImpact = 0M,
                    PremiseId = premiseId,
                    ServiceId = serviceId
                });
            }

            return highlights;
        }

        #endregion CalcAmiSevenDayAverageHighlight

        private int GetAmiIntervalDays()
        {
            var intervaldays = IntervalDays;

            var cf = new ContentModelFactoryContentful();
            var cp = cf.CreateContentProvider(_clientId);

            var clientconfigs = cp.GetContentConfiguration("action", "insight.highestinterval.days");

            if (clientconfigs == null || clientconfigs.Count <= 0) return intervaldays;
            var config = clientconfigs[0];
            intervaldays = Convert.ToInt32(config.Value);

            return intervaldays;
        }

        /// <summary>
        /// In the formulas below, 
        /// •	Percent impact = (usage for recent day – average usage for prev week)/average usage for prev week
        /// •	Cost Impact = (usage for recent day – average usage for prev week) * Cost of Usage.
        /// </summary>
        /// <param name="consumptionData">The AMI consumption data associated with the most recent bill.</param>
        /// <param name="recentBillService">The service associated with the most recent bill.</param>
        /// <param name="accountId">An Account Id</param>
        /// <param name="premiseId">A Premise Id</param>
        /// <param name="serviceId">A Service Id</param>
        /// <param name="meterId">A Meter Id</param>
        /// <param name="interval"></param>
        private List<AmiHighlightModel> CalcMaxConsumptionHighlight(List<Reading> consumptionData,
            BillService recentBillService,
            string accountId, string premiseId, string serviceId, string meterId, int interval)
        {
            if (consumptionData == null || !consumptionData.Any())
            {
                return new List<AmiHighlightModel>();
            }
            var highlights = new List<AmiHighlightModel>();
            const decimal costImpact = 0;
            int factor;

            if (interval == 24)
            {
                factor = 1;
            }
            else
            {
                factor = 24 * (60 / interval);
            }

            var intervaldays = GetAmiIntervalDays();

            // Retrieve the interval data
            var intervaldata = consumptionData.Take(intervaldays * factor).ToList();
            var maxConsumptionDay = intervaldata.FindMaxBy(r => r.Quantity);
            var totalUse = Convert.ToDecimal(intervaldata.Sum(d => d.Quantity));
            var maxUsage = Convert.ToDecimal(maxConsumptionDay.Quantity);

            var averageUse = totalUse / intervaldata.Count;

            if (averageUse == 0)
            {
                return new List<AmiHighlightModel>();
            }

            var percentImpact = (maxUsage - averageUse) / averageUse;

            // Check if "Impact" is positive or negative
            if (percentImpact > 0)
            {
                highlights.Add(new AmiHighlightModel
                {
                    AccountId = accountId,
                    ActionKey = "insight.highestinterval",
                    Commoditykey = recentBillService.CommodityKey,
                    MeterId = meterId,
                    CostImpact = costImpact,
                    PercentageImpact = percentImpact,
                    PremiseId = premiseId,
                    ServiceId = serviceId,
                    HighlightTimeStamp = maxConsumptionDay.Timestamp
                });
            }
            return highlights;
        }

        #region GetAccountBills

        /// <summary>
        /// Get last 24 bills for the selected account
        /// </summary>
        /// <param name="customerId">A Customer Id</param>
        /// <param name="accountId">An Account Id</param>
        /// <returns>An AccountBill object that contains the Bills</returns>
        private BillAccount GetAccountBills(string customerId, string accountId)
        {
            
            BillAccount accountBills = null;
            var today = DateTime.Today.Date;
            var startDate = today.AddMonths(NumOfBills * -1);

            var billRequest = new BillRequest
            {
                CustomerId = customerId,
                AccountId = accountId,
                StartDate = startDate,
                EndDate = today,
                Count = NumOfBills
            };

            var bills = InsightsEfRepository.GetBills(_clientId, billRequest);

            if (bills.Customer?.Accounts != null && bills.Customer.Accounts.Any())
            {
                accountBills = bills.Customer.Accounts.Find(a => a.Id == accountId);
            }

            return accountBills;
        }

        #endregion GetAccountBills

        #region GetCustomerInfoAccountWithMeters

        /// <inheritdoc />
        /// <summary>
        /// Get the CustomerInfo Account
        /// </summary>
        /// <param name="customerId">A Customer Id</param>
        /// <param name="accountId">An Account Id</param>
        public CustomerInfoAccount GetCustomerInfoAccountWithMeters(string customerId, string accountId)
        {
            var customerInfoAccounts = new CustomerInfoAccount();

            // get the customer info for retrieving meter ids
            var customerInfoRequest = new CustomerInfoRequest
            {
                CustomerId = customerId
            };

            var customerInfo = InsightsEfRepository.GetCustomerInfo(_clientId, customerInfoRequest);

            if (customerInfo?.Customer?.Accounts != null)
            {
                customerInfoAccounts = customerInfo.Customer.Accounts.Find(a => a.Id == accountId);
            }

            return customerInfoAccounts;
        }

        #endregion GetCustomerInfoAccountWithMeters

        #region GetAmiReadings

        /// <inheritdoc />
        /// <summary>
        /// Get the consumption data for the most recent bill using Version2 Consumption API
        /// </summary>
        /// <param name="clientUser">A Client User</param>
        /// <param name="recentBillService">The service associated with the most recent bill.</param>
        /// <param name="accountId">An Account Id</param>
        /// <param name="premiseId">A Premise Id</param>
        /// <param name="customerId">A Customer Id</param>
        /// <param name="serviceId">A Service Id</param>
        /// <param name="servicePointId">A Service Id</param>
        /// <param name="meterId">A Meter Id</param>
        /// <param name="dailyReadings"></param>
        /// <param name="intervalReadings"></param>
        /// <param name="interval"></param>
        public virtual void GetAmiReadings(ClientUser clientUser, BillService recentBillService,
            string accountId, string premiseId, string customerId, string serviceId, string servicePointId,
            string meterId, out List<Reading> dailyReadings, out List<Reading> intervalReadings, out int interval)
        {
            var today = DateTime.Today.Date;
            dailyReadings = null;
            intervalReadings = null;
            interval = 24;
            // look back 365 days
            var startDate = DateTime.Parse(recentBillService.BillStartDate).AddYears(-1);

            var amiManager = _container.Resolve<ITallAMI>();
            var tallAmiModels = amiManager.GetAmiList(startDate, today, meterId, accountId, _clientId)?.ToList();

            if (tallAmiModels == null || !tallAmiModels.Any()) return;
            var tallAmiModel = tallAmiModels.First();
            interval = tallAmiModel.IntervalType;

            intervalReadings = amiManager.GetReadings(tallAmiModels, out var _);
            dailyReadings = interval == (int)ResolutionTypes.day ? intervalReadings : AggregateDailyReadings(tallAmiModels, interval);
        }

        public List<Reading> AggregateDailyReadings(List<TallAmiModel> amiList, int intervalType)
        {
            var readings = new List<Reading>();

            if (!amiList.Any()) return readings;

            // NOTE:  This behavior needs to ultimately use the pre-aggregated consumption values that MDM is introducing.  Since that is not ready
            // yet for this migration, we will need to manually loop through interval readings and aggregate all the values for each day.  

            var aggregatedDateTime = amiList[0].AmiTimeStamp; // Initialize with first ami reading in list.
            double aggregatedConsumption = 0;

            foreach (var amiRow in amiList.OrderByDescending(a => a.AmiTimeStamp))
            {
                var currentRowDateTime = amiRow.AmiTimeStamp;
                var currentRowConsumption = amiRow.Consumption;

                if (currentRowDateTime.Date == aggregatedDateTime.Date) // We're in the same day, so we need to aggregate.
                {
                    aggregatedConsumption += currentRowConsumption ?? 0;
                }
                else // We are in a new day.
                {
                    //We must have hit a new day.  Add the last day to list.
                    readings.Add(new Reading(aggregatedDateTime.Date, aggregatedConsumption));

                    // Start the new days aggregation.
                    aggregatedConsumption = currentRowConsumption ?? 0;
                    aggregatedDateTime = currentRowDateTime;
                }
            }

            // We'll always have one aggregated value left to add when exiting loop.
            readings.Add(new Reading(aggregatedDateTime.Date, aggregatedConsumption));

            return readings;
        }

        #endregion GetAmiReadings
    }
}

