﻿using CE.Models;
using Microsoft.Practices.Unity;

namespace CE.AmiHighlightsModel
{
    public class AmiHighlightsModelFactory
    {
        ///// <summary>
        ///// Create AmiHighlightModelManager that implements IAmiHighlightsModel.
        ///// Set InsightRepository for unit test
        ///// </summary>
        ///// <param name="clientUser"></param>
        ///// <returns></returns>
        //public IAmiHighlightsModelManager CreateAmiHighlightsModelManager(ClientUser clientUser)
        //{
        //    return (new AmiHighlightsModelManager(clientUser));
        //}


        /// <summary>
        /// Create AmiHighlightModelManager that implements IAmiHighlightsModel.
        /// </summary>
        /// <param name="clientUser"></param>
        /// <param name="unityContainer"></param>
        /// <returns></returns>
        public IAmiHighlightsModelManager CreateAmiHighlightsModelManager(ClientUser clientUser, IUnityContainer unityContainer) {
            return new AmiHighlightsModelManager(clientUser, unityContainer);
        }


        /// <summary>
        /// Create AmiHighlightModelManager that implements IAmiHighlightsModel.
        /// Set unityContainer and InsightRepository for unit test
        /// </summary>
        /// <param name="clientUser"></param>
        /// <param name="unityContainer"></param>
        /// <param name="insightsEfRepository"></param>
        /// <returns></returns>
        public IAmiHighlightsModelManager CreateAmiHighlightsModelManager(ClientUser clientUser, IUnityContainer unityContainer, IInsightsEFRepository insightsEfRepository)
        {
            return (new AmiHighlightsModelManager(clientUser, unityContainer, insightsEfRepository));
        }
    }
}
