﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using CE.Models.Insights;
using CE.Models.Insights.Types;
using CE.RateModel;

namespace CE.AmiHighlightsModelTests.Helpers
{
    public class TestHelper
    {

        public static BillResponse GetMockSingleServiceSinglePremiseSingleAccountData(int clientId, string customerId, string accountId, string premiseId, string serviceId, string commodityKey, string uomKey, int billDays, string rateclass, string add1, string city, string state, string zip, string fname, string lname, DateTime endDate)
        {
            var customer = new BillCustomer
            {
                Id = customerId,
                FirstName = fname,
                LastName = lname,
                Accounts = new List<BillAccount>()
            };

            var account = new BillAccount
            {
                Id = accountId,
                Bills = new List<Bill>()
            };

            var startDate = endDate.AddDays(billDays * -1 + 1);

            for (int i = 0; i < 24; i++)
            {
                var days = billDays;
                if (i%2 == 0)
                {
                    days = billDays + 1;
                }
                var billService = CreateBillService(serviceId, days, endDate.ToString("yyyy-MM-dd"),
                    startDate.ToString("yyyy-MM-dd"), commodityKey, rateclass, uomKey);
                var billPremise = CreateBillPremise(premiseId, add1, city, state, zip);
                billPremise.Service.Add(billService);
                var bill = CreateBill(endDate.ToString("yyyy-MM-dd"));
                bill.Premises.Add(billPremise);
                account.Bills.Add(bill);
                endDate = startDate.Date.AddDays(-1);
                startDate = endDate.AddDays(days * -1 + 1);
            }

            customer.Accounts.Add(account);
            return new BillResponse { ClientId = clientId, Customer = customer };
        }

        public static BillResponse GetMockSingleServiceSinglePremiseSingleAccountData_NoCommodityKey(int clientId, string customerId, string accountId, string premiseId, string serviceId, string commodityKey, string uomKey, int billDays, string rateclass, string add1, string city, string state, string zip, string fname, string lname, DateTime endDate)
        {
            var NoCommodityKey = "NoCommodityKey";

            var customer = new BillCustomer
            {
                Id = customerId,
                FirstName = fname,
                LastName = lname,
                Accounts = new List<BillAccount>()
            };

            var account = new BillAccount
            {
                Id = accountId,
                Bills = new List<Bill>()
            };

            var startDate = endDate.AddDays(billDays * -1 + 1);

            for (int i = 0; i < 24; i++)
            {
                var days = billDays;
                if (i % 2 == 0)
                {
                    days = billDays + 1;
                }
                var billService = CreateBillService(serviceId, days, endDate.ToString("yyyy-MM-dd"),
                    startDate.ToString("yyyy-MM-dd"), NoCommodityKey, rateclass, uomKey);
                var billPremise = CreateBillPremise(premiseId, add1, city, state, zip);
                billPremise.Service.Add(billService);
                var bill = CreateBill(endDate.ToString("yyyy-MM-dd"));
                bill.Premises.Add(billPremise);
                account.Bills.Add(bill);
                endDate = startDate.Date.AddDays(-1);
                startDate = endDate.AddDays(days * -1 + 1);
            }

            customer.Accounts.Add(account);
            return new BillResponse { ClientId = clientId, Customer = customer };
        }

        public static BillResponse GetMockMultipleServiceSinglePremiseSingleAccountData(int clientId, string customerId, string accountId, string premiseId, string serviceId, string serviceId2, string commodityKey, string commodity2Key, string uomKey, string uom2Key, int billDays, string rateclass, string rateclass2, string add1, string city, string state, string zip, string fname, string lname)
        {
            var customer = new BillCustomer
            {
                Id = customerId,
                FirstName = fname,
                LastName = lname,
                Accounts = new List<BillAccount>()
            };

            var account = new BillAccount
            {
                Id = accountId,
                Bills = new List<Bill>()
            };

            var endDate = DateTime.Today.Date.AddDays(-10);
            var startDate = endDate.AddDays(billDays * -1);

            for (int i = 0; i < 24; i++)
            {
                var billService = CreateBillService(serviceId, billDays, endDate.ToString("yyyy-MM-dd"),
                    startDate.ToString("yyyy-MM-dd"), commodityKey, rateclass, uomKey);
                var billService2 = CreateBillService(serviceId2, billDays, endDate.ToString("yyyy-MM-dd"),
                    startDate.ToString("yyyy-MM-dd"), commodity2Key, rateclass2, uom2Key);
                var billPremise = CreateBillPremise(premiseId, add1, city, state, zip);
                billPremise.Service.Add(billService);
                billPremise.Service.Add(billService2);
                var bill = CreateBill(endDate.ToString("yyyy-MM-dd"));
                bill.Premises.Add(billPremise);
                account.Bills.Add(bill);
                endDate = startDate.Date.AddDays(-1);
                startDate = endDate.AddDays(billDays * -1);
            }

            customer.Accounts.Add(account);
            return new BillResponse { ClientId = clientId, Customer = customer };
        }

        private static Bill CreateBill(string end)
        {

            var bill = new Bill
            {
                TotalAmount = 215.5m,
                AdditionalBillCost = 0,
                BillDate = end, //endDate.ToString("yyyy-MM-dd"),
                BillFrequency = "Monthly",
                Premises = new List<BillPremise>()
            };

            return bill;
        }

        private static BillPremise CreateBillPremise(string premiseId, string add1, string city, string state, string zip)
        {
            var billPremise = new BillPremise
            {
                Id = premiseId,
                Addr1 = add1,
                City = city,
                State = state,
                Zip = zip,
                Service = new List<BillService>()
            };

            return billPremise;
        }

        public static BillService CreateBillService(string serviceId, int billDays, string end, string start, string commodityKey, string rateclass, string uomKey)
        {
            var billService = new BillService
            {
                Id = serviceId,
                AdditionalServiceCost = 0,
                AverageTemperature = 72,
                BillDays = billDays,
                BillEndDate = end, //endDate.ToString("yyyy-MM-dd"),
                BillStartDate = start, //startDate.ToString("yyyy-MM-dd"),
                CommodityKey = commodityKey,
                CostofUsage = 193,
                RateClass = rateclass,
                TotalServiceUse = 550,
                UOMKey = uomKey
            };

            return billService;
        }

        public static CustomerInfoResponse GetMockSingleServiceSinglePremiseSingleCustomerInfoAccountData(int clientId, string customerId, 
            string accountId, string premiseId, string serviceId, string commodityKey, string add1, string city, string state, string zip, 
            string country, string fname, string lname, DateTime endDate, string customerInfoServiceId, DateTime activeDate, 
            string budgetBillingInd, string description, string inActiveDate, string type, string servicePointId, string meterId)
        {

            var customerInfo = new CustomerInfo
            {
                Id = customerId,
                FirstName = fname,
                LastName = lname,
                Accounts = new List<CustomerInfoAccount>()
            };

            var account = CreateCustomerInfoAccount(accountId);

            var premise = CreateCustomerInfoPremise(premiseId, add1, city, state, country, zip);

            var customerInfoService = CreateCustomerInfoService(customerInfoServiceId,
                activeDate, budgetBillingInd, commodityKey, description, inActiveDate, type);

            var servicePoint = CreateCustomerInfoServicePoint(servicePointId);

            var meter = CreateCustomerInfoMeter(meterId, null, null);

            servicePoint.Meters.Add(meter);
            customerInfoService.ServicePoints.Add(servicePoint);
            premise.Service.Add(customerInfoService);
            account.Premises.Add(premise);
            customerInfo.Accounts.Add(account);

            return new CustomerInfoResponse { ClientId = clientId, Customer = customerInfo };
        }

        public static CustomerInfoResponse GetMockSingleServiceSinglePremiseSingleCustomerInfoAccountData_NoCustomerInfoService(int clientId, string customerId,
            string accountId, string premiseId, string serviceId, string commodityKey, string add1, string city, string state, string zip,
            string country, string fname, string lname, DateTime endDate, string customerInfoServiceId, DateTime activeDate,
            string budgetBillingInd, string description, string inActiveDate, string type, string servicePointId, string meterId)
        {

            var customerInfo = new CustomerInfo
            {
                Id = customerId,
                FirstName = fname,
                LastName = lname,
                Accounts = new List<CustomerInfoAccount>()
            };

            var account = CreateCustomerInfoAccount(accountId);

            var premise = CreateCustomerInfoPremise(premiseId, add1, city, state, country, zip);

            // Test empty customerInfoService
            var customerInfoService = CreateCustomerInfoService(customerInfoServiceId, activeDate, budgetBillingInd, commodityKey, description, inActiveDate, type);
            //var customerInfoService = new CustomerInfoService();

            var servicePoint = CreateCustomerInfoServicePoint(servicePointId);

            var meter = CreateCustomerInfoMeter(meterId, null, null);

            servicePoint.Meters.Add(meter);
            //customerInfoService.ServicePoints.Add(servicePoint);
            premise.Service.Add(customerInfoService);
            account.Premises.Add(premise);
            customerInfo.Accounts.Add(account);

            return new CustomerInfoResponse { ClientId = clientId, Customer = customerInfo };
        }

        public static CustomerInfoResponse GetMockMultipleMeterMultipleServiceSinglePremiseSingleCustomerInfoAccountData(int clientId, string customerId,
            string accountId, string premiseId, string serviceId, string commodityKey, string commodityKey2, string add1, string city, string state, string zip,
            string country, string fname, string lname, DateTime endDate, string customerInfoServiceId, string customerInfoServiceId2, DateTime activeDate,
            string budgetBillingInd, string description, string inActiveDate, string type, string servicePointId, string servicePointId2, string meterId, string meterId2)
        {

            var customerInfo = new CustomerInfo
            {
                Id = customerId,
                FirstName = fname,
                LastName = lname,
                Accounts = new List<CustomerInfoAccount>()
            };

            var account = CreateCustomerInfoAccount(accountId);

            var premise = CreateCustomerInfoPremise(premiseId, add1, city, state, country, zip);

            var customerInfoService = CreateCustomerInfoService(customerInfoServiceId,
                activeDate, budgetBillingInd, commodityKey, description, inActiveDate, type);

            var customerInfoService2 = CreateCustomerInfoService(customerInfoServiceId2,
                activeDate, budgetBillingInd, commodityKey2, description, inActiveDate, type);

            var servicePoint = CreateCustomerInfoServicePoint(servicePointId);

            var servicePoint2 = CreateCustomerInfoServicePoint(servicePointId2);

            var meter = CreateCustomerInfoMeter(meterId, null, null);

            var meter2 = CreateCustomerInfoMeter(meterId2, null, null);

            servicePoint.Meters.Add(meter);
            servicePoint2.Meters.Add(meter2);
            customerInfoService.ServicePoints.Add(servicePoint);
            customerInfoService2.ServicePoints.Add(servicePoint2);
            premise.Service.Add(customerInfoService);
            premise.Service.Add(customerInfoService2);
            account.Premises.Add(premise);
            customerInfo.Accounts.Add(account);

            return new CustomerInfoResponse { ClientId = clientId, Customer = customerInfo };

        }

        public static CustomerInfoAccount CreateCustomerInfoAccount(string customerInfoAccountId)
        {
            var customerInfoAccount = new CustomerInfoAccount
            {
                Id = customerInfoAccountId,
                Premises = new List<CustomerInfoPremise>()
            };

            return customerInfoAccount;
        }

        public static CustomerInfoPremise CreateCustomerInfoPremise(string premiseId, string addr1, 
            string city, string stateProvince, string country, string postalCode)
        {
            var customerInfoPremise = new CustomerInfoPremise
            {
                Id = premiseId,
                Addr1 = addr1,
                City = city,
                StateProvince = stateProvince,
                Country = country,
                PostalCode = postalCode,
                Service = new List<CustomerInfoService>()
        };

            return customerInfoPremise;
        }

        public static CustomerInfoService CreateCustomerInfoService(string customerInfoServiceId,
            DateTime activeDate, string budgetBillingInd, string commodityKey,
            string description, string inActiveDate, string type)
        {
            var customerInfoService = new CustomerInfoService
            {
                Id = customerInfoServiceId,
                ActiveDate = activeDate.ToString(),
                BudgetBillingInd = budgetBillingInd,
                CommodityKey = commodityKey,
                Description = description,
                InActiveDate = inActiveDate,
                ServicePoints = new List<CustomerInfoServicePoint>(),
                Type = type
            };

            return customerInfoService;
        }

        public static CustomerInfoServicePoint CreateCustomerInfoServicePoint(string customerInfoServicePointId)
        {
            var customerInfoServicePoint = new CustomerInfoServicePoint
            {
                Id = customerInfoServicePointId,
                Meters = new List<CustomerInfoMeter>()
            };

            return customerInfoServicePoint;
        }

        public static CustomerInfoMeter CreateCustomerInfoMeter(string meterId,
            string isDeleted, string replacedMeterId)
        {
            var customerInfoMeter = new CustomerInfoMeter
            {
                Id = meterId,
                IsDeleted = isDeleted,
                ReplacedMeterId = replacedMeterId
            };

            return customerInfoMeter;
        }

        public static ConsumptionTouData CreateConsumptionTouData(string touKey, decimal value)
        {
            var consumptionTouData = new ConsumptionTouData
            {
                TouKey = touKey,
                Value = value
            };

            return consumptionTouData;
        }

        public static RateModel.Reading CreateDailyConsumptionData(DateTime dateTime, decimal value)
        {

            var reading = new RateModel.Reading(dateTime, Convert.ToDouble(value));
            reading.BaseOrTier = Enums.BaseOrTier.TotalServiceUse;
            reading.TimeOfUse = Enums.TimeOfUse.Undefined;
            reading.SpecialInterval = Enums.SpecialIntervalType.IntervalRegular;
            reading.Season = Enums.Season.Undefined;
            reading.IsEmpty = false;
            reading.IsGap = false;

            return reading;
        }

        public static List<RateModel.Reading> MockZeroImpactDailyConsumptionData(DateTime endDate)
        {
            var consumptionData = new List<RateModel.Reading>();

            consumptionData.Add(CreateDailyConsumptionData(endDate, 12.78M));
            consumptionData.Add(CreateDailyConsumptionData(endDate.AddDays(-1), 12.78M));
            consumptionData.Add(CreateDailyConsumptionData(endDate.AddDays(-2), 12.78M));
            consumptionData.Add(CreateDailyConsumptionData(endDate.AddDays(-3), 12.78M));
            consumptionData.Add(CreateDailyConsumptionData(endDate.AddDays(-4), 12.78M));
            consumptionData.Add(CreateDailyConsumptionData(endDate.AddDays(-5), 12.78M));
            consumptionData.Add(CreateDailyConsumptionData(endDate.AddDays(-6), 12.78M));
            consumptionData.Add(CreateDailyConsumptionData(endDate.AddDays(-7), 12.78M));
            consumptionData.Add(CreateDailyConsumptionData(endDate.AddDays(-8), 12.78M));
            consumptionData.Add(CreateDailyConsumptionData(endDate.AddDays(-9), 12.78M));
            consumptionData.Add(CreateDailyConsumptionData(endDate.AddDays(-11), 12.78M));
            consumptionData.Add(CreateDailyConsumptionData(endDate.AddDays(-12), 12.78M));
            consumptionData.Add(CreateDailyConsumptionData(endDate.AddDays(-13), 12.78M));
            consumptionData.Add(CreateDailyConsumptionData(endDate.AddDays(-14), 12.78M));
            consumptionData.Add(CreateDailyConsumptionData(endDate.AddDays(-15), 12.78M));
            consumptionData.Add(CreateDailyConsumptionData(endDate.AddDays(-16), 12.78M));
            consumptionData.Add(CreateDailyConsumptionData(endDate.AddDays(-17), 12.78M));
            consumptionData.Add(CreateDailyConsumptionData(endDate.AddDays(-18), 12.78M));
            consumptionData.Add(CreateDailyConsumptionData(endDate.AddDays(-19), 12.78M));
            consumptionData.Add(CreateDailyConsumptionData(endDate.AddDays(-20), 12.78M));
            consumptionData.Add(CreateDailyConsumptionData(endDate.AddDays(-21), 12.78M));
            consumptionData.Add(CreateDailyConsumptionData(endDate.AddDays(-22), 12.78M));
            consumptionData.Add(CreateDailyConsumptionData(endDate.AddDays(-23), 12.78M));
            consumptionData.Add(CreateDailyConsumptionData(endDate.AddDays(-24), 12.78M));
            consumptionData.Add(CreateDailyConsumptionData(endDate.AddDays(-25), 12.78M));
            consumptionData.Add(CreateDailyConsumptionData(endDate.AddDays(-26), 12.78M));
            consumptionData.Add(CreateDailyConsumptionData(endDate.AddDays(-27), 12.78M));
            consumptionData.Add(CreateDailyConsumptionData(endDate.AddDays(-28), 12.78M));
            consumptionData.Add(CreateDailyConsumptionData(endDate.AddDays(-29), 12.78M));
            consumptionData.Add(CreateDailyConsumptionData(endDate.AddDays(-30), 12.78M));
            consumptionData.Add(CreateDailyConsumptionData(endDate.AddDays(-31), 12.78M));

            return consumptionData;
        }

        public static List<RateModel.Reading> MockZeroAverageDailyConsumptionData(DateTime endDate)
        {
            var consumptionData = new List<RateModel.Reading>();

            consumptionData.Add(CreateDailyConsumptionData(endDate, 12.78M));
            consumptionData.Add(CreateDailyConsumptionData(endDate.AddDays(-1), 0M));
            consumptionData.Add(CreateDailyConsumptionData(endDate.AddDays(-2), 0M));
            consumptionData.Add(CreateDailyConsumptionData(endDate.AddDays(-3), 0M));
            consumptionData.Add(CreateDailyConsumptionData(endDate.AddDays(-4), 0M));
            consumptionData.Add(CreateDailyConsumptionData(endDate.AddDays(-5), 0M));
            consumptionData.Add(CreateDailyConsumptionData(endDate.AddDays(-6), 0M));
            consumptionData.Add(CreateDailyConsumptionData(endDate.AddDays(-7), 0M));
            consumptionData.Add(CreateDailyConsumptionData(endDate.AddDays(-8), 12.78M));
            consumptionData.Add(CreateDailyConsumptionData(endDate.AddDays(-9), 12.78M));
            consumptionData.Add(CreateDailyConsumptionData(endDate.AddDays(-11), 12.78M));
            consumptionData.Add(CreateDailyConsumptionData(endDate.AddDays(-12), 12.78M));
            consumptionData.Add(CreateDailyConsumptionData(endDate.AddDays(-13), 12.78M));
            consumptionData.Add(CreateDailyConsumptionData(endDate.AddDays(-14), 12.78M));
            consumptionData.Add(CreateDailyConsumptionData(endDate.AddDays(-15), 12.78M));
            consumptionData.Add(CreateDailyConsumptionData(endDate.AddDays(-16), 12.78M));
            consumptionData.Add(CreateDailyConsumptionData(endDate.AddDays(-17), 12.78M));
            consumptionData.Add(CreateDailyConsumptionData(endDate.AddDays(-18), 12.78M));
            consumptionData.Add(CreateDailyConsumptionData(endDate.AddDays(-19), 12.78M));
            consumptionData.Add(CreateDailyConsumptionData(endDate.AddDays(-20), 12.78M));
            consumptionData.Add(CreateDailyConsumptionData(endDate.AddDays(-21), 12.78M));
            consumptionData.Add(CreateDailyConsumptionData(endDate.AddDays(-22), 12.78M));
            consumptionData.Add(CreateDailyConsumptionData(endDate.AddDays(-23), 12.78M));
            consumptionData.Add(CreateDailyConsumptionData(endDate.AddDays(-24), 12.78M));
            consumptionData.Add(CreateDailyConsumptionData(endDate.AddDays(-25), 12.78M));
            consumptionData.Add(CreateDailyConsumptionData(endDate.AddDays(-26), 12.78M));
            consumptionData.Add(CreateDailyConsumptionData(endDate.AddDays(-27), 12.78M));
            consumptionData.Add(CreateDailyConsumptionData(endDate.AddDays(-28), 12.78M));
            consumptionData.Add(CreateDailyConsumptionData(endDate.AddDays(-29), 12.78M));
            consumptionData.Add(CreateDailyConsumptionData(endDate.AddDays(-30), 12.78M));
            consumptionData.Add(CreateDailyConsumptionData(endDate.AddDays(-31), 12.78M));

            return consumptionData;
        }

        public static List<RateModel.Reading> MockPositiveImpactDailyConsumptionData(DateTime endDate)
        {
            var consumptionData = new List<RateModel.Reading>();

            //this is the recent day = 35.63
            consumptionData.Add(CreateDailyConsumptionData(endDate, 35.63M));

            //the seven day average = 26.79
            consumptionData.Add(CreateDailyConsumptionData(endDate.AddDays(-1), 28.62M));
            consumptionData.Add(CreateDailyConsumptionData(endDate.AddDays(-2), 23.89M));
            consumptionData.Add(CreateDailyConsumptionData(endDate.AddDays(-3), 22.90M));
            consumptionData.Add(CreateDailyConsumptionData(endDate.AddDays(-4), 29.02M));
            consumptionData.Add(CreateDailyConsumptionData(endDate.AddDays(-5), 32.44M));
            consumptionData.Add(CreateDailyConsumptionData(endDate.AddDays(-6), 26.73M));
            consumptionData.Add(CreateDailyConsumptionData(endDate.AddDays(-7), 23.93M));

            consumptionData.Add(CreateDailyConsumptionData(endDate.AddDays(-8), 22.65M));
            consumptionData.Add(CreateDailyConsumptionData(endDate.AddDays(-9), 22.11M));
            consumptionData.Add(CreateDailyConsumptionData(endDate.AddDays(-11), 25.34M));
            consumptionData.Add(CreateDailyConsumptionData(endDate.AddDays(-12), 27.85M));
            consumptionData.Add(CreateDailyConsumptionData(endDate.AddDays(-13), 31.12M));
            consumptionData.Add(CreateDailyConsumptionData(endDate.AddDays(-14), 30.93M));
            consumptionData.Add(CreateDailyConsumptionData(endDate.AddDays(-15), 24.02M));
            consumptionData.Add(CreateDailyConsumptionData(endDate.AddDays(-16), 20.19M));
            consumptionData.Add(CreateDailyConsumptionData(endDate.AddDays(-17), 23.10M));
            consumptionData.Add(CreateDailyConsumptionData(endDate.AddDays(-18), 22.55M));
            consumptionData.Add(CreateDailyConsumptionData(endDate.AddDays(-19), 29.09M));
            consumptionData.Add(CreateDailyConsumptionData(endDate.AddDays(-20), 22.93M));
            consumptionData.Add(CreateDailyConsumptionData(endDate.AddDays(-21), 32.87M));
            consumptionData.Add(CreateDailyConsumptionData(endDate.AddDays(-22), 35.81M));
            consumptionData.Add(CreateDailyConsumptionData(endDate.AddDays(-23), 27.69M));
            consumptionData.Add(CreateDailyConsumptionData(endDate.AddDays(-24), 19.73M));
            consumptionData.Add(CreateDailyConsumptionData(endDate.AddDays(-25), 20.62M));
            consumptionData.Add(CreateDailyConsumptionData(endDate.AddDays(-26), 28.10M));
            consumptionData.Add(CreateDailyConsumptionData(endDate.AddDays(-27), 25.47M));
            consumptionData.Add(CreateDailyConsumptionData(endDate.AddDays(-28), 28.83M));
            consumptionData.Add(CreateDailyConsumptionData(endDate.AddDays(-29), 31.92M));
            consumptionData.Add(CreateDailyConsumptionData(endDate.AddDays(-30), 24.04M));
            consumptionData.Add(CreateDailyConsumptionData(endDate.AddDays(-31), 23.18M));

            return consumptionData;
        }

        public static List<RateModel.Reading> MockNegativeImpactDailyConsumptionData(DateTime endDate)
        {
            var consumptionData = new List<RateModel.Reading>();

            //this is the recent day = 21.86
            consumptionData.Add(CreateDailyConsumptionData(endDate, 21.86M));

            //the seven day average = 26.79
            consumptionData.Add(CreateDailyConsumptionData(endDate.AddDays(-1), 28.62M));
            consumptionData.Add(CreateDailyConsumptionData(endDate.AddDays(-2), 23.89M));
            consumptionData.Add(CreateDailyConsumptionData(endDate.AddDays(-3), 22.90M));
            consumptionData.Add(CreateDailyConsumptionData(endDate.AddDays(-4), 29.02M));
            consumptionData.Add(CreateDailyConsumptionData(endDate.AddDays(-5), 32.44M));
            consumptionData.Add(CreateDailyConsumptionData(endDate.AddDays(-6), 26.73M));
            consumptionData.Add(CreateDailyConsumptionData(endDate.AddDays(-7), 23.93M));
            consumptionData.Add(CreateDailyConsumptionData(endDate.AddDays(-8), 22.65M));
            consumptionData.Add(CreateDailyConsumptionData(endDate.AddDays(-9), 22.11M));
            consumptionData.Add(CreateDailyConsumptionData(endDate.AddDays(-11), 25.34M));
            consumptionData.Add(CreateDailyConsumptionData(endDate.AddDays(-12), 27.85M));
            consumptionData.Add(CreateDailyConsumptionData(endDate.AddDays(-13), 31.12M));
            consumptionData.Add(CreateDailyConsumptionData(endDate.AddDays(-14), 30.93M));
            consumptionData.Add(CreateDailyConsumptionData(endDate.AddDays(-15), 24.02M));
            consumptionData.Add(CreateDailyConsumptionData(endDate.AddDays(-16), 20.19M));
            consumptionData.Add(CreateDailyConsumptionData(endDate.AddDays(-17), 23.10M));
            consumptionData.Add(CreateDailyConsumptionData(endDate.AddDays(-18), 22.55M));
            consumptionData.Add(CreateDailyConsumptionData(endDate.AddDays(-19), 29.09M));
            consumptionData.Add(CreateDailyConsumptionData(endDate.AddDays(-20), 22.93M));
            consumptionData.Add(CreateDailyConsumptionData(endDate.AddDays(-21), 32.87M));
            consumptionData.Add(CreateDailyConsumptionData(endDate.AddDays(-22), 35.81M));
            consumptionData.Add(CreateDailyConsumptionData(endDate.AddDays(-23), 27.69M));
            consumptionData.Add(CreateDailyConsumptionData(endDate.AddDays(-24), 19.73M));
            consumptionData.Add(CreateDailyConsumptionData(endDate.AddDays(-25), 20.62M));
            consumptionData.Add(CreateDailyConsumptionData(endDate.AddDays(-26), 28.10M));
            consumptionData.Add(CreateDailyConsumptionData(endDate.AddDays(-27), 25.47M));
            consumptionData.Add(CreateDailyConsumptionData(endDate.AddDays(-28), 28.83M));
            consumptionData.Add(CreateDailyConsumptionData(endDate.AddDays(-29), 31.92M));
            consumptionData.Add(CreateDailyConsumptionData(endDate.AddDays(-30), 24.04M));
            consumptionData.Add(CreateDailyConsumptionData(endDate.AddDays(-31), 23.18M));

            return consumptionData;
        }

        public static List<RateModel.Reading> MockIntervalAMIData(DateTime endate, int interval, int numOfDays)
        {
            var consumptionData = new List<RateModel.Reading>();
            double consumptionconstant = 5;
            int mininHour = 60;
            var totalReadings = numOfDays * 24 * mininHour/interval;

            for (int i=1;i< totalReadings; i++)
            {
                var divider = mininHour/interval;

                var consumption = (consumptionconstant * (endate.AddMinutes(-i * interval)).Day)/(24 * divider);

                consumptionData.Add(CreateDailyConsumptionData(endate.AddMinutes(-i * interval), Convert.ToDecimal(consumption)));

            }
            return consumptionData;
        }

        public static List<RateModel.Reading> MockDailyReadings(DateTime endate, int numOfDays)
        {
            var consumptionData = new List<RateModel.Reading>();
            double consumptionconstant = 5;
            int mininHour = 60;
            //var totalReadings = numOfDays * 24 * mininHour / interval;

            for (int i = 1; i <= numOfDays; i++)
            {
                //var divider = mininHour / interval;

                var consumption = consumptionconstant * (endate.AddDays(-i).Day);

                consumptionData.Add(CreateDailyConsumptionData(endate.AddDays(-i), Convert.ToDecimal(consumption)));

            }
            return consumptionData;
        }
    }
}

