﻿using System;
using System.Collections.Generic;
using System.Linq;
using CE.AmiHighlightsModel;
using CE.AmiHighlightsModelTests.Helpers;
using CE.Models;
using CE.Models.Insights;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Microsoft.Practices.Unity;
using AO.BusinessContracts;
using AO.EventProcessors;
using CE.RateModel;
using AO.Registrar;

namespace CE.AmiHighlightsModelTests
{
    [TestClass()]
    public class AmiHighlightsModelTests
    {
        #region Constants for content

        private const string ClientSpace = "netnln2itvho";
        private const string ClientAccessToken = "c58ec8d495044174267c257f105914f7fb9567127a879b2419e1c3d405a54be2";

        #endregion

        private ClientUser _clientUser;
        private int _clientId = 87;
        private UnityContainer _container;
        private Mock<IInsightsEFRepository> _insightRepository;
        private Mock<ITallAMI> _amiManager;

        [TestInitialize]
        public void TestInit()
        {
            _container = new UnityContainer();
            (new DataStorageRegistrar()).Initialize<TransientLifetimeManager>(_container);

            _insightRepository = new Mock<IInsightsEFRepository>();
            _amiManager = new Mock<ITallAMI>();


            _container.RegisterInstance(_insightRepository.Object);
            _container.RegisterInstance(_amiManager.Object);

            //add property for client user, normally from the stored procedure but hardcoded here for unit test purposes
            _clientUser = new ClientUser
            {
                ClientID = _clientId,
                ClientProperties = new List<ClientProperties>
                {
                    new ClientProperties((PropertyType) Convert.ToInt32("0"), ClientAccessToken),
                    new ClientProperties((PropertyType) Convert.ToInt32("1"), ClientSpace)
                }
            };
        }

        [TestMethod()]
        public void GetAccountBillsTest_SingleService_Private()
        {
            var clientId = _clientId;
            var customerId = "GB001";
            var accountId = "GB001a1";
            var premiseId = "GB01a1p1";
            var serviceId = "GB01a1p1s1";
            var rateClass = "RG-1";
            var uomKey = "kwh";
            var commodityKey = "electric";
            var addr1 = "16 Laurel Ave";
            var city = "Wellesley";
            var state = "MA";
            var zip = "02481";
            var billDays = 30;
            var firstname = "TestFirst";
            var lastname = "TestLast";
            var billCount = 24;
            var today = DateTime.Today.Date;
            var startDate = today.AddMonths(24 * -1);
            

            var endDate = DateTime.Today.Date.AddDays(-10);
            var mockData = TestHelper.GetMockSingleServiceSinglePremiseSingleAccountData(clientId, customerId, accountId,
                premiseId, serviceId, commodityKey, uomKey, billDays, rateClass, addr1, city, state, zip, firstname,
                lastname, endDate);
            _insightRepository.Setup(
                c =>
                    c.GetBills(clientId,
                        It.Is<BillRequest>(
                            b =>
                                b.CustomerId == customerId && b.AccountId == accountId && b.Count == billCount &&
                                b.StartDate == startDate && b.EndDate == today))).Returns(mockData);
            var factory = new AmiHighlightsModelFactory();
            var model = factory.CreateAmiHighlightsModelManager(_clientUser, _container);
            //var model = new AmiHighlightsModel.AmiHighlightsModelManager(clientId, rateCompanyId, _insightRepository.Object);
            PrivateObject obj = new PrivateObject(model);
            var result = obj.Invoke("GetAccountBills", customerId,accountId);
            Assert.IsNotNull(result);
            var accountBills = (BillAccount)result;
            Assert.AreEqual(billCount, accountBills.Bills.Count);
            var premiseBills = accountBills.Bills.Where(b => b.Premises.All(p => p.Id == premiseId)).ToList();
            Assert.AreEqual(premiseId, premiseBills[0].Premises[0].Id);
            Assert.AreEqual(1, premiseBills[0].Premises[0].Service.Count);
            bool backwardOrder = Convert.ToDateTime(premiseBills[0].BillDate) > Convert.ToDateTime(premiseBills[1].BillDate);
            Assert.IsTrue(backwardOrder);
        }

        [TestMethod()]
        public void GetAccountBillsTest_MultipleService_Private()
        {
            var clientId = _clientId;
            var customerId = "GB001";
            var accountId = "GB001a1";
            var premiseId = "GB01a1p1";
            var serviceId = "GB01a1p1s1";
            var serviceId2 = "GB01a1p1s2";
            var rateClass = "RG-1";
            var rateClass2 = "RTGas";
            var uomKey = "kwh";
            var commodityKey = "electric";
            var commodityKey2 = "gas";
            var uomKey2 = "therms";
            var addr1 = "16 Laurel Ave";
            var city = "Wellesley";
            var state = "MA";
            var zip = "02481";
            var billDays = 30;
            var firstname = "TestFirst";
            var lastname = "TestLast";
            var billCount = 24;
            var today = DateTime.Today.Date;
            var startDate = today.AddMonths(24 * -1);


            var mockData = TestHelper.GetMockMultipleServiceSinglePremiseSingleAccountData(clientId, customerId, accountId,
                premiseId, serviceId, serviceId2, commodityKey, commodityKey2, uomKey, uomKey2, billDays, rateClass, rateClass2, addr1, city, state, zip, firstname,
                lastname);
            _insightRepository.Setup(
                c =>
                    c.GetBills(clientId,
                        It.Is<BillRequest>(
                            b =>
                                b.CustomerId == customerId && b.AccountId == accountId && b.Count == billCount &&
                                b.StartDate == startDate && b.EndDate == today))).Returns(mockData);


            var factory = new AmiHighlightsModelFactory();
            var model = factory.CreateAmiHighlightsModelManager(_clientUser, _container);

            PrivateObject obj = new PrivateObject(model);
            var result = obj.Invoke("GetAccountBills", customerId, accountId);
            Assert.IsNotNull(result);
            var accountBills = (BillAccount)result;
            Assert.AreEqual(billCount, accountBills.Bills.Count);

            var premiseBills = accountBills.Bills.Where(b => b.Premises.All(p => p.Id == premiseId)).ToList();
            Assert.AreEqual(premiseId, premiseBills[0].Premises[0].Id);
            Assert.AreEqual(2, premiseBills[0].Premises[0].Service.Count);
            bool backwardOrder = Convert.ToDateTime(premiseBills[0].BillDate) > Convert.ToDateTime(premiseBills[1].BillDate);
            Assert.IsTrue(backwardOrder);
        }

        [TestMethod()]
        public void GetAccountBillsTest_NoPremise_Private()
        {
            var clientId = _clientId;
            var customerId = "GB001";
            var accountId = "GB001a1";
            var premiseId = "GB01a1p1";
            var serviceId = "GB01a1p1s1";
            var serviceId2 = "GB01a1p1s2";
            var rateClass = "RG-1";
            var rateClass2 = "RTGas";
            var uomKey = "kwh";
            var commodityKey = "electric";
            var commodityKey2 = "gas";
            var uomKey2 = "therms";
            var addr1 = "16 Laurel Ave";
            var city = "Wellesley";
            var state = "MA";
            var zip = "02481";
            var billDays = 30;
            var firstname = "TestFirst";
            var lastname = "TestLast";
            var billCount = 24;
            var today = DateTime.Today.Date;
            var startDate = today.AddMonths(24 * -1);
            var requestPremise = "noPrmeise";


            var mockData = TestHelper.GetMockMultipleServiceSinglePremiseSingleAccountData(clientId, customerId, accountId,
                premiseId, serviceId, serviceId2, commodityKey, commodityKey2, uomKey, uomKey2, billDays, rateClass, rateClass2, addr1, city, state, zip, firstname,
                lastname);
            _insightRepository.Setup(
                c =>
                    c.GetBills(clientId,
                        It.Is<BillRequest>(
                            b =>
                                b.CustomerId == customerId && b.AccountId == accountId && b.Count == billCount &&
                                b.StartDate == startDate && b.EndDate == today))).Returns(mockData);


            var factory = new AmiHighlightsModelFactory();
            var model = factory.CreateAmiHighlightsModelManager(_clientUser, _container);

            PrivateObject obj = new PrivateObject(model);
            var result = obj.Invoke("GetAccountBills", customerId, accountId);
            Assert.IsNotNull(result);
            var accountBills = (BillAccount)result;

            var premiseBills = accountBills.Bills.Where(b => b.Premises.All(p => p.Id == requestPremise)).ToList();
            Assert.AreEqual(0, premiseBills.Count);
        }

        [TestMethod()]
        public void GetCustomerInfoAccountWithMetersTest_SingleService_Private()
        {
            var clientId = _clientId;
            var customerId = "GB001";
            var premiseId = "GB01a1p1";
            var serviceId = "GB01a1p1s1";
            var commodityKey = "electric";
            var addr1 = "16 Laurel Ave";
            var city = "Wellesley";
            var state = "MA";
            var zip = "02481";
            var country = "US";
            var firstname = "TestFirst";
            var lastname = "TestLast";
            var today = DateTime.Today.Date;
            var startDate = today.AddMonths(24 * -1);
            var activeDate = today.AddMonths(36 * -1);
            var budgetBillingInd = String.Empty;
            var description = String.Empty;
            var inActiveDate = String.Empty;
            var type = String.Empty;
            var customerInfoAccountId = "BTD001a1";
            var customerInfoServicePointId = "SP_BTD001a1";
            var meterId = "EM001";
            var endDate = DateTime.Today.Date.AddDays(-10);

            var mockData = TestHelper.GetMockSingleServiceSinglePremiseSingleCustomerInfoAccountData(clientId, customerId,
            customerInfoAccountId, premiseId, serviceId, commodityKey, addr1, city, state, zip,
            country, firstname, lastname, endDate, serviceId, activeDate,
            budgetBillingInd, description, inActiveDate, type, customerInfoServicePointId, meterId);

            _insightRepository.Setup(c => c.GetCustomerInfo(
                clientId, It.Is<CustomerInfoRequest>(b => b.CustomerId == customerId))).Returns(mockData);

            var factory = new AmiHighlightsModelFactory();
            var model = factory.CreateAmiHighlightsModelManager(_clientUser, _container);
            //var model = new AmiHighlightsModel.AmiHighlightsModelManager(clientId, rateCompanyId, _insightRepository.Object);
            PrivateObject obj = new PrivateObject(model);
            var result = obj.Invoke("GetCustomerInfoAccountWithMeters", customerId, customerInfoAccountId);
            Assert.IsNotNull(result);

            var customerInfoAccount = (CustomerInfoAccount)result;
            Assert.AreEqual(customerInfoAccountId, customerInfoAccount.Id);
            Assert.AreEqual(1, customerInfoAccount.Premises.Count);

            var premises = customerInfoAccount.Premises.Where(b => b.Id == premiseId).ToList();
            Assert.AreEqual(premiseId, premises[0].Id);
            Assert.AreEqual(1, premises.Count);

            var services = customerInfoAccount.Premises[0].Service.Where(b => b.Id == serviceId).ToList();
            Assert.AreEqual(serviceId, services[0].Id);
            Assert.AreEqual(1, services.Count);

            var servicePoints = customerInfoAccount.Premises[0].Service[0].ServicePoints.Where(b => b.Id == customerInfoServicePointId).ToList();
            Assert.AreEqual(customerInfoServicePointId, servicePoints[0].Id);
            Assert.AreEqual(1, servicePoints.Count);

            var meters = customerInfoAccount.Premises[0].Service[0].ServicePoints[0].Meters.Where(b => b.Id == meterId).ToList();
            Assert.AreEqual(meterId, meters[0].Id);
            Assert.AreEqual(1, meters.Count);

        }

        [TestMethod()]
        public void GetCustomerInfoAccountWithMetersTest_MultipleService_Private()
        {
            var clientId = _clientId;
            var customerId = "GB001";
            var premiseId = "GB01a1p1";
            var serviceId = "GB01a1p1s1";
            var serviceId2 = "GB01a1p1s2";
            var commodityKey = "electric";
            var commodityKey2 = "gas";
            var addr1 = "16 Laurel Ave";
            var city = "Wellesley";
            var state = "MA";
            var zip = "02481";
            var country = "US";
            var firstname = "TestFirst";
            var lastname = "TestLast";
            var today = DateTime.Today.Date;
            var startDate = today.AddMonths(24 * -1);
            var activeDate = today.AddMonths(36 * -1);
            var budgetBillingInd = String.Empty;
            var description = String.Empty;
            var inActiveDate = String.Empty;
            var type = String.Empty;
            var customerInfoAccountId = "BTD001a1";
            var customerInfoServicePointId = "SP_BTD001a1";
            var customerInfoServicePointId2 = "SP_BTD002a1";
            var meterId = "EM001";
            var meterId2 = "EM002";
            var endDate = DateTime.Today.Date.AddDays(-10);

            var mockData = TestHelper.GetMockMultipleMeterMultipleServiceSinglePremiseSingleCustomerInfoAccountData(clientId, customerId,
            customerInfoAccountId, premiseId, serviceId, commodityKey, commodityKey2, addr1, city, state, zip,
            country, firstname, lastname, endDate, serviceId, serviceId2, activeDate,
            budgetBillingInd, description, inActiveDate, type, customerInfoServicePointId, customerInfoServicePointId2, meterId, meterId2);

            _insightRepository.Setup(c => c.GetCustomerInfo(
                clientId, It.Is<CustomerInfoRequest>(b => b.CustomerId == customerId))).Returns(mockData);

            var factory = new AmiHighlightsModelFactory();
            var model = factory.CreateAmiHighlightsModelManager(_clientUser, _container);
            //var model = new AmiHighlightsModel.AmiHighlightsModelManager(clientId, rateCompanyId, _insightRepository.Object);
            PrivateObject obj = new PrivateObject(model);
            var result = obj.Invoke("GetCustomerInfoAccountWithMeters", customerId, customerInfoAccountId);
            Assert.IsNotNull(result);

            var customerInfoAccount = (CustomerInfoAccount)result;
            Assert.AreEqual(customerInfoAccountId, customerInfoAccount.Id);
            Assert.AreEqual(1, customerInfoAccount.Premises.Count);

            var premises = customerInfoAccount.Premises.Where(b => b.Id == premiseId).ToList();
            Assert.AreEqual(premiseId, premises[0].Id);
            Assert.AreEqual(1, premises.Count);

            var services = customerInfoAccount.Premises[0].Service.Where(b => b.Id == serviceId).ToList();
            Assert.AreEqual(serviceId, services[0].Id);
            Assert.AreEqual(1, services.Count);

            var servicePoints = customerInfoAccount.Premises[0].Service[0].ServicePoints.Where(b => b.Id == customerInfoServicePointId).ToList();
            Assert.AreEqual(customerInfoServicePointId, servicePoints[0].Id);
            Assert.AreEqual(1, servicePoints.Count);

            var meters = customerInfoAccount.Premises[0].Service[0].ServicePoints[0].Meters.Where(b => b.Id == meterId).ToList();
            Assert.AreEqual(meterId, meters[0].Id);
            Assert.AreEqual(1, meters.Count);

            var services2 = customerInfoAccount.Premises[0].Service.Where(b => b.Id == serviceId2).ToList();
            Assert.AreEqual(serviceId2, services2[0].Id);
            Assert.AreEqual(1, services2.Count);

            var servicePoints2 = customerInfoAccount.Premises[0].Service[1].ServicePoints.Where(b => b.Id == customerInfoServicePointId2).ToList();
            Assert.AreEqual(customerInfoServicePointId2, servicePoints2[0].Id);
            Assert.AreEqual(1, servicePoints2.Count);

            var meters2 = customerInfoAccount.Premises[0].Service[1].ServicePoints[0].Meters.Where(b => b.Id == meterId2).ToList();
            Assert.AreEqual(meterId2, meters2[0].Id);
            Assert.AreEqual(1, meters.Count);
        }

        [TestMethod()]
        public void CalcAmiSevenDayAverageHighlights_ZeroImpactTest_Private()
        {
            var clientId = _clientId;
            var customerId = "GB001";
            var accountId = "GB001a1";
            var premiseId = "GB01a1p1";
            var serviceId = "GB01a1p1s1";
            var servicePointId = "GB01a1p1s1";
            var meterId = "M189354704";
            var rateClass = "RG-1";
            var uomKey = "kwh";
            var commodityKey = "electric";
            var addr1 = "16 Laurel Ave";
            var city = "Wellesley";
            var state = "MA";
            var zip = "02481";
            var billDays = 30;
            var firstname = "TestFirst";
            var lastname = "TestLast";
            var today = Convert.ToDateTime("2/13/2010");

            var mockData = TestHelper.GetMockSingleServiceSinglePremiseSingleAccountData(clientId, customerId, accountId,
                premiseId, serviceId, commodityKey, uomKey, billDays, rateClass, addr1, city, state, zip, firstname,
                lastname, today);

            _insightRepository.Setup(
                c =>
                    c.GetBills(clientId,
                        It.IsAny<BillRequest>())).Returns(mockData);

            var factory = new AmiHighlightsModelFactory();
            var model = factory.CreateAmiHighlightsModelManager(_clientUser, _container);

            PrivateObject obj = new PrivateObject(model);
            var result = obj.Invoke("GetAccountBills", customerId, accountId);
            Assert.IsNotNull(result);
            var accountBills = (BillAccount)result;

            // get premise bills
            var premiseBills = accountBills?.Bills.Where(b => b.Premises.All(p => p.Id == premiseId)).ToList();
            var recentBill = premiseBills[0];
            var recentBillService = recentBill.Premises[0].Service[0];

            var mockDailyConsumptionData = TestHelper.MockZeroImpactDailyConsumptionData(today);
            var highlights = new List<AmiHighlightModel>();
            obj.Invoke("CalcAmiSevenDayAverageHighlight", mockDailyConsumptionData, recentBillService, accountId, premiseId, customerId,
                serviceId, servicePointId, meterId, highlights);

            Assert.IsNotNull(highlights);
            Assert.AreEqual(0, highlights.Count);
        }

        [TestMethod()]
        public void CalcAmiSevenDayAverageHighlights_SevenDayAverageIncreaseTest_Private()
        {
            var clientId = _clientId;
            var customerId = "GB001";
            var accountId = "GB001a1";
            var premiseId = "GB01a1p1";
            var serviceId = "GB01a1p1s1";
            var servicePointId = "GB01a1p1s1";
            var meterId = "M189354704";
            var rateClass = "RG-1";
            var uomKey = "kwh";
            var commodityKey = "electric";
            var addr1 = "16 Laurel Ave";
            var city = "Wellesley";
            var state = "MA";
            var zip = "02481";
            var billDays = 30;
            var firstname = "TestFirst";
            var lastname = "TestLast";
            var today = Convert.ToDateTime("2/13/2010");

            var mockData = TestHelper.GetMockSingleServiceSinglePremiseSingleAccountData(clientId, customerId, accountId,
                premiseId, serviceId, commodityKey, uomKey, billDays, rateClass, addr1, city, state, zip, firstname,
                lastname, today);
            _insightRepository.Setup(
                c =>
                    c.GetBills(clientId,
                        It.IsAny<BillRequest>())).Returns(mockData);
            var factory = new AmiHighlightsModelFactory();
            var model = factory.CreateAmiHighlightsModelManager(_clientUser, _container);

            PrivateObject obj = new PrivateObject(model);
            var result = obj.Invoke("GetAccountBills", customerId, accountId);
            Assert.IsNotNull(result);
            var accountBills = (BillAccount)result;

            // get premise bills
            var premiseBills = accountBills?.Bills.Where(b => b.Premises.All(p => p.Id == premiseId)).ToList();
            var recentBill = premiseBills[0];
            var recentBillService = recentBill.Premises[0].Service[0];

            var mockDailyConsumptionData = TestHelper.MockPositiveImpactDailyConsumptionData(today);
            var highlights = new List<AmiHighlightModel>();
            obj.Invoke("CalcAmiSevenDayAverageHighlight", mockDailyConsumptionData, recentBillService, accountId, premiseId, customerId,
                serviceId, servicePointId, meterId, highlights);

            Assert.IsNotNull(highlights);
            Assert.AreEqual(2, highlights.Count);
            foreach (var highlight in highlights)
            {
                if (highlight.ActionKey.Equals("insight.sevendayaverageincrease", StringComparison.CurrentCultureIgnoreCase))
                {
                    Assert.IsTrue(highlight.PercentageImpact.Equals(0.2518696983386036239522160317M));
                    Assert.IsTrue(highlight.CostImpact.Equals(48.610851779350499422777694118M));
                }
                else if (highlight.ActionKey.Equals("insight.sevendayaveragedecrease", StringComparison.CurrentCultureIgnoreCase))
                {
                    Assert.IsTrue(highlight.PercentageImpact.Equals(0M));
                    Assert.IsTrue(highlight.CostImpact.Equals(0M));
                }
                else
                {
                    Assert.Fail("highlight is not insight.sevendayaverageincrease or insight.sevendayaveragedecrease");
                }
            }
        }

        [TestMethod()]
        public void CalcAmiSevenDayAverageHighlights_SevenDayAverageDecreaseTest_Private()
        {
            var clientId = _clientId;
            var customerId = "GB001";
            var accountId = "GB001a1";
            var premiseId = "GB01a1p1";
            var serviceId = "GB01a1p1s1";
            var servicePointId = "GB01a1p1s1";
            var meterId = "M189354704";
            var rateClass = "RG-1";
            var uomKey = "kwh";
            var commodityKey = "electric";
            var addr1 = "16 Laurel Ave";
            var city = "Wellesley";
            var state = "MA";
            var zip = "02481";
            var billDays = 30;
            var firstname = "TestFirst";
            var lastname = "TestLast";
            var today = Convert.ToDateTime("2/13/2010");

            var mockData = TestHelper.GetMockSingleServiceSinglePremiseSingleAccountData(clientId, customerId, accountId,
                premiseId, serviceId, commodityKey, uomKey, billDays, rateClass, addr1, city, state, zip, firstname,
                lastname, today);
            _insightRepository.Setup(
                c =>
                    c.GetBills(clientId,
                        It.IsAny<BillRequest>())).Returns(mockData);
            var factory = new AmiHighlightsModelFactory();
            var model = factory.CreateAmiHighlightsModelManager(_clientUser, _container);

            PrivateObject obj = new PrivateObject(model);
            var result = obj.Invoke("GetAccountBills", customerId, accountId);
            Assert.IsNotNull(result);
            var accountBills = (BillAccount)result;

            // get premise bills
            var premiseBills = accountBills?.Bills.Where(b => b.Premises.All(p => p.Id == premiseId)).ToList();
            var recentBill = premiseBills[0];
            var recentBillService = recentBill.Premises[0].Service[0];

            var mockDailyConsumptionData = TestHelper.MockNegativeImpactDailyConsumptionData(today);
            var highlights = new List<AmiHighlightModel>();
            obj.Invoke("CalcAmiSevenDayAverageHighlight", mockDailyConsumptionData, recentBillService, accountId, premiseId, customerId,
                serviceId, servicePointId, meterId, highlights);

            Assert.IsNotNull(highlights);
            Assert.AreEqual(2, highlights.Count);
            foreach (var highlight in highlights)
            {
                var bResult =
                    highlight.ActionKey.Equals("insight.sevendayaverageincrease",
                        StringComparison.CurrentCultureIgnoreCase) ||
                    highlight.ActionKey.Equals("insight.sevendayaveragedecrease",
                        StringComparison.CurrentCultureIgnoreCase);

                Assert.IsTrue(bResult);
            }
        }

        [TestMethod()]
        public void CalcAmiSevenDayAverageHighlights_LessThanSevenDaysDataTest_Private()
        {
            var clientId = _clientId;
            var customerId = "GB001";
            var accountId = "GB001a1";
            var premiseId = "GB01a1p1";
            var serviceId = "GB01a1p1s1";
            var servicePointId = "GB01a1p1s1";
            var meterId = "M189354704";
            var rateClass = "RG-1";
            var uomKey = "kwh";
            var commodityKey = "electric";
            var addr1 = "16 Laurel Ave";
            var city = "Wellesley";
            var state = "MA";
            var zip = "02481";
            var billDays = 30;
            var firstname = "TestFirst";
            var lastname = "TestLast";
            var today = Convert.ToDateTime("2/13/2010");

            var mockData = TestHelper.GetMockSingleServiceSinglePremiseSingleAccountData(clientId, customerId, accountId,
                premiseId, serviceId, commodityKey, uomKey, billDays, rateClass, addr1, city, state, zip, firstname,
                lastname, today);
            _insightRepository.Setup(
                c =>
                    c.GetBills(clientId,
                        It.IsAny<BillRequest>())).Returns(mockData);
            var factory = new AmiHighlightsModelFactory();
            var model = factory.CreateAmiHighlightsModelManager(_clientUser, _container);

            PrivateObject obj = new PrivateObject(model);
            var result = obj.Invoke("GetAccountBills", customerId, accountId);
            Assert.IsNotNull(result);
            var accountBills = (BillAccount)result;

            // get premise bills
            var premiseBills = accountBills?.Bills.Where(b => b.Premises.All(p => p.Id == premiseId)).ToList();
            var recentBill = premiseBills[0];
            var recentBillService = recentBill.Premises[0].Service[0];

            // test 5 days of data
            var mockDailyConsumptionData = TestHelper.MockPositiveImpactDailyConsumptionData(today).Take(5).ToList();
            var highlights = new List<AmiHighlightModel>();
            obj.Invoke("CalcAmiSevenDayAverageHighlight", mockDailyConsumptionData, recentBillService, accountId, premiseId, customerId, serviceId, servicePointId, meterId, highlights);

            Assert.IsNotNull(highlights);
            Assert.AreEqual(0, highlights.Count);
        }

        [TestMethod()]
        public void CalcAmiSevenDayAverageHighlights_NoRecentDataTest_Private()
        {
            var clientId = _clientId;
            var customerId = "GB001";
            var accountId = "GB001a1";
            var premiseId = "GB01a1p1";
            var serviceId = "GB01a1p1s1";
            var servicePointId = "GB01a1p1s1";
            var meterId = "M189354704";
            var rateClass = "RG-1";
            var uomKey = "kwh";
            var commodityKey = "electric";
            var addr1 = "16 Laurel Ave";
            var city = "Wellesley";
            var state = "MA";
            var zip = "02481";
            var billDays = 30;
            var firstname = "TestFirst";
            var lastname = "TestLast";
            var today = Convert.ToDateTime("2/13/2010");

            var mockData = TestHelper.GetMockSingleServiceSinglePremiseSingleAccountData(clientId, customerId, accountId,
                premiseId, serviceId, commodityKey, uomKey, billDays, rateClass, addr1, city, state, zip, firstname,
                lastname, today);
            _insightRepository.Setup(
                c =>
                    c.GetBills(clientId,
                        It.IsAny<BillRequest>())).Returns(mockData);
            var factory = new AmiHighlightsModelFactory();
            var model = factory.CreateAmiHighlightsModelManager(_clientUser, _container);

            PrivateObject obj = new PrivateObject(model);
            var result = obj.Invoke("GetAccountBills", customerId, accountId);
            Assert.IsNotNull(result);
            var accountBills = (BillAccount)result;

            // get premise bills
            var premiseBills = accountBills?.Bills.Where(b => b.Premises.All(p => p.Id == premiseId)).ToList();
            var recentBill = premiseBills[0];
            var recentBillService = recentBill.Premises[0].Service[0];

            // test 0 days of data
            var mockDailyConsumptionData = TestHelper.MockPositiveImpactDailyConsumptionData(today).Take(0).ToList();
            var highlights = new List<AmiHighlightModel>();
            obj.Invoke("CalcAmiSevenDayAverageHighlight", mockDailyConsumptionData, recentBillService, accountId, premiseId, customerId,
                serviceId, servicePointId, meterId, highlights);

            Assert.IsNotNull(highlights);
            Assert.AreEqual(0, highlights.Count);
        }

        [TestMethod()]
        public void CalcAmiSevenDayAverageHighlights_ZeroSevenDayAverageTest_Private()
        {
            var clientId = _clientId;
            var customerId = "GB001";
            var accountId = "GB001a1";
            var premiseId = "GB01a1p1";
            var serviceId = "GB01a1p1s1";
            var servicePointId = "GB01a1p1s1";
            var meterId = "M189354704";
            var rateClass = "RG-1";
            var uomKey = "kwh";
            var commodityKey = "electric";
            var addr1 = "16 Laurel Ave";
            var city = "Wellesley";
            var state = "MA";
            var zip = "02481";
            var billDays = 30;
            var firstname = "TestFirst";
            var lastname = "TestLast";
            var today = Convert.ToDateTime("2/13/2010");

            var mockData = TestHelper.GetMockSingleServiceSinglePremiseSingleAccountData(clientId, customerId, accountId,
                premiseId, serviceId, commodityKey, uomKey, billDays, rateClass, addr1, city, state, zip, firstname,
                lastname, today);
            _insightRepository.Setup(
                c =>
                    c.GetBills(clientId,
                        It.IsAny<BillRequest>())).Returns(mockData);
            var factory = new AmiHighlightsModelFactory();
            var model = factory.CreateAmiHighlightsModelManager(_clientUser, _container);

            PrivateObject obj = new PrivateObject(model);
            var result = obj.Invoke("GetAccountBills", customerId, accountId);
            Assert.IsNotNull(result);
            var accountBills = (BillAccount)result;

            // get premise bills
            var premiseBills = accountBills?.Bills.Where(b => b.Premises.All(p => p.Id == premiseId)).ToList();
            var recentBill = premiseBills[0];
            var recentBillService = recentBill.Premises[0].Service[0];

            // test zero average data
            var mockDailyConsumptionData = TestHelper.MockZeroAverageDailyConsumptionData(today).Take(0).ToList();
            var highlights = new List<AmiHighlightModel>();
            obj.Invoke("CalcAmiSevenDayAverageHighlight", mockDailyConsumptionData, recentBillService, accountId, premiseId, customerId, serviceId, servicePointId, meterId, highlights);

            Assert.IsNotNull(highlights);
            Assert.AreEqual(0, highlights.Count);
        }

       [TestMethod()]
        public void CalcHighlightsTest()
        {
            var clientId = _clientId;
            var customerId = "GB001";
            var accountId = "GB001a1";
            var premiseId = "GB01a1p1";
            var serviceId = "GB01a1p1s1";
            var rateClass = "RG-1";
            var billDays = 30;
            var uomKey = "kwh";
            var commodityKey = "electric";
            var addr1 = "16 Laurel Ave";
            var city = "Wellesley";
            var state = "MA";
            var zip = "02481";
            var country = "US";
            var firstname = "TestFirst";
            var lastname = "TestLast";
            //var today = DateTime.Today.Date;
            var today = Convert.ToDateTime("2016-03-01");
            var startDate = today.AddMonths(24 * -1);
            var activeDate = today.AddMonths(36 * -1);
            var budgetBillingInd = String.Empty;
            var description = String.Empty;
            var inActiveDate = String.Empty;
            var type = String.Empty;
            var customerInfoAccountId = "GB001a1";
            var customerInfoServicePointId = "SP_GB001a1";
            var meterId = "EM001";
            var endDate = DateTime.Today.Date.AddDays(-10);
            var intervaltoMock = 15;

            var mockBillingData = TestHelper.GetMockSingleServiceSinglePremiseSingleAccountData(clientId, customerId, accountId,
                premiseId, serviceId, commodityKey, uomKey, billDays, rateClass, addr1, city, state, zip, firstname,
                lastname, today);
            _insightRepository.Setup(c => c.GetBills(clientId, It.IsAny<BillRequest>())).Returns(mockBillingData);

            var mockCustomerInfoAccountData = TestHelper.GetMockSingleServiceSinglePremiseSingleCustomerInfoAccountData(clientId, customerId,
            customerInfoAccountId, premiseId, serviceId, commodityKey, addr1, city, state, zip,
            country, firstname, lastname, endDate, serviceId, activeDate,
            budgetBillingInd, description, inActiveDate, type, customerInfoServicePointId, meterId);
            _insightRepository.Setup(c => c.GetCustomerInfo(clientId, It.IsAny<CustomerInfoRequest>())).Returns(mockCustomerInfoAccountData);

           // var mockDailyConsumptionData = TestHelper.MockPositiveImpactDailyConsumptionData(today);
            var dailyReadings = new List<Reading>();
            var intervalReadings = new List<Reading>();
            Mock<AmiHighlightsModelManager> _amiHighlightsModelManager;
            _amiHighlightsModelManager = new Mock<AmiHighlightsModelManager>(_clientUser, _container);


            intervalReadings = TestHelper.MockIntervalAMIData(today, intervaltoMock, 30);
            dailyReadings = TestHelper.MockDailyReadings(today, 30);

            _amiHighlightsModelManager.Setup(
                m =>
                    m.GetAmiReadings(_clientUser, It.IsAny<BillService>(), accountId, premiseId, customerId,
                        serviceId, customerInfoServicePointId, meterId, out dailyReadings, out intervalReadings, out intervaltoMock));

            
            var factory = new AmiHighlightsModelFactory();
            var model = factory.CreateAmiHighlightsModelManager(_clientUser, _container);
            PrivateObject obj = new PrivateObject(model);

            //dailyReadings = model.
            var resultAccountBills = obj.Invoke("GetAccountBills", customerId, accountId);
            Assert.IsNotNull(resultAccountBills);
            var accountBills = (BillAccount)resultAccountBills;

            var resultCustomerInfoAccount = obj.Invoke("GetCustomerInfoAccountWithMeters", customerId, accountId);
            Assert.IsNotNull(resultCustomerInfoAccount);
            var customerInfoAccount = (CustomerInfoAccount)resultCustomerInfoAccount;

            //_amiHighlightsModelManager.Object.
            var highlights = new List<AmiHighlightModel>();
            _amiHighlightsModelManager.Object.CalcHighlights(customerInfoAccount, accountBills, premiseId, customerId, accountId, commodityKey, ref highlights);

            Assert.IsNotNull(highlights);
            Assert.AreEqual(3, highlights.Count);
            foreach (var highlight in highlights)
            {
                var bResult = highlight.ActionKey.Equals("insight.sevendayaverageincrease", StringComparison.CurrentCultureIgnoreCase) ||
                    highlight.ActionKey.Equals("insight.sevendayaveragedecrease", StringComparison.CurrentCultureIgnoreCase) ||
                    highlight.ActionKey.Equals("insight.highestinterval", StringComparison.CurrentCultureIgnoreCase); ;


                Assert.IsTrue(bResult);
            }

        }

        private void Action()
        {
            throw new NotImplementedException();
        }

        [TestMethod()]
        public void CalcHighlights_NoBillsTest()
        {
            var clientId = _clientId;
            var customerId = "GB001";
            var accountId = "GB001a1";
            var premiseId = "GB01a1p1";
            var serviceId = "GB01a1p1s1";
            var commodityKey = "electric";
            var addr1 = "16 Laurel Ave";
            var city = "Wellesley";
            var state = "MA";
            var zip = "02481";
            var country = "US";
            var firstname = "TestFirst";
            var lastname = "TestLast";
            var today = DateTime.Today.Date;
            var startDate = today.AddMonths(24 * -1);
            var activeDate = today.AddMonths(36 * -1);
            var budgetBillingInd = String.Empty;
            var description = String.Empty;
            var inActiveDate = String.Empty;
            var type = String.Empty;
            var customerInfoAccountId = "GB001a1";
            var customerInfoServicePointId = "SP_GB001a1";
            var meterId = "EM001";
            var endDate = DateTime.Today.Date.AddDays(-10);
            var intervaltoMock = 15;
            _insightRepository.Setup(c => c.GetBills(clientId, It.IsAny<BillRequest>())).Returns(new BillResponse());

            var mockCustomerInfoAccountData = TestHelper.GetMockSingleServiceSinglePremiseSingleCustomerInfoAccountData(clientId, customerId,
            customerInfoAccountId, premiseId, serviceId, commodityKey, addr1, city, state, zip,
            country, firstname, lastname, endDate, serviceId, activeDate,
            budgetBillingInd, description, inActiveDate, type, customerInfoServicePointId, meterId);
            _insightRepository.Setup(c => c.GetCustomerInfo(clientId, It.IsAny<CustomerInfoRequest>())).Returns(mockCustomerInfoAccountData);

            //var mockDailyConsumptionData = TestHelper.MockPositiveImpactDailyConsumptionData(today);

            var dailyReadings = new List<Reading>();
            var intervalReadings = new List<Reading>();
            Mock<AmiHighlightsModelManager> _amiHighlightsModelManager;
            _amiHighlightsModelManager = new Mock<AmiHighlightsModelManager>(_clientUser, _container);
            _amiHighlightsModelManager.Setup(m => m.GetAmiReadings(_clientUser, It.IsAny<BillService>(), accountId, premiseId, customerId, serviceId, customerInfoServicePointId, meterId, out dailyReadings, out intervalReadings, out intervaltoMock));

            var factory = new AmiHighlightsModelFactory();
            var model = factory.CreateAmiHighlightsModelManager(_clientUser, _container);
            PrivateObject obj = new PrivateObject(model);

            var resultAccountBills = obj.Invoke("GetAccountBills", customerId, accountId);
            Assert.IsNull(resultAccountBills);
            var accountBills = new BillAccount();

            var resultCustomerInfoAccount = obj.Invoke("GetCustomerInfoAccountWithMeters", customerId, accountId);
            Assert.IsNotNull(resultCustomerInfoAccount);
            var customerInfoAccount = (CustomerInfoAccount)resultCustomerInfoAccount;

            var highlights = new List<AmiHighlightModel>();
            _amiHighlightsModelManager.Object.CalcHighlights(customerInfoAccount, accountBills, premiseId, customerId, accountId, commodityKey, ref highlights);

            Assert.IsNotNull(highlights);
            Assert.AreEqual(0, highlights.Count);

        }

        [TestMethod()]
        public void CalcHighlights_NoCustomerInfoAccountDataTest()
        {
            var clientId = _clientId;
            var customerId = "GB001";
            var accountId = "GB001a1";
            var premiseId = "GB01a1p1";
            var serviceId = "GB01a1p1s1";
            var rateClass = "RG-1";
            var billDays = 30;
            var uomKey = "kwh";
            var commodityKey = "electric";
            var addr1 = "16 Laurel Ave";
            var city = "Wellesley";
            var state = "MA";
            var zip = "02481";
            var firstname = "TestFirst";
            var lastname = "TestLast";
            var today = DateTime.Today.Date;
            var customerInfoServicePointId = "SP_GB001a1";
            var meterId = "EM001";
            var intervaltoMock = 15;

            var mockBillingData = TestHelper.GetMockSingleServiceSinglePremiseSingleAccountData(clientId, customerId, accountId,
                premiseId, serviceId, commodityKey, uomKey, billDays, rateClass, addr1, city, state, zip, firstname,
                lastname, today);
            _insightRepository.Setup(c => c.GetBills(clientId, It.IsAny<BillRequest>())).Returns(mockBillingData);

            // Test NULL
            _insightRepository.Setup(c => c.GetCustomerInfo(clientId, It.IsAny<CustomerInfoRequest>())).Returns((new CustomerInfoResponse()));

            //var mockDailyConsumptionData = TestHelper.MockPositiveImpactDailyConsumptionData(today);

            var dailyReadings = new List<Reading>();
            var intervalReadings = new List<Reading>();
            Mock<AmiHighlightsModelManager> _amiHighlightsModelManager;
            _amiHighlightsModelManager = new Mock<AmiHighlightsModelManager>(_clientUser, _insightRepository.Object);
            _amiHighlightsModelManager.Setup(m => m.GetAmiReadings(_clientUser, It.IsAny<BillService>(), accountId, premiseId, customerId, serviceId, customerInfoServicePointId, meterId, out dailyReadings, out intervalReadings, out intervaltoMock));

            var factory = new AmiHighlightsModelFactory();
            var model = factory.CreateAmiHighlightsModelManager(_clientUser, _container);
            PrivateObject obj = new PrivateObject(model);

            var resultAccountBills = obj.Invoke("GetAccountBills", customerId, accountId);
            Assert.IsNotNull(resultAccountBills);
            var accountBills = (BillAccount)resultAccountBills;

            var resultCustomerInfoAccount = obj.Invoke("GetCustomerInfoAccountWithMeters", customerId, accountId);
            Assert.IsNotNull(resultCustomerInfoAccount);
            var customerInfoAccount = (CustomerInfoAccount)resultCustomerInfoAccount;

            var highlights = new List<AmiHighlightModel>();
            _amiHighlightsModelManager.Object.CalcHighlights(customerInfoAccount, accountBills, premiseId, customerId, accountId, commodityKey, ref highlights);

            Assert.IsNotNull(highlights);
            Assert.AreEqual(0, highlights.Count);

        }

        [TestMethod()]
        public void CalcHighlights_NorequestedCommodityTest()
        {
            var clientId = _clientId;
            var customerId = "GB001";
            var accountId = "GB001a1";
            var premiseId = "GB01a1p1";
            var serviceId = "GB01a1p1s1";
            var rateClass = "RG-1";
            var billDays = 30;
            var uomKey = "kwh";
            var commodityKey = "electric";
            var addr1 = "16 Laurel Ave";
            var city = "Wellesley";
            var state = "MA";
            var zip = "02481";
            var country = "US";
            var firstname = "TestFirst";
            var lastname = "TestLast";
            var today = DateTime.Today.Date;
            var startDate = today.AddMonths(24 * -1);
            var activeDate = today.AddMonths(36 * -1);
            var budgetBillingInd = String.Empty;
            var description = String.Empty;
            var inActiveDate = String.Empty;
            var type = String.Empty;
            var customerInfoAccountId = "GB001a1";
            var customerInfoServicePointId = "SP_GB001a1";
            var meterId = "EM001";
            var endDate = DateTime.Today.Date.AddDays(-10);
            var intervaltoMock = 15;
            // test empty string
            var requestedCommodityTest = String.Empty;

            var mockBillingData = TestHelper.GetMockSingleServiceSinglePremiseSingleAccountData_NoCommodityKey(clientId, customerId, accountId,
                premiseId, serviceId, requestedCommodityTest, uomKey, billDays, rateClass, addr1, city, state, zip, firstname,
                lastname, today);
            _insightRepository.Setup(c => c.GetBills(clientId, It.IsAny<BillRequest>())).Returns(mockBillingData);

            var mockCustomerInfoAccountData = TestHelper.GetMockSingleServiceSinglePremiseSingleCustomerInfoAccountData(clientId, customerId,
            customerInfoAccountId, premiseId, serviceId, requestedCommodityTest, addr1, city, state, zip,
            country, firstname, lastname, endDate, serviceId, activeDate,
            budgetBillingInd, description, inActiveDate, type, customerInfoServicePointId, meterId);
            _insightRepository.Setup(c => c.GetCustomerInfo(clientId, It.IsAny<CustomerInfoRequest>())).Returns(mockCustomerInfoAccountData);

           // var mockDailyConsumptionData = TestHelper.MockPositiveImpactDailyConsumptionData(today);

            var dailyReadings = new List<Reading>();
            var intervalReadings = new List<Reading>();
            Mock<AmiHighlightsModelManager> _amiHighlightsModelManager;
            _amiHighlightsModelManager = new Mock<AmiHighlightsModelManager>(_clientUser, _insightRepository.Object);
            _amiHighlightsModelManager.Setup(m => m.GetAmiReadings(_clientUser, It.IsAny<BillService>(), accountId, premiseId, customerId, serviceId, customerInfoServicePointId, meterId, out dailyReadings, out intervalReadings, out intervaltoMock));

            var factory = new AmiHighlightsModelFactory();
            var model = factory.CreateAmiHighlightsModelManager(_clientUser, _container);
            PrivateObject obj = new PrivateObject(model);

            var resultAccountBills = obj.Invoke("GetAccountBills", customerId, accountId);
            Assert.IsNotNull(resultAccountBills);
            var accountBills = (BillAccount)resultAccountBills;

            var resultCustomerInfoAccount = obj.Invoke("GetCustomerInfoAccountWithMeters", customerId, accountId);
            Assert.IsNotNull(resultCustomerInfoAccount);
            var customerInfoAccount = (CustomerInfoAccount)resultCustomerInfoAccount;

            var highlights = new List<AmiHighlightModel>();
            _amiHighlightsModelManager.Object.CalcHighlights(customerInfoAccount, accountBills, premiseId, customerId, accountId, commodityKey, ref highlights);

            Assert.IsNotNull(highlights);
            Assert.AreEqual(0, highlights.Count);

        }

        [TestMethod()]
        public void CalcHighlights_BillAccountMismatchedPremiseIdTest()
        {
            var clientId = _clientId;
            var customerId = "GB001";
            var accountId = "GB001a1";
            var premiseId = "GB01a1p1";
            var serviceId = "GB01a1p1s1";
            var rateClass = "RG-1";
            var billDays = 30;
            var uomKey = "kwh";
            var commodityKey = "electric";
            var addr1 = "16 Laurel Ave";
            var city = "Wellesley";
            var state = "MA";
            var zip = "02481";
            var country = "US";
            var firstname = "TestFirst";
            var lastname = "TestLast";
            var today = DateTime.Today.Date;
            var startDate = today.AddMonths(24 * -1);
            var activeDate = today.AddMonths(36 * -1);
            var budgetBillingInd = String.Empty;
            var description = String.Empty;
            var inActiveDate = String.Empty;
            var type = String.Empty;
            var customerInfoAccountId = "GB001a1";
            var customerInfoServicePointId = "SP_GB001a1";
            var meterId = "EM001";
            var endDate = DateTime.Today.Date.AddDays(-10);
            var intervaltoMock = 15;
            // test empty string
            var billAccountMismatchedPremiseId = "MismatchedPremiseId";

            var mockBillingData = TestHelper.GetMockSingleServiceSinglePremiseSingleAccountData(clientId, customerId, accountId,
                premiseId, serviceId, commodityKey, uomKey, billDays, rateClass, addr1, city, state, zip, firstname,
                lastname, today);
            _insightRepository.Setup(c => c.GetBills(clientId, It.IsAny<BillRequest>())).Returns(mockBillingData);

            var mockCustomerInfoAccountData = TestHelper.GetMockSingleServiceSinglePremiseSingleCustomerInfoAccountData(clientId, customerId,
            customerInfoAccountId, billAccountMismatchedPremiseId, serviceId, commodityKey, addr1, city, state, zip,
            country, firstname, lastname, endDate, serviceId, activeDate,
            budgetBillingInd, description, inActiveDate, type, customerInfoServicePointId, meterId);
            _insightRepository.Setup(c => c.GetCustomerInfo(clientId, It.IsAny<CustomerInfoRequest>())).Returns(mockCustomerInfoAccountData);

            //var mockDailyConsumptionData = TestHelper.MockPositiveImpactDailyConsumptionData(today);

            var dailyReadings = new List<Reading>();
            var intervalReadings = new List<Reading>();
            Mock<AmiHighlightsModelManager> _amiHighlightsModelManager;
            _amiHighlightsModelManager = new Mock<AmiHighlightsModelManager>(_clientUser, _container);
            _amiHighlightsModelManager.Setup(m => m.GetAmiReadings(_clientUser, It.IsAny<BillService>(), accountId, premiseId, customerId, serviceId, customerInfoServicePointId, meterId, out dailyReadings, out intervalReadings, out intervaltoMock));

            var factory = new AmiHighlightsModelFactory();
            var model = factory.CreateAmiHighlightsModelManager(_clientUser, _container);
            PrivateObject obj = new PrivateObject(model);

            var resultAccountBills = obj.Invoke("GetAccountBills", customerId, accountId);
            Assert.IsNotNull(resultAccountBills);
            var accountBills = (BillAccount)resultAccountBills;

            var resultCustomerInfoAccount = obj.Invoke("GetCustomerInfoAccountWithMeters", customerId, accountId);
            Assert.IsNotNull(resultCustomerInfoAccount);
            var customerInfoAccount = (CustomerInfoAccount)resultCustomerInfoAccount;

            var highlights = new List<AmiHighlightModel>();
            _amiHighlightsModelManager.Object.CalcHighlights(customerInfoAccount, accountBills, premiseId, customerId, accountId, commodityKey, ref highlights);

            Assert.IsNotNull(highlights);
            Assert.AreEqual(0, highlights.Count);

        }

        [TestMethod()]
        public void CalcHighlights_BillAccountMismatchedServiceIdTest()
        {
            var clientId = _clientId;
            var customerId = "GB001";
            var accountId = "GB001a1";
            var premiseId = "GB01a1p1";
            var serviceId = "GB01a1p1s1";
            var rateClass = "RG-1";
            var billDays = 30;
            var uomKey = "kwh";
            var commodityKey = "electric";
            var addr1 = "16 Laurel Ave";
            var city = "Wellesley";
            var state = "MA";
            var zip = "02481";
            var country = "US";
            var firstname = "TestFirst";
            var lastname = "TestLast";
            var today = DateTime.Today.Date;
            var startDate = today.AddMonths(24 * -1);
            var activeDate = today.AddMonths(36 * -1);
            var budgetBillingInd = String.Empty;
            var description = String.Empty;
            var inActiveDate = String.Empty;
            var type = String.Empty;
            var customerInfoAccountId = "GB001a1";
            var customerInfoServicePointId = "SP_GB001a1";
            var meterId = "EM001";
            var endDate = DateTime.Today.Date.AddDays(-10);
            var intervaltoMock = 15;
            var mockBillingData = TestHelper.GetMockSingleServiceSinglePremiseSingleAccountData(clientId, customerId, accountId,
                premiseId, serviceId, commodityKey, uomKey, billDays, rateClass, addr1, city, state, zip, firstname,
                lastname, today);
            _insightRepository.Setup(c => c.GetBills(clientId, It.IsAny<BillRequest>())).Returns(mockBillingData);

            var mockCustomerInfoAccountData = TestHelper.GetMockSingleServiceSinglePremiseSingleCustomerInfoAccountData_NoCustomerInfoService(clientId, customerId,
            customerInfoAccountId, premiseId, serviceId, commodityKey, addr1, city, state, zip,
            country, firstname, lastname, endDate, serviceId, activeDate,
            budgetBillingInd, description, inActiveDate, type, customerInfoServicePointId, meterId);
            _insightRepository.Setup(c => c.GetCustomerInfo(clientId, It.IsAny<CustomerInfoRequest>())).Returns(mockCustomerInfoAccountData);

            var mockDailyConsumptionData = TestHelper.MockPositiveImpactDailyConsumptionData(today);

            var dailyReadings = new List<Reading>();
            var intervalReadings = new List<Reading>();
            Mock<AmiHighlightsModelManager> _amiHighlightsModelManager;
            _amiHighlightsModelManager = new Mock<AmiHighlightsModelManager>(_clientUser, _container);
            _amiHighlightsModelManager.Setup(m => m.GetAmiReadings(_clientUser, It.IsAny<BillService>(), accountId, premiseId, customerId, serviceId, customerInfoServicePointId, meterId, out dailyReadings, out intervalReadings, out intervaltoMock));

            var factory = new AmiHighlightsModelFactory();
            var model = factory.CreateAmiHighlightsModelManager(_clientUser, _container);
            PrivateObject obj = new PrivateObject(model);

            var resultAccountBills = obj.Invoke("GetAccountBills", customerId, accountId);
            Assert.IsNotNull(resultAccountBills);
            var accountBills = (BillAccount)resultAccountBills;

            var resultCustomerInfoAccount = obj.Invoke("GetCustomerInfoAccountWithMeters", customerId, accountId);
            Assert.IsNotNull(resultCustomerInfoAccount);
            var customerInfoAccount = (CustomerInfoAccount)resultCustomerInfoAccount;

            var highlights = new List<AmiHighlightModel>();
            _amiHighlightsModelManager.Object.CalcHighlights(customerInfoAccount, accountBills, premiseId, customerId, accountId, commodityKey, ref highlights);

            Assert.IsNotNull(highlights);
            Assert.AreEqual(0, highlights.Count);

        }

        [TestMethod()]
        public void CalcHighlights_BillAccountMismatchedServicePointIdTest()
        {
            var clientId = _clientId;
            var customerId = "GB001";
            var accountId = "GB001a1";
            var premiseId = "GB01a1p1";
            var serviceId = "GB01a1p1s1";
            var rateClass = "RG-1";
            var billDays = 30;
            var uomKey = "kwh";
            var commodityKey = "electric";
            var addr1 = "16 Laurel Ave";
            var city = "Wellesley";
            var state = "MA";
            var zip = "02481";
            var country = "US";
            var firstname = "TestFirst";
            var lastname = "TestLast";
            var today = DateTime.Today.Date;
            var startDate = today.AddMonths(24 * -1);
            var activeDate = today.AddMonths(36 * -1);
            var budgetBillingInd = String.Empty;
            var description = String.Empty;
            var inActiveDate = String.Empty;
            var type = String.Empty;
            var customerInfoAccountId = "GB001a1";
            var customerInfoServicePointId = "SP_GB001a1";
            var meterId = "EM001";
            var endDate = DateTime.Today.Date.AddDays(-10);
            var intervaltoMock = 15;
            // test empty string
            var billAccountMismatchedServicePointId = "MismatchedServiceId";

            var mockBillingData = TestHelper.GetMockSingleServiceSinglePremiseSingleAccountData(clientId, customerId, accountId,
                premiseId, serviceId, commodityKey, uomKey, billDays, rateClass, addr1, city, state, zip, firstname,
                lastname, today);
            _insightRepository.Setup(c => c.GetBills(clientId, It.IsAny<BillRequest>())).Returns(mockBillingData);

            var mockCustomerInfoAccountData = TestHelper.GetMockSingleServiceSinglePremiseSingleCustomerInfoAccountData(clientId, customerId,
            customerInfoAccountId, premiseId, serviceId, commodityKey, addr1, city, state, zip,
            country, firstname, lastname, endDate, serviceId, activeDate,
            budgetBillingInd, description, inActiveDate, type, billAccountMismatchedServicePointId, meterId);
            _insightRepository.Setup(c => c.GetCustomerInfo(clientId, It.IsAny<CustomerInfoRequest>())).Returns(mockCustomerInfoAccountData);

            //var mockDailyConsumptionData = TestHelper.MockPositiveImpactDailyConsumptionData(today);

            var dailyReadings = new List<Reading>();
            var intervalReadings = new List<Reading>();
            Mock<AmiHighlightsModelManager> _amiHighlightsModelManager;
            _amiHighlightsModelManager = new Mock<AmiHighlightsModelManager>(_clientUser, _container);
            _amiHighlightsModelManager.Setup(m => m.GetAmiReadings(_clientUser, It.IsAny<BillService>(), accountId, premiseId, customerId, serviceId, customerInfoServicePointId, meterId, out dailyReadings, out intervalReadings, out intervaltoMock));

            var factory = new AmiHighlightsModelFactory();
            var model = factory.CreateAmiHighlightsModelManager(_clientUser, _container);
            PrivateObject obj = new PrivateObject(model);

            var resultAccountBills = obj.Invoke("GetAccountBills", customerId, accountId);
            Assert.IsNotNull(resultAccountBills);
            var accountBills = (BillAccount)resultAccountBills;

            var resultCustomerInfoAccount = obj.Invoke("GetCustomerInfoAccountWithMeters", customerId, accountId);
            Assert.IsNotNull(resultCustomerInfoAccount);
            var customerInfoAccount = (CustomerInfoAccount)resultCustomerInfoAccount;

            var highlights = new List<AmiHighlightModel>();
            _amiHighlightsModelManager.Object.CalcHighlights(customerInfoAccount, accountBills, premiseId, customerId, accountId, commodityKey, ref highlights);

            Assert.IsNotNull(highlights);
            Assert.AreEqual(0, highlights.Count);

        }

        [TestMethod()]
        public void ExecuteAmiHighlightModelTest()
        {
            var clientId = _clientId;
            var customerId = "GB001";
            var accountId = "GB001a1";
            var premiseId = "GB01a1p1";
            var serviceId = "GB01a1p1s1";
            var rateClass = "RG-1";
            var billDays = 30;
            var uomKey = "kwh";
            var commodityKey = "electric";
            var addr1 = "16 Laurel Ave";
            var city = "Wellesley";
            var state = "MA";
            var zip = "02481";
            var country = "US";
            var firstname = "TestFirst";
            var lastname = "TestLast";
            var today = DateTime.Today.Date;
            var startDate = today.AddMonths(24 * -1);
            var activeDate = today.AddMonths(36 * -1);
            var budgetBillingInd = String.Empty;
            var description = String.Empty;
            var inActiveDate = String.Empty;
            var type = String.Empty;
            var customerInfoAccountId = "GB001a1";
            var customerInfoServicePointId = "SP_GB001a1";
            var meterId = "EM001";
            var endDate = DateTime.Today.Date.AddDays(-10);
            var intervaltoMock = 15;
            var mockBillingData = TestHelper.GetMockSingleServiceSinglePremiseSingleAccountData(clientId, customerId, accountId,
                premiseId, serviceId, commodityKey, uomKey, billDays, rateClass, addr1, city, state, zip, firstname,
                lastname, today);
            _insightRepository.Setup(c => c.GetBills(clientId, It.IsAny<BillRequest>())).Returns(mockBillingData);

            var mockCustomerInfoAccountData = TestHelper.GetMockSingleServiceSinglePremiseSingleCustomerInfoAccountData(clientId, customerId,
            customerInfoAccountId, premiseId, serviceId, commodityKey, addr1, city, state, zip,
            country, firstname, lastname, endDate, serviceId, activeDate,
            budgetBillingInd, description, inActiveDate, type, customerInfoServicePointId, meterId);
            _insightRepository.Setup(c => c.GetCustomerInfo(clientId, It.IsAny<CustomerInfoRequest>())).Returns(mockCustomerInfoAccountData);

            //var mockDailyConsumptionData = TestHelper.MockPositiveImpactDailyConsumptionData(today);

            var dailyReadings = new List<Reading>();
            var intervalReadings = new List<Reading>();
            dailyReadings = TestHelper.MockPositiveImpactDailyConsumptionData(today);
            Mock<AmiHighlightsModelManager> _amiHighlightsModelManager;
            _amiHighlightsModelManager = new Mock<AmiHighlightsModelManager>(_clientUser, _insightRepository.Object);
            _amiHighlightsModelManager.Setup(m => m.GetAmiReadings(_clientUser, It.IsAny<BillService>(), accountId, premiseId, customerId, serviceId, customerInfoServicePointId, meterId, out dailyReadings, out intervalReadings, out intervaltoMock ));


            //var factory = new AmiHighlightsModelFactory();
            //var model = factory.CreateAmiHighlightsModelManager(_clientUser, _insightRepository.Object);

            var result = _amiHighlightsModelManager.Object.ExecuteAmiHighlightModel(customerId, customerInfoAccountId, premiseId, commodityKey);
            Assert.IsNotNull(result);
            Assert.AreEqual(2, result.Count);
            foreach (var highlight in result)
            {
                var bResult = highlight.ActionKey.Equals("insight.sevendayaverageincrease", StringComparison.CurrentCultureIgnoreCase) ||
                    highlight.ActionKey.Equals("insight.sevendayaveragedecrease", StringComparison.CurrentCultureIgnoreCase);

                Assert.IsTrue(bResult);
            }
        }

        [TestMethod()]
        public void ExecuteAmiHighlightModel_UnavailableCommoditySpecified()
        {
            var clientId = _clientId;
            var customerId = "GB001";
            var accountId = "GB001a1";
            var premiseId = "GB01a1p1";
            var serviceId = "GB01a1p1s1";
            var rateClass = "RG-1";
            var billDays = 30;
            var uomKey = "kwh";
            var commodityKey = "electric";
            var addr1 = "16 Laurel Ave";
            var city = "Wellesley";
            var state = "MA";
            var zip = "02481";
            var country = "US";
            var firstname = "TestFirst";
            var lastname = "TestLast";
            var today = DateTime.Today.Date;
            var startDate = today.AddMonths(24 * -1);
            var activeDate = today.AddMonths(36 * -1);
            var budgetBillingInd = String.Empty;
            var description = String.Empty;
            var inActiveDate = String.Empty;
            var type = String.Empty;
            var customerInfoAccountId = "GB001a1";
            var customerInfoServicePointId = "SP_GB001a1";
            var meterId = "EM001";
            var endDate = DateTime.Today.Date.AddDays(-10);
            var intervaltoMock = 15;
            var mockBillingData = TestHelper.GetMockSingleServiceSinglePremiseSingleAccountData(clientId, customerId, accountId,
                premiseId, serviceId, commodityKey, uomKey, billDays, rateClass, addr1, city, state, zip, firstname,
                lastname, today);
            _insightRepository.Setup(c => c.GetBills(clientId, It.IsAny<BillRequest>())).Returns(mockBillingData);

            var mockCustomerInfoAccountData = TestHelper.GetMockSingleServiceSinglePremiseSingleCustomerInfoAccountData(clientId, customerId,
            customerInfoAccountId, premiseId, serviceId, commodityKey, addr1, city, state, zip,
            country, firstname, lastname, endDate, serviceId, activeDate,
            budgetBillingInd, description, inActiveDate, type, customerInfoServicePointId, meterId);
            _insightRepository.Setup(c => c.GetCustomerInfo(clientId, It.IsAny<CustomerInfoRequest>())).Returns(mockCustomerInfoAccountData);

            //var mockDailyConsumptionData = TestHelper.MockPositiveImpactDailyConsumptionData(today);

            var dailyReadings = new List<Reading>();
            var intervalReadings = new List<Reading>();
            Mock<AmiHighlightsModelManager> _amiHighlightsModelManager;
            _amiHighlightsModelManager = new Mock<AmiHighlightsModelManager>(_clientUser, _insightRepository.Object);
            _amiHighlightsModelManager.Setup(m => m.GetAmiReadings(_clientUser, It.IsAny<BillService>(), accountId, premiseId, customerId, serviceId, customerInfoServicePointId, meterId, out dailyReadings, out intervalReadings, out intervaltoMock));

            var result = _amiHighlightsModelManager.Object.ExecuteAmiHighlightModel(customerId, customerInfoAccountId, premiseId, "NoCommodityKey");
            Assert.IsNotNull(result);
            Assert.AreEqual(0, result.Count);
        }

        [TestMethod()]
        public void ExecuteAmiHighlightModel_GetCustomerInfoIsNull()
        {
            var clientId = _clientId;
            var customerId = "GB001";
            var accountId = "GB001a1";
            var premiseId = "GB01a1p1";
            var serviceId = "GB01a1p1s1";
            var rateClass = "RG-1";
            var billDays = 30;
            var uomKey = "kwh";
            var commodityKey = "electric";
            var addr1 = "16 Laurel Ave";
            var city = "Wellesley";
            var state = "MA";
            var zip = "02481";
            var firstname = "TestFirst";
            var lastname = "TestLast";
            var today = DateTime.Today.Date;
            var intervaltoMock = 15;
            var customerInfoAccountId = "GB001a1";
            var customerInfoServicePointId = "SP_GB001a1";
            var meterId = "EM001";
            var endDate = DateTime.Today.Date.AddDays(-10);

            var mockBillingData = TestHelper.GetMockSingleServiceSinglePremiseSingleAccountData(clientId, customerId, accountId,
                premiseId, serviceId, commodityKey, uomKey, billDays, rateClass, addr1, city, state, zip, firstname,
                lastname, today);
            _insightRepository.Setup(c => c.GetBills(clientId, It.IsAny<BillRequest>())).Returns(mockBillingData);

            // Test NULL
            _insightRepository.Setup(c => c.GetCustomerInfo(clientId, It.IsAny<CustomerInfoRequest>())).Returns(new CustomerInfoResponse());

            //var mockDailyConsumptionData = TestHelper.MockPositiveImpactDailyConsumptionData(today);

            var dailyReadings = new List<Reading>();
            var intervalReadings = new List<Reading>();
            Mock<AmiHighlightsModelManager> _amiHighlightsModelManager;
            _amiHighlightsModelManager = new Mock<AmiHighlightsModelManager>(_clientUser, _insightRepository.Object);
            _amiHighlightsModelManager.Setup(m => m.GetAmiReadings(_clientUser, It.IsAny<BillService>(), accountId, premiseId, customerId, serviceId, customerInfoServicePointId, meterId, out dailyReadings, out intervalReadings, out intervaltoMock));

            var result = _amiHighlightsModelManager.Object.ExecuteAmiHighlightModel(customerId, customerInfoAccountId, premiseId, "NoCommodityKey");
            Assert.IsNotNull(result);
            Assert.AreEqual(0, result.Count);
        }

        [TestMethod()]
        public void ExecuteAmiHighlightModel_NoCustomerInfoAccountId()
        {
            var clientId = _clientId;
            var customerId = "GB001";
            var accountId = "GB001a1";
            var premiseId = "GB01a1p1";
            var serviceId = "GB01a1p1s1";
            var rateClass = "RG-1";
            var billDays = 30;
            var uomKey = "kwh";
            var commodityKey = "electric";
            var addr1 = "16 Laurel Ave";
            var city = "Wellesley";
            var state = "MA";
            var zip = "02481";
            var firstname = "TestFirst";
            var lastname = "TestLast";
            var today = DateTime.Today.Date;
            var customerInfoServicePointId = "SP_GB001a1";
            var meterId = "EM001";
            var intervaltoMock = 15;
            var mockBillingData = TestHelper.GetMockSingleServiceSinglePremiseSingleAccountData(clientId, customerId, accountId,
                premiseId, serviceId, commodityKey, uomKey, billDays, rateClass, addr1, city, state, zip, firstname,
                lastname, today);
            _insightRepository.Setup(c => c.GetBills(clientId, It.IsAny<BillRequest>())).Returns(mockBillingData);

            var mockDailyConsumptionData = TestHelper.MockPositiveImpactDailyConsumptionData(today);

            var dailyReadings = new List<Reading>();
            var intervalReadings = new List<Reading>();
            Mock<AmiHighlightsModelManager> _amiHighlightsModelManager;
            _amiHighlightsModelManager = new Mock<AmiHighlightsModelManager>(_clientUser, _insightRepository.Object);
            _amiHighlightsModelManager.Setup(m => m.GetAmiReadings(_clientUser, It.IsAny<BillService>(), accountId, premiseId, customerId, serviceId, customerInfoServicePointId, meterId, out dailyReadings, out intervalReadings, out intervaltoMock));

            var result = _amiHighlightsModelManager.Object.ExecuteAmiHighlightModel(customerId, "NoCustomerInfoAccountId", premiseId, commodityKey);
            Assert.IsNotNull(result);
            Assert.AreEqual(0, result.Count);

        }

        [TestMethod()]
        public void ExecuteAmiHighlightModel_NoPremise()
        {
            var clientId = _clientId;
            var customerId = "GB001";
            var accountId = "GB001a1";
            var premiseId = "GB01a1p1";
            var serviceId = "GB01a1p1s1";
            var rateClass = "RG-1";
            var billDays = 30;
            var uomKey = "kwh";
            var commodityKey = "electric";
            var addr1 = "16 Laurel Ave";
            var city = "Wellesley";
            var state = "MA";
            var zip = "02481";
            var country = "US";
            var firstname = "TestFirst";
            var lastname = "TestLast";
            var today = DateTime.Today.Date;
            var startDate = today.AddMonths(24 * -1);
            var activeDate = today.AddMonths(36 * -1);
            var budgetBillingInd = String.Empty;
            var description = String.Empty;
            var inActiveDate = String.Empty;
            var type = String.Empty;
            var customerInfoAccountId = "GB001a1";
            var customerInfoServicePointId = "SP_GB001a1";
            var meterId = "EM001";
            var endDate = DateTime.Today.Date.AddDays(-10);

            var mockBillingData = TestHelper.GetMockSingleServiceSinglePremiseSingleAccountData(clientId, customerId, accountId,
                premiseId, serviceId, commodityKey, uomKey, billDays, rateClass, addr1, city, state, zip, firstname,
                lastname, today);
            _insightRepository.Setup(c => c.GetBills(clientId, It.IsAny<BillRequest>())).Returns(mockBillingData);

            var mockCustomerInfoAccountData = TestHelper.GetMockSingleServiceSinglePremiseSingleCustomerInfoAccountData(clientId, customerId,
            customerInfoAccountId, premiseId, serviceId, commodityKey, addr1, city, state, zip,
            country, firstname, lastname, endDate, serviceId, activeDate,
            budgetBillingInd, description, inActiveDate, type, customerInfoServicePointId, meterId);
            _insightRepository.Setup(c => c.GetCustomerInfo(clientId, It.IsAny<CustomerInfoRequest>())).Returns(mockCustomerInfoAccountData);

            var mockDailyConsumptionData = TestHelper.MockPositiveImpactDailyConsumptionData(today);

            Mock<AmiHighlightsModelManager> _amiHighlightsModelManager;
            _amiHighlightsModelManager = new Mock<AmiHighlightsModelManager>(_clientUser, _insightRepository.Object);
            //_amiHighlightsModelManager.Setup(m => m.GetConsumptionData(_clientUser, It.IsAny<BillService>(), accountId, premiseId, customerId, serviceId, customerInfoServicePointId, meterId)).Returns(mockDailyConsumptionData);

            var result = _amiHighlightsModelManager.Object.ExecuteAmiHighlightModel(customerId, customerInfoAccountId, "NoPremise", commodityKey);
            Assert.IsNotNull(result);
            Assert.AreEqual(0, result.Count);

        }

        [TestMethod()]
        public void ExecuteAmiHighlightModel_NoCustomer()
        {
            var clientId = _clientId;
            var customerId = "GB001";
            var accountId = "GB001a1";
            var premiseId = "GB01a1p1";
            var serviceId = "GB01a1p1s1";
            var rateClass = "RG-1";
            var billDays = 30;
            var uomKey = "kwh";
            var commodityKey = "electric";
            var addr1 = "16 Laurel Ave";
            var city = "Wellesley";
            var state = "MA";
            var zip = "02481";
            var country = "US";
            var firstname = "TestFirst";
            var lastname = "TestLast";
            var today = DateTime.Today.Date;
            var startDate = today.AddMonths(24 * -1);
            var activeDate = today.AddMonths(36 * -1);
            var budgetBillingInd = String.Empty;
            var description = String.Empty;
            var inActiveDate = String.Empty;
            var type = String.Empty;
            var customerInfoAccountId = "GB001a1";
            var customerInfoServicePointId = "SP_GB001a1";
            var meterId = "EM001";
            var endDate = DateTime.Today.Date.AddDays(-10);

            var mockBillingData = TestHelper.GetMockSingleServiceSinglePremiseSingleAccountData(clientId, customerId, accountId,
                premiseId, serviceId, commodityKey, uomKey, billDays, rateClass, addr1, city, state, zip, firstname,
                lastname, today);
            _insightRepository.Setup(c => c.GetBills(clientId, It.IsAny<BillRequest>())).Returns(mockBillingData);

            var mockCustomerInfoAccountData = TestHelper.GetMockSingleServiceSinglePremiseSingleCustomerInfoAccountData(clientId, customerId,
            customerInfoAccountId, premiseId, serviceId, commodityKey, addr1, city, state, zip,
            country, firstname, lastname, endDate, serviceId, activeDate,
            budgetBillingInd, description, inActiveDate, type, customerInfoServicePointId, meterId);
            _insightRepository.Setup(c => c.GetCustomerInfo(clientId, It.IsAny<CustomerInfoRequest>())).Returns(mockCustomerInfoAccountData);

            var mockDailyConsumptionData = TestHelper.MockPositiveImpactDailyConsumptionData(today);

            Mock<AmiHighlightsModelManager> _amiHighlightsModelManager;
            _amiHighlightsModelManager = new Mock<AmiHighlightsModelManager>(_clientUser, _insightRepository.Object);
            //_amiHighlightsModelManager.Setup(m => m.GetConsumptionData(_clientUser, It.IsAny<BillService>(), accountId, premiseId, customerId, serviceId, customerInfoServicePointId, meterId)).Returns(mockDailyConsumptionData);

            var result = _amiHighlightsModelManager.Object.ExecuteAmiHighlightModel("NoCustomer", customerInfoAccountId, premiseId, commodityKey);
            Assert.IsNotNull(result);
            Assert.AreEqual(0, result.Count);

        }

        [TestMethod()]
        public void GetConsumptionDataTest()
        {
            var clientId = _clientId;
            var customerId = "GB001";
            var accountId = "GB001a1";
            var premiseId = "GB01a1p1";
            var serviceId = "GB01a1p1s1";
            var servicePointId = "GB01a1p1s1";
            var meterId = "M189354704";
            var rateClass = "RG-1";
            var uomKey = "kwh";
            var commodityKey = "electric";
            var addr1 = "16 Laurel Ave";
            var city = "Wellesley";
            var state = "MA";
            var zip = "02481";
            var billDays = 30;
            var firstname = "TestFirst";
            var lastname = "TestLast";
            var today = Convert.ToDateTime("2/13/2010");
            var intervaltoMock = 15;
            var mockData = TestHelper.GetMockSingleServiceSinglePremiseSingleAccountData(clientId, customerId, accountId,
                premiseId, serviceId, commodityKey, uomKey, billDays, rateClass, addr1, city, state, zip, firstname,
                lastname, today);
            _insightRepository.Setup(
                c =>
                    c.GetBills(clientId,
                        It.IsAny<BillRequest>())).Returns(mockData);
            var factory = new AmiHighlightsModelFactory();
            var model = factory.CreateAmiHighlightsModelManager(_clientUser, _container);

            PrivateObject obj = new PrivateObject(model);
            var result = obj.Invoke("GetAccountBills", customerId, accountId);
            Assert.IsNotNull(result);
            var accountBills = (BillAccount)result;

            // get premise bills
            var premiseBills = accountBills?.Bills.Where(b => b.Premises.All(p => p.Id == premiseId)).ToList();
            var recentBill = premiseBills[0];
            var recentBillService = recentBill.Premises[0].Service[0];
            List<Reading> dailyReadings = new List<Reading>();
            List<Reading> intervalReadings = new List<Reading>();

             model.GetAmiReadings(_clientUser, recentBillService, accountId, premiseId, customerId, serviceId,
                servicePointId, meterId, out dailyReadings, out intervalReadings, out intervaltoMock);

            Assert.IsNotNull(dailyReadings);
            Assert.IsNotNull(intervalReadings);
        }


    }
}