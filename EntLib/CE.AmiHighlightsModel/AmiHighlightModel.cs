﻿
using System;

namespace CE.AmiHighlightsModel
{
    public class AmiHighlightModel
    {
        /// <summary>
        /// Account Id of the highlight.
        /// </summary>
        public string AccountId { get; set; }
        /// <summary>
        /// Premise Id of the highlight.
        /// </summary>
        public string PremiseId { get; set; }
        /// <summary>
        /// Service Id (Service Point Id) of the highlight.
        /// </summary>
        public string ServiceId { get; set; }
        /// <summary>
        /// Identifier of the action from the content management system.
        /// </summary>
        public string ActionKey { get; set; }

        /// <summary>
        /// Cost of impact.
        /// </summary>
        public decimal CostImpact { get; set; }

        /// <summary>
        /// Percentage of impact.
        /// </summary>
        public decimal PercentageImpact { get; set; }

        /// <summary>
        /// Type of commodity. 
        /// </summary>
        public string Commoditykey { get; set; }

        /// <summary>
        /// Type of Highlight
        /// </summary>
        public string HighlightLevel { get; set; }

        /// <summary>
        /// Meter Id of the highlight.
        /// </summary>
        public string MeterId { get; set; }


        /// <summary>
        /// Date for the AMI Highlight.
        /// </summary>
        public DateTime? HighlightTimeStamp{ get; set; }
    }
}
