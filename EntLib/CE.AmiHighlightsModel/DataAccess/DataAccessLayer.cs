﻿using System.Configuration;

namespace CE.AmiHighlightsModel.DataAccess
{
    public partial class DataAccessLayer
    {
        private const string InsightsMetaDataConnName = "InsightsMetaDataConn";

        private readonly string _metaDataConnString;

        public DataAccessLayer()
        {
            _metaDataConnString = ConfigurationManager.ConnectionStrings[InsightsMetaDataConnName].ConnectionString;
        }

        private string GetConnectionStringMetaData()
        {
            return _metaDataConnString;
        }
    }
}
