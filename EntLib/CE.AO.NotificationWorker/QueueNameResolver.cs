﻿using Microsoft.Azure;
using Microsoft.Azure.WebJobs;

namespace CE.AO.NotificationWorker
{
    class QueueNameResolver : INameResolver
    {
        public string Resolve(string name)
        {
            return CloudConfigurationManager.GetSetting(name);
        }
    }
}
