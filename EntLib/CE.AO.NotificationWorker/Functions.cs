﻿using System;
using System.IO;
using System.Linq;
using AO.BusinessContracts;
using AO.Registrar;
using CE.AO.Logging;
using CE.AO.Models;
using Microsoft.Azure.WebJobs;
using Microsoft.Practices.Unity;
using Enums = CE.AO.Utilities.Enums;

namespace CE.AO.NotificationWorker
{
    public static class Functions
    {
        public static LogModel LogModel { get; set; }
        public static IUnityContainer UnityContainer { get; set; }

        public static void ProcessNotification([QueueTrigger(StaticConfig.NotificationQueue)] string message, TextWriter log)
        {
			LogModel logModel = new LogModel();
			if (LogModel == null)
			{
				logModel.Module = Enums.Module.Notification;
			}
			else
			{
				logModel = LogModel;
			}

            IUnityContainer unityContainer = new UnityContainer();
            if (UnityContainer != null)
                unityContainer = UnityContainer;
            else
                (new DataStorageRegistrar()).Initialize<TransientLifetimeManager>(unityContainer);


            try
            {
                // Read the Queue Message
                //ClientId^^Cusotmer Id^^Account Id^^Service Contract Id^^Program Name^^Source^^RowIndex^^Metadata^^ProcessingType
                //These parameters are required to get subscriptions and evaluations          
                string[] parameter = message.Split(new[] { "^^" }, StringSplitOptions.None).ToArray();
                if (parameter.Length == 10)
                {
                    var clientId = Convert.ToInt32(parameter[0]);
                    var customerId = parameter[1];
                    var accountId = parameter[2];
                    var serviceContractId = parameter[3];
                    var programName = parameter[4];
                    var source = parameter[5];
                    var rowIndex = parameter[6];
                    var metadata = parameter[7];
                    var processingType = parameter[8];
                    var asOfEvaluationId = parameter[9];

                    // Set Log Parameters
                    logModel.ClientId = clientId.ToString();
                    logModel.CustomerId = customerId;
                    logModel.AccountId = accountId;
                    logModel.ServiceContractId = serviceContractId;
                    logModel.Source = source;
                    logModel.RowIndex = rowIndex;
                    logModel.Metadata = metadata;
                    logModel.ProcessingType = (Enums.ProcessingType)Enum.Parse(typeof(Enums.ProcessingType), processingType);

                    // Create Facade Object to start Notification Process
                    var notificationFacade = unityContainer.Resolve<INotificationFacade>(new ParameterOverride("logModel", logModel));
                    ////Logger.Info(
                    ////    $"Notification started for client {clientId} customer {customerId} account {accountId} serviceContract {serviceContractId} programName {programName} evalTickCount {evaluationTickCount}",
                    ////    logModel);
                    notificationFacade.ProcessNotification(clientId, customerId, accountId, serviceContractId,
                        programName, string.IsNullOrWhiteSpace(asOfEvaluationId)
                            ? (Guid?)null
							: new Guid(asOfEvaluationId));
                }
                else
                {
                    Logger.Fatal(
                        $@"Queue message ""{message
                            }"" received for notification is not in proper format. Format should be ""ClientId^^Cusotmer Id^^Account Id^^Service Contract Id^^Program Name^^Source^^Row Index^^Metadata^^Processing Type"".",
                        logModel);
                }
            }
            catch (Exception ex)
            {
                // Fill Log Model and log
                Logger.Error(ex, logModel);
                throw;
            }
        }

        public static void ProcessNotificationReport([QueueTrigger("%AONotificationReportQueue%")] string data, TextWriter log)
        {
            LogModel logModel = new LogModel();
            if (LogModel == null)
            {
                logModel.Module = Enums.Module.InsightEvaluation;
            }
            else
            {
                logModel = LogModel;
            }

            IUnityContainer unityContainer = new UnityContainer();
            if (UnityContainer != null)
                unityContainer = UnityContainer;
            else
                (new DataStorageRegistrar()).Initialize<TransientLifetimeManager>(unityContainer);

            try
            {
                // Read the Queue Message
                //ClientId^^date^^ProcessingType
                //These parameters are required for re           
                string[] parameter = data.Split(new[] { "^^" }, StringSplitOptions.None).ToArray();
                if (parameter.Length == 3)
                {
                    var clientId = Convert.ToInt32(parameter[0]);
                    var date = parameter[1];
                    var processingType = parameter[2];

                    // Set Log Parameters
                    logModel.ClientId = clientId.ToString();
                    logModel.ProcessingType = (Enums.ProcessingType)Convert.ToInt32(processingType);

                    var asOfDate = DateTime.Now;
                    if (!string.IsNullOrEmpty(date))
                        asOfDate = Convert.ToDateTime(date);
                    var notificationReport = unityContainer.Resolve<IReport>(new ParameterOverride("logModel", logModel));

                    Logger.Info($"Export notification started for client {clientId}", logModel);

                    notificationReport.ExportNotifications(clientId, asOfDate);
                }
                else
                {
                    Logger.Fatal(
                        $@"Queue message ""{data
                            }"" received for notification report is not in proper format. Format should be ""ClientId^^Date^^ProcessingType"".",
                        logModel);
                }
            }
            catch (Exception ex)
            {
                // Fill Log Model and log
                Logger.Error(ex, logModel);
                throw;
            }
        }
    }
}
