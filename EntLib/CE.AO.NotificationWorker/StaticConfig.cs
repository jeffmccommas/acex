﻿namespace CE.AO.NotificationWorker
{
    public static class StaticConfig
    {
        public const string AclaraOneStorageConnectionString = "AclaraOneStorageConnectionString";
        public const string NotificationQueue = "%NotificationQueue%";
    }
}
