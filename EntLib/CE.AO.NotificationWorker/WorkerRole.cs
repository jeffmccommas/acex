using System;
using System.Diagnostics;
using System.Net;
using System.Threading;
using AO.Registrar;
using Microsoft.Azure;
using Microsoft.Azure.WebJobs;
using Microsoft.Practices.Unity;
using Microsoft.WindowsAzure.ServiceRuntime;

namespace CE.AO.NotificationWorker
{
    public class WorkerRole : RoleEntryPoint, IDisposable
    {
        private CancellationTokenSource _cancellationTokenSource = new CancellationTokenSource();
        private ManualResetEvent _runCompleteEvent = new ManualResetEvent(false);

        public override void Run()
        {
            Trace.TraceInformation("CE.AO.NotificationWorker is running");

            try
            {
                if (!_cancellationTokenSource.Token.IsCancellationRequested)
                {
                    new DataStorageRegistrar().Initialize<TransientLifetimeManager>();


                    var storageConn = CloudConfigurationManager.GetSetting(StaticConfig.AclaraOneStorageConnectionString);
                    var config = new JobHostConfiguration { NameResolver = new QueueNameResolver() };

                    int inputQueueBatchSize = 8;
                    int inputQueueDequeueCount = 5;
                    var testVal = CloudConfigurationManager.GetSetting("InputQueueBatchSize");
                    if (!string.IsNullOrEmpty(testVal)) {
                        int testInt;
                        if (int.TryParse(testVal, out testInt)) {
                            inputQueueBatchSize = testInt;
                        }
                    }
                    testVal = CloudConfigurationManager.GetSetting("InputQueueDequeueCount");
                    if (!string.IsNullOrEmpty(testVal)) {
                        int testInt;
                        if (int.TryParse(testVal, out testInt)) {
                            inputQueueDequeueCount = testInt;
                        }
                    }


                    config.Queues.BatchSize = inputQueueBatchSize; //(default is 16).  cjh was 8
                    config.Queues.MaxDequeueCount = inputQueueDequeueCount;  //(default is 5).  cjh was 5
                    config.Queues.MaxPollingInterval = TimeSpan.FromSeconds(15); //(default is 1 minute).
                    config.StorageConnectionString = storageConn;
                    config.DashboardConnectionString = storageConn;

                    var host = new JobHost(config);
                    host.RunAndBlock();
                }

                //Wait for shutdown to be called, else the role will recycle
                _runCompleteEvent.WaitOne();
            }
            finally
            {
                _runCompleteEvent.Set();
            }
        }

        public override bool OnStart()
        {
            // Set the maximum number of concurrent connections
            ServicePointManager.DefaultConnectionLimit = 12;

            // For information on handling configuration changes
            // see the MSDN topic at http://go.microsoft.com/fwlink/?LinkId=166357.

            bool result = base.OnStart();

            Trace.TraceInformation("CE.AO.NotificationWorker has been started");

            return result;
        }

        public override void OnStop()
        {
            Trace.TraceInformation("CE.AO.NotificationWorker is stopping");

            _cancellationTokenSource.Cancel();
            _runCompleteEvent.WaitOne();

            base.OnStop();

            Trace.TraceInformation("CE.AO.NotificationWorker has stopped");
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (_cancellationTokenSource != null)
            {
                _cancellationTokenSource.Cancel();
                _cancellationTokenSource.Dispose();
                _cancellationTokenSource = null;
            }
            if (_runCompleteEvent != null)
            {
                _runCompleteEvent.Dispose();
                _runCompleteEvent = null;
            }
        }
    }
}
