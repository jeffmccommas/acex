﻿using CE.ContentModel;
using CE.ContentModel.ContentCache;
using CE.NotificationManager.Entities;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Linq;
using System.Xml.Serialization;

namespace CE.WebServiceClient.ELRS
{
    public class ElrsClient : IElrs
    {
        #region "private variables"
        private ElrsConfig _config;
        private readonly NLogger.ASyncLogger _logger;
        private string _environment;
        private readonly int _clientId;
        private bool _configSetUp;
        //private bool _simulation;
        //private int _simulationInterval;
        #endregion

        #region "private const"
        private const string PjmNoticationEvent = "PJM Notification Event";
        private const string SeverityHigh = "High";
        private const string SeverityMedium = "Medium";
        private const string SeverityLow = "Low";
        private const string CatagoryPoll = "Poll";
        private const string CatagoryPingPoll = "Ping Poll";
        private const string CatagoryAck = "Acknowledge";
        private const string EventBegin = "Begins";
        private const string EventEnd = "Ends";
        private const string MsgPollEvents = "PJM Poll Event";
        private const string MsgAckEvents = "Acknowledge Event";
        private const string MsgProcessResponse = "Process PJM Response";
        private const string MsgSimulateEvents = "PJM Event Simulation";
        private const string MsgProcessEvents = "Process PJM Events";
        private const string MsgEventsReceived = "Event Received";
        private const string MsgRequest = "Request";
        private const string MsgResponse = "Response";
        private const string MsgPingEvents = "Ping Poll Event";
        private const string MsgNoEvent = "No Event";
        private const string MsgNoTeamNameMapping = "No Team Name Mapping for Registration ID";
        private const string HeaderVerbGet = "get";
        private const string HeaderVerbChange = "change";
        private const string HeaderNounEvent = "event";
        private const string HeaderRevision = "1.0";
        private const string HeaderSource = "OECSP";
        private const string RequestOptionUnAck = "unack";
        private const string RequestOptionAck = "ack";
        private const string PayloadFormat = "XML";
        private const string ReplyCodeOkay = "ok";
        private const string ReplyCodeWarning = "warning";
        private const string ReplyCodeFatal = "fatal";
        private const string ErrorCode = "4005";
        private const int DefaultMaxRetry = 5;
        private const string MsgLeadTimeWarning = "Invalid Lead Time, default to Quick_30";
        #endregion

        public ElrsClient(int clientId)
        {
            //_environment = environment;
            _clientId = clientId;
            Console.WriteLine("Initiate Logger" );
            _logger = new NLogger.ASyncLogger(clientId, _environment);            
            GetElrsConfig(clientId);
        }

        #region "public methods"
        /// <summary>
        /// call the api to poll events
        /// </summary>
        /// <returns></returns>
        public EventsCollection PollEvents()
        {
            EventsCollection eventsCol = null;
            if (_configSetUp)
            {
                if (_config.Simulation)
                {
                    var simulate = false;
                    var currentTime = DateTime.Now;
                    switch (_config.SimulationInterval)
                    {
                        case 5:
                            if (currentTime.Minute % 5 == 0)
                                simulate = true;
                            break;
                        case 15:
                            if (currentTime.Minute % 15 == 0)
                                simulate = true;
                            break;
                        case 30:
                            if (currentTime.Minute % 30 == 0)
                                simulate = true;
                            break;
                        case 60:
                            if (currentTime.Minute % 60 == 0)
                                simulate = true;
                            break;
                        case 1440:
                            if (currentTime.TimeOfDay.Ticks == 0)
                                simulate = true;
                            break;
                    }
                    // simulate poll pjm events
                    eventsCol = Simulate(simulate ? GetEventsResponseString() : GetNoEventResponseString());
                }
                else
                {
                    // poll pjm events
                    eventsCol = Poll(0);
                }
            }
            return eventsCol;
        }
        /// <summary>
        /// call api to acknowledge events
        /// </summary>
        /// <param name="events"></param>
        /// <param name="retry"></param>
        public void AcknowledgeEvents(List<EventInfo> events, int retry)
        {
            if (_configSetUp)
            {
                var client = new ElrsServiceReference.HTTPEndPointBindingClient();
                try
                {
                    Console.WriteLine("{0} {1}", MsgAckEvents, EventBegin);
                    _logger.Info(CatagoryAck, SeverityLow, PjmNoticationEvent, string.Format("{0} {1}", MsgAckEvents, EventBegin));

                    client = GetElrsWebServiceClient();

                    // create payload events and convert into xml doc
                    var acknowledgementPayloadStr = GetAcknowledgementPayloadString(events);
                    var xmlDoc = new XmlDocument();
                    xmlDoc.LoadXml(acknowledgementPayloadStr);

                    // create request message for acknowledgement
                    var request = new ElrsServiceReference.LRSTransactions
                    {
                        TransactionRequest = new ElrsServiceReference.RequestMessageType
                        {
                            Header = new ElrsServiceReference.HeaderType
                            {
                                Verb = HeaderVerbChange,
                                Noun = HeaderNounEvent,
                                Revision = HeaderRevision,
                                Source = HeaderSource
                            }
                        }
                    };
                    request.TransactionRequest.Request = new ElrsServiceReference.RequestType
                    {
                        Option = RequestOptionAck
                    };
                    request.TransactionRequest.Payload = new ElrsServiceReference.PayloadType
                    {
                        Any = new[] {xmlDoc.DocumentElement},
                        format = PayloadFormat
                    };

                    var reqString = new StringBuilder();
                    var serializer = new XmlSerializer(request.GetType());
                    serializer.Serialize(new StringWriter(reqString), request);
                    // log the request info
                    _logger.Info(CatagoryAck, SeverityLow, PjmNoticationEvent, reqString.ToString(), string.Empty, MsgRequest);

                    Console.WriteLine("Send Request");
                    var response = client.LRSTransactions(request.TransactionRequest);
                    Console.WriteLine("Response Received");

                    var respString = new StringBuilder();
                    var respSerializer = new XmlSerializer(response.GetType());
                    respSerializer.Serialize(new StringWriter(respString), response);
                    //var response = client.LRSTransactions(request);
                    if (response.Reply.ReplyCode == "warning")
                    {
                        Console.WriteLine(response.Reply.Error[0]);
                        _logger.Info(CatagoryAck, SeverityMedium, PjmNoticationEvent, reqString.ToString(), respString.ToString(), response.Reply.Error[0]);
                    }
                    else if (response.Reply.ReplyCode == "fatal")
                    {
                        _logger.Error(CatagoryAck, SeverityHigh, PjmNoticationEvent, reqString.ToString(), respString.ToString(), string.Format("Response fatal - {0}", response.Reply.Error[0]));
                        throw new Exception(string.Format("Response fatal - {0}", response.Reply.Error[0]));
                    }
                    else
                    {
                        //log the response info
                        Console.WriteLine(respString.ToString());
                        _logger.Info(CatagoryAck, SeverityLow, PjmNoticationEvent, string.Empty, respString.ToString(), MsgResponse);
                    }
                    _logger.Info(CatagoryAck, SeverityLow, PjmNoticationEvent, string.Format("{0} {1}", MsgAckEvents, EventEnd));
                    Console.WriteLine("{0} {1}", MsgAckEvents, EventEnd);

                }
                catch (Exception ex)
                {
                    _logger.ErrorException(CatagoryAck, SeverityHigh, PjmNoticationEvent, ex);
                    if (retry < _config.MaxRetry)
                    {
                        retry++;
                        _logger.Info(CatagoryAck, SeverityLow, PjmNoticationEvent, string.Format("{0} {1}", MsgAckEvents, EventEnd));
                        Console.WriteLine("{0} {1}", MsgAckEvents, EventEnd);
                        Task.Delay(_config.RetryWaitDuration);
                        _logger.Info(CatagoryAck, SeverityMedium, PjmNoticationEvent, string.Format("Retry Attempt: {0}", retry));
                        Console.WriteLine("Retry Attempt: {0}", retry);
                        AcknowledgeEvents(events, retry);
                    }
                    else
                    {
                        _logger.Info(CatagoryAck, SeverityLow, PjmNoticationEvent, string.Format("{0} {1}", MsgAckEvents, EventEnd));
                        Console.WriteLine("{0} {1}", MsgAckEvents, EventEnd);
                        _logger.EmailErrorException(CatagoryAck, SeverityHigh, PjmNoticationEvent, ex);
                        throw;
                    }


                    //_logger.Error(catagoryAck, string.Empty, string.Empty, ex.Message);
                }
                finally
                {
                    client.Close();
                }
            }
        }

        /// <summary>
        /// for standalone ELRS webservice pinging
        /// </summary>
        public void PingElrs(int retry)
        {
            var client = new ElrsServiceReference.HTTPEndPointBindingClient();
            try
            {
                Console.WriteLine("{0} {1}", MsgPingEvents, EventBegin);
                _logger.Info(CatagoryPingPoll, SeverityLow, PjmNoticationEvent, string.Format("{0} {1}", MsgPingEvents, EventBegin));
                client = GetElrsWebServiceClient();

                var request = new ElrsServiceReference.LRSInformation
                {
                    InformationRequest = new ElrsServiceReference.RequestMessageType
                    {
                        Header = new ElrsServiceReference.HeaderType
                        {
                            Verb = HeaderVerbGet,
                            Noun = HeaderNounEvent,
                            Revision = HeaderRevision,
                            Source = HeaderSource
                        }
                    }
                };


                request.InformationRequest.Request = new ElrsServiceReference.RequestType {Option = RequestOptionUnAck};

                Console.WriteLine("Send Request");
                client.LRSInformation(request.InformationRequest);
                Console.WriteLine("Response Received");
                _logger.Info(CatagoryPingPoll, SeverityLow, PjmNoticationEvent, string.Format("{0} Succeeded", MsgPingEvents));
                Console.WriteLine("{0} Succeeded", MsgPingEvents);

                _logger.Info(CatagoryPingPoll, SeverityLow, PjmNoticationEvent, string.Format("{0} {1}", MsgPingEvents, EventEnd));
                Console.WriteLine("{0} {1}", MsgPingEvents, EventEnd);
            }
            catch (Exception ex)
            {
                _logger.ErrorException(CatagoryPingPoll, SeverityMedium, PjmNoticationEvent, ex);
                if (retry < _config.MaxRetry)
                {
                    retry++;
                    _logger.Info(CatagoryPingPoll, SeverityLow, PjmNoticationEvent, string.Format("{0} {1}", MsgPingEvents, EventEnd));
                    Console.WriteLine("{0} {1}", MsgPingEvents, EventEnd);
                    Task.Delay(_config.RetryWaitDuration);
                    _logger.Info(CatagoryPingPoll, SeverityMedium, PjmNoticationEvent, string.Format("Retry Attempt: {0}", retry));
                    Console.WriteLine("Retry Attempt: {0}", retry);
                    PingElrs(retry);
                }
                else
                {
                    _logger.Info(CatagoryPingPoll, SeverityLow, PjmNoticationEvent, string.Format("{0} {1}", MsgPingEvents, EventEnd));
                    Console.WriteLine("{0} {1}", MsgPingEvents, EventEnd);
                    _logger.EmailErrorException(CatagoryPingPoll, SeverityMedium, PjmNoticationEvent, ex);
                    throw;
                }
            }
            finally
            {
                client.Close();
            }
        }
        #endregion

        #region "private methods"
        private EventsCollection Poll(int retry)
        {
            var client = new ElrsServiceReference.HTTPEndPointBindingClient();
            EventsCollection events;
            try
            {
                Console.WriteLine("{0} {1}", MsgPollEvents, EventBegin);
                _logger.Info(CatagoryPoll, SeverityLow, PjmNoticationEvent, string.Format("{0} {1}", MsgPollEvents, EventBegin));
                client = GetElrsWebServiceClient();

                var request = new ElrsServiceReference.LRSInformation
                {
                    InformationRequest = new ElrsServiceReference.RequestMessageType
                    {
                        Header = new ElrsServiceReference.HeaderType
                        {
                            Verb = HeaderVerbGet,
                            Noun = HeaderNounEvent,
                            Revision = HeaderRevision,
                            Source = HeaderSource
                        }
                    }
                };


                request.InformationRequest.Request = new ElrsServiceReference.RequestType {Option = RequestOptionUnAck};

                Console.WriteLine("Send Request");
                var response = client.LRSInformation(request.InformationRequest);
                Console.WriteLine("Response Received");

                events = ProcessResponse(request, response);
            }
            catch (Exception ex)
            {

                if (retry < _config.MaxRetry)
                {
                    retry++;
                    Console.WriteLine("{0} {1}", MsgPollEvents, EventEnd);
                    _logger.Info(CatagoryPoll, SeverityLow, PjmNoticationEvent, string.Format("{0} {1}", MsgPollEvents, EventEnd));
                    Task.Delay(_config.RetryWaitDuration);
                    Console.WriteLine("Retry Attempt: {0}", retry);
                    _logger.Info(CatagoryPoll, SeverityMedium, PjmNoticationEvent, string.Format("Retry Attempt: {0}", retry));
                    events = Poll(retry);

                }
                else
                {
                    var currentTime = TimeZoneInfo.ConvertTimeBySystemTimeZoneId(DateTime.UtcNow, "Eastern Standard Time");

                    if (currentTime.TimeOfDay >= Convert.ToDateTime("10:00").TimeOfDay && currentTime.TimeOfDay <= Convert.ToDateTime("20:30").TimeOfDay)
                    {
                        _logger.ErrorException(CatagoryPoll, SeverityHigh, PjmNoticationEvent, ex);
                        _logger.EmailErrorException(CatagoryPoll, SeverityHigh, PjmNoticationEvent, ex);
                    }
                    else
                    {
                        _logger.ErrorException(CatagoryPoll, SeverityLow, PjmNoticationEvent, ex);
                        _logger.EmailErrorException(CatagoryPoll, SeverityLow, PjmNoticationEvent, ex);
                    }

                    //_logger.Error(catagoryPoll, string.Empty, string.Empty, ex.Message);
                    _logger.Info(CatagoryPoll, SeverityLow, PjmNoticationEvent, string.Format("{0} {1}", MsgPollEvents, EventEnd));
                    Console.WriteLine("{0} {1}", MsgPollEvents, EventEnd);
                    throw;
                }


            }
            finally
            {
                client.Close();
            }

            return events;
        }

        private void GetElrsConfig(int clientId)
        {
            var cacheKey = string.Empty;
            Console.WriteLine("Initiate content model factory contentful");
            var cf = new ContentModelFactoryContentful();
            Console.WriteLine("Create ContentCacheProvider");
            var contentCashProvider = ContentCacheProviderFactory.CreateContentCacheProvider();

            try
            {
                Console.WriteLine("Create/Retrieve ELRS Config Begins");
                var contentRepository = cf.CreateContentRepository(clientId);
                Console.WriteLine("Get Content Eviornment");
                _environment = contentRepository.GetContentEnvironment();
                _logger.Envirnoment = _environment;
                Console.WriteLine("Create cache Key");
                cacheKey = string.Format("ElrsConfig_{0}_{1}", clientId, _environment);
                Console.WriteLine("Get cached config");
                _config = (ElrsConfig)contentCashProvider.Get(cacheKey);
            }
            catch
            {
                _config = null;
            }
            if (_config != null)
            {
                _configSetUp = true;
                Console.WriteLine("Create/Retrieve ELRS Config Ends");
                return;
            }

            try
            {
                Console.WriteLine("No cache, create new config");
                _config = new ElrsConfig();
                Console.WriteLine("Create ContentProvider");
                var cp = cf.CreateContentProvider(clientId);
                var configurationList = cp.GetContentConfiguration(ContentCategory.Notification.ToString().ToLower(), string.Empty);
                if (configurationList.Any())
                {
                    _config.WebServiceEndPoint = configurationList.Find(item => item.Key == "webserviceclient.elrs.endpoint").Value;
                    _config.Username = configurationList.Find(item => item.Key == "webserviceclient.elrs.username").Value;
                    _config.Password = configurationList.Find(item => item.Key == "webserviceclient.elrs.password").Value;
                    _config.EventTypes = configurationList.Find(item => item.Key == "webserviceclient.elrs.eventtypes").Value
                            .Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries).ToList();

                    _config.MaxRetry = Convert.ToInt32(configurationList.Find(item => item.Key == "webserviceclient.global.maxretry").Value);
                    if (_config.MaxRetry > DefaultMaxRetry) _config.MaxRetry = DefaultMaxRetry;
                    _config.RetryWaitDuration = Convert.ToInt32(configurationList.Find(item => item.Key == "webserviceclient.global.retrywaitduration").Value);
                    _config.Simulation = Convert.ToBoolean(configurationList.Find(item => item.Key == "webserviceclient.elrs.enablesimulation").Value);
                    _config.SimulationInterval = Convert.ToInt32(configurationList.Find(item => item.Key == "webserviceclient.elrs.simulationeventinterval").Value);
                }

                _configSetUp = true;

                contentCashProvider.Set(cacheKey, _config, DateTime.Now.AddMinutes(30));
                Console.WriteLine("Create/Retrieve ELRS Config Ends");
            }
            catch (Exception ex)
            {
                _logger.ErrorException(CatagoryPoll, SeverityHigh, PjmNoticationEvent, ex);
                _logger.EmailErrorException(CatagoryPoll, SeverityHigh, PjmNoticationEvent, ex);
                throw;
            }


        }
        private EventsCollection ProcessResponse(ElrsServiceReference.LRSInformation request, ElrsServiceReference.ResponseMessage response)
        {
            EventsCollection eventsCol = null;

            var reqString = new StringBuilder();
            var respString = new StringBuilder();

            Console.WriteLine("{0} {1}", MsgProcessResponse, EventBegin);
            _logger.Info(CatagoryPoll, SeverityLow, PjmNoticationEvent, string.Format("{0} {1}", MsgProcessResponse, EventBegin));

            if (request != null)
            {
                var serializer = new XmlSerializer(request.GetType());
                serializer.Serialize(new StringWriter(reqString), request);
            }

            var respSerializer = new XmlSerializer(response.GetType());
            respSerializer.Serialize(new StringWriter(respString), response);


            if (response.Reply.ReplyCode == ReplyCodeOkay)
            {
                _logger.Info(CatagoryPoll, SeverityLow, PjmNoticationEvent, reqString.ToString(), respString.ToString(), string.Empty);
                eventsCol = ProcessEvents(response.Payload);
            }
            else if (response.Reply.ReplyCode == ReplyCodeWarning)
            {
                var noEvents = false;

                if (response.Reply.Error.Length > 0)
                {
                    var errorCode = response.Reply.Error[0].Substring(0, 4);

                    if (errorCode == ErrorCode)
                        noEvents = true;
                }

                if (!noEvents)
                {
                    Console.WriteLine(response.Reply.Error[0]);
                    _logger.Info(CatagoryPoll, SeverityMedium, PjmNoticationEvent, reqString.ToString(), respString.ToString(), response.Reply.Error[0]);
                }
                else
                {
                    Console.WriteLine(MsgNoEvent);
                    _logger.Info(CatagoryPoll, SeverityMedium, PjmNoticationEvent, reqString.ToString(), respString.ToString(), MsgNoEvent);
                }
            }
            else if (response.Reply.ReplyCode == ReplyCodeFatal)
            {
                _logger.EmailError(CatagoryPoll, SeverityHigh, PjmNoticationEvent, reqString.ToString(), respString.ToString(), response.Reply.Error[0]);
                _logger.Error(CatagoryPoll, SeverityHigh, PjmNoticationEvent, reqString.ToString(), respString.ToString(), response.Reply.Error[0]);
            }

            _logger.Info(CatagoryPoll, SeverityLow, PjmNoticationEvent, string.Format("{0} {1}", MsgProcessResponse, EventEnd));

            Console.WriteLine("{0} {1}", MsgProcessResponse, EventEnd);
            return eventsCol;
        }

        private EventsCollection Simulate(string serializedResponse)
        {
            try
            {
                Console.WriteLine("{0} {1}", MsgSimulateEvents, EventBegin);
                _logger.Info(CatagoryPoll, SeverityLow, PjmNoticationEvent, string.Format("{0} {1}", MsgSimulateEvents, EventBegin));
                var mySerializer = new XmlSerializer(typeof(ElrsServiceReference.ResponseMessage));

                var reader = new StringReader(serializedResponse);
                var response = (ElrsServiceReference.ResponseMessage)mySerializer.Deserialize(reader);

                var events = ProcessResponse(null, response);

                _logger.Info(CatagoryPoll, SeverityLow, PjmNoticationEvent, string.Format("{0} {1}", MsgSimulateEvents, EventEnd));
                Console.WriteLine("{0} {1}", MsgSimulateEvents, EventEnd);

                return events;
            }
            catch (Exception ex)
            {
                _logger.ErrorException(CatagoryPoll, SeverityHigh, PjmNoticationEvent, ex);
                _logger.Info(CatagoryPoll, SeverityLow, PjmNoticationEvent, string.Format("{0} {1}", MsgSimulateEvents, EventEnd));
                _logger.EmailErrorException(CatagoryPoll, SeverityHigh, PjmNoticationEvent, ex);
                throw;
            }
        }


        private EventsCollection ProcessEvents(ElrsServiceReference.PayloadType payload)
        {
            Console.WriteLine("{0} {1}", MsgProcessEvents, EventBegin);
            _logger.Info(CatagoryPoll, SeverityLow, PjmNoticationEvent, string.Format("{0} {1}", MsgProcessEvents, EventBegin));

            // pull the team name mapping list from configuration
            var teamNames = GetTeamNameMappingList();

            var response = new EventsCollection {Xml = payload.Any[0].OuterXml};


            XNamespace ns = "http://www.pjm.com/schema/drbiznet/2009/model";

            var eventsDoc = XDocument.Load(new StringReader(response.Xml));

            (from x in eventsDoc.Descendants(ns + "Event")
                let firstOrDefault = x.Descendants(ns + "summaryInfo").FirstOrDefault()
                let xElement = firstOrDefault.Element(ns + "eventType")
                where xElement != null && (firstOrDefault != null && _config.EventTypes.Contains(xElement.Value))
             select x).ToList().ForEach(x =>
             {
                 string eventId = string.Empty;
                 var xId = x.Element(ns + "id");
                 if (xId != null) eventId = xId.Value;
                 string type = String.Empty;
                 string target = string.Empty;
                 string status = string.Empty;
                 var startTime = DateTime.MinValue;
                 var notifyTime = DateTime.MinValue;
                 var endTime = DateTime.MinValue;
                 var startUtcTime = DateTime.MinValue;
                 var notifyUtcTime = DateTime.MinValue;
                 var endUtcTime = DateTime.MinValue;
                 var sleadTime = string.Empty;
                 var xSummaryInfo = x.Descendants(ns + "summaryInfo").FirstOrDefault();
                 if (xSummaryInfo != null)
                 {
                     var xEventType = xSummaryInfo.Element(ns + "eventType");
                     if (xEventType != null) type = xEventType.Value;
                     var xTarget = xSummaryInfo.Element(ns + "target");
                     if (xTarget != null) target = xTarget.Value;
                     var xStatus = xSummaryInfo.Element(ns + "status");
                     if (xStatus != null) status = xStatus.Value;
                     var xStartTime = xSummaryInfo.Element(ns + "startTime");
                     if (xStartTime != null)
                     {
                         startUtcTime = Convert.ToDateTime(xStartTime.Value).ToUniversalTime();
                         startTime = DateTimeOffset.Parse(xStartTime.Value).DateTime;
                     }
                     var xNotifyTime = xSummaryInfo.Element(ns + "notifyTime");
                     if (xNotifyTime != null)
                     {
                         notifyUtcTime = Convert.ToDateTime(xNotifyTime.Value).ToUniversalTime();
                         notifyTime = DateTimeOffset.Parse(xNotifyTime.Value).DateTime;
                     }
                     var xEndTime = xSummaryInfo.Element(ns + "endTime");
                     if (xEndTime != null)
                     {
                         endUtcTime = Convert.ToDateTime(xEndTime.Value).ToUniversalTime();
                         endTime = DateTimeOffset.Parse(xEndTime.Value).DateTime;
                     }
                     var xRegLeadTime = xSummaryInfo.Element(ns + "regLeadTime");
                     if (xRegLeadTime != null) sleadTime = xRegLeadTime.Value;
                 }
                 LeadTimeType leadTime;
                 switch (sleadTime.ToLower())
                 {
                     case "long_120":
                         leadTime = LeadTimeType.Long120;
                         break;
                     case "short_60":
                         leadTime = LeadTimeType.Short60;
                         break;
                     case "quick_30":
                         leadTime = LeadTimeType.Quick30;
                         break;
                     default:
                         leadTime = LeadTimeType.Quick30;
                         _logger.Error(CatagoryPoll, SeverityMedium, PjmNoticationEvent, MsgLeadTimeWarning);
                         break;
                 }

                 var regInfoList = new List<RegistrationInfo>();

                 x.Descendants(ns + "registrationInfo").Select(r => r).ToList().ForEach(r =>
                 {
                     var xRegistrationId = r.Element(ns + "registrationId");
                     if (xRegistrationId != null)
                     {
                         var registrationId = xRegistrationId.Value;
                         var registractionZone = string.Empty;
                         var xZone = r.Element(ns + "zone");
                         if (xZone != null)
                         {
                             registractionZone = xZone.Value;
                             
                         }
                         regInfoList.Add(new RegistrationInfo { RegistrationId = registrationId, Zone = registractionZone });
                     }
                 });
                 var eventType = EventType.None;
                 switch (type.ToLower())
                 {
                     case "load management":
                         eventType = EventType.LoadManagement;
                         break;
                     case "real-time dispatch":
                         eventType = EventType.RealTimeDispatch;
                         break;
                     case "synchronized reserved":
                         eventType = EventType.SynchronizedReserved;
                         break;
                     case "zonal test":
                         eventType = EventType.ZonalTest;
                         break;
                     case "zonal retest":
                         eventType = EventType.ZonalRetest;
                         break;
                 }
                 var eventStatus = EventStatus.None;
                 switch (status.ToLower())
                 {
                     case "scheduled":
                         eventStatus = EventStatus.Scheduled;
                         break;
                     case "active":
                         eventStatus = EventStatus.Active;
                         break;
                     case "completed":
                         eventStatus = EventStatus.Completed;
                         break;
                     case "canceled":
                     case "cancelled":
                         eventStatus = EventStatus.Canceled;
                         break;
                 }


                 var regIds = string.Empty;
                 regInfoList.ForEach(r =>
                 {
                     if (regIds != string.Empty)
                         regIds += ",";
                     regIds += r.RegistrationId;
                 });

                 _logger.Info(CatagoryPoll, SeverityLow, PjmNoticationEvent, MsgEventsReceived, x.ToString());
                 Console.WriteLine(x.ToString());

                 // get team names based on the registration id
                 GetTeamName(regInfoList, teamNames);

                 // remove all the ones with empty team name
                 regInfoList.RemoveAll(t => string.IsNullOrEmpty(t.TeamName));

                 response.Events.Add(new EventInfo
                 {
                     EndTime = endTime,
                     EndUtcTime = endUtcTime,
                     EventId = eventId,
                     EventType = eventType,
                     LeadTime = leadTime,
                     NotifyTime = notifyTime,
                     NotifyUtcTime = notifyUtcTime,
                     RegistrationInfoList = regInfoList,
                     StartTime = startTime,
                     StartUtcTime = startUtcTime,
                     Status = eventStatus,
                     Target = target,
                     EventXml = x.ToString()
                 });
             });

            _logger.Info(CatagoryPoll, SeverityLow, PjmNoticationEvent, string.Format("{0} {1}", MsgProcessEvents, EventEnd));
            Console.WriteLine("{0} {1}", MsgProcessEvents, EventEnd);
            return response;
        }

        /// <summary>
        ///  get team name based on registration id
        /// </summary>
        /// <param name="regInfoList"></param>
        /// <param name="teamNames"></param>
        /// <returns></returns>
        private void GetTeamName(List<RegistrationInfo> regInfoList, List<TeamNameMapping> teamNames)
        {
            regInfoList.ForEach(r =>
            {
                var team = teamNames.Find(t => t.RegistrationId == r.RegistrationId);

                if (team != null)
                {
                    r.TeamName = team.TeamName;
                    r.IsHoneywellDlc = team.IsHoneywellDlc;
                }
                else
                {
                    _logger.Error(CatagoryPoll, SeverityHigh, PjmNoticationEvent, string.Format("{0} {1}", MsgNoTeamNameMapping, r.RegistrationId));
                    _logger.EmailError(CatagoryPoll, SeverityHigh, PjmNoticationEvent, string.Format("{0} {1}", MsgNoTeamNameMapping, r.RegistrationId));
                }
            });
        }

        private ElrsServiceReference.HTTPEndPointBindingClient GetElrsWebServiceClient()
        {
            var client = new ElrsServiceReference.HTTPEndPointBindingClient("HTTPEndPointBindingImplPort", _config.WebServiceEndPoint);

            if (client.ClientCredentials != null)
            {
                client.ClientCredentials.UserName.UserName = _config.Username;
                client.ClientCredentials.UserName.Password = _config.Password;
            }


            return client;
        }

        /// <summary>
        /// retrieve Tame Name Mapping list
        /// </summary>
        /// <returns></returns>
        private List<TeamNameMapping> GetTeamNameMappingList()
        {
            var teamNames = new List<TeamNameMapping>();

            var mappingStr = GetTeamNameMappingXml();
            var teamNameDoc = XDocument.Load(new StringReader(mappingStr));

            var teamNameMapping = teamNameDoc.Descendants("TeamNameMapping").Select(x => x).ToList();

            teamNameMapping.ForEach(tm =>
            {
                tm.Descendants("MapInfo").Select(m => m).ToList().ForEach(t =>
                {
                    var honeywellDlc = false;
                    if (t.Attribute("HoneywellDLC") != null)
                    {
                        honeywellDlc = Convert.ToBoolean(t.Attribute("HoneywellDLC").Value);
                    }
                    teamNames.Add(new TeamNameMapping
                    {
                        RegistrationId = t.Attribute("key").Value,
                        IsHoneywellDlc = honeywellDlc,
                        TeamName = t.Value.Replace("&amp;", "&")
                    });
                });
            });

            return teamNames;
        }

        /// <summary>
        /// retreive team name mapping xml from database
        /// </summary>
        /// <returns></returns>
        private string GetTeamNameMappingXml()
        {
            string mappingStr;
            var connectionStr = ConfigurationManager.ConnectionStrings["InsightsMetaDataConn"].ConnectionString;

            using (var sqlConnection = new SqlConnection(connectionStr))
            {
                var cmd = new SqlCommand
                {
                    CommandText = "[dbo].[GetComponentXmlConfiguration]",
                    CommandType = CommandType.StoredProcedure,
                    Connection = sqlConnection
                };


                cmd.Parameters.Add(new SqlParameter("@ClientId", SqlDbType.Int) { Value = _clientId });
                cmd.Parameters.Add(new SqlParameter("@ComponentName", SqlDbType.Text) { Value = "notification" });
                var param = cmd.CreateParameter();
                param.ParameterName = "@XmlConfiguration";
                param.Direction = ParameterDirection.Output;
                param.DbType = DbType.Xml;
                cmd.Parameters.Add(param);

                sqlConnection.Open();

                cmd.ExecuteNonQuery();

                mappingStr = param.Value.ToString();
            }

            return mappingStr;

        }

        /// <summary>
        /// set up the acknowledgement pay load
        /// </summary>
        /// <param name="events"></param>
        /// <returns></returns>
        private string GetAcknowledgementPayloadString(List<EventInfo> events)
        {
            var acknowledgementPayloadStr = @"<Events xmlns='http://www.pjm.com/schema/drbiznet/2009/model'>";

            foreach (var e in events)
            {
                acknowledgementPayloadStr += "<Event><id>" + e.EventId + "</id>";
                double amount;
                if (double.TryParse(e.Target, out amount))
                    acknowledgementPayloadStr += "<summaryInfo><ackAmount>" + e.Target + "</ackAmount></summaryInfo>";

                acknowledgementPayloadStr += "</Event>";
            }

            acknowledgementPayloadStr += @"</Events>";

            return acknowledgementPayloadStr;
        }


        private string GetNoEventResponseString()
        {
            var xmlDoc = new XmlDocument();
            xmlDoc.Load("Samples\\SimulateNoEvent.xml");
            var objectStr = xmlDoc.InnerXml;
            return objectStr;
        }

        private string GetEventsResponseString()
        {
            var xmlDoc = new XmlDocument();
            xmlDoc.Load("Samples\\SimulateEvent.xml");
            var objectStr = xmlDoc.InnerXml;

            return objectStr;
        }
        #endregion
    }

    [Serializable]
    public class ElrsConfig
    {
        public string Username { get; set; }
        public string Password { get; set; }

        public string WebServiceEndPoint { get; set; }
        public List<string> EventTypes { get; set; }
        public int MaxRetry { get; set; }
        public int RetryWaitDuration { get; set; }
        public bool Simulation { get; set; }
        public int SimulationInterval { get; set; }
    }

    [Serializable]
    public class TeamNameMapping
    {
        public string RegistrationId { get; set; }
        public string TeamName { get; set; }
        public bool IsHoneywellDlc { get; set; }
    }
}
