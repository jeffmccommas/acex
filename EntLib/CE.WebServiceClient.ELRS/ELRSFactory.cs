﻿namespace CE.WebServiceClient.ELRS
{
    public class ElrsFactory
    {

        public IElrs CreateElrs(int clientId)
        {
            var elrs = new ElrsClient(clientId);

            return elrs;
        }

    }
}
