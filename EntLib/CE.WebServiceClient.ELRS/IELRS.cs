﻿using CE.NotificationManager.Entities;
using System.Collections.Generic;

namespace CE.WebServiceClient.ELRS
{
    public interface IElrs
    {
        EventsCollection PollEvents();

        void AcknowledgeEvents(List<EventInfo> events, int retry);

        void PingElrs(int retry);
    }
}
