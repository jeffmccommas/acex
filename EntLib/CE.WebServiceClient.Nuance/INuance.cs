﻿using CE.NotificationManager.Entities;

namespace CE.WebServiceClient.Nuance
{
    public interface INuance
    {
        void SendMessage(EventsCollection eventList, int retry);

        void PingNuance(int retry);
    }
}
