﻿using CE.ContentModel;
using CE.ContentModel.ContentCache;
using CE.NLogger;
using CE.NotificationManager.Entities;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace CE.WebServiceClient.Nuance
{
    public class NuanceClient : INuance
    {
        #region "private const"
        private const string PjmNoticationEvent = "PJM Notification Event";
        private const string SeverityHigh = "High";
        private const string SeverityMedium = "Medium";
        private const string SeverityLow = "Low";
        private const string CatagoryEmail = "Email";
        private const string CatagoryPingEmail = "Ping Email";
        internal const string CatagoryAck = "Acknowledge";
        private const string EventBegin = "Begins";
        private const string EventEnd = "Ends";
        private const string MsgSendMessage = "Send Message";
        private const string MsgPingNuance = "Ping Nuance";
        private const string MsgNoTeam = "No Team return for RegistrationID/TeamName";
        private const string MsgNoMemeber = "No Member return for username";
        private const string MsgEventSent = "Event Created Successfully";
        private const int DefaultMaxRetry = 5;
        #endregion

        #region "private variables"
        private string _environment;
        private NuanceConfig _config;
        private readonly ASyncLogger _logger;
        private bool _configSetup;
        #endregion

        public NuanceClient(int clientId)
        {
            _logger = new ASyncLogger(clientId, _environment);
            GetNuanceConfigAndContent(clientId);
        }

        #region "public methods"

        /// <summary>
        /// call api to send message
        /// </summary>
        /// <param name="eventList"></param>
        /// <param name="retry"></param>
        public void SendMessage(EventsCollection eventList, int retry)
        {
            if (_configSetup)
            {
                var client = new NuanceServiceReference.EPAPIPortTypeClient();
                try
                {
                    Console.WriteLine("{0} {1}", MsgSendMessage, EventBegin);
                    _logger.Info(CatagoryEmail, SeverityLow, PjmNoticationEvent, string.Format("{0} {1}", MsgSendMessage, EventBegin));
                    //_logger.Info(1, catagoryEmail, severityLow, pjmNoticationEvent, string.Format("{0} {1}", msgSendMessage, eventBegin), string.Empty, _environment);
                    client = new NuanceServiceReference.EPAPIPortTypeClient("EPAPI", _config.WebServiceEndPoint);
                    var header = new NuanceServiceReference.AuthHeader
                    {
                        Domain = _config.Domain,
                        UserId = _config.Username,
                        UserPassword = _config.Password,
                        OemId = _config.OemId,
                        OemPassword = _config.OemPassword
                    };
                    var retryEventList = new EventsCollection();

                    foreach (var pjmEvent in eventList.Events)
                    {
                        try
                        {
                            if (pjmEvent.EventType == EventType.LoadManagement)
                            {
                                // get members based on the username
                                var username = new[] { _config.Username };
                                Console.WriteLine("Send Request for Memeber");

                                var memebers = client.MemberQueryByUsername(header, username);
                                Console.WriteLine("Response Received");

                                if (memebers != null && memebers.Any())
                                {
                                    // external message for non honeywell teams
                                    var numTeams = pjmEvent.RegistrationInfoList.Count() + _config.AclaraSupportTeams.Count();

                                    var teamNames = new string[numTeams];
                                    int teamCount = 0;
                                    foreach (var regInfo in pjmEvent.RegistrationInfoList)
                                    {
                                        // Get the teams based on the name in the array.
                                        if (!regInfo.IsHoneywellDlc)
                                        {
                                            teamNames[teamCount] = regInfo.TeamName;

                                            teamCount++;
                                        }
                                    }

                                    if (teamCount > 0)
                                    {
                                        // add aclara support team
                                        Array.Copy(_config.AclaraSupportTeams, 0, teamNames, pjmEvent.RegistrationInfoList.Count(), _config.AclaraSupportTeams.Length);

                                        Console.WriteLine("Send Request for Team");
                                        var teams = client.TeamQueryByName(header, teamNames);
                                        Console.WriteLine("Response Received");

                                        // send external message
                                        SendMessageMain(client, header, teams, memebers[0].MemberId, pjmEvent, true, false);
                                    }

                                    // if honeywell DLS Teams exist, send honeywell specific external message
                                    var honeywellRegInfoList = pjmEvent.RegistrationInfoList.FindAll(r => r.IsHoneywellDlc);

                                    if (honeywellRegInfoList.Count > 0)
                                    {
                                        var honeywellTeamCount = 0;
                                        var honeywellTeamNames = new string[honeywellRegInfoList.Count() + _config.AclaraSupportTeams.Count()];
                                        foreach (var regInfo in honeywellRegInfoList)
                                        {
                                            honeywellTeamNames[honeywellTeamCount] = regInfo.TeamName;
                                            honeywellTeamCount++;
                                        }
                                        // add aclara support team
                                        Array.Copy(_config.AclaraSupportTeams, 0, honeywellTeamNames, honeywellRegInfoList.Count(), _config.AclaraSupportTeams.Length);
                                        var honeyWellTeams = client.TeamQueryByName(header, honeywellTeamNames);

                                        SendMessageMain(client, header, honeyWellTeams, memebers[0].MemberId, pjmEvent, true, true);
                                    }


                                    // send internal message; exclude honeywell
                                    if (teamCount > 0)
                                    {
                                        var internalTeamList = _config.InternalTeamNames.ToArray();
                                        Array.Resize(ref internalTeamList, internalTeamList.Length + _config.AclaraSupportTeams.Length);
                                        Array.Copy(_config.AclaraSupportTeams, 0, internalTeamList, _config.InternalTeamNames.Count(), _config.AclaraSupportTeams.Length);
                                        var internalTeams = client.TeamQueryByName(header, internalTeamList);

                                        SendMessageMain(client, header, internalTeams, memebers[0].MemberId, pjmEvent, false, false);
                                    }

                                }
                                else
                                {
                                    // log no member return based on username
                                    _logger.Error(CatagoryEmail, SeverityMedium, PjmNoticationEvent, string.Format("{0} {1}", MsgNoMemeber, username[0]));
                                }
                            }


                            _logger.Info(CatagoryEmail, SeverityLow, PjmNoticationEvent, string.Format("{0} {1}", MsgSendMessage, EventEnd));
                            Console.WriteLine("{0} {1}", MsgSendMessage, EventEnd);
                        }
                        catch (Exception ex)
                        {
                            _logger.EmailErrorException(CatagoryEmail, SeverityHigh, PjmNoticationEvent, ex);
                            // if it's within max retry configuration, then log the retry attempt, and attempt send message again
                            // otherwise, log the exception and throw
                            if (retry < _config.MaxRetry)
                            {
                                
                                Console.WriteLine("{0} {1}", MsgSendMessage, EventEnd);
                                _logger.Info(CatagoryEmail, SeverityLow, PjmNoticationEvent, string.Format("{0} {1}", MsgSendMessage, EventEnd));
                                Task.Delay(_config.RetryWaitDuration);
                                Console.WriteLine("Retry Attempt: {0}", retry);
                                _logger.Info(CatagoryEmail, SeverityMedium, PjmNoticationEvent, string.Format("Retry Attempt: {0}", retry));

                                
                                
                                retryEventList.Events.Add(pjmEvent);
                                

                            }
                            else
                            {
                                //_logger.Info(CatagoryEmail, SeverityLow, PjmNoticationEvent, string.Format("{0} {1}", MsgSendMessage, EventEnd));
                                //_logger.EmailErrorException(CatagoryEmail, SeverityHigh, PjmNoticationEvent, ex);
                                throw;
                            }
                        }
                    }
                    if(retryEventList.Events.Count > 0)
                    {
                        retry++;
                        SendMessage(retryEventList, retry);
                    }
                }
                catch (Exception ex)
                {
                    _logger.EmailErrorException(CatagoryEmail, SeverityHigh, PjmNoticationEvent, ex);
                    // if it's within max retry configuration, then log the retry attempt, and attempt send message again
                    // otherwise, log the exception and throw
                    if (retry < _config.MaxRetry)
                    {
                        retry++;
                        Console.WriteLine("{0} {1}", MsgSendMessage, EventEnd);
                        _logger.Info(CatagoryEmail, SeverityLow, PjmNoticationEvent, string.Format("{0} {1}", MsgSendMessage, EventEnd));
                        Task.Delay(_config.RetryWaitDuration);
                        Console.WriteLine("Retry Attempt: {0}", retry);
                        _logger.Info(CatagoryEmail, SeverityMedium, PjmNoticationEvent, string.Format("Retry Attempt: {0}", retry));
                        SendMessage(eventList, retry);

                    }
                    else
                    {
                        _logger.Info(CatagoryEmail, SeverityLow, PjmNoticationEvent, string.Format("{0} {1}", MsgSendMessage, EventEnd));
                        _logger.EmailErrorException(CatagoryEmail, SeverityHigh, PjmNoticationEvent, ex);
                        throw;
                    }

                }
                finally
                {
                    client.Close();
                }
            }
        }


        /// <summary>
        /// ping nuance client webservice
        /// </summary>
        public void PingNuance(int retry)
        {
            var client = new NuanceServiceReference.EPAPIPortTypeClient();
            try
            {
                Console.WriteLine("{0} {1}", MsgPingNuance, EventBegin);
                _logger.Info(CatagoryPingEmail, SeverityLow, PjmNoticationEvent, string.Format("{0} {1}", MsgPingNuance, EventBegin));
                client = new NuanceServiceReference.EPAPIPortTypeClient("EPAPI", _config.WebServiceEndPoint);
                var header = new NuanceServiceReference.AuthHeader
                {
                    Domain = _config.Domain,
                    UserId = _config.Username,
                    UserPassword = _config.Password,
                    OemId = _config.OemId,
                    OemPassword = _config.OemPassword
                };


                Console.WriteLine("Send Request");
                client.MemberQueryByUsername(header, new[] { _config.Username });
                Console.WriteLine("Response Received");

                _logger.Info(CatagoryPingEmail, SeverityLow, PjmNoticationEvent, string.Format("{0} Succeeded", MsgPingNuance));
                Console.WriteLine("{0} Succeeded", MsgPingNuance);
                _logger.Info(CatagoryPingEmail, SeverityLow, PjmNoticationEvent, string.Format("{0} {1}", MsgPingNuance, EventEnd));
                Console.WriteLine("{0} {1}", MsgPingNuance, EventEnd);
            }
            catch (Exception ex)
            {
                _logger.ErrorException(CatagoryPingEmail, SeverityMedium, PjmNoticationEvent, ex);
                if (retry < _config.MaxRetry)
                {
                    retry++;
                    _logger.Info(CatagoryPingEmail, SeverityLow, PjmNoticationEvent, string.Format("{0} {1}", MsgPingNuance, EventEnd));
                    Console.WriteLine("{0} {1}", MsgPingNuance, EventEnd);
                    Task.Delay(_config.RetryWaitDuration);
                    _logger.Info(CatagoryPingEmail, SeverityMedium, PjmNoticationEvent, string.Format("Retry Attempt: {0}", retry));
                    Console.WriteLine("Retry Attempt: {0}", retry);
                    PingNuance(retry);
                }
                else
                {
                    _logger.Info(CatagoryPingEmail, SeverityLow, PjmNoticationEvent, string.Format("{0} {1}", MsgPingNuance, EventEnd));
                    Console.WriteLine("{0} {1}", MsgPingNuance, EventEnd);
                    _logger.EmailErrorException(CatagoryPingEmail, SeverityMedium, PjmNoticationEvent, ex);
                    //throw;
                }
            }
            finally
            {
                client.Close();
            }

        }

        #endregion

        #region "private methods"
        private void GetNuanceConfigAndContent(int clientId)
        {
            var cacheKey = string.Empty;
            var contentCashProvider = ContentCacheProviderFactory.CreateContentCacheProvider();
            var cf = new ContentModelFactoryContentful();

            try
            {
                var contentRepository = cf.CreateContentRepository(clientId);
                _environment = contentRepository.GetContentEnvironment();
                _logger.Envirnoment = _environment;
                cacheKey = string.Format("NuanceConfig_{0}_{1}", clientId, _environment);
                _config = (NuanceConfig)contentCashProvider.Get(cacheKey);
            }
            catch
            {
                _config = null;
            }
            if (_config != null)
            {
                _configSetup = true;
                return;
            }

            try
            {
                _config = new NuanceConfig();
                var cp = cf.CreateContentProvider(clientId);
                // get Configuration
                Console.WriteLine("Get Configuration");
                var configurationList = cp.GetContentConfiguration(ContentCategory.Notification.ToString().ToLower(), string.Empty);
                if (configurationList.Any())
                {
                    _config.Domain = configurationList.Find(item => item.Key == "webserviceclient.nuance.domain").Value;
                    _config.Username = configurationList.Find(item => item.Key == "webserviceclient.nuance.username").Value;
                    _config.Password = configurationList.Find(item => item.Key == "webserviceclient.nuance.password").Value;
                    _config.OemId = configurationList.Find(item => item.Key == "webserviceclient.nuance.oemid").Value;
                    _config.OemPassword = configurationList.Find(item => item.Key == "webserviceclient.nuance.oempassword").Value;
                    _config.SenderEmail = configurationList.Find(item => item.Key == "webserviceclient.nuance.senderemail").Value;
                    _config.SenderCallerId = configurationList.Find(item => item.Key == "webserviceclient.nuance.sendercallerid").Value;
                    _config.SenderFax = configurationList.Find(item => item.Key == "webserviceclient.nuance.senderfax").Value;
                    _config.SenderNumericPager = configurationList.Find(item => item.Key == "webserviceclient.nuance.sendernumericpager").Value;
                    _config.EventTypeName = configurationList.Find(item => item.Key == "webserviceclient.nuance.eventtypename").Value;
                    _config.WebServiceEndPoint = configurationList.Find(item => item.Key == "webserviceclient.nuance.endpoint").Value;
                    _config.MaxRetry = Convert.ToInt32(configurationList.Find(item => item.Key == "webserviceclient.global.maxretry").Value);
                    if (_config.MaxRetry > DefaultMaxRetry) _config.MaxRetry = DefaultMaxRetry;
                    _config.RetryWaitDuration = Convert.ToInt32(configurationList.Find(item => item.Key == "webserviceclient.global.retrywaitduration").Value);
                    _config.InternalTeamNames = configurationList.Find(item => item.Key == "webserviceclient.nuance.internalteamnames").Value
                        .Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries).ToList();
                    _config.AclaraSupportTeams = configurationList.Find(item => item.Key == "webserviceclient.nuance.aclarasupportteams").Value
                        .Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries).ToArray();
                    _config.TeamSendOption = configurationList.Find(item => item.Key == "webserviceclient.nuance.teamsendingoptions").Value;
                    _config.PhoneGreeting = configurationList.Find(item => item.Key == "webserviceclient.nuance.usergreeting").Value;
                    _config.Message1ExternalEnabled = Convert.ToBoolean(configurationList.Find(item => item.Key == "webserviceclient.nuance.message1external.enabled").Value);
                    _config.Message1InternalEnabled = Convert.ToBoolean(configurationList.Find(item => item.Key == "webserviceclient.nuance.message1internal.enabled").Value);
                    _config.Message2ExternalEnabled = Convert.ToBoolean(configurationList.Find(item => item.Key == "webserviceclient.nuance.message2external.enabled").Value);
                    _config.Message2InternalEnabled = Convert.ToBoolean(configurationList.Find(item => item.Key == "webserviceclient.nuance.message2internal.enabled").Value);
                    _config.Message3ExternalEnabled = Convert.ToBoolean(configurationList.Find(item => item.Key == "webserviceclient.nuance.message3external.enabled").Value);
                    _config.Message3InternalEnabled = Convert.ToBoolean(configurationList.Find(item => item.Key == "webserviceclient.nuance.message3internal.enabled").Value);
                    _config.Message4ExternalEnabled = Convert.ToBoolean(configurationList.Find(item => item.Key == "webserviceclient.nuance.message4external.enabled").Value);
                    _config.Message5ExternalEnabled = Convert.ToBoolean(configurationList.Find(item => item.Key == "webserviceclient.nuance.message5external.enabled").Value); 
                }

                Console.WriteLine("Get Content");
                var contentList = cp.GetContentTextContent(ContentCategory.Notification.ToString().ToLower(),
                    ContentLocale.EnglishUnitedStates, string.Empty);
                if (contentList.Any())
                {
                    var val = contentList.Find(item => item.Key == "message.loadmanagement.internal1.subject");
                    if (val != null)
                    {
                        _config.Message1InternalSubject = val.ShortText;
                    }
                    val = contentList.Find(item => item.Key == "message.loadmanagement.internal1.body");
                    if (val != null)
                    {
                        _config.Message1InternalBody = val.MediumText;
                    }
                    val = contentList.Find(item => item.Key == "message.loadmanagement.external1.subject");
                    if (val != null)
                    {
                        _config.Message1ExternalSubject = val.ShortText;
                    }
                    val = contentList.Find(item => item.Key == "message.loadmanagement.external1.body");
                    if (val != null)
                    {
                        _config.Message1ExternalBody = val.MediumText;
                    }
                    val = contentList.Find(item => item.Key == "message.loadmanagement.internal2.subject");
                    if (val != null)
                    {
                        _config.Message2InternalSubject = val.ShortText;
                    }
                    val = contentList.Find(item => item.Key == "message.loadmanagement.internal2.body");
                    if (val != null)
                    {
                        _config.Message2InternalBody = val.MediumText;
                    }
                    val = contentList.Find(item => item.Key == "message.loadmanagement.external2.subject");
                    if (val != null)
                    {
                        _config.Message2ExternalSubject = val.ShortText;
                    }
                    val = contentList.Find(item => item.Key == "message.loadmanagement.external2.body");
                    if (val != null)
                    {
                        _config.Message2ExternalBody = val.MediumText;
                    }
                    val = contentList.Find(item => item.Key == "message.loadmanagement.internal3.subject");
                    if (val != null)
                    {
                        _config.Message3InternalSubject = val.ShortText;
                    }
                    val = contentList.Find(item => item.Key == "message.loadmanagement.internal3.body");
                    if (val != null)
                    {
                        _config.Message3InternalBody = val.MediumText;
                    }
                    val = contentList.Find(item => item.Key == "message.loadmanagement.external3.subject");
                    if (val != null)
                    {
                        _config.Message3ExternalSubject = val.ShortText;
                    }
                    val = contentList.Find(item => item.Key == "message.loadmanagement.external3.body");
                    if (val != null)
                    {
                        _config.Message3ExternalBody = val.MediumText;
                    }
                    val = contentList.Find(item => item.Key == "message.loadmanagement.external4.subject");
                    if (val != null)
                    {
                        _config.Message4ExternalSubject = val.ShortText;
                    }
                    val = contentList.Find(item => item.Key == "message.loadmanagement.external4.body");
                    if (val != null)
                    {
                        _config.Message4ExternalBody = val.MediumText;
                    }
                    val = contentList.Find(item => item.Key == "message.loadmanagement.external5.subject");
                    if (val != null)
                    {
                        _config.Message5ExternalSubject = val.ShortText;
                    }
                    val = contentList.Find(item => item.Key == "message.loadmanagement.external5.body");
                    if (val != null)
                    {
                        _config.Message5ExternalBody = val.MediumText;
                    }
                    val = contentList.Find(item => item.Key == "message.loadmanagement.sendername");
                    if (val != null)
                    {
                        _config.SenderName = val.ShortText;
                    }
                }
                _configSetup = true;

                contentCashProvider.Set(cacheKey, _config, DateTime.Now.AddMinutes(30));

            }
            catch (Exception ex)
            {
                _logger.ErrorException(CatagoryEmail, SeverityHigh, PjmNoticationEvent, ex);
                _logger.EmailErrorException(CatagoryEmail, SeverityHigh, PjmNoticationEvent, ex);
                throw;
            }
        }

        private void SendMessageMain(NuanceServiceReference.EPAPIPortTypeClient client, NuanceServiceReference.AuthHeader header, NuanceServiceReference.Team[] teams, string memberId, EventInfo pjmEvent, bool external, bool honeywell)
        {
            // Create new EventTeam object and set the team id in it.
            if (teams != null && teams.Any())
            {

                var nuanceEvent = new NuanceServiceReference.Event {MemberId = memberId};
                var eventTeams = new NuanceServiceReference.EventTeam[teams.Count()];
                var teamCount = 0;
                foreach (var team in teams)
                {
                    var eventTeam = new NuanceServiceReference.EventTeam {TeamId = team.TeamId};

                    eventTeams[teamCount] = eventTeam;
                    teamCount++;
                }

                nuanceEvent.EventTeams = eventTeams;

                // setup event type name
                nuanceEvent.EventTypeName = _config.EventTypeName;

                // create eventArgs Array
                var subject = GetEventSubject(pjmEvent, external, honeywell);
                if (subject != string.Empty)
                {
                    nuanceEvent.EventArgs = new NuanceServiceReference.EventArg[7];
                    nuanceEvent.EventArgs[0] = new NuanceServiceReference.EventArg
                    {
                        Name = "EMAIL_ADDR",
                        Value = _config.SenderEmail
                    };
                    nuanceEvent.EventArgs[1] = new NuanceServiceReference.EventArg
                    {
                        Name = "SUBJECT",
                        Value = subject
                    };
                    nuanceEvent.EventArgs[2] = new NuanceServiceReference.EventArg
                    {
                        Name = "BODY",
                        Value = GetEventBody(pjmEvent, external, honeywell)
                    };
                    nuanceEvent.EventArgs[3] = new NuanceServiceReference.EventArg
                    {
                        Name = "SENDER",
                        Value = _config.SenderName
                    };
                    nuanceEvent.EventArgs[4] = new NuanceServiceReference.EventArg
                    {
                        Name = "TEAM_SENDING_OPTIONS",
                        Value = _config.TeamSendOption
                    };
                    nuanceEvent.EventArgs[5] = new NuanceServiceReference.EventArg
                    {
                        Name = "USER_GREETING",
                        Value = _config.PhoneGreeting
                    };
                    nuanceEvent.EventArgs[6] = new NuanceServiceReference.EventArg
                    {
                        Name = "CALLERID",
                        Value = _config.SenderCallerId
                    };

                    var events = new NuanceServiceReference.Event[1];
                    events[0] = nuanceEvent;

                    Console.WriteLine("Send Request");
                    var response = client.EventCreate(header, events);
                    Console.WriteLine("Response Received");

                    var reqString = new StringBuilder();
                    var respString = new StringBuilder();
                    var serializer = new XmlSerializer(events.GetType());
                    serializer.Serialize(new StringWriter(reqString), events);
                    serializer = new XmlSerializer(response.GetType());
                    serializer.Serialize(new StringWriter(respString), response);
                    _logger.Info(CatagoryEmail, SeverityLow, PjmNoticationEvent, reqString.ToString(), respString.ToString(), MsgEventSent, string.Empty);

                }
            }
            else
            {
                //log no teams return
                var teamString = new StringBuilder();
                var serializer = new XmlSerializer(pjmEvent.RegistrationInfoList.GetType());
                serializer.Serialize(new StringWriter(teamString), pjmEvent.RegistrationInfoList);
                _logger.Error(CatagoryEmail, SeverityMedium, PjmNoticationEvent, MsgNoTeam, teamString.ToString());
            }
        }



        private string ReplaceContent(string content, EventInfo eventInfo)
        {
            var textReplacementTokenMatchRegex = @"(\{%\w*%\})";
            var replacementTokenMatchRegex = new Regex(textReplacementTokenMatchRegex, RegexOptions.IgnoreCase);
            var result = content;
            if (content != string.Empty)
            {
                foreach (Match replacementTokenMatch in replacementTokenMatchRegex.Matches(content))
                {
                    var replaceValue = GetReplaceValue(replacementTokenMatch.Value, eventInfo);
                    result = result.Replace(replacementTokenMatch.Value, replaceValue);
                }
            }
            return result;
        }

        private string GetReplaceValue(string replaceToken, EventInfo eventInfo)
        {
            var replaceValue = string.Empty;

            switch (replaceToken)
            {
                case "{%notificationdate%}":
                    replaceValue = eventInfo.NotifyTime.ToShortDateString();
                    break;
                case "{%personalizedstarttime%}":
                    int addMin;
                    var starttime = eventInfo.NotifyTime;
                    if (eventInfo.LeadTime == LeadTimeType.Long120)
                    { addMin = 120; }
                    else if (eventInfo.LeadTime == LeadTimeType.Short60)
                    { addMin = 60; }
                    else
                    { addMin = 30; }
                    replaceValue = starttime.AddMinutes(addMin).ToShortTimeString();                    
                    break;
                case "{%enddate%}":
                    replaceValue = eventInfo.EndTime.ToShortDateString();
                    break;
                case "{%endtime%}":
                    replaceValue = eventInfo.EndTime.ToShortTimeString();
                    break;
                case "{%newline%}":
                    replaceValue = "\r\n";
                    break;

            }

            return replaceValue;
        }

        private string GetEventBody(EventInfo pjmEvent, bool external, bool isHoneyWell)
        {
            var sbBody = new StringBuilder();
            string body;

            switch (pjmEvent.Status)
            {
                case EventStatus.Scheduled:
                case EventStatus.Active:
                    if (external)
                    {
                        if (!isHoneyWell)
                        {
                            if (_config.Message1ExternalEnabled)
                            {
                                body = ReplaceContent(_config.Message1ExternalBody, pjmEvent);
                                sbBody.Append(body);
                            }
                        }
                        else
                        {
                            if (_config.Message4ExternalEnabled)
                            {
                                body = ReplaceContent(_config.Message4ExternalBody, pjmEvent);
                                sbBody.Append(body);
                            }
                        }
                    }
                    else
                    {
                        if (!isHoneyWell)
                        {
                            if (_config.Message1InternalEnabled)
                            {
                                body = ReplaceContent(_config.Message1InternalBody, pjmEvent);
                                sbBody.Append(body);
                                foreach (var regInfo in pjmEvent.RegistrationInfoList)
                                {
                                    if (!regInfo.IsHoneywellDlc)
                                    {
                                        sbBody.AppendLine();
                                        sbBody.Append(regInfo.TeamName);
                                    }
                                }
                            }
                        }
                    }
                    break;
                case EventStatus.Completed:
                case EventStatus.Canceled:
                    if (isHoneyWell)
                    {
                        if (external)
                        {
                            if (_config.Message5ExternalEnabled)
                            {
                                body = ReplaceContent(_config.Message5ExternalBody, pjmEvent);
                                sbBody.Append(body);
                            }
                        }
                    }
                    else
                    {
                        if (pjmEvent.EndUtcTime < DateTime.UtcNow)
                        {
                            if (external)
                            {
                                if (_config.Message2ExternalEnabled)
                                {
                                    body = ReplaceContent(_config.Message2ExternalBody, pjmEvent);
                                    sbBody.Append(body);
                                }
                            }
                            else
                            {
                                if (_config.Message2InternalEnabled)
                                {
                                    body = ReplaceContent(_config.Message2InternalBody, pjmEvent);
                                    sbBody.Append(body);
                                    foreach (var regInfo in pjmEvent.RegistrationInfoList)
                                    {
                                        if (!regInfo.IsHoneywellDlc)
                                        {
                                            sbBody.AppendLine();
                                            sbBody.Append(regInfo.TeamName);
                                            sbBody.AppendLine();
                                        }
                                    }
                                }
                            }
                        }
                        else
                        {
                            if (external)
                            {
                                if (_config.Message3ExternalEnabled)
                                {
                                    body = ReplaceContent(_config.Message3ExternalBody, pjmEvent);
                                    sbBody.Append(body);
                                }
                            }
                            else
                            {
                                if (_config.Message3InternalEnabled)
                                {
                                    body = ReplaceContent(_config.Message3InternalBody, pjmEvent);
                                    sbBody.Append(body);
                                    foreach (var regInfo in pjmEvent.RegistrationInfoList)
                                    {
                                        if (!regInfo.IsHoneywellDlc)
                                        {
                                            sbBody.AppendLine();
                                            sbBody.Append(regInfo.TeamName);
                                        }
                                    }
                                }
                            }
                        }
                    }
                    
                    
                    break;
            }
            return sbBody.ToString();
        }

        private string GetEventSubject(EventInfo pjmEvent, bool external, bool isHoneyWell)
        {
            var subject = string.Empty;

            switch (pjmEvent.Status)
            {
                case EventStatus.Scheduled:
                case EventStatus.Active:
                    if (external)
                    {
                        if (!isHoneyWell)
                        {
                            if (_config.Message1ExternalEnabled)
                            {
                                subject = _config.Message1ExternalSubject;
                            }
                        }
                        else
                        {
                            if (_config.Message4ExternalEnabled)
                            {
                                subject = _config.Message4ExternalSubject;
                            }
                        }
                    }
                    else
                    {
                        if (!isHoneyWell)
                        {
                            if (_config.Message1InternalEnabled)
                            {
                                subject = _config.Message1InternalSubject;
                            }
                        }
                    }
                    break;
                case EventStatus.Completed:
                case EventStatus.Canceled:
                    if (isHoneyWell)
                    {
                        if (external)
                        {
                            if (_config.Message5ExternalEnabled)
                            {
                                subject = _config.Message5ExternalSubject;
                            }
                        }
                    }
                    else
                    {
                        if (pjmEvent.EndUtcTime < DateTime.UtcNow)
                        {
                            if (external)
                            {
                                if (_config.Message2ExternalEnabled)
                                {
                                    subject = _config.Message2ExternalSubject;
                                }
                            }
                            else
                            {
                                if (_config.Message2InternalEnabled)
                                {
                                    subject = _config.Message2InternalSubject;
                                }
                            }
                        }
                        else
                        {
                            if (external)
                            {
                                if (_config.Message3ExternalEnabled)
                                {
                                    subject = _config.Message3ExternalSubject;
                                }
                            }
                            else
                            {
                                if (_config.Message3InternalEnabled)
                                {
                                    subject = _config.Message3InternalSubject;
                                }
                            }
                        }
                    }


                    break;
            }

            return subject;
        }
        #endregion

    }

    [Serializable]
    public class NuanceConfig
    {
        public string Username { get; set; }
        public string Password { get; set; }
        public string Domain { get; set; }
        public string OemId { get; set; }
        public string OemPassword { get; set; }
        public string WebServiceEndPoint { get; set; }
        public string EventTypeName { get; set; }
        public string SenderEmail { get; set; }
        public string SenderFax { get; set; }
        public string SenderCallerId { get; set; }
        public string SenderNumericPager { get; set; }
        public int MaxRetry { get; set; }
        public int RetryWaitDuration { get; set; }
        public List<string> InternalTeamNames { get; set; }
        public string[] AclaraSupportTeams { get; set; }
        public string TeamSendOption { get; set; }
        public string PhoneGreeting { get; set; }

        public bool Message1ExternalEnabled;
        public string Message1ExternalSubject;
        public string Message1ExternalBody;
        public bool Message1InternalEnabled;
        public string Message1InternalSubject;
        public string Message1InternalBody;
        public bool Message2ExternalEnabled;
        public string Message2ExternalSubject;
        public string Message2ExternalBody;
        public bool Message2InternalEnabled;
        public string Message2InternalSubject;
        public string Message2InternalBody;
        public bool Message3ExternalEnabled;
        public string Message3ExternalSubject;
        public string Message3ExternalBody;
        public bool Message3InternalEnabled;
        public string Message3InternalSubject;
        public string Message3InternalBody;
        public bool Message4ExternalEnabled;
        public string Message4ExternalSubject;
        public string Message4ExternalBody;
        public bool Message5ExternalEnabled;
        public string Message5ExternalSubject;
        public string Message5ExternalBody;
        public string SenderName;
    }
}
