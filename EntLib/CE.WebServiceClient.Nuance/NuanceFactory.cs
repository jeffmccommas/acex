﻿namespace CE.WebServiceClient.Nuance
{
    public class NuanceFactory
    {
        public INuance CreateNuance(int clientId)
        {
            var nuance = new NuanceClient(clientId);

            return nuance;
        }
    }
}
