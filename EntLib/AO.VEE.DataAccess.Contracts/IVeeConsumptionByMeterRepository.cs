﻿using System.Collections.Generic;
using AO.VEE.DTO;
using CE.AO.Models;

namespace AO.VEE.DataAccess.Contracts
{
	/// <summary>
	/// IVeeConsumptionByMeterRepository interface is used to fetch/insert/update data from/to the database
	/// </summary>
	public interface IVeeConsumptionByMeterRepository
	{
		LogModel LogModel { get; set; }

		/// <summary>
		/// This method is used to fetch the VeeConsumptionByMeter detail.
		/// </summary>
		/// <param name="clientId">Client Id of the data to be fetched</param>
		/// <param name="meterId">Meter Id of the data to be fetched</param>
		/// <returns>VeeConsumptionByMeterDTO</returns>
		VeeConsumptionByMeterDto GetLastVeeConsumptionByMeter(int clientId, string meterId);

		/// <summary>
		/// To insert/update the VeeConsumptionByMeter data in batch to the database
		/// </summary>
		/// <param name="veeConsumptions">List of VeeConsumptionByMeter data which is to be inserted/updated</param>
		/// <returns></returns>
		void InsertOrMergeBatchVeeConsumptionByMeterAsync(List<VeeConsumptionByMeterDto> veeConsumptions);

		/// <summary>
		/// To delete the VeeConsumptionByMeter data in batch to the database
		/// </summary>
		/// <param name="veeConsumptions">VeeConsumptionByMeter data which is to be deleted</param>
		void DeleteVeeConsumptionByMeter(VeeConsumptionByMeterDto veeConsumptions);
	}
}
