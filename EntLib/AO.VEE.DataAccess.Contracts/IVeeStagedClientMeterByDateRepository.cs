﻿using System.Collections.Generic;
using AO.VEE.DTO;
using CE.AO.Models;

namespace AO.VEE.DataAccess.Contracts
{
	/// <summary>
	/// IVeeStagedClientMeterByDateRepository interface is used to fetch/insert/update data from/to the database
	/// </summary>
	public interface IVeeStagedClientMeterByDateRepository
	{
		LogModel LogModel { get; set; }

		/// <summary>
		/// To insert/update the VeeStagedClientMeterByDateDTO to the database
		/// </summary>
		/// <param name="model">VeeStagedClientMeterByDateDTO model which is to be inserted/updated</param>
		void InsertOrMergeVeeStagedDataAsync(VeeStagedClientMeterByDateDto model);

        /// <summary>
        /// This method is used to fetch the VeeStagedClientMeterByDate details based on start date and end date.
        /// </summary>
        /// <param name="clientId">Client Id of the data to be fetched</param>
        /// <param name="meterId">Meter Id of the data to be fetched</param>
        /// <returns>List of model</returns>
        IList<VeeStagedClientMeterByDateDto> GetVeeStagedDataList(int clientId, string meterId);

		/// <summary>
		/// To delete the data from database
		/// </summary>
		/// <param name="model">VeeStagedClientMeterByDateDTO model which is to be inserted/updated</param>
		void DeleteVeeStagedData(VeeStagedClientMeterByDateDto model);
	}
}
