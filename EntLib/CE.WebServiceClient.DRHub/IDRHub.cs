﻿using CE.NotificationManager.Entities;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CE.WebServiceClient.DRHub
{
    public interface IDRHub
    {
        EventsCollection PollEvents();

        void AcknowledgeEvents(EventsCollection events, int retry);

        void Ping(int retry);
    }
}
