﻿namespace CE.WebServiceClient.DRHub
{
    public class DRHubFactory
    {

        public IDRHub CreateDRHub(int clientId)
        {
            var drhub = new DRHubClient(clientId);

            return drhub;
        }

    }
}
