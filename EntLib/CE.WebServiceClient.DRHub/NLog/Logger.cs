﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NLog;
using NLog.Targets;

namespace CE.WebServiceClient.ELRS
{
    public class ASyncLogger
    {
        private static Logger _logger = LogManager.GetCurrentClassLogger();

        private const string propCategory = "Category";
        private const string propRequestData = "RequestData";
        private const string propResponseData = "ResponseData";
        private const string propMessage = "Message";

        private void LogEvent(LogLevel logLevel, string category, string requestData, string responseData, string message)
        {
            LogEventInfo theEvent = new LogEventInfo(logLevel, "", message);
            theEvent.Properties[propCategory] = category;
            theEvent.Properties[propRequestData] = requestData;
            theEvent.Properties[propResponseData] = responseData;
            theEvent.Properties[propMessage] = message;
            _logger.Log(theEvent);
        }

        private void LogEventException(LogLevel logLevel, Exception ex)
        {
            LogEventInfo theEvent = new LogEventInfo(logLevel, "", ex.ToString());
            theEvent.Exception = ex;
            _logger.Log(theEvent);
        }

        public void Info(string category, string requestData, string responseData, string message)
        {
            LogEvent(LogLevel.Info,category, requestData, responseData, message);
        }

        public void Warn(string category, string requestData, string responseData, string message)
        {
            LogEvent(LogLevel.Warn,category, requestData, responseData, message);
        }

        public void Debug(string trackingid, string message)
        {
            throw new NotImplementedException();
        }

        public void Error(string category,string requestData, string responseData, string message)
        {
            LogEvent(LogLevel.Error, category, requestData, responseData, message);
        }

        public void ErrorException(string trackingid, Exception ex)
        {
            LogEventException(LogLevel.Error, ex);
        }

        public void Fatal(string trackingid, string message)
        {
            throw new NotImplementedException();
        }
    }
}
