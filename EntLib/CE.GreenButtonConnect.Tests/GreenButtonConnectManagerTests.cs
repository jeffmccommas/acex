﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using CE.GreenButtonConnect.Tests.Helper;

namespace CE.GreenButtonConnect.Tests
{
    [TestClass()]
    public class GreenButtonConnectManagerTests
    {
        private string _greenButtonDomain = "http://www.greenbutton.com";
        public TestContext TestContext { get; set; }

        //private GreenButtonConnectConfig _config;
        //[TestInitialize]
        //public void TestInit()
        //{
        //    _config = GreenButtonHelper.GetConfigSetting();
        //}

        [TestMethod()]
        [DeploymentItem("TestData\\GetUsagePointTestData.xml")]
        [DataSource("Microsoft.VisualStudio.TestTools.DataSource.XML", "|DataDirectory|\\GetUsagePointTestData.xml", "UsagePoint", DataAccessMethod.Sequential)]
        public void GetUsagePointTests()
        {
            try
            {
                var clientId = Convert.ToInt32(TestContext.DataRow["clientId"].ToString());
                var numberOfUsagePoint = Convert.ToInt32(TestContext.DataRow["UsagePointCount"].ToString());
                var numOfReadingType = Convert.ToInt32(TestContext.DataRow["ReadingTypeCount"].ToString());
                var intervalId = Convert.ToInt32(TestContext.DataRow["IntervalId"].ToString());
                var commodityId = Convert.ToInt32(TestContext.DataRow["CommodityId"].ToString());
                var uomKey = TestContext.DataRow["UomKey"].ToString();
                var convertGas = Convert.ToBoolean(TestContext.DataRow["ConvertGas"].ToString());
                var hasConsumption = Convert.ToBoolean(TestContext.DataRow["HasConsumption"].ToString());
                var start = TestContext.DataRow["StartDate"].ToString();
                var end = TestContext.DataRow["EndDate"].ToString();
                var expectedCount = Convert.ToInt32(TestContext.DataRow["resultCount"].ToString());

                DateTime startDate = !string.IsNullOrEmpty(start) ? Convert.ToDateTime(start) : DateTime.UtcNow.Date.AddDays(-1);
                DateTime? endDate = !string.IsNullOrEmpty(end) ? Convert.ToDateTime(end) : (DateTime?)null;

                // mock usage point
                var usagePointMock = GreenButtonHelper.MockUsagePoints(numberOfUsagePoint, numOfReadingType, intervalId, commodityId, uomKey,
                    convertGas, hasConsumption, startDate, endDate);


                var gbcFactory = new GreenButtonConnectFactory();
                var gbcManager = gbcFactory.CreateGreenButtonManager(clientId, _greenButtonDomain);


                var result = gbcManager.GetUsagePoints(usagePointMock);

                Assert.IsNotNull(result);
                Assert.AreEqual(expectedCount, result.Count);
               
            }
            catch (Exception ex)
            {
                Assert.Fail(ex.Message);
            }
        }

        [TestMethod()]
        [DeploymentItem("TestData\\GetUsagePointTestData.xml")]
        [DataSource("Microsoft.VisualStudio.TestTools.DataSource.XML", "|DataDirectory|\\GetUsagePointTestData.xml", "UsagePoint", DataAccessMethod.Sequential)]
        public void GetLocalTimeParamTests()
        {
            try
            {
                var clientId = Convert.ToInt32(TestContext.DataRow["clientId"].ToString());
                var numberOfUsagePoint = Convert.ToInt32(TestContext.DataRow["UsagePointCount"].ToString());
                var numOfReadingType = Convert.ToInt32(TestContext.DataRow["ReadingTypeCount"].ToString());
                var intervalId = Convert.ToInt32(TestContext.DataRow["IntervalId"].ToString());
                var commodityId = Convert.ToInt32(TestContext.DataRow["CommodityId"].ToString());
                var uomKey = TestContext.DataRow["UomKey"].ToString();
                var convertGas = Convert.ToBoolean(TestContext.DataRow["ConvertGas"].ToString());
                var hasConsumption = Convert.ToBoolean(TestContext.DataRow["HasConsumption"].ToString());
                var start = TestContext.DataRow["StartDate"].ToString();
                var end = TestContext.DataRow["EndDate"].ToString();

                DateTime startDate = !string.IsNullOrEmpty(start) ? Convert.ToDateTime(start) : DateTime.UtcNow.Date.AddDays(-1);
                DateTime? endDate = !string.IsNullOrEmpty(end) ? Convert.ToDateTime(end) : (DateTime?)null;

                // mock usage point
                var usagePointMock = GreenButtonHelper.MockUsagePoints(numberOfUsagePoint, numOfReadingType, intervalId, commodityId, uomKey,
                    convertGas, hasConsumption, startDate, endDate);
                //var localTimeParamMock = usagePointMock.Select(u => u.TimeConfiguration).GroupBy(t => t.Id).Select(t => t.FirstOrDefault()).ToList();
                var localTimeParamMock = usagePointMock.Select(u => u.TimeConfiguration).GroupBy(t => t.Id).Select(t => t.FirstOrDefault()).ToList();
                var gbcFactory = new GreenButtonConnectFactory();
                var gbcManager = gbcFactory.CreateGreenButtonManager(clientId, _greenButtonDomain);


                var result = gbcManager.GetLocalTimeParams(localTimeParamMock);

                Assert.IsNotNull(result);
                Assert.AreEqual(1, result.Count);

            }
            catch (Exception ex)
            {
                Assert.Fail(ex.Message);
            }
        }

        [TestMethod()]
        [DeploymentItem("TestData\\GetReadingTypeTestData.xml")]
        [DataSource("Microsoft.VisualStudio.TestTools.DataSource.XML", "|DataDirectory|\\GetReadingTypeTestData.xml", "ReadingType", DataAccessMethod.Sequential)]
        public void GetReadingTypesTest()
        {
            try
            {
                var clientId = Convert.ToInt32(TestContext.DataRow["clientId"].ToString());
                var numberOfUsagePoint = Convert.ToInt32(TestContext.DataRow["UsagePointCount"].ToString());
                var numOfReadingType = Convert.ToInt32(TestContext.DataRow["ReadingTypeCount"].ToString());
                var intervalId = Convert.ToInt32(TestContext.DataRow["IntervalId"].ToString());
                var commodityId = Convert.ToInt32(TestContext.DataRow["CommodityId"].ToString());
                var uomKey = TestContext.DataRow["UomKey"].ToString();
                var convertGas = Convert.ToBoolean(TestContext.DataRow["ConvertGas"].ToString());
                var hasConsumption = Convert.ToBoolean(TestContext.DataRow["HasConsumption"].ToString());
                var start = TestContext.DataRow["StartDate"].ToString();
                var end = TestContext.DataRow["EndDate"].ToString();
                var expectedCount = Convert.ToInt32(TestContext.DataRow["resultCount"].ToString());

                DateTime startDate = !string.IsNullOrEmpty(start) ? Convert.ToDateTime(start) : DateTime.UtcNow.Date.AddDays(-1);
                DateTime? endDate = !string.IsNullOrEmpty(end) ? Convert.ToDateTime(end) : (DateTime?)null;

                // mock data
                var usagePointMock = GreenButtonHelper.MockUsagePoints(numberOfUsagePoint, numOfReadingType, intervalId, commodityId, uomKey,
                    convertGas, hasConsumption, startDate, endDate);
                var readingTypeMock = usagePointMock.SelectMany(u => u.MeterReading).Select(m => m.ReadingType).ToList();

                var gbcFactory = new GreenButtonConnectFactory();
                var gbcManager = gbcFactory.CreateGreenButtonManager(clientId, _greenButtonDomain);


                var result = gbcManager.GetReadingTypes(readingTypeMock);

                Assert.IsNotNull(result);
                Assert.AreEqual(expectedCount, result.Count);

            }
            catch (Exception ex)
            {
                Assert.Fail(ex.Message);
            }
        }

        [TestMethod()]
        [DeploymentItem("TestData\\GetMeterReadingTestData.xml")]
        [DataSource("Microsoft.VisualStudio.TestTools.DataSource.XML", "|DataDirectory|\\GetMeterReadingTestData.xml", "MeterReading", DataAccessMethod.Sequential)]
        public void GetMeterReadingsTest()
        {
            try
            {
                var clientId = Convert.ToInt32(TestContext.DataRow["clientId"].ToString());
                var numberOfUsagePoint = Convert.ToInt32(TestContext.DataRow["UsagePointCount"].ToString());
                var numOfReadingType = Convert.ToInt32(TestContext.DataRow["ReadingTypeCount"].ToString());
                var intervalId = Convert.ToInt32(TestContext.DataRow["IntervalId"].ToString());
                var commodityId = Convert.ToInt32(TestContext.DataRow["CommodityId"].ToString());
                var uomKey = TestContext.DataRow["UomKey"].ToString();
                var convertGas = Convert.ToBoolean(TestContext.DataRow["ConvertGas"].ToString());
                var hasConsumption = Convert.ToBoolean(TestContext.DataRow["HasConsumption"].ToString());
                var start = TestContext.DataRow["StartDate"].ToString();
                var end = TestContext.DataRow["EndDate"].ToString();
                var expectedCount = Convert.ToInt32(TestContext.DataRow["resultCount"].ToString());

                DateTime startDate = !string.IsNullOrEmpty(start) ? Convert.ToDateTime(start) : DateTime.UtcNow.Date.AddDays(-1);
                DateTime? endDate = !string.IsNullOrEmpty(end) ? Convert.ToDateTime(end) : (DateTime?)null;

                // mock data
                var usagePointMock = GreenButtonHelper.MockUsagePoints(numberOfUsagePoint, numOfReadingType, intervalId, commodityId, uomKey,
                    convertGas, hasConsumption, startDate, endDate);
                var meterReadingMock = usagePointMock.SelectMany(u => u.MeterReading).ToList();

                var gbcFactory = new GreenButtonConnectFactory();
                var gbcManager = gbcFactory.CreateGreenButtonManager(clientId, _greenButtonDomain);


                var result = gbcManager.GetMeterReadings(meterReadingMock);

                Assert.IsNotNull(result);
                Assert.AreEqual(expectedCount, result.Count);

            }
            catch (Exception ex)
            {
                Assert.Fail(ex.Message);
            }
        }

        [TestMethod()]
        [DeploymentItem("TestData\\GetIntervalBlockTestData.xml")]
        [DataSource("Microsoft.VisualStudio.TestTools.DataSource.XML", "|DataDirectory|\\GetIntervalBlockTestData.xml", "IntervalBlock", DataAccessMethod.Sequential)]
        public void GetIntervalBlocksTest()
        {
            try
            {
                var clientId = Convert.ToInt32(TestContext.DataRow["clientId"].ToString());
                var numberOfUsagePoint = Convert.ToInt32(TestContext.DataRow["UsagePointCount"].ToString());
                var numOfReadingType = Convert.ToInt32(TestContext.DataRow["ReadingTypeCount"].ToString());
                var intervalId = Convert.ToInt32(TestContext.DataRow["IntervalId"].ToString());
                var commodityId = Convert.ToInt32(TestContext.DataRow["CommodityId"].ToString());
                var uomKey = TestContext.DataRow["UomKey"].ToString();
                var convertGas = Convert.ToBoolean(TestContext.DataRow["ConvertGas"].ToString());
                var hasConsumption = Convert.ToBoolean(TestContext.DataRow["HasConsumption"].ToString());
                var start = TestContext.DataRow["StartDate"].ToString();
                var end = TestContext.DataRow["EndDate"].ToString();
                var expectedCount = Convert.ToInt32(TestContext.DataRow["resultCount"].ToString());

                DateTime startDate = !string.IsNullOrEmpty(start) ? Convert.ToDateTime(start) : DateTime.UtcNow.Date.AddDays(-1);
                DateTime? endDate = !string.IsNullOrEmpty(end) ? Convert.ToDateTime(end) : (DateTime?)null;

                // mock data
                var usagePointMock = GreenButtonHelper.MockUsagePoints(numberOfUsagePoint, numOfReadingType, intervalId, commodityId, uomKey,
                    convertGas, hasConsumption, startDate, endDate);
                var intervalBlockMock = usagePointMock.SelectMany(u => u.MeterReading).SelectMany(m => m.IntervalBlock ?? new List<IntervalBlockEntry>()).ToList();

                var gbcFactory = new GreenButtonConnectFactory();
                var gbcManager = gbcFactory.CreateGreenButtonManager(clientId, _greenButtonDomain);


                var result = gbcManager.GetIntervalBlocks(intervalBlockMock);

                
                Assert.IsNotNull(result);
                Assert.AreEqual(expectedCount, result.Count);

            }
            catch (Exception ex)
            {
                Assert.Fail(ex.Message);
            }
        }

        [TestMethod()]
        [DeploymentItem("TestData\\GetSubscriptionTestData.xml")]
        [DataSource("Microsoft.VisualStudio.TestTools.DataSource.XML", "|DataDirectory|\\GetSubscriptionTestData.xml", "Subscription", DataAccessMethod.Sequential)]
        public void GetSubscriptionTest()
        {
            try
            {
                var clientId = Convert.ToInt32(TestContext.DataRow["clientId"].ToString());
                var numberOfUsagePoint = Convert.ToInt32(TestContext.DataRow["UsagePointCount"].ToString());
                var numOfReadingType = Convert.ToInt32(TestContext.DataRow["ReadingTypeCount"].ToString());
                var intervalId = Convert.ToInt32(TestContext.DataRow["IntervalId"].ToString());
                var commodityId = Convert.ToInt32(TestContext.DataRow["CommodityId"].ToString());
                var uomKey = TestContext.DataRow["UomKey"].ToString();
                var convertGas = Convert.ToBoolean(TestContext.DataRow["ConvertGas"].ToString());
                var hasConsumption = Convert.ToBoolean(TestContext.DataRow["HasConsumption"].ToString());
                var start = TestContext.DataRow["StartDate"].ToString();
                var end = TestContext.DataRow["EndDate"].ToString();

                DateTime startDate = !string.IsNullOrEmpty(start) ? Convert.ToDateTime(start) : DateTime.UtcNow.Date.AddDays(-1);
                DateTime? endDate = !string.IsNullOrEmpty(end) ? Convert.ToDateTime(end) : (DateTime?)null;

                // mock data
                var subscriptionMock = GreenButtonHelper.MockSubscription(numberOfUsagePoint, numOfReadingType, intervalId,
                    commodityId, uomKey, convertGas, hasConsumption, startDate, endDate);


                var gbcFactory = new GreenButtonConnectFactory();
                var gbcManager = gbcFactory.CreateGreenButtonManager(clientId, _greenButtonDomain);


                var result = gbcManager.GetSubscription(subscriptionMock);

                Assert.IsNotNull(result);

            }
            catch (Exception ex)
            {
                Assert.Fail(ex.Message);
            }
        }

        [TestMethod()]
        [DeploymentItem("TestData\\GenerateGreenButtonTestData.xml")]
        [DataSource("Microsoft.VisualStudio.TestTools.DataSource.XML", "|DataDirectory|\\GenerateGreenButtonTestData.xml", "GreenButton", DataAccessMethod.Sequential)]
        public void GenerateGreenButtonConnectTest()
        {
            try
            {
                var clientId = Convert.ToInt32(TestContext.DataRow["clientId"].ToString());
                var numberOfUsagePoint = Convert.ToInt32(TestContext.DataRow["UsagePointCount"].ToString());
                var numOfReadingType = Convert.ToInt32(TestContext.DataRow["ReadingTypeCount"].ToString());
                var intervalId = Convert.ToInt32(TestContext.DataRow["IntervalId"].ToString());
                var commodityId = Convert.ToInt32(TestContext.DataRow["CommodityId"].ToString());
                var uomKey = TestContext.DataRow["UomKey"].ToString();
                var convertGas = Convert.ToBoolean(TestContext.DataRow["ConvertGas"].ToString());
                var hasConsumption = Convert.ToBoolean(TestContext.DataRow["HasConsumption"].ToString());
                var start = TestContext.DataRow["StartDate"].ToString();
                var end = TestContext.DataRow["EndDate"].ToString();
                var hasResult = Convert.ToBoolean(TestContext.DataRow["hasResult"].ToString());
                GreenButtonType greenButtonType;
                Enum.TryParse(TestContext.DataRow["GreenButtonType"].ToString(), out greenButtonType);



                DateTime startDate = !string.IsNullOrEmpty(start) ? Convert.ToDateTime(start) : DateTime.UtcNow.Date.AddDays(-1);
                DateTime? endDate = !string.IsNullOrEmpty(end) ? Convert.ToDateTime(end) : (DateTime?)null;

                // mock data
                var subscriptionMock = GreenButtonHelper.MockSubscription(numberOfUsagePoint, numOfReadingType, intervalId,
                    commodityId, uomKey, convertGas, hasConsumption, startDate, endDate);

                if (greenButtonType == GreenButtonType.ApplicationInfo)
                {
                    subscriptionMock.ApplicationInformation = GreenButtonHelper.MockApplicationInfo(clientId.ToString(), startDate);
                }

                var gbcFactory = new GreenButtonConnectFactory();
                var gbcManager = gbcFactory.CreateGreenButtonManager(clientId, _greenButtonDomain);


                var result = gbcManager.GenerateGreenButtonConnect(subscriptionMock, greenButtonType);

                Assert.IsNotNull(result);
                if(hasResult)
                    Assert.AreNotEqual(string.Empty, result.GreenButtonXml);
                else
                    Assert.AreEqual(string.Empty, result.GreenButtonXml);

            }
            catch (Exception ex)
            {
                Assert.Fail(ex.Message);
            }
        }


        [TestMethod()]
        [DeploymentItem("TestData\\GenerateServiceStatusTestData.xml")]
        [DataSource("Microsoft.VisualStudio.TestTools.DataSource.XML", "|DataDirectory|\\GenerateServiceStatusTestData.xml", "ServiceStatus", DataAccessMethod.Sequential)]
        public void GenerateGreenButtonServiceStatusTest()
        {
            try
            {
                var clientId = Convert.ToInt32(TestContext.DataRow["clientId"].ToString());
                var clientAccessToken = Convert.ToBoolean(TestContext.DataRow["ClientAccessToken"].ToString());
                

                var gbcFactory = new GreenButtonConnectFactory();
                var gbcManager = gbcFactory.CreateGreenButtonManager(clientId, _greenButtonDomain);


                var result = gbcManager.GenerateGreenButtonServiceStatus(clientAccessToken);

                Assert.IsNotNull(result);
                Assert.AreNotEqual(string.Empty, result.GreenButtonXml);

                if (clientAccessToken)
                    Assert.AreEqual(true, result.GreenButtonXml.Contains("<currentStatus>1</currentStatus>"));
                else
                    Assert.AreEqual(true, result.GreenButtonXml.Contains("<currentStatus>0</currentStatus>"));

            }
            catch (Exception ex)
            {
                Assert.Fail(ex.Message);
            }
        }
    }
}