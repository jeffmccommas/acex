﻿using System.Linq;
using System.Threading.Tasks;
using AO.BusinessContracts;
using AutoMapper;
using CE.AO.DataAccess;
using CE.AO.Entities.TableStorageEntities;
using CE.AO.Entities;
using CE.AO.Models;
using Microsoft.WindowsAzure.Storage.Table;
using System;
using System.Collections.Generic;

namespace CE.AO.Business
{
    public class AccountLookup : IAccountLookup
    {
        public AccountLookup(LogModel logModel, ITableRepository tableRepository)
        {
            LogModel = logModel;
            TableRepository = tableRepository;
        }

        public LogModel LogModel { get; set; }

        public ITableRepository TableRepository { get; set; }

        public List<AccountLookupModel> GetAccountMeterDetails(int clientId, string meterId)
        {
            var partitionKey = AccountLookupEntity.SetPartitionKey(clientId, meterId);
            var filter = TableQuery.GenerateFilterCondition("PartitionKey", QueryComparisons.Equal,
                partitionKey);

            var accountMeterDetails =
                TableRepository
                    .GetAllAsync<AccountLookupEntity>(Constants.TableNames.AccountMeterLookup, filter).Where(e => e.IsActive != false);
            var distinctAccountsMeterDetails = accountMeterDetails.Select(l => new AccountLookupEntity
            {
                CustomerId = l.CustomerId,
                AccountId = l.AccountId,
                MeterId = l.MeterId,
                IsActive = l.IsActive,
                ServiceContractId = l.ServiceContractId,
                ClientId = l.ClientId,
                IsServiceContractCreatedBySystem = l.IsServiceContractCreatedBySystem
            }).GroupBy(e => new { e.CustomerId, e.AccountId, e.MeterId })
                .Select(z => z.OrderBy(i => i.AccountId).First())
                .ToList();
            return Mapper.Map<List<AccountLookupEntity>, List<AccountLookupModel>>(distinctAccountsMeterDetails);
        }


        public async Task<AccountLookupModel> GetAccountMeterDetails(int clientId, string meterId, string customerId)
        {
            var partitionKey = AccountLookupEntity.SetPartitionKey(clientId, meterId);
            var rowKey = AccountLookupEntity.SetRowKey(customerId);
            var entity = await TableRepository.GetSingleAsync<AccountLookupEntity>(Constants.TableNames.AccountMeterLookup,
                partitionKey, rowKey);

            return Mapper.Map<AccountLookupEntity, AccountLookupModel>(entity);
        }

        public async Task<bool> InsertOrMergeAccountLookupAsync(AccountLookupModel model)
        {
            var entity = Mapper.Map<AccountLookupModel, AccountLookupEntity>(model);
            entity.PartitionKey = AccountLookupEntity.SetPartitionKey(model.ClientId, model.MeterId);
            entity.RowKey = AccountLookupEntity.SetRowKey(model.CustomerId);
            return await TableRepository.InsertOrMergeSingleAsync(Constants.TableNames.AccountMeterLookup, entity);
        }

        /// <summary>
        /// To delete the record from the database
        /// </summary>
        /// <param name="clientId"></param>
        /// <param name="meterid"></param>
        /// <param name="customerid"></param>
        /// <returns></returns>
        public Task<bool> DeleteHistoryAccountLookupAsyc(int clientId, string meterid, string customerid)
        {
            //Delete Data from Azure Table Logic goes here. Need to write this method as Interface forcing too. Added Interface Method to Delete Data from cassandra table.
            throw new NotImplementedException();
        }
    }
}