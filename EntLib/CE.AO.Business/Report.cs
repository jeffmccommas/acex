﻿using CE.AO.DataAccess;
using CE.AO.Entities;
using CE.AO.Logging;
using Microsoft.Practices.Unity;
using Microsoft.WindowsAzure.Storage.Table;
using System;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.IO;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Blob;
using CsvHelper;
using System.Linq;
using AO.BusinessContracts;
using CE.AO.Models;

namespace CE.AO.Business
{
    public class Report : IReport
    {
        public Report(LogModel logModel, ITableRepository tableRepository)
        {
            LogModel = logModel;
            TableRepository = tableRepository;
        }

        public LogModel LogModel { get; set; }

        public ITableRepository TableRepository { get; set; }

        [Dependency]
        public IClientAccount ClientAccountManager { get; set; }

        [Dependency]
        public IEvaluation EvaluationManager { get; set; }

        [Dependency]
        public INotification NotificationManager { get; set; }

        [Dependency]
        public Lazy<IClientConfigFacade> ClientConfigFacadeManager { get; set; }

        public Task<bool> ExportInsights(int clientId, DateTime asOfDate)
        {
            var clientSettings = ClientConfigFacadeManager.Value.GetClientSettings(clientId);
            if (clientSettings == null)
            {
                Logger.Warn("No settings found for export insights.", LogModel);
                return Task.FromResult(false);
            }
            TableContinuationToken token = null;
            do
            {
                var clientAcoountsList = ClientAccountManager.GetClientAccounts(clientId, 1000, ref token);
                foreach (var item in clientAcoountsList)
                {
                    var insightModelList = EvaluationManager.GetDayInsights(clientId, item.CustomerId, item.AccountId, asOfDate).ToList();

                    var programList = insightModelList.Select(p => p.ProgramName).Distinct().ToList();

                    foreach (var program in programList)
                    {
                        var clientSettingProgram =
                            clientSettings.Programs.FirstOrDefault(p => p.UtilityProgramName == program);
                        if (clientSettingProgram != null && clientSettingProgram.FileExportRequired)
                        {
                            var filename = ExportToFile(Constants.TableNames.Insight, clientId, asOfDate, program);
                            var programInsigntModelList = insightModelList.Where(p => p.ProgramName == program).ToList();
                            if (programInsigntModelList.Any())
                                CreateUploadCsvFile(clientId, filename, programInsigntModelList);
                        }
                    }
                }
            } while (token != null);

            return Task.FromResult(true);
        }

        public Task<bool> ExportNotifications(int clientId, DateTime asOfDate)
        {
            var clientSettings = ClientConfigFacadeManager.Value.GetClientSettings(clientId);
            if (clientSettings == null)
            {
                Logger.Warn("No settings found for export insights.", LogModel);
                return Task.FromResult(false);
            }

            TableContinuationToken token = null;
            do
            {
                var clientAcoountsList = ClientAccountManager.GetClientAccounts(clientId, 1000, ref token);

                foreach (var item in clientAcoountsList)
                {
                    var notificationModelList = NotificationManager.GetDayNotifications(clientId, item.CustomerId,
                        item.AccountId, asOfDate);
                    var notificationList = (from n in notificationModelList
                                            join p in clientSettings.Programs on n.ProgramName equals p.UtilityProgramName
                                            where p.FileExportRequired
                                            select n).OrderBy(n => n.ProgramName).ToList();

                    var filename = ExportToFile(Constants.TableNames.Notification, clientId, asOfDate, string.Empty);
                    if (notificationList.Any())
                        CreateUploadCsvFile(clientId, filename, notificationList);

                }
            } while (token != null);
            return Task.FromResult(true);
        }

        private string ExportToFile(string exportType, int clientId, DateTime date, string programName)
        {
            string filename = "";
            switch (exportType)
            {
                case "Insight":
                    filename = clientId + "_Insight_" + programName + date.ToString("_yyyy-MM-dd") + ".csv";
                    break;

                case "Notification":
                    filename = clientId + "_Notification_" + date.ToString("yyyy-MM-dd") + ".csv";
                    break;
            }
            return filename;
        }

        public static void CreateUploadCsvFile<T>(int clientId, string csvNameWithExt, IEnumerable<T> exportList)
            where T : class
        {
            var storageAccount =
                CloudStorageAccount.Parse(
                    System.Configuration.ConfigurationManager.AppSettings["AzureStorageConnectionString"]);
            var blobClient = storageAccount.CreateCloudBlobClient();
            var containerName = $"client{clientId}";

            var blobContainer = blobClient.GetContainerReference(containerName);
            blobContainer.CreateIfNotExists();

            var blob = blobContainer.GetAppendBlobReference($"Reports/{csvNameWithExt}");

            if (!blob.Exists())
            {
                blob.CreateOrReplace();
                blob.Properties.ContentType = "application/octet-stream";
                blob.SetProperties();
            }

            var streamWriter = new StreamWriter(blob.OpenWrite(false));
            using (var csvWriter = new CsvWriter(streamWriter))
            {
                csvWriter.WriteRecords(exportList);
            }
        }
    }
}
