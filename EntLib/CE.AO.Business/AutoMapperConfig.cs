﻿using System;
using AutoMapper;
using CE.AO.Entities.TableStorageEntities;
using CE.AO.Models;
using CE.AO.Models.RawFileModels;
using Preprocessing = AO.BusinessContracts.Preprocessing;

namespace CE.AO.Business
{
    public static class AutoMapperConfig
    {
        public static void AutoMapperMappings()
        {
            Mapper.CreateMap<string, bool?>()
                .ConvertUsing(str => !string.IsNullOrWhiteSpace(str) ? Convert.ToBoolean(str.Trim()) : (bool?)null);

            Mapper.CreateMap<string, DateTime?>()
                .ConvertUsing(str => !string.IsNullOrWhiteSpace(str) ? Convert.ToDateTime(str.Trim()) : (DateTime?)null);

            Mapper.CreateMap<string, double?>()
                .ConvertUsing(str => !string.IsNullOrWhiteSpace(str) ? Convert.ToDouble(str.Trim()) : (double?)null);

            Mapper.CreateMap<string, Guid?>()
                .ConvertUsing(str => !string.IsNullOrWhiteSpace(str) ? Guid.Parse(str.Trim()) : (Guid?)null);

            Mapper.CreateMap<string, int?>()
                .ConvertUsing(str => !string.IsNullOrWhiteSpace(str) ? Convert.ToInt32(str.Trim()) : (int?)null);

            Mapper.CreateMap<string, string>()
                .ConvertUsing(str => !string.IsNullOrWhiteSpace(str) ? str.Trim() : str);

            //#region AmiRequestModel

            //Mapper.CreateMap<AmiRequestModel, BillingModel>()
            //    .ForMember(dest => dest.AccountId, opts => opts.MapFrom(src => src.AccountNumber))
            //    .ForMember(dest => dest.ServiceContractId, opts => opts.MapFrom(src => src.ServiceId))
            //    .ForMember(dest => dest.CommodityId, opts => opts.MapFrom(src => src.FuelId))
            //    .ForSourceMember(dest => dest.StartDate, opts => opts.Ignore())
            //    .ForSourceMember(dest => dest.EndDate, opts => opts.Ignore())
            //    .ForSourceMember(dest => dest.BillingPeriodStartDate, opts => opts.Ignore())
            //    .ForAllUnmappedMembers(o => o.Ignore());

            //Mapper.CreateMap<BillingModel, AmiRequestModel>()
            //    .ForMember(dest => dest.AccountNumber, opts => opts.MapFrom(src => src.AccountId))
            //    .ForMember(dest => dest.ServiceId, opts => opts.MapFrom(src => src.ServiceContractId))
            //    .ForMember(dest => dest.FuelId, opts => opts.MapFrom(src => src.CommodityId))
            //    .ForMember(dest => dest.StartDate, opts => opts.Ignore())
            //    .ForMember(dest => dest.EndDate, opts => opts.Ignore())
            //    .ForMember(dest => dest.BillingPeriodStartDate, opts => opts.Ignore())
            //    .ForAllUnmappedMembers(o => o.Ignore());

            //#endregion

            //#region AccountMeterLookup

            //Mapper.CreateMap<AccountLookupEntity, AccountLookupModel>()
            //   .ForAllUnmappedMembers(o => o.Ignore());

            //Mapper.CreateMap<AccountLookupModel, AccountLookupEntity>()
            //   .ForAllUnmappedMembers(o => o.Ignore());

            //Mapper.CreateMap<AccountLookupModel, BillingCustomerPremiseModel>()
            //    .ForMember(dest => dest.customer_id, opts => opts.MapFrom(src => src.CustomerId))
            //    .ForMember(dest => dest.meter_id, opts => opts.MapFrom(src => src.MeterId))
            //    .ForMember(dest => dest.Service_contract, opts => opts.MapFrom(src => src.ServiceContractId))
            //    .ForMember(dest => dest.account_id, opts => opts.MapFrom(src => src.AccountId))
            //    .ForAllUnmappedMembers(o => o.Ignore());

            //Mapper.CreateMap<BillingCustomerPremiseModel, AccountLookupModel>()
            //   .ForMember(dest => dest.CustomerId, opts => opts.MapFrom(src => src.customer_id))
            //   .ForMember(dest => dest.MeterId, opts => opts.MapFrom(src => src.meter_id))
            //   .ForMember(dest => dest.ServiceContractId, opts => opts.MapFrom(src => src.Service_contract))
            //   .ForMember(dest => dest.AccountId, opts => opts.MapFrom(src => src.account_id))
            //   .ForAllUnmappedMembers(o => o.Ignore());
            //#endregion

            #region NccAmiReadingModel

            Mapper.CreateMap<NccAmiReadingModel, RawNccAmiReadingModel>()
                .ForMember(dest => dest.METER_ID, opts => opts.MapFrom(src => src.MeterId))
                .ForMember(dest => dest.TRANSPONDER_ID, opts => opts.MapFrom(src => src.TransponderId))
                .ForMember(dest => dest.TRANSPONDER_PORT, opts => opts.MapFrom(src => src.TransponderPort))
                .ForMember(dest => dest.CUSTOMER_ID, opts => opts.MapFrom(src => src.CustomerId))
                .ForMember(dest => dest.READING_VALUE, opts => opts.MapFrom(src => src.ReadingValue))
                .ForMember(dest => dest.UNIT_OF_MEASURE, opts => opts.MapFrom(src => src.UOMId))
                .ForMember(dest => dest.READING_DATETIME, opts => opts.MapFrom(src => src.AmiTimeStamp))
                .ForMember(dest => dest.TIMEZONE, opts => opts.MapFrom(src => src.Timezone))
                .ForMember(dest => dest.BATTERY_VOLTAGE, opts => opts.MapFrom(src => src.BatteryVoltage))
                .ForMember(dest => dest.COMMODITY_ID, opts => opts.MapFrom(src => src.CommodityId))
                .ForAllUnmappedMembers(o => o.Ignore());

            Mapper.CreateMap<RawNccAmiReadingModel, NccAmiReadingModel>()
                .ForMember(dest => dest.MeterId, opts => opts.MapFrom(src => src.METER_ID))
                .ForMember(dest => dest.TransponderId, opts => opts.MapFrom(src => src.TRANSPONDER_ID))
                .ForMember(dest => dest.TransponderPort, opts => opts.MapFrom(src => src.TRANSPONDER_PORT))
                .ForMember(dest => dest.CustomerId, opts => opts.MapFrom(src => src.CUSTOMER_ID))
                .ForMember(dest => dest.ReadingValue, opts => opts.MapFrom(src => src.READING_VALUE))
                .ForMember(dest => dest.UOMId, opts => opts.MapFrom(src => src.UNIT_OF_MEASURE))
                .ForMember(dest => dest.AmiTimeStamp, opts => opts.MapFrom(src => src.READING_DATETIME))
                .ForMember(dest => dest.Timezone, opts => opts.MapFrom(src => src.TIMEZONE))
                .ForMember(dest => dest.BatteryVoltage, opts => opts.MapFrom(src => src.BATTERY_VOLTAGE))
                .ForMember(dest => dest.CommodityId, opts => opts.MapFrom(src => src.COMMODITY_ID))
                .ForAllUnmappedMembers(o => o.Ignore());

            Mapper.CreateMap<RawBaseReadModel, NccAmiReadingModel>()
                .ForMember(dest => dest.MeterId, opts => opts.MapFrom(src => src.METER_ID))
                .ForMember(dest => dest.TransponderId, opts => opts.MapFrom(src => src.TRANSPONDER_ID))
                .ForMember(dest => dest.TransponderPort, opts => opts.MapFrom(src => src.TRANSPONDER_PORT))
                .ForMember(dest => dest.CustomerId, opts => opts.MapFrom(src => src.CUSTOMER_ID))
                .ForMember(dest => dest.ReadingValue, opts => opts.MapFrom(src => src.READING_VALUE))
                .ForMember(dest => dest.UOMId, opts => opts.MapFrom(src => src.UNIT_OF_MEASURE))
                .ForMember(dest => dest.AmiTimeStamp, opts => opts.MapFrom(src => src.READING_DATETIME))
                .ForMember(dest => dest.Timezone, opts => opts.MapFrom(src => src.TIMEZONE))
                .ForMember(dest => dest.BatteryVoltage, opts => opts.MapFrom(src => src.BATTERY_VOLTAGE))
                .ForMember(dest => dest.CommodityId, opts => opts.MapFrom(src => src.COMMODITY_ID))
                .ForAllUnmappedMembers(o => o.Ignore());

            #endregion

            //#region AmiReading

            //Mapper.CreateMap<NccAmiReadingModel, AmiReadingEntity>()
            //    .ForAllUnmappedMembers(o => o.Ignore());

            //Mapper.CreateMap<AmiReadingEntity, NccAmiReadingModel>()
            //    .ForAllUnmappedMembers(o => o.Ignore());

            //#endregion

            //#region AmiReading Internal

            //Mapper.CreateMap<AmiReading, TallAmiModel>()
            //   .ForAllUnmappedMembers(o => o.Ignore());

            //Mapper.CreateMap<TallAmiModel, Preprocessing.AmiReading>()
            //    .ForAllUnmappedMembers(o => o.Ignore());

            //Mapper.CreateMap<Preprocessing.AmiReading, NccAmiReadingModel>()
            //   .ForAllUnmappedMembers(o => o.Ignore());

            //Mapper.CreateMap<NccAmiReadingModel, Preprocessing.AmiReading>()
            //    .ForAllUnmappedMembers(o => o.Ignore());

            //Mapper.CreateMap<Preprocessing.AmiReading, Ami60MinuteIntervalModel>()
            //  .ForAllUnmappedMembers(o => o.Ignore());

            //Mapper.CreateMap<Ami60MinuteIntervalModel, Preprocessing.AmiReading>()
            //    .ForAllUnmappedMembers(o => o.Ignore());
            //#endregion

            //#region Billing

            //Mapper.CreateMap<BillingCustomerPremiseModel, RawBillingModel>()
            //    .ForMember(dest => dest.bill_enddate, opt => opt.NullSubstitute(null))
            //    .ForMember(dest => dest.service_read_date, opt => opt.NullSubstitute(null))
            //    .ForAllUnmappedMembers(o => o.Ignore());

            //Mapper.CreateMap<RawBillingModel, BillingCustomerPremiseModel>()
            //    .ForMember(dest => dest.bill_enddate, opt => opt.NullSubstitute(null))
            //    .ForMember(dest => dest.service_read_date, opt => opt.NullSubstitute(null))
            //    .ForAllUnmappedMembers(o => o.Ignore());

            //Mapper.CreateMap<BillingEntity, BillingModel>()
            //    .ForAllUnmappedMembers(o => o.Ignore());

            //Mapper.CreateMap<BillingModel, BillingEntity>()
            //    .ForAllUnmappedMembers(o => o.Ignore());

            //Mapper.CreateMap<BillingModel, BillingCustomerPremiseModel>()
            //    .ForMember(dest => dest.bill_enddate, opt => opt.NullSubstitute(null))
            //    .ForMember(dest => dest.service_read_date, opt => opt.NullSubstitute(null))
            //    .ForMember(dest => dest.account_id, opts => opts.MapFrom(src => src.AccountId))
            //    .ForMember(dest => dest.customer_id, opts => opts.MapFrom(src => src.CustomerId))
            //    .ForMember(dest => dest.premise_id, opts => opts.MapFrom(src => src.PremiseId))
            //    .ForMember(dest => dest.inactive_date, opts => opts.MapFrom(src => src.AmiEndDate))
            //    .ForMember(dest => dest.read_cycle, opts => opts.MapFrom(src => src.BillCycleScheduleId))
            //    .ForMember(dest => dest.service_point_id, opts => opts.MapFrom(src => src.ServicePointId))
            //    .ForMember(dest => dest.rate_code, opts => opts.MapFrom(src => src.RateClass))
            //    .ForMember(dest => dest.meter_type, opts => opts.MapFrom(src => src.MeterType))
            //    .ForMember(dest => dest.meter_units, opts => opts.MapFrom(src => src.UOMId))
            //    .ForMember(dest => dest.usage_value, opts => opts.MapFrom(src => src.TotalUsage))
            //    .ForMember(dest => dest.bill_enddate, opts => opts.MapFrom(src => src.EndDate))
            //    .ForMember(dest => dest.bill_days, opts => opts.MapFrom(src => src.BillDays))
            //    .ForMember(dest => dest.is_estimate, opts => opts.MapFrom(src => src.ReadQuality))
            //    .ForMember(dest => dest.usage_charge, opts => opts.MapFrom(src => src.TotalCost))
            //    .ForMember(dest => dest.service_commodity, opts => opts.MapFrom(src => src.CommodityId))
            //    .ForMember(dest => dest.meter_id, opts => opts.MapFrom(src => src.MeterId))
            //    .ForMember(dest => dest.billperiod_type, opts => opts.MapFrom(src => src.BillPeriodType))
            //    .ForMember(dest => dest.meter_replaces_meterid, opts => opts.MapFrom(src => src.ReplacedMeterId))
            //    .ForMember(dest => dest.service_read_date, opts => opts.MapFrom(src => src.ReadDate))
            //    .ForMember(dest => dest.Service_contract, opts => opts.MapFrom(src => src.ServiceContractId))
            //    .ForAllUnmappedMembers(o => o.Ignore());

            //Mapper.CreateMap<BillingCustomerPremiseModel, BillingModel>()
            //    .ForMember(dest => dest.AccountId, opts => opts.MapFrom(src => src.account_id))
            //    .ForMember(dest => dest.CustomerId, opts => opts.MapFrom(src => src.customer_id))
            //    .ForMember(dest => dest.PremiseId, opts => opts.MapFrom(src => src.premise_id))
            //    .ForMember(dest => dest.AmiEndDate, opts => opts.MapFrom(src => src.inactive_date))
            //    .ForMember(dest => dest.BillCycleScheduleId, opts => opts.MapFrom(src => src.read_cycle))
            //    .ForMember(dest => dest.ServicePointId, opts => opts.MapFrom(src => src.service_point_id))
            //    .ForMember(dest => dest.RateClass, opts => opts.MapFrom(src => src.rate_code))
            //    .ForMember(dest => dest.MeterType, opts => opts.MapFrom(src => src.meter_type))
            //    .ForMember(dest => dest.UOMId, opts => opts.MapFrom(src => src.meter_units))
            //    .ForMember(dest => dest.TotalUsage, opts => opts.MapFrom(src => src.usage_value))
            //    .ForMember(dest => dest.EndDate, opts => opts.MapFrom(src => src.bill_enddate))
            //    .ForMember(dest => dest.BillDays, opts => opts.MapFrom(src => src.bill_days))
            //    .ForMember(dest => dest.ReadQuality, opts => opts.MapFrom(src => src.is_estimate))
            //    .ForMember(dest => dest.TotalCost, opts => opts.MapFrom(src => src.usage_charge))
            //    .ForMember(dest => dest.CommodityId, opts => opts.MapFrom(src => src.service_commodity))
            //    .ForMember(dest => dest.MeterId, opts => opts.MapFrom(src => src.meter_id))
            //    .ForMember(dest => dest.BillPeriodType, opts => opts.MapFrom(src => src.billperiod_type))
            //    .ForMember(dest => dest.ReplacedMeterId, opts => opts.MapFrom(src => src.meter_replaces_meterid))
            //    .ForMember(dest => dest.ReadDate, opts => opts.MapFrom(src => src.service_read_date))
            //    .ForMember(dest => dest.ServiceContractId, opts => opts.MapFrom(src => src.Service_contract))
            //    .ForAllUnmappedMembers(o => o.Ignore());
            //#endregion

            //#region Customer

            //Mapper.CreateMap<CustomerEntity, CustomerModel>()
            //    .ForAllUnmappedMembers(o => o.Ignore());

            //Mapper.CreateMap<CustomerModel, CustomerEntity>()
            //    .ForAllUnmappedMembers(o => o.Ignore());

            //Mapper.CreateMap<CustomerModel, BillingCustomerPremiseModel>()
            //    .ForMember(dest => dest.customer_id, opts => opts.MapFrom(src => src.CustomerId))
            //    .ForMember(dest => dest.mail_address_line_1, opts => opts.MapFrom(src => src.AddressLine1))
            //    .ForMember(dest => dest.mail_address_line_2, opts => opts.MapFrom(src => src.AddressLine2))
            //    .ForMember(dest => dest.mail_address_line_3, opts => opts.MapFrom(src => src.AddressLine3))
            //    .ForMember(dest => dest.mail_city, opts => opts.MapFrom(src => src.City))
            //    .ForMember(dest => dest.mail_state, opts => opts.MapFrom(src => src.State))
            //    .ForMember(dest => dest.mail_zip_code, opts => opts.MapFrom(src => src.PostalCode))
            //    .ForMember(dest => dest.first_name, opts => opts.MapFrom(src => src.FirstName))
            //    .ForMember(dest => dest.last_name, opts => opts.MapFrom(src => src.LastName))
            //    .ForMember(dest => dest.phone_1, opts => opts.MapFrom(src => src.Phone1))
            //    .ForMember(dest => dest.phone_2, opts => opts.MapFrom(src => src.Phone2))
            //    .ForMember(dest => dest.email, opts => opts.MapFrom(src => src.EmailAddress))
            //    .ForMember(dest => dest.customer_type, opts => opts.MapFrom(src => src.CustomerType))
            //    .ForAllUnmappedMembers(o => o.Ignore());

            //Mapper.CreateMap<BillingCustomerPremiseModel, CustomerModel>()
            //   .ForMember(dest => dest.CustomerId, opts => opts.MapFrom(src => src.customer_id))
            //   .ForMember(dest => dest.AddressLine1, opts => opts.MapFrom(src => src.mail_address_line_1))
            //   .ForMember(dest => dest.AddressLine2, opts => opts.MapFrom(src => src.mail_address_line_2))
            //   .ForMember(dest => dest.AddressLine3, opts => opts.MapFrom(src => src.mail_address_line_3))
            //   .ForMember(dest => dest.City, opts => opts.MapFrom(src => src.mail_city))
            //   .ForMember(dest => dest.State, opts => opts.MapFrom(src => src.mail_state))
            //   .ForMember(dest => dest.PostalCode, opts => opts.MapFrom(src => src.mail_zip_code))
            //   .ForMember(dest => dest.FirstName, opts => opts.MapFrom(src => src.first_name))
            //   .ForMember(dest => dest.LastName, opts => opts.MapFrom(src => src.last_name))
            //   .ForMember(dest => dest.Phone1, opts => opts.MapFrom(src => src.phone_1))
            //   .ForMember(dest => dest.Phone2, opts => opts.MapFrom(src => src.phone_2))
            //   .ForMember(dest => dest.EmailAddress, opts => opts.MapFrom(src => src.email))
            //   .ForMember(dest => dest.CustomerType, opts => opts.MapFrom(src => src.customer_type))
            //   .ForAllUnmappedMembers(o => o.Ignore());
            //#endregion

            //#region ClientAccountEntity

            //Mapper.CreateMap<ClientAccountEntity, ClientAccountModel>()
            //    .ForAllUnmappedMembers(o => o.Ignore());

            //Mapper.CreateMap<ClientAccountModel, ClientAccountEntity>()
            //    .ForAllUnmappedMembers(o => o.Ignore());

            //Mapper.CreateMap<ClientAccountModel, BillingCustomerPremiseModel>()
            //    .ForMember(dest => dest.customer_id, opts => opts.MapFrom(src => src.CustomerId))
            //    .ForMember(dest => dest.ClientId, opts => opts.MapFrom(src => src.ClientId))
            //    .ForAllUnmappedMembers(o => o.Ignore());

            //Mapper.CreateMap<BillingCustomerPremiseModel, ClientAccountModel>()
            //   .ForMember(dest => dest.CustomerId, opts => opts.MapFrom(src => src.customer_id))
            //   .ForMember(dest => dest.AccountId, opts => opts.MapFrom(src => src.account_id))
            //   .ForMember(dest => dest.ClientId, opts => opts.MapFrom(src => src.ClientId))
            //   .ForAllUnmappedMembers(o => o.Ignore());

            //#endregion

            //#region Premise

            //Mapper.CreateMap<PremiseEntity, PremiseModel>()
            //    .ForAllUnmappedMembers(o => o.Ignore());

            //Mapper.CreateMap<PremiseModel, PremiseEntity>()
            //    .ForAllUnmappedMembers(o => o.Ignore());


            //Mapper.CreateMap<PremiseModel, BillingCustomerPremiseModel>()
            //   .ForMember(dest => dest.premise_id, opts => opts.MapFrom(src => src.PremiseId))
            //   .ForMember(dest => dest.customer_id, opts => opts.MapFrom(src => src.CustomerId))
            //   .ForMember(dest => dest.account_id, opts => opts.MapFrom(src => src.AccountId))
            //   .ForMember(dest => dest.site_addressline1, opts => opts.MapFrom(src => src.AddressLine1))
            //   .ForMember(dest => dest.site_addressline2, opts => opts.MapFrom(src => src.AddressLine2))
            //   .ForMember(dest => dest.site_addressline3, opts => opts.MapFrom(src => src.AddressLine3))
            //   .ForMember(dest => dest.site_city, opts => opts.MapFrom(src => src.City))
            //   .ForMember(dest => dest.site_state, opts => opts.MapFrom(src => src.State))
            //   .ForMember(dest => dest.site_zip_code, opts => opts.MapFrom(src => src.PostalCode))
            //   .ForAllUnmappedMembers(o => o.Ignore());

            //Mapper.CreateMap<BillingCustomerPremiseModel, PremiseModel>()
            //  .ForMember(dest => dest.PremiseId, opts => opts.MapFrom(src => src.premise_id))
            //  .ForMember(dest => dest.CustomerId, opts => opts.MapFrom(src => src.customer_id))
            //  .ForMember(dest => dest.AccountId, opts => opts.MapFrom(src => src.account_id))
            //  .ForMember(dest => dest.AddressLine1, opts => opts.MapFrom(src => src.site_addressline1))
            //  .ForMember(dest => dest.AddressLine2, opts => opts.MapFrom(src => src.site_addressline2))
            //  .ForMember(dest => dest.AddressLine3, opts => opts.MapFrom(src => src.site_addressline3))
            //  .ForMember(dest => dest.City, opts => opts.MapFrom(src => src.site_city))
            //  .ForMember(dest => dest.State, opts => opts.MapFrom(src => src.site_state))
            //  .ForMember(dest => dest.PostalCode, opts => opts.MapFrom(src => src.site_zip_code))
            //  .ForAllUnmappedMembers(o => o.Ignore());
            //#endregion

            //#region TallAmi

            //Mapper.CreateMap<TallAmiEntity, TallAmiModel>()
            //    .ForAllUnmappedMembers(o => o.Ignore());

            //Mapper.CreateMap<TallAmiModel, TallAmiEntity>()
            //    .ForAllUnmappedMembers(o => o.Ignore());

            //#endregion

            //#region Ami15MinuteInterval

            //Mapper.CreateMap<Preprocessing.AmiReading, Ami15MinuteIntervalModel>()
            //   .ForAllUnmappedMembers(o => o.Ignore());

            //Mapper.CreateMap<Ami15MinuteIntervalModel, Preprocessing.AmiReading>()
            //    .ForAllUnmappedMembers(o => o.Ignore());

            //Mapper.CreateMap<Ami15MinuteIntervalModel, Ami15MinuteIntervalModel>()
            //   .ForAllUnmappedMembers(o => o.Ignore());

            //Mapper.CreateMap<Ami15MinuteIntervalModel, RawAmi15MinuteIntervalModel>()
            //    .ForMember(dest => dest.TimeStamp, opts => opts.MapFrom(src => src.AmiTimeStamp))
            //    .ForAllUnmappedMembers(o => o.Ignore());

            //Mapper.CreateMap<RawAmi15MinuteIntervalModel, Ami15MinuteIntervalModel>()
            //    .ForMember(dest => dest.AmiTimeStamp, opts => opts.MapFrom(src => src.TimeStamp))
            //    .ForAllUnmappedMembers(o => o.Ignore());

            //Mapper.CreateMap<Ami15MinuteIntervalEntity, Ami15MinuteIntervalModel>()
            //    .ForAllUnmappedMembers(o => o.Ignore());

            //Mapper.CreateMap<Ami15MinuteIntervalModel, Ami15MinuteIntervalEntity>()
            //    .ForAllUnmappedMembers(o => o.Ignore());

            //Mapper.CreateMap<Ami15MinuteIntervalEntity, Ami30MinuteIntervalModel>()
            //    .ForAllUnmappedMembers(o => o.Ignore());

            //Mapper.CreateMap<Ami15MinuteIntervalEntity, Ami60MinuteIntervalModel>()
            //    .ForAllUnmappedMembers(o => o.Ignore());
            //#endregion

            //#region Ami30MinuteInterval

            //Mapper.CreateMap<Preprocessing.AmiReading, Ami30MinuteIntervalModel>()
            //  .ForAllUnmappedMembers(o => o.Ignore());

            //Mapper.CreateMap<Ami30MinuteIntervalModel, Preprocessing.AmiReading>()
            //    .ForAllUnmappedMembers(o => o.Ignore());

            //Mapper.CreateMap<Ami30MinuteIntervalModel, Ami30MinuteIntervalModel>()
            //   .ForAllUnmappedMembers(o => o.Ignore());

            //Mapper.CreateMap<Ami30MinuteIntervalModel, RawAmi30MinuteIntervalModel>()
            //   .ForMember(dest => dest.TimeStamp, opts => opts.MapFrom(src => src.AmiTimeStamp))
            //   .ForAllUnmappedMembers(o => o.Ignore());

            //Mapper.CreateMap<RawAmi30MinuteIntervalModel, Ami30MinuteIntervalModel>()
            //    .ForMember(dest => dest.AmiTimeStamp, opts => opts.MapFrom(src => src.TimeStamp))
            //    .ForAllUnmappedMembers(o => o.Ignore());

            //Mapper.CreateMap<Ami30MinuteIntervalEntity, Ami30MinuteIntervalModel>()
            //    .ForAllUnmappedMembers(o => o.Ignore());

            //Mapper.CreateMap<Ami30MinuteIntervalModel, Ami30MinuteIntervalEntity>()
            //    .ForAllUnmappedMembers(o => o.Ignore());
            //#endregion

            //#region Ami60MinuteInterval

            //Mapper.CreateMap<Preprocessing.AmiReading, Ami60MinuteIntervalModel>()
            //  .ForAllUnmappedMembers(o => o.Ignore());

            //Mapper.CreateMap<Ami60MinuteIntervalModel, Preprocessing.AmiReading>()
            //    .ForAllUnmappedMembers(o => o.Ignore());

            //Mapper.CreateMap<Ami60MinuteIntervalModel, Ami60MinuteIntervalModel>()
            //   .ForAllUnmappedMembers(o => o.Ignore());

            //Mapper.CreateMap<Ami60MinuteIntervalModel, RawAmi60MinuteIntervalModel>()
            //    .ForMember(dest => dest.TimeStamp, opts => opts.MapFrom(src => src.AmiTimeStamp))
            //    .ForAllUnmappedMembers(o => o.Ignore());

            //Mapper.CreateMap<RawAmi60MinuteIntervalModel, Ami60MinuteIntervalModel>()
            //    .ForMember(dest => dest.AmiTimeStamp, opts => opts.MapFrom(src => src.TimeStamp))
            //    .ForAllUnmappedMembers(o => o.Ignore());

            //Mapper.CreateMap<Ami60MinuteIntervalEntity, Ami60MinuteIntervalModel>()
            //    .ForAllUnmappedMembers(o => o.Ignore());

            //Mapper.CreateMap<Ami60MinuteIntervalModel, Ami60MinuteIntervalEntity>()
            //    .ForAllUnmappedMembers(o => o.Ignore());
            //#endregion

            //#region AmiDailyInterval

            //Mapper.CreateMap<Preprocessing.AmiReading, AmiDailyIntervalModel>()
            //  .ForAllUnmappedMembers(o => o.Ignore());

            //Mapper.CreateMap<AmiDailyIntervalModel, Preprocessing.AmiReading>()
            //    .ForAllUnmappedMembers(o => o.Ignore());

            //Mapper.CreateMap<AmiDailyIntervalModel, AmiDailyIntervalModel>()
            //   .ForAllUnmappedMembers(o => o.Ignore());

            //Mapper.CreateMap<AmiDailyIntervalModel, RawAmiDailyIntervalModel>()
            //    .ForMember(dest => dest.TimeStamp, opts => opts.MapFrom(src => src.AmiTimeStamp))
            //    .ForAllUnmappedMembers(o => o.Ignore());

            //Mapper.CreateMap<RawAmiDailyIntervalModel, AmiDailyIntervalModel>()
            //    .ForMember(dest => dest.AmiTimeStamp, opts => opts.MapFrom(src => src.TimeStamp))
            //    .ForAllUnmappedMembers(o => o.Ignore());

            //Mapper.CreateMap<AmiDailyIntervalEntity, AmiDailyIntervalModel>()
            //    .ForAllUnmappedMembers(o => o.Ignore());

            //Mapper.CreateMap<AmiDailyIntervalModel, AmiDailyIntervalEntity>()
            //    .ForAllUnmappedMembers(o => o.Ignore());
            //#endregion

            //#region AccountUpdates

            //Mapper.CreateMap<AccountUpdatesModel, RawAccountUpdatesModel>()
            //    .ForAllUnmappedMembers(o => o.Ignore());

            //Mapper.CreateMap<RawAccountUpdatesModel, AccountUpdatesModel>()

            //    .ForAllUnmappedMembers(o => o.Ignore());

            //Mapper.CreateMap<AccountUpdatesEntity, AccountUpdatesModel>()
            //    .ForAllUnmappedMembers(o => o.Ignore());

            //Mapper.CreateMap<AccountUpdatesModel, AccountUpdatesEntity>()
            //    .ForAllUnmappedMembers(o => o.Ignore());
            //#endregion

            //#region BillCycleSchedule

            //Mapper.CreateMap<BillCycleScheduleEntity, BillCycleScheduleModel>()
            //    .ForAllUnmappedMembers(o => o.Ignore());

            //Mapper.CreateMap<BillCycleScheduleModel, BillCycleScheduleEntity>()
            //    .ForAllUnmappedMembers(o => o.Ignore());
            //#endregion

            //#region Calculation

            //Mapper.CreateMap<CalculationEntity, CalculationModel>()
            //    .ForAllUnmappedMembers(o => o.Ignore());

            //Mapper.CreateMap<CalculationModel, CalculationEntity>()
            //    .ForAllUnmappedMembers(o => o.Ignore());
            //#endregion

            //#region Subscription

            //Mapper.CreateMap<SubscriptionModel, RawSubscriptionModel>()
            //    .ForAllUnmappedMembers(o => o.Ignore());

            //Mapper.CreateMap<RawSubscriptionModel, SubscriptionModel>()

            //    .ForAllUnmappedMembers(o => o.Ignore());

            //Mapper.CreateMap<SubscriptionEntity, SubscriptionModel>()
            //    .ForAllUnmappedMembers(o => o.Ignore());

            //Mapper.CreateMap<SubscriptionModel, SubscriptionEntity>()
            //    .ForAllUnmappedMembers(o => o.Ignore());
            //#endregion

            //#region Insight

            //Mapper.CreateMap<InsightEntity, InsightModel>()
            //    .ForAllUnmappedMembers(o => o.Ignore());

            //Mapper.CreateMap<InsightModel, InsightEntity>()
            //    .ForAllUnmappedMembers(o => o.Ignore());
            //#endregion

            //#region Notification

            //Mapper.CreateMap<NotificationEntity, NotificationModel>()
            //    .ForAllUnmappedMembers(o => o.Ignore());

            //Mapper.CreateMap<NotificationModel, NotificationEntity>()
            //    .ForAllUnmappedMembers(o => o.Ignore());
            //#endregion

            #region ClientWidgetConfigurationEntity

            Mapper.CreateMap<ClientWidgetConfigurationModel, ClientWidgetConfigurationEntity>()
                .ForAllUnmappedMembers(o => o.Ignore());

            Mapper.CreateMap<ClientWidgetConfigurationEntity, ClientWidgetConfigurationModel>()
                .ForAllUnmappedMembers(o => o.Ignore());

            #endregion

            //#region SmsTemplate

            //Mapper.CreateMap<SmsTemplateEntity, SmsTemplateModel>()
            //    .ForAllUnmappedMembers(o => o.Ignore());

            //Mapper.CreateMap<SmsTemplateModel, SmsTemplateEntity>()
            //    .ForAllUnmappedMembers(o => o.Ignore());
            //#endregion

            //#region CustomerMobileNumberLookup

            //Mapper.CreateMap<CustomerMobileNumberLookupEntity, CustomerMobileNumberLookupModel>()
            //    .ForAllUnmappedMembers(o => o.Ignore());

            //Mapper.CreateMap<CustomerMobileNumberLookupModel, CustomerMobileNumberLookupEntity>()
            //    .ForAllUnmappedMembers(o => o.Ignore());

            //Mapper.CreateMap<CustomerMobileNumberLookupModel, BillingCustomerPremiseModel>()
            //    .ForMember(dest => dest.customer_id, opts => opts.MapFrom(src => src.CustomerId))
            //    .ForMember(dest => dest.ClientId, opts => opts.MapFrom(src => src.ClientId))
            //    .ForMember(dest => dest.phone_1, opts => opts.MapFrom(src => src.MobileNumber))
            //    .ForMember(dest => dest.account_id, opts => opts.MapFrom(src => src.AccountId))
            //    .ForAllUnmappedMembers(o => o.Ignore());

            //Mapper.CreateMap<BillingCustomerPremiseModel, CustomerMobileNumberLookupModel>()
            //   .ForMember(dest => dest.CustomerId, opts => opts.MapFrom(src => src.customer_id))
            //   .ForMember(dest => dest.ClientId, opts => opts.MapFrom(src => src.ClientId))
            //   .ForMember(dest => dest.MobileNumber, opts => opts.MapFrom(src => src.phone_1))
            //   .ForMember(dest => dest.AccountId, opts => opts.MapFrom(src => src.account_id))
            //   .ForAllUnmappedMembers(o => o.Ignore());
            //#endregion

            //#region TrumpiaKeywordConfigurationEntity

            //Mapper.CreateMap<TrumpiaKeywordConfigurationEntity, TrumpiaKeywordConfigurationModel>()
            //    .ForAllUnmappedMembers(o => o.Ignore());

            //Mapper.CreateMap<TrumpiaKeywordConfigurationModel, TrumpiaKeywordConfigurationEntity>()
            //    .ForAllUnmappedMembers(o => o.Ignore());
            //#endregion

            //#region TrumpiaRequestDetail

            //Mapper.CreateMap<TrumpiaRequestDetailEntity, TrumpiaRequestDetailModel>()
            //    .ForAllUnmappedMembers(o => o.Ignore());

            //Mapper.CreateMap<TrumpiaRequestDetailModel, TrumpiaRequestDetailEntity>()
            //    .ForAllUnmappedMembers(o => o.Ignore());
            //#endregion

            //#region EmailRequestDetailModel

            //Mapper.CreateMap<EmailRequestDetailModel, EmailRequestDetailEntity>().ForAllUnmappedMembers(o => o.Ignore());
            //Mapper.CreateMap<EmailRequestDetailEntity, EmailRequestDetailModel>().ForAllUnmappedMembers(o => o.Ignore());

            //#endregion


            Mapper.AssertConfigurationIsValid();
        }

        public static void ForAllUnmappedMembers<TSource, TDestination>(this IMappingExpression<TSource, TDestination> mapping, Action<IMemberConfigurationExpression<TSource>> memberOptions)
        {
            var typeMap = Mapper.FindTypeMapFor<TSource, TDestination>();
            foreach (var memberName in typeMap.GetUnmappedPropertyNames())
                mapping.ForMember(memberName, memberOptions);
        }
    }
}
