﻿using CE.AO.Logging;
using CE.AO.Models;
using CE.BillToDate;
using CE.RateModel;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AO.BusinessContracts;

namespace CE.AO.Business
{
    public class NotificationFacade : INotificationFacade
    {
        public NotificationFacade(LogModel logModel)
        {
            LogModel = logModel;
        }

        public LogModel LogModel { get; set; }

        [Dependency]
        public INotification NotificationManager { get; set; }

        [Dependency]
        public IEvaluation EvaluationManager { get; set; }

        [Dependency]
        public ISubscription SubscriptionManager { get; set; }

        [Dependency]
        public ICustomer CustomerManager { get; set; }

        [Dependency]
        public IBilling BillingManager { get; set; }

        [Dependency]
        public ICalculation CalculationManager { get; set; }

        [Dependency]
        public Lazy<IClientConfigFacade> ClientConfigFacadeManager { get; set; }

        [Dependency]
        public Lazy<ICalculationManagerFactory> CalculationManagerFactory { get; set; }

        public void ProcessNotification(int clientId, string customerId, string accountId, string serviceContractId, string programName, Guid? asOfEvaluationId)
        {
            // Get data Subscription data for customer
            var subscriptions = SubscriptionManager.GetCustomerSubscriptions(clientId, customerId, accountId).ToList();
            if (subscriptions.Count > 0)
            {
                // Get data Evaluation data for customer
                var evaluations = string.IsNullOrEmpty(serviceContractId)
                    ? EvaluationManager.GetAccountLevelEvaluationsAsync(clientId, customerId, accountId, programName).Result
                    : EvaluationManager.GetServiceLevelEvaluationsAsync(clientId, customerId, accountId, programName,
                        serviceContractId).Result;

                if (asOfEvaluationId != null && evaluations.AsOfEvaluationId != asOfEvaluationId)
                    return;

                //var tasks = new List<Task<InsightModel>>
                //{
                //    EvaluationManager.GetAccountLevelEvaluationsAsync(clientId, accountId, programName),
                //    EvaluationManager.GetServiceLevelEvaluationsAsync(clientId, accountId, programName,
                //        serviceContractId)
                //};
                //Task.WhenAll(tasks);
                //var evaluations = tasks.Where(e => e.Result != null).Select(task => task.Result).ToList();
                var customerModel = CustomerManager.GetCustomerAsync(clientId, customerId).Result;

                var billingModel =
                    BillingManager.GetServiceDetailsFromBillAsync(clientId, customerId, accountId, serviceContractId)
                        .Result;

                var calculationTasks = new List<Task<CalculationModel>>
                {
                    CalculationManager.GetBtdCalculationAsync(clientId, accountId),
                    CalculationManager.GetCtdCalculationAsync(clientId, accountId, serviceContractId)
                };
                Task.WhenAll(calculationTasks);
                var calculations = calculationTasks.Where(e => e.Result != null).Select(task => task.Result).ToList();
                Dictionary<string, List<TierBoundary>> tierBoundariesWithServiceContractId =
                    calculations.Where(calculation => !string.IsNullOrEmpty(calculation.ServiceContractId))
                        .ToDictionary(calculation => calculation.ServiceContractId,
                            calculation => GetTierBoundries(calculation, clientId));

                //Get settings from Contentful
                var clientSettings = ClientConfigFacadeManager.Value.GetClientSettings(clientId);
                if (clientSettings == null)
                {
                    Logger.Warn("No settings found for notification.", LogModel);
                    return;
                }

                // sms settings
                var smsApiKey = clientSettings.TrumpiaApiKey;
                var smsUserName = clientSettings.TrumpiaUserName;
                var smsContactList = clientSettings.TrumpiaContactList;

                //var custSubscriptions = subscriptions.Where(s => s.CustomerId == customerModel.CustomerId).ToList();

                // create sms subscription if there is sms/emailandsms channel among the insights and no sms subscription id 
                if (
                    subscriptions.Exists(
                        s =>
                            !string.IsNullOrEmpty(s.Channel) && (s.Channel.Equals(Utilities.Enums.SubscriptionChannel.SMS.GetDescription(),
                                StringComparison.CurrentCultureIgnoreCase) ||
                            s.Channel.Equals(Utilities.Enums.SubscriptionChannel.EmailAndSMS.GetDescription(),
                                StringComparison.CurrentCultureIgnoreCase))))
                {
                    if (customerModel != null &&
                        string.IsNullOrEmpty(customerModel.TrumpiaSubscriptionId) &&
                        !string.IsNullOrEmpty(customerModel.Phone1))
                    {
                        CustomerManager.CreateSubscriptionAndUpdateCustomer(customerModel, smsApiKey, smsUserName, smsContactList);
                    }
                }

                NotificationManager.ProcessMessage(subscriptions, evaluations, clientSettings, customerModel,
                    billingModel, tierBoundariesWithServiceContractId, calculations).Wait();
            }
        }

        public List<TierBoundary> GetTierBoundries(CalculationModel calculationModel, int clientId)
        {
            //Get tire boundries 
            var settings = ClientConfigFacadeManager.Value.GetBillToDateSettings(clientId);
            var rateCompanyId = settings.RateCompanyId;
            var billToDateCalculator = CalculationManagerFactory.Value.CreateCalculationManager(settings);

            var rateClass = calculationModel.RateClass;
            var tierBoundaries = billToDateCalculator.GetTierBoundaries(rateCompanyId, rateClass,
                Convert.ToDateTime(calculationModel.BillCycleStartDate),
                Convert.ToDateTime(calculationModel.BillCycleEndDate));

            // Get BTD setting for conversion
            //Get tire boundries 
            double conversionFactor = 1;
            if (calculationModel.CommodityId == 2 && settings.Settings.General.UseConversionFactorForGas)
            {
                conversionFactor = settings.Settings.General.ConversionFactorForGas;
            }
            else if (calculationModel.CommodityId == 3 && settings.Settings.General.UseConversionFactorForWater)
                conversionFactor = settings.Settings.General.ConversionFactorForWater;
            if (tierBoundaries != null)
            {
                foreach (var tierBoundary in tierBoundaries)
                {
                    tierBoundary.Threshold = tierBoundary.Threshold / conversionFactor;
                }
            }
            return tierBoundaries;
        }
    }
}
