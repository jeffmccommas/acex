﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using AO.BusinessContracts;
using AutoMapper;
using CE.AO.DataAccess;
using CE.AO.Entities;
using CE.AO.Entities.TableStorageEntities;
using CE.AO.Models;
using CE.RateModel;
using Microsoft.Practices.Unity;

namespace CE.AO.Business
{
    // ReSharper disable once InconsistentNaming
    public class TallAMI : ITallAMI
    {
        public TallAMI(LogModel logModel, ITableRepository tableRepository)
        {
            LogModel = logModel;
            TableRepository = tableRepository;
        }

        public LogModel LogModel { get; set; }

        [Dependency]
        public ITableRepository TableRepository { get; set; }

        public async Task<bool> InsertAmiAsync(TallAmiModel model)
        {
            var entityModel = Mapper.Map<TallAmiModel, TallAmiEntity>(model);
            entityModel.PartitionKey = TallAmiEntity.SetPartitionKey(model.ClientId, model.MeterId, model.AmiTimeStamp);
            entityModel.RowKey = TallAmiEntity.SetRowKey(model.AmiTimeStamp);
            return await TableRepository.InsertAsync(entityModel, Constants.TableNames.TallAmi);
        }

        public async Task<bool> InsertOrMergeAmiAsync(TallAmiModel model)
        {
            var entityModel = Mapper.Map<TallAmiModel, TallAmiEntity>(model);
            entityModel.PartitionKey = TallAmiEntity.SetPartitionKey(model.ClientId, model.MeterId, model.AmiTimeStamp);
            entityModel.RowKey = TallAmiEntity.SetRowKey(model.AmiTimeStamp);
            return await TableRepository.InsertOrMergeSingleAsync(Constants.TableNames.TallAmi, entityModel);
        }

        public async Task<bool> InsertOrMergeBatchAsync(List<TallAmiModel> tallAmiModels)
        {
            var entities = new List<TallAmiEntity>();
            foreach (var model in tallAmiModels)
            {
                var entityModel = Mapper.Map<TallAmiModel, TallAmiEntity>(model);
                entityModel.PartitionKey = TallAmiEntity.SetPartitionKey(model.ClientId, model.MeterId, model.AmiTimeStamp);
                entityModel.RowKey = TallAmiEntity.SetRowKey(model.AmiTimeStamp);
                entities.Add(entityModel);
            }
            return await TableRepository.InsertOrMergeBatchAsync(entities, Constants.TableNames.TallAmi);
        }

        /// <summary>
        /// Not Implemented
        /// </summary>
        /// <param name="date"></param>
        /// <param name="meterId"></param>
        /// <param name="accountId"></param>
        /// <param name="clientId"></param>
        /// <returns></returns>
        public IEnumerable<TallAmiModel> GetAmi(DateTime date, string meterId, string accountId, int clientId)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Not Implemented
        /// </summary>
        /// <param name="startDate"></param>
        /// <param name="endDate"></param>
        /// <param name="meterId"></param>
        /// <param name="clientId"></param>
        /// <returns></returns>
        public IEnumerable<TallAmiModel> GetAmiList(DateTime startDate, DateTime endDate, string meterId, int clientId)
        {           
            throw new NotImplementedException();
        }

        public IEnumerable<TallAmiModel> GetAmiList(DateTime startDate, DateTime endDate, string meterId, string accountId, int clientId)
        {
            throw new NotImplementedException();
        }

        public List<Reading> GetAmiReadings(int clientId, string meterId, ref DateTime startDate,ref DateTime endDate, string requestedResolution,
            out int intervalType, out int amiUomId, out int amiCommodityId)
        {
            throw new NotImplementedException();
        }

        public List<Reading> GetReadings(IEnumerable<TallAmiModel> amiList, out int noOfDays, bool convertToLocalTimeZone = true)
        {
            throw new NotImplementedException();
        }


        public List<ReadingModel> GetReadings(IEnumerable<TallAmiModel> amiList)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Not Implemented
        /// </summary>
        /// <param name="clientId"></param>
        /// <param name="meterId"></param>
        /// <returns></returns>
        public Task<bool> DeleteReadingAsync(int clientId, string meterId)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Not Implemented
        /// </summary>
        /// <param name="tallAmiModels"></param>
        /// <returns></returns>
        public Task<bool> InsertOrMergeDailyAmiAsync(List<TallAmiModel> tallAmiModels)
        {
            throw new NotImplementedException();
        }
    }
}
