﻿using CE.AO.DataAccess;
using CE.AO.Models;
using AutoMapper;
using CE.AO.Entities.TableStorageEntities;
using System.Threading.Tasks;
using AO.BusinessContracts;
using CE.AO.Entities;
using System;

namespace CE.AO.Business
{
    public class Calculation : ICalculation
    {
        public Calculation(LogModel logModel, ITableRepository tableRepository)
        {
            LogModel = logModel;
            TableRepository = tableRepository;
        }

        public LogModel LogModel { get; set; }
       
        public ITableRepository TableRepository { get; set; }

        public async Task InsertCtdCalculationAsync(CalculationModel model)
        {
            var entityModel = Mapper.Map<CalculationModel, CalculationEntity>(model);
            entityModel.PartitionKey = CalculationEntity.SetPartitionKey(entityModel.ClientId, entityModel.AccountId);
            entityModel.RowKey = CalculationEntity.SetCtdRowKey(entityModel.ServiceContractId, false);
            await TableRepository.InsertOrMergeSingleAsync(Constants.TableNames.Calculation, entityModel);

            //insert History Record
            // TODO: Change this to use Customers Timezone
            var date = model.AsOfCalculationDate;
            entityModel.RowKey = CalculationEntity.SetCtdRowKey(entityModel.ServiceContractId, true, date);

            await TableRepository.InsertOrMergeSingleAsync(Constants.TableNames.Calculation, entityModel);
        }

        public async Task InsertBtdCalculationAsync(CalculationModel model)
        {
            var entityModel = Mapper.Map<CalculationModel, CalculationEntity>(model);
            entityModel.PartitionKey = CalculationEntity.SetPartitionKey(entityModel.ClientId, entityModel.AccountId);
            entityModel.RowKey = CalculationEntity.SetBtdRowKey(false);
            await TableRepository.InsertOrMergeSingleAsync(Constants.TableNames.Calculation, entityModel);

            //insert History Record
            // TODO: Change this to use Customers Timezone
            var date = model.AsOfCalculationDate;
            entityModel.RowKey = CalculationEntity.SetBtdRowKey(true, date);

            await TableRepository.InsertOrMergeSingleAsync(Constants.TableNames.Calculation, entityModel);
        }

        public async Task<CalculationModel> GetBtdCalculationAsync(int clientId, string accountId)
        {
            var btd =  await TableRepository.GetSingleAsync<CalculationEntity>(Constants.TableNames.Calculation,
                CalculationEntity.SetPartitionKey(clientId, accountId), CalculationEntity.SetBtdRowKey(false));
            return Mapper.Map<CalculationEntity, CalculationModel>(btd);
        }

        public async Task<CalculationModel> GetCtdCalculationAsync(int clientId, string accountId, string serviceContractId)
        {
            var ctd = await TableRepository.GetSingleAsync<CalculationEntity>(Constants.TableNames.Calculation,
                CalculationEntity.SetPartitionKey(clientId, accountId),
                CalculationEntity.SetCtdRowKey(serviceContractId, false));
            return Mapper.Map<CalculationEntity, CalculationModel>(ctd);
        }

        /// <summary>
        /// Delete Ctd Data
        /// </summary>
        /// <param name="clientId"></param>
        /// <param name="accountId"></param>
        /// <returns></returns>
        public Task DeleteCtdCalculationAsyc(int clientId, string accountId)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Delete Btd Data
        /// </summary>
        /// <param name="clientId"></param>
        /// <param name="accountId"></param>
        /// <returns></returns>
        public Task DeleteBtdCalculationAsyc(int clientId, string accountId)
        {
            throw new NotImplementedException();
        }
    }
}
