﻿using System;
using System.Linq;
using AO.BusinessContracts;
using AutoMapper;
using CE.AO.DataAccess;
using CE.AO.Entities.TableStorageEntities;
using CE.AO.Models;
using Microsoft.WindowsAzure.Storage.Table;
using CE.AO.Entities;
using System.Threading.Tasks;

namespace CE.AO.Business
{
    public class BillingCycleSchedule : IBillingCycleSchedule
    {
       
        public BillingCycleSchedule(LogModel logModel, ITableRepository tableRepository)
        {
            LogModel = logModel;
            TableRepository = tableRepository;
        }

        public LogModel LogModel { get; set; }
        
        public ITableRepository TableRepository { get; set; }

        public Task<bool> DeleteHistoryBillingCycleScheduleAsyc(string clientId, string billCycleScheduleId, string beginDate, string endDate)
        {
            throw new NotImplementedException();
        }

        public BillCycleScheduleModel GetBillingCycleScheduleByDateAsync(int clientId, string billCycleId,
            DateTime date)
        {

            //var filter = TableQuery.CombineFilters(
            //    TableQuery.GenerateFilterCondition("PartitionKey", QueryComparisons.Equal, BillCycleScheduleEntity.SetPartitionKey(clientId, billCycleId)),
            //    TableOperators.And,
            //    TableQuery.CombineFilters(
            //        TableQuery.GenerateFilterCondition("RowKey", QueryComparisons.GreaterThanOrEqual,
            //            date.ToString("yyyy-MM-dd")),
            //        TableOperators.And,
            //        TableQuery.GenerateFilterCondition("RowKey", QueryComparisons.LessThan,
            //            date.AddMonths(1).ToString("yyyy-MM-dd")))
            //    // Add 1 month to current date to filter as we don’t have Bill days but roughly max days in bill cycle will be 30
            //    );
            var filter = TableQuery.CombineFilters(
                TableQuery.GenerateFilterCondition("PartitionKey", QueryComparisons.Equal, BillCycleScheduleEntity.SetPartitionKey(clientId, billCycleId)),
                TableOperators.And,
                TableQuery.GenerateFilterCondition("RowKey", QueryComparisons.GreaterThanOrEqual,
                        date.ToString("yyyy-MM-dd"))
                );
            var billingCycleSchedules =
                TableRepository.GetAllAsync<BillCycleScheduleEntity>(Constants.TableNames.BillCycleSchedule, filter);
            var billingCycleSchedule =
                billingCycleSchedules.FirstOrDefault(
                    b => Convert.ToDateTime(b.BeginDate) <= date.Date && date.Date <= Convert.ToDateTime(b.EndDate));

            return Mapper.Map<BillCycleScheduleEntity, BillCycleScheduleModel>(billingCycleSchedule);

        }

        public Task<bool> InsertOrMergeBillingCycleScheduleAsync(BillCycleScheduleModel billCycleScheduleModel)
        {
            throw new NotImplementedException();
        }
    }
}
