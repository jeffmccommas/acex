﻿using CE.AO.DataAccess;
using System;
using System.Threading.Tasks;
using AO.BusinessContracts;

namespace CE.AO.Business
{
    public class AzureQueue : IAzureQueue
    {
        public AzureQueue(Lazy<IQueueRepository> queueRepository)
        {
            QueueRepository = queueRepository;
        }

        private Lazy<IQueueRepository> QueueRepository { get; }

        public async Task AddMessageToBatchQueue(string message)
        {
            await QueueRepository.Value.AddQueueAsync(Entities.Constants.QueueNames.AoImportBatchProcessQueue, message);
        }

        public async Task AddMessageToCalculationQueue(string message)
        {
            await QueueRepository.Value.AddQueueAsync(Entities.Constants.QueueNames.AoCalculationQueue, message);
        }
    }
}
