﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AO.BusinessContracts;
using AutoMapper;
using CE.AO.Logging;
using Microsoft.Practices.Unity;
using CE.AO.Entities.TableStorageEntities;
using CE.AO.Models;
using Enums = CE.AO.Utilities.Enums;

namespace CE.AO.Business
{
    public class AccountUpdatesFacade : IAccountUpdatesFacade
    {
        public AccountUpdatesFacade(LogModel logModel)
        {
            LogModel = logModel;
        }

        public LogModel LogModel { get; set; }

        [Dependency]
        public IAccountUpdates AccountUpdatesManager { get; set; }
        [Dependency]
        public IBilling BillingManager { get; set; }
        [Dependency]
        public ICustomer CustomerManager { get; set; }
        [Dependency]
        public IAccountLookup AccountLookupManager { get; set; }

        public async Task<bool> InsertOrMergeAccountUpdateAsync(AccountUpdatesModel model)
        {
            var accountUpdatesEntity = Mapper.Map<AccountUpdatesModel, AccountUpdatesEntity>(model);

            var tasks = new List<Task<bool>>();
            var accountUpdateTypeEnum = (Enums.AccountUpdateType)Enum.Parse(typeof(Enums.AccountUpdateType), Convert.ToString(accountUpdatesEntity.UpdateType));

            tasks.Add(AccountUpdatesManager.InsertOrMergeAccountUpdateAsync(model, false));
            tasks.Add(AccountUpdatesManager.InsertOrMergeAccountUpdateAsync(model, true));

            switch (accountUpdateTypeEnum)
            {
                case Enums.AccountUpdateType.RateClass:
                    tasks.Add(InsertorMergeForRateClassAsync(accountUpdatesEntity));
                    break;
                case Enums.AccountUpdateType.Email:
                    tasks.Add(InsertorMergeForEmailClassAsync(accountUpdatesEntity));
                    break;
                case Enums.AccountUpdateType.Phone1:
                    tasks.Add(InsertorMergeForPhone1ClassAsync(accountUpdatesEntity));
                    break;
                case Enums.AccountUpdateType.MeterReplacement:
                    tasks.Add(InsertorMergeForMeterReplacementClassAsync(accountUpdatesEntity));
                    break;
                case Enums.AccountUpdateType.ReadStartDate:
                case Enums.AccountUpdateType.ReadEndDate:
                    //Do Nothing
                    break;
            }

            var taskResults = await Task.WhenAll(tasks);
            return !taskResults.Any(result => false);
        }

        public Task<bool> InsertOrMergeAccountUpdateAsync(int clientId, string customerId, string accountId, string premiseId,
            string serviceContractId, string meterId, string newValue, string replacedValue, string accountUpdateType)
        {
            throw new NotImplementedException();
        }

        private async Task<bool> InsertorMergeForRateClassAsync(AccountUpdatesEntity accountUpdatesEntityModel)
        {
            var isSuccess = false;
            var billingModel = await BillingManager.GetServiceDetailsFromBillAsync(accountUpdatesEntityModel.ClientId,
                accountUpdatesEntityModel.CustomerId, accountUpdatesEntityModel.AccountId,
                accountUpdatesEntityModel.ServiceContractId);

            if (billingModel == null)
            {
                Logger.Fatal($@"ServiceContractId ""{accountUpdatesEntityModel.ServiceContractId}"" not found while updating {accountUpdatesEntityModel.UpdateType}.", LogModel);
            }
            else
            {
                billingModel.RateClass = accountUpdatesEntityModel.NewValue;
                billingModel.IsFault = true;
                isSuccess = await BillingManager.InsertOrMergeLatestBillAsync(billingModel);
            }

            return isSuccess;
        }

        private async Task<bool> InsertorMergeForEmailClassAsync(AccountUpdatesEntity accountUpdatesEntityModel)
        {
            var isSuccess = false;
            var customerModel = await CustomerManager.GetCustomerAsync(accountUpdatesEntityModel.ClientId, accountUpdatesEntityModel.CustomerId);

            if (customerModel == null)
            {
                Logger.Fatal($@"Customer ""{accountUpdatesEntityModel.CustomerId}"" not found while updating {accountUpdatesEntityModel.UpdateType}.", LogModel);
            }
            else
            {
                customerModel.EmailAddress = accountUpdatesEntityModel.NewValue;
                isSuccess = await CustomerManager.InsertOrMergeCustomerAsync(customerModel);
            }
            return isSuccess;
        }

        private async Task<bool> InsertorMergeForPhone1ClassAsync(AccountUpdatesEntity accountUpdatesEntityModel)
        {
            var isSuccess = false;
            var customerModel = await CustomerManager.GetCustomerAsync(accountUpdatesEntityModel.ClientId, accountUpdatesEntityModel.CustomerId);

            if (customerModel == null)
            {
                Logger.Fatal($@"Customer ""{accountUpdatesEntityModel.CustomerId}"" not found while updating {accountUpdatesEntityModel.UpdateType}.", LogModel);
            }
            else
            {
                customerModel.Phone1 = accountUpdatesEntityModel.NewValue;
                isSuccess = await CustomerManager.InsertOrMergeCustomerAsync(customerModel);
            }
            return isSuccess;
        }

        private async Task<bool> InsertorMergeForMeterReplacementClassAsync(AccountUpdatesEntity accountUpdatesEntityModel)
        {
            var isSuccess = false;
            var billingEntity = await BillingManager.GetServiceDetailsFromBillAsync(accountUpdatesEntityModel.ClientId,
                accountUpdatesEntityModel.CustomerId, accountUpdatesEntityModel.AccountId,
                accountUpdatesEntityModel.ServiceContractId);

            if (billingEntity == null)
            {
                Logger.Fatal($@"ServiceContractId ""{accountUpdatesEntityModel.ServiceContractId}"" not found while updating {accountUpdatesEntityModel.UpdateType}.", LogModel);
            }
            else
            {
                billingEntity.MeterId = accountUpdatesEntityModel.NewValue;
                billingEntity.ReplacedMeterId = accountUpdatesEntityModel.OldValue;
                billingEntity.IsFault = true;
                isSuccess = await BillingManager.InsertOrMergeLatestBillAsync(billingEntity);

                var accountLookupEntity = await AccountLookupManager.GetAccountMeterDetails(billingEntity.ClientId,
                    billingEntity.ReplacedMeterId, billingEntity.CustomerId);
                if (accountLookupEntity != null)
                {
                    accountLookupEntity.IsActive = false;
                    await AccountLookupManager.InsertOrMergeAccountLookupAsync(accountLookupEntity);

                    accountLookupEntity.IsActive = true;
                    accountLookupEntity.MeterId = accountUpdatesEntityModel.NewValue;
                    if (accountLookupEntity.IsServiceContractCreatedBySystem)
                        accountLookupEntity.ServiceContractId = BillingEntity.GetServiceContractId(accountUpdatesEntityModel.MeterId,
                            accountUpdatesEntityModel.AccountId, accountUpdatesEntityModel.PremiseId, billingEntity.CommodityId);
                    else
                        accountLookupEntity.ServiceContractId = billingEntity.ServiceContractId;

                    isSuccess = await AccountLookupManager.InsertOrMergeAccountLookupAsync(accountLookupEntity);
                    return isSuccess;
                }
            }
            return isSuccess;
        }
    }
}