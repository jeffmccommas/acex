﻿using System;
using System.Collections.Generic;
using CE.AO.Entities.TableStorageEntities;
using System.Globalization;
using System.Linq;
using Microsoft.WindowsAzure.Storage.Table;

namespace UnitTests.MockData
{
    public class MockTables
    {
        //Generate List for following tables
        // Billing, BillCycleSchedule,  Ami , CustomerPremiseProfile, Calculation, Insight, , AccountMeterLookup, AccountUpdates, Subscription, ClientAccount

        public int ClientId;
        public DateTime CurrentDate;

        public List<BillCycleScheduleEntity> BillCycleSchedule { get; set; }
        public List<AccountLookupEntity> AccountMeterLookup { get; set; }
        public List<DynamicTableEntity> Ami { get; set; }
        public List<Ami15MinuteIntervalEntity> AmiEntity { get; set; }
        public List<ClientAccountEntity> ClientAccount { get; set; }
        public List<BillingEntity> Billing { get; set; }
        public List<AccountUpdatesEntity> AccountUpdates { get; set; }
        public List<SubscriptionEntity> Subscription { get; set; }
        public List<CustomerEntity> CustomerPremiseProfile { get; set; }
        public List<CalculationEntity> Calculation { get; set; }
        public List<InsightEntity> Insight { get; set; }
        public List<PremiseEntity> Premise { get; set; }
        public List<TallAmiEntity> TallAmi { get; set; }
        public List<NotificationEntity> Notification { get; set; }
        public List<SmsTemplateEntity> SmsTemplate { get; set; }
        public List<TrumpiaKeywordConfigurationEntity> TrumpiaKeywordConfiguration { get; set; }

        private static MockTables _mockTables;
        public static MockTables GetInstance()
        {
            return _mockTables ?? (_mockTables = new MockTables());
        }

        private MockTables()
        {
            ClientId = 87;
            CurrentDate = DateTime.UtcNow;
            FillBillCycleScheduleEntity();
            FillBillingEntity();
            FillAccountLookupEntity();
            FillClientAccountEntity();
            FillAccountUpdatesEntity();
            FillCustomerEntity();
            FillSubscriptionEntity();
            FillAmi15MinuteIntervalEntity();
            FillCalculationEntity();
            FillInsightEntity();
            FillPremiseEntity();
            FillTallAmiEntity();
            FillNotificationEntity();
            FillSmsTemplateEntity();
            FillTrumpiaKeywordConfigurationEntity();
        }

        public void FillBillCycleScheduleEntity()
        {
            BillCycleSchedule = new List<BillCycleScheduleEntity>
            {
                new BillCycleScheduleEntity()
                {
                    BeginDate =
                        (new DateTime(CurrentDate.Year, CurrentDate.AddMonths(-1).Month, 1)).ToString("MM-dd-yyyy"),
                BillCycleScheduleName = "Cycle01",
                ClientId = ClientId.ToString(),
                    Description =
                        "Cycle01-" + CurrentDate.AddMonths(-1).Month.ToString("MMM", CultureInfo.InvariantCulture),
                    EndDate =
                        (new DateTime(CurrentDate.Year, CurrentDate.AddMonths(-1).Month,
                            DateTime.DaysInMonth(CurrentDate.Year, CurrentDate.AddMonths(-1).Month))).ToString(
                                "MM-dd-yyyy"),
                ETag = "",
                PartitionKey = ClientId + "_Cycle01",
                    RowKey =
                        (new DateTime(CurrentDate.Year, CurrentDate.AddMonths(-1).Month,
                            DateTime.DaysInMonth(CurrentDate.Year, CurrentDate.AddMonths(-1).Month))).ToString(
                                "yyyy-MM-dd")
                },
                new BillCycleScheduleEntity()
            {
                    BeginDate =
                        (new DateTime(CurrentDate.Year, CurrentDate.AddMonths(0).Month, 1)).ToString("MM-dd-yyyy"),
                BillCycleScheduleName = "Cycle01",
                ClientId = ClientId.ToString(),
                    Description =
                        "Cycle01-" + CurrentDate.AddMonths(0).Month.ToString("MMM", CultureInfo.InvariantCulture),
                    EndDate =
                        (new DateTime(CurrentDate.Year, CurrentDate.AddMonths(0).Month,
                            DateTime.DaysInMonth(CurrentDate.Year, CurrentDate.AddMonths(0).Month))).ToString(
                                "MM-dd-yyyy"),
                ETag = "",
                PartitionKey = ClientId + "_Cycle01",
                    RowKey =
                        (new DateTime(CurrentDate.Year, CurrentDate.AddMonths(0).Month,
                            DateTime.DaysInMonth(CurrentDate.Year, CurrentDate.AddMonths(0).Month))).ToString(
                                "yyyy-MM-dd")
                },
                new BillCycleScheduleEntity()
            {
                    BeginDate =
                        (new DateTime(CurrentDate.Year, CurrentDate.AddMonths(1).Month, 1)).ToString("MM-dd-yyyy"),
                BillCycleScheduleName = "Cycle01",
                ClientId = ClientId.ToString(),
                    Description =
                        "Cycle01-" + CurrentDate.AddMonths(1).Month.ToString("MMM", CultureInfo.InvariantCulture),
                    EndDate =
                        (new DateTime(CurrentDate.Year, CurrentDate.AddMonths(1).Month,
                            DateTime.DaysInMonth(CurrentDate.Year, CurrentDate.AddMonths(1).Month))).ToString(
                                "MM-dd-yyyy"),
                ETag = "",
                PartitionKey = ClientId + "_Cycle01",
                    RowKey =
                        (new DateTime(CurrentDate.Year, CurrentDate.AddMonths(1).Month,
                            DateTime.DaysInMonth(CurrentDate.Year, CurrentDate.AddMonths(1).Month))).ToString(
                                "yyyy-MM-dd")
                },
                new BillCycleScheduleEntity()
            {
                    BeginDate =
                        (new DateTime(CurrentDate.Year, CurrentDate.AddMonths(2).Month, 1)).ToString("MM-dd-yyyy"),
                BillCycleScheduleName = "Cycle01",
                ClientId = ClientId.ToString(),
                    Description =
                        "Cycle01-" + CurrentDate.AddMonths(2).Month.ToString("MMM", CultureInfo.InvariantCulture),
                    EndDate =
                        (new DateTime(CurrentDate.Year, CurrentDate.AddMonths(2).Month,
                            DateTime.DaysInMonth(CurrentDate.Year, CurrentDate.AddMonths(2).Month))).ToString(
                                "MM-dd-yyyy"),
                ETag = "",
                PartitionKey = ClientId + "_Cycle01",
                    RowKey =
                        (new DateTime(CurrentDate.Year, CurrentDate.AddMonths(2).Month,
                            DateTime.DaysInMonth(CurrentDate.Year, CurrentDate.AddMonths(2).Month))).ToString(
                                "yyyy-MM-dd")
                },
                new BillCycleScheduleEntity()
            {
                    BeginDate =
                        (new DateTime(CurrentDate.Year, CurrentDate.AddMonths(3).Month, 1)).ToString("MM-dd-yyyy"),
                BillCycleScheduleName = "Cycle01",
                ClientId = ClientId.ToString(),
                    Description =
                        "Cycle01-" + CurrentDate.AddMonths(3).Month.ToString("MMM", CultureInfo.InvariantCulture),
                    EndDate =
                        (new DateTime(CurrentDate.Year, CurrentDate.AddMonths(3).Month,
                            DateTime.DaysInMonth(CurrentDate.Year, CurrentDate.AddMonths(3).Month))).ToString(
                                "MM-dd-yyyy"),
                ETag = "",
                PartitionKey = ClientId + "_Cycle01",
                    RowKey =
                        (new DateTime(CurrentDate.Year, CurrentDate.AddMonths(3).Month,
                            DateTime.DaysInMonth(CurrentDate.Year, CurrentDate.AddMonths(3).Month))).ToString(
                                "yyyy-MM-dd")
                }
            };
        }

        public void FillBillingEntity()
        {
            Billing = new List<BillingEntity>
            {
                new BillingEntity()
                {
                    PartitionKey = "87_1000_a1".ToLower(),
                    RowKey = "B_sc1",
                    AccountId = "a1",
                    AmiEndDate = CurrentDate,
                    AmiStartDate = CurrentDate,
                    BillCycleScheduleId = "Cycle01",
                    BillDays = DateTime.DaysInMonth(CurrentDate.Year, CurrentDate.Month),
                    BillPeriodType = 1,
                    ClientId = ClientId,
                    CommodityId = 1,
                    CustomerId = "1000",
                    EndDate = CurrentDate.AddDays(-5),
                    MeterId = "m1",
                    MeterType = "AMI",
                    PremiseId = "p1",
                    RateClass = "binTierM",
                    ReadDate = CurrentDate.AddDays(-5),
                    ReadQuality = "Estimated",
                    ReplacedMeterId = "",
                    ServiceContractId = "sc1",
                    ServicePointId = "sp1",
                    Source = "Utility",
                    StartDate = CurrentDate.AddDays(-10),
                    TotalCost = 1000,
                    TotalUsage = 12,
                    UOMId = 1,
                    IsFault = false
                },
                new BillingEntity()
                {
                    PartitionKey = "87_1000_a1".ToLower(),
                    RowKey = "B_sc2",
                    AccountId = "a1",
                    AmiEndDate = CurrentDate,
                    AmiStartDate = CurrentDate,
                    BillCycleScheduleId = "Cycle01",
                    BillDays = DateTime.DaysInMonth(CurrentDate.Year, CurrentDate.Month),
                    BillPeriodType = 1,
                    ClientId = ClientId,
                    CommodityId = 1,
                    CustomerId = "1000",
                    EndDate = CurrentDate.AddDays(-5),
                    MeterId = "m2",
                    MeterType = "AMI",
                    PremiseId = "p1",
                    RateClass = "binTierM",
                    ReadDate = CurrentDate.AddDays(-5),
                    ReadQuality = "Estimated",
                    ReplacedMeterId = "",
                    ServiceContractId = "sc2",
                    ServicePointId = "sp2",
                    Source = "Utility",
                    StartDate = CurrentDate.AddDays(-10),
                    TotalCost = 1000,
                    TotalUsage = 12,
                    UOMId = 1,
                    IsFault = false
                },
                new BillingEntity()
                {
                    PartitionKey = "87_1000_a1".ToLower(),
                    RowKey = "H_sc3_" + CurrentDate.ToString("yyyy-MM-dd"),
                    AccountId = "a1",
                    AmiEndDate = CurrentDate,
                    AmiStartDate = CurrentDate,
                    BillCycleScheduleId = "Cycle01",
                    BillDays = DateTime.DaysInMonth(CurrentDate.Year, CurrentDate.Month),
                    BillPeriodType = 1,
                    ClientId = ClientId,
                    CommodityId = 1,
                    CustomerId = "1000",
                    EndDate = CurrentDate.AddDays(-5),
                    MeterId = "m3",
                    MeterType = "AMI",
                    PremiseId = "p1",
                    RateClass = "binTierM",
                    ReadDate = CurrentDate.AddDays(-5),
                    ReadQuality = "Estimated",
                    ReplacedMeterId = "",
                    ServiceContractId = "sc3",
                    ServicePointId = "sp3",
                    Source = "Utility",
                    StartDate = CurrentDate.AddDays(-10),
                    TotalCost = 1000,
                    TotalUsage = 12,
                    UOMId = 1,
                    IsFault = false
                },
                new BillingEntity()
                {
                    PartitionKey = "87_1000_a1".ToLower(),
                    RowKey = "B_sc5",
                    AccountId = "a1",
                    AmiEndDate = CurrentDate,
                    AmiStartDate = CurrentDate,
                    BillCycleScheduleId = "Cycle01",
                    BillDays = DateTime.DaysInMonth(CurrentDate.Year, CurrentDate.Month),
                    BillPeriodType = 1,
                    ClientId = ClientId,
                    CommodityId = 1,
                    CustomerId = "1000",
                    EndDate = CurrentDate.AddDays(-5),
                    MeterId = "m5",
                    MeterType = "AMI",
                    PremiseId = "p1",
                    RateClass = "binTierM",
                    ReadDate = CurrentDate.AddDays(-5),
                    ReadQuality = "Estimated",
                    ReplacedMeterId = "",
                    ServiceContractId = "sc5",
                    ServicePointId = "sp5",
                    Source = "Utility",
                    StartDate = CurrentDate.AddDays(-10),
                    TotalCost = 1000,
                    TotalUsage = 12,
                    UOMId = 1,
                    IsFault = false
                }
            };
        }

        public void FillAccountLookupEntity()
        {
            AccountMeterLookup = new List<AccountLookupEntity>
            {
                new AccountLookupEntity()
                {
                    PartitionKey = "87_m1".ToLower(),
                    RowKey = "1000",
                    AccountId = "a1",
                    ClientId = ClientId,
                    CustomerId = "1000",
                    IsServiceContractCreatedBySystem = false,
                    MeterId = "m1",
                    ServiceContractId = "sc1"
                },
                new AccountLookupEntity()
                {
                    PartitionKey = "87_m2".ToLower(),
                    RowKey = "1000",
                    AccountId = "a1",
                    ClientId = ClientId,
                    CustomerId = "1000",
                    IsServiceContractCreatedBySystem = false,
                    MeterId = "m2",
                    ServiceContractId = "sc2"
                },
                new AccountLookupEntity()
                {
                    PartitionKey = "87_m3".ToLower(),
                    RowKey = "1000",
                    AccountId = "a1",
                    ClientId = ClientId,
                    CustomerId = "1000",
                    IsServiceContractCreatedBySystem = false,
                    MeterId = "m3",
                    ServiceContractId = "sc3"
                },
                new AccountLookupEntity()
                {
                    PartitionKey = "87_m5".ToLower(),
                    RowKey = "1000",
                    AccountId = "a1",
                    ClientId = ClientId,
                    CustomerId = "1000",
                    IsServiceContractCreatedBySystem = false,
                    MeterId = "m5",
                    ServiceContractId = "sc5"
                }
            };
        }

        public void FillClientAccountEntity()
        {
            ClientAccount = new List<ClientAccountEntity>
            {
                new ClientAccountEntity()
                {
                    PartitionKey = ClientId.ToString(),
                    RowKey = "a1",
                    AccountId = "a1",
                    ClientId = Convert.ToInt32(ClientId),
                    CustomerId = "1000",
                    ETag = ""
                }
            };

        }

        public void FillAccountUpdatesEntity()
        {
            AccountUpdates = new List<AccountUpdatesEntity>
            {
                new AccountUpdatesEntity()
                {
                    PartitionKey = ClientId + "_1000",
                    RowKey = "BillCycle_S_sc1",
                    AccountId = "a1",
                    ClientId = Convert.ToInt32(ClientId),
                    CustomerId = "1000",
                    MeterId = "m1",
                    OldValue = "",
                    NewValue = "",
                    PremiseId = "p1",
                    ServiceContractId = "sc1",
                    UpdateSentDate = CurrentDate,
                    UpdateType = "BillCycleChanged"
                },
                new AccountUpdatesEntity()
                {
                    PartitionKey = ClientId + "_1000",
                    RowKey = "MeterReplacement_S_sc1",
                    AccountId = "a1",
                    ClientId = Convert.ToInt32(ClientId),
                    CustomerId = "1000",
                    MeterId = "m2",
                    OldValue = "",
                    NewValue = "",
                    PremiseId = "p2",
                    ServiceContractId = "sc2",
                    UpdateSentDate = CurrentDate,
                    UpdateType = "MeterReplacement"
                }
            };
        }

        public void FillCustomerEntity()
        {
            CustomerPremiseProfile = new List<CustomerEntity>
            {
                new CustomerEntity()
                {
                    PartitionKey = ClientId + "_1000",
                    RowKey = "C",
                    AddressLine1 = "",
                    AddressLine2 = "",
                    AddressLine3 = "",
                    City = "",
                    ClientId = ClientId,
                    Country = "US",
                    CustomerId = "1000",
                    CustomerType = "Residential",
                    EmailAddress = "",
                    FirstName = "Anjana",
                    IsBusiness = false,
                    LastName = "Khaire1",
                    Phone1 = "",
                    Phone2 = "",
                    PostalCode = "9075",
                    Source = "Utility",
                    State = "Aundh"
                }
            };
        }

        public void FillSubscriptionEntity()
        {
            Subscription = new List<SubscriptionEntity>
            {
                new SubscriptionEntity()
                {
                    PartitionKey = "87_1000_a1",
                    RowKey = "A_Program 1",
                    AccountId = "a1",
                    Channel = "Email",
                    ClientId = ClientId,
                    CustomerId = "1000",
                    InsightTypeName = "",
                    IsSelected = false,
                    OptOutDate = CurrentDate,
                    ProgramName = "Program 1",
                    ServiceContractId = "sc1",
                    SubscriptionDate = CurrentDate,
                    SubscriberThresholdValue = 10
                },
                new SubscriptionEntity()
                {
                    PartitionKey = "87_7007_a1",
                    RowKey = "S_Program 2_sc2_CostToDate",
                    AccountId = "a1",
                    Channel = "Email",
                    ClientId = ClientId,
                    CustomerId = "7007",
                    InsightTypeName = "CostToDate",
                    IsSelected = false,
                    OptOutDate = CurrentDate,
                    ProgramName = "Program 2",
                    ServiceContractId = "sc2",
                    SubscriptionDate = CurrentDate,
                    SubscriberThresholdValue = 10
                }
            };
        }

        public void FillAmi15MinuteIntervalEntity()
        {
            Ami = new List<DynamicTableEntity>();
            AmiEntity = new List<Ami15MinuteIntervalEntity>();
            DateTime tempDate = CurrentDate.AddDays(-2);

            while (tempDate < CurrentDate.AddMonths(1))
            {
                Ami15MinuteIntervalEntity obj = new Ami15MinuteIntervalEntity
                {
                    PartitionKey = "87_m1",
                    RowKey = tempDate.ToString("yyyy-MM-dd"),
                    ClientId = ClientId,
                    AccountNumber = "a1",
                    MeterId = "m1",
                    ServicePointId = "sp1",
                    CommodityId = 1,
                    UOMId = 3,
                    VolumeFactor = 3,
                    Direction = 1,
                    ProjectedReadDate = CurrentDate.AddMonths(1),
                    Timezone = "EST",
                    AmiTimeStamp = tempDate,
                    IntervalType = 15
                };
                Random rnd = new Random();
                var properties = obj.GetType().GetProperties();

                properties.Where(p => p.Name.Contains("IntValue")).ToList()
                    .ForEach(reflect =>
                    {
                        reflect.SetValue(obj, rnd.NextDouble());
                    });

                DynamicTableEntity dynamicTableEntity = new DynamicTableEntity(obj.PartitionKey, obj.RowKey);
                foreach (var prop in obj.GetType().GetProperties())
                {
                    var value = prop.GetValue(obj);
                    if (prop.PropertyType.FullName.ToLower().Contains("int32"))
                        dynamicTableEntity.Properties.Add(prop.Name, new EntityProperty(Convert.ToInt32(value)));
                    else if (prop.PropertyType.FullName.ToLower().Contains("string"))
                        dynamicTableEntity.Properties.Add(prop.Name, new EntityProperty(Convert.ToString(value)));
                    else if (prop.PropertyType.FullName.ToLower().Contains("dateTime"))
                        dynamicTableEntity.Properties.Add(prop.Name, new EntityProperty(Convert.ToDateTime(value)));
                    else if (prop.PropertyType.FullName.ToLower().Contains("double"))
                        dynamicTableEntity.Properties.Add(prop.Name, new EntityProperty(Convert.ToDouble(value)));
                }
                Ami.Add(dynamicTableEntity);
                AmiEntity.Add(obj);
                tempDate = tempDate.AddDays(1);
            }

            // m5
            tempDate = CurrentDate.AddDays(-5);
            while (tempDate < CurrentDate.AddMonths(1))
            {
                Ami15MinuteIntervalEntity obj = new Ami15MinuteIntervalEntity
                {
                    PartitionKey = "87_m5",
                    RowKey = tempDate.ToString("yyyy-MM-dd"),
                    ClientId = ClientId,
                    AccountNumber = "a1",
                    MeterId = "m5",
                    ServicePointId = "sp5",
                    CommodityId = 1,
                    UOMId = 3,
                    VolumeFactor = 3,
                    Direction = 1,
                    ProjectedReadDate = CurrentDate.AddMonths(1),
                    Timezone = "EST",
                    AmiTimeStamp = tempDate,
                    IntervalType = 15
                };
                Random rnd = new Random();
                var properties = obj.GetType().GetProperties();

                properties.Where(p => p.Name.Contains("IntValue")).ToList()
                    .ForEach(reflect =>
                    {
                        reflect.SetValue(obj, rnd.NextDouble());
                    });

                DynamicTableEntity dynamicTableEntity = new DynamicTableEntity(obj.PartitionKey, obj.RowKey);
                foreach (var prop in obj.GetType().GetProperties())
                {
                    var value = prop.GetValue(obj);
                    if (prop.PropertyType.FullName.ToLower().Contains("int32"))
                        dynamicTableEntity.Properties.Add(prop.Name, new EntityProperty(Convert.ToInt32(value)));
                    else if (prop.PropertyType.FullName.ToLower().Contains("string"))
                        dynamicTableEntity.Properties.Add(prop.Name, new EntityProperty(Convert.ToString(value)));
                    else if (prop.PropertyType.FullName.ToLower().Contains("dateTime"))
                        dynamicTableEntity.Properties.Add(prop.Name, new EntityProperty(Convert.ToDateTime(value)));
                    else if (prop.PropertyType.FullName.ToLower().Contains("double"))
                        dynamicTableEntity.Properties.Add(prop.Name, new EntityProperty(Convert.ToDouble(value)));
                }
                Ami.Add(dynamicTableEntity);
                AmiEntity.Add(obj);
                tempDate = tempDate.AddDays(1);
            }
        }

        public void FillCalculationEntity()
        {
            Calculation = new List<CalculationEntity>
            {
                new CalculationEntity()
                {
                    PartitionKey = "87_EM15M1a1p1s1-RCB6-76".ToLower(),
                    RowKey = $"C_{"EM15M1a1p1s1-RCB6-76_premQAInttest_09182015121003_0_2_a1-RCB6-76".ToLower()}",
                    AccountId = "EM15M1a1p1s1-RCB6-76",
                    AsOfAmiDate = DateTime.Now.AddHours(-5),
                    AsOfCalculationDate = DateTime.Now.AddHours(-5),
                    AverageDailyUsage = 12444.399,
                    AverageDailyCost = 414.92439596296,
                    ClientId = ClientId,
                    CustomerId = "1000",
                    MeterId = "a1-RCB6-76",
                    ServiceContractId = "EM15M1a1p1s1-RCB6-76_premQAInttest_09182015121003_0_2_a1-RCB6-76",
                    CommodityId = 0,
                    Unit = 1,
                    Cost = 25.45,
                    ProjectedCost = 13297.76586,
                    ProjectedUsage = 14748.9173333333,
                    Usage = 12444.399,
                    BillDays = 27,
                    BillCycleStartDate = DateTime.Now.AddDays(-15),
                    BillCycleEndDate = DateTime.Now.AddDays(15)
                },
                 new CalculationEntity()
                {
                    PartitionKey = "87_EM15M1a1p1s1-RCB6-76".ToLower(),
                    RowKey = "B",
                    AccountId = "EM15M1a1p1s1-RCB6-76",
                    AsOfAmiDate = DateTime.Now.AddHours(-5),
                    AsOfCalculationDate = DateTime.Now.AddHours(-5),
                    AverageDailyUsage = 0,
                    AverageDailyCost = 414.92439596296,
                    ClientId = ClientId,
                    CustomerId = "1000",
                    MeterId = "a1-RCB6-76",
                    ServiceContractId = "EM15M1a1p1s1-RCB6-76_premQAInttest_09182015121003_0_2_a1-RCB6-76",
                    CommodityId = 0,
                    Unit = 1,
                    Cost = 50.45,
                    ProjectedCost = 13297.76586,
                    ProjectedUsage = 14748.9173333333,
                    Usage = 12444.399,
                    BillDays = 27,
                    BillCycleStartDate = DateTime.Now.AddDays(-15),
                    BillCycleEndDate = DateTime.Now.AddDays(15)
                }
            };
        }

        public List<InsightEntity> FillInsightEntity()
        {
           return Insight = new List<InsightEntity>
            {
                new InsightEntity()
                {
                    PartitionKey = "87_a1",
                    RowKey = "A_Program1",
                    AccountId = "a1",
                    AccountLevelCostThresholdExceed = false,
                    ClientId = ClientId,
                    DayThresholdExceed = false,
                    ProgramName = "Program1",
                    ServiceContractId = "sp1",
                    ServiceLevelTieredThresholdExceed = false,
                    BtdCost = 12.2,
                    BtdProjectedCost = 12.2,
                    ServiceLevelCostThresholdExceed = false,
                    ServiceLevelUsageThresholdExceed = false,
                    Usage = 12.50,
                    CtdCost = 12.50,
                    CtdProjectedCost = 12.50
                },
                new InsightEntity()
                {
                    PartitionKey = "87_EM15M1a1p1s1-RCB6-72".ToLower(),
                    RowKey =
                        $"S_Program1_{"EM15M1a1p1s1-RCB6-72_premQAInttest_09182015121003_4_2_a1-RCB6-72".ToLower()}",
                    AccountId = "EM15M1a1p1s1-RCB6-72",
                    AccountLevelCostThresholdExceed = false,
                    ClientId = ClientId,
                    DayThresholdExceed = false,
                    ProgramName = "Program1",
                    ServiceContractId = "EM15M1a1p1s1-RCB6-72_premQAInttest_09182015121003_4_2_a1-RCB6-72",
                    ServiceLevelTieredThresholdExceed = false,
                    BtdCost = 128.20,
                    BtdProjectedCost = 128.20,
                    ServiceLevelCostThresholdExceed = false,
                    ServiceLevelUsageThresholdExceed = false,
                    Usage = 128.20,
                    CtdCost = 128.20,
                    CtdProjectedCost = 128.20
                },
                new InsightEntity()
                {
                    PartitionKey = "87_a1",
                    RowKey = $"{CurrentDate.ToString("yyyy-MM-dd")}_A_Program1",
                    AccountId = "a1",
                    AccountLevelCostThresholdExceed = false,
                    ClientId = ClientId,
                    DayThresholdExceed = false,
                    ProgramName = "Program1",
                    ServiceContractId = "",
                    ServiceLevelTieredThresholdExceed = false,
                    BtdCost = 12.2,
                    BtdProjectedCost = 12.2,
                    ServiceLevelCostThresholdExceed = false,
                    ServiceLevelUsageThresholdExceed = false,
                    Usage = 12.50,
                    CtdCost = 12.50,
                    CtdProjectedCost = 12.50
                }
            };
        }

        public void FillPremiseEntity()
        {
            Premise = new List<PremiseEntity>
            {
                new PremiseEntity()
                {
                    PartitionKey = "87_1000",
                    RowKey = "P_700",
                    AccountId = "Acc-RCB6-76",
                    ClientId = ClientId,
                    CustomerId = "1000",
                    PremiseId = "700"
                },
                 new PremiseEntity()
                {
                    PartitionKey = "87_1000",
                    RowKey = "P_800",
                    AccountId = "Acc-RCB6-76",
                    ClientId = ClientId,
                    CustomerId = "1000",
                    PremiseId = "700"
                }
            };
        }

        public void FillTallAmiEntity()
        {
            TallAmi = new List<TallAmiEntity>
            {
                new TallAmiEntity()
                {
                    PartitionKey = "87_m1_" + CurrentDate.ToString("yyyy-MM-dd"),
                    RowKey = CurrentDate.Hour.ToString().PadLeft(2, '0') + ":" + CurrentDate.Minute.ToString().PadLeft(2, '0'),
                    ClientId = ClientId,
                    MeterId = "m1",
                    CommodityId = 11,
                    UOMId = 22,
                    AmiTimeStamp = CurrentDate,
                    ServicePointId = "sp1"
                },
                new TallAmiEntity()
                {
                    PartitionKey = "87_m2" + CurrentDate.ToString("yyyy-MM-dd"),
                    RowKey = CurrentDate.Hour.ToString().PadLeft(2, '0') + ":" + CurrentDate.Minute.ToString().PadLeft(2, '0'),
                    ClientId = ClientId,
                    MeterId = "m2",
                    CommodityId = 44,
                    UOMId = 33,
                    AmiTimeStamp = CurrentDate,
                    ServicePointId = "sp2"
                }
            };
        }

        public void FillNotificationEntity()
        {
            Notification = new List<NotificationEntity>
            {
                new NotificationEntity()
                {
                    PartitionKey = "87_1000",
                    RowKey = "Program 1_BillToDate_Email",
                    AccountId = "a1",
                    ClientId = ClientId,
                    CustomerId = "1000",
                    ProgramName= "Program 1",
                    Insight = "BillToDate",
                    Channel = "Email",
                    TemplateId = "0b4beb88-b659-4921-86b0-9c61f6369c41",
                    ServiceContractId = "",
                    IsNotified = null
                },
                 new NotificationEntity()
                {
                    PartitionKey = "87_1000",
                    RowKey = "Program 1_DayThreshold_Email",
                    AccountId = "a1",
                    ClientId = ClientId,
                    CustomerId = "1000",
                    ProgramName= "Program 1",
                    Insight = "DayThreshold",
                    Channel = "Email",
                    TemplateId = "1e794f68-6dd7-4be5-b93b-da1bf17a46b1",
                    ServiceContractId = "sc1",
                    IsNotified = null
                },
                 new NotificationEntity()
                {
                    PartitionKey = "87_1000",
                    RowKey = CurrentDate.ToString("yyyy-MM-dd") + "_Program 2_DayThreshold_Email",
                    AccountId = "a1",
                    ClientId = ClientId,
                    CustomerId = "1000",
                    ProgramName= "Program 2",
                    Insight = "DayThreshold",
                    Channel = "Email",
                    TemplateId = "1e794f68-6dd7-4be5-b93b-da1bf17a46b1",
                    ServiceContractId = "sc1",
                    IsNotified = null
                }
            };
        }

        public void FillSmsTemplateEntity()
        {
            SmsTemplate = new List<SmsTemplateEntity>
            {
                new SmsTemplateEntity()
                {
                    PartitionKey = "87_AccountProjectedCost",
                    RowKey = "87_3",
                    ClientId = ClientId,
                    TemplateId = "87_3",
                    InsightTypeName = "AccountProjectedCost",
                    Body = @"Bill-to-date estimate is $-BillToDate- for account ending -AccountNumber-. Projected bill is $-ProjectedCost-. -NoOfDaysRemaining- days remain in bill cycle."
                },
                 new SmsTemplateEntity()
                {
                    PartitionKey = "87_Usage",
                    RowKey = "87_5",
                    ClientId = ClientId,
                    TemplateId = "87_5",
                    InsightTypeName = "Usage",
                    Body = @"Current usage -Usage- -UOM- for -FUEL- meter for account -AccountNumber-. Cost-to-date estimate is $-CostToDate-."
                }
            };
        }

        public void FillTrumpiaKeywordConfigurationEntity()
        {
            TrumpiaKeywordConfiguration = new List<TrumpiaKeywordConfigurationEntity>
            {
            };
        }
    }
}
