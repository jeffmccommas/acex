﻿using System;
using AutoMapper;
using CE.AO.Business;
using CE.AO.DataAccess;
using CE.AO.Entities.TableStorageEntities;
using CE.AO.Models;
using Microsoft.Practices.Unity;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.WindowsAzure.Storage.Table;
using Moq;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace UnitTests
{
    /// <summary>
    /// Contains test methods for PremiseTest Class
    /// </summary>
    /// <seealso cref="System.IDisposable" />
    [TestClass]
    public class PremiseTest : IDisposable
    {
        /// <summary>
        /// The _unity container
        /// </summary>
        private UnityContainer _unityContainer;

        /// <summary>
        /// The _premise manager
        /// </summary>
        private Premise _premiseManager;

        /// <summary>
        /// The _table repository mock
        /// </summary>
        private Mock<ITableRepository> _tableRepositoryMock;

        /// <summary>
        /// The _disposed
        /// </summary>
        private bool _disposed;

        /// <summary>
        /// Initializes this instance.
        /// </summary>
        [TestInitialize]
        public void Init()
        {
            AutoMapperConfig.AutoMapperMappings();
            _tableRepositoryMock = new Mock<ITableRepository>();
            _unityContainer = new UnityContainer();
            RegisterTypesForUnity.RegisterTypes(_unityContainer);
            _premiseManager = _unityContainer.Resolve<Premise>();
            _premiseManager.TableRepository = _tableRepositoryMock.Object;
        }

        /// <summary>
        /// Test case for accessesing the log model
        /// </summary>
        [TestMethod]
        public void AccessLogModel()
        {
            _premiseManager.LogModel.DisableLog = true;
            Assert.AreEqual(true, _premiseManager.LogModel.DisableLog);
        }

        /// <summary>
        /// Test Case for default constructor of Premise model
        /// </summary>
        [TestMethod]
        public void Premise()
        {
            // ReSharper disable once ObjectCreationAsStatement
            new Premise();
            Assert.AreEqual(true, true);
        }

        /// <summary>
        /// Test case for inserts the or merge premise asynchronous.
        /// </summary>
        [TestMethod]
        public void InsertOrMergePremiseAsync()
        {
            var premiseModel = GetSamplePremiseModel();
            _tableRepositoryMock.Setup(t => t.InsertOrMergeSingleAsync(It.IsAny<string>(), It.IsAny<PremiseEntity>())).Returns(Task.FromResult(true));
            var result = _premiseManager.InsertOrMergePremiseAsync(premiseModel).Result;
            _tableRepositoryMock.Verify(t => t.InsertOrMergeSingleAsync(It.IsAny<string>(), It.IsAny<PremiseEntity>()), Times.Once);
            _tableRepositoryMock.VerifyAll();
            Assert.AreEqual(true, result);
        }

        /// <summary>
        /// Test case for gets the premises.
        /// </summary>
        [TestMethod]
        public void GetPremises()
        {
            IList<PremiseEntity> list = new List<PremiseEntity>();
            var premiseEntity = Mapper.Map<PremiseModel, PremiseEntity>(GetSamplePremiseModel());
            list.Add(premiseEntity);
            var filter = TableQuery.CombineFilters(TableQuery.GenerateFilterCondition("PartitionKey", QueryComparisons.Equal, PremiseEntity.SetPartitionKey(87, "1012")),TableOperators.And,TableQuery.CombineFilters(
                    TableQuery.GenerateFilterCondition("RowKey", QueryComparisons.GreaterThanOrEqual, "P"),TableOperators.And,TableQuery.GenerateFilterCondition("RowKey", QueryComparisons.LessThan, "Q")));
            _tableRepositoryMock.Setup(t =>t.GetAllAsync<PremiseEntity>(It.IsAny<string>(), filter)).Returns(list).Verifiable();
            List<PremiseModel> result = _premiseManager.GetPremises(87, "1012").ToList();
            _tableRepositoryMock.Verify(t => t.GetAllAsync<PremiseEntity>(It.IsAny<string>(), filter), Times.Once);
            _tableRepositoryMock.VerifyAll();
            Assert.AreEqual(result.Count, 1);
        }

        /// <summary>
        /// Gets the sample premise model.
        /// </summary>
        /// <returns></returns>
        private static PremiseModel GetSamplePremiseModel()
        {
            return new PremiseModel()
            {
                AddressLine1 = "",
                AddressLine2 = "",
                AddressLine3 = "",
                City = "",
                ClientId = 87,
                Country = "US",
                CustomerId = "1012",
                PostalCode = "9041",
                State = "NJ",
                PremiseId = "1234",
                AccountId = "a5"
            };
        }

        /// <summary>
        /// Releases unmanaged and - optionally - managed resources.
        /// </summary>
        /// <param name="disposing"><c>true</c> to release both managed and unmanaged resources; <c>false</c> to release only unmanaged resources.</param>
        private void Dispose(bool disposing)
        {
            if (!_disposed)
            {
                if (disposing)
                {
                    _unityContainer.Dispose();
                }
            }
            _disposed = true;
        }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            Dispose(true);
            // ReSharper disable once GCSuppressFinalizeForTypeWithoutDestructor
            GC.SuppressFinalize(this);
        }
    }
}
