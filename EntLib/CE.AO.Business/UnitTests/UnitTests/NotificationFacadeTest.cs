﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using CE.AO.Business;
using CE.AO.Business.Contract;
using CE.AO.Logging;
using CE.AO.Models;
using CE.RateModel;
using Microsoft.Practices.Unity;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace UnitTests
{
    /// <summary>
    /// Contains test methods for NotificationFacade Class
    /// </summary>
    /// <seealso cref="System.IDisposable" />
    [TestClass]
    public class NotificationFacadeTest : IDisposable
    {
        /// <summary>
        /// The _unity container
        /// </summary>
        private UnityContainer _unityContainer;

        /// <summary>
        /// The _notification facade manager
        /// </summary>
        private NotificationFacade _notificationFacadeManager;

        /// <summary>
        /// The _subscription mock
        /// </summary>
        private Mock<ISubscription> _subscriptionMock;

        /// <summary>
        /// The _evaluation mock
        /// </summary>
        private Mock<IEvaluation> _evaluationMock;

        /// <summary>
        /// The _customer mock
        /// </summary>
        private Mock<ICustomer> _customerMock;

        /// <summary>
        /// The _billing mock
        /// </summary>
        private Mock<IBilling> _billingMock;

        /// <summary>
        /// The _calculation mock
        /// </summary>
        private Mock<ICalculation> _calculationMock;

        /// <summary>
        /// The _notification mock
        /// </summary>
        private Mock<INotification> _notificationMock;

        /// <summary>
        /// The _disposed
        /// </summary>
        private bool _disposed;

        /// <summary>
        /// Test Case Init
        /// </summary>
        [TestInitialize]
        public void Init()
        {
            _unityContainer = new UnityContainer();
            RegisterTypesForUnity.RegisterTypes(_unityContainer);
            var overrides = new ParameterOverrides
            {
                {"logModel", new LogModel {DisableLog = true} }
            };
            _notificationFacadeManager = _unityContainer.Resolve<NotificationFacade>(overrides);
            _evaluationMock = new Mock<IEvaluation>();
            _subscriptionMock = new Mock<ISubscription>();
            _evaluationMock = new Mock<IEvaluation>();
            _customerMock = new Mock<ICustomer>();
            _billingMock = new Mock<IBilling>();
            _calculationMock = new Mock<ICalculation>();
            _notificationMock = new Mock<INotification>();
            _notificationFacadeManager.EvaluationManager = _evaluationMock.Object;
            _notificationFacadeManager.SubscriptionManager = _subscriptionMock.Object;
            _notificationFacadeManager.EvaluationManager = _evaluationMock.Object;
            _notificationFacadeManager.CustomerManager = _customerMock.Object;
            _notificationFacadeManager.BillingManager = _billingMock.Object;
            _notificationFacadeManager.CalculationManager = _calculationMock.Object;
            _notificationFacadeManager.NotificationManager = _notificationMock.Object;
        }

        /// <summary>
        /// Test Case for default constructor of NotificationFacade model
        /// </summary>
        [TestMethod]
        public void NotificationFacade()
        {
            // ReSharper disable once ObjectCreationAsStatement
            new NotificationFacade();
            Assert.AreEqual(true, true);
        }

        /// <summary>
        /// Test Case for accessing Log Model
        /// </summary>
        [TestMethod]
        public void AccessLogModel()
        {
            _notificationFacadeManager.LogModel.DisableLog = true;
            Assert.AreEqual(true, _notificationFacadeManager.LogModel.DisableLog);
        }

        /// <summary>
        /// Test case for Notification insertion and mail
        /// </summary>
        [TestMethod]
        public void ProcessNotification()
        {
            var subscriptionModel = new SubscriptionModel()
            {
                ClientId = 1,
                CustomerId = "1234",
                AccountId = "AC0001",
                ProgramName = "Rate Class",
                InsightTypeName = "",
                PremiseId = "",
                ServiceContractId = "SC001",
                ServicePointId = "",
                IsEmailOptInCompleted = false,
                IsSmsOptInCompleted = true,
                IsSelected = false,
                Channel = "SMS",
                AllowedChannels = "",
                SubscriberThresholdValue = 1234,
                CommodityKey = "Water",
                ThresholdType = "",
                ThresholdMin = 132,
                ThresholdMax = 136,
                IsDoubleOptIn = true,
                UomId = 12
            };
            IList<SubscriptionModel> subscriptionModelList = new List<SubscriptionModel>();
            subscriptionModelList.Add(subscriptionModel);
            _subscriptionMock.Setup(t => t.GetCustomerSubscriptions(It.IsAny<int>(), It.IsAny<string>(), It.IsAny<string>())).Returns(subscriptionModelList).Verifiable();

            var insightModel = new InsightModel()
            {
                ClientId = 1,
                AccountId = "1234",
                ServiceContractId = "SC001",
                BilllDays = 23,
                BtdCost = 2332,
                BtdProjectedCost = 3544,
                Usage = 32,
                SupressInsight = true,
                AccountLevelCostThresholdExceed = true,
                ServiceLevelUsageThresholdExceed = true,
                ServiceLevelCostThresholdExceed = true,
                DayThresholdExceed = true,
                ServiceLevelTieredThresholdExceed = false,
                ServiceDays = 32,
                CtdCost = 4545,
                CtdProjectedCost = 5678,
                NotifyTime = "21 12 2016",
                ProgramName = "Rate Class",
                AsOfEvaluationDate = DateTime.Now,
                AsOfEvaluationTickCount = DateTime.Now.Ticks,
                BillCycleStartDate = "12 12",
                BillCycleEndDate = " 12 12 12"
            };

            _evaluationMock.Setup(t => t.GetAccountLevelEvaluationsAsync(It.IsAny<int>(), It.IsAny<string>(), It.IsAny<string>())).Returns(Task.FromResult(insightModel)).Verifiable();

            var customerModel = new CustomerModel()
            {
                ClientId = 1,
                CustomerId = "1234",
                Country = "USA",
                Source = "",
                IsBusiness = false,
                AddressLine1 = "C Block",
                AddressLine2 = "Flat no B-804",
                AddressLine3 = "Wakad",
                City = "Pune",
                State = "New york",
                PostalCode = "NY001",
                FirstName = "Sharad",
                LastName = "Saini",
                Phone1 = "9911559988",
                Phone2 = "8975757012",
                EmailAddress = "sharad.saini@saviantconsulting.com",
                CustomerType = "Good",
                TrumpiaSubscriptionId = ""
            };

            _customerMock.Setup(t => t.GetCustomerAsync(It.IsAny<int>(), It.IsAny<string>())).Returns(Task.FromResult(customerModel)).Verifiable();
            _customerMock.Setup(t => t.CreateSubscriptionAndUpdateCustomer(It.IsAny<CustomerModel>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>())).Returns(true).Verifiable();

            var billingModel = new BillingModel()
            {
                ClientId = 1,
                CustomerId = "1234",
                AccountId = "AC00012",
                PremiseId = "12",
                AmiStartDate = DateTime.Now,
                AmiEndDate = DateTime.Now,
                BillCycleScheduleId = "BS12122",
                RateClass = "Rate Class",
                ServicePointId = "SP0012",
                Source = "Rate Class",
                MeterType = "",
                UOMId = 12,
                TotalUsage = 1232323,
                StartDate = DateTime.Now,
                EndDate = DateTime.Today.AddDays(12),
                BillDays = 12,
                ReadQuality = "",
                TotalCost = 122323,
                CommodityId = 2,
                MeterId = " ",
                BillPeriodType = 2,
                ReplacedMeterId = "",
                ReadDate = DateTime.Now,
                ServiceContractId = "SC001",
                IsFault = false
            };

            _billingMock.Setup(t => t.GetServiceDetailsFromBillAsync(It.IsAny<int>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>())).Returns(Task.FromResult(billingModel)).Verifiable();

            var calculationModel = new CalculationModel()
            {
                ClientId = 1,
                AccountId = "",
                CustomerId = "",
                MeterId = "",
                ServiceContractId = "",
                CommodityId = 1,
                Unit = 2,
                Cost = 22.3,
                ProjectedCost = 23,
                ProjectedUsage = 32,
                Usage = 2,
                BillDays = 23,
                BillCycleStartDate = "",
                BillCycleEndDate = "",
                AsOfAmiDate = DateTime.Now,
                AsOfCalculationDate = DateTime.Now,
                RateClass = "",
                AverageDailyUsage = 23,
                AverageDailyCost = 23
            };

            _calculationMock.Setup(t => t.GetBtdCalculationAsync(It.IsAny<int>(), It.IsAny<string>())).Returns(Task.FromResult(calculationModel)).Verifiable();
            _calculationMock.Setup(t => t.GetCtdCalculationAsync(It.IsAny<int>(), It.IsAny<string>(), It.IsAny<string>())).Returns(Task.FromResult(calculationModel)).Verifiable();
            _notificationMock.Setup(t => t.ProcessMessage(It.IsAny<List<SubscriptionModel>>(), It.IsAny<InsightModel>(), It.IsAny<ClientSettings>(), It.IsAny<CustomerModel>(), It.IsAny<BillingModel>(), It.IsAny<Dictionary<string, List<TierBoundary>>>(), It.IsAny<List<CalculationModel>>())).Returns(Task.CompletedTask).Verifiable();

            var clientId = 1;
            string customerId = "1234", accountId = "A0001", programName = "Rate Class";
            _notificationFacadeManager.ProcessNotification(clientId, customerId, accountId, null, programName, null);

            _subscriptionMock.Verify(t => t.GetCustomerSubscriptions(It.IsAny<int>(), It.IsAny<string>(), It.IsAny<string>()), Times.Once);
            _subscriptionMock.VerifyAll();

            _evaluationMock.Verify(t => t.GetAccountLevelEvaluationsAsync(It.IsAny<int>(), It.IsAny<string>(), It.IsAny<string>()),Times.Once);
            _evaluationMock.VerifyAll();

            _customerMock.Verify(t => t.GetCustomerAsync(It.IsAny<int>(), It.IsAny<string>()), Times.Once);
            _customerMock.Verify(t => t.CreateSubscriptionAndUpdateCustomer(It.IsAny<CustomerModel>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>()), Times.Once);
            _customerMock.VerifyAll();

            _calculationMock.Verify(t => t.GetBtdCalculationAsync(It.IsAny<int>(), It.IsAny<string>()), Times.Once);
            _calculationMock.Verify(t => t.GetCtdCalculationAsync(It.IsAny<int>(), It.IsAny<string>(), It.IsAny<string>()), Times.Once);
            _calculationMock.VerifyAll();
        }

        /// <summary>
        /// Processes the notification with evaluation ticket count.
        /// </summary>
        [TestMethod]
        public void ProcessNotificationWithEvaluationTicketCount()
        {
            var subscriptionModel = new SubscriptionModel()
            {
                ClientId = 1,
                CustomerId = "1234",
                AccountId = "AC0001",
                ProgramName = "Rate Class",
                InsightTypeName = "",
                PremiseId = "",
                ServiceContractId = "SC001",
                ServicePointId = "",
                IsEmailOptInCompleted = false,
                IsSmsOptInCompleted = true,
                IsSelected = false,
                Channel = "SMS",
                AllowedChannels = "",
                SubscriberThresholdValue = 1234,
                CommodityKey = "Water",
                ThresholdType = "",
                ThresholdMin = 132,
                ThresholdMax = 136,
                IsDoubleOptIn = true,
                UomId = 12
            };
            IList<SubscriptionModel> subscriptionModelList = new List<SubscriptionModel>();
            subscriptionModelList.Add(subscriptionModel);
            _subscriptionMock.Setup(t => t.GetCustomerSubscriptions(It.IsAny<int>(), It.IsAny<string>(), It.IsAny<string>())).Returns(subscriptionModelList).Verifiable();

            var insightModel = new InsightModel()
            {
                ClientId = 1,
                AccountId = "1234",
                ServiceContractId = "SC001",
                BilllDays = 23,
                BtdCost = 2332,
                BtdProjectedCost = 3544,
                Usage = 32,
                SupressInsight = true,
                AccountLevelCostThresholdExceed = true,
                ServiceLevelUsageThresholdExceed = true,
                ServiceLevelCostThresholdExceed = true,
                DayThresholdExceed = true,
                ServiceLevelTieredThresholdExceed = false,
                ServiceDays = 32,
                CtdCost = 4545,
                CtdProjectedCost = 5678,
                NotifyTime = "21 12 2016",
                ProgramName = "Rate Class",
                AsOfEvaluationDate = DateTime.Now,
                AsOfEvaluationTickCount = DateTime.Now.Ticks,
                BillCycleStartDate = "12 12",
                BillCycleEndDate = " 12 12 12"
            };

            _evaluationMock.Setup(t => t.GetAccountLevelEvaluationsAsync(It.IsAny<int>(), It.IsAny<string>(), It.IsAny<string>())).Returns(Task.FromResult(insightModel)).Verifiable();

            var customerModel = new CustomerModel()
            {
                ClientId = 1,
                CustomerId = "1234",
                Country = "USA",
                Source = "",
                IsBusiness = false,
                AddressLine1 = "C Block",
                AddressLine2 = "Flat no B-804",
                AddressLine3 = "Wakad",
                City = "Pune",
                State = "New york",
                PostalCode = "NY001",
                FirstName = "Sharad",
                LastName = "Saini",
                Phone1 = "9911559988",
                Phone2 = "8975757012",
                EmailAddress = "sharad.saini@saviantconsulting.com",
                CustomerType = "Good",
                TrumpiaSubscriptionId = ""
            };

            _customerMock.Setup(t => t.GetCustomerAsync(It.IsAny<int>(), It.IsAny<string>())).Returns(Task.FromResult(customerModel)).Verifiable();
            _customerMock.Setup(t => t.CreateSubscriptionAndUpdateCustomer(It.IsAny<CustomerModel>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>())).Returns(true).Verifiable();

            var billingModel = new BillingModel()
            {
                ClientId = 1,
                CustomerId = "1234",
                AccountId = "AC00012",
                PremiseId = "12",
                AmiStartDate = DateTime.Now,
                AmiEndDate = DateTime.Now,
                BillCycleScheduleId = "BS12122",
                RateClass = "Rate Class",
                ServicePointId = "SP0012",
                Source = "Rate Class",
                MeterType = "",
                UOMId = 12,
                TotalUsage = 1232323,
                StartDate = DateTime.Now,
                EndDate = DateTime.Today.AddDays(12),
                BillDays = 12,
                ReadQuality = "",
                TotalCost = 122323,
                CommodityId = 2,
                MeterId = " ",
                BillPeriodType = 2,
                ReplacedMeterId = "",
                ReadDate = DateTime.Now,
                ServiceContractId = "SC001",
                IsFault = false
            };

            _billingMock.Setup(t => t.GetServiceDetailsFromBillAsync(It.IsAny<int>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>())).Returns(Task.FromResult(billingModel)).Verifiable();

            var calculationModel = new CalculationModel()
            {
                ClientId = 1,
                AccountId = "",
                CustomerId = "",
                MeterId = "",
                ServiceContractId = "",
                CommodityId = 1,
                Unit = 2,
                Cost = 22.3,
                ProjectedCost = 23,
                ProjectedUsage = 32,
                Usage = 2,
                BillDays = 23,
                BillCycleStartDate = "",
                BillCycleEndDate = "",
                AsOfAmiDate = DateTime.Now,
                AsOfCalculationDate = DateTime.Now,
                RateClass = "",
                AverageDailyUsage = 23,
                AverageDailyCost = 23
            };

            _calculationMock.Setup(t => t.GetBtdCalculationAsync(It.IsAny<int>(), It.IsAny<string>())).Returns(Task.FromResult(calculationModel)).Verifiable();
            _calculationMock.Setup(t => t.GetCtdCalculationAsync(It.IsAny<int>(), It.IsAny<string>(), It.IsAny<string>())).Returns(Task.FromResult(calculationModel)).Verifiable();

            _notificationMock.Setup(t => t.ProcessMessage(It.IsAny<List<SubscriptionModel>>(), It.IsAny<InsightModel>(), It.IsAny<ClientSettings>(), It.IsAny<CustomerModel>(), It.IsAny<BillingModel>(), It.IsAny<Dictionary<string, List<TierBoundary>>>(), It.IsAny<List<CalculationModel>>())).Returns(Task.CompletedTask).Verifiable();

            var clientId = 1;
            string customerId = "1234", accountId = "A0001", programName = "Rate Class";

            _notificationFacadeManager.ProcessNotification(clientId, customerId, accountId, null, programName, 1);

            _subscriptionMock.Verify(t => t.GetCustomerSubscriptions(It.IsAny<int>(), It.IsAny<string>(), It.IsAny<string>()), Times.Once);
            _subscriptionMock.VerifyAll();

            _evaluationMock.Verify(t => t.GetAccountLevelEvaluationsAsync(It.IsAny<int>(), It.IsAny<string>(), It.IsAny<string>()),Times.Once);
            _evaluationMock.VerifyAll();
        }

        /// <summary>
        /// Test case to get the tier boundries
        /// </summary>
        [TestMethod]
        public void GetTierBoundries()
        {
            var calculationModel = new CalculationModel()
            {
                ClientId = 1,
                AccountId = "",
                CustomerId = "",
                MeterId = "",
                ServiceContractId = "",
                CommodityId = 2,
                Unit = 2,
                Cost = 22.3,
                ProjectedCost = 23,
                ProjectedUsage = 32,
                Usage = 2,
                BillDays = 23,
                BillCycleStartDate = DateTime.Now.ToShortDateString(),
                BillCycleEndDate = DateTime.Now.AddDays(12).ToShortDateString(),
                AsOfAmiDate = DateTime.Now,
                AsOfCalculationDate = DateTime.Now,
                RateClass = "BillToDate",
                AverageDailyUsage = 23,
                AverageDailyCost = 23
            };

            List<TierBoundary> result = _notificationFacadeManager.GetTierBoundries(calculationModel, 1);
            Assert.AreEqual(result, null);
        }

        /// <summary>
        /// Releases unmanaged and - optionally - managed resources.
        /// </summary>
        /// <param name="disposing"><c>true</c> to release both managed and unmanaged resources; <c>false</c> to release only unmanaged resources.</param>
        private void Dispose(bool disposing)
        {
            if (!_disposed)
            {
                if (disposing)
                {
                    _unityContainer.Dispose();
                }
            }
            _disposed = true;
        }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            Dispose(true);
            // ReSharper disable once GCSuppressFinalizeForTypeWithoutDestructor
            GC.SuppressFinalize(this);
        }
    }
}
