﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.Practices.Unity;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using CE.AO.Business;
using Moq;
using CE.AO.DataAccess;
using CE.AO.Entities.TableStorageEntities;
using CE.AO.Logging;
using CE.AO.Models;
using Microsoft.WindowsAzure.Storage.Table;

namespace UnitTests
{
    /// <summary>
    /// contains test methods for ClientAccount class
    /// </summary>
    [TestClass]
    public class ClientAccountTest
    {
        private UnityContainer _unityContainer;
        private ClientAccount _clientAccount;
        private Mock<ITableRepository> _tableRepositoryMock;
        //private Mock<IQueueRepository> _queueRepositoryMock;

        /// <summary>
        /// Test Case Init
        /// </summary>
        [TestInitialize]
        public void Init()
        {
            AutoMapperConfig.AutoMapperMappings();
            _tableRepositoryMock = new Mock<ITableRepository>();
            _unityContainer = new UnityContainer();
            RegisterTypesForUnity.RegisterTypes(_unityContainer);
            var overrides = new ParameterOverrides
            {
                {"logModel", new LogModel {DisableLog = true}}
            };
            _clientAccount = _unityContainer.Resolve<ClientAccount>(overrides);
            _clientAccount.TableRepository = _tableRepositoryMock.Object;
        }

        /// <summary>
        /// Test Case for accessing default constructor
        /// </summary>
        [TestMethod]
        public void ClientAccount()
        {
            // ReSharper disable once ObjectCreationAsStatement
            new ClientAccount();
            Assert.AreEqual(true, true);
        }

        /// <summary>
        /// Test Case for accessing Log Model
        /// </summary>
        [TestMethod]
        public void AccessLogModel()
        {
            _clientAccount.LogModel.DisableLog = true;
            Assert.AreEqual(true, _clientAccount.LogModel.DisableLog);
        }

        /// <summary>
        /// Test case for InsertOrMergeClientAccount method in ClientAccount class
        /// </summary>
        [TestMethod]
        public void InsertOrMergeClientAccount()
        {
            var cliententity = GetClientAccountEntity();
            _tableRepositoryMock.Setup(t => t.InsertOrMergeSingleAsync(It.IsAny<string>(), It.IsAny<ClientAccountEntity>())).Returns(Task.FromResult(true)).Verifiable();
            var result = _clientAccount.InsertOrMergeClientAccount(cliententity).Result;
            _tableRepositoryMock.Verify(t => t.InsertOrMergeSingleAsync(It.IsAny<string>(), It.IsAny<ClientAccountEntity>()), Times.Once);
            _tableRepositoryMock.VerifyAll();
            Assert.AreEqual(true, result);
        }

        /// <summary>
        /// Test case for GetClientAccounts methods from ClientAccount class
        /// </summary>
        [TestMethod]
        public void GetClientAccounts()
        {
            var partitionKey = ClientAccountEntity.SetPartitionKey(1);
            var filter = TableQuery.GenerateFilterCondition("PartitionKey", QueryComparisons.Equal,partitionKey);
            var list = new List<ClientAccountEntity>();
            var clientEntity = Mapper.Map<ClientAccountModel, ClientAccountEntity>(GetClientAccountEntity());
            list.Add(clientEntity);
            _tableRepositoryMock.Setup(t => t.GetAllAsync<ClientAccountEntity>(It.IsAny<string>(), filter)).Returns(list).Verifiable();
            List<ClientAccountModel> result = _clientAccount.GetClientAccounts(1).ToList();
            _tableRepositoryMock.Verify(t => t.GetAllAsync<ClientAccountEntity>(It.IsAny<string>(), filter), Times.Once);
            _tableRepositoryMock.VerifyAll();
            Assert.AreEqual(result.Count, 1);
        }

        /// <summary>
        /// Test case for GetClientAccounts methods from ClientAccount class with dyanamic entity
        /// </summary>
        [TestMethod]
        public void GetClientAccountsDynamicEntity()
        {
            var partitionKey = ClientAccountEntity.SetPartitionKey(1);
            var filter = TableQuery.GenerateFilterCondition("PartitionKey", QueryComparisons.Equal,partitionKey);
            TableContinuationToken[] token = { null };
            var list = new List<DynamicTableEntity>();
            var clientEntity = GetClientAccountModelDynamicTableEntity(GetClientAccountEntity());
            list.Add(clientEntity);
            _tableRepositoryMock.Setup(t => t.GetAllAsync<DynamicTableEntity>(It.IsAny<string>(), filter, 1, ref token[0])).Returns(list).Verifiable();

            // ReSharper disable once SuggestVarOrType_Elsewhere
            var result = _clientAccount.GetClientAccounts(1, 1, ref token[0]).ToList();
            _tableRepositoryMock.Verify(t => t.GetAllAsync<DynamicTableEntity>(It.IsAny<string>(), filter, 1, ref token[0]), Times.Once);
            _tableRepositoryMock.VerifyAll();
            Assert.AreEqual(result.Count, 1);
        }

        private static ClientAccountModel GetClientAccountEntity()
        {
            return new ClientAccountModel
            {
                ClientId = 11,
                AccountId = "21",
                CustomerId = "31"
            };
        }

        private static DynamicTableEntity GetClientAccountModelDynamicTableEntity(ClientAccountModel clientAccountModel)
        {
            return new DynamicTableEntity
            {
                PartitionKey = clientAccountModel.PartitionKey,
                RowKey = clientAccountModel.RowKey,
                Properties = new Dictionary<string, EntityProperty>()
                {
                     {"ClientId",  new EntityProperty(clientAccountModel.ClientId)},
                     {"AccountId", new EntityProperty(clientAccountModel.AccountId)},
                     {"CustomerId",  new EntityProperty(clientAccountModel.CustomerId)}
                }
            };
        }
    }
}