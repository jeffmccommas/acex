﻿using System;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.Practices.Unity;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using CE.AO.Business;
using Moq;
using CE.AO.DataAccess;
using CE.AO.Entities.TableStorageEntities;
using CE.AO.Logging;
using CE.AO.Models;

namespace UnitTests
{
    /// <summary>
    /// Contains test methods for Business.AmiReading class
    /// </summary>
    [TestClass]
    public class AmiReadingTest
    {
        private UnityContainer _unityContainer;
        private AmiReading _amiReadingManager;
        private Mock<ITableRepository> _tableRepositoryMock;

        /// <summary>
        /// Test Case Init
        /// </summary>
        [TestInitialize]
        public void Init()
        {
            AutoMapperConfig.AutoMapperMappings();
            _tableRepositoryMock = new Mock<ITableRepository>();
            _unityContainer = new UnityContainer();
            RegisterTypesForUnity.RegisterTypes(_unityContainer);
            var overrides = new ParameterOverrides
            {
                {"logModel", new LogModel {DisableLog = true} }
            };
            _amiReadingManager = _unityContainer.Resolve<AmiReading>(overrides);
            _amiReadingManager.TableRepository = _tableRepositoryMock.Object;
        }

        /// <summary>
        /// Method for default constructor call
        /// </summary>
        [TestMethod]
        public void AmiReading()
        {
            // ReSharper disable once ObjectCreationAsStatement
            new AmiReading();
            Assert.AreEqual(true, true);
        }

        /// <summary>
        /// Test Case for accessing Log Model
        /// </summary>
        [TestMethod]
        public void AccessLogModel()
        {
            _amiReadingManager.LogModel.DisableLog = true;
            Assert.AreEqual(true, _amiReadingManager.LogModel.DisableLog);
        }

        /// <summary>
        /// Test case for inserting AmiReading
        /// </summary>
        [TestMethod]
        public void InsertOrMergeAmiReadingAsync()
        {
            var amiReading = GetAmiRead();
            _tableRepositoryMock.Setup(t => t.InsertOrMergeSingleAsync(It.IsAny<string>(), It.IsAny<AmiReadingEntity>())).Returns(Task.FromResult(true)).Verifiable();
            var result = _amiReadingManager.InsertOrMergeAmiReadingAsync(amiReading).Result;
            _tableRepositoryMock.Verify(t => t.InsertOrMergeSingleAsync(It.IsAny<string>(), It.IsAny<AmiReadingEntity>()), Times.Once);
            _tableRepositoryMock.VerifyAll();
            Assert.AreEqual(true, result);
        }

        /// <summary>
        /// Test case to fetch AmiReadings
        /// </summary>
        [TestMethod]
        public void GetAmiReading()
        {
            var amiReading = Mapper.Map<NccAmiReadingModel, AmiReadingEntity>(GetAmiRead());
            _tableRepositoryMock.Setup(t => t.GetSingleAsync<AmiReadingEntity>(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>())).Returns(Task.FromResult(amiReading)).Verifiable();
            var amiRead = _amiReadingManager.GetAmiReading(1, "3", DateTime.Now).Result;
            Assert.AreEqual(amiRead.ClientId, 1);
        }

        private static NccAmiReadingModel GetAmiRead()
        {
            return new NccAmiReadingModel
            {
                ClientId = 1,
                CommodityId = 2,
                MeterId = "3",
                TransponderId = 4,
                TransponderPort = 5,
                CustomerId = "6",
                ReadingValue = 100,
                UOMId = 7,
                AmiTimeStamp = DateTime.Now,
                Timezone = "IST",
                BatteryVoltage = "8",
            };
        }
    }
}
