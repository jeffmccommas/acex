﻿using CE.AO.Business;
using CE.AO.Business.Contract;
using CE.AO.DataAccess;
using Microsoft.Practices.Unity;
using CE.AO.Logging;

namespace UnitTests
{
    public static class RegisterTypesForUnity
    {
        public static void RegisterTypes(IUnityContainer container)
        {
            container.RegisterType<ITableRepository, TableRepositoryMock>();

            container.RegisterType<IQueueRepository, QueueRepositoryMock>();

            container.RegisterType<IAccountLookup, AccountLookup>();
            container.RegisterType<IAccountLookup, AccountLookup>(new InjectionConstructor(new LogModel()));

            container.RegisterType<IAccountUpdates, AccountUpdates>();
            container.RegisterType<IAccountUpdates, AccountUpdates>(new InjectionConstructor(new LogModel()));
            
            container.RegisterType<IAMI, AMI>();
            container.RegisterType<IAMI, AMI>(new InjectionConstructor(new LogModel()));

            container.RegisterType<ITallAMI, TallAMI>();
            container.RegisterType<ITallAMI, TallAMI>(new InjectionConstructor(new LogModel()));

            container.RegisterType<IBilling, Billing>();
            container.RegisterType<IBilling, Billing>(new InjectionConstructor(new LogModel()));

            container.RegisterType<IBillingCycleSchedule, BillingCycleSchedule>();
            container.RegisterType<IBillingCycleSchedule, BillingCycleSchedule>(new InjectionConstructor(new LogModel()));

            container.RegisterType<ICalculation, Calculation>();
            container.RegisterType<ICalculation, Calculation>(new InjectionConstructor(new LogModel()));

            container.RegisterType<ICalculationFacade, CalculationFacade>();
            container.RegisterType<ICalculationFacade, CalculationFacade>(new InjectionConstructor(new LogModel()));

            container.RegisterType<ICustomer, Customer>();
            container.RegisterType<ICustomer, Customer>(new InjectionConstructor(new LogModel()));

            container.RegisterType<IEvaluation, Evaluation>();
            container.RegisterType<IEvaluation, Evaluation>(new InjectionConstructor(new LogModel()));
            
            container.RegisterType<ISubscription, Subscription>();
            container.RegisterType<ISubscription, Subscription>(new InjectionConstructor(new LogModel()));
            
            container.RegisterType<IClientAccount, ClientAccount>();
            container.RegisterType<IClientAccount, ClientAccount>(new InjectionConstructor(new LogModel()));

            container.RegisterType<ITrumpiaKeywordConfiguration, TrumpiaKeywordConfiguration>();
            container.RegisterType<ITrumpiaKeywordConfiguration, TrumpiaKeywordConfiguration>(new InjectionConstructor(new LogModel()));

            container.RegisterType<ITrumpiaRequestDetail, TrumpiaRequestDetail>();
            container.RegisterType<ITrumpiaRequestDetail, TrumpiaRequestDetail>(new InjectionConstructor(new LogModel()));

            container.RegisterType<ISubscription, Subscription>();
            container.RegisterType<ISubscription, Subscription>(new InjectionConstructor(new LogModel()));

            container.RegisterType<INotification, Notification>();
            container.RegisterType<INotification, Notification>(new InjectionConstructor(new LogModel()));

            container.RegisterType<ICustomerMobileNumberLookup, CustomerMobileNumberLookup>();
            container.RegisterType<ICustomerMobileNumberLookup, CustomerMobileNumberLookup>(new InjectionConstructor(new LogModel()));

            container.RegisterType<IPremise, Premise>();
            container.RegisterType<IPremise, Premise>(new InjectionConstructor(new LogModel()));
        }

        /// <summary>
        /// To Enable Logging
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="unityContainer"></param>
        /// <param name="logModel"></param>
        /// <returns></returns>
        public static T Resolve<T>(this IUnityContainer unityContainer, LogModel logModel)
        {
            return unityContainer.Resolve<T>(new ParameterOverride("logModel", new LogModel()));

        }
    }
}
