﻿using CE.AO.Business;
using CE.AO.DataAccess;
using CE.AO.Entities.TableStorageEntities;
using CE.AO.Models;
using Microsoft.Practices.Unity;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace UnitTests
{
    /// <summary>
    /// Contains test methods for Subscription Class
    /// </summary>
    /// <seealso cref="System.IDisposable" />
    [TestClass]
    public class SubscriptionTest : IDisposable
    {
        /// <summary>
        /// The _unity container
        /// </summary>
        private UnityContainer _unityContainer;

        /// <summary>
        /// The _subscription manager
        /// </summary>
        private Subscription _subscriptionManager;

        /// <summary>
        /// The _table repository mock
        /// </summary>
        private Mock<ITableRepository> _tableRepositoryMock;

        /// <summary>
        /// The _disposed
        /// </summary>
        private bool _disposed;

        /// <summary>
        /// Test Case Init
        /// </summary>
        [TestInitialize]
        public void Init()
        {
            AutoMapperConfig.AutoMapperMappings();
            _tableRepositoryMock = new Mock<ITableRepository>();
            _unityContainer = new UnityContainer();
            RegisterTypesForUnity.RegisterTypes(_unityContainer);
            _subscriptionManager = _unityContainer.Resolve<Subscription>();
            _subscriptionManager.TableRepository = _tableRepositoryMock.Object;
        }

        /// <summary>
        /// Test Case for accessing Log Model
        /// </summary>
        [TestMethod]
        public void AccessLogModel()
        {
            _subscriptionManager.LogModel.DisableLog = true;
            Assert.AreEqual(true, _subscriptionManager.LogModel.DisableLog);
        }

        /// <summary>
        /// Test Case for default constructor of Subscription model
        /// </summary>
        [TestMethod]
        public void Subscription()
        {
            // ReSharper disable once ObjectCreationAsStatement
            new Subscription();
            Assert.AreEqual(true, true);
        }

        /// <summary>
        /// Test case for inserting Subscription Async
        /// </summary>
        [TestMethod]
        public void InsertSubscriptionAsync()
        {
            var subscriptionModel = new SubscriptionModel()
            {
                ClientId = 1,
                CustomerId = "2",
                AccountId = "3",
                AllowedChannels = "4",
                Channel = "C",
                CommodityKey = "RT4",
                InsightTypeName = "TYUI",
                IsDoubleOptIn = true,
                IsEmailOptInCompleted = true,
                IsSelected = true,
                IsSmsOptInCompleted = true,
                PremiseId = "67",
                ProgramName = "TUYRHDF",
                ServiceContractId = "RT67",
                ServicePointId = "67i",
                SubscriberThresholdValue = 45.34,
                ThresholdMax = 90.3,
                ThresholdMin = 90.67,
                ThresholdType = "qwe",
                UomId = 23
            };

            var subscriptionEntity = new SubscriptionEntity()
            {
                ClientId = 1,
                CustomerId = "2",
                AccountId = "3",
                Channel = "C",
                EmailConfirmationCount = 5,
                EmailOptInCompletedDate = DateTime.Now,
                InsightTypeName = "TYUI",
                IsEmailOptInCompleted = true,
                IsSelected = true,
                IsSmsOptInCompleted = true,
                OptOutDate = DateTime.Now,
                ProgramName = "TUYRHDF",
                ServiceContractId = "RT67",
                SmsConfirmationCount = 45,
                SmsOptInCompletedDate = DateTime.Now,
                SubscriberThresholdValue = 45.34,
                SubscriptionDate = DateTime.Now,
                UomId = 23
            };

            _tableRepositoryMock.Setup(t => t.GetSingleAsync<SubscriptionEntity>(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>())).Returns(Task.FromResult(subscriptionEntity)).Verifiable();
            _tableRepositoryMock.Setup(t => t.InsertOrMergeSingleAsync(It.IsAny<string>(), It.IsAny<SubscriptionEntity>())).Returns(Task.FromResult(true)).Verifiable();
            _tableRepositoryMock.Setup(t => t.DeleteSingleAsync<SubscriptionEntity>(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>())).Returns(Task.FromResult(true));

            var result = _subscriptionManager.InsertSubscriptionAsync(subscriptionModel, true, true).Result;

            _tableRepositoryMock.Verify(t => t.GetSingleAsync<SubscriptionEntity>(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>()), Times.Once);
            _tableRepositoryMock.Verify(t => t.DeleteSingleAsync<SubscriptionEntity>(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>()), Times.AtMost(2));
            _tableRepositoryMock.Verify(t => t.InsertOrMergeSingleAsync(It.IsAny<string>(), It.IsAny<SubscriptionEntity>()), Times.AtLeast(2));

            Assert.AreEqual(result, true);
        }

        /// <summary>
        /// Inserts the subscription asynchronous boolean value modified.
        /// </summary>
        [TestMethod]
        public void InsertSubscriptionAsyncBooleanValueModified()
        {
            var subscriptionModel = new SubscriptionModel()
            {
                ClientId = 1,
                CustomerId = "2",
                AccountId = "3",
                AllowedChannels = "4",
                Channel = "C",
                CommodityKey = "RT4",
                InsightTypeName = "TYUI",
                IsDoubleOptIn = true,
                IsEmailOptInCompleted = false,
                IsSelected = false,
                IsSmsOptInCompleted = false,
                PremiseId = "67",
                ProgramName = "TUYRHDF",
                ServiceContractId = "RT67",
                ServicePointId = "67i",
                SubscriberThresholdValue = 45.34,
                ThresholdMax = 90.3,
                ThresholdMin = 90.67,
                ThresholdType = "qwe",
                UomId = 23
            };

            var subscriptionEntity = new SubscriptionEntity()
            {
                ClientId = 1,
                CustomerId = "2",
                AccountId = "3",
                Channel = "C",
                EmailConfirmationCount = 5,
                EmailOptInCompletedDate = DateTime.Now,
                InsightTypeName = "TYUI",
                IsEmailOptInCompleted = true,
                IsSelected = true,
                IsSmsOptInCompleted = true,
                OptOutDate = DateTime.Now,
                ProgramName = "TUYRHDF",
                ServiceContractId = "RT67",
                SmsConfirmationCount = 45,
                SmsOptInCompletedDate = DateTime.Now,
                SubscriberThresholdValue = 45.34,
                SubscriptionDate = DateTime.Now,
                UomId = 23
            };

            _tableRepositoryMock.Setup(t => t.GetSingleAsync<SubscriptionEntity>(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>())).Returns(Task.FromResult(subscriptionEntity)).Verifiable();
            _tableRepositoryMock.Setup(t => t.InsertOrMergeSingleAsync(It.IsAny<string>(), It.IsAny<SubscriptionEntity>())).Returns(Task.FromResult(true)).Verifiable();
            _tableRepositoryMock.Setup(t => t.DeleteSingleAsync<SubscriptionEntity>(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>())).Returns(Task.FromResult(true));

            var result = _subscriptionManager.InsertSubscriptionAsync(subscriptionModel, false, false).Result;

            _tableRepositoryMock.Verify(t => t.GetSingleAsync<SubscriptionEntity>(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>()), Times.Once);
            _tableRepositoryMock.Verify(t => t.DeleteSingleAsync<SubscriptionEntity>(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>()), Times.AtMost(2));
            _tableRepositoryMock.Verify(t => t.InsertOrMergeSingleAsync(It.IsAny<string>(), It.IsAny<SubscriptionEntity>()), Times.AtLeast(2));

            Assert.AreEqual(result, true);
        }
        /// <summary>
        /// Inserts the subscription asynchronous model modified.
        /// </summary>
        [TestMethod]
        public void InsertSubscriptionAsyncModelModified()
        {
            var subscriptionModel = new SubscriptionModel()
            {
                ClientId = 1,
                CustomerId = "2",
                AccountId = "3",
                AllowedChannels = "4",
                Channel = "C",
                CommodityKey = "RT4",
                InsightTypeName = "TYUI",
                IsDoubleOptIn = true,
                IsEmailOptInCompleted = true,
                IsSelected = false,
                IsSmsOptInCompleted = true,
                PremiseId = "67",
                ProgramName = "TUYRHDF",
                ServiceContractId = "RT67",
                ServicePointId = "67i",
                SubscriberThresholdValue = 45.34,
                ThresholdMax = 90.3,
                ThresholdMin = 90.67,
                ThresholdType = "qwe",
                UomId = 23
            };

            var subscriptionEntity = new SubscriptionEntity()
            {
                ClientId = 1,
                CustomerId = "2",
                AccountId = "3",
                Channel = "C",
                EmailConfirmationCount = 5,
                EmailOptInCompletedDate = DateTime.Now,
                InsightTypeName = "TYUI",
                IsEmailOptInCompleted = false,
                IsSelected = true,
                IsSmsOptInCompleted = false,
                OptOutDate = DateTime.Now,
                ProgramName = "TUYRHDF",
                ServiceContractId = "RT67",
                SmsConfirmationCount = 45,
                SmsOptInCompletedDate = DateTime.Now,
                SubscriberThresholdValue = 45.34,
                SubscriptionDate = DateTime.Now,
                UomId = 23
            };

            _tableRepositoryMock.Setup(t => t.GetSingleAsync<SubscriptionEntity>(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>())).Returns(Task.FromResult(subscriptionEntity)).Verifiable();
            _tableRepositoryMock.Setup(t => t.InsertOrMergeSingleAsync(It.IsAny<string>(), It.IsAny<SubscriptionEntity>())).Returns(Task.FromResult(true)).Verifiable();
            _tableRepositoryMock.Setup(t => t.DeleteSingleAsync<SubscriptionEntity>(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>())).Returns(Task.FromResult(true));

            var result = _subscriptionManager.InsertSubscriptionAsync(subscriptionModel, false, false).Result;

            _tableRepositoryMock.Verify(t => t.GetSingleAsync<SubscriptionEntity>(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>()), Times.Once);
            _tableRepositoryMock.Verify(t => t.DeleteSingleAsync<SubscriptionEntity>(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>()), Times.AtMost(2));
            _tableRepositoryMock.Verify(t => t.InsertOrMergeSingleAsync(It.IsAny<string>(), It.IsAny<SubscriptionEntity>()), Times.AtLeast(2));

            Assert.AreEqual(result, true);
        }

        /// <summary>
        /// Inserts the subscription asynchronous model is selected false.
        /// </summary>
        [TestMethod]
        public void InsertSubscriptionAsyncModelIsSelectedFalse()
        {
            var subscriptionModel = new SubscriptionModel()
            {
                ClientId = 1,
                CustomerId = "2",
                AccountId = "3",
                AllowedChannels = "4",
                Channel = "C",
                CommodityKey = "RT4",
                InsightTypeName = "TYUI",
                IsDoubleOptIn = true,
                IsEmailOptInCompleted = true,
                IsSelected = true,
                IsSmsOptInCompleted = true,
                PremiseId = "67",
                ProgramName = "TUYRHDF",
                ServiceContractId = "RT67",
                ServicePointId = "67i",
                SubscriberThresholdValue = 45.34,
                ThresholdMax = 90.3,
                ThresholdMin = 90.67,
                ThresholdType = "qwe",
                UomId = 23
            };

            var subscriptionEntity = new SubscriptionEntity()
            {
                ClientId = 1,
                CustomerId = "2",
                AccountId = "3",
                Channel = "C",
                EmailConfirmationCount = 0,
                EmailOptInCompletedDate = DateTime.Now,
                InsightTypeName = "TYUI",
                IsEmailOptInCompleted = false,
                IsSelected = false,
                IsSmsOptInCompleted = false,
                OptOutDate = DateTime.Now,
                ProgramName = "TUYRHDF",
                ServiceContractId = "RT67",
                SmsConfirmationCount = 0,
                SmsOptInCompletedDate = DateTime.Now,
                SubscriberThresholdValue = 45.34,
                SubscriptionDate = DateTime.Now,
                UomId = 23
            };

            _tableRepositoryMock.Setup(t => t.GetSingleAsync<SubscriptionEntity>(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>())).Returns(Task.FromResult(subscriptionEntity)).Verifiable();
            _tableRepositoryMock.Setup(t => t.InsertOrMergeSingleAsync(It.IsAny<string>(), It.IsAny<SubscriptionEntity>())).Returns(Task.FromResult(true)).Verifiable();
            _tableRepositoryMock.Setup(t => t.DeleteSingleAsync<SubscriptionEntity>(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>())).Returns(Task.FromResult(true));
            var result = _subscriptionManager.InsertSubscriptionAsync(subscriptionModel, true, true).Result;
            _tableRepositoryMock.Verify(t => t.GetSingleAsync<SubscriptionEntity>(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>()), Times.Once);
            _tableRepositoryMock.Verify(t => t.DeleteSingleAsync<SubscriptionEntity>(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>()), Times.AtMost(2));
            _tableRepositoryMock.Verify(t => t.InsertOrMergeSingleAsync(It.IsAny<string>(), It.IsAny<SubscriptionEntity>()), Times.AtLeast(2));

            Assert.AreEqual(result, true);
        }

        /// <summary>
        /// Inserts the subscription asynchronous model is null.
        /// </summary>
        [TestMethod]
        public void InsertSubscriptionAsyncModelIsNull()
        {
            var subscriptionModel = new SubscriptionModel()
            {
                ClientId = 1,
                CustomerId = "2",
                AccountId = "3",
                AllowedChannels = "4",
                Channel = "C",
                CommodityKey = "RT4",
                InsightTypeName = "TYUI",
                IsDoubleOptIn = true,
                IsEmailOptInCompleted = true,
                IsSelected = false,
                IsSmsOptInCompleted = true,
                PremiseId = "67",
                ProgramName = "TUYRHDF",
                ServiceContractId = "RT67",
                ServicePointId = "67i",
                SubscriberThresholdValue = 45.34,
                ThresholdMax = 90.3,
                ThresholdMin = 90.67,
                ThresholdType = "qwe",
                UomId = 23
            };
            
            _tableRepositoryMock.Setup(t => t.GetSingleAsync<SubscriptionEntity>(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>())).Returns(Task.FromResult((SubscriptionEntity)null)).Verifiable();
            _tableRepositoryMock.Setup(t => t.InsertOrMergeSingleAsync(It.IsAny<string>(), It.IsAny<SubscriptionEntity>())).Returns(Task.FromResult(true)).Verifiable();
            _tableRepositoryMock.Setup(t => t.DeleteSingleAsync<SubscriptionEntity>(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>())).Returns(Task.FromResult(true));
            var result = _subscriptionManager.InsertSubscriptionAsync(subscriptionModel, false, false).Result;
            _tableRepositoryMock.Verify(t => t.GetSingleAsync<SubscriptionEntity>(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>()), Times.Once);
            _tableRepositoryMock.Verify(t => t.DeleteSingleAsync<SubscriptionEntity>(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>()), Times.AtMost(2));
            _tableRepositoryMock.Verify(t => t.InsertOrMergeSingleAsync(It.IsAny<string>(), It.IsAny<SubscriptionEntity>()), Times.AtLeast(2));

            Assert.AreEqual(result, true);
        }

        /// <summary>
        /// Test case for inserting Subscription Async
        /// </summary>
        [TestMethod]
        public void InsertSubscription()
        {
            var subscriptionModel = new SubscriptionModel()
            {
                ClientId = 1,
                CustomerId = "2",
                AccountId = "3",
                AllowedChannels = "4",
                Channel = "C",
                CommodityKey = "RT4",
                InsightTypeName = "TYUI",
                IsDoubleOptIn = true,
                IsEmailOptInCompleted = true,
                IsSelected = true,
                IsSmsOptInCompleted = true,
                PremiseId = "67",
                ProgramName = "TUYRHDF",
                ServiceContractId = "RT67",
                ServicePointId = "67i",
                SubscriberThresholdValue = 45.34,
                ThresholdMax = 90.3,
                ThresholdMin = 90.67,
                ThresholdType = "qwe",
                UomId = 23
            };

            var subscriptionEntity = new SubscriptionEntity()
            {
                ClientId = 1,
                CustomerId = "2",
                AccountId = "3",
                Channel = "C",
                EmailConfirmationCount = 5,
                EmailOptInCompletedDate = DateTime.Now,
                InsightTypeName = "TYUI",
                IsEmailOptInCompleted = true,
                IsSelected = true,
                IsSmsOptInCompleted = true,
                OptOutDate = DateTime.Now,
                ProgramName = "TUYRHDF",
                ServiceContractId = "RT67",
                SmsConfirmationCount = 45,
                SmsOptInCompletedDate = DateTime.Now,
                SubscriberThresholdValue = 45.34,
                SubscriptionDate = DateTime.Now,
                UomId = 23
            };

            _tableRepositoryMock.Setup(t => t.GetSingle<SubscriptionEntity>(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>())).Returns(subscriptionEntity).Verifiable();
            _tableRepositoryMock.Setup(t => t.InsertOrMergeSingle(It.IsAny<string>(), It.IsAny<SubscriptionEntity>())).Returns(true).Verifiable();
            _tableRepositoryMock.Setup(t => t.DeleteSingle<SubscriptionEntity>(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>())).Returns(true);
            var result = _subscriptionManager.InsertSubscription(subscriptionModel);
            _tableRepositoryMock.Verify(t => t.GetSingle<SubscriptionEntity>(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>()), Times.Once);
            _tableRepositoryMock.Verify(t => t.DeleteSingle<SubscriptionEntity>(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>()), Times.AtMost(2));
            _tableRepositoryMock.Verify(t => t.InsertOrMergeSingle(It.IsAny<string>(), It.IsAny<SubscriptionEntity>()), Times.AtLeast(2));
            Assert.AreEqual(result, true);
        }

        /// <summary>
        /// Inserts the subscription subscription model is null.
        /// </summary>
        [TestMethod]
        public void InsertSubscriptionSubscriptionModelIsNull()
        {
            var subscriptionModel = new SubscriptionModel()
            {
                ClientId = 1,
                CustomerId = "2",
                AccountId = "3",
                AllowedChannels = "4",
                Channel = "C",
                CommodityKey = "RT4",
                InsightTypeName = "TYUI",
                IsDoubleOptIn = true,
                IsEmailOptInCompleted = true,
                IsSelected = false,
                IsSmsOptInCompleted = true,
                PremiseId = "67",
                ProgramName = "TUYRHDF",
                ServiceContractId = "RT67",
                ServicePointId = "67i",
                SubscriberThresholdValue = 45.34,
                ThresholdMax = 90.3,
                ThresholdMin = 90.67,
                ThresholdType = "qwe",
                UomId = 23
            };

            _tableRepositoryMock.Setup(t => t.GetSingle<SubscriptionEntity>(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>())).Returns((SubscriptionEntity)null).Verifiable();
            _tableRepositoryMock.Setup(t => t.InsertOrMergeSingle(It.IsAny<string>(), It.IsAny<SubscriptionEntity>())).Returns(true).Verifiable();
            _tableRepositoryMock.Setup(t => t.DeleteSingle<SubscriptionEntity>(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>())).Returns(true);
            var result = _subscriptionManager.InsertSubscription(subscriptionModel);
            _tableRepositoryMock.Verify(t => t.GetSingle<SubscriptionEntity>(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>()), Times.Once);
            _tableRepositoryMock.Verify(t => t.DeleteSingle<SubscriptionEntity>(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>()), Times.AtMost(2));
            _tableRepositoryMock.Verify(t => t.InsertOrMergeSingle(It.IsAny<string>(), It.IsAny<SubscriptionEntity>()), Times.AtLeast(2));
            Assert.AreEqual(result, true);
        }

        /// <summary>
        /// Inserts the subscription boolean value modified.
        /// </summary>
        [TestMethod]
        public void InsertSubscriptionBooleanValueModified()
        {
            var subscriptionModel = new SubscriptionModel()
            {
                ClientId = 1,
                CustomerId = "2",
                AccountId = "3",
                AllowedChannels = "4",
                Channel = "C",
                CommodityKey = "RT4",
                InsightTypeName = "TYUI",
                IsDoubleOptIn = true,
                IsEmailOptInCompleted = false,
                IsSelected = false,
                IsSmsOptInCompleted = false,
                PremiseId = "67",
                ProgramName = "TUYRHDF",
                ServiceContractId = "RT67",
                ServicePointId = "67i",
                SubscriberThresholdValue = 45.34,
                ThresholdMax = 90.3,
                ThresholdMin = 90.67,
                ThresholdType = "qwe",
                UomId = 23
            };

            var subscriptionEntity = new SubscriptionEntity()
            {
                ClientId = 1,
                CustomerId = "2",
                AccountId = "3",
                Channel = "C",
                EmailConfirmationCount = 5,
                EmailOptInCompletedDate = DateTime.Now,
                InsightTypeName = "TYUI",
                IsEmailOptInCompleted = true,
                IsSelected = true,
                IsSmsOptInCompleted = true,
                OptOutDate = DateTime.Now,
                ProgramName = "TUYRHDF",
                ServiceContractId = "RT67",
                SmsConfirmationCount = 45,
                SmsOptInCompletedDate = DateTime.Now,
                SubscriberThresholdValue = 45.34,
                SubscriptionDate = DateTime.Now,
                UomId = 23
            };
            
            _tableRepositoryMock.Setup(t => t.GetSingle<SubscriptionEntity>(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>())).Returns(subscriptionEntity).Verifiable();
            _tableRepositoryMock.Setup(t => t.InsertOrMergeSingle(It.IsAny<string>(), It.IsAny<SubscriptionEntity>())).Returns(true).Verifiable();
            _tableRepositoryMock.Setup(t => t.DeleteSingle<SubscriptionEntity>(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>())).Returns(true);
            var result = _subscriptionManager.InsertSubscription(subscriptionModel);
            _tableRepositoryMock.Verify(t => t.GetSingle<SubscriptionEntity>(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>()), Times.Once);
            _tableRepositoryMock.Verify(t => t.DeleteSingle<SubscriptionEntity>(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>()), Times.AtMost(2));
            _tableRepositoryMock.Verify(t => t.InsertOrMergeSingle(It.IsAny<string>(), It.IsAny<SubscriptionEntity>()), Times.AtLeast(2));
            Assert.AreEqual(result, true);
        }

        /// <summary>
        /// Inserts the subscription model modified.
        /// </summary>
        [TestMethod]
        public void InsertSubscriptionModelModified()
        {
            var subscriptionModel = new SubscriptionModel()
            {
                ClientId = 1,
                CustomerId = "2",
                AccountId = "3",
                AllowedChannels = "4",
                Channel = "C",
                CommodityKey = "RT4",
                InsightTypeName = "TYUI",
                IsDoubleOptIn = true,
                IsEmailOptInCompleted = true,
                IsSelected = false,
                IsSmsOptInCompleted = true,
                PremiseId = "67",
                ProgramName = "TUYRHDF",
                ServiceContractId = "RT67",
                ServicePointId = "67i",
                SubscriberThresholdValue = 45.34,
                ThresholdMax = 90.3,
                ThresholdMin = 90.67,
                ThresholdType = "qwe",
                UomId = 23
            };

            var subscriptionEntity = new SubscriptionEntity()
            {
                ClientId = 1,
                CustomerId = "2",
                AccountId = "3",
                Channel = "C",
                EmailConfirmationCount = 5,
                EmailOptInCompletedDate = DateTime.Now,
                InsightTypeName = "TYUI",
                IsEmailOptInCompleted = false,
                IsSelected = true,
                IsSmsOptInCompleted = false,
                OptOutDate = DateTime.Now,
                ProgramName = "TUYRHDF",
                ServiceContractId = "RT67",
                SmsConfirmationCount = 45,
                SmsOptInCompletedDate = DateTime.Now,
                SubscriberThresholdValue = 45.34,
                SubscriptionDate = DateTime.Now,
                UomId = 23
            };

            _tableRepositoryMock.Setup(t => t.GetSingle<SubscriptionEntity>(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>())).Returns(subscriptionEntity).Verifiable();
            _tableRepositoryMock.Setup(t => t.InsertOrMergeSingle(It.IsAny<string>(), It.IsAny<SubscriptionEntity>())).Returns(true).Verifiable();
            _tableRepositoryMock.Setup(t => t.DeleteSingle<SubscriptionEntity>(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>())).Returns(true);
            var result = _subscriptionManager.InsertSubscription(subscriptionModel);
            _tableRepositoryMock.Verify(t => t.GetSingle<SubscriptionEntity>(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>()), Times.Once);
            _tableRepositoryMock.Verify(t => t.DeleteSingle<SubscriptionEntity>(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>()), Times.AtMost(2));
            _tableRepositoryMock.Verify(t => t.InsertOrMergeSingle(It.IsAny<string>(), It.IsAny<SubscriptionEntity>()), Times.AtLeast(2));
            Assert.AreEqual(result, true);
        }

        /// <summary>
        /// Inserts the subscription model is selected false.
        /// </summary>
        [TestMethod]
        public void InsertSubscriptionModelIsSelectedFalse()
        {
            var subscriptionModel = new SubscriptionModel()
            {
                ClientId = 1,
                CustomerId = "2",
                AccountId = "3",
                AllowedChannels = "4",
                Channel = "C",
                CommodityKey = "RT4",
                InsightTypeName = "TYUI",
                IsDoubleOptIn = true,
                IsEmailOptInCompleted = true,
                IsSelected = true,
                IsSmsOptInCompleted = true,
                PremiseId = "67",
                ProgramName = "TUYRHDF",
                ServiceContractId = "RT67",
                ServicePointId = "67i",
                SubscriberThresholdValue = 45.34,
                ThresholdMax = 90.3,
                ThresholdMin = 90.67,
                ThresholdType = "qwe",
                UomId = 23
            };

            var subscriptionEntity = new SubscriptionEntity()
            {
                ClientId = 1,
                CustomerId = "2",
                AccountId = "3",
                Channel = "C",
                EmailConfirmationCount = 0,
                EmailOptInCompletedDate = DateTime.Now,
                InsightTypeName = "TYUI",
                IsEmailOptInCompleted = false,
                IsSelected = false,
                IsSmsOptInCompleted = false,
                OptOutDate = DateTime.Now,
                ProgramName = "TUYRHDF",
                ServiceContractId = "RT67",
                SmsConfirmationCount = 0,
                SmsOptInCompletedDate = DateTime.Now,
                SubscriberThresholdValue = 45.34,
                SubscriptionDate = DateTime.Now,
                UomId = 23
            };

            _tableRepositoryMock.Setup(t => t.GetSingle<SubscriptionEntity>(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>())).Returns(subscriptionEntity).Verifiable();
            _tableRepositoryMock.Setup(t => t.InsertOrMergeSingle(It.IsAny<string>(), It.IsAny<SubscriptionEntity>())).Returns(true).Verifiable();
            _tableRepositoryMock.Setup(t => t.DeleteSingle<SubscriptionEntity>(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>())).Returns(true);
            var result = _subscriptionManager.InsertSubscription(subscriptionModel);
            _tableRepositoryMock.Verify(t => t.GetSingle<SubscriptionEntity>(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>()), Times.Once);
            _tableRepositoryMock.Verify(t => t.DeleteSingle<SubscriptionEntity>(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>()), Times.AtMost(2));
            _tableRepositoryMock.Verify(t => t.InsertOrMergeSingle(It.IsAny<string>(), It.IsAny<SubscriptionEntity>()), Times.AtLeast(2));

            Assert.AreEqual(result, true);
        }

        /// <summary>
        /// Test case for Get Customer Subscription
        /// </summary>
        [TestMethod]
        public void GetCustomerSubscriptions()
        {
            IList<SubscriptionEntity> subList = new List<SubscriptionEntity>();
            var subscriptionEntity = new SubscriptionEntity()
            {
                ClientId = 1,
                CustomerId = "2",
                AccountId = "3",
                Channel = "C",
                EmailConfirmationCount = 5,
                EmailOptInCompletedDate = DateTime.Now,
                InsightTypeName = "TYUI",
                IsEmailOptInCompleted = true,
                IsSelected = true,
                IsSmsOptInCompleted = true,
                OptOutDate = DateTime.Now,
                ProgramName = "TUYRHDF",
                ServiceContractId = "RT67",
                SmsConfirmationCount = 45,
                SmsOptInCompletedDate = DateTime.Now,
                SubscriberThresholdValue = 45.34,
                SubscriptionDate = DateTime.Now,
                UomId = 23
            };

            subList.Add(subscriptionEntity);
            _tableRepositoryMock.Setup(t => t.GetAllAsync<SubscriptionEntity>(It.IsAny<string>(), It.IsAny<string>())).Returns(subList);
            IEnumerable<SubscriptionModel> result = _subscriptionManager.GetCustomerSubscriptions(1, "2", "3");
            _tableRepositoryMock.Verify(t => t.GetAllAsync<SubscriptionEntity>(It.IsAny<string>(), It.IsAny<string>()), Times.AtLeast(2));
            var firstOrDefault = result.FirstOrDefault();
            if (firstOrDefault != null)
                Assert.AreEqual(firstOrDefault.ClientId, 1);
        }

        /// <summary>
        /// Test case for Get Customer Subscription
        /// </summary>
        [TestMethod]
        public void MergeDefaultSubscriptionListWithSubscribedPrograms()
        {
            var subscriptionModel = new SubscriptionModel()
            {
                ClientId = 1,
                CustomerId = "2",
                AccountId = "3",
                AllowedChannels = "4",
                Channel = "C",
                CommodityKey = "RT4",
                InsightTypeName = "",
                IsDoubleOptIn = true,
                IsEmailOptInCompleted = true,
                IsSelected = true,
                IsSmsOptInCompleted = true,
                PremiseId = "67",
                ProgramName = "Program1",
                ServiceContractId = "RT67",
                ServicePointId = "67i",
                SubscriberThresholdValue = 45.34,
                ThresholdMax = 90.3,
                ThresholdMin = 90.67,
                ThresholdType = "qwe",
                UomId = 23
            };
            var subModelList = new List<SubscriptionModel> {subscriptionModel};
            var defSub = new SubscriptionModel()
            {
                ClientId = 1,
                CustomerId = "2",
                AccountId = "3",
                AllowedChannels = "4",
                Channel = "C",
                CommodityKey = "RT4",
                InsightTypeName = "BillToDate",
                IsDoubleOptIn = true,
                IsEmailOptInCompleted = true,
                IsSelected = true,
                IsSmsOptInCompleted = true,
                PremiseId = "67",
                ProgramName = "Program1",
                ServiceContractId = "RT67",
                ServicePointId = "67i",
                SubscriberThresholdValue = 45.34,
                ThresholdMax = 90.3,
                ThresholdMin = 90.67,
                ThresholdType = "qwe",
                UomId = 23
            };
            var defSubList = new List<SubscriptionModel> {defSub};
            List<SubscriptionModel> result = _subscriptionManager.MergeDefaultSubscriptionListWithSubscribedPrograms(subModelList, defSubList);
            var firstOrDefault = result.FirstOrDefault();
            if (firstOrDefault != null) Assert.AreEqual((object)subscriptionModel.AccountId, firstOrDefault.AccountId);
        }

        /// <summary>
        /// Merges the default subscription list with subscribed programs model modified.
        /// </summary>
        [TestMethod]
        public void MergeDefaultSubscriptionListWithSubscribedProgramsModelModified()
        {
            var subscriptionModel = new SubscriptionModel()
            {
                ClientId = 1,
                CustomerId = "2",
                AccountId = "3",
                AllowedChannels = "4",
                Channel = "C",
                CommodityKey = "RT4",
                InsightTypeName = "BillToDate",
                IsDoubleOptIn = true,
                IsEmailOptInCompleted = true,
                IsSelected = true,
                IsSmsOptInCompleted = true,
                PremiseId = "67",
                ProgramName = "Program1",
                ServiceContractId = string.Empty,
                ServicePointId = "67i",
                SubscriberThresholdValue = 45.34,
                ThresholdMax = 90.3,
                ThresholdMin = 90.67,
                ThresholdType = "qwe",
                UomId = 23
            };
            var subModelList = new List<SubscriptionModel> {subscriptionModel};
            var defSub = new SubscriptionModel()
            {
                ClientId = 1,
                CustomerId = "2",
                AccountId = "3",
                AllowedChannels = "4",
                Channel = "C",
                CommodityKey = "RT4",
                InsightTypeName = "BillToDate",
                IsDoubleOptIn = true,
                IsEmailOptInCompleted = true,
                IsSelected = true,
                IsSmsOptInCompleted = true,
                PremiseId = "67",
                ProgramName = "Program1",
                ServiceContractId = string.Empty,
                ServicePointId = "67i",
                SubscriberThresholdValue = 45.34,
                ThresholdMax = 90.3,
                ThresholdMin = 90.67,
                ThresholdType = "qwe",
                UomId = 23
            };
            var defSubList = new List<SubscriptionModel> {defSub};
            List<SubscriptionModel> result = _subscriptionManager.MergeDefaultSubscriptionListWithSubscribedPrograms(subModelList, defSubList);
            var firstOrDefault = result.FirstOrDefault();
            if (firstOrDefault != null) Assert.AreEqual((object)subscriptionModel.AccountId, firstOrDefault.AccountId);
        }

        /// <summary>
        /// Merges the default subscription list with subscribed programs service contract identifier null.
        /// </summary>
        [TestMethod]
        public void MergeDefaultSubscriptionListWithSubscribedProgramsServiceContractIdNull()
        {
            var subscriptionModel = new SubscriptionModel()
            {
                ClientId = 1,
                CustomerId = "2",
                AccountId = "3",
                AllowedChannels = "4",
                Channel = "C",
                CommodityKey = "RT4",
                InsightTypeName = string.Empty,
                IsDoubleOptIn = true,
                IsEmailOptInCompleted = true,
                IsSelected = true,
                IsSmsOptInCompleted = true,
                PremiseId = "67",
                ProgramName = "Program1",
                ServiceContractId = "sc1",
                ServicePointId = "67i",
                SubscriberThresholdValue = 45.34,
                ThresholdMax = 90.3,
                ThresholdMin = 90.67,
                ThresholdType = "qwe",
                UomId = 23
            };
            var subModelList = new List<SubscriptionModel> {subscriptionModel};
            var defSub = new SubscriptionModel()
            {
                ClientId = 1,
                CustomerId = "2",
                AccountId = "3",
                AllowedChannels = "4",
                Channel = "C",
                CommodityKey = "RT4",
                InsightTypeName = string.Empty,
                IsDoubleOptIn = true,
                IsEmailOptInCompleted = true,
                IsSelected = true,
                IsSmsOptInCompleted = true,
                PremiseId = "67",
                ProgramName = "Program1",
                ServiceContractId = "sc1",
                ServicePointId = "67i",
                SubscriberThresholdValue = 45.34,
                ThresholdMax = 90.3,
                ThresholdMin = 90.67,
                ThresholdType = "qwe",
                UomId = 23
            };

            var defSub2 = new SubscriptionModel()
            {
                ClientId = 1,
                CustomerId = "2",
                AccountId = "3",
                AllowedChannels = "4",
                Channel = "C",
                CommodityKey = "RT4",
                InsightTypeName = "BillToDate",
                IsDoubleOptIn = true,
                IsEmailOptInCompleted = true,
                IsSelected = true,
                IsSmsOptInCompleted = true,
                PremiseId = "67",
                ProgramName = "Program1",
                ServiceContractId = "sc1",
                ServicePointId = "67i",
                SubscriberThresholdValue = 45.34,
                ThresholdMax = 90.3,
                ThresholdMin = 90.67,
                ThresholdType = "qwe",
                UomId = 23
            };

            var defSub3 = new SubscriptionModel()
            {
                ClientId = 1,
                CustomerId = "2",
                AccountId = "3",
                AllowedChannels = "4",
                Channel = "C",
                CommodityKey = "RT4",
                InsightTypeName = "BillToDate",
                IsDoubleOptIn = true,
                IsEmailOptInCompleted = true,
                IsSelected = true,
                IsSmsOptInCompleted = true,
                PremiseId = "67",
                ProgramName = "Program1",
                ServiceContractId = string.Empty,
                ServicePointId = "67i",
                SubscriberThresholdValue = 45.34,
                ThresholdMax = 90.3,
                ThresholdMin = 90.67,
                ThresholdType = "qwe",
                UomId = 23
            };

            var defSubList = new List<SubscriptionModel> {defSub, defSub2, defSub3};
            List<SubscriptionModel> result = _subscriptionManager.MergeDefaultSubscriptionListWithSubscribedPrograms(subModelList, defSubList);
            var firstOrDefault = result.FirstOrDefault();
            if (firstOrDefault != null)
            Assert.AreEqual((object)subscriptionModel.AccountId, firstOrDefault.AccountId);
        }

        /// <summary>
        /// Merges the default subscription list with subscribed programs insight types.
        /// </summary>
        [TestMethod]
        public void MergeDefaultSubscriptionListWithSubscribedProgramsInsightTypes()
        {
            var subscriptionModel = new SubscriptionModel()
            {
                ClientId = 1,
                CustomerId = "2",
                AccountId = "3",
                AllowedChannels = "4",
                Channel = "C",
                CommodityKey = "RT4",
                InsightTypeName = "BillToDate",
                IsDoubleOptIn = true,
                IsEmailOptInCompleted = true,
                IsSelected = true,
                IsSmsOptInCompleted = true,
                PremiseId = "67",
                ProgramName = "Program1",
                ServiceContractId = "sc1",
                ServicePointId = "67i",
                SubscriberThresholdValue = 45.34,
                ThresholdMax = 90.3,
                ThresholdMin = 90.67,
                ThresholdType = "qwe",
                UomId = 23
            };
            var subscriptionModel2 = new SubscriptionModel()
            {
                ClientId = 1,
                CustomerId = "2",
                AccountId = "3",
                AllowedChannels = "4",
                Channel = "C",
                CommodityKey = "RT4",
                InsightTypeName = string.Empty,
                IsDoubleOptIn = true,
                IsEmailOptInCompleted = true,
                IsSelected = true,
                IsSmsOptInCompleted = true,
                PremiseId = "67",
                ProgramName = "Program1",
                ServiceContractId = "sc1",
                ServicePointId = "67i",
                SubscriberThresholdValue = 45.34,
                ThresholdMax = 90.3,
                ThresholdMin = 90.67,
                ThresholdType = "qwe",
                UomId = 23
            };
            var subModelList = new List<SubscriptionModel> {subscriptionModel, subscriptionModel2};
            var defSub = new SubscriptionModel()
            {
                ClientId = 1,
                CustomerId = "2",
                AccountId = "3",
                AllowedChannels = "4",
                Channel = "C",
                CommodityKey = "RT4",
                InsightTypeName = "BillToDate",
                IsDoubleOptIn = true,
                IsEmailOptInCompleted = true,
                IsSelected = true,
                IsSmsOptInCompleted = true,
                PremiseId = "67",
                ProgramName = "Program1",
                ServiceContractId = "sc1",
                ServicePointId = "67i",
                SubscriberThresholdValue = 45.34,
                ThresholdMax = 90.3,
                ThresholdMin = 90.67,
                ThresholdType = "qwe",
                UomId = 23
            };

            var defSub2 = new SubscriptionModel()
            {
                ClientId = 1,
                CustomerId = "2",
                AccountId = "3",
                AllowedChannels = "4",
                Channel = "C",
                CommodityKey = "RT4",
                InsightTypeName = "BillToDate",
                IsDoubleOptIn = true,
                IsEmailOptInCompleted = true,
                IsSelected = true,
                IsSmsOptInCompleted = true,
                PremiseId = "67",
                ProgramName = "Program1",
                ServiceContractId = "sc1",
                ServicePointId = "67i",
                SubscriberThresholdValue = 45.34,
                ThresholdMax = 90.3,
                ThresholdMin = 90.67,
                ThresholdType = "qwe",
                UomId = 23
            };

            var defSub3 = new SubscriptionModel()
            {
                ClientId = 1,
                CustomerId = "2",
                AccountId = "3",
                AllowedChannels = "4",
                Channel = "C",
                CommodityKey = "RT4",
                InsightTypeName = "BillToDate",
                IsDoubleOptIn = true,
                IsEmailOptInCompleted = true,
                IsSelected = true,
                IsSmsOptInCompleted = true,
                PremiseId = "67",
                ProgramName = "Program1",
                ServiceContractId = "sc1",
                ServicePointId = "67i",
                SubscriberThresholdValue = 45.34,
                ThresholdMax = 90.3,
                ThresholdMin = 90.67,
                ThresholdType = "qwe",
                UomId = 23
            };

            var defSubList = new List<SubscriptionModel> {defSub, defSub2, defSub3};
            List<SubscriptionModel> result = _subscriptionManager.MergeDefaultSubscriptionListWithSubscribedPrograms(subModelList, defSubList);
            var firstOrDefault = result.FirstOrDefault();
            if (firstOrDefault != null)
            Assert.AreEqual((object)subscriptionModel.AccountId, firstOrDefault.AccountId);
        }

        /// <summary>
        /// Merges the default subscription list with subscribed programs subscriptionmodel is null.
        /// </summary>
        [TestMethod]
        public void MergeDefaultSubscriptionListWithSubscribedProgramsSubscriptionmodelIsNull()
        {
            var defSub = new SubscriptionModel()
            {
                ClientId = 1,
                CustomerId = "2",
                AccountId = "3",
                AllowedChannels = "4",
                Channel = "C",
                CommodityKey = "RT4",
                InsightTypeName = "BillToDate",
                IsDoubleOptIn = true,
                IsEmailOptInCompleted = true,
                IsSelected = true,
                IsSmsOptInCompleted = true,
                PremiseId = "67",
                ProgramName = "Program1",
                ServiceContractId = "sc1",
                ServicePointId = "67i",
                SubscriberThresholdValue = 45.34,
                ThresholdMax = 90.3,
                ThresholdMin = 90.67,
                ThresholdType = "qwe",
                UomId = 23
            };

            var defSub2 = new SubscriptionModel()
            {
                ClientId = 1,
                CustomerId = "2",
                AccountId = "3",
                AllowedChannels = "4",
                Channel = "C",
                CommodityKey = "RT4",
                InsightTypeName = "BillToDate",
                IsDoubleOptIn = true,
                IsEmailOptInCompleted = true,
                IsSelected = true,
                IsSmsOptInCompleted = true,
                PremiseId = "67",
                ProgramName = "Program1",
                ServiceContractId = "sc1",
                ServicePointId = "67i",
                SubscriberThresholdValue = 45.34,
                ThresholdMax = 90.3,
                ThresholdMin = 90.67,
                ThresholdType = "qwe",
                UomId = 23
            };

            var defSub3 = new SubscriptionModel()
            {
                ClientId = 1,
                CustomerId = "2",
                AccountId = "3",
                AllowedChannels = "4",
                Channel = "C",
                CommodityKey = "RT4",
                InsightTypeName = "BillToDate",
                IsDoubleOptIn = true,
                IsEmailOptInCompleted = true,
                IsSelected = true,
                IsSmsOptInCompleted = true,
                PremiseId = "67",
                ProgramName = "Program1",
                ServiceContractId = "sc1",
                ServicePointId = "67i",
                SubscriberThresholdValue = 45.34,
                ThresholdMax = 90.3,
                ThresholdMin = 90.67,
                ThresholdType = "qwe",
                UomId = 23
            };

            var defSubList = new List<SubscriptionModel> {defSub, defSub2, defSub3};
            List<SubscriptionModel> result = _subscriptionManager.MergeDefaultSubscriptionListWithSubscribedPrograms(null, defSubList);
            var firstOrDefault = result.FirstOrDefault();
            if (firstOrDefault != null)
            Assert.AreEqual(false, firstOrDefault.IsSelected);
        }

        /// <summary>
        /// Test case for Get Customer Subscription
        /// </summary>
        [TestMethod]
        public void InsertorMergeForSmsOptOut()
        {
            var billingModel = new BillingModel()
            {
                AccountId = "1",
                CustomerId = "2",
                ClientId = 3,
                ServiceContractId = "asd",
                ServicePointId = "23",
                Source = "Meter",
                PremiseId = "467",
                AmiEndDate = DateTime.Now,
                AmiStartDate = DateTime.Now,
                BillCycleScheduleId = "UYTH76",
                BillDays = 23,
                BillPeriodType = 35,
                CommodityId = 2,
                EndDate = DateTime.Now,
                IsFault = true,
                MeterId = "Id7",
                MeterType = "T",
                RateClass = "G",
                ReadDate = DateTime.Now,
                ReadQuality = "Yes",
                ReplacedMeterId = "45t",
                StartDate = DateTime.Now,
                TotalCost = 245.5,
                TotalUsage = 234.5,
                UOMId = 2
            };
            var subscriptionEntity = new SubscriptionEntity()
            {
                ClientId = 1,
                CustomerId = "2",
                AccountId = "3",
                Channel = "C",
                EmailConfirmationCount = 5,
                EmailOptInCompletedDate = DateTime.Now,
                InsightTypeName = "TYUI",
                IsEmailOptInCompleted = true,
                IsSelected = true,
                IsSmsOptInCompleted = true,
                OptOutDate = DateTime.Now,
                ProgramName = "TUYRHDF",
                ServiceContractId = "RT67",
                SmsConfirmationCount = 45,
                SmsOptInCompletedDate = DateTime.Now,
                SubscriberThresholdValue = 45.34,
                SubscriptionDate = DateTime.Now,
                UomId = 23
            };
            var subscriptionModel = new SubscriptionModel()
            {
                ClientId = 87,
                CustomerId = "2",
                AccountId = "3",
                AllowedChannels = "4",
                Channel = "C",
                CommodityKey = "RT4",
                InsightTypeName = "",
                IsDoubleOptIn = true,
                IsEmailOptInCompleted = true,
                IsSelected = true,
                IsSmsOptInCompleted = true,
                PremiseId = "67",
                ProgramName = "Program1",
                ServiceContractId = "RT67",
                ServicePointId = "67i",
                SubscriberThresholdValue = 45.34,
                ThresholdMax = 90.3,
                ThresholdMin = 90.67,
                ThresholdType = "qwe",
                UomId = 23
            };
            var subModelList = new List<SubscriptionModel> {subscriptionModel};

            _tableRepositoryMock.Setup(t => t.GetSingle<SubscriptionEntity>(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>())).Returns(subscriptionEntity).Verifiable();
            _tableRepositoryMock.Setup(t => t.InsertOrMergeSingle(It.IsAny<string>(), It.IsAny<SubscriptionEntity>())).Returns(true).Verifiable();
            _tableRepositoryMock.Setup(t => t.DeleteSingle<SubscriptionEntity>(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>()));
            var result = _subscriptionManager.InsertorMergeForSmsOptOut(billingModel, subModelList, 87, "2", "3", null, "Program1", "BillToDate");
            _tableRepositoryMock.Verify(t => t.GetSingle<SubscriptionEntity>(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>()), Times.AtMost(4));
            _tableRepositoryMock.Verify(t => t.DeleteSingle<SubscriptionEntity>(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>()), Times.AtMost(4));
            _tableRepositoryMock.Verify(t => t.InsertOrMergeSingle(It.IsAny<string>(), It.IsAny<SubscriptionEntity>()), Times.AtMost(4));

            Assert.AreEqual(result, true);
        }

        /// <summary>
        /// Insertors the merge for SMS opt out email and SMS channel.
        /// </summary>
        [TestMethod]
        public void InsertorMergeForSmsOptOutEmailAndSmsChannel()
        {
            var billingModel = new BillingModel()
            {
                AccountId = "1",
                CustomerId = "2",
                ClientId = 3,
                ServiceContractId = "asd",
                ServicePointId = "23",
                Source = "Meter",
                PremiseId = "467",
                AmiEndDate = DateTime.Now,
                AmiStartDate = DateTime.Now,
                BillCycleScheduleId = "UYTH76",
                BillDays = 23,
                BillPeriodType = 35,
                CommodityId = 2,
                EndDate = DateTime.Now,
                IsFault = true,
                MeterId = "Id7",
                MeterType = "T",
                RateClass = "G",
                ReadDate = DateTime.Now,
                ReadQuality = "Yes",
                ReplacedMeterId = "45t",
                StartDate = DateTime.Now,
                TotalCost = 245.5,
                TotalUsage = 234.5,
                UOMId = 2
            };
            var subscriptionEntity = new SubscriptionEntity()
            {
                ClientId = 1,
                CustomerId = "2",
                AccountId = "3",
                Channel = "EmailAndSMS",
                EmailConfirmationCount = 5,
                EmailOptInCompletedDate = DateTime.Now,
                InsightTypeName = "TYUI",
                IsEmailOptInCompleted = true,
                IsSelected = true,
                IsSmsOptInCompleted = true,
                OptOutDate = DateTime.Now,
                ProgramName = "TUYRHDF",
                ServiceContractId = "RT67",
                SmsConfirmationCount = 45,
                SmsOptInCompletedDate = DateTime.Now,
                SubscriberThresholdValue = 45.34,
                SubscriptionDate = DateTime.Now,
                UomId = 23
            };
            var subscriptionModel = new SubscriptionModel()
            {
                ClientId = 87,
                CustomerId = "2",
                AccountId = "3",
                AllowedChannels = "4",
                Channel = "C",
                CommodityKey = "RT4",
                InsightTypeName = "",
                IsDoubleOptIn = true,
                IsEmailOptInCompleted = true,
                IsSelected = true,
                IsSmsOptInCompleted = true,
                PremiseId = "67",
                ProgramName = "Program1",
                ServiceContractId = "RT67",
                ServicePointId = "67i",
                SubscriberThresholdValue = 45.34,
                ThresholdMax = 90.3,
                ThresholdMin = 90.67,
                ThresholdType = "qwe",
                UomId = 23
            };
            var subModelList = new List<SubscriptionModel> {subscriptionModel};
            _tableRepositoryMock.Setup(t => t.GetSingle<SubscriptionEntity>(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>())).Returns(subscriptionEntity).Verifiable();
            _tableRepositoryMock.Setup(t => t.InsertOrMergeSingle(It.IsAny<string>(), It.IsAny<SubscriptionEntity>())).Returns(true).Verifiable();
            _tableRepositoryMock.Setup(t => t.DeleteSingle<SubscriptionEntity>(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>()));
            var result = _subscriptionManager.InsertorMergeForSmsOptOut(billingModel, subModelList, 87, "2", "3", null, "Program1", "BillToDate");
            _tableRepositoryMock.Verify(t => t.GetSingle<SubscriptionEntity>(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>()), Times.AtMost(4));
            _tableRepositoryMock.Verify(t => t.DeleteSingle<SubscriptionEntity>(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>()), Times.AtMost(4));
            _tableRepositoryMock.Verify(t => t.InsertOrMergeSingle(It.IsAny<string>(), It.IsAny<SubscriptionEntity>()), Times.AtMost(4));

            Assert.AreEqual(result, true);
        }

        /// <summary>
        /// Insertors the merge for SMS opt out SMS channel.
        /// </summary>
        [TestMethod]
        public void InsertorMergeForSmsOptOutSmsChannel()
        {
            var billingModel = new BillingModel()
            {
                AccountId = "1",
                CustomerId = "2",
                ClientId = 3,
                ServiceContractId = "asd",
                ServicePointId = "23",
                Source = "Meter",
                PremiseId = "467",
                AmiEndDate = DateTime.Now,
                AmiStartDate = DateTime.Now,
                BillCycleScheduleId = "UYTH76",
                BillDays = 23,
                BillPeriodType = 35,
                CommodityId = 2,
                EndDate = DateTime.Now,
                IsFault = true,
                MeterId = "Id7",
                MeterType = "T",
                RateClass = "G",
                ReadDate = DateTime.Now,
                ReadQuality = "Yes",
                ReplacedMeterId = "45t",
                StartDate = DateTime.Now,
                TotalCost = 245.5,
                TotalUsage = 234.5,
                UOMId = 2
            };
            var subscriptionEntity = new SubscriptionEntity()
            {
                ClientId = 1,
                CustomerId = "2",
                AccountId = "3",
                Channel = "SMS",
                EmailConfirmationCount = 5,
                EmailOptInCompletedDate = DateTime.Now,
                InsightTypeName = "TYUI",
                IsEmailOptInCompleted = true,
                IsSelected = true,
                IsSmsOptInCompleted = true,
                OptOutDate = DateTime.Now,
                ProgramName = "TUYRHDF",
                ServiceContractId = "RT67",
                SmsConfirmationCount = 45,
                SmsOptInCompletedDate = DateTime.Now,
                SubscriberThresholdValue = 45.34,
                SubscriptionDate = DateTime.Now,
                UomId = 23
            };
            var subscriptionModel = new SubscriptionModel()
            {
                ClientId = 87,
                CustomerId = "2",
                AccountId = "3",
                AllowedChannels = "4",
                Channel = "C",
                CommodityKey = "RT4",
                InsightTypeName = "",
                IsDoubleOptIn = true,
                IsEmailOptInCompleted = true,
                IsSelected = true,
                IsSmsOptInCompleted = true,
                PremiseId = "67",
                ProgramName = "Program1",
                ServiceContractId = "RT67",
                ServicePointId = "67i",
                SubscriberThresholdValue = 45.34,
                ThresholdMax = 90.3,
                ThresholdMin = 90.67,
                ThresholdType = "qwe",
                UomId = 23
            };
            var subModelList = new List<SubscriptionModel> {subscriptionModel};
            _tableRepositoryMock.Setup(t => t.GetSingle<SubscriptionEntity>(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>())).Returns(subscriptionEntity).Verifiable();
            _tableRepositoryMock.Setup(t => t.InsertOrMergeSingle(It.IsAny<string>(), It.IsAny<SubscriptionEntity>())).Returns(true).Verifiable();
            _tableRepositoryMock.Setup(t => t.DeleteSingle<SubscriptionEntity>(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>()));
            var result = _subscriptionManager.InsertorMergeForSmsOptOut(billingModel, subModelList, 87, "2", "3", null, "Program1", "BillToDate");
            _tableRepositoryMock.Verify(t => t.GetSingle<SubscriptionEntity>(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>()), Times.AtMost(4));
            _tableRepositoryMock.Verify(t => t.DeleteSingle<SubscriptionEntity>(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>()), Times.AtMost(4));
            _tableRepositoryMock.Verify(t => t.InsertOrMergeSingle(It.IsAny<string>(), It.IsAny<SubscriptionEntity>()), Times.AtMost(4));
            Assert.AreEqual(result, true);
        }

        /// <summary>
        /// Insertors the merge for SMS opt out model modified.
        /// </summary>
        [TestMethod]
        public void InsertorMergeForSmsOptOutModelModified()
        {
            var billingModel = new BillingModel()
            {
                AccountId = "1",
                CustomerId = "2",
                ClientId = 3,
                ServiceContractId = "asd",
                ServicePointId = "23",
                Source = "Meter",
                PremiseId = "467",
                AmiEndDate = DateTime.Now,
                AmiStartDate = DateTime.Now,
                BillCycleScheduleId = "UYTH76",
                BillDays = 23,
                BillPeriodType = 35,
                CommodityId = 2,
                EndDate = DateTime.Now,
                IsFault = true,
                MeterId = "Id7",
                MeterType = "T",
                RateClass = "G",
                ReadDate = DateTime.Now,
                ReadQuality = "Yes",
                ReplacedMeterId = "45t",
                StartDate = DateTime.Now,
                TotalCost = 245.5,
                TotalUsage = 234.5,
                UOMId = 2
            };
            var subscriptionEntity = new SubscriptionEntity()
            {
                ClientId = 1,
                CustomerId = "2",
                AccountId = "3",
                Channel = "C",
                EmailConfirmationCount = 5,
                EmailOptInCompletedDate = DateTime.Now,
                InsightTypeName = "TYUI",
                IsEmailOptInCompleted = true,
                IsSelected = false,
                IsSmsOptInCompleted = true,
                OptOutDate = DateTime.Now,
                ProgramName = "TUYRHDF",
                ServiceContractId = "RT67",
                SmsConfirmationCount = 45,
                SmsOptInCompletedDate = DateTime.Now,
                SubscriberThresholdValue = 45.34,
                SubscriptionDate = DateTime.Now,
                UomId = 23
            };
            var subscriptionModel = new SubscriptionModel()
            {
                ClientId = 87,
                CustomerId = "2",
                AccountId = "3",
                AllowedChannels = "4",
                Channel = "C",
                CommodityKey = "RT4",
                InsightTypeName = "",
                IsDoubleOptIn = true,
                IsEmailOptInCompleted = true,
                IsSelected = true,
                IsSmsOptInCompleted = true,
                PremiseId = "67",
                ProgramName = "Program1",
                ServiceContractId = "RT67",
                ServicePointId = "67i",
                SubscriberThresholdValue = 45.34,
                ThresholdMax = 90.3,
                ThresholdMin = 90.67,
                ThresholdType = "qwe",
                UomId = 23
            };
            var subModelList = new List<SubscriptionModel> {subscriptionModel};
            _tableRepositoryMock.Setup(t => t.GetSingle<SubscriptionEntity>(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>())).Returns(subscriptionEntity).Verifiable();
            _tableRepositoryMock.Setup(t => t.InsertOrMergeSingle(It.IsAny<string>(), It.IsAny<SubscriptionEntity>())).Returns(true).Verifiable();
            _tableRepositoryMock.Setup(t => t.DeleteSingle<SubscriptionEntity>(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>()));
            var result = _subscriptionManager.InsertorMergeForSmsOptOut(billingModel, subModelList, 87, "2", "3", null, "Program1", "BillToDate");
            _tableRepositoryMock.Verify(t => t.GetSingle<SubscriptionEntity>(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>()), Times.AtMost(4));
            _tableRepositoryMock.Verify(t => t.DeleteSingle<SubscriptionEntity>(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>()), Times.AtMost(4));
            _tableRepositoryMock.Verify(t => t.InsertOrMergeSingle(It.IsAny<string>(), It.IsAny<SubscriptionEntity>()), Times.AtMost(4));
            Assert.AreEqual(result, true);
        }

        /// <summary>
        /// Insertors the merge for SMS opt out model is null SMS channel.
        /// </summary>
        [TestMethod]
        public void InsertorMergeForSmsOptOutModelIsNullSmsChannel()
        {
            var billingModel = new BillingModel()
            {
                AccountId = "1",
                CustomerId = "2",
                ClientId = 3,
                ServiceContractId = "asd",
                ServicePointId = "23",
                Source = "Meter",
                PremiseId = "467",
                AmiEndDate = DateTime.Now,
                AmiStartDate = DateTime.Now,
                BillCycleScheduleId = "UYTH76",
                BillDays = 23,
                BillPeriodType = 35,
                CommodityId = 2,
                EndDate = DateTime.Now,
                IsFault = true,
                MeterId = "Id7",
                MeterType = "T",
                RateClass = "G",
                ReadDate = DateTime.Now,
                ReadQuality = "Yes",
                ReplacedMeterId = "45t",
                StartDate = DateTime.Now,
                TotalCost = 245.5,
                TotalUsage = 234.5,
                UOMId = 2
            };

            var subscriptionEntity = new SubscriptionEntity()
            {
                ClientId = 1,
                CustomerId = "2",
                AccountId = "3",
                Channel = "SMS",
                EmailConfirmationCount = 5,
                EmailOptInCompletedDate = DateTime.Now,
                InsightTypeName = "TYUI",
                IsEmailOptInCompleted = true,
                IsSelected = false,
                IsSmsOptInCompleted = true,
                OptOutDate = DateTime.Now,
                ProgramName = "TUYRHDF",
                ServiceContractId = "RT67",
                SmsConfirmationCount = 45,
                SmsOptInCompletedDate = DateTime.Now,
                SubscriberThresholdValue = 45.34,
                SubscriptionDate = DateTime.Now,
                UomId = 23
            };

            var subscriptionModel = new SubscriptionModel()
            {
                ClientId = 87,
                CustomerId = "2",
                AccountId = "3",
                AllowedChannels = "4",
                Channel = "SMS",
                CommodityKey = "RT4",
                InsightTypeName = "",
                IsDoubleOptIn = true,
                IsEmailOptInCompleted = true,
                IsSelected = true,
                IsSmsOptInCompleted = true,
                PremiseId = "67",
                ProgramName = "Program1",
                ServiceContractId = "RT67",
                ServicePointId = "67i",
                SubscriberThresholdValue = 45.34,
                ThresholdMax = 90.3,
                ThresholdMin = 90.67,
                ThresholdType = "qwe",
                UomId = 23
            };
            var subModelList = new List<SubscriptionModel> {subscriptionModel};
            _tableRepositoryMock.Setup(t => t.GetSingle<SubscriptionEntity>(It.IsAny<string>(), It.IsAny<string>(), It.Is<string>(s => s.Contains("A_Program1")))).Returns(subscriptionEntity).Verifiable();
            _tableRepositoryMock.Setup(t => t.InsertOrMergeSingle(It.IsAny<string>(), It.IsAny<SubscriptionEntity>())).Returns(true).Verifiable();
            _tableRepositoryMock.Setup(t => t.DeleteSingle<SubscriptionEntity>(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>()));
            var result = _subscriptionManager.InsertorMergeForSmsOptOut(billingModel, subModelList, 87, "2", "3", null, "Program1", "BillToDate");
            _tableRepositoryMock.Verify(t => t.GetSingle<SubscriptionEntity>(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>()), Times.AtMost(4));
            _tableRepositoryMock.Verify(t => t.DeleteSingle<SubscriptionEntity>(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>()), Times.AtMost(4));
            _tableRepositoryMock.Verify(t => t.InsertOrMergeSingle(It.IsAny<string>(), It.IsAny<SubscriptionEntity>()), Times.AtMost(4));

            Assert.AreEqual(result, true);
        }

        /// <summary>
        /// Insertors the merge for SMS opt out model is null email and SMS channel.
        /// </summary>
        [TestMethod]
        public void InsertorMergeForSmsOptOutModelIsNullEmailAndSmsChannel()
        {
            var billingModel = new BillingModel()
            {
                AccountId = "1",
                CustomerId = "2",
                ClientId = 3,
                ServiceContractId = "asd",
                ServicePointId = "23",
                Source = "Meter",
                PremiseId = "467",
                AmiEndDate = DateTime.Now,
                AmiStartDate = DateTime.Now,
                BillCycleScheduleId = "UYTH76",
                BillDays = 23,
                BillPeriodType = 35,
                CommodityId = 2,
                EndDate = DateTime.Now,
                IsFault = true,
                MeterId = "Id7",
                MeterType = "T",
                RateClass = "G",
                ReadDate = DateTime.Now,
                ReadQuality = "Yes",
                ReplacedMeterId = "45t",
                StartDate = DateTime.Now,
                TotalCost = 245.5,
                TotalUsage = 234.5,
                UOMId = 2
            };

            var subscriptionEntity = new SubscriptionEntity()
            {
                ClientId = 1,
                CustomerId = "2",
                AccountId = "3",
                Channel = "EmailAndSMS",
                EmailConfirmationCount = 5,
                EmailOptInCompletedDate = DateTime.Now,
                InsightTypeName = "TYUI",
                IsEmailOptInCompleted = true,
                IsSelected = false,
                IsSmsOptInCompleted = true,
                OptOutDate = DateTime.Now,
                ProgramName = "TUYRHDF",
                ServiceContractId = "RT67",
                SmsConfirmationCount = 45,
                SmsOptInCompletedDate = DateTime.Now,
                SubscriberThresholdValue = 45.34,
                SubscriptionDate = DateTime.Now,
                UomId = 23
            };

            var subscriptionModel = new SubscriptionModel()
            {
                ClientId = 87,
                CustomerId = "2",
                AccountId = "3",
                AllowedChannels = "4",
                Channel = "EmailAndSMS",
                CommodityKey = "RT4",
                InsightTypeName = "",
                IsDoubleOptIn = true,
                IsEmailOptInCompleted = true,
                IsSelected = true,
                IsSmsOptInCompleted = true,
                PremiseId = "67",
                ProgramName = "Program1",
                ServiceContractId = "RT67",
                ServicePointId = "67i",
                SubscriberThresholdValue = 45.34,
                ThresholdMax = 90.3,
                ThresholdMin = 90.67,
                ThresholdType = "qwe",
                UomId = 23
            };
            var subModelList = new List<SubscriptionModel> {subscriptionModel};
            _tableRepositoryMock.Setup(t => t.GetSingle<SubscriptionEntity>(It.IsAny<string>(), It.IsAny<string>(), It.Is<string>(s => s.Contains("A_Program1")))).Returns(subscriptionEntity).Verifiable();
            _tableRepositoryMock.Setup(t => t.InsertOrMergeSingle(It.IsAny<string>(), It.IsAny<SubscriptionEntity>())).Returns(true).Verifiable();
            _tableRepositoryMock.Setup(t => t.DeleteSingle<SubscriptionEntity>(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>()));
            var result = _subscriptionManager.InsertorMergeForSmsOptOut(billingModel, subModelList, 87, "2", "3", null, "Program1", "BillToDate");
            _tableRepositoryMock.Verify(t => t.GetSingle<SubscriptionEntity>(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>()), Times.AtMost(4));
            _tableRepositoryMock.Verify(t => t.DeleteSingle<SubscriptionEntity>(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>()), Times.AtMost(4));
            _tableRepositoryMock.Verify(t => t.InsertOrMergeSingle(It.IsAny<string>(), It.IsAny<SubscriptionEntity>()), Times.AtMost(4));

            Assert.AreEqual(result, true);
        }

        /// <summary>
        /// Test case for Get Customer Subscription
        /// </summary>
        [TestMethod]
        public void GetSubscription()
        {
            IList<SubscriptionEntity> subList = new List<SubscriptionEntity>();
            var subscriptionEntity = new SubscriptionEntity()
            {
                ClientId = 1,
                CustomerId = "2",
                AccountId = "3",
                Channel = "C",
                EmailConfirmationCount = 5,
                EmailOptInCompletedDate = DateTime.Now,
                InsightTypeName = "TYUI",
                IsEmailOptInCompleted = true,
                IsSelected = true,
                IsSmsOptInCompleted = true,
                OptOutDate = DateTime.Now,
                ProgramName = "TUYRHDF",
                ServiceContractId = "RT67",
                SmsConfirmationCount = 45,
                SmsOptInCompletedDate = DateTime.Now,
                SubscriberThresholdValue = 45.34,
                SubscriptionDate = DateTime.Now,
                UomId = 23
            };
            subList.Add(subscriptionEntity);
            _tableRepositoryMock.Setup(t => t.GetAllAsync<SubscriptionEntity>(It.IsAny<string>(), It.IsAny<string>())).Returns(subList);
            var result = _subscriptionManager.GetSubscription(1, DateTime.Now, DateTime.Now, "TUYRHDF");
            _tableRepositoryMock.Verify(t => t.GetAllAsync<SubscriptionEntity>(It.IsAny<string>(), It.IsAny<string>()), Times.Once);
            var firstOrDefault = result.FirstOrDefault();
            if (firstOrDefault != null) Assert.AreEqual(firstOrDefault.ClientId, 1);
        }

        /// <summary>
        /// Replaces the latest subscription asynchronous.
        /// </summary>
        [TestMethod]
        public void ReplaceLatestSubscriptionAsync()
        {
            var subscriptionEntity = new SubscriptionEntity()
            {
                ClientId = 1,
                CustomerId = "2",
                AccountId = "3",
                Channel = "C",
                EmailConfirmationCount = 5,
                EmailOptInCompletedDate = DateTime.Now,
                InsightTypeName = "TYUI",
                IsEmailOptInCompleted = true,
                IsSelected = true,
                IsSmsOptInCompleted = true,
                OptOutDate = DateTime.Now,
                ProgramName = "TUYRHDF",
                ServiceContractId = "RT67",
                SmsConfirmationCount = 45,
                SmsOptInCompletedDate = DateTime.Now,
                SubscriberThresholdValue = 45.34,
                SubscriptionDate = DateTime.Now,
                UomId = 23
            };

            _tableRepositoryMock.Setup(t => t.ReplaceSingleAsync(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<SubscriptionEntity>(), null)).Returns(Task.FromResult(true)).Verifiable();
            var result = _subscriptionManager.ReplaceLatestSubscriptionAsync(subscriptionEntity, It.IsAny<string>()).Result;
            _tableRepositoryMock.Verify(t => t.ReplaceSingleAsync(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<SubscriptionEntity>(), null), Times.AtMost(4));
            Assert.AreEqual(result, true);
        }
        
        /// <summary>
        /// Replaces the history subscription asynchronous.
        /// </summary>
        [TestMethod]
        public void ReplaceHistorySubscriptionAsync()
        {
            var subscriptionEntity = new SubscriptionEntity()
            {
                ClientId = 1,
                CustomerId = "2",
                AccountId = "3",
                Channel = "C",
                EmailConfirmationCount = 5,
                EmailOptInCompletedDate = DateTime.Now,
                InsightTypeName = "TYUI",
                IsEmailOptInCompleted = true,
                IsSelected = true,
                IsSmsOptInCompleted = true,
                OptOutDate = DateTime.Now,
                ProgramName = "TUYRHDF",
                ServiceContractId = "RT67",
                SmsConfirmationCount = 45,
                SmsOptInCompletedDate = DateTime.Now,
                SubscriberThresholdValue = 45.34,
                SubscriptionDate = DateTime.Now,
                UomId = 23
            };

            _tableRepositoryMock.Setup(t => t.ReplaceSingleAsync(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<SubscriptionEntity>(), null)).Returns(Task.FromResult(true)).Verifiable();
            var result = _subscriptionManager.ReplaceHistorySubscriptionAsync(subscriptionEntity, It.IsAny<string>()).Result;
            _tableRepositoryMock.Verify(t => t.ReplaceSingleAsync(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<SubscriptionEntity>(), null), Times.AtMost(4));
            Assert.AreEqual(result, true);
        }

        /// <summary>
        /// Releases unmanaged and - optionally - managed resources.
        /// </summary>
        /// <param name="disposing"><c>true</c> to release both managed and unmanaged resources; <c>false</c> to release only unmanaged resources.</param>
        private void Dispose(bool disposing)
        {
            if (!_disposed)
            {
                if (disposing)
                {
                    _unityContainer.Dispose();
                }
            }
            _disposed = true;
        }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            Dispose(true);
            // ReSharper disable once GCSuppressFinalizeForTypeWithoutDestructor
            GC.SuppressFinalize(this);
        }
    }
}
