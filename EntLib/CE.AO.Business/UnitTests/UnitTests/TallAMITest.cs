﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.Practices.Unity;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using CE.AO.Business;
using Moq;
using CE.AO.DataAccess;
using CE.AO.Entities.TableStorageEntities;
using CE.AO.Logging;
using CE.AO.Models;

namespace UnitTests
{
    /// <summary>
    /// contains test methods for TallAMI class
    /// </summary>
    [TestClass]
    public class TallAmiTest
    {
        private UnityContainer _unityContainer;
        private TallAMI _tallAmiManager;
        private Mock<ITableRepository> _tableRepositoryMock;

        /// <summary>
        /// Test Case Init
        /// </summary>
        [TestInitialize]
        public void Init()
        {
            AutoMapperConfig.AutoMapperMappings();
            _tableRepositoryMock = new Mock<ITableRepository>();
            _unityContainer = new UnityContainer();
            RegisterTypesForUnity.RegisterTypes(_unityContainer);
            var overrides = new ParameterOverrides
            {
                {"logModel", new LogModel {DisableLog = true} }
            };
            _tallAmiManager = _unityContainer.Resolve<TallAMI>(overrides);
            _tallAmiManager.TableRepository = _tableRepositoryMock.Object;
        }

        /// <summary>
        /// Test Case for accessing default constructor
        /// </summary>
        [TestMethod]
        public void TallAmi()
        {
            // ReSharper disable once ObjectCreationAsStatement
            new TallAMI();
            Assert.AreEqual(true,true);
        }

        /// <summary>
        /// Test Case for accessing Log Model
        /// </summary>
        [TestMethod]
        public void AccessLogModel()
        {
            _tallAmiManager.LogModel.DisableLog = true;
            Assert.AreEqual(true, _tallAmiManager.LogModel.DisableLog);
        }

        /// <summary>
        /// Test case for InsertAmiAsync method in TallAMI class
        /// </summary>
        [TestMethod]
        public void InsertAmiAsync()
        {
            var tallAmi = GetTallAmiEntity();
            _tableRepositoryMock.Setup(t => t.InsertAsync(It.IsAny<TallAmiEntity>(), It.IsAny<string>())).Returns(Task.FromResult(true)).Verifiable();
            var result = _tallAmiManager.InsertAmiAsync(tallAmi).Result;
            _tableRepositoryMock.Verify(t => t.InsertAsync(It.IsAny<TallAmiEntity>(), It.IsAny<string>()), Times.Once);
            _tableRepositoryMock.VerifyAll();
            Assert.AreEqual(true, result);
        }

        /// <summary>
        /// Test case for InsertOrMergeAmiAsync method in TallAMI class
        /// </summary>
        [TestMethod]
        public void InsertOrMergeAmiAsync()
        {
            var tallAmi = GetTallAmiEntity();
            _tableRepositoryMock.Setup(t => t.InsertOrMergeSingleAsync(It.IsAny<string>(), It.IsAny<TallAmiEntity>())).Returns(Task.FromResult(true)).Verifiable();
            var result = _tallAmiManager.InsertOrMergeAmiAsync(tallAmi).Result;
            _tableRepositoryMock.Verify(t => t.InsertOrMergeSingleAsync(It.IsAny<string>(), It.IsAny<TallAmiEntity>()), Times.Once);
            _tableRepositoryMock.VerifyAll();
            Assert.AreEqual(true, result);
        }

        /// <summary>
        /// Test case for InsertOrMergeAmiAsync method in TallAMI class
        /// </summary>
        [TestMethod]
        public void InsertOrMergeBatchAsync()
        {
            var tallAmi = GetTallAmiEntity();
            var entities = new List<TallAmiModel> {tallAmi};
            _tableRepositoryMock.Setup(t => t.InsertOrMergeBatchAsync(It.IsAny<List<TallAmiEntity>>(), It.IsAny<string>())).Returns(Task.FromResult(true)).Verifiable();
            var result = _tallAmiManager.InsertOrMergeBatchAsync(entities).Result;
            _tableRepositoryMock.Verify(t => t.InsertOrMergeBatchAsync(It.IsAny<List<TallAmiEntity>>(), It.IsAny<string>()), Times.Once);
            _tableRepositoryMock.VerifyAll();
            Assert.AreEqual(true, result);
        }

        private static TallAmiModel GetTallAmiEntity()
        {
            return new TallAmiModel
            {
                ClientId = 1,
                AccountNumber = "2",
                MeterId = "3",
                ServicePointId = "4",
                CommodityId = 4,
                UOMId = 5,
                VolumeFactor = 5,
                Direction = 6,
                ProjectedReadDate = DateTime.Now,
                AmiTimeStamp = DateTime.Now,
                Consumption = 7,
                BatteryVoltage = "8",
                TransponderId = "9",
                TransponderPort = "10",
                DialRead = "11",
                QualityCode = "12",
                Scenario = "B",
                ReadingType = "C"
            };
        }
    }
}
