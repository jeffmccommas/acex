﻿using System;
using System.Threading.Tasks;
using CE.AO.Business;
using CE.AO.Business.Contract;
using CE.AO.Logging;
using CE.AO.Models;
using Microsoft.Practices.Unity;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace UnitTests
{
    /// <summary>
    /// Test cases for AccountUpdatesFacade class
    /// </summary>
    /// <seealso cref="System.IDisposable" />
    [TestClass]
    public class AccountUpdatesFacadeTest : IDisposable
    {
        /// <summary>
        /// The _unity container
        /// </summary>
        private UnityContainer _unityContainer;
        /// <summary>
        /// The _account updates facade manager
        /// </summary>
        private AccountUpdatesFacade _accountUpdatesFacadeManager;
        /// <summary>
        /// The _account updates
        /// </summary>
        private Mock<IAccountUpdates> _accountUpdates;
        /// <summary>
        /// The _billing
        /// </summary>
        private Mock<IBilling> _billing;
        /// <summary>
        /// The _customer
        /// </summary>
        private Mock<ICustomer> _customer;
        /// <summary>
        /// The _account lookup
        /// </summary>
        private Mock<IAccountLookup> _accountLookup;
        /// <summary>
        /// The _disposed
        /// </summary>
        private bool _disposed;

        /// <summary>
        /// Test Case Init
        /// </summary>
        [TestInitialize]
        public void Init()
        {
            AutoMapperConfig.AutoMapperMappings();
            _unityContainer = new UnityContainer();
            RegisterTypesForUnity.RegisterTypes(_unityContainer);
            var overrides = new ParameterOverrides
            {
                {"logModel", new LogModel {DisableLog = true} }
            };
            _accountUpdatesFacadeManager= _unityContainer.Resolve<AccountUpdatesFacade>(overrides);
            _accountUpdates = new Mock<IAccountUpdates>();
            _billing = new Mock<IBilling>();
            _customer = new Mock<ICustomer>();
            _accountLookup = new Mock<IAccountLookup>();
            _accountUpdatesFacadeManager.AccountUpdatesManager= _accountUpdates.Object;
            _accountUpdatesFacadeManager.BillingManager = _billing.Object;
            _accountUpdatesFacadeManager.CustomerManager= _customer.Object;
            _accountUpdatesFacadeManager.AccountLookupManager = _accountLookup.Object;
        }

        /// <summary>
        /// Test Case for accessing Log Model
        /// </summary>
        [TestMethod]
        public void AccessLogModel()
        {
            _accountUpdatesFacadeManager.LogModel.DisableLog = true;
            Assert.AreEqual(true, _accountUpdatesFacadeManager.LogModel.DisableLog);
        }

        /// <summary>
        /// Test Case for default constructor of AccountUpdatesFacade model
        /// </summary>
        [TestMethod]
        public void AccountUpdatesFacade()
        {
            // ReSharper disable once ObjectCreationAsStatement
            new AccountUpdatesFacade();
            Assert.AreEqual(true, true);
        }

        /// <summary>
        /// Test case for insert of AccountUpdateModel model
        /// </summary>
        [TestMethod]
        public void InsertOrMergeAccountUpdateAsync()
        {
            var accountUpdatesModel = new AccountUpdatesModel
            {
                AccountId = "",
                ClientId = 87,
                CustomerId = "",
                MeterId = "METER_QA_02",
                NewValue = "",
                OldValue = "",
                PremiseId = "",
                ServiceContractId = "",
                UpdateType = CE.AO.Utilities.Enums.AccountUpdateType.RateClass.ToString()
            };
            _accountUpdates.Setup(t => t.InsertOrMergeAccountUpdateAsync(It.IsAny<AccountUpdatesModel>(), It.IsAny<bool>())).Returns(Task.FromResult(true)).Verifiable();
            var billingModel = new BillingModel();
            _billing.Setup(t =>t.GetServiceDetailsFromBillAsync(It.IsAny<int>(), It.IsAny<string>(), It.IsAny<string>(),It.IsAny<string>())).Returns(Task.FromResult(billingModel)).Verifiable();
            _billing.Setup(t => t.InsertOrMergeLatestBillAsync(It.IsAny<BillingModel>())).Returns(Task.FromResult(true)).Verifiable();

            // Same for phone1 and email
            var customerModel = new CustomerModel();
            _customer.Setup(t => t.GetCustomerAsync(It.IsAny<int>(), It.IsAny<string>())).Returns(Task.FromResult(customerModel)).Verifiable();
            _customer.Setup(t => t.InsertOrMergeCustomerAsync(It.IsAny<CustomerModel>())).Returns(Task.FromResult(true)).Verifiable();

            //AccountLookup
            var accountLookupModel = new AccountLookupModel();
            _accountLookup.Setup(t => t.GetAccountMeterDetails(It.IsAny<int>(), It.IsAny<string>(), It.IsAny<string>())).Returns(Task.FromResult(accountLookupModel)).Verifiable();
            _accountLookup.Setup(t => t.InsertOrMergeAccountLookupAsync(It.IsAny<AccountLookupModel>())).Returns(Task.FromResult(true)).Verifiable();
            var result = _accountUpdatesFacadeManager.InsertOrMergeAccountUpdateAsync(accountUpdatesModel).Result;

            // For Email updation
            accountUpdatesModel.UpdateType = CE.AO.Utilities.Enums.AccountUpdateType.Email.ToString();
            result = result && _accountUpdatesFacadeManager.InsertOrMergeAccountUpdateAsync(accountUpdatesModel).Result;

            // For Phone updation
            accountUpdatesModel.UpdateType = CE.AO.Utilities.Enums.AccountUpdateType.Phone1.ToString();
            result = result && _accountUpdatesFacadeManager.InsertOrMergeAccountUpdateAsync(accountUpdatesModel).Result;

            // For MeterReplacement
            accountUpdatesModel.UpdateType = CE.AO.Utilities.Enums.AccountUpdateType.MeterReplacement.ToString();
            result = result && _accountUpdatesFacadeManager.InsertOrMergeAccountUpdateAsync(accountUpdatesModel).Result;

            // 2 times in each call of InsertOrMergeAccountUpdateAsync
            _accountUpdates.Verify(t => t.InsertOrMergeAccountUpdateAsync(It.IsAny<AccountUpdatesModel>(), It.IsAny<bool>()), Times.AtMost(8));
            _accountUpdates.VerifyAll();

            _billing.Verify(t => t.GetServiceDetailsFromBillAsync(It.IsAny<int>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>()), Times.AtMost(2));
            _billing.Verify(t => t.InsertOrMergeLatestBillAsync(It.IsAny<BillingModel>()), Times.AtMost(2));
            _billing.VerifyAll();

            _customer.Verify(t => t.GetCustomerAsync(It.IsAny<int>(), It.IsAny<string>()), Times.AtMost(2));
            _customer.Verify(t => t.InsertOrMergeCustomerAsync(It.IsAny<CustomerModel>()), Times.AtMost(2));
            _customer.VerifyAll();

            _accountLookup.Verify(t => t.GetAccountMeterDetails(It.IsAny<int>(), It.IsAny<string>(), It.IsAny<string>()), Times.AtMost(1));
            _accountLookup.Verify(t => t.InsertOrMergeAccountLookupAsync(It.IsAny<AccountLookupModel>()), Times.AtMost(2));
            _accountLookup.VerifyAll();

            Assert.AreEqual(result, true);
        }

        /// <summary>
        /// Releases unmanaged and - optionally - managed resources.
        /// </summary>
        /// <param name="disposing"><c>true</c> to release both managed and unmanaged resources; <c>false</c> to release only unmanaged resources.</param>
        private void Dispose(bool disposing)
        {
            if (!_disposed)
            {
                if (disposing)
                {
                    _unityContainer.Dispose();
                }
            }
            _disposed = true;
        }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            Dispose(true);
            // ReSharper disable once GCSuppressFinalizeForTypeWithoutDestructor
            GC.SuppressFinalize(this);
        }
    }
}
