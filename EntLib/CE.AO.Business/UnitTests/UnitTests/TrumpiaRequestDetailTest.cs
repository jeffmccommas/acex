﻿using System.Threading.Tasks;
using Microsoft.Practices.Unity;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using CE.AO.Business;
using Moq;
using CE.AO.DataAccess;
using CE.AO.Entities.TableStorageEntities;
using CE.AO.Logging;

namespace UnitTests
{
    /// <summary>
    /// contains test methods for TrumpiaRequestDetail class
    /// </summary>
    [TestClass]
    public class TrumpiaRequestDetailTest
    {
        private UnityContainer _unityContainer;
        private TrumpiaRequestDetail _trumpiaRequestDetailManager;
        private Mock<ITableRepository> _tableRepositoryMock;

        /// <summary>
        /// Test Case Init
        /// </summary>
        [TestInitialize]
        public void Init()
        {
            AutoMapperConfig.AutoMapperMappings();
            _tableRepositoryMock = new Mock<ITableRepository>();
            _unityContainer = new UnityContainer();
            RegisterTypesForUnity.RegisterTypes(_unityContainer);
            var overrides = new ParameterOverrides
            {
                {"logModel", new LogModel {DisableLog = true}}
            };
            _trumpiaRequestDetailManager = _unityContainer.Resolve<TrumpiaRequestDetail>(overrides);
            _trumpiaRequestDetailManager.TableRepository = _tableRepositoryMock.Object;
        }

        /// <summary>
        /// Test Case for accessing default constructor
        /// </summary>
        [TestMethod]
        public void TrumpiaRequestDetail()
        {
            // ReSharper disable once ObjectCreationAsStatement
            new TrumpiaRequestDetail();
            Assert.AreEqual(true, true);
        }

        /// <summary>
        /// Test Case for accessing Log Model
        /// </summary>
        [TestMethod]
        public void AccessLogModel()
        {
            _trumpiaRequestDetailManager.LogModel.DisableLog = true;
            Assert.AreEqual(true, _trumpiaRequestDetailManager.LogModel.DisableLog);
        }

        /// <summary>
        /// Test case for InsertOrMergeTrumpiaRequestDetailAsync method in TrumpiaRequestDetail class
        /// </summary>
        [TestMethod]
        public void InsertOrMergeTrumpiaRequestDetailAsync()
        {
            var trumpiaRequest = GetTrumpiaRequest();
            _tableRepositoryMock.Setup(t => t.InsertOrMergeSingleAsync(It.IsAny<string>(), It.IsAny<TrumpiaRequestDetailEntity>())).Returns(Task.FromResult(true)).Verifiable();
            var result = _trumpiaRequestDetailManager.InsertOrMergeTrumpiaRequestDetailAsync(trumpiaRequest).Result;
            _tableRepositoryMock.Verify(t => t.InsertOrMergeSingleAsync(It.IsAny<string>(), It.IsAny<TrumpiaRequestDetailEntity>()), Times.Once);
            _tableRepositoryMock.VerifyAll();
            Assert.AreEqual(true, result);
        }

        /// <summary>
        /// Test case for InsertOrMergeTrumpiaRequestDetail method in TrumpiaRequestDetail class
        /// </summary>
        [TestMethod]
        public void InsertOrMergeTrumpiaRequestDetail()
        {
            var trumpiaRequest = GetTrumpiaRequest();
            _tableRepositoryMock.Setup(t => t.InsertOrMergeSingle(It.IsAny<string>(), It.IsAny<TrumpiaRequestDetailEntity>())).Returns(true).Verifiable();
            var result = _trumpiaRequestDetailManager.InsertOrMergeTrumpiaRequestDetail(trumpiaRequest);
            _tableRepositoryMock.Verify(t => t.InsertOrMergeSingle(It.IsAny<string>(), It.IsAny<TrumpiaRequestDetailEntity>()), Times.Once);
            _tableRepositoryMock.VerifyAll();
            Assert.AreEqual(true, result);
        }

        /// <summary>
        /// Test case for GetTrumpiaRequestDetailAsync method in TrumpiaRequestDetail class
        /// </summary>
        [TestMethod]
        public void GetTrumpiaRequestDetailAsync()
        {
            var trumpiaRequest = GetTrumpiaRequest();
            _tableRepositoryMock.Setup(t => t.GetSingleAsync<TrumpiaRequestDetailEntity>(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>())).Returns(Task.FromResult(trumpiaRequest)).Verifiable();
            var customer = _trumpiaRequestDetailManager.GetTrumpiaRequestDetailAsync("2").Result;
            Assert.AreEqual(trumpiaRequest.RequestId,customer.RequestId);
        }

        /// <summary>
        /// Test case for GetTrumpiaRequestDetailAsync method in TrumpiaRequestDetail class
        /// </summary>
        [TestMethod]
        public void GetTrumpiaRequestDetail()
        {
            var trumpRequest = GetTrumpiaRequest();
            _tableRepositoryMock.Setup(t => t.GetSingle<TrumpiaRequestDetailEntity>(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>())).Returns(trumpRequest).Verifiable();
            var trumpiaEntity =_trumpiaRequestDetailManager.GetTrumpiaRequestDetail(trumpRequest.RequestId);
            _tableRepositoryMock.Verify(t => t.GetSingle<TrumpiaRequestDetailEntity>(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>()), Times.Once);
            _tableRepositoryMock.VerifyAll();
            Assert.AreEqual(trumpRequest.RequestId, trumpiaEntity.RequestId);
        }

        private static TrumpiaRequestDetailEntity GetTrumpiaRequest()
        {
            return new TrumpiaRequestDetailEntity
            {
                MessageId = "1",
                RequestId = "2",
                Description = "ABC",
                StatusCode = "123",
                ErrorMessage = "Error",
                SmsBody = "Hi"
            };
        }
    }
}