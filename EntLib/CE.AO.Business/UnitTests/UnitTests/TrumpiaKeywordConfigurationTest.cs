﻿using System.Collections.Generic;
using Microsoft.Practices.Unity;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using CE.AO.Business;
using Moq;
using CE.AO.DataAccess;
using CE.AO.Entities.TableStorageEntities;
using CE.AO.Logging;
using Microsoft.WindowsAzure.Storage.Table;

namespace UnitTests
{
    /// <summary>
    /// contains test methods for TrumpiaKeywordConfiguration class
    /// </summary>
    [TestClass]
    public class TrumpiaKeywordConfigurationTest
    {
        private UnityContainer _unityContainer;
        private TrumpiaKeywordConfiguration _trumpiaKeywordConfigurationManager;
        private Mock<ITableRepository> _tableRepositoryMock;

        /// <summary>
        /// Test Case Init
        /// </summary>
        [TestInitialize]
        public void Init()
        {
            AutoMapperConfig.AutoMapperMappings();
            _tableRepositoryMock = new Mock<ITableRepository>();
            _unityContainer = new UnityContainer();
            RegisterTypesForUnity.RegisterTypes(_unityContainer);
            var overrides = new ParameterOverrides
            {
                {"logModel", new LogModel {DisableLog = true} }
            };
            _trumpiaKeywordConfigurationManager = _unityContainer.Resolve<TrumpiaKeywordConfiguration>(overrides);
            _trumpiaKeywordConfigurationManager.TableRepository = _tableRepositoryMock.Object;
        }

        /// <summary>
        /// Test Case for accessing default constructor
        /// </summary>
        [TestMethod]
        public void TrumpiaKeywordConfiguration()
        {
            // ReSharper disable once ObjectCreationAsStatement
            new TrumpiaKeywordConfiguration();
            Assert.AreEqual(true, true);
        }

        /// <summary>
        /// Test Case for accessing Log Model
        /// </summary>
        [TestMethod]
        public void AccessLogModel()
        {
            _trumpiaKeywordConfigurationManager.LogModel.DisableLog = true;
            Assert.AreEqual(true, _trumpiaKeywordConfigurationManager.LogModel.DisableLog);
        }

        /// <summary>
        /// Test case for GetServiceLevelEvaluationsAsync method in TrumpiaKeywordConfiguration class
        /// </summary>
        [TestMethod]
        public void GetServiceLevelEvaluationsAsync()
        {
            var trumpiaKeywordConfigurationEntity = new TrumpiaKeywordConfigurationEntity
            {
                TrumpiaKeyword = "A",
                ClientId = "1"
            };
            IList<TrumpiaKeywordConfigurationEntity> list = new List<TrumpiaKeywordConfigurationEntity>();
            list.Add(trumpiaKeywordConfigurationEntity);
            var partitionKey = TrumpiaKeywordConfigurationEntity.SetPartitionKey("A");
            var filter = TableQuery.GenerateFilterCondition("PartitionKey", QueryComparisons.Equal,partitionKey);
            _tableRepositoryMock.Setup(t =>t.GetAllAsync<TrumpiaKeywordConfigurationEntity>(It.IsAny<string>(), filter)).Returns(list).Verifiable();
            var result = _trumpiaKeywordConfigurationManager.GetTrumpiaKeywordConfiguration("A");
            _tableRepositoryMock.Verify(t => t.GetAllAsync<TrumpiaKeywordConfigurationEntity>(It.IsAny<string>(), filter), Times.Once);
            _tableRepositoryMock.VerifyAll();
            Assert.AreEqual(result.ClientId, "1");
        }
    }
}