﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.Practices.Unity;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using CE.AO.Business;
using CE.AO.Business.Contract;
using Moq;
using CE.AO.DataAccess;
using CE.AO.Entities.TableStorageEntities;
using CE.AO.Logging;
using CE.AO.Models;
using CE.RateModel;

namespace UnitTests
{
    /// <summary>
    /// contains test methods for Notification class
    /// </summary>
    [TestClass]
    public class NotificationTest
    {
        private UnityContainer _unityContainer;

        private INotification _notificationManager;

        private Notification _notification;

        private Mock<INotification> _notificationMock;

        private Mock<ITableRepository> _tableRepositoryMock;

        private Mock<ITrumpiaRequestDetail> _trumpiaManager;

        private Mock<ISubscription> _subscriptionManager;

        /// <summary>
        /// Test Case Init
        /// </summary>
        [TestInitialize]
        public void Init()
        {
            AutoMapperConfig.AutoMapperMappings();
            _tableRepositoryMock = new Mock<ITableRepository>();
            //var m = MockTables.GetInstance();
            _unityContainer = new UnityContainer();
            RegisterTypesForUnity.RegisterTypes(_unityContainer);
            var overrides = new ParameterOverrides
            {
                {"logModel", new LogModel {DisableLog = true} }
            };
            _notificationManager = _unityContainer.Resolve<Notification>(overrides);
            _notification = _unityContainer.Resolve<Notification>(overrides);
            _trumpiaManager = new Mock<ITrumpiaRequestDetail>();
            _subscriptionManager = new Mock<ISubscription>();
            _notificationMock = new Mock<INotification>();
            _notification.SubscriptionManager = _subscriptionManager.Object;
            _notification.TrumpiaManager = _trumpiaManager.Object;
            _notification.TableRepository = _tableRepositoryMock.Object;
        }

        /// <summary>
        /// Test Case for accessing default constructor
        /// </summary>
        [TestMethod]
        public void Notification()
        {
            // ReSharper disable once ObjectCreationAsStatement
            new Notification();
            Assert.AreEqual(true, true);
        }

        /// <summary>
        /// Test Case for accessing Log Model
        /// </summary>
        [TestMethod]
        public void AccessLogModel()
        {
            _notificationManager.LogModel.DisableLog = true;
            Assert.AreEqual(true, _notificationManager.LogModel.DisableLog);
        }

        /// <summary>
        /// Test case to send notification using email
        /// </summary>
        [TestMethod]
        public void ProcessMessageWithChannelEmail()
        {
            var insightModel = GetInsightModel();
            var clientSettings = GetClientSettingsWithChannelEmail();
            var customerModel = GetCustomerModel();
            var billingModel = GetBillingModel();
            var subscriptionModelList = new List<SubscriptionModel> { GetSubscriptionModelWithChannelEmail() };
            var tierBoundary = GetTierBoundary();
            var listTierBoundary = new List<TierBoundary> { tierBoundary };
            var dicTierBoundary = new Dictionary<string, List<TierBoundary>>
            {
                {"SC001", listTierBoundary}
            };
            var calculationModelList = new List<CalculationModel> { GetCalculationModel() };
            _notificationManager.ProcessMessage(subscriptionModelList, insightModel, clientSettings, customerModel, billingModel, dicTierBoundary, calculationModelList);
            Assert.AreEqual(true,true);
        }

        /// <summary>
        /// Test case to send notification using email
        /// </summary>
        [TestMethod]
        public void ProcessMessageWithChannelSms()
        {
            var insightModel = GetInsightModel();
            var clientSettings = GetClientSettingsWithChannelSms();
            var customerModel = GetCustomerModel();
            var billingModel = GetBillingModel();
            var subscriptionModelList = new List<SubscriptionModel> { GetSubscriptionModelWithChannelSms() };
            var tierBoundary = GetTierBoundary();
            var listTierBoundary = new List<TierBoundary> { tierBoundary };
            var dicTierBoundary = new Dictionary<string, List<TierBoundary>>
            {
                {"SC001", listTierBoundary}
            };
            var calculationModelList = new List<CalculationModel> { GetCalculationModel() };
            _notificationManager.ProcessMessage(subscriptionModelList, insightModel, clientSettings, customerModel, billingModel, dicTierBoundary, calculationModelList);
            Assert.AreEqual(true, true);
        }

        /// <summary>
        /// Test case to send notification using email
        /// </summary>
        [TestMethod]
        public void ProcessMessageWithChannelEmailAndSms()
        {
            var insightModel = GetInsightModel();
            var clientSettings = GetClientSettingsWithChannelEmailAndSms();
            var customerModel = GetCustomerModel();
            var billingModel = GetBillingModel();
            var subscriptionModelList = new List<SubscriptionModel> { GetSubscriptionModelWithChannelEmailAndSms() };
            var tierBoundary = GetTierBoundary();
            var listTierBoundary = new List<TierBoundary> { tierBoundary };
            var dicTierBoundary = new Dictionary<string, List<TierBoundary>>
            {
                {"SC001", listTierBoundary}
            };
            var calculationModelList = new List<CalculationModel> { GetCalculationModel() };
            _notificationManager.ProcessMessage(subscriptionModelList, insightModel, clientSettings, customerModel, billingModel, dicTierBoundary, calculationModelList);
            Assert.AreEqual(true, true);
        }


        /// <summary>
        /// Test case to send notification using email
        /// </summary>
        [TestMethod]
        public void ProcessMessageWithChannelFile()
        {
            var insightModel = GetInsightModel();
            var clientSettings = GetClientSettingsWithChannelFile();
            var customerModel = GetCustomerModel();
            var billingModel = GetBillingModel();
            var subscriptionModelList = new List<SubscriptionModel> { GetSubscriptionModelWithChannelFile() };
            var tierBoundary = GetTierBoundary();
            var listTierBoundary = new List<TierBoundary> { tierBoundary };
            var dicTierBoundary = new Dictionary<string, List<TierBoundary>>
            {
                {"SC001", listTierBoundary}
            };
            var calculationModelList = new List<CalculationModel> { GetCalculationModel() };
            _notificationManager.ProcessMessage(subscriptionModelList, insightModel, clientSettings, customerModel, billingModel, dicTierBoundary, calculationModelList);
            Assert.AreEqual(true, true);
        }
        
        /// <summary>
        /// Test case for SendSms methods from Notification class
        /// </summary>
        [TestMethod]
        public void SendSms()
        {
            var billingModel = GetBillingModel();
            var customerModel = GetCustomerModel();
            var insightModel = GetInsightModel();
            var subscriptionModel = new SubscriptionModel
            {
                ClientId = 1,
                CustomerId = "1234",
                AccountId = "AC0001",
                ProgramName = "testprogram",
                InsightTypeName = "",
                PremiseId = "",
                ServiceContractId = "",
                ServicePointId = "",
                IsEmailOptInCompleted = false,
                IsSmsOptInCompleted = false,
                IsSelected = true,
                Channel = "SMS",
                AllowedChannels = "",
                SubscriberThresholdValue = 1234,
                CommodityKey = "Water",
                ThresholdType = "",
                ThresholdMin = 132,
                ThresholdMax = 136,
                IsDoubleOptIn = true,
                UomId = 12
            };
            var subscriptionModelList = new List<SubscriptionModel> { subscriptionModel };
            var notificationModel = new NotificationModel
            {
                ClientId = 1,
                AccountId = "AC0001",
                ServiceContractId = "",
                ProgramName = "testProgram",
                Insight = "BillToDate",
                IsNotified = false,
                IsDelivered = false,
                IsDropped = false,
                IsBounced = false,
                IsOpened = false,
                IsClicked = false,
                IsScheduled = false,
                Channel = "SMS",
                TemplateId = "",
                CustomerId = "1",
                NotifiedDateTime = DateTime.Now,
                TrumpiaRequestDetailPkRk = "ABC",
                Tier = "Tier1"
            };
            
            var customKey = "";
            var smsApiKey = "";
            var smsUserName = "";
            var smsBody = "";
            var programName = "";
            var insightTypeName = "";
            var requestDate = DateTime.Now;
            DateTime? notifyDateTime = DateTime.Now;
            var result = _notification.SendSms(billingModel, subscriptionModelList, notificationModel,customerModel, customKey, smsApiKey,smsUserName, smsBody, insightModel, programName, insightTypeName,requestDate, "", notifyDateTime);
            Assert.AreEqual(true, result.IsCompleted);
        }

        /// <summary>
        /// Test method to get SMS report
        /// </summary>
        [TestMethod]
        public void GetSmsReport()
        {
            var billingModel = GetBillingModel();
            var customerModel = GetCustomerModel();
            var subscriptionModel = new SubscriptionModel
            {
                ClientId = 1,
                CustomerId = "1234",
                AccountId = "AC0001",
                ProgramName = "testprogram",
                InsightTypeName = "",
                PremiseId = "",
                ServiceContractId = "",
                ServicePointId = "",
                IsEmailOptInCompleted = false,
                IsSmsOptInCompleted = false,
                IsSelected = true,
                Channel = "SMS",
                AllowedChannels = "",
                SubscriberThresholdValue = 1234,
                CommodityKey = "Water",
                ThresholdType = "",
                ThresholdMin = 132,
                ThresholdMax = 136,
                IsDoubleOptIn = true,
                UomId = 12
            };
            var subscriptionModelList = new List<SubscriptionModel> { subscriptionModel };

            var notificationModel = new NotificationModel
            {
                ClientId = 1,
                AccountId = "AC0001",
                ServiceContractId = "",
                ProgramName = "testProgram",
                Insight = "BillToDate",
                IsNotified = false,
                IsDelivered = false,
                IsDropped = false,
                IsBounced = false,
                IsOpened = false,
                IsClicked = false,
                IsScheduled = false,
                Channel = "SMS",
                TemplateId = "",
                CustomerId = "1",
                NotifiedDateTime = DateTime.Now,
                TrumpiaRequestDetailPkRk = "123"
            };
            var trumpiaRequestDetailEntity = new TrumpiaRequestDetailEntity()
            {
                MessageId = "1",
                RequestId = "1",
                Description = "Test Description",
                StatusCode = "1",
                ErrorMessage = " ",
                SmsBody = "Hello! This is trumpia."
            };
            _subscriptionManager.Setup(t => t.InsertorMergeForSmsOptOut(It.IsAny<BillingModel>(), It.IsAny<List<SubscriptionModel>>(), It.IsAny<int>(), It.IsAny<string>(),It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>())).Returns(true).Verifiable();
            _trumpiaManager.Setup(t => t.GetTrumpiaRequestDetailAsync(It.IsAny<string>())).Returns(Task.FromResult(trumpiaRequestDetailEntity)).Verifiable();
            var result = _notification.GetSmsReport(billingModel, subscriptionModelList, notificationModel, customerModel, "", "testapikey", "testusername", DateTime.Now);
            _trumpiaManager.Verify(t => t.GetTrumpiaRequestDetailAsync(It.IsAny<string>()), Times.Once);
            _trumpiaManager.VerifyAll();
            Assert.AreEqual(true, result.IsCompleted);
        }

        /// <summary>
        /// Test method to fetch the sms template from database
        /// </summary>
        [TestMethod]
        public void GetSmsTemplateFromDb()
        {
            var smsTemplate = GetSmsTemplate();
            _tableRepositoryMock.Setup(t => t.GetSingleAsync<SmsTemplateEntity>(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>())).Returns(Task.FromResult(smsTemplate)).Verifiable();
            var sms = _notification.GetSmsTemplateFromDB(1, "xyz", "2").Result;
            Assert.AreEqual(smsTemplate.ClientId, sms.ClientId);
        }

        /// <summary>
        /// Test case for GetDayNotifications method of Notification class
        /// </summary>
        [TestMethod]
        public void GetDayNotifications()
        {
            List<NotificationEntity> list = new List<NotificationEntity>();
            var notificationEntity = Mapper.Map<NotificationModel, NotificationEntity>(GetdayNotify());
            list.Add(notificationEntity);
            _tableRepositoryMock.Setup(t => t.GetAllAsync<NotificationEntity>(It.IsAny<string>(), It.IsAny<string>())).Returns(list).Verifiable();
            List<NotificationModel> result = _notification.GetDayNotifications(1, "", DateTime.Now).ToList();
            _tableRepositoryMock.Verify(t => t.GetAllAsync<NotificationEntity>(It.IsAny<string>(), It.IsAny<string>()), Times.Once);
            _tableRepositoryMock.VerifyAll();
            Assert.AreEqual(result.Count, 1);
        }

        /// <summary>
        /// Test case for GetDayNotification method of Notification class
        /// </summary>
        [TestMethod]
        public void GetNotification()
        {
            var notify = Mapper.Map<NotificationModel, NotificationEntity>(GetdayNotify());
            _tableRepositoryMock.Setup(t => t.GetSingleAsync<NotificationEntity>(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>())).Returns(Task.FromResult(notify)).Verifiable();
            var notification = _notification.GetNotification("1", "2").Result;
            Assert.AreEqual(notification.ClientId, 1);
        }

        /// <summary>
        /// Test method for GetDayNotificationSync of Notification class
        /// </summary>
        [TestMethod]
        public void GetNotificationSync()
        {
            var notify = Mapper.Map<NotificationModel, NotificationEntity>(GetdayNotify());
            _tableRepositoryMock.Setup(t => t.GetSingle<NotificationEntity>(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>())).Returns(notify).Verifiable();
            var notification = _notification.GetNotificationSync("1", "2");
            Assert.AreEqual(notification.ClientId, 1);
        }

        /// <summary>
        /// Test case for SaveNotificationFromEmail method of Notification class
        /// </summary>
        [TestMethod]
        public void SaveNotificationFromEmail()
        {
            var saveNotify = GetdayNotify();
            _tableRepositoryMock.Setup(t => t.InsertOrMergeSingle(It.IsAny<string>(), It.IsAny<NotificationEntity>())).Returns(true).Verifiable();
            var result = _notification.SaveNotificationFromEmail(saveNotify, "1", "2");
            _tableRepositoryMock.Verify(t => t.InsertOrMergeSingle(It.IsAny<string>(), It.IsAny<NotificationEntity>()), Times.Once);
            _tableRepositoryMock.VerifyAll();
            Assert.AreEqual(true, result);
        }

        /// <summary>
        /// Test case for SaveNotification method of Notification class
        /// </summary>
        [TestMethod]
        public void SaveNotification()
        {
            var saveNotify = GetdayNotify();
            _tableRepositoryMock.Setup(t => t.InsertOrMergeSingle(It.IsAny<string>(), It.IsAny<NotificationEntity>())).Returns(true).Verifiable();
            var result = _notification.SaveNotification(saveNotify, "1", "2");
            _tableRepositoryMock.Verify(t => t.InsertOrMergeSingle(It.IsAny<string>(), It.IsAny<NotificationEntity>()), Times.AtLeastOnce);
            _tableRepositoryMock.VerifyAll();
            Assert.AreEqual(true, result);
        }

        /// <summary>
        /// Test case for SaveNotification1 method of Notification class
        /// </summary>
        [TestMethod]
        public void SaveNotification1()
        {
            var saveNotify = GetdayNotify();
            _tableRepositoryMock.Setup(t => t.InsertOrMergeSingle(It.IsAny<string>(), It.IsAny<NotificationEntity>())).Returns(true).Verifiable();
            var result = _notification.SaveNotification(saveNotify);
            _tableRepositoryMock.Verify(t => t.InsertOrMergeSingle(It.IsAny<string>(), It.IsAny<NotificationEntity>()), Times.Once);
            _tableRepositoryMock.VerifyAll();
            Assert.AreEqual(true, result);
        }

        /// <summary>
        /// Test case for GetNotifications method of Notification class
        /// </summary>
        [TestMethod]
        public void GetNotifications()
        {
            var list = new List<NotificationEntity>();
            var notify = Mapper.Map<NotificationModel, NotificationEntity>(GetdayNotify());
            list.Add(notify);
            _tableRepositoryMock.Setup(t => t.GetAllAsync<NotificationEntity>(It.IsAny<string>(), It.IsAny<string>())).Returns(list).Verifiable();
            List<NotificationModel> result = _notification.GetNotifications(1, DateTime.Now, DateTime.Now).ToList();
            _tableRepositoryMock.Verify(t => t.GetAllAsync<NotificationEntity>(It.IsAny<string>(), It.IsAny<string>()), Times.Once);
            _tableRepositoryMock.VerifyAll();
            Assert.AreEqual(result.Count, 1);
        }

        [TestMethod]
        public void GetReplacedSmsTemplateForSubscriptionDoubleOptInConfirmation()
        {
            var smsBody = "Thank you for enrolling in -ProgramName-  alerts. To confirm your participation reply -Keyword- CONFIRM -AccountNumber-.";
            var substitutes = Regex.Matches(smsBody, "[-][a-zA-Z]+[-]");
            var subscriptionEntity =Mapper.Map<SubscriptionModel, SubscriptionEntity>(GetSubscriptionModelWithChannelEmail());
            var clientSetting = GetClientSettingsWithChannelEmail();
            var customerModel = GetCustomerModel();
            _notificationMock.Setup(n=>n.GetReplacedSmsTemplateForSubscriptionDoubleOptInConfirmation(It.IsAny<string>(), It.IsAny<SubscriptionEntity>(), It.IsAny<ClientSettings>(), It.IsAny<CustomerModel>(), It.IsAny<MatchCollection>())).Returns(smsBody).Verifiable();
            var result = _notification.GetReplacedSmsTemplateForSubscriptionDoubleOptInConfirmation(smsBody, subscriptionEntity,clientSetting, customerModel, substitutes);
            Assert.AreEqual(string.IsNullOrWhiteSpace(result), false);
        }

        private static SmsTemplateEntity GetSmsTemplate()
        {
            return new SmsTemplateEntity
            {
                ClientId = 1,
                InsightTypeName = "xyz",
                Body = "abcd",
                TemplateId = "2"
            };
        }
        
        private static NotificationModel GetdayNotify()
        {
            return new NotificationModel
            {
                ClientId = 1,
                AccountId = "AC0001",
                ServiceContractId = "",
                ProgramName = "testProgram",
                Insight = "BillToDate",
                IsNotified = false,
                IsDelivered = false,
                IsDropped = false,
                IsBounced = false,
                IsOpened = false,
                IsClicked = false,
                IsScheduled = false,
                Channel = "SMS",
                TemplateId = "",
                CustomerId = "1",
                NotifiedDateTime = DateTime.Now,
                Tier = "Tier1"
            };
        }

        private BillingModel GetBillingModel()
        {
            return new BillingModel
            {
                ClientId = 1,
                CustomerId = "1234",
                AccountId = "AC00012",
                PremiseId = "12",
                AmiStartDate = DateTime.Now,
                AmiEndDate = DateTime.Now,
                BillCycleScheduleId = "BS12122",
                RateClass = "Rate Class",
                ServicePointId = "SP0012",
                Source = "Rate Class",
                MeterType = "",
                UOMId = 12,
                TotalUsage = 1232323,
                StartDate = DateTime.Now,
                EndDate = DateTime.Today.AddDays(12),
                BillDays = 12,
                ReadQuality = "",
                TotalCost = 122323,
                CommodityId = 2,
                MeterId = " ",
                BillPeriodType = 2,
                ReplacedMeterId = "",
                ReadDate = DateTime.Now,
                ServiceContractId = "",
                IsFault = false,
            };
        }

        private CalculationModel GetCalculationModel()
        {
            return new CalculationModel
            {
                ClientId = 1,
                AccountId = "C700",
                CustomerId = "",
                MeterId = "",
                ServiceContractId = "",
                CommodityId = 1,
                Unit = 2,
                Cost = 22.3,
                ProjectedCost = 23,
                ProjectedUsage = 32,
                Usage = 2,
                BillDays = 23,
                BillCycleStartDate = DateTime.Now.AddDays(-2).ToShortDateString(),
                BillCycleEndDate = DateTime.Now.AddDays(10).ToShortDateString(),
                AsOfAmiDate = DateTime.Now,
                AsOfCalculationDate = DateTime.Now,
                RateClass = "",
                AverageDailyUsage = 23,
                AverageDailyCost = 23,
            };
        }

        private SubscriptionModel GetSubscriptionModelWithChannelEmail()
        {
            return new SubscriptionModel
            {
                ClientId = 1,
                CustomerId = "1234",
                AccountId = "AC0001",
                ProgramName = "testprogram",
                InsightTypeName = "",
                PremiseId = "",
                ServiceContractId = "",
                ServicePointId = "",
                IsEmailOptInCompleted = true,
                IsSmsOptInCompleted = true,
                IsSelected = true,
                Channel = "Email",
                AllowedChannels = "",
                SubscriberThresholdValue = 1234,
                CommodityKey = "Electric",
                ThresholdType = "",
                ThresholdMin = 132,
                ThresholdMax = 136,
                IsDoubleOptIn = true,
                UomId = 12,
            };
        }

        private static SubscriptionModel GetSubscriptionModelWithChannelSms()
        {
            return new SubscriptionModel
            {
                ClientId = 1,
                CustomerId = "1234",
                AccountId = "AC0001",
                ProgramName = "testprogram",
                InsightTypeName = "",
                PremiseId = "",
                ServiceContractId = "",
                ServicePointId = "",
                IsEmailOptInCompleted = true,
                IsSmsOptInCompleted = true,
                IsSelected = true,
                Channel = "SMS",
                AllowedChannels = "",
                SubscriberThresholdValue = 1234,
                CommodityKey = "Electric",
                ThresholdType = "",
                ThresholdMin = 132,
                ThresholdMax = 136,
                IsDoubleOptIn = true,
                UomId = 12
            };
        }

        private SubscriptionModel GetSubscriptionModelWithChannelEmailAndSms()
        {
            return new SubscriptionModel
            {
                ClientId = 1,
                CustomerId = "1234",
                AccountId = "AC0001",
                ProgramName = "testprogram",
                InsightTypeName = "",
                PremiseId = "",
                ServiceContractId = "",
                ServicePointId = "",
                IsEmailOptInCompleted = true,
                IsSmsOptInCompleted = true,
                IsSelected = true,
                Channel = "EmailAndSMS",
                AllowedChannels = "",
                SubscriberThresholdValue = 1234,
                CommodityKey = "Electric",
                ThresholdType = "",
                ThresholdMin = 132,
                ThresholdMax = 136,
                IsDoubleOptIn = true,
                UomId = 12,
            };
        }

        private SubscriptionModel GetSubscriptionModelWithChannelFile()
        {
            return new SubscriptionModel
            {
                ClientId = 1,
                CustomerId = "1234",
                AccountId = "AC0001",
                ProgramName = "testprogram",
                InsightTypeName = "",
                PremiseId = "",
                ServiceContractId = "",
                ServicePointId = "",
                IsEmailOptInCompleted = true,
                IsSmsOptInCompleted = true,
                IsSelected = true,
                Channel = "File",
                AllowedChannels = "",
                SubscriberThresholdValue = 1234,
                CommodityKey = "Electric",
                ThresholdType = "",
                ThresholdMin = 132,
                ThresholdMax = 136,
                IsDoubleOptIn = true,
                UomId = 12,
            };
        }

        private CustomerModel GetCustomerModel()
        {
            return new CustomerModel
            {
                ClientId = 1,
                CustomerId = "1234",
                Country = "USA",
                Source = "",
                IsBusiness = false,
                AddressLine1 = "C Block",
                AddressLine2 = "Flat no B-804",
                AddressLine3 = "Wakad",
                City = "Pune",
                State = "New york",
                PostalCode = "NY001",
                FirstName = "Sharad",
                LastName = "Saini",
                Phone1 = "9911559988",
                Phone2 = "8975757012",
                EmailAddress = "sharad.saini@saviantconsulting.com",
                CustomerType = "Good",
                TrumpiaSubscriptionId = "12"
            };
        }

        private TierBoundary GetTierBoundary()
        {
            return new TierBoundary
            {
                BaseOrTier = CE.RateModel.Enums.BaseOrTier.Tier6,
                TimeOfUse = CE.RateModel.Enums.TimeOfUse.OnPeak,
                Season = CE.RateModel.Enums.Season.SeasonB,
                DaysIntoSeason = 1,
                SeasonFactor = 1,
                Threshold = 2,
                SecondaryThreshold = 3
            };
        }

        private InsightModel GetInsightModel()
        {
            return new InsightModel
            {
                ClientId = 1,
                AccountId = "1234",
                ServiceContractId = "",
                BilllDays = 23,
                BtdCost = 2332,
                BtdProjectedCost = 3544,
                Usage = 32,
                SupressInsight = true,
                AccountLevelCostThresholdExceed = true,
                ServiceLevelUsageThresholdExceed = true,
                ServiceLevelCostThresholdExceed = true,
                DayThresholdExceed = true,
                ServiceLevelTieredThresholdExceed = false,
                ServiceLevelTiered1ThresholdApproaching = true,
                ServiceDays = 32,
                CtdCost = 4545,
                CtdProjectedCost = 5678,
                NotifyTime = DateTime.Now.ToShortDateString(),
                ProgramName = "testprogram",
                AsOfEvaluationDate = DateTime.Now,
                AsOfEvaluationTickCount = DateTime.Now.Ticks,
                BillCycleStartDate = "12 12",
                BillCycleEndDate = " 12 12 12"
            };
        }

        private ClientSettings GetClientSettingsWithChannelEmail()
        {
            var insightSettings = new InsightSettings
            {
                InsightName = "BillToDate",
                UtilityInsightName = "BillToDate",
                AllowedCommunicationChannel = "Email",
                DefaultCommunicationChannel = "Email",
                DefaultFrequency = "Daily",
                DefaultValue = "Daily",
                EmailTemplateId = "1",
                SmsTemplateId = "1",
                NotifyTime = "13:00",
                NotificationDay = "Monday",
                TrumpiaKeyword = "TrumpiaKeywordTest",
                Level = "Basic",
                CommodityType = "",
                ThresholdType = "TestThresholdType",
                ThresholdMin = "50",
                ThresholdMax = "100",
            };
            return GetClientSettings(insightSettings);
        }

        private ClientSettings GetClientSettingsWithChannelSms()
        {
            var insightSettings = new InsightSettings
            {
                InsightName = "BillToDate",
                UtilityInsightName = "BillToDate",
                AllowedCommunicationChannel = "SMS",
                DefaultCommunicationChannel = "SMS",
                DefaultFrequency = "Daily",
                DefaultValue = "Daily",
                EmailTemplateId = "1",
                SmsTemplateId = "1",
                NotifyTime = "13:00",
                NotificationDay = "Monday",
                TrumpiaKeyword = "TrumpiaKeywordTest",
                Level = "Basic",
                CommodityType = "",
                ThresholdType = "TestThresholdType",
                ThresholdMin = "50",
                ThresholdMax = "100",
            };
            return GetClientSettings(insightSettings);
        }

        private ClientSettings GetClientSettingsWithChannelEmailAndSms()
        {
            var insightSettings = new InsightSettings
            {
                InsightName = "BillToDate",
                UtilityInsightName = "BillToDate",
                AllowedCommunicationChannel = "Email",
                DefaultCommunicationChannel = "Email",
                DefaultFrequency = "Daily",
                DefaultValue = "Daily",
                EmailTemplateId = "1",
                SmsTemplateId = "1",
                NotifyTime = "13:00",
                NotificationDay = "Monday",
                TrumpiaKeyword = "TrumpiaKeywordTest",
                Level = "Basic",
                CommodityType = "",
                ThresholdType = "TestThresholdType",
                ThresholdMin = "50",
                ThresholdMax = "100",
            };
            return GetClientSettings(insightSettings);
        }

        private ClientSettings GetClientSettingsWithChannelFile()
        {
            var insightSettings = new InsightSettings
            {
                InsightName = "BillToDate",
                UtilityInsightName = "BillToDate",
                AllowedCommunicationChannel = "File",
                DefaultCommunicationChannel = "File",
                DefaultFrequency = "Daily",
                DefaultValue = "Daily",
                EmailTemplateId = "1",
                SmsTemplateId = "1",
                NotifyTime = "13:00",
                NotificationDay = "Monday",
                TrumpiaKeyword = "TrumpiaKeywordTest",
                Level = "Basic",
                CommodityType = "",
                ThresholdType = "TestThresholdType",
                ThresholdMin = "50",
                ThresholdMax = "100",
            };
            return GetClientSettings(insightSettings);
        }

        private ClientSettings GetClientSettings(InsightSettings insightSettings)
        {
            var insightSettingsList = new List<InsightSettings> { insightSettings };

            var programSettings = new ProgramSettings
            {
                ProgramName = "testprogram",
                UtilityProgramName = "testprogram",
                FileExportRequired = false,
                DoubleOptInRequired = false,
                TrumpiaKeyword = "trumpiatestkeyword",
                Insights = insightSettingsList
            };
            var programSettingsList = new List<ProgramSettings> { programSettings };

            return new ClientSettings
            {
                ClientId = 1,
                TimeZone = "ADT",
                IsAmiIntervalStart = false,
                IsDstHandled = false,
                MinimumDaysForInsightNotification = 1,
                CalculationScheduleTime = "",
                NotificationReportScheduleTime = "",
                InsightReportScheduleTime = "",
                ForgotPasswordEmailTemplateId = "",
                OptInEmailUserName = "UserNameTest",
                OptInEmailPassword = "test@123",
                OptInEmailFrom = "testemail@gmail.com",
                RegistrationConfirmationEmailTemplateId = "",
                OptInEmailConfirmationTemplateId = "",
                OptInSmsConfirmationTemplateId = "",
                TrumpiaShortCode = "",
                TrumpiaApiKey = "TrumpiaTestKey",
                TrumpiaUserName = "TrumpiaTestUserName",
                TrumpiaContactList = "",
                UnmaskedAccountIdEndingDigit = 1,
                Programs = new List<ProgramSettings>(programSettingsList)
            };
        }
    }
}
