﻿using System;
using System.Threading.Tasks;
using Microsoft.Practices.Unity;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using CE.AO.Business;
using CE.AO.Business.Contract;
using Moq;
using CE.AO.Logging;
using CE.AO.Models;

namespace UnitTests
{
    /// <summary>
    /// contains test methods for BillingFacade class
    /// </summary>
    [TestClass]
    public class BillingFacadeTest
    {
        private UnityContainer _unityContainer;
        private BillingFacade _billingFacadeManager;
        private Mock<ICustomer> _customerMock;
        private Mock<IClientAccount> _clientAccountMock;
        private Mock<IBilling> _billingMock;
        private Mock<IPremise> _premiseMock;
        private Mock<ICustomerMobileNumberLookup> _customerMobileNumberMock;
        private Mock<IAccountLookup> _accountLookupMock;

        /// <summary>
        /// Test Case Init
        /// </summary>
        [TestInitialize]
        public void Init()
        {
            AutoMapperConfig.AutoMapperMappings();
            _unityContainer = new UnityContainer();
            RegisterTypesForUnity.RegisterTypes(_unityContainer);
            var overrides = new ParameterOverrides
            {
                {"logModel", new LogModel {DisableLog = true}}
            };
            _customerMock = new Mock<ICustomer>();
            _clientAccountMock = new Mock<IClientAccount>();
            _billingMock = new Mock<IBilling>();
            _premiseMock = new Mock<IPremise>();
            _customerMobileNumberMock = new Mock<ICustomerMobileNumberLookup>();
            _accountLookupMock = new Mock<IAccountLookup>();
            _billingFacadeManager = _unityContainer.Resolve<BillingFacade>(overrides);
            
            _billingFacadeManager.CustomerManager = _customerMock.Object;
            _billingFacadeManager.ClientAccount = _clientAccountMock.Object;
            _billingFacadeManager.BillingManager = _billingMock.Object;
            _billingFacadeManager.PremiseManager = _premiseMock.Object;
            _billingFacadeManager.CustomerMobileNumberLookupManager = _customerMobileNumberMock.Object;
            _billingFacadeManager.AccountLookupManager = _accountLookupMock.Object;
        }

        /// <summary>
        /// Test Case for accessing default constructor
        /// </summary>
        [TestMethod]
        public void BillingFacade()
        {
            // ReSharper disable once ObjectCreationAsStatement
            new BillingFacade();
            Assert.AreEqual(true, true);
        }

        /// <summary>
        /// Test Case for accessing Log Model
        /// </summary>
        [TestMethod]
        public void AccessLogModel()
        {
            _billingFacadeManager.LogModel.DisableLog = true;
            Assert.AreEqual(true, _billingFacadeManager.LogModel.DisableLog);
        }

        /// <summary>
        /// Test method to Insert billing information
        /// </summary>
        [TestMethod]
        public void InsertOrMergeBillingAsync()
        {
            var billingCustomerPremiseModel = new BillingCustomerPremiseModel()
            {
                ClientId = 1,
                customer_id = "1",
                account_id = "1",
                meter_id = "1",
                first_name = "Sharad",
                last_name = "Saini",
                site_addressline1 = "B-804",
                site_addressline2 = "Akshay Tower",
                site_addressline3 = "New York",
                service_commodity = 2,
                phone_1 = "9911559988",
                email = "sharad.saini@saviantconsulting.com",
                meter_replaces_meterid = "2",
                premise_id = "B-804",
                Service_contract = "SC001",
                service_point_id = "SC001",
                bill_days = 20,
                service_read_date = DateTime.Now,
                bill_enddate = DateTime.Now,
                is_estimate = "0",
                customer_type = "Commercial",
                meter_type = "regular"
            };

            _customerMock.Setup(t => t.InsertOrMergeCustomerAsync(It.IsAny<CustomerModel>())).Returns(Task.FromResult(true)).Verifiable();
            _clientAccountMock.Setup(t => t.InsertOrMergeClientAccount(It.IsAny<ClientAccountModel>())).Returns(Task.FromResult(true)).Verifiable();
            
            var billingModel = new BillingModel()
            {
                ClientId = 1,
                CustomerId = "1",
                AccountId = "1",
                PremiseId = "B-804",
                AmiStartDate = DateTime.Now,
                AmiEndDate = DateTime.Now,
                BillCycleScheduleId = "BS12122",
                RateClass = "Rate Class",
                ServicePointId = "SP0012",
                Source = "Rate Class",
                MeterType = "",
                UOMId = 12,
                TotalUsage = 1232323,
                StartDate = DateTime.Now,
                EndDate = DateTime.Today.AddDays(12),
                BillDays = 12,
                ReadQuality = "0",
                TotalCost = 122323,
                CommodityId = 2,
                MeterId = " ",
                BillPeriodType = 2,
                ReplacedMeterId = "",
                ReadDate = DateTime.Now,
                ServiceContractId = "SC001",
                IsFault = false
            };

            _billingMock.Setup(t => t.GetServiceDetailsFromBillAsync(It.IsAny<int>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>())).Returns(Task.FromResult(billingModel)).Verifiable();
            _premiseMock.Setup(t => t.InsertOrMergePremiseAsync(It.IsAny<PremiseModel>())).Returns(Task.FromResult(true)).Verifiable();
            _customerMobileNumberMock.Setup(t => t.InsertOrMergeCustomerMobileNumberLookupAsync(It.IsAny<CustomerMobileNumberLookupModel>())).Returns(Task.FromResult(true)).Verifiable();
            _accountLookupMock.Setup(t => t.InsertOrMergeAccountLookupAsync(It.IsAny<AccountLookupModel>())).Returns(Task.FromResult(true)).Verifiable();
            var result = _billingFacadeManager.InsertOrMergeBillingAsync(billingCustomerPremiseModel);
            _customerMock.Verify(t => t.InsertOrMergeCustomerAsync(It.IsAny<CustomerModel>()), Times.Once);
            _customerMock.VerifyAll();
            
            _clientAccountMock.Verify(t => t.InsertOrMergeClientAccount(It.IsAny<ClientAccountModel>()), Times.Once);
            _clientAccountMock.VerifyAll();

            _billingMock.Verify(t => t.GetServiceDetailsFromBillAsync(It.IsAny<int>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>()), Times.Once);
            _billingMock.VerifyAll();

            _premiseMock.Verify(t => t.InsertOrMergePremiseAsync(It.IsAny<PremiseModel>()), Times.Once);
            _premiseMock.VerifyAll();

            _customerMobileNumberMock.Verify(t => t.InsertOrMergeCustomerMobileNumberLookupAsync(It.IsAny<CustomerMobileNumberLookupModel>()), Times.Once);
            _customerMobileNumberMock.VerifyAll();

            _accountLookupMock.Verify(t => t.InsertOrMergeAccountLookupAsync(It.IsAny<AccountLookupModel>()), Times.Once);
            _accountLookupMock.VerifyAll();
            Assert.AreEqual(true, result.Result);
        }
    }
}
