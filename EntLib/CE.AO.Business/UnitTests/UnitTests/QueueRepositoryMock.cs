﻿using System.Threading.Tasks;
using CE.AO.DataAccess;

namespace UnitTests
{
    class QueueRepositoryMock : IQueueRepository
    {
        public Task AddQueueAsync(string queueName, string message)
        {
            return Task.FromResult(true);
        }

        public Task<bool> CreateQueueAsync(string queueName)
        {
            return Task.FromResult(true);
        }

        public Task DeleteQueueAsync(string queueName)
        {
            return Task.FromResult(true);
        }
    }
}
