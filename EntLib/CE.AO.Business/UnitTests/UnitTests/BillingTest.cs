﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CE.AO.Business;
using CE.AO.DataAccess;
using CE.AO.Entities.TableStorageEntities;
using CE.AO.Models;
using Microsoft.Practices.Unity;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.WindowsAzure.Storage.Table;
using Moq;

namespace UnitTests
{
    /// <summary>
    /// Contains test methods for BillingTest Class
    /// </summary>
    /// <seealso cref="System.IDisposable" />
    [TestClass]
    public class BillingTest : IDisposable
    {
        /// <summary>
        /// The _unity container
        /// </summary>
        private UnityContainer _unityContainer;

        /// <summary>
        /// The _billing manager
        /// </summary>
        private Billing _billingManager;

        /// <summary>
        /// The _table repository mock
        /// </summary>
        private Mock<ITableRepository> _tableRepositoryMock;

        /// <summary>
        /// The _disposed
        /// </summary>
        private bool _disposed;

        /// <summary>
        /// Initializes this instance.
        /// </summary>
        [TestInitialize]
        public void Init()
        {
            AutoMapperConfig.AutoMapperMappings();
            _tableRepositoryMock = new Mock<ITableRepository>();
            _unityContainer = new UnityContainer();
            RegisterTypesForUnity.RegisterTypes(_unityContainer);
            _billingManager = _unityContainer.Resolve<Billing>();
            _billingManager.TableRepository = _tableRepositoryMock.Object;
        }

        /// <summary>
        /// Test case for accessesing log model
        /// </summary>
        [TestMethod]
        public void AccessLogModel()
        {
            _billingManager.LogModel.DisableLog = true;
            Assert.AreEqual(true, _billingManager.LogModel.DisableLog);
        }

        /// <summary>
        /// Test Case for default constructor of Billing model
        /// </summary>
        [TestMethod]
        public void Billing()
        {
            // ReSharper disable once ObjectCreationAsStatement
            new Billing();
            Assert.AreEqual(true, true);
        }

        /// <summary>
        /// Test case to fetch all services from last bill
        /// </summary>
        [TestMethod]
        public void GetAllServicesFromLastBill()
        {
            var billingEntity = GetSampleBillingEntity();
            IList<BillingEntity> list = new List<BillingEntity>();
            list.Add(billingEntity);
            _tableRepositoryMock.Setup(t =>t.GetAllAsync<BillingEntity>(It.IsAny<string>(), It.IsAny<string>())).Returns(list).Verifiable();
            IEnumerable<BillingModel> result = _billingManager.GetAllServicesFromLastBill(87, "C05", "Accn05");
            _tableRepositoryMock.Verify(t => t.GetAllAsync<BillingEntity>(It.IsAny<string>(), It.IsAny<string>()), Times.Once);
            _tableRepositoryMock.VerifyAll();
            Assert.AreEqual(result.ToList().Count, 1);
        }

        /// <summary>
        /// Test case to fetch all bills for service, latest or hisroty
        /// </summary>
        [TestMethod]
        public void GetBillsForService()
        {
            var billingEntity = GetSampleBillingEntity();
            IList<BillingEntity> list = new List<BillingEntity>();
            list.Add(billingEntity);
            var filter = TableQuery.CombineFilters(TableQuery.GenerateFilterCondition("PartitionKey", QueryComparisons.Equal, BillingEntity.SetPartitionKey(billingEntity.ClientId, billingEntity.CustomerId, billingEntity.AccountId)),TableOperators.And,
                TableQuery.CombineFilters(TableQuery.GenerateFilterCondition("RowKey", QueryComparisons.LessThanOrEqual, BillingEntity.SetHistoryRowKey(billingEntity.ServiceContractId, billingEntity.EndDate)),TableOperators.And,
                    TableQuery.GenerateFilterCondition("RowKey", QueryComparisons.GreaterThanOrEqual, BillingEntity.SetHistoryRowKey(billingEntity.ServiceContractId, billingEntity.StartDate))));
            _tableRepositoryMock.Setup(t =>t.GetAllAsync<BillingEntity>(It.IsAny<string>(), filter)).Returns(list).Verifiable();
            //latest or history
            List<BillingModel> result = _billingManager.GetBillsForService(87, "C05", "Accn05", "SC05", DateTime.Now.AddDays(-1), DateTime.Now).ToList();
            _tableRepositoryMock.Verify(t => t.GetAllAsync<BillingEntity>(It.IsAny<string>(), filter), Times.Once);
            _tableRepositoryMock.VerifyAll();
            Assert.AreEqual(result.Count, 1);
        }

        /// <summary>
        /// Gets the bills for service startdate.
        /// </summary>
        [TestMethod]
        public void GetBillsForServiceStartdate()
        {
            var billingEntity = GetSampleBillingEntity();
            IList<BillingEntity> list = new List<BillingEntity>();
            list.Add(billingEntity);
            var filter = TableQuery.CombineFilters(TableQuery.GenerateFilterCondition("PartitionKey", QueryComparisons.Equal, BillingEntity.SetPartitionKey(billingEntity.ClientId, billingEntity.CustomerId, billingEntity.AccountId)),TableOperators.And,
                  TableQuery.CombineFilters(TableQuery.GenerateFilterCondition("RowKey", QueryComparisons.Equal, BillingEntity.SetLatestRowKey(billingEntity.ServiceContractId)),TableOperators.Or,
                      TableQuery.GenerateFilterCondition("RowKey", QueryComparisons.GreaterThanOrEqual, BillingEntity.SetHistoryRowKey(billingEntity.ServiceContractId, billingEntity.StartDate))));
            _tableRepositoryMock.Setup(t =>t.GetAllAsync<BillingEntity>(It.IsAny<string>(), filter)).Returns(list).Verifiable();
            //latest or history
            List<BillingModel> result = _billingManager.GetBillsForService(87, "C05", "Accn05", "SC05", DateTime.Now.AddDays(-1)).ToList();
            _tableRepositoryMock.Verify(t => t.GetAllAsync<BillingEntity>(It.IsAny<string>(), filter), Times.Once);
            _tableRepositoryMock.VerifyAll();
            Assert.AreEqual(result.Count, 1);
        }

        /// <summary>
        /// Test case to fetch all history bills for service
        /// </summary>
        [TestMethod]
        public void GetBillsForServiceHistory()
        {
            var billingEntity = GetSampleBillingEntity();
            IList<BillingEntity> list = new List<BillingEntity>();
            list.Add(billingEntity);
            var filter = TableQuery.CombineFilters(TableQuery.GenerateFilterCondition("PartitionKey", QueryComparisons.Equal, BillingEntity.SetPartitionKey(billingEntity.ClientId, billingEntity.CustomerId, billingEntity.AccountId)),TableOperators.And,
                TableQuery.CombineFilters(TableQuery.GenerateFilterCondition("RowKey", QueryComparisons.LessThanOrEqual, BillingEntity.SetHistoryRowKey(billingEntity.ServiceContractId, billingEntity.EndDate)),TableOperators.And,
                    TableQuery.GenerateFilterCondition("RowKey", QueryComparisons.GreaterThanOrEqual, BillingEntity.SetHistoryRowKey(billingEntity.ServiceContractId, billingEntity.StartDate))));
            _tableRepositoryMock.Setup(t =>t.GetAllAsync<BillingEntity>(It.IsAny<string>(), filter)).Returns(list).Verifiable();
            List<BillingModel> billingResult = _billingManager.GetBillsForService(87, "C05", "Accn05", "SC05", DateTime.Now.AddDays(-1), DateTime.Now).ToList();
            _tableRepositoryMock.Verify(t => t.GetAllAsync<BillingEntity>(It.IsAny<string>(), filter), Times.Once);
            _tableRepositoryMock.VerifyAll();
            Assert.AreEqual(billingResult.Count, 1);
        }

        /// <summary>
        /// Test case to fetch all service details from bills
        /// </summary>
        [TestMethod]
        public void GetServiceDetailsFromBillAsync()
        {
            var billingEntity = GetSampleBillingEntity();
            var partitionKey = BillingEntity.SetPartitionKey(billingEntity.ClientId, billingEntity.CustomerId, billingEntity.AccountId);
            var rowKey = BillingEntity.SetLatestRowKey(billingEntity.ServiceContractId);
            _tableRepositoryMock.Setup(t =>t.GetSingleAsync<BillingEntity>(It.IsAny<string>(), partitionKey, rowKey)).Returns(Task.FromResult(billingEntity)).Verifiable();

            var result = _billingManager.GetServiceDetailsFromBillAsync(87, "C05", "Accn05", "SC05").Result;
            _tableRepositoryMock.Verify(t => t.GetSingleAsync<BillingEntity>(It.IsAny<string>(), partitionKey, rowKey), Times.Once);
            _tableRepositoryMock.VerifyAll();
            Assert.AreEqual(result.ClientId, 87);
        }

        /// <summary>
        /// Test case for inserting latest bill
        /// </summary>
        [TestMethod]
        public void InsertOrMergeLatestBillAsync()
        {
            var billingModel = GetBillingModel();
            _tableRepositoryMock.Setup(t => t.InsertOrMergeSingleAsync(It.IsAny<string>(), It.IsAny<BillingEntity>())).Returns(Task.FromResult(true)).Verifiable();
            var result = _billingManager.InsertOrMergeLatestBillAsync(billingModel).Result;
            _tableRepositoryMock.Verify(t => t.InsertOrMergeSingleAsync(It.IsAny<string>(), It.IsAny<BillingEntity>()), Times.Once);
            _tableRepositoryMock.VerifyAll();
            Assert.AreEqual(true, result);
        }

        /// <summary>
        /// Test case for inserting latest bill
        /// </summary>
        [TestMethod]
        public void InsertOrMergeHistoryBillAsync()
        {
            var billingModel = GetBillingModel();
            _tableRepositoryMock.Setup(t => t.InsertOrMergeSingleAsync(It.IsAny<string>(), It.IsAny<BillingEntity>())).Returns(Task.FromResult(true)).Verifiable();
            var result = _billingManager.InsertOrMergeHistoryBillAsync(billingModel).Result;
            _tableRepositoryMock.Verify(t => t.InsertOrMergeSingleAsync(It.IsAny<string>(), It.IsAny<BillingEntity>()), Times.Once);
            _tableRepositoryMock.VerifyAll();
            Assert.AreEqual(true, result);
        }

        /// <summary>
        /// Test case for deleting latest bill
        /// </summary>
        [TestMethod]
        public void DeleteLatestServiceDetailsAsync()
        {
            var billingModel = GetBillingModel();
            _tableRepositoryMock.Setup(t => t.InsertOrMergeSingleAsync(It.IsAny<string>(), It.IsAny<BillingEntity>())).Returns(Task.FromResult(true)).Verifiable();
            // ReSharper disable once UnusedVariable
            var result = _billingManager.InsertOrMergeLatestBillAsync(billingModel).Result;
            var partitionKey = BillingEntity.SetPartitionKey(billingModel.ClientId, billingModel.CustomerId, billingModel.AccountId);
            var rowKey = BillingEntity.SetLatestRowKey(billingModel.ServiceContractId);
            _tableRepositoryMock.Setup(t => t.DeleteSingleAsync<BillingEntity>(It.IsAny<string>(), partitionKey, rowKey)).Returns(Task.FromResult(true)).Verifiable();

            var billingResult = _billingManager.DeleteLatestServiceDetailsAsync(87, "C05", "Accn05", "SC05").Result;
            _tableRepositoryMock.Verify(t => t.DeleteSingleAsync<BillingEntity>(It.IsAny<string>(), partitionKey, rowKey), Times.Once);
            _tableRepositoryMock.VerifyAll();
            Assert.AreEqual(true, billingResult);
        }

        /// <summary>
        /// Test case for deleting history bill
        /// </summary>
        [TestMethod]
        public void DeleteHistoryServiceDetailsAsync()
        {
            var billingModel = GetBillingModel();
            _tableRepositoryMock.Setup(t => t.InsertOrMergeSingleAsync(It.IsAny<string>(), It.IsAny<BillingEntity>())).Returns(Task.FromResult(true)).Verifiable();
            // ReSharper disable once UnusedVariable
            var result = _billingManager.InsertOrMergeLatestBillAsync(billingModel).Result;
            var partitionKey = BillingEntity.SetPartitionKey(billingModel.ClientId, billingModel.CustomerId, billingModel.AccountId);
            var rowKey = BillingEntity.SetHistoryRowKey(billingModel.ServiceContractId, billingModel.EndDate);
            _tableRepositoryMock.Setup(t => t.DeleteSingleAsync<BillingEntity>(It.IsAny<string>(), partitionKey, rowKey)).Returns(Task.FromResult(true)).Verifiable();

            var billingResult = _billingManager.DeleteHistoryServiceDetailsAsync(87, "C05", "Accn05", "SC05", DateTime.Now).Result;
            _tableRepositoryMock.Verify(t => t.DeleteSingleAsync<BillingEntity>(It.IsAny<string>(), partitionKey, rowKey), Times.Once);
            _tableRepositoryMock.VerifyAll();
            Assert.AreEqual(true, billingResult);
        }

        /// <summary>
        /// Create and returns new BillingModel
        /// </summary>
        /// <returns>
        /// BillingModel
        /// </returns>
        private static BillingModel GetBillingModel()
        {
            return new BillingModel
            {
                ClientId = 87,
                CustomerId = "C05",
                AccountId = "Accn05",
                PremiseId = "P05",
                CommodityId = 3,
                MeterId = "M05",
                ServiceContractId = "SC05",
                StartDate = DateTime.Now.AddDays(-1),
                EndDate = DateTime.Now,
                TotalCost = 2093,
                BillDays = 30,
                Source = "Utility"
            };
        }

        /// <summary>
        /// Create and return new BillingEntity
        /// </summary>
        /// <returns>
        /// BillingEntity
        /// </returns>
        private static BillingEntity GetSampleBillingEntity()
        {
            return new BillingEntity
            {
                ClientId = 87,
                CustomerId = "C05",
                AccountId = "Accn05",
                PremiseId = "P05",
                CommodityId = 3,
                MeterId = "M05",
                ServiceContractId = "SC05",
                StartDate = DateTime.Now.AddDays(-1),
                EndDate = DateTime.Now,
                TotalCost = 2093,
                BillDays = 30,
                Source = "Utility"
            };
        }

        /// <summary>
        /// Releases unmanaged and - optionally - managed resources.
        /// </summary>
        /// <param name="disposing"><c>true</c> to release both managed and unmanaged resources; <c>false</c> to release only unmanaged resources.</param>
        private void Dispose(bool disposing)
        {
            if (!_disposed)
            {
                if (disposing)
                {
                    _unityContainer.Dispose();
                }
            }
            _disposed = true;
        }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            Dispose(true);
            // ReSharper disable once GCSuppressFinalizeForTypeWithoutDestructor
            GC.SuppressFinalize(this);
        }
    }
}
