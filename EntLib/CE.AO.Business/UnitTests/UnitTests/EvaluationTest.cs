﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Practices.Unity;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using CE.AO.Business;
using CE.AO.Business.Contract;
//using CE.AO.Business.Contract;
using Moq;
using CE.AO.DataAccess;
using CE.AO.Entities.TableStorageEntities;
using CE.AO.Logging;
using CE.AO.Models;
using CE.RateModel;

namespace UnitTests
{
    /// <summary>
    /// contains test methods for Evaluation class
    /// </summary>
    [TestClass]
    public class EvaluationTest
    {
        private UnityContainer _unityContainer;
        private Evaluation _evaluationManager;
        private IEvaluation _evaluation;
        private Mock<IEvaluation> _evaluationMock;
        private Mock<ITableRepository> _tableRepositoryMock;

        /// <summary>
        /// Test Case Init
        /// </summary>
        [TestInitialize]
        public void Init()
        {
            AutoMapperConfig.AutoMapperMappings();
            _tableRepositoryMock = new Mock<ITableRepository>();
            _unityContainer = new UnityContainer();
            RegisterTypesForUnity.RegisterTypes(_unityContainer);
            var overrides = new ParameterOverrides
            {
                {"logModel", new LogModel {DisableLog = true} }
            };
            _evaluationManager = _unityContainer.Resolve<Evaluation>(overrides);
            _evaluationManager.TableRepository = _tableRepositoryMock.Object;
            _evaluationMock = new Mock<IEvaluation>();
            _evaluation = _unityContainer.Resolve<IEvaluation>(overrides);
        }

        /// <summary>
        /// Test Case for accessing default constructor
        /// </summary>
        [TestMethod]
        public void Evaluation()
        {
            // ReSharper disable once UnusedVariable
            var evaluation = new Evaluation();
            Assert.AreEqual(true, true);
        }

        /// <summary>
        /// Test Case for accessing Log Model
        /// </summary>
        [TestMethod]
        public void AccessLogModel()
        {
            _evaluationManager.LogModel.DisableLog = true;
            Assert.AreEqual(true, _evaluationManager.LogModel.DisableLog);
        }

        /// <summary>
        /// Test case for EvaluateInsightAccountLevel method in Evaluation class
        /// </summary>
        [TestMethod]
        public void EvaluateInsightAccountLevel()
        {
            List<SubscriptionModel> listSubscriptionModels = GetSubscriptionModel();
            var billingModel = GetBillingModel();
            var listAmi = new List<dynamic> { GetAmi15MinuteIntervalModel() };
            var tier = new List<TierBoundary>
            {
                new TierBoundary{
                    BaseOrTier = CE.RateModel.Enums.BaseOrTier.Tier1,
                    DaysIntoSeason = 180,
                    Season = CE.RateModel.Enums.Season.Winter,
                    Threshold = 223,
                    SecondaryThreshold = 150
                }
            };

            //Account level
            List<CalculationModel> accountLevelCalculationModels = GetAccountLevelCalculationModel();
            var accountLevelClientSettings = GetAccountLevelClientSettings();
            _evaluationMock.Setup(t =>t.EvaluateInsight(It.IsAny<IEnumerable<SubscriptionModel>>(),It.IsAny<IEnumerable<CalculationModel>>(), It.IsAny<ClientSettings>(), It.IsAny<List<dynamic>>(),
                                      It.IsAny<List<TierBoundary>>(), It.IsAny<DateTime>(),It.IsAny<BillingModel>())).Returns(Task.FromResult(typeof(void)));
            _evaluation.EvaluateInsight(listSubscriptionModels, accountLevelCalculationModels,accountLevelClientSettings, listAmi, tier, DateTime.Now, billingModel);
            Assert.AreEqual(true, true);
        }

        /// <summary>
        /// Test case for EvaluateInsightServiceLevel method in Evaluation class
        /// </summary>
        [TestMethod]
        public void EvaluateInsightServiceLevel()
        {
            List<SubscriptionModel> listSubscriptionModels = GetSubscriptionModel();
            var billingModel = GetBillingModel();
            var listAmi = new List<dynamic> {GetAmi15MinuteIntervalModel()};
            var tier = new List<TierBoundary>
            {
                new TierBoundary
                {
                    BaseOrTier = CE.RateModel.Enums.BaseOrTier.Tier1,
                    DaysIntoSeason = 180,
                    Season = CE.RateModel.Enums.Season.Winter,
                    Threshold = 223,
                    SecondaryThreshold = 150
                }
            };

            //Service level
            List<CalculationModel> serviceLevelCalculationModels = GetServiceLevelCalculationModel();
            var serviceLevelClientSettings = GetServiceLevelClientSettings();
            _evaluationMock.Setup(t =>t.EvaluateInsight(It.IsAny<IEnumerable<SubscriptionModel>>(),It.IsAny<IEnumerable<CalculationModel>>(), It.IsAny<ClientSettings>(), It.IsAny<List<dynamic>>(),It.IsAny<List<TierBoundary>>(), It.IsAny<DateTime>(), It.IsAny<BillingModel>())).Returns(Task.FromResult(typeof(void)));
            _evaluation.EvaluateInsight(listSubscriptionModels, serviceLevelCalculationModels,serviceLevelClientSettings, listAmi, tier, DateTime.Now, billingModel);

            Assert.AreEqual(true, true);
        }

        /// <summary>
        /// Test case for GetServiceLevelEvaluationsAsync method in Evaluation class
        /// </summary>
        [TestMethod]
        public void GetServiceLevelEvaluationsAsync()
        {
            var serviceEval = GetInsightEntity("sc05", "S");
            _tableRepositoryMock.Setup(t => t.GetSingleAsync<InsightEntity>(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>())).Returns(Task.FromResult(serviceEval)).Verifiable();
            var evaluation = _evaluationManager.GetServiceLevelEvaluationsAsync(1, "accn05", "Program1", "sc05").Result;
            _tableRepositoryMock.Verify(t => t.GetSingleAsync<InsightEntity>(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>()), Times.Once);
            _tableRepositoryMock.VerifyAll();
            Assert.AreEqual(evaluation.ClientId, 87);
        }

        /// <summary>
        /// Test case for GetAccountLevelEvaluationsAsync method in Evaluation class
        /// </summary>
        [TestMethod]
        public void GetAccountLevelEvaluationsAsync()
        {
            var accountEval = GetInsightEntity("sc05", "S");
            _evaluationManager.TableRepository = _tableRepositoryMock.Object;
            _tableRepositoryMock.Setup(t => t.GetSingleAsync<InsightEntity>(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>())).Returns(Task.FromResult(accountEval)).Verifiable();
            var evaluation = _evaluationManager.GetAccountLevelEvaluationsAsync(1, "accn05", "Program1").Result;
            Assert.AreEqual(evaluation.ClientId, 87);
        }

        /// <summary>
        /// Test case for GetDayInsights method in Evaluation class
        /// </summary>
        [TestMethod]
        public void GetDayInsights()
        {
            IList<InsightEntity> list = new List<InsightEntity>();
            list.Add(GetHistoryInsightEntity("sc05", "S"));
            _tableRepositoryMock.Setup(t =>t.GetAllAsync<InsightEntity>(It.IsAny<string>(), It.IsAny<string>())).Returns(list).Verifiable();
            List<InsightModel> result = _evaluationManager.GetDayInsights(87, "a1", DateTime.Now).ToList();
            _tableRepositoryMock.Verify(t => t.GetAllAsync<InsightEntity>(It.IsAny<string>(), It.IsAny<string>()), Times.Once);
            _tableRepositoryMock.VerifyAll();
            Assert.AreEqual(result.Count, 1);
        }
        
        private static InsightEntity GetInsightEntity(string serviceContractId, string type)
        {
            return new InsightEntity
            {
                PartitionKey = "87_accn05",
                RowKey = string.IsNullOrWhiteSpace(serviceContractId) ? type + "_Program1" : type + "_Program1_" + serviceContractId,
                ClientId = 87,
                AccountId = "accn05",
                ServiceContractId = serviceContractId,
                ProgramName = "Program1",
                AsOfEvaluationDate = DateTime.Now,
                AsOfEvaluationTickCount = 12,
                BillCycleStartDate = DateTime.Now.AddDays(-30).ToShortDateString(),
                BillCycleEndDate = DateTime.Now.ToShortDateString(),
                DayThresholdExceed = false,
                ServiceLevelTieredThresholdExceed = false,
                BtdCost = 128.20,
                BtdProjectedCost = 12.2,
                ServiceLevelCostThresholdExceed = false,
                ServiceLevelUsageThresholdExceed = false,
                Usage = 12.50,
                CtdCost = 12.50,
                CtdProjectedCost = 12.50
            };
        }

        private static InsightEntity GetHistoryInsightEntity(string serviceContractId, string type)
        {
            return new InsightEntity
            {
                PartitionKey = "87_a1",
                RowKey = DateTime.Now.ToString("yyyy-MM-dd") + "_" + type + (string.IsNullOrWhiteSpace(serviceContractId) ? "_Program1" : "_Program1_" + serviceContractId),
                ClientId = 87,
                AccountId = "a1",
                ServiceContractId = serviceContractId,
                ProgramName = "Program1",
                AsOfEvaluationDate = DateTime.Now,
                AsOfEvaluationTickCount = 12,
                BillCycleStartDate = DateTime.Now.AddDays(-30).ToShortDateString(),
                BillCycleEndDate = DateTime.Now.ToShortDateString(),
                DayThresholdExceed = false,
                ServiceLevelTieredThresholdExceed = false,
                BtdCost = 128.20,
                BtdProjectedCost = 12.2,
                ServiceLevelCostThresholdExceed = false,
                ServiceLevelUsageThresholdExceed = false,
                Usage = 12.50,
                CtdCost = 12.50,
                CtdProjectedCost = 12.50
            };
        }

        /// <summary>
        /// Create and returns new SubscriptionModel
        /// </summary>
        /// <returns>List of SubscriptionModel</returns>
        private static List<SubscriptionModel> GetSubscriptionModel()
        {
            return new List<SubscriptionModel>
            {
                new SubscriptionModel
                {
                    ClientId = 87,
                    CustomerId = "C05",
                    AccountId = "Accn05",
                    PremiseId = "P05",
                    ProgramName = "Program1",
                    InsightTypeName = "BillToDate",
                    ServiceContractId = "",
                    ServicePointId="2",
                    IsSelected = true,
                    Channel = "Email",
                    IsEmailOptInCompleted = true,
                    UomId = 3,
                 },
                new SubscriptionModel
                {
                    ClientId = 87,
                    CustomerId = "C05",
                    AccountId = "Accn05",
                    PremiseId = "P05",
                    ProgramName = "Program1",
                    InsightTypeName = "ServiceLevelUsageThreshold",
                    ServiceContractId = "SC05",
                    IsSelected = true,
                    ServicePointId="2",
                    Channel = "Email",
                    IsEmailOptInCompleted = true,
                    UomId = 3,
                },
                new SubscriptionModel
                {
                    ClientId = 87,
                    CustomerId = "C05",
                    AccountId = "Accn05",
                    PremiseId = "P05",
                    ProgramName = "Program1",
                    InsightTypeName = "DayThreshold",
                    ServiceContractId = "SC05",
                    IsSelected = true,
                    ServicePointId="2",
                    Channel = "Email",
                    IsEmailOptInCompleted = true,
                    UomId = 3
                },
                new SubscriptionModel
                {
                    ClientId = 87,
                    CustomerId = "C05",
                    AccountId = "Accn05",
                    PremiseId = "P05",
                    ProgramName = "Program1",
                    InsightTypeName = "ServiceLevelTieredThresholdApproaching",
                    ServiceContractId = "SC05",
                    IsSelected = true,
                    ServicePointId="2",
                    Channel = "Email",
                    IsEmailOptInCompleted = true,
                    UomId = 3
                },
                new SubscriptionModel
                {
                    ClientId = 87,
                    CustomerId = "C05",
                    AccountId = "Accn05",
                    PremiseId = "P05",
                    ProgramName = "Program1",
                    InsightTypeName = "ServiceLevelTieredThresholdExceed",
                    ServiceContractId = "SC05",
                    ServicePointId="2",
                    IsSelected = true,
                    Channel = "Email",
                    IsEmailOptInCompleted = true,
                    UomId = 3
                }
            };
        }

        /// <summary>
        /// Create and returns account level new CalculationModel list
        /// </summary>
        /// <returns>List of CalculationModel</returns>
        private static List<CalculationModel> GetAccountLevelCalculationModel()
        {
            return new List<CalculationModel>
            {
                new CalculationModel
                {
                    ClientId = 87,
                    AccountId = "Accn05",
                    CustomerId = "C05",
                    MeterId = "M05",
                    ServiceContractId = "",
                    CommodityId = 3,
                    Unit = 78,
                    Cost = 105.245,
                    ProjectedCost = 3333,
                    ProjectedUsage = 222,
                    Usage = 22,
                    BillDays = 67,
                    BillCycleStartDate = DateTime.Now.ToString(CultureInfo.InvariantCulture),
                    BillCycleEndDate = DateTime.Now.ToString(CultureInfo.InvariantCulture),
                    AsOfAmiDate = DateTime.Now,
                    AsOfCalculationDate = DateTime.Now,
                    RateClass = "C",
                    AverageDailyUsage = 6785,
                    AverageDailyCost = 98
                }
            };
        }

        /// <summary>
        /// Create and returns service level new CalculationModel list
        /// </summary>
        /// <returns>List of CalculationModel</returns>
        private static List<CalculationModel> GetServiceLevelCalculationModel()
        {
            return new List<CalculationModel>
            {
                new CalculationModel
                {
                    ClientId = 87,
                    AccountId = "Accn05",
                    CustomerId = "C05",
                    MeterId = "M05",
                    ServiceContractId = "SC05",
                    CommodityId = 3,
                    Unit = 78,
                    Cost = 105.245,
                    ProjectedCost = 3333,
                    ProjectedUsage = 222,
                    Usage = 22,
                    BillDays = 67,
                    BillCycleStartDate = DateTime.Now.ToString(CultureInfo.InvariantCulture),
                    BillCycleEndDate = DateTime.Now.ToString(CultureInfo.InvariantCulture),
                    AsOfAmiDate = DateTime.Now,
                    AsOfCalculationDate = DateTime.Now,
                    RateClass = "C",
                    AverageDailyUsage = 6785,
                    AverageDailyCost = 98
                }
            };
        }

        /// <summary>
        /// Create and returns new account level ClientSettings
        /// </summary>
        /// <returns>ClientSettings</returns>
        private static ClientSettings GetAccountLevelClientSettings()
        {
            return new ClientSettings
            {
                ClientId = 2,
                MinimumDaysForInsightNotification = 20,
                TimeZone = "EST",
                OptInEmailUserName = "AclaraMdm",
                OptInEmailPassword = "AclaraMdm@123",
                OptInEmailFrom = "Support@aclara.com",
                Programs = new List<ProgramSettings>
                {
                    new ProgramSettings
                    {
                        ProgramName = "Program1",
                        UtilityProgramName = "Program1",
                        DoubleOptInRequired = true,
                        Insights = new List<InsightSettings>
                        {
                            new InsightSettings
                            {
                                InsightName = "BillToDate",
                                UtilityInsightName = "BillToDate",
                                AllowedCommunicationChannel = "Email",
                                DefaultCommunicationChannel = "Email",
                                CommodityType = "Water",
                                EmailTemplateId = "sd120d025",
                                DefaultValue = "210",
                                Level = "Account"
                            },
                            new InsightSettings
                            {
                                InsightName = "AccountProjectedCost",
                                UtilityInsightName = "AccountProjectedCost",
                                AllowedCommunicationChannel = "Email",
                                DefaultCommunicationChannel = "Email",
                                CommodityType = "Water",
                                EmailTemplateId = "sd120d026",
                                DefaultValue = "165",
                                Level = "Account"
                            },
                            new InsightSettings
                            {
                                InsightName = "AccountLevelCostThreshold",
                                UtilityInsightName = "AccountLevelCostThreshold",
                                AllowedCommunicationChannel = "Email",
                                DefaultCommunicationChannel = "Email",
                                CommodityType = "Water",
                                EmailTemplateId = "sd120d027",
                                DefaultValue = "198",
                                Level = "Account"
                            }
                        }
                    }
                }
            };
        }

        /// <summary>
        /// Create and returns new service level ClientSettings
        /// </summary>
        /// <returns>ClientSettings</returns>
        private static ClientSettings GetServiceLevelClientSettings()
        {
            return new ClientSettings
            {
                ClientId = 2,
                MinimumDaysForInsightNotification = 20,
                TimeZone = "EST",
                OptInEmailUserName = "AclaraMdm",
                OptInEmailPassword = "AclaraMdm@123",
                OptInEmailFrom = "Support@aclara.com",
                Programs = new List<ProgramSettings>
                {
                    new ProgramSettings
                    {
                         ProgramName = "Program1",
                         UtilityProgramName = "Program1",
                         DoubleOptInRequired = true,
                         Insights = new List<InsightSettings>
                         {
                            new InsightSettings
                            {
                                InsightName = "CostToDate",
                                UtilityInsightName = "CostToDate",
                                AllowedCommunicationChannel = "Email",
                                DefaultCommunicationChannel = "Email",
                                CommodityType = "Water",
                                EmailTemplateId = "sd120d028",
                                DefaultValue = "210",
                                Level = "Service"
                            },
                            new InsightSettings
                            {
                                InsightName = "ServiceProjectedCost",
                                UtilityInsightName = "ServiceProjectedCost",
                                AllowedCommunicationChannel = "Email",
                                DefaultCommunicationChannel = "Email",
                                CommodityType = "Water",
                                EmailTemplateId = "sd120d029",
                                DefaultValue = "165",
                                Level = "Service"
                            },
                            new InsightSettings
                            {
                                InsightName = "Usage",
                                UtilityInsightName = "Usage",
                                AllowedCommunicationChannel = "Email",
                                DefaultCommunicationChannel = "Email",
                                CommodityType = "Water",
                                EmailTemplateId = "sd120d030",
                                DefaultValue = "198",
                                Level = "Service"
                            },
                            new InsightSettings
                            {
                                InsightName = "ServiceLevelUsageThreshold",
                                UtilityInsightName = "ServiceLevelUsageThreshold",
                                AllowedCommunicationChannel = "Email",
                                DefaultCommunicationChannel = "Email",
                                CommodityType = "Water",
                                EmailTemplateId = "sd120d031",
                                DefaultValue = "250",
                                Level = "Service"
                            },
                            new InsightSettings
                            {
                                InsightName = "DayThreshold",
                                UtilityInsightName = "DayThreshold",
                                AllowedCommunicationChannel = "Email",
                                DefaultCommunicationChannel = "Email",
                                CommodityType = "Water",
                                EmailTemplateId = "sd120d032",
                                DefaultValue = "150",
                                Level = "Service"
                            },
                            new InsightSettings
                            {
                                InsightName = "ServiceLevelTieredThresholdApproaching",
                                UtilityInsightName = "ServiceLevelTieredThresholdApproaching",
                                AllowedCommunicationChannel = "Email",
                                DefaultCommunicationChannel = "Email",
                                CommodityType = "Water",
                                EmailTemplateId = "sd120d033",
                                DefaultValue = "177",
                                Level = "Service"
                            },
                            new InsightSettings
                            {
                                InsightName = "ServiceLevelTieredThresholdExceed",
                                UtilityInsightName = "ServiceLevelTieredThresholdExceed",
                                AllowedCommunicationChannel = "Email",
                                DefaultCommunicationChannel = "Email",
                                CommodityType = "Water",
                                EmailTemplateId = "sd120d034",
                                DefaultValue = "150",
                                Level = "Service"
                            },
                            new InsightSettings
                            {
                                InsightName = "ServiceLevelCostThreshold",
                                UtilityInsightName = "ServiceLevelCostThreshold",
                                AllowedCommunicationChannel = "Email",
                                DefaultCommunicationChannel = "Email",
                                CommodityType = "Water",
                                EmailTemplateId = "sd120d035",
                                DefaultValue = "130",
                                Level = "Service"
                            }
                         }
                    }
                }
            };
        }

        /// <summary>
        /// Create and returns new BillingModel
        /// </summary>
        /// <returns>BillingModel</returns>
        private static BillingModel GetBillingModel()
        {
            return new BillingModel
            {
                ClientId = 87,
                CustomerId = "C05",
                AccountId = "Accn05",
                PremiseId = "P05",
                CommodityId = 3,
                MeterId = "M05",
                ServiceContractId = "SC05",
                StartDate = DateTime.Now.AddDays(-1),
                EndDate = DateTime.Now,
                TotalCost = 2093,
                BillDays = 30,
                Source = "Utility",
            };
        }

        /// <summary>
        /// Create and returns new Ami15MinuteIntervalModel
        /// </summary>
        /// <returns>Ami15MinuteIntervalModel</returns>
        private static Ami15MinuteIntervalModel GetAmi15MinuteIntervalModel()
        {
            return new Ami15MinuteIntervalModel
            {
                AccountNumber = "Accn05",
                AmiTimeStamp = DateTime.Now,
                ClientId = 87,
                UOMId = 3,
                CommodityId = 3,
                MeterId = "M05",
                IntervalType = 15,
                IntValue0000 = 10,
                IntValue0015 = 15,
                IntValue0030 = 30,
                IntValue0045 = 45,
                IntValue0100 = 100
            };
        }
    }
}
