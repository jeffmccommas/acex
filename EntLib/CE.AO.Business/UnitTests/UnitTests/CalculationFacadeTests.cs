﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Runtime.Remoting.Messaging;
using System.Threading.Tasks;
using CE.AO.Business;
using CE.AO.Business.Contract;
using CE.AO.DataAccess;
using CE.AO.Logging;
using CE.AO.Models;
using CE.RateModel;
using Microsoft.Practices.Unity;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace UnitTests
{
    /// <summary>
    /// Contains test methods for CalculationFacade Class
    /// </summary>
    /// <seealso cref="System.IDisposable" />
    [TestClass]
    public class CalculationFacadeTest : IDisposable
    {
        /// <summary>
        /// The _unity container
        /// </summary>
        private UnityContainer _unityContainer;

        /// <summary>
        /// The _calculation facade manager
        /// </summary>
        private CalculationFacade _calculationFacadeManager;

        /// <summary>
        /// The _calculation mock
        /// </summary>
        private Mock<ICalculation> _calculationMock;

        /// <summary>
        /// The _billing mock
        /// </summary>
        private Mock<IBilling> _billingMock;

        /// <summary>
        /// The _ami mock
        /// </summary>
        private Mock<IAMI> _amiMock;

        /// <summary>
        /// The _billing cycle schedule mock
        /// </summary>
        private Mock<IBillingCycleSchedule> _billingCycleScheduleMock;

        /// <summary>
        /// The _account lookup mock
        /// </summary>
        private Mock<IAccountLookup> _accountLookupMock;

        /// <summary>
        /// The _account updates mock
        /// </summary>
        private Mock<IAccountUpdates> _accountUpdatesMock;

        /// <summary>
        /// The _queue repository mock
        /// </summary>
        private Mock<IQueueRepository> _queueRepositoryMock;

        /// <summary>
        /// The _client account mock
        /// </summary>
        private Mock<IClientAccount> _clientAccountMock;

        /// <summary>
        /// The _disposed
        /// </summary>
        private bool _disposed;

        /// <summary>
        /// Test Case Init
        /// </summary>
        [TestInitialize]
        public void Init()
        {
            _unityContainer = new UnityContainer();
            RegisterTypesForUnity.RegisterTypes(_unityContainer);
            var overrides = new ParameterOverrides
            {
                {"logModel", new LogModel {DisableLog = true} }
            };
            _calculationFacadeManager = _unityContainer.Resolve<CalculationFacade>(overrides);
            _calculationMock = new Mock<ICalculation>();
            _billingMock = new Mock<IBilling>();
            _amiMock = new Mock<IAMI>();
            _billingCycleScheduleMock = new Mock<IBillingCycleSchedule>();
            _accountLookupMock = new Mock<IAccountLookup>();
            _accountUpdatesMock = new Mock<IAccountUpdates>();
            _queueRepositoryMock = new Mock<IQueueRepository>();
            _clientAccountMock = new Mock<IClientAccount>();
            _calculationFacadeManager.CalculationManager = _calculationMock.Object;
            _calculationFacadeManager.AccountLookupManager = _accountLookupMock.Object;
            _calculationFacadeManager.AccountManager = _accountUpdatesMock.Object;
            _calculationFacadeManager.AmiManager = _amiMock.Object;
            _calculationFacadeManager.BillingCycleScheduleManager = _billingCycleScheduleMock.Object;
            _calculationFacadeManager.BillingManager = _billingMock.Object;
            _calculationFacadeManager.QueueRepository = _queueRepositoryMock.Object;
            _calculationFacadeManager.ClientAccount = _clientAccountMock.Object;
        }

        /// <summary>
        /// Test Case for accessing Log Model
        /// </summary>
        [TestMethod]
        public void AccessLogModel()
        {
            _calculationFacadeManager.LogModel.DisableLog = true;
            Assert.AreEqual(true, _calculationFacadeManager.LogModel.DisableLog);
        }

        /// <summary>
        /// Test Case for default constructor of CalculationFacade model
        /// </summary>
        [TestMethod]
        public void CalculationFacade()
        {
            // ReSharper disable once ObjectCreationAsStatement
            new CalculationFacade();
            Assert.AreEqual(true, true);
        }

        /// <summary>
        /// Test case for CalculateCostToDate method in CalculationFacade class
        /// </summary>
        [TestMethod]
        public void CalculateCostToDate()
        {
            var listBillingModels = new List<BillingModel>
            {
                new BillingModel
                {
                    ClientId = 87,
                    MeterId = "METER_QA_02",
                    CustomerId = "123123",
                    AccountId = "A",
                    ServiceContractId = "sc1",
                    MeterType = "ami",
                    StartDate = DateTime.Now.AddDays(-5),
                    EndDate = DateTime.Now.AddDays(-2),
                    CommodityId = 2
                }
            };
            var listAmi15MinuteIntervalModels = new List<Ami15MinuteIntervalModel>
            {
                new Ami15MinuteIntervalModel
                {
                    UOMId = 10
                }
            };
            _accountLookupMock.Setup(t => t.GetAccountMeterDetails(It.IsAny<int>(), It.IsAny<string>())).Returns(new AccountLookupModel
            {
                ServiceContractId = "sc1"
            });
            _billingMock.Setup(t => t.GetAllServicesFromLastBill(It.IsAny<int>(), It.IsAny<string>(), It.IsAny<string>())).Returns(listBillingModels);
            _billingCycleScheduleMock.Setup(t => t.GetBillingCycleScheduleByDateAsync(It.IsAny<int>(), It.IsAny<string>(), It.IsAny<DateTime>())).Returns(new BillCycleScheduleModel
            {
                BeginDate = DateTime.Now.AddDays(-2).ToString(CultureInfo.InvariantCulture),
                EndDate = DateTime.Now.AddDays(-1).ToString(CultureInfo.InvariantCulture)
            });
            _accountUpdatesMock.Setup(t => t.getAccountUpdate(It.IsAny<int>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(),
                        It.IsAny<bool>())).Returns(Task.FromResult(new AccountUpdatesModel
                        {
                            UpdateSentDate = DateTime.Now
                        }));
            _amiMock.Setup(t => t.GetAmiList(It.IsAny<DateTime>(), It.IsAny<DateTime>(), It.IsAny<string>(), It.IsAny<int>())).Returns(listAmi15MinuteIntervalModels);
            int noOfDays;
            _amiMock.Setup(t => t.GetReadings(It.IsAny<IEnumerable<dynamic>>(), out noOfDays)).Returns(new List<Reading>());
            _calculationMock.Setup(t => t.InsertCtdCalculationAsync(It.IsAny<CalculationModel>())).Returns(Task.FromResult(typeof(void)));
            _queueRepositoryMock.Setup(t => t.AddQueueAsync(It.IsAny<string>(), It.IsAny<string>())).Returns(Task.FromResult(typeof(void)));
            // ReSharper disable once UnusedVariable
            var result = _calculationFacadeManager.CalculateCostToDate(87, "METER_QA_02", DateTime.Now.Date);
        }

        /// <summary>
        /// Test case for CalculateBillToDate method in CalculationFacade class
        /// </summary>
        [TestMethod]
        public void CalculateBillToDate()
        {
            var listBillingModels = new List<BillingModel>
            {
                new BillingModel
                {
                    ClientId = 87,
                    MeterId = "METER02",
                    CustomerId = "A",
                    AccountId = "A",
                    ServiceContractId = "A",
                    MeterType = "ami",
                    StartDate = DateTime.Now.AddDays(-5),
                    EndDate = DateTime.Now.AddDays(-2),
                    CommodityId = 2,
                    TotalUsage = 100,
                    TotalCost = 100,
                    RateClass = "C"
                },
                new BillingModel
                {
                    ClientId = 87,
                    MeterId = "METER02",
                    CustomerId = "A",
                    AccountId = "A",
                    ServiceContractId = "A",
                    MeterType = "nonmetered",
                    StartDate = DateTime.Now.AddDays(-5),
                    EndDate = DateTime.Now.AddDays(-2),
                    CommodityId = 2,
                    TotalUsage = 100,
                    TotalCost = 100,
                    RateClass = "C"
                }
            };

            var listAmi15MinuteIntervalModels = new List<Ami15MinuteIntervalModel>
            {
                new Ami15MinuteIntervalModel
                {
                    UOMId = 10
                }
            };
            var listReading = new List<Reading>
            {
                new Reading(new DateTime(DateTime.Now.AddDays(-3).Year,DateTime.Now.AddDays(-3).Month,DateTime.Now.AddDays(-3).Day,0,15,0 ),100)
            };
            _billingMock.Setup(t => t.GetAllServicesFromLastBill(It.IsAny<int>(), It.IsAny<string>(), It.IsAny<string>())).Returns(listBillingModels);
            _billingCycleScheduleMock.Setup(t => t.GetBillingCycleScheduleByDateAsync(It.IsAny<int>(), It.IsAny<string>(), It.IsAny<DateTime>())).Returns(new BillCycleScheduleModel
            {
                BeginDate = DateTime.Now.AddDays(-2).ToString(CultureInfo.InvariantCulture),
                EndDate = DateTime.Now.AddDays(-1).ToString(CultureInfo.InvariantCulture)
            });
            _amiMock.Setup(t => t.GetAmiList(It.IsAny<DateTime>(), It.IsAny<DateTime>(), It.IsAny<string>(), It.IsAny<int>())).Returns(listAmi15MinuteIntervalModels);
            int noOfDays;
            _amiMock.Setup(t => t.GetReadings(It.IsAny<IEnumerable<dynamic>>(), out noOfDays)).Returns(listReading);
            _accountUpdatesMock.Setup(t => t.getAccountUpdate(It.IsAny<int>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(),
                        It.IsAny<bool>())).Returns(Task.FromResult(new AccountUpdatesModel
                        {
                            UpdateSentDate = DateTime.Now.AddDays(-3)
                        }));
            _calculationMock.Setup(t => t.InsertCtdCalculationAsync(It.IsAny<CalculationModel>())).Returns(Task.FromResult(typeof(void)));
            _calculationMock.Setup(t => t.InsertBtdCalculationAsync(It.IsAny<CalculationModel>())).Returns(Task.FromResult(typeof(void)));
            _queueRepositoryMock.Setup(t => t.AddQueueAsync(It.IsAny<string>(), It.IsAny<string>())).Returns(Task.FromResult(typeof(void)));
            // ReSharper disable once UnusedVariable
            var result = _calculationFacadeManager.CalculateBillToDate(87, "A", "AA", DateTime.Now);
           // Assert.AreEqual(result.Id == 3 ? 3 : 1, result.Id);
        }

        /// <summary>
        /// Test case for ScheduledCalculation method in CalculationFacade class
        /// </summary>
        [TestMethod]
        public void ScheduledCalculation()
        {
            _clientAccountMock.Setup(t => t.sendScheduleMsssage(It.IsAny<int>(), It.IsAny<string>()));
            _calculationFacadeManager.ScheduledCalculation(It.IsAny<int>(), It.IsAny<string>());
            _clientAccountMock.Verify(t => t.sendScheduleMsssage(It.IsAny<int>(), It.IsAny<string>()), Times.Once);
        }

        /// <summary>
        /// Test case for GetClientAccount method in CalculationFacade class
        /// </summary>
        [TestMethod]
        public void GetClientAccount()
        {
            _clientAccountMock.Setup(t => t.GetClientAccounts(It.IsAny<int>())).Returns(new List<ClientAccountModel>());
            _calculationFacadeManager.GetClientAccount(It.IsAny<int>());
            _clientAccountMock.Verify(t => t.GetClientAccounts(It.IsAny<int>()), Times.Once);
        }

        /// <summary>
        /// Cleans up method.
        /// </summary>
        [TestCleanup]
        public void CleanUpMethod()
        {
            CallContext.FreeNamedDataSlot("log4net.Util.LogicalThreadContextProperties");
        }

        /// <summary>
        /// Releases unmanaged and - optionally - managed resources.
        /// </summary>
        /// <param name="disposing"><c>true</c> to release both managed and unmanaged resources; <c>false</c> to release only unmanaged resources.</param>
        private void Dispose(bool disposing)
        {
            if (!_disposed)
            {
                if (disposing)
                {
                    _unityContainer.Dispose();
                }
            }
            _disposed = true;
        }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            Dispose(true);
            // ReSharper disable once GCSuppressFinalizeForTypeWithoutDestructor
            GC.SuppressFinalize(this);
        }
    }
}