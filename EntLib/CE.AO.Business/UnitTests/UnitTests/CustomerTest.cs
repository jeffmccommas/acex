﻿using System.Runtime.Remoting.Messaging;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.Practices.Unity;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using CE.AO.Business;
using CE.AO.Business.Contract;
using Moq;
using CE.AO.DataAccess;
using CE.AO.Entities.TableStorageEntities;
using CE.AO.Logging;
using CE.AO.Models;

namespace UnitTests
{
    /// <summary>
    /// Test cases for Customer class
    /// </summary>
    [TestClass]
    public class CustomerTest
    {
        private UnityContainer _unityContainer;
        private Customer _customerManager;
        private ICustomer _customer;
        private Mock<ITableRepository> _tableRepositoryMock;

        /// <summary>
        /// Test Case Init
        /// </summary>
        [TestInitialize]
        public void Init()
        {
            AutoMapperConfig.AutoMapperMappings();
            _tableRepositoryMock = new Mock<ITableRepository>();
            _unityContainer = new UnityContainer();
            RegisterTypesForUnity.RegisterTypes(_unityContainer);
            var overrides = new ParameterOverrides
            {
                {"logModel", new LogModel {DisableLog = true} }
            };
            _customerManager = _unityContainer.Resolve<Customer>(overrides);
            _customer = _unityContainer.Resolve<ICustomer>(overrides);
            _customerManager.TableRepository = _tableRepositoryMock.Object;
        }

        /// <summary>
        /// Test Case for accessing default constructor
        /// </summary>
        [TestMethod]
        public void Customer()
        {
            // ReSharper disable once ObjectCreationAsStatement
            new Customer();
            Assert.AreEqual(true, true);
        }

        /// <summary>
        /// Test Case for accessing Log Model
        /// </summary>
        [TestMethod]
        public void AccessLogModel()
        {
            _customerManager.LogModel.DisableLog = true;
            Assert.AreEqual(true, _customerManager.LogModel.DisableLog);
        }

        /// <summary>
        /// Test case for GetCustomerAsync method of Customer class
        /// </summary>
        [TestMethod]
        public void GetCustomerAsync()
        {

            var customerEntity = Mapper.Map<CustomerModel, CustomerEntity>(GetCustomerEntity());
            _tableRepositoryMock.Setup(t => t.GetSingleAsync<CustomerEntity>(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>())).Returns(Task.FromResult(customerEntity)).Verifiable();
            var customer = _customerManager.GetCustomerAsync(87, "1012").Result;
            Assert.AreEqual(customer.ClientId, 87);
        }

        /// <summary>
        /// Test case for InsertOrMergeCustomerAsync method of Customer class
        /// </summary>
        [TestMethod]
        public void InsertOrMergeCustomerAsync()
        {
            var customerEntity = GetCustomerEntity();
            _tableRepositoryMock.Setup(t => t.InsertOrMergeSingleAsync(It.IsAny<string>(), It.IsAny<CustomerEntity>())).Returns(Task.FromResult(true)).Verifiable();
            var result = _customerManager.InsertOrMergeCustomerAsync(customerEntity).Result;
            _tableRepositoryMock.Verify(t => t.InsertOrMergeSingleAsync(It.IsAny<string>(), It.IsAny<CustomerEntity>()), Times.Once);
            _tableRepositoryMock.VerifyAll();
            Assert.AreEqual(true, result);
        }

        /// <summary>
        /// Test case for CreateSubscriptionAndUpdateCustomer method of Customer class
        /// </summary>
        [TestMethod]
        public void CreateSubscriptionAndUpdateCustomer()
        {
            var customerEntity = GetCustomerEntity();
            //Mock<SendSms> sms = new Mock<SendSms>();
            var result = _customer.CreateSubscriptionAndUpdateCustomer(customerEntity, "apiKey", "userName", "contactList");
            Assert.AreEqual(result, false);
        }

        /// <summary>
        /// Test case for SmsSubscirptionCheckAndUpdate method of Customer class
        /// </summary>
        [TestMethod]
        public void SmsSubscirptionCheckAndUpdate()
        {
            var smsSubCheck = GetCustomerEntity();
            var str = true;
            var result = _customer.SmsSubscirptionCheckAndUpdate(smsSubCheck, "1", "ert", "ct", ref str);
            Assert.AreEqual(result, true);
        }

        /// <summary>
        /// Test case for SmsSubscirptionCheckAndUpdate1 method of Customer class
        /// </summary>
        [TestMethod]
        public void SmsSubscirptionCheckAndUpdate1()
        {
            var smsSubCheck1 = GetCustomerEntity1();
            var str = true;
            var result1 = _customer.SmsSubscirptionCheckAndUpdate(smsSubCheck1, "1", "rt", "t", ref str);
            Assert.AreEqual(result1, false);
        }

        /// <summary>
        /// Test case Clenup
        /// </summary>
        [TestCleanup]
        public void CleanUpMethod()
        {
            CallContext.FreeNamedDataSlot("log4net.Util.LogicalThreadContextProperties");
        }

        private static CustomerModel GetCustomerEntity()
        {
            return new CustomerModel
            {
                AddressLine1 = "",
                AddressLine2 = "",
                AddressLine3 = "",
                City = "",
                ClientId = 87,
                Country = "US",
                CustomerId = "1012",
                CustomerType = "Residential",
                EmailAddress = "",
                FirstName = "Mark",
                IsBusiness = false,
                LastName = "s",
                Phone1 = "12323",
                Phone2 = "",
                PostalCode = "9041",
                Source = "Utility",
                State = "NJ",
                TrumpiaSubscriptionId = "1",
                TrumpiaRequestDetailPkRk = "2"
            };
        }

        private static CustomerModel GetCustomerEntity1()
        {
            return new CustomerModel
            {
                AddressLine1 = "",
                AddressLine2 = "",
                AddressLine3 = "",
                City = "",
                ClientId = 87,
                Country = "US",
                CustomerId = "1012",
                CustomerType = "Residential",
                EmailAddress = "",
                FirstName = "Mark",
                IsBusiness = false,
                LastName = "s",
                Phone1 = "",
                Phone2 = "",
                PostalCode = "9041",
                Source = "Utility",
                State = "NJ",
                TrumpiaSubscriptionId = "1",
                TrumpiaRequestDetailPkRk = "2"
            };
        }
    }
}
