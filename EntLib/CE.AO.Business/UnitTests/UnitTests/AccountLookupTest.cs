﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using CE.AO.Business;
using CE.AO.DataAccess;
using CE.AO.Entities.TableStorageEntities;
using CE.AO.Models;
using Microsoft.Practices.Unity;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.WindowsAzure.Storage.Table;
using Moq;

namespace UnitTests
{
    /// <summary>
    /// Contains test methods for AccountLookup Class
    /// </summary>
    /// <seealso cref="System.IDisposable" />
    [TestClass]
    public class AccountLookupTest : IDisposable
    {
        /// <summary>
        /// The _disposed
        /// </summary>
        private bool _disposed;

        /// <summary>
        /// The _unity container
        /// </summary>
        private UnityContainer _unityContainer;

        /// <summary>
        /// The _account lookup manager
        /// </summary>
        private AccountLookup _accountLookupManager;

        /// <summary>
        /// The _table repository mock
        /// </summary>
        private Mock<ITableRepository> _tableRepositoryMock;

        /// <summary>
        /// Initializes this instance.
        /// </summary>
        [TestInitialize]
        public void Init()
        {
            AutoMapperConfig.AutoMapperMappings();
            _tableRepositoryMock = new Mock<ITableRepository>();
            _unityContainer = new UnityContainer();
            RegisterTypesForUnity.RegisterTypes(_unityContainer);
            _accountLookupManager = _unityContainer.Resolve<AccountLookup>();
            _accountLookupManager.TableRepository = _tableRepositoryMock.Object;
        }

        /// <summary>
        /// Test Case for accessing Log Model
        /// </summary>
        [TestMethod]
        public void AccessLogModel()
        {
            _accountLookupManager.LogModel.DisableLog = true;
            Assert.AreEqual(true, _accountLookupManager.LogModel.DisableLog);
        }

        /// <summary>
        /// Test Case for default constructor of AccountLookup model
        /// </summary>
        [TestMethod]
        public void AccountLookup()
        {
            // ReSharper disable once ObjectCreationAsStatement
            new AccountLookup();
            Assert.AreEqual(true, true);
        }

        /// <summary>
        /// Test case to get account meter details based on ClientId and MeterId
        /// </summary>
        [TestMethod]
        public void GetAccountMeterDetails()
        {
            var accountLookupEntity = GetSampleAccountLookupEntity();
            IList<AccountLookupEntity> list = new List<AccountLookupEntity>();
            list.Add(accountLookupEntity);
            var partitionKey = AccountLookupEntity.SetPartitionKey(1, "M0001");
            var filter = TableQuery.GenerateFilterCondition("PartitionKey", QueryComparisons.Equal,partitionKey);
            _tableRepositoryMock.Setup(t => t.GetAllAsync<AccountLookupEntity>(It.IsAny<string>(), filter)).Returns(list).Verifiable();
            var result = _accountLookupManager.GetAccountMeterDetails(1, "M0001");
            _tableRepositoryMock.Verify(t => t.GetAllAsync<AccountLookupEntity>(It.IsAny<string>(), filter), Times.Exactly(1));
            _tableRepositoryMock.VerifyAll();
            var accountLookupModel = Mapper.Map<AccountLookupEntity, AccountLookupModel>(accountLookupEntity);
            PropertyValuesAreEquals(result, accountLookupModel);
        }

        /// <summary>
        /// Test case to get account meter details based on ClientId, MeterId and CustomerId
        /// </summary>
        [TestMethod]
        public void GetAccountMeterDetailsByCustomerId()
        {
            var accountLookupEntity = GetSampleAccountLookupEntity();
            var partitionKey = AccountLookupEntity.SetPartitionKey(1, "M0001");
            var rowKey = AccountLookupEntity.SetRowKey("132323");
            _tableRepositoryMock.Setup(t => t.GetSingleAsync<AccountLookupEntity>(It.IsAny<string>(), partitionKey, rowKey)).Returns(Task.FromResult(accountLookupEntity)).Verifiable();
            var result = _accountLookupManager.GetAccountMeterDetails(1, "M0001", "132323");
            _tableRepositoryMock.Verify(t => t.GetSingleAsync<AccountLookupEntity>(It.IsAny<string>(), partitionKey, rowKey), Times.Exactly(1));
            _tableRepositoryMock.VerifyAll();
            var accountLookupModel = Mapper.Map<AccountLookupEntity, AccountLookupModel>(accountLookupEntity);
            PropertyValuesAreEquals(result.Result, accountLookupModel);
        }

        /// <summary>
        /// Test Case to insert client meter
        /// </summary>
        [TestMethod]
        public void InsertOrMergeAccountLookupAsync()
        {
            var accountLookupModel = Mapper.Map<AccountLookupEntity, AccountLookupModel>(GetSampleAccountLookupEntity());
            _tableRepositoryMock.Setup(t => t.InsertOrMergeSingleAsync(It.IsAny<string>(), It.IsAny<AccountLookupEntity>()))
                .Returns(Task.FromResult(true))
                .Verifiable();
            var result = _accountLookupManager.InsertOrMergeAccountLookupAsync(accountLookupModel).Result;
            _tableRepositoryMock.Verify(t => t.InsertOrMergeSingleAsync(It.IsAny<string>(), It.IsAny<AccountLookupEntity>()), Times.Once);
            _tableRepositoryMock.VerifyAll();
            Assert.AreEqual(true, result);
        }

        /// <summary>
        /// Properties the values are equals.
        /// </summary>
        /// <param name="actual">The actual.</param>
        /// <param name="expected">The expected.</param>
        /// <returns></returns>
        private static void PropertyValuesAreEquals(object actual, object expected)
        {
            var properties = expected.GetType().GetProperties();
            foreach (var property in properties)
            {
                var expectedValue = property.GetValue(expected, null);
                var actualValue = property.GetValue(actual, null);

                if (!Equals(expectedValue, actualValue))
                {
                }
            }
        }

        /// <summary>
        /// Gets the sample account lookup entity.
        /// </summary>
        /// <returns></returns>
        private static AccountLookupEntity GetSampleAccountLookupEntity()
        {
            return new AccountLookupEntity()
            {
                ClientId = 1,
                AccountId = "A0001",
                MeterId = "M0001",
                IsActive = true,
                ServiceContractId = "SC001",
                CustomerId = "132323",
                IsServiceContractCreatedBySystem = false
            };
        }

        /// <summary>
        /// Releases unmanaged and - optionally - managed resources.
        /// </summary>
        /// <param name="disposing"><c>true</c> to release both managed and unmanaged resources; <c>false</c> to release only unmanaged resources.</param>
        private void Dispose(bool disposing)
        {
            if (!_disposed)
            {
                if (disposing)
                {
                    _unityContainer.Dispose();
                }
            }
            _disposed = true;
        }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            Dispose(true);
            // ReSharper disable once GCSuppressFinalizeForTypeWithoutDestructor
            GC.SuppressFinalize(this);
        }
    }
}
