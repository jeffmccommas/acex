﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CE.AO.DataAccess;
using Microsoft.WindowsAzure.Storage.Table;
using System.Linq.Dynamic;
using UnitTests.MockData;

namespace UnitTests
{
    public class TableRepositoryMock : ITableRepository
    {
        private List<T> GetEntity<T>(string tableName)
        {
            var list = MockTables.GetInstance();
            var properties = list.GetType().GetProperties();
            var listValues = properties.Where(p => p.Name == tableName).ToList().FirstOrDefault()?.GetValue(list);
            return (List<T>)listValues;
        }

        private string SetFilter(string filter)
        {
            return filter.Replace(" eq ", "==").Replace(" lt ", "<").Replace(" gt ", ">").Replace(" le ", "<=").Replace(" ge ", ">=").Replace("'", "\"");
        }

        public bool InsertOrMergeSingle<T>(string tableName, T updateData) where T : ITableEntity, new()
        {
            if (tableName == "Ami")
                tableName = "AmiEntity";
            var entityList = GetEntity<T>(tableName);
            T oldEntity = new T();
            foreach (var property in updateData.GetType().GetProperties())
            {
                oldEntity.GetType()
                    .GetProperties()
                    .First(e => e.Name == property.Name)
                    .SetValue(oldEntity, property.GetValue(updateData));
            }
            var entity = entityList.Find(e => e.PartitionKey == oldEntity.PartitionKey && e.RowKey == oldEntity.RowKey);

            if (entity != null)
                entityList.Remove(entity);
            entityList.Add(oldEntity);
            return true;
        }

        public Task<bool> DeleteSingleAsync<T>(string tableName, string partitionKey, string rowKey) where T : ITableEntity, new()
        {
            var entityList = GetEntity<T>(tableName);
            var entity = entityList.Remove(entityList.Find(e => e.PartitionKey == partitionKey && e.RowKey == rowKey));
            return Task.FromResult(entity);
        }

        public IEnumerable<T> GetAllAsync<T>(string tableName, string filter) where T : ITableEntity, new()
        {
            var entityList = GetEntity<T>(tableName);
            var entity = entityList.Where(SetFilter(filter));
            return entity;
        }

        public Task<T> GetSingleAsync<T>(string tableName, string partitionKey, string rowKey) where T : ITableEntity, new()
        {
            var entityList = GetEntity<T>(tableName);
            var entity = entityList.FirstOrDefault(e => e.PartitionKey == partitionKey && e.RowKey == rowKey);
            return Task.FromResult(entity);
        }

        public bool DeleteSingle<T>(string tableName, string partitionKey, string rowKey) where T : ITableEntity, new()
        {
            var entityList = GetEntity<T>(tableName);
            var entity = entityList.Remove(entityList.Find(e => e.PartitionKey == partitionKey && e.RowKey == rowKey));
            return entity;
        }

        public T GetSingle<T>(string tableName, string partitionKey, string rowKey) where T : ITableEntity, new()
        {
            var entityList = GetEntity<T>(tableName);
            var entity = entityList.FirstOrDefault(e => e.PartitionKey == partitionKey && e.RowKey == rowKey);
            return entity;
        }

        public Task<bool> InsertAsync<T>(T tableModel, string tableName)
        {
            if (tableName == "Ami")
                tableName = "AmiEntity";
            var entityList = GetEntity<T>(tableName);
            entityList.Add(tableModel);
            return Task.FromResult(true);
        }

        public Task<bool> InsertBatchAsync<T>(List<T> tableModel, string tableName)
        {
            var entityList = GetEntity<T>(tableName);
            entityList.AddRange(tableModel);
            return Task.FromResult(true);
        }

        public Task<bool> InsertOrMergeBatchAsync<T>(List<T> tableModel, string tableName)
        {
            if (tableName == "Ami")
                tableName = "AmiEntity";
            var entityList = GetEntity<T>(tableName);
            var tempEntity = entityList.Select(entity => entity as ITableEntity).ToList();
            foreach (var item in tableModel)
            {
                var objTableEntity = item as ITableEntity;
                //var tempEntity = entityList as List<ITableEntity>;
                var entity = tempEntity.Find(e => e.PartitionKey == objTableEntity.PartitionKey && e.RowKey == objTableEntity.RowKey);
                if (entity != null)
                    entityList.Remove((T)entity);
                entityList.Add(item);
            }
            return Task.FromResult(true);
        }

        public Task<bool> InsertOrMergeSingleAsync<T>(string tableName, T updateData) where T : ITableEntity, new()
        {
            if (tableName == "Ami")
                tableName = "AmiEntity";
            var entityList = GetEntity<T>(tableName);
            T oldEntity = new T();
            foreach (var property in updateData.GetType().GetProperties())
            {
                oldEntity.GetType()
                    .GetProperties()
                    .First(e => e.Name == property.Name)
                    .SetValue(oldEntity, property.GetValue(updateData));
            }
            var entity = entityList.Find(e => e.PartitionKey == oldEntity.PartitionKey && e.RowKey == oldEntity.RowKey);

            if (entity != null)
                entityList.Remove(entity);
            entityList.Add(oldEntity);
            return Task.FromResult(true);
        }

        public Task<bool> InsertOrReplaceSingleAsync<T>(string tableName, T updateData) where T : ITableEntity, new()
        {
            if (tableName == "Ami")
                tableName = "AmiEntity";
            var entityList = GetEntity<T>(tableName);
            T oldEntity = new T();
            foreach (var property in updateData.GetType().GetProperties())
            {
                oldEntity.GetType()
                    .GetProperties()
                    .First(e => e.Name == property.Name)
                    .SetValue(oldEntity, property.GetValue(updateData));
            }
            var entity = entityList.Find(e => e.PartitionKey == oldEntity.PartitionKey && e.RowKey == oldEntity.RowKey);

            if (entity != null)
                entityList.Remove(entity);
            entityList.Add(oldEntity);
            return Task.FromResult(true);
        }

        public Task<bool> ReplaceSingleAsync<T>(string tableName, string partitionKey, string rowKey, T updateData, List<string> propertiesToUpdate) where T : ITableEntity, new()
        {
            if (tableName == "Ami")
                tableName = "AmiEntity";
            var entityList = GetEntity<T>(tableName);
            var entity = entityList.Find(e => e.PartitionKey == updateData.PartitionKey && e.RowKey == updateData.RowKey);
            if (entity != null)
                entityList.Remove(entity);
            entityList.Add(updateData);
            return Task.FromResult(true);
        }

        public Task<bool> CreateTableAsync(string tableName)
        {
            throw new NotImplementedException();
        }

        public Task DeleteTableAsync(string tableName)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<T> GetAllAsync<T>(string tableName, TableQuery<T> query) where T : ITableEntity, new()
        {
            throw new NotImplementedException();
        }

        public List<DynamicTableEntity> GetScheduleMessage(string tableName, int maxEntitiesToFetch, string filterString, ref TableContinuationToken token)
        {
            throw new NotImplementedException();
        }

        public List<DynamicTableEntity> GetAllAsync<T>(string tableName, string filter, int maxEntitiesToFetch, ref TableContinuationToken token) where T : ITableEntity, new()
        {
            throw new NotImplementedException();
            //var entityList = GetEntity<T>(tableName);
            //var entity = entityList.Where(SetFilter(filter));
            //return entity;
        }

    }
}
