﻿using System;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.Practices.Unity;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using CE.AO.Business;
using Moq;
using CE.AO.DataAccess;
using CE.AO.Entities.TableStorageEntities;
using CE.AO.Logging;
using CE.AO.Models;

namespace UnitTests
{
    /// <summary>
    /// contains test methods for Calculation class
    /// </summary>
    [TestClass]
    public class CalculationTest
    {
        private UnityContainer _unityContainer;
        private Calculation _calculationManager;
        private Mock<ITableRepository> _tableRepositoryMock;

        /// <summary>
        /// Test Case Init
        /// </summary>
        [TestInitialize]
        public void Init()
        {
            AutoMapperConfig.AutoMapperMappings();
            _tableRepositoryMock = new Mock<ITableRepository>();
            _unityContainer = new UnityContainer();
            RegisterTypesForUnity.RegisterTypes(_unityContainer);
            var overrides = new ParameterOverrides
            {
                {"logModel", new LogModel {DisableLog = true} }
            };
            _calculationManager = _unityContainer.Resolve<Calculation>(overrides);
            _calculationManager.TableRepository = _tableRepositoryMock.Object;
        }
        
        /// <summary>
        /// Test Case for accessing default constructor
        /// </summary>
        [TestMethod]
        public void Calculation()
        {
            // ReSharper disable once ObjectCreationAsStatement
            new Calculation();
            Assert.AreEqual(true, true);
        }

        /// <summary>
        /// Test Case for accessing Log Model
        /// </summary>
        [TestMethod]
        public void AccessLogModel()
        {
            _calculationManager.LogModel.DisableLog = true;
            Assert.AreEqual(true, _calculationManager.LogModel.DisableLog);
        }

        /// <summary>
        /// Test case for inserting ctd Calculations
        /// </summary>
        [TestMethod]
        public void InsertCtdCalculationAsync()
        {
            var calculationEntity = GetCalculationEntity();
            _tableRepositoryMock.Setup(t => t.InsertOrMergeSingleAsync(It.IsAny<string>(), It.IsAny<CalculationEntity>())).Returns(Task.FromResult(true)).Verifiable();
            // ReSharper disable once UnusedVariable
            var result = _calculationManager.InsertCtdCalculationAsync(calculationEntity);
            _tableRepositoryMock.Verify(t => t.InsertOrMergeSingleAsync(It.IsAny<string>(), It.IsAny<CalculationEntity>()), Times.AtLeastOnce);
            _tableRepositoryMock.VerifyAll();
        }

        /// <summary>
        /// Test Method For Btd Calculations
        /// </summary>
        [TestMethod]
        public void InsertBtdCalculationAsync()
        {
            var calculationEntity = GetCalculationEntity();
            _tableRepositoryMock.Setup(t => t.InsertOrMergeSingleAsync(It.IsAny<string>(), It.IsAny<CalculationEntity>())).Returns(Task.FromResult(true)).Verifiable();
            // ReSharper disable once UnusedVariable
            var result = _calculationManager.InsertBtdCalculationAsync(calculationEntity);
            _tableRepositoryMock.Verify(t => t.InsertOrMergeSingleAsync(It.IsAny<string>(), It.IsAny<CalculationEntity>()), Times.AtLeastOnce);
            _tableRepositoryMock.VerifyAll();
        }

        /// <summary>
        /// Test Method For Get Btd Calculations
        /// </summary>
        [TestMethod]
        public void GetBtdCalculationAsync()
        {
            var calculationEntity = Mapper.Map<CalculationModel, CalculationEntity>(GetCalculationEntity());
            _tableRepositoryMock.Setup(t => t.GetSingleAsync<CalculationEntity>(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>())).Returns(Task.FromResult(calculationEntity)).Verifiable();
            var customer = _calculationManager.GetBtdCalculationAsync(1, "2").Result;
            Assert.AreEqual(customer.ClientId, 1);
        }

        /// <summary>
        /// Test Method For Get ctd Calculations
        /// </summary>
        [TestMethod]
        public void GetCtdCalculationAsync()
        {
            var calculationEntity = Mapper.Map<CalculationModel, CalculationEntity>(GetCalculationEntity());
            _tableRepositoryMock.Setup(t => t.GetSingleAsync<CalculationEntity>(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>())).Returns(Task.FromResult(calculationEntity)).Verifiable();
            var customer = _calculationManager.GetCtdCalculationAsync(1, "2", "6").Result;
            Assert.AreEqual(customer.ClientId, 1);
        }

        private static CalculationModel GetCalculationEntity()
        {
            return new CalculationModel
            {
                ClientId = 1,
                AccountId = "2",
                MeterId = "3",
                CustomerId = "4",
                CommodityId = 5,
                ServiceContractId = "6",
                Unit = 7,
                Cost = 100,
                ProjectedCost = 110,
                ProjectedUsage = 120,
                Usage = 130,
                BillDays = 140,
                BillCycleStartDate = DateTime.Now.AddDays(-1).ToShortDateString(),
                BillCycleEndDate = DateTime.Now.ToShortDateString(),
                AsOfAmiDate = DateTime.Now,
                AsOfCalculationDate = DateTime.Now,
                RateClass = "10",
                AverageDailyUsage = 140,
                AverageDailyCost = 150
            };
        }
    }
}
