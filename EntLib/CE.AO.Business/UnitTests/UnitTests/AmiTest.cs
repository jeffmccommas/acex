﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using AutoMapper;
using CE.AO.Business;
using CE.AO.DataAccess;
using CE.AO.Entities.TableStorageEntities;
using CE.AO.Models;
using CE.RateModel;
using Microsoft.Practices.Unity;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.WindowsAzure.Storage.Table;
using Moq;
using Enums = CE.AO.Utilities.Enums;

namespace UnitTests
{
    /// <summary>
    /// Contains test methods for AMI Class
    /// </summary>
    /// <seealso cref="System.IDisposable" />
    [TestClass]
    public class AmiTest : IDisposable
    {
        /// <summary>
        /// The _unity container
        /// </summary>
        private UnityContainer _unityContainer;

        /// <summary>
        /// The _ami manager
        /// </summary>
        private AMI _amiManager;

        /// <summary>
        /// The _table repository mock
        /// </summary>
        private Mock<ITableRepository> _tableRepositoryMock;

        /// <summary>
        /// The _disposed
        /// </summary>
        private bool _disposed;

        /// <summary>
        /// Initializes this instance.
        /// </summary>
        [TestInitialize]
        public void Init()
        {
            AutoMapperConfig.AutoMapperMappings();
            _tableRepositoryMock = new Mock<ITableRepository>();
            _unityContainer = new UnityContainer();
            RegisterTypesForUnity.RegisterTypes(_unityContainer);
            _amiManager = _unityContainer.Resolve<AMI>();
            _amiManager.TableRepository = _tableRepositoryMock.Object;
        }

        /// <summary>
        /// Test Case for accessing Log Model
        /// </summary>
        [TestMethod]
        public void AccessLogModel()
        {
            _amiManager.LogModel.DisableLog = true;
            Assert.AreEqual(true, _amiManager.LogModel.DisableLog);
        }

        /// <summary>
        /// Test Case for default constructor of AMI model
        /// </summary>
        [TestMethod]
        public void Ami()
        {
            // ReSharper disable once ObjectCreationAsStatement
            new AMI();
            Assert.AreEqual(true, true);
        }

        /// <summary>
        /// Test case for inserting ami for Ami15MinuteIntervalModel
        /// </summary>
        [TestMethod]
        public void InsertAmiAsync15MinuteInterval()
        {
            var ami15MinuteIntervalEntity = GetSampleAmi15MinuteIntervalEntity();
            ami15MinuteIntervalEntity.PartitionKey = Ami15MinuteIntervalEntity.SetPartitionKey(ami15MinuteIntervalEntity.ClientId, ami15MinuteIntervalEntity.MeterId);
            ami15MinuteIntervalEntity.RowKey = Ami15MinuteIntervalEntity.SetRowKey(ami15MinuteIntervalEntity.AmiTimeStamp);
            var ami15MinuteIntervalModel = Mapper.Map<Ami15MinuteIntervalEntity, Ami15MinuteIntervalModel>(ami15MinuteIntervalEntity);
            _tableRepositoryMock.Setup(t => t.InsertAsync(It.IsAny<Ami15MinuteIntervalEntity>(), It.IsAny<string>())).Returns(Task.FromResult(true));
            var result = _amiManager.InsertAmiAsync(ami15MinuteIntervalModel).Result;
            _tableRepositoryMock.Verify(t => t.InsertAsync(It.IsAny<Ami15MinuteIntervalEntity>(), It.IsAny<string>()), Times.Once);
            _tableRepositoryMock.VerifyAll();
            Assert.AreEqual(true, result);
        }

        /// <summary>
        /// Test case for inserting ami for Ami30MinuteIntervalModel
        /// </summary>
        [TestMethod]
        public void InsertAmiAsync30MinuteInterval()
        {
            var ami30MinuteIntervalEntity = GetSampleAmi30MinuteIntervalEntity();
            ami30MinuteIntervalEntity.PartitionKey = Ami30MinuteIntervalEntity.SetPartitionKey(ami30MinuteIntervalEntity.ClientId, ami30MinuteIntervalEntity.MeterId);
            ami30MinuteIntervalEntity.RowKey = Ami30MinuteIntervalEntity.SetRowKey(ami30MinuteIntervalEntity.AmiTimeStamp);
            var ami30MinuteIntervalModel = Mapper.Map<Ami30MinuteIntervalEntity, Ami30MinuteIntervalModel>(ami30MinuteIntervalEntity);
            _tableRepositoryMock.Setup(t => t.InsertAsync(It.IsAny<Ami30MinuteIntervalEntity>(), It.IsAny<string>())).Returns(Task.FromResult(true));
            var result = _amiManager.InsertAmiAsync(ami30MinuteIntervalModel).Result;
            _tableRepositoryMock.Verify(t => t.InsertAsync(It.IsAny<Ami30MinuteIntervalEntity>(), It.IsAny<string>()), Times.Once);
            _tableRepositoryMock.VerifyAll();
            Assert.AreEqual(true, result);
        }

        /// <summary>
        /// Test case for inserting ami for Ami60MinuteIntervalModel
        /// </summary>
        [TestMethod]
        public void InsertAmiAsync60MinuteInterval()
        {
            var ami60MinuteIntervalEntity = GetSampleAmi60MinuteIntervalEntity();
            ami60MinuteIntervalEntity.PartitionKey = Ami60MinuteIntervalEntity.SetPartitionKey(ami60MinuteIntervalEntity.ClientId, ami60MinuteIntervalEntity.MeterId);
            ami60MinuteIntervalEntity.RowKey = Ami60MinuteIntervalEntity.SetRowKey(ami60MinuteIntervalEntity.AmiTimeStamp);
            var ami60MinuteIntervalModel = Mapper.Map<Ami60MinuteIntervalEntity, Ami60MinuteIntervalModel>(ami60MinuteIntervalEntity);
            _tableRepositoryMock.Setup(t => t.InsertAsync(It.IsAny<Ami60MinuteIntervalEntity>(), It.IsAny<string>())).Returns(Task.FromResult(true));
            var result = _amiManager.InsertAmiAsync(ami60MinuteIntervalModel).Result;
            _tableRepositoryMock.Verify(t => t.InsertAsync(It.IsAny<Ami60MinuteIntervalEntity>(), It.IsAny<string>()), Times.Once);
            _tableRepositoryMock.VerifyAll();
            Assert.AreEqual(true, result);
        }

        /// <summary>
        /// Test case for inserting ami for InsertOrMergeAmiAsync method in AMI class for Ami15MinuteIntervalModel
        /// </summary>
        [TestMethod]
        public void InsertOrMergeAmiAsync15MinuteInterval()
        {
            var premiseModel = GetSampleAmi15MinuteIntervalEntity();
            premiseModel.PartitionKey = Ami15MinuteIntervalEntity.SetPartitionKey(premiseModel.ClientId, premiseModel.MeterId);
            premiseModel.RowKey = Ami15MinuteIntervalEntity.SetRowKey(premiseModel.AmiTimeStamp);
            _tableRepositoryMock.Setup(t => t.InsertOrMergeSingleAsync(It.IsAny<string>(), It.IsAny<Ami15MinuteIntervalEntity>())).Returns(Task.FromResult(true));
            var result = _amiManager.InsertOrMergeAmiAsync(premiseModel, Enums.IntervalType.Fifteen).Result;
            _tableRepositoryMock.Verify(t => t.InsertOrMergeSingleAsync(It.IsAny<string>(), It.IsAny<Ami15MinuteIntervalEntity>()), Times.Once);
            _tableRepositoryMock.VerifyAll();
            Assert.AreEqual(true, result);
        }

        /// <summary>
        /// Test case for inserting ami for InsertOrMergeAmiAsync method in AMI class for Ami30MinuteIntervalModel
        /// </summary>
        [TestMethod]
        public void InsertOrMergeAmiAsync30MinuteInterval()
        {
            var premiseModel = GetSampleAmi30MinuteIntervalEntity();
            premiseModel.PartitionKey = Ami30MinuteIntervalEntity.SetPartitionKey(premiseModel.ClientId, premiseModel.MeterId);
            premiseModel.RowKey = Ami30MinuteIntervalEntity.SetRowKey(premiseModel.AmiTimeStamp);
            _tableRepositoryMock.Setup(t => t.InsertOrMergeSingleAsync(It.IsAny<string>(), It.IsAny<Ami30MinuteIntervalEntity>())).Returns(Task.FromResult(true));
            var result = _amiManager.InsertOrMergeAmiAsync(premiseModel, Enums.IntervalType.Thirty).Result;
            _tableRepositoryMock.Verify(t => t.InsertOrMergeSingleAsync(It.IsAny<string>(), It.IsAny<Ami30MinuteIntervalEntity>()), Times.Once);
            _tableRepositoryMock.VerifyAll();
            Assert.AreEqual(true, result);
        }

        /// <summary>
        /// Test case for inserting ami for InsertOrMergeAmiAsync method in AMI class for Ami60MinuteIntervalModel
        /// </summary>
        [TestMethod]
        public void InsertOrMergeAmiAsync60MinuteInterval()
        {
            var premiseModel = GetSampleAmi60MinuteIntervalEntity();
            premiseModel.PartitionKey = Ami60MinuteIntervalEntity.SetPartitionKey(premiseModel.ClientId, premiseModel.MeterId);
            premiseModel.RowKey = Ami60MinuteIntervalEntity.SetRowKey(premiseModel.AmiTimeStamp);
            _tableRepositoryMock.Setup(t => t.InsertOrMergeSingleAsync(It.IsAny<string>(), It.IsAny<Ami60MinuteIntervalEntity>())).Returns(Task.FromResult(true));
            var result = _amiManager.InsertOrMergeAmiAsync(premiseModel, Enums.IntervalType.Sixty).Result;
            _tableRepositoryMock.Verify(t => t.InsertOrMergeSingleAsync(It.IsAny<string>(), It.IsAny<Ami60MinuteIntervalEntity>()), Times.Once);
            _tableRepositoryMock.VerifyAll();
            Assert.AreEqual(true, result);
        }

        /// <summary>
        /// Test case for inserting ami for InsertOrMergeAmiAsync method in AMI class for AmiModel where interval type is none
        /// </summary>
        [TestMethod]
        public void InsertOrMergeAmiAsyncNoneInterval()
        {
            var premiseModel = GetSampleAmi60MinuteIntervalEntity();
            premiseModel.PartitionKey = Ami60MinuteIntervalEntity.SetPartitionKey(premiseModel.ClientId, premiseModel.MeterId);
            premiseModel.RowKey = Ami60MinuteIntervalEntity.SetRowKey(premiseModel.AmiTimeStamp);
            _tableRepositoryMock.Setup(t => t.InsertOrMergeSingleAsync(It.IsAny<string>(), It.IsAny<Ami60MinuteIntervalEntity>())).Returns(Task.FromResult(false));
            var result = _amiManager.InsertOrMergeAmiAsync(premiseModel, Enums.IntervalType.None).Result;
            _tableRepositoryMock.Verify(t => t.InsertOrMergeSingleAsync(It.IsAny<string>(), It.IsAny<Ami60MinuteIntervalEntity>()), Times.Never);
            Assert.AreEqual(false, result);
        }

        /// <summary>
        /// Test case to get single 15 min interval type ami model.
        /// </summary>
        [TestMethod]
        public void GetAmiList15MinuteInterval()
        {
            var list = new List<DynamicTableEntity>();
            var ami15MinuteIntervalEntity = GetSampleAmi15MinuteIntervalEntity();
            ami15MinuteIntervalEntity.PartitionKey = Ami15MinuteIntervalEntity.SetPartitionKey(ami15MinuteIntervalEntity.ClientId, ami15MinuteIntervalEntity.MeterId);
            ami15MinuteIntervalEntity.RowKey = Ami15MinuteIntervalEntity.SetRowKey(ami15MinuteIntervalEntity.AmiTimeStamp);
            var dynamicAmi15MinuteIntervalEntity = GetAmi15MinuteIntervalEntityDynamicTableEntity(ami15MinuteIntervalEntity);
            list.Add(dynamicAmi15MinuteIntervalEntity);
            var filter = TableQuery.CombineFilters(TableQuery.GenerateFilterCondition("PartitionKey", QueryComparisons.Equal,$"{87}_{"m5".ToLower()}"),TableOperators.And,
                TableQuery.CombineFilters(TableQuery.GenerateFilterCondition("RowKey", QueryComparisons.GreaterThanOrEqual, DateTime.UtcNow.ToString("yyyy-MM-dd")),TableOperators.And,
                    TableQuery.GenerateFilterCondition("RowKey", QueryComparisons.LessThanOrEqual, DateTime.UtcNow.AddDays(4).ToString("yyyy-MM-dd"))));
            _tableRepositoryMock.Setup(t =>t.GetAllAsync<DynamicTableEntity>(It.IsAny<string>(), filter)).Returns(list).Verifiable();
            var result = _amiManager.GetAmiList(DateTime.UtcNow, DateTime.UtcNow.AddDays(4), "m5", 87);
            _tableRepositoryMock.Verify(t => t.GetAllAsync<DynamicTableEntity>(It.IsAny<string>(), filter), Times.Once);
            _tableRepositoryMock.VerifyAll();
            Assert.AreEqual(result.ToList().Count, 1);
        }

        /// <summary>
        /// Test case to get single 30 min interval type ami model.
        /// </summary>
        [TestMethod]
        public void GetAmiList30MinuteInterval()
        {
            var list = new List<DynamicTableEntity>();
            var ami30MinuteIntervalEntity = GetSampleAmi30MinuteIntervalEntity();
            ami30MinuteIntervalEntity.PartitionKey = Ami30MinuteIntervalEntity.SetPartitionKey(ami30MinuteIntervalEntity.ClientId, ami30MinuteIntervalEntity.MeterId);
            ami30MinuteIntervalEntity.RowKey = Ami30MinuteIntervalEntity.SetRowKey(ami30MinuteIntervalEntity.AmiTimeStamp);
            var dynamicAmi30MinuteIntervalEntity = GetAmi30MinuteIntervalEntityDynamicTableEntity(ami30MinuteIntervalEntity);
            list.Add(dynamicAmi30MinuteIntervalEntity);
            var filter = TableQuery.CombineFilters(TableQuery.GenerateFilterCondition("PartitionKey", QueryComparisons.Equal,$"{87}_{"m5".ToLower()}"),TableOperators.And,TableQuery.CombineFilters(TableQuery.GenerateFilterCondition("RowKey", QueryComparisons.GreaterThanOrEqual, DateTime.UtcNow.ToString("yyyy-MM-dd")),
                    TableOperators.And,TableQuery.GenerateFilterCondition("RowKey", QueryComparisons.LessThanOrEqual, DateTime.UtcNow.AddDays(4).ToString("yyyy-MM-dd"))));
            _tableRepositoryMock.Setup(t =>t.GetAllAsync<DynamicTableEntity>(It.IsAny<string>(), filter)).Returns(list).Verifiable();
            IEnumerable<dynamic> result = _amiManager.GetAmiList(DateTime.UtcNow, DateTime.UtcNow.AddDays(4), "m5", 87);
            _tableRepositoryMock.Verify(t => t.GetAllAsync<DynamicTableEntity>(It.IsAny<string>(), filter), Times.Once);
            _tableRepositoryMock.VerifyAll();
            Assert.AreEqual(result.ToList().Count, 1);
        }

        /// <summary>
        /// Test case to get single 60 min interval type ami model.
        /// </summary>
        [TestMethod]
        public void GetAmiList60MinuteInterval()
        {
            var list = new List<DynamicTableEntity>();
            var ami60MinuteIntervalEntity = GetSampleAmi60MinuteIntervalEntity();
            ami60MinuteIntervalEntity.PartitionKey = Ami60MinuteIntervalEntity.SetPartitionKey(ami60MinuteIntervalEntity.ClientId, ami60MinuteIntervalEntity.MeterId);
            ami60MinuteIntervalEntity.RowKey = Ami60MinuteIntervalEntity.SetRowKey(ami60MinuteIntervalEntity.AmiTimeStamp);
            var dynamicAmi60MinuteIntervalEntity = GetAmi60MinuteIntervalEntityDynamicTableEntity(ami60MinuteIntervalEntity);
            list.Add(dynamicAmi60MinuteIntervalEntity);
            var filter = TableQuery.CombineFilters(TableQuery.GenerateFilterCondition("PartitionKey", QueryComparisons.Equal,$"{87}_{"m5".ToLower()}"),TableOperators.And,
                TableQuery.CombineFilters(TableQuery.GenerateFilterCondition("RowKey", QueryComparisons.GreaterThanOrEqual, DateTime.UtcNow.ToString("yyyy-MM-dd")),TableOperators.And,
                    TableQuery.GenerateFilterCondition("RowKey", QueryComparisons.LessThanOrEqual, DateTime.UtcNow.AddDays(4).ToString("yyyy-MM-dd"))));
            _tableRepositoryMock.Setup(t =>t.GetAllAsync<DynamicTableEntity>(It.IsAny<string>(), filter)).Returns(list).Verifiable();
            IEnumerable<dynamic> result = _amiManager.GetAmiList(DateTime.UtcNow, DateTime.UtcNow.AddDays(4), "m5", 87);
            _tableRepositoryMock.Verify(t => t.GetAllAsync<DynamicTableEntity>(It.IsAny<string>(), filter), Times.Once);
            _tableRepositoryMock.VerifyAll();
            Assert.AreEqual(result.ToList().Count, 1);
        }

        /// <summary>
        /// Test case to get single none as an interval type ami model.
        /// </summary>
        [TestMethod]
        public void GetAmiListNoneTypeOfInterval()
        {
            var list = new List<DynamicTableEntity>();
            var ami60MinuteIntervalEntity = GetSampleAmi60MinuteIntervalEntity();
            ami60MinuteIntervalEntity.IntervalType = 0;
            ami60MinuteIntervalEntity.PartitionKey = Ami60MinuteIntervalEntity.SetPartitionKey(ami60MinuteIntervalEntity.ClientId, ami60MinuteIntervalEntity.MeterId);
            ami60MinuteIntervalEntity.RowKey = Ami60MinuteIntervalEntity.SetRowKey(ami60MinuteIntervalEntity.AmiTimeStamp);
            var dynamicAmi60MinuteIntervalEntity = GetAmi60MinuteIntervalEntityDynamicTableEntity(ami60MinuteIntervalEntity);
            list.Add(dynamicAmi60MinuteIntervalEntity);
            var filter = TableQuery.CombineFilters(TableQuery.GenerateFilterCondition("PartitionKey", QueryComparisons.Equal,$"{87}_{"m5".ToLower()}"),TableOperators.And,
                TableQuery.CombineFilters(TableQuery.GenerateFilterCondition("RowKey", QueryComparisons.GreaterThanOrEqual, DateTime.UtcNow.ToString("yyyy-MM-dd")),TableOperators.And,TableQuery.GenerateFilterCondition("RowKey", QueryComparisons.LessThanOrEqual, DateTime.UtcNow.AddDays(4).ToString("yyyy-MM-dd"))));
            _tableRepositoryMock.Setup(t =>t.GetAllAsync<DynamicTableEntity>(It.IsAny<string>(), filter)).Returns(list).Verifiable();
            IEnumerable<dynamic> result = _amiManager.GetAmiList(DateTime.UtcNow, DateTime.UtcNow.AddDays(4), "m5", 87);
            _tableRepositoryMock.Verify(t => t.GetAllAsync<DynamicTableEntity>(It.IsAny<string>(), filter), Times.Once);
            _tableRepositoryMock.VerifyAll();
            Assert.AreEqual(result, null);
        }

        /// <summary>
        /// Test case to get empty list of ami models.
        /// </summary>
        [TestMethod]
        public void GetAmiListIfNull()
        {
            var filter = TableQuery.CombineFilters(
                TableQuery.GenerateFilterCondition("PartitionKey", QueryComparisons.Equal,$"{87}_{"m5".ToLower()}"),TableOperators.And,
                TableQuery.CombineFilters(TableQuery.GenerateFilterCondition("RowKey", QueryComparisons.GreaterThanOrEqual, DateTime.UtcNow.ToString("yyyy-MM-dd")),TableOperators.And,TableQuery.GenerateFilterCondition("RowKey", QueryComparisons.LessThanOrEqual, DateTime.UtcNow.AddDays(4).ToString("yyyy-MM-dd"))));
            _tableRepositoryMock.Setup(t =>t.GetAllAsync<DynamicTableEntity>(It.IsAny<string>(), filter)).Returns(new List<DynamicTableEntity>()).Verifiable();
            IEnumerable<dynamic> result = _amiManager.GetAmiList(DateTime.UtcNow, DateTime.UtcNow.AddDays(4), "m5", 87);
            _tableRepositoryMock.Verify(t => t.GetAllAsync<DynamicTableEntity>(It.IsAny<string>(), filter), Times.Once);
            _tableRepositoryMock.VerifyAll();
            Assert.AreEqual(result, null);
        }

        /// <summary>
        /// Test case to get the list of 15 interval type ami models.
        /// </summary>
        [TestMethod]
        public void GetAmi15MinuteInterval()
        {
            var ami15MinuteIntervalEntity = GetSampleAmi15MinuteIntervalEntity();
            var partitionKey = Ami15MinuteIntervalEntity.SetPartitionKey(ami15MinuteIntervalEntity.ClientId, ami15MinuteIntervalEntity.MeterId);
            var rowKey = Ami15MinuteIntervalEntity.SetRowKey(ami15MinuteIntervalEntity.AmiTimeStamp);
            var entity = GetAmi15MinuteIntervalEntityDynamicTableEntity(ami15MinuteIntervalEntity);
            _tableRepositoryMock.Setup(t => t.GetSingleAsync<DynamicTableEntity>(It.IsAny<string>(), partitionKey, rowKey)).Returns(Task.FromResult(entity)).Verifiable();
            var result = _amiManager.GetAmi(DateTime.UtcNow, "m5", 87).Result;
            _tableRepositoryMock.Verify(t => t.GetSingleAsync<DynamicTableEntity>(It.IsAny<string>(), partitionKey, rowKey), Times.Exactly(1));
            _tableRepositoryMock.VerifyAll();
            Assert.AreEqual(result.ClientId, 87);
        }

        /// <summary>
        /// Test case to get the list of 30 interval type ami models.
        /// </summary>
        [TestMethod]
        public void GetAmi30MinuteInterval()
        {
            var ami30MinuteIntervalEntity = GetSampleAmi30MinuteIntervalEntity();
            var partitionKey = Ami30MinuteIntervalEntity.SetPartitionKey(ami30MinuteIntervalEntity.ClientId, ami30MinuteIntervalEntity.MeterId);
            var rowKey = Ami30MinuteIntervalEntity.SetRowKey(ami30MinuteIntervalEntity.AmiTimeStamp);
            var entity = GetAmi30MinuteIntervalEntityDynamicTableEntity(ami30MinuteIntervalEntity);
            _tableRepositoryMock.Setup(t => t.GetSingleAsync<DynamicTableEntity>(It.IsAny<string>(), partitionKey, rowKey)).Returns(Task.FromResult(entity)).Verifiable();
            var result = _amiManager.GetAmi(DateTime.UtcNow, "m5", 87).Result;
            _tableRepositoryMock.Verify(t => t.GetSingleAsync<DynamicTableEntity>(It.IsAny<string>(), partitionKey, rowKey), Times.Exactly(1));
            _tableRepositoryMock.VerifyAll();
            Assert.AreEqual(result.ClientId, 87);
        }

        /// <summary>
        /// Test case to get the list of 60 interval type ami models.
        /// </summary>
        [TestMethod]
        public void GetAmi60MinuteInterval()
        {
            var ami60MinuteIntervalEntity = GetSampleAmi60MinuteIntervalEntity();
            var partitionKey = Ami60MinuteIntervalEntity.SetPartitionKey(ami60MinuteIntervalEntity.ClientId, ami60MinuteIntervalEntity.MeterId);
            var rowKey = Ami60MinuteIntervalEntity.SetRowKey(ami60MinuteIntervalEntity.AmiTimeStamp);
            var entity = GetAmi60MinuteIntervalEntityDynamicTableEntity(ami60MinuteIntervalEntity);
            _tableRepositoryMock.Setup(t => t.GetSingleAsync<DynamicTableEntity>(It.IsAny<string>(), partitionKey, rowKey)).Returns(Task.FromResult(entity)).Verifiable();
            var result = _amiManager.GetAmi(DateTime.UtcNow, "m5", 87).Result;
            _tableRepositoryMock.Verify(t => t.GetSingleAsync<DynamicTableEntity>(It.IsAny<string>(), partitionKey, rowKey), Times.Exactly(1));
            _tableRepositoryMock.VerifyAll();
            Assert.AreEqual(result.ClientId, 87);
        }

        /// <summary>
        /// Test case to get single ami model.
        /// </summary>
        [TestMethod]
        public void GetAmiIfNull()
        {
            var ami60MinuteIntervalEntity = new Ami60MinuteIntervalEntity();
            var partitionKey = Ami60MinuteIntervalEntity.SetPartitionKey(87, "m5");
            var rowKey = Ami60MinuteIntervalEntity.SetRowKey(ami60MinuteIntervalEntity.AmiTimeStamp);
            var entity = GetAmi60MinuteIntervalEntityDynamicTableEntity(ami60MinuteIntervalEntity);
            _tableRepositoryMock.Setup(t => t.GetSingleAsync<DynamicTableEntity>(It.IsAny<string>(), partitionKey, rowKey)).Returns(Task.FromResult(entity)).Verifiable();
            var result = _amiManager.GetAmi(DateTime.UtcNow, "m5", 87).Result;
            _tableRepositoryMock.Verify(t => t.GetSingleAsync<DynamicTableEntity>(It.IsAny<string>(), partitionKey, rowKey), Times.Never);
            Assert.AreEqual(result, null);
        }

        /// <summary>
        /// Gets the readings.
        /// </summary>
        [TestMethod]
        public void GetReadings()
        {
            var ami15MinuteIntervalModel = new Ami15MinuteIntervalModel
            {
                AccountNumber = "",
                AmiTimeStamp = DateTime.Now,
                ClientId = 87,
                UOMId = 5,
                IntervalType = 15,
                IntValue0000 = 10,
                IntValue0015 = 15,
                IntValue0030 = 30,
                IntValue0045 = 45,
                IntValue0100 = 100
            };
            var ami30MinuteIntervalModel = new Ami30MinuteIntervalModel
            {
                AccountNumber = "",
                AmiTimeStamp = DateTime.Now,
                ClientId = 87,
                UOMId = 6,
                IntervalType = 30,
                IntValue0000 = 10,
                IntValue0030 = 30,
                IntValue0100 = 100
            };
            var ami60MinuteIntervalModel = new Ami60MinuteIntervalModel
            {
                AccountNumber = "",
                AmiTimeStamp = DateTime.Now,
                ClientId = 87,
                UOMId = 7,
                IntervalType = 60,
                IntValue0000 = 10,
                IntValue0100 = 100
            };
            var ami15List = new List<Ami15MinuteIntervalModel> { ami15MinuteIntervalModel };
            var ami30List = new List<Ami30MinuteIntervalModel> { ami30MinuteIntervalModel };
            var ami60List = new List<Ami60MinuteIntervalModel> { ami60MinuteIntervalModel };
            int noOfDays;
            List<Reading> result = _amiManager.GetReadings(ami15List, out noOfDays);
            Assert.AreEqual(noOfDays, 1);
            Assert.AreEqual(result.Count, 96);
            List<ReadingModel> result2 = _amiManager.GetReadings(ami15List);
            List<ReadingModel> result3 = _amiManager.GetReadings(ami30List);
            List<ReadingModel> result4 = _amiManager.GetReadings(ami60List);
            Assert.AreEqual(result2.First().UOMId, 5);
            Assert.AreEqual(result2.Count, 96);
            Assert.AreEqual(result3.First().UOMId, 6);
            Assert.AreEqual(result3.Count, 48);
            Assert.AreEqual(result4.First().UOMId, 7);
            Assert.AreEqual(result4.Count, 24);
        }

        /// <summary>
        /// Gets the readings ami model is null.
        /// </summary>
        [TestMethod]
        public void GetReadingsAmiModelIsNull()
        {
            int noOfDays;
            List<Reading> result = _amiManager.GetReadings(null, out noOfDays);
            Assert.AreEqual(noOfDays, 0);
            Assert.AreEqual(result.Count, 0);
        }

        /// <summary>
        /// Gets the daily readings.
        /// </summary>
        [TestMethod]
        public void GetDailyReadings()
        {
            var ami15List = new List<Ami15MinuteIntervalModel>
            {
                new Ami15MinuteIntervalModel
            {
                AccountNumber = "",
                AmiTimeStamp = DateTime.Now,
                ClientId = 87,
                UOMId = 5,
                IntervalType = 15,
                IntValue0000 = 10,
                IntValue0015 = 15,
                IntValue0030 = 30,
                IntValue0045 = 45,
                IntValue0100 = 100
            },
                new Ami15MinuteIntervalModel
        {
                AccountNumber = "",
                AmiTimeStamp = DateTime.Now.AddDays(-1),
                ClientId = 87,
                UOMId = 5,
                IntervalType = 15,
                IntValue0000 = 10,
                IntValue0015 = 15,
                IntValue0030 = 30,
                IntValue0045 = 45,
                IntValue0100 = 100
            }
            };
            List<Reading> result = _amiManager.GetDailyReadings(ami15List, 15);
            Assert.AreEqual(result.Count, 2);
            Assert.AreEqual(result.First().Quantity, 200);
        }

        /// <summary>
        /// Gets the monthly readings.
        /// </summary>
        [TestMethod]
        public void GetMonthlyReadings()
        {
            var ami15List = new List<Ami15MinuteIntervalModel>
            {
                new Ami15MinuteIntervalModel
            {
                AccountNumber = "",
                AmiTimeStamp = DateTime.Now,
                ClientId = 87,
                UOMId = 5,
                IntervalType = 15,
                IntValue0000 = 10,
                IntValue0015 = 15,
                IntValue0030 = 30,
                IntValue0045 = 45,
                IntValue0100 = 100
            },
                new Ami15MinuteIntervalModel
            {
                AccountNumber = "",
                AmiTimeStamp = DateTime.Now.AddDays(-1),
                ClientId = 87,
                UOMId = 5,
                IntervalType = 15,
                IntValue0000 = 10,
                IntValue0015 = 15,
                IntValue0030 = 30,
                IntValue0045 = 45,
                IntValue0100 = 100
            }
            };

            List<Reading> result = _amiManager.GetMonthlyReadings(ami15List, 15);
            Assert.AreEqual(result.First().Quantity, 400);
            Assert.AreEqual(result.Count, 1);
        }

        /// <summary>
        /// Gets the sample ami15 minute interval entity.
        /// </summary>
        /// <returns></returns>
        private static Ami15MinuteIntervalEntity GetSampleAmi15MinuteIntervalEntity()
        {
            var ami15MinuteIntervalEntity = new Ami15MinuteIntervalEntity()
        {
                ClientId = 87,
                AccountNumber = "a5",
                MeterId = "m5",
                ServicePointId = "sp5",
                CommodityId = 1,
                UOMId = 3,
                VolumeFactor = 3,
                Direction = 1,
                ProjectedReadDate = DateTime.UtcNow.AddMonths(1),
                Timezone = "EST",
                AmiTimeStamp = DateTime.UtcNow,
                IntervalType = 15
            };

            var rnd = new Random();
            PropertyInfo[] properties = ami15MinuteIntervalEntity.GetType().GetProperties();
            properties.Where(p => p.Name.Contains("IntValue")).ToList()
                .ForEach(reflect =>
                {
                    reflect.SetValue(ami15MinuteIntervalEntity, rnd.NextDouble());
                });
            return ami15MinuteIntervalEntity;
        }

        /// <summary>
        /// Gets the sample ami30 minute interval entity.
        /// </summary>
        /// <returns></returns>
        private static Ami30MinuteIntervalEntity GetSampleAmi30MinuteIntervalEntity()
        {
            var ami30MinuteIntervalEntity = new Ami30MinuteIntervalEntity()
            {
                ClientId = 87,
                AccountNumber = "a5",
                MeterId = "m5",
                ServicePointId = "sp5",
                CommodityId = 1,
                UOMId = 3,
                VolumeFactor = 3,
                Direction = 1,
                ProjectedReadDate = DateTime.UtcNow.AddMonths(1),
                Timezone = "EST",
                AmiTimeStamp = DateTime.UtcNow,
                IntervalType = 30
            };

            var rnd = new Random();
            PropertyInfo[] properties = ami30MinuteIntervalEntity.GetType().GetProperties();
            properties.Where(p => p.Name.Contains("IntValue")).ToList()
                .ForEach(reflect =>
                {
                    reflect.SetValue(ami30MinuteIntervalEntity, rnd.NextDouble());
                });
            return ami30MinuteIntervalEntity;
        }

        /// <summary>
        /// Gets the sample ami60 minute interval entity.
        /// </summary>
        /// <returns></returns>
        private static Ami60MinuteIntervalEntity GetSampleAmi60MinuteIntervalEntity()
        {
            var ami60MinuteIntervalEntity = new Ami60MinuteIntervalEntity()
        {
                ClientId = 87,
                AccountNumber = "a5",
                MeterId = "m5",
                ServicePointId = "sp5",
                CommodityId = 1,
                UOMId = 3,
                VolumeFactor = 3,
                Direction = 1,
                ProjectedReadDate = DateTime.UtcNow.AddMonths(1),
                Timezone = "EST",
                AmiTimeStamp = DateTime.UtcNow,
                IntervalType = 60
            };

            var rnd = new Random();
            PropertyInfo[] properties = ami60MinuteIntervalEntity.GetType().GetProperties();
            properties.Where(p => p.Name.Contains("IntValue")).ToList()
                .ForEach(reflect =>
                {
                    reflect.SetValue(ami60MinuteIntervalEntity, rnd.NextDouble());
                });
            return ami60MinuteIntervalEntity;
        }

        /// <summary>
        /// Gets the ami15 minute interval entity dynamic table entity.
        /// </summary>
        /// <param name="ami15MinuteIntervalEntity">The ami15 minute interval entity.</param>
        /// <returns></returns>
        private static DynamicTableEntity GetAmi15MinuteIntervalEntityDynamicTableEntity(Ami15MinuteIntervalEntity ami15MinuteIntervalEntity)
        {
            return new DynamicTableEntity
            {
                PartitionKey = ami15MinuteIntervalEntity.PartitionKey,
                RowKey = ami15MinuteIntervalEntity.RowKey,
                Properties = new Dictionary<string, EntityProperty>()
                    {
                     {"ClientId",  new EntityProperty(ami15MinuteIntervalEntity.ClientId)},
                     {"AccountNumber", new EntityProperty(ami15MinuteIntervalEntity.AccountNumber)},
                     {"MeterId",  new EntityProperty(ami15MinuteIntervalEntity.MeterId)},
                     {"ServicePointId",  new EntityProperty(ami15MinuteIntervalEntity.ServicePointId)},
                     {"CommodityId",new EntityProperty(ami15MinuteIntervalEntity.CommodityId)},
                     {"UOMId", new EntityProperty(ami15MinuteIntervalEntity.UOMId)},
                     {"VolumeFactor", new EntityProperty(ami15MinuteIntervalEntity.VolumeFactor)},
                     {"Direction",new EntityProperty(ami15MinuteIntervalEntity.Direction)},
                     {"ProjectedReadDate", new EntityProperty(ami15MinuteIntervalEntity.ProjectedReadDate)},
                     {"Timezone", new EntityProperty(ami15MinuteIntervalEntity.Timezone)},
                     {"AmiTimeStamp",new EntityProperty(ami15MinuteIntervalEntity.AmiTimeStamp)},
                     {"IntervalType", new EntityProperty(ami15MinuteIntervalEntity.IntervalType)},
                     {"IntValue0000", new EntityProperty(ami15MinuteIntervalEntity.IntValue0000)}
                }
            };
        }

        /// <summary>
        /// Gets the ami30 minute interval entity dynamic table entity.
        /// </summary>
        /// <param name="ami30MinuteIntervalEntity">The ami30 minute interval entity.</param>
        /// <returns></returns>
        private static DynamicTableEntity GetAmi30MinuteIntervalEntityDynamicTableEntity(Ami30MinuteIntervalEntity ami30MinuteIntervalEntity)
        {
            return new DynamicTableEntity
            {
                PartitionKey = ami30MinuteIntervalEntity.PartitionKey,
                RowKey = ami30MinuteIntervalEntity.RowKey,
                Properties = new Dictionary<string, EntityProperty>()
                {
                     {"ClientId",  new EntityProperty(ami30MinuteIntervalEntity.ClientId)},
                     {"AccountNumber", new EntityProperty(ami30MinuteIntervalEntity.AccountNumber)},
                     {"MeterId",  new EntityProperty(ami30MinuteIntervalEntity.MeterId)},
                     {"ServicePointId",  new EntityProperty(ami30MinuteIntervalEntity.ServicePointId)},
                     {"CommodityId",new EntityProperty(ami30MinuteIntervalEntity.CommodityId)},
                     {"UOMId", new EntityProperty(ami30MinuteIntervalEntity.UOMId)},
                     {"VolumeFactor", new EntityProperty(ami30MinuteIntervalEntity.VolumeFactor)},
                     {"Direction",new EntityProperty(ami30MinuteIntervalEntity.Direction)},
                     {"ProjectedReadDate", new EntityProperty(ami30MinuteIntervalEntity.ProjectedReadDate)},
                     {"Timezone", new EntityProperty(ami30MinuteIntervalEntity.Timezone)},
                     {"AmiTimeStamp",new EntityProperty(ami30MinuteIntervalEntity.AmiTimeStamp)},
                     {"IntervalType", new EntityProperty(ami30MinuteIntervalEntity.IntervalType)},
                     {"IntValue0000", new EntityProperty(ami30MinuteIntervalEntity.IntValue0000)}
                }
            };
        }

        /// <summary>
        /// Gets the ami60 minute interval entity dynamic table entity.
        /// </summary>
        /// <param name="ami60MinuteIntervalEntity">The ami60 minute interval entity.</param>
        /// <returns></returns>
        private static DynamicTableEntity GetAmi60MinuteIntervalEntityDynamicTableEntity(Ami60MinuteIntervalEntity ami60MinuteIntervalEntity)
        {
            return new DynamicTableEntity
            {
                PartitionKey = ami60MinuteIntervalEntity.PartitionKey,
                RowKey = ami60MinuteIntervalEntity.RowKey,
                Properties = new Dictionary<string, EntityProperty>()
                {
                     {"ClientId",  new EntityProperty(ami60MinuteIntervalEntity.ClientId)},
                     {"AccountNumber", new EntityProperty(ami60MinuteIntervalEntity.AccountNumber)},
                     {"MeterId",  new EntityProperty(ami60MinuteIntervalEntity.MeterId)},
                     {"ServicePointId",  new EntityProperty(ami60MinuteIntervalEntity.ServicePointId)},
                     {"CommodityId",new EntityProperty(ami60MinuteIntervalEntity.CommodityId)},
                     {"UOMId", new EntityProperty(ami60MinuteIntervalEntity.UOMId)},
                     {"VolumeFactor", new EntityProperty(ami60MinuteIntervalEntity.VolumeFactor)},
                     {"Direction",new EntityProperty(ami60MinuteIntervalEntity.Direction)},
                     {"ProjectedReadDate", new EntityProperty(ami60MinuteIntervalEntity.ProjectedReadDate)},
                     {"Timezone", new EntityProperty(ami60MinuteIntervalEntity.Timezone)},
                     {"AmiTimeStamp",new EntityProperty(ami60MinuteIntervalEntity.AmiTimeStamp)},
                     {"IntervalType", new EntityProperty(ami60MinuteIntervalEntity.IntervalType)},
                     {"IntValue0000", new EntityProperty(ami60MinuteIntervalEntity.IntValue0000)}
                }
            };
        }

        /// <summary>
        /// Releases unmanaged and - optionally - managed resources.
        /// </summary>
        /// <param name="disposing"><c>true</c> to release both managed and unmanaged resources; <c>false</c> to release only unmanaged resources.</param>
        private void Dispose(bool disposing)
        {
            if (!_disposed)
            {
                if (disposing)
                {
                    _unityContainer.Dispose();
                }
            }
            _disposed = true;
        }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            Dispose(true);
            // ReSharper disable once GCSuppressFinalizeForTypeWithoutDestructor
            GC.SuppressFinalize(this);
        }
    }
}
