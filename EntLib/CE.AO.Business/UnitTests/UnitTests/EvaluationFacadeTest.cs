﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Runtime.Remoting.Messaging;
using System.Threading.Tasks;
using CE.AO.Business;
using CE.AO.Business.Contract;
using CE.AO.Logging;
using CE.AO.Models;
using CE.RateModel;
using Microsoft.Practices.Unity;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace UnitTests
{
    /// <summary>
    /// Contains test methods for EvaluationFacade Class
    /// </summary>
    [TestClass]
    public class EvaluationFacadeTest : IDisposable
    {
        /// <summary>
        /// The _unity container
        /// </summary>
        private UnityContainer _unityContainer;

        /// <summary>
        /// The _evaluation facade manager
        /// </summary>
        private EvaluationFacade _evaluationFacadeManager;

        /// <summary>
        /// The _evaluation mock
        /// </summary>
        private Mock<IEvaluation> _evaluationMock;
        
        /// <summary>
        /// The _subscription mock
        /// </summary>
        private Mock<ISubscription> _subscriptionMock;

        /// <summary>
        /// The _calculation mock
        /// </summary>
        private Mock<ICalculation> _calculationMock;

        /// <summary>
        /// The _ami mock
        /// </summary>
        private Mock<IAMI> _amiMock;

        /// <summary>
        /// The _billing mock
        /// </summary>
        private Mock<IBilling> _billingMock;

        /// <summary>
        /// The _disposed
        /// </summary>
        private bool _disposed;

        /// <summary>
        /// Test Case Init
        /// </summary>
        [TestInitialize]
        public void Init()
        {
            _unityContainer = new UnityContainer();
            RegisterTypesForUnity.RegisterTypes(_unityContainer);
            var overrides = new ParameterOverrides
            {
                {"logModel", new LogModel {DisableLog = true} }
            };
            _evaluationFacadeManager = _unityContainer.Resolve<EvaluationFacade>(overrides);
            _evaluationMock = new Mock<IEvaluation>();
            _subscriptionMock = new Mock<ISubscription>();
            _calculationMock = new Mock<ICalculation>();
            _amiMock = new Mock<IAMI>();
            _billingMock = new Mock<IBilling>();
            _evaluationFacadeManager.EvaluationManager = _evaluationMock.Object;
            _evaluationFacadeManager.SubscriptionManager = _subscriptionMock.Object;
            _evaluationFacadeManager.CalculationManager = _calculationMock.Object;
            _evaluationFacadeManager.AmiManager = _amiMock.Object;
            _evaluationFacadeManager.BillingManager = _billingMock.Object;
        }

        /// <summary>
        /// Test Case for default constructor of EvaluationFacade model
        /// </summary>
        [TestMethod]
        public void EvaluationFacade()
        {
            // ReSharper disable once ObjectCreationAsStatement
            new EvaluationFacade();
            Assert.AreEqual(true, true);
        }

        /// <summary>
        /// Test Case for accessing Log Model
        /// </summary>
        [TestMethod]
        public void AccessLogModel()
        {
            _evaluationFacadeManager.LogModel.DisableLog = true;
            Assert.AreEqual(true, _evaluationFacadeManager.LogModel.DisableLog);
        }

        /// <summary>
        /// Test case for ProcessEvaluation method in EvaluationFacade class
        /// </summary>
        [TestMethod]
        public void ProcessEvaluation()
        {
            IList<SubscriptionModel> listSubscriptionModels = new List<SubscriptionModel> { new SubscriptionModel() };
            IList<BillingModel> listBillingModels = new List<BillingModel>
            {
                new BillingModel
                {
                    AccountId = "SC001",
                    ClientId = 87,
                    MeterType = "ami"
                }
            };

            _subscriptionMock.Setup(t => t.GetCustomerSubscriptions(It.IsAny<int>(), It.IsAny<string>(), It.IsAny<string>())).Returns(listSubscriptionModels);

            _billingMock.Setup(t => t.GetAllServicesFromLastBill(It.IsAny<int>(), It.IsAny<string>(), It.IsAny<string>())).Returns(listBillingModels);
            _billingMock.Setup(t => t.GetServiceDetailsFromBillAsync(It.IsAny<int>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>())).Returns(Task.FromResult(listBillingModels.FirstOrDefault()));

            _calculationMock.Setup(t => t.GetCtdCalculationAsync(It.IsAny<int>(), It.IsAny<string>(), It.IsAny<string>())).Returns(Task.FromResult(new CalculationModel { ServiceContractId = "SC001", RateClass = "", BillCycleEndDate = DateTime.Now.ToString(CultureInfo.InvariantCulture), BillCycleStartDate = DateTime.Now.ToString(CultureInfo.InvariantCulture) }));
            _calculationMock.Setup(t => t.GetBtdCalculationAsync(It.IsAny<int>(), It.IsAny<string>())).Returns(Task.FromResult(new CalculationModel { ServiceContractId = "SC001", RateClass = "", BillCycleEndDate = DateTime.Now.ToString(CultureInfo.InvariantCulture), BillCycleStartDate = DateTime.Now.ToString(CultureInfo.InvariantCulture) }));

            _amiMock.Setup(t => t.GetAmi(It.IsAny<DateTime>(), It.IsAny<string>(), It.IsAny<int>())).ReturnsAsync(new Ami15MinuteIntervalModel());
            _evaluationMock.Setup(t =>t.EvaluateInsight(It.IsAny<IEnumerable<SubscriptionModel>>(),It.IsAny<IEnumerable<CalculationModel>>(), It.IsAny<ClientSettings>(), It.IsAny<List<dynamic>>(),It.IsAny<List<TierBoundary>>(), It.IsAny<DateTime>(), It.IsAny<BillingModel>())).Returns(Task.FromResult(typeof(void)));

            _evaluationFacadeManager.ProcessEvaluation(87, It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<DateTime>());
            _evaluationFacadeManager.ProcessEvaluation(87, It.IsAny<string>(), It.IsAny<string>(), "SC001", It.IsAny<DateTime>());
            Assert.AreEqual(1, 1);
        }

        /// <summary>
        /// Processes the evaluation model is null.
        /// </summary>
        [TestMethod]
        public void ProcessEvaluationModelIsNull()
        {
            IList<SubscriptionModel> listSubscriptionModels = new List<SubscriptionModel> { new SubscriptionModel() };
            IList<BillingModel> listBillingModels = new List<BillingModel>
            {
                new BillingModel
                {
                    AccountId = "SC001",
                    ClientId = 87,
                    MeterType = "ami"
                }
            };

            _subscriptionMock.Setup(t => t.GetCustomerSubscriptions(It.IsAny<int>(), It.IsAny<string>(), It.IsAny<string>())).Returns(listSubscriptionModels);

            _billingMock.Setup(t => t.GetAllServicesFromLastBill(It.IsAny<int>(), It.IsAny<string>(), It.IsAny<string>())).Returns(listBillingModels);
            _billingMock.Setup(t => t.GetServiceDetailsFromBillAsync(It.IsAny<int>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>())).Returns(Task.FromResult(listBillingModels.FirstOrDefault()));

            _calculationMock.Setup(t => t.GetCtdCalculationAsync(It.IsAny<int>(), It.IsAny<string>(), It.IsAny<string>())).Returns(Task.FromResult((CalculationModel)null));
            _calculationMock.Setup(t => t.GetBtdCalculationAsync(It.IsAny<int>(), It.IsAny<string>())).Returns(Task.FromResult((CalculationModel)null));

            _amiMock.Setup(t => t.GetAmi(It.IsAny<DateTime>(), It.IsAny<string>(), It.IsAny<int>()))
                .ReturnsAsync(new Ami15MinuteIntervalModel());

            _evaluationMock.Setup(t =>t.EvaluateInsight(It.IsAny<IEnumerable<SubscriptionModel>>(),It.IsAny<IEnumerable<CalculationModel>>(), It.IsAny<ClientSettings>(), It.IsAny<List<dynamic>>(),It.IsAny<List<TierBoundary>>(), It.IsAny<DateTime>(), It.IsAny<BillingModel>())).Returns(Task.FromResult(typeof(void)));

            _evaluationFacadeManager.ProcessEvaluation(87, It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<DateTime>());
            _evaluationFacadeManager.ProcessEvaluation(87, It.IsAny<string>(), It.IsAny<string>(), "SC001", It.IsAny<DateTime>());
            Assert.AreEqual(1, 1);
        }

        /// <summary>
        /// Cleans up method.
        /// </summary>
        [TestCleanup]
        public void CleanUpMethod()
        {
            CallContext.FreeNamedDataSlot("log4net.Util.LogicalThreadContextProperties");
        }

        /// <summary>
        /// Releases unmanaged and - optionally - managed resources.
        /// </summary>
        /// <param name="disposing"><c>true</c> to release both managed and unmanaged resources; <c>false</c> to release only unmanaged resources.</param>
        private void Dispose(bool disposing)
        {
            if (!_disposed)
            {
                if (disposing)
                {
                    _unityContainer.Dispose();
                }
            }
            _disposed = true;
        }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            Dispose(true);
            // ReSharper disable once GCSuppressFinalizeForTypeWithoutDestructor
            GC.SuppressFinalize(this);
        }
    }
}
