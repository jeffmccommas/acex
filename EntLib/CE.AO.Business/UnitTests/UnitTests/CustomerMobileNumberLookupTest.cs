﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.Practices.Unity;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using CE.AO.Business;
using Moq;
using CE.AO.DataAccess;
using CE.AO.Entities.TableStorageEntities;
using CE.AO.Logging;
using CE.AO.Models;
using Microsoft.WindowsAzure.Storage.Table;

namespace UnitTests
{
    /// <summary>
    /// contains test methods for CustomerMobileNumberLookup class
    /// </summary>
    [TestClass]
    public class CustomerMobileNumberLookupTest
    {
        private UnityContainer _unityContainer;
        private CustomerMobileNumberLookup _customerMobileNumberLookupManager;
        private Mock<ITableRepository> _tableRepositoryMock;

        /// <summary>
        /// Test Case Init
        /// </summary>
        [TestInitialize]
        public void Init()
        {
            AutoMapperConfig.AutoMapperMappings();
            _tableRepositoryMock = new Mock<ITableRepository>();
            _unityContainer = new UnityContainer();
            RegisterTypesForUnity.RegisterTypes(_unityContainer);
            var overrides = new ParameterOverrides
            {
                {"logModel", new LogModel {DisableLog = true} }
            };
            _customerMobileNumberLookupManager = _unityContainer.Resolve<CustomerMobileNumberLookup>(overrides);
            _customerMobileNumberLookupManager.TableRepository = _tableRepositoryMock.Object;
        }

        /// <summary>
        /// Test Case for accessing default constructor
        /// </summary>
        [TestMethod]
        public void CustomerMobileNumberLookup()
        {
            // ReSharper disable once ObjectCreationAsStatement
            new CustomerMobileNumberLookup();
            Assert.AreEqual(true,true);
        }

        /// <summary>
        /// Test Case for accessing Log Model
        /// </summary>
        [TestMethod]
        public void AccessLogModel()
        {
            _customerMobileNumberLookupManager.LogModel.DisableLog = true;
            Assert.AreEqual(true, _customerMobileNumberLookupManager.LogModel.DisableLog);
        }

        /// <summary>
        /// Test case for GetCustomerMobileNumberLookup method in CustomerMobileNumberLookup class
        /// </summary>
        [TestMethod]
        public void GetCustomerMobileNumberLookup()
        {
            var customerMobileNumberModel = GetCustomerMobLookup();
            var customerMobileNumberEntity =Mapper.Map<CustomerMobileNumberLookupModel, CustomerMobileNumberLookupEntity>(customerMobileNumberModel);
            _tableRepositoryMock.Setup(t =>t.GetSingle<CustomerMobileNumberLookupEntity>(It.IsAny<string>(), It.IsAny<string>(),It.IsAny<string>())).Returns(customerMobileNumberEntity).Verifiable();
            var result = _customerMobileNumberLookupManager.GetCustomerMobileNumberLookup(1, "123456789", "3");
            _tableRepositoryMock.Verify(t =>t.GetSingle<CustomerMobileNumberLookupEntity>(It.IsAny<string>(), It.IsAny<string>(),It.IsAny<string>()), Times.Once);
            _tableRepositoryMock.VerifyAll();
            Assert.AreEqual(result.ClientId, 1);
        }

        /// <summary>
        /// Test case for InsertOrMergeCustomerMobileNumberLookupAsync method in CustomerMobileNumberLookup class
        /// </summary>
        [TestMethod]
        public void InsertOrMergeCustomerMobileNumberLookupAsync()
        {
            var customerMobile = GetCustomerMobLookup();
            _tableRepositoryMock.Setup(t => t.InsertOrMergeSingleAsync(It.IsAny<string>(), It.IsAny<CustomerMobileNumberLookupEntity>())).Returns(Task.FromResult(true)).Verifiable();
            _customerMobileNumberLookupManager.TableRepository = _tableRepositoryMock.Object;
            var result = _customerMobileNumberLookupManager.InsertOrMergeCustomerMobileNumberLookupAsync(customerMobile).Result;
            _tableRepositoryMock.Verify(t => t.InsertOrMergeSingleAsync(It.IsAny<string>(), It.IsAny<CustomerMobileNumberLookupEntity>()), Times.Once);
            _tableRepositoryMock.VerifyAll();
            Assert.AreEqual(true, result);
        }
        
        /// <summary>
        /// Test case for GetAllCustomerMobileNumberLookup method in CustomerMobileNumberLookup class
        /// </summary>
        [TestMethod]
        public void GetAllCustomerMobileNumberLookup()
        {
           var filter = TableQuery.GenerateFilterCondition("PartitionKey", QueryComparisons.Equal, CustomerMobileNumberLookupEntity.SetPartitionKey(1, "123456789"));
            List<CustomerMobileNumberLookupEntity> list = new List<CustomerMobileNumberLookupEntity>();
            var customerMobile = Mapper.Map<CustomerMobileNumberLookupModel, CustomerMobileNumberLookupEntity>(GetCustomerMobLookup());
            list.Add(customerMobile);
            _customerMobileNumberLookupManager.TableRepository = _tableRepositoryMock.Object;
            _tableRepositoryMock.Setup(t => t.GetAllAsync<CustomerMobileNumberLookupEntity>(It.IsAny<string>(), filter)).Returns(list).Verifiable();
            List<CustomerMobileNumberLookupModel> result = _customerMobileNumberLookupManager.GetAllCustomerMobileNumberLookup(1,"123456789").ToList();
            _tableRepositoryMock.Verify(t => t.GetAllAsync<CustomerMobileNumberLookupEntity>(It.IsAny<string>(), filter), Times.Once);
            _tableRepositoryMock.VerifyAll();
            Assert.AreEqual(result.Count, 1);
        }

        private static CustomerMobileNumberLookupModel GetCustomerMobLookup()
        {
            return new CustomerMobileNumberLookupModel
            {
                ClientId=1,
                CustomerId="2",
                AccountId="3",
                MobileNumber="123456789"
            };
        }
    }
}
