﻿using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using Microsoft.Practices.Unity;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using CE.AO.Business;
using Moq;
using CE.AO.DataAccess;
using CE.AO.Entities.TableStorageEntities;
using CE.AO.Logging;
using CE.AO.Models;
using Microsoft.WindowsAzure.Storage.Table;

namespace UnitTests
{
    /// <summary>
    /// contains test methods for ClientWidgetConfiguration class
    /// </summary>
    [TestClass]
    public class ClientWidgetConfigurationTest
    {
        private UnityContainer _unityContainer;
        private ClientWidgetConfiguration _clientWidgetConfigurationManager;
        private Mock<ITableRepository> _tableRepositoryMock;

        /// <summary>
        /// Test Case Init
        /// </summary>
        [TestInitialize]
        public void Init()
        {
            AutoMapperConfig.AutoMapperMappings();
            _tableRepositoryMock = new Mock<ITableRepository>();
            _unityContainer = new UnityContainer();
            RegisterTypesForUnity.RegisterTypes(_unityContainer);
            var overrides = new ParameterOverrides
            {
                {"logModel", new LogModel {DisableLog = true}}
            };
            _clientWidgetConfigurationManager = _unityContainer.Resolve<ClientWidgetConfiguration>(overrides);
            _clientWidgetConfigurationManager.TableRepository = _tableRepositoryMock.Object;
        }

        /// <summary>
        /// Test Case for accessing default constructor
        /// </summary>
        [TestMethod]
        public void ClientWidgetConfiguration()
        {
            // ReSharper disable once ObjectCreationAsStatement
            new ClientWidgetConfiguration();
            Assert.AreEqual(true, true);
        }

        /// <summary>
        /// Test Case for accessing Log Model
        /// </summary>
        [TestMethod]
        public void AccessLogModel()
        {
            _clientWidgetConfigurationManager.LogModel.DisableLog = true;
            Assert.AreEqual(true, _clientWidgetConfigurationManager.LogModel.DisableLog);
        }

        /// <summary>
        /// Test case for GetClientWidgetConfiguration method in ClientWidgetConfiguration class
        /// </summary>
        [TestMethod]
        public void GetClientWidgetConfiguration()
        {
            var partitionKey = ClientAccountEntity.SetPartitionKey(1);
            var filter = TableQuery.GenerateFilterCondition("PartitionKey", QueryComparisons.Equal,partitionKey);
            var list = new List<ClientWidgetConfigurationEntity>();
            var clientWidConfigEntity = Mapper.Map<ClientWidgetConfigurationModel, ClientWidgetConfigurationEntity>(GetClientWidConfigEntity());
            list.Add(clientWidConfigEntity);
            _tableRepositoryMock.Setup(t => t.GetAllAsync<ClientWidgetConfigurationEntity>(It.IsAny<string>(), filter)).Returns(list).Verifiable();
            List<ClientWidgetConfigurationModel> result = _clientWidgetConfigurationManager.GetClientWidgetConfiguration(1).ToList();
            _tableRepositoryMock.Verify(t => t.GetAllAsync<ClientWidgetConfigurationEntity>(It.IsAny<string>(), filter), Times.Once);
            _tableRepositoryMock.VerifyAll();
            Assert.AreEqual(result.Count, 1);
        }
        
        private static ClientWidgetConfigurationModel GetClientWidConfigEntity()
        {
            return new ClientWidgetConfigurationModel
            {
                ClientId = 1,
                WebTokenUrl="",
                WidgetPageUrl="",
                AccessKeyId="2",
                Password="ABC"
            };
        }
    }
}