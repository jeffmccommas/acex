﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using CE.AO.Business;
using CE.AO.Models;

namespace UnitTests
{
    [TestClass()]
    public class SendSmsTests
    {
        [TestMethod()]
        public void GetSubscriptionIdTest()
        {
            var customerModel = new CustomerModel
            {
                Phone1 = "6173315402",
                FirstName = "Lee",
                LastName = "Chu"
            };
            var username = "aclaradev";
            var apiKey = "b4c1971153b3509b6ec0d8a24a33454c";
            var contactList = "AclaraDemoDev";

            var smsManager = new SendSms();
            var requestId = smsManager.CreateNewTrumpiaSubscription(customerModel, apiKey, username, contactList);

            var subscriptonId = smsManager.GetSubscriptionId(customerModel, requestId, apiKey, username);

            Assert.IsNotNull(subscriptonId);
            Assert.AreNotEqual(string.Empty,subscriptonId);
            
        }
    }
}