﻿using System;
using System.Threading.Tasks;
using AutoMapper;
using CE.AO.Business;
using CE.AO.DataAccess;
using CE.AO.Entities.TableStorageEntities;
using CE.AO.Models;
using Microsoft.Practices.Unity;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace UnitTests
{
    /// <summary>
    /// Contains test methods for AccountUpdates Class
    /// </summary>
    /// <seealso cref="System.IDisposable" />
    [TestClass]
    public class AccountUpdatesTest : IDisposable
    {
        /// <summary>
        /// The _unity container
        /// </summary>
        private UnityContainer _unityContainer;

        /// <summary>
        /// The _account updates manager
        /// </summary>
        private AccountUpdates _accountUpdatesManager;

        /// <summary>
        /// The _table repository mock
        /// </summary>
        private Mock<ITableRepository> _tableRepositoryMock;

        /// <summary>
        /// The _disposed
        /// </summary>
        private bool _disposed;

        /// <summary>
        /// Initializes this instance.
        /// </summary>
        [TestInitialize]
        public void Init()
        {
            AutoMapperConfig.AutoMapperMappings();
            _tableRepositoryMock = new Mock<ITableRepository>();
            _unityContainer = new UnityContainer();
            RegisterTypesForUnity.RegisterTypes(_unityContainer);
            _accountUpdatesManager = _unityContainer.Resolve<AccountUpdates>();
            _accountUpdatesManager.TableRepository = _tableRepositoryMock.Object;
        }

        /// <summary>
        /// Test Case for accessing Log Model
        /// </summary>
        [TestMethod]
        public void AccessLogModel()
        {
            _accountUpdatesManager.LogModel.DisableLog = true;
            Assert.AreEqual(true, _accountUpdatesManager.LogModel.DisableLog);
        }

        /// <summary>
        /// Test Case for default constructor of AccountUpdates model
        /// </summary>
        [TestMethod]
        public void AccountUpdates()
        {
            // ReSharper disable once ObjectCreationAsStatement
            new AccountUpdates();
            Assert.AreEqual(true, true);
        }

        /// <summary>
        /// Test case for Inserts the or merge account update asynchronous.
        /// </summary>
        [TestMethod]
        public void InsertOrMergeAccountUpdateAsync()
        {
            var accountUpdateEntity = Mapper.Map<AccountUpdatesModel, AccountUpdatesEntity>(GetSampleAccountUpdateModel());
            _tableRepositoryMock.Setup(t => t.InsertOrMergeSingleAsync(It.IsAny<string>(), It.IsAny<AccountUpdatesEntity>())).Returns(Task.FromResult(true)).Verifiable();
            var result = _accountUpdatesManager.InsertOrMergeAccountUpdateAsync(Mapper.Map<AccountUpdatesEntity, AccountUpdatesModel>(accountUpdateEntity), false).Result;
            _tableRepositoryMock.Verify(t => t.InsertOrMergeSingleAsync(It.IsAny<string>(), It.IsAny<AccountUpdatesEntity>()), Times.Once);
            _tableRepositoryMock.VerifyAll();
            Assert.AreEqual(true, result);
        }

        /// <summary>
        /// Test case for Gets the account update.
        /// </summary>
        [TestMethod]
        public void GetAccountUpdate()
        {
            var accountUpdatesEntity = Mapper.Map<AccountUpdatesModel, AccountUpdatesEntity>(GetSampleAccountUpdateModel());
            var partitionKey = AccountUpdatesEntity.SetPartitionKey(87, "123123");
            var rowKey = AccountUpdatesEntity.SetRowKey("RateClass", "sc1", "123123", false);
            _tableRepositoryMock.Setup(t => t.GetSingleAsync<AccountUpdatesEntity>(It.IsAny<string>(), partitionKey, rowKey)).Returns(Task.FromResult(accountUpdatesEntity)).Verifiable();
            var result = _accountUpdatesManager.getAccountUpdate(87, "123123", "RateClass", "sc1", false).Result;
            _tableRepositoryMock.Verify(t => t.GetSingleAsync<AccountUpdatesEntity>(It.IsAny<string>(), partitionKey, rowKey), Times.Exactly(1));
            _tableRepositoryMock.VerifyAll();
            Assert.AreEqual(result.ClientId, 87);
        }

        /// <summary>
        /// Gets the sample account update model.
        /// </summary>
        /// <returns></returns>
        private static AccountUpdatesModel GetSampleAccountUpdateModel()
        {
            return new AccountUpdatesModel()
            {
                ClientId = 87,
                CustomerId = "123123",
                AccountId = "A",
                PremiseId = "",
                MeterId = "METER_QA_02",
                NewValue = "",
                OldValue = "",
                ServiceContractId = "sc1",
                UpdateType = "RateClass"
            };
        }

        /// <summary>
        /// Releases unmanaged and - optionally - managed resources.
        /// </summary>
        /// <param name="disposing"><c>true</c> to release both managed and unmanaged resources; <c>false</c> to release only unmanaged resources.</param>
        private void Dispose(bool disposing)
        {
            if (!_disposed)
            {
                if (disposing)
                {
                    _unityContainer.Dispose();
                }
            }
            _disposed = true;
        }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            Dispose(true);
            // ReSharper disable GCSuppressFinalizeForTypeWithoutDestructor
            GC.SuppressFinalize(this);
        }
    }
}
