﻿using System;
using System.Collections.Generic;
using System.Reflection;
using AutoMapper;
using CE.AO.Business;
using CE.AO.DataAccess;
using CE.AO.Entities.TableStorageEntities;
using CE.AO.Models;
using Microsoft.Practices.Unity;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.WindowsAzure.Storage.Table;
using Moq;

namespace UnitTests
{
    /// <summary>
    /// Contains test methods for BillingCycleSchedule Class
    /// </summary>
    /// <seealso cref="System.IDisposable" />
    [TestClass]
    public class BillingCycleScheduleTest : IDisposable
    {
        /// <summary>
        /// The _unity container
        /// </summary>
        private UnityContainer _unityContainer;

        /// <summary>
        /// The _billing cycle schedule manager
        /// </summary>
        private BillingCycleSchedule _billingCycleScheduleManager;

        /// <summary>
        /// The _table repository mock
        /// </summary>
        private Mock<ITableRepository> _tableRepositoryMock;

        /// <summary>
        /// The _disposed
        /// </summary>
        private bool _disposed;

        /// <summary>
        /// Initializes this instance.
        /// </summary>
        [TestInitialize]
        public void Init()
        {
            AutoMapperConfig.AutoMapperMappings();
            _tableRepositoryMock = new Mock<ITableRepository>();
            _unityContainer = new UnityContainer();
            RegisterTypesForUnity.RegisterTypes(_unityContainer);
            _billingCycleScheduleManager = _unityContainer.Resolve<BillingCycleSchedule>();
            _billingCycleScheduleManager.TableRepository = _tableRepositoryMock.Object;
        }

        /// <summary>
        /// Test case for accessesing the log model
        /// </summary>
        [TestMethod]
        public void AccessLogModel()
        {
            _billingCycleScheduleManager.LogModel.DisableLog = true;
            Assert.AreEqual(true, _billingCycleScheduleManager.LogModel.DisableLog);
        }

        /// <summary>
        /// Test Case for default constructor of BillingCycleSchedule model
        /// </summary>
        [TestMethod]
        public void BillingCycleSchedule()
        {
            // ReSharper disable once ObjectCreationAsStatement
            new BillingCycleSchedule();
            Assert.AreEqual(true, true);
        }

        /// <summary>
        /// Test case for getting the billing cycle schedule by date asynchronous.
        /// </summary>
        [TestMethod]
        public void GetBillingCycleScheduleByDateAsync()
        {
            var billCycleScheduleEntity = GetSampleBillingCycleScheduleEntity();
            IList<BillCycleScheduleEntity> list = new List<BillCycleScheduleEntity>();
            list.Add(billCycleScheduleEntity);
            var filter = TableQuery.CombineFilters(TableQuery.GenerateFilterCondition("PartitionKey", QueryComparisons.Equal, BillCycleScheduleEntity.SetPartitionKey(87, "Cycle1")),TableOperators.And,
              TableQuery.GenerateFilterCondition("RowKey", QueryComparisons.GreaterThanOrEqual,DateTime.UtcNow.ToString("yyyy-MM-dd")));
            _tableRepositoryMock.Setup(t => t.GetAllAsync<BillCycleScheduleEntity>(It.IsAny<string>(), filter)).Returns(list).Verifiable();
            var result = _billingCycleScheduleManager.GetBillingCycleScheduleByDateAsync(87, "Cycle1", DateTime.UtcNow);
            _tableRepositoryMock.Verify(t => t.GetAllAsync<BillCycleScheduleEntity>(It.IsAny<string>(), filter), Times.Exactly(1));
            _tableRepositoryMock.VerifyAll();
            var billCycleScheduleModel = Mapper.Map<BillCycleScheduleEntity, BillCycleScheduleModel>(billCycleScheduleEntity);
            PropertyValuesAreEquals(result, billCycleScheduleModel);
        }

        /// <summary>
        /// Gets the sample billing cycle schedule entity.
        /// </summary>
        /// <returns></returns>
        private static BillCycleScheduleEntity GetSampleBillingCycleScheduleEntity()
        {
            return new BillCycleScheduleEntity()
            {
                ClientId = "1",
                BillCycleScheduleName = "Q1",
                BeginDate = DateTime.Now.ToShortDateString(),
                EndDate = DateTime.Now.ToShortDateString(),
                Description = "Data is for Testing purpose only"
            };
        }

        /// <summary>
        /// Properties the values are equals.
        /// </summary>
        /// <param name="actual">The actual.</param>
        /// <param name="expected">The expected.</param>
        /// <returns></returns>
        private static void PropertyValuesAreEquals(object actual, object expected)
        {
            PropertyInfo[] properties = expected.GetType().GetProperties();
            foreach (var property in properties)
            {
                var expectedValue = property.GetValue(expected, null);
                var actualValue = property.GetValue(actual, null);
                if (!Equals(expectedValue, actualValue))
                {
                }
            }
        }

        /// <summary>
        /// Releases unmanaged and - optionally - managed resources.
        /// </summary>
        /// <param name="disposing"><c>true</c> to release both managed and unmanaged resources; <c>false</c> to release only unmanaged resources.</param>
        private void Dispose(bool disposing)
        {
            if (!_disposed)
            {
                if (disposing)
                {
                    _unityContainer.Dispose();
                }
            }
            _disposed = true;
        }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            Dispose(true);
            // ReSharper disable once GCSuppressFinalizeForTypeWithoutDestructor
            GC.SuppressFinalize(this);
        }
    }
}
