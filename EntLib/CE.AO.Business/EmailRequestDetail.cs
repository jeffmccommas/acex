﻿using System;
using System.Threading.Tasks;
using AO.BusinessContracts;
using CE.AO.DataAccess;
using CE.AO.Models;
using Microsoft.Practices.Unity;

namespace CE.AO.Business
{
    public class EmailRequestDetail : IEmailRequestDetail
    {
        public EmailRequestDetail()
        {
            LogModel = new LogModel { DisableLog = false };
        }

        public EmailRequestDetail(LogModel logModel)
        {
            LogModel = logModel;
        }

        public LogModel LogModel { get; set; }
        [Dependency]
        public ITableRepository TableRepository { get; set; }
        public EmailRequestDetailModel GetEmailRequestDetail(int clientId, string accountId, string programName,
            string serviceContractId, string insightName, string defaultCommunicationChannel, string date, string eventType)
        {
            throw new NotImplementedException();

            //var entity = TableRepository.GetSingle<EmailRequestDetailEntity>(Constants.TableNames.EmailRequestDetail,
            //    partitionKey, rowKey);
            //return Mapper.Map<EmailRequestDetailEntity, EmailRequestDetailModel>(entity); 
        }

        public Task<EmailRequestDetailModel> GetEmailRequestDetailAsync(int clientId, string accountId, string programName,
            string serviceContractId, string insightName, string defaultCommunicationChannel, string date)
        {
            throw new NotImplementedException();
            //var entity =
            //       await
            //           TableRepository.GetSingleAsync<EmailRequestDetailEntity>(Constants.TableNames.EmailRequestDetail, partitionKey,
            //               rowKey);
            //return Mapper.Map<EmailRequestDetailEntity, EmailRequestDetailModel>(entity);
        }

        public bool InsertOrMergeEmailRequestDetail(EmailRequestDetailModel model)
        {
            throw new NotImplementedException();
            //            return TableRepository.InsertOrMergeSingle(Constants.TableNames.EmailRequestDetail, Mapper.Map<EmailRequestDetailModel, EmailRequestDetailEntity>(model));
        }

        public Task<bool> InsertOrMergeEmailRequestDetailAsync(EmailRequestDetailModel model)
        {
            throw new NotImplementedException();
            //            return await TableRepository.InsertOrMergeSingleAsync(Constants.TableNames.EmailRequestDetail, Mapper.Map<EmailRequestDetailModel, EmailRequestDetailEntity>(model));
        }
    }
}
