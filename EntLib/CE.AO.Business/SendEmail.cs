﻿using SendGrid;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Threading.Tasks;
using AO.BusinessContracts;
using Microsoft.Azure;

namespace CE.AO.Business
{
    public class SendEmail : ISendEmail
    {
        /// <summary>
        /// Send Email
        /// </summary>
        /// <param name="sender">The sender</param>
        /// <param name="recipients">The message recipients</param>
        /// <param name="identifiers">This replacement key must exist in the message body. There should be one value for each recipient in the To list</param>
        /// <param name="templateId">The active sendgrid Template Id</param>
        /// <param name="username">sendgrid username</param>
        /// <param name="password">sendgrid password</param>
        /// <param name="customKey">Combination of partionkey and rowkey</param>
        /// <param name="notifyDateTime">Send at time</param>
        /// <returns></returns>
        public async Task SendEmailAsync(string sender, IEnumerable<string> recipients,
            Dictionary<string, List<string>> identifiers, string templateId, string username, string password, string customKey, DateTime? notifyDateTime)
        {
            DateTime utcNotifyDateTime = notifyDateTime ?? DateTime.UtcNow;

            if (string.IsNullOrEmpty(username) || string.IsNullOrEmpty(password))
            {
                throw new Exception(
                    "Sendgrid configurations- sendGridUserName or sendGridPassword, missing in config file.");
            }
            if (string.IsNullOrEmpty(sender))
                throw new Exception("Sender cannot be empty.");

            var toList = recipients as IList<string> ?? recipients.ToList();
            if (recipients == null || !toList.Any())
                throw new Exception("At least one recipient is required.");

            if (string.IsNullOrEmpty(templateId))
                throw new Exception("TemplateId cannot be empty.");


            //create a new message object
            var message = new SendGridMessage();

            foreach (var recipient in toList)
            {
                message.AddTo(recipient);
            }

            // To avoid sending email to actual users in testing phase
            var emailOverride = CloudConfigurationManager.GetSetting("EmailOverride");
            if (!string.IsNullOrEmpty(emailOverride))
            {
                var emailList = emailOverride.Split(';');
                var toMailList = message.To.ToList();
                for (int i = toMailList.Count - 1; i >= 0; i--)
                {
                    toMailList.RemoveAt(i);
                }
                message.To = toMailList.ToArray();
                foreach (var email in emailList)
                {
                    message.AddTo(email);
                }
            }
            message.From = new MailAddress(sender);
            message.Html = "&nbsp;";
            message.Text = " ";
            message.Subject = " ";

            if (!string.IsNullOrEmpty(customKey))
            {
                var uniqueArg = new Dictionary<string, string> { { "customKey", customKey } };
                message.Header.AddUniqueArgs(uniqueArg);
            }
            message.Header.SetSendAt(utcNotifyDateTime);

            foreach (var identifier in identifiers)
            {
                message.AddSubstitution(identifier.Key, identifier.Value);
            }

            message.EnableTemplateEngine(templateId);

            var transportInstance =
                new Web(new NetworkCredential(username, password));

            await transportInstance.DeliverAsync(message);
        }

        /// <summary>
        /// Send Email
        /// </summary>
        /// <param name="sender">The sender</param>
        /// <param name="recipients">The message recipients</param>
        /// <param name="identifiers">This replacement key must exist in the message body. There should be one value for each recipient in the To list</param>
        /// <param name="body">Email body</param>
        /// <param name="subject">Email Subject</param>
        /// <param name="username">sendgrid username</param>
        /// <param name="password">sendgrid password</param>
        /// <param name="customKey">Combination of partionkey and rowkey</param>
        /// <param name="notifyDateTime">Send at time</param>
        /// <returns></returns>
        public async Task SendEmailAsync(string sender, IEnumerable<string> recipients,
            Dictionary<string, List<string>> identifiers, string body, string subject, string username, string password, string customKey, DateTime? notifyDateTime)
        {
            DateTime utcNotifyDateTime = notifyDateTime ?? DateTime.UtcNow;

            if (string.IsNullOrEmpty(username) || string.IsNullOrEmpty(password))
            {
                throw new Exception(
                    "Sendgrid configurations- sendGridUserName or sendGridPassword, missing in config file.");
            }
            if (string.IsNullOrEmpty(sender))
                throw new Exception("Sender cannot be empty.");

            var toList = recipients as IList<string> ?? recipients.ToList();
            if (recipients == null || !toList.Any())
                throw new Exception("At least one recipient is required.");

            //create a new message object
            var message = new SendGridMessage();

            foreach (var recipient in toList)
            {
                message.AddTo(recipient);
            }
            // To avoid sending email to actual users in testing phase
            var emailOverride = CloudConfigurationManager.GetSetting("EmailOverride");
            if (!string.IsNullOrEmpty(emailOverride))
            {
                message.To.ToList().Clear();
                message.AddTo(emailOverride);
            }

            message.From = new MailAddress(sender);
            message.Html = body;
            message.Text = body;
            message.Subject = subject;

            var uniqueArg = new Dictionary<string, string> { { "customKey", customKey } };
            message.Header.AddUniqueArgs(uniqueArg);
            message.Header.SetSendAt(utcNotifyDateTime);

            foreach (var identifier in identifiers)
            {
                message.AddSubstitution(identifier.Key, identifier.Value);
            }

            var transportInstance =
                new Web(new NetworkCredential(username, password));

            await transportInstance.DeliverAsync(message);

        }

        public async Task SendEmailAsync(MailMessage message,
            Dictionary<string, List<string>> identifiers, string templateId, string username, string password, string customKey, DateTime? notifyDateTime)
        {
            DateTime utcNotifyDateTime = notifyDateTime ?? DateTime.UtcNow;
            var recipients = message.To.Select(m => m.Address);
            await SendEmailAsync(message.Sender.Address, recipients, identifiers, templateId, username, password, customKey, utcNotifyDateTime);
        }

        public async Task SendEmailAsync(MailMessage message, string templateId, string username, string password, string customKey, DateTime? notifyDateTime)
        {
            DateTime utcNotifyDateTime = notifyDateTime ?? DateTime.UtcNow;
            var recipients = message.To.Select(m => m.Address);
            await SendEmailAsync(message.Sender.Address, recipients, null, templateId, username, password, customKey, utcNotifyDateTime);
        }

        public async Task SendEmailAsync(MailMessage message, string username, string password, string customKey, DateTime? notifyDateTime)
        {
            DateTime utcNotifyDateTime = notifyDateTime ?? DateTime.UtcNow;
            var recipients = message.To.Select(m => m.Address);
            await SendEmailAsync(message.Sender.Address, recipients, null, message.Body, message.Subject, username, password, customKey, utcNotifyDateTime);
        }
    }
}
