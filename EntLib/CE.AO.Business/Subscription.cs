﻿using AutoMapper;
using CE.AO.DataAccess;
using CE.AO.Entities;
using CE.AO.Entities.TableStorageEntities;
using CE.AO.Logging;
using CE.AO.Models;
using Microsoft.Practices.Unity;
using Microsoft.WindowsAzure.Storage.Table;
using System;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.Linq;
using AO.BusinessContracts;

namespace CE.AO.Business
{
    public class Subscription : ISubscription
    {
        public Subscription(LogModel logModel,ITableRepository tableRepository)
        {
            LogModel = logModel;
            TableRepository = tableRepository;
        }

        public LogModel LogModel { get; set; }
     
        public ITableRepository TableRepository { get; set; }

        [Dependency]
        public Lazy<IClientConfigFacade> ClientConfigFacadeManager { get; set; }

        public async Task<bool> InsertSubscriptionAsync(SubscriptionModel model, bool addEmailConfirmationCount, bool addSmsConfirmationCount)
        {
            var tasks = new List<Task<bool>>();
            var entity = Mapper.Map<SubscriptionModel, SubscriptionEntity>(model);
            entity.PartitionKey = SubscriptionEntity.SetPartitionKey(model.ClientId, model.CustomerId, model.AccountId);

            var subscriptionData = await GetSubscriptionDetailsAsync(entity.ClientId, entity.CustomerId, entity.AccountId, entity.ProgramName, entity.ServiceContractId, entity.InsightTypeName);

            if (subscriptionData != null)
            {
                if (subscriptionData.IsSelected && entity.IsSelected == false)
                {
                    entity.OptOutDate = DateTime.UtcNow;
                    entity.SubscriptionDate = subscriptionData.SubscriptionDate;
                    entity.EmailConfirmationCount = subscriptionData.EmailConfirmationCount;
                    entity.SmsConfirmationCount = subscriptionData.SmsConfirmationCount;
                }
                else if (subscriptionData.IsSelected == false && entity.IsSelected)
                {
                    entity.OptOutDate = null;
                    entity.SubscriptionDate = DateTime.UtcNow;
                    entity.EmailConfirmationCount = null;
                    entity.SmsConfirmationCount = null;
                }
                else
                {
                    entity.OptOutDate = subscriptionData.OptOutDate;
                    entity.SubscriptionDate = subscriptionData.SubscriptionDate;
                    entity.EmailConfirmationCount = subscriptionData.EmailConfirmationCount;
                    entity.SmsConfirmationCount = subscriptionData.SmsConfirmationCount;
                }

                if ((subscriptionData.IsEmailOptInCompleted == false || subscriptionData.IsEmailOptInCompleted == null) && entity.IsEmailOptInCompleted == true)
                    entity.EmailOptInCompletedDate = DateTime.UtcNow;
                else if (entity.IsEmailOptInCompleted == false && subscriptionData.IsEmailOptInCompleted == true)
                    entity.EmailOptInCompletedDate = null;
                else
                {
                    entity.EmailOptInCompletedDate = subscriptionData.EmailOptInCompletedDate;
                    entity.IsEmailOptInCompleted = subscriptionData.IsEmailOptInCompleted;
                }

                if ((subscriptionData.IsSmsOptInCompleted == false || subscriptionData.IsSmsOptInCompleted == null) &&
                         entity.IsSmsOptInCompleted == true)
                    entity.SmsOptInCompletedDate = DateTime.UtcNow;
                else if (entity.IsSmsOptInCompleted == false && subscriptionData.IsSmsOptInCompleted == true)
                    entity.SmsOptInCompletedDate = null;
                else
                {
                    entity.SmsOptInCompletedDate = subscriptionData.SmsOptInCompletedDate;
                    entity.IsSmsOptInCompleted = subscriptionData.IsSmsOptInCompleted;
                }

                if (addEmailConfirmationCount)
                {
                    if (subscriptionData.EmailConfirmationCount == null ||
                                      subscriptionData.EmailConfirmationCount <= 0)
                        entity.EmailConfirmationCount = 1;
                    else
                        entity.EmailConfirmationCount += 1;
                }

                if (addSmsConfirmationCount)
                {
                    if (subscriptionData.SmsConfirmationCount == null ||
                                      subscriptionData.SmsConfirmationCount <= 0)
                        entity.SmsConfirmationCount = 1;
                    else
                        entity.SmsConfirmationCount += 1;
                }

                var partitionKey = SubscriptionEntity.SetPartitionKey(subscriptionData.ClientId,
                    subscriptionData.CustomerId, subscriptionData.AccountId);
                var rowKey = SubscriptionEntity.SetLatestRowKey(subscriptionData.ProgramName,
                    subscriptionData.ServiceContractId, subscriptionData.InsightTypeName);
                var historyRowKey = SubscriptionEntity.SetHistoryRowKey(subscriptionData.ProgramName,
                    subscriptionData.ServiceContractId, subscriptionData.InsightTypeName);
                await DeleteSubscriptionDetailsAsync(partitionKey, rowKey);
                await DeleteSubscriptionDetailsAsync(partitionKey, historyRowKey);
            }
            else
            {
                entity.SubscriptionDate = DateTime.UtcNow;
                if (entity.IsSmsOptInCompleted == true)
                    entity.SmsOptInCompletedDate = DateTime.UtcNow;

                if (entity.IsEmailOptInCompleted == true)
                    entity.EmailOptInCompletedDate = DateTime.UtcNow;

                if (!entity.IsSelected)
                {
                    entity.OptOutDate = DateTime.UtcNow;
                }
            }

            tasks.Add(InsertOrMergeLatestSubscriptionAsync(entity));
            tasks.Add(InsertOrMergeHistorySubscriptionAsync(entity));

            var taskResults = await Task.WhenAll(tasks);
            return !taskResults.Any(result => false);
        }

        public bool InsertSubscription(SubscriptionModel model)
        {
            var entity = Mapper.Map<SubscriptionModel, SubscriptionEntity>(model);
            entity.PartitionKey = SubscriptionEntity.SetPartitionKey(model.ClientId, model.CustomerId, model.AccountId);

            var subscriptionData = GetSubscriptionDetails(entity.ClientId, entity.CustomerId, entity.AccountId, entity.ProgramName, entity.ServiceContractId, entity.InsightTypeName);

            if (subscriptionData != null)
            {
                if (subscriptionData.IsSelected && entity.IsSelected == false)
                {
                    entity.OptOutDate = DateTime.UtcNow;
                    entity.SubscriptionDate = subscriptionData.SubscriptionDate;
                    entity.EmailConfirmationCount = subscriptionData.EmailConfirmationCount;
                    entity.SmsConfirmationCount = subscriptionData.SmsConfirmationCount;
                }
                else if (subscriptionData.IsSelected == false && entity.IsSelected)
                {
                    entity.OptOutDate = null;
                    entity.SubscriptionDate = DateTime.UtcNow;
                    entity.EmailConfirmationCount = null;
                    entity.SmsConfirmationCount = null;
                }
                else
                {
                    entity.OptOutDate = subscriptionData.OptOutDate;
                    entity.SubscriptionDate = subscriptionData.SubscriptionDate;
                    entity.EmailConfirmationCount = subscriptionData.EmailConfirmationCount;
                    entity.SmsConfirmationCount = subscriptionData.SmsConfirmationCount;
                }

                if ((subscriptionData.IsEmailOptInCompleted == false || subscriptionData.IsEmailOptInCompleted == null) && entity.IsEmailOptInCompleted == true)
                    entity.EmailOptInCompletedDate = DateTime.UtcNow;
                else if (entity.IsEmailOptInCompleted == false && subscriptionData.IsEmailOptInCompleted == true)
                    entity.EmailOptInCompletedDate = null;
                else
                {
                    entity.EmailOptInCompletedDate = subscriptionData.EmailOptInCompletedDate;
                    entity.IsEmailOptInCompleted = subscriptionData.IsEmailOptInCompleted;
                }

                if ((subscriptionData.IsSmsOptInCompleted == false || subscriptionData.IsSmsOptInCompleted == null) &&
                         entity.IsSmsOptInCompleted == true)
                    entity.SmsOptInCompletedDate = DateTime.UtcNow;
                else if (entity.IsSmsOptInCompleted == false && subscriptionData.IsSmsOptInCompleted == true)
                    entity.SmsOptInCompletedDate = null;
                else
                {
                    entity.SmsOptInCompletedDate = subscriptionData.SmsOptInCompletedDate;
                    entity.IsSmsOptInCompleted = subscriptionData.IsSmsOptInCompleted;
                }

                var partitionKey = SubscriptionEntity.SetPartitionKey(subscriptionData.ClientId,
                    subscriptionData.CustomerId, subscriptionData.AccountId);
                var rowKey = SubscriptionEntity.SetLatestRowKey(subscriptionData.ProgramName,
                    subscriptionData.ServiceContractId, subscriptionData.InsightTypeName);
                var historyRowKey = SubscriptionEntity.SetHistoryRowKey(subscriptionData.ProgramName,
                    subscriptionData.ServiceContractId, subscriptionData.InsightTypeName);
                DeleteSubscriptionDetails(partitionKey, rowKey);
                DeleteSubscriptionDetails(partitionKey, historyRowKey);
            }
            else
            {
                entity.SubscriptionDate = DateTime.UtcNow;
                if (entity.IsSmsOptInCompleted == true)
                    entity.SmsOptInCompletedDate = DateTime.UtcNow;

                if (entity.IsEmailOptInCompleted == true)
                    entity.EmailOptInCompletedDate = DateTime.UtcNow;

                if (!entity.IsSelected)
                {
                    entity.OptOutDate = DateTime.UtcNow;
                }
            }

            InsertOrMergeLatestSubscription(entity);
            InsertOrMergeHistorySubscription(entity);

            return true;
        }

        public async Task<bool> InsertOrMergeLatestSubscriptionAsync(SubscriptionEntity entity)
        {
            entity.RowKey = SubscriptionEntity.SetLatestRowKey(entity.ProgramName, entity.ServiceContractId,
                entity.InsightTypeName);
            return await TableRepository.InsertOrMergeSingleAsync(Constants.TableNames.Subscription, entity);
        }

        // ReSharper disable once UnusedMethodReturnValue.Local
        private bool InsertOrMergeLatestSubscription(SubscriptionEntity entity)
        {
            entity.RowKey = SubscriptionEntity.SetLatestRowKey(entity.ProgramName, entity.ServiceContractId,
                entity.InsightTypeName);
            return TableRepository.InsertOrMergeSingle(Constants.TableNames.Subscription, entity);
        }

        public async Task<bool> InsertOrMergeHistorySubscriptionAsync(SubscriptionEntity entity)
        {
            entity.RowKey = SubscriptionEntity.SetHistoryRowKey(entity.ProgramName, entity.ServiceContractId,
              entity.InsightTypeName);
            return await TableRepository.InsertOrMergeSingleAsync(Constants.TableNames.Subscription, entity);
        }

        // ReSharper disable once UnusedMethodReturnValue.Local
        private bool InsertOrMergeHistorySubscription(SubscriptionEntity entity)
        {
            entity.RowKey = SubscriptionEntity.SetHistoryRowKey(entity.ProgramName, entity.ServiceContractId,
              entity.InsightTypeName);
            return TableRepository.InsertOrMergeSingle(Constants.TableNames.Subscription, entity);
        }

        public async Task<bool> ReplaceLatestSubscriptionAsync(SubscriptionEntity entity, string partitionKey)
        {
            entity.RowKey = SubscriptionEntity.SetHistoryRowKey(entity.ProgramName, entity.ServiceContractId, entity.InsightTypeName);
            return await TableRepository.ReplaceSingleAsync(Constants.TableNames.Subscription, partitionKey, entity.RowKey, entity, null);
        }

        public async Task<bool> ReplaceHistorySubscriptionAsync(SubscriptionEntity entity, string partitionKey)
        {
            entity.RowKey = SubscriptionEntity.SetHistoryRowKey(entity.ProgramName, entity.ServiceContractId, entity.InsightTypeName);
            return await TableRepository.ReplaceSingleAsync(Constants.TableNames.Subscription, partitionKey, entity.RowKey, entity, null);
        }

        public async Task<SubscriptionModel> GetSubscriptionDetailsAsync(int clientId, string customerId, string accountId, string programName, string serviceContractId, string insightTypeName)
        {
            var partitionKey = SubscriptionEntity.SetPartitionKey(clientId, customerId, accountId);
            var rowkey = SubscriptionEntity.SetLatestRowKey(programName, serviceContractId, insightTypeName);
            var subscriptionDetails =
                await TableRepository.GetSingleAsync<SubscriptionEntity>(Constants.TableNames.Subscription, partitionKey, rowkey);
            return Mapper.Map<SubscriptionEntity, SubscriptionModel>(subscriptionDetails);
        }

        public SubscriptionModel GetSubscriptionDetails(int clientId, string customerId, string accountId, string programName, string serviceContractId, string insightTypeName)
        {
            var partitionKey = SubscriptionEntity.SetPartitionKey(clientId, customerId, accountId);
            var rowkey = SubscriptionEntity.SetLatestRowKey(programName, serviceContractId, insightTypeName);
            var subscriptionDetails =
                TableRepository.GetSingle<SubscriptionEntity>(Constants.TableNames.Subscription, partitionKey, rowkey);
            return Mapper.Map<SubscriptionEntity, SubscriptionModel>(subscriptionDetails);
        }

        private async Task<bool> DeleteSubscriptionDetailsAsync(string partitionKey, string rowKey)
        {
            return await TableRepository.DeleteSingleAsync<SubscriptionEntity>(Constants.TableNames.Subscription, partitionKey, rowKey);
        }

        // ReSharper disable once UnusedMethodReturnValue.Local
        private bool DeleteSubscriptionDetails(string partitionKey, string rowKey)
        {
            return TableRepository.DeleteSingle<SubscriptionEntity>(Constants.TableNames.Subscription, partitionKey, rowKey);
        }

        public IEnumerable<SubscriptionModel> GetCustomerSubscriptions(int clientId, string customerId, string accountId)
        {
            var filter = TableQuery.CombineFilters(
                TableQuery.GenerateFilterCondition("PartitionKey", QueryComparisons.Equal, SubscriptionEntity.SetPartitionKey(clientId, customerId, accountId)),
                TableOperators.And,
                TableQuery.CombineFilters(
                    TableQuery.GenerateFilterCondition("RowKey", QueryComparisons.GreaterThanOrEqual, "A"),
                    TableOperators.And,
                    TableQuery.GenerateFilterCondition("RowKey", QueryComparisons.LessThan, "B"))
                );

            var subscriptions = TableRepository.GetAllAsync<SubscriptionEntity>(Constants.TableNames.Subscription,
                filter).ToList();

            filter = TableQuery.CombineFilters(
                TableQuery.GenerateFilterCondition("PartitionKey", QueryComparisons.Equal, SubscriptionEntity.SetPartitionKey(clientId, customerId, accountId)),
                TableOperators.And,
                TableQuery.CombineFilters(
                    TableQuery.GenerateFilterCondition("RowKey", QueryComparisons.GreaterThanOrEqual, "S"),
                    TableOperators.And,
                    TableQuery.GenerateFilterCondition("RowKey", QueryComparisons.LessThan, "T"))
                );

            var insightSubscriptions = TableRepository.GetAllAsync<SubscriptionEntity>(Constants.TableNames.Subscription, filter).ToList();
            subscriptions.AddRange(insightSubscriptions);

            return Mapper.Map<IEnumerable<SubscriptionEntity>, IEnumerable<SubscriptionModel>>(subscriptions);
        }

        public List<SubscriptionModel> MergeDefaultSubscriptionListWithSubscribedPrograms(List<SubscriptionModel> subscriptions, List<SubscriptionModel> defaultSubscriptions)
        {
            if (subscriptions == null || subscriptions.Count == 0)
            {
                defaultSubscriptions.ForEach(s => s.IsSelected = false);
            }
            else
            {
                // program level
                var program = subscriptions.FindAll(s => !string.IsNullOrEmpty(s.ProgramName) && string.IsNullOrEmpty(s.InsightTypeName));
                if (program.Count > 0)
                {
                    program.ForEach(p =>
                    {
                        defaultSubscriptions.ForEach(ds =>
                        {
                            if (ds.ProgramName.Equals(p.ProgramName, StringComparison.OrdinalIgnoreCase))
                            {
                                ds.IsSelected = p.IsSelected;
                            }
                        });
                    });

                    var serviceDefaultSubscriptions = defaultSubscriptions.GroupBy(s => s.ServiceContractId);

                    foreach (var serivces in serviceDefaultSubscriptions)
                    {
                        var serviceDefaults = serivces.ToList();
                        var defaultServiceInsights = defaultSubscriptions.FindAll(
                            s =>
                                !string.IsNullOrEmpty(s.ProgramName) && !string.IsNullOrEmpty(s.InsightTypeName) && s.ServiceContractId == serivces.Key);

                        var insights = serviceDefaults.FindAll(s => !string.IsNullOrEmpty(s.ProgramName) && !string.IsNullOrEmpty(s.InsightTypeName));
                        insights.ForEach(i =>
                        {
                            if (
                            subscriptions.Exists(
                                s => s.InsightTypeName.IndexOf(i.InsightTypeName, StringComparison.OrdinalIgnoreCase) >= 0 && s.ProgramName.ToLower() == i.ProgramName.ToLower() &&
                                     ((!string.IsNullOrEmpty(s.ServiceContractId) &&
                                       s.ServiceContractId == i.ServiceContractId) ||
                                      string.IsNullOrEmpty(s.ServiceContractId))))
                            {
                                var insightList =
                                subscriptions.FindAll(
                                    s =>
                                        s.InsightTypeName.ToLower() == i.InsightTypeName.ToLower() && s.ProgramName.ToLower() == i.ProgramName.ToLower() &&
                                        ((!string.IsNullOrEmpty(s.ServiceContractId) &&
                                          s.ServiceContractId == i.ServiceContractId) ||
                                         string.IsNullOrEmpty(s.ServiceContractId)));
                                foreach (var insight in insightList)
                                {
                                    i.IsSelected = insight.IsSelected;
                                    i.SubscriberThresholdValue = insight.SubscriberThresholdValue.HasValue && insight.SubscriberThresholdValue != 0 ? insight.SubscriberThresholdValue :
                                                             i.SubscriberThresholdValue;
                                    i.Channel = insight.Channel ?? i.Channel;
                                }
                            }

                            if (!string.IsNullOrEmpty(i.ServiceContractId))
                            {

                                var subscription = defaultServiceInsights.Find(
                                d =>
                                    d.ProgramName.ToLower() == i.ProgramName.ToLower() && d.InsightTypeName.ToLower() == i.InsightTypeName.ToLower() &&
                                    i.ServiceContractId == d.ServiceContractId);

                                subscription.IsSelected = i.IsSelected;
                                subscription.SubscriberThresholdValue = i.SubscriberThresholdValue;
                                subscription.Channel = i.Channel;
                            }
                            else
                            {
                                var subscription = defaultServiceInsights.FindAll(
                                d =>
                                    d.ProgramName.ToLower() == i.ProgramName.ToLower() && d.InsightTypeName.ToLower() == i.InsightTypeName.ToLower() &&
                                    string.IsNullOrEmpty(d.ServiceContractId));

                                foreach (var subscriptionModel in subscription)
                                {
                                    subscriptionModel.IsSelected = i.IsSelected;
                                    subscriptionModel.SubscriberThresholdValue = i.SubscriberThresholdValue;
                                    subscriptionModel.Channel = i.Channel;
                                }
                            }
                        });
                    }


                }
                else // insight level
                {
                    foreach (var subscription in subscriptions)
                    {
                        defaultSubscriptions.ForEach(
                            ds =>
                            {
                                if (ds.ProgramName.ToLower() == subscription.ProgramName.ToLower() && ds.InsightTypeName.ToLower() == subscription.InsightTypeName.ToLower() && subscription.ServiceContractId == ds.ServiceContractId)
                                {
                                    ds.IsSelected = subscription.IsSelected;
                                    ds.SubscriberThresholdValue = subscription.SubscriberThresholdValue.HasValue && subscription.SubscriberThresholdValue != 0 ? subscription.SubscriberThresholdValue : ds.SubscriberThresholdValue;
                                    ds.Channel = subscription.Channel;
                                }
                            }
                            );
                    }
                }
            }
            return defaultSubscriptions;
        }

        public IEnumerable<SubscriptionModel> GetSubscription(int clientId, DateTime startDate, DateTime endDate, string program)
        {
            var dateFilter = TableQuery.CombineFilters(
                  TableQuery.GenerateFilterConditionForDate("SubscriptionDate", QueryComparisons.GreaterThanOrEqual, startDate)
                  , TableOperators.And,
                  TableQuery.GenerateFilterConditionForDate("SubscriptionDate", QueryComparisons.LessThanOrEqual, endDate)
                  );

            var partitionKeyFilter =
                TableQuery.CombineFilters(
                  TableQuery.GenerateFilterCondition("PartitionKey", QueryComparisons.GreaterThanOrEqual, clientId.ToString()), TableOperators.And, TableQuery.GenerateFilterCondition("PartitionKey", QueryComparisons.LessThanOrEqual, (clientId + 1).ToString()));

            var partialfilter =
                TableQuery.CombineFilters(
                  TableQuery.GenerateFilterCondition("InsightTypeName", QueryComparisons.Equal, null), TableOperators.And, TableQuery.GenerateFilterCondition("RowKey", QueryComparisons.Equal, "A_" + program));

            string filter = TableQuery.CombineFilters(dateFilter, TableOperators.And, partitionKeyFilter);
            string finalFilter = TableQuery.CombineFilters(filter, TableOperators.And, partialfilter);

            var insightSubscriptions = TableRepository.GetAllAsync<SubscriptionEntity>(Constants.TableNames.Subscription, finalFilter).ToList();

            return Mapper.Map<List<SubscriptionEntity>,List<SubscriptionModel>>(insightSubscriptions);
        }

        public bool InsertorMergeForSmsOptOut(BillingModel billingModel, List<SubscriptionModel> subscriptions, int clientId, string customerId, string accountId, string serviceContractId, string programName, string insightName)
        {

            var programSubscriptionModel =
                GetSubscriptionDetails(clientId, customerId, accountId, programName, String.Empty, string.Empty);
            if (programSubscriptionModel != null)
            {
                var insightSubscriptionModel =
                    GetSubscriptionDetails(clientId, customerId, accountId, programName, serviceContractId, insightName);

                if (insightSubscriptionModel != null)
                {
                    if (insightSubscriptionModel.IsSelected)
                    {
                        if (insightSubscriptionModel.Channel ==
                            Convert.ToString(Utilities.Enums.SubscriptionChannel.EmailAndSMS))
                        {
                            insightSubscriptionModel.Channel = Convert.ToString(Utilities.Enums.SubscriptionChannel.Email);
                            insightSubscriptionModel.IsSmsOptInCompleted = false;
                        }
                        else if (insightSubscriptionModel.Channel ==
                                 Convert.ToString(Utilities.Enums.SubscriptionChannel.SMS))
                        {
                            insightSubscriptionModel.IsSelected = false;
                            insightSubscriptionModel.IsSmsOptInCompleted = false;
                        }
                    }
                }
                else
                {

                    var clientSettings = ClientConfigFacadeManager.Value.GetClientSettings(clientId);

                    Extensions.MergeSettings(subscriptions, clientSettings, billingModel);
                    var program = clientSettings.Programs.FirstOrDefault(p => p.UtilityProgramName == programName);
                    var insight = program?.Insights.FirstOrDefault(i => i.UtilityInsightName == insightName);
                    if (insight != null)
                    {
                        if (insight.DefaultCommunicationChannel.Contains(
                            Convert.ToString(Utilities.Enums.SubscriptionChannel.EmailAndSMS))
                            &&
                            (programSubscriptionModel.Channel ==
                             Convert.ToString(Utilities.Enums.SubscriptionChannel.EmailAndSMS) ||
                             string.IsNullOrWhiteSpace(programSubscriptionModel.Channel)))
                        {
                            insightSubscriptionModel = new SubscriptionModel
                            {
                                ClientId = clientId,
                                CustomerId = customerId,
                                AccountId = accountId,
                                ProgramName = program.UtilityProgramName,
                                InsightTypeName = insight.UtilityInsightName,
                                ServiceContractId = serviceContractId,
                                IsSelected = true,
                                SmsOptInCompletedDate = programSubscriptionModel.SmsOptInCompletedDate,
                                IsSmsOptInCompleted = false,
                                EmailOptInCompletedDate = programSubscriptionModel.EmailOptInCompletedDate,
                                IsEmailOptInCompleted = programSubscriptionModel.IsEmailOptInCompleted,
                                Channel = Convert.ToString(Utilities.Enums.SubscriptionChannel.Email)
                            };
                        }
                        else if (insight.DefaultCommunicationChannel.Contains(
                            Convert.ToString(Utilities.Enums.SubscriptionChannel.SMS))
                                 &&
                                 (programSubscriptionModel.Channel ==
                                  Convert.ToString(Utilities.Enums.SubscriptionChannel.SMS) ||
                                  string.IsNullOrWhiteSpace(programSubscriptionModel.Channel)))
                            insightSubscriptionModel = new SubscriptionModel
                            {
                                ClientId = clientId,
                                CustomerId = customerId,
                                AccountId = accountId,
                                ProgramName = program.UtilityProgramName,
                                InsightTypeName = insight.UtilityInsightName,
                                ServiceContractId = serviceContractId,
                                IsSelected = false,
                                SmsOptInCompletedDate = programSubscriptionModel.SmsOptInCompletedDate,
                                IsSmsOptInCompleted = false,
                                EmailOptInCompletedDate = programSubscriptionModel.EmailOptInCompletedDate,
                                IsEmailOptInCompleted = programSubscriptionModel.IsEmailOptInCompleted,
                                Channel = Convert.ToString(Utilities.Enums.SubscriptionChannel.SMS)
                            };
                    }
                }

                if (insightSubscriptionModel != null)
                {
                    if (!InsertSubscription(insightSubscriptionModel))
                    {
                        Logger.Fatal(
                            @" Insertion failed for subscription optout.", LogModel);
                        return false;
                    }

                    // opt-out at program level
                    if (insightSubscriptionModel.IsSelected || subscriptions.Exists(
                        s =>
                            s.Channel.Equals(Utilities.Enums.SubscriptionChannel.EmailAndSMS.GetDescription(),
                                StringComparison.CurrentCultureIgnoreCase)))
                    {
                        programSubscriptionModel.IsSelected = true;
                        programSubscriptionModel.IsSmsOptInCompleted = false;
                    }
                    else
                    {
                        programSubscriptionModel.IsSelected = false;
                        programSubscriptionModel.IsSmsOptInCompleted = false;
                    }

                    if (!InsertSubscription(programSubscriptionModel))
                    {
                        Logger.Fatal(
                            @" Insertion failed for subscription optout.", LogModel);
                        return false;
                    }
                }

                //if (!subscription.InsertSubscription(subscriptionModel))
                //{
                //    Logger.Fatal(
                //        @" Insertion failed for subscription optout.", LogModel);
                //    return false;
                //}
            }

            return true;
        }

        public Task<bool> DeleteSubscriptionAsync(int clientId, string type, string customerId, string accountId)
        {
            throw new NotImplementedException();
        }
    }
}
