﻿using System.Collections.Generic;
using System.Threading.Tasks;
using AO.BusinessContracts;
using AutoMapper;
using CE.AO.DataAccess;
using CE.AO.Entities.TableStorageEntities;
using CE.AO.Models;
using CE.AO.Entities;
using Microsoft.WindowsAzure.Storage.Table;
using System;

namespace CE.AO.Business
{
    public class Premise : IPremise
    {
        public Premise(LogModel logModel, ITableRepository tableRepository)
        {
            LogModel = logModel;
            TableRepository = tableRepository;
        }

        public ITableRepository TableRepository { get; set; }
        public LogModel LogModel { get; set; }

        public async Task<bool> InsertOrMergePremiseAsync(PremiseModel model)
        {
            var entity = Mapper.Map<PremiseModel, PremiseEntity>(model);
            entity.PartitionKey = PremiseEntity.SetPartitionKey(entity.ClientId, entity.CustomerId);
            entity.RowKey = PremiseEntity.SetRowKey(entity.PremiseId);
            return await TableRepository.InsertOrMergeSingleAsync(Constants.TableNames.CustomerPremiseProfile, entity);
        }

        public IEnumerable<PremiseModel> GetPremises(int clientId, string customerId)
        {
            //var partitionKey = PremiseEntity.SetPartitionKey(clientId, customerId);
            //var rowKey = PremiseEntity.SetRowKey(customerId);
            var filter = TableQuery.CombineFilters(
                TableQuery.GenerateFilterCondition("PartitionKey", QueryComparisons.Equal, PremiseEntity.SetPartitionKey(clientId, customerId)),
                TableOperators.And,
                TableQuery.CombineFilters(
                    TableQuery.GenerateFilterCondition("RowKey", QueryComparisons.GreaterThanOrEqual, "P"),
                    TableOperators.And,
                    TableQuery.GenerateFilterCondition("RowKey", QueryComparisons.LessThan, "Q"))
                );
            IEnumerable<PremiseEntity> entity = TableRepository.GetAllAsync<PremiseEntity>(Constants.TableNames.CustomerPremiseProfile,
                filter);

            return Mapper.Map<IEnumerable<PremiseEntity>, IEnumerable<PremiseModel>>(entity);
        }

        public Task<bool> DeletePremisesAsync(int clientId, string customerId, string premiseId)
        {
            throw new NotImplementedException();
        }
    }
}
