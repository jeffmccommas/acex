﻿using AutoMapper;
using CE.AO.DataAccess;
using CE.AO.Entities;
using CE.AO.Entities.TableStorageEntities;
using CE.AO.Logging;
using CE.AO.Models;
using CE.RateModel;
using Enums = CE.AO.Utilities.Enums;
using Microsoft.Practices.Unity;
using Microsoft.WindowsAzure.Storage.Table;
using System.Collections.Generic;
using System;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using AO.BusinessContracts;

namespace CE.AO.Business
{
    public class Evaluation : IEvaluation
    {
        public Evaluation(LogModel logModel, ITableRepository tableRepository)
        {
            LogModel = logModel;
            TableRepository = tableRepository;
        }

        public LogModel LogModel { get; set; }

        public ITableRepository TableRepository { get; set; }

        [Dependency]
        public IQueueRepository QueueRepository { get; set; }

        public async Task EvaluateInsight(IEnumerable<SubscriptionModel> subscriptions,
           IEnumerable<CalculationModel> calculations, ClientSettings clientSettings, List<TallAmiModel> amiList, List<TierBoundary> tierBoundaries, DateTime asOfDate, BillingModel billingmodel)
        {
            IList<SubscriptionModel> subscriptionModels = subscriptions as IList<SubscriptionModel> ?? subscriptions.ToList();
            MergeSettings(subscriptionModels, clientSettings, billingmodel);

            // loop through all insights
            IList<CalculationModel> calculationModels = calculations as IList<CalculationModel> ?? calculations.ToList();
            foreach (var program in clientSettings.Programs)
            {
                foreach (var calculation in calculationModels)
                {
                    if (clientSettings.MinimumDaysForInsightNotification != 0 &&
                            (calculation.BillDays < clientSettings.MinimumDaysForInsightNotification))
                        continue;
                    var insightModel = new InsightModel
                    {
                        ProgramName = program.ProgramName,
                        ServiceContractId = calculation.ServiceContractId,
                        AccountId = calculation.AccountId,
                        ClientId = calculation.ClientId
                    };
                    var serviceAmi = amiList.FirstOrDefault(e => e.MeterId == calculation.MeterId);
                    int? amiUomId = null;
                    double? dailyConsumption = serviceAmi != null ? GetDailyConsumption(serviceAmi, out amiUomId) : null;
                    foreach (var insight in program.Insights)
                    {
                        string commodityType = calculation.CommodityId == null
                               ? string.Empty
                               : ((Enums.CommodityType)calculation.CommodityId).GetDescription();

                        if ((calculation.CommodityId != null && insight.CommodityType.Contains(commodityType)) || string.IsNullOrEmpty(calculation.ServiceContractId))
                        {
                            var customInsightModel = Extensions.GetCustomInsightModel(subscriptionModels.ToList(), insight, program, calculation.ServiceContractId, commodityType, billingmodel?.UOMId);
                            InsightEvaluation(insightModel, calculation, dailyConsumption, tierBoundaries, customInsightModel, amiUomId);
                        }
                    }
                    insightModel.AsOfEvaluationDate = asOfDate;
                    insightModel.BillCycleStartDate = calculation.BillCycleStartDate;
                    insightModel.BillCycleEndDate = calculation.BillCycleEndDate;
					insightModel.AsOfEvaluationId = Guid.NewGuid();

					await SaveInsight(insightModel, string.IsNullOrEmpty(calculation.ServiceContractId));

                    string message =
                        $"{calculation.ClientId}^^{calculation.CustomerId}^^{calculation.AccountId}^^{calculation.ServiceContractId}^^{program.ProgramName}^^{LogModel.Source}^^{LogModel.RowIndex}^^{LogModel.Metadata}^^{(int)LogModel.ProcessingType}^^{insightModel.AsOfEvaluationId}";
                    await QueueRepository.AddQueueAsync(Constants.QueueNames.AoNotificationQueue, message);
                }
            }
        }

        private double? GetDailyConsumption(dynamic serviceAmi, out int? amiUomId)
        {
            double? dailyConsumption = 0;
            var properties = (PropertyInfo[])serviceAmi.GetType().GetProperties();
            //Get interval type. Needed in case of daily to get touid.
            var intervalType = Convert.ToInt32(serviceAmi.GetType().GetProperty("IntervalType").GetValue(serviceAmi));
            //In case of daily ami consumption will be sum of all TOU consumption
            if (intervalType == 24)
                properties.Where(p => p.Name.Contains("TOU_")).ToList().ForEach(
                        property =>
                        {
                            dailyConsumption = dailyConsumption +
                                               (property.GetValue(serviceAmi) != null
                                                   ? Convert.ToDouble(property.GetValue(serviceAmi))
                                                   : 0);
                        });
            else
                properties.Where(p => p.Name.Contains("IntValue")).ToList()
                    .ForEach(
                        property =>
                        {
                            dailyConsumption = dailyConsumption +
                                               (property.GetValue(serviceAmi) != null
                                                   ? Convert.ToDouble(property.GetValue(serviceAmi))
                                                   : 0);
                        });

            // get the uom id from ami
            var uomId = properties.FirstOrDefault(p => p.Name.Contains("UOMId"));
            amiUomId = uomId?.GetValue(serviceAmi) != null
                // ReSharper disable once ConstantConditionalAccessQualifier
                ? Convert.ToInt32(uomId?.GetValue(serviceAmi))
                : null;
            return dailyConsumption;
        }

        private void InsightEvaluation(InsightModel insightModel, CalculationModel calculation, double? dailyConsumption, List<TierBoundary> tierBoundaries, CustomInsightModel customInsightModel, int? amiUom)
        {
            switch (customInsightModel.InsightName)
            {
                case "BillToDate":
                    if (customInsightModel.IsAccountLevel)
                        insightModel.BtdCost = calculation.Cost;
                    break;
                case "CostToDate":
                    if (!customInsightModel.IsAccountLevel)
                        insightModel.CtdCost = calculation.Cost;
                    break;
                case "AccountProjectedCost":
                    if (customInsightModel.IsAccountLevel)
                        insightModel.BtdProjectedCost = calculation.ProjectedCost;
                    break;
                case "ServiceProjectedCost":
                    if (!customInsightModel.IsAccountLevel)
                        insightModel.CtdProjectedCost = calculation.ProjectedCost;
                    break;
                case "Usage":
                    if (!customInsightModel.IsAccountLevel)
                        insightModel.Usage = calculation.Usage;
                    break;
                case "AccountLevelCostThreshold":
                    if (customInsightModel.IsAccountLevel)
                        insightModel.AccountLevelCostThresholdExceed = calculation.Cost > customInsightModel.ThresholdValue;
                    break;
                case "ServiceLevelUsageThreshold":
                    if (!customInsightModel.IsAccountLevel)
                    {
                        // if the uom of the usage (from ami) is different from bill
                        // convert the usage into bill's uom, then evaluate
                        double? usage;
                        if (amiUom.HasValue && customInsightModel.UomId.HasValue && amiUom != customInsightModel.UomId)
                        {
                            var conversionFactor = Extensions.GetConversionFactor(insightModel.ClientId, amiUom.Value, customInsightModel.UomId.Value);
                            usage = calculation.Usage * conversionFactor;
                        }
                        else
                        {
                            usage = calculation.Usage;
                        }
                        insightModel.ServiceLevelUsageThresholdExceed = usage > customInsightModel.ThresholdValue;
                    }
                    break;
                case "ServiceLevelCostThreshold":
                    if (!customInsightModel.IsAccountLevel)
                        insightModel.ServiceLevelCostThresholdExceed = calculation.Cost > customInsightModel.ThresholdValue;
                    break;
                case "DayThreshold":
                    if (!customInsightModel.IsAccountLevel)
                    {
                        if (dailyConsumption != null)
                        {
                            // if the uom of the usage (from ami) is different from bill
                            // convert the usage into bill's uom, then evaluate
                            double? consumption;
                            if (amiUom.HasValue && customInsightModel.UomId.HasValue && amiUom != customInsightModel.UomId)
                            {
                                var conversionFactor = Extensions.GetConversionFactor(insightModel.ClientId, amiUom.Value, customInsightModel.UomId.Value);
                                consumption = dailyConsumption * conversionFactor;
                            }
                            else
                            {
                                consumption = dailyConsumption;
                            }
                            insightModel.DayThresholdExceed = consumption > customInsightModel.ThresholdValue;
                        }
                        else
                            Logger.Info("No AMI received to evaluate Day Threshold", LogModel);
                    }
                    break;
                case "ServiceLevelTieredThresholdApproaching":
                    if (!customInsightModel.IsAccountLevel)
                    {
                        if (tierBoundaries != null && tierBoundaries.Count > 0)
                        {
                            TierBoundary tierBoundary;
                            var tierApproaching = GetServiceLevelTieredThreshold(tierBoundaries, calculation, true, out tierBoundary, customInsightModel.ThresholdValue);
                            switch (tierBoundary.BaseOrTier)
                            {
                                case RateModel.Enums.BaseOrTier.Tier1:
                                    insightModel.ServiceLevelTiered1ThresholdApproaching = tierApproaching;
                                    break;
                                case RateModel.Enums.BaseOrTier.Tier2:
                                    insightModel.ServiceLevelTiered2ThresholdApproaching = tierApproaching;
                                    break;
                                case RateModel.Enums.BaseOrTier.Tier3:
                                    insightModel.ServiceLevelTiered3ThresholdApproaching = tierApproaching;
                                    break;
                                case RateModel.Enums.BaseOrTier.Tier4:
                                    insightModel.ServiceLevelTiered4ThresholdApproaching = tierApproaching;
                                    break;
                                case RateModel.Enums.BaseOrTier.Tier5:
                                    insightModel.ServiceLevelTiered5ThresholdApproaching = tierApproaching;
                                    break;
                                case RateModel.Enums.BaseOrTier.Tier6:
                                    insightModel.ServiceLevelTiered6ThresholdApproaching = tierApproaching;
                                    break;
                            }
                        }
                        else
                            Logger.Info("No Tiered boundaries found to evaluate Service Level Tiered Threshold Approaching", LogModel);
                    }
                    break;
                case "ServiceLevelTieredThresholdExceed":
                    if (!customInsightModel.IsAccountLevel)
                    {
                        if (tierBoundaries != null && tierBoundaries.Count > 0)
                        {

                            TierBoundary tierBoundary;
                            insightModel.ServiceLevelTieredThresholdExceed = GetServiceLevelTieredThreshold(tierBoundaries, calculation, false, out tierBoundary, 0);
                        }
                        else
                            Logger.Info("No Tiered boundaries found to evaluate Service Level Tiered Threshold Exceed", LogModel);
                    }
                    break;
            }
        }

        private void MergeSettings(IList<SubscriptionModel> subscriptionModels, ClientSettings clientSettings, BillingModel billingModel)
        {
            foreach (var program in clientSettings.Programs)
            {
                var listOfInsightsSubscribed =
                    subscriptionModels.Where(e => e.ProgramName == program.UtilityProgramName).ToList();

                if (listOfInsightsSubscribed.Count > 0)
                {
                    foreach (var insight in listOfInsightsSubscribed)
                    {
                        if (!string.IsNullOrEmpty(insight.InsightTypeName))
                        {
                            InsightSettings ins;
                            if (billingModel == null)
                            {
                                ins = program.Insights.FirstOrDefault(
                                        e => e.UtilityInsightName == insight.InsightTypeName);
                            }
                            else
                            {
                                ins =
                                    program.Insights.FirstOrDefault(
                                        e => e.UtilityInsightName == insight.InsightTypeName && e.CommodityType.Contains(((Enums.CommodityType)billingModel.CommodityId).GetDescription()));
                            }
                            if (ins == null) continue;
                            ins.DefaultCommunicationChannel = !string.IsNullOrEmpty(insight.Channel)
                                ? ins.AllowedCommunicationChannel.ToLower(CultureInfo.CurrentCulture).Contains(insight.Channel.ToLower(CultureInfo.CurrentCulture)) ? insight.Channel : null
                                : ins.DefaultCommunicationChannel;
                        }
                    }
                }
            }
        }

        private async Task SaveInsight(InsightModel insightModel, bool isAccountLevel)
        {
            var entityModel = Mapper.Map<InsightModel, InsightEntity>(insightModel);
            entityModel.PartitionKey = InsightEntity.SetInsightPartitionKey(entityModel.ClientId, entityModel.AccountId);

            entityModel.RowKey = isAccountLevel
                ? InsightEntity.SetAccountLevelInsightRowKey(entityModel.ProgramName, false)
                : InsightEntity.SetServiceLevelInsightRowKey(entityModel.ProgramName, entityModel.ServiceContractId,
                    false);

            await TableRepository.InsertOrMergeSingleAsync(Constants.TableNames.Insight, entityModel);

            //insert History Record
            var date = insightModel.AsOfEvaluationDate;
            entityModel.RowKey = isAccountLevel
                ? InsightEntity.SetAccountLevelInsightRowKey(entityModel.ProgramName, true, date)
                : InsightEntity.SetServiceLevelInsightRowKey(entityModel.ProgramName, entityModel.ServiceContractId,
                    true, date);

            await TableRepository.InsertOrMergeSingleAsync(Constants.TableNames.Insight, entityModel);
        }

        private bool GetServiceLevelTieredThreshold(List<TierBoundary> tierBoundaries, CalculationModel calculation, bool isApproaching, out TierBoundary tieredBountryItem, double? defaultValue)
        {
            tieredBountryItem = isApproaching ? tierBoundaries.OrderBy(p => p.Threshold).FirstOrDefault(p => p.Threshold >= calculation.Usage) : tierBoundaries.OrderByDescending(p => p.Threshold).FirstOrDefault(p => p.Threshold < calculation.Usage);

            if (tieredBountryItem == null) return false;
            if (isApproaching)
            {
                if (tieredBountryItem.Threshold <= tieredBountryItem.Threshold * (defaultValue / 100) + calculation.Usage)
                    return true;
            }
            else if (tieredBountryItem.Threshold < calculation.Usage)
                return true;

            return false;
        }

        public async Task<InsightModel> GetServiceLevelEvaluationsAsync(int clientId, string customerId,string accountId, string programName, string serviceContractId)
        {
            var serviceLevelInsights = await TableRepository.GetSingleAsync<InsightEntity>(Constants.TableNames.Insight,
                InsightEntity.SetInsightPartitionKey(clientId, accountId), InsightEntity.SetServiceLevelInsightRowKey(programName, serviceContractId, false));
            return Mapper.Map<InsightEntity, InsightModel>(serviceLevelInsights);
        }

        public async Task<InsightModel> GetAccountLevelEvaluationsAsync(int clientId, string customerId, string accountId, string programName)
        {
            var accountLevelInsights = await TableRepository.GetSingleAsync<InsightEntity>(Constants.TableNames.Insight,
                InsightEntity.SetInsightPartitionKey(clientId, accountId),
                InsightEntity.SetAccountLevelInsightRowKey(programName, false));
            return Mapper.Map<InsightEntity, InsightModel>(accountLevelInsights);
        }

        public IEnumerable<InsightModel> GetDayInsights(int clientId, string customerId, string accountId, DateTime asOfDate)
        {
            string partitionkey = $"{clientId}_{accountId}";
            var datefilter =
                TableQuery.CombineFilters(
                TableQuery.GenerateFilterCondition("RowKey", QueryComparisons.GreaterThanOrEqual, asOfDate.Date.ToString("yyyy-MM-dd")),
                TableOperators.And,
                TableQuery.GenerateFilterCondition("RowKey", QueryComparisons.LessThan, asOfDate.Date.AddDays(1).ToString("yyyy-MM-dd")));

            var filter = TableQuery.CombineFilters(
                TableQuery.GenerateFilterCondition("PartitionKey", QueryComparisons.Equal, partitionkey),
                TableOperators.And,
                datefilter);

            IEnumerable<InsightEntity> insightEntityList = TableRepository.GetAllAsync<InsightEntity>(Constants.TableNames.Insight, filter);
            IEnumerable<InsightModel> insightModelList = Mapper.Map<IEnumerable<InsightEntity>, IEnumerable<InsightModel>>(insightEntityList);

            return insightModelList;
        }
    }
}
