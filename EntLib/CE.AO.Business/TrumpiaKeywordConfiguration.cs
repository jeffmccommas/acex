﻿using System.Linq;
using AO.BusinessContracts;
using AutoMapper;
using CE.AO.DataAccess;
using CE.AO.Entities;
using CE.AO.Entities.TableStorageEntities;
using CE.AO.Models;
using Microsoft.WindowsAzure.Storage.Table;

namespace CE.AO.Business
{
    public class TrumpiaKeywordConfiguration : ITrumpiaKeywordConfiguration
    {
        public TrumpiaKeywordConfiguration(LogModel logModel, ITableRepository tableRepository)
        {
            LogModel = logModel;
            TableRepository = tableRepository;
        }

        public LogModel LogModel { get; set; }

        public ITableRepository TableRepository { get; set; }

        public TrumpiaKeywordConfigurationModel GetTrumpiaKeywordConfiguration(string trumpiaKeyword)
        {
            var partitionKey = TrumpiaKeywordConfigurationEntity.SetPartitionKey(trumpiaKeyword);
            var filter = TableQuery.GenerateFilterCondition("PartitionKey", QueryComparisons.Equal,
                partitionKey);

            var trumpiaKeywordConfiguration =
                TableRepository
                    .GetAllAsync<TrumpiaKeywordConfigurationEntity>(Constants.TableNames.TrumpiaKeywordConfiguration, filter).FirstOrDefault();
            return Mapper.Map<TrumpiaKeywordConfigurationEntity, TrumpiaKeywordConfigurationModel>(trumpiaKeywordConfiguration);
        }
    }
}
