﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WindowsAzure.Table.EntityConverters;
using AO.BusinessContracts;
using AutoMapper;
using CE.AO.DataAccess;
using CE.AO.Entities.TableStorageEntities;
using CE.AO.Entities;
using CE.AO.Models;
using Microsoft.Practices.Unity;
using Microsoft.WindowsAzure.Storage.Table;
using Enums = CE.AO.Utilities.Enums;

namespace CE.AO.Business
{
    // ReSharper disable once InconsistentNaming
    public class AMI : IAmi
    {
        public AMI(LogModel logModel, ITableRepository tableRepository)
        {
            LogModel = logModel;
            TableRepository = tableRepository;
        }

        public LogModel LogModel { get; set; }

        [Dependency]
        public ITableRepository TableRepository { get; set; }

        public async Task<bool> InsertAmiAsync(Ami15MinuteIntervalModel model)
        {
            var entityModel = Mapper.Map<Ami15MinuteIntervalModel, Ami15MinuteIntervalEntity>(model);
            entityModel.PartitionKey = Ami15MinuteIntervalEntity.SetPartitionKey(model.ClientId, model.MeterId);
            entityModel.RowKey = Ami15MinuteIntervalEntity.SetRowKey(model.AmiTimeStamp);
            return await TableRepository.InsertAsync(entityModel, Constants.TableNames.Ami);
        }

        public async Task<bool> InsertAmiAsync(Ami30MinuteIntervalModel model)
        {
            var entityModel = Mapper.Map<Ami30MinuteIntervalModel, Ami30MinuteIntervalEntity>(model);
            entityModel.PartitionKey = Ami30MinuteIntervalEntity.SetPartitionKey(model.ClientId, model.MeterId);
            entityModel.RowKey = Ami30MinuteIntervalEntity.SetRowKey(model.AmiTimeStamp);
            return await TableRepository.InsertAsync(entityModel, Constants.TableNames.Ami);
        }

        public async Task<bool> InsertAmiAsync(Ami60MinuteIntervalModel model)
        {
            var entityModel = Mapper.Map<Ami60MinuteIntervalModel, Ami60MinuteIntervalEntity>(model);
            entityModel.PartitionKey = Ami60MinuteIntervalEntity.SetPartitionKey(model.ClientId, model.MeterId);
            entityModel.RowKey = Ami60MinuteIntervalEntity.SetRowKey(model.AmiTimeStamp);
            return await TableRepository.InsertAsync(entityModel, Constants.TableNames.Ami);
        }

        public async Task<bool> InsertAmiAsync(AmiDailyIntervalModel model)
        {
            var entityModel = Mapper.Map<AmiDailyIntervalModel, AmiDailyIntervalEntity>(model);
            entityModel.PartitionKey = AmiDailyIntervalEntity.SetPartitionKey(model.ClientId, model.MeterId);
            entityModel.RowKey = AmiDailyIntervalEntity.SetRowKey(model.AmiTimeStamp);
            return await TableRepository.InsertAsync(entityModel, Constants.TableNames.Ami);
        }

        public async Task<bool> InsertOrMergeAmiAsync<T>(T model, Enums.IntervalType interval) where T : new()
        {
            switch (interval)
            {
                case Enums.IntervalType.None:
                    return false;
                case Enums.IntervalType.Fifteen:
                    var entityModel15Min = Mapper.Map<T, Ami15MinuteIntervalEntity>(model);
                    entityModel15Min.PartitionKey = Ami15MinuteIntervalEntity.SetPartitionKey(entityModel15Min.ClientId, entityModel15Min.MeterId);
                    entityModel15Min.RowKey = Ami15MinuteIntervalEntity.SetRowKey(entityModel15Min.AmiTimeStamp);
                    return await TableRepository.InsertOrMergeSingleAsync(Constants.TableNames.Ami, entityModel15Min);
                case Enums.IntervalType.Thirty:
                    var entityModel30Min = Mapper.Map<T, Ami30MinuteIntervalEntity>(model);
                    entityModel30Min.PartitionKey = Ami30MinuteIntervalEntity.SetPartitionKey(entityModel30Min.ClientId, entityModel30Min.MeterId);
                    entityModel30Min.RowKey = Ami30MinuteIntervalEntity.SetRowKey(entityModel30Min.AmiTimeStamp);
                    return await TableRepository.InsertOrMergeSingleAsync(Constants.TableNames.Ami, entityModel30Min);
                case Enums.IntervalType.Sixty:
                    var entityModel60Min = Mapper.Map<T, Ami60MinuteIntervalEntity>(model);
                    entityModel60Min.PartitionKey = Ami60MinuteIntervalEntity.SetPartitionKey(entityModel60Min.ClientId, entityModel60Min.MeterId);
                    entityModel60Min.RowKey = Ami60MinuteIntervalEntity.SetRowKey(entityModel60Min.AmiTimeStamp);
                    return await TableRepository.InsertOrMergeSingleAsync(Constants.TableNames.Ami, entityModel60Min);
                case Enums.IntervalType.Daily:
                    var entityModelDaily = Mapper.Map<T, AmiDailyIntervalEntity>(model);
                    entityModelDaily.PartitionKey = AmiDailyIntervalEntity.SetPartitionKey(entityModelDaily.ClientId,entityModelDaily.MeterId);
                    entityModelDaily.RowKey = AmiDailyIntervalEntity.SetRowKey(entityModelDaily.AmiTimeStamp);
                    return await TableRepository.InsertOrMergeSingleAsync(Constants.TableNames.Ami, entityModelDaily);
            }
            return false;
        }

        public IEnumerable<dynamic> GetAmiList(DateTime startDate, DateTime endDate, string meterId, int clientId)
        {
            var filter = TableQuery.CombineFilters(
                TableQuery.GenerateFilterCondition("PartitionKey", QueryComparisons.Equal, $"{clientId}_{meterId.ToLower()}"),
                TableOperators.And,
                TableQuery.CombineFilters(
                    TableQuery.GenerateFilterCondition("RowKey", QueryComparisons.GreaterThanOrEqual,
                        startDate.ToString("yyyy-MM-dd")),
                    TableOperators.And,
                    TableQuery.GenerateFilterCondition("RowKey", QueryComparisons.LessThanOrEqual,
                        endDate.ToString("yyyy-MM-dd")))
                );

            IList<Ami15MinuteIntervalEntity> amiList = TableRepository.GetAllAsync<Ami15MinuteIntervalEntity>(Constants.TableNames.Ami, filter).ToList();
            
            if (amiList.Count > 0)
            {
                var amiData = amiList.First();
                var intervalType = amiData.IntervalType;
                if (intervalType.Equals((int)Enums.IntervalType.Fifteen))
                {
                    return Mapper.Map<List<Ami15MinuteIntervalModel>>(amiList);
                }
                if (intervalType.Equals((int)Enums.IntervalType.Thirty))
                {
                    return Mapper.Map<List<Ami30MinuteIntervalModel>>(amiList);
                }
                if (intervalType.Equals((int)Enums.IntervalType.Sixty))
                {
                    return Mapper.Map<List<Ami60MinuteIntervalModel>>(amiList);
                }
                if (intervalType.Equals((int)Enums.IntervalType.Daily))
                {
                    return Mapper.Map<List<AmiDailyIntervalModel>>(amiList);
                }
            }
            return null;
        }

        /// <summary>
        /// retrieve ami data list by the requested inteval type
        /// </summary>
        /// <param name="startDate"></param>
        /// <param name="endDate"></param>
        /// <param name="meterId"></param>
        /// <param name="clientId"></param>
        /// <param name="intervalType"></param>
        /// <returns></returns>
        public IEnumerable<dynamic> GetAmiListByInterval(DateTime startDate, DateTime endDate, string meterId, int clientId, int intervalType)
        {
            var filter = TableQuery.CombineFilters(
                TableQuery.GenerateFilterCondition("PartitionKey", QueryComparisons.Equal,
                    $"{clientId}_{meterId.ToLower()}"),
                TableOperators.And,
                TableQuery.CombineFilters(
                    TableQuery.GenerateFilterCondition("RowKey", QueryComparisons.GreaterThanOrEqual, startDate.ToString("yyyy-MM-dd")),
                    TableOperators.And,
                    TableQuery.GenerateFilterCondition("RowKey", QueryComparisons.LessThanOrEqual, endDate.ToString("yyyy-MM-dd")))
                );

            var amiList = TableRepository.GetAllAsync<DynamicTableEntity>(Constants.TableNames.Ami, filter);
            var dynamicTableEntities = amiList as IList<DynamicTableEntity> ?? amiList.ToList();

            var dynamicTableEntitiesList = dynamicTableEntities.ToList();
            if (dynamicTableEntitiesList.Exists(a => a["IntervalType"].Int32Value == intervalType))
            {
                var intervalEntities = dynamicTableEntities.Where(a => a["IntervalType"].Int32Value == intervalType);
                // TBD - for 5min when we support 5 mins
                //if (intervalType.Equals((int)Enums.IntervalType.Five))
                //{
                //    return GetAmiFor5MinInterval(intervalEntities);
                //}
                if (intervalType.Equals((int)Enums.IntervalType.Fifteen))
                {
                    return GetAmiFor15MinInterval(intervalEntities);
                }
                if (intervalType.Equals((int)Enums.IntervalType.Thirty))
                {
                    return GetAmiFor30MinInterval(intervalEntities);
                }
                if (intervalType.Equals((int)Enums.IntervalType.Sixty))
                {
                    return GetAmiFor60MinInterval(intervalEntities);
                }
            }
            return null;
        }


        public async Task<dynamic> GetAmi(DateTime date, string meterId, int clientId)
        {
            var partitionKey = $"{clientId}_{meterId.ToLower()}";
            var rowkey = date.ToString("yyyy-MM-dd");
            var ami =
                await TableRepository.GetSingleAsync<Ami15MinuteIntervalEntity>(Constants.TableNames.Ami, partitionKey, rowkey);

            // TODO: Remove this dirty Fix
            if (ami == null) return null;
            var intervalType = ami.IntervalType;
            if (intervalType.Equals((int)Enums.IntervalType.Fifteen))
            {
                return Mapper.Map<Ami15MinuteIntervalModel>(ami);
            }
            if (intervalType.Equals((int)Enums.IntervalType.Thirty))
            {
                return Mapper.Map<Ami30MinuteIntervalModel>(ami);
            }
            if (intervalType.Equals((int)Enums.IntervalType.Sixty))
            {
                return Mapper.Map<Ami60MinuteIntervalModel>(ami);
            }
            return intervalType.Equals((int)Enums.IntervalType.Daily) ? Mapper.Map<AmiDailyIntervalModel>(ami) : null;
        }

        public List<RateModel.Reading> GetAmiReadings(int clientId, string meterId, DateTime startDate, DateTime endDate, string requestedResolution, out int intervalType, out int amiUomId, out int amiCommodityId)
        {
            amiUomId = 99;
            amiCommodityId = 0;
            intervalType = 0;
            List<RateModel.Reading> readings = null;

            if (requestedResolution.ToLower() == "month")
            {
                startDate = new DateTime(startDate.Year, startDate.Month, 1);
                if (endDate.Month != DateTime.Today.Month && endDate.Year != DateTime.Today.Year)
                {
                    endDate = new DateTime(endDate.AddMonths(1).Year, endDate.AddMonths(1).Month, 1).AddDays(-1);
                }
            }

            var amiList = GetAmiList(startDate, endDate, meterId, clientId);
            if (amiList != null)
            {
                var enumerable = amiList as dynamic[] ?? amiList.ToArray();
                if (enumerable.Count() > 0)
                {
                    var type = amiList.GetType();

                    if (type == typeof (List<Ami15MinuteIntervalModel>))
                    {
                        intervalType = 15;
                        var ami = (List<Ami15MinuteIntervalModel>) amiList;
                        amiUomId = ami[0].UOMId;
                        amiCommodityId = ami[0].CommodityId;
                    }
                    else if (type == typeof (List<Ami30MinuteIntervalModel>))
                    {
                        intervalType = 30;
                        var ami = (List<Ami30MinuteIntervalModel>) amiList;
                        amiUomId = ami[0].UOMId;
                        amiCommodityId = ami[0].CommodityId;
                    }
                    else if (type == typeof (List<Ami60MinuteIntervalModel>))
                    {
                        intervalType = 60;
                        var ami = (List<Ami60MinuteIntervalModel>) amiList;
                        amiUomId = ami[0].UOMId;
                        amiCommodityId = ami[0].CommodityId;
                    }
                    else if (type == typeof (List<AmiDailyIntervalModel>))
                    {
                        intervalType = 24;
                        var ami = (List<AmiDailyIntervalModel>) amiList;
                        amiUomId = ami[0].UOMId;
                        amiCommodityId = ami[0].CommodityId;
                    }

                    switch (requestedResolution.ToLower())
                    {
                        case "day":
                            readings = GetDailyReadings(enumerable, intervalType);
                            break;
                        case "month":
                            readings = GetMonthlyReadings(enumerable, intervalType);
                            break;
                        default:
                            var getReading = false;
                            if ((intervalType == 15 && requestedResolution == "fifteen") ||
                                (intervalType == 30 && requestedResolution == "thirty") ||
                                (intervalType == 60 && requestedResolution == "hour") ||
                                (requestedResolution == string.Empty))
                                getReading = true;
                            if (getReading)
                            {
                                int noOfDays;
                                readings = GetReadings(enumerable, out noOfDays);
                            }
                            break;

                    }
                }
            }

            return readings;
        }

        private IEnumerable<Ami15MinuteIntervalModel> GetAmiFor15MinInterval(IEnumerable<DynamicTableEntity> amiDataList)
        {
            var converter = new TableEntityConverter<Ami15MinuteIntervalModel>();
            return
                amiDataList.Select(
                    dynamicEntity => converter.GetEntity(dynamicEntity)).ToList();
        }

        private IEnumerable<Ami30MinuteIntervalModel> GetAmiFor30MinInterval(IEnumerable<DynamicTableEntity> amiDataList)
        {
            var converter = new TableEntityConverter<Ami30MinuteIntervalModel>();
            return
                amiDataList.Select(
                    dynamicEntity => converter.GetEntity(dynamicEntity)).ToList();
        }

        private IEnumerable<Ami60MinuteIntervalModel> GetAmiFor60MinInterval(IEnumerable<DynamicTableEntity> amiDataList)
        {
            var converter = new TableEntityConverter<Ami60MinuteIntervalModel>();
            return
                amiDataList.Select(
                    dynamicEntity => converter.GetEntity(dynamicEntity)).ToList();
        }

        private IEnumerable<AmiDailyIntervalModel> GetAmiForDailyInterval(IEnumerable<DynamicTableEntity> amiDataList)
        {
            var converter = new TableEntityConverter<AmiDailyIntervalModel>();
            return
                amiDataList.Select(
                    dynamicEntity => converter.GetEntity(dynamicEntity)).ToList();
        }

        public List<RateModel.Reading> GetReadings(IEnumerable<dynamic> amiList, out int noOfDays)
        {
            var readings = new List<RateModel.Reading>();
            noOfDays = 0;

            if (amiList == null)
            {
                return readings;
            }

            foreach (var row in amiList)
            {
                noOfDays++;
                var amiRow = row;
                //var date = amiRow.AmiTimeStamp;
                var propertyList = amiRow.GetType()
                    .GetProperties();
                //Get interval type. Needed in case of daily to get touid.
                var intervalType = Convert.ToInt32(amiRow.GetType().GetProperty("IntervalType").GetValue(amiRow));
                //.ToList();
                //.Where(e => e.Name.Contains("IntValue"));
                foreach (var property in propertyList)
                {
                    double quantity;
                    dynamic reading;
                    //If interval type is 24 then get touid then get reading using that touid 
                    if (intervalType == 24)
                    {
                        var timestamp = new DateTime(amiRow.AmiTimeStamp.Year, amiRow.AmiTimeStamp.Month,
                            amiRow.AmiTimeStamp.Day, 00, 00, 0);
                        if (!property.Name.ToString().Contains("TOU_")) continue;
                        var touId = GetTOUIDFromProperty(property);
                        var value = property.GetValue(amiRow);
                        if(value != null)
                        {
                            quantity = Convert.ToDouble(value);
                            reading = new RateModel.Reading((RateModel.Enums.TimeOfUse)touId, timestamp, quantity);
                            readings.Add(reading);
                        }
                    }
                    else
                    {
                        if (!property.Name.ToString().Contains("IntValue")) continue;
                        var hh = Convert.ToInt32(property.Name.Replace("IntValue", "").Substring(0, 2));
                        var mm = Convert.ToInt32(property.Name.Replace("IntValue", "").Substring(2, 2));
                        if (hh >= 24) continue;
                        var timestamp = new DateTime(amiRow.AmiTimeStamp.Year, amiRow.AmiTimeStamp.Month,
                            amiRow.AmiTimeStamp.Day, hh, mm, 0);
                        quantity = Convert.ToDouble(property.GetValue(amiRow));
                        reading = new RateModel.Reading(timestamp, quantity);
                        readings.Add(reading);
                    }
                }
            }
            return readings;
        }

        public List<ReadingModel> GetReadings(IEnumerable<dynamic> amiList)
        {
            var readings = new List<ReadingModel>();
            var uomId = 0;
            if (amiList == null)
            {
                return readings;
            }

            foreach (var row in amiList)
            {
                int interval = Convert.ToInt32(row.IntervalType.ToString());
                var amiRow = row;
                //var date = amiRow.AmiTimeStamp;
                var propertyList = amiRow.GetType()
                    .GetProperties();
                switch (interval)
                {
                    case 15:
                        var ami15Row = (Ami15MinuteIntervalModel)row;
                        uomId = ami15Row.UOMId;
                        break;
                    case 30:
                        var ami30Row = (Ami30MinuteIntervalModel)row;
                        uomId = ami30Row.UOMId;
                        break;
                    case 60:
                        var ami60Row = (Ami60MinuteIntervalModel)row;
                        uomId = ami60Row.UOMId;
                        break;
                    case 24:
                        var ami24Row = (AmiDailyIntervalModel) row;
                        uomId = ami24Row.UOMId;
                        break;
                }
                foreach (var property in propertyList)
                {
                    double quantity;
                    ReadingModel reading;
                    //If interval type is 24 then get touid then get reading using that touid 
                    if (interval == 24)
                    {
                        var timestamp = new DateTime(amiRow.AmiTimeStamp.Year, amiRow.AmiTimeStamp.Month,
                            amiRow.AmiTimeStamp.Day, 00, 00, 0);
                        if (!property.Name.ToString().Contains("TOU_")) continue;
                        var touId = GetTOUIDFromProperty(property);
                        var value = property.GetValue(amiRow);
                        if (value != null)
                        {
                            quantity = Convert.ToDouble(value);
                            reading = new ReadingModel((RateModel.Enums.TimeOfUse)touId, timestamp, quantity);
                            reading.TimestampString = reading.Timestamp.ToString("o");
                            reading.UOMId = uomId;
                            readings.Add(reading);
                        }
                    }
                    else
                    {
                        if (!property.Name.ToString().Contains("IntValue")) continue;
                        var hh = Convert.ToInt32(property.Name.Replace("IntValue", "").Substring(0, 2));
                        var mm = Convert.ToInt32(property.Name.Replace("IntValue", "").Substring(2, 2));
                        if (hh >= 24) continue;
                        var timestamp = new DateTime(amiRow.AmiTimeStamp.Year, amiRow.AmiTimeStamp.Month,
                            amiRow.AmiTimeStamp.Day, hh, mm, 0);
                        quantity = Convert.ToDouble(property.GetValue(amiRow));
                        reading = new ReadingModel(timestamp, quantity);
                        reading.TimestampString = reading.Timestamp.ToString("o");
                        reading.UOMId = uomId;
                        readings.Add(reading);
                    }
                }
            }
            return readings;
        }

        public List<RateModel.Reading> GetDailyReadings(IEnumerable<dynamic> amiList, int intervalType)
        {
            List<RateModel.Reading> readings;
            // check if the ami is daily reading model
            // if it is, call GetReadings to convert it to daily ratemodel reading instead of summing all the interval readings up into daily
            var enumerable = amiList as dynamic[] ?? amiList.ToArray();
            if (intervalType == 24)
            {
                int noOfDays;
                readings = GetReadings(enumerable, out noOfDays);
            }
            else
            {
                readings = new List<RateModel.Reading>();
                foreach (var row in enumerable)
                {
                    var amiRow = row;
                    var date = amiRow.AmiTimeStamp;
                    var propertyList = amiRow.GetType()
                        .GetProperties();

                    var quantity = (double)0;
                    foreach (var property in propertyList)
                    {
                        if (!property.Name.ToString().Contains("IntValue")) continue;
                        var value = property.GetValue(amiRow);
                        if (value != null)
                        {
                            quantity = (double)property.GetValue(amiRow) + quantity;
                        }
                    }
                    var reading = new RateModel.Reading(date, quantity);
                    readings.Add(reading);
                }
            }
           
            return readings;
        }

        public List<RateModel.Reading> GetMonthlyReadings(IEnumerable<dynamic> amiList, int intervalType)
        {
            var readings = new List<RateModel.Reading>();
            var timeStamp = DateTime.MinValue;
            var noTou = (double)0;
            var onPeak = (double)0;
            var offPeak = (double)0;
            var criticalPeak = (double)0;
            var shoulder1 = (double)0;
            var shoulder2 = (double)0;
            bool hasOnPeak = false, hasOffPeak = false, hasCriticalPeak = false, hasShoulder1 = false, hasShoulder2 = false;

            foreach (var row in amiList)
            {
                var amiRow = row;
                //var date = amiRow.AmiTimeStamp;
                if (timeStamp == DateTime.MinValue)
                {
                    timeStamp = new DateTime(amiRow.AmiTimeStamp.Year, amiRow.AmiTimeStamp.Month, 1);
                    noTou = 0;
                    onPeak = 0;
                    offPeak = 0;
                    criticalPeak = 0;
                    shoulder1 = 0;
                    shoulder2 = 0;
                }
                if (amiRow.AmiTimeStamp.Month != timeStamp.Month)
                {
                    var reading = new RateModel.Reading(timeStamp, noTou);
                    readings.Add(reading);
                    // add monthly tou readings if exists
                    if (hasOnPeak)
                    {
                        reading = new RateModel.Reading(timeStamp, onPeak);
                        reading.TimeOfUse = RateModel.Enums.TimeOfUse.OnPeak;
                        readings.Add(reading);
                    }
                    if (hasOffPeak)
                    {
                        reading = new RateModel.Reading(timeStamp, offPeak);
                        reading.TimeOfUse = RateModel.Enums.TimeOfUse.OffPeak;
                        readings.Add(reading);
                    }
                    if (hasShoulder1)
                    {
                        reading = new RateModel.Reading(timeStamp, shoulder1);
                        reading.TimeOfUse = RateModel.Enums.TimeOfUse.Shoulder1;
                        readings.Add(reading);
                    }
                    if (hasShoulder2)
                    {
                        reading = new RateModel.Reading(timeStamp, shoulder2);
                        reading.TimeOfUse = RateModel.Enums.TimeOfUse.Shoulder2;
                        readings.Add(reading);
                    }
                    if (hasCriticalPeak)
                    {
                        reading = new RateModel.Reading(timeStamp, criticalPeak);
                        reading.TimeOfUse = RateModel.Enums.TimeOfUse.CriticalPeak;
                        readings.Add(reading);
                    }

                    timeStamp = new DateTime(amiRow.AmiTimeStamp.Year, amiRow.AmiTimeStamp.Month, 1);
                    noTou = 0;
                    onPeak = 0;
                    offPeak = 0;
                    criticalPeak = 0;
                    shoulder1 = 0;
                    shoulder2 = 0;
                }
                var propertyList = amiRow.GetType()
                    .GetProperties();

                foreach (var property in propertyList)
                {

                    if (intervalType == 24)
                    {
                        if (!property.Name.ToString().Contains("TOU_")) continue;
                        var touId = GetTOUIDFromProperty(property);
                        var value = property.GetValue(amiRow);
                        if (value != null)
                        {
                            var quantity = Convert.ToDouble(value);
                            switch ((int)touId)
                            {
                                case 0:
                                    noTou = noTou + quantity;
                                    break;
                                case 1:
                                    onPeak = onPeak + quantity;
                                    hasOnPeak = true;
                                    break;
                                case 2:
                                    offPeak = offPeak + quantity;
                                    hasOffPeak = true;
                                    break;
                                case 3:
                                    shoulder1 = shoulder1 + quantity;
                                    hasShoulder1 = true;
                                    break;
                                case 4:
                                    shoulder2 = shoulder2 + quantity;
                                    hasShoulder2 = true;
                                    break;
                                case 5:
                                    criticalPeak = criticalPeak + quantity;
                                    hasCriticalPeak = true;
                                    break;
                            }
                        }
                    }
                    else
                    {
                        if (!property.Name.ToString().Contains("IntValue")) continue;

                        var value = property.GetValue(amiRow);
                        if (value != null)
                        {
                            noTou = (double)property.GetValue(amiRow) + noTou;
                        }
                    }
                }
            }
            // the last monthly reading
            readings.Add(new RateModel.Reading(timeStamp, noTou));

            // add monthly tou readings if exists
            if (hasOnPeak)
            {
                var reading = new RateModel.Reading(timeStamp, onPeak);
                reading.TimeOfUse = RateModel.Enums.TimeOfUse.OnPeak;
                readings.Add(reading);
            }
            if (hasOffPeak)
            {
                var reading = new RateModel.Reading(timeStamp, offPeak);
                reading.TimeOfUse = RateModel.Enums.TimeOfUse.OffPeak;
                readings.Add(reading);
            }
            if (hasShoulder1)
            {
                var reading = new RateModel.Reading(timeStamp, shoulder1);
                reading.TimeOfUse = RateModel.Enums.TimeOfUse.Shoulder1;
                readings.Add(reading);
            }
            if (hasShoulder2)
            {
                var reading = new RateModel.Reading(timeStamp, shoulder2);
                reading.TimeOfUse = RateModel.Enums.TimeOfUse.Shoulder2;
                readings.Add(reading);
            }
            if (hasCriticalPeak)
            {
                var reading = new RateModel.Reading(timeStamp, criticalPeak);
                reading.TimeOfUse = RateModel.Enums.TimeOfUse.CriticalPeak;
                readings.Add(reading);
            }
            return readings;
        }

        /// <summary>
        /// Get TouId depending on column
        /// </summary>
        /// <param name="property">property of model</param>
        /// <returns>TouId</returns>
        private int GetTOUIDFromProperty(dynamic property)
        {
            if (property.Name.ToString().Equals("TOU_Regular"))
                return 0;
            if (property.Name.ToString().Equals("TOU_OnPeak"))
                return 1;
            if (property.Name.ToString().Equals("TOU_OffPeak"))
                return 2;
            if (property.Name.ToString().Equals("TOU_Shoulder1"))
                return 3;
            if (property.Name.ToString().Equals("TOU_Shoulder2"))
                return 4;
            //if (property.Name.ToString().Equals("TOU_CriticalPeak"))
            return 5;
        }
    }
}