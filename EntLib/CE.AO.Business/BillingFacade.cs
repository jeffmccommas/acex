﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AO.BusinessContracts;
using AutoMapper;
using CE.AO.Models;
using CE.AO.Entities.TableStorageEntities;
using Microsoft.Practices.ObjectBuilder2;
using Microsoft.Practices.Unity;
using Enums = CE.AO.Utilities.Enums;

namespace CE.AO.Business
{
    public class BillingFacade : IBillingFacade
    {
        public BillingFacade(LogModel logModel)
        {
            LogModel = logModel;
        }

        public LogModel LogModel { get; set; }

        [Dependency]
        public ICustomer CustomerManager { get; set; }

        [Dependency]
        public IClientAccount ClientAccount { get; set; }

        [Dependency]
        public IBilling BillingManager { get; set; }

        [Dependency]
        public IPremise PremiseManager { get; set; }

        [Dependency]
        public ICustomerMobileNumberLookup CustomerMobileNumberLookupManager { get; set; }

        [Dependency]
        public IAccountLookup AccountLookupManager { get; set; }

        public async Task<bool> InsertOrMergeBillingAsync(BillingCustomerPremiseModel model)
        {
            if (string.IsNullOrEmpty(model.email) || string.IsNullOrEmpty(model.phone_1))
            {
                // get existing customer info for the email/phone number, so it won't overwrite it with empty string
                var existingCustomerModel = await CustomerManager.GetCustomerAsync(model.ClientId, model.customer_id);
                if (existingCustomerModel != null)
                {
                    model.phone_1 = existingCustomerModel.Phone1;
                    model.phone_2 = existingCustomerModel.Phone2;
                    model.email = existingCustomerModel.EmailAddress;
                }

            }
            var customerModel = Mapper.Map<BillingCustomerPremiseModel, CustomerModel>(model);
            var billingModel = Mapper.Map<BillingCustomerPremiseModel, BillingModel>(model);
            var premiseModel = Mapper.Map<BillingCustomerPremiseModel, PremiseModel>(model);
            var accountLookUpModel = Mapper.Map<BillingCustomerPremiseModel, AccountLookupModel>(model);
            var clientAccountModel = Mapper.Map<BillingCustomerPremiseModel, ClientAccountModel>(model);
            var customerMobileNumberLookupModel = Mapper.Map<BillingCustomerPremiseModel, CustomerMobileNumberLookupModel>(model);

            var tasks = new List<Task<bool>>
            {
                InsertForCustomer(customerModel),
                InsertForBilling(billingModel),
                InsertForPremise(premiseModel),
                InsertForAccountLookup(accountLookUpModel,model.premise_id,model.service_commodity,model.meter_replaces_meterid,model.meter_type),
                InsertForClientAccount(clientAccountModel),
                InsertForcustomerMobileNumberLookup(customerMobileNumberLookupModel)
            };

            var taskResults = await Task.WhenAll(tasks);

            return !taskResults.Any(result => false);
        }

        private async Task<bool> InsertForcustomerMobileNumberLookup(CustomerMobileNumberLookupModel model)
        {
            if (!string.IsNullOrWhiteSpace(model.MobileNumber))
            {
                var mobileNumber = string.Empty;
                model.MobileNumber.ToCharArray().ForEach(c =>
                {
                    if (char.IsNumber(c))
                    {
                        mobileNumber = mobileNumber + c;
                    }
                });
                model.MobileNumber = mobileNumber;
                return await CustomerMobileNumberLookupManager.InsertOrMergeCustomerMobileNumberLookupAsync(model);
            }
            return true;
        }

        private async Task<bool> InsertForClientAccount(ClientAccountModel model)
        {
            return await ClientAccount.InsertOrMergeClientAccount(model);
        }

        private async Task<bool> InsertForCustomer(CustomerModel customerModel)
        {
            customerModel.Country = Enums.Country.US.ToString();
            customerModel.Source = Enums.Source.Utility.ToString();
            customerModel.IsBusiness = customerModel.CustomerType.Equals("Commercial");

            return await CustomerManager.InsertOrMergeCustomerAsync(customerModel);

        }

        private async Task<bool> InsertForBilling(BillingModel billingModel)
        {
            var tasks = new List<Task<bool>>();

            if (string.IsNullOrWhiteSpace(billingModel.ServiceContractId))
                billingModel.ServiceContractId = BillingEntity.GetServiceContractId(billingModel.MeterId,
                    billingModel.AccountId, billingModel.PremiseId, billingModel.CommodityId);

            billingModel.StartDate = billingModel.EndDate.AddDays(-1 * Convert.ToInt32(billingModel.BillDays));
            billingModel.Source = Enums.Source.Utility.ToString();
            billingModel.ReadQuality = billingModel.ReadQuality.Equals("0") ? "Actual" : "Estimated";

            var latestBill = await BillingManager.GetServiceDetailsFromBillAsync(billingModel.ClientId, billingModel.CustomerId,
                billingModel.AccountId, billingModel.ServiceContractId);

            if (latestBill == null || latestBill.EndDate <= billingModel.EndDate)
                tasks.Add(BillingManager.InsertOrMergeLatestBillAsync(billingModel));

            tasks.Add(BillingManager.InsertOrMergeHistoryBillAsync(billingModel));

            var taskResults = await Task.WhenAll(tasks);

            return !taskResults.Any(result => false);
        }

        private async Task<bool> InsertForPremise(PremiseModel premiseModel)
        {
            premiseModel.Country = Enums.Country.US.ToString();
            return
                await
                    PremiseManager.InsertOrMergePremiseAsync(premiseModel);
        }

        private async Task<bool> InsertForAccountLookup(AccountLookupModel accountLookupModel, string premiseId, int commodityId, string replacedMeterId, string meterType)
        {
            bool isSuccess;

            // do not update AccountMeterLookup when the commodity is sewer (4)
            if (commodityId == 4)
            {
                return (true);
            }

            if (!string.IsNullOrWhiteSpace(replacedMeterId))
            {

                var data = await AccountLookupManager.GetAccountMeterDetails(accountLookupModel.ClientId, replacedMeterId, accountLookupModel.CustomerId);
                if (data != null)
                {
                    data.IsActive = false;
                    await AccountLookupManager.InsertOrMergeAccountLookupAsync(data);

                    if (data.IsServiceContractCreatedBySystem)
                        accountLookupModel.ServiceContractId = BillingEntity.GetServiceContractId(accountLookupModel.MeterId,
                            accountLookupModel.AccountId, premiseId, commodityId);
                    else
                        accountLookupModel.ServiceContractId = data.ServiceContractId;

                    isSuccess = await AccountLookupManager.InsertOrMergeAccountLookupAsync(accountLookupModel);
                    return isSuccess;
                }
            }

            accountLookupModel.IsServiceContractCreatedBySystem =
                string.IsNullOrWhiteSpace(accountLookupModel.ServiceContractId);

            if (string.IsNullOrWhiteSpace(accountLookupModel.ServiceContractId))
                accountLookupModel.ServiceContractId = BillingEntity.GetServiceContractId(accountLookupModel.MeterId, accountLookupModel.AccountId,
                premiseId, commodityId);

            if (meterType.ToLower() != Convert.ToString(Enums.MeterType.nonmetered).ToLower())
            {
                isSuccess = await AccountLookupManager.InsertOrMergeAccountLookupAsync(accountLookupModel);
                return isSuccess;
            }
            return true;
        }
    }
}
