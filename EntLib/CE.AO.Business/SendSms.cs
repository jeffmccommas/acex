﻿using CE.AO.DataAccess;
using CE.AO.Entities;
using CE.AO.Entities.TableStorageEntities;
using CE.AO.Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using AO.BusinessContracts;

namespace CE.AO.Business
{
    public class SendSms : ISendSms
    {
        private const string AlreadyRegisterNumber = "MPSE2401";
        public async Task<string> Send(string apiKey, string username, string smsBody, string phone, string subscriptionId, string customKey, int clientId, string accountId, string programName, string serviceContractId, string insightTypeName, DateTime requestDate, DateTime? notifyDateTime = null)
        {
            var requestId = string.Empty;
            using (var client = new HttpClient())
            {

                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Add("X-Apikey", apiKey);

                var smsData = new JObject { { "message", smsBody } };

                JObject recipientData = new JObject { { "type", "subscription" }, { "value", subscriptionId } };
               
                var info = new SmsMessageInfo { description = customKey, sms = smsData, recipients = recipientData };

                if (notifyDateTime != null)
                    info.send_date = ((DateTime)notifyDateTime).ToString("yyyy-MM-dd HH:mm:ss");

                var postUrl = new Uri($"http://api.Aclara.MessagingChannel.com/rest/v1/{username}/message");
                var response = await client.PutAsJsonAsync(postUrl, info);
                if (response.IsSuccessStatusCode)
                {
                    var messageResultModel = response.Content.ReadAsAsync<MessageResultModel>().Result;

                    var trumpiaRequestDetailEntity = new TrumpiaRequestDetailEntity
                    {
                        PartitionKey = TrumpiaRequestDetailEntity.SetPartitionKey(messageResultModel.request_id),
                        RowKey = TrumpiaRequestDetailEntity.SetRowKey(messageResultModel.request_id),
                        RequestId = messageResultModel.request_id,
                        MessageId = messageResultModel.message_id,
                        Description = customKey,
                        SmsBody = smsBody
                    };
                    
                    var isSent = await new TableRepository().InsertOrMergeSingleAsync(Constants.TableNames.TrumpiaRequestDetail, trumpiaRequestDetailEntity);

                    if (isSent)
                        requestId = messageResultModel.request_id;
                }
            }
            return requestId;
        }

        //Create new trumpia subscription
        public string CreateNewTrumpiaSubscription(CustomerModel customerModel, string apiKey, string username, string contactList)
        {
            var subscriptionResultModel = new SubscriptionResultModel();
            using (var client = new HttpClient())
            {
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Add("X-Apikey", $"{apiKey}");
                
                var firstName = customerModel.FirstName;
                var lastName = customerModel.LastName;
                if (firstName.Length > 30)
                    firstName = firstName.Substring(0, 30);

                if (lastName.Length > 30)
                    lastName = lastName.Substring(0, 30);

                //PUT
                JObject subscription = new JObject
                {
                    {"first_name", firstName},
                    {"last_name", lastName}
                };

                JObject mobile = new JObject { { "number", customerModel.Phone1 }, { "country_code", "+1" } };
                //For US
                subscription.Add("mobile", mobile);

                JArray array = new JArray { subscription };

                var info = new SubscriptionInfoModel { list_name = $"{contactList}", subscriptions = array };
                Uri postUrl = new Uri($"http://api.Aclara.MessagingChannel.com/rest/v1/{username}/subscription");
                var response = client.PutAsJsonAsync(postUrl, info).Result;
                if (response.IsSuccessStatusCode)
                {
                    subscriptionResultModel = response.Content.ReadAsAsync<SubscriptionResultModel>().Result;
                }
            }
            return subscriptionResultModel.request_id;
        }

        //Get the subcription id by request id
        public string GetSubscriptionId(CustomerModel customerModel, string requestId, string apiKey, string username)
        {
            var subscriptionId = string.Empty;
            var subscriptionResultModelList = new List<SubscriptionResultModel>();
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("http://api.Aclara.MessagingChannel.com");
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Add("X-Apikey", $"{apiKey}");
                
                //GET
                HttpResponseMessage response = client.GetAsync($"/rest/v1/{username}/report/{requestId}").Result;
                if (response.IsSuccessStatusCode)
                {
                    subscriptionResultModelList = JsonConvert.DeserializeObject<List<SubscriptionResultModel>>(response.Content.ReadAsStringAsync().Result);
                }


                if (subscriptionResultModelList.Count > 0)
                {
                    var trumpiaRequestDetailEntity = new TrumpiaRequestDetailEntity
                    {
                        PartitionKey = TrumpiaRequestDetailEntity.SetPartitionKey(requestId),
                        RowKey = TrumpiaRequestDetailEntity.SetRowKey(requestId),
                        RequestId = requestId,
                        Description = "Subscription"
                    };

                    if (subscriptionResultModelList[0].status_code == null)
                        subscriptionId = Convert.ToString(subscriptionResultModelList[0].subscription_id);
                    else if (subscriptionResultModelList[0].status_code == AlreadyRegisterNumber)
                    {
                        // if number already registered, get the existing subscription id
                        response = client.GetAsync($"/rest/v1/{username}/subscription/search?search_type=2&search_data={customerModel.Phone1}").Result;
                        if (response.IsSuccessStatusCode)
                        {
                            var subscriptionSearchResultModel =
                                JsonConvert.DeserializeObject<SubscriptionSearchResultModel>(
                                    response.Content.ReadAsStringAsync().Result);
                            if (subscriptionSearchResultModel.status_code == null)
                            {
                                if (subscriptionSearchResultModel.subscription_id_list.Count > 0)
                                    subscriptionId = subscriptionSearchResultModel.subscription_id_list[0];
                            }
                            else
                            {
                                trumpiaRequestDetailEntity.ErrorMessage =
                                    subscriptionSearchResultModel.error_message;
                                trumpiaRequestDetailEntity.StatusCode = subscriptionSearchResultModel.status_code;
                                subscriptionId = string.Empty;
                            }

                        }
                        else
                        {
                            trumpiaRequestDetailEntity.ErrorMessage = response.StatusCode.ToString();
                            trumpiaRequestDetailEntity.StatusCode = response.StatusCode.ToString();
                            subscriptionId = string.Empty;
                        }
                    }
                    else
                    {
                        trumpiaRequestDetailEntity.ErrorMessage = subscriptionResultModelList[0].error_message;
                        trumpiaRequestDetailEntity.StatusCode = subscriptionResultModelList[0].status_code;
                        subscriptionId = string.Empty;
                    }


                    var trumpiaTask = Task.Run(() => new TableRepository().InsertOrMergeSingleAsync(Constants.TableNames.TrumpiaRequestDetail, trumpiaRequestDetailEntity));
                    Task.WhenAll(trumpiaTask);
                }
            }
            return subscriptionId;
        }


        public async Task<SmsStatsModel> GetSmsStatics(string requestId, string apiKey, string username)
        {
            var smsStatsModel = new SmsStatsModel();
            try
            {
                using (var client = new HttpClient())
                {

                    client.BaseAddress = new Uri("http://api.Aclara.MessagingChannel.com");
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                    client.DefaultRequestHeaders.Add("X-Apikey", $"{apiKey}");

                    //GET
                    HttpResponseMessage response = await client.GetAsync($"/rest/v1/{username}/report/{requestId}");
                    if (response.IsSuccessStatusCode)
                    {
                        smsStatsModel =
                            JsonConvert.DeserializeObject<SmsStatsModel>(response.Content.ReadAsStringAsync().Result);
                    }
                }
            }
            catch (Exception ex)
            {
                // do nothing, return stats model with exception message
                smsStatsModel.error_message = ex.Message;
            }
            

            return smsStatsModel;
        }


        public SubscriptionInfoResultModel GetSubscriptionInfo(string subscriptionId, string apiKey, string username)
        {
            var subscriptionInfo = new SubscriptionInfoResultModel();
            using (var client = new HttpClient())
            {

                client.BaseAddress = new Uri("http://api.Aclara.MessagingChannel.com");
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Add("X-Apikey", $"{apiKey}");

                //GET
                HttpResponseMessage response = client.GetAsync($"/rest/v1/{username}/subscription/{subscriptionId}").Result;
                if (response.IsSuccessStatusCode)
                {
                    subscriptionInfo = JsonConvert.DeserializeObject<SubscriptionInfoResultModel>(response.Content.ReadAsStringAsync().Result);
                }
            }

            return subscriptionInfo;
        }

        public string UpdateSubscriptionInfo(CustomerModel customerModel, string subscriptionId, string apiKey, string username, string contactList)
        {
            var subscriptionResultModel = new SubscriptionResultModel();
            using (var client = new HttpClient())
            {
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Add("X-Apikey", $"{apiKey}");

                var firstName = customerModel.FirstName;
                var lastName = customerModel.LastName;

                if (firstName.Length > 30)
                    firstName = firstName.Substring(0, 30);

                if (lastName.Length > 30)
                    lastName = lastName.Substring(0, 30);
                //PUT
                JObject subscription = new JObject
                {
                    {"first_name", firstName},
                    {"last_name", lastName}
                };

                JObject mobile = new JObject { { "number", customerModel.Phone1 }, { "country_code", "+1" } };
                //For US
                subscription.Add("mobile", mobile);

                JArray array = new JArray { subscription };

                var info = new SubscriptionInfoModel { list_name = $"{contactList}", subscriptions = array };
                Uri postUrl = new Uri(
                    $"http://api.Aclara.MessagingChannel.com/rest/v1/{username}/subscription/{subscriptionId}");
                var response = client.PostAsJsonAsync(postUrl, info).Result;
                if (response.IsSuccessStatusCode)
                {
                    subscriptionResultModel = response.Content.ReadAsAsync<SubscriptionResultModel>().Result;
                }
            }
            return subscriptionResultModel.request_id;
        }




    }
}
