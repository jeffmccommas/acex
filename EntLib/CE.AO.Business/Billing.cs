﻿using AutoMapper;
using CE.AO.DataAccess;
using CE.AO.Entities;
using CE.AO.Entities.TableStorageEntities;
using CE.AO.Models;
using Microsoft.WindowsAzure.Storage.Table;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AO.BusinessContracts;

namespace CE.AO.Business
{
    public class Billing : IBilling
    {
        public Billing(LogModel logModel, ITableRepository tableRepository)
        {
            LogModel = logModel;
            TableRepository = tableRepository;
        }

        public LogModel LogModel { get; set; }
       
        public ITableRepository TableRepository { get; set; }

        public IEnumerable<BillingModel> GetAllServicesFromLastBill(int clientId, string customerId, string accountId)
        {
            var filter = TableQuery.CombineFilters(
                TableQuery.GenerateFilterCondition("PartitionKey", QueryComparisons.Equal, BillingEntity.SetPartitionKey(clientId, customerId, accountId)),
                TableOperators.And,
                TableQuery.CombineFilters(
                    TableQuery.GenerateFilterCondition("RowKey", QueryComparisons.GreaterThanOrEqual, "B"),
                    TableOperators.And,
                    TableQuery.GenerateFilterCondition("RowKey", QueryComparisons.LessThan, "C"))
                );

            var services =
                TableRepository.GetAllAsync<BillingEntity>(Constants.TableNames.Billing, filter);

            return Mapper.Map<IEnumerable<BillingEntity>, IEnumerable<BillingModel>>(services);
        }

        public IEnumerable<BillingModel> GetBillsForService(int clientId, string customerId, string accountId,
            string serviceContractId, DateTime startDate)
        {
            var filter = TableQuery.CombineFilters(
                TableQuery.GenerateFilterCondition("PartitionKey", QueryComparisons.Equal, BillingEntity.SetPartitionKey(clientId, customerId, accountId)),
                TableOperators.And,
                TableQuery.CombineFilters(
                    TableQuery.GenerateFilterCondition("RowKey", QueryComparisons.Equal, BillingEntity.SetLatestRowKey(serviceContractId)),
                    TableOperators.Or,
                    TableQuery.GenerateFilterCondition("RowKey", QueryComparisons.GreaterThanOrEqual, BillingEntity.SetHistoryRowKey(serviceContractId, startDate)))
                    );

            var services =
                 TableRepository.GetAllAsync<BillingEntity>(Constants.TableNames.Billing, filter);

            var bills = services.ToList();
            bills.RemoveAll(s => s.ServiceContractId != serviceContractId);

            return Mapper.Map<IEnumerable<BillingEntity>, IEnumerable<BillingModel>>(bills);

        }


        public IEnumerable<BillingModel> GetBillsForService(int clientId, string customerId, string accountId,
            string serviceContractId, DateTime startDate, DateTime endDate)
        {
            var filter = TableQuery.CombineFilters(
                TableQuery.GenerateFilterCondition("PartitionKey", QueryComparisons.Equal, BillingEntity.SetPartitionKey(clientId, customerId, accountId)),
                TableOperators.And,
                TableQuery.CombineFilters(
                    TableQuery.GenerateFilterCondition("RowKey", QueryComparisons.LessThanOrEqual, BillingEntity.SetHistoryRowKey(serviceContractId, endDate)),
                    TableOperators.And,
                    TableQuery.GenerateFilterCondition("RowKey", QueryComparisons.GreaterThanOrEqual, BillingEntity.SetHistoryRowKey(serviceContractId, startDate)))
                );

            var services =
                TableRepository.GetAllAsync<BillingEntity>(Constants.TableNames.Billing, filter);

            var bills = services.ToList();
            bills.RemoveAll(s => s.ServiceContractId != serviceContractId);

            return Mapper.Map<IEnumerable<BillingEntity>, IEnumerable<BillingModel>>(bills);

        }

        public async Task<BillingModel> GetServiceDetailsFromBillAsync(int clientId, string customerId, string accountId,
            string serviceContractId)
        {
            var partitionKey = BillingEntity.SetPartitionKey(clientId, customerId, accountId);
            var rowkey = BillingEntity.SetLatestRowKey(serviceContractId);
            var serviceDetails =
                await TableRepository.GetSingleAsync<BillingEntity>(Constants.TableNames.Billing, partitionKey, rowkey);
            return Mapper.Map<BillingEntity, BillingModel>(serviceDetails);
        }

        public async Task<bool> InsertOrMergeLatestBillAsync(BillingModel model)
        {
            var entity = Mapper.Map<BillingModel, BillingEntity>(model);
            entity.PartitionKey = BillingEntity.SetPartitionKey(model.ClientId, model.CustomerId, model.AccountId);
            entity.RowKey = BillingEntity.SetLatestRowKey(model.ServiceContractId);
            return await TableRepository.InsertOrMergeSingleAsync(Constants.TableNames.Billing, entity);
        }

        public async Task<bool> InsertOrMergeHistoryBillAsync(BillingModel model)
        {
            var entity = Mapper.Map<BillingModel, BillingEntity>(model);
            entity.PartitionKey = BillingEntity.SetPartitionKey(model.ClientId, model.CustomerId, model.AccountId);
            entity.RowKey = BillingEntity.SetHistoryRowKey(model.ServiceContractId, model.EndDate);
            return await TableRepository.InsertOrMergeSingleAsync(Constants.TableNames.Billing, entity);
        }

        public async Task<bool> DeleteLatestServiceDetailsAsync(int clientId, string customerId, string accountId,
             string serviceContractId)
        {
            var partitionKey = BillingEntity.SetPartitionKey(clientId, customerId, accountId);
            var rowKey = BillingEntity.SetLatestRowKey(serviceContractId);
            return await TableRepository.DeleteSingleAsync<BillingEntity>(Constants.TableNames.Billing, partitionKey, rowKey);
        }

        public async Task<bool> DeleteHistoryServiceDetailsAsync(int clientId, string customerId, string accountId,
            string serviceContractId, DateTime endDate)
        {
            var partitionKey = BillingEntity.SetPartitionKey(clientId, customerId, accountId);
            var rowKey = BillingEntity.SetHistoryRowKey(serviceContractId, endDate);
            return await TableRepository.DeleteSingleAsync<BillingEntity>(Constants.TableNames.Billing, partitionKey, rowKey);
        }
    }
}
