﻿using CE.AO.DataAccess;
using CE.AO.Entities;
using CE.AO.Logging;
using CE.AO.Models;
using CE.BillToDate;
using CE.RateModel;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AO.BusinessContracts;
using Enums = CE.AO.Utilities.Enums;

namespace CE.AO.Business
{
    public class CalculationFacade : ICalculationFacade
    {
        public CalculationFacade()
        {
            LogModel = new LogModel { DisableLog = true };
        }

        public CalculationFacade(LogModel logModel)
        {
            LogModel = logModel;
        }

        public LogModel LogModel { get; set; }

        [Dependency]
        public ICalculation CalculationManager { get; set; }

        [Dependency]
        public IBilling BillingManager { get; set; }

        [Dependency]
        public IAmi AmiManager { get; set; }


        [Dependency]
        public IBillingCycleSchedule BillingCycleScheduleManager { get; set; }

        [Dependency]
        public IAccountLookup AccountLookupManager { get; set; }

        [Dependency]
        public IAccountUpdates AccountManager { get; set; }

        [Dependency]
        public IQueueRepository QueueRepository { get; set; }

        [Dependency]
        public IClientAccount ClientAccount { get; set; }

        [Dependency]
        public Lazy<IClientConfigFacade> ClientConfigFacadeManager { get; set; }

        [Dependency]
        public Lazy<ICalculationManagerFactory> CalculationManagerFactory { get; set; }

        public async Task CalculateCostToDate(int clientId, string meterId, DateTime asOfDate)
        {
            LogModel.Module = Enums.Module.CostToDateCalculation;
            var accountMeterDetails = AccountLookupManager.GetAccountMeterDetails(clientId, meterId);
            List<BillingModel> billingDetails = null;
            foreach (var accountMeterDetail in accountMeterDetails)
            {
                billingDetails = accountMeterDetail != null
                    ? BillingManager.GetAllServicesFromLastBill(clientId, accountMeterDetail.CustomerId,
                        accountMeterDetail.AccountId).ToList()
                    : null;
            }

            if (accountMeterDetails != null && accountMeterDetails.Any())
            {
                LogModel.CustomerId = accountMeterDetails[0].CustomerId;
                LogModel.AccountId = accountMeterDetails[0].AccountId;
                LogModel.ServiceContractId = accountMeterDetails[0].ServiceContractId;
            }

            if (billingDetails == null || billingDetails.Count == 0)
            {
                Logger.Warn("Billing information is missing.", LogModel);
                return;
            }

            // Find matching meter from last bill, there will be more than one match when a sewer service contract is in-play for a water meter
            var matchingMeters = billingDetails.Where(e => e.MeterId == meterId && e.MeterType.Equals(Enums.MeterType.ami.ToString(), StringComparison.InvariantCultureIgnoreCase));

            foreach (var meterDetails in matchingMeters)
            {
                //// READ BILL-TO-DATE SETTINGS
                var btdSettings = ClientConfigFacadeManager.Value.GetBillToDateSettings(clientId);
                var rateCompanyId = btdSettings.RateCompanyId;

                BillCycleScheduleModel billCycleSchedule;

                // if bill to date setting's use projected num days is enabled, 
                // then instead of using bill cycle schedule, 
                // use projected num day and the most recent bill's end date + 1 (the start date of current cycle) to compate the cycle with bill info
                if (btdSettings.Settings.General.UseProjectedNumDaysforBilltoDate)
                {
                    var startDate = meterDetails.EndDate.AddDays(1);
                    var endDate = startDate.AddDays(btdSettings.Settings.General.ProjectedNumberOfDays - 1);

                    // exclude cost to date calculation if the computed cycle is in the past; bill to date/cost to date should always be current
                    if (endDate < DateTime.Today.Date)
                    {
                        Logger.Warn($"Computed current bill cycle is in the past for meterId {meterId}.", LogModel);
                        return;
                    }

                    billCycleSchedule = new BillCycleScheduleModel
                    {
                        BeginDate = startDate.ToString("MM-dd-yyyy"),
                        EndDate = endDate.ToString("MM-dd-yyyy")
                    };

                    Logger.Warn(
                        $"Bill cycle schedule is overrided for meter Id {meterId} Begin Date {billCycleSchedule.BeginDate} End Date{billCycleSchedule.EndDate}.",
                        LogModel);
                }
                else
                {
                    billCycleSchedule = BillingCycleScheduleManager.GetBillingCycleScheduleByDateAsync(clientId,
                        meterDetails.BillCycleScheduleId, asOfDate);

                    if (billCycleSchedule == null)
                    {
                        Logger.Warn("BillCycle schedule is missing.", LogModel);
                        return;
                    }
                }

                var isFault = await IsFault(clientId, accountMeterDetails[0], meterDetails);
                // get the account updated beginDate/endDate
                // for shared account, only use the first available one if exists
                foreach (var accountMeterDetail in accountMeterDetails)
                {
                    var accountUpdateForReadStartDate = AccountManager.GetAccountUpdate(clientId,
                        accountMeterDetail.CustomerId, Enums.AccountUpdateType.ReadStartDate.ToString(),
                        accountMeterDetail.ServiceContractId, false);

                    var accountUpdateForReadEndDate = AccountManager.GetAccountUpdate(clientId,
                        accountMeterDetail.CustomerId, Enums.AccountUpdateType.ReadEndDate.ToString(),
                        accountMeterDetail.ServiceContractId, false);

                    var tasks = new List<Task<AccountUpdatesModel>>
                    {
                        accountUpdateForReadStartDate,
                        accountUpdateForReadEndDate
                    };

                    await Task.WhenAll(tasks);

                    billCycleSchedule.BeginDate = accountUpdateForReadStartDate.Result == null
                        ? billCycleSchedule.BeginDate
                        : string.IsNullOrEmpty(accountUpdateForReadStartDate.Result.NewValue)
                            ? billCycleSchedule.BeginDate
                            : accountUpdateForReadStartDate.Result.NewValue;

                    billCycleSchedule.EndDate = accountUpdateForReadEndDate.Result == null
                        ? billCycleSchedule.EndDate
                        : string.IsNullOrEmpty(accountUpdateForReadEndDate.Result.NewValue)
                            ? billCycleSchedule.EndDate
                            : accountUpdateForReadEndDate.Result.NewValue;

                    if (accountUpdateForReadStartDate.Result != null || accountUpdateForReadEndDate != null)
                        break;
                }
                var amiList = AmiManager.GetAmiList(Convert.ToDateTime(billCycleSchedule.BeginDate), asOfDate, meterId, clientId);

                if (amiList == null)
                {
                    Logger.Warn("AMI data is missing.", LogModel);
                    return;
                }

                amiList = amiList.ToList();
                // ReSharper disable once PossibleMultipleEnumeration
                var firstEntity = amiList.FirstOrDefault();
                int amiUnitId = 0;
                if (firstEntity != null)
                {
                    amiUnitId = firstEntity.GetType().GetProperty("UOMId").GetValue(firstEntity, null);
                }


                int noOfDays;
                var fueltype = FuelType.water;
                var commodityType = (Enums.CommodityType)Convert.ToInt32(meterDetails.CommodityId);
                var serviceContractId = accountMeterDetails[0].ServiceContractId;

                if (commodityType == Enums.CommodityType.Gas)
                {
                    fueltype = FuelType.gas;
                }

                if (commodityType == Enums.CommodityType.Electric)
                {
                    fueltype = FuelType.electric;
                }

                if (commodityType == Enums.CommodityType.Sewer)
                {
                    fueltype = FuelType.water;
                    serviceContractId = meterDetails.ServiceContractId; //override here with the sewer SC from the bill where it appears with a water meter
                }

                // ReSharper disable once PossibleMultipleEnumeration
                var readings = AmiManager.GetReadings(amiList, out noOfDays);
                var asOfAmiDate = readings?.OrderByDescending(e => e.Timestamp).FirstOrDefault()?.Timestamp;

                var meter = new Meter(meterId, rateCompanyId, meterDetails.RateClass,
                    fueltype, Convert.ToDateTime(billCycleSchedule.BeginDate), asOfAmiDate.HasValue && asOfAmiDate > DateTime.MinValue ? asOfAmiDate.Value : asOfDate,
                    Convert.ToDateTime(billCycleSchedule.EndDate));

                var billToDateCalculator = CalculationManagerFactory.Value.CreateCalculationManager(btdSettings);

                CostToDateResult ctdResult = null;
                meter.Readings = readings;

                if (isFault == false)
                {
                    ctdResult = billToDateCalculator.CalculateMeterCostToDate(meter);
                }

                var usage = meter.Readings?.Select(e => e.Quantity).Sum();

                var costToDate = new CalculationModel
                {
                    AccountId = accountMeterDetails[0].AccountId,
                    AsOfAmiDate = asOfAmiDate,
                    AsOfCalculationDate = asOfDate,
                    CustomerId = accountMeterDetails[0].CustomerId,
                    BillCycleEndDate = billCycleSchedule.EndDate,
                    BillCycleStartDate = billCycleSchedule.BeginDate,
                    BillDays = noOfDays,
                    ClientId = clientId,
                    MeterId = meterId,
                    ServiceContractId = serviceContractId,
                    CommodityId = Convert.ToInt32(meterDetails.CommodityId),
                    // Unit = Convert.ToInt32(meterDetails?.UOMId), //enum UomType
                    Unit = amiUnitId,
                    Usage = ctdResult?.Usages.Select(e => e.Quantity).Sum() ?? usage,
                    Cost = ctdResult?.Cost,
                    ProjectedCost = ctdResult?.ProjectedCost,
                    ProjectedUsage = ctdResult?.ProjectedUsages.Select(e => e.Quantity).Sum(),
                    AverageDailyCost = ctdResult?.Cost / noOfDays,
                    AverageDailyUsage = usage / noOfDays,
                    RateClass = meterDetails.RateClass
                };

                await CalculationManager.InsertCtdCalculationAsync(costToDate);

                // Start BTD calculation
                CalculateBillToDate(billingDetails, billCycleSchedule, asOfDate, billToDateCalculator, costToDate, accountMeterDetails[0], btdSettings, clientId);

                //send msg to Evaluation Queue - but not for sewer
                if (commodityType != Enums.CommodityType.Sewer)
                {
                    foreach (var accountMeterDetail in accountMeterDetails)
                    {
                        string message =
                            $"{clientId}^^{accountMeterDetail.CustomerId}^^{accountMeterDetail.AccountId}^^{accountMeterDetail.ServiceContractId}^^{asOfDate.ToShortDateString()}^^{LogModel.Source}^^{LogModel.RowIndex}^^{LogModel.Metadata}^^{(int) LogModel.ProcessingType}";

                        await QueueRepository.AddQueueAsync(Constants.QueueNames.AoEvaluationQueue, message);
                    }
                }
            }
        }

        private async Task<bool> IsFault(int clientId, AccountLookupModel accountMeterDetails,
            BillingModel meterDetails)
        {
            Task<AccountUpdatesModel> billCycle = AccountManager.GetAccountUpdate(clientId,
                accountMeterDetails.CustomerId, Enums.AccountUpdateType.BillCycle.ToString(),
                accountMeterDetails.ServiceContractId, false);

            Task<AccountUpdatesModel> meterreplacement = AccountManager.GetAccountUpdate(clientId,
                accountMeterDetails.CustomerId, Enums.AccountUpdateType.MeterReplacement.ToString(),
                accountMeterDetails.ServiceContractId, false);

            var tasks = new List<Task<AccountUpdatesModel>> { billCycle, meterreplacement };

            await Task.WhenAll(tasks);

            bool isFault = (billCycle.Result?.UpdateSentDate != null && meterDetails != null && billCycle.Result.UpdateSentDate.Value.Date > meterDetails.EndDate.Date) ||
                           (meterreplacement.Result?.UpdateSentDate != null &&
                           meterreplacement.Result.UpdateSentDate.Value > meterDetails?.EndDate.Date);
            if (isFault)
                Logger.Warn("Fault occured either becasue of Bill cycle change Or Meter replacement.", LogModel);

            return isFault;
        }

        private async Task<bool> IsFault(int clientId, string customerId, string serviceContractId, BillingModel meterDetails)
        {
            var tasks = new List<Task<AccountUpdatesModel>>
            {
                AccountManager.GetAccountUpdate(clientId, customerId, Enums.AccountUpdateType.BillCycle.ToString(),
                    serviceContractId, false),
                AccountManager.GetAccountUpdate(clientId, customerId,
                    Enums.AccountUpdateType.MeterReplacement.ToString(), serviceContractId, false)
            };

            await Task.WhenAll(tasks);
            bool isFault = (tasks[0].Result?.UpdateSentDate != null && meterDetails != null && tasks[0].Result.UpdateSentDate.Value.Date > meterDetails.EndDate.Date) ||
                           (tasks[1].Result?.UpdateSentDate != null &&
                           tasks[1].Result.UpdateSentDate.Value > meterDetails?.EndDate.Date);
            if (isFault)
                Logger.Warn("Fault occured either becasue of Bill cycle change Or Meter replacement.", LogModel);

            return isFault;
        }

        private void CalculateBillToDate(List<BillingModel> billingDetails, BillCycleScheduleModel billCycleSchedule, DateTime asOfDate, IBillToDate billToDateCalculator, CalculationModel costToDate, AccountLookupModel accountMeterDetails, BillToDateSettings btdSettings, int clientId)
        {
            LogModel.Module = Enums.Module.BillToDateCalculation;
            var servicesInBill =
             billingDetails.Where(
                 e => e.MeterType == Convert.ToString(Enums.MeterType.ami) && e.ServiceContractId != costToDate.ServiceContractId).ToList();

            var tasks = servicesInBill.Select(service => CalculationManager.GetCtdCalculationAsync(service.ClientId, service.AccountId, service.ServiceContractId)).ToList();
            Task.WhenAll(tasks);

            var ctds = tasks.Where(task => task.Result != null).Select(e => e.Result).ToList();

            // Calculate BTD
            ctds.Add(costToDate);

            var nmBillDate = ctds.Min(c => Convert.ToDateTime(c.BillCycleStartDate));


            var nonMeteredServiceResult = GetNonMeteredServices(billingDetails, btdSettings, nmBillDate,
                billToDateCalculator, clientId);

            var nonMeterCharges = nonMeteredServiceResult.Select(e => e.Cost).Sum();
            var nonMeterChargesProjected = nonMeteredServiceResult.Select(e => e.ProjectedCost).Sum();

            var totalCtdCost = ctds.Select(e => e.Cost ?? 0).Sum();
            var totalProjectedCtdCost = ctds.Select(e => e.ProjectedCost ?? 0).Sum();
            var totalBtd = totalCtdCost + nonMeterCharges;
            var projectedBtd = totalProjectedCtdCost + nonMeterChargesProjected;
            var asOfAmiDate = ctds.Select(e => e.AsOfAmiDate).Max();
            //var noOfBillDays = asOfAmiDate != null
            //    ? (Convert.ToDateTime(asOfAmiDate) - Convert.ToDateTime(billCycleSchedule.BeginDate)).Days + 1
            //    : (int?)null;
            var noOfBillDays =
                ctds.Select(e => (Convert.ToDateTime(e.AsOfAmiDate) - Convert.ToDateTime(e.BillCycleStartDate)).Days + 1)
                    .Max();

            var maxBillCycleDays =
                ctds.Select(
                    e =>
                        Math.Floor(
                            (Convert.ToDateTime(e.BillCycleEndDate) - Convert.ToDateTime(e.BillCycleStartDate))
                                .TotalDays + 1)).Max();
            var startDate = asOfAmiDate != null ? (Convert.ToDateTime(asOfAmiDate).Date.AddDays(-1 * noOfBillDays + 1).Date).ToString("MM-dd-yyyy") : billCycleSchedule.BeginDate;
            var endDate = asOfAmiDate != null ? (Convert.ToDateTime(startDate).AddDays(maxBillCycleDays - 1).Date).ToString("MM-dd-yyyy") : billCycleSchedule.EndDate;


            // ReSharper disable once CompareOfFloatsByEqualityOperator
            if (totalCtdCost == 0) // If for some reason calculations are not performered then Bill days = current date- Schedule Begin Date 
            {
                noOfBillDays = (Convert.ToDateTime(asOfDate) - Convert.ToDateTime(billCycleSchedule.BeginDate)).Days + 1;
            }

            var billToDate = new CalculationModel
            {
                AccountId = accountMeterDetails.AccountId,
                CustomerId = accountMeterDetails.CustomerId,
                BillCycleEndDate = endDate,
                BillCycleStartDate = startDate,
                AsOfCalculationDate = asOfDate,
                AsOfAmiDate = asOfAmiDate,
                BillDays = noOfBillDays, //Max (AMI Date-beginDate)
                ClientId = Convert.ToInt32(accountMeterDetails.ClientId),
                Cost = totalBtd,
                ProjectedCost = projectedBtd,
                AverageDailyCost = totalBtd / noOfBillDays
            };

            CalculationManager.InsertBtdCalculationAsync(billToDate).Wait();
        }

        public async Task CalculateBillToDate(int clientId, string accountId, string customerId, DateTime asOfDate)
        {
            LogModel.Module = Enums.Module.BillToDateCalculation;
            var billingDetails = BillingManager.GetAllServicesFromLastBill(clientId, customerId, accountId).ToList();

            LogModel.CustomerId = customerId;
            LogModel.AccountId = accountId;

            if (billingDetails.Count == 0)
            {
                Logger.Warn("Billing information is missing.", LogModel);
                return;
            }

            string billCycleScheduleStartDate = "";
            string billCycleScheduleEndDate = "";

            var meterServiceList = new List<MeteredService>();

            Dictionary<string, dynamic> meterUsage = new Dictionary<string, dynamic>();

            //// READ BILL-TO-DATE SETTINGS            
            var btdSettings = ClientConfigFacadeManager.Value.GetBillToDateSettings(clientId);
            var rateCompanyId = btdSettings.RateCompanyId;
            var billToDateCalculator = CalculationManagerFactory.Value.CreateCalculationManager(btdSettings);

            // ReSharper disable once SimplifyLinqExpression
            if (!billingDetails.Any(b => b.MeterType == Convert.ToString(Enums.MeterType.ami)))
            {
                Logger.Warn("Metered Services are missing.", LogModel);
                return;
            }

            foreach (var meterDetails in billingDetails)
            {
                MeteredService meteredService = new MeteredService();
                if (meterDetails.MeterType != Convert.ToString(Enums.MeterType.ami)) continue;

                if (string.IsNullOrEmpty(billCycleScheduleStartDate))
                {
                    var billCycleSchedule = BillingCycleScheduleManager.GetBillingCycleScheduleByDateAsync(clientId,
                        meterDetails?.BillCycleScheduleId, asOfDate);

                    if (billCycleSchedule == null)
                    {
                        Logger.Warn("BillCycle schedule is missing.", LogModel);
                        return;
                    }

                    billCycleScheduleStartDate = billCycleSchedule.BeginDate;
                    billCycleScheduleEndDate = billCycleSchedule.EndDate;

                }

                var isFault = await IsFault(clientId, customerId, meterDetails.ServiceContractId, meterDetails);

                if (isFault) continue;

                var tasks = new List<Task<AccountUpdatesModel>>
                {
                    AccountManager.GetAccountUpdate(clientId,
                        customerId, Enums.AccountUpdateType.ReadStartDate.ToString(),
                        meterDetails.ServiceContractId, false),
                    AccountManager.GetAccountUpdate(clientId,
                        customerId, Enums.AccountUpdateType.ReadEndDate.ToString(),
                        meterDetails.ServiceContractId, false)
                };

                await Task.WhenAll(tasks);
                var accountUpdateForReadStartDate = tasks[0].Result;
                var accountUpdateForReadEndDate = tasks[1].Result;

                billCycleScheduleStartDate = accountUpdateForReadStartDate == null
                    ? billCycleScheduleStartDate
                    : string.IsNullOrEmpty(accountUpdateForReadStartDate.NewValue)
                        ? billCycleScheduleStartDate
                        : accountUpdateForReadStartDate.NewValue;

                billCycleScheduleEndDate = accountUpdateForReadEndDate == null
                    ? billCycleScheduleEndDate
                    : string.IsNullOrEmpty(accountUpdateForReadEndDate.NewValue)
                        ? billCycleScheduleEndDate
                        : accountUpdateForReadEndDate.NewValue;

                var amiList = AmiManager.GetAmiList(Convert.ToDateTime(billCycleScheduleStartDate), asOfDate,
                    meterDetails.MeterId, clientId);

                if (amiList == null)
                {
                    Logger.Warn("AMI data is missing.", LogModel);
                    return;
                }

                amiList = amiList.ToList();
                var firstEntity = amiList.FirstOrDefault();
                int amiUnitId = 0;
                if (firstEntity != null)
                {
                    amiUnitId = firstEntity.GetType().GetProperty("UOMId").GetValue(firstEntity, null);
                }

                var fueltype = FuelType.water;
                var commodityType = (Enums.CommodityType)Convert.ToInt32(meterDetails?.CommodityId);
                var serviceContractId = meterDetails.ServiceContractId;

                if (commodityType == Enums.CommodityType.Gas)
                {
                    fueltype = FuelType.gas;
                }

                if (commodityType == Enums.CommodityType.Electric)
                {
                    fueltype = FuelType.electric;
                }

                if (commodityType == Enums.CommodityType.Sewer)
                {
                    fueltype = FuelType.water;
                    serviceContractId = meterDetails.ServiceContractId;
                }

                int noOfDays;
                var readingsforMeter = AmiManager.GetReadings(amiList, out noOfDays);
                var meter = new Meter(meterDetails.MeterId, rateCompanyId, meterDetails.RateClass, fueltype,
                    Convert.ToDateTime(billCycleScheduleStartDate), asOfDate,
                    Convert.ToDateTime(billCycleScheduleEndDate), readingsforMeter);
                meteredService.Meters.Add(meter);
                meterServiceList.Add(meteredService);
                var usage = meter.Readings?.Select(e => e.Quantity).Sum();
                var asOfAmiDate = meter.Readings?.OrderByDescending(e => e.Timestamp).FirstOrDefault()?.Timestamp;

                meterUsage.Add(meter.MeterID + "_" + serviceContractId,
                    new
                    {
                        NoOfDays = noOfDays,
                        Usage = usage,
                        AsOfAmiDate = asOfAmiDate,
                        meterDetails.ServiceContractId,
                        AmiUnitID = amiUnitId
                    });

            }

            // create a bill-to-date bill
            Bill bill = new Bill(Convert.ToDateTime(billCycleScheduleStartDate), clientId, customerId, accountId,
                rateCompanyId);
            bill.MeteredServices.AddRange(meterServiceList);
            var nonmeteredServices = GetNonMeteredServicesList(billingDetails, billCycleScheduleStartDate,
                billCycleScheduleEndDate, asOfDate);
            bill.NonMeteredServices.AddRange(nonmeteredServices);

            var result = billToDateCalculator.CalculateBillToDate(bill);

            IEnumerable<Meter> metersResults = new List<Meter>();
            if (result?.Bill != null)
            {
                metersResults = result.Bill.MeteredServices.SelectMany(p => p.Meters.ToList()).ToList();
            }

            List<CalculationModel> listOfCalculationModel;
            if (metersResults.Any())
            {
                listOfCalculationModel = (from meterdetails in billingDetails
                                          join meter in metersResults on meterdetails.MeterId equals meter.MeterID
                                          where (meterdetails.MeterType == Convert.ToString(Enums.MeterType.ami) && meterdetails.RateClass == meter.RateClass)
                                          select new CalculationModel
                                          {
                                              AccountId = accountId,
                                              AsOfAmiDate = meterUsage[meterdetails.MeterId + "_" + meterdetails.ServiceContractId].AsOfAmiDate,
                                              AsOfCalculationDate = asOfDate,
                                              CustomerId = customerId,
                                              BillCycleEndDate = billCycleScheduleEndDate,
                                              BillCycleStartDate = billCycleScheduleStartDate,
                                              BillDays = meterUsage[meterdetails.MeterId + "_" + meterdetails.ServiceContractId].NoOfDays,
                                              ClientId = clientId,
                                              MeterId = meterdetails.MeterId,
                                              ServiceContractId = meterUsage[meterdetails.MeterId + "_" + meterdetails.ServiceContractId].ServiceContractId,
                                              CommodityId = meterdetails.CommodityId,
                                              Unit = meterUsage[meterdetails.MeterId + "_" + meterdetails.ServiceContractId].AmiUnitID, //enum UomType
                                              Usage = meterUsage[meterdetails.MeterId + "_" + meterdetails.ServiceContractId].Usage,
                                              RateClass = meterdetails.RateClass,
                                              Cost = meter.CostToDateResult.Cost,
                                              ProjectedCost = meter.CostToDateResult.ProjectedCost,
                                              ProjectedUsage = meter.CostToDateResult.ProjectedUsages.Select(e => e.Quantity).Sum(),
                                              AverageDailyCost = meter.CostToDateResult.Cost / meterUsage[meterdetails.MeterId + "_" + meterdetails.ServiceContractId].NoOfDays,
                                              AverageDailyUsage =
                                                  meterUsage[meterdetails.MeterId + "_" + meterdetails.ServiceContractId].Usage / meterUsage[meterdetails.MeterId + "_" + meterdetails.ServiceContractId].NoOfDays
                                          }).ToList();
            }
            else
            {
                listOfCalculationModel = (from meterdetails in billingDetails
                                          where meterdetails.MeterType == Convert.ToString(Enums.MeterType.ami)
                                          select new CalculationModel
                                          {
                                              AccountId = accountId,
                                              AsOfAmiDate = meterUsage[meterdetails.MeterId + "_" + meterdetails.ServiceContractId].AsOfAmiDate,
                                              AsOfCalculationDate = asOfDate,
                                              CustomerId = customerId,
                                              BillCycleEndDate = billCycleScheduleEndDate,
                                              BillCycleStartDate = billCycleScheduleStartDate,
                                              BillDays = meterUsage[meterdetails.MeterId + "_" + meterdetails.ServiceContractId].NoOfDays,
                                              ClientId = clientId,
                                              MeterId = meterdetails.MeterId,
                                              ServiceContractId = meterUsage[meterdetails.MeterId + "_" + meterdetails.ServiceContractId].ServiceContractId,
                                              CommodityId = meterdetails.CommodityId,
                                              Unit = meterUsage[meterdetails.MeterId + "_" + meterdetails.ServiceContractId].AmiUnitID, //enum UomType
                                              Usage = meterUsage[meterdetails.MeterId + "_" + meterdetails.ServiceContractId].Usage,
                                              RateClass = meterdetails.RateClass,
                                              Cost = 0,
                                              ProjectedCost = 0,
                                              ProjectedUsage = 0,
                                              AverageDailyCost = 0,
                                              AverageDailyUsage =
                                                  meterUsage[meterdetails.MeterId + "_" + meterdetails.ServiceContractId].Usage / meterUsage[meterdetails.MeterId + "_" + meterdetails.ServiceContractId].NoOfDays
                                          }).ToList();
            }

            //var taskForCalculations = new List<Task>();
            // ReSharper disable once LoopCanBeConvertedToQuery
            foreach (var calculationModel in listOfCalculationModel)
            {
                //taskForCalculations.Add(CalculationManager.InsertCtdCalculationAsync(calculationModel));
                await CalculationManager.InsertCtdCalculationAsync(calculationModel);
            }

            var maxAsOfAmiDate = listOfCalculationModel.Select(e => e.AsOfAmiDate).Max();
            var noOfBillDaysForBtd = maxAsOfAmiDate != null
                ? (Convert.ToDateTime(maxAsOfAmiDate) - Convert.ToDateTime(billCycleScheduleStartDate)).Days + 1
                : (int?)null;

            var totalCtdCost = listOfCalculationModel.Select(e => e.Cost ?? 0).Sum();
            // ReSharper disable once CompareOfFloatsByEqualityOperator
            if (totalCtdCost == 0)
            { // If for some reason calculations are not performered then Bill days = current date- Schedule Begin Date 
                noOfBillDaysForBtd =
                    (Convert.ToDateTime(asOfDate) - Convert.ToDateTime(billCycleScheduleStartDate)).Days + 1;
            }

            var billToDate = new CalculationModel()
            {
                AccountId = accountId,
                CustomerId = customerId,
                BillCycleEndDate = billCycleScheduleEndDate,
                BillCycleStartDate = billCycleScheduleStartDate,
                AsOfCalculationDate = asOfDate,
                AsOfAmiDate = null,
                BillDays = noOfBillDaysForBtd,
                ClientId = clientId,
                Cost = result.TotalCost,
                ProjectedCost = result.TotalProjectedCost,
                AverageDailyCost = result.TotalCost / noOfBillDaysForBtd,
            };

            await CalculationManager.InsertBtdCalculationAsync(billToDate);
            //taskForCalculations.Add(CalculationManager.InsertBtdCalculationAsync(billToDate));
            //await Task.WhenAll(taskForCalculations);

            //send msg to Evaluation Queue
            string message =
                $"{clientId}^^{customerId}^^{accountId}^^{string.Empty}^^{asOfDate.ToShortDateString()}^^{LogModel.Source}^^{LogModel.RowIndex}^^{LogModel.Metadata}^^{(int)LogModel.ProcessingType}";

            // string message = $"{clientId}:{ customerId}:{accountId}::" +
            // $"{asOfDate.ToShortDateString()}:{LogModel.Source}:{ LogModel.RowIndex}:{LogModel.Metadata}:{LogModel.ProcessingType}";

            await QueueRepository.AddQueueAsync(Constants.QueueNames.AoEvaluationQueue, message);
        }

        private List<NonMeteredServiceResult> GetNonMeteredServices(IEnumerable<BillingModel> billingDetails, BillToDateSettings btdSettings, DateTime asOfDate, IBillToDate billToDateCalculator, int clientId)
        {
            var nonMeterServices =
                billingDetails.Where(e => e.MeterType.ToLower() == "nonmetered");
            var nonMeteredServiceResult = new List<NonMeteredServiceResult>();
            // ReSharper disable once LoopCanBeConvertedToQuery
            foreach (var nonMeterService in nonMeterServices)
            {
                BillCycleScheduleModel billCycleSchedule;
                if (btdSettings.Settings.General.UseProjectedNumDaysforBilltoDate)
                {
                    var endDate = asOfDate.AddDays(btdSettings.Settings.General.ProjectedNumberOfDays - 1);

                    // exclude cost to date calculation if the computed cycle is in the past; bill to date/cost to date should always be current
                    if (endDate < DateTime.Today.Date)
                    {
                        Logger.Warn($"Computed current bill cycle is in the past for non meter commodity {((Enums.CommodityType)nonMeterService.CommodityId).ToString()}.", LogModel);
                        break;
                    }

                    billCycleSchedule = new BillCycleScheduleModel
                    {
                        BeginDate = asOfDate.ToString("MM-dd-yyyy"),
                        EndDate = endDate.ToString("MM-dd-yyyy")
                    };

                    Logger.Warn(
                        $"Bill cycle schedule is overrided for non meter commodity {((Enums.CommodityType)nonMeterService.CommodityId).ToString()} Begin Date {billCycleSchedule.BeginDate} End Date{billCycleSchedule.EndDate}.",
                        LogModel);
                }
                else
                {
                    //var  IsRateClass =  AccountManager.IsAccountUpdate(clientId,accountMeterDetails.CustomerId, Enums.AccountUpdateType.RateClass.ToString(),accountMeterDetails.ServiceContractId,false);
                    billCycleSchedule = BillingCycleScheduleManager.GetBillingCycleScheduleByDateAsync(clientId,
                        nonMeterService.BillCycleScheduleId, asOfDate);

                    if (billCycleSchedule == null)
                    {
                        Logger.Warn("BillCycle schedule is missing.", LogModel);
                        break;
                    }
                }

                var nms = new NonMeteredService(((Enums.CommodityType) nonMeterService.CommodityId).ToString(),
                    nonMeterService.TotalCost, nonMeterService.TotalUsage ?? 0,
                    nonMeterService.UOMId == null
                        ? Enums.UomType.Other.ToString().ToLower()
                        : ((Enums.UomType) nonMeterService.UOMId).ToString(),
                    Convert.ToDateTime(billCycleSchedule.BeginDate), DateTime.Today.Date);
                
                if (!btdSettings.Settings.General.UseProjectedNumDaysforBilltoDate)
                {
                    nms.ProjectedEndDate = Convert.ToDateTime(billCycleSchedule.EndDate);
                }

                nonMeteredServiceResult.Add(billToDateCalculator.CalculateNonMeteredServiceCostToDate(nms));
            }

            return nonMeteredServiceResult;
        }

        private List<NonMeteredService> GetNonMeteredServicesList(IEnumerable<BillingModel> billingDetails, string beginDate, string endDate, DateTime asOfDate)
        {
            var nonMeterServices = billingDetails.Where(e => e.MeterType.ToLower() == "nonmetered");
            var nonMeteredServiceList = new List<NonMeteredService>();

            // ReSharper disable once LoopCanBeConvertedToQuery
            foreach (var nonMeterService in nonMeterServices)
            {
                var nms = new NonMeteredService(((Enums.CommodityType)nonMeterService.CommodityId).ToString(),
                    nonMeterService.TotalCost, Convert.ToDateTime(beginDate), asOfDate, Convert.ToDateTime(endDate));
                nonMeteredServiceList.Add(nms);
            }
            return nonMeteredServiceList;
        }

        public void ScheduledCalculation(int clientId, string asOfDate)
        {
            ClientAccount.SendScheduleMessage(clientId, asOfDate);
        }

        public IEnumerable<ClientAccountModel> GetClientAccount(int clientId)
        {
            return ClientAccount.GetClientAccounts(clientId);
        }

    }
}
