﻿using AutoMapper;
using CE.AO.DataAccess;
using CE.AO.Entities.TableStorageEntities;
using CE.AO.Models;
using Microsoft.Practices.Unity;
using CE.AO.Entities;
using System.Collections.Generic;
using AO.BusinessContracts;
using Microsoft.WindowsAzure.Storage.Table;

namespace CE.AO.Business
{
    public class ClientWidgetConfiguration : IClientWidgetConfiguration
    {
        public ClientWidgetConfiguration()
        {
            LogModel = new LogModel { DisableLog = true };
        }

        public ClientWidgetConfiguration(LogModel logModel)
        {
            LogModel = logModel;
        }

        public LogModel LogModel { get; set; }

        [Dependency]
        public ITableRepository TableRepository { get; set; }

        public IEnumerable<ClientWidgetConfigurationModel> GetClientWidgetConfiguration(int clientId)
        {
            var partitionKey = ClientWidgetConfigurationEntity.SetPartitionKey(clientId);
            var filter = TableQuery.GenerateFilterCondition("PartitionKey", QueryComparisons.Equal,
                partitionKey);

            var clientWidgetConfigurationDetails = TableRepository.GetAllAsync<ClientWidgetConfigurationEntity>(Constants.TableNames.ClientWidgetConfiguration, filter);
            return Mapper.Map<IEnumerable<ClientWidgetConfigurationEntity>, IEnumerable<ClientWidgetConfigurationModel>>(clientWidgetConfigurationDetails);
        }
    }
}