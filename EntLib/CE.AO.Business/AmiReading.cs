﻿using System;
using System.Threading.Tasks;
using AO.BusinessContracts;
using AutoMapper;
using CE.AO.DataAccess;
using CE.AO.Entities;
using CE.AO.Entities.TableStorageEntities;
using CE.AO.Models;

namespace CE.AO.Business
{
    public class AmiReading : IAmiReading
    {
        public AmiReading(LogModel logModel, ITableRepository tableRepository)
        {
            LogModel = logModel;
            TableRepository = tableRepository;
        }

        public LogModel LogModel { get; set; }
       
        public ITableRepository TableRepository { get; set; }

        public async Task<bool> InsertOrMergeAmiReadingAsync(NccAmiReadingModel model)
        {
            var entityModel = Mapper.Map<NccAmiReadingModel, AmiReadingEntity>(model);
            entityModel.PartitionKey = AmiReadingEntity.SetPartitionKey(model.ClientId, model.MeterId, model.AmiTimeStamp);
            entityModel.RowKey = AmiReadingEntity.SetRowKey(model.AmiTimeStamp);
            return await TableRepository.InsertOrMergeSingleAsync(Constants.TableNames.AmiReading, entityModel);
        }

        public async Task<NccAmiReadingModel> GetAmiReading(int clientId, string meterId, DateTime amiTimeStamp)
        {
            var partitionKey = AmiReadingEntity.SetPartitionKey(clientId, meterId, amiTimeStamp);
            var rowkey = AmiReadingEntity.SetRowKey(amiTimeStamp);
            var amiReading =
                await TableRepository.GetSingleAsync<AmiReadingEntity>(Constants.TableNames.AmiReading, partitionKey, rowkey);

            return Mapper.Map<AmiReadingEntity, NccAmiReadingModel>(amiReading);
        }

        public Task<bool> DeleteAmiReadingAsync(int clientId, string meterId)
        {
            throw new NotImplementedException();
        }
    }
}
