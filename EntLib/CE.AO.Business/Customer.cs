﻿using System;
using AutoMapper;
using CE.AO.DataAccess;
using CE.AO.Entities.TableStorageEntities;
using CE.AO.Logging;
using CE.AO.Models;
using CE.AO.Entities;
using Microsoft.Practices.Unity;
using System.Threading.Tasks;
using AO.BusinessContracts;

namespace CE.AO.Business
{
    public class Customer : ICustomer
    {
        public Customer(LogModel logModel, ITableRepository tableRepository)
        {
            LogModel = logModel;
            TableRepository = tableRepository;
        }

        public LogModel LogModel { get; set; }
      
        public ITableRepository TableRepository { get; set; }

        [Dependency]
        public ISendSms SendSms { get; set; }

        public async Task<CustomerModel> GetCustomerAsync(int clientId, string customerId)
        {
            var partitionKey = CustomerEntity.SetPartitionKey(clientId, customerId);
            var rowKey = CustomerEntity.SetRowKey();
            var entity = await TableRepository.GetSingleAsync<CustomerEntity>(Constants.TableNames.CustomerPremiseProfile, partitionKey, rowKey);
            return Mapper.Map<CustomerEntity, CustomerModel>(entity);
        }

        public async Task<bool> InsertOrMergeCustomerAsync(CustomerModel model)
        {
            var entity = Mapper.Map<CustomerModel, CustomerEntity>(model);
            entity.PartitionKey = CustomerEntity.SetPartitionKey(model.ClientId, model.CustomerId);
            entity.RowKey = CustomerEntity.SetRowKey();
            return await TableRepository.InsertOrMergeSingleAsync(Constants.TableNames.CustomerPremiseProfile, entity);
        }

        //Create subscription and then update customer with newly created trumpia subscription id
        public bool CreateSubscriptionAndUpdateCustomer(CustomerModel customerModel, string apiKey, string username, string contactList)
        {
            if (string.IsNullOrEmpty(customerModel.Phone1))
            {
                Logger.Fatal(@" Phone number not specified in Customer information.", LogModel);
                return false;
            }
            
            var requestId = SendSms.CreateNewTrumpiaSubscription(customerModel,apiKey,username,contactList);
            if (!string.IsNullOrEmpty(requestId))
            {
                customerModel.TrumpiaSubscriptionId = SendSms.GetSubscriptionId(customerModel, requestId, apiKey, username);
                //Update customer with new subscription id
                customerModel.TrumpiaRequestDetailPkRk = requestId;
                var customerTask = Task.Run(() => InsertOrMergeCustomerAsync(customerModel));
                Task.WaitAll(customerTask);
                return customerTask.Result;
                //return InsertOrMergeCustomerAsync(customerModel).Result;
            }
            else
                return false;
        }

        public bool SmsSubscirptionCheckAndUpdate(CustomerModel customerModel, string apiKey, string username,
            string contactList, ref bool isReOptIn)
        {
            if (string.IsNullOrEmpty(customerModel.Phone1))
            {
                Logger.Fatal(@" Phone number not specified in Customer information.", LogModel);
                return false;
            }

            var smsSubscriptionInfo = SendSms.GetSubscriptionInfo(customerModel.TrumpiaSubscriptionId, apiKey, username);

            // update the existing subscription with the number
            if(smsSubscriptionInfo.status_code == null)
            {
                if (smsSubscriptionInfo.mobile == null)
                {
                    SendSms.UpdateSubscriptionInfo(customerModel, customerModel.TrumpiaSubscriptionId, apiKey, username,
                        contactList);
                    isReOptIn = true;
                    return true;
                }
                else if(!string.IsNullOrEmpty(smsSubscriptionInfo.mobile.verified) && Convert.ToBoolean(smsSubscriptionInfo.mobile.verified))
                {
                    if(string.IsNullOrEmpty(smsSubscriptionInfo.mobile.value) || !smsSubscriptionInfo.mobile.value.Equals(customerModel.Phone1))
                    {
                        SendSms.UpdateSubscriptionInfo(customerModel, customerModel.TrumpiaSubscriptionId, apiKey, username,
                           contactList);
                        return true;
                    }
                }
            }
            else
            {   // if subscription is completely deleted, create a new subscription and update customer model
                CreateSubscriptionAndUpdateCustomer(customerModel, apiKey, username, contactList);
                isReOptIn = true;
                return true;
            }

            return false;
        }

        public Task<bool> DeleteCustomerAsync(int clientId, string customerId)
        {
            throw new NotImplementedException();
        }
    }
}
