﻿using CE.AO.DataAccess;
using CE.AO.Entities;
using CE.AO.Entities.TableStorageEntities;
using System.Threading.Tasks;
using AO.BusinessContracts;
using AutoMapper;
using CE.AO.Models;
using System;

namespace CE.AO.Business
{
    public class TrumpiaRequestDetail : ITrumpiaRequestDetail
    {
        public TrumpiaRequestDetail(LogModel logModel, ITableRepository tableRepository)
        {
            LogModel = logModel;
            TableRepository = tableRepository;
        }

        public LogModel LogModel { get; set; }

        public ITableRepository TableRepository { get; set; }

        public Task<bool> DeleteTrumpiaReqeustnAsync(string request)
        {
            throw new NotImplementedException();
        }

        public TrumpiaRequestDetailModel GetTrumpiaRequestDetail(string requestId)
        {
            var partitionKey = TrumpiaRequestDetailEntity.SetPartitionKey(requestId);
            var rowKey = TrumpiaRequestDetailEntity.SetRowKey(requestId);
            var entity = TableRepository.GetSingle<TrumpiaRequestDetailEntity>(Constants.TableNames.TrumpiaRequestDetail,
                partitionKey, rowKey);
            return Mapper.Map<TrumpiaRequestDetailEntity, TrumpiaRequestDetailModel>(entity);
        }

        public async Task<TrumpiaRequestDetailModel> GetTrumpiaRequestDetailAsync(string requestId)
        {
            var partitionKey = TrumpiaRequestDetailEntity.SetPartitionKey(requestId);
            var rowkey = TrumpiaRequestDetailEntity.SetRowKey(requestId);
            var trumpiaRequestDetailEntity =
                await
                    TableRepository.GetSingleAsync<TrumpiaRequestDetailEntity>(Constants.TableNames.TrumpiaRequestDetail, partitionKey,
                        rowkey);
            return Mapper.Map<TrumpiaRequestDetailEntity, TrumpiaRequestDetailModel>(trumpiaRequestDetailEntity);
        }

        public bool InsertOrMergeTrumpiaRequestDetail(TrumpiaRequestDetailModel model)
        {
            var entity = Mapper.Map<TrumpiaRequestDetailModel, TrumpiaRequestDetailEntity>(model);
            entity.PartitionKey = TrumpiaRequestDetailEntity.SetPartitionKey(entity.RequestId);
            entity.RowKey = TrumpiaRequestDetailEntity.SetRowKey(entity.RequestId);
            return TableRepository.InsertOrMergeSingle(Constants.TableNames.TrumpiaRequestDetail, entity);
        }

        public async Task<bool> InsertOrMergeTrumpiaRequestDetailAsync(TrumpiaRequestDetailModel model)
        {
            var entity = Mapper.Map<TrumpiaRequestDetailModel, TrumpiaRequestDetailEntity>(model);
            entity.PartitionKey = TrumpiaRequestDetailEntity.SetPartitionKey(entity.RequestId);
            entity.RowKey = TrumpiaRequestDetailEntity.SetRowKey(entity.RequestId);
            return await TableRepository.InsertOrMergeSingleAsync(Constants.TableNames.TrumpiaRequestDetail, entity);
        }
    }
}
