﻿using AutoMapper;
using CE.AO.DataAccess;
using CE.AO.Entities;
using CE.AO.Entities.TableStorageEntities;
using CE.AO.Logging;
using CE.AO.Models;
using CE.RateModel;
using Enums = CE.AO.Utilities.Enums;
using Microsoft.Practices.Unity;
using Microsoft.WindowsAzure.Storage.Table;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using AO.BusinessContracts;
using CE.ContentModel.Entities;

namespace CE.AO.Business
{
    public class Notification : INotification
    {
        private const string StatusNoRecipient = "MPME2009";
        private const string StatusMmsNotSupported = "MPME2020";
        private const string StatusInvalidSubscription = "MPME2301";
        private const string StatusInProgress = "MPCE4001";
        private const string StatusSent = "sent";
        private const string StatusSending = "sending";
        private const string StatusScheduled = "scheduled";

        public Notification(LogModel logModel, ITableRepository tableRepository)
        {
            LogModel = logModel;
            TableRepository = tableRepository;
        }

        public LogModel LogModel { get; set; }

        [Dependency]
        // ReSharper disable once UnusedAutoPropertyAccessor.Local
        public ITrumpiaRequestDetail TrumpiaManager { get; set; }

        [Dependency]
        // ReSharper disable once UnusedAutoPropertyAccessor.Local
        public ISubscription SubscriptionManager { get; set; }

        [Dependency]
        public ICustomer CustomerManager { get; set; }

        [Dependency]
        public ISendSms SendSmsManager { get; set; }

        [Dependency]
        public ISendEmail SendEmailManager { get; set; }

        public ITableRepository TableRepository { get; set; }

        public async Task ProcessMessage(List<SubscriptionModel> subscriptions, InsightModel evaluation, ClientSettings clientSettings,
            CustomerModel customerModel, BillingModel billingModel, Dictionary<string, List<TierBoundary>> tierBoundariesWithServiceContractId, List<CalculationModel> calculations)
        {
            Extensions.MergeSettings(subscriptions, clientSettings, billingModel);

            var clientTimezoneString = clientSettings.TimeZone; //Set from Settings            
            var clientTimezoneEnum = (Enums.Timezone)Enum.Parse(typeof(Enums.Timezone), Convert.ToString(clientTimezoneString));
            var clientTimezone = TimeZoneInfo.FindSystemTimeZoneById(clientTimezoneEnum.GetDescription());

            // email settings
            var clientEmailUsername = clientSettings.OptInEmailUserName;
            var clientEmailPassword = clientSettings.OptInEmailPassword;
            var clientEmailFrom = clientSettings.OptInEmailFrom;

            // sms settings
            var smsApiKey = clientSettings.TrumpiaApiKey;
            var smsUserName = clientSettings.TrumpiaUserName;

            var programs = clientSettings.Programs.Where(p => p.UtilityProgramName == evaluation.ProgramName).ToList();
            //var insightModels = evaluations as IList<InsightModel> ?? evaluations.ToList();
            foreach (var program in programs)
            {
                var programSubscription = subscriptions.FirstOrDefault(s => s.ProgramName == program.UtilityProgramName && string.IsNullOrEmpty(s.InsightTypeName));
                //if (evaluation.ProgramName == program.UtilityProgramName)
                //{
                foreach (var insight in program.Insights)
                {
                    //Get the notifytime from clientsetting and convert it to utc time and then send to email
                    DateTime notifyDateTime = !string.IsNullOrEmpty(insight.NotifyTime) ? DateTime.ParseExact(insight.NotifyTime, "HH:mm", CultureInfo.InvariantCulture) : DateTime.UtcNow;
                    var currentDate = new DateTime(DateTime.UtcNow.Year, DateTime.UtcNow.Month, DateTime.UtcNow.Day, notifyDateTime.Hour, notifyDateTime.Minute, notifyDateTime.Second);
                    var setDate = !string.IsNullOrEmpty(insight.NotifyTime) ? (TimeZoneInfo.ConvertTimeToUtc(currentDate, clientTimezone)) : DateTime.UtcNow;
                    var setSmsDate = !string.IsNullOrEmpty(insight.NotifyTime) ? (TimeZoneInfo.ConvertTimeBySystemTimeZoneId(TimeZoneInfo.ConvertTimeToUtc(currentDate, clientTimezone), "Eastern Standard Time")) : (DateTime?)null;
                    string tier;
                    var notificationModelList = NotificationRequired(evaluation, insight, customerModel.CustomerId, out tier);
                    var calculation = !string.IsNullOrEmpty(evaluation.ServiceContractId)
                        ? calculations.Where(c => !string.IsNullOrEmpty(c.ServiceContractId)).FirstOrDefault(c => c.ServiceContractId.Equals(evaluation.ServiceContractId))
                        : calculations.FirstOrDefault(c => string.IsNullOrEmpty(c.ServiceContractId));
                    var commodityType = calculation.CommodityId != null ? ((Enums.CommodityType)calculation.CommodityId).GetDescription() : string.Empty;

                    var customInsightModel = Extensions.GetCustomInsightModel(subscriptions, insight, program, evaluation.ServiceContractId, commodityType, billingModel?.UOMId);

                    bool isCommodityTypecheck = false;

                    string insightcommodityType = string.IsNullOrEmpty(insight.CommodityType) ? "" : insight.CommodityType;

                    if (string.IsNullOrEmpty(commodityType) && string.IsNullOrEmpty(insightcommodityType))
                    {
                        isCommodityTypecheck = true;
                    }
                    else if (insightcommodityType.Contains(commodityType) || string.IsNullOrEmpty(evaluation.ServiceContractId))
                    {
                        isCommodityTypecheck = true;
                    }
                    else if (!string.IsNullOrEmpty(insightcommodityType))
                    {
                        isCommodityTypecheck = insightcommodityType.Contains(commodityType);
                    }

                    if (isCommodityTypecheck)
                    {
                        var tierBoundaries = new List<TierBoundary>();
                        if (!string.IsNullOrEmpty(evaluation.ServiceContractId))
                            tierBoundariesWithServiceContractId.TryGetValue(evaluation.ServiceContractId, out tierBoundaries);
                        var sendEmail = new SendEmail();

                        //var sendSms = new SendSms();
                        var notificationDate = evaluation.AsOfEvaluationDate;
                        // the channel portion will be added to custom key when sending email/sms
                        var customKey = !string.IsNullOrEmpty(evaluation.ServiceContractId) ?
                            $"{evaluation.ClientId}_{evaluation.AccountId}|{notificationDate.ToString("yyyy-MM-dd")}_{program.ProgramName}_{evaluation.ServiceContractId}_{insight.InsightName}" :
                            $"{evaluation.ClientId}_{evaluation.AccountId}|{notificationDate.ToString("yyyy-MM-dd")}_{program.ProgramName}_{insight.InsightName}";


                        if (notificationModelList != null && IsNotified(evaluation, insight, tier))
                        {
                            var uomContentList = Extensions.GetUomContents(evaluation.ClientId);
                            var textContentList = Extensions.GetTextContents(evaluation.ClientId, "en-US", "common");

                            var isSent = false;
                            var tasks = new List<Task>();
                            if (insight.DefaultCommunicationChannel.Equals(Enums.SubscriptionChannel.Email.GetDescription(), StringComparison.InvariantCultureIgnoreCase) && Convert.ToBoolean(programSubscription.IsEmailOptInCompleted))
                            {
                                if (!string.IsNullOrEmpty(customerModel.EmailAddress) && !string.IsNullOrEmpty(insight.EmailTemplateId))
                                {
                                    tasks.Add(SaveNotification(notificationModelList.FirstOrDefault(n => n.Channel.Equals(Enums.SubscriptionChannel.Email.GetDescription())), notificationDate));
                                    var emailIdentifiers = GetEmailSubstitutions(evaluation, customInsightModel, customerModel, billingModel, tierBoundaries, calculation, clientSettings.UnmaskedAccountIdEndingDigit, uomContentList, textContentList);
                                    tasks.Add(sendEmail.SendEmailAsync(clientEmailFrom,
                                            new List<string> { customerModel.EmailAddress }, emailIdentifiers,
                                            insight.EmailTemplateId, clientEmailUsername, clientEmailPassword,
                                        $"{customKey}_{Enums.SubscriptionChannel.Email.GetDescription()}", setDate));
                                    isSent = true;
                                }
                            }
                            else if (insight.DefaultCommunicationChannel.Equals(Enums.SubscriptionChannel.SMS.GetDescription(), StringComparison.InvariantCultureIgnoreCase) && Convert.ToBoolean(programSubscription.IsSmsOptInCompleted))
                            {
                                if (!string.IsNullOrEmpty(customerModel.TrumpiaSubscriptionId))
                                {
                                    tasks.Add(SaveNotification(notificationModelList.FirstOrDefault(n => n.Channel.Equals(Enums.SubscriptionChannel.SMS.GetDescription())), notificationDate));
                                    var smsBody = GetSmsBody(evaluation, calculation, billingModel, customInsightModel, insight.SmsTemplateId, clientSettings.UnmaskedAccountIdEndingDigit, uomContentList, textContentList);
                                    //tasks.Add(sendSms.Send(smsApiKey, smsUserName, smsBody, customerModel.TrumpiaSubscriptionId, customKey,evaluation.ClientId,evaluation.AccountId,program.ProgramName,evaluation.ServiceContractId,insight.InsightName, notificationDate, setSmsDate));
                                    tasks.Add(
                                        SendSms(billingModel, subscriptions,
                                            notificationModelList.FirstOrDefault(
                                                n => n.Channel.Equals(Enums.SubscriptionChannel.SMS.GetDescription())),
                                            customerModel, $"{customKey}_{Enums.SubscriptionChannel.SMS.GetDescription()}", smsApiKey, smsUserName, smsBody, evaluation,
                                            program.ProgramName, insight.InsightName, notificationDate, tier, setSmsDate));
                                    isSent = true;
                                }
                            }
                            else if (insight.DefaultCommunicationChannel.Equals(Enums.SubscriptionChannel.EmailAndSMS.GetDescription(), StringComparison.InvariantCultureIgnoreCase))
                            {
                                if (Convert.ToBoolean(programSubscription.IsEmailOptInCompleted) && !string.IsNullOrEmpty(customerModel.EmailAddress) &&
                                    !string.IsNullOrEmpty(insight.EmailTemplateId))
                                {
                                    var emailIdentifiers = GetEmailSubstitutions(evaluation, customInsightModel,
                                        customerModel,
                                        billingModel, tierBoundaries, calculation,
                                        clientSettings.UnmaskedAccountIdEndingDigit, uomContentList, textContentList);
                                    tasks.Add(SaveNotification(notificationModelList.FirstOrDefault(n => n.Channel.Equals(Enums.SubscriptionChannel.Email.GetDescription())), notificationDate));
                                    tasks.Add(SendEmailManager.SendEmailAsync(clientEmailFrom,
                                        new List<string> { customerModel.EmailAddress }, emailIdentifiers,
                                        insight.EmailTemplateId, clientEmailUsername, clientEmailPassword, $"{customKey}_{Enums.SubscriptionChannel.Email.GetDescription()}", setDate));
                                    isSent = true;

                                }
                                if (Convert.ToBoolean(programSubscription.IsSmsOptInCompleted) && !string.IsNullOrEmpty(customerModel.TrumpiaSubscriptionId))
                                {
                                    var smsBody = GetSmsBody(evaluation, calculation, billingModel, customInsightModel,
                                        insight.SmsTemplateId, clientSettings.UnmaskedAccountIdEndingDigit,
                                        uomContentList, textContentList);
                                    tasks.Add(SaveNotification(notificationModelList.FirstOrDefault(n => n.Channel.Equals(Enums.SubscriptionChannel.SMS.GetDescription())), notificationDate));
                                    //tasks.Add(sendSms.Send(smsApiKey, smsUserName, smsBody, customerModel.TrumpiaSubscriptionId, customKey, evaluation.ClientId, evaluation.AccountId, program.ProgramName, evaluation.ServiceContractId, insight.InsightName, notificationDate, setSmsDate));
                                    tasks.Add(
                                        SendSms(billingModel, subscriptions,
                                            notificationModelList.FirstOrDefault(
                                                n => n.Channel.Equals(Enums.SubscriptionChannel.SMS.GetDescription())),
                                            customerModel, $"{customKey}_{Enums.SubscriptionChannel.SMS.GetDescription()}", smsApiKey, smsUserName, smsBody, evaluation,
                                            program.ProgramName, insight.InsightName, notificationDate, tier, setSmsDate));
                                    isSent = true;
                                }
                            }
                            else if (insight.DefaultCommunicationChannel.Equals(Enums.SubscriptionChannel.File.GetDescription(), StringComparison.InvariantCultureIgnoreCase))
                            {
                                tasks.Add(SaveNotification(notificationModelList.FirstOrDefault(n => n.Channel.Equals(Enums.SubscriptionChannel.File.GetDescription())), notificationDate));
                                isSent = true;
                            }
                            await Task.WhenAll(tasks);
                            //After successful email, sms sending, update IsNotified with true
                            if (isSent)
                            {
                                foreach (var notificationModel in notificationModelList)
                                {
                                    if (!notificationModel.Channel.Equals(Enums.SubscriptionChannel.SMS.GetDescription(), StringComparison.InvariantCulture))
                                    {
                                        notificationModel.IsNotified = true;
                                        SaveNotification(notificationModel, notificationDate).Wait();
                                    }
                                }

                            }
                        }
                    }
                }
                //}
            }
        }


        /// <summary>
        /// send sms
        /// get the report status on the sms request
        /// update notification with the status
        /// </summary>
        /// <param name="billingModel"></param>
        /// <param name="subscriptions"></param>
        /// <param name="notificationModel"></param>
        /// <param name="customerModel"></param>
        /// <param name="customKey"></param>
        /// <param name="smsApiKey"></param>
        /// <param name="smsUserName"></param>
        /// <param name="smsBody"></param>
        /// <param name="insightModel"></param>
        /// <param name="programName"></param>
        /// <param name="insightTypeName"></param>
        /// <param name="requestDate"></param>
        /// <param name="tier"></param>
        /// <param name="notifyDateTime"></param>
        /// <returns></returns>
        public async Task SendSms(BillingModel billingModel, List<SubscriptionModel> subscriptions, NotificationModel notificationModel, CustomerModel customerModel, string customKey, string smsApiKey, string smsUserName, string smsBody, InsightModel insightModel, string programName, string insightTypeName, DateTime requestDate, string tier, DateTime? notifyDateTime = null)
        {
            var phone = customerModel.Phone1 ?? customerModel.Phone2;
            // send sms
            var task = SendSmsManager.Send(smsApiKey, smsUserName, smsBody, phone, customerModel.TrumpiaSubscriptionId, customKey,
                insightModel.ClientId, insightModel.AccountId, programName, insightModel.ServiceContractId,
                insightTypeName, requestDate, notifyDateTime);

            Task.WaitAll(task);

            if (task.Status == TaskStatus.RanToCompletion)
            {
                // get sms report
                notificationModel.TrumpiaRequestDetailPkRk = task.Result;

                //Task.Delay(500).Wait();

                if (!string.IsNullOrEmpty(notificationModel.TrumpiaRequestDetailPkRk))
                    await GetSmsReport(billingModel, subscriptions, notificationModel, insightModel, customerModel,
                        customerModel.TrumpiaSubscriptionId, smsApiKey, smsUserName, requestDate);
            }

        }


        public async Task GetSmsReport(BillingModel billingModel, List<SubscriptionModel> subscriptions, NotificationModel notificationModel, InsightModel insightModel, CustomerModel customerModel, string subscriptionId, string apiKey, string username, DateTime requestedDate, int? retry = 0)
        {
            var statusCode = string.Empty;
            var errorMessage = string.Empty;
            
            var bUpdate = true;
            var isScheduleUpdate = false;
            // check the status of the notification after request is sent
            if (notificationModel != null)
            {
                // sms request is sent
                notificationModel.IsNotified = true;

                // get trumpia details
                var trumpiaRequestDetailEntity = TrumpiaManager.GetTrumpiaRequestDetailAsync(notificationModel.TrumpiaRequestDetailPkRk).Result;

                // if there's a trumpia request, get status on the request
                if (trumpiaRequestDetailEntity != null)
                {
                    // get report status on the request
                    Logger.Info($"Trumpia Request Id {trumpiaRequestDetailEntity.RequestId}", LogModel);
                    var task = SendSmsManager.GetSmsStatics(trumpiaRequestDetailEntity.RequestId, apiKey, username);
                    Task.WaitAll(task);
                    var smsStats = task.Result;
                    if (smsStats == null ||
                        (string.IsNullOrEmpty(smsStats.status_code) && string.IsNullOrEmpty(smsStats.status)))
                    {
                        GetSmsReport(billingModel, subscriptions, notificationModel, insightModel, customerModel,
                            subscriptionId, apiKey, username,
                            requestedDate, retry + 1).Wait();
                        // ReSharper disable once RedundantAssignment
                        bUpdate = false;
                    }
                    else
                    {
                        if (!string.IsNullOrEmpty(smsStats.status_code))
                        {
                            statusCode = smsStats.status_code;
                            errorMessage = smsStats.error_message;
                            switch (smsStats.status_code)
                            {
                                case StatusMmsNotSupported:
                                case StatusInvalidSubscription:
                                case StatusNoRecipient:
                                    // update opt out date in subscription
                                    SubscriptionManager.InsertorMergeForSmsOptOut(billingModel, subscriptions,
                                        notificationModel.ClientId,
                                        notificationModel.CustomerId, notificationModel.AccountId,
                                        notificationModel.ServiceContractId, notificationModel.ProgramName,
                                        notificationModel.Insight);
                                    break;
                                case StatusInProgress:
                                    // if it's still in progress, get the report again
                                    GetSmsReport(billingModel, subscriptions, notificationModel, insightModel, customerModel,
                                        subscriptionId, apiKey, username,
                                        requestedDate, retry + 1).Wait();
                                    bUpdate = false;
                                    break;
                                default:
                                    notificationModel.IsDropped = true;
                                    break;
                            }
                        }
                        else if (!string.IsNullOrEmpty(smsStats.status))
                        {
                            if (smsStats.status == StatusSending)
                            {
                                //notificationModel.IsScheduled = true;
                                GetSmsReport(billingModel, subscriptions, notificationModel, insightModel, customerModel,
                                    subscriptionId, apiKey, username,
                                    requestedDate, retry + 1).Wait();
                                bUpdate = false;

                            }
                            else if (smsStats.status == StatusScheduled)
                            {
                                notificationModel.IsScheduled = true;
                            }
                            else if (smsStats.status == StatusSent)
                            {
                                if (smsStats.sms.sent > 0)
                                {
                                    notificationModel.IsDelivered = true;
                                    //Changing isScheduled from true to null
                                    notificationModel.IsScheduled = false;
                                    isScheduleUpdate = true;
                                    bUpdate = false;
                                }
                                else if (smsStats.sms.failed > 0)
                                {
                                    notificationModel.IsBounced = true;
                                    errorMessage = "Invalid number.  Check if the number is valid or blocked.";
                                    //Changing isScheduled from true to null
                                    notificationModel.IsScheduled = false;
                                    isScheduleUpdate = true;
                                    bUpdate = false;
                                }
                            }
                        }
                        else
                        {
                            errorMessage = smsStats.error_message;
                            notificationModel.IsDropped = true;
                        }
                        if (bUpdate)
                        {
                            await SaveNotification(notificationModel, requestedDate);
                            if (!string.IsNullOrEmpty(statusCode))
                                trumpiaRequestDetailEntity.StatusCode = statusCode;
                            if (!string.IsNullOrEmpty(errorMessage))
                                trumpiaRequestDetailEntity.ErrorMessage = errorMessage;

                            if (!string.IsNullOrEmpty(statusCode) || !string.IsNullOrEmpty(errorMessage))
                                await TrumpiaManager.InsertOrMergeTrumpiaRequestDetailAsync(trumpiaRequestDetailEntity);
                        }
                        if (isScheduleUpdate)
                        {
                            SaveNotification(notificationModel);
                        }
                    }


                }

            }
        }
        
        private List<NotificationModel> NotificationRequired(InsightModel evaluation, InsightSettings insightSettings, string customerId, out string tier)
        {
            List<NotificationModel> notificationModelList = null;
            tier = string.Empty;
            switch (insightSettings.InsightName)
            {
                case "BillToDate":
                    if (string.IsNullOrEmpty(evaluation.ServiceContractId))
                        notificationModelList = CreateNotification(evaluation, insightSettings, customerId, out tier);
                    break;
                case "CostToDate":
                    if (!string.IsNullOrEmpty(evaluation.ServiceContractId))
                        notificationModelList = CreateNotification(evaluation, insightSettings, customerId, out tier);
                    break;
                case "AccountProjectedCost":
                    if (string.IsNullOrEmpty(evaluation.ServiceContractId))
                        notificationModelList = CreateNotification(evaluation, insightSettings, customerId, out tier);
                    break;
                case "ServiceProjectedCost":
                    if (!string.IsNullOrEmpty(evaluation.ServiceContractId))
                        notificationModelList = CreateNotification(evaluation, insightSettings, customerId, out tier);
                    break;
                case "Usage":
                    if (!string.IsNullOrEmpty(evaluation.ServiceContractId))
                        notificationModelList = CreateNotification(evaluation, insightSettings, customerId, out tier);
                    break;
                case "AccountLevelCostThreshold":
                    if (string.IsNullOrEmpty(evaluation.ServiceContractId) && Convert.ToBoolean(evaluation.AccountLevelCostThresholdExceed))
                        notificationModelList = CreateNotification(evaluation, insightSettings, customerId, out tier);
                    break;
                case "ServiceLevelUsageThreshold":
                    if (!string.IsNullOrEmpty(evaluation.ServiceContractId) && Convert.ToBoolean(evaluation.ServiceLevelUsageThresholdExceed))
                        notificationModelList = CreateNotification(evaluation, insightSettings, customerId, out tier);
                    break;
                case "ServiceLevelCostThreshold":
                    if (!string.IsNullOrEmpty(evaluation.ServiceContractId) && Convert.ToBoolean(evaluation.ServiceLevelCostThresholdExceed))
                        notificationModelList = CreateNotification(evaluation, insightSettings, customerId, out tier);
                    break;
                case "DayThreshold":
                    if (!string.IsNullOrEmpty(evaluation.ServiceContractId) && Convert.ToBoolean(evaluation.DayThresholdExceed))
                        notificationModelList = CreateNotification(evaluation, insightSettings, customerId, out tier);
                    break;
                case "ServiceLevelTieredThresholdApproaching":
                    if (!string.IsNullOrEmpty(evaluation.ServiceContractId) && 
                        (Convert.ToBoolean(evaluation.ServiceLevelTiered1ThresholdApproaching) 
                        || Convert.ToBoolean(evaluation.ServiceLevelTiered2ThresholdApproaching) 
                        || Convert.ToBoolean(evaluation.ServiceLevelTiered3ThresholdApproaching) 
                        || Convert.ToBoolean(evaluation.ServiceLevelTiered4ThresholdApproaching) 
                        || Convert.ToBoolean(evaluation.ServiceLevelTiered5ThresholdApproaching) 
                        || Convert.ToBoolean(evaluation.ServiceLevelTiered6ThresholdApproaching)))
                        notificationModelList = CreateNotification(evaluation, insightSettings, customerId, out tier);
                    break;
                case "ServiceLevelTieredThresholdExceed":
                    if (!string.IsNullOrEmpty(evaluation.ServiceContractId) && Convert.ToBoolean(evaluation.ServiceLevelTieredThresholdExceed))
                        notificationModelList = CreateNotification(evaluation, insightSettings, customerId, out tier);
                    break;
            }
            return notificationModelList;
        }

        private List<NotificationModel> CreateNotification(InsightModel evaluation, InsightSettings insightsetting, string customerId, out string tier)
        {
            tier = string.Empty;
            if (insightsetting.InsightName.Equals("ServiceLevelTieredThresholdApproaching", StringComparison.InvariantCultureIgnoreCase))
            {
                if (evaluation.ServiceLevelTiered6ThresholdApproaching == true)
                {
                    tier = "Tier6";
                }
                else if (evaluation.ServiceLevelTiered5ThresholdApproaching == true)
                {
                    tier = "Tier5";
                }
                else if (evaluation.ServiceLevelTiered4ThresholdApproaching == true)
                {
                    tier = "Tier4";
                }
                else if (evaluation.ServiceLevelTiered3ThresholdApproaching == true)
                {
                    tier = "Tier3";
                }
                else if (evaluation.ServiceLevelTiered2ThresholdApproaching == true)
                {
                    tier = "Tier2";
                }
                else if (evaluation.ServiceLevelTiered1ThresholdApproaching == true)
                {
                    tier = "Tier1";
                }
            }
            List<NotificationModel> notificationModelList = new List<NotificationModel>();
            var notificationModel = new NotificationModel()
            {
                ClientId = evaluation.ClientId,
                AccountId = evaluation.AccountId,
                ProgramName = evaluation.ProgramName,
                Insight = insightsetting.InsightName,
                CustomerId = customerId,
                ServiceContractId = evaluation.ServiceContractId,
                IsNotified = false,
                NotifiedDateTime = DateTime.UtcNow,
                IsBounced = null,
                IsDelivered = null,
                IsDropped = null,
                IsOpened = null,
                IsClicked = null,
                EvaluationPK = $"{evaluation.ClientId}_{evaluation.AccountId}",
                EvaluationRK =
                    string.IsNullOrEmpty(evaluation.ServiceContractId)
                        ? $@"{evaluation.AsOfEvaluationDate.ToString("yyyy-MM-dd")}_A_{evaluation.ProgramName}"
                        : $"{evaluation.AsOfEvaluationDate.ToString("yyyy-MM-dd")}_S_{evaluation.ProgramName}_{evaluation.ServiceContractId}"
            };

            if (!string.IsNullOrEmpty(tier))
                notificationModel.Tier = tier;
            if (insightsetting.DefaultCommunicationChannel.Equals(Enums.SubscriptionChannel.Email.GetDescription(), StringComparison.InvariantCultureIgnoreCase))
            {
                notificationModel.Channel = Enums.SubscriptionChannel.Email.GetDescription();
                notificationModel.TemplateId = insightsetting.EmailTemplateId;
            }
            if (insightsetting.DefaultCommunicationChannel.Equals(Enums.SubscriptionChannel.SMS.GetDescription(), StringComparison.InvariantCultureIgnoreCase))
            {
                notificationModel.Channel = Enums.SubscriptionChannel.SMS.GetDescription();
                notificationModel.TemplateId = insightsetting.SmsTemplateId;
            }

            if (insightsetting.DefaultCommunicationChannel.Equals(Enums.SubscriptionChannel.EmailAndSMS.GetDescription(), StringComparison.InvariantCultureIgnoreCase))
            {
                notificationModel.Channel = Enums.SubscriptionChannel.Email.GetDescription();
                notificationModel.TemplateId = insightsetting.EmailTemplateId;

                var notificationModelSms = new NotificationModel();

                foreach (var prop in notificationModel.GetType().GetProperties())
                {
                    notificationModelSms.GetType().GetProperty(prop.Name).SetValue(notificationModelSms, prop.GetValue(notificationModel));
                }
                notificationModelSms.Channel = Enums.SubscriptionChannel.SMS.GetDescription();
                notificationModelSms.TemplateId = insightsetting.SmsTemplateId;

                notificationModelList.Add(notificationModelSms);
            }
            if (insightsetting.DefaultCommunicationChannel.Equals(Enums.SubscriptionChannel.File.GetDescription()))
                notificationModel.Channel = Enums.SubscriptionChannel.File.GetDescription();
            notificationModelList.Add(notificationModel);
            return notificationModelList;
        }

        private bool IsNotified(InsightModel evaluation, InsightSettings insightSettings, string tier)
        {
            var notification = GetNotification(evaluation, insightSettings).Result;
            if (notification != null)
            {
                if (insightSettings.DefaultFrequency.Equals(Enums.NotificationFrequency.Daily.GetDescription()) &&
                    notification.IsNotified != true)
                    return true;
                else if (insightSettings.DefaultFrequency.Equals(Enums.NotificationFrequency.Daily.GetDescription()) &&
                         DateTime.UtcNow.Date > notification.NotifiedDateTime.Date)
                    return true;
                if (insightSettings.DefaultFrequency.Equals(Enums.NotificationFrequency.Weekly.GetDescription()) &&
                    insightSettings.NotificationDay.Equals(DateTime.UtcNow.DayOfWeek.ToString()) &&
                    notification.IsNotified != true)
                    return true;
                else if (insightSettings.DefaultFrequency.Equals(Enums.NotificationFrequency.Weekly.GetDescription()) &&
                         insightSettings.NotificationDay.Equals(DateTime.UtcNow.DayOfWeek.ToString()) &&
                         DateTime.UtcNow.AddDays(-7).Date > notification.NotifiedDateTime.Date)
                    return true;
                if (insightSettings.DefaultFrequency.Equals(Enums.NotificationFrequency.Monthly.GetDescription()) &&
                    notification.IsNotified != true)
                    return true;
                else if (insightSettings.DefaultFrequency.Equals(Enums.NotificationFrequency.Monthly.GetDescription()) &&
                         DateTime.UtcNow.AddMonths(-1).Date > notification.NotifiedDateTime.Date)
                    return true;
                if (insightSettings.DefaultFrequency.Equals(Enums.NotificationFrequency.Immediate.GetDescription()) &&
                    Convert.ToDateTime(evaluation.BillCycleStartDate).Date <= notification.NotifiedDateTime.Date &&
                    notification.NotifiedDateTime.Date <= Convert.ToDateTime(evaluation.BillCycleEndDate).Date)
                {
                    if (notification.IsNotified == true)
                    {
                        if (notification.Insight.Equals("ServiceLevelTieredThresholdApproaching",
                            StringComparison.InvariantCultureIgnoreCase))
                        {
                            if (tier.Equals(notification.Tier, StringComparison.InvariantCultureIgnoreCase))
                            {
                                return false;
                            }
                            else return true;
                        }
                        else
                        {
                            return false;
                        }
                    }
                    else
                        return true;
                }
                return false;
            }
            return true;
        }

        private async Task<NotificationModel> GetNotification(InsightModel evaluation, InsightSettings insightSettings)
        {
            NotificationEntity notification;
            if (
                insightSettings.DefaultCommunicationChannel.Equals(
                    Enums.SubscriptionChannel.EmailAndSMS.GetDescription(), StringComparison.CurrentCultureIgnoreCase))
            {
                notification = await TableRepository.GetSingleAsync<NotificationEntity>(Constants.TableNames.Notification,
                NotificationEntity.SetNotificationPartitionKey(evaluation.ClientId, evaluation.AccountId), NotificationEntity.SetNotificationRowKey(evaluation.ProgramName, evaluation.ServiceContractId, insightSettings.InsightName, Enums.SubscriptionChannel.Email.GetDescription(), false));
                if (notification == null || (notification.IsNotified.HasValue && !notification.IsNotified.Value))
                {
                    notification = await TableRepository.GetSingleAsync<NotificationEntity>(Constants.TableNames.Notification,
                 NotificationEntity.SetNotificationPartitionKey(evaluation.ClientId, evaluation.AccountId), NotificationEntity.SetNotificationRowKey(evaluation.ProgramName, evaluation.ServiceContractId, insightSettings.InsightName, Enums.SubscriptionChannel.SMS.GetDescription(), false));
                }
            }
            else
            {
                notification = await TableRepository.GetSingleAsync<NotificationEntity>(Constants.TableNames.Notification,
                NotificationEntity.SetNotificationPartitionKey(evaluation.ClientId, evaluation.AccountId), NotificationEntity.SetNotificationRowKey(evaluation.ProgramName, evaluation.ServiceContractId, insightSettings.InsightName, insightSettings.DefaultCommunicationChannel, false));
            }
            return Mapper.Map<NotificationEntity, NotificationModel>(notification);
        }

        private async Task SaveNotification(NotificationModel notificationModel, DateTime asOfDate)
        {
            var entityModel = Mapper.Map<NotificationModel, NotificationEntity>(notificationModel);
            entityModel.PartitionKey = NotificationEntity.SetNotificationPartitionKey(entityModel.ClientId, entityModel.AccountId);
            entityModel.RowKey = NotificationEntity.SetNotificationRowKey(entityModel.ProgramName, entityModel.ServiceContractId, entityModel.Insight, entityModel.Channel, false);

            await TableRepository.InsertOrMergeSingleAsync(Constants.TableNames.Notification, entityModel);
            //insert History Record
            //var date = DateTime.UtcNow;
            entityModel.RowKey = NotificationEntity.SetNotificationRowKey(entityModel.ProgramName, entityModel.ServiceContractId, entityModel.Insight, entityModel.Channel, true, asOfDate);
            await TableRepository.InsertOrMergeSingleAsync(Constants.TableNames.Notification, entityModel);
        }

        private Dictionary<string, List<string>> GetEmailSubstitutions(InsightModel evaluation, CustomInsightModel customInsightModel,
            CustomerModel customerModel, BillingModel billingModel, List<TierBoundary> tierBoundaries, CalculationModel calculation, int unmaskedAccountIdEndingDigit, List<ClientUOM> uomContentList, List<ClientTextContent> textContentList)
        {
            Dictionary<string, List<string>> identifiers = new Dictionary<string, List<string>>();
            switch (customInsightModel.InsightName)
            {
                case "BillToDate":
                    if (customInsightModel.IsAccountLevel)
                        identifiers = GetBillToDateEmailSubstitutions(customerModel, calculation, calculation.AsOfAmiDate.HasValue ? calculation.AsOfAmiDate.Value : calculation.AsOfCalculationDate, unmaskedAccountIdEndingDigit);
                    break;
                case "CostToDate":
                    if (!customInsightModel.IsAccountLevel)
                        identifiers = GetCostToDateEmailSubstitutions(customerModel, calculation, billingModel, calculation.AsOfAmiDate.HasValue ? calculation.AsOfAmiDate.Value : calculation.AsOfCalculationDate, unmaskedAccountIdEndingDigit);
                    break;
                case "AccountProjectedCost":
                    if (customInsightModel.IsAccountLevel & evaluation.BtdProjectedCost != null)
                        identifiers = GetBillToDateEmailSubstitutions(customerModel, calculation, calculation.AsOfAmiDate.HasValue ? calculation.AsOfAmiDate.Value : calculation.AsOfCalculationDate, unmaskedAccountIdEndingDigit);
                    break;
                case "ServiceProjectedCost":
                    if (!customInsightModel.IsAccountLevel & evaluation.CtdProjectedCost != null)
                        identifiers = GetCostToDateEmailSubstitutions(customerModel, calculation, billingModel, calculation.AsOfAmiDate.HasValue ? calculation.AsOfAmiDate.Value : calculation.AsOfCalculationDate, unmaskedAccountIdEndingDigit);
                    break;
                case "Usage":
                    if (!customInsightModel.IsAccountLevel & evaluation.Usage != null)
                        identifiers = GetUsageEmailSubstitutions(customerModel, calculation, billingModel,
                            calculation.AsOfAmiDate.HasValue ? calculation.AsOfAmiDate.Value : calculation.AsOfCalculationDate, unmaskedAccountIdEndingDigit, uomContentList, textContentList);
                    break;
                case "AccountLevelCostThreshold":
                    if (customInsightModel.IsAccountLevel & Convert.ToBoolean(evaluation.AccountLevelCostThresholdExceed))
                        identifiers = GetAccountLevelCostThresholdEmailSubstitutions(customerModel, calculation, customInsightModel, calculation.AsOfAmiDate.HasValue ? calculation.AsOfAmiDate.Value : calculation.AsOfCalculationDate, unmaskedAccountIdEndingDigit);
                    break;
                case "ServiceLevelUsageThreshold":
                    if (!customInsightModel.IsAccountLevel &
                        Convert.ToBoolean(evaluation.ServiceLevelUsageThresholdExceed))
                        identifiers = GetServiceLevelUsageThresholdEmailSubstitutions(customerModel, calculation,
                            customInsightModel, billingModel, unmaskedAccountIdEndingDigit, uomContentList,
                            textContentList);
                    break;
                case "ServiceLevelCostThreshold":
                    if (!customInsightModel.IsAccountLevel &
                        Convert.ToBoolean(evaluation.ServiceLevelCostThresholdExceed))
                        identifiers = GetServiceLevelCostThresholdEmailSubstitutions(customerModel, calculation,
                            customInsightModel, billingModel, unmaskedAccountIdEndingDigit, uomContentList,
                            textContentList);
                    break;
                case "DayThreshold":
                    if (!customInsightModel.IsAccountLevel & Convert.ToBoolean(evaluation.DayThresholdExceed))
                        identifiers = GetDayThresholdEmailSubstitutions(customerModel, calculation, customInsightModel,
                            billingModel, calculation.AsOfAmiDate.HasValue ? calculation.AsOfAmiDate.Value : calculation.AsOfCalculationDate, unmaskedAccountIdEndingDigit, uomContentList,
                            textContentList);
                    break;
                case "ServiceLevelTieredThresholdApproaching":
                    if (!customInsightModel.IsAccountLevel &
                        (Convert.ToBoolean(evaluation.ServiceLevelTiered1ThresholdApproaching)
                        || Convert.ToBoolean(evaluation.ServiceLevelTiered2ThresholdApproaching)
                        || Convert.ToBoolean(evaluation.ServiceLevelTiered3ThresholdApproaching)
                        || Convert.ToBoolean(evaluation.ServiceLevelTiered4ThresholdApproaching)
                        || Convert.ToBoolean(evaluation.ServiceLevelTiered5ThresholdApproaching)
                        || Convert.ToBoolean(evaluation.ServiceLevelTiered6ThresholdApproaching)))
                        identifiers = GetServiceLevelTieredThresholdApproachingEmailSubstitutions(customerModel,
                            calculation, billingModel, tierBoundaries, calculation.AsOfAmiDate.HasValue ? calculation.AsOfAmiDate.Value : calculation.AsOfCalculationDate,
                            unmaskedAccountIdEndingDigit, uomContentList, textContentList);
                    break;
                case "ServiceLevelTieredThresholdExceed":
                    if (!customInsightModel.IsAccountLevel &
                        Convert.ToBoolean(evaluation.ServiceLevelTieredThresholdExceed))
                        identifiers = GetServiceLevelTieredThresholdExceedEmailSubstitutions(customerModel, calculation,
                            billingModel, tierBoundaries, calculation.AsOfAmiDate.HasValue ? calculation.AsOfAmiDate.Value : calculation.AsOfCalculationDate, unmaskedAccountIdEndingDigit,
                            uomContentList, textContentList);
                    break;
            }
            return identifiers;
        }

        private Dictionary<string, List<string>> GetBillToDateEmailSubstitutions(CustomerModel customerModel, CalculationModel calculation, DateTime asOfDate, int unmaskedAccountIdEndingDigit)
        {
            var accountNumber = calculation.AccountId;
            if (unmaskedAccountIdEndingDigit > 0)
            {
                accountNumber = Extensions.GetMaskedAccountId(accountNumber, unmaskedAccountIdEndingDigit);
            }
            var address = string.Empty;

            if (customerModel != null)
            {
                if (!string.IsNullOrEmpty(customerModel.AddressLine1))
                    address = address + customerModel.AddressLine1;

                if (!string.IsNullOrEmpty(customerModel.AddressLine2))
                {
                    address = !string.IsNullOrEmpty(address)
                        ? address + ", " + customerModel.AddressLine2
                        : address + customerModel.AddressLine2;
                }

                if (!string.IsNullOrEmpty(customerModel.AddressLine3))
                {
                    address = !string.IsNullOrEmpty(address)
                        ? address + ", " + customerModel.AddressLine3
                        : address + customerModel.AddressLine3;
                }
            }

            Dictionary<string, List<string>> identifiers = new Dictionary<string, List<string>>
            {
                {"-FirstName-", new List<string> {customerModel != null ? customerModel.FirstName : ""}},
                {"-LastName-", new List<string> {customerModel != null ? customerModel.LastName : ""}},
                {"-AsOfDate-", new List<string> {asOfDate.ToShortDateString()}},
                {"-AccountNumber-", new List<string> {accountNumber}},
                {"-AccountAddress-", new List<string>{address}},
                {
                    "-DaysIntoBillingCycle-", new List<string>
                    {
                        ((asOfDate.Date - Convert.ToDateTime(calculation.BillCycleStartDate).Date).Days + 1)
                            .ToString()
                    }
                },
                {"-ProjectedCost-", new List<string> {$"{calculation.ProjectedCost:0.00}"}},
                {"-BillToDate-", new List<string> {$"{calculation.Cost:0.00}"}}
            };
            return identifiers;
        }

        private Dictionary<string, List<string>> GetCostToDateEmailSubstitutions(CustomerModel customerModel, CalculationModel calculation, BillingModel billingModel, DateTime asOfDate, int unmaskedAccountIdEndingDigit)
        {
            var commodity = Enums.CommodityType.Undefined;
            if (calculation.CommodityId != null)
            {
                commodity = (Enums.CommodityType)calculation.CommodityId;
            }
            var address = string.Empty;

            if (customerModel != null)
            {
                if (!string.IsNullOrEmpty(customerModel.AddressLine1))
                    address = address + customerModel.AddressLine1;

                if (!string.IsNullOrEmpty(customerModel.AddressLine2))
                {
                    address = !string.IsNullOrEmpty(address)
                        ? address + ", " + customerModel.AddressLine2
                        : address + customerModel.AddressLine2;
                }

                if (!string.IsNullOrEmpty(customerModel.AddressLine3))
                {
                    address = !string.IsNullOrEmpty(address)
                        ? address + ", " + customerModel.AddressLine3
                        : address + customerModel.AddressLine3;
                }
            }


            var accountNumber = calculation.AccountId;
            if (unmaskedAccountIdEndingDigit > 0)
            {
                accountNumber = Extensions.GetMaskedAccountId(accountNumber, unmaskedAccountIdEndingDigit);
            }
            Dictionary<string, List<string>> identifiers = new Dictionary<string, List<string>>
            {
                {"-FirstName-", new List<string> {customerModel != null ? customerModel.FirstName : ""}},
                {"-LastName-", new List<string> {customerModel != null ? customerModel.LastName : ""}},
                {"-AsOfDate-", new List<string> {asOfDate.ToShortDateString()}},
                {"-AccountNumber-", new List<string> {accountNumber}},
                {"-MeterNumber-", new List<string> {billingModel != null ? billingModel.MeterId : ""}},
                {"-ServiceAddress-", new List<string>{address}},
                {
                    "-DaysIntoBillingCycle-", new List<string>
                    {
                        ((asOfDate.Date - Convert.ToDateTime(calculation.BillCycleStartDate).Date).Days + 1)
                            .ToString()
                    }
                },
                {"-ProjectedCostToDate-", new List<string> {$"{calculation.ProjectedCost:0.00}"}},
                {"-CostToDate-", new List<string> {$"{calculation.Cost:0.00}"}},
                {"-CommodityType-", new List<string> {commodity.GetDescription()}}
            };
            return identifiers;
        }

        private Dictionary<string, List<string>> GetUsageEmailSubstitutions(CustomerModel customerModel, CalculationModel calculation, BillingModel billingModel, DateTime asOfDate, int unmaskedAccountIdEndingDigit, List<ClientUOM> uomContentList, List<ClientTextContent> textContentList)
        {
            var commodity = Enums.CommodityType.Undefined;
            if (calculation.CommodityId != null)
            {
                commodity = (Enums.CommodityType)calculation.CommodityId;
            }

            var unit = billingModel?.UOMId ?? calculation.Unit;
            var uomId = 99;
            if (unit.HasValue)
            {
                uomId = unit.Value;
            }
            // if the calcution is done in a different UOM from bill's
            // convert the usage into bill's UOM for the notification
            double? usage;

            if (unit.HasValue && calculation.Unit.HasValue && unit != calculation.Unit)
            {
                var conversionFactor = Extensions.GetConversionFactor(customerModel.ClientId, calculation.Unit.Value,
                    unit.Value);
                usage = calculation.Usage * conversionFactor;
            }
            else
            {
                usage = calculation.Usage;
            }
            var address = string.Empty;

            if (customerModel != null)
            {
                if (!string.IsNullOrEmpty(customerModel.AddressLine1))
                    address = address + customerModel.AddressLine1;

                if (!string.IsNullOrEmpty(customerModel.AddressLine2))
                {
                    address = !string.IsNullOrEmpty(address)
                        ? address + ", " + customerModel.AddressLine2
                        : address + customerModel.AddressLine2;
                }

                if (!string.IsNullOrEmpty(customerModel.AddressLine3))
                {
                    address = !string.IsNullOrEmpty(address)
                        ? address + ", " + customerModel.AddressLine3
                        : address + customerModel.AddressLine3;
                }
            }

            // get uom content
            var uomKey = ((Enums.UomType)uomId).ToString();
            var uom = Extensions.GetUomTextContent(uomKey, uomContentList, textContentList);
            if (string.IsNullOrEmpty(uom))
                uom = uomKey;

            var accountNumber = calculation.AccountId;
            if (unmaskedAccountIdEndingDigit > 0)
            {
                accountNumber = Extensions.GetMaskedAccountId(accountNumber, unmaskedAccountIdEndingDigit);
            }
            Dictionary<string, List<string>> identifiers = new Dictionary<string, List<string>>
            {
                {"-FirstName-", new List<string> {customerModel != null ? customerModel.FirstName : ""}},
                {"-LastName-", new List<string> {customerModel != null ? customerModel.LastName : ""}},
                {"-AsOfDate-", new List<string> {asOfDate.ToShortDateString()}},
                {"-AccountNumber-", new List<string> {accountNumber}},
                {"-MeterNumber-", new List<string> {billingModel != null ? billingModel.MeterId : ""}},
                {"-ServiceAddress-", new List<string>{address}},
                {
                    "-DaysIntoBillingCycle-", new List<string>
                    {
                        ((asOfDate.Date - Convert.ToDateTime(calculation.BillCycleStartDate).Date).Days + 1)
                            .ToString()
                    }
                },
                {"-ProjectedCostToDate-", new List<string> {$"{calculation.ProjectedCost:0.00}"}},
                {"-UsageToDate-", new List<string> {$"{usage:0.00}"}},
                {"-UOM-", new List<string> {uom}},
                {"-CommodityType-", new List<string> { commodity.GetDescription()}}
            };
            return identifiers;
        }

        private Dictionary<string, List<string>> GetAccountLevelCostThresholdEmailSubstitutions(CustomerModel customerModel, CalculationModel calculation, CustomInsightModel customInsightModel, DateTime asOfDate, int unmaskedAccountIdEndingDigit)
        {
            var accountNumber = calculation.AccountId;
            if (unmaskedAccountIdEndingDigit > 0)
            {
                accountNumber = Extensions.GetMaskedAccountId(accountNumber, unmaskedAccountIdEndingDigit);
            }
            var address = string.Empty;

            if (customerModel != null)
            {
                if (!string.IsNullOrEmpty(customerModel.AddressLine1))
                    address = address + customerModel.AddressLine1;

                if (!string.IsNullOrEmpty(customerModel.AddressLine2))
                {
                    address = !string.IsNullOrEmpty(address)
                        ? address + ", " + customerModel.AddressLine2
                        : address + customerModel.AddressLine2;
                }

                if (!string.IsNullOrEmpty(customerModel.AddressLine3))
                {
                    address = !string.IsNullOrEmpty(address)
                        ? address + ", " + customerModel.AddressLine3
                        : address + customerModel.AddressLine3;
                }
            }
            Dictionary<string, List<string>> identifiers = new Dictionary<string, List<string>>
            {
                {"-FirstName-", new List<string> {customerModel != null ? customerModel.FirstName : ""}},
                {"-LastName-", new List<string> {customerModel != null ? customerModel.LastName : ""}},
                {"-AsOfDate-", new List<string> {asOfDate.ToShortDateString()}},
                {"-AccountNumber-", new List<string> {accountNumber}},
                {"-AccountAddress-", new List<string> {address}},
                {"-AccountCostThreshold-", new List<string> {$"{customInsightModel.ThresholdValue:0.00}"}},
                {"-BTDCost-", new List<string> {$"{calculation.Cost:0.00}"}},
                {
                    "-DaysIntoBillingCycle-", new List<string>
                    {
                        ((asOfDate.Date - Convert.ToDateTime(calculation.BillCycleStartDate).Date).Days + 1)
                            .ToString()
                    }
                },
                {"-ProjectedCost-", new List<string> {$"{calculation.ProjectedCost:0.00}"}}
            };
            return identifiers;
        }

        private Dictionary<string, List<string>> GetServiceLevelUsageThresholdEmailSubstitutions(CustomerModel customerModel, CalculationModel calculation, CustomInsightModel customInsightModel, BillingModel billingModel, int unmaskedAccountIdEndingDigit, List<ClientUOM> uomContentList, List<ClientTextContent> textContentList  )
        {
            var commodity = Enums.CommodityType.Undefined;
            if (calculation.CommodityId != null)
            {
                commodity = (Enums.CommodityType)calculation.CommodityId;
            }

            var unit = billingModel?.UOMId ?? calculation.Unit;
            var uomId = 99;
            if (unit.HasValue)
            {
                uomId = unit.Value;
            }

            // if the calcution is done in a different UOM from bill's
            // convert the usage into bill's UOM for the notification
            double? usage;
            if (unit.HasValue && calculation.Unit.HasValue && unit != calculation.Unit)
            {
                var conversionFactor = Extensions.GetConversionFactor(customerModel.ClientId, calculation.Unit.Value,
                    unit.Value);
                usage = calculation.Usage * conversionFactor;
            }
            else
            {
                usage = calculation.Usage;
            }
            var address = string.Empty;

            if (customerModel != null)
            {
                if (!string.IsNullOrEmpty(customerModel.AddressLine1))
                    address = address + customerModel.AddressLine1;

                if (!string.IsNullOrEmpty(customerModel.AddressLine2))
                {
                    address = !string.IsNullOrEmpty(address)
                        ? address + ", " + customerModel.AddressLine2
                        : address + customerModel.AddressLine2;
                }

                if (!string.IsNullOrEmpty(customerModel.AddressLine3))
                {
                    address = !string.IsNullOrEmpty(address)
                        ? address + ", " + customerModel.AddressLine3
                        : address + customerModel.AddressLine3;
                }
            }

            var accountNumber = calculation.AccountId;
            if (unmaskedAccountIdEndingDigit > 0)
            {
                accountNumber = Extensions.GetMaskedAccountId(accountNumber, unmaskedAccountIdEndingDigit);
            }

            // get uom content
            var uomKey = ((Enums.UomType)uomId).ToString();
            var uom = Extensions.GetUomTextContent(uomKey, uomContentList, textContentList);
            if (string.IsNullOrEmpty(uom))
                uom = uomKey;
            var asofDate = calculation.AsOfAmiDate.HasValue
                ? calculation.AsOfAmiDate.Value
                : calculation.AsOfCalculationDate;
            Dictionary<string, List<string>> identifiers = new Dictionary<string, List<string>>
            {
                {"-FirstName-", new List<string> {customerModel != null ? customerModel.FirstName : ""}},
                {"-LastName-", new List<string> {customerModel != null ? customerModel.LastName : ""}},
                {"-AsOfDate-", new List<string> { asofDate.ToShortDateString()}},
                {"-AccountNumber-", new List<string> {accountNumber}},
                {"-MeterNumber-", new List<string> {billingModel != null ? billingModel.MeterId : ""}},
                {"-ServiceAddress-", new List<string>{address}},
                {"-UsageThreshold-", new List<string> {$"{customInsightModel.ThresholdValue:0.00}"}},
                {"-UOM-", new List<string> {uom}},
                {"-CostToDate-", new List<string> {$"{calculation.Cost:0.00}"}},
                {
                    "-DaysIntoBillingCycle-", new List<string>
                    {
                        ((asofDate.Date - Convert.ToDateTime(calculation.BillCycleStartDate).Date).Days + 1)
                            .ToString()
                    }
                },
                {"-UsageToDate-", new List<string> {$"{usage:0.00}"}},
                {"-CommodityType-", new List<string> {commodity.GetDescription()}}
            };
            return identifiers;
        }

        private Dictionary<string, List<string>> GetServiceLevelCostThresholdEmailSubstitutions(CustomerModel customerModel, CalculationModel calculation, CustomInsightModel customInsightModel, BillingModel billingModel, int unmaskedAccountIdEndingDigit, List<ClientUOM> uomContentList, List<ClientTextContent> textContentList  )
        {
            var commodity = Enums.CommodityType.Undefined;
            if (calculation.CommodityId != null)
            {
                commodity = (Enums.CommodityType)calculation.CommodityId;
            }

            var unit = billingModel?.UOMId ?? calculation.Unit;
            var uomId = 99;
            if (unit.HasValue)
            {
                uomId = unit.Value;
            }

            // if the calcution is done in a different UOM from bill's
            // convert the usage into bill's UOM for the notification
            double? usage;
            if (unit.HasValue && calculation.Unit.HasValue && unit != calculation.Unit)
            {
                var conversionFactor = Extensions.GetConversionFactor(customerModel.ClientId, calculation.Unit.Value,
                    unit.Value);
                usage = calculation.Usage * conversionFactor;
            }
            else
            {
                usage = calculation.Usage;
            }
            var address = string.Empty;

            if (customerModel != null)
            {
                if (!string.IsNullOrEmpty(customerModel.AddressLine1))
                    address = address + customerModel.AddressLine1;

                if (!string.IsNullOrEmpty(customerModel.AddressLine2))
                {
                    address = !string.IsNullOrEmpty(address)
                        ? address + ", " + customerModel.AddressLine2
                        : address + customerModel.AddressLine2;
                }

                if (!string.IsNullOrEmpty(customerModel.AddressLine3))
                {
                    address = !string.IsNullOrEmpty(address)
                        ? address + ", " + customerModel.AddressLine3
                        : address + customerModel.AddressLine3;
                }
            }

            var accountNumber = calculation.AccountId;
            if (unmaskedAccountIdEndingDigit > 0)
            {
                accountNumber = Extensions.GetMaskedAccountId(accountNumber, unmaskedAccountIdEndingDigit);
            }
            // get uom content
            var uomKey = ((Enums.UomType)uomId).ToString();
            var uom = Extensions.GetUomTextContent(uomKey, uomContentList, textContentList);
            if (string.IsNullOrEmpty(uom))
                uom = uomKey;
            var asOfDate = calculation.AsOfAmiDate.HasValue
                ? calculation.AsOfAmiDate.Value
                : calculation.AsOfCalculationDate;
            Dictionary<string, List<string>> identifiers = new Dictionary<string, List<string>>
            {
                {"-FirstName-", new List<string> {customerModel != null ? customerModel.FirstName : ""}},
                {"-LastName-", new List<string> {customerModel != null ? customerModel.LastName : ""}},
                {"-AsOfDate-", new List<string> { asOfDate.ToShortDateString()}},
                {"-AccountNumber-", new List<string> {accountNumber}},
                {"-MeterNumber-", new List<string> {billingModel != null ? billingModel.MeterId : ""}},
                {"-ServiceAddress-", new List<string>{address}},
                {"-CostThreshold-", new List<string> {$"{customInsightModel.ThresholdValue:0.00}"}},
                {"-CostToDate-", new List<string> {$"{calculation.Cost:0.00}"}},
                {
                    "-DaysIntoBillingCycle-", new List<string>
                    {
                        ((asOfDate.Date - Convert.ToDateTime(calculation.BillCycleStartDate).Date).Days + 1)
                            .ToString()
                    }
                },
                {"-UsageToDate-", new List<string> {$"{usage:0.00}"}},
                {"-UOM-", new List<string> {uom}},
                {"-CommodityType-", new List<string> {commodity.GetDescription()}}
            };
            return identifiers;
        }

        private Dictionary<string, List<string>> GetDayThresholdEmailSubstitutions(CustomerModel customerModel, CalculationModel calculation, CustomInsightModel customInsightModel, BillingModel billingModel, DateTime asOfDate, int unmaskedAccountIdEndingDigit, List<ClientUOM> uomContentList, List<ClientTextContent> textContentList)
        {
            var commodity = Enums.CommodityType.Undefined;
            if (calculation.CommodityId != null)
            {
                commodity = (Enums.CommodityType)calculation.CommodityId;
            }
            var unit = billingModel?.UOMId ?? calculation.Unit;
            var uomId = 99;
            if (unit.HasValue)
            {
                uomId = unit.Value;
            }
            var address = string.Empty;

            if (customerModel != null)
            {
                if (!string.IsNullOrEmpty(customerModel.AddressLine1))
                    address = address + customerModel.AddressLine1;

                if (!string.IsNullOrEmpty(customerModel.AddressLine2))
                {
                    address = !string.IsNullOrEmpty(address)
                        ? address + ", " + customerModel.AddressLine2
                        : address + customerModel.AddressLine2;
                }

                if (!string.IsNullOrEmpty(customerModel.AddressLine3))
                {
                    address = !string.IsNullOrEmpty(address)
                        ? address + ", " + customerModel.AddressLine3
                        : address + customerModel.AddressLine3;
                }
            }

            var accountNumber = calculation.AccountId;
            if (unmaskedAccountIdEndingDigit > 0)
            {
                accountNumber = Extensions.GetMaskedAccountId(accountNumber, unmaskedAccountIdEndingDigit);
            }

            // get uom content
            var uomKey = ((Enums.UomType)uomId).ToString();
            var uom = Extensions.GetUomTextContent(uomKey, uomContentList, textContentList);
            if (string.IsNullOrEmpty(uom))
                uom = uomKey;
            Dictionary<string, List<string>> identifiers = new Dictionary<string, List<string>>
            {
                {"-FirstName-", new List<string> {customerModel != null ? customerModel.FirstName : ""}},
                {"-LastName-", new List<string> {customerModel != null ? customerModel.LastName : ""}},
                {"-AsOfDate-", new List<string> {asOfDate.ToShortDateString()}},
                {"-AccountNumber-", new List<string> {accountNumber}},
                {"-MeterNumber-", new List<string> {billingModel != null ? billingModel.MeterId : ""}},
                {"-ServiceAddress-", new List<string>{address}},
                {"-DayThreshold-", new List<string> {$"{customInsightModel.ThresholdValue:0.00}"}},
                {"-CommodityType-", new List<string> {commodity.GetDescription()}},
                {"-UOM-", new List<string> {uom}}
            };
            return identifiers;
        }

        private Dictionary<string, List<string>> GetServiceLevelTieredThresholdApproachingEmailSubstitutions(CustomerModel customerModel, CalculationModel calculation, BillingModel billingModel, List<TierBoundary> tierBoundaries, DateTime asOfDate, int unmaskedAccountIdEndingDigit, List<ClientUOM> uomContentList, List<ClientTextContent> textContentList)
        {

            var accountNumber = calculation.AccountId;
            if (unmaskedAccountIdEndingDigit > 0)
            {
                accountNumber = Extensions.GetMaskedAccountId(accountNumber, unmaskedAccountIdEndingDigit);
            }

            Dictionary<string, List<string>> identifiers = new Dictionary<string, List<string>>();
            var tieredBoundryApproching =
                tierBoundaries.OrderBy(p => p.Threshold)
                    .ToList()
                    .FirstOrDefault(p => p.Threshold > calculation.Usage);
            var nextTieredBountryApproching =
                tierBoundaries.OrderBy(p => p.Threshold)
                    .ToList()
                    .FirstOrDefault(p => p.Threshold > tieredBoundryApproching?.Threshold);

            var commodity = Enums.CommodityType.Undefined;
            if (calculation.CommodityId != null)
            {
                commodity = (Enums.CommodityType)calculation.CommodityId;
            }

            var unit = billingModel?.UOMId ?? calculation.Unit;
            var uomId = 99;
            if (unit.HasValue)
            {
                uomId = unit.Value;
            }
            // if the calcution is done in a different UOM from bill's
            // convert the threshold into bill's UOM for the notification
            double? currentUsageThreshold;
            double? nextTierUsage;
            double? currentUsage;
            double? avgDailyUsage;
            if (unit.HasValue && calculation.Unit.HasValue && unit != calculation.Unit)
            {
                var conversionFactor = Extensions.GetConversionFactor(customerModel.ClientId, calculation.Unit.Value,
                    unit.Value);
                currentUsageThreshold = tieredBoundryApproching?.Threshold * conversionFactor;
                nextTierUsage = nextTieredBountryApproching?.Threshold * conversionFactor;
                currentUsage = calculation.Usage * conversionFactor;
                avgDailyUsage = calculation.AverageDailyUsage * conversionFactor;
            }
            else
            {
                currentUsageThreshold = tieredBoundryApproching?.Threshold;
                nextTierUsage = nextTieredBountryApproching?.Threshold;
                currentUsage = calculation.Usage;
                avgDailyUsage = calculation.AverageDailyUsage;
            }
            var address = string.Empty;

            if (customerModel != null)
            {
                if (!string.IsNullOrEmpty(customerModel.AddressLine1))
                    address = address + customerModel.AddressLine1;

                if (!string.IsNullOrEmpty(customerModel.AddressLine2))
                {
                    address = !string.IsNullOrEmpty(address)
                        ? address + ", " + customerModel.AddressLine2
                        : address + customerModel.AddressLine2;
                }

                if (!string.IsNullOrEmpty(customerModel.AddressLine3))
                {
                    address = !string.IsNullOrEmpty(address)
                        ? address + ", " + customerModel.AddressLine3
                        : address + customerModel.AddressLine3;
                }
            }

            // get uom content
            var uomKey = ((Enums.UomType)uomId).ToString();
            var uom = Extensions.GetUomTextContent(uomKey, uomContentList, textContentList);
            if (string.IsNullOrEmpty(uom))
                uom = uomKey;
            identifiers.Add("-FirstName-",
                new List<string>
                {
                    customerModel != null ? customerModel.FirstName : ""
                });
            identifiers.Add("-LastName-", new List<string> {customerModel != null ? customerModel.LastName : ""});
            identifiers.Add("-AsOfDate-", new List<string> { asOfDate.ToShortDateString() });
            identifiers.Add("-NumberOfDays-",
                new List<string>
                {
                    ((asOfDate.Date - Convert.ToDateTime(calculation.BillCycleStartDate).Date).Days + 1).ToString()
                });
            identifiers.Add("-UsagePerDay-", new List<string> { $"{avgDailyUsage:0.00}" });
            identifiers.Add("-AccountNumber-", new List<string> { accountNumber });
            identifiers.Add("-AccountAddress-", new List<string> { address });
            identifiers.Add("-CurrentUsage-", new List<string> { $"{currentUsage:0.00}" });
            identifiers.Add("-UOM-", new List<string> { uom });
            identifiers.Add("-CurrentUsageThreshold-", new List<string> { $"{currentUsageThreshold ?? 0:0.00}" });   //If null then set to 0
            identifiers.Add("-NextTierUsage-", new List<string> { $"{nextTierUsage ?? 0:0.00}" });
            identifiers.Add("-CommodityType-",
                new List<string> { commodity.GetDescription() });
            return identifiers;
        }

        private Dictionary<string, List<string>> GetServiceLevelTieredThresholdExceedEmailSubstitutions(CustomerModel customerModel, CalculationModel calculation, BillingModel billingModel, List<TierBoundary> tierBoundaries, DateTime asOfDate, int unmaskedAccountIdEndingDigit, List<ClientUOM> uomContentList, List<ClientTextContent> textContentList)
        {
            Dictionary<string, List<string>> identifiers = new Dictionary<string, List<string>>();
            var tieredBoundryExceeding =
                tierBoundaries.OrderByDescending(p => p.Threshold)
                    .ToList()
                    .FirstOrDefault(p => p.Threshold < calculation.Usage);
            var nextTieredBoundryExceeding =
                tierBoundaries.OrderBy(p => p.Threshold)
                    .ToList()
                    .FirstOrDefault(p => p.Threshold > tieredBoundryExceeding?.Threshold);
            var baseOrTier = RateModel.Enums.BaseOrTier.Undefined;
            if (tieredBoundryExceeding != null)
            {
                baseOrTier = tieredBoundryExceeding.BaseOrTier;
            }

            var commodity = Enums.CommodityType.Undefined;
            if (calculation.CommodityId != null)
            {
                commodity = (Enums.CommodityType)calculation.CommodityId;
            }

            var unit = billingModel?.UOMId ?? calculation.Unit;
            var uomId = 99;
            if (unit.HasValue)
            {
                uomId = unit.Value;
            }

            var address = string.Empty;

            if (customerModel != null)
            {
                if (!string.IsNullOrEmpty(customerModel.AddressLine1))
                    address = address + customerModel.AddressLine1;

                if (!string.IsNullOrEmpty(customerModel.AddressLine2))
                {
                    address = !string.IsNullOrEmpty(address)
                        ? address + ", " + customerModel.AddressLine2
                        : address + customerModel.AddressLine2;
                }

                if (!string.IsNullOrEmpty(customerModel.AddressLine3))
                {
                    address = !string.IsNullOrEmpty(address)
                        ? address + ", " + customerModel.AddressLine3
                        : address + customerModel.AddressLine3;
                }
            }
            // if the calcution is done in a different UOM from bill's
            // convert the threshold into bill's UOM for the notification
            double? previousTierUsage;
            double? currentTierUsage;
            double? currentUsage;
            if (unit.HasValue && calculation.Unit.HasValue && unit != calculation.Unit)
            {
                var conversionFactor = Extensions.GetConversionFactor(customerModel.ClientId, calculation.Unit.Value,
                    unit.Value);
                previousTierUsage = tieredBoundryExceeding?.Threshold * conversionFactor;
                currentTierUsage = nextTieredBoundryExceeding?.Threshold * conversionFactor;
                currentUsage = calculation.Usage * conversionFactor;
            }
            else
            {
                previousTierUsage = tieredBoundryExceeding?.Threshold;
                currentTierUsage = nextTieredBoundryExceeding?.Threshold;
                currentUsage = calculation.Usage;
            }

            var accountNumber = calculation.AccountId;
            if (unmaskedAccountIdEndingDigit > 0)
            {
                accountNumber = Extensions.GetMaskedAccountId(accountNumber, unmaskedAccountIdEndingDigit);
            }

            // get uom content
            var uomKey = ((Enums.UomType)uomId).ToString();
            var uom = Extensions.GetUomTextContent(uomKey, uomContentList, textContentList);
            if (string.IsNullOrEmpty(uom))
                uom = uomKey;
            identifiers.Add("-FirstName-",
                new List<string>
                {
                    customerModel != null ? customerModel.FirstName : ""
                });
            identifiers.Add("-LastName-", new List<string> { customerModel != null ? customerModel.LastName : "" });
            identifiers.Add("-AsOfDate-", new List<string> { asOfDate.ToShortDateString() });
            identifiers.Add("-TierNumber-", new List<string> { baseOrTier.GetDescription() });
            identifiers.Add("-DateThresholdExceeded-", new List<string> { asOfDate.ToShortDateString() });
            identifiers.Add("-AccountNumber-", new List<string> { accountNumber });
            identifiers.Add("-AccountAddress-", new List<string> { address });
            identifiers.Add("-CurrentUsage-", new List<string> { $"{currentUsage:0.00}" });
            identifiers.Add("-UOM-", new List<string> { uom });
            identifiers.Add("-NewTieredRateThreshold-", new List<string> { $"{tieredBoundryExceeding?.Threshold ?? 0:0.00}" });
            identifiers.Add("-PreviousTierUsage-", new List<string> { $"{previousTierUsage ?? 0:0.00}" });
            identifiers.Add("-CurrentTierUsage-", new List<string> { $"{currentTierUsage ?? 0:0.00}" });
            identifiers.Add("-CommodityType-",
                new List<string> { commodity.GetDescription() });
            return identifiers;
        }

        private string GetSmsBody(InsightModel evaluation, CalculationModel calculation, BillingModel billingModel, CustomInsightModel customInsightModel, string smsTemplateId, int unmaskedAccountIdEndingDigit, List<ClientUOM> uomContentList, List<ClientTextContent> textContentList)
        {
            var smsTemplateModel = GetSmsTemplateFromDB(evaluation.ClientId, customInsightModel.InsightName, smsTemplateId).Result;
            var substitutes = Regex.Matches(smsTemplateModel.Body, "[-][a-zA-Z]+[-]");
            switch (customInsightModel.InsightName)
            {
                case "BillToDate":
                    if (customInsightModel.IsAccountLevel)
                        smsTemplateModel.Body = GetReplacedSmsTemplate(smsTemplateModel.Body, calculation, billingModel, customInsightModel, substitutes, unmaskedAccountIdEndingDigit, uomContentList, textContentList);
                    break;
                case "CostToDate":
                    if (!customInsightModel.IsAccountLevel)
                        smsTemplateModel.Body = GetReplacedSmsTemplate(smsTemplateModel.Body, calculation, billingModel, customInsightModel, substitutes, unmaskedAccountIdEndingDigit, uomContentList, textContentList);
                    break;
                case "AccountProjectedCost":
                    if (customInsightModel.IsAccountLevel)
                        smsTemplateModel.Body = GetReplacedSmsTemplate(smsTemplateModel.Body, calculation, billingModel, customInsightModel, substitutes, unmaskedAccountIdEndingDigit, uomContentList, textContentList);
                    break;
                case "ServiceProjectedCost":
                    if (!customInsightModel.IsAccountLevel)
                        smsTemplateModel.Body = GetReplacedSmsTemplate(smsTemplateModel.Body, calculation, billingModel, customInsightModel, substitutes, unmaskedAccountIdEndingDigit, uomContentList, textContentList);
                    break;
                case "Usage":
                    if (!customInsightModel.IsAccountLevel)
                        smsTemplateModel.Body = GetReplacedSmsTemplate(smsTemplateModel.Body, calculation, billingModel, customInsightModel, substitutes, unmaskedAccountIdEndingDigit, uomContentList, textContentList);
                    break;
                case "AccountLevelCostThreshold":
                    if (customInsightModel.IsAccountLevel)
                        smsTemplateModel.Body = GetReplacedSmsTemplate(smsTemplateModel.Body, calculation, billingModel, customInsightModel, substitutes, unmaskedAccountIdEndingDigit, uomContentList, textContentList);
                    break;
                case "ServiceLevelUsageThreshold":
                    if (!customInsightModel.IsAccountLevel)
                        smsTemplateModel.Body = GetReplacedSmsTemplate(smsTemplateModel.Body, calculation, billingModel, customInsightModel, substitutes, unmaskedAccountIdEndingDigit, uomContentList, textContentList);
                    break;
                case "ServiceLevelCostThreshold":
                    if (!customInsightModel.IsAccountLevel)
                        smsTemplateModel.Body = GetReplacedSmsTemplate(smsTemplateModel.Body, calculation, billingModel, customInsightModel, substitutes, unmaskedAccountIdEndingDigit, uomContentList, textContentList);
                    break;
                case "DayThreshold":
                    if (!customInsightModel.IsAccountLevel)
                        smsTemplateModel.Body = GetReplacedSmsTemplate(smsTemplateModel.Body, calculation, billingModel, customInsightModel, substitutes, unmaskedAccountIdEndingDigit, uomContentList, textContentList);
                    break;
                case "ServiceLevelTieredThresholdApproaching":
                    if (!customInsightModel.IsAccountLevel)
                        smsTemplateModel.Body = GetReplacedSmsTemplate(smsTemplateModel.Body, calculation, billingModel, customInsightModel, substitutes, unmaskedAccountIdEndingDigit, uomContentList, textContentList);
                    break;
                case "ServiceLevelTieredThresholdExceed":
                    if (!customInsightModel.IsAccountLevel)
                        smsTemplateModel.Body = GetReplacedSmsTemplate(smsTemplateModel.Body, calculation, billingModel, customInsightModel, substitutes, unmaskedAccountIdEndingDigit, uomContentList, textContentList);
                    break;
            }
            return smsTemplateModel.Body;
        }

        public async Task<SmsTemplateModel> GetSmsTemplateFromDB(int clientId, string insightTypeName, string smsTemplateId)
        {
            var partitionKey = SmsTemplateEntity.SetSmsTemplatePartitionKey(clientId, insightTypeName);
            var rowkey = SmsTemplateEntity.SetSmsTemplateRowKey(smsTemplateId);
            var serviceDetails =
                await
                    TableRepository.GetSingleAsync<SmsTemplateEntity>(Constants.TableNames.SmsTemplate, partitionKey,
                        rowkey);
            return Mapper.Map<SmsTemplateEntity, SmsTemplateModel>(serviceDetails);
        }



        private string GetReplacedSmsTemplate(string smsBody, CalculationModel calculation, BillingModel billingModel, CustomInsightModel customInsightModel, MatchCollection substitutes, int unmaskedAccontIdEndingDigit, List<ClientUOM> uomContentList, List<ClientTextContent> textContentList)
        {

            var commodity = Enums.CommodityType.Undefined;
            if (calculation.CommodityId != null)
            {
                commodity = (Enums.CommodityType)calculation.CommodityId;
            }

            var unit = billingModel?.UOMId ?? calculation.Unit;
            var uomId = 99;
            if (unit.HasValue)
            {
                uomId = unit.Value;
            }
            // if the calcution is done in a different UOM from bill's
            // convert the usage into bill's UOM for the notification
            double? usage;
            if (unit.HasValue && calculation.Unit.HasValue && unit != calculation.Unit)
            {
                var conversionFactor = Extensions.GetConversionFactor(calculation.ClientId, calculation.Unit.Value,
                    unit.Value);
                usage = calculation.Usage * conversionFactor;
            }
            else
            {
                usage = calculation.Usage;
            }

            // get uom content
            var uomKey = ((Enums.UomType)uomId).ToString();
            var uom = Extensions.GetUomTextContent(uomKey, uomContentList, textContentList);
            if (string.IsNullOrEmpty(uom))
                uom = uomKey;
            foreach (Match match in substitutes)
            {
                foreach (Capture capture in match.Captures)
                {
                    switch (capture.Value)
                    {
                        case "-ServiceLevelUsageThreshold-":
                            smsBody = smsBody.Replace(capture.Value, $"{customInsightModel.ThresholdValue:0.00}");
                            break;
                        case "-ServiceLevelCostThreshold-":
                            smsBody = smsBody.Replace(capture.Value, $"{customInsightModel.ThresholdValue:0.00}");
                            break;
                        case "-AccountLevelCostThreshold-":
                            smsBody = smsBody.Replace(capture.Value, $"{customInsightModel.ThresholdValue:0.00}");
                            break;
                        case "-AccountNumber-":
                            if (unmaskedAccontIdEndingDigit > 0)
                                smsBody = smsBody.Replace(capture.Value, calculation.AccountId.Substring(calculation.AccountId.Length - unmaskedAccontIdEndingDigit, unmaskedAccontIdEndingDigit));
                            else 
                                smsBody = smsBody.Replace(capture.Value, calculation.AccountId);
                            break;
                        case "-BillToDate-":
                            smsBody = smsBody.Replace(capture.Value, $"{calculation.Cost:0.00}");
                            break;
                        case "-ProjectedCost-":
                            smsBody = smsBody.Replace(capture.Value, $"{calculation.ProjectedCost:0.00}");
                            break;
                        case "-NoOfDaysRemaining-":
                            smsBody = smsBody.Replace(capture.Value,
                                Convert.ToString(
                                    (Convert.ToDateTime(calculation.BillCycleEndDate) -
                                     Convert.ToDateTime(calculation.BillCycleStartDate)).Days -
                                    Convert.ToDouble(calculation.BillDays), CultureInfo.InvariantCulture));
                            break;
                        case "-UOM-":
                            smsBody = smsBody.Replace(capture.Value, uom);
                            break;
                        case "-FUEL-":
                            smsBody = smsBody.Replace(capture.Value, commodity.GetDescription());
                            break;
                        case "-CostToDate-":
                            smsBody = smsBody.Replace(capture.Value, $"{calculation.Cost:0.00}");
                            break;
                        case "-Usage-":
                            smsBody = smsBody.Replace(capture.Value, $"{usage:0.00}");
                            break;
                        case "-DayThreshold-":
                            smsBody = smsBody.Replace(capture.Value, $"{customInsightModel.ThresholdValue:0.00}");
                            break;
                        case "-ServiceLevelTieredThresholdApproaching-":
                            smsBody = smsBody.Replace(capture.Value, $"{usage:0.00}");
                            break;
                        case "-ServiceLevelTieredThresholdExceed-":
                            smsBody = smsBody.Replace(capture.Value, $"{usage:0.00}");
                            break;
                    }
                }
            }
            return smsBody;
        }

        public string GetReplacedSmsTemplateForSubscriptionDoubleOptInConfirmation(string smsBody, SubscriptionModel subscriptionModel, ClientSettings clientSetting, CustomerModel customerModel, MatchCollection substitutes)
        {
            foreach (Match match in substitutes)
            {
                foreach (Capture capture in match.Captures)
                {
                    switch (capture.Value)
                    {
                        case "-ProgramName-":
                            smsBody = smsBody.Replace(capture.Value, Convert.ToString(subscriptionModel.ProgramName));
                            break;
                        case "-AccountNumber-":
                            if (clientSetting.UnmaskedAccountIdEndingDigit > 0)
                                smsBody = smsBody.Replace(capture.Value, subscriptionModel.AccountId.Substring(subscriptionModel.AccountId.Length - clientSetting.UnmaskedAccountIdEndingDigit, clientSetting.UnmaskedAccountIdEndingDigit));
                            else
                                smsBody = smsBody.Replace(capture.Value, Convert.ToString(subscriptionModel.AccountId));
                            break;
                        case "-Keyword-":
                            {
                                var program =
                                    clientSetting.Programs.FirstOrDefault(
                                        p => p.UtilityProgramName == subscriptionModel.ProgramName);
                                if (program != null)
                                {
                                    smsBody = smsBody.Replace(capture.Value, Convert.ToString(program.TrumpiaKeyword));
                                }
                                break;
                            }
                        case "-ShortCode-":
                            smsBody = smsBody.Replace(capture.Value, Convert.ToString(clientSetting.TrumpiaShortCode));
                            break;
                    }
                }
            }
            return smsBody;
        }

        public IEnumerable<NotificationModel> GetDayNotifications(int clientId, string customerId, string accountId, DateTime asOfDate)
        {
            string partitionkey = $"{clientId}_{accountId}";
            var datefilter =
            TableQuery.CombineFilters(
            TableQuery.GenerateFilterCondition("RowKey", QueryComparisons.GreaterThanOrEqual, asOfDate.Date.ToString("yyyy-MM-dd")),
            TableOperators.And,
            TableQuery.GenerateFilterCondition("RowKey", QueryComparisons.LessThan, asOfDate.Date.AddDays(1).ToString("yyyy-MM-dd")));

            var filter = TableQuery.CombineFilters(
                TableQuery.GenerateFilterCondition("PartitionKey", QueryComparisons.Equal, partitionkey),
                TableOperators.And,
                datefilter);
            var notificationEntityList = TableRepository.GetAllAsync<NotificationEntity>(Constants.TableNames.Notification, filter);
            var notificationModelList = Mapper.Map<IEnumerable<NotificationEntity>, IEnumerable<NotificationModel>>(notificationEntityList);
            return notificationModelList;
        }

        public async Task<NotificationModel> GetNotification(string partitionKey, string rowKey)
        {
            var serviceDetails =
                await TableRepository.GetSingleAsync<NotificationEntity>(Constants.TableNames.Notification, partitionKey, rowKey);
            return Mapper.Map<NotificationEntity, NotificationModel>(serviceDetails);
        }

        public NotificationModel GetNotificationSync(int clientId, string customerId, string accountId, string programName,
            string serviceContractId, string insightName, string defaultCommunicationChannel, bool isLatest, string date)
        {
            var partitionKey = NotificationEntity.SetNotificationPartitionKey(clientId, accountId);
            var rowKey = NotificationEntity.SetNotificationRowKey(programName, serviceContractId, insightName,
                defaultCommunicationChannel, !isLatest, Convert.ToDateTime(date));
            var serviceDetails =
                TableRepository.GetSingle<NotificationEntity>(Constants.TableNames.Notification, partitionKey, rowKey);
            return Mapper.Map<NotificationEntity, NotificationModel>(serviceDetails);
        }

        public bool SaveNotificationFromEmail(NotificationModel notificationModel)
        {
            var entityModel = Mapper.Map<NotificationModel, NotificationEntity>(notificationModel);
            entityModel.PartitionKey = NotificationEntity.SetNotificationPartitionKey(notificationModel.ClientId, notificationModel.AccountId);
            entityModel.RowKey = NotificationEntity.SetNotificationRowKey(notificationModel.ProgramName,
                notificationModel.ServiceContractId, notificationModel.Insight, notificationModel.Channel, false);

            return TableRepository.InsertOrMergeSingle(Constants.TableNames.Notification, entityModel);
        }

        public bool SaveNotification(NotificationModel notificationModel, string partitionKey, string rowKey)
        {
            var entityModel = Mapper.Map<NotificationModel, NotificationEntity>(notificationModel);
            entityModel.PartitionKey = partitionKey;
            entityModel.RowKey = rowKey;

            TableRepository.InsertOrMergeSingle(Constants.TableNames.Notification, entityModel);

            //insert History Record
            var date = DateTime.Now;
            entityModel.RowKey = NotificationEntity.SetNotificationRowKey(entityModel.ProgramName, entityModel.ServiceContractId, entityModel.Insight, entityModel.Channel, true, date);
            return TableRepository.InsertOrMergeSingle(Constants.TableNames.Notification, entityModel);
        }

        public bool SaveNotification(NotificationModel notificationModel)
        {
            var entityModel = Mapper.Map<NotificationModel, NotificationEntity>(notificationModel);
            entityModel.PartitionKey = NotificationEntity.SetNotificationPartitionKey(entityModel.ClientId, entityModel.AccountId);
            entityModel.RowKey = NotificationEntity.SetNotificationRowKey(entityModel.ProgramName, entityModel.ServiceContractId, entityModel.Insight, entityModel.Channel, false);
            return TableRepository.InsertOrMergeSingle(Constants.TableNames.Notification, entityModel);
        }

        public IEnumerable<NotificationModel> GetNotifications(int clientId, DateTime startDate, DateTime endDate)
        {
            var datefilter =
            TableQuery.CombineFilters(
            TableQuery.GenerateFilterCondition("RowKey", QueryComparisons.GreaterThanOrEqual, startDate.Date.ToString("yyyy-MM-dd")),
            TableOperators.And,
            TableQuery.GenerateFilterCondition("RowKey", QueryComparisons.LessThan, endDate.AddDays(1).Date.ToString("yyyy-MM-dd")));

            var partitionKeyfilter =
            TableQuery.CombineFilters(
            TableQuery.GenerateFilterCondition("PartitionKey", QueryComparisons.GreaterThanOrEqual, clientId.ToString()),
            TableOperators.And,
            TableQuery.GenerateFilterCondition("PartitionKey", QueryComparisons.LessThanOrEqual, (clientId + 1).ToString()));


            string filter = TableQuery.CombineFilters(
                partitionKeyfilter,
                TableOperators.And,
                datefilter);

            var notificationEntityList = TableRepository.GetAllAsync<NotificationEntity>(Constants.TableNames.Notification, filter);
            var notificationModelList = Mapper.Map<IEnumerable<NotificationEntity>, IEnumerable<NotificationModel>>(notificationEntityList);
            return notificationModelList;
        }

        public void SaveNotification(NotificationModel notificationModel, bool forceLatest)
        {
            // Method intentionally left empty.  INotification interface was
            // updated to facilite AO.MDM requirements.  Expectation is that 
            // CE.AO.Business.Notification will go away at some point. 

            throw new NotImplementedException();
        }

        public IEnumerable<NotificationModel> GetNotifications()
        {
            string filter = TableQuery.GenerateFilterCondition("PartitionKey", QueryComparisons.GreaterThanOrEqual, "0");
            var notificationList = TableRepository.GetAllAsync<NotificationEntity>(Constants.TableNames.Notification, filter);

            //Separate rows by eliminating RowKey with historical data.
            var filteredNotifications = new List<NotificationEntity>();
            using (var sequenceEnum = notificationList.GetEnumerator())
            {
                while (sequenceEnum.MoveNext())
                {
                    string rowKey = sequenceEnum.Current.RowKey;
                    string splitRowKey = rowKey.Substring(0, rowKey.IndexOf("_", StringComparison.Ordinal));

                    if (!splitRowKey.Contains("-") && sequenceEnum.Current.Channel == "SMS")
                    {
                        filteredNotifications.Add(sequenceEnum.Current);
                    }
                }
            }

            var notificationModelList = Mapper.Map<IEnumerable<NotificationEntity>, IEnumerable<NotificationModel>>(filteredNotifications);
            
            return notificationModelList;
        }

        public Task DeleteNotificationAsync(int clientId, string customerId, string accountId)
        {
            // Method intentionally left empty.  INotification interface was
            // updated to facilite AO.MDM requirements.  Expectation is that 
            // CE.AO.Business.Notification will go away at some point. 

            throw new NotImplementedException();
        }

        public IList<NotificationModel> GetScheduledNotifications()
        {
            // Method intentionally left empty.  INotification interface was
            // updated to facilite AO.MDM requirements.  Expectation is that 
            // CE.AO.Business.Notification will go away at some point. 

            throw new NotImplementedException();
        }

        public NotificationModel GetNotificationSync(NotificationModel notification)
        {
            // Method intentionally left empty.  INotification interface was
            // updated to facilite AO.MDM requirements.  Expectation is that 
            // CE.AO.Business.Notification will go away at some point. 

            throw new NotImplementedException();
        }
        
    }
}
