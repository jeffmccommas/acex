﻿using System;
using System.Threading.Tasks;
using AO.BusinessContracts;
using AutoMapper;
using CE.AO.DataAccess;
using CE.AO.Entities.TableStorageEntities;
using CE.AO.Models;
using Microsoft.Practices.Unity;
using CE.AO.Entities;

namespace CE.AO.Business
{
    public class AccountUpdates : IAccountUpdates
    {
        public AccountUpdates(LogModel logModel, ITableRepository tableRepository)
        {
            LogModel = logModel;
            TableRepository = tableRepository;
        }

        public LogModel LogModel { get; set; }
        
        [Dependency]
        public ITableRepository TableRepository { get; set; }

        public async Task<bool> InsertOrMergeAccountUpdateAsync(AccountUpdatesModel model, bool history)
        {
            var entity = Mapper.Map<AccountUpdatesModel, AccountUpdatesEntity>(model);
            entity.PartitionKey = AccountUpdatesEntity.SetPartitionKey(model.ClientId, model.CustomerId);
            entity.RowKey = AccountUpdatesEntity.SetRowKey(model.UpdateType, model.ServiceContractId, model.CustomerId, history);
            return await TableRepository.InsertOrMergeSingleAsync(Constants.TableNames.AccountUpdates, entity);
        }

        public async Task<AccountUpdatesModel> GetAccountUpdate(int client, string customerId, string updateType, string serviceContractId, bool history)
        {
            var partitionKey = AccountUpdatesEntity.SetPartitionKey(client, customerId);
            var rowkey = AccountUpdatesEntity.SetRowKey(updateType, serviceContractId, customerId, history);
            var accountUpdateDetails = await TableRepository.GetSingleAsync<AccountUpdatesEntity>(Constants.TableNames.AccountUpdates, partitionKey, rowkey);
            var model = Mapper.Map<AccountUpdatesEntity, AccountUpdatesModel>(accountUpdateDetails);
            return model;
        }

        public Task<bool> DeleteHistoryAccountUpdateAsyc(int clientId, string customerId, string updateType, string serviceContractId,
            bool isLatest)
        {
            throw new NotImplementedException();
        }
    }
}