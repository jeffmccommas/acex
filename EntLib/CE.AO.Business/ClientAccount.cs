﻿using System.Threading.Tasks;
using AutoMapper;
using CE.AO.DataAccess;
using CE.AO.Entities.TableStorageEntities;
using CE.AO.Models;
using Microsoft.Practices.Unity;
using CE.AO.Entities;
using System.Collections.Generic;
using Microsoft.WindowsAzure.Storage.Table;
using WindowsAzure.Table.EntityConverters;
using System.Linq;
using AO.BusinessContracts;

namespace CE.AO.Business
{
    public class ClientAccount : IClientAccount
    {
        public ClientAccount(LogModel logModel, ITableRepository tableRepository)
        {
            LogModel = logModel;
            TableRepository = tableRepository;
        }

        public LogModel LogModel { get; set; }

        [Dependency]
        public ITableRepository TableRepository { get; set; }

        [Dependency]
        public IQueueRepository QueueRepository { get; set; }

        public async Task<bool> InsertOrMergeClientAccount(ClientAccountModel model)
        {
            var entity = Mapper.Map<ClientAccountModel, ClientAccountEntity>(model);
            entity.PartitionKey = ClientAccountEntity.SetPartitionKey(model.ClientId);
            entity.RowKey = ClientAccountEntity.SetRowKey(model.AccountId);
            return await TableRepository.InsertOrMergeSingleAsync(Constants.TableNames.ClientAccount, entity);
        }

        public IEnumerable<ClientAccountModel> GetClientAccounts(int clientId)
        {
            var partitionKey = ClientAccountEntity.SetPartitionKey(clientId);
            var filter = TableQuery.GenerateFilterCondition("PartitionKey", QueryComparisons.Equal,
                partitionKey);

            var accountMeterDetails = TableRepository.GetAllAsync<ClientAccountEntity>(Constants.TableNames.ClientAccount, filter);
            return Mapper.Map<IEnumerable<ClientAccountEntity>, IEnumerable<ClientAccountModel>>(accountMeterDetails);
        }

        public void SendScheduleMessage(int clientId, string asOfDate)
        {
            var partitionKey = ClientAccountEntity.SetPartitionKey(clientId);
            var filter = TableQuery.GenerateFilterCondition("PartitionKey", QueryComparisons.Equal,
               partitionKey);

            TableContinuationToken token = null;
            do
            {
                var queryResult = TableRepository.GetAllAsync<DynamicTableEntity>(Constants.TableNames.ClientAccount, filter, 1000, ref token);
                foreach (var entity in queryResult)
                {
                    var message = $"{entity["AccountId"].StringValue}^^{entity["ClientId"].Int32Value}^^{entity["CustomerId"].StringValue}^^{asOfDate}";
                    QueueRepository.AddQueueAsync(Constants.QueueNames.AoProcessScheduleCalculationQueue, message).Wait();
                }
            } while (token != null);
        }

        // public List<DynamicTableEntity> GetClientAccounts(int clientId, int maxEntitiesToFetch,ref TableContinuationToken token)
        public IEnumerable<ClientAccountModel> GetClientAccounts(int clientId, int maxEntitiesToFetch, ref TableContinuationToken token)
        {
            var partitionKey = ClientAccountEntity.SetPartitionKey(clientId);
            var filter = TableQuery.GenerateFilterCondition("PartitionKey", QueryComparisons.Equal,
                partitionKey);
            var converter = new TableEntityConverter<ClientAccountModel>();
            var clientAccounts = TableRepository.GetAllAsync<DynamicTableEntity>(Constants.TableNames.ClientAccount, filter, maxEntitiesToFetch, ref token);
            var clientAccountEntityList = clientAccounts.Select(dynamicEntity => converter.GetEntity(dynamicEntity)).ToList();
            return clientAccountEntityList;
        }

        public IEnumerable<ClientAccountModel> GetClientAccounts(int clientId, int maxEntitiesToFetch, ref byte[] token)
        {
            throw new System.NotImplementedException();
        }

        public Task<bool> DeleteClientAccountAsyc(int clientId, string accountId)
        {
            throw new System.NotImplementedException();
        }
    }
}
