﻿using CE.AO.Logging;
using CE.AO.Models;
using System.Collections.Generic;
using System.Threading.Tasks;
using System;
using System.Linq;
using AO.BusinessContracts;
using CE.ContentModel;
using Newtonsoft.Json;
using CE.BillToDate;
using CE.ContentModel.Entities;
using CE.RateModel;
using Microsoft.Practices.Unity;

namespace CE.AO.Business
{
    public class EvaluationFacade : IEvaluationFacade
    {
        public EvaluationFacade(LogModel logModel)
        {
            LogModel = logModel;
        }

        public LogModel LogModel { get; set; }

        [Dependency]
        public IEvaluation EvaluationManager { get; set; }

        [Dependency]
        public ISubscription SubscriptionManager { get; set; }

        [Dependency]
        public ICalculation CalculationManager { get; set; }

        [Dependency]
        public ITallAMI AmiManager { get; set; }

        [Dependency]
        public IBilling BillingManager { get; set; }

        [Dependency]
        public Lazy<ICalculationManagerFactory> CalculationManagerFactory { get; set; }

        [Dependency]
        public Lazy<IClientConfigFacade> ClientConfigFacadeManager { get; set; }

        public void ProcessEvaluation(int clientId, string customerId, string accountId, string serviceContractId, DateTime asOfDate)
        {
            // Get data Subscription data for customer
            List<SubscriptionModel> subscriptions = SubscriptionManager.GetCustomerSubscriptions(clientId, customerId, accountId).ToList();
            var tasks = new List<Task<CalculationModel>>();
            //BillingModel billingmodel = new BillingModel();

            var billingModels = new List<BillingModel>();

            if (string.IsNullOrEmpty(serviceContractId))
            {
                billingModels.AddRange(BillingManager.GetAllServicesFromLastBill(clientId, customerId,
                    accountId).ToList());

                //var billingDetails = BillingManager.GetAllServicesFromLastBill(clientId, customerId,
                //    accountId).ToList();

                if (billingModels.Any())
                {
                    // ReSharper disable once LoopCanBeConvertedToQuery
                    foreach (var bill in billingModels.Where(e => e.MeterType == Utilities.Enums.MeterType.ami.ToString()))
                    {
                        tasks.Add(CalculationManager.GetCtdCalculationAsync(clientId, accountId, bill.ServiceContractId));
                    }
                    tasks.Add(CalculationManager.GetBtdCalculationAsync(clientId, accountId));
                }
            }
            else
            {
                billingModels.Add(BillingManager.GetServiceDetailsFromBillAsync(clientId, customerId,
                   accountId, serviceContractId).Result);
                //billingmodel = BillingManager.GetServiceDetailsFromBillAsync(clientId, customerId,
                //   accountId, serviceContractId).Result;

                tasks.Add(CalculationManager.GetCtdCalculationAsync(clientId, accountId, serviceContractId));
                tasks.Add(CalculationManager.GetBtdCalculationAsync(clientId, accountId));
            }

            Task.WhenAll(tasks);

            var calculations = tasks.Where(e => e.Result != null).Select(task => task.Result).ToList();
            if (calculations.Count <= 0)
            {
                Logger.Info("Calculations needs to be performed before Evaluation", LogModel);
                return;
            }
            //Get settings from Contentful
            var factory = new ContentModelFactoryContentful();
            var provider = factory.CreateContentProvider(clientId);
            List<ClientConfigurationBulk> clientSettingsJson = provider.GetContentClientConfigurationBulk("system", "aclaraone.clientsettings");
            if (string.IsNullOrEmpty(clientSettingsJson?[0].Value))
            {
                Logger.Warn("No settings found for evaluation.", LogModel);
                return;
            }

            var clientSettings = JsonConvert.DeserializeObject<ClientSettings>(clientSettingsJson[0].Value);

            var btdCalculation = calculations.FirstOrDefault(e => string.IsNullOrEmpty(e.ServiceContractId));
            var serviceAccountCalculations = new List<CalculationModel> { btdCalculation };

            EvaluationManager.EvaluateInsight(subscriptions, serviceAccountCalculations, clientSettings, new List<TallAmiModel>(), new List<TierBoundary>(), asOfDate, null).Wait();

            foreach (var calculation in calculations.Where(e => !string.IsNullOrEmpty(e.ServiceContractId)))
            {
                serviceAccountCalculations = new List<CalculationModel>();
                var amiList = new List<TallAmiModel>();

                //Get tire boundries                 
                var settings = ClientConfigFacadeManager.Value.GetBillToDateSettings(clientId);
                var rateCompanyId = settings.RateCompanyId;
                var billToDateCalculator = CalculationManagerFactory.Value.CreateCalculationManager(settings);

                var rateClass = calculation.RateClass;
                List<TierBoundary> tierBoundaries = billToDateCalculator.GetTierBoundaries(rateCompanyId, rateClass,
                    Convert.ToDateTime(calculation.BillCycleStartDate),
                    Convert.ToDateTime(calculation.BillCycleEndDate));

                double conversionFactor = 1;
                if (calculation.CommodityId == 2 && settings.Settings.General.UseConversionFactorForGas)
                {
                    conversionFactor = settings.Settings.General.ConversionFactorForGas;
                }
                else if (calculation.CommodityId == 3 && settings.Settings.General.UseConversionFactorForWater)
                    conversionFactor = settings.Settings.General.ConversionFactorForWater;

                if (tierBoundaries != null)
                {
                    foreach (var tierBoundary in tierBoundaries)
                    {
                        tierBoundary.Threshold = tierBoundary.Threshold / conversionFactor;
                    }
                }


                //Get Daily AMI usage 
                IEnumerable<TallAmiModel> ami = AmiManager.GetAmi(Convert.ToDateTime(calculation.AsOfAmiDate), calculation.MeterId, calculation.AccountId, clientId);
                if (ami != null)
                    amiList.AddRange(ami);

                var billingmodel =
                    billingModels.FirstOrDefault(b => b.ServiceContractId == calculation.ServiceContractId);

                serviceAccountCalculations.Add(calculation);
                EvaluationManager.EvaluateInsight(subscriptions, serviceAccountCalculations, clientSettings, amiList,
                    tierBoundaries, asOfDate, billingmodel).Wait();
            }
            //Send msg to notification Queue
        }
    }
}
