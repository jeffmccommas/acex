﻿using CE.AO.Entities.TableStorageEntities;
using CE.AO.Logging;
using CE.AO.Models;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using AO.BusinessContracts;

namespace CE.AO.Business
{
    public class SubscriptionFacade : ISubscriptionFacade
    {
        // Constants
        private const string ProgramNotKnownError = "The insight is associated with an unknown program.";
        private const string NoProgramsError = "The client is not associated with any programs.";
        private const string InvalidEmailError = "The customer's email address is invalid.";
        private const string InvalidPhoneNumberError = "The customer's phone number is invalid.";

        private const string MissingProgramNameError = "The insight has a missing program name.";
        private const string MissingInsightTypeNameError = "The insight has a missing type name.";

        private const string InsightNotAssociatedWithProgramError =
            "The insight is not associated with the program specified.";

        private const string SubscriberThresholdValueExceedsMaxError =
            "The subscriber has chosen a threshold that is greater than the max allowed.";

        private const string SubscriberThresholdValueExceedsMinError =
            "The subscriber has chosen a threshold that is less than the min allowed.";

        private const string InvalidChannelError = "The insight has an invalid channel.";

        private const string UnableToDetermineCustomerServices = "Unable to determine customer services. Please verify bill data is available for this customer.";

        public SubscriptionFacade(LogModel logModel)
        {
            LogModel = logModel;
        }

        public LogModel LogModel { get; set; }

        [Dependency]
        public IBilling BillingManager { get; set; }

        [Dependency]
        public ISubscription SubscriptionManager { get; set; }

        [Dependency]
        public ICustomer CustomerManager { get; set; }

        [Dependency]
        public INotification NotificationManager { get; set; }

        [Dependency]
        public Lazy<IClientConfigFacade> ClientConfigFacadeManager { get; set; }

        [Dependency]
        public Lazy<ISendSms> SendSmsManager { get; set; }

        [Dependency]
        public Lazy<ISendEmail> SendEmailManager { get; set; }

        [Dependency]
        public Lazy<ITrumpiaRequestDetail> TrumpiaRequestManager { get; set; }

        public List<SubscriptionModel> GetDefaultListWithSubscribedPrograms(int clientId, string customerId,
            string accountId)
        {
            // get default subscription settings
            var clientSettings = ClientConfigFacadeManager.Value.GetClientSettings(clientId);
            var programs = clientSettings.Programs;

            // get customer subscription
            var subscriptionModelList = SubscriptionManager.GetCustomerSubscriptions(clientId, customerId, accountId);

            // get merge subscription
            var mergedSubscriptionList = GetDefaultListWithSubscribedPrograms(clientId, customerId, accountId, programs,
                subscriptionModelList.ToList());

            return mergedSubscriptionList;
        }

        /// <summary>
        /// Validate a subscription insights list
        /// </summary>
        /// <param name="clientId">The current client Id.</param>
        /// <param name="insights">The  list of subscription insights</param>
        /// <returns>A list of invalid insights.</returns>
        public List<SubscriptionModel> ValidateInsightList(int clientId, List<SubscriptionModel> insights)
        {
            var insightErrMsg = string.Empty;
            return ValidateInsightList(clientId, insights, string.Empty, string.Empty, ref insightErrMsg);
        }


        /// <summary>
        /// Validate a subscription insights list
        /// </summary>
        /// <param name="clientId">The current client Id.</param>
        /// <param name="insights">The  list of subscription insights</param>
        /// <param name="custEmail">The Email Address of the Customer</param>
        /// <param name="custPhone">The Phone number of the Customer</param>
        /// <param name="insightErrMsg">The specific error found with the Insights.</param>
        /// <returns>A list of invalid insights.</returns>
        public List<SubscriptionModel> ValidateInsightList(int clientId, List<SubscriptionModel> insights,
            string custEmail, string custPhone, ref string insightErrMsg)
        {
            // Get the settings for the current client.
            var clientSettings = ClientConfigFacadeManager.Value.GetClientSettings(clientId);

            // Get the current programs being offered.
            var programs = clientSettings.Programs;

            // No programs found.
            if (!programs.Any())
            {
                insightErrMsg = NoProgramsError;
                return insights;
            }

            // Check that the email address is valid.
            // If they are not valid all insights are invalid.
            if (!Extensions.IsValidEmail(custEmail))
            {
                insightErrMsg = InvalidEmailError;
                return insights;
            }

            // Check that the phone number is valid.
            // If they are not valid all insights are invalid.
            if (!Extensions.IsValidPhone(custPhone))
            {
                insightErrMsg = InvalidPhoneNumberError;
                return insights;
            }

            // For each of the insights.
            foreach (var insight in insights)
            {
                // Check the program associated with the insight for validity.
                if (string.IsNullOrEmpty(insight.ProgramName))
                {
                    insightErrMsg = MissingProgramNameError;
                    return insights;
                }

                // Check if the insight is using a known program.
                if (!programs.Exists(p =>
                    string.Equals(p.ProgramName, insight.ProgramName,
                        StringComparison.CurrentCultureIgnoreCase)))
                {
                    insightErrMsg = ProgramNotKnownError;
                    return insights;
                }

                var activeProgram =
                    programs.Find(
                        p =>
                            string.Equals(p.ProgramName, insight.ProgramName,
                                StringComparison.CurrentCultureIgnoreCase));

                // If the insight associated with the program is not the same one the client wants it is invalid.
                if (string.IsNullOrEmpty(insight.InsightTypeName))
                {
                    insightErrMsg = MissingInsightTypeNameError;
                    return insights;
                }

                var activeInsight =
                    activeProgram.Insights.Find(
                        i =>
                            string.Equals(i.InsightName, insight.InsightTypeName,
                                StringComparison.CurrentCultureIgnoreCase));


                if (activeInsight == null)
                {
                    insightErrMsg = InsightNotAssociatedWithProgramError;
                    return insights;
                }

                // Check if the subscriber chosen threshold is greater that the max allowed.
                if (insight.SubscriberThresholdValue.HasValue &&
                    insight.SubscriberThresholdValue != 0 &&
                    !string.IsNullOrEmpty(activeInsight.ThresholdMax))
                {
                    if (insight.SubscriberThresholdValue > double.Parse(activeInsight.ThresholdMax))
                    {
                        insightErrMsg = SubscriberThresholdValueExceedsMaxError;
                        return insights;
                    }
                }

                // Check if the subscriber chosen threshold is lesser that the min allowed.
                if (insight.SubscriberThresholdValue.HasValue &&
                    insight.SubscriberThresholdValue != 0 &&
                    !string.IsNullOrEmpty(activeInsight.ThresholdMin))
                {
                    if (insight.SubscriberThresholdValue < double.Parse(activeInsight.ThresholdMin))
                    {
                        insightErrMsg = SubscriberThresholdValueExceedsMinError;
                        return insights;
                    }
                }

                // If the selected threshold value is zero then reset it to the default.
                if (insight.SubscriberThresholdValue == 0)
                {
                    insight.SubscriberThresholdValue = activeInsight.GetCommodityTypevalue(insight.CommodityKey);
                }

                // Check if chosen channel is allowed
                if (!string.IsNullOrEmpty(insight.Channel) &&
                    !activeInsight.AllowedCommunicationChannel.ToLower().Contains(insight.Channel.ToLower()))
                {
                    insightErrMsg = InvalidChannelError;
                    return insights;
                }
            }

            return new List<SubscriptionModel>();
        }

        /// <summary>
        /// Get Subscribe List
        /// </summary>
        /// <param name="clientId"></param>
        /// <param name="customerId"></param>
        /// <param name="accountId"></param>
        /// <param name="insights"></param>
        /// <returns></returns>
        public List<SubscriptionModel> GetSubscribeList(int clientId, string customerId, string accountId,
            List<SubscriptionModel> insights)
        {
            var insightErrorMsg = string.Empty;
            return GetSubscribeList(clientId, customerId, accountId, insights, ref insightErrorMsg);
        }


        /// <summary>
        /// Get Subscribe List
        /// </summary>
        /// <param name="clientId"></param>
        /// <param name="customerId"></param>
        /// <param name="accountId"></param>
        /// <param name="insights"></param>
        /// <param name="insightErrorMsg"></param>
        /// <returns></returns>
        public List<SubscriptionModel> GetSubscribeList(int clientId, string customerId, string accountId, List<SubscriptionModel> insights, ref string insightErrorMsg)
        {
            var subscribeProgramInsights = new List<SubscriptionModel>();
            // get default subscription settings
            var clientSettings = ClientConfigFacadeManager.Value.GetClientSettings(clientId);
            var programs = clientSettings.Programs;

            // get customer subscription
            //var newSubscription = false;
            var subscriptionModelList = SubscriptionManager.GetCustomerSubscriptions(clientId, customerId, accountId).ToList();
            //if (subscriptionModelList.Count == 0 )
            //{
            //    newSubscription = true;
            //}
            //if (insights != null && insights.Count > 0)
            //{
            var existingSubscriptionwithDefault = GetDefaultListWithSubscribedPrograms(clientId, customerId, accountId,
                programs, subscriptionModelList.ToList());
            if (existingSubscriptionwithDefault==null)
            {
                insightErrorMsg = UnableToDetermineCustomerServices;
                return subscribeProgramInsights;
            }

            var existingUnSubsrcibedInsights = existingSubscriptionwithDefault.ToList().FindAll(i => !i.IsSelected);

            var tobeSubscribe = insights.FindAll(i => i.IsSelected);
            var tobeUnsubscribe = insights.FindAll(i => !i.IsSelected);


            if (tobeSubscribe.Any())
            { 
                // subscribe
              // program insight list
                foreach (var program in programs.FindAll(p => tobeSubscribe.Exists(i => string.Equals(i.ProgramName, p.ProgramName, StringComparison.CurrentCultureIgnoreCase))))
                {
                    var optInEmailCompleted = false;
                    var optInSmsCompleted = false;
                    // check if the program has sms, email or both
                    var hasSms = false;
                    var hasEmail = false;
                    if (
                        existingSubscriptionwithDefault.Exists(
                            e =>
                                e.ProgramName.Equals(program.ProgramName, StringComparison.InvariantCultureIgnoreCase) && e.Channel.Equals(Utilities.Enums.SubscriptionChannel.SMS.ToString(),
                                    StringComparison.InvariantCultureIgnoreCase)) || tobeSubscribe.Exists(
                            e =>
                                e.ProgramName.Equals(program.ProgramName, StringComparison.InvariantCultureIgnoreCase) && !string.IsNullOrEmpty(e.Channel) && e.Channel.Equals(Utilities.Enums.SubscriptionChannel.SMS.ToString(),
                                    StringComparison.InvariantCultureIgnoreCase)))
                    {
                        hasSms = true;
                    }
                    if (
                        existingSubscriptionwithDefault.Exists(
                            e =>
                                e.ProgramName.Equals(program.ProgramName, StringComparison.InvariantCultureIgnoreCase) && e.Channel.Equals(Utilities.Enums.SubscriptionChannel.Email.ToString(),
                                    StringComparison.InvariantCultureIgnoreCase)) || tobeSubscribe.Exists(
                            e =>
                                e.ProgramName.Equals(program.ProgramName, StringComparison.InvariantCultureIgnoreCase) && !string.IsNullOrEmpty(e.Channel) && e.Channel.Equals(Utilities.Enums.SubscriptionChannel.SMS.ToString(),
                                    StringComparison.InvariantCultureIgnoreCase)))
                    {
                        hasEmail = true;
                    }
                    if (
                        existingSubscriptionwithDefault.Exists(
                            e =>
                                e.ProgramName.Equals(program.ProgramName, StringComparison.InvariantCultureIgnoreCase) && e.Channel.Equals(Utilities.Enums.SubscriptionChannel.EmailAndSMS.ToString(),
                                    StringComparison.InvariantCultureIgnoreCase)) || tobeSubscribe.Exists(
                            e =>
                                e.ProgramName.Equals(program.ProgramName, StringComparison.InvariantCultureIgnoreCase) && !string.IsNullOrEmpty(e.Channel) && e.Channel.Equals(Utilities.Enums.SubscriptionChannel.SMS.ToString(),
                                    StringComparison.InvariantCultureIgnoreCase)))
                    {
                        hasEmail = true;
                        hasSms = true;
                    }

                    // get the existing double opt in status
                    if (
                        subscriptionModelList.Exists(
                            s =>
                                s.AccountId == accountId && customerId == s.CustomerId &&
                                string.Equals(s.ProgramName, program.ProgramName, StringComparison.CurrentCultureIgnoreCase)))
                    {
                        var savedProgram = subscriptionModelList.Find(
                            s =>
                                s.AccountId == accountId && customerId == s.CustomerId &&
                                string.Equals(s.ProgramName, program.ProgramName, StringComparison.CurrentCultureIgnoreCase));
                        optInEmailCompleted = savedProgram.IsEmailOptInCompleted?? false;
                        optInSmsCompleted = savedProgram.IsSmsOptInCompleted?? false;

                    }
                    else
                    {
                        if(!program.DoubleOptInRequired)
                        {
                            optInEmailCompleted = true;
                            optInSmsCompleted = true;
                        }
                        else
                        {
                            if (hasEmail && hasSms)
                            {
                                optInSmsCompleted = false;
                                optInEmailCompleted = false;
                            }
                            else if (hasEmail)
                                optInEmailCompleted = false;
                            else if (hasSms)
                                optInSmsCompleted = false;
                            
                        }
                    }

                    var programSubscription = new SubscriptionModel
                    {
                        AccountId = accountId,
                        ClientId = clientId,
                        //CommodityKey = program.CommodityType.ToLower(),
                        CustomerId = customerId,
                        IsSelected = true,
                        ProgramName = program.ProgramName,
                        PremiseId = string.Empty,
                        ServiceContractId = string.Empty,
                        ServicePointId = string.Empty,
                        InsightTypeName = string.Empty,
                        IsSmsOptInCompleted = optInSmsCompleted,
                        IsEmailOptInCompleted = optInEmailCompleted
                    };

                    if (program.DoubleOptInRequired)
                    {
                        
                        if (hasEmail && hasSms && !optInSmsCompleted && !optInEmailCompleted)
                            programSubscription.IsDoubleOptIn = true;
                        else if(hasEmail && !optInEmailCompleted)
                            programSubscription.IsDoubleOptIn = true;
                        else if (hasSms && !optInSmsCompleted)
                            programSubscription.IsDoubleOptIn = true;
                    }

                    // add the program that is not previously subscribed to the list
                    if (!subscribeProgramInsights.Exists(s => s.AccountId == accountId && customerId == s.CustomerId && s.ProgramName.ToLower() == program.ProgramName.ToLower()))
                        subscribeProgramInsights.Add(programSubscription);



                    // account insights
                    var accountInsights = tobeSubscribe.FindAll(i => string.IsNullOrEmpty(i.ServiceContractId) && i.ProgramName.ToLower() == program.ProgramName.ToLower());
                    //if (program.Insights.FindAll(i => i.Level.ToLower() == "account").Count != accountInsights.Count)
                    //{
                        foreach (
                            var accountDefaultInsight in program.Insights.FindAll(i => i.Level.ToLower() == "account"))
                    {
                            // if the default account insight doesn't exist in the to-be subscrib list, then add to the list as unsubscribed
                            if (
                                !accountInsights.Exists(
                                    i => i.InsightTypeName.ToLower() == accountDefaultInsight.InsightName.ToLower()))
                            {
                                var existingSubscritionwithDefaultAccountLevel =
                           existingSubscriptionwithDefault.FindAll(
                               i =>
                                   string.IsNullOrEmpty(i.ServiceContractId) &&
                                   i.ProgramName.ToLower() == program.ProgramName.ToLower() && i.IsSelected);
                                if (
                                    !existingSubscritionwithDefaultAccountLevel.Exists(
                                        i =>
                                            i.InsightTypeName.ToLower() ==
                                            accountDefaultInsight.InsightName.ToLower()))
                                {
                                    var insightSubscription = new SubscriptionModel
                                    {
                                        AccountId = accountId,
                                        ClientId = clientId,
                                        //CommodityKey = program.CommodityType.ToLower(),
                                        CustomerId = customerId,
                                        IsSelected = false,
                                        ProgramName = program.ProgramName,
                                        InsightTypeName = accountDefaultInsight.InsightName,
                                        Channel = accountDefaultInsight.DefaultCommunicationChannel,
                                        //SubscriberThresholdValue = accountDefaultInsight.DefaultValue,
                                        PremiseId = string.Empty,
                                        ServiceContractId = string.Empty,
                                        ServicePointId = string.Empty,
                                        IsSmsOptInCompleted = true,
                                        IsEmailOptInCompleted = true
                                    };

                                    if (!string.IsNullOrEmpty(accountDefaultInsight.DefaultValue))
                                        insightSubscription.SubscriberThresholdValue =
                                            accountDefaultInsight.GetCommodityTypevalue(insightSubscription.CommodityKey);
                                    subscribeProgramInsights.Add(insightSubscription);
                                }


                            }
                            else
                            {
                                // if exists, check if the threshold and channal is same as the default setting; 
                                // if not, add to the list with the updated channel/threshold
                                var subscribeAccountInsight =
                                    accountInsights.Find(
                                        i => i.InsightTypeName.ToLower() == accountDefaultInsight.InsightName.ToLower());

                                var savedAccountInsight =
                           existingSubscriptionwithDefault.Find(
                               i =>
                                   string.IsNullOrEmpty(i.ServiceContractId) &&
                                            string.Equals(i.ProgramName, program.ProgramName,
                                                StringComparison.CurrentCultureIgnoreCase) &&
                                            string.Equals(i.InsightTypeName, accountDefaultInsight.InsightName,
                                                StringComparison.CurrentCultureIgnoreCase));


                                var savedThreshold = savedAccountInsight.SubscriberThresholdValue;

                                if ((!string.IsNullOrEmpty(subscribeAccountInsight.Channel) &&
                                     !string.IsNullOrEmpty(savedAccountInsight.Channel) &&
                                     !string.Equals(subscribeAccountInsight.Channel, savedAccountInsight.Channel,
                                         StringComparison.CurrentCultureIgnoreCase)) ||
                                    (!string.IsNullOrEmpty(subscribeAccountInsight.Channel) &&
                                     string.IsNullOrEmpty(savedAccountInsight.Channel)) ||
                                    (string.IsNullOrEmpty(subscribeAccountInsight.Channel) &&
                                     !string.IsNullOrEmpty(savedAccountInsight.Channel)) ||
                                    (subscribeAccountInsight.SubscriberThresholdValue.HasValue &&
                                     !savedThreshold.HasValue) ||
                                    (subscribeAccountInsight.SubscriberThresholdValue.HasValue &&
                                     savedThreshold.HasValue &&
                                     subscribeAccountInsight.SubscriberThresholdValue != savedThreshold))
                                {
                                    var insightSubscription = new SubscriptionModel
                                    {
                                        AccountId = accountId,
                                        ClientId = clientId,
                                        CustomerId = customerId,
                                        IsSelected = true,
                                        ProgramName = program.ProgramName,
                                        InsightTypeName = accountDefaultInsight.InsightName,
                                        PremiseId = string.Empty,
                                        ServiceContractId = string.Empty,
                                        ServicePointId = string.Empty,
                                        SubscriberThresholdValue = savedAccountInsight.SubscriberThresholdValue,
                                        Channel = savedAccountInsight.Channel,
                                        IsSmsOptInCompleted = true,
                                        IsEmailOptInCompleted = true
                                    };

                                    if ((!string.IsNullOrEmpty(subscribeAccountInsight.Channel) &&
                                         !string.IsNullOrEmpty(savedAccountInsight.Channel) &&
                                         subscribeAccountInsight.Channel.ToLower() !=
                                         savedAccountInsight.Channel.ToLower()) ||
                                        (!string.IsNullOrEmpty(subscribeAccountInsight.Channel) &&
                                         string.IsNullOrEmpty(savedAccountInsight.Channel)))
                                    {
                                        insightSubscription.Channel = subscribeAccountInsight.Channel;
                                    }
                                    else if (string.IsNullOrEmpty(subscribeAccountInsight.Channel) &&
                                              !string.IsNullOrEmpty(savedAccountInsight.Channel))
                                    {
                                        insightSubscription.Channel = savedAccountInsight.Channel;
                                    }


                                    if ((subscribeAccountInsight.SubscriberThresholdValue.HasValue &&
                                    subscribeAccountInsight.SubscriberThresholdValue != 0 &&
                                         !savedThreshold.HasValue) ||
                                        (subscribeAccountInsight.SubscriberThresholdValue.HasValue &&
                                         savedThreshold.HasValue &&
                                    subscribeAccountInsight.SubscriberThresholdValue != 0 &&
                                         subscribeAccountInsight.SubscriberThresholdValue != savedThreshold))
                                    {
                                        insightSubscription.SubscriberThresholdValue =
                                            subscribeAccountInsight.SubscriberThresholdValue;
                                    }
                                    subscribeProgramInsights.Add(insightSubscription);
                                }

                                
                            }
                        }
                    //}
                    //else
                    //{
                    //    // check if the threshold value is updated 
                    //    foreach (
                    //        var accountDefaultInsight in existingSubscriptionwithDefault.FindAll(i => string.IsNullOrEmpty(i.ServiceContractId) && i.ProgramName.Equals(program.ProgramName, StringComparison.InvariantCultureIgnoreCase)))
                    //    {
                    //        var insight =
                    //            accountInsights.Find(
                    //                a =>
                    //                    a.InsightTypeName.Equals(accountDefaultInsight.InsightTypeName,
                    //                        StringComparison.InvariantCultureIgnoreCase));
                    //        if (insight.SubscriberThresholdValue != accountDefaultInsight.SubscriberThresholdValue)
                    //        {
                                
                    //        }
                    //    }

                    //}
                    // serivce insights
                    var services = tobeSubscribe.FindAll(i => !string.IsNullOrEmpty(i.ServiceContractId) && i.ProgramName.ToLower() == program.ProgramName.ToLower()); //.GroupBy(i => i.InsightTypeName).ToList();
                                                                                                                                                                       //var serviceDefaultInsights = program.Insights.FindAll(i => i.Level.ToLower() == "service");
                    //if (program.Insights.FindAll(i => i.Level.ToLower() == "service").Count != services.Count)
                    //{
                        //SubscriptionModel service = null;
                        //if (services.Count > 0)
                        //{
                        var existingSubscritionwithDefaultServiceLevel =
                            existingUnSubsrcibedInsights.FindAll(
                                i =>
                                    !string.IsNullOrEmpty(i.ServiceContractId) &&
                                    i.ProgramName.ToLower() == program.ProgramName.ToLower());
                        var unscribeSerivcesInsights = existingSubscritionwithDefaultServiceLevel.FindAll(e => !services.Exists(s => s.ServiceContractId == e.ServiceContractId && s.InsightTypeName.ToLower() == e.InsightTypeName.ToLower()));
                        foreach (var service in unscribeSerivcesInsights)
                        {
                            var insightSubscription = new SubscriptionModel
                            {
                                AccountId = accountId,
                                ClientId = clientId,
                                //CommodityKey = program.CommodityType.ToLower(),
                                CustomerId = customerId,
                                IsSelected = false,
                                ProgramName = program.ProgramName,
                                InsightTypeName = service?.InsightTypeName,
                                Channel = service?.Channel,
                                PremiseId = service?.PremiseId,
                                ServiceContractId = service?.ServiceContractId,
                                ServicePointId = service?.ServicePointId,
                                SubscriberThresholdValue = service?.SubscriberThresholdValue,
                                IsSmsOptInCompleted = true,
                                IsEmailOptInCompleted = true,
                                UomId = service.UomId
                            };
                            subscribeProgramInsights.Add(insightSubscription);
                        }

                        // add subscribe serivce to the list if the channel/threshold is different from the default's
                        foreach (var service in services)
                        {
                            var serviceDefault = existingSubscriptionwithDefault.Find(
                                s =>
                                    s.ServiceContractId == service.ServiceContractId &&
                                    s.InsightTypeName.ToLower() == service.InsightTypeName.ToLower() &&
                                    s.ProgramName.ToLower() == service.ProgramName.ToLower()
                                    );
                            if(serviceDefault != null)
                            {
                                if ((!string.IsNullOrEmpty(service.Channel) && !string.IsNullOrEmpty(serviceDefault.Channel) && service.Channel.ToLower() != serviceDefault.Channel.ToLower()) ||
                                    (!string.IsNullOrEmpty(service.Channel) && string.IsNullOrEmpty(serviceDefault.Channel)) ||
                                    (string.IsNullOrEmpty(service.Channel) && !string.IsNullOrEmpty(serviceDefault.Channel)) ||
                                  (service.SubscriberThresholdValue.HasValue && !serviceDefault.SubscriberThresholdValue.HasValue) ||
                                  (service.SubscriberThresholdValue.HasValue && serviceDefault.SubscriberThresholdValue.HasValue && service.SubscriberThresholdValue != serviceDefault.SubscriberThresholdValue))
                                {
                                    var insightSubscription = new SubscriptionModel
                                    {
                                        AccountId = accountId,
                                        ClientId = clientId,
                                        CustomerId = customerId,
                                        IsSelected = true,
                                        ProgramName = program.ProgramName,
                                        InsightTypeName = serviceDefault.InsightTypeName,
                                        PremiseId = serviceDefault.PremiseId,
                                        ServiceContractId = serviceDefault.ServiceContractId,
                                        ServicePointId = serviceDefault.ServicePointId,
                                        SubscriberThresholdValue = serviceDefault.SubscriberThresholdValue,
                                        Channel = serviceDefault.Channel,
                                        IsSmsOptInCompleted = true,
                                        IsEmailOptInCompleted = true,
                                        UomId = serviceDefault.UomId
                                    };

                                    if ((!string.IsNullOrEmpty(service.Channel) && !string.IsNullOrEmpty(serviceDefault.Channel) && service.Channel.ToLower() !=
                                        serviceDefault.Channel.ToLower()) ||
                                        (!string.IsNullOrEmpty(service.Channel) && string.IsNullOrEmpty(serviceDefault.Channel))
                                        )
                                    {
                                        insightSubscription.Channel = service.Channel;
                                    }else if (string.IsNullOrEmpty(service.Channel) &&
                                              !string.IsNullOrEmpty(serviceDefault.Channel))
                                    {
                                        insightSubscription.Channel = serviceDefault.Channel;
                                    }

                                    if ((service.SubscriberThresholdValue.HasValue && service.SubscriberThresholdValue != 0 && !serviceDefault.SubscriberThresholdValue.HasValue) ||
                                    (service.SubscriberThresholdValue.HasValue && service.SubscriberThresholdValue != 0 && serviceDefault.SubscriberThresholdValue.HasValue && service.SubscriberThresholdValue != serviceDefault.SubscriberThresholdValue))
                                    {
                                        insightSubscription.SubscriberThresholdValue =
                                            service.SubscriberThresholdValue;
                                    }
                                    subscribeProgramInsights.Add(insightSubscription);
                                }
                            }
                           
                        }
                        //}
                    //}


                }

                // add the new subscribed one
                existingUnSubsrcibedInsights.FindAll(
                    p => subscribeProgramInsights.Exists(i => i.ProgramName.ToLower() == p.ProgramName.ToLower()))
                    .ForEach(i =>
                    {
                        if (!subscribeProgramInsights.Exists(s => s.AccountId == i.AccountId && (s.ServiceContractId == i.ServiceContractId || (string.IsNullOrEmpty(i.ServiceContractId) && string.IsNullOrEmpty(s.ServiceContractId))) && s.ProgramName.ToLower() == i.ProgramName.ToLower() && s.InsightTypeName.ToLower() == i.InsightTypeName.ToLower()))
                        {
                            if (subscriptionModelList.ToList().Exists(s => s.AccountId == i.AccountId && (s.ServiceContractId == i.ServiceContractId || (string.IsNullOrEmpty(i.ServiceContractId) && string.IsNullOrEmpty(s.ServiceContractId))) && s.ProgramName.ToLower() == i.ProgramName.ToLower() && ((!string.IsNullOrEmpty(s.InsightTypeName) && s.InsightTypeName.ToLower() == i.InsightTypeName.ToLower()) || (string.IsNullOrEmpty(s.InsightTypeName) && i.InsightTypeName == string.Empty))))
                            {
                                i.IsSelected = true;
                                if (string.IsNullOrEmpty(i.ServiceContractId))
                                {
                                    i.ServiceContractId = string.Empty;
                                    i.ServicePointId = string.Empty;
                                    i.PremiseId = string.Empty;
                                }

                                // check if the subscribe threshold/channel is same as the default's; if not, update them
                                var subscribe =
                                           tobeSubscribe.Find(s => i.ProgramName.ToLower() == s.ProgramName.ToLower() && s.AccountId == i.AccountId && (s.ServiceContractId == i.ServiceContractId || (string.IsNullOrEmpty(i.ServiceContractId) && string.IsNullOrEmpty(s.ServiceContractId))) && s.ProgramName.ToLower() == i.ProgramName.ToLower() && s.InsightTypeName.ToLower() == i.InsightTypeName.ToLower());
                                if (!string.IsNullOrEmpty(subscribe.Channel) && subscribe.Channel.ToLower() !=
                                   i.Channel.ToLower())
                                {
                                    i.Channel = subscribe.Channel;
                                }

                                if ((subscribe.SubscriberThresholdValue.HasValue && subscribe.SubscriberThresholdValue !=0  && !i.SubscriberThresholdValue.HasValue) ||
                                (subscribe.SubscriberThresholdValue.HasValue && subscribe.SubscriberThresholdValue != 0 && i.SubscriberThresholdValue.HasValue && subscribe.SubscriberThresholdValue != i.SubscriberThresholdValue))
                                {
                                    i.SubscriberThresholdValue =
                                        subscribe.SubscriberThresholdValue;
                                }

                                subscribeProgramInsights.Add(i);
                            }
                        }
                    });
            }


            // unsubscrib
            if (tobeUnsubscribe.Any())
            {
                var hasSms = false;
                var hasEmail = false;
                // service level
                foreach (var subscriptionModel in existingSubscriptionwithDefault.FindAll(e => e.IsSelected && !string.IsNullOrEmpty(e.ServiceContractId)).
                    FindAll(u => tobeUnsubscribe.Exists(i => i.ProgramName.ToLower() == u.ProgramName.ToLower() && i.InsightTypeName.ToLower() == u.InsightTypeName.ToLower() && i.ServiceContractId == u.ServiceContractId)))
                {
                    //if (subscriptionModelList.ToList().Exists(s => s.AccountId == subscriptionModel.AccountId && (s.ServiceContractId == subscriptionModel.ServiceContractId || (string.IsNullOrEmpty(subscriptionModel.ServiceContractId) && string.IsNullOrEmpty(s.ServiceContractId))) && s.ProgramName.ToLower() == subscriptionModel.ProgramName.ToLower() && ((!string.IsNullOrEmpty(s.InsightTypeName) && s.InsightTypeName.ToLower() == subscriptionModel.InsightTypeName.ToLower()) || (string.IsNullOrEmpty(s.InsightTypeName) && subscriptionModel.InsightTypeName == string.Empty))))
                    //{
                    var defaultInsight =
                        programs.Find(p => p.ProgramName.ToLower() == subscriptionModel.ProgramName.ToLower())
                            .Insights.Find(
                                i => i.InsightName.ToLower() == subscriptionModel.InsightTypeName.ToLower());
                    if (!subscribeProgramInsights.Exists(s => s.AccountId == subscriptionModel.AccountId && (s.ServiceContractId == subscriptionModel.ServiceContractId) && s.ProgramName.ToLower() == subscriptionModel.ProgramName.ToLower() && s.InsightTypeName.ToLower() == subscriptionModel.InsightTypeName.ToLower()))
                    {
                        subscriptionModel.IsSelected = false;
                        if(!string.IsNullOrEmpty(subscriptionModel.Channel))
                        {
                            if (subscriptionModel.Channel.Equals(Utilities.Enums.SubscriptionChannel.SMS.ToString(),
                                StringComparison.InvariantCultureIgnoreCase))
                            {
                                hasSms = true;
                            }
                            else if (subscriptionModel.Channel.Equals(Utilities.Enums.SubscriptionChannel.Email.ToString(),
                               StringComparison.InvariantCultureIgnoreCase))
                            {
                                hasEmail = true;
                            }
                            else if (subscriptionModel.Channel.Equals(Utilities.Enums.SubscriptionChannel.EmailAndSMS.ToString(),
                               StringComparison.InvariantCultureIgnoreCase))
                            {
                                hasEmail = true;
                                hasSms = true;
                            }
                        }
                        subscriptionModel.Channel = defaultInsight.DefaultCommunicationChannel;
                        subscriptionModel.SubscriberThresholdValue = !string.IsNullOrEmpty(defaultInsight.DefaultValue)
                            ? defaultInsight.GetCommodityTypevalue(subscriptionModel.CommodityKey)
                            : null;
                        if (string.IsNullOrEmpty(subscriptionModel.ServiceContractId))
                        {
                            subscriptionModel.ServiceContractId = string.Empty;
                            subscriptionModel.ServicePointId = string.Empty;
                            subscriptionModel.PremiseId = string.Empty;
                        }
                        subscribeProgramInsights.Add(subscriptionModel);
                        //}
                    }
                    else
                    {
                        {
                            var unsubscribe =
                                subscribeProgramInsights.Find(
                                    s =>
                                        s.AccountId == subscriptionModel.AccountId &&
                                        (s.ServiceContractId == subscriptionModel.ServiceContractId ||
                                         (string.IsNullOrEmpty(subscriptionModel.ServiceContractId) &&
                                          string.IsNullOrEmpty(s.ServiceContractId))) &&
                                        s.ProgramName.ToLower() == subscriptionModel.ProgramName.ToLower() &&
                                        s.InsightTypeName.ToLower() == subscriptionModel.InsightTypeName.ToLower());
                            unsubscribe.IsSelected = false;
                            if (!string.IsNullOrEmpty(subscriptionModel.Channel))
                            {
                                if (subscriptionModel.Channel.Equals(Utilities.Enums.SubscriptionChannel.SMS.ToString(),
                                    StringComparison.InvariantCultureIgnoreCase))
                                {
                                    hasSms = true;
                                }
                                else if (subscriptionModel.Channel.Equals(Utilities.Enums.SubscriptionChannel.Email.ToString(),
                                   StringComparison.InvariantCultureIgnoreCase))
                                {
                                    hasEmail = true;
                                }
                                else if (subscriptionModel.Channel.Equals(Utilities.Enums.SubscriptionChannel.EmailAndSMS.ToString(),
                                   StringComparison.InvariantCultureIgnoreCase))
                                {
                                    hasEmail = true;
                                    hasSms = true;
                                }
                            }
                            unsubscribe.Channel = defaultInsight.DefaultCommunicationChannel;
                            unsubscribe.SubscriberThresholdValue = !string.IsNullOrEmpty(defaultInsight.DefaultValue)
                                ? defaultInsight.GetCommodityTypevalue(unsubscribe.CommodityKey)
                                : null;

                        }
                    }
                }

                // account level
                foreach (var subscriptionModel in existingSubscriptionwithDefault.FindAll(e => e.IsSelected && string.IsNullOrEmpty(e.ServiceContractId)).
                    FindAll(u => tobeUnsubscribe.Exists(i => i.ProgramName.ToLower() == u.ProgramName.ToLower() && i.InsightTypeName.ToLower() == u.InsightTypeName.ToLower())))
                {
                    var defaultInsight =
                        programs.Find(p => p.ProgramName.ToLower() == subscriptionModel.ProgramName.ToLower())
                            .Insights.Find(
                                i => i.InsightName.ToLower() == subscriptionModel.InsightTypeName.ToLower());
                    if (!subscribeProgramInsights.Exists(s => s.AccountId == subscriptionModel.AccountId && (s.ServiceContractId == subscriptionModel.ServiceContractId) && s.ProgramName.ToLower() == subscriptionModel.ProgramName.ToLower() && s.InsightTypeName.ToLower() == subscriptionModel.InsightTypeName.ToLower()))
                    {
                        subscriptionModel.IsSelected = false;
                        if (!string.IsNullOrEmpty(subscriptionModel.Channel))
                        {
                            if (subscriptionModel.Channel.Equals(Utilities.Enums.SubscriptionChannel.SMS.ToString(),
                                StringComparison.InvariantCultureIgnoreCase))
                            {
                                hasSms = true;
                            }
                            else if (subscriptionModel.Channel.Equals(Utilities.Enums.SubscriptionChannel.Email.ToString(),
                               StringComparison.InvariantCultureIgnoreCase))
                            {
                                hasEmail = true;
                            }
                            else if (subscriptionModel.Channel.Equals(Utilities.Enums.SubscriptionChannel.EmailAndSMS.ToString(),
                               StringComparison.InvariantCultureIgnoreCase))
                            {
                                hasEmail = true;
                                hasSms = true;
                            }
                        }
                        subscriptionModel.Channel = defaultInsight.DefaultCommunicationChannel;
                        subscriptionModel.SubscriberThresholdValue = !string.IsNullOrEmpty(defaultInsight.DefaultValue)
                            ? defaultInsight.GetCommodityTypevalue(subscriptionModel.CommodityKey)
                            : null;
                        if (string.IsNullOrEmpty(subscriptionModel.ServiceContractId))
                        {
                            subscriptionModel.ServiceContractId = string.Empty;
                            subscriptionModel.ServicePointId = string.Empty;
                            subscriptionModel.PremiseId = string.Empty;
                        }
                        subscribeProgramInsights.Add(subscriptionModel);
                    }
                    else
                    {
                        {
                            var unsubscribe =
                                subscribeProgramInsights.Find(
                                    s =>
                                        s.AccountId == subscriptionModel.AccountId &&
                                        (s.ServiceContractId == subscriptionModel.ServiceContractId ||
                                         (string.IsNullOrEmpty(subscriptionModel.ServiceContractId) &&
                                          string.IsNullOrEmpty(s.ServiceContractId))) &&
                                        s.ProgramName.ToLower() == subscriptionModel.ProgramName.ToLower() &&
                                        s.InsightTypeName.ToLower() == subscriptionModel.InsightTypeName.ToLower());
                            unsubscribe.IsSelected = false;
                            if (!string.IsNullOrEmpty(subscriptionModel.Channel))
                            {
                                if (subscriptionModel.Channel.Equals(Utilities.Enums.SubscriptionChannel.SMS.ToString(),
                                    StringComparison.InvariantCultureIgnoreCase))
                                {
                                    hasSms = true;
                                }
                                else if (subscriptionModel.Channel.Equals(Utilities.Enums.SubscriptionChannel.Email.ToString(),
                                   StringComparison.InvariantCultureIgnoreCase))
                                {
                                    hasEmail = true;
                                }
                                else if (subscriptionModel.Channel.Equals(Utilities.Enums.SubscriptionChannel.EmailAndSMS.ToString(),
                                   StringComparison.InvariantCultureIgnoreCase))
                                {
                                    hasEmail = true;
                                    hasSms = true;
                                }
                            }
                            unsubscribe.Channel = defaultInsight.DefaultCommunicationChannel;
                            unsubscribe.SubscriberThresholdValue = !string.IsNullOrEmpty(defaultInsight.DefaultValue)
                                ? defaultInsight.GetCommodityTypevalue(unsubscribe.CommodityKey)
                                : null;

                        }
                    }
                }

                foreach (
                    var program in
                        programs.FindAll(
                            p => tobeUnsubscribe.Exists(i => i.ProgramName.ToLower() == p.ProgramName.ToLower())))
                {
                    var programInsights = existingSubscriptionwithDefault.FindAll(e => e.ProgramName.ToLower() == program.ProgramName.ToLower());

                    var unscribeProgramInsights =
                        subscribeProgramInsights.FindAll(s => !s.IsSelected && s.ProgramName.ToLower() == program.ProgramName.ToLower());

                    var defaultUnscribeProgramInsights = programInsights.FindAll(e => !e.IsSelected && !unscribeProgramInsights
                        .Exists(s => s.ProgramName.ToLower() == e.ProgramName.ToLower() && s.AccountId == e.AccountId && s.InsightTypeName.ToLower() == e.InsightTypeName.ToLower()
                        && (s.ServiceContractId == e.ServiceContractId ||
                                         (string.IsNullOrEmpty(e.ServiceContractId) &&
                                          string.IsNullOrEmpty(s.ServiceContractId))
                        ))
                        && !tobeSubscribe.Exists(s => s.ProgramName.ToLower() == e.ProgramName.ToLower() && s.AccountId == e.AccountId && s.InsightTypeName.ToLower() == e.InsightTypeName.ToLower()
                        && (s.ServiceContractId == e.ServiceContractId ||
                                         (string.IsNullOrEmpty(e.ServiceContractId) &&
                                          string.IsNullOrEmpty(s.ServiceContractId))
                        ))
                        );

                    if (programInsights.Count == unscribeProgramInsights.Count + defaultUnscribeProgramInsights.Count)
                    {
                        var unsubscribeProgram = subscribeProgramInsights.Find(
                            s => s.ProgramName == program.ProgramName && string.IsNullOrEmpty(s.InsightTypeName));
                        if (unsubscribeProgram != null)
                            unsubscribeProgram.IsSelected = false;
                        else
                        {
                            var optInEmailCompleted = false;
                            var optInSmsCompleted = false;
                            // get the existing optin status
                            if (
                                subscriptionModelList.Exists(
                                    s =>
                                        s.AccountId == accountId && customerId == s.CustomerId &&
                                        s.ProgramName.ToLower() == program.ProgramName.ToLower()))
                            {
                                var savedProgram = subscriptionModelList.Find(
                                    s =>
                                        s.AccountId == accountId && customerId == s.CustomerId &&
                                        s.ProgramName.ToLower() == program.ProgramName.ToLower());
                                optInEmailCompleted = savedProgram.IsEmailOptInCompleted ?? false;
                                optInSmsCompleted = savedProgram.IsSmsOptInCompleted ?? false;

                            }
                            // check if the program is using sms, email or both to update the optin status for unsubscribe program
                            if (
                                existingSubscriptionwithDefault.Exists(
                                    e =>
                                        e.ProgramName.Equals(program.ProgramName, StringComparison.InvariantCultureIgnoreCase) && e.Channel.Equals(Utilities.Enums.SubscriptionChannel.SMS.ToString(),
                                            StringComparison.InvariantCultureIgnoreCase)))
                            {
                                hasSms = true;
                            }
                            if (
                                existingSubscriptionwithDefault.Exists(
                                    e =>
                                        e.ProgramName.Equals(program.ProgramName, StringComparison.InvariantCultureIgnoreCase) && e.Channel.Equals(Utilities.Enums.SubscriptionChannel.Email.ToString(),
                                            StringComparison.InvariantCultureIgnoreCase)))
                            {
                                hasEmail = true;
                            }
                            if (
                                existingSubscriptionwithDefault.Exists(
                                    e =>
                                        e.ProgramName.Equals(program.ProgramName, StringComparison.InvariantCultureIgnoreCase) && e.Channel.Equals(Utilities.Enums.SubscriptionChannel.EmailAndSMS.ToString(),
                                            StringComparison.InvariantCultureIgnoreCase)))
                            {
                                hasEmail = true;
                                hasSms = true;
                            }

                            if (!program.DoubleOptInRequired)
                            {
                                optInEmailCompleted = true;
                                optInSmsCompleted = true;
                            }
                            else
                            {
                                if (hasEmail && hasSms)
                                {
                                    optInEmailCompleted = false;
                                    optInSmsCompleted = false;
                                }
                                else if (hasEmail)
                                    optInEmailCompleted = false;
                                else if (hasSms)
                                    optInSmsCompleted = false;
                            }
                            unsubscribeProgram = new SubscriptionModel
                            {
                                AccountId = accountId,
                                ClientId = clientId,
                                //CommodityKey = program.CommodityType.ToLower(),
                                CustomerId = customerId,
                                IsSelected = false,
                                ProgramName = program.ProgramName,
                                PremiseId = string.Empty,
                                ServiceContractId = string.Empty,
                                ServicePointId = string.Empty,
                                InsightTypeName = string.Empty,
                                IsSmsOptInCompleted = optInSmsCompleted,
                                IsEmailOptInCompleted = optInEmailCompleted
                            };
                            subscribeProgramInsights.Add(unsubscribeProgram);
                        }
                    }
                }
            }
            return subscribeProgramInsights;
        }

        public TrumpiaRequestDetailModel DoubleOptin(SubscriptionModel subscription)
        {
            TrumpiaRequestDetailModel smsRequestDetail = null;
            var addEmailConfirmationCount = false;
            var addSmsConfirmationCount = false;
            var clientSettings = ClientConfigFacadeManager.Value.GetClientSettings(subscription.ClientId);
            var programs = clientSettings.Programs;
            
            var program = programs.Find(p => p.ProgramName.ToLower() == subscription.ProgramName.ToLower());

            if (program.DoubleOptInRequired)
            {
                var customerTask = GetCustomerAsync(subscription.ClientId, subscription.CustomerId);
                Task.WaitAll(customerTask);
                if (customerTask.Status == TaskStatus.RanToCompletion)
                {
                    var customerModel = customerTask.Result;

                    var subscriptionEntity = SubscriptionManager.GetSubscriptionDetails(subscription.ClientId, subscription.CustomerId,
                        subscription.AccountId, subscription.ProgramName, subscription.ServiceContractId,
                        subscription.InsightTypeName) ?? subscription;

                    if (subscription.IsEmailOptInCompleted != null && !subscription.IsEmailOptInCompleted.Value)
                    {
                        // send optin email
                        addEmailConfirmationCount = SendEmail(clientSettings, customerModel, subscriptionEntity);
                        if (addEmailConfirmationCount)
                            subscriptionEntity.IsEmailOptInCompleted = true;
                    }

                    if (subscription.IsSmsOptInCompleted != null && !subscription.IsSmsOptInCompleted.Value)
                    {
                        // send optin sms 
                        addSmsConfirmationCount = SendSms(clientSettings, customerModel, subscriptionEntity);
                        if (addSmsConfirmationCount)
                            subscriptionEntity.IsSmsOptInCompleted = true;
                        else
                        {
                            // TFS 8022 - get status code if sms is not sent
                            var trumpiaRequestDetailModel = TrumpiaRequestManager.Value.GetTrumpiaRequestDetail(
                                customerModel.TrumpiaRequestDetailId);
                            if (trumpiaRequestDetailModel != null)
                            {
                                smsRequestDetail = trumpiaRequestDetailModel;
                            }
                        }
                    }

                    if (addEmailConfirmationCount || addSmsConfirmationCount)
                    {
                        var subscriptionTask = Task.Run(() => SubscriptionManager.InsertSubscriptionAsync(
                            subscriptionEntity,
                            addEmailConfirmationCount, addSmsConfirmationCount).ConfigureAwait(false));
                        Task.WhenAll(subscriptionTask);

                    }
                }
            }
            return smsRequestDetail;
        }

        public TrumpiaRequestDetailModel TrumpiaSubscriptionCheck(string requestId, string subscriptionId, string apiKey,
            string username)
        {
            throw new NotImplementedException();
        }

        private List<SubscriptionModel> GetDefaultListWithSubscribedPrograms(int clientId, string customerId, string accountId, List<ProgramSettings> programs, List<SubscriptionModel> subscriptionModelList)
        {
            // get service info
            var services = BillingManager.GetAllServicesFromLastBill(clientId, customerId, accountId);

            if (services == null)
            {
                return null;
            }

                var serviceList = services as IList<BillingModel> ?? services.ToList();
                if (!serviceList.Any())
            {
                    return null;
            }

                // map the program to subscription model
                var defaultSusbscriptionList = MapProgramsToSubscriptionModel(programs, clientId, customerId, accountId, serviceList.ToList());
                // mrege customer subscription with default subscripton settings
                var mergedSubscriptionList = SubscriptionManager.MergeDefaultSubscriptionListWithSubscribedPrograms(subscriptionModelList, defaultSusbscriptionList);
                return mergedSubscriptionList;
            }

        private bool SendEmail(ClientSettings clientSetting, CustomerModel customerModel, SubscriptionModel subscriptionModel)
        {
            var emailSent = false;
            if (customerModel != null)
            {
                // email settings
                var clientEmailUsername = clientSetting.OptInEmailUserName;
                var clientEmailPassword = clientSetting.OptInEmailPassword;
                var sender = clientSetting.OptInEmailFrom;
                var accountNumber = subscriptionModel.AccountId;
                if (clientSetting.UnmaskedAccountIdEndingDigit > 0)
                {
                    accountNumber = Extensions.GetMaskedAccountId(accountNumber, clientSetting.UnmaskedAccountIdEndingDigit);
                }
                if (string.IsNullOrEmpty(customerModel.EmailAddress))
                {
                    Logger.Fatal(@" Email address not specified in Customer information.", LogModel);
                    return false;
                }

                var address = string.Empty;

                if (!string.IsNullOrEmpty(customerModel.AddressLine1))
                    address = address + customerModel.AddressLine1;

                if (!string.IsNullOrEmpty(customerModel.AddressLine2))
                {
                    address = !string.IsNullOrEmpty(address)
                        ? address + ", " + customerModel.AddressLine2
                        : address + customerModel.AddressLine2;
                }

                if (!string.IsNullOrEmpty(customerModel.AddressLine3))
                {
                    address = !string.IsNullOrEmpty(address)
                        ? address + ", " + customerModel.AddressLine3
                        : address + customerModel.AddressLine3;
                }

                List<string> emailAddress = new List<string> { customerModel.EmailAddress };
                string description = $@"{subscriptionModel.ClientId}|{subscriptionModel.CustomerId}|{subscriptionModel.AccountId}|{subscriptionModel.ProgramName}";
                var encodedDescription = Encoding.UTF8.GetBytes(description);
                var identifiers = new Dictionary<string, List<string>>
            {
                {"-FirstName-", new List<string> {customerModel.FirstName}},
                {"-LastName-", new List<string> {customerModel.LastName}},
                {"-ConfirmationLink-" , new List<string> { $"description={Convert.ToBase64String(encodedDescription)}" } },
                {"-ProgramName-" , new List<string> { subscriptionModel.ProgramName } },
                {"-AccountNumber-", new List<string> {accountNumber}},
                { "-AccountAddress-",new List<string>{address}}
            };

                var templateId = clientSetting.OptInEmailConfirmationTemplateId;
                var notificationPartitionKey = NotificationEntity.SetNotificationPartitionKey(subscriptionModel.ClientId,
                    subscriptionModel.AccountId);
                var notificationRowKey = NotificationEntity.SetNotificationRowKey(subscriptionModel.ProgramName, null, null,
                    subscriptionModel.Channel, false);

                var partitionKey = SubscriptionEntity.SetPartitionKey(subscriptionModel.ClientId,
                    subscriptionModel.CustomerId, subscriptionModel.AccountId);
                var rowKey = SubscriptionEntity.SetLatestRowKey(subscriptionModel.ProgramName,subscriptionModel.ServiceContractId, subscriptionModel.InsightTypeName);
                var customKey = string.Format(notificationPartitionKey + "|" + notificationRowKey + "|" + partitionKey + "|" + rowKey);

                //await sendEmail.SendEmailAsync(sender, emailAddress, identifiers, templateId, clientEmailUsername, clientEmailPassword, customKey, null);
                var emailTask =
                    Task.Run(
                        () =>
                            SendEmailManager.Value.SendEmailAsync(sender, emailAddress, identifiers, templateId, clientEmailUsername,
                                clientEmailPassword, customKey, null).ConfigureAwait(false));
                //var customerTask = Task.Run(() => customerManager.InsertOrMergeCustomerAsync(customer).ConfigureAwait(false));
                Task.WaitAll(emailTask);
                emailSent = true;
            }

            return emailSent;
        }

        private bool SendSms(ClientSettings clientSetting, CustomerModel customerModel, SubscriptionModel subscriptionModel)
        {
            var smsSent = false;
            if(customerModel != null)
            {
                // sms settings
                var smsApiKey = clientSetting.TrumpiaApiKey;
                var smsUserName = clientSetting.TrumpiaUserName;
                var smsContactList = clientSetting.TrumpiaContactList;

                if (string.IsNullOrEmpty(customerModel.TrumpiaSubscriptionId))
                {
                    if (string.IsNullOrEmpty(customerModel.Phone1))
                    {
                        Logger.Fatal(@"Phone number not specified in Customer information.",
                            LogModel);
                    }
                    else
                    {
                        CustomerManager.CreateSubscriptionAndUpdateCustomer(customerModel, smsApiKey, smsUserName, smsContactList);
                    }
                }

                if (!string.IsNullOrEmpty(customerModel.TrumpiaSubscriptionId))
                {
                    var notificationTask = GetSmsTemplateAsync(clientSetting, subscriptionModel);
                    Task.WaitAll(notificationTask);

                    if (notificationTask.Status == TaskStatus.RanToCompletion)
                    {
                        var smsTemplateModel = notificationTask.Result;
                        if (smsTemplateModel != null)
                        {
                            var substitutes = Regex.Matches(smsTemplateModel.Body, "[-][a-zA-Z]+[-]");
                            smsTemplateModel.Body = NotificationManager.GetReplacedSmsTemplateForSubscriptionDoubleOptInConfirmation(smsTemplateModel.Body,
                                subscriptionModel, clientSetting, customerModel, substitutes);

                            var partitionKey = SubscriptionEntity.SetPartitionKey(subscriptionModel.ClientId,
                                subscriptionModel.CustomerId, subscriptionModel.AccountId);
                            var rowKey = SubscriptionEntity.SetLatestRowKey(subscriptionModel.ProgramName,
                                subscriptionModel.ServiceContractId, subscriptionModel.InsightTypeName);
                            var phone = customerModel.Phone1 ?? customerModel.Phone2;
                            var smsTask =
                                Task.Run(
                                    () =>
                                        SendSmsManager.Value.Send(smsApiKey, smsUserName, smsTemplateModel.Body, phone, customerModel.TrumpiaSubscriptionId,
                                            $"s|{partitionKey}|{rowKey}",
                                            customerModel.ClientId, subscriptionModel.AccountId,
                                            subscriptionModel.ProgramName, subscriptionModel.ServiceContractId,
                                            subscriptionModel.InsightTypeName, DateTime.UtcNow)
                                            .ConfigureAwait(false));
                            Task.WhenAll(smsTask);
                            smsSent = true;
                        }

                    }
                }
            }
            
            return smsSent;
        }

        private async Task<SmsTemplateModel> GetSmsTemplateAsync(ClientSettings clientSetting, SubscriptionModel subscriptionModel)
        {
            var task = await Task.Factory.StartNew(() => NotificationManager.GetSmsTemplateFromDB(subscriptionModel.ClientId,
                        "OptInConfirmation",
                        clientSetting.OptInSmsConfirmationTemplateId)).ConfigureAwait(false);

            return task.Result;

        }

        /// <summary>
        /// return customer info async from business layer
        /// </summary>
        /// <param name="clientId"></param>
        /// <param name="customerId"></param>
        /// <returns></returns>
        private async Task<CustomerModel> GetCustomerAsync(int clientId, string customerId)
        {
            
            var task = await Task.Factory.StartNew(() => CustomerManager.GetCustomerAsync(clientId, customerId)).ConfigureAwait(false);

            return task.Result;

        }

        private List<SubscriptionModel> MapProgramsToSubscriptionModel(List<ProgramSettings> programs, int clientId, string customerId, string accountId, List<BillingModel> serviceList)
        {
            var subscriptions = new List<SubscriptionModel>();
            foreach (var program in programs)
            {
                foreach (var programInsight in program.Insights)
                {

                    //if (subscription.SubscriberThresholdValue.HasValue)
                    //    subscription.SubscriberThresholdValue = programInsight.DefaultValue;
                    if (programInsight.Level == "Account")
                    {
                        var subscription = new SubscriptionModel
                        {
                            IsSelected = false,
                            Channel = programInsight.DefaultCommunicationChannel,
                            ProgramName = program.ProgramName,
                            InsightTypeName = programInsight.InsightName,
                            CustomerId = customerId,
                            AccountId = accountId,
                            PremiseId = string.Empty,
                            ServiceContractId = string.Empty,
                            ServicePointId = string.Empty,
                            ClientId = clientId,
                            AllowedChannels = programInsight.AllowedCommunicationChannel,
                            IsEmailOptInCompleted = true,
                            IsSmsOptInCompleted = true
                        };

                        if (!string.IsNullOrEmpty(programInsight.DefaultValue))
                        {
                            subscription.SubscriberThresholdValue = programInsight.GetCommodityTypevalue(subscription.CommodityKey);
                            subscription.ThresholdMin = Convert.ToDouble(programInsight.ThresholdMin);
                            subscription.ThresholdMax = Convert.ToDouble(programInsight.ThresholdMax);
                            subscription.ThresholdType = programInsight.ThresholdType;
                        }
                        subscriptions.Add(subscription);
                    }
                    else
                    {
                        foreach (var service in serviceList)
                        {
                            var commodities = programInsight.CommodityType.ToLower().Split(',').ToList();

                            var commodityKey = ((Utilities.Enums.CommodityType)service.CommodityId).ToString().ToLower();
                            if (commodities.Exists(c => c == commodityKey))
                            {
                                var serviceSubscription = new SubscriptionModel
                                {
                                    CommodityKey = commodityKey,
                                    IsSelected = false,
                                    Channel = programInsight.DefaultCommunicationChannel,
                                    ProgramName = program.ProgramName,
                                    InsightTypeName = programInsight.InsightName,
                                    ServiceContractId = service.ServiceContractId,
                                    ServicePointId = service.ServicePointId,
                                    PremiseId = service.PremiseId,
                                    AccountId = service.AccountId,
                                    CustomerId = customerId,
                                    ClientId = clientId,
                                    AllowedChannels = programInsight.AllowedCommunicationChannel,
                                    // set up UOM ID only for service usage threshold insight
                                    UomId = (!string.IsNullOrEmpty(programInsight.ThresholdType) && programInsight.ThresholdType.ToLower() == "usage") ? (service.UOMId ?? (int)Utilities.Enums.UomType.Other) : (int?)null,
                                    IsEmailOptInCompleted = true,
                                    IsSmsOptInCompleted = true
                                };
                                if (!string.IsNullOrEmpty(programInsight.DefaultValue))
                                {
                                    serviceSubscription.SubscriberThresholdValue = programInsight.GetCommodityTypevalue(serviceSubscription.CommodityKey);
                                    serviceSubscription.ThresholdMin = Convert.ToDouble(programInsight.ThresholdMin);
                                    serviceSubscription.ThresholdMax = Convert.ToDouble(programInsight.ThresholdMax);
                                    serviceSubscription.ThresholdType = programInsight.ThresholdType;
                                }
                                subscriptions.Add(serviceSubscription);
                            }
                        }
                    }
                }
            }
            return subscriptions;
        }
    }
}
