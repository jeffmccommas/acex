﻿using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using CE.AO.DataAccess;
using CE.AO.Entities;
using CE.AO.Entities.TableStorageEntities;
using CE.AO.Models;
using System.Threading.Tasks;
using AO.BusinessContracts;
using Microsoft.WindowsAzure.Storage.Table;

namespace CE.AO.Business
{
    public class CustomerMobileNumberLookup : ICustomerMobileNumberLookup
    {
        public CustomerMobileNumberLookup(LogModel logModel, ITableRepository tableRepository)
        {
            LogModel = logModel;
           TableRepository = tableRepository;
        }

        public LogModel LogModel { get; set; }
       
        public ITableRepository TableRepository { get; set; }
      
        public CustomerMobileNumberLookupModel GetCustomerMobileNumberLookup(int clientId,
            string mobileNumber, string accountId)
        {
            var partitionKey = CustomerMobileNumberLookupEntity.SetPartitionKey(clientId, mobileNumber);
            var rowKey = CustomerMobileNumberLookupEntity.SetRowKey(accountId);
            var entity = TableRepository.GetSingle<CustomerMobileNumberLookupEntity>(Constants.TableNames.CustomerMobileNumberLookup,
                partitionKey, rowKey);

            return Mapper.Map<CustomerMobileNumberLookupEntity, CustomerMobileNumberLookupModel>(entity);
        }

        public List<CustomerMobileNumberLookupModel> GetAllCustomerMobileNumberLookup(int clientId,
            string mobileNumber)
        {
            var filter = TableQuery.GenerateFilterCondition("PartitionKey", QueryComparisons.Equal,
                CustomerMobileNumberLookupEntity.SetPartitionKey(clientId, mobileNumber));
            var entities = TableRepository.GetAllAsync<CustomerMobileNumberLookupEntity>(Constants.TableNames.CustomerMobileNumberLookup, filter).ToList();
            return Mapper.Map<List<CustomerMobileNumberLookupEntity>, List<CustomerMobileNumberLookupModel>>(entities);
        }

        public async Task<bool> InsertOrMergeCustomerMobileNumberLookupAsync(CustomerMobileNumberLookupModel model)
        {
            var entity = Mapper.Map<CustomerMobileNumberLookupModel, CustomerMobileNumberLookupEntity>(model);
            entity.PartitionKey = CustomerMobileNumberLookupEntity.SetPartitionKey(model.ClientId, model.MobileNumber);
            entity.RowKey = CustomerMobileNumberLookupEntity.SetRowKey(model.AccountId);
            return await TableRepository.InsertOrMergeSingleAsync(Constants.TableNames.CustomerMobileNumberLookup, entity);
        }

		public async Task<bool> DeleteCustomerMobileNumberLookupAsync(int clientId, string mobileNumber, string accountId)
		{
			var partitionKey = CustomerMobileNumberLookupEntity.SetPartitionKey(clientId, mobileNumber);
			var rowKey = CustomerMobileNumberLookupEntity.SetRowKey(accountId);
			return await TableRepository.DeleteSingleAsync<CustomerMobileNumberLookupEntity>(Constants.TableNames.CustomerMobileNumberLookup, partitionKey, rowKey);
		}
	}
}
