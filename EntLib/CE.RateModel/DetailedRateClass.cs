﻿using System.Collections.Generic;

namespace CE.RateModel
{
    public class DetailedRateClass : RateClass
    {
        public List<UseCharge> UseCharges { get; set; }
        public List<ServiceCharge> ServiceCharges { get; set; }
        public List<MinimumCharge> MinimumCharges { get; set; }
        public List<TaxCharge> TaxCharges { get; set; }
        public List<TierBoundary> TierBoundaries { get; set; }
        public List<TouBoundary> TouBoundaries { get; set; }
        public List<SeasonBoundary> SeasonBoundaries { get; set; }
    }
}
