﻿using System.Collections.Generic;

namespace CE.RateModel
{
    /// <summary>
    /// List of rate classes.
    /// </summary>
    public class RateClassList :  List<RateClass>
    {
        public RateClassList()
        {

        }

        /// <summary>
        /// Find the matching rate class.
        /// </summary>
        /// <param name="rateClass"></param>
        /// <returns></returns>
        public RateClass FindByRateClassID(string rateClassID)
        {
            return (this.Find(p => p.RateClassID.Trim() == rateClassID));
        }

        /// <summary>
        /// Is the RateClass a rate class of the specified customer type.
        /// </summary>
        /// <param name="rateClassID"></param>
        /// <returns></returns>
        public bool IsRateClassOfCustomerType(string rateClassID, Enums.CustomerType customerType)
        {
            bool result = false;
            RateClass r;

            try
            {
                r = FindByRateClassID(rateClassID);

                if (r != null)
                {
                    if (r.CustomerType == customerType)
                    {
                        result = true;
                    }
                }
            }
            catch
            {
                result = false;
            }

            return (result);
        }

    }
}
