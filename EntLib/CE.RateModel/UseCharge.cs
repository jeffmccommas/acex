﻿
namespace CE.RateModel
{
    public class UseCharge
    {
        public bool FuelCostRecovery { get; set; }
        public int FCRGroup { get; set; }
        public bool RealTimePricing { get; set; }
        public int RTPGroup { get; set; }
        public Enums.BaseOrTier BaseOrTier { get; set; }
        public Enums.TimeOfUse TimeOfUse { get; set; }
        public Enums.Season Season { get; set; }
        public Enums.PartType PartType { get; set; }
        public double ChargeValue { get; set; }
        public string CostType { get; set; }
    }
}
