﻿using System.Collections.Generic;

namespace CE.RateModel
{

    /// <summary>
    /// Simple class for simple calculate cost calculations made through Rate Model interface.
    /// </summary>
    public class CostResult
    {
        public double TotalCost { get; set; }
        public List<Cost> Costs { get; set; }
        public Enums.ErrorCode ErrCode { get; set; }
        public int BillDays { get; set; }
        public bool MinimumCostsApplied { get; set; }
        public bool CalculationsHadChildren { get; set; }

        public CostResult()
        {
            CalculationsHadChildren = false;
            MinimumCostsApplied = false;
            ErrCode = Enums.ErrorCode.NoError;
        }

    }

}
