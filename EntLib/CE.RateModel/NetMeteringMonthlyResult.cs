﻿using System.Collections.Generic;

namespace CE.RateModel
{
    /// <summary>
    /// Simple class for Net Metering Result in a cost to date via interface.
    /// </summary>
    public class NetMeteringMonthlyResult
    {
        public Enums.NetMeteringType NetMeteringType { get; set; }
        public double RegularCost { get; set; }
        public double MinimumCost { get; set; }
        public List<Cost> MinimumCosts { get; set; }
        public double CarryoverCharge { get; set; }

        public NetMeteringMonthlyResult()
        {
            NetMeteringType = Enums.NetMeteringType.Unspecified;
            MinimumCosts = null;
        }

    }
}
