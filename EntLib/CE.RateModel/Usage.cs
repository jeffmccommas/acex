﻿using System;
using System.Collections.Generic;

namespace CE.RateModel
{
    public class Usage
    {
        private Enums.BaseOrTier _baseOrTier;
        private Enums.TimeOfUse _timeOfUse;
        private Enums.Season _season;
        private double _quantity;
        private List<Reading> _readings;
        private DateTime _UsageDate;
        private int _NumberDaysMissingReadings;

        private Usage()
        {
        }

        public Usage(Enums.BaseOrTier baseOrTier, Enums.TimeOfUse tou, Enums.Season season, double quantity, DateTime usageDate, int MissingDaysReadings)
        {
            _baseOrTier = baseOrTier;
            _timeOfUse = tou;
            _season = season;
            _quantity = quantity;
            _UsageDate = usageDate;
            NumberDaysMissingReadings = MissingDaysReadings;
        }

        public Usage(Enums.BaseOrTier baseOrTier, Enums.TimeOfUse tou, Enums.Season season, double quantity, DateTime usageDate)
        {
            _baseOrTier = baseOrTier;
            _timeOfUse = tou;
            _season = season;
            _quantity = quantity;
            _UsageDate = usageDate;
        }


        public Usage(Enums.BaseOrTier baseOrTier, Enums.TimeOfUse tou, Enums.Season season, double quantity)
        {
            _baseOrTier = baseOrTier;
            _timeOfUse = tou;
            _season = season;
            _quantity = quantity;
        }

        public Usage(Enums.BaseOrTier baseOrTier, Enums.TimeOfUse tou, double quantity)
        {
            _baseOrTier = baseOrTier;
            _timeOfUse = tou;
            _season = Enums.Season.Undefined;
            _quantity = quantity;
        }

        public Usage(Enums.TimeOfUse tou, double quantity)
        {
            _baseOrTier = Enums.BaseOrTier.Undefined;
            _timeOfUse = tou;
            _season = Enums.Season.Undefined;
            _quantity = quantity;
        }

        public Usage(double quantity)
        {
            _baseOrTier = Enums.BaseOrTier.TotalServiceUse;
            _timeOfUse = Enums.TimeOfUse.Undefined;
            _season = Enums.Season.Undefined;
            _quantity = quantity;
        }
        public Usage(double quantity, DateTime usageDate)
        {
            _baseOrTier = Enums.BaseOrTier.TotalServiceUse;
            _timeOfUse = Enums.TimeOfUse.Undefined;
            _season = Enums.Season.Undefined;
            _quantity = quantity;
            _UsageDate = usageDate;
        }

        #region Properties

        public Enums.BaseOrTier BaseOrTier
        {
            get { return _baseOrTier; }
            set { _baseOrTier = value; }
        }

        public Enums.TimeOfUse TimeOfUse
        {
            get { return _timeOfUse; }
            set { _timeOfUse = value; }
        }

        public Enums.Season Season
        {
            get { return _season; }
            set { _season = value; }
        }

        public double Quantity
        {
            get { return _quantity; }
            set { _quantity = value; }
        }

        public List<Reading> Readings
        {
            get { return _readings; }
            set { _readings = value; }
        }

        public DateTime UsageDate
        {
            get { return _UsageDate; }
            set { _UsageDate = value; }
        }
        public int NumberDaysMissingReadings
        {
            get { return _NumberDaysMissingReadings; }
            set { _NumberDaysMissingReadings = value; }
        }
        #endregion

        /// <summary>
        /// Create name from fields.
        /// </summary>
        /// <returns></returns>
        public string GetName()
        {
            string result = string.Empty;

            if (_baseOrTier != Enums.BaseOrTier.Undefined)
            {
                result = result + _baseOrTier.ToString();
            }

            if (_timeOfUse != Enums.TimeOfUse.Undefined)
            {
                result = result + _timeOfUse.ToString();
            }

            if (_season != Enums.Season.Undefined)
            {
                result = result + _season.ToString();
            }

            return (result);
        }
    }

}

