﻿using CE.RateEngine;
using System;
using System.Collections;
using System.Collections.Generic;

namespace CE.RateModel
{
    public class RateModelEPAdapter : IRateModel
    {
        private CE.RateEngine.RateEngine _rateEngine;
        private const string _invalidCustomerType = "Invalid Customer Type {0}.";

        private RateModelEPAdapter()
        {
        }

        public RateModelEPAdapter(CE.RateEngine.RateEngine rateEngine)
        {
            this._rateEngine = rateEngine;
        }


        /// <summary>
        /// Rate Engine is logging, IRateModel
        /// </summary>
        /// <returns></returns>
        public bool IsLoggingEnabled()
        {
            return (_rateEngine.IsLoggingEnabled);
        }

        /// <summary>
        /// Rate Engine Logging ID, IRateModel
        /// </summary>
        /// <returns></returns>
        public string LogSessionID()
        {
            return (Convert.ToString(_rateEngine.LogSessionID));
        }


        #region CostToDate

        /// <summary>
        /// Use this to do a full CostToDate with usage determinants from readings, ICostToDate
        /// </summary>
        /// <param name="rateCompanyID"></param>
        /// <param name="readings"></param>
        /// <param name="rateClass"></param>
        /// <param name="startDate"></param>
        /// <param name="endDate"></param>
        /// <param name="projectedEndDate"></param>
        /// <returns></returns>
        public CostToDateResult CostToDate(int rateCompanyID, List<Reading> readings, string rateClass, DateTime startDate, DateTime endDate, DateTime projectedEndDate, CostToDateExtParam costToDateExtParam)
        {
            CostToDateResult result;
            CE.RateEngine.CostToDateExtParam extParamEP;
            CE.RateEngine.ReadingCollection readingsEP;
            CE.RateEngine.CostToDateResult resultEP;

            readingsEP = MapToReadingsEP(readings);

            extParamEP = MapToCostToDateExtParamEP(costToDateExtParam);
            // CR 51610 Oct 2014 - pass either usage collection or reading collection to cost to date based on the interval type
            var interval = readingsEP.DetermineIntervalOfReadings();
            if (interval == CE.RateEngine.Enums.ReadingInterval.Daily)
            {
                // set extra param to indication daily only data
                extParamEP.IsDailyOnlyReadings = true;

            }

            resultEP = _rateEngine.CostToDate(rateCompanyID, readingsEP, rateClass, startDate, endDate, projectedEndDate, extParamEP);

            result = MapFromCostToDateResultEP(resultEP);

            DetailedRateDefinitionCollection detailedDefs = null;

            detailedDefs = _rateEngine.GetDetailedRateInformation(rateCompanyID, rateClass, startDate, endDate);
            if (detailedDefs != null && detailedDefs.Count > 0)
            {
                if (detailedDefs[0].IsDemand && !extParamEP.IncludeDailyDemandCalc)
                    result.DailyDemandCalcExcluded = true;
                else
                    result.DailyDemandCalcExcluded = false;
            }

            return (result);
        }

        /// <summary>
        /// Use this to do a full CostToDate with usage determinants from readings, and baselines; ICostToDate
        /// </summary>
        /// <param name="rateCompanyID"></param>
        /// <param name="readings"></param>
        /// <param name="baselines"></param>
        /// <param name="rateClass"></param>
        /// <param name="startDate"></param>
        /// <param name="endDate"></param>
        /// <param name="projectedEndDate"></param>
        /// <param name="costToDateExtParam"></param>
        /// <returns></returns>
        public CostToDateResult CostToDate(int rateCompanyID, List<Reading> readings, List<Baseline> baselines, string rateClass, DateTime startDate, DateTime endDate, DateTime projectedEndDate, CostToDateExtParam costToDateExtParam)
        {
            CostToDateResult result;
            CE.RateEngine.CostToDateExtParam extParamEP;
            CE.RateEngine.ReadingCollection readingsEP;
            CE.RateEngine.CostToDateResult resultEP;
            CE.RateEngine.BaselineCollection baselinesEP;

            readingsEP = MapToReadingsEP(readings);

            baselinesEP = MapToBaselinesEP(baselines);

            extParamEP = MapToCostToDateExtParamEP(costToDateExtParam);
            // CR 51610 Oct 2014 - pass either usage collection or reading collection to cost to date based on the interval type
            var interval = readingsEP.DetermineIntervalOfReadings();
            if (interval == CE.RateEngine.Enums.ReadingInterval.Daily)
            {
                // set extra param to indication daily only data
                extParamEP.IsDailyOnlyReadings = true;

            }

            resultEP = _rateEngine.CostToDate(rateCompanyID, readingsEP, baselinesEP, rateClass, startDate, endDate, projectedEndDate, extParamEP);

            result = MapFromCostToDateResultEP(resultEP);

            DetailedRateDefinitionCollection detailedDefs = null;
            detailedDefs = _rateEngine.GetDetailedRateInformation(rateCompanyID, rateClass, startDate, endDate);


            if (detailedDefs[0].IsDemand && !extParamEP.IncludeDailyDemandCalc)
                result.DailyDemandCalcExcluded = true;
            else
                result.DailyDemandCalcExcluded = false;

            return (result);
        }

        /// <summary>
        /// Use this to do a full CostToDate with usage determinants from readings, and baselines; special parameter to get newest definition and reset start date; ICostToDate
        /// </summary>
        /// <param name="rateCompanyID"></param>
        /// <param name="readings"></param>
        /// <param name="baselines"></param>
        /// <param name="rateClass"></param>
        /// <param name="startDate"></param>
        /// <param name="endDate"></param>
        /// <param name="projectedEndDate"></param>
        /// <param name="costToDateExtParam"></param>
        /// <param name="getNewestDefAndResetStartDate"></param>
        /// <returns></returns>
        public CostToDateResult CostToDate(int rateCompanyID, List<Reading> readings, List<Baseline> baselines, string rateClass, DateTime startDate, DateTime endDate, DateTime projectedEndDate, CostToDateExtParam costToDateExtParam, bool getNewestDefAndResetStartDate)
        {
            CostToDateResult result;
            CE.RateEngine.CostToDateExtParam extParamEP;
            CE.RateEngine.ReadingCollection readingsEP;
            CE.RateEngine.CostToDateResult resultEP;
            CE.RateEngine.BaselineCollection baselinesEP;

            readingsEP = MapToReadingsEP(readings);

            baselinesEP = MapToBaselinesEP(baselines);

            extParamEP = MapToCostToDateExtParamEP(costToDateExtParam);

            // CR 51610 Oct 2014 - pass either usage collection or reading collection to cost to date based on the interval type
            var interval = readingsEP.DetermineIntervalOfReadings();
            if (interval == CE.RateEngine.Enums.ReadingInterval.Daily)
            {
                // set extra param to indication daily only data
                extParamEP.IsDailyOnlyReadings = true;
            }

            resultEP = _rateEngine.CostToDate(rateCompanyID, readingsEP, baselinesEP, rateClass, startDate, endDate, projectedEndDate, extParamEP, getNewestDefAndResetStartDate);

            result = MapFromCostToDateResultEP(resultEP);
            DetailedRateDefinitionCollection detailedDefs = null;
            if (getNewestDefAndResetStartDate)
            {
                const int yearOffset = 5;
                var defStartDate = DateTime.Now.AddYears(yearOffset);
                var defEndDate = defStartDate.AddDays(2);

                detailedDefs = _rateEngine.GetDetailedRateInformation(rateCompanyID, rateClass, defStartDate, defEndDate, -yearOffset);
            }
            else
            {
                detailedDefs = _rateEngine.GetDetailedRateInformation(rateCompanyID, rateClass, startDate, endDate);
            }
            if (detailedDefs[0].IsDemand && !extParamEP.IncludeDailyDemandCalc)
                result.DailyDemandCalcExcluded = true;
            else
                result.DailyDemandCalcExcluded = false;

            return (result);
        }

        /// <summary>
        /// Use this for a CostToDate calculation after processing readings and adding to provided usage, ICostToDate 
        /// </summary>
        /// <param name="rateCompanyID"></param>
        /// <param name="readings"></param>
        /// <param name="currentUsages"></param>
        /// <param name="rateClass"></param>
        /// <param name="startDate"></param>
        /// <param name="endDate"></param>
        /// <param name="projectedEndDate"></param>
        /// <returns></returns>
        public CostToDateResult CostToDate(int rateCompanyID, List<Reading> readings, List<Usage> currentUsages, string rateClass, DateTime startDate, DateTime endDate, DateTime projectedEndDate, CostToDateExtParam costToDateExtParam)
        {
            CostToDateResult result;
            CE.RateEngine.CostToDateExtParam extParamEP;
            CE.RateEngine.UsageCollection usagesEP;
            CE.RateEngine.ReadingCollection readingsEP;
            CE.RateEngine.CostToDateResult resultEP;

            readingsEP = MapToReadingsEP(readings);

            usagesEP = MapToUsagesEP(currentUsages);

            extParamEP = MapToCostToDateExtParamEP(costToDateExtParam);

            // CR 51610 Oct 2014 - pass either usage collection or reading collection to cost to date based on the interval type
            var interval = readingsEP.DetermineIntervalOfReadings();
            if (interval == CE.RateEngine.Enums.ReadingInterval.Daily)
            {
                // set extra param to indication daily only data
                extParamEP.IsDailyOnlyReadings = true;

            }

            resultEP = _rateEngine.CostToDate(rateCompanyID, readingsEP, usagesEP, rateClass, startDate, endDate, projectedEndDate, extParamEP);

            result = MapFromCostToDateResultEP(resultEP);
            DetailedRateDefinitionCollection detailedDefs = null;
            detailedDefs = _rateEngine.GetDetailedRateInformation(rateCompanyID, rateClass, startDate, endDate);

            if (detailedDefs[0].IsDemand && !extParamEP.IncludeDailyDemandCalc)
                result.DailyDemandCalcExcluded = true;
            else
                result.DailyDemandCalcExcluded = false;

            return (result);
        }

        /// <summary>
        /// Use this for CostToDate calculation when all usage determinants are provided, ICostToDate
        /// </summary>
        /// <param name="rateCompanyID"></param>
        /// <param name="usages"></param>
        /// <param name="rateClass"></param>
        /// <param name="startDate"></param>
        /// <param name="endDate"></param>
        /// <param name="projectedEndDate"></param>
        /// <returns></returns>
        public CostToDateResult CostToDate(int rateCompanyID, List<Usage> usages, string rateClass, DateTime startDate, DateTime endDate, DateTime projectedEndDate, CostToDateExtParam costToDateExtParam)
        {
            CostToDateResult result;
            CE.RateEngine.CostToDateExtParam extParamEP;
            CE.RateEngine.UsageCollection usagesEP;
            CE.RateEngine.CostToDateResult resultEP;

            usagesEP = MapToUsagesEP(usages);

            extParamEP = MapToCostToDateExtParamEP(costToDateExtParam);

            resultEP = _rateEngine.CostToDate(rateCompanyID, usagesEP, rateClass, startDate, endDate, projectedEndDate, extParamEP);

            result = MapFromCostToDateResultEP(resultEP);

            return (result);
        }

        // Mapping to EP
        private CE.RateEngine.UsageCollection MapToUsagesEP(List<Usage> usages)
        {
            CE.RateEngine.UsageCollection usagesEP;
            CE.RateEngine.Usage usageEP;
            CE.RateEngine.Enums.BaseOrTier baseOrTierEP;
            CE.RateEngine.Enums.TimeOfUse timeOfUseEP;
            CE.RateEngine.Enums.Season seasonEP;
            Usage usage;
            int i = 0;

            usagesEP = new CE.RateEngine.UsageCollection();

            for (i = 0; i < usages.Count; i++)
            {
                usage = usages[i];

                switch (usage.BaseOrTier)
                {
                    case Enums.BaseOrTier.TotalServiceUse:
                        baseOrTierEP = CE.RateEngine.Enums.BaseOrTier.TotalServiceUse;
                        break;

                    case Enums.BaseOrTier.Undefined:
                        baseOrTierEP = CE.RateEngine.Enums.BaseOrTier.Undefined;
                        break;

                    default:
                        baseOrTierEP = CE.RateEngine.Enums.BaseOrTier.Undefined;
                        break;
                }

                switch (usage.TimeOfUse)
                {
                    case Enums.TimeOfUse.OffPeak:
                        timeOfUseEP = CE.RateEngine.Enums.TimeOfUse.OffPeak;
                        break;

                    case Enums.TimeOfUse.OnPeak:
                        timeOfUseEP = CE.RateEngine.Enums.TimeOfUse.OnPeak;
                        break;

                    case Enums.TimeOfUse.Shoulder1:
                        timeOfUseEP = CE.RateEngine.Enums.TimeOfUse.Shoulder1;
                        break;

                    case Enums.TimeOfUse.Shoulder2:
                        timeOfUseEP = CE.RateEngine.Enums.TimeOfUse.Shoulder2;
                        break;

                    case Enums.TimeOfUse.CriticalPeak:
                        timeOfUseEP = CE.RateEngine.Enums.TimeOfUse.CriticalPeak;
                        break;

                    default:
                        timeOfUseEP = CE.RateEngine.Enums.TimeOfUse.Undefined;
                        break;
                }

                switch (usage.Season)
                {
                    case Enums.Season.Undefined:
                        seasonEP = CE.RateEngine.Enums.Season.Undefined;
                        break;

                    default:
                        seasonEP = CE.RateEngine.Enums.Season.Undefined;
                        break;
                }

                // create the Rate Engine Usage 
                usageEP = new CE.RateEngine.Usage(baseOrTierEP, timeOfUseEP, seasonEP, usage.Quantity);

                // add to usages
                usagesEP.Add(usageEP);

            }

            return (usagesEP);
        }

        private CE.RateEngine.ReadingCollection MapToReadingsEP(List<Reading> readings)
        {
            CE.RateEngine.ReadingCollection readingsEP;
            CE.RateEngine.Reading readingEP;
            CE.RateEngine.Enums.BaseOrTier baseOrTierEP;
            CE.RateEngine.Enums.TimeOfUse timeOfUseEP;
            CE.RateEngine.Enums.Season seasonEP;
            CE.RateEngine.Enums.DayType dayTypeEP;
            CE.RateEngine.Enums.SpecialIntervalType specialInterval;
            Reading reading;
            int i = 0;

            readingsEP = new CE.RateEngine.ReadingCollection();

            for (i = 0; i < readings.Count; i++)
            {
                reading = readings[i];

                switch (reading.BaseOrTier)
                {
                    case Enums.BaseOrTier.TotalServiceUse:
                        baseOrTierEP = CE.RateEngine.Enums.BaseOrTier.TotalServiceUse;
                        break;

                    case Enums.BaseOrTier.Undefined:
                        baseOrTierEP = CE.RateEngine.Enums.BaseOrTier.Undefined;
                        break;

                    default:
                        baseOrTierEP = CE.RateEngine.Enums.BaseOrTier.TotalServiceUse;
                        break;
                }

                switch (reading.TimeOfUse)
                {
                    case Enums.TimeOfUse.OffPeak:
                        timeOfUseEP = CE.RateEngine.Enums.TimeOfUse.OffPeak;
                        break;

                    case Enums.TimeOfUse.OnPeak:
                        timeOfUseEP = CE.RateEngine.Enums.TimeOfUse.OnPeak;
                        break;

                    case Enums.TimeOfUse.Shoulder1:
                        timeOfUseEP = CE.RateEngine.Enums.TimeOfUse.Shoulder1;
                        break;

                    case Enums.TimeOfUse.Shoulder2:
                        timeOfUseEP = CE.RateEngine.Enums.TimeOfUse.Shoulder2;
                        break;

                    case Enums.TimeOfUse.CriticalPeak:
                        timeOfUseEP = CE.RateEngine.Enums.TimeOfUse.CriticalPeak;
                        break;

                    default:
                        timeOfUseEP = CE.RateEngine.Enums.TimeOfUse.Undefined;
                        break;
                }

                switch (reading.Season)
                {
                    case Enums.Season.Summer:
                        seasonEP = CE.RateEngine.Enums.Season.Summer;
                        break;

                    case Enums.Season.Winter:
                        seasonEP = CE.RateEngine.Enums.Season.Winter;
                        break;

                    case Enums.Season.SeasonA:
                        seasonEP = CE.RateEngine.Enums.Season.SeasonA;
                        break;

                    case Enums.Season.SeasonB:
                        seasonEP = CE.RateEngine.Enums.Season.SeasonB;
                        break;

                    case Enums.Season.Undefined:
                        seasonEP = CE.RateEngine.Enums.Season.Undefined;
                        break;

                    default:
                        seasonEP = CE.RateEngine.Enums.Season.Undefined;
                        break;
                }

                switch (reading.Timestamp.DayOfWeek)
                {
                    case System.DayOfWeek.Monday:
                    case System.DayOfWeek.Tuesday:
                    case System.DayOfWeek.Wednesday:
                    case System.DayOfWeek.Thursday:
                    case System.DayOfWeek.Friday:
                        dayTypeEP = CE.RateEngine.Enums.DayType.Weekday;
                        break;

                    case System.DayOfWeek.Saturday:
                    case System.DayOfWeek.Sunday:
                        dayTypeEP = CE.RateEngine.Enums.DayType.WeekendHoliday;
                        break;

                    default:
                        dayTypeEP = CE.RateEngine.Enums.DayType.WeekendHoliday;
                        break;

                }

                switch (reading.SpecialInterval)
                {
                    case Enums.SpecialIntervalType.Interval2Hour:
                        specialInterval = CE.RateEngine.Enums.SpecialIntervalType.Interval2Hour;
                        break;
                    case Enums.SpecialIntervalType.Interval4Hour:
                        specialInterval = CE.RateEngine.Enums.SpecialIntervalType.Interval4Hour;
                        break;
                    case Enums.SpecialIntervalType.Interval6Hour:
                        specialInterval = CE.RateEngine.Enums.SpecialIntervalType.Interval6Hour;
                        break;
                    case Enums.SpecialIntervalType.Interval12Hour:
                        specialInterval = CE.RateEngine.Enums.SpecialIntervalType.Interval12Hour;
                        break;
                    default:
                        specialInterval = CE.RateEngine.Enums.SpecialIntervalType.IntervalRegular;
                        break;
                }


                // create the Rate Engine Reading 
                readingEP = new CE.RateEngine.Reading(baseOrTierEP, timeOfUseEP, seasonEP, reading.Timestamp, reading.Quantity, dayTypeEP, reading.IsEmpty, false, reading.IsGap, specialInterval);

                // add to readings
                readingsEP.Add(readingEP);

            }

            return (readingsEP);
        }

        private CE.RateEngine.BaselineCollection MapToBaselinesEP(List<Baseline> baselines)
        {
            CE.RateEngine.BaselineCollection baselinesEP = null;

            if (baselines != null)
            {
                baselinesEP = new CE.RateEngine.BaselineCollection();

                for (int i = 0; i < baselines.Count; i++)
                {
                    var b = baselines[i];

                    // create the Rate Engine Baseline 
                    var baselineEP = new CE.RateEngine.Baseline()
                    {
                        UsageDate = b.UsageDate,
                        ActualUsage = b.ActualUsage,
                        ActualUsageMissing = b.ActualUsageMissing,
                        BaselineUsage = b.BaselineUsage,
                        BaselineMissing = b.BaselineUsageMissing,
                        RebateAmount = b.RebateAmount,
                        RebateMissing = b.RebateAmountMissing
                    };

                    // add to baselines
                    baselinesEP.Add(baselineEP);
                }
            }

            return (baselinesEP);
        }

        private CE.RateEngine.Enums.MinimumChargeType MapToMinimumChargeTypeEP(Enums.MinimumChargeType minimumChargeType)
        {
            CE.RateEngine.Enums.MinimumChargeType result;

            switch (minimumChargeType)
            {
                case Enums.MinimumChargeType.NoMinimumCharge:
                    result = CE.RateEngine.Enums.MinimumChargeType.NoMinimumCharge;
                    break;

                case Enums.MinimumChargeType.ProrateMinimumCharge:
                    result = CE.RateEngine.Enums.MinimumChargeType.ProrateMinimumCharge;
                    break;

                case Enums.MinimumChargeType.NoProrateMinimumCharge:
                    result = CE.RateEngine.Enums.MinimumChargeType.NoProrateMinimumCharge;
                    break;

                default:
                    result = CE.RateEngine.Enums.MinimumChargeType.NoMinimumCharge;
                    break;
            }

            return (result);
        }

        private CE.RateEngine.CostToDateExtParam MapToCostToDateExtParamEP(CostToDateExtParam extParam)
        {
            CE.RateEngine.CostToDateExtParam result;
            CostToDateExtParam p = null;

            if (extParam == null)
            {
                p = new CostToDateExtParam();
            }
            else
            {
                p = extParam;
            }

            result = new CE.RateEngine.CostToDateExtParam(p.ProrateMonthlyServiceCharges,
                                                             MapToMinimumChargeTypeEP(p.MinimumChargeType),
                                                             p.ProrateDemandUsageDeterminants,
                                                             p.SmoothTiers,
                                                             p.CheckDaysInFullMonth,
                                                             p.DaysInFullMonth,
                                                             p.ProjectedNumDays,
                                                             p.DSTEndDate,
                                                             p.AllowRebateCalculations,
                                                             p.AllowBaselineCalculations);

            // advanced settings to map explicitely.
            result.UseProjectedEndDateWhenFinalizingTierBoundaries = p.UseProjectedEndDateWhenFinalizingTierBoundaries;
            result.AdjustReadings = p.AdjustReadings;
            result.OmitProjectedCostCalculation = p.OmitProjectedCostCalculation;
            result.InlinePeakEvents = p.InlinePeakEvents;
            result.GeneralConservationRAF = p.GeneralConservationRAF;
            result.TypicalResponseRAF = p.TypicalResponseRAF;
            result.CriticalResponseRAF = p.CriticalResponseRAF;
            result.DemandControlRAF = p.DemandControlRAF;
            result.PercentReductionAsShiftedRAF = p.PercentReductionAsShiftedRAF;
            result.PercentCriticalReductionAsShiftedRAF = p.PercentCriticalReductionAsShiftedRAF;
            result.IncludeDailyDemandCalc = p.IncludeDailyDemandCalc;

            if (p.PeakEventInformation != null)
            {
                result.PeakEventInformation = MapToPeakEventInfoEP(p.PeakEventInformation);
            }

            return (result);
        }

        private CE.RateEngine.PeakEventInfo MapToPeakEventInfoEP(PeakEventInfo pei)
        {
            CE.RateEngine.PeakEventInfo result = null;
            CE.RateEngine.PeakEvent e = null;

            if (pei != null)
            {
                if (pei.PeakEvents != null)
                {
                    result = new CE.RateEngine.PeakEventInfo();
                    result.UseInlinePeakEvents = true;

                    foreach (var pe in pei.PeakEvents)
                    {
                        e = new CE.RateEngine.PeakEvent();
                        e.Datestamp = new DateTime(pe.StartDate.Year, pe.StartDate.Month, pe.StartDate.Day);

                        if (pe.Durations != null)
                        {
                            foreach (var dur in pe.Durations)
                            {
                                e.Durations.Add(new CE.RateEngine.SimDuration(MapToSeasonEP(dur.Season), dur.StartHour, dur.EndHour));
                            }
                        }
                        else
                        {
                            //default fallback if there are no durations.
                            e.Durations.Add(new SimDuration(CE.RateEngine.Enums.Season.Undefined, 12, 16));
                        }

                        result.PeakEvents.Add(e);
                    }
                }
            }

            return (result);
        }

        private CE.RateEngine.Enums.Season MapToSeasonEP(RateModel.Enums.Season s)
        {
            CE.RateEngine.Enums.Season result;

            switch (s)
            {
                case Enums.Season.Undefined:
                    result = CE.RateEngine.Enums.Season.Undefined;
                    break;

                case Enums.Season.Summer:
                    result = CE.RateEngine.Enums.Season.Summer;
                    break;

                case Enums.Season.Winter:
                    result = CE.RateEngine.Enums.Season.Winter;
                    break;

                case Enums.Season.SeasonA:
                    result = CE.RateEngine.Enums.Season.SeasonA;
                    break;

                case Enums.Season.SeasonB:
                    result = CE.RateEngine.Enums.Season.SeasonB;
                    break;

                default:
                    result = CE.RateEngine.Enums.Season.Undefined;
                    break;
            }

            return (result);
        }


        // Mapping from EP
        private List<Usage> MapFromUsagesEP(CE.RateEngine.UsageCollection usagesEP)
        {
            List<Usage> usages;
            Usage usage;
            Enums.BaseOrTier baseOrTier;
            Enums.TimeOfUse timeOfUse;
            Enums.Season season;
            CE.RateEngine.Usage usageEP;
            int i = 0;

            usages = new List<Usage>();

            if (usagesEP != null)
            {

                for (i = 0; i < usagesEP.Count; i++)
                {
                    usageEP = usagesEP[i];

                    switch (usageEP.BaseOrTier)
                    {
                        case CE.RateEngine.Enums.BaseOrTier.TotalServiceUse:
                            baseOrTier = Enums.BaseOrTier.TotalServiceUse;
                            break;

                        case CE.RateEngine.Enums.BaseOrTier.Undefined:
                            baseOrTier = Enums.BaseOrTier.Undefined;
                            break;

                        case CE.RateEngine.Enums.BaseOrTier.Tier1:
                            baseOrTier = Enums.BaseOrTier.Tier1;
                            break;

                        case CE.RateEngine.Enums.BaseOrTier.Tier2:
                            baseOrTier = Enums.BaseOrTier.Tier2;
                            break;

                        case CE.RateEngine.Enums.BaseOrTier.Tier3:
                            baseOrTier = Enums.BaseOrTier.Tier3;
                            break;

                        case CE.RateEngine.Enums.BaseOrTier.Tier4:
                            baseOrTier = Enums.BaseOrTier.Tier4;
                            break;

                        case CE.RateEngine.Enums.BaseOrTier.Tier5:
                            baseOrTier = Enums.BaseOrTier.Tier5;
                            break;

                        case CE.RateEngine.Enums.BaseOrTier.Tier6:
                            baseOrTier = Enums.BaseOrTier.Tier6;
                            break;

                        case CE.RateEngine.Enums.BaseOrTier.Demand:
                            baseOrTier = Enums.BaseOrTier.Demand;
                            break;

                        case CE.RateEngine.Enums.BaseOrTier.DemandBill:
                            baseOrTier = Enums.BaseOrTier.DemandBill;
                            break;

                        case CE.RateEngine.Enums.BaseOrTier.DemandBill2:
                            baseOrTier = Enums.BaseOrTier.DemandBill2;
                            break;

                        case CE.RateEngine.Enums.BaseOrTier.DemandBill3:
                            baseOrTier = Enums.BaseOrTier.DemandBill3;
                            break;

                        default:
                            baseOrTier = Enums.BaseOrTier.Undefined;
                            break;
                    }

                    switch (usageEP.TimeOfUse)
                    {
                        case CE.RateEngine.Enums.TimeOfUse.OffPeak:
                            timeOfUse = Enums.TimeOfUse.OffPeak;
                            break;

                        case CE.RateEngine.Enums.TimeOfUse.OnPeak:
                            timeOfUse = Enums.TimeOfUse.OnPeak;
                            break;

                        case CE.RateEngine.Enums.TimeOfUse.Shoulder1:
                            timeOfUse = Enums.TimeOfUse.Shoulder1;
                            break;

                        case CE.RateEngine.Enums.TimeOfUse.Shoulder2:
                            timeOfUse = Enums.TimeOfUse.Shoulder2;
                            break;

                        case CE.RateEngine.Enums.TimeOfUse.CriticalPeak:
                            timeOfUse = Enums.TimeOfUse.CriticalPeak;
                            break;

                        case CE.RateEngine.Enums.TimeOfUse.Undefined:
                            timeOfUse = Enums.TimeOfUse.Undefined;
                            break;

                        default:
                            timeOfUse = Enums.TimeOfUse.Undefined;
                            break;
                    }

                    switch (usageEP.Season)
                    {
                        case CE.RateEngine.Enums.Season.Undefined:
                            season = Enums.Season.Undefined;
                            break;

                        case CE.RateEngine.Enums.Season.Summer:
                            season = Enums.Season.Summer;
                            break;

                        case CE.RateEngine.Enums.Season.Winter:
                            season = Enums.Season.Winter;
                            break;

                        case CE.RateEngine.Enums.Season.SeasonA:
                            season = Enums.Season.SeasonA;
                            break;

                        case CE.RateEngine.Enums.Season.SeasonB:
                            season = Enums.Season.SeasonB;
                            break;

                        default:
                            season = Enums.Season.Undefined;
                            break;
                    }

                    // create new usage
                    usage = new Usage(baseOrTier, timeOfUse, season, usageEP.Quantity, usageEP.Datestamp);

                    // add to usages
                    usages.Add(usage);

                }

            }

            return (usages);
        }

        private PTRResult MapFromPeakTimeRebatesEP(CE.RateEngine.PTRResult ptrResultEP)
        {
            PTRResult ptrResult = null;

            if (ptrResultEP != null)
            {
                ptrResult = new PTRResult();
                ptrResult.RebateAmount = ptrResultEP.RebateAmount;

                switch (ptrResultEP.OverallAccountingRule)
                {
                    case CE.RateEngine.Enums.RebateAccountingRule.EachEvent:
                        ptrResult.OverallAccountingRule = Enums.RebateAccountingRule.EachEvent;
                        break;

                    case CE.RateEngine.Enums.RebateAccountingRule.EntireBillPeriod:
                        ptrResult.OverallAccountingRule = Enums.RebateAccountingRule.EntireBillPeriod;
                        break;

                    case CE.RateEngine.Enums.RebateAccountingRule.Unspecified:
                        ptrResult.OverallAccountingRule = Enums.RebateAccountingRule.Unspecified;
                        break;
                }

                ptrResult.PeakTimeRebateEvents = MapFromPTREventsEP(ptrResultEP.PTREvents);

            }

            return (ptrResult);
        }

        private List<PTREvent> MapFromPTREventsEP(CE.RateEngine.PTREventCollection ptrEventsEP)
        {
            var events = new List<PTREvent>();

            if (ptrEventsEP != null)
            {
                for (var i = 0; i < ptrEventsEP.Count; i++)
                {
                    var e = new PTREvent();
                    e.StartDate = ptrEventsEP[i].StartDate;
                    e.EndDate = ptrEventsEP[i].EndDate;

                    switch (ptrEventsEP[i].BaselineRule)
                    {
                        case CE.RateEngine.Enums.BaselineRule.ThreeOfTen:
                            e.BaselineRule = Enums.BaselineRule.ThreeOfTen;
                            break;

                        case CE.RateEngine.Enums.BaselineRule.FiveOfThirty:
                            e.BaselineRule = Enums.BaselineRule.FiveOfThirty;
                            break;

                        case CE.RateEngine.Enums.BaselineRule.Unspecified:
                            e.BaselineRule = Enums.BaselineRule.Unspecified;
                            break;
                    }

                    e.Rebate = MapFromPTRRebateEP(ptrEventsEP[i].Rebate);

                    events.Add(e);
                }

            }

            return (events);
        }

        private PTREventRebate MapFromPTRRebateEP(CE.RateEngine.PTREventRebate ptrRebateEP)
        {
            var eventRebate = new PTREventRebate();

            eventRebate.RebateAmount = ptrRebateEP.RebateAmount;
            eventRebate.ActualUsage = ptrRebateEP.ActualUsage;
            eventRebate.ActualUsageCalculated = ptrRebateEP.ActualUsageCalculated;
            eventRebate.ActualUsageProvided = ptrRebateEP.ActualUsageProvided;
            eventRebate.BaselineCalculated = ptrRebateEP.BaselineCalculated;
            eventRebate.BaselineUsage = ptrRebateEP.BaselineUsage;
            eventRebate.BaselineUsageEmpty = ptrRebateEP.BaselineUsageEmpty;
            eventRebate.MissingActualUsage = ptrRebateEP.MissingActualUsage;
            eventRebate.RealSavingsUsage = ptrRebateEP.RealSavingsUsage;
            eventRebate.RebateAmountCalculated = ptrRebateEP.RebateAmountCalculated;
            eventRebate.RebateAmountProvided = ptrRebateEP.RebateAmountProvided;
            eventRebate.SavingsUsage = ptrRebateEP.SavingsUsage;
            eventRebate.UnitPrice = ptrRebateEP.UnitPrice;
            eventRebate.Valid = ptrRebateEP.Valid;

            switch (ptrRebateEP.AccountingRule)
            {
                case CE.RateEngine.Enums.RebateAccountingRule.EachEvent:
                    eventRebate.RebateAccountingRule = Enums.RebateAccountingRule.EachEvent;
                    break;

                case CE.RateEngine.Enums.RebateAccountingRule.EntireBillPeriod:
                    eventRebate.RebateAccountingRule = Enums.RebateAccountingRule.EntireBillPeriod;
                    break;

                case CE.RateEngine.Enums.RebateAccountingRule.Unspecified:
                    eventRebate.RebateAccountingRule = Enums.RebateAccountingRule.Unspecified;
                    break;
            }

            return (eventRebate);
        }

        private List<Cost> MapFromCostsEP(CE.RateEngine.CostCollection costsEP)
        {
            var costs = new List<Cost>(); ;
            CE.RateEngine.Cost costEP;

            if (costsEP != null)
            {
                foreach (System.Collections.DictionaryEntry c in costsEP)
                {
                    costEP = (CE.RateEngine.Cost)c.Value;
                    costs.Add(new Cost(costEP.CostType.ToString(), costEP.Amount));
                }
            }

            return (costs);
        }

        private NetMeteringMonthlyResult MapFromNetMeteringResultsEP(CE.RateEngine.NetMeteringMonthlyResult resultEP)
        {
            NetMeteringMonthlyResult netResult = null;

            if (resultEP != null)
            {
                netResult = new NetMeteringMonthlyResult();

                switch (resultEP.NetMeteringType)
                {
                    case CE.RateEngine.Enums.NetMeteringType.LikeEPR6:
                        netResult.NetMeteringType = Enums.NetMeteringType.LikeEPR6;
                        break;

                    case CE.RateEngine.Enums.NetMeteringType.Nem3:
                        netResult.NetMeteringType = Enums.NetMeteringType.Nem3;
                        break;

                    case CE.RateEngine.Enums.NetMeteringType.Nem4:
                        netResult.NetMeteringType = Enums.NetMeteringType.Nem4;
                        break;

                    case CE.RateEngine.Enums.NetMeteringType.Nem5:
                        netResult.NetMeteringType = Enums.NetMeteringType.Nem5;
                        break;

                    case CE.RateEngine.Enums.NetMeteringType.Unspecified:
                        netResult.NetMeteringType = Enums.NetMeteringType.Unspecified;
                        break;

                }

                netResult.RegularCost = resultEP.RegularCost;
                netResult.MinimumCost = resultEP.MinimumCost;
                netResult.CarryoverCharge = resultEP.CarryoverCharge;
                netResult.MinimumCosts = MapFromCostsEP(resultEP.MinimumCosts);
            }

            return (netResult);
        }

        private CostToDateResult MapFromCostToDateResultEP(CE.RateEngine.CostToDateResult resultEP)
        {
            CostToDateResult result;

            result = new CostToDateResult();

            result.Cost = resultEP.Cost;
            result.ProjectedCost = resultEP.ProjectedCost;
            result.StartDate = resultEP.StartDate;
            result.EndDate = resultEP.EndDate;
            result.ProjectedEndDate = resultEP.ProjectedEndDate;
            result.NumberOfDaysInCostToDate = resultEP.NumberOfDaysInCostToDate;
            result.NumberOfDaysInProjectedCost = resultEP.NumberOfDaysInProjectedCost;
            result.MonthProrateFactorUsed = resultEP.MonthProrateFactor;
            result.UsageScaleFactorUsed = resultEP.UsageScaleFactor;
            result.MinimumCostsApplied = resultEP.MinimumCostsApplied;
            result.AverageDailyCost = resultEP.AverageDailyCost;
            result.LastReadingDate = resultEP.LastReadingDate;

            switch (resultEP.ErrorCode)
            {
                case CE.RateEngine.Enums.ErrCode.NoError:
                    result.ErrCode = Enums.ErrorCode.NoError;
                    break;

                case CE.RateEngine.Enums.ErrCode.ErrAllReadingsNotProvidedForRTP:
                    result.ErrCode = Enums.ErrorCode.ErrAllReadingsNotProvidedForRTP;
                    break;

                case CE.RateEngine.Enums.ErrCode.ErrGeneric:
                    result.ErrCode = Enums.ErrorCode.ErrGeneric;
                    break;

                case CE.RateEngine.Enums.ErrCode.ErrUnsupportedCalculation:
                    result.ErrCode = Enums.ErrorCode.ErrUnsupportedCalculation;
                    break;

                case CE.RateEngine.Enums.ErrCode.ErrUnsupportedComplexTieredTOU:
                    result.ErrCode = Enums.ErrorCode.ErrUnsupportedComplexTieredTOU;
                    break;

                case CE.RateEngine.Enums.ErrCode.ErrTOUComplex:
                    result.ErrCode = Enums.ErrorCode.ErrTOUComplex;
                    break;

                case CE.RateEngine.Enums.ErrCode.ErrRateDefinitionsNotFound:
                    result.ErrCode = Enums.ErrorCode.ErrRateDefinitionsNotFound;
                    break;

                case CE.RateEngine.Enums.ErrCode.ErrUnsupportedProjectedCalculation:
                    result.ErrCode = Enums.ErrorCode.ErrUnsupportedProjectedCalculation;
                    break;

                case CE.RateEngine.Enums.ErrCode.ErrUnableToApplyRateClassModifier:
                    result.ErrCode = Enums.ErrorCode.ErrUnableToApplyRateClassModifier;
                    break;
                case CE.RateEngine.Enums.ErrCode.ErrRequiredReadingsAreMissing:
                    result.ErrCode = Enums.ErrorCode.ErrRequiredReadingsAreMissing;
                    break;
                default:
                    result.ErrCode = Enums.ErrorCode.NoError;
                    break;

            }

            switch (resultEP.ProjectedCostErrorCode)
            {
                case CE.RateEngine.Enums.ErrCode.NoError:
                    result.ProjectedCostErrCode = Enums.ErrorCode.NoError;
                    break;

                case CE.RateEngine.Enums.ErrCode.ErrAllReadingsNotProvidedForRTP:
                    result.ProjectedCostErrCode = Enums.ErrorCode.ErrAllReadingsNotProvidedForRTP;
                    break;

                case CE.RateEngine.Enums.ErrCode.ErrGeneric:
                    result.ProjectedCostErrCode = Enums.ErrorCode.ErrGeneric;
                    break;

                case CE.RateEngine.Enums.ErrCode.ErrUnsupportedCalculation:
                    result.ProjectedCostErrCode = Enums.ErrorCode.ErrUnsupportedCalculation;
                    break;

                case CE.RateEngine.Enums.ErrCode.ErrUnsupportedComplexTieredTOU:
                    result.ProjectedCostErrCode = Enums.ErrorCode.ErrUnsupportedComplexTieredTOU;
                    break;

                case CE.RateEngine.Enums.ErrCode.ErrTOUComplex:
                    result.ProjectedCostErrCode = Enums.ErrorCode.ErrTOUComplex;
                    break;

                case CE.RateEngine.Enums.ErrCode.ErrRateDefinitionsNotFound:
                    result.ProjectedCostErrCode = Enums.ErrorCode.ErrRateDefinitionsNotFound;
                    break;

                case CE.RateEngine.Enums.ErrCode.ErrUnsupportedProjectedCalculation:
                    result.ProjectedCostErrCode = Enums.ErrorCode.ErrUnsupportedProjectedCalculation;
                    break;

                case CE.RateEngine.Enums.ErrCode.ErrUnableToApplyRateClassModifier:
                    result.ProjectedCostErrCode = Enums.ErrorCode.ErrUnableToApplyRateClassModifier;
                    break;
                case CE.RateEngine.Enums.ErrCode.ErrRequiredReadingsAreMissing:
                    result.ProjectedCostErrCode = Enums.ErrorCode.ErrRequiredReadingsAreMissing;
                    break;
                default:
                    result.ProjectedCostErrCode = Enums.ErrorCode.NoError;
                    break;

            }

            result.Usages = MapFromUsagesEP(resultEP.Usages);
            result.ProjectedUsages = MapFromUsagesEP(resultEP.ProjectedUsages);

            result.PeakTimeRebates = MapFromPeakTimeRebatesEP(resultEP.PeakTimeRebates);
            result.ProjectedPeakTimeRebates = result.PeakTimeRebates;
            // ProjectedPeakTimeRebates uses pointer to other results at this time, rather than creating same object again
            // result.ProjectedPeakTimeRebates = MapFromPeakTimeRebatesEP(resultEP.ProjectedPeakTimeRebates);

            result.Costs = MapFromCostsEP(resultEP.Costs);
            result.ProjectedCosts = MapFromCostsEP(resultEP.ProjectedCosts);

            result.NetMeteringResult = MapFromNetMeteringResultsEP(resultEP.NetMeteringResult);
            result.ProjectedNetMeteringResult = MapFromNetMeteringResultsEP(resultEP.ProjectedNetMeteringResult);

            return (result);
        }

        #endregion


        #region GetRateClassList and GetRateClassInformation

        /// <summary>
        /// Get a list of rate classes for a client.  All available rate classes are returned in a list.
        /// null will be returned if no rate classes could be returned. 
        /// </summary>
        /// <param name="rateCompanyID"></param>
        /// <returns></returns>
        public RateClassList GetRateClassList(int rateCompanyID)
        {
            RateClassList result = null;
            CE.RateEngine.SimpleRateDefinitionCollection rateDefsEP;
            DateTime startDate = DateTime.MinValue;
            DateTime endDate = DateTime.MinValue;

            startDate = DateTime.Now;
            endDate = DateTime.Now.AddDays(1);

            try
            {
                rateDefsEP = _rateEngine.GetRateClassList(rateCompanyID, startDate, endDate);

                result = MapFromSimpleRateDefinitionsToResultEP(rateDefsEP);
            }
            catch (Exception)
            {
                // right now be sure result is null; caller shall check for null
                throw;
            }

            return (result);
        }

        /// <summary>
        /// Get basic information on a rate class. Most recent rate definition is retrieved using a date of today. 
        /// </summary>
        /// <param name="rateCompanyID"></param>
        /// <returns></returns>
        public RateClass GetRateClassInformation(int rateCompanyID, string rateClass)
        {
            RateClass finalResult = null;
            DateTime startDate = DateTime.MinValue;

            startDate = DateTime.Now;

            finalResult = GetRateClassInformation(rateCompanyID, rateClass, startDate);

            return (finalResult);
        }

        public RateClass GetRateClassInformation(int rateCompanyID, string rateClass, DateTime startDate)
        {
            RateClass finalResult = null;
            RateClassList result = null;
            CE.RateEngine.SimpleRateDefinitionCollection rateDefsEP;
            DateTime endDate = DateTime.MinValue;
            endDate = startDate.AddDays(1);

            try
            {
                rateDefsEP = _rateEngine.GetSimpleRateInformation(rateCompanyID, rateClass, startDate, endDate);

                result = MapFromSimpleRateDefinitionsToResultEP(rateDefsEP);

                if (result != null && result.Count > 0)
                {
                    finalResult = result[0];
                }
            }
            catch (Exception)
            {
                // right now be sure result is null; caller shall check for null
                throw;
            }

            return (finalResult);
        }

        public DetailedRateClass GetDetailsRateClassInformation(int rateCompanyID, string rateClass, DateTime startDate)
        {
            DetailedRateClass finalResult = null;
            List<DetailedRateClass> result = null;
            DateTime endDate = DateTime.MinValue;
            endDate = startDate.AddDays(1);

            CE.RateEngine.DetailedRateDefinitionCollection rateDefsEP;
            try
            {
                rateDefsEP = _rateEngine.GetDetailedRateInformation(rateCompanyID, rateClass, startDate, endDate);
                result = MapFromDetailedRateDefinitionsToResultEP(rateDefsEP);

                if (result != null && result.Count > 0)
                    finalResult = result[0];
            }
            catch (Exception)
            {
                // right now be sure result is null; caller shall check for null
                throw;
            }

            return finalResult;
        }



        private List<DetailedRateClass> MapFromDetailedRateDefinitionsToResultEP(CE.RateEngine.DetailedRateDefinitionCollection rateDefsEP)
        {
            List<DetailedRateClass> result = null;
            DetailedRateClass rateClass = null;

            if (rateDefsEP != null)
            {
                if (rateDefsEP.Count > 0)
                {
                    result = new List<DetailedRateClass>();

                    foreach (DetailedRateDefinition rateDef in rateDefsEP)
                    {
                        rateClass = new DetailedRateClass();


                        var useCharges = new List<UseCharge>();
                        foreach(RateEngine.UseCharge reUseCharge in rateDef.UseCharges)
                        {
                            var useCharge = new UseCharge
                            {
                                BaseOrTier = (Enums.BaseOrTier)reUseCharge.BaseOrTier,
                                ChargeValue = reUseCharge.ChargeValue,
                                CostType = reUseCharge.CostType.ToString(),
                                FCRGroup = reUseCharge.FCRGroup,
                                FuelCostRecovery = reUseCharge.FuelCostRecovery,
                                PartType = (Enums.PartType)reUseCharge.PartType,
                                RealTimePricing = reUseCharge.RealTimePricing,
                                RTPGroup = reUseCharge.RTPGroup,
                                Season = (Enums.Season)reUseCharge.Season,
                                TimeOfUse = (Enums.TimeOfUse)reUseCharge.TimeOfUse
                            };

                            useCharges.Add(useCharge);
                        }
                        var serviceCharges = new List<ServiceCharge>();
                        foreach (RateEngine.ServiceCharge reServiceCharge in rateDef.ServiceCharges)
                        {
                            var serviceCharge = new ServiceCharge
                            {
                                CalculationType = (Enums.ChargeCalcType)reServiceCharge.CalculationType,
                                ChargeValue = reServiceCharge.ChargeValue,
                                CostType = reServiceCharge.CostType.ToString(),
                                RebateClass = (Enums.RebateClass)reServiceCharge.RebateClass,
                                Season = (Enums.Season)reServiceCharge.Season,
                                Step = (Enums.ServiceStep)reServiceCharge.Step
                            };
                            

                            serviceCharges.Add(serviceCharge);
                        }
                        var minCharges = new List<MinimumCharge>();
                        foreach (RateEngine.MinimumCharge reMinCharge in rateDef.MinimumCharges)
                        {
                            var minimumCharge = new MinimumCharge
                            {
                                CalculationType = (Enums.ChargeCalcType)reMinCharge.CalculationType,
                                ChargeValue = reMinCharge.ChargeValue,
                                CostType = reMinCharge.CostType.ToString(),
                            };


                            minCharges.Add(minimumCharge);
                        }
                        var taxCharges = new List<TaxCharge>();
                        foreach (RateEngine.TaxCharge reTaxCharge in rateDef.TaxCharges)
                        {
                            var taxCharge = new TaxCharge
                            {
                                ChargeValue = reTaxCharge.ChargeValue,
                                CostType = reTaxCharge.CostType.ToString(),
                            };


                            taxCharges.Add(taxCharge);
                        }
                        var tierBoundaries = new List<TierBoundary>();
                        foreach (RateEngine.TierBoundary reTierBoundary in rateDef.TierBoundaries)
                        {
                            var tierBoundary = new TierBoundary
                            {
                                BaseOrTier = (Enums.BaseOrTier)reTierBoundary.BaseOrTier,
                                Threshold = reTierBoundary.Threshold,
                                SecondaryThreshold = reTierBoundary.SecondaryThreshold,
                                Season = (Enums.Season)reTierBoundary.Season,
                                TimeOfUse = (Enums.TimeOfUse)reTierBoundary.TimeOfUse
                            };

                            tierBoundaries.Add(tierBoundary);
                        }
                        var seasonBoundaries = new List<SeasonBoundary>();
                        foreach (RateEngine.SeasonBoundary reSeasonBoundary in rateDef.SeasonBoundaries)
                        {
                            var seasonBoundary = new SeasonBoundary
                            {
                                EndDay = reSeasonBoundary.EndDay,
                                StartDay = reSeasonBoundary.StartDay,
                                Season = (Enums.Season)reSeasonBoundary.Season
                            };

                            seasonBoundaries.Add(seasonBoundary);
                        }
                        var touBoundaries = new List<TouBoundary>();
                        foreach (RateEngine.TOUBoundary reTouBoundary in rateDef.TOUBoundaries)
                        {
                            var touBoundary = new TouBoundary
                            {
                                DayType = (Enums.DayType)reTouBoundary.DayType,
                                EndTime = reTouBoundary.EndTime,
                                StartTime = reTouBoundary.StartTime,
                                Season = (Enums.Season)reTouBoundary.Season,
                                TimeOfUse = (Enums.TimeOfUse)reTouBoundary.TimeOfUse
                            };

                            touBoundaries.Add(touBoundary);
                        }

                        rateClass.RateClassID = rateDef.RateClass;
                        rateClass.Name = rateDef.Name;
                        rateClass.Description = rateDef.Description;
                        rateClass.Fuel = MapToFuel(rateDef.FuelType);
                        rateClass.CustomerType = MapToCustomerType(rateDef.CustomerType);
                        rateClass.ContainsPTR = rateDef.IsRTPRate && (rateDef.RTPStream == CE.RateEngine.Enums.RTPStream.PeakTimeRebateOnly);
                        rateClass.CommodityAsString = rateClass.Fuel.ToString().ToLower();
                        rateClass.UnitOfMeasureAsString = GetUnitOfMeasureAsString(rateDef.UnitOfMeasure);
                        rateClass.IsTiered = rateDef.IsTiered;
                        rateClass.IsTimeOfUse = rateDef.IsTimeOfUse;
                        rateClass.IsSeasonal = rateDef.IsSeasonal;
                        rateClass.ServiceCharges = serviceCharges;
                        if(useCharges.Count > 0)
                            rateClass.UseCharges = useCharges;
                        if (minCharges.Count > 0)
                            rateClass.MinimumCharges = minCharges;
                        if (taxCharges.Count > 0)
                            rateClass.TaxCharges = taxCharges;
                        if(rateClass.IsTiered && tierBoundaries.Count > 0)
                            rateClass.TierBoundaries = tierBoundaries;
                        if (rateClass.IsTimeOfUse && touBoundaries.Count > 0)
                            rateClass.TouBoundaries = touBoundaries;
                        if(rateClass.IsSeasonal && seasonBoundaries.Count > 0)
                            rateClass.SeasonBoundaries = seasonBoundaries;
                        
                        result.Add(rateClass);
                    }
                }
            }

            return (result);
        }

        private RateClassList MapFromSimpleRateDefinitionsToResultEP(CE.RateEngine.SimpleRateDefinitionCollection rateDefsEP)
        {
            RateClassList result = null;
            RateClass rateClass = null;

            if (rateDefsEP != null)
            {
                if (rateDefsEP.Count > 0)
                {
                    result = new RateClassList();

                    foreach (SimpleRateDefinition rateDef in rateDefsEP)
                    {
                        rateClass = new RateClass();

                        rateClass.RateClassID = rateDef.RateClass;
                        rateClass.Name = rateDef.Name;
                        rateClass.Description = rateDef.Description;
                        rateClass.Fuel = MapToFuel(rateDef.FuelType);
                        rateClass.CustomerType = MapToCustomerType(rateDef.CustomerType);
                        rateClass.ContainsPTR = rateDef.IsRTPRate && (rateDef.RTPStream == CE.RateEngine.Enums.RTPStream.PeakTimeRebateOnly);
                        rateClass.CommodityAsString = rateClass.Fuel.ToString().ToLower();
                        rateClass.UnitOfMeasureAsString = GetUnitOfMeasureAsString(rateDef.UnitOfMeasure);
                        rateClass.IsTiered = rateDef.IsTiered;
                        rateClass.IsTimeOfUse = rateDef.IsTimeOfUse;
                        rateClass.IsSeasonal = rateDef.IsSeasonal;
                        result.Add(rateClass);
                    }
                }
            }

            return (result);
        }

        private Enums.CustomerType MapToCustomerType(CE.RateEngine.Enums.CustomerType customerType)
        {
            Enums.CustomerType result;

            switch (customerType)
            {
                case CE.RateEngine.Enums.CustomerType.Commercial:
                    result = Enums.CustomerType.Commercial;
                    break;

                case CE.RateEngine.Enums.CustomerType.Residential:
                    result = Enums.CustomerType.Residential;
                    break;

                case CE.RateEngine.Enums.CustomerType.Farm:
                    result = Enums.CustomerType.Farm;
                    break;

                default:
                    throw new System.ArgumentOutOfRangeException(string.Format(_invalidCustomerType, customerType));
            }

            return (result);
        }

        private Enums.Fuel MapToFuel(CE.RateEngine.Enums.FuelType fuelType)
        {
            Enums.Fuel result;

            switch (fuelType)
            {
                case CE.RateEngine.Enums.FuelType.Electric:
                    result = Enums.Fuel.Electric;
                    break;

                case CE.RateEngine.Enums.FuelType.Gas:
                    result = Enums.Fuel.Gas;
                    break;

                case CE.RateEngine.Enums.FuelType.Oil:
                    result = Enums.Fuel.Oil;
                    break;

                case CE.RateEngine.Enums.FuelType.Propane:
                    result = Enums.Fuel.Propane;
                    break;

                case CE.RateEngine.Enums.FuelType.Water:
                    result = Enums.Fuel.Water;
                    break;

                case CE.RateEngine.Enums.FuelType.Steam:
                    result = Enums.Fuel.Steam;
                    break;

                default:
                    result = Enums.Fuel.Undefined;
                    break;

            }

            return (result);
        }

        private string GetUnitOfMeasureAsString(int unitOfMeasure)
        {
            string result;

            switch (unitOfMeasure)
            {
                case 0:
                    result = CE.RateEngine.Enums.ElectricUOM.KWh.ToString().ToLower();
                    break;

                case 1:
                    result = CE.RateEngine.Enums.ElectricUOM.KVA.ToString().ToLower();
                    break;

                case 2:
                    result = CE.RateEngine.Enums.GasUOM.Therm.ToString().ToLower();
                    break;

                case 3:
                    result = CE.RateEngine.Enums.GasUOM.DKTherm.ToString().ToLower();
                    break;

                case 4:
                    result = CE.RateEngine.Enums.GasUOM.CCF.ToString().ToLower();
                    break;

                case 5:
                    result = CE.RateEngine.Enums.GasUOM.MCF.ToString().ToLower();
                    break;

                case 6:
                    result = CE.RateEngine.Enums.GasUOM.M3.ToString().ToLower();
                    break;

                case 7:
                    result = CE.RateEngine.Enums.WaterUOM.CCF.ToString().ToLower();
                    break;

                case 8:
                    result = CE.RateEngine.Enums.WaterUOM.Gal.ToString().ToLower();
                    break;

                case 9:
                    result = CE.RateEngine.Enums.WaterUOM.CGal.ToString().ToLower();
                    break;

                case 10:
                    result = CE.RateEngine.Enums.WaterUOM.CF.ToString().ToLower();
                    break;

                case 11:
                    result = CE.RateEngine.Enums.WaterUOM.KGAL.ToString().ToLower();
                    break;

                case 12:
                    result = CE.RateEngine.Enums.WaterUOM.hgal.ToString().ToLower();
                    break;

                case 13:
                    result = CE.RateEngine.Enums.WaterUOM.DKTherms.ToString().ToLower();
                    break;

                case 15:
                    result = CE.RateEngine.Enums.ElectricUOM.kVAH.ToString().ToLower();
                    break;

                case 16:
                    result = CE.RateEngine.Enums.ElectricUOM.kVAR.ToString().ToLower();
                    break;

                case 17:
                    result = CE.RateEngine.Enums.ElectricUOM.kVARh.ToString().ToLower();
                    break;

                case 18:
                    result = CE.RateEngine.Enums.WaterUOM.hcf.ToString().ToLower();
                    break;

                case 19:
                    result = CE.RateEngine.Enums.WaterUOM.lb.ToString().ToLower();
                    break;

                case 20:
                    result = CE.RateEngine.Enums.WaterUOM.klb.ToString().ToLower();
                    break;

                case 21:
                    result = CE.RateEngine.Enums.WaterUOM.USGL.ToString().ToLower();
                    break;

                case 22:
                    result = CE.RateEngine.Enums.WaterUOM.CubicMeter.ToString().ToLower();
                    break;

                case 23:
                    result = CE.RateEngine.Enums.WaterUOM.CubicFoot.ToString().ToLower();
                    break;

                default:
                    result = "unknown";
                    break;

            }

            return (result);
        }


        #endregion


        #region GetBaseRateClass

        public string GetBaseRateClass(int rateCompanyID, string rateClass)
        {
            string baseRateClass = string.Empty;

            try
            {
                baseRateClass = _rateEngine.GetBaseRateClass(rateCompanyID, rateClass);
            }
            catch
            {
                throw;
            }

            return (baseRateClass);
        }

        #endregion


        #region CalculateCost

        /// <summary>
        /// Simple Calculate Cost.  Calculated cost from usages.
        /// </summary>
        /// <param name="rateCompanyID"></param>
        /// <param name="rateClass"></param>
        /// <param name="startDate"></param>
        /// <param name="endDate"></param>
        /// <param name="usages"></param>
        /// <returns></returns>
        public CostResult CalculateCost(int rateCompanyID, string rateClass, DateTime startDate, DateTime endDate, List<Usage> usages)
        {
            CostResult result;
            CE.RateEngine.CalculateCostExtParam extParamEP;
            CE.RateEngine.UsageCollection usagesEP;
            CE.RateEngine.CostResult resultEP;

            usagesEP = MapToUsagesEP(usages);

            extParamEP = new CalculateCostExtParam();

            resultEP = _rateEngine.CalculateCost(rateCompanyID, rateClass, startDate, endDate, usagesEP, extParamEP);

            result = MapFromCalculateCostResultEP(resultEP);

            return (result);
        }

        private CostResult MapFromCalculateCostResultEP(CE.RateEngine.CostResult resultEP)
        {
            CostResult result;

            result = new CostResult();

            result.TotalCost = resultEP.TotalCost;
            result.CalculationsHadChildren = resultEP.CalculationsHadChildren;
            result.MinimumCostsApplied = resultEP.MinimumCostsApplied;
            result.BillDays = resultEP.BillDays;

            switch (resultEP.ErrorCode)
            {
                case CE.RateEngine.Enums.ErrCode.NoError:
                    result.ErrCode = Enums.ErrorCode.NoError;
                    break;

                case CE.RateEngine.Enums.ErrCode.ErrAllReadingsNotProvidedForRTP:
                    result.ErrCode = Enums.ErrorCode.ErrAllReadingsNotProvidedForRTP;
                    break;

                case CE.RateEngine.Enums.ErrCode.ErrGeneric:
                    result.ErrCode = Enums.ErrorCode.ErrGeneric;
                    break;

                case CE.RateEngine.Enums.ErrCode.ErrUnsupportedCalculation:
                    result.ErrCode = Enums.ErrorCode.ErrUnsupportedCalculation;
                    break;

                case CE.RateEngine.Enums.ErrCode.ErrUnsupportedComplexTieredTOU:
                    result.ErrCode = Enums.ErrorCode.ErrUnsupportedComplexTieredTOU;
                    break;

                case CE.RateEngine.Enums.ErrCode.ErrTOUComplex:
                    result.ErrCode = Enums.ErrorCode.ErrTOUComplex;
                    break;

                case CE.RateEngine.Enums.ErrCode.ErrRateDefinitionsNotFound:
                    result.ErrCode = Enums.ErrorCode.ErrRateDefinitionsNotFound;
                    break;

                case CE.RateEngine.Enums.ErrCode.ErrUnsupportedProjectedCalculation:
                    result.ErrCode = Enums.ErrorCode.ErrUnsupportedProjectedCalculation;
                    break;

                case CE.RateEngine.Enums.ErrCode.ErrUnableToApplyRateClassModifier:
                    result.ErrCode = Enums.ErrorCode.ErrUnableToApplyRateClassModifier;
                    break;
                case CE.RateEngine.Enums.ErrCode.ErrRequiredReadingsAreMissing:
                    result.ErrCode = Enums.ErrorCode.ErrRequiredReadingsAreMissing;
                    break;
                default:
                    result.ErrCode = Enums.ErrorCode.NoError;
                    break;

            }

            result.Costs = MapFromCostsEP(resultEP.CostCollection);

            return (result);
        }

        #endregion


        #region GetSimpleRTPInfo and GetDetailedRTPInfo

        /// <summary>
        /// GetSimpleRTPInfo.
        /// Faster call to get basic properties.
        /// </summary>
        /// <param name="rateCompanyID"></param>
        /// <param name="rateClass"></param>
        /// <returns></returns>
        public RTPInfo GetSimpleRTPInfo(int rateCompanyID, string rateClass)
        {
            RTPInfo result = new RTPInfo();
            CE.RateEngine.RTPInfo resultEP;

            resultEP = _rateEngine.GetSimpleRTPInfo(rateCompanyID, rateClass);

            if (resultEP != null)
            {
                result.IsRTP = resultEP.IsRTP;
                result.IsCPP = resultEP.IsCPP;
                result.IsPTR = resultEP.IsPTR;
                result.IsRTPGroupIdSpecified = result.IsRTPGroupIdSpecified;
                result.RTPGroupId = resultEP.RTPGroupId;
            }

            return (result);
        }

        /// <summary>
        /// GetDetailedRTPInfo.
        /// Slower call that must expand entire detailed definition to get charges so that potential RTP group ID can be retreived.
        /// </summary>
        /// <param name="rateCompanyID"></param>
        /// <param name="rateClass"></param>
        /// <returns></returns>
        public RTPInfo GetDetailedRTPInfo(int rateCompanyID, string rateClass)
        {
            RTPInfo result = new RTPInfo();
            CE.RateEngine.RTPInfo resultEP;

            resultEP = _rateEngine.GetDetailedRTPInfo(rateCompanyID, rateClass);

            if (resultEP != null)
            {
                result.IsRTP = resultEP.IsRTP;
                result.IsCPP = resultEP.IsCPP;
                result.IsPTR = resultEP.IsPTR;
                result.IsRTPGroupIdSpecified = resultEP.RTPGroupIdSpecified;
                result.RTPGroupId = resultEP.RTPGroupId;
            }

            return (result);
        }

        #endregion


        #region GetPeakEvents

        /// <summary>
        /// Get peak events for the provided groud id from the rate engine.
        /// </summary>
        /// <param name="rtpGroupId"></param>
        /// <param name="startDate"></param>
        /// <param name="endDate"></param>
        /// <returns></returns>
        public List<PeakEvent> GetPeakEvents(int rtpGroupId, DateTime startDate, DateTime endDate)
        {
            List<PeakEvent> events = new List<PeakEvent>();
            CE.RateEngine.CPPEventCollection resultEP;

            resultEP = _rateEngine.GetAnyPeakEvents(rtpGroupId, startDate, endDate);

            if (resultEP != null && resultEP.Count > 0)
            {
                foreach (CPPEvent r in resultEP)
                {
                    var e = new PeakEvent()
                    {
                        StartDate = r.StartDate,
                        EndDate = r.EndDate
                    };

                    events.Add(e);
                }

            }

            return (events);
        }

        #endregion


        #region GetRTPSimulatedEvents

        /// <summary>
        /// Get RTP simulated events.  Just a month and priority list is returned.
        /// </summary>
        /// <param name="rateCompanyID"></param>
        /// <param name="numberOfEvents"></param>
        /// <returns></returns>
        public List<SimEvent> GetRTPSimulatedEvents(int rateCompanyID, int numberOfEvents)
        {
            List<SimEvent> events = new List<SimEvent>();
            CE.RateEngine.SimEventCollection resultEP;

            resultEP = _rateEngine.GetRTPSimulatedEvents(rateCompanyID, numberOfEvents);

            if (resultEP != null && resultEP.Count > 0)
            {
                foreach (CE.RateEngine.SimEvent r in resultEP)
                {
                    var e = new SimEvent()
                    {
                        Month = r.Month,
                        Priority = r.Priority
                    };

                    events.Add(e);
                }

            }

            return (events);
        }

        #endregion


        #region GetRTPSimulatedEventDuration

        /// <summary>
        /// Get RTP simulated events.  Just a month and priority list is returned.
        /// </summary>
        /// <param name="rateCompanyID"></param>
        /// <param name="numberOfEvents"></param>
        /// <returns></returns>
        public List<EventDuration> GetRTPSimulatedEventDuration(int rtpGroupId)
        {
            List<EventDuration> durations = new List<EventDuration>();
            CE.RateEngine.SimDurationCollection resultEP;
            Enums.Season season = Enums.Season.Undefined;

            resultEP = _rateEngine.GetRTPSimulatedEventDuration(rtpGroupId);

            if (resultEP != null && resultEP.Count > 0)
            {
                foreach (CE.RateEngine.SimDuration r in resultEP)
                {
                    // season mapping.
                    switch (r.Season)
                    {
                        case CE.RateEngine.Enums.Season.Undefined:
                            season = Enums.Season.Undefined;
                            break;

                        case CE.RateEngine.Enums.Season.Summer:
                            season = Enums.Season.Summer;
                            break;

                        case CE.RateEngine.Enums.Season.Winter:
                            season = Enums.Season.Winter;
                            break;

                        case CE.RateEngine.Enums.Season.SeasonA:
                            season = Enums.Season.SeasonA;
                            break;

                        case CE.RateEngine.Enums.Season.SeasonB:
                            season = Enums.Season.SeasonB;
                            break;

                        default:
                            season = Enums.Season.Undefined;
                            break;
                    }

                    // create duration.
                    var d = new EventDuration()
                    {
                        StartHour = r.StartHour,
                        EndHour = r.EndHour,
                        Season = season
                    };

                    // finally add duration.
                    durations.Add(d);
                }

            }

            return (durations);
        }

        #endregion


        #region GetNetMeteringTotalResult (2)

        /// <summary>
        /// Get a Net Metering Total Result from a list of monthly results.
        /// </summary>
        /// <param name="rateCompanyID"></param>
        /// <param name="monthlyResults"></param>
        /// <returns></returns>
        public NetMeteringTotalResult GetNetMeteringTotalResult(int rateCompanyID, List<NetMeteringMonthlyResult> monthlyResults)
        {
            NetMeteringTotalResult result = null;
            CE.RateEngine.NetMeteringTotalResult resultEP = null;

            if (monthlyResults.Count > 0)
            {
                List<CE.RateEngine.NetMeteringMonthlyResult> monthlyResultListEP = new List<CE.RateEngine.NetMeteringMonthlyResult>();

                foreach (NetMeteringMonthlyResult mr in monthlyResults)
                {
                    CE.RateEngine.NetMeteringMonthlyResult monthlyResultEP = new CE.RateEngine.NetMeteringMonthlyResult()
                    {
                        MinimumCost = mr.MinimumCost,
                        RegularCost = mr.RegularCost,
                        CarryoverCharge = mr.CarryoverCharge,
                        NetMeteringType = MapToNetMeteringTypeEP(mr.NetMeteringType)
                    };

                    monthlyResultListEP.Add(monthlyResultEP);
                }

                resultEP = _rateEngine.GetNetMeteringTotalResult(rateCompanyID, monthlyResultListEP);

                if (resultEP != null)
                {
                    result = new NetMeteringTotalResult()
                    {
                        TotalMinimumCost = resultEP.TotalMinimumCost,
                        TotalRegularCost = resultEP.TotalRegularCost,
                        TotalCarryoverCharge = resultEP.TotalCarryoverCharge,
                        TotalCost = resultEP.TotalCost
                    };

                }
            }

            return (result);
        }

        /// <summary>
        /// Get a Net Metering Total Result from the sum of monthly results.
        /// </summary>
        /// <param name="rateCompanyID"></param>
        /// <param name="sumMonthlyResult"></param>
        /// <returns></returns>
        public NetMeteringTotalResult GetNetMeteringTotalResult(int rateCompanyID, NetMeteringMonthlyResult sumMonthlyResult)
        {
            NetMeteringTotalResult result = null;
            CE.RateEngine.NetMeteringTotalResult resultEP = null;

            if (sumMonthlyResult != null)
            {
                CE.RateEngine.NetMeteringMonthlyResult monthlyResultEP = new CE.RateEngine.NetMeteringMonthlyResult()
                {
                    MinimumCost = sumMonthlyResult.MinimumCost,
                    RegularCost = sumMonthlyResult.RegularCost,
                    CarryoverCharge = sumMonthlyResult.CarryoverCharge,
                    NetMeteringType = MapToNetMeteringTypeEP(sumMonthlyResult.NetMeteringType)
                };

                resultEP = _rateEngine.GetNetMeteringTotalResult(rateCompanyID, monthlyResultEP);

                if (resultEP != null)
                {
                    result = new NetMeteringTotalResult()
                    {
                        TotalMinimumCost = resultEP.TotalMinimumCost,
                        TotalRegularCost = resultEP.TotalRegularCost,
                        TotalCarryoverCharge = resultEP.TotalCarryoverCharge,
                        TotalCost = resultEP.TotalCost
                    };

                }
            }

            return (result);
        }

        /// <summary>
        /// Net metering type mapping from RateModel to RateEngine.
        /// </summary>
        /// <param name="netMeteringType"></param>
        /// <returns></returns>
        private CE.RateEngine.Enums.NetMeteringType MapToNetMeteringTypeEP(Enums.NetMeteringType netMeteringType)
        {
            CE.RateEngine.Enums.NetMeteringType result;

            switch (netMeteringType)
            {
                case Enums.NetMeteringType.Nem3:
                    result = CE.RateEngine.Enums.NetMeteringType.Nem3;
                    break;

                case Enums.NetMeteringType.Nem4:
                    result = CE.RateEngine.Enums.NetMeteringType.Nem4;
                    break;

                case Enums.NetMeteringType.Nem5:
                    result = CE.RateEngine.Enums.NetMeteringType.Nem5;
                    break;

                case Enums.NetMeteringType.LikeEPR6:
                    result = CE.RateEngine.Enums.NetMeteringType.LikeEPR6;
                    break;

                default:
                    result = CE.RateEngine.Enums.NetMeteringType.Unspecified;
                    break;
            }

            return (result);
        }

        #endregion

        /// <summary>
        /// Get finalized tier boundaries based on rate definiton of Date, and filter it by date range (bill start and end)
        /// </summary>
        /// <param name="rateCompanyId"></param>
        /// <param name="rateClass"></param>
        /// <param name="date"></param>
        /// <param name="billStartDate"></param>
        /// <param name="billEndDate"></param>
        /// <returns></returns>
        public List<TierBoundary> FinalizedTierBoundariesByDateRange(int rateCompanyId, string rateClass, DateTime date, DateTime billStartDate, DateTime billEndDate, DateTime billProjectedEndDate, bool useProjectedEndDate)
        {
            List<TierBoundary> tbs = null;
            var start = date;
            var end = date.AddDays(1);
            var defs = _rateEngine.GetDetailedRateInformation(rateCompanyId, rateClass, start, end);
            
            var resultEP = _rateEngine.GetAllCalculatedTierBoundaryInformation(defs, rateClass, billStartDate, billEndDate, billProjectedEndDate, useProjectedEndDate);

            if (resultEP != null && resultEP.TierBoundariesExist && resultEP.TierBoundaryCollectionsList.Count > 0)
            {
                var tierBoundariesCollection = new TierBoundaryCollection();
                foreach(var list in resultEP.TierBoundaryCollectionsList)
                {
                    foreach(CE.RateEngine.TierBoundary tb in list)
                    {
                        tierBoundariesCollection.Add(tb);
                    }
                }
                tbs = MapFromTierBoundariesEP(tierBoundariesCollection);

                if(resultEP.SeasonFactors != null && resultEP.SeasonFactors.Count > 1)
                {
                    foreach (DictionaryEntry dictSeason in resultEP.SeasonFactors)
                    {
                        var sf = (SeasonFactor)dictSeason.Value;
                        tbs.FindAll(t => t.Season == (Enums.Season)sf.Season).ForEach(t => { t.SeasonFactor = sf.Factor; t.DaysIntoSeason = sf.Days; });
                    }
                }
            }

            return tbs;

        }

        /// <summary>
        /// Get finalized tier boundaries based on rate definiton of Date, and filter it by Date
        /// </summary>
        /// <param name="rateCompanyId"></param>
        /// <param name="rateClass"></param>
        /// <param name="date"></param>
        /// <returns></returns>
        public List<TierBoundary> FinalizedTierBoundariesByDate(int rateCompanyId, string rateClass, DateTime date)
        {
            List<TierBoundary> tbs = null;
            var start = date;
            var end = date.AddDays(1);
            var defs = _rateEngine.GetDetailedRateInformation(rateCompanyId, rateClass, start, end);

            if(defs != null && defs.Count > 0)
            {
                CE.RateEngine.TierBoundaryCollection tierBoundaries;
                var def = defs[0];
                if (def.IsSeasonal)
                {
                    var season = def.SeasonBoundaries.FindInclusiveBoundary(date);

                    tierBoundaries = def.TierBoundaries.GetFirstAvailableForSeason(season.Season);

                }
                else
                {
                    tierBoundaries = def.TierBoundaries;
                }

                if (tierBoundaries != null && tierBoundaries.Count > 0)
                {
                    tbs = MapFromTierBoundariesEP(tierBoundaries);
                }
            }

            return tbs;

        }

        /// <summary>
        /// Get tier boundaries.  Inclusive to first rate def in range, and season of the enddate.  Returns null if rate class is not tired, or rate class invalid.
        /// </summary>
        /// <param name="rateCompanyId"></param>
        /// <param name="rateClass"></param>
        /// <param name="startDate"></param>
        /// <param name="endDate"></param>
        /// <returns></returns>
        public List<TierBoundary> GetTierBoundaries(int rateCompanyId, string rateClass, DateTime startDate, DateTime endDate)
        {
            List<TierBoundary> tbs = null;
            CE.RateEngine.TierBoundaryCollection resultEP;

            resultEP = _rateEngine.GetCalculatedTierBoundaries(rateCompanyId, rateClass, startDate, endDate);

            if (resultEP != null && resultEP.Count > 0)
            {
                tbs = MapFromTierBoundariesEP(resultEP);                

            }

            return (tbs);
        }

        private List<TierBoundary> MapFromTierBoundariesEP(CE.RateEngine.TierBoundaryCollection resultEP)
        {
            List<TierBoundary> tbs = null;
            Enums.Season season = Enums.Season.Undefined;
            Enums.TimeOfUse tou = Enums.TimeOfUse.Undefined;
            Enums.BaseOrTier bot = Enums.BaseOrTier.Undefined;

            tbs = new List<TierBoundary>();

            foreach (CE.RateEngine.TierBoundary r in resultEP)
            {
                // season mapping.
                switch (r.Season)
                {
                    case CE.RateEngine.Enums.Season.Undefined:
                        season = Enums.Season.Undefined;
                        break;

                    case CE.RateEngine.Enums.Season.Summer:
                        season = Enums.Season.Summer;
                        break;

                    case CE.RateEngine.Enums.Season.Winter:
                        season = Enums.Season.Winter;
                        break;

                    case CE.RateEngine.Enums.Season.SeasonA:
                        season = Enums.Season.SeasonA;
                        break;

                    case CE.RateEngine.Enums.Season.SeasonB:
                        season = Enums.Season.SeasonB;
                        break;

                    default:
                        season = Enums.Season.Undefined;
                        break;
                }

                // tou mapping.
                switch (r.TimeOfUse)
                {
                    case CE.RateEngine.Enums.TimeOfUse.Undefined:
                        tou = Enums.TimeOfUse.Undefined;
                        break;

                    case CE.RateEngine.Enums.TimeOfUse.OffPeak:
                        tou = Enums.TimeOfUse.OffPeak;
                        break;

                    case CE.RateEngine.Enums.TimeOfUse.OnPeak:
                        tou = Enums.TimeOfUse.OnPeak;
                        break;

                    case CE.RateEngine.Enums.TimeOfUse.CriticalPeak:
                        tou = Enums.TimeOfUse.CriticalPeak;
                        break;

                    case CE.RateEngine.Enums.TimeOfUse.Shoulder1:
                        tou = Enums.TimeOfUse.Shoulder1;
                        break;

                    case CE.RateEngine.Enums.TimeOfUse.Shoulder2:
                        tou = Enums.TimeOfUse.Shoulder2;
                        break;

                    default:
                        tou = Enums.TimeOfUse.Undefined;
                        break;
                }

                // tou mapping.
                switch (r.BaseOrTier)
                {
                    case CE.RateEngine.Enums.BaseOrTier.Undefined:
                        bot = Enums.BaseOrTier.Undefined;
                        break;

                    case CE.RateEngine.Enums.BaseOrTier.TotalServiceUse:
                        bot = Enums.BaseOrTier.TotalServiceUse;
                        break;

                    case CE.RateEngine.Enums.BaseOrTier.Tier1:
                        bot = Enums.BaseOrTier.Tier1;
                        break;

                    case CE.RateEngine.Enums.BaseOrTier.Tier2:
                        bot = Enums.BaseOrTier.Tier2;
                        break;

                    case CE.RateEngine.Enums.BaseOrTier.Tier3:
                        bot = Enums.BaseOrTier.Tier3;
                        break;

                    case CE.RateEngine.Enums.BaseOrTier.Tier4:
                        bot = Enums.BaseOrTier.Tier4;
                        break;

                    case CE.RateEngine.Enums.BaseOrTier.Tier5:
                        bot = Enums.BaseOrTier.Tier5;
                        break;

                    case CE.RateEngine.Enums.BaseOrTier.Tier6:
                        bot = Enums.BaseOrTier.Tier6;
                        break;

                    default:
                        bot = Enums.BaseOrTier.Undefined;
                        break;
                }

                // create tier boundary.
                var tb = new TierBoundary()
                {
                    Season = season,
                    TimeOfUse = tou,
                    BaseOrTier = bot,
                    Threshold = r.Threshold,
                    SecondaryThreshold = r.SecondaryThreshold   //rarely used, for vey special type of boundary; pass value along if present
                };

                // finally add tier boundary.
                tbs.Add(tb);
            }


            return (tbs);
        }

        public List<string> GetLogHistory(Guid guidSession)
        {
            var result = new List<string>();
            var logEntries = _rateEngine.GetLogHistory(guidSession);
            foreach(LogEntry logEntry in logEntries)
            {
                result.Add(logEntry.Text);
            }

            return result;
        }

    }
}
