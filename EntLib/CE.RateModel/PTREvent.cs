﻿using System;

namespace CE.RateModel
{
    public class PTREvent
    {
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public Enums.BaselineRule BaselineRule { get; set; }
        public PTREventRebate Rebate { get; set; }
    }

}
