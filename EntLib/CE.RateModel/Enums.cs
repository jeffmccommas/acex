﻿
namespace CE.RateModel
{
    public class Enums
    {

        public enum ErrorCode
        {
            NoError = 0,
            ErrGeneric,
            ErrTOUComplex,
            ErrAllReadingsNotProvidedForRTP,
            ErrUnsupportedComplexTieredTOU,
            ErrUnsupportedCalculation,
            ErrRateDefinitionsNotFound,
            ErrUnsupportedProjectedCalculation,
            ErrMinimumBillDaysNotMet,               // check made in BillToDate API
            ErrMinimumUsageNotMet,                  // check made in BillToDate API
            ErrMaximumMissingDaysMet,               // check made in BillToDate API
            ErrUnsupportedMultipleMetersInService,   // check made in BillToDate API
            ErrUnableToApplyRateClassModifier,
            ErrRequiredReadingsAreMissing,
            ErrTodayExceedsNextReadDate,
            ErrStartDateGreaterThanNextReadDate
        }

        public enum BaseOrTier
        {
            Undefined = 0,
            Tier1 = 1,
            Tier2 = 2,
            Tier3 = 3,
            Tier4 = 4,
            Tier5 = 5,
            Tier6 = 6,
            TotalServiceUse = 10,
            Demand = 11,
            DemandBill = 12,
            DemandBill2 = 13,
            //--- CR50560 BEGIN -------------------------------------------------------------------------------------------
            DemandBill3 = 14
            //--- CR50560 END ---------------------------------------------------------------------------------------------
        }

        public enum TimeOfUse
        {
            Undefined = 0,
            OnPeak = 1,
            OffPeak = 2,
            Shoulder1 = 3,
            Shoulder2 = 4,
            CriticalPeak = 5
        }

        public enum Season
        {
            Undefined = 0,
            Summer = 1,
            Winter = 2,
            SeasonA = 3,
            SeasonB = 4
        }

        public enum MinimumChargeType
        {
            NoMinimumCharge = 0,
            ProrateMinimumCharge = 1,
            NoProrateMinimumCharge = 2
        }


        public enum Fuel
        {
            Undefined = 0,
            Electric = 1,
            Gas = 2,
            Oil = 3,
            Propane = 4,
            Water = 9,
            Steam = 10
        }

        public enum CustomerType
        {
            Commercial = 0,
            Residential = 1,
            Farm = 2
        }

        public enum RebateAccountingRule
        {
            Unspecified = 0,
            EachEvent,
            EntireBillPeriod
        }

        public enum BaselineRule
        {
            Unspecified = 0,
            ThreeOfTen = 1,
            FiveOfThirty = 2
        }

        /// <summary>
        /// Indicates how daily tier boundaries and percentBaseTier1 boundaries are determined.
        /// </summary>
        public enum DailyTierBillDaysType
        {
            UseProjectedBillDays = 0,
            UseCurrentBillDays = 1
        }

        public enum SpecialIntervalType
        {
            IntervalRegular = 0,
            Interval2Hour,
            Interval4Hour,
            Interval6Hour,
            Interval12Hour
        }

        public enum NetMeteringType
        {
            Unspecified = 0,
            LikeEPR6 = 1,
            Nem3 = 2,
            Nem4 = 3,
            Nem5 = 4,
        }

        public enum PartType
        {
            Primary = 0,
            Secondary = 1,
            Tertiary = 2,
            Quaternary = 3,
            Quinary = 4,
            Senary = 5,
            Septenary = 6,
            Octonary = 7
        }

        public enum ChargeCalcType
        {
            None = 0,
            AvgDailyKWh = 1,
            Daily = 2,
            Monthly = 3,
            PerMeterPerDay = 4,
            PerDwellingUnitPerDay = 5,
            SteppedDailyDemandBill = 6,
            SteppedMonthlyDemandBill = 7,
            SteppedDiffStepDemandBill = 8,
            SteppedDiffStepDemand = 9,
            PTRRebateEachEvent = 10,      // to support PeakTimeRebate, and maps to enum RebateAccountingRule
            PTRRebateEntireBillPeriod = 11,  // to support PeakTimeRebate, and maps to enum RebateAccountingRule
            AnnExcGen = 12,
            AnnExcGenP = 13,
            AnnExcGenOP = 14,
            AnnExcGenS1 = 15,
            AnnExcGenS2 = 16,
            AnnExcGenCP = 17,
            MonthlySeasonAdjustedAVGU = 18,
            CRCDemandCharge = 19,
            CRCExcessKWh = 20,
            NCDemandCharge = 21,
            NCDemandMultFactor = 22,
            CPDemandCharge = 23
        }

        public enum ServiceStep
        {
            Undefined = 0,
            Step1 = 1,
            Step2 = 2,
            Step3 = 3,
            Step4 = 4,
            Step5 = 5,
            Step6 = 6,
            Step7 = 7,
            Step8 = 8
        }

        public enum RebateClass
        {
            Undefined = 0,
            Class1 = 1,
            Class2 = 2,
            Class3 = 3,
            Class4 = 4,
            Class5 = 5
        }

        public enum DayType
        {
            Weekday = 0,
            WeekendHoliday = 1,
            Weekend = 2,
            Saturday = 3,
            Sunday = 4,
            Holiday = 5,
            All = 6,
            Monday = 7,
            Tuesday = 8,
            Wednesday = 9,
            Thursday = 10,
            Friday = 11
        }
    }

}
