﻿
namespace CE.RateModel
{
    /// <summary>
    /// Simple class for Net Metering Total Result.
    /// </summary>
    public class NetMeteringTotalResult
    {
        public double TotalRegularCost { get; set; }
        public double TotalMinimumCost { get; set; }
        public double TotalCarryoverCharge { get; set; }
        public double TotalCost { get; set; }

        public NetMeteringTotalResult()
        {
        }

    }
}



