﻿
namespace CE.RateModel
{
    public class RateClass
    {
        private string _rateClassID;
        private string _name;
        private string _description;
        private Enums.Fuel _fuel;
        private Enums.CustomerType _customerType;
        public bool ContainsPTR { get; set; }
        public bool IsTiered { get; set; }
        public bool IsTimeOfUse { get; set; }
        public bool IsSeasonal { get; set; }
        public string CommodityAsString { get; set; }
        public string UnitOfMeasureAsString { get; set; }

        public RateClass()
        {
        }

        #region Properties

        public string RateClassID
        {
            get { return _rateClassID; }
            set { _rateClassID = value; }
        }

        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }

        public string Description
        {
            get { return _description; }
            set { _description = value; }
        }

        public Enums.CustomerType CustomerType
        {
            get { return _customerType; }
            set { _customerType = value; }
        }

        public Enums.Fuel Fuel
        {
            get { return _fuel; }
            set { _fuel = value; }
        }

        #endregion

    }

}
