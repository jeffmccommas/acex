﻿
namespace CE.RateModel
{
    public class MinimumCharge
    {
        public Enums.ChargeCalcType CalculationType { get; set; }
        public double ChargeValue { get; set; }
        public string CostType { get; set; }
    }
}
