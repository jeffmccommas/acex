﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using CE.RateModel;
using System.Xml;

namespace CE.RateModel.UnitTests
{
    [TestClass]
    public class RateModelExampleTests
    {
        const bool default_enableLogging = false;
        //const string default_databaseConn = "Data Source=SW-DEVUFXSQL-01\\UFX;Initial Catalog=RateEngine;Integrated Security=SSPI;";
        //const string default_databaseConn = @"Data Source=SWEPSQLDEV1\EP1;Initial Catalog=RateEngine;Integrated Security=SSPI;";
        const string default_databaseConn = @"Server=tcp:pocacesql1.database.windows.net,1433;Database=aceRE;User ID=AclaraCEAdmin@pocacesql1;Password=Acl@r@282;Encrypt=True;TrustServerCertificate=False;Connection Timeout=30;Min Pool Size=5;";

        public RateModelExampleTests()
        {

        }

        [TestMethod]
        public void RateModelFactoryTest()
        {
            GetRateModel();
        }

        //<summary>
        // Benchmark Meter-to-date with Hourly Readings; regular binning only
        //</summary>
        [TestMethod()]
        public void Example_Benchmark_CostToDate_A_Hourly()
        {
            int i;
            DateTime startTime;
            DateTime stopTime;
            TimeSpan span;
            int max = 1000;

            startTime = DateTime.Now;

            for (i = 0; i < max; i++)
            {
                DoCostToDateHourly("EM60M00000001.xml");
            }

            stopTime = DateTime.Now;
            span = stopTime - startTime;

            // always assert okay for now
            Assert.IsTrue(false, "(actually passed) iterations = " + max + ", totalSeconds = " + span.TotalSeconds);
        }


        bool DoCostToDateHourly(string filename)
        {
            IRateModel RateModel;
            int rateCompanyID;
            List<Reading> readings;
            string rateClass = String.Empty;
            DateTime dtStartDate;
            DateTime dtEndDate;
            DateTime dtProjectedEndDate;
            CostToDateResult costToDateResult;
            Enums.MinimumChargeType minimumChargeType = Enums.MinimumChargeType.NoMinimumCharge;
            bool prorateMonthlyServiceCharges = false;
            CostToDateExtParam extParam;

            // Get readings and initialize parameters
            readings = GetAMI(filename);
            rateCompanyID = 101;    //generic referrer (84)
            rateClass = "P1";
            dtStartDate = new DateTime(2010, 1, 1);
            dtEndDate = new DateTime(2010, 1, 25);
            dtProjectedEndDate = new DateTime(2010, 1, 31);
            extParam = new CostToDateExtParam(prorateMonthlyServiceCharges, minimumChargeType);

            RateModel = GetRateModel();
            costToDateResult = RateModel.CostToDate(rateCompanyID, readings, rateClass, dtStartDate, dtEndDate, dtProjectedEndDate, extParam);

            return (true);
        }


        [TestMethod()]
        public void Example_CostToDate_A_Hourly_RateClassIsTOU()
        {
            IRateModel RateModel;
            int rateCompanyID;
            List<Reading> readings;
            string rateClass = String.Empty;
            DateTime dtStartDate;
            DateTime dtEndDate;
            DateTime dtProjectedEndDate;
            CostToDateResult costToDateResult;
            Enums.MinimumChargeType minimumChargeType = Enums.MinimumChargeType.NoMinimumCharge;
            bool prorateMonthlyServiceCharges = false;
            CostToDateExtParam extParam;

            // Full range of readings
            readings = GetAMI("EM60M00000001.xml");
            rateCompanyID = 101;    //generic referrer (84)
            rateClass = "P1";
            dtStartDate = new DateTime(2010, 1, 1);
            dtEndDate = new DateTime(2010, 1, 25);
            dtProjectedEndDate = new DateTime(2010, 1, 31);
            extParam = new CostToDateExtParam(prorateMonthlyServiceCharges, minimumChargeType);

            RateModel = GetRateModel();
            costToDateResult = RateModel.CostToDate(rateCompanyID, readings, rateClass, dtStartDate, dtEndDate, dtProjectedEndDate, extParam);

            // always assert okay for now
            //Assert.IsTrue(true);
        }

        [TestMethod()]
        public void Example_CostToDate_B_Hourly_RateClassIsTOU()
        {
            IRateModel RateModel;
            int rateCompanyID;
            List<Reading> readings;
            string rateClass = String.Empty;
            DateTime dtStartDate;
            DateTime dtEndDate;
            DateTime dtProjectedEndDate;
            CostToDateResult costToDateResult;
            List<Usage> usages;
            Enums.MinimumChargeType minimumChargeType = Enums.MinimumChargeType.NoMinimumCharge;
            bool prorateMonthlyServiceCharges = false;
            CostToDateExtParam extParam;

            // This is ONE DAY of readings;  simulate previous usage and one day of readings for this call
            readings = GetAMI("EM60M00000002.xml");
            rateCompanyID = 101;    //generic referrer (84)
            rateClass = "P1";
            dtStartDate = new DateTime(2010, 1, 1);
            dtEndDate = new DateTime(2010, 1, 25);
            dtProjectedEndDate = new DateTime(2010, 1, 31);
            extParam = new CostToDateExtParam(prorateMonthlyServiceCharges, minimumChargeType);

            usages = new List<Usage>();
            usages.Add(new Usage(Enums.TimeOfUse.OnPeak, 100.0));
            usages.Add(new Usage(Enums.TimeOfUse.OffPeak, 400.0));

            RateModel = GetRateModel();
            costToDateResult = RateModel.CostToDate(rateCompanyID, readings, usages, rateClass, dtStartDate, dtEndDate, dtProjectedEndDate, extParam);

            // always assert okay for now
            // Assert.IsTrue(true);
        }

        [TestMethod()]
        public void Example_CostToDate_C_RateClassIsTOU()
        {
            IRateModel RateModel;
            int rateCompanyID;
            string rateClass = String.Empty;
            DateTime dtStartDate;
            DateTime dtEndDate;
            DateTime dtProjectedEndDate;
            CostToDateResult costToDateResult;
            List<Usage> usages;
            Enums.MinimumChargeType minimumChargeType = Enums.MinimumChargeType.NoMinimumCharge;
            bool prorateMonthlyServiceCharges = false;
            CostToDateExtParam extParam;

            // This takes just usages as input, no readings
            rateCompanyID = 101;    //generic referrer (84)
            rateClass = "P1";
            dtStartDate = new DateTime(2010, 1, 1);
            dtEndDate = new DateTime(2010, 1, 25);
            dtProjectedEndDate = new DateTime(2010, 1, 31);
            extParam = new CostToDateExtParam(prorateMonthlyServiceCharges, minimumChargeType);

            usages = new List<Usage>();
            usages.Add(new Usage(Enums.TimeOfUse.OnPeak, 100.0));
            usages.Add(new Usage(Enums.TimeOfUse.OffPeak, 400.0));

            RateModel = GetRateModel();
            costToDateResult = RateModel.CostToDate(rateCompanyID, usages, rateClass, dtStartDate, dtEndDate, dtProjectedEndDate, extParam);

            // always assert okay for now
            //Assert.IsTrue(true);
        }

        [TestMethod()]
        public void Example_CostToDate_A_Daily_RateClassIsTOU()
        {
            IRateModel RateModel;
            int rateCompanyID;
            List<Reading> readings;
            string rateClass = String.Empty;
            DateTime dtStartDate;
            DateTime dtEndDate;
            DateTime dtProjectedEndDate;
            CostToDateResult costToDateResult;
            Enums.MinimumChargeType minimumChargeType = Enums.MinimumChargeType.NoMinimumCharge;
            bool prorateMonthlyServiceCharges = false;
            CostToDateExtParam extParam;

            // Daily TOU readings
            readings = GetAMI("E00000002.xml");
            rateCompanyID = 101;    //generic referrer (84)
            rateClass = "P1";
            dtStartDate = new DateTime(2010, 1, 1);
            dtEndDate = new DateTime(2010, 1, 25);
            dtProjectedEndDate = new DateTime(2010, 1, 31);
            extParam = new CostToDateExtParam(prorateMonthlyServiceCharges, minimumChargeType);

            RateModel = GetRateModel();
            costToDateResult = RateModel.CostToDate(rateCompanyID, readings, rateClass, dtStartDate, dtEndDate, dtProjectedEndDate, extParam);

            // always assert okay for now
            //Assert.IsTrue(true);
        }

        [TestMethod()]
        public void Example_CostToDate_A_Daily_RateClassIsTIERED()
        {
            IRateModel RateModel;
            int rateCompanyID;
            List<Reading> readings;
            string rateClass = String.Empty;
            DateTime dtStartDate;
            DateTime dtEndDate;
            DateTime dtProjectedEndDate;
            CostToDateResult costToDateResult;
            Enums.MinimumChargeType minimumChargeType = Enums.MinimumChargeType.NoMinimumCharge;
            bool prorateMonthlyServiceCharges = false;
            CostToDateExtParam extParam;

            // Daily readings
            readings = GetAMI("E00000001.xml");
            rateCompanyID = 101;    //generic referrer (84)
            rateClass = "T1";
            dtStartDate = new DateTime(2010, 1, 1);
            dtEndDate = new DateTime(2010, 1, 25);
            dtProjectedEndDate = new DateTime(2010, 1, 31);
            extParam = new CostToDateExtParam(prorateMonthlyServiceCharges, minimumChargeType);

            RateModel = GetRateModel();
            costToDateResult = RateModel.CostToDate(rateCompanyID, readings, rateClass, dtStartDate, dtEndDate, dtProjectedEndDate, extParam);

            // always assert okay for now
            // Assert.IsTrue(true);
        }

        [TestMethod()]
        public void Example_CostToDate_B_Daily_RateClassIsTIERED()
        {
            IRateModel RateModel;
            int rateCompanyID;
            List<Reading> readings;
            string rateClass = String.Empty;
            DateTime dtStartDate;
            DateTime dtEndDate;
            DateTime dtProjectedEndDate;
            CostToDateResult costToDateResult;
            List<Usage> usages;
            Enums.MinimumChargeType minimumChargeType = Enums.MinimumChargeType.NoMinimumCharge;
            bool prorateMonthlyServiceCharges = false;
            CostToDateExtParam extParam;

            // Daily readings for one day, and previous readings totals
            readings = GetAMI("E00000003.xml");
            rateCompanyID = 101;    //generic referrer (84)
            rateClass = "T1";
            dtStartDate = new DateTime(2010, 1, 1);
            dtEndDate = new DateTime(2010, 1, 25);
            dtProjectedEndDate = new DateTime(2010, 1, 31);
            extParam = new CostToDateExtParam(prorateMonthlyServiceCharges, minimumChargeType);

            usages = new List<Usage>();
            usages.Add(new Usage(500.0));

            RateModel = GetRateModel();
            costToDateResult = RateModel.CostToDate(rateCompanyID, readings, usages, rateClass, dtStartDate, dtEndDate, dtProjectedEndDate, extParam);

            // always assert okay for now
            // Assert.IsTrue(true);
        }

        [TestMethod()]
        public void Example_CostToDate_B_Daily_RateClassIsTOU()
        {
            IRateModel RateModel;
            int rateCompanyID;
            List<Reading> readings;
            string rateClass = String.Empty;
            DateTime dtStartDate;
            DateTime dtEndDate;
            DateTime dtProjectedEndDate;
            CostToDateResult costToDateResult;
            List<Usage> usages;
            Enums.MinimumChargeType minimumChargeType = Enums.MinimumChargeType.NoMinimumCharge;
            bool prorateMonthlyServiceCharges = false;
            CostToDateExtParam extParam;

            // Daily tou readings for one day, and previous readings totals
            readings = GetAMI("E00000004.xml");
            rateCompanyID = 101;    //generic referrer (84)
            rateClass = "P1";
            dtStartDate = new DateTime(2010, 1, 1);
            dtEndDate = new DateTime(2010, 1, 25);
            dtProjectedEndDate = new DateTime(2010, 1, 31);
            extParam = new CostToDateExtParam(prorateMonthlyServiceCharges, minimumChargeType);

            usages = new List<Usage>();
            usages.Add(new Usage(500.0));       //provide total optionally; internally this is created if not present
            usages.Add(new Usage(125.0));
            usages.Add(new Usage(375.0));

            RateModel = GetRateModel();
            costToDateResult = RateModel.CostToDate(rateCompanyID, readings, usages, rateClass, dtStartDate, dtEndDate, dtProjectedEndDate, extParam);

            // always assert okay for now
            // Assert.IsTrue(true);
        }


        [TestMethod()]
        public void Example_CostToDate_A_Daily_X1()
        {
            IRateModel RateModel;
            int rateCompanyID;
            List<Reading> readings;
            string rateClass = String.Empty;
            DateTime dtStartDate;
            DateTime dtEndDate;
            DateTime dtProjectedEndDate;
            CostToDateResult costToDateResult;
            Enums.MinimumChargeType minimumChargeType = Enums.MinimumChargeType.NoMinimumCharge;
            bool prorateMonthlyServiceCharges = false;
            CostToDateExtParam extParam;

            // Daily readings
            readings = GetAMI("E000X1.xml");
            rateCompanyID = 100;    //referrer 87, readings from 5/30 - 6/1
            rateClass = "RG-1";
            dtStartDate = new DateTime(2010, 5, 17);
            dtEndDate = new DateTime(2010, 5, 20);
            dtProjectedEndDate = new DateTime(2010, 5, 21);
            extParam = new CostToDateExtParam(prorateMonthlyServiceCharges, minimumChargeType);

            RateModel = GetRateModel();
            costToDateResult = RateModel.CostToDate(rateCompanyID, readings, rateClass, dtStartDate, dtEndDate, dtProjectedEndDate, extParam);

            // always assert okay for now
            // Assert.IsTrue(true);
        }

        [TestMethod()]
        public void Example_CostToDate_F_Hourly()
        {
            IRateModel RateModel;
            int rateCompanyID;
            List<Reading> readings;
            string rateClass = String.Empty;
            DateTime dtStartDate;
            DateTime dtEndDate;
            DateTime dtProjectedEndDate;
            CostToDateResult costToDateResult;
            Enums.MinimumChargeType minimumChargeType = Enums.MinimumChargeType.ProrateMinimumCharge;
            bool prorateMonthlyServiceCharges = false;
            CostToDateExtParam extParam;

            // Full range of readings
            readings = GetAMI("EM60M00000001.xml");
            rateCompanyID = 100;    //demo referrer
            rateClass = "MinTest";
            dtStartDate = new DateTime(2010, 1, 1);
            dtEndDate = new DateTime(2010, 1, 25);
            dtProjectedEndDate = new DateTime(2010, 1, 31);
            extParam = new CostToDateExtParam(prorateMonthlyServiceCharges, minimumChargeType);

            RateModel = GetRateModel2();
            costToDateResult = RateModel.CostToDate(rateCompanyID, readings, rateClass, dtStartDate, dtEndDate, dtProjectedEndDate, extParam);

            // always assert okay for now
            //Assert.IsTrue(true);
        }

        [TestMethod()]
        public void Example_CostToDate_G_Hourly()
        {
            IRateModel RateModel;
            int rateCompanyID;
            List<Reading> readings;
            string rateClass = String.Empty;
            DateTime dtStartDate;
            DateTime dtEndDate;
            DateTime dtProjectedEndDate;
            CostToDateResult costToDateResult;
            Enums.MinimumChargeType minimumChargeType = Enums.MinimumChargeType.ProrateMinimumCharge;
            bool prorateMonthlyServiceCharges = false;
            CostToDateExtParam extParam;

            // Full range of readings
            readings = GetAMI("EM60M00000003.xml");
            rateCompanyID = 356;    //QACORE referrer
            rateClass = "ER-TEST-S";
            dtStartDate = new DateTime(2010, 7, 1);
            dtEndDate = new DateTime(2010, 7, 21);
            dtProjectedEndDate = new DateTime(2010, 7, 31);
            extParam = new CostToDateExtParam(prorateMonthlyServiceCharges, minimumChargeType);

            RateModel = GetRateModel();
            costToDateResult = RateModel.CostToDate(rateCompanyID, readings, rateClass, dtStartDate, dtEndDate, dtProjectedEndDate, extParam);

            // always assert okay for now
            //Assert.IsTrue(true);
        }

        [TestMethod()]
        public void Example_GetRateClassList()
        {
            IRateModel RateModel;
            int rateCompanyID;
            RateClassList rateClassList = null;
            RateClass rateClass = null;
            bool retval = false;

            rateCompanyID = 356;    //QACORE referrer

            RateModel = GetRateModel();
            rateClassList = RateModel.GetRateClassList(rateCompanyID);

            if (rateClassList != null)
            {
                rateClass = rateClassList.FindByRateClassID("E-RES-TEST2XX");
                
                if (rateClass != null)
                {
                   
                }
            }

            retval = rateClassList.IsRateClassOfCustomerType("E-RES-TEST2", Enums.CustomerType.Commercial);        // this is residential
            retval = rateClassList.IsRateClassOfCustomerType("E-RES-TEST2XX", Enums.CustomerType.Commercial);      // this is a non-existant rate class
            retval = rateClassList.IsRateClassOfCustomerType("E-CHURCH-1-P", Enums.CustomerType.Commercial);       // this is commercial

            // always assert okay for now
            Assert.IsInstanceOfType(rateClassList, typeof(RateClassList), "failed to return a rateClassList");

            rateClass = rateClassList.FindByRateClassID("E-RES-TEST2");

            Assert.IsTrue(rateClass.CustomerType == Enums.CustomerType.Residential, "rate class not residential");
        }

        List<Reading> GetAMI(string filename)
        {
            XmlDocument amrReadingXmlDocument = new XmlDocument();
            XmlNodeList amrXmlNodeList;
            List<Reading> readings;
            Reading r;
            Enums.BaseOrTier eBaseOrTier;
            Enums.TimeOfUse eTimeOfUse;
            Enums.Season eSeason;
            DateTime timestamp;
            Double quantity;
            Boolean isEmpty = false;

            readings = new List<Reading>();

            amrReadingXmlDocument.Load("C:\\aclsc\\ACEx\\15.10\\EntLib\\CE.RateModel\\UnitTests\\TestData\\" + filename);
            amrXmlNodeList = amrReadingXmlDocument.SelectNodes("//Reading");

            // The baseOrTier is determined when setting the TimeOfUse below;
            eBaseOrTier = Enums.BaseOrTier.Undefined;

            foreach (XmlNode node in amrXmlNodeList)
            {

                // map the TimeOfUse 
                switch (node.Attributes["TOUBin"].InnerText)
                {
                    case "NoTOU":
                        eTimeOfUse = Enums.TimeOfUse.Undefined;
                        eBaseOrTier = Enums.BaseOrTier.TotalServiceUse;
                        break;
                    case "OnPeak":
                        eTimeOfUse = Enums.TimeOfUse.OnPeak;
                        break;
                    case "OffPeak":
                        eTimeOfUse = Enums.TimeOfUse.OffPeak;
                        break;
                    case "Shoulder1":
                        eTimeOfUse = Enums.TimeOfUse.Shoulder1;
                        break;
                    case "Shoulder2":
                        eTimeOfUse = Enums.TimeOfUse.Shoulder2;
                        break;
                    case "CriticalPeak":
                        eTimeOfUse = Enums.TimeOfUse.CriticalPeak;
                        break;
                    default:
                        eTimeOfUse = Enums.TimeOfUse.Undefined;
                        eBaseOrTier = Enums.BaseOrTier.TotalServiceUse;
                        break;
                }

                eSeason = Enums.Season.Undefined;
                timestamp = System.Convert.ToDateTime(node.Attributes["IntEnd"].InnerText);
                quantity = System.Convert.ToDouble(node.Attributes["Read"].InnerText);

                r = new Reading(eBaseOrTier, eTimeOfUse, eSeason, timestamp, quantity, isEmpty);
                readings.Add(r);

            }

            return (readings);
        }

        public IRateModel GetRateModel()
        {
            IRateModel RateModel;
            RateModelEPFactory RateModelEPFactory = new RateModelEPFactory();
            CE.RateModel.RateModelConfiguration config;

            // use the real configuration item in order to test the real classes from RateModel here
            config = new CE.RateModel.RateModelConfiguration();
            config.EnableLogging = default_enableLogging;
            config.DatabaseConn = default_databaseConn;

            RateModel = RateModelEPFactory.CreateRateModel(config);

            return (RateModel);
        }

        public IRateModel GetRateModel2()
        {
            IRateModel RateModel;
            RateModelEPFactory RateModelEPFactory = new RateModelEPFactory();

            RateModel = RateModelEPFactory.CreateRateModel();

            return (RateModel);
        }


        #region Older for Comparison Direct Rate Engine

        // DIRECT OLDER for comparison

        //[TestMethod()]
        //public void OldExampleCostToDateWithHourlyReadingsDirect()
        //{
        //    int i;
        //    DateTime startTime;
        //    DateTime stopTime;
        //    TimeSpan span;
        //    int max = 1000;

        //    startTime = DateTime.Now;

        //    for (i = 0; i < max; i++)
        //    {
        //        DoCostToDateHourlyDirect("EM60M00000001.xml");
        //    }

        //    stopTime = DateTime.Now;
        //    span = stopTime - startTime;

        //    // always assert okay for now
        //    Assert.IsTrue(false, "(actually passed) iterations = " + max + ", totalSeconds = " + span.TotalSeconds);
        //}

        //bool DoCostToDateHourlyDirect(string filename)
        //{
        //    int nRateCompanyID;
        //    ReadingCollection readings;
        //    string rateClass = String.Empty;
        //    DateTime dtStartDate;
        //    DateTime dtEndDate;
        //    DateTime dtProjectedEndDate;
        //    CE.RateEngine.CostToDateResult result;
        //    CE.RateEngine.RateEngine RE;

        //    //Get readings and initialize parameters
        //    readings = GetAMRReadingCollection(filename);
        //    nRateCompanyID = 101;  //generic referrer (84)
        //    rateClass = "P1";  //T1 for totals, try P1 even with Daily TOU readings.
        //    dtStartDate = new DateTime(2010, 1, 1);
        //    dtEndDate = new DateTime(2010, 1, 25);
        //    dtProjectedEndDate = new DateTime(2010, 1, 31);

        //    RE = GetRateEngine();
        //    result = RE.CostToDate(nRateCompanyID, readings, rateClass, dtStartDate, dtEndDate, dtProjectedEndDate, CE.RateEngine.Enums.MinimumChargeType.NoProrateMinimumCharge);

        //    return (true);
        //}

        // For older comparison
        //CE.RateEngine.ReadingCollection GetAMRReadingCollection(string filename)
        //{
        //    XmlDocument amrReadingXmlDocument = new XmlDocument();
        //    XmlNodeList amrXmlNodeList;

        //    CE.RateEngine.ReadingCollection result;
        //    CE.RateEngine.Reading objREReading;

        //    CE.RateEngine.Enums.BaseOrTier eBaseOrTier;
        //    CE.RateEngine.Enums.TimeOfUse eTimeOfUse;
        //    CE.RateEngine.Enums.Season eSeason;
        //    DateTime timestamp;
        //    Double quantity;
        //    CE.RateEngine.Enums.DayType eDayType = CE.RateEngine.Enums.DayType.WeekendHoliday;
        //    Boolean eIsEmpty = false;

        //    result = new ReadingCollection();

        //    amrReadingXmlDocument.Load("C:\\ACLTFS\\UFx\\Alerts\\UFxSandbox\\Aclara.UFx.Alerts.RateModel\\UnitTest\\TestData\\" + filename);
        //    amrXmlNodeList = amrReadingXmlDocument.SelectNodes("//Reading");

        //    // The baseOrTier is determined when setting the TimeOfUse below;
        //    eBaseOrTier = CE.RateEngine.Enums.BaseOrTier.Undefined;

        //    foreach (XmlNode node in amrXmlNodeList)
        //    {

        //        switch (node.Attributes["TOUBin"].InnerText)
        //        {
        //            case "NoTOU":
        //                eTimeOfUse = CE.RateEngine.Enums.TimeOfUse.Undefined;
        //                eBaseOrTier = CE.RateEngine.Enums.BaseOrTier.TotalServiceUse;
        //                break;
        //            case "OnPeak":
        //                eTimeOfUse = CE.RateEngine.Enums.TimeOfUse.OnPeak;
        //                break;
        //            case "OffPeak":
        //                eTimeOfUse = CE.RateEngine.Enums.TimeOfUse.OffPeak;
        //                break;
        //            case "Shoulder1":
        //                eTimeOfUse = CE.RateEngine.Enums.TimeOfUse.Shoulder1;
        //                break;
        //            case "Shoulder2":
        //                eTimeOfUse = CE.RateEngine.Enums.TimeOfUse.Shoulder2;
        //                break;
        //            case "CriticalPeak":
        //                eTimeOfUse = CE.RateEngine.Enums.TimeOfUse.CriticalPeak;
        //                break;
        //            default:
        //                eTimeOfUse = CE.RateEngine.Enums.TimeOfUse.Undefined;
        //                eBaseOrTier = CE.RateEngine.Enums.BaseOrTier.TotalServiceUse;
        //                break;
        //        }


        //        eSeason = CE.RateEngine.Enums.Season.Undefined;
        //        timestamp = System.Convert.ToDateTime(node.Attributes["IntEnd"].InnerText);
        //        quantity = System.Convert.ToDouble(node.Attributes["Read"].InnerText);

        //        // Assign the DayType
        //        switch (timestamp.DayOfWeek)
        //        {
        //            case System.DayOfWeek.Monday:
        //            case System.DayOfWeek.Tuesday:
        //            case System.DayOfWeek.Wednesday:
        //            case System.DayOfWeek.Thursday:
        //            case System.DayOfWeek.Friday:
        //                eDayType = CE.RateEngine.Enums.DayType.Weekday;
        //                break;
        //            case System.DayOfWeek.Saturday:
        //            case System.DayOfWeek.Sunday:
        //                eDayType = CE.RateEngine.Enums.DayType.WeekendHoliday;
        //                break;
        //            default:
        //                break;
        //        }

        //        objREReading = new CE.RateEngine.Reading(eBaseOrTier, eTimeOfUse, eSeason, timestamp, quantity, eDayType, eIsEmpty);
        //        result.Add(objREReading);

        //    }

        //    return (result);
        //}

        //// classic
        //public CE.RateEngine.RateEngine GetRateEngine()
        //{
        //    CE.RateEngine.RateEngine RE;
        //    RE = new RateEngine(default_databaseConn, default_enableLogging);
        //    return (RE);
        //}

        #endregion

    }
}
