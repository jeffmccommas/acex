﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using CE.RateModel;
using System.Collections.Generic;
using System.Linq;

namespace CE.RateModel.UnitTests
{
    [TestClass]
    public class ExtensionTests
    {
        /// <summary>
        /// Sample to test RateModel Extension method.
        /// </summary>
        [TestMethod]
        public void TestExtensionMethod_1()
        {
            var totalQuantity = 0.0;
            List<Usage> usages = new List<Usage>();

            usages.Add(new Usage(Enums.BaseOrTier.TotalServiceUse, Enums.TimeOfUse.Undefined, Enums.Season.Undefined, 100));

            totalQuantity = usages.GetTotalUsageQuantity();

        }

        [TestMethod]
        public void TestExtensionMethod_1Demand()
        {
            var totalQuantity = 0.0;
            List<Usage> usages = new List<Usage>();

            usages.Add(new Usage(Enums.BaseOrTier.TotalServiceUse, Enums.TimeOfUse.Undefined, Enums.Season.Undefined, 100));
            usages.Add(new Usage(Enums.BaseOrTier.DemandBill, Enums.TimeOfUse.Undefined, Enums.Season.Undefined, 27));

            totalQuantity = usages.GetTotalUsageQuantity();

        }

        [TestMethod]
        public void TestExtensionMethod_2()
        {
            var totalQuantity = 0.0;
            List<Usage> usages = new List<Usage>();

            usages.Add(new Usage(Enums.BaseOrTier.Undefined, Enums.TimeOfUse.OnPeak, Enums.Season.Undefined, 100));
            usages.Add(new Usage(Enums.BaseOrTier.Undefined, Enums.TimeOfUse.OffPeak, Enums.Season.Undefined, 50));

            totalQuantity = usages.GetTotalUsageQuantity();

        }

        [TestMethod]
        public void TestExtensionMethod_3()
        {
            var totalQuantity = 0.0;
            List<Usage> usages = new List<Usage>();

            usages.Add(new Usage(Enums.BaseOrTier.TotalServiceUse, Enums.TimeOfUse.Undefined, Enums.Season.Undefined, 150));
            usages.Add(new Usage(Enums.BaseOrTier.Undefined, Enums.TimeOfUse.OnPeak, Enums.Season.Undefined, 100));
            usages.Add(new Usage(Enums.BaseOrTier.Undefined, Enums.TimeOfUse.OffPeak, Enums.Season.Undefined, 50));

            totalQuantity = usages.GetTotalUsageQuantity();

        }

        [TestMethod]
        public void TestExtensionMethod_4()
        {
            var totalQuantity = 0.0;
            List<Usage> usages = new List<Usage>();

            usages.Add(new Usage(Enums.BaseOrTier.Undefined, Enums.TimeOfUse.OnPeak, Enums.Season.Undefined, 100));
            usages.Add(new Usage(Enums.BaseOrTier.Undefined, Enums.TimeOfUse.OffPeak, Enums.Season.Undefined, 50));

            usages.Add(new Usage(Enums.BaseOrTier.Undefined, Enums.TimeOfUse.OnPeak, Enums.Season.Summer, 50));
            usages.Add(new Usage(Enums.BaseOrTier.Undefined, Enums.TimeOfUse.OffPeak, Enums.Season.Summer, 50));
            usages.Add(new Usage(Enums.BaseOrTier.Undefined, Enums.TimeOfUse.OnPeak, Enums.Season.Winter, 25));
            usages.Add(new Usage(Enums.BaseOrTier.Undefined, Enums.TimeOfUse.OffPeak, Enums.Season.Winter, 25));

            totalQuantity = usages.GetTotalUsageQuantity();

        }

        [TestMethod]
        public void TestExtensionMethod_5()
        {
            var totalQuantity = 0.0;
            List<Usage> usages = new List<Usage>();

            usages.Add(new Usage(Enums.BaseOrTier.Undefined, Enums.TimeOfUse.OnPeak, Enums.Season.Undefined, 100));
            usages.Add(new Usage(Enums.BaseOrTier.Undefined, Enums.TimeOfUse.OffPeak, Enums.Season.Undefined, 50));

            usages.Add(new Usage(Enums.BaseOrTier.Undefined, Enums.TimeOfUse.OnPeak, Enums.Season.Summer, 50));
            usages.Add(new Usage(Enums.BaseOrTier.Undefined, Enums.TimeOfUse.OffPeak, Enums.Season.Summer, 50));
            usages.Add(new Usage(Enums.BaseOrTier.Undefined, Enums.TimeOfUse.OnPeak, Enums.Season.Winter, 25));
            usages.Add(new Usage(Enums.BaseOrTier.Undefined, Enums.TimeOfUse.OffPeak, Enums.Season.Winter, 25));

            usages.Add(new Usage(Enums.BaseOrTier.Tier1, Enums.TimeOfUse.OnPeak, Enums.Season.Summer, 50));
            usages.Add(new Usage(Enums.BaseOrTier.Tier1, Enums.TimeOfUse.OffPeak, Enums.Season.Summer, 50));
            usages.Add(new Usage(Enums.BaseOrTier.Tier1, Enums.TimeOfUse.OnPeak, Enums.Season.Winter, 25));
            usages.Add(new Usage(Enums.BaseOrTier.Tier1, Enums.TimeOfUse.OffPeak, Enums.Season.Winter, 25));

            totalQuantity = usages.GetTotalUsageQuantity();

        }

        [TestMethod]
        public void TestExtensionMethod_6()
        {
            var totalQuantity = 0.0;
            List<Usage> usages = new List<Usage>();

            usages.Add(new Usage(Enums.BaseOrTier.Tier1, Enums.TimeOfUse.OnPeak, Enums.Season.Summer, 50));
            usages.Add(new Usage(Enums.BaseOrTier.Tier1, Enums.TimeOfUse.OffPeak, Enums.Season.Summer, 50));
            usages.Add(new Usage(Enums.BaseOrTier.Tier1, Enums.TimeOfUse.OnPeak, Enums.Season.Winter, 25));
            usages.Add(new Usage(Enums.BaseOrTier.Tier1, Enums.TimeOfUse.OffPeak, Enums.Season.Winter, 25));

            usages.Add(new Usage(Enums.BaseOrTier.Tier2, Enums.TimeOfUse.OnPeak, Enums.Season.Summer, 5));
            usages.Add(new Usage(Enums.BaseOrTier.Tier2, Enums.TimeOfUse.OffPeak, Enums.Season.Summer, 5));
            usages.Add(new Usage(Enums.BaseOrTier.Tier2, Enums.TimeOfUse.OnPeak, Enums.Season.Winter, 5));
            usages.Add(new Usage(Enums.BaseOrTier.Tier2, Enums.TimeOfUse.OffPeak, Enums.Season.Winter, 5));

            totalQuantity = usages.GetTotalUsageQuantity();

        }

    }
}
