﻿using System;
using System.Collections.Generic;


namespace CE.RateModel
{
    public interface IRateModel
    {
        string LogSessionID();

        bool IsLoggingEnabled();


        CostToDateResult CostToDate(int rateCompanyID, List<Reading> readings, string rateClass, DateTime startDate, DateTime endDate, DateTime projectedEndDate, CostToDateExtParam extParam);

        CostToDateResult CostToDate(int rateCompanyID, List<Reading> readings, List<Baseline> baselines, string rateClass, DateTime startDate, DateTime endDate, DateTime projectedEndDate, CostToDateExtParam extParam);

        CostToDateResult CostToDate(int rateCompanyID, List<Reading> readings, List<Baseline> baselines, string rateClass, DateTime startDate, DateTime endDate, DateTime projectedEndDate, CostToDateExtParam extParam, bool resetStartDate);

        CostToDateResult CostToDate(int rateCompanyID, List<Reading> readings, List<Usage> currentUsages, string rateClass, DateTime startDate, DateTime endDate, DateTime projectedEndDate, CostToDateExtParam extParam);

        CostToDateResult CostToDate(int rateCompanyID, List<Usage> usages, string rateClass, DateTime startDate, DateTime endDate, DateTime projectedEndDate, CostToDateExtParam extParam);

        RateClassList GetRateClassList(int rateCompanyID);

        string GetBaseRateClass(int rateCompanyID, string rateClass);

        CostResult CalculateCost(int rateCompanyID, string rateClass, DateTime startDate, DateTime endDate, List<Usage> usages);


        RTPInfo GetSimpleRTPInfo(int rateCompanyID, string rateClass);

        RTPInfo GetDetailedRTPInfo(int rateCompanyID, string rateClass);

        List<PeakEvent> GetPeakEvents(int rtpGroupId, DateTime startDate, DateTime endDate);

        List<SimEvent> GetRTPSimulatedEvents(int rateCompanyID, int numberOfEvents);

        List<EventDuration> GetRTPSimulatedEventDuration(int rtpGroupId);


        NetMeteringTotalResult GetNetMeteringTotalResult(int rateCompanyID, List<NetMeteringMonthlyResult> monthlyResults);

        NetMeteringTotalResult GetNetMeteringTotalResult(int rateCompanyID, NetMeteringMonthlyResult sumMonthlyResult);
        List<TierBoundary> FinalizedTierBoundariesByDate(int rateCompanyId, string rateClass, DateTime date);
        List<TierBoundary> FinalizedTierBoundariesByDateRange(int rateCompanyId, string rateClass, DateTime date, DateTime billStartDate, DateTime billEndDate, DateTime billProjectedEndDate, bool useProjectedEndDate);

        List<TierBoundary> GetTierBoundaries(int rateCompanyId, string rateClass, DateTime startDate, DateTime endDate);

        RateClass GetRateClassInformation(int rateCompanyID, string rateClass);

        RateClass GetRateClassInformation(int rateCompanyID, string rateClass, DateTime startDate);

        DetailedRateClass GetDetailsRateClassInformation(int rateCompanyID, string rateClass, DateTime startDate);

        List<string> GetLogHistory(Guid guidSession);

    }
}
