﻿
namespace CE.RateModel
{
    public class RTPInfo
    {
        public bool IsRTP { get; set; }
        public bool IsCPP { get; set; }
        public bool IsPTR { get; set; }
        public bool IsRTPGroupIdSpecified { get; set; }
        public int RTPGroupId { get; set; }

        public RTPInfo()
        {
            IsRTP = false;
            IsCPP = false;
            IsPTR = false;
            IsRTPGroupIdSpecified = false;
            RTPGroupId = -1;
        }

        public RTPInfo(bool isrtp, bool iscpp, bool isptr)
        {
            IsRTP = isrtp;
            IsCPP = iscpp;
            IsPTR = isptr;
            RTPGroupId = -1;
            IsRTPGroupIdSpecified = false;
        }

        public RTPInfo(bool isrtp, bool iscpp, bool isptr, int rtpGroupId)
        {
            IsRTP = isrtp;
            IsCPP = iscpp;
            IsPTR = isptr;
            RTPGroupId = rtpGroupId;
            IsRTPGroupIdSpecified = true;
        }

    }
}
