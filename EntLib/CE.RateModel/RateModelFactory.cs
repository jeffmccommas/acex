﻿
namespace CE.RateModel
{
    public abstract class RateModelFactory
    {
        public abstract IRateModel CreateRateModel();

        public abstract IRateModel CreateRateModel(bool enableLogOverride);

        public abstract IRateModel CreateRateModel(RateModelConfiguration config);
    }
}
