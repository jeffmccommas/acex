﻿using System;
using System.Collections.Generic;

namespace CE.RateModel
{
    public class CostToDateResult
    {
        private double _cost;
        private double _projectedCost;
        private DateTime _startDate;
        private DateTime _endDate;
        private DateTime _projectedEndDate;
        private int _numberOfDaysInCostToDate;
        private int _numberOfDaysInProjectedCost;
        private double _monthProrateFactorUsed;
        private double _usageScaleFactorUsed;
        private bool _minimumCostsApplied;
        private double _averageDailyCost;
        private List<Usage> _usages;
        private List<Usage> _projectedUsages;
        private DateTime? _lastReadingDate;
        private Enums.ErrorCode _errCode;
        private Enums.ErrorCode _projectedCostErrCode;
        private PTRResult _ptrResult;
        private PTRResult _ptrProjectedResult;
        private List<Cost> _costs;
        private List<Cost> _projectedCosts;
        private NetMeteringMonthlyResult _netMeteringResult;
        private NetMeteringMonthlyResult _projectedNetMeteringResult;

        public CostToDateResult()
        {
            _errCode = Enums.ErrorCode.NoError;
        }


        #region Properties

        public Enums.ErrorCode ErrCode                  // for cost calculation portion of result
        {
            get { return _errCode; }
            set { _errCode = value; }
        }

        public Enums.ErrorCode ProjectedCostErrCode     // for projected cost calculation portion of result
        {
            get { return _projectedCostErrCode; }
            set { _projectedCostErrCode = value; }
        }

        public List<Usage> Usages
        {
            get { return _usages; }
            set { _usages = value; }
        }

        public List<Usage> ProjectedUsages
        {
            get { return _projectedUsages; }
            set { _projectedUsages = value; }
        }

        public double UsageScaleFactorUsed
        {
            get { return _usageScaleFactorUsed; }
            set { _usageScaleFactorUsed = value; }
        }

        public double MonthProrateFactorUsed
        {
            get { return _monthProrateFactorUsed; }
            set { _monthProrateFactorUsed = value; }
        }

        public int NumberOfDaysInCostToDate
        {
            get { return _numberOfDaysInCostToDate; }
            set { _numberOfDaysInCostToDate = value; }
        }

        public int NumberOfDaysInProjectedCost
        {
            get { return _numberOfDaysInProjectedCost; }
            set { _numberOfDaysInProjectedCost = value; }
        }

        public double Cost
        {
            get { return _cost; }
            set { _cost = value; }
        }

        public double AverageDailyCost
        {
            get { return _averageDailyCost; }
            set { _averageDailyCost = value; }
        }

        public double ProjectedCost
        {
            get { return _projectedCost; }
            set { _projectedCost = value; }
        }

        public DateTime StartDate
        {
            get { return _startDate; }
            set { _startDate = value; }
        }

        public DateTime EndDate
        {
            get { return _endDate; }
            set { _endDate = value; }
        }

        public DateTime ProjectedEndDate
        {
            get { return _projectedEndDate; }
            set { _projectedEndDate = value; }
        }

        public bool MinimumCostsApplied
        {
            get { return _minimumCostsApplied; }
            set { _minimumCostsApplied = value; }
        }

        public DateTime? LastReadingDate
        {
            get { return _lastReadingDate; }
            set { _lastReadingDate = value; }
        }

        public PTRResult PeakTimeRebates
        {
            get { return _ptrResult; }
            set { _ptrResult = value; }
        }

        public PTRResult ProjectedPeakTimeRebates
        {
            get { return _ptrProjectedResult; }
            set { _ptrProjectedResult = value; }
        }

        public List<Cost> Costs
        {
            get { return _costs; }
            set { _costs = value; }
        }

        public List<Cost> ProjectedCosts
        {
            get { return _projectedCosts; }
            set { _projectedCosts = value; }
        }

        public NetMeteringMonthlyResult NetMeteringResult
        {
            get { return _netMeteringResult; }
            set { _netMeteringResult = value; }
        }

        public NetMeteringMonthlyResult ProjectedNetMeteringResult
        {
            get { return _projectedNetMeteringResult; }
            set { _projectedNetMeteringResult = value; }
        }

        /// <summary>
        /// indicate if daily demand is excluded in the calculated result
        /// </summary>
        public bool DailyDemandCalcExcluded { get; set; }
        #endregion

    }
}
