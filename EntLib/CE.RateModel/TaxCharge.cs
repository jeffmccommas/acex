﻿
namespace CE.RateModel
{
    public class TaxCharge
    {
        public double ChargeValue { get; set; }
        public string CostType { get; set; }
    }
}
