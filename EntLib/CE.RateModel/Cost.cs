﻿
namespace CE.RateModel
{
    /// <summary>
    /// Cost class.
    /// </summary>
    public class Cost
    {
        private string _name;
        private double _amount;

        private Cost()
        {
        }

        public Cost(string name, double amount)
        {
            _name = name;
            _amount = amount;
        }


        #region Properties

        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }

        public double Amount
        {
            get { return _amount; }
            set { _amount = value; }
        }

        #endregion

    }
}
