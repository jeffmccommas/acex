﻿
namespace CE.RateModel
{
    public class PTREventRebate
    {
        public double ActualUsage { get; set; }
        public bool ActualUsageProvided { get; set; }
        public bool ActualUsageCalculated { get; set; }
        public bool MissingActualUsage { get; set; }
        public double BaselineUsage { get; set; }
        public bool BaselineUsageEmpty { get; set; }
        public double SavingsUsage { get; set; }
        public double RealSavingsUsage { get; set; }
        public double UnitPrice { get; set; }
        public double RebateAmount { get; set; }
        public bool RebateAmountProvided { get; set; }
        public bool RebateAmountCalculated { get; set; }
        public bool Valid { get; set; }
        public Enums.RebateAccountingRule RebateAccountingRule { get; set; }
        public bool BaselineCalculated { get; set; }
    }
}
