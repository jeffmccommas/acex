﻿
namespace CE.RateModel
{
    public class CostToDateExtParam
    {
        private bool _prorateMonthlyServiceCharges;
        private Enums.MinimumChargeType _minimumChargeType;
        private bool _prorateDemandUsageDeterminants;
        private bool _smoothTiers;
        private bool _checkDaysInFullMonth;
        private int _daysInFullMonth;
        private int _projectedNumDays;
        private const Enums.MinimumChargeType DefaultMinimumChargeType = Enums.MinimumChargeType.NoProrateMinimumCharge;
        private const int DefaultDaysInFullMonth = 32;
        private const int DefaultProjectedNumDays = 32;
        private const bool DefaultAllowRebateCalculations = false;
        private const bool DefaultAllowBaselineCalculations = false;
        private const double DefaultFactorsRAF = 0.2;                               //advanced 
        private string _dstEndDate;
        public bool AllowRebateCalculations { get; set; }                           //only applies to PTR
        public bool AllowBaselineCalculations { get; set; }                         //only applies to PTR
        public bool UseProjectedEndDateWhenFinalizingTierBoundaries { get; set; }   //advanced tier handling option
        public bool OmitProjectedCostCalculation { get; set; }                      //advanced
        public bool AdjustReadings { get; set; }                                    //advanced
        public double GeneralConservationRAF { get; set; }                          //advanced
        public double TypicalResponseRAF { get; set; }                              //advanced
        public double CriticalResponseRAF { get; set; }                             //advanced
        public double DemandControlRAF { get; set; }                                //advanced
        public double PercentReductionAsShiftedRAF { get; set; }                    //advanced
        public double PercentCriticalReductionAsShiftedRAF { get; set; }            //advanced
        public bool InlinePeakEvents { get; set; }                                  //advanced
        public PeakEventInfo PeakEventInformation { get; set; }                     //advanced

        public CostToDateExtParam()
        {
            _prorateMonthlyServiceCharges = false;
            _minimumChargeType = DefaultMinimumChargeType;
            _prorateDemandUsageDeterminants = false;
            _smoothTiers = false;
            _checkDaysInFullMonth = false;
            _daysInFullMonth = DefaultDaysInFullMonth;
            _projectedNumDays = DefaultProjectedNumDays;
            _dstEndDate = string.Empty;
            AllowRebateCalculations = DefaultAllowRebateCalculations;
            AllowBaselineCalculations = DefaultAllowBaselineCalculations;
            UseProjectedEndDateWhenFinalizingTierBoundaries = false;
            OmitProjectedCostCalculation = false;
            InlinePeakEvents = false;
            AdjustReadings = false;
            GeneralConservationRAF = DefaultFactorsRAF;
            TypicalResponseRAF = DefaultFactorsRAF;
            CriticalResponseRAF = DefaultFactorsRAF;
            DemandControlRAF = DefaultFactorsRAF;
            PercentReductionAsShiftedRAF = 1.0;
            PercentCriticalReductionAsShiftedRAF = 1.0;
            PeakEventInformation = null;
        }

        public CostToDateExtParam(bool prorateMonthlyServiceCharges,
                          Enums.MinimumChargeType minimumChargeType,
                          bool prorateDemandUsageDeterminants,
                          bool smoothTiers,
                          bool checkDaysInFullMonth,
                          int daysInFullMonth,
                          int projectedNumDays,
                          string dstEndDate,
                          bool allowRebateCalculations,
                          bool allowBaselineCalculations)
        {
            _prorateMonthlyServiceCharges = prorateMonthlyServiceCharges;
            _minimumChargeType = minimumChargeType;
            _prorateDemandUsageDeterminants = prorateDemandUsageDeterminants;
            _smoothTiers = smoothTiers;
            _checkDaysInFullMonth = checkDaysInFullMonth;
            _daysInFullMonth = daysInFullMonth;
            _projectedNumDays = projectedNumDays;
            _dstEndDate = dstEndDate;
            AllowRebateCalculations = allowRebateCalculations;
            AllowBaselineCalculations = allowBaselineCalculations;
            UseProjectedEndDateWhenFinalizingTierBoundaries = false;
            OmitProjectedCostCalculation = false;
            InlinePeakEvents = false;
            AdjustReadings = false;
            GeneralConservationRAF = DefaultFactorsRAF;
            TypicalResponseRAF = DefaultFactorsRAF;
            CriticalResponseRAF = DefaultFactorsRAF;
            DemandControlRAF = DefaultFactorsRAF;
            PercentReductionAsShiftedRAF = 1.0;
            PercentCriticalReductionAsShiftedRAF = 1.0;
            PeakEventInformation = null;
        }

        public CostToDateExtParam(bool prorateMonthlyServiceCharges,
                                  Enums.MinimumChargeType minimumChargeType,
                                  bool prorateDemandUsageDeterminants,
                                  bool smoothTiers,
                                  bool checkDaysInFullMonth,
                                  int daysInFullMonth,
                                  int projectedNumDays,
                                  string dstEndDate)
        {
            _prorateMonthlyServiceCharges = prorateMonthlyServiceCharges;
            _minimumChargeType = minimumChargeType;
            _prorateDemandUsageDeterminants = prorateDemandUsageDeterminants;
            _smoothTiers = smoothTiers;
            _checkDaysInFullMonth = checkDaysInFullMonth;
            _daysInFullMonth = daysInFullMonth;
            _projectedNumDays = projectedNumDays;
            _dstEndDate = dstEndDate;
            AllowRebateCalculations = DefaultAllowRebateCalculations;
            AllowBaselineCalculations = DefaultAllowBaselineCalculations;
            UseProjectedEndDateWhenFinalizingTierBoundaries = false;
            OmitProjectedCostCalculation = false;
            InlinePeakEvents = false;
            AdjustReadings = false;
            GeneralConservationRAF = DefaultFactorsRAF;
            TypicalResponseRAF = DefaultFactorsRAF;
            CriticalResponseRAF = DefaultFactorsRAF;
            DemandControlRAF = DefaultFactorsRAF;
            PercentReductionAsShiftedRAF = 1.0;
            PercentCriticalReductionAsShiftedRAF = 1.0;
            PeakEventInformation = null;
        }

        public CostToDateExtParam(bool prorateMonthlyServiceCharges,
                                  Enums.MinimumChargeType minimumChargeType)
        {
            _prorateMonthlyServiceCharges = prorateMonthlyServiceCharges;
            _minimumChargeType = minimumChargeType;
            _prorateDemandUsageDeterminants = false;
            _smoothTiers = false;
            _checkDaysInFullMonth = false;
            _daysInFullMonth = DefaultDaysInFullMonth;
            _projectedNumDays = DefaultProjectedNumDays;
            _dstEndDate = string.Empty;
            AllowRebateCalculations = DefaultAllowRebateCalculations;
            AllowBaselineCalculations = DefaultAllowBaselineCalculations;
            UseProjectedEndDateWhenFinalizingTierBoundaries = false;
            OmitProjectedCostCalculation = false;
            InlinePeakEvents = false;
            AdjustReadings = false;
            GeneralConservationRAF = DefaultFactorsRAF;
            TypicalResponseRAF = DefaultFactorsRAF;
            CriticalResponseRAF = DefaultFactorsRAF;
            DemandControlRAF = DefaultFactorsRAF;
            PercentReductionAsShiftedRAF = 1.0;
            PercentCriticalReductionAsShiftedRAF = 1.0;
            PeakEventInformation = null;
        }

        #region Properties

        public bool ProrateMonthlyServiceCharges
        {
            get { return (_prorateMonthlyServiceCharges); }
        }

        public bool ProrateDemandUsageDeterminants
        {
            get { return (_prorateDemandUsageDeterminants); }
        }

        public bool SmoothTiers
        {
            get { return (_smoothTiers); }
        }

        public bool CheckDaysInFullMonth
        {
            get { return (_checkDaysInFullMonth); }
        }

        public Enums.MinimumChargeType MinimumChargeType
        {
            get { return (_minimumChargeType); }
        }

        public int DaysInFullMonth
        {
            get { return (_daysInFullMonth); }
        }

        public int ProjectedNumDays
        {
            get { return (_projectedNumDays); }
        }

        public string DSTEndDate
        {
            get { return _dstEndDate; }
        }
        /// <summary>
        /// indicate if cost to date include daily demand usage calc
        ///should be true, except calls from alerts
        /// CR 51610 Oct 2014
        /// </summary>
        public bool IncludeDailyDemandCalc { get; set; }

        #endregion


        /// <summary>
        /// Create a deep copy.
        /// </summary>
        /// <returns></returns>
        public CostToDateExtParam Copy()
        {
            var t = new CostToDateExtParam();

            t._prorateMonthlyServiceCharges = this.ProrateMonthlyServiceCharges;
            t._minimumChargeType = this.MinimumChargeType;
            t._prorateDemandUsageDeterminants = this.ProrateDemandUsageDeterminants;
            t._smoothTiers = this.SmoothTiers;
            t._checkDaysInFullMonth = this.CheckDaysInFullMonth;
            t._daysInFullMonth = this.DaysInFullMonth;
            t._projectedNumDays = this.ProjectedNumDays;
            t._dstEndDate = this.DSTEndDate;
            t.AllowRebateCalculations = this.AllowRebateCalculations;
            t.AllowBaselineCalculations = this.AllowBaselineCalculations;
            t.UseProjectedEndDateWhenFinalizingTierBoundaries = this.UseProjectedEndDateWhenFinalizingTierBoundaries;
            t.OmitProjectedCostCalculation = this.OmitProjectedCostCalculation;
            t.InlinePeakEvents = this.InlinePeakEvents;
            t.AdjustReadings = this.AdjustReadings;
            t.GeneralConservationRAF = this.GeneralConservationRAF;
            t.TypicalResponseRAF = this.TypicalResponseRAF;
            t.CriticalResponseRAF = this.CriticalResponseRAF;
            t.DemandControlRAF = this.DemandControlRAF;
            t.PercentReductionAsShiftedRAF = this.PercentReductionAsShiftedRAF;
            t.PercentCriticalReductionAsShiftedRAF = this.PercentCriticalReductionAsShiftedRAF;
            if (PeakEventInformation != null)  t.PeakEventInformation = PeakEventInfo.DeepClone(this.PeakEventInformation);

            return(t);
        }


    }
}
