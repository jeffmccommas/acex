﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

namespace CE.RateModel
{
    /// <summary>
    /// This is any peak event.  a.k.a.  Critical Peak Event.
    /// </summary>
    [Serializable]
    public class PeakEvent
    {
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public List<EventDuration> Durations { get; set; }  /// optionally used.
    }


    /// <summary>
    /// Simulated event.
    /// </summary>
    [Serializable]
    public class SimEvent
    {
        public int Month { get; set; }
        public int Priority { get; set; }
    }


    /// <summary>
    /// Event duration by season.  Usually used for simulation.
    /// </summary>
    [Serializable]
    public class EventDuration
    {
        public Enums.Season Season { get; set; }
        public int StartHour { get; set; }
        public int EndHour { get; set; }
    }


    /// <summary>
    /// Peak Event Information container class for specal use.  May have additional properties added as needed.
    /// </summary>
    [Serializable]
    public class PeakEventInfo
    {
        public List<PeakEvent> PeakEvents { get; set; }

        /// <summary>
        /// For cloning.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="obj"></param>
        /// <returns></returns>
        public static T DeepClone<T>(T obj)
        {
            using (var ms = new MemoryStream())
            {
                var formatter = new BinaryFormatter();
                formatter.Serialize(ms, obj);
                ms.Position = 0;

                return (T)formatter.Deserialize(ms);
            }
        }

    }


}
