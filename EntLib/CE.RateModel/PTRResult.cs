﻿using System.Collections.Generic;

namespace CE.RateModel
{
    public class PTRResult
    {
        public Enums.RebateAccountingRule OverallAccountingRule { get; set; }
        public double RebateAmount { get; set; }
        public List<PTREvent> PeakTimeRebateEvents { get; set; }
    }
}
