﻿using System;

namespace CE.RateModel
{
    public class Reading
    {
        private DateTime _timestamp;
        private double _quantity;
        private Enums.BaseOrTier _baseOrTier;
        private Enums.TimeOfUse _timeOfUse;
        private Enums.Season _season;
        private bool _empty;
        private bool _gap;
        private Enums.SpecialIntervalType _specialInterval;

        private Reading()
        {
        }
        public Reading(Enums.BaseOrTier baseOrTier, Enums.TimeOfUse timeOfUse, Enums.Season season, DateTime timestamp, double quantity, bool isEmpty, bool isGap, Enums.SpecialIntervalType specialInterval)
        {
            _baseOrTier = baseOrTier;
            _timeOfUse = timeOfUse;
            _season = season;
            _timestamp = timestamp;
            _quantity = quantity;
            _empty = isEmpty;
            _gap = isGap;
            _specialInterval = specialInterval;
        }

        public Reading( Enums.BaseOrTier baseOrTier, Enums.TimeOfUse timeOfUse, Enums.Season season, DateTime timestamp, double quantity, bool isEmpty )
        {
            _baseOrTier = baseOrTier;
            _timeOfUse = timeOfUse;
            _season = season;
            _timestamp = timestamp;
            _quantity = quantity;
            _empty = isEmpty;
            _gap = false;
            _specialInterval = Enums.SpecialIntervalType.IntervalRegular;
        }

        public Reading(Enums.TimeOfUse timeOfUse, DateTime timestamp, double quantity)
        {
            _baseOrTier = Enums.BaseOrTier.Undefined;
            _timeOfUse = timeOfUse;
            _season = Enums.Season.Undefined;
            _timestamp = timestamp;
            _quantity = quantity;
            _empty = false;
            _gap = false;
            _specialInterval = Enums.SpecialIntervalType.IntervalRegular;
        }

        public Reading(DateTime timestamp, double quantity)
        {
            _baseOrTier = Enums.BaseOrTier.TotalServiceUse;
            _timeOfUse = Enums.TimeOfUse.Undefined;
            _season = Enums.Season.Undefined;
            _timestamp = timestamp;
            _quantity = quantity;
            _empty = false;
            _gap = false;
            _specialInterval = Enums.SpecialIntervalType.IntervalRegular;
        }

        public Reading(DateTime timestamp, double quantity, bool isEmpty)
        {
            _baseOrTier = Enums.BaseOrTier.TotalServiceUse;
            _timeOfUse = Enums.TimeOfUse.Undefined;
            _season = Enums.Season.Undefined;
            _timestamp = timestamp;
            _quantity = quantity;
            _empty = isEmpty;
            _gap = false;
            _specialInterval = Enums.SpecialIntervalType.IntervalRegular;
        }

        public Reading(DateTime timestamp)
        {
            _baseOrTier = Enums.BaseOrTier.TotalServiceUse;
            _timeOfUse = Enums.TimeOfUse.Undefined;
            _season = Enums.Season.Undefined;
            _timestamp = timestamp;
            _quantity = 0.0;
            _empty = true;
            _gap = false;
            _specialInterval = Enums.SpecialIntervalType.IntervalRegular;
        }


        #region Properties

        public DateTime Timestamp
        {
            get { return _timestamp; }
            set { _timestamp = value; }
        }

        public Enums.BaseOrTier BaseOrTier
        {
            get { return _baseOrTier; }
            set { _baseOrTier = value; }
        }

        public Enums.TimeOfUse TimeOfUse
        {
            get { return _timeOfUse; }
            set { _timeOfUse = value; }
        }

        public Enums.Season Season
        {
            get { return _season; }
            set { _season = value; }
        }

        public double Quantity
        {
            get { return _quantity; }
            set { _quantity = value; }
        }

        public bool IsEmpty
        {
            get { return _empty; }
            set { _empty = value; }
        }

        public bool IsGap { get { return _gap; } set { _gap = value; } }
        public Enums.SpecialIntervalType SpecialInterval { get { return _specialInterval; } set { value = _specialInterval; } }
        #endregion

    }
}
