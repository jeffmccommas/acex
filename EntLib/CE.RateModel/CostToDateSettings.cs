﻿
namespace CE.RateModel
{
    /// <summary>
    /// Settings used by metered service.
    /// Note that 'FirstDateOffset', aka. StartDateOffset is handled by the caller.
    /// The BillToDate API has no knowledge of date adjustments that are made by the caller.
    /// </summary>
    public class CostToDateSettings
    {
        private GeneralGroup _generalGroup;
        private ValidateGroup _validateGroup;
        private SewerGroup _sewerGroup;

        public CostToDateSettings()
        {
            _generalGroup = new GeneralGroup();
            _validateGroup = new ValidateGroup();
            _sewerGroup = new SewerGroup();
        }

        #region Properties

        public GeneralGroup General
        {
            get { return _generalGroup; }
            set { _generalGroup = value; }
        }

        public ValidateGroup Validate
        {
            get { return _validateGroup; }
            set { _validateGroup = value; }
        }

        public SewerGroup Sewer
        {
            get { return _sewerGroup; }
            set { _sewerGroup = value; }
        }

        #endregion

        /// <summary>
        /// General Settings.
        /// </summary>
        public class GeneralGroup
        {
            private bool _prorateMonthlyServiceCharges;
            private bool _prorateNonMeteredCharges;
            private Enums.MinimumChargeType _minimumChargeType;
            private bool _prorateDemandUsageDeterminants;
            private bool _useConversionFactorForGas;
            private double _conversionFactorForGas;
            private bool _useConversionFactorForWater;
            private double _conversionFactorForWater;
            private int _projectedNumberOfDays;             // startDate, endDate, and projectedEndDate should setup by caller; this may be used for filling missing projectedEndDates as needed in API
            private bool _smoothTiers;                      // should always false until smoothing feature added
            private bool _allowMultipleMetersPerService;    // should always false until meter aggregation feature is added
            private string _dstEndDate;
            private bool _allowRebateCalculations;
            private bool _allowBaselineCalculations;
            private Enums.DailyTierBillDaysType _dailyTierBillDaysType;
            private bool _UseProjectedNumDaysforBilltoDate;
            private bool _SupportDailyDemand;

            private const double DefaultConversionFactorForGas = 1.0;
            private const double DefaultConversionFactorForWater = 1.0;
            private const Enums.MinimumChargeType DefaultMinimumChargeType = Enums.MinimumChargeType.NoMinimumCharge;
            private const int DefaultProjectedNumberOfDays = 31;
            private const Enums.DailyTierBillDaysType DefaultDailyTierBillDaysType = Enums.DailyTierBillDaysType.UseProjectedBillDays;

            // CR 53944 - configuration for handling mixed interval
            private bool _handleMixedIntervalReading;

            public GeneralGroup()
            {
                _prorateMonthlyServiceCharges = false;
                _prorateNonMeteredCharges = true;
                _prorateDemandUsageDeterminants = false;
                _useConversionFactorForGas = false;
                _conversionFactorForGas = DefaultConversionFactorForGas;
                _minimumChargeType = DefaultMinimumChargeType;
                _projectedNumberOfDays = DefaultProjectedNumberOfDays;
                _smoothTiers = false;
                _allowMultipleMetersPerService = false;
                _dstEndDate = string.Empty;
                _allowRebateCalculations = false;
                _allowBaselineCalculations = false;
                _useConversionFactorForWater = false;
                _conversionFactorForWater = DefaultConversionFactorForWater;
                _dailyTierBillDaysType = DefaultDailyTierBillDaysType;
                _UseProjectedNumDaysforBilltoDate = false;
                _SupportDailyDemand = true;
            }


            #region Properties

            public bool ProrateMonthlyServiceCharges
            {
                get { return _prorateMonthlyServiceCharges; }
                set { _prorateMonthlyServiceCharges = value; }
            }

            public bool ProrateNonMeteredCharges
            {
                get { return _prorateNonMeteredCharges; }
                set { _prorateNonMeteredCharges = value; }
            }

            public Enums.MinimumChargeType MinimumChargeType
            {
                get { return _minimumChargeType; }
                set { _minimumChargeType = value; }
            }

            public bool ProrateDemandUsageDeterminants
            {
                get { return _prorateDemandUsageDeterminants; }
                set { _prorateDemandUsageDeterminants = value; }
            }

            public bool UseConversionFactorForGas
            {
                get { return _useConversionFactorForGas; }
                set { _useConversionFactorForGas = value; }
            }

            public double ConversionFactorForGas
            {
                get { return _conversionFactorForGas; }
                set { _conversionFactorForGas = value; }
            }

            public int ProjectedNumberOfDays
            {
                get { return _projectedNumberOfDays; }
                set { _projectedNumberOfDays = value; }
            }

            public bool SmoothTiers
            {
                get { return _smoothTiers; }
                set { _smoothTiers = value; }
            }

            public bool AllowMultipleMetersPerService
            {
                get { return _allowMultipleMetersPerService; }
                set { _allowMultipleMetersPerService = value; }
            }

            public string DSTEndDate
            {
                get { return _dstEndDate; }
                set { _dstEndDate = value; }
            }

            public bool AllowRebateCalculations
            {
                get { return _allowRebateCalculations; }
                set { _allowRebateCalculations = value; }
            }

            // CR 53944 - New attribute 
            public bool HandleMixedInterval
            {
                get { return _handleMixedIntervalReading; }
                set { _handleMixedIntervalReading = value; }
            }

            public bool AllowBaselineCalculations
            {
                get { return _allowBaselineCalculations; }
                set { _allowBaselineCalculations = value; }
            }


            public bool UseConversionFactorForWater
            {
                get { return _useConversionFactorForWater; }
                set { _useConversionFactorForWater = value; }
            }

            public double ConversionFactorForWater
            {
                get { return _conversionFactorForWater; }
                set { _conversionFactorForWater = value; }

            }


            public Enums.DailyTierBillDaysType DailyTierBillDaysType
            {
                get { return _dailyTierBillDaysType; }
                set { _dailyTierBillDaysType = value; }
            }

            public bool UseProjectedNumDaysforBilltoDate
            {
                get { return _UseProjectedNumDaysforBilltoDate; }
                set { _UseProjectedNumDaysforBilltoDate = value; }
            }
            /// <summary>
            /// property to indicate if cost to date calc should support daily demand
            /// alerts should hard-coded to false since per requirement, alerts doesn't support daily demand unless change in the future
            /// CR 51610 2014
            /// </summary>
            public bool SupportDailyDemand
            {
                get { return _SupportDailyDemand; }
                set { _SupportDailyDemand = value; }
            }

            #endregion

        }

        /// <summary>
        /// Validation Settings.
        /// </summary>
        public class ValidateGroup
        {
            private bool _checkMaximumMissingDays;
            private bool _checkDaysInFullMonth;
            private bool _checkMinimumBillDays;
            private bool _checkMinimumUsage;
            private int _maximumMissingDays;
            private int _daysInFullMonth;
            private int _minimumBillDaysElectric;
            private int _minimumBillDaysGas;
            private int _minimumBillDaysWater;
            private double _minimumUsage;

            private const int DefaultMaximumMissingDays = 8;
            private const int DefaulDaysInFullMonth = 25;
            private const int DefaulMinimumBillDays = 5;            // for all fuels
            private const int DefaulMinimumUsage = 5;               // for all fuels

            public ValidateGroup()
            {
                _checkMaximumMissingDays = false;
                _checkDaysInFullMonth = false;
                _checkMinimumBillDays = false;
                _checkMinimumUsage = false;
                _maximumMissingDays = DefaultMaximumMissingDays;
                _daysInFullMonth = DefaulDaysInFullMonth;
                _minimumBillDaysElectric = DefaulMinimumBillDays;
                _minimumBillDaysGas = DefaulMinimumBillDays;
                _minimumBillDaysWater = DefaulMinimumBillDays;
                _minimumUsage = DefaulMinimumUsage;
            }

            #region Properties

            public bool CheckMaximumMissingDays
            {
                get { return _checkMaximumMissingDays; }
                set { _checkMaximumMissingDays = value; }
            }

            public bool CheckDaysInFullMonth
            {
                get { return _checkDaysInFullMonth; }
                set { _checkDaysInFullMonth = value; }
            }

            public bool CheckMinimumBillDays
            {
                get { return _checkMinimumBillDays; }
                set { _checkMinimumBillDays = value; }
            }

            public bool CheckMinimumUsage
            {
                get { return _checkMinimumUsage; }
                set { _checkMinimumUsage = value; }
            }

            public int MaximumMissingDays
            {
                get { return _maximumMissingDays; }
                set { _maximumMissingDays = value; }
            }

            public int DaysInFullMonth
            {
                get { return _daysInFullMonth; }
                set { _daysInFullMonth = value; }
            }

            public int MinimumBillDaysElectric
            {
                get { return _minimumBillDaysElectric; }
                set { _minimumBillDaysElectric = value; }
            }

            public int MinimumBillDaysGas
            {
                get { return _minimumBillDaysGas; }
                set { _minimumBillDaysGas = value; }
            }

            public int MinimumBillDaysWater
            {
                get { return _minimumBillDaysWater; }
                set { _minimumBillDaysWater = value; }
            }

            public double MinimumUsage
            {
                get { return _minimumUsage; }
                set { _minimumUsage = value; }
            }


            #endregion

        }

        /// <summary>
        /// Sewer Settings.
        /// </summary>
        public class SewerGroup
        {
            private bool _prorateSewerMaximumMonthsUsageDeterminants;
            private bool _useSewerMaximumMonthsUsageForResidential;
            private bool _useSewerMaximumMonthsUsageForCommercial;
            private int _sewerMaximumMonthsUsageForResidential;
            private int _sewerMaximumMonthsUsageForCommercial;

            private const int DefaultMaximumMonthsUsageForResidential = 80;
            private const int DefaultMaximumMonthsUsageForCommercial = 80;

            public SewerGroup()
            {
                _prorateSewerMaximumMonthsUsageDeterminants = false;
                _useSewerMaximumMonthsUsageForResidential = false;
                _useSewerMaximumMonthsUsageForCommercial = false;
                _sewerMaximumMonthsUsageForResidential = DefaultMaximumMonthsUsageForResidential;
                _sewerMaximumMonthsUsageForCommercial = DefaultMaximumMonthsUsageForCommercial;
            }


            #region Properties

            public bool ProrateSewerMaximumMonthsUsageDeterminants
            {
                get { return _prorateSewerMaximumMonthsUsageDeterminants; }
                set { _prorateSewerMaximumMonthsUsageDeterminants = value; }
            }

            public bool UseSewerMaximumMonthsUsageForResidential
            {
                get { return _useSewerMaximumMonthsUsageForResidential; }
                set { _useSewerMaximumMonthsUsageForResidential = value; }
            }

            public bool UseSewerMaximumMonthsUsageForCommercial
            {
                get { return _useSewerMaximumMonthsUsageForCommercial; }
                set { _useSewerMaximumMonthsUsageForCommercial = value; }
            }

            public int SewerMaximumMonthsUsageForResidential
            {
                get { return _sewerMaximumMonthsUsageForResidential; }
                set { _sewerMaximumMonthsUsageForResidential = value; }
            }

            public int SewerMaximumMonthsUsageForCommercial
            {
                get { return _sewerMaximumMonthsUsageForCommercial; }
                set { _sewerMaximumMonthsUsageForCommercial = value; }
            }

            #endregion

        }

    }

}
