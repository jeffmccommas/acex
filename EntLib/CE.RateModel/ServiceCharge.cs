﻿
namespace CE.RateModel
{
    public class ServiceCharge
    {
        public Enums.ChargeCalcType CalculationType { get; set; }
        public Enums.ServiceStep Step { get; set; }
        public Enums.Season Season { get; set; }
        public Enums.RebateClass RebateClass { get; set; }
        public double ChargeValue { get; set; }
        public string CostType { get; set; }
    }
}
