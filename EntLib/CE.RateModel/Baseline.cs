﻿using System;

namespace CE.RateModel
{
    /// <summary>
    /// Baseline usage for use in Peak-Time-Rebates
    /// </summary>
    public class Baseline
    {
        public DateTime UsageDate { get; set; }
        public double BaselineUsage { get; set; }
        public double ActualUsage { get; set; }
        public double RebateAmount { get; set; }
        public bool ActualUsageMissing { get; set; }
        public bool BaselineUsageMissing { get; set; }
        public bool RebateAmountMissing { get; set; }

        private Baseline()
        {
        }

        public Baseline(DateTime usageDate, double baselineUsage, double actualUsage, double rebateAmount, bool actualUsageMissing, bool baselineUsageMissing, bool rebateAmountMissing)
        {
            UsageDate = usageDate;
            BaselineUsage = baselineUsage;
            BaselineUsageMissing = baselineUsageMissing;
            ActualUsage = actualUsage;
            ActualUsageMissing = actualUsageMissing;
            RebateAmount = rebateAmount;
            RebateAmountMissing = rebateAmountMissing;
        }

    }

}
