﻿
namespace CE.RateModel
{
    public class TouBoundary
    {
        public Enums.TimeOfUse TimeOfUse { get; set; }
        public Enums.Season Season { get; set; }
        public Enums.DayType DayType { get; set; }
        public string StartTime { get; set; }
        public string EndTime { get; set; }
    }
}
