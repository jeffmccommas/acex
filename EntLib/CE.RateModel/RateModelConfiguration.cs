﻿
namespace CE.RateModel
{
    public class RateModelConfiguration
    {
        private bool _enableLogging;
        private string _databaseConn;

        #region Properties

        public bool EnableLogging
        {
            get { return _enableLogging; }
            set { _enableLogging = value; }
        }

        public string DatabaseConn
        {
            get { return _databaseConn; }
            set { _databaseConn = value; }
        }

        #endregion

    }
}
