﻿using CE.RateEngine;
using System;
using System.Configuration;

namespace CE.RateModel
{
    public class RateModelEPFactory : RateModelFactory
    {
        RateModelConfiguration RateModelConfig;
        private const string _connectionStringName = "RateEngineConn";
        private const string _enableLoggingName = "RateEngineLogging";
        private const string _configurationErrorDesc = "Rate Model configuration error.";

        public RateModelEPFactory()
        {

        }

        public override IRateModel CreateRateModel()
        {
            IRateModel result;

            LoadRateModelConfiguration();

            result = new RateModelEPAdapter(new CE.RateEngine.RateEngine(RateModelConfig.DatabaseConn, RateModelConfig.EnableLogging));

            return (result);
        }

        public override IRateModel CreateRateModel(bool logOverride)
        {
            IRateModel result;

            LoadRateModelConfiguration();

            result = new RateModelEPAdapter(new CE.RateEngine.RateEngine(RateModelConfig.DatabaseConn, logOverride));

            return (result);
        }

        public override IRateModel CreateRateModel(RateModelConfiguration config)
        {
            IRateModel result;

            result = new RateModelEPAdapter(new CE.RateEngine.RateEngine(config.DatabaseConn, config.EnableLogging));

            return (result);
        }



        private void LoadRateModelConfiguration()
        {
            string conn = string.Empty;
            bool enableLogging = false;

            try
            {
                conn = ConfigurationManager.ConnectionStrings[_connectionStringName].ConnectionString;
                enableLogging = System.Convert.ToBoolean(ConfigurationManager.AppSettings[_enableLoggingName]);
            }
            catch (Exception ex )
            {
                throw new Exception(_configurationErrorDesc, ex);
            }

            RateModelConfig = new RateModelConfiguration();
            RateModelConfig.DatabaseConn = conn;
            RateModelConfig.EnableLogging = enableLogging;

        }

    }
}
