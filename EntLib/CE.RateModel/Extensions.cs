﻿using System.Collections.Generic;

namespace CE.RateModel
{

    /// <summary>
    /// Extensions for use within Rate Model.
    /// </summary>
    public static class RateModelExtensions
    {
        /// <summary>
        /// Extension to Usage List to determine the best total usage quantity to return from usage determinants.
        /// </summary>
        /// <param name="usages"></param>
        /// <returns></returns>
        public static double GetTotalUsageQuantity(this List<Usage> usages)
        {
            var totalExists = false;
            var totalSeasonalExists = false;
            var totalTOUExists = false;
            var totalTOUSeasonalExists = false;
            var totalTieredExists = false;
            var totalTieredTOUExists = false;
            var totalTieredSeasonalExists = false;
            var totalTieredSeasonalTOUExists = false;

            var total = 0.0;
            var totalSeasonal = 0.0;
            var totalTOU = 0.0;
            var totalTOUSeasonal = 0.0;
            var totalTiered = 0.0;
            var totalTieredTOU = 0.0;
            var totalTieredSeasonal = 0.0;
            var totalTieredSeasonalTOU = 0.0;

            if (usages != null)
            {
                foreach (Usage u in usages)
                {
                    switch (u.BaseOrTier)
                    {
                        case Enums.BaseOrTier.TotalServiceUse:

                            if (u.Season == Enums.Season.Undefined)
                            {
                                totalExists = true;
                                total = total + u.Quantity;
                            }
                            else
                            {
                                totalSeasonalExists = true;
                                totalSeasonal = totalSeasonal + u.Quantity;
                            }

                            break;

                        case Enums.BaseOrTier.Undefined:

                            if (u.Season == Enums.Season.Undefined)
                            {
                                totalTOUExists = true;
                                totalTOU = totalTOU + u.Quantity;
                            }
                            else
                            {
                                totalTOUSeasonalExists = true;
                                totalTOUSeasonal = totalTOUSeasonal + u.Quantity;
                            }

                            break;

                        case Enums.BaseOrTier.Tier1:
                        case Enums.BaseOrTier.Tier2:
                        case Enums.BaseOrTier.Tier3:
                        case Enums.BaseOrTier.Tier4:
                        case Enums.BaseOrTier.Tier5:
                        case Enums.BaseOrTier.Tier6:

                            if (u.Season == Enums.Season.Undefined)
                            {
                                if (u.TimeOfUse == Enums.TimeOfUse.Undefined)
                                {
                                    totalTieredExists = true;
                                    totalTiered = totalTiered + u.Quantity;
                                }
                                else
                                {
                                    totalTieredTOUExists = true;
                                    totalTieredTOU = totalTieredTOU + u.Quantity;
                                }

                            }
                            else
                            {
                                if (u.TimeOfUse == Enums.TimeOfUse.Undefined)
                                {
                                    totalTieredSeasonalExists = true;
                                    totalTieredSeasonal = totalTieredSeasonal + u.Quantity;
                                }
                                else
                                {
                                    totalTieredSeasonalTOUExists = true;
                                    totalTieredSeasonalTOU = totalTieredSeasonalTOU + u.Quantity;
                                }
                            }

                            break;

                        default:
                            //noop
                            break;
                    }

                }

            }


            // return first present and calculated total.

            if (totalExists)
            {
                return (total);
            }

            if (totalSeasonalExists)
            {
                return (totalSeasonal);
            }

            if (totalTOUExists)
            {
                return (totalTOU);
            }

            if (totalTOUSeasonalExists)
            {
                return (totalTOUSeasonal);
            }

            if (totalTieredExists)
            {
                return (totalTiered);
            }

            if (totalTieredTOUExists)
            {
                return (totalTieredTOU);
            }

            if (totalTieredSeasonalExists)
            {
                return (totalTieredSeasonal);
            }

            if (totalTieredSeasonalTOUExists)
            {
                return (totalTieredSeasonalTOU);
            }

            return (0.0);
        }

    }

}
