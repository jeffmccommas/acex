﻿
namespace CE.RateModel
{
    public class SeasonBoundary
    {
        public Enums.Season Season { get; set; }
        public string StartDay { get; set; }
        public string EndDay { get; set; }
    }
}
