﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CE.RateModel
{
    public class TierBoundary
    {
        public Enums.BaseOrTier BaseOrTier { get; set; }
        public Enums.TimeOfUse TimeOfUse { get; set; }
        public Enums.Season Season { get; set; }
        public int DaysIntoSeason { get; set; }
        public double SeasonFactor { get; set; }
        public double Threshold { get; set; }
        public double SecondaryThreshold { get; set; }

        public TierBoundary()
        {

        }

    }
}
