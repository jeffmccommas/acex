﻿
using CE.WebServiceClient.DRHub;
using CE.WebServiceClient.Nuance;
using System.Threading;
using System.Threading.Tasks;
using System;

namespace CE.PJMDRNotify.DRMS
{
    public class NotificationManager
    {
        private const string PjmNoticationEvent = "PJM Notification Event";
        private const string SeverityLow = "Low";
        private const string SeverityHigh = "High";
        private const string CatagoryNotification = "Notification";
        private const string MsgNotification = "Notification Process";
        private const string EventBegin = "Begins";
        private const string EventEnd = "Ends";
        private const string DatabaseLog = "Database Log";
        public void ProcessNotifications(int clientId)
        {            
            var logger = new NLogger.ASyncLogger(clientId, string.Empty);
            try
            {
                Console.WriteLine("{0} {1}", MsgNotification, EventBegin);
                Console.WriteLine("{0} {1}", DatabaseLog, EventBegin);
                logger.Info(CatagoryNotification, SeverityLow, PjmNoticationEvent, string.Format("{0} {1}", MsgNotification, EventBegin));
                Console.WriteLine("{0} {1}", DatabaseLog, EventEnd);
            }
            catch(Exception ex)
            {
                logger.ErrorException(CatagoryNotification, SeverityHigh, PjmNoticationEvent, ex);
                logger.EmailErrorException(CatagoryNotification, SeverityHigh, PjmNoticationEvent, ex);
                throw;
            }


            Console.WriteLine("Initiate DRHub factory");

            var drHubFactory = new DRHubFactory();
            var client = drHubFactory.CreateDRHub(clientId);
           
            var eventsCol = client.PollEvents();


            
            if (eventsCol != null && eventsCol.Events.Count > 0)
            {
                // create nuance events
                var nuanceFactory = new NuanceFactory();
                var nuanceClient = nuanceFactory.CreateNuance(clientId);
                nuanceClient.SendMessage(eventsCol, 0);

                
                client.AcknowledgeEvents(eventsCol, 0);

            }
            else
            {
                // ping nuance
                PingNuance(clientId);

            }

            try
            {
                Console.WriteLine("{0} {1}", MsgNotification, EventEnd);
                logger.Info(CatagoryNotification, SeverityLow, PjmNoticationEvent, string.Format("{0} {1}", MsgNotification, EventEnd));
            }
            catch (Exception ex)
            {
                logger.ErrorException(CatagoryNotification, SeverityHigh, PjmNoticationEvent, ex);
                logger.EmailErrorException(CatagoryNotification, SeverityHigh, PjmNoticationEvent, ex);
                throw;
            }
            //return false;
        }



        /// <summary>
        /// ping nuance client webserivce
        /// </summary>
        /// <param name="clientId"></param>
        public void PingNuance(int clientId)
        {
            var nuanceFactory = new NuanceFactory();
            var client = nuanceFactory.CreateNuance(clientId);
            client.PingNuance(0);
        }



    }
}
