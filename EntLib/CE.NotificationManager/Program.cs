﻿using CE.WebServiceClient.ELRS;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CE.NotificationManager
{
    class Program
    {
        static void Main(string[] args)
        {
            var manager = new DRMS.NotificationManager();

            manager.Process();
        }
    }
}
