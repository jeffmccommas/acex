﻿using CE.ContentModel;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace CE.NotificationManagerTests
{

    [TestClass]
    public class NotificationMangerTests
    {
        [TestInitialize]
        public void NotificationTestInit()
        {
            // This initializes the automapper!
            new AutoMapperBootStrapper().BootStrap();
        }

        [TestCleanup]
        public void NotificationTestClean()
        {
            //noop
        }

        [TestMethod]
        public void ProcessNotificationsTest()
        {
            try
            {
                var manager = new NotificationManager.DRMS.NotificationManager();
                var clientId = 233;
                manager.ProcessNotifications(clientId);
            }
            catch (Exception ex)
            {
                Console.Out.WriteLine(ex.Message);
                throw;
            }
        }
    }
}
