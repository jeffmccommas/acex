﻿using CE.RateModel;
using System;
using System.Collections.Generic;

namespace CE.BillToDate
{
    public interface IBillToDate
    {
        BillToDateResult CalculateBillToDate(Bill input);

        CostToDateResult CalculateMeterCostToDate(Meter input);

        NonMeteredServiceResult CalculateNonMeteredServiceCostToDate(NonMeteredService input);

        //RateClassList GetRateClassList(int rateCompanyID);

        List<TierBoundary> GetTierBoundaries(int rateCompanyId, string rateClass, DateTime startDate, DateTime endDate);

        RateClass GetRateClassInformation(int rateCompanyID, string rateClass);

        //string GenerateTestData(Bill bill, BillToDateSettings btds);

        List<string> GetLoggingInfo(string logSessionId);
    }
}
