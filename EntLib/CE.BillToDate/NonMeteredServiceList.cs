﻿using System;
using System.Collections.Generic;

namespace CE.BillToDate
{
    public class NonMeteredServiceList : List<NonMeteredService>
    {
        public NonMeteredServiceList()
        {

        }
        /*
        public int getLargestNumberOfBillingDays()
        {
            TimeSpan tsBillingDaysDiff;
            int maxBillingDays = 0;
            int diffBillingDays = 0;
            foreach (var item in this)
            {
                //I wanted to use EndDate here, but I can't because it is incorrect. Enddate for a non-metered service should be today's date and it is not, so I am just going to get the current date
                tsBillingDaysDiff = (TimeSpan)(DateTime.Now - item.StartDate);
                diffBillingDays = tsBillingDaysDiff.Days + 1;
                if (maxBillingDays < diffBillingDays)
                {
                    maxBillingDays = diffBillingDays;
                }
            }
            return maxBillingDays;
        }

        public int getLargestProjectedBillingDays()
        {
            TimeSpan tsBillingDaysDiff;
            int maxBillingDays = 0;
            int diffBillingDays = 0;
            foreach (var item in this)
            {
                //I wanted to use EndDate here, but I can't because it is incorrect. Enddate for a non-metered service should be today's date and it is not, so I am just going to get the current date
                tsBillingDaysDiff = (TimeSpan)(item.ProjectedEndDate - item.StartDate);
                diffBillingDays = tsBillingDaysDiff.Days + 1;
                if (maxBillingDays < diffBillingDays)
                {
                    maxBillingDays = diffBillingDays;
                }
            }
            return maxBillingDays;

        }

        public DateTime getLargestCurrentReadDate()
        {
            DateTime largestCurrentReadDate = DateTime.MinValue;
            foreach (var item in this)
            {
                if (item.EndDate != null && item.EndDate > largestCurrentReadDate)
                    largestCurrentReadDate = (DateTime)item.EndDate;
            }

            return largestCurrentReadDate;

        }

        public DateTime getLargestNextReadDate()
        {
            DateTime largestNextReadDate = DateTime.MinValue;
            foreach (var item in this)
            {
                if (item.ProjectedEndDate != null && item.ProjectedEndDate > largestNextReadDate)
                    largestNextReadDate = (DateTime)item.ProjectedEndDate;
            }

            return largestNextReadDate;
        }
       */
    }
}
