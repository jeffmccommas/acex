﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using CE.RateModel;
using CE.BillToDate;
using System.Xml;
using CE.ContentModel;

namespace UnitTests
{
    public partial class BillToDateTests
    {

        [TestMethod]
        public void TieredBoundaries_TestA()
        {
            CalculationManagerFactory billToDateFactory = null;
            IBillToDate mgr = null;
            var rateClass = string.Empty;
            DateTime startDate;
            DateTime endDate;

            int clientid = 87;      //same as referrerid
            int rateCompanyID = 0;

            // READ BILL-TO-DATE SETTINGS
            var reader = new CE.BillToDate.Configuration.SettingsReader(clientid);
            var btdSettings = reader.GetSettings();
            rateCompanyID = btdSettings.RateCompanyId;

            //CREATE CALCULATION MANAGER - INJECT THE SETTINGS THAT WERE READ FROM CONTENT FOR THE CLIENTID
            billToDateFactory = new CalculationManagerFactory();
            mgr = billToDateFactory.CreateCalculationManager(btdSettings); //inject settings that we read from content

            startDate = new DateTime(2015, 12, 1);
            endDate = new DateTime(2015, 12, 8);
            rateClass = "binBasic"; //binBasic, binTierM, binTOU

            // TIER BOUNDARIES, a tiered rate class
            var tierBoundaries = mgr.GetTierBoundaries(rateCompanyID, "binTierM", startDate, endDate);

            // TIER BOUNDARIES, a tiered rate class
            var tierBoundaries2 = mgr.GetTierBoundaries(rateCompanyID, "binBasic", startDate, endDate);

            // TIER BOUNDARIES, a tiered rate class
            var tierBoundaries3 = mgr.GetTierBoundaries(rateCompanyID, "binTOU", startDate, endDate);

           

        }

        [TestMethod]
        public void GetRateClassInformation_TestA()
        {
            CalculationManagerFactory billToDateFactory = null;
            IBillToDate mgr = null;

            int clientid = 87;      //same as referrerid
            int rateCompanyID = 0;

            // READ BILL-TO-DATE SETTINGS
            var reader = new CE.BillToDate.Configuration.SettingsReader(clientid);
            var btdSettings = reader.GetSettings();
            rateCompanyID = btdSettings.RateCompanyId;

            //CREATE CALCULATION MANAGER - INJECT THE SETTINGS THAT WERE READ FROM CONTENT FOR THE CLIENTID
            billToDateFactory = new CalculationManagerFactory();
            mgr = billToDateFactory.CreateCalculationManager(btdSettings); //inject settings that we read from content

            var res = mgr.GetRateClassInformation(rateCompanyID, "binTierM");
            var res2 = mgr.GetRateClassInformation(rateCompanyID, "binBasic");
            var res3 = mgr.GetRateClassInformation(rateCompanyID, "binTOU");

        }

    }
}
