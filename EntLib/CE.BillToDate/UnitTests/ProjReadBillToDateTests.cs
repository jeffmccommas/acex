﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using CE.RateModel;
using CE.BillToDate;
using System.Xml;

namespace UnitTests
{
    // continuation of test class
    public partial class BillToDateTests
    {
        /// <summary>
        /// Test the "binTP3a" rate class (Tiered Time-of-use) with meter readings from 4/10/2012 to 5/2/2012 inclusive.
        /// Projected cost calculation should project readings.  5/10/2012 is projected end date.
        /// NOTE THAT ALL OF THESE TEST METHODS IN THIS FILE USE REFERRER=87, RATECOMPANYID=100
        /// </summary>
        [TestMethod]
        public void BillToDate_TestProjectingReadings_1_binTP3a()
        {
            CalculationManagerFactory billToDateFactory = null;
            IBillToDate billToDateCalculator = null;
            Bill bill = null;
            MeteredService meteredService = null;
            List<Reading> readings = null;
            Meter meter = null;
            const int referrerId = 87;
            const int rateCompanyId = 100;
            const string customerId = "customerid";   // for reference and use in output
            const string accountId = "accountid";     // for reference and use in output

            billToDateFactory = new CalculationManagerFactory();
            billToDateCalculator = billToDateFactory.CreateCalculationManager();

            var startDate = new DateTime(2012, 4, 10);
            var endDate = new DateTime(2012, 5, 2);
            var projectedEndDate = new DateTime(2012, 5, 10);

            // create a bill-to-date bill
            bill = new Bill(startDate, referrerId, customerId, accountId, rateCompanyId);

            // create a metered service
            var serviceId = "serviceid";    // for reference and use in output
            var premiseId = "premiseid";    // for reference and use in output
            var fuel = FuelType.electric;
            meteredService = new MeteredService(serviceId, premiseId, fuel);

            // create and simulate data for a meter, add to metered service
            var meterId = "EM15M00FIX";
            var rateClass = "binTP3a";
            readings = GetAMI("EM15M00FIX.xml");
            meter = new Meter(meterId, rateCompanyId, rateClass, fuel, startDate, endDate, projectedEndDate, readings);
            meteredService.Meters.Add(meter);
            bill.MeteredServices.Add(meteredService);

            // make main call
            var result = billToDateCalculator.CalculateBillToDate(bill);

            Assert.IsTrue(result.StatusList.HasNoErrors() == true);
        }


        /// <summary>
        /// TP3 - Tiered TOU Seasonal; 3/15-2012 to 4/4/2012
        /// Season changes on 4/1 from winter to summer
        /// </summary>
        [TestMethod]
        public void BillToDate_TestProjectingReadings_2_binTP3()
        {
            CalculationManagerFactory billToDateFactory = null;
            IBillToDate billToDateCalculator = null;
            Bill bill = null;
            MeteredService meteredService = null;
            List<Reading> readings = null;
            Meter meter = null;
            const int referrerId = 87;
            const int rateCompanyId = 100;
            const string customerId = "customerid";   // for reference and use in output
            const string accountId = "accountid";     // for reference and use in output

            billToDateFactory = new CalculationManagerFactory();
            billToDateCalculator = billToDateFactory.CreateCalculationManager();

            var startDate = new DateTime(2012, 3, 15);
            var endDate = new DateTime(2012, 4, 4);
            var projectedEndDate = new DateTime(2012, 4, 15);

            // create a bill-to-date bill
            bill = new Bill(startDate, referrerId, customerId, accountId, rateCompanyId);

            // create a metered service
            var serviceId = "serviceid";    // for reference and use in output
            var premiseId = "premiseid";    // for reference and use in output
            var fuel = FuelType.electric;
            meteredService = new MeteredService(serviceId, premiseId, fuel);

            // create and simulate data for a meter, add to metered service
            var meterId = "EM15M00FIX";
            var rateClass = "binTP3";
            readings = GetAMI("EM15M0SFIX.xml");
            meter = new Meter(meterId, rateCompanyId, rateClass, fuel, startDate, endDate, projectedEndDate, readings);
            meteredService.Meters.Add(meter);
            bill.MeteredServices.Add(meteredService);

            // make main call
            var result = billToDateCalculator.CalculateBillToDate(bill);

            Assert.IsTrue(result.StatusList.HasNoErrors() == true);
        }

        /// <summary>
        /// TP3x - Tiered TOU Seasonal; 3/15-2012 to 4/4/2012
        /// Season changes on 4/1 from winter to summer
        /// Using binTP3x rate class, which is single hour of peak per day
        /// EM15M0S2FIX has all AMI in winter for first BTD; projectedBTD cross from winter to summer
        /// GOOD ONE TO TEST WITH
        /// UT1
        /// </summary>
        [TestMethod]
        public void BillToDate_TestProjectingReadings_3_15min_binTP3x()
        {
            CalculationManagerFactory billToDateFactory = null;
            IBillToDate billToDateCalculator = null;
            Bill bill = null;
            MeteredService meteredService = null;
            List<Reading> readings = null;
            Meter meter = null;
            const int referrerId = 87;
            const int rateCompanyId = 100;
            const string customerId = "customerid";   // for reference and use in output
            const string accountId = "accountid";     // for reference and use in output

            billToDateFactory = new CalculationManagerFactory();
            billToDateCalculator = billToDateFactory.CreateCalculationManager();

            var startDate = new DateTime(2012, 3, 15);
            var endDate = new DateTime(2012, 3, 29);
            var projectedEndDate = new DateTime(2012, 4, 15);

            // create a bill-to-date bill
            bill = new Bill(startDate, referrerId, customerId, accountId, rateCompanyId);

            // create a metered service
            var serviceId = "serviceid";    // for reference and use in output
            var premiseId = "premiseid";    // for reference and use in output
            var fuel = FuelType.electric;
            meteredService = new MeteredService(serviceId, premiseId, fuel);

            // create and simulate data for a meter, add to metered service
            var meterId = "EM15M00FIX";
            var rateClass = "binTP3x";
            readings = GetAMI("EM15M0S2FIX.xml");
            meter = new Meter(meterId, rateCompanyId, rateClass, fuel, startDate, endDate, projectedEndDate, readings);
            meteredService.Meters.Add(meter);
            bill.MeteredServices.Add(meteredService);

            // make main call
            var result = billToDateCalculator.CalculateBillToDate(bill);

            Assert.IsTrue(result.StatusList.HasNoErrors() == true);
        }

        [TestMethod]
        public void BillToDate_TestProjectingReadings_Load_15min_binTP3x()
        {
            DateTime startTime;
            DateTime endTime;
            TimeSpan span;
            const int max = 20;

            startTime = DateTime.Now;

            for (var i = 0; i < max; i++)
            {
                BillToDate_TestProjectingReadings_3_15min_binTP3x();
            }

            endTime = DateTime.Now;

            span = endTime - startTime;

            // always assert okay for now
            Assert.Inconclusive("(actually passed) iterations = " + max + ", totalSeconds = " + span.TotalSeconds);
        }

        /// <summary>
        /// TP3x - Tiered TOU Seasonal; 3/15-2012 to 4/4/2012
        /// Season changes on 4/1 from winter to summer
        /// Using binTP3x rate class, which is single hour of peak per day
        /// EM60M0S2FIX has all AMI in winter for first BTD; projectedBTD cross from winter to summer
        /// </summary>
        [TestMethod]
        public void BillToDate_TestProjectingReadings_3h_binTP3x()
        {
            CalculationManagerFactory billToDateFactory = null;
            IBillToDate billToDateCalculator = null;
            Bill bill = null;
            MeteredService meteredService = null;
            List<Reading> readings = null;
            Meter meter = null;
            const int referrerId = 87;
            const int rateCompanyId = 100;
            const string customerId = "customerid";   // for reference and use in output
            const string accountId = "accountid";     // for reference and use in output

            billToDateFactory = new CalculationManagerFactory();
            billToDateCalculator = billToDateFactory.CreateCalculationManager();

            var startDate = new DateTime(2012, 3, 15);
            var endDate = new DateTime(2012, 3, 29);
            var projectedEndDate = new DateTime(2012, 4, 15);

            // create a bill-to-date bill
            bill = new Bill(startDate, referrerId, customerId, accountId, rateCompanyId);

            // create a metered service
            var serviceId = "serviceid";    // for reference and use in output
            var premiseId = "premiseid";    // for reference and use in output
            var fuel = FuelType.electric;
            meteredService = new MeteredService(serviceId, premiseId, fuel);

            // create and simulate data for a meter, add to metered service
            var meterId = "EM60M0S2FIX";
            var rateClass = "binTP3x";
            readings = GetAMI("EM60M0S2FIX.xml");
            meter = new Meter(meterId, rateCompanyId, rateClass, fuel, startDate, endDate, projectedEndDate, readings);
            meteredService.Meters.Add(meter);
            bill.MeteredServices.Add(meteredService);

            // make main call
            var result = billToDateCalculator.CalculateBillToDate(bill);

            Assert.IsTrue(result.StatusList.HasNoErrors() == true);
        }

        /// <summary>
        /// Try single day to test binning.  10 kWh per hour (rather than 1 per hour)
        /// To verify binning of tiers.  ONE DAY
        /// </summary>
        [TestMethod]
        public void BillToDate_TestProjectingReadings_3h1_binTP3x()
        {
            CalculationManagerFactory billToDateFactory = null;
            IBillToDate billToDateCalculator = null;
            Bill bill = null;
            MeteredService meteredService = null;
            List<Reading> readings = null;
            Meter meter = null;
            const int referrerId = 87;
            const int rateCompanyId = 100;
            const string customerId = "customerid";   // for reference and use in output
            const string accountId = "accountid";     // for reference and use in output

            billToDateFactory = new CalculationManagerFactory();
            billToDateCalculator = billToDateFactory.CreateCalculationManager();

            var startDate = new DateTime(2012, 3, 15);
            var endDate = new DateTime(2012, 3, 15);
            var projectedEndDate = new DateTime(2012, 4, 15);

            // create a bill-to-date bill
            bill = new Bill(startDate, referrerId, customerId, accountId, rateCompanyId);

            // create a metered service
            var serviceId = "serviceid";    // for reference and use in output
            var premiseId = "premiseid";    // for reference and use in output
            var fuel = FuelType.electric;
            meteredService = new MeteredService(serviceId, premiseId, fuel);

            // create and simulate data for a meter, add to metered service
            var meterId = "EM60M0S3FIX";
            var rateClass = "binTP3x";
            readings = GetAMI("EM60M0S3FIX.xml");
            meter = new Meter(meterId, rateCompanyId, rateClass, fuel, startDate, endDate, projectedEndDate, readings);
            meteredService.Meters.Add(meter);
            bill.MeteredServices.Add(meteredService);

            // make main call
            var result = billToDateCalculator.CalculateBillToDate(bill);

            Assert.IsTrue(result.StatusList.HasNoErrors() == true);
        }

        /// <summary>
        /// binDRTOU 
        /// Using binDRTOU rate class; like SDG&E rate class, complex, had issues; has PTR that is unsupported in this Unit Test
        /// </summary>
        [TestMethod]
        public void BillToDate_TestProjectingReadings_4_binDRTOU_XX()
        {
            CalculationManagerFactory billToDateFactory = null;
            IBillToDate billToDateCalculator = null;
            Bill bill = null;
            MeteredService meteredService = null;
            List<Reading> readings = null;
            Meter meter = null;
            const int referrerId = 87;
            const int rateCompanyId = 100;
            const string customerId = "customerid";   // for reference and use in output
            const string accountId = "accountid";     // for reference and use in output

            billToDateFactory = new CalculationManagerFactory();
            billToDateCalculator = billToDateFactory.CreateCalculationManager();

            var startDate = new DateTime(2012, 3, 15);
            var endDate = new DateTime(2012, 4, 4);
            var projectedEndDate = new DateTime(2012, 4, 15);

            // create a bill-to-date bill
            bill = new Bill(startDate, referrerId, customerId, accountId, rateCompanyId);

            // create a metered service
            var serviceId = "serviceid";    // for reference and use in output
            var premiseId = "premiseid";    // for reference and use in output
            var fuel = FuelType.electric;
            meteredService = new MeteredService(serviceId, premiseId, fuel);

            // create and simulate data for a meter, add to metered service
            // CALIFORNIA STYLE; DEMO uses pipies for terr+code delimiter!
            var meterId = "EM15M00FIX";
            var rateClass = "binDRTOU|B1";
            readings = GetAMI("EM15M0SFIX.xml");
            meter = new Meter(meterId, rateCompanyId, rateClass, fuel, startDate, endDate, projectedEndDate, readings);
            meteredService.Meters.Add(meter);
            bill.MeteredServices.Add(meteredService);

            // make main call
            var result = billToDateCalculator.CalculateBillToDate(bill);

            Assert.IsTrue(result.StatusList.HasNoErrors() == true);
        }

        /// <summary>
        /// binDRTOU - season cross at 5/1
        /// 4/27 to 5/11 data; proj to 5/27
        /// </summary>
        [TestMethod]
        public void BillToDate_TestProjectingReadings_4_binDRTOU_XX_seasonX()
        {
            CalculationManagerFactory billToDateFactory = null;
            IBillToDate billToDateCalculator = null;
            Bill bill = null;
            MeteredService meteredService = null;
            List<Reading> readings = null;
            Meter meter = null;
            const int referrerId = 87;
            const int rateCompanyId = 100;
            const string customerId = "customerid";   // for reference and use in output
            const string accountId = "accountid";     // for reference and use in output

            billToDateFactory = new CalculationManagerFactory();
            billToDateCalculator = billToDateFactory.CreateCalculationManager();

            var startDate = new DateTime(2012, 4, 27);
            var endDate = new DateTime(2012, 5, 11);
            var projectedEndDate = new DateTime(2012, 5, 27);

            // create a bill-to-date bill
            bill = new Bill(startDate, referrerId, customerId, accountId, rateCompanyId);

            // create a metered service
            var serviceId = "serviceid";    // for reference and use in output
            var premiseId = "premiseid";    // for reference and use in output
            var fuel = FuelType.electric;
            meteredService = new MeteredService(serviceId, premiseId, fuel);

            // create and simulate data for a meter, add to metered service
            // CALIFORNIA STYLE; DEMO uses pipies for terr+code delimiter!
            var meterId = "EM15M0P1FIX";
            var rateClass = "binDRTOU|B1";
            readings = GetAMI("EM15M0P1FIX.xml");
            meter = new Meter(meterId, rateCompanyId, rateClass, fuel, startDate, endDate, projectedEndDate, readings);
            meteredService.Meters.Add(meter);
            bill.MeteredServices.Add(meteredService);

            // make main call
            var result = billToDateCalculator.CalculateBillToDate(bill);

            Assert.IsTrue(result.StatusList.HasNoErrors() == true);
        }


        /// <summary>
        /// Test of near zero usage.  Related to old issues too.
        /// </summary>
        [TestMethod]
        public void BillToDate_TestProjectingReadings_5_nearZero()
        {
            CalculationManagerFactory billToDateFactory = null;
            IBillToDate billToDateCalculator = null;
            Bill bill = null;
            MeteredService meteredService = null;
            List<Reading> readings = null;
            Meter meter = null;
            const int referrerId = 87;
            const int rateCompanyId = 100;
            const string customerId = "customerid";   // for reference and use in output
            const string accountId = "accountid";     // for reference and use in output

            billToDateFactory = new CalculationManagerFactory();
            billToDateCalculator = billToDateFactory.CreateCalculationManager();

            var startDate = new DateTime(2012, 3, 15);
            var endDate = new DateTime(2012, 3, 18);
            var projectedEndDate = new DateTime(2012, 4, 15);

            // create a bill-to-date bill
            bill = new Bill(startDate, referrerId, customerId, accountId, rateCompanyId);

            // create a metered service
            var serviceId = "serviceid";    // for reference and use in output
            var premiseId = "premiseid";    // for reference and use in output
            var fuel = FuelType.electric;
            meteredService = new MeteredService(serviceId, premiseId, fuel);

            // create and simulate data for a meter, add to metered service
            var meterId = "EM60M00801";
            var rateClass = "binTP3";
            readings = GetAMI("EM60M00801.xml");
            meter = new Meter(meterId, rateCompanyId, rateClass, fuel, startDate, endDate, projectedEndDate, readings);
            meteredService.Meters.Add(meter);
            bill.MeteredServices.Add(meteredService);

            // make main call
            var result = billToDateCalculator.CalculateBillToDate(bill);

            Assert.IsTrue(result.StatusList.HasNoErrors() == true);
        }



        [TestMethod]
        public void BillToDate_TestProjectingReadings_4_binDRTOU_WITH_BASELINES()
        {
            CalculationManagerFactory billToDateFactory = null;
            IBillToDate billToDateCalculator = null;
            Bill bill = null;
            MeteredService meteredService = null;
            List<Reading> readings = null;
            List<Baseline> baselines = null;
            Meter meter = null;
            const int referrerId = 87;
            const int rateCompanyId = 100;
            const string customerId = "customerid";   // for reference and use in output
            const string accountId = "accountid";     // for reference and use in output

            billToDateFactory = new CalculationManagerFactory();
            billToDateCalculator = billToDateFactory.CreateCalculationManager();

            var startDate = new DateTime(2012, 3, 15);
            var endDate = new DateTime(2012, 4, 4);
            var projectedEndDate = new DateTime(2012, 4, 15);

            // create a bill-to-date bill
            bill = new Bill(startDate, referrerId, customerId, accountId, rateCompanyId);

            // create a metered service
            var serviceId = "serviceid";    // for reference and use in output
            var premiseId = "premiseid";    // for reference and use in output
            var fuel = FuelType.electric;
            meteredService = new MeteredService(serviceId, premiseId, fuel);

            // create and simulate data for a meter, add to metered service
            // CALIFORNIA STYLE; DEMO uses pipies for terr+code delimiter!
            var meterId = "EM15M00FIX";
            var rateClass = "binDRTOU|B1";
            readings = GetAMI("EM15M0SFIX.xml");

            //CALLER should probably check and see if rateClass needs baselines first, if it is a PTR rate class;
            baselines = GetBaselines("xxx");   

            meter = new Meter(meterId, rateCompanyId, rateClass, fuel, startDate, endDate, projectedEndDate, readings, baselines);
            meteredService.Meters.Add(meter);
            bill.MeteredServices.Add(meteredService);

            // make main call
            var result = billToDateCalculator.CalculateBillToDate(bill);

            Assert.IsTrue(result.StatusList.HasNoErrors() == true);
        }

        [TestMethod]
        public void BillToDate_Test_PTR()
        {
            CalculationManagerFactory billToDateFactory = null;
            IBillToDate billToDateCalculator = null;
            Bill bill = null;
            MeteredService meteredService = null;
            List<Reading> readings = null;
            List<Baseline> baselines = null;
            Meter meter = null;
            const int referrerId = 87;
            const int rateCompanyId = 100;
            const string customerId = "lax4";   // for reference and use in output
            const string accountId = "199322518K";     // for reference and use in output

            billToDateFactory = new CalculationManagerFactory();
            billToDateCalculator = billToDateFactory.CreateCalculationManager();

            var startDate = new DateTime(2012, 4, 1);
            var endDate = new DateTime(2012, 4, 17);
            var projectedEndDate = new DateTime(2012, 4, 30);

            // create a bill-to-date bill
            bill = new Bill(startDate, referrerId, customerId, accountId, rateCompanyId);

            // create a metered service
            var serviceId = "pk091";    // for reference and use in output
            var premiseId = "pk091";    // for reference and use in output
            var fuel = FuelType.electric;
            meteredService = new MeteredService(serviceId, premiseId, fuel);

            // create and simulate data for a meter, add to metered service
            // CALIFORNIA STYLE; DEMO uses pipies for terr+code delimiter!
            var meterId = "EM30M001V2";
            var rateClass = "PTR-A-TEST";
            readings = GetAMI("EM30M001V2.xml");

            //CALLER should probably check and see if rateClass needs baselines first, if it is a PTR rate class;
            baselines = GetBaselines("EM30M001V2-Baselines.xml");

            meter = new Meter(meterId, rateCompanyId, rateClass, fuel, startDate, endDate, projectedEndDate, readings, baselines);
            meteredService.Meters.Add(meter);
            bill.MeteredServices.Add(meteredService);

            // make main call
            var result = billToDateCalculator.CalculateBillToDate(bill);

            Assert.IsTrue(result.StatusList.HasNoErrors() == true);
        }




    }
}
