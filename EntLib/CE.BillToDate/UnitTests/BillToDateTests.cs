﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using CE.RateModel;
using CE.BillToDate;
using System.Xml;

namespace UnitTests
{

    [TestClass]
    public partial class BillToDateTests
    {
        [TestInitialize]
        public void TestInit()
        {
            // This initializes the automapper!
            new CE.ContentModel.AutoMapperBootStrapper().BootStrap();
        }

        /// <summary>
        /// To test individual component that does a meter cost to date.
        /// </summary>
        [TestMethod]
        public void BillToDate_DefaultConstructor_CalculateMeterCostToDate()
        {
            CalculationManagerFactory billToDateFactory = null;
            IBillToDate billToDateCalculator = null;
            int rateCompanyID;
            string rateClass = String.Empty;
            DateTime startDate;
            DateTime endDate;
            DateTime projectedEndDate;
            List<Reading> readings;
            CostToDateResult result;
            FuelType fuel;
            Meter meter = null;

            billToDateFactory = new CalculationManagerFactory();
            billToDateCalculator = billToDateFactory.CreateCalculationManager();

            // simulate data for a meter
            readings = GetAMI("EM60M00000001.xml");
            rateCompanyID = 101;                        //generic referrer (84)
            rateClass = "P1";
            fuel = FuelType.electric;
            startDate = new DateTime(2010, 1, 1);
            endDate = new DateTime(2010, 1, 25);
            projectedEndDate = new DateTime(2010, 1, 31);

            meter = new Meter("12345", rateCompanyID, rateClass, fuel, startDate, endDate, projectedEndDate);
            meter.Readings = readings;

            result = billToDateCalculator.CalculateMeterCostToDate(meter);

            Assert.IsTrue(result.ErrCode == Enums.ErrorCode.NoError);
        }

        /// <summary>
        /// To test validation of minimum bill days for Gas
        /// </summary>
        [TestMethod]
        public void BillToDate_ValidateMinimumBillDays_Gas()
        {
            CalculationManagerFactory billToDateFactory = null;
            IBillToDate billToDateCalculator = null;
            int rateCompanyID;
            string rateClass = String.Empty;
            DateTime startDate;
            DateTime endDate;
            DateTime projectedEndDate;
            List<Reading> readings;
            CostToDateResult result;
            FuelType fuel;
            Meter meter = null;
            BillToDateSettings btdSettings = null;

            // provide settings to calc manager; this normally populated from referrer settings
            // Change settings here to enable sewer handling
            btdSettings = new BillToDateSettings();
            btdSettings.Settings.Validate.CheckMinimumBillDays = true;
            btdSettings.Settings.Validate.CheckMaximumMissingDays = true;
            btdSettings.Settings.General.UseConversionFactorForGas = true;
            billToDateFactory = new CalculationManagerFactory();
            billToDateCalculator = billToDateFactory.CreateCalculationManager(btdSettings);
           
            // simulate data for a meter
            readings = GetAMI("EM60M00000001.xml");
            rateCompanyID = 101;                        //generic referrer (84)
            rateClass = "P1";
            fuel = FuelType.gas;
            startDate = new DateTime(2010, 1, 1);
            endDate = new DateTime(2010, 1, 25);
            projectedEndDate = new DateTime(2010, 1, 31);

            meter = new Meter("12345", rateCompanyID, rateClass, fuel, startDate, endDate, projectedEndDate);
            meter.Readings = readings;

            result = billToDateCalculator.CalculateMeterCostToDate(meter);

            Assert.IsTrue(result.ErrCode == Enums.ErrorCode.NoError);
        }

        /// <summary>
        /// To test ProcessConversionfactors and revertConversion for Gas
        /// </summary>
        [TestMethod]
        public void BillToDate_ProcessConvFactor_Gas()
        {
            CalculationManagerFactory billToDateFactory = null;
            IBillToDate billToDateCalculator = null;
            int rateCompanyID;
            string rateClass = String.Empty;
            DateTime startDate;
            DateTime endDate;
            DateTime projectedEndDate;
            List<Reading> readings;
            CostToDateResult result;
            FuelType fuel;
            Meter meter = null;
            BillToDateSettings btdSettings = null;

            // provide settings to calc manager; this normally populated from referrer settings
            // Change settings here to enable sewer handling
            btdSettings = new BillToDateSettings();
            btdSettings.Settings.Validate.CheckMinimumBillDays = true;
            btdSettings.Settings.Validate.CheckMaximumMissingDays = true;
            btdSettings.Settings.General.UseConversionFactorForGas = true;
            billToDateFactory = new CalculationManagerFactory();
            billToDateCalculator = billToDateFactory.CreateCalculationManager(btdSettings);

            // simulate data for a meter
            readings = GetAMI("EM60M00000001.xml");
            rateCompanyID = 101; //generic referrer (84)
            rateClass = "P1";
            fuel = FuelType.gas;
            startDate = new DateTime(2010, 1, 1);
            endDate = new DateTime(2010, 1, 25);
            projectedEndDate = new DateTime(2010, 1, 31);

            meter = new Meter("12345", rateCompanyID, rateClass, fuel, startDate, endDate, projectedEndDate);
            meter.UsageConversionFactor = 2.0;
            meter.Usages = new List<Usage>();
            meter.Usages.Add(new Usage(100));
            meter.UsagesInitiallyProvided = true; // to be sure it is set;  test
            meter.Readings = readings;

            result = billToDateCalculator.CalculateMeterCostToDate(meter);
            result = billToDateCalculator.CalculateMeterCostToDate(meter);

            Assert.IsTrue(result.ErrCode == Enums.ErrorCode.NoError);
        }

        /// <summary>
        /// To test ProcessConversionfactors and RevertConversion for Water
        /// </summary>
        [TestMethod]
        public void BillToDate_ProcessConvFactor_Water()
        {
            CalculationManagerFactory billToDateFactory = null;
            IBillToDate billToDateCalculator = null;
            int rateCompanyID;
            string rateClass = String.Empty;
            DateTime startDate;
            DateTime endDate;
            DateTime projectedEndDate;
            List<Reading> readings;
            CostToDateResult result;
            FuelType fuel;
            Meter meter = null;
            BillToDateSettings btdSettings = null;

            // provide settings to calc manager; this normally populated from referrer settings
            // Change settings here to enable sewer handling
            btdSettings = new BillToDateSettings();
            btdSettings.Settings.Validate.CheckMinimumBillDays = true;
            btdSettings.Settings.Validate.CheckMaximumMissingDays = true;
            btdSettings.Settings.General.UseConversionFactorForWater = true;
            billToDateFactory = new CalculationManagerFactory();
            billToDateCalculator = billToDateFactory.CreateCalculationManager(btdSettings);

            // simulate data for a meter
            readings = GetAMI("EM60M00000001.xml");
            rateCompanyID = 101;                        //generic referrer (84)
            rateClass = "P1";
            fuel = FuelType.water;
            startDate = new DateTime(2010, 1, 1);
            endDate = new DateTime(2010, 1, 25);
            projectedEndDate = new DateTime(2010, 1, 31);

            meter = new Meter("12345", rateCompanyID, rateClass, fuel, startDate, endDate, projectedEndDate);
            meter.UsageConversionFactor = 2.0;
            meter.Usages = new List<Usage>();
            meter.Usages.Add(new Usage(100));
            meter.UsagesInitiallyProvided = true;   // to be sure it is set;  test
            meter.Readings = readings;

            result = billToDateCalculator.CalculateMeterCostToDate(meter);

            Assert.IsTrue(result.ErrCode == Enums.ErrorCode.NoError);
        }

        /// <summary>
        /// To test validation of minimum bill days for water
        /// </summary>
        [TestMethod]
        public void BillToDate_ValidateMinimumBillDays_water()
        {
            CalculationManagerFactory billToDateFactory = null;
            IBillToDate billToDateCalculator = null;
            int rateCompanyID;
            string rateClass = String.Empty;
            DateTime startDate;
            DateTime endDate;
            DateTime projectedEndDate;
            List<Reading> readings;
            CostToDateResult result;
            FuelType fuel;
            Meter meter = null;
            BillToDateSettings btdSettings = null;

            // provide settings to calc manager; this normally populated from referrer settings
            // Change settings here to enable sewer handling
            btdSettings = new BillToDateSettings();
            btdSettings.Settings.Validate.CheckMinimumBillDays = true;
            btdSettings.Settings.Validate.CheckMaximumMissingDays = true;
            billToDateFactory = new CalculationManagerFactory();
            billToDateCalculator = billToDateFactory.CreateCalculationManager(btdSettings);
            // simulate data for a meter
            readings = GetAMI("EM60M00000001.xml");
            rateCompanyID = 101;                        //generic referrer (84)
            rateClass = "P1";
            fuel = FuelType.water;
            startDate = new DateTime(2010, 1, 1);
            endDate = new DateTime(2010, 1, 25);
            projectedEndDate = new DateTime(2010, 1, 31);

            meter = new Meter("12345", rateCompanyID, rateClass, fuel, startDate, endDate, projectedEndDate);
            meter.Readings = readings;

            result = billToDateCalculator.CalculateMeterCostToDate(meter);

            Assert.IsTrue(result.ErrCode == Enums.ErrorCode.NoError);
        }

        /// <summary>
        /// To test validation of nonmetered services when empty
        /// </summary>
        [TestMethod]
        public void BillToDate_ValidateEmptyNonmeteredServices()
        {
            CalculationManagerFactory billToDateFactory = null;
            IBillToDate billToDateCalculator = null;
            DateTime startDate;
            DateTime endDate;
            DateTime projectedEndDate;
            NonMeteredService nonMeteredService = null;
            NonMeteredServiceResult result = new NonMeteredServiceResult();
            double amount;

            billToDateFactory = new CalculationManagerFactory();
            billToDateCalculator = billToDateFactory.CreateCalculationManager();

            startDate = new DateTime();
            endDate = new DateTime();
            projectedEndDate = new DateTime();
            amount = 20.75;

            nonMeteredService = new NonMeteredService("", amount, startDate, endDate, projectedEndDate);
            result.StartDate = startDate;
            result.EndDate = endDate;
            result.ProjectedEndDate = projectedEndDate;
            result.MonthProrateFactorUsed = 0;
            result.AverageDailyCost = 0;
            result = billToDateCalculator.CalculateNonMeteredServiceCostToDate(nonMeteredService);

            Assert.IsTrue(result.StatusList.HasNoErrors() == false);
        }
        /// <summary>
        /// To test individual component that does a meter cost to date for sewer only
        /// (test for first alerts conversion)
        /// </summary>
        [TestMethod]
        public void BillToDate_CalculateMeterCostToDate_SewerOnly()
        {
            CalculationManagerFactory billToDateFactory = null;
            IBillToDate billToDateCalculator = null;
            int rateCompanyID;
            string rateClass = String.Empty;
            DateTime startDate;
            DateTime endDate;
            DateTime projectedEndDate;
            List<Reading> readings;
            CostToDateResult result;
            FuelType fuel;
            Meter meter = null;

            billToDateFactory = new CalculationManagerFactory();
            billToDateCalculator = billToDateFactory.CreateCalculationManager();

            // simulate data for a meter
            readings = GetAMI("EM60M00000001.xml");
            rateCompanyID = 101;                        //generic referrer (84)
            rateClass = "P1W";
            fuel = FuelType.water;
            startDate = new DateTime(2010, 1, 1);
            endDate = new DateTime(2010, 1, 25);
            projectedEndDate = new DateTime(2010, 1, 31);

            meter = new Meter("12345", rateCompanyID, rateClass, fuel, startDate, endDate, projectedEndDate);
            meter.Readings = readings;
            meter.DerivedFuel = DerivedFuelType.sewer;  // for sewer calc
            meter.RateClass2 = "T1W";                    // for sewer calc
            meter.MaximumDerivedUsage = 50;             // for sewer calc (from past bill information)
            meter.DerivedFuelIsPrimary = true;

            result = billToDateCalculator.CalculateMeterCostToDate(meter);

            Assert.IsTrue(result.ErrCode == Enums.ErrorCode.NoError);
        }

        /// <summary>
        /// To negative test empty meter details
        /// </summary>
        [TestMethod]
        public void BillToDate_CalculateMeterCostToDate_EmptyMeter()
        {
            CalculationManagerFactory billToDateFactory = null;
            IBillToDate billToDateCalculator = null;
           CostToDateResult result;
            Meter meter = null;
            meter = new Meter();
            billToDateFactory = new CalculationManagerFactory();
            billToDateCalculator = billToDateFactory.CreateCalculationManager();

            result = billToDateCalculator.CalculateMeterCostToDate(meter);

            Assert.IsTrue(result.ErrCode == Enums.ErrorCode.ErrGeneric);
        }


        /// <summary>
        /// To test individual component that does a non-meter cost to date.  Like trash.
        /// </summary>
        [TestMethod]
        //[ExpectedException(typeof(System.NullReferenceException))]
        public void BillToDate_DefaultConstructor_CalculateNonMeterCostToDate()
        {
            CalculationManagerFactory billToDateFactory = null;
            IBillToDate billToDateCalculator = null;
            DateTime startDate;
            DateTime endDate;
            DateTime projectedEndDate;
            NonMeteredService nonMeteredService = null;
            NonMeteredServiceResult result = new NonMeteredServiceResult();
            double amount;

            billToDateFactory = new CalculationManagerFactory();
            billToDateCalculator = billToDateFactory.CreateCalculationManager();

            startDate = new DateTime(2010, 1, 1);
            endDate = new DateTime(2010, 1, 25);
            projectedEndDate = new DateTime(2010, 1, 31);
            amount = 20.75;

            nonMeteredService = new NonMeteredService("refuse", amount, startDate, endDate, projectedEndDate);
            result.StartDate = startDate;
            result.EndDate = endDate;
            result.ProjectedEndDate = projectedEndDate;
            result.MonthProrateFactorUsed = 0;
            result.AverageDailyCost = 0;
            result = billToDateCalculator.CalculateNonMeteredServiceCostToDate(nonMeteredService);

            Assert.IsTrue(result.StatusList.HasNoErrors() == true);
           
        }

        /// <summary>
        /// To test bill-to-date with one meter and one non-meter charge.  Simple test.
        /// </summary>
        [TestMethod]
        public void BillToDate_DefaultConstructor_CalculateBTD()
        {
            CalculationManagerFactory billToDateFactory = null;
            IBillToDate billToDateCalculator = null;
            Bill bill = null;
            NonMeteredService nms = null;
            MeteredService meteredService = null;
            string rateClass = String.Empty;
            DateTime startDate;
            DateTime endDate;
            DateTime projectedEndDate;
            List<Reading> readings;
            Meter meter = null;
            int referrerID = 84;
            int rateCompanyID = 101;
            string customerID = "c12345";   // for reference and use in output
            string accountID = "a12345";    // for reference and use in output
            string serviceID;
            string premiseID;
            string meterID;
            FuelType fuel;
            BillToDateResult result;

            billToDateFactory = new CalculationManagerFactory();
            billToDateCalculator = billToDateFactory.CreateCalculationManager();

            startDate = new DateTime(2010, 1, 1);
            endDate = new DateTime(2010, 1, 25);
            projectedEndDate = new DateTime(2010, 1, 31);

            // create a bill-to-date bill
            bill = new Bill(startDate, referrerID, customerID, accountID, rateCompanyID);

            // create two non-metered services with basic charges
            nms = new NonMeteredService("refuse", 75.00, startDate, endDate, projectedEndDate);
            bill.NonMeteredServices.Add(nms);
            nms = new NonMeteredService("fire", 20.00, startDate, endDate, projectedEndDate);
            bill.NonMeteredServices.Add(nms);

            // create a metered service
            serviceID = "s12345";
            premiseID = "p12345";
            fuel = FuelType.electric;
            meteredService = new MeteredService(serviceID, premiseID, fuel);

            // create and simulate data for a meter, add to metered service
            meterID = "m12345";
            readings = GetAMI("EM60M00000001.xml");
            rateClass = "P1";
            meter = new Meter(meterID, rateCompanyID, rateClass, fuel, startDate, endDate, projectedEndDate, readings);
            meteredService.Meters.Add(meter);
            bill.MeteredServices.Add(meteredService);

            // make main call
            result = billToDateCalculator.CalculateBillToDate(bill);

            Assert.IsTrue(result.StatusList.HasNoErrors() == true);
        }

        /// <summary>
        /// To test bill-to-date Trace method
        /// </summary>
        [TestMethod]
        public void BillToDate_TraceTest()
        {
            CalculationManagerFactory billToDateFactory = null;
            IBillToDate billToDateCalculator = null;
            Bill bill = null;
            NonMeteredService nms = null;
            MeteredService meteredService = null;
            string rateClass = String.Empty;
            DateTime startDate;
            DateTime endDate;
            DateTime projectedEndDate;
            List<Reading> readings;
            Meter meter = null;
            int referrerID = 84;
            int rateCompanyID = 101;
            string customerID = "c12345";   // for reference and use in output
            string accountID = "a12345";    // for reference and use in output
            string serviceID;
            string premiseID;
            string meterID;
            FuelType fuel;
            BillToDateResult result;
            BillToDateSettings btd=new BillToDateSettings();
            billToDateFactory = new CalculationManagerFactory();
            billToDateCalculator = billToDateFactory.CreateCalculationManager(btd,true);

            startDate = new DateTime(2010, 1, 1);
            endDate = new DateTime(2010, 1, 25);
            projectedEndDate = new DateTime(2010, 1, 31);

            // create a bill-to-date bill
            bill = new Bill(startDate, referrerID, customerID, accountID, rateCompanyID);

            // create two non-metered services with basic charges
            nms = new NonMeteredService("refuse", 75.00, startDate, endDate, projectedEndDate);
            //nms.CostToDateResult.StatusList.Add(new Status());
            bill.NonMeteredServices.Add(nms);
            nms = new NonMeteredService("fire", 20.00, startDate, endDate, projectedEndDate);
            bill.NonMeteredServices.Add(nms);

            // create a metered service
            serviceID = "s12345";
            premiseID = "p12345";
            fuel = FuelType.electric;
            meteredService = new MeteredService(serviceID, premiseID, fuel);

            // create and simulate data for a meter, add to metered service
            meterID = "m12345";
            readings = GetAMI("EM60M00000001.xml");
            rateClass = "P1";
            meter = new Meter(meterID, rateCompanyID, rateClass, fuel, startDate, endDate, projectedEndDate, readings);
            
            CostToDateResult ctdresult = null;
            PTRResult ptrResult = null;
            ptrResult = new PTRResult();
            
            ptrResult.PeakTimeRebateEvents = new List<PTREvent>();
            var eventRebate = new PTREvent();
            eventRebate.StartDate = DateTime.Now;
            eventRebate.EndDate = DateTime.Now;
            ptrResult.PeakTimeRebateEvents.Add(eventRebate);
            ctdresult=new CostToDateResult();
            ctdresult.PeakTimeRebates = ptrResult;
            meter.CostToDateResult = ctdresult;
            meter.CostToDateResult.DailyDemandCalcExcluded = true;
            meter.CostToDateResult.ProjectedPeakTimeRebates = ptrResult;
            meter.CostToDateResult.ProjectedPeakTimeRebates.OverallAccountingRule = Enums.RebateAccountingRule.EntireBillPeriod;

            meteredService.Meters.Add(meter);
            bill.MeteredServices.Add(meteredService);

            // make main call
            result = billToDateCalculator.CalculateBillToDate(bill);

            Assert.IsTrue(result.StatusList.HasNoErrors() == true);
        }

        /// <summary>
        /// To test bill-to-date with DerivedMeterProcesing
        /// </summary>
        [TestMethod]
        public void BillToDate_DerivedMeterProcessingTest()
        {
            CalculationManagerFactory billToDateFactory = null;
            IBillToDate billToDateCalculator = null;
            Bill bill = null;
            NonMeteredService nms = null;
            MeteredService meteredService = null;
            string rateClass = String.Empty;
            DateTime startDate;
            DateTime endDate;
            DateTime projectedEndDate;
            List<Reading> readings;
            Meter meter = null;
            int referrerID = 84;
            int rateCompanyID = 100;
            string customerID = "c12345";   // for reference and use in output
            string accountID = "a12345";    // for reference and use in output
            string serviceID;
            string premiseID;
            string meterID;
            FuelType fuel;
            BillToDateResult result;
            var btd=new BillToDateSettings();
            btd.Settings.Sewer.UseSewerMaximumMonthsUsageForCommercial = true;
            btd.Settings.Sewer.UseSewerMaximumMonthsUsageForResidential = true;
            btd.Settings.Sewer.ProrateSewerMaximumMonthsUsageDeterminants = true;
            billToDateFactory = new CalculationManagerFactory();
            billToDateCalculator = billToDateFactory.CreateCalculationManager(btd,true);

            startDate = new DateTime(2010, 1, 1);
            endDate = new DateTime(2010, 1, 25);
            projectedEndDate = new DateTime(2010, 1, 31);

            // create a bill-to-date bill
            bill = new Bill(startDate, referrerID, customerID, accountID, rateCompanyID);

            // create two non-metered services with basic charges
            nms = new NonMeteredService("refuse", 75.00, startDate, endDate, projectedEndDate);
            bill.NonMeteredServices.Add(nms);
            nms = new NonMeteredService("fire", 20.00, startDate, endDate, projectedEndDate);
            bill.NonMeteredServices.Add(nms);

            // create a metered service
            serviceID = "s12345";
            premiseID = "p12345";
            fuel = FuelType.water;
            meteredService = new MeteredService(serviceID, premiseID, fuel);

            // create and simulate data for a meter, add to metered service
            meterID = "m12345";
            readings = GetAMI("EM60M00000001.xml");
            rateClass = "P1W";
            meter = new Meter(meterID, rateCompanyID, rateClass, fuel, startDate, endDate, projectedEndDate, readings);
            meter.DerivedFuel = DerivedFuelType.sewer;
            meter.RateClass2 = "binTOU";
            meter.MaximumDerivedUsage = 50;
            meteredService.Meters.Add(meter);
            bill.MeteredServices.Add(meteredService);

            // make main call
            result = billToDateCalculator.CalculateBillToDate(bill);

            //Assert.IsTrue(result.StatusList.HasNoErrors());
        }


        /// <summary>
        /// To test bill-to-date with one meter and one non-meter charge, and sewer test (derived meter.)
        /// </summary>
        [TestMethod]
        public void BillToDate_CalculateBTD_WithSewer()
        {
            CalculationManagerFactory billToDateFactory = null;
            IBillToDate billToDateCalculator = null;
            Bill bill = null;
            NonMeteredService nms = null;
            MeteredService meteredService = null;
            string rateClass = String.Empty;
            DateTime startDate;
            DateTime endDate;
            DateTime projectedEndDate;
            List<Reading> readings;
            Meter meter = null;
            int referrerID = 84;
            int rateCompanyID = 101;
            string customerID = "c12345";   // for reference and use in output
            string accountID = "a12345";    // for reference and use in output
            string serviceID;
            string premiseID;
            string meterID;
            FuelType fuel;
            BillToDateResult result;
            BillToDateSettings btdSettings = null;

            // provide settings to calc manager; this normally populated from referrer settings
            // Change settings here to enable sewer handling
            btdSettings = new BillToDateSettings();
            btdSettings.Settings.Sewer.UseSewerMaximumMonthsUsageForResidential = true;
            btdSettings.Settings.Sewer.UseSewerMaximumMonthsUsageForCommercial = true;
            btdSettings.Settings.Sewer.ProrateSewerMaximumMonthsUsageDeterminants = true;

            billToDateFactory = new CalculationManagerFactory();
            billToDateCalculator = billToDateFactory.CreateCalculationManager(btdSettings,true);

            startDate = new DateTime(2010, 1, 1);
            endDate = new DateTime(2010, 1, 25);
            projectedEndDate = new DateTime(2010, 1, 31);

            // create a bill-to-date bill
            bill = new Bill(startDate, referrerID, customerID, accountID, rateCompanyID);

            // create two non-metered services with basic charges
            nms = new NonMeteredService("refuse", 75.00, startDate, endDate, projectedEndDate);
            bill.NonMeteredServices.Add(nms);
            nms = new NonMeteredService("fire", 20.00, startDate, endDate, projectedEndDate);
            bill.NonMeteredServices.Add(nms);

            // create a metered service
            serviceID = "s12345";
            premiseID = "p12345";
            fuel = FuelType.water;
            meteredService = new MeteredService(serviceID, premiseID, fuel);

            // create and simulate data for a meter, add to metered service
            meterID = "m12345";
            readings = GetAMI("EM60M00000001.xml");
            rateClass = "P1W";
            meter = new Meter(meterID, rateCompanyID, rateClass, fuel, startDate, endDate, projectedEndDate, readings);
            meter.DerivedFuel = DerivedFuelType.sewer;  // for sewer calc
            meter.RateClass2 = "T1W";                    // for sewer calc
            meter.MaximumDerivedUsage = 50;             // for sewer calc
            meteredService.Meters.Add(meter);
            bill.MeteredServices.Add(meteredService);

            // make main call
            result = billToDateCalculator.CalculateBillToDate(bill);

            Assert.IsTrue(!result.StatusList.HasNoErrors());
        }


        /// <summary>
        /// Test errors; test for minimum usage met
        /// </summary>
        [TestMethod]
        public void BillToDate_CalculateBTD_A()
        {
            CalculationManagerFactory billToDateFactory = null;
            IBillToDate billToDateCalculator = null;
            Bill bill = null;
            NonMeteredService nms = null;
            MeteredService meteredService = null;
            string rateClass = String.Empty;
            DateTime startDate;
            DateTime endDate;
            DateTime projectedEndDate;
            List<Reading> readings;
            Meter meter = null;
            int referrerID = 84;
            int rateCompanyID = 101;
            string customerID = "c12345";   // for reference and use in output
            string accountID = "a12345";    // for reference and use in output
            string serviceID;
            string premiseID;
            string meterID;
            FuelType fuel;
            BillToDateResult result;
            BillToDateSettings btdSettings = null;
           
            btdSettings = new BillToDateSettings();
            btdSettings.Settings.General.ProrateMonthlyServiceCharges = true;
            btdSettings.Settings.Validate.CheckMinimumUsage = true;
            btdSettings.Settings.Validate.MinimumUsage = 0;     //force errors

            billToDateFactory = new CalculationManagerFactory();
            billToDateCalculator = billToDateFactory.CreateCalculationManager(btdSettings,true);

            startDate = new DateTime(2010, 1, 1);
            endDate = new DateTime(2010, 1, 25);
            projectedEndDate = new DateTime(2010, 1, 31);

            // create a bill-to-date bill
            bill = new Bill(startDate, referrerID, customerID, accountID, rateCompanyID);

            // create two non-metered services with basic charges
            nms = new NonMeteredService("refuse", 75.00, startDate, endDate, projectedEndDate);
            bill.NonMeteredServices.Add(nms);
            nms = new NonMeteredService("fire", 20.00, startDate, endDate, projectedEndDate);
            bill.NonMeteredServices.Add(nms);

            // create a metered service 1
            serviceID = "s1";
            premiseID = "p1";
            fuel = FuelType.electric;
            meteredService = new MeteredService(serviceID, premiseID, fuel);

            // create and simulate data for a meter, add to metered service
            meterID = "m1";
            readings = GetAMI("EM60M00000001.xml");
            rateClass = "P1";
            meter = new Meter(meterID, rateCompanyID, rateClass, fuel, startDate, endDate, projectedEndDate, readings);
            meteredService.Meters.Add(meter);
            bill.MeteredServices.Add(meteredService);

            // create a metered service 2
            serviceID = "s2";
            premiseID = "p1";
            fuel = FuelType.electric;
            meteredService = new MeteredService(serviceID, premiseID, fuel);

            // create and simulate data for a meter, add to metered service
            meterID = "m2";
            readings = GetAMI("EM60M00000001.xml");
            rateClass = "P1";
            meter = new Meter(meterID, rateCompanyID, rateClass, fuel, startDate, endDate, projectedEndDate, readings);
            meter.Usages = new List<Usage>();
            meter.Usages.Add(new Usage(100));
            meter.UsagesInitiallyProvided = true;   // to be sure it is set;  test

            meteredService.Meters.Add(meter);
            bill.MeteredServices.Add(meteredService);


            // make main call
            result = billToDateCalculator.CalculateBillToDate(bill);

            
            Assert.IsTrue(result.StatusList.HasNoErrors());
        }

        
        [TestMethod]
        public void BillToDate_CalculateBTD_TP2()
        {
            CalculationManagerFactory billToDateFactory = null;
            IBillToDate billToDateCalculator = null;
            Bill bill = null;
             MeteredService meteredService = null;
            string rateClass = String.Empty;
            DateTime startDate;
            DateTime endDate;
            DateTime projectedEndDate;
            List<Reading> readings;
            Meter meter = null;
            int referrerID = 84;
            int rateCompanyID = 101;
            string customerID = "c12345";   // for reference and use in output
            string accountID = "a12345";    // for reference and use in output
            string serviceID;
            string premiseID;
            string meterID;
            FuelType fuel;
            BillToDateResult result;
           
            billToDateFactory = new CalculationManagerFactory();
            billToDateCalculator = billToDateFactory.CreateCalculationManager();

            startDate = new DateTime(2010, 1, 1);
            endDate = new DateTime(2010, 1, 25);
            projectedEndDate = new DateTime(2010, 1, 31);

            // create a bill-to-date bill
            bill = new Bill(startDate, referrerID, customerID, accountID, rateCompanyID);

            // create a metered service
            serviceID = "s12345";
            premiseID = "p12345";
            fuel = FuelType.electric;
            meteredService = new MeteredService(serviceID, premiseID, fuel);

            // create and simulate data for a meter, add to metered service
            meterID = "m12345";
            readings = GetAMI("EM60M00000004.xml");
            rateClass = "TP2";
            meter = new Meter(meterID, rateCompanyID, rateClass, fuel, startDate, endDate, projectedEndDate, readings);
            meteredService.Meters.Add(meter);
            bill.MeteredServices.Add(meteredService);

            // make main call
            result = billToDateCalculator.CalculateBillToDate(bill);
         
            Assert.IsTrue(result.StatusList.HasNoErrors() == true);
            var status = new Status();
            foreach (Status item in result.StatusList)
            {
                status = item;
                Assert.IsFalse(status.StatusSeverity.Equals(null));
                Assert.IsFalse(status.Exception.Equals(null));
            }
        }

        [TestMethod]
        public void BillToDate_CalculateBTD_ComplexB_TP4()
        {
            CalculationManagerFactory billToDateFactory = null;
            IBillToDate billToDateCalculator = null;
            Bill bill = null;
            MeteredService meteredService = null;
            string rateClass = String.Empty;
            DateTime startDate;
            DateTime endDate;
            DateTime projectedEndDate;
            List<Reading> readings;
            Meter meter = null;
            int referrerID = 84;
            int rateCompanyID = 101;
            string customerID = "c12345";   // for reference and use in output
            string accountID = "a12345";    // for reference and use in output
            string serviceID;
            string premiseID;
            string meterID;
            FuelType fuel;
            BillToDateResult result;

            billToDateFactory = new CalculationManagerFactory();
            billToDateCalculator = billToDateFactory.CreateCalculationManager();

            startDate = new DateTime(2010, 1, 1);
            endDate = new DateTime(2010, 1, 25);
            projectedEndDate = new DateTime(2010, 1, 31);

            // create a bill-to-date bill
            bill = new Bill(startDate, referrerID, customerID, accountID, rateCompanyID);

            // create a metered service
            serviceID = "s12345";
            premiseID = "p12345";
            fuel = FuelType.electric;
            meteredService = new MeteredService(serviceID, premiseID, fuel);

            // create and simulate data for a meter, add to metered service
            meterID = "m12345";
            readings = GetAMI("EM60M00000004.xml");
            rateClass = "TP4";
            meter = new Meter(meterID, rateCompanyID, rateClass, fuel, startDate, endDate, projectedEndDate, readings);
            meteredService.Meters.Add(meter);
            bill.MeteredServices.Add(meteredService);

            // make main call
            result = billToDateCalculator.CalculateBillToDate(bill);

            Assert.IsTrue(result.StatusList.HasNoErrors() == true);
        }

        /// <summary>
        /// Complex rate class for binning (tiered seasonal tou)
        /// </summary>
        [TestMethod]
        public void BillToDate_CalculateBTD_ComplexA_TP4b()
        {
            CalculationManagerFactory billToDateFactory = null;
            IBillToDate billToDateCalculator = null;
            Bill bill = null;
            MeteredService meteredService = null;
            string rateClass = String.Empty;
            DateTime startDate;
            DateTime endDate;
            DateTime projectedEndDate;
            List<Reading> readings;
            Meter meter = null;
            int referrerID = 84;
            int rateCompanyID = 101;
            string customerID = "c12345";   // for reference and use in output
            string accountID = "a12345";    // for reference and use in output
            string serviceID;
            string premiseID;
            string meterID;
            FuelType fuel;
            BillToDateResult result;

            billToDateFactory = new CalculationManagerFactory();
            billToDateCalculator = billToDateFactory.CreateCalculationManager();

            startDate = new DateTime(2010, 4, 1);
            endDate = new DateTime(2010, 4, 10);
            projectedEndDate = new DateTime(2010, 4, 30);

            // create a bill-to-date bill
            bill = new Bill(startDate, referrerID, customerID, accountID, rateCompanyID);

            // create a metered service
            serviceID = "s12345";
            premiseID = "p12345";
            fuel = FuelType.electric;
            meteredService = new MeteredService(serviceID, premiseID, fuel);

            // create and simulate data for a meter, add to metered service
            meterID = "m12345";
            readings = GetAMI("EM60M00000004b.xml");
            rateClass = "TP4";
            meter = new Meter(meterID, rateCompanyID, rateClass, fuel, startDate, endDate, projectedEndDate, readings);
            meteredService.Meters.Add(meter);
            bill.MeteredServices.Add(meteredService);

            // make main call
            result = billToDateCalculator.CalculateBillToDate(bill);

            Assert.IsTrue(result.StatusList.HasNoErrors() == true);
        }

        [TestMethod]
        public void BillToDate_CalculateBTD_ComplexB_TP3()
        {
            CalculationManagerFactory billToDateFactory = null;
            IBillToDate billToDateCalculator = null;
            Bill bill = null;
            MeteredService meteredService = null;
            string rateClass = String.Empty;
            DateTime startDate;
            DateTime endDate;
            DateTime projectedEndDate;
            List<Reading> readings;
            Meter meter = null;
            int referrerID = 84;
            int rateCompanyID = 101;
            string customerID = "c12345";   // for reference and use in output
            string accountID = "a12345";    // for reference and use in output
            string serviceID;
            string premiseID;
            string meterID;
            FuelType fuel;
            BillToDateResult result;

            billToDateFactory = new CalculationManagerFactory();
            billToDateCalculator = billToDateFactory.CreateCalculationManager();

            startDate = new DateTime(2010, 1, 1);
            endDate = new DateTime(2010, 1, 25);
            projectedEndDate = new DateTime(2010, 1, 31);

            // create a bill-to-date bill
            bill = new Bill(startDate, referrerID, customerID, accountID, rateCompanyID);

            // create a metered service
            serviceID = "s12345";
            premiseID = "p12345";
            fuel = FuelType.electric;
            meteredService = new MeteredService(serviceID, premiseID, fuel);

            // create and simulate data for a meter, add to metered service
            meterID = "m12345";
            readings = GetAMI("EM60M00000004.xml");
            rateClass = "TP3";
            meter = new Meter(meterID, rateCompanyID, rateClass, fuel, startDate, endDate, projectedEndDate, readings);
            meteredService.Meters.Add(meter);
            bill.MeteredServices.Add(meteredService);

            // make main call
            result = billToDateCalculator.CalculateBillToDate(bill);

            Assert.IsTrue(result.StatusList.HasNoErrors() == true);
        }

        [TestMethod]
        public void BillToDate_CalculateBTD_ComplexC_TP31()
        {
            CalculationManagerFactory billToDateFactory = null;
            IBillToDate billToDateCalculator = null;
            Bill bill = null;
            MeteredService meteredService = null;
            string rateClass = String.Empty;
            DateTime startDate;
            DateTime endDate;
            DateTime projectedEndDate;
            List<Reading> readings;
            Meter meter = null;
            int referrerID = 84;
            int rateCompanyID = 101;
            string customerID = "c12345";   // for reference and use in output
            string accountID = "a12345";    // for reference and use in output
            string serviceID;
            string premiseID;
            string meterID;
            FuelType fuel;
            BillToDateResult result;

            billToDateFactory = new CalculationManagerFactory();
            billToDateCalculator = billToDateFactory.CreateCalculationManager();

            startDate = new DateTime(2010, 1, 1);
            endDate = new DateTime(2010, 1, 25);
            projectedEndDate = new DateTime(2010, 1, 31);

            // create a bill-to-date bill
            bill = new Bill(startDate, referrerID, customerID, accountID, rateCompanyID);

            // create a metered service
            serviceID = "s12345";
            premiseID = "p12345";
            fuel = FuelType.electric;
            meteredService = new MeteredService(serviceID, premiseID, fuel);

            // create and simulate data for a meter, add to metered service
            meterID = "m12345";
            readings = GetAMI("EM60M00000004.xml");
            rateClass = "TP31";
            meter = new Meter(meterID, rateCompanyID, rateClass, fuel, startDate, endDate, projectedEndDate, readings);
            meteredService.Meters.Add(meter);
            bill.MeteredServices.Add(meteredService);

            // make main call
            result = billToDateCalculator.CalculateBillToDate(bill);

            Assert.IsTrue(result.StatusList.HasNoErrors() == true);
        }

        [TestMethod]
        public void BillToDate_CalculateBTD_ComplexC_TP311()
        {
            CalculationManagerFactory billToDateFactory = null;
            IBillToDate billToDateCalculator = null;
            Bill bill = null;
             MeteredService meteredService = null;
            string rateClass = String.Empty;
            DateTime startDate;
            DateTime endDate;
            DateTime projectedEndDate;
            List<Reading> readings;
            Meter meter = null;
            int referrerID = 84;
            int rateCompanyID = 101;
            string customerID = "c12345";   // for reference and use in output
            string accountID = "a12345";    // for reference and use in output
            string serviceID;
            string premiseID;
            string meterID;
            FuelType fuel;
            BillToDateResult result;

            billToDateFactory = new CalculationManagerFactory();
            billToDateCalculator = billToDateFactory.CreateCalculationManager();

            startDate = new DateTime(2010, 1, 1);
            endDate = new DateTime(2010, 1, 25);
            projectedEndDate = new DateTime(2010, 1, 31);

            // create a bill-to-date bill
            bill = new Bill(startDate, referrerID, customerID, accountID, rateCompanyID);

            // create a metered service
            serviceID = "s12345";
            premiseID = "p12345";
            fuel = FuelType.electric;
            meteredService = new MeteredService(serviceID, premiseID, fuel);

            // create and simulate data for a meter, add to metered service
            meterID = "m12345";
            readings = GetAMI("EM60M00000004.xml");
            rateClass = "TP311";
            meter = new Meter(meterID, rateCompanyID, rateClass, fuel, startDate, endDate, projectedEndDate, readings);
            meteredService.Meters.Add(meter);
            bill.MeteredServices.Add(meteredService);

            // make main call
            result = billToDateCalculator.CalculateBillToDate(bill);

            Assert.IsTrue(result.StatusList.HasNoErrors() == true);
        }

        [TestMethod]
        public void BillToDate_CalculateBTD_ComplexA_P2()
        {
            CalculationManagerFactory billToDateFactory = null;
            IBillToDate billToDateCalculator = null;
            Bill bill = null;
            MeteredService meteredService = null;
            string rateClass = String.Empty;
            DateTime startDate;
            DateTime endDate;
            DateTime projectedEndDate;
            List<Reading> readings;
            Meter meter = null;
            int referrerID = 84;
            int rateCompanyID = 101;
            string customerID = "c12345";   // for reference and use in output
            string accountID = "a12345";    // for reference and use in output
            string serviceID;
            string premiseID;
            string meterID;
            FuelType fuel;
            BillToDateResult result;

            billToDateFactory = new CalculationManagerFactory();
            billToDateCalculator = billToDateFactory.CreateCalculationManager();

            startDate = new DateTime(2010, 4, 1);
            endDate = new DateTime(2010, 4, 10);
            projectedEndDate = new DateTime(2010, 4, 30);

            // create a bill-to-date bill
            bill = new Bill(startDate, referrerID, customerID, accountID, rateCompanyID);

            // create a metered service
            serviceID = "s12345";
            premiseID = "p12345";
            fuel = FuelType.electric;
            meteredService = new MeteredService(serviceID, premiseID, fuel);

            // create and simulate data for a meter, add to metered service
            meterID = "m12345";
            readings = GetAMI("EM60M00000004b.xml");
            rateClass = "P2";
            meter = new Meter(meterID, rateCompanyID, rateClass, fuel, startDate, endDate, projectedEndDate, readings);
            meteredService.Meters.Add(meter);
            bill.MeteredServices.Add(meteredService);

            // make main call
            result = billToDateCalculator.CalculateBillToDate(bill);

            Assert.IsTrue(result.StatusList.HasNoErrors() == true);
        }

        #region Support Functions

        List<Reading> GetAMI(string filename)
        {
            XmlDocument amrReadingXmlDocument = new XmlDocument();
            XmlNodeList amrXmlNodeList;
            List<Reading> readings;
            Reading r;
            Enums.BaseOrTier eBaseOrTier;
            Enums.TimeOfUse eTimeOfUse;
            Enums.Season eSeason;
            DateTime timestamp;
            Double quantity;
            Boolean isEmpty = false;

            readings = new List<Reading>();

            amrReadingXmlDocument.Load("C:\\acex\\ACEx\\16.09\\EntLib\\CE.BillToDate\\UnitTests\\TestData\\" + filename);
            amrXmlNodeList = amrReadingXmlDocument.SelectNodes("//Reading");

            // The baseOrTier is determined when setting the TimeOfUse below;
            eBaseOrTier = Enums.BaseOrTier.Undefined;

            foreach (XmlNode node in amrXmlNodeList)
            {

                // map the TimeOfUse 
                switch (node.Attributes["TOUBin"].InnerText)
                {
                    case "NoTOU":
                        eTimeOfUse = Enums.TimeOfUse.Undefined;
                        eBaseOrTier = Enums.BaseOrTier.TotalServiceUse;
                        break;
                    case "OnPeak":
                        eTimeOfUse = Enums.TimeOfUse.OnPeak;
                        break;
                    case "OffPeak":
                        eTimeOfUse = Enums.TimeOfUse.OffPeak;
                        break;
                    case "Shoulder1":
                        eTimeOfUse = Enums.TimeOfUse.Shoulder1;
                        break;
                    case "Shoulder2":
                        eTimeOfUse = Enums.TimeOfUse.Shoulder2;
                        break;
                    case "CriticalPeak":
                        eTimeOfUse = Enums.TimeOfUse.CriticalPeak;
                        break;
                    default:
                        eTimeOfUse = Enums.TimeOfUse.Undefined;
                        eBaseOrTier = Enums.BaseOrTier.TotalServiceUse;
                        break;
                }

                eSeason = Enums.Season.Undefined;
                timestamp = System.Convert.ToDateTime(node.Attributes["IntEnd"].InnerText);
                quantity = System.Convert.ToDouble(node.Attributes["Read"].InnerText);

                r = new Reading(eBaseOrTier, eTimeOfUse, eSeason, timestamp, quantity, isEmpty);
                readings.Add(r);

            }

            return (readings);
        }

        /// <summary>
        /// Baselines helper.
        /// </summary>
        /// <param name="filename"></param>
        /// <returns></returns>
        List<Baseline> GetBaselines(string filename)
        {
            var baselines = new List<Baseline>();

            baselines.Add(new Baseline(DateTime.Parse("4/1/2012"), 30.0, 24.0, 1.25, false, false, false));
            baselines.Add(new Baseline(DateTime.Parse("4/2/2012"), 31.0, 24.0, 1.25, false, false, false));
            baselines.Add(new Baseline(DateTime.Parse("4/3/2012"), 29.0, 24.0, 1.25, false, false, false));
            baselines.Add(new Baseline(DateTime.Parse("4/4/2012"), 33.0, 24.0, 1.25, false, false, false));
            baselines.Add(new Baseline(DateTime.Parse("4/5/2012"), 20.0, 24.0, 1.25, false, false, false));
            baselines.Add(new Baseline(DateTime.Parse("4/6/2012"), 20.0, 24.0, 1.25, false, false, false));
            baselines.Add(new Baseline(DateTime.Parse("4/7/2012"), 30.0, 24.0, 1.25, false, false, false));
            baselines.Add(new Baseline(DateTime.Parse("4/8/2012"), 30.5, 24.0, 1.25, false, false, false));
            baselines.Add(new Baseline(DateTime.Parse("4/9/2012"), 30.25, 24.0, 1.25, false, false, false));
            baselines.Add(new Baseline(DateTime.Parse("4/10/2012"), 31.0, 24.0, 1.25, false, false, false));
            baselines.Add(new Baseline(DateTime.Parse("4/11/2012"), 30.0, 24.0, 1.25, false, false, false));
            baselines.Add(new Baseline(DateTime.Parse("4/12/2012"), 30.0, 24.0, 1.25, false, false, false));
            baselines.Add(new Baseline(DateTime.Parse("4/13/2012"), 30.0, 24.0, 1.25, false, false, false));
            baselines.Add(new Baseline(DateTime.Parse("4/14/2012"), 30.0, 24.0, 4.00, false, false, false));
            return (baselines);
        }

        #endregion

    }
}
