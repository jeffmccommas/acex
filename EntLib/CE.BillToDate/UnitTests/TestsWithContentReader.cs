﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using CE.RateModel;
using CE.BillToDate;
using System.Xml;
using CE.ContentModel;

namespace UnitTests
{

    public partial class BillToDateTests
    {
        /// <summary>
        /// New Test to Illustrate use of 87 with 15-minute data and inject settings
        /// </summary>
        [TestMethod]
        public void AAA_BillToDate_Client87_RateCompanyId100_NewTest()
        {
            CalculationManagerFactory billToDateFactory = null;
            IBillToDate billToDateCalculator = null;
            Bill bill = null;
            NonMeteredService nms = null;
            MeteredService meteredService = null;
            string rateClass = String.Empty;
            DateTime startDate;
            DateTime endDate;
            DateTime projectedEndDate;
            List<Reading> readings;
            Meter meter = null;
            int clientid = 87;      //same as referrerid
            int rateCompanyID = 0;
            string customerID = "customer1";   // for reference and use in output
            string accountID = "account1";    // for reference and use in output
            string serviceID;
            string premiseID;
            string meterID;
            FuelType fuel;
            BillToDateResult result;

            // READ BILL-TO-DATE SETTINGS
            var reader = new CE.BillToDate.Configuration.SettingsReader(clientid);
            var btdSettings = reader.GetSettings();
            rateCompanyID = btdSettings.RateCompanyId;
            
            //CREATE CALCULATION MANAGER - INJECT THE SETTINGS THAT WERE READ FROM CONTENT FOR THE CLIENTID
            billToDateFactory = new CalculationManagerFactory();
            billToDateCalculator = billToDateFactory.CreateCalculationManager(btdSettings); //inject settings that we read from content

            startDate = new DateTime(2012, 4, 10);      //the sample ami below is from 4/10 to 5/2
            endDate = new DateTime(2012, 5, 2);
            projectedEndDate = new DateTime(2012, 5, 9);

            // create a bill-to-date bill
            bill = new Bill(startDate, clientid, customerID, accountID, rateCompanyID);

            // create two non-metered services with basic charges
            nms = new NonMeteredService("refuse", 25.00, startDate, endDate, projectedEndDate);
            bill.NonMeteredServices.Add(nms);
            nms = new NonMeteredService("fire", 10.00, startDate, endDate, projectedEndDate);
            bill.NonMeteredServices.Add(nms);
            nms = new NonMeteredService("fireprotection", 10.00, startDate, endDate, 20);
            bill.NonMeteredServices.Add(nms);
            // create a metered service
            serviceID = "svc1";
            premiseID = "prem1";
            fuel = FuelType.electric;
            meteredService = new MeteredService(serviceID, premiseID, fuel);

            // create and simulate data for a meter, add to metered service
            meterID = "EM15M00FIX";
            readings = GetAMI("EM15M00FIX.xml");
            rateClass = "binBasic"; 
            meter = new Meter(meterID, rateCompanyID, rateClass, fuel, startDate, endDate, projectedEndDate, readings);
            meteredService.Meters.Add(meter);
            bill.MeteredServices.Add(meteredService);

            // MAKE MAIN CALL HERE
            result = billToDateCalculator.CalculateBillToDate(bill);

            Assert.IsTrue(result.StatusList.HasNoErrors() == true);
        }

        /// <summary>
        /// Testing bulk retreival here for simplicity.
        /// </summary>
        [TestMethod]
        public void GetContentConfigurationBulkTest()
        {
            var factory = new ContentModelFactoryContentful(); 
            var provider = factory.CreateContentProvider(87);
            var result1 = provider.GetContentConfigurationBulk("system", "aclaraone.utilitysetting");

        }
    }
}
