﻿using CE.RateModel;
using System;
using System.Collections.Generic;
using System.Linq;

namespace CE.BillToDate
{
    /// <summary>
    /// Meter class for BillToDate API.  Note that the date fields are nullable types.
    /// </summary>
    public class Meter
    {
        private string _meterID;
        private string _serviceID;
        private int _rateCompanyID;                         // also present at bill level, also here for individual callers for simplicity
        private FuelType _fuel;
        private string _rateClass;
        private DerivedFuelType _derivedFuel;               // used only with derivations, such as sewer from water
        private string _rateClass2;                         // used only with derivations, such as with sewer
        private bool _derivedFuelIsPrimary;                 // boolean to indicate that the derived fuel is in play
        private double _maximumDerivedUsage = 0.0;          // used only with derivations, such as with sewer
        private DateTime? _startDate = null;
        private DateTime? _endDate = null;
        private DateTime? _projectedEndDate = null;
        private List<Reading> _readings = null;
        private List<Baseline> _baselines = null;
        private List<Usage> _usages = null;
        private bool _usagesInitiallyProvided;
        private double _usageConversionFactor = 1.0;
        private UnitOfMeasureType _primaryUnitOfMeasure;
        private CostToDateResult _costToDateResult;         // null on input; populated for output
        private int _internalMeterID;                       // not used by Energy Prism callers; used by Alert callers
        private int _daysWithMissingReadings;               // not used by Energy Prism callers; used by Alert callers
        private DateTime? _billNextReadDate = null;
        private bool _newMeter;


        /// <summary>
        /// Default constructor.
        /// </summary>
        public Meter()
        {
        }

        public Meter(string meterID, int rateCompanyID, string rateClass, FuelType fuel, DateTime startDate, DateTime endDate)
        {
            _meterID = meterID;
            _rateCompanyID = rateCompanyID;
            _rateClass = rateClass;
            _fuel = fuel;
            _startDate = startDate;
            _endDate = endDate;
        }

        /// <summary>
        /// Created a meter with the most common properties to initialize.
        /// Properties still need to be set by the caller.  Dates are inclusive.
        /// </summary>
        /// <param name="meterID"></param>
        /// <param name="rateClass"></param>
        /// <param name="fuel"></param>
        /// <param name="startDate"></param>
        /// <param name="endDate"></param>
        /// <param name="projectedEndDate"></param>
        public Meter(string meterID, int rateCompanyID, string rateClass, FuelType fuel, DateTime startDate, DateTime endDate, DateTime projectedEndDate)
            : this(meterID, rateCompanyID, rateClass, fuel, startDate, endDate)
        {
            _projectedEndDate = projectedEndDate;
        }

        /// <summary>
        /// Created a meter with common properties to initialize.  Projected number of days in time period used to set the projected end date in this case.
        /// Properties still need to be set by the caller.  Dates are inclusive.
        /// </summary>
        /// <param name="meterID"></param>
        /// <param name="rateClass"></param>
        /// <param name="fuel"></param>
        /// <param name="startDate"></param>
        /// <param name="endDate"></param>
        /// <param name="projectedNumberOfDays"></param>
        public Meter(string meterID, int rateCompanyID, string rateClass, FuelType fuel, DateTime startDate, DateTime endDate, int projectedNumberOfDays)
            : this(meterID, rateCompanyID, rateClass, fuel, startDate, endDate)
        {
            InitializeProjectedEndDate(projectedNumberOfDays);
        }

        /// <summary>
        /// Created a meter with common properties to initialize.
        /// </summary>
        /// <param name="meterID"></param>
        /// <param name="rateCompanyID"></param>
        /// <param name="rateClass"></param>
        /// <param name="fuel"></param>
        /// <param name="startDate"></param>
        /// <param name="endDate"></param>
        /// <param name="projectedEndDate"></param>
        /// <param name="readings"></param>
        public Meter(string meterID, int rateCompanyID, string rateClass, FuelType fuel, DateTime startDate, DateTime endDate, DateTime projectedEndDate, List<Reading> readings)
            : this(meterID, rateCompanyID, rateClass, fuel, startDate, endDate, projectedEndDate)
        {
            _readings = readings;
        }

        /// <summary>
        /// Created a meter common properties and baselines.
        /// </summary>
        /// <param name="meterID"></param>
        /// <param name="rateCompanyID"></param>
        /// <param name="rateClass"></param>
        /// <param name="fuel"></param>
        /// <param name="startDate"></param>
        /// <param name="endDate"></param>
        /// <param name="projectedEndDate"></param>
        /// <param name="readings"></param>
        /// <param name="baselines"></param>
        public Meter(string meterID, int rateCompanyID, string rateClass, FuelType fuel, DateTime startDate, DateTime endDate, DateTime projectedEndDate, List<Reading> readings, List<Baseline> baselines )
            : this(meterID, rateCompanyID, rateClass, fuel, startDate, endDate, projectedEndDate)
        {
            _readings = readings;
            _baselines = baselines;
        }

        /// <summary>
        /// Created a meter with common properties to initialize.  Specify primary unit of measure as hint for readings.
        /// </summary>
        /// <param name="meterID"></param>
        /// <param name="rateCompanyID"></param>
        /// <param name="rateClass"></param>
        /// <param name="fuel"></param>
        /// <param name="startDate"></param>
        /// <param name="endDate"></param>
        /// <param name="projectedEndDate"></param>
        /// <param name="primaryUnitOfMeasure"></param>
        /// <param name="readings"></param>
        public Meter(string meterID, int rateCompanyID, string rateClass, FuelType fuel, DateTime startDate, DateTime endDate, DateTime projectedEndDate, UnitOfMeasureType primaryUnitOfMeasure, List<Reading> readings)
            : this(meterID, rateCompanyID, rateClass, fuel, startDate, endDate, projectedEndDate)
        {
            _readings = readings;
            _primaryUnitOfMeasure = primaryUnitOfMeasure;
        }

        #region Properties

        public string MeterID
        {
            get { return _meterID; }
            set { _meterID = value; }
        }

        public int RateCompanyID
        {
            get { return _rateCompanyID; }
            set { _rateCompanyID = value; }
        }

        public string ServiceID
        {
            get { return _serviceID; }
            set { _serviceID = value; }
        }

        public FuelType Fuel
        {
            get { return _fuel; }
            set { _fuel = value; }
        }

        public DerivedFuelType DerivedFuel
        {
            get { return _derivedFuel; }
            set { _derivedFuel = value; }
        }

        public string RateClass
        {
            get { return _rateClass; }
            set { _rateClass = value; }
        }

        public string RateClass2
        {
            get { return _rateClass2; }
            set { _rateClass2 = value; }
        }

        public DateTime? StartDate
        {
            get { return _startDate; }
            set { _startDate = value; }
        }

        public DateTime? EndDate
        {
            get { return _endDate; }
            set { _endDate = value; }
        }

        public DateTime? ProjectedEndDate
        {
            get { return _projectedEndDate; }
            set { _projectedEndDate = value; }
        }

        public List<Reading> Readings
        {
            get { return _readings; }
            set { _readings = value; }
        }

        public List<Baseline> Baselines
        {
            get { return _baselines; }
            set { _baselines = value; }
        }

        public List<Usage> Usages
        {
            get { return _usages; }
            set { _usages = value; }
        }

        /// <summary>
        /// Property: UsagesInitiallyProvided. Callers that want to know they initially provided usages will set this.
        /// This is not set to true automatically when usages are assigned to a meter.
        /// </summary>
        public bool UsagesInitiallyProvided
        {
            get { return _usagesInitiallyProvided; }
            set { _usagesInitiallyProvided = value; }
        }

        public double UsageConversionFactor
        {
            get { return _usageConversionFactor; }
            set { _usageConversionFactor = value; }
        }

        public UnitOfMeasureType PrimaryUnitOfMeasure
        {
            get { return _primaryUnitOfMeasure; }
            set { _primaryUnitOfMeasure = value; }
        }

        public CostToDateResult CostToDateResult
        {
            get { return _costToDateResult; }
            set { _costToDateResult = value; }
        }

        public bool DerivedFuelIsPrimary
        {
            get { return _derivedFuelIsPrimary; }
            set { _derivedFuelIsPrimary = value; }
        }

        public double MaximumDerivedUsage
        {
            get { return _maximumDerivedUsage; }
            set { _maximumDerivedUsage = value; }
        }

        /// <summary>
        /// Property:  InternalMeterID.  
        /// Used only by Alerts callers to store an an internal identifier.  Not used by EP callers.
        /// </summary>
        public int InternalMeterID
        {
            get { return _internalMeterID; }
            set { _internalMeterID = value; }
        }

        /// <summary>
        /// Property:  DaysWithMissingReadings.  
        /// Used only by Alerts callers for keeping track of missing readings.  Not used by EP callers.
        /// </summary>
        public int DaysWithMissingReadings
        {
            get { return _daysWithMissingReadings; }
            set { _daysWithMissingReadings = value; }
        }

        public bool NewMeter
        {
            get { return _newMeter; }
            set { _newMeter = value; }
        }

        public DateTime? BillNextReadDate
        {
            get { return _billNextReadDate; }
            set { _billNextReadDate = value; }
        }

        #endregion

        /// <summary>
        /// Initialize the projected end date when it is not initialized.  This is public.
        /// </summary>
        /// <param name="projectedNumberOfDays"></param>
        public void InitializeProjectedEndDate(int projectedNumberOfDays)
        {
            // the projected end date cannot already have been set
            if (_projectedEndDate == null)
            {
                if (_startDate != null)
                {
                    _projectedEndDate = _startDate.Value.AddDays(projectedNumberOfDays - 1);    // endpoints are inclusive
                }
            }
        }

        /// <summary>
        /// Indicates if readings object exists and has one or more readings.
        /// </summary>
        /// <returns></returns>
        public bool HasReadings()
        {
            bool result = false;

            if (_readings != null)
            {
                if (_readings.Count() > 0)
                {
                    result = true;
                }
            }

            return (result);
        }

        /// <summary>
        /// Indicates if baselines object exists and any elements.
        /// </summary>
        /// <returns></returns>
        public bool HasBaselines()
        {
            var result = false;

            if (_baselines != null)
            {
                if (_baselines.Any())
                {
                    result = true;
                }
            }

            return (result);
        }

        /// <summary>
        /// Indicates if usages object exists and has one or more usages.
        /// </summary>
        /// <returns></returns>
        public bool HasUsages()
        {
            bool result = false;

            if (_usages != null)
            {
                if (_usages.Count() > 0)
                {
                    result = true;
                }
            }

            return (result);
        }


        /// <summary>
        /// Returns a copy of this object.  However, reading list and usage list are null.
        /// </summary>
        /// <returns></returns>
        public Meter Copy()
        {
            Meter result = null;

            result = new Meter();
            result.MeterID = this.MeterID;
            result.ServiceID = this.ServiceID;
            result.RateCompanyID = this.RateCompanyID;
            result.RateClass = this.RateClass;
            result.RateClass2 = this.RateClass2;
            result.Fuel = this.Fuel;
            result.DerivedFuel = this.DerivedFuel;
            result.UsageConversionFactor = this.UsageConversionFactor;
            result.PrimaryUnitOfMeasure = this.PrimaryUnitOfMeasure;
            result.StartDate = this.StartDate;
            result.EndDate = this.EndDate;
            result.ProjectedEndDate = this.ProjectedEndDate;
            result.DerivedFuelIsPrimary = this.DerivedFuelIsPrimary;
            result.MaximumDerivedUsage = this.MaximumDerivedUsage;
            result.CostToDateResult = this.CostToDateResult;        // the CostToDateResult is shallow, not normally populated before a copy
            result.InternalMeterID = this.InternalMeterID;
            result.UsagesInitiallyProvided = this.UsagesInitiallyProvided;
            result.DaysWithMissingReadings = this.DaysWithMissingReadings;

            return (result);
        }
    }
}
