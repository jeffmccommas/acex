﻿using System;

namespace CE.BillToDate
{
    /// <summary>
    /// Class for the non-metered service CostToDate calculation result.  
    /// </summary>
    public class NonMeteredServiceResult
    {
        private double _cost;
        private double _projectedCost;
        private double _usage;
        private double _projectedUsage;
        private DateTime _startDate;
        private DateTime _endDate;
        private DateTime _projectedEndDate;
        private int _numberOfDaysInCostToDate;
        private int _numberOfDaysInProjectedCost;
        private double _monthProrateFactorUsed;     // the prorate factor used in calculation
        private double _averageDailyCost;
        private StatusList _statusList;

        public NonMeteredServiceResult()
        {
            _statusList = new StatusList();
        }

        #region Properties

        public double Cost
        {
            get { return _cost; }
            set { _cost = value; }
        }

        public double ProjectedCost
        {
            get { return _projectedCost; }
            set { _projectedCost = value; }
        }

        public double Usage
        {
            get { return _usage; }
            set { _usage = value; }
        }

        public double ProjectedUsage
        {
            get { return _projectedUsage; }
            set { _projectedUsage = value; }
        }

        public DateTime StartDate
        {
            get { return _startDate; }
            set { _startDate = value; }
        }

        public DateTime EndDate
        {
            get { return _endDate; }
            set { _endDate = value; }
        }

        public DateTime ProjectedEndDate
        {
            get { return _projectedEndDate; }
            set { _projectedEndDate = value; }
        }

        public double MonthProrateFactorUsed
        {
            get { return _monthProrateFactorUsed; }
            set { _monthProrateFactorUsed = value; }
        }

        public int NumberOfDaysInCostToDate
        {
            get { return _numberOfDaysInCostToDate; }
            set { _numberOfDaysInCostToDate = value; }
        }

        public int NumberOfDaysInProjectedCost
        {
            get { return _numberOfDaysInProjectedCost; }
            set { _numberOfDaysInProjectedCost = value; }
        }

        public double AverageDailyCost
        {
            get { return _averageDailyCost; }
            set { _averageDailyCost = value; }
        }

        public StatusList StatusList
        {
            get { return _statusList; }
            set { _statusList = value; }
        }

        #endregion

    }
}
