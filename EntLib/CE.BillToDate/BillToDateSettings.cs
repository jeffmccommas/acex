﻿using CE.RateModel;

namespace CE.BillToDate
{
    /// <summary>
    /// Settings class for bill to date settings.  This class exists to hold additional settings that might be
    /// outside of the rate model settings.
    /// </summary>
    public class BillToDateSettings
    {
        private CostToDateSettings _settings;
        public int RateCompanyId { get; set; }

        public BillToDateSettings()
        {
            _settings = new CostToDateSettings();
        }

        #region Properties

        public CostToDateSettings Settings
        {
            get { return _settings; }
            set { _settings = value; }
        }

        #endregion

    }
    /*
    /// <summary>
    /// This should be moved out of here in future.
    /// </summary>
    public class BillToDateExtraParams
    {
        private const bool _defaultGetAMRfromDatabase = false;
        private const bool _defaultApplyConversionFactorForGas = false;
        private const bool _defaultApplyConversionFactorForWater = false;
        private const int _defaultAmiHighlightMaxDaysElectric = 0;
        private const int _defaultAmiHighlightMaxDaysGas = 0;
        private const int _defaultAmiHighlightMaxDaysWater = 0;
        private const int _defaultCurrentDay = 0;

        public BillToDateExtraParams()
        {
            GetAmrFromDatabase = _defaultGetAMRfromDatabase;
            ApplyConversionFactorForGas = _defaultApplyConversionFactorForGas;
            ApplyConversionFactorForWater = _defaultApplyConversionFactorForWater;
            AmiHighlightMaxDaysElectric = _defaultAmiHighlightMaxDaysElectric;
            AmiHighlightMaxDaysGas = _defaultAmiHighlightMaxDaysGas;
            AmiHighlightMaxDaysWater = _defaultAmiHighlightMaxDaysWater;
            DefaultCurrentDay = _defaultCurrentDay;
        }

        public bool UseNextReadDate { get; set; }
        public int ProjectedNumberOfDaysElectric { get; set; }
        public int ProjectedNumberOfDaysGas { get; set; }
        public int ProjectedNumberOfDaysWater { get; set; }
        public int AmiHighlightMaxDaysElectric { get; set; }
        public int AmiHighlightMaxDaysGas { get; set; }
        public int AmiHighlightMaxDaysWater { get; set; }
        public bool GetAmrFromDatabase { get; set; }
        public bool ApplyConversionFactorForGas { get; set; }
        public bool ApplyConversionFactorForWater { get; set; }
        public int DefaultCurrentDay { get; set; }

    }
    */
}
