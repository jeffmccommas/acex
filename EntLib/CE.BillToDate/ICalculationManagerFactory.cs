﻿using CE.RateModel;

namespace CE.BillToDate
{
    public interface ICalculationManagerFactory
    {
        /// <summary>
        /// Create CalculationManager that implements IBillToDate.
        /// </summary>
        /// <returns></returns>
        IBillToDate CreateCalculationManager();

        /// <summary>
        /// Create CalculationManager that implements IBillToDate.  Provide BillToDateSettings to inject.
        /// </summary>
        /// <param name="settings"></param>
        /// <returns></returns>
        IBillToDate CreateCalculationManager(BillToDateSettings settings);

        /// <summary>
        /// Create CalculationManager that implements IBillToDate.  Provide BillToDateSettings and logging override to inject.
        /// </summary>
        /// <param name="settings"></param>
        /// <returns></returns>
        IBillToDate CreateCalculationManager(BillToDateSettings settings, bool logOverride);

        /*
        /// <summary>
        /// Create CalculationManager that implements IBillToDate.  Provide IRateModel to inject into manager.
        /// </summary>
        /// <returns></returns>
        IBillToDate CreateCalculationManager(IRateModel rateModel);

        /// <summary>
        /// Create CalculationManager that implements IBillToDate.  Provide BillToDateSettings and IRateModel to inject into manager. 
        /// </summary>
        /// <param name="rateModel"></param>
        /// <param name="settings"></param>
        /// <returns></returns>
        IBillToDate CreateCalculationManager(IRateModel rateModel, BillToDateSettings settings);
        */
    }
}
