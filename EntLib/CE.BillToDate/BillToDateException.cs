﻿using System;
using System.Runtime.Serialization;

namespace CE.BillToDate
{
    public class BillToDateException : System.Exception
    {
        public BillToDateException()
            : base()
        {
        }

        public BillToDateException(string message)
            : base(message)
        {
        }

        public BillToDateException(string message, Exception inner)
            : base(message, inner)
        {
        }

        protected BillToDateException(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
        }

    }
}
