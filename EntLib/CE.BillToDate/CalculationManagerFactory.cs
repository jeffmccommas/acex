﻿using CE.RateModel;

namespace CE.BillToDate
{
    public class CalculationManagerFactory : ICalculationManagerFactory
    {
        public CalculationManagerFactory()
        {

        }

        /// <summary>
        /// Create CalculationManager that implements IBillToDate.
        /// </summary>
        /// <returns></returns>
        public IBillToDate CreateCalculationManager()
        {
            return (new CalculationManager());
        }

        /// <summary>
        /// Create CalculationManager that implements IBillToDate.  Provide BillToDateSettings to inject.
        /// </summary>
        /// <param name="settings"></param>
        /// <returns></returns>
        public IBillToDate CreateCalculationManager(BillToDateSettings settings)
        {
            return (new CalculationManager(settings));
        }


        /// <summary>
        /// Create CalculationManager that implements IBillToDate.  Provide BillToDateSettings and logging override to inject.
        /// </summary>
        /// <param name="settings"></param>
        /// <returns></returns>
        public IBillToDate CreateCalculationManager(BillToDateSettings settings, bool logOverride)
        {
            return (new CalculationManager(settings, logOverride));
        }
        /*
        /// <summary>
        /// Create CalculationManager that implements IBillToDate.  Provide IRateModel to inject into manager.
        /// </summary>
        /// <returns></returns>
        public IBillToDate CreateCalculationManager(IRateModel rateModel)
        {
            return (new CalculationManager(rateModel));
        }

        
        /// <summary>
        /// Create CalculationManager that implements IBillToDate.  Provide BillToDateSettings and IRateModel to inject into manager. 
        /// </summary>
        /// <param name="rateModel"></param>
        /// <param name="settings"></param>
        /// <returns></returns>
        public IBillToDate CreateCalculationManager(IRateModel rateModel, BillToDateSettings settings)
        {
            return (new CalculationManager(rateModel, settings));
        }
        */
    }
}
