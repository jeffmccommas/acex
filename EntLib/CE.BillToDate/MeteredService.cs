﻿
namespace CE.BillToDate
{
    public class MeteredService
    {
        private string _serviceID;
        private string _premiseID;
        private FuelType _fuel;
        private MeterList _meters;

        public MeteredService()
        {
            _meters = new MeterList();
        }

        public MeteredService(string serviceID, string premiseID, FuelType fuel)
            : this()
        {
            _serviceID = serviceID;
            _premiseID = premiseID;
            _fuel = fuel;
        }

        #region Properties

        public string ServiceID
        {
            get { return _serviceID; }
            set { _serviceID = value; }
        }

        public string PremiseID
        {
            get { return _premiseID; }
            set { _premiseID = value; }
        }

        public FuelType Fuel
        {
            get { return _fuel; }
            set { _fuel = value; }
        }

        public MeterList Meters
        {
            get { return _meters; }
            set { _meters = value; }
        }

        #endregion

        /// <summary>
        /// Return a copy of this item.  Shallow, lists are initialized, but empty.
        /// </summary>
        /// <returns></returns>
        public MeteredService Copy()
        {
            MeteredService result = null;

            result = new MeteredService();
            result.ServiceID = this.ServiceID;
            result.PremiseID = this.PremiseID;
            result.Fuel = this.Fuel;

            return (result);
        }
    }
}
