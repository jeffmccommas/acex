﻿using System;

namespace CE.BillToDate
{
    public class NonMeteredService
    {
        private string _name;                               // aka. NexusType in EP context
        private double _amount;                             // dollar amount, usually a charge from last bill
        private double _usage;
        private string _description;
        private string _uom;
        private DateTime? _startDate = null;                        // use this for consistency between non-metered and metered
        private DateTime? _endDate = null;                          // use this for consistency between non-metered and metered
        private DateTime? _projectedEndDate = null;                 // use this for consistency between non-metered and metered
        private NonMeteredServiceResult _costToDateResult = null;   // null on input

        /// <summary>
        /// Default constructor.
        /// </summary>
        public NonMeteredService()
        {
        }


        public NonMeteredService(string name, double amount, DateTime startDate, DateTime endDate)
        {
            _costToDateResult = null;
            _name = name;
            _amount = amount;
            _startDate = startDate;
            _endDate = endDate;
        }
        public NonMeteredService(string name, double amount, double usage, string uom, DateTime startDate, DateTime endDate)
        {
            _costToDateResult = null;
            _name = name;
            _amount = amount;
            _startDate = startDate;
            _endDate = endDate;
            _usage = usage;
            _uom = uom;
        }

        /// <summary>
        /// Creates a non-metered service given the name, cost amount, startDate, the endDate (usually current date), 
        /// and the projected end date for the time period.
        /// </summary>
        /// <param name="name"></param>
        /// <param name="amount"></param>
        /// <param name="startDate"></param>
        /// <param name="endDate"></param>
        /// <param name="projectedEndDate"></param>
        public NonMeteredService(string name, double amount, DateTime startDate, DateTime endDate, DateTime projectedEndDate)
            : this(name, amount, startDate, endDate)
        {
            _projectedEndDate = projectedEndDate;
        }

        /// <summary>
        /// Creates a non-metered service given the name, cost amount, startDate, the endDate (usually current date), 
        /// and the projected number of days for the time period.
        /// </summary>
        /// <param name="name"></param>
        /// <param name="amount"></param>
        /// <param name="startDate"></param>
        /// <param name="endDate"></param>
        /// <param name="projectedNumberOfdays"></param>
        public NonMeteredService(string name, double amount, DateTime startDate, DateTime endDate, int projectedNumberOfDays)
            : this(name, amount, startDate, endDate)
        {
            InitializeProjectedEndDate(projectedNumberOfDays);
        }

        #region Properties

        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }

        public string Description
        {
            get { return _description; }
            set { _description = value; }
        }
        
        public string Uom
        {
            get { return _uom; }
            set { _uom = value; }
        }

        public double Amount
        {
            get { return _amount; }
            set { _amount = value; }
        }
        public double Usage
        {
            get { return _usage; }
            set { _usage = value; }
        }

        public DateTime? StartDate
        {
            get { return _startDate; }
            set { _startDate = value; }
        }

        public DateTime? EndDate
        {
            get { return _endDate; }
            set { _endDate = value; }
        }

        public DateTime? ProjectedEndDate
        {
            get { return _projectedEndDate; }
            set { _projectedEndDate = value; }
        }

        public NonMeteredServiceResult CostToDateResult
        {
            get { return _costToDateResult; }
            set { _costToDateResult = value; }
        }

        #endregion

        /// <summary>
        /// Initialize the projected end date when it is not initialized.  This is public.
        /// </summary>
        /// <param name="projectedNumberOfDays"></param>
        public void InitializeProjectedEndDate(int projectedNumberOfDays)
        {
            // the projected end date cannot already have been set
            if (_projectedEndDate == null)
            {
                if (_startDate != null)
                {
                    _projectedEndDate = _startDate.Value.AddDays(projectedNumberOfDays - 1);    // endpoints are inclusive
                }
            }
        }

        /// <summary>
        /// Return a  copy of this object.  Each member is copied.  The costToDate result is shallow.
        /// </summary>
        /// <returns></returns>
        public NonMeteredService Copy()
        {
            NonMeteredService result = null;

            result = new NonMeteredService();
            result.Name = this.Name;
            result.Amount = this.Amount;
            result.Description = this.Description;
            result.StartDate = this.StartDate;
            result.EndDate = this.EndDate;
            result.ProjectedEndDate = this.ProjectedEndDate;
            result.CostToDateResult = this.CostToDateResult;
            result.Uom = this.Uom;
            result.Usage = this.Usage;
            return (result);
        }
    }
}
