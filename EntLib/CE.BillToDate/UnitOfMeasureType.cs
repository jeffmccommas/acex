﻿
namespace CE.BillToDate
{
    public enum UnitOfMeasureType
    {
        undefined,
        kWh,
        kW,
        Therm,
        CCF,
        MCF,
        CF,
        M3,
        gal,
        CGal,
        kGal,
        other,
        HCF
    }
}
