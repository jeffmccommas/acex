﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace CE.BillToDate
{
    /// <summary>
    /// Status class.
    /// </summary>
    public class Status
    {
        Exception _exception;
        StatusTypes.StatusSeverity _statusSeverity;


        #region "Public Properties"

        /// <summary>
        /// Property: Exception
        /// </summary>
        public Exception Exception
        {
            get { return _exception; }
            set { _exception = value; }
        }

        /// <summary>
        /// Property: Status serverity.
        /// </summary>
        public StatusTypes.StatusSeverity StatusSeverity
        {
            get { return _statusSeverity; }
            set { _statusSeverity = value; }
        }

        #endregion


        /// <summary>
        /// Default constructor.
        /// </summary>
        public Status()
        {
            _exception = null;
            _statusSeverity = StatusTypes.StatusSeverity.Unspecified;
        }

        /// <summary>
        /// Constructor.
        /// </summary>
        public Status(Exception exception, StatusTypes.StatusSeverity statusServerity)
        {
            _exception = exception;
            _statusSeverity = statusServerity;
        }

    }


    /// <summary>
    /// StatusList class.
    /// </summary>
    public class StatusList : List<Status>
    {

        /// <summary>
        /// Quick determiniation to see if status list has any errors.
        /// </summary>
        /// <returns></returns>
        public bool HasNoErrors()
        {
            return (!HasStatusSeverity(StatusTypes.StatusSeverity.Error));
        }


        /// <summary>
        /// Determine if status list containts specified status severity.
        /// </summary>
        /// <returns></returns>
        public bool HasStatusSeverity(StatusTypes.StatusSeverity statusSeverity)
        {
            bool result = false;
            Status status = null;

            try
            {
                //Find status with matching status severity.
                status = this.Find(statusToFind => statusToFind.StatusSeverity == statusSeverity);

                //Status with matching status severtiy found.
                if (status != null)
                {
                    result = true;
                }
            }
            catch (Exception)
            {
                throw;
            }

            return result;
        }

        /*
        /// <summary>
        /// Determine count of specified status severity.
        /// </summary>
        /// <returns></returns>
        public int CountOfStatusSeverity(StatusTypes.StatusSeverity statusSeverity)
        {
            List<Status> statusList = null;
            var count = 0;

            try
            {
                //Find status with matching status severity.
                statusList = this.FindAll(statusToFind => statusToFind.StatusSeverity == statusSeverity);

                //Status with matching status severtiy found.
                if (statusList != null)
                {
                    count = statusList.Count();
                }
            }
            catch (Exception)
            {
                throw;
            }

            return (count);
        }
        
        /// <summary>
        /// Determine if status list containts specified type.
        /// </summary>
        /// <returns></returns>
        public bool HasException(System.Type comparisonType)
        {
            bool result = false;
            Status status = null;

            try
            {

                //Find status with matching exception subtype.
                status = this.Find(statusToFind => statusToFind.Exception.GetType() == comparisonType);

                //Status with matching exception subtype found.
                if (status != null)
                {
                    result = true;
                }

            }
            catch (Exception)
            {
                throw;
            }

            return result;

        }
        
        /// <summary>
        /// Return status list with all status objects that match specified status severity.
        /// If no statuses match then empty status list is returned.
        /// </summary>
        /// <param name="statusSeverity"></param>
        /// <returns></returns>
        public StatusList GetStatusListByStatusSeverity(StatusTypes.StatusSeverity statusSeverity)
        {
            StatusList result = null;
            List<Status> matchingStatuslist = null;

            matchingStatuslist = this.FindAll(statusToFind => statusToFind.StatusSeverity == statusSeverity);

            result = new StatusList();

            result.AddRange(matchingStatuslist);

            return result;
        }
        */
    }


    /// <summary>
    /// Status management types.
    /// </summary>
    public class StatusTypes
    {

        public enum StatusSeverity
        {
            Unspecified = 0,
            Error = 1,
            Warning = 2,
            Informational = 3
        }

    }


}
