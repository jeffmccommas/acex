﻿using CE.RateModel;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Xml.Linq;

namespace CE.BillToDate
{
    public class CalculationManager : IBillToDate
    {
        private IRateModel _rateModel = null;
        private BillToDateSettings _settings = null;
        private RateClassList _rateClassList = null;

        private const string Exception_Message_NonMeteredServiceDatesAreNull = "Non-metered service startDate, endDate, or projectedEndDate is null.";
        private const string Exception_Message_NonMeteredServiceNameIsEmpty = "Name of non-metered service is null or empty.";
        private const string Exception_Message_NoInputBill = "No bill data input to calculation.";
        private const string Exception_Message_BillToDateFailed = "BillToDate failed.";
        private const string Exception_Message_CalculateNonMeteredServiceCostToDateFailed = "CalculateNonMeteredServiceCostToDate failed.";
        private const string Exception_Message_MeterCostToDateFailed = "CalculateMeterCostToDate failed.";
        private const string Exception_Message_CustomerTypeError = "Customer Type of Rate class cannot be determined.";
        private const string Exception_Message_AppSettingsReadError = "Could not read from app settings.";
        private const string ErrorMessageForMeterFormatString = "{0} | meterID = {1}";
        private const string WarningMessageForMeterFormatString = "Warning: {0} | meterID = {1}";
        private const string Exception_Message_CostIsNegative = "Total Cost or Projected Cost is negative";
        private const string Exception_Informational_Tracing_RateModelLogSessionID = "RateModelLogSessionID = ";
        private const string AppSettings_BillToDateTracing = "BillToDateTracing";
        private const string Trace_Source = "BillToDate";   //do not change this
        private const string Trace_Source_Log = "Aclara";   //do not change this
        private const string DailyDemandWarningString = "Demand usage is excluded from calculation";
        private bool _enableTracing = false;
        private EventLogTraceListener _primaryTraceListener = null;

        #region Constructors

        public CalculationManager()
        {
            _rateModel = GetRateModel();
            LoadConfiguration();
        }
        
        public CalculationManager(bool logOverride)
        {
            _rateModel = GetRateModel(logOverride);
            LoadConfiguration();
            _enableTracing = logOverride;
        }
        /*
        public CalculationManager(IRateModel rateModel)
        {
            _rateModel = rateModel;
            LoadConfiguration();
        }
        
        public CalculationManager(IRateModel rateModel, BillToDateSettings settings)
            : this(rateModel)
        {
            _settings = settings;
        }
        */
        public CalculationManager(BillToDateSettings settings)
            : this()
        {
            _settings = settings;
        }

        public CalculationManager(BillToDateSettings settings, bool logOverride)
            : this(logOverride)
        {
            _settings = settings;
        }

        #endregion

        /*
        #region Properties

        public BillToDateSettings Settings
        {
            get { return _settings; }
            set { _settings = value; }
        }

        #endregion
            */

        #region Functions that Implement Interface IBillToDate

        /// <summary>
        /// Calculate BillToDate for full hierarchy of bill information.
        /// </summary>
        /// <param name="inputBill"></param>
        /// <returns></returns>
        public BillToDateResult CalculateBillToDate(Bill inputBill)
        {
            BillToDateResult result = null;

            try
            {
                // if settings were not initialized or set prior to this call, be sure to create with default settings
                if (_settings == null)
                {
                    _settings = new BillToDateSettings();
                }

                result = MainCalculateBillToDate(inputBill, _settings);

            }
            catch (Exception ex)
            {
                // error, general
                result = new BillToDateResult();
                result.StatusList.Add(new Status(new BillToDateException(Exception_Message_BillToDateFailed, ex), StatusTypes.StatusSeverity.Error));
            }

            // If RateModel has logging enabled, be sure to save the LogSessionID as an informational Exception to the StatusList
            if (_rateModel.IsLoggingEnabled())
            {
                result.StatusList.Add(new Status(new BillToDateException(Exception_Informational_Tracing_RateModelLogSessionID + _rateModel.LogSessionID()), StatusTypes.StatusSeverity.Informational));
            }

            return (result);
        }



        /// <summary>
        /// Calculate bill-to-date by iterating metered services and non-metered services
        /// </summary>
        /// <param name="inputBill"></param>
        /// <param name="settings"></param>
        /// <returns></returns>
        private BillToDateResult MainCalculateBillToDate(Bill inputBill, BillToDateSettings s)
        {
            BillToDateResult result = null;
            Bill bill = null;
            NonMeteredService nms = null;
            MeteredService ms = null;
            Meter m = null;
            bool valid = true;

            result = new BillToDateResult();

            if (inputBill != null)
            {
                // copy the bill
                bill = inputBill.Copy();

                // iterate non-metered services, create new copies and add non-metered service results
                foreach (NonMeteredService nmsItem in inputBill.NonMeteredServices)
                {
                    nms = nmsItem.Copy();
                    nms.CostToDateResult = MainCalculateNonMeteredServiceCostToDate(nmsItem, s);
                    // TFS 29208 the negative non- meter service charge is not included in the calculation.
                    if (nms.CostToDateResult.ProjectedCost > 0)
                    {
                        // add each result to the bill
                        bill.NonMeteredServices.Add(nms);
                    }

                    // add errors to result status list if exist
                    if (!nms.CostToDateResult.StatusList.HasNoErrors())
                    {
                        result.StatusList.AddRange(nms.CostToDateResult.StatusList);
                    }
                }


                // iterate the metered services, created new copies and adding meters and results for each meter
                foreach (MeteredService msItem in inputBill.MeteredServices)
                {
                    ms = msItem.Copy();

                    // Validate the use of multiple meters per service
                    valid = true;
                    if (!s.Settings.General.AllowMultipleMetersPerService && msItem.Meters.Count > 1)
                    {
                        valid = false;
                    }

                    if (valid)
                    {

                        foreach (Meter mItem in msItem.Meters)
                        {
                            m = mItem.Copy();
                            m.CostToDateResult = MainCalculateMeterCostToDate(mItem, s);

                            // add each meter to the meter list within the metered service
                            ms.Meters.Add(m);

                            // add error to result statusList if exist
                            if (m.CostToDateResult.ErrCode != Enums.ErrorCode.NoError)
                            {
                                result.StatusList.Add(new Status(new BillToDateException(ErrorMessageForMeter(m.CostToDateResult.ErrCode.ToString(), m.MeterID)), StatusTypes.StatusSeverity.Error));
                            }

                            // add warning to result statusList if error present for the projected cost
                            if (m.CostToDateResult.ProjectedCostErrCode != Enums.ErrorCode.NoError)
                            {
                                result.StatusList.Add(new Status(new BillToDateException(WarningMessageForMeter(m.CostToDateResult.ProjectedCostErrCode.ToString() + " for Projected Cost", m.MeterID)), StatusTypes.StatusSeverity.Warning));
                            }

                            // CR 51610 Oct 2014 - add daily demand exclude message to statuslist as informational.  
                            // alerts will use it to publish warning instead of error
                            if(m.CostToDateResult.DailyDemandCalcExcluded)
                                result.StatusList.Add(new Status(new BillToDateException(WarningMessageForMeter(DailyDemandWarningString, m.MeterID)), StatusTypes.StatusSeverity.Warning));

                            // check to see if extra work is required here for derived meter (sewer)
                            DerivedMeterProcessing(result, ms, mItem, m, s);

                        }

                    }
                    else
                    {
                        // mark each meter with error, multiple meters per service not allowed;
                        foreach (Meter mItem in msItem.Meters)
                        {
                            m = mItem.Copy();
                            m.CostToDateResult = new CostToDateResult();
                            m.CostToDateResult.ErrCode = Enums.ErrorCode.ErrUnsupportedMultipleMetersInService;

                            ms.Meters.Add(m);

                            result.StatusList.Add(new Status(new BillToDateException(ErrorMessageForMeter(m.CostToDateResult.ErrCode.ToString(), m.MeterID)), StatusTypes.StatusSeverity.Error));
                        }
                    }

                    // add each meter service and subsequent meter result to the bill
                    bill.MeteredServices.Add(ms);

                }

                // assign bill to result
                result.Bill = bill;

                // sum the total cost and total projected cost
                result.TotalCost = bill.NonMeteredServices.Sum(p => p.CostToDateResult.Cost) +
                                    bill.MeteredServices.Sum(p => p.Meters.Sum(q => q.CostToDateResult.Cost));

                result.TotalProjectedCost = bill.NonMeteredServices.Sum(p => p.CostToDateResult.ProjectedCost) +
                                    bill.MeteredServices.Sum(p => p.Meters.Sum(q => q.CostToDateResult.ProjectedCost));
                if (result.TotalCost < 0 || result.TotalProjectedCost < 0)
                {
                    result.StatusList.Add(new Status(new BillToDateException(Exception_Message_CostIsNegative), StatusTypes.StatusSeverity.Error));
                }



            }
            else
            {
                // create empty bill and add an error to the status list
                result.Bill = new Bill();
                result.StatusList.Add(new Status(new BillToDateException(Exception_Message_NoInputBill), StatusTypes.StatusSeverity.Error));
            }

            // attempt to output details of the result if tracing enabled (for QA and debugging use mostly)
            TraceHelper(result, s);

            return (result);
        }


        /// <summary>
        /// Create meters based upon derived meter settings.  Right now, sewer from water is one kind.
        /// </summary>
        /// <param name="ms"></param>
        /// <param name="currentMeter"></param>
        /// <param name="m"></param>
        /// <param name="s"></param>
        private void DerivedMeterProcessing(BillToDateResult result, MeteredService ms, Meter currentMeter, Meter derivedFromMeter, BillToDateSettings s)
        {
            Enums.CustomerType custType;
            Meter m2 = null;
            Meter newMeter = null;
            double totalUsage = 0.0;

            // check for derived meters based on fuel;  one type now is deriving sewer from water

            if (currentMeter.DerivedFuel == DerivedFuelType.sewer)
            {
                custType = GetCustomerType(currentMeter.RateCompanyID, currentMeter.RateClass2);

                if ((s.Settings.Sewer.UseSewerMaximumMonthsUsageForResidential && custType == Enums.CustomerType.Residential)
                    || (s.Settings.Sewer.UseSewerMaximumMonthsUsageForCommercial && custType == Enums.CustomerType.Commercial))
                {

                    if (currentMeter.Readings != null)
                    {
                        totalUsage = currentMeter.Readings.Sum(p => p.Quantity);
                    }
                    else if (currentMeter.Usages != null)
                    {
                        totalUsage = currentMeter.Usages.Sum(p => p.Quantity);
                    }

                    double adjustedMaxMonthsUsage = 0;
                    if (s.Settings.Sewer.ProrateSewerMaximumMonthsUsageDeterminants)
                    {
                        if (currentMeter.MaximumDerivedUsage > 0)
                        {
                            adjustedMaxMonthsUsage = currentMeter.MaximumDerivedUsage * GetProrateFactor(currentMeter.StartDate.Value, currentMeter.EndDate.Value, s);   //s.Settings.Validate.DaysInFullMonth, s.Settings.General.ProjectedNumberOfDays);
                        }
                        else
                        {
                            // fallback to maximums from settings
                            if (custType == Enums.CustomerType.Residential)
                            {
                                adjustedMaxMonthsUsage = (double)s.Settings.Sewer.SewerMaximumMonthsUsageForResidential;
                            }
                            else if (custType == Enums.CustomerType.Commercial)
                            {
                                adjustedMaxMonthsUsage = (double)s.Settings.Sewer.SewerMaximumMonthsUsageForCommercial;
                            }
                            adjustedMaxMonthsUsage = adjustedMaxMonthsUsage * GetProrateFactor(currentMeter.StartDate.Value, currentMeter.EndDate.Value, s);   //s.Settings.Validate.DaysInFullMonth, s.Settings.General.ProjectedNumberOfDays);
                        }

                    }


                    // use provided maximum first if available
                    if (totalUsage > adjustedMaxMonthsUsage)
                    {
                        totalUsage = adjustedMaxMonthsUsage;
                    }


                    newMeter = currentMeter.Copy();
                    newMeter.Usages = new List<Usage>();
                    newMeter.Usages.Add(new Usage(totalUsage));
                    newMeter.DerivedFuelIsPrimary = true;           // sewer is in play, with modified usage

                }
                else
                {
                    newMeter = currentMeter.Copy();
                    newMeter.Readings = currentMeter.Readings;      // point at water readings
                    newMeter.Usages = currentMeter.Usages;          // point at water usages
                    newMeter.DerivedFuelIsPrimary = true;           // sewer is in play, with water readings/usage
                }

                m2 = newMeter.Copy();
                m2.CostToDateResult = MainCalculateMeterCostToDate(newMeter, s);

                // special setting of LastReadingsDate here, using result from source meter;  even though only usages were provided for derived meter calculation
                m2.CostToDateResult.LastReadingDate = derivedFromMeter.CostToDateResult.LastReadingDate;

                // add the derived sewer meter calculation to the result, based on water
                ms.Meters.Add(m2);

                // add error to result statusList if exist
                if (m2.CostToDateResult.ErrCode != Enums.ErrorCode.NoError)
                {
                    result.StatusList.Add(new Status(new BillToDateException(ErrorMessageForMeter(m2.CostToDateResult.ErrCode.ToString(), m2.MeterID)), StatusTypes.StatusSeverity.Error));
                }

                // add warning to result statusList if error present for the projected cost
                if (m2.CostToDateResult.ProjectedCostErrCode != Enums.ErrorCode.NoError)
                {
                    result.StatusList.Add(new Status(new BillToDateException(WarningMessageForMeter(m2.CostToDateResult.ProjectedCostErrCode.ToString(), m2.MeterID)), StatusTypes.StatusSeverity.Warning));
                }

            }

        }



        /// <summary>
        /// Calculate cost-to-date for an individual meter.  Appropriate overload will be called internally based upon
        /// readings and usage present or not present within the provided meter object.
        /// </summary>
        /// <param name="m"></param>
        /// <returns></returns>
        public CostToDateResult CalculateMeterCostToDate(Meter m)
        {
            CostToDateResult result = null;

            try
            {
                // if settings were not initialized or set prior to this call, be sure to create with default settings
                if (_settings == null)
                {
                    _settings = new BillToDateSettings();
                }

                result = MainCalculateMeterCostToDate(m, _settings);

            }
            catch(Exception ex)
            {                
                // general error
                result = new CostToDateResult();
                result.ErrCode = Enums.ErrorCode.ErrGeneric;
                throw ex;
            }

            return (result);
        }


        /// <summary>
        /// Calculate cost-to-date for individual meter.
        /// </summary>
        /// <param name="m"></param>
        /// <param name="s"></param>
        /// <returns></returns>
        private CostToDateResult MainCalculateMeterCostToDate(Meter m, BillToDateSettings s)
        {
            CostToDateResult result = null;
            string rateClass;
            bool valid = true;
            CostToDateExtParam extParam = null;
            bool hasReadings = false;
            bool hasUsages = false;
            bool hasBaselines = false;

            // use settings.ProjectedNumberOfDays to set the projected date, but only 
            // if it is currently null; projected number of days is normally derived from startDate and projectedEndDate
            m.InitializeProjectedEndDate(s.Settings.General.ProjectedNumberOfDays);

            // derived or regular
            if (!m.DerivedFuelIsPrimary)
            {
                rateClass = m.RateClass;
            }
            else
            {
                rateClass = m.RateClass2;
            }

            // Check meter properties against settings for anything that will short-circuit call to calculate
            // If invalid, the meter (m) will contain the error in its result
            valid = ValidateMeterProperties(m, s);

            if (valid)
            {
                ProcessConversionFactors(m, s);

                extParam = new CostToDateExtParam(s.Settings.General.ProrateMonthlyServiceCharges,
                                                   s.Settings.General.MinimumChargeType,
                                                   s.Settings.General.ProrateDemandUsageDeterminants,
                                                   s.Settings.General.SmoothTiers,
                                                   s.Settings.Validate.CheckDaysInFullMonth,
                                                   s.Settings.Validate.DaysInFullMonth,
                                                   s.Settings.General.ProjectedNumberOfDays,
                                                   s.Settings.General.DSTEndDate,
                                                   s.Settings.General.AllowRebateCalculations,
                                                   s.Settings.General.AllowBaselineCalculations);
                extParam.IncludeDailyDemandCalc = s.Settings.General.SupportDailyDemand;
                

                // assign some properties outside of constructor.
                if (s.Settings.General.DailyTierBillDaysType == Enums.DailyTierBillDaysType.UseProjectedBillDays)
                {
                    extParam.UseProjectedEndDateWhenFinalizingTierBoundaries = true;
                }
                else
                {
                    extParam.UseProjectedEndDateWhenFinalizingTierBoundaries = false;
                }

                // call proper method here, based on presence or absence of readings and/or usage.
                hasReadings = m.HasReadings();
                hasUsages = m.HasUsages();
                hasBaselines = m.HasBaselines();

                if (hasReadings && hasUsages)
                {
                    var reversedReadings = m.Readings;
                    reversedReadings.Reverse();
                    result = _rateModel.CostToDate(m.RateCompanyID,
                                                    reversedReadings,
                                                    m.Usages,
                                                    rateClass,
                                                    m.StartDate.Value,
                                                    m.EndDate.Value,
                                                    m.ProjectedEndDate.Value,
                                                    extParam);
                }
                else if (hasReadings && hasBaselines)
                {
                    result = _rateModel.CostToDate(m.RateCompanyID,
                                                    m.Readings,
                                                    m.Baselines,
                                                    rateClass,
                                                    m.StartDate.Value,
                                                    m.EndDate.Value,
                                                    m.ProjectedEndDate.Value,
                                                    extParam);
                }
                else if (hasReadings) {
                    var reversedReadings = m.Readings;
                    reversedReadings.Reverse();
                    result = _rateModel.CostToDate(m.RateCompanyID,
                                                    reversedReadings,
                                                    rateClass,
                                                    m.StartDate.Value,
                                                    m.EndDate.Value,
                                                    m.ProjectedEndDate.Value,
                                                    extParam);
                }
                else if (hasUsages)
                {
                    result = _rateModel.CostToDate(m.RateCompanyID,
                                                    m.Usages,
                                                    rateClass,
                                                    m.StartDate.Value,
                                                    m.EndDate.Value,
                                                    m.ProjectedEndDate.Value,
                                                    extParam);
                }
                else
                {
                    result = new CostToDateResult();
                    result.ErrCode = Enums.ErrorCode.ErrGeneric;
                }

            }
            else
            {
                result = m.CostToDateResult;
            }

            RevertConversionFactors(m, result.Usages, s);
            RevertConversionFactors(m, result.ProjectedUsages, s);


            return (result);
        }




        private bool ValidateMeterProperties(Meter m, BillToDateSettings s)
        {
            bool valid = true;

            if (valid)
            {
                valid = ValidateMinimumBillDays(m, s);
            }

            if (valid)
            {
                valid = ValidateMinimumUsage(m, s);
            }

            if (valid)
            {
                valid = ValidateMaximumMissingDays(m, s);
            }

            return (valid);
        }

        private bool ValidateMaximumMissingDays(Meter m, BillToDateSettings s)
        {
            bool valid = true;
            int markedDay = 0;
            int day = 0;
            int missingDayCount = 0;

            if (s.Settings.Validate.CheckMaximumMissingDays)
            {
                if (m.Readings != null)
                {
                    for (int i = 0; i < m.Readings.Count(); i++)
                    {
                        day = m.Readings[i].Timestamp.DayOfYear;
                        if (day != markedDay && m.Readings[i].IsEmpty)
                        {
                            missingDayCount++;
                            markedDay = day;
                        }
                    }

                    if (missingDayCount > s.Settings.Validate.MaximumMissingDays)
                    {
                        valid = false;
                        m.CostToDateResult = new CostToDateResult();
                        m.CostToDateResult.ErrCode = Enums.ErrorCode.ErrMinimumUsageNotMet;
                    }
                }
            }

            return (valid);
        }

        private bool ValidateMinimumUsage(Meter m, BillToDateSettings s)
        {
            bool valid = true;

            if (s.Settings.Validate.CheckMinimumUsage)
            {
                if (m.Readings != null)
                {
                    if (m.Readings.Sum(p => p.Quantity) < s.Settings.Validate.MinimumUsage)
                    {
                        valid = false;
                        m.CostToDateResult = new CostToDateResult();
                        m.CostToDateResult.ErrCode = Enums.ErrorCode.ErrMinimumUsageNotMet;
                    }
                }
            }

            return (valid);
        }

        private bool ValidateMinimumBillDays(Meter m, BillToDateSettings s)
        {
            bool valid = true;
            int minimumBillDays = 0;

            if (s.Settings.Validate.CheckMinimumBillDays)
            {
                switch (m.Fuel)
                {
                    case FuelType.electric:
                        minimumBillDays = s.Settings.Validate.MinimumBillDaysElectric;
                        break;
                    case FuelType.gas:
                        minimumBillDays = s.Settings.Validate.MinimumBillDaysGas;
                        break;
                    case FuelType.water:
                        minimumBillDays = s.Settings.Validate.MinimumBillDaysWater;
                        break;
                    default:
                        minimumBillDays = s.Settings.Validate.MinimumBillDaysElectric;
                        break;
                }

                if ((m.EndDate.Value.Subtract(m.StartDate.Value).TotalDays + 1) < minimumBillDays)
                {
                    valid = false;
                    m.CostToDateResult = new CostToDateResult();
                    m.CostToDateResult.ErrCode = Enums.ErrorCode.ErrMinimumBillDaysNotMet;
                }

            }

            return (valid);
        }

        /// <summary>
        /// Check and adjust readings and usage if conversion factor(s) are in play
        /// </summary>
        /// <param name="m"></param>
        /// <param name="s"></param>
        private void ProcessConversionFactors(Meter m, BillToDateSettings s)
        {
            double factor;

            // for gas or water;  use if supplied with meter OR if a conversion factor exists in settings and is enabled and not normal; scale usage and readings
            if ((m.Fuel == FuelType.gas && m.UsageConversionFactor != 1.0) ||
                (m.Fuel == FuelType.water && m.UsageConversionFactor != 1.0) ||
                (m.Fuel == FuelType.gas && s.Settings.General.UseConversionFactorForGas) ||
                (m.Fuel == FuelType.water && s.Settings.General.UseConversionFactorForWater))
            {
                double conversionFactor = 0;
                if (m.Fuel == FuelType.gas)
                {
                    conversionFactor = s.Settings.General.ConversionFactorForGas;
                }
                else
                {
                    conversionFactor = s.Settings.General.ConversionFactorForWater;
                }

                if (m.UsageConversionFactor != 1.0 || conversionFactor != 1.0)
                {
                    if (m.UsageConversionFactor != 1.0)
                    {
                        factor = m.UsageConversionFactor;
                    }
                    else
                    {
                        factor = conversionFactor;
                    }

                    //scale readings
                    if (m.Readings != null && factor != 0)
                    {
                        for (int i = 0; i < m.Readings.Count; i++)
                        {
                            m.Readings[i].Quantity = m.Readings[i].Quantity * factor;
                        }

                    }

                    //scale usages
                    if (m.Usages != null && factor != 0)
                    {
                        for (int i = 0; i < m.Usages.Count; i++)
                        {
                            m.Usages[i].Quantity = m.Usages[i].Quantity * factor;
                        }

                    }

                }

            }

        }

        /// <summary>
        /// Check and adjust readings and usage if conversion factor(s) are in play
        /// </summary>
        /// <param name="m"></param>
        /// <param name="s"></param>
        private void RevertConversionFactors(Meter m, List<Usage> usages, BillToDateSettings s)
        {
            double factor;

            // for gas or water;  use if supplied with meter OR if a conversion factor exists in settings and is enabled and not normal; scale usage and readings
            if ((m.Fuel == FuelType.gas && m.UsageConversionFactor != 1.0) ||
                (m.Fuel == FuelType.water && m.UsageConversionFactor != 1.0) ||
                (m.Fuel == FuelType.gas && s.Settings.General.UseConversionFactorForGas) ||
                (m.Fuel == FuelType.water && s.Settings.General.UseConversionFactorForWater))
            {
                double conversionFactor = 0;
                if (m.Fuel == FuelType.gas)
                {
                    conversionFactor = s.Settings.General.ConversionFactorForGas;
                }
                else
                {
                    conversionFactor = s.Settings.General.ConversionFactorForWater;
                }

                if (m.UsageConversionFactor != 1.0 || conversionFactor != 1.0)
                {
                    if (m.UsageConversionFactor != 1.0)
                    {
                        factor = m.UsageConversionFactor;
                    }
                    else
                    {
                        factor = conversionFactor;
                    }                    

                    //scale usages
                    if (factor != 0)
                    {
                        for (int i = 0; i < usages.Count; i++)
                        {
                            usages[i].Quantity = usages[i].Quantity * (1 / factor);
                            //scale readings
                            if (usages[i].Readings != null && factor != 0)
                            {
                                for (int j = 0; j < m.Readings.Count; j++)
                                {
                                    usages[i].Readings[j].Quantity = usages[i].Readings[j].Quantity * (1 / factor);
                                }
                            }
                        }

                    }

                }

            }

        }


        /// <summary>
        /// Calculate the non-metered service CostToDate for the given item.
        /// </summary>
        /// <param name="m"></param>
        /// <returns></returns>
        public NonMeteredServiceResult CalculateNonMeteredServiceCostToDate(NonMeteredService m)
        {
            NonMeteredServiceResult result = null;

            try
            {
                // if settings were not initialized or set prior to this call, be sure to create with default settings
                if (_settings == null)
                {
                    _settings = new BillToDateSettings();
                }

                result = MainCalculateNonMeteredServiceCostToDate(m, _settings);

            }
            catch (Exception ex)
            {
                // basic error for now, when failure
                result = new NonMeteredServiceResult();
                result.StatusList.Add(new Status(new BillToDateException(Exception_Message_CalculateNonMeteredServiceCostToDateFailed, ex), StatusTypes.StatusSeverity.Error));
            }

            return (result);
        }

        /// <summary>
        /// Calculate non-metered service cost-to-date.
        /// </summary>
        /// <param name="m"></param>
        /// <param name="s"></param>
        /// <returns></returns>
        private NonMeteredServiceResult MainCalculateNonMeteredServiceCostToDate(NonMeteredService m, BillToDateSettings s)
        {
            NonMeteredServiceResult result = null;
            TimeSpan currSpan;
            TimeSpan projectedSpan;
            double monthProrateFactor = 1.0;
            double currNumDays = 0;
            double projectedNumDays = 0;
            double proratedResultCost = 0.0;
            double avgDailyCost = 0.0;

            // use settings.ProjectedNumberOfDays to set the projected date, but only 
            // if it is currently null; projected number of days is normally derived from startDate and projectedEndDate
            m.InitializeProjectedEndDate(s.Settings.General.ProjectedNumberOfDays);

            // validate the inputs; be sure all dates and name are present
            ValidateProperties(m);

            // calculate day spans from provided dates; endpoints are inclusive
            currSpan = m.EndDate.Value.Subtract(m.StartDate.Value);
            currNumDays = currSpan.TotalDays + 1;
            projectedSpan = m.ProjectedEndDate.Value.Subtract(m.StartDate.Value);
            projectedNumDays = projectedSpan.TotalDays + 1;                         // This SHOULD be same as nmss.projectedNumberOfDays

            if (projectedNumDays > 0)
            {
                // set the currNumber of days to full month (projected num days) when special settings in play
                if ((s.Settings.Validate.CheckDaysInFullMonth && currNumDays >= s.Settings.Validate.DaysInFullMonth) || s.Settings.General.ProrateNonMeteredCharges == false)
                {
                    monthProrateFactor = 1;
                }
                else
                {
                    monthProrateFactor = (currNumDays / projectedNumDays);
                }
            }

            proratedResultCost = m.Amount * monthProrateFactor;

            if (currNumDays > 0)
            {
                avgDailyCost = proratedResultCost / currNumDays;
            }

            result = new NonMeteredServiceResult();
            result.StartDate = m.StartDate.Value;
            result.EndDate = m.EndDate.Value;
            result.ProjectedEndDate = m.ProjectedEndDate.Value;
            result.Cost = proratedResultCost;
            result.ProjectedCost = m.Amount;                    // the projected cost
            result.Usage = m.Usage * monthProrateFactor;
            result.ProjectedUsage = m.Usage;
            result.MonthProrateFactorUsed = monthProrateFactor;
            result.NumberOfDaysInCostToDate = (int)currNumDays;
            result.NumberOfDaysInProjectedCost = (int)projectedNumDays;
            result.AverageDailyCost = avgDailyCost;

            return (result);
        }



        /*
        /// <summary>
        /// GetRateClass list implemented as part of interface.  Some callers may want the rate class list for a rateCompanyID.
        /// This gets a new list every time it is called.  It does not populate the internal rate class list used by other calculations.
        /// </summary>
        /// <param name="rateCompanyID"></param>
        /// <returns></returns>
        public RateClassList GetRateClassList(int rateCompanyID)
        {
            return (_rateModel.GetRateClassList(rateCompanyID));
        }
        */

        /// <summary>
        ///  Get tier boundaries.  Inclusive to first rate definition in range, and season of the enddate.  Returns null if rate class is not tiered, or rate class invalid.
        /// </summary>
        /// <param name="rateCompanyId"></param>
        /// <param name="rateClass"></param>
        /// <param name="startDate"></param>
        /// <param name="endDate"></param>
        /// <returns></returns>
        public List<TierBoundary> GetTierBoundaries(int rateCompanyId, string rateClass, DateTime startDate, DateTime endDate)
        {
            return (_rateModel.GetTierBoundaries(rateCompanyId, rateClass, startDate, endDate));
        }


        /// <summary>
        /// Get information on the rate class.  Today's date is used to get most recent definition.
        /// </summary>
        /// <param name="rateCompanyId"></param>
        /// <param name="rateClass"></param>
        /// <returns></returns>
        public RateClass GetRateClassInformation(int rateCompanyId, string rateClass)
        {
            return (_rateModel.GetRateClassInformation(rateCompanyId, rateClass));
        }


        /// <summary>
        /// Be sure the dates are initialzed in a non-metered service
        /// </summary>
        /// <param name="m"></param>
        private void ValidateProperties(NonMeteredService m)
        {

            if (m.EndDate == null || m.StartDate == null || m.ProjectedEndDate == null)
            {
                throw new ArgumentException(Exception_Message_NonMeteredServiceDatesAreNull);
            }

            if (string.IsNullOrEmpty(m.Name))
            {
                throw new ArgumentException(Exception_Message_NonMeteredServiceNameIsEmpty);
            }

        }

        /// <summary>
        /// Get the customer type of the rate class.  
        /// Get it from the rate class list populated in the calculation manager.
        /// </summary>
        /// <param name="rateCompanyID"></param>
        /// <param name="rateClass"></param>
        /// <returns></returns>
        private Enums.CustomerType GetCustomerType(int rateCompanyID, string rateClass)
        {
            Enums.CustomerType custType;

            try
            {
                if (_rateClassList == null)
                {
                    _rateClassList = _rateModel.GetRateClassList(rateCompanyID);
                }

                var baseRateClass = _rateModel.GetBaseRateClass(rateCompanyID, rateClass);
                
                custType = _rateClassList.FindByRateClassID(baseRateClass).CustomerType;

            }
            catch (Exception ex)
            {
                throw new Exception(Exception_Message_CustomerTypeError, ex);
            }

            return (custType);
        }

        /// <summary>
        /// Derive the prorate factor from some settings provided via arguments.
        /// </summary>
        /// <param name="currentNumDays"></param>
        /// <param name="minDaysForFullMonth"></param>
        /// <param name="projectedNumDays"></param>
        /// <returns></returns>
        private double GetProrateFactor(DateTime startDate, DateTime endDate, BillToDateSettings s)
        {
            double prorateFactor = 0;
            TimeSpan currSpan;
            double currentNumDays;

            currSpan = endDate.Subtract(startDate);
            currentNumDays = currSpan.TotalDays + 1;

            try
            {
                if (s.Settings.Validate.CheckDaysInFullMonth)
                {
                    if ((int)currentNumDays >= s.Settings.Validate.DaysInFullMonth)
                    {
                        prorateFactor = 1;
                    }
                    else
                    {
                        prorateFactor = currentNumDays / s.Settings.General.ProjectedNumberOfDays;
                    }
                }
                else
                {
                    prorateFactor = currentNumDays / s.Settings.General.ProjectedNumberOfDays;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return (prorateFactor);
        }



        #endregion


        #region String Formatting Helpers

        private string ErrorMessageForMeter(string msg, string meterID)
        {
            return (String.Format(ErrorMessageForMeterFormatString, msg, meterID));
        }

        private string WarningMessageForMeter(string msg, string meterID)
        {
            return (String.Format(WarningMessageForMeterFormatString, msg, meterID));
        }

        #endregion


        #region GetRateModel

        private IRateModel GetRateModel()
        {
            IRateModel rateModel;
            RateModelEPFactory RateModelEPFactory = new RateModelEPFactory();

            rateModel = RateModelEPFactory.CreateRateModel();

            return (rateModel);
        }


        private IRateModel GetRateModel(bool logOverride)
        {
            IRateModel rateModel;
            RateModelEPFactory RateModelEPFactory = new RateModelEPFactory();

            rateModel = RateModelEPFactory.CreateRateModel(logOverride);

            return (rateModel);
        }

        #endregion


        #region Tracing

        /// <summary>
        /// Attach informational tracing to StatusList of BillToDateResult object being returned to the caller.
        /// </summary>
        /// <param name="r"></param>
        /// <param name="s"></param>
        private void TraceHelper(BillToDateResult r, BillToDateSettings s)
        {
            var traceOut = Trace(r, s);

            if (traceOut != null)
            {
                r.StatusList.Add(new Status(new BillToDateException(traceOut), StatusTypes.StatusSeverity.Informational));
            }
        }
        /*
        /// <summary>
        /// Trace details of the result object.
        /// </summary>
        /// <param name="result"></param>
        private string Trace(BillToDateResult result)
        {
            return (Trace(result, null));
        }
        */
        /// <summary>
        /// Trace details of result object and settings object.
        /// </summary>
        /// <param name="result"></param>
        /// <param name="s"></param>
        private string Trace(BillToDateResult result, BillToDateSettings btds)
        {
            StringBuilder s = null;
            string traceOut = null;

            try
            {

                if (_enableTracing)
                {
                    s = new StringBuilder();

                    if (result != null)
                    {
                        s.AppendLine(result.GetType().ToString());
                        s.AppendFormat("TotalCost = {0}", result.TotalCost); s.AppendLine();
                        s.AppendFormat("TotalProjectedCost = {0}", result.TotalProjectedCost); s.AppendLine();

                        if (result.StatusList != null)
                        {
                            // iterate status list for errors first
                            foreach (Status item in result.StatusList)
                            {
                                s.AppendFormat("Status = {0}, {1}", item.StatusSeverity.ToString(), item.Exception.Message); s.AppendLine();
                            }
                        }

                        if (result.Bill != null)
                        {
                            if (result.Bill.NonMeteredServices != null)
                            {
                                // iterate non-metered services
                                foreach (NonMeteredService nms in result.Bill.NonMeteredServices)
                                {
                                    s.AppendFormat("NonMeteredService = {0}, {1}, {2}, {3}, {4}", nms.Name, nms.Amount, nms.StartDate, nms.EndDate, nms.ProjectedEndDate); s.AppendLine();
                                    if (nms.CostToDateResult != null)
                                    {
                                        s.AppendFormat("  Cost = {0}", nms.CostToDateResult.Cost); s.AppendLine();
                                        s.AppendFormat("  ProjectedCost = {0}", nms.CostToDateResult.ProjectedCost); s.AppendLine();
                                        s.AppendFormat("  NumberOfDaysInCostToDate = {0}", nms.CostToDateResult.NumberOfDaysInCostToDate); s.AppendLine();
                                        s.AppendFormat("  NumberOfDaysInProjectedCost = {0}", nms.CostToDateResult.NumberOfDaysInProjectedCost); s.AppendLine();

                                        foreach (Status item in nms.CostToDateResult.StatusList)
                                        {
                                            s.AppendFormat("  Status = {0}, {1}", item.StatusSeverity.ToString(), item.Exception.Message); s.AppendLine();
                                        }
                                    }
                                }
                            }

                            if (result.Bill.MeteredServices != null)
                            {
                                // iterate metered services
                                foreach (MeteredService ms in result.Bill.MeteredServices)
                                {
                                    s.AppendFormat("MeteredService = {0}, {1}, {2}", ms.ServiceID, ms.Fuel.ToString(), ms.PremiseID); s.AppendLine();

                                    foreach (Meter m in ms.Meters)
                                    {
                                        s.AppendFormat("  Meter = {0}, {1}, {2}, {3}, {4}", m.MeterID, m.Fuel.ToString(), m.StartDate, m.EndDate, m.ProjectedEndDate); s.AppendLine();
                                        s.AppendFormat("    RateClass = {0}", m.RateClass); s.AppendLine();
                                        s.AppendFormat("    RateClass2 = {0}", m.RateClass2); s.AppendLine();
                                        s.AppendFormat("    RateCompanyID = {0}", m.RateCompanyID); s.AppendLine();
                                        s.AppendFormat("    DerivedFuel = {0}", m.DerivedFuel); s.AppendLine();
                                        s.AppendFormat("    DerivedFuelIsPrimary = {0}", m.DerivedFuelIsPrimary); s.AppendLine();
                                        s.AppendFormat("    PrimaryUnitOfMeasure = {0}", m.PrimaryUnitOfMeasure); s.AppendLine();
                                        s.AppendFormat("    MaximumDerivedUsage = {0}", m.MaximumDerivedUsage); s.AppendLine();
                                        s.AppendFormat("    UsageConversionFactor = {0}", m.UsageConversionFactor); s.AppendLine();

                                        if (m.CostToDateResult != null)
                                        {
                                            s.AppendFormat("    CostToDateResult"); s.AppendLine();
                                            s.AppendFormat("    Cost = {0}", m.CostToDateResult.Cost); s.AppendLine();
                                            s.AppendFormat("    ProjectedCost = {0}", m.CostToDateResult.ProjectedCost); s.AppendLine();
                                            s.AppendFormat("    NumberOfDaysInCostToDate = {0}", m.CostToDateResult.NumberOfDaysInCostToDate); s.AppendLine();
                                            s.AppendFormat("    NumberOfDaysInProjectedCost = {0}", m.CostToDateResult.NumberOfDaysInProjectedCost); s.AppendLine();
                                            s.AppendFormat("    MinimumCostsApplied = {0}", m.CostToDateResult.MinimumCostsApplied); s.AppendLine();
                                            s.AppendFormat("    MonthProrateFactorUsed = {0}", m.CostToDateResult.MonthProrateFactorUsed); s.AppendLine();
                                            s.AppendFormat("    UsageScaleFactorUsed = {0}", m.CostToDateResult.UsageScaleFactorUsed); s.AppendLine();
                                            s.AppendFormat("    StartDate = {0}", m.CostToDateResult.StartDate); s.AppendLine();
                                            s.AppendFormat("    EndDate = {0}", m.CostToDateResult.EndDate); s.AppendLine();
                                            s.AppendFormat("    ProjectedEndDate = {0}", m.CostToDateResult.ProjectedEndDate); s.AppendLine();
                                            s.AppendFormat("    LastReadingDate = {0}", m.CostToDateResult.LastReadingDate); s.AppendLine();
                                            s.AppendFormat("    ErrCode = {0}", m.CostToDateResult.ErrCode.ToString()); s.AppendLine();
                                            s.AppendFormat("    ProjectedCostErrCode = {0}", m.CostToDateResult.ProjectedCostErrCode.ToString()); s.AppendLine();
                                            if (m.CostToDateResult.Usages != null)
                                            {
                                                foreach (Usage u in m.CostToDateResult.Usages)
                                                {
                                                    s.AppendFormat("    Usage = {0}, {1} {2} {3}", u.Quantity, u.BaseOrTier.ToString(), u.TimeOfUse.ToString(), u.Season.ToString()); s.AppendLine();
                                                }
                                            }
                                            if (m.CostToDateResult.ProjectedUsages != null)
                                            {
                                                foreach (Usage u in m.CostToDateResult.ProjectedUsages)
                                                {
                                                    s.AppendFormat("    ProjUsage = {0}, {1} {2} {3}", u.Quantity, u.BaseOrTier.ToString(), u.TimeOfUse.ToString(), u.Season.ToString()); s.AppendLine();
                                                }
                                            }
                                            if (m.CostToDateResult.PeakTimeRebates != null)
                                            {
                                                s.AppendFormat("    PeakTimeRebates"); s.AppendLine();
                                                s.AppendFormat("      OverallAccountingRule = {0}", m.CostToDateResult.PeakTimeRebates.OverallAccountingRule); s.AppendLine();
                                                if (m.CostToDateResult.PeakTimeRebates.OverallAccountingRule == Enums.RebateAccountingRule.EntireBillPeriod)
                                                {
                                                    s.AppendFormat("      RebateAmount = {0}", m.CostToDateResult.PeakTimeRebates.RebateAmount); s.AppendLine();
                                                }
                                                foreach (var p in m.CostToDateResult.PeakTimeRebates.PeakTimeRebateEvents)
                                                {
                                                    s.AppendFormat("        PTREvent = {0}, {1}, {2}", p.StartDate, p.EndDate, p.BaselineRule); s.AppendLine();
                                                    s.AppendFormat("          Rebate"); s.AppendLine();
                                                    s.AppendFormat("          RebateAmount = {0}", p.Rebate.RebateAmount); s.AppendLine();
                                                    s.AppendFormat("          ActualUsage = {0}", p.Rebate.ActualUsage); s.AppendLine();
                                                    s.AppendFormat("          ActualUsageCalculated = {0}", p.Rebate.ActualUsageCalculated); s.AppendLine();
                                                    s.AppendFormat("          ActualUsageProvided = {0}", p.Rebate.ActualUsageProvided); s.AppendLine();
                                                    s.AppendFormat("          BaselineCalculated = {0}", p.Rebate.BaselineCalculated); s.AppendLine();
                                                    s.AppendFormat("          BaselineUsage = {0}", p.Rebate.BaselineUsage); s.AppendLine();
                                                    s.AppendFormat("          BaselineUsageEmpty = {0}", p.Rebate.BaselineUsageEmpty); s.AppendLine();
                                                    s.AppendFormat("          MissingActualUsage = {0}", p.Rebate.MissingActualUsage); s.AppendLine();
                                                    s.AppendFormat("          RealSavingsUsage = {0}", p.Rebate.RealSavingsUsage); s.AppendLine();
                                                    s.AppendFormat("          RebateAccountingRule = {0}", p.Rebate.RebateAccountingRule); s.AppendLine();
                                                    s.AppendFormat("          RebateAmountCalculated = {0}", p.Rebate.RebateAmountCalculated); s.AppendLine();
                                                    s.AppendFormat("          RebateAmountProvided = {0}", p.Rebate.RebateAmountProvided); s.AppendLine();
                                                    s.AppendFormat("          SavingsUsage = {0}", p.Rebate.SavingsUsage); s.AppendLine();
                                                    s.AppendFormat("          UnitPrice = {0}", p.Rebate.UnitPrice); s.AppendLine();
                                                    s.AppendFormat("          Valid = {0}", p.Rebate.Valid); s.AppendLine();
                                                }
                                            }
                                            if (m.CostToDateResult.ProjectedPeakTimeRebates != null)
                                            {
                                                s.AppendFormat("    ProjectedPeakTimeRebates"); s.AppendLine();
                                                s.AppendFormat("      OverallAccountingRule = {0}", m.CostToDateResult.ProjectedPeakTimeRebates.OverallAccountingRule); s.AppendLine();
                                                if (m.CostToDateResult.ProjectedPeakTimeRebates.OverallAccountingRule == Enums.RebateAccountingRule.EntireBillPeriod)
                                                {
                                                    s.AppendFormat("      RebateAmount = {0}", m.CostToDateResult.ProjectedPeakTimeRebates.RebateAmount); s.AppendLine();
                                                }
                                                foreach (var p in m.CostToDateResult.ProjectedPeakTimeRebates.PeakTimeRebateEvents)
                                                {
                                                    s.AppendFormat("        PTREvent = {0}, {1}, {2}", p.StartDate, p.EndDate, p.BaselineRule); s.AppendLine();
                                                    s.AppendFormat("          Rebate"); s.AppendLine();
                                                    s.AppendFormat("          RebateAccountingRule = {0}", p.Rebate.RebateAccountingRule); s.AppendLine();
                                                    s.AppendFormat("          RebateAmount = {0}", p.Rebate.RebateAmount); s.AppendLine();
                                                    s.AppendFormat("          ActualUsage = {0}", p.Rebate.ActualUsage); s.AppendLine();
                                                    s.AppendFormat("          ActualUsageCalculated = {0}", p.Rebate.ActualUsageCalculated); s.AppendLine();
                                                    s.AppendFormat("          ActualUsageProvided = {0}", p.Rebate.ActualUsageProvided); s.AppendLine();
                                                    s.AppendFormat("          BaselineCalculated = {0}", p.Rebate.BaselineCalculated); s.AppendLine();
                                                    s.AppendFormat("          BaselineUsage = {0}", p.Rebate.BaselineUsage); s.AppendLine();
                                                    s.AppendFormat("          BaselineUsageEmpty = {0}", p.Rebate.BaselineUsageEmpty); s.AppendLine();
                                                    s.AppendFormat("          MissingActualUsage = {0}", p.Rebate.MissingActualUsage); s.AppendLine();
                                                    s.AppendFormat("          RealSavingsUsage = {0}", p.Rebate.RealSavingsUsage); s.AppendLine();

                                                    s.AppendFormat("          RebateAmountCalculated = {0}", p.Rebate.RebateAmountCalculated); s.AppendLine();
                                                    s.AppendFormat("          RebateAmountProvided = {0}", p.Rebate.RebateAmountProvided); s.AppendLine();
                                                    s.AppendFormat("          SavingsUsage = {0}", p.Rebate.SavingsUsage); s.AppendLine();
                                                    s.AppendFormat("          UnitPrice = {0}", p.Rebate.UnitPrice); s.AppendLine();
                                                    s.AppendFormat("          Valid = {0}", p.Rebate.Valid); s.AppendLine();
                                                }
                                            }
                                        }

                                    }
                                }
                            }

                        }

                        if (btds != null)
                        {
                            s.AppendFormat("BillToDateSettings"); s.AppendLine();
                            s.AppendFormat("General"); s.AppendLine();
                            s.AppendFormat("ProrateMonthlyServiceCharges = {0}", btds.Settings.General.ProrateMonthlyServiceCharges); s.AppendLine();
                            s.AppendFormat("MinimumChargeType = {0}", btds.Settings.General.MinimumChargeType.ToString()); s.AppendLine();
                            s.AppendFormat("ProjectedNumberOfDays = {0}", btds.Settings.General.ProjectedNumberOfDays); s.AppendLine();
                            s.AppendFormat("AllowMultipleMetersPerService = {0}", btds.Settings.General.AllowMultipleMetersPerService); s.AppendLine();
                            s.AppendFormat("UseConversionFactorForGas = {0}", btds.Settings.General.UseConversionFactorForGas); s.AppendLine();
                            s.AppendFormat("ConversionFactorForGas = {0}", btds.Settings.General.ConversionFactorForGas); s.AppendLine();
                            s.AppendFormat("UseConversionFactorForWater = {0}", btds.Settings.General.UseConversionFactorForWater); s.AppendLine();
                            s.AppendFormat("ConversionFactorForWater = {0}", btds.Settings.General.ConversionFactorForWater); s.AppendLine();
                            s.AppendFormat("ProrateDemandUsageDeterminants = {0}", btds.Settings.General.ProrateDemandUsageDeterminants); s.AppendLine();
                            s.AppendFormat("SmoothTiers = {0}", btds.Settings.General.SmoothTiers); s.AppendLine();
                            s.AppendFormat("AllowRebateCalculations = {0}", btds.Settings.General.AllowRebateCalculations); s.AppendLine();
                            s.AppendFormat("AllowBaselineCalculations = {0}", btds.Settings.General.AllowBaselineCalculations); s.AppendLine();
                            s.AppendFormat("ProrateNonMeteredCharges = {0}", btds.Settings.General.ProrateNonMeteredCharges); s.AppendLine();
                            s.AppendFormat("DailyTierBillDaysType = {0}", btds.Settings.General.DailyTierBillDaysType); s.AppendLine();
                            s.AppendFormat("Validate"); s.AppendLine();
                            s.AppendFormat("CheckDaysInFullMonth = {0}", btds.Settings.Validate.CheckDaysInFullMonth); s.AppendLine();
                            s.AppendFormat("CheckMaximumMissingDays = {0}", btds.Settings.Validate.CheckMaximumMissingDays); s.AppendLine();
                            s.AppendFormat("CheckMinimumBillDays = {0}", btds.Settings.Validate.CheckMinimumBillDays); s.AppendLine();
                            s.AppendFormat("CheckMinimumUsage = {0}", btds.Settings.Validate.CheckMinimumUsage); s.AppendLine();
                            s.AppendFormat("DaysInFullMonth = {0}", btds.Settings.Validate.DaysInFullMonth); s.AppendLine();
                            s.AppendFormat("MaximumMissingDays = {0}", btds.Settings.Validate.MaximumMissingDays); s.AppendLine();
                            s.AppendFormat("MinimumUsage = {0}", btds.Settings.Validate.MinimumUsage); s.AppendLine();
                            s.AppendFormat("MinimumBillDaysElectric = {0}", btds.Settings.Validate.MinimumBillDaysElectric); s.AppendLine();
                            s.AppendFormat("MinimumBillDaysGas = {0}", btds.Settings.Validate.MinimumBillDaysGas); s.AppendLine();
                            s.AppendFormat("MinimumBillDaysWater = {0}", btds.Settings.Validate.MinimumBillDaysWater); s.AppendLine();
                            s.AppendFormat("Sewer"); s.AppendLine();
                            s.AppendFormat("ProrateSewerMaximumMonthsUsageDeterminants = {0}", btds.Settings.Sewer.ProrateSewerMaximumMonthsUsageDeterminants); s.AppendLine();
                            s.AppendFormat("SewerMaximumMonthsUsageForCommercial = {0}", btds.Settings.Sewer.SewerMaximumMonthsUsageForCommercial); s.AppendLine();
                            s.AppendFormat("SewerMaximumMonthsUsageForResidential = {0}", btds.Settings.Sewer.SewerMaximumMonthsUsageForResidential); s.AppendLine();
                            s.AppendFormat("UseSewerMaximumMonthsUsageForCommercial = {0}", btds.Settings.Sewer.UseSewerMaximumMonthsUsageForCommercial); s.AppendLine();
                            s.AppendFormat("UseSewerMaximumMonthsUsageForResidential = {0}", btds.Settings.Sewer.UseSewerMaximumMonthsUsageForResidential); s.AppendLine();
                        }

                        traceOut = s.ToString();
                        Trace(traceOut);
                    }

                }
            }
            catch
            {
                // If trace message cannot be created, simply do nothing, since the application
                // should not stop because of a trace.
            }

            return (traceOut);
        }

        /// <summary>
        /// Create an event log trace listener if required, and write trace message when tracing enabled.
        /// </summary>
        /// <param name="msg"></param>
        private void Trace(string msg)
        {
            try
            {
                if (_enableTracing)
                {

                    if (_primaryTraceListener == null)
                    {
                        // InstallUtil will normally initialize
                        if (!EventLog.SourceExists(Trace_Source))
                        {
                            EventLog.CreateEventSource(Trace_Source, Trace_Source_Log);
                        }

                        _primaryTraceListener = new EventLogTraceListener(Trace_Source);
                        System.Diagnostics.Trace.Listeners.Add(_primaryTraceListener);
                    }

                    System.Diagnostics.Trace.WriteLine(msg);
                    System.Diagnostics.Trace.Flush();

                }
            }
            catch
            {
                // If trace cannot be written, simply do nothing, since the application
                // should not stop because of a trace.  I failure, most likely because the event log 
                // has not been initialized using "InstallUtil CE.BillToDate.dll"
            }
        }


        /// <summary>
        /// Load app settings used by the calculation manager.  
        /// Other app settings may be loaded by rate model interface as well.
        /// </summary>
        private void LoadConfiguration()
        {

            try
            {
                _enableTracing = System.Convert.ToBoolean(ConfigurationManager.AppSettings[AppSettings_BillToDateTracing]);
            }
            catch
            {
                // do not stop application if tracing boolean cannot be read.
                _enableTracing = false;
            }

        }

        #endregion

        public List<string> GetLoggingInfo(string logSessionId)
        {
            return _rateModel.GetLogHistory(new Guid(logSessionId));
        }


        #region Test XML
        /*
        /// <summary>
        /// Generate XML to use in the Rate Engine testing tool.
        /// </summary>
        /// <param name="bill"></param>
        /// <param name="btds"></param>
        /// <returns></returns>
        public string GenerateTestData(Bill bill, BillToDateSettings btds)
        {
            StringBuilder s = null;
            string xml = string.Empty;
            string formattedXml = string.Empty;

            try
            {
                s = new StringBuilder();

                if (bill != null)
                {
                    var notes = string.Empty;

                    if (!string.IsNullOrEmpty(bill.CustomerID))
                    {
                        notes = "For customer " + bill.CustomerID;
                        if (!string.IsNullOrEmpty(bill.AccountID))
                        {
                            notes = notes + " | For account " + bill.AccountID;
                        }
                    }

                    s.AppendLine("<BillToDate  xmlns=\"Aclara:BillToDateTesting\"");
                    s.AppendFormat(" Notes=\"{0}\"", notes); s.AppendLine();
                    s.AppendFormat(" RateCompanyID=\"{0}\"", bill.RateCompanyID); s.AppendLine();
                    s.AppendFormat(" ReferrerID=\"{0}\"", bill.ReferrerID); s.AppendLine();

                    if (bill.Datestamp != null && bill.Datestamp != DateTime.MinValue && bill.Datestamp != DateTime.MaxValue)
                    {
                        s.AppendFormat(" StartDate=\"{0}\"", bill.Datestamp.ToString("MM-dd-yyyy"));
                    }

                    s.AppendLine(">");
                    s.AppendLine();

                    if (bill.NonMeteredServices != null && bill.NonMeteredServices.Count > 0)
                    {
                        s.AppendLine("<NonMeteredServices>");

                        // iterate non-metered services
                        foreach (NonMeteredService nms in bill.NonMeteredServices)
                        {
                            s.AppendFormat("<NonMeteredService Name=\"{0}\" Amount=\"{1}\"", nms.Name, nms.Amount.ToString("#.00"));

                            if (nms.StartDate != null)
                            {
                                s.AppendFormat(" StartDate=\"{0}\"", nms.StartDate.Value.ToString("MM-dd-yyyy"));
                            }

                            if (nms.EndDate != null)
                            {
                                s.AppendFormat(" EndDate=\"{0}\"", nms.EndDate.Value.ToString("MM-dd-yyyy"));
                            }

                            if (nms.ProjectedEndDate != null)
                            {
                                s.AppendFormat(" ProjectedEndDate=\"{0}\"", nms.ProjectedEndDate.Value.ToString("MM-dd-yyyy"));
                            }

                            s.Append("/>");
                        }

                        s.AppendLine("</NonMeteredServices>");
                    }


                    if (bill.MeteredServices != null && bill.MeteredServices.Count > 0)
                    {
                        s.AppendLine("<MeteredServices>");

                        // iterate metered services
                        foreach (MeteredService ms in bill.MeteredServices)
                        {
                            // the metered service opening element is in meter since we want to fuel to include

                            foreach (Meter m in ms.Meters)
                            {

                                s.AppendFormat("<MeteredService Fuel=\"{0}\" ServiceID=\"{1}\" PremiseID=\"{2}\">", m.Fuel.ToString(), ms.ServiceID, ms.PremiseID); // <--- Note the FUEL is from meter here!

                                s.AppendFormat("<Meter MeterID=\"{0}\" RateClass=\"{1}\" StartDate=\"{2}\" EndDate=\"{3}\" ProjectedEndDate=\"{4}\"",
                                    m.MeterID, m.RateClass, m.StartDate.Value.ToString("MM-dd-yyyy"), m.EndDate.Value.ToString("MM-dd-yyyy"), m.ProjectedEndDate.Value.ToString("MM-dd-yyyy"));

                                if (!string.IsNullOrEmpty(m.RateClass2))
                                {
                                    s.AppendFormat(" SewerRateClass=\"{0}\"", m.RateClass2);

                                    if (m.MaximumDerivedUsage > 0)
                                    {
                                        s.AppendFormat(" SewerMaxDerivedUsage=\"{0}\"", m.MaximumDerivedUsage);
                                    }
                                }

                                s.Append(">");

                                if (m.Baselines != null && m.Baselines.Count > 0)
                                {
                                    s.AppendLine("<Baselines>");

                                    foreach (Baseline b in m.Baselines)
                                    {
                                        s.AppendFormat("<Baseline UsageDate=\"{0}\"", b.UsageDate.ToString("yyyy-MM-dd"));

                                        if (!b.BaselineUsageMissing)
                                        {
                                            s.AppendFormat(" BaselineUsage=\"{0}\"", b.BaselineUsage);
                                        }

                                        if (!b.ActualUsageMissing)
                                        {
                                            s.AppendFormat(" ActualUsage=\"{0}\"", b.ActualUsage);
                                        }

                                        if (!b.RebateAmountMissing)
                                        {
                                            s.AppendFormat(" RebateAmount=\"{0}\"", b.RebateAmount);
                                        }

                                        s.Append("/>");

                                    }

                                    s.AppendLine("</Baselines>");
                                }

                                if (m.Readings != null && m.Readings.Count > 0)
                                {
                                    s.AppendLine("<Readings>");

                                    foreach (Reading r in m.Readings)
                                    {
                                        s.AppendFormat("<Reading IntEnd=\"{0}\" EditCode=\"{1}\" TOUBin=\"{2}\" Read=\"{3}\"/>",
                                            r.Timestamp.ToString("yyyy-MM-ddTHH:mm:ss"), "Direct", "NoTOU", r.Quantity);
                                    }

                                    s.AppendLine("</Readings>");
                                }

                                s.AppendLine("</Meter>");


                            }

                            s.AppendLine("</MeteredService>");
                        }

                        s.AppendLine("</MeteredServices>");
                    }

                    if (btds != null)
                    {
                        s.AppendLine("<Settings");

                        s.AppendFormat(" ProrateMonthlyServiceCharges=\"{0}\"", btds.Settings.General.ProrateMonthlyServiceCharges); s.AppendLine();

                        s.AppendFormat(" MinimumChargeType=\"{0}\"", btds.Settings.General.MinimumChargeType.ToString()); s.AppendLine();
                        s.AppendFormat(" ProjectedNumberOfDays=\"{0}\"", btds.Settings.General.ProjectedNumberOfDays); s.AppendLine();
                        s.AppendFormat(" AllowMultipleMetersPerService=\"{0}\"", btds.Settings.General.AllowMultipleMetersPerService); s.AppendLine();
                        s.AppendFormat(" UseConversionFactorForGas=\"{0}\"", btds.Settings.General.UseConversionFactorForGas); s.AppendLine();
                        s.AppendFormat(" ConversionFactorForGas=\"{0}\"", btds.Settings.General.ConversionFactorForGas); s.AppendLine();
                        s.AppendFormat(" UseConversionFactorForWater=\"{0}\"", btds.Settings.General.UseConversionFactorForWater); s.AppendLine();
                        s.AppendFormat(" ConversionFactorForWater=\"{0}\"", btds.Settings.General.ConversionFactorForWater); s.AppendLine();
                        s.AppendFormat(" ProrateDemandUsageDeterminants=\"{0}\"", btds.Settings.General.ProrateDemandUsageDeterminants); s.AppendLine();
                        s.AppendFormat(" AllowRebateCalculations=\"{0}\"", btds.Settings.General.AllowRebateCalculations); s.AppendLine();
                        s.AppendFormat(" AllowBaselineCalculations=\"{0}\"", btds.Settings.General.AllowBaselineCalculations); s.AppendLine();
                        s.AppendFormat(" ProrateNonMeteredCharges=\"{0}\"", btds.Settings.General.ProrateNonMeteredCharges); s.AppendLine();
                        s.AppendFormat(" DailyTierBillDaysType=\"{0}\"", btds.Settings.General.DailyTierBillDaysType); s.AppendLine();

                        s.AppendFormat(" CheckDaysInFullMonth=\"{0}\"", btds.Settings.Validate.CheckDaysInFullMonth); s.AppendLine();
                        s.AppendFormat(" CheckMaximumMissingDays=\"{0}\"", btds.Settings.Validate.CheckMaximumMissingDays); s.AppendLine();
                        s.AppendFormat(" CheckMinimumBillDays=\"{0}\"", btds.Settings.Validate.CheckMinimumBillDays); s.AppendLine();
                        s.AppendFormat(" CheckMinimumUsage=\"{0}\"", btds.Settings.Validate.CheckMinimumUsage); s.AppendLine();
                        s.AppendFormat(" DaysInFullMonth=\"{0}\"", btds.Settings.Validate.DaysInFullMonth); s.AppendLine();
                        s.AppendFormat(" MaximumMissingDays=\"{0}\"", btds.Settings.Validate.MaximumMissingDays); s.AppendLine();
                        s.AppendFormat(" MinimumUsage=\"{0}\"", btds.Settings.Validate.MinimumUsage); s.AppendLine();
                        s.AppendFormat(" MinimumBillDaysElectric=\"{0}\"", btds.Settings.Validate.MinimumBillDaysElectric); s.AppendLine();
                        s.AppendFormat(" MinimumBillDaysGas=\"{0}\"", btds.Settings.Validate.MinimumBillDaysGas); s.AppendLine();
                        s.AppendFormat(" MinimumBillDaysWater=\"{0}\"", btds.Settings.Validate.MinimumBillDaysWater); s.AppendLine();

                        s.AppendFormat(" ProrateSewerMaximumMonthsUsageDeterminants=\"{0}\"", btds.Settings.Sewer.ProrateSewerMaximumMonthsUsageDeterminants); s.AppendLine();
                        s.AppendFormat(" SewerMaximumMonthsUsageForCommercial=\"{0}\"", btds.Settings.Sewer.SewerMaximumMonthsUsageForCommercial); s.AppendLine();
                        s.AppendFormat(" SewerMaximumMonthsUsageForResidential=\"{0}\"", btds.Settings.Sewer.SewerMaximumMonthsUsageForResidential); s.AppendLine();
                        s.AppendFormat(" UseSewerMaximumMonthsUsageForCommercial=\"{0}\"", btds.Settings.Sewer.UseSewerMaximumMonthsUsageForCommercial); s.AppendLine();
                        s.AppendFormat(" UseSewerMaximumMonthsUsageForResidential=\"{0}\"", btds.Settings.Sewer.UseSewerMaximumMonthsUsageForResidential); s.AppendLine();

                        s.AppendLine("/>");
                    }



                    s.AppendLine("</BillToDate>");

                    xml = s.ToString();
                    formattedXml = XElement.Parse(xml).ToString();

                }



            }
            catch (Exception ex)
            {
                if (!string.IsNullOrEmpty(ex.Message))
                {
                    formattedXml = ex.Message;
                }
            }

            return (formattedXml);
        }
        */
        #endregion

    }
}
