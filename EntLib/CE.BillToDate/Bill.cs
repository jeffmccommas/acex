﻿using System;

namespace CE.BillToDate
{
    public class Bill
    {
        public Bill()
        {
            ReferrerID = -1;
            ClientID = -1;
            MeteredServices = new MeteredServiceList();
            NonMeteredServices = new NonMeteredServiceList();
         }

        public Bill(DateTime datestamp, int referrerId, string customerId, string accountId, int rateCompanyId)
            : this()
        {
            Datestamp = datestamp;
            ReferrerID = referrerId;
            CustomerID = customerId;
            AccountID = accountId;
            RateCompanyID = rateCompanyId;
        }

        public Bill(int clientId, DateTime datestamp, string customerId, string accountId, int rateCompanyId)
            : this()
        {
            Datestamp = datestamp;
            ClientID = clientId;
            CustomerID = customerId;
            AccountID = accountId;
            RateCompanyID = rateCompanyId;
        }

        public Bill(DateTime datestamp, string customerId, string accountId, int rateCompanyId, NonMeteredServiceList nonMeteredServiceList, MeteredServiceList meteredServiceList)
        {
            ReferrerID = -1;
            ClientID = -1; 
            NonMeteredServices = nonMeteredServiceList ?? new NonMeteredServiceList();
            MeteredServices = meteredServiceList ?? new MeteredServiceList();      
            Datestamp = datestamp;
            CustomerID = customerId;
            AccountID = accountId;
            RateCompanyID = rateCompanyId;
        }

        #region Properties

        public DateTime Datestamp { get; set; }

        public int ReferrerID { get; set; }

        public int ClientID { get; set; }

        public int RateCompanyID { get; set; }

        public string CustomerID { get; set; }

        public string AccountID { get; set; }

        public MeteredServiceList MeteredServices { get; set; }

        public NonMeteredServiceList NonMeteredServices { get; set; }

        #endregion

        /// <summary>
        /// Returns a copy of all members.  Lists are created, but they are new and empty.
        /// </summary>
        /// <returns></returns>
        public Bill Copy()
        {
            var result = new Bill
            {
                ReferrerID = ReferrerID,
                ClientID = ClientID,
                RateCompanyID = RateCompanyID,
                CustomerID = CustomerID,
                AccountID = AccountID,
                Datestamp = Datestamp
            };

            return result;
        }
    }
}
