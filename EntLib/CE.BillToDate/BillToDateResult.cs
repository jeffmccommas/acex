﻿
namespace CE.BillToDate
{
    public class BillToDateResult
    {
        private Bill _bill = null;
        private double _totalCost;
        private double _totalProjectedCost;
        private StatusList _statusList;

        public BillToDateResult()
        {
            _statusList = new StatusList();
        }

        #region Properties

        public Bill Bill
        {
            get { return _bill; }
            set { _bill = value; }
        }

        public double TotalCost
        {
            get { return _totalCost; }
            set { _totalCost = value; }
        }

        public double TotalProjectedCost
        {
            get { return _totalProjectedCost; }
            set { _totalProjectedCost = value; }
        }

        public StatusList StatusList
        {
            get { return _statusList; }
            set { _statusList = value; }
        }

        #endregion

    }
}
