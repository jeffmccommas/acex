﻿
namespace CE.ContentModel
{
    public interface IContentModelFactory
    {
        /// <summary>
        /// Override
        /// </summary>
        /// <param name="clientId"></param>
        /// <returns></returns>
        IContentProvider CreateContentProvider(int clientId);

        /// <summary>
        /// Override
        /// </summary>
        /// <param name="clientId"></param>
        /// <param name="type"></param>
        /// <returns></returns>
        IContentProvider CreateContentProvider(int clientId, ContentProviderType type);

        /// <summary>
        /// Override
        /// </summary>
        /// <returns></returns>
        IContentManager CreateContentManager(int clientId);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="insightsMetaDataConnectionString"></param>
        /// <param name="clientId"></param>
        /// <returns></returns>
        IContentRepository CreateContentRepository(string insightsMetaDataConnectionString, int clientId);
    }
}
