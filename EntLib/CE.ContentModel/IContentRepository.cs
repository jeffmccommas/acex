﻿using System.Collections.Generic;
using System.Data;
using CE.ContentModel.Entities;

namespace CE.ContentModel
{
    public interface IContentRepository
    {
        #region Get Content Methods

        /// <summary>
        /// Gets Action Content in Insights Metadata database
        /// </summary>
        /// <returns></returns>
        List<ClientAction> GetContentClientActions();

        /// <summary>
        /// Gets ActionSavings Content in Insights Metadata database
        /// </summary>
        /// <returns></returns>
        List<ClientActionSaving> GetContentClientActionSavings();

        /// <summary>
        /// Gets Appliance Content in Insights Metadata database
        /// </summary>
        /// <returns></returns>
        List<ClientAppliance> GetContentClientAppliances();

        /// <summary>
        /// Gets BenchmarkGroup Content in Insights Metadata database
        /// </summary>
        /// <returns></returns>
        List<ClientBenchmarkGroup> GetContentClientBenchMarkGroups();

        /// <summary>
        /// Gets Commodity Content in Insights Metadata database
        /// </summary>
        /// <returns></returns>
        List<ClientCommodity> GetContentClientCommodities();

        /// <summary>
        /// Gets Condition Content in Insights Metadata database
        /// </summary>
        /// <returns></returns>
        List<ClientCondition> GetContentClientConditions();

        /// <summary>
        /// Gets Configuration Content in Insights Metadata database
        /// </summary>
        /// <returns></returns>
        List<ClientConfiguration> GetContentClientConfigurations();

        /// <summary>
        /// Gets System Configuration Bulk Content (json format) in Insights Metadata database
        /// </summary>
        /// <returns></returns>
        List<PortalModuleSystemConfigurationBulk> GetPortalModuleSystemConfigurationBulks();

        /// <summary>
        /// Gets Client Configuration Bulk Content (json format) in Insights Metadata database
        /// TFS 598 Aug 2015
        /// </summary>
        /// <returns></returns>
        List<ClientConfigurationBulk> GetContentClientConfigurationBulks();

        /// <summary>
        /// Gets Currency Content in Insights Metadata database
        /// </summary>
        /// <returns></returns>
        List<ClientCurrency> GetContentClientCurrencies();

        /// <summary>
        /// Gets EndUse Content in Insights Metadata database
        /// </summary>
        /// <returns></returns>
        List<ClientEndUse> GetContentClientEnduses();

        /// <summary>
        /// Gets Enumeration Content in Insights Metadata database
        /// </summary>
        /// <returns></returns>
        List<ClientEnumeration> GetContentClientEnumerations();

        /// <summary>
        /// Gets Expression Content in Insights Metadata database
        /// </summary>
        /// <returns></returns>
        List<ClientExpression> GetContentClientExpressions();

        /// <summary>
        /// Gets FileContent Content in Insights Metadata database
        /// </summary>
        /// <returns></returns>
        List<ClientFileContent> GetContentClientFileContentList();

        /// <summary>
        /// Gets Layout Content in Insights Metadata database
        /// </summary>
        /// <returns></returns>
        List<ClientLayout> GetContentClientLayouts();
        /// <summary>
        /// Gets Measurement Content in Insights Metadata database
        /// </summary>
        /// <returns></returns>
        List<ClientMeasurement> GetContentClientMeasurements();

        /// <summary>
        /// Gets Profile Attribute Content in Insights Metadata database
        /// </summary>
        /// <returns></returns>
        List<ClientProfileAttribute> GetContentClientProfileAttributes();

        /// <summary>
        /// Gets ProfileDefaults Content in Insights Metadata database
        /// </summary>
        /// <returns></returns>
        List<ClientProfileDefault> GetContentClientProfileDefaults(string profileDefaultCollectionName);

        /// <summary>
        /// Gets ProfileDefaultCollection Content in Insights Metadata database
        /// </summary>
        /// <returns></returns>
        List<ClientProfileDefaultCollection> GetContentClientProfileDefaultCollections();

        /// <summary>
        /// Gets ProfileOptions Content in Insights Metadata database
        /// </summary>
        /// <returns></returns>
        List<ClientProfileOption> GetContentClientProfileOptions();

        /// <summary>
        /// Gets Profile Section Content in Insights Metadata database
        /// </summary>
        /// <returns></returns>
        List<ClientProfileSection> GetContentClientProfileSection();

        /// <summary>
        /// Gets Season Content in Insights Metadata database
        /// </summary>
        /// <returns></returns>
        List<ClientSeason> GetContentClientSeasons();

        /// <summary>
        /// Gets Tab Content in Insights Metadata database
        /// </summary>
        /// <returns></returns>
        List<ClientTab> GetContentClientTabs();

        /// <summary>
        /// Gets TextContent in Insights Metadata database
        /// </summary>
        /// <returns></returns>
        List<ClientTextContent> GetContentClientTextContentList(string locale);

        /// <summary>
        /// Gets UOM Content in Insights Metadata database
        /// </summary>
        /// <returns></returns>
        List<ClientUOM> GetContentClientUoms();

        /// <summary>
        /// Gets WhatIfData Content in Insights Metadata database
        /// </summary>
        /// <returns></returns>
        List<ClientWhatIfData> GetContentClientWhatIfData();

        /// <summary>
        /// Gets Widget Content in Insights Metadata database
        /// </summary>
        /// <returns></returns>
        List<ClientWidget> GetContentClientWidgets();
        #endregion

        #region Update Content Methods

        /// <summary>
        /// Updates Action in Insights Metadata database
        /// </summary>
        /// <param name="contentTable"></param>
        /// <returns></returns>
        int UpdateAction(DataTable contentTable);

        /// <summary>
        /// Updates Appliance in Insights Metadata database
        /// </summary>
        /// <param name="contentTable"></param>
        /// <returns></returns>
        int UpdateAppliance(DataTable contentTable);

        /// <summary>
        /// Updates Asset in Insights Metadata database
        /// </summary>
        /// <param name="contentTable"></param>
        /// <returns></returns>
        int UpdateAsset(DataTable contentTable);

        /// <summary>
        /// Updates BenchmarkGroup Content in Insights Metadata database
        /// </summary>
        /// <param name="contentTable"></param>
        /// <returns></returns>
        int UpdateBenchmarkGroup(DataTable contentTable);

        /// <summary>
        /// Updates Commodity Content in Insights Metadata database
        /// </summary>
        /// <param name="contentTable"></param>
        /// <returns></returns>
        int UpdateCommodity(DataTable contentTable);

        /// <summary>
        /// Updates Condition in Insights Metadata database
        /// </summary>
        /// <param name="contentTable"></param>
        /// <returns></returns>
        int UpdateCondition(DataTable contentTable);

        /// <summary>
        /// Updates Configuration Bulk (Json format) in Insights Metadata database
        /// TFS 598 Aug 2015
        /// </summary>
        /// <param name="contentTable"></param>
        /// <returns></returns>
        int UpdateConfigurationBulk(DataTable contentTable);

        /// <summary>
        /// Updates Configuration in Insights Metadata database
        /// </summary>
        /// <param name="contentTable"></param>
        /// <returns></returns>
        int UpdateConfiguration(DataTable contentTable);

        /// <summary>
        /// Updates Currency in Insights Metadata database
        /// </summary>
        /// <param name="contentTable"></param>
        /// <returns></returns>
        int UpdateCurrency(DataTable contentTable);

        /// <summary>
        /// Updates Enduse in Insights Metadata database
        /// </summary>
        /// <param name="contentTable"></param>
        /// <returns></returns>
        int UpdateEndUse(DataTable contentTable);

        /// <summary>
        /// Updates Enumeration in Insights Metadata database
        /// </summary>
        /// <param name="contentTable"></param>
        /// <returns></returns>
        int UpdateEnumeration(DataTable contentTable);

        /// <summary>
        /// Updates Enumeration Item in Insights Metadata database
        /// </summary>
        /// <param name="contentTable"></param>
        /// <returns></returns>
        int UpdateEnumerationItem(DataTable contentTable);

        /// <summary>
        /// Updates Expression in Insights Metadata database
        /// </summary>
        /// <param name="contentTable"></param>
        /// <returns></returns>
        int UpdateExpression(DataTable contentTable);

        /// <summary>
        /// Updates FileContent in Insights Metadata database
        /// </summary>
        /// <param name="contentTable"></param>
        /// <returns></returns>
        int UpdateFileContent(DataTable contentTable);

        /// <summary>
        /// Updates Layout in Insights Metadata database
        /// </summary>
        /// <param name="contentTable"></param>
        /// <returns></returns>
        int UpdateLayout(DataTable contentTable);

        /// <summary>
        /// Updates Season in Insights Metadata database
        /// </summary>
        /// <param name="contentTable"></param>
        /// <returns></returns>
        int UpdateSeason(DataTable contentTable);

        /// <summary>
        /// Updates Measurement Content in Insights Metadata database
        /// </summary>
        /// <param name="contentTable"></param>
        /// <returns></returns>
        int UpdateMeasurement(DataTable contentTable);

        /// <summary>
        /// Updates ProfileAttributes in Insights Metadata database
        /// </summary>
        /// <param name="contentTable"></param>
        /// <returns></returns>
        int UpdateProfileAttribute(DataTable contentTable);

        /// <summary>
        /// Updates ProfileDefaults in Insights Metadata database
        /// </summary>
        /// <param name="contentTable"></param>
        /// <returns></returns>
        int UpdateProfileDefault(DataTable contentTable);

        /// <summary>
        /// Updates ProfileDefaultCollections in Insights Metadata database
        /// </summary>
        /// <param name="contentTable"></param>
        /// <returns></returns>
        int UpdateProfileDefaultCollection(DataTable contentTable);

        /// <summary>
        /// Updates ProfileOption in Insights Metadata database
        /// </summary>
        /// <param name="contentTable"></param>
        /// <returns></returns>
        int UpdateProfileOption(DataTable contentTable);

        /// <summary>
        /// Updates Tab in Insights Metadata database
        /// </summary>
        /// <param name="contentTable"></param>
        /// <returns></returns>
        int UpdateTab(DataTable contentTable);

        /// <summary>
        /// Updates ProfileSection in Insights Metadata database
        /// </summary>
        /// <param name="contentTable"></param>
        /// <returns></returns>
        int UpdateProfileSection(DataTable contentTable);

        /// <summary>
        /// Updates TextContent in Insights Metadata database
        /// </summary>
        /// <param name="contentTable"></param>
        /// <returns></returns>
        int UpdateTextContent(DataTable contentTable);

        /// <summary>
        /// Updates WhatIfData in Insights Metadata database
        /// </summary>
        /// <param name="contentTable"></param>
        /// <returns></returns>
        int UpdateWhatIfData(DataTable contentTable);

        /// <summary>
        /// Updates Widget in Insights Metadata database
        /// </summary>
        /// <param name="contentTable"></param>
        /// <returns></returns>
        int UpdateWidget(DataTable contentTable);

        /// <summary>
        /// Updates Uom Content in Insights Metadata database
        /// </summary>
        /// <param name="contentTable"></param>
        /// <returns></returns>
        int UpdateUom(DataTable contentTable);


        #endregion

        #region Utility Methods

        /// <summary>
        /// Returns Data Table based on Content Data Table Type
        /// </summary>
        /// <param name="table"></param>
        /// <returns></returns>
        DataTable GetDataTable(ContentDataTable table);

        /// <summary>
        /// Gets content environment from database
        /// </summary>
        /// <returns></returns>
        string GetContentEnvironment();

        /// <summary>
        /// Writes Dictionary to CSV file
        /// </summary>
        /// <param name="dict"></param>
        /// <param name="csvFilePath"></param>
        void WriteDictionaryToCsv(Dictionary<string, string> dict, string csvFilePath);

        /// <summary>
        /// Gets Dictionary from CSV file
        /// </summary>
        /// <param name="csvFilePath"></param>
        /// <returns></returns>
        Dictionary<string, string> GetDictionaryFromCsv(string csvFilePath);

        /// <summary>
        /// Writes content Orphans to CSV table
        /// </summary>
        void GetContentOrphan(string outputCsvFilePath);

        /// <summary>
        /// Deletes content Orphans to CSV table
        /// </summary>
        void DeleteContentOrphan(string contentType, int clientId, string key);

        /// <summary>
        /// Writes content Orphans to CSV table
        /// </summary>

        void DeleteContentOrphanLog();

        #endregion

        #region Raw Content Methods

        /// <summary>
        /// Updates content entries cache in Insights database
        /// </summary>
        /// <param name="key"></param>
        /// <param name="clientContent"></param>
        /// <returns></returns>
        int UpdateRawContent(string key, string clientContent);

        /// <summary>
        /// Retrieves content entries cache from Insights database
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        string GetRawContent(string key);

        /// <summary>
        /// Deletes content entries cache in Insights database
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        int DeleteRawContent(string key);

        #endregion

        #region Get Content Defaults Methods

        /// <summary>
        /// Gets Default Action Keys in Insights Metadata database
        /// </summary>
        /// <param name="clientId"></param>
        /// <returns></returns>
        List<ClientActionDefault> GetDefaultActionKeys(int clientId);

        /// <summary>
        /// Gets Default Profile Attribute Keys in Insights Metadata database
        /// </summary>
        /// <param name="clientId"></param>
        /// <param name="attributeKeysFilterTable"></param>
        /// <param name="enduseKeysFilterTable"></param>
        /// <param name="applianceKeysFilterTable"></param>
        /// <param name="entitylevelFilter"></param>
        /// <returns></returns>
        List<ClientProfileAttributeDefault> GetDefaultProfileAttributeKeys(int clientId,
            DataTable attributeKeysFilterTable, DataTable enduseKeysFilterTable, DataTable applianceKeysFilterTable,
            string entitylevelFilter);

        #endregion

    }
}



