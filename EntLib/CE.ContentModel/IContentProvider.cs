﻿using System.Collections.Generic;
using System.Threading.Tasks;
using CE.ContentModel.Entities;
using Newtonsoft.Json.Linq;

namespace CE.ContentModel
{
    public interface IContentProvider
    {

        #region GetContent Methods

        /// <summary>
        /// Retrieves content for the type Action
        /// </summary>
        /// <param name="keys"></param>
        /// <returns></returns>
        List<ClientAction> GetContentAction(string keys);

        /// <summary>
        /// Retrieves content for the type Action Savings
        /// </summary>
        /// <param name="keys"></param>
        /// <returns></returns>
        List<ClientActionSaving> GetContentActionSavings(string keys);

        /// <summary>
        /// Retrieves content for the type Appliance
        /// </summary>
        /// <param name="keys"></param>
        /// <returns></returns>
        List<ClientAppliance> GetContentAppliance(string keys);

        /// <summary>
        /// Retrieves content for the type BenchmarkGroup
        /// </summary>
        /// <param name="keys"></param>
        /// <returns></returns>
        List<ClientBenchmarkGroup> GetContentBenchmarkGroup(string keys);

        /// <summary>
        /// Retrieves content for the type Commodity
        /// </summary>
        /// <param name="keys"></param>
        /// <returns></returns>
        List<ClientCommodity> GetContentCommodity(string keys);

        /// <summary>
        /// Retrieves content for the type Commodity Async
        /// </summary>
        /// <param name="keys"></param>
        /// <returns></returns>
        Task<List<ClientCommodity>> GetContentCommodityAsync(string keys);

        /// <summary>
        /// Retrieves configuration values for the given category and keys
        /// </summary>
        /// <param name="categories"></param>
        /// <param name="keys"></param>
        /// <returns></returns>
        List<ClientConfiguration> GetContentConfiguration(string categories, string keys);

        /// <summary>
        /// Retrieves configuration values for the given category and keys Async
        /// </summary>
        /// <param name="categories"></param>
        /// <param name="keys"></param>
        /// <returns></returns>
        Task<List<ClientConfiguration>> GetContentConfigurationAsync(string categories, string keys);

        /// <summary>
        /// Retrieves configuration bulk (json format) values for the given category and keys
        /// TFS 598 Aug 2015
        /// </summary>
        /// <param name="categories"></param>
        /// <param name="keys"></param>
        /// <returns></returns>
        List<ClientConfigurationBulk> GetContentClientConfigurationBulk(string categories, string keys);

        /// <summary>
        /// Retrieves content for the type Condition
        /// </summary>
        /// <param name="categories"></param>
        /// <param name="keys"></param>
        /// <returns></returns>
        List<ClientCondition> GetContentCondition(string categories, string keys);

        /// <summary>
        /// Retrieves content for the type Currency
        /// </summary>
        /// <param name="keys"></param>
        /// <returns></returns>
        List<ClientCurrency> GetContentCurrency(string keys);

        /// <summary>
        /// Retrieves content for the type Currency Async
        /// </summary>
        /// <param name="keys"></param>
        /// <returns></returns>
        Task<List<ClientCurrency>> GetContentCurrencyAsync(string keys);

        /// <summary>
        /// Retrieves content for the type Enduse
        /// </summary>
        /// <param name="keys"></param>
        /// <returns></returns>
        List<ClientEndUse> GetContentEnduse(string keys);

        /// <summary>
        /// Retrieves content for the type Enumeration
        /// </summary>
        /// <param name="categories"></param>
        /// <param name="keys"></param>
        /// <returns></returns>
        List<ClientEnumeration> GetContentEnumeration(string categories, string keys);

        /// <summary>
        /// Retrieves content for the type Expression
        /// </summary>
        /// <param name="keys"></param>
        /// <returns></returns>
        List<ClientExpression> GetContentExpression(string keys);

        /// <summary>
        /// Retrieves content for the type FileContent
        /// </summary>
        /// <param name="categories"></param>
        /// <param name="locale"></param>
        /// <param name="keys"></param>
        /// <returns></returns>
        List<ClientFileContent> GetContentFileContent(string categories, ContentLocale locale, string keys);

        /// <summary>
        /// Retrieves content for the type Layout
        /// </summary>
        /// <param name="keys"></param>
        /// <returns></returns>
        List<ClientLayout> GetContentLayout(string keys);

        /// <summary>
        /// Retrieves content for the type Measurement
        /// </summary>
        /// <param name="keys"></param>
        /// <returns></returns>
        List<ClientMeasurement> GetContentMeasurement(string keys);

        /// <summary>
        /// Retrieves content for the type Profile Attribute
        /// </summary>
        /// <param name="keys"></param>
        List<ClientProfileAttribute> GetContentProfileAttribute(string keys);


        /// <summary>
        /// Retrieves content for the type Profile Default
        /// </summary>
        /// <param name="keys"></param>
        /// <returns></returns>
        List<ClientProfileDefault> GetContentProfileDefault(string keys);


        /// <summary>
        /// Retrieves content for the type Profile Default by profile Collection Name
        /// overload function
        /// </summary>
        /// <param name="keys"></param>
        /// <param name="profileCollectionName"></param>
        /// <returns></returns>
        List<ClientProfileDefault> GetContentProfileDefault(string keys, string profileCollectionName);

        /// <summary>
        /// Retrieves content for the type Profile Default Collection
        /// </summary>
        /// <param name="keys"></param>
        /// <returns></returns>
        List<ClientProfileDefaultCollection> GetContentProfileDefaultCollection(string keys);

        /// <summary>
        /// Retrieves content for the type Profile Option
        /// </summary>
        /// <param name="keys"></param>
        /// <returns></returns>
        List<ClientProfileOption> GetContentProfileOption(string keys);

        /// <summary>
        /// Retrieves content for the type Profile Section
        /// </summary>
        /// <param name="keys"></param>
        /// <returns></returns>
        List<ClientProfileSection> GetContentProfileSection(string keys);

        /// <summary>
        /// Retrieves content for the type Season
        /// </summary>
        /// <param name="keys"></param>
        /// <returns></returns>
        List<ClientSeason> GetContentSeason(string keys);

        /// <summary>
        /// Retrieves system configuration information (application modules, menu hierarchy of each module, etc.) in JSON format
        /// for the given category and keys.
        /// </summary>
        /// <param name="categories"></param>
        /// <param name="keys"></param>
        /// <returns></returns>
        List<PortalModuleSystemConfigurationBulk> GetPortalModuleSystemConfigurationBulk(string categories, string keys);

        /// <summary>
        /// Retrieves content for the type Tab
        /// </summary>
        /// <param name="keys"></param>
        /// <returns></returns>
        List<ClientTab> GetContentTab(string keys);

        /// <summary>
        /// Retrieves Label content values for the given category and locale.
        /// </summary>
        /// <param name="categories"></param>
        /// <param name="locale"></param>
        /// <param name="keys"></param>
        /// <returns></returns>
        List<ClientTextContent> GetContentTextContent(string categories, ContentLocale locale, string keys);

        /// <summary>
        /// Retrieves Label content values for the given category and locale Aysnc.
        /// </summary>
        /// <param name="categories"></param>
        /// <param name="locale"></param>
        /// <param name="keys"></param>
        /// <returns></returns>
        Task<List<ClientTextContent>> GetContentTextContentAsync(string categories, ContentLocale locale, string keys);

        /// <summary>
        /// Retrieves content for the type Uom
        /// </summary>
        /// <param name="keys"></param>
        /// <returns></returns>
        List<ClientUOM> GetContentUom(string keys);

        /// <summary>
        /// Retrieves content for the type Uom Async
        /// </summary>
        /// <param name="keys"></param>
        /// <returns></returns>
        Task<List<ClientUOM>> GetContentUomAsync(string keys);

        /// <summary>
        /// Retrieves content for the type WhatIfData
        /// </summary>
        /// <param name="keys"></param>
        /// <returns></returns>
        List<ClientWhatIfData> GetContentWhatIfData(string keys);

        /// <summary>
        /// Retrieves content for the type Widget
        /// </summary>
        /// <param name="keys"></param>
        /// <returns></returns>
        List<ClientWidget> GetContentWidget(string keys);

        #endregion

        #region GetContent Helper Methods

        /// <summary>
        /// 
        /// </summary>
        /// <param name="configurationKey"></param>
        /// <param name="defaultValue"></param>
        /// <returns></returns>
        int GetContentConfigurationAsInt(string configurationKey, int defaultValue);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="configurationKey"></param>
        /// <param name="defaultValue"></param>
        /// <returns></returns>
        bool GetContentConfigurationAsBoolean(string configurationKey, bool defaultValue);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="configurationKey"></param>
        /// <param name="defaultValue"></param>
        /// <returns></returns>
        string GetContentConfigurationAsString(string configurationKey, string defaultValue);

        /// <summary>
        /// get configuration from configuration bulk
        /// TFS 598 2015
        /// </summary>
        /// <param name="configurationKey"></param>
        /// <param name="defaultValue"></param>
        /// <returns></returns>
        object GetContentConfigurationAsObject(string configurationKey, string defaultValue);

        #endregion
        
        #region GetContentTypes Methods

        /// <summary>
        /// Gets content locale based on string passed
        /// </summary>
        /// <param name="locale"></param>
        /// <returns></returns>
        ContentLocale GetContentLocale(string locale);

        /// <summary>
        /// Gets content environment based on string passed
        /// </summary>
        /// <param name="environment"></param>
        /// <returns></returns>
        ContentEnvironment GetContentEnvironment(string environment);

        /// <summary>
        /// Gets content type based on string passed
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        ContentType GetContentType(string type);

        /// <summary>
        /// Gets content category based on string passed
        /// </summary>
        /// <param name="category"></param>
        /// <returns></returns>
        ContentCategory GetContentCategory(string category);

        #endregion

        #region Content Cache Methods

        /// <summary>
        /// Retrieves all content from external CMS and caches locally
        /// </summary>
        void RefreshContentCacheFromContentful(string profileDefaultCollection);

        /// <summary>
        /// Retrieves all content from external CMS and caches locally
        /// </summary>
        bool KeepContentCacheAlive();

        #endregion
        
        #region Application Helper Methods

        /// <summary>
        /// Get profile attributes filtered by one or more filtering parameters
        /// Each filtering parameter is a csv string.  The string values are the keys of each kind of item.
        /// </summary>
        /// <param name="clientId"></param>
        /// <param name="attributesFilter"></param>
        /// <param name="enduseFilter"></param>
        /// <param name="applianceFilter"></param>
        /// <param name="entitylevelFilter"></param>
        /// <returns></returns>
        List<ClientProfileAttributeDefault> GetProfileAttributes(int clientId, string attributesFilter, string enduseFilter,
            string applianceFilter, string entitylevelFilter);

        /// <summary>
        /// Gets Action Content in Insights Metadata database
        /// </summary>
        /// <param name="clientId"></param>
        /// <returns></returns>
        List<ClientActionDefault> GetDefaultAction(int clientId);

        JArray GetSingleContentFromContenful(string key);

        JArray GetSingleAssetFromContenful(string key);

        JArray GetContentFromContenful(string contentType);

        #endregion
        
        #region Content Debug Methods

        /// <summary>
        /// Retrieves content JSON object for the given content type, key and locale.
        /// </summary>
        /// <param name="type"></param>
        /// <param name="key"></param>
        /// <returns></returns>
        JObject GetContentRaw(ContentType type, string key);

        /// <summary>
        /// Retrieves content JSON object for the given content type, key, and field name.
        /// </summary>
        /// <param name="type"></param>
        /// <param name="key"></param>
        /// <param name="name"></param>
        /// <param name="locale"></param>
        /// <returns></returns>
        string GetContentByFieldName(ContentType type, string key, ContentField name, ContentLocale locale);

        #endregion

    }
}
