﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using CE.ContentModel.ContentCache;
using CE.ContentModel.Entities;

// ReSharper disable once CheckNamespace
namespace CE.ContentModel
{
    /// <summary>
    /// Todo: Implement cache duration
    /// </summary>
    class ContentRepositoryCacheContentful : IContentRepositoryCache
    {

        private readonly IContentRepository _contentRepository;
        private readonly int _clientId;
        private readonly string _environment = "dev";
        private readonly IContentCacheProvider _contentCacheProvider;
        private const int AbsoluteExpirationDuration = 30;


        /// <summary>
        /// Default constructor 
        /// </summary>
        /// 
        // ReSharper disable once UnusedMember.Local
        private ContentRepositoryCacheContentful()
        {
        }
        //Default constructor not allowed.

        //Constructor for testability allow moq content for contentful.
        public ContentRepositoryCacheContentful(int clientId, IContentRepository contentRepository)
        {
            _contentRepository = contentRepository;
            _clientId = clientId;
            _environment = _contentRepository.GetContentEnvironment();
            _contentCacheProvider = ContentCacheProviderFactory.CreateContentCacheProvider();
        }


        #region Public Methods

        /// <summary>
        /// Gets ClientContent from Cache or database
        /// </summary>
        /// <returns></returns>
        public ClientContent GetClientContent(string locale, string profileDefaultCollectionName)
        {
            var result = new ClientContent
            {
                Action = GetClientAction(),
                ActionSavings = GetClientActionSaving(),
                Appliance = GetClientAppliance(),
                BenchmarkGroup = GetClientBenchmarkGroup(),
                Commodity = GetClientCommodity(),
                Configuration = GetClientConfiguration(),
                ConfigurationBulk = GetClientConfigurationBulk(),
                Condition = GetClientCondition(),
                Currency = GetClientCurrency(),
                EndUse = GetClientEndUse(),
                Enumeration = GetClientEnumeration(),
                Expression = GetClientExpression(),
                FileContent = GetClientFileContent(),
                Layout = GetClientLayout(),
                Measurement = GetClientMeasurement(),
                ProfileAttribute = GetClientProfileAttribute(),
                ProfileDefault = GetClientProfileDefault(profileDefaultCollectionName),
                ProfileDefaultCollection = GetClientProfileDefaultCollection(),
                ProfileOption = GetClientProfileOption(),
                ProfileSection = GetClientProfileSection(),
                Season = GetClientSeason(),
                Tab = GetClientTab(),
                TextContent = GetClientTextContent(locale),
                Uom = GetClientUom(),
                WhatIfData = GetClientWhatIfData(),
                Widget = GetClientWidget()
            };

            return result;
        }

        public List<ClientAction> GetClientAction()
        {
            List<ClientAction> result;
            var cacheKey = GetCacheKey(ContentConstants.StringActionKey);

            try
            {
                result = (List<ClientAction>)_contentCacheProvider.Get(cacheKey);
                if (result != null)
                    return result;
            }
            catch (Exception ex)
            {
                Trace.WriteLine(ex.Message);
            }

            result = _contentRepository.GetContentClientActions();
            SetItem(result, cacheKey);
            return result;
        }

        public List<ClientActionSaving> GetClientActionSaving()
        {
            List<ClientActionSaving> result;
            var cacheKey = GetCacheKey(ContentConstants.StringActionSavingsKey);

            try
            {
                result = (List<ClientActionSaving>)_contentCacheProvider.Get(cacheKey);
                if (result != null)
                    return result;
            }
            catch (Exception ex)
            {
                Trace.WriteLine(ex.Message);
            }

            result = _contentRepository.GetContentClientActionSavings();
            SetItem(result, cacheKey);
            return result;
        }

        public List<ClientAppliance> GetClientAppliance()
        {
            List<ClientAppliance> result;
            var cacheKey = GetCacheKey(ContentConstants.StringApplianceKey);

            try
            {
                result = (List<ClientAppliance>)_contentCacheProvider.Get(cacheKey);
                if (result != null)
                    return result;
            }
            catch (Exception ex)
            {
                Trace.WriteLine(ex.Message);
            }

            result = _contentRepository.GetContentClientAppliances();
            SetItem(result, cacheKey);
            return result;
        }

        public List<ClientBenchmarkGroup> GetClientBenchmarkGroup()
        {
            List<ClientBenchmarkGroup> result;
            var cacheKey = GetCacheKey(ContentConstants.StringBenchmarkGroupKey);

            try
            {
                result = (List<ClientBenchmarkGroup>)_contentCacheProvider.Get(cacheKey);
                if (result != null)
                    return result;
            }
            catch (Exception ex)
            {
                Trace.WriteLine(ex.Message);
            }

            result = _contentRepository.GetContentClientBenchMarkGroups();
            SetItem(result, cacheKey);
            return result;
        }

        public List<ClientCommodity> GetClientCommodity()
        {
            List<ClientCommodity> result;
            var cacheKey = GetCacheKey(ContentConstants.StringCommodityKey);

            try
            {
                result = (List<ClientCommodity>)_contentCacheProvider.Get(cacheKey);
                if (result != null)
                    return result;
            }
            catch (Exception ex)
            {
                Trace.WriteLine(ex.Message);
            }

            result = _contentRepository.GetContentClientCommodities();
            SetItem(result, cacheKey);
            return result;
        }

        public List<ClientConfiguration> GetClientConfiguration()
        {
            List<ClientConfiguration> result;
            var cacheKey = GetCacheKey(ContentConstants.StringConfigurationKey);

            try
            {
                result = (List<ClientConfiguration>)_contentCacheProvider.Get(cacheKey);
                if (result != null)
                    return result;
            }
            catch (Exception ex)
            {
                Trace.WriteLine(ex.Message);
            }

            result = _contentRepository.GetContentClientConfigurations();
            SetItem(result, cacheKey);
            return result;
        }

        public List<PortalModuleSystemConfigurationBulk> GetSystemConfigurationBulk()
        {
            List<PortalModuleSystemConfigurationBulk> result;
            var cacheKey = GetCacheKey(ContentConstants.StringSystemConfigurationBulkKey);

            try
            {
                result = (List<PortalModuleSystemConfigurationBulk>)_contentCacheProvider.Get(cacheKey);
                if (result != null)
                    return result;
            }
            catch (Exception ex)
            {
                Trace.WriteLine(ex.Message);
            }

            result = _contentRepository.GetPortalModuleSystemConfigurationBulks();
            SetItem(result, cacheKey);
            return result;
        }

        public List<ClientConfigurationBulk> GetClientConfigurationBulk()
        {
            List<ClientConfigurationBulk> result;
            var cacheKey = GetCacheKey(ContentConstants.StringClientConfigurationBulkKey);

            try
            {
                result = (List<ClientConfigurationBulk>)_contentCacheProvider.Get(cacheKey);
                if (result != null)
                    return result;
            }
            catch (Exception ex)
            {
                Trace.WriteLine(ex.Message);
            }

            result = _contentRepository.GetContentClientConfigurationBulks();
            SetItem(result, cacheKey);
            return result;
        }

        public List<ClientCondition> GetClientCondition()
        {
            List<ClientCondition> result;
            var cacheKey = GetCacheKey(ContentConstants.StringConditionKey);

            try
            {
                result = (List<ClientCondition>)_contentCacheProvider.Get(cacheKey);
                if (result != null)
                    return result;
            }
            catch (Exception ex)
            {
                Trace.WriteLine(ex.Message);
            }

            result = _contentRepository.GetContentClientConditions();
            SetItem(result, cacheKey);
            return result;
        }

        public List<ClientCurrency> GetClientCurrency()
        {
            List<ClientCurrency> result;
            var cacheKey = GetCacheKey(ContentConstants.StringCurrencyKey);

            try
            {
                result = (List<ClientCurrency>)_contentCacheProvider.Get(cacheKey);
                if (result != null)
                    return result;
            }
            catch (Exception ex)
            {
                Trace.WriteLine(ex.Message);
            }

            result = _contentRepository.GetContentClientCurrencies();
            SetItem(result, cacheKey);
            return result;
        }

        public List<ClientEndUse> GetClientEndUse()
        {
            List<ClientEndUse> result;
            var cacheKey = GetCacheKey(ContentConstants.StringEndUseKey);

            try
            {
                result = (List<ClientEndUse>)_contentCacheProvider.Get(cacheKey);
                if (result != null)
                    return result;
            }
            catch (Exception ex)
            {
                Trace.WriteLine(ex.Message);
            }

            result = _contentRepository.GetContentClientEnduses();
            SetItem(result, cacheKey);
            return result;
        }

        public List<ClientEnumeration> GetClientEnumeration()
        {
            List<ClientEnumeration> result;
            var cacheKey = GetCacheKey(ContentConstants.StringEnumerationKey);

            try
            {
                result = (List<ClientEnumeration>)_contentCacheProvider.Get(cacheKey);
                if (result != null)
                    return result;
            }
            catch (Exception ex)
            {
                Trace.WriteLine(ex.Message);
            }

            result = _contentRepository.GetContentClientEnumerations();
            SetItem(result, cacheKey);
            return result;
        }

        public List<ClientExpression> GetClientExpression()
        {
            List<ClientExpression> result;
            var cacheKey = GetCacheKey(ContentConstants.StringExpressionKey);

            try
            {
                result = (List<ClientExpression>)_contentCacheProvider.Get(cacheKey);
                if (result != null)
                    return result;
            }
            catch (Exception ex)
            {
                Trace.WriteLine(ex.Message);
            }

            result = _contentRepository.GetContentClientExpressions();
            SetItem(result, cacheKey);
            return result;
        }

        public List<ClientFileContent> GetClientFileContent()
        {
            List<ClientFileContent> result;
            var cacheKey = GetCacheKey(ContentConstants.StringFileContentKey);

            try
            {
                result = (List<ClientFileContent>)_contentCacheProvider.Get(cacheKey);
                if (result != null)
                    return result;
            }
            catch (Exception ex)
            {
                Trace.WriteLine(ex.Message);
            }

            result = _contentRepository.GetContentClientFileContentList();
            SetItem(result, cacheKey);
            return result;
        }

        public List<ClientLayout> GetClientLayout()
        {
            List<ClientLayout> result;
            var cacheKey = GetCacheKey(ContentConstants.StringLayoutKey);

            try
            {
                result = (List<ClientLayout>)_contentCacheProvider.Get(cacheKey);
                if (result != null)
                    return result;
            }
            catch (Exception ex)
            {
                Trace.WriteLine(ex.Message);
            }

            result = _contentRepository.GetContentClientLayouts();
            SetItem(result, cacheKey);
            return result;
        }

        public List<ClientMeasurement> GetClientMeasurement()
        {
            List<ClientMeasurement> result;
            var cacheKey = GetCacheKey(ContentConstants.StringMeasurementKey);

            try
            {
                result = (List<ClientMeasurement>)_contentCacheProvider.Get(cacheKey);
                if (result != null)
                    return result;
            }
            catch (Exception ex)
            {
                Trace.WriteLine(ex.Message);
            }

            result = _contentRepository.GetContentClientMeasurements();
            SetItem(result, cacheKey);
            return result;
        }

        public List<ClientProfileAttribute> GetClientProfileAttribute()
        {
            List<ClientProfileAttribute> result;
            var cacheKey = GetCacheKey(ContentConstants.StringProfileAttributeKey);

            try
            {
                result = (List<ClientProfileAttribute>)_contentCacheProvider.Get(cacheKey);
                if (result != null)
                    return result;
            }
            catch (Exception ex)
            {
                Trace.WriteLine(ex.Message);
            }

            result = _contentRepository.GetContentClientProfileAttributes();
            SetItem(result, cacheKey);
            return result;
        }

        public List<ClientProfileDefault> GetClientProfileDefault(string profileDefaultCollectionName)
        {
            List<ClientProfileDefault> result;
            var cacheKey = GetCacheKey(ContentConstants.StringProfileDefaultKey, profileDefaultCollectionName);

            try
            {
                result = (List<ClientProfileDefault>)_contentCacheProvider.Get(cacheKey);
                if (result != null)
                    return result;
            }
            catch (Exception ex)
            {
                Trace.WriteLine(ex.Message);
            }

            result = _contentRepository.GetContentClientProfileDefaults(profileDefaultCollectionName);
            SetItem(result, cacheKey);
            return result;
        }

        public List<ClientProfileDefaultCollection> GetClientProfileDefaultCollection()
        {
            List<ClientProfileDefaultCollection> result;
            var cacheKey = GetCacheKey(ContentConstants.StringProfileDefaultCollectionKey);

            try
            {
                result = (List<ClientProfileDefaultCollection>)_contentCacheProvider.Get(cacheKey);
                if (result != null)
                    return result;
            }
            catch (Exception ex)
            {
                Trace.WriteLine(ex.Message);
            }

            result = _contentRepository.GetContentClientProfileDefaultCollections();
            SetItem(result, cacheKey);
            return result;
        }

        public List<ClientProfileOption> GetClientProfileOption()
        {
            List<ClientProfileOption> result;
            var cacheKey = GetCacheKey(ContentConstants.StringProfileOptionKey);

            try
            {
                result = (List<ClientProfileOption>)_contentCacheProvider.Get(cacheKey);
                if (result != null)
                    return result;
            }
            catch (Exception ex)
            {
                Trace.WriteLine(ex.Message);
            }

            result = _contentRepository.GetContentClientProfileOptions();
            SetItem(result, cacheKey);
            return result;
        }

        public List<ClientProfileSection> GetClientProfileSection()
        {
            List<ClientProfileSection> result;
            var cacheKey = GetCacheKey(ContentConstants.StringProfileSectionKey);

            try
            {
                result = (List<ClientProfileSection>)_contentCacheProvider.Get(cacheKey);
                if (result != null)
                    return result;
            }
            catch (Exception ex)
            {
                Trace.WriteLine(ex.Message);
            }

            result = _contentRepository.GetContentClientProfileSection();
            SetItem(result, cacheKey);
            return result;
        }

        public List<ClientSeason> GetClientSeason()
        {
            List<ClientSeason> result;
            var cacheKey = GetCacheKey(ContentConstants.StringSeasonKey);

            try
            {
                result = (List<ClientSeason>)_contentCacheProvider.Get(cacheKey);
                if (result != null)
                    return result;
            }
            catch (Exception ex)
            {
                Trace.WriteLine(ex.Message);
            }

            result = _contentRepository.GetContentClientSeasons();
            SetItem(result, cacheKey);
            return result;
        }

        public List<ClientTab> GetClientTab()
        {
            List<ClientTab> result;
            var cacheKey = GetCacheKey(ContentConstants.StringTabKey);

            try
            {
                result = (List<ClientTab>)_contentCacheProvider.Get(cacheKey);
                if (result != null)
                    return result;
            }
            catch (Exception ex)
            {
                Trace.WriteLine(ex.Message);
            }

            result = _contentRepository.GetContentClientTabs();
            SetItem(result, cacheKey);
            return result;
        }

        public List<ClientTextContent> GetClientTextContent(string locale)
        {
            List<ClientTextContent> result;
            var cacheKey = GetCacheKey(ContentConstants.StringTextContentKey, locale);

            try
            {
                result = (List<ClientTextContent>)_contentCacheProvider.Get(cacheKey);
                if (result != null)
                    return result;
            }
            catch (Exception ex)
            {
                Trace.WriteLine(ex.Message);
            }

            result = _contentRepository.GetContentClientTextContentList(locale);
            SetItem(result, cacheKey);
            return result;
        }

        public List<ClientUOM> GetClientUom()
        {
            List<ClientUOM> result;
            var cacheKey = GetCacheKey(ContentConstants.StringUomKey);

            try
            {
                result = (List<ClientUOM>)_contentCacheProvider.Get(cacheKey);
                if (result != null)
                    return result;
            }
            catch (Exception ex)
            {
                Trace.WriteLine(ex.Message);
            }

            result = _contentRepository.GetContentClientUoms();
            SetItem(result, cacheKey);
            return result;
        }

        public List<ClientWhatIfData> GetClientWhatIfData()
        {
            List<ClientWhatIfData> result;
            var cacheKey = GetCacheKey(ContentConstants.StringWhatIfDataKey);

            try
            {
                result = (List<ClientWhatIfData>)_contentCacheProvider.Get(cacheKey);
                if (result != null)
                    return result;
            }
            catch (Exception ex)
            {
                Trace.WriteLine(ex.Message);
            }

            result = _contentRepository.GetContentClientWhatIfData();
            SetItem(result, cacheKey);
            return result;
        }

        public List<ClientWidget> GetClientWidget()
        {
            List<ClientWidget> result;
            var cacheKey = GetCacheKey(ContentConstants.StringWidgetKey);

            try
            {
                result = (List<ClientWidget>)_contentCacheProvider.Get(cacheKey);
                if (result != null)
                    return result;
            }
            catch (Exception ex)
            {
                Trace.WriteLine(ex.Message);
            }

            result = _contentRepository.GetContentClientWidgets();
            SetItem(result, cacheKey);
            return result;
        }

        public void RemoveClientContentCache(string contentName)
        {
            _contentCacheProvider.Remove(GetCacheKey(ContentConstants.StringConfigurationKey));
            _contentCacheProvider.Remove(GetCacheKey(ContentConstants.StringTextContentKey));
            _contentCacheProvider.Remove(GetCacheKey(ContentConstants.StringActionKey));
            _contentCacheProvider.Remove(GetCacheKey(ContentConstants.StringActionSavingsKey));
            _contentCacheProvider.Remove(GetCacheKey(ContentConstants.StringApplianceKey));
            _contentCacheProvider.Remove(GetCacheKey(ContentConstants.StringBenchmarkGroupKey));
            _contentCacheProvider.Remove(GetCacheKey(ContentConstants.StringCommodityKey));
            _contentCacheProvider.Remove(GetCacheKey(ContentConstants.StringConditionKey));
            _contentCacheProvider.Remove(GetCacheKey(ContentConstants.StringCurrencyKey));
            _contentCacheProvider.Remove(GetCacheKey(ContentConstants.StringEndUseKey));
            _contentCacheProvider.Remove(GetCacheKey(ContentConstants.StringEnumerationKey));
            _contentCacheProvider.Remove(GetCacheKey(ContentConstants.StringExpressionKey));
            _contentCacheProvider.Remove(GetCacheKey(ContentConstants.StringFileContentKey));
            _contentCacheProvider.Remove(GetCacheKey(ContentConstants.StringLayoutKey));
            _contentCacheProvider.Remove(GetCacheKey(ContentConstants.StringMeasurementKey));
            _contentCacheProvider.Remove(GetCacheKey(ContentConstants.StringProfileAttributeKey));
            _contentCacheProvider.Remove(GetCacheKey(ContentConstants.StringProfileDefaultKey, contentName));
            _contentCacheProvider.Remove(GetCacheKey(ContentConstants.StringProfileDefaultCollectionKey));
            _contentCacheProvider.Remove(GetCacheKey(ContentConstants.StringProfileOptionKey));
            _contentCacheProvider.Remove(GetCacheKey(ContentConstants.StringProfileSectionKey));
            _contentCacheProvider.Remove(GetCacheKey(ContentConstants.StringSeasonKey));
            _contentCacheProvider.Remove(GetCacheKey(ContentConstants.StringTabKey));
            _contentCacheProvider.Remove(GetCacheKey(ContentConstants.StringUomKey));
            _contentCacheProvider.Remove(GetCacheKey(ContentConstants.StringWhatIfDataKey));
            _contentCacheProvider.Remove(GetCacheKey(ContentConstants.StringWidgetKey));
        }

        #endregion


        private void SetItem(object objectToCache, string key)
        {
            _contentCacheProvider.Set(key, objectToCache, DateTime.Now.AddMinutes(AbsoluteExpirationDuration));
        }

        private string GetCacheKey(string contentType)
        {
            return GetCacheKey(contentType, string.Empty);
        }

        private string GetCacheKey(string contentType, string contentName)
        {
            const string cacheKeyTemplate = "ClientContent_{0}_{1}_{2}_{3}";
            return string.Format(cacheKeyTemplate, _environment, _clientId, contentType, contentName);
        }

    }
}
