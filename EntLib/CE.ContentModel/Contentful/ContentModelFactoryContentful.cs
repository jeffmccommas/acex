﻿using System;
using System.Configuration;

// ReSharper disable once CheckNamespace
namespace CE.ContentModel
{
    public class ContentModelFactoryContentful : IContentModelFactory
    {

        const string InsightsMetaDataConnName = "InsightsMetaDataConn";

        /// <summary>
        /// Create content model.
        /// </summary>
        /// <param name="clientId"></param>
        /// <returns></returns>
        public IContentProvider CreateContentProvider(int clientId)
        {
            return CreateContentProvider(clientId, ContentProviderType.Contentful);
        }

        /// <summary>
        /// Create content model.
        /// </summary>
        /// <param name="clientId"></param>
        /// <param name="type"></param>
        /// <returns></returns>
        public IContentProvider CreateContentProvider(int clientId, ContentProviderType type)
        {
            var insightsMetaDataConnectionString = ConfigurationManager.ConnectionStrings[InsightsMetaDataConnName].ConnectionString;

            return CreateContentProvider(clientId, type, insightsMetaDataConnectionString);
        }

        /// <summary>
        /// Create content model.
        /// </summary>
        /// <param name="clientId"></param>
        /// <param name="type"></param>
        /// <param name="insightsMetaDataConnectionString"></param>
        /// <returns></returns>
        public IContentProvider CreateContentProvider(int clientId, ContentProviderType type, string insightsMetaDataConnectionString)
        {
            const string templateContentProviderNotFound = "Content provider type {0} not found.";
            var contentRepository = CreateContentRepository(insightsMetaDataConnectionString, clientId);
            ContentProviderContentfulAdapter result;

            switch (type)
            {
                case ContentProviderType.Contentful:
                    result = new ContentProviderContentfulAdapter(clientId, contentRepository);
                    break;
                case ContentProviderType.Mock:
                    result = new ContentProviderContentfulAdapter(contentRepository);
                    break;
                default:
                    throw new ArgumentException(string.Format(templateContentProviderNotFound, Enum.GetName(type.GetType(), type)));
            }

            return result;
        }



        /// <summary>
        /// Override
        /// </summary>
        /// <returns></returns>
        public IContentManager CreateContentManager(int clientId)
        {
            var contentRepository = CreateContentRepository(clientId);
            var contentProvider = CreateContentProvider(clientId);
            var result = new ContentManagerContentful(contentRepository, contentProvider);
            return result;
        }


        /// <summary>
        /// override
        /// </summary>
        /// <returns></returns>
        public IContentRepository CreateContentRepository(int clientId)
        {
            const string templateConnStringNotFound = "Unable to find connectionstring {0}.";
            try
            {
                var insightsMetaDataConnectionString = ConfigurationManager.ConnectionStrings[InsightsMetaDataConnName].ConnectionString;
                var result = new ContentRepositoryContentful(insightsMetaDataConnectionString, clientId);
                return result;
            }
            catch (Exception)
            {
                throw new ArgumentException(string.Format(templateConnStringNotFound, InsightsMetaDataConnName));
            }
        }

        /// <summary>
        /// override
        /// </summary>
        /// <param name="insightsMetaDataConnectionString"></param>
        /// <param name="clientId"></param>
        /// <returns></returns>
        public IContentRepository CreateContentRepository(string insightsMetaDataConnectionString, int clientId)
        {
            var result = new ContentRepositoryContentful(insightsMetaDataConnectionString, clientId);
            return result;
        }

    }
}
