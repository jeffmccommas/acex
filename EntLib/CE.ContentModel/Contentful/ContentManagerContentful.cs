﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using CE.ContentModel.ContentfulEntities;
using Microsoft.VisualBasic.FileIO;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using RestSharp;

// ReSharper disable once CheckNamespace
namespace CE.ContentModel
{
    class ContentManagerContentful : IContentManager
    {

        #region Private Properties

        private readonly IContentRepository _contentRepository;
        private readonly IContentProvider _contentProvider;
        private Dictionary<string, string> _sysIdAndKeyDictionary;

        #endregion

        #region Public Properties

        public Dictionary<string, string> SysIdAndKeyDictionary
        {
            get { return _sysIdAndKeyDictionary ?? (_sysIdAndKeyDictionary = GetContentfulSysIdAndKeyDictionary()); }
        }

        //private Dictionary<string, string> _sysIdAndKeyDictionaryFromFile;

        #endregion

        #region Constants

        private const string TemplateUrl = "https://api.contentful.com/spaces/{0}/entries/{1}?access_token={2}";
        private const string TemplateUrlPublishContent = "https://api.contentful.com/spaces/{0}/entries/{1}/published?access_token={2}";
        private const string TemplateUrlDeleteContent = "https://api.contentful.com/spaces/{0}/entries/{1}?access_token={2}";
        private const string TemplateUrlAsset = "https://api.contentful.com/spaces/{0}/assets/{1}?access_token={2}";
        private const string TemplateUrlEnglishProcessAsset = "https://api.contentful.com/spaces/{0}/assets/{1}/files/en-US/process?access_token={2}";
        private const string TemplateUrlSpanishProcessAsset = "https://api.contentful.com/spaces/{0}/assets/{1}/files/es-ES/process?access_token={2}";
        private const string TemplateUrlRussianProcessAsset = "https://api.contentful.com/spaces/{0}/assets/{1}/files/ru-RU/process?access_token={2}";
        private const string AccessToken = "f9d20476224dee29d3bf0760eeaab7557e1162cf84905a00a8339700e71dc231";
        private const string DefaultSpace = "wp32f8j5jqi2";
        private const string StringEntry = "Entry";
        const string TemplateCacheKey = "{0}_{1}";
        private const string StringAsset = "Asset";

        private const string TemplateTokenField = "fields.{0}.{1}";
        private const string TokenContentId = "sys.id";

        #endregion

        #region Constructor

        /// <summary>
        /// constructor
        /// </summary>
        /// <param name="contentRepository"></param>
        /// <param name="contentProvider"></param>
        public ContentManagerContentful(IContentRepository contentRepository, IContentProvider contentProvider)
        {
            _contentRepository = contentRepository;
            _contentProvider = contentProvider;
        }

        #endregion

        #region Public Methods

        #region Contentful Methods

        /// <summary>
        /// Creates new asset in Contentful
        /// </summary>
        /// <param name="url"></param>
        /// <param name="requestBody"></param>
        /// <param name="version"></param>
        /// <returns></returns>
        public string CreateContentAsset(string url, string requestBody, int version = 0)
        {
            var result = CreateAsset(url, requestBody, version);

            return result;
        }

        /// <summary>
        /// process new asset in Contentful
        /// </summary>
        /// <param name="url"></param>
        /// <returns></returns>
        public string ProcessContentAsset(string url)
        {
            var result = ProcessAsset(url);

            return result;
        }


        /// <summary>
        /// Creates new content type in Contentful
        /// </summary>
        /// <param name="url"></param>
        /// <param name="requestBody"></param>
        /// <returns></returns>
        public string CreateContentType(string url, string requestBody)
        {
            var result = PutContent(url, requestBody, null, 0);

            return result;
        }

        /// <summary>
        /// Creates new content entry in Contentful
        /// </summary>
        /// <param name="url"></param>
        /// <param name="requestBody"></param>
        /// <param name="contentType"></param>
        /// <param name="version"></param>
        /// <returns></returns>
        public string CreateContentEntry(string url, string requestBody, string contentType, int version = 0)
        {
            var result = PutContent(url, requestBody, contentType, version);

            return result;
        }

        /// <summary>
        /// Publishes Creates new content entry in Contentful
        /// </summary>
        /// <param name="url"></param>
        /// <param name="version"></param>
        /// <returns></returns>
        public string PublishContentEntry(string url, int version)
        {
            var result = PutContent(url, null, null, version);

            return result;
        }

        /// <summary>
        /// UnPublishes content entry in Contentful
        /// </summary>
        /// <param name="url"></param>
        /// <returns></returns>
        public string UnPublishContentEntry(string url)
        {
            var result = DeleteContent(url);

            return result;
        }

        /// <summary>
        /// Deletes content entry in Contentful
        /// </summary>
        /// <param name="url"></param>
        /// <returns></returns>
        public string DeleteContentEntry(string url)
        {
            var result = DeleteContent(url);

            return result;
        }

        #endregion

        #region CSV Methods

        public string CreateContentEntriesUsingCsv(ContentType type, string csvFilePath)
        {
            var result = CreateEntriesInContentful(type, csvFilePath);
            return result;
        }

        public void PublishContentEntriesUsingCsv(string csvFilePath)
        {
            PublishEntriesInContentful(csvFilePath);
        }

        public void UnPublishContentEntriesUsingCsv(string csvFilePath)
        {
            UnPublishEntriesInContentful(csvFilePath);
        }

        public void DeleteContentEntriesUsingCsv(string csvFilePath)
        {
            DeleteEntriesInContentful(csvFilePath);
        }

        public void UpdateDatabaseClientContentUsingCsv(int clientId, ContentType type, string csvFilePath)
        {
            UpdateDatabaseClientContent(clientId, type, csvFilePath);
        }

        #endregion

        #region Helper Methods

        public Dictionary<string, string> GetContentfulSysIdAndKeyDictionary()
        {
            string cacheKeyEntry = string.Format(TemplateCacheKey, DefaultSpace, StringEntry);
            return GetContentfulIdAndKeyDictionary(_contentRepository, cacheKeyEntry);
        }

        #endregion

        #endregion

        #region Private Methods

        #region CSV Methods

        #region Contentful Create Entry Methods

        private string CreateEntriesInContentful(ContentType type, string csvFilePath)
        {
            //var keyTable = ToDataTable<string>(keys);
            const string templateContentNotFound = "ContentType '{0}' not found.";
            const string templateContentfulError = "Failed to create/update one or more entries in Contentful. Verify following output, update arguments, and rerun the process.  The entries might already exist in Contentful, but weren't published yet.  Please publish them before update the entries";
            const string templateContentfulErrorDetails = "***********Errors*********** {0}. ***********Information*********** {1}";

            var sbInfo = new StringBuilder();
            var sbError = new StringBuilder();
            var parser = new TextFieldParser(csvFilePath) { HasFieldsEnclosedInQuotes = true };
            parser.SetDelimiters(",");
            parser.ReadLine(); //ignore 1st row

            //_sysIdAndKeyDictionaryFromFile = _contentRepository.GetDictionaryFromCsv(@"C:\Temp\Contentful\ContentfulKeys.csv");

            // July 2015 TFS 287: get entry cache from database
            string cacheKeyEntry = string.Format(TemplateCacheKey, DefaultSpace, StringEntry);
            var contentCache = _contentRepository.GetRawContent(cacheKeyEntry);

            while (!parser.EndOfData)
            {
                var fields = parser.ReadFields();
                if (fields == null) continue;

                switch (type)
                {
                    case ContentType.Asset:
                        //string cacheKeyAsset = string.Format(TemplateCacheKey, DefaultSpace, StringAsset);
                        var assetCache = _contentProvider.GetContentFromContenful(StringAsset);
                        ProcessContentfulOutput(sbInfo, sbError, CreateContentfulEntryAsset(fields, assetCache.ToString()), fields);
                        break;
                    case ContentType.Action:
                        ProcessContentfulOutput(sbInfo, sbError, CreateContentfulEntryAction(fields, contentCache), fields);
                        break;
                    case ContentType.Appliance:
                        ProcessContentfulOutput(sbInfo, sbError, CreateContentfulEntryAppliance(fields, contentCache), fields);
                        break;
                    case ContentType.BenchmarkGroup:
                        ProcessContentfulOutput(sbInfo, sbError, CreateContentfulEntryBenchmarkGroup(fields, contentCache), fields);
                        break;
                    case ContentType.Commodity:
                        ProcessContentfulOutput(sbInfo, sbError, CreateContentfulEntryCommodity(fields, contentCache), fields);
                        break;
                    case ContentType.Condition:
                        ProcessContentfulOutput(sbInfo, sbError, CreateContentfulEntryCondition(fields, contentCache), fields);
                        break;
                    case ContentType.ConfigurationBulk:
                        ProcessContentfulOutput(sbInfo, sbError, CreateContentfulEntryConfigurationBulk(fields, contentCache), fields);
                        break;
                    case ContentType.Configuration:
                        ProcessContentfulOutput(sbInfo, sbError, CreateContentfulEntryConfiguration(fields, contentCache), fields);
                        break;
                    case ContentType.Currency:
                        ProcessContentfulOutput(sbInfo, sbError, CreateContentfulEntryCurrency(fields, contentCache), fields);
                        break;
                    case ContentType.Enduse:
                        ProcessContentfulOutput(sbInfo, sbError, CreateContentfulEntryEnduse(fields, contentCache), fields);
                        break;
                    case ContentType.EnumerationItem:
                        ProcessContentfulOutput(sbInfo, sbError, CreateContentfulEntryEnumerationItem(fields, contentCache), fields);
                        break;
                    case ContentType.Enumeration:
                        ProcessContentfulOutput(sbInfo, sbError, CreateContentfulEntryEnumeration(fields, contentCache), fields);
                        break;
                    case ContentType.Expression:
                        ProcessContentfulOutput(sbInfo, sbError, CreateContentfulEntryExpression(fields, contentCache), fields);
                        break;
                    case ContentType.FileContent:
                        ProcessContentfulOutput(sbInfo, sbError, CreateContentfulEntryFileContent(fields, contentCache), fields);
                        break;
                    case ContentType.Layout:
                        ProcessContentfulOutput(sbInfo, sbError, CreateContentfulEntryLayout(fields, contentCache), fields);
                        break;
                    case ContentType.Measurement:
                        ProcessContentfulOutput(sbInfo, sbError, CreateContentfulEntryMeasurement(fields, contentCache), fields);
                        break;
                    case ContentType.ProfileDefault:
                        ProcessContentfulOutput(sbInfo, sbError, CreateContentfulEntryProfileDefault(fields, contentCache), fields);
                        break;
                    case ContentType.ProfileDefaultCollection:
                        ProcessContentfulOutput(sbInfo, sbError, CreateContentfulEntryProfileDefaultCollection(fields, contentCache), fields);
                        break;
                    case ContentType.ProfileOption:
                        ProcessContentfulOutput(sbInfo, sbError, CreateContentfulEntryProfileOption(fields, contentCache), fields);
                        break;
                    case ContentType.ProfileAttribute:
                        ProcessContentfulOutput(sbInfo, sbError, CreateContentfulEntryProfileAttribute(fields, contentCache), fields);
                        break;
                    case ContentType.ProfileSection:
                        ProcessContentfulOutput(sbInfo, sbError, CreateContentfulEntryProfileSection(fields, contentCache), fields);
                        break;
                    case ContentType.Season:
                        ProcessContentfulOutput(sbInfo, sbError, CreateContentfulEntrySeason(fields, contentCache), fields);
                        break;
                    case ContentType.Tab:
                        ProcessContentfulOutput(sbInfo, sbError, CreateContentfulEntryTab(fields, contentCache), fields);
                        break;
                    case ContentType.TextContent:
                        ProcessContentfulOutput(sbInfo, sbError, CreateContentfulEntryTextContent(fields, contentCache), fields);
                        break;
                    case ContentType.Uom:
                        ProcessContentfulOutput(sbInfo, sbError, CreateContentfulEntryUom(fields, contentCache), fields);
                        break;
                    case ContentType.WhatIfData:
                        ProcessContentfulOutput(sbInfo, sbError, CreateContentfulEntryWhatIfData(fields, contentCache), fields);
                        break;
                    case ContentType.Widget:
                        ProcessContentfulOutput(sbInfo, sbError, CreateContentfulEntryWidget(fields, contentCache), fields);
                        break;
                    default:
                        throw new ArgumentException(string.Format(templateContentNotFound, type));

                }

                System.Threading.Thread.Sleep(10);
            }
            parser.Close();

            var result = sbInfo.ToString();
            if (sbError.Length > 0)
            {
                throw new ArgumentException(templateContentfulError, new ArgumentException(string.Format(templateContentfulErrorDetails, sbError, result)));
            }
            return result;
        }

        private void ProcessContentfulOutput(StringBuilder info, StringBuilder error, string output, IList<string> fields)
        {
            const string stringError = "Error";
            const string templateContentfulEntryError = "{0} - Error encountered while processing entry with key '{1}'. Error details:{2}";
            const string templateContentfulEntryInfo = "{0} - Successfully processed entry with key '{1}'. Entry details:{2}";
            var key = GetKeyValue(fields[0]);

            if (output.Contains(stringError))
            {
                error.AppendFormat(templateContentfulEntryError, DateTime.Now, key, output);
            }
            else
            {
                info.AppendFormat(templateContentfulEntryInfo, DateTime.Now, key, output);
            }
        }

        private void UpdateDatabaseClientContent(int clientId, ContentType type, string csvFilePath)
        {
            //var keyTable = ToDataTable<string>(keys);
            const string templateContentNotFound = "ContentType '{0}' not found.";

            var parser = new TextFieldParser(csvFilePath) { HasFieldsEnclosedInQuotes = true };
            parser.SetDelimiters(",");
            parser.ReadLine(); //ignore 1st row

            switch (type)
            {
                case ContentType.Action:
                    UpdateDatabaseClientAction(clientId, parser);
                    break;
                case ContentType.Appliance:
                    UpdateDatabaseClientAppliance(clientId, parser);
                    break;
                case ContentType.BenchmarkGroup:
                    UpdateDatabaseClientBenchmarkGroup(clientId, parser);
                    break;
                case ContentType.Commodity:
                    UpdateDatabaseClientCommodity(clientId, parser);
                    break;
                case ContentType.Condition:
                    UpdateDatabaseClientCondition(clientId, parser);
                    break;
                case ContentType.ConfigurationBulk:
                    // TFS 598 Aug 2015 - configuration in json foramt
                    UpdateDatabaseClientConfigurationBulk(clientId, parser);
                    break;
                case ContentType.Configuration:
                    UpdateDatabaseClientConfiguration(clientId, parser);
                    break;
                case ContentType.Currency:
                    UpdateDatabaseClientCurrency(clientId, parser);
                    break;
                case ContentType.Enduse:
                    UpdateDatabaseClientEnduse(clientId, parser);
                    break;
                case ContentType.Enumeration:
                    UpdateDatabaseClientEnumeration(clientId, parser);
                    break;
                case ContentType.EnumerationItem:
                    UpdateDatabaseClientEnumerationItem(clientId, parser);
                    break;
                case ContentType.Expression:
                    UpdateDatabaseClientExpression(clientId, parser);
                    break;
                case ContentType.FileContent:
                    UpdateDatabaseClientFileContent(clientId, parser);
                    break;
                case ContentType.Layout:
                    UpdateDatabaseClientLayout(clientId, parser);
                    break;
                case ContentType.Measurement:
                    UpdateDatabaseClientMeasurement(clientId, parser);
                    break;
                case ContentType.ProfileDefault:
                    UpdateDatabaseClientProfileDefault(clientId, parser);
                    break;
                case ContentType.ProfileDefaultCollection:
                    UpdateDatabaseClientProfileDefaultCollection(clientId, parser);
                    break;
                case ContentType.ProfileOption:
                    UpdateDatabaseClientProfileOption(clientId, parser);
                    break;
                case ContentType.ProfileAttribute:
                    UpdateDatabaseClientProfileAttribute(clientId, parser);
                    break;
                case ContentType.ProfileSection:
                    UpdateDatabaseClientProfileSection(clientId, parser);
                    break;
                case ContentType.Season:
                    UpdateDatabaseClientSeason(clientId, parser);
                    break;
                case ContentType.Tab:
                    UpdateDatabaseClientTab(clientId, parser);
                    break;
                case ContentType.TextContent:
                    UpdateDatabaseClientTextContent(clientId, parser);
                    break;
                case ContentType.Uom:
                    UpdateDatabaseClientUom(clientId, parser);
                    break;
                case ContentType.WhatIfData:
                    UpdateDatabaseClientWhatIfData(clientId, parser);
                    break;
                case ContentType.Widget:
                    UpdateDatabaseClientWidget(clientId, parser);
                    break;
                default:
                    throw new ArgumentException(string.Format(templateContentNotFound, type));

            }

            parser.Close();
        }

        #endregion

        #region Contentful Publish Methods

        private void PublishEntriesInContentful(string csvFilePath)
        {

            const int version = 1; //Using version as 1 assuming that we have created entries through Unit Test
            var parser = new TextFieldParser(csvFilePath) { HasFieldsEnclosedInQuotes = true };
            parser.SetDelimiters(",");
            parser.ReadLine(); //ignore 1st row
            while (!parser.EndOfData)
            {
                var fields = parser.ReadFields();
                if (fields == null) continue;
                var key = GetKeyValue(fields[0]);
                var url = string.Format(TemplateUrlPublishContent, DefaultSpace, key, AccessToken);
                var result = PublishContentEntry(url, version);
                System.Threading.Thread.Sleep(10);
                Console.Out.WriteLine(result);
            }

            parser.Close();
        }

        #endregion

        #region Contentful UnPublish Methods

        /// <summary>
        /// 
        /// </summary>
        /// <param name="csvFilePath"></param>
        private void UnPublishEntriesInContentful(string csvFilePath)
        {
            var parser = new TextFieldParser(csvFilePath) { HasFieldsEnclosedInQuotes = true };
            parser.SetDelimiters(",");
            parser.ReadLine(); //ignore 1st row
            while (!parser.EndOfData)
            {
                var fields = parser.ReadFields();
                if (fields == null) continue;
                var keySysId = GetSysId(fields[0]);
                var url = string.Format(TemplateUrlPublishContent, DefaultSpace, keySysId, AccessToken);
                var result = UnPublishContentEntry(url);
                System.Threading.Thread.Sleep(10);
                Console.Out.WriteLine(result);
            }

            parser.Close();
        }

        #endregion

        #region Contentful Delete Methods

        /// <summary>
        /// 
        /// </summary>
        /// <param name="csvFilePath"></param>
        private void DeleteEntriesInContentful(string csvFilePath)
        {
            var parser = new TextFieldParser(csvFilePath) { HasFieldsEnclosedInQuotes = true };
            parser.SetDelimiters(",");
            parser.ReadLine(); //ignore 1st row
            while (!parser.EndOfData)
            {
                var fields = parser.ReadFields();
                if (fields == null) continue;
                var keySysId = GetSysId(fields[0]);
                if (string.IsNullOrEmpty(keySysId))
                {
                    keySysId = GetStringValue(fields[0]);
                }
                var url = string.Format(TemplateUrlDeleteContent, DefaultSpace, keySysId, AccessToken);
                var result = DeleteContentEntry(url);
                System.Threading.Thread.Sleep(10);
                Console.Out.WriteLine(result);
            }

            parser.Close();
        }

        #endregion

        #endregion

        #region Contentful Core Methods

        /// <summary>
        /// 
        /// </summary>
        /// <param name="contentUrl"></param>
        /// <param name="jsonBody"></param>
        /// <param name="version"></param>
        /// <returns></returns>
        private static string CreateAsset(string contentUrl, string jsonBody, int version)
        {
            //Previous TLS levels deprecated by Contentful since 30 April 2017
            ServicePointManager.SecurityProtocol=SecurityProtocolType.Tls12;
            var restClient = new RestClient(contentUrl);
            var request = new RestRequest(Method.PUT) { RequestFormat = DataFormat.Json };

            request.AddParameter("Content-Type", "application/vnd.contentful.management.v1+json", ParameterType.HttpHeader);
            // set the version if greater 0
            if (version > 0)
            {
                request.AddParameter("X-Contentful-Version", version, ParameterType.HttpHeader);
                request.AddParameter("Content-Length", 0, ParameterType.HttpHeader);
            }

            if (!string.IsNullOrEmpty(jsonBody))
            {
                request.AddParameter("application/json; charset=utf-8", jsonBody, ParameterType.RequestBody);
            }

            var response = restClient.Execute(request);
            var result = response.Content;

            return result;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="contentUrl"></param>
        /// <returns></returns>
        private static string ProcessAsset(string contentUrl)
        {
            //Previous TLS levels deprecated by Contentful since 30 April 2017
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
            var restClient = new RestClient(contentUrl);
            var request = new RestRequest(Method.PUT) { RequestFormat = DataFormat.Json };

            request.AddParameter("Content-Type", "application/vnd.contentful.management.v1+json", ParameterType.HttpHeader);
            var response = restClient.Execute(request);
            var result = response.Content;

            return result;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="contentUrl"></param>
        /// <param name="jsonBody"></param>
        /// <param name="contentType"></param>
        /// <param name="version"></param>
        /// <returns></returns>
        private static string PutContent(string contentUrl, string jsonBody, string contentType, int version)
        {
            //Previous TLS levels deprecated by Contentful since 30 April 2017
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
            var restClient = new RestClient(contentUrl);
            var request = new RestRequest(Method.PUT) { RequestFormat = DataFormat.Json };

            request.AddParameter("Content-Type", "application/vnd.contentful.management.v1+json", ParameterType.HttpHeader);
            if (!string.IsNullOrEmpty(contentType) && contentType != "asset")
            {
                request.AddParameter("X-Contentful-Content-Type", contentType, ParameterType.HttpHeader);
            }
            if (version > 0)
            {
                request.AddParameter("X-Contentful-Version", version, ParameterType.HttpHeader);
                request.AddParameter("Content-Length", 0, ParameterType.HttpHeader);
            }
            if (!string.IsNullOrEmpty(jsonBody))
            {
                request.AddParameter("application/json; charset=utf-8", jsonBody, ParameterType.RequestBody);
            }

            //var requestString = JsonConvert.SerializeObject(request);
            var response = restClient.Execute(request);
            var result = response.Content;
            //var responseString = JsonConvert.SerializeObject(response);

            return result;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="contentUrl"></param>
        /// <returns></returns>
        private static string DeleteContent(string contentUrl)
        {
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
            var restClient = new RestClient(contentUrl);
            var request = new RestRequest(Method.DELETE) { RequestFormat = DataFormat.Json };

            //var requestString = JsonConvert.SerializeObject(request);
            var response = restClient.Execute(request);
            var result = response.Content;
            //var responseString = JsonConvert.SerializeObject(response);

            return result;
        }

        #endregion

        #region Contentful Entry Creation Methods
        //private byte[] getimagefile(string filepath)
        //{
        //    //filepath = @"C:\temp\Contentful\Small.jpg";

        //    //var file = System.IO.Directory.GetFiles(filepath);

        //    byte[] fileBytes;

        //    if (System.IO.File.Exists(filepath))
        //    {
        //        fileBytes = System.IO.File.ReadAllBytes(filepath);
        //    }
        //    else
        //    {
        //        return null;
        //    }

        //    return fileBytes;


        //}

        private string CreateContentfulEntryAsset(IList<string> fields, string assetCache)
        {

            var titleEnglish = GetStringValue(fields[0]);
            var descriptionEnglish = GetStringValue(fields[1]);
            var englishFileContentType = GetStringValue(fields[2]);
            var englishFileName = GetStringValue(fields[3]);
            var englishFileurl = GetStringValue(fields[4]);
            //var spanishFileContentType = GetStringValue(fields[5]);
            //var spanishFileName = GetStringValue(fields[6]);
            //var spanishFileurl = GetStringValue(fields[7]);
            //var russianFileContentType = GetStringValue(fields[8]);
            //var russianFileName = GetStringValue(fields[9]);
            //var russianFileurl = GetStringValue(fields[10]);


            var titleSpanish = TranslateText(titleEnglish, ContentConstants.StringLocaleEnglishToSpanish);
            var titleRussian = TranslateText(titleEnglish, ContentConstants.StringLocaleEnglishToRussian);

            var descriptionSpanish = TranslateText(descriptionEnglish, ContentConstants.StringLocaleEnglishToSpanish);
            var descriptionRussian = TranslateText(descriptionEnglish, ContentConstants.StringLocaleEnglishToRussian);

            var asset = new ContentfulAsset
            {
                Fields =
                    new AssetFields
                    {
                        Title = new AssetText { EnUs = titleEnglish, EsEs = titleSpanish, RuRu = titleRussian },
                        Description = new AssetText { EnUs = descriptionEnglish, EsEs = descriptionSpanish, RuRu = descriptionRussian },
                        File = new AssetFile { EnFile = new AssetFileInfo { ContentType = englishFileContentType, FileName = englishFileName, FileUrl = englishFileurl }, EsFile = new AssetFileInfo { ContentType = englishFileContentType, FileName = englishFileName, FileUrl = englishFileurl }, RuFile = new AssetFileInfo { ContentType = englishFileContentType, FileName = englishFileName, FileUrl = englishFileurl } }
                    }
            };

            // get the version number
            var version = GetVersionAsset(titleEnglish,  assetCache);

            var jsonString = JsonConvert.SerializeObject(asset);
            var url = string.Format(TemplateUrlAsset, DefaultSpace, titleEnglish, AccessToken);
            var result = CreateContentAsset(url, jsonString, version);
            var englishProcessUrl = string.Format(TemplateUrlEnglishProcessAsset, DefaultSpace, titleEnglish, AccessToken);
            ProcessContentAsset(englishProcessUrl);

            var spanishProcessUrl = string.Format(TemplateUrlSpanishProcessAsset, DefaultSpace, titleEnglish, AccessToken);
            ProcessContentAsset(spanishProcessUrl);

            var russianProcessUrl = string.Format(TemplateUrlRussianProcessAsset, DefaultSpace, titleEnglish, AccessToken);
            ProcessContentAsset(russianProcessUrl);

            return result;

        }

        private string CreateContentfulEntryAction(IList<string> fields, string contentCache)
        {

            const string id = "action";

            var key = GetKeyValue(fields[0]);
            var nameKeySysId = GetSysId(fields[1]);
            var conditions = GetStringValue(fields[2]);
            var descriptionKeySysId = GetSysId(fields[3]);
            var disable = GetBooleanValue(fields[4]);
            var hide = GetBooleanValue(fields[5]);
            var videoKeySysId = GetSysId(fields[6]);
            var imageKeySysId = GetSysId(fields[7]);
            var iconClass = GetStringValue(fields[8]);
            var commodityKeySysId = GetSysId(fields[9]);
            var actionTypeKey = GetStringValue(fields[10]);
            var appliances = GetStringValue(fields[11]);
            var upfrontCost = GetDecimalValue(fields[12]);
            var annualCost = GetDecimalValue(fields[13]);
            var costVariancePercent = GetDecimalValue(fields[14]);
            var difficulty = GetStringValue(fields[15]);
            var habitInterval = GetStringValue(fields[16]);
            var annualSavingsEstimate = GetDecimalValue(fields[17]);
            var annualUsageSavingsEstimate = GetDecimalValue(fields[18]);
            var annualUsageSavingsUomKeySysId = GetSysId(fields[19]);
            var defaultPaybackTime = GetIntValue(fields[20]);
            var defaultRoi = GetDecimalValue(fields[21]);
            var rebateAvailable = GetBooleanValue(fields[22]);
            var rebateUrl = GetStringValue(fields[23]);
            var rebateImageKeySysId = GetSysId(fields[24]);
            var rebateAmount = GetOptionalDecimalValue(fields[25]);
            var seasons = GetStringValue(fields[26]);
            var comments = GetStringValue(fields[27]);
            var actionPriority = GetIntValue(fields[28]);
            var savingsCalcMethod = GetStringValue(fields[29]);
            var savingsAmount = GetOptionalDecimalValue(fields[30]);
            var whatIfDataKeys = GetStringValue(fields[31]);
            var costExpression = GetStringValue(fields[32]);
            var rebateExpression = GetStringValue(fields[33]);
            var nextStepLink = GetStringValue(fields[34]);
            var nextStepLinkTextSysId = GetSysId(fields[35]);
            var nextStepLinkType = GetStringValue(fields[36]);
            var tags = GetStringValue(fields[37]);
            var savingsMin = GetOptionalDecimalValue(fields[38]);
            var savingsMax = GetOptionalDecimalValue(fields[39]);

            var conditionSysIds = GetSysIds(conditions);
            var applianceSysIds = GetSysIds(appliances);
            var seasonSysIds = GetSysIds(seasons);
            var whatIfDataSysIds = GetSysIds(whatIfDataKeys);
            var costExpressionSysId = GetSysIds(costExpression);
            var rebateExpressionSysId = GetSysIds(rebateExpression);

            if (string.IsNullOrEmpty(nameKeySysId))
            {
                nameKeySysId = "common.undefined";
            }

            var action = new ContentfulAction
            {
                Fields =
                    new ActionFields
                    {
                        ActionKey = new EnglishText { EnUs = key },
                        NameKey = new EnglishEntryLink { EnUs = new EntryLink { Link = new EntryLinkAttributes { Id = nameKeySysId } } },
                        DescriptionKey = new EnglishEntryLink { EnUs = new EntryLink { Link = new EntryLinkAttributes { Id = descriptionKeySysId } } },
                        Disable = new EnglishBoolean { EnUs = disable },
                        Hide = new EnglishBoolean { EnUs = hide },
                        VideoKey = !string.IsNullOrEmpty(videoKeySysId) ? new EnglishEntryLink { EnUs = new EntryLink { Link = new EntryLinkAttributes { Id = videoKeySysId } } } : null,
                        ImageKey = !string.IsNullOrEmpty(imageKeySysId) ? new EnglishEntryLink { EnUs = new EntryLink { Link = new EntryLinkAttributes { Id = imageKeySysId } } } : null,
                        IconClass = !string.IsNullOrEmpty(iconClass) ? new EnglishText {EnUs = iconClass} : null,
                        CommodityKey = new EnglishEntryLink { EnUs = new EntryLink { Link = new EntryLinkAttributes { Id = commodityKeySysId } } },
                        ActionTypeKey = new EnglishText { EnUs = actionTypeKey },
                        UpfrontCost = new EnglishDecimal { EnUs = upfrontCost },
                        AnnualCost = new EnglishDecimal { EnUs = annualCost },
                        CostVariancePercent = new EnglishDecimal { EnUs = costVariancePercent },
                        Difficulty = new EnglishText { EnUs = difficulty },
                        HabitInterval = new EnglishText { EnUs = habitInterval },
                        DefaultAnnualSavingsEstimate = new EnglishDecimal { EnUs = annualSavingsEstimate },
                        DefaultAnnualUsageSavingsEstimate = new EnglishDecimal { EnUs = annualUsageSavingsEstimate },
                        AnnualUsageSavingsUomKey = !string.IsNullOrEmpty(annualUsageSavingsUomKeySysId) ? new EnglishEntryLink { EnUs = new EntryLink { Link = new EntryLinkAttributes { Id = annualUsageSavingsUomKeySysId } } } : null,
                        DefaultPaybackTime = new EnglishNumber { EnUs = defaultPaybackTime },
                        DefaultRoi = new EnglishDecimal { EnUs = defaultRoi },
                        RebateAvailable = new EnglishBoolean { EnUs = rebateAvailable },
                        RebateUrl = !string.IsNullOrEmpty(rebateUrl) ? new EnglishText { EnUs = rebateUrl } : null,
                        RebateImageKey = !string.IsNullOrEmpty(rebateImageKeySysId) ? new EnglishEntryLink { EnUs = new EntryLink { Link = new EntryLinkAttributes { Id = rebateImageKeySysId } } } : null,
                        RebateAmount = rebateAmount.HasValue ? new EnglishDecimal { EnUs = rebateAmount.Value } : null,
                        Comments = !string.IsNullOrEmpty(comments) ? new EnglishText { EnUs = comments } : null,
                        ActionPriority = new EnglishNumber { EnUs = actionPriority },
                        SavingsCalcMethod = new EnglishText { EnUs = savingsCalcMethod },
                        SavingsAmount = savingsAmount.HasValue ? new EnglishDecimal { EnUs = savingsAmount.Value } : null,
                        CostExpression = !string.IsNullOrEmpty(costExpressionSysId) ? new EnglishEntryLink { EnUs = new EntryLink { Link = new EntryLinkAttributes { Id = costExpressionSysId } } } : null,
                        RebateExpression = !string.IsNullOrEmpty(rebateExpressionSysId) ? new EnglishEntryLink { EnUs = new EntryLink { Link = new EntryLinkAttributes { Id = rebateExpressionSysId } } } : null,
                        NextStepLink = !string.IsNullOrEmpty(nextStepLink) ? new EnglishText { EnUs = nextStepLink } : null,
                        NextStepLinkText = !string.IsNullOrEmpty(nextStepLinkTextSysId) ? new EnglishEntryLink { EnUs = new EntryLink { Link = new EntryLinkAttributes { Id = nextStepLinkTextSysId } } } : null,
                        NextStepLinkType = !string.IsNullOrEmpty(nextStepLinkType) ? new EnglishText { EnUs = nextStepLinkType } : null,
                        Tags =  !string.IsNullOrEmpty(tags) ? new EnglishText {EnUs = tags} : null,
                        MinimumSavingsAmt = savingsMin.HasValue ? new EnglishDecimal { EnUs = savingsMin.Value } : null,
                        MaximumSavingsAmt = savingsMax.HasValue ? new EnglishDecimal { EnUs = savingsMax.Value } : null
                    }
            };

            if (conditionSysIds != string.Empty)
            {
                action.Fields.ConditionKeys = new EnglishEntryLinks { EnUs = new List<EntryLink>() };
                foreach (var condition in conditionSysIds.Split(';').Select(conditionSysId => new EntryLink { Link = new EntryLinkAttributes { Id = conditionSysId } }))
                {
                    action.Fields.ConditionKeys.EnUs.Add(condition);
                }
            }

            if (applianceSysIds != string.Empty)
            {
                action.Fields.ApplianceKeys = new EnglishEntryLinks { EnUs = new List<EntryLink>() };
                foreach (var appliance in applianceSysIds.Split(';').Select(applianceSysId => new EntryLink { Link = new EntryLinkAttributes { Id = applianceSysId } }))
                {
                    action.Fields.ApplianceKeys.EnUs.Add(appliance);
                }
            }

            if (seasonSysIds != string.Empty)
            {
                action.Fields.SeasonKeys = new EnglishEntryLinks { EnUs = new List<EntryLink>() };
                foreach (var season in seasonSysIds.Split(';').Select(seasonSysId => new EntryLink { Link = new EntryLinkAttributes { Id = seasonSysId } }))
                {
                    action.Fields.SeasonKeys.EnUs.Add(season);
                }
            }

            if (whatIfDataSysIds != string.Empty)
            {
                action.Fields.WhatIfDataKeys = new EnglishEntryLinks { EnUs = new List<EntryLink>() };
                foreach (var whatifdata in whatIfDataSysIds.Split(';').Select(whatIfDataSysId => new EntryLink { Link = new EntryLinkAttributes { Id = whatIfDataSysId } }))
                {
                    action.Fields.WhatIfDataKeys.EnUs.Add(whatifdata);
                }
            }

            // get version for the content entry
            var version = GetVersionContentEntry(key, ContentType.Action, contentCache);

            var jsonString = JsonConvert.SerializeObject(action);
            var url = string.Format(TemplateUrl, DefaultSpace, key, AccessToken);
            var result = CreateContentEntry(url, jsonString, id, version);
            return result;

        }

        private string CreateContentfulEntryAppliance(IList<string> fields, string contentCache)
        {

            const string id = "appliance";

            var key = GetKeyValue(fields[0]);
            var nameKeySysId = GetSysId(fields[1]);
            var averageCostPerYear = GetDecimalValue(fields[2]);
            var profileAttributes = GetStringValue(fields[3]);
            var expressions = GetStringValue(fields[4]);
            var profileAttributeSysIds = GetSysIds(profileAttributes);
            var expressionSysIds = GetSysIds(expressions);

            var appliance = new ContentfulAppliance
            {
                Fields =
                    new ApplianceFields
                    {
                        ApplianceKey = new EnglishText { EnUs = key },
                        NameKey = new EnglishEntryLink { EnUs = new EntryLink { Link = new EntryLinkAttributes { Id = nameKeySysId } } },
                        AverageCostPerYear = new EnglishDecimal { EnUs = averageCostPerYear }
                    }
            };

            if (profileAttributeSysIds != string.Empty)
            {
                appliance.Fields.ProfileAttributes = new EnglishEntryLinks { EnUs = new List<EntryLink>() };
                foreach (var attribute in profileAttributeSysIds.Split(';').Select(profileAttributeSysId => new EntryLink { Link = new EntryLinkAttributes { Id = profileAttributeSysId } }))
                {
                    appliance.Fields.ProfileAttributes.EnUs.Add(attribute);
                }
            }

            if (expressionSysIds != string.Empty)
            {
                appliance.Fields.Expressions = new EnglishEntryLinks { EnUs = new List<EntryLink>() };
                foreach (var expression in expressionSysIds.Split(';').Select(expressionSysId => new EntryLink { Link = new EntryLinkAttributes { Id = expressionSysId } }))
                {
                    appliance.Fields.Expressions.EnUs.Add(expression);
                }
            }

            // get version for the content entry
            var version = GetVersionContentEntry(key, ContentType.Appliance,contentCache);

            var jsonString = JsonConvert.SerializeObject(appliance);
            var url = string.Format(TemplateUrl, DefaultSpace, key, AccessToken);
            var result = CreateContentEntry(url, jsonString, id, version);
            return result;

        }

        private string CreateContentfulEntryBenchmarkGroup(IList<string> fields, string contentCache)
        {

            const string id = "benchmarkgroup";

            var key = GetKeyValue(fields[0]);
            var nameKeySysId = GetSysId(fields[1]);
            var descriptionKeySysId = GetSysId(fields[2]);
            var disable = GetBooleanValue(fields[3]);
            var hide = GetBooleanValue(fields[4]);

            var benchmarkGroup = new ContentfulBenchmarkGroup
            {
                Fields =
                    new BenchmarkGroupFields
                    {
                        BenchmarkGroupKey = new EnglishText { EnUs = key },
                        NameKey = new EnglishEntryLink { EnUs = new EntryLink { Link = new EntryLinkAttributes { Id = nameKeySysId } } },
                        DescriptionKey = new EnglishEntryLink { EnUs = new EntryLink { Link = new EntryLinkAttributes { Id = descriptionKeySysId } } },
                        Disable = new EnglishBoolean { EnUs = disable },
                        Hide = new EnglishBoolean { EnUs = hide }
                    }
            };

            // get version for the content entry
            var version = GetVersionContentEntry(key, ContentType.BenchmarkGroup, contentCache);

            var jsonString = JsonConvert.SerializeObject(benchmarkGroup);
            var url = string.Format(TemplateUrl, DefaultSpace, key, AccessToken);
            var result = CreateContentEntry(url, jsonString, id, version);
            return result;

        }

        private string CreateContentfulEntryCommodity(IList<string> fields, string contentCache)
        {

            const string id = "commodity";

            var key = GetKeyValue(fields[0]);
            var nameKeySysId = GetSysId(fields[1]);

            var commodity = new ContentfulCommodity
            {
                Fields =
                    new CommodityFields
                    {
                        CommodityKey = new EnglishText { EnUs = key },
                        NameKey = new EnglishEntryLink { EnUs = new EntryLink { Link = new EntryLinkAttributes { Id = nameKeySysId } } }
                    }
            };

            // get version for the content entry
            var version = GetVersionContentEntry(key, ContentType.Commodity, contentCache);

            var jsonString = JsonConvert.SerializeObject(commodity);
            var url = string.Format(TemplateUrl, DefaultSpace, key, AccessToken);
            var result = CreateContentEntry(url, jsonString, id, version);
            return result;

        }

        private string CreateContentfulEntryCondition(IList<string> fields, string contentCache)
        {

            const string id = "condition";

            var key = GetKeyValue(fields[0]);
            var category = GetStringValue(fields[1]);
            var description = GetStringValue(fields[2]);
            var profileAttributeSysId = GetSysId(fields[3]);
            var conditionOperator = GetStringValue(fields[4]);
            var value = GetStringValue(fields[5]);
            var profileOptionSysId = GetSysId(fields[6]);

            var condition = new ContentfulCondition
            {
                Fields =
                    new ConditionFields
                    {
                        ConditionKey = new EnglishText { EnUs = key },
                        CategoryKey = new EnglishText { EnUs = category },
                        Description = new EnglishText { EnUs = description },
                        ProfileAttributeKey = new EnglishEntryLink { EnUs = new EntryLink { Link = new EntryLinkAttributes { Id = profileAttributeSysId } } },
                        ConditionOperator = new EnglishText { EnUs = conditionOperator },
                        Value = !string.IsNullOrEmpty(value) ? new EnglishText { EnUs = value } : null,
                        ProfileOptionKey = new EnglishEntryLink { EnUs = new EntryLink { Link = new EntryLinkAttributes { Id = profileOptionSysId } } }
                    }
            };

            // get version for the content entry
            var version = GetVersionContentEntry(key, ContentType.Condition, contentCache);

            var jsonString = JsonConvert.SerializeObject(condition);
            var url = string.Format(TemplateUrl, DefaultSpace, key, AccessToken);
            var result = CreateContentEntry(url, jsonString, id, version);
            return result;

        }

        private string CreateContentfulEntryConfiguration(IList<string> fields, string contentCache)
        {

            const string id = "configuration";

            var key = GetKeyValue(fields[0]);
            var category = GetStringValue(fields[1]);
            var description = GetStringValue(fields[2]);
            var value = GetStringValue(fields[3]);
            var environment = GetStringValue(fields[4]);

            var configuration = new ContentfulConfiguration
            {
                Fields =
                    new ConfigurationFields
                    {
                        ConfigurationKey = new EnglishText { EnUs = key },
                        CategoryKey = new EnglishText { EnUs = category },
                        EnvironmentKey = new EnglishText { EnUs = environment },
                        Description = new EnglishText { EnUs = description },
                        Value = new EnglishText { EnUs = value }
                    }
            };

            // get version for the content entry
            var version = GetVersionContentEntry(key, ContentType.Configuration, contentCache);

            var jsonString = JsonConvert.SerializeObject(configuration);
            var url = string.Format(TemplateUrl, DefaultSpace, key, AccessToken);
            var result = CreateContentEntry(url, jsonString, id, version);
            return result;

        }

        /// <summary>
        /// Create Configuration Bulk for Contentful
        /// TFS 598 Aug 2015
        /// </summary>
        /// <param name="fields"></param>
        /// <param name="contentCache"></param>
        /// <returns></returns>
        private string CreateContentfulEntryConfigurationBulk(IList<string> fields, string contentCache)
        {

            const string id = "configurationbulk";

            var key = GetKeyValue(fields[0]);
            var name = GetStringValue(fields[1]);
            var category = GetStringValue(fields[2]);
            var description = GetStringValue(fields[3]);
            var value = GetStringValue(fields[4]);
            var environment = GetStringValue(fields[5]);

            var configurationBulk = new ContentfulConfigurationBulk
            {
                Fields =
                    new ConfigurationBulkFields()
                    {
                        ConfigurationKey = new EnglishText { EnUs = key },
                        CategoryKey = new EnglishText { EnUs = category },
                        EnvironmentKey = new EnglishText { EnUs = environment },
                        Description = new EnglishText { EnUs = description },
                        JsonConfiguation = new EnglishObject { EnUs = (JObject)JsonConvert.DeserializeObject(value) },
                        Name = new EnglishText { EnUs = name }
                    }
            };

            // get version for the content entry
            var version = GetVersionContentEntry(key, ContentType.ConfigurationBulk, contentCache);

            var jsonString = JsonConvert.SerializeObject(configurationBulk);
            var url = string.Format(TemplateUrl, DefaultSpace, key, AccessToken);
            var result = CreateContentEntry(url, jsonString, id, version);
            return result;

        }

        private string CreateContentfulEntryCurrency(IList<string> fields, string contentCache)
        {

            const string id = "currency";

            var key = GetKeyValue(fields[0]);
            var nameKeySysId = GetSysId(fields[1]);
            var symbolKeySysId = GetSysId(fields[2]);

            var currency = new ContentfulCurrency
            {
                Fields =
                    new CurrencyFields
                    {
                        CurrencyKey = new EnglishText { EnUs = key },
                        NameKey = new EnglishEntryLink { EnUs = new EntryLink { Link = new EntryLinkAttributes { Id = nameKeySysId } } },
                        SymbolKey = new EnglishEntryLink { EnUs = new EntryLink { Link = new EntryLinkAttributes { Id = symbolKeySysId } } }
                    }
            };

            // get version for the content entry
            var version = GetVersionContentEntry(key, ContentType.Currency, contentCache);

            var jsonString = JsonConvert.SerializeObject(currency);
            var url = string.Format(TemplateUrl, DefaultSpace, key, AccessToken);
            var result = CreateContentEntry(url, jsonString, id, version);
            return result;

        }

        private string CreateContentfulEntryEnduse(IList<string> fields, string contentCache)
        {

            const string id = "enduse";

            var key = GetKeyValue(fields[0]);
            var nameKeySysId = GetSysId(fields[1]);
            var endUseCategoryKey = GetStringValue(fields[2]);
            var disable = GetBooleanValue(fields[3]);
            var hide = GetBooleanValue(fields[4]);
            var appliances = GetStringValue(fields[5]);
            var applianceSysIds = GetSysIds(appliances);

            var enduse = new ContentfulEndUse
            {
                Fields =
                    new EndUseFields
                    {
                        EndUseKey = new EnglishText { EnUs = key },
                        NameKey = new EnglishEntryLink { EnUs = new EntryLink { Link = new EntryLinkAttributes { Id = nameKeySysId } } },
                        EndUseCategoryKey = new EnglishText { EnUs = endUseCategoryKey },
                        Disable = new EnglishBoolean { EnUs = disable },
                        Hide = new EnglishBoolean { EnUs = hide }
                    }
            };

            if (applianceSysIds != string.Empty)
            {
                enduse.Fields.Appliances = new EnglishEntryLinks { EnUs = new List<EntryLink>() };
                foreach (var appliance in applianceSysIds.Split(';').Select(applianceSysId => new EntryLink { Link = new EntryLinkAttributes { Id = applianceSysId } }))
                {
                    enduse.Fields.Appliances.EnUs.Add(appliance);
                }
            }

            // get version for the content entry
            var version = GetVersionContentEntry(key, ContentType.Enduse, contentCache);

            var jsonString = JsonConvert.SerializeObject(enduse);
            var url = string.Format(TemplateUrl, DefaultSpace, key, AccessToken);
            var result = CreateContentEntry(url, jsonString, id, version);
            return result;

        }


        private string CreateContentfulEntryEnumerationItem(IList<string> fields, string contentCache)
        {

            const string id = "enumerationitem";

            var key = GetKeyValue(fields[0]);
            var nameKeySysId = GetSysId(fields[1]);

            var ei = new ContentfulEnumerationItem
            {
                Fields =
                    new EnumerationItemFields
                    {
                        EnumerationItemKey = new EnglishText { EnUs = key },
                        NameKey = new EnglishEntryLink { EnUs = new EntryLink { Link = new EntryLinkAttributes { Id = nameKeySysId } } },
                    }

            };

            // get version for the content entry
            var version = GetVersionContentEntry(key, ContentType.EnumerationItem, contentCache);

            var jsonString = JsonConvert.SerializeObject(ei);
            var url = string.Format(TemplateUrl, DefaultSpace, key, AccessToken);
            var result = CreateContentEntry(url, jsonString, id, version);
            return result;

        }

        private string CreateContentfulEntryEnumeration(IList<string> fields, string contentCache)
        {

            const string id = "enumeration";

            var key = GetKeyValue(fields[0]);
            var category = GetKeyValue(fields[1]);
            var enumerationitems = GetKeyValue(fields[2]);
            var eiSysIds = GetSysIds(enumerationitems);
            var defaultei = GetSysId(fields[3]);


            var enumeration = new ContentfulEnumeration
            {
                Fields =
                    new EnumerationFields
                    {
                        EnumerationKey = new EnglishText { EnUs = key },
                        Category = new EnglishText { EnUs = category },
                        DefaultEnumerationItem = new EnglishEntryLink { EnUs = new EntryLink { Link = new EntryLinkAttributes { Id = defaultei } } },
                    }

            };

            if (eiSysIds != string.Empty)
            {
                enumeration.Fields.EnumerationItems = new EnglishEntryLinks { EnUs = new List<EntryLink>() };
                foreach (var ei in eiSysIds.Split(';').Select(eiSysId => new EntryLink { Link = new EntryLinkAttributes { Id = eiSysId } }))
                {
                    enumeration.Fields.EnumerationItems.EnUs.Add(ei);
                }
            }

            // get version for the content entry
            var version = GetVersionContentEntry(key, ContentType.Enumeration, contentCache);

            var jsonString = JsonConvert.SerializeObject(enumeration);
            var url = string.Format(TemplateUrl, DefaultSpace, key, AccessToken);
            var result = CreateContentEntry(url, jsonString, id, version);
            return result;

        }

        private string CreateContentfulEntryExpression(IList<string> fields, string contentCache)
        {

            const string id = "expression";

            var key = GetKeyValue(fields[0]);
            var name = GetStringValue(fields[1]);
            var description = GetStringValue(fields[2]);
            var formula = GetStringValue(fields[3]);
            var comments = GetStringValue(fields[4]);

            var expression = new ContentfulExpression
            {
                Fields =
                    new ExpressionFields
                    {
                        ExpressionKey = new EnglishText { EnUs = key },
                        Name = new EnglishText { EnUs = name },
                        Description = new EnglishText { EnUs = description },
                        Formula = new EnglishText { EnUs = formula },
                        Comments = new EnglishText { EnUs = comments }
                    }
            };

            // get version for the content entry
            var version = GetVersionContentEntry(key, ContentType.Expression, contentCache);

            var jsonString = JsonConvert.SerializeObject(expression);
            var url = string.Format(TemplateUrl, DefaultSpace, key, AccessToken);
            var result = CreateContentEntry(url, jsonString, id, version);
            return result;

        }

        private string CreateContentfulEntryFileContent(IList<string> fields, string contentCache)
        {

            const string id = "filecontent";

            var key = GetKeyValue(fields[0]);
            var type = GetStringValue(fields[1]);
            var category = GetStringValue(fields[2]);
            var smallFileSysId = GetStringValue(fields[3]);
            var mediumFileSysId = GetStringValue(fields[4]);
            var largeFileSysId = GetStringValue(fields[5]);

            var filecontent = new ContentfulFileContent
            {
                Fields =
                    new FileContentFields
                    {
                        FileContentKey = new EnglishText { EnUs = key },
                        Type = new EnglishText { EnUs = type },
                        Category = new EnglishText { EnUs = category },
                        SmallFile = new EnglishAssetLink { EnUs = new AssetLink { Link = new AssetLinkAttributes { Id = smallFileSysId } } },
                        MediumFile = new EnglishAssetLink { EnUs = new AssetLink { Link = new AssetLinkAttributes { Id = mediumFileSysId } } },
                        LargeFile = new EnglishAssetLink { EnUs = new AssetLink { Link = new AssetLinkAttributes { Id = largeFileSysId } } },
                    }
            };

            // get version for the content entry
            var version = GetVersionContentEntry(key, ContentType.FileContent, contentCache);

            var jsonString = JsonConvert.SerializeObject(filecontent);
            var url = string.Format(TemplateUrl, DefaultSpace, key, AccessToken);
            var result = CreateContentEntry(url, jsonString, id,version);
            return result;

        }
        private string CreateContentfulEntryLayout(IList<string> fields, string contentCache)
        {

            const string id = "layout";

            var key = GetKeyValue(fields[0]);
            var description = GetStringValue(fields[1]);
            var layoutJson = GetStringValue(fields[2]);

            var layout = new ContentfulLayout
            {
                Fields =
                    new LayoutFields
                    {
                        LayoutKey = new EnglishText { EnUs = key },
                        Description = new EnglishText { EnUs = description },
                        LayoutJson = new EnglishObject { EnUs = (JObject)JsonConvert.DeserializeObject(layoutJson) },
                    }
            };

            // get version for the content entry
            var version = GetVersionContentEntry(key, ContentType.Layout, contentCache);

            var jsonString = JsonConvert.SerializeObject(layout);
            var url = string.Format(TemplateUrl, DefaultSpace, key, AccessToken);
            var result = CreateContentEntry(url, jsonString, id, version);
            return result;

        }

        private string CreateContentfulEntryMeasurement(IList<string> fields, string contentCache)
        {

            const string id = "measurement";

            var key = GetKeyValue(fields[0]);
            var nameKeySysId = GetSysId(fields[1]);
            var calculation = GetStringValue(fields[2]);
            var comments = GetStringValue(fields[3]);

            var measurement = new ContentfulMeasurement
            {
                Fields =
                    new MeasurementFields
                    {
                        MeasurementKey = new EnglishText { EnUs = key },
                        NameKey = new EnglishEntryLink { EnUs = new EntryLink { Link = new EntryLinkAttributes { Id = nameKeySysId } } },
                        Calculation = new EnglishText { EnUs = calculation },
                        Comments = !string.IsNullOrEmpty(comments) ? new EnglishText { EnUs = comments } : null
                    }
            };

            // get version for the content entry
            var version = GetVersionContentEntry(key, ContentType.Measurement, contentCache);

            var jsonString = JsonConvert.SerializeObject(measurement);
            var url = string.Format(TemplateUrl, DefaultSpace, key, AccessToken);
            var result = CreateContentEntry(url, jsonString, id, version);
            return result;

        }

        private string CreateContentfulEntryProfileDefault(IList<string> fields, string contentCache)
        {

            const string id = "profiledefault";

            var profileDefaultCollectionKey = GetStringValue(fields[1]);
            var profileDefaultCollectionKeySysId = GetSysId(profileDefaultCollectionKey);
            // sprint 4 - create contentful entry with profiledefaultcollectionname prefix to the key to avoid dupl syskey id error on contentfule
            string key;
            if (profileDefaultCollectionKey == "aclara")
                key = GetKeyValue(fields[0]);
            else
                key = string.Format("{0}.{1}", profileDefaultCollectionKeySysId, GetKeyValue(fields[0]));
            var profileAttributeKeySysId = GetSysId(fields[2]);
            var defaultKeySysId = GetSysId(fields[3]);
            var defaultValue = GetStringValue(fields[4]);

            var profiledefault = new ContentfulProfileDefault
            {
                Fields =
                    new ProfileDefaultFields
                    {
                        ProfileDefaultKey = new EnglishText { EnUs = key },
                        ProfileDefaultCollectionKey = new EnglishEntryLink { EnUs = new EntryLink { Link = new EntryLinkAttributes { Id = profileDefaultCollectionKeySysId } } },
                        ProfileAttributeKey = new EnglishEntryLink { EnUs = new EntryLink { Link = new EntryLinkAttributes { Id = profileAttributeKeySysId } } },
                        DefaultKey = new EnglishEntryLink { EnUs = new EntryLink { Link = new EntryLinkAttributes { Id = defaultKeySysId } } },
                        DefaultValue = !string.IsNullOrEmpty(defaultValue) ? new EnglishText { EnUs = defaultValue } : null
                    }
            };

            // get version for the content entry
            var version = GetVersionContentEntry(key, ContentType.ProfileDefault, contentCache);

            var jsonString = JsonConvert.SerializeObject(profiledefault);
            var url = string.Format(TemplateUrl, DefaultSpace, key, AccessToken);
            var result = CreateContentEntry(url, jsonString, id, version);
            return result;

        }

        private string CreateContentfulEntryProfileDefaultCollection(IList<string> fields, string contentCache)
        {

            const string id = "profiledefaultcollection";

            var key = GetKeyValue(fields[0]);
            var nameKeySysId = GetSysId(fields[1]);
            var description = GetStringValue(fields[2]);

            var profiledefaultcollection = new ContentfulProfileDefaultCollection
            {
                Fields =
                    new ProfileDefaultCollectionFields
                    {
                        ProfileDefaultCollectionKey = new EnglishText { EnUs = key },
                        NameKey = new EnglishEntryLink { EnUs = new EntryLink { Link = new EntryLinkAttributes { Id = nameKeySysId } } },
                        Description = new EnglishText { EnUs = description }
                    }
            };

            // get version for the content entry
            var version = GetVersionContentEntry(key, ContentType.ProfileDefaultCollection, contentCache);

            var jsonString = JsonConvert.SerializeObject(profiledefaultcollection);
            var url = string.Format(TemplateUrl, DefaultSpace, key, AccessToken);
            var result = CreateContentEntry(url, jsonString, id, version);
            return result;

        }

        private string CreateContentfulEntryProfileOption(IList<string> fields, string contentCache)
        {

            const string id = "profileoption";

            var key = GetKeyValue(fields[0]);
            var profileOptionValue = GetDecimalValue(fields[1]);
            var nameKeySysId = GetSysId(fields[2]);
            var description = GetStringValue(fields[3]);

            var profileoption = new ContentfulProfileOption
            {
                Fields =
                    new ProfileOptionFields
                    {
                        ProfileOptionKey = new EnglishText { EnUs = key },
                        NameKey = new EnglishEntryLink { EnUs = new EntryLink { Link = new EntryLinkAttributes { Id = nameKeySysId } } },
                        Description = new EnglishText { EnUs = description },
                        ProfileOptionValue = new EnglishDecimal { EnUs = profileOptionValue }
                    }
            };

            // get version for the content entry
            var version = GetVersionContentEntry(key, ContentType.ProfileOption, contentCache);

            var jsonString = JsonConvert.SerializeObject(profileoption);
            var url = string.Format(TemplateUrl, DefaultSpace, key, AccessToken);
            var result = CreateContentEntry(url, jsonString, id, version);
            return result;

        }

        private string CreateContentfulEntryProfileAttribute(IList<string> fields, string contentCache)
        {

            const string id = "profileattribute";

            var key = GetKeyValue(fields[0]);
            var description = GetStringValue(fields[1]);
            var type = GetStringValue(fields[2]);
            var profileoptions = GetStringValue(fields[3]);
            var profileoptionSysIds = GetSysIds(profileoptions);
            var disableconditions = GetStringValue(fields[4]);
            var disableconditionSysIds = GetSysIds(disableconditions);
            var entitylevel = GetStringValue(fields[5]);
            var questionTextKeySysId = GetSysId(fields[6]);
            var minvalue = GetOptionalIntValue(fields[7]);
            var maxvalue = GetOptionalIntValue(fields[8]);
            var donotdefault = GetBooleanValue(fields[9]);
            var disable = GetBooleanValue(fields[10]);

            var profileattribute = new ContentfulProfileAttribute
            {
                Fields =
                    new ProfileAttributeFields
                    {
                        ProfileAttributeKey = new EnglishText { EnUs = key },
                        Description = new EnglishText { EnUs = description },
                        ProfileAttributeTypeKey = new EnglishText { EnUs = type },
                        EntityLevelKey = new EnglishText { EnUs = entitylevel },
                        QuestionTextKey = new EnglishEntryLink { EnUs = new EntryLink { Link = new EntryLinkAttributes { Id = questionTextKeySysId } } },
                        MinValue = minvalue.HasValue ? new EnglishNumber { EnUs = minvalue.Value } : null,
                        MaxValue = maxvalue.HasValue ? new EnglishNumber { EnUs = maxvalue.Value } : null,
                        DoNotDefault = new EnglishBoolean { EnUs = donotdefault },
                        Disable = new EnglishBoolean { EnUs = disable },
                    }
            };

            if (profileoptionSysIds != string.Empty)
            {
                profileattribute.Fields.ProfileOptions = new EnglishEntryLinks { EnUs = new List<EntryLink>() };
                foreach (var profileOption in profileoptions.Split(';').Select(profileOptionSysId => new EntryLink { Link = new EntryLinkAttributes { Id = profileOptionSysId } }))
                {
                    profileattribute.Fields.ProfileOptions.EnUs.Add(profileOption);
                }
            }

            if (disableconditionSysIds != string.Empty)
            {
                profileattribute.Fields.DisableConditions = new EnglishEntryLinks { EnUs = new List<EntryLink>() };
                foreach (var condition in disableconditions.Split(';').Select(conditionSysId => new EntryLink { Link = new EntryLinkAttributes { Id = conditionSysId } }))
                {
                    profileattribute.Fields.DisableConditions.EnUs.Add(condition);
                }
            }

            // get version for the content entry
            var version = GetVersionContentEntry(key, ContentType.ProfileAttribute, contentCache);

            var jsonString = JsonConvert.SerializeObject(profileattribute);
            var url = string.Format(TemplateUrl, DefaultSpace, key, AccessToken);
            var result = CreateContentEntry(url, jsonString, id, version);
            return result;
        }

        /// <summary>
        /// Contentful entry for profile section
        /// TFS 587 July 2015
        /// </summary>
        /// <param name="fields"></param>
        /// <param name="contentCache"></param>
        /// <returns></returns>
        private string CreateContentfulEntryProfileSection(IList<string> fields, string contentCache)
        {

            const string id = "profilesection";

            var key = GetKeyValue(fields[0]);
            var nameKey = GetKeyValue(fields[1]);
            var descriptionKeySysId = GetSysId(fields[2]);
            var titleKeySysId = GetSysId(fields[3]);
            var subTitleKeySysId = GetSysId(fields[4]);
            var rank = GetIntValue(fields[5]);
            var profileAttributes = GetStringValue(fields[6]);
            var conditions = GetStringValue(fields[7]);
            var imageKeySysId = GetSysId(fields[8]);
            var introTextSysId = GetSysId(fields[9]);

            var profileAttributeSysIds = GetSysIds(profileAttributes);
            var conditionSysIds = GetSysIds(conditions);

            var profileSection = new ContentfulProfileSection
            {

                Fields =
                    new ProfileSectionFields
                    {
                        ProfileSectionKey = new EnglishText { EnUs = key },
                        Name = new EnglishText { EnUs = nameKey },
                        DescriptionKey = new EnglishEntryLink { EnUs = new EntryLink { Link = new EntryLinkAttributes { Id = descriptionKeySysId } } },
                        TitleKey = new EnglishEntryLink { EnUs = new EntryLink { Link = new EntryLinkAttributes { Id = titleKeySysId } } },
                        SubTitleKey = new EnglishEntryLink { EnUs = new EntryLink { Link = new EntryLinkAttributes { Id = subTitleKeySysId } } },
                        Rank = new EnglishNumber { EnUs = rank},
                        Images = new EnglishEntryLink { EnUs = new EntryLink { Link = new EntryLinkAttributes { Id = imageKeySysId } } }
                    }
            };

            if (!string.IsNullOrEmpty(introTextSysId))
            {
                profileSection.Fields.IntroTextKey = new EnglishEntryLink
                {
                    EnUs = new EntryLink { Link = new EntryLinkAttributes { Id = introTextSysId } }
                };
            }

            if (profileAttributeSysIds != string.Empty)
            {
                profileSection.Fields.ProfileAttributes = new EnglishEntryLinks{EnUs =  new List<EntryLink>()};
                foreach (var profileAttribute in profileAttributeSysIds.Split(';').Select(profileAttributeSysId => new EntryLink { Link = new EntryLinkAttributes { Id = profileAttributeSysId } }))
                {
                    profileSection.Fields.ProfileAttributes.EnUs.Add(profileAttribute);
                }
            }

            if (conditionSysIds != string.Empty)
            {
                profileSection.Fields.ConditionKeys = new EnglishEntryLinks { EnUs = new List<EntryLink>() };
                foreach (var condition in conditionSysIds.Split(';').Select(conditionSysId => new EntryLink { Link = new EntryLinkAttributes { Id = conditionSysId } }))
                {
                    profileSection.Fields.ConditionKeys.EnUs.Add(condition);
                }
            }
            

            // get version for the content entry
            var version = GetVersionContentEntry(key, ContentType.ProfileSection, contentCache);

            var jsonString = JsonConvert.SerializeObject(profileSection);
            var url = string.Format(TemplateUrl, DefaultSpace, key, AccessToken);
            var result = CreateContentEntry(url, jsonString, id, version);
            return result;
        }

        private string CreateContentfulEntrySeason(IList<string> fields, string contentCache)
        {

            const string id = "season";

            var key = GetKeyValue(fields[0]);
            var nameKeySysId = GetSysId(fields[1]);
            var startMonth = GetIntValue(fields[2]);
            var startDay = GetIntValue(fields[3]);
            var endMonth = GetIntValue(fields[4]);
            var endDay = GetIntValue(fields[5]);

            var season = new ContentfulSeason
            {
                Fields =
                    new SeasonFields
                    {
                        SeasonKey = new EnglishText { EnUs = key },
                        NameKey = new EnglishEntryLink { EnUs = new EntryLink { Link = new EntryLinkAttributes { Id = nameKeySysId } } },
                        StartMonth = new EnglishNumber { EnUs = startMonth },
                        StartDay = new EnglishNumber { EnUs = startDay },
                        EndMonth = new EnglishNumber { EnUs = endMonth },
                        EndDay = new EnglishNumber { EnUs = endDay },
                    }
            };

            // get version for the content entry
            var version = GetVersionContentEntry(key, ContentType.Season, contentCache);

            var jsonString = JsonConvert.SerializeObject(season);
            var url = string.Format(TemplateUrl, DefaultSpace, key, AccessToken);
            var result = CreateContentEntry(url, jsonString, id, version);
            return result;

        }

        private string CreateContentfulEntryTab(IList<string> fields, string contentCache)
        {

            const string id = "tab";

            var key = GetKeyValue(fields[0]);
            var name = GetStringValue(fields[1]);
            var menuOrder = GetIntValue(fields[2]);
            var layoutKeySysId = GetSysId(fields[3]);
            var tabType = GetStringValue(fields[4]);
            var nameKeySysId = GetSysId(fields[5]);
            var urlString = GetStringValue(fields[6]);
            var conditions = GetStringValue(fields[7]);
            var conditionSysIds = GetSysIds(conditions);
            var configurations = GetStringValue(fields[8]);
            var configurationSysIds = GetSysIds(configurations);
            var textContentKeys = GetStringValue(fields[9]);
            var textContentSysIds = GetSysIds(textContentKeys);
            var childTabs = GetStringValue(fields[10]);
            var childTabSysIds = GetSysIds(childTabs);
            var profileSections = GetStringValue(fields[11]);
            var profileSectionSysIds = GetSysIds(profileSections);
            var widgetKeySysId = GetSysId(fields[12]);
            var tabIconTypeSysId = GetSysId(fields[13]);
            var tabIconClass = GetStringValue(fields[14]);
            var imagesSysId = GetSysId(fields[15]);
            var actions = GetStringValue(fields[16]);
            var actionSysIds = GetSysIds(actions);


            var tab = new ContentfulTab
            {
                Fields =
                    new TabFields
                    {
                        TabKey = new EnglishText { EnUs = key },
                        Name = new EnglishText { EnUs = name },
                        MenuOrder = new EnglishNumber { EnUs = menuOrder },
                        LayoutKey = new EnglishEntryLink { EnUs = new EntryLink { Link = new EntryLinkAttributes { Id = layoutKeySysId } } },
                        TabType = new EnglishText { EnUs = tabType },
                        NameKey = new EnglishEntryLink { EnUs = new EntryLink { Link = new EntryLinkAttributes { Id = nameKeySysId } } },
                        Url = new EnglishText { EnUs = urlString },
                        WidgetKey = !string.IsNullOrEmpty(widgetKeySysId) ? new EnglishEntryLink { EnUs = new EntryLink { Link = new EntryLinkAttributes { Id = widgetKeySysId } } } : null,
                        TabIconClass = new EnglishText { EnUs = tabIconClass }
                    }

            };

            // TFS 683 Aug 2015 - only create tab icon type if it's available
            if (tabIconTypeSysId != null)
                tab.Fields.TabIconType = new EnglishEntryLink
                {
                    EnUs = new EntryLink {Link = new EntryLinkAttributes {Id = tabIconTypeSysId}}
                };

            // TFS 683 Aug 2015 - only create image if it's available
            if (imagesSysId != null)
                tab.Fields.Images = new EnglishEntryLink
                {
                    EnUs = new EntryLink {Link = new EntryLinkAttributes {Id = imagesSysId}}
                };

            if (conditionSysIds != string.Empty)
            {
                tab.Fields.Conditions = new EnglishEntryLinks { EnUs = new List<EntryLink>() };
                foreach (var condition in conditionSysIds.Split(';').Select(conditionSysId => new EntryLink { Link = new EntryLinkAttributes { Id = conditionSysId } }))
                {
                    tab.Fields.Conditions.EnUs.Add(condition);
                }
            }

            if (configurationSysIds != string.Empty)
            {
                tab.Fields.Configurations = new EnglishEntryLinks { EnUs = new List<EntryLink>() };
                foreach (var configuration in configurationSysIds.Split(';').Select(configurationSysId => new EntryLink { Link = new EntryLinkAttributes { Id = configurationSysId } }))
                {
                    tab.Fields.Configurations.EnUs.Add(configuration);
                }
            }

            if (textContentSysIds != string.Empty)
            {
                tab.Fields.TextContentKeys = new EnglishEntryLinks { EnUs = new List<EntryLink>() };
                foreach (var textContent in textContentSysIds.Split(';').Select(textContentSysId => new EntryLink { Link = new EntryLinkAttributes { Id = textContentSysId } }))
                {
                    tab.Fields.TextContentKeys.EnUs.Add(textContent);
                }
            }

            if (profileSectionSysIds != string.Empty)
            {
                tab.Fields.ProfileSections = new EnglishEntryLinks { EnUs = new List<EntryLink>() };
                foreach (var profilesection in profileSectionSysIds.Split(';').Select(profilesectionSysId => new EntryLink { Link = new EntryLinkAttributes { Id = profilesectionSysId } }))
                {
                    tab.Fields.ProfileSections.EnUs.Add(profilesection);
                }
            }

            if (childTabSysIds != string.Empty)
            {
                tab.Fields.ChildTabs = new EnglishEntryLinks { EnUs = new List<EntryLink>() };
                foreach (var childTab in childTabSysIds.Split(';').Select(childTabSysId => new EntryLink { Link = new EntryLinkAttributes { Id = childTabSysId } }))
                {
                    tab.Fields.ChildTabs.EnUs.Add(childTab);
                }
            }


            if (actionSysIds != string.Empty)
            {
                tab.Fields.Actions = new EnglishEntryLinks { EnUs = new List<EntryLink>() };
                foreach (var action in actionSysIds.Split(';').Select(actionSysId => new EntryLink { Link = new EntryLinkAttributes { Id = actionSysId } }))
                {
                    tab.Fields.Actions.EnUs.Add(action);
                }
            }

            // get version for the content entry
            var version = GetVersionContentEntry(key, ContentType.Tab, contentCache);

            var jsonString = JsonConvert.SerializeObject(tab);
            var url = string.Format(TemplateUrl, DefaultSpace, key, AccessToken);
            var result = CreateContentEntry(url, jsonString, id, version);
            return result;

        }

        private string CreateContentfulEntryTextContent(IList<string> fields, string contentCache)
        {

            const string id = "textcontent";

            var key = GetKeyValue(fields[0]);
            var categoryKey = GetKeyValue(fields[1]);
            var shortTextEnglish = GetStringValue(fields[2]);
            var mediumTextEnglish = GetStringValue(fields[3]);
            var longTextEnglish = GetStringValue(fields[4]);

            var shortTextSpanish = TranslateText(shortTextEnglish, ContentConstants.StringLocaleEnglishToSpanish);
            var shortTextRussian = TranslateText(shortTextEnglish, ContentConstants.StringLocaleEnglishToRussian);
            var mediumTextSpanish = shortTextEnglish != mediumTextEnglish ? TranslateText(mediumTextEnglish, ContentConstants.StringLocaleEnglishToSpanish) : shortTextSpanish;
            var mediumTextRussian = shortTextEnglish != mediumTextEnglish ? TranslateText(mediumTextEnglish, ContentConstants.StringLocaleEnglishToRussian) : shortTextRussian;
            var longTextSpanish = shortTextEnglish != longTextEnglish ? TranslateText(longTextEnglish, ContentConstants.StringLocaleEnglishToSpanish) : shortTextSpanish;
            var longTextRussian = shortTextEnglish != longTextEnglish ? TranslateText(longTextEnglish, ContentConstants.StringLocaleEnglishToRussian) : shortTextRussian;

            var textContent = new ContentfulTextContent
            {
                Fields =
                    new TextContentFields
                    {
                        TextContentKey = new EnglishText { EnUs = key },
                        Category = new EnglishText { EnUs = categoryKey },
                        ShortText = new TextContentText { EnUs = shortTextEnglish, EsEs = shortTextSpanish, RuRu = shortTextRussian },
                        MediumText = new TextContentText { EnUs = mediumTextEnglish, EsEs = mediumTextSpanish, RuRu = mediumTextRussian },
                        LongText = new TextContentText { EnUs = longTextEnglish, EsEs = longTextSpanish, RuRu = longTextRussian }
                    }
            };

            // get version for the content entry
            var version = GetVersionContentEntry(key, ContentType.TextContent, contentCache);

            var jsonString = JsonConvert.SerializeObject(textContent);
            var url = string.Format(TemplateUrl, DefaultSpace, key, AccessToken);
            var result = CreateContentEntry(url, jsonString, id, version);
            return result;

        }

        private string CreateContentfulEntryUom(IList<string> fields, string contentCache)
        {

            const string id = "uom";

            var key = GetKeyValue(fields[0]);
            var nameKeySysId = GetSysId(fields[1]);

            var uom = new ContentfulUom
            {
                Fields =
                    new UomFields
                    {
                        UomKey = new EnglishText { EnUs = key },
                        NameKey = new EnglishEntryLink { EnUs = new EntryLink { Link = new EntryLinkAttributes { Id = nameKeySysId } } }
                    }
            };

            // get version for the content entry
            var version = GetVersionContentEntry(key, ContentType.Uom, contentCache);

            var jsonString = JsonConvert.SerializeObject(uom);
            var url = string.Format(TemplateUrl, DefaultSpace, key, AccessToken);
            var result = CreateContentEntry(url, jsonString, id, version);
            return result;

        }

        private string CreateContentfulEntryWhatIfData(IList<string> fields, string contentCache)
        {

            const string id = "whatifdata";

            var key = GetKeyValue(fields[0]);
            var profileAttributeSysId = GetSysId(fields[1]);
            var valueExpressionSysId = GetSysId(fields[2]);

            var whatifdata = new ContentfulWhatIfData
            {
                Fields =
                    new WhatIfDataFields
                    {
                        WhatIfDataKey = new EnglishText { EnUs = key },
                        ProfileAttributeKey = new EnglishEntryLink { EnUs = new EntryLink { Link = new EntryLinkAttributes { Id = profileAttributeSysId } } },
                        ValueExpressionKey = new EnglishEntryLink { EnUs = new EntryLink { Link = new EntryLinkAttributes { Id = valueExpressionSysId } } }
                    }
            };

            // get version for the content entry
            var version = GetVersionContentEntry(key, ContentType.WhatIfData, contentCache);

            var jsonString = JsonConvert.SerializeObject(whatifdata);
            var url = string.Format(TemplateUrl, DefaultSpace, key, AccessToken);
            var result = CreateContentEntry(url, jsonString, id, version);
            return result;

        }

        private string CreateContentfulEntryWidget(IList<string> fields, string contentCache)
        {

            const string id = "widget";

            var key = GetKeyValue(fields[0]);
            var name = GetStringValue(fields[1]);
            var tabKeySysId = GetSysId(fields[2]);
            var widgetType = GetStringValue(fields[3]);
            var column = GetIntValue(fields[4]);
            var row = GetIntValue(fields[5]);
            var order = GetIntValue(fields[6]);
            var conditions = GetStringValue(fields[7]);
            var conditionSysIds = GetSysIds(conditions);
            var configurations = GetStringValue(fields[8]);
            var configurationSysIds = GetSysIds(configurations);
            var textContentKeys = GetStringValue(fields[9]);
            var textContentSysIds = GetSysIds(textContentKeys);
            // TFS 284 July 2015 - intro text for widget
            var introTextSysId = GetSysId(fields[11]);

            
            var widget = new ContentfulWidget
            {
                Fields =
                    new WidgetFields
                    {
                        WidgetKey = new EnglishText { EnUs = key },
                        Name = new EnglishText { EnUs = name },
                        TabKey = new EnglishEntryLink { EnUs = new EntryLink { Link = new EntryLinkAttributes { Id = tabKeySysId } } },
                        WidgetTypeKey = new EnglishText { EnUs = widgetType },
                        Column = new EnglishNumber { EnUs = column },
                        Row = new EnglishNumber { EnUs = row },
                        Order = new EnglishNumber { EnUs = order }
                    }

            };

            if (!string.IsNullOrEmpty(introTextSysId))
            {
                widget.Fields.IntroTextKey = new EnglishEntryLink
                {
                    EnUs = new EntryLink { Link = new EntryLinkAttributes { Id = introTextSysId } }
                };
            }

            if (conditionSysIds != string.Empty)
            {
                widget.Fields.Conditions = new EnglishEntryLinks { EnUs = new List<EntryLink>() };
                foreach (var condition in conditionSysIds.Split(';').Select(conditionSysId => new EntryLink { Link = new EntryLinkAttributes { Id = conditionSysId } }))
                {
                    widget.Fields.Conditions.EnUs.Add(condition);
                }
            }

            if (configurationSysIds != string.Empty)
            {
                widget.Fields.Configurations = new EnglishEntryLinks { EnUs = new List<EntryLink>() };
                foreach (var configuration in configurationSysIds.Split(';').Select(configurationSysId => new EntryLink { Link = new EntryLinkAttributes { Id = configurationSysId } }))
                {
                    widget.Fields.Configurations.EnUs.Add(configuration);
                }
            }

            if (textContentSysIds != string.Empty)
            {
                widget.Fields.TextContentKeys = new EnglishEntryLinks { EnUs = new List<EntryLink>() };
                foreach (var textContent in textContentSysIds.Split(';').Select(textContentSysId => new EntryLink { Link = new EntryLinkAttributes { Id = textContentSysId } }))
                {
                    widget.Fields.TextContentKeys.EnUs.Add(textContent);
                }
            }

            // get version for the content entry
            var version = GetVersionContentEntry(key, ContentType.Widget, contentCache);

            var jsonString = JsonConvert.SerializeObject(widget);
            var url = string.Format(TemplateUrl, DefaultSpace, key, AccessToken);
            var result = CreateContentEntry(url, jsonString, id, version);
            return result;

        }


        #endregion

        #region Database Client Content Override Methods

        private void UpdateDatabaseClientAction(int clientId, TextFieldParser parser)
        {
            var dataTable = _contentRepository.GetDataTable(ContentDataTable.ClientActionTable);

            while (!parser.EndOfData)
            {
                var fields = parser.ReadFields();
                if (fields == null) continue;

                var key = GetKeyValue(fields[0]);
                var nameKey = GetStringValue(fields[1]);
                var conditionKeys = GetStringValue(fields[2]);
                var descriptionKey = GetStringValue(fields[3]);
                var disable = GetBooleanValue(fields[4]);
                var hide = GetBooleanValue(fields[5]);
                var videoKey = GetStringValue(fields[6]);
                var imageKey = GetStringValue(fields[7]);
                var iconClass = GetStringValue(fields[8]);
                var commodityKey = GetStringValue(fields[9]);
                var actionTypeKey = GetStringValue(fields[10]);
                var applianceKeys = GetStringValue(fields[11]);
                var upfrontCost = GetDecimalValue(fields[12]);
                var annualCost = GetDecimalValue(fields[13]);
                var costVariancePercent = GetDecimalValue(fields[14]);
                var difficultyKey = GetStringValue(fields[15]);
                var habitIntervalKey = GetStringValue(fields[16]);
                var annualSavingsEstimate = GetDecimalValue(fields[17]);
                var annualUsageSavingsEstimate = GetDecimalValue(fields[18]);
                var annualUsageSavingsUomKey = GetStringValue(fields[19]);
                var paybackTime = GetIntValue(fields[20]);
                var roi = GetDecimalValue(fields[21]);
                var rebateAvailable = GetBooleanValue(fields[22]);
                var rebateUrl = GetStringValue(fields[23]);
                var rebateImageKey = GetStringValue(fields[24]);
                var rebateAmount = GetDecimalValue(fields[25]);
                var seasonKeys = GetStringValue(fields[26]);
                var comments = GetStringValue(fields[27]);
                var actionPriority = GetOptionalIntValue(fields[28]);
                var savingsCalcMethod = GetStringValue(fields[29]);
                var savingsAmount = GetDecimalValue(fields[30]);
                var whatIfDataKeys = GetStringValue(fields[31]);
                var costExpression = GetStringValue(fields[32]);
                var rebateExpression = GetStringValue(fields[33]);
                var nextStepLink = GetStringValue(fields[34]);
                var nextStepLinkText = GetStringValue(fields[35]);
                var nextStepLinkType = GetStringValue(fields[36]);
                var tags = GetStringValue(fields[37]);
                var savingssMin = GetStringValue(fields[38]);
                var savingsMax = GetStringValue(fields[39]);


                var newRow1 = dataTable.NewRow();
                newRow1[ContentConstants.StringClientId] = clientId;
                newRow1[ContentConstants.StringActionKey] = key;
                newRow1[ContentConstants.StringActionTypeKey] = actionTypeKey;
                newRow1[ContentConstants.StringHide] = hide;
                newRow1[ContentConstants.StringDisable] = disable;
                newRow1[ContentConstants.StringCommodityKey] = commodityKey;
                newRow1[ContentConstants.StringDifficultyKey] = difficultyKey;
                newRow1[ContentConstants.StringHabitIntervalKey] = habitIntervalKey;
                newRow1[ContentConstants.StringNameKey] = nameKey;
                newRow1[ContentConstants.StringDescriptionKey] = descriptionKey;
                newRow1[ContentConstants.StringAnnualCost] = annualCost;
                newRow1[ContentConstants.StringUpfrontCost] = upfrontCost;
                newRow1[ContentConstants.StringAnnualSavingsEstimate] = annualSavingsEstimate;
                newRow1[ContentConstants.StringAnnualUsageSavingsEstimate] = annualUsageSavingsEstimate;
                newRow1[ContentConstants.StringAnnualUsageSavingsUomKey] = annualUsageSavingsUomKey;
                newRow1[ContentConstants.StringCostVariancePercent] = costVariancePercent;
                newRow1[ContentConstants.StringPaybackTime] = paybackTime;
                newRow1[ContentConstants.StringRoi] = roi;
                newRow1[ContentConstants.StringRebateAvailable] = rebateAvailable;
                newRow1[ContentConstants.StringRebateAmount] = rebateAmount;
                newRow1[ContentConstants.StringRebateUrl] = rebateUrl;
                newRow1[ContentConstants.StringRebateImageKey] = rebateImageKey;
                newRow1[ContentConstants.StringIconClass] = iconClass;
                newRow1[ContentConstants.StringImageKey] = imageKey;
                newRow1[ContentConstants.StringVideoKey] = videoKey;
                newRow1[ContentConstants.StringComments] = comments;
                newRow1[ContentConstants.StringConditionKeys] = conditionKeys;
                newRow1[ContentConstants.StringApplianceKeys] = applianceKeys;
                newRow1[ContentConstants.StringSeasonKeys] = seasonKeys;
                newRow1[ContentConstants.StringActionPriority] = actionPriority.HasValue ? (object)actionPriority : DBNull.Value;
                newRow1[ContentConstants.StringSavingsCalcMethod] = savingsCalcMethod;
                newRow1[ContentConstants.StringSavingsAmount] = savingsAmount;
                newRow1[ContentConstants.StringWhatIfDataKeys] = whatIfDataKeys;
                newRow1[ContentConstants.StringCostExpression] = costExpression;
                newRow1[ContentConstants.StringRebateExpression] = rebateExpression;
                newRow1[ContentConstants.StringNextStepLink] = nextStepLink;
                newRow1[ContentConstants.StringNextStepLinkText] = nextStepLinkText;
                newRow1[ContentConstants.StringNextStepLinkType] = nextStepLinkType;
                newRow1[ContentConstants.StringTags] = tags;
                newRow1[ContentConstants.StringMinimumSavingsAmt] = savingssMin;
                newRow1[ContentConstants.StringMaximumSavingsAmt] = savingsMax;

                dataTable.Rows.Add(newRow1);
            }

            _contentRepository.UpdateAction(dataTable);
        }

        private void UpdateDatabaseClientAppliance(int clientId, TextFieldParser parser)
        {
            var dataTable = _contentRepository.GetDataTable(ContentDataTable.ClientApplianceTable);

            while (!parser.EndOfData)
            {
                var fields = parser.ReadFields();
                if (fields == null) continue;

                var key = GetKeyValue(fields[0]);
                var nameKey = GetStringValue(fields[1]);
                var averageCostPerYear = GetDecimalValue(fields[2]);
                var profileAttributes = GetStringValue(fields[3]);
                var expressions = GetStringValue(fields[4]);


                var newRow1 = dataTable.NewRow();
                newRow1[ContentConstants.StringClientId] = clientId;
                newRow1[ContentConstants.StringApplianceKey] = key;
                newRow1[ContentConstants.StringNameKey] = nameKey;
                newRow1[ContentConstants.StringAverageCostPerYear] = averageCostPerYear;
                newRow1[ContentConstants.StringProfileAttributes] = profileAttributes;
                newRow1[ContentConstants.StringExpressions] = expressions;
                dataTable.Rows.Add(newRow1);
            }

            _contentRepository.UpdateAppliance(dataTable);

        }


        private void UpdateDatabaseClientBenchmarkGroup(int clientId, TextFieldParser parser)
        {
            var dataTable = _contentRepository.GetDataTable(ContentDataTable.ClientConfigurationTable);

            while (!parser.EndOfData)
            {
                var fields = parser.ReadFields();
                if (fields == null) continue;

                var key = GetKeyValue(fields[0]);
                var nameKey = GetStringValue(fields[1]);
                var descriptionKey = GetStringValue(fields[2]);
                var disable = GetBooleanValue(fields[3]);
                var hide = GetBooleanValue(fields[4]);

                var newRow1 = dataTable.NewRow();
                newRow1[ContentConstants.StringClientId] = clientId;
                newRow1[ContentConstants.StringBenchmarkGroupKey] = key;
                newRow1[ContentConstants.StringNameKey] = nameKey;
                newRow1[ContentConstants.StringDescriptionKey] = descriptionKey;
                newRow1[ContentConstants.StringDisable] = disable;
                newRow1[ContentConstants.StringHide] = hide;
                dataTable.Rows.Add(newRow1);
            }

            _contentRepository.UpdateBenchmarkGroup(dataTable);
        }

        private void UpdateDatabaseClientCommodity(int clientId, TextFieldParser parser)
        {
            var dataTable = _contentRepository.GetDataTable(ContentDataTable.ClientCommodityTable);

            while (!parser.EndOfData)
            {
                var fields = parser.ReadFields();
                if (fields == null) continue;

                var key = GetKeyValue(fields[0]);
                var nameKey = GetStringValue(fields[1]);

                var newRow1 = dataTable.NewRow();
                newRow1[ContentConstants.StringClientId] = clientId;
                newRow1[ContentConstants.StringCommodityKey] = key;
                newRow1[ContentConstants.StringNameKey] = nameKey;
                dataTable.Rows.Add(newRow1);
            }

            _contentRepository.UpdateCommodity(dataTable);
        }

        private void UpdateDatabaseClientCondition(int clientId, TextFieldParser parser)
        {
            var dataTable = _contentRepository.GetDataTable(ContentDataTable.ClientConditionTable);

            while (!parser.EndOfData)
            {
                var fields = parser.ReadFields();
                if (fields == null) continue;

                var key = GetKeyValue(fields[0]);
                var category = GetStringValue(fields[1]);
                var description = GetStringValue(fields[2]);
                var profileAttribute = GetStringValue(fields[3]);
                var conditionOperator = GetStringValue(fields[4]);
                var value = GetStringValue(fields[5]);
                var profileOption = GetStringValue(fields[6]);

                var newRow1 = dataTable.NewRow();
                newRow1[ContentConstants.StringClientId] = clientId;
                newRow1[ContentConstants.StringConditionKey] = key;
                newRow1[ContentConstants.StringCategoryKey] = category;
                newRow1[ContentConstants.StringDescription] = description;
                newRow1[ContentConstants.StringProfileAttributeKey] = profileAttribute;
                newRow1[ContentConstants.StringOperator] = conditionOperator;
                newRow1[ContentConstants.StringValue] = value;
                newRow1[ContentConstants.StringProfileOptionKey] = profileOption;
                dataTable.Rows.Add(newRow1);
            }

            _contentRepository.UpdateCondition(dataTable);
        }

        private void UpdateDatabaseClientConfiguration(int clientId, TextFieldParser parser)
        {
            var dataTable = _contentRepository.GetDataTable(ContentDataTable.ClientConfigurationTable);

            while (!parser.EndOfData)
            {
                var fields = parser.ReadFields();
                if (fields == null) continue;

                var key = GetKeyValue(fields[0]);
                var category = GetStringValue(fields[1]);
                var description = GetStringValue(fields[2]);
                var value = GetStringValue(fields[3]);
                var environment = GetStringValue(fields[4]);

                var newRow1 = dataTable.NewRow();
                newRow1[ContentConstants.StringClientId] = clientId;
                newRow1[ContentConstants.StringConfigurationKey] = key;
                newRow1[ContentConstants.StringEnvironment] = environment;
                newRow1[ContentConstants.StringCategory] = category;
                newRow1[ContentConstants.StringDescription] = description;
                newRow1[ContentConstants.StringValue] = value;
                dataTable.Rows.Add(newRow1);
            }

            _contentRepository.UpdateConfiguration(dataTable);
        }

        /// <summary>
        /// insert/update client configuration bulk (configuration json format)
        /// TFS 598 Aug 2015
        /// </summary>
        /// <param name="clientId"></param>
        /// <param name="parser"></param>
        private void UpdateDatabaseClientConfigurationBulk(int clientId, TextFieldParser parser)
        {
            var dataTable = _contentRepository.GetDataTable(ContentDataTable.ClientConfigurationBulkTable);

            while (!parser.EndOfData)
            {
                var fields = parser.ReadFields();
                if (fields == null) continue;

                var key = GetKeyValue(fields[0]);
                var name = GetKeyValue(fields[1]);
                var category = GetStringValue(fields[2]);
                var description = GetStringValue(fields[3]);
                var value = GetStringValue(fields[4]);
                var environment = GetStringValue(fields[5]);

                var newRow1 = dataTable.NewRow();
                newRow1[ContentConstants.StringClientId] = clientId;
                newRow1[ContentConstants.StringClientConfigurationBulkKey] = key;
                newRow1[ContentConstants.StringName] = name;
                newRow1[ContentConstants.StringEnvironment] = environment;
                newRow1[ContentConstants.StringCategory] = category;
                newRow1[ContentConstants.StringDescription] = description;
                newRow1[ContentConstants.StringJsonConfiguration] = value;
                dataTable.Rows.Add(newRow1);
            }

            _contentRepository.UpdateConfigurationBulk(dataTable);
        }

        private void UpdateDatabaseClientCurrency(int clientId, TextFieldParser parser)
        {
            var dataTable = _contentRepository.GetDataTable(ContentDataTable.ClientCurrencyTable);

            while (!parser.EndOfData)
            {
                var fields = parser.ReadFields();
                if (fields == null) continue;

                var key = GetKeyValue(fields[0]);
                var nameKey = GetStringValue(fields[1]);
                var symbolKey = GetStringValue(fields[2]);

                var newRow1 = dataTable.NewRow();
                newRow1[ContentConstants.StringClientId] = clientId;
                newRow1[ContentConstants.StringCurrencyKey] = key;
                newRow1[ContentConstants.StringNameKey] = nameKey;
                newRow1[ContentConstants.StringSymbolKey] = symbolKey;
                dataTable.Rows.Add(newRow1);
            }

            _contentRepository.UpdateCurrency(dataTable);
        }

        private void UpdateDatabaseClientEnduse(int clientId, TextFieldParser parser)
        {
            var dataTable = _contentRepository.GetDataTable(ContentDataTable.ClientEndUseTable);

            while (!parser.EndOfData)
            {
                var fields = parser.ReadFields();
                if (fields == null) continue;

                var key = GetKeyValue(fields[0]);
                var nameKey = GetSysId(fields[1]);
                var endUseCategoryKey = GetStringValue(fields[2]);
                var disable = GetBooleanValue(fields[3]);
                var hide = GetBooleanValue(fields[4]);
                var appliances = GetStringValue(fields[5]);

                var newRow1 = dataTable.NewRow();
                newRow1[ContentConstants.StringClientId] = clientId;
                newRow1[ContentConstants.StringEndUseKey] = key;
                newRow1[ContentConstants.StringEndUseCategoryKey] = endUseCategoryKey;
                newRow1[ContentConstants.StringNameKey] = nameKey;
                newRow1[ContentConstants.StringDisable] = disable;
                newRow1[ContentConstants.StringHide] = hide;
                newRow1[ContentConstants.StringAppliances] = appliances;
                dataTable.Rows.Add(newRow1);
            }

            _contentRepository.UpdateEndUse(dataTable);
        }

        /// <summary>
        /// Updates Client Enumeration content in InsightsMetadata database with the content in CSV file
        /// </summary>

        private void UpdateDatabaseClientEnumeration(int clientId, TextFieldParser parser)
        {

            var dataTable = _contentRepository.GetDataTable(ContentDataTable.ClientEnumerationTable);

            while (!parser.EndOfData)
            {
                var fields = parser.ReadFields();
                if (fields == null) continue;

                var key = GetKeyValue(fields[0]);
                var category = GetStringValue(fields[1]);
                var enumerationItems = GetStringValue(fields[2]);
                var defaultEnumerationItem = GetStringValue(fields[3]);

                var newRow1 = dataTable.NewRow();
                newRow1[ContentConstants.StringClientId] = clientId;
                newRow1[ContentConstants.StringEnumerationKey] = key;
                newRow1[ContentConstants.StringCategory] = category;
                newRow1[ContentConstants.StringEnumerationItems] = enumerationItems;
                newRow1[ContentConstants.StringDefaultEnumerationItem] = defaultEnumerationItem;
                dataTable.Rows.Add(newRow1);
            }

            _contentRepository.UpdateEnumeration(dataTable);
        }

        /// <summary>
        /// Updates Client EnumerationItem content in InsightsMetadata database with the content in csv file
        /// </summary>

        private void UpdateDatabaseClientEnumerationItem(int clientId, TextFieldParser parser)
        {

            var dataTable = _contentRepository.GetDataTable(ContentDataTable.ClientEnumerationItemTable);

            while (!parser.EndOfData)
            {
                var fields = parser.ReadFields();
                if (fields == null) continue;

                var key = GetKeyValue(fields[0]);
                var nameKey = GetStringValue(fields[1]);

                var newRow1 = dataTable.NewRow();
                newRow1[ContentConstants.StringClientId] = clientId;
                newRow1[ContentConstants.StringEnumerationItemKey] = key;
                newRow1[ContentConstants.StringNameKey] = nameKey;
                dataTable.Rows.Add(newRow1);
            }

            _contentRepository.UpdateEnumerationItem(dataTable);
        }

        private void UpdateDatabaseClientExpression(int clientId, TextFieldParser parser)
        {
            var dataTable = _contentRepository.GetDataTable(ContentDataTable.ClientExpressionTable);

            while (!parser.EndOfData)
            {
                var fields = parser.ReadFields();
                if (fields == null) continue;

                var key = GetKeyValue(fields[0]);
                var name = GetStringValue(fields[1]);
                var description = GetStringValue(fields[2]);
                var formula = GetStringValue(fields[3]);
                var comments = GetStringValue(fields[4]);

                var newRow1 = dataTable.NewRow();
                newRow1[ContentConstants.StringClientId] = clientId;
                newRow1[ContentConstants.StringExpressionKey] = key;
                newRow1[ContentConstants.StringName] = name;
                newRow1[ContentConstants.StringDescription] = description;
                newRow1[ContentConstants.StringFormula] = formula;
                newRow1[ContentConstants.StringComments] = comments;
                dataTable.Rows.Add(newRow1);
            }

            _contentRepository.UpdateExpression(dataTable);
        }

        private void UpdateDatabaseClientFileContent(int clientId, TextFieldParser parser)
        {
            var dataTable = _contentRepository.GetDataTable(ContentDataTable.ClientFileContentTable);

            while (!parser.EndOfData)
            {
                var fields = parser.ReadFields();
                if (fields == null) continue;

                var key = GetKeyValue(fields[0]);
                //                var type = GetStringValue(fields[1]);
                var category = GetStringValue(fields[2]);
                var smallFile = GetStringValue(fields[3]);
                var mediumFile = GetStringValue(fields[4]);
                var largeFile = GetStringValue(fields[5]);

                var newRow1 = dataTable.NewRow();
                newRow1[ContentConstants.StringClientId] = clientId;
                newRow1[ContentConstants.StringFileContentKey] = key;
                //              newRow1[ContentConstants.StringType] = type;
                newRow1[ContentConstants.StringCategoryKey] = category;
                newRow1[ContentConstants.StringSmallFile] = smallFile;
                newRow1[ContentConstants.StringMediumFile] = mediumFile;
                newRow1[ContentConstants.StringLargeFile] = largeFile;
                dataTable.Rows.Add(newRow1);
            }

            _contentRepository.UpdateFileContent(dataTable);
        }

        private void UpdateDatabaseClientLayout(int clientId, TextFieldParser parser)
        {
            var dataTable = _contentRepository.GetDataTable(ContentDataTable.ClientLayoutTable);

            while (!parser.EndOfData)
            {
                var fields = parser.ReadFields();
                if (fields == null) continue;

                var key = GetKeyValue(fields[0]);
                var description = GetStringValue(fields[1]);
                var layoutJson = GetStringValue(fields[2]);

                var newRow1 = dataTable.NewRow();
                newRow1[ContentConstants.StringClientId] = clientId;
                newRow1[ContentConstants.StringLayoutKey] = key;
                newRow1[ContentConstants.StringDescription] = description;
                newRow1[ContentConstants.StringLayoutJson] = layoutJson;
                dataTable.Rows.Add(newRow1);
            }

            _contentRepository.UpdateLayout(dataTable);
        }

        private void UpdateDatabaseClientMeasurement(int clientId, TextFieldParser parser)
        {
            var dataTable = _contentRepository.GetDataTable(ContentDataTable.ClientMeasurementTable);

            while (!parser.EndOfData)
            {
                var fields = parser.ReadFields();
                if (fields == null) continue;

                var key = GetKeyValue(fields[0]);
                var nameKey = GetStringValue(fields[1]);
                var calculation = GetStringValue(fields[2]);
                var comments = GetStringValue(fields[3]);

                var newRow1 = dataTable.NewRow();
                newRow1[ContentConstants.StringClientId] = clientId;
                newRow1[ContentConstants.StringMeasurementKey] = key;
                newRow1[ContentConstants.StringNameKey] = nameKey;
                newRow1[ContentConstants.StringCalculation] = calculation;
                newRow1[ContentConstants.StringComments] = comments;

                dataTable.Rows.Add(newRow1);
            }

            _contentRepository.UpdateMeasurement(dataTable);
        }

        /// <summary>
        /// Updates ProfileAttribute content in InsightsMetadata database with the content defined in Contentful
        /// </summary>

        private void UpdateDatabaseClientProfileAttribute(int clientId, TextFieldParser parser)
        {
            var dataTable = _contentRepository.GetDataTable(ContentDataTable.ClientProfileAttributeTable);

            while (!parser.EndOfData)
            {
                var fields = parser.ReadFields();
                if (fields == null) continue;

                var key = GetKeyValue(fields[0]);
                var description = GetStringValue(fields[1]);
                var profileAttributeTypeKey = GetStringValue(fields[2]);
                var profileOptions = GetStringValue(fields[3]);
                var disableConditions = GetStringValue(fields[4], string.Empty);
                var entityLevelKey = GetStringValue(fields[5]);
                var questionTextKey = GetStringValue(fields[6]);
                var minValue = GetOptionalIntValue(GetStringValue(fields[7]));
                var maxValue = GetOptionalIntValue(GetStringValue(fields[8]));
                bool doNotDefault = GetBooleanValue(GetStringValue(fields[9]));
                bool disable = GetBooleanValue(GetStringValue(fields[10]));

                var newRow1 = dataTable.NewRow();
                newRow1[ContentConstants.StringClientId] = clientId;
                newRow1[ContentConstants.StringProfileAttributeKey] = key;
                newRow1[ContentConstants.StringDescription] = description;
                newRow1[ContentConstants.StringProfileAttributeTypeKey] = profileAttributeTypeKey;
                newRow1[ContentConstants.StringProfileOptions] = profileOptions;
                newRow1[ContentConstants.StringDisableConditions] = disableConditions;
                newRow1[ContentConstants.StringEntityLevelKey] = entityLevelKey;
                newRow1[ContentConstants.StringQuestionTextKey] = questionTextKey;
                if (minValue != null)
                    newRow1[ContentConstants.StringMinValue] = minValue;
                if (maxValue != null)
                    newRow1[ContentConstants.StringMaxValue] = maxValue;
                newRow1[ContentConstants.StringDoNotDefault] = doNotDefault;
                newRow1[ContentConstants.StringDisable] = disable;
                dataTable.Rows.Add(newRow1);
            }

            _contentRepository.UpdateProfileAttribute(dataTable);

        }

        /// <summary>
        /// Updates ProfileDefault content in InsightsMetadata database with client content in CSV file
        /// </summary>
        /// 
        private void UpdateDatabaseClientProfileDefault(int clientId, TextFieldParser parser)
        {
            var dataTable = _contentRepository.GetDataTable(ContentDataTable.ClientProfileDefaultTable);

            while (!parser.EndOfData)
            {
                var fields = parser.ReadFields();
                if (fields == null) continue;

                var key = GetKeyValue(fields[0]);
                var profileDefaultCollectionKey = GetStringValue(fields[1]);
                var profileAttributeKey = GetStringValue(fields[2]);
                var defaultKey = GetStringValue(fields[3]);
                var defaultValue = GetStringValue(fields[4]);

                var newRow1 = dataTable.NewRow();
                newRow1[ContentConstants.StringClientId] = clientId;
                newRow1[ContentConstants.StringProfileDefaultKey] = key;
                newRow1[ContentConstants.StringProfileDefaultCollectionKey] = profileDefaultCollectionKey;
                newRow1[ContentConstants.StringProfileAttributeKey] = profileAttributeKey;
                newRow1[ContentConstants.StringDefaultKey] = defaultKey;
                newRow1[ContentConstants.StringDefaultValue] = defaultValue;
                dataTable.Rows.Add(newRow1);
            }

            _contentRepository.UpdateProfileDefault(dataTable);
        }


        /// <summary>
        /// Updates ProfileDefaultCollection content in InsightsMetadata database with client content in CSV file
        /// </summary>

        private void UpdateDatabaseClientProfileDefaultCollection(int clientId, TextFieldParser parser)
        {

            var dataTable = _contentRepository.GetDataTable(ContentDataTable.ClientProfileDefaultCollectionTable);

            while (!parser.EndOfData)
            {
                var fields = parser.ReadFields();
                if (fields == null) continue;

                var key = GetKeyValue(fields[0]);
                var nameKey = GetStringValue(fields[1]);
                var description = GetStringValue(fields[2]);

                var newRow1 = dataTable.NewRow();
                newRow1[ContentConstants.StringClientId] = clientId;
                newRow1[ContentConstants.StringProfileDefaultCollectionKey] = key;
                newRow1[ContentConstants.StringNameKey] = nameKey;
                newRow1[ContentConstants.StringDescription] = description;
                dataTable.Rows.Add(newRow1);
            }

            _contentRepository.UpdateProfileDefaultCollection(dataTable);

        }

        /// <summary>
        /// Updates Client ProfileOption content in InsightsMetadata database with the content in CSV file
        /// </summary>

        private void UpdateDatabaseClientProfileOption(int clientId, TextFieldParser parser)
        {
            var dataTable = _contentRepository.GetDataTable(ContentDataTable.ClientProfileOptionTable);


            while (!parser.EndOfData)
            {
                var fields = parser.ReadFields();
                if (fields == null) continue;

                var key = GetKeyValue(fields[0]);
                var nameKey = GetStringValue(fields[1]);
                var description = GetStringValue(fields[2]);
                var profileOptionValue = GetStringValue(fields[3]);

                var newRow1 = dataTable.NewRow();
                newRow1[ContentConstants.StringClientId] = clientId;
                newRow1[ContentConstants.StringProfileOptionKey] = key;
                newRow1[ContentConstants.StringProfileOptionValue] = profileOptionValue;
                newRow1[ContentConstants.StringNameKey] = nameKey;
                newRow1[ContentConstants.StringDescription] = description;
                dataTable.Rows.Add(newRow1);
            }

            _contentRepository.UpdateProfileOption(dataTable);

        }

        /// <summary>
        /// Updates Client ProfileSection content in InsightsMetadata database with the content in CSV file
        /// </summary>

        private void UpdateDatabaseClientProfileSection(int clientId, TextFieldParser parser)
        {
            var dataTable = _contentRepository.GetDataTable(ContentDataTable.ClientProfileSectionTable);

            while (!parser.EndOfData)
            {
                var fields = parser.ReadFields();
                if (fields == null) continue;

                var key = GetKeyValue(fields[0]);
                var name = GetStringValue(fields[1]);
                var descriptionKey = GetStringValue(fields[2]);
                var titleKey = GetStringValue(fields[3]);
                var subTitleKey = GetStringValue(fields[4]);
                var rank = GetIntValue(fields[5]);
                var profileAttributes = GetStringValue(fields[6], string.Empty);
                var conditions = GetStringValue(fields[7], string.Empty);
                var images = GetStringValue(fields[8], string.Empty);
                var introTextKey = GetStringValue(fields[9]);

                var newRow1 = dataTable.NewRow();
                newRow1[ContentConstants.StringClientId] = clientId;
                newRow1[ContentConstants.StringProfileSectionKey] = key;
                newRow1[ContentConstants.StringName] = name;
                newRow1[ContentConstants.StringDescriptionKey] = descriptionKey;
                newRow1[ContentConstants.StringRank] = rank;
                newRow1[ContentConstants.StringTitleKey] = titleKey;
                newRow1[ContentConstants.StringSubTitleKey] = subTitleKey;
                newRow1[ContentConstants.StringProfileAttributes] = profileAttributes;
                newRow1[ContentConstants.StringConditions] = conditions;
                newRow1[ContentConstants.StringImages] = images;
                newRow1[ContentConstants.StringIntroText] = introTextKey;
                dataTable.Rows.Add(newRow1);
            }

            _contentRepository.UpdateProfileSection(dataTable);
        }

        private void UpdateDatabaseClientSeason(int clientId, TextFieldParser parser)
        {
            var dataTable = _contentRepository.GetDataTable(ContentDataTable.ClientSeasonTable);

            while (!parser.EndOfData)
            {
                var fields = parser.ReadFields();
                if (fields == null) continue;

                var key = GetKeyValue(fields[0]);
                var nameKey = GetStringValue(fields[1]);
                var startMonth = GetIntValue(fields[2]);
                var startDay = GetIntValue(fields[3]);
                var endMonth = GetIntValue(fields[4]);
                var endDay = GetIntValue(fields[5]);

                var newRow1 = dataTable.NewRow();
                newRow1[ContentConstants.StringClientId] = clientId;
                newRow1[ContentConstants.StringSeasonKey] = key;
                newRow1[ContentConstants.StringNameKey] = nameKey;
                newRow1[ContentConstants.StringStartMonth] = startMonth;
                newRow1[ContentConstants.StringStartDay] = startDay;
                newRow1[ContentConstants.StringEndMonth] = endMonth;
                newRow1[ContentConstants.StringEndDay] = endDay;

                dataTable.Rows.Add(newRow1);
            }

            _contentRepository.UpdateSeason(dataTable);
        }

        private void UpdateDatabaseClientTab(int clientId, TextFieldParser parser)
        {
            var dataTable = _contentRepository.GetDataTable(ContentDataTable.ClientTabTable);

            while (!parser.EndOfData)
            {
                var fields = parser.ReadFields();
                if (fields == null) continue;

                var key = GetKeyValue(fields[0]);
                var name = GetStringValue(fields[1]);
                var menuOrder = GetIntValue(fields[2]);
                var layoutKey = GetStringValue(fields[3]);
                var tabTypeKey = GetStringValue(fields[4]);
                var nameKey = GetStringValue(fields[5]);
                var url = GetStringValue(fields[6]);
                var conditionKeys = GetStringValue(fields[7], string.Empty);
                var configurationKeys = GetStringValue(fields[8], string.Empty);
                var textContentKeys = GetStringValue(fields[9], string.Empty);
                var childTabKeys = GetStringValue(fields[10], string.Empty);
                var profileSectionKeys = GetStringValue(fields[11], string.Empty);
                var widgetKey = GetStringValue(fields[12]);
                var tabIconType = GetStringValue(fields[13]);
                var tabIconClass = GetStringValue(fields[14]);
                var tabImages = GetStringValue(fields[15]);
                var actions = GetStringValue(fields[16], string.Empty);
                var disable = GetBooleanValue(fields[17]);

                var newRow1 = dataTable.NewRow();
                newRow1[ContentConstants.StringClientId] = clientId;
                newRow1[ContentConstants.StringTabKey] = key;
                newRow1[ContentConstants.StringName] = name;
                newRow1[ContentConstants.StringMenuOrder] = menuOrder;
                newRow1[ContentConstants.StringLayoutKey] = layoutKey;
                newRow1[ContentConstants.StringTabTypeKey] = tabTypeKey;
                newRow1[ContentConstants.StringNameKey] = nameKey;
                newRow1[ContentConstants.StringWidgetKey] = widgetKey;
                newRow1[ContentConstants.StringUrl] = url;
                newRow1[ContentConstants.StringConditionKeys] = conditionKeys;
                newRow1[ContentConstants.StringConfigurationKeys] = configurationKeys;
                newRow1[ContentConstants.StringTextContentKeys] = textContentKeys;
                newRow1[ContentConstants.StringProfileSectionKeys] = profileSectionKeys;
                newRow1[ContentConstants.StringChildTabKeys] = childTabKeys;
                newRow1[ContentConstants.StringTabIconType] = tabIconType;
                newRow1[ContentConstants.StringTabIconClass] = tabIconClass;
                newRow1[ContentConstants.StringImages] = tabImages;
                newRow1[ContentConstants.StringActions] = actions;
                newRow1[ContentConstants.StringDisable] = disable;
                dataTable.Rows.Add(newRow1);
            }

            _contentRepository.UpdateTab(dataTable);
        }

        private void UpdateDatabaseClientTextContent(int clientId, TextFieldParser parser)
        {
            var dataTable = _contentRepository.GetDataTable(ContentDataTable.ClientTextContentTable);

            while (!parser.EndOfData)
            {
                var fields = parser.ReadFields();
                if (fields == null) continue;

                var key = GetKeyValue(fields[0]);
                var categoryKey = GetStringValue(fields[1]);
                var shortTextEnglish = GetStringValue(fields[2]);
                var mediumTextEnglish = GetStringValue(fields[3]);
                var longTextEnglish = GetStringValue(fields[4]);

                var rvsShortText = GetRequireVariableSubstitutionValue(shortTextEnglish);
                var rvsMediumText = GetRequireVariableSubstitutionValue(mediumTextEnglish);
                var rvsLongText = GetRequireVariableSubstitutionValue(longTextEnglish);
                var requireVariableSubstitution = (rvsShortText) || (rvsMediumText) || (rvsLongText);

                var shortTextSpanish = TranslateText(shortTextEnglish, ContentConstants.StringLocaleEnglishToSpanish);
                var shortTextRussian = TranslateText(shortTextEnglish, ContentConstants.StringLocaleEnglishToRussian);
                var mediumTextSpanish = shortTextEnglish != mediumTextEnglish ? TranslateText(mediumTextEnglish, ContentConstants.StringLocaleEnglishToSpanish) : shortTextSpanish;
                var mediumTextRussian = shortTextEnglish != mediumTextEnglish ? TranslateText(mediumTextEnglish, ContentConstants.StringLocaleEnglishToRussian) : shortTextRussian;
                var longTextSpanish = shortTextEnglish != longTextEnglish ? TranslateText(longTextEnglish, ContentConstants.StringLocaleEnglishToSpanish) : shortTextSpanish;
                var longTextRussian = shortTextEnglish != longTextEnglish ? TranslateText(longTextEnglish, ContentConstants.StringLocaleEnglishToRussian) : shortTextRussian;

                var newRow1 = dataTable.NewRow();
                newRow1[ContentConstants.StringClientId] = clientId;
                newRow1[ContentConstants.StringTextContentKey] = key;
                newRow1[ContentConstants.StringLocaleKey] = ContentConstants.StringLocaleEnglish;
                newRow1[ContentConstants.StringCategoryKey] = categoryKey;
                newRow1[ContentConstants.StringShortText] = shortTextEnglish;
                newRow1[ContentConstants.StringMediumText] = mediumTextEnglish;
                newRow1[ContentConstants.StringLongText] = longTextEnglish;
                newRow1[ContentConstants.StringRequireVariableSubstitution] = requireVariableSubstitution;
                dataTable.Rows.Add(newRow1);

                var newRow2 = dataTable.NewRow();
                newRow2[ContentConstants.StringClientId] = clientId;
                newRow2[ContentConstants.StringTextContentKey] = key;
                newRow2[ContentConstants.StringLocaleKey] = ContentConstants.StringLocaleSpanish;
                newRow2[ContentConstants.StringCategoryKey] = categoryKey;
                newRow2[ContentConstants.StringShortText] = shortTextSpanish;
                newRow2[ContentConstants.StringMediumText] = mediumTextSpanish;
                newRow2[ContentConstants.StringLongText] = longTextSpanish;
                newRow2[ContentConstants.StringRequireVariableSubstitution] = rvsMediumText;
                dataTable.Rows.Add(newRow2);

                var newRow3 = dataTable.NewRow();
                newRow3[ContentConstants.StringClientId] = clientId;
                newRow3[ContentConstants.StringTextContentKey] = key;
                newRow3[ContentConstants.StringLocaleKey] = ContentConstants.StringLocaleRussian;
                newRow3[ContentConstants.StringCategoryKey] = categoryKey;
                newRow3[ContentConstants.StringShortText] = shortTextRussian;
                newRow3[ContentConstants.StringMediumText] = mediumTextRussian;
                newRow3[ContentConstants.StringLongText] = longTextRussian;
                newRow3[ContentConstants.StringRequireVariableSubstitution] = rvsLongText;
                dataTable.Rows.Add(newRow3);

            }

            _contentRepository.UpdateTextContent(dataTable);
        }

        private void UpdateDatabaseClientUom(int clientId, TextFieldParser parser)
        {
            var dataTable = _contentRepository.GetDataTable(ContentDataTable.ClientUomTable);

            while (!parser.EndOfData)
            {
                var fields = parser.ReadFields();
                if (fields == null) continue;

                var key = GetKeyValue(fields[0]);
                var nameKey = GetStringValue(fields[1]);

                var newRow1 = dataTable.NewRow();
                newRow1[ContentConstants.StringClientId] = clientId;
                newRow1[ContentConstants.StringUomKey] = key;
                newRow1[ContentConstants.StringNameKey] = nameKey;

                dataTable.Rows.Add(newRow1);
            }

            _contentRepository.UpdateUom(dataTable);
        }

        /// <summary>
        /// Updates Client ProfileOption content in InsightsMetadata database with the content in CSV file
        /// </summary>

        private void UpdateDatabaseClientWhatIfData(int clientId, TextFieldParser parser)
        {
            var dataTable = _contentRepository.GetDataTable(ContentDataTable.ClientWhatIfDataTable);


            while (!parser.EndOfData)
            {
                var fields = parser.ReadFields();
                if (fields == null) continue;

                var key = GetKeyValue(fields[0]);
                var profileAttributeKey = GetStringValue(fields[1]);
                var valueExpressionKey = GetStringValue(fields[2]);

                var newRow1 = dataTable.NewRow();
                newRow1[ContentConstants.StringClientId] = clientId;
                newRow1[ContentConstants.StringWhatIfDataKey] = key;
                newRow1[ContentConstants.StringProfileAttributeKey] = profileAttributeKey;
                newRow1[ContentConstants.StringValueExpressionKey] = valueExpressionKey;

                dataTable.Rows.Add(newRow1);
            }

            _contentRepository.UpdateWhatIfData(dataTable);

        }

        private void UpdateDatabaseClientWidget(int clientId, TextFieldParser parser)
        {
            var dataTable = _contentRepository.GetDataTable(ContentDataTable.ClientWidgetTable);

            while (!parser.EndOfData)
            {
                var fields = parser.ReadFields();
                if (fields == null) continue;

                var key = GetKeyValue(fields[0]);
                var name = GetStringValue(fields[1]);
                var tabKey = GetStringValue(fields[2]);
                var widgetTypeKey = GetStringValue(fields[3]);
                var widgetColumn = GetIntValue(fields[4]);
                var widgetRow = GetIntValue(fields[5]);
                var widgetOrder = GetIntValue(fields[6]);
                var conditionKeys = GetStringValue(fields[7], string.Empty);
                var configurationKeys = GetStringValue(fields[8], string.Empty);
                var textContentKeys = GetStringValue(fields[9], string.Empty);
                var disable = GetBooleanValue(fields[10]);
                var introTextKey = GetStringValue(fields[11], string.Empty);

                var newRow1 = dataTable.NewRow();
                newRow1[ContentConstants.StringClientId] = clientId;
                newRow1[ContentConstants.StringWidgetKey] = key;
                newRow1[ContentConstants.StringName] = name;
                newRow1[ContentConstants.StringTabKey] = tabKey;
                newRow1[ContentConstants.StringWidgetTypeKey] = widgetTypeKey;
                newRow1[ContentConstants.StringWidgetColumn] = widgetColumn;
                newRow1[ContentConstants.StringWidgetRow] = widgetRow;
                newRow1[ContentConstants.StringWidgetOrder] = widgetOrder;
                newRow1[ContentConstants.StringConditionKeys] = conditionKeys;
                newRow1[ContentConstants.StringConfigurationKeys] = configurationKeys;
                newRow1[ContentConstants.StringTextContentKeys] = textContentKeys;
                newRow1[ContentConstants.StringDisable] = disable;
                newRow1[ContentConstants.StringIntroText] = introTextKey;
                dataTable.Rows.Add(newRow1);
            }

            _contentRepository.UpdateWidget(dataTable);
        }

        #endregion

        #region Database Client Content Orphan Methods

        /// <summary>
        /// 
        /// </summary>
        /// <param name="outputCsvFilePath"></param>
        public void GetContentOrphan(string outputCsvFilePath)
        {
            _contentRepository.GetContentOrphan(outputCsvFilePath);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="csvFilePath"></param>
        public void DeleteContentOrphan(string csvFilePath)
        {
            var parser = new TextFieldParser(csvFilePath) { HasFieldsEnclosedInQuotes = true };
            parser.SetDelimiters(",");
            parser.ReadLine(); //ignore 1st row

            while (!parser.EndOfData)
            {
                var fields = parser.ReadFields();
                if (fields == null) continue;

                var clientId = GetIntValue(fields[0]);
                var contentType = GetStringValue(fields[1], string.Empty);
                var key = GetStringValue(fields[3], string.Empty);
                var approved = GetStringValue(fields[5], string.Empty);

                if (approved.Substring(0, 1).ToLower() == "y")
                    _contentRepository.DeleteContentOrphan(contentType, clientId, key);

                System.Threading.Thread.Sleep(10);
            }
            parser.Close();

        }

        #endregion

        #region String Convertion Methods

        private static bool GetBooleanValue(string value)
        {
            return (value == "1");
        }

        private static decimal GetDecimalValue(string value)
        {
            decimal result;
            Decimal.TryParse(value, out result);
            return result;
        }

        private static string GetStringValue(string value, string defaultValue = null)
        {
            var result = defaultValue;
            if (!string.IsNullOrEmpty(value))
            {
                result = value;
            }
            return result;
        }

        private static int GetIntValue(string value)
        {
            int result;
            int.TryParse(value, out result);
            return result;
        }

        private static int? GetOptionalIntValue(string value)
        {
            int outValue;
            return int.TryParse(value, out outValue) ? (int?)outValue : null;
        }

        private static decimal? GetOptionalDecimalValue(string value)
        {
            decimal outValue;
            return decimal.TryParse(value, out outValue) ? (decimal?)outValue : null;
        }

        private static string GetKeyValue(string value)
        {
            return value.ToLower().Replace(" ", "");
        }

        #endregion

        #region Helper Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="contentRepository"></param>
        /// <param name="defaultSpace"></param>
        /// <returns></returns>
        private static Dictionary<string, string> GetContentfulIdAndKeyDictionary(IContentRepository contentRepository, string defaultSpace)
        {
            var contentCache = contentRepository.GetRawContent(defaultSpace);
            var defaultContentEntries = (JArray)JsonConvert.DeserializeObject(contentCache);

            var keyPath = GetTokenPath(ContentConstants.StringKey);
            return defaultContentEntries.ToDictionary(content => content.SelectToken(TokenContentId).ToString(), entry => entry.SelectToken(keyPath).ToString());
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="keyString"></param>
        /// <returns></returns>
        private string GetSysId(string keyString)
        {
            var keys = SysIdAndKeyDictionary;

            var result = keys.FirstOrDefault(x => x.Value == GetKeyValue(keyString)).Key;

            return result;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="keysString"></param>
        /// <returns></returns>
        private string GetSysIds(string keysString)
        {
            var result = string.Empty;

            var keys = SysIdAndKeyDictionary;

            if (keysString == null) return result;
            foreach (var key in keysString.Split(';'))
            {
                if (result != string.Empty)
                {
                    result += ";";
                }
                result += keys.FirstOrDefault(x => x.Value == GetKeyValue(key)).Key;
            }

            return result;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="fieldName"></param>
        /// <returns></returns>
        private static string GetTokenPath(string fieldName)
        {
            return string.Format(TemplateTokenField, fieldName.ToLower(), ContentConstants.StringLocaleEnglish);
        }

        /// <summary>
        /// Translate Text using Translate API's
        /// </summary>
        /// <param name="input">Input string</param>
        /// <param name="languagePair">2 letter Language Pair, delimited by "|".
        /// E.g. "ar|en" language pair means to translate from Arabic to English</param>
        /// <returns>Translated to String</returns>
        private static string TranslateText(string input, string languagePair)
        {
            const string tokenTranslatedText = "responseData.translatedText";
            const string tranlsationUrlTemplate = "http://mymemory.translated.net/api/get?q={0}&langpair={1}&de=sambati@aclara.com";

            var processedInput = input.Substring(0, Math.Min(input.Length, 500)).Replace("###", "^^^");

            var url = string.Format(tranlsationUrlTemplate, Uri.EscapeUriString(processedInput), languagePair);
            var webClient = new WebClient { Encoding = Encoding.UTF8 };
            var responseString = webClient.DownloadString(url);
            var responseJson = JObject.Parse(responseString);
            var output = responseJson.SelectToken(tokenTranslatedText).ToString();
            var result = output.Replace("^ ^ ^ ", "###");

            return result;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="textContent"></param>
        /// <returns></returns>
        private static bool GetRequireVariableSubstitutionValue(string textContent)
        {
            var result = false;

            const string pattern = @"\[\%(.*?)\%\]"; //@"([%).+?(%])";
            var myRegex = new Regex(pattern, RegexOptions.IgnoreCase);

            var m = myRegex.Match(textContent);   // m is the first match
            if (m.Success)
            {
                result = true;
            }

            return result;
        }

        /// <summary>
        /// get version from contentful for the selected key 
        /// July 2015 TFS 287
        /// </summary>
        /// <param name="key"></param>
        /// <param name="contentType"></param>
        /// <param name="contentCache"></param>
        /// <returns></returns>
        private int GetVersionContentEntry(string key, ContentType contentType, string contentCache)
        {
            var version = 0;
            try
            {
                const string tokenContentType = "sys.contentType.sys.id";

                var defaultContentEntries = (JArray)JsonConvert.DeserializeObject(contentCache);

                var entry = (from content in defaultContentEntries.Children<JObject>()
                              let contentKey = (string)content.SelectToken(GetTokenPath(ContentConstants.StringKey))
                             let contentTypeName = (string)content.SelectToken(tokenContentType)
                              where (contentTypeName.ToLower() == contentType.ToString().ToLower()) && (contentKey == key)
                              select content).FirstOrDefault();


                // if entry exist in entry cache, then make contentful management call to retrieve version number; otherwise, version number remains 0
                if (entry != null)
                {
                    var content = _contentProvider.GetSingleContentFromContenful(key);
                    if (content != null)
                    {
                        var versionToken =
                            content.Children<JObject>().Select(t => t.SelectToken("sys.version")).FirstOrDefault();
                        if (versionToken != null)
                            version = Convert.ToInt32(versionToken);


                    }
                }
            }
            catch (Exception)
            {
                version = 0;
            }

            return version;
        }

        /// <summary>
        /// get version from contentful for the selected key 
        /// </summary>
        /// <param name="key"></param>
        /// <param name="assetCache"></param>
        /// <returns></returns>
        private int GetVersionAsset(string key,  string assetCache)
        {
            var version = 0;
            try
            {

                var defaultContentEntries = (JArray)JsonConvert.DeserializeObject(assetCache);

                var contentAsset = defaultContentEntries.Children<JObject>().Select(t => t.SelectToken("sys.id")).ToList().Find(a => (string)a == key);


                if (contentAsset != null)
                {
                    var asset = _contentProvider.GetSingleAssetFromContenful(key);
                    if (asset != null)
                    {
                        var versionToken =
                            asset.Children<JObject>().Select(t => t.SelectToken("sys.version")).FirstOrDefault();
                        if (versionToken != null)
                            version = Convert.ToInt32(versionToken);


                    }
                }
            }
            catch (Exception)
            {
                version = 0;
            }

            return version;
        }

        #endregion

        #endregion

    }
}
