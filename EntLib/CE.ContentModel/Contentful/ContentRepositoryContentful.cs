﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using AutoMapper;
using CE.ContentModel.Entities;

// ReSharper disable once CheckNamespace
namespace CE.ContentModel
{

    /// <summary>
    /// Data Reader Extension classes
    /// </summary>
    public static class DataReaderExtensions
    {
        public static string GetStringOrNull(this IDataReader reader, int ordinal)
        {
            return reader.IsDBNull(ordinal) ? null : reader.GetString(ordinal);
        }

        public static string GetStringOrNull(this IDataReader reader, string columnName)
        {
            return reader.GetStringOrNull(reader.GetOrdinal(columnName));
        }

        public static decimal? GetDecimalOrNull(this IDataReader reader, int ordinal)
        {
            return reader.IsDBNull(ordinal) ? (decimal?)null : reader.GetDecimal(ordinal);
        }

        public static int? GetInt32OrNull(this IDataReader reader, int ordinal)
        {
            return reader.IsDBNull(ordinal) ? (int?)null : reader.GetInt32(ordinal);
        }
    }

    /// <summary>
    /// Data Access Layer for Content Model
    /// </summary>
    class ContentRepositoryContentful : IContentRepository
    {

        #region Private Constants

        private const string ParamClientSpace = "@ClientSpace";
        private const string ParamClientContent = "@ClientContent";

        private const string ParamClientId = "@clientID";
        private const string ParamProfileDefaultCollectionName = "@profileDefaultCollectionKey";
        private const string ParamLocale = "@locale";

        private const string ParamAttributeKeysFilterTable = "@attributeKeysFilterTable";
        private const string ParamEnduseKeysFilterTable = "@enduseKeysFilterTable";
        private const string ParamApplianceKeysFilterTable = "@applianceKeysFilterTable";
        private const string ParamEntitylevelFilter = "@entitylevelFilter";

        //private const string ProfileDefaultCollectionName = "aclara";

        private const string ProcSelectContentClientProfileDefaults = "[cm].[uspSelectContentClientProfileDefaults]";
        private const string ProcSelectContentClientTextContent = "[cm].[uspSelectContentClientTextContent]";

        #endregion

        #region Private Enums

        private enum ActionContentFields
        {
            Key
        }

        #endregion

        #region Private Class Variables

        private readonly string _insightsMetaDataConnectionString = string.Empty;
        private readonly int _clientId;

        #endregion

        #region Constructors

        /// <summary>
        /// Default constructor 
        /// </summary>
        // ReSharper disable once UnusedMember.Local
        private ContentRepositoryContentful()
        {
            //Default constructor not allowed.
        }

        /// <summary>
        /// Public constructor
        /// </summary>
        /// <param name="insightsMetaDataConnectionString"></param>
        /// <param name="clientId"></param>
        public ContentRepositoryContentful(string insightsMetaDataConnectionString, int clientId)
        {
            _insightsMetaDataConnectionString = insightsMetaDataConnectionString;
            _clientId = clientId;
        }

        #endregion

        #region Public Methods

        #region Get Content Methods

        /// <summary>
        /// Gets Action Content in Insights Metadata database
        /// </summary>
        /// <returns></returns>
        public List<ClientAction> GetContentClientActions()
        {
            const string storedProcedureName = "[cm].[uspSelectContentClientAction]";
            return GetContentFromDatabase<ClientAction>(storedProcedureName);
        }

        /// <summary>
        /// Gets ActionSavings Content in Insights Metadata database
        /// </summary>
        /// <returns></returns>
        public List<ClientActionSaving> GetContentClientActionSavings()
        {
            const string storedProcedureName = "[cm].[uspSelectContentClientActionSavings]";
            return GetContentFromDatabase<ClientActionSaving>(storedProcedureName);
        }

        /// <summary>
        /// Gets Appliance Content in Insights Metadata database
        /// </summary>
        /// <returns></returns>
        public List<ClientAppliance> GetContentClientAppliances()
        {
            const string storedProcedureName = "[cm].[uspSelectContentClientAppliances]";
            return GetContentFromDatabase<ClientAppliance>(storedProcedureName);
        }

        /// <summary>
        /// Gets BenchmarkGroup Content in Insights Metadata database
        /// </summary>
        /// <returns></returns>
        public List<ClientBenchmarkGroup> GetContentClientBenchMarkGroups()
        {
            const string storedProcedureName = "[cm].[uspSelectContentClientBenchMarkGroups]";
            return GetContentFromDatabase<ClientBenchmarkGroup>(storedProcedureName);
        }

        /// <summary>
        /// Gets Commodity Content in Insights Metadata database
        /// </summary>
        /// <returns></returns>
        public List<ClientCommodity> GetContentClientCommodities()
        {
            const string storedProcedureName = "[cm].[uspSelectContentClientCommodities]";
            return GetContentFromDatabase<ClientCommodity>(storedProcedureName);
        }

        /// <summary>
        /// Gets Condition Content in Insights Metadata database
        /// </summary>
        /// <returns></returns>
        public List<ClientCondition> GetContentClientConditions()
        {
            const string storedProcedureName = "[cm].[uspSelectContentClientCondition]";
            return GetContentFromDatabase<ClientCondition>(storedProcedureName);
        }

        /// <summary>
        /// Gets Configuration Content in Insights Metadata database
        /// </summary>
        /// <returns></returns>
        public List<ClientConfiguration> GetContentClientConfigurations()
        {
            const string storedProcedureName = "[cm].[uspSelectContentClientConfigurations]";
            return GetContentFromDatabase<ClientConfiguration>(storedProcedureName);
        }

        /// <summary>
        /// Gets Configuration Bulk Content in Insights Metadata database
        /// </summary>
        /// <returns></returns>
        public List<ClientConfigurationBulk> GetContentClientConfigurationBulks()
        {
            const string storedProcedureName = "[cm].[uspSelectContentClientConfigurationBulks]";
            return GetContentFromDatabase<ClientConfigurationBulk>(storedProcedureName);
        }

        /// <summary>
        /// Gets Currency Content in Insights Metadata database
        /// </summary>
        /// <returns></returns>
        public List<ClientCurrency> GetContentClientCurrencies()
        {
            const string storedProcedureName = "[cm].[uspSelectContentClientCurrencies]";
            return GetContentFromDatabase<ClientCurrency>(storedProcedureName);
        }

        /// <summary>
        /// Gets EndUse Content in Insights Metadata database
        /// </summary>
        /// <returns></returns>
        public List<ClientEndUse> GetContentClientEnduses()
        {
            const string storedProcedureName = "[cm].[uspSelectContentClientEnduses]";
            return GetContentFromDatabase<ClientEndUse>(storedProcedureName);
        }

        /// <summary>
        /// Gets Enumeration Content in Insights Metadata database
        /// </summary>
        /// <returns></returns>
        public List<ClientEnumeration> GetContentClientEnumerations()
        {
            const string storedProcedureName = "[cm].[uspSelectContentClientEnumeration]";
            return GetContentFromDatabase<ClientEnumeration>(storedProcedureName);
        }

        /// <summary>
        /// Gets Expression Content in Insights Metadata database
        /// </summary>
        /// <returns></returns>
        public List<ClientExpression> GetContentClientExpressions()
        {
            const string storedProcedureName = "[cm].[uspSelectContentClientExpressions]";
            return GetContentFromDatabase<ClientExpression>(storedProcedureName);
        }

        /// <summary>
        /// Updates FileContent in Insights Metadata database
        /// </summary>
        /// <param name="contentTable"></param>
        /// <returns></returns>
        public int UpdateFileContent(DataTable contentTable)
        {
            const string storedProcedureName = "[cm].[uspUpdateClientFileContent]";
            const string paramTable = "@clientFileContentTable";
            var result = UpdateContent(contentTable, storedProcedureName, paramTable);
            return result;
        }

        /// <summary>
        /// Gets FileContent Content in Insights Metadata database
        /// </summary>
        /// <returns></returns>
        public List<ClientFileContent> GetContentClientFileContentList()
        {
            const string storedProcedureName = "[cm].[uspSelectContentClientFileContent]";
            return GetContentFromDatabase<ClientFileContent>(storedProcedureName);
        }

        /// <summary>
        /// Gets Layout Content in Insights Metadata database
        /// </summary>
        /// <returns></returns>
        public List<ClientLayout> GetContentClientLayouts()
        {
            const string storedProcedureName = "[cm].[uspSelectContentClientLayouts]";
            return GetContentFromDatabase<ClientLayout>(storedProcedureName);
        }
        /// <summary>
        /// Gets Measurement Content in Insights Metadata database
        /// </summary>
        /// <returns></returns>
        public List<ClientMeasurement> GetContentClientMeasurements()
        {
            const string storedProcedureName = "[cm].[uspSelectContentClientMeasurements]";
            return GetContentFromDatabase<ClientMeasurement>(storedProcedureName);
        }

        /// <summary>
        /// Gets Profile Attribute Content in Insights Metadata database
        /// </summary>
        /// <returns></returns>
        public List<ClientProfileAttribute> GetContentClientProfileAttributes()
        {
            const string storedProcedureName = "[cm].[uspSelectContentClientProfileAttribute]";
            return GetContentFromDatabase<ClientProfileAttribute>(storedProcedureName);
        }

        /// <summary>
        /// Gets ProfileDefaults Content in Insights Metadata database
        /// </summary>
        /// <returns></returns>
        public List<ClientProfileDefault> GetContentClientProfileDefaults(string profileDefaultCollectionName)
        {
            // sprint 4 - set the profile collection name into dictionary
            var param = new Dictionary<string, string>
            {
                {ParamProfileDefaultCollectionName, profileDefaultCollectionName}
            };
            return GetContentFromDatabase<ClientProfileDefault>(ProcSelectContentClientProfileDefaults, param);
        }

        /// <summary>
        /// Gets ProfileDefaultCollection Content in Insights Metadata database
        /// </summary>
        /// <returns></returns>
        public List<ClientProfileDefaultCollection> GetContentClientProfileDefaultCollections()
        {
            const string storedProcedureName = "[cm].[uspSelectContentClientProfileDefaultCollection]";
            return GetContentFromDatabase<ClientProfileDefaultCollection>(storedProcedureName);
        }

        /// <summary>
        /// Gets ProfileOptions Content in Insights Metadata database
        /// </summary>
        /// <returns></returns>
        public List<ClientProfileOption> GetContentClientProfileOptions()
        {
            const string storedProcedureName = "[cm].[uspSelectContentClientProfileOptions]";
            return GetContentFromDatabase<ClientProfileOption>(storedProcedureName);
        }

        /// <summary>
        /// Gets Profile Section Content in Insights Metadata database
        /// </summary>
        /// <returns></returns>
        public List<ClientProfileSection> GetContentClientProfileSection()
        {
            const string storedProcedureName = "[cm].[uspSelectContentClientProfileSection]";
            return GetContentFromDatabase<ClientProfileSection>(storedProcedureName);
        }

        /// <summary>
        /// Gets Season Content in Insights Metadata database
        /// </summary>
        /// <returns></returns>
        public List<ClientSeason> GetContentClientSeasons()
        {
            const string storedProcedureName = "[cm].[uspSelectContentClientSeason]";
            return GetContentFromDatabase<ClientSeason>(storedProcedureName);
        }

        /// <summary>
        /// Gets Tab Content in Insights Metadata database
        /// </summary>
        /// <returns></returns>
        public List<ClientTab> GetContentClientTabs()
        {
            const string storedProcedureName = "[cm].[uspSelectContentClientTabs]";
            return GetContentFromDatabase<ClientTab>(storedProcedureName);
        }

        /// <summary>
        /// Gets TextContent in Insights Metadata database
        /// </summary>
        /// <returns></returns>
        public List<ClientTextContent> GetContentClientTextContentList(string locale)
        {
            var param = new Dictionary<string, string>
            {
                {ParamLocale, locale}
            };

            return GetContentFromDatabase<ClientTextContent>(ProcSelectContentClientTextContent, param);
        }

        /// <summary>
        /// Gets UOM Content in Insights Metadata database
        /// </summary>
        /// <returns></returns>
        public List<ClientUOM> GetContentClientUoms()
        {
            const string storedProcedureName = "[cm].[uspSelectContentClientUoms]";
            return GetContentFromDatabase<ClientUOM>(storedProcedureName);
        }


        /// <summary>
        /// Gets WhatIfData Content in Insights Metadata database
        /// </summary>
        /// <returns></returns>
        public List<ClientWhatIfData> GetContentClientWhatIfData()
        {
            const string storedProcedureName = "[cm].[uspSelectContentClientWhatIfData]";
            return GetContentFromDatabase<ClientWhatIfData>(storedProcedureName);
        }

        /// <summary>
        /// Gets Widget Content in Insights Metadata database
        /// </summary>
        /// <returns></returns>
        public List<ClientWidget> GetContentClientWidgets()
        {
            const string storedProcedureName = "[cm].[uspSelectContentClientWidgets]";
            return GetContentFromDatabase<ClientWidget>(storedProcedureName);
        }

        #endregion

        #region Update Content Methods

        /// <summary>
        /// Updates Action Content in Insights Metadata database
        /// </summary>
        /// <param name="contentTable"></param>
        /// <returns></returns>
        public int UpdateAction(DataTable contentTable)
        {
            const string storedProcedureName = "[cm].[uspUpdateClientAction]";
            const string paramTable = "@clientActionTable";
            var result = UpdateContent(contentTable, storedProcedureName, paramTable);
            return result;
        }

        /// <summary>
        /// Updates Appliance Content in Insights Metadata database
        /// </summary>
        /// <param name="contentTable"></param>
        /// <returns></returns>
        public int UpdateAppliance(DataTable contentTable)
        {
            const string storedProcedureName = "[cm].[uspUpdateClientAppliance]";
            const string paramTable = "@clientApplianceTable";
            var result = UpdateContent(contentTable, storedProcedureName, paramTable);
            return result;
        }

        /// <summary>
        /// Updates Asset Content in Insights Metadata database
        /// </summary>
        /// <param name="contentTable"></param>
        /// <returns></returns>
        public int UpdateAsset(DataTable contentTable)
        {
            const string storedProcedureName = "[cm].[uspUpdateClientAsset]";
            const string paramTable = "@clientAssetTable";
            var result = UpdateContent(contentTable, storedProcedureName, paramTable);
            return result;
        }

        /// <summary>
        /// Updates BenchmarkGroup Content in Insights Metadata database
        /// </summary>
        /// <param name="contentTable"></param>
        /// <returns></returns>
        public int UpdateBenchmarkGroup(DataTable contentTable)
        {
            const string storedProcedureName = "[cm].[uspUpdateClientBenchmarkGroup]";
            const string paramTable = "@clientBenchmarkGroupTable";
            var result = UpdateContent(contentTable, storedProcedureName, paramTable);
            return result;
        }

        /// <summary>
        /// Updates Commodity Content in Insights Metadata database
        /// </summary>
        /// <param name="contentTable"></param>
        /// <returns></returns>
        public int UpdateCommodity(DataTable contentTable)
        {
            const string storedProcedureName = "[cm].[uspUpdateClientCommodity]";
            const string paramTable = "@clientCommodityTable";
            var result = UpdateContent(contentTable, storedProcedureName, paramTable);
            return result;
        }

        /// <summary>
        /// Updates Condition in Insights Metadata database
        /// </summary>
        /// <param name="contentTable"></param>
        /// <returns></returns>
        public int UpdateCondition(DataTable contentTable)
        {
            const string storedProcedureName = "[cm].[uspUpdateClientCondition]";
            const string paramTable = "@clientConditionTable";
            var result = UpdateContent(contentTable, storedProcedureName, paramTable);
            return result;
        }

        /// <summary>
        /// Updates Configuration in Insights Metadata database
        /// </summary>
        /// <param name="contentTable"></param>
        /// <returns></returns>
        public int UpdateConfiguration(DataTable contentTable)
        {
            const string storedProcedureName = "[cm].[uspUpdateClientConfiguration]";
            const string paramTable = "@clientConfigurationTable";
            var result = UpdateContent(contentTable, storedProcedureName, paramTable);
            return result;
        }

        /// <summary>
        /// Updates Configuration Bulk (Json format) in Insights Metadata database
        /// TFS 598 Aug 2015
        /// </summary>
        /// <param name="contentTable"></param>
        /// <returns></returns>
        public int UpdateConfigurationBulk(DataTable contentTable)
        {
            const string storedProcedureName = "[cm].[uspUpdateClientConfigurationBulk]";
            const string paramTable = "@clientConfigurationBulkTable";
            var result = UpdateContent(contentTable, storedProcedureName, paramTable);
            return result;
        }

        /// <summary>
        /// Updates Currency in Insights Metadata database
        /// </summary>
        /// <param name="contentTable"></param>
        /// <returns></returns>
        public int UpdateCurrency(DataTable contentTable)
        {
            const string storedProcedureName = "[cm].[uspUpdateClientCurrency]";
            const string paramTable = "@clientCurrencyTable";
            var result = UpdateContent(contentTable, storedProcedureName, paramTable);
            return result;
        }

        /// <summary>
        /// Updates EndUse Content in Insights Metadata database
        /// </summary>
        /// <param name="contentTable"></param>
        /// <returns></returns>
        public int UpdateEndUse(DataTable contentTable)
        {
            const string storedProcedureName = "[cm].[uspUpdateClientEndUse]";
            const string paramTable = "@clientEndUseTable";
            var result = UpdateContent(contentTable, storedProcedureName, paramTable);
            return result;
        }

        /// <summary>
        /// Updates Enumeration Content in Insights Metadata database
        /// </summary>
        /// <param name="contentTable"></param>
        /// <returns></returns>
        public int UpdateEnumeration(DataTable contentTable)
        {
            const string storedProcedureName = "[cm].[uspUpdateClientEnumeration]";
            const string paramTable = "@clientEnumerationTable";
            var result = UpdateContent(contentTable, storedProcedureName, paramTable);
            return result;
        }

        /// <summary>
        /// Updates Enumeration Item Content in Insights Metadata database
        /// </summary>
        /// <param name="contentTable"></param>
        /// <returns></returns>
        public int UpdateEnumerationItem(DataTable contentTable)
        {
            const string storedProcedureName = "[cm].[uspUpdateClientEnumerationItem]";
            const string paramTable = "@clientEnumerationItemTable";
            var result = UpdateContent(contentTable, storedProcedureName, paramTable);
            return result;
        }

        /// <summary>
        /// Updates Expression in Insights Metadata database
        /// </summary>
        /// <param name="contentTable"></param>
        /// <returns></returns>
        public int UpdateExpression(DataTable contentTable)
        {
            const string storedProcedureName = "[cm].[uspUpdateClientExpression]";
            const string paramTable = "@clientExpressionTable";
            var result = UpdateContent(contentTable, storedProcedureName, paramTable);
            return result;
        }

        /// <summary>
        /// Updates Measurement Content in Insights Metadata database
        /// </summary>
        /// <param name="contentTable"></param>
        /// <returns></returns>
        public int UpdateMeasurement(DataTable contentTable)
        {
            const string storedProcedureName = "[cm].[uspUpdateClientMeasurement]";
            const string paramTable = "@clientMeasurementTable";
            var result = UpdateContent(contentTable, storedProcedureName, paramTable);
            return result;
        }

        /// <summary>
        /// Updates Layout in Insights Metadata database
        /// </summary>
        /// <param name="contentTable"></param>
        /// <returns></returns>
        public int UpdateLayout(DataTable contentTable)
        {
            const string storedProcedureName = "[cm].[uspUpdateClientLayout]";
            const string paramTable = "@clientLayoutTable";
            var result = UpdateContent(contentTable, storedProcedureName, paramTable);
            return result;
        }


        /// <summary>
        /// Updates Season in Insights Metadata database
        /// </summary>
        /// <param name="contentTable"></param>
        /// <returns></returns>
        public int UpdateSeason(DataTable contentTable)
        {
            const string storedProcedureName = "[cm].[uspUpdateClientSeason]";
            const string paramTable = "@clientSeasonTable";
            var result = UpdateContent(contentTable, storedProcedureName, paramTable);
            return result;
        }

        /// <summary>
        /// Updates ProfileAttribute in Insights Metadata database
        /// </summary>
        /// <param name="contentTable"></param>
        /// <returns></returns>
        public int UpdateProfileAttribute(DataTable contentTable)
        {
            const string storedProcedureName = "[cm].[uspUpdateClientProfileAttribute]";
            const string paramTable = "@clientProfileAttributeTable";
            var result = UpdateContent(contentTable, storedProcedureName, paramTable);
            return result;
        }

        /// <summary>
        /// Updates ProfileDefault in Insights Metadata database
        /// </summary>
        /// <param name="contentTable"></param>
        /// <returns></returns>
        public int UpdateProfileDefault(DataTable contentTable)
        {
            const string storedProcedureName = "[cm].[uspUpdateClientProfileDefault]";
            const string paramTable = "@clientProfileDefaultTable";
            var result = UpdateContent(contentTable, storedProcedureName, paramTable);
            return result;
        }

        /// <summary>
        /// Updates ProfileDefaultCollection in Insights Metadata database
        /// </summary>
        /// <param name="contentTable"></param>
        /// <returns></returns>
        public int UpdateProfileDefaultCollection(DataTable contentTable)
        {
            const string storedProcedureName = "[cm].[uspUpdateClientProfileDefaultCollection]";
            const string paramTable = "@clientProfileDefaultCollectionTable";
            var result = UpdateContent(contentTable, storedProcedureName, paramTable);
            return result;
        }

        /// <summary>
        /// Updates ProfileOption in Insights Metadata database
        /// </summary>
        /// <param name="contentTable"></param>
        /// <returns></returns>
        public int UpdateProfileOption(DataTable contentTable)
        {
            const string storedProcedureName = "[cm].[uspUpdateClientProfileOption]";
            const string paramTable = "@clientProfileOptionTable";
            var result = UpdateContent(contentTable, storedProcedureName, paramTable);
            return result;
        }

        /// <summary>
        /// Updates ProfileSection Content in Insights Metadata database
        /// </summary>
        /// <param name="contentTable"></param>
        /// <returns></returns>
        public int UpdateProfileSection(DataTable contentTable)
        {
            const string storedProcedureName = "[cm].[uspUpdateClientProfileSection]";
            const string paramTable = "@clientProfileSectionTable";
            var result = UpdateContent(contentTable, storedProcedureName, paramTable);
            return result;
        }

        /// <summary>
        /// Updates Tab in Insights Metadata database
        /// </summary>
        /// <param name="contentTable"></param>
        /// <returns></returns>
        public int UpdateTab(DataTable contentTable)
        {
            const string storedProcedureName = "[cm].[uspUpdateClientTab]";
            const string paramTable = "@clientTabTable";
            var result = UpdateContent(contentTable, storedProcedureName, paramTable);
            return result;
        }

        /// <summary>
        /// Updates TextContent in Insights Metadata database
        /// </summary>
        /// <param name="contentTable"></param>
        /// <returns></returns>
        public int UpdateTextContent(DataTable contentTable)
        {
            const string storedProcedureName = "[cm].[uspUpdateClientTextContent]";
            const string paramTable = "@clientTextContentTable";
            var result = UpdateContent(contentTable, storedProcedureName, paramTable);
            return result;
        }

        /// <summary>
        /// Updates Uom Content in Insights Metadata database
        /// </summary>
        /// <param name="contentTable"></param>
        /// <returns></returns>
        public int UpdateUom(DataTable contentTable)
        {
            const string storedProcedureName = "[cm].[uspUpdateClientUom]";
            const string paramTable = "@clientUomTable";
            var result = UpdateContent(contentTable, storedProcedureName, paramTable);
            return result;
        }

        /// <summary>
        /// Updates WhatIfData in Insights Metadata database
        /// </summary>
        /// <param name="contentTable"></param>
        /// <returns></returns>
        public int UpdateWhatIfData(DataTable contentTable)
        {
            const string storedProcedureName = "[cm].[uspUpdateClientWhatIfData]";
            const string paramTable = "@clientWhatIfDataTable";
            var result = UpdateContent(contentTable, storedProcedureName, paramTable);
            return result;
        }

        /// <summary>
        /// Updates Widget in Insights Metadata database
        /// </summary>
        /// <param name="contentTable"></param>
        /// <returns></returns>
        public int UpdateWidget(DataTable contentTable)
        {
            const string storedProcedureName = "[cm].[uspUpdateClientWidget]";
            const string paramTable = "@clientWidgetTable";
            var result = UpdateContent(contentTable, storedProcedureName, paramTable);
            return result;
        }

        #endregion

        #region Utility Methods

        /// <summary>
        /// Returns Data Table based on Content Data Table Type
        /// </summary>
        /// <param name="table"></param>
        /// <returns></returns>
        public DataTable GetDataTable(ContentDataTable table)
        {
            DataTable result;

            const string templateContentNotFound = "ContentDataTable '{0}' not found.";

            var dataTable = Enum.GetName(table.GetType(), table);

            switch (table)
            {
                case ContentDataTable.ClientActionTable:
                    result = new DataTable(dataTable);
                    result.Columns.Add(ContentConstants.StringClientId, typeof(int));
                    result.Columns.Add(ContentConstants.StringActionKey, typeof(string));
                    result.Columns.Add(ContentConstants.StringActionTypeKey, typeof(string));
                    result.Columns.Add(ContentConstants.StringHide, typeof(bool));
                    result.Columns.Add(ContentConstants.StringDisable, typeof(bool));
                    result.Columns.Add(ContentConstants.StringCommodityKey, typeof(string));
                    result.Columns.Add(ContentConstants.StringDifficultyKey, typeof(string));
                    result.Columns.Add(ContentConstants.StringHabitIntervalKey, typeof(string));
                    result.Columns.Add(ContentConstants.StringNameKey, typeof(string));
                    result.Columns.Add(ContentConstants.StringDescriptionKey, typeof(string));
                    result.Columns.Add(ContentConstants.StringAnnualCost, typeof(decimal));
                    result.Columns.Add(ContentConstants.StringUpfrontCost, typeof(decimal));
                    result.Columns.Add(ContentConstants.StringAnnualSavingsEstimate, typeof(decimal));
                    result.Columns.Add(ContentConstants.StringAnnualUsageSavingsEstimate, typeof(decimal));
                    result.Columns.Add(ContentConstants.StringAnnualUsageSavingsUomKey, typeof(string));
                    result.Columns.Add(ContentConstants.StringCostVariancePercent, typeof(decimal));
                    result.Columns.Add(ContentConstants.StringPaybackTime, typeof(int));
                    result.Columns.Add(ContentConstants.StringRoi, typeof(decimal));
                    result.Columns.Add(ContentConstants.StringRebateAvailable, typeof(bool));
                    result.Columns.Add(ContentConstants.StringRebateAmount, typeof(decimal));
                    result.Columns.Add(ContentConstants.StringRebateUrl, typeof(string));
                    result.Columns.Add(ContentConstants.StringRebateImageKey, typeof(string));
                    result.Columns.Add(ContentConstants.StringIconClass, typeof(string));
                    result.Columns.Add(ContentConstants.StringImageKey, typeof(string));
                    result.Columns.Add(ContentConstants.StringVideoKey, typeof(string));
                    result.Columns.Add(ContentConstants.StringComments, typeof(string));
                    result.Columns.Add(ContentConstants.StringConditionKeys, typeof(string));
                    result.Columns.Add(ContentConstants.StringApplianceKeys, typeof(string));
                    result.Columns.Add(ContentConstants.StringSeasonKeys, typeof(string));
                    result.Columns.Add(ContentConstants.StringActionPriority, typeof(int));
                    result.Columns.Add(ContentConstants.StringSavingsCalcMethod, typeof(string));
                    result.Columns.Add(ContentConstants.StringSavingsAmount, typeof(decimal));
                    result.Columns.Add(ContentConstants.StringWhatIfDataKeys, typeof(string));
                    result.Columns.Add(ContentConstants.StringCostExpression, typeof(string));
                    result.Columns.Add(ContentConstants.StringRebateExpression, typeof(string));
                    result.Columns.Add(ContentConstants.StringNextStepLink, typeof(string));
                    result.Columns.Add(ContentConstants.StringNextStepLinkText, typeof(string));
                    result.Columns.Add(ContentConstants.StringNextStepLinkType, typeof(string));
                    result.Columns.Add(ContentConstants.StringTags, typeof(string));
                    result.Columns.Add(ContentConstants.StringMinimumSavingsAmt, typeof(decimal));
                    result.Columns.Add(ContentConstants.StringMaximumSavingsAmt, typeof(decimal));
                    break;
                case ContentDataTable.ClientApplianceLocaleContentTable:
                    result = new DataTable(dataTable);
                    result.Columns.Add(ContentConstants.StringClientId, typeof(int));
                    result.Columns.Add(ContentConstants.StringApplianceKey, typeof(string));
                    result.Columns.Add(ContentConstants.StringLocaleKey, typeof(string));
                    result.Columns.Add(ContentConstants.StringName, typeof(string));
                    break;
                case ContentDataTable.ClientApplianceProfileAttributeTable:
                    result = new DataTable(dataTable);
                    result.Columns.Add(ContentConstants.StringClientId, typeof(int));
                    result.Columns.Add(ContentConstants.StringApplianceKey, typeof(string));
                    result.Columns.Add(ContentConstants.StringProfileAttributeKey, typeof(string));
                    break;
                case ContentDataTable.ClientApplianceTable:
                    result = new DataTable(dataTable);
                    result.Columns.Add(ContentConstants.StringClientId, typeof(int));
                    result.Columns.Add(ContentConstants.StringApplianceKey, typeof(string));
                    result.Columns.Add(ContentConstants.StringNameKey, typeof(string));
                    result.Columns.Add(ContentConstants.StringAverageCostPerYear, typeof(decimal));
                    result.Columns.Add(ContentConstants.StringProfileAttributes, typeof(string));
                    result.Columns.Add(ContentConstants.StringExpressions, typeof(string));
                    break;
                case ContentDataTable.ClientAssetTable:
                    result = new DataTable(dataTable);
                    result.Columns.Add(ContentConstants.StringClientId, typeof(int));
                    result.Columns.Add(ContentConstants.StringAssetKey, typeof(string));
                    result.Columns.Add(ContentConstants.StringLocaleKey, typeof(string));
                    result.Columns.Add(ContentConstants.StringTitle, typeof(string));
                    result.Columns.Add(ContentConstants.StringDescription, typeof(string));
                    result.Columns.Add(ContentConstants.StringFileName, typeof(string));
                    result.Columns.Add(ContentConstants.StringFileContentType, typeof(string));
                    result.Columns.Add(ContentConstants.StringFileSize, typeof(int));
                    result.Columns.Add(ContentConstants.StringFileUrl, typeof(string));
                    break;
                case ContentDataTable.ClientBenchmarkGroupTable:
                    result = new DataTable(dataTable);
                    result.Columns.Add(ContentConstants.StringClientId, typeof(int));
                    result.Columns.Add(ContentConstants.StringBenchmarkGroupKey, typeof(string));
                    result.Columns.Add(ContentConstants.StringNameKey, typeof(string));
                    result.Columns.Add(ContentConstants.StringDescriptionKey, typeof(string));
                    result.Columns.Add(ContentConstants.StringDisable, typeof(bool));
                    result.Columns.Add(ContentConstants.StringHide, typeof(bool));
                    break;
                case ContentDataTable.ClientCommodityTable:
                    result = new DataTable(dataTable);
                    result.Columns.Add(ContentConstants.StringClientId, typeof(int));
                    result.Columns.Add(ContentConstants.StringCommodityKey, typeof(string));
                    result.Columns.Add(ContentConstants.StringNameKey, typeof(string));
                    break;
                case ContentDataTable.ClientConditionTable:
                    result = new DataTable(dataTable);
                    result.Columns.Add(ContentConstants.StringClientId, typeof(int));
                    result.Columns.Add(ContentConstants.StringConditionKey, typeof(string));
                    result.Columns.Add(ContentConstants.StringCategoryKey, typeof(string));
                    result.Columns.Add(ContentConstants.StringDescription, typeof(string));
                    result.Columns.Add(ContentConstants.StringProfileAttributeKey, typeof(string));
                    result.Columns.Add(ContentConstants.StringOperator, typeof(string));
                    result.Columns.Add(ContentConstants.StringValue, typeof(string));
                    result.Columns.Add(ContentConstants.StringProfileOptionKey, typeof(string));
                    break;
                case ContentDataTable.ClientConfigurationTable:
                    result = new DataTable(dataTable);
                    result.Columns.Add(ContentConstants.StringClientId, typeof(int));
                    result.Columns.Add(ContentConstants.StringConfigurationKey, typeof(string));
                    result.Columns.Add(ContentConstants.StringEnvironment, typeof(string));
                    result.Columns.Add(ContentConstants.StringCategory, typeof(string));
                    result.Columns.Add(ContentConstants.StringDescription, typeof(string));
                    result.Columns.Add(ContentConstants.StringValue, typeof(string));
                    break;
                case ContentDataTable.ClientConfigurationBulkTable:
                    result = new DataTable(dataTable);
                    result.Columns.Add(ContentConstants.StringClientId, typeof(int));
                    result.Columns.Add(ContentConstants.StringClientConfigurationBulkKey, typeof(string));
                    result.Columns.Add(ContentConstants.StringName, typeof(string));
                    result.Columns.Add(ContentConstants.StringEnvironment, typeof(string));
                    result.Columns.Add(ContentConstants.StringCategory, typeof(string));
                    result.Columns.Add(ContentConstants.StringDescription, typeof(string));
                    result.Columns.Add(ContentConstants.StringJsonConfiguration, typeof(string));
                    break;
                case ContentDataTable.ClientCurrencyTable:
                    result = new DataTable(dataTable);
                    result.Columns.Add(ContentConstants.StringClientId, typeof(int));
                    result.Columns.Add(ContentConstants.StringCurrencyKey, typeof(string));
                    result.Columns.Add(ContentConstants.StringNameKey, typeof(string));
                    result.Columns.Add(ContentConstants.StringSymbolKey, typeof(string));
                    break;
                case ContentDataTable.ClientEndUseTable:
                    result = new DataTable(dataTable);
                    result.Columns.Add(ContentConstants.StringClientId, typeof(int));
                    result.Columns.Add(ContentConstants.StringEndUseKey, typeof(string));
                    result.Columns.Add(ContentConstants.StringEndUseCategoryKey, typeof(string));
                    result.Columns.Add(ContentConstants.StringNameKey, typeof(string));
                    result.Columns.Add(ContentConstants.StringDisable, typeof(bool));
                    result.Columns.Add(ContentConstants.StringHide, typeof(bool));
                    result.Columns.Add(ContentConstants.StringAppliances, typeof(string));
                    break;
                case ContentDataTable.ClientEnumerationTable:
                    result = new DataTable(dataTable);
                    result.Columns.Add(ContentConstants.StringClientId, typeof(int));
                    result.Columns.Add(ContentConstants.StringEnumerationKey, typeof(string));
                    result.Columns.Add(ContentConstants.StringCategory, typeof(string));
                    result.Columns.Add(ContentConstants.StringEnumerationItems, typeof(string));
                    result.Columns.Add(ContentConstants.StringDefaultEnumerationItem, typeof(string));
                    break;
                case ContentDataTable.ClientEnumerationItemTable:
                    result = new DataTable(dataTable);
                    result.Columns.Add(ContentConstants.StringClientId, typeof(int));
                    result.Columns.Add(ContentConstants.StringEnumerationItemKey, typeof(string));
                    result.Columns.Add(ContentConstants.StringNameKey, typeof(string));
                    break;
                case ContentDataTable.ClientExpressionTable:
                    result = new DataTable(dataTable);
                    result.Columns.Add(ContentConstants.StringClientId, typeof(int));
                    result.Columns.Add(ContentConstants.StringExpressionKey, typeof(string));
                    result.Columns.Add(ContentConstants.StringName, typeof(string));
                    result.Columns.Add(ContentConstants.StringDescription, typeof(string));
                    result.Columns.Add(ContentConstants.StringFormula, typeof(string));
                    result.Columns.Add(ContentConstants.StringComments, typeof(string));
                    break;
                case ContentDataTable.ClientFileContentTable:
                    result = new DataTable(dataTable);
                    result.Columns.Add(ContentConstants.StringClientId, typeof(int));
                    result.Columns.Add(ContentConstants.StringFileContentKey, typeof(string));
                    result.Columns.Add(ContentConstants.StringCategoryKey, typeof(string));
                    result.Columns.Add(ContentConstants.StringSmallFile, typeof(string));
                    result.Columns.Add(ContentConstants.StringMediumFile, typeof(string));
                    result.Columns.Add(ContentConstants.StringLargeFile, typeof(string));
                    break;
                case ContentDataTable.ClientLayoutTable:
                    result = new DataTable(dataTable);
                    result.Columns.Add(ContentConstants.StringClientId, typeof(int));
                    result.Columns.Add(ContentConstants.StringLayoutKey, typeof(string));
                    result.Columns.Add(ContentConstants.StringDescription, typeof(string));
                    result.Columns.Add(ContentConstants.StringLayoutJson, typeof(string));
                    break;
                case ContentDataTable.ClientMeasurementTable:
                    result = new DataTable(dataTable);
                    result.Columns.Add(ContentConstants.StringClientId, typeof(int));
                    result.Columns.Add(ContentConstants.StringMeasurementKey, typeof(string));
                    result.Columns.Add(ContentConstants.StringNameKey, typeof(string));
                    result.Columns.Add(ContentConstants.StringCalculation, typeof(string));
                    result.Columns.Add(ContentConstants.StringComments, typeof(string));
                    break;
                case ContentDataTable.ClientProfileAttributeTable:
                    result = new DataTable(dataTable);
                    result.Columns.Add(ContentConstants.StringClientId, typeof(int));
                    result.Columns.Add(ContentConstants.StringProfileAttributeKey, typeof(string));
                    result.Columns.Add(ContentConstants.StringDescription, typeof(string));
                    result.Columns.Add(ContentConstants.StringProfileAttributeTypeKey, typeof(string));
                    result.Columns.Add(ContentConstants.StringProfileOptions, typeof(string));
                    result.Columns.Add(ContentConstants.StringDisableConditions, typeof(string));
                    result.Columns.Add(ContentConstants.StringEntityLevelKey, typeof(string));
                    result.Columns.Add(ContentConstants.StringQuestionTextKey, typeof(string));
                    result.Columns.Add(ContentConstants.StringMinValue, typeof(int));
                    result.Columns.Add(ContentConstants.StringMaxValue, typeof(int));
                    result.Columns.Add(ContentConstants.StringDoNotDefault, typeof(bool));
                    result.Columns.Add(ContentConstants.StringDisable, typeof(bool));
                    break;
                case ContentDataTable.ClientProfileDefaultTable:
                    result = new DataTable(dataTable);
                    result.Columns.Add(ContentConstants.StringClientId, typeof(int));
                    result.Columns.Add(ContentConstants.StringProfileDefaultKey, typeof(string));
                    result.Columns.Add(ContentConstants.StringProfileDefaultCollectionKey, typeof(string));
                    result.Columns.Add(ContentConstants.StringProfileAttributeKey, typeof(string));
                    result.Columns.Add(ContentConstants.StringDefaultKey, typeof(string));
                    result.Columns.Add(ContentConstants.StringDefaultValue, typeof(string));
                    break;
                case ContentDataTable.ClientProfileDefaultCollectionTable:
                    result = new DataTable(dataTable);
                    result.Columns.Add(ContentConstants.StringClientId, typeof(int));
                    result.Columns.Add(ContentConstants.StringProfileDefaultCollectionKey, typeof(string));
                    result.Columns.Add(ContentConstants.StringNameKey, typeof(string));
                    result.Columns.Add(ContentConstants.StringDescription, typeof(string));
                    break;
                case ContentDataTable.ClientProfileOptionTable:
                    result = new DataTable(dataTable);
                    result.Columns.Add(ContentConstants.StringClientId, typeof(int));
                    result.Columns.Add(ContentConstants.StringProfileOptionKey, typeof(string));
                    result.Columns.Add(ContentConstants.StringProfileOptionValue, typeof(decimal));
                    result.Columns.Add(ContentConstants.StringNameKey, typeof(string));
                    result.Columns.Add(ContentConstants.StringDescription, typeof(string));
                    break;
                case ContentDataTable.ClientProfileSectionTable:
                    result = new DataTable(dataTable);
                    result.Columns.Add(ContentConstants.StringClientId, typeof(int));
                    result.Columns.Add(ContentConstants.StringProfileSectionKey, typeof(string));
                    result.Columns.Add(ContentConstants.StringName, typeof(string));
                    result.Columns.Add(ContentConstants.StringDescriptionKey, typeof(string));
                    result.Columns.Add(ContentConstants.StringRank, typeof(int));
                    result.Columns.Add(ContentConstants.StringTitleKey, typeof(string));
                    result.Columns.Add(ContentConstants.StringSubTitleKey, typeof(string));
                    result.Columns.Add(ContentConstants.StringProfileAttributes, typeof(string));
                    result.Columns.Add(ContentConstants.StringConditions, typeof(string));
                    result.Columns.Add(ContentConstants.StringImages, typeof(string));
                    result.Columns.Add(ContentConstants.StringIntroText, typeof(string)); // TFS 587 July 2015 intro text in datatable for profile section
                    break;
                case ContentDataTable.ClientSeasonTable:
                    result = new DataTable(dataTable);
                    result.Columns.Add(ContentConstants.StringClientId, typeof(int));
                    result.Columns.Add(ContentConstants.StringSeasonKey, typeof(string));
                    result.Columns.Add(ContentConstants.StringNameKey, typeof(string));
                    result.Columns.Add(ContentConstants.StringStartMonth, typeof(int));
                    result.Columns.Add(ContentConstants.StringStartDay, typeof(int));
                    result.Columns.Add(ContentConstants.StringEndMonth, typeof(int));
                    result.Columns.Add(ContentConstants.StringEndDay, typeof(int));
                    break;
                case ContentDataTable.ClientTabTable:
                    result = new DataTable(dataTable);
                    result.Columns.Add(ContentConstants.StringClientId, typeof(int));
                    result.Columns.Add(ContentConstants.StringTabKey, typeof(string));
                    result.Columns.Add(ContentConstants.StringName, typeof(string));
                    result.Columns.Add(ContentConstants.StringMenuOrder, typeof(int));
                    result.Columns.Add(ContentConstants.StringLayoutKey, typeof(string));
                    result.Columns.Add(ContentConstants.StringTabTypeKey, typeof(string));
                    result.Columns.Add(ContentConstants.StringNameKey, typeof(string));
                    result.Columns.Add(ContentConstants.StringWidgetKey, typeof(string));
                    result.Columns.Add(ContentConstants.StringUrl, typeof(string));
                    result.Columns.Add(ContentConstants.StringConditionKeys, typeof(string));
                    result.Columns.Add(ContentConstants.StringConfigurationKeys, typeof(string));
                    result.Columns.Add(ContentConstants.StringTextContentKeys, typeof(string));
                    result.Columns.Add(ContentConstants.StringProfileSectionKeys, typeof(string));
                    result.Columns.Add(ContentConstants.StringChildTabKeys, typeof(string));
                    result.Columns.Add(ContentConstants.StringTabIconType, typeof(string));
                    result.Columns.Add(ContentConstants.StringTabIconClass, typeof(string));
                    result.Columns.Add(ContentConstants.StringImages, typeof(string));
                    result.Columns.Add(ContentConstants.StringActions, typeof(string));
                    result.Columns.Add(ContentConstants.StringDisable, typeof(bool));
                    break;
                case ContentDataTable.ClientTextContentTable:
                    result = new DataTable(dataTable);
                    result.Columns.Add(ContentConstants.StringClientId, typeof(int));
                    result.Columns.Add(ContentConstants.StringTextContentKey, typeof(string));
                    result.Columns.Add(ContentConstants.StringLocaleKey, typeof(string));
                    result.Columns.Add(ContentConstants.StringCategoryKey, typeof(string));
                    result.Columns.Add(ContentConstants.StringShortText, typeof(string));
                    result.Columns.Add(ContentConstants.StringMediumText, typeof(string));
                    result.Columns.Add(ContentConstants.StringLongText, typeof(string));
                    result.Columns.Add(ContentConstants.StringRequireVariableSubstitution, typeof(bool));
                    break;
                case ContentDataTable.ClientUomTable:
                    result = new DataTable(dataTable);
                    result.Columns.Add(ContentConstants.StringClientId, typeof(int));
                    result.Columns.Add(ContentConstants.StringUomKey, typeof(string));
                    result.Columns.Add(ContentConstants.StringNameKey, typeof(string));
                    break;
                case ContentDataTable.ClientWhatIfDataTable:
                    result = new DataTable(dataTable);
                    result.Columns.Add(ContentConstants.StringClientId, typeof(int));
                    result.Columns.Add(ContentConstants.StringWhatIfDataKey, typeof(string));
                    result.Columns.Add(ContentConstants.StringProfileAttributeKey, typeof(string));
                    result.Columns.Add(ContentConstants.StringValueExpressionKey, typeof(string));
                    break;
                case ContentDataTable.ClientWidgetTable:
                    result = new DataTable(dataTable);
                    result.Columns.Add(ContentConstants.StringClientId, typeof(int));
                    result.Columns.Add(ContentConstants.StringWidgetKey, typeof(string));
                    result.Columns.Add(ContentConstants.StringName, typeof(string));
                    result.Columns.Add(ContentConstants.StringTabKey, typeof(string));
                    result.Columns.Add(ContentConstants.StringWidgetTypeKey, typeof(string));
                    result.Columns.Add(ContentConstants.StringWidgetColumn, typeof(int));
                    result.Columns.Add(ContentConstants.StringWidgetRow, typeof(int));
                    result.Columns.Add(ContentConstants.StringWidgetOrder, typeof(int));
                    result.Columns.Add(ContentConstants.StringConditionKeys, typeof(string));
                    result.Columns.Add(ContentConstants.StringConfigurationKeys, typeof(string));
                    result.Columns.Add(ContentConstants.StringTextContentKeys, typeof(string));
                    result.Columns.Add(ContentConstants.StringDisable, typeof(bool));
                    result.Columns.Add(ContentConstants.StringIntroText, typeof(string)); // TFS 584 July 2015 intro text in database for widget
                    break;
                default:
                    throw new ArgumentException(string.Format(templateContentNotFound, dataTable));
            }
            return result;
        }

        /// <summary>
        /// Gets content environment from database
        /// </summary>
        /// <returns></returns>
        public string GetContentEnvironment()
        {
            const string storedProcedureName = "[dbo].[uspSelectContentEnvironment]";

            var result = string.Empty;

            using (var connection = new SqlConnection(_insightsMetaDataConnectionString))
            {
                using (var cmd = new SqlCommand(storedProcedureName, connection))
                {
                    cmd.CommandType = CommandType.StoredProcedure;

                    connection.Open();

                    var reader = cmd.ExecuteScalar();

                    if (reader != null)
                    {
                        result = reader.ToString();
                    }
                }
            }

            return result;
        }

        /// <summary>
        /// Gets the out-of-the-box system modules for the system.
        /// </summary>
        /// <returns></returns>
        public List<PortalModuleSystemConfigurationBulk> GetPortalModuleSystemConfigurationBulks()
        {
            const string storedProcedureName = "[cm].[uspSelectContentSystemConfigurationBulks]";

            var result = new List<PortalModuleSystemConfigurationBulk>();

            using (var connection = new SqlConnection(_insightsMetaDataConnectionString))
            {
                using (var cmd = new SqlCommand(storedProcedureName, connection))
                {
                    cmd.CommandType = CommandType.StoredProcedure;

                    connection.Open();

                    var reader = cmd.ExecuteReader();

                    // This stored procedure should only ever return one record.
                    if (reader.Read())
                    {
                        result.Add(new PortalModuleSystemConfigurationBulk
                        {
                            Key = reader["Key"].ToString(),
                            Category = reader["Category"].ToString(),
                            JsonValue = reader["JsonValue"].ToString()
                        });
                    }
                }
            }

            return result;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dict"></param>
        /// <param name="csvFilePath"></param>
        public void WriteDictionaryToCsv(Dictionary<string, string> dict, string csvFilePath)
        {
            var dt = ToDataTable(dict);
            WriteDataTableToCsv(dt, csvFilePath);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="csvFilePath"></param>
        /// <returns></returns>
        public Dictionary<string, string> GetDictionaryFromCsv(string csvFilePath)
        {
            var result = File.ReadLines(csvFilePath).Select(line => line.Split(',')).ToDictionary(line => line[0], line => line[1]);
            return result;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="outputCsvFilePath"></param>
        public void GetContentOrphan(string outputCsvFilePath)
        {
            const string storedProcedureName = "[cm].[uspGetContentOrphan]";

            using (var connection = new SqlConnection(_insightsMetaDataConnectionString))
            {
                using (var cmd = new SqlCommand(storedProcedureName, connection))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter(ParamClientId, typeof(Int32)) { Value = _clientId });
                    connection.Open();
                    var reader = cmd.ExecuteReader();
                    WriteDataReaderToCsv(reader, outputCsvFilePath, true);
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="contentType"></param>
        /// <param name="clientId"></param>
        /// <param name="key"></param>
        public void DeleteContentOrphan(string contentType, int clientId, string key)
        {

            var storedProcName = "cm.uspDeleteContentOrphan" + contentType;
            DeleteContentOrphanInDatabase(storedProcName, clientId, key);

        }

        public void DeleteContentOrphanLog()
        {
            const string storedProcedureName = "[cm].[uspDeleteContentOrphanLog]";

            using (var connection = new SqlConnection(_insightsMetaDataConnectionString))
            {
                using (var cmd = new SqlCommand(storedProcedureName, connection))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    connection.Open();
                    cmd.ExecuteNonQuery();
                }
            }

        }

        #endregion

        #region  Raw Content Methods

        /// <summary>
        /// Updates content cache in Insights database
        /// </summary>
        /// <param name="clientSpace"></param>
        /// <param name="clientContent"></param>
        /// <returns></returns>
        public int UpdateRawContent(string clientSpace, string clientContent)
        {
            const string storedProcedureName = "[cm].[uspUpdateContentCache]";

            int result;

            using (var connection = new SqlConnection(_insightsMetaDataConnectionString))
            {
                using (var cmd = new SqlCommand(storedProcedureName, connection))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter(ParamClientSpace, typeof(string)) { Value = clientSpace });
                    cmd.Parameters.Add(new SqlParameter(ParamClientContent, typeof(string)) { Value = clientContent });

                    cmd.CommandTimeout = 60;

                    connection.Open();
                    result = cmd.ExecuteNonQuery();
                }
            }

            return result;
        }

        /// <summary>
        /// Retrieves content cache from Insights database
        /// </summary>
        /// <param name="clientSpace"></param>
        /// <returns></returns>
        public string GetRawContent(string clientSpace)
        {
            var result = string.Empty;

            const string storedProcedureName = "[cm].[uspGetContentCache]";

            using (var connection = new SqlConnection(_insightsMetaDataConnectionString))
            {
                using (var cmd = new SqlCommand(storedProcedureName, connection))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter(ParamClientSpace, typeof(string)) { Value = clientSpace });
                    cmd.CommandTimeout = 0;
                    connection.Open();

                    using (var reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            result = reader["ClientContent"].ToString();
                        }
                    }
                }
            }

            return result;
        }

        /// <summary>
        /// Deletes content cache in Insights database
        /// </summary>
        /// <param name="clientSpace"></param>
        /// <returns></returns>
        public int DeleteRawContent(string clientSpace)
        {
            const string storedProcedureName = "[cm].[uspDeleteContentCache]";

            int result;

            using (var connection = new SqlConnection(_insightsMetaDataConnectionString))
            {
                using (var cmd = new SqlCommand(storedProcedureName, connection))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter(ParamClientSpace, typeof(string)) { Value = clientSpace });

                    connection.Open();
                    result = cmd.ExecuteNonQuery();
                }
            }

            return result;
        }

        #endregion

        #region Get Content Defaults Methods

        /// <summary>
        /// Gets Action Keys in Insights Metadata database
        /// </summary>
        /// <param name="clientId"></param>
        /// <returns></returns>
        public List<ClientActionDefault> GetDefaultActionKeys(int clientId)
        {
            const string storedProcedureName = "[cm].[uspSelectDefaultActionKeys]";

            var result = new List<ClientActionDefault>();

            using (var connection = new SqlConnection(_insightsMetaDataConnectionString))
            {
                using (var cmd = new SqlCommand(storedProcedureName, connection))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter(ParamClientId, typeof(int)) { Value = clientId });

                    connection.Open();

                    var reader = cmd.ExecuteReader();

                    while (reader.Read())
                    {
                        var action = new ClientActionDefault
                        {
                            Key = reader.GetStringOrNull((int)ActionContentFields.Key),
                        };

                        result.Add(action);
                    }
                }
            }

            return result;
        }


        #region InsightsPerfModifications

        /// <summary>
        /// Gets Profile Attribute Content in Insights Metadata database
        /// 
        /// </summary>
        /// <param name="clientId"></param>
        /// <param name="attributeKeysFilterTable"></param>
        /// <param name="enduseKeysFilterTable"></param>
        /// <param name="applianceKeysFilterTable"></param>
        /// <param name="entitylevelFilter"></param>
        /// <returns></returns>
        public List<ClientProfileAttributeDefault> GetDefaultProfileAttributeKeys(int clientId,
            DataTable attributeKeysFilterTable, DataTable enduseKeysFilterTable, DataTable applianceKeysFilterTable,
            string entitylevelFilter)
        {
            string storedProcedureName = "[cm].[uspSelectDefaultProfileAttributeKeys_nofn]";

            var result = new List<ClientProfileAttributeDefault>();

            // some perf test stuff to run a tight single threaded latency test 
            int evalTestIterations = 1;  // how many loops to run for perf test. 1 = normal run;
            int evalDiscardIterations = 0;  // how many to skip to "warm up" the pipeline
            bool evalDoPerfEval = false;  // easy way to turn all this stuff off (including config gets)
            if (evalDoPerfEval)
            { // if this is on it looks for configured test values and some stats are collected/displayed
                string evalTempVal;
                if ((evalTempVal = ConfigurationManager.AppSettings.Get("evalTestIterations")) != null)
                {
                    int tval;
                    if (int.TryParse(evalTempVal, out tval))
                    {
                        evalTestIterations = tval;
                    }
                }
                if ((evalTempVal = ConfigurationManager.AppSettings.Get("evalDiscardIterations")) != null)
                {
                    int tval;
                    if (int.TryParse(evalTempVal, out tval))
                    {
                        evalDiscardIterations = tval;
                    }
                }
                if ((evalTempVal = ConfigurationManager.AppSettings.Get("useProfileSproc")) != null)
                {
                    storedProcedureName = evalTempVal;
                }
            }

            // to keep it simple to turn on perf testing, leave these in.
            // with eval off this acts like a simple single request
            int evalTestCount = evalTestIterations;  // counter for test iterations
            int evalDiscardCount = evalDiscardIterations;  // how many hits to ignore (reset start timer) for "warm up" in perf test
            bool evalFirstIteration = true;
            double evalFirstTimeMs = 0; // seperate record for first time which includes cache creation
            DateTime evalStartTime = DateTime.Now;
            while (evalTestCount-- > 0)
            {  // to keep it simple to turn on perf testing, leave this in.
                using (var connection = new SqlConnection(_insightsMetaDataConnectionString))
                {
                    using (var cmd = new SqlCommand(storedProcedureName, connection))
                    {

                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Add(new SqlParameter(ParamClientId, typeof(int)) { Value = clientId });
                        cmd.Parameters.Add(new SqlParameter(ParamAttributeKeysFilterTable, typeof(DataTable))
                        {
                            Value = attributeKeysFilterTable
                        });
                        cmd.Parameters.Add(new SqlParameter(ParamEnduseKeysFilterTable, typeof(DataTable))
                        {
                            Value = enduseKeysFilterTable
                        });
                        cmd.Parameters.Add(new SqlParameter(ParamApplianceKeysFilterTable, typeof(DataTable))
                        {
                            Value = applianceKeysFilterTable
                        });
                        cmd.Parameters.Add(new SqlParameter(ParamEntitylevelFilter, typeof(string))
                        {
                            Value = string.IsNullOrEmpty(entitylevelFilter) ? (object)DBNull.Value : entitylevelFilter
                        });

                        connection.Open();

                        var reader = cmd.ExecuteReader();
                        if (!reader.HasRows) return result;

                        result = ConstructProfileResultsList(reader);
                        // replaced with the above due to change in SPROC and wanting to localize changes
                        //Mapper.CreateMap<IDataReader, ClientProfileAttributeDefault>();
                        //result = Mapper.Map<IDataReader, List<ClientProfileAttributeDefault>>(reader);

                        // code that was commented out before I showed up
                        //while (reader.Read())
                        //{
                        //    var action = new ClientProfileAttributeDefault
                        //    {
                        //        AttributeKey =
                        //            reader.GetStringOrNull((int) ProfileAttributeContentFields.ProfileAttributeKey),
                        //        MinValue = reader.GetInt32OrNull((int) ProfileAttributeContentFields.MinValue),
                        //        MaxValue = reader.GetInt32OrNull((int) ProfileAttributeContentFields.MaxValue),
                        //        OptionKeys = reader.GetStringOrNull((int) ProfileAttributeContentFields.OptionKeys),
                        //        Type = reader.GetStringOrNull((int) ProfileAttributeContentFields.Type),
                        //        EntityLevel = reader.GetStringOrNull((int) ProfileAttributeContentFields.EntityLevel)
                        //    };

                        //    result.Add(action);
                        //}
                    }
                }
                if (evalDoPerfEval)
                {
                    if (evalFirstIteration)
                    {
                        evalFirstIteration = false;
                        evalFirstTimeMs = DateTime.Now.Subtract(evalStartTime).TotalMilliseconds;
                    }
                    if (evalDiscardIterations > 0)
                    {
                        // discarding some for "warm up"
                        if (evalDiscardCount-- > 0)
                        {
                            evalStartTime = DateTime.Now;
                        }
                    }
                }
            } // eval test repeat loop (single hit for non-test scenarios

            if (evalDoPerfEval)
            {
                double evalTotalMs = DateTime.Now.Subtract(evalStartTime).TotalMilliseconds;
                Debug.WriteLine(
                    $" eval test avg={evalTotalMs / (evalTestIterations - evalDiscardIterations)} msec.   {evalTotalMs} msec for {evalTestIterations - evalDiscardIterations} iterations (discarded first {evalDiscardIterations} runs).  first iteration = {evalFirstTimeMs} msec");
            }
            else
            {
                //Debug.WriteLine($" result has {result.Count} rows");
            }

            return result;
        }



        /// <summary>
        /// artifact to hold data which previously was compute in the DB via a function call.
        /// see PrepareOptionKeysCache
        /// </summary>
        static Hashtable _optionKeysHashTable = null;
        static string _padlock = "";  // to use as a lock around cache initialization

        /// note: there are multple places where there is a db function call that gets called for every row in a table during an SP select.
        /// this particular cache technique might make a lot of things faster.

        /// <summary>
        /// create the profile attribute list from the query results.
        /// This was created to deal with the issue of optimizing the data access without
        /// changing the classes.  sproc now returns int id which we use to generate the csv string it used to return
        /// </summary>
        /// <param name="reader"></param>
        /// <returns></returns>
        private List<ClientProfileAttributeDefault> ConstructProfileResultsList(SqlDataReader reader)
        {
            if (_optionKeysHashTable == null)
            {  // create the artifact that replaces the sql function data.
                lock (_padlock)
                {
                    if (_optionKeysHashTable == null)
                    {
                        PrepareOptionKeysCache();
                    }
                }
            }

            DataTable resultTable = new DataTable();
            resultTable.Load(reader);
            List<ClientProfileAttributeDefault> resultList = new List<ClientProfileAttributeDefault>();

            // cols in db row as of this writing 9/23/2016
            //col:AttributeKey
            //col:MinValue
            //col:MaxValue
            //col:OptionKeys
            //col:Type
            //col:EntityLevel

            // having issues with serializer, typesand changing data, so do it old school rather than adjust other things
            foreach (DataRow ro in resultTable.Rows)
            {
                ClientProfileAttributeDefault cpfd = new ClientProfileAttributeDefault();
                cpfd.AttributeKey = ro["AttributeKey"] as string;
                if ((ro["MinValue"] != null) && (ro["MinValue"] is int)) cpfd.MinValue = (int)ro["MinValue"];
                if ((ro["MaxValue"] != null) && (ro["MaxValue"] is int)) cpfd.MaxValue = (int)ro["MaxValue"];
                cpfd.Type = ro["Type"] as string;
                cpfd.EntityLevel = ro["EntityLevel"] as string;
                if ((ro["OptionKeys"] != null) && (ro["OptionKeys"] is int))
                {  // all this is for this guy
                    int optkid = (int)ro["OptionKeys"];
                    if (_optionKeysHashTable[optkid] != null)
                    {
                        cpfd.OptionKeys = _optionKeysHashTable[optkid].ToString();
                    }
                }
                resultList.Add(cpfd);
            }
            return resultList;
        }



        /// <summary>
        /// create the hashtable to hold the attribute profile data which used to be
        /// done in an expensive heavily hit function call in a db sproc.
        /// Replicates the functionality of having a csv list of options for each unique attribute id.
        /// This should be done first time up and perhaps refreshed depending on how volatile the 
        /// ClientProfileAttributeProfileOption table is. Normal pool recycle should be sufficient.
        /// </summary>
        /// <param name="connection"></param>
        private void PrepareOptionKeysCache()
        {
            using (var connection = new SqlConnection(_insightsMetaDataConnectionString))
            {
                connection.Open();
                // don't like this hard coded string here, but it seems to be the pattern for this code base.
                const string queryStg = "SELECT * FROM [cm].[ClientProfileAttributeProfileOption]";
                using (var cmd = new SqlCommand(queryStg, connection))
                {
                    cmd.CommandType = CommandType.Text;
                    using (DataTable optionKeysDataTable = new DataTable())
                    {
                        _optionKeysHashTable = new Hashtable();
                        using (var reader = cmd.ExecuteReader())
                        {
                            optionKeysDataTable.Load(reader);
                        }
                        IEnumerable<int> uniqueIds = (from row in optionKeysDataTable.AsEnumerable()
                                                      select row.Field<int>("ClientProfileAttributeID")).Distinct();
                        foreach (int id in uniqueIds)
                        {
                            var results = from myRow in optionKeysDataTable.AsEnumerable()
                                          where myRow.Field<int>("ClientProfileAttributeID") == id
                                          select myRow;
                            string opts = results.Aggregate("", (current, r) => current + (r["ProfileOptionKey"] + ","));
                            _optionKeysHashTable[id] = opts.TrimEnd(',');
                        }
                    }
                }
            }
        }

        #endregion InsightsPerfModifications

        #endregion

        #endregion

        #region Private Methods

        /// <summary>
        /// Updates Content in Insights Metadata database
        /// </summary>
        /// <param name="contentTable"></param>
        /// <param name="storedProcedureName"></param>
        /// <param name="paramName"></param>
        /// <returns></returns>
        private int UpdateContent(DataTable contentTable, string storedProcedureName, string paramName)
        {

            int result;

            using (var connection = new SqlConnection(_insightsMetaDataConnectionString))
            {
                using (var cmd = new SqlCommand(storedProcedureName, connection))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter(paramName, typeof(DataTable))
                    {
                        Value = contentTable
                    });

                    connection.Open();
                    result = cmd.ExecuteNonQuery();
                }
            }

            return result;
        }

        /// <summary>
        /// Updates Content in Insights Metadata database
        /// </summary>
        private void DeleteContentOrphanInDatabase(string storedProcedureName, int clientId, string key)
        {

            using (var connection = new SqlConnection(_insightsMetaDataConnectionString))
            {
                using (var cmd = new SqlCommand(storedProcedureName, connection))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter(ContentConstants.StringClientId, clientId));
                    cmd.Parameters.Add(new SqlParameter(ContentConstants.StringKey, key));

                    connection.Open();
                    cmd.ExecuteNonQuery();
                }
            }

        }

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="storedProcedureName"></param>
        /// <param name="paramList"></param>
        /// <returns></returns>
        private List<T> GetContentFromDatabase<T>(string storedProcedureName, Dictionary<string, string> paramList = null)
        {
            var result = new List<T>();

            using (var connection = new SqlConnection(_insightsMetaDataConnectionString))
            {
                using (var cmd = new SqlCommand(storedProcedureName, connection))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter(ParamClientId, typeof(int)) { Value = _clientId });
                    string paramValue;
                    switch (storedProcedureName)
                    {
                        case ProcSelectContentClientProfileDefaults:
                            paramValue = GetParamKeyValuePair(paramList, ParamProfileDefaultCollectionName, ContentConstants.StringAclara);
                            cmd.Parameters.Add(new SqlParameter(ParamProfileDefaultCollectionName, typeof(string)) { Value = paramValue });
                            break;
                        case ProcSelectContentClientTextContent:
                            paramValue = GetParamKeyValuePair(paramList, ParamLocale, ContentConstants.StringLocaleEnglish);
                            cmd.Parameters.Add(new SqlParameter(ParamLocale, typeof(string)) { Value = paramValue });
                            break;
                    }
                    connection.Open();
                    var reader = cmd.ExecuteReader();
                    if (!reader.HasRows) return result;
                    Mapper.CreateMap<IDataReader, List<T>>();
                    result = Mapper.Map<IDataReader, List<T>>(reader);
                }
            }

            return result;

        }

        private static string GetParamKeyValuePair(IReadOnlyDictionary<string, string> paramList, string paramName, string defaultValue)
        {
            var value = defaultValue;
            if (paramList != null && paramList.ContainsKey(paramName))
            {
                value = paramList[paramName];
            }

            return value;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dict"></param>
        /// <returns></returns>
        private static DataTable ToDataTable(Dictionary<string, string> dict)
        {
            var table = new DataTable();
            table.Columns.Add("Sys.Id", typeof(string));
            table.Columns.Add("AclaraKey", typeof(string));

            foreach (var entry in dict)
            {
                var dr = table.NewRow();
                dr[0] = entry.Key;
                dr[1] = entry.Value;
                table.Rows.Add(dr);
            }

            return table;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dt"></param>
        /// <param name="csvFilePath"></param>
        private static void WriteDataTableToCsv(DataTable dt, string csvFilePath)
        {
            var sb = new StringBuilder();

            var columnNames = dt.Columns.Cast<DataColumn>().Select(column => column.ColumnName);
            sb.AppendLine(string.Join(",", columnNames));

            foreach (var fields in from DataRow row in dt.Rows select row.ItemArray.Select(field => field.ToString()))
            {
                sb.AppendLine(string.Join(",", fields));
            }

            File.WriteAllText(csvFilePath, sb.ToString());
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dataReader"></param>
        /// <param name="fileName"></param>
        /// <param name="includeHeaderAsFirstRow"></param>
        private static void WriteDataReaderToCsv(IDataReader dataReader, string fileName, bool includeHeaderAsFirstRow)
        {

            const string separator = ",";

            var streamWriter = new StreamWriter(fileName);

            StringBuilder sb;

            if (includeHeaderAsFirstRow)
            {
                sb = new StringBuilder();
                for (var index = 0; index < dataReader.FieldCount; index++)
                {
                    if (dataReader.GetName(index) != null)
                        sb.Append(dataReader.GetName(index));

                    if (index < dataReader.FieldCount - 1)
                        sb.Append(separator);
                }
                streamWriter.WriteLine(sb.ToString());
            }

            while (dataReader.Read())
            {
                sb = new StringBuilder();
                for (var index = 0; index < dataReader.FieldCount - 1; index++)
                {
                    if (!dataReader.IsDBNull(index))
                    {
                        var value = dataReader.GetValue(index).ToString();
                        if (dataReader.GetFieldType(index) == typeof(string))
                        {
                            if (value.IndexOf("\"", StringComparison.Ordinal) >= 0)
                                value = value.Replace("\"", "\"\"");

                            if (value.IndexOf(separator, StringComparison.Ordinal) >= 0)
                                value = "\"" + value + "\"";
                        }
                        sb.Append(value);
                    }

                    if (index < dataReader.FieldCount - 1)
                        sb.Append(separator);
                }

                if (!dataReader.IsDBNull(dataReader.FieldCount - 1))
                    sb.Append(dataReader.GetValue(dataReader.FieldCount - 1).ToString().Replace(separator, " "));

                streamWriter.WriteLine(sb.ToString());
            }
            dataReader.Close();
            streamWriter.Close();
        }
    }

        #endregion

}
