﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using CE.ContentModel.Entities;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using RestSharp;
using System.Net;
using System.Threading.Tasks;

// ReSharper disable once CheckNamespace
namespace CE.ContentModel
{
    /// <summary>
    /// Object Extensions class that provides safe methonds.
    /// </summary>
    public static class ObjectExtensions
    {
        /// <summary>
        /// ToString safe method.
        /// </summary>
        /// <param name="obj"></param>
        /// <returns>Returns null if the string is null instead of null reference exception</returns>
        public static string ToStringSafe(this object obj)
        {
            return obj != null ? obj.ToString() : null;
        }

        /// <summary>
        /// ToString safe method.
        /// </summary>
        /// <param name="obj"></param>
        /// <returns>Returns string.Empty if the string is null instead of null reference exception</returns>
        public static string ToStringValue(this object obj)
        {
            return obj != null ? obj.ToString() : string.Empty;
        }
    }

    /// <summary>
    /// Contentful Adapter for ContentProvider
    /// </summary>
    class ContentProviderContentfulAdapter : IContentProvider
    {

        #region Private Properties

        private readonly IContentRepository _contentRepository;
        private readonly IContentRepositoryCache _contentRepositoryCache;

        #endregion

        #region Constants

        private const int DefaultClientId = 0;

        private const string DefaultUrl = "https://cdn.contentful.com";
        private const string DefaultSpace = "wp32f8j5jqi2";
        private const string DefaultAccessToken = "3fd372f61dc6f11761eaf8a8ff307e4b7572c0d6b4de7b76395d4c7ef572d510";


        private const string DefaultManagementUrl = "https://api.contentful.com";
        private const string DefaultManagementAccessToken = "f9d20476224dee29d3bf0760eeaab7557e1162cf84905a00a8339700e71dc231";

        private const string StringCommon = "common";
        private const string StringAll = "all";
        private const string StringEntry = "Entry";
        private const string StringAsset = "Asset";

        private const string TokenContentType = "sys.contentType.sys.id";
        private const string TokenContentId = "sys.id";
        private const string TemplateTokenField = "fields.{0}.{1}";
        private const string TemplateTokenFieldSysId = "fields.{0}.{1}.sys.id";
        private const string TemplateKeyNotFoundError = "Key for sys.id '{0}' for {1} field with key '{2}' not found. To resolve this error, login to Contentful and republish this entry, wait at least a minute before trying content refresh again.";

        #endregion

        #region Constructor

        //Constructor for testability allow moq content for contentful.
        public ContentProviderContentfulAdapter(IContentRepository contentRepository)
        {
            ////testability allow moq usage for contentful.
            //var result = new JArray();
            //var profileattributesList = new List<ClientProfileAttributeDefault>();
            //var profileattribute = new ClientProfileAttributeDefault
            //{
            //    AttributeKey = "pool.size",
            //    ConditionKeys = null,
            //    DoNotDefault = true,
            //    EntityLevel = "premise",
            //};

            //profileattributesList.Add(profileattribute);
            //var profileattribute1 = new ClientProfileAttributeDefault
            //{
            //    AttributeKey = "pool",
            //    ConditionKeys = null,
            //    DoNotDefault = true,
            //    EntityLevel = "premise",
            //};
            //profileattributesList.Add(profileattribute1);
            //foreach (var item in profileattributesList.Select(JsonConvert.SerializeObject))
            //{
            //    result.Add(item);
            //}

            _contentRepository = contentRepository;
        }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="clientId"></param>
        /// <param name="contentRepository"></param>
        public ContentProviderContentfulAdapter(int clientId, IContentRepository contentRepository)
        {
            _contentRepository = contentRepository;
            _contentRepositoryCache = new ContentRepositoryCacheContentful(clientId, contentRepository);
        }

        #endregion

        #region Public Methods

        #region GetContent Methods

        /// <summary>
        /// Retrieves content for the type Action
        /// </summary>
        /// <param name="keys"></param>
        /// <returns></returns>
        public List<ClientAction> GetContentAction(string keys)
        {
            var itemList = _contentRepositoryCache.GetClientAction();
            return string.IsNullOrEmpty(keys) ? itemList.ToList() : itemList.Where(item => (keys.Split(',').Contains(item.Key))).ToList();
        }

        /// <summary>
        /// Retrieves content for the type Action Savings
        /// </summary>
        /// <param name="keys"></param>
        /// <returns></returns>
        public List<ClientActionSaving> GetContentActionSavings(string keys)
        {
            var itemList = _contentRepositoryCache.GetClientActionSaving();
            return string.IsNullOrEmpty(keys) ? itemList.ToList() : itemList.Where(item => (keys.Split(',').Contains(item.Key))).ToList();
        }

        /// <summary>
        /// Retrieves content for the type Appliance
        /// </summary>
        /// <param name="keys"></param>
        /// <returns></returns>
        public List<ClientAppliance> GetContentAppliance(string keys)
        {
            var itemList = _contentRepositoryCache.GetClientAppliance();
            return string.IsNullOrEmpty(keys) ? itemList.ToList() : itemList.Where(item => (keys.Split(',').Contains(item.Key))).ToList();
        }

        /// <summary>
        /// Retrieves content for the type BenchmarkGroup
        /// </summary>
        /// <returns></returns>
        public List<ClientBenchmarkGroup> GetContentBenchmarkGroup(string keys)
        {
            var itemList = _contentRepositoryCache.GetClientBenchmarkGroup();
            return string.IsNullOrEmpty(keys) ? itemList : itemList.Where(benchmarkGroup => keys.Split(',').Contains(benchmarkGroup.Key)).ToList();
        }

        /// <summary>
        /// Retrieves content for the type Commodity
        /// </summary>
        /// <param name="keys"></param>
        /// <returns></returns>
        public List<ClientCommodity> GetContentCommodity(string keys)
        {
            var itemList = _contentRepositoryCache.GetClientCommodity();
            return string.IsNullOrEmpty(keys) ? itemList.ToList() : itemList.Where(item => (keys.Split(',').Contains(item.Key))).ToList();
        }

        /// <summary>
        /// Retrieves content for the type Commodity Async
        /// </summary>
        /// <param name="keys"></param>
        /// <returns></returns>
        public async Task<List<ClientCommodity>> GetContentCommodityAsync(string keys) {
            var itemList = await Task.Factory.StartNew(() => GetContentCommodity(keys)).ConfigureAwait(false);
            return itemList;
        }

        /// <summary>
        /// Retrieves content for the type Configuration
        /// </summary>
        /// <param name="categories"></param>
        /// <param name="keys"></param>
        /// <returns></returns>
        public List<ClientConfiguration> GetContentConfiguration(string categories, string keys)
        {
            var itemList = _contentRepositoryCache.GetClientConfiguration();

            if ((string.IsNullOrEmpty(categories)) || (categories == StringAll))
            {
                return !string.IsNullOrEmpty(keys) ? itemList.Where(item => keys.Split(',').Contains(item.Key)).ToList() : itemList;
            }

            return !string.IsNullOrEmpty(keys) ?
                itemList.Where(item => ((categories.Split(',').Contains(item.Category)) && (keys.Split(',').Contains(item.Key)))).ToList() :
                itemList.Where(item => ((categories.Split(',').Contains(item.Category)))).ToList();
        }

        /// <summary>
        /// Retrieves content for the type Configuration Async
        /// </summary>
        /// <param name="categories"></param>
        /// <param name="keys"></param>
        /// <returns></returns>
        public async Task<List<ClientConfiguration>> GetContentConfigurationAsync(string categories, string keys) {
            var itemList = await Task.Factory.StartNew(() => GetContentConfiguration(categories, keys)).ConfigureAwait(false);
            return itemList;
        }

        /// <inheritdoc />
        /// <param name="categories"></param>
        /// <param name="keys"></param>
        /// <returns></returns>
        public List<PortalModuleSystemConfigurationBulk> GetPortalModuleSystemConfigurationBulk(string categories, string keys)
        {
            var itemList = _contentRepositoryCache.GetSystemConfigurationBulk();

            if ((string.IsNullOrEmpty(categories)) || (categories == StringAll))
            {
                return !string.IsNullOrEmpty(keys) ? itemList.Where(item => keys.Split(',').Contains(item.Key)).ToList() : itemList;
            }

            return !string.IsNullOrEmpty(keys) ?
                itemList.Where(item => ((categories.Split(',').Contains(item.Category)) && (keys.Split(',').Contains(item.Key)))).ToList() :
                itemList.Where(item => ((categories.Split(',').Contains(item.Category)))).ToList();
        }

        /// <summary>
        /// Retrieves content for the type Client Configuration bulk
        /// TFS 598 Aug 2015
        /// </summary>
        /// <param name="categories"></param>
        /// <param name="keys"></param>
        /// <returns></returns>
        public List<ClientConfigurationBulk> GetContentClientConfigurationBulk(string categories, string keys)
        {
            var itemList = _contentRepositoryCache.GetClientConfigurationBulk();

            if ((string.IsNullOrEmpty(categories)) || (categories == StringAll))
            {
                return !string.IsNullOrEmpty(keys) ? itemList.Where(item => keys.Split(',').Contains(item.Key)).ToList() : itemList;
            }

            return !string.IsNullOrEmpty(keys) ?
                itemList.Where(item => ((categories.Split(',').Contains(item.Category)) && (keys.Split(',').Contains(item.Key)))).ToList() :
                itemList.Where(item => ((categories.Split(',').Contains(item.Category)))).ToList();
        }

        /// <summary>
        /// Retrieves content for the type Condition
        /// </summary>
        /// <param name="categories"></param>
        /// <param name="keys"></param>
        /// <returns></returns>
        public List<ClientCondition> GetContentCondition(string categories, string keys)
        {

            var itemList = _contentRepositoryCache.GetClientCondition();

            if ((string.IsNullOrEmpty(categories)) || (categories == StringAll))
            {
                return !string.IsNullOrEmpty(keys) ? itemList.Where(item => keys.Split(',').Contains(item.Key)).ToList() : itemList;
            }

            return !string.IsNullOrEmpty(keys) ?
                itemList.Where(item => ((categories.Split(',').Contains(item.Category)) && (keys.Split(',').Contains(item.Key)))).ToList() :
                itemList.Where(item => ((categories.Split(',').Contains(item.Category)))).ToList();
        }

        /// <summary>
        /// Retrieves content for the type Currency
        /// </summary>
        /// <returns></returns>
        public List<ClientCurrency> GetContentCurrency(string keys)
        {
            var itemList = _contentRepositoryCache.GetClientCurrency();
            return string.IsNullOrEmpty(keys) ? itemList.ToList() : itemList.Where(item => (keys.Split(',').Contains(item.Key))).ToList();
        }

        /// <summary>
        /// Retrieves content for the type Currency Async
        /// </summary>
        /// <returns></returns>
        public async Task<List<ClientCurrency>> GetContentCurrencyAsync(string keys) {
            var itemList = await Task.Factory.StartNew(() => GetContentCurrency(keys)).ConfigureAwait(false);
            return itemList;
        }

        /// <summary>
        /// Retrieves content for the type Enduse
        /// </summary>
        /// <param name="keys"></param>
        /// <returns></returns>
        public List<ClientEndUse> GetContentEnduse(string keys)
        {
            var itemList = _contentRepositoryCache.GetClientEndUse();
            return string.IsNullOrEmpty(keys) ? itemList.ToList() : itemList.Where(item => (keys.Split(',').Contains(item.Key))).ToList();
        }

        /// <summary>
        /// Retrieves content for the type Enumeration
        /// </summary>
        /// <param name="categories"></param>
        /// <param name="keys"></param>
        /// <returns></returns>
        public List<ClientEnumeration> GetContentEnumeration(string categories, string keys)
        {

            var itemList = _contentRepositoryCache.GetClientEnumeration();

            if ((string.IsNullOrEmpty(categories)) || (categories == StringAll))
            {
                return !string.IsNullOrEmpty(keys) ? itemList.Where(item => keys.Split(',').Contains(item.Key)).ToList() : itemList;
            }

            return !string.IsNullOrEmpty(keys) ?
                itemList.Where(item => ((categories.Split(',').Contains(item.Category)) && (keys.Split(',').Contains(item.Key)))).ToList() :
                itemList.Where(item => ((categories.Split(',').Contains(item.Category)))).ToList();
        }

        /// <summary>
        /// Retrieves content for the type Expression
        /// </summary>
        /// <param name="keys"></param>
        /// <returns></returns>
        public List<ClientExpression> GetContentExpression(string keys)
        {
            var itemList = _contentRepositoryCache.GetClientExpression();
            return string.IsNullOrEmpty(keys) ? itemList.ToList() : itemList.Where(item => (keys.Split(',').Contains(item.Key))).ToList();
        }

        /// <summary>
        /// Retrieves content for the type File Content
        /// </summary>
        /// <param name="categories"></param>
        /// <param name="locale"></param>
        /// <param name="keys"></param>
        /// <returns></returns>
        public List<ClientFileContent> GetContentFileContent(string categories, ContentLocale locale, string keys)
        {
            var localeName = GetContentLocaleName(locale);
            var itemList = _contentRepositoryCache.GetClientFileContent();

            if ((string.IsNullOrEmpty(categories)) || (categories == StringAll))
            {
                return !string.IsNullOrEmpty(keys) ? itemList.Where(item => (item.Locale == localeName) && (keys.Split(',').Contains(item.Key))).ToList() : itemList.Where(item => (item.Locale == localeName)).ToList();
            }

            return !string.IsNullOrEmpty(keys) ?
                itemList.Where(item => ((item.Locale == localeName) && (categories.Split(',').Contains(item.Category)) && (keys.Split(',').Contains(item.Key)))).ToList() :
                itemList.Where(item => ((item.Locale == localeName) && (categories.Split(',').Contains(item.Category)))).ToList();
        }

        /// <summary>
        /// Retrieves content for the type Layout
        /// </summary>
        /// <returns></returns>
        public List<ClientLayout> GetContentLayout(string keys)
        {
            var itemList = _contentRepositoryCache.GetClientLayout();
            return string.IsNullOrEmpty(keys) ? itemList.ToList() : itemList.Where(item => (keys.Split(',').Contains(item.Key))).ToList();
        }

        /// <summary>
        /// Retrieves content for the type Measurement
        /// </summary>
        /// <returns></returns>
        public List<ClientMeasurement> GetContentMeasurement(string keys)
        {
            var itemList = _contentRepositoryCache.GetClientMeasurement();
            return string.IsNullOrEmpty(keys) ? itemList.ToList() : itemList.Where(item => (keys.Split(',').Contains(item.Key))).ToList();
        }

        /// <summary>
        /// Retrieves content for the type ProfileAttribute
        /// </summary>
        /// <param name="keys"></param>
        /// <returns></returns>
        public List<ClientProfileAttribute> GetContentProfileAttribute(string keys)
        {
            var itemList = _contentRepositoryCache.GetClientProfileAttribute();
            return string.IsNullOrEmpty(keys) ? itemList.ToList() : itemList.Where(item => (keys.Split(',').Contains(item.Key))).ToList();
        }

        /// <summary>
        /// Retrieves content for the type Profile Default
        /// </summary>
        /// <param name="keys"></param>
        /// <returns></returns>
        public List<ClientProfileDefault> GetContentProfileDefault(string keys)
        {
            return GetContentProfileDefault(keys, ContentConstants.StringAclara);
        }

        /// <summary>
        /// Retrieves content for the type Profile Default by Profile Collection Name
        /// overload function
        /// sprint 4
        /// </summary>
        /// <param name="keys"></param>
        /// <param name="profileCollectionName"></param>
        /// <returns></returns>
        public List<ClientProfileDefault> GetContentProfileDefault(string keys, string profileCollectionName)
        {
            var itemList = _contentRepositoryCache.GetClientProfileDefault(profileCollectionName);
            return string.IsNullOrEmpty(keys) ? itemList.ToList() : itemList.Where(item => (keys.Split(',').Contains(item.ProfileAttributeKey))).ToList();
        }

        /// <summary>
        /// Retrieves content for the type Profile Default Collection
        /// </summary>
        /// <param name="keys"></param>
        /// <returns></returns>
        public List<ClientProfileDefaultCollection> GetContentProfileDefaultCollection(string keys)
        {
            var itemList = _contentRepositoryCache.GetClientProfileDefaultCollection();
            return string.IsNullOrEmpty(keys) ? itemList.ToList() : itemList.Where(item => (keys.Split(',').Contains(item.Key))).ToList();
        }

        /// <summary>
        /// Retrieves content for the type Profile Option
        /// </summary>
        /// <param name="keys"></param>
        /// <returns></returns>
        public List<ClientProfileOption> GetContentProfileOption(string keys)
        {
            var itemList = _contentRepositoryCache.GetClientProfileOption();
            return string.IsNullOrEmpty(keys) ? itemList.ToList() : itemList.Where(item => (keys.Split(',').Contains(item.Key))).ToList();
        }

        /// <summary>
        /// Retrieves content for the type Profile Section
        /// </summary>
        /// <param name="keys"></param>
        /// <returns></returns>
        public List<ClientProfileSection> GetContentProfileSection(string keys)
        {
            var itemList = _contentRepositoryCache.GetClientProfileSection();
            return string.IsNullOrEmpty(keys) ? itemList.ToList() : itemList.Where(item => (keys.Split(',').Contains(item.Key))).ToList();
        }

        /// <summary>
        /// Retrieves content for the type Season
        /// </summary>
        /// <param name="keys"></param>
        /// <returns></returns>
        public List<ClientSeason> GetContentSeason(string keys)
        {
            var itemList = _contentRepositoryCache.GetClientSeason();
            return string.IsNullOrEmpty(keys) ? itemList.ToList() : itemList.Where(item => (keys.Split(',').Contains(item.Key))).ToList();
        }

        /// <summary>
        /// Retrieves content for the type Tab
        /// </summary>
        /// <param name="keys"></param>
        /// <returns></returns>
        public List<ClientTab> GetContentTab(string keys)
        {
            var itemList = _contentRepositoryCache.GetClientTab();
            return string.IsNullOrEmpty(keys) ? itemList.ToList() : itemList.Where(item => (keys.Split(',').Contains(item.Key))).ToList();
        }

        /// <summary>
        /// Retrieves label content values for the given category and locale.
        /// </summary>
        /// <param name="categories"></param>
        /// <param name="locale"></param>
        /// <param name="keys"></param>
        /// <returns></returns>
        public List<ClientTextContent> GetContentTextContent(string categories, ContentLocale locale, string keys)
        {
            var localeName = GetContentLocaleName(locale);
            var itemList = _contentRepositoryCache.GetClientTextContent(localeName);

            if ((string.IsNullOrEmpty(categories)) || (categories == StringAll))
            {
                return !string.IsNullOrEmpty(keys) ? itemList.Where(item => keys.Split(',').Contains(item.Key)).ToList() : itemList;
            }

            return !string.IsNullOrEmpty(keys) ?
                itemList.Where(item => ((categories.Split(',').Contains(item.Category)) && (keys.Split(',').Contains(item.Key)))).ToList() :
                itemList.Where(item => ((categories.Split(',').Contains(item.Category)))).ToList();
        }

        /// <summary>
        /// Retrieves label content values for the given category and locale Async.
        /// </summary>
        /// <param name="categories"></param>
        /// <param name="locale"></param>
        /// <param name="keys"></param>
        /// <returns></returns>
        public async Task<List<ClientTextContent>> GetContentTextContentAsync(string categories, ContentLocale locale, string keys) {
            var itemList = await Task.Factory.StartNew(() => GetContentTextContent(categories, locale, keys)).ConfigureAwait(false);
            return itemList;
        }

        /// <summary>
        /// Retrieves content for the type Uom
        /// </summary>
        /// <returns></returns>
        public List<ClientUOM> GetContentUom(string keys)
        {
            var itemList = _contentRepositoryCache.GetClientUom();
            return string.IsNullOrEmpty(keys) ? itemList.ToList() : itemList.Where(item => (keys.Split(',').Contains(item.Key))).ToList();
        }

        /// <summary>
        /// Retrieves content for the type Uom Async
        /// </summary>
        /// <param name="keys"></param>
        /// <returns></returns>
        public async Task<List<ClientUOM>> GetContentUomAsync(string keys) {
            var itemList = await Task.Factory.StartNew(() => GetContentUom(keys)).ConfigureAwait(false);
            return itemList;
        }

        /// <summary>
        /// Retrieves content for the type WhatIfData
        /// </summary>
        /// <returns></returns>
        public List<ClientWhatIfData> GetContentWhatIfData(string keys)
        {
            var itemList = _contentRepositoryCache.GetClientWhatIfData();
            return string.IsNullOrEmpty(keys) ? itemList.ToList() : itemList.Where(item => (keys.Split(',').Contains(item.Key))).ToList();
        }

        /// <summary>
        /// Retrieves content for the type Widget
        /// </summary>
        /// <param name="keys"></param>
        /// <returns></returns>
        public List<ClientWidget> GetContentWidget(string keys)
        {
            var itemList = _contentRepositoryCache.GetClientWidget();
            return string.IsNullOrEmpty(keys) ? itemList.ToList() : itemList.Where(item => (keys.Split(',').Contains(item.Key))).ToList();
        }

        #endregion

        #region GetContent Helper Methods

        /// <summary>
        /// 
        /// </summary>
        /// <param name="configurationKey"></param>
        /// <param name="defaultValue"></param>
        /// <returns></returns>
        public int GetContentConfigurationAsInt(string configurationKey, int defaultValue)
        {
            var result = defaultValue;

            var rr = GetContentConfiguration(StringAll, configurationKey);

            if (rr.Any())
            {
                result = int.Parse(rr[0].Value);
            }
            return result;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="configurationKey"></param>
        /// <param name="defaultValue"></param>
        /// <returns></returns>
        public bool GetContentConfigurationAsBoolean(string configurationKey, bool defaultValue)
        {
            var result = defaultValue;

            var rr = GetContentConfiguration(StringAll, configurationKey);

            if (rr.Any())
            {
                result = bool.Parse(rr[0].Value);
            }
            return result;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="configurationKey"></param>
        /// <param name="defaultValue"></param>
        /// <returns></returns>
        public string GetContentConfigurationAsString(string configurationKey, string defaultValue)
        {
            var result = defaultValue;

            var rr = GetContentConfiguration(StringAll, configurationKey);

            if (rr.Any())
            {
                result = rr[0].Value;
            }
            return result;
        }

        /// <summary>
        /// Get configuration from configuration bulk
        /// TFS 598 Aug 2015
        /// </summary>
        /// <param name="configurationKey"></param>
        /// <param name="defaultValue"></param>
        /// <returns></returns>
        public object GetContentConfigurationAsObject(string configurationKey, string defaultValue)
        {
            var result = JsonConvert.DeserializeObject(defaultValue);

            var rr = GetContentClientConfigurationBulk(StringAll, configurationKey);

            if (rr.Any())
            {

                result = JsonConvert.DeserializeObject(rr[0].Value);
            }
            return result;
        }

        #endregion

        #region GetContentTypes Methods

        /// <summary>
        /// Gets content locale based on string passed
        /// </summary>
        /// <param name="locale"></param>
        /// <returns></returns>
        public ContentLocale GetContentLocale(string locale)
        {
            //const string templateLocaleNotFound = "Locale related to '{0}' is not found.";
            ContentLocale result;

            switch (locale)
            {
                case ContentConstants.StringLocaleEnglish:
                    result = ContentLocale.EnglishUnitedStates;
                    break;
                case ContentConstants.StringLocaleSpanish:
                    result = ContentLocale.SpanishSpain;
                    break;
                case ContentConstants.StringLocaleRussian:
                    result = ContentLocale.RussianRussia;
                    break;
                default:
                    result = ContentLocale.EnglishUnitedStates;
                    //TODO: Log warning if incorrect locale is passed here
                    break;
            }

            return result;
        }

        /// <summary>
        /// Gets content environment based on string passed
        /// </summary>
        /// <param name="environment"></param>
        /// <returns></returns>
        public ContentEnvironment GetContentEnvironment(string environment)
        {
            //const string templateEnvironmentNotFound = "Environment related to '{0}' is not found.";
            ContentEnvironment result;
            const string environmentProd = "prod";
            const string environmentDr = "dr";
            const string environmentUat = "uat";
            const string environmentQa = "qa";
            const string environmentDev = "dev";
            const string environmentLocalDev = "localdev";

            switch (environment.ToLower())
            {
                case environmentProd:
                    result = ContentEnvironment.Prod;
                    break;
                case environmentDr:
                    result = ContentEnvironment.Dr;
                    break;
                case environmentUat:
                    result = ContentEnvironment.Uat;
                    break;
                case environmentQa:
                    result = ContentEnvironment.Qa;
                    break;
                case environmentDev:
                    result = ContentEnvironment.Dev;
                    break;
                case environmentLocalDev:
                    result = ContentEnvironment.LocalDev;
                    break;
                default:
                    result = ContentEnvironment.Prod;
                    break;
            }

            return result;
        }

        /// <summary>
        /// Gets content type based on string passed
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        public ContentType GetContentType(string type)
        {
            const string templateTypeNotFound = "Type '{0}' is invalid.";
            ContentType result;

            switch (type.ToLower())
            {
                case "action":
                    result = ContentType.Action;
                    break;
                case "actionsavings":
                    result = ContentType.ActionSavings;
                    break;
                case "appliance":
                    result = ContentType.Appliance;
                    break;
                case "benchmarkgroup":
                    result = ContentType.BenchmarkGroup;
                    break;
                case "commodity":
                    result = ContentType.Commodity;
                    break;
                case "condition":
                    result = ContentType.Condition;
                    break;
                case "configurationbulk":
                    result = ContentType.ConfigurationBulk;
                    break;
                case "configuration":
                    result = ContentType.Configuration;
                    break;
                case "currency":
                    result = ContentType.Currency;
                    break;
                case "enduse":
                    result = ContentType.Enduse;
                    break;
                case "enumeration":
                    result = ContentType.Enumeration;
                    break;
                case "expression":
                    result = ContentType.Expression;
                    break;
                case "filecontent":
                    result = ContentType.FileContent;
                    break;
                case "layout":
                    result = ContentType.Layout;
                    break;
                case "measurement":
                    result = ContentType.Measurement;
                    break;
                case "profileattribute":
                    result = ContentType.ProfileAttribute;
                    break;
                case "profiledefault":
                    result = ContentType.ProfileDefault;
                    break;
                case "profiledefaultcollection":
                    result = ContentType.ProfileDefaultCollection;
                    break;
                case "profileoption":
                    result = ContentType.ProfileOption;
                    break;
                case "profilesection":
                    result = ContentType.ProfileSection;
                    break;
                case "season":
                    result = ContentType.Season;
                    break;
                case "tab":
                    result = ContentType.Tab;
                    break;
                case "textcontent":
                    result = ContentType.TextContent;
                    break;
                case "uom":
                    result = ContentType.Uom;
                    break;
                case "whatifdata":
                    result = ContentType.WhatIfData;
                    break;
                case "widget":
                    result = ContentType.Widget;
                    break;
                default:
                    throw new HttpException(400, string.Format(templateTypeNotFound, type));
            }

            return result;
        }

        /// <summary>
        /// Gets content category based on string passed
        /// </summary>
        /// <param name="category"></param>
        /// <returns></returns>
        public ContentCategory GetContentCategory(string category)
        {
            const string templateCategoryNotFound = "Category '{0}' is invalid.";
            ContentCategory result;

            switch (category.ToLower())
            {
                case "action":
                    result = ContentCategory.Action;
                    break;
                case "benchmark":
                    result = ContentCategory.Benchmark;
                    break;
                case "bill":
                    result = ContentCategory.Bill;
                    break;
                case "billdisagg":
                    result = ContentCategory.BillDisagg;
                    break;
                case "common":
                    result = ContentCategory.Common;
                    break;
                case "consumption":
                    result = ContentCategory.Consumption;
                    break;
                case "energymodel":
                    result = ContentCategory.EnergyModel;
                    break;
                case "notification":
                    result = ContentCategory.Notification;
                    break;
                case "profile":
                    result = ContentCategory.Profile;
                    break;
                case "system":
                    result = ContentCategory.System;
                    break;
                default:
                    throw new HttpException(400, string.Format(templateCategoryNotFound, category));
            }

            return result;
        }

        #endregion

        #region Content Refresh Methods

        /// <summary>
        /// Removes content from memory cache and gets entries from contentful. 
        /// Updates the memory cache with the contentful entries for default content and client content 
        /// by calling the ce.contentmodel  [cm].[uspUpdateContentCache] stored procedure
        /// </summary>
        public void RefreshContentCacheFromContentful(string profileDefaultCollection)
        {
            const string stringDevEnvironment = "dev";

            var cacheKeyEntry = GetRawContentKey(DefaultSpace, StringEntry);
            var cacheKeyAsset = GetRawContentKey(DefaultSpace, StringAsset);

            // Remove serialized content from cache
            _contentRepositoryCache.RemoveClientContentCache(profileDefaultCollection);

            // Get raw Contentful Content 
            var defaultContentEntries = GetDefaultContent(false, StringEntry, cacheKeyEntry);
            var defaultContentAssets = GetDefaultContent(false, StringAsset, cacheKeyAsset);

            // If this is authoring environment, update database content stored in relational database 
            var environment = _contentRepository.GetContentEnvironment();
            if (environment == stringDevEnvironment)
            {
                UpdateContent(defaultContentEntries, defaultContentAssets);
            }
        }


        public bool KeepContentCacheAlive()
        {
            var result = _contentRepositoryCache.GetClientContent(ContentConstants.StringLocaleEnglish,ContentConstants.StringAclara) != null;
            return result;
        }

        #endregion

        #region Application Helper methods

        /// <summary>
        /// Get profile attributes at all entity levels and filter down to a list that honors the filter parameters before returning a list.
        /// If a filter parameter is null or empty then the default for that paramater is to not filter-out attributes based upon that parameter.
        /// Return an empty list if there are no attributes left at the end of filtering.
        /// </summary>
        /// <param name="clientId"></param>
        /// <param name="attributesFilter"></param>
        /// <param name="enduseFilter"></param>
        /// <param name="applianceFilter"></param>
        /// <param name="entitylevelFilter"></param>
        /// <returns></returns>
        public List<ClientProfileAttributeDefault> GetProfileAttributes(int clientId, string attributesFilter,
            string enduseFilter, string applianceFilter, string entitylevelFilter)
        {

            return _contentRepository.GetDefaultProfileAttributeKeys(
                clientId,
                GetKeysDataTable(attributesFilter),
                GetKeysDataTable(enduseFilter),
                GetKeysDataTable(applianceFilter),
                entitylevelFilter);
        }

        /// <summary>
        /// Gets Action Content in Insights Metadata database
        /// </summary>
        /// <param name="clientId"></param>
        /// <returns></returns>
        public List<ClientActionDefault> GetDefaultAction(int clientId)
        {
            return _contentRepository.GetDefaultActionKeys(clientId);
        }


        /// <summary>
        /// get single content from contentful
        /// July 2015 TFS 287
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public JArray GetSingleContentFromContenful(string key)
        {

            return GetSingleContent(DefaultSpace, DefaultManagementAccessToken, DefaultManagementUrl, key);

        }

        /// <summary>
        /// get sing asset form contentful
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public JArray GetSingleAssetFromContenful(string key)
        {

            return GetSingleAsset(DefaultSpace, DefaultManagementAccessToken, DefaultManagementUrl, key);

        }

        /// <summary>
        /// get all content (entries/assets) froom contentful using delivery API sync
        /// </summary>
        /// <param name="contentType"></param>
        /// <returns></returns>
        public JArray GetContentFromContenful(string contentType)
        {

            return GetContent(DefaultSpace, DefaultAccessToken, DefaultUrl, contentType);

        }

        #endregion

        #region Content Debug Methods

        /// <summary>
        /// Retrieves content JSON object for the given content type and key.
        /// </summary>
        /// <param name="type"></param>
        /// <param name="key"></param>
        /// <returns></returns>
        public JObject GetContentRaw(ContentType type, string key)
        {
            const string templateContentNotFound = "Content related to {0} type and {1} key is not found.";

            var typeName = GetContentTypeName(type);

            var result = GetContentEntryByTypeAndKey(GetDefaultContent(true, StringEntry, GetRawContentKey(DefaultSpace, StringEntry)), typeName, key);

            if (result == null)
            {
                throw new ArgumentException(string.Format(templateContentNotFound, typeName, key));
            }

            return result;
        }

        /// <summary>
        /// Retrieves content value for the given content type, key, field name, locale.
        /// </summary>
        /// <param name="type"></param>
        /// <param name="key"></param>
        /// <param name="field"></param>
        /// <param name="locale"></param>
        /// <returns></returns>
        public string GetContentByFieldName(ContentType type, string key, ContentField field, ContentLocale locale)
        {
            const string templateContentNotFound =
                "Content related to {0} type, {1} key, {2} name, and locale {3} is not found.";

            string result;
            var typeName = GetContentTypeName(type);
            var fieldName = GetContentFieldName(field);
            var localeName = GetContentLocaleName(locale);
            var tokenPath = string.Format(TemplateTokenField, fieldName, localeName);

            var content = GetContentEntryByTypeAndKey(GetDefaultContent(true, StringEntry, GetRawContentKey(DefaultSpace, StringEntry)), typeName, key);
            var token = content.SelectToken(tokenPath);

            if (token != null)
            {
                result = token.ToString();
            }
            else
            {
                throw new ArgumentException(string.Format(templateContentNotFound, typeName, key, fieldName, localeName));
            }

            return result;
        }

        #endregion

        #endregion

        #region Private Methods

        /// <summary>
        /// 
        /// </summary>
        /// <param name="fieldName"></param>
        /// <returns></returns>
        private static string GetTokenPath(string fieldName)
        {
            return string.Format(TemplateTokenField, fieldName.ToLower(), ContentConstants.StringLocaleEnglish);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="fieldName"></param>
        /// <returns></returns>
        private static string GetTokenPathOfSysId(string fieldName)
        {
            return string.Format(TemplateTokenFieldSysId, fieldName.ToLower(), ContentConstants.StringLocaleEnglish);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        private JArray GetDefaultContent(bool refresh, string contentType, string cacheKey)
        {
            // Get default content from database
            JArray result = null;

            if (refresh)
            {
                result = GetContentFromRepository(_contentRepository, cacheKey);
            }

            if (result != null) return result;

            // For some reason database doesn't have the content, get the same from contentful and insert into cache
            result = GetContent(DefaultSpace, DefaultAccessToken, DefaultUrl, contentType);
            _contentRepository.UpdateRawContent(cacheKey, result.ToString(Formatting.None));

            return result;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="keyFilter"></param>
        /// <returns></returns>
        private static DataTable GetKeysDataTable(string keyFilter)
        {
            var result = new DataTable("KeyTable");
            result.Columns.Add(ContentConstants.StringKey, typeof(String));

            if (!string.IsNullOrEmpty(keyFilter))
            {
                var keys = keyFilter.Split(',');

                foreach (var key in keys)
                {
                    var dr = result.NewRow();
                    dr[0] = key;
                    result.Rows.Add(dr);
                }
            }
            return result;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="defaultContentEntries"></param>
        /// <returns></returns>
        private static Dictionary<string, string> GetContentfulIdAndKeyDictionary(IEnumerable<JToken> defaultContentEntries)
        {
            var keyPath = GetTokenPath(ContentConstants.StringKey);
            return defaultContentEntries.ToDictionary(content => content.SelectToken(TokenContentId).ToString(), entry => entry.SelectToken(keyPath).ToString());
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="keys"></param>
        /// <param name="content"></param>
        /// <param name="fieldName"></param>
        /// <returns></returns>
        private static string GetLinkedContentKeys(IReadOnlyDictionary<string, string> keys, JToken content, string fieldName)
        {
            var result = string.Empty;
            var sysId = string.Empty;
            var key = string.Empty;

            try
            {
                var links = content.SelectToken(GetTokenPath(fieldName));

                key = content.SelectToken(GetTokenPath(ContentConstants.StringKey)).ToString();

                if (links == null) return result;

                foreach (var link in links)
                {
                    if (result != string.Empty)
                    {
                        result += ";";
                    }
                    sysId = link.SelectToken("sys.id").ToString();
                    result += keys[link.SelectToken("sys.id").ToString()];
                }
            }
            catch (Exception ex)
            {

                throw new ArgumentException(string.Format(TemplateKeyNotFoundError, sysId, fieldName, key), ex);
            }

            return result;
        }

        private static string GetLinkedContentKey(IReadOnlyDictionary<string, string> keys, JToken content, string fieldName)
        {
            var sysId = content.SelectToken(GetTokenPathOfSysId(fieldName)).ToStringValue();
            var key = content.SelectToken(GetTokenPath(ContentConstants.StringKey)).ToString();
            string result = null;
            try
            {
                if (!string.IsNullOrEmpty(sysId))
                {
                    result = keys[sysId];
                }
            }
            catch (Exception ex)
            {
                throw new ArgumentException(string.Format(TemplateKeyNotFoundError, sysId, fieldName, key), ex);
            }

            return result;
        }

        /// <summary>
        /// Updates all content in InsightsMetadata database with the content defined in Contentful
        /// </summary>
        /// <param name="defaultContentEntries"></param>
        /// <param name="defaultContentAssets"></param>
        private void UpdateContent(JArray defaultContentEntries, JArray defaultContentAssets)
        {

            _contentRepository.DeleteContentOrphanLog();

            var keys = GetContentfulIdAndKeyDictionary(defaultContentEntries);

            //_contentRepository.WriteDictionaryToCsv(keys, @"C:\Temp\Contentful\ContentfulKeys.csv");

            UpdateDatabaseAssetFromContentful(defaultContentAssets);
            UpdateDatabaseFileContentFromContentful(defaultContentEntries);
            UpdateDatabaseTextContentFromContentful(defaultContentEntries);
            // TFS 598 Aug 2015 - Configuration Bulk (json format)
            UpdateDatabaseConfigurationBulkFromContentful(defaultContentEntries);
            UpdateDatabaseConfigurationFromContentful(defaultContentEntries);
            UpdateDatabaseLayoutFromContentful(defaultContentEntries);
            UpdateDatabaseTabFromContentful(defaultContentEntries, keys);
            UpdateDatabaseWidgetFromContentful(defaultContentEntries, keys);
            UpdateDatabaseExpressionFromContentful(defaultContentEntries);
            UpdateDatabaseProfileDefaultCollectionFromContentful(defaultContentEntries, keys);
            UpdateDatabaseProfileOptionFromContentful(defaultContentEntries, keys);
            UpdateDatabaseSeasonFromContentful(defaultContentEntries, keys);
            UpdateDatabaseCurrencyFromContentful(defaultContentEntries, keys);
            UpdateDatabaseProfileAttributeFromContentful(defaultContentEntries, keys);
            UpdateDatabaseWhatIfDataFromContentful(defaultContentEntries, keys);
            UpdateDatabaseConditionFromContentful(defaultContentEntries, keys);
            UpdateDatabaseApplianceFromContentful(defaultContentEntries, keys);
            UpdateDatabaseProfileSectionFromContentful(defaultContentEntries, keys);
            UpdateDatabaseEndUseFromContentful(defaultContentEntries, keys);
            UpdateDatabaseEnumerationItemFromContentful(defaultContentEntries, keys);
            UpdateDatabaseEnumerationFromContentful(defaultContentEntries, keys);
            UpdateDatabaseBenchmarkGroupFromContentful(defaultContentEntries, keys);
            UpdateDatabaseCommodityFromContentful(defaultContentEntries, keys);
            UpdateDatabaseMeasurementFromContentful(defaultContentEntries, keys);
            UpdateDatabaseUomFromContentful(defaultContentEntries, keys);
            UpdateDatabaseProfileDefaultFromContentful(defaultContentEntries, keys);
            UpdateDatabaseActionFromContentful(defaultContentEntries, keys);
        }

        /// <summary>
        /// Updates Action content in InsightsMetadata database with the content defined in Contentful
        /// </summary>
        /// <param name="defaultContentEntries"></param>
        /// <param name="keys"></param>
        private void UpdateDatabaseActionFromContentful(JArray defaultContentEntries, IReadOnlyDictionary<string, string> keys)
        {
            var typeName = GetContentTypeName(ContentType.Action);

            var cde = GetContentEntriesDefault(defaultContentEntries, typeName, StringAll);

            var dataTable = _contentRepository.GetDataTable(ContentDataTable.ClientActionTable);

            foreach (var content in cde.Children<JObject>())
            {
                var key = content.SelectToken(GetTokenPath(ContentConstants.StringKey)).ToString();

                var actionTypeKey = content.SelectToken(GetTokenPath(ContentConstants.StringActionTypeKey)).ToString();

                bool disable;
                bool.TryParse(content.SelectToken(GetTokenPath(ContentConstants.StringDisable)).ToString(), out disable);

                bool hide;
                bool.TryParse(content.SelectToken(GetTokenPath(ContentConstants.StringHide)).ToString(), out hide);

                var commodityKey = GetLinkedContentKey(keys, content, ContentConstants.StringCommodityKey);
                var difficultyKey = content.SelectToken(GetTokenPath(ContentConstants.StringDifficulty)).ToString();
                var habitIntervalKey = content.SelectToken(GetTokenPath(ContentConstants.StringHabitInterval)).ToString();
                var nameKey = GetLinkedContentKey(keys, content, ContentConstants.StringNameKey);
                var descriptionKey = GetLinkedContentKey(keys, content, ContentConstants.StringDescriptionKey);
                var annualCost = content.SelectToken(GetTokenPath(ContentConstants.StringAnnualCost)).ToString();
                var upfrontCost = content.SelectToken(GetTokenPath(ContentConstants.StringUpfrontCost)).ToString();
                var annualSavingsEstimate = content.SelectToken(GetTokenPath(ContentConstants.StringDefaultAnnualSavingsEstimate)).ToString();
                var annualUsageSavingsEstimate = content.SelectToken(GetTokenPath(ContentConstants.StringDefaultAnnualUsageSavingsEstimate)).ToString();
                var annualUsageSavingsUomKey = GetLinkedContentKey(keys, content, ContentConstants.StringAnnualUsageSavingsUomKey);
                var costVariancePercent = content.SelectToken(GetTokenPath(ContentConstants.StringCostVariancePercent)).ToString();
                var paybackTime = content.SelectToken(GetTokenPath(ContentConstants.StringDefaultPaybackTime)).ToString();
                var roi = content.SelectToken(GetTokenPath(ContentConstants.StringDefaultRoi)).ToString();

                bool rebateAvailable;
                bool.TryParse(content.SelectToken(GetTokenPath(ContentConstants.StringRebateAvailable)).ToString(), out rebateAvailable);

                var rebateAmount = content.SelectToken(GetTokenPath(ContentConstants.StringRebateAmount)).ToStringSafe();
                var rebateUrl = content.SelectToken(GetTokenPath(ContentConstants.StringRebateUrl)).ToStringSafe();
                var rebateImageKey = GetLinkedContentKey(keys, content, ContentConstants.StringRebateImageKey);
                var iconClass = content.SelectToken(GetTokenPath(ContentConstants.StringIconClass)).ToStringSafe();
                var imageKey = GetLinkedContentKey(keys, content, ContentConstants.StringImageKey);
                var videoKey = GetLinkedContentKey(keys, content, ContentConstants.StringVideoKey);
                var comments = content.SelectToken(GetTokenPath(ContentConstants.StringComments)).ToStringSafe();
                var conditionKeys = GetLinkedContentKeys(keys, content, ContentConstants.StringConditionKeys);
                var applianceKeys = GetLinkedContentKeys(keys, content, ContentConstants.StringApplianceKeys);
                var seasonKeys = GetLinkedContentKeys(keys, content, ContentConstants.StringSeasonKeys);

                int? actionPriority = null;
                var actionPriorityString = content.SelectToken(GetTokenPath(ContentConstants.StringActionPriority)).ToStringSafe();

                if (!string.IsNullOrEmpty(actionPriorityString))
                {
                    actionPriority = int.Parse(actionPriorityString);
                }

                var savingsCalcMethod = content.SelectToken(GetTokenPath(ContentConstants.StringSavingsCalcMethod)).ToStringSafe();
                var savingsAmount = content.SelectToken(GetTokenPath(ContentConstants.StringSavingsAmount)).ToStringSafe();
                var whatIfDataKeys = GetLinkedContentKeys(keys, content, ContentConstants.StringWhatIfData);
                var costExpression = GetLinkedContentKey(keys, content, ContentConstants.StringCostExpression);
                var rebateExpression = GetLinkedContentKey(keys, content, ContentConstants.StringRebateExpression);
                var nextStepLink =  content.SelectToken(GetTokenPath(ContentConstants.StringNextStepLink)).ToStringSafe();
                var nextStepLinkText = GetLinkedContentKey(keys, content, ContentConstants.StringNextStepLinkText);
                var nextStepLinkType = content.SelectToken(GetTokenPath(ContentConstants.StringNextStepLinkType)).ToStringSafe();
                var tags = content.SelectToken(GetTokenPath(ContentConstants.StringTags)).ToStringSafe();
                var savingsMin = content.SelectToken(GetTokenPath(ContentConstants.StringMinimumSavingsAmt)).ToStringSafe();
                var savingsMax = content.SelectToken(GetTokenPath(ContentConstants.StringMaximumSavingsAmt)).ToStringSafe();

                var newRow1 = dataTable.NewRow();
                newRow1[ContentConstants.StringClientId] = DefaultClientId;
                newRow1[ContentConstants.StringActionKey] = key;
                newRow1[ContentConstants.StringActionTypeKey] = actionTypeKey;
                newRow1[ContentConstants.StringHide] = hide;
                newRow1[ContentConstants.StringDisable] = disable;
                newRow1[ContentConstants.StringCommodityKey] = commodityKey;
                newRow1[ContentConstants.StringDifficultyKey] = difficultyKey;
                newRow1[ContentConstants.StringHabitIntervalKey] = habitIntervalKey;
                newRow1[ContentConstants.StringNameKey] = nameKey;
                newRow1[ContentConstants.StringDescriptionKey] = descriptionKey;
                newRow1[ContentConstants.StringAnnualCost] = annualCost;
                newRow1[ContentConstants.StringUpfrontCost] = upfrontCost;
                newRow1[ContentConstants.StringAnnualSavingsEstimate] = annualSavingsEstimate;
                newRow1[ContentConstants.StringAnnualUsageSavingsEstimate] = annualUsageSavingsEstimate;
                newRow1[ContentConstants.StringAnnualUsageSavingsUomKey] = annualUsageSavingsUomKey;
                newRow1[ContentConstants.StringCostVariancePercent] = costVariancePercent;
                newRow1[ContentConstants.StringPaybackTime] = paybackTime;
                newRow1[ContentConstants.StringRoi] = roi;
                newRow1[ContentConstants.StringRebateAvailable] = rebateAvailable;
                newRow1[ContentConstants.StringRebateAmount] = rebateAmount;
                newRow1[ContentConstants.StringRebateUrl] = rebateUrl;
                newRow1[ContentConstants.StringRebateImageKey] = rebateImageKey;
                newRow1[ContentConstants.StringIconClass] = iconClass;
                newRow1[ContentConstants.StringImageKey] = imageKey;
                newRow1[ContentConstants.StringVideoKey] = videoKey;
                newRow1[ContentConstants.StringComments] = comments;
                newRow1[ContentConstants.StringConditionKeys] = conditionKeys;
                newRow1[ContentConstants.StringApplianceKeys] = applianceKeys;
                newRow1[ContentConstants.StringSeasonKeys] = seasonKeys;

                if (actionPriority != null)
                {
                    newRow1[ContentConstants.StringActionPriority] = actionPriority;
                }

                newRow1[ContentConstants.StringSavingsCalcMethod] = savingsCalcMethod;

                if (!string.IsNullOrEmpty(savingsAmount))
                {
                    newRow1[ContentConstants.StringSavingsAmount] = savingsAmount;
                }

                if (whatIfDataKeys != null)
                {
                    newRow1[ContentConstants.StringWhatIfDataKeys] = whatIfDataKeys;
                }

                newRow1[ContentConstants.StringCostExpression] = costExpression;
                newRow1[ContentConstants.StringRebateExpression] = rebateExpression;
                newRow1[ContentConstants.StringNextStepLink] = nextStepLink;
                newRow1[ContentConstants.StringNextStepLinkText] = nextStepLinkText;
                newRow1[ContentConstants.StringNextStepLinkType] = nextStepLinkType;
                newRow1[ContentConstants.StringTags] = tags;

                if (!string.IsNullOrEmpty(savingsMin))
                {
                    newRow1[ContentConstants.StringMinimumSavingsAmt] = savingsMin;
                }

                if (!string.IsNullOrEmpty(savingsMax))
                {
                    newRow1[ContentConstants.StringMaximumSavingsAmt] = savingsMax;
                }

                dataTable.Rows.Add(newRow1);
            }

            _contentRepository.UpdateAction(dataTable);

        }

        /// <summary>
        /// Updates Appliance content in InsightsMetadata database with the content defined in Contentful
        /// </summary>
        /// <param name="defaultContentEntries"></param>
        /// <param name="keys"></param>
        private void UpdateDatabaseApplianceFromContentful(JArray defaultContentEntries, IReadOnlyDictionary<string, string> keys)
        {
            var typeName = GetContentTypeName(ContentType.Appliance);

            var cde = GetContentEntriesDefault(defaultContentEntries, typeName, StringAll);

            var dataTable = _contentRepository.GetDataTable(ContentDataTable.ClientApplianceTable);

            foreach (var content in cde.Children<JObject>())
            {
                var key = content.SelectToken(GetTokenPath(ContentConstants.StringKey)).ToString();
                var nameKey = GetLinkedContentKey(keys, content, ContentConstants.StringNameKey);
                decimal averageCostPerYear;
                decimal.TryParse(content.SelectToken(GetTokenPath(ContentConstants.StringAverageCostPerYear)).ToString(), out averageCostPerYear);
                var profileAttributes = GetLinkedContentKeys(keys, content, ContentConstants.StringProfileAttributes);
                var expressions = GetLinkedContentKeys(keys, content, ContentConstants.StringExpressions);

                var newRow1 = dataTable.NewRow();
                newRow1[ContentConstants.StringClientId] = DefaultClientId;
                newRow1[ContentConstants.StringApplianceKey] = key;
                newRow1[ContentConstants.StringNameKey] = nameKey;
                newRow1[ContentConstants.StringAverageCostPerYear] = averageCostPerYear;
                newRow1[ContentConstants.StringProfileAttributes] = profileAttributes;
                newRow1[ContentConstants.StringExpressions] = expressions;
                dataTable.Rows.Add(newRow1);
            }

            _contentRepository.UpdateAppliance(dataTable);
        }

        /// <summary>
        /// Updates Asset in InsightsMetadata database with the content defined in Contentful
        /// </summary>
        /// <param name="defaultContentAssets"></param>
        private void UpdateDatabaseAssetFromContentful(JArray defaultContentAssets)
        {
            var cde = defaultContentAssets;

            var clientTable = _contentRepository.GetDataTable(ContentDataTable.ClientAssetTable);

            foreach (var content in cde.Children<JObject>())
            {
                var newRow1 = clientTable.NewRow();
                UpdateDataRowFileContent(newRow1, content, ContentConstants.StringLocaleEnglish);
                clientTable.Rows.Add(newRow1);

                var newRow2 = clientTable.NewRow();
                UpdateDataRowFileContent(newRow2, content, ContentConstants.StringLocaleSpanish);
                clientTable.Rows.Add(newRow2);


                var newRow3 = clientTable.NewRow();
                UpdateDataRowFileContent(newRow3, content, ContentConstants.StringLocaleRussian);
                clientTable.Rows.Add(newRow3);
            }

            _contentRepository.UpdateAsset(clientTable);

        }

        private static void UpdateDataRowFileContent(DataRow row, JToken content, string locale)
        {
            const string stringFileSize = "details.size";
            const string stringFileName = "fileName";
            const string stringFileContentType = "contentType";
            const string stringFileUrl = "url";

            var key = content.SelectToken(TokenContentId).ToString();
            var localeForDatabase = locale;

            //TODO: Comment following code once we have unit test to create entries in Contentful Media Library with all localized images.
            if (string.IsNullOrEmpty(GetFieldValueByLocale(content, ContentConstants.StringFile, locale, stringFileSize)))
            {
                locale = ContentConstants.StringLocaleEnglish;
            }

            row[ContentConstants.StringClientId] = DefaultClientId;
            row[ContentConstants.StringAssetKey] = key;
            row[ContentConstants.StringLocaleKey] = localeForDatabase;
            row[ContentConstants.StringTitle] = GetFieldValueByLocale(content, ContentConstants.StringTitle, locale);
            row[ContentConstants.StringDescription] = GetFieldValueByLocale(content, ContentConstants.StringDescription, locale);
            row[ContentConstants.StringFileName] = GetFieldValueByLocale(content, ContentConstants.StringFile, locale, stringFileName);
            row[ContentConstants.StringFileContentType] = GetFieldValueByLocale(content, ContentConstants.StringFile, locale, stringFileContentType);
            row[ContentConstants.StringFileSize] = int.Parse(GetFieldValueByLocale(content, ContentConstants.StringFile, locale, stringFileSize));
            row[ContentConstants.StringFileUrl] = GetFieldValueByLocale(content, ContentConstants.StringFile, locale, stringFileUrl);
        }


        /// <summary>
        /// Updates BenchmarkGroup content in InsightsMetadata database with the content defined in Contentful
        /// </summary>
        /// <param name="defaultContentEntries"></param>
        /// <param name="keys"></param>
        private void UpdateDatabaseBenchmarkGroupFromContentful(JArray defaultContentEntries, IReadOnlyDictionary<string, string> keys)
        {
            var typeName = GetContentTypeName(ContentType.BenchmarkGroup);

            var cde = GetContentEntriesDefault(defaultContentEntries, typeName, StringAll);

            var dataTable = _contentRepository.GetDataTable(ContentDataTable.ClientBenchmarkGroupTable);

            foreach (var content in cde.Children<JObject>())
            {
                var key = content.SelectToken(GetTokenPath(ContentConstants.StringKey)).ToString();
                var nameKey = GetLinkedContentKey(keys, content, ContentConstants.StringNameKey);
                var descriptionKey = GetLinkedContentKey(keys, content, ContentConstants.StringDescriptionKey);
                bool disable;
                bool.TryParse(content.SelectToken(GetTokenPath(ContentConstants.StringDisable)).ToString(), out disable);
                bool hide;
                bool.TryParse(content.SelectToken(GetTokenPath(ContentConstants.StringHide)).ToString(), out hide);

                var newRow1 = dataTable.NewRow();
                newRow1[ContentConstants.StringClientId] = DefaultClientId;
                newRow1[ContentConstants.StringBenchmarkGroupKey] = key;
                newRow1[ContentConstants.StringNameKey] = nameKey;
                newRow1[ContentConstants.StringDescriptionKey] = descriptionKey;
                newRow1[ContentConstants.StringDisable] = disable;
                newRow1[ContentConstants.StringHide] = hide;
                dataTable.Rows.Add(newRow1);
            }

            _contentRepository.UpdateBenchmarkGroup(dataTable);
        }

        /// <summary>
        /// Updates Commodity content in InsightsMetadata database with the content defined in Contentful
        /// </summary>
        /// <param name="defaultContentEntries"></param>
        /// <param name="keys"></param>
        private void UpdateDatabaseCommodityFromContentful(JArray defaultContentEntries, IReadOnlyDictionary<string, string> keys)
        {
            var typeName = GetContentTypeName(ContentType.Commodity);

            var cde = GetContentEntriesDefault(defaultContentEntries, typeName, StringAll);

            var dataTable = _contentRepository.GetDataTable(ContentDataTable.ClientCommodityTable);

            foreach (var content in cde.Children<JObject>())
            {
                var key = content.SelectToken(GetTokenPath(ContentConstants.StringKey)).ToString();
                var nameKey = GetLinkedContentKey(keys, content, ContentConstants.StringNameKey);

                var newRow1 = dataTable.NewRow();
                newRow1[ContentConstants.StringClientId] = DefaultClientId;
                newRow1[ContentConstants.StringCommodityKey] = key;
                newRow1[ContentConstants.StringNameKey] = nameKey;
                dataTable.Rows.Add(newRow1);
            }

            _contentRepository.UpdateCommodity(dataTable);
        }

        /// <summary>
        /// Updates WhatIfData content in InsightsMetadata database with the content defined in Contentful
        /// </summary>
        /// <param name="defaultContentEntries"></param>
        /// <param name="keys"></param>
        private void UpdateDatabaseWhatIfDataFromContentful(JArray defaultContentEntries, IReadOnlyDictionary<string, string> keys)
        {
            var typeName = GetContentTypeName(ContentType.WhatIfData);

            var cde = GetContentEntriesDefault(defaultContentEntries, typeName, StringAll);

            var dataTable = _contentRepository.GetDataTable(ContentDataTable.ClientWhatIfDataTable);

            foreach (var content in cde.Children<JObject>())
            {
                var whatIfDataKey = content.SelectToken(GetTokenPath(ContentConstants.StringKey)).ToString();
                var profileAttributeKey = GetLinkedContentKey(keys, content, ContentConstants.StringProfileAttributeKey);
                var valueExpressionKey = GetLinkedContentKey(keys, content, ContentConstants.StringValueExpressionKey);

                var newRow1 = dataTable.NewRow();
                newRow1[ContentConstants.StringClientId] = DefaultClientId;
                newRow1[ContentConstants.StringWhatIfDataKey] = whatIfDataKey;
                newRow1[ContentConstants.StringProfileAttributeKey] = profileAttributeKey;
                newRow1[ContentConstants.StringValueExpressionKey] = valueExpressionKey;

                dataTable.Rows.Add(newRow1);
            }

            _contentRepository.UpdateWhatIfData(dataTable);

        }

        /// <summary>
        /// Updates Condition content in InsightsMetadata database with the content defined in Contentful
        /// </summary>
        /// <param name="defaultContentEntries"></param>
        /// <param name="keys"></param>
        private void UpdateDatabaseConditionFromContentful(JArray defaultContentEntries, IReadOnlyDictionary<string, string> keys)
        {
            var typeName = GetContentTypeName(ContentType.Condition);

            var cde = GetContentEntriesDefault(defaultContentEntries, typeName, StringAll);

            var dataTable = _contentRepository.GetDataTable(ContentDataTable.ClientConditionTable);

            foreach (var content in cde.Children<JObject>())
            {
                var key = content.SelectToken(GetTokenPath(ContentConstants.StringKey)).ToString();
                var categoryKey = content.SelectToken(GetTokenPath(ContentConstants.StringCategory)).ToString();
                var description = content.SelectToken(GetTokenPath(ContentConstants.StringDescription)).ToString();
                var profileAttributeKey = GetLinkedContentKey(keys, content, ContentConstants.StringProfileAttributeKey);
                var profileOperator = content.SelectToken(GetTokenPath(ContentConstants.StringOperator)).ToString();
                var value = content.SelectToken(GetTokenPath(ContentConstants.StringValue)).ToStringValue();
                var profileOptionKey = GetLinkedContentKey(keys, content, ContentConstants.StringProfileOptionKey);

                var newRow1 = dataTable.NewRow();
                newRow1[ContentConstants.StringClientId] = DefaultClientId;
                newRow1[ContentConstants.StringConditionKey] = key;
                newRow1[ContentConstants.StringCategoryKey] = categoryKey;
                newRow1[ContentConstants.StringDescription] = description;
                newRow1[ContentConstants.StringProfileAttributeKey] = profileAttributeKey;
                newRow1[ContentConstants.StringOperator] = profileOperator;
                newRow1[ContentConstants.StringValue] = value;
                newRow1[ContentConstants.StringProfileOptionKey] = profileOptionKey;
                dataTable.Rows.Add(newRow1);
            }

            _contentRepository.UpdateCondition(dataTable);

        }

        /// <summary>
        /// Updates Configuration content in InsightsMetadata database with the content defined in Contentful
        /// </summary>
        /// <param name="defaultContentEntries"></param>
        private void UpdateDatabaseConfigurationFromContentful(JArray defaultContentEntries)
        {
            var typeName = GetContentTypeName(ContentType.Configuration);

            var cde = GetContentEntriesDefault(defaultContentEntries, typeName, StringAll);

            var dataTable = _contentRepository.GetDataTable(ContentDataTable.ClientConfigurationTable);

            foreach (var content in cde.Children<JObject>())
            {
                var key = content.SelectToken(GetTokenPath(ContentConstants.StringKey)).ToString();
                var environment = content.SelectToken(GetTokenPath(ContentConstants.StringEnvironment)).ToString();
                var category = content.SelectToken(GetTokenPath(ContentConstants.StringCategory)).ToString();
                var description = content.SelectToken(GetTokenPath(ContentConstants.StringDescription)).ToString();
                var value = content.SelectToken(GetTokenPath(ContentConstants.StringValue)).ToString();

                var newRow1 = dataTable.NewRow();
                newRow1[ContentConstants.StringClientId] = DefaultClientId;
                newRow1[ContentConstants.StringConfigurationKey] = key;
                newRow1[ContentConstants.StringEnvironment] = environment;
                newRow1[ContentConstants.StringCategory] = category;
                newRow1[ContentConstants.StringDescription] = description;
                newRow1[ContentConstants.StringValue] = value;
                dataTable.Rows.Add(newRow1);
            }

            _contentRepository.UpdateConfiguration(dataTable);
        }

        /// <summary>
        /// Updates Configuration bulk (Json format) content in InsightsMetadata database with the content defined in Contentful
        /// TFS 598 Aug 2015
        /// </summary>
        /// <param name="defaultContentEntries"></param>
        private void UpdateDatabaseConfigurationBulkFromContentful(JArray defaultContentEntries)
        {
            var typeName = GetContentTypeName(ContentType.ConfigurationBulk);

            var cde = GetContentEntriesDefault(defaultContentEntries, typeName, StringAll);

            var dataTable = _contentRepository.GetDataTable(ContentDataTable.ClientConfigurationBulkTable);

            foreach (var content in cde.Children<JObject>())
            {
                var key = content.SelectToken(GetTokenPath(ContentConstants.StringKey)).ToString();
                var environment = content.SelectToken(GetTokenPath(ContentConstants.StringEnvironment)).ToString();
                var category = content.SelectToken(GetTokenPath(ContentConstants.StringCategory)).ToString();
                var description = content.SelectToken(GetTokenPath(ContentConstants.StringDescription)).ToString();
                var value = content.SelectToken(GetTokenPath(ContentConstants.StringJsonConfiguration)).ToString();
                var name = content.SelectToken(GetTokenPath(ContentConstants.StringName)).ToString();

                var newRow1 = dataTable.NewRow();
                newRow1[ContentConstants.StringClientId] = DefaultClientId;
                newRow1[ContentConstants.StringClientConfigurationBulkKey] = key;
                newRow1[ContentConstants.StringName] = name;
                newRow1[ContentConstants.StringEnvironment] = environment;
                newRow1[ContentConstants.StringCategory] = category;
                newRow1[ContentConstants.StringDescription] = description;
                newRow1[ContentConstants.StringJsonConfiguration] = value;
                dataTable.Rows.Add(newRow1);
            }

            _contentRepository.UpdateConfigurationBulk(dataTable);
        }

        /// <summary>
        /// Updates Currency content in InsightsMetadata database with the content defined in Contentful
        /// </summary>
        /// <param name="defaultContentEntries"></param>
        /// <param name="keys"></param>
        private void UpdateDatabaseCurrencyFromContentful(JArray defaultContentEntries, IReadOnlyDictionary<string, string> keys)
        {
            var typeName = GetContentTypeName(ContentType.Currency);

            var cde = GetContentEntriesDefault(defaultContentEntries, typeName, StringAll);

            var dataTable = _contentRepository.GetDataTable(ContentDataTable.ClientCurrencyTable);

            foreach (var content in cde.Children<JObject>())
            {
                var key = content.SelectToken(GetTokenPath(ContentConstants.StringKey)).ToString();
                var nameKey = GetLinkedContentKey(keys, content, ContentConstants.StringNameKey);
                var symbolKey = GetLinkedContentKey(keys, content, ContentConstants.StringSymbolKey);

                var newRow1 = dataTable.NewRow();
                newRow1[ContentConstants.StringClientId] = DefaultClientId;
                newRow1[ContentConstants.StringCurrencyKey] = key;
                newRow1[ContentConstants.StringNameKey] = nameKey;
                newRow1[ContentConstants.StringSymbolKey] = symbolKey;
                dataTable.Rows.Add(newRow1);
            }

            _contentRepository.UpdateCurrency(dataTable);

        }

        /// <summary>
        /// Updates EndUse content in InsightsMetadata database with the content defined in Contentful
        /// </summary>
        /// <param name="defaultContentEntries"></param>
        /// <param name="keys"></param>
        private void UpdateDatabaseEndUseFromContentful(JArray defaultContentEntries, IReadOnlyDictionary<string, string> keys)
        {
            var typeName = GetContentTypeName(ContentType.Enduse);

            var cde = GetContentEntriesDefault(defaultContentEntries, typeName, StringAll);

            var dataTable = _contentRepository.GetDataTable(ContentDataTable.ClientEndUseTable);

            foreach (var content in cde.Children<JObject>())
            {
                var key = content.SelectToken(GetTokenPath(ContentConstants.StringKey)).ToString();
                var endUseCategorykey = content.SelectToken(GetTokenPath(ContentConstants.StringEndUseCategoryKey)).ToString();
                var nameKey = GetLinkedContentKey(keys, content, ContentConstants.StringNameKey);
                bool disable;
                bool.TryParse(content.SelectToken(GetTokenPath(ContentConstants.StringDisable)).ToString(), out disable);
                bool hide;
                bool.TryParse(content.SelectToken(GetTokenPath(ContentConstants.StringHide)).ToString(), out hide);
                var appliances = GetLinkedContentKeys(keys, content, ContentConstants.StringAppliances);

                var newRow1 = dataTable.NewRow();
                newRow1[ContentConstants.StringClientId] = DefaultClientId;
                newRow1[ContentConstants.StringEndUseKey] = key;
                newRow1[ContentConstants.StringEndUseCategoryKey] = endUseCategorykey;
                newRow1[ContentConstants.StringNameKey] = nameKey;
                newRow1[ContentConstants.StringDisable] = disable;
                newRow1[ContentConstants.StringHide] = hide;
                newRow1[ContentConstants.StringAppliances] = appliances;
                dataTable.Rows.Add(newRow1);
            }

            _contentRepository.UpdateEndUse(dataTable);
        }

        /// <summary>
        /// Updates Enumeration content in InsightsMetadata database with the content defined in Contentful
        /// </summary>
        /// <param name="defaultContentEntries"></param>
        /// <param name="keys"></param>
        private void UpdateDatabaseEnumerationFromContentful(JArray defaultContentEntries, IReadOnlyDictionary<string, string> keys)
        {
            var typeName = GetContentTypeName(ContentType.Enumeration);

            var cde = GetContentEntriesDefault(defaultContentEntries, typeName, StringAll);

            var dataTable = _contentRepository.GetDataTable(ContentDataTable.ClientEnumerationTable);

            foreach (var content in cde.Children<JObject>())
            {
                var key = content.SelectToken(GetTokenPath(ContentConstants.StringKey)).ToString();
                var category = content.SelectToken(GetTokenPath(ContentConstants.StringCategory)).ToString();
                var enumerationItems = GetLinkedContentKeys(keys, content, ContentConstants.StringEnumerationItems);

                if (!string.IsNullOrEmpty(enumerationItems))
                {
                    enumerationItems = enumerationItems.Replace("enum.", "");
                }
                var defaultEnumerationItem = GetLinkedContentKey(keys, content, ContentConstants.StringDefaultEnumerationItem);
                if (!string.IsNullOrEmpty(defaultEnumerationItem))
                {
                    defaultEnumerationItem = defaultEnumerationItem.Replace("enum.", "");
                }

                var newRow1 = dataTable.NewRow();
                newRow1[ContentConstants.StringClientId] = DefaultClientId;
                newRow1[ContentConstants.StringEnumerationKey] = key;
                newRow1[ContentConstants.StringCategory] = category;
                newRow1[ContentConstants.StringEnumerationItems] = enumerationItems;
                newRow1[ContentConstants.StringDefaultEnumerationItem] = defaultEnumerationItem;
                dataTable.Rows.Add(newRow1);
            }

            _contentRepository.UpdateEnumeration(dataTable);
        }

        /// <summary>
        /// Updates EnumerationItem content in InsightsMetadata database with the content defined in Contentful
        /// </summary>
        /// <param name="defaultContentEntries"></param>
        /// <param name="keys"></param>
        private void UpdateDatabaseEnumerationItemFromContentful(JArray defaultContentEntries, IReadOnlyDictionary<string, string> keys)
        {
            var typeName = GetContentTypeName(ContentType.EnumerationItem);

            var cde = GetContentEntriesDefault(defaultContentEntries, typeName, StringAll);

            var dataTable = _contentRepository.GetDataTable(ContentDataTable.ClientEnumerationItemTable);

            foreach (var content in cde.Children<JObject>())
            {
                var key = content.SelectToken(GetTokenPath(ContentConstants.StringKey)).ToString().Replace("enum.", "");
                var nameKey = GetLinkedContentKey(keys, content, ContentConstants.StringNameKey);

                var newRow1 = dataTable.NewRow();
                newRow1[ContentConstants.StringClientId] = DefaultClientId;
                newRow1[ContentConstants.StringEnumerationItemKey] = key;
                newRow1[ContentConstants.StringNameKey] = nameKey;
                dataTable.Rows.Add(newRow1);
            }

            _contentRepository.UpdateEnumerationItem(dataTable);
        }

        /// <summary>
        /// Updates Expression content in InsightsMetadata database with the content defined in Contentful
        /// </summary>
        /// <param name="defaultContentEntries"></param>
        private void UpdateDatabaseExpressionFromContentful(JArray defaultContentEntries)
        {
            var typeName = GetContentTypeName(ContentType.Expression);

            var cde = GetContentEntriesDefault(defaultContentEntries, typeName, StringAll);

            var dataTable = _contentRepository.GetDataTable(ContentDataTable.ClientExpressionTable);

            foreach (var content in cde.Children<JObject>())
            {
                var key = content.SelectToken(GetTokenPath(ContentConstants.StringKey)).ToString();
                var name = content.SelectToken(GetTokenPath(ContentConstants.StringName)).ToString();
                var description = content.SelectToken(GetTokenPath(ContentConstants.StringDescription)).ToString();
                var formula = content.SelectToken(GetTokenPath(ContentConstants.StringFormula)).ToString();
                var comments = content.SelectToken(GetTokenPath(ContentConstants.StringComments)).ToStringSafe();

                var newRow1 = dataTable.NewRow();
                newRow1[ContentConstants.StringClientId] = DefaultClientId;
                newRow1[ContentConstants.StringExpressionKey] = key;
                newRow1[ContentConstants.StringName] = name;
                newRow1[ContentConstants.StringDescription] = description;
                newRow1[ContentConstants.StringFormula] = formula;
                newRow1[ContentConstants.StringComments] = comments;
                dataTable.Rows.Add(newRow1);
            }

            _contentRepository.UpdateExpression(dataTable);

        }

        /// <summary>
        /// Updates FileContent content in InsightsMetadata database with the content defined in Contentful
        /// </summary>
        /// <param name="defaultContentEntries"></param>
        private void UpdateDatabaseFileContentFromContentful(JArray defaultContentEntries)
        {
            var typeName = GetContentTypeName(ContentType.FileContent);

            var cde = GetContentEntriesDefault(defaultContentEntries, typeName, StringAll);

            var dataTable = _contentRepository.GetDataTable(ContentDataTable.ClientFileContentTable);

            foreach (var content in cde.Children<JObject>())
            {
                var key = content.SelectToken(GetTokenPath(ContentConstants.StringKey)).ToString();
                var categoryKey = content.SelectToken(GetTokenPath(ContentConstants.StringCategory)).ToString();
                var smallFile = content.SelectToken(GetTokenPathOfSysId(ContentConstants.StringSmallFile)).ToStringValue();
                var mediumFile = content.SelectToken(GetTokenPathOfSysId(ContentConstants.StringMediumFile)).ToStringValue();
                var largeFile = content.SelectToken(GetTokenPathOfSysId(ContentConstants.StringLargeFile)).ToStringValue();

                var newRow1 = dataTable.NewRow();
                newRow1[ContentConstants.StringClientId] = DefaultClientId;
                newRow1[ContentConstants.StringFileContentKey] = key;
                newRow1[ContentConstants.StringCategoryKey] = categoryKey;
                newRow1[ContentConstants.StringSmallFile] = smallFile;
                newRow1[ContentConstants.StringMediumFile] = mediumFile;
                newRow1[ContentConstants.StringLargeFile] = largeFile;
                dataTable.Rows.Add(newRow1);
            }

            _contentRepository.UpdateFileContent(dataTable);
        }

        /// <summary>
        /// Updates Layout content in InsightsMetadata database with the content defined in Contentful
        /// </summary>
        /// <param name="defaultContentEntries"></param>
        private void UpdateDatabaseLayoutFromContentful(JArray defaultContentEntries)
        {
            var typeName = GetContentTypeName(ContentType.Layout);

            var cde = GetContentEntriesDefault(defaultContentEntries, typeName, StringAll);

            var dataTable = _contentRepository.GetDataTable(ContentDataTable.ClientLayoutTable);

            foreach (var content in cde.Children<JObject>())
            {
                var key = content.SelectToken(GetTokenPath(ContentConstants.StringKey)).ToString();
                var description = content.SelectToken(GetTokenPath(ContentConstants.StringDescription)).ToString();
                var layoutJson = content.SelectToken(GetTokenPath(ContentConstants.StringLayoutJson)).ToString();

                var newRow1 = dataTable.NewRow();
                newRow1[ContentConstants.StringClientId] = DefaultClientId;
                newRow1[ContentConstants.StringLayoutKey] = key;
                newRow1[ContentConstants.StringDescription] = description;
                newRow1[ContentConstants.StringLayoutJson] = layoutJson;
                dataTable.Rows.Add(newRow1);
            }

            _contentRepository.UpdateLayout(dataTable);

        }

        /// <summary>
        /// Updates Measurement content in InsightsMetadata database with the content defined in Contentful
        /// </summary>
        /// <param name="defaultContentEntries"></param>
        /// <param name="keys"></param>
        private void UpdateDatabaseMeasurementFromContentful(JArray defaultContentEntries, IReadOnlyDictionary<string, string> keys)
        {
            var typeName = GetContentTypeName(ContentType.Measurement);

            var cde = GetContentEntriesDefault(defaultContentEntries, typeName, StringAll);

            var dataTable = _contentRepository.GetDataTable(ContentDataTable.ClientMeasurementTable);

            foreach (var content in cde.Children<JObject>())
            {
                var key = content.SelectToken(GetTokenPath(ContentConstants.StringKey)).ToString();
                var nameKey = GetLinkedContentKey(keys, content, ContentConstants.StringNameKey);
                var calculation = content.SelectToken(GetTokenPath(ContentConstants.StringCalculation)).ToString();
                var comments = content.SelectToken(GetTokenPath(ContentConstants.StringComments)).ToStringValue();

                var newRow1 = dataTable.NewRow();
                newRow1[ContentConstants.StringClientId] = DefaultClientId;
                newRow1[ContentConstants.StringMeasurementKey] = key;
                newRow1[ContentConstants.StringNameKey] = nameKey;
                newRow1[ContentConstants.StringCalculation] = calculation;
                newRow1[ContentConstants.StringComments] = comments;
                dataTable.Rows.Add(newRow1);
            }

            _contentRepository.UpdateMeasurement(dataTable);
        }

        /// <summary>
        /// Updates Layout content in InsightsMetadata database with the content defined in Contentful
        /// </summary>
        /// <param name="defaultContentEntries"></param>
        /// <param name="keys"></param>
        private void UpdateDatabaseSeasonFromContentful(JArray defaultContentEntries, IReadOnlyDictionary<string, string> keys)
        {
            var typeName = GetContentTypeName(ContentType.Season);

            var cde = GetContentEntriesDefault(defaultContentEntries, typeName, StringAll);

            var dataTable = _contentRepository.GetDataTable(ContentDataTable.ClientSeasonTable);

            foreach (var content in cde.Children<JObject>())
            {
                var key = content.SelectToken(GetTokenPath(ContentConstants.StringKey)).ToString();
                var nameKey = GetLinkedContentKey(keys, content, ContentConstants.StringNameKey);
                var startMonth = content.SelectToken(GetTokenPath(ContentConstants.StringStartMonth)).ToString();
                var startDay = content.SelectToken(GetTokenPath(ContentConstants.StringStartDay)).ToString();
                var endMonth = content.SelectToken(GetTokenPath(ContentConstants.StringEndMonth)).ToString();
                var endDay = content.SelectToken(GetTokenPath(ContentConstants.StringEndDay)).ToString();

                var newRow1 = dataTable.NewRow();
                newRow1[ContentConstants.StringClientId] = DefaultClientId;
                newRow1[ContentConstants.StringSeasonKey] = key;
                newRow1[ContentConstants.StringNameKey] = nameKey;
                newRow1[ContentConstants.StringStartMonth] = startMonth;
                newRow1[ContentConstants.StringStartDay] = startDay;
                newRow1[ContentConstants.StringEndMonth] = endMonth;
                newRow1[ContentConstants.StringEndDay] = endDay;
                dataTable.Rows.Add(newRow1);
            }

            _contentRepository.UpdateSeason(dataTable);

        }

        /// <summary>
        /// Updates ProfileAttribute content in InsightsMetadata database with the content defined in Contentful
        /// </summary>
        /// <param name="defaultContentEntries"></param>
        /// <param name="keys"></param>
        private void UpdateDatabaseProfileAttributeFromContentful(JArray defaultContentEntries, IReadOnlyDictionary<string, string> keys)
        {
            var typeName = GetContentTypeName(ContentType.ProfileAttribute);

            var cde = GetContentEntriesDefault(defaultContentEntries, typeName, StringAll);

            var dataTable = _contentRepository.GetDataTable(ContentDataTable.ClientProfileAttributeTable);

            foreach (var content in cde.Children<JObject>())
            {
                var key = content.SelectToken(GetTokenPath(ContentConstants.StringKey)).ToString();
                var description = content.SelectToken(GetTokenPath(ContentConstants.StringDescription)).ToString();
                var profileAttributeTypeKey = content.SelectToken(GetTokenPath(ContentConstants.StringProfileAttributeTypeKey)).ToString();
                var profileOptions = GetLinkedContentKeys(keys, content, ContentConstants.StringProfileOptions);
                var disableConditions = GetLinkedContentKeys(keys, content, ContentConstants.StringDisableConditions);
                var entityLevelKey = content.SelectToken(GetTokenPath(ContentConstants.StringEntityLevelKey)).ToString();
                var questionTextKey = GetLinkedContentKey(keys, content, ContentConstants.StringQuestionTextKey);
                int? minValue = null;
                var minValueString = content.SelectToken(GetTokenPath(ContentConstants.StringMinValue)).ToStringSafe();
                if (!string.IsNullOrEmpty(minValueString))
                    minValue = int.Parse(minValueString);
                int? maxValue = null;
                var maxValueString = content.SelectToken(GetTokenPath(ContentConstants.StringMaxValue)).ToStringSafe();
                if (!string.IsNullOrEmpty(maxValueString))
                    maxValue = int.Parse(maxValueString);
                bool doNotDefault;
                bool.TryParse(content.SelectToken(GetTokenPath(ContentConstants.StringDoNotDefault)).ToString(), out doNotDefault);
                bool disable;
                bool.TryParse(content.SelectToken(GetTokenPath(ContentConstants.StringDisable)).ToString(), out disable);

                var newRow1 = dataTable.NewRow();
                newRow1[ContentConstants.StringClientId] = DefaultClientId;
                newRow1[ContentConstants.StringProfileAttributeKey] = key;
                newRow1[ContentConstants.StringDescription] = description;
                newRow1[ContentConstants.StringProfileAttributeTypeKey] = profileAttributeTypeKey;
                newRow1[ContentConstants.StringProfileOptions] = profileOptions;
                newRow1[ContentConstants.StringDisableConditions] = disableConditions;
                newRow1[ContentConstants.StringEntityLevelKey] = entityLevelKey;
                newRow1[ContentConstants.StringQuestionTextKey] = questionTextKey;
                if (minValue != null)
                    newRow1[ContentConstants.StringMinValue] = minValue;
                if (maxValue != null)
                    newRow1[ContentConstants.StringMaxValue] = maxValue;
                newRow1[ContentConstants.StringDoNotDefault] = doNotDefault;
                newRow1[ContentConstants.StringDisable] = disable;

                dataTable.Rows.Add(newRow1);
            }

            _contentRepository.UpdateProfileAttribute(dataTable);

        }

        /// <summary>
        /// Updates ProfileDefault content in InsightsMetadata database with the content defined in Contentful
        /// </summary>
        /// <param name="defaultContentEntries"></param>
        /// <param name="keys"></param>
        private void UpdateDatabaseProfileDefaultFromContentful(JArray defaultContentEntries, IReadOnlyDictionary<string, string> keys)
        {
            var typeName = GetContentTypeName(ContentType.ProfileDefault);

            var cde = GetContentEntriesDefault(defaultContentEntries, typeName, StringAll);

            var dataTable = _contentRepository.GetDataTable(ContentDataTable.ClientProfileDefaultTable);

            foreach (var content in cde.Children<JObject>())
            {
                var profileDefaultCollectionKey = GetLinkedContentKey(keys, content, ContentConstants.StringProfileDefaultCollectionKey);
                // remove profiledefaultcollectionname prefix from key before insert into database; it was there only for contentful
                var key = content.SelectToken(GetTokenPath(ContentConstants.StringKey)).ToString().Replace(profileDefaultCollectionKey + ".", "");
                var profileAttributeKey = GetLinkedContentKey(keys, content, ContentConstants.StringProfileAttributeKey);
                var defaultKey = GetLinkedContentKey(keys, content, ContentConstants.StringDefaultKey);
                var defaultValue = content.SelectToken(GetTokenPath(ContentConstants.StringDefaultValue)).ToStringSafe();

                var newRow1 = dataTable.NewRow();
                newRow1[ContentConstants.StringClientId] = DefaultClientId;
                newRow1[ContentConstants.StringProfileDefaultKey] = key;
                newRow1[ContentConstants.StringProfileDefaultCollectionKey] = profileDefaultCollectionKey;
                newRow1[ContentConstants.StringProfileAttributeKey] = profileAttributeKey;
                newRow1[ContentConstants.StringDefaultKey] = defaultKey;
                newRow1[ContentConstants.StringDefaultValue] = defaultValue;

                dataTable.Rows.Add(newRow1);
            }

            _contentRepository.UpdateProfileDefault(dataTable);

        }

        /// <summary>
        /// Updates ProfileDefaultCollection content in InsightsMetadata database with the content defined in Contentful
        /// </summary>
        /// <param name="defaultContentEntries"></param>
        /// <param name="keys"></param>
        private void UpdateDatabaseProfileDefaultCollectionFromContentful(JArray defaultContentEntries, IReadOnlyDictionary<string, string> keys)
        {
            var typeName = GetContentTypeName(ContentType.ProfileDefaultCollection);

            var cde = GetContentEntriesDefault(defaultContentEntries, typeName, StringAll);

            var dataTable = _contentRepository.GetDataTable(ContentDataTable.ClientProfileDefaultCollectionTable);

            foreach (var content in cde.Children<JObject>())
            {
                var key = content.SelectToken(GetTokenPath(ContentConstants.StringKey)).ToString();
                var nameKey = GetLinkedContentKey(keys, content, ContentConstants.StringNameKey);
                var description = content.SelectToken(GetTokenPath(ContentConstants.StringDescription)).ToString();


                var newRow1 = dataTable.NewRow();
                newRow1[ContentConstants.StringClientId] = DefaultClientId;
                newRow1[ContentConstants.StringProfileDefaultCollectionKey] = key;
                newRow1[ContentConstants.StringNameKey] = nameKey;
                newRow1[ContentConstants.StringDescription] = description;

                dataTable.Rows.Add(newRow1);
            }

            _contentRepository.UpdateProfileDefaultCollection(dataTable);

        }

        /// <summary>
        /// Updates ProfileOption content in InsightsMetadata database with the content defined in Contentful
        /// </summary>
        /// <param name="defaultContentEntries"></param>
        /// <param name="keys"></param>
        private void UpdateDatabaseProfileOptionFromContentful(JArray defaultContentEntries, IReadOnlyDictionary<string, string> keys)
        {
            var typeName = GetContentTypeName(ContentType.ProfileOption);

            var cde = GetContentEntriesDefault(defaultContentEntries, typeName, StringAll);

            var dataTable = _contentRepository.GetDataTable(ContentDataTable.ClientProfileOptionTable);

            foreach (var content in cde.Children<JObject>())
            {
                var key = content.SelectToken(GetTokenPath(ContentConstants.StringKey)).ToString();
                var profileOptionValue = content.SelectToken(GetTokenPath(ContentConstants.StringProfileOptionValue)).ToString();
                var nameKey = GetLinkedContentKey(keys, content, ContentConstants.StringNameKey);
                var description = content.SelectToken(GetTokenPath(ContentConstants.StringDescription)).ToString();


                var newRow1 = dataTable.NewRow();
                newRow1[ContentConstants.StringClientId] = DefaultClientId;
                newRow1[ContentConstants.StringProfileOptionKey] = key;
                newRow1[ContentConstants.StringProfileOptionValue] = profileOptionValue;
                newRow1[ContentConstants.StringNameKey] = nameKey;
                newRow1[ContentConstants.StringDescription] = description;

                dataTable.Rows.Add(newRow1);
            }

            _contentRepository.UpdateProfileOption(dataTable);

        }

        /// <summary>
        /// Updates ProfileSection content in InsightsMetadata database with the content defined in Contentful
        /// </summary>
        /// <param name="defaultContentEntries"></param>
        /// <param name="keys"></param>
        private void UpdateDatabaseProfileSectionFromContentful(JArray defaultContentEntries, IReadOnlyDictionary<string, string> keys)
        {
            var typeName = GetContentTypeName(ContentType.ProfileSection);

            var cde = GetContentEntriesDefault(defaultContentEntries, typeName, StringAll);

            var dataTable = _contentRepository.GetDataTable(ContentDataTable.ClientProfileSectionTable);

            foreach (var content in cde.Children<JObject>())
            {
                var key = content.SelectToken(GetTokenPath(ContentConstants.StringKey)).ToString();
                var name = content.SelectToken(GetTokenPath(ContentConstants.StringName)).ToString();
                var profileAttributes = GetLinkedContentKeys(keys, content, ContentConstants.StringProfileAttributes);
                var conditions = GetLinkedContentKeys(keys, content, ContentConstants.StringConditions);
                var images = GetLinkedContentKey(keys, content, ContentConstants.StringImages);
                var descriptionKey = GetLinkedContentKey(keys, content, ContentConstants.StringDescriptionKey);
                var titleKey = GetLinkedContentKey(keys, content, ContentConstants.StringTitleKey);
                var subTitleKey = GetLinkedContentKey(keys, content, ContentConstants.StringSubTitleKey);
                var rank = Convert.ToInt32(content.SelectToken(GetTokenPath(ContentConstants.StringRank)));
                // TFS 587 July 2015 intro text in database for profile section
                var introTextKey = GetLinkedContentKey(keys, content, ContentConstants.StringIntroText);


                var newRow1 = dataTable.NewRow();
                newRow1[ContentConstants.StringClientId] = DefaultClientId;
                newRow1[ContentConstants.StringProfileSectionKey] = key;
                newRow1[ContentConstants.StringName] = name;
                newRow1[ContentConstants.StringDescriptionKey] = descriptionKey;
                newRow1[ContentConstants.StringRank] = rank;
                newRow1[ContentConstants.StringTitleKey] = titleKey;
                newRow1[ContentConstants.StringSubTitleKey] = subTitleKey;
                newRow1[ContentConstants.StringProfileAttributes] = profileAttributes;
                newRow1[ContentConstants.StringConditions] = conditions;
                newRow1[ContentConstants.StringImages] = images;
                newRow1[ContentConstants.StringIntroText] = introTextKey;
                dataTable.Rows.Add(newRow1);
            }

            _contentRepository.UpdateProfileSection(dataTable);
        }


        /// <summary>
        /// Updates Tab content in InsightsMetadata database with the content defined in Contentful
        /// </summary>
        /// <param name="defaultContentEntries"></param>
        /// <param name="keys"></param>
        private void UpdateDatabaseTabFromContentful(JArray defaultContentEntries, IReadOnlyDictionary<string, string> keys)
        {
            var typeName = GetContentTypeName(ContentType.Tab);

            var cde = GetContentEntriesDefault(defaultContentEntries, typeName, StringAll);

            var dataTable = _contentRepository.GetDataTable(ContentDataTable.ClientTabTable);

            foreach (var content in cde.Children<JObject>())
            {
                var key = content.SelectToken(GetTokenPath(ContentConstants.StringKey)).ToString();
                var name = content.SelectToken(GetTokenPath(ContentConstants.StringName)).ToString();
                var menuOrder = int.Parse(content.SelectToken(GetTokenPath(ContentConstants.StringMenuOrder)).ToString());
                var layoutKey = GetLinkedContentKey(keys, content, ContentConstants.StringLayoutKey);
                var tabTypeKey = content.SelectToken(GetTokenPath(ContentConstants.StringType)).ToString();
                var nameKey = GetLinkedContentKey(keys, content, ContentConstants.StringNameKey);
                var url = content.SelectToken(GetTokenPath(ContentConstants.StringUrl)).ToString();
                var conditionKeys = GetLinkedContentKeys(keys, content, ContentConstants.StringConditionKeys);
                var configurationKeys = GetLinkedContentKeys(keys, content, ContentConstants.StringConfigurationKeys);
                var textContentKeys = GetLinkedContentKeys(keys, content, ContentConstants.StringTextContentKeys);
                var profileSectionKeys = GetLinkedContentKeys(keys, content, ContentConstants.StringProfileSectionKeys);
                var childTabKeys = GetLinkedContentKeys(keys, content, ContentConstants.StringChildTabKeys);
                var widgetKey = GetLinkedContentKey(keys, content, ContentConstants.StringWidgetKey);
                var tabIconType = GetLinkedContentKey(keys, content, ContentConstants.StringTabIconType);
                var tabIconClass = content.SelectToken(GetTokenPath(ContentConstants.StringTabIconClass));
                var tabImages = GetLinkedContentKey(keys, content, ContentConstants.StringImages);
                var actions = GetLinkedContentKeys(keys, content, ContentConstants.StringActions);

                var newRow1 = dataTable.NewRow();
                newRow1[ContentConstants.StringClientId] = DefaultClientId;
                newRow1[ContentConstants.StringTabKey] = key;
                newRow1[ContentConstants.StringName] = name;
                newRow1[ContentConstants.StringMenuOrder] = menuOrder;
                newRow1[ContentConstants.StringLayoutKey] = layoutKey;
                newRow1[ContentConstants.StringTabTypeKey] = tabTypeKey;
                newRow1[ContentConstants.StringNameKey] = nameKey;
                newRow1[ContentConstants.StringWidgetKey] = widgetKey;
                newRow1[ContentConstants.StringUrl] = url;
                newRow1[ContentConstants.StringConditionKeys] = conditionKeys;
                newRow1[ContentConstants.StringConfigurationKeys] = configurationKeys;
                newRow1[ContentConstants.StringTextContentKeys] = textContentKeys;
                newRow1[ContentConstants.StringProfileSectionKeys] = profileSectionKeys;
                newRow1[ContentConstants.StringChildTabKeys] = childTabKeys;
                newRow1[ContentConstants.StringTabIconType] = tabIconType;
                newRow1[ContentConstants.StringTabIconClass] = tabIconClass;
                newRow1[ContentConstants.StringImages] = tabImages;
                newRow1[ContentConstants.StringActions] = actions;
                newRow1[ContentConstants.StringDisable] = false; //defaulting false since we don't have the ability to disable this entry in Contentful.

                dataTable.Rows.Add(newRow1);
            }

            _contentRepository.UpdateTab(dataTable);
        }

        /// <summary>
        /// Updates TextContent in InsightsMetadata database with the content defined in Contentful
        /// </summary>
        /// <param name="defaultContentEntries"></param>
        private void UpdateDatabaseTextContentFromContentful(JArray defaultContentEntries)
        {
            var typeName = GetContentTypeName(ContentType.TextContent);

            var cde = GetContentEntriesDefault(defaultContentEntries, typeName, StringAll);

            var clientTable = _contentRepository.GetDataTable(ContentDataTable.ClientTextContentTable);

            foreach (var content in cde.Children<JObject>())
            {
                var key = content.SelectToken(GetTokenPath(ContentConstants.StringKey)).ToString();
                var categoryKey = content.SelectToken(GetTokenPath(ContentConstants.StringCategory)).ToString();
                var englishLongText = content.SelectToken(string.Format(TemplateTokenField, GetContentFieldName(ContentField.LongText), ContentConstants.StringLocaleEnglish)).ToStringValue();
                var spanishLongText = content.SelectToken(string.Format(TemplateTokenField, GetContentFieldName(ContentField.LongText), ContentConstants.StringLocaleSpanish)).ToStringValue();
                var russianLongText = content.SelectToken(string.Format(TemplateTokenField, GetContentFieldName(ContentField.LongText), ContentConstants.StringLocaleRussian)).ToStringValue();
                var rvsEnglish = GetRequireVariableSubstitutionValue(englishLongText);
                var rvsSpanish = GetRequireVariableSubstitutionValue(spanishLongText);
                var rvsRussian = GetRequireVariableSubstitutionValue(russianLongText);

                var newRow1 = clientTable.NewRow();
                newRow1[ContentConstants.StringClientId] = DefaultClientId;
                newRow1[ContentConstants.StringTextContentKey] = key;
                newRow1[ContentConstants.StringLocaleKey] = ContentConstants.StringLocaleEnglish;
                newRow1[ContentConstants.StringCategoryKey] = categoryKey;
                newRow1[ContentConstants.StringShortText] = content.SelectToken(string.Format(TemplateTokenField, GetContentFieldName(ContentField.ShortText), ContentConstants.StringLocaleEnglish)).ToStringValue();
                newRow1[ContentConstants.StringMediumText] = content.SelectToken(string.Format(TemplateTokenField, GetContentFieldName(ContentField.MediumText), ContentConstants.StringLocaleEnglish)).ToStringValue();
                newRow1[ContentConstants.StringLongText] = englishLongText;
                newRow1[ContentConstants.StringRequireVariableSubstitution] = rvsEnglish;
                clientTable.Rows.Add(newRow1);

                var newRow2 = clientTable.NewRow();
                newRow2[ContentConstants.StringClientId] = DefaultClientId;
                newRow2[ContentConstants.StringTextContentKey] = key;
                newRow2[ContentConstants.StringLocaleKey] = ContentConstants.StringLocaleSpanish;
                newRow2[ContentConstants.StringCategoryKey] = categoryKey;
                newRow2[ContentConstants.StringShortText] = content.SelectToken(string.Format(TemplateTokenField, GetContentFieldName(ContentField.ShortText), ContentConstants.StringLocaleSpanish)).ToStringValue();
                newRow2[ContentConstants.StringMediumText] = content.SelectToken(string.Format(TemplateTokenField, GetContentFieldName(ContentField.MediumText), ContentConstants.StringLocaleSpanish)).ToStringValue();
                newRow2[ContentConstants.StringLongText] = spanishLongText;
                newRow2[ContentConstants.StringRequireVariableSubstitution] = rvsSpanish;
                clientTable.Rows.Add(newRow2);

                var newRow3 = clientTable.NewRow();
                newRow3[ContentConstants.StringClientId] = DefaultClientId;
                newRow3[ContentConstants.StringTextContentKey] = key;
                newRow3[ContentConstants.StringLocaleKey] = ContentConstants.StringLocaleRussian;
                newRow3[ContentConstants.StringCategoryKey] = categoryKey;
                newRow3[ContentConstants.StringShortText] = content.SelectToken(string.Format(TemplateTokenField, GetContentFieldName(ContentField.ShortText), ContentConstants.StringLocaleRussian)).ToStringValue();
                newRow3[ContentConstants.StringMediumText] = content.SelectToken(string.Format(TemplateTokenField, GetContentFieldName(ContentField.MediumText), ContentConstants.StringLocaleRussian)).ToStringValue();
                newRow3[ContentConstants.StringLongText] = russianLongText;
                newRow3[ContentConstants.StringRequireVariableSubstitution] = rvsRussian;
                clientTable.Rows.Add(newRow3);
            }

            _contentRepository.UpdateTextContent(clientTable);

        }

        /// <summary>
        /// Updates Widget content in InsightsMetadata database with the content defined in Contentful
        /// </summary>
        /// <param name="defaultContentEntries"></param>
        /// <param name="keys"></param>
        private void UpdateDatabaseWidgetFromContentful(JArray defaultContentEntries, IReadOnlyDictionary<string, string> keys)
        {
            var typeName = GetContentTypeName(ContentType.Widget);

            var cde = GetContentEntriesDefault(defaultContentEntries, typeName, StringAll);

            var dataTable = _contentRepository.GetDataTable(ContentDataTable.ClientWidgetTable);

            foreach (var content in cde.Children<JObject>())
            {
                var key = content.SelectToken(GetTokenPath(ContentConstants.StringKey)).ToString();
                var name = content.SelectToken(GetTokenPath(ContentConstants.StringName)).ToString();
                var tabKey = GetLinkedContentKey(keys, content, ContentConstants.StringTabKey);
                var widgetTypeKey = content.SelectToken(GetTokenPath(ContentConstants.StringWidgetTypeKey)).ToString();
                var widgetColumn = int.Parse(content.SelectToken(GetTokenPath(ContentConstants.StringWidgetColumn)).ToString());
                var widgetRow = int.Parse(content.SelectToken(GetTokenPath(ContentConstants.StringWidgetRow)).ToString());
                var widgetOrder = int.Parse(content.SelectToken(GetTokenPath(ContentConstants.StringWidgetOrder)).ToString());
                var conditionKeys = GetLinkedContentKeys(keys, content, ContentConstants.StringConditionKeys);
                var configurationKeys = GetLinkedContentKeys(keys, content, ContentConstants.StringConfigurationKeys);
                var textContentKeys = GetLinkedContentKeys(keys, content, ContentConstants.StringTextContentKeys);
                // TFS 584 July 2015 intro text in database for widget
                var introTextKey = GetLinkedContentKey(keys, content, ContentConstants.StringIntroText);

                var newRow1 = dataTable.NewRow();
                newRow1[ContentConstants.StringClientId] = DefaultClientId;
                newRow1[ContentConstants.StringWidgetKey] = key;
                newRow1[ContentConstants.StringName] = name;
                newRow1[ContentConstants.StringTabKey] = tabKey;
                newRow1[ContentConstants.StringWidgetTypeKey] = widgetTypeKey;
                newRow1[ContentConstants.StringWidgetColumn] = widgetColumn;
                newRow1[ContentConstants.StringWidgetRow] = widgetRow;
                newRow1[ContentConstants.StringWidgetOrder] = widgetOrder;
                newRow1[ContentConstants.StringConditionKeys] = conditionKeys;
                newRow1[ContentConstants.StringConfigurationKeys] = configurationKeys;
                newRow1[ContentConstants.StringTextContentKeys] = textContentKeys;
                newRow1[ContentConstants.StringDisable] = false; //defaulting false since we don't have the ability to disable this entry in Contentful.
                newRow1[ContentConstants.StringIntroText] = introTextKey;

                dataTable.Rows.Add(newRow1);
            }

            _contentRepository.UpdateWidget(dataTable);
        }

        /// <summary>
        /// Updates Uom content in InsightsMetadata database with the content defined in Contentful
        /// </summary>
        /// <param name="defaultContentEntries"></param>
        /// <param name="keys"></param>
        private void UpdateDatabaseUomFromContentful(JArray defaultContentEntries, IReadOnlyDictionary<string, string> keys)
        {
            var typeName = GetContentTypeName(ContentType.Uom);

            var cde = GetContentEntriesDefault(defaultContentEntries, typeName, StringAll);

            var dataTable = _contentRepository.GetDataTable(ContentDataTable.ClientUomTable);

            foreach (var content in cde.Children<JObject>())
            {
                var key = content.SelectToken(GetTokenPath(ContentConstants.StringKey)).ToString();
                var nameKey = GetLinkedContentKey(keys, content, ContentConstants.StringNameKey);

                var newRow1 = dataTable.NewRow();
                newRow1[ContentConstants.StringClientId] = DefaultClientId;
                newRow1[ContentConstants.StringUomKey] = key;
                newRow1[ContentConstants.StringNameKey] = nameKey;
                dataTable.Rows.Add(newRow1);
            }

            _contentRepository.UpdateUom(dataTable);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="textContent"></param>
        /// <returns></returns>
        private static bool GetRequireVariableSubstitutionValue(string textContent)
        {
            var result = false;

            const string pattern = @"\[\%(.*?)\%\]"; //@"([%).+?(%])";
            var myRegex = new Regex(pattern, RegexOptions.IgnoreCase);

            var m = myRegex.Match(textContent);   // m is the first match
            if (m.Success)
            {
                result = true;
            }

            return result;
        }

        /// <summary>
        /// Retrieves content entry for a given content key and locale
        /// </summary>
        /// <param name="contentEntries"></param>
        /// <param name="typeName"></param>
        /// <param name="key"></param>
        /// <returns></returns>
        private static JObject GetContentEntryByTypeAndKey(JArray contentEntries, string typeName, string key)
        {

            var result = (from content in contentEntries.Children<JObject>()
                          let contentKey = (string)content.SelectToken(GetTokenPath(ContentConstants.StringKey))
                          let contentTypeName = (string)content.SelectToken(TokenContentType)
                          where (contentTypeName == typeName) && (contentKey == key)
                          select content).FirstOrDefault();

            # region DebugCode
            //foreach (JObject content in contentEntries.Children<JObject>())
            //{
            //    var contentKey = (string)content.SelectToken(GetTokenPath(ContentConstants.StringKey));
            //    var contentTypeName = (string)content.SelectToken(TokenContentType);

            //    if ((contentTypeName == typeName) && (contentKey == key))
            //    {
            //        result = content;
            //        break;
            //    }
            //}
            #endregion

            return result;
        }

        /// <summary>
        /// Retrieves default content entries by type and category
        /// </summary>
        /// <param name="contentEntries"></param>
        /// <param name="type"></param>
        /// <param name="category"></param>
        /// <returns></returns>
        private static JArray GetContentEntriesDefault(JArray contentEntries, string type, string category)
        {
            var result = new JArray
            {
                (from content in contentEntries.Children<JObject>()
                    let contentType = (string) content.SelectToken(TokenContentType)
                    let contentCategory = (string) content.SelectToken(GetTokenPath(ContentConstants.StringCategory))
                    where ((contentType == type) 
                    && ((category == StringAll) || ((contentCategory == category) || (contentCategory == StringCommon))))
                    select content).ToList()
            };

            #region DebugCode
            //var result = new JArray();
            //foreach (JObject content in contentEntries.Children<JObject>())
            //{
            //    var contentType = (string)content.SelectToken(TokenContentType);
            //    var contentCategory = (string)content.SelectToken(GetTokenPath(ContentConstants.StringCategory));

            //    if ((contentType == type) && ((contentCategory == category) || (contentCategory == StringCommon)))
            //    {
            //        result.Add(content);
            //    }
            //}
            #endregion

            return result;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="content"></param>
        /// <param name="fieldName"></param>
        /// <param name="locale"></param>
        /// <returns></returns>
        private static string GetFieldValueByLocale(JToken content, string fieldName, string locale)
        {
            return content.SelectToken(string.Format(TemplateTokenField, fieldName.ToLower(), locale)).ToStringValue();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="content"></param>
        /// <param name="fieldName"></param>
        /// <param name="locale"></param>
        /// <param name="childFieldName"></param>
        /// <returns></returns>
        private static string GetFieldValueByLocale(JToken content, string fieldName, string locale, string childFieldName)
        {
            return content.SelectToken(string.Format(TemplateTokenField, fieldName.ToLower(), locale + "." + childFieldName)).ToStringValue();
        }

        #region Private Methods - Contentful

        /// <summary>
        /// Returns Content Entries/Assets JArray either from cache or after retrieving from Contentful CMS
        /// </summary>
        /// <param name="space"></param>
        /// <param name="accessToken"></param>
        /// <param name="defaultUrl"></param>
        /// <param name="contentType"></param>
        /// <returns></returns>
        private static JArray GetContent(string space, string accessToken, string defaultUrl, string contentType)
        {
            const string templateUrl = "{0}/spaces/{1}/sync?initial=true&type={2}";
            var syncUrl = string.Format(templateUrl, defaultUrl, space, contentType == "Asset" ? "Asset" : "Entry");
            var result = GetContentFromContentful(syncUrl, accessToken);
            return result;
        }

        /// <summary>
        /// Get single content entry from contentful
        /// July 2015 TFS 287
        /// </summary>
        /// <param name="space"></param>
        /// <param name="accessToken"></param>
        /// <param name="defaultUrl"></param>
        /// <param name="key"></param>
        /// <returns></returns>
        private static JArray GetSingleContent(string space, string accessToken, string defaultUrl, string key)
        {
            const string templateUrl = "{0}/spaces/{1}/entries/{2}";
            var syncUrl = string.Format(templateUrl, defaultUrl, space, key);
            var result = GetSingleContentFromContentful(syncUrl, accessToken);
            return result;
        }

        /// <summary>
        /// get single content asset from contentful
        /// </summary>
        /// <param name="space"></param>
        /// <param name="accessToken"></param>
        /// <param name="defaultUrl"></param>
        /// <param name="key"></param>
        /// <returns></returns>
        private static JArray GetSingleAsset(string space, string accessToken, string defaultUrl, string key)
        {
            const string templateUrl = "{0}/spaces/{1}/assets/{2}";
            var syncUrl = string.Format(templateUrl, defaultUrl, space, key);
            var result = GetSingleContentFromContentful(syncUrl, accessToken);
            return result;
        }

        /// <summary>
        /// Returns Content Entries JArray either from cache or from database
        /// </summary>
        /// <param name="contentRepository"></param>
        /// <param name="cacheKey"></param>
        /// <returns></returns>
        private static JArray GetContentFromRepository(IContentRepository contentRepository, string cacheKey)
        {
            var contentCache = contentRepository.GetRawContent(cacheKey);
            var result = (JArray)JsonConvert.DeserializeObject(contentCache);
            return result;
        }

        /// <summary>
        /// Retrieves all content entries from Contentful CMS
        /// </summary>
        /// <param name="syncUrl"></param>
        /// <param name="accessToken"></param>
        /// <returns></returns>
        private static JArray GetContentFromContentful(string syncUrl, string accessToken)
        {
            const string templateUrlWithAccessToken = "{0}&access_token={1}";
            const string stringErrorMsg = "Content not authorized - Invalid access key '{0}'.";
            const string stringItems = "items";
            const string stringNextPageUrl = "nextPageUrl";
            const string stringSysType = "sys.type";
            const string stringError = "Error";

            var reqeustNextPage = true;
            var result = new JArray();
            //var pageNumber = 1;
            //var sb = new StringBuilder();

            do
            {
                syncUrl = string.Format(templateUrlWithAccessToken, syncUrl, accessToken);
                
                var jresponse = RetrieveContentFromContentful(syncUrl);
                //sb.AppendLine(syncUrl);
                //File.WriteAllText(string.Format(@"C:\Temp\Contentful\SyncEntries\ContentfulEntries_{0}.json", pageNumber), jresponse.ToString());
                syncUrl = (string)jresponse.SelectToken(stringNextPageUrl);
                if (string.IsNullOrEmpty(syncUrl))
                {
                    reqeustNextPage = false;
                }

                if ((jresponse.SelectToken(stringSysType).ToString() == stringError))
                {
                    throw new HttpException(401, string.Format(stringErrorMsg, accessToken));
                }

                var array = (JArray)jresponse.SelectToken(stringItems);
                result.Add(array.ToList());
                //pageNumber = pageNumber + 1;

            } while (reqeustNextPage);

            //File.WriteAllText(@"C:\Temp\Contentful\SyncEntries\ContentfulSyncUrls.txt", sb.ToString());

            return result;
        }

        /// <summary>
        /// Call to retrieve single content entry from Contentful CMS using management API
        /// July 2015 TFS 287
        /// </summary>
        /// <param name="syncUrl"></param>
        /// <param name="accessToken"></param>
        /// <returns></returns>
        private static JArray GetSingleContentFromContentful(string syncUrl, string accessToken)
        {
            const string templateUrlWithAccessToken = "{0}?access_token={1}";
            const string stringErrorMsg = "Content not authorized - Invalid access key '{0}'.";
            const string stringSysType = "sys.type";
            const string stringError = "Error";

            var result = new JArray();

            syncUrl = string.Format(templateUrlWithAccessToken, syncUrl, accessToken);

            var jresponse = RetrieveContentFromContentful(syncUrl);
            
            if ((jresponse.SelectToken(stringSysType).ToString() == stringError))
            {
                throw new HttpException(401, string.Format(stringErrorMsg, accessToken));
            }

            result.Add(jresponse);
            return result;
        }

        /// <summary>
        /// Retrieves content entries from Contentful
        /// </summary>
        /// <param name="contentUrl"></param>
        /// <returns></returns>
        private static JObject RetrieveContentFromContentful(string contentUrl)
        {
            //Previous TLS levels deprecated by Contentful since 30 April 2017
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
            var restClient = new RestClient(contentUrl);
            var request = new RestRequest(Method.GET);
            var response = restClient.Execute(request);
            var result = JObject.Parse(response.Content);

            return result;
        }

        #endregion

        #region Private Methods - Raw Content Operations

        /// <summary>
        /// Creates Content Cache Key
        /// </summary>
        /// <param name="space"></param>
        /// <param name="contentType"></param>
        /// <returns></returns>
        private static string GetRawContentKey(string space, string contentType)
        {
            const string templateCacheKey = "{0}_{1}";
            var result = string.Format(templateCacheKey, space, contentType);
            return result;
        }

        #endregion

        #region Private Methods - Get Enum Name

        private static string GetContentFieldName(ContentField fieldName)
        {
            const string errMessage = "ContentField can't be null.";
            var name = Enum.GetName(fieldName.GetType(), fieldName);
            if (name == null) throw new NullReferenceException(string.Format(errMessage));
            var result = name.ToLower();

            return result;
        }
        private static string GetContentTypeName(ContentType type)
        {
            const string errMessage = "ContentType can't be null.";
            var name = Enum.GetName(type.GetType(), type);
            if (name == null) throw new NullReferenceException(string.Format(errMessage));
            var result = name.ToLower();

            return result;
        }

        /// <summary>
        /// Gets locale string based on Enum passed
        /// </summary>
        /// <param name="locale"></param>
        /// <returns></returns>
        private static string GetContentLocaleName(ContentLocale locale)
        {
            string result;

            switch (locale)
            {
                case ContentLocale.EnglishUnitedStates:
                    result = ContentConstants.StringLocaleEnglish;
                    break;
                case ContentLocale.SpanishSpain:
                    result = ContentConstants.StringLocaleSpanish;
                    break;
                case ContentLocale.RussianRussia:
                    result = ContentConstants.StringLocaleRussian;
                    break;
                default:
                    result = ContentConstants.StringLocaleEnglish;
                    break;
            }

            return result;
        }

        #endregion

        #endregion

    }
}
