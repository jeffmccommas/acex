﻿
using System.Collections.Generic;
using CE.ContentModel.Entities;

namespace CE.ContentModel
{
    interface IContentRepositoryCache
    {
        /// <summary>
        /// Gets ClientContent from Cache
        /// </summary>
        /// <returns></returns>
        ClientContent GetClientContent(string locale, string profileDefaultCollectionName);

        List<ClientAction> GetClientAction();

        List<ClientActionSaving> GetClientActionSaving();

        List<ClientAppliance> GetClientAppliance();

        List<ClientBenchmarkGroup> GetClientBenchmarkGroup();

        List<ClientCommodity> GetClientCommodity();

        List<ClientCondition> GetClientCondition();

        List<ClientConfigurationBulk> GetClientConfigurationBulk();

        List<ClientConfiguration> GetClientConfiguration();

        List<ClientCurrency> GetClientCurrency();

        List<ClientEndUse> GetClientEndUse();

        List<ClientEnumeration> GetClientEnumeration();

        List<ClientExpression> GetClientExpression();

        List<ClientFileContent> GetClientFileContent();

        List<ClientLayout> GetClientLayout();

        List<ClientMeasurement> GetClientMeasurement();

        List<ClientProfileAttribute> GetClientProfileAttribute();

        List<ClientProfileDefault> GetClientProfileDefault(string profileDefaultCollectionName);

        List<ClientProfileDefaultCollection> GetClientProfileDefaultCollection();

        List<ClientProfileOption> GetClientProfileOption();

        List<ClientProfileSection> GetClientProfileSection();

        List<ClientSeason> GetClientSeason();

        List<ClientTab> GetClientTab();

        List<ClientTextContent> GetClientTextContent(string locale);

        List<ClientUOM> GetClientUom();

        List<ClientWhatIfData> GetClientWhatIfData();

        List<ClientWidget> GetClientWidget();

        List<PortalModuleSystemConfigurationBulk> GetSystemConfigurationBulk();

        /// <summary>
        /// Removes client content cache
        /// </summary>
        void RemoveClientContentCache(string profileDefaultCollectionName);

    }
}
