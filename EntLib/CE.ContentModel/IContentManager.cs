﻿
using System.Collections.Generic;

namespace CE.ContentModel
{
    public interface IContentManager
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="url"></param>
        /// <param name="requestBody"></param>
        /// <returns></returns>
        string CreateContentType(string url, string requestBody);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="type"></param>
        /// <param name="csvFilePath"></param>
        string CreateContentEntriesUsingCsv(ContentType type, string csvFilePath);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="csvFilePath"></param>
        void PublishContentEntriesUsingCsv(string csvFilePath);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="csvFilePath"></param>
        void UnPublishContentEntriesUsingCsv(string csvFilePath);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="csvFilePath"></param>
        void DeleteContentEntriesUsingCsv(string csvFilePath);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="clientId"></param>
        /// <param name="type"></param>
        /// <param name="csvFilePath"></param>
        void UpdateDatabaseClientContentUsingCsv(int clientId, ContentType type, string csvFilePath);

        /// <summary>
        /// Creates new asset in Contentful
        /// </summary>
        /// <param name="url"></param>
        /// <param name="requestBody"></param>
        /// <param name="version"></param>
        /// <returns></returns>
        string CreateContentAsset(string url, string requestBody, int version);

        /// <summary>
        /// process new asset in Contentful
        /// </summary>
        /// <param name="url"></param>
        /// <returns></returns>
        string ProcessContentAsset(string url);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="url"></param>
        /// <param name="requestBody"></param>
        /// <param name="contentType"></param>
        /// <param name="version"></param>
        /// <returns></returns>
        string CreateContentEntry(string url, string requestBody, string contentType, int version);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="url"></param>
        /// <param name="version"></param>
        /// <returns></returns>
        string PublishContentEntry(string url, int version);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="url"></param>
        /// <returns></returns>
        string UnPublishContentEntry(string url);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="url"></param>
        /// <returns></returns>
        string DeleteContentEntry(string url);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="outputCsvFilePath"></param>
        void GetContentOrphan(string outputCsvFilePath);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="csvFilePath"></param>
        void DeleteContentOrphan(string csvFilePath);

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        Dictionary<string, string> GetContentfulSysIdAndKeyDictionary();
    }
}
