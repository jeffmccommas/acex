﻿using System;
using System.Runtime.Caching;

namespace CE.ContentModel.ContentCache
{

    /// <summary>
    /// Content cache provider - implemented via memory cache.
    /// </summary>
    public class ContentCacheProviderMemoryCache : IContentCacheProvider
    {

        /// <summary>
        /// Store value in cache.
        /// </summary>
        /// <param name="key"></param>
        /// <param name="value"></param>
        /// <param name="absoluteExpiration"></param>
        /// <param name="regionName"></param>
        public void Set(string key, object value, DateTimeOffset absoluteExpiration, string regionName = null)
        {
            MemoryCache.Default.Set(key, value, absoluteExpiration, regionName);
        }

        /// <summary>
        /// Retrieve value from cache.
        /// </summary>
        /// <param name="key"></param>
        /// <param name="regionName"></param>
        /// <returns></returns>
        public object Get(string key, string regionName = null)
        {
            var result = MemoryCache.Default.Get(key, regionName);
            return result;
        }

        /// <summary>
        /// Remove value from cache.
        /// </summary>
        /// <param name="key"></param>
        /// <param name="regionName"></param>
        /// <returns></returns>
        public object Remove(string key, string regionName = null)
        {
            var result = MemoryCache.Default.Remove(key, regionName);
            return result;
        }
    }
}
