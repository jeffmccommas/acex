﻿using System.Configuration;

namespace CE.ContentModel.ContentCache
{

    /// <summary>
    /// Content cache provider factory.
    /// </summary>
    public static class ContentCacheProviderFactory
    {

        #region Private Constants

        private const string CacheProviderNameMemoryCache = "MemoryCache";
        private const string CacheProviderNameRedisAzureCache = "RedisAzureCache";
        private const string AppSettingContentCacheProvider = "ContentCacheProvider";

        #endregion

        #region Public Static Methods

        /// <summary>
        /// Create content cache provider.
        /// </summary>
        /// <returns></returns>
        public static IContentCacheProvider CreateContentCacheProvider()
        {
            IContentCacheProvider result;

            var cacheProviderName = ConfigurationManager.AppSettings.Get(AppSettingContentCacheProvider);
            if (string.IsNullOrEmpty(cacheProviderName))
            {
                //Default cache provider.
                cacheProviderName = CacheProviderNameMemoryCache;
            }

            switch (cacheProviderName)
            {
                case CacheProviderNameRedisAzureCache:

                    if (ContentCacheProviderRedisAzure.IsAzureRedisCacheAvailable())
                    {
                        result = new ContentCacheProviderRedisAzure();
                    }
                    else
                    {
                        result = new ContentCacheProviderMemoryCache();
                    }
                    break;

                case CacheProviderNameMemoryCache:

                    result = new ContentCacheProviderMemoryCache();
                    break;

                default:

                    result = new ContentCacheProviderMemoryCache();
                    break;
            }

            return result;
        }

        #endregion

    }
}
