﻿using System.Configuration;

namespace CE.ContentModel.ContentCache
{
    /// <summary>
    /// Azure Redis cache configuration section (web.config/app.config).
    /// </summary>
    public sealed class AzureRedisCacheSection : ConfigurationSection
    {
        /// <summary>
        /// Configuration options.
        /// </summary>
        [ConfigurationProperty("azureRedisCacheConfigurationOptions", IsKey = true, IsRequired = false)]
        public AzureRedisCacheConfigurationOptionsElement ConfigurationOptions
        {
            get
            {
                return (AzureRedisCacheConfigurationOptionsElement)this["azureRedisCacheConfigurationOptions"];
            }
            set
            {
                this["azureRedisCacheConfigurationOptions"] = value;
            }
        }

        /// <summary>
        /// Configuration element - Azure Redis configuration options.
        /// </summary>
        public class AzureRedisCacheConfigurationOptionsElement : ConfigurationElement
        {

            [ConfigurationProperty("EndPoint", IsKey = true, IsRequired = false)]
            public string EndPoint
            {
                get
                {
                    return (string)base["EndPoint"];
                }
                set
                {
                    base["EndPoint"] = value;
                }
            }
            [ConfigurationProperty("Password", IsKey = true, IsRequired = false)]
            public string Password
            {
                get
                {
                    return (string)base["Password"];
                }
                set
                {
                    base["Password"] = value;
                }
            }
            [ConfigurationProperty("Ssl", IsKey = true, IsRequired = false)]
            public string Ssl
            {
                get
                {
                    return (string)base["Ssl"];
                }
                set
                {
                    base["Ssl"] = value;
                }
            }
            [ConfigurationProperty("ConnectRetry", IsKey = true, IsRequired = false)]
            public string ConnectRetry
            {
                get
                {
                    return (string)base["ConnectRetry"];
                }
                set
                {
                    base["ConnectRetry"] = value;
                }
            }

            [ConfigurationProperty("ConnectTimeout", IsKey = true, IsRequired = false)]
            public string ConnectTimeout
            {
                get
                {
                    return (string)base["ConnectTimeout"];
                }
                set
                {
                    base["ConnectTimeout"] = value;
                }
            }
            [ConfigurationProperty("SyncTimeout", IsKey = true, IsRequired = false)]
            public string SyncTimeout
            {
                get
                {
                    return (string)base["SyncTimeout"];
                }
                set
                {
                    base["SyncTimeout"] = value;
                }
            }
        }

    }

}
