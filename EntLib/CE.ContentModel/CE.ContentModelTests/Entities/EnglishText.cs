﻿using System.Runtime.Serialization;

namespace CE.ContentModelTests.Entities
{
    [DataContract]
    public class EnglishText
    {
        [DataMember(Order = 0, Name = "en-US")]
        public string EnUs { get; set; }
    }
}
