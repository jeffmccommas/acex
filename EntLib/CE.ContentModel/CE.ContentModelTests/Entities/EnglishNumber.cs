﻿using System.Runtime.Serialization;

namespace CE.ContentModelTests.Entities
{
    [DataContract]
    public class EnglishNumber
    {
        [DataMember(Order = 0, Name = "en-US")]
        public int EnUs { get; set; }
    }
}
