﻿using System.Runtime.Serialization;
namespace CE.ContentModelTests.Entities
{
    [DataContract]
    public class ContentfulTextContent
    {
        [DataMember(Order = 0, Name = "fields")]
        public TextContentFields Fields { get; set; }
    }

    [DataContract]
    public class TextContentFields
    {
        [DataMember(Order = 0, Name = "key")]
        public EnglishText TextContentKey { get; set; }
        [DataMember(Order = 1, Name = "shorttext")]
        public TextContentText ShortText { get; set; }
        [DataMember(Order = 2, Name = "mediumtext")]
        public TextContentText MediumText { get; set; }
        [DataMember(Order = 3, Name = "longtext")]
        public TextContentText LongText { get; set; }
        [DataMember(Order = 4, Name = "category")]
        public EnglishText Category { get; set; }
    }

    [DataContract]
    public class TextContentText
    {
        [DataMember(Order = 0, Name = "en-US")]
        public string EnUs { get; set; }
        [DataMember(Order = 1, Name = "es-ES")]
        public string EsEs { get; set; }
        [DataMember(Order = 2, Name = "ru-RU")]
        public string RuRu { get; set; }
    }
}
