﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace CE.ContentModelTests.Entities
{

    [DataContract]
    public class ContentfulAppliance
    {
        [DataMember(Order = 0, Name = "fields")]
        public ApplianceFields Fields { get; set; }
    }

    [DataContract]
    public class ApplianceFields
    {
        [DataMember(Order = 0, Name = "key")]
        public EnglishText ApplianceKey { get; set; }

        [DataMember(Order = 1, Name = "name")]
        public TextContentText Name { get; set; }

        [DataMember(Order = 2, Name = "averagecostperyear")]
        public EnglishNumber AverageCostPerYear { get; set; }

        [DataMember(Order = 3, Name = "profileattributes")]
        public EnglishEntryLinks ProfileAttributes { get; set; }

    }
}
