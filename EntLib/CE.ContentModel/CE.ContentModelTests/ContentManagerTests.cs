﻿using CE.ContentModel;
using NUnit.Framework;
using System;
using System.Diagnostics;
using System.IO;

namespace CE.ContentModelTests
{
    [TestFixture]
    public class ContentManagerTests
    {
        #region Constants

        private const string DefaultSpace = "wp32f8j5jqi2";
        private const string TemplateElapsedTime = "Elapsed={0}\r\n";
        private const int DefaultClientId = 0;

        #endregion

        #region Contentful Methods

        #region Create Contentful Types

        [Test]
        //[Ignore("Ignore unit tests related to Content Management API")]
        public void CreateContentTypeTest()
        {
            const string jasonBody = "{\"name\":\"_Enter name here\",\"description\":\"Enter Description here.\",\"fields\":[{\"id\":\"key\",\"name\":\"Key\",\"type\":\"Symbol\"},{\"id\":\"name\",\"name\":\"Name\",\"type\":\"Symbol\"}]}";
            const string templateUrl = "https://api.contentful.com/spaces/{0}/content_types/{1}/?access_token={2}";
            const string accessToken = "cf5bdb8ff14a16d8a405a99607b708db02c4c575e2d37450dd74a1164a49ba47";
            const string id = "whatifdata";

            //var url = string.Format(templateUrl, ClientSpace, id, accessToken);
            var url = string.Format(templateUrl, DefaultSpace, id, accessToken);

            var sw = new Stopwatch();

            sw.Start();
            var factory = new ContentModelFactoryContentful();
            var manager = factory.CreateContentManager(DefaultClientId);
            var result = manager.CreateContentType(url, jasonBody);
            sw.Stop();
            Console.Out.WriteLine(TemplateElapsedTime, sw.Elapsed);
            Console.Out.WriteLine(result);

            Assert.NotNull(result);

        }

        #endregion

        #region Create Contentful Entries

        #region Working Methods

        [Test]
        public void InsertActionEntriesIntoContentfulFromCsv()
        {
            try
            {
                var factory = new ContentModelFactoryContentful();
                var manager = factory.CreateContentManager(DefaultClientId);
                Console.Out.WriteLine(manager.CreateContentEntriesUsingCsv(ContentType.Action, @"C:\Temp\Contentful\SQL\ClientAction4.csv"));
            }
            catch (Exception ex)
            {
                Console.Out.WriteLine(ex.Message);
                throw;
            }

        }

        [Test]
        public void InsertApplianceEntriesIntoContentfulFromCsv()
        {
            try
            {
                var factory = new ContentModelFactoryContentful();
                var manager = factory.CreateContentManager(DefaultClientId);
                Console.Out.WriteLine(manager.CreateContentEntriesUsingCsv(ContentType.Appliance, @"C:\Temp\Contentful\SQL\ClientAppliance4.csv"));
            }
            catch (Exception ex)
            {
                Console.Out.WriteLine(ex.Message);
                throw;
            }

        }

        [Test]
        public void InsertAssetFromCsv()
        {
            try
            {
                var factory = new ContentModelFactoryContentful();
                var manager = factory.CreateContentManager(DefaultClientId);
                Console.Out.WriteLine(manager.CreateContentEntriesUsingCsv(ContentType.Asset, @"C:\Temp\Contentful\SQL\ClientAsset4.csv"));
            }
            catch (Exception ex)
            {
                Console.Out.WriteLine(ex.Message);
                throw;
            }
        }

        [Test]
        public void InsertBenchmarkGroupEntriesIntoContentfulFromCsv()
        {
            try
            {
                var factory = new ContentModelFactoryContentful();
                var manager = factory.CreateContentManager(DefaultClientId);
                Console.Out.WriteLine(manager.CreateContentEntriesUsingCsv(ContentType.BenchmarkGroup, @"C:\Temp\Contentful\SQL\ClientBenchmarkGroup4.csv"));
            }
            catch (Exception ex)
            {
                Console.Out.WriteLine(ex.Message);
                throw;
            }

        }

        [Test]
        public void InsertCommodityEntriesIntoContentfulFromCsv()
        {
            try
            {
                var factory = new ContentModelFactoryContentful();
                var manager = factory.CreateContentManager(DefaultClientId);
                Console.Out.WriteLine(manager.CreateContentEntriesUsingCsv(ContentType.Commodity, @"C:\Temp\Contentful\SQL\ClientCommodity4.csv"));
            }
            catch (Exception ex)
            {
                Console.Out.WriteLine(ex.Message);
                throw;
            }
        }

        [Test]
        public void InsertConditionEntriesIntoContentfulFromCsv()
        {
            try
            {
                var factory = new ContentModelFactoryContentful();
                var manager = factory.CreateContentManager(DefaultClientId);
                Console.Out.WriteLine(manager.CreateContentEntriesUsingCsv(ContentType.Condition, @"C:\Temp\Contentful\SQL\ClientCondition4.csv"));
            }
            catch (Exception ex)
            {
                Console.Out.WriteLine(ex.Message);
                throw;
            }
        }

        [Test]
        public void InsertConfigurationIntoContentfulFromCsv()
        {
            try
            {
                var factory = new ContentModelFactoryContentful();
                var manager = factory.CreateContentManager(DefaultClientId);
                Console.Out.WriteLine(manager.CreateContentEntriesUsingCsv(ContentType.Configuration, @"C:\Temp\Contentful\SQL\ClientConfiguration4.csv"));
            }
            catch (Exception ex)
            {
                Console.Out.WriteLine(ex.Message);
                throw;
            }
        }

        [Test]
        public void InsertConfigurationBulkIntoContentfulFromCsv()
        {
            try
            {
                var factory = new ContentModelFactoryContentful();
                var manager = factory.CreateContentManager(DefaultClientId);
                Console.Out.WriteLine(manager.CreateContentEntriesUsingCsv(ContentType.ConfigurationBulk, @"C:\Temp\Contentful\SQL\ClientConfigurationBulk4.csv"));
            }
            catch (Exception ex)
            {
                Console.Out.WriteLine(ex.Message);
                throw;
            }
        }

        [Test]
        public void InsertCurrencyEntriesIntoContentfulFromCsv()
        {
            try
            {
                var factory = new ContentModelFactoryContentful();
                var manager = factory.CreateContentManager(DefaultClientId);
                Console.Out.WriteLine(manager.CreateContentEntriesUsingCsv(ContentType.Currency, @"C:\Temp\Contentful\SQL\ClientCurrency4.csv"));
            }
            catch (Exception ex)
            {
                Console.Out.WriteLine(ex.Message);
                throw;
            }
        }

        [Test]
        public void InsertEndUseEntriesIntoContentfulFromCsv()
        {
            try
            {
                var factory = new ContentModelFactoryContentful();
                var manager = factory.CreateContentManager(DefaultClientId);
                Console.Out.WriteLine(manager.CreateContentEntriesUsingCsv(ContentType.Enduse, @"C:\Temp\Contentful\SQL\ClientEnduse4.csv"));
            }
            catch (Exception ex)
            {
                Console.Out.WriteLine(ex.Message);
                throw;
            }
        }

        [Test]
        public void InsertEnumerationItemEntriesIntoContentfulFromCsv()
        {
            try
            {
                var factory = new ContentModelFactoryContentful();
                var manager = factory.CreateContentManager(DefaultClientId);
                Console.Out.WriteLine(manager.CreateContentEntriesUsingCsv(ContentType.EnumerationItem, @"C:\Temp\Contentful\SQL\ClientEnumItem.csv"));
            }
            catch (Exception ex)
            {
                Console.Out.WriteLine(ex.Message);
                throw;
            }
        }

        [Test]
        public void InsertEnumerationEntriesIntoContentfulFromCsv()
        {
            try
            {
                var factory = new ContentModelFactoryContentful();
                var manager = factory.CreateContentManager(DefaultClientId);
                Console.Out.WriteLine(manager.CreateContentEntriesUsingCsv(ContentType.Enumeration, @"C:\Temp\Contentful\SQL\ClientEnum4.csv"));
            }
            catch (Exception ex)
            {
                Console.Out.WriteLine(ex.Message);
                throw;
            }
        }


        [Test]
        public void InsertExpressionIntoContentfulFromCsv()
        {
            try
            {
                var factory = new ContentModelFactoryContentful();
                var manager = factory.CreateContentManager(DefaultClientId);
                Console.Out.WriteLine(manager.CreateContentEntriesUsingCsv(ContentType.Expression, @"C:\Temp\Contentful\SQL\ClientExpression4.csv"));
            }
            catch (Exception ex)
            {
                Console.Out.WriteLine(ex.Message);
                throw;
            }
        }

        [Test]
        public void InsertFileContentIntoContentfulFromCsv()
        {
            try
            {
                var factory = new ContentModelFactoryContentful();
                var manager = factory.CreateContentManager(DefaultClientId);
                Console.Out.WriteLine(manager.CreateContentEntriesUsingCsv(ContentType.FileContent, @"C:\Temp\Contentful\SQL\ClientFileContent4.csv"));
            }
            catch (Exception ex)
            {
                Console.Out.WriteLine(ex.Message);
                throw;
            }
        }

        [Test]
        public void InsertLayoutEntriesIntoContentfulFromCsv()
        {
            try
            {
                var factory = new ContentModelFactoryContentful();
                var manager = factory.CreateContentManager(DefaultClientId);
                Console.Out.WriteLine(manager.CreateContentEntriesUsingCsv(ContentType.Layout, @"C:\Temp\Contentful\SQL\ClientLayout4.csv"));
            }
            catch (Exception ex)
            {
                Console.Out.WriteLine(ex.Message);
                throw;
            }
        }

        [Test]
        public void InsertMeasurementEntriesIntoContentfulFromCsv()
        {
            try
            {
                var factory = new ContentModelFactoryContentful();
                var manager = factory.CreateContentManager(DefaultClientId);
                Console.Out.WriteLine(manager.CreateContentEntriesUsingCsv(ContentType.Measurement, @"C:\Temp\Contentful\SQL\ClientMeasurement4.csv"));
            }
            catch (Exception ex)
            {
                Console.Out.WriteLine(ex.Message);
                throw;
            }
        }

        [Test]
        public void InsertProfileDefaultIntoContentfulFromCsv()
        {
            try
            {
                var factory = new ContentModelFactoryContentful();
                var manager = factory.CreateContentManager(DefaultClientId);
                Console.Out.WriteLine(manager.CreateContentEntriesUsingCsv(ContentType.ProfileDefault, @"C:\Temp\Contentful\SQL\ClientProfileDefault4.csv"));
            }
            catch (Exception ex)
            {
                Console.Out.WriteLine(ex.Message);
                throw;
            }
        }

        [Test]
        public void InsertProfileDefaultCollectionIntoContentfulFromCsv()
        {
            try
            {
                var factory = new ContentModelFactoryContentful();
                var manager = factory.CreateContentManager(DefaultClientId);
                Console.Out.WriteLine(manager.CreateContentEntriesUsingCsv(ContentType.ProfileDefaultCollection, @"C:\Temp\Contentful\SQL\ProfileDefaultCollection4.csv"));
            }
            catch (Exception ex)
            {
                Console.Out.WriteLine(ex.Message);
                throw;
            }
        }

        [Test]
        public void InsertProfileOptionIntoContentfulFromCsv()
        {
            try
            {
                var factory = new ContentModelFactoryContentful();
                var manager = factory.CreateContentManager(DefaultClientId);
                Console.Out.WriteLine(manager.CreateContentEntriesUsingCsv(ContentType.ProfileOption, @"C:\Temp\Contentful\SQL\ClientProfileOption4.csv"));
            }
            catch (Exception ex)
            {
                Console.Out.WriteLine(ex.Message);
                throw;
            }
        }

        [Test]
        public void InsertProfileAttributeIntoContentfulFromCsv()
        {
            try
            {
                var factory = new ContentModelFactoryContentful();
                var manager = factory.CreateContentManager(DefaultClientId);
                Console.Out.WriteLine(manager.CreateContentEntriesUsingCsv(ContentType.ProfileAttribute, @"C:\Temp\Contentful\SQL\ProfileAttributes_WeatherSensitivities.csv"));
            }
            catch (Exception ex)
            {
                Console.Out.WriteLine(ex.Message);
                throw;
            }
        }

        [Test]
        public void InsertProfileSectionEntriesIntoContentfulFromCsv()
        {
            try
            {
                var factory = new ContentModelFactoryContentful();
                var manager = factory.CreateContentManager(DefaultClientId);
                Console.Out.WriteLine(manager.CreateContentEntriesUsingCsv(ContentType.ProfileSection, @"C:\Temp\Contentful\SQL\ProfileSection4.csv"));
            }
            catch (Exception ex)
            {
                Console.Out.WriteLine(ex.Message);
                throw;
            }
        }

        [Test]
        public void InsertSeasonEntriesIntoContentfulFromCsv()
        {
            try
            {
                var factory = new ContentModelFactoryContentful();
                var manager = factory.CreateContentManager(DefaultClientId);
                Console.Out.WriteLine(manager.CreateContentEntriesUsingCsv(ContentType.Season, @"C:\Temp\Contentful\SQL\ClientSeason4.csv"));
            }
            catch (Exception ex)
            {
                Console.Out.WriteLine(ex.Message);
                throw;
            }
        }

        [Test]
        public void InsertTabEntriesIntoContentfulFromCsv()
        {
            try
            {
                var factory = new ContentModelFactoryContentful();
                var manager = factory.CreateContentManager(DefaultClientId);
                Console.Out.WriteLine(manager.CreateContentEntriesUsingCsv(ContentType.Tab, @"C:\Temp\Contentful\SQL\ClientTab4.csv"));
            }
            catch (Exception ex)
            {
                Console.Out.WriteLine(ex.Message);
                throw;
            }
        }

        [Test]
        public void InsertTextContentIntoContentfulFromCsv()
        {
            try
            {
                var factory = new ContentModelFactoryContentful();
                var manager = factory.CreateContentManager(DefaultClientId);
                Console.Out.WriteLine(manager.CreateContentEntriesUsingCsv(ContentType.TextContent, @"C:\Temp\Contentful\SQL\ClientTextContent4.csv"));
            }
            catch (Exception ex)
            {
                Console.Out.WriteLine(ex.Message);
                throw;
            }
        }

        [Test]
        public void InsertUomEntriesIntoContentfulFromCsv()
        {
            try
            {
                var factory = new ContentModelFactoryContentful();
                var manager = factory.CreateContentManager(DefaultClientId);
                Console.Out.WriteLine(manager.CreateContentEntriesUsingCsv(ContentType.Uom, @"C:\Temp\Contentful\SQL\ClientUom4.csv"));
            }
            catch (Exception ex)
            {
                Console.Out.WriteLine(ex.Message);
                throw;
            }

        }

        [Test]
        public void InsertWhatIfDataEntriesIntoContentfulFromCsv()
        {
            try
            {
                var factory = new ContentModelFactoryContentful();
                var manager = factory.CreateContentManager(DefaultClientId);
                Console.Out.WriteLine(manager.CreateContentEntriesUsingCsv(ContentType.Season, @"C:\Temp\Contentful\SQL\ClientWhatIfData4.csv"));
            }
            catch (Exception ex)
            {
                Console.Out.WriteLine(ex.Message);
                throw;
            }
        }

        [Test]
        public void InsertWidgetEntriesIntoContentfulFromCsv()
        {
            try
            {
                var factory = new ContentModelFactoryContentful();
                var manager = factory.CreateContentManager(DefaultClientId);
                Console.Out.WriteLine(manager.CreateContentEntriesUsingCsv(ContentType.Widget, @"C:\Temp\Contentful\SQL\ClientWidget4.csv"));
            }
            catch (Exception ex)
            {
                Console.Out.WriteLine(ex.Message);
                throw;
            }
        }




        #endregion

        #endregion

        #region Publish Contentful Entries

        #region Working Methods

        [Test]
        public void PublishEntriesInContentful()
        {
            var factory = new ContentModelFactoryContentful();
            var manager = factory.CreateContentManager(DefaultClientId);
            manager.PublishContentEntriesUsingCsv(@"C:\Temp\Contentful\SQL\ClientWidget4.csv");
        }

        #endregion

        #endregion

        #region UnPublish Contentful Entries

        #region Working Methods

        [Test]
        public void UnPublishEntriesInContentful()
        {
            var factory = new ContentModelFactoryContentful();
            var manager = factory.CreateContentManager(DefaultClientId);
            manager.UnPublishContentEntriesUsingCsv(@"C:\Temp\Contentful\SQL\ClientWidget4.csv");
        }

        #endregion

        #endregion

        #region Delete Contentful Entries

        #region Working Methods

        [Test]
        public void DeleteEntriesInContentful()
        {
            var factory = new ContentModelFactoryContentful();
            var manager = factory.CreateContentManager(DefaultClientId);
            manager.DeleteContentEntriesUsingCsv(@"C:\Temp\Contentful\SQL\ClientSeason4.csv");
        }

        #endregion

        #endregion

        #endregion

        #region Database Methods

        #region Client Content Override Methods

        [Test]
        public void MergeClientContentInDatabaseFromCsv()
        {
            const string inputCsvFilePath = @"C:\Temp\SQL\configuration.csv";
            const int clientId = 61;
            const ContentType contentType = ContentType.Configuration;

            if (!Utility.IsClientIdValidForDatabaseUpdate(clientId)) return;

            if (File.Exists(inputCsvFilePath))
            {
                var factory = new ContentModelFactoryContentful();
                var manager = factory.CreateContentManager(clientId);

                manager.UpdateDatabaseClientContentUsingCsv(clientId, contentType, inputCsvFilePath);

                Console.Out.WriteLine("Successfully updated the database for clientId {0} and CSV file {1}.", clientId, inputCsvFilePath);
            }
            else
            {
                Console.Out.WriteLine("Error: File {0} not found.", inputCsvFilePath);
            }
        }

        [Test]
        public void MergeClientContentConfigInDatabaseFromCsv()
        {
            const string inputCsvFilePath = @"C:\Temp\Contentful\SQL\configcontentfooter.csv";
            const int clientId = 256;
            const ContentType contentType = ContentType.Configuration;

            if (!Utility.IsClientIdValidForDatabaseUpdate(clientId)) return;

            if (File.Exists(inputCsvFilePath))
            {
                var factory = new ContentModelFactoryContentful();
                var manager = factory.CreateContentManager(clientId);

                manager.UpdateDatabaseClientContentUsingCsv(clientId, contentType, inputCsvFilePath);

                Console.Out.WriteLine("Successfully updated the database for clientId {0} and CSV file {1}.", clientId, inputCsvFilePath);
            }
            else
            {
                Console.Out.WriteLine("Error: File {0} not found.", inputCsvFilePath);
            }
        }

        [Test]
        public void MergeClientContentConfigBulkInDatabaseFromCsv()
        {
            const string inputCsvFilePath = @"C:\Temp\Contentful\SQL\ConfigurationBulk-87.csv";
            const int clientId = 87;
            const ContentType contentType = ContentType.ConfigurationBulk;

            if (!Utility.IsClientIdValidForDatabaseUpdate(clientId)) return;

            if (File.Exists(inputCsvFilePath))
            {
                var factory = new ContentModelFactoryContentful();
                var manager = factory.CreateContentManager(clientId);

                manager.UpdateDatabaseClientContentUsingCsv(clientId, contentType, inputCsvFilePath);

                Console.Out.WriteLine("Successfully updated the database for clientId {0} and CSV file {1}.", clientId, inputCsvFilePath);
            }
            else
            {
                Console.Out.WriteLine("Error: File {0} not found.", inputCsvFilePath);
            }
        }

        [Test]
        public void MergeClientContentInDatabaseFromCsvTab()
        {
            const string inputCsvFilePath = @"C:\Temp\Contentful\SQL\tabstest.csv";
            const int clientId = 256;
            const ContentType contentType = ContentType.Tab;

            if (!Utility.IsClientIdValidForDatabaseUpdate(clientId)) return;

            if (File.Exists(inputCsvFilePath))
            {
                var factory = new ContentModelFactoryContentful();
                var manager = factory.CreateContentManager(clientId);

                manager.UpdateDatabaseClientContentUsingCsv(clientId, contentType, inputCsvFilePath);

                Console.Out.WriteLine("Successfully updated the database for clientId {0} and CSV file {1}.", clientId, inputCsvFilePath);
            }
            else
            {
                Console.Out.WriteLine("Error: File {0} not found.", inputCsvFilePath);
            }
        }


        [Test]
        public void MergeClientContentInDatabaseFromCsvWidget()
        {
            const string inputCsvFilePath = @"C:\Temp\Contentful\SQL\widgetstests.csv";
            const int clientId = 256;
            const ContentType contentType = ContentType.Widget;

            if (!Utility.IsClientIdValidForDatabaseUpdate(clientId)) return;

            if (File.Exists(inputCsvFilePath))
            {
                var factory = new ContentModelFactoryContentful();
                var manager = factory.CreateContentManager(clientId);

                manager.UpdateDatabaseClientContentUsingCsv(clientId, contentType, inputCsvFilePath);

                Console.Out.WriteLine("Successfully updated the database for clientId {0} and CSV file {1}.", clientId, inputCsvFilePath);
            }
            else
            {
                Console.Out.WriteLine("Error: File {0} not found.", inputCsvFilePath);
            }
        }

        #endregion

        #region Client Content Orphan Handling Methods

        [Test]
        public void GetContentOrphanTest()
        {

            const string outputCsvFilePath = @"C:\Temp\Contentful\ContentfulOrphans.csv";
            const int clientId = 0;

            var factory = new ContentModelFactoryContentful();
            var manager = factory.CreateContentManager(clientId);

            manager.GetContentOrphan(outputCsvFilePath);

            Console.Out.WriteLine("Successfully created orphan content CSV file '{0}'  from the database for clientId {1}.", outputCsvFilePath, clientId);
        }


        [Test]
        public void DeleteContentOrphanTest()
        {
            const string inputCsvFilePath = @"C:\Temp\Contentful\ContentfulOrphans.csv";
            const int clientId = 0;

            if (File.Exists(inputCsvFilePath))
            {
                var factory = new ContentModelFactoryContentful();
                var manager = factory.CreateContentManager(clientId);

                manager.DeleteContentOrphan(inputCsvFilePath);

                Console.Out.WriteLine("Successfully deleted orphan content from the database for clientId {0} and CSV file {1}.", clientId, inputCsvFilePath);
            }
            else
            {
                Console.Out.WriteLine("Error: File {0} not found.", inputCsvFilePath);
            }
        }

        #endregion

        #endregion

        #region Json Helper Methods

        [Test]
        [Ignore("Ignore unit tests related to Content Management API")]
        public void CreateJsonFromCsv()
        {
            var utility = new Utility();

            utility.CreateJsonFromCsv();
        }
        #endregion

        #region Database Helper Methods

        #region Debug Methods
        [Test]
        [Ignore("Ignore unit tests related to Content Management API")]
        public void CreateInsertClientProfileAttributeProfileOptionScriptsFromCsv()
        {
            const string sqlTemplate = "INSERT INTO [cm].[ClientProfileAttributeProfileOption] ([ClientProfileAttributeID],[ProfileOptionKey]) VALUES ([cm].[fnGetClientProfileAttributeID](0, '{0}'),'{1}');";
            const string inputCsvFilePath = @"C:\Temp\Contentful\SQL\ClientProfileOptions.csv";
            const string outputSqlFilePath = @"C:\Temp\Contentful\SQL\ClientProfileOptionsInsertScripts.sql";

            var utility = new Utility();
            utility.CreateInsertManyToManyTableInsertScripts(sqlTemplate, inputCsvFilePath, outputSqlFilePath);
        }

        #endregion

        #endregion

    }
}
