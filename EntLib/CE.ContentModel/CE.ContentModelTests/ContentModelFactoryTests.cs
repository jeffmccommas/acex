﻿using CE.ContentModel;
using NUnit.Framework;
using System;
using System.Diagnostics;


namespace CE.ContentModelTests
{
    [TestFixture]
    public class ContentModelFactoryTests
    {


        #region Constants

        private const string TemplateElapsedTime = "Elapsed={0}\r\n";
        private const int DefaultClientId = 0;

        #endregion

        [Test]
        public void CreateContentProviderTest()
        {

            var sw = new Stopwatch();

            sw.Start();
            var factory = new ContentModelFactoryContentful();
            var result = factory.CreateContentProvider(256);
            sw.Stop();
            Console.Out.WriteLine(TemplateElapsedTime, sw.Elapsed);
            Console.Out.WriteLine(result);

            Assert.NotNull(result);

        }

        [Test]
        public void CreateContentManagerTest()
        {

            var sw = new Stopwatch();

            sw.Start();
            var factory = new ContentModelFactoryContentful();
            var result = factory.CreateContentManager(DefaultClientId);
            sw.Stop();
            Console.Out.WriteLine(TemplateElapsedTime, sw.Elapsed);
            Console.Out.WriteLine(result);

            Assert.NotNull(result);

        }

    }
}
