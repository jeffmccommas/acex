﻿using CE.ContentModel;
using NUnit.Framework;
using System;
using System.Diagnostics;

namespace CE.ContentModelTests
{
    [TestFixture]
    public class ContentRepositoryTests
    {

        #region Constants

        private const string TemplateElapsedTime = "Elapsed={0}\r\n";
        private const string ClientSpace = "j9e8f655sutg_dev";
        private const string MessageCacheRemoval = "Removed content cache successfully for client space.\r\n";
        private const string MessageCacheRefresh = "Refreshed content cache successfully with Content provider data.\r\n";

        #endregion

        [Test]
        public void GetActionContentTest()
        {

            var sw = new Stopwatch();

            sw.Start();
            var factory = new ContentModelFactoryContentful();
            var repository = factory.CreateContentRepository(256);
            var result = repository.GetContentClientActions();
            sw.Stop();
            Console.Out.WriteLine(TemplateElapsedTime, sw.Elapsed);
            Console.Out.WriteLine(result);

            Assert.NotNull(result);

        }

        [Test]
        public void GetContentCacheTest()
        {

            var sw = new Stopwatch();

            sw.Start();
            var factory = new ContentModelFactoryContentful();
            var repository = factory.CreateContentRepository(256);
            var result = repository.GetRawContent(GetRawContentKey(ClientSpace, "Entry"));
            sw.Stop();
            Console.Out.WriteLine(TemplateElapsedTime, sw.Elapsed);
            Console.Out.WriteLine(result);

            Assert.NotNull(result);

        }


        [Test]
        public void DeleteContentCacheTest()
        {

            var sw = new Stopwatch();

            sw.Start();
            var factory = new ContentModelFactoryContentful();
            var repository = factory.CreateContentRepository(256);
            var result = repository.DeleteRawContent(GetRawContentKey(ClientSpace, "Entry"));
            sw.Stop();
            Console.Out.WriteLine(TemplateElapsedTime, sw.Elapsed);
            Console.Out.WriteLine(result);

            Assert.NotNull(result);

            Console.Out.WriteLine(MessageCacheRemoval);

            //reset the content cache again after is deleted so doesn't leave it in an unfinished state
            var provider = factory.CreateContentProvider(256);
            provider.RefreshContentCacheFromContentful("aclara");

            Console.Out.WriteLine(MessageCacheRefresh);
        }


        /// <summary>
        /// Creates Content Cache Key
        /// </summary>
        /// <param name="space"></param>
        /// <param name="contentType"></param>
        /// <returns></returns>
        private static string GetRawContentKey(string space, string contentType)
        {
            const string templateCacheKey = "{0}_{1}";
            var result = string.Format(templateCacheKey, space, contentType);
            return result;
        }
    }
}
