﻿using Newtonsoft.Json;
using System;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace CE.ContentModelTests
{
    internal class Utility
    {
        #region Constants


        #endregion

        #region Class Variables

        #endregion

        #region Public Methods

        #region Working Methods

        public static bool IsClientIdValidForDatabaseUpdate(int clientId)
        {
            var result = false;
            if (clientId > 0)
            {
                result = true;
            }
            else
            {
                Console.Out.WriteLine("ClientId {0} is not valid for database content update.", clientId);
            }
            return result;
        }

        #endregion

        #region Debug Methods
        public void CreateJsonFromCsv()
        {
            var sr = new StreamReader(@"c:\temp\Json\Label.csv"); //SOURCE FILE
            var sw = new StreamWriter(@"c:\temp\Json\Label.json"); // DESTINATION FILE

            var line = sr.ReadLine();


            if (line != null)
            {
                string[] cols = Regex.Split(line, ",");

                var table = new DataTable();
                foreach (string t in cols)
                {
                    table.Columns.Add(t, typeof(string));
                }
                while ((line = sr.ReadLine()) != null)
                {
                    table.Rows.Clear();

                    int i;
                    string[] rows = Regex.Split(line, ",");
                    var dr = table.NewRow();

                    for (i = 0; i < rows.Length; i++)
                    {
                        dr[i] = rows[i];

                    }
                    table.Rows.Add(dr);

                    string json = JsonConvert.SerializeObject(table, Formatting.Indented);
                    sw.Write(json);
                }
            }


            sw.Close();
            sr.Close();

        }


        public void CreateInsertManyToManyTableInsertScripts(string sqlTemplate, string inputCsvFilePath, string outputSqlFilePath)
        {
            string line;
            var sr = new StreamReader(inputCsvFilePath);
            var sb = new StringBuilder();
            while ((line = sr.ReadLine()) != null)
            {
                var row = Regex.Split(line, ",");

                var table1Key = row[0];
                var table2Keys = row[1];

                var table2KeyList = table2Keys.Split(Convert.ToChar(";"));

                foreach (var sql in from table2Key in table2KeyList
                                    where !string.IsNullOrEmpty(table2Key)
                                    select string.Format(sqlTemplate, table1Key.Trim(),
                                        table2Key.Trim().Replace(" ", ""))
                                        into sql
                                        where !string.IsNullOrEmpty(sql)
                                        select sql)
                {
                    sb.AppendLine(sql);
                }
            }

            using (var writer = new StreamWriter(outputSqlFilePath, true))
            {
                writer.WriteLine(sb.ToString());
            }


            sr.Close();

        }

        #endregion

        #endregion

    }
}
