﻿using CE.ContentModel;
using NUnit.Framework;
using System;
using System.Diagnostics;
using System.Globalization;

namespace CE.ContentModelTests
{
    [TestFixture]
    public class ContentProviderTests
    {


        #region Constants

        private const string TemplateElapsedTime = "Elapsed={0}\r\n";
        private const string MessageCacheRefresh = "Refreshed content cache successfully with Content provider data.\r\n";

        #endregion


        [Test]
        public void GetDefaultActionTest()
        {

            var sw = new Stopwatch();

            sw.Start();
            var factory = new ContentModelFactoryContentful();
            var provider = factory.CreateContentProvider(256);

            var result1 = provider.GetDefaultAction(87);

            sw.Stop();
            Console.Out.WriteLine(TemplateElapsedTime, sw.Elapsed);
            Console.Out.WriteLine(result1);

            Assert.NotNull(result1);

        }

        [Test]
        public void GetContentActionTest()
        {
            var sw = new Stopwatch();

            sw.Start();
            var factory = new ContentModelFactoryContentful();
            var provider = factory.CreateContentProvider(256);

            var result1 = provider.GetContentAction(string.Empty);

            sw.Stop();
            Console.Out.WriteLine(TemplateElapsedTime, sw.Elapsed);
            Console.Out.WriteLine(result1);

            Assert.NotNull(result1);
        }

        [Test]
        public void GetProfileAttributesTest()
        {

            var sw = new Stopwatch();

            sw.Start();
            var factory = new ContentModelFactoryContentful();
            var provider = factory.CreateContentProvider(256);
            const string attributesFilter = "clotheswasher.count,clotheswasher.efficiency";
            const string enduseFilter = "heating";
            const string applianceFilter = "clotheswasher";
            const string entitylevelFilter = "premise";

            var result1 = provider.GetProfileAttributes(87, attributesFilter, enduseFilter, applianceFilter, entitylevelFilter);

            sw.Stop();
            Console.Out.WriteLine(TemplateElapsedTime, sw.Elapsed);
            Console.Out.WriteLine(result1);

            Assert.NotNull(result1);

        }

        [Test]
        public void GetContentRawTest_Currencyfound_WhenContentTypeCurrencyusd()
        {

            var sw = new Stopwatch();

            sw.Start();
            var factory = new ContentModelFactoryContentful();
            var provider = factory.CreateContentProvider(256);
            var result1 = provider.GetContentRaw(ContentType.Currency, "usd");
            sw.Stop();
            Console.Out.WriteLine(TemplateElapsedTime, sw.Elapsed);
            Console.Out.WriteLine(result1);

            Assert.NotNull(result1);

        }

        [Test]
        public void GetContentRawTest_Exceptioncurrencycadkeynotfound_WhenInvalidCurrency()
        {
            var sw = new Stopwatch();
            try
            {
                sw.Start();
                var factory = new ContentModelFactoryContentful();
                var provider = factory.CreateContentProvider(256);
                var result1 = provider.GetContentRaw(ContentType.Currency, "cad");
                sw.Stop();
                Console.Out.WriteLine(TemplateElapsedTime, sw.Elapsed);
                Console.Out.WriteLine(result1);

                Assert.NotNull(result1);
            }
            catch (Exception e)
            {
                Console.Out.WriteLine("Exception thrown:" + e.Message);
                Assert.IsTrue(e.Message.Contains("Content related to currency type and cad key is not found"));
            }
            Console.Out.WriteLine(TemplateElapsedTime, sw.Elapsed);

        }

        [Test]
        public void GetContentStringTest_ResultsNotNull_WhenContentTypeCurrencyandKeyusdSpanishLocale()
        {

            var sw = new Stopwatch();

            sw.Start();
            var factory = new ContentModelFactoryContentful();
            var provider = factory.CreateContentProvider(256);
            var result1 = provider.GetContentByFieldName(ContentType.Currency, "usd", ContentField.Name, ContentLocale.SpanishSpain);
            sw.Stop();
            Console.Out.WriteLine(TemplateElapsedTime, sw.Elapsed);
            Console.Out.WriteLine(result1);

            Assert.NotNull(result1);

        }

        [Test]
        public void GetContentTextContentTest_ResultsNotNull_ForLabelContentCategoryBillDisaggandSpanishLocale()
        {

            var sw = new Stopwatch();

            sw.Start();
            var factory = new ContentModelFactoryContentful();
            var provider = factory.CreateContentProvider(256);
            var result1 = provider.GetContentTextContent("billdisagg", ContentLocale.SpanishSpain, string.Empty);
            sw.Stop();
            Console.Out.WriteLine(TemplateElapsedTime, sw.Elapsed);
            Console.Out.WriteLine(result1);

            Assert.NotNull(result1);

        }

        /// <summary>
        /// Check that benchmark.groupsize is returned as part of the content configuration for benchmark
        /// </summary>
        [Test]
        public void GetContentConfigurationTest_ContentCategoryBenchmarkReturned_WhenContentCategoryBenchmarkandQAEnvironment()
        {

            var sw = new Stopwatch();

            sw.Start();
            var factory = new ContentModelFactoryContentful();
            var provider = factory.CreateContentProvider(256);
            var result1 = provider.GetContentConfiguration("benchmark", string.Empty);
            sw.Stop();
            Console.Out.WriteLine(TemplateElapsedTime, sw.Elapsed);
            Console.Out.WriteLine(result1);

            Assert.NotNull(result1);
            Assert.IsTrue(result1.ToString().Contains("groupsize"), "groupsize key for benchmark not found");
        }

        /// <summary>
        /// Verifies that entry overrideconfigkey  key is found when getcontentconfiguration, 
        /// contentcategory of comment on qa content environment (was created only for qa environment)
        /// </summary>
        [Test]
        public void GetContentConfigurationTest_ContentCategoryCommontestoverrideconfigkeyReturned_WhenContentCategoryCommonandQAEnvironment()
        {

            var sw = new Stopwatch();

            sw.Start();
            var factory = new ContentModelFactoryContentful();
            var provider = factory.CreateContentProvider(256);
            var result1 = provider.GetContentConfiguration("common", string.Empty);
            sw.Stop();
            Console.Out.WriteLine(TemplateElapsedTime, sw.Elapsed);
            Console.Out.WriteLine(result1);

            Assert.NotNull(result1);
            Assert.IsTrue(result1.ToString().Contains("test123qa"), "test123qa value for common.configuration qa environment not found");
        }


        /// <summary>
        /// Verifies that entry overrideconfigkey  key is not found when getcontentconfiguration, 
        /// contentcategory of comment on prod content environment (was created only for qa environment)
        /// </summary>
        [Test]
        public void GetContentConfigurationTest_ContentCategoryCommontestoverrideconfigkeyNotFound_WhenContentCategoryCommonandProdEnvironment()
        {

            var sw = new Stopwatch();

            sw.Start();
            var factory = new ContentModelFactoryContentful();
            var provider = factory.CreateContentProvider(256);
            var result1 = provider.GetContentConfiguration("common", string.Empty);
            sw.Stop();
            Console.Out.WriteLine(TemplateElapsedTime, sw.Elapsed);
            Console.Out.WriteLine(result1);

            Assert.NotNull(result1);
            Assert.IsFalse(result1.ToString().Contains("test123qa"), "test123qa value for common.configuration prod environment was found");
        }

        /// <summary>
        /// Verifies that entry overrideconfigkey  key is not found when getcontentconfiguration, 
        /// contentcategory of comment on prod content environment (was created only for qa environment)
        /// </summary>
        [Test]
        public void GetContentConfigurationBulkTest()
        {

            var sw = new Stopwatch();

            sw.Start();
            var factory = new ContentModelFactoryContentful();
            var provider = factory.CreateContentProvider(87);
            var result1 = provider.GetContentClientConfigurationBulk("system", "aclaraone.clientsettings");
            sw.Stop();
            Console.Out.WriteLine(TemplateElapsedTime, sw.Elapsed);
            Console.Out.WriteLine(result1);

            Assert.NotNull(result1);
            //Assert.IsFalse(result1.ToString().Contains("test123qa"), "test123qa value for common.configuration prod environment was found");
        }

        [Test]
        public void GetContentApplianceTest_ResultsNotNull_WhenSpanishLocale()
        {

            var sw = new Stopwatch();

            sw.Start();
            var factory = new ContentModelFactoryContentful();
            var provider = factory.CreateContentProvider(256);
            var result1 = provider.GetContentAppliance(string.Empty);
            sw.Stop();
            Console.Out.WriteLine(TemplateElapsedTime, sw.Elapsed);
            Console.Out.WriteLine(result1);

            Assert.NotNull(result1);

        }

        [Test]
        public void GetContentCommodityTestSpanishLocale_ElectridadTextFound_WhenSpanish()
        {

            var sw = new Stopwatch();

            sw.Start();
            var factory = new ContentModelFactoryContentful();
            var provider = factory.CreateContentProvider(256);
            var result1 = provider.GetContentCommodity(string.Empty);
            sw.Stop();
            Console.Out.WriteLine(TemplateElapsedTime, sw.Elapsed);
            Console.Out.WriteLine(result1);

            Assert.NotNull(result1);
            //Find some text in Spanish
            Assert.IsTrue(result1.ToString().Contains("Electricidad"), "Electricidad Commodity not found");
        }

        [Test]
        public void GetContentCommodityTestEnglishLocale_ElectricTextFound_WhenEnglish()
        {

            var sw = new Stopwatch();

            sw.Start();
            var factory = new ContentModelFactoryContentful();
            var provider = factory.CreateContentProvider(256);
            var result1 = provider.GetContentCommodity(string.Empty);
            sw.Stop();
            Console.Out.WriteLine(TemplateElapsedTime, sw.Elapsed);
            Console.Out.WriteLine(result1);

            Assert.NotNull(result1);
            //Find some text in Spanish
            Assert.IsTrue(result1.ToString().Contains("Electric"), "Electric Commodity not found");
        }

        [Test]
        public void GetContentApplianceTest_ExceptionInvalidSpaceorAccessKey_WhenInvalidToken()
        {

            var sw = new Stopwatch();
            try
            {
                sw.Start();
                var factory = new ContentModelFactoryContentful();
                var provider = factory.CreateContentProvider(256);
                var result1 = provider.GetContentAppliance(string.Empty);
                Assert.NotNull(result1);
                sw.Stop();
                Console.Out.WriteLine(result1);
            }
            catch (Exception e)
            {
                Console.Out.WriteLine("Exception thrown:" + e.Message.ToString(CultureInfo.InvariantCulture));
                Assert.IsTrue(e.Message.Contains("Invalid space or access key"));
            }
            Console.Out.WriteLine(TemplateElapsedTime, sw.Elapsed);

        }

        [Test]
        public void GetContentEnduseTestSpanishLocale_ValidContentCocina_WhenSpanishLocale()
        {

            var sw = new Stopwatch();

            sw.Start();
            var factory = new ContentModelFactoryContentful();
            var provider = factory.CreateContentProvider(256);
            var result1 = provider.GetContentEnduse(string.Empty);
            sw.Stop();
            Console.Out.WriteLine(TemplateElapsedTime, sw.Elapsed);
            Console.Out.WriteLine(result1);

            Assert.NotNull(result1);
            //Find some text in Spanish
            Assert.IsTrue(result1.ToString().Contains("Cocina"), "Cocina not found");

        }

        [Test]
        public void GetContentEnduseTestEnglishLocale_ValidContentCookingFound_WhenEnglishLocale()
        {

            var sw = new Stopwatch();

            sw.Start();
            var factory = new ContentModelFactoryContentful();
            var provider = factory.CreateContentProvider(256);
            var result1 = provider.GetContentEnduse(string.Empty);
            sw.Stop();
            Console.Out.WriteLine(TemplateElapsedTime, sw.Elapsed);
            Console.Out.WriteLine(result1);

            Assert.NotNull(result1);
            //Find some text in English
            Assert.IsTrue(result1.ToString().Contains("Cooking"), "Cooking not found");
        }

        /// <summary>
        /// Verifies that invalid Locale when passed should default to English. Detects for english word 'Cooking' on results back.
        /// </summary>
        [Test]
        public void GetContentEnduseTest_DefaultEnUsLocaleReturned_WhenInvalidLocaleEnum3()
        {

            var sw = new Stopwatch();

            sw.Start();
            var factory = new ContentModelFactoryContentful();
            var provider = factory.CreateContentProvider(256);
            //use invalid locale only have enums 0 and 1 for English and Spanish
            var result1 = provider.GetContentEnduse(string.Empty);
            sw.Stop();
            Console.Out.WriteLine(TemplateElapsedTime, sw.Elapsed);
            Console.Out.WriteLine(result1);

            Assert.NotNull(result1);
            //Find some text in English
            Assert.IsTrue(result1.ToString().Contains("Cooking"), "Cooking not found");
        }

        /// <summary>
        /// Verifies that Spanish Locale when passed should return valid content for meaurements. Detects for spanish word for trees on results back.
        /// </summary>
        [Test]
        public void GetContentMeasurementTest_ValidContent_WhenSpanishLocale()
        {

            var sw = new Stopwatch();

            sw.Start();
            var factory = new ContentModelFactoryContentful();
            var provider = factory.CreateContentProvider(256);
            //use invalid locale only have enums 0 and 1 for English and Spanish
            var result1 = provider.GetContentMeasurement(string.Empty);
            sw.Stop();
            Console.Out.WriteLine(TemplateElapsedTime, sw.Elapsed);
            Console.Out.WriteLine(result1);

            Assert.NotNull(result1);
            //Find some text in Spanish
            Assert.IsTrue(result1.ToString().Contains("árboles"), "Spanish árboles not found");
        }

        /// <summary>
        /// Verifies that default english content returned  when invalid locale supplied without throwing error.
        /// </summary>
        [Test]
        public void GetContentMeasurementTest_DefaultEnUsLocaleReturned_WhenInvalidLocaleEnum3()
        {

            var sw = new Stopwatch();

            sw.Start();
            var factory = new ContentModelFactoryContentful();
            var provider = factory.CreateContentProvider(256);
            //use invalid locale only have enums 0 and 1 for English and Spanish
            var result1 = provider.GetContentMeasurement(string.Empty);
            sw.Stop();
            Console.Out.WriteLine(TemplateElapsedTime, sw.Elapsed);
            Console.Out.WriteLine(result1);

            Assert.NotNull(result1);
            //Find some text in English
            Assert.IsTrue(result1.ToString().Contains("trees"), "English trees not found");
        }

        /// <summary>
        /// Verifies that default english content returned  when invalid locale supplied without throwing error.
        /// </summary>
        [Test]
        public void GetContentUOMTest_DefaultEnUsLocaleReturned_WhenInvalidLocaleEnum3()
        {

            var sw = new Stopwatch();

            sw.Start();
            var factory = new ContentModelFactoryContentful();
            var provider = factory.CreateContentProvider(256);
            //use invalid locale only have enums 0 and 1 for English and Spanish
            var result1 = provider.GetContentUom(string.Empty);
            sw.Stop();
            Console.Out.WriteLine(TemplateElapsedTime, sw.Elapsed);
            Console.Out.WriteLine(result1);

            Assert.NotNull(result1);
            //Find some text in English
            Assert.IsTrue(result1.ToString().Contains("kWh"), "kWh not found");
        }

        /// <summary>
        /// Verifies that spanish content returned for UOM when spanish locale supplied.
        /// </summary>
        [Test]
        public void GetContentUOMTest_ValidContent_WhenSpanishLocale()
        {

            var sw = new Stopwatch();

            sw.Start();
            var factory = new ContentModelFactoryContentful();
            var provider = factory.CreateContentProvider(256);

            var result1 = provider.GetContentUom(string.Empty);
            sw.Stop();
            Console.Out.WriteLine(TemplateElapsedTime, sw.Elapsed);
            Console.Out.WriteLine(result1);

            Assert.NotNull(result1);
            //Find some text in Spanish
            Assert.IsTrue(result1.ToString().Contains("árboles"), "Spanish árboles not found");
        }

        /// <summary>
        /// Verifies that english content returned for Currency when english locale supplied.
        /// </summary>
        [Test]
        public void GetContentCurrencyTest_ValidContent_WhenEnglishLocale()
        {

            var sw = new Stopwatch();

            sw.Start();
            var factory = new ContentModelFactoryContentful();
            var provider = factory.CreateContentProvider(256);
            var result1 = provider.GetContentCurrency(string.Empty);
            sw.Stop();
            Console.Out.WriteLine(TemplateElapsedTime, sw.Elapsed);
            Console.Out.WriteLine(result1);

            Assert.NotNull(result1);
            //Find some text in english
            Assert.IsTrue(result1.ToString().Contains("Euros"), "Euro currency not found");
        }

        /// <summary>
        /// Verifies that english content returned for Currency when english locale supplied.
        /// </summary>
        [Test]
        public void GetContentCurrencyTest_ValidContent_WhenSpanishLocale()
        {

            var sw = new Stopwatch();

            sw.Start();
            var factory = new ContentModelFactoryContentful();
            var provider = factory.CreateContentProvider(256);
            var result1 = provider.GetContentCurrency(string.Empty);
            sw.Stop();
            Console.Out.WriteLine(TemplateElapsedTime, sw.Elapsed);
            Console.Out.WriteLine(result1);

            Assert.NotNull(result1);
            //Find some text in spanish
            Assert.IsTrue(result1.ToString().Contains("Dólares"), "Dólares currency not found");
        }

        [Test]
        public void RefreshContentCacheTest()
        {
            var factory = new ContentModelFactoryContentful();
            var provider = factory.CreateContentProvider(256);

            provider.RefreshContentCacheFromContentful("aclara");

            Console.Out.WriteLine(MessageCacheRefresh);

        }

    }
}
