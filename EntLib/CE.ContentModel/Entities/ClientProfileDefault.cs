﻿using System;
using System.Runtime.Serialization;

namespace CE.ContentModel.Entities
{
    [DataContract]
    [Serializable]
    public class ClientProfileDefault
    {
        [DataMember(Order = 0, Name = "key")]
        public string Key { get; set; }
        [DataMember(Order = 1, Name = "profileattributekey")]
        public string ProfileAttributeKey { get; set; }
        [DataMember(Order = 2, Name = "defaultprofileoptionkey")]
        public string DefaultKey { get; set; }
        [DataMember(Order = 3, Name = "defaultvalue")]
        public string DefaultValue { get; set; }
        [DataMember(Order = 4)]
        public string ContentSource { get; set; }
    }
}
