﻿using System;
using System.Runtime.Serialization;

// ReSharper disable once CheckNamespace
namespace CE.ContentModel
{
    [Serializable]
    [DataContract]
    public class ClientProfileAttributeDefault
    {
        [DataMember(Order = 0)]
        public string AttributeKey { get; set; }
        [DataMember(Order = 1)]
        public string AttributeValue { get; set; }
        [DataMember(Order = 2)]
        public string Type { get; set; }
        [DataMember(Order = 3)]
        public string OptionKeys { get; set; }
        [DataMember(Order = 4)]
        public string ConditionKeys { get; set; }
        [DataMember(Order = 5)]
        public string EntityLevel { get; set; }
        [DataMember(Order = 6)]
        public int? MinValue { get; set; }
        [DataMember(Order = 7)]
        public int? MaxValue { get; set; }
        [DataMember(Order = 8)]
        public bool DoNotDefault { get; set; }
    }
}
