﻿using System;
using System.Runtime.Serialization;

namespace CE.ContentModel.Entities
{
    [DataContract]
    [Serializable]
    public class ClientCurrency
    {
        [DataMember(Order = 0, Name = "key")]
        public string Key { get; set; }
        [DataMember(Order = 1, Name = "namekey")]
        public string NameKey { get; set; }
        [DataMember(Order = 2, Name = "symbolkey")]
        public string SymbolKey { get; set; }
    }
}
