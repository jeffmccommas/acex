﻿using System;
using System.Runtime.Serialization;

namespace CE.ContentModel.Entities
{
    [DataContract]
    [Serializable]
    public class ClientEndUse
    {
        [DataMember(Order = 0, Name = "key")]
        public string Key { get; set; }
        [DataMember(Order = 1, Name = "namekey")]
        public string NameKey { get; set; }
        [DataMember(Order = 2, Name = "category")]
        public string Category { get; set; }
        [DataMember(Order = 3, Name = "appliances")]
        public string Appliances { get; set; }
        [DataMember(Order = 4, Name = "hide")]
        public string Hide { get; set; }
    }
}
