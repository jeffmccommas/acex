﻿using System;
using System.Runtime.Serialization;

namespace CE.ContentModel.Entities
{
    [DataContract]
    [Serializable]
    public class ClientProfileOption
    {
        [DataMember(Order = 0, Name = "key")]
        public string Key { get; set; }
        [DataMember(Order = 1, Name = "value")]
        public decimal ProfileOptionValue { get; set; }
        [DataMember(Order = 2, Name = "namekey")]
        public string NameKey { get; set; }
        [DataMember(Order = 3, Name = "description")]
        public string Description { get; set; }
    }
}
