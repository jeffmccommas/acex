﻿using System;
using System.Runtime.Serialization;

namespace CE.ContentModel.Entities
{
    [DataContract]
    [Serializable]
    public class ClientProfileSection
    {
        [DataMember(Order = 0, Name = "key")]
        public string Key { get; set; }
        [DataMember(Order = 1, Name = "name")]
        public string Name { get; set; }
        [DataMember(Order = 2, Name = "descriptionkey")]
        public string DescriptionKey { get; set; }
        [DataMember(Order = 3, Name = "titlekey")]
        public string TitleKey { get; set; }
        [DataMember(Order = 4, Name = "subtitlekey")]
        public string SubTitleKey { get; set; }
        [DataMember(Order = 5, Name = "rank")]
        public int ProfileSectionRank { get; set; }
        [DataMember(Order = 6, Name = "profileattributes")]
        public string ProfileAttributes { get; set; }
        [DataMember(Order = 7, Name = "conditions")]
        public string Conditions { get; set; }
        [DataMember(Order = 8, Name = "images")]
        public string Images { get; set; }
        [DataMember(Order = 9, Name = "introtextkey")]
        public string IntroTextKey { get; set; }

    }
}