﻿using System;
using System.Runtime.Serialization;

namespace CE.ContentModel.Entities
{
    [DataContract]
    [Serializable]
    public class ClientSeason
    {
        [DataMember(Order = 0, Name = "key")]
        public string Key { get; set; }
        [DataMember(Order = 1, Name = "namekey")]
        public string NameKey { get; set; }
        [DataMember(Order = 2, Name = "startmonth")]
        public int StartMonth { get; set; }
        [DataMember(Order = 3, Name = "startday")]
        public int StartDay { get; set; }
        [DataMember(Order = 4, Name = "endmonth")]
        public int EndMonth { get; set; }
        [DataMember(Order = 5, Name = "endday")]
        public int EndDay { get; set; }
    }
}
