﻿using System;
using System.Runtime.Serialization;

namespace CE.ContentModel.Entities
{
    [DataContract]
    [Serializable]
    public class ClientEnumeration
    {
        [DataMember(Order = 0, Name = "enumerationkey")]
        public string EnumerationKey { get; set; }
        [DataMember(Order = 1, Name = "category")]
        public string Category { get; set; }
        [DataMember(Order = 2, Name = "key")]
        public string Key { get; set; }
        [DataMember(Order = 3, Name = "namekey")]
        public string NameKey { get; set; }
        [DataMember(Order = 4, Name = "displayorder")]
        public int DisplayOrder { get; set; }

    }
}
