﻿using System;
using System.Runtime.Serialization;

namespace CE.ContentModel.Entities
{
    /// <summary>
    /// Configuration settings to use in the bill-to-date calculation. These will override any stored configurations. 
    /// </summary>
    [DataContract]
    [Serializable]
    public class ClientConfiguration
    {
        /// <summary>
        /// Key value for the configuration setting.
        /// </summary>
        [DataMember(Order = 0, Name = "key")]
        public string Key { get; set; }

        /// <summary>
        /// The category of the configuration setting.
        /// </summary>
        [DataMember(Order = 1, Name = "category")]
        public string Category { get; set; }

        /// <summary>
        /// The configuration value.
        /// </summary>
        [DataMember(Order = 2, Name = "value")]
        public string @Value { get; set; }

        /// <summary>
        /// The description of the configuration setting. 
        /// </summary>
        [DataMember(Order = 3, Name = "description")]
        public string @Description { get; set; }
    }
}
