﻿
// ReSharper disable once CheckNamespace
namespace CE.ContentModel
{
    #region Enums

    public enum ContentProviderType
    {
        Contentful,
        Mock
    }

    public enum ContentCategory
    {
        Action,
        All,
        Bill,
        BillDisagg,
        Benchmark,
        Consumption,
        Common,
        EnergyModel,
        Notification,
        Profile,
        System
    }

    public enum ContentEnvironment
    {
        Prod,
        Dr,
        Uat,
        Qa,
        Dev,
        LocalDev

    }

    public enum ContentField
    {
        Category,
        Description,
        Key,
        Label,
        LongText,
        MediumText,
        Name,
        ShortText,
        Symbol
    }

    public enum ContentLocale
    {
        EnglishUnitedStates,
        RussianRussia,
        SpanishSpain

    }

    public enum ContentType
    {
        All,
        Action,
        ActionSavings,
        Appliance,
        Asset,
        BenchmarkGroup,
        Commodity,
        Condition,
        ConfigurationBulk,
        Configuration,
        Currency,
        Enduse,
        Enumeration,
        EnumerationItem,
        Expression,
        FileContent,
        Layout,
        Measurement,
        ProfileAttribute,
        ProfileDefault,
        ProfileDefaultCollection,
        ProfileOption,
        ProfileSection,
        Season,
        Tab,
        TextContent,
        Uom,
        Widget,
        WhatIfData
    }

    public enum ContentDataTable
    {
        ClientActionTable,
        ClientApplianceTable,
        ClientApplianceProfileAttributeTable,
        ClientApplianceLocaleContentTable,
        ClientAssetTable,
        ClientBenchmarkGroupTable,
        ClientCommodityTable,
        ClientConditionTable,
        ClientConfigurationBulkTable,
        ClientConfigurationTable,
        ClientCurrencyTable,
        ClientEndUseTable,
        ClientEnumerationTable,
        ClientEnumerationItemTable,
        ClientExpressionTable,
        ClientFileContentTable,
        ClientMeasurementTable,
        ClientLayoutTable,
        ClientProfileAttributeTable,
        ClientProfileDefaultTable,
        ClientProfileDefaultCollectionTable,
        ClientProfileOptionTable,
        ClientProfileSectionTable,
        ClientSeasonTable,
        ClientTabTable,
        ClientTextContentTable,
        ClientUomTable,
        ClientWhatIfDataTable,
        ClientWidgetTable
    }

    #endregion
}
