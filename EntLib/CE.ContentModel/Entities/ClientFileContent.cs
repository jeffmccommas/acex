﻿using System;
using System.Runtime.Serialization;
namespace CE.ContentModel.Entities
{
    [DataContract]
    [Serializable]
    public class ClientFileContent
    {
        public string Locale { get; set; }
        [DataMember(Order = 0, Name = "key")]
        public string Key { get; set; }
        [DataMember(Order = 1, Name = "category")]
        public string Category { get; set; }

        [DataMember(Order = 2, Name = "smallfile")]
        public string SmallFile { get; set; }
        [DataMember(Order = 3, Name = "smallfiletitle")]
        public string SmallFileTitle { get; set; }
        [DataMember(Order = 4, Name = "smallfiledescription")]
        public string SmallFileDescription { get; set; }
        [DataMember(Order = 5, Name = "smallfilefilename")]
        public string SmallFileFileName { get; set; }
        [DataMember(Order = 6, Name = "smallfilefilecontenttype")]
        public string SmallFileType { get; set; }
        [DataMember(Order = 7, Name = "smallfilesize")]
        public int SmallFileFileSize { get; set; }

        [DataMember(Order = 8, Name = "mediumfile")]
        public string MediumFile { get; set; }
        [DataMember(Order = 9, Name = "mediumfiletitle")]
        public string MediumFileTitle { get; set; }
        [DataMember(Order = 10, Name = "mediumfiledescription")]
        public string MediumFileDescription { get; set; }
        [DataMember(Order = 11, Name = "mediumfilefilename")]
        public string MediumFileFileName { get; set; }
        [DataMember(Order = 12, Name = "mediumfilefilecontenttype")]
        public string MediumFileType { get; set; }
        [DataMember(Order = 13, Name = "mediumfilesize")]
        public int MediumFileFileSize { get; set; }

        [DataMember(Order = 14, Name = "largefile")]
        public string LargeFile { get; set; }
        [DataMember(Order = 15, Name = "largefiletitle")]
        public string LargeFileTitle { get; set; }
        [DataMember(Order = 16, Name = "largefiledescription")]
        public string LargeFileDescription { get; set; }
        [DataMember(Order = 17, Name = "largefilefilename")]
        public string LargeFileFileName { get; set; }
        [DataMember(Order = 18, Name = "largefilefilecontenttype")]
        public string LargeFileType { get; set; }
        [DataMember(Order = 19, Name = "largefilesize")]
        public int LargeFileFileSize { get; set; }

    }
}
