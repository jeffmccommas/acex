﻿using System;
using System.Runtime.Serialization;

namespace CE.ContentModel.Entities
{
    [DataContract]
    [Serializable]
    public class ClientAction
    {
        [DataMember(Order = 0, Name = "key")]
        public string Key { get; set; }
        [DataMember(Order = 1, Name = "namekey")]
        public string NameKey { get; set; }
        [DataMember(Order = 2, Name = "conditionkeys")]
        public string ConditionKeys { get; set; }
        [DataMember(Order = 3, Name = "descriptionkey")]
        public string DescriptionKey { get; set; }
        [DataMember(Order = 4, Name = "hide")]
        public bool Hide { get; set; }
        [DataMember(Order = 5, Name = "videokey")]
        public string VideoKey { get; set; }
        [DataMember(Order = 6, Name = "imagekey")]
        public string ImageKey { get; set; }
        [DataMember(Order = 7, Name = "iconclass")]
        public string IconClass { get; set; }
        [DataMember(Order = 8, Name = "commoditykey")]
        public string CommodityKey { get; set; }
        [DataMember(Order = 9, Name = "type")]
        public string Type { get; set; }
        [DataMember(Order = 10, Name = "appliancekeys")]
        public string ApplianceKeys { get; set; }
        [DataMember(Order = 11, Name = "defaultupfrontcost")]
        public decimal DefaultUpfrontCost { get; set; }
        [DataMember(Order = 12, Name = "defaultannualcost")]
        public decimal DefaultAnnualCost { get; set; }
        [DataMember(Order = 13, Name = "defaultcostvariancepercent")]
        public decimal DefaultCostVariancePercent { get; set; }
        [DataMember(Order = 14, Name = "difficulty")]
        public string Difficulty { get; set; }
        [DataMember(Order = 15, Name = "habitinterval")]
        public string HabitInterval { get; set; }
        [DataMember(Order = 16, Name = "defaultannualsavingsestimate")]
        public decimal DefaultAnnualSavingsEstimate { get; set; }
        [DataMember(Order = 17, Name = "defaultannualusagesavingsestimate")]
        public decimal DefaultAnnualUsageSavingsEstimate { get; set; }
        [DataMember(Order = 18, Name = "defaultannualusagesavingsuomkey")]
        public string DefaultAnnualUsageSavingsUomKey { get; set; }
        [DataMember(Order = 19, Name = "defaultpaybacktime")]
        public int DefaultPaybackTime { get; set; }
        [DataMember(Order = 20, Name = "defaultroi")]
        public decimal DefaultRoi { get; set; }
        [DataMember(Order = 21, Name = "rebateavailable")]
        public bool RebateAvailable { get; set; }
        [DataMember(Order = 22, Name = "rebateurl")]
        public string RebateUrl { get; set; }
        [DataMember(Order = 23, Name = "rebateimagekey")]
        public string RebateImageKey { get; set; }
        [DataMember(Order = 24, Name = "rebateamount")]
        public decimal RebateAmount { get; set; }
        [DataMember(Order = 25, Name = "seasonkeys")]
        public string SeasonKeys { get; set; }
        [DataMember(Order = 26, Name = "actionpriority")]
        public int ActionPriority { get; set; }
        [DataMember(Order = 27, Name = "savingscalcmethod")]
        public string SavingsCalcMethod { get; set; }
        [DataMember(Order = 28, Name = "savingsamount")]
        public decimal SavingsAmount { get; set; }
        [DataMember(Order = 29, Name = "whatifdatakeys")]
        public string WhatIfDataKeys { get; set; }
        [DataMember(Order = 30, Name = "costexpression")]
        public string CostExpression { get; set; }
        [DataMember(Order = 31, Name = "rebateexpression")]
        public string RebateExpression { get; set; }
        [DataMember(Order = 32, Name = "nextsteplink")]
        public string NextStepLink { get; set; }
        [DataMember(Order = 33, Name = "nextsteplinktext")]
        public string NextStepLinkText { get; set; }
        [DataMember(Order = 34, Name = "nextsteplinktype")]
        public string NextStepLinkType { get; set; }
        [DataMember(Order = 35, Name = "tags")]
        public string Tags { get; set; }
        [DataMember(Order = 36, Name = "minimumsavingsamt")]
        public decimal MinimumSavingsAmt { get; set; }
        [DataMember(Order = 37, Name = "maximumsavingsamt")]
        public decimal MaximumSavingsAmt { get; set; }
    }
}
