﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace CE.ContentModel.Entities
{
    [DataContract]
    [Serializable]
    public class ClientEnumerationList
    {
        [DataMember(Order = 0, Name = "key")]
        public string Key { get; set; }
        [DataMember(Order = 1, Name = "category")]
        public string Category { get; set; }
        [DataMember(Order = 2, Name = "list")]
        public List<ClientEnumerationItem> @List { get; set; }
    }
}
