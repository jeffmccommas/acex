﻿using System;
using System.Runtime.Serialization;

namespace CE.ContentModel.Entities
{
    [DataContract]
    [Serializable]
    public class ClientWidget
    {
        [DataMember(Order = 0, Name = "key")]
        public string Key { get; set; }
        [DataMember(Order = 1, Name = "name")]
        public string Name { get; set; }
        [DataMember(Order = 2, Name = "tabkey")]
        public string TabKey { get; set; }
        [DataMember(Order = 3, Name = "widgettype")]
        public string WidgetType { get; set; }
        [DataMember(Order = 4, Name = "column")]
        public int WidgetColumn { get; set; }
        [DataMember(Order = 5, Name = "row")]
        public int WidgetRow { get; set; }
        [DataMember(Order = 6, Name = "order")]
        public int WidgetOrder { get; set; }
        [DataMember(Order = 7, Name = "conditions")]
        public string Conditions { get; set; }
        [DataMember(Order = 8, Name = "configurations")]
        public string Configurations { get; set; }
        [DataMember(Order = 9, Name = "textcontentlist")]
        public string TextContentList { get; set; }
        [DataMember(Order = 10, Name = "introtextkey")]
        public string IntroTextKey { get; set; }
    }
}
