﻿using System;
using System.Runtime.Serialization;

namespace CE.ContentModel.Entities
{
    [DataContract]
    [Serializable]
    public class ClientConfigurationBulk
    {
        [DataMember(Order = 0, Name = "key")]
        public string Key { get; set; }
        [DataMember(Order = 1, Name = "category")]
        public string Category { get; set; }
        [DataMember(Order = 2, Name = "jsonconfiguration")]
        public string @Value { get; set; }
        [DataMember(Order = 3, Name = "description")]
        public string @Description { get; set; }
    }
}
