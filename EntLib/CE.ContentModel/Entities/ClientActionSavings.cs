﻿using System;
using System.Runtime.Serialization;

namespace CE.ContentModel.Entities
{
    [DataContract]
    [Serializable]
    public class ClientActionSaving
    {
        [DataMember(Order = 0, Name = "key")]
        public string Key { get; set; }
        [DataMember(Order = 1, Name = "rank")]
        public int Rank { get; set; }
        [DataMember(Order = 2, Name = "conditions")]
        public string Conditions { get; set; }
        [DataMember(Order = 3, Name = "cost")]
        public decimal Cost { get; set; }
        [DataMember(Order = 4, Name = "annualsavingsestimate")]
        public decimal AnnualSavingsEstimate { get; set; }
        [DataMember(Order = 5, Name = "annualusagesavingsestimate")]
        public decimal AnnualUsageSavingsEstimate { get; set; }
        [DataMember(Order = 6, Name = "comments")]
        public string Comments { get; set; }
    }
}
