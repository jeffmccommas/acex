﻿using System;
using System.Runtime.Serialization;

namespace CE.ContentModel.Entities
{
    [DataContract]
    [Serializable]
    public class ClientBenchmarkGroup
    {
        [DataMember(Order = 0, Name = "key")]
        public string Key { get; set; }
        [DataMember(Order = 1, Name = "namekey")]
        public string NameKey { get; set; }
        [DataMember(Order = 2, Name = "descriptionkey")]
        public string DescriptionKey { get; set; }
        public string LongText { get; set; }
        [DataMember(Order = 3, Name = "hide")]
        public bool Hide { get; set; }
    }
}
