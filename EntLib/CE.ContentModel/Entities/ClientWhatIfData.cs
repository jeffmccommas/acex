﻿using System;
using System.Runtime.Serialization;

namespace CE.ContentModel.Entities
{
    [DataContract]
    [Serializable]
    public class ClientWhatIfData
    {
        [DataMember(Order = 0, Name = "key")]
        public string Key { get; set; }
        [DataMember(Order = 1, Name = "profileattributekey")]
        public string ProfileAttributeKey { get; set; }
        [DataMember(Order = 2, Name = "valueexpressionkey")]
        public string ValueExpressionKey { get; set; }
    }
}
