﻿using System;
using System.Runtime.Serialization;

namespace CE.ContentModel.Entities
{
    [DataContract]
    [Serializable]
    public class ClientTextContent
    {
        public string Locale { get; set; }
        [DataMember(Order = 0, Name = "key")]
        public string Key { get; set; }
        [DataMember(Order = 1, Name = "category")]
        public string Category { get; set; }
        [DataMember(Order = 2, Name = "shorttext")]
        public string ShortText { get; set; }
        [DataMember(Order = 3, Name = "mediumtext")]
        public string MediumText { get; set; }
        [DataMember(Order = 4, Name = "longtext")]
        public string LongText { get; set; }
        [DataMember(Order = 5, Name = "requirevariablesubstitution")]
        public bool RequireVariableSubstitution { get; set; }
    }
}
