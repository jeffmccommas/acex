﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace CE.ContentModel.Entities
{

    [DataContract]
    [Serializable]
    public class ClientContent
    {
        /// <summary>
        /// Action Content
        /// </summary>
        [DataMember(Order = 0)]
        public List<ClientAction> Action { get; set; }

        /// <summary>
        /// Action Savings Content
        /// </summary>
        [DataMember(Order = 1)]
        public List<ClientActionSaving> ActionSavings { get; set; }

        /// <summary>
        /// Appliance Content
        /// </summary>
        [DataMember(Order = 2)]
        public List<ClientAppliance> Appliance { get; set; }

        /// <summary>
        /// Benchmark Group Content
        /// </summary>
        [DataMember(Order = 4)]
        public List<ClientBenchmarkGroup> BenchmarkGroup { get; set; }

        /// <summary>
        /// Commodity Content
        /// </summary>
        [DataMember(Order = 5)]
        public List<ClientCommodity> Commodity { get; set; }

        /// <summary>
        /// Condition Content
        /// </summary>
        [DataMember(Order = 6)]
        public List<ClientCondition> Condition { get; set; }

        /// <summary>
        /// Configuration Content
        /// </summary>
        [DataMember(Order = 7)]
        public List<ClientConfiguration> Configuration { get; set; }

        /// <summary>
        /// Configuration Bulk Content
        /// TFS 598 Aug 2015
        /// </summary>
        [DataMember(Order = 8)]
        public List<ClientConfigurationBulk> ConfigurationBulk { get; set; }

        /// <summary>
        /// Currency Content
        /// </summary>
        [DataMember(Order = 9)]
        public List<ClientCurrency> Currency { get; set; }

        /// <summary>
        /// Enduse Content
        /// </summary>
        [DataMember(Order = 10)]
        public List<ClientEndUse> EndUse { get; set; }

        /// <summary>
        /// Enumeration Content
        /// </summary>
        [DataMember(Order = 11)]
        public List<ClientEnumeration> Enumeration { get; set; }

        /// <summary>
        /// Expression Content
        /// </summary>
        [DataMember(Order = 12)]
        public List<ClientExpression> Expression { get; set; }

        /// <summary>
        /// File Content
        /// </summary>
        [DataMember(Order = 13)]
        public List<ClientFileContent> FileContent { get; set; }

        /// <summary>
        /// Layout Content
        /// </summary>
        [DataMember(Order = 14)]
        public List<ClientLayout> Layout { get; set; }

        /// <summary>
        /// Measurement Content
        /// </summary>
        [DataMember(Order = 15)]
        public List<ClientMeasurement> Measurement { get; set; }

        /// <summary>
        /// Profile Attribute Content
        /// </summary>
        [DataMember(Order = 16)]
        public List<ClientProfileAttribute> ProfileAttribute { get; set; }

        /// <summary>
        /// Profile Default Content
        /// </summary>
        [DataMember(Order = 17)]
        public List<ClientProfileDefault> ProfileDefault { get; set; }

        /// <summary>
        /// Profile Default Collection Content
        /// </summary>
        [DataMember(Order = 18)]
        public List<ClientProfileDefaultCollection> ProfileDefaultCollection { get; set; }

        /// <summary>
        /// Profile Option Content
        /// </summary>
        [DataMember(Order = 19)]
        public List<ClientProfileOption> ProfileOption { get; set; }

        /// <summary>
        /// Profile Section Content
        /// </summary>
        [DataMember(Order = 20)]
        public List<ClientProfileSection> ProfileSection { get; set; }

        /// <summary>
        /// Season Content
        /// </summary>
        [DataMember(Order = 21)]
        public List<ClientSeason> Season { get; set; }

        /// <summary>
        /// Tab Content
        /// </summary>
        [DataMember(Order = 22)]
        public List<ClientTab> Tab { get; set; }

        /// <summary>
        /// Text Content
        /// </summary>
        [DataMember(Order = 23)]
        public List<ClientTextContent> TextContent { get; set; }

        /// <summary>
        /// UOM Content
        /// </summary>
        [DataMember(Order = 24, Name = "UOM")]
        public List<ClientUOM> Uom { get; set; }

        /// <summary>
        /// Widget Content
        /// </summary>
        [DataMember(Order = 25)]
        public List<ClientWhatIfData> WhatIfData { get; set; }

        /// <summary>
        /// Widget Content
        /// </summary>
        [DataMember(Order = 26)]
        public List<ClientWidget> Widget { get; set; }
    }
}


