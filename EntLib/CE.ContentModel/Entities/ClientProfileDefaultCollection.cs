﻿using System;
using System.Runtime.Serialization;

namespace CE.ContentModel.Entities
{
    [DataContract]
    [Serializable]
    public class ClientProfileDefaultCollection
    {
        [DataMember(Order = 0, Name = "key")]
        public string Key { get; set; }
        [DataMember(Order = 1, Name = "namekey")]
        public string NameKey { get; set; }
        [DataMember(Order = 2, Name = "description")]
        public string Description { get; set; }
    }
}
