﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using Newtonsoft.Json;

namespace CE.ContentModel.Entities
{
    [DataContract]
    [Serializable]
    public class ClientLayout
    {
        [DataMember(Order = 0, Name = "key")]
        public string Key { get; set; }
        [DataMember(Order = 1, Name = "description")]
        public string Description { get; set; }

        [DataMember(Order = 2, Name = "layoutjson")]
        public LayoutRoot TabLayout { get; set; }

        [DataMember(Order = 3)]
        public string LayoutJson
        {
            get { return null; }
            set
            {
                TabLayout = (LayoutRoot)JsonConvert.DeserializeObject(value, typeof(LayoutRoot));
            }
        }
    }

    [DataContract]
    [Serializable]
    public class Column
    {
        [DataMember(Order = 0)]
        public int Span { get; set; }
        [DataMember(Order = 1)]
        public string Size { get; set; }
    }

    [DataContract]
    [Serializable]
    public class Row
    {
        [DataMember(Order = 0)]
        public List<Column> Columns { get; set; }
    }

    [DataContract]
    [Serializable]
    public class Layout
    {
        [DataMember(Order = 0)]
        public List<Row> Rows { get; set; }
    }

    [DataContract]
    [Serializable]
    public class LayoutRoot
    {
        [DataMember(Order = 0)]
        public Layout Layout { get; set; }
    }

}
