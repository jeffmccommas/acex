﻿using System;
using System.Runtime.Serialization;

namespace CE.ContentModel.Entities
{
    /// <summary>
    /// This is Default Client Actions
    /// </summary>
    [DataContract]
    [Serializable]
    public class ClientActionDefault
    {
        [DataMember(Order = 0)]
        public string Key { get; set; }
        [DataMember(Order = 1)]
        public decimal? UpfrontCost { get; set; }
        [DataMember(Order = 2)]
        public decimal? AnnualCost { get; set; }
        [DataMember(Order = 3)]
        public decimal? CostVariancePercent { get; set; }
        [DataMember(Order = 4)]
        public decimal? AnnualSavingsEstimate { get; set; }
        [DataMember(Order = 5)]
        public decimal? AnnualUsageSavingsEstimate { get; set; }
        [DataMember(Order = 6, Name = "AnnualUsageSavingsUOMKey")]
        public string AnnualUsageSavingsUomKey { get; set; }
        [DataMember(Order = 7)]
        public string SavingsEstimateQuality { get; set; }
        [DataMember(Order = 8)]
        public decimal? PaybackTime { get; set; }
        [DataMember(Order = 9, Name = "ROI")]
        public decimal? Roi { get; set; }
        [DataMember(Order = 10)]
        public string Popularity { get; set; }
    }
}
