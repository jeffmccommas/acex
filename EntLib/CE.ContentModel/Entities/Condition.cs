﻿using System;
using System.Runtime.Serialization;

namespace CE.ContentModel.Entities
{
    [DataContract]
    [Serializable]
    public class Condition
    {
        [DataMember(Order = 0)]
        public string Key { get; set; }
        [DataMember(Order = 1)]
        public string Type { get; set; }
        [DataMember(Order = 2)]
        public string Description { get; set; }
        [DataMember(Order = 3)]
        public string AttributeKey { get; set; }
        [DataMember(Order = 4)]
        public string Operator { get; set; }
        [DataMember(Order = 5)]
        public string Value { get; set; }
    }
}
