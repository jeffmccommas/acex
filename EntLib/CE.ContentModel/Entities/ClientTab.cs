﻿using System;
using System.Runtime.Serialization;

namespace CE.ContentModel.Entities
{
    [DataContract]
    [Serializable]
    public class ClientTab
    {
        [DataMember(Order = 0, Name = "key")]
        public string Key { get; set; }
        [DataMember(Order = 1, Name = "name")]
        public string Name { get; set; }
        [DataMember(Order = 2, Name = "menuorder")]
        public int MenuOrder { get; set; }
        [DataMember(Order = 3, Name = "layoutkey")]
        public string LayoutKey { get; set; }
        [DataMember(Order = 4, Name = "tabtype")]
        public string TabType { get; set; }
        [DataMember(Order = 5, Name = "namekey")]
        public string NameKey { get; set; }
        [DataMember(Order = 6, Name = "widgetkey")]
        public string WidgetKey { get; set; }
        [DataMember(Order = 7, Name = "url")]
        public string Url { get; set; }
        [DataMember(Order = 8, Name = "conditions")]
        public string Conditions { get; set; }
        [DataMember(Order = 9, Name = "configurations")]
        public string Configurations { get; set; }
        [DataMember(Order = 10, Name = "textcontentlist")]
        public string TextContentList { get; set; }
        [DataMember(Order = 11, Name = "profilesections")]
        public string ProfileSections { get; set; }
        [DataMember(Order = 12, Name = "childtabs")]
        public string ChildTabs { get; set; }
        [DataMember(Order = 13, Name = "isurlinternal")]
        public bool IsUrlInternal { get; set; }
        [DataMember(Order = 14, Name = "tabicontype")]
        public string TabIconType { get; set; }
        [DataMember(Order = 15, Name = "tabiconclass")]
        public string TabIconClass { get; set; }
        [DataMember(Order = 16, Name = "images")]
        public string Images { get; set; }
        [DataMember(Order = 17, Name = "actions")]
        public string Actions { get; set; }
    }
}
