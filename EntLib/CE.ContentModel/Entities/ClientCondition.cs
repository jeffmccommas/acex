﻿using System;
using System.Runtime.Serialization;

namespace CE.ContentModel.Entities
{
    [DataContract]
    [Serializable]
    public class ClientCondition
    {
        [DataMember(Order = 0, Name = "key")]
        public string Key { get; set; }
        [DataMember(Order = 1, Name = "category")]
        public string Category { get; set; }
        [DataMember(Order = 2, Name = "description")]
        public string Description { get; set; }
        [DataMember(Order = 3, Name = "profileattributekey")]
        public string ProfileAttributeKey { get; set; }
        [DataMember(Order = 4, Name = "operator")]
        public string @Operator { get; set; }
        [DataMember(Order = 5, Name = "value")]
        public string @Value { get; set; }
        [DataMember(Order = 6, Name = "profileoptionkey")]
        public string ProfileOptionKey { get; set; }
    }
}
