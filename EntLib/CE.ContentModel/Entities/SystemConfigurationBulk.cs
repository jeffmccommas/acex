﻿using System;
using System.Diagnostics.CodeAnalysis;
using System.Runtime.Serialization;

namespace CE.ContentModel.Entities
{
    /// <summary>
    /// Class that contains system configuration information (application modules, menu hierarchy of each module, etc.).
    /// </summary>
    [DataContract]
    [Serializable]
    public class PortalModuleSystemConfigurationBulk
    {
        [DataMember(Order = 0, Name = "key")]
        public string Key { get; set; }

        [DataMember(Order = 1, Name = "category")]
        public string Category { get; set; }

        [DataMember(Order = 2, Name = "jsonconfiguration")]
        public string JsonValue { get; set; }
    }
}
