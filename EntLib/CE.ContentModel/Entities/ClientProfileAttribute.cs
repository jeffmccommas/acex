﻿using System;
using System.Runtime.Serialization;

namespace CE.ContentModel.Entities
{
    [DataContract]
    [Serializable]
    public class ClientProfileAttribute
    {
        [DataMember(Order = 0, Name = "key")]
        public string Key { get; set; }
        [DataMember(Order = 1, Name = "minvalue")]
        public int? MinValue { get; set; }
        [DataMember(Order = 2, Name = "maxvalue")]
        public int? MaxValue { get; set; }
        [DataMember(Order = 3, Name = "donotdefault")]
        public bool DoNotDefault { get; set; }
        [DataMember(Order = 4, Name = "disableconditionkeys")]
        public string DisableConditionKeys { get; set; }
        [DataMember(Order = 5, Name = "optionkeys")]
        public string OptionKeys { get; set; }
        [DataMember(Order = 6, Name = "type")]
        public string @Type { get; set; }
        [DataMember(Order = 7, Name = "entitylevel")]
        public string EntityLevel { get; set; }
        [DataMember(Order = 8, Name = "questiontextkey")]
        public string QuestionTextKey { get; set; }
    }
}
