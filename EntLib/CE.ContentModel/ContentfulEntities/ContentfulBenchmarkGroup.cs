﻿using System.Runtime.Serialization;

namespace CE.ContentModel.ContentfulEntities
{

    [DataContract]
    public class ContentfulBenchmarkGroup
    {
        [DataMember(Order = 0, Name = "fields", EmitDefaultValue = false)]
        public BenchmarkGroupFields Fields { get; set; }
    }

    [DataContract]
    public class BenchmarkGroupFields
    {
        [DataMember(Order = 0, Name = "key", EmitDefaultValue = false)]
        public EnglishText BenchmarkGroupKey { get; set; }

        [DataMember(Order = 1, Name = "namekey", EmitDefaultValue = false)]
        public EnglishEntryLink NameKey { get; set; }

        [DataMember(Order = 2, Name = "descriptionkey", EmitDefaultValue = false)]
        public EnglishEntryLink DescriptionKey { get; set; }

        [DataMember(Order = 3, Name = "disable", EmitDefaultValue = false)]
        public EnglishBoolean Disable { get; set; }

        [DataMember(Order = 4, Name = "hide", EmitDefaultValue = false)]
        public EnglishBoolean Hide { get; set; }

    }
}
