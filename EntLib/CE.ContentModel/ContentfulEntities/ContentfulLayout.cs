﻿using System.Runtime.Serialization;

namespace CE.ContentModel.ContentfulEntities
{

    [DataContract]
    public class ContentfulLayout
    {
        [DataMember(Order = 0, Name = "fields", EmitDefaultValue = false)]
        public LayoutFields Fields { get; set; }
    }

    [DataContract]
    public class LayoutFields
    {
        [DataMember(Order = 0, Name = "key", EmitDefaultValue = false)]
        public EnglishText LayoutKey { get; set; }

        [DataMember(Order = 1, Name = "description", EmitDefaultValue = false)]
        public EnglishText Description { get; set; }

        [DataMember(Order = 2, Name = "layoutjson", EmitDefaultValue = false)]
        public EnglishObject LayoutJson { get; set; }
    }
}
