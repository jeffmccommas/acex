﻿using System.Runtime.Serialization;

namespace CE.ContentModel.ContentfulEntities
{
    [DataContract]
    public class ContentfulProfileAttribute
    {
        [DataMember(Order = 0, Name = "fields", EmitDefaultValue = false)]
        public ProfileAttributeFields Fields { get; set; }
    }

    [DataContract]
    public class ProfileAttributeFields
    {
        [DataMember(Order = 0, Name = "key", EmitDefaultValue = false)]
        public EnglishText ProfileAttributeKey { get; set; }

        [DataMember(Order = 1, Name = "description", EmitDefaultValue = false)]
        public EnglishText Description { get; set; }

        [DataMember(Order = 2, Name = "profileattributetypekey", EmitDefaultValue = false)]
        public EnglishText ProfileAttributeTypeKey { get; set; }

        [DataMember(Order = 3, Name = "profileoptions", EmitDefaultValue = false)]
        public EnglishEntryLinks ProfileOptions { get; set; }

        [DataMember(Order = 4, Name = "disableconditions", EmitDefaultValue = false)]
        public EnglishEntryLinks DisableConditions { get; set; }

        [DataMember(Order = 5, Name = "entitylevelkey", EmitDefaultValue = false)]
        public EnglishText EntityLevelKey { get; set; }

        [DataMember(Order = 1, Name = "questiontextkey", EmitDefaultValue = false)]
        public EnglishEntryLink QuestionTextKey { get; set; }

        [DataMember(Order = 6, Name = "minvalue", EmitDefaultValue = false)]
        public EnglishNumber MinValue { get; set; }

        [DataMember(Order = 7, Name = "maxvalue", EmitDefaultValue = false)]
        public EnglishNumber MaxValue { get; set; }

        [DataMember(Order = 8, Name = "donotdefault", EmitDefaultValue = false)]
        public EnglishBoolean DoNotDefault { get; set; }

        [DataMember(Order = 9, Name = "disable", EmitDefaultValue = false)]
        public EnglishBoolean Disable { get; set; }
    }
}
