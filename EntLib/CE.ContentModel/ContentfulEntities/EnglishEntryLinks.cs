﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace CE.ContentModel.ContentfulEntities
{
    [DataContract]
    public class EnglishEntryLinks
    {
        [DataMember(Order = 0, Name = "en-US")]
        public List<EntryLink> EnUs { get; set; }
    }

    [DataContract]

    public class EntryLink
    {
        [DataMember(Order = 0, Name = "sys")]
        public EntryLinkAttributes Link { get; set; }
    }

    [DataContract]
    public class EntryLinkAttributes
    {
        private const string StringEntry = "Entry";
        private const string StringLink = "Link";

        [DataMember(Order = 0, Name = "type")]
        public string Type { get { return StringLink; } }
        [DataMember(Order = 1, Name = "linkType")]
        public string LinkType { get { return StringEntry; } }
        [DataMember(Order = 2, Name = "id")]
        public string Id { get; set; }
    }
}
