﻿using System.Runtime.Serialization;

namespace CE.ContentModel.ContentfulEntities
{
    [DataContract]
    public class ContentfulProfileDefault
    {
        [DataMember(Order = 0, Name = "fields", EmitDefaultValue = false)]
        public ProfileDefaultFields Fields { get; set; }
    }

    [DataContract]
    public class ProfileDefaultFields
    {
        [DataMember(Order = 0, Name = "key", EmitDefaultValue = false)]
        public EnglishText ProfileDefaultKey { get; set; }

        [DataMember(Order = 0, Name = "profiledefaultcollectionkey", EmitDefaultValue = false)]
        public EnglishEntryLink ProfileDefaultCollectionKey { get; set; }

        [DataMember(Order = 0, Name = "profileattributekey", EmitDefaultValue = false)]
        public EnglishEntryLink ProfileAttributeKey { get; set; }

        [DataMember(Order = 0, Name = "defaultkey", EmitDefaultValue = false)]
        public EnglishEntryLink DefaultKey { get; set; }

        [DataMember(Order = 0, Name = "defaultvalue", EmitDefaultValue = false)]
        public EnglishText DefaultValue { get; set; }
    }
}
