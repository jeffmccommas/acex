﻿using System.Runtime.Serialization;

namespace CE.ContentModel.ContentfulEntities
{

    [DataContract]
    public class ContentfulSeason
    {
        [DataMember(Order = 0, Name = "fields", EmitDefaultValue = false)]
        public SeasonFields Fields { get; set; }
    }

    [DataContract]
    public class SeasonFields
    {
        [DataMember(Order = 0, Name = "key", EmitDefaultValue = false)]
        public EnglishText SeasonKey { get; set; }

        [DataMember(Order = 1, Name = "namekey", EmitDefaultValue = false)]
        public EnglishEntryLink NameKey { get; set; }

        [DataMember(Order = 2, Name = "startmonth", EmitDefaultValue = false)]
        public EnglishNumber StartMonth { get; set; }

        [DataMember(Order = 3, Name = "startday", EmitDefaultValue = false)]
        public EnglishNumber StartDay { get; set; }

        [DataMember(Order = 4, Name = "endmonth", EmitDefaultValue = false)]
        public EnglishNumber EndMonth { get; set; }

        [DataMember(Order = 5, Name = "endday", EmitDefaultValue = false)]
        public EnglishNumber EndDay { get; set; }


    }
}
