﻿using System.Runtime.Serialization;

namespace CE.ContentModel.ContentfulEntities
{

    [DataContract]
    public class ContentfulEnumeration
    {
        [DataMember(Order = 0, Name = "fields", EmitDefaultValue = false)]
        public EnumerationFields Fields { get; set; }
    }

    [DataContract]
    public class EnumerationFields
    {
        [DataMember(Order = 0, Name = "key", EmitDefaultValue = false)]
        public EnglishText EnumerationKey { get; set; }

        [DataMember(Order = 1, Name = "category", EmitDefaultValue = false)]
        public EnglishText Category { get; set; }

        [DataMember(Order = 2, Name = "enumerationitems", EmitDefaultValue = false)]
        public EnglishEntryLinks EnumerationItems { get; set; }

        [DataMember(Order = 3, Name = "defaultenumerationitem", EmitDefaultValue = false)]
        public EnglishEntryLink DefaultEnumerationItem { get; set; }
    }
}
