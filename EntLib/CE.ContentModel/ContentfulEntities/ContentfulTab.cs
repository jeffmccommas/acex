﻿using System.Runtime.Serialization;

namespace CE.ContentModel.ContentfulEntities
{

    [DataContract]
    public class ContentfulTab
    {
        [DataMember(Order = 0, Name = "fields", EmitDefaultValue = false)]
        public TabFields Fields { get; set; }
    }

    [DataContract]
    public class TabFields
    {
        [DataMember(Order = 0, Name = "key", EmitDefaultValue = false)]
        public EnglishText TabKey { get; set; }

        [DataMember(Order = 1, Name = "name", EmitDefaultValue = false)]
        public EnglishText Name { get; set; }

        [DataMember(Order = 2, Name = "menuorder", EmitDefaultValue = false)]
        public EnglishNumber MenuOrder { get; set; }

        [DataMember(Order = 3, Name = "layoutkey", EmitDefaultValue = false)]
        public EnglishEntryLink LayoutKey { get; set; }

        [DataMember(Order = 4, Name = "type", EmitDefaultValue = false)]
        public EnglishText TabType { get; set; }

        [DataMember(Order = 5, Name = "namekey", EmitDefaultValue = false)]
        public EnglishEntryLink NameKey { get; set; }

        [DataMember(Order = 6, Name = "url", EmitDefaultValue = false)]
        public EnglishText Url { get; set; }

        [DataMember(Order = 7, Name = "conditionkeys", EmitDefaultValue = false)]
        public EnglishEntryLinks Conditions { get; set; }

        [DataMember(Order = 8, Name = "configurationkeys", EmitDefaultValue = false)]
        public EnglishEntryLinks Configurations { get; set; }

        [DataMember(Order = 9, Name = "textcontentkeys", EmitDefaultValue = false)]
        public EnglishEntryLinks TextContentKeys { get; set; }

        [DataMember(Order = 10, Name = "profilesections")]
        public EnglishEntryLinks ProfileSections { get; set; }

        [DataMember(Order = 11, Name = "childtabs")]
        public EnglishEntryLinks ChildTabs { get; set; }

        [DataMember(Order = 12, Name = "widgetkey")]
        public EnglishEntryLink WidgetKey { get; set; }

        [DataMember(Order = 13, Name = "tabicontype", EmitDefaultValue = false)]
        public EnglishEntryLink TabIconType { get; set; }

        [DataMember(Order = 14, Name = "tabiconclass", EmitDefaultValue = false)]
        public EnglishText TabIconClass { get; set; }

        [DataMember(Order = 15, Name = "images", EmitDefaultValue = false)]
        public EnglishEntryLink Images { get; set; }

        [DataMember(Order = 16, Name = "actions", EmitDefaultValue = false)]
        public EnglishEntryLinks Actions { get; set; }

    }
}
