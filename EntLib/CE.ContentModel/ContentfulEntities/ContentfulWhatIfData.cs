﻿using System.Runtime.Serialization;

namespace CE.ContentModel.ContentfulEntities
{
    [DataContract]
    public class ContentfulWhatIfData
    {
        [DataMember(Order = 0, Name = "fields", EmitDefaultValue = false)]
        public WhatIfDataFields Fields { get; set; }
    }

    [DataContract]
    public class WhatIfDataFields
    {
        [DataMember(Order = 0, Name = "key", EmitDefaultValue = false)]
        public EnglishText WhatIfDataKey { get; set; }

        [DataMember(Order = 1, Name = "profileattributekey", EmitDefaultValue = false)]
        public EnglishEntryLink ProfileAttributeKey { get; set; }

        [DataMember(Order = 2, Name = "valueexpressionkey", EmitDefaultValue = false)]
        public EnglishEntryLink ValueExpressionKey { get; set; }

    }
}
