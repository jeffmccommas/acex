﻿using System.Runtime.Serialization;

namespace CE.ContentModel.ContentfulEntities
{

    [DataContract]
    public class ContentfulCommodity
    {
        [DataMember(Order = 0, Name = "fields", EmitDefaultValue = false)]
        public CommodityFields Fields { get; set; }
    }

    [DataContract]
    public class CommodityFields
    {
        [DataMember(Order = 0, Name = "key", EmitDefaultValue = false)]
        public EnglishText CommodityKey { get; set; }

        [DataMember(Order = 1, Name = "namekey", EmitDefaultValue = false)]
        public EnglishEntryLink NameKey { get; set; }

    }
}
