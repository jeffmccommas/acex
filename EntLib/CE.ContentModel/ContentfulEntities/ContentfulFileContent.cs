﻿using System.Runtime.Serialization;

namespace CE.ContentModel.ContentfulEntities
{
    [DataContract]
    public class ContentfulFileContent
    {
        [DataMember(Order = 0, Name = "fields", EmitDefaultValue = false)]
        public FileContentFields Fields { get; set; }
    }

    [DataContract]
    public class FileContentFields
    {
        [DataMember(Order = 0, Name = "key", EmitDefaultValue = false)]
        public EnglishText FileContentKey { get; set; }

        [DataMember(Order = 1, Name = "type", EmitDefaultValue = false)]
        public EnglishText Type { get; set; }

        [DataMember(Order = 2, Name = "category", EmitDefaultValue = false)]
        public EnglishText Category { get; set; }

        [DataMember(Order = 3, Name = "smallfile", EmitDefaultValue = false)]
        public EnglishAssetLink SmallFile { get; set; }

        [DataMember(Order = 4, Name = "mediumfile", EmitDefaultValue = false)]
        public EnglishAssetLink MediumFile { get; set; }

        [DataMember(Order = 5, Name = "largefile", EmitDefaultValue = false)]
        public EnglishAssetLink LargeFile { get; set; }
    }
}
