﻿using System.Runtime.Serialization;

namespace CE.ContentModel.ContentfulEntities
{

    [DataContract]
    public class ContentfulWidget
    {
        [DataMember(Order = 0, Name = "fields", EmitDefaultValue = false)]
        public WidgetFields Fields { get; set; }
    }

    [DataContract]
    public class WidgetFields
    {
        [DataMember(Order = 0, Name = "key", EmitDefaultValue = false)]
        public EnglishText WidgetKey { get; set; }

        [DataMember(Order = 1, Name = "name", EmitDefaultValue = false)]
        public EnglishText Name { get; set; }

        [DataMember(Order = 3, Name = "tabkey", EmitDefaultValue = false)]
        public EnglishEntryLink TabKey { get; set; }

        [DataMember(Order = 4, Name = "widgettypekey", EmitDefaultValue = false)]
        public EnglishText WidgetTypeKey { get; set; }

        [DataMember(Order = 5, Name = "widgetcolumn", EmitDefaultValue = false)]
        public EnglishNumber Column { get; set; }

        [DataMember(Order = 6, Name = "widgetrow", EmitDefaultValue = false)]
        public EnglishNumber Row { get; set; }

        [DataMember(Order = 7, Name = "widgetorder", EmitDefaultValue = false)]
        public EnglishNumber Order { get; set; }

        [DataMember(Order = 8, Name = "conditionkeys", EmitDefaultValue = false)]
        public EnglishEntryLinks Conditions { get; set; }

        [DataMember(Order = 9, Name = "configurationkeys", EmitDefaultValue = false)]
        public EnglishEntryLinks Configurations { get; set; }

        [DataMember(Order = 10, Name = "textcontentkeys", EmitDefaultValue = false)]
        public EnglishEntryLinks TextContentKeys { get; set; }

        [DataMember(Order = 11, Name = "introtextkey", EmitDefaultValue = false)]
        public EnglishEntryLink IntroTextKey { get; set; }

    }
}
