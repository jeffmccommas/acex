﻿using System.Runtime.Serialization;
using Newtonsoft.Json.Linq;

namespace CE.ContentModel.ContentfulEntities
{
    [DataContract]
    public class EnglishObject
    {
        [DataMember(Order = 0, Name = "en-US")]
        public JObject EnUs { get; set; }
    }
}
