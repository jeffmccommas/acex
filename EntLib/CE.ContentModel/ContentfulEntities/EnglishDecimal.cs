﻿using System.Runtime.Serialization;

namespace CE.ContentModel.ContentfulEntities
{
    [DataContract]
    public class EnglishDecimal
    {
        [DataMember(Order = 0, Name = "en-US")]
        public decimal EnUs { get; set; }
    }
}
