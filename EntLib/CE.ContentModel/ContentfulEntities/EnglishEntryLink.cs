﻿using System.Runtime.Serialization;

namespace CE.ContentModel.ContentfulEntities
{
    [DataContract]
    public class EnglishEntryLink
    {
        [DataMember(Order = 0, Name = "en-US")]
        public EntryLink EnUs { get; set; }
    }


}
