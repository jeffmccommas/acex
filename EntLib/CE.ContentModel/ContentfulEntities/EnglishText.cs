﻿using System.Runtime.Serialization;

namespace CE.ContentModel.ContentfulEntities
{
    [DataContract]
    public class EnglishText
    {
        [DataMember(Order = 0, Name = "en-US")]
        public string EnUs { get; set; }
    }
}
