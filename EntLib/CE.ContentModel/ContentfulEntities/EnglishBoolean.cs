﻿using System.Runtime.Serialization;

namespace CE.ContentModel.ContentfulEntities
{
    [DataContract]
    public class EnglishBoolean
    {
        [DataMember(Order = 0, Name = "en-US")]
        public bool EnUs { get; set; }
    }
}
