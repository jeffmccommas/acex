﻿using System.Runtime.Serialization;

namespace CE.ContentModel.ContentfulEntities
{

    [DataContract]
    public class ContentfulMeasurement
    {
        [DataMember(Order = 0, Name = "fields", EmitDefaultValue = false)]
        public MeasurementFields Fields { get; set; }
    }

    [DataContract]
    public class MeasurementFields
    {
        [DataMember(Order = 0, Name = "key", EmitDefaultValue = false)]
        public EnglishText MeasurementKey { get; set; }

        [DataMember(Order = 1, Name = "namekey", EmitDefaultValue = false)]
        public EnglishEntryLink NameKey { get; set; }

        [DataMember(Order = 2, Name = "calculation", EmitDefaultValue = false)]
        public EnglishText Calculation { get; set; }

        [DataMember(Order = 3, Name = "comments", EmitDefaultValue = false)]
        public EnglishText Comments { get; set; }

    }
}
