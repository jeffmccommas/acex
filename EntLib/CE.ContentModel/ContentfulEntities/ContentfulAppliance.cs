﻿using System.Runtime.Serialization;

namespace CE.ContentModel.ContentfulEntities
{

    [DataContract]
    public class ContentfulAppliance
    {
        [DataMember(Order = 0, Name = "fields", EmitDefaultValue = false)]
        public ApplianceFields Fields { get; set; }
    }

    [DataContract]
    public class ApplianceFields
    {
        [DataMember(Order = 0, Name = "key", EmitDefaultValue = false)]
        public EnglishText ApplianceKey { get; set; }

        [DataMember(Order = 1, Name = "namekey", EmitDefaultValue = false)]
        public EnglishEntryLink NameKey { get; set; }

        [DataMember(Order = 2, Name = "averagecostperyear", EmitDefaultValue = false)]
        public EnglishDecimal AverageCostPerYear { get; set; }

        [DataMember(Order = 3, Name = "profileattributes", EmitDefaultValue = false)]
        public EnglishEntryLinks ProfileAttributes { get; set; }

        [DataMember(Order = 4, Name = "expressions", EmitDefaultValue = false)]
        public EnglishEntryLinks Expressions { get; set; }

    }
}
