﻿using System.Runtime.Serialization;

namespace CE.ContentModel.ContentfulEntities
{
    [DataContract]
    public class ContentfulProfileSection
    {
        [DataMember(Order = 0, Name = "fields", EmitDefaultValue = false)]
        public ProfileSectionFields Fields { get; set; }
    }

    [DataContract]
    public class ProfileSectionFields
    {
        [DataMember(Order = 0, Name = "key", EmitDefaultValue = false)]
        public EnglishText ProfileSectionKey     { get; set; }
        [DataMember(Order = 1, Name = "name", EmitDefaultValue = false)]
        public EnglishText Name { get; set; }
        [DataMember(Order = 2, Name = "conditions", EmitDefaultValue = false)]
        public EnglishEntryLinks ConditionKeys { get; set; }
        [DataMember(Order = 3, Name = "rank", EmitDefaultValue = false)]
        public EnglishNumber Rank { get; set; }
        [DataMember(Order = 4, Name = "titlekey", EmitDefaultValue = false)]
        public EnglishEntryLink TitleKey { get; set; }
        [DataMember(Order = 5, Name = "subtitlekey", EmitDefaultValue = false)]
        public EnglishEntryLink SubTitleKey { get; set; }
        [DataMember(Order = 6, Name = "descriptionkey", EmitDefaultValue = false)]
        public EnglishEntryLink DescriptionKey { get; set; }
        [DataMember(Order = 7, Name = "profileattributes", EmitDefaultValue = false)]
        public EnglishEntryLinks ProfileAttributes { get; set; }
        [DataMember(Order = 8, Name = "images", EmitDefaultValue = false)]
        public EnglishEntryLink Images { get; set; }
        [DataMember(Order = 9, Name = "introtextkey", EmitDefaultValue = false)]
        public EnglishEntryLink IntroTextKey { get; set; }
    }
}
