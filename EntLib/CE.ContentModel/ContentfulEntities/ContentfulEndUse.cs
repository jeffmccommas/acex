﻿using System.Runtime.Serialization;

namespace CE.ContentModel.ContentfulEntities
{

    [DataContract]
    public class ContentfulEndUse
    {
        [DataMember(Order = 0, Name = "fields", EmitDefaultValue = false)]
        public EndUseFields Fields { get; set; }
    }

    [DataContract]
    public class EndUseFields
    {
        [DataMember(Order = 0, Name = "key", EmitDefaultValue = false)]
        public EnglishText EndUseKey { get; set; }

        [DataMember(Order = 1, Name = "namekey", EmitDefaultValue = false)]
        public EnglishEntryLink NameKey { get; set; }

        [DataMember(Order = 2, Name = "endusecategorykey", EmitDefaultValue = false)]
        public EnglishText EndUseCategoryKey { get; set; }

        [DataMember(Order = 3, Name = "disable", EmitDefaultValue = false)]
        public EnglishBoolean Disable { get; set; }

        [DataMember(Order = 4, Name = "hide", EmitDefaultValue = false)]
        public EnglishBoolean Hide { get; set; }

        [DataMember(Order = 5, Name = "appliances", EmitDefaultValue = false)]
        public EnglishEntryLinks Appliances { get; set; }

    }
}
