﻿using System.Runtime.Serialization;

namespace CE.ContentModel.ContentfulEntities
{

    [DataContract]
    public class ContentfulUom
    {
        [DataMember(Order = 0, Name = "fields", EmitDefaultValue = false)]
        public UomFields Fields { get; set; }
    }

    [DataContract]
    public class UomFields
    {
        [DataMember(Order = 0, Name = "key", EmitDefaultValue = false)]
        public EnglishText UomKey { get; set; }

        [DataMember(Order = 1, Name = "namekey", EmitDefaultValue = false)]
        public EnglishEntryLink NameKey { get; set; }

    }
}
