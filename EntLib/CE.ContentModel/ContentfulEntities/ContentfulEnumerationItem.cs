﻿using System.Runtime.Serialization;

namespace CE.ContentModel.ContentfulEntities
{

    [DataContract]
    public class ContentfulEnumerationItem
    {
        [DataMember(Order = 0, Name = "fields", EmitDefaultValue = false)]
        public EnumerationItemFields Fields { get; set; }
    }

    [DataContract]
    public class EnumerationItemFields
    {
        [DataMember(Order = 0, Name = "key", EmitDefaultValue = false)]
        public EnglishText EnumerationItemKey { get; set; }

        [DataMember(Order = 1, Name = "namekey", EmitDefaultValue = false)]
        public EnglishEntryLink NameKey { get; set; }

    }
}
