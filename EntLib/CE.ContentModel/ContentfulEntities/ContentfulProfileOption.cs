﻿using System.Runtime.Serialization;

namespace CE.ContentModel.ContentfulEntities
{
    [DataContract]
    public class ContentfulProfileOption
    {
        [DataMember(Order = 0, Name = "fields", EmitDefaultValue = false)]
        public ProfileOptionFields Fields { get; set; }
    }

    [DataContract]
    public class ProfileOptionFields
    {
        [DataMember(Order = 0, Name = "key", EmitDefaultValue = false)]
        public EnglishText ProfileOptionKey { get; set; }

        [DataMember(Order = 1, Name = "namekey", EmitDefaultValue = false)]
        public EnglishEntryLink NameKey { get; set; }

        [DataMember(Order = 2, Name = "description", EmitDefaultValue = false)]
        public EnglishText Description { get; set; }

        [DataMember(Order = 3, Name = "profileoptionvalue", EmitDefaultValue = false)]
        public EnglishDecimal ProfileOptionValue { get; set; }
    }
}
