﻿using System.Runtime.Serialization;

namespace CE.ContentModel.ContentfulEntities
{

    [DataContract]
    public class ContentfulAction
    {
        [DataMember(Order = 0, Name = "fields", EmitDefaultValue = false)]
        public ActionFields Fields { get; set; }
    }

    [DataContract]
    public class ActionFields
    {
        [DataMember(Order = 0, Name = "key", EmitDefaultValue = false)]
        public EnglishText ActionKey { get; set; }
        [DataMember(Order = 1, Name = "namekey", EmitDefaultValue = false)]
        public EnglishEntryLink NameKey { get; set; }
        [DataMember(Order = 2, Name = "conditionkeys", EmitDefaultValue = false)]
        public EnglishEntryLinks ConditionKeys { get; set; }
        [DataMember(Order = 3, Name = "descriptionkey", EmitDefaultValue = false)]
        public EnglishEntryLink DescriptionKey { get; set; }
        [DataMember(Order = 4, Name = "disable", EmitDefaultValue = false)]
        public EnglishBoolean Disable { get; set; }
        [DataMember(Order = 5, Name = "hide", EmitDefaultValue = false)]
        public EnglishBoolean Hide { get; set; }
        [DataMember(Order = 6, Name = "videokey", EmitDefaultValue = false)]
        public EnglishEntryLink VideoKey { get; set; }
        [DataMember(Order = 7, Name = "imagekey", EmitDefaultValue = false)]
        public EnglishEntryLink ImageKey { get; set; }
        [DataMember(Order = 8, Name = "iconclass", EmitDefaultValue = false)]
        public EnglishText IconClass { get; set; }
        [DataMember(Order = 9, Name = "commoditykey", EmitDefaultValue = false)]
        public EnglishEntryLink CommodityKey { get; set; }
        [DataMember(Order = 10, Name = "actiontypekey", EmitDefaultValue = false)]
        public EnglishText ActionTypeKey { get; set; }
        [DataMember(Order = 11, Name = "appliancekeys", EmitDefaultValue = false)]
        public EnglishEntryLinks ApplianceKeys { get; set; }
        [DataMember(Order = 12, Name = "upfrontcost", EmitDefaultValue = false)]
        public EnglishDecimal UpfrontCost { get; set; }
        [DataMember(Order = 13, Name = "annualcost", EmitDefaultValue = false)]
        public EnglishDecimal AnnualCost { get; set; }
        [DataMember(Order = 14, Name = "costvariancepercent", EmitDefaultValue = false)]
        public EnglishDecimal CostVariancePercent { get; set; }
        [DataMember(Order = 15, Name = "difficulty", EmitDefaultValue = false)]
        public EnglishText Difficulty { get; set; }
        [DataMember(Order = 16, Name = "habitinterval", EmitDefaultValue = false)]
        public EnglishText HabitInterval { get; set; }
        [DataMember(Order = 17, Name = "defaultannualsavingsestimate", EmitDefaultValue = false)]
        public EnglishDecimal DefaultAnnualSavingsEstimate { get; set; }
        [DataMember(Order = 18, Name = "defaultannualusagesavingsestimate", EmitDefaultValue = false)]
        public EnglishDecimal DefaultAnnualUsageSavingsEstimate { get; set; }
        [DataMember(Order = 19, Name = "annualusagesavingsuomkey", EmitDefaultValue = false)]
        public EnglishEntryLink AnnualUsageSavingsUomKey { get; set; }
        [DataMember(Order = 20, Name = "defaultpaybacktime", EmitDefaultValue = false)]
        public EnglishNumber DefaultPaybackTime { get; set; }
        [DataMember(Order = 21, Name = "defaultroi", EmitDefaultValue = false)]
        public EnglishDecimal DefaultRoi { get; set; }
        [DataMember(Order = 22, Name = "rebateavailable", EmitDefaultValue = false)]
        public EnglishBoolean RebateAvailable { get; set; }
        [DataMember(Order = 23, Name = "rebateurl", EmitDefaultValue = false)]
        public EnglishText RebateUrl { get; set; }
        [DataMember(Order = 24, Name = "rebateimagekey", EmitDefaultValue = false)]
        public EnglishEntryLink RebateImageKey { get; set; }
        [DataMember(Order = 25, Name = "rebateamount", EmitDefaultValue = false)]
        public EnglishDecimal RebateAmount { get; set; }
        [DataMember(Order = 26, Name = "seasonkeys", EmitDefaultValue = false)]
        public EnglishEntryLinks SeasonKeys { get; set; }
        [DataMember(Order = 27, Name = "comments", EmitDefaultValue = false)]
        public EnglishText Comments { get; set; }
        [DataMember(Order = 28, Name = "actionpriority", EmitDefaultValue = false)]
        public EnglishNumber ActionPriority { get; set; }
        [DataMember(Order = 29, Name = "savingscalcmethod", EmitDefaultValue = false)]
        public EnglishText SavingsCalcMethod { get; set; }
        [DataMember(Order = 30, Name = "savingsamount", EmitDefaultValue = false)]
        public EnglishDecimal SavingsAmount { get; set; }
        [DataMember(Order = 31, Name = "whatifdata", EmitDefaultValue = false)]
        public EnglishEntryLinks WhatIfDataKeys { get; set; }
        [DataMember(Order = 32, Name = "costexpression", EmitDefaultValue = false)]
        public EnglishEntryLink CostExpression { get; set; }
        [DataMember(Order = 33, Name = "rebateexpression", EmitDefaultValue = false)]
        public EnglishEntryLink RebateExpression { get; set; }
        [DataMember(Order = 34, Name = "nextsteplink", EmitDefaultValue = false)]
        public EnglishText NextStepLink { get; set; }
        [DataMember(Order = 35, Name = "nextsteplinktext", EmitDefaultValue = false)]
        public EnglishEntryLink NextStepLinkText { get; set; }
        [DataMember(Order = 36, Name = "nextsteplinktype", EmitDefaultValue = false)]
        public EnglishText NextStepLinkType { get; set; }
        [DataMember(Order = 37, Name = "tags", EmitDefaultValue = false)]
        public EnglishText Tags { get; set; }
        [DataMember(Order = 38, Name = "minimumsavingsamt", EmitDefaultValue = false)]
        public EnglishDecimal MinimumSavingsAmt { get; set; }
        [DataMember(Order = 39, Name = "maximumsavingsamt", EmitDefaultValue = false)]
        public EnglishDecimal MaximumSavingsAmt { get; set; }
    }
}
