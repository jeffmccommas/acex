﻿using System.Runtime.Serialization;

namespace CE.ContentModel.ContentfulEntities
{

    [DataContract]
    public class ContentfulCurrency
    {
        [DataMember(Order = 0, Name = "fields", EmitDefaultValue = false)]
        public CurrencyFields Fields { get; set; }
    }

    [DataContract]
    public class CurrencyFields
    {
        [DataMember(Order = 0, Name = "key", EmitDefaultValue = false)]
        public EnglishText CurrencyKey { get; set; }

        [DataMember(Order = 1, Name = "namekey", EmitDefaultValue = false)]
        public EnglishEntryLink NameKey { get; set; }

        [DataMember(Order = 2, Name = "symbolkey", EmitDefaultValue = false)]
        public EnglishEntryLink SymbolKey { get; set; }


    }
}
