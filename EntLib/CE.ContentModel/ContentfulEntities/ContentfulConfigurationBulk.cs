﻿using System.Runtime.Serialization;

namespace CE.ContentModel.ContentfulEntities
{
    [DataContract]
    public class ContentfulConfigurationBulk
    {
        [DataMember(Order = 0, Name = "fields", EmitDefaultValue = false)]
        public ConfigurationBulkFields Fields { get; set; }
    }

    [DataContract]
    public class ConfigurationBulkFields
    {
        [DataMember(Order = 0, Name = "key", EmitDefaultValue = false)]
        public EnglishText ConfigurationKey { get; set; }

        [DataMember(Order = 1, Name = "name", EmitDefaultValue = false)]
        public EnglishText Name { get; set; }

        [DataMember(Order = 2, Name = "jsonconfiguration", EmitDefaultValue = false)]
        public EnglishObject JsonConfiguation { get; set; }

        [DataMember(Order = 3, Name = "category", EmitDefaultValue = false)]
        public EnglishText CategoryKey { get; set; }

        [DataMember(Order = 4, Name = "description", EmitDefaultValue = false)]
        public EnglishText Description { get; set; }

        [DataMember(Order = 5, Name = "environment", EmitDefaultValue = false)]
        public EnglishText EnvironmentKey { get; set; }
    }
}
