﻿using System.Runtime.Serialization;

namespace CE.ContentModel.ContentfulEntities
{
    [DataContract]
    public class ContentfulCondition
    {
        [DataMember(Order = 0, Name = "fields", EmitDefaultValue = false)]
        public ConditionFields Fields { get; set; }
    }

    [DataContract]
    public class ConditionFields
    {
        [DataMember(Order = 0, Name = "key", EmitDefaultValue = false)]
        public EnglishText ConditionKey { get; set; }

        [DataMember(Order = 1, Name = "category", EmitDefaultValue = false)]
        public EnglishText CategoryKey { get; set; }

        [DataMember(Order = 2, Name = "description", EmitDefaultValue = false)]
        public EnglishText Description { get; set; }

        [DataMember(Order = 3, Name = "profileattributekey", EmitDefaultValue = false)]
        public EnglishEntryLink ProfileAttributeKey { get; set; }

        [DataMember(Order = 4, Name = "operator", EmitDefaultValue = false)]
        public EnglishText ConditionOperator { get; set; }

        [DataMember(Order = 5, Name = "value", EmitDefaultValue = false)]
        public EnglishText Value { get; set; }

        [DataMember(Order = 6, Name = "profileoptionkey", EmitDefaultValue = false)]
        public EnglishEntryLink ProfileOptionKey { get; set; }
    }
}
