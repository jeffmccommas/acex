﻿using System.Runtime.Serialization;

namespace CE.ContentModel.ContentfulEntities
{
    [DataContract]
    public class ContentfulAsset
    {
        [DataMember(Order = 0, Name = "fields", EmitDefaultValue = false)]
        public AssetFields Fields { get; set; }
    }

    [DataContract]
    public class AssetFields
    {
        [DataMember(Order = 0, Name = "title", EmitDefaultValue = false)]
        public AssetText Title { get; set; }

        [DataMember(Order = 1, Name = "description", EmitDefaultValue = false)]
        public AssetText Description { get; set; }

        [DataMember(Order = 2, Name = "file", EmitDefaultValue = false)]
        public AssetFile File { get; set; }

    }

    [DataContract]
    public class AssetText
    {
        [DataMember(Order = 0, Name = "en-US", EmitDefaultValue = false)]
        public string EnUs { get; set; }
        [DataMember(Order = 1, Name = "es-ES", EmitDefaultValue = false)]
        public string EsEs { get; set; }
        [DataMember(Order = 2, Name = "ru-RU", EmitDefaultValue = false)]
        public string RuRu { get; set; }
    }

    [DataContract]
    public class AssetFile
    {
        [DataMember(Order = 0, Name = "en-US", EmitDefaultValue = false)]
        public AssetFileInfo EnFile { get; set; }

        [DataMember(Order = 0, Name = "es-ES", EmitDefaultValue = false)]
        public AssetFileInfo EsFile { get; set; }

        [DataMember(Order = 0, Name = "ru-RU", EmitDefaultValue = false)]
        public AssetFileInfo RuFile { get; set; }

    }

    [DataContract]
    public class AssetFileInfo
    {
        [DataMember(Order = 0, Name = "contentType", EmitDefaultValue = false)]
        public string ContentType { get; set; }

        [DataMember(Order = 1, Name = "fileName", EmitDefaultValue = false)]
        public string FileName { get; set; }

        [DataMember(Order = 2, Name = "upload", EmitDefaultValue = false)]
        public string FileUrl { get; set; }

    }
    

}
