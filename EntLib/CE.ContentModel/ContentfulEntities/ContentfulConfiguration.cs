﻿using System.Runtime.Serialization;

namespace CE.ContentModel.ContentfulEntities
{
    [DataContract]
    public class ContentfulConfiguration
    {
        [DataMember(Order = 0, Name = "fields", EmitDefaultValue = false)]
        public ConfigurationFields Fields { get; set; }
    }

    [DataContract]
    public class ConfigurationFields
    {
        [DataMember(Order = 0, Name = "key", EmitDefaultValue = false)]
        public EnglishText ConfigurationKey { get; set; }

        [DataMember(Order = 1, Name = "category", EmitDefaultValue = false)]
        public EnglishText CategoryKey { get; set; }

        [DataMember(Order = 2, Name = "environment", EmitDefaultValue = false)]
        public EnglishText EnvironmentKey { get; set; }

        [DataMember(Order = 3, Name = "description", EmitDefaultValue = false)]
        public EnglishText Description { get; set; }

        [DataMember(Order = 4, Name = "value", EmitDefaultValue = false)]
        public EnglishText Value { get; set; }
    }
}
