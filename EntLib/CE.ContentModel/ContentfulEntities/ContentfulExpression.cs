﻿using System.Runtime.Serialization;

namespace CE.ContentModel.ContentfulEntities
{
    [DataContract]
    public class ContentfulExpression
    {
        [DataMember(Order = 0, Name = "fields", EmitDefaultValue = false)]
        public ExpressionFields Fields { get; set; }
    }

    [DataContract]
    public class ExpressionFields
    {
        [DataMember(Order = 0, Name = "key", EmitDefaultValue = false)]
        public EnglishText ExpressionKey { get; set; }

        [DataMember(Order = 1, Name = "name", EmitDefaultValue = false)]
        public EnglishText Name { get; set; }

        [DataMember(Order = 2, Name = "description", EmitDefaultValue = false)]
        public EnglishText Description { get; set; }

        [DataMember(Order = 3, Name = "formula", EmitDefaultValue = false)]
        public EnglishText Formula { get; set; }

        [DataMember(Order = 4, Name = "comments", EmitDefaultValue = false)]
        public EnglishText Comments { get; set; }
    }
}
