﻿using System.Runtime.Serialization;
namespace CE.ContentModel.ContentfulEntities
{
    [DataContract]
    public class ContentfulTextContent
    {
        [DataMember(Order = 0, Name = "fields", EmitDefaultValue = false)]
        public TextContentFields Fields { get; set; }
    }

    [DataContract]
    public class TextContentFields
    {
        [DataMember(Order = 0, Name = "key", EmitDefaultValue = false)]
        public EnglishText TextContentKey { get; set; }
        [DataMember(Order = 1, Name = "shorttext", EmitDefaultValue = false)]
        public TextContentText ShortText { get; set; }
        [DataMember(Order = 2, Name = "mediumtext", EmitDefaultValue = false)]
        public TextContentText MediumText { get; set; }
        [DataMember(Order = 3, Name = "longtext", EmitDefaultValue = false)]
        public TextContentText LongText { get; set; }
        [DataMember(Order = 4, Name = "category", EmitDefaultValue = false)]
        public EnglishText Category { get; set; }
    }

    [DataContract]
    public class TextContentText
    {
        [DataMember(Order = 0, Name = "en-US", EmitDefaultValue = false)]
        public string EnUs { get; set; }
        [DataMember(Order = 1, Name = "es-ES", EmitDefaultValue = false)]
        public string EsEs { get; set; }
        [DataMember(Order = 2, Name = "ru-RU", EmitDefaultValue = false)]
        public string RuRu { get; set; }
    }
}
