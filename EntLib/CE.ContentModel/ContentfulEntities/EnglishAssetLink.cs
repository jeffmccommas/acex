﻿using System.Runtime.Serialization;

namespace CE.ContentModel.ContentfulEntities
{
    [DataContract]
    public class EnglishAssetLink
    {
        [DataMember(Order = 0, Name = "en-US")]
        public AssetLink EnUs { get; set; }
    }

    [DataContract]

    public class AssetLink
    {
        [DataMember(Order = 0, Name = "sys")]
        public AssetLinkAttributes Link { get; set; }
    }

    [DataContract]
    public class AssetLinkAttributes
    {
        private const string StringAsset = "Asset";
        private const string StringLink = "Link";

        [DataMember(Order = 0, Name = "type")]
        public string Type { get { return StringLink; } }
        [DataMember(Order = 1, Name = "linkType")]
        public string LinkType { get { return StringAsset; } }
        [DataMember(Order = 2, Name = "id")]
        public string Id { get; set; }
    }

}
