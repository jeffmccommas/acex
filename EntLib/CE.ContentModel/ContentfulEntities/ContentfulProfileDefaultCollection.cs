﻿using System.Runtime.Serialization;

namespace CE.ContentModel.ContentfulEntities
{

    [DataContract]
    public class ContentfulProfileDefaultCollection
    {
        [DataMember(Order = 0, Name = "fields", EmitDefaultValue = false)]
        public ProfileDefaultCollectionFields Fields { get; set; }
    }

    [DataContract]
    public class ProfileDefaultCollectionFields
    {
        [DataMember(Order = 0, Name = "key", EmitDefaultValue = false)]
        public EnglishText ProfileDefaultCollectionKey { get; set; }

        [DataMember(Order = 1, Name = "namekey", EmitDefaultValue = false)]
        public EnglishEntryLink NameKey { get; set; }

        [DataMember(Order = 2, Name = "description", EmitDefaultValue = false)]
        public EnglishText Description { get; set; }

    }
}
