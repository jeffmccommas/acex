﻿using System.Data;
using AutoMapper;
using CE.ContentModel.Entities;

namespace CE.ContentModel
{
    public class AutoMapperBootStrapper
    {
        /// <summary>
        /// This is called one time to create mappings when application starts ideally.
        /// </summary>
        public void BootStrap()
        {
            Mapper.CreateMap<IDataReader, ClientAction>();
            Mapper.CreateMap<IDataReader, ClientActionDefault>();
            Mapper.CreateMap<IDataReader, ClientActionSaving>();
            Mapper.CreateMap<IDataReader, ClientAppliance>();
            Mapper.CreateMap<IDataReader, ClientBenchmarkGroup>();
            Mapper.CreateMap<IDataReader, ClientCommodity>();
            Mapper.CreateMap<IDataReader, ClientCondition>();
            Mapper.CreateMap<IDataReader, ClientConfiguration>();
            Mapper.CreateMap<IDataReader, ClientContent>();
            Mapper.CreateMap<IDataReader, ClientCurrency>();
            Mapper.CreateMap<IDataReader, ClientEndUse>();
            Mapper.CreateMap<IDataReader, ClientEnumeration>();
            Mapper.CreateMap<IDataReader, ClientEnumerationItem>();
            Mapper.CreateMap<IDataReader, ClientEnumerationList>();
            Mapper.CreateMap<IDataReader, ClientExpression>();
            Mapper.CreateMap<IDataReader, ClientFileContent>();
            Mapper.CreateMap<IDataReader, ClientLayout>();
            Mapper.CreateMap<IDataReader, ClientMeasurement>();
            Mapper.CreateMap<IDataReader, ClientProfileAttribute>();
            Mapper.CreateMap<IDataReader, ClientProfileAttributeDefault>();
            Mapper.CreateMap<IDataReader, ClientProfileDefault>();
            Mapper.CreateMap<IDataReader, ClientProfileDefaultCollection>();
            Mapper.CreateMap<IDataReader, ClientProfileOption>();
            Mapper.CreateMap<IDataReader, ClientProfileSection>();
            Mapper.CreateMap<IDataReader, ClientSeason>();
            Mapper.CreateMap<IDataReader, ClientTab>();
            Mapper.CreateMap<IDataReader, ClientTextContent>();
            Mapper.CreateMap<IDataReader, ClientUOM>();
            Mapper.CreateMap<IDataReader, ClientWhatIfData>();
            Mapper.CreateMap<IDataReader, ClientWidget>();
            Mapper.AssertConfigurationIsValid();
        }

    }


}
