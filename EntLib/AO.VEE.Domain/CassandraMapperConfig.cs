﻿using AO.Entities;
using Cassandra.Mapping;

namespace AO.VEE.Domain
{
	/// <summary>
	/// Cassandra mapping configuration are configured in this class
	/// </summary>
	public class CassandraMapperConfig : Mappings
	{
		/// <summary>
		/// Cassandra mapping configuration are configured in the constructor
		/// </summary>
		public CassandraMapperConfig()
		{
			//Note: Add Cassandra Mappings
			For<VeeConsumptionByMeterDomain>()
				.TableName(Constants.CassandraTableNames.VeeConsumptionByMeter)
				.Column(u => u.ClientId, tami => tami.WithName("client_id"))
				.Column(u => u.MeterId, tami => tami.WithName("meter_id"))
				.Column(u => u.AmiTimeStamp, tami => tami.WithName("ami_time_stamp"))
				.Column(u => u.AccountNumber, tami => tami.WithName("account_number"))
				.Column(u => u.CommodityId, tami => tami.WithName("commodity_id"))
				.Column(u => u.Consumption, tami => tami.WithName("consumption"))
				.Column(u => u.Direction, tami => tami.WithName("direction"))
				.Column(u => u.EditedConsumption, tami => tami.WithName("edited_consumption"))
				.Column(u => u.EstimatedConsumption, tami => tami.WithName("estimated_consumption"))
				.Column(u => u.StandardUOMConsumption, tami => tami.WithName("standard_uom_consumption"))
				.Column(u => u.RawConsumption, tami => tami.WithName("raw_consumption"))
				.Column(u => u.StandardUOMId, tami => tami.WithName("standard_uom_id"))
				.Column(u => u.EstimationMethod, tami => tami.WithName("estimation_method"))
				.Column(u => u.Timezone, tami => tami.WithName("time_zone"))
				.Column(u => u.TransponderId, tami => tami.WithName("transponder_id"))
				.Column(u => u.TransponderPort, tami => tami.WithName("transponder_port"))
				.Column(u => u.UOMId, tami => tami.WithName("uom_id"))
				.Column(u => u.IntervalType, tami => tami.WithName("interval_type"))
				.Column(u => u.IsApproved, tami => tami.WithName("is_approved"))
				.Column(u => u.IsEdited, tami => tami.WithName("is_edited"))
				.Column(u => u.IsEstimated, tami => tami.WithName("is_estimated"))
				.Column(u => u.IsMetered, tami => tami.WithName("is_metered"))
				.Column(u => u.IsMissing, tami => tami.WithName("is_missing"))
				.Column(u => u.IsNegative, tami => tami.WithName("is_negative"))
				.Column(u => u.IsSpike, tami => tami.WithName("is_spike"))
				.Column(u => u.IsStatic, tami => tami.WithName("is_static"))
				.Column(u => u.IsZero, tami => tami.WithName("is_zero"));

			For<VeeStagedClientMeterByDateDomain>()
				.TableName(Constants.CassandraTableNames.VeeStagedClientMeterByDate)
				.Column(u => u.ClientId, tami => tami.WithName("client_id"))
				.Column(u => u.MeterId, tami => tami.WithName("meter_id"))
				.Column(u => u.AmiDate, tami => tami.WithName("ami_date"));

			For<ConsumptionByMeterEntity>()
				.TableName(Constants.CassandraTableNames.ConsumptionByMeter)
				.Column(u => u.ClientId, tami => tami.WithName("client_id"))
				.Column(u => u.MeterId, tami => tami.WithName("meter_id"))
				.Column(u => u.AmiTimeStamp, tami => tami.WithName("ami_time_stamp"))
				.Column(u => u.AccountNumber, tami => tami.WithName("account_number"))
				.Column(u => u.BatteryVoltage, tami => tami.WithName("battery_voltage"))
				.Column(u => u.CommodityId, tami => tami.WithName("commodity_id"))
				.Column(u => u.Consumption, tami => tami.WithName("consumption"))
				.Column(u => u.DialRead, tami => tami.WithName("dial_read"))
				.Column(u => u.Direction, tami => tami.WithName("direction"))
				.Column(u => u.ProjectedReadDate, tami => tami.WithName("projected_read_date"))
				.Column(u => u.QualityCode, tami => tami.WithName("quality_code"))
				.Column(u => u.ReadingType, tami => tami.WithName("reading_type"))
				.Column(u => u.Scenario, tami => tami.WithName("scenario"))
				.Column(u => u.ServicePointId, tami => tami.WithName("service_point_id"))
				.Column(u => u.Timezone, tami => tami.WithName("time_zone"))
				.Column(u => u.TOUBin, tami => tami.WithName("toub_in"))
				.Column(u => u.TransponderId, tami => tami.WithName("transponder_id"))
				.Column(u => u.TransponderPort, tami => tami.WithName("transponder_port"))
				.Column(u => u.UOMId, tami => tami.WithName("uom_id"))
				.Column(u => u.VolumeFactor, tami => tami.WithName("volume_factor"))
				.Column(u => u.CustomerId, tami => tami.WithName("customer_id"))
				.Column(u => u.IntervalType, tami => tami.WithName("interval_type"))
				.Column(u => u.StandardUOMConsumption, cm => cm.WithName("standard_uom_consumption"))
				.Column(u => u.StandardUOMId, cm => cm.WithName("standard_uom_id"));

			For<MeterHistoryByMeterEntity>()
				.TableName(Constants.CassandraTableNames.MeterHistoryByMeter)
				.Column(e => e.ClientId, cm => cm.WithName("client_id"))
				.Column(e => e.MeterId, cm => cm.WithName("meter_id"))
				.Column(e => e.MeterInstallDate, cm => cm.WithName("meter_install_date"))
				.Column(e => e.AccountNumber, cm => cm.WithName("account_number"))
				.Column(e => e.Active, cm => cm.WithName("active"))
				.Column(e => e.CrossReferenceId, cm => cm.WithName("crossreference_id"))
				.Column(e => e.Latitude, cm => cm.WithName("latitude"))
				.Column(e => e.Longitude, cm => cm.WithName("longitude"))
				.Column(e => e.MeterSerialNumber, cm => cm.WithName("meter_serialnumber"))
				.Column(e => e.MeterTypeId, cm => cm.WithName("metertype_id"))
				.Column(e => e.MeterTypeDescripton, cm => cm.WithName("meter_type_description"))
				.Column(e => e.MtuId, cm => cm.WithName("mtu_id"))
				.Column(e => e.MtuInstallDate, cm => cm.WithName("mtu_install_date"))
				.Column(e => e.MtuTypeId, cm => cm.WithName("mtu_type_id"))
				.Column(e => e.MtuType, cm => cm.WithName("mtu_type"))
				.Column(e => e.MtuTypeDescription, cm => cm.WithName("mtu_type_description"));
		}
	}
}