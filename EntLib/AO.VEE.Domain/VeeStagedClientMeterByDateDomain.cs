﻿using System;

namespace AO.VEE.Domain
{
	/// <summary>
	/// VeeStagedClientMeterByDate Domain
	/// </summary>
	public class VeeStagedClientMeterByDateDomain
	{
		public int ClientId { get; set; }
		public string MeterId { get; set; }
		public DateTime AmiDate { get; set; }
	}
}
