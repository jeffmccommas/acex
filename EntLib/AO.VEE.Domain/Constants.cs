﻿namespace AO.VEE.Domain
{
	/// <summary>
	/// class for constants
	/// </summary>
	public static class Constants
	{
		/// <summary>
		/// Constants for CassandraTableNames
		/// </summary>
		public static class CassandraTableNames
		{
			public const string VeeStagedClientMeterByDate = "vee_staged_client_meter_by_date";
			public const string VeeConsumptionByMeter = "vee_consumption_by_meter";
			public const string ConsumptionByMeter = "consumption_by_meter";
			public const string MeterHistoryByMeter = "meter_history_by_meter";
		}
	}
}
