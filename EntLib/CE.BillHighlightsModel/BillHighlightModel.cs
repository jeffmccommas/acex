﻿
namespace CE.BillHighlightsModel
{
    public class BillHighlightModel
    {
        /// <summary>
        /// Account Id of the highlight.
        /// </summary>
        public string AccountId { get; set; }
        /// <summary>
        /// Premise Id of the highlight.
        /// </summary>
        public string PremiseId { get; set; }
        /// <summary>
        /// Service Id (Service Contract Id) of the highlight.
        /// </summary>
        public string ServiceId { get; set; }
        /// <summary>
        /// Identifier of the action from the content management system.
        /// </summary>
        public string ActionKey { get; set; }

        /// <summary>
        /// Cost of impact.
        /// </summary>
        public decimal CostImpact { get; set; }

        /// <summary>
        /// Percentage of impact.
        /// </summary>
        public decimal PercentageImpact { get; set; }

        /// <summary>
        /// Type of commodity. 
        /// </summary>
        public string Commoditykey { get; set; }

        /// <summary>
        /// Type of Highlight
        /// account or service
        /// </summary>
        public string HighlightLevel { get; set; }
    }
}
