﻿using CE.Models;

namespace CE.BillHighlightsModel
{
    public class BillHighlightsModelFactory
    {

        /// <summary>
        /// Create BillHighlightModelManager that implements IBillHighlightsModel.
        /// </summary>
        /// <param name="clientId"></param>
        /// <param name="rateCompanyId"></param>
        /// <returns></returns>
        public IBillHighlightsModel CreateBillHighlightsModelManager(int clientId, int rateCompanyId) => (new BillHighlightsModelManager(clientId, rateCompanyId));

        /// <summary>
        /// Create BillHighlightModelManager that implements IBillHighlightsModel.
        /// Set InsightRepository for unit test
        /// </summary>
        /// <param name="clientId"></param>
        /// <param name="rateCompanyId"></param>
        /// <param name="insightsEfRepository"></param>
        /// <returns></returns>
        public IBillHighlightsModel CreateBillHighlightsModelManager(int clientId, int rateCompanyId,
            IInsightsEFRepository insightsEfRepository)
        {
            return (new BillHighlightsModelManager(clientId, rateCompanyId, insightsEfRepository));
        }
    }
}
