﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Threading.Tasks;
using CE.EnergyModel;
using CE.EnergyModel.Enums;
using CE.EnergyModel.Helper;
using CE.Models;
using CE.Models.Insights;
using CE.RateEngine;
using CommodityTypeEnergy = CE.EnergyModel.Enums.CommodityType;
using CommodityTypesInsights = CE.Models.Insights.Types.CommodityType;

namespace CE.BillHighlightsModel
{
    public class BillHighlightsModelManager : IBillHighlightsModel
    {
        #region Private Constants

        private const int NumOfBills = 24; // default to 24 to be consistant with bill history
        private const string ConnectionStringName = "RateEngineConn";
        private const string EnableLoggingName = "RateEngineLogging";
        private const string ConfigurationErrorDesc = "Rate Engine configuration error.";

        #endregion

        private static IInsightsEFRepository _insightsEfRepository = new InsightsEfRepository();
        private RateEngine.RateEngine _rateEngine = new RateEngine.RateEngine();

        private readonly int _clientId;
        private readonly int _ratecompanyId;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="clientId">A Client Id</param>
        /// <param name="rateCompanyId">A Rate Company Id</param>
        public BillHighlightsModelManager(int clientId, int rateCompanyId)
        {
            _clientId = clientId;
            _ratecompanyId = rateCompanyId;
            LoadRateEngineConfiguration();
        }

        /// <inheritdoc />
        /// <summary>
        /// Testability, passed in repository so not relying on data access through sql server
        /// </summary>
        /// <param name="clientId">A Client Id</param>
        /// <param name="rateCompanyId">A Rate Company Id</param>
        /// <param name="insightsEfRepository">Repository</param>
        public BillHighlightsModelManager(int clientId, int rateCompanyId, IInsightsEFRepository insightsEfRepository)
            : this(clientId, rateCompanyId)
        {
            _insightsEfRepository = insightsEfRepository ?? throw new ArgumentNullException(nameof(insightsEfRepository), @"InsightsRepository is null!");
        }

        /// <summary>
        /// Execute bill highlight evaluations
        /// </summary>
        /// <param name="customerId">A Customer Id</param>
        /// <param name="accountId">An Account Id</param>
        /// <param name="premiseId">A Premise Id</param>
        /// <param name="requestCommodityKeys">The commodity keys passed in via the Action request</param>
        /// <param name="latestBillDate"></param>
        /// <param name="previousBillDate"></param>
        public List<BillHighlightModel> ExecuteBillHighlightModel(string customerId, string accountId, string premiseId,
            string requestCommodityKeys, DateTime? latestBillDate, DateTime? previousBillDate)
        {
            var highlights = new List<BillHighlightModel>();

            // Get the Account's Bills 
            var accountBills = GetAccountBills(customerId, accountId);

            if (accountBills == null)
            {
                return highlights;
            }

            // Calc the Highlights
            CalcHighlights(accountBills, premiseId, customerId, accountId, requestCommodityKeys, ref highlights, latestBillDate, previousBillDate);

            // Return the highlights
            return highlights.Where(x => x.CostImpact != 0 || x.PercentageImpact != 0).ToList();
        }

        /// <summary>
        /// Get last 24 bills for the selected account
        /// </summary>
        /// <param name="customerId">A Customer Id</param>
        /// <param name="accountId">An Account Id</param>
        /// <returns>An AccountBill object that contains the Bills</returns>
        private BillAccount GetAccountBills(string customerId, string accountId)
        {
            BillAccount accountBills = null;
            var today = DateTime.Today.Date;
            var startDate = today.AddMonths(24 * -1);

            var billRequest = new BillRequest
            {
                CustomerId = customerId,
                AccountId = accountId,
                StartDate = startDate,
                EndDate = today,
                Count = NumOfBills
            };

            var bills = _insightsEfRepository.GetBills(_clientId, billRequest);

            if (bills.Customer?.Accounts != null && bills.Customer.Accounts.Any())
            {
                accountBills = bills.Customer.Accounts.Find(a => a.Id == accountId);
            }

            return accountBills;
        }

        /// <summary>
        /// Calculate Service Level Bill Highlights
        /// </summary>
        /// <param name="accountBills">An AccountBill object</param>
        /// <param name="premiseId">A Premise Id</param>
        /// <param name="customerId">A Customer Id</param>
        /// <param name="accountId">An Account Id</param>
        /// <param name="requestCommodityKeys">The commodity keys passed in via the Action request</param>
        /// <param name="highlights">A refrenence to a list of BillHighlight objects.</param>
        /// <param name="latestBillDate">Latest Bill date to compare</param>
        /// <param name="previousBillDate">Previous Bill date to compare</param>
        private void CalcHighlights(BillAccount accountBills, string premiseId, string customerId, string accountId, string requestCommodityKeys,
            ref List<BillHighlightModel> highlights, DateTime? latestBillDate, DateTime? previousBillDate)
        {
            // get premise bills
            var premiseBills = accountBills?.Bills.Where(b => b.Premises.All(p => p.Id == premiseId)).ToList();
            Bill recentBill;
            Bill previousBill;

            if (premiseBills == null || premiseBills.Count < 2)
            {
                return;
            }

            if (latestBillDate == DateTime.MinValue && previousBillDate == DateTime.MinValue || latestBillDate == null && previousBillDate == null)
            {
                // get latest bill at premise level
                recentBill = premiseBills[0];

                // get previous bill at premise level
                previousBill = premiseBills[1];
            }
            else
            {
                recentBill = premiseBills.Find(b => latestBillDate != null && b.BillDate.Equals(latestBillDate.Value.ToString("yyyy-MM-dd")));
                previousBill = premiseBills.Find(b => previousBillDate != null && b.BillDate.Equals(previousBillDate.Value.ToString("yyyy-MM-dd")));
            }

            if (recentBill == null || previousBill == null)
            {
                return;
            }

            var lstComm = requestCommodityKeys.Split(',').ToList();
            foreach (var com in lstComm)
            {
                var recentBillServ = new BillService();
                var previousBillServ = new BillService();
                foreach (var recentBillService in recentBill.Premises[0].Service)
                {
                    if (!string.IsNullOrEmpty(com))
                    {
                        if (!com.ToLower().Contains(recentBillService.CommodityKey.ToLower()))
                        {
                            continue;
                        }
                    }
                    recentBillServ = recentBillService;
                    // get the same service from previous bill; if exists, compare
                    var previousBillService = previousBill.Premises.SelectMany(p => p.Service).ToList().Find(s => s.Id == recentBillService.Id);

                    if (recentBillService == null || previousBillService == null) continue;
                    previousBillServ = previousBillService;
                    break;
                }

                if (recentBillServ.BillEndDate == null) continue;
                // Weather Highlights
                var weatherHighlightTask = GetWeatherHighlightAsync(recentBillServ, previousBillServ, previousBill, customerId, accountId, premiseId);
                var weatherHighlights = weatherHighlightTask.Result;
                highlights.AddRange(weatherHighlights);

                // Calc Rate Highlight
                var rateHighlightTask = GetRateHighlightAsync(recentBillServ, previousBillServ, accountBills.Id, premiseId);
                var rateHighlights = rateHighlightTask.Result;
                highlights.AddRange(rateHighlights);
            }
            
            foreach (var com in lstComm)
            {
                decimal recentCost = 0;
                decimal previousCost = 0;
                decimal recentServiceUse = 0;
                decimal previousServiceUse = 0;
                decimal recentBillDays = 0;
                decimal previousBillDays = 0;
                foreach (var recentBillService in recentBill.Premises[0].Service)
                {
                    if (!string.IsNullOrEmpty(com))
                    {
                        if (!com.ToLower().Contains(recentBillService.CommodityKey.ToLower()))
                        {
                            continue;
                        }
                    }
                    recentCost = recentCost + recentBillService.CostofUsage;
                    recentServiceUse = recentServiceUse + recentBillService.TotalServiceUse;
                    recentBillDays = recentBillDays + recentBillService.BillDays;

                    var previousBillService = previousBill.Premises[0].Service.Find(s => s.Id == recentBillService.Id);
                    if (previousBillService == null) continue;
                    previousCost = previousCost + previousBillService.CostofUsage;
                    previousServiceUse = previousServiceUse + previousBillService.TotalServiceUse;
                    previousBillDays = previousBillDays + previousBillService.BillDays;
                }
                
                // Calc bill higlight
                var billHighlightTask = GetBillHighlightAsync(accountId, com, recentCost, previousCost, recentServiceUse, previousServiceUse, recentBillDays, previousBillDays);
                var billHighlights = billHighlightTask.Result;
                highlights.AddRange(billHighlights);

                // Calc energy charge higlight
                var energyChargeHighlightTask = GetEnergyChargeHighlightAsync(accountId, com, previousBill.TotalAmount, recentCost, previousCost);
                var energyChargeHighlights = energyChargeHighlightTask.Result;
                highlights.AddRange(energyChargeHighlights);
            }
        }

        private static async Task<List<BillHighlightModel>> GetBillHighlightAsync(string accountId, string commodity, decimal recentCost, decimal previousCost, decimal recentServiceUse, decimal previousServiceUse, decimal recentBillDays, decimal previousBillDays)
        {
            var task = await Task.Factory.StartNew(() => CalcBillHighlight(accountId, commodity, recentCost, previousCost, recentServiceUse, previousServiceUse, recentBillDays, previousBillDays)).ConfigureAwait(false);
            return task;
        }

        private static async Task<List<BillHighlightModel>> GetEnergyChargeHighlightAsync(string accountId, string commodity, decimal totalAmount, decimal recentCost, decimal previousCost)
        {
            var task = await Task.Factory.StartNew(() => CalcUsageCostHighlight(accountId, commodity, totalAmount, recentCost, previousCost)).ConfigureAwait(false);
            return task;
        }

        private async Task<List<BillHighlightModel>> GetRateHighlightAsync(BillService recentBillService, BillService previousBillService, string accountId, string premiseId)
        {
            var task = await Task.Factory.StartNew(() => CalcRateHighlight(recentBillService, previousBillService, accountId, premiseId)).ConfigureAwait(false);
            return task;
        }

        private async Task<List<BillHighlightModel>> GetWeatherHighlightAsync(BillService recentBillService, BillService previousBillService, Bill previousBill, string customerId, string accountId, string premiseId)
        {
            var task = await Task.Factory.StartNew(() => CalcWeatherHighlights(recentBillService, previousBillService, previousBill, _clientId, customerId, accountId, premiseId)).ConfigureAwait(false);
            return task;
        }

        # region CalcBillHighlights

        /// <summary>
        /// In the formulas below, the suffix ‘2’ refers to the “latest” bill and the suffix ‘1’ refers to the comparison or “previous” bill.
        /// •              CostImpact = (BillingDays2 - BillingDays1) * AverageUse2 * AverageCost1
        /// •              If the energy usage is zero for one of the bills, then the CostImpact = 0
        /// •              PercentImpact = DollarChangeBillingDays / TotalChargesBill1
        /// </summary>
        /// <param name="accountId">An Account Id</param>
        /// <param name="commodity"></param>
        /// <param name="recentCost"></param>
        /// <param name="previousCost"></param>
        /// <param name="recentServiceUse"></param>
        /// <param name="previousServiceUse"></param>
        /// <param name="recentBillDays"></param>
        /// <param name="previousBillDays"></param>
        private static List<BillHighlightModel> CalcBillHighlight(string accountId, string commodity, decimal recentCost, decimal previousCost,
            decimal recentServiceUse, decimal previousServiceUse, decimal recentBillDays, decimal previousBillDays)
        {
            var highlights = new List<BillHighlightModel>();

            if (recentCost == 0 || previousCost == 0) return highlights;

            decimal averageCost1 = 0;
            if (previousServiceUse != 0)
            {
                averageCost1 = previousCost / previousServiceUse;
            }

            decimal averageUse2 = 0;
            if (recentBillDays != 0)
            {
                averageUse2 = recentServiceUse / recentBillDays;
            }

            var costImpact = (recentBillDays - previousBillDays) * averageUse2 * averageCost1;

            decimal percentImpact = 0;
            if (recentCost != 0)
            {
                percentImpact = costImpact / recentCost;
            }

            // Create the billhightlight
            highlights.Add(new BillHighlightModel
            {
                AccountId = accountId,
                ActionKey = "insight.billperioddecrease",
                Commoditykey = commodity,
                CostImpact = costImpact < 0 ? costImpact : 0,
                PercentageImpact = percentImpact < 0 ? percentImpact : 0
            });

            highlights.Add(new BillHighlightModel
            {
                AccountId = accountId,
                ActionKey = "insight.billperiodincrease",
                Commoditykey = commodity,
                CostImpact = costImpact < 0 ? 0 : costImpact,
                PercentageImpact = percentImpact < 0 ? 0 : percentImpact
            });

            return highlights;
        }

        #endregion CalcBillHighlights

        #region CalcUsageCostHighlight

        /// <summary>
        /// In the formulas below, the suffix ‘2’ refers to the “latest” bill and the suffix ‘1’ refers to the comparison or “previous” bill.
        /// •              CostImpact =  (Usagecost2 – Usagecost1)
        /// •              PercentImpact =  CostImpact/TotalChargesBill1
        /// </summary>
        /// <param name="accountId">An Account Id</param>
        /// <param name="commodity"></param>
        /// <param name="totalAmount">Total Bill Charge</param>
        /// <param name="recentCost"></param>
        /// <param name="previousCost"></param>
        private static List<BillHighlightModel> CalcUsageCostHighlight(string accountId, string commodity, decimal totalAmount, decimal recentCost, decimal previousCost)
        {
            var highlights = new List<BillHighlightModel>();

            if (recentCost == 0 || previousCost == 0) return highlights;
            var costImpact = recentCost - previousCost;

            decimal percentImpact = 0;
            if (totalAmount > 0)
            {
                percentImpact = costImpact / totalAmount;
            }
            // Create the billhightlight
            if (costImpact < 0)
            {
                if (commodity != null)
                    highlights.Add(new BillHighlightModel
                    {
                        AccountId = accountId,
                        ActionKey = "insight.usagecostdecrease",
                        Commoditykey = commodity.ToLower(),
                        CostImpact = costImpact < 0 ? costImpact * -1 : 0,
                        PercentageImpact = percentImpact < 0 ? percentImpact * -1 : 0
                    });
            }
            else
            {
                if (commodity != null)
                    highlights.Add(new BillHighlightModel
                    {
                        AccountId = accountId,
                        ActionKey = "insight.usagecostincrease",
                        Commoditykey = commodity.ToLower(),
                        CostImpact = costImpact < 0 ? 0 : costImpact,
                        PercentageImpact = percentImpact < 0 ? 0 : percentImpact
                    });
            }

            return highlights;
        }

        #endregion CalcUsageCostHighlight

        #region CalcRateHighlight

        /// <summary>
        /// Calculate the rate highlight
        /// </summary>
        /// <param name="recentBillService">The service associated with the most recent bill.</param>
        /// <param name="previousBillService">The service associated with the next to the most recent bill.</param>
        /// <param name="accountId">An Account Id</param>
        /// <param name="premiseId">A Premise Id</param>
        /// <returns></returns>
        private List<BillHighlightModel> CalcRateHighlight(BillService recentBillService, BillService previousBillService, string accountId, string premiseId)
        {
            var highlights = new List<BillHighlightModel>();
            var isRateClassChange = IsRateClassChange(recentBillService, previousBillService);

            if (isRateClassChange) return highlights;
            // Check if there's rate change
            var detailedRateInfoCol = _rateEngine.GetDetailedRateInformation(_ratecompanyId,
                recentBillService.RateClass,
                Convert.ToDateTime(previousBillService.BillStartDate),
                Convert.ToDateTime(recentBillService.BillEndDate));

            var rateChangeInfo = _rateEngine.GetRateChangeInformation(detailedRateInfoCol,
                Convert.ToDateTime(previousBillService.BillStartDate),
                Convert.ToDateTime(previousBillService.BillEndDate),
                Convert.ToDateTime(recentBillService.BillStartDate),
                Convert.ToDateTime(recentBillService.BillEndDate));

            // if rate change, calc the rate change impact
            if (!rateChangeInfo.RateChange) return highlights;
            var usageRecentCol = new UsageCollection
            {
                new Usage(Enums.BaseOrTier.TotalServiceUse, Convert.ToDouble(recentBillService.TotalServiceUse))
            };

            var usagePreviousCol = new UsageCollection
            {
                new Usage(Enums.BaseOrTier.TotalServiceUse, Convert.ToDouble(recentBillService.TotalServiceUse))
            };

            var previousBillResult = _rateEngine.CalculateCost(_ratecompanyId, recentBillService.RateClass,
                Convert.ToDateTime(previousBillService.BillStartDate),
                Convert.ToDateTime(previousBillService.BillEndDate), usagePreviousCol);

            var recentBillResult = _rateEngine.CalculateCost(_ratecompanyId, recentBillService.RateClass,
                Convert.ToDateTime(recentBillService.BillStartDate),
                Convert.ToDateTime(recentBillService.BillEndDate), usageRecentCol);

            if (previousBillResult.RateCountApplied == 0 || recentBillResult.RateCountApplied == 0 || previousBillService.CostofUsage == 0) return highlights;

            var impact = recentBillResult.TotalCost - previousBillResult.TotalCost;
            var impactPercentage = impact / Math.Abs(Convert.ToDouble(previousBillService.CostofUsage));

            // Create the billhightlight
            highlights.Add(new BillHighlightModel
            {
                AccountId = accountId,
                PremiseId = premiseId,
                ActionKey = "insight.rateincrease",
                CostImpact = impact > 0 ? Convert.ToDecimal(impact) : 0,
                PercentageImpact = impact > 0 ? Convert.ToDecimal(impactPercentage) : 0,
                Commoditykey = recentBillService.CommodityKey,
                ServiceId = recentBillService.Id,
            });

            highlights.Add(new BillHighlightModel
            {
                AccountId = accountId,
                PremiseId = premiseId,
                ActionKey = "insight.ratedecrease",
                CostImpact = impact < 0 ? Convert.ToDecimal(impact) : 0,
                PercentageImpact = impact < 0 ? Convert.ToDecimal(impactPercentage) : 0,
                Commoditykey = recentBillService.CommodityKey,
                ServiceId = recentBillService.Id,
            });
            return highlights;
        }

        /// <summary>
        /// Check if rate class change between 2 bills
        /// </summary>
        /// <param name="recentBillService">The service associated with the most recent bill.</param>
        /// <param name="previousBillService">The service associated with the next to the most recent bill.</param>
        /// <returns>True, if there is a rate change.</returns>
        private static bool IsRateClassChange(BillService recentBillService, BillService previousBillService)
        {
            return recentBillService.RateClass != previousBillService.RateClass;
        }

        /// <summary>
        /// Load the Rate Engine configuration.
        /// </summary>
        private void LoadRateEngineConfiguration()
        {
            try
            {
                var conn = ConfigurationManager.ConnectionStrings[ConnectionStringName].ConnectionString;
                var enableLogging = Convert.ToBoolean(ConfigurationManager.AppSettings[EnableLoggingName]);
                _rateEngine = new RateEngine.RateEngine(conn, enableLogging);
            }
            catch (Exception ex)
            {
                throw new Exception(ConfigurationErrorDesc, ex);
            }
        }

        #endregion CalcRateHighlight

        #region Weather Highlights

        /// <summary>
        /// Calculate Weather Bill Highlights
        /// </summary>
        /// <param name="recentBillService">The most recent bill service</param>
        /// <param name="previousBillService">The previous bill service.</param>
        /// <param name="previousBill">The previous Bill</param>
        /// <param name="clientId">The Client Id</param>
        /// <param name="customerId">A Customer Id</param>
        /// <param name="accountId">An Account Id</param>
        /// <param name="premiseId">A Premise Id</param>
        private List<BillHighlightModel> CalcWeatherHighlights(BillService recentBillService, BillService previousBillService,
            Bill previousBill, int clientId, string customerId, string accountId,
            string premiseId)
        {
            var highlights = new List<BillHighlightModel>();
            // Get the Energy Model results
            var factory = new EnergyModelManagerFactory();
            var emmgr = factory.CreateEnergyModelManager(_clientId);

            var idParams = new IDParams
            {
                CustomerId = customerId,
                AccountId = accountId,
                PremiseId = premiseId
            };

            var extraParams = new ExtraParams
            {
                ExecuteBillDisagg = true
            };

            var emResult = emmgr.ExecuteEnergyModel(idParams, extraParams, null, null, null);

            // remove non bill disagg enduses and cleanup addons
            var billdisaggCategory = "billdisagg";
            if (emResult.ModelType == ModelType.Business)
            {
                billdisaggCategory = "business";
            }

            emResult.EndUses.RemoveAll(a => a.Category.ToLower() != billdisaggCategory);
            emResult.EndUses.RemoveAddOnEnergyUses();
            emResult.Appliances.RemoveAddOnEnergyUses();
            emResult.Appliances.RemoveEmpty();
            emResult.Appliances.SetOriginalQuantities();
            //This is in case we need to revert to model usages because of bill disagg failure
            emResult.TotalEnergyUses.Clear(); //Totals are no longer valid so clear this collection

            // Get the weather data.
            var weatherCalc = new WeatherCalc();
            var commodityTypeList = GetCommodityTypeList(recentBillService.CommodityKey);

            // Get the dates
            var endDateForYear = Convert.ToDateTime(recentBillService.BillEndDate).AddDays(-1);
            var startDateForYear = endDateForYear.AddYears(-1);

            var endDateForBill = Convert.ToDateTime(recentBillService.BillEndDate).AddDays(-1);
            var startDateBill = Convert.ToDateTime(previousBillService.BillStartDate);

            var bdInputParams = new BillDisagg.DisaggInputParams
            {
                EMResultAsInput = emResult,
                Bills = null,
                CustomerId = customerId,
                AccountId = accountId,
                PremiseId = premiseId
            };

            var bdmFactory = new BillDisagg.BillDisaggManagerFactory();
            var bdMgr = bdmFactory.CreateBillDisaggManager(clientId);

            emResult = bdMgr.UpdateEnergyModelWithBillDisagg(bdInputParams, commodityTypeList);

            // Get the weather from the database.
            weatherCalc.AdjustModeledEnergyUsingActualWeather(startDateForYear, endDateForYear, commodityTypeList,
                emResult);

            var dailyWeather = weatherCalc.GetDailyWeather(clientId, startDateBill, endDateForBill,
                emResult.IDParams.Zipcode, emResult.RegionParams.CountryCode, emResult.RegionParams.StationId, null);

            var newbillWeather = dailyWeather.Where(w => w.WeatherReadingDate >= Convert.ToDateTime(recentBillService.BillStartDate) &&
                                                         w.WeatherReadingDate <= Convert.ToDateTime(recentBillService.BillEndDate)).ToList();

            var newbillHdDays = newbillWeather.Sum(w => w.HeatingDegreeDays);
            var newbillCdDays = newbillWeather.Sum(w => w.CoolingDegreeDays);

            var oldbillWeather = dailyWeather.Where(w => w.WeatherReadingDate >= Convert.ToDateTime(previousBillService.BillStartDate) &&
                                                         w.WeatherReadingDate <= Convert.ToDateTime(previousBillService.BillEndDate)).ToList();

            var oldbillHdDays = oldbillWeather.Sum(w => w.HeatingDegreeDays);
            var oldbillCdDays = oldbillWeather.Sum(w => w.CoolingDegreeDays);

            decimal heatingFactor = 0;
            if (emResult.AnnualHDD != 0)
            {
                heatingFactor = emResult.Highlights.TotalHeatLoad / emResult.AnnualHDD;
            }

            decimal coolingFactor = 0;
            if (emResult.AnnualCDD != 0)
            {
                coolingFactor = emResult.Highlights.TotalCoolLoad / emResult.AnnualCDD;
            }

            var baseFactor = emResult.Highlights.BaseFuelLoad / 365;

            var modeledLoad = heatingFactor * oldbillHdDays +
                              coolingFactor * oldbillCdDays + baseFactor * previousBillService.BillDays;

            // EnergyChargesBill1 is equal to the total charges for the fuel being analyzed for the previous (older)bill.
            // FixedChargesBill1 is equal to the portion of the total charges that is not usage based, but is fixed.  This is typically called the Customer Charge or Basic Service Charge. 

            var oldBillEnergyCharges = previousBillService.CostofUsage;
            var oldBillFixedCharges = previousBillService.AdditionalServiceCost;

            if (modeledLoad * (oldBillEnergyCharges - oldBillFixedCharges) == 0 || previousBill.TotalAmount == 0)
            {
                return new List<BillHighlightModel>();
            }

            var impactDollars = (heatingFactor * (newbillHdDays - oldbillHdDays) +
                                 coolingFactor * (newbillCdDays - oldbillCdDays)) *
                                1 / modeledLoad * (oldBillEnergyCharges - oldBillFixedCharges);

            var impactPercentage = impactDollars / previousBill.TotalAmount;

            // Create the billhightlight
            highlights.Add(new BillHighlightModel
            {
                AccountId = accountId,
                PremiseId = premiseId,
                ActionKey = "insight.weatherincrease",
                CostImpact = impactDollars > 0 ? Convert.ToDecimal(impactDollars) : 0,
                PercentageImpact = impactPercentage > 0 ? Convert.ToDecimal(impactPercentage) : 0,
                Commoditykey = recentBillService.CommodityKey.ToLower(),
                ServiceId = recentBillService.Id,
            });

            highlights.Add(new BillHighlightModel
            {
                AccountId = accountId,
                PremiseId = premiseId,
                ActionKey = "insight.weatherdecrease",
                CostImpact = impactDollars < 0 ? Convert.ToDecimal(impactDollars) : 0,
                PercentageImpact = impactPercentage < 0 ? Convert.ToDecimal(impactPercentage) : 0,
                Commoditykey = recentBillService.CommodityKey.ToLower(),
                ServiceId = recentBillService.Id,
            });

            return highlights;
        }

        /// <summary>
        /// Get a list of EnergyModel.Enums.CommodityType from the requested commodities
        /// </summary>
        /// <param name="requestCommodityKeys">The commodity keys passed in via the Action request</param>
        /// <returns>A list of EnergyModel.Enums.CommodityType</returns>
        private static List<CommodityTypeEnergy> GetCommodityTypeList(string requestCommodityKeys)
        {
            var returnList = new List<CommodityTypeEnergy>();
            foreach (CommodityTypeEnergy commodityType in Enum.GetValues(typeof(CommodityTypeEnergy)))
            {
                if (!Enum.IsDefined(typeof(CommodityTypesInsights), commodityType.ToString()))
                {
                    continue;
                }

                if (string.IsNullOrEmpty(requestCommodityKeys))
                {
                    returnList.Add(commodityType);
                    continue;
                }

                if (requestCommodityKeys.ToLower().Contains(commodityType.ToString().ToLower()))
                {
                    returnList.Add(commodityType);
                }
            }

            return returnList;
        }

        #endregion Weather Highlights
    }
}
