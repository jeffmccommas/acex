﻿using System;
using System.Collections.Generic;

namespace CE.BillHighlightsModel
{
    public interface IBillHighlightsModel
    {
        List<BillHighlightModel> ExecuteBillHighlightModel(string customerId, string accountId, string premiseId, string requestCommodityKeys, DateTime? latestBillDate, DateTime? previousBillDate);
    }
}
