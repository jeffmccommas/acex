﻿using System;
using System.Collections.Generic;
using CE.Models.Insights;

namespace CE.BillHighlightsModelTests.Helpers
{
    public class TestHelper
    {

        public static BillResponse GetMockSingleServiceSinglePremiseSingleAccountData(int clientId, string customerId, string accountId, string premiseId, string serviceId, string commodityKey, string uomKey, int billDays, string rateclass, string add1, string city, string state, string zip, string fname, string lname, DateTime endDate)
        {
            var customer = new BillCustomer
            {
                Id = customerId,
                FirstName = fname,
                LastName = lname,
                Accounts = new List<BillAccount>()
            };

            var account = new BillAccount
            {
                Id = accountId,
                Bills = new List<Bill>()
            };

            var startDate = endDate.AddDays(billDays * -1 + 1);

            for (int i = 0; i < 24; i++)
            {
                var days = billDays;
                if (i%2 == 0)
                {
                    days = billDays + 1;
                }
                var billService = CreateBillService(serviceId, days, endDate.ToString("yyyy-MM-dd"),
                    startDate.ToString("yyyy-MM-dd"), commodityKey, rateclass, uomKey);
                var billPremise = CreateBillPremise(premiseId, add1, city, state, zip);
                billPremise.Service.Add(billService);
                var bill = CreateBill(endDate.ToString("yyyy-MM-dd"));
                bill.Premises.Add(billPremise);
                account.Bills.Add(bill);
                endDate = startDate.Date.AddDays(-1);
                startDate = endDate.AddDays(days * -1 + 1);
            }

            customer.Accounts.Add(account);
            return new BillResponse { ClientId = clientId, Customer = customer };
        }

        public static BillResponse GetMockMultipleServiceSinglePremiseSingleAccountData(int clientId, string customerId, string accountId, string premiseId, string serviceId, string serviceId2, string commodityKey, string commodity2Key, string uomKey, string uom2Key, int billDays, string rateclass, string rateclass2, string add1, string city, string state, string zip, string fname, string lname)
        {
            var customer = new BillCustomer
            {
                Id = customerId,
                FirstName = fname,
                LastName = lname,
                Accounts = new List<BillAccount>()
            };

            var account = new BillAccount
            {
                Id = accountId,
                Bills = new List<Bill>()
            };

            var endDate = DateTime.Today.Date.AddDays(-10);
            var startDate = endDate.AddDays(billDays * -1);

            for (int i = 0; i < 24; i++)
            {
                var billService = CreateBillService(serviceId, billDays, endDate.ToString("yyyy-MM-dd"),
                    startDate.ToString("yyyy-MM-dd"), commodityKey, rateclass, uomKey);
                var billService2 = CreateBillService(serviceId2, billDays, endDate.ToString("yyyy-MM-dd"),
                    startDate.ToString("yyyy-MM-dd"), commodity2Key, rateclass2, uom2Key);
                var billPremise = CreateBillPremise(premiseId, add1, city, state, zip);
                billPremise.Service.Add(billService);
                billPremise.Service.Add(billService2);
                var bill = CreateBill(endDate.ToString("yyyy-MM-dd"));
                bill.Premises.Add(billPremise);
                account.Bills.Add(bill);
                endDate = startDate.Date.AddDays(-1);
                startDate = endDate.AddDays(billDays * -1);
            }

            customer.Accounts.Add(account);
            return new BillResponse { ClientId = clientId, Customer = customer };
        }

        private static Bill CreateBill(string end)
        {

            var bill = new Bill
            {
                TotalAmount = 215.5m,
                AdditionalBillCost = 0,
                BillDate = end, //endDate.ToString("yyyy-MM-dd"),
                BillFrequency = "Monthly",
                Premises = new List<BillPremise>()
            };

            return bill;
        }

        private static BillPremise CreateBillPremise(string premiseId, string add1, string city, string state, string zip)
        {
            var billPremise = new BillPremise
            {
                Id = premiseId,
                Addr1 = add1,
                City = city,
                State = state,
                Zip = zip,
                Service = new List<BillService>()
            };

            return billPremise;
        }

        public static BillService CreateBillService(string serviceId, int billDays, string end, string start, string commodityKey, string rateclass, string uomKey)
        {
            var billService = new BillService
            {
                Id = serviceId,
                AdditionalServiceCost = 0,
                AverageTemperature = 72,
                BillDays = billDays,
                BillEndDate = end, //endDate.ToString("yyyy-MM-dd"),
                BillStartDate = start, //startDate.ToString("yyyy-MM-dd"),
                CommodityKey = commodityKey,
                CostofUsage = 193,
                RateClass = rateclass,
                TotalServiceUse = 550,
                UOMKey = uomKey
            };

            return billService;
        }

    }
}
