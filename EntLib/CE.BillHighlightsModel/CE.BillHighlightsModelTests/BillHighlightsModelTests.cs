﻿using System;
using System.Collections.Generic;
using System.Linq;
using CE.BillHighlightsModel;
using CE.BillHighlightsModelTests.Helpers;
using CE.Models;
using CE.Models.Insights;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;


namespace CE.BillHighlightsModelTests
{
    [TestClass()]
    public class BillHighlightsModelTests
    {
        private Mock<IInsightsEFRepository> _insightRepository;

        [TestInitialize]
        public void TestInit()
        {
            _insightRepository = new Mock<IInsightsEFRepository>();
        }

        [TestMethod()]
        public void GetAccountBillsTest_SingleService_Private()
        {
            var clientId = 87;
            var rateCompanyId = 100;
            var customerId = "GB001";
            var accountId = "GB001a1";
            var premiseId = "GB01a1p1";
            var serviceId = "GB01a1p1s1";
            var rateClass = "RG-1";
            var uomKey = "kwh";
            var commodityKey = "electric";
            var addr1 = "16 Laurel Ave";
            var city = "Wellesley";
            var state = "MA";
            var zip = "02481";
            var billDays = 30;
            var firstname = "TestFirst";
            var lastname = "TestLast";
            var billCount = 24;
            var today = DateTime.Today.Date;
            var startDate = today.AddMonths(24 * -1);
            
            var endDate = DateTime.Today.Date.AddDays(-10);
            var mockData = TestHelper.GetMockSingleServiceSinglePremiseSingleAccountData(clientId, customerId, accountId,
                premiseId, serviceId, commodityKey, uomKey, billDays, rateClass, addr1, city, state, zip, firstname,
                lastname, endDate);
            _insightRepository.Setup(
                c =>
                    c.GetBills(clientId,
                        It.Is<BillRequest>(
                            b =>
                                b.CustomerId == customerId && b.AccountId == accountId && b.Count == billCount &&
                                b.StartDate == startDate && b.EndDate == today))).Returns(mockData);
            var factory = new BillHighlightsModelFactory();
            var model = factory.CreateBillHighlightsModelManager(clientId, rateCompanyId, _insightRepository.Object);
            //var model = new BillHighlightsModel.BillHighlightsModelManager(clientId, rateCompanyId, _insightRepository.Object);
            PrivateObject obj = new PrivateObject(model);
            var result = obj.Invoke("GetAccountBills", customerId,accountId);
            Assert.IsNotNull(result);
            var accountBills = (BillAccount)result;
            Assert.AreEqual(billCount, accountBills.Bills.Count);
            var premiseBills = accountBills.Bills.Where(b => b.Premises.All(p => p.Id == premiseId)).ToList();
            Assert.AreEqual(premiseId, premiseBills[0].Premises[0].Id);
            Assert.AreEqual(1, premiseBills[0].Premises[0].Service.Count);
            bool backwardOrder = Convert.ToDateTime(premiseBills[0].BillDate) > Convert.ToDateTime(premiseBills[1].BillDate);
            Assert.IsTrue(backwardOrder);
        }

        [TestMethod()]
        public void GetAccountBillsTest_MultipleService_Private()
        {
            var clientId = 87;
            var rateCompanyId = 100;
            var customerId = "GB001";
            var accountId = "GB001a1";
            var premiseId = "GB01a1p1";
            var serviceId = "GB01a1p1s1";
            var serviceId2 = "GB01a1p1s2";
            var rateClass = "RG-1";
            var rateClass2 = "RTGas";
            var uomKey = "kwh";
            var commodityKey = "electric";
            var commodityKey2 = "gas";
            var uomKey2 = "therms";
            var addr1 = "16 Laurel Ave";
            var city = "Wellesley";
            var state = "MA";
            var zip = "02481";
            var billDays = 30;
            var firstname = "TestFirst";
            var lastname = "TestLast";
            var billCount = 24;
            var today = DateTime.Today.Date;
            var startDate = today.AddMonths(24 * -1);


            var mockData = TestHelper.GetMockMultipleServiceSinglePremiseSingleAccountData(clientId, customerId, accountId,
                premiseId, serviceId, serviceId2, commodityKey, commodityKey2, uomKey, uomKey2, billDays, rateClass, rateClass2, addr1, city, state, zip, firstname,
                lastname);
            _insightRepository.Setup(
                c =>
                    c.GetBills(clientId,
                        It.Is<BillRequest>(
                            b =>
                                b.CustomerId == customerId && b.AccountId == accountId && b.Count == billCount &&
                                b.StartDate == startDate && b.EndDate == today))).Returns(mockData);


            var factory = new BillHighlightsModelFactory();
            var model = factory.CreateBillHighlightsModelManager(clientId, rateCompanyId, _insightRepository.Object);

            PrivateObject obj = new PrivateObject(model);
            var result = obj.Invoke("GetAccountBills", customerId, accountId);
            Assert.IsNotNull(result);
            var accountBills = (BillAccount)result;
            Assert.AreEqual(billCount, accountBills.Bills.Count);

            var premiseBills = accountBills.Bills.Where(b => b.Premises.All(p => p.Id == premiseId)).ToList();
            Assert.AreEqual(premiseId, premiseBills[0].Premises[0].Id);
            Assert.AreEqual(2, premiseBills[0].Premises[0].Service.Count);
            bool backwardOrder = Convert.ToDateTime(premiseBills[0].BillDate) > Convert.ToDateTime(premiseBills[1].BillDate);
            Assert.IsTrue(backwardOrder);
        }

        [TestMethod()]
        public void GetAccountBillsTest_NoPremise_Private()
        {
            var clientId = 87;
            var rateCompanyId = 100;
            var customerId = "GB001";
            var accountId = "GB001a1";
            var premiseId = "GB01a1p1";
            var serviceId = "GB01a1p1s1";
            var serviceId2 = "GB01a1p1s2";
            var rateClass = "RG-1";
            var rateClass2 = "RTGas";
            var uomKey = "kwh";
            var commodityKey = "electric";
            var commodityKey2 = "gas";
            var uomKey2 = "therms";
            var addr1 = "16 Laurel Ave";
            var city = "Wellesley";
            var state = "MA";
            var zip = "02481";
            var billDays = 30;
            var firstname = "TestFirst";
            var lastname = "TestLast";
            var billCount = 24;
            var today = DateTime.Today.Date;
            var startDate = today.AddMonths(24 * -1);
            var requestPremise = "noPrmeise";


            var mockData = TestHelper.GetMockMultipleServiceSinglePremiseSingleAccountData(clientId, customerId, accountId,
                premiseId, serviceId, serviceId2, commodityKey, commodityKey2, uomKey, uomKey2, billDays, rateClass, rateClass2, addr1, city, state, zip, firstname,
                lastname);
            _insightRepository.Setup(
                c =>
                    c.GetBills(clientId,
                        It.Is<BillRequest>(
                            b =>
                                b.CustomerId == customerId && b.AccountId == accountId && b.Count == billCount &&
                                b.StartDate == startDate && b.EndDate == today))).Returns(mockData);


            var factory = new BillHighlightsModelFactory();
            var model = factory.CreateBillHighlightsModelManager(clientId, rateCompanyId, _insightRepository.Object);

            PrivateObject obj = new PrivateObject(model);
            var result = obj.Invoke("GetAccountBills", customerId, accountId);
            Assert.IsNotNull(result);
            var accountBills = (BillAccount)result;

            var premiseBills = accountBills.Bills.Where(b => b.Premises.All(p => p.Id == requestPremise)).ToList();
            Assert.AreEqual(0, premiseBills.Count);
        }


        [TestMethod()]
        public void CalcRateHighlightTest_Private()
        {
            var clientId = 87;
            var accountId = "GB001a1";
            var premiseId = "GB01a1p1";
            var rateCompanyId = 100;
            var serviceId = "GB01a1p1s1";
            var rateClass = "PTR-A-TEST";
            var uomKey = "kwh";
            var commodityKey = "electric";
            var billDays = 30;
            var newEndDate = Convert.ToDateTime("2/13/2010");
            var newStartDate = Convert.ToDateTime("1/15/2010");

            var oldEndDate = Convert.ToDateTime("1/14/2010");
            var oldStartDate = Convert.ToDateTime("12/16/2009");

            var recentBillService = TestHelper.CreateBillService(serviceId, billDays,
                newEndDate.ToString("yyyy-MM-dd"), newStartDate.ToString("yyyy-MM-dd"), commodityKey, rateClass, uomKey);
            var previousBillService = TestHelper.CreateBillService(serviceId, billDays,
                oldEndDate.ToString("yyyy-MM-dd"), oldStartDate.ToString("yyyy-MM-dd"), commodityKey, rateClass, uomKey);
            var factory = new BillHighlightsModelFactory();
            var model = factory.CreateBillHighlightsModelManager(clientId, rateCompanyId, _insightRepository.Object);
            PrivateObject obj = new PrivateObject(model);

            var highlights = new List<BillHighlightModel>();
            obj.Invoke("CalcRateHighlight", recentBillService, previousBillService, accountId,premiseId, highlights);
            Assert.AreEqual(2, highlights.Count);
            foreach (var highlight in highlights)
            {
                bool bIncrease = false;
                if (highlight.CostImpact > 0)
                    bIncrease = true;
                if (highlight.ActionKey.Equals("insight.rateincrease", StringComparison.CurrentCultureIgnoreCase))
                    Assert.IsTrue(bIncrease);
                else
                    Assert.IsFalse(bIncrease);
            }
        }


        [TestMethod()]
        public void CalcRateHighlightTest_NoHighlight_Private()
        {
            var clientId = 87;
            var accountId = "GB001a1";
            var premiseId = "GB01a1p1";
            var rateCompanyId = 100;
            var serviceId = "GB01a1p1s1";
            var rateClass = "PTR-A-TEST";
            var uomKey = "kwh";
            var commodityKey = "electric";
            var billDays = 30;
            var newEndDate = Convert.ToDateTime("5/30/2016");
            var newStartDate = Convert.ToDateTime("5/1/2016");

            var oldEndDate = Convert.ToDateTime("4/30/2016");
            var oldStartDate = Convert.ToDateTime("4/1/2016");

            var recentBillService = TestHelper.CreateBillService(serviceId, billDays,
                newEndDate.ToString("yyyy-MM-dd"), newStartDate.ToString("yyyy-MM-dd"), commodityKey, rateClass, uomKey);
            var previousBillService = TestHelper.CreateBillService(serviceId, billDays,
                oldEndDate.ToString("yyyy-MM-dd"), oldStartDate.ToString("yyyy-MM-dd"), commodityKey, rateClass, uomKey);
            var factory = new BillHighlightsModelFactory();
            var model = factory.CreateBillHighlightsModelManager(clientId, rateCompanyId, _insightRepository.Object);
            PrivateObject obj = new PrivateObject(model);

            var highlights = new List<BillHighlightModel>();
            obj.Invoke("CalcRateHighlight", recentBillService, previousBillService, accountId, premiseId, highlights);
            
            Assert.AreEqual(0, highlights.Count);
        }


        [TestMethod()]
        public void CalcBillHighlightTest_Private()
        {
            var clientId = 87;
            var accountId = "GB001a1";
            var rateCompanyId = 100;
            var serviceId = "GB01a1p1s1";
            var rateClass = "PTR-A-TEST";
            var uomKey = "kwh";
            var commodityKey = "electric";
            var newEndDate = Convert.ToDateTime("5/31/2016");
            var newStartDate = Convert.ToDateTime("5/1/2016");

            var oldEndDate = Convert.ToDateTime("4/30/2016");
            var oldStartDate = Convert.ToDateTime("4/1/2016");

            var recentBillService = TestHelper.CreateBillService(serviceId, 31,
                newEndDate.ToString("yyyy-MM-dd"), newStartDate.ToString("yyyy-MM-dd"), commodityKey, rateClass, uomKey);
            var previousBillService = TestHelper.CreateBillService(serviceId, 30,
                oldEndDate.ToString("yyyy-MM-dd"), oldStartDate.ToString("yyyy-MM-dd"), commodityKey, rateClass, uomKey);

            //var serivceBills = new List<BillService>();
            //serivceBills.Add(recentBillService);
            //serivceBills.Add(previousBillService);

            var factory = new BillHighlightsModelFactory();
            var model = factory.CreateBillHighlightsModelManager(clientId, rateCompanyId, _insightRepository.Object);
            PrivateObject obj = new PrivateObject(model);

            var highlights = new List<BillHighlightModel>();
            obj.Invoke("CalcBillHighlight", recentBillService, previousBillService, accountId,highlights);
            Assert.AreEqual(2, highlights.Count);
            foreach (var highlight in highlights)
            {
                bool bIncrease = false;
                if (highlight.CostImpact > 0)
                    bIncrease = true;
                if (highlight.ActionKey.Equals("insight.billperiodincrease", StringComparison.CurrentCultureIgnoreCase))
                    Assert.IsTrue(bIncrease);
                else
                    Assert.IsFalse(bIncrease);
            }
        }


        [TestMethod()]
        public void ExecuteBillHighlightModelNoDatesTest()
        {
            var clientId = 87;
            var rateCompanyId = 100;
            var customerId = "GB001";
            var accountId = "GB001a1";
            var premiseId = "GB01a1p1";
            var serviceId = "GB01a1p1s1";
            var rateClass = "RG-1";
            var uomKey = "kwh";
            var commodityKey = "electric";
            var addr1 = "16 Laurel Ave";
            var city = "Wellesley";
            var state = "MA";
            var zip = "02481";
            var billDays = 30;
            var firstname = "TestFirst";
            var lastname = "TestLast";
            var today = Convert.ToDateTime("2/13/2010");

            var mockData = TestHelper.GetMockSingleServiceSinglePremiseSingleAccountData(clientId, customerId, accountId,
                premiseId, serviceId, commodityKey, uomKey, billDays, rateClass, addr1, city, state, zip, firstname,
                lastname, today);
            _insightRepository.Setup(
                c =>
                    c.GetBills(clientId,
                        It.IsAny<BillRequest>())).Returns(mockData);

            var factory = new BillHighlightsModelFactory();
            var model = factory.CreateBillHighlightsModelManager(clientId, rateCompanyId, _insightRepository.Object);
            var result = model.ExecuteBillHighlightModel(customerId, accountId, premiseId, string.Empty, null, null);
            Assert.IsNotNull(result);
            Assert.AreEqual(6, result.Count);
            foreach (var highlight in result)
            {
                bool bIncrease = false;
                if (highlight.CostImpact > 0)
                    bIncrease = true;
                if (highlight.ActionKey.Equals("insight.rateincrease", StringComparison.CurrentCultureIgnoreCase))
                    Assert.IsTrue(bIncrease);
                else if (highlight.ActionKey.Equals("insight.billperiodincrease", StringComparison.CurrentCultureIgnoreCase))
                    Assert.IsTrue(bIncrease);
                else if (highlight.ActionKey.Equals("insight.weatherincrease", StringComparison.CurrentCultureIgnoreCase))
                    Assert.IsTrue(bIncrease);
                else
                    Assert.IsFalse(bIncrease);
            }
        }

        [TestMethod()]
        public void ExecuteBillHighlightModelBillDatesTest()
        {
            var clientId = 87;
            var rateCompanyId = 100;
            var customerId = "GB001";
            var accountId = "GB001a1";
            var premiseId = "GB01a1p1";
            var serviceId = "GB01a1p1s1";
            var rateClass = "RG-1";
            var uomKey = "kwh";
            var commodityKey = "electric";
            var addr1 = "16 Laurel Ave";
            var city = "Wellesley";
            var state = "MA";
            var zip = "02481";
            var billDays = 30;
            var firstname = "TestFirst";
            var lastname = "TestLast";
            var today = Convert.ToDateTime("2/13/2010");

            var recentBillDate = Convert.ToDateTime("2009-12-14");
            var previousBillDate = Convert.ToDateTime("2009-01-13");

            var mockData = TestHelper.GetMockSingleServiceSinglePremiseSingleAccountData(clientId, customerId, accountId,
                premiseId, serviceId, commodityKey, uomKey, billDays, rateClass, addr1, city, state, zip, firstname,
                lastname, today);
            _insightRepository.Setup(
                c =>
                    c.GetBills(clientId,
                        It.IsAny<BillRequest>())).Returns(mockData);

            var factory = new BillHighlightsModelFactory();
            var model = factory.CreateBillHighlightsModelManager(clientId, rateCompanyId, _insightRepository.Object);
            var result = model.ExecuteBillHighlightModel(customerId, accountId, premiseId, string.Empty, recentBillDate, previousBillDate);
            Assert.IsNotNull(result);
            Assert.AreEqual(6, result.Count);
            foreach (var highlight in result)
            {
                bool bIncrease = false;
                if (highlight.CostImpact > 0)
                    bIncrease = true;

                if (highlight.ActionKey.Equals("insight.billperiodincrease", StringComparison.CurrentCultureIgnoreCase))
                    Assert.IsTrue(bIncrease);
                else
                    Assert.IsFalse(bIncrease);
            }
        }
    }
}