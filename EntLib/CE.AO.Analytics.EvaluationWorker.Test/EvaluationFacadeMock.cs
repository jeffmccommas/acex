﻿using System;
using AO.BusinessContracts;
using CE.AO.Models;

namespace CE.AO.Analytics.EvaluationWorker.Test
{
    public class EvaluationFacadeMock : IEvaluationFacade
    {
        public EvaluationFacadeMock()
        {
            LogModel = new LogModel { DisableLog = false };
        }

        /// <summary>
        /// Constructor of EvaluationFacadeMock
        /// </summary>
        /// <param name="logModel"></param>
        public EvaluationFacadeMock(LogModel logModel)
        {
            LogModel = logModel;
        }
        public LogModel LogModel { get; set; }

        /// <summary>
        /// Mock method ProcessEvaluation
        /// </summary>
        /// <param name="clientId"></param>
        /// <param name="customerId"></param>
        /// <param name="accountId"></param>
        /// <param name="serviceContractId"></param>
        /// <param name="asOfDate"></param>
        public void ProcessEvaluation(int clientId, string customerId, string accountId, string serviceContractId, DateTime asOfDate)
        {
            
        }
    }
}
