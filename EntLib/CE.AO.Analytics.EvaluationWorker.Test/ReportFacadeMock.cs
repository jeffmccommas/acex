﻿using System;
using System.Threading.Tasks;
using AO.BusinessContracts;
using CE.AO.Models;

namespace CE.AO.Analytics.EvaluationWorker.Test
{
    public class ReportFacadeMock : IReport
    {
        /// <summary>
        /// Default constructor of ReportFacadeMock
        /// </summary>
        public ReportFacadeMock()
        {
            LogModel = new LogModel { DisableLog = false };
        }

        /// <summary>
        ///Parameterized constructor of ReportFacadeMock
        /// </summary>
        public ReportFacadeMock(LogModel logModel)
        {
            LogModel = logModel;
        }

        public LogModel LogModel { get; set; }

        /// <summary>
        /// Mock method ExportInsights of Report class
        /// </summary>
        /// <param name="clientId"></param>
        /// <param name="asOfDate"></param>
        /// <returns></returns>
        public Task<bool> ExportInsights(int clientId, DateTime asOfDate)
        {
            return Task.FromResult(true);
        }

        /// <summary>
        /// Mock method ExportNotifications of Report class
        /// </summary>
        public Task<bool> ExportNotifications(int clientId, DateTime asOfDate)
        {
            return Task.FromResult(true);
        }
    }
}
