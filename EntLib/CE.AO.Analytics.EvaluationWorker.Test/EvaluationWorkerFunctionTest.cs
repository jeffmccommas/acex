﻿using System.IO;
using System.Runtime.Remoting.Messaging;
using CE.AO.Logging;
using CE.AO.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace CE.AO.Analytics.EvaluationWorker.Test
{
    /// <summary>
    /// Test Case Class for Evaluation Worker Role(Function.cs)
    /// </summary>
    [TestClass]
    public class EvaluationWorkerFunctionTest
    {
        /// <summary>
        /// Test Case for Process Evaluation
        /// </summary>
        [TestMethod]
        public void ProcessEvaluation()
        {
            var log = TextWriter.Null;
            Functions.LogModel = new LogModel
            {
                Module = Utilities.Enums.Module.InsightEvaluation,
                DisableLog = false
            };
            Functions.ProcessEvaluation("87^^600^^22^^8700^^09/24/2016^^Source^^22^^MetaData^^2", log);
            Assert.AreEqual(true, true);
        }

        /// <summary>
        /// Test Case for Process Evaluation else part
        /// </summary>
        [TestMethod]
        public void ProcessEvaluation_Else()
        {
            var log = TextWriter.Null;
            Functions.LogModel = new LogModel
            {
                Module = Utilities.Enums.Module.Calculation,
                DisableLog = false
            };
            Functions.ProcessEvaluation("87^^600^^22^^8700^^09/24/2016^^Source^^22^^MetaData", log);
            Assert.AreEqual(true, true);
        }

        /// <summary>
        /// Test Case for Process Evaluation catch part
        /// </summary>
        [TestMethod]
        public void ProcessEvaluation_Catch()
        {
            var log = TextWriter.Null;
            Functions.LogModel = new LogModel
            {
                Module = Utilities.Enums.Module.InsightEvaluation,
                DisableLog = false
            };
            try
            {
                Functions.ProcessEvaluation("87^^600^^22^^8700^^Source^^09/24/2016^^22^^MetaData^^2", log);
            }
            catch
            {
                Assert.AreEqual(true, true);
            }
        }

        /// <summary>
        /// Test Case for ProcessInsightReport
        /// </summary>
        [TestMethod]
        public void ProcessInsightReport()
        {
            var log = TextWriter.Null;
            Functions.LogModel = new LogModel
            {
                Module = Utilities.Enums.Module.InsightEvaluation,
                DisableLog = false
            };
            Functions.ProcessInsightReport("87^^09/24/2016^^22", log);
            Assert.AreEqual(true, true);
        }

        /// <summary>
        /// Test Case for ProcessInsightReport else part
        /// </summary>
        [TestMethod]
        public void ProcessInsightReport_Else()
        {
            var log = TextWriter.Null;
            Functions.LogModel = new LogModel
            {
                Module = Utilities.Enums.Module.InsightEvaluation,
                DisableLog = false
            };
            Functions.ProcessInsightReport("87^^09/24/2016", log);
            Assert.AreEqual(true, true);
        }

        /// <summary>
        /// Test Case for ProcessInsightReport catch part
        /// </summary>
        [TestMethod]
        public void ProcessInsightReport_Catch()
        {
            var log = TextWriter.Null;
            Functions.LogModel = new LogModel
            {
                Module = Utilities.Enums.Module.InsightEvaluation,
                DisableLog = false
            };
            try
            {
                Functions.ProcessInsightReport("87^^22^^09/24/2016", log);
            }
            catch
            {
                Assert.AreEqual(true, true);
            }
        }

        /// <summary>
        /// Cleanup Method To Release Log4Net Data Resources hold by Any Test Method
        /// </summary>
        [TestCleanup]
        public void CleanUpMethod()
        {
            CallContext.FreeNamedDataSlot("log4net.Util.LogicalThreadContextProperties");
        }
    }
}
