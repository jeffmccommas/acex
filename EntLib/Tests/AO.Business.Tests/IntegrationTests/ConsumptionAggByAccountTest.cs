﻿using System;
using System.Collections.Generic;
using AO.BusinessContracts;
using CE.AO.Models;
using Microsoft.Practices.Unity;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace AO.Business.Tests.IntegrationTests
{
    [TestClass]
    public class ConsumptionAggByAccountTest : TestBase
    {
        private IConsumptionAggByAccount _consumptionAggByAccount;

        /// <summary>
        /// Test Case Init
        /// </summary>
        [TestInitialize]
        public void Init()
        {
            _consumptionAggByAccount = UnityContainer.Resolve<IConsumptionAggByAccount>();
        }

        /// <summary>
        /// Test case to insert ConsumptionAggByAccount Model with AggregationType as Day
        /// Test case for InsertOrMergeConsumptionAggByAccountAsync method for ConsumptionAggByAccount class
        /// </summary>
        [TestMethod]
        public void InsertOrMergeConsumptionAggByAccountForDayAsync()
        {
            var consumptionAggByAccountModel = new ConsumptionAggByAccountModel()
            {
                StandardUOMId = 3,
                AccountNumber = "Day Test Account",
                ClientId = 8787,
                CommodityId = 3,
                Timezone = "PST",
                StandardUOMConsumption = 10,
                AggregationEndDateTime = DateTime.UtcNow,
                AggregationType = CE.AO.Utilities.Enums.PreAggregationTypes.Day.ToString()
            };

           _consumptionAggByAccount.InsertOrMergeConsumptionAggByAccountAsync(consumptionAggByAccountModel).Wait();
            // ReSharper disable once UnusedVariable
            var consumptionFetched = _consumptionAggByAccount.GetConsumptionAggByAccountAsync(87, consumptionAggByAccountModel.AggregationEndDateTime, consumptionAggByAccountModel.AccountNumber).Result;
            _consumptionAggByAccount.DeleteConsumptionAggByAccountAsync(consumptionAggByAccountModel).Wait();
            _consumptionAggByAccount.DeleteConsumptionAggByAccountUsingPartionAsync(consumptionAggByAccountModel).Wait();
        }

        /// <summary>
        /// Test case to insert ConsumptionAggByAccount Model with AggregationType as Day
        /// Test case for InsertOrMergeConsumptionAggByAccountAsync method for ConsumptionAggByAccount class
        /// </summary>
        [TestMethod]
        public void InsertOrMergeConsumptionAggByAccountForHourAsync()
        {
            var consumptionAggByAccountModel = new ConsumptionAggByAccountModel()
            {
                StandardUOMId = 3,
                AccountNumber = "Hour Test Account",
                ClientId = 8787,
                CommodityId = 3,
                Timezone = "PST",
                StandardUOMConsumption = 10,
                AggregationEndDateTime = DateTime.UtcNow,
                AggregationType = CE.AO.Utilities.Enums.PreAggregationTypes.Hour.ToString()
            };

            _consumptionAggByAccount.InsertOrMergeConsumptionAggByAccountAsync(consumptionAggByAccountModel).Wait();
            _consumptionAggByAccount.GetConsumptionAggByAccountAsync(87, consumptionAggByAccountModel.AggregationEndDateTime, consumptionAggByAccountModel.AccountNumber).Wait();
            _consumptionAggByAccount.DeleteConsumptionAggByAccountAsync(consumptionAggByAccountModel).Wait();
            _consumptionAggByAccount.DeleteConsumptionAggByAccountUsingPartionAsync(consumptionAggByAccountModel).Wait();
        }

        /// <summary>
        /// Test case to insert ConsumptionAggByAccount Model in Batch
        /// Test case for InsertOrMergeConsumptionAggByAccountAsync method for ConsumptionAggByAccount class
        /// </summary>
        [TestMethod]
        public void BatchInsertOrMergeConsumptionAggByAccountAsync()
        {
            List<ConsumptionAggByAccountModel> listofConsumptionAggByAccountModel = new List<ConsumptionAggByAccountModel>();
            var consumptionAggByAccountModel = new ConsumptionAggByAccountModel()
            {
                StandardUOMId = 3,
                AccountNumber = "Batch Test Account",
                ClientId = 87,
                CommodityId = 3,
                Timezone = "PST",
                StandardUOMConsumption = 10,
                AggregationEndDateTime = DateTime.UtcNow,
                AggregationType = CE.AO.Utilities.Enums.PreAggregationTypes.Day.ToString()
            };

            listofConsumptionAggByAccountModel.Add(consumptionAggByAccountModel);

            _consumptionAggByAccount.InsertOrMergeConsumptionAggByAccountAsync(listofConsumptionAggByAccountModel).Wait();
            // ReSharper disable once UnusedVariable
            var consumptionFetched = _consumptionAggByAccount.GetConsumptionAggByAccountAsync(87, consumptionAggByAccountModel.AggregationEndDateTime, consumptionAggByAccountModel.AccountNumber).Result;
            _consumptionAggByAccount.DeleteConsumptionAggByAccountAsync(consumptionAggByAccountModel).Wait();
            _consumptionAggByAccount.DeleteConsumptionAggByAccountUsingPartionAsync(consumptionAggByAccountModel).Wait();
        }
    }
}
