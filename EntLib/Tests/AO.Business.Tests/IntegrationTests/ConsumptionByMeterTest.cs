﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AO.BusinessContracts;
using CE.AO.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.Practices.Unity;

namespace AO.Business.Tests.IntegrationTests
{


    /// <summary>
    /// Test Case Class for ConsumptionByMeter 
    /// </summary>
    [TestClass]
    public class ConsumptionByMeterTest : TestBase
    {
        private ITallAMI _iTallAmi;

        /// <summary>
        /// Initialization of test case.
        /// </summary>
        [TestInitialize]
        public void Init()
        {
            _iTallAmi = UnityContainer.Resolve<ITallAMI>();
        }

        /// <summary>
        /// Initialization of test case.
        /// </summary>
        [TestMethod]
        public void InsertOrMergeAmiAsync()
        {
            TallAmiModel tallAMiModel = new TallAmiModel()
            {
                CustomerId = "23",
                ClientId = 87,
                MeterId = "MET900",
                ServicePointId = "SP23",
                Timezone = "EST",
                CommodityId = 3,
                UOMId = 3,
                AccountNumber = "ACC500",
                AmiTimeStamp = DateTime.Now.Date,
                BatteryVoltage = "23V",
                TransponderPort = "P",
                TransponderId = "P278",
                IntervalType = 15,
                Consumption = 234.45,
                DialRead = "23RT",
                Direction = 2,
                ProjectedReadDate = DateTime.Now.Date,
                QualityCode = "QA23",
                ReadingType = "RD",
                Scenario = "",
                TOUID = 989,
                TOU_CriticalPeak = 22.2,
                TOU_OffPeak = 13.2,
                TOU_OnPeak = 11.2,
                TOU_Regular = 21,
                TOU_Shoulder1 = 20,
                TOU_Shoulder2 = 21.5,
                VolumeFactor = 15

            };
            var result = _iTallAmi.InsertOrMergeAmiAsync(tallAMiModel).Result;
            Assert.AreEqual(result, true);
        }


        /// <summary>
        /// Initialization of test case.
        /// </summary>
        [TestMethod]
        public void InsertOrMergeBatchAsync()
        {
            TallAmiModel tallAMiModel = new TallAmiModel()
            {
                CustomerId = "23",
                ClientId = 87,
                MeterId = "MET900",
                ServicePointId = "SP23",
                Timezone = "EST",
                CommodityId = 3,
                UOMId = 3,
                AccountNumber = "ACC500",
                AmiTimeStamp = DateTime.Now.Date,
                BatteryVoltage = "23V",
                TransponderPort = "P",
                TransponderId = "P278",
                IntervalType = 15,
                Consumption = 234.45,
                DialRead = "23RT",
                Direction = 2,
                ProjectedReadDate = DateTime.Now.Date,
                QualityCode = "QA23",
                ReadingType = "RD",
                Scenario = "",
                TOUID = 989,
                TOU_CriticalPeak = 22.2,
                TOU_OffPeak = 13.2,
                TOU_OnPeak = 11.2,
                TOU_Regular = 21,
                TOU_Shoulder1 = 20,
                TOU_Shoulder2 = 21.5,
                VolumeFactor = 15

            };
            List<TallAmiModel> list = new List<TallAmiModel>()
            {
                tallAMiModel
            };
            var result = _iTallAmi.InsertOrMergeBatchAsync(list).Result;
            Assert.AreEqual(result, true);
        }

        /// <summary>
        /// GetAmi test case.
        /// </summary>
        [TestMethod]
        public void GetAmi()
        {
            var result = _iTallAmi.GetAmi(DateTime.Now.Date, "MET900", "ACC500", 87);
            var firstOrDefault = result.FirstOrDefault();
            if (firstOrDefault != null) Assert.AreEqual(firstOrDefault.ClientId, 87);
            else Assert.AreEqual(null, null);
        }


        /// <summary>
        /// GetAmiList test case.
        /// </summary>
        [TestMethod]
        public void GetAmiList()
        {
            var result = _iTallAmi.GetAmiList(DateTime.Now.Date, DateTime.Now.AddDays(1).Date, "MET900", "ACC500", 87);
            var firstOrDefault = result.FirstOrDefault();
            if (firstOrDefault != null) Assert.AreEqual(firstOrDefault.ClientId, 87);
            else Assert.AreEqual(null, null);
        }

        /// <summary>
        /// Test case for DeleteReadingAsync method.
        /// </summary>
        [TestMethod]
        public async Task DeleteReadingAsync()
        {
            var result = await _iTallAmi.DeleteReadingAsync(87, "MET900");
            Assert.AreEqual(result, true);
        }
    }
}
