﻿using AO.Entities;
using Microsoft.Azure;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace AO.Business.Tests.IntegrationTests
{
    /// <summary>
    /// Contains test methods for RedisCacheProvider Class
    /// </summary>
    [TestClass]
    [Ignore]
    public class RedisCacheProviderTest
    {
        private RedisCacheProvider _redisCacheProvider;
        private readonly DcuAlarmTypeEntity _cacheValue = new DcuAlarmTypeEntity();
        private const string CacheKey = "RemoveKeyAfterIntegrationTest";
        private const int CacheDatabase = 1;

        /// <summary>
        /// Test Case Init
        /// </summary>
        [TestInitialize]
        public void Init()
        {
            _redisCacheProvider = new RedisCacheProvider();
        }

        /// <summary>
        /// Test Case for Set,Get,Remove and FlushAllKeys Methods
        /// </summary>
        [TestMethod]
        public void SetAccessRemoveFlushCacheItems()
        {
            _redisCacheProvider.Set(CacheKey, _cacheValue, CacheDatabase);
            _redisCacheProvider.Get<DcuAlarmTypeEntity>(CacheKey, CacheDatabase);
            var success = _redisCacheProvider.Remove(CacheKey, CacheDatabase);
            _redisCacheProvider.Set(CacheKey, _cacheValue, CacheDatabase);
            _redisCacheProvider.FlushAllKeys(CacheDatabase).Wait();
            _redisCacheProvider.FlushAllKeys().Wait();
            _redisCacheProvider.Set(CacheKey, _cacheValue, CacheDatabase);
            var connectionString = CloudConfigurationManager.GetSetting(StaticConfig.RedisCacheConnectionString);
            _redisCacheProvider.FlushAllKeys(CacheDatabase, connectionString).Wait();
            _redisCacheProvider.FlushAllKeys(connectionString).Wait();
            Assert.IsTrue(success);
        }

        /// <summary>
        /// Test Case for FlushAllKeys using connection string
        /// </summary>
        [TestMethod]
        public void FlushCacheItemsByConnection()
        {
            var connectionString = CloudConfigurationManager.GetSetting(StaticConfig.RedisCacheConnectionString);
            _redisCacheProvider.FlushAllKeys(connectionString).Wait();
            Assert.IsTrue(true);
        }

        /// <summary>
        /// Test Case for FlushAllKeys using connection string, database
        /// </summary>
        [TestMethod]
        public void FlushCacheItemsByConnectionByDatabase()
        {
            var connectionString = CloudConfigurationManager.GetSetting(StaticConfig.RedisCacheConnectionString);
            _redisCacheProvider.FlushAllKeys(CacheDatabase, connectionString).Wait();
            Assert.IsTrue(true);
        }
    }
}
