﻿using System;
using System.Threading.Tasks;
using AO.DataAccess;
using AO.DTO;
using AO.Entities;
using AutoMapper;
using CE.AO.Models;
using Microsoft.Practices.Unity;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Enums = CE.AO.Utilities.Enums;

namespace AO.Business.Tests.IntegrationTests
{
    /// <summary>
    /// Integration Test Class for Log Class
    /// </summary>

    [TestClass]
    public class OperationsFacadeTest : TestBase
    {
        private OperationsFacade _operationsFacade;
        private LogRepository _logRepository;
        /// <summary>
        /// Test Case Init
        /// </summary>
        [TestInitialize]
        public void Init()
        {
            _operationsFacade = UnityContainer.Resolve<OperationsFacade>();
            _logRepository = UnityContainer.Resolve<LogRepository>();
        }

        /// <summary>
        /// Test case for InsertOrMergeLogAsync method in Log class
        /// </summary>
        [TestMethod]
        public void InsertOrMergeLogAsync()
        {
            LogViewModel logViewModel = new LogViewModel()
            {
                MonthYear = Convert.ToInt32(DateTime.UtcNow.Month + "" + DateTime.UtcNow.Year),
                ClientId = "898989",
                MeterId = "TestMeter",
                AccountId = "TestAccount",
                CustomerId = "23424",
                ProcessingEventCount = 12,
                Module = "TestModule",
                ProcessingType = Enums.ProcessingType.File.ToString(),
                TimestampUtc = DateTimeOffset.UtcNow,
                Level = "Info"
            };

            var filterParams = new LogHistoryRequestDTO()
            {
                StartDateTime = DateTime.UtcNow.AddDays(-1),
                EndDateTime = DateTime.UtcNow.AddDays(1),
                ClientID = logViewModel.ClientId,
                Levels = new [] { logViewModel.Level},
                Modules = new []{ logViewModel.Module}
            };

            var isInserted = _operationsFacade.InsertOrMergeLogAsync(logViewModel).Result;
            var isFetched = _operationsFacade.GetLogHistory(filterParams);
            var isDelete = DeleteLogAsync(logViewModel).Result;

            Assert.AreEqual(isFetched != null,true);
            Assert.IsTrue(isInserted);
            Assert.IsTrue(isDelete);
        }

        /// <summary>
        /// Delete LogViewModel to Log table
        /// </summary>
        /// <param name="model">LogViewModel to be Deleted</param>
        /// <returns></returns>
        private async Task<bool> DeleteLogAsync(LogViewModel model)
        {
            await _logRepository.DeleteLogAsync(Mapper.Map<LogViewModel, LogEntity>(model));
            return await Task.Factory.StartNew(() => true);
        }
    }
}