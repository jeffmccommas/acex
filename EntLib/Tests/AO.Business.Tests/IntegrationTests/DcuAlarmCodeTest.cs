﻿using System;
using CE.AO.Models;
using Microsoft.Practices.Unity;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace AO.Business.Tests.IntegrationTests
{
	[TestClass]
	public class DcuAlarmCodeTest : TestBase
	{
		private DcuAlarmCode _dcuAlarmCode;

		[TestInitialize]
		public void Init()
		{
            _dcuAlarmCode = UnityContainer.Resolve<DcuAlarmCode>();
		}

        /// <summary>
        /// Test case for InsertOrMergeDcuAlarmCodeAsync method in DcuAlarmCode class
        /// </summary>
        [TestMethod]
		public void InsertOrMergeDcuAlarmCodeAsync()
		{
            DcuAlarmModel dcuAlarmModel = new DcuAlarmModel
            {
				ClientId = 89898989,
				AlarmTime = DateTime.UtcNow,
				DcuId = 1,
				AlarmCode = "Integration Test Delete after insert",
				ComponentType = "Test_Component",
				NotificationType = "Test_Notification",
				Severity = 100,
				UserFriendlyDescription = "Testing Entity"
			};

            // ReSharper disable once UnusedVariable
			var insertResult = _dcuAlarmCode.InsertOrMergeDcuAlarmCodeAsync(dcuAlarmModel).Result;
            // ReSharper disable once UnusedVariable
            var isDelete = _dcuAlarmCode.DeleteDcuAlarmCodeAsync(dcuAlarmModel).Result;
            var isDeleteWithPartition = _dcuAlarmCode.DeleteDcuAlarmCodeWithPartionAsync(dcuAlarmModel).Result;
            
            Assert.IsTrue(isDeleteWithPartition);
        }
    }
}
