﻿using System;
using CE.AO.Models;
using Microsoft.Practices.Unity;
using Microsoft.VisualStudio.TestTools.UnitTesting;


namespace AO.Business.Tests.IntegrationTests
{
    [TestClass]
    public class DcuCallDataTest : TestBase
    {
        private DcuCallData _dcuCallData;

        /// <summary>
        /// Test Case Init
        /// </summary>
        [TestInitialize]
        public void Init()
        {
            _dcuCallData = UnityContainer.Resolve<DcuCallData>();
        }

        /// <summary>
        /// Test case for InsertOrMergeDcuCallDataAsync method in MeterAccountMtu class
        /// </summary>
        [TestMethod]
        public void InsertOrMergeDcuCallDataAsync()
        {
            var dcuCallDataModel = new DcuCallDataModel
            {
                ClientId = 87,
                DcuId = 123,
                CallStart = DateTime.UtcNow,
                Cleared = true,
                HungUp = false,
                CallType = "AL",
                CallEnd = DateTime.UtcNow,
                Expected = 123,
                Received = 123
            };

            _dcuCallData.InsertOrMergeDcuCallDataAsync(dcuCallDataModel).Wait();
            _dcuCallData.DeleteDcuCallDataAsync(dcuCallDataModel).Wait();
        }
    }
}
