﻿using System;
using AO.BusinessContracts;
using CE.AO.Models;
using Microsoft.Practices.Unity;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace AO.Business.Tests.IntegrationTests
{
	/// <summary>
	/// Test Case Class for Account Lookup
	/// </summary>
	[TestClass]
	public class AccountUpdatesTest : TestBase
	{
		private IAccountUpdates _iaccountUpdates;

		/// <summary>
		/// Initialization of test case.
		/// </summary>
		[TestInitialize]
		public void Init()
		{
			_iaccountUpdates = UnityContainer.Resolve<IAccountUpdates>();
		}

		/// <summary>
		/// Test case for InsertOrMergeAccountLookupAsync method.
		/// </summary>
		[TestMethod]
		public void InsertOrMergeAccountLookupAsync()
		{
			AccountUpdatesModel accountUpdatesModel = new AccountUpdatesModel()
			{
				CustomerId = "500",
				ClientId = 87,
				AccountId = "Ac500",
				ServiceContractId = "SC23",
				MeterId = "MET900",
				IsLatest = true,
				PremiseId = "90",
				NewValue = "YT",
				OldValue = "XT",
				UpdateSentDate = DateTime.Now,
				UpdateType = "Y",
				UpdatedDate = DateTime.Now
			};
			var result = _iaccountUpdates.InsertOrMergeAccountUpdateAsync(accountUpdatesModel,true).Result;
			Assert.AreEqual(result, true);
		}

		/// <summary>
		/// Test case for InsertOrMergeAccountLookupAsync method.
		/// </summary>
		[TestMethod]
		public void GetAccountUpdate()
		{
			var result = _iaccountUpdates.GetAccountUpdate(87, "500", "Y", "SC23", true).Result;
			Assert.AreEqual(result.ClientId, 87);
		}

		/// <summary>
		/// Test case for DeleteHistoryAccountUpdateAsyc method.
		/// </summary>
		[TestMethod]
		public void DeleteHistoryAccountUpdateAsyc()
		{
			var result = _iaccountUpdates.DeleteHistoryAccountUpdateAsyc(87, "500", "Y", "SC23", true).Result;
			Assert.AreEqual(result, true);
		}

	}
}
