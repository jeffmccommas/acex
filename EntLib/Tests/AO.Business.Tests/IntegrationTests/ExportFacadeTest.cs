﻿using System.IO;
using AO.BusinessContracts;
using CE.AO.Models;
using CE.AO.Utilities;
using CsvHelper;
using Microsoft.Practices.Unity;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace AO.Business.Tests.IntegrationTests
{
    [TestClass]
    public class ExportFacadeTest : TestBase
    {
        private MeterType _meterType;

        [TestInitialize]
        public void Init()
        {
            _meterType = UnityContainer.Resolve<MeterType>();
        }

        [TestMethod]
        public void TestExport()
        {
            var connectionString =
                "DefaultEndpointsProtocol=http;AccountName=watermdmstgdev;AccountKey=NLiPTjsHibbVo6sPlgujN+qbHoh8gXBelSdlgKFWxzSiD4Mlx+mUoF/eLxUb4p0jCmpZyEiBjry1+GJbJawrRA==;";
            var blobName = "my_integration_test.csv";
            var containerName = "test2";
            var filterName = "test_meter_type";

            MeterTypeModel meterTypeModel = new MeterTypeModel
            {
                Active = true,
                Descripton = "Test Description",
                MeterReadingUnit = "Test Unit",
                MeterReadingUtility = "Test Utitlity",
                MeterTypeId = 9999999,
                Model = "Test Model",
                PsionDisplay = "Test Display",
                Registercount = 000,
                UnitEnumerator = 1212,
                UnitScalar = null,
                Vendor = "TEst Vendor"
            };

            _meterType.InsertOrMergeMeterTypeAsync(meterTypeModel).Wait();
            // Delete existing blob.
            BlobStorageManager.DeleteBlob(connectionString, containerName, blobName);

            // Make sure its gone.
            Assert.IsFalse(BlobStorageManager.BlobExists(connectionString, containerName, blobName));

            // Do the export.
            IExportFacade exportFacade = UnityContainer.Resolve<IExportFacade>(ParameterOverrides);

            exportFacade.ExportData(connectionString, containerName, blobName, "select meter_type_id as \"MeterTypeId\",active as \"Active\",description as \"Descripton\",meter_reading_units as \"MeterReadingUnit\",meter_reading_utility as \"MeterReadingUtility\",model as \"Model\",psion_display as \"PsionDisplay\",register_count as \"Registercount\",units_enumerator as \"UnitEnumerator\",units_scalar as \"UnitScalar\",vendor as \"Vendor\" from meter_type where table_name=\'meter_type\' and meter_type_id=9999999", filterName);

            // Make sure the .csv exists now.
            Assert.IsTrue(BlobStorageManager.BlobExists(connectionString, containerName, blobName));

            //check for the record with MeterTypeId is 9999999
            var blockBlob = BlobStorageManager.CreateAppendBlob(connectionString, containerName, blobName);
            var stream = blockBlob.OpenRead();
            var sr = new StreamReader(stream);
            using (var reader = new CsvReader(sr))
            {
                reader.Read();
                var records = reader.GetRecord<MeterTypeModel>();
                Assert.AreEqual(records.MeterTypeId, 9999999);
            }
            _meterType.DeleteMeterTypeAsync(meterTypeModel).Wait();
            BlobStorageManager.DeleteBlob(connectionString, containerName, blobName);

        }
    }
}
