﻿using AO.BusinessContracts;
using Microsoft.Practices.Unity;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace AO.Business.Tests.IntegrationTests
{
    /// <summary>
    /// Test Case Class for TrumpiaKeywordConfiguration 
    /// </summary>
    [TestClass]
    public class TrumpiaKeywordConfigurationTest : TestBase
    {
        private ITrumpiaKeywordConfiguration _iTrumpiaKeywordConfiguration;

        /// <summary>
        /// Initialization of test case.
        /// </summary>
        [TestInitialize]
        public void Init()
        {
            _iTrumpiaKeywordConfiguration = UnityContainer.Resolve<ITrumpiaKeywordConfiguration>();
        }

        /// <summary>
        /// Test case for GetTrumpiaKeywordConfiguration method.
        /// </summary>
        [TestMethod]
        public void GetTrumpiaKeywordConfiguration()
        {
            var result = _iTrumpiaKeywordConfiguration.GetTrumpiaKeywordConfiguration("abagdasarian2");
            Assert.AreEqual(result.TrumpiaKeyword, "abagdasarian2");
        }
    }
}
