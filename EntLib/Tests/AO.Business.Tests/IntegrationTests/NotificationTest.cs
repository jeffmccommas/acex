﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AO.BusinessContracts;
using CE.AO.Models;
using CE.RateModel;
using Microsoft.Practices.Unity;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Enums = CE.RateModel.Enums;

namespace AO.Business.Tests.IntegrationTests
{
    /// <summary>
    /// Test Class for Notification
    /// </summary>
    [TestClass]
    public class NotificationTest : TestBase
    {
        private INotification _notificationManager;

        /// <summary>
        /// Test Case Init
        /// </summary>
        [TestInitialize]
        public void Init()
        {
            _notificationManager = UnityContainer.Resolve<INotification>();
        }

        /// <summary>
        /// Test case to send notification using email/sms
        /// </summary>
        [TestMethod]
        public void ProcessMessage()
        {
            var subscriptionModel = new SubscriptionModel
            {
                ClientId = 87,
                CustomerId = "1234",
                AccountId = "1234",
                ProgramName = "testprogram",
                InsightTypeName = "",
                PremiseId = "",
                ServiceContractId = "",
                ServicePointId = "",
                IsEmailOptInCompleted = true,
                IsSmsOptInCompleted = true,
                IsSelected = true,
                Channel = "Email",
                AllowedChannels = "",
                SubscriberThresholdValue = 1234,
                CommodityKey = "Water",
                ThresholdType = "",
                ThresholdMin = 132,
                ThresholdMax = 136,
                IsDoubleOptIn = true,
                UomId = 12,
                Type = "",
                IsLatest = true
            };
            var subscriptionModelList = new List<SubscriptionModel> { subscriptionModel };

            var insightModel = new InsightModel
            {
                ClientId = 87,
                AccountId = "1234",
                ServiceContractId = "",
                BilllDays = 23,
                BtdCost = 2332,
                BtdProjectedCost = 3544,
                Usage = 32,
                SupressInsight = true,
                AccountLevelCostThresholdExceed = true,
                ServiceLevelUsageThresholdExceed = true,
                ServiceLevelCostThresholdExceed = true,
                DayThresholdExceed = true,
                ServiceLevelTiered1ThresholdApproaching = false,
                ServiceLevelTieredThresholdExceed = false,
                ServiceDays = 32,
                CtdCost = 4545,
                CtdProjectedCost = 5678,
                NotifyTime = DateTime.Now.ToShortDateString(),
                ProgramName = "testprogram",
                AsOfEvaluationDate = DateTime.Now,
                AsOfEvaluationId = Guid.NewGuid(),
                BillCycleStartDate = "12 12",
                BillCycleEndDate = " 12 12 12",
                Type = "ABC",
                IsLatest = true
            };
            var insightSettings = new InsightSettings
            {
                InsightName = "BillToDate",
                UtilityInsightName = "BillToDate",
                AllowedCommunicationChannel = "Email",
                DefaultCommunicationChannel = "Email",
                DefaultFrequency = "Daily",
                DefaultValue = "Daily",
                EmailTemplateId = "1",
                SmsTemplateId = "87_1",
                NotifyTime = "13:00",
                NotificationDay = "Monday",
                TrumpiaKeyword = "TrumpiaKeywordTest",
                Level = "Basic",
                CommodityType = "Water",
                ThresholdType = "TestThresholdType",
                ThresholdMin = "50",
                ThresholdMax = "100"
            };

            var insightSettingsList = new List<InsightSettings> { insightSettings };

            var programSettings = new ProgramSettings
            {
                ProgramName = "testprogram",
                UtilityProgramName = "testprogram",
                FileExportRequired = false,
                DoubleOptInRequired = false,
                TrumpiaKeyword = "trumpiatestkeyword",
                Insights = insightSettingsList
            };
            var programSettingsList = new List<ProgramSettings> { programSettings };


            var clientSettings = new ClientSettings
            {
                ClientId = 87,
                TimeZone = "ADT",
                IsAmiIntervalStart = false,
                IsDstHandled = false,
                MinimumDaysForInsightNotification = 1,
                CalculationScheduleTime = "",
                NotificationReportScheduleTime = "",
                InsightReportScheduleTime = "",
                ForgotPasswordEmailTemplateId = "",
                OptInEmailUserName = "UserNameTest",
                OptInEmailPassword = "test@123",
                OptInEmailFrom = "testemail@gmail.com",
                RegistrationConfirmationEmailTemplateId = "",
                OptInEmailConfirmationTemplateId = "",
                OptInSmsConfirmationTemplateId = "",
                TrumpiaShortCode = "",
                TrumpiaApiKey = "TrumpiaTestKey",
                TrumpiaUserName = "TrumpiaTestUserName",
                TrumpiaContactList = "",
                UnmaskedAccountIdEndingDigit = 1,
                Programs = new List<ProgramSettings>(programSettingsList)
            };
            var customerModel = new CustomerModel
            {
                ClientId = 87,
                CustomerId = "1234",
                Country = "USA",
                Source = "",
                IsBusiness = false,
                AddressLine1 = "C Block",
                AddressLine2 = "Flat no B-804",
                AddressLine3 = "Wakad",
                City = "Pune",
                State = "New york",
                PostalCode = "NY001",
                FirstName = "Sharad",
                LastName = "Saini",
                Phone1 = "9911559988",
                Phone2 = "8975757012",
                EmailAddress = "tushar.wadhavane@saviantconsulting.com",
                CustomerType = "Good",
                TrumpiaSubscriptionId = "87",
                TrumpiaRequestDetailId = ""
            };
            var billingModel = new BillingModel
            {
                ClientId = 87,
                CustomerId = "1234",
                AccountId = "1234",
                PremiseId = "12",
                AmiStartDate = DateTime.Now,
                AmiEndDate = DateTime.Now,
                BillCycleScheduleId = "BS12122",
                RateClass = "Rate Class",
                ServicePointId = "SP0012",
                Source = "Rate Class",
                MeterType = "",
                UOMId = 12,
                TotalUsage = 1232323,
                StartDate = DateTime.Now,
                EndDate = DateTime.Today.AddDays(12),
                BillDays = 12,
                ReadQuality = "",
                TotalCost = 122323,
                CommodityId = 2,
                MeterId = " ",
                BillPeriodType = 2,
                ReplacedMeterId = "",
                ReadDate = DateTime.Now,
                ServiceContractId = "SC001",
                IsFault = false,
                IsLatest = true
            };
            var tierBoundary = new TierBoundary
            {
                BaseOrTier = Enums.BaseOrTier.Tier6,
                TimeOfUse = Enums.TimeOfUse.OnPeak,
                Season = Enums.Season.SeasonB,
                DaysIntoSeason = 1,
                SeasonFactor = 1,
                Threshold = 2,
                SecondaryThreshold = 3
            };
            var listTierBoundary = new List<TierBoundary> { tierBoundary };
            var dicTierBoundary = new Dictionary<string, List<TierBoundary>>
            {
                {"SC001", listTierBoundary}
            };
            var calculationModel = new CalculationModel
            {
                ClientId = 87,
                AccountId = "1234",
                CustomerId = "1234",
                MeterId = "",
                ServiceContractId = "",
                CommodityId = 1,
                Unit = 2,
                Cost = 22.3,
                ProjectedCost = 23,
                ProjectedUsage = 32,
                Usage = 2,
                BillDays = 1,
                BillCycleStartDate = "08/18/2016",
                BillCycleEndDate = "08/20/2016",
                AsOfAmiDate = DateTime.Now.Date,
                AsOfCalculationDate = DateTime.Now,
                RateClass = "",
                AverageDailyUsage = 23,
                AverageDailyCost = 23,
                Type = "",
                IsLatest = false

            };

            var calculationModelList = new List<CalculationModel> { calculationModel };

            var result = Task.Run(
                    () =>
                        _notificationManager.ProcessMessage(subscriptionModelList, insightModel, clientSettings,
                            customerModel, billingModel, dicTierBoundary, calculationModelList)).ContinueWith(t =>
                            {
                                if (t.Exception != null)
                                {
                                }
                            });
            result.Wait();

            Assert.AreEqual(true, true);
        }

        /// <summary>
        /// test case to fetch all the notification details of specific date from the database
        /// </summary>
        [TestMethod]
        public void GetDayNotifications()
        {
            int clientId = 87;
            string accountId = "1234";
            string customerId = "1234";
            DateTime asOfDate = DateTime.UtcNow;
            var result = _notificationManager.GetDayNotifications(clientId, customerId, accountId, asOfDate);
            var firstOrDefault = result.FirstOrDefault();
            if (firstOrDefault != null) Assert.AreEqual(firstOrDefault.ClientId, 87);
        }

        /// <summary>
        /// Test case to fetch the notification detail from the database
        /// </summary>
        [TestMethod]
        public void GetNotificationSync()
        {
            int clientId = 87;
            string accountId = "1234";
            string customerId = "1234";
            string programName = "testprogram";
            string serviceContractId = "";
            string insightName = "BillToDate";
            string defaultCommunicationChannel = "Email";
            string date = "08/18/2016";
            var result = _notificationManager.GetNotificationSync(clientId, customerId, accountId, programName, serviceContractId, insightName, defaultCommunicationChannel, true, date);
            var firstOrDefault = result;
            if (firstOrDefault != null) Assert.AreEqual(firstOrDefault.ClientId, 87);
        }

        /// <summary>
        /// Test case to update notification with email event like delivered, bounce etc.
        /// </summary>
        [TestMethod]
        public void SaveNotificationFromEmail()
        {
            var notificationModel = new NotificationModel
            {
                ClientId = 87,
                AccountId = "12345",
                ServiceContractId = "",
                ProgramName = "testProgram",
                Insight = "BillToDate",
                IsNotified = false,
                IsDelivered = false,
                IsDropped = false,
                IsBounced = false,
                IsOpened = false,
                IsClicked = false,
                IsScheduled = false,
                Channel = "",
                TemplateId = "",
                CustomerId = "1",
                EvaluationType = "",
                NotifiedDateTime = DateTime.UtcNow,
                TrumpiaRequestDetailId = "",
                IsLatest = true
            };

            _notificationManager.SaveNotification(notificationModel);
        }

        /// <summary>
        /// Test case to insert or update latest NotificationModel to database
        /// </summary>
        [TestMethod]
        public void SaveNotification()
        {
            // Arrange
            var notificationModel = new NotificationModel
            {
                ClientId = 87,
                AccountId = "12345",
                ServiceContractId = "",
                ProgramName = "testProgram",
                Insight = "BillToDate",
                IsNotified = false,
                IsDelivered = false,
                IsDropped = false,
                IsBounced = false,
                IsOpened = false,
                IsClicked = false,
                IsScheduled = false,
                Channel = "",
                TemplateId = "",
                CustomerId = "1",
                EvaluationType = "",
                NotifiedDateTime = DateTime.UtcNow,
                TrumpiaRequestDetailId = "",
                IsLatest = false // Arrange as false so we can assert SaveNotification() forced true
            };

            // Act
            _notificationManager.SaveNotification(notificationModel, true);
            var result = _notificationManager.GetNotificationSync(notificationModel);

            // Assert
            Assert.IsNotNull(result);
            Assert.IsTrue(result.IsLatest);
        }

        /// <summary>
        /// Test method to get all notifications for all clients
        /// </summary>
        [TestMethod]
        public void GetScheduledNotifications()
        {
            var result = _notificationManager.GetScheduledNotifications();

            Assert.IsNotNull(result, "Expected at least one notification to be found");
            Assert.AreEqual(result.First().Channel, "SMS");
        }

        /// <summary>
        /// Test method to fetch the sms template from database
        /// </summary>
        [TestMethod]
        public void GetSmsTemplateFromDb()
        {
            var result = _notificationManager.GetSmsTemplateFromDB(87, "BillToDate", "87_1");
            Assert.AreEqual("87_1", result.Result.TemplateId);
        }

        /// <summary>
        /// Test case for DeleteCustomerAsync method.
        /// </summary>
        [TestMethod]
        public void DeleteNotificationAsync()
        {
            _notificationManager.DeleteNotificationAsync(87, "1234", "1234");
        }
    }
}
