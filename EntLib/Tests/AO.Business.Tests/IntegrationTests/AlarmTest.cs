﻿using System;
using CE.AO.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.Practices.Unity;

namespace AO.Business.Tests.IntegrationTests
{
    /// <summary>
    /// Integration Test Class for Alarm Class
    /// </summary>
    [TestClass]
  public class AlarmTest : TestBase
    {
        private Alarm _alarm;

        /// <summary>
        /// Test Case Init
        /// </summary>
        [TestInitialize]
        public void Init()
        {
            _alarm = UnityContainer.Resolve<Alarm>();
        }

        /// <summary>
        /// Test case for InsertOrMergeAlarmAsync method in Alarm class
        /// </summary>
        [TestMethod]
        public void InsertOrMergeAlarmAsync()
        {
            var alarmModel = new AlarmModel
            {
                ClientId = 89898989,
                AlarmTime = DateTime.UtcNow,
                AlarmGroupId = 99898989,
                AlarmGroupName = "TestGroupName",
                AlarmId = 79898989,
                AlarmSourceId = 69898989,
                AlarmSourceType = "TestSourceType",
                AlarmSourceTypeId = 59898989,
                AlarmTypeId = 49898989,
                AlarmTypeName = "TestAlarmTypeName"
            };
            
            _alarm.InsertOrMergeAlarmAsync(alarmModel).Wait();
            var isDelete = _alarm.DeleteAlarmAsync(alarmModel).Result;
            Assert.IsTrue(isDelete);

        }
    }
}
