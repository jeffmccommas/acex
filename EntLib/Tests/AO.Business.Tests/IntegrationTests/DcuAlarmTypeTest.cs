﻿using System.Threading.Tasks;
using CE.AO.Models;
using Microsoft.Practices.Unity;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace AO.Business.Tests.IntegrationTests
{
    [TestClass]
    public class DcuAlarmTypeTest : TestBase
    {
        private DcuAlarmType _dcuAlarmType;

        [TestInitialize]
        public void Init()
        {
            _dcuAlarmType = UnityContainer.Resolve<DcuAlarmType>();
        }

        /// <summary>
        /// Test case for InsertOrMergeDcuAlarmCodeAsync method in DcuAlarmCode class
        /// </summary>
        [TestMethod]
        public void GetDcuAlarmMapping()
        {
            var dcuAlarmMappingModel = new DcuAlarmTypeModel
            {
                TableName = "dcu_alarm_types",
                AlarmCode = "Dcu_Call_Me",
                ComponentType = "Test_Component",
                NotificationType = "Test_Notification",
                Severity = 100,
                UserFriendlyDescription = "Testing Entity"
            };

            // ReSharper disable once UnusedVariable
            var result = _dcuAlarmType.InsertOrMergeDcuAlarmTypeAsync(dcuAlarmMappingModel);
            Task.WaitAll(result);
            var getResult = _dcuAlarmType.GetDcuAlarmType(dcuAlarmMappingModel.AlarmCode);

            // ReSharper disable once UnusedVariable
            var isDelete = _dcuAlarmType.DeleteDcuAlarmTypeAsync(dcuAlarmMappingModel).Result;
            Assert.AreEqual(getResult.AlarmCode, dcuAlarmMappingModel.AlarmCode);
        }

    }
}
