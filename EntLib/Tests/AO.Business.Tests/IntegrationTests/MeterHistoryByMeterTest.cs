﻿using System.Linq;
using System.Threading.Tasks;
using AO.BusinessContracts;
using CE.AO.Models;
using Microsoft.Practices.Unity;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace AO.Business.Tests.IntegrationTests
{
	/// <summary>
	/// Test class for MeterHistoryByMeter
	/// </summary>
	[TestClass]
	public class MeterHistoryByMeterTest : TestBase
	{
		private IMeterHistoryByMeter _meterHistoryByMeter;

		/// <summary>
		/// Test Case Init
		/// </summary>
		[TestInitialize]
		public void Init()
		{
			_meterHistoryByMeter = UnityContainer.Resolve<IMeterHistoryByMeter>();
		}

		/// <summary>
		/// Test case for InsertOrMergeMeterHistoryByMeterAsync method in MeterHistoryByMeter class
		/// </summary>
		[TestMethod]
		public void InsertOrMergeMeterHistoryByMeterAsync()
		{
			var meterrHistoryByMeter = GetMeterHistoryByMeter();
			var result = _meterHistoryByMeter.InsertOrMergeMeterHistoryByMeterAsync(meterrHistoryByMeter).Result;
			Assert.AreEqual(result, true);
		}

		/// <summary>
		/// Unit Test method for GetMeterHistoryByMeterList
		/// </summary>
		[TestMethod]
		public void GetMeterHistoryByMeterList()
		{
			var pageSize = 3;
			byte[][] token = { null };
			var clientId = 87;

			var result = _meterHistoryByMeter.GetMeterHistoryByMeterList(pageSize, ref token[0], clientId);
			var firstOrDefault = result.FirstOrDefault();
			if (firstOrDefault != null) Assert.AreEqual(firstOrDefault.ClientId, 87);
			else Assert.AreEqual(null, null);
		}

		/// <summary>
		/// Test case for DeleteMeterHistoryByMeterAsync method.
		/// </summary>
		[TestMethod]
		public async Task DeleteMeterHistoryByMeterAsync()
		{
			var meterHistoryByMeter = GetMeterHistoryByMeter();
			await _meterHistoryByMeter.DeleteMeterHistoryByMeterAsync(meterHistoryByMeter);
			Assert.AreEqual(true, true);
		}

		/// <summary>
		/// To get the MeterHistoryByMeter model
		/// </summary>
		/// <returns>MeterHistoryByMeterModel</returns>
		private static MeterHistoryByMeterModel GetMeterHistoryByMeter()
		{
			return new MeterHistoryByMeterModel
			{
				ClientId = 87,
				MeterId = 1,
				MeterSerialNumber = "1",
				Active = true,
				AccountNumber = "Accn1"
			};
		}
	}
}
