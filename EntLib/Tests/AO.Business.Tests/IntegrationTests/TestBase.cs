﻿using System;
using AO.BusinessContracts;
using AO.DataAccess;
using AO.DataAccess.Contracts;
using AO.Registrar;
using CE.AO.Logging;
using CE.AO.Models;
using Microsoft.Practices.Unity;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace AO.Business.Tests.IntegrationTests
{
    [TestClass]
    public class TestBase : IDisposable
    {
        protected ICassandraRepository CassandraRepository;
        protected ICacheProvider CacheProvider;
        protected UnityContainer UnityContainer;
        protected ParameterOverrides ParameterOverrides;

        /// <summary>
        /// Test Case Init
        /// </summary>
        public TestBase()
        {
            var container = new UnityContainer();
            new DataStorageRegistrar().Initialize<TransientLifetimeManager>(container);
            UnityContainer = container;

            CassandraRepository = UnityContainer.Resolve<CassandraRepository>();
            CacheProvider = UnityContainer.Resolve<ICacheProvider>();
            UnityContainer.RegisterInstance(typeof(LogModel), new LogModel() {DisableLog = true});

            ParameterOverrides = new ParameterOverrides
            {
                {"logger", new LogModelBypassLogger()}
            };
        }

        protected virtual void Dispose(bool isDisposing)
        {
            if (isDisposing)
            {
                UnityContainer.Dispose();
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}
