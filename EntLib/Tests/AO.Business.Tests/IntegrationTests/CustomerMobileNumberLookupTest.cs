﻿using System.Linq;
using AO.BusinessContracts;
using CE.AO.Models;
using Microsoft.Practices.Unity;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace AO.Business.Tests.IntegrationTests
{
    /// <summary>
    /// Test Case Class for CustomerMobileNumberLookup 
    /// </summary>
    [TestClass]
    public class CustomerMobileNumberLookupTest : TestBase
    {
        private ICustomerMobileNumberLookup _iCustomerMobileNumberLookup;

        /// <summary>
        /// Initialization of test case.
        /// </summary>
        [TestInitialize]
        public void Init()
        {
            _iCustomerMobileNumberLookup = UnityContainer.Resolve<ICustomerMobileNumberLookup>();
        }

        /// <summary>
        /// Test case for InsertOrMergeCustomerAsync method.
        /// </summary>
        [TestMethod]
        public void InsertOrMergeCustomerMobileNumberLookupAsync()
        {
            var customerMobileNumberLookupModel = new CustomerMobileNumberLookupModel()
            {
                CustomerId = "23",
                ClientId = 87,
                AccountId = "ACC24",
                MobileNumber = "9028179781"
            };
            _iCustomerMobileNumberLookup.InsertOrMergeCustomerMobileNumberLookupAsync(customerMobileNumberLookupModel).Wait();
        }

        /// <summary>
        /// Test case for GetCustomerMobileNumberLookup method.
        /// </summary>
        [TestMethod]
        public void GetCustomerMobileNumberLookup()
        {
            var result = _iCustomerMobileNumberLookup.GetCustomerMobileNumberLookup(87, "9028179781", "ACC24");
            Assert.AreEqual(result.ClientId, 87);
        }


        /// <summary>
        /// Test case for GetCustomerMobileNumberLookup method.
        /// </summary>
        [TestMethod]
        public void GetAllCustomerMobileNumberLookup()
        {
            var result = _iCustomerMobileNumberLookup.GetAllCustomerMobileNumberLookup(87, "9028179781");
            var customerMobileNumberLookupModel = result.FirstOrDefault();
            if (customerMobileNumberLookupModel != null) Assert.AreEqual(customerMobileNumberLookupModel.ClientId, 87);
        }

        /// <summary>
        /// Test case for DeleteCustomerMobileNumberLookupAsync method.
        /// </summary>
        [TestMethod]
        public void DeleteCustomerMobileNumberLookupAsync()
        {
            _iCustomerMobileNumberLookup.DeleteCustomerMobileNumberLookupAsync(87, "9028179781", "ACC24").Wait();
        }
    }
}
