﻿using System.Linq;
using AO.Business.Tests.ObjectBuilders;
using AO.Entities;
using Microsoft.Practices.Unity;
using Microsoft.VisualStudio.TestTools.UnitTesting;


namespace AO.Business.Tests.IntegrationTests
{
    [TestClass]
    public class MeterAccountMtuTest : TestBase
    {
        private MeterAccountMtu _meterAccountMtu;

        /// <summary>
        /// Test Case Init
        /// </summary>
        [TestInitialize]
        public void Init()
        {
            _meterAccountMtu = UnityContainer.Resolve<MeterAccountMtu>();
        }

        /// <summary>
        /// Test case for InsertOrMergeMeterAccountMtuAsync method in MeterAccountMtu class
        /// </summary>
        [TestMethod]
        public void InsertOrMergeMeterAccountMtuAsync()
        {
            // Arrange
            var meterAccountMtuModel = new MeterAccountMtuModelBuilder().WithMtuType(10).Build();

            // Act
            _meterAccountMtu.InsertOrMergeMeterAccountMtuAsync(meterAccountMtuModel).Wait();
            var result =
                CassandraRepository.GetSingle<MeterAccountHistoryEntity>(
                    Constants.CassandraTableNames.MeterHistoryByAccount,
                    a =>
                        a.AccountNumber == meterAccountMtuModel.AccountNumber &&
                        a.ClientId == meterAccountMtuModel.ClientId);
            var isdelete = _meterAccountMtu.DeleteMeterAccountMtuAsync(meterAccountMtuModel).Result;

            // Assert
            Assert.IsTrue(isdelete, "Failed to cleanup MeterAccountMtu test data.");
            Assert.AreEqual("WRE", result.MtuType, $"Expected MtuType to be [WRE].  Found [{result.MtuType}].");  // 10 is 'WRE MTU Type'
            Assert.IsFalse(string.IsNullOrEmpty(meterAccountMtuModel.MtuTypeDescription), "Expected MtuTypeDescription to be populated.");
        }

        [TestMethod]
        public void GetInactiveAccountMeter()
        {
            var activeAccounts =
                CassandraRepository.GetAsync<MeterAccountHistoryEntity>(
                    Constants.CassandraTableNames.MeterHistoryByAccount, x => x.Active);

            var totalAccounts =
                CassandraRepository.GetAsync<MeterAccountHistoryEntity>(
                    Constants.CassandraTableNames.MeterHistoryByAccount);

            var inactiveAccounts =
                CassandraRepository.GetAsync<MeterAccountHistoryEntity>(
                    Constants.CassandraTableNames.MeterHistoryByAccount, x => !x.Active);
            
            // Assert 
            Assert.IsTrue(activeAccounts.Result.All(r=>r.Active), "Expected all accounts to be active.");
            Assert.AreEqual(activeAccounts.Result.Count + inactiveAccounts.Result.Count, totalAccounts.Result.Count);
        } 
    }
}
