﻿using System;
using System.Linq;
using AO.Business.Tests.ObjectBuilders;
using AO.DataAccess.Contracts;
using Microsoft.Practices.Unity;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace AO.Business.Tests.IntegrationTests
{
    [TestClass]
    public class NotificationRepositoryIntegrationTest : TestBase
    {
        private readonly INotificationRepository _notificationRepository;
        private int _clientId = 1234;
        private string _accountId = "4321";
        private string _customerId = "4321";

        public NotificationRepositoryIntegrationTest()
        {
            _notificationRepository = UnityContainer.Resolve<INotificationRepository>();
        }

        [TestCleanup]
        public void NotificationRepositoryTestCleanup()
        {
            // Act - Primary responsibility is test cleanup but also tests the deletenotification method.
            _notificationRepository.DeleteNotification(_clientId, _customerId, _accountId);
            var result = _notificationRepository.GetNotifications(_clientId, _customerId, _accountId).Result;

            // Assert
            Assert.IsFalse(result.Any());
        }

        [TestMethod]
        public void SaveNotificationUnitTest()
        {
            // Arrange
            var notificationEntity = new NotificationBuilder().WithClientId(_clientId).WithCustomerId(_customerId).WithAccountId(_accountId).Build();
            var startDate = DateTime.UtcNow.AddSeconds(-1);

            // Act
            _notificationRepository.InsertOrMergeNotification(notificationEntity).Wait();
            var result = _notificationRepository.GetNotification(notificationEntity).Result;

            // Assert
            Assert.IsNotNull(result, "Expected notification entity to be non null.");
            Assert.IsTrue(startDate < result.LastModifiedDate, $"Expected last modified date to be greater than [{startDate}]");
            Assert.IsTrue(result.LastModifiedDate < DateTime.UtcNow.AddSeconds(1), "Expected last modified date to be less than current time.");
        }

        [TestMethod]
        public void GetNotifications()
        {
            // Arrange
            var n1 = new NotificationBuilder().WithClientId(_clientId).WithCustomerId(_customerId).WithAccountId(_accountId).Build();
            var n2 = new NotificationBuilder().WithClientId(_clientId)
                .WithCustomerId(_customerId)
                .WithAccountId(_accountId)
                .WithNotificationDate(DateTime.UtcNow.AddDays(-1))
                .Build();

            // Act
            _notificationRepository.InsertOrMergeNotification(n1).Wait();
            _notificationRepository.InsertOrMergeNotification(n2).Wait();
            var result = _notificationRepository.GetNotifications().Result;

            // Assert
            Assert.IsTrue(result.Count(
                r => r.ClientId == n1.ClientId || r.AccountId == n2.AccountId) > 2,
                $"Expected to find at least two notification entities with clientId [{n1.ClientId}] and accountId [{n2.AccountId}]");
        }

        [TestMethod]
        public void GetNotificationsWithinDateRangeByClientUnitTest()
        {
            // Arrange
            var n1 = new NotificationBuilder().WithClientId(_clientId).WithCustomerId(_customerId).WithAccountId(_accountId).Build();
            var n2 = new NotificationBuilder().WithClientId(_clientId)
                .WithCustomerId(_customerId)
                .WithAccountId(_accountId)
                .WithNotificationDate(DateTime.UtcNow.AddDays(25))
                .Build();

            // Act
            _notificationRepository.InsertOrMergeNotification(n1).Wait();
            _notificationRepository.InsertOrMergeNotification(n2).Wait();
            var result = _notificationRepository.GetNotifications(_clientId, DateTime.UtcNow, DateTime.UtcNow.AddDays(20)).Result;

            // Assert
            Assert.IsTrue(result.Any(r => r.ClientId == _clientId && r.AccountId == _accountId));
            Assert.IsFalse(result.Any(r => r.NotifiedDateTime == n2.NotifiedDateTime),
                "Expected to find no notifications with date of '{n2.NotificationDateTime}' because it falls outside the search date range.");
        }
    }
}
