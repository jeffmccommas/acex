﻿using System;
using System.Configuration;
using System.IO;
using System.Linq;
using AO.BusinessContracts;
using CE.AO.Models;
using CE.AO.Models.RawFileModels;
using CsvHelper;
using Microsoft.Practices.Unity;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Blob;
using Enums = CE.AO.Utilities.Enums;

namespace AO.Business.Tests.IntegrationTests
{
    /// <summary>
    /// Test Case Class for Account Lookup
    /// </summary>
    [TestClass]
    public class AmiTest : TestBase
    {
        private IAmi _iAmi;

        /// <summary>
        /// Initialization of test case.
        /// </summary>
        [TestInitialize]
        public void Init()
        {
            _iAmi = UnityContainer.Resolve<IAmi>();
        }

        /// <summary>
        /// Gets the CloudBlockBlob using container name and file name
        /// </summary>
        /// <param name="fileName">Name of the blob</param>
        /// <param name="containerName">Name of the container in which the blob resides</param>
        /// <returns></returns>
        public ICloudBlob GetBlockBlob(string fileName, string containerName)
        {
            var container = CloudBlobClient.GetContainerReference(containerName);
            return container.GetBlockBlobReference(fileName);
        }

        /// <summary>
        /// CloudBlobClient private variable
        /// </summary>
        private CloudBlobClient _cloudBlobClient;
        public CloudBlobClient CloudBlobClient
        {
            get
            {
                if (_cloudBlobClient != null) return _cloudBlobClient;
                var storageAccount =
                    CloudStorageAccount.Parse(ConfigurationManager.AppSettings["AzureStorageConnectionString"]);
                _cloudBlobClient = storageAccount.CreateCloudBlobClient();
                return _cloudBlobClient;
            }
        }

        /// <summary>
        /// Test case for InsertAmiAsync_15Min method.
        /// </summary>
        [TestMethod]
        public void InsertAmiAsync_15Min()
        {
            var blockBlob = GetBlockBlob("EST_AMI_87_15min_13JuneuTushar-Test.csv", "client87");
            var stream = blockBlob.OpenRead();
            var sr = new StreamReader(stream);
            using (var reader = new CsvReader(sr))
            {
                reader.Read();
                var record = reader.GetRecord<RawAmi15MinuteIntervalModel>();
                var ami15MinModel = AutoMapper.Mapper.Map<RawAmi15MinuteIntervalModel, Ami15MinuteIntervalModel>(record);
                ami15MinModel.IntervalType = 15;
                var result = _iAmi.InsertAmiAsync(ami15MinModel).Result;
                Assert.AreEqual(result, true);
            }
        }

        /// <summary>
        /// Test case for InsertAmiAsync_30Min method.
        /// </summary>
        [TestMethod]
        public void InsertAmiAsync_30Min()
        {
            var blockBlob = GetBlockBlob("EST Timezone AMI_87_30min_Valid Data - Tush_Test.csv", "client87");
            var stream = blockBlob.OpenRead();
            var sr = new StreamReader(stream);
            using (var reader = new CsvReader(sr))
            {
                reader.Read();
                var record = reader.GetRecord<RawAmi30MinuteIntervalModel>();
                var ami30MinModel = AutoMapper.Mapper.Map<RawAmi30MinuteIntervalModel, Ami30MinuteIntervalModel>(record);
                ami30MinModel.IntervalType = 30;
                var result = _iAmi.InsertAmiAsync(ami30MinModel).Result;
                Assert.AreEqual(result, true);
            }
        }

        /// <summary>
        /// Test case for InsertAmiAsync_60Min method.
        /// </summary>
        [TestMethod]
        public void InsertAmiAsync_60Min()
        {
            var blockBlob = GetBlockBlob("AMIEnd_EST_AMI_87_60min-Does not Handles DST1_TestTush1.csv", "client87");
            var stream = blockBlob.OpenRead();
            var sr = new StreamReader(stream);
            using (var reader = new CsvReader(sr))
            {
                reader.Read();
                var record = reader.GetRecord<RawAmi60MinuteIntervalModel>();
                var ami60MinModel = AutoMapper.Mapper.Map<RawAmi60MinuteIntervalModel, Ami60MinuteIntervalModel>(record);
                ami60MinModel.IntervalType = 60;
                var result = _iAmi.InsertAmiAsync(ami60MinModel).Result;
                Assert.AreEqual(result, true);
            }
        }

        /// <summary>
        /// Test case for InsertAmiAsync_60Min method.
        /// </summary>
        [TestMethod]
        public void InsertOrMergeAmiAsync_60Min()
        {
            var blockBlob = GetBlockBlob("AMIEnd_EST_AMI_87_60min-Does not Handles DST1_TestTush1.csv", "client87");
            var stream = blockBlob.OpenRead();
            var sr = new StreamReader(stream);
            using (var reader = new CsvReader(sr))
            {
                reader.Read();
                var record = reader.GetRecord<RawAmi60MinuteIntervalModel>();
                var ami60MinModel = AutoMapper.Mapper.Map<RawAmi60MinuteIntervalModel, Ami60MinuteIntervalModel>(record);
                ami60MinModel.IntervalType = 60;
                var result = _iAmi.InsertOrMergeAmiAsync(ami60MinModel, Enums.IntervalType.Sixty).Result;
                Assert.AreEqual(result, true);
            }
        }

        /// <summary>
        /// Test case for InsertAmiAsync_30Min method.
        /// </summary>
        [TestMethod]
        public void InsertOrMergeAmiAsync_30Min()
        {
            var blockBlob = GetBlockBlob("EST Timezone AMI_87_30min_Valid Data - Tush_Test.csv", "client87");
            var stream = blockBlob.OpenRead();
            var sr = new StreamReader(stream);
            using (var reader = new CsvReader(sr))
            {
                reader.Read();
                var record = reader.GetRecord<RawAmi30MinuteIntervalModel>();
                var ami30MinModel = AutoMapper.Mapper.Map<RawAmi30MinuteIntervalModel, Ami30MinuteIntervalModel>(record);
                ami30MinModel.IntervalType = 30;
                var result = _iAmi.InsertOrMergeAmiAsync(ami30MinModel, Enums.IntervalType.Thirty).Result;
                Assert.AreEqual(result, true);
            }
        }

        /// <summary>   
        /// Test case for InsertAmiAsync_15Min method.
        /// </summary>
        [TestMethod]
        public void InsertOrMergeAmiAsync_15Min()
        {
            var blockBlob = GetBlockBlob("EST_AMI_87_15min_13JuneuTushar-Test.csv", "client87");
            var stream = blockBlob.OpenRead();
            var sr = new StreamReader(stream);
            using (var reader = new CsvReader(sr))
            {
                reader.Read();
                var record = reader.GetRecord<RawAmi15MinuteIntervalModel>();
                var ami15MinModel = AutoMapper.Mapper.Map<RawAmi15MinuteIntervalModel, Ami15MinuteIntervalModel>(record);
                ami15MinModel.IntervalType = 15;
                var result = _iAmi.InsertOrMergeAmiAsync(ami15MinModel, Enums.IntervalType.Fifteen).Result;
                Assert.AreEqual(result, true);
            }
        }

        /// <summary>
        /// Test case for GetAmiList_15Min method.
        /// </summary>
        [TestMethod]
        public void GetAmiList_15Min()
        {
            var result = _iAmi.GetAmiList(DateTime.Parse("2016-04-15 05:30:00"), DateTime.Parse("2016-06-15 05:30:00"),
                "Meter_QA_027", 87);
            if (result != null)
            {
                var firstOrDefault = result.FirstOrDefault();
                if (firstOrDefault != null) Assert.AreEqual(firstOrDefault.ClientId, 87);
            }
            else Assert.AreEqual(null, null);
        }

        /// <summary>
        /// Test case for GetAmiList_30Min method.
        /// </summary>
        [TestMethod]
        public void GetAmiList_30Min()
        {
            var result = _iAmi.GetAmiList(DateTime.Parse("2015-02-15 05:30:00"), DateTime.Parse("2015-04-15 05:30:00"),
                "256_a1-RCB6-74", 87);
            if (result != null)
            {
                var firstOrDefault = result.FirstOrDefault();
                if (firstOrDefault != null) Assert.AreEqual(firstOrDefault.ClientId, 87);
            }
            else Assert.AreEqual(null, null);
        }

        /// <summary>
        /// Test case for GetAmiList_60Min method.
        /// </summary>
        [TestMethod]
        public void GetAmiList_60Min()
        {
            var result = _iAmi.GetAmiList(DateTime.Parse("2016-01-15 05:30:00"), DateTime.Parse("2016-03-15 05:30:00"),
                "UATDAY4M001", 87);
            if (result != null)
            {
                var firstOrDefault = result.FirstOrDefault();
                if (firstOrDefault != null) Assert.AreEqual(firstOrDefault.ClientId, 87);
            }
            else Assert.AreEqual(null, null);
        }

        /// <summary>
        /// Test case for GetAmi_15Min method.
        /// </summary>
        [TestMethod]
        public void GetAmi_15Min()
        {
            var result = _iAmi.GetAmi(DateTime.Parse("2016-05-15"), "Meter_QA_027", 87).Result;
            if (result != null)
            {
                Assert.AreEqual(result.ClientId, 87);
            }
            else Assert.AreEqual(null, null);
        }

        /// <summary>
        /// Test case for GetAmi_30Min method.
        /// </summary>
        [TestMethod]
        public void GetAmi_30Min()
        {
            var result = _iAmi.GetAmi(DateTime.Parse("2015-03-15"), "256_a1-RCB6-74", 87).Result;
            if (result != null)
            {
                Assert.AreEqual(result.ClientId, 87);
            }
            else Assert.AreEqual(null, null);
        }

        /// <summary>
        /// Test case for GetAmi_60Min method.
        /// </summary>
        [TestMethod]
        public void GetAmi_60Min()
        {
            var result = _iAmi.GetAmi(DateTime.Parse("2016-02-15"), "UATDAY4M001", 87).Result;
            if (result != null)
            {
                Assert.AreEqual(result.ClientId, 87);
            }
            else Assert.AreEqual(null, null);
        }
    }
}
