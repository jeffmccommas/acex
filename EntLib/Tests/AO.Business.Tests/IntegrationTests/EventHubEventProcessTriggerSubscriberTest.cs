﻿using AO.BusinessContracts;
using Microsoft.Practices.Unity;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace AO.Business.Tests.IntegrationTests
{
    [TestClass]
    public class EventHubEventProcessTriggerSubscriberTest : TestBase
    {
        [TestMethod]
        public void TestConstructionOfSubscriber()
        {
            var triggerSubscriber = UnityContainer.Resolve<ITriggerSubscriber>("EventProcessTriggerSubscriber");
            Assert.IsNotNull(triggerSubscriber);
        }
    }
}
