﻿using System;
using System.Text;
using CE.AO.Models;
using Microsoft.Azure;
using Microsoft.Practices.Unity;
using Microsoft.ServiceBus;
using Microsoft.ServiceBus.Messaging;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AO.DataAccess;
using AO.Entities;
using AO.EventProcessors;
using AutoMapper;
using CE.AO.Utilities;
using Enums = CE.AO.Utilities.Enums;

namespace AO.Business.Tests.IntegrationTests
{
    /// <summary>
    /// Test class for EventHubLogToCassandraTriggerSubscriber
    /// </summary>
    [TestClass]
    public class EventHubLogToCassandraTriggerSubscriberTest : TestBase
    {
        private EventHubLogToCassandraTriggerSubscriber _eventHubLogToCassandraTriggerSubscriber;
        private LogRepository _logRepository;

        /// <summary>
        /// Test Case Init
        /// </summary>
        [TestInitialize]
        public void Init()
        {
            _eventHubLogToCassandraTriggerSubscriber = UnityContainer.Resolve<EventHubLogToCassandraTriggerSubscriber>();
            _logRepository = UnityContainer.Resolve<LogRepository>();
        }

        /// <summary>
        /// Test method for Subscribe and UnSubscribe from EventHubLogToCassandraTriggerSubscriber Class
        /// </summary>
        [TestMethod]
        public void TestEventHubLogToCassandraTrigger()
        {
            var importDataEventHubName = CloudConfigurationManager.GetSetting("LoggingEventHubName"); //eventHubName
            var connectionString = CloudConfigurationManager.GetSetting("LoggingEventHubManageConnectionString");

            _eventHubLogToCassandraTriggerSubscriber.Subscribe();
            _eventHubLogToCassandraTriggerSubscriber.UnSubscribe();

            #region Code to delete above created Event Hub
            var connectionbuilder = new ServiceBusConnectionStringBuilder(connectionString)
            {
                TransportType = TransportType.Amqp
            };
            var namespaceManager =
                NamespaceManager.CreateFromConnectionString(connectionbuilder.ToString());
            namespaceManager.DeleteEventHub(importDataEventHubName);

            var isEventHubCreated = EventHubManager.GetNamespaceManager(connectionString).EventHubExists(importDataEventHubName);
            Assert.IsFalse(isEventHubCreated);
            #endregion
        }

        /// <summary>
        /// Test Case for OpenAsync Method
        /// </summary>
        [TestMethod]
        public void OpenAsync()
        {
            PartitionContext partitionContext = new PartitionContext();
            Lease lease = new Lease();
            partitionContext.Lease = lease;
            partitionContext.Lease.Offset = "Lease";

            var result = _eventHubLogToCassandraTriggerSubscriber.OpenAsync(partitionContext);
            var result1 = _eventHubLogToCassandraTriggerSubscriber.CloseAsync(partitionContext, CloseReason.Shutdown);
            Assert.IsTrue(result.IsCompleted);
            Assert.IsTrue(result1.IsCompleted);
        }

        /// <summary>
        /// Test case for ProcessEventsAsync in EventHubLogToCassandraTriggerSubscriber Class
        /// </summary>
        [TestMethod]
        public void ProcessEventsAsync()
        {
            LogViewModel logentity = new LogViewModel()
            {
                ClientId = "898989",
                MeterId = "TestMeter",
                AccountId = "TestAccount",
                CustomerId = "23424",
                ProcessingEventCount = 12,
                Module = "TestModule",
                ProcessingType = Enums.ProcessingType.File.ToString(),
                TimestampUtc = DateTimeOffset.UtcNow,
                Level = "Info"
            };

            var serializedEventData = JsonConvert.SerializeObject(logentity);
            var eventData = new EventData(Encoding.UTF8.GetBytes(serializedEventData));
            PartitionContext context = new PartitionContext();
            Lease lease = new Lease();
            context.Lease = lease;
            context.Lease.Offset = "Lease";
            context.Lease.PartitionId = "PartitionId";
            eventData.Properties.Add("ProcessingType", Enums.ProcessingType.File);
            eventData.Properties.Add("Source", "S");
            eventData.Properties.Add("RowIndex", 1);

            var eventDataList = new List<EventData> { eventData };
            // ReSharper disable once UnusedVariable
            var isOpen = _eventHubLogToCassandraTriggerSubscriber.OpenAsync(context);
            _eventHubLogToCassandraTriggerSubscriber.ProcessEventsAsync(context, eventDataList.AsEnumerable()).Wait();
            var isLogDelete = DeleteLogAsync(logentity).Result;
            Assert.IsTrue(isLogDelete);
        }

        /// <summary>
        /// Delete LogViewModel to Log table
        /// </summary>
        /// <param name="model">LogViewModel to be Deleted</param>
        /// <returns></returns>
        private async Task<bool> DeleteLogAsync(LogViewModel model)
        {
            await _logRepository.DeleteLogAsync(Mapper.Map<LogViewModel, LogEntity>(model));
            return await Task.Factory.StartNew(() => true);
        }
    }
}