﻿using AO.BusinessContracts;
using CE.AO.Models;
using Microsoft.Practices.Unity;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace AO.Business.Tests.IntegrationTests
{
    /// <summary>
    /// Integration Test Class for AlarmType Class
    /// </summary>
    [TestClass]
    public class AlarmTypeTest : TestBase
    {
        private IAlarmTypes _alarmType;

        /// <summary>
        /// Test Case Init
        /// </summary>
        [TestInitialize]
        public void Init()
        {
            _alarmType = UnityContainer.Resolve<IAlarmTypes>();
        }

        /// <summary>
        /// Test case for InsertOrMergeAlarmTypeAsync method in AlarmType class
        /// </summary>
        [TestMethod]
        public void InsertOrMergeAlarmTypeAsync()
        {
            var alarmTypeModel = new AlarmTypeModel
            {
                AlarmGroupId = 99898989,
                AlarmGroupName = "TestGroupName",
                AlarmSourceType = "TestSourceType",
                AlarmSourceTypeId = 59898989,
                AlarmTypeId = 49898989,
                AlarmTypeName = "TestAlarmTypeName"
            };

            _alarmType.InsertOrMergeAlarmTypeAsync(alarmTypeModel).Wait();
            _alarmType.GetAlarmType(alarmTypeModel.AlarmSourceTypeId, alarmTypeModel.AlarmGroupId, alarmTypeModel.AlarmTypeId);
            var isDelete = _alarmType.DeleteAlarmTypeAsync(alarmTypeModel).Result;
            Assert.IsTrue(isDelete);
        }
    }
}
