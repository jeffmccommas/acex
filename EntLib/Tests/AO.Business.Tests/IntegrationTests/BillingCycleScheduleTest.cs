﻿using System;
using AO.BusinessContracts;
using CE.AO.Models;
using Microsoft.Practices.Unity;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace AO.Business.Tests.IntegrationTests
{
    /// <summary>
    /// Test Case Class for Billing 
    /// </summary>
    [TestClass]
    public class BillingCycleScheduleTest : TestBase
    {
        private IBillingCycleSchedule _iBillingCycleSchedule;

        /// <summary>
        /// Initialization of test case.
        /// </summary>
        [TestInitialize]
        public void Init()
        {
            _iBillingCycleSchedule = UnityContainer.Resolve<IBillingCycleSchedule>();
        }

        /// <summary>
        /// Test case for Insert BillingCycle Schedule method.
        /// </summary>
        [TestMethod]
        public void InsertOrMergeBillingCycleScheduleAsync()
        {
            BillCycleScheduleModel billCycleScheduleModel = new BillCycleScheduleModel()
            {
                ClientId = "87",
                EndDate = DateTime.Parse("08/23/2016").Date.ToString("MM-dd-yyyy"),
                BeginDate = DateTime.Parse("08/21/2016").Date.ToString("MM-dd-yyyy"),
                BillCycleScheduleId = "100_Cycle_Test",
                BillCycleScheduleName = "100_Cycles",
                Description = "100_Terst_Cycles"
            };
            var result = _iBillingCycleSchedule.InsertOrMergeBillingCycleScheduleAsync(billCycleScheduleModel).Result;
            Assert.IsTrue(result);
        }


        /// <summary>
        /// Test case for GetBillingCycleScheduleByDateAsync method.
        /// </summary>
        [TestMethod]
        public void GetBillingCycleScheduleByDateAsync()
        {
            var result = _iBillingCycleSchedule.GetBillingCycleScheduleByDateAsync(87, "100_Cycle_Test",
                DateTime.Parse("08/22/2016"));
            if (result != null)
                Assert.AreEqual(result.ClientId, "87");
            else Assert.AreEqual(false, false);
        }


        /// <summary>
        /// Test case for GetBillingCycleScheduleByDateAsync method.
        /// </summary>
        [TestMethod]
        public void DeleteHistoryBillingCycleScheduleAsyc()
       {
            var result = _iBillingCycleSchedule.DeleteHistoryBillingCycleScheduleAsyc("87", "100_Cycle_Test",
                DateTime.Parse("08/21/2016").Date.ToString("MM-dd-yyyy"), DateTime.Parse("08/23/2016").Date.ToString("MM-dd-yyyy")).Result;
            Assert.IsTrue(result);
        }
    }
}
