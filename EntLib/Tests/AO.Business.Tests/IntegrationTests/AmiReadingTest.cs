﻿using System;
using AO.BusinessContracts;
using CE.AO.Models;
using Microsoft.Practices.Unity;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace AO.Business.Tests.IntegrationTests
{
    /// <summary>
    /// Test Case Class for Ami Reading
    /// </summary>
    [TestClass]
    public class AmiReadingTest : TestBase
    {
        private IAmiReading _iAmiReading;

        /// <summary>
        /// Initialization of test case.
        /// </summary>
        [TestInitialize]
        public void Init()
        {
            _iAmiReading = UnityContainer.Resolve<IAmiReading>();
        }

        /// <summary>
        /// Test case for InsertOrMergeAmiReadingAsync method.
        /// </summary>
        [TestMethod]
        public void InsertOrMergeAmiReadingAsync()
        {
            var nccAmiReadingModel = new NccAmiReadingModel
            {
                CustomerId = "500",
                ClientId = 87,
                MeterId = "Met900",
                Timezone = "EST",
                UOMId = 3,
                CommodityId = 3,
                BatteryVoltage = "23 V",
                AmiTimeStamp = DateTime.Parse("2016-04-15"),
                AccountNumber = "ACC90015",
                ReadingValue = 29,
                TransponderId = 2667,
                TransponderPort = 2
            };
            var result = _iAmiReading.InsertOrMergeAmiReadingAsync(nccAmiReadingModel).Result;
            Assert.AreEqual(result, true);
        }

        /// <summary>
        /// Test case for InsertOrMergeAmiReadingAsync method.
        /// </summary>
        [TestMethod]
        public void GetAmiReading()
        {
            var result = _iAmiReading.GetAmiReading(87, "Met900", DateTime.Parse("2016-04-15")).Result;
            Assert.AreEqual(result.ClientId, 87);
        }

        /// <summary>
        /// Test case for DeleteAmiReadingAsync method.
        /// </summary>
        [TestMethod]
        public void DeleteAmiReadingAsync()
        {
            var result = _iAmiReading.DeleteAmiReadingAsync(87, "Met900").Result;
            Assert.AreEqual(result, true);
        }
    }
}
