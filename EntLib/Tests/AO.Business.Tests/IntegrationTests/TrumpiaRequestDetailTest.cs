﻿using System;
using AO.Business.Tests.ObjectBuilders;
using AO.BusinessContracts;
using AO.Entities;
using AutoMapper;
using CE.AO.Models;
using Microsoft.Practices.Unity;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace AO.Business.Tests.IntegrationTests
{
    /// <summary>
    /// Integration test for TrumpiaRequestDetailTest
    /// </summary>
    [TestClass]
    public class TrumpiaRequestDetailTest : TestBase
    {
        private ITrumpiaRequestDetail _iTrumpiaRequestDetail;
        private readonly string _requestId = Guid.NewGuid().ToString();

        /// <summary>
        /// Initialization of test case.
        /// </summary>
        [TestInitialize]
        public void Init()
        {
            _iTrumpiaRequestDetail = UnityContainer.Resolve<ITrumpiaRequestDetail>();
        }

        /// <summary>
        /// Test case for InsertOrMergeHistoryBillAsync method.
        /// </summary>
        [TestMethod]
        public void InsertOrMergeTrumpiaRequestDetail()
        {
            // Arrange
            var entity = new TrumipaRequestDetailBuilder().WithRequestId(_requestId).Build();
            var model = Mapper.Map<TrumpiaRequestDetailEntity, TrumpiaRequestDetailModel>(entity);

            // Act
            _iTrumpiaRequestDetail.InsertOrMergeTrumpiaRequestDetail(model);
            var result = _iTrumpiaRequestDetail.GetTrumpiaRequestDetail(model.RequestId);

            // Assert
            Assert.IsNotNull(result);
            Assert.AreEqual(result.RequestId, model.RequestId);
        }

        /// <summary>
        /// Test case for InsertOrMergeHistoryBillAsync method.
        /// </summary>
        [TestMethod]
        public void GetTrumpiaRequestDetail()
        {
            // Arrange
            var entity = new TrumipaRequestDetailBuilder().WithRequestId(_requestId).Build();
            var model = Mapper.Map<TrumpiaRequestDetailEntity, TrumpiaRequestDetailModel>(entity);
            _iTrumpiaRequestDetail.InsertOrMergeTrumpiaRequestDetail(model);

            // Act
            var result = _iTrumpiaRequestDetail.GetTrumpiaRequestDetail(entity.RequestId);

            // Assert
            Assert.IsNotNull(result);
            Assert.AreEqual(result.RequestId, entity.RequestId);
        }

        /// <summary>
        /// Test case for InsertOrMergeHistoryBillAsync method.
        /// </summary>
        [TestMethod]
        public void GetTrumpiaRequestDetailAsync()
        {
            // Arrange
            var entity = new TrumipaRequestDetailBuilder().WithRequestId(_requestId).Build();
            var model = Mapper.Map<TrumpiaRequestDetailEntity, TrumpiaRequestDetailModel>(entity);

            // Act
            _iTrumpiaRequestDetail.InsertOrMergeTrumpiaRequestDetail(model);
            var result = _iTrumpiaRequestDetail.GetTrumpiaRequestDetailAsync(model.RequestId).Result;

            // Assert
            Assert.IsNotNull(result);
            Assert.AreEqual(result.RequestId, model.RequestId);
        }

        /// <summary>
        /// Test case for InsertOrMergeHistoryBillAsync method.
        /// </summary>
        [TestMethod]
        public void InsertOrMergeTrumpiaRequestDetailAsync()
        {
            // Arrange
            var entity = new TrumipaRequestDetailBuilder().WithRequestId(_requestId).Build();
            var model = Mapper.Map<TrumpiaRequestDetailEntity, TrumpiaRequestDetailModel>(entity);

            // Act
            _iTrumpiaRequestDetail.InsertOrMergeTrumpiaRequestDetailAsync(model).Wait();
            var result = _iTrumpiaRequestDetail.GetTrumpiaRequestDetail(_requestId);

            // Assert
            Assert.IsNotNull(result);
            Assert.AreEqual(result.RequestId, model.RequestId);
        }


        /// <summary>
        /// Test case for DeleteTrumpiaReqeustnAsync method.
        /// </summary>
        [TestMethod]
        public void DeleteTrumpiaReqeustnAsync()
        {

            // Act
            _iTrumpiaRequestDetail.DeleteTrumpiaReqeustnAsync("Sample23").Wait();
            var result = _iTrumpiaRequestDetail.GetTrumpiaRequestDetail("Sample23");

            // Assert
            Assert.IsNull(result);
        }
    }
}
