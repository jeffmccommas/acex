﻿using System;
using CE.AO.Models;
using Microsoft.Practices.Unity;
using Microsoft.VisualStudio.TestTools.UnitTesting;


namespace AO.Business.Tests.IntegrationTests
{
    [TestClass]
    public class ConsumptionByAccountTest : TestBase
    {
        private ConsumptionByAccount _consumptionByAccount;

        /// <summary>
        /// Test Case Init
        /// </summary>
        [TestInitialize]
        public void Init()
        {
            _consumptionByAccount = UnityContainer.Resolve<ConsumptionByAccount>();
        }

        /// <summary>
        /// Test case to insert ConsumptionByAccount Model
        /// Test case for InsertOrMergeConsumptionByAccountAsync method for ConsumptionByAccount class
        /// </summary>
        [TestMethod]
        public void InsertOrMergeConsumptionByAccountAsync()
        {
            var consumptionByAccountModel = new ConsumptionByAccountModel
            {
                AmiTimeStamp = DateTime.UtcNow,
                IntervalType = 60,
                TransponderPort = "1",
                UOMId = 3,
                AccountNumber = "Test Account",
                ClientId = 87,
                BatteryVoltage = "10",
                CommodityId = 3,
                MeterId = "Test Meter",
                TransponderId = "Test Transponder",
                Timezone = "PST",
                Consumption = 10,
                CustomerId = "Test Customer"
            };

            _consumptionByAccount.InsertOrMergeConsumptionByAccountAsync(consumptionByAccountModel).Wait();
            // ReSharper disable once UnusedVariable
            var consumptionFetched = _consumptionByAccount.GetConsumptionByAccountByDateRangeAsync(87, "Test Account",
                DateTime.UtcNow.AddDays(-1), DateTime.UtcNow.AddDays(1)).Result;
            var isdelete = _consumptionByAccount.DeleteConsumptionByAccountAsync(consumptionByAccountModel).Result;

            Assert.IsTrue(isdelete);
        }
    }
}
