﻿using AO.BusinessContracts;
using AO.DataAccess;
using AO.Registrar;
using Microsoft.Practices.Unity;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace AO.Business.Tests.IntegrationTests
{
    /// <summary>
    /// Test Case Class for IdentityFacade
    /// </summary>
    [TestClass]
    public class IdentityFacadeTest : TestBase
    {
        private IIdentityFacade _identityFacade;
        private TenantRepository _tenantRespository;

        /// <summary>
        /// Initialization of test case.
        /// </summary>
        [TestInitialize]
        public void Init()
        {
            new PortalRegistrar().Initialize<TransientLifetimeManager>(UnityContainer);
            _identityFacade = UnityContainer.Resolve<IIdentityFacade>();
            _tenantRespository = UnityContainer.Resolve<TenantRepository>();
        }

        /// <summary>
        /// Test case for GetClientIdForTenant() method. Ensure ClientId property of Tenant object is returned.
        /// </summary>
        [TestMethod]
        public void GetClientIdForCurrentUser()
        {
            // Pass in the tenant ID of the Aclara account.
            var result = _identityFacade.GetClientIdForTenant("a2ecb94e-60b9-4578-9816-9e17ff7974fe");

            // Client ID for the Aclara account should be 5.
            Assert.AreEqual(result.GetType(), typeof(int));
            Assert.AreEqual(result, 5);
        }
    }
}
