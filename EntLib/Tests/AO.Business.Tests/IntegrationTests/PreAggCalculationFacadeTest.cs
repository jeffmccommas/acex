﻿using System;
using CE.AO.Models;
using Microsoft.Practices.Unity;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Enums = CE.AO.Utilities.Enums;

namespace AO.Business.Tests.IntegrationTests
{
    /// <summary>
    /// Integration Test Class for PreAggCalculationFacade Class
    /// </summary>
    [TestClass]
    public class PreAggCalculationFacadeTest : TestBase
    {
        private PreAggCalculationFacade _preAggCalculationFacade;
        private ConsumptionByAccount _consumptionByAccount;
        private ConsumptionAggByAccount _consumptionAggByAccount;

        /// <summary>
        /// Test Case Init
        /// </summary>
        [TestInitialize]
        public void Init()
        {
            _preAggCalculationFacade = UnityContainer.Resolve<PreAggCalculationFacade>();
            _consumptionByAccount = UnityContainer.Resolve<ConsumptionByAccount>();
            _consumptionAggByAccount = UnityContainer.Resolve<ConsumptionAggByAccount>();
        }

        /// <summary>
        /// Test Method for CalculateAccountPreAgg
        /// </summary>
        [TestMethod]
        public void CalculateAccountPreAggForDay()
        {
            var utcDate = DateTime.UtcNow;
            var consumptionByAccountModel = new ConsumptionByAccountModel
            {
                AmiTimeStamp = utcDate,
                IntervalType = 60,
                TransponderPort = "1",
                UOMId = 3,
                AccountNumber = "TestAccount",
                ClientId = 899898,
                BatteryVoltage = "10",
                CommodityId = 3,
                MeterId = "Test Meter",
                TransponderId = "Test Transponder",
                Timezone = "PST",
                Consumption = 20,
                CustomerId = "Test Customer"
            };

            var consumptionByAccountModel2 = new ConsumptionByAccountModel
            {
                AmiTimeStamp = utcDate.AddHours(-2),
                IntervalType = 60,
                TransponderPort = "1",
                UOMId = 3,
                AccountNumber = "TestAccount",
                ClientId = 899898,
                BatteryVoltage = "10",
                CommodityId = 3,
                MeterId = "Test Meter",
                TransponderId = "Test Transponder",
                Timezone = "PST",
                Consumption = 10,
                CustomerId = "Test Customer"
            };

            _consumptionByAccount.InsertOrMergeConsumptionByAccountAsync(consumptionByAccountModel).Wait();
            _consumptionByAccount.InsertOrMergeConsumptionByAccountAsync(consumptionByAccountModel2).Wait();

            var accountPreAggmodel = new AccountPreAggCalculationRequestModel
            {
                ClientId = 899898,
                AccountId = "TestAccount",
                AmiTimestamp = DateTime.UtcNow,
                CalculationRange = Enums.PreAggCalculationRange.Day,
                Timezone = "PST"
            };

            var consumptionAggByAccountModel = new ConsumptionAggByAccountModel
            {
                StandardUOMId = 3,
                AccountNumber = "TestAccount",
                ClientId = 899898,
                CommodityId = 3,
                Timezone = "PST",
                StandardUOMConsumption = 10,
                AggregationEndDateTime = utcDate,
                AggregationType = Enums.PreAggregationTypes.Day.ToString()
            };

            var result = _preAggCalculationFacade.CalculateAccountPreAgg(accountPreAggmodel).Result;
            var isconsumptionByAccountdeleted = _consumptionByAccount.DeleteConsumptionByAccountAsync(consumptionByAccountModel).Result;
            var isconsumptionByAccountdeleted2 = _consumptionByAccount.DeleteConsumptionByAccountAsync(consumptionByAccountModel2).Result;
            _consumptionAggByAccount.DeleteConsumptionAggByAccountUsingPartionAsync(consumptionAggByAccountModel).Wait();
            Assert.IsTrue(isconsumptionByAccountdeleted);
            Assert.IsTrue(isconsumptionByAccountdeleted2);
            Assert.IsTrue(result);
        }

        /// <summary>
        /// Test Method for CalculateAccountPreAggForHour
        /// </summary>
        [TestMethod]
        public void CalculateAccountPreAggForHour()
        {
            var utcDate = new DateTime(DateTime.UtcNow.Year, DateTime.UtcNow.Month, DateTime.UtcNow.Day, 4, 0, 0); //DateTime.UtcNow;
            var consumptionByAccountModel = new ConsumptionByAccountModel
            {
                AmiTimeStamp = utcDate,
                IntervalType = 60,
                TransponderPort = "1",
                UOMId = 3,
                AccountNumber = "TestAccount",
                ClientId = 899898,
                BatteryVoltage = "10",
                CommodityId = 3,
                MeterId = "Test Meter",
                TransponderId = "Test Transponder",
                Timezone = "PST",
                Consumption = 20,
                CustomerId = "Test Customer"
            };

            var consumptionByAccountModel2 = new ConsumptionByAccountModel
            {
                AmiTimeStamp = utcDate.AddHours(-2),
                IntervalType = 60,
                TransponderPort = "1",
                UOMId = 3,
                AccountNumber = "TestAccount",
                ClientId = 899898,
                BatteryVoltage = "10",
                CommodityId = 3,
                MeterId = "Test Meter",
                TransponderId = "Test Transponder",
                Timezone = "PST",
                Consumption = 10,
                CustomerId = "Test Customer"
            };

            _consumptionByAccount.InsertOrMergeConsumptionByAccountAsync(consumptionByAccountModel).Wait();
            _consumptionByAccount.InsertOrMergeConsumptionByAccountAsync(consumptionByAccountModel2).Wait();

            var accountPreAggmodel = new AccountPreAggCalculationRequestModel
            {
                ClientId = 899898,
                AccountId = "TestAccount",
                AmiTimestamp = utcDate,
                CalculationRange = Enums.PreAggCalculationRange.Hour,
                Timezone = "PST"
            };

            var consumptionAggByAccountModel = new ConsumptionAggByAccountModel
            {
                StandardUOMId = 3,
                AccountNumber = "TestAccount",
                ClientId = 899898,
                CommodityId = 3,
                Timezone = "PST",
                StandardUOMConsumption = 10,
                AggregationEndDateTime = utcDate,
                AggregationType = Enums.PreAggregationTypes.Hour.ToString()
            };

            var result = _preAggCalculationFacade.CalculateAccountPreAgg(accountPreAggmodel).Result;

            var isconsumptionByAccountdeleted = _consumptionByAccount.DeleteConsumptionByAccountAsync(consumptionByAccountModel).Result;
            var isconsumptionByAccountdeleted2 = _consumptionByAccount.DeleteConsumptionByAccountAsync(consumptionByAccountModel2).Result;
            _consumptionAggByAccount.DeleteConsumptionAggByAccountUsingPartionAsync(consumptionAggByAccountModel).Wait();
            Assert.IsTrue(isconsumptionByAccountdeleted);
            Assert.IsTrue(isconsumptionByAccountdeleted2);
            Assert.IsTrue(result);
        }

        /// <summary>
        /// Test Method for CalculateAccountPreAggForHour for DST hours
        /// </summary>
        [TestMethod]
        public void CalculateAccountPreAggForDstHour()
        {
            var utcDate = new DateTime(2012, 11, 4, 6, 0, 0);// DST second hour
            var consumptionByAccountModel = new ConsumptionByAccountModel
            {
                AmiTimeStamp = utcDate,
                IntervalType = 60,
                TransponderPort = "1",
                UOMId = 3,
                AccountNumber = "TestAccount",
                ClientId = 899898,
                BatteryVoltage = "10",
                CommodityId = 3,
                MeterId = "Test Meter",
                TransponderId = "Test Transponder",
                Timezone = "EST",
                Consumption = 20,
                CustomerId = "Test Customer"
            };

            var consumptionByAccountModel2 = new ConsumptionByAccountModel
            {
                AmiTimeStamp = utcDate.AddHours(-1),
                IntervalType = 60,
                TransponderPort = "1",
                UOMId = 3,
                AccountNumber = "TestAccount",
                ClientId = 899898,
                BatteryVoltage = "10",
                CommodityId = 3,
                MeterId = "Test Meter",
                TransponderId = "Test Transponder",
                Timezone = "EST",
                Consumption = 10,
                CustomerId = "Test Customer"
            };

            _consumptionByAccount.InsertOrMergeConsumptionByAccountAsync(consumptionByAccountModel).Wait();
            _consumptionByAccount.InsertOrMergeConsumptionByAccountAsync(consumptionByAccountModel2).Wait();

            var accountPreAggmodel = new AccountPreAggCalculationRequestModel
            {
                ClientId = 899898,
                AccountId = "TestAccount",
                AmiTimestamp = utcDate,
                CalculationRange = Enums.PreAggCalculationRange.Hour,
                Timezone = "EST"
            };

            var consumptionAggByAccountModel = new ConsumptionAggByAccountModel
            {
                StandardUOMId = 3,
                AccountNumber = "TestAccount",
                ClientId = 899898,
                CommodityId = 3,
                Timezone = "EST",
                StandardUOMConsumption = 10,
                AggregationEndDateTime = utcDate,
                AggregationType = Enums.PreAggregationTypes.Hour.ToString()
            };

            var result = _preAggCalculationFacade.CalculateAccountPreAgg(accountPreAggmodel).Result;

            var isconsumptionByAccountdeleted = _consumptionByAccount.DeleteConsumptionByAccountAsync(consumptionByAccountModel).Result;
            var isconsumptionByAccountdeleted2 = _consumptionByAccount.DeleteConsumptionByAccountAsync(consumptionByAccountModel2).Result;
            _consumptionAggByAccount.DeleteConsumptionAggByAccountUsingPartionAsync(consumptionAggByAccountModel).Wait();
            Assert.IsTrue(isconsumptionByAccountdeleted);
            Assert.IsTrue(isconsumptionByAccountdeleted2);
            Assert.IsTrue(result);

            utcDate = new DateTime(2012, 11, 4, 5, 0, 0);// DST first hour
            consumptionByAccountModel = new ConsumptionByAccountModel
            {
                AmiTimeStamp = utcDate,
                IntervalType = 60,
                TransponderPort = "1",
                UOMId = 3,
                AccountNumber = "TestAccount",
                ClientId = 899898,
                BatteryVoltage = "10",
                CommodityId = 3,
                MeterId = "Test Meter",
                TransponderId = "Test Transponder",
                Timezone = "EST",
                Consumption = 20,
                CustomerId = "Test Customer"
            };

            consumptionByAccountModel2 = new ConsumptionByAccountModel
            {
                AmiTimeStamp = utcDate.AddHours(-1),
                IntervalType = 60,
                TransponderPort = "1",
                UOMId = 3,
                AccountNumber = "TestAccount",
                ClientId = 899898,
                BatteryVoltage = "10",
                CommodityId = 3,
                MeterId = "Test Meter",
                TransponderId = "Test Transponder",
                Timezone = "EST",
                Consumption = 10,
                CustomerId = "Test Customer"
            };

            _consumptionByAccount.InsertOrMergeConsumptionByAccountAsync(consumptionByAccountModel).Wait();
            _consumptionByAccount.InsertOrMergeConsumptionByAccountAsync(consumptionByAccountModel2).Wait();

            accountPreAggmodel = new AccountPreAggCalculationRequestModel
            {
                ClientId = 899898,
                AccountId = "TestAccount",
                AmiTimestamp = utcDate,
                CalculationRange = Enums.PreAggCalculationRange.Hour,
                Timezone = "EST"
            };

            consumptionAggByAccountModel = new ConsumptionAggByAccountModel
            {
                StandardUOMId = 3,
                AccountNumber = "TestAccount",
                ClientId = 899898,
                CommodityId = 3,
                Timezone = "EST",
                StandardUOMConsumption = 10,
                AggregationEndDateTime = utcDate,
                AggregationType = Enums.PreAggregationTypes.Hour.ToString()
            };

            result = _preAggCalculationFacade.CalculateAccountPreAgg(accountPreAggmodel).Result;

            isconsumptionByAccountdeleted = _consumptionByAccount.DeleteConsumptionByAccountAsync(consumptionByAccountModel).Result;
            isconsumptionByAccountdeleted2 = _consumptionByAccount.DeleteConsumptionByAccountAsync(consumptionByAccountModel2).Result;
            _consumptionAggByAccount.DeleteConsumptionAggByAccountUsingPartionAsync(consumptionAggByAccountModel).Wait();
            Assert.IsTrue(isconsumptionByAccountdeleted);
            Assert.IsTrue(isconsumptionByAccountdeleted2);
            Assert.IsTrue(result);
        }
    }
}
