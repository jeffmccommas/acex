﻿using System;
using System.Threading.Tasks;
using CE.AO.Models;
using Microsoft.Practices.Unity;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace AO.Business.Tests.IntegrationTests
{
	[TestClass]
	public class DcuAlarmTest : TestBase
	{
		private DcuAlarm _dcuAlarm;

		/// <summary>
		/// Test Case Init
		/// </summary>
		[TestInitialize]
		public void Init()
		{
			_dcuAlarm = UnityContainer.Resolve<DcuAlarm>();
		}

		/// <summary>
		/// Test case for InsertOrMergeDcuAlarmAsync method in DcuAlarm class
		/// </summary>
		[TestMethod]
		public void InsertOrMergeDcuAlarmAsync()
		{
			DcuAlarmModel dcuAlarmModel = new DcuAlarmModel
			{
				ClientId = 89898989,
				AlarmTime = DateTime.UtcNow,
				DcuId = 1,
				AlarmCode = "Dcu_Call_Me",
				ComponentType = "Test_Component",
				NotificationType = "Test_Notification",
				Severity = 100,
				UserFriendlyDescription = "Testing Entity"
			};

			// ReSharper disable once UnusedVariable
			var insertTask = _dcuAlarm.InsertOrMergeDcuAlarmAsync(dcuAlarmModel);
			var deleteTask = _dcuAlarm.DeleteDcuAlarmAsync(dcuAlarmModel);

			Task.WaitAll(insertTask);
			Task.WaitAll(deleteTask);
		}
	}
}
