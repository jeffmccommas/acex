﻿using AO.EventProcessors;
using Microsoft.Azure;
using Microsoft.Practices.Unity;
using Microsoft.ServiceBus;
using Microsoft.ServiceBus.Messaging;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace AO.Business.Tests.IntegrationTests
{
	/// <summary>
	/// Test class for EventHubValidationEstimationTriggerSubscriber
	/// </summary>
	[TestClass]
	public class EventHubValidationEstimationTriggerSubscriberTest : TestBase
	{
		private EventHubValidationEstimationTriggerSubscriber _eventHubValidationEstimationTriggerSubscriber;

		/// <summary>
		/// Test Case Init
		/// </summary>
		[TestInitialize]
		public void Init()
		{
			_eventHubValidationEstimationTriggerSubscriber = UnityContainer.Resolve<EventHubValidationEstimationTriggerSubscriber>();
		}

		/// <summary>
		/// Test method for Subscribe and UnSubscribe from EventHubValidationEstimationTriggerSubscriber Class
		/// </summary>
		[TestMethod]
		public void TestEventHubLogToCassandraTrigger()
		{
			var importDataEventHubName = CloudConfigurationManager.GetSetting("ValidationEstimationEventHubName"); //eventHubName
			var connectionString = CloudConfigurationManager.GetSetting("ValidationEstimationEventHubManageConnectionString");

			_eventHubValidationEstimationTriggerSubscriber.Subscribe();
			_eventHubValidationEstimationTriggerSubscriber.UnSubscribe();

			#region Code to delete above created Event Hub
			var connectionbuilder = new ServiceBusConnectionStringBuilder(connectionString)
			{
				TransportType = TransportType.Amqp
			};
			var namespaceManager =
				NamespaceManager.CreateFromConnectionString(connectionbuilder.ToString());
			namespaceManager.DeleteEventHub(importDataEventHubName);

			Assert.IsTrue(true);
			#endregion
		}

		/// <summary>
		/// Test Case for OpenAsync Method
		/// </summary>
		[TestMethod]
		public void OpenAsync()
		{
			var partitionContext = new PartitionContext();
			var lease = new Lease();
			partitionContext.Lease = lease;
			partitionContext.Lease.Offset = "Lease";

			var result = _eventHubValidationEstimationTriggerSubscriber.OpenAsync(partitionContext);
			var result1 = _eventHubValidationEstimationTriggerSubscriber.CloseAsync(partitionContext, CloseReason.Shutdown);
			Assert.IsTrue(result.IsCompleted);
			Assert.IsTrue(result1.IsCompleted);
		}
	}
}
