﻿using CE.AO.Models;
using Microsoft.Practices.Unity;
using Microsoft.VisualStudio.TestTools.UnitTesting;


namespace AO.Business.Tests.IntegrationTests
{
    [TestClass]
    public class DcuProvisioningTest : TestBase
    {
        private DcuProvisioning _dcuProvisioning;

        /// <summary>
        /// Test Case Init
        /// </summary>
        [TestInitialize]
        public void Init()
        {
            _dcuProvisioning = UnityContainer.Resolve<DcuProvisioning>();
        }

        /// <summary>
        /// Test case for InsertOrMergeDcuProvisioningAsync method in DcuProvisioning class
        /// </summary>
        [TestMethod]
        public void InsertOrMergeDcuProvisioningAsync()
        {
            var dcuProvisioningModel = new DcuProvisioningModel
            {
                ClientId = 87,
                DcuId = 12,
                InstalledDate = "",
                Latitude = "12:12",
                Longitude = "12:12",
                Name = "Fake DCU"
            };

            _dcuProvisioning.InsertOrMergeDcuProvisioningAsync(dcuProvisioningModel).Wait();
            _dcuProvisioning.DeleteDcuProvisioningAsync(dcuProvisioningModel).Wait();
        }
    }
}
