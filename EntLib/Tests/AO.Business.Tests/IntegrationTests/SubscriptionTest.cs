﻿using System;
using System.Collections.Generic;
using System.Linq;
using AO.BusinessContracts;
using CE.AO.Models;
using Microsoft.Practices.Unity;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace AO.Business.Tests.IntegrationTests
{
    /// <summary>
    /// Test Case Class for SubscriptionTest 
    /// </summary>
    [TestClass]
    public class SubscriptionTest : TestBase
    {
        private ISubscription _iSubscription;

        /// <summary>
        /// Initialization of test case.
        /// </summary>
        [TestInitialize]
        public void Init()
        {
            _iSubscription = UnityContainer.Resolve<ISubscription>();
        }

        /// <summary>
        /// Method Which returns List of Static Models
        /// </summary>
        /// <returns></returns>
        private static List<object> ListOfModelAndEntity()
        {
            List<object> getModelEntities = new List<object>()
            {
                new BillingModel()
                {
                    AccountId = "1",
                    CustomerId = "2",
                    ClientId = 87,
                    ServiceContractId = "asd",
                    ServicePointId = "23",
                    Source = "Meter",
                    PremiseId = "467",
                    IsLatest = true,
                    AmiEndDate = DateTime.Now,
                    AmiStartDate = DateTime.Now,
                    BillCycleScheduleId = "UYTH76",
                    BillDays = 23,
                    BillPeriodType = 35,
                    CommodityId = 2,
                    EndDate = DateTime.Now,
                    IsFault = true,
                    MeterId = "Id7",
                    MeterType = "T",
                    RateClass = "G",
                    ReadDate = DateTime.Now,
                    ReadQuality = "Yes",
                    ReplacedMeterId = "45t",
                    StartDate = DateTime.Now,
                    TotalCost = 245.5,
                    TotalUsage = 234.5,
                    UOMId = 2
                },
                    new SubscriptionModel()
                {
                    ClientId = 87,
                    CustomerId = "2",
                    AccountId = "3",
                    AllowedChannels = "4",
                    Channel = "SMS",
                    CommodityKey = "RT4",
                    EmailConfirmationCount = 5,
                    EmailOptInCompletedDate = DateTime.Now.Date,
                    InsightTypeName = "",
                    IsDoubleOptIn = true,
                    IsEmailOptInCompleted = true,
                    IsLatest = false,
                    IsSelected = true,
                    IsSmsOptInCompleted = true,
                    OptOutDate = DateTime.Now.Date,
                    PremiseId = "67",
                    ProgramName = "Program1",
                    ServiceContractId = "RT67",
                    ServicePointId = "67i",
                    SmsConfirmationCount = 45,
                    SmsOptInCompletedDate = DateTime.Now.Date,
                    SubscriberThresholdValue = 45.34,
                    SubscriptionDate = DateTime.Now.Date,
                    ThresholdMax = 90.3,
                    ThresholdMin = 90.67,
                    ThresholdType = "QW",
                    Type = "ST",
                    UomId = 23
                }
            };
            return getModelEntities;
        }

        /// <summary>
        /// Test case for InsertSubscriptionAsync method.
        /// </summary>
        [TestMethod]
        public void InsertSubscriptionAsync()
        {
            var getEntitiesModels = ListOfModelAndEntity();
            var getSubModel = (SubscriptionModel)getEntitiesModels[1];
            var result = _iSubscription.InsertSubscriptionAsync(getSubModel, true, true).Result;
            Assert.AreEqual(result, true);
        }


        /// <summary>
        /// Test case for GetCustomerSubscriptions method.
        /// </summary>
        [TestMethod]
        public void GetCustomerSubscriptions()
        {
            var result = _iSubscription.GetCustomerSubscriptions(87, "2", "3");
            var firstOrDefault = result.FirstOrDefault();
            if (firstOrDefault != null) Assert.AreEqual(firstOrDefault.ClientId, 87);
            else Assert.AreEqual(null, null);
        }

        /// <summary>
        /// Test case for DeleteSubscriptionAsync method.
        /// </summary>
        [TestMethod]
        public void DeleteSubscriptionAsync()
        {
            var result = _iSubscription.DeleteSubscriptionAsync(87, "A", "2", "3").Result;
            Assert.AreEqual(result, true);
        }
    }
}
