﻿using System.Linq;
using System.Threading.Tasks;
using AO.BusinessContracts;
using CE.AO.Models;
using Microsoft.Practices.Unity;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace AO.Business.Tests.IntegrationTests
{
    /// <summary>
    /// Test Case Class for Billing 
    /// </summary>
    [TestClass]
    public class ClientAccountTest : TestBase
    {
        private IClientAccount _iClientAccount;

        /// <summary>
        /// Initialization of test case.
        /// </summary>
        [TestInitialize]
        public void Init()
        {
            _iClientAccount = UnityContainer.Resolve<IClientAccount>();
        }

        /// <summary>
        /// Test case for InsertCtdCalculationAsync method.
        /// </summary>
        [TestMethod]
        public void InsertOrMergeClientAccount()
        {
            // Arrange
            var clientAccountModel = new ClientAccountModel()
            {
                CustomerId = "500",
                ClientId = 87,
                AccountId = "24",
            };

            // Act
            _iClientAccount.InsertOrMergeClientAccount(clientAccountModel).Wait();
            var result = _iClientAccount.GetClientAccounts(87).Any(c =>c.CustomerId == clientAccountModel.CustomerId && c.AccountId == clientAccountModel.AccountId);

            // Assert
            Assert.IsTrue(result, "Expected to find at least one client account with customerId of '500', cliend id of 87, and account id of 24.");
        }

        /// <summary>
        /// Test case for GetClientAccounts method.
        /// </summary>
        [TestMethod]
        public void GetClientAccounts()
        {
            var result = Task.Run(() => _iClientAccount.GetClientAccounts(87)).Result;
            var firstOrDefault = result.FirstOrDefault();

            Assert.IsNotNull(firstOrDefault);
            Assert.AreEqual(firstOrDefault.ClientId, 87);
        }

        /// <summary>
        /// Test case for GetClientAccounts method.
        /// </summary>
        [TestMethod]
        public void GetClientAccounts_Parameterized()
        {
            byte[] sampleBytes = null;
            var result = _iClientAccount.GetClientAccounts(87, 1, ref sampleBytes);
            var clientAccountModel = result.FirstOrDefault();
            Assert.IsNotNull(clientAccountModel, "Expected valid client account model object.");
            Assert.AreEqual(clientAccountModel.ClientId, 87, $"Expected clientId of 87.  Found [{clientAccountModel.ClientId}]");
        }

        /// <summary>
        /// Test case for DeleteClientAccountAsync method.
        /// </summary>
        [TestMethod]
        public void DeleteClietnAccountAsync()
        {
            _iClientAccount.DeleteClientAccountAsyc(87, "24").Wait();
        }
    }
}
