﻿using System;
using AO.EventProcessors;
using CE.AO.Models;
using CE.AO.Utilities;
using Microsoft.Azure;
using Microsoft.Practices.Unity;
using Microsoft.ServiceBus;
using Microsoft.ServiceBus.Messaging;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Enums = CE.AO.Utilities.Enums;

namespace AO.Business.Tests.IntegrationTests
{
    [TestClass]
    public class EventHubPreAggCalculationTriggerTest : TestBase
    {
        private EventHubPreAggCalculationTriggerSubscriber _eventHubPreAggCalculationTriggerSubscriber;
        private EventHubPreAggCalculationTrigger _eventHubPreAggCalculationTrigger;

        /// <summary>
        /// Test Case Init
        /// </summary>
        [TestInitialize]
        public void Init()
        {
            _eventHubPreAggCalculationTriggerSubscriber = UnityContainer.Resolve<EventHubPreAggCalculationTriggerSubscriber>();
            _eventHubPreAggCalculationTrigger = UnityContainer.Resolve<EventHubPreAggCalculationTrigger>();
        }

        [TestMethod]
        public void TriggerTest()
        {
            _eventHubPreAggCalculationTriggerSubscriber.Subscribe();
            var accountPreAggCalculationRequestModel = new AccountPreAggCalculationRequestModel
            {
                ClientId = 899898,
                AccountId = "TestEventhubAccount",
                AmiTimestamp = DateTime.UtcNow,
                CalculationRange = Enums.PreAggCalculationRange.Day,
                Timezone = "PST"
            };
            _eventHubPreAggCalculationTrigger.LogModel.ProcessingType = Enums.ProcessingType.File;
            _eventHubPreAggCalculationTrigger.LogModel.RowIndex="1" ;
            _eventHubPreAggCalculationTrigger.LogModel.Source = "Scheduled";

            _eventHubPreAggCalculationTrigger.Trigger(accountPreAggCalculationRequestModel);

            #region Code to delete above created Event Hub
            _eventHubPreAggCalculationTriggerSubscriber.UnSubscribe();
            var importDataEventHubName = CloudConfigurationManager.GetSetting("PreAggCalculationEventHubName"); //eventHubName
            var connectionString = CloudConfigurationManager.GetSetting("PreAggCalculationEventHubManageConnectionString");
            var connectionbuilder = new ServiceBusConnectionStringBuilder(connectionString)
            {
                TransportType = TransportType.Amqp
            };
            var namespaceManager =
                NamespaceManager.CreateFromConnectionString(connectionbuilder.ToString());
            namespaceManager.DeleteEventHub(importDataEventHubName);

            var isEventHubCreated = EventHubManager.GetNamespaceManager(connectionString).EventHubExists(importDataEventHubName);
            Assert.IsFalse(isEventHubCreated);
            
            #endregion
        }
    }
}
