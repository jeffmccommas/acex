﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.Practices.Unity;
using CE.AO.Models;

namespace AO.Business.Tests.IntegrationTests
{
	[TestClass]

 public class DcuAlarmCodeHistoryTest : TestBase
	{
		private DcuAlarmCodeHistory _dcuAlarm;

		[TestInitialize]
		public void Init()
		{
			_dcuAlarm = UnityContainer.Resolve<DcuAlarmCodeHistory>();
		}

		/// <summary>
		/// Test case for InsertOrMergeDcuAlarmCodeHistoryAsync method in DcuAlarmCodeHistory class
		/// </summary>
		[TestMethod]
		public void InsertOrMergeDcuAlarmCodeHistoryAsync()
		{
			DcuAlarmModel dcuAlarmModel = new DcuAlarmModel
			{
				ClientId = 89898989,
				AlarmTime = DateTime.UtcNow,
				DcuId = 1,
				AlarmCode = "Dcu_Call_Me",
				ComponentType = "Test_Component",
				NotificationType = "Test_Notification",
				Severity = 100,
				UserFriendlyDescription = "Testing Entity"
			};

			// ReSharper disable once UnusedVariable
			var insertResult = _dcuAlarm.InsertOrMergeDcuAlarmCodeHistoryAsync(dcuAlarmModel).Result;
			// ReSharper disable once UnusedVariable
			var isDelete = _dcuAlarm.DeleteDcuAlarmCodeHistoryAsync(dcuAlarmModel).Result;
			var isDeleteByPartition = _dcuAlarm.DeleteDcuAlarmCodeWithPartitionHistoryAsync(dcuAlarmModel).Result;
			Assert.IsTrue(isDeleteByPartition);
		}

	}
}
