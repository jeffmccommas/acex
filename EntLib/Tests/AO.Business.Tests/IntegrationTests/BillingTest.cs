﻿using System;
using System.Linq;
using AO.BusinessContracts;
using CE.AO.Models;
using Microsoft.Practices.Unity;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace AO.Business.Tests.IntegrationTests
{
    /// <summary>
    /// Test Case Class for Billing 
    /// </summary>
    [TestClass]
    public class BillingTest : TestBase
    {
        private IBilling _iBilling;

        /// <summary>
        /// Initialization of test case.
        /// </summary>
        [TestInitialize]
        public void Init()
        {
            _iBilling = UnityContainer.Resolve<IBilling>();
        }

        /// <summary>
        /// Test case for InsertOrMergeAmiReadingAsync method.
        /// </summary>
        [TestMethod]
        public void InsertOrMergeLatestBillAsync()
        {
            BillingModel billingModel = new BillingModel()
            {
                CustomerId = "500",
                ClientId = 87,
                AccountId = "ACC500",
                IsLatest = true,
                ServiceContractId = "SC76",
                MeterId = "Met980",
                ServicePointId = "SP10",
                PremiseId = "PR01",
                CommodityId = 3,
                UOMId = 3,
                Source = "S",
                BillPeriodType = 2,
                StartDate = DateTime.Parse("2016-09-24"),
                ReplacedMeterId = "Met908",
                MeterType = "Act",
                BillDays = 23,
                RateClass = "C",
                EndDate = DateTime.Parse("2016-09-24"),
                IsFault = false,
                TotalUsage = 45.45,
                TotalCost = 2000,
                AmiStartDate = DateTime.Parse("2016-09-24"),
                AmiEndDate = DateTime.Parse("2016-09-26"),
                ReadQuality = "",
                ReadDate = DateTime.Now,
                BillCycleScheduleId = "RT4"
            };
            var result = _iBilling.InsertOrMergeLatestBillAsync(billingModel).Result;
            Assert.AreEqual(result, true);
        }

        /// <summary>
        /// Test case for InsertOrMergeHistoryBillAsync method.
        /// </summary>
        [TestMethod]
        public void InsertOrMergeHistoryBillAsync()
        {
            BillingModel billingModel = new BillingModel()
            {
                CustomerId = "500",
                ClientId = 87,
                AccountId = "ACC500",
                IsLatest = true,
                ServiceContractId = "SC76",
                MeterId = "Met980",
                ServicePointId = "SP10",
                PremiseId = "PR01",
                CommodityId = 3,
                UOMId = 3,
                Source = "S",
                BillPeriodType = 2,
                StartDate = DateTime.Parse("2016-09-24"),
                ReplacedMeterId = "Met908",
                MeterType = "Act",
                BillDays = 23,
                RateClass = "C",
                EndDate = DateTime.Parse("2016-09-26"),
                IsFault = false,
                TotalUsage = 45.45,
                TotalCost = 2000,
                AmiStartDate = DateTime.Parse("2016-09-24"),
                AmiEndDate = DateTime.Parse("2016-09-26"),
                ReadQuality = "",
                ReadDate = DateTime.Now,
                BillCycleScheduleId = "RT4"
            };
            var result = _iBilling.InsertOrMergeHistoryBillAsync(billingModel).Result;
            Assert.AreEqual(result, true);
        }

        /// <summary>
        /// Test case for InsertOrMergeHistoryBillAsync method.
        /// </summary>
        [TestMethod]
        public void DeleteLatestServiceDetailsAsync()
        {
            var result = _iBilling.DeleteLatestServiceDetailsAsync(87, "500", "ACC500", "SC76").Result;
            Assert.AreEqual(result, true);
        }

        /// <summary>
        /// Test case for DeleteHistoryServiceDetailsAsync method.
        /// </summary>
        [TestMethod]
        public void DeleteHistoryServiceDetailsAsync()
        {
            var result = _iBilling.DeleteHistoryServiceDetailsAsync(87, "500", "ACC500", "SC76", DateTime.Parse("2016-09-24")).Result;
            Assert.AreEqual(result, true);
        }

        /// <summary>
        /// Test case for GetAllServicesFromLastBill method.
        /// </summary>
        [TestMethod]
        public void GetAllServicesFromLastBill()
        {
            var result = _iBilling.GetAllServicesFromLastBill(87, "500", "ACC500");
            var firstOrDefault = result.FirstOrDefault();
            if (firstOrDefault != null) Assert.AreEqual(firstOrDefault.ClientId, 87);
            else Assert.AreEqual(null, null);
        }

        /// <summary>
        /// Test case for GetBillsForService method.
        /// </summary>
        [TestMethod]
        public void GetServiceDetailsFromBillAsync()
        {
            var result = _iBilling.GetServiceDetailsFromBillAsync(87, "500", "ACC500", "SC76").Result;
            if (result != null) Assert.AreEqual(result.ClientId, 87);
            else Assert.AreEqual(null, null);
        }
    }
}
