﻿using System;
using System.Linq;
using System.Threading.Tasks;
using AO.BusinessContracts;
using CE.AO.Models;
using Microsoft.Practices.Unity;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace AO.Business.Tests.IntegrationTests
{
    /// <summary>
    /// Test Case Class for Account Lookup
    /// </summary>
    [TestClass]
    public class AccountLookupTest : TestBase
    {
        private IAccountLookup _iaccountLookup;

        /// <summary>
        /// Initialization of test case.
        /// </summary>
        [TestInitialize]
        public void Init()
        {
            _iaccountLookup = UnityContainer.Resolve<IAccountLookup>();
        }

        /// <summary>
        /// Test case for InsertOrMergeAccountLookupAsync method.
        /// </summary>
        [TestMethod]
        public void InsertOrMergeAccountLookupAsync()
        {
            var accountLookupModel = new AccountLookupModel()
            {
                CustomerId = "500",
                ClientId = 87,
                AccountId = "Ac500",
                ServiceContractId = "SC23",
                MeterId = "MET900",
                AccountMeterTimeStamp = DateTime.Now,
                IsActive = true,
                IsServiceContractCreatedBySystem = false
            };
            var result = _iaccountLookup.InsertOrMergeAccountLookupAsync(accountLookupModel).Result;
            Assert.AreEqual(result, true);
        }

        /// <summary>
        /// Test case for GetAccountMeterDetails Async method.
        /// </summary>
        [TestMethod]
        public void GetAccountMeterDetails_Async()
        {
            var result = _iaccountLookup.GetAccountMeterDetails(87, "MET900", "500").Result;
            Assert.AreEqual(result.ClientId, 87);
        }

        /// <summary>
        /// Test case for GetAccountMeterDetails method.
        /// </summary>
        [TestMethod]
        public void GetAccountMeterDetails_Sync()
        {
            var result = _iaccountLookup.GetAccountMeterDetails(87, "MET900");
            Assert.AreEqual(result[0].ClientId, 87);
        }


        /// <summary>
        /// Test case for GetAccountMeterDetails method - mulitple customers for the same account.
        /// </summary>
        [TestMethod]
        public void GetAccountMeterDetails_SharedAccount_Sync()
        {
            var customerId1 = "customer87testmultcustacctlee1";
            var customerId2 = "customer87testmultcustacctlee2";
            var result = _iaccountLookup.GetAccountMeterDetails(87, "meter87testmultcustacctlee");
            Assert.AreEqual(result[0].ClientId, 87);
            Assert.AreEqual(2, result.Count);
            Assert.IsTrue(result.Exists(c => c.CustomerId == customerId1));
            Assert.IsTrue(result.Exists(c => c.CustomerId == customerId2));
        }

        /// <summary>
        /// Test case for DeleteHistoryAccountLookupAsyc method.
        /// </summary>
        [TestMethod]
        public async Task DeleteHistoryAccountLookupAsyc()
        {
            var result = await _iaccountLookup.DeleteHistoryAccountLookupAsyc(87, "MET900", "500");
            Assert.AreEqual(result, true);
        }
    }
}
