﻿using System.Threading.Tasks;
using AO.BusinessContracts;
using CE.AO.Models;
using Microsoft.Practices.Unity;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace AO.Business.Tests.IntegrationTests
{
    /// <summary>
    /// Test Case Class for Customer 
    /// </summary>
    [TestClass]
    public class CustomerTest : TestBase
    {
        private ICustomer _iCustomer;

        /// <summary>
        /// Initialization of test case.
        /// </summary>
        [TestInitialize]
        public void Init()
        {
            _iCustomer = UnityContainer.Resolve<ICustomer>();
        }

        /// <summary>
        /// Test case for InsertOrMergeCustomerAsync method.
        /// </summary>
        [TestMethod]
        public async Task InsertOrMergeCustomerAsync()
        {
            var customerModel = new CustomerModel()
            {
                AddressLine1 = "1",
                AddressLine2 = "2",
                AddressLine3 = "3",
                City = "P",
                ClientId = 87,
                Country = "In",
                CustomerId = "2",
                CustomerType = "spl",
                EmailAddress = "gmail",
                FirstName = "tush",
                IsBusiness = true,
                LastName = "W",
                Phone1 = "89022",
                Phone2 = "",
                PostalCode = "422103",
                Source = "Branch",
                State = "MH",
                TrumpiaRequestDetailId = "25",
                TrumpiaSubscriptionId = "234"
            };
            var result = await _iCustomer.InsertOrMergeCustomerAsync(customerModel);
            Assert.IsTrue(result);
        }

        /// <summary>
        /// Test case for GetCustomerAsync method.
        /// </summary>
        [TestMethod]
        public async Task GetCustomerAsync()
        {
            var result = await _iCustomer.GetCustomerAsync(87, "2");
            Assert.AreEqual(result.ClientId, 87);
        }

        /// <summary>
        /// Test case for DeleteCustomerAsync method.
        /// </summary>
        [TestMethod]
        public async Task DeleteCustomerAsync()
        {
            var result = await _iCustomer.DeleteCustomerAsync(87, "2");
            Assert.AreEqual(result, true);
        }
    }
}
