﻿using CE.AO.Models;
using Microsoft.Practices.Unity;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace AO.Business.Tests.IntegrationTests
{
	/// <summary>
	/// Integration Test for MeterType
	/// </summary>
	[TestClass]
	public class MeterTypeTest : TestBase
	{
		private MeterType _meterType;

		/// <summary>
		/// Test Case Init
		/// </summary>
		[TestInitialize]
		public void Init()
		{
			_meterType = UnityContainer.Resolve<MeterType>();
		}

		/// <summary>
		/// Test case for InsertOrMergeMeterTypeAsync method in MeterType class
		/// </summary>
		[TestMethod]
		public void InsertOrMergeMeterTypeAsync()
		{
			MeterTypeModel meterTypeModel = new MeterTypeModel
			{
				Active = true,
				Descripton = "Test Description",
				MeterReadingUnit = "Test Unit",
				MeterReadingUtility = "Test Utitlity",
				MeterTypeId = 9999999,
				Model = "Test Model",
				PsionDisplay = "Test Display",
				Registercount = 000,
				UnitEnumerator = 1212,
				UnitScalar = null,
				Vendor = "TEst Vendor"
			};

		    // ReSharper disable once UnusedVariable
		    _meterType.InsertOrMergeMeterTypeAsync(meterTypeModel).Wait();
		    _meterType.GetMeterType(meterTypeModel.MeterTypeId);
			_meterType.DeleteMeterTypeAsync(meterTypeModel).Wait();
		}

	}
}
