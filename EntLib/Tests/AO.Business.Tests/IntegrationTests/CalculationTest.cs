﻿using System;
using System.Globalization;
using System.Threading.Tasks;
using AO.BusinessContracts;
using CE.AO.Models;
using Microsoft.Practices.Unity;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace AO.Business.Tests.IntegrationTests
{

    /// <summary>
    /// Test Case Class for Calculation 
    /// </summary>
    [TestClass]
    public class CalculationTest : TestBase
    {
        private ICalculation _iCalculation;

        /// <summary>
        /// Initialization of test case.
        /// </summary>
        [TestInitialize]
        public void Init()
        {
            _iCalculation = UnityContainer.Resolve<ICalculation>();
        }

        /// <summary>
        /// Test case for InsertCtdCalculationAsync method.
        /// </summary>
        [TestMethod]
        public async Task InsertCtdCalculationAsync()
        {
            var calculationModel = new CalculationModel
            {
                ClientId = 87,
                AccountId = "23",
                CustomerId = "32",
                MeterId = "23",
                ServiceContractId = "56",
                CommodityId = 23,
                Unit = 78,
                Cost = 105.245,
                ProjectedCost = 3333,
                ProjectedUsage = 222,
                Usage = 22,
                BillDays = 67,
                BillCycleStartDate = DateTime.Now.ToString(CultureInfo.InvariantCulture),
                BillCycleEndDate = DateTime.Now.ToString(CultureInfo.InvariantCulture),
                AsOfAmiDate = DateTime.Now,
                AsOfCalculationDate = DateTime.Now,
                RateClass = "C",
                AverageDailyUsage = 6785,
                AverageDailyCost = 98,
                Type = "X",
                IsLatest = true
            };
            await _iCalculation.InsertCtdCalculationAsync(calculationModel);
            Assert.AreEqual(true, true);
        }

        /// <summary>
        /// Test case for InsertBtdCalculationAsync method.
        /// </summary>
        [TestMethod]
        public async Task InsertBtdCalculationAsync()
        {
            var calculationModel = new CalculationModel
            {
                ClientId = 87,
                AccountId = "23",
                CustomerId = "32",
                MeterId = "23",
                ServiceContractId = "56",
                CommodityId = 23,
                Unit = 78,
                Cost = 105.245,
                ProjectedCost = 3333,
                ProjectedUsage = 222,
                Usage = 22,
                BillDays = 67,
                BillCycleStartDate = DateTime.Now.ToString("yyyy-MM-dd"),
                BillCycleEndDate = DateTime.Now.ToString("yyyy-MM-dd"),
                AsOfAmiDate = DateTime.Now,
                AsOfCalculationDate = DateTime.Now,
                RateClass = "C",
                AverageDailyUsage = 6785,
                AverageDailyCost = 98,
                Type = "C",
                IsLatest = true
            };
            await _iCalculation.InsertBtdCalculationAsync(calculationModel);
            Assert.AreEqual(true, true);
        }

        /// <summary>
        /// Test case for InsertBtdCalculationAsync method.
        /// </summary>
        [TestMethod]
        public async Task GetBtdCalculationAsync()
        {
            var result = await _iCalculation.GetBtdCalculationAsync(87, "23");
            if (result != null) Assert.AreEqual(result.ClientId, 87);
            else Assert.IsNull(result);
        }

        /// <summary>
        /// Test case for InsertBtdCalculationAsync method.
        /// </summary>
        [TestMethod]
        public async Task GetCtdCalculationAsync()
        {
            var result = await _iCalculation.GetCtdCalculationAsync(87, "23", "56");
            if (result != null) Assert.AreEqual(result.ClientId, 87);
            else Assert.IsNull(result);
        }

        /// <summary>
        /// Test case for DeleteCtdCalculationAsync method.
        /// </summary>
        [TestMethod]
        public void DeleteCtdCalculationAsync()
        {
            _iCalculation.DeleteCtdCalculationAsyc(87, "23").Wait();
        }

        /// <summary>
        /// Test case for DeleteBtdCalculationAsync method.
        /// </summary>
        [TestMethod]
        public void DeleteBtdCalculationAsync()
        {
            _iCalculation.DeleteBtdCalculationAsyc(87, "23").Wait();
        }
    }
}
