﻿using AO.BusinessContracts;
using CE.AO.Models;
using CE.BillToDate;
using Microsoft.Practices.Unity;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace AO.Business.Tests.IntegrationTests
{
    /// <summary>
    /// Test Case Class for ClientConfigFacade
    /// </summary>
    [TestClass]
    public class ClientConfigFacadeTest : TestBase
    {
        private IClientConfigFacade _clientConfigFacade;

        /// <summary>
        /// Initialization of test case.
        /// </summary>
        [TestInitialize]
        public void Init()
        {
            _clientConfigFacade = UnityContainer.Resolve<IClientConfigFacade>();
        }

        /// <summary>
        /// Test case for GetDynamicPortalModules() method. Ensure results are deserialized into model.
        /// </summary>
        [TestMethod]
        public void GetDynamicPortalModules()
        {
            var result = _clientConfigFacade.GetDynamicPortalModules(5);
            Assert.AreEqual(result.GetType(), typeof(DynamicAppModuleConfig));
        }

        /// <summary>
        /// Test case for GetBillToDateSettings() method. Ensure results are deserialized into model.
        /// </summary>
        [TestMethod]
        public void GetBillToDateSettings()
        {
            var result = _clientConfigFacade.GetBillToDateSettings(87);
            Assert.AreEqual(result.GetType(), typeof(BillToDateSettings));
        }

        /// <summary>
        /// Integration test for getting Standard Unit of measure setting from DB
        /// </summary>
        [TestMethod]
        public void GetStandardUom()
        {
            var result = _clientConfigFacade.GetStandardUom();
            Assert.AreEqual(result.GetType(), typeof(StandardUomModel));
        }


        /// <summary>
        /// Test case for GetClientSettings() method. Ensure results are deserialized into model.
        /// </summary>
        [TestMethod]
        public void GetClientSettings()
        {
            var result = _clientConfigFacade.GetClientSettings(87);
            Assert.AreEqual(result.GetType(), typeof(ClientSettings));
        }
    }
}
