﻿using System.Linq;
using AO.BusinessContracts;
using CE.AO.Models;
using Microsoft.Practices.Unity;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace AO.Business.Tests.IntegrationTests
{
    /// <summary>
    /// Test Case Class for PremiseTest 
    /// </summary>
    [TestClass]
    public class PremiseTest : TestBase
    {
        private IPremise _iPremise;

        /// <summary>
        /// Initialization of test case.
        /// </summary>
        [TestInitialize]
        public void Init()
        {
            _iPremise = UnityContainer.Resolve<IPremise>();
        }

        /// <summary>
        /// Test case for InsertOrMergePremiseAsync method.
        /// </summary>
        [TestMethod]
        public void InsertOrMergePremiseAsync()
        {
            PremiseModel premiseModel = new PremiseModel()
            {
                AddressLine1 = "H 24, Viola Hsg. Society",
                AddressLine2 = "Warje",
                AddressLine3 = "Pune",
                City = "Pune",
                ClientId = 87,
                Country = "US",
                CustomerId = "1012",
                PostalCode = "9041",
                State = "NJ",
                PremiseId = "1234",
                AccountId = "a5"
            };
            var result = _iPremise.InsertOrMergePremiseAsync(premiseModel).Result;
            Assert.AreEqual(result, true);
        }

        /// <summary>
        /// Test case for GetPremises method.
        /// </summary>
        [TestMethod]
        public void GetPremises()
        {
            PremiseModel premiseModel = new PremiseModel()
            {
                AddressLine1 = "H 24, Viola Hsg. Society",
                AddressLine2 = "Warje",
                AddressLine3 = "Pune",
                City = "Pune",
                ClientId = 87,
                Country = "US",
                CustomerId = "1012",
                PostalCode = "9041",
                State = "NJ",
                PremiseId = "1234",
                AccountId = "a5"
            };
            var result = _iPremise.GetPremises(87, "1012");
            var firstOrDefault = result.FirstOrDefault();
            if (firstOrDefault != null) Assert.AreEqual(firstOrDefault.ClientId, 87);
            else Assert.AreEqual(null, null);
        }

        /// <summary>
        /// Test case for DeletePremisesAsync method.
        /// </summary>
        [TestMethod]
        public void DeletePremisesAsync()
        {
            var result = _iPremise.DeletePremisesAsync(87, "1012", "1234").Result;
            Assert.AreEqual(result, true);
        }
    }
}
