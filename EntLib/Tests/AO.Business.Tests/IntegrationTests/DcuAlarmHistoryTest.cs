﻿using System;
using Microsoft.Practices.Unity;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using CE.AO.Models;

namespace AO.Business.Tests.IntegrationTests
{
	[TestClass]
	public class DcuAlarmHistoryTest : TestBase
	{
		private DcuAlarmHistory _dcuAlarm;

		[TestInitialize]
		public void Init()
		{
			_dcuAlarm = UnityContainer.Resolve<DcuAlarmHistory>();
		}

		/// <summary>
		/// Test case for InsertOrMergeDcuAlarmAsync method in DcuAlarmHistory class
		/// </summary>
		[TestMethod]
		public void InsertOrMergeDcuAlarmHistoryAsync()
		{
            DcuAlarmModel dcuAlarmModel = new DcuAlarmModel
            {
				ClientId = 87,
				AlarmTime = DateTime.UtcNow,
				DcuId = 1,
				AlarmCode = "Dcu_Call_Me",
				ComponentType = "Test_Component",
				NotificationType = "Test_Notification",
				Severity = 100,
				UserFriendlyDescription = "Testing Entity"
			};

		    // ReSharper disable once UnusedVariable
			var insertResult = _dcuAlarm.InsertOrMergeDcuAlarmHistoryAsync(dcuAlarmModel).Result;
			var isDelete = _dcuAlarm.DeleteDcuAlarmHistoryAsync(dcuAlarmModel).Result;
			Assert.IsTrue(isDelete);
		}

	}
}
