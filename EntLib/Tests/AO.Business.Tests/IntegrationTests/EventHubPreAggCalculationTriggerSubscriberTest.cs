﻿using System;
using System.Text;
using CE.AO.Models;
using Microsoft.Azure;
using Microsoft.Practices.Unity;
using Microsoft.ServiceBus;
using Microsoft.ServiceBus.Messaging;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;
using AO.EventProcessors;
using CE.AO.Utilities;
using Enums = CE.AO.Utilities.Enums;

namespace AO.Business.Tests.IntegrationTests
{

    /// <summary>
    /// Test class for EventHubPreAggCalculationTriggerSubscriber
    /// </summary>
    [TestClass]
    public class EventHubPreAggCalculationTriggerSubscriberTest : TestBase
    {
        private EventHubPreAggCalculationTriggerSubscriber _eventHubPreAggCalculationTriggerSubscriber;
        private ConsumptionByAccount _consumptionByAccount;
        private ConsumptionAggByAccount _consumptionAggByAccount;
        //private IPreAggCalculationFacade _preAggCalculationFacade;
        /// <summary>
        /// Test Case Init
        /// </summary>
        [TestInitialize]
        public void Init()
        {
            _eventHubPreAggCalculationTriggerSubscriber = UnityContainer.Resolve<EventHubPreAggCalculationTriggerSubscriber>();
            _consumptionByAccount = UnityContainer.Resolve<ConsumptionByAccount>();
            _consumptionAggByAccount = UnityContainer.Resolve<ConsumptionAggByAccount>();
        }

        /// <summary>
        /// Test method for EventHubPreAggCalculationTriggerSubscriber
        /// </summary>
        [TestMethod]
        public void TestEventHubPreAggCalculationTrigger()
        {
            _eventHubPreAggCalculationTriggerSubscriber.Subscribe();
            _eventHubPreAggCalculationTriggerSubscriber.UnSubscribe();

            #region Code to delete above created Event Hub

            var importDataEventHubName = CloudConfigurationManager.GetSetting("PreAggCalculationEventHubName"); //eventHubName
            var connectionString = CloudConfigurationManager.GetSetting("PreAggCalculationEventHubManageConnectionString");

            var connectionbuilder = new ServiceBusConnectionStringBuilder(connectionString)
            {
                TransportType = TransportType.Amqp
            };

            var namespaceManager = NamespaceManager.CreateFromConnectionString(connectionbuilder.ToString());
            namespaceManager.DeleteEventHub(importDataEventHubName);

            var isEventHubCreated = EventHubManager.GetNamespaceManager(connectionString).EventHubExists(importDataEventHubName);
            Assert.IsFalse(isEventHubCreated);
            #endregion
        }

        /// <summary>
        /// Test Case for OpenAsync Method
        /// </summary>
        [TestMethod]
        public void OpenAsync()
        {
            PartitionContext partitionContext = new PartitionContext();
            Lease lease = new Lease();
            partitionContext.Lease = lease;
            partitionContext.Lease.Offset = "Lease";

            var result = _eventHubPreAggCalculationTriggerSubscriber.OpenAsync(partitionContext);
            var result1 = _eventHubPreAggCalculationTriggerSubscriber.CloseAsync(partitionContext, CloseReason.Shutdown);
            Assert.IsTrue(result.IsCompleted);
            Assert.IsTrue(result1.IsCompleted);
        }

        /// <summary>
        /// Test case for ProcessEventsAsync in EventHubPreAggCalculationTriggerSubscriber Class
        /// </summary>
        [TestMethod]
        public void ProcessEventsAsync()
        {
            var utcDate = DateTime.UtcNow;
            var consumptionByAccountModel = new ConsumptionByAccountModel
            {
                AmiTimeStamp = utcDate,
                IntervalType = 60,
                TransponderPort = "1",
                UOMId = 3,
                AccountNumber = "TestEventhubAccount",
                ClientId = 899898,
                BatteryVoltage = "10",
                CommodityId = 3,
                MeterId = "Test Meter",
                TransponderId = "Test Transponder",
                Timezone = "PST",
                Consumption = 20,
                CustomerId = "Test Customer"
            };

            var consumptionByAccountModel2 = new ConsumptionByAccountModel
            {
                AmiTimeStamp = utcDate.AddHours(-2),
                IntervalType = 60,
                TransponderPort = "1",
                UOMId = 3,
                AccountNumber = "TestEventhubAccount",
                ClientId = 899898,
                BatteryVoltage = "10",
                CommodityId = 3,
                MeterId = "Test Meter",
                TransponderId = "Test Transponder",
                Timezone = "PST",
                Consumption = 10,
                CustomerId = "Test Customer"
            };

            var consumptionAggByAccountModel = new ConsumptionAggByAccountModel
            {
                StandardUOMId = 3,
                AccountNumber = "TestEventhubAccount",
                ClientId = 899898,
                CommodityId = 3,
                Timezone = "PST",
                StandardUOMConsumption = 10,
                AggregationEndDateTime = utcDate,
                AggregationType = Enums.PreAggregationTypes.Day.ToString()
            };

            _consumptionByAccount.InsertOrMergeConsumptionByAccountAsync(consumptionByAccountModel).Wait();
            _consumptionByAccount.InsertOrMergeConsumptionByAccountAsync(consumptionByAccountModel2).Wait();

            var accountPreAggCalculationRequestModel = new AccountPreAggCalculationRequestModel
            {
                ClientId = 899898,
                AccountId = "TestEventhubAccount",
                AmiTimestamp = DateTime.UtcNow,
                CalculationRange = Enums.PreAggCalculationRange.Day,
                Timezone = "PST"
            };

            var serializedEventData = JsonConvert.SerializeObject(accountPreAggCalculationRequestModel);
            var eventData = new EventData(Encoding.UTF8.GetBytes(serializedEventData));
            PartitionContext context = new PartitionContext();
            Lease lease = new Lease();
            context.Lease = lease;
            context.Lease.Offset = "Lease";
            context.Lease.PartitionId = "PartitionId";
            eventData.Properties.Add("ProcessingType", Enums.ProcessingType.File);
            eventData.Properties.Add("Source", "S");
            eventData.Properties.Add("RowIndex", 1);

            var eventDataList = new List<EventData> { eventData };
            // ReSharper disable once UnusedVariable
            var isOpen = _eventHubPreAggCalculationTriggerSubscriber.OpenAsync(context);
            _eventHubPreAggCalculationTriggerSubscriber.ProcessEventsAsync(context, eventDataList.AsEnumerable()).Wait();

            var isconsumptionByAccountdeleted = _consumptionByAccount.DeleteConsumptionByAccountAsync(consumptionByAccountModel).Result;
            var isconsumptionByAccountdeleted2 = _consumptionByAccount.DeleteConsumptionByAccountAsync(consumptionByAccountModel2).Result;
            _consumptionAggByAccount.DeleteConsumptionAggByAccountUsingPartionAsync(consumptionAggByAccountModel).Wait();

            Assert.IsTrue(isconsumptionByAccountdeleted);
            Assert.IsTrue(isconsumptionByAccountdeleted2);
        }
    }
}
