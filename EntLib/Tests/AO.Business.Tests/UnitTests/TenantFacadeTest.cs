﻿using System.Collections.Generic;
using AO.DataAccess.Contracts;
using AO.Domain;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace AO.Business.Tests.UnitTests
{
    [TestClass]
    public class TenantFacadeTest : TestBase
    {
        private TenantFacade _tenantFacade;
        private Mock<ITenantRepository> _tenantRepository;

        /// <summary>
        /// Test Case Init
        /// </summary>
        [TestInitialize]
        public void Init()
        {
            _tenantRepository = new Mock<ITenantRepository>();
            _tenantFacade = new TenantFacade(_tenantRepository.Object);
        }

        /// <summary>
        /// Test case for GetClients method
        /// </summary>
        [TestMethod]
        public void GetClients()
        {
            var client = new Client()
            {
                ClientID = 87
            };
            _tenantRepository.Setup(t => t.GetClients()).Returns(new List<Client> { client }).Verifiable();
            var result = _tenantFacade.GetClients();
            _tenantRepository.Verify(t => t.GetClients(), Times.Once);
            Assert.AreEqual(result.Count, 1);
        }
    }
}