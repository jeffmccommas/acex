﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Linq.Expressions;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using AO.Business.Tests.ObjectBuilders;
using AO.BusinessContracts;
using AO.DataAccess.Contracts;
using AO.Entities;
using AutoMapper;
using CE.AO.Models;
using CE.ContentModel.Entities;
using CE.RateModel;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Enums = CE.RateModel.Enums;

namespace AO.Business.Tests.UnitTests
{
    /// <summary>
    /// Contains test methods for CassandraBusiness.Notification class
    /// </summary>
    [TestClass]
    public class NotificationTest : TestBase
    {
        private Notification _notificationManager;
        private Mock<ITrumpiaRequestDetail> _trumpiaManagerMock;
        private Mock<ISubscription> _subscriptionManagerMock;
        private Mock<IEvaluation> _evaluationManagerMock;
        private Mock<ISendEmail> _sendEmailManagerMock;
        private Mock<IClientConfigFacade> _clientConfigFacadeMock;
        private Mock<INotificationRepository> _notificationRepositoryMock;

        /// <summary>
        /// Test Case Init
        /// </summary>
        [TestInitialize]
        public void Init()
        {
            _trumpiaManagerMock = new Mock<ITrumpiaRequestDetail>();
            _subscriptionManagerMock = new Mock<ISubscription>();
            _sendEmailManagerMock = new Mock<ISendEmail>();
            _clientConfigFacadeMock = new Mock<IClientConfigFacade>();
            _notificationRepositoryMock = new Mock<INotificationRepository>();
            _evaluationManagerMock = new Mock<IEvaluation>();

            _notificationManager = new Notification(LogModelMock.Object,
                _notificationRepositoryMock.Object, _trumpiaManagerMock.Object,
                _subscriptionManagerMock.Object, _evaluationManagerMock.Object,
                 new Lazy<ISendEmail>(() => _sendEmailManagerMock.Object),
                new Lazy<IClientConfigFacade>(() => _clientConfigFacadeMock.Object),
                new Lazy<ICassandraRepository>(() => CassandraRepositoryMock.Object));
        }


        public class FormatTestConfiguration {
            public string useFormat { get; set; }  // which of the configured formats to use
            public string defaultFormat { get; set; }  // what format to use if there is no matching configured format
            public double rawValue { get; set; }  // the raw value to be formatted
            public string formattedValue { get; set; }  // what we expect the formatted string to look like
        }

        /// <summary>
        /// Test case to validate the new formatting for notification messages (put here because notifications uses functionality first).
        /// </summary>
        [TestMethod]
        public void ProcessFormatting() {
            var clientSettings = new ClientSettings
            {
                ExtendedAttributes = "altNumericFormat=0,noDecimalFormat=0,altUsageFormat=0000.0000,normalDecimalFormat=0.00"
            };
            List<FormatTestConfiguration> testRows = CreateFormatTestRows();
            RunNumericFormatTests(testRows, clientSettings);

            // try again with no extended config
            clientSettings = new ClientSettings
            {
                //ExtendedAttributes = "altNumericFormat=0,noDecimalFormat=0,altUsageFormat=0000.0000,normalDecimalFormat=0.00"
            };
            foreach (FormatTestConfiguration ftc in testRows) {
                ftc.formattedValue = "123.46";
            }
            RunNumericFormatTests(testRows, clientSettings);
        }

        /// <summary>
        /// run a set of tests for format changes
        /// </summary>
        /// <param name="testRows"></param>
        /// <param name="clientSettings"></param>
        private static void RunNumericFormatTests(List<FormatTestConfiguration> testRows, ClientSettings clientSettings) {
            for (int i = 0; i < testRows.Count; i++) {
                string formattedValue = clientSettings.GetFormattedValue(testRows[i].rawValue, testRows[i].defaultFormat,
                    testRows[i].useFormat);
                Debug.WriteLine(
                    $" fmtTest got={formattedValue} for {testRows[i].rawValue}, {testRows[i].formattedValue}, {testRows[i].defaultFormat}, {testRows[i].useFormat} ");
                Assert.AreEqual(testRows[i].formattedValue, formattedValue);
            }
            Assert.AreEqual("123.46", clientSettings.GetFormattedValue(123.456));
            Assert.AreEqual("123.5", clientSettings.GetFormattedValue(123.456, "0.0", null));
        }


        /// <summary>
        /// create a set of data for running format tests
        /// </summary>
        /// <returns></returns>
        private static List<FormatTestConfiguration> CreateFormatTestRows() {
            List<FormatTestConfiguration> testRows = new List<FormatTestConfiguration>();
            testRows.Add(new FormatTestConfiguration {
                defaultFormat = "0.00",
                useFormat = "altNumericFormat",
                rawValue = 123.456,
                formattedValue = "123"
            });
            testRows.Add(new FormatTestConfiguration {
                defaultFormat = "0.00",
                useFormat = "altNumericFormat",
                rawValue = 123.456,
                formattedValue = "123"
            });
            testRows.Add(new FormatTestConfiguration {
                defaultFormat = "0.00",
                useFormat = "noDecimalFormat",
                rawValue = 123.456,
                formattedValue = "123"
            });
            testRows.Add(new FormatTestConfiguration {
                defaultFormat = "0.00",
                useFormat = "altUsageFormat",
                rawValue = 123.456,
                formattedValue = "0123.4560"
            });
            testRows.Add(new FormatTestConfiguration {
                defaultFormat = "0.00",
                useFormat = "doesntExist",
                rawValue = 123.456,
                formattedValue = "123.46"
            });
            testRows.Add(new FormatTestConfiguration {
                defaultFormat = "0.00",
                useFormat = null,
                rawValue = 123.456,
                formattedValue = "123.46"
            });
            testRows.Add(new FormatTestConfiguration {
                defaultFormat = null,
                useFormat = null,
                rawValue = 123.456,
                formattedValue = "123.46"
            });
            return testRows;
        }


        /// <summary>
        /// Test case to send notification using email/sms
        /// </summary>
        [TestMethod]
        public void ProcessMessage() {
            //return;
#pragma warning disable 162
            SmsTemplateEntity smsTemplateModel = new SmsTemplateEntity
            {
                ClientId = 1,
                Body = "Sample--ServiceLevelUsageThreshold--ServiceLevelCostThreshold--AccountLevelCostThreshold--AccountNumber--BillToDate--ProjectedCost--NoOfDaysRemaining--UOM--FUEL--CostToDate--Usage--DayThreshold--ServiceLevelTieredThresholdApproaching--ServiceLevelTieredThresholdExceed-"
            };
            NotificationEntity notifiactionEntity = new NotificationEntity
            {
                ClientId = 1,
                AccountId = "AC0001",
                ServiceContractId = "",
                ProgramName = "testProgram",
                Insight = "BillToDate",
                IsNotified = false,
                IsDelivered = false,
                IsDropped = false,
                IsBounced = false,
                IsOpened = false,
                IsClicked = false,
                IsScheduled = false,
                Channel = "",
                TemplateId = "",
                CustomerId = "1",
                EvaluationType = "",
                NotifiedDateTime = DateTime.Now,
                TrumpiaRequestDetailId = "",
                IsLatest = true
            };

            // CJH inconsistencies  var clientUoms = new List<ClientUOM> { new ClientUOM { Key = "gal", NameKey = "Gallons" } };
            var clientUoms = new List<ClientUOM> { new ClientUOM { Key = "mcf", NameKey = "MCubFeet" } };
            var clientText = new List<ClientTextContent> { new ClientTextContent() { Key = "mcf", ShortText = "MCubFeet" } };

            _clientConfigFacadeMock.Setup(mock => mock.GetClientUom(It.IsAny<int>())).Returns(clientUoms).Verifiable();
            _clientConfigFacadeMock.Setup(mock => mock.GetTextContents(It.IsAny<int>(), It.IsAny<string>(),It.IsAny<string>())).Returns(clientText).Verifiable();

            CassandraRepositoryMock.Setup(
                t => t.GetSingleAsync(It.IsAny<string>(), It.IsAny<Expression<Func<SmsTemplateEntity, bool>>>()))
                .Returns(Task.FromResult(smsTemplateModel));
            CassandraRepositoryMock.Setup(
                t => t.GetSingleAsync(It.IsAny<string>(), It.IsAny<Expression<Func<NotificationEntity, bool>>>()))
                .Returns(Task.FromResult(notifiactionEntity));
            CassandraRepositoryMock.Verify(t => t.GetSingleAsync(It.IsAny<string>(), It.IsAny<Expression<Func<SmsTemplateEntity, bool>>>()), Times.AtMost(5));
            var subscriptionModel = new SubscriptionModel
            {
                ClientId = 87,
                CustomerId = "1234",
                AccountId = "AC0001",
                ProgramName = "testprogram",
                InsightTypeName = "",
                PremiseId = "",
                ServiceContractId = "SC001",
                ServicePointId = "",
                IsEmailOptInCompleted = true,
                IsSmsOptInCompleted = true,
                IsSelected = true,
                Channel = "Email",
                AllowedChannels = "",
                SubscriberThresholdValue = 1234,
                CommodityKey = "Water",
                ThresholdType = "",
                ThresholdMin = 132,
                ThresholdMax = 136,
                IsDoubleOptIn = true,
                UomId = 12,  // CJH: mcf is 12, gal is 6
                Type = "",
                IsLatest = true
            };
            var subscriptionModelList = new List<SubscriptionModel> { subscriptionModel };

            var insightModel = new InsightModel
            {
                ClientId = 87,
                AccountId = "1234",
                ServiceContractId = "",
                BilllDays = 23,
                BtdCost = 2332,
                BtdProjectedCost = 3544,
                Usage = 32,
                SupressInsight = true,
                AccountLevelCostThresholdExceed = true,
                ServiceLevelUsageThresholdExceed = true,
                ServiceLevelCostThresholdExceed = true,
                DayThresholdExceed = true,
                ServiceLevelTiered1ThresholdApproaching = true,
                ServiceLevelTieredThresholdExceed = true,
                ServiceDays = 32,
                CtdCost = 4545,
                CtdProjectedCost = 5678,
                NotifyTime = DateTime.Now.ToShortDateString(),
                ProgramName = "testprogram",
                AsOfEvaluationDate = DateTime.Now,
				AsOfEvaluationId = Guid.NewGuid(),
                BillCycleStartDate = "12 12",
                BillCycleEndDate = " 12 12 12",
                Type = "ABC",
                IsLatest = true
            };
            var insightSettings = new InsightSettings
            {
                InsightName = "BillToDate",
                UtilityInsightName = "BillToDate",
                AllowedCommunicationChannel = "Email",
                DefaultCommunicationChannel = "Email",
                DefaultFrequency = "Daily",
                DefaultValue = "Daily",
                EmailTemplateId = "1",
                SmsTemplateId = "87_1",
                NotifyTime = "13:00",
                NotificationDay = "Monday",
                TrumpiaKeyword = "TrumpiaKeywordTest",
                Level = "Basic",
                CommodityType = "Water",
                ThresholdType = "TestThresholdType",
                ThresholdMin = "50",
                ThresholdMax = "100"
            };

            var insightSettingsList = new List<InsightSettings> { insightSettings };

            var programSettings = new ProgramSettings
            {
                ProgramName = "testprogram",
                UtilityProgramName = "testprogram",
                FileExportRequired = false,
                DoubleOptInRequired = false,
                TrumpiaKeyword = "trumpiatestkeyword",
                Insights = insightSettingsList
            };
            var programSettingsList = new List<ProgramSettings> { programSettings };


            var clientSettings = new ClientSettings
            {
                ClientId = 87,
                TimeZone = "ADT",
                IsAmiIntervalStart = false,
                IsDstHandled = false,
                MinimumDaysForInsightNotification = 1,
                CalculationScheduleTime = "",
                NotificationReportScheduleTime = "",
                InsightReportScheduleTime = "",
                ForgotPasswordEmailTemplateId = "",
                OptInEmailUserName = "UserNameTest",
                OptInEmailPassword = "test@123",
                OptInEmailFrom = "testemail@gmail.com",
                RegistrationConfirmationEmailTemplateId = "",
                OptInEmailConfirmationTemplateId = "",
                OptInSmsConfirmationTemplateId = "",
                TrumpiaShortCode = "",
                TrumpiaApiKey = "TrumpiaTestKey",
                TrumpiaUserName = "TrumpiaTestUserName",
                TrumpiaContactList = "",
                UnmaskedAccountIdEndingDigit = 1,
                //NotifyAltNumericFormat = "0",
                //ExtendedAttributes = "altNumericFormat=0,noDecimalFormat=0,altUsageFormat=000.0000,normalDecimalFormat=0.00",
                ExtendedAttributes = "",
                Programs = new List<ProgramSettings>(programSettingsList)
            };
            var customerModel = new CustomerModel
            {
                ClientId = 87,
                CustomerId = "1234",
                Country = "USA",
                Source = "",
                IsBusiness = false,
                AddressLine1 = "C Block",
                AddressLine2 = "Flat no B-804",
                AddressLine3 = "Wakad",
                City = "Pune",
                State = "New york",
                PostalCode = "NY001",
                FirstName = "Sharad",
                LastName = "Saini",
                Phone1 = "9911559988",
                Phone2 = "8975757012",
                EmailAddress = "tushar.wadhavane@saviantconsulting.com",
                CustomerType = "Good",
                TrumpiaSubscriptionId = "87",
                TrumpiaRequestDetailId = ""
            };
            var billingModel = new BillingModel
            {
                ClientId = 87,
                CustomerId = "1234",
                AccountId = "AC00012",
                PremiseId = "12",
                AmiStartDate = DateTime.Now,
                AmiEndDate = DateTime.Now,
                BillCycleScheduleId = "BS12122",
                RateClass = "Rate Class",
                ServicePointId = "SP0012",
                Source = "Rate Class",
                MeterType = "",
                UOMId = 12,
                TotalUsage = 1232323,
                StartDate = DateTime.Now,
                EndDate = DateTime.Today.AddDays(12),
                BillDays = 12,
                ReadQuality = "",
                TotalCost = 122323,
                CommodityId = 2,
                MeterId = " ",
                BillPeriodType = 2,
                ReplacedMeterId = "",
                ReadDate = DateTime.Now,
                ServiceContractId = "SC001",
                IsFault = false,
                IsLatest = true
            };
            var tierBoundary = new TierBoundary
            {
                BaseOrTier = Enums.BaseOrTier.Tier6,
                TimeOfUse = Enums.TimeOfUse.OnPeak,
                Season = Enums.Season.SeasonB,
                DaysIntoSeason = 1,
                SeasonFactor = 1,
                Threshold = 2,
                SecondaryThreshold = 3
            };
            var listTierBoundary = new List<TierBoundary> { tierBoundary };
            var dicTierBoundary = new Dictionary<string, List<TierBoundary>>
            {
                {"SC001", listTierBoundary}
            };
            var calculationModel = new CalculationModel
            {
                ClientId = 87,
                AccountId = "1234",
                CustomerId = "AC00012",
                MeterId = "",
                ServiceContractId = "",
                CommodityId = 1,
                Unit = 2,
                Cost = 22.3,
                ProjectedCost = 23,
                ProjectedUsage = 32,
                Usage = 2,
                BillDays = 1,
                BillCycleStartDate = "08/18/2016",
                BillCycleEndDate = "08/20/2016",
                AsOfAmiDate = DateTime.Now.Date,
                AsOfCalculationDate = DateTime.Now,
                RateClass = "",
                AverageDailyUsage = 23,
                AverageDailyCost = 23,
                Type = "",
                IsLatest = false

            };

            var calculationModelList = new List<CalculationModel> { calculationModel };
            
            var result = _notificationManager.ProcessMessage(subscriptionModelList, insightModel, clientSettings, customerModel, billingModel, dicTierBoundary, calculationModelList);
            insightSettings.InsightName = "AccountLevelCostThreshold";
            var result7 = Task.Run(() => _notificationManager.ProcessMessage(subscriptionModelList, insightModel, clientSettings, customerModel, billingModel, dicTierBoundary, calculationModelList)).ContinueWith(t =>
            {
                if (t.Exception != null)
                {
                }
            });
            result7.Wait();

            clientSettings.ExtendedAttributes = "";

            //For AccountProjectedCost insightName
            insightSettings.InsightName = "AccountProjectedCost";
            var result4 = Task.Run(() => _notificationManager.ProcessMessage(subscriptionModelList, insightModel, clientSettings, customerModel, billingModel, dicTierBoundary, calculationModelList)).ContinueWith(t =>
            {
                if (t.Exception != null)
                {
                }
            });
            result4.Wait();

            subscriptionModel.Channel = "SMS";
            insightSettings.InsightName = "CostToDate";
            insightModel.ServiceContractId = "SEC009";
            calculationModel.ServiceContractId = "SEC009";
            calculationModel.CommodityId = 3;
            var firstOrDefault = clientSettings.Programs.FirstOrDefault();
            var orDefault = firstOrDefault?.Insights.FirstOrDefault();
            if (orDefault != null)
                orDefault.Level = "Account";
            var result2 = Task.Run(() => _notificationManager.ProcessMessage(subscriptionModelList, insightModel, clientSettings, customerModel, billingModel, dicTierBoundary, calculationModelList)).ContinueWith(t =>
            {
                if (t.Exception != null)
                {
                }
            });
            result2.Wait();

            subscriptionModel.Channel = "EmailAndSMS";
            var result13 = Task.Run(() => _notificationManager.ProcessMessage(subscriptionModelList, insightModel, clientSettings, customerModel, billingModel, dicTierBoundary, calculationModelList)).ContinueWith(t =>
            {
                if (t.Exception != null)
                {
                }
            });
            result13.Wait();
            
            insightSettings.InsightName = "ServiceProjectedCost";
            var result5 = Task.Run(() => _notificationManager.ProcessMessage(subscriptionModelList, insightModel, clientSettings, customerModel, billingModel, dicTierBoundary, calculationModelList)).ContinueWith(t =>
            {
                if (t.Exception != null)
                {
                }
            });
            result5.Wait();

            insightSettings.InsightName = "ServiceLevelTieredThresholdExceed";
            var result11 = Task.Run(() => _notificationManager.ProcessMessage(subscriptionModelList, insightModel, clientSettings, customerModel, billingModel, dicTierBoundary, calculationModelList)).ContinueWith(t =>
            {
                if (t.Exception != null)
                {
                }
            });
            result11.Wait();

            insightSettings.InsightName = "ServiceLevelUsageThreshold";
            var result8 = Task.Run(() => _notificationManager.ProcessMessage(subscriptionModelList, insightModel, clientSettings, customerModel, billingModel, dicTierBoundary, calculationModelList)).ContinueWith(t =>
            {
                if (t.Exception != null)
                {
                }
            });
            result8.Wait();

            insightSettings.InsightName = "ServiceLevelCostThreshold";
            var result9 = Task.Run(() => _notificationManager.ProcessMessage(subscriptionModelList, insightModel, clientSettings, customerModel, billingModel, dicTierBoundary, calculationModelList)).ContinueWith(t =>
            {
                if (t.Exception != null)
                {
                }
            });
            result9.Wait();

            insightSettings.InsightName = "DayThreshold";
            var result10 = Task.Run(() => _notificationManager.ProcessMessage(subscriptionModelList, insightModel, clientSettings, customerModel, billingModel, dicTierBoundary, calculationModelList)).ContinueWith(t =>
            {
                if (t.Exception != null)
                {
                }
            });
            result10.Wait();

            insightSettings.InsightName = "Usage";
            var result6 = Task.Run(() => _notificationManager.ProcessMessage(subscriptionModelList, insightModel, clientSettings, customerModel, billingModel, dicTierBoundary, calculationModelList)).ContinueWith(t =>
            {
                if (t.Exception != null)
                {
                }
            });
            result6.Wait();


            var result3 = Task.Run(() => _notificationManager.ProcessMessage(subscriptionModelList, insightModel, clientSettings, customerModel, billingModel, dicTierBoundary, calculationModelList)).ContinueWith(t =>
            {
                if (t.Exception != null)
                {
                }
            });
            result3.Wait();

            insightSettings.InsightName = "ServiceLevelTieredThresholdApproaching";
            var result14 = Task.Run(() => _notificationManager.ProcessMessage(subscriptionModelList, insightModel, clientSettings, customerModel, billingModel, dicTierBoundary, calculationModelList)).ContinueWith(t =>
            {
                if (t.Exception != null)
                {
                }
            });
            result14.Wait();

            subscriptionModel.Channel = "File";
            var result12 = _notificationManager.ProcessMessage(subscriptionModelList, insightModel, clientSettings, customerModel, billingModel, dicTierBoundary, calculationModelList);

            Assert.AreEqual(result.Exception, null);
            Assert.AreEqual(result2.Exception, null);
            Assert.AreEqual(result3.Exception, null);
            Assert.AreEqual(result4.Exception, null);
            Assert.AreEqual(result5.Exception, null);
            Assert.AreEqual(result6.Exception, null);
            Assert.AreEqual(result7.Exception, null);
            Assert.AreEqual(result8.Exception, null);
            Assert.AreEqual(result9.Exception, null);
            Assert.AreEqual(result10.Exception, null);
            Assert.AreEqual(result11.Exception, null);
            Assert.AreEqual(result12.Exception, null);
            Assert.AreEqual(result13.Exception, null);
            Assert.AreEqual(result14.Exception, null);

            MockRepository.VerifyAll();
#pragma warning restore 162
        }

        /// <summary>
        /// test case to fetch the sms template for subscription double opt in confirmation with replaced substitutions
        /// </summary>
        [TestMethod]
        public void GetReplacedSmsTemplateForSubscriptionDoubleOptInConfirmation()
        {
            string smsBody = "You are subscribed to -ProgramName-. Your account number is -AccountNumber-. Your trumpia Keyword and short code are -Keyword-,-ShortCode- respectively. Thanks";
            var subscriptionModel = new SubscriptionModel
            {
                ClientId = 1,
                CustomerId = "1234",
                AccountId = "AC0001",
                ProgramName = "testprogram",
                InsightTypeName = "",
                PremiseId = "",
                ServiceContractId = "SC001",
                ServicePointId = "",
                IsEmailOptInCompleted = false,
                IsSmsOptInCompleted = true,
                IsSelected = true,
                Channel = "SMS",
                AllowedChannels = "",
                SubscriberThresholdValue = 1234,
                CommodityKey = "Water",
                ThresholdType = "",
                ThresholdMin = 132,
                ThresholdMax = 136,
                IsDoubleOptIn = true,
                UomId = 12,
                Type = "",
                IsLatest = true
            };

            var insightSettings = new InsightSettings
            {
                InsightName = "BillToDate",
                UtilityInsightName = "BillToDate",
                AllowedCommunicationChannel = "TestAllowedCommunicationChannel",
                DefaultCommunicationChannel = "TestDefaultCommunicationChannel",
                DefaultFrequency = "Daily",
                DefaultValue = "Daily",
                EmailTemplateId = "1",
                SmsTemplateId = "1",
                NotifyTime = "13:00",
                NotificationDay = "Monday",
                TrumpiaKeyword = "TrumpiaKeywordTest",
                Level = "Basic",
                CommodityType = "Water",
                ThresholdType = "TestThresholdType",
                ThresholdMin = "50",
                ThresholdMax = "100"
            };

            var insightSettingsList = new List<InsightSettings> { insightSettings };

            var programSettings = new ProgramSettings
            {
                ProgramName = "testprogram",
                UtilityProgramName = "testprogram",
                FileExportRequired = false,
                DoubleOptInRequired = false,
                TrumpiaKeyword = "smshulk",
                Insights = insightSettingsList
            };
            var programSettingsList = new List<ProgramSettings> { programSettings };

            var clientSettings = new ClientSettings
            {

                ClientId = 1,
                TimeZone = "ADT",
                IsAmiIntervalStart = false,
                IsDstHandled = false,
                MinimumDaysForInsightNotification = 1,
                CalculationScheduleTime = "",
                NotificationReportScheduleTime = "",
                InsightReportScheduleTime = "",
                ForgotPasswordEmailTemplateId = "",
                OptInEmailUserName = "UserNameTest",
                OptInEmailPassword = "test@123",
                OptInEmailFrom = "testemail@gmail.com",
                RegistrationConfirmationEmailTemplateId = "",
                OptInEmailConfirmationTemplateId = "",
                OptInSmsConfirmationTemplateId = "",
                TrumpiaShortCode = "TSC001",
                TrumpiaApiKey = "TrumpiaTestKey",
                TrumpiaUserName = "TrumpiaTestUserName",
                TrumpiaContactList = "",
                UnmaskedAccountIdEndingDigit = 0,
                Programs = new List<ProgramSettings>(programSettingsList)
            };

            var customerModel = new CustomerModel
            {
                ClientId = 1,
                CustomerId = "1234",
                Country = "USA",
                Source = "",
                IsBusiness = false,
                AddressLine1 = "C Block",
                AddressLine2 = "Flat no B-804",
                AddressLine3 = "Wakad",
                City = "Pune",
                State = "New york",
                PostalCode = "NY001",
                FirstName = "Sharad",
                LastName = "Saini",
                Phone1 = "9911559988",
                Phone2 = "8975757012",
                EmailAddress = "sharad.saini@saviantconsulting.com",
                CustomerType = "Good",
                TrumpiaSubscriptionId = "",
                TrumpiaRequestDetailId = ""

            };

            var substitutes = Regex.Matches(smsBody, "[-][a-zA-Z]+[-]");
            var result = _notificationManager.GetReplacedSmsTemplateForSubscriptionDoubleOptInConfirmation(smsBody, subscriptionModel, clientSettings, customerModel, substitutes);

            Assert.AreEqual("You are subscribed to testprogram. Your account number is AC0001. Your trumpia Keyword and short code are smshulk,TSC001 respectively. Thanks", result);
            MockRepository.VerifyAll();
        }

        /// <summary>
        /// test case to fetch all the notification details of specific date from the database
        /// </summary>
        [TestMethod]
        public void GetDayNotifications()
        {
            var notificationEntity = new NotificationBuilder().WithClientId(1234).WithCustomerId("C0001").WithAccountId("AC0001").Build();
            IList<NotificationEntity> notificationEntityList = new List<NotificationEntity> {notificationEntity};

            _notificationRepositoryMock.Setup(t => t.GetNotifications(It.IsAny<int>(), It.IsAny<string>(), It.IsAny<string>()))
                .Returns(Task.FromResult(notificationEntityList))
                .Verifiable();

            // Act
            var result = _notificationManager.GetDayNotifications(notificationEntity.ClientId,
                notificationEntity.CustomerId, notificationEntity.AccountId, DateTime.UtcNow);

            // Assert
            Assert.AreEqual(1, result.Count());
            MockRepository.VerifyAll();
        }

        /// <summary>
        /// Test case to fetch the notification detail from the database
        /// </summary>
        [TestMethod]
        public void GetNotificationSync()
        {
            // Arrange
            var notificationEntity = new NotificationBuilder().WithClientId(1234).WithAccountId("4321").Build();

            _notificationRepositoryMock.Setup(mock => mock.GetNotification(It.IsAny<NotificationEntity>()))
                .Returns(Task.FromResult(notificationEntity))
                .Verifiable();

            // Act
            var result = _notificationManager.GetNotificationSync(notificationEntity.ClientId, notificationEntity.CustomerId,
                notificationEntity.AccountId, notificationEntity.ProgramName, notificationEntity.ServiceContractId,
                notificationEntity.Insight, notificationEntity.Channel, true,
                notificationEntity.NotifiedDateTime.ToShortDateString());

            // Assert
            Assert.AreEqual(notificationEntity.CustomerId, result.CustomerId);
            Assert.AreEqual(notificationEntity.ClientId, result.ClientId);
            Assert.AreEqual(notificationEntity.AccountId, result.AccountId);
            MockRepository.VerifyAll();
        }

        /// <summary>
        /// Test case to update notification with email event like delivered, bounce etc.
        /// </summary>
        [TestMethod]
        public void SaveNotificationFromEmail()
        {
            // Arrange
            var notificationEntity = new NotificationBuilder().WithClientId(1234).WithAccountId("4321").Build();
            var notificationModel = Mapper.Map<NotificationEntity, NotificationModel>(notificationEntity);

            _notificationRepositoryMock.Setup(mock => mock.GetNotification(It.IsAny<NotificationEntity>()))
                .Returns(Task.FromResult(notificationEntity))
                .Verifiable();

            // Act
            _notificationManager.SaveNotification(notificationModel);
            var result = _notificationManager.GetNotificationSync(notificationModel);

            // Assert
            Assert.AreEqual(result.ClientId, notificationModel.ClientId);
            Assert.AreEqual(result.AccountId, notificationModel.AccountId);
            Assert.AreEqual(result.NotifiedDateTime, notificationModel.NotifiedDateTime);
            Assert.AreEqual(result.IsLatest, notificationEntity.IsLatest);
            MockRepository.VerifyAll();
        }

        /// <summary>
        /// Test case to insert or update latest NotificationModel to database
        /// </summary>
        [TestMethod]
        public void SaveNotification()
        {
            // Arrange
            var notificationEntity = new NotificationBuilder().WithClientId(1234).WithAccountId("4321").Build();
            var notificationModel = Mapper.Map<NotificationEntity, NotificationModel>(notificationEntity);

            _notificationRepositoryMock.Setup(mock => mock.GetNotification(It.IsAny<NotificationEntity>()))
                .Returns(Task.FromResult(notificationEntity))
                .Verifiable();

            // Act
            _notificationManager.SaveNotification(notificationModel, true);
            var result = _notificationManager.GetNotificationSync(notificationModel);

            // Assert
            Assert.AreEqual(result.ClientId, notificationModel.ClientId);
            Assert.AreEqual(result.AccountId, notificationModel.AccountId);
            Assert.AreEqual(result.NotifiedDateTime, notificationModel.NotifiedDateTime);
            Assert.IsTrue(notificationEntity.IsLatest);
            MockRepository.VerifyAll();
        }

        /// <summary>
        /// Test case to get notification for client within provided date range
        /// </summary>
        [TestMethod]
        public void GetNotificationsByClientId()
        {
            // Arrange
            var entity = new NotificationBuilder().WithClientId(1234).Build();
            IList<NotificationEntity> notificationEntityList = new List<NotificationEntity> { entity };

            _notificationRepositoryMock.Setup(mock => mock.GetNotifications(It.IsAny<int>(), It.IsAny<DateTime>(), It.IsAny<DateTime>()))
                .Returns(Task.FromResult(notificationEntityList))
                .Verifiable();

            // Act
            var result = _notificationManager.GetNotifications(entity.ClientId, DateTime.UtcNow, DateTime.UtcNow.AddDays(20)).ToList();

            // Assert
            Assert.AreEqual(1, result.Count);
            Assert.AreEqual(result.First().AccountId, entity.AccountId);
            MockRepository.VerifyAll();
        }

        /// <summary>
        /// Test method to get all notifications for all clients
        /// </summary>
        [TestMethod]
        public void GetNotifications()
        {
            // Arrange
            var entity1 = new NotificationBuilder().WithScheduled().WithClientId(1234).Build();
            var entity2 = new NotificationBuilder().WithScheduled().WithClientId(5678).Build();
            var invalidEntity = new NotificationBuilder().WithAccountId("Invalid").Build();
            IList<NotificationEntity> notificationEntityList = new List<NotificationEntity> { entity1, entity2 };

            _notificationRepositoryMock.Setup(mock => mock.GetNotifications())
                .Returns(Task.FromResult(notificationEntityList))
                .Verifiable();

            // Act
            var result = _notificationManager.GetScheduledNotifications();

            // Assert
            Assert.AreEqual(2, result.Count);
            Assert.IsTrue(result.Any(r => r.ClientId == entity1.ClientId));
            Assert.IsTrue(result.Any(r => r.ClientId == entity2.ClientId));
            Assert.IsFalse(result.Any(r=>r.AccountId == invalidEntity.AccountId));
            MockRepository.VerifyAll();
        }

        /// <summary>
        /// Test method to fetch the sms template from database
        /// </summary>
        [TestMethod]
        public void GetSmsTemplateFromDb()
        {
            var smsTemplateEntity = new SmsTemplateEntity
            {
                ClientId = 1,
                InsightTypeName = "BillToDate",
                Body = "Template Body for testing purpose",
                TemplateId = "1"
            };

            CassandraRepositoryMock.Setup(t => t.GetSingleAsync(It.IsAny<string>(), It.IsAny<Expression<Func<SmsTemplateEntity, bool>>>())).Returns(Task.FromResult(smsTemplateEntity)).Verifiable();

            var result = _notificationManager.GetSmsTemplateFromDB(1, "BillToDate", "1");

            CassandraRepositoryMock.Verify(t => t.GetSingleAsync(It.IsAny<string>(), It.IsAny<Expression<Func<SmsTemplateEntity, bool>>>()), Times.Once);
            CassandraRepositoryMock.VerifyAll();
            Assert.AreEqual("1", result.Result.TemplateId);
            MockRepository.VerifyAll();
        }

        /// <summary>
        /// Test method to send sms
        /// get the report status on the sms request
        /// update notification with the status
        /// </summary>
        [TestMethod]
        public void SendSms()
        {
            var billingModel = new BillingModel
            {
                ClientId = 1,
                CustomerId = "1234",
                AccountId = "AC00012",
                PremiseId = "12",
                AmiStartDate = DateTime.Now,
                AmiEndDate = DateTime.Now,
                BillCycleScheduleId = "BS12122",
                RateClass = "Rate Class",
                ServicePointId = "SP0012",
                Source = "Rate Class",
                MeterType = "",
                UOMId = 12,
                TotalUsage = 1232323,
                StartDate = DateTime.Now,
                EndDate = DateTime.Today.AddDays(12),
                BillDays = 12,
                ReadQuality = "",
                TotalCost = 122323,
                CommodityId = 2,
                MeterId = " ",
                BillPeriodType = 2,
                ReplacedMeterId = "",
                ReadDate = DateTime.Now,
                ServiceContractId = "SC001",
                IsFault = false,
                IsLatest = true
            };
            var subscriptionModel = new SubscriptionModel
            {
                ClientId = 1,
                CustomerId = "1234",
                AccountId = "AC0001",
                ProgramName = "testprogram",
                InsightTypeName = "",
                PremiseId = "",
                ServiceContractId = "SC001",
                ServicePointId = "",
                IsEmailOptInCompleted = false,
                IsSmsOptInCompleted = true,
                IsSelected = true,
                Channel = "SMS",
                AllowedChannels = "",
                SubscriberThresholdValue = 1234,
                CommodityKey = "Water",
                ThresholdType = "",
                ThresholdMin = 132,
                ThresholdMax = 136,
                IsDoubleOptIn = true,
                UomId = 12,
                Type = "",
                IsLatest = true
            };
            var subscriptionModelList = new List<SubscriptionModel> { subscriptionModel };

            var notificationModel = new NotificationModel
            {
                ClientId = 1,
                AccountId = "AC0001",
                ServiceContractId = "",
                ProgramName = "testProgram",
                Insight = "BillToDate",
                IsNotified = false,
                IsDelivered = false,
                IsDropped = false,
                IsBounced = false,
                IsOpened = false,
                IsClicked = false,
                IsScheduled = false,
                Channel = "SMS",
                TemplateId = "",
                CustomerId = "1",
                EvaluationType = "",
                NotifiedDateTime = DateTime.Now,
                TrumpiaRequestDetailId = "",
                IsLatest = true
            };

            var customerModel = new CustomerModel
            {
                ClientId = 1,
                CustomerId = "1234",
                Country = "USA",
                Source = "",
                IsBusiness = false,
                AddressLine1 = "C Block",
                AddressLine2 = "Flat no B-804",
                AddressLine3 = "Wakad",
                City = "Pune",
                State = "New york",
                PostalCode = "NY001",
                FirstName = "Sharad",
                LastName = "Saini",
                Phone1 = "9911559988",
                Phone2 = "8975757012",
                //EmailAddress = "sharad.saini@saviantconsulting.com",
                EmailAddress = "chackett@aclara.com",
                CustomerType = "Good",
                TrumpiaSubscriptionId = "",
                TrumpiaRequestDetailId = ""

            };

            string customKey = "";
            string smsApiKey = "";
            string smsUserName = "";
            string smsBody = "";
            var insightModel = new InsightModel
            {
                ClientId = 1,
                AccountId = "1234",
                ServiceContractId = "",
                BilllDays = 23,
                BtdCost = 2332,
                BtdProjectedCost = 3544,
                Usage = 32,
                SupressInsight = true,
                AccountLevelCostThresholdExceed = true,
                ServiceLevelUsageThresholdExceed = true,
                ServiceLevelCostThresholdExceed = true,
                DayThresholdExceed = true,
                ServiceLevelTiered1ThresholdApproaching = true,
                ServiceLevelTieredThresholdExceed = true,
                ServiceDays = 32,
                CtdCost = 4545,
                CtdProjectedCost = 5678,
                NotifyTime = DateTime.Now.ToShortDateString(),
                ProgramName = "testprogram",
                AsOfEvaluationDate = DateTime.Now,
				AsOfEvaluationId = Guid.NewGuid(),
                BillCycleStartDate = "12 12",
                BillCycleEndDate = " 12 12 12",
                Type = "ABC",
                IsLatest = true
            };

            string programName = "";
            string insightTypeName = "";
            var insight = new InsightSettings
            {
                InsightName = insightTypeName
            };
            DateTime requestDate = DateTime.Now;
            DateTime? notifyDateTime = DateTime.Now;

            var result = _notificationManager.SendSms(billingModel, subscriptionModelList, notificationModel, customerModel,
                customKey, smsApiKey, smsUserName, smsBody, insightModel, programName, insight, requestDate,
                "my tier", notifyDateTime);

            Assert.AreEqual(true, result.IsCompleted);
            MockRepository.VerifyAll();
        }

        /// <summary>
        /// Test method to get SMS report
        /// </summary>
        [TestMethod]
        [Ignore]
        public void GetSmsReport()
        {
            var billingModel = new BillingModel
            {
                ClientId = 1,
                CustomerId = "1234",
                AccountId = "AC00012",
                PremiseId = "12",
                AmiStartDate = DateTime.Now,
                AmiEndDate = DateTime.Now,
                BillCycleScheduleId = "BS12122",
                RateClass = "Rate Class",
                ServicePointId = "SP0012",
                Source = "Rate Class",
                MeterType = "",
                UOMId = 12,
                TotalUsage = 1232323,
                StartDate = DateTime.Now,
                EndDate = DateTime.Today.AddDays(12),
                BillDays = 12,
                ReadQuality = "",
                TotalCost = 122323,
                CommodityId = 2,
                MeterId = " ",
                BillPeriodType = 2,
                ReplacedMeterId = "",
                ReadDate = DateTime.Now,
                ServiceContractId = "SC001",
                IsFault = false,
                IsLatest = true
            };
            var subscriptionModel = new SubscriptionModel
            {
                ClientId = 1,
                CustomerId = "1234",
                AccountId = "AC0001",
                ProgramName = "testprogram",
                InsightTypeName = "",
                PremiseId = "",
                ServiceContractId = "SC001",
                ServicePointId = "",
                IsEmailOptInCompleted = false,
                IsSmsOptInCompleted = true,
                IsSelected = true,
                Channel = "SMS",
                AllowedChannels = "",
                SubscriberThresholdValue = 1234,
                CommodityKey = "Water",
                ThresholdType = "",
                ThresholdMin = 132,
                ThresholdMax = 136,
                IsDoubleOptIn = true,
                UomId = 12,
                Type = "",
                IsLatest = true
            };
            var subscriptionModelList = new List<SubscriptionModel> { subscriptionModel };

            var insightModel = new InsightModel
            {
                ClientId = 1,
                AccountId = "AC0001",
                ServiceContractId = "",
                ProgramName = "testProgram",
                CustomerId = "1",
                IsLatest = true,
                AccountLevelCostThresholdExceed = true,
                AsOfEvaluationDate = DateTime.Now,
                AsOfEvaluationId = new Guid(),
                BillCycleEndDate = DateTime.Today.AddDays(12).ToShortDateString(),
                Type = "A"
            };

            var notificationModel = new NotificationModel
            {
                ClientId = 1,
                AccountId = "AC0001",
                ServiceContractId = "",
                ProgramName = "testProgram",
                Insight = "BillToDate",
                IsNotified = false,
                IsDelivered = false,
                IsDropped = false,
                IsBounced = false,
                IsOpened = false,
                IsClicked = false,
                IsScheduled = false,
                Channel = "SMS",
                TemplateId = "",
                CustomerId = "1",
                EvaluationType = "",
                NotifiedDateTime = DateTime.Now,
                TrumpiaRequestDetailId = "",
                IsLatest = true,
                TrumpiaRequestDetailPkRk = "123"
            };

            var customerModel = new CustomerModel
            {
                ClientId = 1,
                CustomerId = "1234",
                Country = "USA",
                Source = "",
                IsBusiness = false,
                AddressLine1 = "C Block",
                AddressLine2 = "Flat no B-804",
                AddressLine3 = "Wakad",
                City = "Pune",
                State = "New york",
                PostalCode = "NY001",
                FirstName = "Sharad",
                LastName = "Saini",
                Phone1 = "9911559988",
                Phone2 = "8975757012",
                EmailAddress = "sharad.saini@saviantconsulting.com",
                CustomerType = "Good",
                TrumpiaSubscriptionId = "",
                TrumpiaRequestDetailId = ""

            };

            var trumpiaRequestDetailModel = new TrumpiaRequestDetailModel
            {
                MessageId = "1",
                RequestId = "1",
                Description = "Test Description",
                StatusCode = "1",
                ErrorMessage = " ",
                SmsBody = "Hello! This is trumpia."
            };

            _subscriptionManagerMock.Setup(t => t.InsertorMergeForSmsOptOut(It.IsAny<BillingModel>(),
                It.IsAny<List<SubscriptionModel>>(), It.IsAny<int>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(),
                It.IsAny<string>(), It.IsAny<string>())).Returns(true).Verifiable();
            
            _trumpiaManagerMock.Setup(t => t.GetTrumpiaRequestDetailAsync(It.IsAny<string>())).Returns(Task.FromResult(trumpiaRequestDetailModel)).Verifiable();

            var result = _notificationManager.GetSmsReport(billingModel, subscriptionModelList, notificationModel, insightModel, customerModel, "", "testapikey", "testusername", DateTime.Now);

            _trumpiaManagerMock.Verify(t => t.GetTrumpiaRequestDetailAsync(It.IsAny<string>()), Times.Once);
            _trumpiaManagerMock.VerifyAll();

            Assert.AreEqual(true, result.IsCompleted);
            MockRepository.VerifyAll();
        }

        [TestMethod]
        public void DeleteNotification()
        {
            // Arrange
            CassandraRepositoryMock.Setup(t => t.DeleteAsync(It.IsAny<string>(), It.IsAny<Expression<Func<NotificationEntity, bool>>>())).Returns(Task.FromResult(typeof(void)));

            // Act
            _notificationManager.DeleteNotificationAsync(87, "1234", "1234").Wait();

            // Assert
            MockRepository.VerifyAll();
        }
    }
}
