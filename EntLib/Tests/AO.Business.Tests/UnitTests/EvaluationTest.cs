﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using AO.DataAccess.Contracts;
using AO.Entities;
using CE.AO.Models;
using CE.AO.Utilities;
using CE.RateModel;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Enums = CE.RateModel.Enums;

namespace AO.Business.Tests.UnitTests
{
    /// <summary>
    /// Test cases for Evaluation class
    /// </summary>
    [TestClass]
    public class EvaluationTest : TestBase
    {
        private Evaluation _evalationManager;
        
        /// <summary>
        /// Test Case Init
        /// </summary>
        [TestInitialize]
        public void Init()
        {
            _evalationManager = new Evaluation(new LogModel(),
                new Lazy<ICassandraRepository>(() => CassandraRepositoryMock.Object),
                new Lazy<IQueueManager>(() => new Mock<IQueueManager>().Object));
        }

        private void SetupMockMethods()
        {
            CassandraRepositoryMock.Setup(
                t => t.GetSingleAsync(It.IsAny<string>(), It.IsAny<Expression<Func<InsightEntity, bool>>>()))
                .Returns(Task.FromResult(GetInsightEntity("", "")));
            CassandraRepositoryMock.Setup(
                t => t.DeleteAsync(It.IsAny<string>(), It.IsAny<Expression<Func<InsightEntity, bool>>>()))
                .Returns(Task.FromResult(typeof(void)));

        }

        /// <summary>
        /// To evaluate insight, to save insight using subscription, calculation, ami, billing model
        /// </summary>
        [TestMethod]
        public void EvaluateInsight()
        {
            var listSubscriptionModels = GetSubscriptionModel();
            var billingModel = GetBillingModel();

            var listAmi = new List<TallAmiModel> { GetAmi15MinuteIntervalModel() };
            var tier = new List<TierBoundary>
            {
                new TierBoundary{
                    BaseOrTier = Enums.BaseOrTier.Tier1,
                    DaysIntoSeason = 180,
                    Season = Enums.Season.Winter,
                    Threshold = 223,
                    SecondaryThreshold = 150
                }
            };

            //Account level
            var accountLevelCalculationModels = GetAccountLevelCalculationModel();
            var accountLevelClientSettings = GetAccountLevelClientSettings();

            SetupMockMethods();

            _evalationManager.EvaluateInsight(listSubscriptionModels, accountLevelCalculationModels,
                accountLevelClientSettings, listAmi, tier, DateTime.Now, billingModel).Wait();

            //Service level
            var serviceLevelCalculationModels = GetServiceLevelCalculationModel();
            var serviceLevelClientSettings = GetServiceLevelClientSettings();
            _evalationManager.EvaluateInsight(listSubscriptionModels, serviceLevelCalculationModels,
                serviceLevelClientSettings, listAmi, tier, DateTime.Now, billingModel).Wait();

            Assert.AreEqual(true, true);
        }

        /// <summary>
        /// To fetch service level evaluation from database
        /// </summary>
        [TestMethod]
        public void GetServiceLevelEvaluationsAsync()
        {
            // Arrange
            var insightEntity = GetInsightEntity("SC05", "S");
            CassandraRepositoryMock.Setup(
                t =>
                    t.GetSingleAsync(It.IsAny<string>(), It.IsAny<Expression<Func<InsightEntity, bool>>>()))
                .Returns(Task.FromResult(insightEntity))
                .Verifiable();

            // Act
            var result = _evalationManager.GetServiceLevelEvaluationsAsync(87, "Cust05", "Accn05", "Program1", "SC05").Result;

            // Assert
            CassandraRepositoryMock.Verify(t => t.GetSingleAsync(It.IsAny<string>(), It.IsAny<Expression<Func<InsightEntity, bool>>>()), Times.Once);
            CassandraRepositoryMock.VerifyAll();
            Assert.AreEqual(result.IsLatest, true);
        }

        /// <summary>
        /// To fetch account level evaluation from database
        /// </summary>
        [TestMethod]
        public void GetAccountLevelEvaluationsAsync()
        {
            // Arrange
            var insightEntity = GetInsightEntity("", "A");
            CassandraRepositoryMock.Setup(
                t =>
                    t.GetSingleAsync(It.IsAny<string>(), It.IsAny<Expression<Func<InsightEntity, bool>>>()))
                .Returns(Task.FromResult(insightEntity))
                .Verifiable();

            // Act
            var result = _evalationManager.GetAccountLevelEvaluationsAsync(87, "Cust05", "Accn05", "Program1").Result;
            
            // Assert
            CassandraRepositoryMock.Verify(t => t.GetSingleAsync(It.IsAny<string>(), It.IsAny<Expression<Func<InsightEntity, bool>>>()), Times.Once);
            CassandraRepositoryMock.VerifyAll();
            Assert.AreEqual(result.IsLatest, true);
        }

        /// <summary>
        /// To fetch list of all InsightModel model in a day
        /// </summary>
        [TestMethod]
        public void GetDayInsights()
        {
            // Arrange
            var insightEntity = GetInsightEntity("SC05", "S");
            IList<InsightEntity> list = new List<InsightEntity>();
            list.Add(insightEntity);
            CassandraRepositoryMock.Setup(
                t =>
                    t.GetAsync(It.IsAny<string>(), It.IsAny<Expression<Func<InsightEntity, bool>>>()))
                .Returns(Task.FromResult(list))
                .Verifiable();

            // Act
            var result = _evalationManager.GetDayInsights(87, "Cust05", "Accn05", DateTime.Now).FirstOrDefault();

            // Assert
            CassandraRepositoryMock.Verify(t => t.GetAsync(It.IsAny<string>(), It.IsAny<Expression<Func<InsightEntity, bool>>>()), Times.Once);
            CassandraRepositoryMock.VerifyAll();
            if (result != null) Assert.AreEqual(result.Type, "S");
        }

        /// <summary>
        /// Create and returns new InsightEntity
        /// </summary>
        /// <returns>InsightEntity</returns>
        private static InsightEntity GetInsightEntity(string serviceContractId, string type)
        {
            return new InsightEntity
            {
                ClientId = 87,
                AccountId = "Accn05",
                ServiceContractId = serviceContractId,
                ProgramName = "Program1",
                AsOfEvaluationDate = DateTime.Now,
				AsOfEvaluationId = Guid.NewGuid(),
                BillCycleStartDate = DateTime.Now.AddDays(-30).ToShortDateString(),
                BillCycleEndDate = DateTime.Now.ToShortDateString(),
                Type = type,
                IsLatest = true
            };
        }

        /// <summary>
        /// Create and returns new BillingModel
        /// </summary>
        /// <returns>BillingModel</returns>
        private static BillingModel GetBillingModel()
        {
            return new BillingModel
            {
                ClientId = 87,
                CustomerId = "C05",
                AccountId = "Accn05",
                PremiseId = "P05",
                CommodityId = 3,
                MeterId = "M05",
                ServiceContractId = "SC05",
                StartDate = DateTime.Now.AddDays(-1),
                EndDate = DateTime.Now,
                TotalCost = 2093,
                BillDays = 30,
                Source = "Utility",
                IsLatest = true
            };
        }

        /// <summary>
        /// Create and returns new SubscriptionModel
        /// </summary>
        /// <returns>List of SubscriptionModel</returns>
        private static List<SubscriptionModel> GetSubscriptionModel()
        {
            return new List<SubscriptionModel>
            {
                new SubscriptionModel
                {
                    ClientId = 87,
                    CustomerId = "C05",
                    AccountId = "Accn05",
                    PremiseId = "P05",
                    ProgramName = "Program1",
                    InsightTypeName = "BillToDate",
                    ServiceContractId = "",
                    IsSelected = true,
                    Channel = "Email",
                    IsEmailOptInCompleted = true,
                    UomId = 3,
                    Type = "A",
                    IsLatest = true
                },
                new SubscriptionModel
                {
                    ClientId = 87,
                    CustomerId = "C05",
                    AccountId = "Accn05",
                    PremiseId = "P05",
                    ProgramName = "Program1",
                    InsightTypeName = "ServiceLevelUsageThreshold",
                    ServiceContractId = "SC05",
                    IsSelected = true,
                    Channel = "Email",
                    IsEmailOptInCompleted = true,
                    UomId = 3,
                    Type = "S",
                    IsLatest = true
                },
                new SubscriptionModel
                {
                    ClientId = 87,
                    CustomerId = "C05",
                    AccountId = "Accn05",
                    PremiseId = "P05",
                    ProgramName = "Program1",
                    InsightTypeName = "DayThreshold",
                    ServiceContractId = "SC05",
                    IsSelected = true,
                    Channel = "Email",
                    IsEmailOptInCompleted = true,
                    UomId = 3,
                    Type = "S",
                    IsLatest = true
                },
                new SubscriptionModel
                {
                    ClientId = 87,
                    CustomerId = "C05",
                    AccountId = "Accn05",
                    PremiseId = "P05",
                    ProgramName = "Program1",
                    InsightTypeName = "ServiceLevelTieredThresholdApproaching",
                    ServiceContractId = "SC05",
                    IsSelected = true,
                    Channel = "Email",
                    IsEmailOptInCompleted = true,
                    UomId = 3,
                    Type = "S",
                    IsLatest = true
                },
                new SubscriptionModel
                {
                    ClientId = 87,
                    CustomerId = "C05",
                    AccountId = "Accn05",
                    PremiseId = "P05",
                    ProgramName = "Program1",
                    InsightTypeName = "ServiceLevelTieredThresholdExceed",
                    ServiceContractId = "SC05",
                    IsSelected = true,
                    Channel = "Email",
                    IsEmailOptInCompleted = true,
                    UomId = 3,
                    Type = "S",
                    IsLatest = true
                }
            };
        }

        /// <summary>
        /// Create and returns account level new CalculationModel list
        /// </summary>
        /// <returns>List of CalculationModel</returns>
        private static List<CalculationModel> GetAccountLevelCalculationModel()
        {
            return new List<CalculationModel>
            {
                new CalculationModel
                {
                    ClientId = 87,
                    AccountId = "Accn05",
                    CustomerId = "C05",
                    MeterId = "M05",
                    ServiceContractId = "",
                    CommodityId = 3,
                    Unit = 78,
                    Cost = 105.245,
                    ProjectedCost = 3333,
                    ProjectedUsage = 222,
                    Usage = 22,
                    BillDays = 67,
                    BillCycleStartDate = DateTime.Now.ToString(CultureInfo.InvariantCulture),
                    BillCycleEndDate = DateTime.Now.ToString(CultureInfo.InvariantCulture),
                    AsOfAmiDate = DateTime.Now,
                    AsOfCalculationDate = DateTime.Now,
                    RateClass = "C",
                    AverageDailyUsage = 6785,
                    AverageDailyCost = 98,
                    Type = "X",
                    IsLatest = true
                }
            };
        }

        /// <summary>
        /// Create and returns service level new CalculationModel list
        /// </summary>
        /// <returns>List of CalculationModel</returns>
        private static List<CalculationModel> GetServiceLevelCalculationModel()
        {
            return new List<CalculationModel>
            {
                new CalculationModel
                {
                    ClientId = 87,
                    AccountId = "Accn05",
                    CustomerId = "C05",
                    MeterId = "M05",
                    ServiceContractId = "SC05",
                    CommodityId = 3,
                    Unit = 78,
                    Cost = 105.245,
                    ProjectedCost = 3333,
                    ProjectedUsage = 222,
                    Usage = 22,
                    BillDays = 67,
                    BillCycleStartDate = DateTime.Now.ToString(CultureInfo.InvariantCulture),
                    BillCycleEndDate = DateTime.Now.ToString(CultureInfo.InvariantCulture),
                    AsOfAmiDate = DateTime.Now,
                    AsOfCalculationDate = DateTime.Now,
                    RateClass = "C",
                    AverageDailyUsage = 6785,
                    AverageDailyCost = 98,
                    Type = "X",
                    IsLatest = true
                }
            };
        }

        /// <summary>
        /// Create and returns new account level ClientSettings
        /// </summary>
        /// <returns>ClientSettings</returns>
        private static ClientSettings GetAccountLevelClientSettings()
        {
            return new ClientSettings
            {
                ClientId = 2,
                MinimumDaysForInsightNotification = 20,
                TimeZone = "EST",
                OptInEmailUserName = "AclaraMdm",
                OptInEmailPassword = "AclaraMdm@123",
                OptInEmailFrom = "Support@aclara.com",
                Programs = new List<ProgramSettings>
                {
                    new ProgramSettings
                    {
                        ProgramName = "Program1",
                        UtilityProgramName = "Program1",
                        DoubleOptInRequired = true,
                        Insights = new List<InsightSettings>
                        {
                            new InsightSettings
                            {
                                InsightName = "BillToDate",
                                UtilityInsightName = "BillToDate",
                                AllowedCommunicationChannel = "Email",
                                DefaultCommunicationChannel = "Email",
                                CommodityType = "Water",
                                EmailTemplateId = "sd120d025",
                                DefaultValue = "210",
                                Level = "Account"
                            },
                            new InsightSettings
                            {
                                InsightName = "AccountProjectedCost",
                                UtilityInsightName = "AccountProjectedCost",
                                AllowedCommunicationChannel = "Email",
                                DefaultCommunicationChannel = "Email",
                                CommodityType = "Water",
                                EmailTemplateId = "sd120d026",
                                DefaultValue = "165",
                                Level = "Account"
                            },
                            new InsightSettings
                            {
                                InsightName = "AccountLevelCostThreshold",
                                UtilityInsightName = "AccountLevelCostThreshold",
                                AllowedCommunicationChannel = "Email",
                                DefaultCommunicationChannel = "Email",
                                CommodityType = "Water",
                                EmailTemplateId = "sd120d027",
                                DefaultValue = "198",
                                Level = "Account"
                            }
                        }
                    }
                }
            };
        }

        /// <summary>
        /// Create and returns new service level ClientSettings
        /// </summary>
        /// <returns>ClientSettings</returns>
        private static ClientSettings GetServiceLevelClientSettings()
        {
            return new ClientSettings
            {
                ClientId = 2,
                MinimumDaysForInsightNotification = 20,
                TimeZone = "EST",
                OptInEmailUserName = "AclaraMdm",
                OptInEmailPassword = "AclaraMdm@123",
                OptInEmailFrom = "Support@aclara.com",
                Programs = new List<ProgramSettings>
                {
                    new ProgramSettings
                    {
                         ProgramName = "Program1",
                         UtilityProgramName = "Program1",
                         DoubleOptInRequired = true,
                         Insights = new List<InsightSettings>
                         {
                            new InsightSettings
                            {
                                InsightName = "CostToDate",
                                UtilityInsightName = "CostToDate",
                                AllowedCommunicationChannel = "Email",
                                DefaultCommunicationChannel = "Email",
                                CommodityType = "Water",
                                EmailTemplateId = "sd120d028",
                                DefaultValue = "210",
                                Level = "Service"
                            },
                            new InsightSettings
                            {
                                InsightName = "ServiceProjectedCost",
                                UtilityInsightName = "ServiceProjectedCost",
                                AllowedCommunicationChannel = "Email",
                                DefaultCommunicationChannel = "Email",
                                CommodityType = "Water",
                                EmailTemplateId = "sd120d029",
                                DefaultValue = "165",
                                Level = "Service"
                            },
                            new InsightSettings
                            {
                                InsightName = "Usage",
                                UtilityInsightName = "Usage",
                                AllowedCommunicationChannel = "Email",
                                DefaultCommunicationChannel = "Email",
                                CommodityType = "Water",
                                EmailTemplateId = "sd120d030",
                                DefaultValue = "198",
                                Level = "Service"
                            },
                            new InsightSettings
                            {
                                InsightName = "ServiceLevelUsageThreshold",
                                UtilityInsightName = "ServiceLevelUsageThreshold",
                                AllowedCommunicationChannel = "Email",
                                DefaultCommunicationChannel = "Email",
                                CommodityType = "Water",
                                EmailTemplateId = "sd120d031",
                                DefaultValue = "250",
                                Level = "Service"
                            },
                            new InsightSettings
                            {
                                InsightName = "DayThreshold",
                                UtilityInsightName = "DayThreshold",
                                AllowedCommunicationChannel = "Email",
                                DefaultCommunicationChannel = "Email",
                                CommodityType = "Water",
                                EmailTemplateId = "sd120d032",
                                DefaultValue = "150",
                                Level = "Service"
                            },
                            new InsightSettings
                            {
                                InsightName = "ServiceLevelTieredThresholdApproaching",
                                UtilityInsightName = "ServiceLevelTieredThresholdApproaching",
                                AllowedCommunicationChannel = "Email",
                                DefaultCommunicationChannel = "Email",
                                CommodityType = "Water",
                                EmailTemplateId = "sd120d033",
                                DefaultValue = "177",
                                Level = "Service"
                            },
                            new InsightSettings
                            {
                                InsightName = "ServiceLevelTieredThresholdExceed",
                                UtilityInsightName = "ServiceLevelTieredThresholdExceed",
                                AllowedCommunicationChannel = "Email",
                                DefaultCommunicationChannel = "Email",
                                CommodityType = "Water",
                                EmailTemplateId = "sd120d034",
                                DefaultValue = "150",
                                Level = "Service"
                            },
                            new InsightSettings
                            {
                                InsightName = "ServiceLevelCostThreshold",
                                UtilityInsightName = "ServiceLevelCostThreshold",
                                AllowedCommunicationChannel = "Email",
                                DefaultCommunicationChannel = "Email",
                                CommodityType = "Water",
                                EmailTemplateId = "sd120d035",
                                DefaultValue = "130",
                                Level = "Service"
                            }
                         }
                    }
                }
            };
        }

        /// <summary>
        /// Create and returns new Ami15MinuteIntervalModel
        /// </summary>
        /// <returns>Ami15MinuteIntervalModel</returns>
        private static TallAmiModel GetAmi15MinuteIntervalModel()
        {
            return new TallAmiModel
            {
                CustomerId = "23",
                ClientId = 87,
                MeterId = "M05",
                ServicePointId = "SP23",
                Timezone = "EST",
                CommodityId = 3,
                UOMId = 3,
                AccountNumber = "ACC500",
                AmiTimeStamp = DateTime.Now.Date,
                BatteryVoltage = "23V",
                TransponderPort = "P",
                TransponderId = "P278",
                IntervalType = 15,
                Consumption = 234.45,
                DialRead = "23RT",
                Direction = 2,
                ProjectedReadDate = DateTime.Now.Date,
                QualityCode = "QA23",
                ReadingType = "RD",
                Scenario = "",
                TOUID = 989,
                TOU_CriticalPeak = 22.2,
                TOU_OffPeak = 13.2,
                TOU_OnPeak = 11.2,
                TOU_Regular = 21,
                TOU_Shoulder1 = 20,
                TOU_Shoulder2 = 21.5,
                VolumeFactor = 15
            };
        }
    }
}
