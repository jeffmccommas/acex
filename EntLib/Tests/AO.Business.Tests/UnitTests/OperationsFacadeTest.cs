﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using AO.DataAccess.Contracts;
using AO.DTO;
using AO.Entities;
using CE.AO.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace AO.Business.Tests.UnitTests
{
    /// <summary>
    /// Test Class for Log
    /// </summary>
    [TestClass]
    public class OperationsFacadeTest : TestBase
    {
        private OperationsFacade _operationsFacade;
        private Mock<ILogRepository> LogRepositoryMock;
        /// <summary>
        /// Test Case Init
        /// </summary>
        [TestInitialize]
        public void Init()
        {
            LogRepositoryMock = new Mock<ILogRepository>();
            _operationsFacade = new OperationsFacade(LogModelMock.Object, new Lazy<ILogRepository>(() => LogRepositoryMock.Object));
        }

        /// <summary>
        /// Test case for InsertOrMergeLogAsync method in Log class
        /// </summary>
        [TestMethod]
        public void InsertOrMergeLogAsync()
        {
            LogRepositoryMock.Setup(
                t => t.InsertOrMergeLogAsync(It.IsAny<LogEntity>()))
                .Returns(Task.FromResult(true));
            var result = _operationsFacade.InsertOrMergeLogAsync(new LogViewModel()).Result;

            LogRepositoryMock.Verify(t => t.InsertOrMergeLogAsync(It.IsAny<LogEntity>()), Times.Once);
            Assert.IsTrue(result);
        }

        /// <summary>
        /// Test case for method GetLogView from LogView class
        /// </summary>
        [TestMethod]
        public void GetLogView()
        {
            var logEntity= new LogEntity
            {
                ClientId = "87",
                TimestampUtc = DateTime.UtcNow
            };

            LogRepositoryMock.Setup(
                t => t.GetLogHistory(It.IsAny<LogHistoryRequestDTO>())).Returns(new List<LogEntity> { logEntity }).Verifiable();

            var result = _operationsFacade.GetLogHistory(new LogHistoryRequestDTO());

            LogRepositoryMock.Verify(
                t => t.GetLogHistory(It.IsAny<LogHistoryRequestDTO>()));

            MockRepository.VerifyAll();
            Assert.AreEqual(result[0].ClientId, logEntity.ClientId);
        }
    }
}