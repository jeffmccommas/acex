﻿using System;
using System.Linq.Expressions;
using System.Threading.Tasks;
using AO.BusinessContracts;
using AO.DataAccess.Contracts;
using AO.Entities;
using CE.AO.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace AO.Business.Tests.UnitTests
{
    /// <summary>
    /// Test cases for AccountUpdates class
    /// </summary>
    [TestClass]
    public class AccountUpdatesTest : TestBase
    {
        private IAccountUpdates _accountUpdates;

        /// <summary>
        /// Test Case Init
        /// </summary>
        [TestInitialize]
        public void Init()
        {
            _accountUpdates = new AccountUpdates(LogModelMock.Object, new Lazy<ICassandraRepository>(()=>CassandraRepositoryMock.Object));
        }

        /// <summary>
        /// Test Case for accessing Log Model
        /// </summary>
        [TestMethod]
        public void AccessLogModel()
        {
            _accountUpdates.LogModel.DisableLog = true;
            Assert.AreEqual(true, _accountUpdates.LogModel.DisableLog);
        }

        /// <summary>
        /// Test case for insert of AccountUpdateModel model
        /// Test case for InsertOrMergeAccountUpdateAsync method in AccountUpdates class
        /// </summary>
        [TestMethod]
        public void InsertOrMergeAccountUpdateAsync()
        {
            var accountUpdatesModel = new AccountUpdatesModel()
            {
                ClientId = 87,
                CustomerId = "",
                AccountId = "",
                PremiseId = "",
                MeterId = "METER_QA_02",
                NewValue = "",
                OldValue = "",
                ServiceContractId = ""
            };
            CassandraRepositoryMock.Setup(t => t.InsertOrUpdateAsync(It.IsAny<AccountUpdatesEntity>(), It.IsAny<string>())).Returns(Task.FromResult(true)).Verifiable();

            var result = _accountUpdates.InsertOrMergeAccountUpdateAsync(accountUpdatesModel,false).Result;

            CassandraRepositoryMock.Verify(t => t.InsertOrUpdateAsync(It.IsAny<AccountUpdatesEntity>(), It.IsAny<string>()), Times.Once);
            Assert.AreEqual(true, result);
            MockRepository.VerifyAll();
        }

        /// <summary>
        /// Test case to fetch AccountUpdateEntity
        /// Test case for GetAccountUpdate method in AccountUpdates class
        /// </summary>
        [TestMethod]
        public void GetAccountUpdate()
        {
            var accountUpdatesEntity = new AccountUpdatesEntity()
            {
                ClientId = 87,
                CustomerId = "",
                AccountId = "",
                PremiseId = "",
                MeterId = "METER_QA_02",
                NewValue = "",
                OldValue = "",
                ServiceContractId = "",
                IsLatest = true
            };
            CassandraRepositoryMock.Setup(
                t => t.GetSingleAsync(It.IsAny<string>(), It.IsAny<Expression<Func<AccountUpdatesEntity, bool>>>()))
                .Returns(Task.FromResult(accountUpdatesEntity));

            //_accountUpdates.CassandraRepository = new Lazy<ICassandraRepository>(() => _cassandraRepositoryMock.Object);

            var result = _accountUpdates.GetAccountUpdate(87, "", "", "", true).Result;
            CassandraRepositoryMock.Verify(t => t.GetSingleAsync(It.IsAny<string>(),It.IsAny<Expression<Func<AccountUpdatesEntity,bool>>>()),Times.Once);
            Assert.AreEqual(result.ClientId,87);
            MockRepository.VerifyAll();
        }

        /// <summary>
        /// Test case to delete AccountUpdateEntity entity
        /// Test case for DeleteHistoryAccountUpdateAsyc method in AccountUpdates class
        /// </summary>
        [TestMethod]
        public void DeleteHistoryAccountUpdateAsyc()
        {
            CassandraRepositoryMock.Setup(
                t =>
                    t.DeleteAsync(It.IsAny<string>(),
                        It.IsAny<Expression<Func<AccountUpdatesEntity, bool>>>()))
                        .Returns(Task.FromResult(typeof(void)));
            //_accountUpdates.CassandraRepository = new Lazy<ICassandraRepository>(() => _cassandraRepositoryMock.Object);

            var result = _accountUpdates.DeleteHistoryAccountUpdateAsyc(87, "", "", "", true).Result;
            CassandraRepositoryMock.Verify(t => t.DeleteAsync(It.IsAny<string>(), It.IsAny<Expression<Func<AccountUpdatesEntity, bool>>>()), Times.Once);
            Assert.AreEqual(true,result);
            MockRepository.VerifyAll();
        }
    }
}
