﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using AO.DataAccess.Contracts;
using AO.Entities;
using CE.AO.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace AO.Business.Tests.UnitTests
{
    /// <summary>
    /// Test cases for ConsumptionByMeter class 
    /// </summary>
    [TestClass]
    public class ConsumptionByMeterTest : TestBase
    {
        private ConsumptionByMeter _consumptionByMeter;

        /// <summary>
        /// Test Case Init
        /// </summary>
        [TestInitialize]
        public void Init()
        {
            _consumptionByMeter = new ConsumptionByMeter(LogModelMock.Object, new Lazy<ICassandraRepository>(()=>CassandraRepositoryMock.Object));
        }

        /// <summary>
        /// Test case to insert TallAmiModel
        /// Test case for InsertOrMergeAmiAsync method for TallAMI class
        /// </summary>
        [TestMethod]
        public void InsertOrMergeAmiAsync()
        {
            // Arrange
            CassandraRepositoryMock.Setup(t => t.InsertOrUpdateAsync(It.IsAny<ConsumptionByMeterEntity>(), It.IsAny<string>()))
                .Returns(Task.FromResult(typeof(void))).Verifiable();
            TallAmiModel tallAmiModel = new TallAmiModel();

            // Act
            var result = _consumptionByMeter.InsertOrMergeAmiAsync(tallAmiModel).Result;

            // Assert
            CassandraRepositoryMock.Verify(t => t.InsertOrUpdateAsync(It.IsAny<ConsumptionByMeterEntity>(), It.IsAny<string>()), Times.Once);
            Assert.AreEqual(true, result);
            MockRepository.VerifyAll();
        }

        /// <summary>
        /// Test case to insert list of TallAmiModel model in batch
        /// Test case for InsertOrMergeBatchAsync method for TallAMI class
        /// </summary>
        [TestMethod]
        public void InsertOrMergeBatchAsync()
        {
            // Arrange
            List<TallAmiModel> tallAmiModels = new List<TallAmiModel>
            {
                new TallAmiModel(),
                new TallAmiModel()
            };
            CassandraRepositoryMock.Setup(t => t.InsertOrUpdateBatchAsync(It.IsAny<List<ConsumptionByMeterEntity>>(), It.IsAny<string>()))
                .Returns(Task.FromResult(typeof(void))).Verifiable();

            // Act
            var result = _consumptionByMeter.InsertOrMergeBatchAsync(tallAmiModels).Result;
            
            // Assert
            Assert.AreEqual(true, result);
            MockRepository.VerifyAll();
        }

        /// <summary>
        /// Test case for GetAmiList method for TallAMI class
        /// </summary>
        [TestMethod]
        public void GetAmiList()
        {
            // Arrange
            IList<ConsumptionByMeterEntity> listEntity = new List<ConsumptionByMeterEntity>()
            {
                new ConsumptionByMeterEntity
                {
                    ClientId = 87,
                    MeterId = "Met67",
                    AccountNumber = "Acct87"
                }
            };
            CassandraRepositoryMock.Setup(
                t => t.Get(It.IsAny<string>(), It.IsAny<char>(), It.IsAny<Expression<Func<ConsumptionByMeterEntity, bool>>>())).Returns(listEntity).Verifiable();

            // Act
            var result = _consumptionByMeter.GetAmiList(DateTime.Now, DateTime.Now, "", "Acct87", 87).FirstOrDefault();

            // Assert
            Assert.IsNotNull(result);
            Assert.AreEqual(result.MeterId, "Met67");
            MockRepository.VerifyAll();
        }

        /// <summary>
        /// Test case for GetAmi method for TallAMI class
        /// </summary>
        [TestMethod]
        public void GetAmi()
        {
            // Arrange
            IList<ConsumptionByMeterEntity> listEntity = new List<ConsumptionByMeterEntity>
            {
                new ConsumptionByMeterEntity
                {
                    ClientId = 87,
                    MeterId = "Met67",
                    AccountNumber = "Acct87"
                }
            };
            CassandraRepositoryMock.Setup(
                t => t.GetAsync(It.IsAny<string>(), It.IsAny<char>(), It.IsAny<Expression<Func<ConsumptionByMeterEntity, bool>>>())).Returns(Task.FromResult(listEntity)).Verifiable();

            // Act
            var result = _consumptionByMeter.GetAmi(DateTime.Now, "Met900", "Acct87", 87).FirstOrDefault();

            // Assert
            Assert.IsNotNull(result);
            Assert.AreEqual(result.MeterId, "Met67");
            MockRepository.VerifyAll();
        }

        [TestMethod]
        public void GetReadings_DaylightSaving_WithoutNoDays()
        {
            // Arrange
            var utcTime = Convert.ToDateTime("11/1/2016");
            var localTime = Convert.ToDateTime("10/31/2016");
            IList<TallAmiModel> listEntity = new List<TallAmiModel>
            {
                new TallAmiModel
                {
                ClientId = 87,
                MeterId = "Met67",
                AmiTimeStamp = utcTime.AddHours(1),
                Timezone = "EST"
            },
                new TallAmiModel
                {
                ClientId = 87,
                MeterId = "Met67",
                AmiTimeStamp = utcTime.AddHours(2),
                Timezone = "EST"
            },
               new TallAmiModel
               {
                ClientId = 87,
                MeterId = "Met67",
                AmiTimeStamp = utcTime.AddHours(3),
                Timezone = "EST"
            },
            };

            // Act
            var result = _consumptionByMeter.GetReadings(listEntity);

            // Assert
            Assert.AreEqual(result.Count, 3);
            var resultDesOrder = result.OrderByDescending(r => r.Timestamp).ToList();
            Assert.AreEqual(localTime.AddHours(23), resultDesOrder[0].Timestamp);
            Assert.AreEqual(localTime.AddHours(22), resultDesOrder[1].Timestamp);
            Assert.AreEqual(localTime.AddHours(21), resultDesOrder[2].Timestamp);
            MockRepository.VerifyAll();
        }

        [TestMethod]
        public void GetReadings_WithoutNoDays()
        {
            // Arrange
            var utcTime = Convert.ToDateTime("1/1/2017");
            var localTime = Convert.ToDateTime("12/31/2016");
            IList<TallAmiModel> listEntity = new List<TallAmiModel>
            {
                new TallAmiModel
                {
                ClientId = 87,
                MeterId = "Met67",
                AmiTimeStamp = utcTime.AddHours(1),
                Timezone = "EST"
            },
                new TallAmiModel
                {
                ClientId = 87,
                MeterId = "Met67",
                AmiTimeStamp = utcTime.AddHours(2),
                Timezone = "EST"
            },
               new TallAmiModel
               {
                ClientId = 87,
                MeterId = "Met67",
                AmiTimeStamp = utcTime.AddHours(3),
                Timezone = "EST"
            },
            };

            // Act
            var result = _consumptionByMeter.GetReadings(listEntity);

            // Assert
            Assert.AreEqual(result.Count, 3);
            var resultDesOrder = result.OrderByDescending(r => r.Timestamp).ToList();
            Assert.AreEqual(localTime.AddHours(22), resultDesOrder[0].Timestamp);
            Assert.AreEqual(localTime.AddHours(21), resultDesOrder[1].Timestamp);
            Assert.AreEqual(localTime.AddHours(20), resultDesOrder[2].Timestamp);
            MockRepository.VerifyAll();
        }

        /// <summary>
        /// Test case for GetReadings
        /// </summary>
        [TestMethod]
        public void GetReadings()
        {
            // Arrange
            var utcTime = Convert.ToDateTime("1/1/2017");
            var localTime = Convert.ToDateTime("12/31/2016");
            IList<TallAmiModel> listEntity = new List<TallAmiModel>
            {
                new TallAmiModel
                {
                ClientId = 87,
                MeterId = "Met67",
                AmiTimeStamp = utcTime.AddHours(1),
                Timezone = "EST"
            },
                new TallAmiModel
                {
                ClientId = 87,
                MeterId = "Met67",
                AmiTimeStamp = utcTime.AddHours(2),
                Timezone = "EST"
            },
               new TallAmiModel
               {
                ClientId = 87,
                MeterId = "Met67",
                AmiTimeStamp = utcTime.AddHours(3),
                Timezone = "EST"
            },
            };
            int noOfDays;

            // Act
            var result = _consumptionByMeter.GetReadings(listEntity, out noOfDays);

            // Assert
            Assert.AreEqual(result.Count, 3);
            var resultDesOrder = result.OrderByDescending(r => r.Timestamp).ToList();
            Assert.AreEqual(localTime.AddHours(22), resultDesOrder[0].Timestamp);
            Assert.AreEqual(localTime.AddHours(21), resultDesOrder[1].Timestamp);
            Assert.AreEqual(localTime.AddHours(20), resultDesOrder[2].Timestamp);
            MockRepository.VerifyAll();
        }


        ///// <summary>
        ///// Test case for AggregateDailyReadings
        ///// </summary>
        //[TestMethod]
        //public void AggregateDailyReadings_Test()
        //{
        //    // Arrange
        //    var utcTime = Convert.ToDateTime("1/1/2017");
        //    var localTime = Convert.ToDateTime("12/31/2016");
        //    List<TallAmiModel> listEntity = new List<TallAmiModel>
        //    {
        //        new TallAmiModel
        //        {
        //        ClientId = 87,
        //        MeterId = "Met67",
        //        AmiTimeStamp = utcTime.AddHours(1),
        //        Timezone = "EST",
        //        Consumption =  1
        //    },
        //        new TallAmiModel
        //        {
        //        ClientId = 87,
        //        MeterId = "Met67",
        //        AmiTimeStamp = utcTime.AddHours(2),
        //        Timezone = "EST",
        //        Consumption =  1
        //    },
        //       new TallAmiModel
        //       {
        //        ClientId = 87,
        //        MeterId = "Met67",
        //        AmiTimeStamp = utcTime.AddHours(3),
        //        Timezone = "EST",
        //        Consumption =  1
        //    },
        //       new TallAmiModel
        //       {
        //        ClientId = 87,
        //        MeterId = "Met67",
        //        AmiTimeStamp = utcTime.AddHours(4),
        //        Timezone = "EST",
        //        Consumption =  1
        //    },
        //       new TallAmiModel
        //       {
        //        ClientId = 87,
        //        MeterId = "Met67",
        //        AmiTimeStamp = utcTime.AddHours(5),
        //        Timezone = "EST",
        //        Consumption =  1
        //    },
        //       new TallAmiModel
        //       {
        //        ClientId = 87,
        //        MeterId = "Met67",
        //        AmiTimeStamp = utcTime.AddHours(6),
        //        Timezone = "EST",
        //        Consumption =  1
        //    },
        //       new TallAmiModel
        //       {
        //        ClientId = 87,
        //        MeterId = "Met67",
        //        AmiTimeStamp = utcTime.AddHours(7),
        //        Timezone = "EST",
        //        Consumption =  1
        //    },
        //       new TallAmiModel
        //       {
        //        ClientId = 87,
        //        MeterId = "Met67",
        //        AmiTimeStamp = utcTime.AddHours(8),
        //        Timezone = "EST",
        //        Consumption =  1
        //    }
        //    };

        //    // Act
        //    var result = _consumptionByMeter.AggregateDailyReadings(listEntity, 60);

        //    // Assert
        //    Assert.AreEqual(result.Count, 2);
        //    Assert.AreEqual(4, result[0].Quantity);
        //    Assert.AreEqual(4, result[1].Quantity);
        //    MockRepository.VerifyAll();
        //}

        ///// <summary>
        ///// Test case for AggregateDailyReadings
        ///// </summary>
        //[TestMethod]
        //public void AggregateMonthlyReadings_Test()
        //{
        //    // Arrange
        //    var utcTime = Convert.ToDateTime("1/1/2017");
        //    var localTime = Convert.ToDateTime("12/31/2016");
        //    List<TallAmiModel> listEntity = new List<TallAmiModel>
        //    {
        //        new TallAmiModel
        //        {
        //        ClientId = 87,
        //        MeterId = "Met67",
        //        AmiTimeStamp = utcTime.AddHours(1),
        //        Timezone = "EST",
        //        Consumption =  1
        //    },
        //        new TallAmiModel
        //        {
        //        ClientId = 87,
        //        MeterId = "Met67",
        //        AmiTimeStamp = utcTime.AddHours(2),
        //        Timezone = "EST",
        //        Consumption =  1
        //    },
        //       new TallAmiModel
        //       {
        //        ClientId = 87,
        //        MeterId = "Met67",
        //        AmiTimeStamp = utcTime.AddHours(3),
        //        Timezone = "EST",
        //        Consumption =  1
        //    },
        //       new TallAmiModel
        //       {
        //        ClientId = 87,
        //        MeterId = "Met67",
        //        AmiTimeStamp = utcTime.AddHours(4),
        //        Timezone = "EST",
        //        Consumption =  1
        //    },
        //       new TallAmiModel
        //       {
        //        ClientId = 87,
        //        MeterId = "Met67",
        //        AmiTimeStamp = utcTime.AddHours(5),
        //        Timezone = "EST",
        //        Consumption =  1
        //    },
        //       new TallAmiModel
        //       {
        //        ClientId = 87,
        //        MeterId = "Met67",
        //        AmiTimeStamp = utcTime.AddHours(6),
        //        Timezone = "EST",
        //        Consumption =  1
        //    },
        //       new TallAmiModel
        //       {
        //        ClientId = 87,
        //        MeterId = "Met67",
        //        AmiTimeStamp = utcTime.AddHours(7),
        //        Timezone = "EST",
        //        Consumption =  1
        //    },
        //       new TallAmiModel
        //       {
        //        ClientId = 87,
        //        MeterId = "Met67",
        //        AmiTimeStamp = utcTime.AddHours(8),
        //        Timezone = "EST",
        //        Consumption =  1
        //    }
        //    };

        //    // Act
        //    var result = _consumptionByMeter.AggregateMonthlyReadings(listEntity, 60);

        //    // Assert
        //    Assert.AreEqual(result.Count, 2);
        //    Assert.AreEqual(4, result[0].Quantity);
        //    Assert.AreEqual(4, result[1].Quantity);
        //    MockRepository.VerifyAll();
        //}
        /// <summary>
        /// Test case for GetReadings
        /// </summary>
        [TestMethod]
        public void GetReadings_NoOfReadings()
        {
            // Arrange
            int noOfDays;

            // Act
            var result = _consumptionByMeter.GetReadings(null, out noOfDays);

            // Assert
            Assert.AreEqual(result.Count, 0);
            MockRepository.VerifyAll();
        }

        [TestMethod]
        public void DeleteReadingAsync()
        {
            // Arrange
            CassandraRepositoryMock.Setup(
                t => t.DeleteAsync(It.IsAny<string>(), It.IsAny<Expression<Func<ConsumptionByMeterEntity, bool>>>()))
                .Returns(Task.FromResult(typeof(void))).Verifiable();

            // Act
            var result = _consumptionByMeter.DeleteReadingAsync(87, "Acc500").Result;

            // Assert
            CassandraRepositoryMock.Verify(t => t.DeleteAsync(It.IsAny<string>(), It.IsAny<Expression<Func<ConsumptionByMeterEntity, bool>>>()), Times.Once());
            Assert.AreEqual(result, true);
            MockRepository.VerifyAll();
        }

    }
}