﻿using AutoMapper;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace AO.Business.Tests.UnitTests
{
    [TestClass]
    public class AutoMapperUnitTest
    {
        [TestMethod]
        public void ConfigurationUnitTest()
        {
            Mapper.AssertConfigurationIsValid();
        }
    }
}
