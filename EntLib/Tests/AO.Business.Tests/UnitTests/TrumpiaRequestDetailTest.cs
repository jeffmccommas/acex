﻿using System;
using System.Linq.Expressions;
using System.Threading.Tasks;
using AO.Business.Tests.ObjectBuilders;
using AO.Entities;
using AutoMapper;
using CE.AO.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace AO.Business.Tests.UnitTests
{
    [TestClass]
    public class TrumpiaRequestDetailTest : TestBase
    {
        private TrumpiaRequestDetail _trumpiaRequestDetail;
        private readonly string _requestId = Guid.NewGuid().ToString();

        /// <summary>
        /// Test Case Init
        /// </summary>
        [TestInitialize]
        public void Init()
        {
            _trumpiaRequestDetail = new TrumpiaRequestDetail(CassandraRepositoryMock.Object, LogModelMock.Object);
        }

        /// <summary>
        /// Test case to get Trumpia request detail
        /// Test case for GetTrumpiaRequestDetail method in TrumpiaRequestDetail
        /// </summary>
        [TestMethod]
        public void GetTrumpiaRequestDetail()
        {
            // Arrange
            var trumpiaRequstDetailEntity = new TrumipaRequestDetailBuilder().WithRequestId(_requestId).Build();
            CassandraRepositoryMock.Setup(
                t => t.GetSingle(It.IsAny<string>(), It.IsAny<Expression<Func<TrumpiaRequestDetailEntity, bool>>>())).Returns(trumpiaRequstDetailEntity);

            // Act
            var result = _trumpiaRequestDetail.GetTrumpiaRequestDetail(trumpiaRequstDetailEntity.RequestId);

            // Assert
            CassandraRepositoryMock.Verify(t => t.GetSingle(It.IsAny<string>(), It.IsAny<Expression<Func<TrumpiaRequestDetailEntity, bool>>>()),Times.Once);
            Assert.AreEqual(result.RequestId, trumpiaRequstDetailEntity.RequestId);
        }

        /// <summary>
        /// Test case to get Trumpia request detail async
        /// Test case for GetTrumpiaRequestDetailAsync method in TrumpiaRequestDetail
        /// </summary>
        [TestMethod]
        public void GetTrumpiaRequestDetailAsync()
        {
            // Arrange
            var trumpiaRequstDetailEntity = new TrumipaRequestDetailBuilder().WithRequestId(_requestId).Build();
            CassandraRepositoryMock.Setup(
                t => t.GetSingleAsync(It.IsAny<string>(), It.IsAny<Expression<Func<TrumpiaRequestDetailEntity, bool>>>())).Returns(Task.FromResult(trumpiaRequstDetailEntity));

            // Act
            var result = _trumpiaRequestDetail.GetTrumpiaRequestDetailAsync(trumpiaRequstDetailEntity.RequestId).Result;

            // Assert
            CassandraRepositoryMock.Verify(t => t.GetSingleAsync(It.IsAny<string>(), It.IsAny<Expression<Func<TrumpiaRequestDetailEntity, bool>>>()), Times.Once);
            Assert.AreEqual(result.RequestId, trumpiaRequstDetailEntity.RequestId);
        }

        /// <summary>
        /// Test case for InsertOrMergeTrumpiaRequestDetail method in TrumpiaRequestDetail class
        /// </summary>
        [TestMethod]
        public void InsertOrMergeTrumpiaRequestDetail()
        {
            // Arrange
            var entity = new TrumipaRequestDetailBuilder().WithRequestId(_requestId).Build();
            var model = Mapper.Map<TrumpiaRequestDetailEntity, TrumpiaRequestDetailModel>(entity);
            CassandraRepositoryMock.Setup(
                t =>
                    t.InsertOrUpdate(It.Is<TrumpiaRequestDetailEntity>(x => x.LastModifiedDate > DateTime.MinValue),
                        It.IsAny<string>())).Returns(true).Verifiable();

            // Act
            _trumpiaRequestDetail.InsertOrMergeTrumpiaRequestDetail(model);

            // Assert
            MockRepository.VerifyAll();
        }

        /// <summary>
        /// Test case for InsertOrMergeTrumpiaRequestDetailAsync method in TrumpiaRequestDetail class
        /// </summary>
        [TestMethod]
        public void InsertOrMergeTrumpiaRequestDetailAsync()
        {
            // Arrange
            var entity = new TrumipaRequestDetailBuilder().WithRequestId(_requestId).Build();
            var model = Mapper.Map<TrumpiaRequestDetailEntity, TrumpiaRequestDetailModel>(entity);
            CassandraRepositoryMock.Setup(t => t.InsertOrUpdateAsync(It.IsAny<TrumpiaRequestDetailEntity>(), It.IsAny<string>())).Returns(Task.FromResult(true));

            // Act
            _trumpiaRequestDetail.InsertOrMergeTrumpiaRequestDetailAsync(model).Wait();

            // Assert
            CassandraRepositoryMock.Verify(t => t.InsertOrUpdateAsync(It.IsAny<TrumpiaRequestDetailEntity>(), It.IsAny<string>()), Times.Once);
            MockRepository.VerifyAll();
        }
    }
}
