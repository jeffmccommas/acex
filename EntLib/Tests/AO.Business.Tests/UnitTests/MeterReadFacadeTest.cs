﻿using System;
using System.Threading.Tasks;
using AO.BusinessContracts;
using CE.AO.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace AO.Business.Tests.UnitTests
{
    /// <summary>
    /// Test cases for MeterReadFacade class
    /// </summary>
    [TestClass]
    public class MeterReadFacadeTest : TestBase
    {
        private MeterReadFacade _meterReadFacade;

        private Mock<IConsumptionByAccount> _consumptionByAccountMock;
		private Mock<ITallAMI> _tallAmiMock;
        /// <summary>
        /// Test Case Init
        /// </summary>
        [TestInitialize]
        public void Init()
        {
            _consumptionByAccountMock = new Mock<IConsumptionByAccount>();
			_tallAmiMock = new Mock<ITallAMI>();
            
            _meterReadFacade = new MeterReadFacade(_consumptionByAccountMock.Object, _tallAmiMock.Object);
        }
        
        /// <summary>
        /// Test case for insert of MeterRead model
        /// Test case for InsertOrMergeMeterReadsAsync method in MeterReadFacade class
        /// </summary>
        [TestMethod]
        public void InsertOrMergeMeterReadsAsync()
        {
            var meterReadModel = new MeterReadModel
            {
                IntervalType = 60,
                AmiTimeStamp = DateTime.UtcNow,
                TransponderPort = "1",
                UOMId = 3,
                AccountNumber = "Mock Account",
                ClientId = 87,
                BatteryVoltage = "10",
                CommodityId = 1,
                MeterId = "Mock Meter",
                TransponderId = "Mock Transponder",
                Timezone = "PST",
                Consumption = 10,
                CustomerId = "Mock Customer"
            };
            _consumptionByAccountMock.Setup(
                t => t.InsertOrMergeConsumptionByAccountAsync(It.IsAny<ConsumptionByAccountModel>()))
                .Returns(Task.FromResult(true)).Verifiable();            
            
            _tallAmiMock.Setup(t => t.InsertOrMergeAmiAsync(It.IsAny<TallAmiModel>())).Returns(Task.FromResult(true)).Verifiable();
			
            var result = _meterReadFacade.InsertOrMergeMeterReadsAsync(meterReadModel).Result;
			MockRepository.VerifyAll();
			Assert.AreEqual(result, true);
        }
	}
}