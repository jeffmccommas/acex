﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Threading.Tasks;
using AO.BusinessContracts;
using AO.Entities;
using CE.AO.Models;
using CE.ContentModel.Entities;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Newtonsoft.Json;

namespace AO.Business.Tests.UnitTests
{
    /// <summary>
    /// Test Case Class for CassandraBusiness.Premise
    /// </summary>
    [TestClass]
    public class SubscriptionTest : TestBase
    {
        private Subscription _subscription;

        private Mock<IClientConfigFacade> _clientConfigFacadeMock;

        /// <summary>
        /// Test Case Init
        /// </summary>
        [TestInitialize]
        public void Init()
        {
            _clientConfigFacadeMock = new Mock<IClientConfigFacade>();
            _subscription = new Subscription(LogModelMock.Object, CassandraRepositoryMock.Object, new Lazy<IClientConfigFacade>(()=>_clientConfigFacadeMock.Object));
        }



        private static List<object> ListOfModelAndEntity()
        {
            List<object> getModelEntities = new List<object>()
            {
                new BillingModel()
                {
                    AccountId = "1",
                    CustomerId = "2",
                    ClientId = 3,
                    ServiceContractId = "asd",
                    ServicePointId = "23",
                    Source = "Meter",
                    PremiseId = "467",
                    IsLatest = true,
                    AmiEndDate = DateTime.Now,
                    AmiStartDate = DateTime.Now,
                    BillCycleScheduleId = "UYTH76",
                    BillDays = 23,
                    BillPeriodType = 35,
                    CommodityId = 2,
                    EndDate = DateTime.Now,
                    IsFault = true,
                    MeterId = "Id7",
                    MeterType = "T",
                    RateClass = "G",
                    ReadDate = DateTime.Now,
                    ReadQuality = "Yes",
                    ReplacedMeterId = "45t",
                    StartDate = DateTime.Now,
                    TotalCost = 245.5,
                    TotalUsage = 234.5,
                    UOMId = 2
                },
                new SubscriptionEntity()
                {
                    ClientId = 1,
                    CustomerId = "2",
                    AccountId = "3",
                    Channel = "SMS",
                    EmailConfirmationCount = 5,
                    EmailOptInCompletedDate = DateTime.Now,
                    InsightTypeName = "TYUI",
                    IsEmailOptInCompleted = true,
                    IsLatest = false,
                    IsSelected = true,
                    IsSmsOptInCompleted = true,
                    OptOutDate = DateTime.Now,
                    ProgramName = "TUYRHDF",
                    ServiceContractId = "RT67",
                    SmsConfirmationCount = 45,
                    SmsOptInCompletedDate = DateTime.Now,
                    SubscriberThresholdValue = 45.34,
                    SubscriptionDate = DateTime.Now,
                    UomId = 23
                },
                    new SubscriptionModel()
                {
                    ClientId = 87,
                    CustomerId = "2",
                    AccountId = "3",
                    AllowedChannels = "4",
                    Channel = "SMS",
                    CommodityKey = "RT4",
                    EmailConfirmationCount = 5,
                    EmailOptInCompletedDate = DateTime.Now,
                    InsightTypeName = "",
                    IsDoubleOptIn = true,
                    IsEmailOptInCompleted = true,
                    IsLatest = false,
                    IsSelected = true,
                    IsSmsOptInCompleted = true,
                    OptOutDate = DateTime.Now,
                    PremiseId = "67",
                    ProgramName = "Program1",
                    ServiceContractId = "RT67",
                    ServicePointId = "67i",
                    SmsConfirmationCount = 45,
                    SmsOptInCompletedDate = DateTime.Now,
                    SubscriberThresholdValue = 45.34,
                    SubscriptionDate = DateTime.Now,
                    ThresholdMax = 90.3,
                    ThresholdMin = 90.67,
                    ThresholdType = "QW",
                    Type = "ST",
                    UomId = 23
                }
            };
            return getModelEntities;
        }
        
        /// <summary>
        /// Test case for inserting Subscription Async
        /// </summary>
        [TestMethod]
        public void InsertSubscriptionAsync()
        {
            var getEntitiesModels = ListOfModelAndEntity();
            var getSubModel = (SubscriptionModel)getEntitiesModels[2];
            var getSubEntity = (SubscriptionEntity)getEntitiesModels[1];
            CassandraRepositoryMock.Setup(t => t.GetSingleAsync(It.IsAny<string>(), It.IsAny<Expression<Func<SubscriptionEntity, bool>>>())).Returns(Task.FromResult(getSubEntity)).Verifiable();
            CassandraRepositoryMock.Setup(t => t.InsertOrUpdateAsync(It.IsAny<SubscriptionEntity>(), It.IsAny<string>())).Returns(Task.FromResult(getSubEntity)).Verifiable();
            CassandraRepositoryMock.Setup(t => t.DeleteAsync(It.IsAny<string>(), It.IsAny<Expression<Func<SubscriptionEntity, bool>>>())).Returns(Task.FromResult(typeof(void)));

            var result = _subscription.InsertSubscriptionAsync(getSubModel, true, true).Result;

            CassandraRepositoryMock.Setup(t => t.GetSingleAsync(It.IsAny<string>(), It.IsAny<Expression<Func<SubscriptionEntity, bool>>>())).Returns(Task.FromResult((SubscriptionEntity)null)).Verifiable();
            CassandraRepositoryMock.Setup(t => t.InsertOrUpdateAsync(It.IsAny<SubscriptionEntity>(), It.IsAny<string>())).Returns(Task.FromResult(getSubEntity)).Verifiable();
            CassandraRepositoryMock.Setup(t => t.DeleteAsync(It.IsAny<string>(), It.IsAny<Expression<Func<SubscriptionEntity, bool>>>())).Returns(Task.FromResult(typeof(void)));
            getSubModel.IsSelected = false;
            var resultCovered = _subscription.InsertSubscriptionAsync(getSubModel, true, true).Result;

            CassandraRepositoryMock.Verify(t => t.GetSingleAsync(It.IsAny<string>(), It.IsAny<Expression<Func<SubscriptionEntity, bool>>>()), Times.AtMost(2));
            CassandraRepositoryMock.Verify(t => t.DeleteAsync(It.IsAny<string>(), It.IsAny<Expression<Func<SubscriptionEntity, bool>>>()), Times.AtMost(2));
            CassandraRepositoryMock.Verify(
                t => t.InsertOrUpdateAsync(It.Is<SubscriptionEntity>(s => s.LastModifiedDate >= DateTime.MinValue), It.IsAny<string>()), Times.AtMost(4));

            Assert.AreEqual(result, true);
            Assert.AreEqual(resultCovered, true);
        }

        /// <summary>
        /// Test case for inserting Subscription Async
        /// </summary>
        [TestMethod]
        public void InsertSubscriptionAsync_ElseIf()
        {
            // Arrange
            var getEntitiesModels = ListOfModelAndEntity();
            var getSubModel = (SubscriptionModel) getEntitiesModels[2];
            var getSubEntity = (SubscriptionEntity) getEntitiesModels[1];
            getSubEntity.IsSelected = false;
            getSubEntity.IsEmailOptInCompleted = false;
            getSubEntity.IsSmsOptInCompleted = false;
            CassandraRepositoryMock.Setup(
                t => t.GetSingleAsync(It.IsAny<string>(), It.IsAny<Expression<Func<SubscriptionEntity, bool>>>()))
                .Returns(Task.FromResult(getSubEntity))
                .Verifiable();
            CassandraRepositoryMock.Setup(t => t.InsertOrUpdateAsync(It.IsAny<SubscriptionEntity>(), It.IsAny<string>()))
                .Returns(Task.FromResult(true))
                .Verifiable();
            CassandraRepositoryMock.Setup(
                t => t.DeleteAsync(It.IsAny<string>(), It.IsAny<Expression<Func<SubscriptionEntity, bool>>>()))
                .Returns(Task.FromResult(true));
            getSubModel.IsSelected = true;
            getSubModel.IsEmailOptInCompleted = true;
            getSubModel.IsSmsOptInCompleted = true;

            // Act
            var result = _subscription.InsertSubscriptionAsync(getSubModel, true, true);

            // Assert
            CassandraRepositoryMock.Verify(
                t => t.GetSingleAsync(It.IsAny<string>(), It.IsAny<Expression<Func<SubscriptionEntity, bool>>>()),
                Times.AtMost(2));
            CassandraRepositoryMock.Verify(
                t => t.DeleteAsync(It.IsAny<string>(), It.IsAny<Expression<Func<SubscriptionEntity, bool>>>()),
                Times.AtMost(2));
            CassandraRepositoryMock.Verify(
                t => t.InsertOrUpdateAsync(It.Is<SubscriptionEntity>(s => s.LastModifiedDate >= DateTime.MinValue), It.IsAny<string>()), Times.AtMost(4));
            Assert.AreEqual(result.Result, true);
        }


        /// <summary>
        /// Test case for inserting Subscription Async
        /// </summary>
        [TestMethod]
        public void InsertSubscriptionAsync_If()
        {
            // Arrange
            var getEntitiesModels = ListOfModelAndEntity();
            var getSubModel = (SubscriptionModel) getEntitiesModels[2];
            var getSubEntity = (SubscriptionEntity) getEntitiesModels[1];
            getSubEntity.IsSelected = true;
            getSubEntity.IsEmailOptInCompleted = true;
            getSubEntity.IsSmsOptInCompleted = true;
            CassandraRepositoryMock.Setup(
                t => t.GetSingleAsync(It.IsAny<string>(), It.IsAny<Expression<Func<SubscriptionEntity, bool>>>()))
                .Returns(Task.FromResult(getSubEntity))
                .Verifiable();
            CassandraRepositoryMock.Setup(t => t.InsertOrUpdateAsync(It.IsAny<SubscriptionEntity>(), It.IsAny<string>()))
                .Returns(Task.FromResult(true))
                .Verifiable();
            CassandraRepositoryMock.Setup(
                t => t.DeleteAsync(It.IsAny<string>(), It.IsAny<Expression<Func<SubscriptionEntity, bool>>>()))
                .Returns(Task.FromResult(true));
            getSubModel.IsSelected = false;
            getSubModel.IsEmailOptInCompleted = false;
            getSubModel.IsSmsOptInCompleted = false;

            // Act
            var result = _subscription.InsertSubscriptionAsync(getSubModel, true, true);

            // Assert
            CassandraRepositoryMock.Verify(
                t => t.GetSingleAsync(It.IsAny<string>(), It.IsAny<Expression<Func<SubscriptionEntity, bool>>>()),
                Times.AtMost(2));
            CassandraRepositoryMock.Verify(
                t => t.DeleteAsync(It.IsAny<string>(), It.IsAny<Expression<Func<SubscriptionEntity, bool>>>()),
                Times.AtMost(2));
            CassandraRepositoryMock.Verify(
                t => t.InsertOrUpdateAsync(It.Is<SubscriptionEntity>(s =>s.LastModifiedDate >= DateTime.MinValue), It.IsAny<string>()), Times.AtMost(4));
            Assert.AreEqual(result.Result, true);
            MockRepository.VerifyAll();
        }
        
        /// <summary>
        /// Test case for inserting Subscription Async
        /// </summary>
        [TestMethod]
        public void InsertSubscription()
        {
            var getEntitiesModels = ListOfModelAndEntity();
            var getSubModel = (SubscriptionModel)getEntitiesModels[2];
            var getSubEntity = (SubscriptionEntity)getEntitiesModels[1];

            CassandraRepositoryMock.Setup(t => t.GetSingle(It.IsAny<string>(), It.IsAny<Expression<Func<SubscriptionEntity, bool>>>())).Returns(getSubEntity).Verifiable();
            CassandraRepositoryMock.Setup(t => t.InsertOrUpdate(It.IsAny<SubscriptionEntity>(), It.IsAny<string>())).Returns(true).Verifiable();
            CassandraRepositoryMock.Setup(t => t.Delete(It.IsAny<string>(), It.IsAny<Expression<Func<SubscriptionEntity, bool>>>()));

            var result = _subscription.InsertSubscription(getSubModel);

            CassandraRepositoryMock.Setup(t => t.GetSingle(It.IsAny<string>(), It.IsAny<Expression<Func<SubscriptionEntity, bool>>>())).Returns((SubscriptionEntity)null).Verifiable();
            CassandraRepositoryMock.Setup(t => t.InsertOrUpdate(It.IsAny<SubscriptionEntity>(), It.IsAny<string>())).Returns(true).Verifiable();
            CassandraRepositoryMock.Setup(t => t.Delete(It.IsAny<string>(), It.IsAny<Expression<Func<SubscriptionEntity, bool>>>()));
            getSubModel.IsSelected = false;
            var resultCovered = _subscription.InsertSubscription(getSubModel);

            // Assert
            CassandraRepositoryMock.Verify(t => t.GetSingle(It.IsAny<string>(), It.IsAny<Expression<Func<SubscriptionEntity, bool>>>()), Times.AtMost(2));
            CassandraRepositoryMock.Verify(t => t.Delete(It.IsAny<string>(), It.IsAny<Expression<Func<SubscriptionEntity, bool>>>()), Times.AtMost(2));
            CassandraRepositoryMock.Verify(
                t => t.InsertOrUpdateAsync(It.Is<SubscriptionEntity>(s => s.LastModifiedDate >= DateTime.MinValue), It.IsAny<string>()), Times.AtMost(4));
            Assert.AreEqual(result, true);
            Assert.AreEqual(resultCovered, true);
        }

        /// <summary>
        /// Test case for inserting Subscription Async
        /// </summary>
        [TestMethod]
        public void InsertSubscription_ElseIf()
        {
            // Arrange
            var getEntitiesModels = ListOfModelAndEntity();
            var getSubModel = (SubscriptionModel)getEntitiesModels[2];
            var getSubEntity = (SubscriptionEntity)getEntitiesModels[1];
            getSubEntity.IsSelected = false;
            getSubEntity.IsEmailOptInCompleted = false;
            getSubEntity.IsSmsOptInCompleted = false;
            CassandraRepositoryMock.Setup(t => t.GetSingle(It.IsAny<string>(), It.IsAny<Expression<Func<SubscriptionEntity, bool>>>())).Returns(getSubEntity).Verifiable();
            CassandraRepositoryMock.Setup(t => t.InsertOrUpdate(It.IsAny<SubscriptionEntity>(), It.IsAny<string>())).Returns(true).Verifiable();
            CassandraRepositoryMock.Setup(t => t.Delete(It.IsAny<string>(), It.IsAny<Expression<Func<SubscriptionEntity, bool>>>()));
            getSubModel.IsSelected = true;
            getSubModel.IsEmailOptInCompleted = true;
            getSubModel.IsSmsOptInCompleted = true;

            // Act
            var result = _subscription.InsertSubscription(getSubModel);

            // Assert
            CassandraRepositoryMock.Verify(t => t.InsertOrUpdate(It.Is<SubscriptionEntity>(s => s.LastModifiedDate >= DateTime.MinValue), It.IsAny<string>()), Times.AtLeastOnce);
            Assert.AreEqual(result, true);
        }

        /// <summary>
        /// Test case for Get Customer Subscription
        /// </summary>
        [TestMethod]
        public void GetCustomerSubscriptions()
        {
            IList<SubscriptionEntity> subList = new List<SubscriptionEntity>();
            var getEntitiesModels = ListOfModelAndEntity();
            var getSubEntity = (SubscriptionEntity)getEntitiesModels[1];
            subList.Add(getSubEntity);

            CassandraRepositoryMock.Setup(t => t.GetAsync(It.IsAny<string>(), It.IsAny<Expression<Func<SubscriptionEntity, bool>>>())).Returns(Task.FromResult(subList));
            var result = _subscription.GetCustomerSubscriptions(1, "2", "3");
            CassandraRepositoryMock.Verify(t => t.GetAsync(It.IsAny<string>(), It.IsAny<Expression<Func<SubscriptionEntity, bool>>>()), Times.AtLeast(2));

            var firstOrDefault = result.FirstOrDefault();
            if (firstOrDefault != null) Assert.AreEqual(firstOrDefault.ClientId, 1);
        }


        /// <summary>
        /// Test case for Get Customer Subscription
        /// </summary>
        [TestMethod]
        public void MergeDefaultSubscriptionListWithSubscribedPrograms()
        {
            var getEntitiesModels = ListOfModelAndEntity();
            var getSubModel = (SubscriptionModel)getEntitiesModels[2];
            var getSubModelCopied = (SubscriptionModel)CloneObject(getSubModel);
            getSubModelCopied.InsightTypeName = "BillToDate";
            List<SubscriptionModel> subModelList = new List<SubscriptionModel> { getSubModel };
            List<SubscriptionModel> defSubList = new List<SubscriptionModel> { getSubModelCopied };
            var result = _subscription.MergeDefaultSubscriptionListWithSubscribedPrograms(subModelList, defSubList);
            var firstOrDefault = result.FirstOrDefault();
            var orDefault = subModelList.FirstOrDefault();
            if (orDefault != null) { orDefault.ServiceContractId = "RT67"; orDefault.InsightTypeName = "BillToDate"; }
            var resultAgain = _subscription.MergeDefaultSubscriptionListWithSubscribedPrograms(subModelList, defSubList);
            var firstOrDefaultAgain = resultAgain.FirstOrDefault();
            if (firstOrDefault != null) Assert.AreEqual((object)getSubModel.AccountId, firstOrDefault.AccountId);
            if (firstOrDefaultAgain != null) Assert.AreEqual((object)getSubModel.AccountId, firstOrDefaultAgain.AccountId);
        }


        /// <summary>
        /// Test case for Get Customer Subscription
        /// </summary>
        [TestMethod]
        public void MergeDefaultSubscriptionListWithSubscribedPrograms_SubScriptionInsighExist()
        {
            var getEntitiesModels = ListOfModelAndEntity();
            var getSubModel = (SubscriptionModel)getEntitiesModels[2];
            var getSubModelCopied = (SubscriptionModel)CloneObject(getSubModel);
            getSubModelCopied.InsightTypeName = "BillToDate";
            getSubModelCopied.ServiceContractId = "";
            List<SubscriptionModel> subModelList = new List<SubscriptionModel> { getSubModel };
            List<SubscriptionModel> defSubList = new List<SubscriptionModel> { getSubModelCopied };
            var result = _subscription.MergeDefaultSubscriptionListWithSubscribedPrograms(subModelList, defSubList);
            var firstOrDefault = result.FirstOrDefault();
            if (firstOrDefault != null) Assert.AreEqual((object)getSubModel.AccountId, firstOrDefault.AccountId);
        }

        /// <summary>
        /// Test case for Get Customer Subscription
        /// </summary>
        [TestMethod]
        public void MergeDefaultSubscriptionListWithSubscribedPrograms_SubscriptionsNull()
        {
            var getEntitiesModels = ListOfModelAndEntity();
            var getSubModel = (SubscriptionModel)getEntitiesModels[2];
            var getSubModelCopied = (SubscriptionModel)CloneObject(getSubModel);
            getSubModelCopied.InsightTypeName = "BillToDate";
            List<SubscriptionModel> defSubList = new List<SubscriptionModel> { getSubModelCopied };
            var result = _subscription.MergeDefaultSubscriptionListWithSubscribedPrograms(null, defSubList);
            var firstOrDefault = result.FirstOrDefault();
            if (firstOrDefault != null) Assert.AreEqual((object)getSubModel.AccountId, firstOrDefault.AccountId);
        }

        /// <summary>
        /// Method Written for Copying an object by Value(Not by reference)
        /// </summary>
        /// <param name="objSource"></param>
        /// <returns>object</returns>
        public static object CloneObject(object objSource)
        {
            //step : 1 Get the type of source object and create a new instance of that type
            Type typeSource = objSource.GetType();
            object objTarget = Activator.CreateInstance(typeSource);
            //Step2 : Get all the properties of source object type
            PropertyInfo[] propertyInfo = typeSource.GetProperties(BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance);
            //Step : 3 Assign all source property to taget object 's properties
            foreach (PropertyInfo property in propertyInfo)
            {
                //Check whether property can be written to
                if (!property.CanWrite) continue;
                //Step : 4 check whether property type is value type, enum or string type
                if (property.PropertyType.IsValueType || property.PropertyType.IsEnum || property.PropertyType == typeof(string))
                {
                    property.SetValue(objTarget, property.GetValue(objSource, null), null);
                }
                //else property type is object/complex types, so need to recursively call this method until the end of the tree is reached
                else
                {
                    object objPropertyValue = property.GetValue(objSource, null);
                    property.SetValue(objTarget, objPropertyValue == null ? null : CloneObject(objPropertyValue), null);
                }
            }
            return objTarget;
        }

        /// <summary>
        /// Test case for Get Customer Subscription
        /// </summary>
        [TestMethod]
        public void InsertorMergeForSmsOptOut()
        {
            var getEntitiesModels = ListOfModelAndEntity();
            var getSubModel = (SubscriptionModel)getEntitiesModels[2];
            var getSubEntity = (SubscriptionEntity)getEntitiesModels[1];
            var getBillingModel = (BillingModel)getEntitiesModels[0];

            List<SubscriptionModel> subModelList = new List<SubscriptionModel> { getSubModel };

            _clientConfigFacadeMock.Setup(t => t.GetClientSettings(It.IsAny<int>())).Returns(GetClientSettings());
            CassandraRepositoryMock.Setup(t => t.GetSingle(It.IsAny<string>(), It.IsAny<Expression<Func<SubscriptionEntity, bool>>>())).Returns(getSubEntity).Verifiable();

            CassandraRepositoryMock.Setup(t => t.InsertOrUpdate(It.IsAny<SubscriptionEntity>(), It.IsAny<string>())).Returns(true).Verifiable();
            CassandraRepositoryMock.Setup(t => t.Delete(It.IsAny<string>(), It.IsAny<Expression<Func<SubscriptionEntity, bool>>>()));

            var result = _subscription.InsertorMergeForSmsOptOut(getBillingModel, subModelList, 87, "2", "3", null, "Program1", "BillToDate");

            var queueStuff = new Queue<SubscriptionEntity>();
            queueStuff.Enqueue(getSubEntity);
            queueStuff.Enqueue(null);
            queueStuff.Enqueue(getSubEntity);
            queueStuff.Enqueue(getSubEntity);

            CassandraRepositoryMock.Setup(t => t.GetSingle(It.IsAny<string>(), It.IsAny<Expression<Func<SubscriptionEntity, bool>>>())).Returns(queueStuff.Dequeue).Verifiable();
            CassandraRepositoryMock.Setup(t => t.InsertOrUpdate(It.IsAny<SubscriptionEntity>(), It.IsAny<string>())).Returns(true).Verifiable();
            CassandraRepositoryMock.Setup(t => t.Delete(It.IsAny<string>(), It.IsAny<Expression<Func<SubscriptionEntity, bool>>>()));
            
            var resultSecond = _subscription.InsertorMergeForSmsOptOut(getBillingModel, subModelList, 87, "2", "3", null, "Program1", "BillToDate");

            CassandraRepositoryMock.Verify(t => t.GetSingle(It.IsAny<string>(), It.IsAny<Expression<Func<SubscriptionEntity, bool>>>()), Times.AtMost(8));
            CassandraRepositoryMock.Verify(t => t.Delete(It.IsAny<string>(), It.IsAny<Expression<Func<SubscriptionEntity, bool>>>()), Times.AtMost(8));
            CassandraRepositoryMock.Verify(t => t.InsertOrUpdate(It.IsAny<SubscriptionEntity>(), It.IsAny<string>()), Times.AtMost(8));
            Assert.AreEqual(result, true);
            Assert.AreEqual(resultSecond, true);
        }


        private static ClientSettings GetClientSettings()
        {
            var clientSettingsJson = new List<ClientConfigurationBulk>
            {
                new ClientConfigurationBulk
                {
                    Category = "system",
                    Key = "aclaraone.clientsettings",
                    Value =
                        "{\r\n \"ClientId\": 87,\r\n \"TimeZone\": \"EST\",\r\n \"IsAmiIntervalStart\": false,\r\n \"IsDstHandled\": false,\r\n \"CalculationScheduleTime\": \"04:00\",\r\n \"NotificationReportScheduleTime\": \"05:00\",\r\n \"InsightReportScheduleTime\": \"06:00\",\r\n \"ForgotPasswordEmailTemplateId\": \"af5c0251-486f-4100-a4cf-2d91a905f108\",\r\n \"RegistrationConfirmationEmailTemplateId\": \"7c0657cd-1020-42a4-a281-87db82a7f6c4\",\r\n \"OptInEmailConfirmationTemplateId\": \"777ca9f8-bb03-490b-946b-b1e6991b2b15\",\r\n \"OptInSmsConfirmationTemplateId\": \"87_12\",\r\n \"OptInEmailUserName\": \"aclaradev\",\r\n \"OptInEmailPassword\": \"Xaclaradev2311X\",\r\n \"OptInEmailFrom\": \"support@aclarax.com\",\r\n \"TrumpiaShortCode\": \"99000\",\r\n \"TrumpiaApiKey\": \"b4c1971153b3509b6ec0d8a24a33454c\",\r\n \"TrumpiaUserName\": \"aclaradev\",\r\n \"TrumpiaContactList\": \"AclaraDemoDev\",\r\n \"UnmaskedAccountIdEndingDigit\" : \"4\",\r\n \"Programs\": [\r\n  {\r\n   \"ProgramName\": \"Program1\",\r\n   \"UtilityProgramName\": \"Program1\",\r\n   \"FileExportRequired\": true,\r\n   \"DoubleOptInRequired\": true,\r\n   \"TrumpiaKeyword\": \"abagdasarian2\",\r\n   \"Insights\": [\r\n    {\r\n     \"InsightName\": \"BillToDate\",\r\n     \"UtilityInsightName\": \"BillToDate\",\r\n     \"AllowedCommunicationChannel\": \"File,Email,SMS,EmailandSMS\",\r\n     \"DefaultCommunicationChannel\": \"Email\",\r\n     \"DefaultFrequency\": \"Daily\",\r\n     \"DefaultValue\": \"\",\r\n     \"EmailTemplateId\": \"c8d5e21f-1150-4b3d-b4b8-ad869d89a040\",\r\n     \"SMSTemplateId\": \"87_1\",\r\n     \"NotifyTime\": \"20:00\",\r\n     \"NotificationDay\": \"Monday\",\r\n     \"TrumpiaKeyword\": \"\",\r\n     \"Level\": \"Account\",\r\n     \"CommodityType\": \"\",\r\n     \"ThresholdType\": \"\",\r\n     \"ThresholdMin\": \"\",\r\n     \"ThresholdMax\": \"\"\r\n    }, {\r\n     \"InsightName\": \"AccountProjectedCost\",\r\n     \"UtilityInsightName\": \"AccountProjectedCost\",\r\n     \"AllowedCommunicationChannel\": \"File,Email,SMS,EmailandSMS\",\r\n     \"DefaultCommunicationChannel\": \"Email\",\r\n     \"DefaultFrequency\": \"Daily\",\r\n     \"DefaultValue\": \"\",\r\n     \"EmailTemplateId\": \"7367b661-0dfd-4eb2-a0d8-0735a459fcb5\",\r\n     \"SMSTemplateId\": \"87_3\",\r\n     \"NotifyTime\": \"20:00\",\r\n     \"NotificationDay\": \"Monday\",\r\n     \"TrumpiaKeyword\": \"\",\r\n     \"Level\": \"Account\",\r\n     \"CommodityType\": \"\",\r\n     \"ThresholdType\": \"\",\r\n     \"ThresholdMin\": \"\",\r\n     \"ThresholdMax\": \"\"\r\n    }, {\r\n     \"InsightName\": \"AccountLevelCostThreshold\",\r\n     \"UtilityInsightName\": \"AccountLevelCostThreshold\",\r\n     \"AllowedCommunicationChannel\": \"File,Email,SMS,EmailandSMS\",\r\n     \"DefaultCommunicationChannel\": \"Email\",\r\n     \"DefaultFrequency\": \"Immediate\",\r\n     \"DefaultValue\": \"100\",\r\n     \"EmailTemplateId\": \"7b3f9981-ae52-4fd9-b223-8ae516f6f82f\",\r\n     \"SMSTemplateId\": \"87_6\",\r\n     \"NotifyTime\": \"\",\r\n     \"NotificationDay\": \"\",\r\n     \"TrumpiaKeyword\": \"\",\r\n     \"Level\": \"Account\",\r\n     \"CommodityType\": \"\",\r\n     \"ThresholdType\": \"Cost\",\r\n     \"ThresholdMin\": \"1\",\r\n     \"ThresholdMax\": \"1000000\"\r\n    }, {\r\n     \"InsightName\": \"ServiceLevelCostThreshold\",\r\n     \"UtilityInsightName\": \"ServiceLevelCostThreshold\",\r\n     \"AllowedCommunicationChannel\": \"File,Email,SMS,EmailandSMS\",\r\n     \"DefaultCommunicationChannel\": \"Email\",\r\n     \"DefaultFrequency\": \"Immediate\",\r\n     \"DefaultValue\": \"25,25\",\r\n     \"EmailTemplateId\": \"87791eaa-0342-45d5-85d4-4645f7a85f7d\",\r\n     \"SMSTemplateId\": \"87_8\",\r\n     \"NotifyTime\": \"\",\r\n     \"NotificationDay\": \"\",\r\n     \"TrumpiaKeyword\": \"\",\r\n     \"Level\": \"Service\",\r\n     \"CommodityType\": \"Gas,Electric\",\r\n     \"ThresholdType\": \"Cost\",\r\n     \"ThresholdMin\": \"1\",\r\n     \"ThresholdMax\": \"1000000\"\r\n    }, {\r\n     \"InsightName\": \"ServiceLevelUsageThreshold\",\r\n     \"UtilityInsightName\": \"ServiceLevelUsageThreshold\",\r\n     \"AllowedCommunicationChannel\": \"File,Email,SMS,EmailandSMS\",\r\n     \"DefaultCommunicationChannel\": \"Email\",\r\n     \"DefaultFrequency\": \"Immediate\",\r\n     \"DefaultValue\": \"25,500\",\r\n     \"EmailTemplateId\": \"f7791cc2-9665-4d4b-8a7f-9421daf55e29\",\r\n     \"SMSTemplateId\": \"87_7\",\r\n     \"NotifyTime\": \"\",\r\n     \"NotificationDay\": \"\",\r\n     \"TrumpiaKeyword\": \"\",\r\n     \"Level\": \"Service\",\r\n     \"CommodityType\": \"Gas,Electric\",\r\n     \"ThresholdType\": \"Usage\",\r\n     \"ThresholdMin\": \"0.1\",\r\n     \"ThresholdMax\": \"1000000\"\r\n    }, {\r\n     \"InsightName\": \"CostToDate\",\r\n     \"UtilityInsightName\": \"CostToDate\",\r\n     \"AllowedCommunicationChannel\": \"File,Email,SMS,EmailandSMS\",\r\n     \"DefaultCommunicationChannel\": \"Email\",\r\n     \"DefaultFrequency\": \"Weekly\",\r\n     \"DefaultValue\": \"\",\r\n     \"EmailTemplateId\": \"b4425b3d-de12-4694-8751-edb41bc5cd79\",\r\n     \"SMSTemplateId\": \"87_2\",\r\n     \"NotifyTime\": \"20:00\",\r\n     \"NotificationDay\": \"Monday\",\r\n     \"TrumpiaKeyword\": \"\",\r\n     \"Level\": \"Service\",\r\n     \"CommodityType\": \"Gas,Electric\",\r\n     \"ThresholdType\": \"\",\r\n     \"ThresholdMin\": \"\",\r\n     \"ThresholdMax\": \"\"\r\n    }, {\r\n     \"InsightName\": \"ServiceProjectedCost\",\r\n     \"UtilityInsightName\": \"ServiceProjectedCost\",\r\n     \"AllowedCommunicationChannel\": \"File,Email,SMS,EmailandSMS\",\r\n     \"DefaultCommunicationChannel\": \"Email\",\r\n     \"DefaultFrequency\": \"Daily\",\r\n     \"DefaultValue\": \"\",\r\n     \"EmailTemplateId\": \"6bd40b25-fc86-480f-a1d4-0f2a22b7d270\",\r\n     \"SMSTemplateId\": \"87_4\",\r\n     \"NotifyTime\": \"20:00\",\r\n     \"NotificationDay\": \"Monday\",\r\n     \"TrumpiaKeyword\": \"\",\r\n     \"Level\": \"Service\",\r\n     \"CommodityType\": \"Gas,Electric\",\r\n     \"ThresholdType\": \"\",\r\n     \"ThresholdMin\": \"\",\r\n     \"ThresholdMax\": \"\"\r\n    }, {\r\n     \"InsightName\": \"Usage\",\r\n     \"UtilityInsightName\": \"Usage\",\r\n     \"AllowedCommunicationChannel\": \"File,Email,SMS,EmailandSMS\",\r\n     \"DefaultCommunicationChannel\": \"Email\",\r\n     \"DefaultFrequency\": \"Daily\",\r\n     \"DefaultValue\": \"\",\r\n     \"EmailTemplateId\": \"3fdb3336-0192-4bb9-bc53-8810dd110a3a\",\r\n     \"SMSTemplateId\": \"87_5\",\r\n     \"NotifyTime\": \"20:00\",\r\n     \"NotificationDay\": \"Monday\",\r\n     \"TrumpiaKeyword\": \"\",\r\n     \"Level\": \"Service\",\r\n     \"CommodityType\": \"Gas,Electric\",\r\n     \"ThresholdType\": \"\",\r\n     \"ThresholdMin\": \"\",\r\n     \"ThresholdMax\": \"\"\r\n    }, {\r\n     \"InsightName\": \"DayThreshold\",\r\n     \"UtilityInsightName\": \"DayThreshold\",\r\n     \"AllowedCommunicationChannel\": \"File,Email,SMS,EmailandSMS\",\r\n     \"DefaultCommunicationChannel\": \"Email\",\r\n     \"DefaultFrequency\": \"Immediate\",\r\n     \"DefaultValue\": \"1,16\",\r\n     \"EmailTemplateId\": \"ce508747-444b-4e1c-9f39-53629e47260b\",\r\n     \"SMSTemplateId\": \"87_9\",\r\n     \"NotifyTime\": \"\",\r\n     \"NotificationDay\": \"\",\r\n     \"TrumpiaKeyword\": \"\",\r\n     \"Level\": \"Service\",\r\n     \"CommodityType\": \"Gas,Electric\",\r\n     \"ThresholdType\": \"Usage\",\r\n     \"ThresholdMin\": \"0.1\",\r\n     \"ThresholdMax\": \"1000000\"\r\n    }, {\r\n     \"InsightName\": \"ServiceLevelTieredThresholdApproaching\",\r\n     \"UtilityInsightName\": \"ServiceLevelTieredThresholdApproaching\",\r\n     \"AllowedCommunicationChannel\": \"File,Email,SMS,EmailandSMS\",\r\n     \"DefaultCommunicationChannel\": \"Email\",\r\n     \"DefaultFrequency\": \"Immediate\",\r\n     \"DefaultValue\": \"20,20\",\r\n     \"EmailTemplateId\": \"4962e788-fb1e-43cf-a2f6-faf713d25e0c\",\r\n     \"SMSTemplateId\": \"87_10\",\r\n     \"NotifyTime\": \"\",\r\n     \"NotificationDay\": \"\",\r\n     \"TrumpiaKeyword\": \"\",\r\n     \"Level\": \"Service\",\r\n     \"CommodityType\": \"Gas,Electric\",\r\n     \"ThresholdType\": \"Percent\",\r\n     \"ThresholdMin\": \"1\",\r\n     \"ThresholdMax\": \"100\"\r\n    }, {\r\n     \"InsightName\": \"ServiceLevelTieredThresholdExceed\",\r\n     \"UtilityInsightName\": \"ServiceLevelTieredThresholdExceed\",\r\n     \"AllowedCommunicationChannel\": \"File,Email,SMS,EmailandSMS\",\r\n     \"DefaultCommunicationChannel\": \"Email\",\r\n     \"DefaultFrequency\": \"Immediate\",\r\n     \"DefaultValue\": \"\",\r\n     \"EmailTemplateId\": \"c1dc511c-0dd7-4208-bad7-8729fa097194\",\r\n     \"SMSTemplateId\": \"87_11\",\r\n     \"NotifyTime\": \"\",\r\n     \"NotificationDay\": \"\",\r\n     \"TrumpiaKeyword\": \"\",\r\n     \"Level\": \"Service\",\r\n     \"CommodityType\": \"Gas,Electric\",\r\n     \"ThresholdType\": \"\",\r\n     \"ThresholdMin\": \"\",\r\n     \"ThresholdMax\": \"\"\r\n    }\r\n   ]\r\n  }, {\r\n   \"ProgramName\": \"Program2\",\r\n   \"UtilityProgramName\": \"Program2\",\r\n   \"FileExportRequired\": false,\r\n   \"DoubleOptInRequired\": true,\r\n   \"TrumpiaKeyword\": \"\",\r\n   \"Insights\": [\r\n    {\r\n     \"InsightName\": \"ServiceLevelCostThreshold\",\r\n     \"UtilityInsightName\": \"ServiceLevelCostThreshold\",\r\n     \"AllowedCommunicationChannel\": \"File,Email,SMS,EmailandSMS\",\r\n     \"DefaultCommunicationChannel\": \"Email\",\r\n     \"DefaultFrequency\": \"Immediate\",\r\n     \"DefaultValue\": \"25\",\r\n     \"EmailTemplateId\": \"87791eaa-0342-45d5-85d4-4645f7a85f7d\",\r\n     \"SMSTemplateId\": \"87_8\",\r\n     \"NotifyTime\": \"\",\r\n     \"NotificationDay\": \"\",\r\n     \"TrumpiaKeyword\": \"\",\r\n     \"Level\": \"Service\",\r\n     \"CommodityType\": \"Water\",\r\n     \"ThresholdType\": \"Cost\",\r\n     \"ThresholdMin\": \"1\",\r\n     \"ThresholdMax\": \"1000000\"\r\n    }, {\r\n     \"InsightName\": \"ServiceLevelUsageThreshold\",\r\n     \"UtilityInsightName\": \"ServiceLevelUsageThreshold\",\r\n     \"AllowedCommunicationChannel\": \"File,Email,SMS,EmailandSMS\",\r\n     \"DefaultCommunicationChannel\": \"Email\",\r\n     \"DefaultFrequency\": \"Immediate\",\r\n     \"DefaultValue\": \"5\",\r\n     \"EmailTemplateId\": \"f7791cc2-9665-4d4b-8a7f-9421daf55e29\",\r\n     \"SMSTemplateId\": \"87_7\",\r\n     \"NotifyTime\": \"\",\r\n     \"NotificationDay\": \"\",\r\n     \"TrumpiaKeyword\": \"\",\r\n     \"Level\": \"Service\",\r\n     \"CommodityType\": \"Water\",\r\n     \"ThresholdType\": \"Usage\",\r\n     \"ThresholdMin\": \"0.1\",\r\n     \"ThresholdMax\": \"1000000\"\r\n    }, {\r\n     \"InsightName\": \"CostToDate\",\r\n     \"UtilityInsightName\": \"CostToDate\",\r\n     \"AllowedCommunicationChannel\": \"File,Email,SMS,EmailandSMS\",\r\n     \"DefaultCommunicationChannel\": \"Email\",\r\n     \"DefaultFrequency\": \"Weekly\",\r\n     \"DefaultValue\": \"\",\r\n     \"EmailTemplateId\": \"b4425b3d-de12-4694-8751-edb41bc5cd79\",\r\n     \"SMSTemplateId\": \"87_2\",\r\n     \"NotifyTime\": \"20:00\",\r\n     \"NotificationDay\": \"Monday\",\r\n     \"TrumpiaKeyword\": \"\",\r\n     \"Level\": \"Service\",\r\n     \"CommodityType\": \"Water\",\r\n     \"ThresholdType\": \"\",\r\n     \"ThresholdMin\": \"\",\r\n     \"ThresholdMax\": \"\"\r\n    }, {\r\n     \"InsightName\": \"ServiceProjectedCost\",\r\n     \"UtilityInsightName\": \"ServiceProjectedCost\",\r\n     \"AllowedCommunicationChannel\": \"File,Email,SMS,EmailandSMS\",\r\n     \"DefaultCommunicationChannel\": \"Email\",\r\n     \"DefaultFrequency\": \"Daily\",\r\n     \"DefaultValue\": \"\",\r\n     \"EmailTemplateId\": \"6bd40b25-fc86-480f-a1d4-0f2a22b7d270\",\r\n     \"SMSTemplateId\": \"87_4\",\r\n     \"NotifyTime\": \"20:00\",\r\n     \"NotificationDay\": \"Monday\",\r\n     \"TrumpiaKeyword\": \"\",\r\n     \"Level\": \"Service\",\r\n     \"CommodityType\": \"Water\",\r\n     \"ThresholdType\": \"\",\r\n     \"ThresholdMin\": \"\",\r\n     \"ThresholdMax\": \"\"\r\n    }, {\r\n     \"InsightName\": \"Usage\",\r\n     \"UtilityInsightName\": \"Usage\",\r\n     \"AllowedCommunicationChannel\": \"File,Email,SMS,EmailandSMS\",\r\n     \"DefaultCommunicationChannel\": \"Email\",\r\n     \"DefaultFrequency\": \"Daily\",\r\n     \"DefaultValue\": \"\",\r\n     \"EmailTemplateId\": \"3fdb3336-0192-4bb9-bc53-8810dd110a3a\",\r\n     \"SMSTemplateId\": \"87_5\",\r\n     \"NotifyTime\": \"20:00\",\r\n     \"NotificationDay\": \"Monday\",\r\n     \"TrumpiaKeyword\": \"\",\r\n     \"Level\": \"Service\",\r\n     \"CommodityType\": \"Water\",\r\n     \"ThresholdType\": \"\",\r\n     \"ThresholdMin\": \"\",\r\n     \"ThresholdMax\": \"\"\r\n    }, {\r\n     \"InsightName\": \"DayThreshold\",\r\n     \"UtilityInsightName\": \"DayThreshold\",\r\n     \"AllowedCommunicationChannel\": \"File,Email,SMS,EmailandSMS\",\r\n     \"DefaultCommunicationChannel\": \"Email\",\r\n     \"DefaultFrequency\": \"Immediate\",\r\n     \"DefaultValue\": \"0.15\",\r\n     \"EmailTemplateId\": \"ce508747-444b-4e1c-9f39-53629e47260b\",\r\n     \"SMSTemplateId\": \"87_9\",\r\n     \"NotifyTime\": \"\",\r\n     \"NotificationDay\": \"\",\r\n     \"TrumpiaKeyword\": \"\",\r\n     \"Level\": \"Service\",\r\n     \"CommodityType\": \"Water\",\r\n     \"ThresholdType\": \"Usage\",\r\n     \"ThresholdMin\": \"0.1\",\r\n     \"ThresholdMax\": \"1000000\"\r\n    }, {\r\n     \"InsightName\": \"ServiceLevelTieredThresholdApproaching\",\r\n     \"UtilityInsightName\": \"ServiceLevelTieredThresholdApproaching\",\r\n     \"AllowedCommunicationChannel\": \"File,Email,SMS,EmailandSMS\",\r\n     \"DefaultCommunicationChannel\": \"Email\",\r\n     \"DefaultFrequency\": \"Immediate\",\r\n     \"DefaultValue\": \"20\",\r\n     \"EmailTemplateId\": \"4962e788-fb1e-43cf-a2f6-faf713d25e0c\",\r\n     \"SMSTemplateId\": \"87_10\",\r\n     \"NotifyTime\": \"\",\r\n     \"NotificationDay\": \"\",\r\n     \"TrumpiaKeyword\": \"\",\r\n     \"Level\": \"Service\",\r\n     \"CommodityType\": \"Water\",\r\n     \"ThresholdType\": \"Percent\",\r\n     \"ThresholdMin\": \"1\",\r\n     \"ThresholdMax\": \"100\"\r\n    }, {\r\n     \"InsightName\": \"ServiceLevelTieredThresholdExceed\",\r\n     \"UtilityInsightName\": \"ServiceLevelTieredThresholdExceed\",\r\n     \"AllowedCommunicationChannel\": \"File,Email,SMS,EmailandSMS\",\r\n     \"DefaultCommunicationChannel\": \"Email\",\r\n     \"DefaultFrequency\": \"Immediate\",\r\n     \"DefaultValue\": \"\",\r\n     \"EmailTemplateId\": \"c1dc511c-0dd7-4208-bad7-8729fa097194\",\r\n     \"SMSTemplateId\": \"87_11\",\r\n     \"NotifyTime\": \"\",\r\n     \"NotificationDay\": \"\",\r\n     \"TrumpiaKeyword\": \"\",\r\n     \"Level\": \"Service\",\r\n     \"CommodityType\": \"Water\",\r\n     \"ThresholdType\": \"\",\r\n     \"ThresholdMin\": \"\",\r\n     \"ThresholdMax\": \"\"\r\n    }\r\n   ]\r\n  }\r\n ]\r\n}"
                }
            };
            return JsonConvert.DeserializeObject<ClientSettings>(clientSettingsJson[0].Value);
        }


        [TestMethod]
        public void DeleteSubscriptionAsync()
        {
            CassandraRepositoryMock.Setup(
                t => t.DeleteAsync(It.IsAny<string>(), It.IsAny<Expression<Func<SubscriptionEntity, bool>>>())).Verifiable();
            var result = _subscription.DeleteSubscriptionAsync(87, "A", "Cust900", "ACC23").Result;
            CassandraRepositoryMock.Verify(t => t.DeleteAsync(It.IsAny<string>(), It.IsAny<Expression<Func<SubscriptionEntity, bool>>>()), Times.Once());
            Assert.AreEqual(result, true);
        }

        /// <summary>
        /// Test case for Get Customer Subscription
        /// </summary>
        [TestMethod]
        public void GetSubscription()
        {
            var getEntitiesModels = ListOfModelAndEntity();
            var getSubEntity = (SubscriptionEntity)getEntitiesModels[1];
            IList<SubscriptionEntity> subList = new List<SubscriptionEntity>();
            subList.Add(getSubEntity);
            CassandraRepositoryMock.Setup(t => t.GetAsync(It.IsAny<string>(), It.IsAny<Expression<Func<SubscriptionEntity, bool>>>())).Returns(Task.FromResult(subList));
            var result = _subscription.GetSubscription(1, DateTime.Now, DateTime.Now, "TUYRHDF");
            CassandraRepositoryMock.Verify(t => t.GetAsync(It.IsAny<string>(), It.IsAny<Expression<Func<SubscriptionEntity, bool>>>()), Times.Once);
            var firstOrDefault = result.FirstOrDefault();
            if (firstOrDefault != null) Assert.AreEqual(firstOrDefault.ClientId, 1);
        }
    }
}
