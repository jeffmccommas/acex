﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;
using AO.BusinessContracts;
using AO.Entities;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace AO.Business.Tests.UnitTests
{
    /// <summary>
    /// Test case class for CassandraBusiness.TrumpiaKeywordConfiguration
    /// </summary>
    [TestClass]
    public class TrumpiaKeywordConfigurationTest : TestBase
    {
        private ITrumpiaKeywordConfiguration _trumpiaKeywordConfigurationManager;

        /// <summary>
        /// Test Case Init
        /// </summary>
        [TestInitialize]
        public void Init()
        {
            _trumpiaKeywordConfigurationManager = new TrumpiaKeywordConfiguration(CassandraRepositoryMock.Object);
        }

        /// <summary>
        /// Test case to fetch trumpia keyword configuration
        /// </summary>
        [TestMethod]
        public void GetTrumpiaKeywordConfiguration()
        {
            var trumpiaKeywordConfiguration = new TrumpiaKeywordConfigurationEntity()
            {
                ClientId = "87",
                TrumpiaKeyword = "1012"
            };
            IList<TrumpiaKeywordConfigurationEntity> list = new List<TrumpiaKeywordConfigurationEntity>();
            list.Add(trumpiaKeywordConfiguration);
            CassandraRepositoryMock.Setup(
                t =>
                    t.GetAsync(It.IsAny<string>(), It.IsAny<Expression<Func<TrumpiaKeywordConfigurationEntity, bool>>>()))
                .Returns(Task.FromResult(list))
                .Verifiable();
            
            var result = _trumpiaKeywordConfigurationManager.GetTrumpiaKeywordConfiguration("1012");
            CassandraRepositoryMock.Verify(t => t.GetAsync(It.IsAny<string>(), It.IsAny<Expression<Func<TrumpiaKeywordConfigurationEntity, bool>>>()), Times.Once);
            CassandraRepositoryMock.VerifyAll();
            Assert.AreEqual(result.ClientId, "87");
        }
    }
}
