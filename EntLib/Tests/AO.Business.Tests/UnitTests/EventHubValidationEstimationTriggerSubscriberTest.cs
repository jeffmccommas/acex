﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AO.EventProcessors;
using AO.VEE.Business.Contracts;
using AO.VEE.DTO;
using CE.AO.Utilities;
using Microsoft.ServiceBus.Messaging;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Newtonsoft.Json;
using CE.AO.Models;

namespace AO.Business.Tests.UnitTests
{
    /// <summary>
    /// Test calss for EventHubValidationEstimationTriggerSubscriber
    /// </summary>
    [TestClass]
    public class EventHubValidationEstimationTriggerSubscriberTest
    {
        private EventHubValidationEstimationTriggerSubscriber _eventHubValidationEstimationTriggerSubscriber;
        private Mock<IVeeFacade> _veeFacade;

        /// <summary>
        /// Test Case Init
        /// </summary>
        [TestInitialize]
        public void Init()
        {
            _veeFacade = new Mock<IVeeFacade>();

            _eventHubValidationEstimationTriggerSubscriber =
                new EventHubValidationEstimationTriggerSubscriber(new LogModel { DisableLog = true },
                    new Lazy<EventProcessorFactory<EventHubValidationEstimationTriggerSubscriber>>(),
                    new Lazy<IVeeFacade>(() => _veeFacade.Object));
        }

        /// <summary>
        /// Test Case for ProcessEventsAsync
        /// </summary>%%
        [TestMethod]
        public void ProcessEventsAsync()
        {
            _veeFacade.Setup(p => p.ExecuteValidationsAndEstimations(It.IsAny<VeeValidationEstimationTriggerDto>()));

            var veeValidationEstimationTrigger = new VeeValidationEstimationTriggerDto()
            {
                ClientId = 87,
                MeterId = "Meter100",
                AmiDate = DateTime.Now
            };

            var serializedEventData = JsonConvert.SerializeObject(veeValidationEstimationTrigger);
            var eventData = new EventData(Encoding.UTF8.GetBytes(serializedEventData));
            var context = new PartitionContext();
            var lease = new Lease();
            context.Lease = lease;
            context.Lease.Offset = "121212";
            context.Lease.PartitionId = "0";
            context.ConsumerGroupName = "$Default";
            context.EventHubPath = "watermdmTest";
            eventData.Properties.Add("ProcessingType", Enums.ProcessingType.File);
            eventData.Properties.Add("Source", "S");

            var eventDataList = new List<EventData> { eventData };
            var isOpen = _eventHubValidationEstimationTriggerSubscriber.OpenAsync(context);
            var result = _eventHubValidationEstimationTriggerSubscriber.ProcessEventsAsync(context, eventDataList.AsEnumerable());
            Assert.IsTrue(isOpen.IsCompleted);
            Assert.IsTrue(result.IsCompleted);
        }
    }
}
