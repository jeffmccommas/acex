﻿using System;
using System.Linq.Expressions;
using System.Threading.Tasks;
using AO.BusinessContracts;
using AO.DataAccess.Contracts;
using AO.Entities;
using CE.AO.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace AO.Business.Tests.UnitTests
{
    /// <summary>
    /// Contains test methods for CassandraBusiness.AmiReading class
    /// </summary>
    [TestClass]
    public class AmiReadingTest : TestBase
    {
        private IAmiReading _amiReadingManager;

        /// <summary>
        /// Test Case Init
        /// </summary>
        [TestInitialize]
        public void Init()
        {
            _amiReadingManager = new AmiReading(LogModelMock.Object, new Lazy<ICassandraRepository>(()=>CassandraRepositoryMock.Object));
        }

        /// <summary>
        /// Test case for inserting AmiReading
        /// </summary>
        [TestMethod]
        public void InsertOrMergeAmiReadingAsync()
        {
            // Arrange
            var nccAmiReadingModel = new NccAmiReadingModel
            {
                CommodityId = 10021,
                MeterId = "65201",
                TransponderId = 225,
                ClientId = 87,
                TransponderPort = 55,
                CustomerId = "1012",
                ReadingValue = 50.65,
                UOMId = 333,
                AmiTimeStamp = DateTime.Now,
                Timezone = "1234",
                BatteryVoltage = ""
            };

            CassandraRepositoryMock.Setup(t => t.InsertOrUpdateAsync(It.IsAny<MeterReadingEntity>(), It.IsAny<string>())).Returns(Task.FromResult(true)).Verifiable();

            // Act
            var result = _amiReadingManager.InsertOrMergeAmiReadingAsync(nccAmiReadingModel).Result;

            // Assert
            CassandraRepositoryMock.Verify(t => t.InsertOrUpdateAsync(It.IsAny<MeterReadingEntity>(), It.IsAny<string>()), Times.Once);
            Assert.AreEqual(true, result);
            MockRepository.VerifyAll();
        }

        /// <summary>
        /// Test case to fetch AmiReadings
        /// </summary>
        [TestMethod]
        public void GetAmiReadings()
        {
            // Arrange
            var amiReadingEntity = new MeterReadingEntity
            {
                CommodityId = 10021,
                MeterId = "65201",
                TransponderId = 225,
                ClientId = 87,
                TransponderPort = 55,
                CustomerId = "1012",
                ReadingValue = 50.65,
                UomId = 333,
                AmiTimeStamp = DateTime.Now,
                Timezone = "1234",
                BatteryVoltage = ""
            };

            CassandraRepositoryMock.Setup(
                t =>
                    t.GetSingleAsync(It.IsAny<string>(), It.IsAny<Expression<Func<MeterReadingEntity, bool>>>()))
                .Returns(Task.FromResult(amiReadingEntity))
                .Verifiable();

            // Act
            Task<NccAmiReadingModel> result = _amiReadingManager.GetAmiReading(87, "1012", DateTime.Now);

            // Assert
            CassandraRepositoryMock.Verify(t => t.GetSingleAsync(It.IsAny<string>(), It.IsAny<Expression<Func<MeterReadingEntity, bool>>>()), Times.Once);
            Assert.AreEqual(result.Result.MeterId, "65201");
            MockRepository.VerifyAll();
        }
    }
}