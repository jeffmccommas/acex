﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using AO.BusinessContracts;
using AO.Entities;
using CE.AO.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace AO.Business.Tests.UnitTests
{
    /// <summary>
    /// Test Case Class for CassandraBusiness.Premise
    /// </summary>
    [TestClass]
    public class PremiseTest : TestBase
    {
        private IPremise _premiseManager;

        /// <summary>
        /// Test Case Init
        /// </summary>
        [TestInitialize]
        public void Init()
        {
            _premiseManager = new Premise(CassandraRepositoryMock.Object);
        }

        /// <summary>
        /// Test case for inserting premise
        /// </summary>
        [TestMethod]
        public void InsertOrMergePremiseAsync()
        {
            // Arrange
            var premiseModel = new PremiseModel
            {
                AddressLine1 = "H 24, Viola Hsg. Society",
                AddressLine2 = "Warje",
                AddressLine3 = "Pune",
                City = "Pune",
                ClientId = 87,
                Country = "US",
                CustomerId = "1012",
                PostalCode = "9041",
                State = "NJ",
                PremiseId = "1234",
                AccountId = "a5"
            };

            CassandraRepositoryMock.Setup(t => t.InsertOrUpdateAsync(It.Is<PremiseEntity>(p=>p.LastModifiedDate>DateTime.MinValue), It.IsAny<string>())).Returns(Task.FromResult(true)).Verifiable();

            // Act
            var result = _premiseManager.InsertOrMergePremiseAsync(premiseModel).Result;

            // Assert
            Assert.IsTrue(result);
            CassandraRepositoryMock.Verify(t => t.InsertOrUpdateAsync(It.IsAny<PremiseEntity>(), It.IsAny<string>()), Times.Once);
            MockRepository.VerifyAll();
        }

        /// <summary>
        /// Test case to fetch premises
        /// </summary>
        [TestMethod]
        public void GetPremises()
        {
            // Arrange
            var premiseEntity = new PremiseEntity
            {
                AddressLine1 = "H 24, Viola Hsg. Society",
                AddressLine2 = "Warje",
                AddressLine3 = "Pune",
                City = "Pune",
                ClientId = 87,
                Country = "US",
                CustomerId = "1012",
                PostalCode = "9041",
                State = "NJ",
                PremiseId = "1234",
                AccountId = "a5"
            };
            IList<PremiseEntity> list = new List<PremiseEntity>();
            list.Add(premiseEntity);

            CassandraRepositoryMock.Setup(
                t =>
                    t.Get(It.IsAny<string>(), It.IsAny<Expression<Func<PremiseEntity, bool>>>()))
                .Returns(list)
                .Verifiable();

            // Act
            List<PremiseModel> result = _premiseManager.GetPremises(87, "1012").ToList();

            // Assert
            CassandraRepositoryMock.Verify(t => t.Get(It.IsAny<string>(), It.IsAny<Expression<Func<PremiseEntity, bool>>>()), Times.Once);
            Assert.AreEqual(result.Count, 1);
            MockRepository.VerifyAll();
        }
    }
}
