﻿using AO.Entities;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace AO.Business.Tests.UnitTests
{
    /// <summary>
    /// Contains test methods for MemoryCacheProvider Class
    /// </summary>
    [TestClass]
    public class MemoryCacheProviderTest
    {
        private MemoryCacheProvider _memoryCacheProvider;
        private readonly DcuAlarmTypeEntity _cacheValue = new DcuAlarmTypeEntity();
        private const string CacheKey = "RemoveKeyAfterIntegrationTest";
        private const int CacheDatabase = 1;

        /// <summary>
        /// Test Case Init
        /// </summary>
        [TestInitialize]
        public void Init()
        {
            _memoryCacheProvider = new MemoryCacheProvider();
        }

        /// <summary>
        /// Test Case for Set,Get,Remove and FlushAllKeys Methods
        /// </summary>
        [TestMethod]
        public void SetAccessRemoveFlushCacheItem()
        {
            _memoryCacheProvider.Set(CacheKey, _cacheValue, CacheDatabase);
            _memoryCacheProvider.Get<DcuAlarmTypeEntity>(CacheKey, CacheDatabase);
            var success = _memoryCacheProvider.Remove(CacheKey, CacheDatabase);
            _memoryCacheProvider.Set(CacheKey, _cacheValue, CacheDatabase);
            _memoryCacheProvider.FlushAllKeys(CacheDatabase).Wait();
            _memoryCacheProvider.FlushAllKeys().Wait();
            _memoryCacheProvider.Set(CacheKey, _cacheValue, CacheDatabase);
            _memoryCacheProvider.FlushAllKeys(CacheDatabase, null).Wait();
            _memoryCacheProvider.FlushAllKeys(null).Wait();
            Assert.IsTrue(success);
        }
    }
}
