﻿using AO.BusinessContracts;
using AO.DataAccess.Contracts;
using CE.AO.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace AO.Business.Tests.UnitTests
{
    [TestClass]
    public class TestBase
    {
        protected Mock<ICassandraRepository> CassandraRepositoryMock;
        protected Mock<ICacheProvider> CacheProviderMock;
        protected Mock<LogModel> LogModelMock; 

        protected readonly MockRepository MockRepository = new MockRepository(MockBehavior.Strict);

        /// <summary>
        /// Test Case Init
        /// </summary>
        public TestBase()
        {
            CassandraRepositoryMock = new Mock<ICassandraRepository>();
            CacheProviderMock = new Mock<ICacheProvider>();
            LogModelMock = new Mock<LogModel>();
            LogModelMock.Object.DisableLog = true;

            AutoMapperConfig.AutoMapperMappings();
		}
    }
}
