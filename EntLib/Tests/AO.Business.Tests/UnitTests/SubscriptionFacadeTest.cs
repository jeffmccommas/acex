﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Runtime.Remoting.Messaging;
using System.Threading.Tasks;
using AO.BusinessContracts;
using AO.DataAccess.Contracts;
using AO.Entities;
using CE.AO.Models;
using CE.ContentModel.Entities;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Newtonsoft.Json;

namespace AO.Business.Tests.UnitTests
{
    [TestClass]
    public class SubscriptionFacadeTest : TestBase
    {
        private SubscriptionFacade _subscriptionFacade;
        private Mock<IBilling> _billingMock;
        private Mock<ISubscription> _subscriptionMock;
        //private Mock<Customer> _customerMock;
        private Customer _customerManager;
        private Mock<IClientConfigFacade> _clientConfigFacadeMock;
        private Mock<ISendEmail> _sendMailMock;
        private Mock<INotification> _notificationMock;
        private Mock<ITrumpiaRequestDetail> _trumpiaRequestDetailMock;
        private Mock<ISendSms> _sendSmsMock;

        /// <summary>
        /// Test Case Init
        /// </summary>
        [TestInitialize]
        public void Init()
        {
            _sendSmsMock = new Mock<ISendSms>();
            _billingMock = new Mock<IBilling>();
            _subscriptionMock = new Mock<ISubscription>();
            _customerManager = new Customer(LogModelMock.Object,
               new Lazy<ICassandraRepository>(() => CassandraRepositoryMock.Object),
               new Lazy<ISendSms>(() => _sendSmsMock.Object));
           _clientConfigFacadeMock = new Mock<IClientConfigFacade>();
            _sendMailMock = new Mock<ISendEmail>();
            _notificationMock = new Mock<INotification>();
            _trumpiaRequestDetailMock = new Mock<ITrumpiaRequestDetail>();
            

            _subscriptionFacade = new SubscriptionFacade(LogModelMock.Object,
                new Lazy<IBilling>(() => _billingMock.Object), new Lazy<ISubscription>(() => _subscriptionMock.Object),
                new Lazy<ICustomer>(() => _customerManager), new Lazy<INotification>(() => _notificationMock.Object),
                new Lazy<IClientConfigFacade>(() => _clientConfigFacadeMock.Object),
                new Lazy<ISendEmail>(() => _sendMailMock.Object),
                new Lazy<ITrumpiaRequestDetail>(() => _trumpiaRequestDetailMock.Object));
        }

        [TestMethod]
        public void GetDefaultListWithSubscribedPrograms()
        {

            _clientConfigFacadeMock.Setup(t => t.GetClientSettings(It.IsAny<int>())).Returns(GetClientSettings());
            List<SubscriptionModel> listSubscriptionModels = new List<SubscriptionModel>
            {
                new SubscriptionModel
                {
                    AccountId = "AA",
                    CustomerId = "A",
                    ClientId = 87,
                    ProgramName = "Program1",
                    InsightTypeName = "BillToDate"
                }
            };
            List<BillingModel> listBillingModels = new List<BillingModel>
            {
                new BillingModel
                {
                    ClientId = 87,
                    MeterId = "METER02",
                    CustomerId = "A",
                    AccountId = "A",
                    ServiceContractId = "A",
                    MeterType = "ami",
                    StartDate = DateTime.Now.AddDays(-5),
                    EndDate = DateTime.Now.AddDays(-2),
                    CommodityId = 2,
                    TotalUsage = 100,
                    TotalCost = 100,
                    RateClass = "C"
                },
                new BillingModel
                {
                    ClientId = 87,
                    MeterId = "METER02",
                    CustomerId = "A",
                    AccountId = "A",
                    ServiceContractId = "A",
                    MeterType = "nonmetered",
                    StartDate = DateTime.Now.AddDays(-5),
                    EndDate = DateTime.Now.AddDays(-2),
                    CommodityId = 2,
                    TotalUsage = 100,
                    TotalCost = 100,
                    RateClass = "C"
                }
            };

            _subscriptionMock.Setup(
                t =>
                    t.MergeDefaultSubscriptionListWithSubscribedPrograms(It.IsAny<List<SubscriptionModel>>(),
                        It.IsAny<List<SubscriptionModel>>())).Returns(listSubscriptionModels);

            _billingMock.Setup(
                t => t.GetAllServicesFromLastBill(It.IsAny<int>(), It.IsAny<string>(), It.IsAny<string>())).Returns(listBillingModels);
            
            List<SubscriptionModel> result = _subscriptionFacade.GetDefaultListWithSubscribedPrograms(87, "A", "AA");

            _subscriptionMock.Verify(t => t.GetCustomerSubscriptions(It.IsAny<int>(), It.IsAny<string>(), It.IsAny<string>()), Times.Once);
            Assert.AreEqual(result.First().ClientId, 87);
        }

        [TestMethod]
        public void GetDefaultListWithSubscribedPrograms_ServicesNull()
        {
            _clientConfigFacadeMock.Setup(t => t.GetClientSettings(It.IsAny<int>())).Returns(GetClientSettings());
            _billingMock.Setup(
                t => t.GetAllServicesFromLastBill(It.IsAny<int>(), It.IsAny<string>(), It.IsAny<string>())).Returns((List<BillingModel>)null);
            List<SubscriptionModel> result = _subscriptionFacade.GetDefaultListWithSubscribedPrograms(87, "A", "AA");
            Assert.AreEqual(result, null);
        }

        [TestMethod]
        public void GetDefaultListWithSubscribedPrograms_ServicesListZeroCount()
        {
            _clientConfigFacadeMock.Setup(t => t.GetClientSettings(It.IsAny<int>())).Returns(GetClientSettings());
            List<SubscriptionModel> listSubscriptionModels = new List<SubscriptionModel>
            {
                new SubscriptionModel
                {
                    AccountId = "AA",
                    CustomerId = "A",
                    ClientId = 87,
                    ProgramName = "Program1",
                    InsightTypeName = "BillToDate"
                }
            };
            // ReSharper disable once CollectionNeverUpdated.Local
            var listBillingModels = new List<BillingModel>();
            _subscriptionMock.Setup(
                t =>
                    t.MergeDefaultSubscriptionListWithSubscribedPrograms(It.IsAny<List<SubscriptionModel>>(),
                        It.IsAny<List<SubscriptionModel>>())).Returns(listSubscriptionModels);
            _billingMock.Setup(
                t => t.GetAllServicesFromLastBill(It.IsAny<int>(), It.IsAny<string>(), It.IsAny<string>())).Returns(listBillingModels);
            
            List<SubscriptionModel> result = _subscriptionFacade.GetDefaultListWithSubscribedPrograms(87, "A", "AA");

            _subscriptionMock.Verify(t => t.GetCustomerSubscriptions(It.IsAny<int>(), It.IsAny<string>(), It.IsAny<string>()), Times.Once);
            Assert.AreEqual(result, null);
        }

        [TestMethod]
        public void ValidateInsightList()
        {
            List<SubscriptionModel> listSubscriptionModels = new List<SubscriptionModel>
            {
                new SubscriptionModel
                {
                    AccountId = "AA",
                    CustomerId = "A",
                    ClientId = 87,
                    ProgramName = "Program1",
                    InsightTypeName = "BillToDate",
                    Channel = "Email"
                }
            };

            _clientConfigFacadeMock.Setup(t => t.GetClientSettings(It.IsAny<int>())).Returns(GetClientSettings());
            List<SubscriptionModel> result = _subscriptionFacade.ValidateInsightList(87, listSubscriptionModels);

            Assert.AreEqual(result.Count, 0);
        }

        [TestMethod]
        public void ValidateInsightList_SubscriberThresholdMin()
        {
            List<SubscriptionModel> listSubscriptionModels = new List<SubscriptionModel>
            {
                new SubscriptionModel
                {
                    AccountId = "AA",
                    CustomerId = "A",
                    ClientId = 87,
                    ProgramName = "Program1",
                    InsightTypeName = "ServiceLevelUsageThreshold",
                    Channel = "Email",
                    SubscriberThresholdValue = 0.01
                }
            };

            _clientConfigFacadeMock.Setup(t => t.GetClientSettings(It.IsAny<int>())).Returns(GetClientSettings());
            List<SubscriptionModel> result = _subscriptionFacade.ValidateInsightList(87, listSubscriptionModels);

            Assert.AreEqual(result.Count, 1);
        }

        [TestMethod]
        public void ValidateInsightList_SubscriberThreshold()
        {
            List<SubscriptionModel> listSubscriptionModels = new List<SubscriptionModel>
            {
                new SubscriptionModel
                {
                    AccountId = "AA",
                    CustomerId = "A",
                    ClientId = 87,
                    ProgramName = "Program1",
                    InsightTypeName = "ServiceLevelUsageThreshold",
                    Channel = "Email",
                    SubscriberThresholdValue = 1000001
                }
            };

            _clientConfigFacadeMock.Setup(t => t.GetClientSettings(It.IsAny<int>())).Returns(GetClientSettings());
            List<SubscriptionModel> result = _subscriptionFacade.ValidateInsightList(87, listSubscriptionModels);

            Assert.AreEqual(result.Count, 1);
        }

        [TestMethod]
        public void ValidateInsightList_InsightsThresholdZero()
        {
            List<SubscriptionModel> listSubscriptionModels = new List<SubscriptionModel>
            {
                new SubscriptionModel
                {
                    AccountId = "AA",
                    CustomerId = "A",
                    ClientId = 87,
                    ProgramName = "Program1",
                    InsightTypeName = "ServiceLevelUsageThreshold",
                    SubscriberThresholdValue = 0,
                    Channel = "invalidCh"
                }
            };

            _clientConfigFacadeMock.Setup(t => t.GetClientSettings(It.IsAny<int>())).Returns(GetClientSettings());
            List<SubscriptionModel> result = _subscriptionFacade.ValidateInsightList(87, listSubscriptionModels);

            Assert.AreEqual(result.Count, 1);
        }

        [TestMethod]
        public void ValidateInsightList_ProgramNameNull()
        {
            List<SubscriptionModel> listSubscriptionModels = new List<SubscriptionModel>
            {
                new SubscriptionModel
                {
                    AccountId = "AA",
                    CustomerId = "A",
                    ClientId = 87,
                    InsightTypeName = "BillToDate",
                    Channel = "Email"
                }
            };
            _clientConfigFacadeMock.Setup(t => t.GetClientSettings(It.IsAny<int>())).Returns(GetClientSettings());
            List<SubscriptionModel> result = _subscriptionFacade.ValidateInsightList(87, listSubscriptionModels);

            Assert.AreEqual(result.Count, 1);
        }

        [TestMethod]
        public void ValidateInsightList_InsightTypeNameNull()
        {
            List<SubscriptionModel> listSubscriptionModels = new List<SubscriptionModel>
            {
                new SubscriptionModel
                {
                    AccountId = "AA",
                    CustomerId = "A",
                    ClientId = 87,
                    ProgramName = "Program1",
                    Channel = "Email"
                }
            };
            _clientConfigFacadeMock.Setup(t => t.GetClientSettings(It.IsAny<int>())).Returns(GetClientSettings());
            List<SubscriptionModel> result = _subscriptionFacade.ValidateInsightList(87, listSubscriptionModels);

            Assert.AreEqual(result.Count, 1);
        }

        [TestMethod]
        public void ValidateInsightList_ProgramNameInsightsProgramNameUnEqual()
        {
            List<SubscriptionModel> listSubscriptionModels = new List<SubscriptionModel>
            {
                new SubscriptionModel
                {
                    AccountId = "AA",
                    CustomerId = "A",
                    ClientId = 87,
                    ProgramName = "Program",
                    InsightTypeName = "BillToDate",
                    Channel = "Email"
                }
            };

            _clientConfigFacadeMock.Setup(t => t.GetClientSettings(It.IsAny<int>())).Returns(GetClientSettings());
            List<SubscriptionModel> result = _subscriptionFacade.ValidateInsightList(87, listSubscriptionModels);

            Assert.AreEqual(result.Count, 1);
        }

        [TestMethod]
        public void ValidateInsightList_ProgramsZero()
        {
            List<SubscriptionModel> listSubscriptionModels = new List<SubscriptionModel>
            {
                new SubscriptionModel
                {
                    AccountId = "AA",
                    CustomerId = "A",
                    ClientId = 87,
                    ProgramName = "Program1",
                    InsightTypeName = "BillToDate",
                    Channel = "Email"
                }
            };

            _clientConfigFacadeMock.Setup(t => t.GetClientSettings(It.IsAny<int>())).Returns(new ClientSettings()
            {
                Programs = new List<ProgramSettings>()
            });
            List<SubscriptionModel> result = _subscriptionFacade.ValidateInsightList(87, listSubscriptionModels);
            Assert.AreEqual(result.Count, 1);
        }


        [TestMethod]
        public void ValidateInsightList_InvalidEmail()
        {
            List<SubscriptionModel> listSubscriptionModels = new List<SubscriptionModel>
            {
                new SubscriptionModel
                {
                    AccountId = "AA",
                    CustomerId = "A",
                    ClientId = 87,
                    ProgramName = "Program1",
                    InsightTypeName = "BillToDate",
                    Channel = "Email"
                }
            };

            _clientConfigFacadeMock.Setup(t => t.GetClientSettings(It.IsAny<int>())).Returns(GetClientSettings());
            var insightsErrMsg = "";
            List<SubscriptionModel> result = _subscriptionFacade.ValidateInsightList(87, listSubscriptionModels, "invalidEmail", "", ref insightsErrMsg);
            List<SubscriptionModel> result2 = _subscriptionFacade.ValidateInsightList(87, listSubscriptionModels, "a@gmail.com", "invalidPh", ref insightsErrMsg);
            Assert.AreEqual(result.Count, 1);
            Assert.AreEqual(result2.Count, 1);
        }

        [TestMethod]
        public void ValidateInsightList_InvalidInsightTypeNames()
        {
            List<SubscriptionModel> listSubscriptionModels = new List<SubscriptionModel>
            {
                new SubscriptionModel
                {
                    AccountId = "AA",
                    CustomerId = "A",
                    ClientId = 87,
                    ProgramName = "Program1",
                    InsightTypeName = "Invalid",
                    Channel = "Email"
                }
            };

            _clientConfigFacadeMock.Setup(t => t.GetClientSettings(It.IsAny<int>())).Returns(GetClientSettings());
            List<SubscriptionModel> result = _subscriptionFacade.ValidateInsightList(87, listSubscriptionModels);

            Assert.AreEqual(result.Count, 1);
        }

        [TestMethod]
        public void GetSubscribeList()
        {

            _clientConfigFacadeMock.Setup(t => t.GetClientSettings(It.IsAny<int>())).Returns(GetClientSettings());

            var listSubscriptionModels = new List<SubscriptionModel>
            {
                new SubscriptionModel
                {
                    AccountId = "AA",
                    CustomerId = "A",
                    ClientId = 87,
                    ProgramName = "Program1",
                    InsightTypeName = "BillToDate",
                    Channel = "Email",
                    IsSelected = true,
                    ServiceContractId = "AA"
                },
                new SubscriptionModel
                {
                    AccountId = "AA",
                    CustomerId = "A",
                    ClientId = 87,
                    ProgramName = "Program1",
                    InsightTypeName = "BillToDate",
                    Channel = "Email",
                    IsSelected = false,
                    ServiceContractId = "AA"
                }
            };
            List<BillingModel> listBillingModels = new List<BillingModel>
            {
                new BillingModel
                {
                    ClientId = 87,
                    MeterId = "METER02",
                    CustomerId = "A",
                    AccountId = "A",
                    ServiceContractId = "A",
                    MeterType = "ami",
                    StartDate = DateTime.Now.AddDays(-5),
                    EndDate = DateTime.Now.AddDays(-2),
                    CommodityId = 2,
                    TotalUsage = 100,
                    TotalCost = 100,
                    RateClass = "C"
                },
                new BillingModel
                {
                    ClientId = 87,
                    MeterId = "METER02",
                    CustomerId = "A",
                    AccountId = "A",
                    ServiceContractId = "A",
                    MeterType = "nonmetered",
                    StartDate = DateTime.Now.AddDays(-5),
                    EndDate = DateTime.Now.AddDays(-2),
                    CommodityId = 2,
                    TotalUsage = 100,
                    TotalCost = 100,
                    RateClass = "C"
                }
            };

            _subscriptionMock.Setup(t => t.GetCustomerSubscriptions(It.IsAny<int>(), It.IsAny<string>(), It.IsAny<string>())).Returns(listSubscriptionModels);
            _subscriptionMock.Setup(
                t =>
                    t.MergeDefaultSubscriptionListWithSubscribedPrograms(It.IsAny<List<SubscriptionModel>>(),
                        It.IsAny<List<SubscriptionModel>>())).Returns(listSubscriptionModels);

            _billingMock.Setup(
               t => t.GetAllServicesFromLastBill(It.IsAny<int>(), It.IsAny<string>(), It.IsAny<string>())).Returns(listBillingModels);
            
            List<SubscriptionModel> result = _subscriptionFacade.GetSubscribeList(87, "", "", listSubscriptionModels);

            Assert.AreEqual(result.First().ClientId, 87);
        }


        [TestMethod]
        public void GetSubscribeList_NullSubModel()
        {
            _clientConfigFacadeMock.Setup(t => t.GetClientSettings(It.IsAny<int>())).Returns(GetClientSettings());

            var listSubscriptionModels = new List<SubscriptionModel>
            {
                new SubscriptionModel
                {
                    AccountId = "AA",
                    CustomerId = "A",
                    ClientId = 87,
                    ProgramName = "Program1",
                    InsightTypeName = "BillToDate",
                    Channel = "Email",
                    IsSelected = true,
                    ServiceContractId = "AA"
                },
                new SubscriptionModel
                {
                    AccountId = "AA",
                    CustomerId = "A",
                    ClientId = 87,
                    ProgramName = "Program1",
                    InsightTypeName = "BillToDate",
                    Channel = "Email",
                    IsSelected = false,
                    ServiceContractId = "AA"
                }
            };

            _subscriptionMock.Setup(t => t.GetCustomerSubscriptions(It.IsAny<int>(), It.IsAny<string>(), It.IsAny<string>())).Returns(listSubscriptionModels);
            _subscriptionMock.Setup(
                t =>
                    t.MergeDefaultSubscriptionListWithSubscribedPrograms(It.IsAny<List<SubscriptionModel>>(),
                        It.IsAny<List<SubscriptionModel>>())).Returns((List<SubscriptionModel>)null);

            _billingMock.Setup(
               t => t.GetAllServicesFromLastBill(It.IsAny<int>(), It.IsAny<string>(), It.IsAny<string>())).Returns((List<BillingModel>)null);
            
            List<SubscriptionModel> result = _subscriptionFacade.GetSubscribeList(87, "", "", listSubscriptionModels);

            Assert.AreEqual(result.Count, 0);
        }

        [TestMethod]
        public void GetSubscribeList_CoverIfCondtions()
        {
            _clientConfigFacadeMock.Setup(t => t.GetClientSettings(It.IsAny<int>())).Returns(GetClientSettings());

            var listSubscriptionModels = new List<SubscriptionModel>
            {
                new SubscriptionModel
                {
                    AccountId = "Acc500",
                    CustomerId = "Cust900",
                    ClientId = 87,
                    ProgramName = "Program1",
                    InsightTypeName = "BillToDate",
                    Channel = "SMS",
                    IsSelected = true,
                    ServiceContractId = "AA"
                },
                new SubscriptionModel
                {
                    AccountId = "Acc500",
                    CustomerId = "Cust900",
                    ClientId = 87,
                    ProgramName = "Program1",
                    InsightTypeName = "BillToDate",
                    Channel = "Email",
                    IsSelected = false,
                    ServiceContractId = "AA"
                }
            };
            List<BillingModel> listBillingModels = new List<BillingModel>
            {
                new BillingModel
                {
                    ClientId = 87,
                    MeterId = "METER02",
                    CustomerId = "Cust900",
                    AccountId = "Acc500",
                    ServiceContractId = "A",
                    MeterType = "ami",
                    StartDate = DateTime.Now.AddDays(-5),
                    EndDate = DateTime.Now.AddDays(-2),
                    CommodityId = 2,
                    TotalUsage = 100,
                    TotalCost = 100,
                    RateClass = "C"
                },
                new BillingModel
                {
                    ClientId = 87,
                    MeterId = "METER02",
                    CustomerId = "A",
                    AccountId = "A",
                    ServiceContractId = "A",
                    MeterType = "nonmetered",
                    StartDate = DateTime.Now.AddDays(-5),
                    EndDate = DateTime.Now.AddDays(-2),
                    CommodityId = 2,
                    TotalUsage = 100,
                    TotalCost = 100,
                    RateClass = "C"
                }
            };

            _subscriptionMock.Setup(t => t.GetCustomerSubscriptions(It.IsAny<int>(), It.IsAny<string>(), It.IsAny<string>())).Returns(listSubscriptionModels);
            _subscriptionMock.Setup(
                t =>
                    t.MergeDefaultSubscriptionListWithSubscribedPrograms(It.IsAny<List<SubscriptionModel>>(),
                        It.IsAny<List<SubscriptionModel>>())).Returns(listSubscriptionModels);

            _billingMock.Setup(
               t => t.GetAllServicesFromLastBill(It.IsAny<int>(), It.IsAny<string>(), It.IsAny<string>())).Returns(listBillingModels);
            
            List<SubscriptionModel> result = _subscriptionFacade.GetSubscribeList(87, "Cust900", "Acc500", listSubscriptionModels);

            Assert.AreEqual(result.Count, 5);
        }


        [TestMethod]
        public void GetSubscribeList_CoverElseCondtions()
        {
            _clientConfigFacadeMock.Setup(t => t.GetClientSettings(It.IsAny<int>())).Returns(GetClientSettings());

            var listSubscriptionModels = new List<SubscriptionModel>
            {
                new SubscriptionModel
                {
                    AccountId = "Acc500",
                    CustomerId = "Cust900",
                    ClientId = 87,
                    ProgramName = "Program1",
                    InsightTypeName = "BillToDate",
                    Channel = "SMS",
                    IsSelected = true
                },
                new SubscriptionModel
                {
                    AccountId = "Acc500",
                    CustomerId = "Cust900",
                    ClientId = 87,
                    ProgramName = "Program1",
                    InsightTypeName = "BillToDate",
                    Channel = "Email",
                    IsSelected = false,
                }
            };
            List<BillingModel> listBillingModels = new List<BillingModel>
            {
                new BillingModel
                {
                    ClientId = 87,
                    MeterId = "METER02",
                    CustomerId = "Cust900",
                    AccountId = "Acc500",
                    ServiceContractId = "A",
                    MeterType = "ami",
                    StartDate = DateTime.Now.AddDays(-5),
                    EndDate = DateTime.Now.AddDays(-2),
                    CommodityId = 2,
                    TotalUsage = 100,
                    TotalCost = 100,
                    RateClass = "C"
                },
                new BillingModel
                {
                    ClientId = 87,
                    MeterId = "METER02",
                    CustomerId = "A",
                    AccountId = "A",
                    ServiceContractId = "A",
                    MeterType = "nonmetered",
                    StartDate = DateTime.Now.AddDays(-5),
                    EndDate = DateTime.Now.AddDays(-2),
                    CommodityId = 2,
                    TotalUsage = 100,
                    TotalCost = 100,
                    RateClass = "C"
                }
            };

            _subscriptionMock.Setup(t => t.GetCustomerSubscriptions(It.IsAny<int>(), It.IsAny<string>(), It.IsAny<string>())).Returns(listSubscriptionModels);
            _subscriptionMock.Setup(
                t =>
                    t.MergeDefaultSubscriptionListWithSubscribedPrograms(It.IsAny<List<SubscriptionModel>>(),
                        It.IsAny<List<SubscriptionModel>>())).Returns(listSubscriptionModels);

            _billingMock.Setup(
               t => t.GetAllServicesFromLastBill(It.IsAny<int>(), It.IsAny<string>(), It.IsAny<string>())).Returns(listBillingModels);
            
            List<SubscriptionModel> result = _subscriptionFacade.GetSubscribeList(87, "Cust900", "Acc500", listSubscriptionModels);

            Assert.AreEqual(result.Count, 5);
        }
        private static ClientSettings GetClientSettings()
        {
            var clientSettingsJson = new List<ClientConfigurationBulk>
            {
                new ClientConfigurationBulk
                {
                    Category = "system",
                    Key = "aclaraone.clientsettings",
                    Value =
                        "{\r\n \"ClientId\": 87,\r\n \"TimeZone\": \"EST\",\r\n \"IsAmiIntervalStart\": false,\r\n \"IsDstHandled\": false,\r\n \"CalculationScheduleTime\": \"04:00\",\r\n \"NotificationReportScheduleTime\": \"05:00\",\r\n \"InsightReportScheduleTime\": \"06:00\",\r\n \"ForgotPasswordEmailTemplateId\": \"af5c0251-486f-4100-a4cf-2d91a905f108\",\r\n \"RegistrationConfirmationEmailTemplateId\": \"7c0657cd-1020-42a4-a281-87db82a7f6c4\",\r\n \"OptInEmailConfirmationTemplateId\": \"777ca9f8-bb03-490b-946b-b1e6991b2b15\",\r\n \"OptInSmsConfirmationTemplateId\": \"87_12\",\r\n \"OptInEmailUserName\": \"aclaradev\",\r\n \"OptInEmailPassword\": \"Xaclaradev2311X\",\r\n \"OptInEmailFrom\": \"support@aclarax.com\",\r\n \"TrumpiaShortCode\": \"99000\",\r\n \"TrumpiaApiKey\": \"b4c1971153b3509b6ec0d8a24a33454c\",\r\n \"TrumpiaUserName\": \"aclaradev\",\r\n \"TrumpiaContactList\": \"AclaraDemoDev\",\r\n \"UnmaskedAccountIdEndingDigit\" : \"4\",\r\n \"Programs\": [\r\n  {\r\n   \"ProgramName\": \"Program1\",\r\n   \"UtilityProgramName\": \"Program1\",\r\n   \"FileExportRequired\": true,\r\n   \"DoubleOptInRequired\": true,\r\n   \"TrumpiaKeyword\": \"abagdasarian2\",\r\n   \"Insights\": [\r\n    {\r\n     \"InsightName\": \"BillToDate\",\r\n     \"UtilityInsightName\": \"BillToDate\",\r\n     \"AllowedCommunicationChannel\": \"File,Email,SMS,EmailandSMS\",\r\n     \"DefaultCommunicationChannel\": \"Email\",\r\n     \"DefaultFrequency\": \"Daily\",\r\n     \"DefaultValue\": \"\",\r\n     \"EmailTemplateId\": \"c8d5e21f-1150-4b3d-b4b8-ad869d89a040\",\r\n     \"SMSTemplateId\": \"87_1\",\r\n     \"NotifyTime\": \"20:00\",\r\n     \"NotificationDay\": \"Monday\",\r\n     \"TrumpiaKeyword\": \"\",\r\n     \"Level\": \"Account\",\r\n     \"CommodityType\": \"\",\r\n     \"ThresholdType\": \"\",\r\n     \"ThresholdMin\": \"\",\r\n     \"ThresholdMax\": \"\"\r\n    }, {\r\n     \"InsightName\": \"AccountProjectedCost\",\r\n     \"UtilityInsightName\": \"AccountProjectedCost\",\r\n     \"AllowedCommunicationChannel\": \"File,Email,SMS,EmailandSMS\",\r\n     \"DefaultCommunicationChannel\": \"Email\",\r\n     \"DefaultFrequency\": \"Daily\",\r\n     \"DefaultValue\": \"\",\r\n     \"EmailTemplateId\": \"7367b661-0dfd-4eb2-a0d8-0735a459fcb5\",\r\n     \"SMSTemplateId\": \"87_3\",\r\n     \"NotifyTime\": \"20:00\",\r\n     \"NotificationDay\": \"Monday\",\r\n     \"TrumpiaKeyword\": \"\",\r\n     \"Level\": \"Account\",\r\n     \"CommodityType\": \"\",\r\n     \"ThresholdType\": \"\",\r\n     \"ThresholdMin\": \"\",\r\n     \"ThresholdMax\": \"\"\r\n    }, {\r\n     \"InsightName\": \"AccountLevelCostThreshold\",\r\n     \"UtilityInsightName\": \"AccountLevelCostThreshold\",\r\n     \"AllowedCommunicationChannel\": \"File,Email,SMS,EmailandSMS\",\r\n     \"DefaultCommunicationChannel\": \"Email\",\r\n     \"DefaultFrequency\": \"Immediate\",\r\n     \"DefaultValue\": \"100\",\r\n     \"EmailTemplateId\": \"7b3f9981-ae52-4fd9-b223-8ae516f6f82f\",\r\n     \"SMSTemplateId\": \"87_6\",\r\n     \"NotifyTime\": \"\",\r\n     \"NotificationDay\": \"\",\r\n     \"TrumpiaKeyword\": \"\",\r\n     \"Level\": \"Account\",\r\n     \"CommodityType\": \"\",\r\n     \"ThresholdType\": \"Cost\",\r\n     \"ThresholdMin\": \"1\",\r\n     \"ThresholdMax\": \"1000000\"\r\n    }, {\r\n     \"InsightName\": \"ServiceLevelCostThreshold\",\r\n     \"UtilityInsightName\": \"ServiceLevelCostThreshold\",\r\n     \"AllowedCommunicationChannel\": \"File,Email,SMS,EmailandSMS\",\r\n     \"DefaultCommunicationChannel\": \"Email\",\r\n     \"DefaultFrequency\": \"Immediate\",\r\n     \"DefaultValue\": \"25,25\",\r\n     \"EmailTemplateId\": \"87791eaa-0342-45d5-85d4-4645f7a85f7d\",\r\n     \"SMSTemplateId\": \"87_8\",\r\n     \"NotifyTime\": \"\",\r\n     \"NotificationDay\": \"\",\r\n     \"TrumpiaKeyword\": \"\",\r\n     \"Level\": \"Service\",\r\n     \"CommodityType\": \"Gas,Electric\",\r\n     \"ThresholdType\": \"Cost\",\r\n     \"ThresholdMin\": \"1\",\r\n     \"ThresholdMax\": \"1000000\"\r\n    }, {\r\n     \"InsightName\": \"ServiceLevelUsageThreshold\",\r\n     \"UtilityInsightName\": \"ServiceLevelUsageThreshold\",\r\n     \"AllowedCommunicationChannel\": \"File,Email,SMS,EmailandSMS\",\r\n     \"DefaultCommunicationChannel\": \"Email\",\r\n     \"DefaultFrequency\": \"Immediate\",\r\n     \"DefaultValue\": \"25,500\",\r\n     \"EmailTemplateId\": \"f7791cc2-9665-4d4b-8a7f-9421daf55e29\",\r\n     \"SMSTemplateId\": \"87_7\",\r\n     \"NotifyTime\": \"\",\r\n     \"NotificationDay\": \"\",\r\n     \"TrumpiaKeyword\": \"\",\r\n     \"Level\": \"Service\",\r\n     \"CommodityType\": \"Gas,Electric\",\r\n     \"ThresholdType\": \"Usage\",\r\n     \"ThresholdMin\": \"0.1\",\r\n     \"ThresholdMax\": \"1000000\"\r\n    }, {\r\n     \"InsightName\": \"CostToDate\",\r\n     \"UtilityInsightName\": \"CostToDate\",\r\n     \"AllowedCommunicationChannel\": \"File,Email,SMS,EmailandSMS\",\r\n     \"DefaultCommunicationChannel\": \"Email\",\r\n     \"DefaultFrequency\": \"Weekly\",\r\n     \"DefaultValue\": \"\",\r\n     \"EmailTemplateId\": \"b4425b3d-de12-4694-8751-edb41bc5cd79\",\r\n     \"SMSTemplateId\": \"87_2\",\r\n     \"NotifyTime\": \"20:00\",\r\n     \"NotificationDay\": \"Monday\",\r\n     \"TrumpiaKeyword\": \"\",\r\n     \"Level\": \"Service\",\r\n     \"CommodityType\": \"Gas,Electric\",\r\n     \"ThresholdType\": \"\",\r\n     \"ThresholdMin\": \"\",\r\n     \"ThresholdMax\": \"\"\r\n    }, {\r\n     \"InsightName\": \"ServiceProjectedCost\",\r\n     \"UtilityInsightName\": \"ServiceProjectedCost\",\r\n     \"AllowedCommunicationChannel\": \"File,Email,SMS,EmailandSMS\",\r\n     \"DefaultCommunicationChannel\": \"Email\",\r\n     \"DefaultFrequency\": \"Daily\",\r\n     \"DefaultValue\": \"\",\r\n     \"EmailTemplateId\": \"6bd40b25-fc86-480f-a1d4-0f2a22b7d270\",\r\n     \"SMSTemplateId\": \"87_4\",\r\n     \"NotifyTime\": \"20:00\",\r\n     \"NotificationDay\": \"Monday\",\r\n     \"TrumpiaKeyword\": \"\",\r\n     \"Level\": \"Service\",\r\n     \"CommodityType\": \"Gas,Electric\",\r\n     \"ThresholdType\": \"\",\r\n     \"ThresholdMin\": \"\",\r\n     \"ThresholdMax\": \"\"\r\n    }, {\r\n     \"InsightName\": \"Usage\",\r\n     \"UtilityInsightName\": \"Usage\",\r\n     \"AllowedCommunicationChannel\": \"File,Email,SMS,EmailandSMS\",\r\n     \"DefaultCommunicationChannel\": \"Email\",\r\n     \"DefaultFrequency\": \"Daily\",\r\n     \"DefaultValue\": \"\",\r\n     \"EmailTemplateId\": \"3fdb3336-0192-4bb9-bc53-8810dd110a3a\",\r\n     \"SMSTemplateId\": \"87_5\",\r\n     \"NotifyTime\": \"20:00\",\r\n     \"NotificationDay\": \"Monday\",\r\n     \"TrumpiaKeyword\": \"\",\r\n     \"Level\": \"Service\",\r\n     \"CommodityType\": \"Gas,Electric\",\r\n     \"ThresholdType\": \"\",\r\n     \"ThresholdMin\": \"\",\r\n     \"ThresholdMax\": \"\"\r\n    }, {\r\n     \"InsightName\": \"DayThreshold\",\r\n     \"UtilityInsightName\": \"DayThreshold\",\r\n     \"AllowedCommunicationChannel\": \"File,Email,SMS,EmailandSMS\",\r\n     \"DefaultCommunicationChannel\": \"Email\",\r\n     \"DefaultFrequency\": \"Immediate\",\r\n     \"DefaultValue\": \"1,16\",\r\n     \"EmailTemplateId\": \"ce508747-444b-4e1c-9f39-53629e47260b\",\r\n     \"SMSTemplateId\": \"87_9\",\r\n     \"NotifyTime\": \"\",\r\n     \"NotificationDay\": \"\",\r\n     \"TrumpiaKeyword\": \"\",\r\n     \"Level\": \"Service\",\r\n     \"CommodityType\": \"Gas,Electric\",\r\n     \"ThresholdType\": \"Usage\",\r\n     \"ThresholdMin\": \"0.1\",\r\n     \"ThresholdMax\": \"1000000\"\r\n    }, {\r\n     \"InsightName\": \"ServiceLevelTieredThresholdApproaching\",\r\n     \"UtilityInsightName\": \"ServiceLevelTieredThresholdApproaching\",\r\n     \"AllowedCommunicationChannel\": \"File,Email,SMS,EmailandSMS\",\r\n     \"DefaultCommunicationChannel\": \"Email\",\r\n     \"DefaultFrequency\": \"Immediate\",\r\n     \"DefaultValue\": \"20,20\",\r\n     \"EmailTemplateId\": \"4962e788-fb1e-43cf-a2f6-faf713d25e0c\",\r\n     \"SMSTemplateId\": \"87_10\",\r\n     \"NotifyTime\": \"\",\r\n     \"NotificationDay\": \"\",\r\n     \"TrumpiaKeyword\": \"\",\r\n     \"Level\": \"Service\",\r\n     \"CommodityType\": \"Gas,Electric\",\r\n     \"ThresholdType\": \"Percent\",\r\n     \"ThresholdMin\": \"1\",\r\n     \"ThresholdMax\": \"100\"\r\n    }, {\r\n     \"InsightName\": \"ServiceLevelTieredThresholdExceed\",\r\n     \"UtilityInsightName\": \"ServiceLevelTieredThresholdExceed\",\r\n     \"AllowedCommunicationChannel\": \"File,Email,SMS,EmailandSMS\",\r\n     \"DefaultCommunicationChannel\": \"Email\",\r\n     \"DefaultFrequency\": \"Immediate\",\r\n     \"DefaultValue\": \"\",\r\n     \"EmailTemplateId\": \"c1dc511c-0dd7-4208-bad7-8729fa097194\",\r\n     \"SMSTemplateId\": \"87_11\",\r\n     \"NotifyTime\": \"\",\r\n     \"NotificationDay\": \"\",\r\n     \"TrumpiaKeyword\": \"\",\r\n     \"Level\": \"Service\",\r\n     \"CommodityType\": \"Gas,Electric\",\r\n     \"ThresholdType\": \"\",\r\n     \"ThresholdMin\": \"\",\r\n     \"ThresholdMax\": \"\"\r\n    }\r\n   ]\r\n  }, {\r\n   \"ProgramName\": \"Program2\",\r\n   \"UtilityProgramName\": \"Program2\",\r\n   \"FileExportRequired\": false,\r\n   \"DoubleOptInRequired\": true,\r\n   \"TrumpiaKeyword\": \"\",\r\n   \"Insights\": [\r\n    {\r\n     \"InsightName\": \"ServiceLevelCostThreshold\",\r\n     \"UtilityInsightName\": \"ServiceLevelCostThreshold\",\r\n     \"AllowedCommunicationChannel\": \"File,Email,SMS,EmailandSMS\",\r\n     \"DefaultCommunicationChannel\": \"Email\",\r\n     \"DefaultFrequency\": \"Immediate\",\r\n     \"DefaultValue\": \"25\",\r\n     \"EmailTemplateId\": \"87791eaa-0342-45d5-85d4-4645f7a85f7d\",\r\n     \"SMSTemplateId\": \"87_8\",\r\n     \"NotifyTime\": \"\",\r\n     \"NotificationDay\": \"\",\r\n     \"TrumpiaKeyword\": \"\",\r\n     \"Level\": \"Service\",\r\n     \"CommodityType\": \"Water\",\r\n     \"ThresholdType\": \"Cost\",\r\n     \"ThresholdMin\": \"1\",\r\n     \"ThresholdMax\": \"1000000\"\r\n    }, {\r\n     \"InsightName\": \"ServiceLevelUsageThreshold\",\r\n     \"UtilityInsightName\": \"ServiceLevelUsageThreshold\",\r\n     \"AllowedCommunicationChannel\": \"File,Email,SMS,EmailandSMS\",\r\n     \"DefaultCommunicationChannel\": \"Email\",\r\n     \"DefaultFrequency\": \"Immediate\",\r\n     \"DefaultValue\": \"5\",\r\n     \"EmailTemplateId\": \"f7791cc2-9665-4d4b-8a7f-9421daf55e29\",\r\n     \"SMSTemplateId\": \"87_7\",\r\n     \"NotifyTime\": \"\",\r\n     \"NotificationDay\": \"\",\r\n     \"TrumpiaKeyword\": \"\",\r\n     \"Level\": \"Service\",\r\n     \"CommodityType\": \"Water\",\r\n     \"ThresholdType\": \"Usage\",\r\n     \"ThresholdMin\": \"0.1\",\r\n     \"ThresholdMax\": \"1000000\"\r\n    }, {\r\n     \"InsightName\": \"CostToDate\",\r\n     \"UtilityInsightName\": \"CostToDate\",\r\n     \"AllowedCommunicationChannel\": \"File,Email,SMS,EmailandSMS\",\r\n     \"DefaultCommunicationChannel\": \"Email\",\r\n     \"DefaultFrequency\": \"Weekly\",\r\n     \"DefaultValue\": \"\",\r\n     \"EmailTemplateId\": \"b4425b3d-de12-4694-8751-edb41bc5cd79\",\r\n     \"SMSTemplateId\": \"87_2\",\r\n     \"NotifyTime\": \"20:00\",\r\n     \"NotificationDay\": \"Monday\",\r\n     \"TrumpiaKeyword\": \"\",\r\n     \"Level\": \"Service\",\r\n     \"CommodityType\": \"Water\",\r\n     \"ThresholdType\": \"\",\r\n     \"ThresholdMin\": \"\",\r\n     \"ThresholdMax\": \"\"\r\n    }, {\r\n     \"InsightName\": \"ServiceProjectedCost\",\r\n     \"UtilityInsightName\": \"ServiceProjectedCost\",\r\n     \"AllowedCommunicationChannel\": \"File,Email,SMS,EmailandSMS\",\r\n     \"DefaultCommunicationChannel\": \"Email\",\r\n     \"DefaultFrequency\": \"Daily\",\r\n     \"DefaultValue\": \"\",\r\n     \"EmailTemplateId\": \"6bd40b25-fc86-480f-a1d4-0f2a22b7d270\",\r\n     \"SMSTemplateId\": \"87_4\",\r\n     \"NotifyTime\": \"20:00\",\r\n     \"NotificationDay\": \"Monday\",\r\n     \"TrumpiaKeyword\": \"\",\r\n     \"Level\": \"Service\",\r\n     \"CommodityType\": \"Water\",\r\n     \"ThresholdType\": \"\",\r\n     \"ThresholdMin\": \"\",\r\n     \"ThresholdMax\": \"\"\r\n    }, {\r\n     \"InsightName\": \"Usage\",\r\n     \"UtilityInsightName\": \"Usage\",\r\n     \"AllowedCommunicationChannel\": \"File,Email,SMS,EmailandSMS\",\r\n     \"DefaultCommunicationChannel\": \"Email\",\r\n     \"DefaultFrequency\": \"Daily\",\r\n     \"DefaultValue\": \"\",\r\n     \"EmailTemplateId\": \"3fdb3336-0192-4bb9-bc53-8810dd110a3a\",\r\n     \"SMSTemplateId\": \"87_5\",\r\n     \"NotifyTime\": \"20:00\",\r\n     \"NotificationDay\": \"Monday\",\r\n     \"TrumpiaKeyword\": \"\",\r\n     \"Level\": \"Service\",\r\n     \"CommodityType\": \"Water\",\r\n     \"ThresholdType\": \"\",\r\n     \"ThresholdMin\": \"\",\r\n     \"ThresholdMax\": \"\"\r\n    }, {\r\n     \"InsightName\": \"DayThreshold\",\r\n     \"UtilityInsightName\": \"DayThreshold\",\r\n     \"AllowedCommunicationChannel\": \"File,Email,SMS,EmailandSMS\",\r\n     \"DefaultCommunicationChannel\": \"Email\",\r\n     \"DefaultFrequency\": \"Immediate\",\r\n     \"DefaultValue\": \"0.15\",\r\n     \"EmailTemplateId\": \"ce508747-444b-4e1c-9f39-53629e47260b\",\r\n     \"SMSTemplateId\": \"87_9\",\r\n     \"NotifyTime\": \"\",\r\n     \"NotificationDay\": \"\",\r\n     \"TrumpiaKeyword\": \"\",\r\n     \"Level\": \"Service\",\r\n     \"CommodityType\": \"Water\",\r\n     \"ThresholdType\": \"Usage\",\r\n     \"ThresholdMin\": \"0.1\",\r\n     \"ThresholdMax\": \"1000000\"\r\n    }, {\r\n     \"InsightName\": \"ServiceLevelTieredThresholdApproaching\",\r\n     \"UtilityInsightName\": \"ServiceLevelTieredThresholdApproaching\",\r\n     \"AllowedCommunicationChannel\": \"File,Email,SMS,EmailandSMS\",\r\n     \"DefaultCommunicationChannel\": \"Email\",\r\n     \"DefaultFrequency\": \"Immediate\",\r\n     \"DefaultValue\": \"20\",\r\n     \"EmailTemplateId\": \"4962e788-fb1e-43cf-a2f6-faf713d25e0c\",\r\n     \"SMSTemplateId\": \"87_10\",\r\n     \"NotifyTime\": \"\",\r\n     \"NotificationDay\": \"\",\r\n     \"TrumpiaKeyword\": \"\",\r\n     \"Level\": \"Service\",\r\n     \"CommodityType\": \"Water\",\r\n     \"ThresholdType\": \"Percent\",\r\n     \"ThresholdMin\": \"1\",\r\n     \"ThresholdMax\": \"100\"\r\n    }, {\r\n     \"InsightName\": \"ServiceLevelTieredThresholdExceed\",\r\n     \"UtilityInsightName\": \"ServiceLevelTieredThresholdExceed\",\r\n     \"AllowedCommunicationChannel\": \"File,Email,SMS,EmailandSMS\",\r\n     \"DefaultCommunicationChannel\": \"Email\",\r\n     \"DefaultFrequency\": \"Immediate\",\r\n     \"DefaultValue\": \"\",\r\n     \"EmailTemplateId\": \"c1dc511c-0dd7-4208-bad7-8729fa097194\",\r\n     \"SMSTemplateId\": \"87_11\",\r\n     \"NotifyTime\": \"\",\r\n     \"NotificationDay\": \"\",\r\n     \"TrumpiaKeyword\": \"\",\r\n     \"Level\": \"Service\",\r\n     \"CommodityType\": \"Water\",\r\n     \"ThresholdType\": \"\",\r\n     \"ThresholdMin\": \"\",\r\n     \"ThresholdMax\": \"\"\r\n    }\r\n   ]\r\n  }\r\n ]\r\n}"
                }
            };
            return JsonConvert.DeserializeObject<ClientSettings>(clientSettingsJson[0].Value);
        }

        [TestMethod]
        public void DoubleOptin()
        {
            
            _clientConfigFacadeMock.Setup(t => t.GetClientSettings(It.IsAny<int>())).Returns(GetClientSettings());
            CassandraRepositoryMock.Setup(t => t.GetSingleAsync(It.IsAny<string>(), It.IsAny<Expression<Func<CustomerEntity, bool>>>())).Returns(Task.FromResult(new CustomerEntity()));
            //_customerMock.Setup(t => t.GetCustomerAsync(It.IsAny<int>(), It.IsAny<string>())).Returns(Task.FromResult(new CustomerModel()));

            _subscriptionMock.Setup(
                t =>
                    t.GetSubscriptionDetails(It.IsAny<int>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(),
                        It.IsAny<string>(), It.IsAny<string>())).Returns(new SubscriptionModel
                        {
                            AccountId = "AA",
                            CustomerId = "A",
                            ClientId = 87,
                            ProgramName = "Program1",
                            InsightTypeName = "BillToDate",
                            Channel = "Email",
                            IsSelected = true,
                            ServiceContractId = "AA"
                        });
            
            _subscriptionFacade.DoubleOptin(new SubscriptionModel
            {
                AccountId = "AA",
                CustomerId = "A",
                ClientId = 87,
                ProgramName = "Program1",
                InsightTypeName = "BillToDate",
                Channel = "Email",
                IsSelected = true,
                ServiceContractId = "AA",
            });
        }

        [TestMethod]
        public void DoubleOptin_Ifs()
        {
            _clientConfigFacadeMock.Setup(t => t.GetClientSettings(It.IsAny<int>())).Returns(GetClientSettings());
            CassandraRepositoryMock.Setup(
                t => t.GetSingleAsync(It.IsAny<string>(), It.IsAny<Expression<Func<CustomerEntity, bool>>>()))
                .Returns(Task.FromResult(new CustomerEntity()
                {
                    EmailAddress = "t@gmail.com",
                    AddressLine1 = "Add1",
                    AddressLine2 = "Add2",
                    AddressLine3 = "Add3",
                    TrumpiaSubscriptionId = "Sub123"
                }));
            //_customerMock.Setup(t => t.GetCustomerAsync(It.IsAny<int>(), It.IsAny<string>())).Returns(Task.FromResult(new CustomerModel()
            //{
            //    EmailAddress = "t@gmail.com",
            //    AddressLine1 = "Add1",
            //    AddressLine2 = "Add2",
            //    AddressLine3 = "Add3",
            //    TrumpiaSubscriptionId = "Sub123"
            //}));

            _subscriptionMock.Setup(
                t =>
                    t.GetSubscriptionDetails(It.IsAny<int>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(),
                        It.IsAny<string>(), It.IsAny<string>())).Returns(new SubscriptionModel
                        {
                            AccountId = "AC900Cust",
                            CustomerId = "A",
                            ClientId = 87,
                            ProgramName = "Program1",
                            InsightTypeName = "BillToDate",
                            Channel = "Email",
                            IsSelected = true,
                            ServiceContractId = "AA"
                        });
            
            _subscriptionFacade.DoubleOptin(new SubscriptionModel
            {
                AccountId = "AC900Cust ",
                CustomerId = "A",
                ClientId = 87,
                ProgramName = "Program1",
                InsightTypeName = "BillToDate",
                Channel = "Email",
                IsSelected = true,
                ServiceContractId = "AA",
                IsEmailOptInCompleted = false,
                IsSmsOptInCompleted = false,
            });


        }

        [TestMethod]
        public void DoubleOptin_Ifs_SendSMS_LogError_PhoneNumCheck()
        {
            _clientConfigFacadeMock.Setup(t => t.GetClientSettings(It.IsAny<int>())).Returns(GetClientSettings());
            CassandraRepositoryMock.Setup(
               t => t.GetSingleAsync(It.IsAny<string>(), It.IsAny<Expression<Func<CustomerEntity, bool>>>()))
               .Returns(Task.FromResult(new CustomerEntity()
               {
                   AddressLine1 = "Add1",
                   AddressLine2 = "Add2",
                   AddressLine3 = "Add3",
                   Phone1 = "9028179781"
               }));
            //_customerMock.Setup(t => t.GetCustomerAsync(It.IsAny<int>(), It.IsAny<string>())).Returns(Task.FromResult(new CustomerModel()
            //{
            //    AddressLine1 = "Add1",
            //    AddressLine2 = "Add2",
            //    AddressLine3 = "Add3",
            //    Phone1 = "9028179781"
            //}));

            _subscriptionMock.Setup(
                t =>
                    t.GetSubscriptionDetails(It.IsAny<int>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(),
                        It.IsAny<string>(), It.IsAny<string>())).Returns(new SubscriptionModel
                        {
                            AccountId = "AC900Cust",
                            CustomerId = "A",
                            ClientId = 87,
                            ProgramName = "Program1",
                            InsightTypeName = "BillToDate",
                            Channel = "Email",
                            IsSelected = true,
                            ServiceContractId = "AA"
                        });
            
            _subscriptionFacade.DoubleOptin(new SubscriptionModel
            {
                AccountId = "AC900Cust ",
                CustomerId = "A",
                ClientId = 87,
                ProgramName = "Program1",
                InsightTypeName = "BillToDate",
                Channel = "Email",
                IsSelected = true,
                ServiceContractId = "AA",
                IsEmailOptInCompleted = false,
                IsSmsOptInCompleted = false,
            });
        }


        [TestMethod]
        public void DoubleOptin_Ifs_SendSMS_LogError()
        {
            _clientConfigFacadeMock.Setup(t => t.GetClientSettings(It.IsAny<int>())).Returns(GetClientSettings());
            CassandraRepositoryMock.Setup(
               t => t.GetSingleAsync(It.IsAny<string>(), It.IsAny<Expression<Func<CustomerEntity, bool>>>()))
               .Returns(Task.FromResult(new CustomerEntity()
               {
                   EmailAddress = "t@gmail.com",
                   AddressLine1 = "Add1",
                   AddressLine2 = "Add2",
                   AddressLine3 = "Add3"
               }));
            //_customerMock.Setup(t => t.GetCustomerAsync(It.IsAny<int>(), It.IsAny<string>())).Returns(Task.FromResult(new CustomerModel()
            //{
            //    EmailAddress = "t@gmail.com",
            //    AddressLine1 = "Add1",
            //    AddressLine2 = "Add2",
            //    AddressLine3 = "Add3"
            //}));

            _subscriptionMock.Setup(
                t =>
                    t.GetSubscriptionDetails(It.IsAny<int>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(),
                        It.IsAny<string>(), It.IsAny<string>())).Returns(new SubscriptionModel
                        {
                            AccountId = "AC900Cust",
                            CustomerId = "A",
                            ClientId = 87,
                            ProgramName = "Program1",
                            InsightTypeName = "BillToDate",
                            Channel = "Email",
                            IsSelected = true,
                            ServiceContractId = "AA"
                        });
            
            _subscriptionFacade.DoubleOptin(new SubscriptionModel
            {
                AccountId = "AC900Cust ",
                CustomerId = "A",
                ClientId = 87,
                ProgramName = "Program1",
                InsightTypeName = "BillToDate",
                Channel = "Email",
                IsSelected = true,
                ServiceContractId = "AA",
                IsEmailOptInCompleted = false,
                IsSmsOptInCompleted = false,
            });


        }


        [TestMethod]
        public void DoubleOptin_Sms_RegisterNonBlockNumberTest()
        {

            var customerEntity = new CustomerEntity
            {
                AddressLine1 = "1",
                AddressLine2 = "2",
                AddressLine3 = "3",
                City = "P",
                ClientId = 87,
                Country = "In",
                CustomerId = "A",
                CustomerType = "spl",
                EmailAddress = "gmail",
                FirstName = "tush",
                IsBusiness = true,
                LastName = "W",
                Phone1 = "1234567890",
                Phone2 = "",
                PostalCode = "02481",
                Source = "Branch",
                State = "MA"
            };

            SmsTemplateModel smsTemplate = new SmsTemplateModel()
            {
                ClientId = 87,
                InsightTypeName = "OptInConfirmation",
                Body = "Body-Sample-Data",
                TemplateId = "12"
            };

            var expected = string.Empty;
            
            _clientConfigFacadeMock.Setup(t => t.GetClientSettings(It.IsAny<int>())).Returns(GetClientSettings());
            CassandraRepositoryMock.Setup(t => t.GetSingleAsync(It.IsAny<string>(), It.IsAny<Expression<Func<CustomerEntity, bool>>>())).Returns(Task.FromResult(customerEntity));
            CassandraRepositoryMock.Setup(t => t.InsertOrUpdate(It.Is<CustomerEntity>(c => c.LastModifiedDate > DateTime.MinValue), It.IsAny<string>())).Returns(true).Verifiable();
   
            _sendSmsMock.Setup(t => t.CreateNewTrumpiaSubscription(It.IsAny<CustomerModel>(), It.IsAny<string>(),
                It.IsAny<string>(), It.IsAny<string>())).Returns("TestRequestId");
            _sendSmsMock.Setup(t => t.GetSubscriptionId(It.IsAny<CustomerModel>(), It.IsAny<string>(),
                It.IsAny<string>(), It.IsAny<string>())).Returns("testSubscriptionId");
            _sendSmsMock.Setup(
                t =>
                    t.Send(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(),
                        It.IsAny<string>(), It.IsAny<string>(),It.IsAny<int>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(),
                        It.IsAny<string>(), It.IsAny<DateTime>(), It.IsAny<DateTime>()))
                .Returns(Task.FromResult("TestRequestId2"));
            _notificationMock.Setup(m => m.GetSmsTemplateFromDB(It.IsAny<int>(), It.IsAny<string>(), It.IsAny<string>()))
                .Returns(Task.FromResult(smsTemplate));
            _subscriptionMock.Setup(
                t =>
                    t.GetSubscriptionDetails(It.IsAny<int>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(),
                        It.IsAny<string>(), It.IsAny<string>())).Returns(new SubscriptionModel
                        {
                            AccountId = "AA",
                            CustomerId = "A",
                            ClientId = 87,
                            ProgramName = "Program1",
                            InsightTypeName = "BillToDate",
                            Channel = "Sms",
                            IsSelected = true,
                            ServiceContractId = "AA",
                            IsEmailOptInCompleted = true
                        });
            _subscriptionMock.Setup(
                t => t.InsertSubscriptionAsync(It.IsAny<SubscriptionModel>(), It.IsAny<bool>(), It.IsAny<bool>()))
                .Returns(Task.FromResult(true));

            var result = _subscriptionFacade.DoubleOptin(new SubscriptionModel
            {
                AccountId = "AA",
                CustomerId = "A",
                ClientId = 87,
                ProgramName = "Program1",
                InsightTypeName = "BillToDate",
                Channel = "Sms",
                IsSelected = true,
                ServiceContractId = "AA",
                IsEmailOptInCompleted = true,
                IsSmsOptInCompleted = false
            });

            Assert.IsNull(result);
        }

        [TestMethod]
        public void DoubleOptin_Sms_RegisterBlockNumberTest()
        {
            var requestId = "TestRequestId";
            var customerEntity = new CustomerEntity
            {
                AddressLine1 = "1",
                AddressLine2 = "2",
                AddressLine3 = "3",
                City = "P",
                ClientId = 87,
                Country = "In",
                CustomerId = "A",
                CustomerType = "spl",
                EmailAddress = "gmail",
                FirstName = "tush",
                IsBusiness = true,
                LastName = "W",
                Phone1 = "1234567890",
                Phone2 = "",
                PostalCode = "02481",
                Source = "Branch",
                State = "MA"
            };
            
            var trumpiaDetail = new TrumpiaRequestDetailModel
            {
                ErrorMessage = "Blocked tools : 1234567890",
                StatusCode = "MPSE0501",
                RequestId = requestId
            };

            var expected = "MPSE0501";
            _clientConfigFacadeMock.Setup(t => t.GetClientSettings(It.IsAny<int>())).Returns(GetClientSettings());
            CassandraRepositoryMock.Setup(t => t.GetSingleAsync(It.IsAny<string>(), It.IsAny<Expression<Func<CustomerEntity, bool>>>())).Returns(Task.FromResult(customerEntity));
            CassandraRepositoryMock.Setup(t => t.InsertOrUpdate(It.Is<CustomerEntity>(c => c.LastModifiedDate > DateTime.MinValue), It.IsAny<string>())).Returns(true).Verifiable();

            _sendSmsMock.Setup(t => t.CreateNewTrumpiaSubscription(It.IsAny<CustomerModel>(), It.IsAny<string>(),
                It.IsAny<string>(), It.IsAny<string>())).Returns(requestId);
            _sendSmsMock.Setup(t => t.GetSubscriptionId(It.IsAny<CustomerModel>(), It.IsAny<string>(),
                It.IsAny<string>(), It.IsAny<string>())).Returns(string.Empty);
            _subscriptionMock.Setup(
                t =>
                    t.GetSubscriptionDetails(It.IsAny<int>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(),
                        It.IsAny<string>(), It.IsAny<string>())).Returns(new SubscriptionModel
                        {
                            AccountId = "AA",
                            CustomerId = "A",
                            ClientId = 87,
                            ProgramName = "Program1",
                            InsightTypeName = "BillToDate",
                            Channel = "Sms",
                            IsSelected = true,
                            ServiceContractId = "AA",
                            IsEmailOptInCompleted = true
                        });
            _subscriptionMock.Setup(
                t => t.InsertSubscriptionAsync(It.IsAny<SubscriptionModel>(), It.IsAny<bool>(), It.IsAny<bool>()))
                .Returns(Task.FromResult(true));
            _trumpiaRequestDetailMock.Setup(t => t.GetTrumpiaRequestDetail(It.IsAny<string>())).Returns(trumpiaDetail);

            var result = _subscriptionFacade.DoubleOptin(new SubscriptionModel
            {
                AccountId = "AA",
                CustomerId = "A",
                ClientId = 87,
                ProgramName = "Program1",
                InsightTypeName = "BillToDate",
                Channel = "Sms",
                IsSelected = true,
                ServiceContractId = "AA",
                IsEmailOptInCompleted = true,
                IsSmsOptInCompleted = false
            });

            Assert.AreEqual(expected, result.StatusCode);
        }
        /// <summary>
        /// Cleanup Method To Release Log4Net Data Resources hold by Any Test Method
        /// </summary>
        [TestCleanup]
        public void CleanUpMethod()
        {
            CallContext.FreeNamedDataSlot("log4net.Util.LogicalThreadContextProperties");
        }
    }
}