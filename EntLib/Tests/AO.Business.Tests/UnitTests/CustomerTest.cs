﻿using System;
using System.Linq.Expressions;
using System.Runtime.Remoting.Messaging;
using System.Threading.Tasks;
using AO.BusinessContracts;
using AO.DataAccess.Contracts;
using AO.Entities;
using CE.AO.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace AO.Business.Tests.UnitTests
{
	
    [TestClass]
    public class CustomerTest : TestBase
    {
        private Customer _customerManager;
        private Mock<ISendSms> _sendSmsManager;

        /// <summary>
        /// Test Case Init
        /// </summary>
        [TestInitialize]
        public void Init()
        {
            _sendSmsManager = new Mock<ISendSms>();
            _customerManager = new Customer(LogModelMock.Object,
                new Lazy<ICassandraRepository>(() => CassandraRepositoryMock.Object),
                new Lazy<ISendSms>(() => _sendSmsManager.Object));
        }

        /// <summary>
        /// Test Case Cleanup
        /// </summary>
        [TestCleanup]
        public void Cleanup()
        {
            CallContext.FreeNamedDataSlot("log4net.Util.LogicalThreadContextProperties");
        }

        /// <summary>
        /// Test Case for accessing GetCustomerAsync
        /// </summary>
        [TestMethod]
        public void GetCustomerAsync()
        {
            var customerEntity = new CustomerEntity
            {
                AddressLine1 = "1",
                AddressLine2 = "2",
                AddressLine3 = "3",
                City = "P",
                ClientId = 1,
                Country = "In",
                CustomerId = "2",
                CustomerType = "spl",
                EmailAddress = "gmail",
                FirstName = "tush",
                IsBusiness = true,
                LastName = "W",
                Phone1 = "89022",
                Phone2 = "",
                PostalCode = "422103",
                Source = "Branch",
                State = "MH",
                TrumpiaRequestDetailId = "25",
                TrumpiaSubscriptionId = "234"
            };

            CassandraRepositoryMock.Setup(t => t.GetSingleAsync(It.IsAny<string>(), It.IsAny<Expression<Func<CustomerEntity, bool>>>())).Returns(Task.FromResult(customerEntity));
            var result = _customerManager.GetCustomerAsync(1, "2").Result;
            Assert.AreEqual(result.ClientId, 1);
            MockRepository.VerifyAll();
        }

        /// <summary>
        /// Test Case for InsertOrMergeCustomerAsync
        /// </summary>
        [TestMethod]
        public void InsertOrMergeCustomerAsync()
        {
            // Arrange
            var customerModel = new CustomerModel
            {
                AddressLine1 = "1",
                AddressLine2 = "2",
                AddressLine3 = "3",
                City = "P",
                ClientId = 1,
                Country = "In",
                CustomerId = "2",
                CustomerType = "spl",
                EmailAddress = "gmail",
                FirstName = "tush",
                IsBusiness = true,
                LastName = "W",
                Phone1 = "89022",
                Phone2 = "",
                PostalCode = "422103",
                Source = "Branch",
                State = "MH",
                TrumpiaRequestDetailId = "25",
                TrumpiaSubscriptionId = "234"
            };

            CassandraRepositoryMock.Setup(t => t.InsertOrUpdate(It.Is<CustomerEntity>(c=>c.LastModifiedDate>DateTime.MinValue), It.IsAny<string>())).Returns(true).Verifiable();

            // Act
            var result = _customerManager.InsertOrMergeCustomerAsync(customerModel).Result;

            // Assert
            Assert.AreEqual(result, true);
            MockRepository.VerifyAll();
        }

        /// <summary>
        /// Test Case for SmsSubscirptionCheckAndUpdate
        /// </summary>
        [TestMethod]
        public void SmsSubscirptionCheckAndUpdate()
        {
            CustomerModel customerModel = new CustomerModel
            {
                AddressLine1 = "1",
                AddressLine2 = "2",
                AddressLine3 = "3",
                City = "P",
                ClientId = 1,
                Country = "In",
                CustomerId = "2",
                CustomerType = "spl",
                EmailAddress = "gmail",
                FirstName = "tush",
                IsBusiness = true,
                LastName = "W",
                Phone1 = "89022",
                Phone2 = "",
                PostalCode = "422103",
                Source = "Branch",
                State = "MH",
                TrumpiaRequestDetailId = "25",
                TrumpiaSubscriptionId = "234"
            };

            bool str = true;
            var result = _customerManager.SmsSubscirptionCheckAndUpdate(customerModel, "1", "ert", "ct", ref str);
            Assert.AreEqual(result, true);
            MockRepository.VerifyAll();
        }

        /// <summary>
        /// Test Case for SmsSubscirptionCheckAndUpdate
        /// </summary>
        [TestMethod]
        public void SmsSubscirptionCheckAndUpdate_LogError()
        {
            CustomerModel customerModel = new CustomerModel
            {
                AddressLine1 = "1",
                AddressLine2 = "2",
                AddressLine3 = "3",
                City = "P",
                ClientId = 1,
                Country = "In",
                CustomerId = "2",
                CustomerType = "spl",
                EmailAddress = "gmail",
                FirstName = "tush",
                IsBusiness = true,
                LastName = "W",
                Phone1 = "",
                Phone2 = "",
                PostalCode = "422103",
                Source = "Branch",
                State = "MH",
                TrumpiaRequestDetailId = "25",
                TrumpiaSubscriptionId = "234"
            };

            bool str = true;
            var result = _customerManager.SmsSubscirptionCheckAndUpdate(customerModel, "1", "ert", "ct", ref str);
            Assert.AreEqual(result, false);
            MockRepository.VerifyAll();
        }

        /// <summary>
        /// Test Case for SmsSubscirptionCheckAndUpdate
        /// </summary>
        [TestMethod]
        public void SmsSubscirptionCheckAndUpdate_StatusCodeCoverage()
        {
            CustomerModel customerModel = new CustomerModel
            {
                AddressLine1 = "1",
                AddressLine2 = "2",
                AddressLine3 = "3",
                City = "P",
                ClientId = 1,
                Country = "In",
                CustomerId = "2",
                CustomerType = "spl",
                EmailAddress = "gmail",
                FirstName = "tush",
                IsBusiness = true,
                LastName = "W",
                Phone1 = "89022",
                Phone2 = "",
                PostalCode = "422103",
                Source = "Branch",
                State = "MH",
                TrumpiaRequestDetailId = "25",
                TrumpiaSubscriptionId = ""
            };

            bool str = true;
            var result = _customerManager.SmsSubscirptionCheckAndUpdate(customerModel, null, null, "ct", ref str);
            Assert.AreEqual(result, true);
            MockRepository.VerifyAll();
        }

        [TestMethod]
        public void CreateSubscriptionAndUpdateCustomer()
        {
            var username = "userName";
            var contactList = "contactList";
            CustomerModel customerModel = new CustomerModel
            {
                AddressLine1 = "1",
                AddressLine2 = "2",
                AddressLine3 = "3",
                City = "P",
                ClientId = 1,
                Country = "In",
                CustomerId = "2",
                CustomerType = "spl",
                EmailAddress = "gmail",
                FirstName = "tush",
                IsBusiness = true,
                LastName = "W",
                Phone1 = "89022",
                Phone2 = "",
                PostalCode = "422103",
                Source = "Branch",
                State = "MH",
                TrumpiaRequestDetailId = "25",
                TrumpiaSubscriptionId = "234"
            };

            _sendSmsManager.Setup(
                mock =>
                    mock.CreateNewTrumpiaSubscription(It.IsAny<CustomerModel>(), It.IsAny<string>(),
                        It.Is<string>(s => s == username), It.Is<string>(s => s == contactList)))
                .Returns("testKeyapiKey")
                .Verifiable();

            var result = _customerManager.CreateSubscriptionAndUpdateCustomer(customerModel, "apiKey", username, contactList);
            Assert.AreEqual(result, true);
            MockRepository.VerifyAll();
        }

        [TestMethod]
        public void CreateSubscriptionAndUpdateCustomer_CoverLog()
        {
            CustomerModel customerModel = new CustomerModel
            {
                AddressLine1 = "1",
                AddressLine2 = "2",
                AddressLine3 = "3",
                City = "P",
                ClientId = 1,
                Country = "In",
                CustomerId = "2",
                CustomerType = "spl",
                EmailAddress = "gmail",
                FirstName = "tush",
                IsBusiness = true,
                LastName = "W",
                Phone1 = "",
                Phone2 = "",
                PostalCode = "422103",
                Source = "Branch",
                State = "MH",
                TrumpiaRequestDetailId = "25",
                TrumpiaSubscriptionId = "234"
            };
            var result = _customerManager.CreateSubscriptionAndUpdateCustomer(customerModel, "apiKey", "userName", "contactList");
            Assert.AreEqual(result, false);
            MockRepository.VerifyAll();
        }

        [TestMethod]
        public void CreateSubscriptionAndUpdateCustomer_RequestIdNull()
        {
            CustomerModel customerModel = new CustomerModel
            {
                AddressLine1 = "1",
                AddressLine2 = "2",
                AddressLine3 = "3",
                City = "P",
                ClientId = 1,
                Country = "In",
                CustomerId = "2",
                CustomerType = "spl",
                EmailAddress = "gmail",
                FirstName = "tush",
                IsBusiness = true,
                LastName = "W",
                Phone1 = "9028179781",
                Phone2 = "",
                PostalCode = "422103",
                Source = "Branch",
                State = "MH",
                TrumpiaRequestDetailId = "25",
                TrumpiaSubscriptionId = "234"
            };

            _sendSmsManager.Setup(
                t =>
                    t.CreateNewTrumpiaSubscription(It.IsAny<CustomerModel>(), It.IsAny<string>(), It.IsAny<string>(),
                        It.IsAny<string>())).Returns("23");
            var result = _customerManager.CreateSubscriptionAndUpdateCustomer(customerModel, "apiKey", "userName", "contactList");
            Assert.AreEqual(result, true);
            MockRepository.VerifyAll();
        }

        [TestMethod]
        public void DeleteCustomerAsync()
        {
            CassandraRepositoryMock.Setup(
                t => t.DeleteAsync(It.IsAny<string>(), It.IsAny<Expression<Func<CustomerEntity, bool>>>()))
                .Returns(Task.FromResult(typeof(void))).Verifiable();
            var result = _customerManager.DeleteCustomerAsync(87, "Acc500").Result;
            CassandraRepositoryMock.Verify(t => t.DeleteAsync(It.IsAny<string>(), It.IsAny<Expression<Func<CustomerEntity, bool>>>()), Times.Once());
            Assert.AreEqual(result, true);
            MockRepository.VerifyAll();
        }

    }
}
