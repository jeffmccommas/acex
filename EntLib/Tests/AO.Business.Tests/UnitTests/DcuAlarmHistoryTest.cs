﻿using System;
using System.Linq.Expressions;
using System.Threading.Tasks;
using AO.Entities;
using CE.AO.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace AO.Business.Tests.UnitTests
{
    [TestClass]
    public  class DcuAlarmHistoryTest : TestBase
    {
        private DcuAlarmHistory _dcuAlarmHistory;

        /// <summary>
        /// Test Case Init
        /// </summary>
        [TestInitialize]
        public void Init()
        {
            _dcuAlarmHistory = new DcuAlarmHistory(CassandraRepositoryMock.Object);
        }

        /// <summary>
        /// Test case for InsertOrMergeDcuAlarmAsync method in DcuAlarmHistory class
        /// </summary>
        [TestMethod]
        public void InsertOrMergeDcuAlarmHistoryAsync()
        {
            // Arrange
            CassandraRepositoryMock.Setup(
                t => t.InsertOrUpdateAsync(It.IsAny<DcuAlarmHistoryEntity>(), It.IsAny<string>()))
                .Returns(Task.FromResult(true));

            // Act
            var result = _dcuAlarmHistory.InsertOrMergeDcuAlarmHistoryAsync(new DcuAlarmModel()).Result;

            // Assert
            CassandraRepositoryMock.Verify(t => t.InsertOrUpdateAsync(It.IsAny<DcuAlarmHistoryEntity>(), It.IsAny<string>()), Times.Once);
            Assert.IsTrue(result);
            MockRepository.VerifyAll();
        }

        /// <summary>
        /// Test case for DeleteDcuAlarmAsync method in DcuAlarmHistory class
        /// </summary>
        [TestMethod]
        public void DeleteDcuAlarmHistoryAsync()
        {
            // Arrange
            CassandraRepositoryMock.Setup(t => t.DeleteAsync(It.IsAny<string>(), It.IsAny<Expression<Func<DcuAlarmHistoryEntity, bool>>>())).Returns(Task.FromResult(true));

            // Act
            var result = _dcuAlarmHistory.DeleteDcuAlarmHistoryAsync(new DcuAlarmModel()).Result;

            // Assert
            CassandraRepositoryMock.Verify(t => t.Delete(It.IsAny<string>(), It.IsAny<Expression<Func<DcuAlarmHistoryEntity, bool>>>()), Times.Never);
            Assert.IsTrue(result);
            MockRepository.VerifyAll();
        }
    }
}
