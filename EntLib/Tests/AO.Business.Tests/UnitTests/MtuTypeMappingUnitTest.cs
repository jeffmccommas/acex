﻿using System;
using System.Linq.Expressions;
using AO.BusinessContracts;
using AO.DataAccess.Contracts;
using AO.Entities;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace AO.Business.Tests.UnitTests
{
    [TestClass]
    public class MtuTypeMappingUnitTest : TestBase
    {
        private MtuTypeMapping _mtuTypeMapping;
        private MtuTypeEntity _mtuEntity;

        /// <summary>
        /// Test Case Init
        /// </summary>
        [TestInitialize]
        public void Init()
        {
            _mtuTypeMapping = new MtuTypeMapping(LogModelMock.Object,
                new Lazy<ICassandraRepository>(() => CassandraRepositoryMock.Object),
                new Lazy<ICacheProvider>(() => CacheProviderMock.Object));

            _mtuEntity = new MtuTypeEntity
            {
                MtuTypeId = 1234,
                Description = "Test MTU Type",
                MtuType = "TEST"
            };
        }

        /// <summary>
        /// Test Case for accessing Log Model
        /// </summary>
        [TestMethod]
        public void AccessLogModel()
        {
            _mtuTypeMapping.LogModel.DisableLog = true;
            Assert.AreEqual(true, _mtuTypeMapping.LogModel.DisableLog);
        }

        /// <summary>
        /// Test case for GetMtuTypeMapping() method in MtuTypeMapping class
        /// </summary>
        [TestMethod]
        public void GetMtuTypeMappingColdCacheTest()
        {
            // Arrange
            CacheProviderMock.Setup(t => t.Get<MtuTypeEntity>(It.IsAny<string>(), It.IsAny<int>()))
                .Returns((MtuTypeEntity)null)
                .Verifiable();
            CassandraRepositoryMock.Setup(
                t => t.GetSingle(It.IsAny<string>(), It.IsAny<Expression<Func<MtuTypeEntity, bool>>>()))
                .Returns(_mtuEntity).Verifiable();
            CacheProviderMock.Setup(
                t => t.Set(_mtuEntity.MtuTypeId.ToString(), It.IsAny<MtuTypeEntity>(), (int)BusinessContracts.Enums.CacheDatabases.MtuTypesDatabaseId))
                .Verifiable();

            // Act
            var result = _mtuTypeMapping.GetMtuTypeMapping(1234);

            // Assert
            Assert.AreEqual(result.MtuTypeId, _mtuEntity.MtuTypeId);
            MockRepository.VerifyAll();
        }

        /// <summary>
        /// Test case for GetMtuTypeMapping() method in MtuTypeMapping class
        /// </summary>
        [TestMethod]
        public void GetMtuTypeMappingWarmCacheTest()
        {
            // Arrange
            CacheProviderMock.Setup(t => t.Get<MtuTypeEntity>(It.IsAny<string>(), It.IsAny<int>()))
                .Returns(_mtuEntity)
                .Verifiable();

            // Act
            var result = _mtuTypeMapping.GetMtuTypeMapping(1234);

            // Assert
            Assert.AreEqual(result.MtuTypeId, _mtuEntity.MtuTypeId);
            MockRepository.VerifyAll();
        }
    }
}
