﻿using System;
using System.Threading.Tasks;
using AO.BusinessContracts;
using CE.AO.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace AO.Business.Tests.UnitTests
{
    [TestClass]
    public class DcuAlarmFacadeTest : TestBase
    {
        private DcuAlarmFacade _dcuAlarmFacade;
        private Mock<IDcuAlarm> _dcuAlarmMock;
        private Mock<IDcuAlarmHistory> _dcuAlarmHistoryMock;
        private Mock<IDcuAlarmCode> _dcuAlarmCodeMock;
        private Mock<IDcuAlarmCodeHistory> _dcuAlarmCodeHistoryMock;
        private Mock<IDcuAlarmType> _dcualarmTypeMock;

        /// <summary>
        /// Test Case Init
        /// </summary>
        [TestInitialize]
        public void Init()
        {
            _dcuAlarmMock = new Mock<IDcuAlarm>();
            _dcuAlarmCodeHistoryMock = new Mock<IDcuAlarmCodeHistory>();
            _dcuAlarmCodeMock = new Mock<IDcuAlarmCode>();
            _dcuAlarmHistoryMock = new Mock<IDcuAlarmHistory>();
            _dcualarmTypeMock = new Mock<IDcuAlarmType>();

            _dcuAlarmFacade = new DcuAlarmFacade(LogModelMock.Object, new Lazy<IDcuAlarm>(()=>_dcuAlarmMock.Object), new Lazy<IDcuAlarmHistory>(()=>_dcuAlarmHistoryMock.Object), new Lazy<IDcuAlarmCode>(()=>_dcuAlarmCodeMock.Object), new Lazy<IDcuAlarmCodeHistory>(()=>_dcuAlarmCodeHistoryMock.Object), new Lazy<IDcuAlarmType>(()=>_dcualarmTypeMock.Object));

        }
        
        /// <summary>
        /// Test case for InsertOrMergeDcuAlarmAsync method for DcuAlarmFacade class
        /// </summary>
        [TestMethod]
        public void InsertOrMergeDcuAlarmAsync()
        {
            var dcuAlarmModel = new DcuAlarmModel
            {
                ClientId = 89898989,
                AlarmTime = DateTime.UtcNow,
                DcuId = 1,
                AlarmCode = "Integration Test Delete after insert",
                ComponentType = "Test_Component",
                NotificationType = "Test_Notification",
                Severity = 100,
                UserFriendlyDescription = "Testing Entity"
            };

            _dcuAlarmMock.Setup(t => t.InsertOrMergeDcuAlarmAsync(It.IsAny<DcuAlarmModel>()))
                .Returns(Task.FromResult(true));

            _dcuAlarmHistoryMock.Setup(t => t.InsertOrMergeDcuAlarmHistoryAsync(It.IsAny<DcuAlarmModel>()))
                .Returns(Task.FromResult(true));

            _dcuAlarmCodeMock.Setup(t => t.InsertOrMergeDcuAlarmCodeAsync(It.IsAny<DcuAlarmModel>()))
                .Returns(Task.FromResult(true));

            _dcuAlarmCodeHistoryMock.Setup(t => t.InsertOrMergeDcuAlarmCodeHistoryAsync(It.IsAny<DcuAlarmModel>()))
                .Returns(Task.FromResult(true));

            _dcualarmTypeMock.Setup(t => t.GetDcuAlarmType(It.IsAny<string>())).Returns((new DcuAlarmTypeModel
            {
                TableName = "dcu_alarm_types",
                AlarmCode = "Dcu_Call_Me",
                ComponentType = "TestComp",
                NotificationType = "Email",
                Severity = 1,
                UserFriendlyDescription = "Test description"
            }));

            var result = _dcuAlarmFacade.InsertOrMergeDcuAlarmAsync(dcuAlarmModel).Result;

            _dcuAlarmMock.Verify(t => t.InsertOrMergeDcuAlarmAsync(It.IsAny<DcuAlarmModel>()), Times.Once);
            _dcuAlarmHistoryMock.Verify(t => t.InsertOrMergeDcuAlarmHistoryAsync(It.IsAny<DcuAlarmModel>()), Times.Once);
            _dcuAlarmCodeMock.Verify(t => t.InsertOrMergeDcuAlarmCodeAsync(It.IsAny<DcuAlarmModel>()), Times.Once);
            _dcuAlarmCodeHistoryMock.Verify(t => t.InsertOrMergeDcuAlarmCodeHistoryAsync(It.IsAny<DcuAlarmModel>()), Times.Once);
            _dcualarmTypeMock.Verify(t => t.GetDcuAlarmType(It.IsAny<string>()), Times.Once);

            Assert.AreEqual(result, true);
        }

        /// <summary>
        /// Test case for InsertOrMergeDcuAlarmAsync method for DcuAlarmFacade class for invalid mapping
        /// </summary>
        [TestMethod]
        public void InsertOrMergeDcuAlarmAsyncforInvalidMapping()
        {
            var dcuAlarmModelforMapping = new DcuAlarmModel
            {
                ClientId = 89821289,
                AlarmTime = DateTime.UtcNow,
                DcuId = 1,
                AlarmCode = "Integration Test Delete for Invalid object",
                ComponentType = "Test_Component",
                NotificationType = "Test_Notification",
                Severity = 100,
                UserFriendlyDescription = "Testing Entity"
            };

            _dcuAlarmMock.Setup(t => t.InsertOrMergeDcuAlarmAsync(It.IsAny<DcuAlarmModel>()))
                .Returns(Task.FromResult(true));

            _dcuAlarmHistoryMock.Setup(t => t.InsertOrMergeDcuAlarmHistoryAsync(It.IsAny<DcuAlarmModel>()))
                .Returns(Task.FromResult(true));

            _dcuAlarmCodeMock.Setup(t => t.InsertOrMergeDcuAlarmCodeAsync(It.IsAny<DcuAlarmModel>()))
                .Returns(Task.FromResult(true));

            _dcuAlarmCodeHistoryMock.Setup(t => t.InsertOrMergeDcuAlarmCodeHistoryAsync(It.IsAny<DcuAlarmModel>()))
                .Returns(Task.FromResult(true));

            _dcualarmTypeMock.Setup(t => t.GetDcuAlarmType(It.IsAny<string>())).Returns((DcuAlarmTypeModel)null);

            var result = _dcuAlarmFacade.InsertOrMergeDcuAlarmAsync(dcuAlarmModelforMapping).Result;

            _dcuAlarmMock.Verify(t => t.InsertOrMergeDcuAlarmAsync(It.IsAny<DcuAlarmModel>()), Times.Once);
            _dcuAlarmHistoryMock.Verify(t => t.InsertOrMergeDcuAlarmHistoryAsync(It.IsAny<DcuAlarmModel>()), Times.Once);
            _dcuAlarmCodeMock.Verify(t => t.InsertOrMergeDcuAlarmCodeAsync(It.IsAny<DcuAlarmModel>()), Times.Once);
            _dcuAlarmCodeHistoryMock.Verify(t => t.InsertOrMergeDcuAlarmCodeHistoryAsync(It.IsAny<DcuAlarmModel>()), Times.Once);
            _dcualarmTypeMock.Verify(t => t.GetDcuAlarmType(It.IsAny<string>()), Times.Once);

            Assert.IsTrue(result);
        }
    }
}
