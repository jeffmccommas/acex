﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace AO.Business.Tests.UnitTests
{
    [TestClass]
    public class ExtensionTest
    {
        public TestContext TestContext { get; set; }

        [TestMethod]
        [DeploymentItem("UnitTests\\TestData\\SendEmailValidationTestData.xml")]
        [DataSource("Microsoft.VisualStudio.TestTools.DataSource.XML", "|DataDirectory|\\SendEmailValidationTestData.xml", "Validation", DataAccessMethod.Sequential)]
        public void SendEmailValidation_Test()
        {
            try
            {
                var email = TestContext.DataRow["Email"].ToString();
                var whitelist = TestContext.DataRow["Whitelist"].ToString();
                var blacklist = TestContext.DataRow["Blacklist"].ToString();
                var expected = Convert.ToBoolean(TestContext.DataRow["result"].ToString());

                var result = Extensions.SendEmailValidation(email, whitelist, blacklist);

                Assert.AreEqual(expected, result);
            }
            catch (Exception ex)
            {
                Assert.Fail(ex.Message);
            }
        }

        [TestMethod]
        [DeploymentItem("UnitTests\\TestData\\SendSmsValidationTestData.xml")]
        [DataSource("Microsoft.VisualStudio.TestTools.DataSource.XML", "|DataDirectory|\\SendSmsValidationTestData.xml", "Validation", DataAccessMethod.Sequential)]
        public void SendSmsValidation_Test()
        {
            try
            {
                var phone = TestContext.DataRow["Phone"].ToString();
                var whitelist = TestContext.DataRow["Whitelist"].ToString();
                var blacklist = TestContext.DataRow["Blacklist"].ToString();
                var expected = Convert.ToBoolean(TestContext.DataRow["result"].ToString());

                var result = Extensions.SendSmsValidation(phone, whitelist, blacklist);

                Assert.AreEqual(expected, result);
            }
            catch (Exception ex)
            {
                Assert.Fail(ex.Message);
            }
        }
    }
    
}

