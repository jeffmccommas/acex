﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AO.BusinessContracts;
using AO.EventProcessors;
using CE.AO.Models;
using CE.AO.Utilities;
using Microsoft.ServiceBus.Messaging;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Newtonsoft.Json;
using Enums = CE.AO.Utilities.Enums;


namespace AO.Business.Tests.UnitTests
{
    /// <summary>
    /// Test Class for EventHubPreAggCalculationTriggerSubscriber
    /// </summary>
    [TestClass]
    public class EventHubPreAggCalculationTriggerSubscriberTest
    {
        private EventHubPreAggCalculationTriggerSubscriber _eventHubPreAggCalculationTriggerSubscriber;
        private Mock<IPreAggCalculationFacade> _preAggCalculationFacade;
        private Mock<EventProcessorFactory<EventHubPreAggCalculationTriggerSubscriber>> _eventProcessorFactoryMock;

        /// <summary>
        /// Test Case Init
        /// </summary>
        [TestInitialize]
        public void Init()
        {
            _preAggCalculationFacade = new Mock<IPreAggCalculationFacade>();
            _eventProcessorFactoryMock = new Mock<EventProcessorFactory<EventHubPreAggCalculationTriggerSubscriber>>();

            _eventHubPreAggCalculationTriggerSubscriber =
                new EventHubPreAggCalculationTriggerSubscriber(new LogModel { DisableLog = true },
                    new Lazy<IPreAggCalculationFacade>(() => _preAggCalculationFacade.Object),
                    new Lazy<EventProcessorFactory<EventHubPreAggCalculationTriggerSubscriber>>(
                        () => _eventProcessorFactoryMock.Object));
        }

        /// <summary>
        /// Test Method for ProcessEventsAsync 
        /// </summary>
        [TestMethod]
        public void ProcessEventsAsync()
        {
            _preAggCalculationFacade.Setup(p => p.CalculateAccountPreAgg(It.IsAny<AccountPreAggCalculationRequestModel>()))
                .Returns(Task.FromResult(true));

            var accountPreAggCalculationRequestModel = new AccountPreAggCalculationRequestModel
            {
                ClientId = 899898,
                AccountId = "TestEventhubAccount",
                AmiTimestamp = DateTime.UtcNow,
                CalculationRange = Enums.PreAggCalculationRange.Day,
                Timezone = "PST"
            };

            var serializedEventData = JsonConvert.SerializeObject(accountPreAggCalculationRequestModel);
            var eventData = new EventData(Encoding.UTF8.GetBytes(serializedEventData));
            var context = new PartitionContext();
            var lease = new Lease();
            context.Lease = lease;
            context.Lease.Offset = "121212";
            context.Lease.PartitionId = "0";
            context.ConsumerGroupName = "$Default";
            context.EventHubPath = "watermdmTest";
            eventData.Properties.Add("ProcessingType", Enums.ProcessingType.File);
            eventData.Properties.Add("Source", "S");
            eventData.Properties.Add("RowIndex", 1);

            var eventDataList = new List<EventData> { eventData };
            // ReSharper disable once UnusedVariable
            var isOpen = _eventHubPreAggCalculationTriggerSubscriber.OpenAsync(context);
            var isProcessCompleted = _eventHubPreAggCalculationTriggerSubscriber.ProcessEventsAsync(context, eventDataList.AsEnumerable());

            Assert.IsTrue(isOpen.IsCompleted);
            Assert.IsTrue(isProcessCompleted.IsCompleted);
        }

        /// <summary>
        /// ProcessEventsAsync For Validation coverage
        /// </summary>
        [TestMethod]
        public void ProcessEventsAsyncForValidation()
        {
            _preAggCalculationFacade.Setup(p => p.CalculateAccountPreAgg(It.IsAny<AccountPreAggCalculationRequestModel>()))
                .Returns(Task.FromResult(true));

            var accountPreAggCalculationRequestModel = new AccountPreAggCalculationRequestModel
            {
                ClientId = 899898,
                AccountId = null,
                AmiTimestamp = DateTime.UtcNow,
                CalculationRange = Enums.PreAggCalculationRange.Day,
                Timezone = "PST"
            };

            var serializedEventData = JsonConvert.SerializeObject(accountPreAggCalculationRequestModel);
            var eventData = new EventData(Encoding.UTF8.GetBytes(serializedEventData));
            var context = new PartitionContext();
            var lease = new Lease();
            context.Lease = lease;
            context.Lease.Offset = "121212";
            context.Lease.PartitionId = "0";
            context.ConsumerGroupName = "$Default";
            context.EventHubPath = "watermdmTest";
            eventData.Properties.Add("ProcessingType", Enums.ProcessingType.File);
            eventData.Properties.Add("Source", "S");
            eventData.Properties.Add("RowIndex", 1);

            var eventDataList = new List<EventData> { eventData };
            // ReSharper disable once UnusedVariable
            var isOpen = _eventHubPreAggCalculationTriggerSubscriber.OpenAsync(context);
            var isProcessCompleted = _eventHubPreAggCalculationTriggerSubscriber.ProcessEventsAsync(context, eventDataList.AsEnumerable());

            Assert.IsTrue(isOpen.IsCompleted);
            Assert.IsTrue(isProcessCompleted.IsCompleted);
        }

        /// <summary>
        /// ProcessEventsAsync For Exception coverage
        /// </summary>
        [TestMethod]
        public void ProcessEventsAsyncForException()
        {
            _preAggCalculationFacade.Setup(p => p.CalculateAccountPreAgg(It.IsAny<AccountPreAggCalculationRequestModel>()))
                .Returns(Task.FromResult(true));

            var accountPreAggCalculationRequestModel = new AccountPreAggCalculationRequestModel
            {
                ClientId = 899898,
                AccountId = null,
                AmiTimestamp = DateTime.UtcNow,
                CalculationRange = Enums.PreAggCalculationRange.Day,
                Timezone = "PST"
            };

            var serializedEventData = JsonConvert.SerializeObject(accountPreAggCalculationRequestModel);
            var eventData = new EventData(Encoding.UTF8.GetBytes(serializedEventData));
            var context = new PartitionContext { Lease = null };
            eventData.Properties.Add("ProcessingType", "NoFileExist");
            eventData.Properties.Add("ForException", 1);

            var eventDataList = new List<EventData> { eventData };
            var isProcessCompleted = _eventHubPreAggCalculationTriggerSubscriber.ProcessEventsAsync(context, eventDataList.AsEnumerable());

            Assert.IsTrue(isProcessCompleted.IsCompleted);
        }
    }
}