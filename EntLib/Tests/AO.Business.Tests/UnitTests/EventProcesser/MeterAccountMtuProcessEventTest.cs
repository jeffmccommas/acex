﻿using System;
using System.Globalization;
using System.Text;
using System.Threading.Tasks;
using AO.BusinessContracts;
using AO.EventProcessors.EventProcessor;
using CE.AO.Models;
using CE.AO.Models.RawFileModels;
using Microsoft.ServiceBus.Messaging;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Newtonsoft.Json;


namespace AO.Business.Tests.UnitTests.EventProcesser
{
    /// <summary>
    /// Test case for MeterAccountMtuProcessEvent
    /// </summary>
    [TestClass]
    public class MeterAccountMtuProcessEventTest :TestBase
    {
        private Mock<IMeterAccountMtuFacade> _meterAccountMtuFacadeMock;
		
        private MeterAccountMtuProcessEvent _meterAccountMtuProcessEvent;

        /// <summary>
        /// Test Case Init
        /// </summary>
        [TestInitialize]
        public void Init()
        {
            _meterAccountMtuFacadeMock = new Mock<IMeterAccountMtuFacade>();
            _meterAccountMtuProcessEvent = new MeterAccountMtuProcessEvent(LogModelMock.Object, new Lazy<IMeterAccountMtuFacade>(() => _meterAccountMtuFacadeMock.Object));
        }

        /// <summary>
        /// Test Case for processing MeterAccountMtu data
        /// </summary>
        [TestMethod]
        public void Process()
        {
            var rawMeterAccountMtuModel = new RawMeterAccountMtuModel
            {
                TENANT_ID = "87",
                ACCOUNT_NUMBER = "3363020124",
                METER_ID = "1",
                METER_TYPE_ID = "2139",
                METER_SERIAL_NUMBER = "96143473",
                METER_INSTALL_DATE = Convert.ToString(DateTime.UtcNow, CultureInfo.InvariantCulture),
                LONGITUDE = "-118.2762",
                MTU_INSTALL_DATE = Convert.ToString(DateTime.UtcNow, CultureInfo.InvariantCulture),
                MTU_ID = "46287572",
                ACTIVE = "true",
                LATITUDE = "34.0439",
                CROSS_REFERENCE_ID = "1"
            };

            var serializedEventData = JsonConvert.SerializeObject(rawMeterAccountMtuModel);
            var eventData = new EventData(Encoding.UTF8.GetBytes(serializedEventData));
            
            _meterAccountMtuFacadeMock.Setup(t => t.InsertOrMergeMeterAccountAsync(It.IsAny<MeterAccountMtuModel>()))
               .Returns(Task.FromResult(true));
			
			var success = _meterAccountMtuProcessEvent.Process(eventData, "87").Result;
            Assert.AreEqual(true, success);
        }

        /// <summary>
        /// Test Case for failing validations
        /// </summary>
        [TestMethod]
        public void FailValidations()
        {

            _meterAccountMtuFacadeMock.Setup(t => t.InsertOrMergeMeterAccountAsync(It.IsAny<MeterAccountMtuModel>()))
              .Returns(Task.FromResult(true));

            var rawMeterAccountMtuModel = new RawMeterAccountMtuModel
            {
                TENANT_ID = "8",
                ACCOUNT_NUMBER = "3363020124",
                METER_ID = "1",
                METER_TYPE_ID = "2139",
                METER_SERIAL_NUMBER = "96143473",
                METER_INSTALL_DATE = Convert.ToString(DateTime.UtcNow, CultureInfo.InvariantCulture),
                LONGITUDE = "-118.2762",
                MTU_INSTALL_DATE = Convert.ToString(DateTime.UtcNow, CultureInfo.InvariantCulture),
                MTU_ID = "46287572",
                ACTIVE = "true",
                LATITUDE = "34.0439",
                CROSS_REFERENCE_ID = "1"
            };

            var serializedEventData = JsonConvert.SerializeObject(rawMeterAccountMtuModel);
            var eventData = new EventData(Encoding.UTF8.GetBytes(serializedEventData));

            var success = _meterAccountMtuProcessEvent.Process(eventData, "87").Result;
            Assert.AreEqual(false, success);

            rawMeterAccountMtuModel.TENANT_ID = "87";
            rawMeterAccountMtuModel.ACCOUNT_NUMBER = "";
            serializedEventData = JsonConvert.SerializeObject(rawMeterAccountMtuModel);
            eventData = new EventData(Encoding.UTF8.GetBytes(serializedEventData));

            success = _meterAccountMtuProcessEvent.Process(eventData, "87").Result;
            Assert.AreEqual(false, success);
        }

    }
}
