﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AO.BusinessContracts;
using AO.BusinessContracts.EventProcessesContracts;
using AO.EventProcessors;
using AO.Registrar;
using CE.AO.Models;
using CE.AO.Models.RawFileModels;
using CsvHelper;
using Microsoft.Practices.Unity;
using Microsoft.ServiceBus.Messaging;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Blob;
using Moq;
using Newtonsoft.Json;
using Enums = CE.AO.Utilities.Enums;


namespace AO.Business.Tests.UnitTests.EventProcesser
{
    /// <summary>
    /// Test Case Class for BatchProcess Worker Role(Function.cs)
    /// </summary>
    [TestClass]
    public class EventProcessorTest
    {
        private UnityContainer _unityContainer;
        private IProcessEvent[] _eventProcessors;

        /// <summary>
        /// Test Case Init
        /// </summary>
        [TestInitialize]
        public void Init()
        {
            _unityContainer = new UnityContainer();
            AutoMapperConfig.AutoMapperMappings();
            new DataStorageRegistrar().Initialize<TransientLifetimeManager>(_unityContainer);
            RegisterMockInstances(_unityContainer);

            _eventProcessors = new IProcessEvent[0];
        }

        private static void RegisterMockInstances(UnityContainer unityContainer)
        {
            unityContainer.RegisterInstance(new Mock<IAzureQueue>().Object);
            unityContainer.RegisterInstance(new Mock<ITallAMI>().Object);
            unityContainer.RegisterInstance(new Mock<IAmi>().Object);
            unityContainer.RegisterInstance(new Mock<IBillingFacade>().Object);
            unityContainer.RegisterInstance(new Mock<IAccountUpdatesFacade>().Object);
            unityContainer.RegisterInstance(new Mock<ISendSms>().Object);
            unityContainer.RegisterInstance(new Mock<ISendEmail>().Object);
            unityContainer.RegisterInstance(GetNotificationMock().Object);
            unityContainer.RegisterInstance(GetAmiReadingMock().Object);
            unityContainer.RegisterInstance(GetAccountLookupMock().Object);
            unityContainer.RegisterInstance(GetClientConfigMock().Object);
            unityContainer.RegisterInstance(GetSubscriptionMock().Object);
            unityContainer.RegisterInstance(GetCustomerMock().Object);
        }

        private static Mock<ICustomer> GetCustomerMock()
        {
            var iCustomer = new Mock<ICustomer>();
            iCustomer.Setup(m => m.GetCustomerAsync(It.IsAny<int>(), It.IsAny<string>())).Returns(Task.FromResult(new CustomerModel()
            {
                EmailAddress = "t@gmail.com",
                AddressLine1 = "1",
                AddressLine3 = "3",
                AddressLine2 = "2",
                FirstName = "tu"
            }));
            return iCustomer;
        }

        private static Mock<ISubscription> GetSubscriptionMock()
        {
            var iSubscription = new Mock<ISubscription>();
            iSubscription.Setup(m => m.InsertSubscriptionAsync(It.IsAny<SubscriptionModel>(), It.IsAny<bool>(), It.IsAny<bool>())).Returns(Task.FromResult(true));
            iSubscription.Setup(m => m.GetSubscriptionDetailsAsync(It.IsAny<int>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>())).Returns(Task.FromResult(new SubscriptionModel()
            {
                InsightTypeName = "",
                IsEmailOptInCompleted = false,
                AccountId = "ACc900",
                ClientId = 87,
                CustomerId = "Cust87",
                ProgramName = "Program1",
                Channel = "C",
                ServiceContractId = "SC009",
                SubscriptionDate = DateTime.Now,
                IsLatest = true
            }));
            return iSubscription;
        }

        /// <summary>
        /// Returns Mock of IClientFacade
        /// </summary>
        /// <returns></returns>
        private static Mock<IClientConfigFacade> GetClientConfigMock()
        {
            var clientSetting = new ClientSettings()
            {
                TrumpiaApiKey = "TrApiKey",
                TrumpiaUserName = "TrUserName",
                TimeZone = "EST",
                OptInEmailFrom = "t@gmail.com",
                OptInEmailPassword = "123",
                OptInEmailUserName = "tush",
                OptInEmailConfirmationTemplateId = "Temp87",
                Programs = new List<ProgramSettings>()
                {
                    new ProgramSettings()
                    {
                        ProgramName = "Program2",
                        DoubleOptInRequired = true,
                        Insights = new List<InsightSettings>()
                        {
                            new InsightSettings()
                            {
                                InsightName = "Insights"
                            }
                        }
                    }
                }
            };
            var iCLientConfigFacade = new Mock<IClientConfigFacade>();
            iCLientConfigFacade.Setup(m => m.GetClientSettings(It.IsAny<int>())).Returns(clientSetting);
            return iCLientConfigFacade;
        }
        /// <summary>
        /// Returns Mock of IAmiReading
        /// </summary>
        /// <returns></returns>
        private static Mock<IAmiReading> GetAmiReadingMock()
        {
            var nccAmiReadingModel = new NccAmiReadingModel()
            {
                BatteryVoltage = "3.570440054",
                CommodityId = 3,
                CustomerId = "",
                MeterId = "96143503",
                AmiTimeStamp = DateTime.Now,
                ReadingValue = 0,
                Timezone = "PST",
                TransponderId = 46287586,
                TransponderPort = 1,
                UOMId = 3
            };
            var iAmiReading = new Mock<IAmiReading>();
            iAmiReading.Setup(m => m.GetAmiReading(It.IsAny<int>(), It.IsAny<string>(), It.IsAny<DateTime>())).Returns(Task.FromResult(nccAmiReadingModel));
            return iAmiReading;
        }
        /// <summary>
        /// Returns Mock of IAccountLookup
        /// </summary>
        /// <returns></returns>
        private static Mock<IAccountLookup> GetAccountLookupMock()
        {
            var iAccountLookUpMockObject = new Mock<IAccountLookup>();
            iAccountLookUpMockObject.Setup(m => m.GetAccountMeterDetails(It.IsAny<int>(), It.IsAny<string>()))
                .Returns(new List<AccountLookupModel> { new AccountLookupModel() { AccountId = "" }});
            return iAccountLookUpMockObject;
        }
        /// <summary>
        /// Returns Mock of INotification
        /// </summary>
        /// <returns></returns>
        private static Mock<INotification> GetNotificationMock()
        {
            SmsTemplateModel smsTemplate = new SmsTemplateModel()
            {
                ClientId = 87,
                InsightTypeName = "InsightTypeName",
                Body = "Body-Sample-Data",
                TemplateId = "45"
            };
            var iNotification = new Mock<INotification>();
            iNotification.Setup(m => m.GetSmsTemplateFromDB(It.IsAny<int>(), It.IsAny<string>(), It.IsAny<string>()))
                .Returns(Task.FromResult(smsTemplate));
            iNotification.Setup(m => m.GetReplacedSmsTemplateForSubscriptionDoubleOptInConfirmation(It.IsAny<string>(), It.IsAny<SubscriptionModel>(), It.IsAny<ClientSettings>(), It.IsAny<CustomerModel>(), It.IsAny<System.Text.RegularExpressions.MatchCollection>()))
              .Returns("str");
            return iNotification;
        }

        /// <summary>
        /// Gets the CloudBlockBlob using container name and file name
        /// </summary>
        /// <param name="fileName">Name of the blob</param>
        /// <param name="containerName">Name of the container in which the blob resides</param>
        /// <returns></returns>
        public ICloudBlob GetBlockBlob(string fileName, string containerName)
        {
            var container = CloudBlobClient.GetContainerReference(containerName);
            return container.GetBlockBlobReference(fileName);
        }

        /// <summary>
        /// CloudBlobClient private variable
        /// </summary>
        private CloudBlobClient _cloudBlobClient;
        public CloudBlobClient CloudBlobClient
        {
            get
            {
                if (_cloudBlobClient != null) return _cloudBlobClient;
                //var storageAccount =
                //    CloudStorageAccount.Parse(
                //        CloudConfigurationManager.GetSetting(""));
                var storageAccount =
                    CloudStorageAccount.Parse(
                        "DefaultEndpointsProtocol=https;AccountName=watermdmstgdev;AccountKey=NLiPTjsHibbVo6sPlgujN+qbHoh8gXBelSdlgKFWxzSiD4Mlx+mUoF/eLxUb4p0jCmpZyEiBjry1+GJbJawrRA==");
                _cloudBlobClient = storageAccount.CreateCloudBlobClient();
                return _cloudBlobClient;
            }
        }

        /// <summary>
        /// Test Case for OpenAsync Method
        /// </summary>
        [TestMethod]
        public void OpenAsync()
        {
            PartitionContext partitionContext = new PartitionContext();
            Lease lease = new Lease();
            partitionContext.Lease = lease;
            partitionContext.Lease.Offset = "Lease";
            EventHubEventProcessTriggerSubscriber eventProc =
                new EventHubEventProcessTriggerSubscriber(new LogModel { DisableLog = true },
                    new Lazy<EventProcessorFactory<EventHubEventProcessTriggerSubscriber>>(), _eventProcessors,
                    new Lazy<IAzureQueue>());

            var result = eventProc.OpenAsync(partitionContext);
            Assert.AreEqual(result.Exception, null);
        }

        /// <summary>
        /// Test Case for Ami 30 Minutes
        /// </summary>%%
        [TestMethod]
        public void ProcessEventsAsync_Ami30()
        {
            var blockBlob = GetBlockBlob("EST Timezone AMI_87_30min_Valid Data - Tush_Test.csv", "client87");
            var stream = blockBlob.OpenRead();
            var sr = new StreamReader(stream);
            using (var reader = new CsvReader(sr))
            {
                reader.Read();
                var record = reader.GetRecord<RawAmi30MinuteIntervalModel>();
                var serializedEventData = JsonConvert.SerializeObject(record);
                var eventData = new EventData(Encoding.UTF8.GetBytes(serializedEventData));
                eventData.Properties.Add("EventType", 1);
                eventData.Properties.Add("IntervalType", 30);
                eventData.Properties.Add("ClientId", "87");
                eventData.Properties.Add("ProcessingType", Enums.ProcessingType.File);
                eventData.Properties.Add("Source", "S");
                eventData.Properties.Add("RowIndex", 1);
                PartitionContext context = new PartitionContext();
                Lease lease = new Lease();
                context.Lease = lease;
                context.Lease.Offset = "Lease";
                context.Lease.PartitionId = "PartitionId";
                //EventProcessor eventProc = new EventProcessor
                //{
                //    LogModel = new LogModel()
                //    {
                //        DisableLog = true
                //    }
                //};
                EventHubEventProcessTriggerSubscriber eventProc =
              new EventHubEventProcessTriggerSubscriber(new LogModel { DisableLog = true },
                  new Lazy<EventProcessorFactory<EventHubEventProcessTriggerSubscriber>>(), _eventProcessors, new Lazy<IAzureQueue>());

                List<EventData> eventDataList = new List<EventData> { eventData };
                eventProc.OpenAsync(context);
                var result = eventProc.ProcessEventsAsync(context, eventDataList.AsEnumerable());
                Assert.AreEqual(result.Exception, null);
            }
        }

        ///// <summary>
        ///// Test Case for Ami 15 Minutes
        ///// </summary>
        //[TestMethod]
        //public void ProcessEventsAsync_Ami15()
        //{
        //    var blockBlob = GetBlockBlob("EST_AMI_87_15min_13JuneuTushar-Test.csv", "client87");
        //    var stream = blockBlob.OpenRead();
        //    var sr = new StreamReader(stream);
        //    using (var reader = new CsvReader(sr))
        //    {
        //        reader.Read();
        //        var record = reader.GetRecord<RawAmi15MinuteIntervalModel>();
        //        var serializedEventData = JsonConvert.SerializeObject(record);
        //        var eventData = new EventData(Encoding.UTF8.GetBytes(serializedEventData));
        //        eventData.Properties.Add("EventType", 1);
        //        eventData.Properties.Add("IntervalType", 15);
        //        eventData.Properties.Add("ClientId", "87");
        //        eventData.Properties.Add("ProcessingType", Enums.ProcessingType.File);
        //        eventData.Properties.Add("Source", "S");
        //        eventData.Properties.Add("RowIndex", 1);
        //        PartitionContext context = new PartitionContext();
        //        Lease lease = new Lease();
        //        context.Lease = lease;
        //        context.Lease.Offset = "Lease";
        //        context.Lease.PartitionId = "PartitionId";
        //        EventProcessor eventProc = new EventProcessor
        //        {
        //            LogModel = new LogModel()
        //            {
        //                DisableLog = true
        //            }
        //        };
        //        List<EventData> eventDataList = new List<EventData> { eventData };
        //        eventProc.OpenAsync(context);
        //        var result = eventProc.ProcessEventsAsync(context, eventDataList.AsEnumerable());
        //        Assert.AreEqual(result.Exception, null);
        //    }
        //}

        ///// <summary>
        ///// Test Case for Ami 60 Minutes
        ///// </summary>
        //[TestMethod]
        //public void ProcessEventsAsync_Ami60()
        //{
        //    var blockBlob = GetBlockBlob("AMIEnd_EST_AMI_87_60min-Does not Handles DST1_TestTush1.csv", "client87");
        //    var stream = blockBlob.OpenRead();
        //    var sr = new StreamReader(stream);
        //    using (var reader = new CsvReader(sr))
        //    {
        //        reader.Read();
        //        var record = reader.GetRecord<RawAmi60MinuteIntervalModel>();
        //        var serializedEventData = JsonConvert.SerializeObject(record);
        //        var eventData = new EventData(Encoding.UTF8.GetBytes(serializedEventData));
        //        eventData.Properties.Add("EventType", 1);
        //        eventData.Properties.Add("IntervalType", 60);
        //        eventData.Properties.Add("ClientId", "87");
        //        eventData.Properties.Add("ProcessingType", Enums.ProcessingType.File);
        //        eventData.Properties.Add("Source", "S");
        //        eventData.Properties.Add("RowIndex", 1);
        //        PartitionContext context = new PartitionContext();
        //        Lease lease = new Lease();
        //        context.Lease = lease;
        //        context.Lease.Offset = "Lease";
        //        context.Lease.PartitionId = "PartitionId";
        //        EventProcessor eventProc = new EventProcessor
        //        {
        //            LogModel = new LogModel()
        //            {
        //                DisableLog = true
        //            }
        //        };
        //        List<EventData> eventDataList = new List<EventData> { eventData };
        //        eventProc.OpenAsync(context);
        //        var result = eventProc.ProcessEventsAsync(context, eventDataList.AsEnumerable());
        //        Assert.AreEqual(result.Exception, null);
        //    }
        //}

        ///// <summary>
        ///// Test Case for Billing
        ///// </summary>%%
        //[TestMethod]
        //public void ProcessEventsAsync_Billing()
        //{
        //    var blockBlob = GetBlockBlob("Bill_87_UATDay41_tush_Test.csv", "client87");
        //    var stream = blockBlob.OpenRead();
        //    var sr = new StreamReader(stream);
        //    using (var reader = new CsvReader(sr))
        //    {
        //        reader.Read();
        //        var record = reader.GetRecord<RawBillingModel>();
        //        var serializedEventData = JsonConvert.SerializeObject(record);
        //        var eventData = new EventData(Encoding.UTF8.GetBytes(serializedEventData));
        //        eventData.Properties.Add("EventType", 2);
        //        eventData.Properties.Add("IntervalType", 0);
        //        eventData.Properties.Add("ClientId", "87");
        //        eventData.Properties.Add("ProcessingType", Enums.ProcessingType.File);
        //        eventData.Properties.Add("Source", "S");
        //        eventData.Properties.Add("RowIndex", 1);
        //        PartitionContext context = new PartitionContext();
        //        Lease lease = new Lease();
        //        context.Lease = lease;
        //        context.Lease.Offset = "Lease";
        //        context.Lease.PartitionId = "PartitionId";
        //        EventProcessor eventProc = new EventProcessor
        //        {
        //            LogModel = new LogModel()
        //            {
        //                DisableLog = true
        //            }
        //        };
        //        List<EventData> eventDataList = new List<EventData> { eventData };
        //        eventProc.OpenAsync(context);
        //        var result = eventProc.ProcessEventsAsync(context, eventDataList.AsEnumerable());
        //        Assert.AreEqual(result.Exception, null);
        //    }
        //}

        ///// <summary>
        ///// Test Case for Subscription
        ///// </summary>
        //[TestMethod]
        //public void ProcessEventsAsync_Subscription()
        //{
        //    var blockBlob = GetBlockBlob("Subscription_87_10Mar_tush_Test.csv", "client87");
        //    var stream = blockBlob.OpenRead();
        //    var sr = new StreamReader(stream);
        //    using (var reader = new CsvReader(sr))
        //    {
        //        reader.Read();
        //        var record = reader.GetRecord<RawSubscriptionModel>();
        //        var serializedEventData = JsonConvert.SerializeObject(record);
        //        var eventData = new EventData(Encoding.UTF8.GetBytes(serializedEventData));
        //        eventData.Properties.Add("EventType", 4);
        //        eventData.Properties.Add("IntervalType", 0);
        //        eventData.Properties.Add("ClientId", "87");
        //        eventData.Properties.Add("ProcessingType", Enums.ProcessingType.File);
        //        eventData.Properties.Add("Source", "S");
        //        eventData.Properties.Add("RowIndex", 1);
        //        PartitionContext context = new PartitionContext();
        //        Lease lease = new Lease();
        //        context.Lease = lease;
        //        context.Lease.Offset = "Lease";
        //        context.Lease.PartitionId = "PartitionId";
        //        EventProcessor eventProc = new EventProcessor
        //        {
        //            LogModel = new LogModel()
        //            {
        //                DisableLog = true
        //            }
        //        };
        //        List<EventData> eventDataList = new List<EventData> { eventData };
        //        eventProc.OpenAsync(context);
        //        var result = eventProc.ProcessEventsAsync(context, eventDataList.AsEnumerable());
        //        Assert.AreEqual(result.Exception, null);
        //    }
        //}

        ///// <summary>
        ///// Test Case for Subscription SendSms
        ///// </summary>
        //[TestMethod]
        //public void ProcessEventsAsync_Subscription_SendSMS()
        //{
        //    //IUnityContainer uContainer = new UnityContainer();
        //    //new DataStorageRegistrar().Initialize<TransientLifetimeManager>(uContainer);

        //    var subscriptionModel = new SubscriptionModel()
        //    {
        //        ClientId = 87,
        //        CustomerId = "500",
        //        AccountId = "Ac9898",
        //        Type = "Active",
        //        InsightTypeName = "Insight",
        //        ServiceContractId = "SC56",
        //        SubscriptionDate = DateTime.Now,
        //        IsLatest = true,
        //        ProgramName = "Program1"
        //    };
        //    ClientSettings clientSetting = new ClientSettings()
        //    {
        //        OptInSmsConfirmationTemplateId = "Conf45T"
        //    };
        //    CustomerModel customerModel = new CustomerModel()
        //    {
        //        TrumpiaSubscriptionId = "TrimpId"

        //    };
        //    SubscriptionProcessEvent subPorcevent = new SubscriptionProcessEvent(new LogModel(), new Mock<Lazy<IClientConfigFacade>>().Object
        //        , new Mock<Lazy<IBilling>>().Object, new Mock<Lazy<ISubscription>>().Object, new Mock<Lazy<ICustomer>>().Object, new Mock<Lazy<INotification>>().Object
        //        , new Mock<Lazy<ISendSms>>().Object, new Mock<Lazy<ISendEmail>>().Object
        //        );
        //    subPorcevent.SendSms(subscriptionModel, clientSetting, customerModel).Wait();
        //    Assert.AreEqual(true, true);
        //}

        ///// <summary>
        ///// Test Case for Subscription SendEmails
        ///// </summary>
        //[TestMethod]
        //public void ProcessEventsAsync_Subscription_SendMail()
        //{
        //    var subscriptionModel = new SubscriptionModel()
        //    {
        //        ClientId = 87,
        //        CustomerId = "500",
        //        AccountId = "Ac9898",
        //        Type = "Active",
        //        InsightTypeName = "Insight",
        //        ServiceContractId = "SC56",
        //        SubscriptionDate = DateTime.Now,
        //        IsLatest = true,
        //        ProgramName = "Program1"
        //    };
        //    ClientSettings clientSetting = new ClientSettings()
        //    {
        //        OptInEmailUserName = "UserName",
        //        OptInEmailPassword = "Password",
        //        OptInEmailFrom = "From@et.com",
        //        UnmaskedAccountIdEndingDigit = 1,
        //        OptInEmailConfirmationTemplateId = "24"
        //    };
        //    CustomerModel customerModel = new CustomerModel()
        //    {
        //        TrumpiaSubscriptionId = "TrimpId",
        //        EmailAddress = "sample@t.com",
        //        AddressLine3 = "Address3",
        //        AddressLine2 = "AddressLine2",
        //        AddressLine1 = "AddressLine1"

        //    };
        //    SubscriptionProcessEvent subPorcevent = new SubscriptionProcessEvent(new LogModel(), new Mock<Lazy<IClientConfigFacade>>().Object
        //        , new Mock<Lazy<IBilling>>().Object, new Mock<Lazy<ISubscription>>().Object, new Mock<Lazy<ICustomer>>().Object, new Mock<Lazy<INotification>>().Object
        //        , new Mock<Lazy<ISendSms>>().Object, new Mock<Lazy<ISendEmail>>().Object);
        //    subPorcevent.SendMail(subscriptionModel, clientSetting, customerModel).ConfigureAwait(false);
        //    Assert.AreEqual(true, true);
        //}

        ///// <summary>
        ///// Test Case for Account Updates
        ///// </summary>
        //[TestMethod]
        //public void ProcessEventsAsync_AccountUpdate()
        //{
        //    var blockBlob = GetBlockBlob("AccountUpdates_87_UATDay4BCycle007_UnitTest_Tush.csv", "client87");
        //    var stream = blockBlob.OpenRead();
        //    var sr = new StreamReader(stream);
        //    using (var reader = new CsvReader(sr))
        //    {
        //        reader.Read();
        //        var record = reader.GetRecord<RawAccountUpdatesModel>();
        //        var serializedEventData = JsonConvert.SerializeObject(record);
        //        var eventData = new EventData(Encoding.UTF8.GetBytes(serializedEventData));
        //        eventData.Properties.Add("EventType", 3);
        //        eventData.Properties.Add("IntervalType", 0);
        //        eventData.Properties.Add("ClientId", "87");
        //        eventData.Properties.Add("ProcessingType", Enums.ProcessingType.File);
        //        eventData.Properties.Add("Source", "S");
        //        eventData.Properties.Add("RowIndex", 1);
        //        PartitionContext context = new PartitionContext();
        //        Lease lease = new Lease();
        //        context.Lease = lease;
        //        context.Lease.Offset = "Lease";
        //        context.Lease.PartitionId = "PartitionId";
        //        EventProcessor eventProc = new EventProcessor
        //        {
        //            LogModel = new LogModel()
        //            {
        //                DisableLog = true
        //            }
        //        };
        //        List<EventData> eventDataList = new List<EventData> { eventData };
        //        eventProc.OpenAsync(context);
        //        var result = eventProc.ProcessEventsAsync(context, eventDataList.AsEnumerable());
        //        Assert.AreEqual(result.Exception, null);
        //    }
        //}

        ///// <summary>
        ///// Test Case for NCC Ami Reading
        ///// </summary>
        //[TestMethod]
        //public void ProcessEventsAsync_NccAmiReading()
        //{
        //    var blockBlob = GetBlockBlob("ncc__2016-07-28_101914__2de7bbe0-edf8-48cf-9854-50e6b4c2a80f_Tush_Test.csv", "client87");
        //    var stream = blockBlob.OpenRead();
        //    var sr = new StreamReader(stream);
        //    using (var reader = new CsvReader(sr))
        //    {
        //        reader.Read();
        //        var record = reader.GetRecord<RawNccAmiReadingModel>();
        //        var serializedEventData = JsonConvert.SerializeObject(record);
        //        var eventData = new EventData(Encoding.UTF8.GetBytes(serializedEventData));
        //        eventData.Properties.Add("EventType", 5);
        //        eventData.Properties.Add("IntervalType", 0);
        //        eventData.Properties.Add("ClientId", "87");
        //        eventData.Properties.Add("ProcessingType", Enums.ProcessingType.File);
        //        eventData.Properties.Add("Source", "S");
        //        eventData.Properties.Add("RowIndex", 1);
        //        PartitionContext context = new PartitionContext();
        //        Lease lease = new Lease();
        //        context.Lease = lease;
        //        context.Lease.Offset = "Lease";
        //        context.Lease.PartitionId = "PartitionId";
        //        EventProcessor eventProc = new EventProcessor
        //        {
        //            LogModel = new LogModel()
        //            {
        //                DisableLog = true
        //            }
        //        };
        //        List<EventData> eventDataList = new List<EventData> { eventData };
        //        eventProc.OpenAsync(context);
        //        var result = eventProc.ProcessEventsAsync(context, eventDataList.AsEnumerable());
        //        Assert.AreEqual(result.Exception, null);
        //    }
        //}

        ///// <summary>
        ///// Test Case for NCC Ami Caluclations
        ///// </summary>%%
        //[TestMethod]
        //public void ProcessEventsAsync_NccAmiCalculations()
        //{
        //    var rawBaseReadModel = new RawBaseReadModel()
        //    {
        //        BATTERY_VOLTAGE = "3.570440054",
        //        COMMODITY_ID = "3",
        //        CUSTOMER_ID = "",
        //        METER_ID = "96143503",
        //        READING_DATETIME = "2015-10-14T18:16:00Z",
        //        READING_VALUE = "0",
        //        TIMEZONE = "PST",
        //        TRANSPONDER_ID = "46287586",
        //        TRANSPONDER_PORT = "1",
        //        UNIT_OF_MEASURE = "Cu. Ft."
        //    };
        //    var serializedEventData = JsonConvert.SerializeObject(rawBaseReadModel);
        //    var eventData = new EventData(Encoding.UTF8.GetBytes(serializedEventData));
        //    eventData.Properties.Add("EventType", 7);
        //    eventData.Properties.Add("IntervalType", 0);
        //    eventData.Properties.Add("ClientId", "87");
        //    eventData.Properties.Add("ProcessingType", Enums.ProcessingType.File);
        //    eventData.Properties.Add("Source", "S");
        //    eventData.Properties.Add("RowIndex", 1);
        //    PartitionContext context = new PartitionContext();
        //    Lease lease = new Lease();
        //    context.Lease = lease;
        //    context.Lease.Offset = "Lease";
        //    context.Lease.PartitionId = "PartitionId";
        //    EventProcessor eventProc = new EventProcessor
        //    {
        //        LogModel = new LogModel()
        //        {
        //            DisableLog = true
        //        }
        //    };
        //    List<EventData> eventDataList = new List<EventData> { eventData };
        //    eventProc.OpenAsync(context);
        //    var result = eventProc.ProcessEventsAsync(context, eventDataList.AsEnumerable());
        //    Assert.AreEqual(result.Exception, null);
        //}

        ///// <summary>
        ///// Test Case for NCC Ami Consumption
        ///// </summary>
        //[TestMethod]
        //public void ProcessEventsAsync_NccAmiConsumption()
        //{
        //    var rawNccAmiReadingModel = new RawNccAmiReadingModel()
        //    {
        //        BATTERY_VOLTAGE = "3.570440054",
        //        COMMODITY_ID = "3",
        //        CUSTOMER_ID = "",
        //        METER_ID = "96143503",
        //        READING_DATETIME = "2015-10-14T18:16:00Z",
        //        READING_VALUE = "0",
        //        TIMEZONE = "PST",
        //        TRANSPONDER_ID = "46287586",
        //        TRANSPONDER_PORT = "1",
        //        UNIT_OF_MEASURE = "Cu. Ft."
        //    };
        //    var serializedEventData = JsonConvert.SerializeObject(rawNccAmiReadingModel);
        //    var eventData = new EventData(Encoding.UTF8.GetBytes(serializedEventData));
        //    eventData.Properties.Add("EventType", 6);
        //    eventData.Properties.Add("IntervalType", 0);
        //    eventData.Properties.Add("ClientId", "87");
        //    eventData.Properties.Add("ProcessingType", Enums.ProcessingType.File);
        //    eventData.Properties.Add("Source", "S");
        //    eventData.Properties.Add("RowIndex", 1);
        //    PartitionContext context = new PartitionContext();
        //    Lease lease = new Lease();
        //    context.Lease = lease;
        //    context.Lease.Offset = "Lease";
        //    context.Lease.PartitionId = "PartitionId";
        //    EventProcessor eventProc = new EventProcessor
        //    {
        //        LogModel = new LogModel()
        //        {
        //            DisableLog = true
        //        }
        //    };
        //    List<EventData> eventDataList = new List<EventData> { eventData };
        //    eventProc.OpenAsync(context);
        //    var result = eventProc.ProcessEventsAsync(context, eventDataList.AsEnumerable());
        //    Assert.AreEqual(result.Exception, null);
        //}

        ///// <summary>
        ///// Test Case for OpenAsync Method
        ///// </summary>
        //[TestMethod]
        //public void CloseAsync()
        //{
        //    PartitionContext context = new PartitionContext();
        //    Lease lease = new Lease();
        //    context.Lease = lease;
        //    context.Lease.Offset = "Lease";
        //    context.Lease.PartitionId = "PartitionId";
        //    EventProcessor eventProc = new EventProcessor();
        //    try
        //    {
        //        eventProc.OpenAsync(context);
        //        eventProc.CloseAsync(context, CloseReason.Shutdown).Wait();
        //    }
        //    catch
        //    {
        //        Assert.AreEqual(true, true);
        //    }
        //}

        ///// <summary>
        ///// Cleanup Method To Release Log4Net Data Resources hold by Any Test Method
        ///// </summary>
        //[TestCleanup]
        //public void CleanUpMethod()
        //{
        //    CallContext.FreeNamedDataSlot("log4net.Util.LogicalThreadContextProperties");
        //}
    }
}
