﻿using System;
using System.Globalization;
using System.Text;
using System.Threading.Tasks;
using AO.BusinessContracts;
using AO.EventProcessors.EventProcessor;
using AO.Registrar;
using CE.AO.Models;
using CE.AO.Models.RawFileModels;
using Microsoft.Practices.Unity;
using Microsoft.ServiceBus.Messaging;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Newtonsoft.Json;

namespace AO.Business.Tests.UnitTests.EventProcesser
{
	/// <summary>
	/// Test case for DcuProvisioningProcessEvent
	/// </summary>
	[TestClass]
    public class DcuProvisioningProcessEventTest
    {
        private UnityContainer _unityContainer;

        private Mock<IDcuProvisioning> _dcuProvisioningMock;

        private DcuProvisioningProcessEvent _dcuProvisioningProcessEvent;

        /// <summary>
        /// Test Case Init
        /// </summary>
        [TestInitialize]
        public void Init()
        {
            _unityContainer = new UnityContainer();
            new DataStorageRegistrar().Initialize<TransientLifetimeManager>(_unityContainer);

            _dcuProvisioningMock = new Mock<IDcuProvisioning>();

            var overrides = new ParameterOverrides
            {
                { "logModel", new LogModel {DisableLog = true} },
                 { "dcuProvisioning", new Lazy<IDcuProvisioning>(() => _dcuProvisioningMock.Object)}
            };

            _dcuProvisioningProcessEvent = _unityContainer.Resolve<DcuProvisioningProcessEvent>(overrides);
        }

        /// <summary>
        /// Test Case for processing MeterAccountMtu data
        /// </summary>
        [TestMethod]
        public void Process()
        {
            var rawDcuProvisioningModel = new RawDcuProvisioningModel
            {
                TENANT_ID = "87",
                DCU_ID = "336302",
                LONGITUDE = "-118.2762",
                LATITUDE = "34.0439",
                DATE_INSTALLED = Convert.ToString(DateTime.UtcNow, CultureInfo.InvariantCulture),
                NAME = "Test DCU"
            };

            var serializedEventData = JsonConvert.SerializeObject(rawDcuProvisioningModel);
            var eventData = new EventData(Encoding.UTF8.GetBytes(serializedEventData));

            _dcuProvisioningMock.Setup(t => t.InsertOrMergeDcuProvisioningAsync(It.IsAny<DcuProvisioningModel>()))
                .Returns(Task.FromResult(true));

            var success = _dcuProvisioningProcessEvent.Process(eventData, "87").Result;
            Assert.AreEqual(true, success);
        }

        /// <summary>
        /// Test Case for failing validations
        /// </summary>
        [TestMethod]
        public void FailValidations()
        {
            _dcuProvisioningMock.Setup(t => t.InsertOrMergeDcuProvisioningAsync(It.IsAny<DcuProvisioningModel>()))
                .Returns(Task.FromResult(true));

            var rawDcuProvisioningModel = new RawDcuProvisioningModel
            {
                TENANT_ID = "8",
                DCU_ID = "336302",
                LONGITUDE = "-118.2762",
                LATITUDE = "34.0439",
                DATE_INSTALLED = Convert.ToString(DateTime.UtcNow, CultureInfo.InvariantCulture),
                NAME = "Test DCU"
            };

            var serializedEventData = JsonConvert.SerializeObject(rawDcuProvisioningModel);
            var eventData = new EventData(Encoding.UTF8.GetBytes(serializedEventData));

            var success = _dcuProvisioningProcessEvent.Process(eventData, "87").Result;
            Assert.AreEqual(false, success);
        }
    }

}
