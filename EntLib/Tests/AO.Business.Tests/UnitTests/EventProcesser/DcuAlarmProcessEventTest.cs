﻿using System;
using System.Globalization;
using System.Text;
using System.Threading.Tasks;
using AO.BusinessContracts;
using AO.EventProcessors.EventProcessor;
using AO.Registrar;
using CE.AO.Models;
using CE.AO.Models.RawFileModels;
using Microsoft.Practices.Unity;
using Microsoft.ServiceBus.Messaging;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Newtonsoft.Json;

namespace AO.Business.Tests.UnitTests.EventProcesser
{
	/// <summary>
	/// Test case for DcuAlarmProcessEvent
	/// </summary>
	[TestClass]
    public class DcuAlarmProcessEventTest
    {
        private UnityContainer _unityContainer;
        private DcuAlarmProcessEvent _dcuAlarmProcessEvent;
        private Mock<IDcuAlarmFacade> _dcuAlarmFacadeMock;

        /// <summary>
        /// Test Case Init
        /// </summary>
        [TestInitialize]
        public void Init()
        {
            _unityContainer = new UnityContainer();
            new DataStorageRegistrar().Initialize<TransientLifetimeManager>(_unityContainer);

            var overrides = new ParameterOverrides
            {
                 { "logModel", new LogModel {DisableLog = true} },
                 { "dcuAlarmFacade", new Lazy<IDcuAlarmFacade>(() => _dcuAlarmFacadeMock.Object)},
            };

            _dcuAlarmFacadeMock = new Mock<IDcuAlarmFacade>();
            _dcuAlarmProcessEvent = _unityContainer.Resolve<DcuAlarmProcessEvent>(overrides);
        }

        /// <summary>
        /// Test Case for processing DcuAlarm data
        /// </summary>
        [TestMethod]
        public void Process()
        {
            var rawDcuAlarmModelModel = new RawDcuAlarmModel
            {
                ALARM_CODE = "Dcu_Call_Me",
                ALARM_TIME = DateTime.UtcNow.ToString(CultureInfo.InvariantCulture),
                DCU_ID = "1",
                TENANT_ID = "87"
            };
            var serializedEventData = JsonConvert.SerializeObject(rawDcuAlarmModelModel);
            var eventData = new EventData(Encoding.UTF8.GetBytes(serializedEventData));

            _dcuAlarmFacadeMock.Setup(t => t.InsertOrMergeDcuAlarmAsync(It.IsAny<DcuAlarmModel>())).Returns(Task.FromResult(true));
            var success = _dcuAlarmProcessEvent.Process(eventData, "87").Result;

            Assert.AreEqual(true, success);
        }

        /// <summary>
        /// Test Case for failing validations
        ///</summary>
        /// 
        [TestMethod]
        public void FailValidations()
        {
            var rawDcuAlarmModelModel = new RawDcuAlarmModel
            {
                ALARM_CODE = "Dcu_Call_Me",
                ALARM_TIME = DateTime.UtcNow.ToString(CultureInfo.InvariantCulture),
                DCU_ID = "1",
                TENANT_ID = "8"
            };


            var serializedEventData = JsonConvert.SerializeObject(rawDcuAlarmModelModel);
            var eventData = new EventData(Encoding.UTF8.GetBytes(serializedEventData));

            var success = _dcuAlarmProcessEvent.Process(eventData, "87").Result;
            Assert.AreEqual(false, success);
        }
    }

}
