﻿using System;
using System.IO;
using System.Text;
using AO.BusinessContracts;
using AO.BusinessContracts.EventProcessesContracts;
using AO.EventProcessors.EventProcessor;
using AO.Registrar;
using CE.AO.Models;
using CE.AO.Models.RawFileModels;
using CsvHelper;
using Microsoft.Practices.Unity;
using Microsoft.ServiceBus.Messaging;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Newtonsoft.Json;
using Enums = CE.AO.Utilities.Enums;

namespace AO.Business.Tests.UnitTests.EventProcesser
{
	/// <summary>
	/// Tests class for Ami30MinuteIntervalProcess Class
	/// </summary>
	[TestClass]
    public class Ami30MinuteIntervalProcessTest
    {
        private UnityContainer _unityContainer;
        private Ami30MinuteIntervalProcessEvent _ami30MinuteIntervalEvent;

        private Mock<ITallAMI> _tallAmi;
        private Mock<IAzureQueue> _azureQueue;

        /// <summary>
        /// Test Case Init
        /// </summary>
        [TestInitialize]
        public void Init()
        {
            _unityContainer = new UnityContainer();
            new DataStorageRegistrar().Initialize<TransientLifetimeManager>(_unityContainer);

            var ami30Overrides = new ParameterOverrides
            {
                 { "logModel", new LogModel {DisableLog = true, Source = "my_source"} },
                 { "clientConfigFacade", new Lazy<IClientConfigFacade>(() => MockObject.GetClientConfigMock().Object)},
                { "tallAmi",new Lazy<ITallAMI>(()=>_tallAmi.Object )},
                { "azureQueue",new Lazy<IAzureQueue>(()=>_azureQueue.Object)},
                //{ "baseAmiProcess",new Lazy<IBaseAmiProcess>(()=>BaseAmiProcess.Object)}
            };

            new Mock<IClientConfigFacade>();
            _tallAmi = new Mock<ITallAMI>();
            _azureQueue = new Mock<IAzureQueue>();
            new Mock<IAmiProcessEventHelper>();

            _ami30MinuteIntervalEvent = _unityContainer.Resolve<Ami30MinuteIntervalProcessEvent>(ami30Overrides);

        }


        /// <summary>
        /// Test Case for Ami 30 Minutes
        /// </summary>%%
        [TestMethod]
        public void ProcessEventsAsync_Ami30()
        {
            var blockBlob = MockObject.GetBlockBlob("EST Timezone AMI_87_30min_Valid Data - Tush_Test.csv", "client87");
            var stream = blockBlob.OpenRead();
            var sr = new StreamReader(stream);
            using (var reader = new CsvReader(sr))
            {
                reader.Read();
                var record = reader.GetRecord<RawAmi30MinuteIntervalModel>();
                var serializedEventData = JsonConvert.SerializeObject(record);
                var eventData = new EventData(Encoding.UTF8.GetBytes(serializedEventData));
                eventData.Properties.Add("EventType", 1);
                eventData.Properties.Add("IntervalType", 30);
                eventData.Properties.Add("ClientId", "87");
                eventData.Properties.Add("ProcessingType", Enums.ProcessingType.File);
                eventData.Properties.Add("Source", "S");
                eventData.Properties.Add("RowIndex", 1);
                PartitionContext context = new PartitionContext();
                Lease lease = new Lease();
                context.Lease = lease;
                context.Lease.Offset = "Lease";
                context.Lease.PartitionId = "PartitionId";
                var result = _ami30MinuteIntervalEvent.Process(eventData, "87");
                Assert.AreEqual(result.Exception, null);
            }
        }

    }
}
