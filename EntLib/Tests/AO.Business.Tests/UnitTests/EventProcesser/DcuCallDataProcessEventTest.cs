﻿using System;
using System.Globalization;
using System.Text;
using System.Threading.Tasks;
using AO.BusinessContracts;
using AO.EventProcessors.EventProcessor;
using AO.Registrar;
using CE.AO.Models;
using CE.AO.Models.RawFileModels;
using Microsoft.Practices.Unity;
using Microsoft.ServiceBus.Messaging;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Newtonsoft.Json;

namespace AO.Business.Tests.UnitTests.EventProcesser
{
	/// <summary>
	/// Test case for DcuCallDataProcessEventTest
	/// </summary>
	[TestClass]
    public class DcuCallDataProcessEventTest
    {
        private UnityContainer _unityContainer;
        private DcuCallDataProcessEvent _dcuCallDataProcessEvent;
        private Mock<IDcuCallData> _dcuCallDataMock;

        /// <summary>
        /// Test Case Init
        /// </summary>
        [TestInitialize]
        public void Init()
        {
            _unityContainer = new UnityContainer();
            new DataStorageRegistrar().Initialize<TransientLifetimeManager>(_unityContainer);

            _dcuCallDataMock = new Mock<IDcuCallData>();
            var overrides = new ParameterOverrides
            {
                 { "logModel", new LogModel {DisableLog = true} },
                 { "dcuCallData", new Lazy<IDcuCallData>(() => _dcuCallDataMock.Object)},
            };

            _dcuCallDataProcessEvent = _unityContainer.Resolve<DcuCallDataProcessEvent>(overrides);
        }

        /// <summary>
        /// Test Case for processing DcuCall data
        /// </summary>
        [TestMethod]
        public void Process()
        {
            var rawDcuCallDataModel = new RawDcuCallDataModel
            {
                TENANT_ID = "87",
                DCU_ID = "123",
                CALL_END = DateTime.UtcNow.ToString(CultureInfo.InvariantCulture),
                CALL_START = DateTime.UtcNow.ToString(CultureInfo.InvariantCulture),
                CALL_TYPE = "AL",
                CLEARED = "TRUE",
                EXPECTED = "123",
                HUNG_UP = "FALSE",
                RECEIVED = "123"
            };

            var serializedEventData = JsonConvert.SerializeObject(rawDcuCallDataModel);
            var eventData = new EventData(Encoding.UTF8.GetBytes(serializedEventData));

            _dcuCallDataMock.Setup(t => t.InsertOrMergeDcuCallDataAsync(It.IsAny<DcuCallDataModel>()))
                .Returns(Task.FromResult(true));

            var success = _dcuCallDataProcessEvent.Process(eventData, "87").Result;
            Assert.AreEqual(true, success);
        }

        /// <summary>
        /// Test Case for failing validations
        /// </summary>
        [TestMethod]
        public void FailValidations()
        {
            _dcuCallDataMock.Setup(t => t.InsertOrMergeDcuCallDataAsync(It.IsAny<DcuCallDataModel>()))
                .Returns(Task.FromResult(true));

            var rawDcuCallDataModel = new RawDcuCallDataModel
            {
                TENANT_ID = "8",
                DCU_ID = "123",
                CALL_END = DateTime.UtcNow.ToString(CultureInfo.InvariantCulture),
                CALL_START = DateTime.UtcNow.ToString(CultureInfo.InvariantCulture),
                CALL_TYPE = "AL",
                CLEARED = "TRUE",
                EXPECTED = "123",
                HUNG_UP = "FALSE",
                RECEIVED = "123"
            };

            var serializedEventData = JsonConvert.SerializeObject(rawDcuCallDataModel);
            var eventData = new EventData(Encoding.UTF8.GetBytes(serializedEventData));

            var success = _dcuCallDataProcessEvent.Process(eventData, "87").Result;
            Assert.AreEqual(false, success);

            rawDcuCallDataModel.TENANT_ID = "87";
            rawDcuCallDataModel.CALL_TYPE = "";
            serializedEventData = JsonConvert.SerializeObject(rawDcuCallDataModel);
            eventData = new EventData(Encoding.UTF8.GetBytes(serializedEventData));

            success = _dcuCallDataProcessEvent.Process(eventData, "87").Result;
            Assert.AreEqual(false, success);
        }
    }

}
