﻿using System;
using System.Globalization;
using System.Text;
using System.Threading.Tasks;
using AO.BusinessContracts;
using AO.EventProcessors.EventProcessor;
using AO.Registrar;
using CE.AO.Models;
using CE.AO.Models.RawFileModels;
using Microsoft.Practices.Unity;
using Microsoft.ServiceBus.Messaging;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Newtonsoft.Json;

namespace AO.Business.Tests.UnitTests.EventProcesser
{
	/// <summary>
	/// Tests AlarmProcessEvent Class
	/// </summary>
	[TestClass]
    public class AlarmProcessEventTest
    {
        private UnityContainer _unityContainer;
        private Mock<IAlarmFacade> _alarmFacadeMock;
        private AlarmProcessEvent _alarmProcessEvent;

        /// <summary>
        /// Test Case Init
        /// </summary>
        [TestInitialize]
        public void Init()
        {
            _unityContainer = new UnityContainer();
            new DataStorageRegistrar().Initialize<TransientLifetimeManager>(_unityContainer);

            var overrides = new ParameterOverrides
            {
                 { "logModel", new LogModel {DisableLog = true} },
                 { "alarmFacade", new Lazy<IAlarmFacade>(() => _alarmFacadeMock.Object)},
            };
            _alarmFacadeMock = new Mock<IAlarmFacade>();
            _alarmProcessEvent = _unityContainer.Resolve<AlarmProcessEvent>(overrides);
         
        }

        /// <summary>
        /// Test Case for processing MeterAccountMtu data
        /// </summary>
        [TestMethod]
        public void Process()
        {
            var rawAlarmModelModel = new RawAlarmModel
            {
                TENANT_ID = "89898989",
                ALARM_TIME = DateTime.UtcNow.ToString(CultureInfo.InvariantCulture),
                ALARM_GROUP_ID = "99898989",
                ALARM_ID = "79898989",
                ALARM_SOURCE_ID = "69898989",
                ALARM_SOURCE_TYPE_ID = "59898989",
                ALARM_TYPE_ID = "49898989",
                DETAILS = "Test Details"
            };

            var serializedEventData = JsonConvert.SerializeObject(rawAlarmModelModel);
            var eventData = new EventData(Encoding.UTF8.GetBytes(serializedEventData));

            _alarmFacadeMock.Setup(t => t.InsertOrMergeAlarmAsync(It.IsAny<AlarmModel>())).Returns(Task.FromResult(true));
            var success = _alarmProcessEvent.Process(eventData, "89898989").Result;

            Assert.AreEqual(true, success);
        }

        /// <summary>
        /// Test Case for failing validations
        /// </summary>
        [TestMethod]
        public void FailValidations()
        {
            
            var rawAlarmModelModel = new RawAlarmModel
            {
                TENANT_ID = "898989",
                ALARM_TIME = DateTime.UtcNow.ToString(CultureInfo.InvariantCulture),
                ALARM_GROUP_ID = "99898989",
                ALARM_ID = "79898989",
                ALARM_SOURCE_ID = "69898989",
                ALARM_SOURCE_TYPE_ID = "59898989",
                ALARM_TYPE_ID = "49898989",
                DETAILS = "Test Details"
            };

            var serializedEventData = JsonConvert.SerializeObject(rawAlarmModelModel);
            var eventData = new EventData(Encoding.UTF8.GetBytes(serializedEventData));

            var success = _alarmProcessEvent.Process(eventData, "89898989").Result;
            Assert.AreEqual(false, success);
        }

    }
}
