﻿using System;
using System.Globalization;
using System.Runtime.Remoting.Messaging;
using System.Text;
using System.Threading.Tasks;
using AO.BusinessContracts;
using AO.EventProcessors.EventProcessor;
using AO.VEE.Business.Contracts;
using AO.VEE.DTO;
using CE.AO.Models;
using CE.AO.Models.RawFileModels;
using Microsoft.ServiceBus.Messaging;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Newtonsoft.Json;

namespace AO.Business.Tests.UnitTests.EventProcesser
{
    /// <summary>
	/// Test class for MeterReadProcessEventTest
	/// </summary>
    [TestClass]
    public class MeterReadProcessEventTest : TestBase
    {
        private Mock<IMeterReadFacade> _meterReadFacadeMock;
        private Mock<IVeeFacade> _veeFacadeMock;
        private IUomConversion _iUomConversion;
        private Mock<IClientConfigFacade> _iClientConfigFacade;
        private Mock<IProcessTrigger<AccountPreAggCalculationRequestModel>> _eventHubPreAggCalculationTrigger;
        private MeterReadProcessEvent _meterReadProcessEvent;

        /// <summary>
        /// Test Case Init
        /// </summary>
        [TestInitialize]
        public void Init()
        {
            _iClientConfigFacade = new Mock<IClientConfigFacade>();
            _iUomConversion = new UomConversion();
            _meterReadFacadeMock = new Mock<IMeterReadFacade>();
            _veeFacadeMock = new Mock<IVeeFacade>();
            _eventHubPreAggCalculationTrigger = new Mock<IProcessTrigger<AccountPreAggCalculationRequestModel>>();
            _meterReadProcessEvent = new MeterReadProcessEvent(LogModelMock.Object, new Lazy<IMeterReadFacade>(() => _meterReadFacadeMock.Object), new Lazy<IVeeFacade>(() => _veeFacadeMock.Object),
                new Lazy<IUomConversion>(() => _iUomConversion), new Lazy<IClientConfigFacade>(() => _iClientConfigFacade.Object), new Lazy<IProcessTrigger<AccountPreAggCalculationRequestModel>>(() => _eventHubPreAggCalculationTrigger.Object));
        }

        /// <summary>
        /// Test Case for processing MeterRead data
        /// </summary>
        [TestMethod]
        public void Process()
        {
            var rawMeterReadModel = new RawMeterReadModel
            {
                TENANT_ID = "87",
                ACCOUNT_NUMBER = "1353720571",
                METER_ID = "90213729",
                CUSTOMER_ID = "",
                COMMODITY_ID = "3",
                TRANSPONDER_ID = "46287727",
                BATTERY_VOLTAGE = "",
                READING_DATETIME = Convert.ToString(DateTime.UtcNow, CultureInfo.InvariantCulture),
                READING_VALUE = "10",
                READ_INTERVAL = "60",
                TIMEZONE = "PST",
                TRANSPONDER_PORT = "1",
                UNIT_OF_MEASURE = "Cu. Ft."
            };

            var serializedEventData = JsonConvert.SerializeObject(rawMeterReadModel);
            var eventData = new EventData(Encoding.UTF8.GetBytes(serializedEventData));
            _meterReadFacadeMock.Setup(t => t.InsertOrMergeMeterReadsAsync(It.IsAny<MeterReadModel>()))
                .Returns(Task.FromResult(true));

            _veeFacadeMock.Setup(t => t.InsertOrMergeVeeStagedDataAsync(It.IsAny<VeeStagedClientMeterByDateDto>()));
            _iClientConfigFacade.Setup(t => t.GetStandardUom()).Returns(() => new StandardUomModel()
            {
                Water = "gal",
                Electric = "kwh",
                Gas = "cf"
            });

            var success = _meterReadProcessEvent.Process(eventData, "87").Result;
            Assert.AreEqual(true, success);

            //Unit test for wrong Standard unit of measure
            _iClientConfigFacade.Setup(t => t.GetStandardUom()).Returns(() => new StandardUomModel()
            {
                Water = "gsssal",
                Electric = "kwh",
                Gas = "cf"
            });
            var fail = _meterReadProcessEvent.Process(eventData, "87").Result;
            Assert.AreEqual(false, fail);

            //Unit test for commodity 1
            rawMeterReadModel.COMMODITY_ID = "1";
            serializedEventData = JsonConvert.SerializeObject(rawMeterReadModel);
            eventData = new EventData(Encoding.UTF8.GetBytes(serializedEventData));
            success = _meterReadProcessEvent.Process(eventData, "87").Result;
            Assert.AreEqual(false, success);
            //Unit test for commodity 2
            rawMeterReadModel.COMMODITY_ID = "2";
            serializedEventData = JsonConvert.SerializeObject(rawMeterReadModel);
            eventData = new EventData(Encoding.UTF8.GetBytes(serializedEventData));
            success = _meterReadProcessEvent.Process(eventData, "87").Result;
            Assert.AreEqual(true, success);

            //Unit test for commodity null Standard UOMs
            _iClientConfigFacade.Setup(t => t.GetStandardUom()).Returns((StandardUomModel)null);
            fail = _meterReadProcessEvent.Process(eventData, "87").Result;
            Assert.AreEqual(false, fail);
            //Unit test for commodity null Commodity other than Mentioned in DB
            rawMeterReadModel.COMMODITY_ID = "7";
            _iClientConfigFacade.Setup(t => t.GetStandardUom()).Returns(() => new StandardUomModel()
            {
                Water = "gal",
                Electric = "kwh",
                Gas = "cf"
            });

            serializedEventData = JsonConvert.SerializeObject(rawMeterReadModel);
            eventData = new EventData(Encoding.UTF8.GetBytes(serializedEventData));
            fail = _meterReadProcessEvent.Process(eventData, "87").Result;
            Assert.AreEqual(false, fail);

            rawMeterReadModel.UNIT_OF_MEASURE = "kwh";
            rawMeterReadModel.UNIT_OF_MEASURE = "3";
            serializedEventData = JsonConvert.SerializeObject(rawMeterReadModel);
            eventData = new EventData(Encoding.UTF8.GetBytes(serializedEventData));
            fail = _meterReadProcessEvent.Process(eventData, "87").Result;
            Assert.AreEqual(false, fail);
        }

        /// <summary>
        /// Test Case for failing validations
        /// </summary>
        [TestMethod]
        public void FailValidations()
        {

            _meterReadFacadeMock.Setup(t => t.InsertOrMergeMeterReadsAsync(It.IsAny<MeterReadModel>()))
                .Returns(Task.FromResult(true));

            _veeFacadeMock.Setup(t => t.InsertOrMergeVeeStagedDataAsync(It.IsAny<VeeStagedClientMeterByDateDto>()));

            var rawMeterReadModel = new RawMeterReadModel
            {
                TENANT_ID = "8",
                ACCOUNT_NUMBER = "1353720571",
                METER_ID = "90213729",
                CUSTOMER_ID = "",
                COMMODITY_ID = "3",
                TRANSPONDER_ID = "46287727",
                BATTERY_VOLTAGE = "",
                READING_DATETIME = Convert.ToString(DateTime.UtcNow, CultureInfo.InvariantCulture),
                READING_VALUE = "10",
                READ_INTERVAL = "60",
                TIMEZONE = "PST",
                TRANSPONDER_PORT = "1",
                UNIT_OF_MEASURE = "Cu. Ft."
            };

            var serializedEventData = JsonConvert.SerializeObject(rawMeterReadModel);
            var eventData = new EventData(Encoding.UTF8.GetBytes(serializedEventData));

            var success = _meterReadProcessEvent.Process(eventData, "87").Result;
            Assert.AreEqual(false, success);

            rawMeterReadModel.TENANT_ID = "87";
            rawMeterReadModel.ACCOUNT_NUMBER = "";
            serializedEventData = JsonConvert.SerializeObject(rawMeterReadModel);
            eventData = new EventData(Encoding.UTF8.GetBytes(serializedEventData));

            // ReSharper disable once RedundantAssignment
            success = _meterReadProcessEvent.Process(eventData, "87").Result;

            rawMeterReadModel.UNIT_OF_MEASURE = "WrongUOM";
            serializedEventData = JsonConvert.SerializeObject(rawMeterReadModel);
            eventData = new EventData(Encoding.UTF8.GetBytes(serializedEventData));
            success = _meterReadProcessEvent.Process(eventData, "87").Result;

            Assert.IsFalse(success);
        }

        /// <summary>
        /// Cleanup Method To Release Log4Net Data Resources hold by Any Test Method
        /// </summary>
        [TestCleanup]
        public void CleanUpMethod()
        {
            CallContext.FreeNamedDataSlot("log4net.Util.LogicalThreadContextProperties");
        }
    }
}