﻿using System;
using System.Globalization;
using System.Text;
using System.Threading.Tasks;
using AO.BusinessContracts;
using AO.EventProcessors.EventProcessor;
using AO.Registrar;
using CE.AO.Models;
using CE.AO.Models.RawFileModels;
using Microsoft.Practices.Unity;
using Microsoft.ServiceBus.Messaging;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Newtonsoft.Json;

namespace AO.Business.Tests.UnitTests.EventProcesser
{
	[TestClass]
    public class MeterReadTriggerCalculationProcessEventTest
    {
        private UnityContainer _unityContainer;

        private Mock<IAzureQueue> _azureQueueMock;

        private MeterReadTriggerCalculationProcessEvent _meterReadTriggerCalculationProcessEvent;
        /// <summary>
        /// Test Case Init
        /// </summary>
        [TestInitialize]
        public void Init()
        {
            _unityContainer = new UnityContainer();
            new DataStorageRegistrar().Initialize<TransientLifetimeManager>(_unityContainer);

            _azureQueueMock = new Mock<IAzureQueue>();
            var overrides = new ParameterOverrides
            {
                { "logModel", new LogModel {DisableLog = true} },
                 { "azureQueue", new Lazy<IAzureQueue>(() => _azureQueueMock.Object)}
            };

            _meterReadTriggerCalculationProcessEvent =
                _unityContainer.Resolve<MeterReadTriggerCalculationProcessEvent>(overrides);

        }

        /// <summary>
        /// Test Case for processing MeterRead data
        /// </summary>
        [TestMethod]
        public void Process()
        {
            var rawBaseReadModel = new RawBaseReadModel
            {
                METER_ID = "90213729",
                CUSTOMER_ID = "",
                COMMODITY_ID = "3",
                TRANSPONDER_ID = "46287727",
                BATTERY_VOLTAGE = "",
                READING_DATETIME = Convert.ToString(DateTime.UtcNow, CultureInfo.InvariantCulture),
                READING_VALUE = "10",
                TIMEZONE = "PST",
                TRANSPONDER_PORT = "1",
                UNIT_OF_MEASURE = "Cu. Ft."
            };

            var serializedEventData = JsonConvert.SerializeObject(rawBaseReadModel);
            var eventData = new EventData(Encoding.UTF8.GetBytes(serializedEventData));


            _azureQueueMock.Setup(t => t.AddMessageToCalculationQueue(It.IsAny<string>())).Returns(Task.CompletedTask);

            var success = _meterReadTriggerCalculationProcessEvent.Process(eventData, "87").Result;
            Assert.AreEqual(true, success);
        }

        /// <summary>
        /// Test Case for failing validations
        /// </summary>
        [TestMethod]
        public void FailValidations()
        {

            _azureQueueMock.Setup(t => t.AddMessageToCalculationQueue(It.IsAny<string>())).Returns(Task.CompletedTask);

            var rawBaseReadModel = new RawBaseReadModel
            {
                METER_ID = "",
                CUSTOMER_ID = "",
                COMMODITY_ID = "3",
                TRANSPONDER_ID = "46287727",
                BATTERY_VOLTAGE = "",
                READING_DATETIME = Convert.ToString(DateTime.UtcNow, CultureInfo.InvariantCulture),
                READING_VALUE = "10",
                TIMEZONE = "PST",
                TRANSPONDER_PORT = "1",
                UNIT_OF_MEASURE = "Cu. Ft."
            };

            var serializedEventData = JsonConvert.SerializeObject(rawBaseReadModel);
            var eventData = new EventData(Encoding.UTF8.GetBytes(serializedEventData));

            var success = _meterReadTriggerCalculationProcessEvent.Process(eventData, "87").Result;
            Assert.AreEqual(false, success);

            ////rawBaseReadModel.TENANT_ID = "87";
            //rawBaseReadModel.METER_ID = "";
            //serializedEventData = JsonConvert.SerializeObject(rawBaseReadModel);
            //eventData = new EventData(Encoding.UTF8.GetBytes(serializedEventData));

            // ReSharper disable once RedundantAssignment
            success = _meterReadTriggerCalculationProcessEvent.Process(eventData, "87").Result;

            rawBaseReadModel.METER_ID = "90213729";
            rawBaseReadModel.UNIT_OF_MEASURE = "WrongUOM";
            serializedEventData = JsonConvert.SerializeObject(rawBaseReadModel);
            eventData = new EventData(Encoding.UTF8.GetBytes(serializedEventData));

            success = _meterReadTriggerCalculationProcessEvent.Process(eventData, "87").Result;

            Assert.IsFalse(success);
        }

    }

}
