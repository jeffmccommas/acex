﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using AO.BusinessContracts;
using CE.AO.Models;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Blob;
using Moq;


namespace AO.Business.Tests.UnitTests.EventProcesser
{
   public static class MockObject
    {
        public static Mock<ICustomer> GetCustomerMock()
        {
            var iCustomer = new Mock<ICustomer>();
            iCustomer.Setup(m => m.GetCustomerAsync(It.IsAny<int>(), It.IsAny<string>())).Returns(Task.FromResult(new CustomerModel()
            {
                EmailAddress = "t@gmail.com",
                AddressLine1 = "1",
                AddressLine3 = "3",
                AddressLine2 = "2",
                FirstName = "tu"
            }));
            return iCustomer;
        }
        public  static Mock<ISubscription> GetSubscriptionMock()
        {
            var iSubscription = new Mock<ISubscription>();
            iSubscription.Setup(m => m.InsertSubscriptionAsync(It.IsAny<SubscriptionModel>(), It.IsAny<bool>(), It.IsAny<bool>())).Returns(Task.FromResult(true));
            iSubscription.Setup(m => m.GetSubscriptionDetailsAsync(It.IsAny<int>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>())).Returns(Task.FromResult(new SubscriptionModel()
            {
                InsightTypeName = "",
                IsEmailOptInCompleted = false,
                AccountId = "ACc900",
                ClientId = 87,
                CustomerId = "Cust87",
                ProgramName = "Program1",
                Channel = "C",
                ServiceContractId = "SC009",
                SubscriptionDate = DateTime.Now,
                IsLatest = true
            }));
            return iSubscription;
        }

        /// <summary>
        /// Returns Mock of IClientFacade
        /// </summary>
        /// <returns></returns>
        public static Mock<IClientConfigFacade> GetClientConfigMock()
        {
            var clientSetting = new ClientSettings()
            {
                TrumpiaApiKey = "TrApiKey",
                TrumpiaUserName = "TrUserName",
                TimeZone = "EST",
                OptInEmailFrom = "t@gmail.com",
                OptInEmailPassword = "123",
                OptInEmailUserName = "tush",
                OptInEmailConfirmationTemplateId = "Temp87",
                IsVeeSubscribed = true,
                Programs = new List<ProgramSettings>()
                {
                    new ProgramSettings()
                    {
                        ProgramName = "Program2",
                        DoubleOptInRequired = true,
                        Insights = new List<InsightSettings>()
                        {
                            new InsightSettings()
                            {
                                InsightName = "Insights"
                            }
                        }
                    }
                }
            };
            var iCLientConfigFacade = new Mock<IClientConfigFacade>();
            iCLientConfigFacade.Setup(m => m.GetClientSettings(It.IsAny<int>())).Returns(clientSetting);
            return iCLientConfigFacade;
        }
        /// <summary>
        /// Returns Mock of IAmiReading
        /// </summary>
        /// <returns></returns>
        public static Mock<IAmiReading> GetAmiReadingMock()
        {
            var nccAmiReadingModel = new NccAmiReadingModel()
            {
                BatteryVoltage = "3.570440054",
                CommodityId = 3,
                CustomerId = "",
                MeterId = "96143503",
                AmiTimeStamp = DateTime.Now,
                ReadingValue = 0,
                Timezone = "PST",
                TransponderId = 46287586,
                TransponderPort = 1,
                UOMId = 3
            };
            var iAmiReading = new Mock<IAmiReading>();
            iAmiReading.Setup(m => m.GetAmiReading(It.IsAny<int>(), It.IsAny<string>(), It.IsAny<DateTime>())).Returns(Task.FromResult(nccAmiReadingModel));
            return iAmiReading;
        }
        /// <summary>
        /// Returns Mock of IAccountLookup
        /// </summary>
        /// <returns></returns>
        public static Mock<IAccountLookup> GetAccountLookupMock()
        {
            var iAccountLookUpMockObject = new Mock<IAccountLookup>();
            iAccountLookUpMockObject.Setup(m => m.GetAccountMeterDetails(It.IsAny<int>(), It.IsAny<string>()))
                .Returns(new List<AccountLookupModel>() { new AccountLookupModel() { AccountId = "" } });
            return iAccountLookUpMockObject;
        }
        /// <summary>
        /// Returns Mock of INotification
        /// </summary>
        /// <returns></returns>
        public static Mock<INotification> GetNotificationMock()
        {
            SmsTemplateModel smsTemplate = new SmsTemplateModel()
            {
                ClientId = 87,
                InsightTypeName = "InsightTypeName",
                Body = "Body-Sample-Data",
                TemplateId = "45"
            };
            var iNotification = new Mock<INotification>();
            iNotification.Setup(m => m.GetSmsTemplateFromDB(It.IsAny<int>(), It.IsAny<string>(), It.IsAny<string>()))
                .Returns(Task.FromResult(smsTemplate));
            iNotification.Setup(m => m.GetReplacedSmsTemplateForSubscriptionDoubleOptInConfirmation(It.IsAny<string>(), It.IsAny<SubscriptionModel>(), It.IsAny<ClientSettings>(), It.IsAny<CustomerModel>(), It.IsAny<System.Text.RegularExpressions.MatchCollection>()))
              .Returns("str");
            return iNotification;
        }

        /// <summary>
        /// Gets the CloudBlockBlob using container name and file name
        /// </summary>
        /// <param name="fileName">Name of the blob</param>
        /// <param name="containerName">Name of the container in which the blob resides</param>
        /// <returns></returns>
        public static ICloudBlob GetBlockBlob(string fileName, string containerName)
        {
            var container = CloudBlobClient.GetContainerReference(containerName);
            return container.GetBlockBlobReference(fileName);
        }

        /// <summary>
        /// CloudBlobClient private variable
        /// </summary>
        private static CloudBlobClient _cloudBlobClient;
        public static CloudBlobClient CloudBlobClient
        {
            get
            {
                if (_cloudBlobClient != null) return _cloudBlobClient;
                //var storageAccount =
                //    CloudStorageAccount.Parse(
                //        CloudConfigurationManager.GetSetting(""));
                var storageAccount =
                    CloudStorageAccount.Parse(
                        "DefaultEndpointsProtocol=https;AccountName=watermdmstgdev;AccountKey=NLiPTjsHibbVo6sPlgujN+qbHoh8gXBelSdlgKFWxzSiD4Mlx+mUoF/eLxUb4p0jCmpZyEiBjry1+GJbJawrRA==");
                _cloudBlobClient = storageAccount.CreateCloudBlobClient();
                return _cloudBlobClient;
            }
        }
    }
}
