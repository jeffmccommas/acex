﻿using System.Runtime.Remoting.Messaging;
using AO.BusinessContracts;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Enums = AO.BusinessContracts.Enums;
using System;
using System.Threading;
using CE.AO.Models;

namespace AO.Business.Tests.UnitTests
{
    /// <summary>
    /// Test Class for TimingMetricsCalculator
    /// </summary>
    [TestClass]
    public class TimingMetricsCalculatorTest
    {
        private ITimingMetricsCalculator _iTimingMetricsCalculator;

        /// <summary>
        /// Test Case Init
        /// </summary>
        [TestInitialize]
        public void Init()
        {
            _iTimingMetricsCalculator = new TimingMetricsCalculator();
        }

        /// <summary>
        /// Test Case Cleanup
        /// </summary>
        [TestCleanup]
        public void Cleanup()
        {
            CallContext.FreeNamedDataSlot("log4net.Util.LogicalThreadContextProperties");
        }

        /// <summary>
        /// Test method when stop is called before start
        /// </summary>
        [TestMethod]
        [ExpectedException(typeof(Exception))]
        public void StopTimingMetricsCalculator()
        {
            _iTimingMetricsCalculator.Stop(87, Enums.Metrics.AccountPreAggregation, new LogModel { DisableLog = true });
        }

        /// <summary>
        /// Test method for start, stop in TimingMetricsCalculator
        /// </summary>
        [TestMethod]
        public void StartStopTimingMetricsCalculator()
        {
            //Call when ClientMetricModels Dictionary is not populated.
            _iTimingMetricsCalculator.Start(87, Enums.Metrics.AccountPreAggregation);

            //Call when MetricModel is found in Dictionary.
            _iTimingMetricsCalculator.Start(87, Enums.Metrics.AccountPreAggregation);

            Thread.Sleep(TimeSpan.FromMilliseconds(10));

            _iTimingMetricsCalculator.Stop(87, Enums.Metrics.AccountPreAggregation, new LogModel { DisableLog = true });
        }
    }
}