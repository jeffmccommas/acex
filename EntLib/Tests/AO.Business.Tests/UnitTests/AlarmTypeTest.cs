﻿using System;
using System.Linq.Expressions;
using System.Threading.Tasks;
using AO.BusinessContracts;
using AO.DataAccess.Contracts;
using AO.Entities;
using CE.AO.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace AO.Business.Tests.UnitTests
{
    /// <summary>
    /// Test Class for AlarmType 
    /// </summary>
    [TestClass]
    public class AlarmTypeTest : TestBase
    {
        private AlarmType _alarmType;

        /// <summary>
        /// Test Case Init
        /// </summary>
        [TestInitialize]
        public void Init()
        {
            _alarmType = new AlarmType(LogModelMock.Object, new Lazy<ICassandraRepository>(()=>CassandraRepositoryMock.Object), new Lazy<ICacheProvider>(()=>CacheProviderMock.Object));
        }

        /// <summary>
        /// Test case for GetAlarmType method in AlarmType class
        /// </summary>
        [TestMethod]
        public void GetAlarmType()
        {
            var alarmTypeEntity = new AlarmTypeEntity
            {
                AlarmGroupId = 99898989,
                AlarmGroupName = "TestGroupName",
                AlarmSourceType = "TestSourceType",
                AlarmSourceTypeId = 59898989,
                AlarmTypeId = 49898989,
                AlarmTypeName = "TestAlarmTypeName"
            };

            CacheProviderMock.Setup(t => t.Get<AlarmTypeEntity>(It.IsAny<string>(), It.IsAny<int>()))
                .Returns((AlarmTypeEntity)null)
                .Verifiable();

            CacheProviderMock.Setup(t => t.Set(It.IsAny<string>(), It.IsAny<object>(), It.IsAny<int>())).Verifiable();

            CassandraRepositoryMock.Setup(
                t => t.GetSingle(It.IsAny<string>(), It.IsAny<Expression<Func<AlarmTypeEntity, bool>>>()))
                .Returns(alarmTypeEntity).Verifiable();

            _alarmType.GetAlarmType(alarmTypeEntity.AlarmSourceTypeId, alarmTypeEntity.AlarmGroupId, alarmTypeEntity.AlarmTypeId);

            CacheProviderMock.Setup(t => t.Get<AlarmTypeEntity>(It.IsAny<string>(), It.IsAny<int>()))
                .Returns(alarmTypeEntity)
                .Verifiable();

            var result = _alarmType.GetAlarmType(alarmTypeEntity.AlarmSourceTypeId, alarmTypeEntity.AlarmGroupId, alarmTypeEntity.AlarmTypeId);

            CassandraRepositoryMock.VerifyAll();
            CacheProviderMock.VerifyAll();

            Assert.AreEqual(result.AlarmTypeName, alarmTypeEntity.AlarmTypeName);
        }

        /// <summary>
        /// Test case for InsertOrMergeAlarmTypeAsync method in AlarmType class
        /// </summary>
        [TestMethod]
        public void InsertOrMergeAlarmTypeAsync()
        {
            CassandraRepositoryMock.Setup(
                t => t.InsertOrUpdateAsync(It.IsAny<AlarmTypeEntity>(), It.IsAny<string>()))
                .Returns(Task.FromResult(true));
            CacheProviderMock.Setup(t => t.Set(It.IsAny<string>(), It.IsAny<object>(), It.IsAny<int>())).Verifiable();

            var result = _alarmType.InsertOrMergeAlarmTypeAsync(new AlarmTypeModel()).Result;

            CassandraRepositoryMock.VerifyAll();
            CacheProviderMock.VerifyAll();

            Assert.IsTrue(result);
        }

        /// <summary>
        /// Test case for DeleteAlarmTypeAsync method in AlarmType class
        /// </summary>
        [TestMethod]
        public void DeleteAlarmTypeAsync()
        {
            CassandraRepositoryMock.Setup(t => t.DeleteAsync(It.IsAny<string>(), It.IsAny<Expression<Func<AlarmTypeEntity, bool>>>())).Returns(Task.FromResult(true));
            CacheProviderMock.Setup(t => t.Remove(It.IsAny<string>(), It.IsAny<int>())).Verifiable();

            var result = _alarmType.DeleteAlarmTypeAsync(new AlarmTypeModel()).Result;

            CassandraRepositoryMock.VerifyAll();
            CacheProviderMock.VerifyAll();

            Assert.IsTrue(result);
        }
    }
}
