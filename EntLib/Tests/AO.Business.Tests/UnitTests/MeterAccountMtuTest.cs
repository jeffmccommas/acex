﻿using System;
using System.Linq.Expressions;
using System.Threading.Tasks;
using AO.BusinessContracts;
using AO.DataAccess.Contracts;
using AO.Entities;
using CE.AO.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace AO.Business.Tests.UnitTests
{
    [TestClass]
    public class MeterAccountMtuTest : TestBase
    {
        private MeterAccountMtu _meterAccountMtu;

        /// <summary>
        /// Test Case Init
        /// </summary>
        [TestInitialize]
        public void Init()
        {
            var mtuTypeMappingMock = new Mock<IMtuTypeMapping>();
            _meterAccountMtu = new MeterAccountMtu(LogModelMock.Object,
                new Lazy<ICassandraRepository>(() => CassandraRepositoryMock.Object),
                new Lazy<IMtuTypeMapping>(() => mtuTypeMappingMock.Object));
        }

        /// <summary>
        /// Test Case for accessing Log Model
        /// </summary>
        [TestMethod]
        public void AccessLogModel()
        {
            _meterAccountMtu.LogModel.DisableLog = true;
            Assert.AreEqual(true, _meterAccountMtu.LogModel.DisableLog);
        }

        /// <summary>
        /// Test case for InsertOrMergeMeterAccountMtuAsync method in MeterAccountMtu class
        /// </summary>
        [TestMethod]
        public void InsertOrMergeMeterAccountMtuAsync()
        {
            CassandraRepositoryMock.Setup(
                t => t.InsertOrUpdateAsync(It.IsAny<MeterAccountHistoryEntity>(), It.IsAny<string>()))
                .Returns(Task.FromResult(true));
            var result = _meterAccountMtu.InsertOrMergeMeterAccountMtuAsync(new MeterAccountMtuModel()).Result;

            CassandraRepositoryMock.Verify(t => t.InsertOrUpdateAsync(It.IsAny<MeterAccountHistoryEntity>(), It.IsAny<string>()), Times.Once);
            Assert.IsTrue(result);
        }

        [TestMethod]
        public void DeleteMeterAccountMtu()
        {
            CassandraRepositoryMock.Setup(t => t.DeleteAsync(It.IsAny<string>(), It.IsAny<Expression<Func<MeterAccountHistoryEntity, bool>>>())).Returns(Task.FromResult(true));
            var result = _meterAccountMtu.DeleteMeterAccountMtuAsync(new MeterAccountMtuModel()).Result;
            CassandraRepositoryMock.Verify(t => t.Delete(It.IsAny<string>(), It.IsAny<Expression<Func<MeterAccountHistoryEntity, bool>>>()), Times.Never);
            Assert.IsTrue(result);
        }
    }
}
