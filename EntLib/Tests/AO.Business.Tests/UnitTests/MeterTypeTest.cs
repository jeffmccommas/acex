﻿using System;
using System.Linq.Expressions;
using System.Threading.Tasks;
using AO.Entities;
using CE.AO.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace AO.Business.Tests.UnitTests
{
    /// <summary>
    /// Test Class for MeterType 
    /// </summary>
    /// 
    [TestClass]
    public class MeterTypeTest : TestBase
    {
        private MeterType _meterType;

        /// <summary>
        /// Test Case Init
        /// </summary>
        [TestInitialize]
        public void Init()
        {
            _meterType = new MeterType(CassandraRepositoryMock.Object, CacheProviderMock.Object);
        }

        /// <summary>
        /// Test case for GetMeterType method in MeterType class
        /// </summary>
        [TestMethod]
        public void GetMeterType()
        {
            // Arrange
            var meterTypeEntity = new MeterTypeEntity
            {
                Active = true,
                Descripton = "Test Description",
                MeterReadingUnit = "Test Unit",
                MeterReadingUtility = "Test Utitlity",
                MeterTypeId = 9999999,
                Model = "Test Model",
                PsionDisplay = "Test Display",
                Registercount = 000,
                TableName = "TestColumnName",
                UnitEnumerator = 1212,
                UnitScalar = null,
                Vendor = "TEst Vendor"
            };

            CacheProviderMock.Setup(t => t.Get<MeterTypeEntity>(It.IsAny<string>(), It.IsAny<int>()))
                .Returns((MeterTypeEntity) null)
                .Verifiable();

            CacheProviderMock.Setup(t => t.Set(It.IsAny<string>(), It.IsAny<object>(), It.IsAny<int>())).Verifiable();

            CassandraRepositoryMock.Setup(
                t => t.GetSingle(It.IsAny<string>(), It.IsAny<Expression<Func<MeterTypeEntity, bool>>>()))
                .Returns(meterTypeEntity).Verifiable();

            _meterType.GetMeterType(meterTypeEntity.MeterTypeId);

            CacheProviderMock.Setup(t => t.Get<MeterTypeEntity>(It.IsAny<string>(), It.IsAny<int>()))
                .Returns(meterTypeEntity)
                .Verifiable();

            // Act
            var result = _meterType.GetMeterType(meterTypeEntity.MeterTypeId);

            // Assert
            Assert.AreEqual(result.MeterTypeId, meterTypeEntity.MeterTypeId);
            MockRepository.VerifyAll();
        }

        /// <summary>
        /// Test case for InsertOrMergeMeterTypeAsync method in MeterType class
        /// </summary>
        [TestMethod]
        public void InsertOrMergeMeterTypeAsync()
        {
            // Arrange
            CassandraRepositoryMock.Setup(
                t => t.InsertOrUpdateAsync(It.IsAny<MeterTypeEntity>(), It.IsAny<string>()))
                .Returns(Task.FromResult(true));
            CacheProviderMock.Setup(t => t.Set(It.IsAny<string>(), It.IsAny<object>(), It.IsAny<int>())).Verifiable();

            // Act
            _meterType.InsertOrMergeMeterTypeAsync(new MeterTypeModel()).Wait();

            // Assert
            MockRepository.VerifyAll();
        }

        /// <summary>
        /// Test case for DeleteMeterTypeAsync method in MeterType class
        /// </summary>
        [TestMethod]
        public void DeleteMeterTypeAsync()
        {
            // Arrange
            CassandraRepositoryMock.Setup(
                t => t.DeleteAsync(It.IsAny<string>(), It.IsAny<Expression<Func<MeterTypeEntity, bool>>>()))
                .Returns(Task.FromResult(true));
            CacheProviderMock.Setup(t => t.Remove(It.IsAny<string>(), It.IsAny<int>())).Verifiable();

            // Act
            _meterType.DeleteMeterTypeAsync(new MeterTypeModel()).Wait();

            // Assert
            MockRepository.VerifyAll();
        }

    }
}
