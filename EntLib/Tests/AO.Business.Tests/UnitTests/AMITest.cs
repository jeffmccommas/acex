﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using AO.DataAccess.Contracts;
using AO.Entities;
using CE.AO.Models;
using CE.RateModel;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Enums = CE.AO.Utilities.Enums;

namespace AO.Business.Tests.UnitTests
{
	/// <summary>
	/// Test cases for AMI class
	/// </summary>
	[TestClass]
	public class AmiTest : TestBase
	{
		private Ami _ami;

		/// <summary>
		/// Test Case Init
		/// </summary>
		[TestInitialize]
		public void Init()
		{
			_ami = new Ami(LogModelMock.Object, new Lazy<ICassandraRepository>(()=>CassandraRepositoryMock.Object));
			
		}

		/// <summary>
		/// Test case for inserting ami for all 3 models Ami15MinuteIntervalModel, Ami30MinuteIntervalModel, Ami60MinuteIntervalModel
		/// Test case for 3 methods InsertAmiAsync() of AMI class
		/// </summary>
		[TestMethod]
		public void InsertAmiAsync()
		{
			var ami15MinuteIntervalModel = new Ami15MinuteIntervalModel();
			var ami30MinuteIntervalModel = new Ami30MinuteIntervalModel();
			var ami60MinuteIntervalModel = new Ami60MinuteIntervalModel();

			CassandraRepositoryMock.Setup(
				t => t.InsertOrUpdateAsync(It.IsAny<Ami15MinuteIntervalEntity>(), It.IsAny<string>()))
				.Returns(Task.FromResult(typeof(void)));
			CassandraRepositoryMock.Setup(
				t => t.InsertOrUpdateAsync(It.IsAny<Ami30MinuteIntervalEntity>(), It.IsAny<string>()))
				.Returns(Task.FromResult(typeof(void)));
			CassandraRepositoryMock.Setup(
				t => t.InsertOrUpdateAsync(It.IsAny<Ami60MinuteIntervalEntity>(), It.IsAny<string>()))
				.Returns(Task.FromResult(typeof(void)));

			var result = _ami.InsertAmiAsync(ami15MinuteIntervalModel).Result;

			result = result && _ami.InsertAmiAsync(ami30MinuteIntervalModel).Result;
			result = result && _ami.InsertAmiAsync(ami60MinuteIntervalModel).Result;

			CassandraRepositoryMock.Verify(t => t.InsertOrUpdateAsync(It.IsAny<Ami15MinuteIntervalEntity>(), It.IsAny<string>()), Times.Once);
			CassandraRepositoryMock.Verify(t => t.InsertOrUpdateAsync(It.IsAny<Ami30MinuteIntervalEntity>(), It.IsAny<string>()), Times.Once);
			CassandraRepositoryMock.Verify(t => t.InsertOrUpdateAsync(It.IsAny<Ami60MinuteIntervalEntity>(), It.IsAny<string>()), Times.Once);

			Assert.IsTrue(result);
			MockRepository.VerifyAll();
		}

		/// <summary>
		/// Test case for inserting ami for InsertOrMergeAmiAsync method in AMI class
		/// Test case for InsertOrMergeAmiAsync method in AMI class
		/// </summary>
		[TestMethod]
		public void InsertOrMergeAmiAsync()
		{
			var ami15MinuteIntervalModel = new Ami15MinuteIntervalModel();
			var ami30MinuteIntervalModel = new Ami30MinuteIntervalModel();
			var ami60MinuteIntervalModel = new Ami60MinuteIntervalModel();

			CassandraRepositoryMock.Setup(
				t => t.InsertOrUpdateAsync(It.IsAny<Ami15MinuteIntervalEntity>(), It.IsAny<string>()))
				.Returns(Task.FromResult(typeof(void)));
			CassandraRepositoryMock.Setup(
				t => t.InsertOrUpdateAsync(It.IsAny<Ami30MinuteIntervalEntity>(), It.IsAny<string>()))
				.Returns(Task.FromResult(typeof(void)));
			CassandraRepositoryMock.Setup(
				t => t.InsertOrUpdateAsync(It.IsAny<Ami60MinuteIntervalEntity>(), It.IsAny<string>()))
				.Returns(Task.FromResult(typeof(void)));

			var result = _ami.InsertOrMergeAmiAsync(ami15MinuteIntervalModel, Enums.IntervalType.Fifteen).Result;
			result = result && _ami.InsertOrMergeAmiAsync(ami30MinuteIntervalModel, Enums.IntervalType.Thirty).Result;
			result = result && _ami.InsertOrMergeAmiAsync(ami60MinuteIntervalModel, Enums.IntervalType.Sixty).Result;

			CassandraRepositoryMock.Verify(t => t.InsertOrUpdateAsync(It.IsAny<Ami15MinuteIntervalEntity>(), It.IsAny<string>()), Times.Once);
			CassandraRepositoryMock.Verify(t => t.InsertOrUpdateAsync(It.IsAny<Ami30MinuteIntervalEntity>(), It.IsAny<string>()), Times.Once);
			CassandraRepositoryMock.Verify(t => t.InsertOrUpdateAsync(It.IsAny<Ami60MinuteIntervalEntity>(), It.IsAny<string>()), Times.Once);
			
			Assert.IsTrue(result);
			MockRepository.VerifyAll();
		}

		/// <summary>
		/// Test case to get list of ami model
		/// Test case for GetAmiList method in AMI class
		/// </summary>
		[TestMethod]
		public void GetAmiList()
		{
			var ami15MinuteIntervalEntity = new Ami15MinuteIntervalEntity
			{
				AccountNumber = "",
				AmiTimeStamp = DateTime.Now,
				ClientId = 87,
				IntervalType = 15
			};
			var ami30MinuteIntervalEntity = new Ami15MinuteIntervalEntity
			{
				AccountNumber = "",
				AmiTimeStamp = DateTime.Now,
				ClientId = 87,
				IntervalType = 30
			};
			var ami60MinuteIntervalEntity = new Ami15MinuteIntervalEntity
			{
				AccountNumber = "",
				AmiTimeStamp = DateTime.Now,
				ClientId = 87,
				IntervalType = 60
			};
			var ami15List = new List<Ami15MinuteIntervalEntity> { ami15MinuteIntervalEntity };
			var ami30List = new List<Ami15MinuteIntervalEntity> { ami30MinuteIntervalEntity };
			var ami60List = new List<Ami15MinuteIntervalEntity> { ami60MinuteIntervalEntity };

			// For 15 min interval data
			CassandraRepositoryMock.Setup(
				t => t.Get(It.IsAny<string>(), It.IsAny<Expression<Func<Ami15MinuteIntervalEntity, bool>>>()))
				.Returns(ami15List);

			var result15 = _ami.GetAmiList(It.IsAny<DateTime>(), It.IsAny<DateTime>(), It.IsAny<string>(), It.IsAny<int>()).First();

			CassandraRepositoryMock.Verify(t => t.Get(It.IsAny<string>(), It.IsAny<Expression<Func<Ami15MinuteIntervalEntity, bool>>>()), Times.Once);
			Assert.AreEqual(15, ((Ami15MinuteIntervalModel)result15).IntervalType);
			CassandraRepositoryMock.Reset();

			// For 30 min interval data
			CassandraRepositoryMock.Setup(
				t => t.Get(It.IsAny<string>(), It.IsAny<Expression<Func<Ami15MinuteIntervalEntity, bool>>>()))
				.Returns(ami30List);

			var result30 = _ami.GetAmiList(It.IsAny<DateTime>(), It.IsAny<DateTime>(), It.IsAny<string>(), It.IsAny<int>()).First();

			CassandraRepositoryMock.Verify(t => t.Get(It.IsAny<string>(), It.IsAny<Expression<Func<Ami15MinuteIntervalEntity, bool>>>()), Times.Once);
			Assert.AreEqual(30, ((Ami30MinuteIntervalModel)result30).IntervalType);
			CassandraRepositoryMock.Reset();

			// For 60 min interval data
			CassandraRepositoryMock.Setup(
				t => t.Get(It.IsAny<string>(), It.IsAny<Expression<Func<Ami15MinuteIntervalEntity, bool>>>()))
				.Returns(ami60List);

			var result60 = _ami.GetAmiList(It.IsAny<DateTime>(), It.IsAny<DateTime>(), It.IsAny<string>(), It.IsAny<int>()).First();
			CassandraRepositoryMock.Verify(t => t.Get(It.IsAny<string>(), It.IsAny<Expression<Func<Ami15MinuteIntervalEntity, bool>>>()), Times.Once);
			Assert.AreEqual(60, ((Ami60MinuteIntervalModel)result60).IntervalType);
			MockRepository.VerifyAll();
		}

		/// <summary>
		/// Test case to get single ami model.
		/// Test case for GetAmi in AMI class
		/// </summary>
		[TestMethod]
		public void GetAmi()
		{
			var ami15MinuteIntervalEntity = new Ami15MinuteIntervalEntity
			{
				AccountNumber = "",
				AmiTimeStamp = DateTime.Now,
				ClientId = 87,
				IntervalType = 15
			};
			var ami30MinuteIntervalEntity = new Ami15MinuteIntervalEntity
			{
				AccountNumber = "",
				AmiTimeStamp = DateTime.Now,
				ClientId = 87,
				IntervalType = 30
			};
			var ami60MinuteIntervalEntity = new Ami15MinuteIntervalEntity
			{
				AccountNumber = "",
				AmiTimeStamp = DateTime.Now,
				ClientId = 87,
				IntervalType = 60
			};

			// For 15 min interval data
			CassandraRepositoryMock.Setup(t => t.GetSingleAsync(It.IsAny<string>(), It.IsAny<Expression<Func<Ami15MinuteIntervalEntity, bool>>>())).Returns(Task.FromResult(ami15MinuteIntervalEntity));

			var result15 = _ami.GetAmi(It.IsAny<DateTime>(), It.IsAny<string>(), It.IsAny<int>()).Result;
			CassandraRepositoryMock.Verify(t => t.GetSingleAsync(It.IsAny<string>(), It.IsAny<Expression<Func<Ami15MinuteIntervalEntity, bool>>>()),Times.Once);
			Assert.AreEqual(((Ami15MinuteIntervalModel)result15).IntervalType, 15);
			CassandraRepositoryMock.Reset();

			// For 30 min interval data
			CassandraRepositoryMock.Setup(t => t.GetSingleAsync(It.IsAny<string>(), It.IsAny<Expression<Func<Ami15MinuteIntervalEntity, bool>>>())).Returns(Task.FromResult(ami30MinuteIntervalEntity));

			var result30 = _ami.GetAmi(It.IsAny<DateTime>(), It.IsAny<string>(), It.IsAny<int>()).Result;
			CassandraRepositoryMock.Verify(t => t.GetSingleAsync(It.IsAny<string>(), It.IsAny<Expression<Func<Ami15MinuteIntervalEntity, bool>>>()), Times.Once);
			Assert.AreEqual(((Ami30MinuteIntervalModel)result30).IntervalType, 30);
			CassandraRepositoryMock.Reset();

			// For 60 min interval data
			CassandraRepositoryMock.Setup(t => t.GetSingleAsync(It.IsAny<string>(), It.IsAny<Expression<Func<Ami15MinuteIntervalEntity, bool>>>())).Returns(Task.FromResult(ami60MinuteIntervalEntity));

			var result60 = _ami.GetAmi(It.IsAny<DateTime>(), It.IsAny<string>(), It.IsAny<int>()).Result;
			CassandraRepositoryMock.Verify(t => t.GetSingleAsync(It.IsAny<string>(), It.IsAny<Expression<Func<Ami15MinuteIntervalEntity, bool>>>()), Times.Once);
			Assert.AreEqual(((Ami60MinuteIntervalModel)result60).IntervalType, 60);
			CassandraRepositoryMock.Reset();
			MockRepository.VerifyAll();
		}

		/// <summary>
		/// Test case to get list of readings.
		/// Test case includes 2 methods (GetReadings) in AMI class
		/// </summary>
		[TestMethod]
		public void GetReadings()
		{
			var ami15MinuteIntervalModel = new Ami15MinuteIntervalModel
			{
				AccountNumber = "",
				AmiTimeStamp = DateTime.Now,
				ClientId = 87,
				UOMId = 5,
				IntervalType = 15,
				IntValue0000 = 10,
				IntValue0015 = 15,
				IntValue0030 = 30,
				IntValue0045 = 45,
				IntValue0100 = 100
			};
			var ami30MinuteIntervalModel = new Ami30MinuteIntervalModel
			{
				AccountNumber = "",
				AmiTimeStamp = DateTime.Now,
				ClientId = 87,
				UOMId = 6,
				IntervalType = 30,
				IntValue0000 = 10,
				IntValue0030 = 30,
				IntValue0100 = 100
			};
			var ami60MinuteIntervalModel = new Ami60MinuteIntervalModel
			{
				AccountNumber = "",
				AmiTimeStamp = DateTime.Now,
				ClientId = 87,
				UOMId = 7,
				IntervalType = 60,
				IntValue0000 = 10,
				IntValue0100 = 100
			};
			var ami15List = new List<Ami15MinuteIntervalModel> { ami15MinuteIntervalModel };
			var ami30List = new List<Ami30MinuteIntervalModel> { ami30MinuteIntervalModel };
			var ami60List = new List<Ami60MinuteIntervalModel> { ami60MinuteIntervalModel };
			int noOfDays;
			List<Reading> result = _ami.GetReadings(ami15List, out noOfDays);

			Assert.AreEqual(noOfDays, 1);
			Assert.AreEqual(result.Count, 96);

			List<ReadingModel> result2 = _ami.GetReadings(ami15List);
			List<ReadingModel> result3 = _ami.GetReadings(ami30List);
			List<ReadingModel> result4 = _ami.GetReadings(ami60List);
			Assert.AreEqual(result2.First().UOMId, 5);
			Assert.AreEqual(result2.Count, 96);
			Assert.AreEqual(result3.First().UOMId, 6);
			Assert.AreEqual(result3.Count, 48);
			Assert.AreEqual(result4.First().UOMId, 7);
			Assert.AreEqual(result4.Count, 24);
			MockRepository.VerifyAll();
		}

		/// <summary>
		/// Test case to get list of daily readings
		/// Test case for GetDailyReadings method in AMI class
		/// </summary>
		[TestMethod]
		public void GetDailyReadings()
		{
			var ami15List = new List<Ami15MinuteIntervalModel>
			{
				new Ami15MinuteIntervalModel
			{
				AccountNumber = "",
				AmiTimeStamp = DateTime.Now,
				ClientId = 87,
				UOMId = 5,
				IntervalType = 15,
				IntValue0000 = 10,
				IntValue0015 = 15,
				IntValue0030 = 30,
				IntValue0045 = 45,
				IntValue0100 = 100
			},
				new Ami15MinuteIntervalModel
			{
				AccountNumber = "",
				AmiTimeStamp = DateTime.Now.AddDays(-1),
				ClientId = 87,
				UOMId = 5,
				IntervalType = 15,
				IntValue0000 = 10,
				IntValue0015 = 15,
				IntValue0030 = 30,
				IntValue0045 = 45,
				IntValue0100 = 100
			}
			};
			List<Reading> result = _ami.GetDailyReadings(ami15List, 15);

			Assert.AreEqual(result.Count, 2);
			Assert.AreEqual(result.First().Quantity, 200);
			MockRepository.VerifyAll();
		}

		/// <summary>
		/// Test case to get list of Monthly readings
		/// Test case for GetMonthlyReadings method in AMI class
		/// </summary>
		[TestMethod]
		public void GetMonthlyReadings()
		{
			var ami15List = new List<Ami15MinuteIntervalModel>
			{
				new Ami15MinuteIntervalModel
			{
				AccountNumber = "",
				AmiTimeStamp = new DateTime(2016,08,02),
				ClientId = 87,
				UOMId = 5,
				IntervalType = 15,
				IntValue0000 = 10,
				IntValue0015 = 15,
				IntValue0030 = 30,
				IntValue0045 = 45,
				IntValue0100 = 100
			},
				new Ami15MinuteIntervalModel
			{
				AccountNumber = "",
				AmiTimeStamp = new DateTime(2016,08,01),
				ClientId = 87,
				UOMId = 5,
				IntervalType = 15,
				IntValue0000 = 10,
				IntValue0015 = 15,
				IntValue0030 = 30,
				IntValue0045 = 45,
				IntValue0100 = 100
			}
			};

			List<Reading> result = _ami.GetMonthlyReadings(ami15List, 15);

			Assert.AreEqual(result.Count,1);
			Assert.AreEqual(result.First().Quantity,400);
			MockRepository.VerifyAll();
		}
	}
}