﻿using System;
using System.Linq.Expressions;
using System.Threading.Tasks;
using AO.Entities;
using CE.AO.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace AO.Business.Tests.UnitTests
{
    [TestClass]
    public class DcuCallDataTest : TestBase
    {
        private DcuCallData _dcuCallData;

        /// <summary>
        /// Test Case Init
        /// </summary>
        [TestInitialize]
        public void Init()
        {
            _dcuCallData = new DcuCallData(CassandraRepositoryMock.Object);
        }

        /// <summary>
        /// Test case for InsertOrMergeDcuCallDataAsync method in DcuCallData class
        /// </summary>
        [TestMethod]
        public void InsertOrMergeDcuCallDataAsync()
        {
            // Arrange
            CassandraRepositoryMock.Setup(
                t => t.InsertOrUpdateAsync(It.IsAny<DcuCallDataHistoryEntity>(), It.IsAny<string>()))
                .Returns(Task.FromResult(true));

            // Act
            _dcuCallData.InsertOrMergeDcuCallDataAsync(new DcuCallDataModel()).Wait();

            // Assert
            CassandraRepositoryMock.Verify(t => t.InsertOrUpdateAsync(It.IsAny<DcuCallDataHistoryEntity>(), It.IsAny<string>()), Times.Once);
            MockRepository.VerifyAll();
        }

        /// <summary>
        /// To delete the DcuCallDataModel from dcu_calldata_history table.
        /// </summary>
        [TestMethod]
        public void DeleteDcuCallDataAsync()
        {
            // Arrange
            CassandraRepositoryMock.Setup(t => t.DeleteAsync(It.IsAny<string>(), It.IsAny<Expression<Func<DcuCallDataHistoryEntity, bool>>>())).Returns(Task.FromResult(true));
            
            // Act
            _dcuCallData.DeleteDcuCallDataAsync(new DcuCallDataModel()).Wait();
            
            // Assert
            CassandraRepositoryMock.Verify(t => t.Delete(It.IsAny<string>(), It.IsAny<Expression<Func<DcuCallDataHistoryEntity, bool>>>()), Times.Never);
            MockRepository.VerifyAll();
        }
    }
}
