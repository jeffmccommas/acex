﻿using System;
using System.Linq.Expressions;
using System.Threading.Tasks;
using AO.DataAccess.Contracts;
using AO.Entities;
using CE.AO.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System.Collections.Generic;
using System.Linq;

namespace AO.Business.Tests.UnitTests
{
    /// <summary>
    /// Test cases for ConsumptionAggByAccount class 
    /// </summary>
    [TestClass]
    public class ConsumptionAggByAccountTest : TestBase
    {
        private ConsumptionAggByAccount _consumptionAggByAccount;

        /// <summary>
        /// Test Case Init
        /// </summary>
        [TestInitialize]
        public void Init()
        {
            _consumptionAggByAccount = new ConsumptionAggByAccount(LogModelMock.Object,
                new Lazy<ICassandraRepository>(() => CassandraRepositoryMock.Object));
        }

        /// <summary>
        /// Test case to insert ConsumptionAggByAccount Model
        /// Test case for InsertOrMergeConsumptionAggByAccountAsync method for ConsumptionAggByAccount class
        /// </summary>
        [TestMethod]
        public void InsertOrMergeConsumptionAggByAccountAsync()
        {
            CassandraRepositoryMock.Setup(t => t.InsertOrUpdateAsync(It.IsAny<ConsumptionAggByAccountEntity>(), It.IsAny<string>()))
                .Returns(Task.FromResult(true)).Verifiable();
            var consumptionAggByAccountModel = new ConsumptionAggByAccountModel();
            var result = _consumptionAggByAccount.InsertOrMergeConsumptionAggByAccountAsync(consumptionAggByAccountModel).Result;
            CassandraRepositoryMock.Verify(t => t.InsertOrUpdateAsync(It.IsAny<ConsumptionAggByAccountEntity>(), It.IsAny<string>()), Times.Once);
            CassandraRepositoryMock.VerifyAll();
            Assert.AreEqual(true, result);
            MockRepository.VerifyAll();
        }

        /// <summary>
        /// Test case to insert List of ConsumptionAggByAccount Model in batch
        /// Test case for InsertOrMergeConsumptionAggByAccountAsync method for ConsumptionAggByAccount class
        /// </summary>
        [TestMethod]
        public void InsertOrMergeConsumptionAggByAccountInBatchAsync()
        {
            CassandraRepositoryMock.Setup(t => t.InsertOrUpdateBatchAsync(It.IsAny<List<ConsumptionAggByAccountEntity>>(), It.IsAny<string>()))
                .Returns(Task.FromResult(true)).Verifiable();
            List<ConsumptionAggByAccountModel> listofConsumptionAggByAccountEntity = new List<ConsumptionAggByAccountModel>();
            var result = _consumptionAggByAccount.InsertOrMergeConsumptionAggByAccountAsync(listofConsumptionAggByAccountEntity).Result;
            CassandraRepositoryMock.Verify(t => t.InsertOrUpdateBatchAsync(It.IsAny<List<ConsumptionAggByAccountEntity>>(), It.IsAny<string>()), Times.Once);
            CassandraRepositoryMock.VerifyAll();

            Assert.AreEqual(true, result);
            MockRepository.VerifyAll();
        }

        /// <summary>
        /// Test case to delete ConsumptionAggByAccount Model
        /// Test case for DeleteConsumptionAggByAccountAsync method for ConsumptionAggByAccount class
        /// </summary>
        [TestMethod]
        public void DeleteConsumptionAggByAccountAsync()
        {
            // Arrange
            CassandraRepositoryMock.Setup(t => t.DeleteAsync(It.IsAny<string>(), It.IsAny<Expression<Func<ConsumptionAggByAccountEntity, bool>>>()))
                .Returns(Task.FromResult(typeof(bool))).Verifiable();

            var consumptionAggByAccountModel = new ConsumptionAggByAccountModel();

            // Act
            _consumptionAggByAccount.DeleteConsumptionAggByAccountAsync(consumptionAggByAccountModel).Wait();

            // Assert
            CassandraRepositoryMock.Verify(t => t.DeleteAsync(It.IsAny<string>(), It.IsAny<Expression<Func<ConsumptionAggByAccountEntity, bool>>>()), Times.Once);
            MockRepository.VerifyAll();
        }

        /// <summary>
        /// Test case to delete ConsumptionAggByAccount Model using Partition
        /// Test case for DeleteConsumptionAggByAccountAsync method for ConsumptionAggByAccount class
        /// </summary>
        [TestMethod]
        public void DeleteConsumptionAggByAccountUsingPartitionAsync()
        {
            // Arrange
            CassandraRepositoryMock.Setup(t => t.DeleteAsync(It.IsAny<string>(), It.IsAny<Expression<Func<ConsumptionAggByAccountEntity, bool>>>()))
                .Returns(Task.FromResult(typeof(bool))).Verifiable();

            var consumptionAggByAccountModel = new ConsumptionAggByAccountModel();

            // Act
            _consumptionAggByAccount.DeleteConsumptionAggByAccountUsingPartionAsync(consumptionAggByAccountModel).Wait();

            // Assert
            CassandraRepositoryMock.Verify(t => t.DeleteAsync(It.IsAny<string>(), It.IsAny<Expression<Func<ConsumptionAggByAccountEntity, bool>>>()), Times.Once);
            MockRepository.VerifyAll();
        }

        /// <summary>
        /// Test case to get ConsumptionAggByAccountModel for AmiTimeStamp greater then or equal to startDateTime and AmiTimeStamp less than endDateTime
        /// </summary>
        [TestMethod]
        public void GetConsumptionAggByAccountAsync()
        {
            // Arrange
            IList<ConsumptionAggByAccountEntity> consumptionAggByAccountEntityList = new List<ConsumptionAggByAccountEntity>()
            {
                new ConsumptionAggByAccountEntity()
                {
                    StandardUOMId = 3,
                    AccountNumber = "Test Account",
                    ClientId = 87,
                    CommodityId = 3,
                    Timezone = "PST",
                    StandardUOMConsumption = 10,
                    AggregationEndDateTime = DateTime.UtcNow,
                    AggregationType = "Test Aggregation"
                }
            };

            CassandraRepositoryMock.Setup(t => t.GetAsync(It.IsAny<string>(),
                It.IsAny<Expression<Func<ConsumptionAggByAccountEntity, bool>>>())).Returns(Task.FromResult(consumptionAggByAccountEntityList));

            // Act
            var consumptionAggByAccountList =
                _consumptionAggByAccount.GetConsumptionAggByAccountAsync(consumptionAggByAccountEntityList[0].ClientId,
                    consumptionAggByAccountEntityList[0].AggregationEndDateTime, consumptionAggByAccountEntityList[0].AccountNumber);
            var result = consumptionAggByAccountList.Result.FirstOrDefault();

            // Assert
            CassandraRepositoryMock.Verify(
                t => t.GetAsync(It.IsAny<string>(),
                    It.IsAny<Expression<Func<ConsumptionAggByAccountEntity, bool>>>()), Times.Once);
            Assert.IsNotNull(result);
            Assert.AreEqual(result.AccountNumber, "Test Account");
            MockRepository.VerifyAll();
        }
    }
}
