﻿using System;
using System.Linq.Expressions;
using System.Threading.Tasks;
using AO.Entities;
using CE.AO.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace AO.Business.Tests.UnitTests
{
    [TestClass]
    public class DcuAlarmCodeHistoryTest : TestBase
    {
        private DcuAlarmCodeHistory _dcuAlarmCodeHistory;

        [TestInitialize]
        public void Init()
        {
            _dcuAlarmCodeHistory = new DcuAlarmCodeHistory(LogModelMock.Object, CassandraRepositoryMock.Object);
        }

        /// <summary>
        /// Test case for InsertOrMergeDcuAlarmCodeHistoryAsync method in DcuAlarmCodeHistory class
        /// </summary>
        [TestMethod]
        public void InsertDcuAlarmByCodeHistoryEntityuAsync()
        {
            // Arrange
            CassandraRepositoryMock.Setup(
                t => t.InsertOrUpdateAsync(It.IsAny<DcuAlarmByCodeHistoryEntity>(), It.IsAny<string>()))
                .Returns(Task.FromResult(true));

            // Act
            var result = _dcuAlarmCodeHistory.InsertOrMergeDcuAlarmCodeHistoryAsync(new DcuAlarmModel()).Result;

            // Assert
            CassandraRepositoryMock.Verify(t => t.InsertOrUpdateAsync(It.IsAny<DcuAlarmByCodeHistoryEntity>(), It.IsAny<string>()), Times.Once);
            Assert.IsTrue(result);
            MockRepository.VerifyAll();
        }

        /// <summary>
        /// Test case for DeleteDcuAlarmCodeHistoryAsync method in DcuAlarmCodeHistory class
        /// </summary>
        [TestMethod]
        public void DeleteDcuAlarmByCodeHistoryEntity()
        {
            // Arrange
            CassandraRepositoryMock.Setup(t => t.DeleteAsync(It.IsAny<string>(), It.IsAny<Expression<Func<DcuAlarmByCodeHistoryEntity, bool>>>())).Returns(Task.FromResult(true));

            // Act
            var result = _dcuAlarmCodeHistory.DeleteDcuAlarmCodeHistoryAsync(new DcuAlarmModel()).Result;

            // Assert
            CassandraRepositoryMock.Verify(t => t.Delete(It.IsAny<string>(), It.IsAny<Expression<Func<DcuAlarmByCodeHistoryEntity, bool>>>()), Times.Never);
            Assert.IsTrue(result);
            MockRepository.VerifyAll();
        }

        /// <summary>
        /// Test case for DeleteDcuAlarmCodeWithPartitionHistoryAsync method in DcuAlarmCodeHistory class
        /// </summary>
        [TestMethod]
        public void DeleteDcuAlarmByCodebyPartitionHistoryEntity()
        {
            // Arrange
            CassandraRepositoryMock.Setup(t => t.DeleteAsync(It.IsAny<string>(), It.IsAny<Expression<Func<DcuAlarmByCodeHistoryEntity, bool>>>())).Returns(Task.FromResult(true));

            // Act
            var result = _dcuAlarmCodeHistory.DeleteDcuAlarmCodeWithPartitionHistoryAsync(new DcuAlarmModel()).Result;

            // Assert
            CassandraRepositoryMock.Verify(t => t.Delete(It.IsAny<string>(), It.IsAny<Expression<Func<DcuAlarmByCodeHistoryEntity, bool>>>()), Times.Never);
            Assert.IsTrue(result);
            MockRepository.VerifyAll();
        }

    }
}
