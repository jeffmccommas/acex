﻿using System;
using System.Threading.Tasks;
using AO.BusinessContracts;
using CE.AO.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace AO.Business.Tests.UnitTests
{

    /// <summary>
    /// Test Class for MeterAccountMtuFacade Class
    /// </summary>

    [TestClass]
    public class MeterAccountMtuFacadeTest : TestBase
    {
        private MeterAccountMtuFacade _meterAccountMtuFacade;
        private Mock<IMeterAccountMtu> _meterAccountMtuMock;
		private Mock<IMeterHistoryByMeter> _meterHistoryByMeter;
		private Mock<IMeterType> _meterTypeMock;

        /// <summary>
        /// Test Case Init
        /// </summary>
        [TestInitialize]
        public void Init()
        {
            _meterAccountMtuMock = new Mock<IMeterAccountMtu>();
            _meterTypeMock = new Mock<IMeterType>();
			_meterHistoryByMeter = new Mock<IMeterHistoryByMeter>();

			_meterAccountMtuFacade = new MeterAccountMtuFacade(LogModelMock.Object, _meterAccountMtuMock.Object, _meterTypeMock.Object, _meterHistoryByMeter.Object);
        }

        /// <summary>
        /// Test case for InsertOrMergeMeterAccountAsync method for MeterAccountMtuFacade class
        /// </summary>
        [TestMethod]
        public void InsertOrMergeMeterAccountAsync()
        {
            var meterAccountMtuModel = new MeterAccountMtuModel
            {
                ClientId = 87,
                MeterId = 12,
                AccountNumber = "TestAccountNo2",
                Active = false,
                CrossReferenceId = 1,
                Latitude = "12:12",
                Longitude = "12:12",
                MeterInstallDate = DateTime.UtcNow,
                MeterSerialNumber = "MSerialNo12",
                MeterTypeId = 12,
                MtuId = 12,
                MtuTypeId = 10, // WRE MTU Type
                MtuInstallDate = DateTime.UtcNow
            };

            _meterAccountMtuMock.Setup(t => t.InsertOrMergeMeterAccountMtuAsync(It.IsAny<MeterAccountMtuModel>()))
                .Returns(Task.FromResult(true));

            _meterHistoryByMeter.Setup(t => t.InsertOrMergeMeterHistoryByMeterAsync(It.IsAny<MeterHistoryByMeterModel>()))
                .Returns(Task.FromResult(true));

            _meterTypeMock.Setup(t => t.GetMeterType(It.IsAny<int>()))
                .Returns((new MeterTypeModel()));

            var result = _meterAccountMtuFacade.InsertOrMergeMeterAccountAsync(meterAccountMtuModel).Result;
			MockRepository.VerifyAll();
			Assert.AreEqual(result, true);
        }
	}
}
