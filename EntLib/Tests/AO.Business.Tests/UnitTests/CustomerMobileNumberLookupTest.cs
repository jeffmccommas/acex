﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;
using AO.BusinessContracts;
using AO.DataAccess.Contracts;
using AO.Entities;
using CE.AO.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace AO.Business.Tests.UnitTests
{
	/// <summary>
	/// Contains test methods for CustomerMobileNumberLookup class
	/// </summary>
	[TestClass]
	public class CustomerMobileNumberLookupTest : TestBase
	{
		private ICustomerMobileNumberLookup _customerMobileNumberLookupManager;

		/// <summary>
		/// Test Case Init
		/// </summary>
		[TestInitialize]
		public void Init()
		{
			_customerMobileNumberLookupManager = new CustomerMobileNumberLookup(LogModelMock.Object, new Lazy<ICassandraRepository>(()=>CassandraRepositoryMock.Object));
		}

		/// <summary>
		/// Test case to get customer mobile number
		/// </summary>
		[TestMethod]
		public void GetCustomerMobileNumberLookup()
		{
			var customerMobileNumberEntity = new CustomerMobileNumberEntity()
			{
				ClientId = 1,
				CustomerId = "1",
				AccountId = "123456789",
				MobileNumber = "9911559988"
			};

			CassandraRepositoryMock.Setup(t=>t.GetSingle(It.IsAny<string>(),It.IsAny<Expression<Func<CustomerMobileNumberEntity, bool>>>())).Returns(customerMobileNumberEntity).Verifiable();          

			CustomerMobileNumberLookupModel result = _customerMobileNumberLookupManager.GetCustomerMobileNumberLookup(1, "9911559988", "123456789");
			CassandraRepositoryMock.Verify(
				t => t.GetSingle(It.IsAny<string>(), It.IsAny<Expression<Func<CustomerMobileNumberEntity, bool>>>()), Times.Once);
			Assert.AreEqual(result.CustomerId,"1");
			MockRepository.VerifyAll();
		}

		/// <summary>
		/// Test case to get all customers mobile number
		/// </summary>
		[TestMethod]
		public void GetAllCustomerMobileNumberLookup()
		{
			var customerMobileNumberEntity = new CustomerMobileNumberEntity
			{
				ClientId = 1,
				CustomerId = "1",
				AccountId = "123456789",
				MobileNumber = "9911559988"
			};
			var list = new List<CustomerMobileNumberEntity> {customerMobileNumberEntity};

			CassandraRepositoryMock.Setup(t => t.Get(It.IsAny<string>(), It.IsAny<Expression<Func<CustomerMobileNumberEntity, bool>>>())).Returns(list).Verifiable();     

			List<CustomerMobileNumberLookupModel> result = _customerMobileNumberLookupManager.GetAllCustomerMobileNumberLookup(1, "9911559988");
			CassandraRepositoryMock.Verify(
				t => t.Get(It.IsAny<string>(), It.IsAny<Expression<Func<CustomerMobileNumberEntity, bool>>>()), Times.Once);
			Assert.AreEqual(result.Count, 1);
			MockRepository.VerifyAll();
		}

		/// <summary>
		/// Test Case to insert customer mobile number
		/// </summary>
		[TestMethod]
		public void InsertOrMergeCustomerMobileNumberLookupAsync()
		{
			var customerMobileNumberLookupModel = new CustomerMobileNumberLookupModel
			{
				ClientId = 87,
				AccountId = "a5",               
				CustomerId = "1012",
				MobileNumber = "9911559988"
			};

			CassandraRepositoryMock.Setup(t => t.InsertOrUpdateAsync(It.IsAny<CustomerMobileNumberEntity>(), It.IsAny<string>()))
				.Returns(Task.FromResult(true))
				.Verifiable();           

			_customerMobileNumberLookupManager.InsertOrMergeCustomerMobileNumberLookupAsync(customerMobileNumberLookupModel).Wait();

			CassandraRepositoryMock.Verify(
				t => t.InsertOrUpdateAsync(It.IsAny<CustomerMobileNumberEntity>(), It.IsAny<string>()), Times.Once);
			MockRepository.VerifyAll();
		}

		/// <summary>
		/// Test case to delete customer mobile number
		/// Test case for DeleteCustomerMobileNumberLookupAsync method from CustomerMobileNumberLookup class
		/// </summary>
		[TestMethod]
		public void DeleteCustomerMobileNumberLookupAsync()
		{
			CassandraRepositoryMock.Setup(
				t => t.DeleteAsync(It.IsAny<string>(), It.IsAny<Expression<Func<CustomerMobileNumberEntity, bool>>>()))
				.Returns(Task.FromResult(typeof(void)));

			_customerMobileNumberLookupManager.DeleteCustomerMobileNumberLookupAsync(It.IsAny<int>(), It.IsAny<string>(), It.IsAny<string>()).Wait();

			CassandraRepositoryMock.Verify(t => t.DeleteAsync(It.IsAny<string>(), It.IsAny<Expression<Func<CustomerMobileNumberEntity, bool>>>()),Times.Once);
			MockRepository.VerifyAll();
		}

	}
}
