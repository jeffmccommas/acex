﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AO.BusinessContracts;
using AO.Entities;
using AO.EventProcessors;
using CE.AO.Models;
using CE.AO.Utilities;
using Microsoft.ServiceBus.Messaging;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Newtonsoft.Json;
using Enums = CE.AO.Utilities.Enums;

namespace AO.Business.Tests.UnitTests
{
    /// <summary>
    /// Test Class for EventHubLogToCassandraTriggerSubscriber
    /// </summary>
    [TestClass]
    public class EventHubLogToCassandraTriggerSubscriberTest
    {
        private EventHubLogToCassandraTriggerSubscriber _eventHubLogToCassandraTriggerSubscriberTest;
        private Mock<IOperationsFacade> _iOperationsFacade;
        private Mock<EventProcessorFactory<EventHubLogToCassandraTriggerSubscriber>> _eventProcessorFactoryMock;

        /// <summary>
        /// Test Case Init
        /// </summary>
        [TestInitialize]
        public void Init()
        {
            _iOperationsFacade = new Mock<IOperationsFacade>();
            _eventProcessorFactoryMock = new Mock<EventProcessorFactory<EventHubLogToCassandraTriggerSubscriber>>();

            _eventHubLogToCassandraTriggerSubscriberTest =
                new EventHubLogToCassandraTriggerSubscriber(
                    new Lazy<IOperationsFacade>(() => _iOperationsFacade.Object),
                    new Lazy<EventProcessorFactory<EventHubLogToCassandraTriggerSubscriber>>(
                        () => _eventProcessorFactoryMock.Object));
        }

        /// <summary>
        /// Test Case for ProcessEventsAsync in EventHubLogToCassandraTriggerSubscriber Class
        /// </summary>
        [TestMethod]
        public void ProcessEventsAsync()
        {
            _iOperationsFacade.Setup(p => p.InsertOrMergeLogAsync(It.IsAny<LogViewModel>()))
                .Returns(Task.FromResult(true));

            LogEntity logentity = new LogEntity()
            {
                ClientId = "898989",
                MeterId = "TestMeter",
                AccountId = "TestAccount",
                CustomerId = "23424",
                ProcessingEventCount = 12,
                TimestampUtc = DateTimeOffset.UtcNow,
                Level = "Info"
            };

            var serializedEventData = JsonConvert.SerializeObject(logentity);
            var eventData = new EventData(Encoding.UTF8.GetBytes(serializedEventData));
            var context = new PartitionContext();
            var lease = new Lease();
            context.Lease = lease;
            context.Lease.Offset = "121212";
            context.Lease.PartitionId = "0";
            context.ConsumerGroupName = "$Default";
            context.EventHubPath = "watermdmTest";
            eventData.Properties.Add("ProcessingType", Enums.ProcessingType.File);
            eventData.Properties.Add("Source", "S");
            eventData.Properties.Add("RowIndex", 1);

            var eventDataList = new List<EventData> { eventData };
            // ReSharper disable once UnusedVariable
            var isOpen = _eventHubLogToCassandraTriggerSubscriberTest.OpenAsync(context);
            var isProcessCompleted = _eventHubLogToCassandraTriggerSubscriberTest.ProcessEventsAsync(context, eventDataList.AsEnumerable());
            Assert.IsTrue(isProcessCompleted.IsCompleted);
        }

    }
}