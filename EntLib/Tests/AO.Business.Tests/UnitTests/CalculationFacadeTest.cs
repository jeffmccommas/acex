﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Threading.Tasks;
using AO.BusinessContracts;
using CE.AO.Models;
using CE.AO.Utilities;
using CE.BillToDate;
using CE.RateModel;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace AO.Business.Tests.UnitTests
{
    [TestClass]
    public class CalculationFacadeTest : TestBase
    {
        private CalculationFacade _calculationFacade;
        private Mock<ICalculation> _calculationMock;
        private Mock<IBilling> _billingMock;
        private Mock<IAmi> _amiMock;
        private Mock<IBillingCycleSchedule> _billingCycleScheduleMock;
        private Mock<IAccountLookup> _accountLookupMock;
        private Mock<IAccountUpdates> _accountUpdatesMock;
        private Mock<IQueueManager> _queueManagerMock;
        private Mock<IClientAccount> _clientAccountMock;
        private Mock<IClientConfigFacade> _iClientConfigFacade;
        private Mock<ITallAMI> _iTallAmi;
        private Mock<ICalculationManagerFactory> _iCalculationManagerFactory;
        private Mock<IBillToDate> _billToDateMock;
        private Mock<CostToDateResult> _costToDateResultMock;
        /// <summary>
        /// Test Case Init
        /// </summary>
        [TestInitialize]
        public void Init()
        {
            _calculationFacade = new CalculationFacade(LogModelMock.Object);
            _calculationMock = new Mock<ICalculation>();
            _billingMock = new Mock<IBilling>();
            _amiMock = new Mock<IAmi>();
            _billingCycleScheduleMock = new Mock<IBillingCycleSchedule>();
            _accountLookupMock = new Mock<IAccountLookup>();
            _accountUpdatesMock = new Mock<IAccountUpdates>();
            _queueManagerMock = new Mock<IQueueManager>();
            _clientAccountMock = new Mock<IClientAccount>();
            _iClientConfigFacade = new Mock<IClientConfigFacade>();
            _iTallAmi = new Mock<ITallAMI>();
            _iCalculationManagerFactory = new Mock<ICalculationManagerFactory>();
            _billToDateMock = new Mock<IBillToDate>();
            _costToDateResultMock = new Mock<CostToDateResult>();
        }

        /// <summary>
        /// Test case for CalculateCostToDate method in CalculationFacade class
        /// </summary>
        [TestMethod]
        public void CalculateCostToDate()
        {
            TallAmiModel tallAMiModel = new TallAmiModel()
            {
                CustomerId = "23",
                ClientId = 87,
                MeterId = "MET900",
                ServicePointId = "SP23",
                Timezone = "EST",
                CommodityId = 3,
                UOMId = 3,
                AccountNumber = "ACC500",
                AmiTimeStamp = DateTime.UtcNow.Date,
                BatteryVoltage = "23V",
                TransponderPort = "P",
                TransponderId = "P278",
                IntervalType = 15,
                Consumption = 234.45,
                DialRead = "23RT",
                Direction = 2,
                ProjectedReadDate = DateTime.UtcNow.AddDays(10).Date,
                QualityCode = "QA23",
                ReadingType = "RD",
                Scenario = "",
                TOUID = 989,
                TOU_CriticalPeak = 22.2,
                TOU_OffPeak = 13.2,
                TOU_OnPeak = 11.2,
                TOU_Regular = 21,
                TOU_Shoulder1 = 20,
                TOU_Shoulder2 = 21.5,
                VolumeFactor = 15

            };
            IEnumerable<TallAmiModel> tallAmiList = new List<TallAmiModel>() { tallAMiModel };
            List<BillingModel> listBillingModels = new List<BillingModel>
            {
                new BillingModel
                {
                    ClientId = 87,
                    MeterId = "METER_QA_02",
                    CustomerId = "A",
                    AccountId = "A",
                    ServiceContractId = "A",
                    MeterType = "ami",
                    StartDate = DateTime.Today.AddDays(-30),
                    EndDate = DateTime.Today.AddDays(-6),
                    CommodityId = 2,
                    RateClass = "RTC"
                }
            };
            List<Ami15MinuteIntervalModel> listAmi15MinuteIntervalModels = new List<Ami15MinuteIntervalModel>
            {
                new Ami15MinuteIntervalModel
                {
                    UOMId = 10
                }
            };
            var billToDateSettings = new BillToDateSettings()
            {
                RateCompanyId = 5,
                Settings = new CostToDateSettings()
                {
                    General = new CostToDateSettings.GeneralGroup()
                    {
                        UseProjectedNumDaysforBilltoDate = true,
                        ProjectedNumberOfDays = 10
                    }
                }
            };
            var insightSettings = new InsightSettings
            {
                InsightName = "BillToDate",
                UtilityInsightName = "BillToDate",
                AllowedCommunicationChannel = "Email",
                DefaultCommunicationChannel = "Email",
                DefaultFrequency = "Daily",
                DefaultValue = "Daily",
                EmailTemplateId = "1",
                SmsTemplateId = "87_1",
                NotifyTime = "13:00",
                NotificationDay = "Monday",
                TrumpiaKeyword = "TrumpiaKeywordTest",
                Level = "Basic",
                CommodityType = "Water",
                ThresholdType = "TestThresholdType",
                ThresholdMin = "50",
                ThresholdMax = "100"
            };
            var insightSettingsList = new List<InsightSettings> { insightSettings };
            var programSettings = new ProgramSettings
            {
                ProgramName = "testprogram",
                UtilityProgramName = "testprogram",
                FileExportRequired = false,
                DoubleOptInRequired = false,
                TrumpiaKeyword = "trumpiatestkeyword",
                Insights = insightSettingsList
            };
            var programSettingsList = new List<ProgramSettings> { programSettings };
            var clientSettings = new ClientSettings
            {
                ClientId = 87,
                TimeZone = "EST",
                IsAmiIntervalStart = false,
                IsDstHandled = false,
                MinimumDaysForInsightNotification = 1,
                CalculationScheduleTime = "",
                NotificationReportScheduleTime = "",
                InsightReportScheduleTime = "",
                ForgotPasswordEmailTemplateId = "",
                OptInEmailUserName = "UserNameTest",
                OptInEmailPassword = "test@123",
                OptInEmailFrom = "testemail@gmail.com",
                RegistrationConfirmationEmailTemplateId = "",
                OptInEmailConfirmationTemplateId = "",
                OptInSmsConfirmationTemplateId = "",
                TrumpiaShortCode = "",
                TrumpiaApiKey = "TrumpiaTestKey",
                TrumpiaUserName = "TrumpiaTestUserName",
                TrumpiaContactList = "",
                UnmaskedAccountIdEndingDigit = 1,
                Programs = new List<ProgramSettings>(programSettingsList)
            };
            _costToDateResultMock.Object.Usages = new List<Usage>();
            _costToDateResultMock.Object.ProjectedUsages = new List<Usage>();
            _billToDateMock.Setup(t => t.CalculateMeterCostToDate(It.IsAny<Meter>()))
                .Returns(_costToDateResultMock.Object);
            _iClientConfigFacade.Setup(t => t.GetBillToDateSettings(It.IsAny<int>())).Returns(billToDateSettings).Verifiable();
            _iClientConfigFacade.Setup(t => t.GetClientSettings(It.IsAny<int>())).Returns(clientSettings).Verifiable();
            _iCalculationManagerFactory.Setup(t => t.CreateCalculationManager(It.IsAny<BillToDateSettings>()))
                .Returns(_billToDateMock.Object);
            _iTallAmi.Setup(
                m => m.GetAmiList(It.IsAny<DateTime>(), It.IsAny<DateTime>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<int>()))
                .Returns(tallAmiList);
            _accountLookupMock.Setup(t => t.GetAccountMeterDetails(It.IsAny<int>(), It.IsAny<string>())).Returns( new List<AccountLookupModel> { new AccountLookupModel
            {
                ServiceContractId = "A",
                AccountId = "ACC900",
                CustomerId = "C98"
            }});

            _billingMock.Setup(
                t => t.GetAllServicesFromLastBill(It.IsAny<int>(), It.IsAny<string>(), It.IsAny<string>())).Returns(listBillingModels);

            _billingCycleScheduleMock.Setup(
                t => t.GetBillingCycleScheduleByDateAsync(It.IsAny<int>(), It.IsAny<string>(), It.IsAny<DateTime>())).Returns(new BillCycleScheduleModel
                {
                    BeginDate = DateTime.Now.AddDays(-2).ToString(CultureInfo.InvariantCulture),
                    EndDate = DateTime.Now.AddDays(-1).ToString(CultureInfo.InvariantCulture)
                });

            _accountUpdatesMock.Setup(
                t =>
                    t.GetAccountUpdate(It.IsAny<int>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(),
                        It.IsAny<bool>())).Returns(Task.FromResult(new AccountUpdatesModel
                        {
                            UpdateSentDate = DateTime.Now
                        }));

            _amiMock.Setup(
                t => t.GetAmiList(It.IsAny<DateTime>(), It.IsAny<DateTime>(), It.IsAny<string>(), It.IsAny<int>())).Returns(listAmi15MinuteIntervalModels);

            int noOfDays;
            _amiMock.Setup(t => t.GetReadings(It.IsAny<IEnumerable<dynamic>>(), out noOfDays))
                .Returns(new List<Reading>());

            _calculationMock.Setup(t => t.InsertCtdCalculationAsync(It.IsAny<CalculationModel>())).Returns(Task.FromResult(typeof(void)));

            _queueManagerMock.Setup(t => t.AddQueueAsync(It.IsAny<string>(), It.IsAny<string>())).Returns(Task.FromResult(typeof(void)));

            _calculationFacade.AccountLookupManager = new Lazy<IAccountLookup>(() => _accountLookupMock.Object);
            _calculationFacade.AccountManager = new Lazy<IAccountUpdates>(() => _accountUpdatesMock.Object);
            _calculationFacade.BillingCycleScheduleManager = new Lazy<IBillingCycleSchedule>(() => _billingCycleScheduleMock.Object);
            _calculationFacade.BillingManager = new Lazy<IBilling>(() => _billingMock.Object);
            //_calculationFacade.AmiManager = new Lazy<IAmi>(() => _amiMock.Object);
            _calculationFacade.CalculationManager = new Lazy<ICalculation>(() => _calculationMock.Object);
            _calculationFacade.ClientAccount = new Lazy<IClientAccount>(() => _clientAccountMock.Object);
            _calculationFacade.QueueManager = new Lazy<IQueueManager>(() => _queueManagerMock.Object);
            _calculationFacade.TallAmiManager = new Lazy<ITallAMI>(() => _iTallAmi.Object);
            _calculationFacade.ClientConfigFacadeManager = new Lazy<IClientConfigFacade>(() => _iClientConfigFacade.Object);
            _calculationFacade.CalculationManagerFactory = new Lazy<ICalculationManagerFactory>(() => _iCalculationManagerFactory.Object);
            // ReSharper disable once UnusedVariable
            var result = _calculationFacade.CalculateCostToDate(87, "METER_QA_02", DateTime.ParseExact("08-31-2016", "MM-dd-yyyy", CultureInfo.InvariantCulture));
            Assert.AreEqual(result.Exception, null);
        }

        /// <summary>
        /// Test case for CalculateCostToDate method in CalculationFacade class for shared accoutns
        /// expected result - 1 message per customer 
        /// </summary>
        [TestMethod]
        public void CalculateCostToDate_SharedAccount_Test()
        {
            var serviceContractId = "S001";
            var servicePointId = "SP001";
            var accountId = "AC001";
            var customerId = "C001";
            var meterId = "M001";
            var clientId = 87;
            TallAmiModel tallAMiModel = new TallAmiModel()
            {
                CustomerId = customerId,
                ClientId = clientId,
                MeterId = meterId,
                ServicePointId = servicePointId,
                Timezone = "EST",
                CommodityId = 3,
                UOMId = 3,
                AccountNumber = accountId,
                AmiTimeStamp = DateTime.UtcNow.Date,
                BatteryVoltage = "23V",
                TransponderPort = "P",
                TransponderId = "P278",
                IntervalType = 15,
                Consumption = 234.45,
                DialRead = "23RT",
                Direction = 2,
                ProjectedReadDate = DateTime.UtcNow.AddDays(10).Date,
                QualityCode = "QA23",
                ReadingType = "RD",
                Scenario = "",
                TOUID = 989,
                TOU_CriticalPeak = 22.2,
                TOU_OffPeak = 13.2,
                TOU_OnPeak = 11.2,
                TOU_Regular = 21,
                TOU_Shoulder1 = 20,
                TOU_Shoulder2 = 21.5,
                VolumeFactor = 15

            };
            IEnumerable<TallAmiModel> tallAmiList = new List<TallAmiModel>() { tallAMiModel };
            List<BillingModel> listBillingModels = new List<BillingModel>
            {
                new BillingModel
                {
                    ClientId = clientId,
                    MeterId = meterId,
                    CustomerId = customerId,
                    AccountId = accountId,
                    ServiceContractId = serviceContractId,
                    MeterType = "ami",
                    StartDate = DateTime.Today.AddDays(-30),
                    EndDate = DateTime.Today.AddDays(-6),
                    CommodityId = 2,
                    RateClass = "RTC"
                }
            };

            List<AccountLookupModel> listAccountLookupModels = new List<AccountLookupModel>
            {
                new AccountLookupModel
                {
                    ServiceContractId = serviceContractId,
                    AccountId = accountId,
                    CustomerId = customerId, 
                    MeterId = meterId,
                    IsActive =  true,
                    ClientId =  clientId
                },new AccountLookupModel
                {
                    ServiceContractId = serviceContractId,
                    AccountId = accountId,
                    CustomerId = "C002",
                    MeterId = meterId,
                    IsActive =  true,
                    ClientId =  clientId
                }
            };
            
            List<Ami15MinuteIntervalModel> listAmi15MinuteIntervalModels = new List<Ami15MinuteIntervalModel>
            {
                new Ami15MinuteIntervalModel
                {
                    UOMId = 10
                }
            };
            var billToDateSettings = new BillToDateSettings()
            {
                RateCompanyId = 5,
                Settings = new CostToDateSettings()
                {
                    General = new CostToDateSettings.GeneralGroup()
                    {
                        UseProjectedNumDaysforBilltoDate = true,
                        ProjectedNumberOfDays = 10
                    }
                }
            };
            var insightSettings = new InsightSettings
            {
                InsightName = "BillToDate",
                UtilityInsightName = "BillToDate",
                AllowedCommunicationChannel = "Email",
                DefaultCommunicationChannel = "Email",
                DefaultFrequency = "Daily",
                DefaultValue = "Daily",
                EmailTemplateId = "1",
                SmsTemplateId = "87_1",
                NotifyTime = "13:00",
                NotificationDay = "Monday",
                TrumpiaKeyword = "TrumpiaKeywordTest",
                Level = "Basic",
                CommodityType = "Water",
                ThresholdType = "TestThresholdType",
                ThresholdMin = "50",
                ThresholdMax = "100"
            };
            var insightSettingsList = new List<InsightSettings> { insightSettings };
            var programSettings = new ProgramSettings
            {
                ProgramName = "testprogram",
                UtilityProgramName = "testprogram",
                FileExportRequired = false,
                DoubleOptInRequired = false,
                TrumpiaKeyword = "trumpiatestkeyword",
                Insights = insightSettingsList
            };
            var programSettingsList = new List<ProgramSettings> { programSettings };
            var clientSettings = new ClientSettings
            {
                ClientId = clientId,
                TimeZone = "EST",
                IsAmiIntervalStart = false,
                IsDstHandled = false,
                MinimumDaysForInsightNotification = 1,
                CalculationScheduleTime = "",
                NotificationReportScheduleTime = "",
                InsightReportScheduleTime = "",
                ForgotPasswordEmailTemplateId = "",
                OptInEmailUserName = "UserNameTest",
                OptInEmailPassword = "test@123",
                OptInEmailFrom = "testemail@gmail.com",
                RegistrationConfirmationEmailTemplateId = "",
                OptInEmailConfirmationTemplateId = "",
                OptInSmsConfirmationTemplateId = "",
                TrumpiaShortCode = "",
                TrumpiaApiKey = "TrumpiaTestKey",
                TrumpiaUserName = "TrumpiaTestUserName",
                TrumpiaContactList = "",
                UnmaskedAccountIdEndingDigit = 1,
                Programs = new List<ProgramSettings>(programSettingsList)
            };
            _costToDateResultMock.Object.Usages = new List<Usage>();
            _costToDateResultMock.Object.ProjectedUsages = new List<Usage>();
            _billToDateMock.Setup(t => t.CalculateMeterCostToDate(It.IsAny<Meter>()))
                .Returns(_costToDateResultMock.Object);
            _iClientConfigFacade.Setup(t => t.GetBillToDateSettings(It.IsAny<int>())).Returns(billToDateSettings).Verifiable();
            _iClientConfigFacade.Setup(t => t.GetClientSettings(It.IsAny<int>())).Returns(clientSettings).Verifiable();
            _iCalculationManagerFactory.Setup(t => t.CreateCalculationManager(It.IsAny<BillToDateSettings>()))
                .Returns(_billToDateMock.Object);
            _iTallAmi.Setup(
                m => m.GetAmiList(It.IsAny<DateTime>(), It.IsAny<DateTime>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<int>()))
                .Returns(tallAmiList);
            _accountLookupMock.Setup(t => t.GetAccountMeterDetails(It.IsAny<int>(), It.IsAny<string>())).Returns(listAccountLookupModels);

            _billingMock.Setup(
                t => t.GetAllServicesFromLastBill(It.IsAny<int>(), It.IsAny<string>(), It.IsAny<string>())).Returns(listBillingModels);

            _billingCycleScheduleMock.Setup(
                t => t.GetBillingCycleScheduleByDateAsync(It.IsAny<int>(), It.IsAny<string>(), It.IsAny<DateTime>())).Returns(new BillCycleScheduleModel
                {
                    BeginDate = DateTime.Now.AddDays(-2).ToString(CultureInfo.InvariantCulture),
                    EndDate = DateTime.Now.AddDays(-1).ToString(CultureInfo.InvariantCulture)
                });

            _accountUpdatesMock.Setup(
                t =>
                    t.GetAccountUpdate(It.IsAny<int>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(),
                        It.IsAny<bool>())).Returns(Task.FromResult(new AccountUpdatesModel
                        {
                            UpdateSentDate = DateTime.Now
                        }));

            _amiMock.Setup(
                t => t.GetAmiList(It.IsAny<DateTime>(), It.IsAny<DateTime>(), It.IsAny<string>(), It.IsAny<int>())).Returns(listAmi15MinuteIntervalModels);

            int noOfDays;
            _amiMock.Setup(t => t.GetReadings(It.IsAny<IEnumerable<dynamic>>(), out noOfDays))
                .Returns(new List<Reading>());

            _calculationMock.Setup(t => t.InsertCtdCalculationAsync(It.IsAny<CalculationModel>())).Returns(Task.FromResult(typeof(void)));

            _queueManagerMock.Setup(t => t.AddQueueAsync(It.IsAny<string>(), It.IsAny<string>())).Returns(Task.FromResult(typeof(void)));

            _calculationFacade.AccountLookupManager = new Lazy<IAccountLookup>(() => _accountLookupMock.Object);
            _calculationFacade.AccountManager = new Lazy<IAccountUpdates>(() => _accountUpdatesMock.Object);
            _calculationFacade.BillingCycleScheduleManager = new Lazy<IBillingCycleSchedule>(() => _billingCycleScheduleMock.Object);
            _calculationFacade.BillingManager = new Lazy<IBilling>(() => _billingMock.Object);
            //_calculationFacade.AmiManager = new Lazy<IAmi>(() => _amiMock.Object);
            _calculationFacade.CalculationManager = new Lazy<ICalculation>(() => _calculationMock.Object);
            _calculationFacade.ClientAccount = new Lazy<IClientAccount>(() => _clientAccountMock.Object);
            _calculationFacade.QueueManager = new Lazy<IQueueManager>(() => _queueManagerMock.Object);
            _calculationFacade.TallAmiManager = new Lazy<ITallAMI>(() => _iTallAmi.Object);
            _calculationFacade.ClientConfigFacadeManager = new Lazy<IClientConfigFacade>(() => _iClientConfigFacade.Object);
            _calculationFacade.CalculationManagerFactory = new Lazy<ICalculationManagerFactory>(() => _iCalculationManagerFactory.Object);
            // ReSharper disable once UnusedVariable
            var result = _calculationFacade.CalculateCostToDate(clientId, meterId, DateTime.UtcNow.Date);
            Assert.AreEqual(result.Exception, null);
            _queueManagerMock.Verify(t => t.AddQueueAsync(It.IsAny<string>(), It.IsAny<string>()), Times.Exactly(2));
        }

        /// <summary>
        /// Test case for CalculateCostToDate method in CalculationFacade class for shared accoutns
        /// expected result - 1 message per customer 
        /// </summary>
        [TestMethod]
        public void CalculateCostToDate_OldNonMeter_Test()
        {
            var serviceContractId = "S001";
            var servicePointId = "SP001";
            var accountId = "AC001";
            var customerId = "C001";
            var meterId = "M001";
            var clientId = 87;
            TallAmiModel tallAMiModel = new TallAmiModel()
            {
                CustomerId = customerId,
                ClientId = clientId,
                MeterId = meterId,
                ServicePointId = servicePointId,
                Timezone = "EST",
                CommodityId = 3,
                UOMId = 3,
                AccountNumber = accountId,
                AmiTimeStamp = DateTime.UtcNow.Date,
                BatteryVoltage = "23V",
                TransponderPort = "P",
                TransponderId = "P278",
                IntervalType = 15,
                Consumption = 234.45,
                DialRead = "23RT",
                Direction = 2,
                ProjectedReadDate = DateTime.UtcNow.AddDays(10).Date,
                QualityCode = "QA23",
                ReadingType = "RD",
                Scenario = "",
                TOUID = 989,
                TOU_CriticalPeak = 22.2,
                TOU_OffPeak = 13.2,
                TOU_OnPeak = 11.2,
                TOU_Regular = 21,
                TOU_Shoulder1 = 20,
                TOU_Shoulder2 = 21.5,
                VolumeFactor = 15

            };
            IEnumerable<TallAmiModel> tallAmiList = new List<TallAmiModel>() { tallAMiModel };
            List<BillingModel> listBillingModels = new List<BillingModel>
            {
                new BillingModel
                {
                    ClientId = clientId,
                    MeterId = meterId,
                    CustomerId = customerId,
                    AccountId = accountId,
                    ServiceContractId = serviceContractId,
                    MeterType = "ami",
                    StartDate = DateTime.Today.AddDays(-30),
                    EndDate = DateTime.Today.AddDays(-6),
                    CommodityId = 2,
                    RateClass = "RTC"
                },
                new BillingModel
                {
                    ClientId = clientId,
                    CustomerId = customerId,
                    AccountId = accountId,
                    MeterType = "nonmetered",
                    StartDate = DateTime.Today.AddDays(-90),
                    EndDate = DateTime.Today.AddDays(-84),
                    CommodityId = 8,
                    BillDays = 30
                }
            };

            List<AccountLookupModel> listAccountLookupModels = new List<AccountLookupModel>
            {
                new AccountLookupModel
                {
                    ServiceContractId = serviceContractId,
                    AccountId = accountId,
                    CustomerId = customerId,
                    MeterId = meterId,
                    IsActive =  true,
                    ClientId =  clientId
                },new AccountLookupModel
                {
                    ServiceContractId = serviceContractId,
                    AccountId = accountId,
                    CustomerId = "C002",
                    MeterId = meterId,
                    IsActive =  true,
                    ClientId =  clientId
                }
            };

            List<Ami15MinuteIntervalModel> listAmi15MinuteIntervalModels = new List<Ami15MinuteIntervalModel>
            {
                new Ami15MinuteIntervalModel
                {
                    UOMId = 10
                }
            };
            var billToDateSettings = new BillToDateSettings()
            {
                RateCompanyId = 5,
                Settings = new CostToDateSettings()
                {
                    General = new CostToDateSettings.GeneralGroup()
                    {
                        UseProjectedNumDaysforBilltoDate = true,
                        ProjectedNumberOfDays = 10
                    }
                }
            };
            var insightSettings = new InsightSettings
            {
                InsightName = "BillToDate",
                UtilityInsightName = "BillToDate",
                AllowedCommunicationChannel = "Email",
                DefaultCommunicationChannel = "Email",
                DefaultFrequency = "Daily",
                DefaultValue = "Daily",
                EmailTemplateId = "1",
                SmsTemplateId = "87_1",
                NotifyTime = "13:00",
                NotificationDay = "Monday",
                TrumpiaKeyword = "TrumpiaKeywordTest",
                Level = "Basic",
                CommodityType = "Water",
                ThresholdType = "TestThresholdType",
                ThresholdMin = "50",
                ThresholdMax = "100"
            };
            var insightSettingsList = new List<InsightSettings> { insightSettings };
            var programSettings = new ProgramSettings
            {
                ProgramName = "testprogram",
                UtilityProgramName = "testprogram",
                FileExportRequired = false,
                DoubleOptInRequired = false,
                TrumpiaKeyword = "trumpiatestkeyword",
                Insights = insightSettingsList
            };
            var programSettingsList = new List<ProgramSettings> { programSettings };
            var clientSettings = new ClientSettings
            {
                ClientId = clientId,
                TimeZone = "EST",
                IsAmiIntervalStart = false,
                IsDstHandled = false,
                MinimumDaysForInsightNotification = 1,
                CalculationScheduleTime = "",
                NotificationReportScheduleTime = "",
                InsightReportScheduleTime = "",
                ForgotPasswordEmailTemplateId = "",
                OptInEmailUserName = "UserNameTest",
                OptInEmailPassword = "test@123",
                OptInEmailFrom = "testemail@gmail.com",
                RegistrationConfirmationEmailTemplateId = "",
                OptInEmailConfirmationTemplateId = "",
                OptInSmsConfirmationTemplateId = "",
                TrumpiaShortCode = "",
                TrumpiaApiKey = "TrumpiaTestKey",
                TrumpiaUserName = "TrumpiaTestUserName",
                TrumpiaContactList = "",
                UnmaskedAccountIdEndingDigit = 1,
                Programs = new List<ProgramSettings>(programSettingsList)
            };
            _costToDateResultMock.Object.Usages = new List<Usage>();
            _costToDateResultMock.Object.ProjectedUsages = new List<Usage>();
            _billToDateMock.Setup(t => t.CalculateMeterCostToDate(It.IsAny<Meter>()))
                .Returns(_costToDateResultMock.Object);
            _iClientConfigFacade.Setup(t => t.GetBillToDateSettings(It.IsAny<int>())).Returns(billToDateSettings).Verifiable();
            _iClientConfigFacade.Setup(t => t.GetClientSettings(It.IsAny<int>())).Returns(clientSettings).Verifiable();
            _iCalculationManagerFactory.Setup(t => t.CreateCalculationManager(It.IsAny<BillToDateSettings>()))
                .Returns(_billToDateMock.Object);
            _iTallAmi.Setup(
                m => m.GetAmiList(It.IsAny<DateTime>(), It.IsAny<DateTime>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<int>()))
                .Returns(tallAmiList);
            _accountLookupMock.Setup(t => t.GetAccountMeterDetails(It.IsAny<int>(), It.IsAny<string>())).Returns(listAccountLookupModels);

            _billingMock.Setup(
                t => t.GetAllServicesFromLastBill(It.IsAny<int>(), It.IsAny<string>(), It.IsAny<string>())).Returns(listBillingModels);

            _billingCycleScheduleMock.Setup(
                t => t.GetBillingCycleScheduleByDateAsync(It.IsAny<int>(), It.IsAny<string>(), It.IsAny<DateTime>())).Returns(new BillCycleScheduleModel
                {
                    BeginDate = DateTime.Now.AddDays(-2).ToString(CultureInfo.InvariantCulture),
                    EndDate = DateTime.Now.AddDays(-1).ToString(CultureInfo.InvariantCulture)
                });

            _accountUpdatesMock.Setup(
                t =>
                    t.GetAccountUpdate(It.IsAny<int>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(),
                        It.IsAny<bool>())).Returns(Task.FromResult(new AccountUpdatesModel
                        {
                            UpdateSentDate = DateTime.Now
                        }));

            _amiMock.Setup(
                t => t.GetAmiList(It.IsAny<DateTime>(), It.IsAny<DateTime>(), It.IsAny<string>(), It.IsAny<int>())).Returns(listAmi15MinuteIntervalModels);

            int noOfDays;
            _amiMock.Setup(t => t.GetReadings(It.IsAny<IEnumerable<dynamic>>(), out noOfDays))
                .Returns(new List<Reading>());

            _calculationMock.Setup(t => t.InsertCtdCalculationAsync(It.IsAny<CalculationModel>())).Returns(Task.FromResult(typeof(void)));

            _queueManagerMock.Setup(t => t.AddQueueAsync(It.IsAny<string>(), It.IsAny<string>())).Returns(Task.FromResult(typeof(void)));

            _calculationFacade.AccountLookupManager = new Lazy<IAccountLookup>(() => _accountLookupMock.Object);
            _calculationFacade.AccountManager = new Lazy<IAccountUpdates>(() => _accountUpdatesMock.Object);
            _calculationFacade.BillingCycleScheduleManager = new Lazy<IBillingCycleSchedule>(() => _billingCycleScheduleMock.Object);
            _calculationFacade.BillingManager = new Lazy<IBilling>(() => _billingMock.Object);
            //_calculationFacade.AmiManager = new Lazy<IAmi>(() => _amiMock.Object);
            _calculationFacade.CalculationManager = new Lazy<ICalculation>(() => _calculationMock.Object);
            _calculationFacade.ClientAccount = new Lazy<IClientAccount>(() => _clientAccountMock.Object);
            _calculationFacade.QueueManager = new Lazy<IQueueManager>(() => _queueManagerMock.Object);
            _calculationFacade.TallAmiManager = new Lazy<ITallAMI>(() => _iTallAmi.Object);
            _calculationFacade.ClientConfigFacadeManager = new Lazy<IClientConfigFacade>(() => _iClientConfigFacade.Object);
            _calculationFacade.CalculationManagerFactory = new Lazy<ICalculationManagerFactory>(() => _iCalculationManagerFactory.Object);
            // ReSharper disable once UnusedVariable
            var result = _calculationFacade.CalculateCostToDate(clientId, meterId, DateTime.UtcNow.Date);
            Assert.AreEqual(result.Exception, null);
        }

        /// <summary>
        /// Test case for CalculateBillToDate method in CalculationFacade class
        /// </summary>
        [TestMethod]
        public void CalculateBillToDate()
        {
            TallAmiModel tallAMiModel = new TallAmiModel()
            {
                CustomerId = "23",
                ClientId = 87,
                MeterId = "MET900",
                ServicePointId = "SP23",
                Timezone = "EST",
                CommodityId = 3,
                UOMId = 3,
                AccountNumber = "ACC500",
                AmiTimeStamp = DateTime.UtcNow.Date,
                BatteryVoltage = "23V",
                TransponderPort = "P",
                TransponderId = "P278",
                IntervalType = 15,
                Consumption = 234.45,
                DialRead = "23RT",
                Direction = 2,
                ProjectedReadDate = DateTime.Now.Date,
                QualityCode = "QA23",
                ReadingType = "RD",
                Scenario = "",
                TOUID = 989,
                TOU_CriticalPeak = 22.2,
                TOU_OffPeak = 13.2,
                TOU_OnPeak = 11.2,
                TOU_Regular = 21,
                TOU_Shoulder1 = 20,
                TOU_Shoulder2 = 21.5,
                VolumeFactor = 15

            };
            IEnumerable<TallAmiModel> tallAmiList = new List<TallAmiModel>() { tallAMiModel };
            var insightSettings = new InsightSettings
            {
                InsightName = "BillToDate",
                UtilityInsightName = "BillToDate",
                AllowedCommunicationChannel = "Email",
                DefaultCommunicationChannel = "Email",
                DefaultFrequency = "Daily",
                DefaultValue = "Daily",
                EmailTemplateId = "1",
                SmsTemplateId = "87_1",
                NotifyTime = "13:00",
                NotificationDay = "Monday",
                TrumpiaKeyword = "TrumpiaKeywordTest",
                Level = "Basic",
                CommodityType = "Water",
                ThresholdType = "TestThresholdType",
                ThresholdMin = "50",
                ThresholdMax = "100"
            };
            var insightSettingsList = new List<InsightSettings> { insightSettings };
            var programSettings = new ProgramSettings
            {
                ProgramName = "testprogram",
                UtilityProgramName = "testprogram",
                FileExportRequired = false,
                DoubleOptInRequired = false,
                TrumpiaKeyword = "trumpiatestkeyword",
                Insights = insightSettingsList
            };
            var programSettingsList = new List<ProgramSettings> { programSettings };
            var clientSettings = new ClientSettings
            {
                ClientId = 87,
                TimeZone = "ADT",
                IsAmiIntervalStart = false,
                IsDstHandled = false,
                MinimumDaysForInsightNotification = 1,
                CalculationScheduleTime = "",
                NotificationReportScheduleTime = "",
                InsightReportScheduleTime = "",
                ForgotPasswordEmailTemplateId = "",
                OptInEmailUserName = "UserNameTest",
                OptInEmailPassword = "test@123",
                OptInEmailFrom = "testemail@gmail.com",
                RegistrationConfirmationEmailTemplateId = "",
                OptInEmailConfirmationTemplateId = "",
                OptInSmsConfirmationTemplateId = "",
                TrumpiaShortCode = "",
                TrumpiaApiKey = "TrumpiaTestKey",
                TrumpiaUserName = "TrumpiaTestUserName",
                TrumpiaContactList = "",
                UnmaskedAccountIdEndingDigit = 1,
                Programs = new List<ProgramSettings>(programSettingsList)
            };
            var billToDateSettings = new BillToDateSettings()
            {
                RateCompanyId = 5,
                Settings = new CostToDateSettings()
                {
                    General = new CostToDateSettings.GeneralGroup()
                    {
                        UseProjectedNumDaysforBilltoDate = true,
                        ProjectedNumberOfDays = 4
                    }
                }
            };
            var listBillingModels = new List<BillingModel>
            {
                new BillingModel
                {
                    ClientId = 87,
                    MeterId = "METER02",
                    CustomerId = "A",
                    AccountId = "A",
                    ServiceContractId = "A",
                    MeterType = "ami",
                    StartDate = DateTime.Now.AddDays(-5),
                    EndDate = DateTime.Now.AddDays(-2),
                    CommodityId = 2,
                    TotalUsage = 100,
                    TotalCost = 100,
                    RateClass = "C"
                }
            };

            var listAmi15MinuteIntervalModels = new List<Ami15MinuteIntervalModel>
            {
                new Ami15MinuteIntervalModel
                {
                    UOMId = 10
                }
            };

            var listReading = new List<Reading>
            {
                new Reading(new DateTime(DateTime.Now.AddDays(-3).Year,DateTime.Now.AddDays(-3).Month,DateTime.Now.AddDays(-3).Day,0,15,0 ),100)
            };

            var billToDateMock = new Mock<IBillToDate>();
            var billToDateResult = new Mock<BillToDateResult>();
            billToDateResult.Object.Bill = new Bill()
            {
                MeteredServices = new MeteredServiceList()
                {
                    new MeteredService()
                    {
                        Meters = new MeterList()
                        {
                            new Meter()
                        }
                    }
                }
            };
            _iTallAmi.Setup(
                m => m.GetAmiList(It.IsAny<DateTime>(), It.IsAny<DateTime>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<int>()))
                .Returns(tallAmiList);
            //costToDateResultMock.Object.ProjectedUsages = new List<Usage>();
            billToDateMock.Setup(t => t.CalculateBillToDate(It.IsAny<Bill>())).Returns(billToDateResult.Object);
            _iClientConfigFacade.Setup(t => t.GetBillToDateSettings(It.IsAny<int>())).Returns(billToDateSettings);
            _iClientConfigFacade.Setup(t => t.GetClientSettings(It.IsAny<int>())).Returns(clientSettings);
            _iCalculationManagerFactory.Setup(t => t.CreateCalculationManager(It.IsAny<BillToDateSettings>()))
                  .Returns(billToDateMock.Object);

            _billingMock.Setup(
                    t => t.GetAllServicesFromLastBill(It.IsAny<int>(), It.IsAny<string>(), It.IsAny<string>())).Returns(listBillingModels);

            _billingCycleScheduleMock.Setup(
                    t => t.GetBillingCycleScheduleByDateAsync(It.IsAny<int>(), It.IsAny<string>(), It.IsAny<DateTime>())).Returns(new BillCycleScheduleModel
                    {
                        BeginDate = DateTime.Now.AddDays(-2).ToString(CultureInfo.InvariantCulture),
                        EndDate = DateTime.Now.AddDays(-1).ToString(CultureInfo.InvariantCulture)
                    });

            _amiMock.Setup(
                t => t.GetAmiList(It.IsAny<DateTime>(), It.IsAny<DateTime>(), It.IsAny<string>(), It.IsAny<int>())).Returns(listAmi15MinuteIntervalModels);

            int noOfDays;
            _amiMock.Setup(t => t.GetReadings(It.IsAny<IEnumerable<dynamic>>(), out noOfDays))
                        .Returns(listReading);

            _accountUpdatesMock.Setup(
                        t =>
                            t.GetAccountUpdate(It.IsAny<int>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(),
                                It.IsAny<bool>())).Returns(Task.FromResult(new AccountUpdatesModel
                                {
                                    UpdateSentDate = DateTime.Now.AddDays(-3)
                                }));
            _calculationMock.Setup(t => t.InsertCtdCalculationAsync(It.IsAny<CalculationModel>())).Returns(Task.FromResult(typeof(void)));
            _calculationMock.Setup(t => t.InsertBtdCalculationAsync(It.IsAny<CalculationModel>())).Returns(Task.FromResult(typeof(void)));
            _queueManagerMock.Setup(t => t.AddQueueAsync(It.IsAny<string>(), It.IsAny<string>())).Returns(Task.FromResult(typeof(void)));
            _calculationFacade.AccountLookupManager = new Lazy<IAccountLookup>(() => _accountLookupMock.Object);
            _calculationFacade.AccountManager = new Lazy<IAccountUpdates>(() => _accountUpdatesMock.Object);
            _calculationFacade.BillingCycleScheduleManager = new Lazy<IBillingCycleSchedule>(() => _billingCycleScheduleMock.Object);
            _calculationFacade.BillingManager = new Lazy<IBilling>(() => _billingMock.Object);
            //_calculationFacade.AmiManager = new Lazy<IAmi>(() => _amiMock.Object);
            _calculationFacade.CalculationManager = new Lazy<ICalculation>(() => _calculationMock.Object);
            _calculationFacade.ClientAccount = new Lazy<IClientAccount>(() => _clientAccountMock.Object);
            _calculationFacade.QueueManager = new Lazy<IQueueManager>(() => _queueManagerMock.Object);
            _calculationFacade.TallAmiManager = new Lazy<ITallAMI>(() => _iTallAmi.Object);
            _calculationFacade.ClientConfigFacadeManager = new Lazy<IClientConfigFacade>(() => _iClientConfigFacade.Object);
            _calculationFacade.CalculationManagerFactory = new Lazy<ICalculationManagerFactory>(() => _iCalculationManagerFactory.Object);
            // ReSharper disable once UnusedVariable
            var result = _calculationFacade.CalculateBillToDate(87, "A", "AA", DateTime.ParseExact("08-31-2016", "MM-dd-yyyy", CultureInfo.InvariantCulture));
            Assert.AreEqual(result.Exception, null);
        }

        /// <summary>
        /// Test case for ScheduledCalculation method in CalculationFacade class
        /// </summary>
        [TestMethod]
        public void ScheduledCalculation()
        {
            _clientAccountMock.Setup(t => t.SendScheduleMessage(It.IsAny<int>(), It.IsAny<string>()));

            _calculationFacade.ClientAccount = new Lazy<IClientAccount>(() => _clientAccountMock.Object);
            _calculationFacade.ScheduledCalculation(It.IsAny<int>(), It.IsAny<string>());

            _clientAccountMock.Verify(t => t.SendScheduleMessage(It.IsAny<int>(), It.IsAny<string>()), Times.Once);
        }

        /// <summary>
        /// Test case for GetClientAccount method in CalculationFacade class
        /// </summary>
        [TestMethod]
        public void GetClientAccount()
        {
            _clientAccountMock.Setup(t => t.GetClientAccounts(It.IsAny<int>())).Returns(new List<ClientAccountModel>());

            _calculationFacade.ClientAccount = new Lazy<IClientAccount>(() => _clientAccountMock.Object);
            _calculationFacade.GetClientAccount(It.IsAny<int>());

            _clientAccountMock.Verify(t => t.GetClientAccounts(It.IsAny<int>()), Times.Once);
        }
    }
}