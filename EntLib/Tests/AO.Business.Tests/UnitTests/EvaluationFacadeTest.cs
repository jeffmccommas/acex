﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Threading.Tasks;
using AO.BusinessContracts;
using CE.AO.Models;
using CE.BillToDate;
using CE.ContentModel.Entities;
using CE.RateModel;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Newtonsoft.Json;
using Enums = CE.RateModel.Enums;

namespace AO.Business.Tests.UnitTests
{
    /// <summary>
    /// Test cases for EvaluationFacade class
    /// </summary>
    [TestClass]
    public class EvaluationFacadeTest : TestBase
    {
        private EvaluationFacade _evaluationFacade;
        private Mock<IEvaluation> _evaluationMock;
        private Mock<ISubscription> _subscriptionMock;
        private Mock<ICalculation> _calculationMock;
        private Mock<ITallAMI> _amiMock;
        private Mock<IBilling> _billingMock;
        private Mock<IClientConfigFacade> _clientConfigFacadeMock;
        private Mock<IBillToDate> _billToDateMock;
        private Mock<ICalculationManagerFactory> _calculationManagerFactoryMock;

        /// <summary>
        /// Test Case Init
        /// </summary>
        [TestInitialize]
        public void Init()
        {
            _evaluationMock = new Mock<IEvaluation>();
            _subscriptionMock = new Mock<ISubscription>();
            _calculationMock = new Mock<ICalculation>();
            _amiMock = new Mock<ITallAMI>();
            _billingMock = new Mock<IBilling>();
            _billToDateMock = new Mock<IBillToDate>();
            _clientConfigFacadeMock = new Mock<IClientConfigFacade>();
            _calculationManagerFactoryMock = new Mock<ICalculationManagerFactory>();

            _evaluationFacade = new EvaluationFacade(LogModelMock.Object);
        }

        /// <summary>
        /// Test case for ProcessEvaluation method in EvaluationFacade class
        /// </summary>
        [TestMethod]
        public void ProcessEvaluation()
        {
            IList<SubscriptionModel> listSubscriptionModels = new List<SubscriptionModel> { new SubscriptionModel() };
            IList<BillingModel> listBillingModels = new List<BillingModel>
            {
                new BillingModel
                {
                    AccountId = "",
                    ClientId = 87,
                    MeterType = "ami"
                }
            };

            var tierBoundary = new TierBoundary
            {
                BaseOrTier = Enums.BaseOrTier.Tier6,
                TimeOfUse = Enums.TimeOfUse.OnPeak,
                Season = Enums.Season.SeasonB,
                DaysIntoSeason = 1,
                SeasonFactor = 1,
                Threshold = 2,
                SecondaryThreshold = 3
            };

            var tierBoundaryList = new List<TierBoundary> { tierBoundary };

            _subscriptionMock.Setup(
                t => t.GetCustomerSubscriptions(It.IsAny<int>(), It.IsAny<string>(), It.IsAny<string>()))
                .Returns(listSubscriptionModels);

            _billingMock.Setup(
                t => t.GetAllServicesFromLastBill(It.IsAny<int>(), It.IsAny<string>(), It.IsAny<string>())).Returns(listBillingModels);

            _calculationMock.Setup(
                t => t.GetCtdCalculationAsync(It.IsAny<int>(), It.IsAny<string>(), It.IsAny<string>())).Returns(
                Task.FromResult(new CalculationModel { CustomerId = "C001", ServiceContractId = "SC001", RateClass = "", BillCycleEndDate = DateTime.Now.ToString(CultureInfo.InvariantCulture), BillCycleStartDate = DateTime.Now.ToString(CultureInfo.InvariantCulture) })
                );
            _calculationMock.Setup(
                t => t.GetBtdCalculationAsync(It.IsAny<int>(), It.IsAny<string>())).Returns(
                Task.FromResult(new CalculationModel { CustomerId = "C001", ServiceContractId = "SC001", RateClass = "", BillCycleEndDate = DateTime.Now.ToString(CultureInfo.InvariantCulture), BillCycleStartDate = DateTime.Now.ToString(CultureInfo.InvariantCulture) })
                );

            _amiMock.Setup(t => t.GetAmi(It.IsAny<DateTime>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<int>()))
                .Returns(new List<TallAmiModel>());

            _evaluationMock.Setup(
                t =>
                    t.EvaluateInsight(It.IsAny<IEnumerable<SubscriptionModel>>(),
                        It.IsAny<IEnumerable<CalculationModel>>(), It.IsAny<ClientSettings>(), It.IsAny<List<TallAmiModel>>(),
                        It.IsAny<List<TierBoundary>>(), It.IsAny<DateTime>(), It.IsAny<BillingModel>())).Returns(Task.FromResult(typeof(void)));

            _clientConfigFacadeMock.Setup(t => t.GetClientSettings(It.IsAny<int>())).Returns(GetClientSettings());

            _clientConfigFacadeMock.Setup(t => t.GetBillToDateSettings(It.IsAny<int>())).Returns(GetBillToDateSettings());

            _calculationManagerFactoryMock.Setup(t => t.CreateCalculationManager(It.IsAny<BillToDateSettings>())).Returns(_billToDateMock.Object);

            _billToDateMock.Setup(t => t.GetTierBoundaries(It.IsAny<int>(), It.IsAny<string>(), It.IsAny<DateTime>(), It.IsAny<DateTime>())).Returns(tierBoundaryList).Verifiable();

            _evaluationFacade.AmiManager = new Lazy<ITallAMI>(() => _amiMock.Object);
            _evaluationFacade.BillingManager = new Lazy<IBilling>(() => _billingMock.Object);
            _evaluationFacade.CalculationManager = new Lazy<ICalculation>(() => _calculationMock.Object);
            _evaluationFacade.EvaluationManager = new Lazy<IEvaluation>(() => _evaluationMock.Object);
            _evaluationFacade.SubscriptionManager = new Lazy<ISubscription>(() => _subscriptionMock.Object);
            _evaluationFacade.ClientConfigFacadeManager = new Lazy<IClientConfigFacade>(() => _clientConfigFacadeMock.Object);
            _evaluationFacade.CalculationManagerFactory = new Lazy<ICalculationManagerFactory>(() => _calculationManagerFactoryMock.Object);

            _evaluationFacade.ProcessEvaluation(87, It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<DateTime>());

            Assert.AreEqual(1, 1);
        }

        private static BillToDateSettings GetBillToDateSettings()
        {
            var billToDateSettings = new BillToDateSettings
            {
                RateCompanyId = 100,
                Settings = new CostToDateSettings
                {
                    General = new CostToDateSettings.GeneralGroup
                    {
                        AllowBaselineCalculations = false,
                        AllowMultipleMetersPerService = false,
                        AllowRebateCalculations = false,
                        ConversionFactorForGas = 1,
                        ConversionFactorForWater = 1,
                        DSTEndDate = "",
                        DailyTierBillDaysType = Enums.DailyTierBillDaysType.UseProjectedBillDays,
                        HandleMixedInterval = false,
                        MinimumChargeType = Enums.MinimumChargeType.NoMinimumCharge,
                        ProjectedNumberOfDays = 31,
                        ProrateDemandUsageDeterminants = false,
                        ProrateMonthlyServiceCharges = false,
                        ProrateNonMeteredCharges = true,
                        SmoothTiers = false,
                        SupportDailyDemand = false,
                        UseConversionFactorForGas = false,
                        UseConversionFactorForWater = false,
                        UseProjectedNumDaysforBilltoDate = true
                    },
                    Sewer = new CostToDateSettings.SewerGroup
                    {
                        ProrateSewerMaximumMonthsUsageDeterminants = false,
                        SewerMaximumMonthsUsageForCommercial = 80,
                        SewerMaximumMonthsUsageForResidential = 80,
                        UseSewerMaximumMonthsUsageForCommercial = false,
                        UseSewerMaximumMonthsUsageForResidential = false
                    },
                    Validate = new CostToDateSettings.ValidateGroup
                    {
                        CheckDaysInFullMonth = false,
                        CheckMaximumMissingDays = false,
                        CheckMinimumBillDays = false,
                        CheckMinimumUsage = false,
                        DaysInFullMonth = 25,
                        MaximumMissingDays = 8,
                        MinimumBillDaysElectric = 5,
                        MinimumBillDaysGas = 5,
                        MinimumBillDaysWater = 5,
                        MinimumUsage = 5
                    }
                }
            };
            return billToDateSettings;
        }

        private static ClientSettings GetClientSettings()
        {
            var clientSettingsJson = new List<ClientConfigurationBulk>
            {
                new ClientConfigurationBulk
                {
                    Category = "system",
                    Key = "aclaraone.clientsettings",
                    Value =
                        "{\r\n \"ClientId\": 87,\r\n \"TimeZone\": \"EST\",\r\n \"IsAmiIntervalStart\": false,\r\n \"IsDstHandled\": false,\r\n \"CalculationScheduleTime\": \"04:00\",\r\n \"NotificationReportScheduleTime\": \"05:00\",\r\n \"InsightReportScheduleTime\": \"06:00\",\r\n \"ForgotPasswordEmailTemplateId\": \"af5c0251-486f-4100-a4cf-2d91a905f108\",\r\n \"RegistrationConfirmationEmailTemplateId\": \"7c0657cd-1020-42a4-a281-87db82a7f6c4\",\r\n \"OptInEmailConfirmationTemplateId\": \"777ca9f8-bb03-490b-946b-b1e6991b2b15\",\r\n \"OptInSmsConfirmationTemplateId\": \"87_12\",\r\n \"OptInEmailUserName\": \"aclaradev\",\r\n \"OptInEmailPassword\": \"Xaclaradev2311X\",\r\n \"OptInEmailFrom\": \"support@aclarax.com\",\r\n \"TrumpiaShortCode\": \"99000\",\r\n \"TrumpiaApiKey\": \"b4c1971153b3509b6ec0d8a24a33454c\",\r\n \"TrumpiaUserName\": \"aclaradev\",\r\n \"TrumpiaContactList\": \"AclaraDemoDev\",\r\n \"UnmaskedAccountIdEndingDigit\" : \"4\",\r\n \"Programs\": [\r\n  {\r\n   \"ProgramName\": \"Program1\",\r\n   \"UtilityProgramName\": \"Program1\",\r\n   \"FileExportRequired\": true,\r\n   \"DoubleOptInRequired\": true,\r\n   \"TrumpiaKeyword\": \"abagdasarian2\",\r\n   \"Insights\": [\r\n    {\r\n     \"InsightName\": \"BillToDate\",\r\n     \"UtilityInsightName\": \"BillToDate\",\r\n     \"AllowedCommunicationChannel\": \"File,Email,SMS,EmailandSMS\",\r\n     \"DefaultCommunicationChannel\": \"Email\",\r\n     \"DefaultFrequency\": \"Daily\",\r\n     \"DefaultValue\": \"\",\r\n     \"EmailTemplateId\": \"c8d5e21f-1150-4b3d-b4b8-ad869d89a040\",\r\n     \"SMSTemplateId\": \"87_1\",\r\n     \"NotifyTime\": \"20:00\",\r\n     \"NotificationDay\": \"Monday\",\r\n     \"TrumpiaKeyword\": \"\",\r\n     \"Level\": \"Account\",\r\n     \"CommodityType\": \"\",\r\n     \"ThresholdType\": \"\",\r\n     \"ThresholdMin\": \"\",\r\n     \"ThresholdMax\": \"\"\r\n    }, {\r\n     \"InsightName\": \"AccountProjectedCost\",\r\n     \"UtilityInsightName\": \"AccountProjectedCost\",\r\n     \"AllowedCommunicationChannel\": \"File,Email,SMS,EmailandSMS\",\r\n     \"DefaultCommunicationChannel\": \"Email\",\r\n     \"DefaultFrequency\": \"Daily\",\r\n     \"DefaultValue\": \"\",\r\n     \"EmailTemplateId\": \"7367b661-0dfd-4eb2-a0d8-0735a459fcb5\",\r\n     \"SMSTemplateId\": \"87_3\",\r\n     \"NotifyTime\": \"20:00\",\r\n     \"NotificationDay\": \"Monday\",\r\n     \"TrumpiaKeyword\": \"\",\r\n     \"Level\": \"Account\",\r\n     \"CommodityType\": \"\",\r\n     \"ThresholdType\": \"\",\r\n     \"ThresholdMin\": \"\",\r\n     \"ThresholdMax\": \"\"\r\n    }, {\r\n     \"InsightName\": \"AccountLevelCostThreshold\",\r\n     \"UtilityInsightName\": \"AccountLevelCostThreshold\",\r\n     \"AllowedCommunicationChannel\": \"File,Email,SMS,EmailandSMS\",\r\n     \"DefaultCommunicationChannel\": \"Email\",\r\n     \"DefaultFrequency\": \"Immediate\",\r\n     \"DefaultValue\": \"100\",\r\n     \"EmailTemplateId\": \"7b3f9981-ae52-4fd9-b223-8ae516f6f82f\",\r\n     \"SMSTemplateId\": \"87_6\",\r\n     \"NotifyTime\": \"\",\r\n     \"NotificationDay\": \"\",\r\n     \"TrumpiaKeyword\": \"\",\r\n     \"Level\": \"Account\",\r\n     \"CommodityType\": \"\",\r\n     \"ThresholdType\": \"Cost\",\r\n     \"ThresholdMin\": \"1\",\r\n     \"ThresholdMax\": \"1000000\"\r\n    }, {\r\n     \"InsightName\": \"ServiceLevelCostThreshold\",\r\n     \"UtilityInsightName\": \"ServiceLevelCostThreshold\",\r\n     \"AllowedCommunicationChannel\": \"File,Email,SMS,EmailandSMS\",\r\n     \"DefaultCommunicationChannel\": \"Email\",\r\n     \"DefaultFrequency\": \"Immediate\",\r\n     \"DefaultValue\": \"25,25\",\r\n     \"EmailTemplateId\": \"87791eaa-0342-45d5-85d4-4645f7a85f7d\",\r\n     \"SMSTemplateId\": \"87_8\",\r\n     \"NotifyTime\": \"\",\r\n     \"NotificationDay\": \"\",\r\n     \"TrumpiaKeyword\": \"\",\r\n     \"Level\": \"Service\",\r\n     \"CommodityType\": \"Gas,Electric\",\r\n     \"ThresholdType\": \"Cost\",\r\n     \"ThresholdMin\": \"1\",\r\n     \"ThresholdMax\": \"1000000\"\r\n    }, {\r\n     \"InsightName\": \"ServiceLevelUsageThreshold\",\r\n     \"UtilityInsightName\": \"ServiceLevelUsageThreshold\",\r\n     \"AllowedCommunicationChannel\": \"File,Email,SMS,EmailandSMS\",\r\n     \"DefaultCommunicationChannel\": \"Email\",\r\n     \"DefaultFrequency\": \"Immediate\",\r\n     \"DefaultValue\": \"25,500\",\r\n     \"EmailTemplateId\": \"f7791cc2-9665-4d4b-8a7f-9421daf55e29\",\r\n     \"SMSTemplateId\": \"87_7\",\r\n     \"NotifyTime\": \"\",\r\n     \"NotificationDay\": \"\",\r\n     \"TrumpiaKeyword\": \"\",\r\n     \"Level\": \"Service\",\r\n     \"CommodityType\": \"Gas,Electric\",\r\n     \"ThresholdType\": \"Usage\",\r\n     \"ThresholdMin\": \"0.1\",\r\n     \"ThresholdMax\": \"1000000\"\r\n    }, {\r\n     \"InsightName\": \"CostToDate\",\r\n     \"UtilityInsightName\": \"CostToDate\",\r\n     \"AllowedCommunicationChannel\": \"File,Email,SMS,EmailandSMS\",\r\n     \"DefaultCommunicationChannel\": \"Email\",\r\n     \"DefaultFrequency\": \"Weekly\",\r\n     \"DefaultValue\": \"\",\r\n     \"EmailTemplateId\": \"b4425b3d-de12-4694-8751-edb41bc5cd79\",\r\n     \"SMSTemplateId\": \"87_2\",\r\n     \"NotifyTime\": \"20:00\",\r\n     \"NotificationDay\": \"Monday\",\r\n     \"TrumpiaKeyword\": \"\",\r\n     \"Level\": \"Service\",\r\n     \"CommodityType\": \"Gas,Electric\",\r\n     \"ThresholdType\": \"\",\r\n     \"ThresholdMin\": \"\",\r\n     \"ThresholdMax\": \"\"\r\n    }, {\r\n     \"InsightName\": \"ServiceProjectedCost\",\r\n     \"UtilityInsightName\": \"ServiceProjectedCost\",\r\n     \"AllowedCommunicationChannel\": \"File,Email,SMS,EmailandSMS\",\r\n     \"DefaultCommunicationChannel\": \"Email\",\r\n     \"DefaultFrequency\": \"Daily\",\r\n     \"DefaultValue\": \"\",\r\n     \"EmailTemplateId\": \"6bd40b25-fc86-480f-a1d4-0f2a22b7d270\",\r\n     \"SMSTemplateId\": \"87_4\",\r\n     \"NotifyTime\": \"20:00\",\r\n     \"NotificationDay\": \"Monday\",\r\n     \"TrumpiaKeyword\": \"\",\r\n     \"Level\": \"Service\",\r\n     \"CommodityType\": \"Gas,Electric\",\r\n     \"ThresholdType\": \"\",\r\n     \"ThresholdMin\": \"\",\r\n     \"ThresholdMax\": \"\"\r\n    }, {\r\n     \"InsightName\": \"Usage\",\r\n     \"UtilityInsightName\": \"Usage\",\r\n     \"AllowedCommunicationChannel\": \"File,Email,SMS,EmailandSMS\",\r\n     \"DefaultCommunicationChannel\": \"Email\",\r\n     \"DefaultFrequency\": \"Daily\",\r\n     \"DefaultValue\": \"\",\r\n     \"EmailTemplateId\": \"3fdb3336-0192-4bb9-bc53-8810dd110a3a\",\r\n     \"SMSTemplateId\": \"87_5\",\r\n     \"NotifyTime\": \"20:00\",\r\n     \"NotificationDay\": \"Monday\",\r\n     \"TrumpiaKeyword\": \"\",\r\n     \"Level\": \"Service\",\r\n     \"CommodityType\": \"Gas,Electric\",\r\n     \"ThresholdType\": \"\",\r\n     \"ThresholdMin\": \"\",\r\n     \"ThresholdMax\": \"\"\r\n    }, {\r\n     \"InsightName\": \"DayThreshold\",\r\n     \"UtilityInsightName\": \"DayThreshold\",\r\n     \"AllowedCommunicationChannel\": \"File,Email,SMS,EmailandSMS\",\r\n     \"DefaultCommunicationChannel\": \"Email\",\r\n     \"DefaultFrequency\": \"Immediate\",\r\n     \"DefaultValue\": \"1,16\",\r\n     \"EmailTemplateId\": \"ce508747-444b-4e1c-9f39-53629e47260b\",\r\n     \"SMSTemplateId\": \"87_9\",\r\n     \"NotifyTime\": \"\",\r\n     \"NotificationDay\": \"\",\r\n     \"TrumpiaKeyword\": \"\",\r\n     \"Level\": \"Service\",\r\n     \"CommodityType\": \"Gas,Electric\",\r\n     \"ThresholdType\": \"Usage\",\r\n     \"ThresholdMin\": \"0.1\",\r\n     \"ThresholdMax\": \"1000000\"\r\n    }, {\r\n     \"InsightName\": \"ServiceLevelTieredThresholdApproaching\",\r\n     \"UtilityInsightName\": \"ServiceLevelTieredThresholdApproaching\",\r\n     \"AllowedCommunicationChannel\": \"File,Email,SMS,EmailandSMS\",\r\n     \"DefaultCommunicationChannel\": \"Email\",\r\n     \"DefaultFrequency\": \"Immediate\",\r\n     \"DefaultValue\": \"20,20\",\r\n     \"EmailTemplateId\": \"4962e788-fb1e-43cf-a2f6-faf713d25e0c\",\r\n     \"SMSTemplateId\": \"87_10\",\r\n     \"NotifyTime\": \"\",\r\n     \"NotificationDay\": \"\",\r\n     \"TrumpiaKeyword\": \"\",\r\n     \"Level\": \"Service\",\r\n     \"CommodityType\": \"Gas,Electric\",\r\n     \"ThresholdType\": \"Percent\",\r\n     \"ThresholdMin\": \"1\",\r\n     \"ThresholdMax\": \"100\"\r\n    }, {\r\n     \"InsightName\": \"ServiceLevelTieredThresholdExceed\",\r\n     \"UtilityInsightName\": \"ServiceLevelTieredThresholdExceed\",\r\n     \"AllowedCommunicationChannel\": \"File,Email,SMS,EmailandSMS\",\r\n     \"DefaultCommunicationChannel\": \"Email\",\r\n     \"DefaultFrequency\": \"Immediate\",\r\n     \"DefaultValue\": \"\",\r\n     \"EmailTemplateId\": \"c1dc511c-0dd7-4208-bad7-8729fa097194\",\r\n     \"SMSTemplateId\": \"87_11\",\r\n     \"NotifyTime\": \"\",\r\n     \"NotificationDay\": \"\",\r\n     \"TrumpiaKeyword\": \"\",\r\n     \"Level\": \"Service\",\r\n     \"CommodityType\": \"Gas,Electric\",\r\n     \"ThresholdType\": \"\",\r\n     \"ThresholdMin\": \"\",\r\n     \"ThresholdMax\": \"\"\r\n    }\r\n   ]\r\n  }, {\r\n   \"ProgramName\": \"Program2\",\r\n   \"UtilityProgramName\": \"Program2\",\r\n   \"FileExportRequired\": false,\r\n   \"DoubleOptInRequired\": true,\r\n   \"TrumpiaKeyword\": \"\",\r\n   \"Insights\": [\r\n    {\r\n     \"InsightName\": \"ServiceLevelCostThreshold\",\r\n     \"UtilityInsightName\": \"ServiceLevelCostThreshold\",\r\n     \"AllowedCommunicationChannel\": \"File,Email,SMS,EmailandSMS\",\r\n     \"DefaultCommunicationChannel\": \"Email\",\r\n     \"DefaultFrequency\": \"Immediate\",\r\n     \"DefaultValue\": \"25\",\r\n     \"EmailTemplateId\": \"87791eaa-0342-45d5-85d4-4645f7a85f7d\",\r\n     \"SMSTemplateId\": \"87_8\",\r\n     \"NotifyTime\": \"\",\r\n     \"NotificationDay\": \"\",\r\n     \"TrumpiaKeyword\": \"\",\r\n     \"Level\": \"Service\",\r\n     \"CommodityType\": \"Water\",\r\n     \"ThresholdType\": \"Cost\",\r\n     \"ThresholdMin\": \"1\",\r\n     \"ThresholdMax\": \"1000000\"\r\n    }, {\r\n     \"InsightName\": \"ServiceLevelUsageThreshold\",\r\n     \"UtilityInsightName\": \"ServiceLevelUsageThreshold\",\r\n     \"AllowedCommunicationChannel\": \"File,Email,SMS,EmailandSMS\",\r\n     \"DefaultCommunicationChannel\": \"Email\",\r\n     \"DefaultFrequency\": \"Immediate\",\r\n     \"DefaultValue\": \"5\",\r\n     \"EmailTemplateId\": \"f7791cc2-9665-4d4b-8a7f-9421daf55e29\",\r\n     \"SMSTemplateId\": \"87_7\",\r\n     \"NotifyTime\": \"\",\r\n     \"NotificationDay\": \"\",\r\n     \"TrumpiaKeyword\": \"\",\r\n     \"Level\": \"Service\",\r\n     \"CommodityType\": \"Water\",\r\n     \"ThresholdType\": \"Usage\",\r\n     \"ThresholdMin\": \"0.1\",\r\n     \"ThresholdMax\": \"1000000\"\r\n    }, {\r\n     \"InsightName\": \"CostToDate\",\r\n     \"UtilityInsightName\": \"CostToDate\",\r\n     \"AllowedCommunicationChannel\": \"File,Email,SMS,EmailandSMS\",\r\n     \"DefaultCommunicationChannel\": \"Email\",\r\n     \"DefaultFrequency\": \"Weekly\",\r\n     \"DefaultValue\": \"\",\r\n     \"EmailTemplateId\": \"b4425b3d-de12-4694-8751-edb41bc5cd79\",\r\n     \"SMSTemplateId\": \"87_2\",\r\n     \"NotifyTime\": \"20:00\",\r\n     \"NotificationDay\": \"Monday\",\r\n     \"TrumpiaKeyword\": \"\",\r\n     \"Level\": \"Service\",\r\n     \"CommodityType\": \"Water\",\r\n     \"ThresholdType\": \"\",\r\n     \"ThresholdMin\": \"\",\r\n     \"ThresholdMax\": \"\"\r\n    }, {\r\n     \"InsightName\": \"ServiceProjectedCost\",\r\n     \"UtilityInsightName\": \"ServiceProjectedCost\",\r\n     \"AllowedCommunicationChannel\": \"File,Email,SMS,EmailandSMS\",\r\n     \"DefaultCommunicationChannel\": \"Email\",\r\n     \"DefaultFrequency\": \"Daily\",\r\n     \"DefaultValue\": \"\",\r\n     \"EmailTemplateId\": \"6bd40b25-fc86-480f-a1d4-0f2a22b7d270\",\r\n     \"SMSTemplateId\": \"87_4\",\r\n     \"NotifyTime\": \"20:00\",\r\n     \"NotificationDay\": \"Monday\",\r\n     \"TrumpiaKeyword\": \"\",\r\n     \"Level\": \"Service\",\r\n     \"CommodityType\": \"Water\",\r\n     \"ThresholdType\": \"\",\r\n     \"ThresholdMin\": \"\",\r\n     \"ThresholdMax\": \"\"\r\n    }, {\r\n     \"InsightName\": \"Usage\",\r\n     \"UtilityInsightName\": \"Usage\",\r\n     \"AllowedCommunicationChannel\": \"File,Email,SMS,EmailandSMS\",\r\n     \"DefaultCommunicationChannel\": \"Email\",\r\n     \"DefaultFrequency\": \"Daily\",\r\n     \"DefaultValue\": \"\",\r\n     \"EmailTemplateId\": \"3fdb3336-0192-4bb9-bc53-8810dd110a3a\",\r\n     \"SMSTemplateId\": \"87_5\",\r\n     \"NotifyTime\": \"20:00\",\r\n     \"NotificationDay\": \"Monday\",\r\n     \"TrumpiaKeyword\": \"\",\r\n     \"Level\": \"Service\",\r\n     \"CommodityType\": \"Water\",\r\n     \"ThresholdType\": \"\",\r\n     \"ThresholdMin\": \"\",\r\n     \"ThresholdMax\": \"\"\r\n    }, {\r\n     \"InsightName\": \"DayThreshold\",\r\n     \"UtilityInsightName\": \"DayThreshold\",\r\n     \"AllowedCommunicationChannel\": \"File,Email,SMS,EmailandSMS\",\r\n     \"DefaultCommunicationChannel\": \"Email\",\r\n     \"DefaultFrequency\": \"Immediate\",\r\n     \"DefaultValue\": \"0.15\",\r\n     \"EmailTemplateId\": \"ce508747-444b-4e1c-9f39-53629e47260b\",\r\n     \"SMSTemplateId\": \"87_9\",\r\n     \"NotifyTime\": \"\",\r\n     \"NotificationDay\": \"\",\r\n     \"TrumpiaKeyword\": \"\",\r\n     \"Level\": \"Service\",\r\n     \"CommodityType\": \"Water\",\r\n     \"ThresholdType\": \"Usage\",\r\n     \"ThresholdMin\": \"0.1\",\r\n     \"ThresholdMax\": \"1000000\"\r\n    }, {\r\n     \"InsightName\": \"ServiceLevelTieredThresholdApproaching\",\r\n     \"UtilityInsightName\": \"ServiceLevelTieredThresholdApproaching\",\r\n     \"AllowedCommunicationChannel\": \"File,Email,SMS,EmailandSMS\",\r\n     \"DefaultCommunicationChannel\": \"Email\",\r\n     \"DefaultFrequency\": \"Immediate\",\r\n     \"DefaultValue\": \"20\",\r\n     \"EmailTemplateId\": \"4962e788-fb1e-43cf-a2f6-faf713d25e0c\",\r\n     \"SMSTemplateId\": \"87_10\",\r\n     \"NotifyTime\": \"\",\r\n     \"NotificationDay\": \"\",\r\n     \"TrumpiaKeyword\": \"\",\r\n     \"Level\": \"Service\",\r\n     \"CommodityType\": \"Water\",\r\n     \"ThresholdType\": \"Percent\",\r\n     \"ThresholdMin\": \"1\",\r\n     \"ThresholdMax\": \"100\"\r\n    }, {\r\n     \"InsightName\": \"ServiceLevelTieredThresholdExceed\",\r\n     \"UtilityInsightName\": \"ServiceLevelTieredThresholdExceed\",\r\n     \"AllowedCommunicationChannel\": \"File,Email,SMS,EmailandSMS\",\r\n     \"DefaultCommunicationChannel\": \"Email\",\r\n     \"DefaultFrequency\": \"Immediate\",\r\n     \"DefaultValue\": \"\",\r\n     \"EmailTemplateId\": \"c1dc511c-0dd7-4208-bad7-8729fa097194\",\r\n     \"SMSTemplateId\": \"87_11\",\r\n     \"NotifyTime\": \"\",\r\n     \"NotificationDay\": \"\",\r\n     \"TrumpiaKeyword\": \"\",\r\n     \"Level\": \"Service\",\r\n     \"CommodityType\": \"Water\",\r\n     \"ThresholdType\": \"\",\r\n     \"ThresholdMin\": \"\",\r\n     \"ThresholdMax\": \"\"\r\n    }\r\n   ]\r\n  }\r\n ]\r\n}"
                }
            };
            return JsonConvert.DeserializeObject<ClientSettings>(clientSettingsJson[0].Value);
        }
    }
}