﻿using System;
using System.Threading.Tasks;
using AO.BusinessContracts;
using CE.AO.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace AO.Business.Tests.UnitTests
{
    /// <summary>
    /// Test Class for AlarmFacadeTest Class
    /// </summary>
    [TestClass]
    public class AlarmFacadeTest : TestBase
    {
        private AlarmFacade _alarmFacade;
        private Mock<IAlarm> _alarmMock;
        private Mock<IAlarmTypes> _alarmTypeMock;

        /// <summary>
        /// Test Case Init
        /// </summary>
        [TestInitialize]
        public void Init()
        {
            _alarmMock = new Mock<IAlarm>();
            _alarmTypeMock = new Mock<IAlarmTypes>();
            _alarmFacade = new AlarmFacade(LogModelMock.Object, new Lazy<IAlarm>(()=>_alarmMock.Object), new Lazy<IAlarmTypes>(()=>_alarmTypeMock.Object));
        }

        /// <summary>
        /// Test case for InsertOrMergeDcuAlarmAsync method for DcuAlarmFacade class
        /// </summary>
        [TestMethod]
        public void InsertOrMergeAlarmAsync()
        {
            AlarmModel alarmModel = new AlarmModel
            {
                ClientId = 89898989,
                AlarmTime = DateTime.UtcNow,
                AlarmGroupId = 99898989,
                AlarmGroupName = "TestGroupName",
                AlarmId = 79898989,
                AlarmSourceId = 69898989,
                AlarmSourceType = "TestSourceType",
                AlarmSourceTypeId = 59898989,
                AlarmTypeId = 49898989,
                AlarmTypeName = "TestAlarmTypeName"
            };

            _alarmMock.Setup(t => t.InsertOrMergeAlarmAsync(It.IsAny<AlarmModel>()))
                .Returns(Task.FromResult(true));

            _alarmTypeMock.Setup(t => t.GetAlarmType(It.IsAny<int>(), It.IsAny<int>(), It.IsAny<int>()))
                .Returns((new AlarmTypeModel() ));

            var result = _alarmFacade.InsertOrMergeAlarmAsync(alarmModel).Result;

            _alarmMock.Verify(t => t.InsertOrMergeAlarmAsync(It.IsAny<AlarmModel>()), Times.Once);
            _alarmTypeMock.Verify(t => t.GetAlarmType(It.IsAny<int>(), It.IsAny<int>(), It.IsAny<int>()), Times.Once);

            Assert.AreEqual(result, true);
        }
    }
}
