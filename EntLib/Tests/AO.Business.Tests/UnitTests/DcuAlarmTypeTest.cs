﻿using System;
using System.Linq.Expressions;
using System.Threading.Tasks;
using AO.Entities;
using CE.AO.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace AO.Business.Tests.UnitTests
{
    [TestClass]
    public class DcuAlarmTypeTest : TestBase
    {
        private DcuAlarmType _dcuAlarmType;

        /// <summary>
        /// Test Case Init
        /// </summary>
        [TestInitialize]
        public void Init()
        {
            _dcuAlarmType = new DcuAlarmType(CassandraRepositoryMock.Object, CacheProviderMock.Object);
        }

        /// <summary>
        /// Test case for GetDcuAlarmType method in DcuAlarmType class
        /// </summary>
        [TestMethod]
        public void GetDcuAlarmTypeTest()
        {
            // Arrange
            var dcuAlarmTypeEntity = new DcuAlarmTypeEntity
            {
                TableName = Constants.CassandraTableNames.DcuAlarmType,
                AlarmCode = "Dcu_Call_Me",
                ComponentType = "Test_Component",
                NotificationType = "Test_Notification",
                Severity = 100,
                UserFriendlyDescription = "Testing Entity"
            };

            CacheProviderMock.Setup(t => t.Get<DcuAlarmTypeEntity>(It.IsAny<string>(), It.IsAny<int>()))
                .Returns((DcuAlarmTypeEntity)null)
                .Verifiable();

            CacheProviderMock.Setup(t => t.Set(It.IsAny<string>(), It.IsAny<object>(), It.IsAny<int>())).Verifiable();

            CassandraRepositoryMock.Setup(
                t => t.GetSingle(It.IsAny<string>(), It.IsAny<Expression<Func<DcuAlarmTypeEntity, bool>>>()))
                .Returns(dcuAlarmTypeEntity).Verifiable();

            _dcuAlarmType.GetDcuAlarmType("Dcu_Call_Me");

            CacheProviderMock.Setup(t => t.Get<DcuAlarmTypeEntity>(It.IsAny<string>(), It.IsAny<int>()))
                .Returns(dcuAlarmTypeEntity)
                .Verifiable();

            // Act
            var result = _dcuAlarmType.GetDcuAlarmType("Dcu_Call_Me");

            // Assert
            Assert.AreEqual(result.AlarmCode, "Dcu_Call_Me");
            MockRepository.VerifyAll();
        }

        /// <summary>
        /// Test case for InsertOrMergeDcuAlarmTypeAsync method in DcuAlarmType class
        /// </summary>
        [TestMethod]
        public void InsertOrMergeDcuAlarmTypeAsync()
        {
            // Arrange
            CassandraRepositoryMock.Setup(
                t => t.InsertOrUpdateAsync(It.IsAny<DcuAlarmTypeEntity>(), It.IsAny<string>()))
                .Returns(Task.FromResult(true));
            CacheProviderMock.Setup(t => t.Set(It.IsAny<string>(), It.IsAny<object>(), It.IsAny<int>())).Verifiable();

            // Act
            var result = _dcuAlarmType.InsertOrMergeDcuAlarmTypeAsync(new DcuAlarmTypeModel());
            Task.WaitAll(result);

            // Assert
            MockRepository.VerifyAll();
        }

        /// <summary>
        /// Test case for DeleteDcuAlarmAsync method in DcuAlarmType class
        /// </summary>
        [TestMethod]
        public void DeleteDcuAlarmTypeAsync()
        {
            // Arrange
            CassandraRepositoryMock.Setup(t => t.DeleteAsync(It.IsAny<string>(), It.IsAny<Expression<Func<DcuAlarmTypeEntity, bool>>>())).Returns(Task.FromResult(true));
            CacheProviderMock.Setup(t => t.Remove(It.IsAny<string>(), It.IsAny<int>())).Verifiable();

            // Act
            var result = _dcuAlarmType.DeleteDcuAlarmTypeAsync(new DcuAlarmTypeModel()).Result;

            // Assert
            Assert.IsTrue(result);
            MockRepository.VerifyAll();
        }
    }
}
