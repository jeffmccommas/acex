﻿using System;
using System.Linq.Expressions;
using System.Threading.Tasks;
using AO.DataAccess.Contracts;
using AO.Entities;
using CE.AO.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System.Collections.Generic;
using System.Linq;

namespace AO.Business.Tests.UnitTests
{
    /// <summary>
    /// Test cases for ConsumptionByAccount class 
    /// </summary>
    [TestClass]
    public class ConsumptionByAccountTest : TestBase
    {
        private ConsumptionByAccount _consumptionByAccount;

        /// <summary>
        /// Test Case Init
        /// </summary>
        [TestInitialize]
        public void Init()
        {
            _consumptionByAccount = new ConsumptionByAccount(LogModelMock.Object, new Lazy<ICassandraRepository>(()=>CassandraRepositoryMock.Object));
        }

        /// <summary>
        /// Test case to insert ConsumptionByAccount Model
        /// Test case for InsertOrMergeConsumptionByAccountAsync method for ConsumptionByAccount class
        /// </summary>
        [TestMethod]
        public void InsertOrMergeConsumptionByAccountAsync()
        {
            CassandraRepositoryMock.Setup(t => t.InsertOrUpdateAsync(It.IsAny<ConsumptionByAccountEntity>(), It.IsAny<string>()))
                .Returns(Task.FromResult(true)).Verifiable();
            var consumptionByAccountModel = new ConsumptionByAccountModel();
            var result = _consumptionByAccount.InsertOrMergeConsumptionByAccountAsync(consumptionByAccountModel).Result;
            CassandraRepositoryMock.Verify(t => t.InsertOrUpdateAsync(It.IsAny<ConsumptionByAccountEntity>(), It.IsAny<string>()), Times.Once);
            CassandraRepositoryMock.VerifyAll();
            Assert.AreEqual(true, result);
            MockRepository.VerifyAll();
        }

        /// <summary>
        /// Test case to delete ConsumptionByAccount Model
        /// Test case for DeleteConsumptionByAccountAsync method for ConsumptionByAccount class
        /// </summary>
        [TestMethod]
        public void DeleteConsumptionByAccountAsync()
        {
            CassandraRepositoryMock.Setup(t => t.DeleteAsync(It.IsAny<string>(), It.IsAny<Expression<Func<ConsumptionByAccountEntity, bool>>>()))
                .Returns(Task.FromResult(typeof(void))).Verifiable();
            var consumptionByAccountModel = new ConsumptionByAccountModel();
            var result = _consumptionByAccount.DeleteConsumptionByAccountAsync(consumptionByAccountModel).Result;
            CassandraRepositoryMock.Verify(t => t.DeleteAsync(It.IsAny<string>(), It.IsAny<Expression<Func<ConsumptionByAccountEntity, bool>>>()), Times.Once);
            CassandraRepositoryMock.VerifyAll();
            Assert.AreEqual(true, result);
            MockRepository.VerifyAll();
        }

        /// <summary>
        /// Test case to get ConsumptionByAccountModel for AmiTimeStamp greater then or equal to startDateTime and AmiTimeStamp less than endDateTime
        /// </summary>
        [TestMethod]
        public void GetConsumptionByAccountByDateRangeAsync()
        {
            // Arrange
            var consumptionByAccount = new ConsumptionByAccountEntity()
            {
                AmiTimeStamp = DateTime.UtcNow,
                IntervalType = 60,
                TransponderPort = "1",
                UOMId = 3,
                AccountNumber = "Test Account",
                ClientId = 87,
                BatteryVoltage = "10",
                CommodityId = 3,
                MeterId = "Test Meter",
                TransponderId = "Test Transponder",
                Timezone = "PST",
                Consumption = 10,
                CustomerId = "Test Customer"
            };

            IList<ConsumptionByAccountEntity>  consumptionByAccountEntity = new List<ConsumptionByAccountEntity>();
            consumptionByAccountEntity.Add(consumptionByAccount);

            CassandraRepositoryMock.Setup(t => t.GetAsync(It.IsAny<string>(),
                It.IsAny<Expression<Func<ConsumptionByAccountEntity, bool>>>())).Returns(Task.FromResult(consumptionByAccountEntity));

            var consumptionByAccountList = _consumptionByAccount.GetConsumptionByAccountByDateRangeAsync(87,
                "Test Account", DateTime.UtcNow, DateTime.UtcNow);

            CassandraRepositoryMock.Verify(
                t => t.GetAsync(It.IsAny<string>(),
                    It.IsAny<Expression<Func<ConsumptionByAccountEntity, bool>>>()), Times.Once);

            // Act
            var result = consumptionByAccountList.Result.FirstOrDefault();

            // Assert
            CassandraRepositoryMock.Verify(
                t => t.GetAsync(It.IsAny<string>(),
                    It.IsAny<Expression<Func<ConsumptionByAccountEntity, bool>>>()), Times.Once);
            Assert.IsNotNull(result, "Expected to find at least one consumption by account aggregation.");
            Assert.AreEqual(result.UOMId, 3, $"Expected unit of measure Id of 3.  Found [{result.UOMId}]");
            MockRepository.VerifyAll();
        }
    }
}