﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using AO.Business.Tests.UnitTests.EventProcesser;
using AO.BusinessContracts;
using AO.BusinessContracts.EventProcessesContracts;
using AO.EventProcessors;
using CE.AO.Models;
using CE.AO.Models.RawFileModels;
using CsvHelper;
using Microsoft.ServiceBus.Messaging;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Newtonsoft.Json;
using Enums = CE.AO.Utilities.Enums;

namespace AO.Business.Tests.UnitTests
{
    /// <summary>
    /// Test case for EventHubEventProcessTriggerSubscriber
    /// </summary>
    [TestClass]
    public class EventHubEventProcessTriggerSubscriberTest :TestBase
    {
        private EventHubEventProcessTriggerSubscriber _eventHubEventProcessTriggerSubscriber;
        private Mock<IAzureQueue> _azureQueue;

        /// <summary>
        /// Test Case Init
        /// </summary>
        [TestInitialize]
        public void Init()
        {
            _azureQueue = new Mock<IAzureQueue>();
            _eventHubEventProcessTriggerSubscriber =
                new EventHubEventProcessTriggerSubscriber(new LogModel { DisableLog = true },
                    new Lazy<EventProcessorFactory<EventHubEventProcessTriggerSubscriber>>(), new IProcessEvent[0], 
                    new Lazy<IAzureQueue>(() => _azureQueue.Object));
        }

        /// <summary>
        /// </summary>%%
        [TestMethod]
        public void ProcessEventsAsync_Ami30()
        {
            var blockBlob = MockObject.GetBlockBlob("EST Timezone AMI_87_30min_Valid Data - Tush_Test.csv", "client87");
            var stream = blockBlob.OpenRead();
            var sr = new StreamReader(stream);
            using (var reader = new CsvReader(sr))
            {
                reader.Read();
                var record = reader.GetRecord<RawAmi30MinuteIntervalModel>();
                var serializedEventData = JsonConvert.SerializeObject(record);
                var eventData = new EventData(Encoding.UTF8.GetBytes(serializedEventData));
                eventData.Properties.Add("EventType", 1);
                eventData.Properties.Add("IntervalType", 30);
                eventData.Properties.Add("ClientId", "87");
                eventData.Properties.Add("ProcessingType", Enums.ProcessingType.File);
                eventData.Properties.Add("Source", "S");
                eventData.Properties.Add("RowIndex", 1);
                PartitionContext context = new PartitionContext();
                Lease lease = new Lease();
                context.Lease = lease;
                context.Lease.Offset = "Lease";
                context.Lease.PartitionId = "PartitionId";

                List<EventData> eventDataList = new List<EventData> { eventData };
                _eventHubEventProcessTriggerSubscriber.OpenAsync(context);

                var result = _eventHubEventProcessTriggerSubscriber.ProcessEventsAsync(context, eventDataList.AsEnumerable());

                Assert.AreEqual(result.Exception, null);
            }
        }
    }
}
