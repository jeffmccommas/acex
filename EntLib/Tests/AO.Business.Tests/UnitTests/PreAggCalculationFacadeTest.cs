﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using AO.BusinessContracts;
using CE.AO.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Enums = CE.AO.Utilities.Enums;

namespace AO.Business.Tests.UnitTests
{
    /// <summary>
    /// Test Case for PreAggCalculationFacade
    /// </summary>
    [TestClass]
    public class PreAggCalculationFacadeTest : TestBase
    {
        private PreAggCalculationFacade _preAggCalculationFacade;
        private Mock<IConsumptionByAccount> _consumptionByAccountMock;
        private Mock<IConsumptionAggByAccount> _consumptionAggByAccountTypeMock;
        private Mock<ITimingMetricsCalculator> _timingMetricsCalculatorMock;

        /// <summary>
        /// Test Case Init
        /// </summary>
        [TestInitialize]
        public void Init()
        {
            _consumptionByAccountMock = new Mock<IConsumptionByAccount>();
            _consumptionAggByAccountTypeMock = new Mock<IConsumptionAggByAccount>();
            _timingMetricsCalculatorMock = new Mock<ITimingMetricsCalculator>();

            _preAggCalculationFacade = new PreAggCalculationFacade(LogModelMock.Object,
                new Lazy<IConsumptionByAccount>(() => _consumptionByAccountMock.Object),
                new Lazy<IConsumptionAggByAccount>(() => _consumptionAggByAccountTypeMock.Object),
                new Lazy<ITimingMetricsCalculator>(() => _timingMetricsCalculatorMock.Object));
        }

        /// <summary>
        /// Test Method for CalculateAccountPreAgg
        /// </summary>
        [TestMethod]
        public void CalculateAccountPreAggForDay()
        {
            AccountPreAggCalculationRequestModel accountPreAggmodel = new AccountPreAggCalculationRequestModel
            {
                ClientId = 899898,
                AccountId = "TestAccount",
                AmiTimestamp = DateTime.UtcNow,
                CalculationRange = Enums.PreAggCalculationRange.Day,
                Timezone = "UTC"
            };

            IList<ConsumptionAggByAccountModel> consumptionAggregateByAccountList = new List<ConsumptionAggByAccountModel>();

            consumptionAggregateByAccountList.Add(new ConsumptionAggByAccountModel
            {
                StandardUOMId = 3,
                AccountNumber = "TestAccount",
                ClientId = 899898,
                CommodityId = 3,
                Timezone = "PST",
                StandardUOMConsumption = 10,
                AggregationEndDateTime = DateTime.UtcNow,
                AggregationType = Enums.PreAggregationTypes.Day.ToString()
            });

            IList<ConsumptionByAccountModel> consumptionByAccountList = new List<ConsumptionByAccountModel>();
            consumptionByAccountList.Add(new ConsumptionByAccountModel
            {
                AmiTimeStamp = DateTime.UtcNow,
                IntervalType = 60,
                TransponderPort = "1",
                UOMId = 3,
                AccountNumber = "TestAccount",
                ClientId = 899898,
                BatteryVoltage = "10",
                CommodityId = 3,
                MeterId = "Test Meter",
                TransponderId = "Test Transponder",
                Timezone = "PST",
                Consumption = 10,
                CustomerId = "Test Customer"
            });

            _consumptionAggByAccountTypeMock.Setup(
                p => p.GetConsumptionAggByAccountAsync(It.IsAny<int>(), It.IsAny<DateTime>(), It.IsAny<string>())).Returns(Task.FromResult(consumptionAggregateByAccountList));

            _consumptionByAccountMock.Setup(
                p => p.GetConsumptionByAccountByDateRangeAsync(It.IsAny<int>(), It.IsAny<string>()
                    , It.IsAny<DateTime>(), It.IsAny<DateTime>())).Returns(Task.FromResult(consumptionByAccountList));

            _consumptionAggByAccountTypeMock.Setup(p => p.InsertOrMergeConsumptionAggByAccountAsync(It.IsAny<List<ConsumptionAggByAccountModel>>()))
                                .Returns(Task.FromResult(true));

            var result = _preAggCalculationFacade.CalculateAccountPreAgg(accountPreAggmodel).Result;

            _consumptionAggByAccountTypeMock.Verify(p => p.GetConsumptionAggByAccountAsync(It.IsAny<int>(), It.IsAny<DateTime>(), It.IsAny<string>()), Times.Once);

            _consumptionByAccountMock.Verify(p => p.GetConsumptionByAccountByDateRangeAsync(It.IsAny<int>(), It.IsAny<string>()
                   , It.IsAny<DateTime>(), It.IsAny<DateTime>()), Times.Once);

            _consumptionAggByAccountTypeMock.Verify(p => p.InsertOrMergeConsumptionAggByAccountAsync(It.IsAny<List<ConsumptionAggByAccountModel>>()), Times.Once);

            Assert.AreEqual(result, true);

        }

        /// <summary>
        /// Test Method for CalculateAccountPreAgg
        /// </summary>
        [TestMethod]
        public void CalculateAccountPreAggForHour()
        {
            var accountPreAggmodel = new AccountPreAggCalculationRequestModel
            {
                ClientId = 899898,
                AccountId = "TestAccount",
                AmiTimestamp = DateTime.UtcNow,
                CalculationRange = Enums.PreAggCalculationRange.Hour,
                Timezone = "UTC"
            };

            IList<ConsumptionAggByAccountModel> consumptionAggregateByAccountList = new List<ConsumptionAggByAccountModel>();

            consumptionAggregateByAccountList.Add(new ConsumptionAggByAccountModel
            {
                StandardUOMId = 3,
                AccountNumber = "TestAccount",
                ClientId = 899898,
                CommodityId = 3,
                Timezone = "PST",
                StandardUOMConsumption = 10,
                AggregationEndDateTime = DateTime.UtcNow,
                AggregationType = Enums.PreAggregationTypes.Hour.ToString()
            });

            IList<ConsumptionByAccountModel> consumptionByAccountList = new List<ConsumptionByAccountModel>();
            consumptionByAccountList.Add(new ConsumptionByAccountModel
            {
                AmiTimeStamp = DateTime.UtcNow,
                IntervalType = 60,
                TransponderPort = "1",
                UOMId = 3,
                AccountNumber = "TestAccount",
                ClientId = 899898,
                BatteryVoltage = "10",
                CommodityId = 3,
                MeterId = "Test Meter",
                TransponderId = "Test Transponder",
                Timezone = "PST",
                Consumption = 10,
                CustomerId = "Test Customer"
            });

            _consumptionAggByAccountTypeMock.Setup(
                p => p.GetConsumptionAggByAccountAsync(It.IsAny<int>(), It.IsAny<DateTime>(), It.IsAny<string>())).Returns(Task.FromResult(consumptionAggregateByAccountList));

            _consumptionByAccountMock.Setup(
                p => p.GetConsumptionByAccountByDateRangeAsync(It.IsAny<int>(), It.IsAny<string>()
                    , It.IsAny<DateTime>(), It.IsAny<DateTime>())).Returns(Task.FromResult(consumptionByAccountList));

            _consumptionAggByAccountTypeMock.Setup(p => p.InsertOrMergeConsumptionAggByAccountAsync(It.IsAny<List<ConsumptionAggByAccountModel>>()))
                                .Returns(Task.FromResult(true));

            var result = _preAggCalculationFacade.CalculateAccountPreAgg(accountPreAggmodel).Result;

            _consumptionAggByAccountTypeMock.Verify(p => p.GetConsumptionAggByAccountAsync(It.IsAny<int>(), It.IsAny<DateTime>(), It.IsAny<string>()), Times.Once);

            _consumptionByAccountMock.Verify(p => p.GetConsumptionByAccountByDateRangeAsync(It.IsAny<int>(), It.IsAny<string>()
                   , It.IsAny<DateTime>(), It.IsAny<DateTime>()), Times.Once);

            _consumptionAggByAccountTypeMock.Verify(p => p.InsertOrMergeConsumptionAggByAccountAsync(It.IsAny<List<ConsumptionAggByAccountModel>>()), Times.Once);

            Assert.AreEqual(result, true);
        }

        /// <summary>
        /// Test Method for CalculateAccountPreAggForHour for DST hours
        /// </summary>
        [TestMethod]
        public void CalculateAccountPreAggForDstHour()
        {
            var utcDate = new DateTime(2012, 11, 4, 6, 0, 0);// DST second hour
            var accountPreAggmodel = new AccountPreAggCalculationRequestModel
            {
                ClientId = 899898,
                AccountId = "TestAccount",
                AmiTimestamp = utcDate,
                CalculationRange = Enums.PreAggCalculationRange.Hour,
                Timezone = "PST"
            };

            IList<ConsumptionAggByAccountModel> consumptionAggregateByAccountList = new List<ConsumptionAggByAccountModel>();
            consumptionAggregateByAccountList.Add(new ConsumptionAggByAccountModel
            {
                StandardUOMId = 3,
                AccountNumber = "TestAccount",
                ClientId = 899898,
                CommodityId = 3,
                Timezone = "PST",
                StandardUOMConsumption = 10,
                AggregationEndDateTime = utcDate,
                AggregationType = Enums.PreAggregationTypes.Hour.ToString()
            });

            IList<ConsumptionByAccountModel> consumptionByAccountList = new List<ConsumptionByAccountModel>();
            consumptionByAccountList.Add(new ConsumptionByAccountModel
            {
                AmiTimeStamp = utcDate,
                IntervalType = 60,
                TransponderPort = "1",
                UOMId = 3,
                AccountNumber = "TestAccount",
                ClientId = 899898,
                BatteryVoltage = "10",
                CommodityId = 3,
                MeterId = "Test Meter",
                TransponderId = "Test Transponder",
                Timezone = "PST",
                Consumption = 10,
                CustomerId = "Test Customer"
            });

            _consumptionAggByAccountTypeMock.Setup(
                p => p.GetConsumptionAggByAccountAsync(It.IsAny<int>(), It.IsAny<DateTime>(), It.IsAny<string>())).Returns(Task.FromResult(consumptionAggregateByAccountList));

            _consumptionByAccountMock.Setup(
                p => p.GetConsumptionByAccountByDateRangeAsync(It.IsAny<int>(), It.IsAny<string>()
                    , It.IsAny<DateTime>(), It.IsAny<DateTime>())).Returns(Task.FromResult(consumptionByAccountList));

            _consumptionAggByAccountTypeMock.Setup(p => p.InsertOrMergeConsumptionAggByAccountAsync(It.IsAny<List<ConsumptionAggByAccountModel>>()))
                                .Returns(Task.FromResult(true));

            var result = _preAggCalculationFacade.CalculateAccountPreAgg(accountPreAggmodel).Result;

            _consumptionAggByAccountTypeMock.VerifyAll();
            _consumptionByAccountMock.VerifyAll();

            Assert.AreEqual(result, true);

            utcDate = new DateTime(2012, 11, 4, 5, 0, 0);// DST first hour
            accountPreAggmodel = new AccountPreAggCalculationRequestModel
            {
                ClientId = 899898,
                AccountId = "TestAccount",
                AmiTimestamp = utcDate,
                CalculationRange = Enums.PreAggCalculationRange.Hour,
                Timezone = "PST"
            };

            consumptionAggregateByAccountList = new List<ConsumptionAggByAccountModel>
            {
                new ConsumptionAggByAccountModel
                {
                    StandardUOMId = 3,
                    AccountNumber = "TestAccount",
                    ClientId = 899898,
                    CommodityId = 3,
                    Timezone = "PST",
                    StandardUOMConsumption= 10,
                    AggregationEndDateTime = utcDate,
                    AggregationType = Enums.PreAggregationTypes.Hour.ToString()
                }
            };

            consumptionByAccountList = new List<ConsumptionByAccountModel>
            {
                new ConsumptionByAccountModel
                {
                    AmiTimeStamp = utcDate,
                    IntervalType = 60,
                    TransponderPort = "1",
                    UOMId = 3,
                    AccountNumber = "TestAccount",
                    ClientId = 899898,
                    BatteryVoltage = "10",
                    CommodityId = 3,
                    MeterId = "Test Meter",
                    TransponderId = "Test Transponder",
                    Timezone = "PST",
                    Consumption = 10,
                    CustomerId = "Test Customer"
                }
            };

            _consumptionAggByAccountTypeMock.Setup(
                p => p.GetConsumptionAggByAccountAsync(It.IsAny<int>(), It.IsAny<DateTime>(), It.IsAny<string>())).Returns(Task.FromResult(consumptionAggregateByAccountList));

            _consumptionByAccountMock.Setup(
                p => p.GetConsumptionByAccountByDateRangeAsync(It.IsAny<int>(), It.IsAny<string>()
                    , It.IsAny<DateTime>(), It.IsAny<DateTime>())).Returns(Task.FromResult(consumptionByAccountList));

            _consumptionAggByAccountTypeMock.Setup(p => p.InsertOrMergeConsumptionAggByAccountAsync(It.IsAny<List<ConsumptionAggByAccountModel>>()))
                                .Returns(Task.FromResult(true));

            result = _preAggCalculationFacade.CalculateAccountPreAgg(accountPreAggmodel).Result;

            _consumptionAggByAccountTypeMock.VerifyAll();
            _consumptionByAccountMock.VerifyAll();

            Assert.AreEqual(result, true);
        }
    }
}
