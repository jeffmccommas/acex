﻿using System;
using System.Threading.Tasks;
using AO.BusinessContracts;
using CE.AO.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace AO.Business.Tests.UnitTests
{
    /// <summary>
    /// contains test methods for BillingFacade class
    /// </summary>
    [TestClass]
    public class BillingFacadeTest : TestBase
    {
        private BillingFacade _billingFacadeManager;

        private Mock<ICustomer> _customerMock;

        private Mock<IClientAccount> _clientAccountMock;

        private Mock<IBilling> _billingMock;

        private Mock<IPremise> _premiseMock;

        private Mock<ICustomerMobileNumberLookup> _customerMobileNumberMock;

        private Mock<IAccountLookup> _accountLookupMock;

        private Mock<IAccountUpdatesFacade> _accountUpdateMock;

        private Mock<IClientConfigFacade> _clientConfigFacadeManager;
        private Mock<IBillingCycleSchedule> _billingCycleScheduleManager;
        /// <summary>
        /// Test Case Init
        /// </summary>
        [TestInitialize]
        public void Init()
        {
            _customerMock = new Mock<ICustomer>();
            _clientAccountMock = new Mock<IClientAccount>();
            _billingMock = new Mock<IBilling>();
            _premiseMock = new Mock<IPremise>();
            _customerMobileNumberMock = new Mock<ICustomerMobileNumberLookup>();
            _accountLookupMock = new Mock<IAccountLookup>();
            _accountUpdateMock = new Mock<IAccountUpdatesFacade>();
            _clientConfigFacadeManager = new Mock<IClientConfigFacade>();
            _billingCycleScheduleManager = new Mock<IBillingCycleSchedule>();

            _billingFacadeManager = new BillingFacade(_customerMock.Object, _clientAccountMock.Object,
                _billingMock.Object, _premiseMock.Object, _customerMobileNumberMock.Object, _accountLookupMock.Object,
                _accountUpdateMock.Object, _clientConfigFacadeManager.Object, _billingCycleScheduleManager.Object);
        }

        /// <summary>
        /// Test method to Insert billing information
        /// </summary>
        [TestMethod]
        public void InsertOrMergeBillingAsync()
        {
            var billingCustomerPremiseModel = new BillingCustomerPremiseModel()
            {
                ClientId = 1,
                customer_id = "1",
                account_id = "1",
                meter_id = "1",
                first_name = "Sharad",
                last_name = "Saini",
                site_addressline1 = "B-804",
                site_addressline2 = "Akshay Tower",
                site_addressline3 = "New York",
                service_commodity = 2,
                phone_1 = "9911559988",
                email = "sharad.saini@saviantconsulting.com",
                meter_replaces_meterid = "2",
                premise_id = "B-804",
                Service_contract = "SC001",
                service_point_id = "SC001",
                bill_days = 20,
                service_read_date = DateTime.Now,
                bill_enddate = DateTime.Now,
                is_estimate = "0",
                customer_type = "Commercial"

            };

            _customerMock.Setup(t => t.InsertOrMergeCustomerAsync(It.IsAny<CustomerModel>())).Returns(Task.FromResult(true)).Verifiable();
            _clientAccountMock.Setup(t => t.InsertOrMergeClientAccount(It.IsAny<ClientAccountModel>())).Returns(Task.FromResult(true)).Verifiable();


            var billingModel = new BillingModel()
            {
                ClientId = 1,
                CustomerId = "1",
                AccountId = "1",
                PremiseId = "B-804",
                AmiStartDate = DateTime.Now,
                AmiEndDate = DateTime.Now,
                BillCycleScheduleId = "BS12122",
                RateClass = "Rate Class",
                ServicePointId = "SP0012",
                Source = "Rate Class",
                MeterType = "",
                UOMId = 12,
                TotalUsage = 1232323,
                StartDate = DateTime.Now,
                EndDate = DateTime.Today.AddDays(12),
                BillDays = 12,
                ReadQuality = "0",
                TotalCost = 122323,
                CommodityId = 2,
                MeterId = " ",
                BillPeriodType = 2,
                ReplacedMeterId = "",
                ReadDate = DateTime.Now,
                ServiceContractId = "SC001",
                IsFault = false,
                IsLatest = true,

            };

            _billingMock.Setup(t => t.GetServiceDetailsFromBillAsync(It.IsAny<int>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>())).Returns(Task.FromResult(billingModel)).Verifiable();

            _premiseMock.Setup(t => t.InsertOrMergePremiseAsync(It.IsAny<PremiseModel>())).Returns(Task.FromResult(true)).Verifiable();
            _customerMobileNumberMock.Setup(t => t.InsertOrMergeCustomerMobileNumberLookupAsync(It.IsAny<CustomerMobileNumberLookupModel>())).Returns(Task.FromResult(true)).Verifiable();
            _accountLookupMock.Setup(t => t.InsertOrMergeAccountLookupAsync(It.IsAny<AccountLookupModel>())).Returns(Task.FromResult(true)).Verifiable();

            // Act
            var result = _billingFacadeManager.InsertOrMergeBillingAsync(billingCustomerPremiseModel);

            // Assert
            _customerMock.Verify(t => t.InsertOrMergeCustomerAsync(It.IsAny<CustomerModel>()), Times.Once);
            _customerMock.VerifyAll();

            _clientAccountMock.Verify(t => t.InsertOrMergeClientAccount(It.IsAny<ClientAccountModel>()), Times.Once);
            _clientAccountMock.VerifyAll();

            _billingMock.Verify(t => t.GetServiceDetailsFromBillAsync(It.IsAny<int>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>()), Times.Once);
            _billingMock.VerifyAll();

            _premiseMock.Verify(t => t.InsertOrMergePremiseAsync(It.IsAny<PremiseModel>()), Times.Once);
            _premiseMock.VerifyAll();

            _customerMobileNumberMock.Verify(t => t.InsertOrMergeCustomerMobileNumberLookupAsync(It.IsAny<CustomerMobileNumberLookupModel>()), Times.Once);
            _customerMobileNumberMock.VerifyAll();

            _accountLookupMock.Verify(t => t.InsertOrMergeAccountLookupAsync(It.IsAny<AccountLookupModel>()), Times.Once);
            _accountLookupMock.VerifyAll();

            Assert.AreEqual(true, result.Result);
            MockRepository.VerifyAll();
        }


        /// <summary>
        /// Test method to Insert billing information
        /// </summary>
        [TestMethod]
        public void InsertOrMergeBillingAsync_NullEmailPhone()
        {
            // Arrange
            var billingCustomerPremiseModel = new BillingCustomerPremiseModel()
            {
                ClientId = 1,
                customer_id = "1",
                account_id = "1",
                meter_id = "1",
                first_name = "Sharad",
                last_name = "Saini",
                site_addressline1 = "B-804",
                site_addressline2 = "Akshay Tower",
                site_addressline3 = "New York",
                service_commodity = 2,
                phone_1 = "",
                email = "",
                meter_replaces_meterid = "2",
                premise_id = "B-804",
                Service_contract = "",
                service_point_id = "SC001",
                bill_days = 20,
                service_read_date = DateTime.Now,
                bill_enddate = DateTime.Now.AddDays(13),
                is_estimate = "0",
                customer_type = "Commercial"
            };

            _customerMock.Setup(t => t.InsertOrMergeCustomerAsync(It.IsAny<CustomerModel>())).Returns(Task.FromResult(true)).Verifiable();
            _customerMock.Setup(t => t.GetCustomerAsync(It.IsAny<int>(), It.IsAny<string>())).Returns(Task.FromResult(new CustomerModel()
            {
                Phone1 = "9028179781",
                Phone2 = "9028179782",
                EmailAddress = "t@gmail.com"
            })).Verifiable();
            
            _clientAccountMock.Setup(t => t.InsertOrMergeClientAccount(It.IsAny<ClientAccountModel>())).Returns(Task.FromResult(true)).Verifiable();

            var billingModel = new BillingModel()
            {
                ClientId = 1,
                CustomerId = "1",
                AccountId = "1",
                PremiseId = "B-804",
                AmiStartDate = DateTime.Now,
                AmiEndDate = DateTime.Now,
                BillCycleScheduleId = "BS12122",
                RateClass = "Rate Class",
                ServicePointId = "",
                Source = "Rate Class",
                MeterType = "",
                UOMId = 12,
                TotalUsage = 1232323,
                StartDate = DateTime.Now,
                EndDate = DateTime.Today.AddDays(12),
                BillDays = 12,
                ReadQuality = "0",
                TotalCost = 122323,
                CommodityId = 2,
                MeterId = " ",
                BillPeriodType = 2,
                ReplacedMeterId = "",
                ReadDate = DateTime.Now,
                ServiceContractId = "SC001",
                IsFault = false,
                IsLatest = true
            };

            _billingMock.Setup(t => t.GetServiceDetailsFromBillAsync(It.IsAny<int>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>())).Returns(Task.FromResult(billingModel)).Verifiable();
            _premiseMock.Setup(t => t.InsertOrMergePremiseAsync(It.IsAny<PremiseModel>())).Returns(Task.FromResult(true)).Verifiable();

            _customerMobileNumberMock.Setup(t => t.InsertOrMergeCustomerMobileNumberLookupAsync(It.IsAny<CustomerMobileNumberLookupModel>())).Returns(Task.FromResult(true)).Verifiable();

            _accountLookupMock.Setup(t => t.InsertOrMergeAccountLookupAsync(It.IsAny<AccountLookupModel>())).Returns(Task.FromResult(true)).Verifiable();

            _accountLookupMock.Setup(t => t.GetAccountMeterDetails(It.IsAny<int>(), It.IsAny<string>(), It.IsAny<string>())).Returns(Task.FromResult(new AccountLookupModel()
            {
                IsServiceContractCreatedBySystem = true
            })).Verifiable();

            // Act
            var result = _billingFacadeManager.InsertOrMergeBillingAsync(billingCustomerPremiseModel);

            // Assert
            _customerMock.Verify(t => t.InsertOrMergeCustomerAsync(It.IsAny<CustomerModel>()), Times.Once);
            _clientAccountMock.Verify(t => t.InsertOrMergeClientAccount(It.IsAny<ClientAccountModel>()), Times.Once);
            _billingMock.Verify(t => t.GetServiceDetailsFromBillAsync(It.IsAny<int>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>()), Times.Once);
            _premiseMock.Verify(t => t.InsertOrMergePremiseAsync(It.IsAny<PremiseModel>()), Times.Once);
            _customerMobileNumberMock.Verify(t => t.InsertOrMergeCustomerMobileNumberLookupAsync(It.IsAny<CustomerMobileNumberLookupModel>()), Times.Once);
            _accountLookupMock.Verify(t => t.InsertOrMergeAccountLookupAsync(It.IsAny<AccountLookupModel>()), Times.AtMost(2));
            _accountLookupMock.Verify(t => t.GetAccountMeterDetails(It.IsAny<int>(), It.IsAny<string>(), It.IsAny<string>()), Times.Once);

            Assert.AreEqual(true, result.Result);
            MockRepository.VerifyAll();}
    }
}
