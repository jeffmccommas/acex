﻿using System;
using System.Globalization;
using System.Linq.Expressions;
using System.Threading.Tasks;
using AO.BusinessContracts;
using AO.DataAccess.Contracts;
using AO.Entities;
using CE.AO.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace AO.Business.Tests.UnitTests
{
    [TestClass]
    public class CalculationTest : TestBase
    {
        private ICalculation _calculation;

        /// <summary>
        /// Test Case Init
        /// </summary>
        [TestInitialize]
        public void Init()
        {
            _calculation = new Calculation(LogModelMock.Object, new Lazy<ICassandraRepository>(()=>CassandraRepositoryMock.Object));
        }

        /// <summary>
        /// Test case for inserting ctd Calculations
        /// </summary>
        [TestMethod]
        public void InsertCtdCalculationAsync()
        {
            var calculationModel = new CalculationModel
            {
                ClientId = 2,
                AccountId = "23",
                CustomerId = "32",
                MeterId = "23",
                ServiceContractId = "56",
                CommodityId = 23,
                Unit = 78,
                Cost = 105.245,

                ProjectedCost = 3333,
                ProjectedUsage = 222,
                Usage = 22,
                BillDays = 67,
                BillCycleStartDate = DateTime.Now.ToString(CultureInfo.InvariantCulture),
                BillCycleEndDate = DateTime.Now.ToString(CultureInfo.InvariantCulture),
                AsOfAmiDate = DateTime.Now,
                AsOfCalculationDate = DateTime.Now,
                RateClass = "C",
                AverageDailyUsage = 6785,
                AverageDailyCost = 98,
                Type = "X",
                IsLatest = true
            };

            var calculationEntity = new CalculationEntity
            {
                ClientId = 2,
                AccountId = "23",
                CustomerId = "32",
                MeterId = "23",
                ServiceContractId = "56",
                CommodityId = 23,
                Unit = 78,
                Cost = 105.245,
                ProjectedCost = 3333,
                ProjectedUsage = 222,
                Usage = 22,
                BillDays = 67,
                BillCycleStartDate = DateTime.Now,
                BillCycleEndDate = DateTime.Now,
                AsOfAmiDate = DateTime.Now,
                AsOfCalculationDate = DateTime.Now,
                RateClass = "C",
                AverageDailyUsage = 6785,
                AverageDailyCost = 98,
                Type = "X",
                IsLatest = true
            };
            
            CassandraRepositoryMock.Setup(t => t.GetSingleAsync(It.IsAny<string>(), It.IsAny<Expression<Func<CalculationEntity, bool>>>())).Returns(Task.FromResult(calculationEntity));
            CassandraRepositoryMock.Setup(t => t.DeleteAsync(It.IsAny<string>(), It.IsAny<Expression<Func<CalculationEntity, bool>>>())).Returns(Task.FromResult(typeof(void)));
            CassandraRepositoryMock.Setup(t => t.InsertOrUpdateAsync(It.IsAny<CalculationEntity>(), It.IsAny<string>())).Returns(Task.FromResult(true)).Verifiable();

            _calculation.InsertCtdCalculationAsync(calculationModel);

            CassandraRepositoryMock.Verify(t => t.GetSingleAsync(It.IsAny<string>(), It.IsAny<Expression<Func<CalculationEntity, bool>>>()), Times.Once);
            CassandraRepositoryMock.Verify(t => t.DeleteAsync(It.IsAny<string>(), It.IsAny<Expression<Func<CalculationEntity, bool>>>()), Times.AtMost(1));
            CassandraRepositoryMock.Verify(t => t.InsertOrUpdateAsync(It.IsAny<CalculationEntity>(), It.IsAny<string>()), Times.Exactly(2));
            MockRepository.VerifyAll();
        }

        /// <summary>
        /// Test Method For Btd Calculations
        /// </summary>
        [TestMethod]
        public void InsertBtdCalculationAsync()
        {
            var calculationModel = new CalculationModel
            {
                ClientId = 2,
                AccountId = "23",
                CustomerId = "32",
                MeterId = "23",
                ServiceContractId = "56",
                CommodityId = 23,
                Unit = 78,
                Cost = 105.245,

                ProjectedCost = 3333,
                ProjectedUsage = 222,
                Usage = 22,
                BillDays = 67,
                BillCycleStartDate = DateTime.Now.ToString(CultureInfo.InvariantCulture),
                BillCycleEndDate = DateTime.Now.ToString(CultureInfo.InvariantCulture),
                AsOfAmiDate = DateTime.Now,
                AsOfCalculationDate = DateTime.Now,
                RateClass = "C",
                AverageDailyUsage = 6785,
                AverageDailyCost = 98,
                Type = "X",
                IsLatest = true
            };

            var calculationEntity = new CalculationEntity
            {
                ClientId = 2,
                AccountId = "23",
                CustomerId = "32",
                MeterId = "23",
                ServiceContractId = "56",
                CommodityId = 23,
                Unit = 78,
                Cost = 105.245,
                ProjectedCost = 3333,
                ProjectedUsage = 222,
                Usage = 22,
                BillDays = 67,
                BillCycleStartDate = DateTime.Now,
                BillCycleEndDate = DateTime.Now,
                AsOfAmiDate = DateTime.Now,
                AsOfCalculationDate = DateTime.Now,
                RateClass = "C",
                AverageDailyUsage = 6785,
                AverageDailyCost = 98,
                Type = "X",
                IsLatest = true
            };
            
            CassandraRepositoryMock.Setup(t => t.GetSingleAsync(It.IsAny<string>(), It.IsAny<Expression<Func<CalculationEntity, bool>>>())).Returns(Task.FromResult(calculationEntity));
            CassandraRepositoryMock.Setup(t => t.DeleteAsync(It.IsAny<string>(), It.IsAny<Expression<Func<CalculationEntity, bool>>>())).Returns(Task.FromResult(typeof(void)));
            CassandraRepositoryMock.Setup(t => t.InsertOrUpdateAsync(It.IsAny<CalculationEntity>(), It.IsAny<string>())).Returns(Task.FromResult(true)).Verifiable();

            _calculation.InsertBtdCalculationAsync(calculationModel);

            CassandraRepositoryMock.Verify(t => t.GetSingleAsync(It.IsAny<string>(), It.IsAny<Expression<Func<CalculationEntity, bool>>>()), Times.Once);
            CassandraRepositoryMock.Verify(t => t.DeleteAsync(It.IsAny<string>(), It.IsAny<Expression<Func<CalculationEntity, bool>>>()), Times.AtMost(1));
            CassandraRepositoryMock.Verify(t => t.InsertOrUpdateAsync(It.IsAny<CalculationEntity>(), It.IsAny<string>()), Times.Exactly(2));

            MockRepository.VerifyAll();
        }
        
        /// <summary>
        /// Test Method For Get Btd Calculations
        /// </summary>
        [TestMethod]
        public void GetBtdCalculationAsync()
        {
            var calculationEntity = new CalculationEntity
            {
                ClientId = 2,
                AccountId = "23",
                CustomerId = "32",
                MeterId = "23",
                ServiceContractId = "56",
                CommodityId = 23,
                Unit = 78,
                Cost = 105.245,
                ProjectedCost = 3333,
                ProjectedUsage = 222,
                Usage = 22,
                BillDays = 67,
                BillCycleStartDate = DateTime.Now,
                BillCycleEndDate = DateTime.Now,
                AsOfAmiDate = DateTime.Now,
                AsOfCalculationDate = DateTime.Now,
                RateClass = "C",
                AverageDailyUsage = 6785,
                AverageDailyCost = 98,
                Type = "X",
                IsLatest = true
            };

            CassandraRepositoryMock.Setup(t => t.GetSingleAsync(It.IsAny<string>(), It.IsAny<Expression<Func<CalculationEntity, bool>>>())).Returns(Task.FromResult(calculationEntity));
            var result = _calculation.GetBtdCalculationAsync(2, "23").Result;
            CassandraRepositoryMock.Verify(t => t.GetSingleAsync(It.IsAny<string>(), It.IsAny<Expression<Func<CalculationEntity, bool>>>()), Times.Once);
            Assert.AreEqual(result.ClientId, 2);
        }

        /// <summary>
        /// Test Method For GEt Btd Calculations
        /// </summary>
        [TestMethod]
        public void GetCtdCalculationAsync()
        {
            var calculationEntity = new CalculationEntity
            {
                ClientId = 2,
                AccountId = "23",
                CustomerId = "32",
                MeterId = "23",
                ServiceContractId = "56",
                CommodityId = 23,
                Unit = 78,
                Cost = 105.245,
                ProjectedCost = 3333,
                ProjectedUsage = 222,
                Usage = 22,
                BillDays = 67,
                BillCycleStartDate = DateTime.Now,
                BillCycleEndDate = DateTime.Now,
                AsOfAmiDate = DateTime.Now,
                AsOfCalculationDate = DateTime.Now,
                RateClass = "C",
                AverageDailyUsage = 6785,
                AverageDailyCost = 98,
                Type = "X",
                IsLatest = true
            };
            CassandraRepositoryMock.Setup(t => t.GetSingleAsync(It.IsAny<string>(), It.IsAny<Expression<Func<CalculationEntity, bool>>>())).Returns(Task.FromResult(calculationEntity));
            var result = _calculation.GetCtdCalculationAsync(2, "23","56").Result;
            CassandraRepositoryMock.Verify(t => t.GetSingleAsync(It.IsAny<string>(), It.IsAny<Expression<Func<CalculationEntity, bool>>>()), Times.Once);
            Assert.AreEqual(result.ClientId, 2);

            MockRepository.VerifyAll();
        }
    }
}

