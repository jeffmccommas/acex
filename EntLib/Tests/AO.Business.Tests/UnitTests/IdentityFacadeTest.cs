﻿using System;
using AO.BusinessContracts;
using AO.DataAccess.Contracts;
using AO.Domain;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace AO.Business.Tests.UnitTests
{
    /// <summary>
    /// Test Case Class for IdentityFacade
    /// </summary>
    [TestClass]
    public class IdentityFacadeTest : TestBase
    {
        private Mock<ITenantRepository> _tenantRepository;

        private IIdentityFacade _identityFacade;

        private static Tenant GetTenant()
        {
            Tenant tenant = new Tenant()
            {
                TenantId = "mock-tenant-identifier",
                ClientId = 999,
                Client = new Client()
                {
                    ClientID = 999,
                    Name = "MockClient",
                    Description = "Some mocked up client",
                    RateCompanyID = 999,
                    ReferrerID = 999,
                    NewDate = new DateTime(2000, 1, 1),
                    UpdDate = new DateTime(2000, 1, 1),
                    AuthType = 9,
                    EnableInd = true
                }
            };

            return tenant;
        }

        /// <summary>
        /// Initialization of test case.
        /// </summary>
        [TestInitialize]
        public void Init()
        {
            _tenantRepository = new Mock<ITenantRepository>();

            _identityFacade = new IdentityFacade(_tenantRepository.Object);
        }

        /// <summary>
        /// Test case for GetClientIdForTenant() method. Ensure ClientId property of Tenant object is returned.
        /// </summary>
        [TestMethod]
        public void GetClientIdForCurrentUser()
        {
            var tenant = GetTenant();

            _tenantRepository.Setup(t => t.GetByTenantId(It.IsAny<string>())).Returns(tenant);

            var result = _identityFacade.GetClientIdForTenant(tenant.TenantId);

            // Verify result comes back correctly.
            Assert.IsInstanceOfType(result, typeof(int));
            Assert.AreEqual(result, 999);
        }
    }
}
