﻿using System;
using System.Linq.Expressions;
using System.Threading.Tasks;
using AO.Entities;
using CE.AO.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace AO.Business.Tests.UnitTests
{
    [TestClass]
    public class DcuAlarmCodeTest : TestBase
    {
        private DcuAlarmCode _dcuAlarmCode;

        /// <summary>
        /// Test Case Init
        /// </summary>
        [TestInitialize]
        public void Init()
        {
            _dcuAlarmCode = new DcuAlarmCode(CassandraRepositoryMock.Object);
        }

        /// <summary>
        /// Test case for InsertOrMergeDcuAlarmCodeAsync method in DcuAlarmCode class
        /// </summary>
        [TestMethod]
        public void InsertOrMergeDcuAlarmCodeAsync()
        {
            // Arrange
            CassandraRepositoryMock.Setup(
                t => t.InsertOrUpdateAsync(It.IsAny<DcuAlarmByCodeEntity>(), It.IsAny<string>()))
                .Returns(Task.FromResult(true));
            
            // Act
            var result = _dcuAlarmCode.InsertOrMergeDcuAlarmCodeAsync(new DcuAlarmModel()).Result;

            // Assert
            CassandraRepositoryMock.Verify(t => t.InsertOrUpdateAsync(It.IsAny<DcuAlarmByCodeEntity>(), It.IsAny<string>()), Times.Once);
            Assert.IsTrue(result);
            MockRepository.VerifyAll();
        }

        /// <summary>
        /// Test case for DeleteDcuAlarmCodeAsync method in DcuAlarmCode class
        /// </summary>
        [TestMethod]
        public void DeleteDcuAlarmCodeAsync()
        {
            // Arrange
            CassandraRepositoryMock.Setup(t => t.DeleteAsync(It.IsAny<string>(), It.IsAny<Expression<Func<DcuAlarmByCodeEntity, bool>>>())).Returns(Task.FromResult(true));

            // Act
            var result = _dcuAlarmCode.DeleteDcuAlarmCodeAsync(new DcuAlarmModel()).Result;

            // Assert
            CassandraRepositoryMock.Verify(t => t.Delete(It.IsAny<string>(), It.IsAny<Expression<Func<DcuAlarmByCodeEntity, bool>>>()), Times.Never);
            Assert.IsTrue(result);
            MockRepository.VerifyAll();
        }

        /// <summary>
        /// Test case for DeleteDcuAlarmCodeWithPartionAsync method in DcuAlarmCode class
        /// </summary>
        [TestMethod]
        public void DeleteDcuAlarmCodeWithPartionAsync()
        {
            // Arrange
            CassandraRepositoryMock.Setup(t => t.DeleteAsync(It.IsAny<string>(), It.IsAny<Expression<Func<DcuAlarmByCodeEntity, bool>>>())).Returns(Task.FromResult(true));

            // Act
            var result = _dcuAlarmCode.DeleteDcuAlarmCodeWithPartionAsync(new DcuAlarmModel()).Result;

            // Assert
            CassandraRepositoryMock.Verify(t => t.Delete(It.IsAny<string>(), It.IsAny<Expression<Func<DcuAlarmByCodeEntity, bool>>>()), Times.Never);
            Assert.IsTrue(result);
            MockRepository.VerifyAll();
        }

    }
}
