﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AO.BusinessContracts;
using AO.DataAccess.Contracts;
using AO.Entities;
using CE.AO.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace AO.Business.Tests.UnitTests
{
    [TestClass]
    public class EmailRequestDetailTest : TestBase
    {
        private readonly IEmailRequestDetail _emailRequestDetail;
        
        public EmailRequestDetailTest()
        {
            _emailRequestDetail = new EmailRequestDetail(LogModelMock.Object,
                new Lazy<ICassandraRepository>(() => CassandraRepositoryMock.Object));
        }

        [TestMethod]
        public void InsertEmailRequestDetailUnitTest()
        {
            // Assert 
            var email = new EmailRequestDetailModel()
            {
                AccountId = "test",
                ClientId = 1,
                ProgramName = "test",
                ServiceContractId = "contract",
                Insight = "whatisaninsight",
                Channel = "2ismyfavorite",
                NotifiedDateTime = DateTime.UtcNow
            };

            CassandraRepositoryMock.Setup(
                mock =>
                    mock.InsertOrUpdateAsync(It.Is<EmailRequestDetailEntity>(e => e.LastModifiedDate > DateTime.MinValue), It.IsAny<string>())).Returns(Task.FromResult(true)).Verifiable();

            // Act
            _emailRequestDetail.InsertOrMergeEmailRequestDetail(email);
        }
    }
}
