﻿using System.Collections.Generic;
using AO.BusinessContracts;
using CE.AO.Models;
using CE.ContentModel;
using CE.ContentModel.Entities;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace AO.Business.Tests.UnitTests
{
    /// <summary>
    /// Test Case Class for ClientConfigFacade
    /// </summary>
    [TestClass]
    public class ClientConfigFacadeTest : TestBase
    {
        private IClientConfigFacade _clientConfigFacade;
        private Mock<IContentProvider> _contentProviderMock;
        private Mock<IContentModelFactory> _contentModelFactoryMock;
        /// <summary>
        /// Initialization of test case.
        /// </summary>
        [TestInitialize]
        public void Init()
        {
            _contentModelFactoryMock = new Mock<IContentModelFactory>();
            _contentProviderMock = new Mock<IContentProvider>();
            _clientConfigFacade = new ClientConfigFacade(_contentModelFactoryMock.Object);
        }

        private static List<PortalModuleSystemConfigurationBulk> GetSystemConfigBulkEntities()
        {
            List<PortalModuleSystemConfigurationBulk> systemConfigEntities = new List<PortalModuleSystemConfigurationBulk>()
            {
                new PortalModuleSystemConfigurationBulk()
                {
                    Category = "SystemModuleConfig",
                    JsonValue =
                        "[" +
                            "{" +
                                "\"id\": 1," +
                                "\"name\":  \"Mock System Mod 1\"," +
                                "\"resources\": [" +
                                    "\"Bundles/Mock1-UI.bundle.js\"" +
                                "]," +
                                "\"menuItems\": [" +
                                    "{" +
                                        "\"position\": 1," +
                                        "\"label\": \"Mock Menu Item Sys 1.1\"," +
                                        "\"icon\": \"fa-mock-menu-sys11\"," +
                                        "\"state\": \"mock.state.menu.sys11\"," +
                                        "\"keywords\": [\"mock-keyword-menu-sys11\"]," +
                                        "\"securityResource\":  \"mockSecurityResourceIdSys11\"," +
                                        "\"metadata\": {" +
                                            "\"mockMetadataKeySys11\": \"mockMetadataValueSys11\"," +
                                            "\"mockMetadataKeySys12\": \"mockMetadataValueSys12\"" +
                                        "}," +
                                        "\"menuItems\": [" +
                                            "{" +
                                                "\"position\": 1," +
                                                "\"label\": \"Mock Submenu Item Sys 1.1.1\"," +
                                                "\"icon\": \"fa-mock-submenu-sys111\"," +
                                                "\"state\": \"mock.state.submenu.sys111\"," +
                                                "\"keywords\": [\"mock-keyword-submenu-sys111\"]," +
                                                "\"securityResource\":  \"mockSecurityResourceIdSys111\"," +
                                                "\"metadata\": {" +
                                                    "\"mockMetadataKeySys111\": \"mockMetadataValueSys111\"," +
                                                    "\"mockMetadataKeySys112\": \"mockMetadataValueSys112\"" +
                                                "}," +
                                                "\"menuItems\": []" +
                                            "}," +
                                            "{" +
                                                "\"position\": 2," +
                                                "\"label\": \"Mock Submenu Item Sys 1.1.2\"," +
                                                "\"icon\": \"\"," +
                                                "\"state\": \"\"," +
                                                "\"keywords\": []," +
                                                "\"securityResource\":  \"\"," +
                                                "\"metadata\": {}," +
                                                "\"menuItems\": []" +
                                            "}," +
                                            "{" +
                                                "\"position\": 3," +
                                                "\"label\": \"Mock Submenu Item Sys 1.1.3\"," +
                                                "\"icon\": \"\"," +
                                                "\"state\": \"\"," +
                                                "\"keywords\": []," +
                                                "\"securityResource\":  \"\"," +
                                                "\"metadata\": {}," +
                                                "\"menuItems\": []" +
                                            "}" +
                                        "]" +
                                    "}" +
                                "]" +
                            "}," +
                            "{" +
                                "\"id\": 2," +
                                "\"name\":  \"Mock System Mod 2\"," +
                                "\"resources\": [" +
                                    "\"Bundles/Mock2-UI.bundle.js\"" +
                                "]," +
                                "\"menuItems\": [" +
                                    "{" +
                                        "\"position\": 1," +
                                        "\"label\": \"Mock Menu Item Sys 2.1\"," +
                                        "\"icon\": \"fa-mock-menu-sys21\"," +
                                        "\"state\": \"mock.state.menu.sys21\"," +
                                        "\"keywords\": [\"mock-keyword-menu-sys21\"]," +
                                        "\"securityResource\":  \"mockSecurityResourceIdSys21\"," +
                                        "\"metadata\": {" +
                                            "\"mockMetadataKeySys21\": \"mockMetadataValueSys21\"," +
                                            "\"mockMetadataKeySys22\": \"mockMetadataValueSys22\"" +
                                        "}," +
                                        "\"menuItems\": [" +
                                            "{" +
                                                "\"position\": 1," +
                                                "\"label\": \"Mock Submenu Item Sys 2.1.1\"," +
                                                "\"icon\": \"fa-mock-submenu-sys211\"," +
                                                "\"state\": \"mock.state.submenu.sys211\"," +
                                                "\"keywords\": [\"mock-keyword-submenu-sys211\"]," +
                                                "\"securityResource\":  \"mockSecurityResourceIdSys211\"," +
                                                "\"metadata\": {" +
                                                    "\"mockMetadataKeySys211\": \"mockMetadataValueSys211\"," +
                                                    "\"mockMetadataKeySys212\": \"mockMetadataValueSys212\"" +
                                                "}," +
                                                "\"menuItems\": []" +
                                            "}," +
                                            "{" +
                                                "\"position\": 2," +
                                                "\"label\": \"Mock Submenu Item Sys 2.1.2\"," +
                                                "\"icon\": \"\"," +
                                                "\"state\": \"\"," +
                                                "\"keywords\": []," +
                                                "\"securityResource\":  \"\"," +
                                                "\"metadata\": {}," +
                                                "\"menuItems\": []" +
                                            "}," +
                                            "{" +
                                                "\"position\": 3," +
                                                "\"label\": \"Mock Submenu Item Sys 2.1.3\"," +
                                                "\"icon\": \"\"," +
                                                "\"state\": \"\"," +
                                                "\"keywords\": []," +
                                                "\"securityResource\":  \"\"," +
                                                "\"metadata\": {}," +
                                                "\"menuItems\": []" +
                                            "}" +
                                        "]" +
                                    "}" +
                                "]" +
                            "}" +
                        "]"
                }
            };

            return systemConfigEntities;
        }

        private static List<ClientConfigurationBulk> GetExportConfigBulkEntites()
        {
            var modelEntities = new List<ClientConfigurationBulk>()
            {
                new ClientConfigurationBulk()
                {
                    Category = "system",
                    Key = "aclaraone.clientsettings",
                    Value ="{\"ClientId\":\"87\",\"Filters\": [{\"ContainerName\":\"Client87\", \"FilterName\":\"amiFilter\",\"OutputFileName\":\"ami_export\",\"FilterQuery\":\"select * from ami where client_id=87\",\"ExportLocation\":\"ExportFile/\"},{\"ContainerName\":\"Client87\",\"FilterName\":\"custFilter\",\"OutputFileName\":\"customer_export\", \"FilterQuery\":\"select * from customer where client_id=87\",\"ExportLocation\":\"/ExportFile\"}]}"
                }
            };

            return modelEntities;

        }

        private static List<ClientConfigurationBulk> GetClientConfigEntities()
        {
            List<ClientConfigurationBulk> clientConfigEntities = new List<ClientConfigurationBulk>()
            {
                new ClientConfigurationBulk()
                {
                    Category = "ClientModuleConfig",
                    Value =
                        "{" +
                            "\"defaultState\": \"mock.default.state\"," +
                            "\"configuredModules\": [" +
                                "{" +
                                    "\"id\": 1," +
                                    "\"position\": 1," +
                                    "\"customMenuItems\": []" +
                                "}," +
                                "{" +
                                    "\"id\": 2," +
                                    "\"position\": 2," +
                                    "\"customMenuItems\":" +
                                    "[" +
                                        "{" +
                                            "\"position\": 1," +
                                            "\"label\": \"Mock2 Custom\"," +
                                            "\"icon\": \"\"," +
                                            "\"state\": \"\"," +
                                            "\"keywords\": []," +
                                            "\"securityResource\":  \"\"," +
                                            "\"metadata\": {}," +
                                            "\"menuItems\": [" +
                                                "{" +
                                                    "\"position\": 1," +
                                                    "\"label\": \"Mock2 Custom1\"," +
                                                    "\"icon\": \"\"," +
                                                    "\"state\": \"\"," +
                                                    "\"keywords\": []," +
                                                    "\"securityResource\": \"\"," +
                                                    "\"metadata\": {}" +
                                                "}," +
                                                "{" +
                                                    "\"position\": 2," +
                                                    "\"label\": \"Mock2 Custom2\"," +
                                                    "\"icon\": \"\"," +
                                                    "\"state\": \"\"," +
                                                    "\"keywords\": []," +
                                                    "\"securityResource\": \"\"," +
                                                    "\"metadata\": {}" +
                                                "}," +
                                                "{" +
                                                    "\"position\": 3," +
                                                    "\"label\": \"Mock2 Custom3\"," +
                                                    "\"icon\": \"\"," +
                                                    "\"state\": \"\"," +
                                                    "\"keywords\": []," +
                                                    "\"securityResource\": \"\"," +
                                                    "\"metadata\": {}" +
                                                "}" +
                                            "]" +
                                        "}" +
                                    "]" +
                                "}" +
                            "]" +
                        "}"
                }
            };

            return clientConfigEntities;
        }

        private static List<ClientConfigurationBulk> GetClientConfigurationBulkForClientSettings()
        {
            var modelEntities = new List<ClientConfigurationBulk>()
            {
                new ClientConfigurationBulk()
                {
                    Category = "system",
                    Key = "aclaraone.clientsettings",
                    Value =
                        "{\r\n \"ClientId\": 87,\r\n \"TimeZone\": \"EST\",\r\n \"IsAmiIntervalStart\": false,\r\n \"IsDstHandled\": false,\r\n \"CalculationScheduleTime\": \"04:00\",\r\n \"NotificationReportScheduleTime\": \"05:00\",\r\n \"InsightReportScheduleTime\": \"06:00\",\r\n \"ForgotPasswordEmailTemplateId\": \"af5c0251-486f-4100-a4cf-2d91a905f108\",\r\n \"RegistrationConfirmationEmailTemplateId\": \"7c0657cd-1020-42a4-a281-87db82a7f6c4\",\r\n \"OptInEmailConfirmationTemplateId\": \"777ca9f8-bb03-490b-946b-b1e6991b2b15\",\r\n \"OptInSmsConfirmationTemplateId\": \"87_12\",\r\n \"OptInEmailUserName\": \"aclaradev\",\r\n \"OptInEmailPassword\": \"Xaclaradev2311X\",\r\n \"OptInEmailFrom\": \"support@aclarax.com\",\r\n \"TrumpiaShortCode\": \"99000\",\r\n \"TrumpiaApiKey\": \"b4c1971153b3509b6ec0d8a24a33454c\",\r\n \"TrumpiaUserName\": \"aclaradev\",\r\n \"TrumpiaContactList\": \"AclaraDemoDev\",\r\n \"UnmaskedAccountIdEndingDigit\" : \"4\",\r\n \"Programs\": [\r\n  {\r\n   \"ProgramName\": \"Program1\",\r\n   \"UtilityProgramName\": \"Program1\",\r\n   \"FileExportRequired\": true,\r\n   \"DoubleOptInRequired\": true,\r\n   \"TrumpiaKeyword\": \"abagdasarian2\",\r\n   \"Insights\": [\r\n    {\r\n     \"InsightName\": \"BillToDate\",\r\n     \"UtilityInsightName\": \"BillToDate\",\r\n     \"AllowedCommunicationChannel\": \"File,Email,SMS,EmailandSMS\",\r\n     \"DefaultCommunicationChannel\": \"Email\",\r\n     \"DefaultFrequency\": \"Daily\",\r\n     \"DefaultValue\": \"\",\r\n     \"EmailTemplateId\": \"c8d5e21f-1150-4b3d-b4b8-ad869d89a040\",\r\n     \"SMSTemplateId\": \"87_1\",\r\n     \"NotifyTime\": \"20:00\",\r\n     \"NotificationDay\": \"Monday\",\r\n     \"TrumpiaKeyword\": \"\",\r\n     \"Level\": \"Account\",\r\n     \"CommodityType\": \"\",\r\n     \"ThresholdType\": \"\",\r\n     \"ThresholdMin\": \"\",\r\n     \"ThresholdMax\": \"\"\r\n    }, {\r\n     \"InsightName\": \"AccountProjectedCost\",\r\n     \"UtilityInsightName\": \"AccountProjectedCost\",\r\n     \"AllowedCommunicationChannel\": \"File,Email,SMS,EmailandSMS\",\r\n     \"DefaultCommunicationChannel\": \"Email\",\r\n     \"DefaultFrequency\": \"Daily\",\r\n     \"DefaultValue\": \"\",\r\n     \"EmailTemplateId\": \"7367b661-0dfd-4eb2-a0d8-0735a459fcb5\",\r\n     \"SMSTemplateId\": \"87_3\",\r\n     \"NotifyTime\": \"20:00\",\r\n     \"NotificationDay\": \"Monday\",\r\n     \"TrumpiaKeyword\": \"\",\r\n     \"Level\": \"Account\",\r\n     \"CommodityType\": \"\",\r\n     \"ThresholdType\": \"\",\r\n     \"ThresholdMin\": \"\",\r\n     \"ThresholdMax\": \"\"\r\n    }, {\r\n     \"InsightName\": \"AccountLevelCostThreshold\",\r\n     \"UtilityInsightName\": \"AccountLevelCostThreshold\",\r\n     \"AllowedCommunicationChannel\": \"File,Email,SMS,EmailandSMS\",\r\n     \"DefaultCommunicationChannel\": \"Email\",\r\n     \"DefaultFrequency\": \"Immediate\",\r\n     \"DefaultValue\": \"100\",\r\n     \"EmailTemplateId\": \"7b3f9981-ae52-4fd9-b223-8ae516f6f82f\",\r\n     \"SMSTemplateId\": \"87_6\",\r\n     \"NotifyTime\": \"\",\r\n     \"NotificationDay\": \"\",\r\n     \"TrumpiaKeyword\": \"\",\r\n     \"Level\": \"Account\",\r\n     \"CommodityType\": \"\",\r\n     \"ThresholdType\": \"Cost\",\r\n     \"ThresholdMin\": \"1\",\r\n     \"ThresholdMax\": \"1000000\"\r\n    }, {\r\n     \"InsightName\": \"ServiceLevelCostThreshold\",\r\n     \"UtilityInsightName\": \"ServiceLevelCostThreshold\",\r\n     \"AllowedCommunicationChannel\": \"File,Email,SMS,EmailandSMS\",\r\n     \"DefaultCommunicationChannel\": \"Email\",\r\n     \"DefaultFrequency\": \"Immediate\",\r\n     \"DefaultValue\": \"25,25\",\r\n     \"EmailTemplateId\": \"87791eaa-0342-45d5-85d4-4645f7a85f7d\",\r\n     \"SMSTemplateId\": \"87_8\",\r\n     \"NotifyTime\": \"\",\r\n     \"NotificationDay\": \"\",\r\n     \"TrumpiaKeyword\": \"\",\r\n     \"Level\": \"Service\",\r\n     \"CommodityType\": \"Gas,Electric\",\r\n     \"ThresholdType\": \"Cost\",\r\n     \"ThresholdMin\": \"1\",\r\n     \"ThresholdMax\": \"1000000\"\r\n    }, {\r\n     \"InsightName\": \"ServiceLevelUsageThreshold\",\r\n     \"UtilityInsightName\": \"ServiceLevelUsageThreshold\",\r\n     \"AllowedCommunicationChannel\": \"File,Email,SMS,EmailandSMS\",\r\n     \"DefaultCommunicationChannel\": \"Email\",\r\n     \"DefaultFrequency\": \"Immediate\",\r\n     \"DefaultValue\": \"25,500\",\r\n     \"EmailTemplateId\": \"f7791cc2-9665-4d4b-8a7f-9421daf55e29\",\r\n     \"SMSTemplateId\": \"87_7\",\r\n     \"NotifyTime\": \"\",\r\n     \"NotificationDay\": \"\",\r\n     \"TrumpiaKeyword\": \"\",\r\n     \"Level\": \"Service\",\r\n     \"CommodityType\": \"Gas,Electric\",\r\n     \"ThresholdType\": \"Usage\",\r\n     \"ThresholdMin\": \"0.1\",\r\n     \"ThresholdMax\": \"1000000\"\r\n    }, {\r\n     \"InsightName\": \"CostToDate\",\r\n     \"UtilityInsightName\": \"CostToDate\",\r\n     \"AllowedCommunicationChannel\": \"File,Email,SMS,EmailandSMS\",\r\n     \"DefaultCommunicationChannel\": \"Email\",\r\n     \"DefaultFrequency\": \"Weekly\",\r\n     \"DefaultValue\": \"\",\r\n     \"EmailTemplateId\": \"b4425b3d-de12-4694-8751-edb41bc5cd79\",\r\n     \"SMSTemplateId\": \"87_2\",\r\n     \"NotifyTime\": \"20:00\",\r\n     \"NotificationDay\": \"Monday\",\r\n     \"TrumpiaKeyword\": \"\",\r\n     \"Level\": \"Service\",\r\n     \"CommodityType\": \"Gas,Electric\",\r\n     \"ThresholdType\": \"\",\r\n     \"ThresholdMin\": \"\",\r\n     \"ThresholdMax\": \"\"\r\n    }, {\r\n     \"InsightName\": \"ServiceProjectedCost\",\r\n     \"UtilityInsightName\": \"ServiceProjectedCost\",\r\n     \"AllowedCommunicationChannel\": \"File,Email,SMS,EmailandSMS\",\r\n     \"DefaultCommunicationChannel\": \"Email\",\r\n     \"DefaultFrequency\": \"Daily\",\r\n     \"DefaultValue\": \"\",\r\n     \"EmailTemplateId\": \"6bd40b25-fc86-480f-a1d4-0f2a22b7d270\",\r\n     \"SMSTemplateId\": \"87_4\",\r\n     \"NotifyTime\": \"20:00\",\r\n     \"NotificationDay\": \"Monday\",\r\n     \"TrumpiaKeyword\": \"\",\r\n     \"Level\": \"Service\",\r\n     \"CommodityType\": \"Gas,Electric\",\r\n     \"ThresholdType\": \"\",\r\n     \"ThresholdMin\": \"\",\r\n     \"ThresholdMax\": \"\"\r\n    }, {\r\n     \"InsightName\": \"Usage\",\r\n     \"UtilityInsightName\": \"Usage\",\r\n     \"AllowedCommunicationChannel\": \"File,Email,SMS,EmailandSMS\",\r\n     \"DefaultCommunicationChannel\": \"Email\",\r\n     \"DefaultFrequency\": \"Daily\",\r\n     \"DefaultValue\": \"\",\r\n     \"EmailTemplateId\": \"3fdb3336-0192-4bb9-bc53-8810dd110a3a\",\r\n     \"SMSTemplateId\": \"87_5\",\r\n     \"NotifyTime\": \"20:00\",\r\n     \"NotificationDay\": \"Monday\",\r\n     \"TrumpiaKeyword\": \"\",\r\n     \"Level\": \"Service\",\r\n     \"CommodityType\": \"Gas,Electric\",\r\n     \"ThresholdType\": \"\",\r\n     \"ThresholdMin\": \"\",\r\n     \"ThresholdMax\": \"\"\r\n    }, {\r\n     \"InsightName\": \"DayThreshold\",\r\n     \"UtilityInsightName\": \"DayThreshold\",\r\n     \"AllowedCommunicationChannel\": \"File,Email,SMS,EmailandSMS\",\r\n     \"DefaultCommunicationChannel\": \"Email\",\r\n     \"DefaultFrequency\": \"Immediate\",\r\n     \"DefaultValue\": \"1,16\",\r\n     \"EmailTemplateId\": \"ce508747-444b-4e1c-9f39-53629e47260b\",\r\n     \"SMSTemplateId\": \"87_9\",\r\n     \"NotifyTime\": \"\",\r\n     \"NotificationDay\": \"\",\r\n     \"TrumpiaKeyword\": \"\",\r\n     \"Level\": \"Service\",\r\n     \"CommodityType\": \"Gas,Electric\",\r\n     \"ThresholdType\": \"Usage\",\r\n     \"ThresholdMin\": \"0.1\",\r\n     \"ThresholdMax\": \"1000000\"\r\n    }, {\r\n     \"InsightName\": \"ServiceLevelTieredThresholdApproaching\",\r\n     \"UtilityInsightName\": \"ServiceLevelTieredThresholdApproaching\",\r\n     \"AllowedCommunicationChannel\": \"File,Email,SMS,EmailandSMS\",\r\n     \"DefaultCommunicationChannel\": \"Email\",\r\n     \"DefaultFrequency\": \"Immediate\",\r\n     \"DefaultValue\": \"20,20\",\r\n     \"EmailTemplateId\": \"4962e788-fb1e-43cf-a2f6-faf713d25e0c\",\r\n     \"SMSTemplateId\": \"87_10\",\r\n     \"NotifyTime\": \"\",\r\n     \"NotificationDay\": \"\",\r\n     \"TrumpiaKeyword\": \"\",\r\n     \"Level\": \"Service\",\r\n     \"CommodityType\": \"Gas,Electric\",\r\n     \"ThresholdType\": \"Percent\",\r\n     \"ThresholdMin\": \"1\",\r\n     \"ThresholdMax\": \"100\"\r\n    }, {\r\n     \"InsightName\": \"ServiceLevelTieredThresholdExceed\",\r\n     \"UtilityInsightName\": \"ServiceLevelTieredThresholdExceed\",\r\n     \"AllowedCommunicationChannel\": \"File,Email,SMS,EmailandSMS\",\r\n     \"DefaultCommunicationChannel\": \"Email\",\r\n     \"DefaultFrequency\": \"Immediate\",\r\n     \"DefaultValue\": \"\",\r\n     \"EmailTemplateId\": \"c1dc511c-0dd7-4208-bad7-8729fa097194\",\r\n     \"SMSTemplateId\": \"87_11\",\r\n     \"NotifyTime\": \"\",\r\n     \"NotificationDay\": \"\",\r\n     \"TrumpiaKeyword\": \"\",\r\n     \"Level\": \"Service\",\r\n     \"CommodityType\": \"Gas,Electric\",\r\n     \"ThresholdType\": \"\",\r\n     \"ThresholdMin\": \"\",\r\n     \"ThresholdMax\": \"\"\r\n    }\r\n   ]\r\n  }, {\r\n   \"ProgramName\": \"Program2\",\r\n   \"UtilityProgramName\": \"Program2\",\r\n   \"FileExportRequired\": false,\r\n   \"DoubleOptInRequired\": true,\r\n   \"TrumpiaKeyword\": \"\",\r\n   \"Insights\": [\r\n    {\r\n     \"InsightName\": \"ServiceLevelCostThreshold\",\r\n     \"UtilityInsightName\": \"ServiceLevelCostThreshold\",\r\n     \"AllowedCommunicationChannel\": \"File,Email,SMS,EmailandSMS\",\r\n     \"DefaultCommunicationChannel\": \"Email\",\r\n     \"DefaultFrequency\": \"Immediate\",\r\n     \"DefaultValue\": \"25\",\r\n     \"EmailTemplateId\": \"87791eaa-0342-45d5-85d4-4645f7a85f7d\",\r\n     \"SMSTemplateId\": \"87_8\",\r\n     \"NotifyTime\": \"\",\r\n     \"NotificationDay\": \"\",\r\n     \"TrumpiaKeyword\": \"\",\r\n     \"Level\": \"Service\",\r\n     \"CommodityType\": \"Water\",\r\n     \"ThresholdType\": \"Cost\",\r\n     \"ThresholdMin\": \"1\",\r\n     \"ThresholdMax\": \"1000000\"\r\n    }, {\r\n     \"InsightName\": \"ServiceLevelUsageThreshold\",\r\n     \"UtilityInsightName\": \"ServiceLevelUsageThreshold\",\r\n     \"AllowedCommunicationChannel\": \"File,Email,SMS,EmailandSMS\",\r\n     \"DefaultCommunicationChannel\": \"Email\",\r\n     \"DefaultFrequency\": \"Immediate\",\r\n     \"DefaultValue\": \"5\",\r\n     \"EmailTemplateId\": \"f7791cc2-9665-4d4b-8a7f-9421daf55e29\",\r\n     \"SMSTemplateId\": \"87_7\",\r\n     \"NotifyTime\": \"\",\r\n     \"NotificationDay\": \"\",\r\n     \"TrumpiaKeyword\": \"\",\r\n     \"Level\": \"Service\",\r\n     \"CommodityType\": \"Water\",\r\n     \"ThresholdType\": \"Usage\",\r\n     \"ThresholdMin\": \"0.1\",\r\n     \"ThresholdMax\": \"1000000\"\r\n    }, {\r\n     \"InsightName\": \"CostToDate\",\r\n     \"UtilityInsightName\": \"CostToDate\",\r\n     \"AllowedCommunicationChannel\": \"File,Email,SMS,EmailandSMS\",\r\n     \"DefaultCommunicationChannel\": \"Email\",\r\n     \"DefaultFrequency\": \"Weekly\",\r\n     \"DefaultValue\": \"\",\r\n     \"EmailTemplateId\": \"b4425b3d-de12-4694-8751-edb41bc5cd79\",\r\n     \"SMSTemplateId\": \"87_2\",\r\n     \"NotifyTime\": \"20:00\",\r\n     \"NotificationDay\": \"Monday\",\r\n     \"TrumpiaKeyword\": \"\",\r\n     \"Level\": \"Service\",\r\n     \"CommodityType\": \"Water\",\r\n     \"ThresholdType\": \"\",\r\n     \"ThresholdMin\": \"\",\r\n     \"ThresholdMax\": \"\"\r\n    }, {\r\n     \"InsightName\": \"ServiceProjectedCost\",\r\n     \"UtilityInsightName\": \"ServiceProjectedCost\",\r\n     \"AllowedCommunicationChannel\": \"File,Email,SMS,EmailandSMS\",\r\n     \"DefaultCommunicationChannel\": \"Email\",\r\n     \"DefaultFrequency\": \"Daily\",\r\n     \"DefaultValue\": \"\",\r\n     \"EmailTemplateId\": \"6bd40b25-fc86-480f-a1d4-0f2a22b7d270\",\r\n     \"SMSTemplateId\": \"87_4\",\r\n     \"NotifyTime\": \"20:00\",\r\n     \"NotificationDay\": \"Monday\",\r\n     \"TrumpiaKeyword\": \"\",\r\n     \"Level\": \"Service\",\r\n     \"CommodityType\": \"Water\",\r\n     \"ThresholdType\": \"\",\r\n     \"ThresholdMin\": \"\",\r\n     \"ThresholdMax\": \"\"\r\n    }, {\r\n     \"InsightName\": \"Usage\",\r\n     \"UtilityInsightName\": \"Usage\",\r\n     \"AllowedCommunicationChannel\": \"File,Email,SMS,EmailandSMS\",\r\n     \"DefaultCommunicationChannel\": \"Email\",\r\n     \"DefaultFrequency\": \"Daily\",\r\n     \"DefaultValue\": \"\",\r\n     \"EmailTemplateId\": \"3fdb3336-0192-4bb9-bc53-8810dd110a3a\",\r\n     \"SMSTemplateId\": \"87_5\",\r\n     \"NotifyTime\": \"20:00\",\r\n     \"NotificationDay\": \"Monday\",\r\n     \"TrumpiaKeyword\": \"\",\r\n     \"Level\": \"Service\",\r\n     \"CommodityType\": \"Water\",\r\n     \"ThresholdType\": \"\",\r\n     \"ThresholdMin\": \"\",\r\n     \"ThresholdMax\": \"\"\r\n    }, {\r\n     \"InsightName\": \"DayThreshold\",\r\n     \"UtilityInsightName\": \"DayThreshold\",\r\n     \"AllowedCommunicationChannel\": \"File,Email,SMS,EmailandSMS\",\r\n     \"DefaultCommunicationChannel\": \"Email\",\r\n     \"DefaultFrequency\": \"Immediate\",\r\n     \"DefaultValue\": \"0.15\",\r\n     \"EmailTemplateId\": \"ce508747-444b-4e1c-9f39-53629e47260b\",\r\n     \"SMSTemplateId\": \"87_9\",\r\n     \"NotifyTime\": \"\",\r\n     \"NotificationDay\": \"\",\r\n     \"TrumpiaKeyword\": \"\",\r\n     \"Level\": \"Service\",\r\n     \"CommodityType\": \"Water\",\r\n     \"ThresholdType\": \"Usage\",\r\n     \"ThresholdMin\": \"0.1\",\r\n     \"ThresholdMax\": \"1000000\"\r\n    }, {\r\n     \"InsightName\": \"ServiceLevelTieredThresholdApproaching\",\r\n     \"UtilityInsightName\": \"ServiceLevelTieredThresholdApproaching\",\r\n     \"AllowedCommunicationChannel\": \"File,Email,SMS,EmailandSMS\",\r\n     \"DefaultCommunicationChannel\": \"Email\",\r\n     \"DefaultFrequency\": \"Immediate\",\r\n     \"DefaultValue\": \"20\",\r\n     \"EmailTemplateId\": \"4962e788-fb1e-43cf-a2f6-faf713d25e0c\",\r\n     \"SMSTemplateId\": \"87_10\",\r\n     \"NotifyTime\": \"\",\r\n     \"NotificationDay\": \"\",\r\n     \"TrumpiaKeyword\": \"\",\r\n     \"Level\": \"Service\",\r\n     \"CommodityType\": \"Water\",\r\n     \"ThresholdType\": \"Percent\",\r\n     \"ThresholdMin\": \"1\",\r\n     \"ThresholdMax\": \"100\"\r\n    }, {\r\n     \"InsightName\": \"ServiceLevelTieredThresholdExceed\",\r\n     \"UtilityInsightName\": \"ServiceLevelTieredThresholdExceed\",\r\n     \"AllowedCommunicationChannel\": \"File,Email,SMS,EmailandSMS\",\r\n     \"DefaultCommunicationChannel\": \"Email\",\r\n     \"DefaultFrequency\": \"Immediate\",\r\n     \"DefaultValue\": \"\",\r\n     \"EmailTemplateId\": \"c1dc511c-0dd7-4208-bad7-8729fa097194\",\r\n     \"SMSTemplateId\": \"87_11\",\r\n     \"NotifyTime\": \"\",\r\n     \"NotificationDay\": \"\",\r\n     \"TrumpiaKeyword\": \"\",\r\n     \"Level\": \"Service\",\r\n     \"CommodityType\": \"Water\",\r\n     \"ThresholdType\": \"\",\r\n     \"ThresholdMin\": \"\",\r\n     \"ThresholdMax\": \"\"\r\n    }\r\n   ]\r\n  }\r\n ]\r\n}"
                }
            };

            return modelEntities;
        }


        private static List<ClientConfigurationBulk> GetClientVeeSettingsEntities()
        {

            var modelEntities = new List<ClientConfigurationBulk>()
            {
                new ClientConfigurationBulk()
                {
                    Category = "system",
                    Key = "aclaraone.clientsettings",
                    Value ="{\r\n \"MissingIntervalCheck\": {\r\n  \"Required\": true,\r\n  \"IntervalThreshold\": 1\r\n },\r\n \"NegativeValueCheck\": {\r\n  \"Required\": true,\r\n  \"IntervalThreshold\": 1\r\n },\r\n \"ZeroCheck\": {\r\n  \"Required\": true,\r\n  \"IntervalThreshold\": 1,\r\n  \"ConsumptionThreshold\":0.5\r\n },\r\n \"StaticCheck\": {\r\n  \"Required\": true,\r\n  \"IntervalThreshold\": 2\r\n },\r\n \"SpikeCheck\": {\r\n  \"Required\": true,\r\n  \"IntervalThreshold\": 1\r\n }\r\n}"
                }
            };

            return modelEntities;


        }

        /// <summary>
        /// Test case for GetDynamicPortalModules() method. Ensure results are deserialized into model.
        /// </summary>
        [TestMethod]
        public void GetDynamicPortalModules()
        {
            var systemEntities = GetSystemConfigBulkEntities();
            var clientEntities = GetClientConfigEntities();

            _contentProviderMock.Setup(t => t.GetPortalModuleSystemConfigurationBulk(It.IsAny<string>(), It.IsAny<string>())).Returns(systemEntities);
            _contentProviderMock.Setup(t => t.GetContentClientConfigurationBulk(It.IsAny<string>(), It.IsAny<string>())).Returns(clientEntities);
            _contentModelFactoryMock.Setup(t => t.CreateContentProvider(It.IsAny<int>())).Returns(_contentProviderMock.Object);

            var result = _clientConfigFacade.GetDynamicPortalModules(5);

            _contentModelFactoryMock.Verify(t => t.CreateContentProvider(It.IsAny<int>()), Times.Once);

            // Verify result comes back correctly.
            Assert.IsInstanceOfType(result, typeof(DynamicAppModuleConfig));
            Assert.AreEqual(result.Modules.Count, 2);

            // Verify system module information is correct.
            Assert.AreEqual(result.Modules[0].Name, "Mock System Mod 1");
            Assert.AreEqual(result.Modules[0].Resources.Length, 1);
            Assert.AreEqual(result.Modules[0].MenuItems.Count, 1);
            Assert.AreEqual(result.Modules[0].MenuItems[0].Label, "Mock Menu Item Sys 1.1");
            Assert.AreEqual(result.Modules[0].MenuItems[0].Metadata.Count, 2);
            Assert.AreEqual(result.Modules[0].MenuItems[0].MenuItems.Count, 3);
            Assert.AreEqual(result.Modules[0].MenuItems[0].MenuItems[0].Label, "Mock Submenu Item Sys 1.1.1");


            // Verify client module information is correct.
            Assert.AreEqual(result.DefaultState, "mock.default.state");

            // Verify client custom menu items are correct.
            Assert.AreEqual(result.Modules[1].MenuItems.Count, 2); // Second module should have additional "Custom" menu item.
            Assert.AreEqual(result.Modules[1].MenuItems[1].MenuItems.Count, 3);
            Assert.AreEqual(result.Modules[1].MenuItems[1].MenuItems[0].Label, "Mock2 Custom1");
        }

        /// <summary>
        /// Test case for GetClientSettings() method. Ensure results are deserialized into model.
        /// </summary>
        [TestMethod]
        public void GetClientSettings()
        {
            var entities = GetClientConfigurationBulkForClientSettings();
            _contentProviderMock.Setup(t => t.GetContentClientConfigurationBulk(It.IsAny<string>(), It.IsAny<string>())).Returns(entities);
            _contentModelFactoryMock.Setup(t => t.CreateContentProvider(It.IsAny<int>())).Returns(_contentProviderMock.Object);
            var result = _clientConfigFacade.GetClientSettings(87);
            _contentModelFactoryMock.Verify(t => t.CreateContentProvider(It.IsAny<int>()), Times.Once);

            Assert.IsInstanceOfType(result, typeof(ClientSettings));
        }

        [TestMethod]
        public void GetClientSettingsExport()
        {
            var entities = GetExportConfigBulkEntites();
            _contentProviderMock.Setup(t => t.GetContentClientConfigurationBulk(It.IsAny<string>(), It.IsAny<string>())).Returns(entities);
            _contentModelFactoryMock.Setup(t => t.CreateContentProvider(It.IsAny<int>())).Returns(_contentProviderMock.Object);
            var result = _clientConfigFacade.GetClientSettingsExport(87, "aclaraone.clientexportsettings");
            _contentModelFactoryMock.Verify(t => t.CreateContentProvider(It.IsAny<int>()), Times.Once);

            Assert.IsInstanceOfType(result, typeof(ClientSettingsExport));
        }


        [TestMethod]
        public void GetClientSettingsExportCheckNUll()
        {
            var entities = GetExportConfigBulkEntites();
            _contentProviderMock.Setup(t => t.GetContentClientConfigurationBulk(It.IsAny<string>(), It.IsAny<string>())).Returns(entities);
            _contentModelFactoryMock.Setup(t => t.CreateContentProvider(It.IsAny<int>())).Returns(_contentProviderMock.Object);
            var result = _clientConfigFacade.GetClientSettingsExport(87, null);
            _contentModelFactoryMock.Verify(t => t.CreateContentProvider(It.IsAny<int>()), Times.Once);

            Assert.IsInstanceOfType(result, typeof(ClientSettingsExport));
        }

        [TestMethod]
        public void GetClientSettingExportFilter()
        {

            var entities = GetExportConfigBulkEntites();
            _contentProviderMock.Setup(t => t.GetContentClientConfigurationBulk(It.IsAny<string>(), It.IsAny<string>())).Returns(entities);
            _contentModelFactoryMock.Setup(t => t.CreateContentProvider(It.IsAny<int>())).Returns(_contentProviderMock.Object);
            var result = _clientConfigFacade.GetClientSettingExportFilter(87, "amiFilter");
            _contentModelFactoryMock.Verify(t => t.CreateContentProvider(It.IsAny<int>()), Times.Once);

            Assert.IsInstanceOfType(result, typeof(Filter));
        }

        
        /// <summary>
        /// Unit test for getting Standard Unit of measure setting from DB
        /// </summary>
        [TestMethod]
        public void GetStandardUom()
        {
            var entities = GetClientConfigurationBulkForStandardUnitOfMeasurements();
            _contentProviderMock.Setup(t => t.GetContentClientConfigurationBulk(It.IsAny<string>(), It.IsAny<string>())).Returns(entities);
            _contentModelFactoryMock.Setup(t => t.CreateContentProvider(It.IsAny<int>())).Returns(_contentProviderMock.Object);
            var result = _clientConfigFacade.GetStandardUom();
            Assert.AreEqual(result.Water, "gal");
        }

        /// <summary>
        /// Get the JSON of Standard UOM
        /// </summary>
        /// <returns></returns>
        private static List<ClientConfigurationBulk> GetClientConfigurationBulkForStandardUnitOfMeasurements()
        {
            var modelEntities = new List<ClientConfigurationBulk>()
            {
                new ClientConfigurationBulk()
                {
                    Category = "system",
                    Key = "aclaraone.clientsettings",
                    Value =
                        "{\"Water\":\"gal\",\"Electric\":\"kwh\",\"Gas\":\"cf\"}"
                }
            };
            return modelEntities;
        }
    }
}
