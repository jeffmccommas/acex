﻿using System;
using System.Reflection;
using AO.BusinessContracts;
using AO.Registrar;
using Microsoft.Practices.Unity;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Enums = CE.AO.Utilities.Enums;

namespace AO.Business.Tests.UnitTests
{
	
	[TestClass]
    // ReSharper disable once InconsistentNaming
    public class UomConversionUnitTest
    {
        private IUomConversion _iUomConversion;
        private IUnityContainer _iUnityContainer;
        /// <summary>
        /// Test Case Init
        /// </summary>
        [TestInitialize]
        public void Init()
        {
            _iUnityContainer = new UnityContainer();
            (new DataStorageRegistrar()).Initialize<TransientLifetimeManager>(_iUnityContainer);
            _iUomConversion = _iUnityContainer.Resolve<IUomConversion>();
        }

        /// <summary>
        /// Test case for inserting ctd Calculations
        /// </summary>
        [TestMethod]
        public void Convert()
        {
            bool conversionFound;
            object ccmToGalConversionFactor = GetInstanceField(typeof(UomConversion), _iUomConversion, "CcmToGalConversionFactor");
            object cfToGalConversionFactor = GetInstanceField(typeof(UomConversion), _iUomConversion, "CfToGalConversionFactor");
            object ccfToGalConversionFactor = GetInstanceField(typeof(UomConversion), _iUomConversion, "CcfToGalConversionFactor");
            object mcfToGalConversionFactor = GetInstanceField(typeof(UomConversion), _iUomConversion, "McfToGalConversionFactor");
            object cmToCfConversionFactor = GetInstanceField(typeof(UomConversion), _iUomConversion, "CmToCfConversionFactor");
            var cmtogalconsumption = _iUomConversion.Convert(Enums.UomType.cm, Enums.UomType.gal, 23, out conversionFound);
            var cmtocfconsumption = _iUomConversion.Convert(Enums.UomType.cm, Enums.UomType.cf, 23, out conversionFound);
            var cftogalconsumption = _iUomConversion.Convert(Enums.UomType.cf, Enums.UomType.gal, 23, out conversionFound);
            var ccftogalconsumption = _iUomConversion.Convert(Enums.UomType.ccf, Enums.UomType.gal, 23, out conversionFound);
            var mcftogalconsumption = _iUomConversion.Convert(Enums.UomType.mcf, Enums.UomType.gal, 23, out conversionFound);
            _iUomConversion.Convert(Enums.UomType.cm, Enums.UomType.ccf, 23, out conversionFound);
            _iUomConversion.Convert(Enums.UomType.cf, Enums.UomType.ccf, 23, out conversionFound);
            _iUomConversion.Convert(Enums.UomType.ccf, Enums.UomType.cm, 23, out conversionFound);
            _iUomConversion.Convert(Enums.UomType.mcf, Enums.UomType.cm, 23, out conversionFound);
            Assert.AreEqual(cmtogalconsumption, 23 * (double)ccmToGalConversionFactor);
            Assert.AreEqual(cmtocfconsumption, 23 * (double)cmToCfConversionFactor);
            Assert.AreEqual(cftogalconsumption, 23 * (double)cfToGalConversionFactor);
            Assert.AreEqual(ccftogalconsumption, 23 * (double)ccfToGalConversionFactor);
            Assert.AreEqual(mcftogalconsumption, 23 * (double)mcfToGalConversionFactor);
            Assert.AreEqual(conversionFound, true);
        }

        /// <summary>
        /// Uses reflection to get the field value from an object.
        /// </summary>
        /// <param name="type">The instance type.</param>
        /// <param name="instance">The instance object.</param>
        /// <param name="fieldName">The field's name which is to be fetched.</param>
        /// <returns>The field value from the object.</returns>
        internal static object GetInstanceField(Type type, object instance, string fieldName)
        {
            BindingFlags bindFlags = BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic
                | BindingFlags.Static;
            FieldInfo field = type.GetField(fieldName, bindFlags);
            return field?.GetValue(instance);
        }
    }
}
