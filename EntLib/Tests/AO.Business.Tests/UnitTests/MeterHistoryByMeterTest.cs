﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using AO.DataAccess.Contracts;
using AO.Entities;
using CE.AO.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace AO.Business.Tests.UnitTests
{
	/// <summary>
	/// Test calss for MeterHistoryByMeter
	/// </summary>
	[TestClass]
	public class MeterHistoryByMeterTest: TestBase
	{
		private MeterHistoryByMeter _meterHistoryByMeter;

		/// <summary>
		/// Test Case Init
		/// </summary>
		[TestInitialize]
		public void Init()
		{
			_meterHistoryByMeter = new MeterHistoryByMeter(LogModelMock.Object,
				new Lazy<ICassandraRepository>(() => CassandraRepositoryMock.Object));
		}
		
		/// <summary>
		/// Test case for InsertOrMergeMeterHistoryByMeterAsync method in MeterHistoryByMeter class
		/// </summary>
		[TestMethod]
		public void InsertOrMergeMeterHistoryByMeterAsync()
		{
			CassandraRepositoryMock.Setup(
				t => t.InsertOrUpdateAsync(It.IsAny<MeterHistoryByMeterEntity>(), It.IsAny<string>()))
				.Returns(Task.FromResult(true));
			var result = _meterHistoryByMeter.InsertOrMergeMeterHistoryByMeterAsync(new MeterHistoryByMeterModel()).Result;

			CassandraRepositoryMock.Verify(t => t.InsertOrUpdateAsync(It.IsAny<MeterHistoryByMeterEntity>(), It.IsAny<string>()), Times.Once);
			Assert.IsTrue(result);
		}

		/// <summary>
		/// Unit Test method for GetMeterHistoryByMeterList
		/// </summary>
		[TestMethod]
		public void GetMeterHistoryByMeterList()
		{
			IList<MeterHistoryByMeterEntity> meterHistoryByMeterList = new List<MeterHistoryByMeterEntity>
			{
				new MeterHistoryByMeterEntity{ ClientId = 87, MeterId = 1},
				new MeterHistoryByMeterEntity{ClientId = 87,MeterId = 2},
				new MeterHistoryByMeterEntity{ClientId = 87,MeterId = 3}
			};

			var pageSize = 3;
			byte[][] token = { null };
			var clientId = 87;

			CassandraRepositoryMock.Setup(
				t => t.Get(It.IsAny<string>(), It.IsAny<int>(), ref token[0], It.IsAny<Expression<Func<MeterHistoryByMeterEntity, bool>>>())).Returns(meterHistoryByMeterList).Verifiable();

			List<MeterHistoryByMeterModel> result = _meterHistoryByMeter.GetMeterHistoryByMeterList(pageSize, ref token[0], clientId).ToList();

			Assert.AreEqual(result.Count, pageSize);
			MockRepository.VerifyAll();
		}
	}
}
