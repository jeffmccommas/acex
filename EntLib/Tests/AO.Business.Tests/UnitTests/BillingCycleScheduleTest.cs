﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq.Expressions;
using System.Threading.Tasks;
using AO.BusinessContracts;
using AO.DataAccess.Contracts;
using AO.Entities;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace AO.Business.Tests.UnitTests
{
    /// <summary>
    /// Contains test methods for BiilingCycleSchedule class
    /// </summary>
    [TestClass]
    public class BillingCycleScheduleTest : TestBase
    {
        private IBillingCycleSchedule _billingCycleScheduleManager;

        /// <summary>
        /// Test Case Init
        /// </summary>
        [TestInitialize]
        public void Init()
        {
            _billingCycleScheduleManager = new BillingCycleSchedule(LogModelMock.Object, new Lazy<ICassandraRepository>(()=>CassandraRepositoryMock.Object));
        }

        /// <summary>
        /// Test case to get Biiling cycle schedule
        /// </summary>
        [TestMethod]
        public void GetBillingCycleScheduleByDateAsync()
        {
            // Arrange
            IList<BillCycleScheduleEntity> billCycleScheduleEntity = new List<BillCycleScheduleEntity>
            {
                new BillCycleScheduleEntity
                {
                    ClientId = "1",
                    BillCycleScheduleId = "1",
                    BillCycleScheduleName = "Q1",
                    BeginDate = DateTime.Now.AddHours(-1).ToString(CultureInfo.CurrentCulture),
                    EndDate = DateTime.Now.AddHours(1).ToString(CultureInfo.CurrentCulture),
                    Description = "Data is for Testing purpose only"
                }
            };

            CassandraRepositoryMock.Setup(t => t.GetAsync(It.IsAny<string>(), It.IsAny<Expression<Func<BillCycleScheduleEntity, bool>>>())).Returns(Task.FromResult(billCycleScheduleEntity)).Verifiable();

            // Act
            var result = _billingCycleScheduleManager.GetBillingCycleScheduleByDateAsync(1, "1", DateTime.Now);

            // Assert
            Assert.AreEqual(result.BillCycleScheduleName, "Q1");
            MockRepository.VerifyAll();
        }
    }
}
