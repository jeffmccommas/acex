﻿using System;
using System.Linq.Expressions;
using System.Threading.Tasks;
using AO.Entities;
using CE.AO.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace AO.Business.Tests.UnitTests
{
    [TestClass]
    public class DcuProvisioningTest : TestBase
    {
        private DcuProvisioning _dcuProvisioning;

        /// <summary>
        /// Test Case Init
        /// </summary>
        [TestInitialize]
        public void Init()
        {
            _dcuProvisioning = new DcuProvisioning(CassandraRepositoryMock.Object);
        }

        /// <summary>
        /// Test case for InsertOrMergeDcuProvisioningAsync method in DcuProvisioning class
        /// </summary>
        [TestMethod]
        public void InsertOrMergeDcuProvisioningAsync()
        {
            // Arrange
            CassandraRepositoryMock.Setup(
                t => t.InsertOrUpdateAsync(It.IsAny<DcuProvisioningEntity>(), It.IsAny<string>()))
                .Returns(Task.FromResult(true));

            // Act
            _dcuProvisioning.InsertOrMergeDcuProvisioningAsync(new DcuProvisioningModel()).Wait();

            // Assert
            CassandraRepositoryMock.Verify(t => t.InsertOrUpdateAsync(It.IsAny<DcuProvisioningEntity>(), It.IsAny<string>()), Times.Once);
            MockRepository.VerifyAll();
        }

        /// <summary>
        /// Test case for DeleteDcuProvisioningAsync method in DcuProvisioning class
        /// </summary>
        [TestMethod]
        public void DeleteDcuProvisioningAsync()
        {
            // Arrange
            CassandraRepositoryMock.Setup(t => t.DeleteAsync(It.IsAny<string>(), It.IsAny<Expression<Func<DcuProvisioningEntity, bool>>>())).Returns(Task.FromResult(true));

            // Act
            _dcuProvisioning.DeleteDcuProvisioningAsync(new DcuProvisioningModel()).Wait();

            // Assert
            CassandraRepositoryMock.Verify(t => t.Delete(It.IsAny<string>(), It.IsAny<Expression<Func<DcuProvisioningEntity, bool>>>()), Times.Never);
            MockRepository.VerifyAll();
        }
    }
}
