﻿using System;
using System.Linq.Expressions;
using System.Threading.Tasks;
using AO.Entities;
using CE.AO.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace AO.Business.Tests.UnitTests
{
    [TestClass]
    public class DcuAlarmTest : TestBase
    {
        private DcuAlarm _dcuAlarm;

        /// <summary>
        /// Test Case Init
        /// </summary>
        [TestInitialize]
        public void Init()
        {
            _dcuAlarm = new DcuAlarm(CassandraRepositoryMock.Object);
        }
        
        /// <summary>
        /// Test case for InsertOrMergeDcuAlarmAsync method in DcuAlarm class
        /// </summary>
        [TestMethod]
        public void InsertOrMergeDcuAlarmAsync()
        {
            // Arrange
            CassandraRepositoryMock.Setup(
                t => t.InsertOrUpdateAsync(It.IsAny<DcuAlarmEntity>(), It.IsAny<string>()))
                .Returns(Task.FromResult(true));

            // Act
            _dcuAlarm.InsertOrMergeDcuAlarmAsync(new DcuAlarmModel()).ConfigureAwait(false);

            // Assert
            CassandraRepositoryMock.Verify(t => t.InsertOrUpdateAsync(It.IsAny<DcuAlarmEntity>(), It.IsAny<string>()), Times.Once);
            MockRepository.VerifyAll();
        }

        /// <summary>
        /// Test case for DeleteDcuAlarmAsync method in DcuAlarm class
        /// </summary>
        [TestMethod]
        public void DeleteDcuAlarmAsync()
        {
            // Arrange
            CassandraRepositoryMock.Setup(t => t.DeleteAsync(It.IsAny<string>(), It.IsAny<Expression<Func<DcuAlarmEntity, bool>>>())).Returns(Task.FromResult(true));

            // Act
            _dcuAlarm.DeleteDcuAlarmAsync(new DcuAlarmModel()).ConfigureAwait(false);

            // Assert
            CassandraRepositoryMock.Verify(t => t.Delete(It.IsAny<string>(), It.IsAny<Expression<Func<DcuAlarmEntity, bool>>>()), Times.Never);
            MockRepository.VerifyAll();
        }
    }
}
