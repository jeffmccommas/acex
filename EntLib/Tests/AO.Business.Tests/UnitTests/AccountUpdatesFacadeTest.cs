﻿using System;
using System.Threading.Tasks;
using AO.BusinessContracts;
using CE.AO.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Enums = CE.AO.Utilities.Enums;

namespace AO.Business.Tests.UnitTests
{
	/// <summary>
	/// Test cases for AccountUpdatesFacade class
	/// </summary>
	[TestClass]
	public class AccountUpdatesFacadeTest : TestBase
	{
		private AccountUpdatesFacade _accountUpdatesFacade;

		private Mock<IAccountUpdates> _accountUpdates;
		private Mock<IBilling> _billing;
		private Mock<ICustomer> _customer;
		private Mock<IAccountLookup> _accountLookup;
		private Mock<ICustomerMobileNumberLookup> _customerMobileNumberLookup;
		/// <summary>
		/// Test Case Init
		/// </summary>
		[TestInitialize]
		public void Init()
		{
			_accountUpdates = new Mock<IAccountUpdates>();
			_billing = new Mock<IBilling>();
			_customer = new Mock<ICustomer>();
			_accountLookup = new Mock<IAccountLookup>();
			_customerMobileNumberLookup = new Mock<ICustomerMobileNumberLookup>();

			_accountUpdatesFacade = new AccountUpdatesFacade(LogModelMock.Object,
				new Lazy<IAccountUpdates>(() => _accountUpdates.Object), new Lazy<IBilling>(() => _billing.Object),
				new Lazy<ICustomer>(() => _customer.Object),
				new Lazy<ICustomerMobileNumberLookup>(() => _customerMobileNumberLookup.Object),
				new Lazy<IAccountLookup>(() => _accountLookup.Object));
		}

		/// <summary>
		/// Test Case for accessing Log Model
		/// </summary>
		[TestMethod]
		public void AccessLogModel()
		{
			_accountUpdatesFacade.LogModel.DisableLog = true;
			Assert.AreEqual(true, _accountUpdatesFacade.LogModel.DisableLog);
		}

		/// <summary>
		/// Test case for insert of AccountUpdateModel model
		/// Test case for InsertOrMergeAccountUpdateAsync method in AccountUpdatesFacade class
		/// </summary>
		[TestMethod]
		public void InsertOrMergeAccountUpdateAsync()
		{
			var accountUpdatesModel = new AccountUpdatesModel
			{
				AccountId = "",
				ClientId = 87,
				CustomerId = "",
				IsLatest = true,
				MeterId = "METER_QA_02",
				NewValue = "",
				OldValue = "",
				PremiseId = "",
				ServiceContractId = "",
				UpdateType = Enums.AccountUpdateType.RateClass.ToString()
			};
			_accountUpdates.Setup(
				t => t.InsertOrMergeAccountUpdateAsync(It.IsAny<AccountUpdatesModel>(), It.IsAny<bool>()))
				.Returns(Task.FromResult(true)).Verifiable();
			_accountUpdates.Setup(t => t.DeleteHistoryAccountUpdateAsyc(It.IsAny<int>(),It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<bool>())).Returns(Task.FromResult(true)).Verifiable();

			var billingModel = new BillingModel();

			_billing.Setup(
				t =>
					t.GetServiceDetailsFromBillAsync(It.IsAny<int>(), It.IsAny<string>(), It.IsAny<string>(),
						It.IsAny<string>())).Returns(Task.FromResult(billingModel)).Verifiable();

			 _billing.Setup(t => t.InsertOrMergeLatestBillAsync(It.IsAny<BillingModel>())).Returns(Task.FromResult(true)).Verifiable();

			// Same for phone1 and email
			var customerModel = new CustomerModel();
			_customer.Setup(t => t.GetCustomerAsync(It.IsAny<int>(), It.IsAny<string>())).Returns(Task.FromResult(customerModel)).Verifiable();

			_customer.Setup(t => t.InsertOrMergeCustomerAsync(It.IsAny<CustomerModel>())).Returns(Task.FromResult(true)).Verifiable();

			// CustomerMobileNumberLookup
			_customerMobileNumberLookup.Setup(t => t.DeleteCustomerMobileNumberLookupAsync(It.IsAny<int>(),It.IsAny<string>(),It.IsAny<string>())).Returns(Task.FromResult(true)).Verifiable();
			_customerMobileNumberLookup.Setup(t => t.InsertOrMergeCustomerMobileNumberLookupAsync(It.IsAny<CustomerMobileNumberLookupModel>())).Returns(Task.FromResult(true)).Verifiable();
			_customerMobileNumberLookup.Setup(t => t.GetCustomerMobileNumberLookup(It.IsAny<int>(), It.IsAny<string>(), It.IsAny<string>())).Returns(new CustomerMobileNumberLookupModel()).Verifiable();

			//AccountLookup
			var accountLookupModel = new AccountLookupModel();

			_accountLookup.Setup(t => t.GetAccountMeterDetails(It.IsAny<int>(),It.IsAny<string>(),It.IsAny<string>())).Returns(Task.FromResult(accountLookupModel)).Verifiable();
			_accountLookup.Setup(t => t.InsertOrMergeAccountLookupAsync(It.IsAny<AccountLookupModel>()))
				.Returns(Task.FromResult(true)).Verifiable();

			var result = _accountUpdatesFacade.InsertOrMergeAccountUpdateAsync(accountUpdatesModel).Result;

			// For Email updation
			accountUpdatesModel.UpdateType = Enums.AccountUpdateType.Email.ToString();
			result = result && _accountUpdatesFacade.InsertOrMergeAccountUpdateAsync(accountUpdatesModel).Result;

			// For Phone updation
			accountUpdatesModel.UpdateType = Enums.AccountUpdateType.Phone1.ToString();
			result = result && _accountUpdatesFacade.InsertOrMergeAccountUpdateAsync(accountUpdatesModel).Result;

			// For MeterReplacement
			accountUpdatesModel.UpdateType = Enums.AccountUpdateType.MeterReplacement.ToString();
			result = result && _accountUpdatesFacade.InsertOrMergeAccountUpdateAsync(accountUpdatesModel).Result;

			// 2 times in each call of InsertOrMergeAccountUpdateAsync
			_accountUpdates.Verify(t => t.InsertOrMergeAccountUpdateAsync(It.IsAny<AccountUpdatesModel>(), It.IsAny<bool>()),Times.AtMost(8));
			_accountUpdates.Verify(t => t.DeleteHistoryAccountUpdateAsyc(It.IsAny<int>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<bool>()), Times.AtMost(4));
			_accountUpdates.VerifyAll();

			_billing.Verify(t =>t.GetServiceDetailsFromBillAsync(It.IsAny<int>(), It.IsAny<string>(), It.IsAny<string>(),It.IsAny<string>()), Times.AtMost(2));
			_billing.Verify(t => t.InsertOrMergeLatestBillAsync(It.IsAny<BillingModel>()), Times.AtMost(2));
			_billing.VerifyAll();

			_customer.Verify(t => t.GetCustomerAsync(It.IsAny<int>(), It.IsAny<string>()), Times.AtMost(2));
			_customer.Verify(t => t.InsertOrMergeCustomerAsync(It.IsAny<CustomerModel>()), Times.AtMost(2));
			_customer.VerifyAll();

			_customerMobileNumberLookup.Verify(t => t.DeleteCustomerMobileNumberLookupAsync(It.IsAny<int>(),It.IsAny<string>(),It.IsAny<string>()),Times.Once);
			_customerMobileNumberLookup.Verify(t => t.GetCustomerMobileNumberLookup(It.IsAny<int>(), It.IsAny<string>(), It.IsAny<string>()), Times.Once);
			_customerMobileNumberLookup.Verify(t => t.InsertOrMergeCustomerMobileNumberLookupAsync(It.IsAny<CustomerMobileNumberLookupModel>()),Times.Once);
			_customerMobileNumberLookup.VerifyAll();

			_accountLookup.Verify(t => t.GetAccountMeterDetails(It.IsAny<int>(), It.IsAny<string>(), It.IsAny<string>()), Times.AtMost(1));
			_accountLookup.Verify(t => t.InsertOrMergeAccountLookupAsync(It.IsAny<AccountLookupModel>()), Times.AtMost(2));
			_accountLookup.VerifyAll();

			Assert.AreEqual(result,true);
		}
	}
}