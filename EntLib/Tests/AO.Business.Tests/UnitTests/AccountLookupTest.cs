﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Threading.Tasks;
using AO.BusinessContracts;
using AO.DataAccess.Contracts;
using AO.Entities;
using AutoMapper;
using CE.AO.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace AO.Business.Tests.UnitTests
{
    /// <summary>
    /// Contains test methods for AccountLookup Class
    /// </summary>
    [TestClass]
    public class AccountLookupTest : TestBase
    {
        private IAccountLookup _accountLookupManager;

        /// <summary>
        /// Test Case Init
        /// </summary>
        [TestInitialize]
        public void Init()
        {          
            _accountLookupManager = new AccountLookup(LogModelMock.Object, new Lazy<ICassandraRepository>(()=>CassandraRepositoryMock.Object));
        }

        /// <summary>
        /// Test Case for accessing Log Model
        /// </summary>
        [TestMethod]
        public void AccessLogModel()
        {
            _accountLookupManager.LogModel.DisableLog = true;
            Assert.AreEqual(true, _accountLookupManager.LogModel.DisableLog);
        }

        /// <summary>
        /// Test Case to insert client meter
        /// </summary>
        [TestMethod]
        public void InsertOrMergeAccountLookupAsync()
        {            

            var accountLookupModel = new AccountLookupModel
            {
                ClientId = 87,
                AccountId = "a5",
                MeterId = "",
                IsActive = true,
                ServiceContractId = "SC007",
                CustomerId = "1012",
                IsServiceContractCreatedBySystem = true,
                AccountMeterTimeStamp = DateTime.Now
            };

            CassandraRepositoryMock.Setup(t => t.InsertOrUpdateAsync(It.IsAny<ClientMeterEntity>(), It.IsAny<string>()))
                .Returns(Task.FromResult(true))
                .Verifiable();           

            var result = _accountLookupManager.InsertOrMergeAccountLookupAsync(accountLookupModel).Result;

            CassandraRepositoryMock.Verify(
                t => t.InsertOrUpdateAsync(It.IsAny<ClientMeterEntity>(), It.IsAny<string>()), Times.Once);
            CassandraRepositoryMock.VerifyAll();
            Assert.AreEqual(true, result);
        }

        /// <summary>       
        /// Test case to get account meter details based on ClientId and MeterId
        /// </summary>
        [TestMethod]
        public void GetAccountMeterDetails()
        {
            var clientMeterEntity = new ClientMeterEntity()
            {
                ClientId = 1,
                AccountId = "A0001",
                MeterId = "M0001",
                IsActive = true,
                ServiceContractId = "SC001",
                CustomerId = "132323",
                IsServiceContractCreatedBySystem = false
            };

            IList<ClientMeterEntity> list = new List<ClientMeterEntity>();
            list.Add(clientMeterEntity);

            CassandraRepositoryMock.Setup(
                t => t.Get(It.IsAny<string>(), It.IsAny<Expression<Func<ClientMeterEntity, bool>>>()))
                .Returns(list)
                .Verifiable();          

            var result = _accountLookupManager.GetAccountMeterDetails(1, "M0001");

            CassandraRepositoryMock.Verify(
                t => t.Get(It.IsAny<string>(), It.IsAny<Expression<Func<ClientMeterEntity, bool>>>()), Times.Exactly(1));
            CassandraRepositoryMock.VerifyAll();



            var accountLookupModel = Mapper.Map<ClientMeterEntity, AccountLookupModel>(clientMeterEntity);
            // Assert.AreEqual(accountLookupModel, result);            
            PropertyValuesAreEquals(result[0], accountLookupModel);

        }

        /// <summary>       
        /// Test case to get account meter details based on ClientId and MeterId for shared account
        /// </summary>
        [TestMethod]
        public void GetAccountMeterDetailsForSharedAccount()
        {
            var clientId = 87;
            var accountId = "SA001";
            var meterId = "SAM001";
            var serviceId = "SASC001";
            var clientMeterEntity1 = new ClientMeterEntity()
            {
                ClientId = clientId,
                AccountId = accountId,
                MeterId = meterId,
                IsActive = true,
                ServiceContractId = serviceId,
                CustomerId = "SAC001",
                IsServiceContractCreatedBySystem = false
            };

            var clientMeterEntity2 = new ClientMeterEntity()
            {
                ClientId = clientId,
                AccountId = accountId,
                MeterId = meterId,
                IsActive = true,
                ServiceContractId = serviceId,
                CustomerId = "SAC002",
                IsServiceContractCreatedBySystem = false
            };

            var clientMeterEntity3 = new ClientMeterEntity()
            {
                ClientId = clientId,
                AccountId = accountId,
                MeterId = meterId,
                IsActive = false,
                ServiceContractId = serviceId,
                CustomerId = "SAC003",
                IsServiceContractCreatedBySystem = false
            };
            var clientMeterEntity4 = new ClientMeterEntity()
            {
                ClientId = clientId,
                AccountId = "SA002",
                MeterId = meterId,
                IsActive = true,
                ServiceContractId = serviceId,
                CustomerId = "SAC004",
                IsServiceContractCreatedBySystem = false
            };

            IList<ClientMeterEntity> list = new List<ClientMeterEntity>();
            list.Add(clientMeterEntity1);
            list.Add(clientMeterEntity2);
            list.Add(clientMeterEntity3);
            list.Add(clientMeterEntity4);

            var expectedResult= new List<ClientMeterEntity>();
            expectedResult.Add(clientMeterEntity1);
            expectedResult.Add(clientMeterEntity2);
            expectedResult.Add(clientMeterEntity4);

            CassandraRepositoryMock.Setup(
                t => t.Get(It.IsAny<string>(), It.IsAny<Expression<Func<ClientMeterEntity, bool>>>()))
                .Returns(list)
                .Verifiable();

            var result = _accountLookupManager.GetAccountMeterDetails(1, "M0001");

            CassandraRepositoryMock.Verify(
                t => t.Get(It.IsAny<string>(), It.IsAny<Expression<Func<ClientMeterEntity, bool>>>()), Times.Exactly(1));
            CassandraRepositoryMock.VerifyAll();



            var expectedAccountLookupModels = Mapper.Map<List<ClientMeterEntity>, List<AccountLookupModel>>(expectedResult);
            // Assert.AreEqual(accountLookupModel, result);    
            Assert.IsNotNull(result);
            Assert.AreEqual(3, result.Count);
            var sortedResult = result.OrderBy(r => r.CustomerId);
            var sortedExpectedResult = expectedAccountLookupModels.OrderBy(e => e.CustomerId);
            PropertyValuesAreEquals(sortedResult, sortedExpectedResult);
        }

        /// <summary>       
        /// Test case to get account meter details based on ClientId, MeterId and CustomerId
        /// </summary>
        [TestMethod]
        public void GetAccountMeterDetailsByCustomerId()
        {

            var clientMeterEntity = new ClientMeterEntity()
            {
                ClientId = 1,
                AccountId = "A0001",
                MeterId = "M0001",
                IsActive = true,
                ServiceContractId = "SC001",
                CustomerId = "132323",
                IsServiceContractCreatedBySystem = false
            };

            CassandraRepositoryMock.Setup(
                t => t.GetSingleAsync(It.IsAny<string>(), It.IsAny<Expression<Func<ClientMeterEntity, bool>>>()))
                .Returns(Task.FromResult(clientMeterEntity))
                .Verifiable();
         
            Task<AccountLookupModel> result = _accountLookupManager.GetAccountMeterDetails(1, "M0001", "132323");
            CassandraRepositoryMock.Verify(
                t => t.GetSingleAsync(It.IsAny<string>(), It.IsAny<Expression<Func<ClientMeterEntity, bool>>>()),
                Times.Exactly(1));
            CassandraRepositoryMock.VerifyAll();
            var accountLookupModel = Mapper.Map<ClientMeterEntity, AccountLookupModel>(clientMeterEntity);
            //Assert.AreEqual(accountLookupModel,result.Result);
            PropertyValuesAreEquals(result.Result,accountLookupModel);

        }

        public static bool PropertyValuesAreEquals(object actual, object expected)
        {
            var isAssert = true;
            PropertyInfo[] properties = expected.GetType().GetProperties();
            foreach (PropertyInfo property in properties)
            {
                var expectedValue = property.GetValue(expected, null);
                var actualValue = property.GetValue(actual, null);

                if (!Equals(expectedValue, actualValue))
                { isAssert = false;}

            }
            return isAssert;
        }
    }
}
