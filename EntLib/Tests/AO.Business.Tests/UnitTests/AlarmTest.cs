﻿using System;
using System.Linq.Expressions;
using System.Threading.Tasks;
using AO.DataAccess.Contracts;
using AO.Entities;
using CE.AO.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace AO.Business.Tests.UnitTests
{
    /// <summary>
    /// Test Class for Alarm
    /// </summary>
    [TestClass]
    public class AlarmTest : TestBase
    {
        private Alarm _alarm;

        /// <summary>
        /// Test Case Init
        /// </summary>
        [TestInitialize]
        public void Init()
        {
            _alarm = new Alarm(new Lazy<ICassandraRepository>(()=>CassandraRepositoryMock.Object));
        }

        /// <summary>
        /// Test case for InsertOrMergeAlarmAsync method in Alarm class
        /// </summary>
        [TestMethod]
        public void InsertOrMergeAlarmAsync()
        {
            // Arrange
            CassandraRepositoryMock.Setup(
                t => t.InsertOrUpdateAsync(It.IsAny<AlarmEntity>(), It.IsAny<string>()))
                .Returns(Task.FromResult(true));

            // Act
            var result = _alarm.InsertOrMergeAlarmAsync(new AlarmModel()).Result;

            // Assert
            CassandraRepositoryMock.Verify(t => t.InsertOrUpdateAsync(It.IsAny<AlarmEntity>(), It.IsAny<string>()), Times.Once);
            Assert.IsTrue(result);
            MockRepository.VerifyAll();
        }

        /// <summary>
        /// Test case for DeleteAlarmAsync method in Alarm class
        /// </summary>
        [TestMethod]
        public void DeleteAlarmAsync()
        {
            // Arrange
            CassandraRepositoryMock.Setup(t => t.DeleteAsync(It.IsAny<string>(), It.IsAny<Expression<Func<AlarmEntity, bool>>>())).Returns(Task.FromResult(true));

            // Act
            var result = _alarm.DeleteAlarmAsync(new AlarmModel()).Result;

            // Assert
            CassandraRepositoryMock.Verify(t => t.Delete(It.IsAny<string>(), It.IsAny<Expression<Func<AlarmEntity, bool>>>()), Times.Never);
            Assert.IsTrue(result);
            MockRepository.VerifyAll();
        }

    }
}
