﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using AO.BusinessContracts;
using AO.DataAccess.Contracts;
using AO.Entities;
using CE.AO.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace AO.Business.Tests.UnitTests
{
    /// <summary>
    /// Test case class for CassandraBusiness.Billing
    /// </summary>
    [TestClass]
    public class BillingTest : TestBase
    {
        private IBilling _billingManager;

        /// <summary>
        /// Test Case Init
        /// </summary>
        [TestInitialize]
        public void Init()
        {
            _billingManager = new Billing(LogModelMock.Object, new Lazy<ICassandraRepository>(()=>CassandraRepositoryMock.Object));
        }

        /// <summary>
        /// Test Case for accessing Log Model
        /// </summary>
        [TestMethod]
        public void AccessLogModel()
        {
            _billingManager.LogModel.DisableLog = true;
            Assert.AreEqual(true, _billingManager.LogModel.DisableLog);
        }

        /// <summary>
        /// Test case to fetch all services from last bill
        /// </summary>
        [TestMethod]
        public void GetAllServicesFromLastBill()
        {
            var billingEntity = GetBillingEntity();
            IList<BillingEntity> list = new List<BillingEntity>();
            list.Add(billingEntity);

            CassandraRepositoryMock.Setup(
                t =>
                    t.Get(It.IsAny<string>(), It.IsAny<Expression<Func<BillingEntity, bool>>>()))
                .Returns(list)
                .Verifiable();
            
            List<BillingModel> result = _billingManager.GetAllServicesFromLastBill(87, "C05", "Accn05").ToList();
            CassandraRepositoryMock.Verify(t => t.Get(It.IsAny<string>(), It.IsAny<Expression<Func<BillingEntity, bool>>>()), Times.Once);
            CassandraRepositoryMock.VerifyAll();
            Assert.AreEqual(result.Count, 1);
        }

        /// <summary>
        /// Test case to fetch all bills for service, latest or hisroty 
        /// </summary>
        [TestMethod]
        public void GetBillsForService()
        {
            var billingEntity = GetBillingEntity();
            IList<BillingEntity> list = new List<BillingEntity>();
            list.Add(billingEntity);

            CassandraRepositoryMock.Setup(
                t =>
                    t.Get(It.IsAny<string>(), It.IsAny<Expression<Func<BillingEntity, bool>>>()))
                .Returns(list)
                .Verifiable();
            
            //latest or history
            List<BillingModel> result = _billingManager.GetBillsForService(87, "C05", "Accn05", "SC05", DateTime.Now.AddDays(-1), DateTime.Now).ToList();
            CassandraRepositoryMock.Verify(t => t.Get(It.IsAny<string>(), It.IsAny<Expression<Func<BillingEntity, bool>>>()), Times.Once);
            CassandraRepositoryMock.VerifyAll();
            Assert.AreEqual(result.Count, 1);
        }

        /// <summary>
        /// Test case to fetch all history bills for service  
        /// </summary>
        [TestMethod]
        public void GetBillsForServiceHistory()
        {
            var billingEntity = GetBillingEntity();
            IList<BillingEntity> list = new List<BillingEntity>();
            list.Add(billingEntity);

            CassandraRepositoryMock.Setup(
                t =>
                    t.Get(It.IsAny<string>(), It.IsAny<Expression<Func<BillingEntity, bool>>>()))
                .Returns(list)
                .Verifiable();
            
            List<BillingModel> billingResult = _billingManager.GetBillsForService(87, "C05", "Accn05", "SC05", DateTime.Now.AddDays(-1), DateTime.Now).ToList();
            CassandraRepositoryMock.Verify(t => t.Get(It.IsAny<string>(), It.IsAny<Expression<Func<BillingEntity, bool>>>()), Times.Once);
            CassandraRepositoryMock.VerifyAll();
            Assert.AreEqual(billingResult.Count, 1);
        }

        /// <summary>
        /// Test case to fetch all service details from bills
        /// </summary>
        [TestMethod]
        public void GetServiceDetailsFromBillAsync()
        {
            var billingEntity = GetBillingEntity();
            CassandraRepositoryMock.Setup(
                t =>
                    t.GetSingle(It.IsAny<string>(), It.IsAny<Expression<Func<BillingEntity, bool>>>()))
                .Returns(billingEntity)
                .Verifiable();
            
            var result = _billingManager.GetServiceDetailsFromBillAsync(87, "C05", "Accn05", "SC05").Result;
            CassandraRepositoryMock.Verify(t => t.GetSingle(It.IsAny<string>(), It.IsAny<Expression<Func<BillingEntity, bool>>>()), Times.Once);
            CassandraRepositoryMock.VerifyAll();
            Assert.AreEqual(result.IsLatest, true);
        }

        /// <summary>
        /// Test case for inserting latest bill
        /// </summary>
        [TestMethod]
        public void InsertOrMergeLatestBillAsync()
        {
            var billingModel = GetBillingModel();
            CassandraRepositoryMock.Setup(t => t.InsertOrUpdate(It.IsAny<BillingEntity>(), It.IsAny<string>())).Returns(true).Verifiable();
            
            var result = _billingManager.InsertOrMergeLatestBillAsync(billingModel).Result;
            CassandraRepositoryMock.Verify(t => t.InsertOrUpdate(It.IsAny<BillingEntity>(), It.IsAny<string>()), Times.Once);
            CassandraRepositoryMock.VerifyAll();
            Assert.AreEqual(true, result);
        }

        /// <summary>
        /// Test case for inserting latest bill
        /// </summary>
        [TestMethod]
        public void InsertOrMergeHistoryBillAsync()
        {
            var billingModel = GetBillingModel();
            CassandraRepositoryMock.Setup(t => t.InsertOrUpdate(It.IsAny<BillingEntity>(), It.IsAny<string>())).Returns(true).Verifiable();
            
            var result = _billingManager.InsertOrMergeHistoryBillAsync(billingModel).Result;
            CassandraRepositoryMock.Verify(t => t.InsertOrUpdate(It.IsAny<BillingEntity>(), It.IsAny<string>()), Times.Once);
            CassandraRepositoryMock.VerifyAll();
            Assert.AreEqual(true, result);
        }

        /// <summary>
        /// Test case for deleting latest bill
        /// </summary>
        [TestMethod]
        public void DeleteLatestServiceDetailsAsync()
        {
            var billingModel = GetBillingModel();
            CassandraRepositoryMock.Setup(t => t.InsertOrUpdate(It.IsAny<BillingEntity>(), It.IsAny<string>())).Returns(true).Verifiable();
            // ReSharper disable once UnusedVariable
            var result = _billingManager.InsertOrMergeLatestBillAsync(billingModel).Result;

            CassandraRepositoryMock.Setup(t => t.DeleteAsync(It.IsAny<string>(), It.IsAny<Expression<Func<BillingEntity, bool>>>())).Returns(Task.FromResult(true)).Verifiable();
            
            var billingResult = _billingManager.DeleteLatestServiceDetailsAsync(87, "C05", "Accn05", "SC05").Result;
            CassandraRepositoryMock.Verify(t => t.DeleteAsync(It.IsAny<string>(), It.IsAny<Expression<Func<BillingEntity, bool>>>()), Times.Once);
            CassandraRepositoryMock.VerifyAll();
            Assert.AreEqual(true, billingResult);
        }

        /// <summary>
        /// Test case for deleting history bill
        /// </summary>
        [TestMethod]
        public void DeleteHistoryServiceDetailsAsync()
        {
            var billingModel = GetBillingModel();
            CassandraRepositoryMock.Setup(t => t.InsertOrUpdate(It.IsAny<BillingEntity>(), It.IsAny<string>())).Returns(true).Verifiable();
            // ReSharper disable once UnusedVariable
            var result = _billingManager.InsertOrMergeLatestBillAsync(billingModel).Result;

            CassandraRepositoryMock.Setup(t => t.DeleteAsync(It.IsAny<string>(), It.IsAny<Expression<Func<BillingEntity, bool>>>())).Returns(Task.FromResult(true)).Verifiable();
            
            var billingResult = _billingManager.DeleteHistoryServiceDetailsAsync(87, "C05", "Accn05", "SC05", DateTime.Now).Result;
            CassandraRepositoryMock.Verify(t => t.DeleteAsync(It.IsAny<string>(), It.IsAny<Expression<Func<BillingEntity, bool>>>()), Times.Once);
            CassandraRepositoryMock.VerifyAll();
            Assert.AreEqual(true, billingResult);
        }

        /// <summary>
        /// Create and returns new BillingModel
        /// </summary>
        /// <returns>BillingModel</returns>
        private static BillingModel GetBillingModel()
        {
            return new BillingModel
            {
                ClientId = 87,
                CustomerId = "C05",
                AccountId = "Accn05",
                PremiseId = "P05",
                CommodityId = 3,
                MeterId = "M05",
                ServiceContractId = "SC05",
                StartDate = DateTime.Now.AddDays(-1),
                EndDate = DateTime.Now,
                TotalCost = 2093,
                BillDays = 30,
                Source = "Utility",
                IsLatest = true
            };
        }

        /// <summary>
        /// Create and return new BillingEntity
        /// </summary>
        /// <returns>BillingEntity</returns>
        private static BillingEntity GetBillingEntity()
        {
            return new BillingEntity
            {
                ClientId = 87,
                CustomerId = "C05",
                AccountId = "Accn05",
                PremiseId = "P05",
                CommodityId = 3,
                MeterId = "M05",
                ServiceContractId = "SC05",
                StartDate = DateTime.Now.AddDays(-1),
                EndDate = DateTime.Now,
                TotalCost = 2093,
                BillDays = 30,
                Source = "Utility",
                IsLatest = true
            };
        }
    }
}
