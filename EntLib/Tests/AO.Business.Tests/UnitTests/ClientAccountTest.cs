﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using AO.DataAccess.Contracts;
using AO.Entities;
using CE.AO.Models;
using CE.AO.Utilities;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

// ReSharper disable once RedundantUsingDirective

namespace AO.Business.Tests.UnitTests
{
    [TestClass]
    public class ClientAccountTest : TestBase
    {
        private ClientAccount _clientAccount;
        private Mock<IQueueManager> _queueManagerMock;

        /// <summary>
        /// Test Case Init
        /// </summary>
        [TestInitialize]
        public void Init()
        {
            _clientAccount = new ClientAccount(LogModelMock.Object, new Lazy<ICassandraRepository>(()=>CassandraRepositoryMock.Object));
        }

        /// <summary>
        /// Test case for InsertOrMergeClientAccount method in ClientAccount class
        /// </summary>
        [TestMethod]
        public void InsertOrMergeClientAccount()
        {
            // Arrange
            CassandraRepositoryMock.Setup(
                t =>
                    t.InsertOrUpdateAsync(It.Is<ClientAccountEntity>(c => c.LastModifiedDate > DateTime.MinValue),
                        It.IsAny<string>())).Returns(Task.FromResult(typeof (void)));

            // Act
            var result = _clientAccount.InsertOrMergeClientAccount(new ClientAccountModel()).Result;

            // Assert
            CassandraRepositoryMock.Verify(t => t.InsertOrUpdateAsync(It.IsAny<ClientAccountEntity>(), It.IsAny<string>()),Times.Once);
            Assert.IsTrue(result);
            MockRepository.VerifyAll();
        }

        /// <summary>
        /// Test case for GetClientAccounts methods from ClientAccount class
        /// </summary>
        [TestMethod]
        public void GetClientAccounts()
        {
            IList<ClientAccountEntity> listClientAccountEntities = new List<ClientAccountEntity>
            {
                new ClientAccountEntity
                {
                    ClientId = 87,
                    AccountId = "",
                    CustomerId = ""
                }
            };

            CassandraRepositoryMock.Setup(
                t => t.Get(It.IsAny<string>(), It.IsAny<Expression<Func<ClientAccountEntity, bool>>>()))
                .Returns(listClientAccountEntities);
            byte[] token = null;
            CassandraRepositoryMock.Setup(
            //    // ReSharper disable once AccessToModifiedClosure
                t => t.Get(It.IsAny<string>(), It.IsAny<int>(),ref token, It.IsAny<Expression<Func<ClientAccountEntity, bool>>>())).Returns(listClientAccountEntities);

            IEnumerable<ClientAccountModel> result = _clientAccount.GetClientAccounts(It.IsAny<int>());
            IEnumerable<ClientAccountModel> result1 = _clientAccount.GetClientAccounts(It.IsAny<int>(),It.IsAny<int>(),ref token);

            CassandraRepositoryMock.Verify(t => t.Get(It.IsAny<string>(), It.IsAny<Expression<Func<ClientAccountEntity, bool>>>()),Times.Once);
            CassandraRepositoryMock.Verify(t => t.Get(It.IsAny<string>(), It.IsAny<int>(), ref token, It.IsAny<Expression<Func<ClientAccountEntity, bool>>>()),Times.AtLeastOnce);
            CassandraRepositoryMock.VerifyAll();

            IEnumerable<ClientAccountModel> clientAccountModels = result as IList<ClientAccountModel> ?? result.ToList();
            IEnumerable<ClientAccountModel> accountModels = result1 as IList<ClientAccountModel> ?? result1.ToList();

            Assert.AreEqual(clientAccountModels.Count(),1);
            Assert.AreEqual(clientAccountModels.First().ClientId,87);
            Assert.AreEqual(accountModels.Count(), 1);
            Assert.AreEqual(accountModels.First().ClientId, 87);
            MockRepository.VerifyAll();
        }

        /// <summary>
        /// Test case for SendScheduleMsssage method from ClientAccount class
        /// </summary>
        [TestMethod]
        public void SendScheduleMssage()
        {
            _queueManagerMock = new Mock<IQueueManager>();
            IList<ClientAccountEntity> listClientAccountEntities = new List<ClientAccountEntity>
            {
                new ClientAccountEntity
                {
                    ClientId = 87,
                    AccountId = "",
                    CustomerId = ""
                }
            };
            byte[] token = null;
            CassandraRepositoryMock.Setup(
                t =>t.Get(It.IsAny<string>(), It.IsAny<int>(), ref token,It.IsAny<Expression<Func<ClientAccountEntity, bool>>>())).Returns(listClientAccountEntities);

            _queueManagerMock.Setup(t => t.AddQueueAsync(It.IsAny<string>(), It.IsAny<string>()))
                .Returns(Task.FromResult(typeof(void)));

            _clientAccount.QueueManager = new Lazy<IQueueManager>(() => _queueManagerMock.Object);
            _clientAccount.SendScheduleMessage(It.IsAny<int>(),It.IsAny<string>());

            CassandraRepositoryMock.Verify(t => t.Get(It.IsAny<string>(), It.IsAny<int>(), ref token, It.IsAny<Expression<Func<ClientAccountEntity, bool>>>()),Times.AtLeastOnce);
            MockRepository.VerifyAll();
        }
    }
}