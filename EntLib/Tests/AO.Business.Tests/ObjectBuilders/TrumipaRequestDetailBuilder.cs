﻿using AO.Entities;

namespace AO.Business.Tests.ObjectBuilders
{
    internal class TrumipaRequestDetailBuilder
    {
        private string _description = "Message1";
        private string _requestId = "TrumpiaDetailTest";

        internal TrumpiaRequestDetailEntity Build()
        {
            var detail = new TrumpiaRequestDetailEntity
            {
                Description = _description,
                ErrorMessage = string.Empty,
                MessageId = string.Empty,
                RequestId = _requestId,
                SmsBody = string.Empty,
                StatusCode = string.Empty
            };

            return detail;
        }

        internal TrumipaRequestDetailBuilder WithRequestId(string requestId)
        {
            _requestId = requestId;
            return this;
        }
    }
}
