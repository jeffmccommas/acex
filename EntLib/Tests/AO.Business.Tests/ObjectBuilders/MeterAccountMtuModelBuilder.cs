﻿using System;
using CE.AO.Models;

namespace AO.Business.Tests.ObjectBuilders
{
    /// <summary>
    /// Helper class designed to build a MeterAccountMtuModel.  The class returns a MeterAccountMtuModel by calling Build().  
    /// 
    /// The class is constructed in such a way that LINQ Query Syntax helps create unit/integration tests that are descriptive 
    /// and easy to follow.  The intent is to abstract the nuances of an object from the tests so that each test can build
    /// an object specifying only the paramters/options of concern to the test.
    /// Example Usage: new MeterAccountMtuModelBuilder().WithMtuType(10).Build() 
    /// This example will return a MeterAccountMtuModel with an MtuTypeId of '10'.  All other values will be defaulted as defined below.  
    /// </summary>
    internal class MeterAccountMtuModelBuilder
    {
        private int _clientId = 87;
        private int _meterid = 12;
        private string _accountNumber = "TestAccountNo2";
        private bool _active = false;
        private DateTime _meterInstallDate = DateTime.UtcNow;
        private string _meterSerialNumber = "MSerialNo12";
        private int _crossReferenceId = 1;
        private int _meterTypeId = 12;
        private int _mtuId = 12;
        private int _mtuTypeId = 1;
        private DateTime _mtuInstallDate = DateTime.UtcNow;

        /// <summary>
        /// Builds an instance of MeterAccountMtuModel
        /// </summary>
        /// <returns>MeterAccountMtuModel</returns>
        public MeterAccountMtuModel Build()
        {
            return new MeterAccountMtuModel
            {
                ClientId = _clientId,
                MeterId = _meterid,
                AccountNumber = _accountNumber,
                Active = _active,
                CrossReferenceId = _crossReferenceId,
                MeterInstallDate = _meterInstallDate,
                MeterSerialNumber = _meterSerialNumber,
                MeterTypeId = _meterTypeId,
                MtuId = _mtuId,
                MtuTypeId = _mtuTypeId,
                MtuInstallDate = _mtuInstallDate
            };
        }

        /// <summary>
        /// Sets value of MtuTypeId
        /// </summary>
        /// <param name="mtuType">Integer value of MtuTypeId</param>
        /// <returns>MeterAccountMtuModelBuilder</returns>
        public MeterAccountMtuModelBuilder WithMtuType(int mtuType)
        {
            _mtuTypeId = mtuType;
            return this;
        }
    }
}
