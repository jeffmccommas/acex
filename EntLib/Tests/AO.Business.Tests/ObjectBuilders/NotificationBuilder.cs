﻿using System;
using AO.Entities;

namespace AO.Business.Tests.ObjectBuilders
{
    /// <summary>
    /// Helper class designed to build a NotificationEntity.  The class returns a NotificationEntity by calling Build().  
    /// 
    /// The class is constructed in such a way that LINQ Query Syntax helps create unit/integration tests that are descriptive 
    /// and easy to follow.  The intent is to abstract the nuances of an object from the tests so that each test can build
    /// an object specifying only the paramters/options of concern to the test.
    /// Example Usage: new NotificationBuilder().WithClientId(10).Build() 
    /// This example will return a NotificationEntity with a ClientId of '10'.  All other values will be defaulted as defined below.  
    /// </summary>
    internal class NotificationBuilder
    {

        private int _clientId = 87;
        private string _accountId = "123456";
        private string _serviceContractId = "ServiceContractId";
        private string _programName = "test program";
        private string _insight = "billtodate";
        private bool _isNotified = false;
        private bool _isDelivered = false;
        private bool _isDropped = false;
        private bool _isBounced = false;
        private bool _isOpened = false;
        private bool _isClicked = false;
        private bool _isScheduled = false;
        private string _channel = "";
        private string _templateId = "";
        private string _customerId = "123456";
        private string _evaluationType = "";
        private DateTime _notifiedTime = DateTime.UtcNow;
        private string _trumpiaRequestDetailId = "";
        private bool _isLatest = true;

        /// <summary>
        /// Builds an instance of NotificationEntity
        /// </summary>
        /// <returns>NotificationEntity</returns>
        internal NotificationEntity Build()
        {
            var notificationEntity = new NotificationEntity
            {
                ClientId = _clientId,
                AccountId = _accountId,
                ServiceContractId = _serviceContractId,
                ProgramName = _programName,
                Insight = _insight,
                IsNotified = _isNotified,
                IsDelivered = _isDelivered,
                IsDropped = _isDropped,
                IsBounced = _isBounced,
                IsOpened = _isOpened,
                IsClicked = _isClicked,
                IsScheduled = _isScheduled,
                Channel = _channel,
                TemplateId = _templateId,
                CustomerId = _customerId,
                EvaluationType = _evaluationType,
                NotifiedDateTime = _notifiedTime,
                TrumpiaRequestDetailId = _trumpiaRequestDetailId,
                IsLatest = _isLatest
            };

            return notificationEntity;
        }

        /// <summary>
        /// Sets the clientId of this builder
        /// </summary>
        /// <param name="clientId">Integer value of clientId</param>
        /// <returns>NotificationBuilder</returns>
        internal NotificationBuilder WithClientId(int clientId)
        {
            _clientId = clientId;
            return this;
        }

        /// <summary>
        /// Sets the clientId of this builder
        /// </summary>
        /// <param name="customerId">Integer value of customerId</param>
        /// <returns>NotificationBuilder</returns>
        internal NotificationBuilder WithCustomerId(string customerId)
        {
            _customerId = customerId;
            return this;
        }
        /// <summary>
        /// Sets the account Id for this builder
        /// </summary>
        /// <param name="accountId">String value of account Id</param>
        /// <returns>NotificationBuilder</returns>
        internal NotificationBuilder WithAccountId(string accountId)
        {
            _accountId = accountId;
            return this;
        }

        public NotificationBuilder WithNotificationDate(DateTime notificationDate)
        {
            _notifiedTime = notificationDate;
            return this;
        }

        public NotificationBuilder WithScheduled()
        {
            _channel = "SMS";
            _isScheduled = true;
            _isNotified = true;

            return this;
        }
    }
}
