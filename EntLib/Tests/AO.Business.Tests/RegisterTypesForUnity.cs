﻿using AO.BusinessContracts;
using AO.BusinessContracts.EventProcessesContracts;
using AO.DataAccess;
using AO.DataAccess.Contracts;
using AO.EventProcessors.EventProcessor;
using CE.AO.Models;
using CE.AO.Business;
using CE.AO.Utilities;
using CE.BillToDate;
using CE.ContentModel;
using Microsoft.Practices.Unity;

namespace AO.Business.Tests
{
    public static class RegisterTypesForUnity
    {
        public static void RegisterTypes(IUnityContainer container)
        {
            container.RegisterType<IQueueManager, QueueManager>();

            container.RegisterType<ICassandraRepository, CassandraRepository>();

            container.RegisterType<IAzureQueue, AzureQueue>();

            container.RegisterType<IAccountLookup, AccountLookup>();

            container.RegisterType<IAccountUpdates, AccountUpdates>();

            container.RegisterType<IAccountUpdatesFacade, AccountUpdatesFacade>();

            container.RegisterType<IAmi, Ami>();

            container.RegisterType<IAmiReading, AmiReading>();

            container.RegisterType<ITallAMI, ConsumptionByMeter>();

            container.RegisterType<IBilling, Billing>();

            container.RegisterType<IBillingFacade, BillingFacade>();

            container.RegisterType<IBillingCycleSchedule, BillingCycleSchedule>();

            container.RegisterType<ICalculation, Calculation>();

            container.RegisterType<ICalculationFacade, CalculationFacade>();

            container.RegisterType<ICustomer, Customer>();

            container.RegisterType<IClientAccount, ClientAccount>();

            container.RegisterType<IEvaluation, Evaluation>();

            container.RegisterType<IEvaluationFacade, EvaluationFacade>();

            container.RegisterType<ISubscription, Subscription>();

            container.RegisterType<ISubscriptionFacade, SubscriptionFacade>();

            container.RegisterType<IPremise, Premise>();

            container.RegisterType<ITrumpiaRequestDetail, TrumpiaRequestDetail>();

            container.RegisterType<ICustomerMobileNumberLookup, CustomerMobileNumberLookup>();

            container.RegisterType<INotification, Notification>();

            container.RegisterType<INotificationFacade, NotificationFacade>();

            container.RegisterType<IReport, Report>();

            container.RegisterType<ITrumpiaKeywordConfiguration, TrumpiaKeywordConfiguration>();

            container.RegisterType<IMeterAccountMtu, MeterAccountMtu>();

            container.RegisterType<ISendEmail, SendEmail>();

            container.RegisterType<ISendSms, SendSms>();

            container.RegisterType<IContentModelFactory, ContentModelFactoryContentful>(new TransientLifetimeManager());

            container.RegisterType<IClientConfigFacade, ClientConfigFacade>();

            container.RegisterType<ICalculationManagerFactory, CalculationManagerFactory>();

            container.RegisterType<IMeterReadFacade, MeterReadFacade>();

            container.RegisterType<IConsumptionByAccount, ConsumptionByAccount>();

            container.RegisterType<IDcuAlarmFacade, DcuAlarmFacade>();

            container.RegisterType<IAlarmFacade, AlarmFacade>();

            container.RegisterType<IProcessEvent, Ami15MinuteIntervalProcessEvent>();

            container.RegisterType<IProcessEvent, DcuAlarmProcessEvent>();

            container.RegisterType<IDcuAlarmCodeHistory, DcuAlarmCodeHistory>();
			
		}

		/// <summary>
		/// To Enable Logging
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="unityContainer"></param>
		/// <param name="logModel"></param>
		/// <returns></returns>
		public static T Resolve<T>(this IUnityContainer unityContainer, LogModel logModel)
        {
            return unityContainer.Resolve<T>(new ParameterOverride("logModel", new LogModel()));

        }
    }
}
