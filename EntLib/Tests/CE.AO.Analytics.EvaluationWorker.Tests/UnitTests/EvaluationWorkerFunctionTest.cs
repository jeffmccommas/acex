﻿using System.IO;
using System.Runtime.Remoting.Messaging;
using AO.Business;
using AO.BusinessContracts;
using AO.Registrar;
using Microsoft.Practices.Unity;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Enums = CE.AO.Utilities.Enums;
using CE.AO.Models;

namespace CE.AO.Analytics.EvaluationWorker.Tests.UnitTests
{
    /// <summary>
    /// Test Case Class for Evaluation Worker Role(Function.cs)
    /// </summary>
    [TestClass]
    public class EvaluationWorkerFunctionTest
    {
        private UnityContainer _unityContainer;
        /// <summary>
        /// Test Case Init
        /// </summary>
        [TestInitialize]
        public void Init()
        {
            _unityContainer = new UnityContainer();
            AutoMapperConfig.AutoMapperMappings();
            new DataStorageRegistrar().Initialize<TransientLifetimeManager>(_unityContainer);
            _unityContainer.RegisterInstance(new Mock<IEvaluationFacade>().Object);
            _unityContainer.RegisterInstance(new Mock<IReport>().Object);
            Functions.UnityContainer = _unityContainer;
        }

        /// <summary>
        /// Test Case for Process Evaluation
        /// </summary>
        [TestMethod]
        public void ProcessEvaluation()
        {
            var log = TextWriter.Null;
            Functions.LogModel = new LogModel
            {
                Module = Enums.Module.InsightEvaluation,
                DisableLog = true
            };
            Functions.ProcessEvaluation("87^^600^^22^^8700^^09/24/2016^^Source^^22^^MetaData^^2", log);
            Assert.AreEqual(true, true);
        }

        /// <summary>
        /// Test Case for Process Evaluation Else Part
        /// </summary>
        [TestMethod]
        public void ProcessEvaluation_Else()
        {
            TextWriter log = TextWriter.Null;
            Functions.LogModel = new LogModel()
            {
                Module = Enums.Module.Calculation,
                DisableLog = true
            };
            Functions.ProcessEvaluation("87^^600^^22^^8700^^09/24/2016^^Source^^22^^MetaData", log);
            Assert.AreEqual(true, true);
        }

        /// <summary>
        /// Test Case for Process Evaluation Catch 
        /// </summary>
        [TestMethod]
        public void ProcessEvaluation_Catch()
        {
            TextWriter log = TextWriter.Null;
            Functions.LogModel = new LogModel()
            {
                Module = Enums.Module.InsightEvaluation,
                DisableLog = true
            };
            try
            {
                Functions.ProcessEvaluation("87^^600^^22^^8700^^Source^^09/24/2016^^22^^MetaData^^2", log);
            }
            catch
            {
                Assert.AreEqual(true, true);
            }
        }

        /// <summary>
        /// Process Insight Report
        /// </summary>
        [TestMethod]
        public void ProcessInsightReport()
        {
            TextWriter log = TextWriter.Null;
            Functions.LogModel = new LogModel()
            {
                Module = Enums.Module.InsightEvaluation,
                DisableLog = true
            };
            Functions.ProcessInsightReport("87^^09/24/2016^^22", log);
            Assert.AreEqual(true, true);
        }

        /// <summary>
        /// Process Insight Report Else
        /// </summary>
        [TestMethod]
        public void ProcessInsightReport_Else()
        {
            TextWriter log = TextWriter.Null;
            Functions.LogModel = new LogModel()
            {
                Module = Enums.Module.InsightEvaluation,
                DisableLog = true
            };
            Functions.ProcessInsightReport("87^^09/24/2016", log);
            Assert.AreEqual(true, true);
        }

        /// <summary>
        /// Process Insight Report Catch
        /// </summary>
        [TestMethod]
        public void ProcessInsightReport_Catch()
        {
            TextWriter log = TextWriter.Null;
            Functions.LogModel = new LogModel()
            {
                Module = Enums.Module.InsightEvaluation,
                DisableLog = true
            };
            try
            {
                Functions.ProcessInsightReport("87^^22^^09/24/2016", log);
            }
            catch
            {
                Assert.AreEqual(true, true);
            }
        }

        /// <summary>
        /// Cleanup Method To Release Log4Net Data Resources hold by Any Test Method
        /// </summary>
        [TestCleanup]
        public void CleanUpMethod()
        {
            CallContext.FreeNamedDataSlot("log4net.Util.LogicalThreadContextProperties");
        }
    }

}
