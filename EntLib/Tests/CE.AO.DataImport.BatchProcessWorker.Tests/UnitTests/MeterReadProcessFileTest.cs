﻿using System.Collections.Generic;
using CE.AO.DataImport.BatchProcessWorker.Files;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using CE.AO.Models;
using CE.AO.Models.RawFileModels;
using Microsoft.ServiceBus.Messaging;
using Microsoft.WindowsAzure.Storage.Blob;
using Enums = CE.AO.Utilities.Enums;

namespace CE.AO.DataImport.BatchProcessWorker.Tests.UnitTests
{
    [TestClass]
    public class MeterReadProcessFileTest
    {
        /// <summary>
        /// Test Case for Initialize method
        /// </summary>
        [TestMethod]
        public void Initialize()
        {
            var meterReadProcessFileMock = new Mock<MeterReadProcessFile>(new LogModel { DisableLog = true }) { CallBase = true };
            var cloudBlobkBlob = new Mock<ICloudBlob>();

            meterReadProcessFileMock.Setup(t => t.GetEventHubClient());
            meterReadProcessFileMock.Setup(
                t =>
                    t.Process<RawMeterReadModel>(It.IsAny<ICloudBlob>(),
                        It.IsAny<IDictionary<string, object>>(), It.IsAny<Enums.EventType>(), It.IsAny<EventHubClient>()));
            meterReadProcessFileMock.Setup(
                t =>
                    t.ArchieveBlob(It.IsAny<ICloudBlob>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<Enums.EventType>()));
            meterReadProcessFileMock.Setup(t =>
                t.GetBlockBlob(It.IsAny<string>(), It.IsAny<string>())).Returns(cloudBlobkBlob.Object);
            meterReadProcessFileMock.Setup(t => t.CloseEventHubClient(It.IsAny<EventHubClient>()));

            meterReadProcessFileMock.Object.Initialize("meterread.csv", "client87", "87");

            meterReadProcessFileMock.Verify(t => t.GetEventHubClient(), Times.Once);
            meterReadProcessFileMock.Verify(t =>
                    t.Process<RawMeterReadModel>(It.IsAny<ICloudBlob>(),
                        It.IsAny<IDictionary<string, object>>(), It.IsAny<Enums.EventType>(), It.IsAny<EventHubClient>()), Times.Once);
            meterReadProcessFileMock.Verify(t =>
                    t.ArchieveBlob(It.IsAny<ICloudBlob>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<Enums.EventType>()), Times.Once);
            meterReadProcessFileMock.Verify(t =>
                t.GetBlockBlob(It.IsAny<string>(), It.IsAny<string>()), Times.Once);
            meterReadProcessFileMock.Verify(t => t.CloseEventHubClient(It.IsAny<EventHubClient>()), Times.Once);
            meterReadProcessFileMock.VerifyAll();
            Assert.AreEqual(true, true);
        }
    }
}
