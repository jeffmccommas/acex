﻿using System.Collections.Generic;
using CE.AO.DataImport.BatchProcessWorker.Files;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using CE.AO.Models;
using CE.AO.Models.RawFileModels;
using Microsoft.ServiceBus.Messaging;
using Microsoft.WindowsAzure.Storage.Blob;
using Enums = CE.AO.Utilities.Enums;

namespace CE.AO.DataImport.BatchProcessWorker.Tests.UnitTests
{
    [TestClass]
    public class MeterAccountMtuProcessFileTest
    {
        /// <summary>
        /// Test Case for Initialize method
        /// </summary>
        [TestMethod]
        public void Initialize()
        {
            var meterAccountMtuProcessFileMock = new Mock<MeterAccountMtuProcessFile>(new LogModel { DisableLog = true }) { CallBase = true };
            var cloudBlobkBlob = new Mock<ICloudBlob>();

            meterAccountMtuProcessFileMock.Setup(t => t.GetEventHubClient());
            meterAccountMtuProcessFileMock.Setup(
                t =>
                    t.Process<RawMeterAccountMtuModel>(It.IsAny<ICloudBlob>(),
                        It.IsAny<IDictionary<string, object>>(), It.IsAny<Enums.EventType>(), It.IsAny<EventHubClient>()));
            meterAccountMtuProcessFileMock.Setup(
                t =>
                    t.ArchieveBlob(It.IsAny<ICloudBlob>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<Enums.EventType>()));
            meterAccountMtuProcessFileMock.Setup(t =>
                t.GetBlockBlob(It.IsAny<string>(), It.IsAny<string>())).Returns(cloudBlobkBlob.Object);
            meterAccountMtuProcessFileMock.Setup(t => t.CloseEventHubClient(It.IsAny<EventHubClient>()));

            meterAccountMtuProcessFileMock.Object.Initialize("meteraccountmtu.csv", "client87", "87");

            meterAccountMtuProcessFileMock.Verify(t => t.GetEventHubClient(), Times.Once);
            meterAccountMtuProcessFileMock.Verify(t =>
                    t.Process<RawMeterAccountMtuModel>(It.IsAny<ICloudBlob>(),
                        It.IsAny<IDictionary<string, object>>(), It.IsAny<Enums.EventType>(), It.IsAny<EventHubClient>()), Times.Once);
            meterAccountMtuProcessFileMock.Verify(t =>
                    t.ArchieveBlob(It.IsAny<ICloudBlob>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<Enums.EventType>()), Times.Once);
            meterAccountMtuProcessFileMock.Verify(t =>
                t.GetBlockBlob(It.IsAny<string>(), It.IsAny<string>()), Times.Once);
            meterAccountMtuProcessFileMock.Verify(t => t.CloseEventHubClient(It.IsAny<EventHubClient>()), Times.Once);
            meterAccountMtuProcessFileMock.VerifyAll();
            Assert.AreEqual(true, true);
        }
    }
}
