﻿using System.Collections.Generic;
using CE.AO.DataImport.BatchProcessWorker.Files;
using CE.AO.Models;
using CE.AO.Models.RawFileModels;
using Microsoft.ServiceBus.Messaging;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.WindowsAzure.Storage.Blob;
using Moq;
using Enums = CE.AO.Utilities.Enums;

namespace CE.AO.DataImport.BatchProcessWorker.Tests.UnitTests
{
    [TestClass]
    public class SubscriptionProcessFileTest
    {
        /// <summary>
        /// Test Case for Initialize method
        /// </summary>
        [TestMethod]
        public void Initialize()
        {
            var subscriptionProcess = new Mock<SubscriptionProcessFile>(new LogModel { DisableLog = true }) { CallBase = true };

            var cloudBlob = new Mock<ICloudBlob>();

            subscriptionProcess.Setup(p => p.GetBlockBlob(It.IsAny<string>(), It.IsAny<string>())).Returns(cloudBlob.Object);
            subscriptionProcess.Setup(p => p.Process<RawSubscriptionModel>(It.IsAny<ICloudBlob>(), It.IsAny<IDictionary<string, object>>(), It.IsAny<Enums.EventType>(), It.IsAny<EventHubClient>()));
            subscriptionProcess.Setup(p => p.ArchieveBlob(It.IsAny<ICloudBlob>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<Enums.EventType>()));
            subscriptionProcess.Setup(t => t.GetEventHubClient());
            subscriptionProcess.Setup(t => t.CloseEventHubClient(It.IsAny<EventHubClient>()));

            subscriptionProcess.Object.Initialize("Subscription_87_10Mar_tush_Test.csv", "client87", "87");

            subscriptionProcess.Verify(p => p.GetBlockBlob(It.IsAny<string>(), It.IsAny<string>()), Times.Once);
            subscriptionProcess.Verify(p => p.Process<RawSubscriptionModel>(It.IsAny<ICloudBlob>(), It.IsAny<IDictionary<string, object>>(), It.IsAny<Enums.EventType>(), It.IsAny<EventHubClient>()), Times.Once);
            subscriptionProcess.Verify(p => p.ArchieveBlob(It.IsAny<ICloudBlob>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<Enums.EventType>()), Times.Once);
            subscriptionProcess.Verify(t => t.GetEventHubClient(), Times.Once);
            subscriptionProcess.Verify(t => t.CloseEventHubClient(It.IsAny<EventHubClient>()), Times.Once);

            subscriptionProcess.VerifyAll();

            Assert.AreEqual(true, true);
        }
    }
}
