﻿using System;
using System.Collections.Generic;
using CE.AO.DataImport.BatchProcessWorker.Files;
using CE.AO.Models;
using CE.AO.Models.RawFileModels;
using Microsoft.ServiceBus.Messaging;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.WindowsAzure.Storage.Blob;
using Moq;

namespace CE.AO.DataImport.BatchProcessWorker.Tests.UnitTests
{
    [TestClass]
    public class ReadsProcessFileTest
    {
        /// <summary>
        /// Test Case for Process method
        /// </summary>
        [TestMethod]
        public void Process()
        {
            var baseReadProcessFileMock = new Mock<ReadsProcessFile>(new LogModel { DisableLog = true }) { CallBase = true };
            var cloudBlobkBlob = new Mock<ICloudBlob>();

            baseReadProcessFileMock.Setup(t => t.SendEventsInBatch(It.IsAny<EventHubClient>(), It.IsAny<List<EventData>>()));
            baseReadProcessFileMock.Setup(t => t.Process<RawBaseReadModel>(It.IsAny<ICloudBlob>(),
                It.IsAny<IDictionary<string, object>>(), It.IsAny<Utilities.Enums.EventType>(), It.IsAny<EventHubClient>()));

            baseReadProcessFileMock.Object.TriggerCalculationForList = new List<RawBaseReadModel>
            {
                new RawBaseReadModel
                {
                    METER_ID = "Test Meter"
                }
            };
            var customProperties = new Dictionary<string, object>
            {
                { "EventType",Convert.ToString(Utilities.Enums.EventType.MeterRead)}
            };

            baseReadProcessFileMock.Object.ProcessFile<RawBaseReadModel>(cloudBlobkBlob.Object, customProperties, Utilities.Enums.EventType.MeterRead, It.IsAny<EventHubClient>());
            baseReadProcessFileMock.Object.ProcessFile<RawBaseReadModel>(cloudBlobkBlob.Object, customProperties, Utilities.Enums.EventType.NccAmiConsumption, It.IsAny<EventHubClient>());

            baseReadProcessFileMock.VerifyAll();
            Assert.AreEqual(true, true);
        }

        /// <summary>
        /// Test Case for PreProcess method
        /// </summary>
        [TestMethod]
        public void PreProcess()
        {
            var baseReadProcessFileMock = new Mock<ReadsProcessFile>(new LogModel { DisableLog = true }) { CallBase = true };
            var record = new RawBaseReadModel
            {
                METER_ID = "Test Meter"
            };            
            baseReadProcessFileMock.Object.PreProcessRow(Utilities.Enums.EventType.MeterRead, record);

            baseReadProcessFileMock.Object.TriggerCalculationForList = new List<RawBaseReadModel> { record };
            baseReadProcessFileMock.Object.PreProcessRow(Utilities.Enums.EventType.MeterRead, record);

            baseReadProcessFileMock.VerifyAll();
            Assert.AreEqual(true, true);
        }
    }
}
