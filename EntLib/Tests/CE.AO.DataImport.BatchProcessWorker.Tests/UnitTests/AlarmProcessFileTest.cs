﻿using System.Collections.Generic;
using CE.AO.DataImport.BatchProcessWorker.Files;
using CE.AO.Models;
using CE.AO.Models.RawFileModels;
using Microsoft.ServiceBus.Messaging;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.WindowsAzure.Storage.Blob;
using Moq;
using Enums = CE.AO.Utilities.Enums;

namespace CE.AO.DataImport.BatchProcessWorker.Tests.UnitTests
{

    /// <summary>
    /// Test class for AlarmProcessFile 
    /// </summary>
    [TestClass]
    public class AlarmProcessFileTest
    {
        /// <summary>
        /// Test Case for Initialize method
        /// </summary>
        [TestMethod]
        public void Initialize()
        {
            var alarmProcessFile = new Mock<AlarmProcessFile>(new LogModel { DisableLog = true }) { CallBase = true };

            var cloudBlob = new Mock<ICloudBlob>();

            alarmProcessFile.Setup(p => p.GetBlockBlob(It.IsAny<string>(), It.IsAny<string>())).Returns(cloudBlob.Object);
            alarmProcessFile.Setup(p => p.Process<RawAlarmModel>(It.IsAny<ICloudBlob>(), It.IsAny<IDictionary<string, object>>(), It.IsAny<Enums.EventType>(), It.IsAny<EventHubClient>()));
            alarmProcessFile.Setup(p => p.ArchieveBlob(It.IsAny<ICloudBlob>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<Enums.EventType>()));
            alarmProcessFile.Setup(t => t.GetEventHubClient());
            alarmProcessFile.Setup(t => t.CloseEventHubClient(It.IsAny<EventHubClient>()));

            alarmProcessFile.Object.Initialize("87_alarm__TestFile", "client87", "87");

            alarmProcessFile.Verify(p => p.GetBlockBlob(It.IsAny<string>(), It.IsAny<string>()), Times.Once);
            alarmProcessFile.Verify(p => p.Process<RawAlarmModel>(It.IsAny<ICloudBlob>(), It.IsAny<IDictionary<string, object>>(), It.IsAny<Enums.EventType>(), It.IsAny<EventHubClient>()), Times.Once);
            alarmProcessFile.Verify(p => p.ArchieveBlob(It.IsAny<ICloudBlob>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<Enums.EventType>()), Times.Once);
            alarmProcessFile.Verify(t => t.GetEventHubClient(), Times.Once);
            alarmProcessFile.Verify(t => t.CloseEventHubClient(It.IsAny<EventHubClient>()), Times.Once);

            alarmProcessFile.VerifyAll();

            Assert.AreEqual(true, true);
        }
    }
}
