﻿using System.Collections.Generic;
using CE.AO.DataImport.BatchProcessWorker.Files;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using CE.AO.Models;
using CE.AO.Models.RawFileModels;
using Microsoft.ServiceBus.Messaging;
using Microsoft.WindowsAzure.Storage.Blob;
using Enums = CE.AO.Utilities.Enums;

namespace CE.AO.DataImport.BatchProcessWorker.Tests.UnitTests
{
    [TestClass]
    public class DcuProvisioningProcessFileTest
    {
        /// <summary>
        /// Test Case for Initialize method
        /// </summary>
        [TestMethod]
        public void Initialize()
        {
            var dcuProvisioningProcessFileMock = new Mock<DcuProvisioningProcessFile>(new LogModel { DisableLog = true }) { CallBase = true };
            var cloudBlobkBlob = new Mock<ICloudBlob>();

            dcuProvisioningProcessFileMock.Setup(t => t.GetEventHubClient());
            dcuProvisioningProcessFileMock.Setup(
                t =>
                    t.Process<RawDcuProvisioningModel>(It.IsAny<ICloudBlob>(),
                        It.IsAny<IDictionary<string, object>>(), It.IsAny<Enums.EventType>(), It.IsAny<EventHubClient>()));
            dcuProvisioningProcessFileMock.Setup(
                t =>
                    t.ArchieveBlob(It.IsAny<ICloudBlob>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<Enums.EventType>()));
            dcuProvisioningProcessFileMock.Setup(t =>
                t.GetBlockBlob(It.IsAny<string>(), It.IsAny<string>())).Returns(cloudBlobkBlob.Object);
            dcuProvisioningProcessFileMock.Setup(t => t.CloseEventHubClient(It.IsAny<EventHubClient>()));

            dcuProvisioningProcessFileMock.Object.Initialize("dcudata.csv", "client87", "87");

            dcuProvisioningProcessFileMock.Verify(t => t.GetEventHubClient(), Times.Once);
            dcuProvisioningProcessFileMock.Verify(t =>
                    t.Process<RawDcuProvisioningModel>(It.IsAny<ICloudBlob>(),
                        It.IsAny<IDictionary<string, object>>(), It.IsAny<Enums.EventType>(), It.IsAny<EventHubClient>()), Times.Once);
            dcuProvisioningProcessFileMock.Verify(t =>
                    t.ArchieveBlob(It.IsAny<ICloudBlob>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<Enums.EventType>()), Times.Once);
            dcuProvisioningProcessFileMock.Verify(t =>
                t.GetBlockBlob(It.IsAny<string>(), It.IsAny<string>()), Times.Once);
            dcuProvisioningProcessFileMock.Verify(t => t.CloseEventHubClient(It.IsAny<EventHubClient>()), Times.Once);
            dcuProvisioningProcessFileMock.VerifyAll();
            Assert.AreEqual(true, true);
        }
    }
}
