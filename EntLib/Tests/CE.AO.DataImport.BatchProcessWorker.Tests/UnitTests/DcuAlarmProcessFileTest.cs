﻿using System.Collections.Generic;
using CE.AO.DataImport.BatchProcessWorker.Files;
using CE.AO.Models;
using CE.AO.Models.RawFileModels;
using Microsoft.ServiceBus.Messaging;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.WindowsAzure.Storage.Blob;
using Moq;
using Enums = CE.AO.Utilities.Enums;

namespace CE.AO.DataImport.BatchProcessWorker.Tests.UnitTests
{
    [TestClass]
    public class DcuAlarmProcessFileTest
    {
        /// <summary>
        /// Test Case for Initialize method
        /// </summary>
        [TestMethod]
        public void Initialize()
        {
            var dcuAlarmProcessFile = new Mock<DcuAlarmProcessFile>(new LogModel { DisableLog = true }) { CallBase = true };
            var cloudBlob = new Mock<ICloudBlob>();
            

            dcuAlarmProcessFile.Setup(p => p.GetBlockBlob(It.IsAny<string>(), It.IsAny<string>())).Returns(cloudBlob.Object);

            dcuAlarmProcessFile.Setup(p => p.Process<RawDcuAlarmModel>(It.IsAny<ICloudBlob>(), It.IsAny<IDictionary<string, object>>(), It.IsAny<Enums.EventType>(), It.IsAny<EventHubClient>()));

            dcuAlarmProcessFile.Setup(p => p.ArchieveBlob(It.IsAny<ICloudBlob>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<Enums.EventType>()));

            dcuAlarmProcessFile.Setup(t => t.GetEventHubClient());

            dcuAlarmProcessFile.Setup(t => t.CloseEventHubClient(It.IsAny<EventHubClient>()));

            dcuAlarmProcessFile.Object.Initialize("87_dcualarm__TestFile", "client87", "87");

            dcuAlarmProcessFile.Verify(p => p.GetBlockBlob(It.IsAny<string>(), It.IsAny<string>()), Times.Once);

            dcuAlarmProcessFile.Verify(p => p.Process<RawDcuAlarmModel>(It.IsAny<ICloudBlob>(), It.IsAny<IDictionary<string, object>>(), It.IsAny<Enums.EventType>(), It.IsAny<EventHubClient>()), Times.Once);

            dcuAlarmProcessFile.Verify(p => p.ArchieveBlob(It.IsAny<ICloudBlob>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<Enums.EventType>()), Times.Once);

            dcuAlarmProcessFile.Verify(t => t.GetEventHubClient(), Times.Once);

            dcuAlarmProcessFile.Verify(t => t.CloseEventHubClient(It.IsAny<EventHubClient>()), Times.Once);

            dcuAlarmProcessFile.VerifyAll();

            Assert.AreEqual(true, true);
        }
    }
}

