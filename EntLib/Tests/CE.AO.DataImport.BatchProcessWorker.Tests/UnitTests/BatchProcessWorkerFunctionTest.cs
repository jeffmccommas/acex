﻿using System.IO;
using System.Runtime.Remoting.Messaging;
using CE.AO.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Enums = CE.AO.Utilities.Enums;

namespace CE.AO.DataImport.BatchProcessWorker.Tests.UnitTests
{
    /// <summary>
    /// Test Case Class for BatchProcess Worker Role(Function.cs)
    /// </summary>
    [TestClass]
    public class BatchProcessWorkerFunctionTest
    {
        /// <summary>
        /// Test Case for Initialization Test
        /// </summary>
        [TestInitialize]
        public void Init()
        {
            AutoMapperConfig.AutoMapperMappings();
        }

        /// <summary>
        /// Test Case for Process Batch for AMi 60 min interval file
        /// </summary>
        [TestMethod]
        public void ProcessBatch_ForAmi60Min()
        {
            try
            {
                TextWriter log = TextWriter.Null;
                Functions.LogModel = new LogModel()
                {
                    DisableLog = true,
                    Module = Enums.Module.FileProcessing,
                    ProcessingType = Enums.ProcessingType.File
                };
                Functions.ProcessBatch("AMIEnd_EST_AMI_87_60min-Does not Handles DST1_TestTush1.csv:client87:87", log);
            }
            catch
            {
                Assert.AreEqual(true, true);
            }
        }

        /// <summary>
        /// Test Case for Process Batch Ami 15 min interval file
        /// </summary>
        [TestMethod]
        public void ProcessBatch_ForAmi15Min()
        {
            try
            {
                TextWriter log = TextWriter.Null;
                Functions.LogModel = new LogModel()
                {
                    DisableLog = true,
                    Module = Enums.Module.FileProcessing,
                    ProcessingType = Enums.ProcessingType.File
                };
                Functions.ProcessBatch("EST_AMI_87_15min_13JuneuTushar-Test.csv:client87:87", log);
            }
            catch
            {
                Assert.AreEqual(true, true);
            }
        }

        /// <summary>
        /// Test Case for Process Batch Ami 30 mIn Interval File
        /// </summary>
        [TestMethod]
        public void ProcessBatch_ForAmi30Min()
        {
            try
            {
                TextWriter log = TextWriter.Null;
                Functions.LogModel = new LogModel()
                {
                    DisableLog = true,
                    Module = Enums.Module.FileProcessing,
                    ProcessingType = Enums.ProcessingType.File
                };
                Functions.ProcessBatch("EST Timezone AMI_87_30min_Valid Data - Tush_Test.csv:client87:87", log);
            }
            catch
            {
                Assert.AreEqual(true, true);
            }
        }

        /// <summary>
        /// Test Case for Process Batch for Accounts
        /// </summary>
        [TestMethod]
        public void ProcessBatch_ForAccountUpdates()
        {
            try
            {
                TextWriter log = TextWriter.Null;
                Functions.LogModel = new LogModel()
                {
                    DisableLog = true,
                    Module = Enums.Module.FileProcessing,
                    ProcessingType = Enums.ProcessingType.File
                };
                Functions.ProcessBatch("AccountUpdates_87_UATDay4BCycle007_UnitTest_Tush.csv:client87:87", log);
            }
            catch
            {
                Assert.AreEqual(true, true);
            }
        }

        /// <summary>
        /// Test Case for Process Batch Billing File
        /// </summary>
        [TestMethod]
        public void ProcessBatch_ForBilling()
        {
            try
            {
                TextWriter log = TextWriter.Null;
                Functions.LogModel = new LogModel()
                {
                    DisableLog = true,
                    Module = Enums.Module.FileProcessing,
                    ProcessingType = Enums.ProcessingType.File
                };
                Functions.ProcessBatch("Bill_87_UATDay41_tush_Test.csv:client87:87", log);
            }
            catch
            {
                Assert.AreEqual(true, true);
            }
        }

        /// <summary>
        /// Test Case for Process Batch Subscription File
        /// </summary>
        [TestMethod]
        public void ProcessBatch_ForSubscription()
        {
            try
            {
                TextWriter log = TextWriter.Null;
                Functions.LogModel = new LogModel()
                {
                    DisableLog = true,
                    Module = Enums.Module.FileProcessing,
                    ProcessingType = Enums.ProcessingType.File
                };
                Functions.ProcessBatch("Subscription_87_10Mar_tush_Test.csv:client87:87", log);
            }
            catch
            {
                Assert.AreEqual(true, true);
            }
        }

        /// <summary>
        /// Test Case for Process Batch for NCC AMi reading File
        /// </summary>
        [TestMethod]
        public void ProcessBatch_ForNCC()
        {
            try
            {
                TextWriter log = TextWriter.Null;
                Functions.LogModel = new LogModel()
                {
                    DisableLog = true,
                    Module = Enums.Module.FileProcessing,
                    ProcessingType = Enums.ProcessingType.File
                };
                Functions.ProcessBatch("ncc__2016-07-28_101914__2de7bbe0-edf8-48cf-9854-50e6b4c2a80f_Tush_Test.csv:client87:87", log);
            }
            catch
            {
                Assert.AreEqual(true, true);
            }
        }

        /// <summary>
        /// Test Case for Process Batch for NCC AMi Consumption File
        /// </summary>
        [TestMethod]
        public void ProcessBatch_ForNCCAmiConsumption()
        {
            try
            {
                TextWriter log = TextWriter.Null;
                Functions.LogModel = new LogModel()
                {
                    DisableLog = true,
                    Module = Enums.Module.FileProcessing,
                    ProcessingType = Enums.ProcessingType.File
                };
                Functions.ProcessBatch("NccCalcConsumptionncc_UnitTest_Tush.csv:client87:87", log);
            }
            catch
            {
                Assert.AreEqual(true, true);
            }
        }

        /// <summary>
        /// Cleanup Method To Release Log4Net Data Resources hold by Any Test Method
        /// </summary>
        [TestCleanup]
        public void CleanUpMethod()
        {
            CallContext.FreeNamedDataSlot("log4net.Util.LogicalThreadContextProperties");
        }

    }
}
