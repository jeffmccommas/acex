﻿using System.Collections.Generic;
using CE.AO.DataImport.BatchProcessWorker.Files;
using CE.AO.Models;
using CE.AO.Models.RawFileModels;
using Microsoft.ServiceBus.Messaging;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.WindowsAzure.Storage.Blob;
using Moq;
using Enums = CE.AO.Utilities.Enums;

namespace CE.AO.DataImport.BatchProcessWorker.Tests.UnitTests
{
    [TestClass]
    public class AccountUpdatesProcessFileTest
    {
        /// <summary>
        /// Test Case for Initialize method
        /// </summary>
        [TestMethod]
        public void Initialize()
        {
            var accountupdates = new Mock<AccountUpdatesProcessFile>(new LogModel { DisableLog = true }) { CallBase = true };

            var cloudBlob = new Mock<ICloudBlob>();

            accountupdates.Setup(p => p.GetBlockBlob(It.IsAny<string>(), It.IsAny<string>())).Returns(cloudBlob.Object);
            accountupdates.Setup(p => p.Process<RawAccountUpdatesModel>(It.IsAny<ICloudBlob>(), It.IsAny<IDictionary<string, object>>(), It.IsAny<Enums.EventType>(), It.IsAny<EventHubClient>()));
            accountupdates.Setup(p => p.ArchieveBlob(It.IsAny<ICloudBlob>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<Enums.EventType>()));
            accountupdates.Setup(t => t.GetEventHubClient());
            accountupdates.Setup(t => t.CloseEventHubClient(It.IsAny<EventHubClient>()));

            accountupdates.Object.Initialize("AccountUpdates_87_UATDay4BCycle007_UnitTest_Tush.csv", "client87", "87");

            accountupdates.Verify(p => p.GetBlockBlob(It.IsAny<string>(), It.IsAny<string>()), Times.Once);
            accountupdates.Verify(p => p.Process<RawAccountUpdatesModel>(It.IsAny<ICloudBlob>(), It.IsAny<IDictionary<string, object>>(), It.IsAny<Enums.EventType>(), It.IsAny<EventHubClient>()), Times.Once);
            accountupdates.Verify(p => p.ArchieveBlob(It.IsAny<ICloudBlob>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<Enums.EventType>()), Times.Once);
            accountupdates.Verify(t => t.GetEventHubClient(), Times.Once);
            accountupdates.Verify(t => t.CloseEventHubClient(It.IsAny<EventHubClient>()), Times.Once);

            accountupdates.VerifyAll();

            Assert.AreEqual(true, true);
        }
    }
}
