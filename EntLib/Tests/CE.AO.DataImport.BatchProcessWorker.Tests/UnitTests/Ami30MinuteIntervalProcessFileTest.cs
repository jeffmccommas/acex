﻿using System.Collections.Generic;
using CE.AO.DataImport.BatchProcessWorker.Files;
using CE.AO.Models;
using CE.AO.Models.RawFileModels;
using Microsoft.ServiceBus.Messaging;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.WindowsAzure.Storage.Blob;
using Moq;
using Enums = CE.AO.Utilities.Enums;

namespace CE.AO.DataImport.BatchProcessWorker.Tests.UnitTests
{
    [TestClass]
    public class Ami30MinuteIntervalProcessFileTest
    {
        /// <summary>
        /// Test Case for Initialize method
        /// </summary>
        [TestMethod]
        public void Initialize()
        {
            var ami30MinInterval = new Mock<Ami30MinuteIntervalProcessFile>(new LogModel { DisableLog = true }) { CallBase = true };

            var cloudBlob = new Mock<ICloudBlob>();

            ami30MinInterval.Setup(p => p.GetBlockBlob(It.IsAny<string>(), It.IsAny<string>())).Returns(cloudBlob.Object);
            ami30MinInterval.Setup(p => p.Process<RawAmi30MinuteIntervalModel>(It.IsAny<ICloudBlob>(), It.IsAny<IDictionary<string, object>>(), It.IsAny<Enums.EventType>(), It.IsAny<EventHubClient>()));
            ami30MinInterval.Setup(p => p.ArchieveBlob(It.IsAny<ICloudBlob>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<Enums.EventType>()));
            ami30MinInterval.Setup(t => t.GetEventHubClient());
            ami30MinInterval.Setup(t => t.CloseEventHubClient(It.IsAny<EventHubClient>()));

            ami30MinInterval.Object.Initialize("EST Timezone AMI_87_30min_Valid Data - Tush_Test.csv", "client87", "87");

            ami30MinInterval.Verify(p => p.GetBlockBlob(It.IsAny<string>(), It.IsAny<string>()), Times.Once);
            ami30MinInterval.Verify(p => p.Process<RawAmi30MinuteIntervalModel>(It.IsAny<ICloudBlob>(), It.IsAny<IDictionary<string, object>>(), It.IsAny<Enums.EventType>(), It.IsAny<EventHubClient>()), Times.Once);
            ami30MinInterval.Verify(p => p.ArchieveBlob(It.IsAny<ICloudBlob>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<Enums.EventType>()), Times.Once);
            ami30MinInterval.Verify(t => t.GetEventHubClient(), Times.Once);
            ami30MinInterval.Verify(t => t.CloseEventHubClient(It.IsAny<EventHubClient>()), Times.Once);

            ami30MinInterval.VerifyAll();

            Assert.AreEqual(true, true);
        }
    }
}
