﻿using System.Collections.Generic;
using CE.AO.DataImport.BatchProcessWorker.Files;
using CE.AO.Models;
using CE.AO.Models.RawFileModels;
using Microsoft.ServiceBus.Messaging;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.WindowsAzure.Storage.Blob;
using Moq;
using Enums = CE.AO.Utilities.Enums;

namespace CE.AO.DataImport.BatchProcessWorker.Tests.UnitTests
{
    [TestClass]
    public class Ami60MinIntervalProcessFileTest
    {
        /// <summary>
        /// Test Case for Initialize method
        /// </summary>
        [TestMethod]
        public void Initialize()
        {
            var ami60MinInterval = new Mock<Ami60MinuteIntervalProcessFile>(new LogModel { DisableLog = true }) { CallBase = true };

            var cloudBlob = new Mock<ICloudBlob>();

            ami60MinInterval.Setup(p => p.GetBlockBlob(It.IsAny<string>(), It.IsAny<string>())).Returns(cloudBlob.Object);
            ami60MinInterval.Setup(p => p.Process<RawAmi60MinuteIntervalModel>(It.IsAny<ICloudBlob>(), It.IsAny<IDictionary<string, object>>(), It.IsAny<Enums.EventType>(), It.IsAny<EventHubClient>()));
            ami60MinInterval.Setup(p => p.ArchieveBlob(It.IsAny<ICloudBlob>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<Enums.EventType>()));
            ami60MinInterval.Setup(t => t.GetEventHubClient());
            ami60MinInterval.Setup(t => t.CloseEventHubClient(It.IsAny<EventHubClient>()));

            ami60MinInterval.Object.Initialize("AMIEnd_EST_AMI_87_60min-Does not Handles DST1_TestTush1.csv", "client87", "87");

            ami60MinInterval.Verify(p => p.GetBlockBlob(It.IsAny<string>(), It.IsAny<string>()), Times.Once);
            ami60MinInterval.Verify(p => p.Process<RawAmi60MinuteIntervalModel>(It.IsAny<ICloudBlob>(), It.IsAny<IDictionary<string, object>>(), It.IsAny<Enums.EventType>(), It.IsAny<EventHubClient>()), Times.Once);
            ami60MinInterval.Verify(p => p.ArchieveBlob(It.IsAny<ICloudBlob>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<Enums.EventType>()), Times.Once);
            ami60MinInterval.Verify(t => t.GetEventHubClient(), Times.Once);
            ami60MinInterval.Verify(t => t.CloseEventHubClient(It.IsAny<EventHubClient>()), Times.Once);

            ami60MinInterval.VerifyAll();

            Assert.AreEqual(true, true);
        }
    }
}
