﻿using CE.AO.DataImport.BatchProcessWorker.Files;
using CE.AO.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.WindowsAzure.Storage.Blob;
using Moq;

namespace CE.AO.DataImport.BatchProcessWorker.Tests.UnitTests
{
    [TestClass]
    public class AdvanceAmiProcessFileTest
    {
        public TestContext TestContext { get; set; }

        /// <summary>
        /// Test Case for Initialize method
        /// </summary>
        [TestMethod()]
        [DeploymentItem("TestData\\AdvanceAmiProcessTestData.xml")]
        [DataSource("Microsoft.VisualStudio.TestTools.DataSource.XML",
            "|DataDirectory|\\AdvanceAmiProcessTestData.xml", "AdvAmi", DataAccessMethod.Sequential)]
        public void Initialize()
        {
            // Arrange
            var interval = TestContext.DataRow["interval"].ToString();
            var blobContainerNameAndClientId = TestContext.DataRow["blobMessage"].ToString();

            //var interval = "15min";
            //var blobContainerNameAndClientId = "EST_AdvAmi_87_15min_Test.csv:client87:87";


            string[] blobInfo = blobContainerNameAndClientId.Split(':');
            var advanceAmi =
                new Mock<AdvanceAmiProcessFile>(new LogModel {DisableLog = true}, blobInfo[0], blobInfo, blobInfo[1])
                {
                    CallBase = true
                };
            var cloudBlob = new Mock<ICloudBlob>();
            advanceAmi.Setup(p => p.GetBlockBlob(It.IsAny<string>(), It.IsAny<string>())).Returns(cloudBlob.Object);
            advanceAmi.Setup(p => p.ProcessAdvanceAmi(cloudBlob.Object, blobInfo[0], interval,blobInfo[1]));

            // Act
            advanceAmi.Object.Initialize(blobInfo[0], blobInfo[1], blobInfo[2]);

            // Assert
            advanceAmi.Verify(p => p.GetBlockBlob(It.IsAny<string>(), It.IsAny<string>()), Times.Once);
            advanceAmi.Verify(p => p.ProcessAdvanceAmi(cloudBlob.Object, blobInfo[0], interval, blobInfo[1]),Times.Once);
            advanceAmi.VerifyAll();
            
        }
    }
}

