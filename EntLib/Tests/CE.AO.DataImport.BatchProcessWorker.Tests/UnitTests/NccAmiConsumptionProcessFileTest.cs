﻿using System.Collections.Generic;
using CE.AO.DataImport.BatchProcessWorker.Files;
using CE.AO.Models;
using CE.AO.Models.RawFileModels;
using Microsoft.ServiceBus.Messaging;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.WindowsAzure.Storage.Blob;
using Moq;
using Enums = CE.AO.Utilities.Enums;

namespace CE.AO.DataImport.BatchProcessWorker.Tests.UnitTests
{
    [TestClass]
    public class NccAmiConsumptionProcessFileTest
    {
        /// <summary>
        /// Test Case for Initialize method
        /// </summary>
        [TestMethod]
        public void Initialize()
        {
            var nccAmiConsumption = new Mock<NccAmiConsumptionProcessFile>(new LogModel { DisableLog = true }) { CallBase = true };

            var cloudBlob = new Mock<ICloudBlob>();

            nccAmiConsumption.Setup(p => p.GetBlockBlob(It.IsAny<string>(), It.IsAny<string>())).Returns(cloudBlob.Object);
            nccAmiConsumption.Setup(p => p.Process<RawNccAmiReadingModel>(It.IsAny<ICloudBlob>(), It.IsAny<IDictionary<string, object>>(), It.IsAny<Enums.EventType>(), It.IsAny<EventHubClient>()));
            nccAmiConsumption.Setup(p => p.ArchieveBlob(It.IsAny<ICloudBlob>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<Enums.EventType>()));
            nccAmiConsumption.Setup(t => t.GetEventHubClient());
            nccAmiConsumption.Setup(t => t.CloseEventHubClient(It.IsAny<EventHubClient>()));

            nccAmiConsumption.Object.Initialize("NccCalcConsumptionncc_UnitTest_Tush.csv", "client87", "87");

            nccAmiConsumption.Verify(p => p.GetBlockBlob(It.IsAny<string>(), It.IsAny<string>()), Times.Once);
            nccAmiConsumption.Verify(p => p.Process<RawNccAmiReadingModel>(It.IsAny<ICloudBlob>(), It.IsAny<IDictionary<string, object>>(), It.IsAny<Enums.EventType>(), It.IsAny<EventHubClient>()), Times.Once);
            nccAmiConsumption.Verify(p => p.ArchieveBlob(It.IsAny<ICloudBlob>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<Enums.EventType>()), Times.Once);
            nccAmiConsumption.Verify(t => t.GetEventHubClient(), Times.Once);
            nccAmiConsumption.Verify(t => t.CloseEventHubClient(It.IsAny<EventHubClient>()), Times.Once);

            nccAmiConsumption.VerifyAll();

            Assert.AreEqual(true, true);
        }
    }
}
