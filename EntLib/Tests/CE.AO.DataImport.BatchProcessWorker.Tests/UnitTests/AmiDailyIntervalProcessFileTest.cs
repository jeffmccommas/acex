﻿using System.Collections.Generic;
using System.IO;
using System.Runtime.Remoting.Messaging;
using System.Text;
using CE.AO.DataImport.BatchProcessWorker.Files;
using CE.AO.Models;
using CE.AO.Models.RawFileModels;
using CE.AO.Utilities;
using Microsoft.ServiceBus.Messaging;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Blob;
using Moq;

namespace CE.AO.DataImport.BatchProcessWorker.Tests.UnitTests
{
    [TestClass]
    public class AmiDailyIntervalProcessFileTest
    {

        private Mock<AmiDailyIntervalProcessFile> _amiDailyInterval;
        private Mock<ICloudBlob> _icloudBlob;
        /// <summary>
        /// Test Case for Initialize method
        /// </summary>
        [TestInitialize]
        public void Initialize()
        {
            _amiDailyInterval = new Mock<AmiDailyIntervalProcessFile>(new LogModel { DisableLog = true })
            {
                CallBase = true
            };
            AutoMapperConfig.AutoMapperMappings();
            _icloudBlob = new Mock<ICloudBlob>();

          
            _amiDailyInterval.Setup(
                p =>
                    p.Process<RawAmiDailyIntervalModel>(It.IsAny<ICloudBlob>(), It.IsAny<IDictionary<string, object>>(),
                        It.IsAny<Enums.EventType>(), It.IsAny<EventHubClient>()));
            _amiDailyInterval.Setup(
                p =>
                    p.ArchieveBlob(It.IsAny<ICloudBlob>(), It.IsAny<string>(), It.IsAny<string>(),
                        It.IsAny<Enums.EventType>()));
        }


        /// <summary>
        /// Test Case for Initialize method
        /// </summary>
        [TestMethod]
        public void ProcessFile()
        {
            _icloudBlob.Setup(
              e =>
                  e.OpenRead(It.IsAny<AccessCondition>(), It.IsAny<BlobRequestOptions>(), It.IsAny<OperationContext>()))
              .Returns(GetStreamData);
            _amiDailyInterval.Setup(p => p.GetBlockBlob(It.IsAny<string>(), It.IsAny<string>()))
                .Returns(_icloudBlob.Object);
            _amiDailyInterval.Object.Initialize("EST_AMI_87_Daily_TestSample.csv", "client87", "87");

            _amiDailyInterval.Verify(p => p.GetBlockBlob(It.IsAny<string>(), It.IsAny<string>()), Times.Once);
            _amiDailyInterval.Verify(
                p =>
                    p.Process<RawAmiDailyIntervalModel>(It.IsAny<ICloudBlob>(), It.IsAny<IDictionary<string, object>>(),
                        It.IsAny<Enums.EventType>(), It.IsAny<EventHubClient>()), Times.Once);
            _amiDailyInterval.Verify(
                p =>
                    p.ArchieveBlob(It.IsAny<ICloudBlob>(), It.IsAny<string>(), It.IsAny<string>(),
                        It.IsAny<Enums.EventType>()), Times.Once);
            Assert.AreEqual(true, true);
        }

        /// <summary>
        /// Test Case for Initialize method
        /// </summary>
        [TestMethod]
        public void ErrorStringLog()
        {
            _icloudBlob.Setup(
            e =>
                e.OpenRead(It.IsAny<AccessCondition>(), It.IsAny<BlobRequestOptions>(), It.IsAny<OperationContext>()))
            .Returns(GetStreamData_ForError);
            _amiDailyInterval.Setup(p => p.GetBlockBlob(It.IsAny<string>(), It.IsAny<string>()))
                .Returns(_icloudBlob.Object);
            _amiDailyInterval.Object.Initialize("EST_AMI_87_Daily_TestSample.csv", "client87", "87");

            _amiDailyInterval.Verify(p => p.GetBlockBlob(It.IsAny<string>(), It.IsAny<string>()), Times.Once);
            _amiDailyInterval.Verify(
                p =>
                    p.ArchieveBlob(It.IsAny<ICloudBlob>(), It.IsAny<string>(), It.IsAny<string>(),
                        It.IsAny<Enums.EventType>()), Times.Once);
            Assert.AreEqual(true, true);
        }


        /// <summary>
        /// Test Case for Initialize method
        /// </summary>
        [TestMethod]
        public void ErrorStringLog_ClientIdUnEqual()
        {
            _icloudBlob.Setup(
            e =>
                e.OpenRead(It.IsAny<AccessCondition>(), It.IsAny<BlobRequestOptions>(), It.IsAny<OperationContext>()))
            .Returns(GetStreamData);
            _amiDailyInterval.Setup(p => p.GetBlockBlob(It.IsAny<string>(), It.IsAny<string>()))
                .Returns(_icloudBlob.Object);
            _amiDailyInterval.Object.Initialize("EST_AMI_87_Daily_TestSample.csv", "client87", "1");

            _amiDailyInterval.Verify(p => p.GetBlockBlob(It.IsAny<string>(), It.IsAny<string>()), Times.Once);
            _amiDailyInterval.Verify(
                p =>
                    p.ArchieveBlob(It.IsAny<ICloudBlob>(), It.IsAny<string>(), It.IsAny<string>(),
                        It.IsAny<Enums.EventType>()), Times.Once);
            Assert.AreEqual(true, true);
        }
        
        /// <summary>
        /// Test Case for Initialize method
        /// </summary>
        [TestMethod]
        public void ErrorStringLog_Client224()
        {
            _icloudBlob.Setup(
            e =>
                e.OpenRead(It.IsAny<AccessCondition>(), It.IsAny<BlobRequestOptions>(), It.IsAny<OperationContext>()))
            .Returns(GetStreamData_ClientId224);
            _amiDailyInterval.Setup(p => p.GetBlockBlob(It.IsAny<string>(), It.IsAny<string>()))
                .Returns(_icloudBlob.Object);
            _amiDailyInterval.Object.Initialize("EST_AMI_87_Daily_TestSample.csv", "client87", "224");

            _amiDailyInterval.Verify(p => p.GetBlockBlob(It.IsAny<string>(), It.IsAny<string>()), Times.Once);
            _amiDailyInterval.Verify(
                p =>
                    p.ArchieveBlob(It.IsAny<ICloudBlob>(), It.IsAny<string>(), It.IsAny<string>(),
                        It.IsAny<Enums.EventType>()), Times.Once);
            Assert.AreEqual(true, true);
        }

        private Stream GetStreamData()
        {
            string[] myheaders = {  "ClientId","MeterId","ServicePointId","UOMId","CommodityId","AccountNumber","TimeZone","Direction",
                            "ProjectedReadDate","VolumeFactor","TOUID","IntValue0000","TimeStamp" };
            string[] actualData = {
                          "87","MET900","SC500","3","3","ACC23","EST",null,"09/22/2016","500","4","24","09/22/2016"
            };
            string csvheaders = string.Join(",", myheaders);
            string csvActData = string.Join(",", actualData);
            var csv = new StringBuilder();
            csv.AppendLine(csvheaders);
            csv.AppendLine(csvActData);
            MemoryStream ms = new MemoryStream();
            var sw = new StreamWriter(ms, Encoding.UTF8);
            sw.Write(csv.ToString());
            sw.Flush();
            ms.Position = 0;
            return ms;
        }

        private Stream GetStreamData_ForError()
        {
            string[] myheaders = {  "ClientId","MeterId","ServicePointId","UOMId","CommodityId","AccountNumber","TimeZone","Direction",
                            "ProjectedReadDate","VolumeFactor","TOUID","IntValue0000","TimeStamp" };
            string[] actualData = {
                          "87","MET900","SC500","3","3","ACC23","EST","2","09/22/2016","500","4","24","09/22/2016"
            };
            string csvheaders = string.Join(",", myheaders);
            string csvActData = string.Join(",", actualData);
            var csv = new StringBuilder();
            csv.AppendLine(csvheaders);
            csv.AppendLine(csvActData);
            MemoryStream ms = new MemoryStream();
            var sw = new StreamWriter(ms, Encoding.UTF8);
            sw.Write(csv.ToString());
            sw.Flush();
            ms.Position = 0;
            return ms;
        }

        private Stream GetStreamData_ClientId224()
        {
            string[] myheaders = {  "ClientId","MeterId","ServicePointId","UOMId","CommodityId","AccountNumber","TimeZone","Direction",
                            "ProjectedReadDate","VolumeFactor","TOUID","IntValue0000","TimeStamp" };
            string[] actualData = {
                          "224","MET900","SC500","3","3","ACC23","EST","1","09/22/2016","500","4","24","09/22/2016"
            };
            string csvheaders = string.Join(",", myheaders);
            string csvActData = string.Join(",", actualData);
            var csv = new StringBuilder();
            csv.AppendLine(csvheaders);
            csv.AppendLine(csvActData);
            MemoryStream ms = new MemoryStream();
            var sw = new StreamWriter(ms, Encoding.UTF8);
            sw.Write(csv.ToString());
            sw.Flush();
            ms.Position = 0;
            return ms;
        }
        /// <summary>
        /// Cleanup Method To Release Log4Net Data Resources hold by Any Test Method
        /// </summary>
        [TestCleanup]
        public void CleanUpMethod()
        {
            CallContext.FreeNamedDataSlot("log4net.Util.LogicalThreadContextProperties");
        }


    }
}
