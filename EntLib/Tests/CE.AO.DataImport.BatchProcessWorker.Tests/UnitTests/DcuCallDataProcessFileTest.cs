﻿using System.Collections.Generic;
using CE.AO.DataImport.BatchProcessWorker.Files;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using CE.AO.Models;
using CE.AO.Models.RawFileModels;
using Microsoft.ServiceBus.Messaging;
using Microsoft.WindowsAzure.Storage.Blob;
using Enums = CE.AO.Utilities.Enums;

namespace CE.AO.DataImport.BatchProcessWorker.Tests.UnitTests
{
    [TestClass]
    public class DcuCallDataProcessFileTest
    {
        /// <summary>
        /// Test Case for Initialize method
        /// </summary>
        [TestMethod]
        public void Initialize()
        {
            var dcuCallDataProcessFileMock = new Mock<DcuCallDataProcessFile>(new LogModel { DisableLog = true }) { CallBase = true };
            var cloudBlobkBlob = new Mock<ICloudBlob>();

            dcuCallDataProcessFileMock.Setup(t => t.GetEventHubClient());
            dcuCallDataProcessFileMock.Setup(
                t =>
                    t.Process<RawDcuCallDataModel>(It.IsAny<ICloudBlob>(),
                        It.IsAny<IDictionary<string, object>>(), It.IsAny<Enums.EventType>(), It.IsAny<EventHubClient>()));
            dcuCallDataProcessFileMock.Setup(
                t =>
                    t.ArchieveBlob(It.IsAny<ICloudBlob>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<Enums.EventType>()));
            dcuCallDataProcessFileMock.Setup(t =>
                t.GetBlockBlob(It.IsAny<string>(), It.IsAny<string>())).Returns(cloudBlobkBlob.Object);
            dcuCallDataProcessFileMock.Setup(t => t.CloseEventHubClient(It.IsAny<EventHubClient>()));

            dcuCallDataProcessFileMock.Object.Initialize("dcucalldata.csv", "client87", "87");

            dcuCallDataProcessFileMock.Verify(t => t.GetEventHubClient(), Times.Once);
            dcuCallDataProcessFileMock.Verify(t =>
                    t.Process<RawDcuCallDataModel>(It.IsAny<ICloudBlob>(),
                        It.IsAny<IDictionary<string, object>>(), It.IsAny<Enums.EventType>(), It.IsAny<EventHubClient>()), Times.Once);
            dcuCallDataProcessFileMock.Verify(t =>
                    t.ArchieveBlob(It.IsAny<ICloudBlob>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<Enums.EventType>()), Times.Once);
            dcuCallDataProcessFileMock.Verify(t =>
                t.GetBlockBlob(It.IsAny<string>(), It.IsAny<string>()), Times.Once);
            dcuCallDataProcessFileMock.Verify(t => t.CloseEventHubClient(It.IsAny<EventHubClient>()), Times.Once);
            dcuCallDataProcessFileMock.VerifyAll();
            Assert.AreEqual(true, true);
        }
    }
}
