﻿using System.Collections.Generic;
using CE.AO.DataImport.BatchProcessWorker.Files;
using CE.AO.Models;
using CE.AO.Models.RawFileModels;
using Microsoft.ServiceBus.Messaging;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.WindowsAzure.Storage.Blob;
using Moq;
using Enums = CE.AO.Utilities.Enums;

namespace CE.AO.DataImport.BatchProcessWorker.Tests.UnitTests
{
    [TestClass]
    public class Ami15MinuteIntervalProcessFileTest
    {
        /// <summary>
        /// Test Case for Initialize method
        /// </summary>
        [TestMethod]
        public void Initialize()
        {
            var ami15MinInterval = new Mock<Ami15MinuteIntervalProcessFile>(new LogModel { DisableLog = true }) { CallBase = true };

            var cloudBlob = new Mock<ICloudBlob>();

            ami15MinInterval.Setup(p => p.GetBlockBlob(It.IsAny<string>(), It.IsAny<string>())).Returns(cloudBlob.Object);
            ami15MinInterval.Setup(p => p.Process<RawAmi15MinuteIntervalModel>(It.IsAny<ICloudBlob>(), It.IsAny<IDictionary<string, object>>(), It.IsAny<Enums.EventType>(), It.IsAny<EventHubClient>()));
            ami15MinInterval.Setup(p => p.ArchieveBlob(It.IsAny<ICloudBlob>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<Enums.EventType>()));
            ami15MinInterval.Setup(t => t.GetEventHubClient());
            ami15MinInterval.Setup(t => t.CloseEventHubClient(It.IsAny<EventHubClient>()));

            ami15MinInterval.Object.Initialize("EST_AMI_87_15min_13JuneuTushar-Test.csv", "client87", "87");

            ami15MinInterval.Verify(p => p.GetBlockBlob(It.IsAny<string>(), It.IsAny<string>()), Times.Once);
            ami15MinInterval.Verify(p => p.Process<RawAmi15MinuteIntervalModel>(It.IsAny<ICloudBlob>(), It.IsAny<IDictionary<string, object>>(), It.IsAny<Enums.EventType>(), It.IsAny<EventHubClient>()), Times.Once);
            ami15MinInterval.Verify(p => p.ArchieveBlob(It.IsAny<ICloudBlob>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<Enums.EventType>()), Times.Once);
            ami15MinInterval.Verify(t => t.GetEventHubClient(), Times.Once);
            ami15MinInterval.Verify(t => t.CloseEventHubClient(It.IsAny<EventHubClient>()), Times.Once);

            ami15MinInterval.VerifyAll();

            Assert.AreEqual(true, true);
        }
    }
}
