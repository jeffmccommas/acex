﻿using System;
using System.IO;
using CE.AO.DataImport.BatchProcessWorker.Files;
using CE.AO.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Blob;
using Moq;

namespace CE.AO.DataImport.BatchProcessWorker.Tests.IntegrationTests
{
    [TestClass]
    public class AdvanceAmiProcessFileTests
    {
        public TestContext TestContext { get; set; }

        /// <summary>
        /// Test Case for ProcessMidTermAmi method
        /// </summary>
        [TestMethod()]
        [DeploymentItem("TestData")]
        [DataSource("Microsoft.VisualStudio.TestTools.DataSource.XML",
            "|DataDirectory|\\AdvanceAmiProcessIntegrationTestData.xml", "Ami", DataAccessMethod.Sequential)]
        public void ProcessAdvanceAmi()
        {
            // Arrange
            var interval = TestContext.DataRow["interval"].ToString();
            var blobContainerNameAndClientId = TestContext.DataRow["blobMessage"].ToString();
            var expected = Convert.ToBoolean(TestContext.DataRow["result"].ToString());

            //var interval = "15min";
            //var blobContainerNameAndClientId = "EST_AdvAmi_87_15min_Test.csv:client87:87";


            string[] blobInfo = blobContainerNameAndClientId.Split(':');
            var advAmiProcess =
                new Mock<AdvanceAmiProcessFile>(new LogModel { DisableLog = true, ClientId = blobInfo[2]}, blobInfo[0], blobInfo, blobInfo[1]) {
                    CallBase = true
                };
            var cloudBlob = new Mock<ICloudBlob>(); //(MockBehavior.Loose, new Uri(@"\EST_Ami_174_60min_Test.csv"));
            cloudBlob.Setup(
                    e =>
                        e.OpenRead(It.IsAny<AccessCondition>(), It.IsAny<BlobRequestOptions>(), It.IsAny<OperationContext>()))
                .Returns(ReadFileStream(blobInfo[0]));
            advAmiProcess.Setup(p => p.ArchieveBlob(cloudBlob.Object, blobInfo[0], blobInfo[1],
                It.IsAny<Utilities.Enums.EventType>()));
            advAmiProcess.Setup(p => p.ArchiveLogFiles(interval, blobInfo[1])).Verifiable();

            // Act
            var result = advAmiProcess.Object.ProcessAdvanceAmi(cloudBlob.Object, blobInfo[0], interval, blobInfo[1]);
            


            // Assert
            if (expected) {
                Assert.IsTrue(result);
                advAmiProcess.Verify(p => p.ArchieveBlob(cloudBlob.Object, blobInfo[0], blobInfo[1],
                    It.IsAny<Utilities.Enums.EventType>()), Times.Once);
            }
            else
                Assert.IsFalse(result);
            cloudBlob.Verify(p => p.OpenRead(It.IsAny<AccessCondition>(), It.IsAny<BlobRequestOptions>(), It.IsAny<OperationContext>()));
            advAmiProcess.Verify(p => p.ArchiveLogFiles(interval, blobInfo[1]), Times.Once);
            
        }

        private MemoryStream ReadFileStream(string filename)
        {
            MemoryStream ms = new MemoryStream();
            var file = File.OpenRead($"{filename}");
            ms.Position = 0;
            file.CopyTo(ms);
            file.Flush();
            ms.Position = 0;
            return ms;
        }
    }
}
