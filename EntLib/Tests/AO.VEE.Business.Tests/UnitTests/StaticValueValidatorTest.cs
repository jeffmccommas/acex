﻿using System;
using System.Collections.Generic;
using AO.VEE.Business.Contracts;
using AO.VEE.DTO;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace AO.VEE.Business.Tests.UnitTests
{
    /// <summary>
    /// This class contains unit test cases for StaticValueValidator class
    /// ValidateForValidIntervals() method is for valid data intervals check and ValidateForInvalidIntervals() is for invalid data interval check
    /// </summary>
    [TestClass]
  public class StaticValueValidatorTest
    {
        private IVeeValidator _veeValidator;

        /// <summary>
        /// Test Case Init
        /// </summary>
        [TestInitialize]
        public void Init()
        {
            _veeValidator = new StaticValueValidator(4);
        }


        /// <summary>
        /// Test case for Valid Data invervals, as it contains NO continuous static values.  
        /// </summary>
        [TestMethod]
        public void ValidateForValidIntervals()
        {
            var validatorSettingsDto = new ValidatorSettingsDto
            {
                Required = true,
                IntervalThreshold = 2,
                ConsumptionThreshold = 0.5
            };
            var veeConfigurationsDto = new VeeConfigurationsDto
            {
                StaticCheck = validatorSettingsDto
            };
            List<VeeConsumptionByMeterDto> veeConsumptionByMeterDto = new List<VeeConsumptionByMeterDto>
            {
                new VeeConsumptionByMeterDto
                {
                    AmiTimeStamp = new DateTime(2016, 09, 23, 00, 00, 00),
                    IsStatic = false,
                    StandardUOMConsumption = 1
                },
                new VeeConsumptionByMeterDto
                {
                    AmiTimeStamp = new DateTime(2016, 09, 23, 00, 15, 00),
                    IsStatic = false,
                    StandardUOMConsumption = 5
                },
                new VeeConsumptionByMeterDto
                {
                    AmiTimeStamp = new DateTime(2016, 09, 23, 00, 30, 00),
                    IsStatic = false,
                    StandardUOMConsumption = 2
                },
                new VeeConsumptionByMeterDto
                {
                    AmiTimeStamp = new DateTime(2016, 09, 23, 00, 45, 00),
                    IsStatic = false,
                    StandardUOMConsumption = 9
                },
                new VeeConsumptionByMeterDto
                {
                    AmiTimeStamp = new DateTime(2016, 09, 23, 00, 45, 00),
                    IsStatic = false,
                    StandardUOMConsumption = 4
                }
            };

            var result = _veeValidator.Validate(veeConsumptionByMeterDto, veeConfigurationsDto);
            Assert.IsTrue(result);
        }


        /// <summary>
        /// Test case for Invalid Data invervals, as it contains continuous static values.  
        /// </summary>
        [TestMethod]
        public void ValidateForInvalidIntervals()
        {
            var validatorSettingsDto = new ValidatorSettingsDto
            {
                Required = true,
                IntervalThreshold = 2,
                ConsumptionThreshold = 0.5
            };
            var veeConfigurationsDto = new VeeConfigurationsDto
            {
                StaticCheck = validatorSettingsDto
            };
            List<VeeConsumptionByMeterDto> veeConsumptionByMeterDto = new List<VeeConsumptionByMeterDto>
            {
                new VeeConsumptionByMeterDto()
                {
                    AmiTimeStamp = new DateTime(2016, 09, 23, 00, 00, 00),
                    IsStatic = false,
                    StandardUOMConsumption = 5
                },
                new VeeConsumptionByMeterDto
                {
                    AmiTimeStamp = new DateTime(2016, 09, 23, 00, 15, 00),
                    IsStatic = false,
                    StandardUOMConsumption = 5
                },
                new VeeConsumptionByMeterDto
                {
                    AmiTimeStamp = new DateTime(2016, 09, 23, 00, 30, 00),
                    IsStatic = false,
                    StandardUOMConsumption = 2
                },
                new VeeConsumptionByMeterDto
                {
                    AmiTimeStamp = new DateTime(2016, 09, 23, 00, 45, 00),
                    IsStatic = false,
                    StandardUOMConsumption = 4
                },
                new VeeConsumptionByMeterDto
                {
                    AmiTimeStamp = new DateTime(2016, 09, 23, 00, 45, 00),
                    IsStatic = false,
                    StandardUOMConsumption = 4
                }
            };

            var result = _veeValidator.Validate(veeConsumptionByMeterDto, veeConfigurationsDto);
            Assert.IsFalse(result);
        }
    }
}
