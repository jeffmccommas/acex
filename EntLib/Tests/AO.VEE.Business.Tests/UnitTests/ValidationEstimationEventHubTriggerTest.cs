﻿using System.Collections.Generic;
using AO.BusinessContracts;
using AO.VEE.DTO;
using CE.AO.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace AO.VEE.Business.Tests.UnitTests
{
    /// <summary>
    /// This class contains the unit test methods ofValidationEstimationEventHubTrigger.cs class.
    /// </summary>
    [TestClass]
    public class ValidationEstimationEventHubTriggerTest : TestBase
    {
        private ValidationEstimationEventHubTrigger _validationEstimationEventHubTrigger;
        private Mock<IMeterHistoryByMeter> _meterHistoryByMeterMock;

        /// <summary>
        /// Test Case Init
        /// </summary>
        [TestInitialize]
        public void Init()
        {
			_meterHistoryByMeterMock = new Mock<IMeterHistoryByMeter>();
			_validationEstimationEventHubTrigger = new ValidationEstimationEventHubTrigger(new LogModel(), _meterHistoryByMeterMock.Object);
           
        }

        /// <summary>
        /// Unit test method for Trigger method of ValidationEstimationEventHubTrigger.cs class with active meters
        /// </summary>
        [TestMethod]
        public void Trigger()
        {
            var veeClientMeterDto = new VeeValidationEstimationTriggerDto
            {
                ClientId = 87,
                AmiDate = null
            };
            byte[] token = null;
            IList<MeterHistoryByMeterModel> clientMeterList = new List<MeterHistoryByMeterModel>();
            clientMeterList.Add(new MeterHistoryByMeterModel { ClientId = 87, MeterId = 1, Active = true});
            clientMeterList.Add(new MeterHistoryByMeterModel { ClientId = 87, MeterId = 2, Active = true});
            clientMeterList.Add(new MeterHistoryByMeterModel { ClientId = 87, MeterId = 3, Active = true });
			_meterHistoryByMeterMock.Setup(t => t.GetMeterHistoryByMeterList(It.IsAny<int>(), ref token, It.IsAny<int>())).Returns(clientMeterList).Verifiable();
            _validationEstimationEventHubTrigger.MeterHistoryByMeter = _meterHistoryByMeterMock.Object;
            _validationEstimationEventHubTrigger.Trigger(veeClientMeterDto);
			_meterHistoryByMeterMock.Verify(t => t.GetMeterHistoryByMeterList(It.IsAny<int>(), ref token, It.IsAny<int>()), Times.Once);
			_meterHistoryByMeterMock.VerifyAll();
        }

        /// <summary>
        /// Unit test method for Trigger method of ValidationEstimationEventHubTrigger.cs class with Inactive meters.
        /// </summary>
        [TestMethod]
        public void TriggerInactiveMeters()
        {
            var veeClientMeterDto = new VeeValidationEstimationTriggerDto
            {
                ClientId = 87,
                AmiDate = null
            };
            byte[] token = null;
            IList<MeterHistoryByMeterModel> clientMeterList = new List<MeterHistoryByMeterModel>();
            clientMeterList.Add(new MeterHistoryByMeterModel { ClientId = 87, MeterId = 1, Active = false });
            clientMeterList.Add(new MeterHistoryByMeterModel { ClientId = 87, MeterId = 2, Active = false });
            clientMeterList.Add(new MeterHistoryByMeterModel { ClientId = 87, MeterId = 3, Active = false });
            _meterHistoryByMeterMock.Setup(t => t.GetMeterHistoryByMeterList(It.IsAny<int>(), ref token, It.IsAny<int>())).Returns(clientMeterList).Verifiable();
            _validationEstimationEventHubTrigger.MeterHistoryByMeter = _meterHistoryByMeterMock.Object;
            _validationEstimationEventHubTrigger.Trigger(veeClientMeterDto);
            _meterHistoryByMeterMock.Verify(t => t.GetMeterHistoryByMeterList(It.IsAny<int>(), ref token, It.IsAny<int>()), Times.Once);
            _meterHistoryByMeterMock.VerifyAll();
        }
    }
}
