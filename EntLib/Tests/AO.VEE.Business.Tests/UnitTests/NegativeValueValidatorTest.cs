﻿using System;
using System.Collections.Generic;
using AO.VEE.Business.Contracts;
using AO.VEE.DTO;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace AO.VEE.Business.Tests.UnitTests
{
    /// <summary>
    /// This class contains unit test cases for NegativeValueValidator class
    /// </summary>
    [TestClass]
    public class NegativeValueValidatorTest
    {
        private IVeeValidator _veeValidator;

        /// <summary>
        /// Test Case Init
        /// </summary>
        [TestInitialize]
        public void Init()
        {
            _veeValidator = new NegativeValueValidator(2);
        }

        /// <summary>
        /// Test case for Validate method
        /// </summary>
        [TestMethod]
        public void Validate()
        {

            var validatorSettingsDto = new ValidatorSettingsDto
            {
                Required = true,
                IntervalThreshold = 2,
                ConsumptionThreshold = -1
            };
            var veeConfigurationsDto = new VeeConfigurationsDto
            {
                NegativeValueCheck = validatorSettingsDto
            };
            List<VeeConsumptionByMeterDto> veeConsumptionByMeterDto = new List<VeeConsumptionByMeterDto>
            {
                new VeeConsumptionByMeterDto()
                {
                    AmiTimeStamp = new DateTime(2016, 09, 23, 00, 00, 00),
                    IsNegative = false,
                    StandardUOMConsumption = 5

                },
                new VeeConsumptionByMeterDto()
                {
                    AmiTimeStamp = new DateTime(2016, 09, 23, 00, 15, 00),
                    IsNegative = false,
                    StandardUOMConsumption = -4

                },
                new VeeConsumptionByMeterDto()
                {
                    AmiTimeStamp = new DateTime(2016, 09, 23, 00, 30, 00),
                    IsNegative = false,
                    StandardUOMConsumption = -3

                },
                new VeeConsumptionByMeterDto()
                {
                    AmiTimeStamp = new DateTime(2016, 09, 23, 00, 30, 00),
                    IsNegative = false,
                    StandardUOMConsumption = -0.5

                },
                new VeeConsumptionByMeterDto()
                {
                    AmiTimeStamp = new DateTime(2016, 09, 23, 00, 45, 00),
                    IsNegative = false,
                    StandardUOMConsumption = -1
                },
                new VeeConsumptionByMeterDto()
                {
                    AmiTimeStamp = new DateTime(2016, 09, 23, 00, 45, 00),
                    IsNegative = false,
                    StandardUOMConsumption = 4
                }
            };

            var result = _veeValidator.Validate(veeConsumptionByMeterDto, veeConfigurationsDto);
            Assert.IsFalse(result);
        }
    }
}