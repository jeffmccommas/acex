﻿using System;
using System.Collections.Generic;
using AO.VEE.Business.Contracts;
using AO.VEE.DTO;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace AO.VEE.Business.Tests.UnitTests
{
    /// <summary>
    /// This class contains the unit test methods for the methods of SpikeValueValidator.cs class.
    /// </summary>
    [TestClass]
    public class SpikeValueValidatorTest : TestBase
    {
        private IVeeValidator _veeValidatorMock;

        /// <summary>
        /// Test Case Init
        /// </summary>
        [TestInitialize]
        public void Init()
        {
            _veeValidatorMock = new SpikeValueValidator(5);
        }

        /// <summary>
        /// Unit test method for Validate method of SpikeValueValidator.cs class
        /// </summary>
        [TestMethod]
        public void Validate()
        {
            string date = "09/22/2016 00:00";
            List<VeeConsumptionByMeterDto> veeConsumptionByMeterList = new List<VeeConsumptionByMeterDto>();

            for (int i = 1; i <= 24; i++)
            {
                VeeConsumptionByMeterDto veeConsumptionByMeterDto = new VeeConsumptionByMeterDto()
                {

                    ClientId = 87,
                    MeterId = "M001",
                    AmiTimeStamp = DateTime.Parse(date).AddHours(i-1),
                    AccountNumber = "AC001",
                    CommodityId = 3,
                    RawConsumption = 102,
                    Direction = null,
                    EstimatedConsumption = 0,
                    EditedConsumption = 0,
                    StandardUOMConsumption = 102,
                    EstimationMethod = 0,
                    IsApproved = null,
                    IsEdited = null,
                    IsEstimated = null,
                    IsMetered = null,
                    IsMissing = null,
                    IsNegative = null,
                    IsZero = null,
                    IsStatic = null,
                    IsSpike = null,
                    Timezone = "EST",
                    TransponderId = "",
                    TransponderPort = "",
                    UOMId = 6
                };
                GetValue(i, veeConsumptionByMeterDto);
                veeConsumptionByMeterList.Add(veeConsumptionByMeterDto);
            }

            VeeConfigurationsDto veeConfigurations = new VeeConfigurationsDto()
            {
                SpikeCheck = new ValidatorSettingsDto() { Required = true, IntervalThreshold = 3 }
            };

            _veeValidatorMock.Validate(veeConsumptionByMeterList, veeConfigurations);

        }

        /// <summary>
        /// This method is used to set the StandardUOMConsumption Value. It is called from validate test method.
        /// </summary>
        /// <param name="i"></param>
        /// <param name="veeConsumptionByMeterDto"></param>
        private static void GetValue(int i, VeeConsumptionByMeterDto veeConsumptionByMeterDto)
        {
            switch (i)
            {
                case 1:
                    veeConsumptionByMeterDto.RawConsumption = 102;
                    veeConsumptionByMeterDto.StandardUOMConsumption = 102;
                    break;           
                case 2:
                    veeConsumptionByMeterDto.RawConsumption = 104;
                    veeConsumptionByMeterDto.StandardUOMConsumption = 104;
                    break;          
                case 3:
                    veeConsumptionByMeterDto.RawConsumption = 106;
                    veeConsumptionByMeterDto.StandardUOMConsumption = 106;
                    break;          
                case 4:
                    veeConsumptionByMeterDto.RawConsumption = 108;
                    veeConsumptionByMeterDto.StandardUOMConsumption = 108;
                    break;           
                case 5:
                    veeConsumptionByMeterDto.RawConsumption = 104;
                    veeConsumptionByMeterDto.StandardUOMConsumption = 104;
                    break;          
                case 6:
                    veeConsumptionByMeterDto.RawConsumption = 112;
                    veeConsumptionByMeterDto.StandardUOMConsumption = 112;
                    break;          
                case 7:
                    veeConsumptionByMeterDto.RawConsumption = 108;
                    veeConsumptionByMeterDto.StandardUOMConsumption = 108;
                    break;          
                case 8:
                    veeConsumptionByMeterDto.RawConsumption = 162;
                    veeConsumptionByMeterDto.StandardUOMConsumption = 162;
                    break;          
                case 9:
                    veeConsumptionByMeterDto.RawConsumption = 104;
                    veeConsumptionByMeterDto.StandardUOMConsumption = 104;
                    break;           
                case 10:
                    veeConsumptionByMeterDto.RawConsumption = 118;
                    veeConsumptionByMeterDto.StandardUOMConsumption = 118;
                    break;           
                case 11:
                    veeConsumptionByMeterDto.RawConsumption = 120;
                    veeConsumptionByMeterDto.StandardUOMConsumption = 120;
                    break;          
                case 12:
                    veeConsumptionByMeterDto.RawConsumption = 108;
                    veeConsumptionByMeterDto.StandardUOMConsumption = 108;
                    break;          
                case 13:
                    veeConsumptionByMeterDto.RawConsumption = 118;
                    veeConsumptionByMeterDto.StandardUOMConsumption = 118;
                    break;           
                case 14:
                    veeConsumptionByMeterDto.RawConsumption = 104;
                    veeConsumptionByMeterDto.StandardUOMConsumption = 104;
                    break;           
                case 15:
                    veeConsumptionByMeterDto.RawConsumption = 106;
                    veeConsumptionByMeterDto.StandardUOMConsumption = 106;
                    break;          
                case 16:
                    veeConsumptionByMeterDto.RawConsumption = 145;
                    veeConsumptionByMeterDto.StandardUOMConsumption = 145;
                    break;          
                case 17:
                    veeConsumptionByMeterDto.RawConsumption = 120;
                    veeConsumptionByMeterDto.StandardUOMConsumption = 120;
                    break;         
                case 18:
                    veeConsumptionByMeterDto.RawConsumption = 108;
                    veeConsumptionByMeterDto.StandardUOMConsumption = 108;
                    break;           
                case 19:
                    veeConsumptionByMeterDto.RawConsumption = 104;
                    veeConsumptionByMeterDto.StandardUOMConsumption = 104;
                    break;          
                case 20:
                    veeConsumptionByMeterDto.RawConsumption = 112;
                    veeConsumptionByMeterDto.StandardUOMConsumption = 112;
                    break;           
                case 21:
                    veeConsumptionByMeterDto.RawConsumption = 108;
                    veeConsumptionByMeterDto.StandardUOMConsumption = 108;
                    break;         
                case 22:
                    veeConsumptionByMeterDto.RawConsumption = 159;
                    veeConsumptionByMeterDto.StandardUOMConsumption = 159;
                    break;           
                case 23:
                    veeConsumptionByMeterDto.RawConsumption = 104;
                    veeConsumptionByMeterDto.StandardUOMConsumption = 104;
                    break;           
                case 24:
                    veeConsumptionByMeterDto.RawConsumption = 140;
                    veeConsumptionByMeterDto.StandardUOMConsumption = 140;
                    break;
            }
        }

    }
}
