﻿using System;
using System.Collections.Generic;
using System.Runtime.Remoting.Messaging;
using AO.BusinessContracts;
using AO.VEE.Business.Contracts;
using AO.VEE.DataAccess.Contracts;
using AO.VEE.DTO;
using CE.AO.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace AO.VEE.Business.Tests.UnitTests
{
    /// <summary>
    /// Unit test cases for VeeFacade class
    /// </summary>
	[TestClass]
	public class VeeFacadeTest : TestBase
	{
		private VeeFacade _veeFacade;
		private Mock<IVeeStagedClientMeterByDateRepository> _veeStagedClientMeterByDateMock;
		private Mock<IVeeConsumptionByMeterRepository> _veeConsumptionByMeterMock;
		private Mock<ITallAMI> _tallAmiMock;
		private IList<IVeeValidator> _validatorsMock;
		private Mock<IVeeEstimatorFacade> _veeEstimatorFacadeMock;
	    private Mock<IClientConfigFacade> _clientConfigFacadeMock;
	    private Mock<IVeeValidator> _missingValidatorMock;
        private Mock<LogModel> _logModel;
        /// <summary>
        /// Test Case Init
        /// </summary>
        [TestInitialize]
		public void Init()
		{
			_veeStagedClientMeterByDateMock = new Mock<IVeeStagedClientMeterByDateRepository>();
			_veeConsumptionByMeterMock = new Mock<IVeeConsumptionByMeterRepository>();
			_tallAmiMock = new Mock<ITallAMI>();
            _veeEstimatorFacadeMock = new Mock<IVeeEstimatorFacade>();
            _clientConfigFacadeMock = new Mock<IClientConfigFacade>();
            _missingValidatorMock = new Mock<IVeeValidator>();
            _validatorsMock = new List<IVeeValidator>
            {
                _missingValidatorMock.Object
            };
            _logModel = new Mock<LogModel>();
            _veeFacade = new VeeFacade(new Lazy<IVeeStagedClientMeterByDateRepository>(() => _veeStagedClientMeterByDateMock.Object), new Lazy<IList<IVeeValidator>>(() => _validatorsMock),
				new Lazy<IClientConfigFacade>(() => _clientConfigFacadeMock.Object),
				new Lazy<ITallAMI>(() => _tallAmiMock.Object),
				new Lazy<IVeeEstimatorFacade>(() => _veeEstimatorFacadeMock.Object), new Lazy<IVeeConsumptionByMeterRepository>(() => _veeConsumptionByMeterMock.Object), _logModel.Object);
		}

		/// <summary>
		/// Test case for insert of MeterRead model
		/// Test case for InsertOrMergeMeterReadsAsync method in MeterReadFacade class
		/// </summary>
		[TestMethod]
		public void InsertOrMergeMeterReadsAsync()
		{
			var veeStagedClientMeterByDateDto = new VeeStagedClientMeterByDateDto
			{
				ClientId = 87,
				MeterId = "M202",
				AmiDate = DateTime.Now.Date
			};

            var clientSettings = new ClientSettings
            {
                IsVeeSubscribed = true
            };

		    _clientConfigFacadeMock.Setup(t => t.GetClientSettings(It.IsAny<int>())).Returns(clientSettings).Verifiable();
			_veeStagedClientMeterByDateMock.Setup(t => t.InsertOrMergeVeeStagedDataAsync(It.IsAny<VeeStagedClientMeterByDateDto>())).Verifiable();

			_veeFacade.InsertOrMergeVeeStagedDataAsync(veeStagedClientMeterByDateDto);

            _clientConfigFacadeMock.Verify(t => t.GetClientSettings(It.IsAny<int>()), Times.Once);
            _veeStagedClientMeterByDateMock.Verify(t => t.InsertOrMergeVeeStagedDataAsync(It.IsAny<VeeStagedClientMeterByDateDto>()), Times.Once);
			MockRepository.VerifyAll();
		}

        /// <summary>
        /// Unit test for ExecuteValidationsAndEstimations method
        /// </summary>
        [TestMethod]
	    public void ExecuteValidationsAndEstimations()
	    {
	        var veeValidationEstimationTriggerDto = new VeeValidationEstimationTriggerDto
	        {
                ClientId = 87,
                MeterId = "M202"
	        };

            var validatorSettingsDto = new ValidatorSettingsDto
            {
                Required = true
            };

            var veeConfigurationsDto = new VeeConfigurationsDto
            {
                MissingIntervalCheck = validatorSettingsDto
            };
            
            var veeStagedClientMeterByDateDtoList = new List<VeeStagedClientMeterByDateDto>
            {
                new VeeStagedClientMeterByDateDto()
                {
                    AmiDate = DateTime.Now,
                    ClientId = 87,
                    MeterId = "M202"
                }
            };
            var veeConsumptionByMeterDto = new VeeConsumptionByMeterDto
            {
                AmiTimeStamp = DateTime.UtcNow
            };

            List<TallAmiModel> tallAmiModelList = GetListTallAmiModel();

            _clientConfigFacadeMock.Setup(t => t.GetClientVeeSettings(It.IsAny<int>())).Returns(veeConfigurationsDto).Verifiable();

            _veeStagedClientMeterByDateMock.Setup(t => t.GetVeeStagedDataList(It.IsAny<int>(), It.IsAny<string>()))
	            .Returns(veeStagedClientMeterByDateDtoList);

	        _veeConsumptionByMeterMock.Setup(t => t.GetLastVeeConsumptionByMeter(It.IsAny<int>(), It.IsAny<string>()))
	            .Returns(veeConsumptionByMeterDto);

	        _tallAmiMock.Setup(
	            t => t.GetAmiList(It.IsAny<DateTime>(), It.IsAny<DateTime>(), It.IsAny<string>(), It.IsAny<int>()))
	            .Returns(tallAmiModelList).Verifiable();

            _missingValidatorMock.Setup(
                t => t.Validate(It.IsAny<List<VeeConsumptionByMeterDto>>(), It.IsAny<VeeConfigurationsDto>()))
                .Returns(false);    
            

	        _veeEstimatorFacadeMock.Setup(t => t.Estimate(It.IsAny<List<VeeConsumptionByMeterDto>>(), It.IsAny<Type>()));

	        _veeConsumptionByMeterMock.Setup(
	            t => t.InsertOrMergeBatchVeeConsumptionByMeterAsync(It.IsAny<List<VeeConsumptionByMeterDto>>())).Verifiable();

	        _veeStagedClientMeterByDateMock.Setup(t => t.DeleteVeeStagedData(It.IsAny<VeeStagedClientMeterByDateDto>())).Verifiable();

            _veeFacade.ExecuteValidationsAndEstimations(veeValidationEstimationTriggerDto);

            _clientConfigFacadeMock.Verify(t => t.GetClientVeeSettings(It.IsAny<int>()),Times.Once);

            _veeConsumptionByMeterMock.Verify(t => t.InsertOrMergeBatchVeeConsumptionByMeterAsync(It.IsAny<List<VeeConsumptionByMeterDto>>()),Times.Once);

            _veeStagedClientMeterByDateMock.Verify(t => t.DeleteVeeStagedData(It.IsAny<VeeStagedClientMeterByDateDto>()),Times.Once);

            _tallAmiMock.Verify(t => t.GetAmiList(It.IsAny<DateTime>(), It.IsAny<DateTime>(), It.IsAny<string>(), It.IsAny<int>()), Times.Once);

            MockRepository.VerifyAll();
        }

        /// <summary>
        /// To get list of TallAmiModel model
        /// </summary>
        /// <returns>List of TallAmiModel model</returns>
        private List<TallAmiModel> GetListTallAmiModel()
        {
            return new List<TallAmiModel>
            {
                new TallAmiModel()
                {
                    ClientId = 87,
                    AmiTimeStamp = DateTime.Now.Date,
                    Consumption = 10,
                    StandardUOMConsumption = 10,
                    IntervalType = 60
                },
                new TallAmiModel()
                {
                    ClientId = 87,
                    AmiTimeStamp = DateTime.Now.Date.Add(new TimeSpan(0,1,0,0)),
                    Consumption = 10,
                    StandardUOMConsumption = 10,
                    IntervalType = 60
                }
            };
        }

        /// <summary>
        /// Cleanup Method To Release Log4Net Data Resources hold by Any Test Method
        /// </summary>
        [TestCleanup]
        public void CleanUpMethod()
        {
            CallContext.FreeNamedDataSlot("log4net.Util.LogicalThreadContextProperties");
        }
    }
}