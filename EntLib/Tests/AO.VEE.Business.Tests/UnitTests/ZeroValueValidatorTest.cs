﻿using System;
using System.Collections.Generic;
using AO.VEE.Business.Contracts;
using AO.VEE.DTO;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace AO.VEE.Business.Tests.UnitTests
{
    /// <summary>
    /// This class contains unit test cases for ZeroValueValidator class
    /// </summary>
    [TestClass]
    public class ZeroValueValidatorTest
    {
        private IVeeValidator _veeValidator;

        /// <summary>
        /// Test Case Init
        /// </summary>
        [TestInitialize]
        public void Init()
        {
            _veeValidator = new ZeroValueValidator(3);
        }

        /// <summary>
        /// Test case for Validate method
        /// </summary>
        [TestMethod]
        public void Validate()
        {
            ValidatorSettingsDto validatorSettingsDto = new ValidatorSettingsDto
            {
                Required = true,
                IntervalThreshold = 2,
                ConsumptionThreshold = 0.5
            };
            VeeConfigurationsDto veeConfigurationsDto = new VeeConfigurationsDto
            {
                ZeroCheck = validatorSettingsDto
            };
            List<VeeConsumptionByMeterDto> veeConsumptionByMeterDto = new List<VeeConsumptionByMeterDto>
            {
                new VeeConsumptionByMeterDto()
                {
                    AmiTimeStamp = new DateTime(2016, 09, 23, 00, 00, 00),
                    IsZero = false,
                    StandardUOMConsumption = 0.2
                },
                new VeeConsumptionByMeterDto()
                {
                    AmiTimeStamp = new DateTime(2016, 09, 23, 00, 15, 00),
                    IsZero = false,
                    StandardUOMConsumption = 0.3
                },
                new VeeConsumptionByMeterDto()
                {
                    AmiTimeStamp = new DateTime(2016, 09, 23, 00, 30, 00),
                    IsZero = false,
                    StandardUOMConsumption = 0
                },
                new VeeConsumptionByMeterDto()
                {
                    AmiTimeStamp = new DateTime(2016, 09, 23, 00, 45, 00),
                    IsZero = false,
                    StandardUOMConsumption = 3
                }
            };

            var result = _veeValidator.Validate(veeConsumptionByMeterDto, veeConfigurationsDto);
            Assert.IsFalse(result);
        }
    }
}