﻿using AO.BusinessContracts;
using AO.VEE.DTO;
using Microsoft.Practices.Unity;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace AO.VEE.Business.Tests.IntegrationTests
{
    /// <summary>
    /// This class contains the integration test methods of ValidationEstimationEventHubTrigger.cs class.
    /// </summary>
    [TestClass]
    public class ValidationEstimationEventHubTriggerTest : TestBase
    {
        private IProcessTrigger<VeeValidationEstimationTriggerDto> _veeTrigger;

        /// <summary>
        /// Test Case Init
        /// </summary>
        [TestInitialize]
        public void Init()
        {
            _veeTrigger = UnityContainer.Resolve<IProcessTrigger<VeeValidationEstimationTriggerDto>>();
        }

        /// <summary>
        /// Integration test method for Trigger method of ValidationEstimationEventHubTrigger.cs class
        /// </summary>
        [TestMethod]
        public void Trigger()
        {
            var veeClientMeterDto = new VeeValidationEstimationTriggerDto
            {
                ClientId = 87,
                AmiDate = null
            };
            _veeTrigger.Trigger(veeClientMeterDto);
        }
    }
}
