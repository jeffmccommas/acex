﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using AO.VEE.DataAccess.Contracts;
using AO.VEE.DTO;
using Microsoft.Practices.Unity;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace AO.VEE.Business.Tests.IntegrationTests
{
	/// <summary>
	/// Test class for VeeStagedClientMeterByDateRepository
	/// </summary>
	[TestClass]
	public class VeeStagedClientMeterByDateRepositoryTest : TestBase
	{
		private IVeeStagedClientMeterByDateRepository _veeStagedClientMeterByDate;

		/// <summary>
		/// Test Case Init
		/// </summary>
		[TestInitialize]
		public void Init()
		{
			_veeStagedClientMeterByDate = UnityContainer.Resolve<IVeeStagedClientMeterByDateRepository>();
		}

		/// <summary>
		/// Test case for InsertOrMergeVeeStagedDataAsync method in VeeStagedClientMeterByDate class
		/// </summary>
		[TestMethod]
		public void InsertOrMergeVeeStagedDataAsync()
		{
			var veeStagedData = new VeeStagedClientMeterByDateDto()
				{
					ClientId = 87,
					MeterId = "MET900",
					AmiDate = DateTime.Now.Date
			};
			Task.Run(() => _veeStagedClientMeterByDate.InsertOrMergeVeeStagedDataAsync(veeStagedData)).Wait();
			Assert.AreEqual(true, true);
		}

		/// <summary>
		/// GetVeeStagedDataList test case.
		/// </summary>
		[TestMethod]
		public void GetVeeStagedDataList()
		{
			IList<VeeStagedClientMeterByDateDto> result = _veeStagedClientMeterByDate.GetVeeStagedDataList(87, "MET900");
			if (result != null) Assert.AreEqual(result[0].ClientId, 87);
			else Assert.AreEqual(null, null);
		}

		/// <summary>
		/// Test case for DeleteVeeStagedDataAsync method in VeeStagedClientMeterByDate class
		/// </summary>
		[TestMethod]
		public void DeleteVeeStagedDataAsync()
		{
			var veeStagedData = new VeeStagedClientMeterByDateDto()
			{
				ClientId = 87,
				MeterId = "MET900",
				AmiDate = DateTime.Now.Date
			};
			_veeStagedClientMeterByDate.DeleteVeeStagedData(veeStagedData);
			Assert.AreEqual(true, true);
		}
	}
}
