﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using AO.VEE.DataAccess.Contracts;
using AO.VEE.DTO;
using Microsoft.Practices.Unity;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace AO.VEE.Business.Tests.IntegrationTests
{
	/// <summary>
	/// Test class for VeeConsumptionByMeterRepository
	/// </summary>
	[TestClass]
	public class VeeConsumptionByMeterRepositoryTest : TestBase
	{
		private IVeeConsumptionByMeterRepository _veeConsumptionByMeterRepository;

		/// <summary>
		/// Initialization of test case.
		/// </summary>
		[TestInitialize]
		public void Init()
		{
			_veeConsumptionByMeterRepository = UnityContainer.Resolve<IVeeConsumptionByMeterRepository>();
		}

		
		/// <summary>
		/// InsertOrMergeBatchVeeConsumptionByMeterAsync of test case.
		/// </summary>
		[TestMethod]
		public void InsertOrMergeBatchVeeConsumptionByMeterAsync()
		{
			var veeConsumptions = new List<VeeConsumptionByMeterDto>
				{
					GetVeeConsumption()
				};
			Task.Run(() => _veeConsumptionByMeterRepository.InsertOrMergeBatchVeeConsumptionByMeterAsync(veeConsumptions)).Wait();

			Assert.AreEqual(true, true);
		}

		/// <summary>
		/// GetLastVeeConsumptionByMeter test case.
		/// </summary>
		[TestMethod]
		public void GetLastVeeConsumptionByMeter()
		{
			var result = _veeConsumptionByMeterRepository.GetLastVeeConsumptionByMeter(87, "MET900");
			if (result != null) Assert.AreEqual(result.ClientId, 87);
			else Assert.AreEqual(null, null);
		}

		/// <summary>
		/// To delete the VeeConsumptionByMeter data in batch to the database
		/// </summary>
		[TestMethod]
		public void DeleteVeeConsumptionByMeter()
		{
			var veeConsumption = GetVeeConsumption();
            _veeConsumptionByMeterRepository.DeleteVeeConsumptionByMeter(veeConsumption);
			Assert.AreEqual(true, true);
		}

		private static VeeConsumptionByMeterDto GetVeeConsumption()
		{
			return new VeeConsumptionByMeterDto
			{
				ClientId = 87,
				MeterId = "MET900",
				Timezone = "EST",
				CommodityId = 3,
				UOMId = 3,
				AccountNumber = "ACC500",
				AmiTimeStamp = DateTime.Now.Date,
				TransponderPort = "P",
				TransponderId = "P278",
				IntervalType = 15,
				StandardUOMConsumption = 234.45,
				Consumption = 123,
				EditedConsumption = 234,
				EstimatedConsumption = 324,
				Direction = 2,
				IsEdited = true,
				IsNegative = false,
				IsMissing = false,
				IsStatic = false,
				IsZero = false,
				IsSpike = false,
				IsApproved = true,
				IsEstimated = true,
				IsMetered = true,
				StandardUOMId = 3
			};
		}
	}
}
