﻿using System.IO;
using System.Runtime.Remoting.Messaging;
using AO.Business;
using AO.BusinessContracts;
using AO.Registrar;
using CE.AO.Models;
using Microsoft.Practices.Unity;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Enums = CE.AO.Utilities.Enums;

namespace CE.AO.NotificationWorker.Tests.UnitTests
{
    /// <summary>
    /// Test Case Class for Notification Worker Role(Function.cs)
    /// </summary>
    [TestClass]
    public class NotificationWorkerFunctionTest
    {
        private UnityContainer _unityContainer;
        /// <summary>
        /// Test Case Init
        /// </summary>
        [TestInitialize]
        public void Init()
        {
            _unityContainer = new UnityContainer();
            AutoMapperConfig.AutoMapperMappings();
            new DataStorageRegistrar().Initialize<TransientLifetimeManager>(_unityContainer);
            _unityContainer.RegisterInstance(new Mock<INotificationFacade>().Object);
            _unityContainer.RegisterInstance(new Mock<IReport>().Object);
            Functions.UnityContainer = _unityContainer;
        }

        /// <summary>
        /// Test Case for Process Notification
        /// </summary>
        [TestMethod]
        public void ProcessNotification()
        {
            TextWriter log = TextWriter.Null;
            Functions.LogModel = new LogModel
            {
                DisableLog = true,
                Module = Enums.Module.Notification
            };
            Functions.ProcessNotification("87^^600^^AC76^^096756^^Program1^^Source^^89^^Meta7890^^None^^34", log);
            Assert.AreEqual(true, true);
        }

        /// <summary>
        /// Test Case for Process Notification
        /// </summary>
        [TestMethod]
        public void ProcessNotification_Else()
        {
            TextWriter log = TextWriter.Null;

            Functions.LogModel = new LogModel()
            {
                DisableLog = true,
                Module = Enums.Module.Notification
            };
            Functions.ProcessNotification("87^^600^^AC76^^096756^^Program1^^Source^^89^^Meta7890^^None", log);
            Assert.AreEqual(true, true);
        }

        /// <summary>
        /// Test Case for Process Notification For Catch
        /// </summary>
        [TestMethod]
        public void ProcessNotification_Catch()
        {
            var log = TextWriter.Null;
            Functions.LogModel = new LogModel
            {
                DisableLog = true,
                Module = Enums.Module.Notification
            };
            try
            {
                Functions.ProcessNotification(null, log);
            }
            catch
            {
                Assert.AreEqual(true, true);
            }
        }

        /// <summary>
        /// Test Case for Process Notification For Catch
        /// </summary>
        [TestMethod]
        public void ProcessNotificationReport()
        {
            TextWriter log = TextWriter.Null;
            Functions.LogModel = new LogModel()
            {
                DisableLog = true,
                Module = Enums.Module.InsightEvaluation
            };
            Functions.ProcessNotificationReport("87^^09/24/2016^^2", log);
            Assert.AreEqual(true, true);
        }

        /// <summary>
        /// Test Case for Process Notification For Catch
        /// </summary>
        [TestMethod]
        public void ProcessNotificationReport_Eles()
        {
            TextWriter log = TextWriter.Null;

            Functions.LogModel = new LogModel()
            {
                DisableLog = true,
                Module = Enums.Module.InsightEvaluation
            };
            Functions.ProcessNotificationReport("87^^09/24/2016^^2^^34", log);
            Assert.AreEqual(true, true);
        }


        /// <summary>
        /// Test Case for Process Notification For Catch
        /// </summary>
        [TestMethod]
        public void ProcessNotificationReport_Catch()
        {
            TextWriter log = TextWriter.Null;

            Functions.LogModel = new LogModel()
            {
                DisableLog = true,
                Module = Enums.Module.InsightEvaluation
            };
            try
            {
                Functions.ProcessNotificationReport("87^^2^^09/24/2016", log);
            }
            catch
            {
                Assert.AreEqual(true, true);
            }
        }

        /// <summary>
        /// Cleanup Method To Release Log4Net Data Resources hold by Any Test Method
        /// </summary>
        [TestCleanup]
        public void CleanUpMethod()
        {
            CallContext.FreeNamedDataSlot("log4net.Util.LogicalThreadContextProperties");
        }
    }
}
