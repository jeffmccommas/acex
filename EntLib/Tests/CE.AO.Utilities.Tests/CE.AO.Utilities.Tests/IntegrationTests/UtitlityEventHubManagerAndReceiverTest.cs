﻿using System;
using System.Diagnostics;
using System.Threading.Tasks;
using AO.EventProcessors;
using AO.Registrar;
using Microsoft.Azure;
using Microsoft.ServiceBus.Messaging;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.Practices.Unity;
using Microsoft.ServiceBus;
using EventHubReceiver = AO.EventProcessors.EventHubReceiver;

namespace CE.AO.Utilities.Tests.IntegrationTests
{
    /// <summary>
    /// Test class for EventManager and EventReceiver from utilities
    /// </summary>
    [TestClass]
    public class UtitlityEventHubManagerAndReceiverTest
    {
        private UnityContainer _unityContainer;

        private string _connectionString = EventHubManager.GetServiceBusConnectionString("ImportDataEventHubManageConnectionString");

        /// <summary>
        /// Initialization of test case.
        /// </summary>
        [TestInitialize]
        public void Init()
        {
            _unityContainer = new UnityContainer();
            new DataStorageRegistrar().Initialize<TransientLifetimeManager>(_unityContainer);
        }

        /// <summary>
        /// Test method to test Methods from both EventHubManager and ReceiverTest
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task CreateRegisterDelete_EventHub()
        {
            EventProcessorFactory<EventProcessor> eventProcessorFactory = null;
            if (_unityContainer != null)
                eventProcessorFactory= new EventProcessorFactory<EventProcessor>();

            var importDataEventHubName = "utilityeventhubdevtest"; //eventHubName
            var numberOfPartitionsForImportDataEventHub = 32;
            var consumerGroupNameForImportDataEventHub = "";
            var blobConnectionString =
                CloudConfigurationManager.GetSetting("AclaraOneStorageConnectionString");

            //Get AMQP connection string
            var connectionString = _connectionString;
            //Create event hub if it does not exist
            var namespaceManager = EventHubManager.GetNamespaceManager(connectionString);//aclaraOneStorageConnectionString
            var firstTask = Task.Run(() => EventHubManager.CreateEventHubIfNotExists(importDataEventHubName,
                            numberOfPartitionsForImportDataEventHub, namespaceManager));
            firstTask.Wait();

            //Receiver Test Part
            //Create consumer group if it does not exist
            var group = string.IsNullOrWhiteSpace(consumerGroupNameForImportDataEventHub) ? null : namespaceManager.CreateConsumerGroupIfNotExists(importDataEventHubName, consumerGroupNameForImportDataEventHub);
            var receiver = new EventHubReceiver(importDataEventHubName, connectionString);
            
            var hostName = "test";

            await Task.Run(() => receiver.RegisterEventProcessor(group, blobConnectionString, hostName, eventProcessorFactory));
            
            await Task.Run(() => receiver.UnregisterEventProcessor());

            //deleting Event Hub
            await firstTask.ContinueWith((t) => DeleteEventHub(importDataEventHubName, namespaceManager));

            var isEventHubCreated = EventHubManager.GetNamespaceManager(_connectionString).EventHubExists(importDataEventHubName);
            Assert.IsFalse(isEventHubCreated);

        }

        /// <summary>
        /// Test method to test Methods from both EventHubManager and ReceiverTest using consumerGroupName and connectionStringKeyName as parameter for CreateEventHubIfNotExists method
        /// </summary>
        [TestMethod]
        public async Task CreateRegisterDelete_EventHubForConsumerGroupDescription()
        {
            EventProcessorFactory<EventProcessor> eventProcessorFactory = null;
            if (_unityContainer != null)
                eventProcessorFactory = new EventProcessorFactory<EventProcessor>();

            var importDataEventHubName = "utilityeventhubdevtestforconsumergroup"; //eventHubName
            var numberOfPartitionsForImportDataEventHub = 32;

            var blobConnectionString =
               CloudConfigurationManager.GetSetting("AclaraOneStorageConnectionString");
            //Get AMQP connection string
            
            //Create event hub if it does not exist
            var namespaceManager = EventHubManager.GetNamespaceManager(_connectionString);   //aclaraOneStorageConnectionString

            var consumerGroupDesc = EventHubManager.CreateEventHubIfNotExists(importDataEventHubName,
                            numberOfPartitionsForImportDataEventHub, "ImportDataEventHubManageConnectionString", "TestConsumerGroup");

            var hostName = "test";
            var receiver = new EventHubReceiver(importDataEventHubName, _connectionString);
            await Task.Run(() => receiver.RegisterEventProcessor(consumerGroupDesc, blobConnectionString, hostName, eventProcessorFactory));
            await Task.Run(() => receiver.UnregisterEventProcessor());

            //deleting Event Hub
            DeleteEventHub(importDataEventHubName, namespaceManager);
            Assert.AreEqual(consumerGroupDesc != null, true);
        }

        /// <summary>
        /// Test method for GetServiceBusConnectionString in EventHubManager Class to cover catch 
        /// </summary>
        [TestMethod]
        public void GetServiceBusConnectionStringWithNullKeyToCoverCatch()
        {
           string conString =  EventHubManager.GetServiceBusConnectionString("InvalidKey");
            Assert.AreEqual( string.IsNullOrEmpty(conString), true);
        }

        /// <summary>
        /// Test method for CreateEventHubIfNotExists in EventHubManager Class to cover catch 
        /// </summary>
        [TestMethod]
        public void CreateEventHubIfNotExistsToCoverCatch()
        {
            //Create event hub if it does not exist
            var namespaceManager = EventHubManager.GetNamespaceManager(_connectionString);//aclaraOneStorageConnectionString
            EventHubManager.CreateEventHubIfNotExists("testeventhub", 86872681, namespaceManager);
            var isEventHubCreated = EventHubManager.GetNamespaceManager(_connectionString).EventHubExists("testeventhub");
            Assert.IsFalse(isEventHubCreated);
        }

        [TestMethod]
        public void CreateEventHubIfNotExistsWithConsumerGroupNameToCoverCatch()
        {
            ConsumerGroupDescription desc = EventHubManager.CreateEventHubIfNotExists("testeventhub", 16, null, "testConsumer&$1:-");
            Assert.IsTrue(desc == null);
        }

        /// <summary>
        /// Method to Delete Event Hub
        /// </summary>
        /// <param name="eventHubName"></param>
        /// <param name="manager"></param>
        private void  DeleteEventHub(string eventHubName, NamespaceManager manager)
        {
            try
            {
                // Delete the Event Hub
                manager.DeleteEventHub(eventHubName);
            }
            catch (Exception exp)
            {
                Trace.WriteLine(exp.Message);
            }
        }
    }
}
