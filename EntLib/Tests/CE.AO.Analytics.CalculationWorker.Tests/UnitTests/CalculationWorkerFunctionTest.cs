﻿using System.IO;
using System.Runtime.Remoting.Messaging;
using AO.Business;
using AO.BusinessContracts;
using AO.Registrar;
using CE.AO.Analytics.CalculationWorker.Calculation;
using Microsoft.Practices.Unity;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Enums = CE.AO.Utilities.Enums;
using CE.AO.Models;

namespace CE.AO.Analytics.CalculationWorker.Tests.UnitTests
{
	/// <summary>
	/// Test Case Class for Calculation Worker Role(Function.cs)
	/// </summary>
	[TestClass]
	public class CalculationWorkerFunctionTest
	{
		private UnityContainer _unityContainer;
		/// <summary>
		/// Test Case Init
		/// </summary>
		[TestInitialize]
		public void Init()
		{
			_unityContainer = new UnityContainer();
			AutoMapperConfig.AutoMapperMappings();
            (new DataStorageRegistrar()).Initialize<TransientLifetimeManager>(_unityContainer);
            _unityContainer.RegisterInstance(new Mock<ICalculationFacade>().Object);
			Functions.UnityContainer = _unityContainer;
		}
		/// <summary>
		/// Test Case for Process Calculations  
		/// </summary>
		[TestMethod]
		public void ProcessCalculation()
		{
			var log = TextWriter.Null;
			Functions.LogModel = new LogModel
			{
				DisableLog = true,
				Module = Enums.Module.Calculation
			};
			Functions.ProcessCalculation("87^^600^^09/24/2016^^Source^^22^^Metadata^^2", log);
			Assert.AreEqual(true, true);
		}

		/// <summary>
		/// Test Case for Process Calculations For Else Statement
		/// </summary>
		[TestMethod]
		public void ProcessCalculation_ForElse()
		{
			TextWriter log = TextWriter.Null;
			Functions.LogModel = new LogModel()
			{
				DisableLog = true,
				Module = Enums.Module.Calculation
			};
			Functions.ProcessCalculation("87^^600^^09/24/2016^^Source^^22", log);
			Assert.AreEqual(true, true);
		}

		/// <summary>
		/// Test Case for Process Calculations For Catch Statement
		/// </summary>
		[TestMethod]
		public void ProcessCalculation_ForCatch()
		{
			TextWriter log = TextWriter.Null;
			Functions.LogModel = new LogModel()
			{
				DisableLog = true,
				Module = Enums.Module.Calculation
			};
			try
			{
				Functions.ProcessCalculation("87^^09/24/2016^^600^^Source^^22^^Metadata^^2", log);
			}
			catch
			{
				Assert.AreEqual(true, true);
			}
		}

		/// <summary>
		/// Test Case for Scheduled Calculation
		/// </summary>
		[TestMethod]
		public void ScheduledCalculation()
		{
			TextWriter log = TextWriter.Null;
			Functions.LogModel = new LogModel()
			{
				DisableLog = true,
				Module = Enums.Module.Calculation
			};
			Functions.ScheduledCalculation("87^^09/24/2016", log);
			Assert.AreEqual(true, true);
		}

		/// <summary>
		/// Test Case for Scheduled Calculation for Else Statement
		/// </summary>
		[TestMethod]
		public void ScheduledCalculation_ForElse()
		{
			TextWriter log = TextWriter.Null;
			Functions.LogModel = new LogModel()
			{
				DisableLog = true,
				Module = Enums.Module.Calculation
			};
			Functions.ScheduledCalculation("09/24/2016^^87^^1", log);
			Assert.AreEqual(true, true);
		}

		/// <summary>
		/// Test Case for Scheduled Calculation for Else Statement
		/// </summary>
		[TestMethod]
		public void ScheduledCalculation_ForCatch()
		{
			TextWriter log = TextWriter.Null;
			try
			{
				Functions.LogModel = new LogModel()
				{
					DisableLog = true,
					Module = Enums.Module.Calculation
				};
				Functions.ScheduledCalculation("09/24/2016^^87", log);
			}
			catch
			{
				Assert.AreEqual(true, true);
			}
		}

		/// <summary>
		/// Test Case for Process Schedule  Calculation
		/// </summary>
		[TestMethod]
		public void ProcessScheduleCalculation()
		{
			TextWriter log = TextWriter.Null;
			Functions.LogModel = new LogModel()
			{
				DisableLog = true,
				Module = Enums.Module.Calculation
			};
			Functions.ProcessScheduleCalculation("9^^87^^500^^09/24/2016", log);
			Assert.AreEqual(true, true);
		}


		/// <summary>
		/// Test Case for Process Process Scheduled Calculations for Else
		/// </summary>
		[TestMethod]
		public void ProcessScheduleCalculation_ForElse()
		{
			TextWriter log = TextWriter.Null;
			Functions.LogModel = new LogModel()
			{
				DisableLog = true,
				Module = Enums.Module.Calculation
			};
			Functions.ProcessScheduleCalculation("9^^87^^500", log);
			Assert.AreEqual(true, true);
		}

		/// <summary>
		/// Test Case for Process Process Scheduled Calculations for Catch
		/// </summary>
		[TestMethod]
		public void ProcessScheduleCalculation_ForCatch()
		{
			TextWriter log = TextWriter.Null;
			try
			{
				Functions.LogModel = new LogModel()
				{
					DisableLog = true,
					Module = Enums.Module.Calculation
				};
				Functions.ProcessCalculation("9^^87^^09/21/2016^^500", log);
			}
			catch
			{
				Assert.AreEqual(true, true);
			}
		}

		[TestMethod]
		public void ContentFull()
		{
			ContentFul cf = new ContentFul();
			var rateClassCompanyId = cf.GetRateClass("2");
			Assert.AreEqual(rateClassCompanyId, "RateClassCompanyId");
		}

		/// <summary>
		/// Cleanup Method To Release Log4Net Data Resources hold by Any Test Method
		/// </summary>
		[TestCleanup]
		public void CleanUpMethod()
		{
			CallContext.FreeNamedDataSlot("log4net.Util.LogicalThreadContextProperties");
		}

	}
}
