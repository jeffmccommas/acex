﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using AO.DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AO.DataAccess.Tests
{
    [TestClass()]
    public class AssemblyInfo
    {
        [TestMethod()]
        public void CheckNetworkAccessibilityTest() {
            string hostNames = "www.google.com,www.aclara.com,doesentexisthost";
            StringBuilder errorData = new StringBuilder();
            List<string> availHosts = CassandraSession.CheckNetworkAccessibility(hostNames, errorData);
            Console.WriteLine("error info = "+ errorData.ToString());
            Assert.AreEqual(availHosts.Count,2);
        }
    }
}