﻿using System;

namespace AO.GreenButtonConnect.IngestionUtil
{
    [Serializable]
    public class SubscriptionMeter
    {
        public int ClientId { get; set; }
        public string CustomerId { get; set; }
        public string CustomerName { get; set; }
        public string AccountId { get; set; }
        public string MeterId { get; set; }
        public string Status { get; set; } = "new";  // options: new, existing, rejected
        public int ErrorCount { get; set; }
        public int IngestedReadingsCount { get; set; }
        public int RejectedReadingsCount { get; set; }
        public DateTime AuthorizationDate { get; set; }
        public DateTime Created { get; set; } = DateTime.UtcNow;
        public DateTime Updated { get; set; } = DateTime.UtcNow;
        public DateTime HistoricalLoadDate { get; set; }
        public string Logfile { get; set; }
    }
}
