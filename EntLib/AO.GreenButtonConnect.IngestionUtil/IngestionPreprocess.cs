﻿using System;
using System.Data;
using System.Linq;
using AO.TimeSeriesProcessing;

namespace AO.GreenButtonConnect.IngestionUtil
{
    public class IngestionPreprocess
    {
        private readonly string _fileName;
        private readonly int _clientId;
        private readonly GreenButtonConnectEfRepository _gbcRepos;
        public IngestionPreprocess(string env, int clientId, string filename)
        {
            _fileName = filename;
            _clientId = clientId;
            _gbcRepos = new GreenButtonConnectEfRepository(env);
        }

        public IngestionPreprocess(InsightsEntities context, int clientId, string filename)
        {
            _fileName = filename;
            _clientId = clientId;
            _gbcRepos = new GreenButtonConnectEfRepository(context);
        }
        
        public DataTable GenerateSubscribedMetersFile(ContextData context, DataTable data)
        {
            try
            {
                var meters = _gbcRepos.GetSubscriptionMeters(_clientId);

                if (meters != null && meters.Rows.Count > 0)
                {
                    var writeFile = AddNewSubscribedMeters(data, ref meters);

                    if (writeFile)
                        return meters;

                    context.ContextWrite("INFO", $"No new Green Button Connect subscriber for clientId {_clientId}, file {_fileName} is not updated/generated");
                }
                else
                {
                    context.ContextWrite("INFO", $"No Green Button Connect subscriber for clientId {_clientId}, file {_fileName} is not updated/generated");
                }
            }
            catch (Exception ex)
            {
                // log error 
                context.ContextWrite("IngestionPreprocess.GenerateSubscribedMetersFile-ERROR", $"Exception: {ex.Message} InnerException: {ex.InnerException?.Message}");
            }
            
            return null;
        }
        private bool AddNewSubscribedMeters(DataTable existingSubscribedMeters, ref DataTable meters)
        {
            if (existingSubscribedMeters != null && existingSubscribedMeters.Rows.Count > 0)
            {
                var meterList = meters.AsEnumerable();
                var newSubscribedMeters = meterList
                    .Where(m => meterList
                        .Select(meter => new
                        {
                            meterId = Convert.ToString(meter["MeterId"]),
                            accountId = Convert.ToString(meter["AccountId"]),
                            clientId = Convert.ToInt32(meter["ClientId"])
                        }).Except(existingSubscribedMeters.AsEnumerable()
                            .Select(s => new
                            {
                                meterId = Convert.ToString(s["MeterId"]),
                                accountId = Convert.ToString(s["AccountId"]),
                                clientId = Convert.ToInt32(s["ClientId"])
                            })).ToList().Exists(t => t.meterId == Convert.ToString(m["MeterId"]) &&
                                                     t.accountId == Convert.ToString(m["AccountId"]) &&
                                                     t.clientId == Convert.ToInt32(m["ClientId"])));

                if (newSubscribedMeters.Any())
                {
                    foreach (var newSubscribedMeter in newSubscribedMeters)
                    {
                        existingSubscribedMeters.Rows.Add(newSubscribedMeter.ItemArray);
                    }
                    meters = existingSubscribedMeters;
                }
                else
                {
                    return false;
                }
            }
            return true;
        }
    }
}
