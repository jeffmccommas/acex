﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using AO.GreenButtonConnect.IngestionUtil;
using Moq;

namespace AO.GreenButtonConnect.IngestionUtilTest
{
    public class InsightsEntitiesMock
    {
        protected Mock<InsightsEntities> MockInsightsEntities;

        public InsightsEntitiesMock()
        {
            MockInsightsEntities = new Mock<InsightsEntities>();
            RetailCustomerDbSetMock();
            SubscriptionDbSetMock();
            SubscriptionUsagePointDbSetMock();
            UsagePointDbSetMock();
        }


        public void SubscribeMeter()
        {
            MockInsightsEntities.Object.retail_customers.Add(new retail_customers
            {
                id = 4,
                data_custodian_account_id = "account4",
                data_custodian_customer_id = "customer4",
                data_custodian_client_id = 87
            });

            MockInsightsEntities.Object.subscriptions.Add(new subscription
            {
                id = 5,
                retail_customer_id = 4,
                updated = DateTime.Today.AddHours(-4)
            });

            MockInsightsEntities.Object.subscription_usage_points.Add(
                new subscription_usage_points {subscription_id = 5, usage_point_id = 5});

            MockInsightsEntities.Object.usage_points.Add(new usage_points
            {
                id = 5,
                uuid = "GuidUsagePoint5",
                self_link_href = "/Datacustodian/espi/1_1/resource/Subscription/5/UsagePoint/meter5encryted",
                self_link_rel = "self",
                up_link_href = "/Datacustodian/espi/1_1/resource/Subscription/5/UsagePoint",
                up_link_rel = "up",
                meterid = "meter5",
                local_time_parameters_id = null
            });
        }
        private void RetailCustomerDbSetMock()
        {
            
            var retailCustomerDbSetMock = new Mock<DbSet<retail_customers>>();
            var mockRetailCustomers = MockRetailCustomerDbs();
            var retailCustomerMock = mockRetailCustomers.AsQueryable();

            retailCustomerDbSetMock.As<IQueryable<retail_customers>>()
                .Setup(m => m.Provider)
                .Returns(retailCustomerMock.Provider);

            retailCustomerDbSetMock.As<IQueryable<retail_customers>>()
                .Setup(m => m.Expression)
                .Returns(retailCustomerMock.Expression);

            retailCustomerDbSetMock.As<IQueryable<retail_customers>>()
                .Setup(m => m.ElementType)
                .Returns(retailCustomerMock.ElementType);

            retailCustomerDbSetMock.As<IQueryable<retail_customers>>()
                .Setup(m => m.GetEnumerator())
                .Returns(retailCustomerMock.GetEnumerator());

            retailCustomerDbSetMock.Setup(m => m.Add(It.IsAny<retail_customers>()))
                .Callback<retail_customers>(s => mockRetailCustomers.Add(s));

            MockInsightsEntities.Setup(i => i.retail_customers).Returns(retailCustomerDbSetMock.Object);
        }
        private List<retail_customers> MockRetailCustomerDbs()
        {
            var retailCustomerMock = new List<retail_customers>
            {
                new retail_customers
                {
                    id = 1,
                    data_custodian_account_id = "account1",
                    data_custodian_customer_id = "customer1",
                    data_custodian_client_id = 87,
                    first_name = "customer1_first",
                    last_name = "last"
                },
                new retail_customers
                {
                    id = 2,
                    data_custodian_account_id = "account2",
                    data_custodian_customer_id = "customer2",
                    data_custodian_client_id = 87,
                    first_name = "customer2_first",
                    last_name = "last"
                },
                new retail_customers
                {
                    id = 3,
                    data_custodian_account_id = "account3",
                    data_custodian_customer_id = "customer3",
                    data_custodian_client_id = 87,
                    first_name = "customer3_first",
                    last_name = "last"
                }
            };


            return retailCustomerMock;
        }

        private void SubscriptionDbSetMock()
        {
            var subscriptionDbSetMock = new Mock<DbSet<subscription>>();
            var mockSubscription = MockSubscriptionDbs();
            var subscriptionMock = mockSubscription.AsQueryable();

            subscriptionDbSetMock.As<IQueryable<subscription>>()
                .Setup(m => m.Provider)
                .Returns(subscriptionMock.Provider);

            subscriptionDbSetMock.As<IQueryable<subscription>>()
                .Setup(m => m.Expression)
                .Returns(subscriptionMock.Expression);

            subscriptionDbSetMock.As<IQueryable<subscription>>()
                .Setup(m => m.ElementType)
                .Returns(subscriptionMock.ElementType);

            subscriptionDbSetMock.As<IQueryable<subscription>>()
                .Setup(m => m.GetEnumerator())
                .Returns(subscriptionMock.GetEnumerator());

            subscriptionDbSetMock.Setup(m => m.Add(It.IsAny<subscription>()))
                .Callback<subscription>(s => mockSubscription.Add(s));

            MockInsightsEntities.Setup(i => i.subscriptions).Returns(subscriptionDbSetMock.Object);
        }
        private List<subscription> MockSubscriptionDbs()
        {

            var subscriptiontMock = new List<subscription>();

            // customer 1 - meter 1, meter 2
            subscriptiontMock.Add(new subscription
            {
                id = 1,
                retail_customer_id = 1,
                updated = DateTime.Today.AddDays(-4)
            });
            // customer 1 - meter 3
            subscriptiontMock.Add(new subscription
            {
                id = 2,
                retail_customer_id = 1,
                updated = DateTime.Today.AddDays(-4)
            });
            // customer 2 - meter 4
            subscriptiontMock.Add(new subscription
            {
                id = 3,
                retail_customer_id = 2,
                updated = DateTime.Today.AddHours(-10)
            });
            // customer 3 - meter 3
            subscriptiontMock.Add(new subscription
            {
                id = 4,
                retail_customer_id = 3,
                updated = DateTime.Today.AddDays(-4)
            });
            // customer 2 - meter 4 - different vendor
            subscriptiontMock.Add(new subscription
            {
                id = 5,
                retail_customer_id = 2,
                updated = DateTime.Today.AddHours(-10)
            });


            return subscriptiontMock;
        }
        private void SubscriptionUsagePointDbSetMock()
        {
            var subscriptionUsagePointsDbSetMock = new Mock<DbSet<subscription_usage_points>>();
            var mockSubscriptionUsagePoint = MockSubscriptionUsagePointDbs();
            var subscriptionUsagePointsMock = mockSubscriptionUsagePoint.AsQueryable();

            subscriptionUsagePointsDbSetMock.As<IQueryable<subscription_usage_points>>()
                .Setup(m => m.Provider)
                .Returns(subscriptionUsagePointsMock.Provider);

            subscriptionUsagePointsDbSetMock.As<IQueryable<subscription_usage_points>>()
                .Setup(m => m.Expression)
                .Returns(subscriptionUsagePointsMock.Expression);

            subscriptionUsagePointsDbSetMock.As<IQueryable<subscription_usage_points>>()
                .Setup(m => m.ElementType)
                .Returns(subscriptionUsagePointsMock.ElementType);

            subscriptionUsagePointsDbSetMock.As<IQueryable<subscription_usage_points>>()
                .Setup(m => m.GetEnumerator())
                .Returns(subscriptionUsagePointsMock.GetEnumerator());

            subscriptionUsagePointsDbSetMock.Setup(m => m.Add(It.IsAny<subscription_usage_points>()))
                .Callback<subscription_usage_points>(s => mockSubscriptionUsagePoint.Add(s));

            MockInsightsEntities.Setup(i => i.subscription_usage_points).Returns(subscriptionUsagePointsDbSetMock.Object);
        }
        private List<subscription_usage_points> MockSubscriptionUsagePointDbs()
        {

            var subscriptionUsagePointMock = new List<subscription_usage_points>();

            subscriptionUsagePointMock.Add(new subscription_usage_points { subscription_id = 1, usage_point_id = 1 });
            subscriptionUsagePointMock.Add(new subscription_usage_points { subscription_id = 1, usage_point_id = 2 });
            subscriptionUsagePointMock.Add(new subscription_usage_points { subscription_id = 2, usage_point_id = 3 });
            subscriptionUsagePointMock.Add(new subscription_usage_points { subscription_id = 3, usage_point_id = 4 });
            subscriptionUsagePointMock.Add(new subscription_usage_points { subscription_id = 4, usage_point_id = 5 });
            subscriptionUsagePointMock.Add(new subscription_usage_points { subscription_id = 5, usage_point_id = 6 });

            return subscriptionUsagePointMock;
        }
        private void UsagePointDbSetMock()
        {
            var usagePointsDbSetMock = new Mock<DbSet<usage_points>>();
            var mockUsagePoint = MockUsagePointDbs();
            var usagePointsMock = mockUsagePoint.AsQueryable();

            usagePointsDbSetMock.As<IQueryable<usage_points>>()
                .Setup(m => m.Provider)
                .Returns(usagePointsMock.Provider);

            usagePointsDbSetMock.As<IQueryable<usage_points>>()
                .Setup(m => m.Expression)
                .Returns(usagePointsMock.Expression);

            usagePointsDbSetMock.As<IQueryable<usage_points>>()
                .Setup(m => m.ElementType)
                .Returns(usagePointsMock.ElementType);

            usagePointsDbSetMock.As<IQueryable<usage_points>>()
                .Setup(m => m.GetEnumerator())
                .Returns(usagePointsMock.GetEnumerator());

            usagePointsDbSetMock.Setup(m => m.Add(It.IsAny<usage_points>()))
                .Callback<usage_points>(s => mockUsagePoint.Add(s));

            MockInsightsEntities.Setup(i => i.usage_points).Returns(usagePointsDbSetMock.Object);

        }
        private List<usage_points> MockUsagePointDbs()
        {
            var usagePointMock = new List<usage_points>();

            usagePointMock.Add(new usage_points { id = 1, uuid = "23FB453B-C50C-4E2F-96E7-3D338BAAD9D8", self_link_href = "/Datacustodian/espi/1_1/resource/Subscription/1/UsagePoint/meter1encryted", self_link_rel = "self", up_link_href = "/Datacustodian/espi/1_1/resource/Subscription/1/UsagePoint", up_link_rel = "up", meterid = "meter1", local_time_parameters_id = null });
            usagePointMock.Add(new usage_points { id = 2, uuid = "A02C4C34-C4E2-490A-8D20-F0780AF91587", self_link_href = "/Datacustodian/espi/1_1/resource/Subscription/1/UsagePoint/meter2encryted", self_link_rel = "self", up_link_href = "/Datacustodian/espi/1_1/resource/Subscription/1/UsagePoint", up_link_rel = "up", meterid = "meter2", local_time_parameters_id = null });
            usagePointMock.Add(new usage_points { id = 3, uuid = "822062ED-9C56-4EF4-82F9-6F5815D68486", self_link_href = "/Datacustodian/espi/1_1/resource/Subscription/2/UsagePoint/meter3encryted", self_link_rel = "self", up_link_href = "/Datacustodian/espi/1_1/resource/Subscription/2/UsagePoint", up_link_rel = "up", meterid = "meter3", local_time_parameters_id = 1 });
            usagePointMock.Add(new usage_points { id = 4, uuid = "GuidUsagePoint4", self_link_href = "/Datacustodian/espi/1_1/resource/Subscription/3/UsagePoint/meter4encryted", self_link_rel = "self", up_link_href = "/Datacustodian/espi/1_1/resource/Subscription/3/UsagePoint", up_link_rel = "up", meterid = "meter4", local_time_parameters_id = null });
            usagePointMock.Add(new usage_points { id = 5, uuid = "822062ED-9C56-4EF4-82F9-6F5815D68486", self_link_href = "/Datacustodian/espi/1_1/resource/Subscription/4/UsagePoint/meter3encryted", self_link_rel = "self", up_link_href = "/Datacustodian/espi/1_1/resource/Subscription/4/UsagePoint", up_link_rel = "up", meterid = "meter3", local_time_parameters_id = 1 });
            usagePointMock.Add(new usage_points { id = 6, uuid = "GuidUsagePoint6", self_link_href = "/Datacustodian/espi/1_1/resource/Subscription/5/UsagePoint/meter4encryted", self_link_rel = "self", up_link_href = "/Datacustodian/espi/1_1/resource/Subscription/5/UsagePoint", up_link_rel = "up", meterid = "meter4", local_time_parameters_id = null });

            return usagePointMock;
        }
    }


}
