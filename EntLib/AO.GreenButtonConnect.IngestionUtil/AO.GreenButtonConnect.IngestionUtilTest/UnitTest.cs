﻿using System;
using System.Data;
using System.Linq;
using AO.GreenButtonConnect.IngestionUtil;
using AO.TimeSeriesProcessing;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace AO.GreenButtonConnect.IngestionUtilTest
{
    [TestClass]
    public class UnitTest : InsightsEntitiesMock
    {

        [TestMethod]
        public void GetSubscriptionMetersTest()
        {
            try
            {
                var clientId = 87;

                var gbcRepository = new GreenButtonConnectEfRepository(MockInsightsEntities.Object);
                DataTable result = gbcRepository.GetSubscriptionMeters(clientId);

                Assert.IsNotNull(result);
                Assert.AreEqual(5, result.Rows.Count);
                Assert.AreEqual(1, result.AsEnumerable().Count(r => r.Field<string>("Status") == "rejected"));
                Assert.AreEqual(4, result.AsEnumerable().Count(r => r.Field<string>("Status") == "new"));
            }
            catch (Exception ex)
            {
                Assert.Fail(ex.Message);
            }
        }

        [TestMethod]
        public void GetGreenButtonConnectSubscribedMetersTest()
        {
            try
            {
                var args = new ProcessingArgs();
                var clientId = 87;
                var filename = "c:/subscribedMeter.xml";
                var gbcModel = new IngestionPreprocess(MockInsightsEntities.Object, clientId, filename);
                args["unique_pathFrag"] = "test1";
                var context = new ContextData(args);
                var result = gbcModel.GenerateSubscribedMetersFile(context, null);

                Assert.IsNotNull(result);
                var size = result.Rows.Count;

                // subscribe meter
                SubscribeMeter();

                // update file
                args["unique_pathFrag"] = "test2";
                context = new ContextData(args);
                result = gbcModel.GenerateSubscribedMetersFile(context, result);
                Assert.IsNotNull(result);
                var size2 = result.Rows.Count;
                Assert.AreNotEqual(size, size2);

                // no update
                args["unique_pathFrag"] = "test3";
                context = new ContextData(args);
                result = gbcModel.GenerateSubscribedMetersFile(context, result);
                Assert.IsNull(result);


                // no subscribed meter
                clientId = 174;
                args["unique_pathFrag"] = "test4";
                context = new ContextData(args);
                gbcModel = new IngestionPreprocess(MockInsightsEntities.Object, clientId, filename);
                result = gbcModel.GenerateSubscribedMetersFile(context, null);
                Assert.IsNull(result);


            }
            catch (Exception ex)
            {
                Assert.Fail(ex.Message);
            }

        }

        [TestMethod]
        public void GenerateReportTest()
        {
            try
            {
                var args = new ProcessingArgs();
                var clientId = 87;
                var localDir = "c:/";
                var desDir = @"\\swepfileuat1\Documents\Development\{%clientid%}\Export\AclaraOne\";
                
                // test data
                var data = ReportTestData(clientId);
                
                var report = new IngestionReport(clientId,localDir,desDir);
                args["unique_pathFrag"] = "reporttest2";
                var context = new ContextData(args);
                var result= report.GenerateReport(context, ref data);
                context.End();

                Assert.IsTrue(result);

                report = new IngestionReport(clientId, localDir, desDir);
                args["unique_pathFrag"] = "reporttest3";
                context = new ContextData(args);
                var dt = new DataTable();
                result = report.GenerateReport(context, ref dt);
                context.End();

                Assert.IsFalse(result);

            }
            catch (Exception ex)
            {
                Assert.Fail(ex.Message);
            }
        }


        private DataTable ReportTestData(int clientId)
        {
            var args = new ProcessingArgs();
            var filename = "c:/subscribedMeter.xml";
            var preprocess = new IngestionPreprocess(MockInsightsEntities.Object, clientId, filename);
            args["unique_pathFrag"] = "reporttest1";
            var context = new ContextData(args);
            var data = preprocess.GenerateSubscribedMetersFile(context, null);
            context.End();

            // meter with load error
            var meters = data.AsEnumerable().Where(d => d.Field<string>("Status") == "new").Select(m => m).ToList();
            meters[0]["IngestedReadingsCount"] = 10;
            meters[0]["ErrorCount"] = 1;
            meters[0]["Updated"] = DateTime.UtcNow;
            meters[0]["Status"] = "existing";

            // meter last load was more than 24 hrs ago, no new ami load
            meters[1]["IngestedReadingsCount"] = 10;
            meters[1]["Updated"] = DateTime.UtcNow.AddHours(-60);
            meters[1]["Status"] = "existing";

            // meter successfully load
            meters[2]["IngestedReadingsCount"] = 10;
            meters[2]["Updated"] = DateTime.UtcNow;
            meters[2]["Status"] = "existing";

            // meters[3] - no historical/incremental load

            // meter[4] - duplicate meter id

            return data;
        }
    }
}
