﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Reflection;

namespace AO.GreenButtonConnect.IngestionUtil
{
    public class GreenButtonConnectEfRepository
    {
        private readonly InsightsEntities _db;
        public GreenButtonConnectEfRepository(string env)
        {
            //var gbcEntities = ConfigurationManager.AppSettings.Get($"{env.ToLower()}InsightsEntities");
            _db = new InsightsEntities(env);
        }


        public GreenButtonConnectEfRepository(InsightsEntities context)
        {
            _db = context;
        }
        public DataTable GetSubscriptionMeters(int clientId)
        {
            DataTable dt;

            //declare the transaction options
            var transactionOptions =
                new System.Transactions.TransactionOptions
                {
                    IsolationLevel = System.Transactions.IsolationLevel.ReadUncommitted
                };
            //set it to read uncommited

            using (
                var transScopeAction =
                    new System.Transactions.TransactionScope(System.Transactions.TransactionScopeOption.Required,
                        transactionOptions))
            {
                var meters =
                    _db.usage_points.Join(_db.subscription_usage_points, up => up.id, sup => sup.usage_point_id,
                            (up, sup) => new { up.meterid, sup.subscription_id })
                        .Join(_db.subscriptions, jup => jup.subscription_id, s => s.id,
                            (jup, s) => new { jup.subscription_id, jup.meterid, s.retail_customer_id, s.updated })
                        .Join(_db.retail_customers, jsup => jsup.retail_customer_id, c => c.id,
                            (jsup, c) => new SubscriptionMeter
                            {
                                MeterId = jsup.meterid,
                                CustomerId = c.data_custodian_customer_id,
                                ClientId = c.data_custodian_client_id,
                                AccountId = c.data_custodian_account_id,
                                AuthorizationDate = jsup.updated,
                                CustomerName = c.first_name + " " + c.last_name
                            })
                        .Where(c => c.ClientId == clientId).ToList();

                meters = meters.GroupBy(m => new {m.MeterId, m.AccountId, m.ClientId, m.CustomerId}).Select(m => m.First()).ToList();

                // if there's duplicate meter, only use the first one
                var distinctMeters = meters.GroupBy(m => m.MeterId).Select(m => m.First()).ToList();
                meters.Where(m => distinctMeters.Exists(
                    d => d.MeterId == m.MeterId && d.ClientId == m.ClientId && d.CustomerId != m.CustomerId &&
                         d.AccountId != m.AccountId)).ToList().ForEach(d => d.Status = "rejected");
                
                dt = ConvertToDataTable(meters);
                transScopeAction.Complete();
            }

            return dt;
        }


        private DataTable ConvertToDataTable<T>(IEnumerable<T> entities)
        {
            DataTable dtReturn = new DataTable("SubscribedMeters");

            // column names
            PropertyInfo[] oProps = null;

            if (entities == null) return dtReturn;

            foreach (T rec in entities)
            {
                // Use reflection to get property names, to create table, Only first time, others will follow
                if (oProps == null)
                {
                    oProps = rec.GetType().GetProperties();
                    foreach (PropertyInfo pi in oProps)
                    {
                        Type colType = pi.PropertyType;

                        if ((colType.IsGenericType) && (colType.GetGenericTypeDefinition() == typeof(Nullable<>)))
                        {
                            colType = colType.GetGenericArguments()[0];
                        }
                        dtReturn.Columns.Add(new DataColumn(pi.Name, colType));
                    }
                }

                DataRow dr = dtReturn.NewRow();
                foreach (PropertyInfo pi in oProps)
                {
                    dr[pi.Name] = pi.GetValue(rec, null) == null ? DBNull.Value : pi.GetValue
                        (rec, null);
                }
                dtReturn.Rows.Add(dr);
            }
            return dtReturn;
        }
    }
}
