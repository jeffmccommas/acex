﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using AO.TimeSeriesProcessing;

namespace AO.GreenButtonConnect.IngestionUtil
{
    public class IngestionReport
    {
        private readonly string _reportDesDir;
        private readonly string _reportLocalDir;
        private readonly DateTime _reportGenerateDate = DateTime.UtcNow;
        public IngestionReport(int clientId, string reportLocalDir, string reportDesDir)
        {
            _reportLocalDir = reportLocalDir;
            _reportDesDir = reportDesDir.Replace("{%clientid%}", clientId.ToString());
        }
        public bool GenerateReport(ContextData context, ref DataTable data)
        {
            var result = false;
            try
            {
                var meters = data.AsEnumerable();
                if (meters.Any())
                {
                    // create local copy
                    var reportFile = $"{_reportLocalDir}AMIProcessReport_{DateTime.Now:MMddyyyy-HHmmss.ffff}.csv";
                    result = CreateReport(context, meters.ToList(), reportFile);
                    if (result)
                        context.ContextWrite("INFO", $"Report file is generated at local dir {reportFile}");
                    // create destination copy if provided
                    if (!string.IsNullOrEmpty(_reportDesDir))
                    {
                        reportFile = $"{_reportDesDir}AMIProcessReport_{DateTime.Now:MMddyyyy-HHmmss.ffff}.csv";
                        var desResult = CreateReport(context, meters.ToList(), reportFile);
                        if (desResult)
                        {
                            context.ContextWrite("INFO", $"Report file is generated at destination dir {reportFile}");
                        }
                    }
                    // validation log
                    var total = meters.Count();
                    var hasError = meters.Count(m => m.Field<int>("ErrorCount") > 0);
                    var isMeterRejected = meters.Count(m => m.Field<string>("Status") == "rejected");

                    var dataLoaded = meters.Count(
                        s => s.Field<DateTime>("Updated") != s.Field<DateTime>("Created") &&
                             s.Field<int>("IngestedReadingsCount") > 0 &&
                             s.Field<DateTime>("Updated") > _reportGenerateDate.AddHours(-24) &&
                             s.Field<int>("ErrorCount") == 0);

                    var dataNotLoaded = meters.Count(s => s.Field<string>("Status") != "rejected" && s.Field<int>("ErrorCount") == 0 && (s.Field<DateTime>("Updated") == s.Field<DateTime>("Created") ||
                                                                                                                                         s.Field<int>("IngestedReadingsCount") == 0 ||
                                                                                                                                         s.Field<DateTime>("Updated") < _reportGenerateDate.AddHours(-24)));
                    
                    if(result)
                    {
                        // reset error count
                        foreach (var dataRow in meters)
                        {
                            dataRow["ErrorCount"] = 0;
                        }
                    }

                    context.Validation.AddLine(
                        $"Total number of meters: {total}");
                    context.Validation.AddLine($"Numbers of meter with AMI Data Load Successful: {dataLoaded}");
                    context.Validation.AddLine($"Numbers of Duplicate Meter Id: {isMeterRejected}");
                    context.Validation.AddLine($"Numbers of meter with AMI Data Load Error:  {hasError}");
                    context.Validation.AddLine($"Numbers of meter with No AMI data found:  {dataNotLoaded}");
                }
                else
                {
                    context.ContextWrite("INFO", "No data to generate report");
                }

            }
            catch (Exception ex)
            {
                // log error 
                context.ContextWrite("IngestionReport.GenerateReport-ERROR", $"Exception: {ex.Message} InnerException: {ex.InnerException?.Message}");
                return false;
            }

            return result;
        }


        private bool CreateReport(ContextData context, List<DataRow> data, string fileName)
        {
            try
            {
                var path = Path.GetDirectoryName(fileName);
                if (!Directory.Exists(path))
                {
                    if (path != null) Directory.CreateDirectory(path);
                }

                using (StreamWriter sw = File.CreateText(fileName))
                {
                    // header row
                    sw.WriteLine("Event Type,Event TimeStamp, Customer Name, Customer Account Number, Customer Meter Id, Authorization Start Date");
                    data.ForEach(s =>
                    {
                        var isMeterRejected = s.Field<string>("Status") == "rejected";
                        var hasError = s.Field<int>("ErrorCount") >0;
                        var dataLoaded = s.Field<DateTime>("Updated") != s.Field<DateTime>("Created") &&
                                         s.Field<int>("IngestedReadingsCount") > 0 &&
                                         s.Field<DateTime>("Updated") > _reportGenerateDate.AddHours(-24) &&
                                         s.Field<int>("ErrorCount") == 0;



                        var eventTimeStamp = s.Field<DateTime>("Updated");
                        var eventType = "AMI Data Load Successful";
                        if (isMeterRejected)
                            eventType = "Duplicate Meter Id";
                        else if (hasError)
                            eventType = "AMI Data Load Error";
                        else if (!dataLoaded)
                        {
                            eventType = "No AMI data Found";
                            // update event time to now because when there's no ami data, 
                            // the field Updated will not be updated.  
                            // It contains the update date for last ami data load
                            eventTimeStamp = _reportGenerateDate; 
                        }
                        var customerName = s.Field<string>("CustomerName");
                        var accountId = s.Field<string>("AccountId");
                        var meterId = s.Field<string>("MeterId");
                        var authDate = s.Field<DateTime>("AuthorizationDate");

                        sw.WriteLine($"{eventType},{eventTimeStamp},{customerName},{accountId},{meterId},{authDate}");
                    });


                }
            }
            catch (Exception ex)
            {
                // log error 
                context.ContextWrite("IngestionReport.CreateReport - ERROR", $"Exception: {ex.Message} InnerException: {ex.InnerException?.Message}");
                return false;
            }

            return true;
        }
    }
}
