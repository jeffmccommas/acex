﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using AO.BusinessContracts;
using CE.AO.Logging;
using CE.AO.Models;


namespace CE.AO.Analytics.CalculationWorker.Test
{
    /// <summary>
    /// Mock class for Calculation Facade
    /// </summary>
    public class CalculationFacadeMock : ICalculationFacade
    {
        /// <summary>
        /// Default constructor of CalculationFacadeMock
        /// </summary>
        public CalculationFacadeMock()
        {
            LogModel = new LogModel { DisableLog = false };
        }

        /// <summary>
        /// Parameterized constructor of CalculationFacadeMock
        /// </summary>
        public CalculationFacadeMock(LogModel logModel)
        {
            LogModel = logModel;
        }

        public LogModel LogModel { get; set; }

        /// <summary>
        /// Mock method CalculateCostToDate of CalculationFacadeMock
        /// </summary>
        /// <param name="clientId"></param>
        /// <param name="meterId"></param>
        /// <param name="asOfDate"></param>
        /// <returns></returns>
        public Task CalculateCostToDate(int clientId, string meterId, DateTime asOfDate)
        {
            return Task.FromResult(true);
        }

        /// <summary>
        /// Mock method CalculateBillToDate of CalculationFacadeMock
        /// </summary>
        public Task CalculateBillToDate(int clientId, string accountId, string customerId, DateTime asOfDate)
        {
            return Task.FromResult(true);
        }

        /// <summary>
        /// Mock method GetClientAccount of CalculationFacadeMock
        /// </summary>
        public IEnumerable<ClientAccountModel> GetClientAccount(int clientId)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Mock method ScheduledCalculation of CalculationFacadeMock
        /// </summary>
        public void ScheduledCalculation(int clientId, string asOfDate)
        {
            throw new NotImplementedException();
        }
    }
}
