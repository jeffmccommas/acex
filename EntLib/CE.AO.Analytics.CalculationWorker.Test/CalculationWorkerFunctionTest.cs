﻿using System.IO;
using System.Runtime.Remoting.Messaging;
using CE.AO.Analytics.CalculationWorker.Calculation;
using CE.AO.Business;
using CE.AO.Logging;
using CE.AO.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace CE.AO.Analytics.CalculationWorker.Test
{
    /// <summary>
    /// Test Case for CalculationWorker class
    /// </summary>
    [TestClass]
    public class CalculationWorkerFunctionTest
    {
        /// <summary>
        /// Test Case Init
        /// </summary>
        [TestInitialize]
        public void Init()
        {
            AutoMapperConfig.AutoMapperMappings();
        }

        /// <summary>
        /// Test Case for Process Calculations
        /// </summary>
        [TestMethod]
        public void ProcessCalculation()
        {
            var log = TextWriter.Null;
            Functions.LogModel = new LogModel
            {
                DisableLog = false,
                Module = Utilities.Enums.Module.Calculation
            };
            Functions.ProcessCalculation("87^^600^^09/24/2016^^Source^^22^^Metadata^^2", log);
            Assert.AreEqual(true, true);
        }

        /// <summary>
        /// Test Case for Process Calculations For Else Statement
        /// </summary>
        [TestMethod]
        public void ProcessCalculation_ForElse()
        {
            var log = TextWriter.Null;
            Functions.LogModel = new LogModel
            {
                DisableLog = false,
                Module = Utilities.Enums.Module.Calculation
            };
            Functions.ProcessCalculation("87^^600^^09/24/2016^^Source^^22", log);
            Assert.AreEqual(true, true);
        }

        /// <summary>
        /// Test Case for Process Calculations For Catch Statement
        /// </summary>
        [TestMethod]
        public void ProcessCalculation_ForCatch()
        {
            var log = TextWriter.Null;
            Functions.LogModel = new LogModel
            {
                DisableLog = false,
                Module = Utilities.Enums.Module.Calculation
            };
            try
            {
                Functions.ProcessCalculation("87^^09/24/2016^^600^^Source^^22^^Metadata^^2", log);
            }
            catch
            {
                Assert.AreEqual(true, true);
            }
        }

        /// <summary>
        /// Test Case for Scheduled Calculation
        /// </summary>
        [TestMethod]
        public void ScheduledCalculation()
        {
            var log = TextWriter.Null;
            Functions.LogModel = new LogModel
            {
                DisableLog = false,
                Module = Utilities.Enums.Module.Calculation
            };
            Functions.ProcessCalculation("87^^600^^09/24/2016^^Source^^22^^Metadata^^2", log);
            Assert.AreEqual(true, true);
        }

        /// <summary>
        /// Test Case for Scheduled Calculation for Else Statement
        /// </summary>
        [TestMethod]
        public void ScheduledCalculation_ForElse()
        {
            var log = TextWriter.Null;
            Functions.LogModel = new LogModel
            {
                DisableLog = false,
                Module = Utilities.Enums.Module.Calculation
            };
            Functions.ScheduledCalculation("09/24/2016^^87^^1", log);
            Assert.AreEqual(true, true);
        }

        /// <summary>
        /// Test Case for Scheduled Calculation for Catch Statement
        /// </summary>
        [TestMethod]
        public void ScheduledCalculation_ForCatch()
        {
            var log = TextWriter.Null;
            try
            {
                Functions.LogModel = new LogModel
                {
                    DisableLog = false,
                    Module = Utilities.Enums.Module.Calculation
                };
                Functions.ScheduledCalculation("09/24/2016^^87", log);
            }
            catch
            {
                Assert.AreEqual(true, true);
            }
        }

        /// <summary>
        /// Test Case for Process Schedule  Calculation
        /// </summary>
        [TestMethod]
        public void ProcessScheduleCalculation()
        {
            var log = TextWriter.Null;
            Functions.LogModel = new LogModel
            {
                DisableLog = false,
                Module = Utilities.Enums.Module.Calculation
            };
            Functions.ProcessScheduleCalculation("9^^87^^500^^09/24/2016", log);
            Assert.AreEqual(true, true);
        }


        /// <summary>
        /// Test Case for Process Process Scheduled Calculations for Else
        /// </summary>
        [TestMethod]
        public void ProcessScheduleCalculation_ForElse()
        {
            var log = TextWriter.Null;
            Functions.LogModel = new LogModel
            {
                DisableLog = false,
                Module = Utilities.Enums.Module.Calculation
            };
            Functions.ProcessScheduleCalculation("9^^87^^500", log);
            Assert.AreEqual(true, true);
        }

        /// <summary>
        /// Test Case for Process Process Scheduled Calculations for Catch
        /// </summary>
        [TestMethod]
        public void ProcessScheduleCalculation_ForCatch()
        {
            var log = TextWriter.Null;
            try
            {
                Functions.LogModel = new LogModel
                {
                    DisableLog = false,
                    Module = Utilities.Enums.Module.Calculation
                };
                Functions.ProcessCalculation("9^^87^^09/21/2016^^500", log);
            }
            catch
            {
                Assert.AreEqual(true, true);
            }
        }

        [TestMethod]
        public void ContentFull()
        {
            var cf = new ContentFul();
            var rateClassCompanyId = cf.GetRateClass("2");
            Assert.AreEqual(rateClassCompanyId, "RateClassCompanyId");
        }

        /// <summary>
        /// Cleanup Method To Release Log4Net Data Resources hold by Any Test Method
        /// </summary>
        [TestCleanup]
        public void CleanUpMethod()
        {
            CallContext.FreeNamedDataSlot("log4net.Util.LogicalThreadContextProperties");
        }
    }
}
