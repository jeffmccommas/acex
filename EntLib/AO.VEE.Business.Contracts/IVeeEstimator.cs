﻿namespace AO.VEE.Business.Contracts
{
	public interface IVeeEstimator
	{
		void Estimate();
	}
}