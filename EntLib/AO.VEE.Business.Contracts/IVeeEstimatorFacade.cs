﻿using System;
using System.Collections.Generic;
using AO.VEE.DTO;

namespace AO.VEE.Business.Contracts
{
	/// <summary>
	/// VeeEstimatorFacade class used to do estimation for invalid data
	/// </summary>
	public interface IVeeEstimatorFacade
	{
		/// <summary>
		/// To do estimation for invalid data
		/// </summary>
		/// <param name="veeValidationDtoList">veeValidationDtoList to be estimated</param>
		/// <param name="validator">validator type</param>
		void Estimate(List<VeeConsumptionByMeterDto> veeValidationDtoList,Type validator);
	}
}
