﻿using AO.VEE.DTO;
using CE.AO.Models;

namespace AO.VEE.Business.Contracts
{
	/// <summary>
	/// To do validation & estimation on the data
	/// </summary>
	public interface IVeeFacade
    {
		/// <summary>
		/// To insert or update Vee staged data to database
		/// </summary>
		/// <param name="veeStagedClientMeterByDateDto">model to be inserted</param>
		void InsertOrMergeVeeStagedDataAsync(VeeStagedClientMeterByDateDto veeStagedClientMeterByDateDto);

        /// <summary>
        /// To do validation and estimation process
        /// </summary>
        /// <param name="veeValidationEstimationTriggerDto">VeeValidationEstimationTriggerDto to fetch consumption details</param>
        void ExecuteValidationsAndEstimations(VeeValidationEstimationTriggerDto veeValidationEstimationTriggerDto);

        /// <summary>
        /// For logging info,warning,error.
        /// </summary>
        LogModel LogModel { get; set; }
    }
}
