﻿using System.Collections.Generic;
using AO.VEE.DTO;

namespace AO.VEE.Business.Contracts
{
	/// <summary>
	/// IVeeValidator interface is used for validating the data
	/// </summary>
	public interface IVeeValidator
	{
		int SequenceId { get; set; }

		/// <summary>
		/// To validate list of VeeConsumptionByMeterDto against negative value check
		/// </summary>
		/// <param name="veeConsumptions">List of VeeConsumptionByMeterDto to be validated</param>
		/// <param name="veeConfigurations">Configuration required for negative value check validation</param>
		/// <returns>True; if no negative check, else False</returns>
		bool Validate(List<VeeConsumptionByMeterDto> veeConsumptions, VeeConfigurationsDto veeConfigurations);
	}
}