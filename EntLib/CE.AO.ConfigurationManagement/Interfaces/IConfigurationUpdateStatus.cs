﻿using Aclara.Tools.Common.StatusManagement;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CE.AO.ConfigurationManagement.Interfaces
{

    /// <summary>
    /// Generate client test data status interface.
    /// </summary>
    public interface IConfigurationUpdateStatus
    {
        string StatusMessage { get; set; }
        StatusList StatusList { get; }
    }
}
