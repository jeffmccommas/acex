﻿using Aclara.Tools.Common.StatusManagement;
using CE.AO.ConfigurationManagement.Entities;
using CE.AO.ConfigurationManagement.Events;
using CE.AO.ConfigurationManagement.Interfaces;
using CE.AO.ConfigurationManagement.Results;
using DataAccess;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Blob;
using Microsoft.WindowsAzure.Storage.Table;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using static CE.AO.ConfigurationManagement.Types.Enumerations;

//using Microsoft.WindowsAzure.StorageClient;

namespace CE.AO.ConfigurationManagement
{

    /// <summary>
    /// Configuration manager.
    /// </summary>
    public class ConfigurationManager
    {

        #region Private Constants

        private const string PartitionKey = "PartitionKey";
        private const string RowKey = "RowKey";
        private const string Body = "Body";
        private const string ClientId = "ClientId";
        private const string InsightTypeName = "InsightTypeName";
        private const string TemplateId = "TemplateId";
        private const string BeginDate = "BeginDate";
        private const string EndDate = "EndDate";
        private const string Description = "Description";
        private const string WebTokenUrl = "WebTokenUrl";
        private const string WidgetPageUrl = "WidgetPageUrl";
        private const string AccessKeyId = "AccessKeyId";
        private const string Password = "Password";
        private const string TrumpiaKeyword = "TrumpiaKeyword";
        private const string BillCycleScheduleName = "BillCycleScheduleName";

        private const string PartitionRowKeyReqired = "Partition key and/or Row Key is required.";
        #endregion

        #region Private Data Members

        private CloudStorageAccount account;

        #endregion

        #region Public Delegates

        public event EventHandler<ConfigurationUpdateStatusEventArgs> ConfigurationUpdateStatus;
        public event EventHandler<ConfigurationUpdateResultEventArgs> ConfigurationUpdated;

        #endregion

        #region Public Properties


        #endregion

        #region Public Constructors

        /// <summary>
        /// Preferred constructor.
        /// </summary>
        public ConfigurationManager(string azureStorageAccountConnectionString)
        {
            account = CloudStorageAccount.Parse(azureStorageAccountConnectionString);
        }

        #endregion

        #region Private Constructors

        #endregion

        #region Public Methods

        /// <summary>
        /// Update configuration.
        /// </summary>
        /// <param name="azureStorageTableName"></param>
        /// <param name="azureStorageAccountAccessKey"></param>
        /// <param name="azureTable"></param>
        /// <param name="configurationCsvPathFileName"></param>
        /// <param name="validateConfigurationCsvFile"></param>
        /// <param name="cancellationToken"></param>
        public void UpdateAzureTableConfiguration(string azureStorageTableName,
                                                  string azureStorageAccountAccessKey,
                                                  AzureTable azureTable,
                                                  string configurationCsvPathFileName,
                                                  bool validateConfigurationCsvFile,
                                                  CancellationToken cancellationToken)
        {
            IConfigurationUpdateResult configurationUpdateResult;
            ConfigurationUpdateResultEventArgs configurationUpdateResultEventArgs;
            string resultMessage;
            string azureTableConfiguratonName = string.Empty;

            try
            {

                switch (azureTable)
                {
                    case AzureTable.Unspecified:
                        HandleUnexpectedAzureTable(azureTable);
                        break;
                    case AzureTable.BillCycleSchedule:
                        UpdateAzureTableBillCycleSchedule(configurationCsvPathFileName,
                                                          validateConfigurationCsvFile,
                                                          cancellationToken);
                        break;
                    case AzureTable.ClientDomain:
                        UpdateAzureTableClientDomain(configurationCsvPathFileName,
                                                     validateConfigurationCsvFile,
                                                     cancellationToken);
                        break;
                    case AzureTable.ClientWidgetConfiguration:
                        UpdateAzureTableClientWidgetConfiguration(configurationCsvPathFileName,
                                                                  validateConfigurationCsvFile,
                                                                  cancellationToken);
                        break;
                    case AzureTable.SmsTemplate:
                        UpdateAzureTableSmsTemplate(configurationCsvPathFileName,
                                                    validateConfigurationCsvFile,
                                                    cancellationToken);
                        break;
                    case AzureTable.TrumpiaKeywordConfiguration:
                        UpdateAzureTableTrumpiaKeywordConfiguration(configurationCsvPathFileName,
                                                                    validateConfigurationCsvFile,
                                                                    cancellationToken);
                        break;
                }

                //Report: Azure table configuration update completed.
                configurationUpdateResult = ConfigurationUpdateResultFactory.CreateConfigurationUpdateResult();
                azureTableConfiguratonName = Enum.GetName(typeof(AzureTable), azureTable);
                resultMessage = string.Format("Azure Table Configuration Update Completed (Reminder: Check for errors). (Azure table name: {0})",
                                               azureTableConfiguratonName);
                configurationUpdateResult.ResultMessage = resultMessage;
                configurationUpdateResultEventArgs = new ConfigurationUpdateResultEventArgs();
                configurationUpdateResultEventArgs.ConfigurationUpdateResult = configurationUpdateResult;
                if (ConfigurationUpdated != null)
                {
                    ConfigurationUpdated(this, configurationUpdateResultEventArgs);
                }

            }
            catch (OperationCanceledException)
            {
                throw;
            }
            catch (Exception ex)
            {
                Status status;
                configurationUpdateResult = ConfigurationUpdateResultFactory.CreateConfigurationUpdateResult();
                configurationUpdateResultEventArgs = new ConfigurationUpdateResultEventArgs();

                status = new Status();
                status.Exception = ex;
                status.StatusServerity = StatusTypes.StatusSeverity.Error;
                configurationUpdateResult.StatusList.Add(status);
                configurationUpdateResultEventArgs.ConfigurationUpdateResult = configurationUpdateResult;
                if (ConfigurationUpdated != null)
                {
                    ConfigurationUpdated(this, configurationUpdateResultEventArgs);
                }
            }

        }

        /// <summary>
        /// Creating a new Azure blob container
        /// </summary>
        /// <param name="azureStorageAccountAccessKey"></param>
        /// <param name="containerName"></param>
        /// <param name="cancellationToken"></param>

        public void CreateAzureBlobContainer(string azureStorageAccountAccessKey, string containerName, CancellationToken cancellationToken)
        {
            IConfigurationUpdateResult configurationUpdateResult;
            ConfigurationUpdateResultEventArgs configurationUpdateResultEventArgs;
            ConfigurationUpdateStatusEventArgs configurationUpdateStatusEventArgs;
            IConfigurationUpdateStatus configurationUpdateStatus;


            try
            {
                //Check for cancellation.
                cancellationToken.ThrowIfCancellationRequested();

                //Retrieve the storage account.
                configurationUpdateStatusEventArgs = new ConfigurationUpdateStatusEventArgs();
                CloudStorageAccount storageAccount = CloudStorageAccount.Parse(azureStorageAccountAccessKey);

                //Check if the storage account has been retrieved.
                if (storageAccount == null)
                {
                    var statusMessage =
                            $"Storage account with the access key could not be retrieved. (Access key: {azureStorageAccountAccessKey})";
                    throw new ArgumentNullException(statusMessage);
                }
                CloudBlobClient blobClient = storageAccount.CreateCloudBlobClient();
                CloudBlobContainer container = blobClient.GetContainerReference(containerName);

                //Check if a container with the same name exists, else create it.
                if (container.Exists())
                {
                    var statusMessage =
                            $"Container with the name {containerName} already exists.";
                    configurationUpdateStatus = ConfigurationUpdateStatusFactory.CreateConfigurationUpdateStatus();
                    configurationUpdateStatus.StatusMessage = statusMessage;
                    configurationUpdateStatusEventArgs.ConfigurationUpdateStatus = configurationUpdateStatus;
                }
                else
                {
                    container.CreateIfNotExists();
                    var statusMessage =
                            $"Container with the name {containerName} created.";
                    configurationUpdateStatus = ConfigurationUpdateStatusFactory.CreateConfigurationUpdateStatus();
                    configurationUpdateStatus.StatusMessage = statusMessage;
                    configurationUpdateStatusEventArgs.ConfigurationUpdateStatus = configurationUpdateStatus;
                }


                if (ConfigurationUpdateStatus != null)
                {
                    ConfigurationUpdateStatus(this, configurationUpdateStatusEventArgs);
                }

            }
            catch (OperationCanceledException)
            {
                throw;
            }
            catch (Exception ex)
            {
                Status status;
                configurationUpdateResult = ConfigurationUpdateResultFactory.CreateConfigurationUpdateResult();
                configurationUpdateResultEventArgs = new ConfigurationUpdateResultEventArgs();

                status = new Status();
                status.Exception = ex;
                status.StatusServerity = StatusTypes.StatusSeverity.Error;
                configurationUpdateResult.StatusList.Add(status);
                configurationUpdateResultEventArgs.ConfigurationUpdateResult = configurationUpdateResult;
                if (ConfigurationUpdated != null)
                {
                    ConfigurationUpdated(this, configurationUpdateResultEventArgs);
                }
            }

        }

        #endregion

        #region Protected methods

        /// <summary>
        /// Update Azure table: Bill Cycle Schedule.
        /// </summary>
        /// <param name="configurationCsvPathFileName"></param>
        /// <param name="validateConfigurationCsvFile"></param>
        /// <param name="cancellationToken"></param>
        protected void UpdateAzureTableBillCycleSchedule(string configurationCsvPathFileName,
                                                         bool validateConfigurationCsvFile,
                                                         CancellationToken cancellationToken)
        {
            IConfigurationUpdateResult configurationUpdateResult;
            ConfigurationUpdateStatusEventArgs configurationUpdateStatusEventArgs;
            IConfigurationUpdateStatus configurationUpdateStatus;
            ConfigurationUpdateResultEventArgs configurationUpdateResultEventArgs;
            string errorMessage = string.Empty;

            try
            {
                //Check for cancellation.
                cancellationToken.ThrowIfCancellationRequested();

                // Create the table client.
                CloudTableClient tableClient = account.CreateCloudTableClient();

                // Retrieve a reference to the table.
                CloudTable table = tableClient.GetTableReference(AzureTable.BillCycleSchedule.ToString());

                // Create the table if it doesn't exist.
                table.CreateIfNotExists();

                var dt = DataTable.New.Read(configurationCsvPathFileName);

                //Validate bill cycle schedule configuration file.
                if (validateConfigurationCsvFile == true && 
                    ValidateBillCycleScheduleConfigFile(dt, ref errorMessage) == false)
                {
                    throw new ArgumentOutOfRangeException(string.Format(errorMessage));
                }

                if (!dt.ColumnNames.ToList().Exists(n => n == PartitionKey) && !dt.ColumnNames.ToList().Exists(n => n == RowKey))
                {
                    throw new ArgumentOutOfRangeException(PartitionRowKeyReqired);
                }

                foreach (var dr in dt.Rows)
                {

                    var billCycleScheduleEntity = new BillCycleScheduleEntity();

                    var names = dr.ColumnNames.ToList();


                    foreach (var name in names)
                    {
                        var value = dr.GetValueOrEmpty(name);
                        if (name.Contains(PartitionKey))
                            billCycleScheduleEntity.PartitionKey = value;
                        else if (name.Contains(RowKey))
                            billCycleScheduleEntity.RowKey = Convert.ToDateTime(value).ToString("yyyy-MM-dd");
                        else if (name.Contains(BeginDate))
                            billCycleScheduleEntity.BeginDate = Convert.ToDateTime(value).ToString("MM-dd-yyyy");
                        else if (name.Contains(ClientId))
                            billCycleScheduleEntity.ClientId = value;
                        else if (name.Contains(EndDate))
                            billCycleScheduleEntity.EndDate = Convert.ToDateTime(value).ToString("MM-dd-yyyy");
                        else if (name.Contains(Description))
                            billCycleScheduleEntity.Description = value;
                        else if (name.Contains(BillCycleScheduleName))
                            billCycleScheduleEntity.BillCycleScheduleName = value;
                    }
                    if (!string.IsNullOrEmpty(billCycleScheduleEntity.PartitionKey) &&
                        !string.IsNullOrEmpty(billCycleScheduleEntity.RowKey))
                    {
                        // insert or update
                        TableOperation insertOrReplaceOperation = TableOperation.InsertOrReplace(billCycleScheduleEntity);

                        table.Execute(insertOrReplaceOperation);

                        //Report: Status.
                        configurationUpdateStatusEventArgs = new ConfigurationUpdateStatusEventArgs();
                        var statusMessage =
                            $"Bill Cycle Schedule for Client Cycle {billCycleScheduleEntity.PartitionKey} updated.";

                        configurationUpdateStatus = ConfigurationUpdateStatusFactory.CreateConfigurationUpdateStatus();
                        configurationUpdateStatus.StatusMessage = statusMessage;
                        configurationUpdateStatusEventArgs.ConfigurationUpdateStatus = configurationUpdateStatus;

                        if (ConfigurationUpdateStatus != null)
                        {
                            ConfigurationUpdateStatus(this, configurationUpdateStatusEventArgs);
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                Status status;
                configurationUpdateResult = ConfigurationUpdateResultFactory.CreateConfigurationUpdateResult();
                configurationUpdateResultEventArgs = new ConfigurationUpdateResultEventArgs();

                status = new Status();
                status.Exception = ex;
                status.StatusServerity = StatusTypes.StatusSeverity.Error;
                configurationUpdateResult.StatusList.Add(status);
                configurationUpdateResultEventArgs.ConfigurationUpdateResult = configurationUpdateResult;
                if (ConfigurationUpdated != null)
                {
                    ConfigurationUpdated(this, configurationUpdateResultEventArgs);
                }
            }
        }


        /// <summary>
        /// Update Azure table: Client Domain.
        /// </summary>
        /// <param name="configurationCsvPathFileName"></param>
        /// <param name="validateConfigurationCsvFile"></param>
        /// <param name="cancellationToken"></param>
        protected void UpdateAzureTableClientDomain(string configurationCsvPathFileName,
                                                    bool validateConfigurationCsvFile,
                                                    CancellationToken cancellationToken)
        {
            IConfigurationUpdateResult configurationUpdateResult;
            ConfigurationUpdateStatusEventArgs configurationUpdateStatusEventArgs;
            IConfigurationUpdateStatus configurationUpdateStatus;
            ConfigurationUpdateResultEventArgs configurationUpdateResultEventArgs;
            string errorMessage = string.Empty;

            try
            {
                // Create the table client.
                CloudTableClient tableClient = account.CreateCloudTableClient();

                // Retrieve a reference to the table.
                CloudTable table = tableClient.GetTableReference(AzureTable.ClientDomain.ToString());

                // Create the table if it doesn't exist.
                table.CreateIfNotExists();

                var dt = DataTable.New.Read(configurationCsvPathFileName);

                //Validate client domain configuration file.
                if (validateConfigurationCsvFile == true &&
                    ValidateClientDomainConfigFile(dt, ref errorMessage) == false)
                {
                    throw new ArgumentOutOfRangeException(string.Format(errorMessage));
                }

                if (!dt.ColumnNames.ToList().Exists(n => n == PartitionKey) && !dt.ColumnNames.ToList().Exists(n => n == RowKey))
                {
                    throw new ArgumentOutOfRangeException(PartitionRowKeyReqired);
                }

                foreach (var dr in dt.Rows)
                {
                    //Check for cancellation.
                    cancellationToken.ThrowIfCancellationRequested();

                    var clientDomainEntity = new ClientDomainEntity();

                    var names = dr.ColumnNames.ToList();


                    foreach (var name in names)
                    {
                        var value = dr.GetValueOrEmpty(name);
                        if (name.Contains(PartitionKey))
                            clientDomainEntity.PartitionKey = value;
                        else if (name.Contains(RowKey))
                            clientDomainEntity.RowKey = value;
                    }
                    if (!string.IsNullOrEmpty(clientDomainEntity.PartitionKey) && !string.IsNullOrEmpty(clientDomainEntity.RowKey))
                    {
                        // insert or update
                        TableOperation insertOrReplaceOperation = TableOperation.InsertOrReplace(clientDomainEntity);

                        table.Execute(insertOrReplaceOperation);

                        //Report: Status.
                        configurationUpdateStatusEventArgs = new ConfigurationUpdateStatusEventArgs();
                        var statusMessage =
                                 $"Client Domain for Client Id {clientDomainEntity.PartitionKey} updated.";

                        configurationUpdateStatus = ConfigurationUpdateStatusFactory.CreateConfigurationUpdateStatus();
                        configurationUpdateStatus.StatusMessage = statusMessage;
                        configurationUpdateStatusEventArgs.ConfigurationUpdateStatus = configurationUpdateStatus;

                        if (ConfigurationUpdateStatus != null)
                        {
                            ConfigurationUpdateStatus(this, configurationUpdateStatusEventArgs);
                        }
                    }
                }


            }
            catch (Exception ex)
            {
                Status status;
                configurationUpdateResult = ConfigurationUpdateResultFactory.CreateConfigurationUpdateResult();
                configurationUpdateResultEventArgs = new ConfigurationUpdateResultEventArgs();

                status = new Status();
                status.Exception = ex;
                status.StatusServerity = StatusTypes.StatusSeverity.Error;
                configurationUpdateResult.StatusList.Add(status);
                configurationUpdateResultEventArgs.ConfigurationUpdateResult = configurationUpdateResult;
                if (ConfigurationUpdated != null)
                {
                    ConfigurationUpdated(this, configurationUpdateResultEventArgs);
                }
            }
        }


        /// <summary>
        /// Update Azure table: Client Widget Configuration.
        /// </summary>
        /// <param name="configurationCsvPathFileName"></param>
        /// <param name="validateConfigurationCsvFile"></param>
        /// <param name="cancellationToken"></param>
        protected void UpdateAzureTableClientWidgetConfiguration(string configurationCsvPathFileName,
                                                                 bool validateConfigurationCsvFile,
                                                                 CancellationToken cancellationToken)
        {
            IConfigurationUpdateResult configurationUpdateResult;
            ConfigurationUpdateStatusEventArgs configurationUpdateStatusEventArgs;
            IConfigurationUpdateStatus configurationUpdateStatus;
            ConfigurationUpdateResultEventArgs configurationUpdateResultEventArgs;
            string errorMessage = string.Empty;

            try
            {
                // Create the table client.
                CloudTableClient tableClient = account.CreateCloudTableClient();

                // Retrieve a reference to the table.
                CloudTable table = tableClient.GetTableReference(AzureTable.ClientWidgetConfiguration.ToString());

                // Create the table if it doesn't exist.
                table.CreateIfNotExists();

                var dt = DataTable.New.Read(configurationCsvPathFileName);

                //Validate configuration file.
                if (validateConfigurationCsvFile == true &&
                    ValidateClientWidgetConfigurationConfigFile(dt, ref errorMessage) == false)
                {
                    throw new ArgumentOutOfRangeException(string.Format(errorMessage));
                }

                if (!dt.ColumnNames.ToList().Exists(n => n == PartitionKey) && !dt.ColumnNames.ToList().Exists(n => n == RowKey))
                {
                    throw new ArgumentOutOfRangeException(PartitionRowKeyReqired);
                }

                foreach (var dr in dt.Rows)
                {
                    //Check for cancellation.
                    cancellationToken.ThrowIfCancellationRequested();

                    var clientWidgetConfigurationEntity = new ClientWidgetConfigurationEntity();

                    var names = dr.ColumnNames.ToList();


                    foreach (var name in names)
                    {
                        var value = dr.GetValueOrEmpty(name);
                        if (name.Contains(PartitionKey))
                            clientWidgetConfigurationEntity.PartitionKey = value;
                        else if (name.Contains(RowKey))
                            clientWidgetConfigurationEntity.RowKey = value;
                        else if (name.Contains(WebTokenUrl))
                            clientWidgetConfigurationEntity.WebTokenUrl = value;
                        else if (name.Contains(ClientId))
                            clientWidgetConfigurationEntity.ClientId = Convert.ToInt32(value);
                        else if (name.Contains(WidgetPageUrl))
                            clientWidgetConfigurationEntity.WidgetPageUrl = value;
                        else if (name.Contains(AccessKeyId))
                            clientWidgetConfigurationEntity.AccessKeyId = value;
                        else if (name.Contains(Password))
                            clientWidgetConfigurationEntity.Password = value;
                    }
                    if (!string.IsNullOrEmpty(clientWidgetConfigurationEntity.PartitionKey) &&
                        !string.IsNullOrEmpty(clientWidgetConfigurationEntity.RowKey))
                    {
                        // insert or update
                        TableOperation insertOrReplaceOperation =
                            TableOperation.InsertOrReplace(clientWidgetConfigurationEntity);

                        table.Execute(insertOrReplaceOperation);

                        //Report: Status.
                        configurationUpdateStatusEventArgs = new ConfigurationUpdateStatusEventArgs();
                        var statusMessage =
                            $"Client Widget Configuration for Client Id {clientWidgetConfigurationEntity.ClientId} updated.";

                        configurationUpdateStatus = ConfigurationUpdateStatusFactory.CreateConfigurationUpdateStatus();
                        configurationUpdateStatus.StatusMessage = statusMessage;
                        configurationUpdateStatusEventArgs.ConfigurationUpdateStatus = configurationUpdateStatus;

                        if (ConfigurationUpdateStatus != null)
                        {
                            ConfigurationUpdateStatus(this, configurationUpdateStatusEventArgs);
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                Status status;
                configurationUpdateResult = ConfigurationUpdateResultFactory.CreateConfigurationUpdateResult();
                configurationUpdateResultEventArgs = new ConfigurationUpdateResultEventArgs();

                status = new Status();
                status.Exception = ex;
                status.StatusServerity = StatusTypes.StatusSeverity.Error;
                configurationUpdateResult.StatusList.Add(status);
                configurationUpdateResultEventArgs.ConfigurationUpdateResult = configurationUpdateResult;
                if (ConfigurationUpdated != null)
                {
                    ConfigurationUpdated(this, configurationUpdateResultEventArgs);
                }
            }
        }

        /// <summary>
        /// Update Azure table: SMS Template.
        /// </summary>
        /// <param name="configurationCsvPathFileName"></param>
        /// <param name="validateConfigurationCsvFile"></param>
        /// <param name="cancellationToken"></param>
        protected void UpdateAzureTableSmsTemplate(string configurationCsvPathFileName,
                                                   bool validateConfigurationCsvFile,
                                                   CancellationToken cancellationToken)
        {
            IConfigurationUpdateResult configurationUpdateResult;
            ConfigurationUpdateStatusEventArgs configurationUpdateStatusEventArgs;
            IConfigurationUpdateStatus configurationUpdateStatus;
            ConfigurationUpdateResultEventArgs configurationUpdateResultEventArgs;
            string errorMessage = string.Empty;

            try
            {
                // Create the table client.
                CloudTableClient tableClient = account.CreateCloudTableClient();

                // Retrieve a reference to the table.
                CloudTable table = tableClient.GetTableReference(AzureTable.SmsTemplate.ToString());

                // Create the table if it doesn't exist.
                table.CreateIfNotExists();

                var dt = DataTable.New.Read(configurationCsvPathFileName);

                //Validate configuration file.
                if (validateConfigurationCsvFile == true &&
                    ValidateSmsTemplateConfigFile(dt, ref errorMessage) == false)
                {
                    throw new ArgumentOutOfRangeException(string.Format(errorMessage));
                }

                foreach (var dr in dt.Rows)
                {
                    //Check for cancellation.
                    cancellationToken.ThrowIfCancellationRequested();

                    var smsEntity = new SmsTemplateEntity();

                    var names = dr.ColumnNames.ToList();


                    foreach (var name in names)
                    {
                        var value = dr.GetValueOrEmpty(name);
                        if (name.Contains(PartitionKey))
                            smsEntity.PartitionKey = value;
                        else if (name.Contains(RowKey))
                            smsEntity.RowKey = value;
                        else if (name.Contains(Body))
                            smsEntity.Body = value;
                        else if (name.Contains(ClientId))
                            smsEntity.ClientId = Convert.ToInt32(value);
                        else if (name.Contains(InsightTypeName))
                            smsEntity.InsightTypeName = value;
                        else if (name.Contains(TemplateId))
                            smsEntity.TemplateId = value;
                    }

                    if (!names.Exists(n => n == PartitionKey))
                    {
                        smsEntity.PartitionKey = SmsTemplateEntity.SetSmsTemplatePartitionKey(smsEntity.ClientId, smsEntity.InsightTypeName);
                    }
                    if (!names.Exists(n => n == RowKey))
                    {
                        smsEntity.RowKey = SmsTemplateEntity.SetSmsTemplateRowKey(smsEntity.TemplateId);
                    }

                    if (!string.IsNullOrEmpty(smsEntity.InsightTypeName) &&
                        !string.IsNullOrEmpty(smsEntity.TemplateId))
                    {

                        // insert or update
                        TableOperation insertOrReplaceOperation = TableOperation.InsertOrReplace(smsEntity);

                        table.Execute(insertOrReplaceOperation);

                        //Report: Status.
                        configurationUpdateStatusEventArgs = new ConfigurationUpdateStatusEventArgs();
                        var statusMessage =
                            $"Sms Tempate for {smsEntity.PartitionKey} updated.";

                        configurationUpdateStatus = ConfigurationUpdateStatusFactory.CreateConfigurationUpdateStatus();
                        configurationUpdateStatus.StatusMessage = statusMessage;
                        configurationUpdateStatusEventArgs.ConfigurationUpdateStatus = configurationUpdateStatus;

                        if (ConfigurationUpdateStatus != null)
                        {
                            ConfigurationUpdateStatus(this, configurationUpdateStatusEventArgs);
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                Status status;
                configurationUpdateResult = ConfigurationUpdateResultFactory.CreateConfigurationUpdateResult();
                configurationUpdateResultEventArgs = new ConfigurationUpdateResultEventArgs();

                status = new Status();
                status.Exception = ex;
                status.StatusServerity = StatusTypes.StatusSeverity.Error;
                configurationUpdateResult.StatusList.Add(status);
                configurationUpdateResultEventArgs.ConfigurationUpdateResult = configurationUpdateResult;
                if (ConfigurationUpdated != null)
                {
                    ConfigurationUpdated(this, configurationUpdateResultEventArgs);
                }
            }
        }

        /// <summary>
        /// Update Azure table: Trumpia Keyword Configuration.
        /// </summary>
        /// <param name="configurationCsvPathFileName"></param>
        /// <param name="validateConfigurationCsvFile"></param>
        /// <param name="cancellationToken"></param>
        protected void UpdateAzureTableTrumpiaKeywordConfiguration(string configurationCsvPathFileName,
                                                                   bool validateConfigurationCsvFile,
                                                                   CancellationToken cancellationToken)
        {
            IConfigurationUpdateResult configurationUpdateResult;
            ConfigurationUpdateStatusEventArgs configurationUpdateStatusEventArgs;
            IConfigurationUpdateStatus configurationUpdateStatus;
            ConfigurationUpdateResultEventArgs configurationUpdateResultEventArgs;
            string errorMessage = string.Empty;

            try
            {
                // Create the table client.
                CloudTableClient tableClient = account.CreateCloudTableClient();

                // Retrieve a reference to the table.
                CloudTable table = tableClient.GetTableReference(AzureTable.TrumpiaKeywordConfiguration.ToString());

                // Create the table if it doesn't exist.
                table.CreateIfNotExists();

                var dt = DataTable.New.Read(configurationCsvPathFileName);

                //Validate configuration file.
                if (validateConfigurationCsvFile == true &&
                    ValidateTrumpiaKeywordConfigurationConfigFile(dt, ref errorMessage) == false)
                {
                    throw new ArgumentOutOfRangeException(string.Format(errorMessage));
                }

                foreach (var dr in dt.Rows)
                {

                    var trumpiaKeywordConfigurationEntity = new TrumpiaKeywordConfigurationEntity();

                    var names = dr.ColumnNames.ToList();


                    foreach (var name in names)
                    {
                        var value = dr.GetValueOrEmpty(name);
                        if (name.Contains(PartitionKey))
                            trumpiaKeywordConfigurationEntity.PartitionKey = value;
                        else if (name.Contains(RowKey))
                            trumpiaKeywordConfigurationEntity.RowKey = value;
                        else if (name.Contains(TrumpiaKeyword))
                            trumpiaKeywordConfigurationEntity.TrumpiaKeyword = value;
                        else if (name.Contains(ClientId))
                            trumpiaKeywordConfigurationEntity.ClientId = value;
                    }

                    if (!names.Exists(n => n == PartitionKey))
                    {
                        trumpiaKeywordConfigurationEntity.PartitionKey =
                            TrumpiaKeywordConfigurationEntity.SetPartitionKey(
                                trumpiaKeywordConfigurationEntity.TrumpiaKeyword);
                    }
                    if (!names.Exists(n => n == RowKey))
                    {
                        trumpiaKeywordConfigurationEntity.RowKey =
                            TrumpiaKeywordConfigurationEntity.SetRowKey(trumpiaKeywordConfigurationEntity.ClientId);
                    }
                    if (!string.IsNullOrEmpty(trumpiaKeywordConfigurationEntity.TrumpiaKeyword) &&
                        !string.IsNullOrEmpty(trumpiaKeywordConfigurationEntity.ClientId))
                    {
                        // insert or update
                        TableOperation insertOrReplaceOperation =
                            TableOperation.InsertOrReplace(trumpiaKeywordConfigurationEntity);

                        table.Execute(insertOrReplaceOperation);


                        //Check for cancellation.
                        cancellationToken.ThrowIfCancellationRequested();

                        //Report: Status.
                        configurationUpdateStatusEventArgs = new ConfigurationUpdateStatusEventArgs();
                        var statusMessage =
                            $"Trumpia Keyword {trumpiaKeywordConfigurationEntity.PartitionKey} updated.";

                        configurationUpdateStatus = ConfigurationUpdateStatusFactory.CreateConfigurationUpdateStatus();
                        configurationUpdateStatus.StatusMessage = statusMessage;
                        configurationUpdateStatusEventArgs.ConfigurationUpdateStatus = configurationUpdateStatus;

                        if (ConfigurationUpdateStatus != null)
                        {
                            ConfigurationUpdateStatus(this, configurationUpdateStatusEventArgs);
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                Status status;
                configurationUpdateResult = ConfigurationUpdateResultFactory.CreateConfigurationUpdateResult();
                configurationUpdateResultEventArgs = new ConfigurationUpdateResultEventArgs();

                status = new Status();
                status.Exception = ex;
                status.StatusServerity = StatusTypes.StatusSeverity.Error;
                configurationUpdateResult.StatusList.Add(status);
                configurationUpdateResultEventArgs.ConfigurationUpdateResult = configurationUpdateResult;
                if (ConfigurationUpdated != null)
                {
                    ConfigurationUpdated(this, configurationUpdateResultEventArgs);
                }
            }
        }


        /// <summary>
        /// Handle unexpected Azure table.
        /// </summary>
        /// <param name="azureTable"></param>
        protected void HandleUnexpectedAzureTable(AzureTable azureTable)
        {
            IConfigurationUpdateResult configurationUpdateResult;
            ConfigurationUpdateStatusEventArgs configurationUpdateStatusEventArgs;
            IConfigurationUpdateStatus configurationUpdateStatus;
            ConfigurationUpdateResultEventArgs configurationUpdateResultEventArgs;
            string statusMessage;

            try
            {

                //Report: Status.
                configurationUpdateStatusEventArgs = new ConfigurationUpdateStatusEventArgs();
                statusMessage = string.Format("Unexpected Azure Table. Update bypassed (AzureTable: {0})",
                                               azureTable.ToString());

                configurationUpdateStatus = ConfigurationUpdateStatusFactory.CreateConfigurationUpdateStatus();
                configurationUpdateStatus.StatusMessage = statusMessage;
                configurationUpdateStatusEventArgs.ConfigurationUpdateStatus = configurationUpdateStatus;

                if (ConfigurationUpdateStatus != null)
                {
                    ConfigurationUpdateStatus(this, configurationUpdateStatusEventArgs);
                }

            }
            catch (Exception ex)
            {
                Status status;
                configurationUpdateResult = ConfigurationUpdateResultFactory.CreateConfigurationUpdateResult();
                configurationUpdateResultEventArgs = new ConfigurationUpdateResultEventArgs();

                status = new Status();
                status.Exception = ex;
                status.StatusServerity = StatusTypes.StatusSeverity.Error;
                configurationUpdateResult.StatusList.Add(status);
                configurationUpdateResultEventArgs.ConfigurationUpdateResult = configurationUpdateResult;
                if (ConfigurationUpdated != null)
                {
                    ConfigurationUpdated(this, configurationUpdateResultEventArgs);
                }
            }
        }

        /// <summary>
        /// Validate configuration file: Bill cycle schedule config file.
        /// </summary>
        /// <param name="mutableDataTable"></param>
        /// <param name="errorMessage"></param>
        /// <returns>Returns (false = validation failed, true = validation passed.)</returns>
        protected bool ValidateBillCycleScheduleConfigFile(MutableDataTable mutableDataTable, ref string errorMessage)
        {
            const string ErrorMessage_MissingColumns = "BillCycleSchedule configuration file is missing columns. ({0})";

            bool result = true;
            List<string> expectedColumnNameList = null;
            string columnNotFoundListAsText = String.Empty;
            BillCycleScheduleEntity billCycleScheduleEntity = null;

            try
            {
                errorMessage = string.Empty;

                expectedColumnNameList = new List<string>();
                expectedColumnNameList.Add(PartitionKey);
                expectedColumnNameList.Add(RowKey);
                expectedColumnNameList.Add(BeginDate);
                expectedColumnNameList.Add(ClientId);
                expectedColumnNameList.Add(EndDate);
                expectedColumnNameList.Add(Description);
                expectedColumnNameList.Add(BillCycleScheduleName);

                billCycleScheduleEntity = new BillCycleScheduleEntity();

                var columnNames = mutableDataTable.ColumnNames.ToList();

                var columnNotFoundList = expectedColumnNameList.Where(n => !columnNames.Any(n2 => n2.Contains(n)));

                //At least one column was not found.
                if (columnNotFoundList.Count() > 0)
                {
                    columnNotFoundListAsText = String.Join(",", columnNotFoundList.ToArray());
                    errorMessage = string.Format(ErrorMessage_MissingColumns, columnNotFoundListAsText);
                    return false;
                }

            }
            catch (Exception)
            {
                throw;
            }

            return result;
        }

        /// <summary>
        /// Validate configuration file: Client domain config file.
        /// </summary>
        /// <param name="mutableDataTable"></param>
        /// <param name="errorMessage"></param>
        /// <returns>Returns (false = validation failed, true = validation passed.)</returns>
        protected bool ValidateClientDomainConfigFile(MutableDataTable mutableDataTable, ref string errorMessage)
        {
            const string ErrorMessage_MissingColumns = "ClientDomain configuration file is missing columns. ({0})";

            bool result = true;
            List<string> expectedColumnNameList = null;
            string columnNotFoundListAsText = String.Empty;
            ClientDomainEntity clientDomainEntity = null;

            try
            {
                errorMessage = string.Empty;

                expectedColumnNameList = new List<string>();
                expectedColumnNameList.Add(PartitionKey);
                expectedColumnNameList.Add(RowKey);

                clientDomainEntity = new ClientDomainEntity();

                var columnNames = mutableDataTable.ColumnNames.ToList();

                var columnNotFoundList = expectedColumnNameList.Where(n => !columnNames.Any(n2 => n2.Contains(n)));

                //At least one column was not found.
                if (columnNotFoundList.Count() > 0)
                {
                    columnNotFoundListAsText = String.Join(",", columnNotFoundList.ToArray());
                    errorMessage = string.Format(ErrorMessage_MissingColumns, columnNotFoundListAsText);
                    return false;
                }

            }
            catch (Exception)
            {
                throw;
            }

            return result;
        }

        /// <summary>
        /// Validate configuration file: Client widget configuration config file.
        /// </summary>
        /// <param name="mutableDataTable"></param>
        /// <param name="errorMessage"></param>
        /// <returns>Returns (false = validation failed, true = validation passed.)</returns>
        protected bool ValidateClientWidgetConfigurationConfigFile(MutableDataTable mutableDataTable, ref string errorMessage)
        {
            const string ErrorMessage_MissingColumns = "ClientWidgetConfiguration configuration file is missing columns. ({0})";

            bool result = true;
            List<string> expectedColumnNameList = null;
            string columnNotFoundListAsText = String.Empty;
            ClientWidgetConfigurationEntity clientWidgetConfigurationEntity = null;

            try
            {
                errorMessage = string.Empty;

                expectedColumnNameList = new List<string>();
                expectedColumnNameList.Add(PartitionKey);
                expectedColumnNameList.Add(RowKey);
                expectedColumnNameList.Add(WebTokenUrl);
                expectedColumnNameList.Add(ClientId);
                expectedColumnNameList.Add(WidgetPageUrl);
                expectedColumnNameList.Add(AccessKeyId);
                expectedColumnNameList.Add(Password);

                clientWidgetConfigurationEntity = new ClientWidgetConfigurationEntity();

                var columnNames = mutableDataTable.ColumnNames.ToList();
                var columnNotFoundList = expectedColumnNameList.Where(n => !columnNames.Any(n2 => n2.Contains(n)));

                //At least one column was not found.
                if (columnNotFoundList.Count() > 0)
                {
                    columnNotFoundListAsText = String.Join(",", columnNotFoundList.ToArray());
                    errorMessage = string.Format(ErrorMessage_MissingColumns, columnNotFoundListAsText);
                    return false;
                }

            }
            catch (Exception)
            {
                throw;
            }

            return result;
        }

        /// <summary>
        /// Validate configuration file: Sms template config file.
        /// </summary>
        /// <param name="mutableDataTable"></param>
        /// <param name="errorMessage"></param>
        /// <returns>Returns (false = validation failed, true = validation passed.)</returns>
        protected bool ValidateSmsTemplateConfigFile(MutableDataTable mutableDataTable, ref string errorMessage)
        {
            const string ErrorMessage_MissingColumns = "SmsTemplate configuration file is missing columns. ({0})";

            bool result = true;
            List<string> expectedColumnNameList = null;
            string columnNotFoundListAsText = String.Empty;
            SmsTemplateEntity smsTemplateEntity = null;

            try
            {
                errorMessage = string.Empty;

                expectedColumnNameList = new List<string>();
                expectedColumnNameList.Add(PartitionKey);
                expectedColumnNameList.Add(RowKey);
                expectedColumnNameList.Add(Body);
                expectedColumnNameList.Add(ClientId);
                expectedColumnNameList.Add(InsightTypeName);
                expectedColumnNameList.Add(TemplateId);


                smsTemplateEntity = new SmsTemplateEntity();

                var columnNames = mutableDataTable.ColumnNames.ToList();
                var columnNotFoundList = expectedColumnNameList.Where(n => !columnNames.Any(n2 => n2.Contains(n)));

                //At least one column was not found.
                if (columnNotFoundList.Count() > 0)
                {
                    columnNotFoundListAsText = String.Join(",", columnNotFoundList.ToArray());
                    errorMessage = string.Format(ErrorMessage_MissingColumns, columnNotFoundListAsText);
                    return false;
                }

            }
            catch (Exception)
            {
                throw;
            }

            return result;
        }

        /// <summary>
        /// Validate configuration file: Trumpia keyword configuration config file.
        /// </summary>
        /// <param name="mutableDataTable"></param>
        /// <param name="errorMessage"></param>
        /// <returns>Returns (false = validation failed, true = validation passed.)</returns>
        protected bool ValidateTrumpiaKeywordConfigurationConfigFile(MutableDataTable mutableDataTable, ref string errorMessage)
        {

            const string ErrorMessage_MissingColumns = "TrumpiaKeywordConfiguration configuration file is missing columns. ({0})";

            bool result = true;
            List<string> expectedColumnNameList = null;
            string columnNotFoundListAsText = String.Empty;
            TrumpiaKeywordConfigurationEntity smsTemplateEntity = null;

            try
            {
                errorMessage = string.Empty;

                expectedColumnNameList = new List<string>();
                expectedColumnNameList.Add(PartitionKey);
                expectedColumnNameList.Add(RowKey);
                expectedColumnNameList.Add(TrumpiaKeyword);
                expectedColumnNameList.Add(ClientId);

                smsTemplateEntity = new TrumpiaKeywordConfigurationEntity();

                var columnNames = mutableDataTable.ColumnNames.ToList();

                var columnNotFoundList = expectedColumnNameList.Where(n => !columnNames.Any(n2 => n2.Contains(n)));

                //At least one column was not found.
                if (columnNotFoundList.Count() > 0)
                {
                    columnNotFoundListAsText = String.Join(",", columnNotFoundList.ToArray());
                    errorMessage = string.Format(ErrorMessage_MissingColumns, columnNotFoundListAsText);
                    return false;
                }

            }
            catch (Exception)
            {
                throw;
            }

            return result;
        }
    }

    #endregion

    #region Private methods

    #endregion

}
