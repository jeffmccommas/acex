﻿using CE.AO.ConfigurationManagement.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CE.AO.ConfigurationManagement.Events
{
    /// <summary>
    /// Generate client test data status event arguments.
    /// </summary>
    public class ConfigurationUpdateStatusEventArgs : EventArgs
    {

        #region Private Constants
        #endregion

        #region Private Data Members

        private IConfigurationUpdateStatus _generateClientTestDataStatus;

        #endregion

        #region Public Properties

        public IConfigurationUpdateStatus ConfigurationUpdateStatus
        {
            get { return _generateClientTestDataStatus; }
            set { _generateClientTestDataStatus = value; }
        }

        #endregion

        #region Public Constructors

        /// <summary>
        /// Default constructor.
        /// </summary>
        public ConfigurationUpdateStatusEventArgs()
        {
        }

        /// <summary>
        /// Overridden constructor.
        /// </summary>
        /// <param name="generateClientTestDataStatus"></param>
        public ConfigurationUpdateStatusEventArgs(IConfigurationUpdateStatus generateClientTestDataStatus)
        {
            _generateClientTestDataStatus = generateClientTestDataStatus;
        }

        #endregion

    }
}
