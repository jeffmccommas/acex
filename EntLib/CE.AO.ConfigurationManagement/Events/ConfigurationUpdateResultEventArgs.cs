﻿using CE.AO.ConfigurationManagement.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CE.AO.ConfigurationManagement.Events
{
    /// <summary>
    /// Configuration update result event arguments.
    /// </summary>
    public class ConfigurationUpdateResultEventArgs
    {

        #region Private Constants
        #endregion

        #region Private Data Members

        private IConfigurationUpdateResult _generateClientTestDataResult;

        #endregion

        #region Public Properties

        public IConfigurationUpdateResult ConfigurationUpdateResult
        {
            get { return _generateClientTestDataResult; }
            set { _generateClientTestDataResult = value; }
        }

        #endregion

        #region Public Constructors

        /// <summary>
        /// Default constructor.
        /// </summary>
        public ConfigurationUpdateResultEventArgs()
        {
        }

        /// <summary>
        /// Overridden constructor.
        /// </summary>
        /// <param name="generateClientTestDataResult"></param>
        public ConfigurationUpdateResultEventArgs(IConfigurationUpdateResult generateClientTestDataResult)
        {
            _generateClientTestDataResult = generateClientTestDataResult;
        }

        #endregion
    }
}
