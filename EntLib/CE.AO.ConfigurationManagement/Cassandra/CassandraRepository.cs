﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Cassandra;
using Cassandra.Data.Linq;
using Cassandra.Mapping;
using CE.AO.ConfigurationManagement.CassandraEntities;

namespace CE.AO.ConfigurationManagement.Cassandra
{
    public class CassandraRepository 
    {
        private static ISession _session;

        public static bool CassandraMappingsInitialized { get; set; }

        public CassandraRepository(string cassandraStorageAccountConnectionString, string userName, string password, string keyspace)
        {
            if (!CassandraMappingsInitialized)
            {
                MappingConfiguration.Global.Define<CassandraMapperConfig>();
                CassandraMappingsInitialized = true;
            }
           _session = CassandraSession.GetSession(cassandraStorageAccountConnectionString, userName, password, keyspace);               
        }


        private static CqlQuery<T> CreateCqlQuery<T>(string tableName, Expression<Func<T, bool>> filter = null) where T : class
        {          
            CqlQuery<T> query = new Table<T>(_session, MappingConfiguration.Global, tableName);
            if (filter != null)
            {
                query = query.Where(filter);
            }
            return query;
        }

        public bool InsertOrUpdateBatch<T>(List<T> tableModel, string tableName) where T : class
        {
            
            var batch = _session.CreateBatch();

            var query = new Table<T>(_session, MappingConfiguration.Global, tableName);
            foreach (var entity in tableModel)
            {
                batch.Append(query.Insert(entity));
            }
            batch.Execute();
            return true;
        }

        public async Task InsertOrUpdateBatchAsync<T>(List<T> tableModel, string tableName) where T : class
        {
            
            var batch = _session.CreateBatch();

            var query = new Table<T>(_session, MappingConfiguration.Global, tableName);
            foreach (var entity in tableModel)
            {
                batch.Append(query.Insert(entity));
            }
            await batch.ExecuteAsync();
        }

        public bool InsertOrUpdate<T>(T tableModel, string tableName) where T : class
        {
          
            var query = new Table<T>(_session, MappingConfiguration.Global, tableName);
            query.Insert(tableModel).Execute();
            return true;
        }

        public async Task InsertOrUpdateAsync<T>(T tableModel, string tableName) where T : class
        {
          
            var query = new Table<T>(_session, MappingConfiguration.Global, tableName);
            await query.Insert(tableModel).ExecuteAsync();
        }

        public bool InsertOrUpdate<T>(T tableModel, string tableName, int ttlInSeconds) where T : class
        {
           
            var query = new Table<T>(_session, MappingConfiguration.Global, tableName);
            query.Insert(tableModel).SetTTL(ttlInSeconds).Execute();
            return true;
        }

        public async Task InsertOrUpdateAsync<T>(T tableModel, string tableName, int ttlInSeconds) where T : class
        {
           
            var query = new Table<T>(_session, MappingConfiguration.Global, tableName);
            await query.Insert(tableModel).SetTTL(ttlInSeconds).ExecuteAsync();
        }

        public IList<T> Get<T>(string tableName, Expression<Func<T, bool>> filter = null) where T : class
        {
          
            CqlQuery<T> query = new Table<T>(_session, MappingConfiguration.Global, tableName);
            if (filter != null)
            {
                query = query.Where(filter);
            }

            return query.Execute().ToList();
        }

        public IList<TResult> Get<T, TResult>(string tableName, Expression<Func<T, TResult>> selection, Expression<Func<T, bool>> filter = null) where T : class
        {
           
            CqlQuery<T> query = new Table<T>(_session, MappingConfiguration.Global, tableName);
            if (filter != null)
            {
                query = query.Where(filter);
            }

            CqlQuery<TResult> selectorQuery = query.Select(selection);
            return selectorQuery.Execute().ToList();
        }

        public IList<T> Get<T>(string tableName, int pageSize, ref byte[] token, Expression<Func<T, bool>> filter = null) where T : class
        {
            CqlQuery<T> query = CreateCqlQuery(tableName, filter);
            query.SetPageSize(pageSize);
            if (token != null)
            {
                query.SetPagingState(token);
            }
            IPage<T> p = query.ExecutePaged();
            token = p.PagingState;
            return p.ToList();
        }

        public async Task<IList<T>> GetAsync<T>(string tableName, Expression<Func<T, bool>> filter = null) where T : class
        {
           
            CqlQuery<T> query = new Table<T>(_session, MappingConfiguration.Global, tableName);
            if (filter != null)
            {
                query = query.Where(filter);
            }

            return (await query.ExecuteAsync()).ToList();
        }

        public async Task<IList<TResult>> GetAsync<T, TResult>(string tableName, Expression<Func<T, TResult>> selection, Expression<Func<T, bool>> filter = null) where T : class
        {
          
            CqlQuery<T> query = new Table<T>(_session, MappingConfiguration.Global, tableName);
            if (filter != null)
            {
                query = query.Where(filter);
            }

            CqlQuery<TResult> selectorQuery = query.Select(selection);
            return (await selectorQuery.ExecuteAsync()).ToList();
        }

        public T GetSingle<T>(string tableName, Expression<Func<T, bool>> filter = null) where T : class
        {
           
            CqlQuery<T> query = new Table<T>(_session, MappingConfiguration.Global, tableName);
            if (filter != null)
            {
                query = query.Where(filter);
            }

            return query.FirstOrDefault().Execute();
        }

        public TResult GetSingle<T, TResult>(string tableName, Expression<Func<T, TResult>> selection, Expression<Func<T, bool>> filter = null) where T : class
        {
            
            CqlQuery<T> query = new Table<T>(_session, MappingConfiguration.Global, tableName);
            if (filter != null)
            {
                query = query.Where(filter);
            }

            CqlQuery<TResult> selectorQuery = query.Select(selection);
            return selectorQuery.FirstOrDefault().Execute();
        }

        public async Task<T> GetSingleAsync<T>(string tableName, Expression<Func<T, bool>> filter = null) where T : class
        {
           
            CqlQuery<T> query = new Table<T>(_session, MappingConfiguration.Global, tableName);
            if (filter != null)
            {
                query = query.Where(filter);
            }

            return await query.FirstOrDefault().ExecuteAsync();
        }

        public async Task<TResult> GetSingleAsync<T, TResult>(string tableName, Expression<Func<T, TResult>> selection, Expression<Func<T, bool>> filter = null) where T : class
        {
           
            CqlQuery<T> query = new Table<T>(_session, MappingConfiguration.Global, tableName);
            if (filter != null)
            {
                query = query.Where(filter);
            }

            CqlQuery<TResult> selectorQuery = query.Select(selection);
            return await selectorQuery.FirstOrDefault().ExecuteAsync();
        }

        public TResult ExecuteSelectQuery<T, TResult>(CqlQuery<T> query, Expression<Func<T, TResult>> selection) where T : class
        {
            CqlQuery<TResult> q = query.Select(selection);
            return q.FirstOrDefault().Execute();
        }

        public CqlQuery<T> CreateQuery<T>(string tableName, Expression<Func<T, bool>> filter = null) where T : class
        {
           
            CqlQuery<T> query = new Table<T>(_session, MappingConfiguration.Global, tableName);

            if (filter != null)
            {
                query = query.Where(filter);
            }

            return query;
        }

        public async Task DeleteAsync<T>(string tableName, Expression<Func<T, bool>> filter) where T : class
        {
            
            CqlQuery<T> query = new Table<T>(_session, MappingConfiguration.Global, tableName);
            query = query.Where(filter);
            await query.Delete().ExecuteAsync();
        }

        public void Delete<T>(string tableName, Expression<Func<T, bool>> filter) where T : class
        {
          
            CqlQuery<T> query = new Table<T>(_session, MappingConfiguration.Global, tableName);
            query = query.Where(filter);
            query.Delete().Execute();
        }
    }
}
