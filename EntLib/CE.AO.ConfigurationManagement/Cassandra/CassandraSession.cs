﻿using Cassandra;

namespace CE.AO.ConfigurationManagement.Cassandra
{
    public static class CassandraSession
    {
        private static Cluster _cluster;
        private static ISession _session;

        public static ISession GetSession(string azureStorageAccountConnectionString, string userName, string password, string keyspace)
        {
            if (!string.IsNullOrEmpty(azureStorageAccountConnectionString))
            {
                SetCluster(azureStorageAccountConnectionString,userName, password);
                if (_cluster != null)
                    _session = _cluster.Connect();
            }
            else if (_session == null)
            {
                _session = _cluster.Connect();
            }

            _session.ChangeKeyspace(keyspace);
            return _session;
        }

        private static void SetCluster(string azureStorageAccountConnectionString, string userName, string password)
        {
            string[] nodes = azureStorageAccountConnectionString.Split(',');

            var queryOptions = new QueryOptions()
                .SetConsistencyLevel(ConsistencyLevel.One);

            var clusterBuilder = Cluster.Builder()
                .WithSocketOptions(new SocketOptions().SetConnectTimeoutMillis(20000))
                .AddContactPoints(nodes)
                .WithQueryOptions(queryOptions);
          
            if (!string.IsNullOrWhiteSpace(userName) && !string.IsNullOrWhiteSpace(password))
            {
                clusterBuilder = clusterBuilder.WithCredentials(userName, password);
            }

            var cluster = clusterBuilder.Build();

            _cluster = cluster;
        }
    }
}
