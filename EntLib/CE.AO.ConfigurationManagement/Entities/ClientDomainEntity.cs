﻿using Microsoft.WindowsAzure.Storage.Table;

namespace CE.AO.ConfigurationManagement.Entities
{
    public class ClientDomainEntity : TableEntity
    {
        public ClientDomainEntity(string partitionKey, string rowKey)
        {
            PartitionKey = partitionKey;
            RowKey = rowKey;
        }

        public ClientDomainEntity()
        {
            PartitionKey = "";
            RowKey = "";
        }

        public static string SetPartitionKey(int clientId, string billCycleId)
        {
            return $"{clientId}_{billCycleId}";
        }
    }
}
