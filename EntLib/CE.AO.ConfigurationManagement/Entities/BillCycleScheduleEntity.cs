﻿using Microsoft.WindowsAzure.Storage.Table;

namespace CE.AO.ConfigurationManagement.Entities
{
    public class BillCycleScheduleEntity : TableEntity
    {
        public BillCycleScheduleEntity(string partitionKey, string rowKey)
        {
            PartitionKey = partitionKey;
            RowKey = rowKey;
        }

        public BillCycleScheduleEntity()
        {
            PartitionKey = "";
            RowKey = "";
        }

        public string ClientId { get; set; }
        public string BillCycleScheduleName { get; set; }
        //changed as table in storage is string 
        public string BeginDate { get; set; }
        //changed as table in storage is string 
        public string EndDate { get; set; }
        public string Description { get; set; }

        public static string SetPartitionKey(int clientId, string billCycleId)
        {
            return $"{clientId}_{billCycleId}";
        }
    }
}
