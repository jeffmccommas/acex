﻿using Microsoft.WindowsAzure.Storage.Table;

namespace CE.AO.ConfigurationManagement.Entities
{
    public class ClientWidgetConfigurationEntity : TableEntity
    {
        public ClientWidgetConfigurationEntity(string partitionKey, string rowKey)
        {
            PartitionKey = partitionKey;
            RowKey = rowKey;
        }
        public ClientWidgetConfigurationEntity()
        {
            PartitionKey = "";
            RowKey = "";
        }

        public int ClientId { get; set; }
        public string WebTokenUrl { get; set; }
        public string WidgetPageUrl { get; set; }
        public string AccessKeyId { get; set; }
        public string Password { get; set; }

        public static string SetPartitionKey(int clientId)
        {
            return clientId.ToString();
        }

        //public static string SetRowKey()
        //{
        //    return "1";
        //}
    }
}