﻿using Microsoft.WindowsAzure.Storage.Table;

namespace CE.AO.ConfigurationManagement.Entities
{
    public class SmsTemplateEntity : TableEntity
    {
        public SmsTemplateEntity(string partitionKey, string rowKey)
        {
            PartitionKey = partitionKey;
            RowKey = rowKey;
        }
        public SmsTemplateEntity()
        {
            PartitionKey = "";
            RowKey = "";
        }

        public int ClientId { get; set; }
        public string InsightTypeName { get; set; }
        public string Body { get; set; }
        public string TemplateId { get; set; }

        public static string SetSmsTemplatePartitionKey(int clientId, string insightTypeName)
        {
            return $"{clientId}_{insightTypeName}";
        }

        public static string SetSmsTemplateRowKey(string templateId)
        {
            return templateId;
        }
    }
}
