﻿using Aclara.Tools.Common.StatusManagement;
using CE.AO.ConfigurationManagement.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CE.AO.ConfigurationManagement.Results
{
    /// <summary>
    /// Generate client test data result.
    /// </summary>
    public class ConfigurationUpdateResult : IConfigurationUpdateResult
    {

        #region Private Constants
        #endregion

        #region Private Data  Members

        private string _resultMessage;

        StatusList _statusList;

        #endregion

        #region Public Properties

        /// <summary>
        /// Property: Result message.
        /// </summary>
        public string ResultMessage
        {
            get { return _resultMessage; }
            set { _resultMessage = value; }
        }

        /// <summary>
        /// Property: Status list.
        /// </summary>
        public StatusList StatusList
        {
            get
            {
                return _statusList;
            }
        }

        #endregion

        #region Public Constructors

        /// <summary>
        /// Default constructor.
        /// </summary>
        public ConfigurationUpdateResult()
        {
            _statusList = new StatusList();
        }

        #endregion

    }
}
