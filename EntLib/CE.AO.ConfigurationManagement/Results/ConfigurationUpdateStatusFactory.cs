﻿using CE.AO.ConfigurationManagement.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CE.AO.ConfigurationManagement.Results
{
    /// <summary>
    /// Create generate client test data factory.
    /// </summary>
    public static class ConfigurationUpdateStatusFactory
    {

        /// <summary>
        /// Create generate client test data status.
        /// </summary>
        /// <returns></returns>
        static public IConfigurationUpdateStatus CreateConfigurationUpdateStatus()
        {
            IConfigurationUpdateStatus result = null;
            ConfigurationUpdateStatus generateClientTestDataStatus = null;

            try
            {
                generateClientTestDataStatus = new ConfigurationUpdateStatus();

                result = generateClientTestDataStatus;

                return result;
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
