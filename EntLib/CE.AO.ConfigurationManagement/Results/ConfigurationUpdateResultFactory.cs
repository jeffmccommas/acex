﻿using Aclara.Tools.Common.StatusManagement;
using CE.AO.ConfigurationManagement.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CE.AO.ConfigurationManagement.Results
{

    /// <summary>
    /// Generate client test data result factory.
    /// </summary>

    /// <summary>
    /// Create generate client test data factory.
    /// </summary>
    public static class ConfigurationUpdateResultFactory
    {

        /// <summary>
        /// Create generate client test data status.
        /// </summary>
        /// <returns></returns>
        static public IConfigurationUpdateResult CreateConfigurationUpdateResult()
        {
            IConfigurationUpdateResult result = null;
            ConfigurationUpdateResult generateClientTestDataResult = null;

            try
            {
                generateClientTestDataResult = new ConfigurationUpdateResult();

                result = generateClientTestDataResult;

                return result;
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
