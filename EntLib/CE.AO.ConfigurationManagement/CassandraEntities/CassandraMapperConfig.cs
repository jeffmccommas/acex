﻿using Cassandra.Mapping;
using static CE.AO.ConfigurationManagement.Types.Enumerations;

namespace CE.AO.ConfigurationManagement.CassandraEntities
{
    /// <summary>
    /// Cassandra mapping configuration are configured in this class
    /// </summary>
    public class CassandraMapperConfig : Mappings
    {
        /// <summary>
        /// Cassandra mapping configuration are configured in the constructor
        /// </summary>
        public CassandraMapperConfig()
        {
            For<BillCycleScheduleEntity>()
                .TableName(CassandraConfigurationManager.GetEnumDescription(CassandraTable.BillCycleSchedule))
                .Column(u => u.ClientId, cm => cm.WithName("client_id"))
                .Column(u => u.BillCycleScheduleId, cm => cm.WithName("bill_cycle_schedule_id"))
                .Column(u => u.BeginDate, cm => cm.WithName("begin_date"))
                .Column(u => u.EndDate, cm => cm.WithName("end_date"))
                .Column(u => u.BillCycleScheduleName, cm => cm.WithName("bill_cycle_schedulename"))
                .Column(u => u.Description, cm => cm.WithName("description"));
            For<SmsTemplateEntity>()
               .TableName(CassandraConfigurationManager.GetEnumDescription(CassandraTable.SmsTemplate))
                .Column(u => u.ClientId, cm => cm.WithName("client_id"))
                .Column(u => u.InsightTypeName, cm => cm.WithName("insight_type_name"))
                .Column(u => u.TemplateId, cm => cm.WithName("template_id"))
                .Column(u => u.Body, cm => cm.WithName("body"));          
            For<TrumpiaKeywordConfigurationEntity>()
                .TableName(CassandraConfigurationManager.GetEnumDescription(CassandraTable.TrumpiaKeywordConfiguration))
                .Column(u => u.ClientId, cm => cm.WithName("client_id"))
                .Column(u => u.TrumpiaKeyword, cm => cm.WithName("trumpia_keyword"));                                  
        }
    }
}
