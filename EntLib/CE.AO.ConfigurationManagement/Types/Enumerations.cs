﻿using System.ComponentModel;

namespace CE.AO.ConfigurationManagement.Types
{
    public class Enumerations
    {
        public enum AzureTable
        {
            Unspecified = 0,
            BillCycleSchedule = 1,
            ClientDomain = 2,
            ClientWidgetConfiguration = 3,
            SmsTemplate = 4,
            TrumpiaKeywordConfiguration = 5
        }

        public enum CassandraTable
        {
            Unspecified = 0,
            [Description("bill_cycle_schedule")]
            BillCycleSchedule = 1,
            [Description("sms_template")]
            SmsTemplate = 2,
            [Description("trumpia_keyword_configuration")]
            TrumpiaKeywordConfiguration = 3
        }
    }
}
