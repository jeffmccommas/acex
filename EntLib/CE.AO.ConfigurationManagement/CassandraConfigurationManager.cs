﻿using Aclara.Tools.Common.StatusManagement;
using CE.AO.ConfigurationManagement.Events;
using CE.AO.ConfigurationManagement.Interfaces;
using CE.AO.ConfigurationManagement.Results;
using DataAccess;
using System;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using System.Threading;
using CE.AO.ConfigurationManagement.Cassandra;
using static CE.AO.ConfigurationManagement.Types.Enumerations;
using CE.AO.ConfigurationManagement.CassandraEntities;

//using Microsoft.WindowsAzure.StorageClient;

namespace CE.AO.ConfigurationManagement
{

    /// <summary>
    /// Configuration manager.
    /// </summary>
    public class CassandraConfigurationManager
    {

        #region Private Constants

        private const string Body = "Body";
        private const string ClientId = "ClientId";
        private const string InsightTypeName = "InsightTypeName";
        private const string TemplateId = "TemplateId";
        private const string BeginDate = "BeginDate";
        private const string EndDate = "EndDate";
        private const string Description = "Description";
        private const string TrumpiaKeyword = "TrumpiaKeyword";
        private const string BillCycleScheduleName = "BillCycleScheduleName";
        private const string BillCycleScheduleId = "BillCycleScheduleId";

        #endregion

        #region Private Data Members


        #endregion

        #region Public Delegates

        public event EventHandler<ConfigurationUpdateStatusEventArgs> ConfigurationUpdateStatus;
        public event EventHandler<ConfigurationUpdateResultEventArgs> ConfigurationUpdated;

        #endregion

        #region Public Constructors

        /// <summary>
        /// Preferred constructor.
        /// </summary>
        public CassandraConfigurationManager(string azureStorageAccountConnectionString, string userName, string password, string keyspace)
        {
            CassandraSession.GetSession(azureStorageAccountConnectionString, userName, password, keyspace);
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Update configuration.
        /// </summary>
        /// <param name="cassandraStorageTableName"></param>
        /// <param name="cassandraStorageAccountAccessKey"></param>
        /// <param name="keyspace"></param>
        /// <param name="cassandraTable"></param>
        /// <param name="configurationCsvPathFileName"></param>
        /// <param name="cancellationToken"></param>
        public void UpdateCassandraTableConfiguration(string cassandraStorageTableName,
                                                  string cassandraStorageAccountAccessKey, string keyspace,
                                                  CassandraTable cassandraTable,
                                                  string configurationCsvPathFileName,
                                                  CancellationToken cancellationToken)
        {
            IConfigurationUpdateResult configurationUpdateResult;
            ConfigurationUpdateResultEventArgs configurationUpdateResultEventArgs;
            string resultMessage;

            try
            {

                switch (cassandraTable)
                {
                    case CassandraTable.Unspecified:
                        HandleUnexpectedCassandraTable(cassandraTable);
                        break;
                    case CassandraTable.BillCycleSchedule:
                        UpdateCassandraTableBillCycleSchedule(configurationCsvPathFileName, cancellationToken, cassandraStorageAccountAccessKey, keyspace);
                        break;
                    case CassandraTable.SmsTemplate:
                        UpdateCassandraTableSmsTemplate(configurationCsvPathFileName,
                                                    cancellationToken, cassandraStorageAccountAccessKey, keyspace);
                        break;
                    case CassandraTable.TrumpiaKeywordConfiguration:
                        UpdateCassandraTableTrumpiaKeywordConfiguration(configurationCsvPathFileName, cancellationToken, cassandraStorageAccountAccessKey, keyspace);
                        break;
                }

                //Report: Client test data generation completed.
                configurationUpdateResult = ConfigurationUpdateResultFactory.CreateConfigurationUpdateResult();
                resultMessage = "Cassandra Table Configuration Update Result: All requested configuration items updated.";
                configurationUpdateResult.ResultMessage = resultMessage;
                configurationUpdateResultEventArgs = new ConfigurationUpdateResultEventArgs();
                configurationUpdateResultEventArgs.ConfigurationUpdateResult = configurationUpdateResult;
                if (ConfigurationUpdated != null)
                {
                    ConfigurationUpdated(this, configurationUpdateResultEventArgs);
                }

            }
            catch (OperationCanceledException)
            {
                throw;
            }
            catch (Exception ex)
            {
                Status status;
                configurationUpdateResult = ConfigurationUpdateResultFactory.CreateConfigurationUpdateResult();
                configurationUpdateResultEventArgs = new ConfigurationUpdateResultEventArgs();

                status = new Status();
                status.Exception = ex;
                status.StatusServerity = StatusTypes.StatusSeverity.Error;
                configurationUpdateResult.StatusList.Add(status);
                configurationUpdateResultEventArgs.ConfigurationUpdateResult = configurationUpdateResult;
                if (ConfigurationUpdated != null)
                {
                    ConfigurationUpdated(this, configurationUpdateResultEventArgs);
                }
            }

        }


        #endregion

        #region Protected methods

        /// <summary>
        /// Update Cassandra table: Bill Cycle Schedule.
        /// </summary>
        /// <param name="configurationCsvPathFileName"></param>
        /// <param name="cancellationToken"></param>
        /// <param name="cassandraStorageAccountAccessKey"></param>
        /// <param name="keyspace"></param>
        protected void UpdateCassandraTableBillCycleSchedule(string configurationCsvPathFileName, CancellationToken cancellationToken, string cassandraStorageAccountAccessKey, string keyspace)
        {
            IConfigurationUpdateResult configurationUpdateResult;
            ConfigurationUpdateStatusEventArgs configurationUpdateStatusEventArgs;
            IConfigurationUpdateStatus configurationUpdateStatus;
            ConfigurationUpdateResultEventArgs configurationUpdateResultEventArgs;

            try
            {
                //Check for cancellation.
                cancellationToken.ThrowIfCancellationRequested();
                string userName;
                string password;
                var clusterNames = ClusterNames(cassandraStorageAccountAccessKey, out userName, out password);


                CassandraRepository respository = new CassandraRepository(clusterNames, userName, password, keyspace);

                var dt = DataTable.New.Read(configurationCsvPathFileName);

                foreach (var dr in dt.Rows)
                {

                    var billCycleScheduleEntity = new BillCycleScheduleEntity();

                    var names = dr.ColumnNames.ToList();


                    foreach (var name in names)
                    {
                        var value = dr.GetValueOrEmpty(name);
                        if (name.Contains(ClientId))
                            billCycleScheduleEntity.ClientId = value;
                        else if (name.Contains(BillCycleScheduleId))
                            billCycleScheduleEntity.BillCycleScheduleId = value;
                        else if (name.Contains(BeginDate))
                            billCycleScheduleEntity.BeginDate = Convert.ToDateTime(value).ToShortDateString();
                        else if (name.Contains(EndDate))
                            billCycleScheduleEntity.EndDate = Convert.ToDateTime(value).ToShortDateString();
                        else if (name.Contains(BillCycleScheduleName))
                            billCycleScheduleEntity.BillCycleScheduleName = value;
                        else if (name.Contains(Description))
                            billCycleScheduleEntity.Description = value;

                    }

                    if (string.IsNullOrEmpty(billCycleScheduleEntity.BillCycleScheduleId))
                    {
                        billCycleScheduleEntity.BillCycleScheduleId = billCycleScheduleEntity.BillCycleScheduleName;
                    }

                    if (!string.IsNullOrEmpty(billCycleScheduleEntity.ClientId) &&
                        !string.IsNullOrEmpty(billCycleScheduleEntity.BillCycleScheduleId) && !string.IsNullOrEmpty(billCycleScheduleEntity.BeginDate) && !string.IsNullOrEmpty(billCycleScheduleEntity.EndDate))
                    {
                       
                        respository.InsertOrUpdate(billCycleScheduleEntity,
                            GetEnumDescription(CassandraTable.BillCycleSchedule));

                        //Report: Status.
                        configurationUpdateStatusEventArgs = new ConfigurationUpdateStatusEventArgs();
                        var statusMessage =
                            $"Bill Cycle Schedule for Cycle {billCycleScheduleEntity.BillCycleScheduleId} updated.";

                        configurationUpdateStatus = ConfigurationUpdateStatusFactory.CreateConfigurationUpdateStatus();
                        configurationUpdateStatus.StatusMessage = statusMessage;
                        configurationUpdateStatusEventArgs.ConfigurationUpdateStatus = configurationUpdateStatus;

                        ConfigurationUpdateStatus?.Invoke(this, configurationUpdateStatusEventArgs);
                    }
                }
            }
            catch (Exception ex)
            {
                configurationUpdateResult = ConfigurationUpdateResultFactory.CreateConfigurationUpdateResult();
                configurationUpdateResultEventArgs = new ConfigurationUpdateResultEventArgs();

                var status = new Status
                {
                    Exception = ex,
                    StatusServerity = StatusTypes.StatusSeverity.Error
                };
                configurationUpdateResult.StatusList.Add(status);
                configurationUpdateResultEventArgs.ConfigurationUpdateResult = configurationUpdateResult;
                ConfigurationUpdated?.Invoke(this, configurationUpdateResultEventArgs);
            }
        }


        /// <summary>
        /// Update Cassandra table: SMS Template.
        /// </summary>
        /// <param name="configurationCsvPathFileName"></param>
        /// <param name="cancellationToken"></param>
        /// <param name="cassandraStorageAccountAccessKey"></param>
        /// <param name="keyspace"></param>
        protected void UpdateCassandraTableSmsTemplate(string configurationCsvPathFileName, CancellationToken cancellationToken, string cassandraStorageAccountAccessKey, string keyspace)
        {
            IConfigurationUpdateResult configurationUpdateResult;
            ConfigurationUpdateStatusEventArgs configurationUpdateStatusEventArgs;
            IConfigurationUpdateStatus configurationUpdateStatus;
            ConfigurationUpdateResultEventArgs configurationUpdateResultEventArgs;

            try
            {

                string userName;
                string password;
                var clusterNames = ClusterNames(cassandraStorageAccountAccessKey, out userName, out password);


                CassandraRepository respository = new CassandraRepository(clusterNames, userName, password, keyspace);

                var dt = DataTable.New.Read(configurationCsvPathFileName);



                foreach (var dr in dt.Rows)
                {
                    //Check for cancellation.
                    cancellationToken.ThrowIfCancellationRequested();

                    var smsEntity = new SmsTemplateEntity();

                    var names = dr.ColumnNames.ToList();


                    foreach (var name in names)
                    {
                        var value = dr.GetValueOrEmpty(name);
                        if (name.Contains(ClientId))
                            smsEntity.ClientId = Convert.ToInt32(value);
                        else if (name.Contains(InsightTypeName))
                            smsEntity.InsightTypeName = value;
                        else if (name.Contains(TemplateId))
                            smsEntity.TemplateId = value;
                        else if (name.Contains(Body))
                            smsEntity.Body = value;
                    }



                    if (!string.IsNullOrEmpty(smsEntity.InsightTypeName) &&
                        !string.IsNullOrEmpty(smsEntity.TemplateId))
                    {
                      
                        respository.InsertOrUpdate(smsEntity, GetEnumDescription(CassandraTable.SmsTemplate));

                        //Report: Status.
                        configurationUpdateStatusEventArgs = new ConfigurationUpdateStatusEventArgs();
                        var statusMessage =
                            $"Sms Tempate for {smsEntity.TemplateId} updated.";

                        configurationUpdateStatus = ConfigurationUpdateStatusFactory.CreateConfigurationUpdateStatus();
                        configurationUpdateStatus.StatusMessage = statusMessage;
                        configurationUpdateStatusEventArgs.ConfigurationUpdateStatus = configurationUpdateStatus;

                        if (ConfigurationUpdateStatus != null)
                        {
                            ConfigurationUpdateStatus(this, configurationUpdateStatusEventArgs);
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                Status status;
                configurationUpdateResult = ConfigurationUpdateResultFactory.CreateConfigurationUpdateResult();
                configurationUpdateResultEventArgs = new ConfigurationUpdateResultEventArgs();

                status = new Status();
                status.Exception = ex;
                status.StatusServerity = StatusTypes.StatusSeverity.Error;
                configurationUpdateResult.StatusList.Add(status);
                configurationUpdateResultEventArgs.ConfigurationUpdateResult = configurationUpdateResult;
                if (ConfigurationUpdated != null)
                {
                    ConfigurationUpdated(this, configurationUpdateResultEventArgs);
                }
            }
        }


        /// <summary>
        /// Update Cassandra table: Trumpia Keyword Configuration.
        /// </summary>
        /// <param name="configurationCsvPathFileName"></param>
        /// <param name="cancellationToken"></param>
        /// <param name="cassandraStorageAccountAccessKey"></param>
        /// <param name="keyspace"></param>
        protected void UpdateCassandraTableTrumpiaKeywordConfiguration(string configurationCsvPathFileName, CancellationToken cancellationToken, string cassandraStorageAccountAccessKey, string keyspace)
        {
            IConfigurationUpdateResult configurationUpdateResult;
            ConfigurationUpdateStatusEventArgs configurationUpdateStatusEventArgs;
            IConfigurationUpdateStatus configurationUpdateStatus;
            ConfigurationUpdateResultEventArgs configurationUpdateResultEventArgs;

            try
            {
                string userName;
                string password;
                var clusterNames = ClusterNames(cassandraStorageAccountAccessKey, out userName, out password);


                CassandraRepository respository = new CassandraRepository(clusterNames, userName, password, keyspace);

                var dt = DataTable.New.Read(configurationCsvPathFileName);


                foreach (var dr in dt.Rows)
                {

                    var trumpiaKeywordConfigurationEntity = new TrumpiaKeywordConfigurationEntity();

                    var names = dr.ColumnNames.ToList();


                    foreach (var name in names)
                    {
                        var value = dr.GetValueOrEmpty(name);
                        if (name.Contains(TrumpiaKeyword))
                            trumpiaKeywordConfigurationEntity.TrumpiaKeyword = value;
                        else if (name.Contains(ClientId))
                            trumpiaKeywordConfigurationEntity.ClientId = value;
                    }


                    if (!string.IsNullOrEmpty(trumpiaKeywordConfigurationEntity.TrumpiaKeyword) &&
                        !string.IsNullOrEmpty(trumpiaKeywordConfigurationEntity.ClientId))
                    {

                        respository.InsertOrUpdate(trumpiaKeywordConfigurationEntity, GetEnumDescription(CassandraTable.TrumpiaKeywordConfiguration));

                        //Check for cancellation.
                        cancellationToken.ThrowIfCancellationRequested();

                        //Report: Status.
                        configurationUpdateStatusEventArgs = new ConfigurationUpdateStatusEventArgs();
                        var statusMessage =
                            $"Trumpia Keyword {trumpiaKeywordConfigurationEntity.TrumpiaKeyword} updated.";

                        configurationUpdateStatus = ConfigurationUpdateStatusFactory.CreateConfigurationUpdateStatus();
                        configurationUpdateStatus.StatusMessage = statusMessage;
                        configurationUpdateStatusEventArgs.ConfigurationUpdateStatus = configurationUpdateStatus;

                        ConfigurationUpdateStatus?.Invoke(this, configurationUpdateStatusEventArgs);
                    }
                }

            }
            catch (Exception ex)
            {
                Status status;
                configurationUpdateResult = ConfigurationUpdateResultFactory.CreateConfigurationUpdateResult();
                configurationUpdateResultEventArgs = new ConfigurationUpdateResultEventArgs();

                status = new Status();
                status.Exception = ex;
                status.StatusServerity = StatusTypes.StatusSeverity.Error;
                configurationUpdateResult.StatusList.Add(status);
                configurationUpdateResultEventArgs.ConfigurationUpdateResult = configurationUpdateResult;
                ConfigurationUpdated?.Invoke(this, configurationUpdateResultEventArgs);
            }
        }


        /// <summary>
        /// Handle unexpected Cassandra table.
        /// </summary>
        /// <param name="cassandraTable"></param>
        protected void HandleUnexpectedCassandraTable(CassandraTable cassandraTable)
        {
            IConfigurationUpdateResult configurationUpdateResult;
            ConfigurationUpdateStatusEventArgs configurationUpdateStatusEventArgs;
            IConfigurationUpdateStatus configurationUpdateStatus;
            ConfigurationUpdateResultEventArgs configurationUpdateResultEventArgs;
            string statusMessage;

            try
            {


                //Report: Status.
                configurationUpdateStatusEventArgs = new ConfigurationUpdateStatusEventArgs();
                statusMessage = string.Format("Unexpected Cassandra Table. Update bypassed (CassandraTable: {0})",
                                               cassandraTable.ToString());

                configurationUpdateStatus = ConfigurationUpdateStatusFactory.CreateConfigurationUpdateStatus();
                configurationUpdateStatus.StatusMessage = statusMessage;
                configurationUpdateStatusEventArgs.ConfigurationUpdateStatus = configurationUpdateStatus;

                if (ConfigurationUpdateStatus != null)
                {
                    ConfigurationUpdateStatus(this, configurationUpdateStatusEventArgs);
                }

            }
            catch (Exception ex)
            {
                Status status;
                configurationUpdateResult = ConfigurationUpdateResultFactory.CreateConfigurationUpdateResult();
                configurationUpdateResultEventArgs = new ConfigurationUpdateResultEventArgs();

                status = new Status();
                status.Exception = ex;
                status.StatusServerity = StatusTypes.StatusSeverity.Error;
                configurationUpdateResult.StatusList.Add(status);
                configurationUpdateResultEventArgs.ConfigurationUpdateResult = configurationUpdateResult;
                if (ConfigurationUpdated != null)
                {
                    ConfigurationUpdated(this, configurationUpdateResultEventArgs);
                }
            }
        }

        #endregion

        public static string GetEnumDescription(Enum value)
        {
            FieldInfo fi = value.GetType().GetField(value.ToString());

            DescriptionAttribute[] attributes =
                (DescriptionAttribute[])fi.GetCustomAttributes(
                typeof(DescriptionAttribute),
                false);

            if (attributes.Length > 0)
                return attributes[0].Description;
            else
                return value.ToString();
        }

        private static string ClusterNames(string cassandraStorageAccountAccessKey, out string userName, out string password)
        {
            var arrStorageAccountAccessKey = cassandraStorageAccountAccessKey.Split(';');
            string clusterNames = arrStorageAccountAccessKey[0].Trim();
            userName = arrStorageAccountAccessKey[1].Replace("UserName=", "").Trim();
            password = arrStorageAccountAccessKey[2].Replace("Password=", "").Trim();
            return clusterNames;
        }
    }
}
