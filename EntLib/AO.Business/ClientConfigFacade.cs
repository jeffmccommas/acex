﻿using System;
using System.Collections.Generic;
using System.Linq;
using AO.BusinessContracts;
using CE.AO.Models;
using CE.BillToDate;
using CE.ContentModel;
using CE.ContentModel.Entities;
using Newtonsoft.Json;

namespace AO.Business
{
    /// <summary>
    /// The ClientConfigFacade class is used to manage the configured settings/modules for the current client.
    /// </summary>
    public class ClientConfigFacade : IClientConfigFacade
    {
        private readonly IContentModelFactory _factory;

        public ClientConfigFacade(IContentModelFactory factory)
        {
            _factory = factory;
        }

        /// <summary>
        /// Retrieves all registered system modules and client associations with those modules and dynamically
        /// builds the module and its menu structure.
        /// </summary>
        public DynamicAppModuleConfig GetDynamicPortalModules(int clientId)
        {
            var provider = _factory.CreateContentProvider(clientId);

            var systemConfigBulk = provider.GetPortalModuleSystemConfigurationBulk("SystemModuleConfig", string.Empty);
            var clientConfigBulk = provider.GetContentClientConfigurationBulk("ClientModuleConfig", string.Empty);

            var systemAppModules = (
                systemConfigBulk != null && systemConfigBulk.Count > 0
                    ? JsonConvert.DeserializeObject<List<DynamicAppModule>>(systemConfigBulk[0].JsonValue)
                    : null);
            var clientAppModuleConfig = (
                clientConfigBulk != null && clientConfigBulk.Count > 0
                    ? JsonConvert.DeserializeObject<DynamicClientModuleConfig>(clientConfigBulk[0].Value)
                    : null);

            return BuildAppModuleConfig(systemAppModules, clientAppModuleConfig);
        }

        public IList<ClientUOM> GetClientUom(int clientId)
        {
            //Get settings from Contentful
            var provider = _factory.CreateContentProvider(clientId);

            return provider.GetContentUom(string.Empty);
        }

        public List<ClientTextContent> GetTextContents(int clientId, string locale, string category)
        {
            //Get settings from Contentful
            var provider = _factory.CreateContentProvider(clientId);

            var textContentList = provider.GetContentTextContent(category, provider.GetContentLocale(locale),
                string.Empty);

            return textContentList;
        }

        /// <summary>
        /// Returns Client Settings for a particular client
        /// </summary>
        public ClientSettings GetClientSettings(int clientId)
        {
            var provider = _factory.CreateContentProvider(clientId);
            List<ClientConfigurationBulk> setting = provider.GetContentClientConfigurationBulk("system", "aclaraone.clientsettings");
            return setting != null && setting.Count > 0 ? JsonConvert.DeserializeObject<ClientSettings>(setting[0].Value) : null;
        }

        /// <summary>
        /// Returns BillToDate Settings for a particular client
        /// </summary>
        public BillToDateSettings GetBillToDateSettings(int clientId)
        {
            var reader = new CE.BillToDate.Configuration.SettingsReader(clientId);
            return reader.GetSettings();
        }

        /// <summary>
        /// Constructs the client's module and menu item structure.
        /// </summary>
        private DynamicAppModuleConfig BuildAppModuleConfig(List<DynamicAppModule> systemAppModules, DynamicClientModuleConfig clientAppModuleConfig)
        {
            var dynamicAppModuleConfig = new DynamicAppModuleConfig();

            if (systemAppModules == null)
            {
                throw new ArgumentNullException(nameof(systemAppModules),
                    @"Configuration Error:  The system application configuration has not been properly configured.");
            }
            if (clientAppModuleConfig == null)
            {
                throw new ArgumentNullException(nameof(clientAppModuleConfig),
                    @"Configuration Error:  The client application configuration has not been properly configured for this client.");
            }

            dynamicAppModuleConfig.DefaultState = clientAppModuleConfig.DefaultState;
            dynamicAppModuleConfig.Modules = new List<DynamicAppModule>();

            foreach (var configuredModule in clientAppModuleConfig.ConfiguredModules.OrderBy(cm => cm.Position))
            {
                var dynamicAppModule = systemAppModules.Single(sam => sam.Id == configuredModule.Id);

                if (configuredModule.CustomMenuItems != null && configuredModule.CustomMenuItems.Count > 0)
                {
                    foreach (DynamicAppMenuItem customMenuItem in configuredModule.CustomMenuItems)
                    {
                        dynamicAppModule.MenuItems.Add(customMenuItem);
                    }
                }

                dynamicAppModuleConfig.Modules.Add(dynamicAppModule);
            }

            return dynamicAppModuleConfig;
        }

        /// <summary>
        /// Returns Client Settings for a particular client using Configuration Bulk Key.
        /// </summary>
        public ClientSettingsExport GetClientSettingsExport(int clientId, string bulkKey)
        {
            if (string.IsNullOrEmpty(bulkKey))
                bulkKey = "aclaraone.clientexportsettings";
            var provider = _factory.CreateContentProvider(clientId);
            List<ClientConfigurationBulk> setting = provider.GetContentClientConfigurationBulk("system", bulkKey);
            return setting != null && setting.Count > 0 ? JsonConvert.DeserializeObject<ClientSettingsExport>(setting[0].Value) : null;
        }

        /// <summary>
        /// Returns particular filter details of client settings.
        /// </summary>
        /// <param name="clientid">client id of client.</param>
        /// <param name="filtername">filter name to select from the list of filters</param>
        /// <returns></returns>
        public Filter GetClientSettingExportFilter(int clientid, string filtername)
        {
            var clientsettings = GetClientSettingsExport(clientid, "aclaraone.clientexportsettings");
            return clientsettings.Filters.FirstOrDefault(x => x.FilterName == filtername);
        }

        /// <summary>
        /// Get Standard Unit of Measure from Insight Metadata DB
        /// </summary>
        /// <returns></returns>
        public StandardUomModel GetStandardUom()
        {
            var provider = _factory.CreateContentProvider(0);
            List<ClientConfigurationBulk> setting = provider.GetContentClientConfigurationBulk("system", "aclaraone.standarduomsettings");
            return setting != null && setting.Count > 0 ? setting[0].Value != null ? JsonConvert.DeserializeObject<StandardUomModel>(setting[0].Value) : null : null;
        }

    }
}