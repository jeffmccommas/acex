﻿using System;
using AO.BusinessContracts;
using AO.DataAccess.Contracts;
using AO.Entities;
using AutoMapper;
using CE.AO.Models;

namespace AO.Business
{
    public class MtuTypeMapping : IMtuTypeMapping
    {
        public LogModel LogModel { get; set; }

        private Lazy<ICassandraRepository> CassandraRepository { get; }

        private Lazy<ICacheProvider> CacheProvider { get; }

        public MtuTypeMapping(LogModel logModel, Lazy<ICassandraRepository> cassandraRepository, Lazy<ICacheProvider> cacheProvider)
        {
            LogModel = logModel;
            CassandraRepository = cassandraRepository;
            CacheProvider = cacheProvider;
        }

        /// <summary>
        /// Get MtuTypeMapping information of the specific mtu type from mtu_type table
        /// </summary>
        /// <param name="mtuTypeId"> mtuTypeId from which Mapping is fetched  </param>
        /// <returns>True</returns>
        public MtuTypeModel GetMtuTypeMapping(int mtuTypeId)
        {
            MtuTypeEntity mtuTypeDetails;
            var keyValue = CacheProvider.Value.Get<MtuTypeEntity>($"{mtuTypeId}", (int)BusinessContracts.Enums.CacheDatabases.MtuTypesDatabaseId);
            if (keyValue == null)
            {
                mtuTypeDetails =
                    CassandraRepository.Value.GetSingle<MtuTypeEntity>(Constants.CassandraTableNames.MtuTypeMapping,
                        a => a.TableName == Constants.CassandraTableNames.MtuTypeMapping && a.MtuTypeId == mtuTypeId);
                if (mtuTypeDetails != null)
                    CacheProvider.Value.Set($"{mtuTypeId}", mtuTypeDetails, (int)BusinessContracts.Enums.CacheDatabases.MtuTypesDatabaseId);
            }
            else
            {
                mtuTypeDetails = keyValue;
            }
            return Mapper.Map<MtuTypeEntity, MtuTypeModel>(mtuTypeDetails);
        }
    }
}
