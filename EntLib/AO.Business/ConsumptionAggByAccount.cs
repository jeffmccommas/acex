﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AO.BusinessContracts;
using AO.DataAccess.Contracts;
using AO.Entities;
using AutoMapper;
using CE.AO.Models;

namespace AO.Business
{
    /// <summary>
    /// ConsumptionAggByAccount class is used to insert/update the ConsumptionAggByAccountModel to the database
    /// </summary>
    public class ConsumptionAggByAccount : IConsumptionAggByAccount
    {
        public LogModel LogModel { get; set; }

        private Lazy<ICassandraRepository> CassandraRepository { get; }

        public ConsumptionAggByAccount(LogModel logModel, Lazy<ICassandraRepository> cassandraRepository)
        {
            LogModel = logModel;
            CassandraRepository = cassandraRepository;
        }

        /// <summary>
        /// To insert/update the ConsumptionAggByAccountModel to database
        /// </summary>
        /// <param name="model">ConsumptionAggByAccountModel which is to be inserted/updated</param>
        /// <returns>True</returns>
        public async Task<bool> InsertOrMergeConsumptionAggByAccountAsync(ConsumptionAggByAccountModel model)
        {
            var entity = Mapper.Map<ConsumptionAggByAccountModel, ConsumptionAggByAccountEntity>(model);
            await CassandraRepository.Value.InsertOrUpdateAsync(entity, Constants.CassandraTableNames.ConsumptionAggByAccount).ConfigureAwait(false);
            return await Task.Factory.StartNew(() => true);
        }

        /// <summary>
        /// To insert/update the ConsumptionAggByAccountModel to database in batch
        /// </summary>
        /// <param name="models">List of ConsumptionAggByAccountModel which are to be inserted/updated</param>        
        /// <returns>True</returns>
        public async Task<bool> InsertOrMergeConsumptionAggByAccountAsync(List<ConsumptionAggByAccountModel> models)
        {
            List<ConsumptionAggByAccountEntity> entities = Mapper.Map<List<ConsumptionAggByAccountModel>, List<ConsumptionAggByAccountEntity>>(models);
            await CassandraRepository.Value.InsertOrUpdateBatchAsync(entities, Constants.CassandraTableNames.ConsumptionAggByAccount).ConfigureAwait(false);
            return await Task.Factory.StartNew(() => true);
        }

        /// <summary>
        /// To get the ConsumptionAggByAccountModels from database
        /// </summary>        
        /// <param name="clientId"></param>
        /// <param name="aggregationEndDateTime"></param>
        /// <param name="accountNumber"></param>        
        public async Task<IList<ConsumptionAggByAccountModel>> GetConsumptionAggByAccountAsync(int clientId, DateTime aggregationEndDateTime, string accountNumber)
        {
            var aggregationTypes = new[]
            {
                Convert.ToString(CE.AO.Utilities.Enums.PreAggregationTypes.Day),
                Convert.ToString(CE.AO.Utilities.Enums.PreAggregationTypes.Hour)
            };
            IList<ConsumptionAggByAccountEntity> entities = await
                CassandraRepository.Value.GetAsync<ConsumptionAggByAccountEntity>(
                    Constants.CassandraTableNames.ConsumptionAggByAccount,
                    c =>
                        c.ClientId == clientId && c.AccountNumber == accountNumber && aggregationTypes.Contains(c.AggregationType) &&
                        c.AggregationEndDateTime == aggregationEndDateTime);
            return Mapper.Map<IList<ConsumptionAggByAccountEntity>, IList<ConsumptionAggByAccountModel>>(entities);
        }

        /// <summary>
        /// To delete the ConsumptionAggByAccountModel to the database using Partition and Cluster Keys
        /// </summary>
        /// <param name="model">ConsumptionAggByAccountModel which is to be deleted</param>
        /// <returns>True</returns>
        public async Task DeleteConsumptionAggByAccountAsync(ConsumptionAggByAccountModel model)
        {
            await
                CassandraRepository.Value.DeleteAsync<ConsumptionAggByAccountEntity>(
                    Constants.CassandraTableNames.ConsumptionAggByAccount,
                    p =>
                        p.ClientId == model.ClientId && p.AccountNumber == model.AccountNumber &&
                        p.AggregationType == model.AggregationType && p.AggregationEndDateTime == model.AggregationEndDateTime).ConfigureAwait(false);
        }

        /// <summary>
        /// To delete the ConsumptionAggByAccountModel to the database using Partition and Cluster Keys
        /// </summary>
        /// <param name="model">ConsumptionAggByAccountModel which is to be deleted</param>
        /// <returns>True</returns>
        public async Task DeleteConsumptionAggByAccountUsingPartionAsync(ConsumptionAggByAccountModel model)
        {
            await
                CassandraRepository.Value.DeleteAsync<ConsumptionAggByAccountEntity>(
                    Constants.CassandraTableNames.ConsumptionAggByAccount,
                    p =>
                        p.ClientId == model.ClientId && p.AccountNumber == model.AccountNumber).ConfigureAwait(false);
        }


    }
}
