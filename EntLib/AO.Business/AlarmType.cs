﻿using System;
using System.Threading.Tasks;
using AO.BusinessContracts;
using AO.DataAccess.Contracts;
using AO.Entities;
using AutoMapper;
using CE.AO.Models;

namespace AO.Business
{
	/// <summary>
	/// IAlarmTypes Interface Implementation for Alarmtype
	/// </summary>
	public class AlarmType : IAlarmTypes
	{
		public LogModel LogModel { get; set; }

		private Lazy<ICassandraRepository> CassandraRepository { get; }

		private Lazy<ICacheProvider> CacheProvider { get; }

		public AlarmType(LogModel logModel, Lazy<ICassandraRepository> cassandraRepository, Lazy<ICacheProvider> cacheProvider)
		{
			LogModel = logModel;
			CassandraRepository = cassandraRepository;
			CacheProvider = cacheProvider;
		}

		/// <summary>
		/// To delete the AlarmTypeModel from alarm_type table
		/// </summary>
		/// <param name="model">AlarmTypeModel which is to be deleted</param>
		/// <returns>True</returns>
		public async Task<bool> DeleteAlarmTypeAsync(AlarmTypeModel model)
		{
			await CassandraRepository.Value.DeleteAsync<AlarmTypeEntity>(Constants.CassandraTableNames.AlarmType,
			p => p.AlarmSourceTypeId == model.AlarmSourceTypeId && p.AlarmGroupId == model.AlarmGroupId && p.AlarmTypeId == model.AlarmTypeId).ConfigureAwait(false);
			CacheProvider.Value.Remove($"{model.AlarmSourceTypeId}_{model.AlarmGroupId}_{model.AlarmTypeId}",
				(int)Enums.CacheDatabases.AlarmTypesDatabaseId);
			return await Task.Factory.StartNew(() => true);
		}

		/// <summary>
		/// Get AlarmTypeModel information of the specific code from AlarmTypeModel table
		/// </summary>
		/// <param name="alarmSourceTypeId"> Alarm Source Type Id </param>
		/// <param name="alarmGroupId"> Alarm Group Id </param>
		/// <param name="alarmTypeId"> Alarm Type Id </param>
		/// <returns>AlarmTypeModel</returns>
		public AlarmTypeModel GetAlarmType(int alarmSourceTypeId, int alarmGroupId, int alarmTypeId)
		{
			var alarmDetails = CacheProvider.Value.Get<AlarmTypeEntity>($"{alarmSourceTypeId}_{alarmGroupId}_{alarmTypeId}",
				(int)Enums.CacheDatabases.AlarmTypesDatabaseId);
			if (alarmDetails == null)
			{
				alarmDetails = CassandraRepository.Value.GetSingle<AlarmTypeEntity>(Constants.CassandraTableNames.AlarmType,
			   a => a.AlarmSourceTypeId == alarmSourceTypeId && a.AlarmGroupId == alarmGroupId && a.AlarmTypeId == alarmTypeId);
				if (alarmDetails != null)
					CacheProvider.Value.Set($"{alarmSourceTypeId}_{alarmGroupId}_{alarmTypeId}", alarmDetails,
						(int)Enums.CacheDatabases.AlarmTypesDatabaseId);
			}
			return Mapper.Map<AlarmTypeEntity, AlarmTypeModel>(alarmDetails);
		}

		/// <summary>
		/// To insert/update the AlarmTypeModel to database
		/// </summary>
		/// <param name="model">AlarmTypeModel which is to be inserted/updated</param>
		/// <returns>True</returns>
		public async Task<bool> InsertOrMergeAlarmTypeAsync(AlarmTypeModel model)
		{
			var entity = Mapper.Map<AlarmTypeModel, AlarmTypeEntity>(model);
			await CassandraRepository.Value.InsertOrUpdateAsync(entity, Constants.CassandraTableNames.AlarmType).ConfigureAwait(false);
			CacheProvider.Value.Set($"{entity.AlarmSourceTypeId}_{entity.AlarmGroupId}_{entity.AlarmTypeId}", entity,
				(int)Enums.CacheDatabases.AlarmTypesDatabaseId);
			return await Task.Factory.StartNew(() => true);
		}
	}
}
