﻿using System;
using System.Threading.Tasks;
using AO.BusinessContracts;
using AO.DataAccess.Contracts;
using AO.Entities;
using AutoMapper;
using CE.AO.Logging;
using CE.AO.Models;

namespace AO.Business
{
    /// <summary>
    /// Customer class is used to fetch/insert/update the customer details from/to the database
    /// </summary>
    public class Customer : ICustomer
    {
        private readonly Lazy<ISendSms> _sendSmsManager;
        private readonly Lazy<ICassandraRepository> _cassandraRepository; 
         
        public Customer(LogModel logModel, Lazy<ICassandraRepository> cassandraRepository, Lazy<ISendSms> sendSmsManager)
        {
            LogModel = logModel;
            _cassandraRepository = cassandraRepository;
            _sendSmsManager = sendSmsManager;
        }

        public LogModel LogModel { get; set; }
        
        /// <summary>
        /// To fetch the customer detail from the database
        /// </summary>
        /// <param name="clientId">clientId of the data to be fetched</param>
        /// <param name="customerId">customerId of the data to be fetched</param>
        /// <returns>CustomerModel</returns>
        public async Task<CustomerModel> GetCustomerAsync(int clientId, string customerId)
        {
            var entity =
                await
                    _cassandraRepository.Value.GetSingleAsync<CustomerEntity>(Constants.CassandraTableNames.Customer,
                        c => c.ClientId == clientId && c.CustomerId == customerId);
            return Mapper.Map<CustomerEntity, CustomerModel>(entity);
        }

        /// <summary>
        /// To insert or update CustomerModel to database
        /// </summary>
        /// <param name="model">model to be inserted</param>
        /// <returns>true</returns>
        public async Task<bool> InsertOrMergeCustomerAsync(CustomerModel model)
        {
            try
            {
                // report copy
                var entityReport = Mapper.Map<CustomerModel, CustomerReportEntity>(model);
                entityReport.LastModifiedDate = DateTime.UtcNow;
                entityReport.RowCreateDate = DateTime.UtcNow.ToString("yyyyMMdd");
                _cassandraRepository.Value.InsertOrUpdate(entityReport, Constants.CassandraTableNames.CustomerReport);
            }
            catch (Exception ex)
            {
                Logger.Error(ex,LogModel);
            }
            var entity = Mapper.Map<CustomerModel, CustomerEntity>(model);
            entity.LastModifiedDate = DateTime.UtcNow;
            _cassandraRepository.Value.InsertOrUpdate(entity, Constants.CassandraTableNames.Customer);
            return await Task.Factory.StartNew(() => true);
        }

        /// <summary>
        /// Create subscription and then update customer with newly created trumpia subscription id
        /// </summary>
        /// <param name="customerModel">customerModel of the trumpia subscription to be created</param>
        /// <param name="apiKey">apiKey of the trumpia subscription to be created</param>
        /// <param name="username">username of the trumpia subscription to be created</param>
        /// <param name="contactList">contactList of the trumpia subscription to be created</param>
        /// <returns>bool</returns>
        public bool CreateSubscriptionAndUpdateCustomer(CustomerModel customerModel, string apiKey, string username,
            string contactList)
        {
            if (string.IsNullOrEmpty(customerModel.Phone1))
            {
                Logger.Fatal(@" Phone number not specified in Customer information.", LogModel);
                return false;
            }

            var requestId = _sendSmsManager.Value.CreateNewTrumpiaSubscription(customerModel, apiKey, username,
                contactList);
            if (!string.IsNullOrEmpty(requestId))
            {
                customerModel.TrumpiaSubscriptionId = _sendSmsManager.Value.GetSubscriptionId(customerModel, requestId, apiKey, username);
                //Update customer with new subscription id
                customerModel.TrumpiaRequestDetailId = requestId;
                Task<bool> customerTask = Task.Run(() => InsertOrMergeCustomerAsync(customerModel));
                Task.WaitAll(customerTask);
                return customerTask.Result;
            }
            return false;
        }

        /// <summary>
        /// Check whether trumpia subscription is created or not, if not then create subscription and then update customer with newly created trumpia subscription id
        /// </summary>
        /// <param name="customerModel">customerModel of the trumpia subscription to be fetched/created</param>
        /// <param name="apiKey">apiKey of the trumpia subscription to be fetched/created</param>
        /// <param name="username">username of the trumpia subscription to be fetched/created</param>
        /// <param name="contactList">contactList of the trumpia subscription to be fetched/created</param>
        /// <param name="isReOptIn">if the customer is opt-out by text, then to re-register set isReOptIn</param>
        /// <returns>bool</returns>
        public bool SmsSubscirptionCheckAndUpdate(CustomerModel customerModel, string apiKey, string username,
            string contactList, ref bool isReOptIn)
        {
            if (string.IsNullOrEmpty(customerModel.Phone1))
            {
                Logger.Fatal(@" Phone number not specified in Customer information.", LogModel);
                return false;
            }

            var sendSms = new SendSms(LogModel);
            var smsSubscriptionInfo = sendSms.GetSubscriptionInfo(customerModel.TrumpiaSubscriptionId, apiKey, username);

            // update the existing subscription with the number
            if (smsSubscriptionInfo.status_code == null)
            {
                if (smsSubscriptionInfo.mobile == null)
                {
                    sendSms.UpdateSubscriptionInfo(customerModel, customerModel.TrumpiaSubscriptionId, apiKey, username,
                        contactList);
                    isReOptIn = true;
                    return true;
                }
                if (string.IsNullOrEmpty(smsSubscriptionInfo.mobile.verified) ||
                    !Convert.ToBoolean(smsSubscriptionInfo.mobile.verified)) return false;
                if (!string.IsNullOrEmpty(smsSubscriptionInfo.mobile.value) &&
                    smsSubscriptionInfo.mobile.value.Equals(customerModel.Phone1)) return false;
                sendSms.UpdateSubscriptionInfo(customerModel, customerModel.TrumpiaSubscriptionId, apiKey, username,
                    contactList);
                return true;
            }

            // if subscription is completely deleted, create a new subscription and update customer model
            CreateSubscriptionAndUpdateCustomer(customerModel, apiKey, username, contactList);
            isReOptIn = true;
            return true;
        }

        public async Task<bool> DeleteCustomerAsync(int clientId, string customerId)
        {
            await
                _cassandraRepository.Value.DeleteAsync<CustomerEntity>(Constants.CassandraTableNames.Customer,
                    a => a.ClientId == clientId && a.CustomerId == customerId).ConfigureAwait(false);
            return await Task.Factory.StartNew(() => true);
        }
    }
}
