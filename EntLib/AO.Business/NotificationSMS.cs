﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using AO.Entities;
using AutoMapper;
using CE.AO.Models;
using CE.ContentModel.Entities;
using Enums = CE.AO.Utilities.Enums;

namespace AO.Business
{
    /// <summary>
    /// NotificationSMS class is used to fetch SMS details, send SMS, get replaced sms templates
    /// </summary>
    public partial class Notification
    {
        /// <summary>
        /// send sms
        /// get the report status on the sms request
        /// update notification with the status
        /// </summary>
        /// <param name="billingModel"></param>
        /// <param name="subscriptions"></param>
        /// <param name="notificationModel"></param>
        /// <param name="customerModel"></param>
        /// <param name="customKey"></param>
        /// <param name="smsApiKey"></param>
        /// <param name="smsUserName"></param>
        /// <param name="smsBody"></param>
        /// <param name="insightModel"></param>
        /// <param name="programName"></param>
        /// <param name="insight"></param>
        /// <param name="requestDate"></param>
        /// <param name="tier"></param>
        /// <param name="notifyDateTime"></param>
        /// <returns></returns>
        public async Task SendSms(BillingModel billingModel, List<SubscriptionModel> subscriptions, NotificationModel notificationModel,
            CustomerModel customerModel, string customKey, string smsApiKey, string smsUserName, string smsBody, InsightModel insightModel,
            string programName, InsightSettings insight, DateTime requestDate, string tier, DateTime? notifyDateTime = null)
        {
            var sendSms = new SendSms(LogModel);

            var phone = customerModel.Phone1 ?? customerModel.Phone2;
            // send sms
            Task<string> task = sendSms.Send(smsApiKey, smsUserName, smsBody, phone, customerModel.TrumpiaSubscriptionId, customKey,
                insightModel.ClientId, insightModel.AccountId, programName, insightModel.ServiceContractId,
                insight.InsightName, requestDate, notifyDateTime);

            Task.WaitAll(task);

            if (task.Status == TaskStatus.RanToCompletion)
            {
                // get sms report
                notificationModel.TrumpiaRequestDetailPkRk = task.Result;
                notificationModel.TrumpiaRequestDetailId = task.Result;

                if (!string.IsNullOrEmpty(notificationModel.TrumpiaRequestDetailPkRk))
                    await GetSmsReport(billingModel, subscriptions, notificationModel, insightModel, customerModel, 
                        customerModel.TrumpiaSubscriptionId, smsApiKey, smsUserName, requestDate).ConfigureAwait(false);
            }

        }

        public async Task GetSmsReport(BillingModel billingModel, List<SubscriptionModel> subscriptions, NotificationModel notificationModel, InsightModel insightModel, CustomerModel customerModel, string subscriptionId, string apiKey, string username, DateTime requestedDate, int? retry = 0)
        {
            var statusCode = string.Empty;
            var errorMessage = string.Empty;
            var sendSms = new SendSms(LogModel);
            var bUpdate = true;
            var isScheduleUpdate = false;
            // check the status of the notification after request is sent
            if (notificationModel != null)
            {
                // sms request is sent
                notificationModel.IsNotified = true;

                // get trumpia details
                var trumpiaRequestDetailEntity = _trumpiaRequestDetail.GetTrumpiaRequestDetailAsync(notificationModel.TrumpiaRequestDetailId).Result;

                // if there's a trumpia request, get status on the request
                if (trumpiaRequestDetailEntity != null)
                {
                    // get report status on the request
                    ////Logger.Info($"Trumpia Request Id {trumpiaRequestDetailEntity.RequestId}", LogModel);
                    Task<SmsStatsModel> task = sendSms.GetSmsStatics(trumpiaRequestDetailEntity.RequestId, apiKey, username);
                    Task.WaitAll(task);
                    var smsStats = task.Result;
                    if (smsStats == null ||
                        (string.IsNullOrEmpty(smsStats.status_code) && string.IsNullOrEmpty(smsStats.status)))
                    {
                        GetSmsReport(billingModel, subscriptions, notificationModel, insightModel, customerModel, 
                            subscriptionId, apiKey, username,
                            requestedDate, retry + 1).Wait();
                    }
                    else
                    {
                        if (!string.IsNullOrEmpty(smsStats.status_code))
                        {
                            statusCode = smsStats.status_code;
                            errorMessage = smsStats.error_message;
                            switch (smsStats.status_code)
                            {
                                case StatusMmsNotSupported:
                                case StatusInvalidSubscription:
                                case StatusNoRecipient:
                                    // update opt out date in subscription
                                    _subscriptionManager.InsertorMergeForSmsOptOut(billingModel, subscriptions,
                                        notificationModel.ClientId,
                                        notificationModel.CustomerId, notificationModel.AccountId,
                                        notificationModel.ServiceContractId, notificationModel.ProgramName,
                                        notificationModel.Insight);
                                    break;
                                case StatusInProgress:
                                    // if it's still in progress, get the report again
                                    GetSmsReport(billingModel, subscriptions, notificationModel, insightModel, customerModel, 
                                        subscriptionId, apiKey, username,
                                        requestedDate, retry + 1).Wait();
                                    bUpdate = false;
                                    break;
                                default:
                                    notificationModel.IsDropped = true;
                                    break;
                            }
                        }
                        else if (!string.IsNullOrEmpty(smsStats.status))
                        {
                            if (smsStats.status == StatusSending)
                            {
                                GetSmsReport(billingModel, subscriptions, notificationModel, insightModel, customerModel, 
                                    subscriptionId, apiKey, username,
                                    requestedDate, retry + 1).Wait();
                                bUpdate = false;

                            }
                            else if (smsStats.status == StatusScheduled)
                            {
                                notificationModel.IsScheduled = true;
                            }
                            else if (smsStats.status == StatusSent)
                            {
                                if (smsStats.sms.sent > 0)
                                {
                                    notificationModel.IsDelivered = true;
                                    //Changing isScheduled from true to null
                                    notificationModel.IsScheduled = false;
                                    isScheduleUpdate = true;
                                    bUpdate = false;
                                }
                                else if (smsStats.sms.failed > 0)
                                {
                                    notificationModel.IsBounced = true;
                                    errorMessage = "Invalid number.  Check if the number is valid or blocked.";
                                    //Changing isScheduled from true to null
                                    notificationModel.IsScheduled = false;
                                    isScheduleUpdate = true;
                                    bUpdate = false;
                                }
                            }
                        }
                        else
                        {
                            errorMessage = smsStats.error_message;
                            notificationModel.IsDropped = true;
                        }
                        if (bUpdate)
                        {
                            var latestEval = string.IsNullOrEmpty(insightModel.ServiceContractId)
                                       ? _evalationManger.GetAccountLevelEvaluationsAsync(insightModel.ClientId,
                                           insightModel.CustomerId, insightModel.AccountId, insightModel.ProgramName).Result
                                       : _evalationManger.GetServiceLevelEvaluationsAsync(insightModel.ClientId,
                                           insightModel.CustomerId, insightModel.AccountId, insightModel.ProgramName,
                                           insightModel.ServiceContractId).Result;

                            // check if it's the latest evaluation before continue
                            if (latestEval.AsOfEvaluationId == insightModel.AsOfEvaluationId)
                            {
                                await SaveLatestAndHistoryNotification(notificationModel).ConfigureAwait(false);
                                if (!string.IsNullOrEmpty(statusCode))
                                    trumpiaRequestDetailEntity.StatusCode = statusCode;
                                if (!string.IsNullOrEmpty(errorMessage))
                                    trumpiaRequestDetailEntity.ErrorMessage = errorMessage;

                                if (!string.IsNullOrEmpty(statusCode) || !string.IsNullOrEmpty(errorMessage))
                                    await _trumpiaRequestDetail.InsertOrMergeTrumpiaRequestDetailAsync(trumpiaRequestDetailEntity);
                            }
                        }
                        if (isScheduleUpdate)
                        {
                            var latestEval = string.IsNullOrEmpty(insightModel.ServiceContractId)
                                          ? _evalationManger.GetAccountLevelEvaluationsAsync(insightModel.ClientId,
                                              insightModel.CustomerId, insightModel.AccountId, insightModel.ProgramName).Result
                                          : _evalationManger.GetServiceLevelEvaluationsAsync(insightModel.ClientId,
                                              insightModel.CustomerId, insightModel.AccountId, insightModel.ProgramName,
                                              insightModel.ServiceContractId).Result;

                            // check if it's the latest evaluation before continue
                            if (latestEval.AsOfEvaluationId == insightModel.AsOfEvaluationId)
                            {
                                SaveNotification(notificationModel, true);
                            }
                        }
                    }
                }
            }
        }

        public async Task<SmsTemplateModel> GetSmsTemplateFromDB(int clientId, string insightTypeName, string smsTemplateId)
        {
            var serviceDetails =
                 await
                     _cassandraRepository.Value.GetSingleAsync<SmsTemplateEntity>(Constants.CassandraTableNames.SmsTemplate, s => s.ClientId == clientId && s.InsightTypeName == insightTypeName && s.TemplateId == smsTemplateId);
            return Mapper.Map<SmsTemplateEntity, SmsTemplateModel>(serviceDetails);
        }

        /// <summary>
        /// To fetch the sms template for subscription double opt in confirmation with replaced substitutions
        /// </summary>
        /// <param name="smsBody">body of the sms</param>
        /// <param name="subscriptionModel">subscription for double opt in confirmation</param>
        /// <param name="clientSetting">client setting for double opt in confirmation</param>
        /// <param name="customerModel"></param>
        /// <param name="substitutes">substitutes needs to replaced with actual value</param>
        /// <returns>sms body</returns>
        public string GetReplacedSmsTemplateForSubscriptionDoubleOptInConfirmation(string smsBody, SubscriptionModel subscriptionModel, ClientSettings clientSetting, CustomerModel customerModel, MatchCollection substitutes)
        {
            foreach (Match match in substitutes)
            {
                foreach (Capture capture in match.Captures)
                {
                    switch (capture.Value)
                    {
                        case "-ProgramName-":
                            smsBody = smsBody.Replace(capture.Value, Convert.ToString(subscriptionModel.ProgramName));
                            break;
                        case "-AccountNumber-":
                            smsBody = smsBody.Replace(capture.Value, clientSetting.UnmaskedAccountIdEndingDigit > 0 ? subscriptionModel.AccountId.Substring(subscriptionModel.AccountId.Length - clientSetting.UnmaskedAccountIdEndingDigit, clientSetting.UnmaskedAccountIdEndingDigit) : Convert.ToString(subscriptionModel.AccountId));
                            break;
                        case "-Keyword-":
                            {
                                var program =
                                    clientSetting.Programs.FirstOrDefault(
                                        p => p.UtilityProgramName == subscriptionModel.ProgramName);
                                if (program != null)
                                {
                                    smsBody = smsBody.Replace(capture.Value, Convert.ToString(program.TrumpiaKeyword));
                                }
                                break;
                            }
                        case "-ShortCode-":
                            smsBody = smsBody.Replace(capture.Value, Convert.ToString(clientSetting.TrumpiaShortCode));
                            break;
                    }
                }
            }
            return smsBody;
        }

        private string GetReplacedSmsTemplate(ClientSettings clientSettings, string smsBody, CalculationModel calculation, BillingModel billingModel, CustomInsightModel customInsightModel, MatchCollection substitutes, int unmaskedAccontIdEndingDigit, IList<ClientUOM> uomContentList, List<ClientTextContent> textContentList)
        {
            var commodity = Enums.CommodityType.Undefined;
            if (calculation.CommodityId != null)
            {
                commodity = (Enums.CommodityType)calculation.CommodityId;
            }

            int? unit = billingModel?.UOMId ?? calculation.Unit;
            var uomId = 99;
            if (unit.HasValue)
            {
                uomId = unit.Value;
            }
            // if the calcution is done in a different UOM from bill's
            // convert the usage into bill's UOM for the notification
            double? usage;
            if (unit.HasValue && calculation.Unit.HasValue && unit != calculation.Unit)
            {
                var conversionFactor = Extensions.GetConversionFactor(calculation.ClientId, calculation.Unit.Value,
                    unit.Value);
                usage = calculation.Usage * conversionFactor;
            }
            else
            {
                usage = calculation.Usage;
            }

            // get uom content
            var uomKey = ((Enums.UomType)uomId).ToString();
            var uom = Extensions.GetUomTextContent(uomKey, uomContentList, textContentList);
            if (string.IsNullOrEmpty(uom))
                uom = uomKey;
            foreach (Match match in substitutes)
            {
                foreach (Capture capture in match.Captures)
                {
                    switch (capture.Value)
                    {
                        case "-AccountLevelCostThreshold-":
                        case "-ServiceLevelCostThreshold-":
                            {
                                string formattedValue = clientSettings.GetFormattedValue(customInsightModel.ThresholdValue ?? 0, "0.00", "altNumericFormat");
                                smsBody = smsBody.Replace(capture.Value, formattedValue);
                                // "smsBody = smsBody.Replace(capture.Value, $"{customInsightModel.ThresholdValue:0:00}");"
                            }
                            break;
                        case "-CostToDate-":
                        case "-BillToDate-":
                            {
                                string formattedValue = clientSettings.GetFormattedValue(calculation.Cost ?? 0, "0.00", "altNumericFormat");
                                smsBody = smsBody.Replace(capture.Value, formattedValue);
                                // "smsBody = smsBody.Replace(capture.Value, $"{calculation.Cost:0.00}");"
                            }
                            break;
                        case "-ProjectedCost-":
                            {
                                string formattedValue = clientSettings.GetFormattedValue(calculation.ProjectedCost ?? 0, "0.00", "altNumericFormat");
                                smsBody = smsBody.Replace(capture.Value, formattedValue);
                                // "smsBody = smsBody.Replace(capture.Value, $"{calculation.ProjectedCost:0.00}");"
                            }
                            break;
                        case "-ServiceLevelUsageThreshold-":
                        case "-DayThreshold-":
                            smsBody = smsBody.Replace(capture.Value, $"{customInsightModel.ThresholdValue:0.00}");
                            break;
                        case "-AccountNumber-":
                            if (unmaskedAccontIdEndingDigit > 0)
                                smsBody = smsBody.Replace(capture.Value, calculation.AccountId.Substring(calculation.AccountId.Length - unmaskedAccontIdEndingDigit, unmaskedAccontIdEndingDigit));
                            else
                                smsBody = smsBody.Replace(capture.Value, calculation.AccountId);
                            break;
                        case "-NoOfDaysRemaining-":
                            smsBody = smsBody.Replace(capture.Value, Convert.ToString((Convert.ToDateTime(calculation.BillCycleEndDate) - Convert.ToDateTime(calculation.BillCycleStartDate)).Days - Convert.ToDouble(calculation.BillDays), CultureInfo.InvariantCulture));
                            break;
                        case "-UOM-":
                            smsBody = smsBody.Replace(capture.Value, uom);
                            break;
                        case "-FUEL-":
                            smsBody = smsBody.Replace(capture.Value, Enums.GetDescriptionOfEnum(commodity));
                            break;
                        case "-Usage-":
                        case "-ServiceLevelTieredThresholdApproaching-":
                        case "-ServiceLevelTieredThresholdExceed-":
                            smsBody = smsBody.Replace(capture.Value, $"{usage:0.00}");
                            break;
                    }
                }
            }
            return smsBody;
        }

        public string GetReplacedSmsTemplateForSubscriptionDoubleOptInConfirmation(string smsBody, SubscriptionEntity subscriptionEntity, ClientSettings clientSetting, CustomerModel customerModel, MatchCollection substitutes)
        {
            foreach (Match match in substitutes)
            {
                foreach (Capture capture in match.Captures)
                {
                    switch (capture.Value)
                    {
                        case "-ProgramName-":
                            smsBody = smsBody.Replace(capture.Value, Convert.ToString(subscriptionEntity.ProgramName));
                            break;
                        case "-AccountNumber-":
                            if (clientSetting.UnmaskedAccountIdEndingDigit > 0)
                                smsBody = smsBody.Replace(capture.Value, subscriptionEntity.AccountId.Substring(subscriptionEntity.AccountId.Length - clientSetting.UnmaskedAccountIdEndingDigit, clientSetting.UnmaskedAccountIdEndingDigit));
                            else
                                smsBody = smsBody.Replace(capture.Value, Convert.ToString(subscriptionEntity.AccountId));
                            break;
                        case "-Keyword-":
                            {
                                var program =
                                    clientSetting.Programs.FirstOrDefault(
                                        p => p.UtilityProgramName == subscriptionEntity.ProgramName);
                                if (program != null)
                                {
                                    smsBody = smsBody.Replace(capture.Value, Convert.ToString(program.TrumpiaKeyword));
                                }
                                break;
                            }
                        case "-ShortCode-":
                            smsBody = smsBody.Replace(capture.Value, Convert.ToString(clientSetting.TrumpiaShortCode));
                            break;
                    }
                }
            }
            return smsBody;
        }
    }
}
