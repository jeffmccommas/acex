﻿using System.Linq;
using AO.BusinessContracts;
using AO.DataAccess.Contracts;
using AO.Entities;
using AutoMapper;
using CE.AO.Models;

namespace AO.Business
{
    /// <summary>
    /// TrumpiaKeywordConfiguration class is used to fetch the trumpia keyword configuration details from the database
    /// </summary>
    public class TrumpiaKeywordConfiguration : ITrumpiaKeywordConfiguration
    {
        private readonly ICassandraRepository _cassandraRepository;

        public TrumpiaKeywordConfiguration(ICassandraRepository cassandraRepository)
        {
            _cassandraRepository = cassandraRepository;
        }

        /// <summary>
        ///  To fetch the trumpia keyword configuration detail from the database
        /// </summary>
        /// <param name="trumpiaKeyword">trumpiaKeyword of the data to be fetched</param>
        /// <returns>TrumpiaKeywordConfigurationModel</returns>
        public TrumpiaKeywordConfigurationModel GetTrumpiaKeywordConfiguration(string trumpiaKeyword)
        {
            var trumpiaKeywordConfiguration =
                _cassandraRepository
                    .GetAsync<TrumpiaKeywordConfigurationEntity>(
                        Constants.CassandraTableNames.TrumpiaKeywordConfiguration,
                        t => t.TrumpiaKeyword == trumpiaKeyword).Result.FirstOrDefault();
            return
                Mapper.Map<TrumpiaKeywordConfigurationEntity, TrumpiaKeywordConfigurationModel>(
                    trumpiaKeywordConfiguration);
        }
    }
}