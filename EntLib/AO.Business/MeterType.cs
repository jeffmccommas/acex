﻿using System;
using System.Threading.Tasks;
using AO.BusinessContracts;
using AO.DataAccess.Contracts;
using AO.Entities;
using AutoMapper;
using CE.AO.Models;

namespace AO.Business
{
	/// <summary>
	/// Implementation for IMeterType
	/// </summary>
	public class MeterType : IMeterType
	{
		public LogModel LogModel { get; set; }

		private readonly ICassandraRepository _cassandraRepository;
		private readonly ICacheProvider _cacheProvider;
		
		public MeterType(ICassandraRepository cassandraRepository, ICacheProvider cacheProvider)
		{
			_cassandraRepository = cassandraRepository;
			_cacheProvider = cacheProvider;
		}

		/// <summary>
		/// To delete the MeterTypeModel from meter_type table
		/// </summary>
		/// <param name="model">MeterTypeModel which is to be deleted</param>
		/// <returns>True</returns>
		public async Task DeleteMeterTypeAsync(MeterTypeModel model)
		{
			await _cassandraRepository.DeleteAsync<MeterTypeEntity>(Constants.CassandraTableNames.MeterType,
			p => p.TableName == Constants.CassandraTableNames.MeterType && p.MeterTypeId== model.MeterTypeId).ConfigureAwait(false);

			_cacheProvider.Remove($"{Constants.CassandraTableNames.MeterType}_{model.MeterTypeId}",
				(int)Enums.CacheDatabases.MeterType);
		}

		/// <summary>
		/// Get MeterTypeModel information of the specific code from meter_type table
		/// </summary>
		/// <param name="meterTypeId"> Meter Type Id</param>
		/// <returns>MeterTypeModel</returns>
		public MeterTypeModel GetMeterType( int meterTypeId)
		{
			var meterDetails = _cacheProvider.Get<MeterTypeEntity>($"{Constants.CassandraTableNames.MeterType}_{meterTypeId}",
				  (int)Enums.CacheDatabases.MeterType);
			if (meterDetails == null)
			{
				meterDetails = _cassandraRepository.GetSingle<MeterTypeEntity>(Constants.CassandraTableNames.MeterType,
			   p => p.TableName == Constants.CassandraTableNames.MeterType && p.MeterTypeId == meterTypeId);

				if (meterDetails != null)
					_cacheProvider.Set($"{Constants.CassandraTableNames.MeterType}_{meterTypeId}", meterDetails,
						(int)Enums.CacheDatabases.MeterType);
			}
			return Mapper.Map<MeterTypeEntity, MeterTypeModel>(meterDetails);
		}

		/// <summary>
		/// To insert/update the MeterTypeModel to database
		/// </summary>
		/// <param name="model">MeterTypeModel which is to be inserted/updated</param>
		/// <returns>True</returns>
		public async Task InsertOrMergeMeterTypeAsync(MeterTypeModel model)
		{
			var entity = Mapper.Map<MeterTypeModel,MeterTypeEntity>(model);
			entity.TableName = Constants.CassandraTableNames.MeterType;
			await _cassandraRepository.InsertOrUpdateAsync(entity, Constants.CassandraTableNames.MeterType).ConfigureAwait(false);
			_cacheProvider.Set($"{Constants.CassandraTableNames.MeterType}_{model.MeterTypeId}", entity,
				(int)Enums.CacheDatabases.MeterType);
		}

	}
}
