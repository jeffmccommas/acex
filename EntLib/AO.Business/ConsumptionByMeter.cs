﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using AO.BusinessContracts;
using AO.DataAccess.Contracts;
using AO.Entities;
using AutoMapper;
using CE.AO.Models;
using CE.AO.Utilities;
using CE.RateModel;
using Enums = CE.RateModel.Enums;

namespace AO.Business
{
    /// <summary>
    /// TallAMI class is used to fetch/insert/update data from/to the database
    /// </summary>
    // ReSharper disable once InconsistentNaming
    public class ConsumptionByMeter : ITallAMI
    {
        public ConsumptionByMeter(LogModel logModel, Lazy<ICassandraRepository> cassandraRepository, IProcessingArgs processingArgs)
        {
            LogModel = logModel;
            CassandraRepository = cassandraRepository;
            ProcessingArgs = processingArgs;
        }

        public LogModel LogModel { get; set; }

        private Lazy<ICassandraRepository> CassandraRepository { get; }

        private IProcessingArgs ProcessingArgs { get; set; }

        /// <summary>
        /// To insert/update the TallAmiModel model to the database
        /// </summary>
        /// <param name="model">TallAmiModel model which is to be inserted/updated</param>
        /// <returns>True</returns>
        public async Task<bool> InsertOrMergeAmiAsync(TallAmiModel model)
        {
            var entityModel = Mapper.Map<TallAmiModel, ConsumptionByMeterEntity>(model);
            await CassandraRepository.Value.InsertOrUpdateAsync(entityModel, Constants.CassandraTableNames.ConsumptionByMeter).ConfigureAwait(false);
            return await Task.Factory.StartNew(() => true);
        }

        /// <summary>
        /// To insert/update the TallAmiModel model in a batch to the database
        /// </summary>
        /// <param name="tallAmiModels">List of TallAmiModel models which is to be inserted/updated</param>
        /// <returns>True</returns>
        public async Task<bool> InsertOrMergeBatchAsync(List<TallAmiModel> tallAmiModels)
        {
            var entities = Mapper.Map<IList<TallAmiModel>, IList<ConsumptionByMeterEntity>>(tallAmiModels);
            await CassandraRepository.Value.InsertOrUpdateBatchAsync(entities.ToList(), Constants.CassandraTableNames.ConsumptionByMeter).ConfigureAwait(false);
            return await Task.Factory.StartNew(() => true);
        }

        /// <summary>
        /// In azure table if we update a row with column having value in DB and null value in model, that row get updated in db with persisting the column value.That column will not get updated with null value.
        /// While in cassandra that column get updated with null value, its previous value get lost.
        /// In Our Case we are storing all tou values for single day in one row. From file we will get one tou value at a time, other values will be null at same time.
        /// Suppose we have tou_regular = 10, then other values will be null. We insert that in DB.Next time we have tou_shoulder1 = 15 for same meter & same day, at that time tou_regular will be null in model.
        /// If we update DB with this model, then in DB hte row will updated as tou_regular = null & tou_shoulder1 = 15.In this case our tou_regular value got lost.To avoid this we have written following function.
        /// </summary>
        /// <param name="tallAmiModels">List of TallAmiModel models with touvalues which is to be inserted/updated</param>
        /// <returns>True</returns>
        public async Task<bool> InsertOrMergeDailyAmiAsync(List<TallAmiModel> tallAmiModels)
        {
            foreach (var model in tallAmiModels)
            {
                string touProperyName;
                double touProperyValue;
                GetTouPropertyNameAndValue(model, out touProperyName, out touProperyValue);
                if (model.Direction is null) {  
                    model.Direction = 0;
                }

                // cjh: q&d try to avoid using null values in the 'upsert'   because it is killing us with tombstones.
                string setData = $" set  account_number = ?, service_point_id = ?, direction = ?, commodity_id = ?, uom_id = ?, time_zone = ?, consumption = ?, interval_type = ?, {touProperyName} = ? ";
                List<object> lvals = new List<object>();
                lvals.Add(model.AccountNumber);
                lvals.Add(model.ServicePointId);
                lvals.Add(model.Direction);
                lvals.Add(model.CommodityId);
                lvals.Add(model.UOMId);
                lvals.Add(model.Timezone);
                lvals.Add(model.Consumption);
                lvals.Add(model.IntervalType);
                lvals.Add(touProperyValue);

                if (model.VolumeFactor != null) {
                    lvals.Add(model.VolumeFactor);
                    setData += ", volume_factor = ?";
                }
                if (model.ProjectedReadDate != null) {
                    lvals.Add(model.ProjectedReadDate);
                    setData += ", projected_read_date = ?";
                }
                if (model.BatteryVoltage != null) {
                    lvals.Add(model.BatteryVoltage);
                    setData += ", battery_voltage = ?";
                }
                if (model.TransponderId != null) {
                    lvals.Add(model.TransponderId);
                    setData += ", transponder_id = ?";
                }
                if (model.TransponderPort != null) {
                    lvals.Add(model.TransponderPort);
                    setData += ", transponder_port = ?";
                }
                if (model.DialRead != null) {
                    lvals.Add(model.DialRead);
                    setData += ", dial_read = ?";
                }
                if (model.QualityCode != null) {
                    lvals.Add(model.QualityCode);
                    setData += ", quality_code = ?";
                }
                if (model.Scenario != null) {
                    lvals.Add(model.Scenario);
                    setData += ", scenario = ?";
                }
                if (model.ReadingType != null) {
                    lvals.Add(model.ReadingType);
                    setData += ", reading_type = ?";
                }
                if (model.CustomerId != null) {
                    lvals.Add(model.CustomerId);
                    setData += ", customer_id = ?";
                }
                setData +=
                    " where client_id = ? and meter_id = ? and ami_time_stamp = ?;";
                lvals.Add(model.ClientId);
                lvals.Add(model.MeterId);
                lvals.Add(model.AmiTimeStamp);

                var updateQuery = "Update " + Constants.CassandraTableNames.ConsumptionByMeter + setData;
                object[] values = lvals.ToArray();

#if useNullyProcessing
                var updateQuery = "Update " + Constants.CassandraTableNames.ConsumptionByMeter + " set  service_point_id = ?"
                                  + ", commodity_id = ?, uom_id = ?, volume_factor = ?, projected_read_date = ?, time_zone = ?, consumption = ?"
                                  + ", battery_voltage = ?, transponder_id = ?, transponder_port = ?, dial_read = ?, quality_code = ?, scenario = ?, reading_type = ?, " + touProperyName
                                  + " = ?, customer_id = ?, interval_type = ? where client_id = ? and meter_id = ? and ami_time_stamp = ? and account_number = ? and direction = ?;";

                object[] values = {
                        model.ServicePointId, model.CommodityId, model.UOMId, model.VolumeFactor,
                        model.ProjectedReadDate, model.Timezone, model.Consumption,
                        model.BatteryVoltage, model.TransponderId, model.TransponderPort, model.DialRead,
                        model.QualityCode, model.Scenario, model.ReadingType, touProperyValue, model.CustomerId,
                        model.IntervalType, model.ClientId, model.MeterId, model.AmiTimeStamp, model.AccountNumber, model.Direction
                    };
#endif
                CassandraRepository.Value.ExecuteUpdateQuery(updateQuery, values);
            }
            return await Task.Factory.StartNew(() => true);
        }

        /// <summary>
        /// To get TOU property name and value which will not having null as value
        /// </summary>
        /// <param name="tallAmiModel">tallAmiModel to get properties & their values</param>
        /// <param name="touProperyName">tou propertyname which will not having null as value</param>
        /// <param name="touProperyValue">tou propertyvalue which will not having null as value</param>
        private static void GetTouPropertyNameAndValue(TallAmiModel tallAmiModel, out string touProperyName, out double touProperyValue)
        {
            var properyName = string.Empty;
            double properyValue = 0;
            PropertyInfo[] properties = tallAmiModel.GetType().GetProperties();
            properties.Where(p => p.Name.Contains("TOU_")).ToList().ForEach(
                property =>
                {
                    if (property.GetValue(tallAmiModel) != null)
                    {
                        properyName = property.Name;
                        properyValue = Convert.ToDouble(property.GetValue(tallAmiModel));
                    }
                });
            touProperyName = properyName;
            touProperyValue = properyValue;
        }

        /// <summary>
        /// This method is used to fetch the Tall Ami details.
        /// The date input is expected as UTC.
        /// </summary>
        /// <param name="date">date of the data to be fetched</param>
        /// <param name="meterId">Meter Id of the data to be fetched</param>
        /// <param name="accountId">Account Id of the data to be fetched</param>
        /// <param name="clientId">Client Id of the data to be fetched</param>
        /// <returns>List of TallAmiModel</returns>
        public IEnumerable<TallAmiModel> GetAmi(DateTime date, string meterId, string accountId, int clientId)
        {
            var startDateTime = date;
            var endDateTime = date.AddDays(1);
            IList<ConsumptionByMeterEntity> amiList = CassandraRepository.Value.GetAsync<ConsumptionByMeterEntity>(Constants.CassandraTableNames.ConsumptionByMeter, 'q', a => a.ClientId == clientId && a.MeterId == meterId && a.AmiTimeStamp >= startDateTime && a.AmiTimeStamp < endDateTime).Result;
            IList<TallAmiModel> amiModelList = Mapper.Map<IList<ConsumptionByMeterEntity>, IList<TallAmiModel>>(amiList);
            return amiModelList.Count > 0 ? amiModelList.Where(a => a.AccountNumber == accountId).OrderByDescending(a => a.AmiTimeStamp) : null;
        }

        /// <summary>
        /// This method is used to fetch the Tall Ami details based on start date and end date.
        /// The startDate and endDate are expected as UTC.
        /// </summary>
        /// <param name="startDate">Start date of the data to be fetched</param>
        /// <param name="endDate">End date of the data to be fetched</param>
        /// <param name="meterId">Meter Id of the data to be fetched</param>
        /// <param name="clientId">Client Id of the data to be fetched</param>
        /// <returns>List of TallAmiModel</returns>
        public IEnumerable<TallAmiModel> GetAmiList(DateTime startDate, DateTime endDate, string meterId, int clientId)
        {
            IList<ConsumptionByMeterEntity> amiList = CassandraRepository.Value.Get<ConsumptionByMeterEntity>(Constants.CassandraTableNames.ConsumptionByMeter, 'q', a => a.ClientId == clientId && a.MeterId == meterId && a.AmiTimeStamp >= startDate && a.AmiTimeStamp < endDate);
            IList<TallAmiModel> amiModelList = Mapper.Map<IList<ConsumptionByMeterEntity>, IList<TallAmiModel>>(amiList);
            return amiModelList.Count > 0 ? amiModelList : null;
        }

        /// <summary>
        /// This method is used to fetch the Tall Ami details based on start date and end date meter id and account id.
        /// The startDate and endDate are expected as UTC.
        /// </summary>
        /// <param name="startDate">Start date of the data to be fetched</param>
        /// <param name="endDate">End date of the data to be fetched</param>
        /// <param name="meterId">Meter Id of the data to be fetched</param>
        /// <param name="clientId">Client Id of the data to be fetched</param>
        /// <param name="accountId">Client Id of the data to be fetched</param>
        /// <returns>List of TallAmiModel</returns>
        public IEnumerable<TallAmiModel> GetAmiList(DateTime startDate, DateTime endDate, string meterId, string accountId, int clientId)
        {
            var filterValues = new Dictionary<string, string>();
            filterValues.Add(Constants.CassandraTableNames.AmiFilterByAccountID, accountId);
            return GetAmiList(startDate, endDate, meterId, clientId, filterValues);
        }

        /// <summary>
        /// This method is used to fetch the Tall Ami details based on start date and end date.
        /// The startDate and endDate are expected as UTC.
        /// </summary>
        /// <param name="startDate">Start date of the data to be fetched</param>
        /// <param name="endDate">End date of the data to be fetched</param>
        /// <param name="meterId">Meter Id of the data to be fetched</param>
        /// <param name="accountId">Account Id of the data to be fetched</param>
        /// <param name="clientId">Client Id of the data to be fetched</param>
        /// <returns>List of TallAmiModel</returns>
        public IEnumerable<TallAmiModel> GetAmiListHeavy(DateTime startDate, DateTime endDate, string meterId, string accountId, int clientId)
        {
            IList<ConsumptionByMeterEntity> amiList = CassandraRepository.Value.Get<ConsumptionByMeterEntity>(Constants.CassandraTableNames.ConsumptionByMeter, 'q', a => a.ClientId == clientId && a.MeterId == meterId && a.AmiTimeStamp >= startDate && a.AmiTimeStamp < endDate);
            IList<TallAmiModel> amiModelList = Mapper.Map<IList<ConsumptionByMeterEntity>, IList<TallAmiModel>>(amiList);
            return amiModelList.Count > 0 ? amiModelList.Where(a => a.AccountNumber == accountId).OrderByDescending(a => a.AmiTimeStamp) : null;
        }


        /// <summary>
        /// This method is used to fetch the Tall Ami details based on start date and end date meter id and account id.
        /// The startDate and endDate are expected as UTC.
        /// </summary>
        /// <param name="startDate">Start date of the data to be fetched</param>
        /// <param name="endDate">End date of the data to be fetched</param>
        /// <param name="meterId">Meter Id of the data to be fetched</param>
        /// <param name="clientId">Client Id of the data to be fetched</param>
        /// <param name="accountId">Client Id of the data to be fetched</param>
        /// <param name="filterValues">Client Id of the data to be fetched</param>
        /// <returns>List of TallAmiModel</returns>
        public IEnumerable<TallAmiModel> GetAmiList(DateTime startDate, DateTime endDate, string meterId, int clientId, IDictionary<string, string> filterValues) {
            string tablename = ProcessingArgs.GetClientControlVal(clientId.ToString(),
                Constants.CassandraTableNames.AmiClientSpecificTablenames,
                Constants.CassandraTableNames.ConsumptionByMeter);
            string filterBy = ProcessingArgs.GetClientControlVal(clientId.ToString(),
                Constants.CassandraTableNames.AmiClientSpecificFilter,
                Constants.CassandraTableNames.AmiFilterByAccountID);

            IList<ConsumptionByMeterEntityLight> amiList = CassandraRepository.Value.Get<ConsumptionByMeterEntityLight>(tablename, 'q', a => a.ClientId == clientId && a.MeterId == meterId && a.AmiTimeStamp >= startDate && a.AmiTimeStamp < endDate);
            IList<TallAmiModel> amiModelList = Mapper.Map<IList<ConsumptionByMeterEntityLight>, IList<TallAmiModel>>(amiList);

            switch (filterBy)
            {
                case Constants.CassandraTableNames.AmiFilterByAccountID:
                    
                    return amiModelList.Count > 0 ? amiModelList.Where(a => a.AccountNumber == filterValues[Constants.CassandraTableNames.AmiFilterByAccountID]).OrderByDescending(a => a.AmiTimeStamp) : null;
                 
                case Constants.CassandraTableNames.AmiFilterByServicePointId:
                    return amiModelList.Count > 0 ? amiModelList.Where(a => a.AccountNumber == filterValues[Constants.CassandraTableNames.AmiFilterByServicePointId]).OrderByDescending(a => a.AmiTimeStamp) : null;
                    

            }
            return null;

        }
        

        /// <summary>
        /// To convert from ami list to Reading list
        /// Timestamp in the reading will be in local time
        /// </summary>
        /// <param name="amiList">List of model from which readings to be converted</param>
        /// <param name="noOfDays">Total number of days of which reading is returned</param>
        /// <param name="convertToLocalTimeZone">indicate if return data should be converted into local time zone</param>
        /// <returns>List of Reading</returns>
        public List<Reading> GetReadings(IEnumerable<TallAmiModel> amiList, out int noOfDays, bool convertToLocalTimeZone = true)
        {
            var readings = new List<Reading>();
            var amiModels = (amiList ?? new List<TallAmiModel>()) as IList<TallAmiModel> ?? amiList.ToList();
            if (!amiModels.Any())
            {
                noOfDays = 0;
                return readings;
            }

            List<TallAmiModel> tallAmiModels = amiModels.OrderByDescending(e => e.AmiTimeStamp).ToList();
            noOfDays = (int)(tallAmiModels.First().AmiTimeStamp.AddDays(1) - tallAmiModels.Last().AmiTimeStamp).TotalDays;
            
            // Convert UTC timestamp into local timestamp
            if(convertToLocalTimeZone)
                ConvertToLocalTime(tallAmiModels);

            foreach (var tallAmiModel in tallAmiModels)
            {
                Reading reading;

                //If interval type is 24 then get touid and reading using that touid
                if (tallAmiModel.IntervalType == 24)
                {
                    PropertyInfo[] propertyList = tallAmiModel.GetType().GetProperties();

                    foreach (var property in propertyList)
                    {
                        var timestamp = new DateTime(tallAmiModel.AmiTimeStamp.Year, tallAmiModel.AmiTimeStamp.Month, tallAmiModel.AmiTimeStamp.Day, tallAmiModel.AmiTimeStamp.Hour, tallAmiModel.AmiTimeStamp.Minute, 0);

                        if (!property.Name.Contains("TOU_")) continue;

                        //Presumably this scode is supposed to add a Reading object for every TOU period during the day.
                        var touId = Extensions.GetTouIdFromProperty(property);

                        if (property.GetValue(tallAmiModel) != null)
                        {
                            var quantity = Convert.ToDouble(property.GetValue(tallAmiModel));

                            reading = new Reading((Enums.TimeOfUse)touId, timestamp, quantity);

                            readings.Add(reading);
                        }
                    }
                }
                else
                {
                    reading = new Reading(tallAmiModel.AmiTimeStamp, Convert.ToDouble(tallAmiModel.Consumption));

                    readings.Add(reading);
                }
            }
            return readings;
        }

        /// <summary>
        /// To convert from ami list to ReadingModel
        /// Timestamp in reading will be in local time
        /// </summary>
        /// <param name="amiList">List of model from which readings to be converted</param>
        /// <returns>List of ReadingModel model</returns>
        public List<ReadingModel> GetReadings(IEnumerable<TallAmiModel> amiList)
        {
            var readings = new List<ReadingModel>();
            if (amiList == null)
            {
                return readings;
            }

            List<TallAmiModel> tallAmiModels = amiList.OrderByDescending(e => e.AmiTimeStamp).ToList();

            // Convert UTC timestamp into local timestamp
            ConvertToLocalTime(tallAmiModels);

            foreach (var tallAmiModel in tallAmiModels)
            {
                ReadingModel reading;

                //If interval type is 24 then get touid and reading using that touid
                if (tallAmiModel.IntervalType == 24)
                {
                    PropertyInfo[] propertyList = tallAmiModel.GetType().GetProperties();

                    foreach (var property in propertyList)
                    {
                        var timestamp = new DateTime(tallAmiModel.AmiTimeStamp.Year, tallAmiModel.AmiTimeStamp.Month, tallAmiModel.AmiTimeStamp.Day, tallAmiModel.AmiTimeStamp.Hour, tallAmiModel.AmiTimeStamp.Minute, 0);

                        if (!property.Name.Contains("TOU_")) continue;

                        //Presumably this scode is supposed to add a Reading object for every TOU period during the day.
                        var touId = Extensions.GetTouIdFromProperty(property);
                        if (property.GetValue(tallAmiModel) != null)
                        {
                            var quantity = Convert.ToDouble(property.GetValue(tallAmiModel));

                            reading = new ReadingModel((Enums.TimeOfUse)touId, timestamp, quantity);
                            reading.TimestampString = reading.Timestamp.ToString("o");
                            readings.Add(reading);
                        }
                    }
                }
                else
                {
                    reading = new ReadingModel(tallAmiModel.AmiTimeStamp, Convert.ToDouble(tallAmiModel.Consumption));
                    reading.TimestampString = reading.Timestamp.ToString("o");
                    readings.Add(reading);
                }
            }
            return readings;
        }

        public async Task<bool> DeleteReadingAsync(int clientId, string meterId)
        {
            await
                CassandraRepository.Value.DeleteAsync<ConsumptionByMeterEntity>(Constants.CassandraTableNames.ConsumptionByMeter,
                    a => a.ClientId == clientId && a.MeterId == meterId).ConfigureAwait(false);
            return await Task.Factory.StartNew(() => true);
        }

        private void ConvertToLocalTime(List<TallAmiModel> amiList)
        {
            if (amiList.Any())
            {
                var timezone = amiList[0].Timezone;

                var timezoneEnum = (CE.AO.Utilities.Enums.Timezone)Enum.Parse(typeof(CE.AO.Utilities.Enums.Timezone), timezone.ToUpper(CultureInfo.InvariantCulture));
                var timezoneId = TimeZoneInfo.FindSystemTimeZoneById(timezoneEnum.GetDescriptionOfEnum());
                foreach (var ami in amiList)
                {
                    ami.AmiTimeStamp = TimezoneHelper.ConvertToTimeZone(ami.AmiTimeStamp, TimeZoneInfo.Utc, timezoneId);
                }
            }
        }
    }
}