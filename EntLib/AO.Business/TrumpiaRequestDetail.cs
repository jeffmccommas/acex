﻿using System;
using System.Threading.Tasks;
using AO.BusinessContracts;
using AO.DataAccess.Contracts;
using AO.Entities;
using AutoMapper;
using CE.AO.Logging;
using CE.AO.Models;

namespace AO.Business
{
    /// <summary>
    /// TrumpiaRequestDetail class is used to insert/update the trumpia request details to the database
    /// </summary>
    public class TrumpiaRequestDetail : ITrumpiaRequestDetail
    {
        private readonly ICassandraRepository _cassandraRepository;
        public LogModel LogModel { get; set; }

        public TrumpiaRequestDetail(ICassandraRepository cassandraRepository, LogModel logModel)
        {
            _cassandraRepository = cassandraRepository;
            LogModel = logModel;
        }

        public async Task<bool> DeleteTrumpiaReqeustnAsync(string request)
        {
            await _cassandraRepository.DeleteAsync<TrumpiaRequestDetailEntity>(Constants.CassandraTableNames.TrumpiaRequestDetail, s => s.RequestId == request);
            return true;
        }

        /// <summary>
        /// To fetch the trumpia request detail from the database
        /// </summary>
        /// <param name="requestId">requestId of the data to be fetched</param>
        /// <returns>TrumpiaRequestDetailEntity</returns>
        public TrumpiaRequestDetailModel GetTrumpiaRequestDetail(string requestId)
        {
            var entity =
                _cassandraRepository.GetSingle<TrumpiaRequestDetailEntity>(
                    Constants.CassandraTableNames.TrumpiaRequestDetail, t => t.RequestId == requestId);
            return Mapper.Map<TrumpiaRequestDetailEntity, TrumpiaRequestDetailModel>(entity);
        }

        /// <summary>
        /// To fetch the trumpia request detail from the database
        /// </summary>
        /// <param name="requestId">requestId of the data to be fetched</param>
        /// <returns>TrumpiaRequestDetailEntity</returns>
        public async Task<TrumpiaRequestDetailModel> GetTrumpiaRequestDetailAsync(string requestId)
        {
            var entity = await
                    _cassandraRepository.GetSingleAsync<TrumpiaRequestDetailEntity>(
                        Constants.CassandraTableNames.TrumpiaRequestDetail, t => t.RequestId == requestId);
            return Mapper.Map<TrumpiaRequestDetailEntity, TrumpiaRequestDetailModel>(entity);
        }

        /// <summary>
        /// To insert or update TrumpiaRequestDetailEntity to database
        /// </summary>
        /// <param name="model">entity to be inserted</param>
        /// <returns>bool</returns>
        public bool InsertOrMergeTrumpiaRequestDetail(TrumpiaRequestDetailModel model)
        {
            try { 
            // report copy
            var entityReport = Mapper.Map<TrumpiaRequestDetailModel, TrumpiaRequestDetailReportEntity>(model);
            entityReport.LastModifiedDate = DateTime.UtcNow;
            entityReport.RowCreateDate = DateTime.UtcNow.ToString("yyyyMMdd");
            _cassandraRepository.InsertOrUpdate(entityReport, Constants.CassandraTableNames.TrumpiaRequestDetailReport);
            }
            catch (Exception ex)
            {
                Logger.Error(ex, LogModel);
            }
            var entity = Mapper.Map<TrumpiaRequestDetailModel, TrumpiaRequestDetailEntity>(model);
            entity.LastModifiedDate = DateTime.UtcNow;
            return _cassandraRepository.InsertOrUpdate(entity, Constants.CassandraTableNames.TrumpiaRequestDetail);
            
        }

        /// <summary>
        /// To insert or update TrumpiaRequestDetailEntity to database
        /// </summary>
        /// <param name="model">entity to be inserted</param>
        /// <returns>bool</returns>
        public async Task<bool> InsertOrMergeTrumpiaRequestDetailAsync(TrumpiaRequestDetailModel model)
        {
            var entity = Mapper.Map<TrumpiaRequestDetailModel, TrumpiaRequestDetailEntity>(model);
            entity.LastModifiedDate = DateTime.UtcNow;
            await _cassandraRepository.InsertOrUpdateAsync(entity, Constants.CassandraTableNames.TrumpiaRequestDetail).ConfigureAwait(false);

            try { 
                // report copy
            var entityReport = Mapper.Map<TrumpiaRequestDetailModel, TrumpiaRequestDetailReportEntity>(model);
                entityReport.LastModifiedDate = DateTime.UtcNow;
                entityReport.RowCreateDate = DateTime.UtcNow.ToString("yyyyMMdd");
                await _cassandraRepository.InsertOrUpdateAsync(entityReport, Constants.CassandraTableNames.TrumpiaRequestDetailReport).ConfigureAwait(false);
            }
            catch (Exception ex)
            {
                Logger.Error(ex, LogModel);
            }
            return await Task.Factory.StartNew(() => true);  // This useless boolean has to linger because CE.AO.Business also uses ITrumpiaRequestDetail.
        }
    }
}
