﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AO.BusinessContracts;
using AO.DataAccess.Contracts;
using AO.Entities;
using AutoMapper;
using CE.AO.Models;

namespace AO.Business
{
    /// <summary>
    /// AccountLookup class is used to fetch/insert/update the client meter records from/to the database
    /// </summary>
    public class AccountLookup : IAccountLookup
    {
        public AccountLookup(LogModel logModel, Lazy<ICassandraRepository> cassandraRepository)
        {
            LogModel = logModel;
            CassandraRepository = cassandraRepository;
        }

        public LogModel LogModel { get; set; }

        private Lazy<ICassandraRepository> CassandraRepository { get; }

        /// <summary>
        ///To fetch the active account meter details from the database
        /// </summary>
        /// <param name="clientId">clientId of the data to be fetched</param>
        /// <param name="meterId">meterId of the data to be fetched</param>
        /// <returns>AccountLookupModel</returns>
        public List<AccountLookupModel> GetAccountMeterDetails(int clientId, string meterId)
        {
            var accountMeterDetails =
                CassandraRepository.Value.Get<ClientMeterEntity>(Constants.CassandraTableNames.ClientMeter,
                    a => a.ClientId == clientId && a.MeterId == meterId).Where( a => a.IsActive != false);

            var distinctAccountsMeterDetails = accountMeterDetails.Select(l => new ClientMeterEntity
            {
                CustomerId = l.CustomerId,
                AccountId = l.AccountId,
                MeterId = l.MeterId,
                IsActive = l.IsActive,
                ServiceContractId = l.ServiceContractId,
                ClientId = l.ClientId,
                IsServiceContractCreatedBySystem = l.IsServiceContractCreatedBySystem
            }).GroupBy(e => new { e.AccountId, e.MeterId, e.CustomerId })
                .Select(z => z.OrderBy(i => i.AccountId).First())
                .ToList();

            return Mapper.Map<List<ClientMeterEntity>, List<AccountLookupModel>>(distinctAccountsMeterDetails);
        }

        /// <summary>
        /// To fetch the account meter details for customer from the database
        /// </summary>
        /// <param name="clientId">clientId of the data to be fetched</param>
        /// <param name="meterId">meterId of the data to be fetched</param>
        /// <param name="customerId">customerId of the data to be fetched</param>
        /// <returns>AccountLookupModel</returns>
        public async Task<AccountLookupModel> GetAccountMeterDetails(int clientId, string meterId, string customerId)
        {
            var entity = await
                CassandraRepository.Value.GetSingleAsync<ClientMeterEntity>(Constants.CassandraTableNames.ClientMeter,
                    a => a.ClientId == clientId & a.MeterId == meterId && a.CustomerId == customerId);
            return Mapper.Map<ClientMeterEntity, AccountLookupModel>(entity);
        }

        /// <summary>
        /// To insert or update AccountLookupModel to database
        /// </summary>
        /// <param name="model">AccountLookupModel to be inserted</param>
        /// <returns>true</returns>
        public async Task<bool> InsertOrMergeAccountLookupAsync(AccountLookupModel model)
        {
            var entity = Mapper.Map<AccountLookupModel, ClientMeterEntity>(model);
            await CassandraRepository.Value.InsertOrUpdateAsync(entity, Constants.CassandraTableNames.ClientMeter).ConfigureAwait(false);
            return await Task.Factory.StartNew(() => true);
        }

        /// <summary>
        /// Delete Client Meter Details Form client Meter Table
        /// </summary>
        /// <param name="clientId"></param>
        /// <param name="meterid"></param>
        /// <param name="customerid"></param>
        /// <returns></returns>
        public async Task<bool> DeleteHistoryAccountLookupAsyc(int clientId, string meterid, string customerid)
        {
            await
                CassandraRepository.Value.DeleteAsync<ClientMeterEntity>(Constants.CassandraTableNames.ClientMeter,
                    a => a.ClientId == clientId && a.MeterId == meterid && a.CustomerId == customerid).ConfigureAwait(false);
            return await Task.Factory.StartNew(() => true);
        }
    }
}
