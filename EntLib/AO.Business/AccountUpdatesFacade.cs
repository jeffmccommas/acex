﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AO.BusinessContracts;
using AO.Entities;
using AutoMapper;
using CE.AO.Logging;
using CE.AO.Models;
using Enums = CE.AO.Utilities.Enums;

namespace AO.Business
{
	/// <summary>
	/// AccountUpdatesFacade class is used to insert/update the AccountUpdatesModel model to the database
	/// </summary>
	public class AccountUpdatesFacade : IAccountUpdatesFacade
	{
		private readonly Lazy<IAccountUpdates> _accountUpdatesManager;
		private readonly Lazy<IBilling> _billingManager;
		private readonly Lazy<ICustomer> _customerManager;
		private readonly Lazy<ICustomerMobileNumberLookup> _customerMobileNumberLookupManager;
		private readonly Lazy<IAccountLookup> _accountLookupManager;

		public AccountUpdatesFacade(LogModel logModel, Lazy<IAccountUpdates> accountUpdateManager,
			Lazy<IBilling> billingManager, Lazy<ICustomer> customerManager,
			Lazy<ICustomerMobileNumberLookup> customerMobileLookupMananger, Lazy<IAccountLookup> accountLookupManager)
		{
			LogModel = logModel;
			_accountUpdatesManager = accountUpdateManager;
			_billingManager = billingManager;
			_customerManager = customerManager;
			_customerMobileNumberLookupManager = customerMobileLookupMananger;
			_accountLookupManager = accountLookupManager;
		}

		public LogModel LogModel { get; set; }

	    public async Task<bool> InsertOrMergeAccountUpdateAsync(int clientId, string customerId, string accountId, string premiseId, string serviceContractId, string meterId, string newValue, string replacedValue, string accountUpdateType)
	    {
	        var accountUpdatesModel = new AccountUpdatesModel
	        {
	            ClientId =  clientId,
                CustomerId = customerId,
                AccountId = accountId,
                ServiceContractId = serviceContractId,
                MeterId = meterId,
                PremiseId =  premiseId,
                NewValue = newValue,
                OldValue = replacedValue,
                UpdateType = accountUpdateType,
                UpdateSentDate = DateTime.UtcNow
	        };

	        return await InsertOrMergeAccountUpdateAsync(accountUpdatesModel);
	    }

        /// <summary>
        /// To insert/update AccountUpdatesModel model to the database 
        /// </summary>
        /// <param name="model">AccountUpdatesModel model to be inserted/updated</param>
        /// <returns>Insertion/Updation is successful or not</returns>
        public async Task<bool> InsertOrMergeAccountUpdateAsync(AccountUpdatesModel model)
		{
			var accountUpdatesEntity = Mapper.Map<AccountUpdatesModel, AccountUpdatesEntity>(model);

			var tasks = new List<Task<bool>>();
			var accountUpdateTypeEnum = (Enums.AccountUpdateType)Enum.Parse(typeof(Enums.AccountUpdateType), Convert.ToString(accountUpdatesEntity.UpdateType));
			
			await _accountUpdatesManager.Value.DeleteHistoryAccountUpdateAsyc(accountUpdatesEntity.ClientId,
				accountUpdatesEntity.CustomerId, accountUpdatesEntity.UpdateType, accountUpdatesEntity.ServiceContractId, true);

			tasks.Add(_accountUpdatesManager.Value.InsertOrMergeAccountUpdateAsync(model, true));
			tasks.Add(_accountUpdatesManager.Value.InsertOrMergeAccountUpdateAsync(model, false));

			switch (accountUpdateTypeEnum)
			{
				case Enums.AccountUpdateType.RateClass:
					tasks.Add(InsertorMergeForRateClassAsync(accountUpdatesEntity));
					break;
				case Enums.AccountUpdateType.Email:
					tasks.Add(InsertorMergeForEmailClassAsync(accountUpdatesEntity));
					break;
				case Enums.AccountUpdateType.Phone1:
					tasks.Add(InsertorMergeForPhone1ClassAsync(accountUpdatesEntity));
					break;
				case Enums.AccountUpdateType.MeterReplacement:
					tasks.Add(InsertorMergeForMeterReplacementClassAsync(accountUpdatesEntity));
					break;
				case Enums.AccountUpdateType.ReadStartDate:
				case Enums.AccountUpdateType.ReadEndDate:
					//Do Nothing
					break;
			}

			bool[] taskResults = await Task.WhenAll(tasks);
			return !taskResults.Any(result => false);
		}

		/// <summary>
		/// To update the BillingModel as per the AccountUpdatesEntity
		/// </summary>
		/// <param name="accountUpdatesEntityModel">AccountUpdatesEntity to update some properties of BillingModel</param>
		/// <returns>Updation is successful or not</returns>
		private async Task<bool> InsertorMergeForRateClassAsync(AccountUpdatesEntity accountUpdatesEntityModel)
		{
			var isSuccess = false;
			var billingModel = await _billingManager.Value.GetServiceDetailsFromBillAsync(accountUpdatesEntityModel.ClientId,
				accountUpdatesEntityModel.CustomerId, accountUpdatesEntityModel.AccountId,
				accountUpdatesEntityModel.ServiceContractId);

			if (billingModel == null)
			{
				Logger.Fatal($@"ServiceContractId ""{accountUpdatesEntityModel.ServiceContractId}"" not found while updating {accountUpdatesEntityModel.UpdateType}.", LogModel);
			}
			else
			{
				billingModel.RateClass = accountUpdatesEntityModel.NewValue;
				billingModel.IsFault = true;
				isSuccess = await _billingManager.Value.InsertOrMergeLatestBillAsync(billingModel);
			}
			return isSuccess;
		}

		/// <summary>
		/// To update the Customer model as per the AccountUpdatesEntity
		/// </summary>
		/// <param name="accountUpdatesEntityModel">AccountUpdatesEntity to update some properties of CustomerModel</param>
		/// <returns>Updation is successful or not</returns>
		private async Task<bool> InsertorMergeForEmailClassAsync(AccountUpdatesEntity accountUpdatesEntityModel)
		{
			var isSuccess = false;
			var customerModel = await _customerManager.Value.GetCustomerAsync(accountUpdatesEntityModel.ClientId, accountUpdatesEntityModel.CustomerId);

			if (customerModel == null)
			{
				Logger.Fatal($@"Customer ""{accountUpdatesEntityModel.CustomerId}"" not found while updating {accountUpdatesEntityModel.UpdateType}.", LogModel);
			}
			else
			{
				customerModel.EmailAddress = accountUpdatesEntityModel.NewValue;
				isSuccess = await _customerManager.Value.InsertOrMergeCustomerAsync(customerModel);
			}
			return isSuccess;
		}

		/// <summary>
		/// To update the Customer model as per the AccountUpdatesEntity
		/// </summary>
		/// <param name="accountUpdatesEntityModel">AccountUpdatesEntity to update some properties of CustomerModel, </param>
		/// <returns>Updation is successful or not</returns>
		private async Task<bool> InsertorMergeForPhone1ClassAsync(AccountUpdatesEntity accountUpdatesEntityModel)
		{
			var isSuccess = false;
			int clientId = accountUpdatesEntityModel.ClientId;
			string accountId = accountUpdatesEntityModel.AccountId;
			string oldMobileNumber = accountUpdatesEntityModel.OldValue;

			var customerModel = await _customerManager.Value.GetCustomerAsync(clientId, accountUpdatesEntityModel.CustomerId);

			if (customerModel == null)
			{
				Logger.Fatal($@"Customer ""{accountUpdatesEntityModel.CustomerId}"" not found while updating {accountUpdatesEntityModel.UpdateType}.", LogModel);
			}
			else
			{
				customerModel.Phone1 = accountUpdatesEntityModel.NewValue;
				isSuccess = await _customerManager.Value.InsertOrMergeCustomerAsync(customerModel);
			}

			var customerMobileNumberLookupModel =
				_customerMobileNumberLookupManager.Value.GetCustomerMobileNumberLookup(clientId, oldMobileNumber, accountId);

			if (customerMobileNumberLookupModel == null)
			{
				Logger.Fatal($@"Phone number ""{accountUpdatesEntityModel.OldValue}"" not found while updating {accountUpdatesEntityModel.UpdateType}.", LogModel);
			}
			else
			{
				await _customerMobileNumberLookupManager.Value.DeleteCustomerMobileNumberLookupAsync(clientId, oldMobileNumber, accountId);

				customerMobileNumberLookupModel.MobileNumber = accountUpdatesEntityModel.NewValue;
				isSuccess = isSuccess && await _customerMobileNumberLookupManager.Value.InsertOrMergeCustomerMobileNumberLookupAsync(customerMobileNumberLookupModel);

			}
			return isSuccess;
		}

		/// <summary>
		/// To update the Billing, AccountLookup model as per the AccountUpdatesEntity
		/// </summary>
		/// <param name="accountUpdatesEntityModel">AccountUpdatesEntity to update some properties of Billing, AccountLookup model</param>
		/// <returns>Updation is successful or not</returns>
		private async Task<bool> InsertorMergeForMeterReplacementClassAsync(AccountUpdatesEntity accountUpdatesEntityModel)
		{
			var isSuccess = false;
			var billingEntity = await _billingManager.Value.GetServiceDetailsFromBillAsync(accountUpdatesEntityModel.ClientId,
				accountUpdatesEntityModel.CustomerId, accountUpdatesEntityModel.AccountId,
				accountUpdatesEntityModel.ServiceContractId);

			if (billingEntity == null)
			{
				Logger.Fatal($@"ServiceContractId ""{accountUpdatesEntityModel.ServiceContractId}"" not found while updating {accountUpdatesEntityModel.UpdateType}.", LogModel);
			}
			else
			{
				billingEntity.MeterId = accountUpdatesEntityModel.NewValue;
				billingEntity.ReplacedMeterId = accountUpdatesEntityModel.OldValue;
				billingEntity.IsFault = true;
				isSuccess = await _billingManager.Value.InsertOrMergeLatestBillAsync(billingEntity);

				var accountLookupEntity = await _accountLookupManager.Value.GetAccountMeterDetails(billingEntity.ClientId,
					billingEntity.ReplacedMeterId, billingEntity.CustomerId);
				if (accountLookupEntity != null)
				{
					accountLookupEntity.IsActive = false;
					await _accountLookupManager.Value.InsertOrMergeAccountLookupAsync(accountLookupEntity);

					accountLookupEntity.IsActive = true;
					accountLookupEntity.MeterId = accountUpdatesEntityModel.NewValue;
					if (accountLookupEntity.IsServiceContractCreatedBySystem)
						accountLookupEntity.ServiceContractId = BillingEntity.GetServiceContractId(accountUpdatesEntityModel.MeterId,
							accountUpdatesEntityModel.AccountId, accountUpdatesEntityModel.PremiseId, billingEntity.CommodityId);
					else
						accountLookupEntity.ServiceContractId = billingEntity.ServiceContractId;

					isSuccess = await _accountLookupManager.Value.InsertOrMergeAccountLookupAsync(accountLookupEntity);
					return isSuccess;
				}
			}
			return isSuccess;
		}
	}
}
