﻿using System;
using System.Collections.Generic;
using System.Linq;
using AO.BusinessContracts;
using CE.AO.Logging;
using CE.AO.Models;
using Microsoft.Azure;
using Enums = AO.BusinessContracts.Enums;

namespace AO.Business
{
    /// <summary>
    /// Class to calculate timing of operations performed
    /// </summary>
    public class TimingMetricsCalculator : ITimingMetricsCalculator
    {
        /// <summary>
        /// Start calculating time to perform operation
        /// </summary>
        public void Start(int clientId, Enums.Metrics metric)
        {
            var model = GetClientMetricModel(clientId, metric);
            model.StopWatch.Reset();
            model.StopWatch.Start();
        }

        /// <summary>
        /// Stop calculating time to perform operation. Call this when operation is performed.
        /// </summary>
        public void Stop(int clientId, Enums.Metrics metric, LogModel logModel)
        {
            var model = GetClientMetricModel(clientId, metric);
            model.StopWatch.Stop();
            if (model.StopWatch.Elapsed.Ticks > 0)
            {
                if (model.LogEachOperation)
                    Logger.Info($@"Time (in miliseconds) taken to perform {metric}: {model.StopWatch.Elapsed.TotalMilliseconds} ", logModel);

                model.NumberOfOperationsPerformed += 1;
                model.TimeElapsed = model.TimeElapsed.Add(model.StopWatch.Elapsed);
                if (model.LogAfterNumberOfOperations == model.NumberOfOperationsPerformed)
                {
                    var averageMiliseconds = model.TimeElapsed.TotalMilliseconds / model.LogAfterNumberOfOperations;
                    Logger.Info($@"Average Time (in miliseconds) taken to perform {metric}: {averageMiliseconds} ", logModel);
                }
            }
            else
            {
                throw new Exception("Start method should be called before Stop method.");
            }
        }

        /// <summary>
        /// Dictionary of client's metrics models
        /// </summary>
        private static readonly Dictionary<int, List<MetricsModel>> ClientMetricModels = new Dictionary<int, List<MetricsModel>>();

        /// <summary>
        /// Gets metrics model from static dictionary depending on the client id and metric
        /// </summary>
        private MetricsModel GetClientMetricModel(int clientId, Enums.Metrics metric)
        {
            List<MetricsModel> metricModels;
            var foundInDictionary = ClientMetricModels.TryGetValue(clientId, out metricModels);
            if (!foundInDictionary)
            {
                metricModels = new List<MetricsModel>();
                ClientMetricModels.Add(clientId, metricModels);
            }

            var model = metricModels.SingleOrDefault(m => m.Name == metric.ToString());
            if (model == null)
            {
                model = new MetricsModel();

                var logAfterNumberOfCalculations = CloudConfigurationManager.GetSetting("LogAfterNumberOfCalculations");
                if (!string.IsNullOrWhiteSpace(logAfterNumberOfCalculations))
                    model.LogAfterNumberOfOperations = Convert.ToInt32(logAfterNumberOfCalculations);

                var logEachCalculation = CloudConfigurationManager.GetSetting("LogEachCalculation");
                if (!string.IsNullOrWhiteSpace(logEachCalculation))
                    model.LogEachOperation = Convert.ToBoolean(logEachCalculation);

                model.Name = metric.ToString();
                metricModels.Add(model);
            }

            return model;
        }
    }
}