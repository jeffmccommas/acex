﻿using System.Threading.Tasks;
using AO.BusinessContracts;
using AO.DataAccess.Contracts;
using AO.Entities;
using AutoMapper;
using CE.AO.Models;

namespace AO.Business
{
	public class DcuCallData : IDcuCallData
	{
		private readonly ICassandraRepository _cassandraRepository;

		public DcuCallData(ICassandraRepository cassandraRepository)
		{
			_cassandraRepository = cassandraRepository;
		}

		/// <summary>
		/// To insert/update the DcuCallDataModel in dcu_calldata_history table.
		/// </summary>
		/// <param name="model">DcuCallDataModel which is to be inserted/updated</param>
		/// <returns>True</returns>
		public async Task InsertOrMergeDcuCallDataAsync(DcuCallDataModel model)
		{
			var entity = Mapper.Map<DcuCallDataModel, DcuCallDataHistoryEntity>(model);
			await _cassandraRepository.InsertOrUpdateAsync(entity, Constants.CassandraTableNames.DcuCallDataHistory).ConfigureAwait(false);
		}

		/// <summary>
		/// To delete the DcuCallDataModel from dcu_calldata_history table.
		/// </summary>
		/// <param name="model">DcuCallDataModel which is to be deleted</param>
		/// <returns>True</returns>
		public async Task DeleteDcuCallDataAsync(DcuCallDataModel model)
		{
			await
				_cassandraRepository.DeleteAsync<DcuCallDataHistoryEntity>(
					Constants.CassandraTableNames.DcuCallDataHistory,
					p =>
						p.ClientId == model.ClientId && p.CallStart == model.CallStart && p.DcuId == model.DcuId &&
						p.CallType == model.CallType).ConfigureAwait(false);
		}
	}
}
