﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using AO.BusinessContracts;
using AO.Entities;
using CE.AO.Logging;
using CE.AO.Models;
using CsvHelper;
using Microsoft.Practices.Unity;
using Microsoft.WindowsAzure.Storage;

namespace AO.Business
{
    /// <summary>
    /// Report class is used to export the reports
    /// </summary>
    public class Report : IReport
    {
        public Report(LogModel logModel)
        {
            LogModel = logModel;
        }

        public LogModel LogModel { get; set; }

        [Dependency]
        public Lazy<IClientAccount> ClientAccountManager { get; set; }

        [Dependency]
        public Lazy<IEvaluation> EvaluationManager { get; set; }

        [Dependency]
        public Lazy<INotification> NotificationManager { get; set; }

        [Dependency]
        public Lazy<IClientConfigFacade> ClientConfigFacadeManager { get; set; }

        /// <summary>
        /// Export the insight report to the blob
        /// </summary>
        /// <param name="clientId">Client id for which report is to be exported</param>
        /// <param name="asOfDate">Date for which report is to be exported</param>
        /// <returns></returns>
        public Task<bool> ExportInsights(int clientId, DateTime asOfDate)
        {
            var clientSettings = ClientConfigFacadeManager.Value.GetClientSettings(clientId);
            if (clientSettings == null)
            {
                Logger.Warn("No settings found for export insights.", LogModel);
                return Task.FromResult(false);
            }
            byte[] token = null;
            do
            {
                IEnumerable<ClientAccountModel> clientAcoountsList = ClientAccountManager.Value.GetClientAccounts(clientId, 1000, ref token);
                foreach (var item in clientAcoountsList)
                {
                    List<InsightModel> insightModelList = EvaluationManager.Value.GetDayInsights(clientId, item.CustomerId, item.AccountId, asOfDate).ToList();
                    List<string> programList = insightModelList.Select(p => p.ProgramName).Distinct().ToList();

                    foreach (var program in programList)
                    {
                        var clientSettingProgram =
                            clientSettings.Programs.FirstOrDefault(p => p.UtilityProgramName == program);
                        if (clientSettingProgram != null && clientSettingProgram.FileExportRequired)
                        {
                            var filename = ExportToFile(Constants.CassandraTableNames.Insight, clientId, asOfDate, program);
                            List<InsightModel> programInsigntModelList = insightModelList.Where(p => p.ProgramName == program).ToList();
                            if (programInsigntModelList.Any())
                                CreateUploadCsvFile(clientId, filename, programInsigntModelList);
                        }
                    }
                }
            } while (token != null);
            return Task.FromResult(true);
        }

        /// <summary>
        /// Export the notifications report to the blob
        /// </summary>
        /// <param name="clientId">Client id for which report is to be exported</param>
        /// <param name="asOfDate">Date for which report is to be exported</param>
        /// <returns></returns>
        public Task<bool> ExportNotifications(int clientId, DateTime asOfDate)
        {
            var clientSettings = ClientConfigFacadeManager.Value.GetClientSettings(clientId);
            if (clientSettings == null)
            {
                Logger.Warn("No settings found for export insights.", LogModel);
                return Task.FromResult(false);
            }

            byte[] token = null;
            do
            {
                IEnumerable<ClientAccountModel> clientAcoountsList = ClientAccountManager.Value.GetClientAccounts(clientId, 1000, ref token);

                foreach (var item in clientAcoountsList)
                {
                    IEnumerable<NotificationModel> notificationModelList =
                        NotificationManager.Value.GetDayNotifications(clientId, item.CustomerId, item.AccountId,
                            asOfDate);
                    List<NotificationModel> notificationList = (from n in notificationModelList
                                                                join p in clientSettings.Programs on n.ProgramName equals p.UtilityProgramName
                                                                where p.FileExportRequired
                                                                select n).OrderBy(n => n.ProgramName).ToList();

                    var filename = ExportToFile(Constants.CassandraTableNames.Notification, clientId, asOfDate, string.Empty);
                    if (notificationList.Any())
                        CreateUploadCsvFile(clientId, filename, notificationList);
                }
            } while (token != null);
            return Task.FromResult(true);
        }

        /// <summary>
        /// File name of the exported report
        /// </summary>
        /// <param name="exportType">Export type</param>
        /// <param name="clientId">Client Id</param>
        /// <param name="date">Date</param>
        /// <param name="programName">Program Name</param>
        /// <returns>Name of file to which report is exported</returns>
        private string ExportToFile(string exportType, int clientId, DateTime date, string programName)
        {
            var filename = "";
            switch (exportType.ToLower(CultureInfo.InvariantCulture))
            {
                case "insight":
                    filename = clientId + "_Insight_" + programName + date.ToString("_yyyy-MM-dd") + ".csv";
                    break;
                case "notification":
                    filename = clientId + "_Notification_" + date.ToString("yyyy-MM-dd") + ".csv";
                    break;
            }
            return filename;
        }

        /// <summary>
        /// Create and upload file to the blob
        /// </summary>
        /// <typeparam name="T">Model for which file is uploaded</typeparam>
        /// <param name="clientId">Client id</param>
        /// <param name="csvNameWithExt">csv file name</param>
        /// <param name="exportList">List of model</param>
        public static void CreateUploadCsvFile<T>(int clientId, string csvNameWithExt, IEnumerable<T> exportList)
            where T : class
        {
            var storageAccount =
                CloudStorageAccount.Parse(
                    System.Configuration.ConfigurationManager.AppSettings["AzureStorageConnectionString"]);
            var blobClient = storageAccount.CreateCloudBlobClient();
            var containerName = $"client{clientId}";

            var blobContainer = blobClient.GetContainerReference(containerName);
            blobContainer.CreateIfNotExists();

            var blob = blobContainer.GetAppendBlobReference($"Reports/{csvNameWithExt}");

            if (!blob.Exists())
            {
                blob.CreateOrReplace();
                blob.Properties.ContentType = "application/octet-stream";
                blob.SetProperties();
            }

            var streamWriter = new StreamWriter(blob.OpenWrite(false));
            using (var csvWriter = new CsvWriter(streamWriter))
            {
                csvWriter.WriteRecords(exportList);
            }
        }
    }
}
