﻿using System.Threading.Tasks;
using AO.BusinessContracts;
using AO.DataAccess.Contracts;
using AO.Entities;
using AutoMapper;
using CE.AO.Models;

namespace AO.Business
{
    /// <summary>
    /// MeterAccountMtu class is used to insert/update the MeterAccountMtuModel to the database
    /// </summary>
    public class DcuProvisioning : IDcuProvisioning
    {
        private readonly ICassandraRepository _cassandraRepository;

        public DcuProvisioning(ICassandraRepository cassandraRepository)
        {
            _cassandraRepository = cassandraRepository;
        }

        /// <summary>
        /// To insert/update the DcuProvisioningModel to the database
        /// </summary>
        /// <param name="model">DcuProvisioningModel which is to be inserted/updated</param>
        /// <returns>True</returns>
        public async Task InsertOrMergeDcuProvisioningAsync(DcuProvisioningModel model)
        {
            var entity = Mapper.Map<DcuProvisioningModel, DcuProvisioningEntity>(model);
            await _cassandraRepository.InsertOrUpdateAsync(entity, Constants.CassandraTableNames.DcuProvisioning).ConfigureAwait(false);
        }

        /// <summary>
        /// To delete the DcuProvisioningModel from the database
        /// </summary>
        /// <param name="model">DcuProvisioningModel which is to be deleted</param>
        /// <returns>True</returns>
        public async Task DeleteDcuProvisioningAsync(DcuProvisioningModel model)
        {
            var entity = Mapper.Map<DcuProvisioningModel, DcuProvisioningEntity>(model);
            await
                _cassandraRepository.DeleteAsync<DcuProvisioningEntity>(
                    Constants.CassandraTableNames.DcuProvisioning,
                    p =>
                        p.ClientId == entity.ClientId && p.DcuId == entity.DcuId && p.InstalledDate == entity.InstalledDate).ConfigureAwait(false);
        }
    }
}