﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AO.BusinessContracts;
using AO.Entities;
using AutoMapper;
using CE.AO.Models;
using CE.AO.Utilities;
using Enums = CE.AO.Utilities.Enums;

namespace AO.Business
{
    /// <summary>
    /// BillingFacade class is used to insert/update the billing details to the database
    /// </summary>
    public class BillingFacade : IBillingFacade
    {
        private readonly ICustomer _customerManager;
        private readonly IClientAccount _clientAccountManager;
        private readonly IBilling _billingManager;
        private readonly IPremise _premiseManager;
        private readonly ICustomerMobileNumberLookup _customerMobileNumberLookup;
        private readonly IAccountLookup _accountManager;
        private readonly IAccountUpdatesFacade _accountUpdatesManager;
        private readonly IClientConfigFacade _clientConfigFacadeManager;
        private readonly IBillingCycleSchedule _billingCycleScheduleManager;

        public BillingFacade(ICustomer customerManager, IClientAccount clientAccountManager, IBilling billingManager, IPremise premiseManager, ICustomerMobileNumberLookup customerMobileNumberLookup, IAccountLookup accountManager, IAccountUpdatesFacade accountUpdateManager, IClientConfigFacade clientConfigFacadeManager, IBillingCycleSchedule billingCycleScheduleManager)
        {
            _customerManager = customerManager;
            _clientAccountManager = clientAccountManager;
            _billingManager = billingManager;
            _premiseManager = premiseManager;
            _customerMobileNumberLookup = customerMobileNumberLookup;
            _accountManager = accountManager;
            _accountUpdatesManager = accountUpdateManager;
            _clientConfigFacadeManager = clientConfigFacadeManager;
            _billingCycleScheduleManager = billingCycleScheduleManager;
        }

        /// <summary>
        /// To insert or update BillingCustomerPremiseModel to database
        /// </summary>
        /// <param name="model">model to be inserted</param>
        /// <returns>true</returns>
        public async Task<bool> InsertOrMergeBillingAsync(BillingCustomerPremiseModel model) {
            //Debug.WriteLine($"[[B_iomba_{model.customer_id}]]")
            if (string.IsNullOrEmpty(model.email) || string.IsNullOrEmpty(model.phone_1))
            {
                // get existing customer info for the email/phone number, so it won't overwrite it with empty string
                var existingCustomerModel = await _customerManager.GetCustomerAsync(model.ClientId, model.customer_id);
                if (existingCustomerModel != null)
                {
                    if (string.IsNullOrEmpty(model.phone_1))
                    {
                        model.phone_1 = existingCustomerModel.Phone1;
                    }
                    if (string.IsNullOrEmpty(model.phone_2))
                    {
                        model.phone_2 = existingCustomerModel.Phone2;
                    }
                    if (string.IsNullOrEmpty(model.email))
                    {
                        model.email = existingCustomerModel.EmailAddress;
                    }
                }
            }

            var customerModel = Mapper.Map<BillingCustomerPremiseModel, CustomerModel>(model);
            var billingModel = Mapper.Map<BillingCustomerPremiseModel, BillingModel>(model);
            var premiseModel = Mapper.Map<BillingCustomerPremiseModel, PremiseModel>(model);
            var accountLookUpModel = Mapper.Map<BillingCustomerPremiseModel, AccountLookupModel>(model);
            var clientAccountModel = Mapper.Map<BillingCustomerPremiseModel, ClientAccountModel>(model);
            var customerMobileNumberLookupModel = Mapper.Map<BillingCustomerPremiseModel, CustomerMobileNumberLookupModel>(model);

            var tasks = new List<Task<bool>>
            {
                InsertForCustomer(customerModel),
                InsertForBilling(billingModel),
                InsertForPremise(premiseModel),
                InsertForAccountLookup(accountLookUpModel,model.premise_id,model.service_commodity,model.meter_replaces_meterid,model.meter_type),
                InsertForClientAccount(clientAccountModel),
                InsertForcustomerMobileNumberLookup(customerMobileNumberLookupModel)
            };

            bool[] taskResults = await Task.WhenAll(tasks);
            //Debug.WriteLine($"[[E_iomba_{model.customer_id}]]")
            return !taskResults.Any(result => false);
        }


        /// <summary>
        /// To insert or update CustomerMobileNumberLookupModel to database
        /// </summary>
        /// <param name="model">model to be inserted</param>
        /// <returns>true</returns>
        private async Task<bool> InsertForcustomerMobileNumberLookup(CustomerMobileNumberLookupModel model)
        {
            if (!string.IsNullOrWhiteSpace(model.MobileNumber))
            {
                return await _customerMobileNumberLookup.InsertOrMergeCustomerMobileNumberLookupAsync(model);
            }
            return true;
        }

        /// <summary>
        /// To insert or update ClientAccountModel to database
        /// </summary>
        /// <param name="model">model to be inserted</param>
        /// <returns>bool</returns>
        private async Task<bool> InsertForClientAccount(ClientAccountModel model)
        {
            return await _clientAccountManager.InsertOrMergeClientAccount(model);
        }

        /// <summary>
        /// To insert or update CustomerModel to database
        /// </summary>
        /// <param name="customerModel">customerModel to be inserted</param>
        /// <returns>bool</returns>
        private async Task<bool> InsertForCustomer(CustomerModel customerModel)
        {
            customerModel.Country = Enums.Country.US.ToString();
            customerModel.Source = Enums.Source.Utility.ToString();
            customerModel.IsBusiness = customerModel.CustomerType.Equals("Commercial");

            return await _customerManager.InsertOrMergeCustomerAsync(customerModel);
        }

        /// <summary>
        /// To insert or update BillingModel to database
        /// </summary>
        /// <param name="billingModel">BillingModel to be inserted</param>
        /// <returns>true</returns>
        private async Task<bool> InsertForBilling(BillingModel billingModel)
        {
            var insertLatest = true;
            var deletePrevious = true;
            var hasMeterChange = false;
            var hasRateClassChange = false;
            if (string.IsNullOrWhiteSpace(billingModel.ServiceContractId))
                billingModel.ServiceContractId = BillingEntity.GetServiceContractId(billingModel.MeterId, billingModel.AccountId, billingModel.PremiseId, billingModel.CommodityId);

            billingModel.StartDate = billingModel.EndDate.AddDays(-1 * Convert.ToInt32(billingModel.BillDays));
            billingModel.Source = Enums.Source.Utility.ToString();
            billingModel.ReadQuality = billingModel.ReadQuality.Equals("0") ? "Actual" : "Estimated";

            var latestBill = await _billingManager.GetServiceDetailsFromBillAsync(billingModel.ClientId, billingModel.CustomerId,
                billingModel.AccountId, billingModel.ServiceContractId);

            // check if there's meter exchange
            if (latestBill != null &&  (!string.IsNullOrEmpty(billingModel.ReplacedMeterId) ||  latestBill.MeterId != billingModel.MeterId) && !IsStartOfBillCycle(billingModel))
            {
                var replacedMeterId = !string.IsNullOrEmpty(billingModel.ReplacedMeterId)
                    ? billingModel.ReplacedMeterId
                    : latestBill.MeterId;
                hasMeterChange = await _accountUpdatesManager.InsertOrMergeAccountUpdateAsync(billingModel.ClientId,
                    billingModel.CustomerId, billingModel.AccountId, billingModel.PremiseId, billingModel.ServiceContractId, billingModel.MeterId,
                    billingModel.MeterId, replacedMeterId, Enums.AccountUpdateType.MeterReplacement.ToString());
            }

            // check if there's rate class change
            if (latestBill != null && !IsStartOfBillCycle(billingModel))
            {
                var latestBillRateClass = latestBill.RateClass;
                var billRateClass = billingModel.RateClass;
                // if rate helper, retrieve rate class attribute
                if(latestBillRateClass.StartsWith("#"))
                {
                    latestBillRateClass = latestBillRateClass.Remove(0, 1);
                    var attributeList = latestBillRateClass.Split('|').ToList();
                    var rcAttribute = attributeList.Find(a => a.StartsWith("rc~"));

                    latestBillRateClass = rcAttribute?.Remove(0, 3);
                }
                // if rate helper, retrieve rate class attribute
                if (billRateClass.StartsWith("#"))
                {
                    billRateClass = billRateClass.Remove(0, 1);
                    var attributeList = billRateClass.Split('|').ToList();
                    var rcAttribute = attributeList.Find(a => a.StartsWith("rc~"));

                    billRateClass = rcAttribute?.Remove(0, 3);
                }

                if (latestBillRateClass != billRateClass)
                {
                    hasRateClassChange =
                        await _accountUpdatesManager.InsertOrMergeAccountUpdateAsync(billingModel.ClientId,
                            billingModel.CustomerId, billingModel.AccountId, billingModel.PremiseId,
                            billingModel.ServiceContractId, billingModel.MeterId,
                            billingModel.RateClass, latestBill.RateClass, Enums.AccountUpdateType.RateClass.ToString());
                }
            }

            if (hasMeterChange || hasRateClassChange)
            {
                return true;
            }

            if (latestBill != null && latestBill.EndDate <= billingModel.EndDate)
            {
                deletePrevious = await
                    _billingManager.DeleteLatestServiceDetailsAsync(billingModel.ClientId, billingModel.CustomerId,
                        billingModel.AccountId, billingModel.ServiceContractId);
                //Thread.Sleep(1000)
            }

            var insertHistory = await _billingManager.InsertOrMergeHistoryBillAsync(billingModel);

            if ((latestBill == null || latestBill.EndDate <= billingModel.EndDate) && deletePrevious)
            {
                insertLatest = await _billingManager.InsertOrMergeLatestBillAsync(billingModel);
                //Thread.Sleep(10)
            }
            //Thread.Sleep(10)

            if (insertLatest && insertHistory && deletePrevious)
                return true;
            return false;
        }

        /// <summary>
        /// To insert or update PremiseModel to database
        /// </summary>
        /// <param name="premiseModel">premiseModel to be inserted</param>
        /// <returns>bool</returns>
        private async Task<bool> InsertForPremise(PremiseModel premiseModel)
        {
            premiseModel.Country = Enums.Country.US.ToString();
            return
                await
                    _premiseManager.InsertOrMergePremiseAsync(premiseModel);
        }

        /// <summary>
        /// To insert or update AccountLookupModel to database
        /// </summary>
        /// <param name="accountLookupModel">accountLookupModel to be inserted</param>
        /// <param name="premiseId">premiseId of the data to be inserted</param>
        /// <param name="commodityId">commodityId of the data to be inserted</param>
        /// <param name="replacedMeterId">replacedMeterId of the data to be inserted</param>
        /// <param name="meterType">meterType of the data to be inserted</param>
        /// <returns>true</returns>
        private async Task<bool> InsertForAccountLookup(AccountLookupModel accountLookupModel, string premiseId, int commodityId, string replacedMeterId, string meterType)
        {
            bool isSuccess;

            // do not update AccountMeterLookup when the commodity is sewer (4)
            if (commodityId == 4)
            {
                return true;
            }

            if (!string.IsNullOrWhiteSpace(replacedMeterId))
            {
                var data = await _accountManager.GetAccountMeterDetails(accountLookupModel.ClientId, replacedMeterId, accountLookupModel.CustomerId);
                if (data != null)
                {
                    data.IsActive = false;
                    await _accountManager.InsertOrMergeAccountLookupAsync(data);

                    if (data.IsServiceContractCreatedBySystem)
                        accountLookupModel.ServiceContractId = BillingEntity.GetServiceContractId(accountLookupModel.MeterId,
                            accountLookupModel.AccountId, premiseId, commodityId);
                    else
                        accountLookupModel.ServiceContractId = data.ServiceContractId;

                    isSuccess = await _accountManager.InsertOrMergeAccountLookupAsync(accountLookupModel);
                    return isSuccess;
                }
            }

            accountLookupModel.IsServiceContractCreatedBySystem =
                string.IsNullOrWhiteSpace(accountLookupModel.ServiceContractId);

            if (string.IsNullOrWhiteSpace(accountLookupModel.ServiceContractId))
                accountLookupModel.ServiceContractId = BillingEntity.GetServiceContractId(accountLookupModel.MeterId, accountLookupModel.AccountId,
                premiseId, commodityId);

            if (!string.Equals(meterType, Convert.ToString(Enums.MeterType.nonmetered), StringComparison.CurrentCultureIgnoreCase))
            {
                isSuccess = await _accountManager.InsertOrMergeAccountLookupAsync(accountLookupModel);
                return isSuccess;
            }
            return true;
        }

        /// <summary>
        /// check if today is start of current bill cycle
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        private bool IsStartOfBillCycle(BillingModel model)
        {
            var btdSettings = _clientConfigFacadeManager.GetBillToDateSettings(model.ClientId);
            var clientSettings = _clientConfigFacadeManager.GetClientSettings(model.ClientId);

            var clientTimezone = (Enums.Timezone)Enum.Parse(typeof(Enums.Timezone), Convert.ToString(clientSettings.TimeZone));
            var timeZoneInfo = TimeZoneInfo.FindSystemTimeZoneById(clientTimezone.GetDescriptionOfEnum());

            var clientLocalDateNow = TimeZoneInfo.ConvertTimeFromUtc(DateTime.UtcNow, timeZoneInfo);


            var billStartDate = clientLocalDateNow;
            if (btdSettings.Settings.General.UseProjectedNumDaysforBilltoDate)
            {
                billStartDate = model.EndDate.AddDays(1);
            }
            else
            {
                var billCycleSchedule = _billingCycleScheduleManager.GetBillingCycleScheduleByDateAsync(model.ClientId,
                    model.BillCycleScheduleId, clientLocalDateNow);

                if (billCycleSchedule != null)
                {
                    billStartDate = Convert.ToDateTime(billCycleSchedule.BeginDate);
                }
            }

            if (billStartDate.Date == clientLocalDateNow.Date)
                return true;
            return false;
        }
    }
}
