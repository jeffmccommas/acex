﻿using System;
using AO.BusinessContracts;
using Enums = CE.AO.Utilities.Enums;

namespace AO.Business
{
    /// <summary>
    /// This class converts unit of measure to the standard unit of measure
    /// </summary>
    public class UomConversion : IUomConversion
    {
        private const double CcmToGalConversionFactor = 264.172;
        private const double CfToGalConversionFactor = 7.481;
        private const double CcfToGalConversionFactor = 748.1;
        private const double McfToGalConversionFactor = 7481;
        private const double CmToCfConversionFactor = 35.96;

        /// <summary>
        /// Converts Consumption value units of measure from <paramref name="fromUom"/> to <paramref name="toUom"/>.
        /// </summary>
        /// <param name="fromUom"></param>
        /// <param name="toUom"></param>
        /// <param name="consumption"></param>
        /// <param name="conversionNotFound"></param>
        /// <returns></returns>
        public double Convert(Enums.UomType fromUom, Enums.UomType toUom, double consumption, out bool conversionNotFound)
        {
            double standardConsumption = 0;
            conversionNotFound = false;
            switch (fromUom)
            {
                case Enums.UomType.cm:
                    switch (toUom)
                    {
                        case Enums.UomType.gal:
                            standardConsumption = ConvertCubicMeterToGallon(consumption);
                            break;
                        case Enums.UomType.cf:
                            standardConsumption = ConvertCubicMeterToCubicFoot(consumption);
                            break;
                        default:
                            conversionNotFound = true;
                            break;
                    }
                    break;
                case Enums.UomType.cf:
                    switch (toUom)
                    {
                        case Enums.UomType.gal:
                            standardConsumption = ConvertCubicFeetToGallon(consumption);
                            break;
                        default:
                            conversionNotFound = true;
                            break;
                    }
                    break;
                case Enums.UomType.ccf:
                    switch (toUom)
                    {
                        case Enums.UomType.gal:
                            standardConsumption = ConvertCentumCubicFeetToGallon(consumption);
                            break;
                        default:
                            conversionNotFound = true;
                            break;
                    }
                    break;
                case Enums.UomType.mcf:
                    switch (toUom)
                    {
                        case Enums.UomType.gal:
                            standardConsumption = ConvertMilCubicFeetToGallon(consumption);
                            break;
                        default:
                            conversionNotFound = true;
                            break;
                    }
                    break;
            }
            return standardConsumption;
        }

        /// <summary>
        /// Converts unit of measure from Cubic Meter to Cubic Foot
        /// </summary>
        /// <param name="consumption"></param>
        /// <returns></returns>
        private double ConvertCubicMeterToCubicFoot(double consumption)
        {
            return Math.Round(consumption * CmToCfConversionFactor, 4);
        }
        /// <summary>
        /// Converts unit of measure from  Cubic Meter to Gallon
        /// </summary>
        /// <param name="consumption"></param>
        /// <returns></returns>
        private double ConvertCubicMeterToGallon(double consumption)
        {
            return Math.Round(consumption * CcmToGalConversionFactor, 4);
        }
        /// <summary>
        /// Converting unit of measure from  Cubic Feet to Gallon
        /// </summary>
        /// <param name="consumption"></param>
        /// <returns></returns>
        private double ConvertCubicFeetToGallon(double consumption)
        {
            return Math.Round(consumption * CfToGalConversionFactor, 4);
        }
        /// <summary>
        /// Converts unit of measure from  CCF to Gallon
        /// </summary>
        /// <param name="consumption"></param>
        /// <returns></returns>
        private double ConvertCentumCubicFeetToGallon(double consumption)
        {
            return Math.Round(consumption * CcfToGalConversionFactor, 4);
        }
        /// <summary>
        /// Converts unit of measure from  Mil Cubic Feet to Gallon
        /// </summary>
        /// <param name="consumption"></param>
        /// <returns></returns>
        private double ConvertMilCubicFeetToGallon(double consumption)
        {
            return Math.Round(consumption * McfToGalConversionFactor, 4);
        }
    }
}
