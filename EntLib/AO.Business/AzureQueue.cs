﻿using System;
using System.Threading.Tasks;
using AO.BusinessContracts;
using CE.AO.Utilities;

namespace AO.Business
{
    public class AzureQueue : IAzureQueue
    {
        public AzureQueue(Lazy<IQueueManager> queueRepository)
        {
            QueueRepository = queueRepository;
        }

        private Lazy<IQueueManager> QueueRepository { get; }

        public async Task AddMessageToBatchQueue(string message)
        {
            await QueueRepository.Value.AddQueueAsync(Entities.Constants.QueueNames.AoImportBatchProcessQueue, message).ConfigureAwait(false);
        }

        public async Task AddMessageToCalculationQueue(string message)
        {
            await QueueRepository.Value.AddQueueAsync(Entities.Constants.QueueNames.AoCalculationQueue, message).ConfigureAwait(false);
        }
    }
}
