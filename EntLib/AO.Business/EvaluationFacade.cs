﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AO.BusinessContracts;
using CE.AO.Logging;
using CE.AO.Models;
using CE.BillToDate;
using CE.RateModel;
using Microsoft.Practices.Unity;
using Utilities = CE.AO.Utilities;

namespace AO.Business
{
    /// <summary>
    /// EvaluationFacade class is used for evaluation
    /// </summary>
    public class EvaluationFacade : IEvaluationFacade
    {
        public EvaluationFacade(LogModel logModel)
        {
            LogModel = logModel;
        }

        public LogModel LogModel { get; set; }

        [Dependency]
        public Lazy<IEvaluation> EvaluationManager { get; set; }

        [Dependency]
        public Lazy<ISubscription> SubscriptionManager { get; set; }

        [Dependency]
        public Lazy<ICalculation> CalculationManager { get; set; }

        [Dependency]
        public Lazy<ITallAMI> AmiManager { get; set; }

        [Dependency]
        public Lazy<IBilling> BillingManager { get; set; }

        [Dependency]
        public Lazy<IClientConfigFacade> ClientConfigFacadeManager { get; set; }

        [Dependency]
        public Lazy<ICalculationManagerFactory> CalculationManagerFactory { get; set; }

        /// <summary>
        /// To process the evaluation
        /// </summary>
        /// <param name="clientId">Client id to be processed</param>
        /// <param name="customerId">Customer id to be processed</param>
        /// <param name="accountId">Accout id to be processed</param>
        /// <param name="serviceContractId">Service contract id to be processed</param>
        /// <param name="asOfDate">Date for which record to be processed</param>
        public void ProcessEvaluation(int clientId, string customerId, string accountId, string serviceContractId, DateTime asOfDate)
        {
            // Get data Subscription data for customer
            List<SubscriptionModel> subscriptions = SubscriptionManager.Value.GetCustomerSubscriptions(clientId, customerId, accountId).ToList();
            var tasks = new List<Task<CalculationModel>>();

            var billingModels = new List<BillingModel>();

            if (string.IsNullOrEmpty(serviceContractId))
            {
                billingModels.AddRange(BillingManager.Value.GetAllServicesFromLastBill(clientId, customerId,
                    accountId).ToList());
                if (billingModels.Any())
                {
                    // ReSharper disable once LoopCanBeConvertedToQuery
                    foreach (var bill in billingModels.Where(e => e.MeterType == Utilities.Enums.MeterType.ami.ToString()))
                    {
                        tasks.Add(CalculationManager.Value.GetCtdCalculationAsync(clientId, accountId, bill.ServiceContractId));
                    }
                    tasks.Add(CalculationManager.Value.GetBtdCalculationAsync(clientId, accountId));
                }
            }
            else
            {
                billingModels.Add(BillingManager.Value.GetServiceDetailsFromBillAsync(clientId, customerId,
                   accountId, serviceContractId).Result);

                tasks.Add(CalculationManager.Value.GetCtdCalculationAsync(clientId, accountId, serviceContractId));
                tasks.Add(CalculationManager.Value.GetBtdCalculationAsync(clientId, accountId));
            }

            Task.WhenAll(tasks);

            List<CalculationModel> calculations = tasks.Where(e => e.Result != null).Select(task => task.Result).ToList();
            if (calculations.Count <= 0)
            {
                Logger.Warn("Calculations needs to be performed before Evaluation", LogModel);
                return;
            }

            var clientSettings = ClientConfigFacadeManager.Value.GetClientSettings(clientId);
            var btdCalculation = calculations.FirstOrDefault(e => string.IsNullOrEmpty(e.ServiceContractId));
            if (btdCalculation != null && !btdCalculation.CustomerId.Equals(customerId, StringComparison.CurrentCultureIgnoreCase))
            {
                btdCalculation.CustomerId = customerId;
            }
            var serviceAccountCalculations = new List<CalculationModel> { btdCalculation };

            EvaluationManager.Value.EvaluateInsight(subscriptions, serviceAccountCalculations, clientSettings, new List<TallAmiModel>(), new List<TierBoundary>(), asOfDate, null).Wait();

            foreach (var calculation in calculations.Where(e => !string.IsNullOrEmpty(e.ServiceContractId)))
            {
                if (!calculation.CustomerId.Equals(customerId, StringComparison.CurrentCultureIgnoreCase))
                {
                    calculation.CustomerId = customerId;
                }

                serviceAccountCalculations = new List<CalculationModel>();
                var amiList = new List<TallAmiModel>();

                //Get tire boundries                 
                var settings = ClientConfigFacadeManager.Value.GetBillToDateSettings(clientId);
                var rateCompanyId = settings.RateCompanyId;
                var billToDateCalculator = CalculationManagerFactory.Value.CreateCalculationManager(settings);
                //NOTE: need to place dynamically
                var rateClass = calculation.RateClass;
                List<TierBoundary> tierBoundaries = billToDateCalculator.GetTierBoundaries(rateCompanyId, rateClass,
                    Convert.ToDateTime(calculation.BillCycleStartDate),
                    Convert.ToDateTime(calculation.BillCycleEndDate));

                double conversionFactor = 1;
                if (calculation.CommodityId == 2 && settings.Settings.General.UseConversionFactorForGas)
                {
                    conversionFactor = settings.Settings.General.ConversionFactorForGas;
                }
                else if (calculation.CommodityId == 3 && settings.Settings.General.UseConversionFactorForWater)
                    conversionFactor = settings.Settings.General.ConversionFactorForWater;

                if (tierBoundaries != null)
                {
                    foreach (var tierBoundary in tierBoundaries)
                    {
                        tierBoundary.Threshold = tierBoundary.Threshold / conversionFactor;
                    }
                }

                //Conversion of local datetime to UTC format
                var clientTimezone = (Utilities.Enums.Timezone)Enum.Parse(typeof(Utilities.Enums.Timezone), Convert.ToString(clientSettings.TimeZone));
                var timeZoneInfo = TimeZoneInfo.FindSystemTimeZoneById(clientTimezone.GetDescription());
                var date = TimeZoneInfo.ConvertTimeToUtc(Convert.ToDateTime(calculation.AsOfAmiDate).Date, timeZoneInfo);

                //Get Daily AMI usage 
                IEnumerable<TallAmiModel> ami = AmiManager.Value.GetAmi(date, calculation.MeterId, calculation.AccountId, clientId);
                if (ami != null)
                    amiList = ami.ToList();

                var billingmodel =
                    billingModels.FirstOrDefault(b => b.ServiceContractId == calculation.ServiceContractId);

                serviceAccountCalculations.Add(calculation);
                EvaluationManager.Value.EvaluateInsight(subscriptions, serviceAccountCalculations, clientSettings, amiList,
                    tierBoundaries, asOfDate, billingmodel).Wait();
            }
        }
    }
}
