﻿using System;
using System.Text;
using CE.AO.Models;
using Microsoft.Azure;
using Microsoft.ServiceBus.Messaging;
using Newtonsoft.Json;
using AO.BusinessContracts;
using EventHubSender = CE.AO.Utilities.EventHubSender;

namespace AO.Business
{
    /// <summary>
    /// Trigger Logic for PreAggCalculation Event Hub
    /// </summary>    
    public class EventHubPreAggCalculationTrigger : IProcessTrigger<AccountPreAggCalculationRequestModel>
    {
        public LogModel LogModel { get; set; }
        public EventHubPreAggCalculationTrigger(LogModel logModel)
        {
            LogModel = logModel;
        }

        private static readonly string ImportDataEventHubPath =
            CloudConfigurationManager.GetSetting("PreAggCalculationEventHubName");
        private static readonly string ImportDataEventHubSendConnectionString =
            CloudConfigurationManager.GetSetting("PreAggCalculationEventHubSendConnectionString");

        /// <summary>
        /// Send PreAggCalculation event to EventHub
        /// </summary>
        public void Trigger(AccountPreAggCalculationRequestModel triggerDto)
        {
            var serializedEventData = JsonConvert.SerializeObject(triggerDto);
            var data = new EventData(Encoding.UTF8.GetBytes(serializedEventData));

            data.Properties.Add("Source", LogModel.Source);
            data.Properties.Add("ProcessingType", Convert.ToString(LogModel.ProcessingType));
            data.Properties.Add("RowIndex", Convert.ToString(LogModel.RowIndex));
            var preAggCalculationEventHubClient = EventHubSender.GetEventHubClient(ImportDataEventHubSendConnectionString,
                            ImportDataEventHubPath);
            preAggCalculationEventHubClient.Send(data);
            preAggCalculationEventHubClient.Close();
        }
    }
}