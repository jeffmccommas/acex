﻿using System;
using AO.BusinessContracts;
using AO.DataAccess.Contracts;

namespace AO.Business
{
    /// <inheritdoc />
    public class IdentityFacade : IIdentityFacade
    {
        private readonly ITenantRepository _tenantRepository;

        public IdentityFacade(ITenantRepository tenantRepository)
        {
            _tenantRepository = tenantRepository;
        }

        /// <inheritdoc />
        public int GetClientIdForTenant(string tenantId)
        {
            var tenant = _tenantRepository.GetByTenantId(tenantId);

            if (tenant == null)
            {
                throw new ArgumentNullException(nameof(tenant),
                    @"Configuration Error:  The client associated with this tenant has not been properly configured.");
            }

            return tenant.ClientId;
        }
    }
}
