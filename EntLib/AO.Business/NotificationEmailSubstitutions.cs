﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using CE.AO.Logging;
using CE.AO.Models;
using CE.ContentModel.Entities;
using Enums = CE.AO.Utilities.Enums;
using CE.RateModel;

namespace AO.Business
{
    /// <summary>
    /// NotificationEmailSubstitutions class is used to replace email substitutes with values from DB
    /// </summary>
    public partial class Notification
    {
        private Dictionary<string, List<string>> GetBillToDateEmailSubstitutions(ClientSettings clientSettings, CustomerModel customerModel, CalculationModel calculation, DateTime asOfDate, int unmaskedAccountIdEndingDigit)
        {
            var accountNumber = calculation.AccountId;
            if (unmaskedAccountIdEndingDigit > 0)
            {
                accountNumber = AccountLookupModel.GetMaskedAccountId(accountNumber, unmaskedAccountIdEndingDigit);
            }

            var address = string.Empty;

            if (customerModel != null)
            {
                if (!string.IsNullOrEmpty(customerModel.AddressLine1))
                    address = address + customerModel.AddressLine1;

                if (!string.IsNullOrEmpty(customerModel.AddressLine2))
                {
                    address = !string.IsNullOrEmpty(address)
                        ? address + ", " + customerModel.AddressLine2
                        : address + customerModel.AddressLine2;
                }

                if (!string.IsNullOrEmpty(customerModel.AddressLine3))
                {
                    address = !string.IsNullOrEmpty(address)
                        ? address + ", " + customerModel.AddressLine3
                        : address + customerModel.AddressLine3;
                }
            }

            var identifiers = new Dictionary<string, List<string>>
            {
                {"-FirstName-", new List<string> {customerModel != null ? customerModel.FirstName : ""}},
                {"-LastName-", new List<string> {customerModel != null ? customerModel.LastName : ""}},
                {"-AsOfDate-", new List<string> {asOfDate.ToShortDateString()}},
                {"-AccountNumber-", new List<string> {accountNumber}},
                {"-AccountAddress-", new List<string>{address}},
                {
                    "-DaysIntoBillingCycle-", new List<string>
                    {
                        ((asOfDate.Date - Convert.ToDateTime(calculation.BillCycleStartDate).Date).Days + 1)
                            .ToString()
                    }
                },
                {"-ProjectedCost-", new List<string> {clientSettings.GetFormattedValue( calculation.ProjectedCost ?? 0, "0.00", "altNumericFormat") }},
                {"-BillToDate-", new List<string> { clientSettings.GetFormattedValue( calculation.Cost ?? 0, "0.00", "altNumericFormat") } }
                // "{"-ProjectedCost-", new List<string> {$"{calculation.ProjectedCost:0.00}"}},"
                // "{"-BillToDate-", new List<string> {$"{calculation.Cost:0.00}"}}"
            };
            return identifiers;
        }

        private Dictionary<string, List<string>> GetCostToDateEmailSubstitutions(ClientSettings clientSettings, CustomerModel customerModel, CalculationModel calculation, BillingModel billingModel, DateTime asOfDate, int unmaskedAccountIdEndingDigit)
        {
            var commodity = Enums.CommodityType.Undefined;
            if (calculation.CommodityId != null)
            {
                commodity = (Enums.CommodityType)calculation.CommodityId;
            }
            var address = string.Empty;

            if (customerModel != null)
            {
                if (!string.IsNullOrEmpty(customerModel.AddressLine1))
                    address = address + customerModel.AddressLine1;

                if (!string.IsNullOrEmpty(customerModel.AddressLine2))
                {
                    address = !string.IsNullOrEmpty(address)
                        ? address + ", " + customerModel.AddressLine2
                        : address + customerModel.AddressLine2;
                }

                if (!string.IsNullOrEmpty(customerModel.AddressLine3))
                {
                    address = !string.IsNullOrEmpty(address)
                        ? address + ", " + customerModel.AddressLine3
                        : address + customerModel.AddressLine3;
                }
            }

            var accountNumber = calculation.AccountId;
            if (unmaskedAccountIdEndingDigit > 0)
            {
                accountNumber = AccountLookupModel.GetMaskedAccountId(accountNumber, unmaskedAccountIdEndingDigit);
            }
            var identifiers = new Dictionary<string, List<string>>
            {
                {"-FirstName-", new List<string> {customerModel != null ? customerModel.FirstName : ""}},
                {"-LastName-", new List<string> {customerModel != null ? customerModel.LastName : ""}},
                {"-AsOfDate-", new List<string> {asOfDate.ToShortDateString()}},
                {"-AccountNumber-", new List<string> {accountNumber}},
                {"-MeterNumber-", new List<string> {billingModel != null ? billingModel.MeterId : ""}},
                {"-ServiceAddress-", new List<string>{address}},
                {
                    "-DaysIntoBillingCycle-", new List<string>
                    {
                        ((asOfDate.Date - Convert.ToDateTime(calculation.BillCycleStartDate).Date).Days + 1)
                            .ToString()
                    }
                },
                {"-ProjectedCostToDate-", new List<string> { clientSettings.GetFormattedValue(calculation.ProjectedCost ?? 0, "0.00", "altNumericFormat") }},
                {"-CostToDate-", new List<string> {clientSettings.GetFormattedValue(calculation.Cost ?? 0, "0.00", "altNumericFormat") }},
                //{"-ProjectedCostToDate-", new List<string> {$"{calculation.ProjectedCost:0.00}"}},
                //{"-CostToDate-", new List<string> {$"{calculation.Cost:0.00}"}},
                {"-CommodityType-", new List<string> {commodity.GetDescription()}}
            };
            return identifiers;
        }

        private Dictionary<string, List<string>> GetUsageEmailSubstitutions(ClientSettings clientSettings, CustomerModel customerModel, CalculationModel calculation, BillingModel billingModel, DateTime asOfDate, int unmaskedAccountIdEndingDigit, IList<ClientUOM> uomContentList, List<ClientTextContent> textContentList)
        {
            var commodity = Enums.CommodityType.Undefined;
            if (calculation.CommodityId != null)
            {
                commodity = (Enums.CommodityType)calculation.CommodityId;
            }

            int? unit = billingModel?.UOMId ?? calculation.Unit;
            var uomId = 99;
            if (unit.HasValue)
            {
                uomId = unit.Value;
            }
            // if the calcution is done in a different UOM from bill's
            // convert the usage into bill's UOM for the notification
            double? usage;

            if (unit.HasValue && calculation.Unit.HasValue && unit != calculation.Unit)
            {
                var conversionFactor = Extensions.GetConversionFactor(customerModel.ClientId, calculation.Unit.Value,
                    unit.Value);
                usage = calculation.Usage * conversionFactor;
            }
            else
            {
                usage = calculation.Usage;
            }
            var address = string.Empty;

            if (customerModel != null)
            {
                if (!string.IsNullOrEmpty(customerModel.AddressLine1))
                    address = address + customerModel.AddressLine1;

                if (!string.IsNullOrEmpty(customerModel.AddressLine2))
                {
                    address = !string.IsNullOrEmpty(address)
                        ? address + ", " + customerModel.AddressLine2
                        : address + customerModel.AddressLine2;
                }

                if (!string.IsNullOrEmpty(customerModel.AddressLine3))
                {
                    address = !string.IsNullOrEmpty(address)
                        ? address + ", " + customerModel.AddressLine3
                        : address + customerModel.AddressLine3;
                }
            }

            // get uom content
            var uomKey = ((Enums.UomType)uomId).ToString();
            var uom = Extensions.GetUomTextContent(uomKey, uomContentList, textContentList);
            if (string.IsNullOrEmpty(uom))
                uom = uomKey;

            var accountNumber = calculation.AccountId;
            if (unmaskedAccountIdEndingDigit > 0)
            {
                accountNumber = AccountLookupModel.GetMaskedAccountId(accountNumber, unmaskedAccountIdEndingDigit);
            }
            var identifiers = new Dictionary<string, List<string>>
            {
                {"-FirstName-", new List<string> {customerModel != null ? customerModel.FirstName : ""}},
                {"-LastName-", new List<string> {customerModel != null ? customerModel.LastName : ""}},
                {"-AsOfDate-", new List<string> {asOfDate.ToShortDateString()}},
                {"-AccountNumber-", new List<string> {accountNumber}},
                {"-MeterNumber-", new List<string> {billingModel != null ? billingModel.MeterId : ""}},
                {"-ServiceAddress-", new List<string>{address}},
                {
                    "-DaysIntoBillingCycle-", new List<string>
                    {
                        ((asOfDate.Date - Convert.ToDateTime(calculation.BillCycleStartDate).Date).Days + 1)
                            .ToString()
                    }
                },
                {"-ProjectedCostToDate-", new List<string> {clientSettings.GetFormattedValue(calculation.ProjectedCost ?? 0, "0.00", "altNumericFormat") }},
                // "{"-ProjectedCostToDate-", new List<string> {$"{calculation.ProjectedCost:0.00}"}},"
                {"-UsageToDate-", new List<string> {$"{usage:0.00}"}},
                {"-UOM-", new List<string> {uom}},
                {"-CommodityType-", new List<string> { commodity.GetDescription()}}
            };
            return identifiers;
        }


        private Dictionary<string, List<string>> GetAccountLevelCostThresholdEmailSubstitutions(ClientSettings clientSettings, CustomerModel customerModel, CalculationModel calculation, CustomInsightModel customInsightModel, DateTime asOfDate, int unmaskedAccountIdEndingDigit)
        {
            var accountNumber = calculation.AccountId;
            if (unmaskedAccountIdEndingDigit > 0)
            {
                accountNumber = AccountLookupModel.GetMaskedAccountId(accountNumber, unmaskedAccountIdEndingDigit);
            }
            var address = string.Empty;

            if (customerModel != null)
            {
                if (!string.IsNullOrEmpty(customerModel.AddressLine1))
                    address = address + customerModel.AddressLine1;

                if (!string.IsNullOrEmpty(customerModel.AddressLine2))
                {
                    address = !string.IsNullOrEmpty(address)
                        ? address + ", " + customerModel.AddressLine2
                        : address + customerModel.AddressLine2;
                }

                if (!string.IsNullOrEmpty(customerModel.AddressLine3))
                {
                    address = !string.IsNullOrEmpty(address)
                        ? address + ", " + customerModel.AddressLine3
                        : address + customerModel.AddressLine3;
                }
            }
            Dictionary<string, List<string>> identifiers = new Dictionary<string, List<string>>
            {
                {"-FirstName-", new List<string> {customerModel != null ? customerModel.FirstName : ""}},
                {"-LastName-", new List<string> {customerModel != null ? customerModel.LastName : ""}},
                {"-AsOfDate-", new List<string> {asOfDate.ToShortDateString()}},
                {"-AccountNumber-", new List<string> {accountNumber}},
                {"-AccountAddress-", new List<string> {address}},
                {"-AccountCostThreshold-", new List<string> {clientSettings.GetFormattedValue( customInsightModel.ThresholdValue ?? 0, "0.0", "altNumericFormat") }},
                {"-BTDCost-", new List<string> {clientSettings.GetFormattedValue(calculation.Cost ?? 0, "0", "altNumericFormat") }},
                // "{"-AccountCostThreshold-", new List<string> {$"{customInsightModel.ThresholdValue:0.00}"}},"
                // "{"-BTDCost-", new List<string> {$"{calculation.Cost:0.00}"}},"
                {
                    "-DaysIntoBillingCycle-", new List<string>
                    {
                        ((asOfDate.Date - Convert.ToDateTime(calculation.BillCycleStartDate).Date).Days + 1)
                            .ToString()
                    }
                },
                {"-ProjectedCost-", new List<string> {clientSettings.GetFormattedValue(calculation.ProjectedCost ?? 0, "0.00", "altNumericFormat") }}
                // "{"-ProjectedCost-", new List<string> {$"{calculation.ProjectedCost:0.00}"}}"
            };
            return identifiers;
        }

        private Dictionary<string, List<string>> GetServiceLevelUsageThresholdEmailSubstitutions(ClientSettings clientSettings, CustomerModel customerModel, CalculationModel calculation, CustomInsightModel customInsightModel, BillingModel billingModel, int unmaskedAccountIdEndingDigit, IList<ClientUOM> uomContentList, List<ClientTextContent> textContentList)
        {
            var commodity = Enums.CommodityType.Undefined;
            if (calculation.CommodityId != null)
            {
                commodity = (Enums.CommodityType)calculation.CommodityId;
            }

            int? unit = billingModel?.UOMId ?? calculation.Unit;
            var uomId = 99;
            if (unit.HasValue)
            {
                uomId = unit.Value;
            }

            // if the calcution is done in a different UOM from bill's
            // convert the usage into bill's UOM for the notification
            double? usage;
            if (unit.HasValue && calculation.Unit.HasValue && unit != calculation.Unit)
            {
                var conversionFactor = Extensions.GetConversionFactor(customerModel.ClientId, calculation.Unit.Value,
                    unit.Value);
                usage = calculation.Usage * conversionFactor;
            }
            else
            {
                usage = calculation.Usage;
            }
            var address = string.Empty;

            if (customerModel != null)
            {
                if (!string.IsNullOrEmpty(customerModel.AddressLine1))
                    address = address + customerModel.AddressLine1;

                if (!string.IsNullOrEmpty(customerModel.AddressLine2))
                {
                    address = !string.IsNullOrEmpty(address)
                        ? address + ", " + customerModel.AddressLine2
                        : address + customerModel.AddressLine2;
                }

                if (!string.IsNullOrEmpty(customerModel.AddressLine3))
                {
                    address = !string.IsNullOrEmpty(address)
                        ? address + ", " + customerModel.AddressLine3
                        : address + customerModel.AddressLine3;
                }
            }

            var accountNumber = calculation.AccountId;
            if (unmaskedAccountIdEndingDigit > 0)
            {
                accountNumber = AccountLookupModel.GetMaskedAccountId(accountNumber, unmaskedAccountIdEndingDigit);
            }

            // get uom content
            var uomKey = ((Enums.UomType)uomId).ToString();
            var uom = Extensions.GetUomTextContent(uomKey, uomContentList, textContentList);
            if (string.IsNullOrEmpty(uom))
                uom = uomKey;

            var asofDate = calculation.AsOfAmiDate.HasValue
                ? calculation.AsOfAmiDate.Value
                : calculation.AsOfCalculationDate;

            var identifiers = new Dictionary<string, List<string>>
            {
                {"-FirstName-", new List<string> {customerModel != null ? customerModel.FirstName : ""}},
                {"-LastName-", new List<string> {customerModel != null ? customerModel.LastName : ""}},
                {"-AsOfDate-", new List<string> { asofDate.ToShortDateString()}},
                {"-AccountNumber-", new List<string> {accountNumber}},
                {"-MeterNumber-", new List<string> {billingModel != null ? billingModel.MeterId : ""}},
                {"-ServiceAddress-", new List<string>{address}},
                {"-UsageThreshold-", new List<string> {$"{customInsightModel.ThresholdValue:0.00}"}},
                {"-UOM-", new List<string> {uom}},
                {"-CostToDate-", new List<string> {clientSettings.GetFormattedValue(calculation.Cost ?? 0, "0.00", "altNumericFormat") }},
                // "{"-CostToDate-", new List<string> {$"{calculation.Cost:0.00}"}},"
                {
                    "-DaysIntoBillingCycle-", new List<string>
                    {
                        ((asofDate.Date - Convert.ToDateTime(calculation.BillCycleStartDate).Date).Days + 1)
                            .ToString()
                    }
                },
                {"-UsageToDate-", new List<string> {$"{usage:0.00}"}},
                {"-CommodityType-", new List<string> {commodity.GetDescription()}}
            };
            return identifiers;
        }

        private Dictionary<string, List<string>> GetServiceLevelCostThresholdEmailSubstitutions(ClientSettings clientSettings,CustomerModel customerModel, CalculationModel calculation, CustomInsightModel customInsightModel, BillingModel billingModel, int unmaskedAccountIdEndingDigit, IList<ClientUOM> uomContentList, List<ClientTextContent> textContentList)
        {
            var commodity = Enums.CommodityType.Undefined;
            if (calculation.CommodityId != null)
            {
                commodity = (Enums.CommodityType)calculation.CommodityId;
            }

            int? unit = billingModel?.UOMId ?? calculation.Unit;
            var uomId = 99;
            if (unit.HasValue)
            {
                uomId = unit.Value;
            }

            // if the calcution is done in a different UOM from bill's
            // convert the usage into bill's UOM for the notification
            double? usage;
            if (unit.HasValue && calculation.Unit.HasValue && unit != calculation.Unit)
            {
                var conversionFactor = Extensions.GetConversionFactor(customerModel.ClientId, calculation.Unit.Value,
                    unit.Value);
                usage = calculation.Usage * conversionFactor;
            }
            else
            {
                usage = calculation.Usage;
            }
            var address = string.Empty;

            if (customerModel != null)
            {
                if (!string.IsNullOrEmpty(customerModel.AddressLine1))
                    address = address + customerModel.AddressLine1;

                if (!string.IsNullOrEmpty(customerModel.AddressLine2))
                {
                    address = !string.IsNullOrEmpty(address)
                        ? address + ", " + customerModel.AddressLine2
                        : address + customerModel.AddressLine2;
                }

                if (!string.IsNullOrEmpty(customerModel.AddressLine3))
                {
                    address = !string.IsNullOrEmpty(address)
                        ? address + ", " + customerModel.AddressLine3
                        : address + customerModel.AddressLine3;
                }
            }

            var accountNumber = calculation.AccountId;
            if (unmaskedAccountIdEndingDigit > 0)
            {
                accountNumber = AccountLookupModel.GetMaskedAccountId(accountNumber, unmaskedAccountIdEndingDigit);
            }
            // get uom content
            var uomKey = ((Enums.UomType)uomId).ToString();
            var uom = Extensions.GetUomTextContent(uomKey, uomContentList, textContentList);
            if (string.IsNullOrEmpty(uom))
                uom = uomKey;
            var asOfDate = calculation.AsOfAmiDate.HasValue
                ? calculation.AsOfAmiDate.Value
                : calculation.AsOfCalculationDate;
            var identifiers = new Dictionary<string, List<string>>
            {
                {"-FirstName-", new List<string> {customerModel != null ? customerModel.FirstName : ""}},
                {"-LastName-", new List<string> {customerModel != null ? customerModel.LastName : ""}},
                {"-AsOfDate-", new List<string> { asOfDate.ToShortDateString()}},
                {"-AccountNumber-", new List<string> {accountNumber}},
                {"-MeterNumber-", new List<string> {billingModel != null ? billingModel.MeterId : ""}},
                {"-ServiceAddress-", new List<string>{address}},
                {"-CostThreshold-", new List<string> {clientSettings.GetFormattedValue( customInsightModel.ThresholdValue ?? 0, "0.00", "altNumericFormat") }},
                {"-CostToDate-", new List<string> {clientSettings.GetFormattedValue(calculation.Cost ?? 0, "0.00", "altNumericFormat") }},
              //{"-CostThreshold-", new List<string> {$"{customInsightModel.ThresholdValue:0.00}"}},
              //{"-CostToDate-", new List<string> {$"{calculation.Cost:0.00}"}},
                {
                    "-DaysIntoBillingCycle-", new List<string>
                    {
                        ((asOfDate.Date - Convert.ToDateTime(calculation.BillCycleStartDate).Date).Days + 1)
                            .ToString()
                    }
                },
                {"-UsageToDate-", new List<string> {$"{usage:0.00}"}},
                {"-UOM-", new List<string> {uom}},
                {"-CommodityType-", new List<string> {commodity.GetDescription()}}
            };
            return identifiers;
        }

        private Dictionary<string, List<string>> GetDayThresholdEmailSubstitutions(CustomerModel customerModel, CalculationModel calculation, CustomInsightModel customInsightModel, BillingModel billingModel, DateTime asOfDate, int unmaskedAccountIdEndingDigit, IList<ClientUOM> uomContentList, List<ClientTextContent> textContentList)
        {
            var commodity = Enums.CommodityType.Undefined;
            if (calculation.CommodityId != null)
            {
                commodity = (Enums.CommodityType)calculation.CommodityId;
            }
            int? unit = billingModel?.UOMId ?? calculation.Unit;
            var uomId = 99;
            if (unit.HasValue)
            {
                uomId = unit.Value;
            }
            var address = string.Empty;

            if (customerModel != null)
            {
                if (!string.IsNullOrEmpty(customerModel.AddressLine1))
                    address = address + customerModel.AddressLine1;

                if (!string.IsNullOrEmpty(customerModel.AddressLine2))
                {
                    address = !string.IsNullOrEmpty(address)
                        ? address + ", " + customerModel.AddressLine2
                        : address + customerModel.AddressLine2;
                }

                if (!string.IsNullOrEmpty(customerModel.AddressLine3))
                {
                    address = !string.IsNullOrEmpty(address)
                        ? address + ", " + customerModel.AddressLine3
                        : address + customerModel.AddressLine3;
                }
            }

            var accountNumber = calculation.AccountId;
            if (unmaskedAccountIdEndingDigit > 0)
            {
                accountNumber = AccountLookupModel.GetMaskedAccountId(accountNumber, unmaskedAccountIdEndingDigit);
            }

            // get uom content
            var uomKey = ((Enums.UomType)uomId).ToString();
            var uom = Extensions.GetUomTextContent(uomKey, uomContentList, textContentList);
            if (string.IsNullOrEmpty(uom))
                uom = uomKey;
            var identifiers = new Dictionary<string, List<string>>
            {
                {"-FirstName-", new List<string> {customerModel != null ? customerModel.FirstName : ""}},
                {"-LastName-", new List<string> {customerModel != null ? customerModel.LastName : ""}},
                {"-AsOfDate-", new List<string> {asOfDate.ToShortDateString()}},
                {"-AccountNumber-", new List<string> {accountNumber}},
                {"-MeterNumber-", new List<string> {billingModel != null ? billingModel.MeterId : ""}},
                {"-ServiceAddress-", new List<string>{address}},
                {"-DayThreshold-", new List<string> {$"{customInsightModel.ThresholdValue:0.00}"}},
                {"-CommodityType-", new List<string> {commodity.GetDescription()}},
                {"-UOM-", new List<string> {uom}}
            };
            return identifiers;
        }

        private Dictionary<string, List<string>> GetServiceLevelTieredThresholdApproachingEmailSubstitutions(CustomerModel customerModel, CalculationModel calculation, BillingModel billingModel, List<TierBoundary> tierBoundaries, DateTime asOfDate, int unmaskedAccountIdEndingDigit, IList<ClientUOM> uomContentList, List<ClientTextContent> textContentList)
        {
            var accountNumber = calculation.AccountId;
            if (unmaskedAccountIdEndingDigit > 0)
            {
                accountNumber = AccountLookupModel.GetMaskedAccountId(accountNumber, unmaskedAccountIdEndingDigit);
            }

            var tieredBoundryApproching =
                            tierBoundaries.OrderBy(p => p.Threshold)
                                .FirstOrDefault(p => p.Threshold > calculation.Usage);
            var nextTieredBountryApproching =
                tierBoundaries.OrderBy(p => p.Threshold)
                    .FirstOrDefault(p => p.Threshold > tieredBoundryApproching?.Threshold);

            var commodity = Enums.CommodityType.Undefined;
            if (calculation.CommodityId != null)
            {
                commodity = (Enums.CommodityType)calculation.CommodityId;
            }

            int? unit = billingModel?.UOMId ?? calculation.Unit;
            var uomId = 99;
            if (unit.HasValue)
            {
                uomId = unit.Value;
            }
            // if the calcution is done in a different UOM from bill's
            // convert the threshold into bill's UOM for the notification
            double? currentUsageThreshold;
            double? nextTierUsage;
            double? currentUsage;
            double? avgDailyUsage;
            if (unit.HasValue && calculation.Unit.HasValue && unit != calculation.Unit)
            {
                var conversionFactor = Extensions.GetConversionFactor(customerModel.ClientId, calculation.Unit.Value,
                    unit.Value);
                currentUsageThreshold = tieredBoundryApproching?.Threshold * conversionFactor;
                nextTierUsage = nextTieredBountryApproching?.Threshold * conversionFactor;
                currentUsage = calculation.Usage * conversionFactor;
                avgDailyUsage = calculation.AverageDailyUsage * conversionFactor;
            }
            else
            {
                currentUsageThreshold = tieredBoundryApproching?.Threshold;
                nextTierUsage = nextTieredBountryApproching?.Threshold;
                currentUsage = calculation.Usage;
                avgDailyUsage = calculation.AverageDailyUsage;
            }
            var address = string.Empty;

            if (customerModel != null)
            {
                if (!string.IsNullOrEmpty(customerModel.AddressLine1))
                    address = address + customerModel.AddressLine1;

                if (!string.IsNullOrEmpty(customerModel.AddressLine2))
                {
                    address = !string.IsNullOrEmpty(address)
                        ? address + ", " + customerModel.AddressLine2
                        : address + customerModel.AddressLine2;
                }

                if (!string.IsNullOrEmpty(customerModel.AddressLine3))
                {
                    address = !string.IsNullOrEmpty(address)
                        ? address + ", " + customerModel.AddressLine3
                        : address + customerModel.AddressLine3;
                }
            }

            var identifiers = new Dictionary<string, List<string>>();

            // get uom content
            var uomKey = ((Enums.UomType)uomId).ToString();
            var uom = Extensions.GetUomTextContent(uomKey, uomContentList, textContentList);
            if (string.IsNullOrEmpty(uom))
                uom = uomKey;
            identifiers.Add("-FirstName-",
                new List<string>
                {
                    customerModel != null ? customerModel.FirstName : ""
                });
            identifiers.Add("-LastName-", new List<string> { customerModel != null ? customerModel.LastName : "" });
            identifiers.Add("-AsOfDate-", new List<string> { asOfDate.ToShortDateString() });
            identifiers.Add("-NumberOfDays-",
                new List<string>
                {
                    ((asOfDate.Date - Convert.ToDateTime(calculation.BillCycleStartDate).Date).Days + 1).ToString()
                });
            identifiers.Add("-UsagePerDay-", new List<string> { $"{avgDailyUsage:0.00}" });
            identifiers.Add("-AccountNumber-", new List<string> { accountNumber });
            identifiers.Add("-AccountAddress-", new List<string> { address });
            identifiers.Add("-CurrentUsage-", new List<string> { $"{currentUsage:0.00}" });
            identifiers.Add("-UOM-", new List<string> { uom });
            identifiers.Add("-CurrentUsageThreshold-", new List<string> { $"{currentUsageThreshold ?? 0:0.00}" });   //If null then set to 0
            identifiers.Add("-NextTierUsage-", new List<string> { $"{nextTierUsage ?? 0:0.00}" });
            identifiers.Add("-CommodityType-",
                new List<string> { commodity.GetDescription() });
            return identifiers;
        }

        private Dictionary<string, List<string>> GetServiceLevelTieredThresholdExceedEmailSubstitutions(CustomerModel customerModel, CalculationModel calculation, BillingModel billingModel, List<TierBoundary> tierBoundaries, DateTime asOfDate, int unmaskedAccountIdEndingDigit, IList<ClientUOM> uomContentList, List<ClientTextContent> textContentList)
        {
            var tieredBoundryExceeding =
                tierBoundaries.OrderByDescending(p => p.Threshold)
                    .FirstOrDefault(p => p.Threshold < calculation.Usage);
            var nextTieredBoundryExceeding =
                tierBoundaries.OrderBy(p => p.Threshold)
                    .FirstOrDefault(p => p.Threshold > tieredBoundryExceeding?.Threshold);
            var baseOrTier = CE.RateModel.Enums.BaseOrTier.Undefined;
            if (tieredBoundryExceeding != null)
            {
                baseOrTier = tieredBoundryExceeding.BaseOrTier;
            }

            var commodity = Enums.CommodityType.Undefined;
            if (calculation.CommodityId != null)
            {
                commodity = (Enums.CommodityType)calculation.CommodityId;
            }

            int? unit = billingModel?.UOMId ?? calculation.Unit;
            var uomId = 99;
            if (unit.HasValue)
            {
                uomId = unit.Value;
            }

            var address = string.Empty;

            if (customerModel != null)
            {
                if (!string.IsNullOrEmpty(customerModel.AddressLine1))
                    address = address + customerModel.AddressLine1;

                if (!string.IsNullOrEmpty(customerModel.AddressLine2))
                {
                    address = !string.IsNullOrEmpty(address)
                        ? address + ", " + customerModel.AddressLine2
                        : address + customerModel.AddressLine2;
                }

                if (!string.IsNullOrEmpty(customerModel.AddressLine3))
                {
                    address = !string.IsNullOrEmpty(address)
                        ? address + ", " + customerModel.AddressLine3
                        : address + customerModel.AddressLine3;
                }
            }
            // if the calcution is done in a different UOM from bill's
            // convert the threshold into bill's UOM for the notification
            double? previousTierUsage;
            double? currentTierUsage;
            double? currentUsage;
            if (unit.HasValue && calculation.Unit.HasValue && unit != calculation.Unit)
            {
                var conversionFactor = Extensions.GetConversionFactor(customerModel.ClientId, calculation.Unit.Value,
                    unit.Value);
                previousTierUsage = tieredBoundryExceeding?.Threshold * conversionFactor;
                currentTierUsage = nextTieredBoundryExceeding?.Threshold * conversionFactor;
                currentUsage = calculation.Usage * conversionFactor;
            }
            else
            {
                previousTierUsage = tieredBoundryExceeding?.Threshold;
                currentTierUsage = nextTieredBoundryExceeding?.Threshold;
                currentUsage = calculation.Usage;
            }

            var accountNumber = calculation.AccountId;
            if (unmaskedAccountIdEndingDigit > 0)
            {
                accountNumber = AccountLookupModel.GetMaskedAccountId(accountNumber, unmaskedAccountIdEndingDigit);
            }

            var identifiers = new Dictionary<string, List<string>>();

            // get uom content
            var uomKey = ((Enums.UomType)uomId).ToString();
            var uom = Extensions.GetUomTextContent(uomKey, uomContentList, textContentList);
            if (string.IsNullOrEmpty(uom))
                uom = uomKey;
            identifiers.Add("-FirstName-",
                new List<string>
                {
                    customerModel != null ? customerModel.FirstName : ""
                });
            identifiers.Add("-LastName-", new List<string> { customerModel != null ? customerModel.LastName : "" });
            identifiers.Add("-AsOfDate-", new List<string> { asOfDate.ToShortDateString() });
            identifiers.Add("-TierNumber-", new List<string> { baseOrTier.GetDescription() });
            identifiers.Add("-DateThresholdExceeded-", new List<string> { asOfDate.ToShortDateString() });
            identifiers.Add("-AccountNumber-", new List<string> { accountNumber });
            identifiers.Add("-AccountAddress-", new List<string> { address });
            identifiers.Add("-CurrentUsage-", new List<string> { $"{currentUsage:0.00}" });
            identifiers.Add("-UOM-", new List<string> { uom });
            identifiers.Add("-NewTieredRateThreshold-", new List<string> { $"{tieredBoundryExceeding?.Threshold ?? 0:0.00}" });
            identifiers.Add("-PreviousTierUsage-", new List<string> { $"{previousTierUsage ?? 0:0.00}" });
            identifiers.Add("-CurrentTierUsage-", new List<string> { $"{currentTierUsage ?? 0:0.00}" });
            identifiers.Add("-CommodityType-",
                new List<string> { commodity.GetDescription() });
            return identifiers;
        }
    }
}
