﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AO.BusinessContracts;
using AO.DataAccess.Contracts;
using AO.Entities;
using AutoMapper;
using CE.AO.Models;

namespace AO.Business
{
    /// <summary>
    /// BillingCycleSchedule class is used to fetch the data for BillCycleScheduleModel model
    /// </summary>
    public class BillingCycleSchedule : IBillingCycleSchedule
    {
        public BillingCycleSchedule(LogModel logModel, Lazy<ICassandraRepository> cassandraRepository)
        {
            LogModel = logModel;
            CassandraRepository = cassandraRepository;
        }

        public LogModel LogModel { get; set; }

        private Lazy<ICassandraRepository> CassandraRepository { get; }

        /// <summary>
        /// To fetch billing cycle schedule by date
        /// </summary>
        /// <param name="clientId">Client Id of model to be fetched</param>
        /// <param name="billCycleId">Bill cycle id of model to be fetched</param>
        /// <param name="date">Date of model to be fetched</param>
        /// <returns></returns>
        public BillCycleScheduleModel GetBillingCycleScheduleByDateAsync(int clientId, string billCycleId, DateTime date)
        {
            IEnumerable<BillCycleScheduleEntity> billingCycleSchedule =
                CassandraRepository.Value.GetAsync<BillCycleScheduleEntity>(
                    Constants.CassandraTableNames.BillCycleSchedule, 'q',
                    a =>
                        a.ClientId == clientId.ToString() && a.BillCycleScheduleId == billCycleId).Result;
            if (billingCycleSchedule == null || !billingCycleSchedule.Any()) return null;

            var billingSchedule =
                billingCycleSchedule
                    .FirstOrDefault(a => Convert.ToDateTime(a.BeginDate) <= date && Convert.ToDateTime(a.EndDate) >= date);

            return Mapper.Map<BillCycleScheduleEntity, BillCycleScheduleModel>(billingSchedule);
        }

        /// <summary>
        /// To insert or update BillCycleScheduleModel to database
        /// </summary>
        /// <param name="model">BillCycleScheduleModel to be inserted</param>
        /// <returns>true</returns>
        public async Task<bool> InsertOrMergeBillingCycleScheduleAsync(BillCycleScheduleModel model)
        {
            var entity = Mapper.Map<BillCycleScheduleModel, BillCycleScheduleEntity>(model);
            await
                CassandraRepository.Value.InsertOrUpdateAsync(entity, Constants.CassandraTableNames.BillCycleSchedule)
                    .ConfigureAwait(false);
            return await Task.Factory.StartNew(() => true);
        }

        /// <summary>
        /// Delete Client BillCycleSchedule Form billing Cycle Schedule Table
        /// </summary>
        /// <param name="clientId"></param>
        /// <param name="billCycleScheduleId"></param>
        /// <param name="beginDate"></param>
        /// <param name="endDate"></param>
        /// <returns></returns>
        public async Task<bool> DeleteHistoryBillingCycleScheduleAsyc(string clientId, string billCycleScheduleId,
            string beginDate, string endDate)
        {
            await
                CassandraRepository.Value.DeleteAsync<BillCycleScheduleEntity>(
                    Constants.CassandraTableNames.BillCycleSchedule,
                    a =>
                        a.ClientId == clientId && a.BillCycleScheduleId == billCycleScheduleId &&
                        a.BeginDate == beginDate && a.EndDate == endDate).ConfigureAwait(false);
            return await Task.Factory.StartNew(() => true);
        }
    }
}
