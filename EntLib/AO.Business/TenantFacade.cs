﻿using System.Collections.Generic;
using AO.BusinessContracts;
using AO.DataAccess.Contracts;
using AO.Domain;

namespace AO.Business
{
    /// <summary>
    /// Class is used to get list of client id and name
    /// </summary>
    public class TenantFacade : ITenantFacade
    {
        private readonly ITenantRepository _tenantRepository;

        public TenantFacade(ITenantRepository tenantRepository)
        {
            _tenantRepository = tenantRepository;
        }

        /// <summary>
        /// Method is used to return list of Clients
        /// </summary>
        /// <returns>List of clients</returns>
        public List<Client> GetClients()
        {
            return _tenantRepository.GetClients();
        }
    }
}