﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using AO.BusinessContracts;
using AO.DataAccess.Contracts;
using AO.Entities;
using AutoMapper;
using CE.AO.Models;

namespace AO.Business
{
	/// <summary>
	/// MeterHistoryByMeter class is used to fetch/insert/update data from/to the database
	/// </summary>
	public class MeterHistoryByMeter : IMeterHistoryByMeter
	{
		public MeterHistoryByMeter(LogModel logModel, Lazy<ICassandraRepository> cassandraRepository)
		{
			LogModel = logModel;
			CassandraRepository = cassandraRepository;
		}

		public LogModel LogModel { get; set; }

		private Lazy<ICassandraRepository> CassandraRepository { get; }

		/// <summary>
		/// This method is used to fetch the MeterHistoryByMeter details from database.
		/// </summary>
		///<param name="pageSize"></param>
		/// <param name="token"></param>
		/// <param name="clientId">Client Id of the data to be fetched</param>
		/// <returns>List of MeterHistoryByMeterModel</returns>
		public IEnumerable<MeterHistoryByMeterModel> GetMeterHistoryByMeterList(int pageSize, ref byte[] token, int clientId)
		{
			IList<MeterHistoryByMeterEntity> meterHistoryByMeters = CassandraRepository.Value.Get<MeterHistoryByMeterEntity>(Constants.CassandraTableNames.MeterHistoryByMeter, pageSize,
					ref token, c => c.ClientId == clientId);
			IList<MeterHistoryByMeterModel> meterHistoryByMeterModelList = Mapper.Map<IList<MeterHistoryByMeterEntity>, IList<MeterHistoryByMeterModel>>(meterHistoryByMeters);
            return meterHistoryByMeterModelList;
		}

		/// <summary>
		/// To insert/update the MeterHistoryByMeterModel to the database
		/// </summary>
		/// <param name="model">MeterHistoryByMeterModel model which is to be inserted/updated</param>
		/// <returns>True</returns>
		public async Task<bool> InsertOrMergeMeterHistoryByMeterAsync(MeterHistoryByMeterModel model)
		{
			var entityModel = Mapper.Map<MeterHistoryByMeterModel, MeterHistoryByMeterEntity>(model);
			await CassandraRepository.Value.InsertOrUpdateAsync(entityModel, Constants.CassandraTableNames.MeterHistoryByMeter).ConfigureAwait(false);
			return await Task.Factory.StartNew(() => true);
		}

		/// <summary>
		/// To delete the MeterHistoryByMeter data in batch to the database
		/// </summary>
		/// <param name="meterHistoryByMeter">MeterHistoryByMeter data which is to be deleted</param>
		public async Task DeleteMeterHistoryByMeterAsync(MeterHistoryByMeterModel meterHistoryByMeter)
		{
			var entities = Mapper.Map<MeterHistoryByMeterModel, MeterHistoryByMeterEntity>(meterHistoryByMeter);
			await CassandraRepository.Value.DeleteAsync<MeterHistoryByMeterEntity>(Constants.CassandraTableNames.MeterHistoryByMeter,
				v => v.ClientId == entities.ClientId && v.MeterSerialNumber == entities.MeterSerialNumber);
		}
	}
}
