﻿using System;
using AO.Entities;
using AutoMapper;
using CE.AO.Models;
using CE.AO.Models.RawFileModels;
using AO.DTO;
using AO.Domain;
using AccountUpdatesEntity = AO.Entities.AccountUpdatesEntity;
using Ami15MinuteIntervalEntity = AO.Entities.Ami15MinuteIntervalEntity;
using Ami30MinuteIntervalEntity = AO.Entities.Ami30MinuteIntervalEntity;
using Ami60MinuteIntervalEntity = AO.Entities.Ami60MinuteIntervalEntity;
using BillCycleScheduleEntity = AO.Entities.BillCycleScheduleEntity;
using BillingEntity = AO.Entities.BillingEntity;
using CalculationEntity = AO.Entities.CalculationEntity;
using ClientAccountEntity = AO.Entities.ClientAccountEntity;
using CustomerEntity = AO.Entities.CustomerEntity;
using CustomerReportEntity = AO.Entities.CustomerReportEntity;
using InsightEntity = AO.Entities.InsightEntity;
using NotificationEntity = AO.Entities.NotificationEntity;
using PremiseEntity = AO.Entities.PremiseEntity;
using SmsTemplateEntity = AO.Entities.SmsTemplateEntity;
using SubscriptionEntity = AO.Entities.SubscriptionEntity;
using ConsumptionByMeterEntity = AO.Entities.ConsumptionByMeterEntity;
using ConsumptionByMeterEntityLight = AO.Entities.ConsumptionByMeterEntityLight;
using TrumpiaKeywordConfigurationEntity = AO.Entities.TrumpiaKeywordConfigurationEntity;
using CE.AO.Logging;

namespace AO.Business
{
    /// <summary>
    /// AutoMapper configuration are configured in this class
    /// </summary>
    public static class AutoMapperConfig
    {
        private static readonly object PadLock = new object();
        private static bool _isInitialized;

        /// <summary>
        /// AutoMapper configuration are configured in this method
        /// </summary>
        public static void AutoMapperMappings()
        {
            try
            {
                if (!_isInitialized)
                {
                    lock (PadLock)
                    {
                        if (!_isInitialized)
                        {

                            Mapper.CreateMap<string, bool?>()
                                .ConvertUsing(str => !string.IsNullOrWhiteSpace(str) ? Convert.ToBoolean(str.Trim()) : (bool?)null);

                            Mapper.CreateMap<string, DateTime?>()
                                .ConvertUsing(str => !string.IsNullOrWhiteSpace(str) ? Convert.ToDateTime(str.Trim()) : (DateTime?)null);

                            Mapper.CreateMap<string, double?>()
                                .ConvertUsing(str => !string.IsNullOrWhiteSpace(str) ? Convert.ToDouble(str.Trim()) : (double?)null);

                            Mapper.CreateMap<string, Guid?>()
                                .ConvertUsing(str => !string.IsNullOrWhiteSpace(str) ? Guid.Parse(str.Trim()) : (Guid?)null);

                            Mapper.CreateMap<string, int?>()
                                .ConvertUsing(str => !string.IsNullOrWhiteSpace(str) ? Convert.ToInt32(str.Trim()) : (int?)null);

                            Mapper.CreateMap<string, string>()
                                .ConvertUsing(str => !string.IsNullOrWhiteSpace(str) ? str.Trim() : str);

                            #region AmiRequestModel

                            Mapper.CreateMap<AmiRequestModel, BillingModel>()
                                .ForMember(dest => dest.AccountId, opts => opts.MapFrom(src => src.AccountNumber))
                                .ForMember(dest => dest.ServiceContractId, opts => opts.MapFrom(src => src.ServiceId))
                                .ForMember(dest => dest.CommodityId, opts => opts.MapFrom(src => Convert.ToInt32(src.FuelId)))
                                .ForMember(dest => dest.StartDate, opts => opts.Ignore())
                                .ForMember(dest => dest.EndDate, opts => opts.Ignore())
                                .ForSourceMember(dest => dest.BillingPeriodStartDate, opts => opts.Ignore())
                                .ForAllUnmappedMembers(o => o.Ignore());

                            Mapper.CreateMap<BillingModel, AmiRequestModel>()
                                .ForMember(dest => dest.AccountNumber, opts => opts.MapFrom(src => src.AccountId))
                                .ForMember(dest => dest.ServiceId, opts => opts.MapFrom(src => src.ServiceContractId))
                                .ForMember(dest => dest.FuelId, opts => opts.MapFrom(src => src.CommodityId))
                                .ForMember(dest => dest.StartDate, opts => opts.Ignore())
                                .ForMember(dest => dest.EndDate, opts => opts.Ignore())
                                .ForMember(dest => dest.BillingPeriodStartDate, opts => opts.Ignore())
                                .ForAllUnmappedMembers(o => o.Ignore());

                            #endregion

                            #region AccountMeterLookup

                            Mapper.CreateMap<AccountLookupModel, BillingCustomerPremiseModel>()
                                .ForMember(dest => dest.customer_id, opts => opts.MapFrom(src => src.CustomerId))
                                .ForMember(dest => dest.meter_id, opts => opts.MapFrom(src => src.MeterId))
                                .ForMember(dest => dest.Service_contract, opts => opts.MapFrom(src => src.ServiceContractId))
                                .ForMember(dest => dest.account_id, opts => opts.MapFrom(src => src.AccountId))
                                .ForAllUnmappedMembers(o => o.Ignore());

                            Mapper.CreateMap<BillingCustomerPremiseModel, AccountLookupModel>()
                               .ForMember(dest => dest.CustomerId, opts => opts.MapFrom(src => src.customer_id))
                               .ForMember(dest => dest.MeterId, opts => opts.MapFrom(src => src.meter_id))
                               .ForMember(dest => dest.ServiceContractId, opts => opts.MapFrom(src => src.Service_contract))
                               .ForMember(dest => dest.AccountId, opts => opts.MapFrom(src => src.account_id))
                               .ForAllUnmappedMembers(o => o.Ignore());
                            #endregion

                            #region NccAmiReadingModel

                            Mapper.CreateMap<NccAmiReadingModel, RawNccAmiReadingModel>()
                                .ForMember(dest => dest.METER_ID, opts => opts.MapFrom(src => src.MeterId))
                                .ForMember(dest => dest.TRANSPONDER_ID, opts => opts.MapFrom(src => src.TransponderId))
                                .ForMember(dest => dest.TRANSPONDER_PORT, opts => opts.MapFrom(src => src.TransponderPort))
                                .ForMember(dest => dest.CUSTOMER_ID, opts => opts.MapFrom(src => src.CustomerId))
                                .ForMember(dest => dest.READING_VALUE, opts => opts.MapFrom(src => src.ReadingValue))
                                .ForMember(dest => dest.UNIT_OF_MEASURE, opts => opts.MapFrom(src => src.UOMId))
                                .ForMember(dest => dest.READING_DATETIME, opts => opts.MapFrom(src => src.AmiTimeStamp))
                                .ForMember(dest => dest.TIMEZONE, opts => opts.MapFrom(src => src.Timezone))
                                .ForMember(dest => dest.BATTERY_VOLTAGE, opts => opts.MapFrom(src => src.BatteryVoltage))
                                .ForMember(dest => dest.COMMODITY_ID, opts => opts.MapFrom(src => src.CommodityId))
                                .ForAllUnmappedMembers(o => o.Ignore());

                            Mapper.CreateMap<RawNccAmiReadingModel, NccAmiReadingModel>()
                                .ForMember(dest => dest.MeterId, opts => opts.MapFrom(src => src.METER_ID))
                                .ForMember(dest => dest.TransponderId, opts => opts.MapFrom(src => Convert.ToInt32(src.TRANSPONDER_ID)))
                                .ForMember(dest => dest.TransponderPort, opts => opts.MapFrom(src => Convert.ToInt32(src.TRANSPONDER_PORT)))
                                .ForMember(dest => dest.CustomerId, opts => opts.MapFrom(src => src.CUSTOMER_ID))
                                .ForMember(dest => dest.ReadingValue, opts => opts.MapFrom(src => Convert.ToDouble(src.READING_VALUE)))
                                .ForMember(dest => dest.UOMId, opts => opts.MapFrom(src => Convert.ToInt32(src.UNIT_OF_MEASURE)))
                                .ForMember(dest => dest.AmiTimeStamp, opts => opts.MapFrom(src => Convert.ToDateTime(src.READING_DATETIME)))
                                .ForMember(dest => dest.Timezone, opts => opts.MapFrom(src => src.TIMEZONE))
                                .ForMember(dest => dest.BatteryVoltage, opts => opts.MapFrom(src => src.BATTERY_VOLTAGE))
                                .ForMember(dest => dest.CommodityId, opts => opts.MapFrom(src => Convert.ToInt32(src.COMMODITY_ID)))
                                .ForAllUnmappedMembers(o => o.Ignore());

                            Mapper.CreateMap<RawBaseReadModel, NccAmiReadingModel>()
                                .ForMember(dest => dest.MeterId, opts => opts.MapFrom(src => src.METER_ID))
                                .ForMember(dest => dest.TransponderId, opts => opts.MapFrom(src => Convert.ToInt32(src.TRANSPONDER_ID)))
                                .ForMember(dest => dest.TransponderPort, opts => opts.MapFrom(src => Convert.ToInt32(src.TRANSPONDER_PORT)))
                                .ForMember(dest => dest.CustomerId, opts => opts.MapFrom(src => src.CUSTOMER_ID))
                                .ForMember(dest => dest.ReadingValue, opts => opts.MapFrom(src => Convert.ToDouble(src.READING_VALUE)))
                                .ForMember(dest => dest.UOMId, opts => opts.MapFrom(src => Convert.ToInt32(src.UNIT_OF_MEASURE)))
                                .ForMember(dest => dest.AmiTimeStamp, opts => opts.MapFrom(src => Convert.ToDateTime(src.READING_DATETIME)))
                                .ForMember(dest => dest.Timezone, opts => opts.MapFrom(src => src.TIMEZONE))
                                .ForMember(dest => dest.BatteryVoltage, opts => opts.MapFrom(src => src.BATTERY_VOLTAGE))
                                .ForMember(dest => dest.CommodityId, opts => opts.MapFrom(src => Convert.ToInt32(src.COMMODITY_ID)))
                                .ForAllUnmappedMembers(o => o.Ignore());

                            #endregion

                            #region MeterReading

                            Mapper.CreateMap<NccAmiReadingModel, MeterReadingEntity>()
                                .ForAllUnmappedMembers(o => o.Ignore());

                            Mapper.CreateMap<MeterReadingEntity, NccAmiReadingModel>()
                                .ForAllUnmappedMembers(o => o.Ignore());

                            #endregion

                            #region AmiReading Internal

                            Mapper.CreateMap<BusinessContracts.Preprocessing.AmiReading, TallAmiModel>()
                               .ForAllUnmappedMembers(o => o.Ignore());

                            Mapper.CreateMap<TallAmiModel, BusinessContracts.Preprocessing.AmiReading>()
                                .ForAllUnmappedMembers(o => o.Ignore());

                            Mapper.CreateMap<BusinessContracts.Preprocessing.AmiReading, NccAmiReadingModel>()
                                .ForMember(dest => dest.TransponderId, opts => opts.MapFrom(src => Convert.ToInt32(src.TransponderId)))
                                .ForMember(dest => dest.TransponderPort, opts => opts.MapFrom(src => Convert.ToInt32(src.TransponderPort)))
                               .ForAllUnmappedMembers(o => o.Ignore());

                            Mapper.CreateMap<NccAmiReadingModel, BusinessContracts.Preprocessing.AmiReading>()
                                .ForAllUnmappedMembers(o => o.Ignore());

                            Mapper.CreateMap<BusinessContracts.Preprocessing.AmiReading, Ami60MinuteIntervalModel>()
                              .ForAllUnmappedMembers(o => o.Ignore());

                            Mapper.CreateMap<Ami60MinuteIntervalModel, BusinessContracts.Preprocessing.AmiReading>()
                                .ForAllUnmappedMembers(o => o.Ignore());
                            #endregion

                            #region Billing

                            Mapper.CreateMap<BillingCustomerPremiseModel, RawBillingModel>()
                                .ForMember(dest => dest.bill_enddate, opt => opt.NullSubstitute(null))
                                .ForMember(dest => dest.service_read_date, opt => opt.NullSubstitute(null))
                                .ForAllUnmappedMembers(o => o.Ignore());
                            
                            Mapper.CreateMap<RawBillingModel, BillingCustomerPremiseModel>()
                                .ForMember(dest => dest.bill_enddate, opt => opt.MapFrom(src => string.IsNullOrEmpty(src.bill_enddate) ? (DateTime?)null : Convert.ToDateTime(src.bill_enddate)))
                                .ForMember(dest => dest.service_read_date, opt => opt.MapFrom(src => string.IsNullOrEmpty(src.bill_enddate) ? (DateTime?)null : Convert.ToDateTime(src.service_read_date)))
                                .ForMember(dest => dest.active_date, opt => opt.MapFrom(src => string.IsNullOrEmpty(src.active_date) ? (DateTime?)null : Convert.ToDateTime(src.active_date)))
                                .ForMember(dest => dest.inactive_date, opt => opt.MapFrom(src => string.IsNullOrEmpty(src.inactive_date) ? (DateTime?)null : Convert.ToDateTime(src.inactive_date)))
                                .ForMember(dest => dest.meter_units, opt => opt.MapFrom(src => string.IsNullOrEmpty(src.meter_units) ? (int?)null : Convert.ToInt32(src.meter_units)))
                                .ForMember(dest => dest.usage_value, opt => opt.MapFrom(src => string.IsNullOrEmpty(src.usage_value) ? (double?)null : Convert.ToDouble(src.usage_value)))
                                .ForMember(dest => dest.usage_charge, opt => opt.MapFrom(src => string.IsNullOrEmpty(src.usage_charge) ? (double?)null : Convert.ToDouble(src.usage_charge)))
                                .ForMember(dest => dest.bill_days, opt => opt.MapFrom(src => Convert.ToInt32(src.bill_days)))
                                .ForMember(dest => dest.ClientId, opt => opt.MapFrom(src => string.IsNullOrEmpty(src.ClientId) ? (int?)null : Convert.ToInt32(src.ClientId)))
                                .ForMember(dest => dest.service_commodity, opt => opt.MapFrom(src => string.IsNullOrEmpty(src.service_commodity) ? (int?)null : Convert.ToInt32(src.service_commodity)))
                                .ForMember(dest => dest.billperiod_type, opt => opt.MapFrom(src => string.IsNullOrEmpty(src.billperiod_type) ? (int?)null : Convert.ToInt32(src.billperiod_type)))
                                .ForAllUnmappedMembers(o => o.Ignore());

                            Mapper.CreateMap<BillingEntity, BillingModel>()
                                .ForAllUnmappedMembers(o => o.Ignore());

                            Mapper.CreateMap<BillingModel, BillingEntity>()
                                .ForAllUnmappedMembers(o => o.Ignore());

                            Mapper.CreateMap<BillingModel, BillingCustomerPremiseModel>()
                                .ForMember(dest => dest.bill_enddate, opt => opt.NullSubstitute(null))
                                .ForMember(dest => dest.service_read_date, opt => opt.NullSubstitute(null))
                                .ForMember(dest => dest.account_id, opts => opts.MapFrom(src => src.AccountId))
                                .ForMember(dest => dest.customer_id, opts => opts.MapFrom(src => src.CustomerId))
                                .ForMember(dest => dest.premise_id, opts => opts.MapFrom(src => src.PremiseId))
                                .ForMember(dest => dest.inactive_date, opts => opts.MapFrom(src => src.AmiEndDate))
                                .ForMember(dest => dest.read_cycle, opts => opts.MapFrom(src => src.BillCycleScheduleId))
                                .ForMember(dest => dest.service_point_id, opts => opts.MapFrom(src => src.ServicePointId))
                                .ForMember(dest => dest.rate_code, opts => opts.MapFrom(src => src.RateClass))
                                .ForMember(dest => dest.meter_type, opts => opts.MapFrom(src => src.MeterType))
                                .ForMember(dest => dest.meter_units, opts => opts.MapFrom(src => src.UOMId))
                                .ForMember(dest => dest.usage_value, opts => opts.MapFrom(src => src.TotalUsage))
                                .ForMember(dest => dest.bill_enddate, opts => opts.MapFrom(src => src.EndDate))
                                .ForMember(dest => dest.bill_days, opts => opts.MapFrom(src => src.BillDays))
                                .ForMember(dest => dest.is_estimate, opts => opts.MapFrom(src => src.ReadQuality))
                                .ForMember(dest => dest.usage_charge, opts => opts.MapFrom(src => src.TotalCost))
                                .ForMember(dest => dest.service_commodity, opts => opts.MapFrom(src => src.CommodityId))
                                .ForMember(dest => dest.meter_id, opts => opts.MapFrom(src => src.MeterId))
                                .ForMember(dest => dest.billperiod_type, opts => opts.MapFrom(src => src.BillPeriodType))
                                .ForMember(dest => dest.meter_replaces_meterid, opts => opts.MapFrom(src => src.ReplacedMeterId))
                                .ForMember(dest => dest.service_read_date, opts => opts.MapFrom(src => src.ReadDate))
                                .ForMember(dest => dest.Service_contract, opts => opts.MapFrom(src => src.ServiceContractId))
                                .ForAllUnmappedMembers(o => o.Ignore());

                            Mapper.CreateMap<BillingCustomerPremiseModel, BillingModel>()
                                .ForMember(dest => dest.AccountId, opts => opts.MapFrom(src => src.account_id))
                                .ForMember(dest => dest.CustomerId, opts => opts.MapFrom(src => src.customer_id))
                                .ForMember(dest => dest.PremiseId, opts => opts.MapFrom(src => src.premise_id))
                                .ForMember(dest => dest.AmiEndDate, opts => opts.MapFrom(src => src.inactive_date))
                                .ForMember(dest => dest.BillCycleScheduleId, opts => opts.MapFrom(src => src.read_cycle))
                                .ForMember(dest => dest.ServicePointId, opts => opts.MapFrom(src => src.service_point_id))
                                .ForMember(dest => dest.RateClass, opts => opts.MapFrom(src => src.rate_code))
                                .ForMember(dest => dest.MeterType, opts => opts.MapFrom(src => src.meter_type))
                                .ForMember(dest => dest.UOMId, opts => opts.MapFrom(src => src.meter_units))
                                .ForMember(dest => dest.TotalUsage, opts => opts.MapFrom(src => src.usage_value))
                                .ForMember(dest => dest.EndDate, opts => opts.MapFrom(src => src.bill_enddate))
                                .ForMember(dest => dest.BillDays, opts => opts.MapFrom(src => src.bill_days))
                                .ForMember(dest => dest.ReadQuality, opts => opts.MapFrom(src => src.is_estimate))
                                .ForMember(dest => dest.TotalCost, opts => opts.MapFrom(src => src.usage_charge))
                                .ForMember(dest => dest.CommodityId, opts => opts.MapFrom(src => src.service_commodity))
                                .ForMember(dest => dest.MeterId, opts => opts.MapFrom(src => src.meter_id))
                                .ForMember(dest => dest.BillPeriodType, opts => opts.MapFrom(src => src.billperiod_type))
                                .ForMember(dest => dest.ReplacedMeterId, opts => opts.MapFrom(src => src.meter_replaces_meterid))
                                .ForMember(dest => dest.ReadDate, opts => opts.MapFrom(src => src.service_read_date))
                                .ForMember(dest => dest.ServiceContractId, opts => opts.MapFrom(src => src.Service_contract))
                                .ForAllUnmappedMembers(o => o.Ignore());
                            #endregion

                            #region Customer

                            Mapper.CreateMap<CustomerEntity, CustomerModel>()
                                .ForAllUnmappedMembers(o => o.Ignore());

                            Mapper.CreateMap<CustomerModel, CustomerEntity>()
                                .ForAllUnmappedMembers(o => o.Ignore());

                            Mapper.CreateMap<CustomerModel, CustomerReportEntity>()
                                .ForAllUnmappedMembers(o => o.Ignore());

                            Mapper.CreateMap<CustomerModel, BillingCustomerPremiseModel>()
                                .ForMember(dest => dest.customer_id, opts => opts.MapFrom(src => src.CustomerId))
                                .ForMember(dest => dest.mail_address_line_1, opts => opts.MapFrom(src => src.AddressLine1))
                                .ForMember(dest => dest.mail_address_line_2, opts => opts.MapFrom(src => src.AddressLine2))
                                .ForMember(dest => dest.mail_address_line_3, opts => opts.MapFrom(src => src.AddressLine3))
                                .ForMember(dest => dest.mail_city, opts => opts.MapFrom(src => src.City))
                                .ForMember(dest => dest.mail_state, opts => opts.MapFrom(src => src.State))
                                .ForMember(dest => dest.mail_zip_code, opts => opts.MapFrom(src => src.PostalCode))
                                .ForMember(dest => dest.first_name, opts => opts.MapFrom(src => src.FirstName))
                                .ForMember(dest => dest.last_name, opts => opts.MapFrom(src => src.LastName))
                                .ForMember(dest => dest.phone_1, opts => opts.MapFrom(src => src.Phone1))
                                .ForMember(dest => dest.phone_2, opts => opts.MapFrom(src => src.Phone2))
                                .ForMember(dest => dest.email, opts => opts.MapFrom(src => src.EmailAddress))
                                .ForMember(dest => dest.customer_type, opts => opts.MapFrom(src => src.CustomerType))
                                .ForAllUnmappedMembers(o => o.Ignore());

                            Mapper.CreateMap<BillingCustomerPremiseModel, CustomerModel>()
                               .ForMember(dest => dest.CustomerId, opts => opts.MapFrom(src => src.customer_id))
                               .ForMember(dest => dest.AddressLine1, opts => opts.MapFrom(src => src.mail_address_line_1))
                               .ForMember(dest => dest.AddressLine2, opts => opts.MapFrom(src => src.mail_address_line_2))
                               .ForMember(dest => dest.AddressLine3, opts => opts.MapFrom(src => src.mail_address_line_3))
                               .ForMember(dest => dest.City, opts => opts.MapFrom(src => src.mail_city))
                               .ForMember(dest => dest.State, opts => opts.MapFrom(src => src.mail_state))
                               .ForMember(dest => dest.PostalCode, opts => opts.MapFrom(src => src.mail_zip_code))
                               .ForMember(dest => dest.FirstName, opts => opts.MapFrom(src => src.first_name))
                               .ForMember(dest => dest.LastName, opts => opts.MapFrom(src => src.last_name))
                               .ForMember(dest => dest.Phone1, opts => opts.MapFrom(src => src.phone_1))
                               .ForMember(dest => dest.Phone2, opts => opts.MapFrom(src => src.phone_2))
                               .ForMember(dest => dest.EmailAddress, opts => opts.MapFrom(src => src.email))
                               .ForMember(dest => dest.CustomerType, opts => opts.MapFrom(src => src.customer_type))
                               .ForAllUnmappedMembers(o => o.Ignore());
                            #endregion

                            #region ClientAccountEntity

                            Mapper.CreateMap<ClientAccountEntity, ClientAccountModel>()
                                .ForAllUnmappedMembers(o => o.Ignore());

                            Mapper.CreateMap<ClientAccountModel, ClientAccountEntity>()
                                .ForAllUnmappedMembers(o => o.Ignore());

                            Mapper.CreateMap<ClientAccountModel, ClientAccountReportEntity>()
                                .ForAllUnmappedMembers(o => o.Ignore());

                            Mapper.CreateMap<ClientAccountModel, BillingCustomerPremiseModel>()
                                .ForMember(dest => dest.customer_id, opts => opts.MapFrom(src => src.CustomerId))
                                .ForMember(dest => dest.ClientId, opts => opts.MapFrom(src => src.ClientId))
                                .ForAllUnmappedMembers(o => o.Ignore());

                            Mapper.CreateMap<BillingCustomerPremiseModel, ClientAccountModel>()
                               .ForMember(dest => dest.CustomerId, opts => opts.MapFrom(src => src.customer_id))
                               .ForMember(dest => dest.AccountId, opts => opts.MapFrom(src => src.account_id))
                               .ForMember(dest => dest.ClientId, opts => opts.MapFrom(src => src.ClientId))
                               .ForAllUnmappedMembers(o => o.Ignore());
                            #endregion

                            #region Premise

                            Mapper.CreateMap<PremiseEntity, PremiseModel>()
                                .ForAllUnmappedMembers(o => o.Ignore());

                            Mapper.CreateMap<PremiseModel, PremiseEntity>()
                                .ForAllUnmappedMembers(o => o.Ignore());


                            Mapper.CreateMap<PremiseModel, BillingCustomerPremiseModel>()
                               .ForMember(dest => dest.premise_id, opts => opts.MapFrom(src => src.PremiseId))
                               .ForMember(dest => dest.customer_id, opts => opts.MapFrom(src => src.CustomerId))
                               .ForMember(dest => dest.account_id, opts => opts.MapFrom(src => src.AccountId))
                               .ForMember(dest => dest.site_addressline1, opts => opts.MapFrom(src => src.AddressLine1))
                               .ForMember(dest => dest.site_addressline2, opts => opts.MapFrom(src => src.AddressLine2))
                               .ForMember(dest => dest.site_addressline3, opts => opts.MapFrom(src => src.AddressLine3))
                               .ForMember(dest => dest.site_city, opts => opts.MapFrom(src => src.City))
                               .ForMember(dest => dest.site_state, opts => opts.MapFrom(src => src.State))
                               .ForMember(dest => dest.site_zip_code, opts => opts.MapFrom(src => src.PostalCode))
                               .ForAllUnmappedMembers(o => o.Ignore());

                            Mapper.CreateMap<BillingCustomerPremiseModel, PremiseModel>()
                              .ForMember(dest => dest.PremiseId, opts => opts.MapFrom(src => src.premise_id))
                              .ForMember(dest => dest.CustomerId, opts => opts.MapFrom(src => src.customer_id))
                              .ForMember(dest => dest.AccountId, opts => opts.MapFrom(src => src.account_id))
                              .ForMember(dest => dest.AddressLine1, opts => opts.MapFrom(src => src.site_addressline1))
                              .ForMember(dest => dest.AddressLine2, opts => opts.MapFrom(src => src.site_addressline2))
                              .ForMember(dest => dest.AddressLine3, opts => opts.MapFrom(src => src.site_addressline3))
                              .ForMember(dest => dest.City, opts => opts.MapFrom(src => src.site_city))
                              .ForMember(dest => dest.State, opts => opts.MapFrom(src => src.site_state))
                              .ForMember(dest => dest.PostalCode, opts => opts.MapFrom(src => src.site_zip_code))
                              .ForAllUnmappedMembers(o => o.Ignore());
                            #endregion

                            #region TallAmi

                            Mapper.CreateMap<ConsumptionByMeterEntity, TallAmiModel>()
                                .ForAllUnmappedMembers(o => o.Ignore());

                            Mapper.CreateMap<TallAmiModel, ConsumptionByMeterEntity>()
                                .ForAllUnmappedMembers(o => o.Ignore());

                            Mapper.CreateMap<ConsumptionByMeterEntityLight, TallAmiModel>()
                                .ForAllUnmappedMembers(o => o.Ignore());

                            Mapper.CreateMap<TallAmiModel, ConsumptionByMeterEntityLight>()
                                .ForAllUnmappedMembers(o => o.Ignore());

                            #endregion

                            #region Ami15MinuteInterval

                            Mapper.CreateMap<BusinessContracts.Preprocessing.AmiReading, Ami15MinuteIntervalModel>()
                               .ForAllUnmappedMembers(o => o.Ignore());

                            Mapper.CreateMap<Ami15MinuteIntervalModel, BusinessContracts.Preprocessing.AmiReading>()
                                .ForAllUnmappedMembers(o => o.Ignore());

                            Mapper.CreateMap<Ami15MinuteIntervalModel, Ami15MinuteIntervalModel>()
                               .ForAllUnmappedMembers(o => o.Ignore());

                            Mapper.CreateMap<Ami15MinuteIntervalModel, RawAmi15MinuteIntervalModel>()
                                .ForMember(dest => dest.TimeStamp, opts => opts.MapFrom(src => src.AmiTimeStamp))
                                .ForAllUnmappedMembers(o => o.Ignore());

                            Mapper.CreateMap<RawAmi15MinuteIntervalModel, Ami15MinuteIntervalModel>()
                                .ForMember(dest => dest.ClientId, opts => opts.MapFrom(src => Convert.ToInt32(src.ClientId)))
                                .ForMember(dest => dest.AmiTimeStamp, opts => opts.MapFrom(src => Convert.ToDateTime(src.TimeStamp)))
                                .ForMember(dest => dest.UOMId, opts => opts.MapFrom(src => Convert.ToInt32(src.UOMId)))
                                .ForMember(dest => dest.CommodityId, opts => opts.MapFrom(src => Convert.ToInt32(src.CommodityId)))
                                .ForMember(dest => dest.VolumeFactor, opts => opts.MapFrom(src => string.IsNullOrEmpty(src.VolumeFactor) ? (double?)null: Convert.ToDouble(src.VolumeFactor)))
                                .ForMember(dest => dest.Direction, opts => opts.MapFrom(src => string.IsNullOrEmpty(src.Direction) ? (int?)null : Convert.ToInt32(src.Direction)))
                                .ForMember(dest => dest.ProjectedReadDate, opts => opts.MapFrom(src => string.IsNullOrEmpty(src.ProjectedReadDate) ? (DateTime?)null : Convert.ToDateTime(src.ProjectedReadDate)))
                                .ForMember(dest => dest.IntValue0000, opts => opts.MapFrom(src => string.IsNullOrEmpty(src.IntValue0000) ? (double?)null : Convert.ToDouble(src.IntValue0000)))
                                .ForMember(dest => dest.IntValue0015, opts => opts.MapFrom(src => string.IsNullOrEmpty(src.IntValue0015) ? (double?)null : Convert.ToDouble(src.IntValue0015)))
                                .ForMember(dest => dest.IntValue0030, opts => opts.MapFrom(src => string.IsNullOrEmpty(src.IntValue0030) ? (double?)null : Convert.ToDouble(src.IntValue0030)))
                                .ForMember(dest => dest.IntValue0045, opts => opts.MapFrom(src => string.IsNullOrEmpty(src.IntValue0045) ? (double?)null : Convert.ToDouble(src.IntValue0045)))
                                .ForMember(dest => dest.IntValue0100, opts => opts.MapFrom(src => string.IsNullOrEmpty(src.IntValue0100) ? (double?)null : Convert.ToDouble(src.IntValue0100)))
                                .ForMember(dest => dest.IntValue0115, opts => opts.MapFrom(src => string.IsNullOrEmpty(src.IntValue0115) ? (double?)null : Convert.ToDouble(src.IntValue0115)))
                                .ForMember(dest => dest.IntValue0130, opts => opts.MapFrom(src => string.IsNullOrEmpty(src.IntValue0130) ? (double?)null : Convert.ToDouble(src.IntValue0130)))
                                .ForMember(dest => dest.IntValue0145, opts => opts.MapFrom(src => string.IsNullOrEmpty(src.IntValue0145) ? (double?)null : Convert.ToDouble(src.IntValue0145)))
                                .ForMember(dest => dest.IntValue0200, opts => opts.MapFrom(src => string.IsNullOrEmpty(src.IntValue0200) ? (double?)null : Convert.ToDouble(src.IntValue0200)))
                                .ForMember(dest => dest.IntValue0215, opts => opts.MapFrom(src => string.IsNullOrEmpty(src.IntValue0215) ? (double?)null : Convert.ToDouble(src.IntValue0215)))
                                .ForMember(dest => dest.IntValue0230, opts => opts.MapFrom(src => string.IsNullOrEmpty(src.IntValue0230) ? (double?)null : Convert.ToDouble(src.IntValue0230)))
                                .ForMember(dest => dest.IntValue0245, opts => opts.MapFrom(src => string.IsNullOrEmpty(src.IntValue0245) ? (double?)null : Convert.ToDouble(src.IntValue0245)))
                                .ForMember(dest => dest.IntValue0300, opts => opts.MapFrom(src => string.IsNullOrEmpty(src.IntValue0300) ? (double?)null : Convert.ToDouble(src.IntValue0300)))
                                .ForMember(dest => dest.IntValue0315, opts => opts.MapFrom(src => string.IsNullOrEmpty(src.IntValue0315) ? (double?)null : Convert.ToDouble(src.IntValue0315)))
                                .ForMember(dest => dest.IntValue0330, opts => opts.MapFrom(src => string.IsNullOrEmpty(src.IntValue0330) ? (double?)null : Convert.ToDouble(src.IntValue0330)))
                                .ForMember(dest => dest.IntValue0345, opts => opts.MapFrom(src => string.IsNullOrEmpty(src.IntValue0345) ? (double?)null : Convert.ToDouble(src.IntValue0345)))
                                .ForMember(dest => dest.IntValue0400, opts => opts.MapFrom(src => string.IsNullOrEmpty(src.IntValue0400) ? (double?)null : Convert.ToDouble(src.IntValue0400)))
                                .ForMember(dest => dest.IntValue0415, opts => opts.MapFrom(src => string.IsNullOrEmpty(src.IntValue0415) ? (double?)null : Convert.ToDouble(src.IntValue0415)))
                                .ForMember(dest => dest.IntValue0430, opts => opts.MapFrom(src => string.IsNullOrEmpty(src.IntValue0430) ? (double?)null : Convert.ToDouble(src.IntValue0430)))
                                .ForMember(dest => dest.IntValue0445, opts => opts.MapFrom(src => string.IsNullOrEmpty(src.IntValue0445) ? (double?)null : Convert.ToDouble(src.IntValue0445)))
                                .ForMember(dest => dest.IntValue0500, opts => opts.MapFrom(src => string.IsNullOrEmpty(src.IntValue0500) ? (double?)null : Convert.ToDouble(src.IntValue0500)))
                                .ForMember(dest => dest.IntValue0515, opts => opts.MapFrom(src => string.IsNullOrEmpty(src.IntValue0515) ? (double?)null : Convert.ToDouble(src.IntValue0515)))
                                .ForMember(dest => dest.IntValue0530, opts => opts.MapFrom(src => string.IsNullOrEmpty(src.IntValue0530) ? (double?)null : Convert.ToDouble(src.IntValue0530)))
                                .ForMember(dest => dest.IntValue0545, opts => opts.MapFrom(src => string.IsNullOrEmpty(src.IntValue0545) ? (double?)null : Convert.ToDouble(src.IntValue0545)))
                                .ForMember(dest => dest.IntValue0600, opts => opts.MapFrom(src => string.IsNullOrEmpty(src.IntValue0600) ? (double?)null : Convert.ToDouble(src.IntValue0600)))
                                .ForMember(dest => dest.IntValue0615, opts => opts.MapFrom(src => string.IsNullOrEmpty(src.IntValue0615) ? (double?)null : Convert.ToDouble(src.IntValue0615)))
                                .ForMember(dest => dest.IntValue0630, opts => opts.MapFrom(src => string.IsNullOrEmpty(src.IntValue0630) ? (double?)null : Convert.ToDouble(src.IntValue0630)))
                                .ForMember(dest => dest.IntValue0645, opts => opts.MapFrom(src => string.IsNullOrEmpty(src.IntValue0645) ? (double?)null : Convert.ToDouble(src.IntValue0645)))
                                .ForMember(dest => dest.IntValue0700, opts => opts.MapFrom(src => string.IsNullOrEmpty(src.IntValue0700) ? (double?)null : Convert.ToDouble(src.IntValue0700)))
                                .ForMember(dest => dest.IntValue0715, opts => opts.MapFrom(src => string.IsNullOrEmpty(src.IntValue0715) ? (double?)null : Convert.ToDouble(src.IntValue0715)))
                                .ForMember(dest => dest.IntValue0730, opts => opts.MapFrom(src => string.IsNullOrEmpty(src.IntValue0730) ? (double?)null : Convert.ToDouble(src.IntValue0730)))
                                .ForMember(dest => dest.IntValue0745, opts => opts.MapFrom(src => string.IsNullOrEmpty(src.IntValue0745) ? (double?)null : Convert.ToDouble(src.IntValue0745)))
                                .ForMember(dest => dest.IntValue0800, opts => opts.MapFrom(src => string.IsNullOrEmpty(src.IntValue0800) ? (double?)null : Convert.ToDouble(src.IntValue0800)))
                                .ForMember(dest => dest.IntValue0815, opts => opts.MapFrom(src => string.IsNullOrEmpty(src.IntValue0815) ? (double?)null : Convert.ToDouble(src.IntValue0815)))
                                .ForMember(dest => dest.IntValue0830, opts => opts.MapFrom(src => string.IsNullOrEmpty(src.IntValue0830) ? (double?)null : Convert.ToDouble(src.IntValue0830)))
                                .ForMember(dest => dest.IntValue0845, opts => opts.MapFrom(src => string.IsNullOrEmpty(src.IntValue0845) ? (double?)null : Convert.ToDouble(src.IntValue0845)))
                                .ForMember(dest => dest.IntValue0900, opts => opts.MapFrom(src => string.IsNullOrEmpty(src.IntValue0900) ? (double?)null : Convert.ToDouble(src.IntValue0900)))
                                .ForMember(dest => dest.IntValue0915, opts => opts.MapFrom(src => string.IsNullOrEmpty(src.IntValue0915) ? (double?)null : Convert.ToDouble(src.IntValue0915)))
                                .ForMember(dest => dest.IntValue0930, opts => opts.MapFrom(src => string.IsNullOrEmpty(src.IntValue0930) ? (double?)null : Convert.ToDouble(src.IntValue0930)))
                                .ForMember(dest => dest.IntValue0945, opts => opts.MapFrom(src => string.IsNullOrEmpty(src.IntValue0945) ? (double?)null : Convert.ToDouble(src.IntValue0945)))
                                .ForMember(dest => dest.IntValue1000, opts => opts.MapFrom(src => string.IsNullOrEmpty(src.IntValue1000) ? (double?)null : Convert.ToDouble(src.IntValue1000)))
                                .ForMember(dest => dest.IntValue1015, opts => opts.MapFrom(src => string.IsNullOrEmpty(src.IntValue1015) ? (double?)null : Convert.ToDouble(src.IntValue1015)))
                                .ForMember(dest => dest.IntValue1030, opts => opts.MapFrom(src => string.IsNullOrEmpty(src.IntValue1030) ? (double?)null : Convert.ToDouble(src.IntValue1030)))
                                .ForMember(dest => dest.IntValue1045, opts => opts.MapFrom(src => string.IsNullOrEmpty(src.IntValue1045) ? (double?)null : Convert.ToDouble(src.IntValue1045)))
                                .ForMember(dest => dest.IntValue1100, opts => opts.MapFrom(src => string.IsNullOrEmpty(src.IntValue1100) ? (double?)null : Convert.ToDouble(src.IntValue1100)))
                                .ForMember(dest => dest.IntValue1115, opts => opts.MapFrom(src => string.IsNullOrEmpty(src.IntValue1115) ? (double?)null : Convert.ToDouble(src.IntValue1115)))
                                .ForMember(dest => dest.IntValue1130, opts => opts.MapFrom(src => string.IsNullOrEmpty(src.IntValue1130) ? (double?)null : Convert.ToDouble(src.IntValue1130)))
                                .ForMember(dest => dest.IntValue1145, opts => opts.MapFrom(src => string.IsNullOrEmpty(src.IntValue1145) ? (double?)null : Convert.ToDouble(src.IntValue1145)))
                                .ForMember(dest => dest.IntValue1200, opts => opts.MapFrom(src => string.IsNullOrEmpty(src.IntValue1200) ? (double?)null : Convert.ToDouble(src.IntValue1200)))
                                .ForMember(dest => dest.IntValue1215, opts => opts.MapFrom(src => string.IsNullOrEmpty(src.IntValue1215) ? (double?)null : Convert.ToDouble(src.IntValue1215)))
                                .ForMember(dest => dest.IntValue1230, opts => opts.MapFrom(src => string.IsNullOrEmpty(src.IntValue1230) ? (double?)null : Convert.ToDouble(src.IntValue1230)))
                                .ForMember(dest => dest.IntValue1245, opts => opts.MapFrom(src => string.IsNullOrEmpty(src.IntValue1245) ? (double?)null : Convert.ToDouble(src.IntValue1245)))
                                .ForMember(dest => dest.IntValue1300, opts => opts.MapFrom(src => string.IsNullOrEmpty(src.IntValue1300) ? (double?)null : Convert.ToDouble(src.IntValue1300)))
                                .ForMember(dest => dest.IntValue1315, opts => opts.MapFrom(src => string.IsNullOrEmpty(src.IntValue1315) ? (double?)null : Convert.ToDouble(src.IntValue1315)))
                                .ForMember(dest => dest.IntValue1330, opts => opts.MapFrom(src => string.IsNullOrEmpty(src.IntValue1330) ? (double?)null : Convert.ToDouble(src.IntValue1330)))
                                .ForMember(dest => dest.IntValue1345, opts => opts.MapFrom(src => string.IsNullOrEmpty(src.IntValue1345) ? (double?)null : Convert.ToDouble(src.IntValue1345)))
                                .ForMember(dest => dest.IntValue1400, opts => opts.MapFrom(src => string.IsNullOrEmpty(src.IntValue1400) ? (double?)null : Convert.ToDouble(src.IntValue1400)))
                                .ForMember(dest => dest.IntValue1415, opts => opts.MapFrom(src => string.IsNullOrEmpty(src.IntValue1415) ? (double?)null : Convert.ToDouble(src.IntValue1415)))
                                .ForMember(dest => dest.IntValue1430, opts => opts.MapFrom(src => string.IsNullOrEmpty(src.IntValue1430) ? (double?)null : Convert.ToDouble(src.IntValue1430)))
                                .ForMember(dest => dest.IntValue1445, opts => opts.MapFrom(src => string.IsNullOrEmpty(src.IntValue1445) ? (double?)null : Convert.ToDouble(src.IntValue1445)))
                                .ForMember(dest => dest.IntValue1500, opts => opts.MapFrom(src => string.IsNullOrEmpty(src.IntValue1500) ? (double?)null : Convert.ToDouble(src.IntValue1500)))
                                .ForMember(dest => dest.IntValue1515, opts => opts.MapFrom(src => string.IsNullOrEmpty(src.IntValue1515) ? (double?)null : Convert.ToDouble(src.IntValue1515)))
                                .ForMember(dest => dest.IntValue1530, opts => opts.MapFrom(src => string.IsNullOrEmpty(src.IntValue1530) ? (double?)null : Convert.ToDouble(src.IntValue1530)))
                                .ForMember(dest => dest.IntValue1545, opts => opts.MapFrom(src => string.IsNullOrEmpty(src.IntValue1545) ? (double?)null : Convert.ToDouble(src.IntValue1545)))
                                .ForMember(dest => dest.IntValue1600, opts => opts.MapFrom(src => string.IsNullOrEmpty(src.IntValue1600) ? (double?)null : Convert.ToDouble(src.IntValue1600)))
                                .ForMember(dest => dest.IntValue1615, opts => opts.MapFrom(src => string.IsNullOrEmpty(src.IntValue1615) ? (double?)null : Convert.ToDouble(src.IntValue1615)))
                                .ForMember(dest => dest.IntValue1630, opts => opts.MapFrom(src => string.IsNullOrEmpty(src.IntValue1630) ? (double?)null : Convert.ToDouble(src.IntValue1630)))
                                .ForMember(dest => dest.IntValue1645, opts => opts.MapFrom(src => string.IsNullOrEmpty(src.IntValue1645) ? (double?)null : Convert.ToDouble(src.IntValue1645)))
                                .ForMember(dest => dest.IntValue1700, opts => opts.MapFrom(src => string.IsNullOrEmpty(src.IntValue1700) ? (double?)null : Convert.ToDouble(src.IntValue1700)))
                                .ForMember(dest => dest.IntValue1715, opts => opts.MapFrom(src => string.IsNullOrEmpty(src.IntValue1715) ? (double?)null : Convert.ToDouble(src.IntValue1715)))
                                .ForMember(dest => dest.IntValue1730, opts => opts.MapFrom(src => string.IsNullOrEmpty(src.IntValue1730) ? (double?)null : Convert.ToDouble(src.IntValue1730)))
                                .ForMember(dest => dest.IntValue1745, opts => opts.MapFrom(src => string.IsNullOrEmpty(src.IntValue1745) ? (double?)null : Convert.ToDouble(src.IntValue1745)))
                                .ForMember(dest => dest.IntValue1800, opts => opts.MapFrom(src => string.IsNullOrEmpty(src.IntValue1800) ? (double?)null : Convert.ToDouble(src.IntValue1800)))
                                .ForMember(dest => dest.IntValue1815, opts => opts.MapFrom(src => string.IsNullOrEmpty(src.IntValue1815) ? (double?)null : Convert.ToDouble(src.IntValue1815)))
                                .ForMember(dest => dest.IntValue1830, opts => opts.MapFrom(src => string.IsNullOrEmpty(src.IntValue1830) ? (double?)null : Convert.ToDouble(src.IntValue1830)))
                                .ForMember(dest => dest.IntValue1845, opts => opts.MapFrom(src => string.IsNullOrEmpty(src.IntValue1845) ? (double?)null : Convert.ToDouble(src.IntValue1845)))
                                .ForMember(dest => dest.IntValue1900, opts => opts.MapFrom(src => string.IsNullOrEmpty(src.IntValue1900) ? (double?)null : Convert.ToDouble(src.IntValue1900)))
                                .ForMember(dest => dest.IntValue1915, opts => opts.MapFrom(src => string.IsNullOrEmpty(src.IntValue1915) ? (double?)null : Convert.ToDouble(src.IntValue1915)))
                                .ForMember(dest => dest.IntValue1930, opts => opts.MapFrom(src => string.IsNullOrEmpty(src.IntValue1930) ? (double?)null : Convert.ToDouble(src.IntValue1930)))
                                .ForMember(dest => dest.IntValue1945, opts => opts.MapFrom(src => string.IsNullOrEmpty(src.IntValue1945) ? (double?)null : Convert.ToDouble(src.IntValue1945)))
                                .ForMember(dest => dest.IntValue2000, opts => opts.MapFrom(src => string.IsNullOrEmpty(src.IntValue2000) ? (double?)null : Convert.ToDouble(src.IntValue2000)))
                                .ForMember(dest => dest.IntValue2015, opts => opts.MapFrom(src => string.IsNullOrEmpty(src.IntValue2015) ? (double?)null : Convert.ToDouble(src.IntValue2015)))
                                .ForMember(dest => dest.IntValue2030, opts => opts.MapFrom(src => string.IsNullOrEmpty(src.IntValue2030) ? (double?)null : Convert.ToDouble(src.IntValue2030)))
                                .ForMember(dest => dest.IntValue2045, opts => opts.MapFrom(src => string.IsNullOrEmpty(src.IntValue2045) ? (double?)null : Convert.ToDouble(src.IntValue2045)))
                                .ForMember(dest => dest.IntValue2100, opts => opts.MapFrom(src => string.IsNullOrEmpty(src.IntValue2100) ? (double?)null : Convert.ToDouble(src.IntValue2100)))
                                .ForMember(dest => dest.IntValue2115, opts => opts.MapFrom(src => string.IsNullOrEmpty(src.IntValue2115) ? (double?)null : Convert.ToDouble(src.IntValue2115)))
                                .ForMember(dest => dest.IntValue2130, opts => opts.MapFrom(src => string.IsNullOrEmpty(src.IntValue2130) ? (double?)null : Convert.ToDouble(src.IntValue2130)))
                                .ForMember(dest => dest.IntValue2145, opts => opts.MapFrom(src => string.IsNullOrEmpty(src.IntValue2145) ? (double?)null : Convert.ToDouble(src.IntValue2145)))
                                .ForMember(dest => dest.IntValue2200, opts => opts.MapFrom(src => string.IsNullOrEmpty(src.IntValue2200) ? (double?)null : Convert.ToDouble(src.IntValue2200)))
                                .ForMember(dest => dest.IntValue2215, opts => opts.MapFrom(src => string.IsNullOrEmpty(src.IntValue2215) ? (double?)null : Convert.ToDouble(src.IntValue2215)))
                                .ForMember(dest => dest.IntValue2230, opts => opts.MapFrom(src => string.IsNullOrEmpty(src.IntValue2230) ? (double?)null : Convert.ToDouble(src.IntValue2230)))
                                .ForMember(dest => dest.IntValue2245, opts => opts.MapFrom(src => string.IsNullOrEmpty(src.IntValue2245) ? (double?)null : Convert.ToDouble(src.IntValue2245)))
                                .ForMember(dest => dest.IntValue2300, opts => opts.MapFrom(src => string.IsNullOrEmpty(src.IntValue2300) ? (double?)null : Convert.ToDouble(src.IntValue2300)))
                                .ForMember(dest => dest.IntValue2315, opts => opts.MapFrom(src => string.IsNullOrEmpty(src.IntValue2315) ? (double?)null : Convert.ToDouble(src.IntValue2315)))
                                .ForMember(dest => dest.IntValue2330, opts => opts.MapFrom(src => string.IsNullOrEmpty(src.IntValue2330) ? (double?)null : Convert.ToDouble(src.IntValue2330)))
                                .ForMember(dest => dest.IntValue2345, opts => opts.MapFrom(src => string.IsNullOrEmpty(src.IntValue2345) ? (double?)null : Convert.ToDouble(src.IntValue2345)))
                                .ForAllUnmappedMembers(o => o.Ignore());

                            Mapper.CreateMap<Ami15MinuteIntervalEntity, Ami15MinuteIntervalModel>()
                                .ForAllUnmappedMembers(o => o.Ignore());

                            Mapper.CreateMap<Ami15MinuteIntervalModel, Ami15MinuteIntervalEntity>()
                                .ForAllUnmappedMembers(o => o.Ignore());

                            Mapper.CreateMap<Ami15MinuteIntervalEntity, Ami30MinuteIntervalModel>()
                                .ForAllUnmappedMembers(o => o.Ignore());

                            Mapper.CreateMap<Ami15MinuteIntervalEntity, Ami60MinuteIntervalModel>()
                                .ForAllUnmappedMembers(o => o.Ignore());

                            #endregion

                            #region Ami30MinuteInterval

                            Mapper.CreateMap<Ami15MinuteIntervalEntity, Ami30MinuteIntervalEntity>()
                                .ForAllUnmappedMembers(o => o.Ignore());

                            Mapper.CreateMap<BusinessContracts.Preprocessing.AmiReading, Ami30MinuteIntervalModel>()
                              .ForAllUnmappedMembers(o => o.Ignore());

                            Mapper.CreateMap<Ami30MinuteIntervalModel, BusinessContracts.Preprocessing.AmiReading>()
                                .ForAllUnmappedMembers(o => o.Ignore());

                            Mapper.CreateMap<Ami30MinuteIntervalModel, Ami30MinuteIntervalModel>()
                               .ForAllUnmappedMembers(o => o.Ignore());

                            Mapper.CreateMap<Ami30MinuteIntervalModel, RawAmi30MinuteIntervalModel>()
                               .ForMember(dest => dest.TimeStamp, opts => opts.MapFrom(src => src.AmiTimeStamp))
                               .ForAllUnmappedMembers(o => o.Ignore());

                            Mapper.CreateMap<RawAmi30MinuteIntervalModel, Ami30MinuteIntervalModel>()
                                .ForMember(dest => dest.ClientId, opts => opts.MapFrom(src => Convert.ToInt32(src.ClientId)))
                                .ForMember(dest => dest.AmiTimeStamp, opts => opts.MapFrom(src => Convert.ToDateTime(src.TimeStamp)))
                                .ForMember(dest => dest.UOMId, opts => opts.MapFrom(src => Convert.ToInt32(src.UOMId)))
                                .ForMember(dest => dest.CommodityId, opts => opts.MapFrom(src => Convert.ToInt32(src.CommodityId)))
                                .ForMember(dest => dest.VolumeFactor, opts => opts.MapFrom(src => string.IsNullOrEmpty(src.VolumeFactor) ? (double?)null : Convert.ToDouble(src.VolumeFactor)))
                                .ForMember(dest => dest.Direction, opts => opts.MapFrom(src => string.IsNullOrEmpty(src.Direction) ? (int?)null : Convert.ToInt32(src.Direction)))
                                .ForMember(dest => dest.ProjectedReadDate, opts => opts.MapFrom(src => string.IsNullOrEmpty(src.ProjectedReadDate) ? (DateTime?)null : Convert.ToDateTime(src.ProjectedReadDate)))
                                .ForMember(dest => dest.IntValue0000, opts => opts.MapFrom(src => string.IsNullOrEmpty(src.IntValue0000) ? (double?)null : Convert.ToDouble(src.IntValue0000)))
                                .ForMember(dest => dest.IntValue0030, opts => opts.MapFrom(src => string.IsNullOrEmpty(src.IntValue0030) ? (double?)null : Convert.ToDouble(src.IntValue0030)))
                                .ForMember(dest => dest.IntValue0100, opts => opts.MapFrom(src => string.IsNullOrEmpty(src.IntValue0100) ? (double?)null : Convert.ToDouble(src.IntValue0100)))
                                .ForMember(dest => dest.IntValue0130, opts => opts.MapFrom(src => string.IsNullOrEmpty(src.IntValue0130) ? (double?)null : Convert.ToDouble(src.IntValue0130)))
                                .ForMember(dest => dest.IntValue0200, opts => opts.MapFrom(src => string.IsNullOrEmpty(src.IntValue0200) ? (double?)null : Convert.ToDouble(src.IntValue0200)))
                                .ForMember(dest => dest.IntValue0230, opts => opts.MapFrom(src => string.IsNullOrEmpty(src.IntValue0230) ? (double?)null : Convert.ToDouble(src.IntValue0230)))
                                .ForMember(dest => dest.IntValue0300, opts => opts.MapFrom(src => string.IsNullOrEmpty(src.IntValue0300) ? (double?)null : Convert.ToDouble(src.IntValue0300)))
                                .ForMember(dest => dest.IntValue0330, opts => opts.MapFrom(src => string.IsNullOrEmpty(src.IntValue0330) ? (double?)null : Convert.ToDouble(src.IntValue0330)))
                                .ForMember(dest => dest.IntValue0400, opts => opts.MapFrom(src => string.IsNullOrEmpty(src.IntValue0400) ? (double?)null : Convert.ToDouble(src.IntValue0400)))
                                .ForMember(dest => dest.IntValue0430, opts => opts.MapFrom(src => string.IsNullOrEmpty(src.IntValue0430) ? (double?)null : Convert.ToDouble(src.IntValue0430)))
                                .ForMember(dest => dest.IntValue0500, opts => opts.MapFrom(src => string.IsNullOrEmpty(src.IntValue0500) ? (double?)null : Convert.ToDouble(src.IntValue0500)))
                                .ForMember(dest => dest.IntValue0530, opts => opts.MapFrom(src => string.IsNullOrEmpty(src.IntValue0530) ? (double?)null : Convert.ToDouble(src.IntValue0530)))
                                .ForMember(dest => dest.IntValue0600, opts => opts.MapFrom(src => string.IsNullOrEmpty(src.IntValue0600) ? (double?)null : Convert.ToDouble(src.IntValue0600)))
                                .ForMember(dest => dest.IntValue0630, opts => opts.MapFrom(src => string.IsNullOrEmpty(src.IntValue0630) ? (double?)null : Convert.ToDouble(src.IntValue0630)))
                                .ForMember(dest => dest.IntValue0700, opts => opts.MapFrom(src => string.IsNullOrEmpty(src.IntValue0700) ? (double?)null : Convert.ToDouble(src.IntValue0700)))
                                .ForMember(dest => dest.IntValue0730, opts => opts.MapFrom(src => string.IsNullOrEmpty(src.IntValue0730) ? (double?)null : Convert.ToDouble(src.IntValue0730)))
                                .ForMember(dest => dest.IntValue0800, opts => opts.MapFrom(src => string.IsNullOrEmpty(src.IntValue0800) ? (double?)null : Convert.ToDouble(src.IntValue0800)))
                                .ForMember(dest => dest.IntValue0830, opts => opts.MapFrom(src => string.IsNullOrEmpty(src.IntValue0830) ? (double?)null : Convert.ToDouble(src.IntValue0830)))
                                .ForMember(dest => dest.IntValue0900, opts => opts.MapFrom(src => string.IsNullOrEmpty(src.IntValue0900) ? (double?)null : Convert.ToDouble(src.IntValue0900)))
                                .ForMember(dest => dest.IntValue0930, opts => opts.MapFrom(src => string.IsNullOrEmpty(src.IntValue0930) ? (double?)null : Convert.ToDouble(src.IntValue0930)))
                                .ForMember(dest => dest.IntValue1000, opts => opts.MapFrom(src => string.IsNullOrEmpty(src.IntValue1000) ? (double?)null : Convert.ToDouble(src.IntValue1000)))
                                .ForMember(dest => dest.IntValue1030, opts => opts.MapFrom(src => string.IsNullOrEmpty(src.IntValue1030) ? (double?)null : Convert.ToDouble(src.IntValue1030)))
                                .ForMember(dest => dest.IntValue1100, opts => opts.MapFrom(src => string.IsNullOrEmpty(src.IntValue1100) ? (double?)null : Convert.ToDouble(src.IntValue1100)))
                                .ForMember(dest => dest.IntValue1130, opts => opts.MapFrom(src => string.IsNullOrEmpty(src.IntValue1130) ? (double?)null : Convert.ToDouble(src.IntValue1130)))
                                .ForMember(dest => dest.IntValue1200, opts => opts.MapFrom(src => string.IsNullOrEmpty(src.IntValue1200) ? (double?)null : Convert.ToDouble(src.IntValue1200)))
                                .ForMember(dest => dest.IntValue1230, opts => opts.MapFrom(src => string.IsNullOrEmpty(src.IntValue1230) ? (double?)null : Convert.ToDouble(src.IntValue1230)))
                                .ForMember(dest => dest.IntValue1300, opts => opts.MapFrom(src => string.IsNullOrEmpty(src.IntValue1300) ? (double?)null : Convert.ToDouble(src.IntValue1300)))
                                .ForMember(dest => dest.IntValue1330, opts => opts.MapFrom(src => string.IsNullOrEmpty(src.IntValue1330) ? (double?)null : Convert.ToDouble(src.IntValue1330)))
                                .ForMember(dest => dest.IntValue1400, opts => opts.MapFrom(src => string.IsNullOrEmpty(src.IntValue1400) ? (double?)null : Convert.ToDouble(src.IntValue1400)))
                                .ForMember(dest => dest.IntValue1430, opts => opts.MapFrom(src => string.IsNullOrEmpty(src.IntValue1430) ? (double?)null : Convert.ToDouble(src.IntValue1430)))
                                .ForMember(dest => dest.IntValue1500, opts => opts.MapFrom(src => string.IsNullOrEmpty(src.IntValue1500) ? (double?)null : Convert.ToDouble(src.IntValue1500)))
                                .ForMember(dest => dest.IntValue1530, opts => opts.MapFrom(src => string.IsNullOrEmpty(src.IntValue1530) ? (double?)null : Convert.ToDouble(src.IntValue1530)))
                                .ForMember(dest => dest.IntValue1600, opts => opts.MapFrom(src => string.IsNullOrEmpty(src.IntValue1600) ? (double?)null : Convert.ToDouble(src.IntValue1600)))
                                .ForMember(dest => dest.IntValue1630, opts => opts.MapFrom(src => string.IsNullOrEmpty(src.IntValue1630) ? (double?)null : Convert.ToDouble(src.IntValue1630)))
                                .ForMember(dest => dest.IntValue1700, opts => opts.MapFrom(src => string.IsNullOrEmpty(src.IntValue1700) ? (double?)null : Convert.ToDouble(src.IntValue1700)))
                                .ForMember(dest => dest.IntValue1730, opts => opts.MapFrom(src => string.IsNullOrEmpty(src.IntValue1730) ? (double?)null : Convert.ToDouble(src.IntValue1730)))
                                .ForMember(dest => dest.IntValue1800, opts => opts.MapFrom(src => string.IsNullOrEmpty(src.IntValue1800) ? (double?)null : Convert.ToDouble(src.IntValue1800)))
                                .ForMember(dest => dest.IntValue1830, opts => opts.MapFrom(src => string.IsNullOrEmpty(src.IntValue1830) ? (double?)null : Convert.ToDouble(src.IntValue1830)))
                                .ForMember(dest => dest.IntValue1900, opts => opts.MapFrom(src => string.IsNullOrEmpty(src.IntValue1900) ? (double?)null : Convert.ToDouble(src.IntValue1900)))
                                .ForMember(dest => dest.IntValue1930, opts => opts.MapFrom(src => string.IsNullOrEmpty(src.IntValue1930) ? (double?)null : Convert.ToDouble(src.IntValue1930)))
                                .ForMember(dest => dest.IntValue2000, opts => opts.MapFrom(src => string.IsNullOrEmpty(src.IntValue2000) ? (double?)null : Convert.ToDouble(src.IntValue2000)))
                                .ForMember(dest => dest.IntValue2030, opts => opts.MapFrom(src => string.IsNullOrEmpty(src.IntValue2030) ? (double?)null : Convert.ToDouble(src.IntValue2030)))
                                .ForMember(dest => dest.IntValue2100, opts => opts.MapFrom(src => string.IsNullOrEmpty(src.IntValue2100) ? (double?)null : Convert.ToDouble(src.IntValue2100)))
                                .ForMember(dest => dest.IntValue2130, opts => opts.MapFrom(src => string.IsNullOrEmpty(src.IntValue2130) ? (double?)null : Convert.ToDouble(src.IntValue2130)))
                                .ForMember(dest => dest.IntValue2200, opts => opts.MapFrom(src => string.IsNullOrEmpty(src.IntValue2200) ? (double?)null : Convert.ToDouble(src.IntValue2200)))
                                .ForMember(dest => dest.IntValue2230, opts => opts.MapFrom(src => string.IsNullOrEmpty(src.IntValue2230) ? (double?)null : Convert.ToDouble(src.IntValue2230)))
                                .ForMember(dest => dest.IntValue2300, opts => opts.MapFrom(src => string.IsNullOrEmpty(src.IntValue2300) ? (double?)null : Convert.ToDouble(src.IntValue2300)))
                                .ForMember(dest => dest.IntValue2330, opts => opts.MapFrom(src => string.IsNullOrEmpty(src.IntValue2330) ? (double?)null : Convert.ToDouble(src.IntValue2330)))
                                .ForAllUnmappedMembers(o => o.Ignore());

                            Mapper.CreateMap<Ami30MinuteIntervalEntity, Ami30MinuteIntervalModel>()
                                .ForAllUnmappedMembers(o => o.Ignore());

                            Mapper.CreateMap<Ami30MinuteIntervalModel, Ami30MinuteIntervalEntity>()
                                .ForAllUnmappedMembers(o => o.Ignore());
                            #endregion

                            #region Ami60MinuteInterval

                            Mapper.CreateMap<Ami15MinuteIntervalEntity, Ami60MinuteIntervalEntity>()
                                .ForAllUnmappedMembers(o => o.Ignore());

                            Mapper.CreateMap<BusinessContracts.Preprocessing.AmiReading, Ami60MinuteIntervalModel>()
                              .ForAllUnmappedMembers(o => o.Ignore());

                            Mapper.CreateMap<Ami60MinuteIntervalModel, BusinessContracts.Preprocessing.AmiReading>()
                                .ForAllUnmappedMembers(o => o.Ignore());

                            Mapper.CreateMap<Ami60MinuteIntervalModel, Ami60MinuteIntervalModel>()
                               .ForAllUnmappedMembers(o => o.Ignore());

                            Mapper.CreateMap<Ami60MinuteIntervalModel, RawAmi60MinuteIntervalModel>()
                                .ForMember(dest => dest.TimeStamp, opts => opts.MapFrom(src => src.AmiTimeStamp))
                                .ForAllUnmappedMembers(o => o.Ignore());

                            Mapper.CreateMap<RawAmi60MinuteIntervalModel, Ami60MinuteIntervalModel>()
                                .ForMember(dest => dest.ClientId, opts => opts.MapFrom(src => Convert.ToInt32(src.ClientId)))
                                .ForMember(dest => dest.AmiTimeStamp, opts => opts.MapFrom(src => Convert.ToDateTime(src.TimeStamp)))
                                .ForMember(dest => dest.UOMId, opts => opts.MapFrom(src => Convert.ToInt32(src.UOMId)))
                                .ForMember(dest => dest.CommodityId, opts => opts.MapFrom(src => Convert.ToInt32(src.CommodityId)))
                                .ForMember(dest => dest.VolumeFactor, opts => opts.MapFrom(src => string.IsNullOrEmpty(src.VolumeFactor) ? (double?)null : Convert.ToDouble(src.VolumeFactor)))
                                .ForMember(dest => dest.Direction, opts => opts.MapFrom(src => string.IsNullOrEmpty(src.Direction) ? (int?)null : Convert.ToInt32(src.Direction)))
                                .ForMember(dest => dest.ProjectedReadDate, opts => opts.MapFrom(src => string.IsNullOrEmpty(src.ProjectedReadDate) ? (DateTime?)null : Convert.ToDateTime(src.ProjectedReadDate)))
                                .ForMember(dest => dest.IntValue0000, opts => opts.MapFrom(src => string.IsNullOrEmpty(src.IntValue0000) ? (double?)null : Convert.ToDouble(src.IntValue0000)))
                                .ForMember(dest => dest.IntValue0100, opts => opts.MapFrom(src => string.IsNullOrEmpty(src.IntValue0100) ? (double?)null : Convert.ToDouble(src.IntValue0100)))
                                .ForMember(dest => dest.IntValue0200, opts => opts.MapFrom(src => string.IsNullOrEmpty(src.IntValue0200) ? (double?)null : Convert.ToDouble(src.IntValue0200)))
                                .ForMember(dest => dest.IntValue0300, opts => opts.MapFrom(src => string.IsNullOrEmpty(src.IntValue0300) ? (double?)null : Convert.ToDouble(src.IntValue0300)))
                                .ForMember(dest => dest.IntValue0400, opts => opts.MapFrom(src => string.IsNullOrEmpty(src.IntValue0400) ? (double?)null : Convert.ToDouble(src.IntValue0400)))
                                .ForMember(dest => dest.IntValue0500, opts => opts.MapFrom(src => string.IsNullOrEmpty(src.IntValue0500) ? (double?)null : Convert.ToDouble(src.IntValue0500)))
                                .ForMember(dest => dest.IntValue0600, opts => opts.MapFrom(src => string.IsNullOrEmpty(src.IntValue0600) ? (double?)null : Convert.ToDouble(src.IntValue0600)))
                                .ForMember(dest => dest.IntValue0700, opts => opts.MapFrom(src => string.IsNullOrEmpty(src.IntValue0700) ? (double?)null : Convert.ToDouble(src.IntValue0700)))
                                .ForMember(dest => dest.IntValue0800, opts => opts.MapFrom(src => string.IsNullOrEmpty(src.IntValue0800) ? (double?)null : Convert.ToDouble(src.IntValue0800)))
                                .ForMember(dest => dest.IntValue0900, opts => opts.MapFrom(src => string.IsNullOrEmpty(src.IntValue0900) ? (double?)null : Convert.ToDouble(src.IntValue0900)))
                                .ForMember(dest => dest.IntValue1000, opts => opts.MapFrom(src => string.IsNullOrEmpty(src.IntValue1000) ? (double?)null : Convert.ToDouble(src.IntValue1000)))
                                .ForMember(dest => dest.IntValue1100, opts => opts.MapFrom(src => string.IsNullOrEmpty(src.IntValue1100) ? (double?)null : Convert.ToDouble(src.IntValue1100)))
                                .ForMember(dest => dest.IntValue1200, opts => opts.MapFrom(src => string.IsNullOrEmpty(src.IntValue1200) ? (double?)null : Convert.ToDouble(src.IntValue1200)))
                                .ForMember(dest => dest.IntValue1300, opts => opts.MapFrom(src => string.IsNullOrEmpty(src.IntValue1300) ? (double?)null : Convert.ToDouble(src.IntValue1300)))
                                .ForMember(dest => dest.IntValue1400, opts => opts.MapFrom(src => string.IsNullOrEmpty(src.IntValue1400) ? (double?)null : Convert.ToDouble(src.IntValue1400)))
                                .ForMember(dest => dest.IntValue1500, opts => opts.MapFrom(src => string.IsNullOrEmpty(src.IntValue1500) ? (double?)null : Convert.ToDouble(src.IntValue1500)))
                                .ForMember(dest => dest.IntValue1600, opts => opts.MapFrom(src => string.IsNullOrEmpty(src.IntValue1600) ? (double?)null : Convert.ToDouble(src.IntValue1600)))
                                .ForMember(dest => dest.IntValue1700, opts => opts.MapFrom(src => string.IsNullOrEmpty(src.IntValue1700) ? (double?)null : Convert.ToDouble(src.IntValue1700)))
                                .ForMember(dest => dest.IntValue1800, opts => opts.MapFrom(src => string.IsNullOrEmpty(src.IntValue1800) ? (double?)null : Convert.ToDouble(src.IntValue1800)))
                                .ForMember(dest => dest.IntValue1900, opts => opts.MapFrom(src => string.IsNullOrEmpty(src.IntValue1900) ? (double?)null : Convert.ToDouble(src.IntValue1900)))
                                .ForMember(dest => dest.IntValue2000, opts => opts.MapFrom(src => string.IsNullOrEmpty(src.IntValue2000) ? (double?)null : Convert.ToDouble(src.IntValue2000)))
                                .ForMember(dest => dest.IntValue2100, opts => opts.MapFrom(src => string.IsNullOrEmpty(src.IntValue2100) ? (double?)null : Convert.ToDouble(src.IntValue2100)))
                                .ForMember(dest => dest.IntValue2200, opts => opts.MapFrom(src => string.IsNullOrEmpty(src.IntValue2200) ? (double?)null : Convert.ToDouble(src.IntValue2200)))
                                .ForMember(dest => dest.IntValue2300, opts => opts.MapFrom(src => string.IsNullOrEmpty(src.IntValue2300) ? (double?)null : Convert.ToDouble(src.IntValue2300)))
                                .ForAllUnmappedMembers(o => o.Ignore());

                            Mapper.CreateMap<Ami60MinuteIntervalEntity, Ami60MinuteIntervalModel>()
                                .ForAllUnmappedMembers(o => o.Ignore());

                            Mapper.CreateMap<Ami60MinuteIntervalModel, Ami60MinuteIntervalEntity>()
                                .ForAllUnmappedMembers(o => o.Ignore());
                            #endregion

                            #region AmiDailyInterval

                            Mapper.CreateMap<BusinessContracts.Preprocessing.AmiReading, AmiDailyIntervalModel>()
                              .ForAllUnmappedMembers(o => o.Ignore());

                            Mapper.CreateMap<AmiDailyIntervalModel, BusinessContracts.Preprocessing.AmiReading>()
                                .ForAllUnmappedMembers(o => o.Ignore());

                            Mapper.CreateMap<AmiDailyIntervalModel, AmiDailyIntervalModel>()
                               .ForAllUnmappedMembers(o => o.Ignore());

                            Mapper.CreateMap<AmiDailyIntervalModel, RawAmiDailyIntervalModel>()
                                .ForMember(dest => dest.TimeStamp, opts => opts.MapFrom(src => src.AmiTimeStamp))
                                .ForAllUnmappedMembers(o => o.Ignore());

                            Mapper.CreateMap<RawAmiDailyIntervalModel, AmiDailyIntervalModel>()
                                .ForMember(dest => dest.ClientId, opts => opts.MapFrom(src => Convert.ToInt32(src.ClientId)))
                                .ForMember(dest => dest.AmiTimeStamp, opts => opts.MapFrom(src => Convert.ToDateTime(src.TimeStamp)))
                                .ForMember(dest => dest.UOMId, opts => opts.MapFrom(src => Convert.ToInt32(src.UOMId)))
                                .ForMember(dest => dest.CommodityId, opts => opts.MapFrom(src => Convert.ToInt32(src.CommodityId)))
                                .ForMember(dest => dest.VolumeFactor, opts => opts.MapFrom(src => string.IsNullOrEmpty(src.VolumeFactor) ? (double?)null : Convert.ToDouble(src.VolumeFactor)))
                                .ForMember(dest => dest.Direction, opts => opts.MapFrom(src => string.IsNullOrEmpty(src.Direction) ? (int?)null : Convert.ToInt32(src.Direction)))
                                .ForMember(dest => dest.ProjectedReadDate, opts => opts.MapFrom(src => string.IsNullOrEmpty(src.ProjectedReadDate) ? (DateTime?)null : Convert.ToDateTime(src.ProjectedReadDate)))
                                .ForMember(dest => dest.IntValue0000, opts => opts.MapFrom(src => string.IsNullOrEmpty(src.IntValue0000) ? (double?)null : Convert.ToDouble(src.IntValue0000)))
                                .ForMember(dest => dest.TOUID, opts => opts.MapFrom(src => string.IsNullOrEmpty(src.TOUID) ? 0 : Convert.ToInt32(src.TOUID)))
                                .ForAllUnmappedMembers(o => o.Ignore());

                            Mapper.CreateMap<AmiDailyIntervalEntity, AmiDailyIntervalModel>()
                                .ForAllUnmappedMembers(o => o.Ignore());

                            Mapper.CreateMap<AmiDailyIntervalModel, AmiDailyIntervalEntity>()
                                .ForAllUnmappedMembers(o => o.Ignore());
                            #endregion

                            #region AccountUpdates

                            Mapper.CreateMap<AccountUpdatesModel, RawAccountUpdatesModel>()
                                .ForAllUnmappedMembers(o => o.Ignore());

                            Mapper.CreateMap<RawAccountUpdatesModel, AccountUpdatesModel>()
                                .ForMember(dest => dest.ClientId, opts => opts.MapFrom(src => Convert.ToInt32(src.ClientId)))
                                .ForMember(dest => dest.UpdateSentDate, opts => opts.MapFrom(src => string.IsNullOrEmpty(src.UpdateSentDate) ? (DateTime?)null:Convert.ToDateTime(src.UpdateSentDate)))
                                .ForAllUnmappedMembers(o => o.Ignore());

                            Mapper.CreateMap<AccountUpdatesEntity, AccountUpdatesModel>()
                                .ForAllUnmappedMembers(o => o.Ignore());

                            Mapper.CreateMap<AccountUpdatesModel, AccountUpdatesEntity>()
                                .ForAllUnmappedMembers(o => o.Ignore());
                            #endregion

                            #region BillCycleSchedule

                            Mapper.CreateMap<BillCycleScheduleEntity, BillCycleScheduleModel>()
                                .ForAllUnmappedMembers(o => o.Ignore());

                            Mapper.CreateMap<BillCycleScheduleModel, BillCycleScheduleEntity>()
                                .ForAllUnmappedMembers(o => o.Ignore());
                            #endregion

                            #region Calculation

                            Mapper.CreateMap<CalculationEntity, CalculationModel>()
                                .ForAllUnmappedMembers(o => o.Ignore());

                            Mapper.CreateMap<CalculationModel, CalculationEntity>()
                                .ForMember(dest => dest.BillCycleStartDate, opts => opts.MapFrom(src => Convert.ToDateTime(src.BillCycleStartDate)))
                                .ForMember(dest => dest.BillCycleEndDate, opts => opts.MapFrom(src => Convert.ToDateTime(src.BillCycleEndDate)))
                                .ForAllUnmappedMembers(o => o.Ignore());
                            #endregion

                            #region Subscription

                            Mapper.CreateMap<SubscriptionModel, RawSubscriptionModel>()
                                .ForAllUnmappedMembers(o => o.Ignore());

                            Mapper.CreateMap<RawSubscriptionModel, SubscriptionModel>()
                                .ForMember(dest => dest.ClientId, opts => opts.MapFrom(src => Convert.ToInt32(src.ClientId)))
                                .ForMember(dest => dest.IsEmailOptInCompleted, opts => opts.MapFrom(src => string.IsNullOrEmpty(src.IsEmailOptInCompleted) ? (bool?)null : Convert.ToBoolean(src.IsEmailOptInCompleted)))
                                .ForMember(dest => dest.IsSmsOptInCompleted, opts => opts.MapFrom(src => string.IsNullOrEmpty(src.IsSmsOptInCompleted) ? (bool?)null : Convert.ToBoolean(src.IsSmsOptInCompleted)))
                                .ForMember(dest => dest.IsSelected, opts => opts.MapFrom(src => Convert.ToBoolean(src.IsSelected)))
                                .ForMember(dest => dest.SubscriberThresholdValue, opts => opts.MapFrom(src => string.IsNullOrEmpty(src.SubscriberThresholdValue) ? (double?)null : Convert.ToDouble(src.SubscriberThresholdValue)))
                                .ForAllUnmappedMembers(o => o.Ignore());

                            Mapper.CreateMap<SubscriptionEntity, SubscriptionModel>()
                                .ForAllUnmappedMembers(o => o.Ignore());

                            Mapper.CreateMap<SubscriptionModel, SubscriptionEntity>()
                                .ForAllUnmappedMembers(o => o.Ignore());

                            Mapper.CreateMap<SubscriptionEntity, SubscriptionReportEntity>()
                                .ForAllUnmappedMembers(o => o.Ignore());
                            #endregion

                            #region Insight

                            Mapper.CreateMap<InsightEntity, InsightModel>()
                                .ForAllUnmappedMembers(o => o.Ignore());

                            Mapper.CreateMap<InsightModel, InsightEntity>()
                                .ForAllUnmappedMembers(o => o.Ignore());
                            #endregion

                            #region Notification

                            Mapper.CreateMap<NotificationEntity, NotificationModel>()
                                .ForAllUnmappedMembers(o => o.Ignore());

                            Mapper.CreateMap<NotificationModel, NotificationEntity>()
                                .ForAllUnmappedMembers(o => o.Ignore());

                            Mapper.CreateMap<NotificationModel, NotificationReportEntity>()
                                .ForAllUnmappedMembers(o => o.Ignore());
                            #endregion

                            #region SmsTemplate

                            Mapper.CreateMap<SmsTemplateEntity, SmsTemplateModel>()
                                .ForAllUnmappedMembers(o => o.Ignore());

                            Mapper.CreateMap<SmsTemplateModel, SmsTemplateEntity>()
                                .ForAllUnmappedMembers(o => o.Ignore());
                            #endregion

                            #region ClientMeter

                            Mapper.CreateMap<ClientMeterEntity, AccountLookupModel>()
                                .ForAllUnmappedMembers(o => o.Ignore());

                            Mapper.CreateMap<AccountLookupModel, ClientMeterEntity>()
                                .ForAllUnmappedMembers(o => o.Ignore());
                            #endregion

                            #region CustomerMobileNumber

                            Mapper.CreateMap<CustomerMobileNumberEntity, CustomerMobileNumberLookupModel>()
                                .ForAllUnmappedMembers(o => o.Ignore());

                            Mapper.CreateMap<CustomerMobileNumberLookupModel, CustomerMobileNumberEntity>()
                                .ForAllUnmappedMembers(o => o.Ignore());
                            #endregion

                            #region CustomerMobileNumberLookup

                            Mapper.CreateMap<CustomerMobileNumberLookupModel, BillingCustomerPremiseModel>()
                                .ForMember(dest => dest.customer_id, opts => opts.MapFrom(src => src.CustomerId))
                                .ForMember(dest => dest.ClientId, opts => opts.MapFrom(src => src.ClientId))
                                .ForMember(dest => dest.phone_1, opts => opts.MapFrom(src => src.MobileNumber))
                                .ForMember(dest => dest.account_id, opts => opts.MapFrom(src => src.AccountId))
                                .ForAllUnmappedMembers(o => o.Ignore());

                            Mapper.CreateMap<BillingCustomerPremiseModel, CustomerMobileNumberLookupModel>()
                               .ForMember(dest => dest.CustomerId, opts => opts.MapFrom(src => src.customer_id))
                               .ForMember(dest => dest.ClientId, opts => opts.MapFrom(src => src.ClientId))
                               .ForMember(dest => dest.MobileNumber, opts => opts.MapFrom(src => src.phone_1))
                               .ForMember(dest => dest.AccountId, opts => opts.MapFrom(src => src.account_id))
                               .ForAllUnmappedMembers(o => o.Ignore());
                            #endregion

                            #region TrumpiaKeywordConfigurationEntity

                            Mapper.CreateMap<TrumpiaKeywordConfigurationEntity, TrumpiaKeywordConfigurationModel>()
                                .ForAllUnmappedMembers(o => o.Ignore());

                            Mapper.CreateMap<TrumpiaKeywordConfigurationModel, TrumpiaKeywordConfigurationEntity>()
                                .ForAllUnmappedMembers(o => o.Ignore());
                            #endregion

                            #region TrumpiaRequestDetail

                            Mapper.CreateMap<TrumpiaRequestDetailEntity, TrumpiaRequestDetailModel>()
                                .ForAllUnmappedMembers(o => o.Ignore());

                            Mapper.CreateMap<TrumpiaRequestDetailModel, TrumpiaRequestDetailEntity>()
                                .ForAllUnmappedMembers(o => o.Ignore());

                            Mapper.CreateMap<TrumpiaRequestDetailEntity, TrumpiaRequestDetailReportEntity>()
                                .ForAllUnmappedMembers(o => o.Ignore());

                            Mapper.CreateMap<TrumpiaRequestDetailModel, TrumpiaRequestDetailReportEntity>()
                                .ForAllUnmappedMembers(o => o.Ignore());
                            #endregion

                            #region MeterAccountMtu

                            Mapper.CreateMap<RawMeterAccountMtuModel, MeterAccountMtuModel>()
                                .ForMember(dest => dest.ClientId, opts => opts.MapFrom(src => Convert.ToInt32(src.TENANT_ID)))
                                .ForMember(dest => dest.CrossReferenceId, opts => opts.MapFrom(src => Convert.ToInt32(src.CROSS_REFERENCE_ID)))
                                .ForMember(dest => dest.AccountNumber, opts => opts.MapFrom(src => src.ACCOUNT_NUMBER))
                                .ForMember(dest => dest.MeterId, opts => opts.MapFrom(src => Convert.ToInt32(src.METER_ID)))
                                .ForMember(dest => dest.MtuId, opts => opts.MapFrom(src => Convert.ToInt32(src.MTU_ID)))
                                .ForMember(dest => dest.MtuInstallDate, opts => opts.MapFrom(src => Convert.ToDateTime(src.MTU_INSTALL_DATE)))
                                .ForMember(dest => dest.MeterInstallDate, opts => opts.MapFrom(src => Convert.ToDateTime(src.METER_INSTALL_DATE)))
                                .ForMember(dest => dest.Active, opts => opts.MapFrom(src => Convert.ToBoolean(src.ACTIVE)))
                                .ForMember(dest => dest.Longitude, opts => opts.MapFrom(src => src.LONGITUDE))
                                .ForMember(dest => dest.Latitude, opts => opts.MapFrom(src => src.LATITUDE))
                                .ForMember(dest => dest.MeterTypeId, opts => opts.MapFrom(src => Convert.ToInt32(src.METER_TYPE_ID)))
                                .ForMember(dest => dest.MeterSerialNumber, opts => opts.MapFrom(src => src.METER_SERIAL_NUMBER))
                                .ForMember(dest => dest.MtuTypeId, opts => opts.MapFrom(src => Convert.ToInt32(src.MTU_TYPE_ID)))
                                .ForAllUnmappedMembers(o => o.Ignore());

                            Mapper.CreateMap<MeterAccountMtuModel, RawMeterAccountMtuModel>()
                                .ForMember(dest => dest.TENANT_ID, opts => opts.MapFrom(src => src.ClientId))
                                .ForMember(dest => dest.CROSS_REFERENCE_ID, opts => opts.MapFrom(src => src.CrossReferenceId))
                                .ForMember(dest => dest.ACCOUNT_NUMBER, opts => opts.MapFrom(src => src.AccountNumber))
                                .ForMember(dest => dest.METER_ID, opts => opts.MapFrom(src => src.MeterId))
                                .ForMember(dest => dest.MTU_ID, opts => opts.MapFrom(src => src.MtuId))
                                .ForMember(dest => dest.MTU_INSTALL_DATE, opts => opts.MapFrom(src => src.MtuInstallDate))
                                .ForMember(dest => dest.METER_INSTALL_DATE, opts => opts.MapFrom(src => src.MeterInstallDate))
                                .ForMember(dest => dest.ACTIVE, opts => opts.MapFrom(src => src.Active))
                                .ForMember(dest => dest.LONGITUDE, opts => opts.MapFrom(src => src.Longitude))
                                .ForMember(dest => dest.LATITUDE, opts => opts.MapFrom(src => src.Latitude))
                                .ForMember(dest => dest.METER_TYPE_ID, opts => opts.MapFrom(src => src.MeterTypeId))
                                .ForMember(dest => dest.METER_SERIAL_NUMBER, opts => opts.MapFrom(src => src.MeterSerialNumber))
                                .ForAllUnmappedMembers(o => o.Ignore());

                            Mapper.CreateMap<MtuTypeModel, MeterAccountMtuModel>()
                                .ForMember(dest => dest.MtuTypeDescription, opts => opts.MapFrom(src => src.Description))
                                .ForAllUnmappedMembers(o => o.Ignore());

                            Mapper.CreateMap<MtuTypeModel, MtuTypeEntity>()
                                .ForMember(dest => dest.TableName, opt => opt.Ignore());

                            Mapper.CreateMap<MtuTypeEntity, MtuTypeModel>();

                            Mapper.CreateMap<MeterAccountHistoryEntity, MeterAccountMtuModel>()
                                .ForAllUnmappedMembers(o => o.Ignore());

                            Mapper.CreateMap<MeterAccountMtuModel, MeterAccountHistoryEntity>()
                                .ForAllUnmappedMembers(o => o.Ignore());

                            #endregion

                            #region MeterRead

                            Mapper.CreateMap<RawMeterReadModel, MeterReadModel>()
                                .ForMember(dest => dest.ClientId, opts => opts.MapFrom(src => Convert.ToInt32(src.TENANT_ID)))
                                .ForMember(dest => dest.MeterId, opts => opts.MapFrom(src => src.METER_ID))
                                .ForMember(dest => dest.TransponderId, opts => opts.MapFrom(src => src.TRANSPONDER_ID))
                                .ForMember(dest => dest.TransponderPort, opts => opts.MapFrom(src => src.TRANSPONDER_PORT))
                                .ForMember(dest => dest.CustomerId, opts => opts.MapFrom(src => src.CUSTOMER_ID))
                                .ForMember(dest => dest.Consumption, opts => opts.MapFrom(src => Convert.ToDouble(src.READING_VALUE)))
                                .ForMember(dest => dest.UOMId, opts => opts.MapFrom(src => Convert.ToInt32(src.UNIT_OF_MEASURE)))
                                .ForMember(dest => dest.AmiTimeStamp, opts => opts.MapFrom(src => Convert.ToDateTime(src.READING_DATETIME)))
                                .ForMember(dest => dest.Timezone, opts => opts.MapFrom(src => src.TIMEZONE))
                                .ForMember(dest => dest.BatteryVoltage, opts => opts.MapFrom(src => src.BATTERY_VOLTAGE))
                                .ForMember(dest => dest.CommodityId, opts => opts.MapFrom(src => Convert.ToInt32(src.COMMODITY_ID)))
                                .ForMember(dest => dest.AccountNumber, opts => opts.MapFrom(src => src.ACCOUNT_NUMBER))
                                .ForMember(dest => dest.IntervalType, opts => opts.MapFrom(src => Convert.ToInt32(src.READ_INTERVAL)))
                                .ForMember(dest => dest.StandardUOMConsumption, opts => opts.Ignore())
                                .ForMember(dest => dest.StandardUOMId, opts => opts.Ignore())
                                .ForAllUnmappedMembers(o => o.Ignore());

                            Mapper.CreateMap<MeterReadModel, RawMeterReadModel>()
                                .ForMember(dest => dest.TENANT_ID, opts => opts.MapFrom(src => src.ClientId))
                                .ForMember(dest => dest.METER_ID, opts => opts.MapFrom(src => src.MeterId))
                                .ForMember(dest => dest.TRANSPONDER_ID, opts => opts.MapFrom(src => src.TransponderId))
                                .ForMember(dest => dest.TRANSPONDER_PORT, opts => opts.MapFrom(src => src.TransponderPort))
                                .ForMember(dest => dest.CUSTOMER_ID, opts => opts.MapFrom(src => src.CustomerId))
                                .ForMember(dest => dest.READING_VALUE, opts => opts.MapFrom(src => src.Consumption))
                                .ForMember(dest => dest.UNIT_OF_MEASURE, opts => opts.MapFrom(src => src.UOMId))
                                .ForMember(dest => dest.READING_DATETIME, opts => opts.MapFrom(src => src.AmiTimeStamp))
                                .ForMember(dest => dest.TIMEZONE, opts => opts.MapFrom(src => src.Timezone))
                                .ForMember(dest => dest.BATTERY_VOLTAGE, opts => opts.MapFrom(src => src.BatteryVoltage))
                                .ForMember(dest => dest.COMMODITY_ID, opts => opts.MapFrom(src => src.CommodityId))
                                .ForMember(dest => dest.ACCOUNT_NUMBER, opts => opts.MapFrom(src => src.AccountNumber))
                                .ForMember(dest => dest.READ_INTERVAL, opts => opts.MapFrom(src => src.IntervalType))
                                .ForAllUnmappedMembers(o => o.Ignore());

                            Mapper.CreateMap<RawBaseReadModel, MeterReadModel>()
                               .ForMember(dest => dest.MeterId, opts => opts.MapFrom(src => src.METER_ID))
                               .ForMember(dest => dest.TransponderId, opts => opts.MapFrom(src => src.TRANSPONDER_ID))
                               .ForMember(dest => dest.TransponderPort, opts => opts.MapFrom(src => src.TRANSPONDER_PORT))
                               .ForMember(dest => dest.CustomerId, opts => opts.MapFrom(src => src.CUSTOMER_ID))
                               .ForMember(dest => dest.Consumption, opts => opts.MapFrom(src => Convert.ToDouble(src.READING_VALUE)))
                               .ForMember(dest => dest.UOMId, opts => opts.MapFrom(src => Convert.ToInt32(src.UNIT_OF_MEASURE)))
                               .ForMember(dest => dest.AmiTimeStamp, opts => opts.MapFrom(src => Convert.ToDateTime(src.READING_DATETIME)))
                               .ForMember(dest => dest.Timezone, opts => opts.MapFrom(src => src.TIMEZONE))
                               .ForMember(dest => dest.BatteryVoltage, opts => opts.MapFrom(src => src.BATTERY_VOLTAGE))
                               .ForMember(dest => dest.CommodityId, opts => opts.MapFrom(src => Convert.ToInt32(src.COMMODITY_ID)))
                               .ForAllUnmappedMembers(o => o.Ignore());

                            Mapper.CreateMap<ConsumptionByAccountModel, MeterReadModel>()
                                .ForAllUnmappedMembers(o => o.Ignore());

                            Mapper.CreateMap<MeterReadModel, ConsumptionByAccountModel>()
                                .ForAllUnmappedMembers(o => o.Ignore());

                            Mapper.CreateMap<ConsumptionByAccountModel, ConsumptionByAccountEntity>()
                                .ForAllUnmappedMembers(o => o.Ignore());

                            Mapper.CreateMap<ConsumptionByAccountEntity, ConsumptionByAccountModel>()
                                .ForAllUnmappedMembers(o => o.Ignore());

                            Mapper.CreateMap<TallAmiModel, MeterReadModel>()
                                .ForAllUnmappedMembers(o => o.Ignore());

                            Mapper.CreateMap<MeterReadModel, TallAmiModel>()
                                .ForAllUnmappedMembers(o => o.Ignore());
                            
                            #endregion

                            #region LogView
                            Mapper.CreateMap<LogViewModel, LogEntity>()
                                .ForAllUnmappedMembers(o => o.Ignore());

                            Mapper.CreateMap<LogEntity, LogViewModel>()
                                .ForAllUnmappedMembers(o => o.Ignore());
                            #endregion

                            #region DCU Provisioning

                            Mapper.CreateMap<RawDcuProvisioningModel, DcuProvisioningModel>()
                                .ForMember(dest => dest.ClientId, opts => opts.MapFrom(src => Convert.ToInt32(src.TENANT_ID)))
                                .ForMember(dest => dest.DcuId, opts => opts.MapFrom(src => Convert.ToInt32(src.DCU_ID)))
                                .ForMember(dest => dest.InstalledDate, opts => opts.MapFrom(src => string.IsNullOrWhiteSpace(src.DATE_INSTALLED) ? "" : DateTime.Parse(src.DATE_INSTALLED).ToString("u")))
                                .ForMember(dest => dest.Longitude, opts => opts.MapFrom(src => src.LONGITUDE))
                                .ForMember(dest => dest.Latitude, opts => opts.MapFrom(src => src.LATITUDE))
                                .ForMember(dest => dest.Name, opts => opts.MapFrom(src => src.NAME))
                                .ForAllUnmappedMembers(o => o.Ignore());

                            Mapper.CreateMap<DcuProvisioningModel, RawDcuProvisioningModel>()
                                .ForMember(dest => dest.TENANT_ID, opts => opts.MapFrom(src => src.ClientId))
                                .ForMember(dest => dest.DCU_ID, opts => opts.MapFrom(src => src.DcuId))
                                .ForMember(dest => dest.DATE_INSTALLED, opts => opts.MapFrom(src => src.InstalledDate))
                                .ForMember(dest => dest.LONGITUDE, opts => opts.MapFrom(src => src.Longitude))
                                .ForMember(dest => dest.LATITUDE, opts => opts.MapFrom(src => src.Latitude))
                                .ForMember(dest => dest.NAME, opts => opts.MapFrom(src => src.Name))
                                .ForAllUnmappedMembers(o => o.Ignore());

                            Mapper.CreateMap<DcuProvisioningEntity, DcuProvisioningModel>()
                                .ForAllUnmappedMembers(o => o.Ignore());

                            Mapper.CreateMap<DcuProvisioningModel, DcuProvisioningEntity>()
                                .ForMember(dest => dest.InstalledDate, opts => opts.MapFrom(src => string.IsNullOrWhiteSpace(src.InstalledDate) ? "" : DateTime.Parse(src.InstalledDate).ToString("u")))
                                .ForAllUnmappedMembers(o => o.Ignore());
                            #endregion

                            #region Dcu Alarm related Models

                            Mapper.CreateMap<RawDcuAlarmModel, DcuAlarmModel>()
                               .ForMember(dest => dest.AlarmCode, opts => opts.MapFrom(src => src.ALARM_CODE))
                               .ForMember(dest => dest.AlarmTime, opts => opts.MapFrom(src => Convert.ToDateTime(src.ALARM_TIME)))
                               .ForMember(dest => dest.ClientId, opts => opts.MapFrom(src => Convert.ToInt32(src.TENANT_ID)))
                               .ForMember(dest => dest.DcuId, opts => opts.MapFrom(src => Convert.ToInt32(src.DCU_ID)))
                               .ForAllUnmappedMembers(o => o.Ignore());

                            Mapper.CreateMap<DcuAlarmModel, RawDcuAlarmModel>()
                               .ForMember(dest => dest.ALARM_CODE, opts => opts.MapFrom(src => src.AlarmCode))
                               .ForMember(dest => dest.ALARM_TIME, opts => opts.MapFrom(src => src.AlarmTime))
                               .ForMember(dest => dest.TENANT_ID, opts => opts.MapFrom(src => src.ClientId))
                               .ForMember(dest => dest.DCU_ID, opts => opts.MapFrom(src => src.DcuId))
                               .ForAllUnmappedMembers(o => o.Ignore());

                            Mapper.CreateMap<DcuAlarmTypeModel, DcuAlarmModel>()
                               .ForMember(dest => dest.AlarmCode, opts => opts.MapFrom(src => src.AlarmCode))
                               .ForMember(dest => dest.ComponentType, opts => opts.MapFrom(src => src.ComponentType))
                               .ForMember(dest => dest.NotificationType, opts => opts.MapFrom(src => src.NotificationType))
                               .ForMember(dest => dest.Severity, opts => opts.MapFrom(src => src.Severity))
                               .ForMember(dest => dest.UserFriendlyDescription, opts => opts.MapFrom(src => src.UserFriendlyDescription))
                               .ForAllUnmappedMembers(o => o.Ignore());

                            Mapper.CreateMap<DcuAlarmModel, DcuAlarmTypeModel>()
                               .ForMember(dest => dest.AlarmCode, opts => opts.MapFrom(src => src.AlarmCode))
                               .ForMember(dest => dest.ComponentType, opts => opts.MapFrom(src => src.ComponentType))
                               .ForMember(dest => dest.NotificationType, opts => opts.MapFrom(src => src.NotificationType))
                               .ForMember(dest => dest.Severity, opts => opts.MapFrom(src => src.Severity))
                               .ForMember(dest => dest.UserFriendlyDescription, opts => opts.MapFrom(src => src.UserFriendlyDescription))
                               .ForAllUnmappedMembers(o => o.Ignore());

                            Mapper.CreateMap<DcuAlarmTypeEntity, DcuAlarmTypeModel>()
                           .ForAllUnmappedMembers(o => o.Ignore());

                            Mapper.CreateMap<DcuAlarmTypeModel, DcuAlarmTypeEntity>()
                           .ForAllUnmappedMembers(o => o.Ignore());

                            Mapper.CreateMap<DcuAlarmEntity, DcuAlarmModel>()
                                .ForAllUnmappedMembers(o => o.Ignore());

                            Mapper.CreateMap<DcuAlarmModel, DcuAlarmEntity>()
                                .ForAllUnmappedMembers(o => o.Ignore());

                            Mapper.CreateMap<DcuAlarmHistoryEntity, DcuAlarmModel>()
                                .ForAllUnmappedMembers(o => o.Ignore());

                            Mapper.CreateMap<DcuAlarmModel, DcuAlarmHistoryEntity>()
                                .ForAllUnmappedMembers(o => o.Ignore());

                            Mapper.CreateMap<DcuAlarmByCodeEntity, DcuAlarmModel>()
                                .ForAllUnmappedMembers(o => o.Ignore());

                            Mapper.CreateMap<DcuAlarmModel, DcuAlarmByCodeEntity>()
                                .ForAllUnmappedMembers(o => o.Ignore());

                            Mapper.CreateMap<DcuAlarmByCodeHistoryEntity, DcuAlarmModel>()
                                .ForAllUnmappedMembers(o => o.Ignore());

                            Mapper.CreateMap<DcuAlarmModel, DcuAlarmByCodeHistoryEntity>()
                                .ForAllUnmappedMembers(o => o.Ignore());

                            Mapper.CreateMap<DcuAlarmTypeEntity, DcuAlarmType>()
                                .ForAllUnmappedMembers(o => o.Ignore());

                            Mapper.CreateMap<DcuAlarmType, DcuAlarmTypeEntity>()
                                .ForAllUnmappedMembers(o => o.Ignore());

                            #endregion

                            #region DCU Call Data

                            Mapper.CreateMap<RawDcuCallDataModel, DcuCallDataModel>()
                                .ForMember(dest => dest.ClientId, opts => opts.MapFrom(src => Convert.ToInt32(src.TENANT_ID)))
                                .ForMember(dest => dest.DcuId, opts => opts.MapFrom(src => Convert.ToInt32(src.DCU_ID)))
                                .ForMember(dest => dest.CallStart, opts => opts.MapFrom(src => Convert.ToDateTime(src.CALL_START)))
                                .ForMember(dest => dest.CallEnd, opts => opts.MapFrom(src => Convert.ToDateTime(src.CALL_END)))
                                .ForMember(dest => dest.CallType, opts => opts.MapFrom(src => src.CALL_TYPE))
                                .ForMember(dest => dest.Received, opts => opts.MapFrom(src => Convert.ToInt32(src.RECEIVED)))
                                .ForMember(dest => dest.Expected, opts => opts.MapFrom(src => Convert.ToInt32(src.EXPECTED)))
                                .ForMember(dest => dest.HungUp, opts => opts.MapFrom(src => Convert.ToBoolean(src.HUNG_UP)))
                                .ForMember(dest => dest.Cleared, opts => opts.MapFrom(src => Convert.ToBoolean(src.CLEARED)))
                                .ForAllUnmappedMembers(o => o.Ignore());

                            Mapper.CreateMap<DcuCallDataModel, RawDcuCallDataModel>()
                                .ForMember(dest => dest.TENANT_ID, opts => opts.MapFrom(src => src.ClientId))
                                .ForMember(dest => dest.DCU_ID, opts => opts.MapFrom(src => src.DcuId))
                                .ForMember(dest => dest.CALL_START, opts => opts.MapFrom(src => src.CallStart))
                                .ForMember(dest => dest.CALL_END, opts => opts.MapFrom(src => src.CallEnd))
                                .ForMember(dest => dest.CALL_TYPE, opts => opts.MapFrom(src => src.CallType))
                                .ForMember(dest => dest.RECEIVED, opts => opts.MapFrom(src => src.Received))
                                .ForMember(dest => dest.EXPECTED, opts => opts.MapFrom(src => src.Expected))
                                .ForMember(dest => dest.HUNG_UP, opts => opts.MapFrom(src => src.HungUp))
                                .ForMember(dest => dest.CLEARED, opts => opts.MapFrom(src => src.Cleared))
                                .ForAllUnmappedMembers(o => o.Ignore());

                            Mapper.CreateMap<DcuCallDataHistoryEntity, DcuCallDataModel>()
                                .ForAllUnmappedMembers(o => o.Ignore());

                            Mapper.CreateMap<DcuCallDataModel, DcuCallDataHistoryEntity>()
                                .ForAllUnmappedMembers(o => o.Ignore());
                            #endregion

                            #region Mtu Alarm Mapping Info

                            Mapper.CreateMap<AlarmModel, RawAlarmModel>()
                            .ForMember(dest => dest.TENANT_ID, opts => opts.MapFrom(src => src.ClientId))
                            .ForMember(dest => dest.ALARM_GROUP_ID, opts => opts.MapFrom(src => src.AlarmGroupId))
                            .ForMember(dest => dest.ALARM_ID, opts => opts.MapFrom(src => src.AlarmId))
                            .ForMember(dest => dest.ALARM_SOURCE_ID, opts => opts.MapFrom(src => src.AlarmSourceId))
                            .ForMember(dest => dest.ALARM_SOURCE_TYPE_ID, opts => opts.MapFrom(src => src.AlarmSourceTypeId))
                            .ForMember(dest => dest.ALARM_TIME, opts => opts.MapFrom(src => src.AlarmTime))
                            .ForMember(dest => dest.ALARM_TYPE_ID, opts => opts.MapFrom(src => src.AlarmTypeId))
                            .ForMember(dest => dest.DETAILS, opts => opts.MapFrom(src => src.Details))
                            .ForAllUnmappedMembers(o => o.Ignore());

                            Mapper.CreateMap<RawAlarmModel, AlarmModel>()
                            .ForMember(dest => dest.ClientId, opts => opts.MapFrom(src => Convert.ToInt32(src.TENANT_ID)))
                            .ForMember(dest => dest.AlarmGroupId, opts => opts.MapFrom(src => Convert.ToInt32(src.ALARM_GROUP_ID)))
                            .ForMember(dest => dest.AlarmId, opts => opts.MapFrom(src => Convert.ToInt32(src.ALARM_ID)))
                            .ForMember(dest => dest.AlarmSourceId, opts => opts.MapFrom(src => Convert.ToInt32(src.ALARM_SOURCE_ID)))
                            .ForMember(dest => dest.AlarmSourceTypeId, opts => opts.MapFrom(src => Convert.ToInt32(src.ALARM_SOURCE_TYPE_ID)))
                            .ForMember(dest => dest.AlarmTime, opts => opts.MapFrom(src => Convert.ToDateTime(src.ALARM_TIME)))
                            .ForMember(dest => dest.AlarmTypeId, opts => opts.MapFrom(src => Convert.ToInt32(src.ALARM_TYPE_ID)))
                            .ForMember(dest => dest.Details, opts => opts.MapFrom(src => src.DETAILS))
                            .ForAllUnmappedMembers(o => o.Ignore());

                            Mapper.CreateMap<AlarmEntity, AlarmModel>()
                             .ForAllUnmappedMembers(o => o.Ignore());

                            Mapper.CreateMap<AlarmModel, AlarmEntity>()
                                .ForAllUnmappedMembers(o => o.Ignore());

                            Mapper.CreateMap<AlarmTypeEntity, AlarmTypeModel>()
                            .ForAllUnmappedMembers(o => o.Ignore());

                            Mapper.CreateMap<AlarmTypeModel, AlarmTypeEntity>()
                                .ForAllUnmappedMembers(o => o.Ignore());

                            Mapper.CreateMap<AlarmTypeModel, AlarmModel>()
                             .ForMember(dest => dest.AlarmSourceTypeId, opts => opts.MapFrom(src => src.AlarmSourceTypeId))
                             .ForMember(dest => dest.AlarmGroupId, opts => opts.MapFrom(src => src.AlarmGroupId))
                             .ForMember(dest => dest.AlarmGroupName, opts => opts.MapFrom(src => src.AlarmGroupName))
                             .ForMember(dest => dest.AlarmSourceType, opts => opts.MapFrom(src => src.AlarmSourceType))
                             .ForMember(dest => dest.AlarmSourceTypeId, opts => opts.MapFrom(src => src.AlarmSourceTypeId))
                             .ForMember(dest => dest.AlarmTypeId, opts => opts.MapFrom(src => src.AlarmTypeId))
                             .ForMember(dest => dest.AlarmTypeName, opts => opts.MapFrom(src => src.AlarmTypeName))
                             .ForAllUnmappedMembers(o => o.Ignore());

                            Mapper.CreateMap<AlarmModel, AlarmTypeModel>()
                             .ForMember(dest => dest.AlarmSourceTypeId, opts => opts.MapFrom(src => src.AlarmSourceTypeId))
                             .ForMember(dest => dest.AlarmGroupId, opts => opts.MapFrom(src => src.AlarmGroupId))
                             .ForMember(dest => dest.AlarmGroupName, opts => opts.MapFrom(src => src.AlarmGroupName))
                             .ForMember(dest => dest.AlarmSourceType, opts => opts.MapFrom(src => src.AlarmSourceType))
                             .ForMember(dest => dest.AlarmSourceTypeId, opts => opts.MapFrom(src => src.AlarmSourceTypeId))
                             .ForMember(dest => dest.AlarmTypeId, opts => opts.MapFrom(src => src.AlarmTypeId))
                             .ForMember(dest => dest.AlarmTypeName, opts => opts.MapFrom(src => src.AlarmTypeName))
                             .ForAllUnmappedMembers(o => o.Ignore());

                            #endregion

                            #region MeterTypes

                            Mapper.CreateMap<MeterTypeEntity, MeterTypeModel>()
                           .ForAllUnmappedMembers(o => o.Ignore());

                            Mapper.CreateMap<MeterTypeModel, MeterTypeEntity>()
                                .ForAllUnmappedMembers(o => o.Ignore());

                            #endregion

                            #region ConsumptionAggByAccountModel

                            Mapper.CreateMap<ConsumptionAggByAccountModel, ConsumptionAggByAccountEntity>()
                                .ForAllUnmappedMembers(o => o.Ignore());

                            Mapper.CreateMap<ConsumptionAggByAccountEntity, ConsumptionAggByAccountModel>()
                                .ForAllUnmappedMembers(o => o.Ignore());

                            Mapper.CreateMap<ConsumptionByAccountModel, ConsumptionAggByAccountModel>()
                                .ForAllUnmappedMembers(o => o.Ignore());

                            Mapper.CreateMap<ConsumptionAggByAccountModel, ConsumptionAggByAccountModel>()
                                .ForAllUnmappedMembers(o => o.Ignore());

                            #endregion

                            #region MeterHistoryByMeter

                            Mapper.CreateMap<MeterHistoryByMeterModel, MeterHistoryByMeterEntity>()
                                .ForAllUnmappedMembers(o => o.Ignore());

                            Mapper.CreateMap<MeterHistoryByMeterEntity, MeterHistoryByMeterModel>()
                                .ForAllUnmappedMembers(o => o.Ignore());

                            Mapper.CreateMap<MeterAccountMtuModel, MeterHistoryByMeterModel>()
                                .ForAllUnmappedMembers(o => o.Ignore());

                            #endregion

                            #region LogViewModel
                            Mapper.CreateMap<LogEntity, LogViewModel>()
                            .ForAllUnmappedMembers(o => o.Ignore());

                            Mapper.CreateMap<LogViewModel, LogEntity>()
                                .ForAllUnmappedMembers(o => o.Ignore());
                            #endregion

                            #region EmailRequestDetailModel

                            Mapper.CreateMap<EmailRequestDetailModel, EmailRequestDetailEntity>().ForAllUnmappedMembers(o => o.Ignore());
                            Mapper.CreateMap<EmailRequestDetailEntity, EmailRequestDetailModel>().ForAllUnmappedMembers(o => o.Ignore());
                            Mapper.CreateMap<EmailRequestDetailModel, EmailRequestDetailReportEntity>()
                                .ForAllUnmappedMembers(o => o.Ignore());
                            #endregion

                            #region ClientDTO
                            Mapper.CreateMap<Client, ClientDTO>()
                                .ForAllUnmappedMembers(o => o.Ignore());
                            Mapper.CreateMap<ClientDTO, Client>()
                                .ForAllUnmappedMembers(o => o.Ignore());
                            #endregion


                            #region AccountPreAggCalculationRequestModel

                            Mapper.CreateMap<MeterReadModel, AccountPreAggCalculationRequestModel>()
                             .ForMember(dest => dest.AccountId, opts => opts.MapFrom(src => src.AccountNumber))
                            .ForAllUnmappedMembers(o => o.Ignore());
                            Mapper.CreateMap<AccountPreAggCalculationRequestModel, MeterReadModel>()
                             .ForMember(dest => dest.AccountNumber, opts => opts.MapFrom(src => src.AccountId))
                                .ForAllUnmappedMembers(o => o.Ignore());

                            #endregion

                            Mapper.AssertConfigurationIsValid();
                            _isInitialized = true;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Fatal(ex,"AutoMapperConfig.AutoMapperMappings", "nada");
                throw;
            }
        }

        /// <summary>
        /// Extension Method for configuring unmapped members
        /// </summary>
        /// <typeparam name="TSource">Type to map from</typeparam>
        /// <typeparam name="TDestination">Type to map to</typeparam>
        /// <param name="mapping">Mapping Expression</param>
        /// <param name="memberOptions">MemberConfigurationExpression</param>
        public static void ForAllUnmappedMembers<TSource, TDestination>(this IMappingExpression<TSource, TDestination> mapping, Action<IMemberConfigurationExpression<TSource>> memberOptions)
        {
            var typeMap = Mapper.FindTypeMapFor<TSource, TDestination>();
            foreach (var memberName in typeMap.GetUnmappedPropertyNames())
                mapping.ForMember(memberName, memberOptions);
        }
    }
}
