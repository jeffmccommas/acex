﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Xml;
using Newtonsoft.Json.Linq;
using Formatting = Newtonsoft.Json.Formatting;
using System.Xml.Serialization;
using AO.BusinessContracts;


namespace AO.Business
{
    public class ProcessingArgs : IProcessingArgs
    {

        StringDictionary args = new StringDictionary();

        /// <summary>
        /// possible flesh out some missing parameters
        /// </summary>
        /// <param name="parsstg"></param>
        public void AddIfMissing(string parsstg, bool overWrite = false)
        {
            string[] nvps = parsstg.Trim().Split(new[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
            foreach (string nvp in nvps)
            {
                string[] nvpairs = nvp.Trim().Split('=');
                string nam = nvpairs[0].Trim();
                if (overWrite || (args[nam] == null))
                {
                    if (nvpairs.Length > 1)
                    {
                        args[nam] = nvpairs[1].Trim();
                    }
                    else
                    {
                        args[nam] = "";
                    }
                }
            }
        }

        /// <summary>
        /// possible add and/override parameters
        /// </summary>
        /// <param name="parsstg"></param>
        public void Override(string parsstg)
        {
            AddIfMissing(parsstg, true);
            //string[] nvps = parsstg.Trim().Split(new [] {' '},StringSplitOptions.RemoveEmptyEntries);
            //foreach (string nvp in nvps) {
            //    string[] nvpairs = nvp.Trim().Split('=');
            //    string nam = nvpairs[0].Trim();
            //    string val = (nvpairs.Length > 1) ? nvpairs[1].Trim() : "";
            //    if (args[nam] != null) {
            //        args[nam] = val;
            //    }
            //    else {
            //        args.Add(nam,val);
            //    }
            //}
        }

        /// <summary>
        /// possible add and/override parameters
        /// </summary>
        /// <param name="parsstg"></param>
        public void Override(NameValueCollection parsstg)
        {
            foreach (string nvp in parsstg.Keys)
            {
                if (args[nvp] != null)
                {
                    args[nvp] = parsstg[nvp];
                }
                else
                {
                    args.Add(nvp, parsstg[nvp]);
                }
            }
        }

        // supporting for multiple custom configs -- .\data, .\app_data, .
        private static XmlDocument _customLocal = null;
        private static bool _checkCustomLocal = true;
        private static XmlDocument _customData = null;
        private static bool _checkCustomData = true;
        private static XmlDocument _customAppData = null;
        private static bool _checkCustomAppData = true;

        // places where custom config file might be found.
        private const string _customAppDataPath = "App_Data\\CustomConfig.xml";
        private const string _customDataPath = "data\\CustomConfig.xml";
        private const string _customLocalPath = "CustomConfig.xml";

        //public ProcessingArgs(ProcessingArgs src)
        //{
        //    foreach (string key in src.args.Keys)
        //    {
        //        args.Add(key, src.args[key]);
        //    }
        //}


        /// <summary>
        /// Init from least to most volatile with defference to most volativle to ensure maximum flexibility
        /// compiled in is least flexible
        /// app.config is more flexible
        /// environment is even more flexible
        /// commandline params is even more flexible
        /// </summary>
        
        public ProcessingArgs()
        {
            
            // init from app.config
            foreach (string key in ConfigurationManager.AppSettings.AllKeys)
            {
                this[key] = ConfigurationManager.AppSettings[key];
            }
            // init from environment
            IDictionary env = Environment.GetEnvironmentVariables();
            foreach (string key in env.Keys)
            {
                this[key] = env[key] as string;
            }

            GetCustomConfig(_checkCustomData, ref _customData, _customDataPath);
            GetCustomConfig(_checkCustomAppData, ref _customData, _customAppDataPath);
            GetCustomConfig(_checkCustomLocal, ref _customData, _customLocalPath);

            //// init from command line
            //AddCommandlineArgs(this);
            //if (inargs != null)
            //{
            //    foreach (string stg in inargs)
            //    {
            //        int idx = stg.IndexOf('=');
            //        if (idx > 0)
            //        {
            //            string name = stg.Substring(0, idx);
            //            string val = stg.Substring(idx + 1);
            //            this[name] = val;
            //        }
            //    }
            //}
        }

        public void ApplyCustomConfig()
        {
            if (_checkCustomAppData)_checkCustomAppData = GetCustomConfig(_checkCustomAppData, ref _customAppData, _customAppDataPath);
            if (_checkCustomData) _checkCustomData = GetCustomConfig(_checkCustomData, ref _customData, _customDataPath);
            if (_checkCustomLocal) _checkCustomLocal = GetCustomConfig(_checkCustomLocal, ref _customLocal, _customLocalPath);
        }

        public string GetCustomConfigFile()
        {
            string result = "<noCustomConfig />";
            string custCfgFilePath = "notSet";
            try
            {
                custCfgFilePath = Path.Combine(Directory.GetCurrentDirectory(), _customLocalPath);

                if (!File.Exists(custCfgFilePath))
                {
                    custCfgFilePath = Path.Combine(Directory.GetCurrentDirectory(), _customDataPath);
                }
                if (!File.Exists(custCfgFilePath))
                {
                    custCfgFilePath = Path.Combine(Directory.GetCurrentDirectory(), _customAppDataPath);
                }
                if (File.Exists(custCfgFilePath))
                {
                    using (StreamReader s = new StreamReader(custCfgFilePath))
                    {
                        result = s.ReadToEnd();
                    }
                }
            }
            catch (Exception exc)
            {
                NameValueCollection pars = new NameValueCollection();
                pars.Add("GetCustomConfigFile", custCfgFilePath);
                result = XmlDocUtils.GetXMLExceptionDocument("GetCustomConfigFile", pars, exc).OuterXml;
            }
            return result;
        }

        /// <summary>
        /// overwrite/create the root directory custom config file
        /// </summary>
        /// <param name="doc"></param>
        /// <returns></returns>
        public string SetCustomConfig(XmlDocument doc)
        {
            string rVal = "nothingForSetCustomConfig";
            bool success = false;
            string custCfgFilePath = Path.Combine(Directory.GetCurrentDirectory(), _customLocalPath);
            string custCfgFilePathBak = custCfgFilePath + $".{DateTime.Now.Ticks}.bak";
            bool didBackup = false;
            try
            {
                if (File.Exists(custCfgFilePath))
                {
                    File.Copy(custCfgFilePath, custCfgFilePathBak);
                    didBackup = true;
                    File.Delete(custCfgFilePath);
                }
                using (StreamWriter sw = new StreamWriter(custCfgFilePath))
                {
                    sw.Write(doc.OuterXml);
                }
                success = true;
            }
            catch (Exception exc)
            {
                rVal = $"SetCustomConfig exception : {exc.Message}, {exc.InnerException?.Message}";
                success = false;
                if (didBackup)
                {
                    File.Copy(custCfgFilePath, custCfgFilePathBak);
                }
            }
            if (success)
            {
                //reset
                _customLocal = null;
                _checkCustomLocal = true;
                _customData = null;
                _checkCustomData = true;
                _customAppData = null;
                _checkCustomAppData = true;
                rVal = "SetCustomConfig and reset success";
            }
            return rVal;
        }

        public bool GetCustomConfig(bool checkIt, ref XmlDocument dataDoc, string pathFrag)
        {
            if (dataDoc == null && checkIt)
            {
                // init from custom config
                string path = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, pathFrag);
                if (File.Exists(path))
                {
                    try
                    {
                        dataDoc = new XmlDocument();
                        dataDoc.Load(path);
                    }
                    catch (Exception exc)
                    {
                        this["CustomDataError"] = $" exception on load {pathFrag} : {exc.Message},{exc.InnerException?.Message}";
                        checkIt = false;
                    }
                }
                else
                {
                    checkIt = false;
                }
            }
            if (dataDoc != null && checkIt)
            {
                try
                {
                    XmlNodeList xnl = dataDoc.SelectNodes("//root/item");
                    foreach (XmlNode xmlNode in xnl)
                    {
                        this[xmlNode.Attributes["name"].InnerText] = xmlNode.InnerText;
                    }

                    // for simplified backwards compatibility with old config and added redirect
                    xnl = dataDoc.SelectNodes("//root/add");
                    try
                    {
                        foreach (XmlNode xmlNode in xnl)
                        {
                            if (xmlNode.Attributes["value"] != null)
                            {
                                this[xmlNode.Attributes["key"].InnerText] = xmlNode.Attributes["value"].InnerText;
                            }
                            else if (xmlNode.Attributes["redirect"] != null)
                            {
                                string tVal = xmlNode.Attributes["redirect"].InnerText;
                                if (!string.IsNullOrEmpty(tVal))
                                {
                                    this[xmlNode.Attributes["key"].InnerText] = this[tVal];
                                }
                            }
                            else if (xmlNode.Attributes["unassigned"] != null)
                            {
                                string tVal = xmlNode.Attributes["unassigned"].InnerText;
                                if (!string.IsNullOrEmpty(tVal))
                                {
                                    if (!args.ContainsKey(xmlNode.Attributes["key"].InnerText))
                                    {
                                        this[xmlNode.Attributes["key"].InnerText] = this[tVal];
                                    }
                                }
                            }
                            else if (xmlNode.Attributes["empty"] != null)
                            {
                                string tVal = xmlNode.Attributes["empty"].InnerText;
                                if (!string.IsNullOrEmpty(tVal))
                                {
                                    if (!args.ContainsKey(xmlNode.Attributes["key"].InnerText))
                                    {
                                        this[xmlNode.Attributes["key"].InnerText] = tVal;
                                    }
                                }
                            }
                        }
                    }
                    catch (Exception exc)
                    {
                        this["CustomDataError2"] += $" inner exception on read {pathFrag} : {exc.Message},{exc.InnerException?.Message};";
                        checkIt = false; // on error, no longer want to check this data source
                    }

                    // grouping  with old config and added redirect
                    xnl = dataDoc.SelectNodes("//root/group");
                    try
                    {
                        for (int i = 0; i < xnl.Count; i++)
                        {
                            XmlNode groupNode = xnl[i];
                            string envKey = groupNode.Attributes["env"].InnerText;
                            string envRedir = groupNode.Attributes["redirect"]?.InnerText;
                            if (!string.IsNullOrEmpty(envRedir))
                            { // redirect/pickup an entire other group.  Can overwrite below.
                                string groupPath = $"//root/group[@env='{envRedir}']/add";
                                XmlNodeList redirNodes = dataDoc.SelectNodes(groupPath);
                                foreach (XmlNode node in redirNodes)
                                {
                                    if ((this[$"{envKey}{node.Attributes["key"].InnerText}"] == null) &&
                                        (this[$"{envRedir}{node.Attributes["key"].InnerText}"] != null))
                                    {
                                        this[$"{envKey}{node.Attributes["key"].InnerText}"] =
                                            this[$"{envRedir}{node.Attributes["key"].InnerText}"];
                                        //string targ = $"{envKey}{node.Attributes["key"].InnerText}";
                                        //string src = $"{envRedir}{node.Attributes["key"].InnerText}";
                                        //string val = this[$"{envRedir}{node.Attributes["key"].InnerText}"];
                                        //Console.WriteLine($"create {targ} from {src}  is {val}");
                                    }
                                }
                            }

                            XmlNodeList srcNodes = groupNode.SelectNodes("item");
                            if ((srcNodes != null) && (srcNodes.Count != 0))
                            {
                                foreach (XmlNode xmlNode in srcNodes)
                                {
                                    this[xmlNode.Attributes["name"].InnerText] = xmlNode.InnerText;
                                }
                            }


                            srcNodes = groupNode.SelectNodes("add");
                            foreach (XmlNode node in srcNodes)
                            {
                                if (node.Attributes["value"] != null)
                                {
                                    this[$"{envKey}{node.Attributes["key"].InnerText}"] = node.Attributes["value"].InnerText;
                                }
                                else if (node.Attributes["redirect"] != null)
                                {
                                    string tVal = node.Attributes["redirect"].InnerText;
                                    if (!string.IsNullOrEmpty(tVal))
                                    {
                                        this[$"{envKey}{node.Attributes["key"].InnerText}"] = this[$"{tVal}{node.Attributes["key"].InnerText}"];
                                    }
                                }
                                else if (node.Attributes["unassigned"] != null)
                                {
                                    string tVal = node.Attributes["unassigned"].InnerText;
                                    if (!string.IsNullOrEmpty(tVal))
                                    {
                                        if (this[$"{envKey}{node.Attributes["key"].InnerText}"] == null)
                                        {
                                            this[$"{envKey}{node.Attributes["key"].InnerText}"] = this[$"{tVal}{node.Attributes["key"].InnerText}"];
                                        }
                                    }
                                }
                            }
                        }
                    }
                    catch (Exception exc)
                    {
                        this["CustomDataError2"] += $" group exception on read {pathFrag} : {exc.Message},{exc.InnerException?.Message};";
                        checkIt = false; // on error, no longer want to check this data source
                    }


                }
                catch (Exception exc)
                {
                    this["CustomDataError2"] += $" exception on read {pathFrag} : {exc.Message},{exc.InnerException?.Message}";
                    checkIt = false; // on error, no longer want to check this data source
                }
            }
            return checkIt;
        }

        public void AddCommandlineArgs(ProcessingArgs pa)
        {
            foreach (string stg in Environment.GetCommandLineArgs())
            {
                int idx = stg.IndexOf('=');
                if (idx > 0)
                {
                    string name = stg.Substring(0, idx);
                    string val = stg.Substring(idx + 1);
                    pa[name] = val;
                }
            }
        }


        private const string masksStg = "password,accountkey,hidethisstring";
        private const string maskedVal = "...requestAccess...";
        public string AsJson(bool justArgs = false, bool secure = false)
        {
            JObject jo = new JObject();
            JArray ja = new JArray();
            StringDictionary useArgs;
            string masksExt = masksStg + "," + GetControlVal("secMaskList", "reallyreallyhide");  // possibly additional things to mask
            string[] maskVals = masksExt.Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
            string hiddenStg = GetControlVal("secMaskVal", maskedVal);
            if (!secure)
            { // maybe override

                int day = DateTime.Now.Day;
                int hour = DateTime.Now.Hour;
                if (GetControlVal("oversecj", "no").Equals(day + hour.ToString()))
                {
                    secure = true;
                }
            }
            if (justArgs)
            {
                useArgs = new StringDictionary();
                foreach (string stg in Environment.GetCommandLineArgs())
                {
                    int idx = stg.IndexOf('=');
                    if (idx > 0)
                    {
                        string name = stg.Substring(0, idx);
                        string val = stg.Substring(idx + 1);
                        if (useArgs.ContainsKey(name))
                        {
                            useArgs[name] = val;
                        }
                        else
                        {
                            useArgs.Add(name, val);
                        }
                    }
                }
            }
            else
            {
                useArgs = args;
            }
            foreach (string key in useArgs.Keys)
            {
                //JObject j = new JObject(key, new JValue(args[key]));
                JObject j = new JObject();
                string val = useArgs[key];
                if (!secure)
                {
                    foreach (string maskVal in maskVals)
                    {
                        if (key.ToLower().Contains(maskVal) || (val != null) && val.ToLower().Contains(maskVal))
                        {
                            val = hiddenStg;
                        }
                    }
                }
                j.Add(key, new JValue(val));
                ja.Add(j);
            }
            jo.Add("settings", ja);
            return jo.ToString();
        }

        /// <summary>
        /// make this smarter
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public bool IsAsserted(string key)
        {
            if ((args[key] != null) && (args[key] != "0"))
            {
                return true;
            }
            return false;
        }

        public string this[string key]
        {
            get
            {
                return args[key];
            }
            set
            {
                if (args.ContainsKey(key))
                {
                    args[key] = value;
                }
                else
                {
                    args.Add(key, value);
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="vsrc">older API table</param>
        /// <param name="venv">older API environment</param>
        /// <param name="vval">older API add'l</param>
        /// <param name="pa"></param>
        public static void AccumulateRequestPars(string vsrc, string venv, string vval, ProcessingArgs pa)
        {
            pa["reqVsrc"] = vsrc;
            pa["reqVenv"] = venv;
            pa["reqVval"] = vval;
            pa["contentType"] = "application/xml";
            pa["contextRecs"] = pa.GetControlVal("contextRecs", "100");
            ExtractProcessingVars(pa);
        }


        public static void AccumulateRequestPars(HttpListenerRequest req, ProcessingArgs pa)
        {
            //pa["reqVsrc"] = pa.GetControlVal("vsrc","ami");
            //pa["reqVenv"] = pa.GetControlVal("venv", "dev");
            //pa["reqVval"] = pa.GetControlVal("vval", "6");
            pa["contentType"] = "application/xml";
            pa["contextRecs"] = pa.GetControlVal("contextRecs", "100");
            pa.Override(req.Headers);
            pa.Override(req.QueryString);

            if (req != null)
            {
                //IEnumerable<string> headerValues;
                if (ConfigurationManager.AppSettings.Get("requireAuth") != "gounsafe")
                {
                    if (req.Headers["X-BIO-DiagUser"] == null)
                    {
                        throw new Exception("disallowed1");
                    }
                    else
                    {
                        if (!req.Headers["X-BIO-DiagUser"].Equals("AclaraDiag"))
                        {
                            throw new Exception("disallowed2");
                        }
                    }
                }
                if (req.Headers["X-BIO-etag"] != null)
                {
                    pa["myEtag"] = req.Headers["X-BIO-etag"];
                }
                if (req.Headers["Accept"] != null)
                {
                    pa["Accept"] = req.Headers["Accept"];
                }
                if (req.Headers["Content-Type"] != null)
                {
                    pa["contentType"] = req.Headers["Content-Type"];
                }

                if (req.HasEntityBody)
                {
                    Stream body = req.InputStream;
                    Encoding encoding = req.ContentEncoding;

                    using (StreamReader reader = new StreamReader(body, encoding))
                    {
                        //reqBody = reader.ReadToEnd();
                        pa["reqContent"] = reader.ReadToEnd();
                    }
                }

            }
            ExtractProcessingVars(pa);
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="req"></param>
        /// <param name="vsrc">older API table</param>
        /// <param name="venv">older API environment</param>
        /// <param name="vval">older API add'l</param>
        /// <param name="pa"></param>
        public static void AccumulateRequestPars(HttpRequestMessage req, string vsrc, string venv, string vval, ProcessingArgs pa)
        {
            pa["reqVsrc"] = vsrc;
            pa["reqVenv"] = venv;
            pa["reqVval"] = vval;
            pa["contentType"] = "application/xml";
            pa["contextRecs"] = pa.GetControlVal("contextRecs", "100");

            if (req != null)
            {
                IEnumerable<string> headerValues;
                if (ConfigurationManager.AppSettings.Get("requireAuth") != "gounsafe")
                {
                    if (!req.Headers.TryGetValues("X-BIO-DiagUser", out headerValues))
                    {
                        throw new Exception("disallowed1");
                    }
                    else
                    {
                        if (!headerValues.FirstOrDefault().Equals("AclaraDiag"))
                        {
                            throw new Exception("disallowed2");
                        }
                    }
                }
                if (req.Headers.TryGetValues("X-BIO-etag", out headerValues))
                {
                    pa["myEtag"] = headerValues.FirstOrDefault();
                }
                if (req.Headers.TryGetValues("Accept", out headerValues))
                {
                    pa["Accept"] = headerValues.FirstOrDefault();
                }
                if (req.Headers.TryGetValues("Content-Type", out headerValues))
                {
                    pa["contentType"] = headerValues.FirstOrDefault();
                }
                if (req.Content != null)
                {
                    pa["reqContent"] = req.Content.ReadAsStringAsync().Result;
                }
            }
            ExtractProcessingVars(pa);
        }

        private static void ExtractProcessingVars(ProcessingArgs pa)
        {
            if (!pa.GetControlVal("alreadyExtractedVars", "nope").Equals("yes"))
            {
                pa.ApplyCustomConfig();
                if (!string.IsNullOrEmpty(pa["maxrows"]))
                {
                    // want to make it hard to ask for insane amounts of data
                    int maxRows = 10000;
                    int tval;
                    if (int.TryParse(pa["maxrows"], out tval))
                    {
                        maxRows = tval;
                    }
                    else
                    {
                        maxRows = 10000;
                    }
                    if (maxRows > 100000)
                    {
                        maxRows = 100000;
                    }
                    pa["maxrows"] = maxRows.ToString();
                }
                else
                {
                    pa["maxrows"] = "10000";
                }
                string useEnv = pa.GetControlVal("reqVenv", "nada");
                if (useEnv.Equals("current"))
                {
                    useEnv = ConfigurationManager.AppSettings["AMiCassandraKey"] ?? "work";
                    pa["aclenv"] = useEnv;
                }
                else
                {
                    pa["aclenv"] = useEnv.Equals("nada") ? "perf" : pa["reqVenv"];
                }
                string useVal = pa.GetControlVal("reqVval", "nada");
                pa["qFilter"] = useVal.Equals("nada") ? "" : $"level = '{pa["reqVval"]}'";

                if (pa.GetControlVal("bodyOverride", "nope").Equals("yes"))
                {
                    // initialize params from the body payload.
                    List<string> cassData = new List<string>();
                    List<string> sqlData = new List<string>();
                    List<string> opData = new List<string>();
                    List<string> roData = new List<string>();
                    List<string> fiData = new List<string>();
                    List<string> vaData = new List<string>();
                    List<string> heData = new List<string>();
                    List<string> enData = new List<string>();
                    List<string> tyData = new List<string>();
                    List<string> inData = new List<string>();

                    if (!String.IsNullOrEmpty(pa["reqContent"]))
                    {
                        string[] plines = pa["reqContent"].Split('\n');
                        for (int i = 0; (i < plines.Length) && !String.IsNullOrEmpty(plines[i]); i++)
                        {
                            string qLine = plines[i].Trim();
                            pa["qSel"] = qLine;
                            int specLen = 2;
                            if (!String.IsNullOrEmpty(qLine) && qLine.Length > specLen)
                            {
                                // expect #[specChar][specdata]
                                char specChar = qLine[1]; // control sequence is #[specChar][specdata]
                                string specData = qLine.Substring(specLen);
                                switch (specChar)
                                {
                                    case 'C':
                                    {
                                        cassData.Add(specData);
                                    }
                                        break;
                                    case 'E':
                                    {
                                        enData.Add(specData);
                                    }
                                        break;
                                    case 'F':
                                    {
                                        fiData.Add(specData);
                                    }
                                        break;
                                    case 'H':
                                    {
                                        heData.Add(specData);
                                    }
                                        break;
                                    case 'I':
                                    {
                                        inData.Add(specData);
                                    }
                                        break;
                                    case 'P':
                                    {
                                        opData.Add(specData);
                                    }
                                        break;
                                    case 'R':
                                    {
                                        roData.Add(specData);
                                    }
                                        break;
                                    case 'S':
                                    {
                                        sqlData.Add(specData);
                                    }
                                        break;
                                    case 'T':
                                    {
                                        tyData.Add(specData);
                                    }
                                        break;
                                    case 'V':
                                    {
                                        vaData.Add(specData);
                                    }
                                        break;
                                }
                            }
                        }
                        for (int i = 0; i < cassData.Count; i++)
                        {
                            pa[$"qSel{i}"] = cassData[i];
                        }
                        for (int i = 0; i < enData.Count; i++)
                        {
                            pa[$"enOper{i}"] = enData[i];
                        }
                        for (int i = 0; i < sqlData.Count; i++)
                        {
                            pa[$"qFilter{i}"] = sqlData[i];
                            if (i == 0) pa["qFilter"] = sqlData[i]; // old!!!!!
                        }
                        for (int i = 0; i < opData.Count; i++)
                        {
                            pa[$"qOper{i}"] = opData[i];
                        }
                        for (int i = 0; i < roData.Count; i++)
                        {
                            pa[$"roOper{i}"] = roData[i];
                        }
                        for (int i = 0; i < fiData.Count; i++)
                        {
                            pa[$"fiOper{i}"] = fiData[i];
                        }
                        for (int i = 0; i < heData.Count; i++)
                        {
                            pa[$"heOper{i}"] = heData[i];
                        }
                        for (int i = 0; i < inData.Count; i++)
                        {
                            //#I has col1=val1,col2=val2      for blank: #I col1=,col2=val2,col3=,
                            pa[$"inOper{i}"] = inData[i];
                        }
                        for (int i = 0; i < tyData.Count; i++)
                        {
                            pa[$"tyOper{i}"] = tyData[i];
                        }
                        for (int i = 0; i < vaData.Count; i++)
                        {
                            pa[$"vaOper{i}"] = vaData[i];
                        }

                        // old single line query/filter
                        //if ((plines.Length > 1) && string.IsNullOrEmpty(pa["qFilter"]) && !string.IsNullOrEmpty(plines[1])) { // maybe supplying complex filtering (old)
                        //    pa["qFilter"] = plines[1];
                        //}
                        //else 
                        if (String.IsNullOrEmpty(pa["qFilter"]))
                        {
                            // otherwise check for simple filtering 
                            pa["qFilter"] = "";
                            if (!String.IsNullOrEmpty(pa["qLevel"]))
                            {
                                // could have come in URL.  use for simplistic filtering.
                                pa["qFilter"] = $"level = '{pa["qLevel"]}'";
                            }
                        }
                    }
                    else
                    {
                        // content overrides primitive uri specification
                        if (!String.IsNullOrEmpty(pa["qLevel"]))
                        {
                            pa["qFilter"] = $"level = '{pa["qLevel"]}'";
                        }
                    }
                    pa["alreadyExtractedVars"] = "yes";
                }
            }
        }


        public string GetControlVal(string parName, string defVal)
        {
            return this[parName] ?? defVal;
        }


        /// <summary>
        /// Get the control val for specified client
        /// </summary>
        /// <param name="clientId"></param>
        /// <param name="parName"></param>
        /// <param name="defVal"></param>
        /// <returns></returns>
        public string GetClientControlVal(string clientId, string parName, string defVal)
        {
            string val = string.Empty;
            if (this[parName] == null) return defVal;
            string values = this[parName] ?? defVal;

            string[] fileMaps = values.Split(new[] { ';' }, StringSplitOptions.RemoveEmptyEntries);
            for (int i = 0; i < fileMaps.Length; i++)
            {
                string[] keyEnv = fileMaps[i].Split(new[] { '=' }, StringSplitOptions.RemoveEmptyEntries);
                if (clientId == keyEnv[0])
                {
                    val = keyEnv[1];
                    break;
                }
            }
            if (string.IsNullOrEmpty(val))
            {
                val = fileMaps[fileMaps.Length - 1].Split(new[] { '=' }, StringSplitOptions.RemoveEmptyEntries)[1];
            }
            return val;
        }


        public int GetControlVal(string parName, int defVal)
        {
            if (!String.IsNullOrEmpty(this[parName]))
            {
                int tVal = defVal;
                if (Int32.TryParse(this[parName], out tVal))
                {
                    defVal = tVal;
                }
            }
            return defVal;
        }

        public DateTime GetControlVal(string parName, DateTime defVal)
        {
            if (!String.IsNullOrEmpty(this[parName]))
            {
                DateTime tVal;
                if (DateTime.TryParse(this[parName], out tVal))
                {
                    defVal = tVal;
                }
            }
            return defVal;
        }
    }


    public class TimingHelper
    {
        private DateTime start;
        private DateTime stop;
        //private DateTime lap;
        private bool isStopped = false;

        public TimingHelper()
        {
            start = DateTime.Now;
        }
        public void Restart()
        {
            start = DateTime.Now;
            isStopped = false;
        }
        public double See => DateTime.Now.Subtract(start).TotalMilliseconds;

        public double Stop()
        {
            stop = DateTime.Now;
            isStopped = true;
            return stop.Subtract(start).TotalMilliseconds;
        }

        public double Diff(TimingHelper reference)
        {
            return (reference != null) ? stop.Subtract(reference.start).TotalMilliseconds : 0;
        }

        public string Complete(string desc, TimingHelper outer = null)
        {
            double stopped = (!isStopped) ? Stop() : stop.Subtract(start).TotalMilliseconds;
            string result = $" {desc}={stopped}ms ";
            if (outer != null)
            {
                result += $" from ref={stop.Subtract(outer.start).TotalMilliseconds}";
            }
            return result;
        }

        public static DateTime RunningTimes(ContextData context, DateTime initial, object incremental, string where, string phase)
        {
            DateTime now = DateTime.Now;
            if (incremental != null)
            {
                string stg = $"{phase} {@where} {now.Subtract((DateTime)incremental).TotalMilliseconds} ms.";
                Console.WriteLine(stg);
                context?.ContextWriteSeg(stg, false, now);
            }
            else
            {
                string stg = $"{phase} {context} accum {now.Subtract(initial).TotalMilliseconds} ms.";
                Console.WriteLine(stg);
                context?.ContextWriteSeg(stg, true, now);
            }
            return now;
        }
    }

    /// <summary>
    /// place holder for this separate copy of processing args
    /// </summary>
    public class ContextData
    {
        public const string StandardTimeFormat = "yyyy-MM-ddTHH:mm:ss.fffzz00";


        public void ContextWriteSeg(string name, bool isBegin, DateTime dateTime)
        {
            string deltat = "";
            if (!isBegin)
            {
                deltat = DateTime.Now.Subtract(dateTime).TotalMilliseconds + " msecs, " + DateTime.Now.Subtract(dateTime).TotalSeconds + " secs";
            }
            string be = isBegin ? "_begin" : "_end";
            //if (_ctxWriter.BaseStream!= null)_ctxWriter.WriteLine($"{RecHdr(name)}{be} {deltat}");
            Console.WriteLine($"{RecHdr(name)}{be} {deltat}");
        }

        private string RecHdr(string hdr)
        {
            return $"[{DateTime.Now.ToString(StandardTimeFormat)}]**{hdr}** ";
        }
    }

    public class JsonHelper
    {
        // TODO this stuff goes in config
        // hack for "key>='val'" ops ( other than basic = which trnaslates cleanly) in qparam
        List<string> opsInQParams = new List<string> { "<", ">", "=", ">=", "<=" };
        List<string> xferParams = new List<string> { "goal", "cols", "orderby", "qlimit" };  // transfer these directly
        List<string> eatItInQParams = new List<string> { "bustCache" };  // throw these away (e.g., chrome anti-cache qparam)


        private string CreateQueryString(IEnumerable<KeyValuePair<string, string>> qparams, ProcessingArgs pa)
        {
            string qstg = "";
            foreach (var kvp in qparams)
            {
                if (eatItInQParams.Contains(kvp.Key))
                {
                    // eat it
                }
                else if (xferParams.Contains(kvp.Key))
                {
                    pa[kvp.Key] = kvp.Value;
                }
                else
                {
                    if (qstg.Length != 0) qstg += " and ";
                    bool done = false;
                    foreach (string op in opsInQParams)
                    {
                        if (kvp.Key.Contains(op))
                        {
                            qstg += $"{kvp.Key}";
                            done = true;
                            break;
                        }
                    }
                    if (!done)
                    {
                        qstg += $"{kvp.Key}='{kvp.Value}'";
                    }
                }
            }
            return qstg;
        }
        private string CreateJsonString(IEnumerable<KeyValuePair<string, string>> qparams, ProcessingArgs pa)
        {
            try
            {
                string qstg = "";
                JArray ja = new JArray();
                foreach (var kvp in qparams)
                {
                    string key = kvp.Key;
                    string val = kvp.Value;
                    JObject j = new JObject();
                    j.Add(key, new JValue(val));
                    ja.Add(j);
                }
                JObject jo = new JObject();
                jo.Add("qparams", ja);
                return jo.ToString(Formatting.None);
            }
            catch (Exception exc)
            {
                JObject jo = new JObject();
                jo.Add("qparams", exc.Message);
                return jo.ToString(Formatting.None);
            }
        }

    }


    public class XmlDocUtils
    {

        public static XmlDocument GetXMLExceptionDocument(string docType, NameValueCollection pars, Exception exc = null)
        {
            XmlDocument xDoc = new XmlDocument();
            XmlNode topNode = xDoc.CreateElement("Errors");
            xDoc.AppendChild(topNode);
            XmlNode bodyNode = xDoc.CreateElement(docType?.Replace(" ", "_") ?? "unknown");
            topNode.AppendChild(bodyNode);
            if (pars != null)
            {
                string[] keys = pars.AllKeys;
                foreach (string stg in keys)
                {
                    XmlNode xn = xDoc.CreateElement(stg);
                    xn.InnerText = pars[stg] ?? "NULL";
                    bodyNode.AppendChild(xn);
                }
            }
            if (exc != null)
            {
                AddExcInfo(exc, xDoc);
            }
            return xDoc;
        }

        /// <summary>
        /// Create an XML document from a bunch of strings. Nodes wil be par0, par1, ... parN.
        /// </summary>
        /// <param name="docType">root node name [unknown].  Might clean up to make them xml path friendly</param>
        /// <param name="pars">values (to be attribute value)  [NULL] </param>
        /// <param name="exc"></param>
        /// <returns></returns>
        public static XmlDocument GetXMLDocument(string docType, string[] pars, Exception exc = null)
        {
            XmlDocument xDoc = new XmlDocument();
            XmlNode topNode = xDoc.CreateElement(docType?.Replace(" ", "_") ?? "unknown");
            XmlNode bodyNode = xDoc.AppendChild(topNode);
            if (pars != null)
            {
                AddParsInfo(pars, xDoc, bodyNode);
            }
            if (exc != null)
            {
                AddExcInfo(exc, xDoc);
            }
            return xDoc;
        }

        private static void AddParsInfo(string[] pars, XmlDocument xDoc, XmlNode bodyNode)
        {
            for (int i = 0; i < pars.Length; i++)
            {
                XmlNode xn = xDoc.CreateElement("par" + i);
                xn.InnerText = pars[i] ?? "NULL";
                bodyNode.AppendChild(xn);
            }
        }

        private static void AddExcInfo(Exception exc, XmlDocument xDoc)
        {
            XmlNode xn = xDoc.CreateElement("exception");
            xn.AppendChild(xDoc.CreateElement("msg")).InnerText = exc.Message;
            if (exc.InnerException != null)
            {
                xn.AppendChild(xDoc.CreateElement("inner")).InnerText = exc.InnerException.Message;
            }
            if (exc.StackTrace != null)
            {
                xn.AppendChild(xDoc.CreateElement("stack")).InnerText = exc.StackTrace;
            }
            xDoc.FirstChild.AppendChild(xn);
        }


        public static string DatasetToXml(DataSet ds)
        {
            using (var memoryStream = new MemoryStream())
            {
                using (TextWriter streamWriter = new StreamWriter(memoryStream))
                {
                    var xmlSerializer = new XmlSerializer(typeof(DataSet));
                    xmlSerializer.Serialize(streamWriter, ds);
                    return Encoding.UTF8.GetString(memoryStream.ToArray());
                }
            }
        }
    }
}
