﻿using System;
using System.Threading.Tasks;
using AO.BusinessContracts;
using AO.DataAccess.Contracts;
using AO.Entities;
using AutoMapper;
using CE.AO.Logging;
using CE.AO.Models;

namespace AO.Business
{
    public class EmailRequestDetail : IEmailRequestDetail
    {
        public LogModel LogModel { get; set; }

        private Lazy<ICassandraRepository> CassandraRepository { get; }

        public EmailRequestDetail(LogModel logModel, Lazy<ICassandraRepository> cassandraRepository)
        {
            LogModel = logModel;
            CassandraRepository = cassandraRepository;
        }

        public EmailRequestDetailModel GetEmailRequestDetail(int clientId, string accountId, string programName,
            string serviceContractId, string insightName, string defaultCommunicationChannel, string date, string eventType)
        {
            var notifiedDate = Convert.ToDateTime(date).Date;

            var emailRequestDetail = CassandraRepository.Value.GetSingle<EmailRequestDetailEntity>(
                   Constants.CassandraTableNames.EmailRequestDetail, e =>
                   e.ClientId == clientId &&
                   e.AccountId == accountId &&
                   e.ProgramName == programName &&
                   e.ServiceContractId == serviceContractId &&
                   e.Insight == insightName &&
                   e.Channel == defaultCommunicationChannel &&
                   e.NotifiedDateTime == notifiedDate &&
                   e.Event == eventType);

            return Mapper.Map<EmailRequestDetailEntity, EmailRequestDetailModel>(emailRequestDetail);
        }

        public async Task<EmailRequestDetailModel> GetEmailRequestDetailAsync(int clientId, string accountId, string programName, string serviceContractId,
            string insightName, string defaultCommunicationChannel, string date)
        {
            var emailRequestDetail = await CassandraRepository.Value.GetSingleAsync<EmailRequestDetailEntity>(
                    Constants.CassandraTableNames.EmailRequestDetail, e => 
                    e.ClientId == clientId && 
                    e.AccountId == accountId && 
                    e.ProgramName ==  programName && 
                    e.ServiceContractId == serviceContractId &&
                    e.Insight == insightName &&
                    e.Channel == defaultCommunicationChannel && 
                    e.NotifiedDateTime == DateTime.Parse(date).Date);

            return Mapper.Map<EmailRequestDetailEntity, EmailRequestDetailModel>(emailRequestDetail);
        }

        public bool InsertOrMergeEmailRequestDetail(EmailRequestDetailModel model)
        {
            try
            {
                // report copy
                var entityReport = Mapper.Map<EmailRequestDetailModel, EmailRequestDetailReportEntity>(model);
                entityReport.LastModifiedDate = DateTime.UtcNow;
                entityReport.RowCreateDate = DateTime.UtcNow.ToString("yyyyMMdd");
                CassandraRepository.Value.InsertOrUpdate(entityReport, Constants.CassandraTableNames.EmailRequestDetailReport);
            }
            catch (Exception ex)
            {
                Logger.Error(ex, LogModel);
            }
            var entity = Mapper.Map<EmailRequestDetailModel, EmailRequestDetailEntity>(model);
            entity.LastModifiedDate = DateTime.UtcNow;
            return CassandraRepository.Value.InsertOrUpdate(entity, Constants.CassandraTableNames.EmailRequestDetail);
        }

        public async Task<bool> InsertOrMergeEmailRequestDetailAsync(EmailRequestDetailModel model)
        {
            var entity = Mapper.Map<EmailRequestDetailModel, EmailRequestDetailEntity>(model);
            await CassandraRepository.Value.InsertOrUpdateAsync(entity, Constants.CassandraTableNames.EmailRequestDetail).ConfigureAwait(false);
            try {
                // report copy
                var entityReport = Mapper.Map<EmailRequestDetailModel, EmailRequestDetailReportEntity>(model);
                entityReport.RowCreateDate = DateTime.UtcNow.ToString("yyyyMMdd");
                await CassandraRepository.Value.InsertOrUpdateAsync(entityReport, Constants.CassandraTableNames.EmailRequestDetailReport);
            }
            catch (Exception ex)
            {
                Logger.Error(ex, LogModel);
            }
            return true;
        }
    }
}
