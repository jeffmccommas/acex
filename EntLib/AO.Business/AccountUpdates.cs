﻿using System;
using System.Threading.Tasks;
using AO.BusinessContracts;
using AO.DataAccess.Contracts;
using AO.Entities;
using AutoMapper;
using CE.AO.Models;

namespace AO.Business
{
    /// <summary>
    /// AccountUpdates class is used to fetch/insert/update the AccountUpdatesModel from/to the database
    /// </summary>
    public class AccountUpdates : IAccountUpdates
    {
        public AccountUpdates(LogModel logModel, Lazy<ICassandraRepository> cassandraRepository)
        {
            LogModel = logModel;
            CassandraRepository = cassandraRepository;
        }
        
        public LogModel LogModel { get; set; }

        private Lazy<ICassandraRepository> CassandraRepository { get; }

        /// <summary>
        /// To insert/update the AccountUpdatesModel to the database
        /// </summary>
        /// <param name="model">AccountUpdatesModel which is to be inserted/updated</param>
        /// <param name="history">Boolean which tells whether the AccountUpdatesModel is latest or not</param>
        /// <returns>True</returns>
        public async Task<bool> InsertOrMergeAccountUpdateAsync(AccountUpdatesModel model, bool history)
        {
            var entity = Mapper.Map<AccountUpdatesModel, AccountUpdatesEntity>(model);
            entity.IsLatest = history;
            entity.UpdatedDate = DateTime.UtcNow;
            await CassandraRepository.Value.InsertOrUpdateAsync(entity,Constants.CassandraTableNames.AccountUpdates).ConfigureAwait(false);
            return await Task.Factory.StartNew(() => true);
        }

        /// <summary>
        /// To fetch the AccountUpdatesModel from the database
        /// </summary>
        /// <param name="client">Client Id of the model</param>
        /// <param name="customerId">Customer Id of the model</param>
        /// <param name="updateType">Update Type of the model</param>
        /// <param name="serviceContractId">Service Contract Id of the model</param>
        /// <param name="history">Boolean value which tells model to be fetched is latest or not</param>
        /// <returns>AccountUpdatesModel model</returns>
        public async Task<AccountUpdatesModel> GetAccountUpdate(int client, string customerId, string updateType,string serviceContractId, bool history)
        {
            var isLatest = !history;

            var accountUpdateDetails =
                await
                    CassandraRepository.Value.GetSingleAsync<AccountUpdatesEntity>(Constants.CassandraTableNames.AccountUpdates,
                        a =>
                            a.ClientId == client && a.CustomerId == customerId && a.UpdateType == updateType &&
                            a.ServiceContractId == serviceContractId && a.IsLatest == isLatest);
            var model = Mapper.Map<AccountUpdatesEntity, AccountUpdatesModel>(accountUpdateDetails);
            return model;
        }

        /// <summary>
        /// To delete the record from the database
        /// </summary>
        /// <param name="clientId">Client Id of the data to be delted</param>
        /// <param name="customerId">Customer Id of the data to be delted</param>
        /// <param name="updateType">Update Type of the data to be delted</param>
        /// <param name="serviceContractId">Service Contract Id of the data to be delted</param>
        /// <param name="isLatest">IsLatest of the data to be delted</param>
        /// <returns>True</returns>
        public async Task<bool> DeleteHistoryAccountUpdateAsyc(int clientId, string customerId, string updateType, string serviceContractId,bool isLatest)
        {
            await
                CassandraRepository.Value.DeleteAsync<AccountUpdatesEntity>(
                    Constants.CassandraTableNames.AccountUpdates,
                    a => a.ClientId == clientId && a.CustomerId == customerId && a.UpdateType == updateType &&
                         a.ServiceContractId == serviceContractId && a.IsLatest == isLatest).ConfigureAwait(false);
            return await Task.Factory.StartNew(() => true);
        }

    }
}
