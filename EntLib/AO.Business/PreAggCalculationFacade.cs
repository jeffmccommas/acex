﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AO.BusinessContracts;
using AutoMapper;
using CE.AO.Models;
using CE.AO.Utilities;
using Enums = CE.AO.Utilities.Enums;

namespace AO.Business
{
    /// <summary>
    /// PreAggCalculationFacade interface is used to do pre aggregations
    /// </summary>
    public class PreAggCalculationFacade : IPreAggCalculationFacade
    {
        private readonly Lazy<IConsumptionByAccount> _consumptionByAccount;
        private readonly Lazy<IConsumptionAggByAccount> _consumptionAggByAccount;
        private readonly Lazy<ITimingMetricsCalculator> _metricsCalculator;
        private readonly LogModel _logModel;

        public PreAggCalculationFacade(LogModel logModel, Lazy<IConsumptionByAccount> consumptionByAccount,
            Lazy<IConsumptionAggByAccount> consumptionAggByAccount, Lazy<ITimingMetricsCalculator> metricsCalculator)
        {
            _consumptionByAccount = consumptionByAccount;
            _consumptionAggByAccount = consumptionAggByAccount;
            _metricsCalculator = metricsCalculator;
            _logModel = logModel;
        }

        /// <summary>
        /// Method to do pre aggregations for an Account
        /// </summary>
        public async Task<bool> CalculateAccountPreAgg(AccountPreAggCalculationRequestModel calculationRequestModel)
        {
            if (calculationRequestModel.CalculationRange == Enums.PreAggCalculationRange.Day ||
                calculationRequestModel.CalculationRange == Enums.PreAggCalculationRange.Hour)
            {
                // Start capturing the time to perform aggregates
                _metricsCalculator.Value.Start(calculationRequestModel.ClientId, BusinessContracts.Enums.Metrics.AccountPreAggregation);

                var clientTimezoneEnum =
                   (Enums.Timezone)Enum.Parse(typeof(Enums.Timezone), Convert.ToString(calculationRequestModel.Timezone));
                var clientTimezone = TimeZoneInfo.FindSystemTimeZoneById(clientTimezoneEnum.GetDescriptionOfEnum());

                IList<ConsumptionAggByAccountModel> consumptionAggByAccountModels = CalculateAggregations(calculationRequestModel, clientTimezone);

                await InsertConsumptionAggByAccountModel(consumptionAggByAccountModels).ConfigureAwait(false);

                // Stop capturing the time to perform aggregates
                _metricsCalculator.Value.Stop(calculationRequestModel.ClientId, BusinessContracts.Enums.Metrics.AccountPreAggregation, _logModel);
            }

            return await Task.Factory.StartNew(() => true);
        }

        /// <summary>
        /// Inserts list of ConsumptionAggByAccountModel models in batch
        /// </summary>        
        private async Task InsertConsumptionAggByAccountModel(IList<ConsumptionAggByAccountModel> models)
        {
            await _consumptionAggByAccount.Value.InsertOrMergeConsumptionAggByAccountAsync(models.ToList());
        }

        /// <summary>
        /// Gets raw consumptions from consumption_by_account table and pre aggregations from concumption_agg_by_account table
        ///  depending on the calculation range and interval type
        /// </summary>        
        private void GetRawAndAggConsumptions(AccountPreAggCalculationRequestModel accountPreAggCalculationRequestModel, TimeZoneInfo clientTimezone,
            out IList<ConsumptionAggByAccountModel> preAggConsumptions, out IList<ConsumptionByAccountModel> rawConsumptions)
        {
            accountPreAggCalculationRequestModel.AmiTimestamp = new DateTime(accountPreAggCalculationRequestModel.AmiTimestamp.Year,
                accountPreAggCalculationRequestModel.AmiTimestamp.Month, accountPreAggCalculationRequestModel.AmiTimestamp.Day,
                accountPreAggCalculationRequestModel.AmiTimestamp.Hour, 0, 0);

            var startTimestampForGettingPreAgg = TimezoneHelper.ConvertToTimeZone(accountPreAggCalculationRequestModel.AmiTimestamp,
                TimeZoneInfo.Utc, clientTimezone);

            preAggConsumptions =
                _consumptionAggByAccount.Value.GetConsumptionAggByAccountAsync(accountPreAggCalculationRequestModel.ClientId,
                    startTimestampForGettingPreAgg.Date, accountPreAggCalculationRequestModel.AccountId).Result;

            //Get consumptions for entire day
            var startTimestampForGettingRawConsumption = accountPreAggCalculationRequestModel.AmiTimestamp.Date;
            var endTimestampForGettingRawConsumption = startTimestampForGettingRawConsumption.AddDays(1);

            rawConsumptions =
                _consumptionByAccount.Value.GetConsumptionByAccountByDateRangeAsync(accountPreAggCalculationRequestModel.ClientId,
                    accountPreAggCalculationRequestModel.AccountId, startTimestampForGettingRawConsumption,
                    endTimestampForGettingRawConsumption).Result;

            // Above steps fetch rawconsumptions for the whole day but for doing hourly aggregates, then we just need raw consumptions for that particular hour.
            // Below if condition checks that raw consumptions for the day is not null, the calculation range is hour and if Interval type is <= 60 
            // because for interval types that are > 60, we cannot do hourly aggregates and we only do daily aggregates.
            if (rawConsumptions != null && rawConsumptions.Count > 0 && rawConsumptions.First().IntervalType <= 60 &&
                accountPreAggCalculationRequestModel.CalculationRange == Enums.PreAggCalculationRange.Hour)
            {
                startTimestampForGettingRawConsumption = new DateTime(accountPreAggCalculationRequestModel.AmiTimestamp.Year,
                    accountPreAggCalculationRequestModel.AmiTimestamp.Month, accountPreAggCalculationRequestModel.AmiTimestamp.Day,
                    accountPreAggCalculationRequestModel.AmiTimestamp.Hour, 0, 0);
                endTimestampForGettingRawConsumption = startTimestampForGettingRawConsumption.AddHours(1);

                var clientTimezoneTime = TimezoneHelper.ConvertToTimeZone(startTimestampForGettingRawConsumption,
                    TimeZoneInfo.Utc, clientTimezone);
                // Check condition where 2 UTC times represent 1 local timezone time and if yes is this the second hour of UTC
                if (TimezoneHelper.IsSecondHour(clientTimezone, clientTimezoneTime, accountPreAggCalculationRequestModel.AmiTimestamp))
                    startTimestampForGettingRawConsumption = startTimestampForGettingRawConsumption.AddHours(-1);
                else if (clientTimezone.IsAmbiguousTime(clientTimezoneTime)) // Is first hour of UTC
                    endTimestampForGettingRawConsumption = endTimestampForGettingRawConsumption.AddHours(1);

                rawConsumptions =
                    rawConsumptions.Where(
                        c =>
                            c.AmiTimeStamp >= startTimestampForGettingRawConsumption &&
                            c.AmiTimeStamp < endTimestampForGettingRawConsumption).ToList();
            }
        }

        /// <summary>
        /// Calculates aggregations depending on the calculation range
        /// </summary>
        private IList<ConsumptionAggByAccountModel> CalculateAggregations(AccountPreAggCalculationRequestModel accountPreAggCalculationRequestModel, TimeZoneInfo clientTimezone)
        {
            IList<ConsumptionAggByAccountModel> preAggConsumptions;
            IList<ConsumptionByAccountModel> rawConsumptions;
            GetRawAndAggConsumptions(accountPreAggCalculationRequestModel, clientTimezone, out preAggConsumptions, out rawConsumptions);

            var preAggConsumptionsToUpsert = new List<ConsumptionAggByAccountModel>();

            if (accountPreAggCalculationRequestModel.CalculationRange == Enums.PreAggCalculationRange.Day)
            {
                IList<int> distinctHours = rawConsumptions.Select(c => c.AmiTimeStamp.Hour).Distinct().ToList();
                foreach (var hour in distinctHours)
                {
                    // Get raw consumption for an hour from list which contains all day's raw consumptions
                    List<ConsumptionByAccountModel> rawConsumptionsForHour = rawConsumptions.Where(c => c.AmiTimeStamp.Hour == hour).ToList();
                    preAggConsumptionsToUpsert.AddRange(AggHourlyUsingRawConsumptions(rawConsumptionsForHour, clientTimezone, preAggConsumptions));
                }
            }
            else if (accountPreAggCalculationRequestModel.CalculationRange == Enums.PreAggCalculationRange.Hour)
            {
                preAggConsumptionsToUpsert.AddRange(AggHourlyUsingRawConsumptions(rawConsumptions, clientTimezone,
                    preAggConsumptions));
            }

            var dailyAgg = AggDaily(preAggConsumptions);
            if (dailyAgg != null)
                preAggConsumptionsToUpsert.Add(dailyAgg);

            return preAggConsumptionsToUpsert;
        }

        /// <summary>
        /// Calculates hourly aggregations depending on the provided raw consumptions for hour
        /// and client's local time zone
        /// </summary>
        private IList<ConsumptionAggByAccountModel> AggHourlyUsingRawConsumptions(
            IList<ConsumptionByAccountModel> rawConsumptionsForHour, TimeZoneInfo clientTimezone,
            IList<ConsumptionAggByAccountModel> preAggConsumptions)
        {
            IList<ConsumptionAggByAccountModel> preAggConsumptionsToUpsert = new List<ConsumptionAggByAccountModel>();
            if (rawConsumptionsForHour != null)
            {
                var firstConsumptionForHour = rawConsumptionsForHour.First();
                foreach (var consumption in rawConsumptionsForHour)
                {
                    consumption.AmiTimeStamp = TimezoneHelper.ConvertToTimeZone(consumption.AmiTimeStamp,
                        TimeZoneInfo.Utc, clientTimezone);
                }

                var consumptionAggForHour =
                    preAggConsumptions.FirstOrDefault(
                        c => c.AggregationEndDateTime.Hour == firstConsumptionForHour.AmiTimeStamp.Hour &&
                             c.AggregationType == Convert.ToString(Enums.PreAggregationTypes.Hour));
                if (consumptionAggForHour == null)
                {
                    consumptionAggForHour =
                        Mapper.Map<ConsumptionByAccountModel, ConsumptionAggByAccountModel>(firstConsumptionForHour);
                    consumptionAggForHour.AggregationType =
                        Convert.ToString(Enums.PreAggregationTypes.Hour);
                    consumptionAggForHour.AggregationEndDateTime =
                        new DateTime(firstConsumptionForHour.AmiTimeStamp.Year,
                            firstConsumptionForHour.AmiTimeStamp.Month, firstConsumptionForHour.AmiTimeStamp.Day,
                            firstConsumptionForHour.AmiTimeStamp.Hour, 0, 0);
                    consumptionAggForHour.StandardUOMConsumption = Convert.ToDouble(0);

                    preAggConsumptions.Add(consumptionAggForHour);
                }

                AggHourly(rawConsumptionsForHour, consumptionAggForHour);

                if (firstConsumptionForHour.IntervalType <= 60)
                {
                    preAggConsumptionsToUpsert.Add(consumptionAggForHour);
                }
            }
            return preAggConsumptionsToUpsert;
        }

        /// <summary>
        /// Aggregates the hourly consumptions provided
        /// </summary>        
        private void AggHourly(IList<ConsumptionByAccountModel> consumptionsForHour, ConsumptionAggByAccountModel consumptionAggForHour)
        {
            consumptionAggForHour.StandardUOMConsumption = consumptionsForHour.Sum(c => c.StandardUOMConsumption);
        }

        /// <summary>
        /// Aggregates the hourly pre aggregations provided
        /// </summary>        
        private ConsumptionAggByAccountModel AggDaily(IList<ConsumptionAggByAccountModel> preAggConsumptions)
        {
            ConsumptionAggByAccountModel dailyAgg = null;
            List<ConsumptionAggByAccountModel> hourlyAggs =
                preAggConsumptions.Where(c => c.AggregationType == Convert.ToString(Enums.PreAggregationTypes.Hour)).ToList();
            if (hourlyAggs.Count > 0)
            {
                dailyAgg =
                   preAggConsumptions.FirstOrDefault(c => c.AggregationType == Convert.ToString(Enums.PreAggregationTypes.Day));
                if (dailyAgg == null)
                {
                    var firstPreAggConsumptions = hourlyAggs.First();
                    dailyAgg = Mapper.Map<ConsumptionAggByAccountModel, ConsumptionAggByAccountModel>(firstPreAggConsumptions);
                    dailyAgg.AggregationType =
                            Convert.ToString(Enums.PreAggregationTypes.Day);
                    dailyAgg.AggregationEndDateTime =
                        new DateTime(firstPreAggConsumptions.AggregationEndDateTime.Year,
                            firstPreAggConsumptions.AggregationEndDateTime.Month,
                            firstPreAggConsumptions.AggregationEndDateTime.Day, 0, 0, 0);
                }

                dailyAgg.StandardUOMConsumption = hourlyAggs.Sum(c => c.StandardUOMConsumption);
            }

            return dailyAgg;
        }
    }
}