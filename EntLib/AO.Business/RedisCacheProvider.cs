﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using AO.BusinessContracts;
using StackExchange.Redis;
using Microsoft.Azure;
using Newtonsoft.Json;

namespace AO.Business
{
    /// <summary>
    /// Redis Cache provider.
    /// Made use of database feature of Redis where every list of items to be cached will be in a separate database
    /// </summary>
    public class RedisCacheProvider : ICacheProvider
    {
        /// <summary>
        /// Redis cache connection multiplexer.
        /// </summary>
        // ReSharper disable once FieldCanBeMadeReadOnly.Local
        private static Lazy<IConnectionMultiplexer> _lazyConnection = new Lazy<IConnectionMultiplexer>(() =>
        {
            var connectionMultiplexer = ConnectionMultiplexer.Connect(CloudConfigurationManager.GetSetting(StaticConfig.RedisCacheConnectionString));
            return connectionMultiplexer;
        });

        /// <summary>
        /// Redis cache connection multiplexer.
        /// </summary>
        public static IConnectionMultiplexer Connection => _lazyConnection.Value;

        /// <summary>
        /// Cache expiration in seconds.
        /// </summary>
        private static int ExpireInSeconds
        {
            get
            {
                var defaultExpireInSeconds = 1800;// 30 minutes
                var expireInSecondsConfigValue = CloudConfigurationManager.GetSetting(StaticConfig.CacheExpireInSeconds);
                if (string.IsNullOrWhiteSpace(expireInSecondsConfigValue)) return defaultExpireInSeconds;
                int expireInSeconds;
                if (int.TryParse(expireInSecondsConfigValue, out expireInSeconds))
                {
                    defaultExpireInSeconds = expireInSeconds;
                }
                return defaultExpireInSeconds;
            }
        }


        /// <summary>
        /// Store object in cache.  If the object already exists then 
        /// the value will be overwritten regardless of type.
        /// </summary>
        /// <param name="key"></param>
        /// <param name="value"></param>
        /// <param name="database"></param>       
        public void Set(string key, object value, int database)
        {
            var absoluteExpiration = DateTime.UtcNow.AddSeconds(ExpireInSeconds);
            var cache = Connection.GetDatabase(database);
            cache.StringSet(key, JsonConvert.SerializeObject(value));
            cache.KeyExpire(key, absoluteExpiration);
        }

        /// <summary>
        /// Retrieve type T object from cache.
        /// </summary>
        /// <param name="key"></param>     
        /// <param name="database"></param>    
        /// <returns></returns>
        public T Get<T>(string key, int database)
        {
            var cache = Connection.GetDatabase(database);
            return JsonConvert.DeserializeObject<T>(cache.StringGet(key));
        }

        /// <summary>
        /// Remove key from cache.
        /// </summary>
        /// <param name="key"></param>
        /// <param name="database"></param>
        /// <returns></returns>
        public bool Remove(string key, int database)
        {
            var cache = Connection.GetDatabase(database);
            return cache.KeyDelete(key);
        }

        /// <summary>
        /// Removes all keys from the database
        /// </summary>        
        /// <returns></returns>
        public async Task FlushAllKeys()
        {
            EndPoint[] endPoints = Connection.GetEndPoints();
            List<Task> tasks = endPoints.Select(endPoint => Connection.GetServer(endPoint).FlushAllDatabasesAsync()).ToList();
            await Task.WhenAll(tasks).ConfigureAwait(false);
        }

        /// <summary>
        /// Removes all keys from the database for the connection string provided
        /// </summary>        
        /// <returns></returns>
        public async Task FlushAllKeys(string connectionString)
        {
            if (!string.IsNullOrWhiteSpace(connectionString))
            {
                var connection = ConnectionMultiplexer.Connect(connectionString);
                EndPoint[] endPoints = connection.GetEndPoints();
                List<Task> tasks = endPoints.Select(endPoint => connection.GetServer(endPoint).FlushAllDatabasesAsync()).ToList();
                await Task.WhenAll(tasks).ConfigureAwait(false);
            }
        }

        /// <summary>
        /// Removes all keys from the database specified
        /// </summary>        
        /// <returns></returns>
        public async Task FlushAllKeys(int database)
        {
            EndPoint[] endPoints = Connection.GetEndPoints();
            List<Task> tasks = endPoints.Select(endPoint => Connection.GetServer(endPoint).FlushDatabaseAsync(database)).ToList();
            await Task.WhenAll(tasks).ConfigureAwait(false);
        }

        /// <summary>
        /// Removes all keys from the database specified and for the connection string provided
        /// </summary>        
        /// <returns></returns>
        public async Task FlushAllKeys(int database, string connectionString)
        {
            if (!string.IsNullOrWhiteSpace(connectionString))
            {
                var connection = ConnectionMultiplexer.Connect(connectionString);
                EndPoint[] endPoints = connection.GetEndPoints();
                List<Task> tasks = endPoints.Select(endPoint => connection.GetServer(endPoint).FlushDatabaseAsync(database)).ToList();
                await Task.WhenAll(tasks).ConfigureAwait(false);
            }
        }
    }
}
