﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using AO.BusinessContracts;
using CE.AO.Logging;
using CE.AO.Models;
using Utilities = CE.AO.Utilities;

namespace AO.Business
{
    /// <summary>
    /// SubscriptionFacade class is used to fetch/insert/update the subscription details from/to the database
    /// </summary>
    public class SubscriptionFacade : ISubscriptionFacade
    {
        // Constants
        private const string ProgramNotKnownError = "The insight is associated with an unknown program.";
        private const string NoProgramsError = "The client is not associated with any programs.";
        private const string InvalidEmailError = "The customer's email address is invalid.";
        private const string InvalidPhoneNumberError = "The customer's phone number is invalid.";
        private const string MissingProgramNameError = "The insight has a missing program name.";
        private const string MissingInsightTypeNameError = "The insight has a missing type name.";
        private const string InsightNotAssociatedWithProgramError =
            "The insight is not associated with the program specified.";
        private const string SubscriberThresholdValueExceedsMaxError =
            "The subscriber has chosen a threshold that is greater than the max allowed.";
        private const string SubscriberThresholdValueExceedsMinError =
            "The subscriber has chosen a threshold that is less than the min allowed.";
        private const string InvalidChannelError = "The insight has an invalid channel.";
        private const string UnableToDetermineCustomerServices = "Unable to determine customer services. Please verify bill data is available for this customer.";
        private const string CustomKeySeperator = ";";
        private const string TrumpiaInvalidSubscriptionCode = "MPSE2303";
        private const string TrumpiaInvalidSubscriptionMessage = "Invalid Subscription {0}; Subcription might be deleted due to blocked number";

        private readonly LogModel _logModel;
        private readonly Lazy<IBilling> _billingManager;
        private readonly Lazy<ISubscription> _subscriptionManager;
        private readonly Lazy<ICustomer> _customerManager;
        private readonly Lazy<INotification> _notificationManager;
        private readonly Lazy<IClientConfigFacade> _clientConfigFacadeManager;
        private readonly Lazy<ISendEmail> _sendEmailManager;
        private readonly Lazy<ITrumpiaRequestDetail> _trumpiaRequestDetailManager;


        public SubscriptionFacade(LogModel logModel, Lazy<IBilling> billingManager,
            Lazy<ISubscription> subscriptionManager, Lazy<ICustomer> customerManager,
            Lazy<INotification> notificationManager, Lazy<IClientConfigFacade> clientConfigFacade,
            Lazy<ISendEmail> sendEmailManager, Lazy<ITrumpiaRequestDetail> trumpiaRequestDetailManager)
        {
            _logModel = logModel;
            _billingManager = billingManager;
            _subscriptionManager = subscriptionManager;
            _customerManager = customerManager;
            _notificationManager = notificationManager;
            _clientConfigFacadeManager = clientConfigFacade;
            _sendEmailManager = sendEmailManager;
            _trumpiaRequestDetailManager = trumpiaRequestDetailManager;
        }

        /// <summary>
        /// To fetch the all subscriptions those are subscribed from the database
        /// </summary>
        /// <param name="clientId">clientId of the data to be fetched</param>
        /// <param name="customerId">customerId of the data to be fetched</param>
        /// <param name="accountId">accountId of the data to be fetched</param>
        /// <returns>List of SubscriptionModel</returns>
        public List<SubscriptionModel> GetDefaultListWithSubscribedPrograms(int clientId, string customerId,
            string accountId)
        {
            // get default subscription settings
            var clientSettings = _clientConfigFacadeManager.Value.GetClientSettings(clientId);
            List<ProgramSettings> programs = clientSettings.Programs;

            // get customer subscription
            IEnumerable<SubscriptionModel> subscriptionModelList = _subscriptionManager.Value.GetCustomerSubscriptions(clientId, customerId, accountId);

            // get merge subscription
            List<SubscriptionModel> mergedSubscriptionList = GetDefaultListWithSubscribedPrograms(clientId, customerId, accountId, programs,
                subscriptionModelList.ToList());

            return mergedSubscriptionList;
        }

        /// <summary>
        /// Validate a subscription insights list
        /// </summary>
        /// <param name="clientId">The current client Id.</param>
        /// <param name="insights">The  list of subscription insights</param>
        /// <returns>A list of invalid insights.</returns>
        public List<SubscriptionModel> ValidateInsightList(int clientId, List<SubscriptionModel> insights)
        {
            var insightErrMsg = string.Empty;
            return ValidateInsightList(clientId, insights, string.Empty, string.Empty, ref insightErrMsg);
        }
        
        /// <summary>
        /// Validate a subscription insights list
        /// </summary>
        /// <param name="clientId">The current client Id.</param>
        /// <param name="insights">The  list of subscription insights</param>
        /// <param name="custEmail">The Email Address of the Customer</param>
        /// <param name="custPhone">The Phone number of the Customer</param>
        /// <param name="insightErrMsg">The specific error found with the Insights.</param>
        /// <returns>A list of invalid insights.</returns>
        public List<SubscriptionModel> ValidateInsightList(int clientId, List<SubscriptionModel> insights,
            string custEmail, string custPhone, ref string insightErrMsg)
        {
            // Get the settings for the current client.
            var clientSettings = _clientConfigFacadeManager.Value.GetClientSettings(clientId);

            // Get the current programs being offered.
            List<ProgramSettings> programs = clientSettings.Programs;

            // No programs found.
            if (!programs.Any())
            {
                insightErrMsg = NoProgramsError;
                return insights;
            }

            // Check that the email address is valid.
            // If they are not valid all insights are invalid.
            if (!Extensions.IsValidEmail(custEmail))
            {
                insightErrMsg = InvalidEmailError;
                return insights;
            }

            // Check that the phone number is valid.
            // If they are not valid all insights are invalid.
            if (!Extensions.IsValidPhone(custPhone))
            {
                insightErrMsg = InvalidPhoneNumberError;
                return insights;
            }

            // For each of the insights.
            foreach (var insight in insights)
            {
                // Check the program associated with the insight for validity.
                if (string.IsNullOrEmpty(insight.ProgramName))
                {
                    insightErrMsg = MissingProgramNameError;
                    return insights;
                }

                // Check if the insight is using a known program.
                if (!programs.Exists(p =>
                    string.Equals(p.ProgramName, insight.ProgramName,
                        StringComparison.CurrentCultureIgnoreCase)))
                {
                    insightErrMsg = ProgramNotKnownError;
                    return insights;
                }

                var activeProgram =
                    programs.Find(
                        p =>
                            string.Equals(p.ProgramName, insight.ProgramName,
                                StringComparison.CurrentCultureIgnoreCase));

                // If the insight associated with the program is not the same one the client wants it is invalid.
                if (string.IsNullOrEmpty(insight.InsightTypeName))
                {
                    insightErrMsg = MissingInsightTypeNameError;
                    return insights;
                }

                var activeInsight =
                    activeProgram.Insights.Find(
                        i =>
                            string.Equals(i.InsightName, insight.InsightTypeName,
                                StringComparison.CurrentCultureIgnoreCase));

                if (activeInsight == null)
                {
                    insightErrMsg = InsightNotAssociatedWithProgramError;
                    return insights;
                }

                // Check if the subscriber chosen threshold is greater that the max allowed.
                if (insight.SubscriberThresholdValue.HasValue &&
                    Math.Abs(insight.SubscriberThresholdValue.Value) > 0 &&
                    !string.IsNullOrEmpty(activeInsight.ThresholdMax) &&
                    insight.SubscriberThresholdValue > double.Parse(activeInsight.ThresholdMax))
                {
                    insightErrMsg = SubscriberThresholdValueExceedsMaxError;
                    return insights;
                }

                // Check if the subscriber chosen threshold is lesser that the min allowed.
                if (insight.SubscriberThresholdValue.HasValue &&
                    Math.Abs(insight.SubscriberThresholdValue.Value) > 0 &&
                    !string.IsNullOrEmpty(activeInsight.ThresholdMin) &&
                    insight.SubscriberThresholdValue < double.Parse(activeInsight.ThresholdMin))
                {
                    insightErrMsg = SubscriberThresholdValueExceedsMinError;
                    return insights;
                }

                // If the selected threshold value is zero then reset it to the default.
                if (insight.SubscriberThresholdValue.HasValue && Math.Abs(insight.SubscriberThresholdValue.Value) <= 0)
                {
                    insight.SubscriberThresholdValue = activeInsight.GetCommodityTypevalue(insight.CommodityKey);
                }

                // Check if chosen channel is allowed
                if (!string.IsNullOrEmpty(insight.Channel) &&
                    !activeInsight.AllowedCommunicationChannel.ToLower(CultureInfo.InvariantCulture).Contains(insight.Channel.ToLower(CultureInfo.InvariantCulture)))
                {
                    insightErrMsg = InvalidChannelError;
                    return insights;
                }
            }
            return new List<SubscriptionModel>();
        }

        /// <summary>
        /// Get Subscribe List
        /// </summary>
        /// <param name="clientId">The current client Id</param>
        /// <param name="customerId">The id of customer</param>
        /// <param name="accountId">The account id of customer</param>
        /// <param name="insights">The  list of subscription insights</param>
        /// <returns>List of SubscriptionModel</returns>
        public List<SubscriptionModel> GetSubscribeList(int clientId, string customerId, string accountId,
            List<SubscriptionModel> insights)
        {
            var insightErrorMsg = string.Empty;
            return GetSubscribeList(clientId, customerId, accountId, insights, ref insightErrorMsg);
        }

        /// <summary>
        /// Get Subscribe List
        /// </summary>
        /// <param name="clientId">The current client Id</param>
        /// <param name="customerId">The id of the customer</param>
        /// <param name="accountId">The account id of the customer</param>
        /// <param name="insights">The  list of subscription insights</param>
        /// <param name="insightErrorMsg">The specific error found with the Insights.</param>
        /// <returns>List of SubscriptionModel</returns>
        public List<SubscriptionModel> GetSubscribeList(int clientId, string customerId, string accountId, List<SubscriptionModel> insights, ref string insightErrorMsg)
        {
            var subscribeProgramInsights = new List<SubscriptionModel>();
            // get default subscription settings
            var clientSettings = _clientConfigFacadeManager.Value.GetClientSettings(clientId);
            List<ProgramSettings> programs = clientSettings.Programs;

            // get customer subscription
            List<SubscriptionModel> subscriptionModelList = _subscriptionManager.Value.GetCustomerSubscriptions(clientId, customerId, accountId).ToList();

            List<SubscriptionModel> existingSubscriptionwithDefault = GetDefaultListWithSubscribedPrograms(clientId, customerId, accountId,
                programs, subscriptionModelList.ToList());
            if (existingSubscriptionwithDefault == null)
            {
                insightErrorMsg = UnableToDetermineCustomerServices;
                return subscribeProgramInsights;
            }

            List<SubscriptionModel> existingUnSubsrcibedInsights = existingSubscriptionwithDefault.ToList().FindAll(i => !i.IsSelected);
            List<SubscriptionModel> tobeSubscribe = insights.FindAll(i => i.IsSelected);
            List<SubscriptionModel> tobeUnsubscribe = insights.FindAll(i => !i.IsSelected);

            if (tobeSubscribe.Any())
            {
                // subscribe
                // program insight list
                foreach (var program in programs.FindAll(p => tobeSubscribe.Exists(i => string.Equals(i.ProgramName, p.ProgramName, StringComparison.CurrentCultureIgnoreCase))))
                {
                    var optInEmailCompleted = false;
                    var optInSmsCompleted = false;
                    // check if the program has sms, email or both
                    var hasSms = false;
                    var hasEmail = false;
                    if (
                        existingSubscriptionwithDefault.Exists(
                            e =>
                                e.ProgramName.Equals(program.ProgramName, StringComparison.InvariantCultureIgnoreCase) && e.Channel.Equals(Utilities.Enums.SubscriptionChannel.SMS.ToString(),
                                    StringComparison.InvariantCultureIgnoreCase)) || tobeSubscribe.Exists(
                            e =>
                                e.ProgramName.Equals(program.ProgramName, StringComparison.InvariantCultureIgnoreCase) && !string.IsNullOrEmpty(e.Channel) && e.Channel.Equals(Utilities.Enums.SubscriptionChannel.SMS.ToString(),
                                    StringComparison.InvariantCultureIgnoreCase)))
                    {
                        hasSms = true;
                    }
                    if (
                        existingSubscriptionwithDefault.Exists(
                            e =>
                                e.ProgramName.Equals(program.ProgramName, StringComparison.InvariantCultureIgnoreCase) && e.Channel.Equals(Utilities.Enums.SubscriptionChannel.Email.ToString(),
                                    StringComparison.InvariantCultureIgnoreCase)) || tobeSubscribe.Exists(
                            e =>
                                e.ProgramName.Equals(program.ProgramName, StringComparison.InvariantCultureIgnoreCase) && !string.IsNullOrEmpty(e.Channel) && e.Channel.Equals(Utilities.Enums.SubscriptionChannel.SMS.ToString(),
                                    StringComparison.InvariantCultureIgnoreCase)))
                    {
                        hasEmail = true;
                    }
                    if (
                        existingSubscriptionwithDefault.Exists(
                            e =>
                                e.ProgramName.Equals(program.ProgramName, StringComparison.InvariantCultureIgnoreCase) && e.Channel.Equals(Utilities.Enums.SubscriptionChannel.EmailAndSMS.ToString(),
                                    StringComparison.InvariantCultureIgnoreCase)) || tobeSubscribe.Exists(
                            e =>
                                e.ProgramName.Equals(program.ProgramName, StringComparison.InvariantCultureIgnoreCase) && !string.IsNullOrEmpty(e.Channel) && e.Channel.Equals(Utilities.Enums.SubscriptionChannel.SMS.ToString(),
                                    StringComparison.InvariantCultureIgnoreCase)))
                    {
                        hasEmail = true;
                        hasSms = true;
                    }

                    // get the existing double opt in status
                    if (
                        subscriptionModelList.Exists(
                            s =>
                                s.AccountId == accountId && customerId == s.CustomerId &&
                                string.Equals(s.ProgramName, program.ProgramName, StringComparison.CurrentCultureIgnoreCase)))
                    {
                        var savedProgram = subscriptionModelList.Find(
                            s =>
                                s.AccountId == accountId && customerId == s.CustomerId &&
                                string.Equals(s.ProgramName, program.ProgramName, StringComparison.CurrentCultureIgnoreCase));
                        optInEmailCompleted = savedProgram.IsEmailOptInCompleted ?? false;
                        optInSmsCompleted = savedProgram.IsSmsOptInCompleted ?? false;
                    }
                    else
                    {
                        if (!program.DoubleOptInRequired)
                        {
                            optInEmailCompleted = true;
                            optInSmsCompleted = true;
                        }
                        else
                        {
                            if (hasEmail && hasSms)
                            {
                                optInSmsCompleted = false;
                                optInEmailCompleted = false;
                            }
                            else if (hasEmail)
                                optInEmailCompleted = false;
                            else if (hasSms)
                                optInSmsCompleted = false;
                        }
                    }

                    var programSubscription = new SubscriptionModel
                    {
                        AccountId = accountId,
                        ClientId = clientId,
                        //CommodityKey = program.CommodityType.ToLower(),
                        CustomerId = customerId,
                        IsSelected = true,
                        ProgramName = program.ProgramName,
                        PremiseId = string.Empty,
                        ServiceContractId = string.Empty,
                        ServicePointId = string.Empty,
                        InsightTypeName = string.Empty,
                        IsSmsOptInCompleted = optInSmsCompleted,
                        IsEmailOptInCompleted = optInEmailCompleted
                    };

                    if (program.DoubleOptInRequired &&
                        ((hasEmail && hasSms && !optInSmsCompleted && !optInEmailCompleted) ||
                         (hasEmail && !optInEmailCompleted) || (hasSms && !optInSmsCompleted)))
                        programSubscription.IsDoubleOptIn = true;

                    // add the program that is not previously subscribed to the list
                    if (!subscribeProgramInsights.Exists(s => s.AccountId == accountId && customerId == s.CustomerId && string.Equals(s.ProgramName, program.ProgramName, StringComparison.CurrentCultureIgnoreCase)))
                        subscribeProgramInsights.Add(programSubscription);

                    // account insights
                    List<SubscriptionModel> accountInsights = tobeSubscribe.FindAll(i => string.IsNullOrEmpty(i.ServiceContractId) && string.Equals(i.ProgramName, program.ProgramName, StringComparison.CurrentCultureIgnoreCase));
                    foreach (var accountDefaultInsight in program.Insights.FindAll(i => i.Level.Equals("account", StringComparison.CurrentCultureIgnoreCase)))
                    {
                        // if the default account insight doesn't exist in the to-be subscrib list, then add to the list as unsubscribed
                        if (
                            !accountInsights.Exists(
                                i => string.Equals(i.InsightTypeName, accountDefaultInsight.InsightName, StringComparison.CurrentCultureIgnoreCase)))
                        {
                            List<SubscriptionModel> existingSubscritionwithDefaultAccountLevel =
                       existingSubscriptionwithDefault.FindAll(
                           i =>
                               string.IsNullOrEmpty(i.ServiceContractId) &&
                               string.Equals(i.ProgramName, program.ProgramName, StringComparison.CurrentCultureIgnoreCase) && i.IsSelected);
                            if (
                                !existingSubscritionwithDefaultAccountLevel.Exists(
                                    i =>
                                        string.Equals(i.InsightTypeName, accountDefaultInsight.InsightName, StringComparison.CurrentCultureIgnoreCase)))
                            {
                                var insightSubscription = new SubscriptionModel
                                {
                                    AccountId = accountId,
                                    ClientId = clientId,
                                    CustomerId = customerId,
                                    IsSelected = false,
                                    ProgramName = program.ProgramName,
                                    InsightTypeName = accountDefaultInsight.InsightName,
                                    Channel = accountDefaultInsight.DefaultCommunicationChannel,
                                    PremiseId = string.Empty,
                                    ServiceContractId = string.Empty,
                                    ServicePointId = string.Empty,
                                    IsSmsOptInCompleted = true,
                                    IsEmailOptInCompleted = true
                                };

                                if (!string.IsNullOrEmpty(accountDefaultInsight.DefaultValue))
                                    insightSubscription.SubscriberThresholdValue =
                                        accountDefaultInsight.GetCommodityTypevalue(insightSubscription.CommodityKey);
                                subscribeProgramInsights.Add(insightSubscription);
                            }
                        }
                        else
                        {
                            // if exists, check if the threshold and channal is same as the default setting
                            // if not, add to the list with the updated channel/threshold
                            var subscribeAccountInsight =
                                accountInsights.Find(
                                    i => string.Equals(i.InsightTypeName, accountDefaultInsight.InsightName, StringComparison.CurrentCultureIgnoreCase));

                            var savedAccountInsight =
                       existingSubscriptionwithDefault.Find(
                           i =>
                               string.IsNullOrEmpty(i.ServiceContractId) &&
                                        string.Equals(i.ProgramName, program.ProgramName,
                                            StringComparison.CurrentCultureIgnoreCase) &&
                                        string.Equals(i.InsightTypeName, accountDefaultInsight.InsightName,
                                            StringComparison.CurrentCultureIgnoreCase));

                            double? savedThreshold = savedAccountInsight.SubscriberThresholdValue;

                            if ((!string.IsNullOrEmpty(subscribeAccountInsight.Channel) &&
                                 !string.IsNullOrEmpty(savedAccountInsight.Channel) &&
                                 !string.Equals(subscribeAccountInsight.Channel, savedAccountInsight.Channel,
                                     StringComparison.CurrentCultureIgnoreCase)) ||
                                (!string.IsNullOrEmpty(subscribeAccountInsight.Channel) &&
                                 string.IsNullOrEmpty(savedAccountInsight.Channel)) ||
                                (string.IsNullOrEmpty(subscribeAccountInsight.Channel) &&
                                 !string.IsNullOrEmpty(savedAccountInsight.Channel)) ||
                                (subscribeAccountInsight.SubscriberThresholdValue.HasValue &&
                                 !savedThreshold.HasValue) ||
                                (subscribeAccountInsight.SubscriberThresholdValue.HasValue &&
                                 savedThreshold.HasValue &&
                                 Math.Abs(subscribeAccountInsight.SubscriberThresholdValue.Value - savedThreshold.Value) > 0))
                            {
                                var insightSubscription = new SubscriptionModel
                                {
                                    AccountId = accountId,
                                    ClientId = clientId,
                                    CustomerId = customerId,
                                    IsSelected = true,
                                    ProgramName = program.ProgramName,
                                    InsightTypeName = accountDefaultInsight.InsightName,
                                    PremiseId = string.Empty,
                                    ServiceContractId = string.Empty,
                                    ServicePointId = string.Empty,
                                    SubscriberThresholdValue = savedAccountInsight.SubscriberThresholdValue,
                                    Channel = savedAccountInsight.Channel,
                                    IsSmsOptInCompleted = true,
                                    IsEmailOptInCompleted = true
                                };

                                if ((!string.IsNullOrEmpty(subscribeAccountInsight.Channel) &&
                                     !string.IsNullOrEmpty(savedAccountInsight.Channel) &&
                                     !subscribeAccountInsight.Channel.Equals(savedAccountInsight.Channel, StringComparison.CurrentCultureIgnoreCase)) ||
                                    (!string.IsNullOrEmpty(subscribeAccountInsight.Channel) &&
                                     string.IsNullOrEmpty(savedAccountInsight.Channel)))
                                {
                                    insightSubscription.Channel = subscribeAccountInsight.Channel;
                                }
                                else if (string.IsNullOrEmpty(subscribeAccountInsight.Channel) &&
                                          !string.IsNullOrEmpty(savedAccountInsight.Channel))
                                {
                                    insightSubscription.Channel = savedAccountInsight.Channel;
                                }

                                if ((subscribeAccountInsight.SubscriberThresholdValue.HasValue &&
                                     Math.Abs(subscribeAccountInsight.SubscriberThresholdValue.Value) > 0 && 
                                     !savedThreshold.HasValue) ||
                                    (subscribeAccountInsight.SubscriberThresholdValue.HasValue &&
                                     savedThreshold.HasValue &&
                                     Math.Abs(subscribeAccountInsight.SubscriberThresholdValue.Value) > 0 && 
                                     Math.Abs(subscribeAccountInsight.SubscriberThresholdValue.Value - savedThreshold.Value) > 0))
                                {
                                    insightSubscription.SubscriberThresholdValue =
                                        subscribeAccountInsight.SubscriberThresholdValue;
                                }
                                subscribeProgramInsights.Add(insightSubscription);
                            }
                        }
                    }
                    
                    // serivce insights
                    List<SubscriptionModel> services = tobeSubscribe.FindAll(i => !string.IsNullOrEmpty(i.ServiceContractId) && string.Equals(i.ProgramName, program.ProgramName, StringComparison.CurrentCultureIgnoreCase)); 
                    List<SubscriptionModel> existingSubscritionwithDefaultServiceLevel =
                        existingUnSubsrcibedInsights.FindAll(
                            i =>
                                !string.IsNullOrEmpty(i.ServiceContractId) &&
                                string.Equals(i.ProgramName, program.ProgramName, StringComparison.CurrentCultureIgnoreCase));
                    List<SubscriptionModel> unscribeSerivcesInsights = existingSubscritionwithDefaultServiceLevel.FindAll(e => !services.Exists(s => s.ServiceContractId == e.ServiceContractId && s.InsightTypeName.Equals(e.InsightTypeName, StringComparison.CurrentCultureIgnoreCase)));
                    subscribeProgramInsights.AddRange(unscribeSerivcesInsights.Select(service => new SubscriptionModel
                    {
                        AccountId = accountId,
                        ClientId = clientId,
                        CustomerId = customerId,
                        IsSelected = false,
                        ProgramName = program.ProgramName,
                        InsightTypeName = service?.InsightTypeName,
                        Channel = service?.Channel,
                        PremiseId = service?.PremiseId,
                        ServiceContractId = service?.ServiceContractId,
                        ServicePointId = service?.ServicePointId,
                        SubscriberThresholdValue = service?.SubscriberThresholdValue,
                        IsSmsOptInCompleted = true,
                        IsEmailOptInCompleted = true,
                        UomId = service?.UomId
                    }));

                    // add subscribe serivce to the list if the channel/threshold is different from the default's
                    foreach (var service in services)
                    {
                        var serviceDefault = existingSubscriptionwithDefault.Find(
                            s =>
                                s.ServiceContractId == service.ServiceContractId &&
                                string.Equals(s.InsightTypeName, service.InsightTypeName, StringComparison.CurrentCultureIgnoreCase) &&
                                string.Equals(s.ProgramName, service.ProgramName, StringComparison.CurrentCultureIgnoreCase)
                                );
                        if (serviceDefault != null &&
                            ((!string.IsNullOrEmpty(service.Channel) && !string.IsNullOrEmpty(serviceDefault.Channel) &&
                              !service.Channel.Equals(serviceDefault.Channel, StringComparison.CurrentCultureIgnoreCase)) ||
                             (!string.IsNullOrEmpty(service.Channel) && string.IsNullOrEmpty(serviceDefault.Channel)) ||
                             (string.IsNullOrEmpty(service.Channel) && !string.IsNullOrEmpty(serviceDefault.Channel)) ||
                             (service.SubscriberThresholdValue.HasValue &&
                              !serviceDefault.SubscriberThresholdValue.HasValue) ||
                             (service.SubscriberThresholdValue.HasValue &&
                              serviceDefault.SubscriberThresholdValue.HasValue &&
                              Math.Abs(service.SubscriberThresholdValue.Value - serviceDefault.SubscriberThresholdValue.Value) > 0)))
                        {
                            var insightSubscription = new SubscriptionModel
                            {
                                AccountId = accountId,
                                ClientId = clientId,
                                CustomerId = customerId,
                                IsSelected = true,
                                ProgramName = program.ProgramName,
                                InsightTypeName = serviceDefault.InsightTypeName,
                                PremiseId = serviceDefault.PremiseId,
                                ServiceContractId = serviceDefault.ServiceContractId,
                                ServicePointId = serviceDefault.ServicePointId,
                                SubscriberThresholdValue = serviceDefault.SubscriberThresholdValue,
                                Channel = serviceDefault.Channel,
                                IsSmsOptInCompleted = true,
                                IsEmailOptInCompleted = true,
                                UomId = serviceDefault.UomId
                            };

                            if ((!string.IsNullOrEmpty(service.Channel) && !string.IsNullOrEmpty(serviceDefault.Channel) &&
                                 !service.Channel.Equals(serviceDefault.Channel, StringComparison.CurrentCultureIgnoreCase)) ||
                                (!string.IsNullOrEmpty(service.Channel) && string.IsNullOrEmpty(serviceDefault.Channel)))
                            {
                                insightSubscription.Channel = service.Channel;
                            }
                            else if (string.IsNullOrEmpty(service.Channel) &&
                                     !string.IsNullOrEmpty(serviceDefault.Channel))
                            {
                                insightSubscription.Channel = serviceDefault.Channel;
                            }

                            if ((service.SubscriberThresholdValue.HasValue && Math.Abs(service.SubscriberThresholdValue.Value) > 0 &&
                                 !serviceDefault.SubscriberThresholdValue.HasValue) ||
                                (service.SubscriberThresholdValue.HasValue && Math.Abs(service.SubscriberThresholdValue.Value) > 0 &&
                                 serviceDefault.SubscriberThresholdValue.HasValue &&
                                 Math.Abs(service.SubscriberThresholdValue.Value - serviceDefault.SubscriberThresholdValue.Value) > 0))
                            {
                                insightSubscription.SubscriberThresholdValue =
                                    service.SubscriberThresholdValue;
                            }
                            subscribeProgramInsights.Add(insightSubscription);
                        }
                    }
                }

                // add the new subscribed one
                existingUnSubsrcibedInsights.FindAll(
                    p => subscribeProgramInsights.Exists(i => i.ProgramName.ToLower(CultureInfo.InvariantCulture) == p.ProgramName.ToLower(CultureInfo.InvariantCulture)))
                    .ForEach(i =>
                    {
                        if (!subscribeProgramInsights.Exists(
                            s =>
                                s.AccountId == i.AccountId &&
                                (s.ServiceContractId == i.ServiceContractId ||
                                 (string.IsNullOrEmpty(i.ServiceContractId) &&
                                  string.IsNullOrEmpty(s.ServiceContractId))) &&
                                s.ProgramName.Equals(i.ProgramName, StringComparison.CurrentCultureIgnoreCase) &&
                                s.InsightTypeName.Equals(i.InsightTypeName, StringComparison.CurrentCultureIgnoreCase)) &&
                            subscriptionModelList.ToList()
                                .Exists(
                                    s =>
                                        s.AccountId == i.AccountId &&
                                        (s.ServiceContractId == i.ServiceContractId ||
                                         (string.IsNullOrEmpty(i.ServiceContractId) &&
                                          string.IsNullOrEmpty(s.ServiceContractId))) &&
                                        s.ProgramName.Equals(i.ProgramName, StringComparison.CurrentCultureIgnoreCase) &&
                                        ((!string.IsNullOrEmpty(s.InsightTypeName) &&
                                          s.InsightTypeName.Equals(i.InsightTypeName, StringComparison.CurrentCultureIgnoreCase)) ||
                                         (string.IsNullOrEmpty(s.InsightTypeName) && i.InsightTypeName == string.Empty))))
                        {
                            i.IsSelected = true;
                            if (string.IsNullOrEmpty(i.ServiceContractId))
                            {
                                i.ServiceContractId = string.Empty;
                                i.ServicePointId = string.Empty;
                                i.PremiseId = string.Empty;
                            }

                            // check if the subscribe threshold/channel is same as the default's; if not, update them
                            var subscribe =
                                tobeSubscribe.Find(
                                    s =>
                                        i.ProgramName.Equals(s.ProgramName, StringComparison.CurrentCultureIgnoreCase) &&
                                        s.AccountId == i.AccountId &&
                                        (s.ServiceContractId == i.ServiceContractId ||
                                         (string.IsNullOrEmpty(i.ServiceContractId) &&
                                          string.IsNullOrEmpty(s.ServiceContractId))) &&
                                        s.ProgramName.Equals(i.ProgramName, StringComparison.CurrentCultureIgnoreCase) &&
                                        s.InsightTypeName.Equals(i.InsightTypeName,
                                            StringComparison.CurrentCultureIgnoreCase));
                            if (!string.IsNullOrEmpty(subscribe.Channel) &&
                                subscribe.Channel.Equals(i.Channel, StringComparison.CurrentCultureIgnoreCase))
                            {
                                i.Channel = subscribe.Channel;
                            }

                            if ((subscribe.SubscriberThresholdValue.HasValue &&
                                 Math.Abs(subscribe.SubscriberThresholdValue.Value) > 0 &&
                                 !i.SubscriberThresholdValue.HasValue) ||
                                (subscribe.SubscriberThresholdValue.HasValue &&
                                 Math.Abs(subscribe.SubscriberThresholdValue.Value) > 0 &&
                                 i.SubscriberThresholdValue.HasValue &&
                                 Math.Abs(subscribe.SubscriberThresholdValue.Value - i.SubscriberThresholdValue.Value) > 0))
                            {
                                i.SubscriberThresholdValue = subscribe.SubscriberThresholdValue;
                            }
                            subscribeProgramInsights.Add(i);
                        }
                    });
            }

            // unsubscrib
            if (tobeUnsubscribe.Any())
            {
                var hasSms = false;
                var hasEmail = false;

                // service level
                foreach (var subscriptionModel in existingSubscriptionwithDefault.FindAll(e => e.IsSelected && !string.IsNullOrEmpty(e.ServiceContractId)).
                    FindAll(u => tobeUnsubscribe.Exists(i => string.Equals(i.ProgramName, u.ProgramName, StringComparison.CurrentCultureIgnoreCase) && string.Equals(i.InsightTypeName, u.InsightTypeName, StringComparison.CurrentCultureIgnoreCase) && i.ServiceContractId == u.ServiceContractId)))
                {
                    var defaultInsight =
                        programs.Find(p => string.Equals(p.ProgramName, subscriptionModel.ProgramName, StringComparison.CurrentCultureIgnoreCase))
                            .Insights.Find(
                                i => string.Equals(i.InsightName, subscriptionModel.InsightTypeName, StringComparison.CurrentCultureIgnoreCase));
                    if (!subscribeProgramInsights.Exists(s => s.AccountId == subscriptionModel.AccountId && (s.ServiceContractId == subscriptionModel.ServiceContractId) && string.Equals(s.ProgramName, subscriptionModel.ProgramName, StringComparison.CurrentCultureIgnoreCase) && string.Equals(s.InsightTypeName, subscriptionModel.InsightTypeName, StringComparison.CurrentCultureIgnoreCase)))
                    {
                        subscriptionModel.IsSelected = false;
                        if (!string.IsNullOrEmpty(subscriptionModel.Channel))
                        {
                            if (subscriptionModel.Channel.Equals(Utilities.Enums.SubscriptionChannel.SMS.ToString(),
                                StringComparison.InvariantCultureIgnoreCase))
                            {
                                hasSms = true;
                            }
                            else if (subscriptionModel.Channel.Equals(Utilities.Enums.SubscriptionChannel.Email.ToString(),
                               StringComparison.InvariantCultureIgnoreCase))
                            {
                                hasEmail = true;
                            }
                            else if (subscriptionModel.Channel.Equals(Utilities.Enums.SubscriptionChannel.EmailAndSMS.ToString(),
                               StringComparison.InvariantCultureIgnoreCase))
                            {
                                hasEmail = true;
                                hasSms = true;
                            }
                        }

                        subscriptionModel.Channel = defaultInsight.DefaultCommunicationChannel;
                        subscriptionModel.SubscriberThresholdValue = !string.IsNullOrEmpty(defaultInsight.DefaultValue)
                            ? defaultInsight.GetCommodityTypevalue(subscriptionModel.CommodityKey)
                            : null;
                        if (string.IsNullOrEmpty(subscriptionModel.ServiceContractId))
                        {
                            subscriptionModel.ServiceContractId = string.Empty;
                            subscriptionModel.ServicePointId = string.Empty;
                            subscriptionModel.PremiseId = string.Empty;
                        }
                        subscribeProgramInsights.Add(subscriptionModel);
                    }
                    else
                    {
                        {
                            var unsubscribe =
                                subscribeProgramInsights.Find(
                                    s =>
                                        s.AccountId == subscriptionModel.AccountId &&
                                        (s.ServiceContractId == subscriptionModel.ServiceContractId ||
                                         (string.IsNullOrEmpty(subscriptionModel.ServiceContractId) &&
                                          string.IsNullOrEmpty(s.ServiceContractId))) &&
                                        string.Equals(s.ProgramName, subscriptionModel.ProgramName, StringComparison.CurrentCultureIgnoreCase) &&
                                        string.Equals(s.InsightTypeName, subscriptionModel.InsightTypeName, StringComparison.CurrentCultureIgnoreCase));
                            unsubscribe.IsSelected = false;
                            if (!string.IsNullOrEmpty(subscriptionModel.Channel))
                            {
                                if (subscriptionModel.Channel.Equals(Utilities.Enums.SubscriptionChannel.SMS.ToString(),
                                    StringComparison.InvariantCultureIgnoreCase))
                                {
                                    hasSms = true;
                                }
                                else if (subscriptionModel.Channel.Equals(Utilities.Enums.SubscriptionChannel.Email.ToString(),
                                   StringComparison.InvariantCultureIgnoreCase))
                                {
                                    hasEmail = true;
                                }
                                else if (subscriptionModel.Channel.Equals(Utilities.Enums.SubscriptionChannel.EmailAndSMS.ToString(),
                                   StringComparison.InvariantCultureIgnoreCase))
                                {
                                    hasEmail = true;
                                    hasSms = true;
                                }
                            }
                            unsubscribe.Channel = defaultInsight.DefaultCommunicationChannel;
                            unsubscribe.SubscriberThresholdValue = !string.IsNullOrEmpty(defaultInsight.DefaultValue)
                                ? defaultInsight.GetCommodityTypevalue(unsubscribe.CommodityKey)
                                : null;
                        }
                    }
                }

                // account level
                foreach (var subscriptionModel in existingSubscriptionwithDefault.FindAll(e => e.IsSelected && string.IsNullOrEmpty(e.ServiceContractId)).
                    FindAll(u => tobeUnsubscribe.Exists(i => string.Equals(i.ProgramName, u.ProgramName, StringComparison.CurrentCultureIgnoreCase) && string.Equals(i.InsightTypeName, u.InsightTypeName, StringComparison.CurrentCultureIgnoreCase))))
                {
                    var defaultInsight =
                        programs.Find(p => string.Equals(p.ProgramName, subscriptionModel.ProgramName, StringComparison.CurrentCultureIgnoreCase))
                            .Insights.Find(
                                i => string.Equals(i.InsightName, subscriptionModel.InsightTypeName, StringComparison.CurrentCultureIgnoreCase));
                    if (!subscribeProgramInsights.Exists(s => s.AccountId == subscriptionModel.AccountId && (s.ServiceContractId == subscriptionModel.ServiceContractId) && string.Equals(s.ProgramName, subscriptionModel.ProgramName, StringComparison.CurrentCultureIgnoreCase)
                    && string.Equals(s.InsightTypeName, subscriptionModel.InsightTypeName, StringComparison.CurrentCultureIgnoreCase)))
                    {
                        subscriptionModel.IsSelected = false;
                        if (!string.IsNullOrEmpty(subscriptionModel.Channel))
                        {
                            if (subscriptionModel.Channel.Equals(Utilities.Enums.SubscriptionChannel.SMS.ToString(),
                                StringComparison.InvariantCultureIgnoreCase))
                            {
                                hasSms = true;
                            }
                            else if (subscriptionModel.Channel.Equals(Utilities.Enums.SubscriptionChannel.Email.ToString(),
                               StringComparison.InvariantCultureIgnoreCase))
                            {
                                hasEmail = true;
                            }
                            else if (subscriptionModel.Channel.Equals(Utilities.Enums.SubscriptionChannel.EmailAndSMS.ToString(),
                               StringComparison.InvariantCultureIgnoreCase))
                            {
                                hasEmail = true;
                                hasSms = true;
                            }
                        }
                        subscriptionModel.Channel = defaultInsight.DefaultCommunicationChannel;
                        subscriptionModel.SubscriberThresholdValue = !string.IsNullOrEmpty(defaultInsight.DefaultValue)
                            ? defaultInsight.GetCommodityTypevalue(subscriptionModel.CommodityKey)
                            : null;
                        if (string.IsNullOrEmpty(subscriptionModel.ServiceContractId))
                        {
                            subscriptionModel.ServiceContractId = string.Empty;
                            subscriptionModel.ServicePointId = string.Empty;
                            subscriptionModel.PremiseId = string.Empty;
                        }
                        subscribeProgramInsights.Add(subscriptionModel);
                    }
                    else
                    {
                        {
                            var unsubscribe =
                                subscribeProgramInsights.Find(
                                    s =>
                                        s.AccountId == subscriptionModel.AccountId &&
                                        (s.ServiceContractId == subscriptionModel.ServiceContractId ||
                                         (string.IsNullOrEmpty(subscriptionModel.ServiceContractId) &&
                                          string.IsNullOrEmpty(s.ServiceContractId))) &&
                                        string.Equals(s.ProgramName, subscriptionModel.ProgramName, StringComparison.CurrentCultureIgnoreCase) &&
                                        string.Equals(s.InsightTypeName, subscriptionModel.InsightTypeName, StringComparison.CurrentCultureIgnoreCase));
                            unsubscribe.IsSelected = false;
                            if (!string.IsNullOrEmpty(subscriptionModel.Channel))
                            {
                                if (subscriptionModel.Channel.Equals(Utilities.Enums.SubscriptionChannel.SMS.ToString(),
                                    StringComparison.InvariantCultureIgnoreCase))
                                {
                                    hasSms = true;
                                }
                                else if (subscriptionModel.Channel.Equals(Utilities.Enums.SubscriptionChannel.Email.ToString(),
                                   StringComparison.InvariantCultureIgnoreCase))
                                {
                                    hasEmail = true;
                                }
                                else if (subscriptionModel.Channel.Equals(Utilities.Enums.SubscriptionChannel.EmailAndSMS.ToString(),
                                   StringComparison.InvariantCultureIgnoreCase))
                                {
                                    hasEmail = true;
                                    hasSms = true;
                                }
                            }
                            unsubscribe.Channel = defaultInsight.DefaultCommunicationChannel;
                            unsubscribe.SubscriberThresholdValue = !string.IsNullOrEmpty(defaultInsight.DefaultValue)
                                ? defaultInsight.GetCommodityTypevalue(unsubscribe.CommodityKey)
                                : null;
                        }
                    }
                }

                foreach (
                    var program in
                        programs.FindAll(
                            p => tobeUnsubscribe.Exists(i => string.Equals(i.ProgramName, p.ProgramName, StringComparison.CurrentCultureIgnoreCase))))
                {
                    List<SubscriptionModel> programInsights = existingSubscriptionwithDefault.FindAll(e => string.Equals(e.ProgramName, program.ProgramName, StringComparison.CurrentCultureIgnoreCase));

                    List<SubscriptionModel> unscribeProgramInsights =
                        subscribeProgramInsights.FindAll(s => !s.IsSelected && string.Equals(s.ProgramName, program.ProgramName, StringComparison.CurrentCultureIgnoreCase));

                    List<SubscriptionModel> defaultUnscribeProgramInsights = programInsights.FindAll(e => !e.IsSelected && !unscribeProgramInsights
                        .Exists(s => string.Equals(s.ProgramName, e.ProgramName, StringComparison.CurrentCultureIgnoreCase) && s.AccountId == e.AccountId && string.Equals(s.InsightTypeName, e.InsightTypeName, StringComparison.CurrentCultureIgnoreCase)
                        && (s.ServiceContractId == e.ServiceContractId ||
                                         (string.IsNullOrEmpty(e.ServiceContractId) &&
                                          string.IsNullOrEmpty(s.ServiceContractId))
                        ))
                        && !tobeSubscribe.Exists(s => string.Equals(s.ProgramName, e.ProgramName, StringComparison.CurrentCultureIgnoreCase) && s.AccountId == e.AccountId && string.Equals(s.InsightTypeName, e.InsightTypeName, StringComparison.CurrentCultureIgnoreCase)
                        && (s.ServiceContractId == e.ServiceContractId ||
                                         (string.IsNullOrEmpty(e.ServiceContractId) &&
                                          string.IsNullOrEmpty(s.ServiceContractId))
                        ))
                        );

                    if (programInsights.Count == unscribeProgramInsights.Count + defaultUnscribeProgramInsights.Count)
                    {
                        var unsubscribeProgram = subscribeProgramInsights.Find(
                            s => s.ProgramName == program.ProgramName && string.IsNullOrEmpty(s.InsightTypeName));
                        if (unsubscribeProgram != null)
                            unsubscribeProgram.IsSelected = false;
                        else
                        {
                            var optInEmailCompleted = false;
                            var optInSmsCompleted = false;
                            // get the existing optin status
                            if (
                                subscriptionModelList.Exists(
                                    s =>
                                        s.AccountId == accountId && customerId == s.CustomerId &&
                                        string.Equals(s.ProgramName, program.ProgramName, StringComparison.CurrentCultureIgnoreCase)))
                            {
                                var savedProgram = subscriptionModelList.Find(
                                    s =>
                                        s.AccountId == accountId && customerId == s.CustomerId &&
                                        string.Equals(s.ProgramName, program.ProgramName, StringComparison.CurrentCultureIgnoreCase));
                                optInEmailCompleted = savedProgram.IsEmailOptInCompleted ?? false;
                                optInSmsCompleted = savedProgram.IsSmsOptInCompleted ?? false;

                            }
                            // check if the program is using sms, email or both to update the optin status for unsubscribe program
                            if (
                                existingSubscriptionwithDefault.Exists(
                                    e =>
                                        e.ProgramName.Equals(program.ProgramName, StringComparison.InvariantCultureIgnoreCase) && e.Channel.Equals(Utilities.Enums.SubscriptionChannel.SMS.ToString(),
                                            StringComparison.InvariantCultureIgnoreCase)))
                            {
                                hasSms = true;
                            }
                            if (
                                existingSubscriptionwithDefault.Exists(
                                    e =>
                                        e.ProgramName.Equals(program.ProgramName, StringComparison.InvariantCultureIgnoreCase) && e.Channel.Equals(Utilities.Enums.SubscriptionChannel.Email.ToString(),
                                            StringComparison.InvariantCultureIgnoreCase)))
                            {
                                hasEmail = true;
                            }
                            if (
                                existingSubscriptionwithDefault.Exists(
                                    e =>
                                        e.ProgramName.Equals(program.ProgramName, StringComparison.InvariantCultureIgnoreCase) && e.Channel.Equals(Utilities.Enums.SubscriptionChannel.EmailAndSMS.ToString(),
                                            StringComparison.InvariantCultureIgnoreCase)))
                            {
                                hasEmail = true;
                                hasSms = true;
                            }

                            if (!program.DoubleOptInRequired)
                            {
                                optInEmailCompleted = true;
                                optInSmsCompleted = true;
                            }
                            else
                            {
                                if (hasEmail && hasSms)
                                {
                                    optInEmailCompleted = false;
                                    optInSmsCompleted = false;
                                }
                                else if (hasEmail)
                                    optInEmailCompleted = false;
                                else if (hasSms)
                                    optInSmsCompleted = false;
                            }
                            unsubscribeProgram = new SubscriptionModel
                            {
                                AccountId = accountId,
                                ClientId = clientId,
                                //CommodityKey = program.CommodityType.ToLower(),
                                CustomerId = customerId,
                                IsSelected = false,
                                ProgramName = program.ProgramName,
                                PremiseId = string.Empty,
                                ServiceContractId = string.Empty,
                                ServicePointId = string.Empty,
                                InsightTypeName = string.Empty,
                                IsSmsOptInCompleted = optInSmsCompleted,
                                IsEmailOptInCompleted = optInEmailCompleted
                            };
                            subscribeProgramInsights.Add(unsubscribeProgram);
                        }
                    }
                }
            }
            return subscribeProgramInsights;
        }

        /// <summary>
        /// Opt in for email and sms
        /// </summary>
        /// <param name="subscription">Opt in for the subscription</param>
        public TrumpiaRequestDetailModel DoubleOptin(SubscriptionModel subscription)
        {
            TrumpiaRequestDetailModel smsRequestDetail = null;
            var addEmailConfirmationCount = false;
            var addSmsConfirmationCount = false;
            var clientSettings = _clientConfigFacadeManager.Value.GetClientSettings(subscription.ClientId);
            List<ProgramSettings> programs = clientSettings.Programs;

            var program = programs.Find(p => string.Equals(p.ProgramName, subscription.ProgramName, StringComparison.CurrentCultureIgnoreCase));

            if (program.DoubleOptInRequired)
            {
                Task<CustomerModel> customerTask = GetCustomerAsync(subscription.ClientId, subscription.CustomerId);
                Task.WaitAll(customerTask);
                if (customerTask.Status == TaskStatus.RanToCompletion)
                {
                    var customerModel = customerTask.Result;

                    var subscriptionEntity = _subscriptionManager.Value.GetSubscriptionDetails(subscription.ClientId, subscription.CustomerId,
                        subscription.AccountId, subscription.ProgramName, subscription.ServiceContractId,
                        subscription.InsightTypeName) ?? subscription;

                    if (subscription.IsEmailOptInCompleted != null && !subscription.IsEmailOptInCompleted.Value)
                    {
                        // send optin email
                        addEmailConfirmationCount = SendEmail(clientSettings, customerModel, subscriptionEntity);
                        if (addEmailConfirmationCount)
                            subscriptionEntity.IsEmailOptInCompleted = true;
                    }

                    if (subscription.IsSmsOptInCompleted != null && !subscription.IsSmsOptInCompleted.Value)
                    {
                        // send optin sms 
                        addSmsConfirmationCount = SendSms(clientSettings, customerModel, subscriptionEntity);
                        if (addSmsConfirmationCount)
                            subscriptionEntity.IsSmsOptInCompleted = true;
                        else
                        {
                            // TFS 8022 - get status code if sms is not sent
                            if(!string.IsNullOrEmpty(customerModel.TrumpiaRequestDetailId))
                            {
                                var trumpiaRequestDetailModel = _trumpiaRequestDetailManager.Value.GetTrumpiaRequestDetail(
                                    customerModel.TrumpiaRequestDetailId);
                                if (trumpiaRequestDetailModel != null)
                                {
                                    smsRequestDetail = trumpiaRequestDetailModel;
                                }
                            }
                        }
                    }

                    if (addEmailConfirmationCount || addSmsConfirmationCount)
                    {
                        Task<ConfiguredTaskAwaitable<bool>> subscriptionTask = Task.Run(() => _subscriptionManager.Value.InsertSubscriptionAsync(
                            subscriptionEntity,
                            addEmailConfirmationCount, addSmsConfirmationCount).ConfigureAwait(false));
                        Task.WhenAll(subscriptionTask);
                    }
                }
            }
            return smsRequestDetail;
        }

        /// <summary>
        /// check if the subscription in trumpia is blocked or deleted
        /// if so, update trumpia request detail with the info
        /// </summary>
        /// <returns></returns>
        public TrumpiaRequestDetailModel TrumpiaSubscriptionCheck(string requestId, string subscriptionId, string apiKey, string username)
        {
            TrumpiaRequestDetailModel smsRequestDetail = null;
            var sendSms = new SendSms(_logModel);
            var smsSubscriptionInfo = sendSms.GetSubscriptionInfo(subscriptionId, apiKey, username);
            if (!string.IsNullOrEmpty(smsSubscriptionInfo.status_code))
            {
                smsRequestDetail = new TrumpiaRequestDetailModel
                {
                    StatusCode = smsSubscriptionInfo.status_code,
                    RequestId = requestId,
                    ErrorMessage = smsSubscriptionInfo.error_message
                };

                if (smsRequestDetail.StatusCode == TrumpiaInvalidSubscriptionCode)
                {
                    smsRequestDetail.ErrorMessage = string.Format(TrumpiaInvalidSubscriptionMessage, subscriptionId);
                }

                _trumpiaRequestDetailManager.Value.InsertOrMergeTrumpiaRequestDetailAsync(smsRequestDetail);
            }

            return smsRequestDetail;
        }

        /// <summary>
        /// Get Subscription List for subscribed programs
        /// </summary>
        /// <param name="clientId">The current client Id</param>
        /// <param name="customerId">The id of the customer</param>
        /// <param name="accountId">The account id of the customer</param>
        /// <param name="programs">The  list of subscription programs</param>
        /// <param name="subscriptionModelList">The  list of subscription</param>
        /// <returns>List of SubscriptionModel</returns>
        private List<SubscriptionModel> GetDefaultListWithSubscribedPrograms(int clientId, string customerId, string accountId, List<ProgramSettings> programs, List<SubscriptionModel> subscriptionModelList)
        {
            // get service info
            IEnumerable<BillingModel> services = _billingManager.Value.GetAllServicesFromLastBill(clientId, customerId, accountId);

            if (services == null)
            {
                return null;
            }
            IList<BillingModel> serviceList = services as IList<BillingModel> ?? services.ToList();
            if (!serviceList.Any())
            {
                return null;
            }

            // map the program to subscription model
            List<SubscriptionModel> defaultSusbscriptionList = MapProgramsToSubscriptionModel(programs, clientId, customerId, accountId, serviceList.ToList());
            // mrege customer subscription with default subscripton settings
            List<SubscriptionModel> mergedSubscriptionList = _subscriptionManager.Value.MergeDefaultSubscriptionListWithSubscribedPrograms(subscriptionModelList, defaultSusbscriptionList);
            return mergedSubscriptionList;
        }

        /// <summary>
        /// Send email for opt in confirmation
        /// </summary>
        /// <param name="clientSetting">To provide client setting details like OptInEmailUserName, OptInEmailPassword, OptInEmailConfirmationTemplateId</param>
        /// <param name="customerModel">To provide customer details like EmailAddress, FirstName</param>
        /// <param name="subscriptionModel">To provide subscription details like AccountId, ProgramName... etc</param>
        /// <returns>bool</returns>
        private bool SendEmail(ClientSettings clientSetting, CustomerModel customerModel, SubscriptionModel subscriptionModel)
        {
            var emailSent = false;
            if (customerModel != null)
            {
                // email settings
                var clientEmailUsername = clientSetting.OptInEmailUserName;
                var clientEmailPassword = clientSetting.OptInEmailPassword;
                var sender = clientSetting.OptInEmailFrom;
                var accountNumber = subscriptionModel.AccountId;
                if (clientSetting.UnmaskedAccountIdEndingDigit > 0)
                {
                    accountNumber = AccountLookupModel.GetMaskedAccountId(accountNumber, clientSetting.UnmaskedAccountIdEndingDigit);
                }
                if (string.IsNullOrEmpty(customerModel.EmailAddress))
                {
                    Logger.Fatal(@" Email address not specified in Customer information.", _logModel);
                    return false;
                }

                var address = string.Empty;

                if (!string.IsNullOrEmpty(customerModel.AddressLine1))
                    address = address + customerModel.AddressLine1;

                if (!string.IsNullOrEmpty(customerModel.AddressLine2))
                {
                    address = !string.IsNullOrEmpty(address)
                        ? address + ", " + customerModel.AddressLine2
                        : address + customerModel.AddressLine2;
                }

                if (!string.IsNullOrEmpty(customerModel.AddressLine3))
                {
                    address = !string.IsNullOrEmpty(address)
                        ? address + ", " + customerModel.AddressLine3
                        : address + customerModel.AddressLine3;
                }

                var emailAddress = new List<string> { customerModel.EmailAddress };
                string description = $@"{subscriptionModel.ClientId}|{subscriptionModel.CustomerId}|{subscriptionModel.AccountId}|{subscriptionModel.ProgramName}";
                byte[] encodedDescription = Encoding.UTF8.GetBytes(description);
                var identifiers = new Dictionary<string, List<string>>
                {
                    {"-FirstName-", new List<string> {customerModel.FirstName}},
                    {"-LastName-", new List<string> {customerModel.LastName}},
                    {"-ConfirmationLink-" , new List<string> { $"description={Convert.ToBase64String(encodedDescription)}" } },
                    {"-ProgramName-" , new List<string> { subscriptionModel.ProgramName } },
                    {"-AccountNumber-", new List<string> {accountNumber}},
                    { "-AccountAddress-",new List<string>{address}}
                };

                var templateId = clientSetting.OptInEmailConfirmationTemplateId;

                var notificationPrimaryKey = subscriptionModel.ClientId + CustomKeySeperator +
                                             subscriptionModel.CustomerId + CustomKeySeperator +
                                             subscriptionModel.AccountId + CustomKeySeperator +
                                             subscriptionModel.ProgramName + CustomKeySeperator + null +
                                             CustomKeySeperator + null + CustomKeySeperator + subscriptionModel.Channel +
                                             CustomKeySeperator + false; 
                var subscriptionPrimaryKey = subscriptionModel.ClientId + CustomKeySeperator +
                                             subscriptionModel.CustomerId + CustomKeySeperator +
                                             subscriptionModel.AccountId + CustomKeySeperator +
                                             subscriptionModel.ProgramName +
                                             CustomKeySeperator + subscriptionModel.InsightTypeName + CustomKeySeperator +
                                             subscriptionModel.ServiceContractId + CustomKeySeperator +
                                             subscriptionModel.SubscriptionDate + CustomKeySeperator +
                                             subscriptionModel.IsLatest;

                var customKey = string.Format(notificationPrimaryKey + "|" + subscriptionPrimaryKey);
                
                Task<ConfiguredTaskAwaitable> emailTask =
                    Task.Run(
                        () =>
                            _sendEmailManager.Value.SendEmailAsync(sender, emailAddress, identifiers, templateId, clientEmailUsername,
                                clientEmailPassword, customKey, null).ConfigureAwait(false));
                Task.WaitAll(emailTask);
                emailSent = true;
            }
            return emailSent;
        }

        /// <summary>
        /// Send sms for opt in confirmation
        /// </summary>
        /// <param name="clientSetting">To provide client setting details like TrumpiaApiKey, TrumpiaUserName, TrumpiaContactList</param>
        /// <param name="customerModel">To provide customer details like TrumpiaSubscriptionId, Phone1</param>
        /// <param name="subscriptionModel">To provide subscription details like AccountId, ProgramName... etc</param>
        /// <returns>bool</returns>
        private bool SendSms(ClientSettings clientSetting, CustomerModel customerModel, SubscriptionModel subscriptionModel)
        {
            var smsSent = false;
            if (customerModel != null)
            {
                // sms settings
                var smsApiKey = clientSetting.TrumpiaApiKey;
                var smsUserName = clientSetting.TrumpiaUserName;
                var smsContactList = clientSetting.TrumpiaContactList;

                if (string.IsNullOrEmpty(customerModel.TrumpiaSubscriptionId))
                {
                    if (string.IsNullOrEmpty(customerModel.Phone1))
                    {
                        Logger.Fatal(@"Phone number not specified in Customer information.",
                            _logModel);
                    }
                    else
                    {
                        _customerManager.Value.CreateSubscriptionAndUpdateCustomer(customerModel, smsApiKey, smsUserName, smsContactList);
                    }
                }

                if (!string.IsNullOrEmpty(customerModel.TrumpiaSubscriptionId))
                {
                    Task<SmsTemplateModel> notificationTask = GetSmsTemplateAsync(clientSetting, subscriptionModel);
                    Task.WaitAll(notificationTask);

                    if (notificationTask.Status == TaskStatus.RanToCompletion)
                    {
                        var smsTemplateModel = notificationTask.Result;
                        if (smsTemplateModel != null)
                        {
                            var substitutes = Regex.Matches(smsTemplateModel.Body, "[-][a-zA-Z]+[-]");
                            smsTemplateModel.Body = _notificationManager.Value.GetReplacedSmsTemplateForSubscriptionDoubleOptInConfirmation(smsTemplateModel.Body,
                                subscriptionModel, clientSetting, customerModel, substitutes);
                            var subscriptionPrimaryKey = subscriptionModel.ClientId + CustomKeySeperator +
                                                         subscriptionModel.CustomerId + CustomKeySeperator +
                                                         subscriptionModel.AccountId + CustomKeySeperator +
                                                         subscriptionModel.Type + CustomKeySeperator +
                                                         subscriptionModel.ProgramName + CustomKeySeperator +
                                                         subscriptionModel.InsightTypeName + CustomKeySeperator +
                                                         subscriptionModel.ServiceContractId + CustomKeySeperator +
                                                         subscriptionModel.SubscriptionDate + CustomKeySeperator +
                                                         subscriptionModel.IsLatest;
                            var sms = new SendSms(_logModel);
                            var phone = customerModel.Phone1 ?? customerModel.Phone2;
                            Task<ConfiguredTaskAwaitable<string>> smsTask =
                                Task.Run(
                                    () =>
                                        sms.Send(smsApiKey, smsUserName, smsTemplateModel.Body, phone, customerModel.TrumpiaSubscriptionId,
                                            $"s|{subscriptionPrimaryKey}",
                                            customerModel.ClientId, subscriptionModel.AccountId,
                                            subscriptionModel.ProgramName, subscriptionModel.ServiceContractId,
                                            subscriptionModel.InsightTypeName, DateTime.UtcNow)
                                            .ConfigureAwait(false));
                            Task.WhenAll(smsTask);
                            smsSent = true;
                        }
                    }
                }
            }
            return smsSent;
        }

        /// <summary>
        /// Get the sms template from database
        /// </summary>
        /// <param name="clientSetting">Provides OptInSmsConfirmationTemplateId of the data to be fetched</param>
        /// <param name="subscriptionModel">Provides ClientId of the data to be fetched</param>
        /// <returns>SmsTemplateModel</returns>
        private async Task<SmsTemplateModel> GetSmsTemplateAsync(ClientSettings clientSetting, SubscriptionModel subscriptionModel)
        {
            Task<SmsTemplateModel> task = await Task.Factory.StartNew(() => _notificationManager.Value.GetSmsTemplateFromDB(subscriptionModel.ClientId,
                        "OptInConfirmation",
                        clientSetting.OptInSmsConfirmationTemplateId)).ConfigureAwait(false);
            return task.Result;
        }

        /// <summary>
        /// Return customer info async from business layer
        /// </summary>
        /// <param name="clientId">The current client Id</param>
        /// <param name="customerId">The id of the customer</param>
        /// <returns>CustomerModel</returns>
        private async Task<CustomerModel> GetCustomerAsync(int clientId, string customerId)
        {
            Task<CustomerModel> task = await Task.Factory.StartNew(() => _customerManager.Value.GetCustomerAsync(clientId, customerId)).ConfigureAwait(false);
            return task.Result;
        }

        /// <summary>
        /// Map account and service level subscription from program list
        /// </summary>
        /// <param name="programs">The  list of subscription programs</param>
        /// <param name="clientId">The current client Id</param>
        /// <param name="customerId">The id of the customer</param>
        /// <param name="accountId">The account id of the customer</param>
        /// <param name="serviceList">Map servicelist to subscription model</param>
        /// <returns></returns>
        private List<SubscriptionModel> MapProgramsToSubscriptionModel(List<ProgramSettings> programs, int clientId, string customerId, string accountId, List<BillingModel> serviceList)
        {
            var subscriptions = new List<SubscriptionModel>();
            foreach (var program in programs)
            {
                foreach (var programInsight in program.Insights)
                {
                    if (programInsight.Level == "Account")
                    {
                        var subscription = new SubscriptionModel
                        {
                            IsSelected = false,
                            Channel = programInsight.DefaultCommunicationChannel,
                            ProgramName = program.ProgramName,
                            InsightTypeName = programInsight.InsightName,
                            CustomerId = customerId,
                            AccountId = accountId,
                            PremiseId = string.Empty,
                            ServiceContractId = string.Empty,
                            ServicePointId = string.Empty,
                            ClientId = clientId,
                            AllowedChannels = programInsight.AllowedCommunicationChannel,
                            IsEmailOptInCompleted = true,
                            IsSmsOptInCompleted = true
                        };

                        if (!string.IsNullOrEmpty(programInsight.DefaultValue))
                        {
                            subscription.SubscriberThresholdValue = programInsight.GetCommodityTypevalue(subscription.CommodityKey);
                            subscription.ThresholdMin = Convert.ToDouble(programInsight.ThresholdMin);
                            subscription.ThresholdMax = Convert.ToDouble(programInsight.ThresholdMax);
                            subscription.ThresholdType = programInsight.ThresholdType;
                        }
                        subscriptions.Add(subscription);
                    }
                    else
                    {
                        foreach (var service in serviceList)
                        {
                            List<string> commodities = programInsight.CommodityType.ToLower(CultureInfo.InvariantCulture).Split(',').ToList();

                            var commodityKey = ((Utilities.Enums.CommodityType)service.CommodityId).ToString().ToLower(CultureInfo.InvariantCulture);
                            if (!commodities.Exists(c => c == commodityKey)) continue;
                            var serviceSubscription = new SubscriptionModel
                            {
                                CommodityKey = commodityKey,
                                IsSelected = false,
                                Channel = programInsight.DefaultCommunicationChannel,
                                ProgramName = program.ProgramName,
                                InsightTypeName = programInsight.InsightName,
                                ServiceContractId = service.ServiceContractId,
                                ServicePointId = service.ServicePointId,
                                PremiseId = service.PremiseId,
                                AccountId = service.AccountId,
                                CustomerId = customerId,
                                ClientId = clientId,
                                AllowedChannels = programInsight.AllowedCommunicationChannel,
                                // set up UOM ID only for service usage threshold insight
                                UomId = !string.IsNullOrEmpty(programInsight.ThresholdType) && programInsight.ThresholdType.Equals("usage", StringComparison.CurrentCultureIgnoreCase) ? (service.UOMId ?? (int)Utilities.Enums.UomType.Other) : (int?)null,
                                IsEmailOptInCompleted = true,
                                IsSmsOptInCompleted = true
                            };
                            if (!string.IsNullOrEmpty(programInsight.DefaultValue))
                            {
                                serviceSubscription.SubscriberThresholdValue = programInsight.GetCommodityTypevalue(serviceSubscription.CommodityKey);
                                serviceSubscription.ThresholdMin = Convert.ToDouble(programInsight.ThresholdMin);
                                serviceSubscription.ThresholdMax = Convert.ToDouble(programInsight.ThresholdMax);
                                serviceSubscription.ThresholdType = programInsight.ThresholdType;
                            }
                            subscriptions.Add(serviceSubscription);
                        }
                    }
                }
            }
            return subscriptions;
        }
    }
}
