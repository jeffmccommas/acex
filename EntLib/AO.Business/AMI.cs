﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using AO.BusinessContracts;
using AO.DataAccess.Contracts;
using AO.Entities;
using AutoMapper;
using CE.AO.Models;
using CE.RateModel;
using Enums = CE.AO.Utilities.Enums;

namespace AO.Business
{
    /// <summary>
    /// AMI class is used to fetch/insert/update the ami readings from/to the database
    /// </summary>
    public class Ami : IAmi
    {
        public Ami(LogModel logModel, Lazy<ICassandraRepository> cassandraRepository)
        {
            LogModel = logModel;
            CassandraRepository = cassandraRepository;
        }

        public LogModel LogModel { get; set; }
        
        private Lazy<ICassandraRepository> CassandraRepository { get; }

        /// <summary>
        /// To insert the Ami15MinuteIntervalModel model to the database
        /// </summary>
        /// <param name="model">Ami15MinuteIntervalModel model which is to be inserted</param>
        /// <returns>True</returns>
        public async Task<bool> InsertAmiAsync(Ami15MinuteIntervalModel model)
        {
            var entityModel = Mapper.Map<Ami15MinuteIntervalModel, Ami15MinuteIntervalEntity>(model);
            entityModel.AmiTimeStamp = entityModel.AmiTimeStamp.Date;
            await CassandraRepository.Value.InsertOrUpdateAsync(entityModel, Constants.CassandraTableNames.Ami).ConfigureAwait(false);
            return await Task.Factory.StartNew(() => true);
        }

        /// <summary>
        /// To insert the Ami30MinuteIntervalModel model to the database
        /// </summary>
        /// <param name="model">Ami30MinuteIntervalModel model which is to be inserted</param>
        /// <returns>True</returns>
        public async Task<bool> InsertAmiAsync(Ami30MinuteIntervalModel model)
        {
            var entityModel = Mapper.Map<Ami30MinuteIntervalModel, Ami30MinuteIntervalEntity>(model);
            entityModel.AmiTimeStamp = entityModel.AmiTimeStamp.Date;
            await CassandraRepository.Value.InsertOrUpdateAsync(entityModel, Constants.CassandraTableNames.Ami).ConfigureAwait(false);
            return await Task.Factory.StartNew(() => true);
        }

        /// <summary>
        /// To insert the Ami60MinuteIntervalModel model to the database
        /// </summary>
        /// <param name="model">Ami60MinuteIntervalModel model which is to be inserted</param>
        /// <returns>True</returns>
        public async Task<bool> InsertAmiAsync(Ami60MinuteIntervalModel model)
        {
            var entityModel = Mapper.Map<Ami60MinuteIntervalModel, Ami60MinuteIntervalEntity>(model);
            entityModel.AmiTimeStamp = entityModel.AmiTimeStamp.Date;
            await CassandraRepository.Value.InsertOrUpdateAsync(entityModel, Constants.CassandraTableNames.Ami).ConfigureAwait(false);
            return await Task.Factory.StartNew(() => true);
        }

        /// <summary>
        /// To insert the Ami60MinuteIntervalModel model to the database
        /// </summary>
        /// <param name="model">Ami60MinuteIntervalModel model which is to be inserted</param>
        /// <returns>True</returns>
        public async Task<bool> InsertAmiAsync(AmiDailyIntervalModel model)
        {
            var entityModel = Mapper.Map<AmiDailyIntervalModel, AmiDailyIntervalEntity>(model);
            entityModel.AmiTimeStamp = entityModel.AmiTimeStamp.Date;
            await CassandraRepository.Value.InsertOrUpdateAsync(entityModel, Constants.CassandraTableNames.Ami).ConfigureAwait(false);
            return await Task.Factory.StartNew(() => true);
        }

        /// <summary>
        /// To insert/update the model to the database
        /// </summary>
        /// <typeparam name="T">Type of model which is to be inserted/updated</typeparam>
        /// <param name="model">Model which is to be inserted/updated</param>
        /// <param name="interval">Interval of the data of the model</param>
        /// <returns>True</returns>
        public async Task<bool> InsertOrMergeAmiAsync<T>(T model, Enums.IntervalType interval) where T : new()
        {
            switch (interval)
            {
                case Enums.IntervalType.None:
                    return false;
                case Enums.IntervalType.Fifteen:
                    var entityModel15Min = Mapper.Map<T, Ami15MinuteIntervalEntity>(model);
                    entityModel15Min.AmiTimeStamp = entityModel15Min.AmiTimeStamp.Date;
                    await CassandraRepository.Value.InsertOrUpdateAsync(entityModel15Min,Constants.CassandraTableNames.Ami).ConfigureAwait(false);
                    return await Task.Factory.StartNew(() => true);
                case Enums.IntervalType.Thirty:
                    var entityModel30Min = Mapper.Map<T, Ami30MinuteIntervalEntity>(model);
                    entityModel30Min.AmiTimeStamp = entityModel30Min.AmiTimeStamp.Date;
                    await CassandraRepository.Value.InsertOrUpdateAsync(entityModel30Min, Constants.CassandraTableNames.Ami).ConfigureAwait(false);
                    return await Task.Factory.StartNew(() => true);
                case Enums.IntervalType.Sixty:
                    var entityModel60Min = Mapper.Map<T, Ami60MinuteIntervalEntity>(model);
                    entityModel60Min.AmiTimeStamp = entityModel60Min.AmiTimeStamp.Date;
                    await CassandraRepository.Value.InsertOrUpdateAsync(entityModel60Min, Constants.CassandraTableNames.Ami).ConfigureAwait(false);
                    return await Task.Factory.StartNew(() => true);
                case Enums.IntervalType.Daily:
                    var entityModelDaily = Mapper.Map<T, AmiDailyIntervalEntity>(model);
                    entityModelDaily.AmiTimeStamp = entityModelDaily.AmiTimeStamp.Date;
                    await CassandraRepository.Value.InsertOrUpdateAsync(entityModelDaily, Constants.CassandraTableNames.Ami).ConfigureAwait(false);
                    return await Task.Factory.StartNew(() => true);
            }

            return false;
        }

        /// <summary>
        /// To fetch the list of models from the database
        /// </summary>
        /// <param name="startDate">Start date of the data to be fetched</param>
        /// <param name="endDate">End date of the data to be fetched</param>
        /// <param name="meterId">Meter Id of the data to be fetched</param>
        /// <param name="clientId">Client Id of the data to be fetched</param>
        /// <returns>List of model</returns>
        public IEnumerable<dynamic> GetAmiList(DateTime startDate, DateTime endDate, string meterId, int clientId)
        {
            IList<Ami15MinuteIntervalEntity> amiList = CassandraRepository.Value.Get<Ami15MinuteIntervalEntity>(Constants.CassandraTableNames.Ami,a =>a.ClientId == clientId && a.MeterId == meterId && a.AmiTimeStamp >= startDate && a.AmiTimeStamp <= endDate);
            
            if (amiList != null && amiList.Count > 0)
            {
                var amiData = amiList.First();
                var intervalType = amiData.IntervalType;
                if (intervalType.Equals((int)Enums.IntervalType.Fifteen))
                {
                    return  Mapper.Map<List<Ami15MinuteIntervalModel>>(amiList);
                }
                if (intervalType.Equals((int)Enums.IntervalType.Thirty))
                {
                    return Mapper.Map<List<Ami30MinuteIntervalModel>>(amiList);
                }
                if (intervalType.Equals((int)Enums.IntervalType.Sixty))
                {
                    return Mapper.Map<List<Ami60MinuteIntervalModel>>(amiList);
                }
                if (intervalType.Equals((int)Enums.IntervalType.Daily))
                {
                    return Mapper.Map<List<AmiDailyIntervalModel>>(amiList);
                }
            }
            return null;
        }

        /// <summary>
        /// retrieve ami data list by the requested inteval type
        /// </summary>
        /// <param name="startDate"></param>
        /// <param name="endDate"></param>
        /// <param name="meterId"></param>
        /// <param name="clientId"></param>
        /// <param name="intervalType"></param>
        /// <returns></returns>
        public IEnumerable<dynamic> GetAmiListByInterval(DateTime startDate, DateTime endDate, string meterId, int clientId, int intervalType)
        {
            IList<Ami15MinuteIntervalEntity> amiList = CassandraRepository.Value.Get<Ami15MinuteIntervalEntity>(Constants.CassandraTableNames.Ami, a => a.ClientId == clientId && a.MeterId == meterId && a.AmiTimeStamp >= startDate && a.AmiTimeStamp <= endDate);

            var intervalEntities = amiList.Where(a => a.IntervalType == intervalType);

            if (intervalEntities.Any())
            {
                if (intervalType.Equals((int)Enums.IntervalType.Fifteen))
                {
                    return Mapper.Map<List<Ami15MinuteIntervalModel>>(intervalEntities);
                }
                if (intervalType.Equals((int)Enums.IntervalType.Thirty))
                {
                    return Mapper.Map<List<Ami30MinuteIntervalModel>>(intervalEntities);
                }
                if (intervalType.Equals((int)Enums.IntervalType.Sixty))
                {
                    return Mapper.Map<List<Ami60MinuteIntervalModel>>(intervalEntities);
                }
                if (intervalType.Equals((int)Enums.IntervalType.Daily))
                {
                    return Mapper.Map<List<AmiDailyIntervalModel>>(intervalEntities);
                }
            }
            return null;
        }

        /// <summary>
        /// To fetch the single model from the database
        /// </summary>
        /// <param name="date">Date of the data to be fetched</param>
        /// <param name="meterId">Meter Id of the data to be fetched</param>
        /// <param name="clientId">Client Id of the data to be fetched</param>
        /// <returns>Model</returns>
        public async Task<dynamic> GetAmi(DateTime date, string meterId, int clientId)
        {
            var ami =
                await CassandraRepository.Value.GetSingleAsync<Ami15MinuteIntervalEntity>(Constants.CassandraTableNames.Ami,a => a.ClientId == clientId && a.MeterId == meterId && a.AmiTimeStamp == date.Date);
            
            if (ami == null) return null;
            var intervalType = ami.IntervalType;
            if (intervalType.Equals((int)Enums.IntervalType.Fifteen))
            {
                return  Mapper.Map<Ami15MinuteIntervalModel>(ami);
            }
            if (intervalType.Equals((int)Enums.IntervalType.Thirty))
            {
                return Mapper.Map<Ami30MinuteIntervalModel>(ami);
            }
            if (intervalType.Equals((int)Enums.IntervalType.Sixty))
            {
                return Mapper.Map<Ami60MinuteIntervalModel>(ami);
            }
            return intervalType.Equals((int)Enums.IntervalType.Daily) ? Mapper.Map<AmiDailyIntervalModel>(ami) : null;

        }

        /// <summary>
        /// To convert from ami list to Reading list
        /// </summary>
        /// <param name="amiList">List of model from which readings to be converted</param>
        /// <param name="noOfDays">Total number of days of which reading is returned</param>
        /// <returns>List of Reading</returns>
        public List<Reading> GetReadings(IEnumerable<dynamic> amiList, out int noOfDays)
        {
            var readings = new List<Reading>();
            noOfDays = 0;

            if (amiList == null)
            {
                return readings;
            }

            foreach (var row in amiList)
            {
                noOfDays++;
                var amiRow = row;
                var propertyList = amiRow.GetType()
                    .GetProperties();
                //Get interval type. Needed in case of daily to get touid.
                var intervalType = Convert.ToInt32(amiRow.GetType().GetProperty("IntervalType").GetValue(amiRow));

                foreach (var property in propertyList)
                {
                    double quantity;
                    dynamic reading;
                    //If interval type is 24 then get touid then get reading using that touid 
                    if (intervalType == 24)
                    {
                        var timestamp = new DateTime(amiRow.AmiTimeStamp.Year, amiRow.AmiTimeStamp.Month,
                            amiRow.AmiTimeStamp.Day, 00, 00, 0);
                        if (!property.Name.ToString().Contains("TOU_")) continue;
                        var touId = Extensions.GetTouIdFromProperty(property);
                        var value = property.GetValue(amiRow);
                        if (value != null)
                        {
                            quantity = Convert.ToDouble(value);
                            reading = new Reading((CE.RateModel.Enums.TimeOfUse)touId, timestamp, quantity);
                            readings.Add(reading);
                        }
                    }
                    else
                    {
                        if (!property.Name.ToString().Contains("IntValue")) continue;
                        var hh = Convert.ToInt32(property.Name.Replace("IntValue", "").Substring(0, 2));
                        var mm = Convert.ToInt32(property.Name.Replace("IntValue", "").Substring(2, 2));
                        if (hh >= 24) continue;
                        var timestamp = new DateTime(amiRow.AmiTimeStamp.Year, amiRow.AmiTimeStamp.Month,
                            amiRow.AmiTimeStamp.Day, hh, mm, 0);
                        quantity = Convert.ToDouble(property.GetValue(amiRow));
                        reading = new Reading(timestamp, quantity);
                        readings.Add(reading);
                    }
                }
            }
            return readings;
        }

        /// <summary>
        /// To convert from ami list to ReadingModel
        /// </summary>
        /// <param name="amiList">List of model from which readings to be converted</param>
        /// <returns>List of ReadingModel model</returns>
        public List<ReadingModel> GetReadings(IEnumerable<dynamic> amiList)
        {
            var readings = new List<ReadingModel>();
            int uomId = 0;
            if (amiList == null)
            {
                return readings;
            }

            foreach (var row in amiList)
            {
                int interval = Convert.ToInt32(row.IntervalType.ToString());
                var amiRow = row;
                var propertyList = amiRow.GetType()
                    .GetProperties();
                switch (interval)
                {
                    case 15:
                        var ami15Row = (Ami15MinuteIntervalModel)row;
                        uomId = ami15Row.UOMId;
                        break;
                    case 30:
                        var ami30Row = (Ami30MinuteIntervalModel)row;
                        uomId = ami30Row.UOMId;
                        break;
                    case 60:
                        var ami60Row = (Ami60MinuteIntervalModel)row;
                        uomId = ami60Row.UOMId;
                        break;
                    case 24:
                        var ami24Row = (AmiDailyIntervalModel)row;
                        uomId = ami24Row.UOMId;
                        break;
                }
                foreach (var property in propertyList)
                {
                    double quantity;
                    ReadingModel reading;
                    //If interval type is 24 then get touid then get reading using that touid 
                    if (interval == 24)
                    {
                        var timestamp = new DateTime(amiRow.AmiTimeStamp.Year, amiRow.AmiTimeStamp.Month,
                            amiRow.AmiTimeStamp.Day, 00, 00, 0);
                        if (!property.Name.ToString().Contains("TOU_")) continue;
                        var touId = GetTOUIDFromProperty(property);
                        var value = property.GetValue(amiRow);
                        if (value != null)
                        {
                            quantity = Convert.ToDouble(value);
                            reading = new ReadingModel((CE.RateModel.Enums.TimeOfUse)touId, timestamp, quantity);
                            reading.TimestampString = reading.Timestamp.ToString("o");
                            reading.UOMId = uomId;
                            readings.Add(reading);
                        }
                    }
                    else
                    {
                        if (!property.Name.ToString().Contains("IntValue")) continue;
                        var hh = Convert.ToInt32(property.Name.Replace("IntValue", "").Substring(0, 2));
                        var mm = Convert.ToInt32(property.Name.Replace("IntValue", "").Substring(2, 2));
                        if (hh >= 24) continue;
                        var timestamp = new DateTime(amiRow.AmiTimeStamp.Year, amiRow.AmiTimeStamp.Month,
                            amiRow.AmiTimeStamp.Day, hh, mm, 0);
                        quantity = Convert.ToDouble(property.GetValue(amiRow));
                        reading = new ReadingModel(timestamp, quantity);
                        reading.TimestampString = reading.Timestamp.ToString("o");
                        reading.UOMId = uomId;
                        readings.Add(reading);
                    }
                }
            }
            return readings;
        }

        /// <summary>
        /// To get the list of daily Reading model
        /// </summary>
        /// <param name="amiList">List of ami from which reading is to be fetched</param>
        /// <param name="intervalType"></param>
        /// <returns>List of Reading model</returns>
        public List<Reading> GetDailyReadings(IEnumerable<dynamic> amiList, int intervalType)
        {
            List<Reading> readings;
            // check if the ami is daily reading model
            // if it is, call GetReadings to convert it to daily ratemodel reading instead of summing all the interval readings up into daily
            var enumerable = amiList as dynamic[] ?? amiList.ToArray();
            if (intervalType == 24)
            {
                int noOfDays;
                readings = GetReadings(enumerable, out noOfDays);
            }
            else
            {
                readings = new List<Reading>();
                foreach (var row in enumerable)
                {
                    var amiRow = row;
                    var date = amiRow.AmiTimeStamp;
                    var propertyList = amiRow.GetType()
                        .GetProperties();

                    var quantity = (double)0;
                    foreach (var property in propertyList)
                    {
                        if (!property.Name.ToString().Contains("IntValue")) continue;
                        var value = property.GetValue(amiRow);
                        if (value != null)
                        {
                            quantity = (double)property.GetValue(amiRow) + quantity;
                        }
                    }
                    var reading = new Reading(date, quantity);
                    readings.Add(reading);
                }
            }

            return readings;
        }

        /// <summary>
        /// To get the list of monthly Reading model
        /// </summary>
        /// <param name="amiList">List of ami from which reading is to be fetched</param>
        /// <param name="intervalType"></param>
        /// <returns>List of Reading model</returns>
        public List<Reading> GetMonthlyReadings(IEnumerable<dynamic> amiList, int intervalType)
        {
            var readings = new List<Reading>();
            var timeStamp = DateTime.MinValue;
            var noTou = (double)0;
            var onPeak = (double)0;
            var offPeak = (double)0;
            var criticalPeak = (double)0;
            var shoulder1 = (double)0;
            var shoulder2 = (double)0;
            bool hasOnPeak = false, hasOffPeak = false, hasCriticalPeak = false, hasShoulder1 = false, hasShoulder2 = false;

            foreach (var row in amiList)
            {
                var amiRow = row;
                
                if (timeStamp == DateTime.MinValue)
                {
                    timeStamp = new DateTime(amiRow.AmiTimeStamp.Year, amiRow.AmiTimeStamp.Month, 1);
                    noTou = 0;
                    onPeak = 0;
                    offPeak = 0;
                    criticalPeak = 0;
                    shoulder1 = 0;
                    shoulder2 = 0;
                }
                if (amiRow.AmiTimeStamp.Month != timeStamp.Month)
                {
                    var reading = new Reading(timeStamp, noTou);
                    readings.Add(reading);
                    // add monthly tou readings if exists
                    if (hasOnPeak)
                    {
                        reading = new Reading(timeStamp, onPeak);
                        reading.TimeOfUse = CE.RateModel.Enums.TimeOfUse.OnPeak;
                        readings.Add(reading);
                    }
                    if (hasOffPeak)
                    {
                        reading = new Reading(timeStamp, offPeak);
                        reading.TimeOfUse = CE.RateModel.Enums.TimeOfUse.OffPeak;
                        readings.Add(reading);
                    }
                    if (hasShoulder1)
                    {
                        reading = new Reading(timeStamp, shoulder1);
                        reading.TimeOfUse = CE.RateModel.Enums.TimeOfUse.Shoulder1;
                        readings.Add(reading);
                    }
                    if (hasShoulder2)
                    {
                        reading = new Reading(timeStamp, shoulder2);
                        reading.TimeOfUse = CE.RateModel.Enums.TimeOfUse.Shoulder2;
                        readings.Add(reading);
                    }
                    if (hasCriticalPeak)
                    {
                        reading = new Reading(timeStamp, criticalPeak);
                        reading.TimeOfUse = CE.RateModel.Enums.TimeOfUse.CriticalPeak;
                        readings.Add(reading);
                    }

                    timeStamp = new DateTime(amiRow.AmiTimeStamp.Year, amiRow.AmiTimeStamp.Month, 1);
                    noTou = 0;
                    onPeak = 0;
                    offPeak = 0;
                    criticalPeak = 0;
                    shoulder1 = 0;
                    shoulder2 = 0;
                }
                var propertyList = amiRow.GetType()
                    .GetProperties();

                foreach (var property in propertyList)
                {

                    if (intervalType == 24)
                    {
                        if (!property.Name.ToString().Contains("TOU_")) continue;
                        var touId = GetTOUIDFromProperty(property);
                        var value = property.GetValue(amiRow);
                        if (value != null)
                        {
                            var quantity = Convert.ToDouble(value);
                            switch ((int)touId)
                            {
                                case 0:
                                    noTou = noTou + quantity;
                                    break;
                                case 1:
                                    onPeak = onPeak + quantity;
                                    hasOnPeak = true;
                                    break;
                                case 2:
                                    offPeak = offPeak + quantity;
                                    hasOffPeak = true;
                                    break;
                                case 3:
                                    shoulder1 = shoulder1 + quantity;
                                    hasShoulder1 = true;
                                    break;
                                case 4:
                                    shoulder2 = shoulder2 + quantity;
                                    hasShoulder2 = true;
                                    break;
                                case 5:
                                    criticalPeak = criticalPeak + quantity;
                                    hasCriticalPeak = true;
                                    break;
                            }
                        }
                    }
                    else
                    {
                        if (!property.Name.ToString().Contains("IntValue")) continue;

                        var value = property.GetValue(amiRow);
                        if (value != null)
                        {
                            noTou = (double)property.GetValue(amiRow) + noTou;
                        }
                    }
                }
            }
            // the last monthly reading
            readings.Add(new Reading(timeStamp, noTou));

            // add monthly tou readings if exists
            if (hasOnPeak)
            {
                var reading = new Reading(timeStamp, onPeak);
                reading.TimeOfUse = CE.RateModel.Enums.TimeOfUse.OnPeak;
                readings.Add(reading);
            }
            if (hasOffPeak)
            {
                var reading = new Reading(timeStamp, offPeak);
                reading.TimeOfUse = CE.RateModel.Enums.TimeOfUse.OffPeak;
                readings.Add(reading);
            }
            if (hasShoulder1)
            {
                var reading = new Reading(timeStamp, shoulder1);
                reading.TimeOfUse = CE.RateModel.Enums.TimeOfUse.Shoulder1;
                readings.Add(reading);
            }
            if (hasShoulder2)
            {
                var reading = new Reading(timeStamp, shoulder2);
                reading.TimeOfUse = CE.RateModel.Enums.TimeOfUse.Shoulder2;
                readings.Add(reading);
            }
            if (hasCriticalPeak)
            {
                var reading = new Reading(timeStamp, criticalPeak);
                reading.TimeOfUse = CE.RateModel.Enums.TimeOfUse.CriticalPeak;
                readings.Add(reading);
            }
            return readings;
        }

        /// <summary>
        /// Get TouId depending on column
        /// </summary>
        /// <param name="property">property of model</param>
        /// <returns>TouId</returns>
        private int GetTOUIDFromProperty(dynamic property)
        {
            if (property.Name.ToString().Equals("TOU_Regular"))
                return 0;
            if (property.Name.ToString().Equals("TOU_OnPeak"))
                return 1;
            if (property.Name.ToString().Equals("TOU_OffPeak"))
                return 2;
            if (property.Name.ToString().Equals("TOU_Shoulder1"))
                return 3;
            if (property.Name.ToString().Equals("TOU_Shoulder2"))
                return 4;
            //if (property.Name.ToString().Equals("TOU_CriticalPeak"))
            return 5;
        }

        public List<Reading> GetAmiReadings(int clientId, string meterId, DateTime startDate, DateTime endDate, string requestedResolution, out int intervalType, out int amiUomId, out int amiCommodityId)
        {
            amiUomId = 99;
            amiCommodityId = 0;
            intervalType = 0;
            List<Reading> readings = null;

            if (requestedResolution.ToLower(CultureInfo.InvariantCulture) == "month")
            {
                startDate = new DateTime(startDate.Year, startDate.Month, 1);
                if (endDate.Month != DateTime.Today.Month && endDate.Year != DateTime.Today.Year)
                {
                    endDate = new DateTime(endDate.AddMonths(1).Year, endDate.AddMonths(1).Month, 1).AddDays(-1);
                }
            }

            var amiList = GetAmiList(startDate, endDate, meterId, clientId);
            if (amiList != null)
            {
                var enumerable = amiList as dynamic[] ?? amiList.ToArray();
                if (enumerable.Any())
                {
                    var type = amiList.GetType();

                    if (type == typeof(List<Ami15MinuteIntervalModel>))
                    {
                        intervalType = 15;
                        var ami = (List<Ami15MinuteIntervalModel>)amiList;
                        amiUomId = ami[0].UOMId;
                        amiCommodityId = ami[0].CommodityId;
                    }
                    else if (type == typeof(List<Ami30MinuteIntervalModel>))
                    {
                        intervalType = 30;
                        var ami = (List<Ami30MinuteIntervalModel>)amiList;
                        amiUomId = ami[0].UOMId;
                        amiCommodityId = ami[0].CommodityId;
                    }
                    else if (type == typeof(List<Ami60MinuteIntervalModel>))
                    {
                        intervalType = 60;
                        var ami = (List<Ami60MinuteIntervalModel>)amiList;
                        amiUomId = ami[0].UOMId;
                        amiCommodityId = ami[0].CommodityId;
                    }
                    else if (type == typeof(List<AmiDailyIntervalModel>))
                    {
                        intervalType = 24;
                        var ami = (List<AmiDailyIntervalModel>)amiList;
                        amiUomId = ami[0].UOMId;
                        amiCommodityId = ami[0].CommodityId;
                    }

                    switch (requestedResolution.ToLower(CultureInfo.InvariantCulture))
                    {
                        case "day":
                            readings = GetDailyReadings(enumerable, intervalType);
                            break;
                        case "month":
                            readings = GetMonthlyReadings(enumerable, intervalType);
                            break;
                        default:
                            var getReading = false;
                            if ((intervalType == 15 && requestedResolution == "fifteen") ||
                                (intervalType == 30 && requestedResolution == "thirty") ||
                                (intervalType == 60 && requestedResolution == "hour") ||
                                (requestedResolution == string.Empty))
                                getReading = true;
                            if (getReading)
                            {
                                int noOfDays;
                                readings = GetReadings(enumerable, out noOfDays);
                            }
                            break;

                    }
                }
            }

            return readings;
        }

    }
}