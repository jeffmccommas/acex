﻿using System;
using System.Threading.Tasks;
using AO.BusinessContracts;
using AO.DataAccess.Contracts;
using AO.Entities;
using AutoMapper;
using CE.AO.Models;

namespace AO.Business
{
    /// <summary>
    /// Calculation class is used to insert/fetch the BTD and CTD calculation to/from the database
    /// </summary>
    public class Calculation : ICalculation
    {
        public Calculation(LogModel logModel, Lazy<ICassandraRepository> cassandraRepository)
        {
            LogModel = logModel;
            CassandraRepository = cassandraRepository;
        }

        public LogModel LogModel { get; set; }

        private Lazy<ICassandraRepository> CassandraRepository { get; }

        /// <summary>
        /// To insert the CTD calculation model to the database
        /// </summary>
        /// <param name="model">CalculationModel model to be inserted</param>
        /// <returns>True</returns>
        public async Task InsertCtdCalculationAsync(CalculationModel model)
        {
            var entityModel = Mapper.Map<CalculationModel, CalculationEntity>(model);
            entityModel.Type = StaticConfig.CtdType;
            var dbCalculationEntity =
                CassandraRepository.Value.GetSingle<CalculationEntity>(Constants.CassandraTableNames.Calculation,
                    a =>
                        a.ClientId == model.ClientId && a.AccountId == model.AccountId && a.Type == StaticConfig.CtdType &&
                        // ReSharper disable once RedundantBoolCompare we need to explicitly specify IsLatest == true, otherwise it gives error. TO execute cassandra query required to provide all values.
                        a.ServiceContractId == model.ServiceContractId && a.IsLatest == true);

            if (dbCalculationEntity != null && dbCalculationEntity.AsOfCalculationDate < model.AsOfCalculationDate)
            {
                await CassandraRepository.Value.DeleteAsync<CalculationEntity>(Constants.CassandraTableNames.Calculation, a =>
                    a.ClientId == model.ClientId && a.AccountId == model.AccountId && a.Type == StaticConfig.CtdType &&
                    // ReSharper disable once RedundantBoolCompare we need to explicitly specify IsLatest == true, otherwise it gives error. TO execute cassandra query required to provide all values.
                    a.ServiceContractId == model.ServiceContractId && a.IsLatest == true);
            }

            entityModel.IsLatest = false;
            await CassandraRepository.Value.InsertOrUpdateAsync(entityModel, Constants.CassandraTableNames.Calculation).ConfigureAwait(false);

            entityModel.IsLatest = true;
            await CassandraRepository.Value.InsertOrUpdateAsync(entityModel, Constants.CassandraTableNames.Calculation).ConfigureAwait(false);

        }

        /// <summary>
        /// To insert th BTD calculation model to the database
        /// </summary>
        /// <param name="model">CalculationModel model to be inserted</param>
        /// <returns>True</returns>
        public async Task InsertBtdCalculationAsync(CalculationModel model)
        {
            var entityModel = Mapper.Map<CalculationModel, CalculationEntity>(model);
            entityModel.Type = StaticConfig.BtdType;
            var dbCalculationEntity =
                CassandraRepository.Value.GetSingle<CalculationEntity>(Constants.CassandraTableNames.Calculation,
                    a =>
                        a.ClientId == model.ClientId && a.AccountId == model.AccountId && a.Type == StaticConfig.BtdType &&
                        // ReSharper disable once RedundantBoolCompare we need to explicitly specify IsLatest == true, otherwise it gives error. TO execute cassandra query required to provide all values.
                        a.ServiceContractId == model.ServiceContractId && a.IsLatest == true);
            if (dbCalculationEntity != null && dbCalculationEntity.AsOfCalculationDate < model.AsOfCalculationDate)
            {
                await CassandraRepository.Value.DeleteAsync<CalculationEntity>(Constants.CassandraTableNames.Calculation, a =>
                    a.ClientId == model.ClientId && a.AccountId == model.AccountId && a.Type == StaticConfig.BtdType &&
                    // ReSharper disable once RedundantBoolCompare we need to explicitly specify IsLatest == true, otherwise it gives error. TO execute cassandra query required to provide all values.
                    a.ServiceContractId == model.ServiceContractId && a.IsLatest == true);
            }

            entityModel.IsLatest = false;
            await CassandraRepository.Value.InsertOrUpdateAsync(entityModel, Constants.CassandraTableNames.Calculation).ConfigureAwait(false);

            entityModel.IsLatest = true;
            await CassandraRepository.Value.InsertOrUpdateAsync(entityModel, Constants.CassandraTableNames.Calculation).ConfigureAwait(false);

        }

        /// <summary>
        /// To fetch BTD calculation model from the database
        /// </summary>
        /// <param name="clientId">Client id of the CalculationModel model</param>
        /// <param name="accountId">Account id of the CalculationModel model</param>
        /// <returns>CalculationModel</returns>
        public async Task<CalculationModel> GetBtdCalculationAsync(int clientId, string accountId)
        {
            var btd =
                // ReSharper disable once RedundantBoolCompare we need to explicitly specify IsLatest == true, otherwise it gives error. TO execute cassandra query required to provide all values.
                await CassandraRepository.Value.GetSingleAsync<CalculationEntity>(Constants.CassandraTableNames.Calculation,
                        a => a.ClientId == clientId && a.AccountId == accountId && a.IsLatest == true && a.Type == StaticConfig.BtdType);
            return Mapper.Map<CalculationEntity, CalculationModel>(btd);
        }

        /// <summary>
        /// To fetch CTD calculation model from the database
        /// </summary>
        /// <param name="clientId">Client id of the CalculationModel model</param>
        /// <param name="accountId">Account id of the CalculationModel model</param>
        /// <param name="serviceContractId">Service contract id of the CalculationModel model</param>
        /// <returns>CalculationModel</returns>
        public async Task<CalculationModel> GetCtdCalculationAsync(int clientId, string accountId, string serviceContractId)
        {
            var ctd =
                await
                    CassandraRepository.Value.GetSingleAsync<CalculationEntity>(Constants.CassandraTableNames.Calculation,
                        a =>
                            // ReSharper disable once RedundantBoolCompare we need to explicitly specify IsLatest == true, otherwise it gives error. TO execute cassandra query required to provide all values.
                            a.ClientId == clientId && a.AccountId == accountId && a.IsLatest == true && a.ServiceContractId == serviceContractId &&
                            a.Type == StaticConfig.CtdType);
            return Mapper.Map<CalculationEntity, CalculationModel>(ctd);
        }

        /// <summary>
        /// Delete BtdCalcultation from Table
        /// </summary>
        /// <param name="clientId"></param>
        /// <param name="accountId"></param>
        /// <returns></returns>
        public async Task DeleteBtdCalculationAsyc(int clientId, string accountId)
        {
            await
                CassandraRepository.Value.DeleteAsync<CalculationEntity>(Constants.CassandraTableNames.Calculation,
                    a => a.ClientId == clientId && a.AccountId == accountId).ConfigureAwait(false);
        }

        /// <summary>
        /// Delete CtdCalcultation from Table
        /// </summary>
        /// <param name="clientId"></param>
        /// <param name="accountId"></param>
        /// <returns></returns>
        public async Task DeleteCtdCalculationAsyc(int clientId, string accountId)
        {
            await
                CassandraRepository.Value.DeleteAsync<CalculationEntity>(Constants.CassandraTableNames.Calculation,
                    a => a.ClientId == clientId && a.AccountId == accountId).ConfigureAwait(false);
        }
    }
}
