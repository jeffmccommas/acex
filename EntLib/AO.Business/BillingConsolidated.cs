﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AO.BusinessContracts;
using AO.DataAccess.Contracts;
using AO.Entities;
using AutoMapper;
using CE.Models;
using CE.AO.Models;

namespace AO.Business
{
    public class BillingConsolidated : IBilling
    {
        public LogModel LogModel { get; set; }

        /// <summary>
        /// To fetch the all latest bills from the database
        /// </summary>
        /// <param name="clientId">clientId of the data to be fetched</param>
        /// <param name="customerId">customerId of the data to be fetched</param>
        /// <param name="accountId">accountId of the data to be fetched</param>
        /// <returns>List of BillingModel</returns>
        public IEnumerable<BillingModel> GetAllServicesFromLastBill(int clientId, string customerId, string accountId)
        {
            var repository = new InsightsEfRepository();
            var billToDate = repository.GetBillToDate(clientId, accountId, customerId);
            Mapper.CreateMap<CE.Models.Insights.EF.GetBillToDate_Result, BillingModel>()
                .ForMember(o => o.IsLatest, d => d.UseValue(true))
                .ForAllUnmappedMembers(o => o.Ignore());
            return Mapper.Map<IEnumerable<CE.Models.Insights.EF.GetBillToDate_Result>, IEnumerable<BillingModel>>(billToDate);
        }

        /// <summary>
        /// To fetch the all bills of specified serviceContractId only from the database
        /// </summary>
        /// <param name="clientId">clientId of the data to be fetched</param>
        /// <param name="customerId">customerId of the data to be fetched</param>
        /// <param name="accountId">accountId of the data to be fetched</param>
        /// <param name="serviceContractId">serviceContractId of the data to be fetched</param>
        /// <param name="startDate">startDate of the data to be fetched</param>
        /// <returns>List of BillingModel</returns>
        public IEnumerable<BillingModel> GetBillsForService(int clientId, string customerId, string accountId,
            string serviceContractId, DateTime startDate)
        {
            var repository = new InsightsEfRepository();
            var billToDate = repository.GetBillToDate(clientId, accountId, customerId, serviceContractId, startDate);
            Mapper.CreateMap<CE.Models.Insights.EF.GetBillToDate_Result, BillingModel>()
                .ForMember(o => o.IsLatest, d => d.UseValue(true))
                .ForAllUnmappedMembers(o => o.Ignore());
            return Mapper.Map<IEnumerable<CE.Models.Insights.EF.GetBillToDate_Result>, IEnumerable<BillingModel>>(billToDate);
        }

        /// <summary>
        /// To fetch the all history bills within the date range only from the database
        /// </summary>
        /// <param name="clientId">clientId of the data to be fetched</param>
        /// <param name="customerId">customerId of the data to be fetched</param>
        /// <param name="accountId">accountId of the data to be fetched</param>
        /// <param name="serviceContractId">serviceContractId of the data to be fetched</param>
        /// <param name="startDate">startDate of the data to be fetched</param>
        /// <param name="endDate">endDate of the data to be fetched</param>
        /// <returns>List of BillingModel</returns>
        public IEnumerable<BillingModel> GetBillsForService(int clientId, string customerId, string accountId,
            string serviceContractId, DateTime startDate, DateTime endDate)
        {
            var repository = new InsightsEfRepository();
            var bills = repository.GetBillToDate(clientId, accountId, customerId, serviceContractId, startDate);
            var listOfBills = bills.ToList();
            listOfBills.RemoveAll(s => s.StartDate > endDate);

            Mapper.CreateMap<CE.Models.Insights.EF.GetBillToDate_Result, BillingModel>()
                .ForMember(o => o.IsLatest, d => d.UseValue(true))
                .ForAllUnmappedMembers(o => o.Ignore());

            return Mapper.Map<IEnumerable<CE.Models.Insights.EF.GetBillToDate_Result>, IEnumerable<BillingModel>>(listOfBills);
        }

        /// <summary>
        /// To fetch the latest bill from the database
        /// </summary>
        /// <param name="clientId">clientId of the data to be fetched</param>
        /// <param name="customerId">customerId of the data to be fetched</param>
        /// <param name="accountId">accountId of the data to be fetched</param>
        /// <param name="serviceContractId">serviceContractId of the data to be fetched</param>
        /// <returns>BillingModel</returns>
        public async Task<BillingModel> GetServiceDetailsFromBillAsync(int clientId, string customerId, string accountId,
            string serviceContractId)
        {
            var repository = new InsightsEfRepository();
            var billToDate = repository.GetBillToDate(clientId, accountId, customerId, serviceContractId);
            Mapper.CreateMap<CE.Models.Insights.EF.GetBillToDate_Result, BillingModel>()
                .ForMember(o => o.IsLatest, d => d.UseValue(true))
                .ForAllUnmappedMembers(o => o.Ignore());
            return Mapper.Map<IEnumerable<CE.Models.Insights.EF.GetBillToDate_Result>, IEnumerable<BillingModel>>(billToDate).FirstOrDefault();
        }

        /// <summary>
        /// To insert or update latest BillingModel to database
        /// </summary>
        /// <param name="model">model to be inserted</param>
        /// <returns>true</returns>
        public async Task<bool> InsertOrMergeLatestBillAsync(BillingModel model)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// To insert or update history BillingModel to database
        /// </summary>
        /// <param name="model">model to be inserted</param>
        /// <returns>true</returns>
        public async Task<bool> InsertOrMergeHistoryBillAsync(BillingModel model)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// To delete latest bill from database
        /// </summary>
        /// <param name="clientId">clientId of the data to be deleted</param>
        /// <param name="customerId">customerId of the data to be deleted</param>
        /// <param name="accountId">accountId of the data to be deleted</param>
        /// <param name="serviceContractId">serviceContractId of the data to be deleted</param>
        /// <returns>true</returns>
        public async Task<bool> DeleteLatestServiceDetailsAsync(int clientId, string customerId, string accountId,
            string serviceContractId)
        {
            throw new NotImplementedException();
        }


        /// <summary>
        /// To delete history bill from database
        /// </summary>
        /// <param name="clientId">clientId of the data to be deleted</param>
        /// <param name="customerId">customerId of the data to be deleted</param>
        /// <param name="accountId">accountId of the data to be deleted</param>
        /// <param name="serviceContractId">serviceContractId of the data to be deleted</param>
        /// <param name="endDate">endDate of the data to be deleted</param>
        /// <returns>true</returns>
        public async Task<bool> DeleteHistoryServiceDetailsAsync(int clientId, string customerId, string accountId,
            string serviceContractId, DateTime endDate)
        {
            throw new NotImplementedException();
        }
    }
}
