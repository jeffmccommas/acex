﻿using System;
using System.Threading.Tasks;
using AO.BusinessContracts;
using AutoMapper;
using CE.AO.Logging;
using CE.AO.Models;

namespace AO.Business
{
    /// <summary>
    /// Class for AlarmFacade 
    /// </summary>
    public class AlarmFacade : IAlarmFacade
    {
        public LogModel LogModel { get; set; }

        private Lazy<IAlarm> Alarm { get; }

        private Lazy<IAlarmTypes> AlarmType { get;  }

        public AlarmFacade(LogModel logModel, Lazy<IAlarm> alarm, Lazy<IAlarmTypes> alarmTypes)
        {
            LogModel = logModel;
            Alarm = alarm;
            AlarmType = alarmTypes;
        }

        /// <summary>
        /// To insert in Alarms by fetching details from Alarmtypes
        /// </summary>
        /// <param name="model">AlarmModel to be inserted</param>
        /// <returns>true</returns>
        public Task<bool> InsertOrMergeAlarmAsync(AlarmModel model)
        {
            var alarmTypeInfo = AlarmType.Value.GetAlarmType(model.AlarmSourceTypeId, model.AlarmGroupId, model.AlarmTypeId);

            if (alarmTypeInfo != null)
                model = Mapper.Map(alarmTypeInfo,model);

            Alarm.Value.InsertOrMergeAlarmAsync(model);

            if (alarmTypeInfo == null)
                Logger.Error($"No mapping found for Alarm {model.AlarmId}.", LogModel);

            return Task.Factory.StartNew(() => true);
        }
    }
}
