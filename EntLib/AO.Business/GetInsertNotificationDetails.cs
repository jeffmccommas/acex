﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AO.Entities;
using AutoMapper;
using CE.AO.Logging;
using CE.AO.Models;
using CE.AO.Utilities;
using Enums = CE.AO.Utilities.Enums;

namespace AO.Business
{
    /// <summary>
    /// GetInsertNotificationDetails class is used to fetch and insert/update notification details from/in DB
    /// </summary>
    public partial class Notification
    {
        private async Task<NotificationModel> GetNotification(InsightModel evaluation, InsightSettings insightSettings)
        {
            NotificationEntity notification;

            if (insightSettings.DefaultCommunicationChannel.Equals(Enums.SubscriptionChannel.EmailAndSMS.GetDescriptionOfEnum(), StringComparison.CurrentCultureIgnoreCase))
            {
                // First try Email.
                notification = await _notificationRepository.GetNotification(evaluation, 
                    new InsightSettings { InsightName = insightSettings.InsightName, DefaultCommunicationChannel = Enums.SubscriptionChannel.Email.GetDescriptionOfEnum() });             

                if (notification == null || (notification.IsNotified.HasValue && !notification.IsNotified.Value))
                {
                    // Then try SMS.
                    notification = await _notificationRepository.GetNotification(evaluation,
                        new InsightSettings { InsightName = insightSettings.InsightName, DefaultCommunicationChannel = Enums.SubscriptionChannel.SMS.GetDescriptionOfEnum() });
                }
            }
            else
            {
                notification = await _notificationRepository.GetNotification(evaluation, insightSettings);
            }

            return Mapper.Map<NotificationEntity, NotificationModel>(notification);
        }

        /// <summary>
        /// To insert or update latest as well as hitory notification in database
        /// </summary>
        /// <param name="notificationModel">model to be inseted/updated</param>
        /// <returns>task</returns>
        private async Task SaveLatestAndHistoryNotification(NotificationModel notificationModel)
        {
            var entityModel = Mapper.Map<NotificationModel, NotificationEntity>(notificationModel);
            // insert historical copy
            entityModel.IsLatest = false;
            await _notificationRepository.InsertOrMergeNotification(entityModel);

            // insert latest copy
            entityModel.IsLatest = true;
            await _notificationRepository.InsertOrMergeNotification(entityModel);

            try {
                var entityReportModel = Mapper.Map<NotificationModel, NotificationReportEntity>(notificationModel);
                // insert report copy
                entityReportModel.IsLatest = true;
                await _notificationRepository.InsertOrMergeNotificationReport(entityReportModel);
            }
            catch (Exception ex)
            {
                Logger.Error(ex, LogModel);
            }
        }

        /// <summary>
        /// To fetch the notification detail from the database
        /// </summary>
        /// <param name="clientId">clientId of the data to be fetched</param>
        /// <param name="customerId">customerId of the data to be fetched</param>
        /// <param name="accountId">accountId of the data to be fetched</param>
        /// <param name="programName">programName of the data to be fetched</param>
        /// <param name="serviceContractId">serviceContractId of the data to be fetched</param>
        /// <param name="insightName">insightTypeName of the data to be fetched</param>
        /// <param name="defaultCommunicationChannel">default communication channel of the data to be fetched</param>
        /// <param name="isLatest">whether it is latest or history record</param>
        /// <param name="date"></param>
        /// <returns>NotificationModel</returns>
        public NotificationModel GetNotificationSync(int clientId, string customerId, string accountId, string programName, string serviceContractId, string insightName, string defaultCommunicationChannel, bool isLatest, string date)
        {
            var notification = new NotificationEntity
            {
                ClientId = clientId,
                CustomerId = customerId,
                AccountId = accountId,
                ProgramName = programName,
                ServiceContractId = serviceContractId,
                Insight = insightName,
                Channel = defaultCommunicationChannel,
                IsLatest = isLatest,
                NotifiedDateTime =  Convert.ToDateTime(date)
            };
            var result = _notificationRepository.GetNotification(notification);

            return Mapper.Map<NotificationEntity, NotificationModel>(result.Result);
        }

        /// <summary>
        /// Retrieves fully hydrated notification based on the supplied notification model.
        /// </summary>
        /// <param name="notification">Notification model</param>
        /// <returns>Fully hydrated notification model</returns>
        public NotificationModel GetNotificationSync(NotificationModel notification)
        {
            var entity = Mapper.Map<NotificationModel, NotificationEntity>(notification);
            var result = _notificationRepository.GetNotification(entity);
            return Mapper.Map<NotificationEntity, NotificationModel>(result.Result);
        }

        /// <summary>
        /// To fetch all the notification details of specific date from the database
        /// </summary>
        /// <param name="clientId">clientId of the data to be fetched</param>
        /// <param name="customerId">customerId of the data to be fetched</param>
        /// <param name="accountId">accountId of the data to be fetched</param>
        /// <param name="asOfDate">date  of the data to be fetched</param>
        /// <returns>List of NotificationModel</returns>
        public IEnumerable<NotificationModel> GetDayNotifications(int clientId, string customerId, string accountId, DateTime asOfDate)
        {
            var day = asOfDate.Date;
            var notificationEntityList = _notificationRepository.GetNotifications(clientId, customerId, accountId);

            // Filter the notifications so only the records falling on the date of "asOfDate" will be returned.
            var results = notificationEntityList.Result.Where(p => p.NotifiedDateTime.Date >= day && p.NotifiedDateTime < day.AddDays(1));

            return Mapper.Map<IEnumerable<NotificationEntity>, IEnumerable<NotificationModel>>(results); 
        }

        /// <summary>
        /// To insert or update latest NotificationModel to database
        /// </summary>
        /// <param name="notificationModel">model to be inserted</param>
        /// <param name="forceLatest">Forces the notification to be saved with IsLatest flag set to true</param>
        /// <returns>bool</returns>
        public void SaveNotification(NotificationModel notificationModel, bool forceLatest = false)
        {
            var entityModel = Mapper.Map<NotificationModel, NotificationEntity>(notificationModel);
            if (forceLatest) entityModel.IsLatest = true;
            _notificationRepository.InsertOrMergeNotification(entityModel);

            try {
                var entityReportModel = Mapper.Map<NotificationModel, NotificationReportEntity>(notificationModel);
                // insert report copy
                entityReportModel.IsLatest = true;
                _notificationRepository.InsertOrMergeNotificationReport(entityReportModel);
            }
            catch (Exception ex)
            {
                Logger.Error(ex, LogModel);
            }
        }

        /// <summary>
        /// Get notification for client within provided date range
        /// </summary>
        /// <param name="clientId">clientId of the data to be fetched</param>
        /// <param name="startDate">start date for date range</param>
        /// <param name="endDate">end date for date range</param>
        /// <returns>List of NotificationModel</returns>
        public IEnumerable<NotificationModel> GetNotifications(int clientId, DateTime startDate, DateTime endDate)
        {
            var notificationEntityList = _notificationRepository.GetNotifications(clientId, startDate, endDate).Result;
            return Mapper.Map<IEnumerable<NotificationEntity>, IEnumerable<NotificationModel>>(notificationEntityList);
        }

        /// <summary>
        /// Retrieves all scheduled notifications for all clients
        /// </summary>
        public IList<NotificationModel> GetScheduledNotifications()
        {
            var notificationList = _notificationRepository.GetNotifications().Result;

            //Filter notifications by eliminating RowKey with historical data.  Verify only scheduled and notified
            //records are in filtered list
            var filteredNotifications = new List<NotificationEntity>();
            using (
                var sequenceEnum =
                    notificationList.Where(
                        notification => notification.IsScheduled == true && notification.IsNotified == true)
                        .GetEnumerator())
            {
                while (sequenceEnum.MoveNext())
                {
                    if (sequenceEnum.Current.IsLatest && sequenceEnum.Current.Channel == "SMS")
                    {
                        filteredNotifications.Add(sequenceEnum.Current);
                    }
                }
            }

            return Mapper.Map<IList<NotificationEntity>, IList<NotificationModel>>(filteredNotifications);
        }
    }
}
