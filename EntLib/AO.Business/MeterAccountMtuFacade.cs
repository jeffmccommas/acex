﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AO.BusinessContracts;
using AutoMapper;
using CE.AO.Logging;
using CE.AO.Models;

namespace AO.Business
{
    public class MeterAccountMtuFacade : IMeterAccountMtuFacade
    {
        private readonly LogModel _logModel;
        private readonly IMeterAccountMtu _meterAccount;
        private readonly IMeterHistoryByMeter _meterHistoryByMeter;
        private readonly IMeterType _meterType;

        public MeterAccountMtuFacade(LogModel logModel, IMeterAccountMtu meterAccountMtu, IMeterType meterType, IMeterHistoryByMeter meterHistoryByMeter)
        {
            _logModel = logModel;
            _meterAccount = meterAccountMtu;
            _meterType = meterType;
            _meterHistoryByMeter = meterHistoryByMeter;
        }

        /// <summary>
        /// To insert in meter_account_history by fetching details from Metertypes
        /// </summary>
        /// <param name="model">AlarmModel to be inserted</param>
        /// <returns>true</returns>
        public async Task<bool> InsertOrMergeMeterAccountAsync(MeterAccountMtuModel model)
        {
            var alarmTypeInfo = _meterType.GetMeterType(model.MeterTypeId);

            if (alarmTypeInfo != null)
                model.MeterTypeDescripton = alarmTypeInfo.Descripton;
            var meterHistoryByMeterModel = Mapper.Map<MeterAccountMtuModel, MeterHistoryByMeterModel>(model);
            var tasks = new List<Task<bool>>
            {
                _meterAccount.InsertOrMergeMeterAccountMtuAsync(model),
                _meterHistoryByMeter.InsertOrMergeMeterHistoryByMeterAsync(meterHistoryByMeterModel)
            };
            if (alarmTypeInfo == null)
                Logger.Error($"No mapping found for MeterType {model.MeterTypeId}.", _logModel);

            bool[] taskResults = await Task.WhenAll(tasks);
			return !taskResults.Any(result => false);
        }
    }
}
