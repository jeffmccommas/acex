﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using AO.BusinessContracts;
using AO.DataAccess.Contracts;
using AO.Entities;
using AutoMapper;
using CE.AO.Logging;
using CE.AO.Models;
using CE.AO.Utilities;
using Microsoft.Practices.Unity;
using Microsoft.WindowsAzure.Storage.Table;

namespace AO.Business
{
    /// <summary>
    /// ClientAccount class is used to fetch/insert/update the clientaccount details from/to the database
    /// </summary>
    public class ClientAccount : IClientAccount
    {
        public ClientAccount(LogModel logModel, Lazy<ICassandraRepository> cassandraRepository)
        {
            LogModel = logModel;
            CassandraRepository = cassandraRepository;
        }

        public LogModel LogModel { get; set; }

        private Lazy<ICassandraRepository> CassandraRepository { get; }

        [Dependency]
        public Lazy<IQueueManager> QueueManager { get; set; }

        /// <summary>
        /// To insert or update ClientAccountModel to database
        /// </summary>
        /// <param name="model">model to be inserted</param>
        /// <returns>true</returns>
        public async Task<bool> InsertOrMergeClientAccount(ClientAccountModel model)
        {
            var entity = Mapper.Map<ClientAccountModel, ClientAccountEntity>(model);
            entity.LastModifiedDate = DateTime.UtcNow;
            await CassandraRepository.Value.InsertOrUpdateAsync(entity, Constants.CassandraTableNames.ClientAccount).ConfigureAwait(false);

            try
            {
                // report copy
                var entityReport = Mapper.Map<ClientAccountModel, ClientAccountReportEntity>(model);
                entityReport.LastModifiedDate = DateTime.UtcNow;
                entityReport.RowCreateDate = DateTime.UtcNow.ToString("yyyyMMdd");
                await CassandraRepository.Value.InsertOrUpdateAsync(entityReport, Constants.CassandraTableNames.ClientAccountReport).ConfigureAwait(false);

            }
            catch (Exception ex)
            {
                Logger.Error(ex, LogModel);
            }
            return await Task.Factory.StartNew(() => true);
        }

        /// <summary>
        /// To fetch the clientaccount details from the database
        /// </summary>
        /// <param name="clientId">clientId of the data to be fetched</param>
        /// <returns>List of ClientAccountModel</returns>
        public IEnumerable<ClientAccountModel> GetClientAccounts(int clientId)
        {
            IList<ClientAccountEntity> accountMeterDetails = CassandraRepository.Value.Get<ClientAccountEntity>(Constants.CassandraTableNames.ClientAccount,
                c => c.ClientId == clientId);
            return Mapper.Map<IEnumerable<ClientAccountEntity>, IEnumerable<ClientAccountModel>>(accountMeterDetails);
        }

        /// <summary>
        /// To schedule calculation add message to queue
        /// </summary>
        /// <param name="clientId">clientId to be send in queue</param>
        /// <param name="asOfDate">asOfDate to be send in queue</param>
        public void SendScheduleMessage(int clientId, string asOfDate)
        {
            byte[] token = null;
            do
            {
                var queryResult = CassandraRepository.Value.Get<ClientAccountEntity>(
                    Constants.CassandraTableNames.ClientAccount, 1, ref token, c => c.ClientId == clientId);
                foreach (var entity in queryResult)
                {
                    var message = $"{entity.AccountId}^^{entity.ClientId}^^{entity.CustomerId}^^{asOfDate}";
                    QueueManager.Value.AddQueueAsync(Constants.QueueNames.AoProcessScheduleCalculationQueue, message).Wait();
                }
            } while (token != null);
        }

        /// <summary>
        /// To fetch the data for client account
        /// </summary>
        /// <param name="clientId">Client id for which data to be fetched</param>
        /// <param name="maxEntitiesToFetch">Number of records to be fetched</param>
        /// <param name="token">Table continuation token</param>
        /// <returns>List of ClientAccountModel model</returns>
        public IEnumerable<ClientAccountModel> GetClientAccounts(int clientId, int maxEntitiesToFetch, ref TableContinuationToken token)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// To fetch the clientaccounts details from the database
        /// </summary>
        /// <param name="clientId">clientId of the data to be fetched</param>
        /// <param name="maxEntitiesToFetch">max number of entities to be fetched</param>
        /// <param name="token">token to be used</param>
        /// <returns>List of ClientAccountModel</returns>
        public IEnumerable<ClientAccountModel> GetClientAccounts(int clientId, int maxEntitiesToFetch, ref byte[] token)
        {
            IList<ClientAccountEntity> clientAccountEntityList = CassandraRepository.Value.Get<ClientAccountEntity>(
                    Constants.CassandraTableNames.ClientAccount, maxEntitiesToFetch, ref token, c => c.ClientId == clientId);
            return Mapper.Map<IEnumerable<ClientAccountEntity>, IEnumerable<ClientAccountModel>>(clientAccountEntityList);
        }

        /// <summary>
        /// Delete Client Account from DB
        /// </summary>
        /// <param name="clientId"></param>
        /// <param name="accountId"></param>
        /// <returns></returns>
        public async Task<bool> DeleteClientAccountAsyc(int clientId, string accountId)
        {
            await
                 CassandraRepository.Value.DeleteAsync<ClientAccountEntity>(Constants.CassandraTableNames.ClientAccount,
                     a => a.ClientId == clientId && a.AccountId == accountId).ConfigureAwait(false);
            return await Task.Factory.StartNew(() => true);
        }
    }
}
