﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using AO.BusinessContracts;
using AO.DataAccess.Contracts;
using AO.Entities;
using AutoMapper;
using CE.AO.Models;

namespace AO.Business
{
    /// <summary>
    /// Premise class is used to fetch/insert/update the premise details from/to the database
    /// </summary>
    public class Premise : IPremise
    {
        private readonly ICassandraRepository _cassandraRepository;

        public Premise(ICassandraRepository cassandraRepository)
        {
            _cassandraRepository = cassandraRepository;
        }

        /// <summary>
        /// To insert or update PremiseModel to database
        /// </summary>
        /// <param name="model">model to be inserted</param>
        /// <returns>true</returns>
        public async Task<bool> InsertOrMergePremiseAsync(PremiseModel model)
        {
            var entity = Mapper.Map<PremiseModel, PremiseEntity>(model);
            entity.LastModifiedDate = DateTime.UtcNow;
            await _cassandraRepository.InsertOrUpdateAsync(entity, Constants.CassandraTableNames.Premise).ConfigureAwait(false);
            return await Task.Factory.StartNew(() => true);
        }

        /// <summary>
        /// To fetch the all premise details from the database
        /// </summary>
        /// <param name="clientId">clientId of the data to be fetched</param>
        /// <param name="customerId">customerId of the data to be fetched</param>
        /// <returns>List of PremiseModel</returns>
        public IEnumerable<PremiseModel> GetPremises(int clientId, string customerId)
        {
            IList<PremiseEntity> entity = _cassandraRepository.Get<PremiseEntity>(Constants.CassandraTableNames.Premise,
                p => p.ClientId == clientId && p.CustomerId == customerId);
            return Mapper.Map<IEnumerable<PremiseEntity>, IEnumerable<PremiseModel>>(entity);
        }

        public async Task<bool> DeletePremisesAsync(int clientId, string customerId, string premiseId)
        {
            await
                   _cassandraRepository.DeleteAsync<PremiseEntity>(Constants.CassandraTableNames.Premise,
                       a => a.ClientId == clientId && a.CustomerId == customerId && a.PremiseId == premiseId).ConfigureAwait(false);
            return await Task.Factory.StartNew(() => true);
        }
    }
}
