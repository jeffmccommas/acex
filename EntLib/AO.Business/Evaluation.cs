﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using AO.BusinessContracts;
using AO.DataAccess.Contracts;
using AO.Entities;
using AutoMapper;
using CE.AO.Logging;
using CE.AO.Models;
using CE.RateModel;
using Enums = CE.AO.Utilities.Enums;

namespace AO.Business
{
    /// <summary>
    /// Evaluation class is used to fetch/insert/update the evaluation from/to the database
    /// </summary>
    public class Evaluation : IEvaluation
    {
        private readonly Lazy<ICassandraRepository> _cassandraRepository;
        private readonly Lazy<CE.AO.Utilities.IQueueManager> _queueManager; 
        private readonly LogModel _logModel;

        public Evaluation(LogModel logModel, Lazy<ICassandraRepository> cassandraRepository, Lazy<CE.AO.Utilities.IQueueManager> queueManager)
        {
            _logModel = logModel;
            _cassandraRepository = cassandraRepository;
            _queueManager = queueManager;
        }

        /// <summary>
        /// To evaluate insight, to save insight using subscription, calculation, ami, billing model
        /// </summary>
        /// <param name="subscriptions">List of SubscriptionModel model</param>
        /// <param name="calculations">List of CalculationModle model</param>
        /// <param name="clientSettings">Client settings</param>
        /// <param name="amiList">List of AMI</param>
        /// <param name="tierBoundaries">List of TierBoundary</param>
        /// <param name="asOfDate">Date for which evaluation should be done</param>
        /// <param name="billingmodel">BillingModel model</param>
        /// <returns></returns>
        public async Task EvaluateInsight(IEnumerable<SubscriptionModel> subscriptions,
                   IEnumerable<CalculationModel> calculations, ClientSettings clientSettings, List<TallAmiModel> amiList, List<TierBoundary> tierBoundaries, DateTime asOfDate, BillingModel billingmodel)
        {
            IList<SubscriptionModel> subscriptionModels = subscriptions as IList<SubscriptionModel> ?? subscriptions.ToList();
            MergeSettings(subscriptionModels, clientSettings, billingmodel);

            // loop through all insights
            IList<CalculationModel> calculationModels = calculations as IList<CalculationModel> ?? calculations.ToList();
            foreach (var program in clientSettings.Programs)
            {
                foreach (var calculation in calculationModels)
                {
                    if (clientSettings.MinimumDaysForInsightNotification != 0 &&
                            (calculation.BillDays < clientSettings.MinimumDaysForInsightNotification))
                        continue;
                    var insightModel = new InsightModel
                    {
                        ProgramName = program.ProgramName,
                        ServiceContractId = calculation.ServiceContractId,
                        AccountId = calculation.AccountId,
                        ClientId = calculation.ClientId,
                        CustomerId = calculation.CustomerId
                    };
                    var serviceAmi = amiList.FirstOrDefault(e => e.MeterId == calculation.MeterId);
                    int? amiUomId = null;
                    double? dailyConsumption = serviceAmi != null ? GetDailyConsumption(serviceAmi, out amiUomId) : null;
                    foreach (var insight in program.Insights)
                    {
                        string commodityType = calculation.CommodityId == null
                               ? string.Empty
                               : ((Enums.CommodityType)calculation.CommodityId).GetDescription();

                        if ((calculation.CommodityId != null && insight.CommodityType.Contains(commodityType)) || string.IsNullOrEmpty(calculation.ServiceContractId))
                        {
                            var customInsightModel = Extensions.GetCustomInsightModel(subscriptionModels.ToList(), insight, program, calculation.ServiceContractId, commodityType, billingmodel?.UOMId);
                            InsightEvaluation(insightModel, calculation, dailyConsumption, tierBoundaries, customInsightModel, amiUomId);
                        }
                    }
                    insightModel.AsOfEvaluationDate = asOfDate;
                    insightModel.BillCycleStartDate = calculation.BillCycleStartDate;
                    insightModel.BillCycleEndDate = calculation.BillCycleEndDate;
					insightModel.AsOfEvaluationId = Guid.NewGuid();

					await SaveInsight(insightModel, string.IsNullOrEmpty(calculation.ServiceContractId));

                    string message =
                        $"{calculation.ClientId}^^{calculation.CustomerId}^^{calculation.AccountId}^^{calculation.ServiceContractId}^^{program.ProgramName}^^{_logModel.Source}^^{_logModel.RowIndex}^^{_logModel.Metadata}^^{(int)_logModel.ProcessingType}^^{insightModel.AsOfEvaluationId}";
                    await _queueManager.Value.AddQueueAsync(Constants.QueueNames.AoNotificationQueue, message).ConfigureAwait(false);
                }
            }
        }

        private double? GetDailyConsumption(dynamic serviceAmi, out int? amiUomId)
        {
            double? dailyConsumption = 0;
            var properties = (PropertyInfo[])serviceAmi.GetType().GetProperties();
            //Get interval type. Needed in case of daily to get touid.
            var intervalType = Convert.ToInt32(serviceAmi.GetType().GetProperty("IntervalType").GetValue(serviceAmi));
            //In case of daily ami consumption will be sum of all TOU consumption
            if (intervalType == 24)
                properties.Where(p => p.Name.Contains("TOU_")).ToList().ForEach(
                        property =>
                        {
                            dailyConsumption = dailyConsumption +
                                               (property.GetValue(serviceAmi) != null
                                                   ? Convert.ToDouble(property.GetValue(serviceAmi))
                                                   : 0);
                        });
            else
                properties.Where(p => p.Name.Contains("IntValue")).ToList()
                    .ForEach(
                        property =>
                        {
                            dailyConsumption = dailyConsumption +
                                               (property.GetValue(serviceAmi) != null
                                                   ? Convert.ToDouble(property.GetValue(serviceAmi))
                                                   : 0);
                        });

            // get the uom id from ami
            var uomId = properties.FirstOrDefault(p => p.Name.Contains("UOMId"));
            amiUomId = uomId?.GetValue(serviceAmi) != null
                // ReSharper disable once ConstantConditionalAccessQualifier
                ? Convert.ToInt32(uomId?.GetValue(serviceAmi))
                : null;
            return dailyConsumption;
        }

        /// <summary>
        /// To populate InsightModel depending on the insight name
        /// </summary>
        /// <param name="insightModel">InsightModel model</param>
        /// <param name="calculation">CalculationModel model</param>
        /// <param name="dailyConsumption">Daily consumption</param>
        /// <param name="tierBoundaries">List of TierBoundary model</param>
        /// <param name="customInsightModel">CustomInsightModel model</param>
        /// <param name="amiUom"></param>
        private void InsightEvaluation(InsightModel insightModel, CalculationModel calculation, double? dailyConsumption, List<TierBoundary> tierBoundaries, CustomInsightModel customInsightModel, int? amiUom)
        {
            switch (customInsightModel.InsightName)
            {
                case "BillToDate":
                    if (customInsightModel.IsAccountLevel)
                        insightModel.BtdCost = calculation.Cost;
                    break;
                case "CostToDate":
                    if (!customInsightModel.IsAccountLevel)
                        insightModel.CtdCost = calculation.Cost;
                    break;
                case "AccountProjectedCost":
                    if (customInsightModel.IsAccountLevel)
                        insightModel.BtdProjectedCost = calculation.ProjectedCost;
                    break;
                case "ServiceProjectedCost":
                    if (!customInsightModel.IsAccountLevel)
                        insightModel.CtdProjectedCost = calculation.ProjectedCost;
                    break;
                case "Usage":
                    if (!customInsightModel.IsAccountLevel)
                        insightModel.Usage = calculation.Usage;
                    break;
                case "AccountLevelCostThreshold":
                    if (customInsightModel.IsAccountLevel)
                        insightModel.AccountLevelCostThresholdExceed = calculation.Cost > customInsightModel.ThresholdValue;
                    break;
                case "ServiceLevelUsageThreshold":
                    if (!customInsightModel.IsAccountLevel)
                    {
                        // if the uom of the usage (from ami) is different from bill
                        // convert the usage into bill's uom, then evaluate
                        double? usage;
                        if (amiUom.HasValue && customInsightModel.UomId.HasValue && amiUom != customInsightModel.UomId)
                        {
                            var conversionFactor = Extensions.GetConversionFactor(insightModel.ClientId, amiUom.Value, customInsightModel.UomId.Value);
                            usage = calculation.Usage * conversionFactor;
                        }
                        else
                        {
                            usage = calculation.Usage;
                        }

                        insightModel.ServiceLevelUsageThresholdExceed = usage > customInsightModel.ThresholdValue;
                    }
                    break;
                case "ServiceLevelCostThreshold":
                    if (!customInsightModel.IsAccountLevel)
                        insightModel.ServiceLevelCostThresholdExceed = calculation.Cost > customInsightModel.ThresholdValue;
                    break;
                case "DayThreshold":
                    if (!customInsightModel.IsAccountLevel)
                    {
                        if (dailyConsumption != null)
                        {
                            // if the uom of the usage (from ami) is different from bill
                            // convert the usage into bill's uom, then evaluate
                            double? consumption;
                            if (amiUom.HasValue && customInsightModel.UomId.HasValue && amiUom != customInsightModel.UomId)
                            {
                                var conversionFactor = Extensions.GetConversionFactor(insightModel.ClientId, amiUom.Value, customInsightModel.UomId.Value);
                                consumption = dailyConsumption * conversionFactor;
                            }
                            else
                            {
                                consumption = dailyConsumption;
                            }
                            insightModel.DayThresholdExceed = consumption > customInsightModel.ThresholdValue;
                        }
                        else
                            Logger.Warn("No AMI received to evaluate Day Threshold", _logModel);
                    }
                    break;
                case "ServiceLevelTieredThresholdApproaching":
                    if (!customInsightModel.IsAccountLevel)
                    {
                        if (tierBoundaries != null && tierBoundaries.Count > 0)
                        {
                            TierBoundary tierBoundary;
                            var tierApproaching = GetServiceLevelTieredThreshold(tierBoundaries, calculation, true, out tierBoundary, customInsightModel.ThresholdValue);
                            switch (tierBoundary?.BaseOrTier)
                            {
                                case CE.RateModel.Enums.BaseOrTier.Tier1:
                                    insightModel.ServiceLevelTiered1ThresholdApproaching = tierApproaching;
                                    break;
                                case CE.RateModel.Enums.BaseOrTier.Tier2:
                                    insightModel.ServiceLevelTiered2ThresholdApproaching = tierApproaching;
                                    break;
                                case CE.RateModel.Enums.BaseOrTier.Tier3:
                                    insightModel.ServiceLevelTiered3ThresholdApproaching = tierApproaching;
                                    break;
                                case CE.RateModel.Enums.BaseOrTier.Tier4:
                                    insightModel.ServiceLevelTiered4ThresholdApproaching = tierApproaching;
                                    break;
                                case CE.RateModel.Enums.BaseOrTier.Tier5:
                                    insightModel.ServiceLevelTiered5ThresholdApproaching = tierApproaching;
                                    break;
                                case CE.RateModel.Enums.BaseOrTier.Tier6:
                                    insightModel.ServiceLevelTiered6ThresholdApproaching = tierApproaching;
                                    break;
                            }
                        }
                        else
                            Logger.Warn("No Tiered boundaries found to evaluate Service Level Tiered Threshold Approaching", _logModel);
                    }
                    break;
                case "ServiceLevelTieredThresholdExceed":
                    if (!customInsightModel.IsAccountLevel)
                    {
                        if (tierBoundaries != null && tierBoundaries.Count > 0)
                        {

                            TierBoundary tierBoundary;
                            insightModel.ServiceLevelTieredThresholdExceed = GetServiceLevelTieredThreshold(tierBoundaries, calculation, false, out tierBoundary, 0);
                        }
                        else
                            Logger.Warn("No Tiered boundaries found to evaluate Service Level Tiered Threshold Exceed", _logModel);
                    }
                    break;
            }
        }

        /// <summary>
        /// To merge the settings in SubscriptionModel from ClientSettings, BillingModel
        /// </summary>
        /// <param name="subscriptionModels">SubscriptionModel model</param>
        /// <param name="clientSettings">Client settings</param>
        /// <param name="billingModel">BillingModel model</param>
        private void MergeSettings(IList<SubscriptionModel> subscriptionModels, ClientSettings clientSettings, BillingModel billingModel)
        {
            foreach (var program in clientSettings.Programs)
            {
                List<SubscriptionModel> listOfInsightsSubscribed =
                    subscriptionModels.Where(e => e.ProgramName == program.UtilityProgramName).ToList();

                if (listOfInsightsSubscribed.Count <= 0) continue;

                foreach (var insight in listOfInsightsSubscribed.Where(insight => !string.IsNullOrEmpty(insight.InsightTypeName)))
                {
                    InsightSettings ins;
                    if (billingModel == null)
                    {
                        ins = program.Insights.FirstOrDefault(
                            e => e.UtilityInsightName == insight.InsightTypeName);
                    }
                    else
                    {
                        ins =
                            program.Insights.FirstOrDefault(
                                e =>
                                    e.UtilityInsightName == insight.InsightTypeName &&
                                    e.CommodityType.Contains(
                                        CE.AO.Logging.Enums.GetDescription(((Enums.CommodityType) billingModel.CommodityId))));
                    }
                    if (ins == null) continue;
                    ins.DefaultCommunicationChannel = !string.IsNullOrEmpty(insight.Channel)
                        ? ins.AllowedCommunicationChannel.IndexOf(insight.Channel, 0, StringComparison.OrdinalIgnoreCase) >
                          -1
                            ? insight.Channel
                            : null
                        : ins.DefaultCommunicationChannel;
                }
            }
        }

        /// <summary>
        /// To save the Insight into the databse
        /// </summary>
        /// <param name="insightModel">InsightModel model to be saved</param>
        /// <param name="isAccountLevel">Is account level model or not</param>
        /// <returns>Void</returns>
        private async Task SaveInsight(InsightModel insightModel, bool isAccountLevel)
        {
            var entityModel = Mapper.Map<InsightModel, InsightEntity>(insightModel);
            if (isAccountLevel)
            {
                entityModel.ServiceContractId = "";
                entityModel.Type = "A";
                var dbInsightEntity =
                    _cassandraRepository.Value.GetSingleAsync<InsightEntity>(Constants.CassandraTableNames.Insight,
                        a =>
                            a.ClientId == entityModel.ClientId && a.AccountId == entityModel.AccountId &&
                            a.CustomerId == entityModel.CustomerId && a.ProgramName == entityModel.ProgramName &&
                            // ReSharper disable once RedundantBoolCompare we need to explicitly specify IsLatest == true, otherwise it gives error. TO execute cassandra query required to provide all values.
                            a.Type == "A" && a.IsLatest == true);
                if (dbInsightEntity != null)
                {
                    await _cassandraRepository.Value.DeleteAsync<InsightEntity>(Constants.CassandraTableNames.Insight,
                        a =>
                            a.ClientId == entityModel.ClientId && a.AccountId == entityModel.AccountId &&
                            a.CustomerId == entityModel.CustomerId &&
                            // ReSharper disable once RedundantBoolCompare we need to explicitly specify IsLatest == true, otherwise it gives error. TO execute cassandra query required to provide all values.
                            a.Type == "A" && a.IsLatest == true && a.ProgramName == entityModel.ProgramName,'A');
                }
            }
            else
            {
                entityModel.Type = "S";
                var dbInsightEntity =
                    _cassandraRepository.Value.GetSingleAsync<InsightEntity>(Constants.CassandraTableNames.Insight,
                        a =>
                            a.ClientId == entityModel.ClientId && a.AccountId == entityModel.AccountId &&
                            a.CustomerId == entityModel.CustomerId && a.ProgramName == entityModel.ProgramName &&
                            // ReSharper disable once RedundantBoolCompare we need to explicitly specify IsLatest == true, otherwise it gives error. TO execute cassandra query required to provide all values.
                            a.Type == "S" && a.IsLatest == true &&
                            a.ServiceContractId == entityModel.ServiceContractId).Result;
                if (dbInsightEntity != null)
                {
                    await _cassandraRepository.Value.DeleteAsync<InsightEntity>(Constants.CassandraTableNames.Insight,
                        a =>
                            a.ClientId == entityModel.ClientId && a.AccountId == entityModel.AccountId &&
                            a.CustomerId == entityModel.CustomerId &&
                            // ReSharper disable once RedundantBoolCompare we need to explicitly specify IsLatest == true, otherwise it gives error. TO execute cassandra query required to provide all values.
                            a.Type == "S" && a.IsLatest == true
                            && a.ServiceContractId == entityModel.ServiceContractId && a.ProgramName == entityModel.ProgramName,'A').ConfigureAwait(false);
                }
            }

            //insert History Record
            entityModel.IsLatest = false;
            await _cassandraRepository.Value.InsertOrUpdateAsync(entityModel, Constants.CassandraTableNames.Insight, 'A');

            entityModel.IsLatest = true;
            await _cassandraRepository.Value.InsertOrUpdateAsync(entityModel, Constants.CassandraTableNames.Insight,'A');
            
        }

        /// <summary>
        /// To get service level tiered threshold
        /// </summary>
        /// <param name="tierBoundaries">TierBoundary model</param>
        /// <param name="calculation">CalculationModel model</param>
        /// <param name="isApproaching">Is tier approaching</param>
        /// <param name="tieredBountryItem"></param>
        /// <param name="defaultValue">Default value</param>
        /// <returns>Boolean value</returns>
        private bool GetServiceLevelTieredThreshold(List<TierBoundary> tierBoundaries, CalculationModel calculation, bool isApproaching, out TierBoundary tieredBountryItem, double? defaultValue)
        {
            tieredBountryItem = isApproaching ? tierBoundaries.OrderBy(p => p.Threshold).FirstOrDefault(p => p.Threshold >= calculation.Usage) : tierBoundaries.OrderByDescending(p => p.Threshold).FirstOrDefault(p => p.Threshold < calculation.Usage);

            if (tieredBountryItem == null) return false;
            if (isApproaching)
            {
                if (tieredBountryItem.Threshold <= tieredBountryItem.Threshold * (defaultValue / 100) + calculation.Usage)
                    return true;
            }
            else if (tieredBountryItem.Threshold < calculation.Usage)
                return true;

            return false;
        }

        /// <summary>
        /// To fetch service level evaluation
        /// </summary>
        /// <param name="clientId">Client id for service level evaluation to be fetched</param>
        /// <param name="customerId">customer id for service level evaluation to be fetched</param>
        /// <param name="accountId">Account id for service level evaluation to be fetched</param>
        /// <param name="programName">Program name for service level evaluation to be fetched</param>
        /// <param name="serviceContractId">Service contract id for service level evaluation to be fetched</param>
        /// <returns>InsightModel model</returns>
        public async Task<InsightModel> GetServiceLevelEvaluationsAsync(int clientId, string customerId, string accountId, string programName, string serviceContractId)
        {
            var serviceLevelInsights =
                await _cassandraRepository.Value.GetSingleAsync<InsightEntity>(Constants.CassandraTableNames.Insight, 'A', a => a.ClientId == clientId && a.CustomerId == customerId && a.AccountId == accountId && a.ProgramName == programName && a.ServiceContractId == serviceContractId
                // ReSharper disable once RedundantBoolCompare we need to explicitly specify IsLatest == true, otherwise it gives error. TO execute cassandra query required to provide all values.
                && a.IsLatest == true && a.Type == "S");
            return Mapper.Map<InsightEntity, InsightModel>(serviceLevelInsights);
        }

        /// <summary>
        /// To fetch account level evaluation
        /// </summary>
        /// <param name="clientId">Client id for account level evaluation to be fetched</param>
        /// <param name="customerId">customer id for account level evaluation to be fetched</param>
        /// <param name="accountId">Account id for account level evaluation to be fetched</param>
        /// <param name="programName">Program name for account level evaluation to be fetched</param>
        /// <returns>InsightModel model</returns>
        public async Task<InsightModel> GetAccountLevelEvaluationsAsync(int clientId, string customerId, string accountId, string programName)
        {
            var accountLevelInsights =
                // ReSharper disable once RedundantBoolCompare we need to explicitly specify IsLatest == true, otherwise it gives error. TO execute cassandra query required to provide all values.
                await _cassandraRepository.Value.GetSingleAsync<InsightEntity>(Constants.CassandraTableNames.Insight, 'A', a => a.ClientId == clientId && a.CustomerId == customerId && a.AccountId == accountId && a.ProgramName == programName && a.IsLatest == true && a.Type == "A");
            return Mapper.Map<InsightEntity, InsightModel>(accountLevelInsights);
        }

        /// <summary>
        /// To fetch list of all InsightModel model in a day
        /// </summary>
        /// <param name="clientId">Client id for which list to be fetched</param>
        /// <param name="customerId">customer id for which list to be fetched</param>
        /// <param name="accountId">Account id for which list to be fetched</param>
        /// <param name="asOfDate">Date for which list to be fetched</param>
        /// <returns>List of InsightModel model</returns>
        public IEnumerable<InsightModel> GetDayInsights(int clientId, string customerId, string accountId, DateTime asOfDate)
        {
            IList<InsightEntity> insightEntityList = _cassandraRepository.Value.GetAsync<InsightEntity>(Constants.CassandraTableNames.Insight, a => a.ClientId == clientId && a.CustomerId == customerId && a.AccountId == accountId && a.AsOfEvaluationDate == asOfDate).Result;
            IEnumerable<InsightModel> insightModelList = Mapper.Map<IEnumerable<InsightEntity>, IEnumerable<InsightModel>>(insightEntityList);
            return insightModelList;
        }
    }
}
