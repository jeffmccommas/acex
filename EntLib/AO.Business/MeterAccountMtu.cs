﻿using System;
using System.Threading.Tasks;
using AO.BusinessContracts;
using AO.DataAccess.Contracts;
using AO.Entities;
using AutoMapper;
using CE.AO.Models;

namespace AO.Business
{
    /// <summary>
    /// MeterAccountMtu class is used to insert/update the MeterAccountMtuModel to the database
    /// </summary>
    public class MeterAccountMtu : IMeterAccountMtu
    {
        public MeterAccountMtu(LogModel logModel, Lazy<ICassandraRepository> cassandraRepository, Lazy<IMtuTypeMapping> mtuTypeMapping)
        {
            LogModel = logModel;
            CassandraRepository = cassandraRepository;
            MtuTypeMapping = mtuTypeMapping;
        }

        public LogModel LogModel { get; set; }

        private Lazy<ICassandraRepository> CassandraRepository { get; }

        private Lazy<IMtuTypeMapping> MtuTypeMapping { get; } 

        /// <summary>
        /// To insert/update the MeterAccountMtuModel to the database
        /// </summary>
        /// <param name="model">MeterAccountMtuModel which is to be inserted/updated</param>
        /// <returns>True</returns>
        public async Task<bool> InsertOrMergeMeterAccountMtuAsync(MeterAccountMtuModel model)
        {
            var mappingInfo = MtuTypeMapping.Value.GetMtuTypeMapping(model.MtuTypeId);

            if (mappingInfo != null)
                model = Mapper.Map(mappingInfo, model);

            var entity = Mapper.Map<MeterAccountMtuModel, MeterAccountHistoryEntity>(model);
            await CassandraRepository.Value.InsertOrUpdateAsync(entity, Constants.CassandraTableNames.MeterHistoryByAccount).ConfigureAwait(false);
            return await Task.Factory.StartNew(() => true);
        }

        /// <summary>
        /// To delete the MeterAccountMtuModel from the database
        /// </summary>
        /// <param name="model">MeterAccountMtuModel which is to be deleted</param>
        /// <returns>True</returns>
        public async Task<bool> DeleteMeterAccountMtuAsync(MeterAccountMtuModel model)
        {
            await
                CassandraRepository.Value.DeleteAsync<MeterAccountHistoryEntity>(
                    Constants.CassandraTableNames.MeterHistoryByAccount,
                    p =>
                        p.AccountNumber == model.AccountNumber &&
                        p.ClientId == model.ClientId &&
                        p.MeterInstallDate == model.MeterInstallDate).ConfigureAwait(false);
            return await Task.Factory.StartNew(() => true);
        }
    }
}