﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AO.BusinessContracts;
using AO.DataAccess.Contracts;
using AO.Entities;
using AutoMapper;
using CE.AO.Logging;
using CE.AO.Models;
using Enums = CE.AO.Utilities.Enums;

namespace AO.Business
{
    /// <summary>
    /// Subscription class is used to fetch/insert/update the subscription details from/to the database
    /// </summary>
    public class Subscription : ISubscription
    {
        private readonly Lazy<IClientConfigFacade> _clientConfigFacade;
        private readonly ICassandraRepository _cassandraRepository;
        public LogModel LogModel { get; set; }
        
        public Subscription(LogModel logModel, ICassandraRepository cassandraRepository, Lazy<IClientConfigFacade> clientConfigFacade)
        {
            LogModel = logModel;
            _cassandraRepository = cassandraRepository;
            _clientConfigFacade = clientConfigFacade;
        }

        /// <summary>
        ///  To insert or update SubscriptionModel to database
        /// </summary>
        /// <param name="model">model to be inserted</param>
        /// <param name="addEmailConfirmationCount">Used to set EmailConfirmationCount</param>
        /// <param name="addSmsConfirmationCount">Used to set SmsConfirmationCount</param>
        /// <returns>bool</returns>
        public async Task<bool> InsertSubscriptionAsync(SubscriptionModel model, bool addEmailConfirmationCount, bool addSmsConfirmationCount)
        {
            // var tasks = new List<Task<bool>>()
            var entity = Mapper.Map<SubscriptionModel, SubscriptionEntity>(model);

            var subscriptionData = await GetSubscriptionDetailsAsync(entity.ClientId, entity.CustomerId, entity.AccountId, entity.ProgramName, entity.ServiceContractId, entity.InsightTypeName);

            if (subscriptionData != null)
            {
                if (subscriptionData.IsSelected && entity.IsSelected == false)
                {
                    entity.OptOutDate = DateTime.UtcNow;
                    entity.SubscriptionDate = subscriptionData.SubscriptionDate;
                    entity.EmailConfirmationCount = subscriptionData.EmailConfirmationCount;
                    entity.SmsConfirmationCount = subscriptionData.SmsConfirmationCount;
                }
                else if (subscriptionData.IsSelected == false && entity.IsSelected)
                {
                    entity.OptOutDate = null;
                    entity.SubscriptionDate = DateTime.UtcNow;
                    entity.EmailConfirmationCount = null;
                    entity.SmsConfirmationCount = null;
                }
                else
                {
                    entity.OptOutDate = subscriptionData.OptOutDate;
                    entity.SubscriptionDate = subscriptionData.SubscriptionDate;
                    entity.EmailConfirmationCount = subscriptionData.EmailConfirmationCount;
                    entity.SmsConfirmationCount = subscriptionData.SmsConfirmationCount;
                }

                if ((subscriptionData.IsEmailOptInCompleted == false || subscriptionData.IsEmailOptInCompleted == null) && entity.IsEmailOptInCompleted == true)
                    entity.EmailOptInCompletedDate = DateTime.UtcNow;
                else if (entity.IsEmailOptInCompleted == false && subscriptionData.IsEmailOptInCompleted == true)
                    entity.EmailOptInCompletedDate = null;
                else
                {
                    entity.EmailOptInCompletedDate = subscriptionData.EmailOptInCompletedDate;
                    entity.IsEmailOptInCompleted = subscriptionData.IsEmailOptInCompleted;
                }

                if ((subscriptionData.IsSmsOptInCompleted == false || subscriptionData.IsSmsOptInCompleted == null) &&
                         entity.IsSmsOptInCompleted == true)
                    entity.SmsOptInCompletedDate = DateTime.UtcNow;
                else if (entity.IsSmsOptInCompleted == false && subscriptionData.IsSmsOptInCompleted == true)
                    entity.SmsOptInCompletedDate = null;
                else
                {
                    entity.SmsOptInCompletedDate = subscriptionData.SmsOptInCompletedDate;
                    entity.IsSmsOptInCompleted = subscriptionData.IsSmsOptInCompleted;
                }

                if (addEmailConfirmationCount)
                {
                    if (subscriptionData.EmailConfirmationCount == null ||
                                      subscriptionData.EmailConfirmationCount <= 0)
                        entity.EmailConfirmationCount = 1;
                    else
                        entity.EmailConfirmationCount += 1;
                }

                if (addSmsConfirmationCount)
                {
                    if (subscriptionData.SmsConfirmationCount == null ||
                                      subscriptionData.SmsConfirmationCount <= 0)
                        entity.SmsConfirmationCount = 1;
                    else
                        entity.SmsConfirmationCount += 1;
                }

                await DeleteLatestSubscriptionDetailsAsync(subscriptionData);
                await DeleteHistorySubscriptionDetailsAsync(subscriptionData);
            }
            else
            {
                entity.SubscriptionDate = DateTime.UtcNow;
                if (entity.IsSmsOptInCompleted == true)
                    entity.SmsOptInCompletedDate = DateTime.UtcNow;

                if (entity.IsEmailOptInCompleted == true)
                    entity.EmailOptInCompletedDate = DateTime.UtcNow;

                if (!entity.IsSelected)
                {
                    entity.OptOutDate = DateTime.UtcNow;
                }
            }

            await InsertOrMergeLatestSubscriptionAsync(entity);
            await InsertOrMergeHistorySubscriptionAsync(entity);
            return true;


            //tasks.Add(InsertOrMergeLatestSubscriptionAsync(entity))
            //tasks.Add(InsertOrMergeHistorySubscriptionAsync(entity))

            //bool[] taskResults = await Task.WhenAll(tasks)
            //return !taskResults.Any(result => false)
        }

        /// <summary>
        /// Map SubscriptionModel to SubscriptionEntity. If subscription already present then delete it and then insert new subscription.
        /// </summary>
        /// <param name="model">model to be inserted</param>
        /// <returns>true</returns>
        public bool InsertSubscription(SubscriptionModel model)
        {
            var entity = Mapper.Map<SubscriptionModel, SubscriptionEntity>(model);

            var subscriptionData = GetSubscriptionDetails(entity.ClientId, entity.CustomerId, entity.AccountId, entity.ProgramName, entity.ServiceContractId, entity.InsightTypeName);

            if (subscriptionData != null)
            {
                if (subscriptionData.IsSelected && entity.IsSelected == false)
                {
                    entity.OptOutDate = DateTime.UtcNow;
                    entity.SubscriptionDate = subscriptionData.SubscriptionDate;
                    entity.EmailConfirmationCount = subscriptionData.EmailConfirmationCount;
                    entity.SmsConfirmationCount = subscriptionData.SmsConfirmationCount;
                }
                else if (subscriptionData.IsSelected == false && entity.IsSelected)
                {
                    entity.OptOutDate = null;
                    entity.SubscriptionDate = DateTime.UtcNow;
                    entity.EmailConfirmationCount = null;
                    entity.SmsConfirmationCount = null;
                }
                else
                {
                    entity.OptOutDate = subscriptionData.OptOutDate;
                    entity.SubscriptionDate = subscriptionData.SubscriptionDate;
                    entity.EmailConfirmationCount = subscriptionData.EmailConfirmationCount;
                    entity.SmsConfirmationCount = subscriptionData.SmsConfirmationCount;
                }

                if ((subscriptionData.IsEmailOptInCompleted == false || subscriptionData.IsEmailOptInCompleted == null) && entity.IsEmailOptInCompleted == true)
                    entity.EmailOptInCompletedDate = DateTime.UtcNow;
                else if (entity.IsEmailOptInCompleted == false && subscriptionData.IsEmailOptInCompleted == true)
                    entity.EmailOptInCompletedDate = null;
                else
                {
                    entity.EmailOptInCompletedDate = subscriptionData.EmailOptInCompletedDate;
                    entity.IsEmailOptInCompleted = subscriptionData.IsEmailOptInCompleted;
                }

                if ((subscriptionData.IsSmsOptInCompleted == false || subscriptionData.IsSmsOptInCompleted == null) &&
                         entity.IsSmsOptInCompleted == true)
                    entity.SmsOptInCompletedDate = DateTime.UtcNow;
                else if (entity.IsSmsOptInCompleted == false && subscriptionData.IsSmsOptInCompleted == true)
                    entity.SmsOptInCompletedDate = null;
                else
                {
                    entity.SmsOptInCompletedDate = subscriptionData.SmsOptInCompletedDate;
                    entity.IsSmsOptInCompleted = subscriptionData.IsSmsOptInCompleted;
                }

                DeleteLatestSubscriptionDetails(subscriptionData);
                DeleteHistorySubscriptionDetails(subscriptionData);
            }
            else
            {
                entity.SubscriptionDate = DateTime.UtcNow;
                if (entity.IsSmsOptInCompleted == true)
                    entity.SmsOptInCompletedDate = DateTime.UtcNow;

                if (entity.IsEmailOptInCompleted == true)
                    entity.EmailOptInCompletedDate = DateTime.UtcNow;

                if (!entity.IsSelected)
                {
                    entity.OptOutDate = DateTime.UtcNow;
                }
            }

            InsertOrMergeLatestSubscription(entity);
            InsertOrMergeHistorySubscription(entity);
            return true;
        }

        /// <summary>
        /// To insert or update latest SubscriptionModel to database
        /// </summary>
        /// <param name="entity">entity to be inserted</param>
        /// <returns>true</returns>
        private void InsertOrMergeLatestSubscription(SubscriptionEntity entity)
        {
            entity.Type = string.IsNullOrWhiteSpace(entity.InsightTypeName) ? "A" : "S";
            entity.IsLatest = true;
            entity.LastModifiedDate = DateTime.UtcNow;
            _cassandraRepository.InsertOrUpdate(entity, Constants.CassandraTableNames.Subscription);

            try {
                // report copy
                var entityReport = Mapper.Map<SubscriptionEntity, SubscriptionReportEntity>(entity);
                entityReport.LastModifiedDate = DateTime.UtcNow;
                entityReport.RowCreateDate = DateTime.UtcNow.ToString("yyyyMMdd");
                _cassandraRepository.InsertOrUpdate(entityReport, Constants.CassandraTableNames.SubscriptionReport);
            }
            catch (Exception ex)
            {
                Logger.Error(ex, LogModel);
            }
}

        /// <summary>
        /// To insert or update latest SubscriptionModel to database
        /// </summary>
        /// <param name="entity">entity to be inserted</param>
        /// <returns>true</returns>
        private async Task<bool> InsertOrMergeLatestSubscriptionAsync(SubscriptionEntity entity)
        {
            entity.Type = string.IsNullOrWhiteSpace(entity.InsightTypeName) ? "A" : "S";
            entity.IsLatest = true;
            entity.LastModifiedDate = DateTime.UtcNow;
            //await _cassandraRepository.InsertOrUpdateAsync(entity, Constants.CassandraTableNames.Subscription).ConfigureAwait(false)
            await _cassandraRepository.InsertOrUpdateAsync(entity, Constants.CassandraTableNames.Subscription,'A');

            try {
                // report copy
                var entityReport = Mapper.Map<SubscriptionEntity, SubscriptionReportEntity>(entity);
                entityReport.LastModifiedDate = DateTime.UtcNow;
                entityReport.RowCreateDate = DateTime.UtcNow.ToString("yyyyMMdd");
                await _cassandraRepository.InsertOrUpdateAsync(entityReport, Constants.CassandraTableNames.SubscriptionReport, 'A');
            }
            catch (Exception ex)
            {
                Logger.Error(ex, LogModel);
            }
            return await Task.Factory.StartNew(() => true);
        }

        /// <summary>
        /// To insert or update history SubscriptionModel to database
        /// </summary>
        /// <param name="entity">entity to be inserted</param>
        /// <returns>true</returns>
        private async Task<bool> InsertOrMergeHistorySubscriptionAsync(SubscriptionEntity entity)
        {
            entity.IsLatest = false;
            entity.Type = string.IsNullOrWhiteSpace(entity.InsightTypeName) ? "A" : "S";
            entity.LastModifiedDate = DateTime.UtcNow;
            //await _cassandraRepository.InsertOrUpdateAsync(entity, Constants.CassandraTableNames.Subscription).ConfigureAwait(false) 
            await _cassandraRepository.InsertOrUpdateAsync(entity, Constants.CassandraTableNames.Subscription, 'A');
            return await Task.Factory.StartNew(() => true);
        }

        /// <summary>
        /// To insert or update history SubscriptionModel to database
        /// </summary>
        /// <param name="entity">entity to be inserted</param>
        /// <returns>true</returns>
        private void InsertOrMergeHistorySubscription(SubscriptionEntity entity)
        {
            entity.IsLatest = false;
            entity.Type = string.IsNullOrWhiteSpace(entity.InsightTypeName) ? "A" : "S";
            entity.LastModifiedDate = DateTime.UtcNow;
            _cassandraRepository.InsertOrUpdate(entity, Constants.CassandraTableNames.Subscription);
        }

        /// <summary>
        /// To fetch the latest subscription from the database
        /// </summary>
        /// <param name="clientId">clientId of the data to be fetched</param>
        /// <param name="customerId">customerId of the data to be fetched</param>
        /// <param name="accountId">accountId of the data to be fetched</param>
        /// <param name="programName">programName of the data to be fetched</param>
        /// <param name="serviceContractId">serviceContractId of the data to be fetched</param>
        /// <param name="insightTypeName">insightTypeName of the data to be fetched</param>
        /// <returns>SubscriptionEntity</returns>
        public async Task<SubscriptionModel> GetSubscriptionDetailsAsync(int clientId, string customerId, string accountId, string programName, string serviceContractId, string insightTypeName)
        {
            var type = string.IsNullOrWhiteSpace(insightTypeName) ? "A" : "S";
            var subscriptionDetails =
                await _cassandraRepository.GetSingleAsync<SubscriptionEntity>(Constants.CassandraTableNames.Subscription, s => s.ClientId == clientId && s.CustomerId == customerId && s.AccountId == accountId && s.Type == type &&
                // ReSharper disable once RedundantBoolCompare we need to explicitly specify IsLatest == true, otherwise it gives error. To execute cassandra query required to provide all values.
                s.ProgramName == programName && s.InsightTypeName == insightTypeName && s.ServiceContractId == serviceContractId && s.IsLatest == true);
            return Mapper.Map<SubscriptionEntity, SubscriptionModel>(subscriptionDetails);
        }

        /// <summary>
        /// To fetch the latest subscription from the database
        /// </summary>
        /// <param name="clientId">clientId of the data to be fetched</param>
        /// <param name="customerId">customerId of the data to be fetched</param>
        /// <param name="accountId">accountId of the data to be fetched</param>
        /// <param name="programName">programName of the data to be fetched</param>
        /// <param name="serviceContractId">serviceContractId of the data to be fetched</param>
        /// <param name="insightTypeName">insightTypeName of the data to be fetched</param>
        /// <returns>SubscriptionEntity</returns>
        public SubscriptionModel GetSubscriptionDetails(int clientId, string customerId, string accountId, string programName, string serviceContractId, string insightTypeName)
        {
            var type = string.IsNullOrWhiteSpace(insightTypeName) ? "A" : "S";
            var subscriptionDetails =
                _cassandraRepository.GetSingle<SubscriptionEntity>(Constants.CassandraTableNames.Subscription, s => s.ClientId == clientId && s.CustomerId == customerId && s.AccountId == accountId && s.Type == type &&
                // ReSharper disable once RedundantBoolCompare we need to explicitly specify IsLatest == true, otherwise it gives error. To execute cassandra query required to provide all values.
                s.ProgramName == programName && s.InsightTypeName == insightTypeName && s.ServiceContractId == serviceContractId && s.IsLatest == true);
            return Mapper.Map<SubscriptionEntity, SubscriptionModel>(subscriptionDetails);
        }

        /// <summary>
        /// To delete latest SubscriptionEntity from database
        /// </summary>
        /// <param name="model">entity to be inserted</param>
        /// <returns>true</returns>
        private async Task<bool> DeleteLatestSubscriptionDetailsAsync(SubscriptionModel model)
        {
            var type = string.IsNullOrWhiteSpace(model.InsightTypeName) ? "A" : "S";
            await
                _cassandraRepository.DeleteAsync<SubscriptionEntity>(Constants.CassandraTableNames.Subscription,
                    s =>
                        s.ClientId == model.ClientId && s.CustomerId == model.CustomerId &&
                        s.AccountId == model.AccountId && s.Type == type &&
                        // ReSharper disable once RedundantBoolCompare we need to explicitly specify IsLatest == true, otherwise it gives error. To execute cassandra query required to provide all values.
                        s.ProgramName == model.ProgramName && s.InsightTypeName == model.InsightTypeName && s.ServiceContractId == model.ServiceContractId && s.IsLatest == true, 'A');
            return await Task.Factory.StartNew(() => true);
        }

        /// <summary>
        /// To delete latest SubscriptionEntity from database
        /// </summary>
        /// <param name="model">entity to be inserted</param>
        private void DeleteLatestSubscriptionDetails(SubscriptionModel model)
        {
            var type = string.IsNullOrWhiteSpace(model.InsightTypeName) ? "A" : "S";
            _cassandraRepository.Delete<SubscriptionEntity>(Constants.CassandraTableNames.Subscription, s => s.ClientId == model.ClientId && s.CustomerId == model.CustomerId && s.AccountId == model.AccountId &&
                 // ReSharper disable once RedundantBoolCompare we need to explicitly specify IsLatest == true, otherwise it gives error. To execute cassandra query required to provide all values.
                 s.Type == type && s.ProgramName == model.ProgramName && s.InsightTypeName == model.InsightTypeName && s.ServiceContractId == model.ServiceContractId && s.IsLatest == true,'A');
        }

        /// <summary>
        /// To delete history SubscriptionEntity from database
        /// </summary>
        /// <param name="model">entity to be inserted</param>
        /// <returns>true</returns>
        private async Task<bool> DeleteHistorySubscriptionDetailsAsync(SubscriptionModel model)
        {
            var type = string.IsNullOrWhiteSpace(model.InsightTypeName) ? "A" : "S";
            await _cassandraRepository.DeleteAsync<SubscriptionEntity>(Constants.CassandraTableNames.Subscription, s => s.ClientId == model.ClientId && s.CustomerId == model.CustomerId && s.AccountId == model.AccountId &&
                 s.Type == type && s.ProgramName == model.ProgramName && s.InsightTypeName == model.InsightTypeName && s.ServiceContractId == model.ServiceContractId && s.IsLatest == false, 'A');
            return await Task.Factory.StartNew(() => true);
        }

        /// <summary>
        /// To delete history SubscriptionEntity from database
        /// </summary>
        /// <param name="model">entity to be inserted</param>
        /// <returns>true</returns>
        private void DeleteHistorySubscriptionDetails(SubscriptionModel model)
        {
            var type = string.IsNullOrWhiteSpace(model.InsightTypeName) ? "A" : "S";
            _cassandraRepository.Delete<SubscriptionEntity>(Constants.CassandraTableNames.Subscription, s => s.ClientId == model.ClientId && s.CustomerId == model.CustomerId && s.AccountId == model.AccountId &&
                 s.Type == type && s.ProgramName == model.ProgramName && s.InsightTypeName == model.InsightTypeName && s.ServiceContractId == model.ServiceContractId && s.IsLatest == false,'A');
        }

        /// <summary>
        /// To fetch the subscription for the customer from the database
        /// </summary>
        /// <param name="clientId">clientId of the data to be fetched</param>
        /// <param name="customerId">customerId of the data to be fetched</param>
        /// <param name="accountId">accountId of the data to be fetched</param>
        /// <returns>List of SubscriptionModel</returns>
        public IEnumerable<SubscriptionModel> GetCustomerSubscriptions(int clientId, string customerId, string accountId)
        {
            //Get latest subscription
            List<SubscriptionEntity> subscriptions = _cassandraRepository.GetAsync<SubscriptionEntity>(Constants.CassandraTableNames.Subscription,
                s => s.ClientId == clientId && s.CustomerId == customerId && s.AccountId == accountId && s.Type == "A").Result.Where(s=>s.IsLatest).ToList();

            List<SubscriptionEntity> insightSubscriptions = _cassandraRepository.GetAsync<SubscriptionEntity>(Constants.CassandraTableNames.Subscription, s => s.ClientId == clientId && s.CustomerId == customerId && s.AccountId == accountId && s.Type == "S").Result.Where(s => s.IsLatest).ToList();
            subscriptions.AddRange(insightSubscriptions);

            return Mapper.Map<IEnumerable<SubscriptionEntity>, IEnumerable<SubscriptionModel>>(subscriptions);
        }

        /// <summary>
        /// Merge default subscriptions with other subscribed subscription
        /// </summary>
        /// <param name="subscriptions">List of subscribed subscriptions</param>
        /// <param name="defaultSubscriptions">List of default subscriptions</param>
        /// <returns>List of SubscriptionModel</returns>
        public List<SubscriptionModel> MergeDefaultSubscriptionListWithSubscribedPrograms(List<SubscriptionModel> subscriptions, List<SubscriptionModel> defaultSubscriptions)
        {
            if (subscriptions == null || subscriptions.Count == 0)
            {
                defaultSubscriptions.ForEach(s => s.IsSelected = false);
            }
            else
            {
                // program level
                List<SubscriptionModel> program = subscriptions.FindAll(s => !string.IsNullOrEmpty(s.ProgramName) && string.IsNullOrEmpty(s.InsightTypeName));
                if (program.Count > 0)
                {
                    program.ForEach(p =>
                    {
                        defaultSubscriptions.ForEach(ds =>
                        {
                            if (ds.ProgramName.Equals(p.ProgramName, StringComparison.OrdinalIgnoreCase))
                            {
                                ds.IsSelected = p.IsSelected;
                            }
                        });
                    });

                    IEnumerable<IGrouping<string, SubscriptionModel>> serviceDefaultSubscriptions = defaultSubscriptions.GroupBy(s => s.ServiceContractId);

                    foreach (IGrouping<string, SubscriptionModel> serivces in serviceDefaultSubscriptions)
                    {
                        List<SubscriptionModel> serviceDefaults = serivces.ToList();
                        List<SubscriptionModel> defaultServiceInsights = defaultSubscriptions.FindAll(
                            s =>
                                !string.IsNullOrEmpty(s.ProgramName) && !string.IsNullOrEmpty(s.InsightTypeName) && s.ServiceContractId == serivces.Key);

                        List<SubscriptionModel> insights = serviceDefaults.FindAll(s => !string.IsNullOrEmpty(s.ProgramName) && !string.IsNullOrEmpty(s.InsightTypeName));
                        insights.ForEach(i =>
                        {
                            if (
                            subscriptions.Exists(
                                s => string.Equals(s.InsightTypeName, i.InsightTypeName, StringComparison.CurrentCultureIgnoreCase) && string.Equals(s.ProgramName, i.ProgramName, StringComparison.CurrentCultureIgnoreCase) &&
                                     ((!string.IsNullOrEmpty(s.ServiceContractId) &&
                                       s.ServiceContractId == i.ServiceContractId) ||
                                      string.IsNullOrEmpty(s.ServiceContractId))))
                            {
                                List<SubscriptionModel> insightList =
                                subscriptions.FindAll(
                                    s =>
                                        string.Equals(s.InsightTypeName, i.InsightTypeName, StringComparison.CurrentCultureIgnoreCase) && string.Equals(s.ProgramName, i.ProgramName, StringComparison.CurrentCultureIgnoreCase) &&
                                        ((!string.IsNullOrEmpty(s.ServiceContractId) &&
                                          s.ServiceContractId == i.ServiceContractId) ||
                                         string.IsNullOrEmpty(s.ServiceContractId)));
                                foreach (var insight in insightList)
                                {
                                    i.IsSelected = insight.IsSelected;
                                    i.SubscriberThresholdValue = insight.SubscriberThresholdValue.HasValue && Math.Abs(insight.SubscriberThresholdValue.Value) > 0 ? insight.SubscriberThresholdValue :
                                        i.SubscriberThresholdValue;

                                    i.Channel = insight.Channel ?? i.Channel;
                                }
                            }

                            if (!string.IsNullOrEmpty(i.ServiceContractId))
                            {
                                var subscription = defaultServiceInsights.Find(
                                d =>
                                    string.Equals(d.ProgramName, i.ProgramName, StringComparison.CurrentCultureIgnoreCase) && string.Equals(d.InsightTypeName, i.InsightTypeName, StringComparison.CurrentCultureIgnoreCase) &&
                                    i.ServiceContractId == d.ServiceContractId);

                                subscription.IsSelected = i.IsSelected;
                                subscription.SubscriberThresholdValue = i.SubscriberThresholdValue;
                                subscription.Channel = i.Channel;
                            }
                            else
                            {
                                var subscription = defaultServiceInsights.FindAll(
                                d =>
                                    d.ProgramName.Equals(i.ProgramName, StringComparison.OrdinalIgnoreCase) && d.InsightTypeName.Equals(i.InsightTypeName, StringComparison.OrdinalIgnoreCase) &&
                                    string.IsNullOrEmpty(d.ServiceContractId));

                                foreach (var subscriptionModel in subscription)
                                {
                                    subscriptionModel.IsSelected = i.IsSelected;
                                    subscriptionModel.SubscriberThresholdValue = i.SubscriberThresholdValue;
                                    subscriptionModel.Channel = i.Channel;
                                }
                            }
                        });
                    }
                }
                else // insight level
                {
                    foreach (var subscription in subscriptions)
                    {
                        defaultSubscriptions.ForEach(
                            ds =>
                            {
                                if (string.Equals(ds.ProgramName, subscription.ProgramName, StringComparison.CurrentCultureIgnoreCase) && string.Equals(ds.InsightTypeName, subscription.InsightTypeName, StringComparison.CurrentCultureIgnoreCase)
                                && subscription.ServiceContractId == ds.ServiceContractId)
                                {
                                    ds.IsSelected = subscription.IsSelected;
                                    ds.SubscriberThresholdValue = subscription.SubscriberThresholdValue.HasValue && Math.Abs(subscription.SubscriberThresholdValue.Value) > 0 ? subscription.SubscriberThresholdValue : ds.SubscriberThresholdValue;
                                    ds.Channel = subscription.Channel;
                                }
                            }
                            );
                    }
                }
            }
            return defaultSubscriptions;
        }

        /// <summary>
        /// Get all account level subscription within the specified date range
        /// </summary>
        /// <param name="clientId">clientId of the data to be fetched</param>
        /// <param name="startDate">start date for date range</param>
        /// <param name="endDate">end date for date range</param>
        /// <param name="program">subscription program</param>
        /// <returns>List of SubscriptionEntity</returns>
        public IEnumerable<SubscriptionModel> GetSubscription(int clientId, DateTime startDate, DateTime endDate, string program)
        {
            List<SubscriptionEntity> insightSubscriptions =
                _cassandraRepository.GetAsync<SubscriptionEntity>(Constants.CassandraTableNames.Subscription,
                    s =>
                        s.ClientId == clientId && s.Type == "A" && s.ProgramName == program &&
                        s.SubscriptionDate >= startDate
                        && s.SubscriptionDate <= endDate).Result.ToList();

            return Mapper.Map<List<SubscriptionEntity>, List<SubscriptionModel>>(insightSubscriptions);
        }

        /// <summary>
        /// If customer opted out for sms then insert or update subscription with same in database
        /// </summary>
        /// <param name="billingModel"></param>
        /// <param name="subscriptions"></param>
        /// <param name="clientId">clientId of the data to be fetched</param>
        /// <param name="customerId">customerId of the data to be fetched</param>
        /// <param name="accountId">accountId of the data to be fetched</param>
        /// <param name="serviceContractId">serviceContractId of the data to be fetched</param>
        /// <param name="programName">programName of the data to be fetched</param>
        /// <param name="insightName">insightName of the data to be fetched</param>
        /// <returns>bool</returns>
        public bool InsertorMergeForSmsOptOut(BillingModel billingModel, List<SubscriptionModel> subscriptions, int clientId, string customerId, string accountId, string serviceContractId, string programName, string insightName)
        {
            var programSubscriptionModel =
                GetSubscriptionDetails(clientId, customerId, accountId, programName, string.Empty, string.Empty);
            if (programSubscriptionModel != null)
            {
                var insightSubscriptionModel =
                    GetSubscriptionDetails(clientId, customerId, accountId, programName, serviceContractId, insightName);

                if (insightSubscriptionModel != null)
                {
                    if (insightSubscriptionModel.IsSelected)
                    {
                        if (insightSubscriptionModel.Channel ==
                            Convert.ToString(Enums.SubscriptionChannel.EmailAndSMS))
                        {
                            insightSubscriptionModel.Channel = Convert.ToString(Enums.SubscriptionChannel.Email);
                            insightSubscriptionModel.IsSmsOptInCompleted = false;
                        }
                        else if (insightSubscriptionModel.Channel ==
                                 Convert.ToString(Enums.SubscriptionChannel.SMS))
                        {
                            insightSubscriptionModel.IsSelected = false;
                            insightSubscriptionModel.IsSmsOptInCompleted = false;
                        }
                    }
                }
                else
                {
                    var clientSettings = _clientConfigFacade.Value.GetClientSettings(clientId);

                    Extensions.MergeSettings(subscriptions, clientSettings, billingModel);
                    var program = clientSettings.Programs.FirstOrDefault(p => p.UtilityProgramName == programName);
                    var insight = program?.Insights.FirstOrDefault(i => i.UtilityInsightName == insightName);
                    if (insight != null)
                    {
                        if (insight.DefaultCommunicationChannel.Contains(
                            Convert.ToString(Enums.SubscriptionChannel.EmailAndSMS))
                            &&
                            (programSubscriptionModel.Channel ==
                             Convert.ToString(Enums.SubscriptionChannel.EmailAndSMS) ||
                             string.IsNullOrWhiteSpace(programSubscriptionModel.Channel)))
                        {
                            insightSubscriptionModel = new SubscriptionModel
                            {
                                ClientId = clientId,
                                CustomerId = customerId,
                                AccountId = accountId,
                                ProgramName = program.UtilityProgramName,
                                InsightTypeName = insight.UtilityInsightName,
                                ServiceContractId = serviceContractId,
                                IsSelected = true,
                                SmsOptInCompletedDate = programSubscriptionModel.SmsOptInCompletedDate,
                                IsSmsOptInCompleted = false,
                                EmailOptInCompletedDate = programSubscriptionModel.EmailOptInCompletedDate,
                                IsEmailOptInCompleted = programSubscriptionModel.IsEmailOptInCompleted,
                                Channel = Convert.ToString(Enums.SubscriptionChannel.Email)
                            };
                        }
                        else if (insight.DefaultCommunicationChannel.Contains(
                            Convert.ToString(Enums.SubscriptionChannel.SMS))
                                 &&
                                 (programSubscriptionModel.Channel ==
                                  Convert.ToString(Enums.SubscriptionChannel.SMS) ||
                                  string.IsNullOrWhiteSpace(programSubscriptionModel.Channel)))
                            insightSubscriptionModel = new SubscriptionModel
                            {
                                ClientId = clientId,
                                CustomerId = customerId,
                                AccountId = accountId,
                                ProgramName = program.UtilityProgramName,
                                InsightTypeName = insight.UtilityInsightName,
                                ServiceContractId = serviceContractId,
                                IsSelected = false,
                                SmsOptInCompletedDate = programSubscriptionModel.SmsOptInCompletedDate,
                                IsSmsOptInCompleted = false,
                                EmailOptInCompletedDate = programSubscriptionModel.EmailOptInCompletedDate,
                                IsEmailOptInCompleted = programSubscriptionModel.IsEmailOptInCompleted,
                                Channel = Convert.ToString(Enums.SubscriptionChannel.SMS)
                            };
                    }
                }

                if (insightSubscriptionModel != null)
                {
                    if (!InsertSubscription(insightSubscriptionModel))
                    {
                        Logger.Fatal(
                            @" Insertion failed for subscription optout.", LogModel);
                        return false;
                    }
                    
                    // opt-out at program level
                    if (insightSubscriptionModel.IsSelected || subscriptions.Exists(
                        s =>
                            !string.IsNullOrEmpty(s.Channel) && s.Channel.Equals(Enums.SubscriptionChannel.EmailAndSMS.GetDescription(),
                                StringComparison.CurrentCultureIgnoreCase)))
                    {
                        programSubscriptionModel.IsSelected = true;
                        programSubscriptionModel.IsSmsOptInCompleted = false;
                    }
                    else
                    {
                        programSubscriptionModel.IsSelected = false;
                        programSubscriptionModel.IsSmsOptInCompleted = false;
                    }

                    if (!InsertSubscription(programSubscriptionModel))
                    {
                        Logger.Fatal(
                            @" Insertion failed for subscription optout.", LogModel);
                        return false;
                    }
                }
            }
            return true;
        }

        public Task<bool> DeleteSubscriptionAsync(int clientId, string type, string customerId, string accountId)
        {
            _cassandraRepository.DeleteAsync<SubscriptionEntity>(Constants.CassandraTableNames.Subscription, s => s.ClientId == clientId && s.CustomerId == customerId && s.AccountId == accountId &&
                 s.Type == type);
            return Task.Run(() => true);
        }
    }
}
