﻿using System.Threading.Tasks;
using AO.BusinessContracts;
using AO.DataAccess.Contracts;
using AO.Entities;
using AutoMapper;
using CE.AO.Models;

namespace AO.Business
{
	public class DcuAlarmCodeHistory : IDcuAlarmCodeHistory
	{
		public LogModel LogModel { get; set; }

		private ICassandraRepository CassandraRepository { get; }

		public DcuAlarmCodeHistory(LogModel logModel, ICassandraRepository cassandraRepository)
		{
			LogModel = logModel;
			CassandraRepository = cassandraRepository;
		}

		/// <summary>
		/// To insert/update the DcuAlarmModel in dcu_alarms_by_code_history table
		/// </summary>
		/// <param name="model">DcuAlarmModel which is to be inserted/updated</param>
		/// <returns>True</returns>
		public async Task<bool> InsertOrMergeDcuAlarmCodeHistoryAsync(DcuAlarmModel model)
		{
			var entity = Mapper.Map<DcuAlarmModel, DcuAlarmByCodeHistoryEntity>(model);
		    await
		        CassandraRepository.InsertOrUpdateAsync(entity, Constants.CassandraTableNames.DcuAlarmByCodeHistory)
		            .ConfigureAwait(false);
			return await Task.Factory.StartNew(() => true);
		}

		/// <summary>
		/// To delete the DcuAlarmModel in dcu_alarms_by_code_history table
		/// </summary>
		/// <param name="model">DcuAlarmModel which is to be deleted</param>
		/// <returns>True</returns>
		public async Task<bool> DeleteDcuAlarmCodeHistoryAsync(DcuAlarmModel model)
		{
		    await
		        CassandraRepository.DeleteAsync<DcuAlarmByCodeHistoryEntity>(
		            Constants.CassandraTableNames.DcuAlarmByCodeHistory,
		            p =>
		                p.ClientId == model.ClientId && p.DcuId == model.DcuId && p.AlarmTime == model.AlarmTime &&
		                p.AlarmCode == model.AlarmCode).ConfigureAwait(false);
			return await Task.Factory.StartNew(() => true);
		}

		/// <summary>
		/// To delete the DcuAlarmModel with Partition from dcu_alarms_by_code_history table
		/// </summary>
		/// <param name="model">DcuAlarmModel which is to be deleted</param>
		/// <returns>True</returns>
		public async Task<bool> DeleteDcuAlarmCodeWithPartitionHistoryAsync(DcuAlarmModel model)
		{
		    await
		        CassandraRepository.DeleteAsync<DcuAlarmByCodeHistoryEntity>(
		            Constants.CassandraTableNames.DcuAlarmByCodeHistory,
		            p => p.ClientId == model.ClientId && p.AlarmCode == model.AlarmCode).ConfigureAwait(false);
			return await Task.Factory.StartNew(() => true);
		}
	}
}
