﻿using System;
using System.Threading.Tasks;
using AO.BusinessContracts;
using AO.DataAccess.Contracts;
using AO.Entities;
using AutoMapper;
using CE.AO.Models;

namespace AO.Business
{
    /// <summary>
    ///  Alarm Class which will interact with database
    /// </summary>
    public class Alarm : IAlarm
    {
        private Lazy<ICassandraRepository> CassandraRepository { get; }

        public Alarm(Lazy<ICassandraRepository> cassandraRepository)
        {
            CassandraRepository = cassandraRepository;
        }

        /// <summary>
        /// To delete the AlarmModel in alarms table.
        /// </summary>
        /// <param name="model"> AlarmModel which is to be deleted</param>
        /// <returns>True</returns>
        public async Task<bool> DeleteAlarmAsync(AlarmModel model)
        {
            await
                CassandraRepository.Value.DeleteAsync<AlarmEntity>(Constants.CassandraTableNames.Alarm,
                    p => p.ClientId == model.ClientId
                         && p.AlarmSourceTypeId == model.AlarmSourceTypeId && p.AlarmGroupId == model.AlarmGroupId &&
                         p.AlarmTime == model.AlarmTime
                         && p.AlarmTypeId == model.AlarmTypeId && p.AlarmId == model.AlarmId).ConfigureAwait(false);
            return await Task.Factory.StartNew(() => true);
        }

        /// <summary>
        /// To insert/update the AlarmModel to the database
        /// </summary>
        /// <param name="model">AlarmModel which is to be inserted/updated</param>
        /// <returns>True</returns>
        /// 
        public async Task<bool> InsertOrMergeAlarmAsync(AlarmModel model)
        {
            var entity = Mapper.Map<AlarmModel, AlarmEntity>(model);
            await
                CassandraRepository.Value.InsertOrUpdateAsync(entity, Constants.CassandraTableNames.Alarm)
                    .ConfigureAwait(false);
            return await Task.Factory.StartNew(() => true);
        }
    }
}
