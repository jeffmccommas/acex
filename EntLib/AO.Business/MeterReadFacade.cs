﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AO.BusinessContracts;
using AutoMapper;
using CE.AO.Models;

namespace AO.Business
{
    /// <summary>
    /// MeterReadFacade class is used to insert/update the meter reads to the database
    /// </summary>
    public class MeterReadFacade : IMeterReadFacade
    {
        private readonly IConsumptionByAccount _consumptionByAccount;
        private readonly ITallAMI _tallAmiManager;
        
        public MeterReadFacade(IConsumptionByAccount consumptionByAccount, ITallAMI tallAmiManager)
        {
            _consumptionByAccount = consumptionByAccount;
            _tallAmiManager = tallAmiManager;
        }

        /// <summary>
        /// To insert or update Meter Reads to database
        /// </summary>
        /// <param name="model">model to be inserted</param>
        /// <returns>true</returns>
        public async Task<bool> InsertOrMergeMeterReadsAsync(MeterReadModel model)
        {
            var consumptionByAccountModel = Mapper.Map<MeterReadModel, ConsumptionByAccountModel>(model);
            var tallAmiModel = Mapper.Map<MeterReadModel, TallAmiModel>(model);
            var tasks = new List<Task<bool>>
            {
                _consumptionByAccount.InsertOrMergeConsumptionByAccountAsync(consumptionByAccountModel),
                _tallAmiManager.InsertOrMergeAmiAsync(tallAmiModel)
            };
            bool[] taskResults = await Task.WhenAll(tasks);
            return !taskResults.Any(result => false);
        }
    }
}
