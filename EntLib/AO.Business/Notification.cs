﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using AO.BusinessContracts;
using AO.DataAccess.Contracts;
using CE.AO.Logging;
using CE.AO.Models;
using CE.ContentModel.Entities;
using CE.RateModel;
using Enums = CE.AO.Utilities.Enums;

//
// 10/2016 chackett change to pass down client config 
//

namespace AO.Business
{
    /// <summary>
    /// Notification class is used to fetch/insert/update the notifiaction details from/to the database and send the notifications through email and sms.
    /// </summary>
    public partial class Notification : INotification
    {
        private const string StatusNoRecipient = "MPME2009";
        private const string StatusMmsNotSupported = "MPME2020";
        private const string StatusInvalidSubscription = "MPME2301";
        private const string StatusInProgress = "MPCE4001";
        private const string StatusSent = "sent";
        private const string StatusSending = "sending";
        private const string StatusScheduled = "scheduled";

        private readonly ITrumpiaRequestDetail _trumpiaRequestDetail;
        private readonly ISubscription _subscriptionManager;
        private readonly Lazy<ISendEmail> _sendEmailManager;
        private readonly INotificationRepository _notificationRepository;
        private readonly Lazy<ICassandraRepository> _cassandraRepository; 
        private readonly Lazy<IClientConfigFacade> _clientConfigFacade;
        private readonly IEvaluation _evalationManger;

        public Notification(LogModel logModel, INotificationRepository notificationRepository, ITrumpiaRequestDetail trumpiaRequestDetail, ISubscription subscriptionManager, IEvaluation evaluationManager, Lazy<ISendEmail> sendEmailManager, Lazy<IClientConfigFacade> clientConfigFacade, Lazy<ICassandraRepository> cassandraRepository)
        {
            LogModel = logModel;
            _trumpiaRequestDetail = trumpiaRequestDetail;
            _subscriptionManager = subscriptionManager;
            _sendEmailManager = sendEmailManager;
            _clientConfigFacade = clientConfigFacade;
            _notificationRepository = notificationRepository;
            _cassandraRepository = cassandraRepository;
            _evalationManger = evaluationManager;
        }

        public LogModel LogModel { get; set; }

        /// <summary>
        /// Send notifications using email/sms
        /// </summary>
        /// <param name="subscriptions">List of subscription to send notification</param>
        /// <param name="evaluation">Insight model</param>
        /// <param name="clientSettings">Client settings</param>
        /// <param name="customerModel">Customer model</param>
        /// <param name="billingModel">Billing model</param>
        /// <param name="tierBoundariesWithServiceContractId">Dictionary of list of TierBoundary</param>
        /// <param name="calculations">List of CalculationModel model</param>
        /// <returns></returns>
        public async Task ProcessMessage(List<SubscriptionModel> subscriptions, InsightModel evaluation, ClientSettings clientSettings,
            CustomerModel customerModel, BillingModel billingModel, Dictionary<string, List<TierBoundary>> tierBoundariesWithServiceContractId, List<CalculationModel> calculations)
        {

            if (LogModel == null)
            {
                LogModel = new LogModel
                {
                    ClientId = evaluation.ClientId.ToString(),
                    CustomerId = evaluation.CustomerId,
                    AccountId = evaluation.AccountId,
                    ServiceContractId = evaluation.ServiceContractId,
                    Module = Enums.Module.Notification
                };
            }
            Extensions.MergeSettings(subscriptions, clientSettings, billingModel);

            var clientTimezoneString = clientSettings.TimeZone; //Set from Settings            
            var clientTimezoneEnum = (Enums.Timezone)Enum.Parse(typeof(Enums.Timezone), Convert.ToString(clientTimezoneString));
            var clientTimezone = TimeZoneInfo.FindSystemTimeZoneById(clientTimezoneEnum.GetDescription());

            // email settings
            var clientEmailUsername = clientSettings.OptInEmailUserName;
            var clientEmailPassword = clientSettings.OptInEmailPassword;
            var clientEmailFrom = clientSettings.OptInEmailFrom;

            // sms settings
            var smsApiKey = clientSettings.TrumpiaApiKey;
            var smsUserName = clientSettings.TrumpiaUserName;

            var programs = clientSettings.Programs.Where(p => p.UtilityProgramName == evaluation.ProgramName).ToList();
            foreach (var program in programs)
            {
                var programSubscription = subscriptions.OrderByDescending(s => s.SubscriptionDate).FirstOrDefault(s => s.ProgramName == program.UtilityProgramName && string.IsNullOrEmpty(s.InsightTypeName));

                foreach (var insight in program.Insights)
                {
                    //Get the notifytime from clientsetting and convert it to utc time and then send to email
	                var utcNow = DateTime.UtcNow;
                    var notifyDateTime = !string.IsNullOrEmpty(insight.NotifyTime) ? DateTime.ParseExact(insight.NotifyTime, "HH:mm", CultureInfo.InvariantCulture) : utcNow;
                    var currentDate = new DateTime(utcNow.Year, utcNow.Month, utcNow.Day, notifyDateTime.Hour, notifyDateTime.Minute, notifyDateTime.Second);
                    var setNotifyEmailDate = !string.IsNullOrEmpty(insight.NotifyTime) ? (TimeZoneInfo.ConvertTimeToUtc(currentDate, clientTimezone)) : utcNow;
					DateTime? setSmsDate = !string.IsNullOrEmpty(insight.NotifyTime) ? (TimeZoneInfo.ConvertTimeBySystemTimeZoneId(TimeZoneInfo.ConvertTimeToUtc(currentDate, clientTimezone), "Eastern Standard Time")) : (DateTime?)null;
	                DateTime? setNotifySmsDate = null;
                    var estNow = (TimeZoneInfo.ConvertTimeBySystemTimeZoneId(utcNow, "Eastern Standard Time"));
                    //The time at which we are sending sms should be always greater than UtcNow else send sms after 1 day.
                    if (setSmsDate != null)
		                setNotifySmsDate = setSmsDate < estNow
                            ? Convert.ToDateTime(setSmsDate).AddDays(1)
			                : setSmsDate;
                    string tier;
                    var calculation = !string.IsNullOrEmpty(evaluation.ServiceContractId)
                        ? calculations.Where(c => !string.IsNullOrEmpty(c.ServiceContractId)).FirstOrDefault(c => c.ServiceContractId.Equals(evaluation.ServiceContractId))
                        : calculations.FirstOrDefault(c => string.IsNullOrEmpty(c.ServiceContractId));
                    var notificationModelList = NotificationRequired(evaluation, calculation, insight, customerModel.CustomerId, out tier);
                    var commodityType = calculation.CommodityId != null ? ((Enums.CommodityType)calculation.CommodityId).GetDescription() : string.Empty;

                    var customInsightModel = Extensions.GetCustomInsightModel(subscriptions, insight, program, evaluation.ServiceContractId, commodityType, billingModel?.UOMId);

                    var isCommodityTypecheck = false;

                    string insightcommodityType = string.IsNullOrEmpty(insight.CommodityType) ? "" : insight.CommodityType;

                    if ((string.IsNullOrEmpty(commodityType) && string.IsNullOrEmpty(insightcommodityType)) || insightcommodityType.Contains(commodityType) || string.IsNullOrEmpty(evaluation.ServiceContractId))
                    {
                        isCommodityTypecheck = true;
                    }
                    else if (!string.IsNullOrEmpty(insightcommodityType))
                    {
                        isCommodityTypecheck = insightcommodityType.Contains(commodityType);
                    }

                    if (isCommodityTypecheck)
                    {
                        var tierBoundaries = new List<TierBoundary>();
                        if (!string.IsNullOrEmpty(evaluation.ServiceContractId))
                            tierBoundariesWithServiceContractId.TryGetValue(evaluation.ServiceContractId, out tierBoundaries);

                        var insightName = insight.InsightName;

                        if (notificationModelList != null && IsNotified(evaluation, insight, tier))
                        {
							var notificationDate = notificationModelList.FirstOrDefault().NotifiedDateTime;
							// the channel portion will be added to custom key when sending email/sms
                            // use : for seperator instead of _ since it's used by service id/service point id already
							var customKey =
								$"{evaluation.ClientId};{evaluation.CustomerId};{evaluation.AccountId};{notificationDate:O};{program.ProgramName};{evaluation.ServiceContractId};{insightName};false";

							var uomContentList = _clientConfigFacade.Value.GetClientUom(evaluation.ClientId);
                            var textContentList = _clientConfigFacade.Value.GetTextContents(evaluation.ClientId, "en-US", "common");
                            
                            var tasks = new List<Task>();
                            
                            if (insight.DefaultCommunicationChannel.Equals(Enums.SubscriptionChannel.Email.GetDescription(), StringComparison.InvariantCultureIgnoreCase) && Convert.ToBoolean(programSubscription.IsEmailOptInCompleted))
                            {
                                if (!string.IsNullOrEmpty(customerModel.EmailAddress) &&
                                    !string.IsNullOrEmpty(insight.EmailTemplateId) &&
                                    IsNotified(evaluation, insight, tier))
                                {
                                    // delete previous latest notification
                                    await DeletePreviousLatestNotification(evaluation, insightName,
                                        insight.DefaultCommunicationChannel);
                                    tasks.Add(
                                        SaveLatestAndHistoryNotification(
                                            notificationModelList.FirstOrDefault(
                                                n => n.Channel.Equals(Enums.SubscriptionChannel.Email.GetDescription()))));
                                    var emailIdentifiers = GetEmailSubstitutions(clientSettings, evaluation,
                                        customInsightModel, customerModel, billingModel, tierBoundaries, calculation,
                                        clientSettings.UnmaskedAccountIdEndingDigit, uomContentList, textContentList);
                                    tasks.Add(_sendEmailManager.Value.SendEmailAsync(clientEmailFrom,
                                        new List<string> {customerModel.EmailAddress}, emailIdentifiers,
                                        insight.EmailTemplateId, clientEmailUsername, clientEmailPassword,
                                        $"{customKey};{Enums.SubscriptionChannel.Email.GetDescription()}",
                                        setNotifyEmailDate));
                                }
                            }
                            else if (insight.DefaultCommunicationChannel.Equals(Enums.SubscriptionChannel.SMS.GetDescription(), StringComparison.InvariantCultureIgnoreCase) && Convert.ToBoolean(programSubscription.IsSmsOptInCompleted))
                            {
                                if (GenerateSms(estNow, setSmsDate, insight) &&
                                    !string.IsNullOrEmpty(customerModel.TrumpiaSubscriptionId) &&
                                    IsNotified(evaluation, insight, tier))
                                {
                                    // delete previous latest notification
                                    await DeletePreviousLatestNotification(evaluation, insightName,
                                        insight.DefaultCommunicationChannel);
                                    tasks.Add(
                                        SaveLatestAndHistoryNotification(
                                            notificationModelList.FirstOrDefault(
                                                n => n.Channel.Equals(Enums.SubscriptionChannel.SMS.GetDescription()))));
                                    var smsBody = GetSmsBody(clientSettings, evaluation, calculation, billingModel,
                                        customInsightModel, insight.SmsTemplateId,
                                        clientSettings.UnmaskedAccountIdEndingDigit, uomContentList, textContentList);
                                    tasks.Add(
                                        SendSms(billingModel, subscriptions,
                                            notificationModelList.FirstOrDefault(
                                                n => n.Channel.Equals(Enums.SubscriptionChannel.SMS.GetDescription())),
                                            customerModel,
                                            $"{customKey};{Enums.SubscriptionChannel.SMS.GetDescription()}", smsApiKey,
                                            smsUserName, smsBody, evaluation,
                                            program.ProgramName, insight, notificationDate, tier,
                                            setNotifySmsDate));
                                }
                            }
                            else if (insight.DefaultCommunicationChannel.Equals(Enums.SubscriptionChannel.EmailAndSMS.GetDescription(), StringComparison.InvariantCultureIgnoreCase))
                            {
                                if (Convert.ToBoolean(programSubscription.IsEmailOptInCompleted) && !string.IsNullOrEmpty(customerModel.EmailAddress) &&
                                    !string.IsNullOrEmpty(insight.EmailTemplateId))
                                {
                                    var emailIdentifiers = GetEmailSubstitutions(clientSettings, evaluation, customInsightModel,
                                        customerModel,
                                        billingModel, tierBoundaries, calculation,
                                        clientSettings.UnmaskedAccountIdEndingDigit, uomContentList, textContentList);


                                    var emailInsight = new InsightSettings
                                    {
                                        AllowedCommunicationChannel = insight.AllowedCommunicationChannel,
                                        CommodityType = insight.CommodityType,
                                        DefaultCommunicationChannel = Enums.SubscriptionChannel.Email.GetDescription(),
                                        DefaultFrequency = insight.DefaultFrequency,
                                        DefaultValue = insight.DefaultValue,
                                        EmailTemplateId = insight.EmailTemplateId,
                                        InsightName = insight.InsightName,
                                        Level = insight.Level,
                                        NotificationDay = insight.NotificationDay,
                                        NotifyTime = insight.NotifyTime,
                                        UtilityInsightName = insight.UtilityInsightName,
                                        SmsTemplateId = insight.SmsTemplateId,
                                        TrumpiaKeyword = insight.TrumpiaKeyword,
                                        ThresholdType = insight.ThresholdType,
                                        ThresholdMax = insight.ThresholdMax,
                                        ThresholdMin = insight.ThresholdMin
                                    };

                                    // check if it's the latest evaluation before continue
                                    if (IsNotified(evaluation, emailInsight, tier))
                                    {
                                        // delete previous latest notification
                                        await DeletePreviousLatestNotification(evaluation, insightName,
                                        Enums.SubscriptionChannel.Email.ToString());
                                        tasks.Add(SaveLatestAndHistoryNotification(notificationModelList.FirstOrDefault(n => n.Channel.Equals(Enums.SubscriptionChannel.Email.GetDescription()))));
                                        tasks.Add(_sendEmailManager.Value.SendEmailAsync(clientEmailFrom,
                                            new List<string> { customerModel.EmailAddress }, emailIdentifiers,
                                            insight.EmailTemplateId, clientEmailUsername, clientEmailPassword, $"{customKey};{Enums.SubscriptionChannel.Email.GetDescription()}", setNotifyEmailDate));
                                    }
                                        
                                }
                                if (GenerateSms(estNow, setSmsDate, insight) &&
                                    (Convert.ToBoolean(programSubscription.IsSmsOptInCompleted) &&
                                     !string.IsNullOrEmpty(customerModel.TrumpiaSubscriptionId)))
                                {
                                    var smsBody = GetSmsBody(clientSettings, evaluation, calculation, billingModel,
                                        customInsightModel,
                                        insight.SmsTemplateId, clientSettings.UnmaskedAccountIdEndingDigit,
                                        uomContentList, textContentList);
                                    
                                    // check if it's the latest evaluation before continue
                                    
                                    var smsInsight = new InsightSettings
                                    {
                                        AllowedCommunicationChannel =  insight.AllowedCommunicationChannel,
                                        CommodityType =  insight.CommodityType,
                                        DefaultCommunicationChannel = Enums.SubscriptionChannel.SMS.GetDescription(),
                                        DefaultFrequency = insight.DefaultFrequency,
                                        DefaultValue = insight.DefaultValue,
                                        EmailTemplateId = insight.EmailTemplateId,
                                        InsightName = insight.InsightName,
                                        Level = insight.Level,
                                        NotificationDay =  insight.NotificationDay,
                                        NotifyTime = insight.NotifyTime,
                                        UtilityInsightName = insight.UtilityInsightName,
                                        SmsTemplateId = insight.SmsTemplateId,
                                        TrumpiaKeyword = insight.TrumpiaKeyword,
                                        ThresholdType = insight.ThresholdType,
                                        ThresholdMax = insight.ThresholdMax,
                                        ThresholdMin = insight.ThresholdMin
                                    };
                                    
                                    if (IsNotified(evaluation, smsInsight, tier))
                                    {
                                        // delete previous latest notification
                                        await DeletePreviousLatestNotification(evaluation, insightName,
                                        Enums.SubscriptionChannel.SMS.ToString());
                                        tasks.Add(
                                            SaveLatestAndHistoryNotification(
                                                notificationModelList.FirstOrDefault(
                                                    n => n.Channel.Equals(Enums.SubscriptionChannel.SMS.GetDescription()))));
                                        tasks.Add(
                                            SendSms(billingModel, subscriptions,
                                                notificationModelList.FirstOrDefault(
                                                    n => n.Channel.Equals(Enums.SubscriptionChannel.SMS.GetDescription())),
                                                customerModel,
                                                $"{customKey};{Enums.SubscriptionChannel.SMS.GetDescription()}", smsApiKey,
                                                smsUserName, smsBody, evaluation,
                                                program.ProgramName, insight, notificationDate, tier,
                                                setNotifySmsDate));
                                    }
                                }
                            }
                            else if (
                                insight.DefaultCommunicationChannel.Equals(
                                    Enums.SubscriptionChannel.File.GetDescription(),
                                    StringComparison.InvariantCultureIgnoreCase) &&
                                IsNotified(evaluation, insight, tier))
                            {
                                // delete previous latest notification
                                await DeletePreviousLatestNotification(evaluation, insightName,
                                    insight.DefaultCommunicationChannel);
                                tasks.Add(
                                    SaveLatestAndHistoryNotification(
                                        notificationModelList.FirstOrDefault(
                                            n => n.Channel.Equals(Enums.SubscriptionChannel.File.GetDescription()))));
                            }

                            try
                            {
                                await Task.WhenAll(tasks).ConfigureAwait(false);
                            }
                            catch (Exception)
                            {
                                // if any exception during send email or sms, set notified as false
                                foreach (var notificationModel in notificationModelList)
                                {
                                    // check if it's the latest evaluation before continue
                                    if (IsNotified(evaluation, insight, tier))
                                    {
                                        notificationModel.IsNotified = false;
                                        SaveLatestAndHistoryNotification(notificationModel).Wait();
                                    }
                                }

                                throw;
                            }
                        }
                    }
                }
            }
        }

        private List<NotificationModel> NotificationRequired(InsightModel evaluation, CalculationModel calculation, InsightSettings insightSettings, string customerId, out string tier)
        {
            List<NotificationModel> notificationModelList = null;
            tier = string.Empty;
            decimal cost;
            decimal projectedCost;
            // Don't generate notification except DayThreshold if calculation was done in previous or older bill cycle
            if (insightSettings.InsightName != "DayThreshold" && Convert.ToDateTime(calculation.BillCycleEndDate).Date < DateTime.Today.Date)
            {
                return null;
            }
            switch (insightSettings.InsightName)
            {
                case "BillToDate":
                case "AccountProjectedCost":
                    cost = 0m;
                    projectedCost = 0m;
                    if (calculation.Cost.HasValue)
                        cost = Convert.ToDecimal(calculation.Cost);
                    if (calculation.ProjectedCost.HasValue)
                        projectedCost = Convert.ToDecimal(calculation.Cost);
                    // don't generate notification if cost and projected cost is 0
                    if (string.IsNullOrEmpty(evaluation.ServiceContractId) && cost != 0m && projectedCost != 0)
                        notificationModelList = CreateNotification(evaluation, insightSettings, customerId, out tier);
                    break;
                case "CostToDate":
                case "ServiceProjectedCost":
                    cost = 0m;
                    projectedCost = 0m;
                    if (calculation.Cost.HasValue)
                        cost = Convert.ToDecimal(calculation.Cost);
                    if (calculation.ProjectedCost.HasValue)
                        projectedCost = Convert.ToDecimal(calculation.Cost);
                    // don't generate notification if cost and projected cost is 0
                    if (!string.IsNullOrEmpty(evaluation.ServiceContractId) && cost != 0m && projectedCost != 0)
                        notificationModelList = CreateNotification(evaluation, insightSettings, customerId, out tier);
                    break;
                case "Usage":
                    if (!string.IsNullOrEmpty(evaluation.ServiceContractId) && evaluation.Usage != null)
                        notificationModelList = CreateNotification(evaluation, insightSettings, customerId, out tier);
                    break;
                case "AccountLevelCostThreshold":
                    if (string.IsNullOrEmpty(evaluation.ServiceContractId) &&
                        Convert.ToBoolean(evaluation.AccountLevelCostThresholdExceed))
                        notificationModelList = CreateNotification(evaluation, insightSettings, customerId, out tier);
                    break;
                case "ServiceLevelUsageThreshold":
                    if (!string.IsNullOrEmpty(evaluation.ServiceContractId) &&
                        Convert.ToBoolean(evaluation.ServiceLevelUsageThresholdExceed))
                        notificationModelList = CreateNotification(evaluation, insightSettings, customerId, out tier);
                    break;
                case "ServiceLevelCostThreshold":
                    if (!string.IsNullOrEmpty(evaluation.ServiceContractId) &&
                        Convert.ToBoolean(evaluation.ServiceLevelCostThresholdExceed))
                        notificationModelList = CreateNotification(evaluation, insightSettings, customerId, out tier);
                    break;
                case "DayThreshold":
                    if (!string.IsNullOrEmpty(evaluation.ServiceContractId) &&
                        Convert.ToBoolean(evaluation.DayThresholdExceed))
                        notificationModelList = CreateNotification(evaluation, insightSettings, customerId, out tier);
                    break;
                case "ServiceLevelTieredThresholdApproaching":
                    if (!string.IsNullOrEmpty(evaluation.ServiceContractId) &&
                        (Convert.ToBoolean(evaluation.ServiceLevelTiered1ThresholdApproaching)
                         || Convert.ToBoolean(evaluation.ServiceLevelTiered2ThresholdApproaching)
                         || Convert.ToBoolean(evaluation.ServiceLevelTiered3ThresholdApproaching)
                         || Convert.ToBoolean(evaluation.ServiceLevelTiered4ThresholdApproaching)
                         || Convert.ToBoolean(evaluation.ServiceLevelTiered5ThresholdApproaching)
                         || Convert.ToBoolean(evaluation.ServiceLevelTiered6ThresholdApproaching)))
                        notificationModelList = CreateNotification(evaluation, insightSettings, customerId, out tier);
                    break;
                case "ServiceLevelTieredThresholdExceed":
                    if (!string.IsNullOrEmpty(evaluation.ServiceContractId) &&
                        Convert.ToBoolean(evaluation.ServiceLevelTieredThresholdExceed))
                        notificationModelList = CreateNotification(evaluation, insightSettings, customerId, out tier);
                    break;
            }
            return notificationModelList;
        }

        private List<NotificationModel> CreateNotification(InsightModel evaluation, InsightSettings insightsetting, string customerId, out string tier)
        {
            tier = string.Empty;
            if (insightsetting.InsightName.Equals("ServiceLevelTieredThresholdApproaching", StringComparison.InvariantCultureIgnoreCase))
            {
                if (evaluation.ServiceLevelTiered6ThresholdApproaching == true)
                {
                    tier = "Tier6";
                }
                else if (evaluation.ServiceLevelTiered5ThresholdApproaching == true)
                {
                    tier = "Tier5";
                }
                else if (evaluation.ServiceLevelTiered4ThresholdApproaching == true)
                {
                    tier = "Tier4";
                }
                else if (evaluation.ServiceLevelTiered3ThresholdApproaching == true)
                {
                    tier = "Tier3";
                }
                else if (evaluation.ServiceLevelTiered2ThresholdApproaching == true)
                {
                    tier = "Tier2";
                }
                else if (evaluation.ServiceLevelTiered1ThresholdApproaching == true)
                {
                    tier = "Tier1";
                }
            }
            var notificationModelList = new List<NotificationModel>();
            var notificationModel = new NotificationModel()
            {
                ClientId = evaluation.ClientId,
                AccountId = evaluation.AccountId,
                ProgramName = evaluation.ProgramName,
                Insight = insightsetting.InsightName,
                CustomerId = customerId,
                ServiceContractId = evaluation.ServiceContractId,
                IsNotified = true,
                NotifiedDateTime = DateTime.UtcNow,
                IsBounced = null,
                IsDelivered = null,
                IsDropped = null,
                IsOpened = null,
                IsClicked = null,
                EvaluationPK = $"{evaluation.ClientId}_{evaluation.AccountId}",
                EvaluationRK =
                    string.IsNullOrEmpty(evaluation.ServiceContractId)
                        ? $@"{evaluation.AsOfEvaluationDate.ToString("yyyy-MM-dd")}_A_{evaluation.ProgramName}"
                        : $"{evaluation.AsOfEvaluationDate.ToString("yyyy-MM-dd")}_S_{evaluation.ProgramName}_{evaluation.ServiceContractId}"
            };

            if (!string.IsNullOrEmpty(tier))
                notificationModel.Tier = tier;
            if (insightsetting.DefaultCommunicationChannel.Equals(Enums.SubscriptionChannel.Email.GetDescription(), StringComparison.InvariantCultureIgnoreCase))
            {
                notificationModel.Channel = Enums.SubscriptionChannel.Email.GetDescription();
                notificationModel.TemplateId = insightsetting.EmailTemplateId;
            }
            if (insightsetting.DefaultCommunicationChannel.Equals(Enums.SubscriptionChannel.SMS.GetDescription(), StringComparison.InvariantCultureIgnoreCase))
            {
                notificationModel.Channel = Enums.SubscriptionChannel.SMS.GetDescription();
                notificationModel.TemplateId = insightsetting.SmsTemplateId;
            }

            if (insightsetting.DefaultCommunicationChannel.Equals(Enums.SubscriptionChannel.EmailAndSMS.GetDescription(), StringComparison.InvariantCultureIgnoreCase))
            {
                notificationModel.Channel = Enums.SubscriptionChannel.Email.GetDescription();
                notificationModel.TemplateId = insightsetting.EmailTemplateId;

                var notificationModelSms = new NotificationModel();

                foreach (var prop in notificationModel.GetType().GetProperties())
                {
                    notificationModelSms.GetType().GetProperty(prop.Name).SetValue(notificationModelSms, prop.GetValue(notificationModel));
                }
                notificationModelSms.Channel = Enums.SubscriptionChannel.SMS.GetDescription();
                notificationModelSms.TemplateId = insightsetting.SmsTemplateId;

                notificationModelList.Add(notificationModelSms);
            }
            if (insightsetting.DefaultCommunicationChannel.Equals(Enums.SubscriptionChannel.File.GetDescription()))
                notificationModel.Channel = Enums.SubscriptionChannel.File.GetDescription();
            notificationModelList.Add(notificationModel);
            return notificationModelList;
        }

        private bool IsNotified(InsightModel evaluation, InsightSettings insightSettings, string tier)
        {
            var latestEval = string.IsNullOrEmpty(evaluation.ServiceContractId)
                ? _evalationManger.GetAccountLevelEvaluationsAsync(evaluation.ClientId, evaluation.CustomerId,
                    evaluation.AccountId, evaluation.ProgramName).Result
                : _evalationManger.GetServiceLevelEvaluationsAsync(evaluation.ClientId, evaluation.CustomerId,
                    evaluation.AccountId, evaluation.ProgramName, evaluation.ServiceContractId).Result;

            // check if it's the latest evaluation
            if (latestEval.AsOfEvaluationId == evaluation.AsOfEvaluationId)
            {
                var notification = GetNotification(evaluation, insightSettings).Result;



                if (notification == null)
                {
                    if (insightSettings.DefaultFrequency.Equals(Enums.NotificationFrequency.Weekly.GetDescription()))
                    {
                        if (insightSettings.NotificationDay.Equals(DateTime.UtcNow.DayOfWeek.ToString()))
                            return true;
                        return false;
                    }
                    return true;
                }

                if (insightSettings.DefaultFrequency.Equals(Enums.NotificationFrequency.Daily.GetDescription()) &&
                    notification.IsNotified != true)
                    return true;
                if (insightSettings.DefaultFrequency.Equals(Enums.NotificationFrequency.Daily.GetDescription()) &&
                    DateTime.UtcNow.Date > notification.NotifiedDateTime.Date)
                    return true;
                if (insightSettings.DefaultFrequency.Equals(Enums.NotificationFrequency.Weekly.GetDescription()) &&
                    insightSettings.NotificationDay.Equals(DateTime.UtcNow.DayOfWeek.ToString()) &&
                    notification.IsNotified != true)
                    return true;
                if (insightSettings.DefaultFrequency.Equals(Enums.NotificationFrequency.Weekly.GetDescription()) &&
                    insightSettings.NotificationDay.Equals(DateTime.UtcNow.DayOfWeek.ToString()) &&
                    DateTime.UtcNow.AddDays(-7).Date >= notification.NotifiedDateTime.Date)
                    return true;
                if (insightSettings.DefaultFrequency.Equals(Enums.NotificationFrequency.Monthly.GetDescription()) &&
                    notification.IsNotified != true)
                    return true;
                if (insightSettings.DefaultFrequency.Equals(Enums.NotificationFrequency.Monthly.GetDescription()) &&
                    DateTime.UtcNow.AddMonths(-1).Date > notification.NotifiedDateTime.Date)
                    return true;
                if (insightSettings.DefaultFrequency.Equals(Enums.NotificationFrequency.Immediate.GetDescription()) &&
                    notification.NotifiedDateTime.Date < Convert.ToDateTime(evaluation.BillCycleStartDate).Date)
                    return true;
                if (insightSettings.DefaultFrequency.Equals(Enums.NotificationFrequency.Immediate.GetDescription()) &&
                        Convert.ToDateTime(evaluation.BillCycleStartDate).Date <= notification.NotifiedDateTime.Date &&
                        notification.NotifiedDateTime.Date <= Convert.ToDateTime(evaluation.BillCycleEndDate).Date)
                {
                    if (notification.IsNotified != true) return true;

                    if (!notification.Insight.Equals("ServiceLevelTieredThresholdApproaching", StringComparison.InvariantCultureIgnoreCase)) return false;
                    return !tier.Equals(notification.Tier, StringComparison.InvariantCultureIgnoreCase);
                }
                return false;
            }
            return false;
        }

        private Dictionary<string, List<string>> GetEmailSubstitutions(ClientSettings clientSettings,InsightModel evaluation, CustomInsightModel customInsightModel,
            CustomerModel customerModel, BillingModel billingModel, List<TierBoundary> tierBoundaries, CalculationModel calculation, int unmaskedAccountIdEndingDigit, IList<ClientUOM> uomContentList, List<ClientTextContent> textContentList)
        {
            var identifiers = new Dictionary<string, List<string>>();
            switch (customInsightModel.InsightName)
            {
                case "BillToDate":
                    if (customInsightModel.IsAccountLevel)
                        identifiers = GetBillToDateEmailSubstitutions(clientSettings, customerModel, calculation, calculation.AsOfAmiDate.HasValue ? calculation.AsOfAmiDate.Value : calculation.AsOfCalculationDate, unmaskedAccountIdEndingDigit);
                    break;
                case "CostToDate":
                    if (!customInsightModel.IsAccountLevel)
                        identifiers = GetCostToDateEmailSubstitutions(clientSettings,customerModel, calculation, billingModel, calculation.AsOfAmiDate.HasValue ? calculation.AsOfAmiDate.Value : calculation.AsOfCalculationDate, unmaskedAccountIdEndingDigit);
                    break;
                case "AccountProjectedCost":
                    if (customInsightModel.IsAccountLevel & evaluation.BtdProjectedCost != null)
                        identifiers = GetBillToDateEmailSubstitutions(clientSettings, customerModel, calculation, calculation.AsOfAmiDate.HasValue ? calculation.AsOfAmiDate.Value : calculation.AsOfCalculationDate, unmaskedAccountIdEndingDigit);
                    break;
                case "ServiceProjectedCost":
                    if (!customInsightModel.IsAccountLevel & evaluation.CtdProjectedCost != null)
                        identifiers = GetCostToDateEmailSubstitutions(clientSettings, customerModel, calculation, billingModel, calculation.AsOfAmiDate.HasValue ? calculation.AsOfAmiDate.Value : calculation.AsOfCalculationDate, unmaskedAccountIdEndingDigit);
                    break;
                case "Usage":
                    if (!customInsightModel.IsAccountLevel & evaluation.Usage != null)
                        identifiers = GetUsageEmailSubstitutions(clientSettings, customerModel, calculation, billingModel,
                            calculation.AsOfAmiDate.HasValue ? calculation.AsOfAmiDate.Value : calculation.AsOfCalculationDate, unmaskedAccountIdEndingDigit, uomContentList, textContentList);
                    break;
                case "AccountLevelCostThreshold":
                    if (customInsightModel.IsAccountLevel & Convert.ToBoolean(evaluation.AccountLevelCostThresholdExceed))
                        identifiers = GetAccountLevelCostThresholdEmailSubstitutions(clientSettings, customerModel, calculation, customInsightModel, calculation.AsOfAmiDate.HasValue ? calculation.AsOfAmiDate.Value : calculation.AsOfCalculationDate, unmaskedAccountIdEndingDigit);
                    break;
                case "ServiceLevelUsageThreshold":
                    if (!customInsightModel.IsAccountLevel &
                        Convert.ToBoolean(evaluation.ServiceLevelUsageThresholdExceed))
                        identifiers = GetServiceLevelUsageThresholdEmailSubstitutions(clientSettings, customerModel, calculation,
                            customInsightModel, billingModel, unmaskedAccountIdEndingDigit, uomContentList,
                            textContentList);
                    break;
                case "ServiceLevelCostThreshold":
                    if (!customInsightModel.IsAccountLevel &
                        Convert.ToBoolean(evaluation.ServiceLevelCostThresholdExceed))
                        identifiers = GetServiceLevelCostThresholdEmailSubstitutions(clientSettings, customerModel, calculation,
                            customInsightModel, billingModel, unmaskedAccountIdEndingDigit, uomContentList,
                            textContentList);
                    break;
                case "DayThreshold":
                    if (!customInsightModel.IsAccountLevel & Convert.ToBoolean(evaluation.DayThresholdExceed))
                        identifiers = GetDayThresholdEmailSubstitutions(customerModel, calculation, customInsightModel,
                            billingModel, calculation.AsOfAmiDate.HasValue ? calculation.AsOfAmiDate.Value : calculation.AsOfCalculationDate, unmaskedAccountIdEndingDigit, uomContentList,
                            textContentList);
                    break;
                case "ServiceLevelTieredThresholdApproaching":
                    if (!customInsightModel.IsAccountLevel &
                        (Convert.ToBoolean(evaluation.ServiceLevelTiered1ThresholdApproaching)
                        || Convert.ToBoolean(evaluation.ServiceLevelTiered2ThresholdApproaching)
                        || Convert.ToBoolean(evaluation.ServiceLevelTiered3ThresholdApproaching)
                        || Convert.ToBoolean(evaluation.ServiceLevelTiered4ThresholdApproaching)
                        || Convert.ToBoolean(evaluation.ServiceLevelTiered5ThresholdApproaching)
                        || Convert.ToBoolean(evaluation.ServiceLevelTiered6ThresholdApproaching)))
                        identifiers = GetServiceLevelTieredThresholdApproachingEmailSubstitutions(customerModel,
                            calculation, billingModel, tierBoundaries, calculation.AsOfAmiDate.HasValue ? calculation.AsOfAmiDate.Value : calculation.AsOfCalculationDate,
                            unmaskedAccountIdEndingDigit, uomContentList, textContentList);
                    break;
                case "ServiceLevelTieredThresholdExceed":
                    if (!customInsightModel.IsAccountLevel &
                        Convert.ToBoolean(evaluation.ServiceLevelTieredThresholdExceed))
                        identifiers = GetServiceLevelTieredThresholdExceedEmailSubstitutions(customerModel, calculation,
                            billingModel, tierBoundaries, calculation.AsOfAmiDate.HasValue ? calculation.AsOfAmiDate.Value : calculation.AsOfCalculationDate, unmaskedAccountIdEndingDigit,
                            uomContentList, textContentList);
                    break;
            }
            return identifiers;
        }

        private string GetSmsBody(ClientSettings clientSettings, InsightModel evaluation, CalculationModel calculation, BillingModel billingModel, CustomInsightModel customInsightModel, string smsTemplateId, int unmaskedAccountIdEndingDigit, IList<ClientUOM> uomContentList, List<ClientTextContent> textContentList)
        {
            var smsTemplateModel = GetSmsTemplateFromDB(evaluation.ClientId, customInsightModel.InsightName, smsTemplateId).Result;
            var substitutes = Regex.Matches(smsTemplateModel.Body, "[-][a-zA-Z]+[-]");
            switch (customInsightModel.InsightName)
            {
                case "BillToDate":
                case "AccountProjectedCost":
                case "AccountLevelCostThreshold":
                    if (customInsightModel.IsAccountLevel)
                        smsTemplateModel.Body = GetReplacedSmsTemplate(clientSettings,smsTemplateModel.Body, calculation, billingModel, customInsightModel, substitutes, unmaskedAccountIdEndingDigit, uomContentList, textContentList);
                    break;
                case "CostToDate":
                case "ServiceProjectedCost":
                case "Usage":
                case "ServiceLevelUsageThreshold":
                case "ServiceLevelCostThreshold":
                case "DayThreshold":
                case "ServiceLevelTieredThresholdApproaching":
                case "ServiceLevelTieredThresholdExceed":
                    if (!customInsightModel.IsAccountLevel)
                        smsTemplateModel.Body = GetReplacedSmsTemplate(clientSettings,smsTemplateModel.Body, calculation, billingModel, customInsightModel, substitutes, unmaskedAccountIdEndingDigit, uomContentList, textContentList);
                    break;
            }
            return smsTemplateModel.Body;
        }

        /// <summary>
        /// Determine if we should generate sms based on the notifytime and the insight
        /// For daily non-threshold SMS alerts like BTD daily, cancel the notification 
        /// </summary>
        /// <param name="estNow"></param>
        /// <param name="smsNotifyTime"></param>
        /// <param name="insight"></param>
        /// <returns></returns>
        private bool GenerateSms(DateTime estNow, DateTime? smsNotifyTime, InsightSettings insight)
        {
            var generateSms = true;

            switch (insight.InsightName)
            {
                case "BillToDate":
                case "AccountProjectedCost":
                case "AccountLevelCostThreshold":
                case "CostToDate":
                case "ServiceProjectedCost":
                    if (
                        insight.DefaultFrequency.Equals(Enums.NotificationFrequency.Daily.GetDescription(),
                            StringComparison.CurrentCultureIgnoreCase) && smsNotifyTime != null && smsNotifyTime < estNow)
                        generateSms = false;
                    break;
            }
            return generateSms;
        }

        public async Task DeleteNotificationAsync(int clientId, string customerId, string accountId)
        {
            await _notificationRepository.DeleteNotification(clientId, customerId, accountId);
        }

        public async Task DeletePreviousLatestNotification(InsightModel evaluation,string insightName, string channel)
        {
            // get the latest notification from previous run
            var latestNotification =
                await
                    _notificationRepository.GetNotification(evaluation,
                        new InsightSettings {InsightName = insightName, DefaultCommunicationChannel = channel});

            if (latestNotification != null)
            {
                await _notificationRepository.DeleteNotification(latestNotification);

            }
        }
    }
}
