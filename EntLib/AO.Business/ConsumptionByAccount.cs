﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using AO.BusinessContracts;
using AO.DataAccess.Contracts;
using AO.Entities;
using AutoMapper;
using CE.AO.Models;

namespace AO.Business
{
    /// <summary>
    /// ConsumptionByAccount class is used to insert/update the ConsumptionByAccountModel to the database
    /// </summary>
    public class ConsumptionByAccount : IConsumptionByAccount
    {
        public ConsumptionByAccount(LogModel logModel, Lazy<ICassandraRepository> cassandraRepository)
        {
            LogModel = logModel;
            CassandraRepository = cassandraRepository;
        }

        public LogModel LogModel { get; set; }

        private Lazy<ICassandraRepository> CassandraRepository { get; }

        /// <summary>
        /// To get the ConsumptionByAccountModel from the database where AmiTimeStamp greater then or equal to startDateTime 
        /// and AmiTimeStamp less than endDateTime. startDateTime and endDateTime are UTC times.
        /// </summary>
        public async Task<IList<ConsumptionByAccountModel>> GetConsumptionByAccountByDateRangeAsync(int clientId,
            string accountId, DateTime startDateTime, DateTime endDateTime)
        {
            IList<ConsumptionByAccountEntity> entities =
                await CassandraRepository.Value.GetAsync<ConsumptionByAccountEntity>(
                    Constants.CassandraTableNames.ConsumptionByAccount,
                    c =>
                        c.ClientId == clientId && c.AccountNumber == accountId && c.AmiTimeStamp >= startDateTime &&
                        c.AmiTimeStamp < endDateTime);

            return Mapper.Map<IList<ConsumptionByAccountEntity>, IList<ConsumptionByAccountModel>>(entities);
        }

        /// <summary>
        /// To insert/update the ConsumptionByAccountModel to the database
        /// </summary>
        /// <param name="model">ConsumptionByAccountModel which is to be inserted/updated</param>
        /// <returns>True</returns>
        public async Task<bool> InsertOrMergeConsumptionByAccountAsync(ConsumptionByAccountModel model)
        {
            var entity = Mapper.Map<ConsumptionByAccountModel, ConsumptionByAccountEntity>(model);
            await CassandraRepository.Value.InsertOrUpdateAsync(entity, Constants.CassandraTableNames.ConsumptionByAccount).ConfigureAwait(false);
            return await Task.Factory.StartNew(() => true);
        }

        /// <summary>
        /// To delete the ConsumptionByAccountModel to the database
        /// </summary>
        /// <param name="model">ConsumptionByAccountModel which is to be deleted</param>
        /// <returns>True</returns>
        public async Task<bool> DeleteConsumptionByAccountAsync(ConsumptionByAccountModel model)
        {
            await
                CassandraRepository.Value.DeleteAsync<ConsumptionByAccountEntity>(
                    Constants.CassandraTableNames.ConsumptionByAccount,
                    p =>
                        p.ClientId == model.ClientId && p.AccountNumber == model.AccountNumber &&
                        p.AmiTimeStamp == model.AmiTimeStamp && p.MeterId == model.MeterId).ConfigureAwait(false);
            return await Task.Factory.StartNew(() => true);
        }
    }
}