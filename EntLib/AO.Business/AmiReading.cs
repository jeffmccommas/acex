﻿using System;
using System.Threading.Tasks;
using AO.BusinessContracts;
using AO.DataAccess.Contracts;
using AO.Entities;
using AutoMapper;
using CE.AO.Models;

namespace AO.Business
{
    /// <summary>
    /// AmiReading class is used to insert/update/fetch ami reading data to/from the database
    /// </summary>
    public class AmiReading : IAmiReading
    {
        public AmiReading(LogModel logModel, Lazy<ICassandraRepository> cassandraRepository)
        {
            LogModel = logModel;
            CassandraRepository = cassandraRepository;
        }

        public LogModel LogModel { get; set; }

        private Lazy<ICassandraRepository> CassandraRepository { get; }

        /// <summary>
        /// To insert/update the NccAmiReadingModel to the database
        /// </summary>
        /// <param name="model">NccAmiReadingModel model to be inserted/updated</param>
        /// <returns>True</returns>
        public async Task<bool> InsertOrMergeAmiReadingAsync(NccAmiReadingModel model)
        {
            var entityModel = Mapper.Map<NccAmiReadingModel, MeterReadingEntity>(model);
            await
                CassandraRepository.Value.InsertOrUpdateAsync(entityModel, Constants.CassandraTableNames.AmiReading)
                    .ConfigureAwait(false);
            return await Task.Factory.StartNew(() => true);
        }

        /// <summary>
        /// To fetch the NccAmiReadingModel model from the database
        /// </summary>
        /// <param name="clientId">Client Id of the model to be fetched</param>
        /// <param name="meterId">Meter Id of the model to be fetched</param>
        /// <param name="amiTimeStamp">Ami time stamp of the model to be fetched</param>
        /// <returns>NccAmiReadingModel model</returns>
        public async Task<NccAmiReadingModel> GetAmiReading(int clientId, string meterId, DateTime amiTimeStamp)
        {
            var amiReading =
                await
                    CassandraRepository.Value.GetSingleAsync<MeterReadingEntity>(
                        Constants.CassandraTableNames.AmiReading,
                        a => a.ClientId == clientId && a.MeterId == meterId && a.AmiTimeStamp == amiTimeStamp);

            return Mapper.Map<MeterReadingEntity, NccAmiReadingModel>(amiReading);
        }

        /// <summary>
        /// Delete Meter Reading from Ami Table
        /// </summary>
        /// <param name="clientId"></param>
        /// <param name="meterId"></param>
        /// <returns></returns>
        public async Task<bool> DeleteAmiReadingAsync(int clientId, string meterId)
        {
            await
                CassandraRepository.Value.DeleteAsync<ClientMeterEntity>(Constants.CassandraTableNames.AmiReading,
                    a => a.ClientId == clientId && a.MeterId == meterId).ConfigureAwait(false);
            return await Task.Factory.StartNew(() => true);
        }
    }
}
