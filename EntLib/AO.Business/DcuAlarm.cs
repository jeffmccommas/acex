﻿using System.Threading.Tasks;
using AO.BusinessContracts;
using AO.DataAccess.Contracts;
using AO.Entities;
using AutoMapper;
using CE.AO.Models;

namespace AO.Business
{
	public class DcuAlarm : IDcuAlarm
	{
		private ICassandraRepository CassandraRepository { get; }

		public DcuAlarm(ICassandraRepository cassandraRepository)
		{
			CassandraRepository = cassandraRepository;
		}

		/// <summary>
		/// To delete the DcuAlarmModel in dcu_alarms table.
		/// </summary>
		/// <param name="model">DcuAlarmModel which is to be deleted</param>
		/// <returns>True</returns>
		
		public async Task DeleteDcuAlarmAsync(DcuAlarmModel model)
		{
		    await CassandraRepository.DeleteAsync<DcuAlarmEntity>(Constants.CassandraTableNames.DcuAlarm,
		            p => p.ClientId == model.ClientId && p.DcuId == model.DcuId && p.AlarmTime == model.AlarmTime &&
		                p.AlarmCode == model.AlarmCode).ConfigureAwait(false);
		}

		/// <summary>
		/// To insert/update the DcuAlarmModel to the database
		/// </summary>
		/// <param name="model">DcuAlarmModel which is to be inserted/updated</param>
		/// <returns>True</returns>
		/// 
		public async Task InsertOrMergeDcuAlarmAsync(DcuAlarmModel model)
		{
			var entity = Mapper.Map<DcuAlarmModel, DcuAlarmEntity>(model);
		    await
		        CassandraRepository.InsertOrUpdateAsync(entity, Constants.CassandraTableNames.DcuAlarm)
		            .ConfigureAwait(false);
		}
	}
}
