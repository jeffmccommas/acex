﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AO.BusinessContracts;
using CE.AO.Logging;
using CE.AO.Models;
using CE.BillToDate;
using CE.RateModel;
using Microsoft.Practices.Unity;
using Utilities = CE.AO.Utilities;

namespace AO.Business
{
    /// <summary>
    /// NotificationFacade class is used to fetch/insert/update the notifiaction details from/to the database and send the notifications through email & sms.
    /// </summary>
    public class NotificationFacade : INotificationFacade
    {
        public NotificationFacade(LogModel logModel)
        {
            LogModel = logModel;
        }

        public LogModel LogModel { get; set; }

        [Dependency]
        public Lazy<INotification> NotificationManager { get; set; }

        [Dependency]
        public Lazy<IEvaluation> EvaluationManager { get; set; }

        [Dependency]
        public Lazy<ISubscription> SubscriptionManager { get; set; }

        [Dependency]
        public Lazy<ICustomer> CustomerManager { get; set; }

        [Dependency]
        public Lazy<IBilling> BillingManager { get; set; }

        [Dependency]
        public Lazy<ICalculation> CalculationManager { get; set; }

        [Dependency]
        public Lazy<IClientConfigFacade> ClientConfigFacadeManager { get; set; }

        [Dependency]
        public Lazy<ICalculationManagerFactory> CalculationManagerFactory { get; set; }

        /// <summary>
        /// Fetch/insert/update the notifiaction details from/to the database and send the notifications through email & sms
        /// </summary>
        /// <param name="clientId">clientId of the data to be fetched</param>
        /// <param name="customerId">customerId of the data to be fetched</param>
        /// <param name="accountId">accountId of the data to be fetched</param>
        /// <param name="serviceContractId">serviceContractId of the data to be fetched</param>
        /// <param name="programName">programName of the data to be fetched</param>
        /// <param name="asOfEvaluationId">unique evaluation id</param>
        public void ProcessNotification(int clientId, string customerId, string accountId, string serviceContractId, string programName, Guid? asOfEvaluationId)
        {
            // Get data Subscription data for customer
            List<SubscriptionModel> subscriptions = SubscriptionManager.Value.GetCustomerSubscriptions(clientId, customerId, accountId).ToList();
			
			// Get data Evaluation data for customer
			var evaluation = string.IsNullOrEmpty(serviceContractId)
				? EvaluationManager.Value.GetAccountLevelEvaluationsAsync(clientId, customerId, accountId, programName).Result
				: EvaluationManager.Value.GetServiceLevelEvaluationsAsync(clientId, customerId, accountId, programName,
					serviceContractId).Result;

			if (subscriptions.Count > 0 && evaluation != null)
            {
                if (asOfEvaluationId != null && evaluation.AsOfEvaluationId != asOfEvaluationId)
                    return;

                var customerModel = CustomerManager.Value.GetCustomerAsync(clientId, customerId).Result;

                var billingModel =
                    BillingManager.Value.GetServiceDetailsFromBillAsync(clientId, customerId, accountId, serviceContractId)
                        .Result;

                var calculationTasks = new List<Task<CalculationModel>>
                {
                    CalculationManager.Value.GetBtdCalculationAsync(clientId, accountId),
                    CalculationManager.Value.GetCtdCalculationAsync(clientId, accountId, serviceContractId)
                };
                Task.WhenAll(calculationTasks);
                List<CalculationModel> calculations = calculationTasks.Where(e => e.Result != null).Select(task => task.Result).ToList();
                Dictionary<string, List<TierBoundary>> tierBoundariesWithServiceContractId =
                    calculations.Where(calculation => !string.IsNullOrEmpty(calculation.ServiceContractId))
                        .ToDictionary(calculation => calculation.ServiceContractId,
                            calculation => GetTierBoundries(calculation, clientId));

                //Get settings from Contentful
                var clientSettings = ClientConfigFacadeManager.Value.GetClientSettings(clientId);
                if (clientSettings == null)
                {
                    Logger.Warn("No settings found for notification.", LogModel);
                    return;
                }

                // sms settings
                var smsApiKey = clientSettings.TrumpiaApiKey;
                var smsUserName = clientSettings.TrumpiaUserName;
                var smsContactList = clientSettings.TrumpiaContactList;

                // create sms subscription if there is sms/emailandsms channel among the insights and no sms subscription id 
                if (subscriptions.Exists(
                    s =>
                        !string.IsNullOrEmpty(s.Channel) &&
                        (s.Channel.Equals(Utilities.Enums.SubscriptionChannel.SMS.GetDescription(),
                            StringComparison.CurrentCultureIgnoreCase) ||
                         s.Channel.Equals(Utilities.Enums.SubscriptionChannel.EmailAndSMS.GetDescription(),
                             StringComparison.CurrentCultureIgnoreCase))) && customerModel != null && string.IsNullOrEmpty(
                                 customerModel.TrumpiaSubscriptionId) && !string.IsNullOrEmpty(customerModel.Phone1))
                {
                    CustomerManager.Value.CreateSubscriptionAndUpdateCustomer(customerModel, smsApiKey, smsUserName,
                        smsContactList);
                }

                NotificationManager.Value.ProcessMessage(subscriptions, evaluation, clientSettings, customerModel,
                    billingModel, tierBoundariesWithServiceContractId, calculations).Wait();
            }
        }

        /// <summary>
        /// Get the tier boundries from database for the client
        /// </summary>
        /// <param name="calculationModel">Calculation for which we want tier boundries</param>
        /// <param name="clientId">Current client id for tier boundries to be fetched</param>
        /// <returns>List of TierBoundary</returns>
        public List<TierBoundary> GetTierBoundries(CalculationModel calculationModel, int clientId)
        {
            //Get BillToDate settings             
            var settings = ClientConfigFacadeManager.Value.GetBillToDateSettings(clientId);

            var rateCompanyId = settings.RateCompanyId;            
            var billToDateCalculator = CalculationManagerFactory.Value.CreateCalculationManager(settings);

            var rateClass = calculationModel.RateClass;
            List<TierBoundary> tierBoundaries = billToDateCalculator.GetTierBoundaries(rateCompanyId, rateClass,
                Convert.ToDateTime(calculationModel.BillCycleStartDate),
                Convert.ToDateTime(calculationModel.BillCycleEndDate));

            // Get BTD setting for conversion
            //Get tire boundries 
            double conversionFactor = 1;
            if (calculationModel.CommodityId == 2 && settings.Settings.General.UseConversionFactorForGas)
            {
                conversionFactor = settings.Settings.General.ConversionFactorForGas;
            }
            else if (calculationModel.CommodityId == 3 && settings.Settings.General.UseConversionFactorForWater)
                conversionFactor = settings.Settings.General.ConversionFactorForWater;
            if (tierBoundaries != null)
            {
                foreach (var tierBoundary in tierBoundaries)
                {
                    tierBoundary.Threshold = tierBoundary.Threshold / conversionFactor;
                }
            }
            return tierBoundaries;
        }
    }
}
