﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using AO.BusinessContracts;
using AO.Entities;
using CE.AO.Logging;
using CE.AO.Models;
using CE.BillToDate;
using CE.RateModel;
using Microsoft.Practices.Unity;
using Enums = CE.AO.Utilities.Enums;

namespace AO.Business
{
    /// <summary>
    /// CalculationFacade class is used to calculate cost to date, calculate bill to date, schedule message and to fetch client accounts.
    /// </summary>
    public class CalculationFacade : ICalculationFacade
    {
        public CalculationFacade() : this(new LogModel { DisableLog = true })
        { }

        public CalculationFacade(LogModel logModel)
        {
            LogModel = logModel;
        }

        public LogModel LogModel { get; set; }

        [Dependency]
        public Lazy<ICalculation> CalculationManager { get; set; }

        [Dependency]
        public Lazy<IBilling> BillingManager { get; set; }

        [Dependency]
        public Lazy<ITallAMI> TallAmiManager { get; set; }

        [Dependency]
        public Lazy<IBillingCycleSchedule> BillingCycleScheduleManager { get; set; }

        [Dependency]
        public Lazy<IAccountLookup> AccountLookupManager { get; set; }

        [Dependency]
        public Lazy<IAccountUpdates> AccountManager { get; set; }

        [Dependency]
        public Lazy<CE.AO.Utilities.IQueueManager> QueueManager { get; set; }

        [Dependency]
        public Lazy<IClientAccount> ClientAccount { get; set; }

        [Dependency]
        public Lazy<ICalculationManagerFactory> CalculationManagerFactory { get; set; }

        [Dependency]
        public Lazy<IClientConfigFacade> ClientConfigFacadeManager { get; set; }

        /// <summary>
        /// To calculate the cost for given date
        /// </summary>
        /// <param name="clientId">Client Id for calculating cost</param>
        /// <param name="meterId">Meter Id for calculating cost</param>
        /// <param name="asOfDate">Date for which cost is to be calculated</param>
        /// <returns>Void</returns>
        public async Task CalculateCostToDate(int clientId, string meterId, DateTime asOfDate)
        {
            LogModel.Module = Enums.Module.CostToDateCalculation;
            var accountMeterDetails = AccountLookupManager.Value.GetAccountMeterDetails(clientId, meterId);

            List<BillingModel> billingDetails = null;
            foreach (var accountMeterDetail in accountMeterDetails)
            {
                billingDetails = accountMeterDetail != null
                ? BillingManager.Value.GetAllServicesFromLastBill(clientId, accountMeterDetail.CustomerId,
                    accountMeterDetail.AccountId).ToList()
                : null;

                if (billingDetails != null)
                    break;
            }
            if(accountMeterDetails != null && accountMeterDetails.Any())
            {
                LogModel.CustomerId = accountMeterDetails[0].CustomerId;
                LogModel.AccountId = accountMeterDetails[0].AccountId;
                LogModel.ServiceContractId = accountMeterDetails[0].ServiceContractId;
            }

            if (billingDetails == null || billingDetails.Count == 0)
            {
                Logger.Warn("Billing information is missing, no calculation is done", LogModel);
                return;
            }

            // Find matching meter from last bill, there will be more than one match when a sewer service contract is in-play for a water meter
            IEnumerable<BillingModel> matchingMeters = billingDetails.Where(e => e.MeterId == meterId && e.MeterType.Equals(Enums.MeterType.ami.ToString(), StringComparison.InvariantCultureIgnoreCase));

            foreach (var meterDetails in matchingMeters)
            {
                // READ BILL-TO-DATE SETTINGS                
                var btdSettings = ClientConfigFacadeManager.Value.GetBillToDateSettings(clientId);
                var rateCompanyId = btdSettings.RateCompanyId;

                BillCycleScheduleModel billCycleSchedule;

                // if bill to date setting's use projected num days is enabled, 
                // then instead of using bill cycle schedule, 
                // use projected num day and the most recent bill's end date + 1 (the start date of current cycle) to compate the cycle with bill info
                if (btdSettings.Settings.General.UseProjectedNumDaysforBilltoDate)
                {
                    var startDate = meterDetails.EndDate.AddDays(1);
                    var endDate = startDate.AddDays(btdSettings.Settings.General.ProjectedNumberOfDays - 1);

                    // exclude cost to date calculation if the computed cycle is in the past; bill to date/cost to date should always be current
                    if (endDate < DateTime.Today.Date)
                    {
                        Logger.Error($"Computed current bill cycle is in the past for meterId {meterId}", LogModel);
                        return;
                    }

                    billCycleSchedule = new BillCycleScheduleModel
                    {
                        BeginDate = startDate.ToString("MM/dd/yyyy"),
                        EndDate = endDate.ToString("MM/dd/yyyy")
                    };

                    Logger.Warn(
                        $"Bill cycle schedule is overrided for meter Id {meterId} Begin Date {billCycleSchedule.BeginDate} End Date{billCycleSchedule.EndDate}.",
                        LogModel);
                }
                else
                {
                    billCycleSchedule = BillingCycleScheduleManager.Value.GetBillingCycleScheduleByDateAsync(clientId,
                        meterDetails.BillCycleScheduleId, asOfDate);

                    if (billCycleSchedule == null)
                    {
                        Logger.Error("BillCycle schedule is missing.", LogModel);
                        return;
                    }
                }

                var isFault = await IsFault(clientId, accountMeterDetails[0], meterDetails);

                // get the account updated beginDate/endDate
                // for shared account, only use the first available one if exists
                foreach (var accountMeterDetail in accountMeterDetails)
                {
                    Task<AccountUpdatesModel> accountUpdateForReadStartDate = AccountManager.Value.GetAccountUpdate(clientId,
                    accountMeterDetail.CustomerId, Enums.AccountUpdateType.ReadStartDate.ToString(),
                    accountMeterDetail.ServiceContractId, false);

                    Task<AccountUpdatesModel> accountUpdateForReadEndDate = AccountManager.Value.GetAccountUpdate(clientId,
                        accountMeterDetail.CustomerId, Enums.AccountUpdateType.ReadEndDate.ToString(),
                        accountMeterDetail.ServiceContractId, false);

                    var tasks = new List<Task<AccountUpdatesModel>>
                    {
                        accountUpdateForReadStartDate,
                        accountUpdateForReadEndDate
                    };

                    await Task.WhenAll(tasks);

                    billCycleSchedule.BeginDate = accountUpdateForReadStartDate.Result == null
                        ? billCycleSchedule.BeginDate
                        : string.IsNullOrEmpty(accountUpdateForReadStartDate.Result.NewValue)
                            ? billCycleSchedule.BeginDate
                            : accountUpdateForReadStartDate.Result.NewValue;

                    billCycleSchedule.EndDate = accountUpdateForReadEndDate.Result == null
                        ? billCycleSchedule.EndDate
                        : string.IsNullOrEmpty(accountUpdateForReadEndDate.Result.NewValue)
                            ? billCycleSchedule.EndDate
                            : accountUpdateForReadEndDate.Result.NewValue;

                    if (accountUpdateForReadStartDate.Result != null || accountUpdateForReadEndDate != null)
                        break;
                }
                

                var clientSettings = ClientConfigFacadeManager.Value.GetClientSettings(clientId);

                //Conversion of local datetime to UTC format
                var clientTimezone = (Enums.Timezone)Enum.Parse(typeof(Enums.Timezone), Convert.ToString(clientSettings.TimeZone));

                var timeZoneInfo = TimeZoneInfo.FindSystemTimeZoneById(clientTimezone.GetDescription());

                var startDateTime = TimeZoneInfo.ConvertTimeToUtc(Convert.ToDateTime(billCycleSchedule.BeginDate), timeZoneInfo);
                var endDateTime = TimeZoneInfo.ConvertTimeToUtc(new DateTime(asOfDate.Year, asOfDate.Month, asOfDate.Day, 23, 59, 59), timeZoneInfo);

                IEnumerable<TallAmiModel> amiList = TallAmiManager.Value.GetAmiList(startDateTime, endDateTime, meterId, accountMeterDetails[0].AccountId, clientId);

                if (amiList == null)
                {
                    Logger.Error($"AMI data is missing for {startDateTime:O} to {endDateTime:O}.", LogModel);
                    return;
                }

                IEnumerable<TallAmiModel> tallAmiModels = amiList as IList<TallAmiModel> ?? amiList.ToList();
				//convert amitimestamp from utc to client's timezone when it is other than UIL(IntervalType != 24)
				tallAmiModels.Where(a => a.IntervalType != 24).ToList().ForEach(a => a.AmiTimeStamp = TimeZoneInfo.ConvertTimeFromUtc(Convert.ToDateTime(a.AmiTimeStamp), timeZoneInfo));
				// ReSharper disable once PossibleMultipleEnumeration
				var firstEntity = tallAmiModels.FirstOrDefault();
                var amiUnitId = 0;
                if (firstEntity != null)
                {
                    amiUnitId = firstEntity.UOMId;
                }

                int noOfDays;
                var fueltype = FuelType.water;
                var commodityType = (Enums.CommodityType)Convert.ToInt32(meterDetails.CommodityId);
                var serviceContractId = accountMeterDetails[0].ServiceContractId;

                if (commodityType == Enums.CommodityType.Gas)
                {
                    fueltype = FuelType.gas;
                }

                if (commodityType == Enums.CommodityType.Electric)
                {
                    fueltype = FuelType.electric;
                }

                if (commodityType == Enums.CommodityType.Sewer)
                {
                    fueltype = FuelType.water;
                    serviceContractId = meterDetails.ServiceContractId; //override here with the sewer SC from the bill where it appears with a water meter
                }

                // ReSharper disable once PossibleMultipleEnumeration
                List<Reading> readings = TallAmiManager.Value.GetReadings(tallAmiModels, out noOfDays);
                DateTime? asOfAmiDate = readings?.OrderByDescending(e => e.Timestamp).FirstOrDefault()?.Timestamp;

                var meter = new Meter(meterId, rateCompanyId, meterDetails.RateClass,
                    fueltype, Convert.ToDateTime(billCycleSchedule.BeginDate), asOfAmiDate.HasValue && asOfAmiDate > DateTime.MinValue ? asOfAmiDate.Value : asOfDate,
                    Convert.ToDateTime(billCycleSchedule.EndDate));

                var billToDateCalculator = CalculationManagerFactory.Value.CreateCalculationManager(btdSettings);

                CostToDateResult ctdResult = null;
                meter.Readings = readings;

                double? usage = meter.Readings?.Select(e => e.Quantity).Sum();

                if (isFault == false)
                {
                    ctdResult = billToDateCalculator.CalculateMeterCostToDate(meter);

                    // log error if there's any
                    if (ctdResult?.ErrCode != CE.RateModel.Enums.ErrorCode.NoError)
                    {
                        Logger.Error($"Error in calculaton error code {ctdResult?.ErrCode.ToString()}", LogModel);
                    }
                }
                else
                {
                    usage = null;
                }

             
                var costToDate = new CalculationModel
                {
                    AccountId = accountMeterDetails[0].AccountId,
                    AsOfAmiDate = Convert.ToDateTime(asOfAmiDate),
                    AsOfCalculationDate = asOfDate,
                    CustomerId = accountMeterDetails[0].CustomerId,
                    BillCycleEndDate = billCycleSchedule.EndDate,
                    BillCycleStartDate = billCycleSchedule.BeginDate,
                    BillDays = noOfDays,
                    ClientId = clientId,
                    MeterId = meterId,
                    ServiceContractId = serviceContractId,
                    CommodityId = Convert.ToInt32(meterDetails.CommodityId),
                    Unit = amiUnitId,
                    Usage = ctdResult?.Usages?.Select(e => e.Quantity).Sum() ?? usage,
                    Cost = ctdResult?.Cost,
                    ProjectedCost = ctdResult?.ProjectedCost,
                    ProjectedUsage = ctdResult?.ProjectedUsages?.Select(e => e.Quantity).Sum(),
                    AverageDailyCost = ctdResult?.Cost / noOfDays,
                    AverageDailyUsage = usage / noOfDays,
                    RateClass = meterDetails.RateClass
                };

                await CalculationManager.Value.InsertCtdCalculationAsync(costToDate).ConfigureAwait(false);

                // Start BTD calculation
                CalculateBillToDate(billingDetails, billCycleSchedule, asOfDate, billToDateCalculator, costToDate, accountMeterDetails[0], btdSettings, clientId);

                //send msg to Evaluation Queue - but not for sewer
                if (commodityType != Enums.CommodityType.Sewer)
                {
                    foreach (var accountMeterDetail in accountMeterDetails)
                    {
                        string message =
                            $"{clientId}^^{accountMeterDetail.CustomerId}^^{accountMeterDetail.AccountId}^^{accountMeterDetail.ServiceContractId}^^{asOfDate.ToShortDateString()}^^{LogModel.Source}^^{LogModel.RowIndex}^^{LogModel.Metadata}^^{(int) LogModel.ProcessingType}";

                        await
                            QueueManager.Value.AddQueueAsync(Constants.QueueNames.AoEvaluationQueue, message)
                                .ConfigureAwait(false);
                    }
                }
            }
        }

        /// <summary>
        /// To check any fault is present
        /// </summary>
        /// <param name="clientId">Client id to be checked for</param>
        /// <param name="accountMeterDetails">AccountLookupModel model</param>
        /// <param name="meterDetails">BillingModel model</param>
        /// <returns>boolean result</returns>
        private async Task<bool> IsFault(int clientId, AccountLookupModel accountMeterDetails, BillingModel meterDetails)
        {
            Task<AccountUpdatesModel> billCycle = AccountManager.Value.GetAccountUpdate(clientId,
                accountMeterDetails.CustomerId, Enums.AccountUpdateType.BillCycle.ToString(),
                accountMeterDetails.ServiceContractId, false);

            Task<AccountUpdatesModel> meterreplacement = AccountManager.Value.GetAccountUpdate(clientId,
                accountMeterDetails.CustomerId, Enums.AccountUpdateType.MeterReplacement.ToString(),
                accountMeterDetails.ServiceContractId, false);

            Task<AccountUpdatesModel> rateclass = AccountManager.Value.GetAccountUpdate(clientId,
                accountMeterDetails.CustomerId, Enums.AccountUpdateType.RateClass.ToString(),
                accountMeterDetails.ServiceContractId, false);

            var tasks = new List<Task<AccountUpdatesModel>> { billCycle, meterreplacement, rateclass };

            await Task.WhenAll(tasks);

            var isFault = (billCycle.Result?.UpdateSentDate != null && meterDetails != null && billCycle.Result.UpdateSentDate.Value.Date > meterDetails.EndDate.Date) ||
                           (meterreplacement.Result?.UpdateSentDate != null &&
                           meterreplacement.Result.UpdateSentDate.Value > meterDetails?.EndDate.Date) ||
                           (rateclass.Result?.UpdateSentDate != null && rateclass.Result.UpdateSentDate.Value.Date > meterDetails?.EndDate.Date);
            if (isFault)
                Logger.Warn("Fault occured either becasue of Bill cycle change Or Meter replacement or rate class change.", LogModel);

            return isFault;
        }

        /// <summary>
        /// To check any fault is present
        /// </summary>
        /// <param name="clientId">Client id to be checked for</param>
        /// <param name="customerId">Customer id to be checked for</param>
        /// <param name="serviceContractId">Service contract id to be checked for</param>
        /// <param name="meterDetails">BillingModel model</param>
        /// <returns>boolean result</returns>
        private async Task<bool> IsFault(int clientId, string customerId, string serviceContractId, BillingModel meterDetails)
        {
            var tasks = new List<Task<AccountUpdatesModel>>
            {
                AccountManager.Value.GetAccountUpdate(clientId, customerId, Enums.AccountUpdateType.BillCycle.ToString(),
                    serviceContractId, false),
                AccountManager.Value.GetAccountUpdate(clientId, customerId,
                    Enums.AccountUpdateType.MeterReplacement.ToString(), serviceContractId, false),
                AccountManager.Value.GetAccountUpdate(clientId, customerId, Enums.AccountUpdateType.RateClass.ToString(),
                    serviceContractId, false)
            };

            await Task.WhenAll(tasks);
            var isFault = (tasks[0].Result?.UpdateSentDate != null && meterDetails != null && tasks[0].Result.UpdateSentDate.Value.Date > meterDetails.EndDate.Date) ||
                           (tasks[1].Result?.UpdateSentDate != null &&
                           tasks[1].Result.UpdateSentDate.Value > meterDetails?.EndDate.Date) ||
                           (tasks[2].Result?.UpdateSentDate != null && tasks[2].Result.UpdateSentDate.Value.Date > meterDetails?.EndDate.Date);
            if (isFault)
                Logger.Warn("Fault occured either becasue of Bill cycle change Or Meter replacement.", LogModel);

            return isFault;
        }

        public static bool AlmostEquals(double double1, double double2, double precision)
        {
            return (Math.Abs(double1 - double2) <= precision);
        }

        /// <summary>
        /// To calculate bill to date and to insert result in the database
        /// </summary>
        /// <param name="billingDetails">List of BillingModel model</param>
        /// <param name="billCycleSchedule">BillCycleScheduleModel model</param>
        /// <param name="asOfDate">Date till </param>
        /// <param name="billToDateCalculator"></param>
        /// <param name="costToDate">CalculationModel model</param>
        /// <param name="accountMeterDetails">AccountLookupModel model</param>
        /// <param name="btdSettings"></param>
        /// <param name="clientId"></param>
        private void CalculateBillToDate(List<BillingModel> billingDetails, BillCycleScheduleModel billCycleSchedule, DateTime asOfDate, IBillToDate billToDateCalculator, CalculationModel costToDate, AccountLookupModel accountMeterDetails, BillToDateSettings btdSettings, int clientId)
        {
            LogModel.Module = Enums.Module.BillToDateCalculation;
            

            var servicesInBill =
             billingDetails.Where(
                 e => e.MeterType == Convert.ToString(Enums.MeterType.ami) && e.ServiceContractId != costToDate.ServiceContractId).ToList();

            var tasks = servicesInBill.Select(service => CalculationManager.Value.GetCtdCalculationAsync(service.ClientId, service.AccountId, service.ServiceContractId)).ToList();
            Task.WhenAll(tasks);

            var ctds = tasks.Where(task => task.Result != null && task.Result.AsOfCalculationDate == asOfDate).Select(e => e.Result).ToList();

            // Calculate BTD
            ctds.Add(costToDate);

            var nmBillDate = ctds.Min(c => Convert.ToDateTime(c.BillCycleStartDate));


            var nonMeteredServiceResult = GetNonMeteredServices(billingDetails, btdSettings, nmBillDate,
                billToDateCalculator, clientId);

            var nonMeterCharges = nonMeteredServiceResult.Select(e => e.Cost).Sum();
            var nonMeterChargesProjected = nonMeteredServiceResult.Select(e => e.ProjectedCost).Sum();

            var amiServiceCount = billingDetails.Count(b => b.MeterType == Convert.ToString(Enums.MeterType.ami));
            var costToDateCount = ctds.Count(c => c.Cost != null);

            var totalCtdCost = ctds.Select(e => e.Cost ?? 0).Sum();
            var totalProjectedCtdCost = ctds.Select(e => e.ProjectedCost ?? 0).Sum();
            var totalBtd = totalCtdCost + nonMeterCharges;
            var projectedBtd = totalProjectedCtdCost + nonMeterChargesProjected;
            var asOfAmiDate = ctds.Select(e => e.AsOfAmiDate).Max();


            var noOfBillDays =
                ctds.Select(e => (Convert.ToDateTime(e.AsOfAmiDate) - Convert.ToDateTime(e.BillCycleStartDate)).Days + 1)
                    .Max();

            var maxBillCycleDays =
                ctds.Select(
                    e =>
                        Math.Floor(
                            (Convert.ToDateTime(e.BillCycleEndDate) - Convert.ToDateTime(e.BillCycleStartDate))
                                .TotalDays + 1)).Max();
            var startDate = asOfAmiDate != null ? (Convert.ToDateTime(asOfAmiDate).Date.AddDays(-1 * noOfBillDays + 1).Date).ToString("MM-dd-yyyy") : billCycleSchedule.BeginDate;
            var endDate = asOfAmiDate != null ? (Convert.ToDateTime(startDate).AddDays(maxBillCycleDays - 1).Date).ToString("MM-dd-yyyy") : billCycleSchedule.EndDate;


            // ReSharper disable once CompareOfFloatsByEqualityOperator
            if (AlmostEquals(totalCtdCost, 0, 0.000001)) // If for some reason calculations are not performered then Bill days = current date- Schedule Begin Date 
            {
                noOfBillDays = (Convert.ToDateTime(asOfDate) - Convert.ToDateTime(billCycleSchedule.BeginDate)).Days + 1;
            }
            
            var billToDate = new CalculationModel
            {
                AccountId = accountMeterDetails.AccountId,
                CustomerId = accountMeterDetails.CustomerId,
                BillCycleEndDate = endDate,
                BillCycleStartDate = startDate,
                AsOfCalculationDate = asOfDate,
                AsOfAmiDate = asOfAmiDate,
                BillDays = noOfBillDays, //Max (AMI Date-beginDate)
                ClientId = Convert.ToInt32(accountMeterDetails.ClientId),
                Cost = costToDateCount == amiServiceCount ?  totalBtd : 0,
                ProjectedCost = costToDateCount == amiServiceCount ? projectedBtd : 0,
                AverageDailyCost = totalBtd / noOfBillDays,
                ServiceContractId= "" //Cannot assigne null values to ServiceContractId, because ServiceContractId is clustring cloumn in cassandra
            };

            CalculationManager.Value.InsertBtdCalculationAsync(billToDate).Wait();
        }

        /// <summary>
        /// To calculate bill to date, cost to date and insert into the database
        /// </summary>
        /// <param name="clientId">Client id for which bill is to be calculated</param>
        /// <param name="accountId">Account id for which bill is to be calculated</param>
        /// <param name="customerId">customer id for which bill is to be calculated</param>
        /// <param name="asOfDate">date for which bill is to be calculated</param>
        /// <returns>Void</returns>
        public async Task CalculateBillToDate(int clientId, string accountId, string customerId, DateTime asOfDate)
        {
            LogModel.Module = Enums.Module.BillToDateCalculation;
            List<BillingModel> billingDetails = BillingManager.Value.GetAllServicesFromLastBill(clientId, customerId, accountId).ToList();

            LogModel.CustomerId = customerId;
            LogModel.AccountId = accountId;

            if (billingDetails.Count == 0)
            {
                Logger.Warn("Billing information is missing.", LogModel);
                return;
            }

            var billCycleScheduleStartDate = "";
            var billCycleScheduleEndDate = "";

            var meterServiceList = new List<MeteredService>();

            var meterUsage = new Dictionary<string, dynamic>();

            // READ BILL-TO-DATE SETTINGS            
            var btdSettings = ClientConfigFacadeManager.Value.GetBillToDateSettings(clientId);
            var rateCompanyId = btdSettings.RateCompanyId;
            var billToDateCalculator = CalculationManagerFactory.Value.CreateCalculationManager(btdSettings);

            // ReSharper disable once SimplifyLinqExpression
            if (!billingDetails.Any(b => b.MeterType == Convert.ToString(Enums.MeterType.ami)))
            {
                Logger.Warn("Metered Services are missing.", LogModel);
                return;
            }

            foreach (var meterDetails in billingDetails)
            {
                var meteredService = new MeteredService();
                if (meterDetails.MeterType != Convert.ToString(Enums.MeterType.ami)) continue;

                BillCycleScheduleModel billCycleSchedule;
                // if bill to date setting's use projected num days is enabled, 
                // then instead of using bill cycle schedule, 
                // use projected num day and the most recent bill's end date + 1 (the start date of current cycle) to compate the cycle with bill info
                if (btdSettings.Settings.General.UseProjectedNumDaysforBilltoDate)
                {
                    var startDate = meterDetails.EndDate.AddDays(1);
                    var endDate = startDate.AddDays(btdSettings.Settings.General.ProjectedNumberOfDays - 1);

                    // exclude cost to date calculation if the computed cycle is in the past; bill to date/cost to date should always be current
                    if (endDate < DateTime.Today.Date)
                    {
                        Logger.Warn($"Computed current bill cycle is in the past for meterId {meterDetails.MeterId}.", LogModel);
                        return;
                    }

                    billCycleSchedule = new BillCycleScheduleModel
                    {
                        BeginDate = startDate.ToString("MM/dd/yyyy"),
                        EndDate = endDate.ToString("MM/dd/yyyy")
                    };

                    Logger.Warn(
                        $"Bill cycle schedule is overrided for meter Id {meterDetails.MeterId} Begin Date {billCycleSchedule.BeginDate} End Date {billCycleSchedule.EndDate}.",
                        LogModel);
                }
                else
                {
                    billCycleSchedule = BillingCycleScheduleManager.Value.GetBillingCycleScheduleByDateAsync(clientId,
                        meterDetails?.BillCycleScheduleId, asOfDate);

                    if (billCycleSchedule == null)
                    {
                        Logger.Warn("BillCycle schedule is missing.", LogModel);
                        return;
                    }
                }
               
                billCycleScheduleStartDate = billCycleSchedule.BeginDate;
                billCycleScheduleEndDate = billCycleSchedule.EndDate;

                var isFault = await IsFault(clientId, customerId, meterDetails.ServiceContractId, meterDetails);

                if (isFault) continue;

                var tasks = new List<Task<AccountUpdatesModel>>
                {
                    AccountManager.Value.GetAccountUpdate(clientId,
                        customerId, Enums.AccountUpdateType.ReadStartDate.ToString(),
                        meterDetails.ServiceContractId, false),
                    AccountManager.Value.GetAccountUpdate(clientId,
                        customerId, Enums.AccountUpdateType.ReadEndDate.ToString(),
                        meterDetails.ServiceContractId, false)
                };

                await Task.WhenAll(tasks);
                var accountUpdateForReadStartDate = tasks[0].Result;
                var accountUpdateForReadEndDate = tasks[1].Result;

                billCycleScheduleStartDate = accountUpdateForReadStartDate == null
                    ? billCycleScheduleStartDate
                    : string.IsNullOrEmpty(accountUpdateForReadStartDate.NewValue)
                        ? billCycleScheduleStartDate
                        : accountUpdateForReadStartDate.NewValue;

                billCycleScheduleEndDate = accountUpdateForReadEndDate == null
                    ? billCycleScheduleEndDate
                    : string.IsNullOrEmpty(accountUpdateForReadEndDate.NewValue)
                        ? billCycleScheduleEndDate
                        : accountUpdateForReadEndDate.NewValue;


                var clientSettings = ClientConfigFacadeManager.Value.GetClientSettings(clientId);

                //Conversion of local datetime to UTC format
                var clientTimezone = (Enums.Timezone)Enum.Parse(typeof(Enums.Timezone), Convert.ToString(clientSettings.TimeZone));

                var timeZoneInfo = TimeZoneInfo.FindSystemTimeZoneById(clientTimezone.GetDescription());

                var startDateTime = TimeZoneInfo.ConvertTimeToUtc(Convert.ToDateTime(billCycleScheduleStartDate), timeZoneInfo);
                var endDateTime = TimeZoneInfo.ConvertTimeToUtc(new DateTime(asOfDate.Year, asOfDate.Month, asOfDate.Day, 23, 59, 59), timeZoneInfo);

                IEnumerable<TallAmiModel> amiList = TallAmiManager.Value.GetAmiList(startDateTime, endDateTime,
                    meterDetails.MeterId, accountId, clientId);

                if (amiList == null)
                {
                    Logger.Warn("AMI data is missing.", LogModel);
                    return;
                }

                IEnumerable<TallAmiModel> tallAmiModels = amiList as IList<TallAmiModel> ?? amiList.ToList();
				//convert amitimestamp from utc to client's timezone when it is other than UIL(IntervalType != 24)
				tallAmiModels.Where(a => a.IntervalType != 24).ToList().ForEach(a => a.AmiTimeStamp = TimeZoneInfo.ConvertTimeFromUtc(Convert.ToDateTime(a.AmiTimeStamp), timeZoneInfo));

				var firstEntity = tallAmiModels.FirstOrDefault();
                var amiUnitId = 0;
                if (firstEntity != null)
                {
                    amiUnitId = firstEntity.UOMId;
                }

                var fueltype = FuelType.water;
                var commodityType = (Enums.CommodityType)Convert.ToInt32(meterDetails?.CommodityId);
                var serviceContractId = meterDetails.ServiceContractId;

                if (commodityType == Enums.CommodityType.Gas)
                {
                    fueltype = FuelType.gas;
                }

                if (commodityType == Enums.CommodityType.Electric)
                {
                    fueltype = FuelType.electric;
                }

                if (commodityType == Enums.CommodityType.Sewer)
                {
                    fueltype = FuelType.water;
                    serviceContractId = meterDetails.ServiceContractId;
                }

                int noOfDays;
                List<Reading> readingsforMeter = TallAmiManager.Value.GetReadings(tallAmiModels, out noOfDays);
                var meter = new Meter(meterDetails.MeterId, rateCompanyId, meterDetails.RateClass, fueltype,
                    Convert.ToDateTime(billCycleScheduleStartDate), asOfDate,
                    Convert.ToDateTime(billCycleScheduleEndDate), readingsforMeter);
                meteredService.Meters.Add(meter);
                meterServiceList.Add(meteredService);
                double? usage = meter.Readings?.Select(e => e.Quantity).Sum();
                DateTime? asOfAmiDate = meter.Readings?.OrderByDescending(e => e.Timestamp).FirstOrDefault()?.Timestamp;

                meterUsage.Add(meter.MeterID + "_" + serviceContractId,
                    new
                    {
                        NoOfDays = noOfDays,
                        Usage = usage,
                        AsOfAmiDate = asOfAmiDate,
                        meterDetails.ServiceContractId,
                        AmiUnitID = amiUnitId
                    });
            }

            // create a bill-to-date bill
            var bill = new Bill(Convert.ToDateTime(billCycleScheduleStartDate), clientId, customerId, accountId,
                rateCompanyId);
            bill.MeteredServices.AddRange(meterServiceList);
            List<NonMeteredService> nonmeteredServices = GetNonMeteredServicesList(billingDetails, billCycleScheduleStartDate,
                billCycleScheduleEndDate, asOfDate);
            bill.NonMeteredServices.AddRange(nonmeteredServices);

            var result = billToDateCalculator.CalculateBillToDate(bill);

            IEnumerable<Meter> metersResults = new List<Meter>();
            if (result?.Bill != null)
            {
                metersResults = result.Bill.MeteredServices.SelectMany(p => p.Meters.ToList()).ToList();
            }

            // log error if there's any
            if (result != null && result.StatusList.HasStatusSeverity(StatusTypes.StatusSeverity.Error))
            {
                foreach (var meterResult in metersResults)
                {
                    if(meterResult.CostToDateResult.ErrCode != CE.RateModel.Enums.ErrorCode.NoError)
                    {
                        Logger.Warn($"Error in calculaton error code {meterResult.CostToDateResult.ErrCode.ToString()}", LogModel);
                    }
                }
            }

            List<CalculationModel> listOfCalculationModel;
            if (metersResults.Any())
            {
                listOfCalculationModel = (from meterdetails in billingDetails
                                          join meter in metersResults on meterdetails.MeterId equals meter.MeterID
                                          where (meterdetails.MeterType == Convert.ToString(Enums.MeterType.ami) && meterdetails.RateClass == meter.RateClass)
                                          select new CalculationModel
                                          {
                                              AccountId = accountId,
                                              AsOfAmiDate = meterUsage[meterdetails.MeterId + "_" + meterdetails.ServiceContractId].AsOfAmiDate,
                                              AsOfCalculationDate = asOfDate,
                                              CustomerId = customerId,
                                              BillCycleEndDate = billCycleScheduleEndDate,
                                              BillCycleStartDate = billCycleScheduleStartDate,
                                              BillDays = meterUsage[meterdetails.MeterId + "_" + meterdetails.ServiceContractId].NoOfDays,
                                              ClientId = clientId,
                                              MeterId = meterdetails.MeterId,
                                              ServiceContractId = meterUsage[meterdetails.MeterId + "_" + meterdetails.ServiceContractId].ServiceContractId,
                                              CommodityId = meterdetails.CommodityId,
                                              Unit = meterUsage[meterdetails.MeterId + "_" + meterdetails.ServiceContractId].AmiUnitID, //enum UomType
                                              Usage = meterUsage[meterdetails.MeterId + "_" + meterdetails.ServiceContractId].Usage,
                                              RateClass = meterdetails.RateClass,
                                              Cost = meter.CostToDateResult.Cost,
                                              ProjectedCost = meter.CostToDateResult.ProjectedCost,
                                              ProjectedUsage = meter.CostToDateResult.ProjectedUsages?.Select(e => e.Quantity).Sum(),
                                              AverageDailyCost = meter.CostToDateResult.Cost / meterUsage[meterdetails.MeterId + "_" + meterdetails.ServiceContractId].NoOfDays,
                                              AverageDailyUsage =
                                                  meterUsage[meterdetails.MeterId + "_" + meterdetails.ServiceContractId].Usage / meterUsage[meterdetails.MeterId + "_" + meterdetails.ServiceContractId].NoOfDays
                                          }).ToList();
            }
            else
            {
                listOfCalculationModel = (from meterdetails in billingDetails
                                          where meterdetails.MeterType == Convert.ToString(Enums.MeterType.ami)
                                          select new CalculationModel
                                          {
                                              AccountId = accountId,
                                              AsOfAmiDate = meterUsage[meterdetails.MeterId + "_" + meterdetails.ServiceContractId].AsOfAmiDate,
                                              AsOfCalculationDate = asOfDate,
                                              CustomerId = customerId,
                                              BillCycleEndDate = billCycleScheduleEndDate,
                                              BillCycleStartDate = billCycleScheduleStartDate,
                                              BillDays = meterUsage[meterdetails.MeterId + "_" + meterdetails.ServiceContractId].NoOfDays,
                                              ClientId = clientId,
                                              MeterId = meterdetails.MeterId,
                                              ServiceContractId = meterUsage[meterdetails.MeterId + "_" + meterdetails.ServiceContractId].ServiceContractId,
                                              CommodityId = meterdetails.CommodityId,
                                              Unit = meterUsage[meterdetails.MeterId + "_" + meterdetails.ServiceContractId].AmiUnitID, //enum UomType
                                              Usage = meterUsage[meterdetails.MeterId + "_" + meterdetails.ServiceContractId].Usage,
                                              RateClass = meterdetails.RateClass,
                                              Cost = 0,
                                              ProjectedCost = 0,
                                              ProjectedUsage = 0,
                                              AverageDailyCost = 0,
                                              AverageDailyUsage =
                                                  meterUsage[meterdetails.MeterId + "_" + meterdetails.ServiceContractId].Usage / meterUsage[meterdetails.MeterId + "_" + meterdetails.ServiceContractId].NoOfDays
                                          }).ToList();
            }
            // ReSharper disable once LoopCanBeConvertedToQuery
            foreach (var calculationModel in listOfCalculationModel)
            {
                await CalculationManager.Value.InsertCtdCalculationAsync(calculationModel).ConfigureAwait(false);
            }

            DateTime? maxAsOfAmiDate = listOfCalculationModel.Select(e => e.AsOfAmiDate).Max();
            int? noOfBillDaysForBtd = listOfCalculationModel.Select(e => (Convert.ToDateTime(e.AsOfAmiDate) - Convert.ToDateTime(e.BillCycleStartDate)).Days + 1)
                    .Max();

            var totalCtdCost = listOfCalculationModel.Select(e => e.Cost ?? 0).Sum();
            if (Math.Abs(totalCtdCost) <= 0)
            {
                // If for some reason calculations are not performered then Bill days = current date- Schedule Begin Date 
                noOfBillDaysForBtd =
                    (Convert.ToDateTime(asOfDate) - Convert.ToDateTime(billCycleScheduleStartDate)).Days + 1;
            }

            var maxBillCycleDays =
                listOfCalculationModel.Select(
                    e =>
                        Math.Floor(
                            (Convert.ToDateTime(e.BillCycleEndDate) - Convert.ToDateTime(e.BillCycleStartDate))
                                .TotalDays + 1)).Max();
            billCycleScheduleStartDate = maxAsOfAmiDate != null ? (Convert.ToDateTime(maxAsOfAmiDate).Date.AddDays(-1 * noOfBillDaysForBtd.Value + 1).Date).ToString("MM-dd-yyyy") : billCycleScheduleStartDate;
            billCycleScheduleEndDate = maxAsOfAmiDate != null ? (Convert.ToDateTime(billCycleScheduleStartDate).AddDays(maxBillCycleDays - 1).Date).ToString("MM-dd-yyyy") : billCycleScheduleEndDate;


            var billToDate = new CalculationModel()
            {
                AccountId = accountId,
                CustomerId = customerId,
                BillCycleEndDate = billCycleScheduleEndDate,
                BillCycleStartDate = billCycleScheduleStartDate,
                AsOfCalculationDate = asOfDate,
                AsOfAmiDate = maxAsOfAmiDate,
                BillDays = noOfBillDaysForBtd,
                ClientId = clientId,
                Cost = result.TotalCost,
                ProjectedCost = result.TotalProjectedCost,
                AverageDailyCost = result.TotalCost / noOfBillDaysForBtd,
                ServiceContractId = ""
            };

            await CalculationManager.Value.InsertBtdCalculationAsync(billToDate).ConfigureAwait(false);
            //send msg to Evaluation Queue
            string message =
                $"{clientId}^^{customerId}^^{accountId}^^{string.Empty}^^{asOfDate.ToShortDateString()}^^{LogModel.Source}^^{LogModel.RowIndex}^^{LogModel.Metadata}^^{(int)LogModel.ProcessingType}";

            await QueueManager.Value.AddQueueAsync(Constants.QueueNames.AoEvaluationQueue, message).ConfigureAwait(false);
        }

        /// <summary>
        /// To return list of non-metered services
        /// </summary>
        /// <param name="billingDetails">List of BillingModel models for which services to be fetched</param>
        /// <param name="btdSettings"></param>
        /// <param name="asOfDate">Date for which services to be fetched</param>
        /// <param name="billToDateCalculator"></param>
        /// <param name="clientId"></param>
        /// <returns>List of NonMeteredServiceResult model</returns>
        private List<NonMeteredServiceResult> GetNonMeteredServices(IEnumerable<BillingModel> billingDetails, BillToDateSettings btdSettings, DateTime asOfDate, IBillToDate billToDateCalculator, int clientId)
        {
            var nonMeterServices =
                billingDetails.Where(e => e.MeterType.ToLower(CultureInfo.InvariantCulture) == "nonmetered");
            var nonMeteredServiceResult = new List<NonMeteredServiceResult>();
            // ReSharper disable once LoopCanBeConvertedToQuery
            foreach (var nonMeterService in nonMeterServices)
            {
                // check if there's non meter service from last bill; if too old, exclude it
                var lastBillStartDate = asOfDate.AddDays(-1*(nonMeterService.BillDays));

                if(nonMeterService.StartDate > lastBillStartDate && nonMeterService.EndDate < asOfDate)
                {
                    BillCycleScheduleModel billCycleSchedule;
                    if (btdSettings.Settings.General.UseProjectedNumDaysforBilltoDate)
                    {
                        var endDate = asOfDate.AddDays(btdSettings.Settings.General.ProjectedNumberOfDays - 1);

                        // exclude cost to date calculation if the computed cycle is in the past; bill to date/cost to date should always be current
                        if (endDate < DateTime.Today.Date)
                        {
                            Logger.Warn($"Computed current bill cycle is in the past for non meter commodity {((Enums.CommodityType)nonMeterService.CommodityId)}.", LogModel);
                            break;
                        }

                        billCycleSchedule = new BillCycleScheduleModel
                        {
                            BeginDate = asOfDate.ToString("MM-dd-yyyy"),
                            EndDate = endDate.ToString("MM-dd-yyyy")
                        };

                        Logger.Warn(
                            $"Bill cycle schedule is overrided for non meter commodity {((Enums.CommodityType)nonMeterService.CommodityId)} Begin Date {billCycleSchedule.BeginDate} End Date {billCycleSchedule.EndDate}.",
                            LogModel);
                    }
                    else
                    {
                        billCycleSchedule = BillingCycleScheduleManager.Value.GetBillingCycleScheduleByDateAsync(clientId,
                            nonMeterService.BillCycleScheduleId, asOfDate);

                        if (billCycleSchedule == null)
                        {
                            Logger.Warn("BillCycle schedule is missing.", LogModel);
                            break;
                        }
                    }

                    var nms = new NonMeteredService(((Enums.CommodityType)nonMeterService.CommodityId).ToString(),
                        nonMeterService.TotalCost, nonMeterService.TotalUsage ?? 0,
                        nonMeterService.UOMId == null
                            ? Enums.UomType.Other.ToString().ToLower(CultureInfo.InvariantCulture)
                            : ((Enums.UomType)nonMeterService.UOMId).ToString(),
                        Convert.ToDateTime(billCycleSchedule.BeginDate), DateTime.Today.Date);

                    if (!btdSettings.Settings.General.UseProjectedNumDaysforBilltoDate)
                    {
                        nms.ProjectedEndDate = Convert.ToDateTime(billCycleSchedule.EndDate);
                    }

                    nonMeteredServiceResult.Add(billToDateCalculator.CalculateNonMeteredServiceCostToDate(nms));
                }
            }

            return nonMeteredServiceResult;
        }

        /// <summary>
        /// To fetch the list of non-metered services
        /// </summary>
        /// <param name="billingDetails">List of BillingModel model</param>
        /// <param name="beginDate">Start date from which list to be fetched</param>
        /// <param name="endDate">Projected end date</param>
        /// <param name="asOfDate">End date to which list to be fetched</param>
        /// <returns>List of NonMeteredServices model</returns>
        private List<NonMeteredService> GetNonMeteredServicesList(IEnumerable<BillingModel> billingDetails, string beginDate, string endDate, DateTime asOfDate)
        {
            IEnumerable<BillingModel> nonMeterServices = billingDetails.Where(e => e.MeterType.ToLower(CultureInfo.InvariantCulture) == "nonmetered");
            var nonMeteredServiceList = new List<NonMeteredService>();

            // ReSharper disable once LoopCanBeConvertedToQuery
            foreach (var nonMeterService in nonMeterServices)
            {
                // check if there's non meter service from last bill; if too old, exclude it
                var lastBillStartDate = asOfDate.AddDays(-1 * (nonMeterService.BillDays));

                if (nonMeterService.StartDate > lastBillStartDate && nonMeterService.EndDate < asOfDate)
                {
                    var nms = new NonMeteredService(((Enums.CommodityType) nonMeterService.CommodityId).ToString(),
                        nonMeterService.TotalCost, Convert.ToDateTime(beginDate), asOfDate, Convert.ToDateTime(endDate));
                    nonMeteredServiceList.Add(nms);
                }
            }
            return nonMeteredServiceList;
        }

        /// <summary>
        /// To send scheduled messages
        /// </summary>
        /// <param name="clientId">Client id for which message to be sent</param>
        /// <param name="asOfDate">Date for which message to be sent</param>
        public void ScheduledCalculation(int clientId, string asOfDate)
        {
            ClientAccount.Value.SendScheduleMessage(clientId, asOfDate);
        }

        /// <summary>
        /// To fetch the ClientAccountModels
        /// </summary>
        /// <param name="clientId">Client id of the ClientAccountModel</param>
        /// <returns>List of ClientAccountModel model</returns>
        public IEnumerable<ClientAccountModel> GetClientAccount(int clientId)
        {
            return ClientAccount.Value.GetClientAccounts(clientId);
        }

    }
}