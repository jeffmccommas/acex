﻿using System.Threading.Tasks;
using AO.BusinessContracts;
using AO.DataAccess.Contracts;
using AO.Entities;
using AutoMapper;
using CE.AO.Models;

namespace AO.Business
{
    public class DcuAlarmType : IDcuAlarmType
    {
        private readonly ICassandraRepository _cassandraRepository;
        private readonly ICacheProvider _cacheProvider;

        public DcuAlarmType(ICassandraRepository cassandraRepository, ICacheProvider cacheProvider)
        {
            _cassandraRepository = cassandraRepository;
            _cacheProvider = cacheProvider;
        }

        /// <summary>
        /// Get DcuAlarmMapping information of the specific code from dcu_alarms_mapping table
        /// </summary>
        /// <param name="alarmCode"> alarmCode from which Mapping is fetched  </param>
        /// <returns>True</returns>
        public DcuAlarmTypeModel GetDcuAlarmType(string alarmCode)
        {
            DcuAlarmTypeEntity dcuAlarmDetails;
            var keyValue = _cacheProvider.Get<DcuAlarmTypeEntity>($"{alarmCode}", (int)BusinessContracts.Enums.CacheDatabases.DcuAlarmTypesDatabaseId);
            if (keyValue == null)
            {
                dcuAlarmDetails =
                    _cassandraRepository.GetSingle<DcuAlarmTypeEntity>(Constants.CassandraTableNames.DcuAlarmType,
                        a => a.TableName == Constants.CassandraTableNames.DcuAlarmType &&  a.AlarmCode == alarmCode);

                if (dcuAlarmDetails != null)
                    _cacheProvider.Set($"{alarmCode}", dcuAlarmDetails, (int)BusinessContracts.Enums.CacheDatabases.DcuAlarmTypesDatabaseId);

            }
            else
            {
                dcuAlarmDetails = keyValue;
            }
            return Mapper.Map<DcuAlarmTypeEntity, DcuAlarmTypeModel>(dcuAlarmDetails);
        }

        /// <summary>
        /// To insert/update the DcuAlarmMappingModel to database
        /// </summary>
        /// <param name="model">DcuAlarmMappingModel which is to be inserted/updated</param>
        /// <returns>True</returns>
        /// 
        public async Task InsertOrMergeDcuAlarmTypeAsync(DcuAlarmTypeModel model)
        {
            var entity = Mapper.Map<DcuAlarmTypeModel, DcuAlarmTypeEntity>(model);
            await _cassandraRepository.InsertOrUpdateAsync(entity, Constants.CassandraTableNames.DcuAlarmType).ConfigureAwait(false);

            if(entity != null)
                _cacheProvider.Set($"{model.AlarmCode}", entity, (int)BusinessContracts.Enums.CacheDatabases.DcuAlarmTypesDatabaseId);
        }

        /// <summary>
        /// To delete the DcuAlarmMappingModel from dcu_alarms_mapping table
        /// </summary>
        /// <param name="model">DcuAlarmMappingModel which is to be deleted</param>
        /// <returns>True</returns>
        /// 
        public async Task<bool> DeleteDcuAlarmTypeAsync(DcuAlarmTypeModel model)
        {
            await _cassandraRepository.DeleteAsync<DcuAlarmTypeEntity>(Constants.CassandraTableNames.DcuAlarmType,
            p =>p.TableName == model.TableName &&  p.AlarmCode == model.AlarmCode).ConfigureAwait(false);
            _cacheProvider.Remove($"{model.AlarmCode}", (int)Enums.CacheDatabases.DcuAlarmTypesDatabaseId);
            return await Task.Factory.StartNew(() => true);
        }
    }
}
