﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AO.BusinessContracts;
using AutoMapper;
using CE.AO.Logging;
using CE.AO.Models;

namespace AO.Business
{
    public class DcuAlarmFacade : IDcuAlarmFacade
    {
        public LogModel LogModel { get; set; }


        private Lazy<IDcuAlarm> DcuAlarm { get; set; }

        private Lazy<IDcuAlarmHistory> DcuAlarmHistory { get; set; }

        private Lazy<IDcuAlarmCode> DcuAlarmCode { get; set; }

        private Lazy<IDcuAlarmCodeHistory> DcuAlarmCodeHistory { get; set; }

        private Lazy<IDcuAlarmType> DcuAlarmType { get; set; }

        public DcuAlarmFacade(LogModel logModel, Lazy<IDcuAlarm> dcuAlarm,
            Lazy<IDcuAlarmHistory> dcuAlarmHistory, Lazy<IDcuAlarmCode> dcuAlarmCode,
            Lazy<IDcuAlarmCodeHistory> dcuAlarmCodeHistory, Lazy<IDcuAlarmType> dcuAlarmType)
        {
            LogModel = logModel;
            DcuAlarm = dcuAlarm;
            DcuAlarmHistory = dcuAlarmHistory;
            DcuAlarmCode = dcuAlarmCode;
            DcuAlarmCodeHistory = dcuAlarmCodeHistory;
            DcuAlarmType = dcuAlarmType;
        }

        /// <summary>
        /// To insert in all 4 tables ie dcu_alarms, dcu_alarms_by_code, dcu_alarms_history, dcu_alarms_by_code_history by fetching Mapping related information from dcu_alarms_mapping
        /// </summary>
        /// <param name="model">DcuAlarmModel is mapped to respective models for insertion</param>
        /// <returns>true</returns>
        public async Task<bool> InsertOrMergeDcuAlarmAsync(DcuAlarmModel model)
        {
            var mappingInfo = DcuAlarmType.Value.GetDcuAlarmType(model.AlarmCode);

            if (mappingInfo != null)
                model = Mapper.Map(mappingInfo, model);

            var tasks = new List<Task<bool>>
            {
                DcuAlarmHistory.Value.InsertOrMergeDcuAlarmHistoryAsync(model),
                DcuAlarmCode.Value.InsertOrMergeDcuAlarmCodeAsync(model),
                DcuAlarmCodeHistory.Value.InsertOrMergeDcuAlarmCodeHistoryAsync(model)
            };

            await DcuAlarm.Value.InsertOrMergeDcuAlarmAsync(model).ConfigureAwait(false);
            var taskResults = await Task.WhenAll(tasks);

            if (mappingInfo == null)
                Logger.Error($"No mapping found for AlarmCode {model.AlarmCode}.", LogModel);

            return !taskResults.Any(result => false);
        }
    }
}
