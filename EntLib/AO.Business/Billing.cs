﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AO.BusinessContracts;
using AO.DataAccess.Contracts;
using AO.Entities;
using AutoMapper;
using CE.AO.Models;

namespace AO.Business
{
    /// <summary>
    /// Billing class is used to fetch/insert/update the billing details from/to the database
    /// </summary>
    public class Billing : IBilling
    {
        public Billing(LogModel logModel, Lazy<ICassandraRepository> cassandraRepository)
        {
            LogModel = logModel;
            CassandraRepository = cassandraRepository;
        }

        public LogModel LogModel { get; set; }


        private Lazy<ICassandraRepository> CassandraRepository { get; }

        /// <summary>
        /// To fetch the all latest bills from the database
        /// </summary>
        /// <param name="clientId">clientId of the data to be fetched</param>
        /// <param name="customerId">customerId of the data to be fetched</param>
        /// <param name="accountId">accountId of the data to be fetched</param>
        /// <returns>List of BillingModel</returns>
        public IEnumerable<BillingModel> GetAllServicesFromLastBill(int clientId, string customerId, string accountId)
        {
            IList<BillingEntity> services =
                CassandraRepository.Value.Get<BillingEntity>(Constants.CassandraTableNames.Billing,
                    a => a.ClientId == clientId && a.CustomerId == customerId && a.AccountId == accountId
                        // ReSharper disable once RedundantBoolCompare we need to explicitly specify IsLatest == true, otherwise it gives error. To execute cassandra query required to provide all values.
                         && a.IsLatest == true);

            return Mapper.Map<IEnumerable<BillingEntity>, IEnumerable<BillingModel>>(services);
        }

        /// <summary>
        /// To fetch the all bills of specified serviceContractId only from the database
        /// </summary>
        /// <param name="clientId">clientId of the data to be fetched</param>
        /// <param name="customerId">customerId of the data to be fetched</param>
        /// <param name="accountId">accountId of the data to be fetched</param>
        /// <param name="serviceContractId">serviceContractId of the data to be fetched</param>
        /// <param name="startDate">startDate of the data to be fetched</param>
        /// <returns>List of BillingModel</returns>
        public IEnumerable<BillingModel> GetBillsForService(int clientId, string customerId, string accountId,
            string serviceContractId, DateTime startDate)
        {
            IList<BillingEntity> services =
                CassandraRepository.Value.Get<BillingEntity>(Constants.CassandraTableNames.Billing,
                    a => a.ClientId == clientId && a.CustomerId == customerId && a.AccountId == accountId
                        // ReSharper disable once RedundantBoolCompare we need to explicitly specify IsLatest == true, 
                        // otherwise it gives error. To execute cassandra query required to provide all values.
                         && a.ServiceContractId == serviceContractId &&
                         (a.IsLatest == true || (a.IsLatest == false && a.EndDate >= startDate)));

            List<BillingEntity> bills = services.ToList();
            bills.RemoveAll(s => s.ServiceContractId != serviceContractId);
            return Mapper.Map<IEnumerable<BillingEntity>, IEnumerable<BillingModel>>(bills);
        }

        /// <summary>
        /// To fetch the all history bills within the date range only from the database
        /// </summary>
        /// <param name="clientId">clientId of the data to be fetched</param>
        /// <param name="customerId">customerId of the data to be fetched</param>
        /// <param name="accountId">accountId of the data to be fetched</param>
        /// <param name="serviceContractId">serviceContractId of the data to be fetched</param>
        /// <param name="startDate">startDate of the data to be fetched</param>
        /// <param name="endDate">endDate of the data to be fetched</param>
        /// <returns>List of BillingModel</returns>
        public IEnumerable<BillingModel> GetBillsForService(int clientId, string customerId, string accountId,
            string serviceContractId, DateTime startDate, DateTime endDate)
        {
            IList<BillingEntity> services =
                CassandraRepository.Value.Get<BillingEntity>(Constants.CassandraTableNames.Billing,
                    a => a.ClientId == clientId & a.CustomerId == customerId && a.AccountId == accountId
                         && a.ServiceContractId == serviceContractId && a.IsLatest == false &&
                         // NOTE: The strange date comparison below is a result of Cassandra 
                         //       not supporting DateTime.Date in Linq Expression.  
                         a.EndDate >= startDate.Date && (a.EndDate < endDate.Date.AddDays(1)));
            
            List<BillingEntity> bills = services.ToList();
            bills.RemoveAll(s => s.ServiceContractId != serviceContractId);
            return Mapper.Map<IEnumerable<BillingEntity>, IEnumerable<BillingModel>>(bills);
        }

        /// <summary>
        /// To fetch the latest bill from the database
        /// </summary>
        /// <param name="clientId">clientId of the data to be fetched</param>
        /// <param name="customerId">customerId of the data to be fetched</param>
        /// <param name="accountId">accountId of the data to be fetched</param>
        /// <param name="serviceContractId">serviceContractId of the data to be fetched</param>
        /// <returns>BillingModel</returns>
        public async Task<BillingModel> GetServiceDetailsFromBillAsync(int clientId, string customerId, string accountId,
            string serviceContractId)
        {
            var serviceDetails =
                CassandraRepository.Value.GetSingle<BillingEntity>(Constants.CassandraTableNames.Billing,
                    a => a.ClientId == clientId && a.CustomerId == customerId
                        // ReSharper disable once RedundantBoolCompare we need to explicitly specify IsLatest == true, otherwise it gives error. To execute cassandra query required to provide all values.
                         && a.AccountId == accountId && a.ServiceContractId == serviceContractId && a.IsLatest == true);

            return await Task.Factory.StartNew(() => Mapper.Map<BillingEntity, BillingModel>(serviceDetails));
        }

        /// <summary>
        /// To insert or update latest BillingModel to database
        /// </summary>
        /// <param name="model">model to be inserted</param>
        /// <returns>true</returns>
        public async Task<bool> InsertOrMergeLatestBillAsync(BillingModel model)
        {
            var entity = Mapper.Map<BillingModel, BillingEntity>(model);
            entity.IsLatest = true;
            await CassandraRepository.Value.InsertOrUpdateAsync(entity, Constants.CassandraTableNames.Billing,'A');
            return await Task.Factory.StartNew(() => true);
        }

        /// <summary>
        /// To insert or update history BillingModel to database
        /// </summary>
        /// <param name="model">model to be inserted</param>
        /// <returns>true</returns>
        public async Task<bool> InsertOrMergeHistoryBillAsync(BillingModel model)
        {
            var entity = Mapper.Map<BillingModel, BillingEntity>(model);
            entity.IsLatest = false;
            await CassandraRepository.Value.InsertOrUpdateAsync(entity, Constants.CassandraTableNames.Billing,'A');
            return await Task.Factory.StartNew(() => true);
        }

        /// <summary>
        /// To delete latest bill from database
        /// </summary>
        /// <param name="clientId">clientId of the data to be deleted</param>
        /// <param name="customerId">customerId of the data to be deleted</param>
        /// <param name="accountId">accountId of the data to be deleted</param>
        /// <param name="serviceContractId">serviceContractId of the data to be deleted</param>
        /// <returns>true</returns>
        public async Task<bool> DeleteLatestServiceDetailsAsync(int clientId, string customerId, string accountId,
            string serviceContractId)
        {
            await
                CassandraRepository.Value.DeleteAsync<BillingEntity>(Constants.CassandraTableNames.Billing,
                    a => a.ClientId == clientId && a.CustomerId == customerId
                        // ReSharper disable once RedundantBoolCompare we need to explicitly specify IsLatest == true, otherwise it gives error. To execute cassandra query required to provide all values.
                         && a.AccountId == accountId && a.ServiceContractId == serviceContractId && a.IsLatest == true,'A');
                    //.ConfigureAwait(false) bug bug
            return await Task.Factory.StartNew(() => true);
        }


        /// <summary>
        /// To delete history bill from database
        /// </summary>
        /// <param name="clientId">clientId of the data to be deleted</param>
        /// <param name="customerId">customerId of the data to be deleted</param>
        /// <param name="accountId">accountId of the data to be deleted</param>
        /// <param name="serviceContractId">serviceContractId of the data to be deleted</param>
        /// <param name="endDate">endDate of the data to be deleted</param>
        /// <returns>true</returns>
        public async Task<bool> DeleteHistoryServiceDetailsAsync(int clientId, string customerId, string accountId,
            string serviceContractId, DateTime endDate)
        {
            await
                CassandraRepository.Value.DeleteAsync<BillingEntity>(Constants.CassandraTableNames.Billing,
                    a => a.ClientId == clientId && a.CustomerId == customerId
                         && a.AccountId == accountId && a.ServiceContractId == serviceContractId && a.IsLatest == false &&
                         a.EndDate == endDate,'A');// .ConfigureAwait(false)
            return await Task.Factory.StartNew(() => true);
        }
    }
}
