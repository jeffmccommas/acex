﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using AO.BusinessContracts;
using AO.DataAccess.Contracts;
using AO.DTO;
using AO.Entities;
using AutoMapper;
using CE.AO.Models;

namespace AO.Business
{
    /// <summary>
    ///  Log Class which will interact with database
    /// </summary>
    public class OperationsFacade: IOperationsFacade
    {
        public LogModel LogModel { get; set; }
        private Lazy<ILogRepository> LogRepository { get; }

        public OperationsFacade(LogModel logModel, Lazy<ILogRepository> logRepository )
        {
            LogModel = logModel;
            LogRepository = logRepository;
        }

        /// <summary>
        /// Insert LogViewModel to Log table
        /// </summary>
        /// <param name="model">LogViewModel to be inserted</param>
        /// <returns></returns>  
        public async Task<bool> InsertOrMergeLogAsync(LogViewModel model)
        {
            await LogRepository.Value.InsertOrMergeLogAsync(Mapper.Map<LogViewModel,LogEntity>(model));
            return true;
        }

        /// <summary>
        /// To fetch the list of logs from the database
        /// </summary>
        /// <param name="filterParams">Filter params for logs to be fetched</param>
        /// <returns>List of LogViewModel model</returns>
        public List<LogViewModel> GetLogHistory(LogHistoryRequestDTO filterParams)
        {
            return Mapper.Map<List<LogEntity>, List<LogViewModel>>(LogRepository.Value.GetLogHistory(filterParams));
        }
    }
}