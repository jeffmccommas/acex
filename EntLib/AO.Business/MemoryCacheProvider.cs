﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AO.BusinessContracts;
using System.Runtime.Caching;
using Microsoft.Azure;

namespace AO.Business
{
    /// <summary>
    /// In Memory Cache provider.
    /// For every list of items to be cached will be a separate instance of memory cache
    /// </summary>
    public class MemoryCacheProvider : ICacheProvider
    {
        /// <summary>
        /// Cache expiration in seconds.
        /// </summary>
        private static int ExpireInSeconds
        {
            get
            {
                var defaultExpireInSeconds = 1800;// 30 minutes
                var expireInSecondsConfigValue = CloudConfigurationManager.GetSetting(StaticConfig.CacheExpireInSeconds);
                if (string.IsNullOrWhiteSpace(expireInSecondsConfigValue)) return defaultExpireInSeconds;
                int expireInSeconds;
                if (int.TryParse(expireInSecondsConfigValue, out expireInSeconds))
                {
                    defaultExpireInSeconds = expireInSeconds;
                }
                return defaultExpireInSeconds;
            }
        }

        /// <summary>
        /// List of Memory Caches
        /// </summary>
        private static readonly List<MemoryCache> MemoryCaches = new List<MemoryCache>();

        /// <summary>
        /// Gets memory cache instance with respect to the database
        /// </summary>
        /// <param name="database"></param>
        /// <returns></returns>
        private static MemoryCache GetMemoryCacheInstance(int database)
        {
            var cacheInstance = MemoryCaches.FirstOrDefault(m => m.Name == Convert.ToString(database));
            if (cacheInstance == null)
            {
                cacheInstance = new MemoryCache(Convert.ToString(database));
                MemoryCaches.Add(cacheInstance);
            }
            return cacheInstance;
        }

        /// <inheritdoc />
        public void Set(string key, object value, int database)
        {
            var absoluteExpiration = DateTime.UtcNow.AddSeconds(ExpireInSeconds);
            GetMemoryCacheInstance(database).Set(key, value, absoluteExpiration);
        }

        /// <summary>
        /// Retrieve type T object from cache.
        /// </summary>
        /// <param name="key"></param>     
        /// <param name="database"></param>    
        /// <returns></returns>
        public T Get<T>(string key, int database)
        {
            var result = (T)GetMemoryCacheInstance(database).Get(key);
            return result;
        }

        /// <summary>
        /// Remove key from cache.
        /// </summary>
        /// <param name="key"></param>
        /// <param name="database"></param>
        /// <returns></returns>
        public bool Remove(string key, int database)
        {
            var result = GetMemoryCacheInstance(database).Remove(key);
            return result != null;
        }

        /// <summary>
        /// Removes all keys from the database
        /// </summary>        
        /// <returns></returns>
        public async Task FlushAllKeys()
        {
            foreach (var memoryCache in MemoryCaches)
            {
                memoryCache.Dispose();
            }
            await Task.Factory.StartNew(() => Task.CompletedTask);
        }

        /// <summary>
        /// Removes all keys from the memory caches.Connection string is not needed here as caching is done in memory.
        /// </summary>        
        /// <returns></returns>
        public async Task FlushAllKeys(string connectionString)
        {
            await FlushAllKeys().ConfigureAwait(false);
        }

        /// <summary>
        /// Removes all keys from the database specified
        /// </summary>        
        /// <returns></returns>
        public async Task FlushAllKeys(int database)
        {
            GetMemoryCacheInstance(database).Dispose();
            await Task.Factory.StartNew(() => Task.CompletedTask);
        }

        /// <summary>
        /// Removes all keys from the database specified.Connection string is not needed here as caching is done in memory.
        /// </summary>        
        /// <returns></returns>
        public async Task FlushAllKeys(int database, string connectionString)
        {
            await FlushAllKeys(database).ConfigureAwait(false);
        }
    }
}
