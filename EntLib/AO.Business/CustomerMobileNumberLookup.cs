﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AO.BusinessContracts;
using AO.DataAccess.Contracts;
using AO.Entities;
using AutoMapper;
using CE.AO.Models;

namespace AO.Business
{
	/// <summary>
	/// CustomerMobileNumberLookup class is used to fetch/insert/update the customermobilenumberlookup details from/to the database
	/// </summary>
	public class CustomerMobileNumberLookup : ICustomerMobileNumberLookup
	{
			  
		public CustomerMobileNumberLookup(LogModel logModel, Lazy<ICassandraRepository> cassandraRepository)
		{
			LogModel = logModel;
			CassandraRepository = cassandraRepository;
		}

		public LogModel LogModel { get; set; }
		
	  
		private Lazy<ICassandraRepository> CassandraRepository { get; }

		/// <summary>
		/// To fetch the customermobilenumberlookup detail from the database
		/// </summary>
		/// <param name="clientId">clientId of the data to be fetched</param>
		/// <param name="mobileNumber">mobileNumber of the data to be fetched</param>
		/// <param name="accountId">accountId of the data to be fetched</param>
		/// <returns>CustomerMobileNumberLookupModel</returns>
		public CustomerMobileNumberLookupModel GetCustomerMobileNumberLookup(int clientId,
			string mobileNumber, string accountId)
		{
			var entity =
				CassandraRepository.Value.GetSingle<CustomerMobileNumberEntity>(
					Constants.CassandraTableNames.CustomerMobileNumber,
					c => c.ClientId == clientId && c.MobileNumber == mobileNumber && c.AccountId == accountId);
			return Mapper.Map<CustomerMobileNumberEntity, CustomerMobileNumberLookupModel>(entity);
		}

		/// <summary>
		/// To fetch the customermobilenumberlookup details from the database
		/// </summary>
		/// <param name="clientId">clientId of the data to be fetched</param>
		/// <param name="mobileNumber">mobileNumber of the data to be fetched</param>
		/// <returns>List of CustomerMobileNumberLookupModel</returns>
		public List<CustomerMobileNumberLookupModel> GetAllCustomerMobileNumberLookup(int clientId,
			string mobileNumber)
		{
			List<CustomerMobileNumberEntity> entities = CassandraRepository.Value.Get<CustomerMobileNumberEntity>(
					Constants.CassandraTableNames.CustomerMobileNumber,
					c => c.ClientId == clientId && c.MobileNumber == mobileNumber).ToList();
			return Mapper.Map<List<CustomerMobileNumberEntity>, List<CustomerMobileNumberLookupModel>>(entities);
		}

		/// <summary>
		/// To insert or update CustomerMobileNumberLookupModel to database
		/// </summary>
		/// <param name="model">model to be inserted</param>
		/// <returns>true</returns>
		public async Task<bool> InsertOrMergeCustomerMobileNumberLookupAsync(CustomerMobileNumberLookupModel model)
		{
			var entity = Mapper.Map<CustomerMobileNumberLookupModel, CustomerMobileNumberEntity>(model);
		    await
		        CassandraRepository.Value.InsertOrUpdateAsync(entity, Constants.CassandraTableNames.CustomerMobileNumber)
		            .ConfigureAwait(false);
			return await Task.Factory.StartNew(() => true);
		}

		/// <summary>
		/// To delete CustomerMobileNumberEntity from the database
		/// </summary>
		/// <param name="clientId">Client id of entity to be deleted</param>
		/// <param name="mobileNumber">Mobile number of entity to be deleted</param>
		/// <param name="accountId">Account Id of entity to be deleted</param>
		/// <returns>true</returns>
		public async Task<bool> DeleteCustomerMobileNumberLookupAsync(int clientId, string mobileNumber, string accountId)
		{
		    await
		        CassandraRepository.Value.DeleteAsync<CustomerMobileNumberEntity>(
		            Constants.CassandraTableNames.CustomerMobileNumber,
		            a => a.ClientId == clientId && a.MobileNumber == mobileNumber && a.AccountId == accountId)
		            .ConfigureAwait(false);
			return await Task.Factory.StartNew(() => true);
		}

	}
}