﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text.RegularExpressions;
using CE.AO.Models;
using CE.AO.Utilities;
using CE.ContentModel;
using CE.ContentModel.Entities;

namespace AO.Business
{
    public static class Extensions
    {
        private const string ConversionCftoCcf = "conversion.cftoccf";
        private const string ConversionCftoMcf = "conversion.cftomcf";
        private const string ConversionCftoGal= "conversion.cftogal";

        private const string ConversionCcftoMcf = "conversion.ccftomcf";
        private const string ConversionCcftoCf = "conversion.ccftocf";
        private const string ConversionCcftoGal = "conversion.ccftogal";
        private const string ConversionCcftoTherms = "conversion.ccftotherm";

        private const string ConversionMcftoTherms = "conversion.mcftotherm";
        private const string ConversionMcftoCcf = "conversion.mcftoccf";
        private const string ConversionMcftoGal = "conversion.mcftogal";
        private const string ConversionMcftoCf = "conversion.mcftocf";

        private const string ConversionGaltoCf = "conversion.galtocf";
        private const string ConversionGaltoCcf = "conversion.galtoccf";
        private const string ConversionGaltoMcf = "conversion.galtomcf";

        private const string AllowAll = "^allowall^";
        private const string AllowNone = "^allownone^";
        private const string BlockNone = "^blocknone^";
        private const string BlockAll = "^blockall^";

        public static CustomInsightModel GetCustomInsightModel(List<SubscriptionModel> subscriptions,
            InsightSettings insight, ProgramSettings program, string serviceContractId, string commodityType,
            int? billUomId)
        {
            var customInsightModel = new CustomInsightModel
            {
                InsightName = insight.InsightName,
                IsAccountLevel = string.IsNullOrEmpty(serviceContractId)
            };

            var insightSubscribed = customInsightModel.IsAccountLevel
                ? subscriptions.FirstOrDefault(
                    e =>
                        e.ProgramName == program.UtilityProgramName && string.IsNullOrWhiteSpace(e.ServiceContractId) &&
                        e.InsightTypeName == insight.InsightName)
                : subscriptions.FirstOrDefault(
                    e =>
                        e.ProgramName == program.UtilityProgramName && e.ServiceContractId == serviceContractId &&
                        e.InsightTypeName == insight.InsightName);

            if (insightSubscribed != null)
            {
                customInsightModel.ThresholdValue =
                    insightSubscribed.SubscriberThresholdValue ?? insight.GetCommodityTypevalue(commodityType);
                // set the Uom Id
                customInsightModel.UomId = insightSubscribed.UomId;
            }
            else
            {
                if (insight.Level.Equals(Enums.InsightLevel.Account.ToString()))
                {
                    double threshouldValue;
                    double.TryParse(insight.DefaultValue, out threshouldValue);
                    customInsightModel.ThresholdValue = threshouldValue;
                }
                else
                {
                    customInsightModel.ThresholdValue = insight.GetCommodityTypevalue(commodityType);
                    // set the Uom Id from bill
                    customInsightModel.UomId = billUomId;
                }
            }

            return customInsightModel;
        }

        public static double? GetCommodityTypevalue(this InsightSettings insight, string commodityType)
        {
            var commodities = insight.CommodityType.ToLower(CultureInfo.InvariantCulture).Split(',').ToArray();
            var defaultValues = insight.DefaultValue.Split(',').ToArray();
            if (string.IsNullOrEmpty(commodityType) && insight.Level.Equals(Enums.InsightLevel.Account.ToString()))
            {
                double threshouldValue;
                double.TryParse(insight.DefaultValue, out threshouldValue);
                return threshouldValue;
            }

            if (string.IsNullOrEmpty(commodityType))
            {
                return 0;
            }

            if (string.IsNullOrEmpty(insight.DefaultValue))
            {
                return null;
            }

            var defaultValue = commodities.Select((commodity, index) =>
                new {commodity, value = defaultValues[index]})
                .FirstOrDefault(e => e.commodity.IndexOf(commodityType, 0, StringComparison.OrdinalIgnoreCase) >= 0);

            if (defaultValue == null) return 0;
            var matchingvalue = defaultValue.value;
            return string.IsNullOrEmpty(matchingvalue) ? (double?) null : Convert.ToDouble(matchingvalue);
        }

        /// <summary>
        /// get the conversion factor based on the source uom and dest uom
        /// </summary>
        /// <param name="clientId">The current ClientId</param>
        /// <param name="sourceUom">The source unit-of-measurement</param>
        /// <param name="destUom">The  desired unit-of-measurement</param>
        /// <returns>The conversion factor.</returns>
        public static double GetConversionFactor(int clientId, int sourceUom, int destUom)
        {
            //Get settings from Contentful
            var factory = new ContentModelFactoryContentful();
            var provider = factory.CreateContentProvider(clientId);

            var conversionKey = string.Empty;
            double conversionFactor = 1;

            var source = (Enums.UomType)sourceUom;
            var dest = (Enums.UomType)destUom;

            // Hcf is the same as Ccf
            if (source == Enums.UomType.hcf)
            {
                source = Enums.UomType.ccf;
            }
            if (dest == Enums.UomType.hcf)
            {
                dest = Enums.UomType.ccf;
            }

            // Handle Cf
            if (source == Enums.UomType.cf && (dest == Enums.UomType.ccf || dest == Enums.UomType.hcf))
            {
                conversionKey = ConversionCftoCcf;
            }
            else if (source == Enums.UomType.cf && dest == Enums.UomType.mcf)
            {
                conversionKey = ConversionCftoMcf;
            }
            else if (source == Enums.UomType.cf && dest == Enums.UomType.gal)
            {
                conversionKey = ConversionCftoGal;
            }

            // Handle Ccf
            else if (source == Enums.UomType.ccf && dest == Enums.UomType.gal)
            {
                conversionKey = ConversionCcftoGal;
            }
            else if (source == Enums.UomType.ccf && dest == Enums.UomType.therms)
            {
                conversionKey = ConversionCcftoTherms;
            }
            else if(source == Enums.UomType.ccf && dest == Enums.UomType.cf)
            {
                conversionKey = ConversionCcftoCf;
            }
            else if (source == Enums.UomType.ccf && dest == Enums.UomType.mcf)
            {
                conversionKey = ConversionCcftoMcf;
            }

            // Handle Mcf
            else if (source == Enums.UomType.mcf && dest == Enums.UomType.gal)
            {
                conversionKey = ConversionMcftoGal;
            }
            else if (source == Enums.UomType.mcf && dest == Enums.UomType.therms)
            {
                conversionKey = ConversionMcftoTherms;
            }
            else if (source == Enums.UomType.mcf && dest == Enums.UomType.cf)
            {
                conversionKey = ConversionMcftoCf;
            }
            else if (source == Enums.UomType.mcf && dest == Enums.UomType.ccf)
            {
                conversionKey = ConversionMcftoCcf;
            }

            // Handle Gal
            else if (source == Enums.UomType.gal && dest == Enums.UomType.cf)
            {
                conversionKey = ConversionGaltoCf;
            }
            else if (source == Enums.UomType.gal && dest == Enums.UomType.ccf)
            {
                conversionKey = ConversionGaltoCcf;
            }
            else if (source == Enums.UomType.gal && dest == Enums.UomType.mcf)
            {
                conversionKey = ConversionGaltoMcf;
            }

            // Convert the key to a double for calculation.
            if (!string.IsNullOrEmpty(conversionKey))
            {
                var conversion = provider.GetContentConfiguration("common", conversionKey);
                if (conversion.Any())
                {
                    conversionFactor = double.Parse(conversion[0].Value);
                }
            }

            // return the conversion
            return conversionFactor;
        }

        /// <summary>
        /// Check the email address of a customer for validity.
        /// </summary>
        /// <param name="email">The email address</param>
        /// <returns>True, if valid.</returns>
        public static bool IsValidEmail(string email)
        {
            try
            {
                // Ignore if null or empty
                if (string.IsNullOrEmpty(email))
                {
                    return true;
                }

                // If a blank string then is invalid
                if (IsAllWhiteSpace(email))
                {
                    return false;
                }

                var addr = new System.Net.Mail.MailAddress(email.Trim());
                return addr.Address == email;
            }
            catch
            {
                return false;
            }
        }

        /// <summary>
        ///  Check the phone number of a customer for validity.
        ///  Must be a 10 digit number
        /// </summary>
        /// <param name="phone">The phone number</param>
        /// <returns>True, if valid</returns>
        public static bool IsValidPhone(string phone)
        {
            string newPhoneNumber;
            return IsValidPhone(phone, out newPhoneNumber);
        }

        /// <summary>
        ///  Check the phone number of a customer for validity.
        ///  Must be a 10 digit number
        /// </summary>
        /// <param name="phone">The phone number</param>
        /// <param name="newPhoneNumber">The phone number with just digits.</param>
        /// <returns>True, if valid</returns>
        public static bool IsValidPhone(string phone, out string newPhoneNumber)
        {
            try
            {
                if (string.IsNullOrEmpty(phone))
                {
                    newPhoneNumber = phone;
                    return true;
                }

                // If a blank string then is invalid
                if (IsAllWhiteSpace(phone))
                {
                    newPhoneNumber = phone;
                    return false;
                }

                phone = Regex.Replace(phone.Trim(), @"[^\d]", string.Empty,
                    RegexOptions.Compiled | RegexOptions.IgnoreCase);

                long thePhoneNo;
                var isNumeric = long.TryParse(phone, out thePhoneNo);

                if (phone.Length != 10 || !isNumeric)
                {
                    newPhoneNumber = phone;
                    return false;
                }

                newPhoneNumber = thePhoneNo.ToString();
                return true;

            }
            catch
            {
                // ignored
            }

            newPhoneNumber = phone;
            return false;
        }

        public static void MergeSettings(List<SubscriptionModel> subscriptions, ClientSettings clientSettings,
            BillingModel billingModel)
        {
            var subscribedProgramList = (from program in clientSettings.Programs
                join subscription in subscriptions on program.UtilityProgramName equals subscription.ProgramName
                where subscription.IsSelected && string.IsNullOrEmpty(subscription.InsightTypeName)
                select new ProgramSettings
                {
                    FileExportRequired = program.FileExportRequired,
                    UtilityProgramName = program.UtilityProgramName,
                    ProgramName = program.ProgramName,
                    Insights = program.Insights,
                    //CommodityType = program.CommodityType
                }).ToList();

            foreach (var program in subscribedProgramList)
            {
                var subscribedProgramInsights =
                    subscriptions.Where(
                        e => e.ProgramName == program.UtilityProgramName && !string.IsNullOrEmpty(e.InsightTypeName))
                        .ToList();
                for (int i = program.Insights.Count - 1; i >= 0; i--)
                {
                    var insight = program.Insights[i];
                    var subscribedProgram = subscriptions.Find(
                        s => s.ProgramName == program.ProgramName && string.IsNullOrEmpty(s.InsightTypeName));

                    if (subscribedProgram != null)
                    {
                        insight.DefaultCommunicationChannel = !string.IsNullOrEmpty(subscribedProgram.Channel)? subscribedProgram.Channel: insight.DefaultCommunicationChannel;
                    }
                                        
                    var spi =
                        subscribedProgramInsights.FirstOrDefault(pi => pi.InsightTypeName == insight.UtilityInsightName);
                    var billingModelFlag = billingModel == null;
                    if (!billingModelFlag)
                    {
                        billingModelFlag = insight.CommodityType.Contains(
                            ((Enums.CommodityType) billingModel.CommodityId).GetDescriptionOfEnum());
                        spi =
                            subscribedProgramInsights.FirstOrDefault(
                                pi =>
                                    pi.ServiceContractId == billingModel.ServiceContractId &&
                                    pi.InsightTypeName == insight.UtilityInsightName);
                    }
                    if (spi != null && billingModelFlag)
                    {
                        if (!spi.IsSelected)
                        {
                            if (insight.DefaultCommunicationChannel != Enums.SubscriptionChannel.File.ToString() &&
                                (string.IsNullOrEmpty(spi.Channel) ||
                                 insight.DefaultCommunicationChannel == spi.Channel ||
                                 spi.Channel == Enums.SubscriptionChannel.EmailAndSMS.ToString()))
                            {
                                program.Insights.RemoveAt(i);
                                continue;
                            }

                            if (insight.DefaultCommunicationChannel ==
                                Enums.SubscriptionChannel.EmailAndSMS.ToString())
                            {
                                if (spi.Channel == Enums.SubscriptionChannel.Email.ToString())
                                {
                                    spi.Channel =
                                        Enums.SubscriptionChannel.SMS.ToString();
                                }
                                if (spi.Channel == Enums.SubscriptionChannel.SMS.ToString())
                                {
                                    spi.Channel =
                                        Enums.SubscriptionChannel.Email.ToString();
                                }
                            }
                        }
                        insight.DefaultCommunicationChannel = !string.IsNullOrEmpty(spi.Channel)
                            ? insight.AllowedCommunicationChannel.IndexOf(spi.Channel, 0, StringComparison.OrdinalIgnoreCase) >= 0
                                ? spi.Channel
                                : string.Empty
                            : insight.DefaultCommunicationChannel;
                    }
                }
            }

            // final list subscribedProgramList
            clientSettings.Programs = subscribedProgramList;
        }      

        public static List<ClientUOM> GetUomContents(int clientId)
        {
            //Get settings from Contentful
            var factory = new ContentModelFactoryContentful();
            var provider = factory.CreateContentProvider(clientId);

            var uomContentList = provider.GetContentUom(string.Empty);

            return uomContentList;
        }

        public static List<ClientTextContent> GetTextContents(int clientId, string locale, string category)
        {
            //Get settings from Contentful
            var factory = new ContentModelFactoryContentful();
            var provider = factory.CreateContentProvider(clientId);

            var textContentList = provider.GetContentTextContent(category, provider.GetContentLocale(locale),
                string.Empty);

            return textContentList;

        }

        public static string GetUomTextContent(string uomKey, IList<ClientUOM> uomList,
            List<ClientTextContent> textContentList)
        {
            var uomTextContent = string.Empty;
            var clientTextContent = textContentList.FirstOrDefault(
                z =>
                {
                    var firstOrDefault = uomList.FirstOrDefault(x => x.Key == uomKey);
                    return firstOrDefault != null && z.Key == firstOrDefault.NameKey;
                });
            if (clientTextContent != null)
            {
                uomTextContent =
                    clientTextContent
                        .ShortText;
            }

            return uomTextContent;
        }

        private static bool IsAllWhiteSpace(string value)
        {
            return value.All(char.IsWhiteSpace);
        }

        /// <summary>
        /// Get TouId depending on column
        /// </summary>
        /// <param name="property">property of model</param>
        /// <returns>TouId</returns>
        public static int GetTouIdFromProperty(dynamic property)
        {
            if (property.Name.ToString().Equals("TOU_Regular"))
                return 0;
            if (property.Name.ToString().Equals("TOU_OnPeak"))
                return 1;
            if (property.Name.ToString().Equals("TOU_OffPeak"))
                return 2;
            if (property.Name.ToString().Equals("TOU_Shoulder1"))
                return 3;
            if (property.Name.ToString().Equals("TOU_Shoulder2"))
                return 4;
            return 5;
        }

        public static bool SendEmailValidation(string email, string whitelist, string blacklist)
        {
            if (IsEmailOnWhitelist(email, whitelist) && !IsEmailOnBlacklist(email, blacklist))
                return true;
            return false;
        }

        public static bool SendSmsValidation(string phone, string whitelist, string blacklist)
        {
            if (IsPhoneOnWhitelist(phone, whitelist) && !IsPhoneOnBlacklist(phone, blacklist))
                return true;
            return false;
        }

        private static bool IsEmailOnWhitelist(string email, string whitelist)
        {
            if (!string.IsNullOrEmpty(whitelist))
            {
                if (whitelist.Contains(AllowAll))
                    return true;
                if (whitelist.Contains(AllowNone))
                    return false;

                var domain = string.Empty;
                var emailPart = email.Split('@');
                if (emailPart.GetUpperBound(0) == 1)
                {
                    domain = $"*@{emailPart[1]}";
                }

                if ((!string.IsNullOrEmpty(domain) && whitelist.Contains(domain)) || whitelist.Contains(email))
                {
                    return true;
                }
            }
            return false;
        }

        private static bool IsEmailOnBlacklist(string email, string blacklist)
        {
            if (!string.IsNullOrEmpty(blacklist))
            {
                if (blacklist.Contains(BlockAll))
                    return true;
                if (blacklist.Contains(BlockNone))
                    return false;

                if (blacklist.Contains(email))
                    return true;
            }
            return false;
        }

        private static bool IsPhoneOnWhitelist(string phone, string whitelist)
        {
            if (!string.IsNullOrEmpty(whitelist))
            {
                if (whitelist.Contains(AllowAll))
                    return true;
                if (whitelist.Contains(AllowNone))
                    return false;
                if (whitelist.Contains(phone))
                    return true;
            }
            return false;
        }

        private static bool IsPhoneOnBlacklist(string phone, string blacklist)
        {
            if (!string.IsNullOrEmpty(blacklist))
            {
                if (blacklist.Contains(BlockAll))
                    return true;
                if (blacklist.Contains(BlockNone))
                    return false;

                if (blacklist.Contains(phone))
                    return true;
            }
            return false;
        }
    }
}
