﻿namespace AO.Business
{
    public static class StaticConfig
    {
        public const string BtdType = "B";
        public const string CtdType = "C";
        public const string RedisCacheConnectionString = "RedisCacheConnectionString";
        public const string CacheExpireInSeconds = "CacheExpireInSeconds";
    }
}
