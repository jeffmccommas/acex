﻿using System.Diagnostics;
using System.IO;
using AO.BusinessContracts;
using AO.DataAccess.Contracts;
using CE.AO.Logging;
using CE.AO.Models;
using CE.AO.Utilities;
using CsvHelper;

namespace AO.Business
{
    /// <summary>
    /// ExportFacade class is used for export table data.
    /// </summary>
    public class ExportFacade : IExportFacade
    {
        private IAoLogger Logger { get; }

        private ICassandraRepository CassandraRepository { get; }

        public ExportFacade(LogModel logModel, ICassandraRepository cassandraRepository, IAoLogger logger)
        {
            LogModel = logModel;
            CassandraRepository = cassandraRepository;
            Logger = logger;
        }

        public LogModel LogModel { get; set; }


        /// <summary>
        /// To export table from database to blob storage.
        /// </summary>
        /// <param name="blobConnectionString">Conncetion string for blob storage</param>
        /// <param name="containerName">Container name for the blob storage.</param>
        /// <param name="fileName">filename of export CSV file in blob storage.</param>
        /// <param name="filterQuery">Generalize SQL expression with filter condition, which will execute on database.</param>
        /// <param name="filterName">Name of the filter.</param>
        public void ExportData(string blobConnectionString, string containerName, string fileName, string filterQuery,string filterName)
        {
            LogModel.Module = CE.AO.Utilities.Enums.Module.ExportProcess;

            Stopwatch sw = new Stopwatch();
            sw.Start();

            var blob = BlobStorageManager.CreateAppendBlob(blobConnectionString, containerName, fileName);

            using (var streamWriter = new StreamWriter(blob.OpenWrite(false)))
            using (var csvWriter = new CsvWriter(streamWriter))
            {

                CassandraRepository.ExecutePagedQuery(filterQuery, (rowNumber, columnValues, columnNames) =>
                {
                    // Write Header.
                    if (rowNumber == 1)
                    {
                        foreach (var columnName in columnNames)
                        {
                            csvWriter.WriteField(columnName);
                        }
                        csvWriter.NextRecord();
                    }

                    // Write row values.
                    foreach (var columnValue in columnValues)
                    {
                        csvWriter.WriteField(columnValue);
                    }
                    csvWriter.NextRecord();
                });
            }

            sw.Stop();

            Logger.Info($"Time to export file [{fileName}] to Blob storage container [{containerName}] was [{sw.Elapsed}].  Query invoked [{filterQuery}]. Filter name is [{filterName}]", LogModel);

        }

       
    }
 }
