﻿using System.Threading.Tasks;
using AO.BusinessContracts;
using AO.DataAccess.Contracts;
using AO.Entities;
using AutoMapper;
using CE.AO.Models;

namespace AO.Business
{
	public class DcuAlarmCode : IDcuAlarmCode
	{
		private readonly ICassandraRepository _cassandraRepository;

		public DcuAlarmCode(ICassandraRepository cassandraRepository)
		{
			_cassandraRepository = cassandraRepository;
		}

		/// <summary>
		/// To insert/update the DcuAlarmModel in dcu_alarms_by_code table
		/// </summary>
		/// <param name="model">DcuAlarmModel which is to be inserted/updated</param>
		/// <returns>True</returns>
		public async Task<bool> InsertOrMergeDcuAlarmCodeAsync(DcuAlarmModel model)
		{
			var entity = Mapper.Map<DcuAlarmModel, DcuAlarmByCodeEntity>(model);
			await _cassandraRepository.InsertOrUpdateAsync(entity, Constants.CassandraTableNames.DcuAlarmByCode).ConfigureAwait(false);
			return await Task.Factory.StartNew(() => true);
		}

		/// <summary>
		/// To delete the DcuAlarmModel in dcu_alarms_by_code table
		/// </summary>
		/// <param name="model">DcuAlarmModel which is to be deleted</param>
		/// <returns>True</returns>
		public async Task<bool> DeleteDcuAlarmCodeAsync(DcuAlarmModel model)
		{
			await _cassandraRepository.DeleteAsync<DcuAlarmByCodeEntity>(Constants.CassandraTableNames.DcuAlarmByCode,
				p => p.ClientId == model.ClientId && p.DcuId == model.DcuId && p.AlarmTime == model.AlarmTime && p.AlarmCode == model.AlarmCode).ConfigureAwait(false);

			return await Task.Factory.StartNew(() => true);
		}

		/// <summary>
		/// To delete the DcuAlarmModel in dcu_alarms_by_code table
		/// </summary>
		/// <param name="model">DcuAlarmModel which is to be deleted</param>
		/// <returns>True</returns>
		public async Task<bool> DeleteDcuAlarmCodeWithPartionAsync(DcuAlarmModel model)
		{
			await _cassandraRepository.DeleteAsync<DcuAlarmByCodeEntity>(Constants.CassandraTableNames.DcuAlarmByCode,
				p => p.ClientId == model.ClientId && p.AlarmCode == model.AlarmCode).ConfigureAwait(false);

			return await Task.Factory.StartNew(() => true);
		}
	}
}
