﻿using System.Threading.Tasks;
using AO.BusinessContracts;
using AO.DataAccess.Contracts;
using AO.Entities;
using AutoMapper;
using CE.AO.Models;

namespace AO.Business
{
	public class DcuAlarmHistory : IDcuAlarmHistory
	{
		private readonly ICassandraRepository _cassandraRepository;

		public DcuAlarmHistory(ICassandraRepository cassandraRepository)
		{
			_cassandraRepository = cassandraRepository;
		}

		/// <summary>
		/// To insert/update the DcuAlarmModel in dcu_alarms_history table
		/// </summary>
		/// <param name="model">DcuAlarmModel which is to be inserted/updated</param>
		/// <returns>True</returns>
		public async Task<bool> InsertOrMergeDcuAlarmHistoryAsync(DcuAlarmModel model)
		{
			var entity = Mapper.Map<DcuAlarmModel, DcuAlarmHistoryEntity>(model);
			await
				_cassandraRepository.InsertOrUpdateAsync(entity, Constants.CassandraTableNames.DcuAlarmHistory)
					.ConfigureAwait(false);
			return await Task.Factory.StartNew(() => true);
		}

		/// <summary>
		/// To delete the DcuAlarmModel in dcu_alarms_history table
		/// </summary>
		/// <param name="model">DcuAlarmModel which is to be deleted</param>
		/// <returns>True</returns>
		public async Task<bool> DeleteDcuAlarmHistoryAsync(DcuAlarmModel model)
		{
			await
				_cassandraRepository.DeleteAsync<DcuAlarmHistoryEntity>(
					Constants.CassandraTableNames.DcuAlarmHistory,
					p =>
						p.ClientId == model.ClientId && p.DcuId == model.DcuId && p.AlarmTime == model.AlarmTime &&
						p.AlarmCode == model.AlarmCode).ConfigureAwait(false);
			return await Task.Factory.StartNew(() => true);
		}
	}
}
