﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading;
using System.Threading.Tasks;
using AO.BusinessContracts;
using AO.DataAccess;
using AO.Entities;
using CE.AO.Logging;
using CE.AO.Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Enums = CE.AO.Utilities.Enums;
using AutoMapper;
using Microsoft.Azure;

namespace AO.Business
{
    public class SendSms : ISendSms
    {
        private const string AlreadyRegisterNumber = "MPSE2401";
        private const string SmsBlacklist = "SmsBlacklist";
        private const string SmsWhitelist = "SmsWhitelist";
        private readonly LogModel _logModel;

        public SendSms(LogModel logModel)
        {
            _logModel = logModel;
        }

        public async Task<string> Send(string apiKey, string username, string smsBody, string phone, string subscriptionId, string customKey, int clientId, string accountId, string programName, string serviceContractId, string insightTypeName, DateTime requestDate, DateTime? notifyDateTime = null)
        {
            var blacklist = CloudConfigurationManager.GetSetting(SmsBlacklist) ?? string.Empty;
            var whitelist = CloudConfigurationManager.GetSetting(SmsWhitelist) ?? string.Empty;

            var requestId = string.Empty;
            
            if(Extensions.SendSmsValidation(phone,whitelist,blacklist))
            {
                using (var client = new HttpClient())
                {
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                    client.DefaultRequestHeaders.Add("X-Apikey", apiKey);

                    var smsData = new JObject { { "message", smsBody } };
                    var recipientData = new JObject { { "type", "subscription" }, { "value", subscriptionId } };
                    var info = new SmsMessageInfo { description = customKey, sms = smsData, recipients = recipientData };

                    if (notifyDateTime != null)
                        info.send_date = ((DateTime)notifyDateTime).ToString("yyyy-MM-dd HH:mm:ss");

                    var postUrl = new Uri($"http://api.Aclara.MessagingChannel.com/rest/v1/{username}/message");
                    var response = await client.PutAsJsonAsync(postUrl, info);
                    if (response.IsSuccessStatusCode)
                    {
                        var messageResultModel = response.Content.ReadAsAsync<MessageResultModel>().Result;
                        var trumpiaRequestDetailEntity = new TrumpiaRequestDetailEntity
                        {
                            RequestId = messageResultModel.request_id,
                            MessageId = messageResultModel.message_id,
                            Description = customKey,
                            SmsBody = smsBody
                        };

                        await new CassandraRepository().InsertOrUpdateAsync(trumpiaRequestDetailEntity, Constants.CassandraTableNames.TrumpiaRequestDetail).ConfigureAwait(false);

                        try
                        {
                            // insert report
                            var trumpiaRequestDetailReportEntity = Mapper.Map<TrumpiaRequestDetailEntity, TrumpiaRequestDetailReportEntity>(trumpiaRequestDetailEntity);
                            trumpiaRequestDetailReportEntity.RowCreateDate = DateTime.UtcNow.ToString("yyyyMMdd");
                            await new CassandraRepository().InsertOrUpdateAsync(trumpiaRequestDetailReportEntity, Constants.CassandraTableNames.TrumpiaRequestDetailReport).ConfigureAwait(false);
                        }
                        catch (Exception ex)
                        {
                            Logger.Error(ex, _logModel);
                        }

                        //if (isSent)
                        requestId = messageResultModel.request_id;
                    }
                }
            }
            return requestId;
        }

        //Create new trumpia subscription
        public string CreateNewTrumpiaSubscription(CustomerModel customerModel, string apiKey, string username, string contactList)
        {
            var subscriptionResultModel = new SubscriptionResultModel();
            using (var client = new HttpClient())
            {                
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Add("X-Apikey", $"{apiKey}");

                
                var firstName = customerModel.FirstName;
                var lastName = customerModel.LastName;
                if (firstName.Length > 30)
                    firstName = firstName.Substring(0, 30);

                if (lastName.Length > 30)
                    lastName = lastName.Substring(0, 30);

                //PUT
                JObject subscription = new JObject
                {
                    {"first_name", firstName},
                    {"last_name", lastName}
                };

                JObject mobile = new JObject { { "number", customerModel.Phone1 }, { "country_code", "+1" } };
                //For US
                subscription.Add("mobile", mobile);

                JArray array = new JArray { subscription };

                var info = new SubscriptionInfoModel { list_name = $"{contactList}", subscriptions = array };
                // ReSharper disable once UseStringInterpolation
                Uri postUrl = new Uri(string.Format("http://api.Aclara.MessagingChannel.com/rest/v1/{0}/subscription", username));
                
                var response = client.PutAsJsonAsync(postUrl, info).Result;
                if (response.IsSuccessStatusCode)
                {
                    subscriptionResultModel = response.Content.ReadAsAsync<SubscriptionResultModel>().Result;
                }
            }

            return subscriptionResultModel.request_id;
        }

        private void LogSometimesInfo(object msg, LogModel lm) {
            Logger.Info(msg, lm);  // BUGBUG: remove this after 12690 diagnosis complete
        }

        //Get the subcription id by request id
        public string GetSubscriptionId(CustomerModel customerModel, string requestId, string apiKey, string username)
        {
            var subscriptionId = string.Empty;
            var subscriptionResultModelList = new List<SubscriptionResultModel>();
            using (var client = new HttpClient()) {
                client.BaseAddress = new Uri("http://api.Aclara.MessagingChannel.com");
                    // BUGBUG: is this correct for all environments?
                client.DefaultRequestHeaders.Accept.Clear();
                //client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json")); linerror
                client.DefaultRequestHeaders.Add("X-Apikey", $"{apiKey}");

                //diagnostics 
                string respStg = "justStarted";
                string restUriFrag = $"/rest/v1/{username}/report/{requestId}";
                LogModel lm = new LogModel() {
                    AccountId = "GetSId",
                    ClientId = customerModel.CustomerId,
                    CustomerId = "cjhdebug",
                    DisableLog = false,
                    Module = Enums.Module.SubscriptionEventProcess,
                    Metadata = $"{apiKey}",
                    ServiceContractId = restUriFrag,
                    Source = respStg,
                    MeterId = username,
                    PremiseId = requestId
                };

                // note: disabled s1643 becaus I am debugging!
                // note:  getting many MPCE400.  trumpia recommends the following
                //This status code is returned when the system is still processing the API request. 
                //It is recommended to put a 200-millisecond pause before using GET Report. 
                //This allows the system some time to process the API request.

                //var response = client.GetAsync($"/rest/v1/{username}/report/{requestId}").Result; orig

                int retryLimit = 10;
                bool done = false;
                while (!done && (retryLimit-- > 0))
                {  // having issues geting a good resoinse from 
                    try
                    {
                        Task<HttpResponseMessage> rTask = client.GetAsync(restUriFrag);
                        rTask.ConfigureAwait(true);
                        HttpResponseMessage response = rTask.Result;
                        if (response.IsSuccessStatusCode) {
                            Task<string> task = response.Content.ReadAsStringAsync();
                            task.ConfigureAwait(true);
                            respStg = task.Result;

                            lm.Source = respStg;
                            lm.ServiceContractId = response.RequestMessage.RequestUri.AbsoluteUri;
                            try {
                                if (!string.IsNullOrEmpty(respStg)) {
                                    var token = JToken.Parse(respStg);
                                    if (token is JArray) {
                                        JArray ja = JArray.Parse(respStg);
                                        if (ja != null) {
                                            done = true;
                                            subscriptionResultModelList =
                                                JsonConvert.DeserializeObject<List<SubscriptionResultModel>>(respStg);
                                        }
                                    }
                                    else {
                                        JObject jo = new JObject(JObject.Parse(respStg));
                                        string statCode = jo["status_code"].ToString();
                                        lm.AccountId += $";NA01;{statCode}";
                                        Logger.Warn("noGoodResp", lm);
                                        Thread.Sleep(1000);
                                    }
                                }
                                else {
                                    lm.AccountId += $";NA02";
                                    Logger.Warn("noGoodResp", lm);
                                    Thread.Sleep(1000);
                                }
                            }
                            catch (Exception jexc) {
                                JObject jo = new JObject(JObject.Parse(respStg));
                                string statCode = jo["status_code"].ToString();
                                lm.AccountId += $";EXC01;{statCode}";
                                Logger.Warn(jexc, lm);
                                Thread.Sleep(1000);
                            }
                        }
                    }

                    catch (Exception exc) {
                        lm.AccountId += ";EXC1";
                        Logger.Error(exc, lm);
                        Thread.Sleep(400);
                    }
                }
                if (!done || (retryLimit<0)) {
                    lm.AccountId += ";badresp1";
                    Logger.Error("noresp", lm);
                }

                lm.AccountId += ";NF1";
                LogSometimesInfo("rxMsg1",lm);  // BUGBUG: remove this after 12690 diagnosis complete?

                if (subscriptionResultModelList.Count > 0)
                {
                    var trumpiaRequestDetailEntity = new TrumpiaRequestDetailEntity
                    {
                        RequestId = requestId,
                        Description = "Subscription"
                    };

                    if (subscriptionResultModelList[0].status_code == null)
                        subscriptionId = Convert.ToString(subscriptionResultModelList[0].subscription_id);
                    else if (subscriptionResultModelList[0].status_code == AlreadyRegisterNumber)
                    {
                        // if number already registered, get the existing subscription id
                        try {
                            restUriFrag = $"/rest/v1/{username}/subscription/search?search_type=2&search_data={customerModel.Phone1}";
                            lm.ServiceContractId = restUriFrag;
                            Task<HttpResponseMessage> rTask = client.GetAsync(restUriFrag);
                            rTask.ConfigureAwait(true);
                            HttpResponseMessage response = rTask.Result;

                            //response = client.GetAsync(restUriFrag).Result;  linerror

                            if (response.IsSuccessStatusCode) {
                                Task<string> task = response.Content.ReadAsStringAsync();
                                task.ConfigureAwait(true);
                                respStg = task.Result;
                                lm.Source = respStg;
                                var subscriptionSearchResultModel =
                                    JsonConvert.DeserializeObject<SubscriptionSearchResultModel>(respStg);
                                if (subscriptionSearchResultModel.status_code == null)
                                {
                                    if (subscriptionSearchResultModel.subscription_id_list.Count > 0)
                                        subscriptionId = subscriptionSearchResultModel.subscription_id_list[0];
                                }
                                else
                                {
                                    trumpiaRequestDetailEntity.ErrorMessage =
                                        subscriptionSearchResultModel.error_message;
                                    trumpiaRequestDetailEntity.StatusCode = subscriptionSearchResultModel.status_code;
                                    subscriptionId = string.Empty;
                                }

                            }
                            else {
                                lm.AccountId += ";GetSIdF2";
                                trumpiaRequestDetailEntity.ErrorMessage = response.StatusCode.ToString();
                                trumpiaRequestDetailEntity.StatusCode = response.StatusCode.ToString();
                                subscriptionId = string.Empty;
                            }
                        }
                        catch (Exception exc) {
                            lm.AccountId += ";EXC2";
                            Logger.Error(exc, lm);
                        }
                        lm.AccountId += ";NF2";
                        LogSometimesInfo("rxMsg2", lm);  // BUGBUG: remove this after 12690 diagnosis complete

                    }
                    else
                    {
                        trumpiaRequestDetailEntity.ErrorMessage = subscriptionResultModelList[0].error_message;
                        trumpiaRequestDetailEntity.StatusCode = subscriptionResultModelList[0].status_code;
                        subscriptionId = string.Empty;
                    }

                    var trumpiaTask = Task.Run(() => new CassandraRepository().InsertOrUpdateAsync(trumpiaRequestDetailEntity, Constants.CassandraTableNames.TrumpiaRequestDetail));
                    Task.WhenAll(trumpiaTask);

                    try
                    {
                        var trumpiaReportEntity =
                            Mapper.Map<TrumpiaRequestDetailEntity, TrumpiaRequestDetailReportEntity>(
                                trumpiaRequestDetailEntity);
                        trumpiaReportEntity.RowCreateDate = DateTime.UtcNow.ToString("yyyyMMdd");
                        var trumpiaReportTask =
                            Task.Run(
                                () =>
                                    new CassandraRepository().InsertOrUpdateAsync(trumpiaReportEntity,
                                        Constants.CassandraTableNames.TrumpiaRequestDetailReport));
                        Task.WhenAll(trumpiaReportTask);
                    }
                    catch (Exception ex)
                    {
                        Logger.Error(ex, _logModel);
                    }
                }
            }
            return subscriptionId;
        }

        public async Task<SmsStatsModel> GetSmsStatics(string requestId, string apiKey, string username)
        {
            var smsStatsModel = new SmsStatsModel();
            try
            {
                using (var client = new HttpClient())
                {

                    client.BaseAddress = new Uri("http://api.Aclara.MessagingChannel.com");
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                    client.DefaultRequestHeaders.Add("X-Apikey", $"{apiKey}");
                    
                    //GET
                    HttpResponseMessage response = await client.GetAsync($"/rest/v1/{username}/report/{requestId}");
                    if (response.IsSuccessStatusCode)
                    {
                        smsStatsModel =
                            JsonConvert.DeserializeObject<SmsStatsModel>(response.Content.ReadAsStringAsync().Result);
                    }
                }
            }
            catch (Exception ex)
            {
                // do nothing, return stats model with exception message
                smsStatsModel.error_message = ex.Message;
            }


            return smsStatsModel;
        }

        public SubscriptionInfoResultModel GetSubscriptionInfo(string subscriptionId, string apiKey, string username)
        {
            var subscriptionInfo = new SubscriptionInfoResultModel();
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("http://api.Aclara.MessagingChannel.com");
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Add("X-Apikey", $"{apiKey}");
                
                //GET
                var response = client.GetAsync($"/rest/v1/{username}/subscription/{subscriptionId}").Result;
                if (response.IsSuccessStatusCode)
                {
                    subscriptionInfo = JsonConvert.DeserializeObject<SubscriptionInfoResultModel>(response.Content.ReadAsStringAsync().Result);
                }
            }
            return subscriptionInfo;
        }

        public string UpdateSubscriptionInfo(CustomerModel customerModel, string subscriptionId, string apiKey, string username, string contactList)
        {
            var subscriptionResultModel = new SubscriptionResultModel();
            using (var client = new HttpClient())
            {
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Add("X-Apikey", $"{apiKey}");
                
                var firstName = customerModel.FirstName;
                var lastName = customerModel.LastName;

                if (firstName.Length > 30)
                    firstName = firstName.Substring(0, 30);

                if (lastName.Length > 30)
                    lastName = lastName.Substring(0, 30);
                //PUT
                JObject subscription = new JObject
                {
                    {"first_name", firstName},
                    {"last_name", lastName}
                };

                JObject mobile = new JObject { { "number", customerModel.Phone1 }, { "country_code", "+1" } };
                //For US
                subscription.Add("mobile", mobile);

                JArray array = new JArray { subscription };

                var info = new SubscriptionInfoModel { list_name = $"{contactList}", subscriptions = array };
                Uri postUrl = new Uri(string.Format("http://api.Aclara.MessagingChannel.com/rest/v1/{0}/subscription/{1}", username, subscriptionId));
                var response = client.PostAsJsonAsync(postUrl, info).Result;
                if (response.IsSuccessStatusCode)
                {
                    subscriptionResultModel = response.Content.ReadAsAsync<SubscriptionResultModel>().Result;
                }
            }
            return subscriptionResultModel.request_id;
        }
    }
}
