﻿using System.Collections;
using System.Xml;

namespace CE.Xml.Transformation
{
    public interface IXmlPart
    {
        Hashtable SharedObject { get; set; }

        void Fill();
        string ReadTemplate();
        string ReplaceToken(string token);
        void GenerateXml(XmlWriter writer);
        void GenerateMeterBillXml(XmlWriter writer);
    }
}
