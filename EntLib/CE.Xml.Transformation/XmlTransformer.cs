﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.ServiceModel.Syndication;
using System.Text;
using System.Xml;
using System.Xml.Serialization;

namespace CE.Xml.Transformation
{
    public class XmlTransformer
    {
        private string _fileName;

        private string _processorInfoFileName;

        private List<IXmlPart> _xmlParts;
        private List<XmlElement> _xmlElements;

        private List<string> _excludeElements;

        private string _greenButtonDataType;

        private Hashtable _sharedObject;
        private Hashtable _xmlTemplates;

        public XmlTransformer(string fileName)
        {
            _fileName = fileName;

            _xmlParts = new List<IXmlPart>();
            _xmlElements = new List<XmlElement>();
            _excludeElements = new List<string>();

            _sharedObject = new Hashtable();
        }

        public XmlTransformer(string fileName, string templates)
            : this(fileName)
        {
            GenerateTemplates(templates);
        }

        public string ProcessorInfoFileName
        {
            get { return _processorInfoFileName; }
            set { _processorInfoFileName = value; }
        }

        public Hashtable SharedObject
        {
            get { return _sharedObject; }
            set { _sharedObject = value; }
        }

        public List<IXmlPart> XmlParts
        {
            get { return _xmlParts; }
            set { _xmlParts = value; }
        }

        public List<XmlElement> XmlElements
        {
            get { return _xmlElements; }
            set { _xmlElements = value; }
        }

        public List<string> ExcludeElements
        {
            get { return _excludeElements; }
            set { _excludeElements = value; }
        }

        public string GreenButtonDataType
        {
            get { return _greenButtonDataType; }
            set { _greenButtonDataType = value; }
        }


        public void GenerateXml()
        {
            using (XmlTextWriter writer = new XmlTextWriter(_fileName, new UTF8Encoding()))
            {
                writer.Formatting = Formatting.Indented;
                writer.WriteStartDocument();

                if (_processorInfoFileName.Length > 0)
                {
                    // Add processor information to the XML file
                    String strPi = "type='text/xsl' href='" + _processorInfoFileName + "'";
                    // Write processor information
                    writer.WriteProcessingInstruction("xml-stylesheet", strPi);
                }

                foreach (XmlElement element in _xmlElements)
                {
                    writer.WriteStartElement(element.Name);

                    foreach (XmlAttribute attribute in element.Attributes)
                        writer.WriteAttributeString(attribute.Name, attribute.Value);
                }

                foreach (IXmlPart part in _xmlParts)
                    part.SharedObject = _sharedObject;

                foreach (IXmlPart part in _xmlParts)
                {
                    part.Fill();

                    string templateName = part.ReadTemplate();

                    if (templateName != string.Empty)
                    {
                        XmlNode node = (XmlNode)_xmlTemplates[templateName];
                        FillTemplate(node.FirstChild, writer, part);
                    }

                    //if (template != null)
                    //{
                    //    FillTemplate(template.DocumentElement.FirstChild, writer, part);
                    //}
                    if (_greenButtonDataType == "bill")
                        part.GenerateMeterBillXml(writer);
                    else
                        part.GenerateXml(writer);
                }

                // ReSharper disable once UnusedVariable
                foreach (XmlElement element in _xmlElements)
                    writer.WriteEndElement();

                writer.WriteEndDocument();
            }
        }


        public string GenerateXmlInMemory()
        {
            string xmlString;
            MemoryStream memoryStream = new MemoryStream();

            //using (StringWriter sw = new StringWriter())
            using (XmlTextWriter writer = new XmlTextWriter(memoryStream, new UTF8Encoding(false)))
            {
                //XmlTextWriter writer = new XmlTextWriter(sw);
                writer.Formatting = Formatting.Indented;
                
                writer.WriteStartDocument();

                if (_processorInfoFileName.Length > 0)
                {
                    // Add processor information to the XML file
                    String strPi = "type='text/xsl' href='" + _processorInfoFileName + "'";
                    // Write processor information
                    writer.WriteProcessingInstruction("xml-stylesheet", strPi);
                }

                foreach (XmlElement element in _xmlElements)
                {
                    writer.WriteStartElement(element.Name);

                    foreach (XmlAttribute attribute in element.Attributes)
                        writer.WriteAttributeString(attribute.Name, attribute.Value);
                }

                foreach (IXmlPart part in _xmlParts)
                    part.SharedObject = _sharedObject;

                foreach (IXmlPart part in _xmlParts)
                {
                    part.Fill();

                    string templateName = part.ReadTemplate();

                    if (templateName != string.Empty)
                    {
                        XmlNode node = (XmlNode)_xmlTemplates[templateName];
                        FillTemplate(node.FirstChild, writer, part);
                    }

                    //if (template != null)
                    //{
                    //    FillTemplate(template.DocumentElement.FirstChild, writer, part);
                    //}
                    if (_greenButtonDataType == "bill")
                        part.GenerateMeterBillXml(writer);
                    else
                        part.GenerateXml(writer);
                }

                // ReSharper disable once UnusedVariable
                foreach (XmlElement element in _xmlElements)
                    writer.WriteEndElement();

                writer.WriteEndDocument();
                writer.Flush();
                //xmlString = sw.ToString();
                //xmlString = Encoding.UTF8.GetString(memoryStream.ToArray());

            }
            xmlString = Encoding.UTF8.GetString(memoryStream.ToArray());
            return xmlString;
        }

        public string GenerateXmlInMemory(SyndicationFeed feed)
        {
            MemoryStream memoryStream = new MemoryStream();


            Atom10FeedFormatter feedFormatter = new Atom10FeedFormatter(feed);

            
            using (XmlTextWriter writer = new XmlTextWriter(memoryStream, new UTF8Encoding(false)))
            {
                writer.Formatting = Formatting.Indented;

                feedFormatter.WriteTo(writer);
                
                writer.Flush();

            }
            var xmlString = Encoding.UTF8.GetString(memoryStream.ToArray());
            return xmlString;
        }


        public string GenerateXmlInMemory(SyndicationItem entry)
        {
            MemoryStream memoryStream = new MemoryStream();


            Atom10ItemFormatter feedFormatter = new Atom10ItemFormatter(entry);

            using (XmlTextWriter writer = new XmlTextWriter(memoryStream, new UTF8Encoding(false)))
            {
                writer.Formatting = Formatting.Indented;

                feedFormatter.WriteTo(writer);

                writer.Flush();

            }
            var xmlString = Encoding.UTF8.GetString(memoryStream.ToArray());
            return xmlString;
        }

        public string GenerateXmlInMemory(object content)
        {
            MemoryStream memoryStream = new MemoryStream();

            XmlSerializer xmlSerializer = new XmlSerializer(content.GetType());

            using (XmlTextWriter writer = new XmlTextWriter(memoryStream, new UTF8Encoding(false)))
            {
                writer.Formatting = Formatting.Indented;
                
                xmlSerializer.Serialize(writer, content);

                writer.Flush();

            }
            var xmlString = Encoding.UTF8.GetString(memoryStream.ToArray());
            return xmlString;
        }


        private void FillTemplate(XmlNode node, XmlWriter writer, IXmlPart xmlPart)
        {
            if (node is System.Xml.XmlElement)
            {
                if (_excludeElements.Contains(node.Name) == false)
                {
                    writer.WriteStartElement(node.Name);

                    if (node.Attributes != null)
                    {
                        foreach (System.Xml.XmlAttribute attr in node.Attributes)
                        {
                            string value = GetTokenValue(attr.Value, xmlPart);
                            writer.WriteAttributeString(attr.Name, value);
                        }
                    }

                    if (node.HasChildNodes)
                        FillTemplate(node.FirstChild, writer, xmlPart);

                    writer.WriteEndElement();
                }

                if (node.NextSibling != null)
                    FillTemplate(node.NextSibling, writer, xmlPart);

            }
            else if (node is XmlText)
            {
                string text = GetTokenValue(((XmlText)node).Value, xmlPart);
                writer.WriteString(text);
            }
        }

        private void GenerateTemplates(string templates)
        {
            _xmlTemplates = new Hashtable();

            if (templates != string.Empty)
            {
                XmlDocument xmlTemplates = new XmlDocument();
                xmlTemplates.LoadXml(templates);

                XmlNodeList nodes = xmlTemplates.GetElementsByTagName("template");

                foreach (XmlNode template in nodes)
                {
                    if (template.Attributes != null && (template.Attributes.Count > 0 && template.Attributes["name"] != null))
                    {
                        string templateName = template.Attributes["name"].Value;

                        if (templateName != string.Empty)
                            _xmlTemplates.Add(templateName, template);
                    }
                }
            }
        }

        private string GetTokenValue(string token, IXmlPart part)
        {
            string value = token;

            if (token.StartsWith("%") && token.EndsWith("%"))
            {
                value = part.ReplaceToken(token);
            }

            return value;
        }
    }

    public class XmlElement
    {
        private string _name;
        private List<XmlAttribute> _attributes;

        public XmlElement(string name)
        {
            _name = name;
            _attributes = new List<XmlAttribute>();
        }

        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }

        public List<XmlAttribute> Attributes
        {
            get { return _attributes; }
            set { _attributes = value; }
        }
    }

    public class XmlAttribute
    {
        private string _name;
        private string _value;

        public XmlAttribute(string name)
        {
            _name = name;
        }

        public XmlAttribute(string name, string value)
            : this(name)
        {
            _value = value;
        }

        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }

        public string Value
        {
            get { return _value; }
            set { _value = value; }
        }

    }
}
