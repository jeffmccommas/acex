Imports CE.RateEngine.Enums

Public Class MasterServiceTerritory

	Private m_sCompanyID As String

	Private Sub New()
	End Sub

	Public Sub New(ByVal sCompanyID As String)
		m_sCompanyID = sCompanyID
	End Sub

#Region "Properties"

	Public ReadOnly Property CompanyID() As String
		Get
			Return (m_sCompanyID)
		End Get
	End Property

#End Region

End Class
