''' <summary>
''' Rate maintenance data collection. Contains rate maintenance data items.
''' </summary>
''' <remarks>None.</remarks>
<Serializable()> Public Class RateMaintDataCollection

    Inherits CollectionBase
    Implements ICloneable

    Public Function Clone() As Object Implements System.ICloneable.Clone

        Dim bf As System.Runtime.Serialization.Formatters.Binary.BinaryFormatter = _
                            New System.Runtime.Serialization.Formatters.Binary.BinaryFormatter
        Dim ms As System.IO.MemoryStream = New System.IO.MemoryStream
        Dim rateMaintDataColl As RateMaintDataCollection

        bf.Serialize(ms, Me)
        ms.Seek(0, System.IO.SeekOrigin.Begin)
        rateMaintDataColl = CType(bf.Deserialize(ms), RateMaintDataCollection)
        ms.Close()

        Return (rateMaintDataColl)

    End Function

    Default ReadOnly Property Item(ByVal index As Integer) As RateMaintData

        Get
            Return CType(Me.InnerList.Item(index), RateMaintData)
        End Get

    End Property

    Public Function Add(ByVal value As RateMaintData) As Integer

        Return Me.InnerList.Add(value)

    End Function

    Public Sub Insert(ByVal index As Integer, ByVal value As RateMaintData)

        Me.InnerList.Insert(index, value)

    End Sub

    Public Sub AddRange(ByVal values() As RateMaintData)

        Me.InnerList.AddRange(values)

    End Sub

    Public Sub Remove(ByVal value As RateMaintData)

        Me.InnerList.Remove(value)

    End Sub

    Public Sub Sort()

        Dim objSorter As IComparer = New DateSorter

        Me.InnerList.Sort(objSorter)

    End Sub

    Public Sub Sort(ByVal reverse As Boolean)

        Dim objSorter As IComparer = New DateSorter(reverse)

        Me.InnerList.Sort(objSorter)

    End Sub

    ''' <summary>
    ''' Embedded class to support sorting.
    ''' </summary>
    ''' <remarks>None.</remarks>
    Private Class DateSorter

        Implements IComparer

        Private _reverse As Boolean

        Public Sub New()
            '_reverse = False
        End Sub

        Public Sub New(ByVal reverse As Boolean)
            _reverse = reverse
        End Sub


        Public Property Reverse() As Boolean
            Get
                Return _reverse
            End Get
            Set(ByVal value As Boolean)
                _reverse = value
            End Set
        End Property

        Public Function Compare(ByVal x As Object, ByVal y As Object) As Integer _
                                        Implements System.Collections.IComparer.Compare

            Dim rateMaintDataA As RateMaintData = CType(x, RateMaintData)
            Dim rateMaintDataB As RateMaintData = CType(y, RateMaintData)
            Dim compareA As IComparable = CType(rateMaintDataA.EffectiveDate, IComparable)
            Dim compareB As IComparable = CType(rateMaintDataB.EffectiveDate, IComparable)
            Dim result As Integer

            'Swapping compare B and compare A makes it asc/desc switch.
            If (Not _reverse) Then
                result = compareA.CompareTo(compareB)
            Else
                result = compareB.CompareTo(compareA)
            End If

            Return (result)

        End Function

    End Class

End Class