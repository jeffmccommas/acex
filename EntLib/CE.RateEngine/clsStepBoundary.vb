Imports CE.RateEngine.Enums

Public Class StepBoundary
	Inherits BoundaryBase

	Private m_eStep As ServiceStep
	Private m_eSeason As Season
	Private m_dblThreshold As Double

	Private Sub New()
	End Sub

	Public Sub New(ByVal eStep As ServiceStep, ByVal eSeason As Season, ByVal dblThreshold As Double)
		MyBase.New()
		m_eStep = eStep
		m_eSeason = eSeason
		m_dblThreshold = dblThreshold
	End Sub

#Region "Properties"

	Public ReadOnly Property [Step]() As ServiceStep
		Get
			Return (m_eStep)
		End Get
	End Property

	Public ReadOnly Property Season() As Season
		Get
			Return (m_eSeason)
		End Get
	End Property

	Public Property Threshold() As Double
		Get
			Return m_dblThreshold
		End Get
		Set(ByVal Value As Double)
			m_dblThreshold = Value
		End Set
	End Property

#End Region

End Class
