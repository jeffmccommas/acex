Imports CE.RateEngine.Enums

Public Class FCRCollection
	Inherits CollectionBase

	Public Sub New()
	End Sub

	Default ReadOnly Property Item(ByVal index As Integer) As FCR
		Get
			Return CType(Me.InnerList.Item(index), FCR)
		End Get
	End Property

	Public Function Add(ByVal value As FCR) As Integer
		Return Me.InnerList.Add(value)
	End Function

	Public Sub Insert(ByVal index As Integer, ByVal value As FCR)
		Me.InnerList.Insert(index, value)
	End Sub

	Public Sub AddRange(ByVal values() As FCR)
		Me.InnerList.AddRange(values)
	End Sub

	Public Sub Remove(ByVal value As FCR)
		Me.InnerList.Remove(value)
	End Sub

	Public Function FindInclusiveFCR(ByVal dtDate As DateTime) As FCR
		Dim i As Integer
		Dim objTemp As FCR
        Dim objFCR As FCR = Nothing

		If (Me.InnerList.Count > 0) Then

			For i = 0 To Me.InnerList.Count - 1
				objTemp = CType(Me.InnerList.Item(i), FCR)
				If objTemp.StartDate <= dtDate Then
					objFCR = CType(Me.InnerList.Item(i), FCR)
					Exit For
				End If
			Next

		End If

		Return (objFCR)
    End Function

    Public Function FindInclusiveFCRChargeValue(ByVal dtDate As DateTime) As Double
        Dim i As Integer
        Dim objTemp As FCR
        Dim objFCR As FCR = Nothing
        Dim value As Double

        If (Me.InnerList.Count > 0) Then

            For i = 0 To Me.InnerList.Count - 1
                objTemp = CType(Me.InnerList.Item(i), FCR)
                If objTemp.StartDate <= dtDate Then
                    objFCR = CType(Me.InnerList.Item(i), FCR)
                    Exit For
                End If
            Next

        End If

        If (Not objFCR Is Nothing) Then
            value = objFCR.ChargeValue
        Else
            value = 0.0
        End If

        Return (value)
    End Function

    ''' <summary>
    ''' Do not use this since it may interfere with caching.
    ''' FCR collections can be retrieved from cache; use GetTrimmedFCRCollection instead to get a new trimmed collection.
    ''' </summary>
    ''' <param name="prorate"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <Obsolete()> Public Function TrimByProrate(ByVal prorate As ProrateType) As Boolean
        Dim i As Integer
        Dim obj As FCR
        Dim objFCR As FCR = Nothing

        ' Nothing to trim if there is only one item;
        ' Note that the item and index zero is the newest fcr, and the item at (count-1) is the oldest;
        If (Me.InnerList.Count > 1) Then

            Select Case prorate
                Case ProrateType.UseLatest
                    ' keep the newest
                    For i = Me.InnerList.Count - 1 To 1 Step -1
                        obj = CType(Me.InnerList.Item(i), FCR)
                        If (Not obj Is Nothing) Then
                            Me.InnerList.RemoveAt(i)
                        End If
                    Next

                Case ProrateType.UsePrevious
                    ' keep the oldest
                    For i = Me.InnerList.Count - 2 To 0 Step -1
                        obj = CType(Me.InnerList.Item(i), FCR)
                        If (Not obj Is Nothing) Then
                            Me.InnerList.RemoveAt(i)
                        End If
                    Next

                Case ProrateType.SplitBillDays
                    ' noop
            End Select

        End If

        Return (True)
    End Function

    ''' <summary>
    ''' Get a new reference to an FCR collection that may be trimmed version of original used for call.
    ''' </summary>
    ''' <param name="prorate"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function GetTrimmedFCRCollection(ByVal prorate As ProrateType) As FCRCollection
        Dim newFCRList As FCRCollection = Nothing

        ' Nothing to trim if there is only one item;
        ' Note that the item and index zero is the newest fcr, and the item at (count-1) is the oldest;
        If (Me.InnerList.Count > 1) Then

            Select Case prorate
                Case ProrateType.UseLatest
                    ' keep the newest
                    newFCRList = New FCRCollection()
                    newFCRList.Add(CType(Me.InnerList.Item(0), FCR))

                Case ProrateType.UsePrevious
                    ' keep the oldest
                    newFCRList = New FCRCollection()
                    newFCRList.Add(CType(Me.InnerList.Item(Me.InnerList.Count - 1), FCR))

                Case ProrateType.SplitBillDays
                    ' basically return this (Me)
                    newFCRList = Me

                Case Else
                    ' basically return this (Me)
                    newFCRList = Me

            End Select

        Else
            newFCRList = Me
        End If

        Return (newFCRList)
    End Function

End Class
