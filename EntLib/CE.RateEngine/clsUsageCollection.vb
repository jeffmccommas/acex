Imports CE.RateEngine.Enums

<Serializable()> Public Class UsageCollection
    Inherits CollectionBase
    Implements ICloneable

    Dim objUsages As Boolean

    Public Sub New()
    End Sub

    Default ReadOnly Property Item(ByVal index As Integer) As Usage
        Get
            Return CType(Me.InnerList.Item(index), Usage)
        End Get
    End Property

    Public Function Add(ByVal value As Usage) As Integer
        Return Me.InnerList.Add(value)
    End Function

    Public Sub Insert(ByVal index As Integer, ByVal value As Usage)
        Me.InnerList.Insert(index, value)
    End Sub

    Public Sub AddRange(ByVal values() As Usage)
        Me.InnerList.AddRange(values)
    End Sub

    Public Sub Remove(ByVal value As Usage)
        Me.InnerList.Remove(value)
    End Sub

    Public Sub Combine(ByVal usages As UsageCollection)
        Dim temp As Usage

        For Each u As Usage In usages

            temp = FindByParts(u.BaseOrTier, u.TimeOfUse, u.Season)

            If (Not temp Is Nothing) Then
                temp.Quantity = temp.Quantity + u.Quantity
            Else
                Me.InnerList.Add(u)
            End If

        Next

    End Sub

    Public Sub Combine(ByVal b As BaseOrTier, ByVal t As TimeOfUse, ByVal s As Season, ByVal quantity As Double)
        Dim temp As Usage

        temp = FindByParts(b, t, s)

        If (Not temp Is Nothing) Then
            temp.Quantity = temp.Quantity + quantity
        Else
            Me.InnerList.Add(New Usage(b, t, s, quantity))
        End If

    End Sub

    ' this combines Usage by SUMMING; and combines demand by MAXING
    Public Sub CombineUsageBySumAndDemandByMax(ByVal usages As UsageCollection)
        Dim temp As Usage

        For Each u As Usage In usages

            temp = FindByParts(u.BaseOrTier, u.TimeOfUse, u.Season)

            If (Not temp Is Nothing) Then
                If (temp.BaseOrTier <> BaseOrTier.Demand And temp.BaseOrTier <> BaseOrTier.DemandBill And temp.BaseOrTier <> BaseOrTier.DemandBill2 And temp.BaseOrTier <> BaseOrTier.DemandBill3) Then
                    temp.Quantity = temp.Quantity + u.Quantity
                Else
                    temp.Quantity = Math.Max(temp.Quantity, u.Quantity)
                End If
            Else
                Me.InnerList.Add(u)
            End If

        Next

    End Sub


    Public Function Exists(ByVal t As BaseOrTier, ByVal tou As TimeOfUse, ByVal s As Season) As Boolean
        Dim i As Integer
        Dim objTemp As Usage
        Dim bExists As Boolean

        If (Me.InnerList.Count > 0) Then

            For i = 0 To Me.InnerList.Count - 1
                objTemp = CType(Me.InnerList.Item(i), Usage)
                If objTemp.BaseOrTier = t And objTemp.TimeOfUse = tou And objTemp.Season = s Then
                    bExists = True
                    Exit For
                End If
            Next

        End If

        Return (bExists)
    End Function

    ''' <summary>
    ''' Finding existing usage that is any tou and any season but matched the baseOrTier.
    ''' </summary>
    ''' <param name="t"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function Exists(ByVal t As BaseOrTier) As Boolean
        Dim i As Integer
        Dim u As Usage
        Dim usageExists As Boolean

        If (Me.InnerList.Count > 0) Then

            For i = 0 To Me.InnerList.Count - 1
                u = CType(Me.InnerList.Item(i), Usage)
                If (u.BaseOrTier = t) Then
                    usageExists = True
                    Exit For
                End If
            Next

        End If

        Return (usageExists)
    End Function

    ' check if any usage with tou exists
    Public Function Exists(ByVal tou As TimeOfUse) As Boolean
        Dim i As Integer
        Dim u As Usage
        Dim bExists As Boolean

        If (Me.InnerList.Count > 0) Then

            For i = 0 To Me.InnerList.Count - 1
                u = CType(Me.InnerList.Item(i), Usage)
                If (u.TimeOfUse = tou) Then
                    bExists = True
                    Exit For
                End If
            Next

        End If

        Return (bExists)
    End Function

    ' check if any usage with season exists
    Public Function Exists(ByVal season As Season) As Boolean
        Dim i As Integer
        Dim u As Usage
        Dim bExists As Boolean

        If (Me.InnerList.Count > 0) Then

            For i = 0 To Me.InnerList.Count - 1
                u = CType(Me.InnerList.Item(i), Usage)
                If (u.Season = season) Then
                    bExists = True
                    Exit For
                End If
            Next

        End If

        Return (bExists)
    End Function

    ''' <summary>
    ''' Check to see if any usage with tou defined exists.  Seasonal can exist or not.
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function ExistsAnyTOU() As Boolean
        Dim i As Integer
        Dim u As Usage
        Dim exists As Boolean = False

        If (Me.InnerList.Count > 0) Then

            For i = 0 To Me.InnerList.Count - 1
                u = CType(Me.InnerList.Item(i), Usage)

                ' TOU seasonal exist
                If (u.TimeOfUse = TimeOfUse.OnPeak Or _
                    u.TimeOfUse = TimeOfUse.OffPeak Or _
                    u.TimeOfUse = TimeOfUse.Shoulder1 Or _
                    u.TimeOfUse = TimeOfUse.Shoulder2 Or _
                    u.TimeOfUse = TimeOfUse.CriticalPeak) Then

                    exists = True
                    Exit For

                End If

            Next

        End If

        Return (exists)
    End Function

    ''' <summary>
    ''' Check to see if any usage with tier and tou defined exists.  Seasonal can exist or not.
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function ExistsAnyTierWithAnyTOU() As Boolean
        Dim i As Integer
        Dim u As Usage
        Dim exists As Boolean = False

        If (Me.InnerList.Count > 0) Then

            For i = 0 To Me.InnerList.Count - 1
                u = CType(Me.InnerList.Item(i), Usage)

                ' tier + TOU seasonal exist together
                If ((u.BaseOrTier = BaseOrTier.Tier1 Or _
                    u.BaseOrTier = BaseOrTier.Tier2 Or _
                    u.BaseOrTier = BaseOrTier.Tier3 Or _
                    u.BaseOrTier = BaseOrTier.Tier4 Or _
                    u.BaseOrTier = BaseOrTier.Tier5 Or _
                    u.BaseOrTier = BaseOrTier.Tier6) AndAlso _
                   (u.TimeOfUse = TimeOfUse.OnPeak Or _
                    u.TimeOfUse = TimeOfUse.OffPeak Or _
                    u.TimeOfUse = TimeOfUse.Shoulder1 Or _
                    u.TimeOfUse = TimeOfUse.Shoulder2 Or _
                    u.TimeOfUse = TimeOfUse.CriticalPeak)) Then

                    exists = True
                    Exit For

                End If

            Next

        End If

        Return (exists)
    End Function

    ''' <summary>
    ''' Check to see if any seasonal tiered usage exists.
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function ExistsAnyTierWithAnySeason() As Boolean
        Dim i As Integer
        Dim u As Usage
        Dim exists As Boolean = False

        If (Me.InnerList.Count > 0) Then

            For i = 0 To Me.InnerList.Count - 1
                u = CType(Me.InnerList.Item(i), Usage)

                ' tier + TOU seasonal exist together
                If ((u.BaseOrTier = BaseOrTier.Tier1 Or _
                    u.BaseOrTier = BaseOrTier.Tier2 Or _
                    u.BaseOrTier = BaseOrTier.Tier3 Or _
                    u.BaseOrTier = BaseOrTier.Tier4 Or _
                    u.BaseOrTier = BaseOrTier.Tier5 Or _
                    u.BaseOrTier = BaseOrTier.Tier6) AndAlso _
                   (u.Season <> Season.Undefined)) Then

                    exists = True
                    Exit For

                End If

            Next

        End If

        Return (exists)
    End Function

    Public Function FindByParts(ByVal t As BaseOrTier, ByVal tou As TimeOfUse, ByVal s As Season) As Usage
        Dim i As Integer
        Dim objTemp As Usage
        Dim objUsage As Usage= Nothing

        If (Me.InnerList.Count > 0) Then

            For i = 0 To Me.InnerList.Count - 1
                objTemp = CType(Me.InnerList.Item(i), Usage)
                If objTemp.BaseOrTier = t And objTemp.TimeOfUse = tou And objTemp.Season = s Then
                    objUsage = CType(Me.InnerList.Item(i), Usage)
                    Exit For
                End If
            Next

        End If

        Return (objUsage)
    End Function

    Public Function FindFirstUsageWithTimeOfUse(ByVal tou As TimeOfUse) As Usage
        Dim i As Integer
        Dim temp As Usage
        Dim u As Usage= Nothing

        If (Me.InnerList.Count > 0) Then

            For i = 0 To Me.InnerList.Count - 1
                temp = CType(Me.InnerList.Item(i), Usage)
                If (temp.TimeOfUse = tou) Then
                    u = CType(Me.InnerList.Item(i), Usage)
                    Exit For
                End If
            Next

        End If

        Return (u)
    End Function

    Public Function FindFirstUsageWithTOUAndSeason(ByVal tou As TimeOfUse, ByVal s As Season) As Usage
        Dim i As Integer
        Dim temp As Usage
        Dim u As Usage= Nothing

        If (Me.InnerList.Count > 0) Then

            For i = 0 To Me.InnerList.Count - 1
                temp = CType(Me.InnerList.Item(i), Usage)
                If (temp.TimeOfUse = tou AndAlso temp.Season = s) Then
                    u = CType(Me.InnerList.Item(i), Usage)
                    Exit For
                End If
            Next

        End If

        Return (u)
    End Function

    Public Function GetAllUsagesWithTimeOfUse(ByVal tou As TimeOfUse) As UsageCollection
        Dim usages As New UsageCollection
        Dim i As Integer
        Dim u As Usage

        If (Me.InnerList.Count > 0) Then

            For i = 0 To Me.InnerList.Count - 1
                u = CType(Me.InnerList.Item(i), Usage)
                If (u.TimeOfUse = tou) Then
                    usages.Add(u)
                End If
            Next

        End If

        Return (usages)
    End Function

    ''' <summary>
    ''' helper function to return usage collection for matched tou and season
    ''' CR 52497 Nov 2014
    ''' </summary>
    ''' <param name="tou"></param>
    ''' <param name="season"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function GetAllUsagesWithTimeOfUseAndSeason(ByVal tou As TimeOfUse, season As Season) As UsageCollection
        Dim usages As New UsageCollection
        Dim i As Integer
        Dim u As Usage

        If (Me.InnerList.Count > 0) Then

            For i = 0 To Me.InnerList.Count - 1
                u = CType(Me.InnerList.Item(i), Usage)
                If (u.TimeOfUse = tou AndAlso u.Season = season) Then
                    usages.Add(u)
                End If
            Next

        End If

        Return (usages)
    End Function

    ''' <summary>
    ''' helper functions to return usages collection with all tiers
    ''' CR 52497 Nov 2014
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function GetAllUsagesWithTiers() As UsageCollection
        Dim usages As New UsageCollection
        Dim i As Integer
        Dim u As Usage

        If (Me.InnerList.Count > 0) Then

            For i = 0 To Me.InnerList.Count - 1
                u = CType(Me.InnerList.Item(i), Usage)
                If (u.BaseOrTier = Enums.BaseOrTier.Tier1 _
                    OrElse u.BaseOrTier = Enums.BaseOrTier.Tier2 _
                    OrElse u.BaseOrTier = Enums.BaseOrTier.Tier3 _
                    OrElse u.BaseOrTier = Enums.BaseOrTier.Tier4 _
                    OrElse u.BaseOrTier = Enums.BaseOrTier.Tier5 _
                    OrElse u.BaseOrTier = Enums.BaseOrTier.Tier6) Then
                    usages.Add(u)
                End If
            Next

        End If

        Return (usages)
    End Function

    Public Function GetDemandUsage() As UsageCollection
        Dim objUsages As New UsageCollection
        Dim i As Integer
        Dim objUsage As Usage

        If (Me.InnerList.Count > 0) Then

            For i = 0 To Me.InnerList.Count - 1
                objUsage = CType(Me.InnerList.Item(i), Usage)
                If (objUsage.BaseOrTier = BaseOrTier.Demand Or objUsage.BaseOrTier = BaseOrTier.DemandBill Or objUsage.BaseOrTier = BaseOrTier.DemandBill2 Or objUsage.BaseOrTier = BaseOrTier.DemandBill3) Then
                    objUsages.Add(objUsage)
                End If
            Next

        End If

        Return (objUsages)
    End Function

    ''' <summary>
    ''' This will actually take the max for specific season and undefined season.
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function GetTotalDemandBillUsage(s As Season) As Double
        Dim dblDemandBill As Double = 0.0
        Dim i As Integer
        Dim u As Usage

        If (Me.InnerList.Count > 0) Then

            For i = 0 To Me.InnerList.Count - 1
                u = CType(Me.InnerList.Item(i), Usage)
                If (u.BaseOrTier = BaseOrTier.DemandBill AndAlso (u.Season = s Or u.Season = Season.Undefined Or s = Season.Undefined)) Then
                    dblDemandBill = Math.Max(dblDemandBill, u.Quantity)
                End If
            Next

        End If

        Return (dblDemandBill)
    End Function

    ''' <summary>
    ''' DemandBill2 specific.  This will actually take the max for specific season and undefined season.
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function GetTotalDemandBill2Usage(s As Season) As Double
        Dim dblDemandBill2 As Double = 0.0
        Dim i As Integer
        Dim u As Usage

        If (Me.InnerList.Count > 0) Then

            For i = 0 To Me.InnerList.Count - 1
                u = CType(Me.InnerList.Item(i), Usage)
                If (u.BaseOrTier = BaseOrTier.DemandBill2 AndAlso (u.Season = s Or u.Season = Season.Undefined Or s = Season.Undefined)) Then
                    dblDemandBill2 = Math.Max(dblDemandBill2, u.Quantity)
                End If
            Next

        End If

        Return (dblDemandBill2)
    End Function

    ''' <summary>
    ''' DemandBill3 specific.  This will actually take the max for specific season and undefined season.
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function GetTotalDemandBill3Usage(season As Season) As Double
        Dim dblDemandBill3 As Double = 0.0
        Dim index As Integer
        Dim usage As Usage

        If (Me.InnerList.Count > 0) Then

            For index = 0 To Me.InnerList.Count - 1
                usage = CType(Me.InnerList.Item(index), Usage)
                If (usage.BaseOrTier = BaseOrTier.DemandBill3 AndAlso
                   (usage.Season = season Or usage.Season = Season.Undefined Or
                    season = Season.Undefined)) Then
                    dblDemandBill3 = Math.Max(dblDemandBill3, usage.Quantity)
                End If
            Next

        End If

        Return (dblDemandBill3)
    End Function

    ''' <summary>
    ''' This will take overall total, regardless of season.
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function GetTotalDemandBillUsage() As Double
        Dim dblDemandBill As Double = 0.0
        Dim i As Integer
        Dim u As Usage

        If (Me.InnerList.Count > 0) Then

            For i = 0 To Me.InnerList.Count - 1
                u = CType(Me.InnerList.Item(i), Usage)
                If (u.BaseOrTier = BaseOrTier.DemandBill) Then
                    dblDemandBill = Math.Max(dblDemandBill, u.Quantity)
                End If
            Next

        End If

        Return (dblDemandBill)
    End Function

    ''' <summary>
    ''' DemandBill2 specific.  This will take overall total, regardless of season.
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function GetTotalDemandBill2Usage() As Double
        Dim dblDemandBill2 As Double = 0.0
        Dim i As Integer
        Dim u As Usage

        If (Me.InnerList.Count > 0) Then

            For i = 0 To Me.InnerList.Count - 1
                u = CType(Me.InnerList.Item(i), Usage)
                If (u.BaseOrTier = BaseOrTier.DemandBill2) Then
                    dblDemandBill2 = Math.Max(dblDemandBill2, u.Quantity)
                End If
            Next

        End If

        Return (dblDemandBill2)
    End Function

    ''' <summary>
    ''' This will actually take the max for specific season and undefined season.
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function GetTotalDemandUsage(s As Season) As Double
        Dim dblDemand As Double = 0.0
        Dim i As Integer
        Dim u As Usage

        If (Me.InnerList.Count > 0) Then

            For i = 0 To Me.InnerList.Count - 1
                u = CType(Me.InnerList.Item(i), Usage)
                If (u.BaseOrTier = BaseOrTier.Demand AndAlso (u.Season = s Or u.Season = Season.Undefined Or s = Season.Undefined)) Then
                    dblDemand = Math.Max(dblDemand, u.Quantity)
                End If
            Next

        End If

        Return (dblDemand)
    End Function

    ''' <summary>
    ''' Gets total regardless of season.
    ''' </summary>
    Public Function GetTotalDemandUsage() As Double
        Dim dblDemand As Double = 0.0
        Dim i As Integer
        Dim u As Usage

        If (Me.InnerList.Count > 0) Then

            For i = 0 To Me.InnerList.Count - 1
                u = CType(Me.InnerList.Item(i), Usage)
                If (u.BaseOrTier = BaseOrTier.Demand) Then
                    dblDemand = Math.Max(dblDemand, u.Quantity)
                End If
            Next

        End If

        Return (dblDemand)
    End Function

    ' Only use this for Service Charges calculations
    Public Function GetTotalUsage() As Double
        Dim dblTotal, dblOther As Double
        Dim i As Integer
        Dim objUsage As Usage
        Dim bTotalServiceUseExists As Boolean

        If (Me.InnerList.Count > 0) Then

            For i = 0 To Me.InnerList.Count - 1
                objUsage = CType(Me.InnerList.Item(i), Usage)
                If (objUsage.BaseOrTier = BaseOrTier.TotalServiceUse) Then
                    dblTotal = dblTotal + objUsage.Quantity
                    bTotalServiceUseExists = True
                ElseIf (objUsage.BaseOrTier = BaseOrTier.Undefined) Then
                    dblOther = dblOther + objUsage.Quantity
                End If
            Next

            If (Not bTotalServiceUseExists) Then
                dblTotal = dblOther
            End If

        End If

        Return (dblTotal)
    End Function

    Public Function GetTotalServiceUse() As Double
        Dim dblTotal As Double
        Dim alternateTotal As Double
        Dim i As Integer
        Dim objUsage As Usage
        Dim regularTotalExists As Boolean

        If (Me.InnerList.Count > 0) Then
            For i = 0 To Me.InnerList.Count - 1
                objUsage = CType(Me.InnerList.Item(i), Usage)
                If (objUsage.BaseOrTier = BaseOrTier.TotalServiceUse And objUsage.TimeOfUse = TimeOfUse.Undefined And objUsage.Season = Season.Undefined) Then
                    dblTotal = dblTotal + objUsage.Quantity
                    regularTotalExists = True
                ElseIf (objUsage.BaseOrTier = BaseOrTier.TotalServiceUse And objUsage.TimeOfUse = TimeOfUse.Undefined And objUsage.Season <> Season.Undefined) Then
                    alternateTotal = alternateTotal + objUsage.Quantity
                End If
            Next
        End If

        If (Not regularTotalExists) Then
            dblTotal = alternateTotal
        End If

        Return (dblTotal)
    End Function

    Public Function GetTotalServiceUse(season As Season) As Double
        Dim dblTotal As Double
        Dim i As Integer
        Dim objUsage As Usage

        If (Me.InnerList.Count > 0) Then
            For i = 0 To Me.InnerList.Count - 1
                objUsage = CType(Me.InnerList.Item(i), Usage)
                If (objUsage.BaseOrTier = BaseOrTier.TotalServiceUse And objUsage.Season = season) Then
                    dblTotal = dblTotal + objUsage.Quantity
                End If
            Next
        End If

        Return (dblTotal)
    End Function

    Public Function GetTotalTimeOfUseUsage(ByVal tou As TimeOfUse) As Double
        Dim dblTotal As Double
        Dim i As Integer
        Dim objUsage As Usage

        If (Me.InnerList.Count > 0) Then
            For i = 0 To Me.InnerList.Count - 1
                objUsage = CType(Me.InnerList.Item(i), Usage)
                If (objUsage.BaseOrTier = BaseOrTier.Undefined And objUsage.TimeOfUse = tou) Then
                    dblTotal = dblTotal + objUsage.Quantity
                End If
            Next
        End If

        Return (dblTotal)
    End Function

    Public Function GetTotalTimeOfUseUsage() As Double
        Dim dblTotal As Double
        Dim i As Integer
        Dim objUsage As Usage

        If (Me.InnerList.Count > 0) Then
            For i = 0 To Me.InnerList.Count - 1
                objUsage = CType(Me.InnerList.Item(i), Usage)
                If (objUsage.BaseOrTier = BaseOrTier.Undefined And objUsage.TimeOfUse <> TimeOfUse.Undefined) Then
                    dblTotal = dblTotal + objUsage.Quantity
                End If
            Next
        End If

        Return (dblTotal)
    End Function

    Public Function GetTotalTimeOfUseUsageForSeason(ByVal tou As TimeOfUse, ByVal s As Season) As Double
        Dim dblTotal As Double = 0
        Dim alternateTierTotal As Double = 0
        Dim regularTotalExists As Boolean = False
        Dim i As Integer
        Dim objUsage As Usage

        If (Me.InnerList.Count > 0) Then
            For i = 0 To Me.InnerList.Count - 1
                objUsage = CType(Me.InnerList.Item(i), Usage)

                ' regular TOU + seasonal total
                If (objUsage.BaseOrTier = BaseOrTier.Undefined And objUsage.TimeOfUse = tou And objUsage.Season = s) Then
                    dblTotal = dblTotal + objUsage.Quantity
                    regularTotalExists = True
                End If

                ' tier + TOU seasonal total
                If ((objUsage.BaseOrTier = BaseOrTier.Tier1 Or _
                    objUsage.BaseOrTier = BaseOrTier.Tier2 Or _
                    objUsage.BaseOrTier = BaseOrTier.Tier3 Or _
                    objUsage.BaseOrTier = BaseOrTier.Tier4 Or _
                    objUsage.BaseOrTier = BaseOrTier.Tier5 Or _
                    objUsage.BaseOrTier = BaseOrTier.Tier6) And objUsage.TimeOfUse = tou And objUsage.Season = s) Then
                    alternateTierTotal = alternateTierTotal + objUsage.Quantity
                End If

            Next

            ' use regular total and fallback to alternate tier total if regular total did not exist
            If (Not regularTotalExists) Then
                dblTotal = alternateTierTotal
            End If

        End If

        Return (dblTotal)
    End Function

    Public Function GetNonDemandUsage() As UsageCollection
        Dim objUsages As New UsageCollection
        Dim i As Integer
        Dim objUsage As Usage

        If (Me.InnerList.Count > 0) Then

            For i = 0 To Me.InnerList.Count - 1
                objUsage = CType(Me.InnerList.Item(i), Usage)
                If (objUsage.BaseOrTier <> BaseOrTier.Demand And objUsage.BaseOrTier <> BaseOrTier.DemandBill And objUsage.BaseOrTier <> BaseOrTier.DemandBill2 And objUsage.BaseOrTier <> BaseOrTier.DemandBill3) Then
                    objUsages.Add(objUsage)
                End If
            Next

        End If

        Return (objUsages)
    End Function

    Public Function GetTimeOfUseUsage(ByVal bExcludeDemand As Boolean) As UsageCollection
        Dim objUsages As New UsageCollection
        Dim i As Integer
        Dim objUsage As Usage

        If (Me.InnerList.Count > 0) Then

            For i = 0 To Me.InnerList.Count - 1
                objUsage = CType(Me.InnerList.Item(i), Usage)
                If (objUsage.TimeOfUse <> TimeOfUse.Undefined) Then
                    If (bExcludeDemand) Then
                        If (objUsage.BaseOrTier <> BaseOrTier.Demand And objUsage.BaseOrTier <> BaseOrTier.DemandBill And objUsage.BaseOrTier <> BaseOrTier.DemandBill2 And objUsage.BaseOrTier <> BaseOrTier.DemandBill3) Then
                            objUsages.Add(objUsage)
                        End If
                    Else
                        objUsages.Add(objUsage)
                    End If
                End If
            Next

        End If

        Return (objUsages)
    End Function

    ' This gets all Non TOU Usage, with TOU demand parts optionally included
    Public Function GetNonTimeOfUseUsage(ByVal bIncludeTOUDemand As Boolean) As UsageCollection
        Dim objUsages As New UsageCollection
        Dim i As Integer
        Dim objUsage As Usage

        If (Me.InnerList.Count > 0) Then

            For i = 0 To Me.InnerList.Count - 1
                objUsage = CType(Me.InnerList.Item(i), Usage)
                If (objUsage.TimeOfUse = TimeOfUse.Undefined) Then
                    objUsages.Add(objUsage)
                Else
                    If (bIncludeTOUDemand) Then
                        If (objUsage.BaseOrTier = BaseOrTier.Demand Or objUsage.BaseOrTier = BaseOrTier.DemandBill Or objUsage.BaseOrTier = BaseOrTier.DemandBill2 Or objUsage.BaseOrTier = BaseOrTier.DemandBill3) Then
                            objUsages.Add(objUsage)
                        End If
                    End If
                End If
            Next

        End If

        Return (objUsages)
    End Function

    Public Sub ClearTOUUsage()
        Dim i As Integer
        Dim obj As Usage

        If (Me.InnerList.Count > 0) Then
            For i = Me.InnerList.Count - 1 To 0 Step -1
                obj = CType(Me.InnerList.Item(i), Usage)
                If (Not obj Is Nothing) Then
                    If (obj.TimeOfUse <> TimeOfUse.Undefined) Then
                        Me.InnerList.RemoveAt(i)
                    End If
                End If
            Next
        End If

    End Sub

    Public Function GetLoadTypeUsageExclude(ByVal eExcludeLoadType As LoadType) As UsageCollection
        Dim objUsages As New UsageCollection
        Dim i As Integer
        Dim objUsage As Usage

        If (Me.InnerList.Count > 0) Then

            For i = 0 To Me.InnerList.Count - 1
                objUsage = CType(Me.InnerList.Item(i), Usage)
                If (objUsage.LoadType <> eExcludeLoadType) Then
                    objUsages.Add(objUsage)
                End If
            Next

        End If

        Return (objUsages)
    End Function

    Public Function GetLoadTypeUsage(ByVal eLoadType As LoadType) As UsageCollection
        Dim objUsages As New UsageCollection
        Dim i As Integer
        Dim objUsage As Usage

        If (Me.InnerList.Count > 0) Then

            For i = 0 To Me.InnerList.Count - 1
                objUsage = CType(Me.InnerList.Item(i), Usage)
                If (objUsage.LoadType = eLoadType) Then
                    objUsages.Add(objUsage)
                End If
            Next

        End If

        Return (objUsages)
    End Function

    ' Excess usage, exclude demand
    Public Function GetNegativeUsage() As UsageCollection
        Dim objUsages As New UsageCollection
        Dim i As Integer
        Dim objUsage As Usage

        If (Me.InnerList.Count > 0) Then

            For i = 0 To Me.InnerList.Count - 1
                objUsage = CType(Me.InnerList.Item(i), Usage)

                'must be negative, and not demand
                If (objUsage.Quantity < 0 And objUsage.BaseOrTier <> BaseOrTier.Demand And objUsage.BaseOrTier <> BaseOrTier.DemandBill And objUsage.BaseOrTier <> BaseOrTier.DemandBill2 And objUsage.BaseOrTier <> BaseOrTier.DemandBill3) Then

                    objUsages.Add(New Usage(objUsage.BaseOrTier, objUsage.TimeOfUse, objUsage.Season, objUsage.Quantity))
                End If

            Next

        End If

        Return (objUsages)
    End Function

    ' every negative usage pin to zero
    Public Function ZeroNegativeUsage() As Boolean
        Dim i As Integer
        Dim objUsage As Usage

        If (Me.InnerList.Count > 0) Then

            For i = 0 To Me.InnerList.Count - 1
                objUsage = CType(Me.InnerList.Item(i), Usage)

                'must be negative, and not demand
                If (objUsage.Quantity < 0 And objUsage.BaseOrTier <> BaseOrTier.Demand And objUsage.BaseOrTier <> BaseOrTier.DemandBill And objUsage.BaseOrTier <> BaseOrTier.DemandBill2 And objUsage.BaseOrTier <> BaseOrTier.DemandBill3) Then
                    objUsage.Quantity = 0.0
                End If

            Next

        End If

        Return (True)
    End Function

    ' Combine excess
    Public Sub CombineExcess(ByVal usages As UsageCollection)
        Dim temp As Usage

        If (Not usages Is Nothing) Then

            For Each u As Usage In usages
                temp = FindByParts(u.BaseOrTier, u.TimeOfUse, u.Season)

                If (Not temp Is Nothing) Then
                    temp.Quantity = temp.Quantity + (u.Quantity)
                Else
                    Me.InnerList.Add(u)
                End If

            Next

        End If

    End Sub

    ' Discard Usage that is undefined, undefined, undefined; (unmapped to specific parts; a.k.a Unknown Usage)
    Public Sub DiscardUnmappedUsage()
        Dim i As Integer
        Dim obj As Usage

        If (Me.InnerList.Count > 0) Then
            For i = Me.InnerList.Count - 1 To 0 Step -1
                obj = CType(Me.InnerList.Item(i), Usage)
                If (Not obj Is Nothing) Then
                    If (obj.BaseOrTier = BaseOrTier.Undefined And obj.TimeOfUse = TimeOfUse.Undefined And obj.Season = Season.Undefined) Then
                        Me.InnerList.RemoveAt(i)
                    End If
                End If
            Next
        End If

    End Sub

    ''' <summary>
    ''' Scale all except for demand usages.
    ''' </summary>
    ''' <param name="scaleFactor"></param>
    ''' <remarks></remarks>
    Public Sub Scale(ByVal scaleFactor As Double)
        Dim i As Integer
        Dim objUsage As Usage

        If (Me.InnerList.Count > 0) Then
            For i = 0 To Me.InnerList.Count - 1
                objUsage = CType(Me.InnerList.Item(i), Usage)
                If (objUsage.BaseOrTier <> BaseOrTier.Demand And objUsage.BaseOrTier <> BaseOrTier.DemandBill And objUsage.BaseOrTier <> BaseOrTier.DemandBill2 And objUsage.BaseOrTier <> BaseOrTier.DemandBill3) Then
                    objUsage.Quantity = objUsage.Quantity * scaleFactor
                End If
            Next
        End If

    End Sub

    ''' <summary>
    ''' Scale just those usages that are TotalServiceUse.
    ''' This does not scale demand.
    ''' </summary>
    ''' <param name="scaleFactor"></param>
    ''' <remarks></remarks>
    Public Sub ScaleTotal(ByVal scaleFactor As Double)
        Dim i As Integer
        Dim u As Usage

        If (Me.InnerList.Count > 0) Then
            For i = 0 To Me.InnerList.Count - 1
                u = CType(Me.InnerList.Item(i), Usage)
                If (u.BaseOrTier = BaseOrTier.TotalServiceUse) Then
                    u.Quantity = u.Quantity * scaleFactor
                End If
            Next
        End If

    End Sub

    ''' <summary>
    ''' Scale just those usages that are non tiered.
    ''' This does not scale demand.
    ''' </summary>
    ''' <param name="scaleFactor"></param>
    ''' <remarks></remarks>
    Public Sub ScaleNonTieredTimeOfUse(ByVal scaleFactor As Double)
        Dim i As Integer
        Dim u As Usage

        If (Me.InnerList.Count > 0) Then
            For i = 0 To Me.InnerList.Count - 1
                u = CType(Me.InnerList.Item(i), Usage)
                If (u.BaseOrTier = BaseOrTier.Undefined And u.TimeOfUse <> TimeOfUse.Undefined) Then
                    u.Quantity = u.Quantity * scaleFactor
                End If
            Next
        End If

    End Sub

    ''' <summary>
    ''' This creates a scaled difference quanity for each usage
    ''' </summary>
    ''' <param name="scaleFactor"></param>
    ''' <remarks></remarks>
    Public Function DifferenceOfScaleNonTieredTimeOfUse(ByVal scaleFactor As Double) As UsageCollection
        Dim usages As New UsageCollection
        Dim i As Integer
        Dim temp As Usage
        Dim u As Usage

        If (Me.InnerList.Count > 0) Then
            For i = 0 To Me.InnerList.Count - 1
                temp = CType(Me.InnerList.Item(i), Usage)
                If (temp.BaseOrTier = BaseOrTier.Undefined And temp.TimeOfUse <> TimeOfUse.Undefined) Then
                    u = New Usage(temp)
                    u.Quantity = (u.Quantity * scaleFactor) - u.Quantity
                    usages.Add(u)
                End If
            Next
        End If

        Return (usages)
    End Function

    ''' <summary>
    ''' Scale just demand usages.
    ''' </summary>
    ''' <param name="scaleFactor"></param>
    ''' <remarks></remarks>
    Public Sub ScaleDemand(ByVal scaleFactor As Double)
        Dim i As Integer
        Dim u As Usage

        If (Me.InnerList.Count > 0) Then
            For i = 0 To Me.InnerList.Count - 1
                u = CType(Me.InnerList.Item(i), Usage)
                If (u.BaseOrTier = BaseOrTier.Demand Or u.BaseOrTier = BaseOrTier.DemandBill Or u.BaseOrTier = BaseOrTier.DemandBill2 Or u.BaseOrTier = BaseOrTier.DemandBill3) Then
                    u.Quantity = u.Quantity * scaleFactor
                End If
            Next
        End If

    End Sub

    ''' <summary>
    ''' Get the highest tier number present in the usages, for the given season.
    ''' Returns 0 if no tiers exist for the given season
    ''' </summary>
    ''' <param name="s"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function GetHighestTierNumber(ByVal s As Season) As Integer
        Dim tierNum As Integer = 0
        Dim i As Integer
        Dim u As Usage

        If (Me.InnerList.Count > 0) Then
            For i = 0 To Me.InnerList.Count - 1
                u = CType(Me.InnerList.Item(i), Usage)
                If ((u.BaseOrTier = BaseOrTier.Tier1 Or _
                    u.BaseOrTier = BaseOrTier.Tier2 Or _
                    u.BaseOrTier = BaseOrTier.Tier3 Or _
                    u.BaseOrTier = BaseOrTier.Tier4 Or _
                    u.BaseOrTier = BaseOrTier.Tier5 Or _
                    u.BaseOrTier = BaseOrTier.Tier6) And _
                    u.Season = s) Then

                    tierNum = Math.Max(tierNum, u.BaseOrTier)

                End If
            Next
        End If

        Return (tierNum)
    End Function

    Public Function GetTieredUsageForTier(ByVal tier As BaseOrTier, ByVal s As Season) As UsageCollection
        Dim usages As New UsageCollection
        Dim i As Integer
        Dim u As Usage

        If (Me.InnerList.Count > 0) Then

            For i = 0 To Me.InnerList.Count - 1
                u = CType(Me.InnerList.Item(i), Usage)
                If (u.BaseOrTier = tier And u.Season = s) Then
                    usages.Add(New Usage(u.LoadType, u.BaseOrTier, u.TimeOfUse, u.Season, u.Quantity))
                End If
            Next

        End If

        Return (usages)
    End Function

    Public Function GetTieredUsageForTierAssignToSeason(ByVal tier As BaseOrTier, ByVal s As Season) As UsageCollection
        Dim usages As New UsageCollection
        Dim i As Integer
        Dim u As Usage
        Dim q As Double

        If (Me.InnerList.Count > 0) Then

            For i = 0 To Me.InnerList.Count - 1
                u = CType(Me.InnerList.Item(i), Usage)

                If (u.BaseOrTier = tier) Then

                    q = 0.0
                    For seas As Season = Season.Summer To Season.SeasonB
                        If (u.BaseOrTier = tier And u.Season = seas) Then
                            q = q + u.Quantity
                        End If
                    Next

                    usages.Combine(u.BaseOrTier, u.TimeOfUse, s, q)

                End If

            Next

        End If

        Return (usages)
    End Function

    Public Function GetTieredTotalUsageForTier(ByVal t As BaseOrTier) As Double
        Dim total As Double
        Dim i As Integer
        Dim u As Usage

        If (Me.InnerList.Count > 0) Then
            For i = 0 To Me.InnerList.Count - 1
                u = CType(Me.InnerList.Item(i), Usage)
                If (u.BaseOrTier = t) Then
                    total = total + u.Quantity
                End If
            Next
        End If

        Return (total)
    End Function

    Public Function GetTieredTotalUsage() As Double
        Dim total As Double
        Dim i As Integer
        Dim u As Usage

        If (Me.InnerList.Count > 0) Then
            For i = 0 To Me.InnerList.Count - 1
                u = CType(Me.InnerList.Item(i), Usage)
                If (u.BaseOrTier = BaseOrTier.Tier1 Or u.BaseOrTier = BaseOrTier.Tier2 Or u.BaseOrTier = BaseOrTier.Tier3 Or u.BaseOrTier = BaseOrTier.Tier4 Or u.BaseOrTier = BaseOrTier.Tier5 Or u.BaseOrTier = BaseOrTier.Tier6) Then
                    total = total + u.Quantity
                End If
            Next
        End If

        Return (total)
    End Function

    Public Function GetTotalExcludeTOU(ByVal tou As TimeOfUse) As Double
        Dim total As Double
        Dim i As Integer
        Dim u As Usage

        If (Me.InnerList.Count > 0) Then
            For i = 0 To Me.InnerList.Count - 1
                u = CType(Me.InnerList.Item(i), Usage)
                If (u.TimeOfUse <> tou) Then
                    total = total + u.Quantity
                End If
            Next
        End If

        Return (total)
    End Function

    Public Function GetTotalForTOU(ByVal tou As TimeOfUse) As Double
        Dim total As Double
        Dim i As Integer
        Dim u As Usage

        If (Me.InnerList.Count > 0) Then
            For i = 0 To Me.InnerList.Count - 1
                u = CType(Me.InnerList.Item(i), Usage)
                If (u.TimeOfUse = tou) Then
                    total = total + u.Quantity
                End If
            Next
        End If

        Return (total)
    End Function

    ''' <summary>
    ''' Set all usage to zero.
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub ZeroAllUsage()
        Dim i As Integer
        Dim u As Usage

        If (Me.InnerList.Count > 0) Then
            For i = 0 To Me.InnerList.Count - 1
                u = CType(Me.InnerList.Item(i), Usage)
                u.Quantity = 0.0
            Next
        End If

    End Sub


    ''' <summary>
    ''' Reassign tier
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub ReassignTier(ByVal newTier As BaseOrTier)
        Dim i As Integer
        Dim u As Usage

        If (Me.InnerList.Count > 0) Then
            For i = 0 To Me.InnerList.Count - 1
                u = CType(Me.InnerList.Item(i), Usage)
                If ((u.BaseOrTier = BaseOrTier.Tier1 Or _
                    u.BaseOrTier = BaseOrTier.Tier2 Or _
                    u.BaseOrTier = BaseOrTier.Tier3 Or _
                    u.BaseOrTier = BaseOrTier.Tier4 Or _
                    u.BaseOrTier = BaseOrTier.Tier5 Or _
                    u.BaseOrTier = BaseOrTier.Tier6)) Then

                    u.ReassignTier(newTier)

                End If
            Next
        End If

    End Sub

    ''' <summary>
    ''' Reassign season
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub ReassignSeason(ByVal newSeason As Season)
        Dim i As Integer
        Dim u As Usage

        If (Me.InnerList.Count > 0) Then
            For i = 0 To Me.InnerList.Count - 1
                u = CType(Me.InnerList.Item(i), Usage)
                u.ReassignSeason(newSeason)
            Next
        End If

    End Sub


    ''' <summary>
    ''' Round the usages of TOU provided - non demand.
    ''' </summary>
    ''' <param name="tou"></param>
    ''' <param name="rdp"></param>
    ''' <remarks></remarks>
    Public Sub Round(ByVal tou As TimeOfUse, ByVal rdp As Integer)
        Dim i As Integer
        Dim objUsage As Usage

        If (Me.InnerList.Count > 0) Then
            For i = 0 To Me.InnerList.Count - 1
                objUsage = CType(Me.InnerList.Item(i), Usage)
                If (objUsage.TimeOfUse = tou AndAlso objUsage.BaseOrTier <> BaseOrTier.Demand AndAlso objUsage.BaseOrTier <> BaseOrTier.DemandBill AndAlso objUsage.BaseOrTier <> BaseOrTier.DemandBill2 AndAlso objUsage.BaseOrTier <> BaseOrTier.DemandBill3) Then
                    objUsage.Quantity = Math.Round(objUsage.Quantity, rdp)
                End If
            Next
        End If

    End Sub


    ' Alternative to Clone, create a deep copy manually
    Public Function Copy() As UsageCollection
        Dim objUsages As New UsageCollection
        Dim i As Integer
        Dim objUsage As Usage

        If (Me.InnerList.Count > 0) Then

            For i = 0 To Me.InnerList.Count - 1
                objUsage = CType(Me.InnerList.Item(i), Usage)

                Dim u As Usage = New Usage(objUsage.LoadType, objUsage.BaseOrTier, objUsage.TimeOfUse, objUsage.Season, objUsage.Quantity)
                u.VirtualQuantity = objUsage.VirtualQuantity

                objUsages.Add(u)
            Next

        End If

        Return (objUsages)
    End Function

    ' Clone by serializing
    Public Function Clone() As Object Implements ICloneable.Clone
        ' This directly implements deep clone by serializing to a memory stream

        Dim bf As System.Runtime.Serialization.Formatters.Binary.BinaryFormatter = _
         New System.Runtime.Serialization.Formatters.Binary.BinaryFormatter
        Dim ms As System.IO.MemoryStream = New System.IO.MemoryStream
        Dim obj As UsageCollection

        bf.Serialize(ms, Me)
        ms.Seek(0, System.IO.SeekOrigin.Begin)
        obj = CType(bf.Deserialize(ms), UsageCollection)
        ms.Close()

        Return (obj)
    End Function

    ''' <summary>
    ''' CR 20298, 20299, 20302 Oct 2011 - Tax Usage Factor scale, scale all usage including demand
    ''' </summary>
    ''' <param name="scaleFactor">1 + tax usage factor</param>
    ''' <remarks></remarks>
    Public Sub ScaleTaxUsageFactor(scaleFactor As Double)
        Dim objUsages As New UsageCollection
        Dim i As Integer
        Dim objUsage As Usage
        If (Me.InnerList.Count > 0) Then
            For i = 0 To Me.InnerList.Count - 1
                objUsage = CType(Me.InnerList.Item(i), Usage)
                objUsage.Quantity = objUsage.Quantity * scaleFactor
            Next
        End If
    End Sub

    ''' <summary>
    ''' Swap the base quantity with the primary quantity if greater than zero.
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub SwapVirtualQuantityOfDemandUsages(Optional onlyifAlreadySwapped As Boolean = False)
        Dim objUsages As New UsageCollection
        Dim i As Integer
        Dim objUsage As Usage

        If (Me.InnerList.Count > 0) Then
            For i = 0 To Me.InnerList.Count - 1
                objUsage = CType(Me.InnerList.Item(i), Usage)

                If (objUsage.BaseOrTier = BaseOrTier.Demand Or objUsage.BaseOrTier = BaseOrTier.DemandBill Or objUsage.BaseOrTier = BaseOrTier.DemandBill2 Or objUsage.BaseOrTier = BaseOrTier.DemandBill3) Then

                    If (objUsage.VirtualQuantity > 0.0 Or (onlyifAlreadySwapped AndAlso objUsage.VirtualQuantityWasSwapped)) Then

                        Dim tempQ As Double = objUsage.Quantity
                        objUsage.Quantity = objUsage.VirtualQuantity
                        objUsage.VirtualQuantity = tempQ
                        objUsage.VirtualQuantityWasSwapped = True
                        If (onlyifAlreadySwapped) Then
                            objUsage.VirtualQuantityWasSwapped = False
                        End If


                    End If

                End If

            Next
        End If

    End Sub
    ''' <summary>
    ''' 'Increase DemandBill values upward to provided value where appropriate.
    ''' 'CR56757 - Applied ADBD Factor and Rule to ADBD Computation
    ''' </summary>
    ''' <param name="value"></param>
    ''' <param name="adbdfactor"></param>
    ''' <param name="adbdRule"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function AdjustDemandBillQuantity(value As Double, Optional adbdfactor As Double = 1.0, Optional adbdRule As Integer = 0) As Boolean
        Dim maxDemand As Double = 0
        Dim i As Integer
        Dim u As Usage
        Dim adjustmentMade As Boolean = False

        If (Me.InnerList.Count > 0) Then

            For i = 0 To Me.InnerList.Count - 1
                u = CType(Me.InnerList.Item(i), Usage)
                If (u.BaseOrTier = BaseOrTier.DemandBill) Then
                    'CR56757 - Applying ADBD Rule
                    If Not ((adbdRule = 1 AndAlso u.TimeOfUse = TimeOfUse.OffPeak) Or (adbdRule = 2 AndAlso u.TimeOfUse = TimeOfUse.Shoulder2)) Then
                        'Check if adbd value multiplied by factor is greater than usage quantity. Consider the greater value for quanity 
                        If (u.Quantity < (value * adbdfactor)) Then
                            u.Quantity = value * adbdfactor
                            adjustmentMade = True
                        End If
                    End If

                End If
            Next

        End If

        Return (adjustmentMade)
    End Function


    ''' <summary>
    ''' Get maximum demand quantity.
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function GetSeasonalMaximumDemandQuantity(tou As TimeOfUse) As Double
        Dim maxDemand As Double = 0
        Dim i As Integer
        Dim u As Usage

        If (Me.InnerList.Count > 0) Then

            For i = 0 To Me.InnerList.Count - 1
                u = CType(Me.InnerList.Item(i), Usage)
                If (u.Season <> Season.Undefined AndAlso u.TimeOfUse = tou AndAlso (u.BaseOrTier = BaseOrTier.Demand Or u.BaseOrTier = BaseOrTier.DemandBill Or u.BaseOrTier = BaseOrTier.DemandBill2 Or u.BaseOrTier = BaseOrTier.DemandBill3)) Then
                    If (u.Quantity > maxDemand) Then
                        maxDemand = u.Quantity
                    End If
                End If
            Next

        End If

        Return (maxDemand)
    End Function

    ''' <summary>
    ''' Set all demand usage to supplied demand quantity.
    ''' </summary>
    Public Function SetSeasonalDemandToOverallMaximumDemand() As Boolean
        Dim i As Integer
        Dim tou As TimeOfUse
        Dim touStart As TimeOfUse = TimeOfUse.Undefined
        Dim touEnd As TimeOfUse = TimeOfUse.CriticalPeak
        Dim u As Usage

        If (Me.InnerList.Count > 0) Then

            ' 15.06 bug 58179 - fix typo, it should loop through all the tous instead of just undefined
            For tou = touStart To touEnd
                Dim maxDemand As Double = GetSeasonalMaximumDemandQuantity(tou)
                For i = 0 To Me.InnerList.Count - 1
                    u = CType(Me.InnerList.Item(i), Usage)
                    If (u.Season <> Season.Undefined AndAlso u.TimeOfUse = tou AndAlso (u.BaseOrTier = BaseOrTier.Demand Or u.BaseOrTier = BaseOrTier.DemandBill Or u.BaseOrTier = BaseOrTier.DemandBill2 Or u.BaseOrTier = BaseOrTier.DemandBill3)) Then
                        u.Quantity = maxDemand
                    End If
                Next
            Next

        End If

        Return (True)
    End Function

    ''' <summary>
    ''' How many seasons are present in the usage.
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function GetSeasonCount() As Integer
        Dim count As Integer = 0
        Dim s As Season = Season.Undefined
        Dim u As Usage

        If (Me.InnerList.Count > 0) Then
            For i As Integer = 0 To Me.InnerList.Count - 1
                u = CType(Me.InnerList.Item(i), Usage)
                If (u.Season <> s) Then
                    count = count + 1
                    s = u.Season
                End If
            Next
        End If

        Return (count)
    End Function

    ''' <summary>
    ''' Get the demand usage determinant with the highest quantity.
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function GetMaximumDemandUsageDeterminant() As Usage
        Dim q As Double = 0.0
        Dim maxDemandDeterminant As Usage = Nothing
        Dim i As Integer
        Dim u As Usage

        If (Me.InnerList.Count > 0) Then

            For i = 0 To Me.InnerList.Count - 1
                u = CType(Me.InnerList.Item(i), Usage)
                If (u.BaseOrTier = BaseOrTier.Demand Or u.BaseOrTier = BaseOrTier.DemandBill) Then

                    If (u.Quantity > q) Then

                        If (maxDemandDeterminant Is Nothing) Then
                            maxDemandDeterminant = New Usage(u.LoadType, u.BaseOrTier, u.TimeOfUse, u.Season, u.Quantity, u.Datestamp)
                        Else
                            maxDemandDeterminant.ReassignTier(u.BaseOrTier)
                            maxDemandDeterminant.ReassignTOU(u.TimeOfUse)
                            maxDemandDeterminant.ReassignSeason(u.Season)
                            maxDemandDeterminant.Datestamp = u.Datestamp
                            maxDemandDeterminant.Quantity = u.Quantity
                        End If

                        q = maxDemandDeterminant.Quantity
                    End If
                End If
            Next

        End If

        Return (maxDemandDeterminant)
    End Function


    ''' <summary>
    ''' This simply totals the usage quantities.  The collection being totaled was from a capacity reserve operation.
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function GetTotalForCapacityReserve() As Double
        Dim t As Double
        Dim i As Integer
        Dim u As Usage

        If (Me.InnerList.Count > 0) Then
            For i = 0 To Me.InnerList.Count - 1
                u = CType(Me.InnerList.Item(i), Usage)
                t = t + u.Quantity
            Next
        End If

        Return (t)
    End Function

End Class

