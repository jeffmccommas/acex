Imports CE.RateEngine.Enums

Public Class TOUBoundary
	Inherits BoundaryBase

	Private m_eTimeOfUse As TimeOfUse
	Private m_eSeason As Season
	Private m_eDayType As DayType
	Private m_sStartTime As String
	Private m_sEndTime As String

	Private Sub New()
	End Sub


	Public Sub New(ByVal eTimeOfUse As TimeOfUse, ByVal eSeason As Season, ByVal eDayType As DayType, ByVal sStartTime As String, ByVal sEndTime As String)
		MyBase.New()
		m_eTimeOfUse = eTimeOfUse
		m_eSeason = eSeason
		m_eDayType = eDayType
		m_sStartTime = sStartTime
		m_sEndTime = sEndTime
	End Sub

#Region "Properties"

	Public ReadOnly Property TimeOfUse() As TimeOfUse
		Get
			Return (m_eTimeOfUse)
		End Get
	End Property

	Public ReadOnly Property Season() As Season
		Get
			Return (m_eSeason)
		End Get
	End Property

	Public ReadOnly Property DayType() As DayType
		Get
			Return (m_eDayType)
		End Get
	End Property

	Public ReadOnly Property StartTime() As String
		Get
			Return (m_sStartTime)
		End Get
	End Property

	Public ReadOnly Property EndTime() As String
		Get
			Return (m_sEndTime)
		End Get
	End Property

#End Region

End Class
