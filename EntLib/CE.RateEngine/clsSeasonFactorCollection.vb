Imports CE.RateEngine.Enums

Public Class SeasonFactorCollection
	Inherits DictionaryBase

	Public Sub New()
	End Sub

	Default Property Item(ByVal eKey As Season) As SeasonFactor
		Get
			Return CType(Me.InnerHashtable.Item(eKey), SeasonFactor)
		End Get
		Set(ByVal Value As SeasonFactor)
			Me.InnerHashtable.Item(eKey) = Value
		End Set
	End Property

	Public Sub SumAdd(ByVal eKey As Season, ByVal objSeasonFactor As SeasonFactor)
		If (Me.InnerHashtable.Contains(eKey)) Then
			Dim sf As New SeasonFactor
			sf = CType(Me.InnerHashtable.Item(eKey), SeasonFactor)
			sf.Factor = sf.Factor + objSeasonFactor.Factor
		Else
			Me.InnerHashtable.Add(eKey, objSeasonFactor)
		End If
	End Sub

	Public Sub Add(ByVal eKey As Season, ByVal obj As SeasonFactor)
		Me.InnerHashtable.Add(eKey, obj)
	End Sub

	Public Sub Remove(ByVal eKey As Season)
		Me.InnerHashtable.Remove(eKey)
	End Sub

	Public Function Contains(ByVal eKey As Season) As Boolean
		Return (Me.InnerHashtable.Contains(eKey))
	End Function

End Class
