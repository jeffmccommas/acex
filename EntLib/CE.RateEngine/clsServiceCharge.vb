Imports CE.RateEngine.Enums

Public Class ServiceCharge
	Inherits ChargeBase

	Private m_eCalculationType As ChargeCalcType
	Private m_eStep As ServiceStep
    Private m_eSeason As Season
    Private m_eRebateClass As RebateClass

	Private Sub New()
	End Sub

	Public Sub New(ByVal eCostType As CostType, ByVal eCalculationType As ChargeCalcType, ByVal dblChargeValue As Double)
		MyBase.New(eCostType, dblChargeValue)
		m_eCalculationType = eCalculationType
		m_eStep = ServiceStep.Undefined
        m_eSeason = Season.Undefined
        m_eRebateClass = RebateClass.Undefined
	End Sub

	Public Sub New(ByVal eCostType As CostType, ByVal eCalculationType As ChargeCalcType, ByVal eStep As ServiceStep, ByVal dblChargeValue As Double)
		MyBase.New(eCostType, dblChargeValue)
		m_eCalculationType = eCalculationType
		m_eStep = eStep
        m_eSeason = Season.Undefined
        m_eRebateClass = RebateClass.Undefined
	End Sub

	Public Sub New(ByVal eCostType As CostType, ByVal eCalculationType As ChargeCalcType, ByVal eStep As ServiceStep, ByVal eSeason As Season, ByVal dblChargeValue As Double)
		MyBase.New(eCostType, dblChargeValue)
		m_eCalculationType = eCalculationType
		m_eStep = eStep
        m_eSeason = eSeason
        m_eRebateClass = RebateClass.Undefined
    End Sub

    ' CR 20298, 20299, 20302 Oct 2011 - new contruct with rebate class
    Public Sub New(ByVal eCostType As CostType, ByVal eCalculationType As ChargeCalcType, ByVal eStep As ServiceStep, ByVal eSeason As Season, ByVal dblChargeValue As Double, eRebateClass As RebateClass)
        MyBase.New(eCostType, dblChargeValue)
        m_eCalculationType = eCalculationType
        m_eStep = eStep
        m_eSeason = eSeason
        m_eRebateClass = eRebateClass
    End Sub

#Region "Properties"

    Public ReadOnly Property CalculationType() As ChargeCalcType
        Get
            Return (m_eCalculationType)
        End Get
    End Property

    Public ReadOnly Property [Step]() As ServiceStep
        Get
            Return (m_eStep)
        End Get
    End Property

    Public ReadOnly Property Season() As Season
        Get
            Return (m_eSeason)
        End Get
    End Property

    ' CR 20298, 20299, 20302 Oct 2011 - demand rebate class
    Public ReadOnly Property RebateClass() As RebateClass
        Get
            Return (m_eRebateClass)
        End Get
    End Property

#End Region

End Class
