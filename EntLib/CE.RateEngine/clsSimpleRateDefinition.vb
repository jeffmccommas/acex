Imports CE.RateEngine.Enums

Public Class SimpleRateDefinition
	Inherits RateDefinitionBase

	Public Sub New()
		MyBase.New()
	End Sub

	' IsTiered, IsTimeOfUse, IsSeasonal, IsDemand, IsStepped, etc., moved to Base Class

End Class
