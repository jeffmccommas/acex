﻿Imports System.Configuration

''' <summary>
''' Interface for the Rate Engine configuration settings.
''' </summary>
''' <remarks></remarks>
Public Interface IRateEngineConfigSettings
    Property RateEngineConnectionString() As String
    Property LoggingEnabled() As Boolean
    Property DataSourceType() As Enums.DataSourceType
    Property CacheType() As Enums.CachingType
    Property XmlFilesPath() As String
    Property CacheTimeoutInMinutes() As Integer
End Interface


''' <summary>
''' Configuration settings class for the Rate Engine.
''' </summary>
''' <remarks>
''' The app.config or web.config may contain these which will be read by default.    
''' key="RateEngineLogging" value="false"
''' key="RateEngineDataSourceType" value="Database"
''' key="RateEngineCacheType" value="LocalCache"
''' key="RateEngineConn" value="some-string-here-to-db"
''' key="RateEngineXmlFilesPath" value="file-path-if-applicable"
''' key="RateEngineCacheTimeoutInMinutes" value="60"
''' </remarks>
Public Class RateEngineConfigSettings
    Implements IRateEngineConfigSettings

    Private _rateEngineConnectionString As String
    Private _loggingEnabled As Boolean
    Private _cacheType As Enums.CachingType
    Private _dataSourceType As Enums.DataSourceType
    Private _xmlFilesPath As String
    Private _cacheTimeoutInMinutes As Integer
    Private Const kCacheTimeoutInMinutesDefault As Integer = 60
    Private Const kDataSourceTypeConfigName As String = "RateEngineDataSourceType"
    Private Const kCacheTypeConfigName As String = "RateEngineCacheType"
    Private Const kLoggingEnabledConfigName As String = "RateEngineLogging"
    Private Const kConnectionStringConfigName As String = "RateEngineConn"
    Private Const kXmlFilesPathConfigName As String = "RateEngineXmlFilesPath"
    Private Const kCacheTimeoutInMinutesConfigName As String = "RateEngineCacheTimeoutInMinutes"

    ''' <summary>
    ''' The default constructor attempts to read the settings from a configuration file first.  
    ''' If these settings are not present, then defaults are used.  There is no default for the connection string.
    ''' </summary>
    ''' <remarks>
    ''' Configuration names that are read:
    ''' RateEngineConn
    ''' RateEngineLogging
    ''' RateEngineCacheType
    ''' RateEngineDataSourceType
    ''' RateEngineXmlFilesPath
    ''' RateEngineCacheTimeoutInMinutes
    ''' </remarks>
    Public Sub New()

        ' connection string setting if present
        Dim connStringsettings As ConnectionStringSettings = ConfigurationManager.ConnectionStrings(kConnectionStringConfigName)
        If (Not connStringsettings Is Nothing) Then
            Dim rateEngineConnectionString As String = connStringsettings.ConnectionString
            If (Not String.IsNullOrEmpty(rateEngineConnectionString)) Then
                Try
                    _rateEngineConnectionString = rateEngineConnectionString
                Catch ex As Exception
                    _rateEngineConnectionString = String.Empty
                End Try
            Else
                _rateEngineConnectionString = String.Empty
            End If
        Else
            _rateEngineConnectionString = String.Empty
        End If

        ' dataSourceType app setting if present
        Dim dataSourceTypeStr As String = ConfigurationManager.AppSettings(kDataSourceTypeConfigName)
        If (Not String.IsNullOrEmpty(dataSourceTypeStr)) Then
            Try
                _dataSourceType = CType(System.Enum.Parse(GetType(Enums.DataSourceType), dataSourceTypeStr), Enums.DataSourceType)
            Catch ex As Exception
                _dataSourceType = Enums.DataSourceType.Database
            End Try
        Else
            _dataSourceType = Enums.DataSourceType.Database
        End If

        ' cacheType app setting if present
        Dim cacheTypeStr As String = ConfigurationManager.AppSettings(kCacheTypeConfigName)
        If (Not String.IsNullOrEmpty(cacheTypeStr)) Then
            Try
                _cacheType = CType(System.Enum.Parse(GetType(Enums.CachingType), cacheTypeStr), Enums.CachingType)
            Catch ex As Exception
                _cacheType = Enums.CachingType.NoCache
            End Try
        Else
            _cacheType = Enums.CachingType.NoCache
        End If

        ' xmlFilesPath app setting if present
        Dim xmlFilesPathStr As String = ConfigurationManager.AppSettings(kXmlFilesPathConfigName)
        If (Not String.IsNullOrEmpty(xmlFilesPathStr)) Then
            Try
                _xmlFilesPath = xmlFilesPathStr
            Catch ex As Exception
                _xmlFilesPath = String.Empty
            End Try
        Else
            _xmlFilesPath = String.Empty
        End If

        ' cacheTimeoutInMinutes app setting if present
        Dim cacheTimeoutInMinutesStr As String = ConfigurationManager.AppSettings(kCacheTimeoutInMinutesConfigName)
        If (Not String.IsNullOrEmpty(cacheTimeoutInMinutesStr)) Then
            Try
                _cacheTimeoutInMinutes = Convert.ToInt32(cacheTimeoutInMinutesStr)
            Catch ex As Exception
                _cacheTimeoutInMinutes = kCacheTimeoutInMinutesDefault
            End Try
        Else
            _cacheTimeoutInMinutes = kCacheTimeoutInMinutesDefault
        End If

        ' logging enabled app setting if present
        Dim loggingStr As String = ConfigurationManager.AppSettings(kLoggingEnabledConfigName)
        If (Not String.IsNullOrEmpty(loggingStr)) Then
            Try
                _loggingEnabled = Convert.ToBoolean(loggingStr)
            Catch ex As Exception
                _loggingEnabled = False
            End Try
        Else
            _loggingEnabled = False
        End If

    End Sub


    Public Property RateEngineConnectionString As String Implements IRateEngineConfigSettings.RateEngineConnectionString
        Get
            Return _rateEngineConnectionString
        End Get
        Set(value As String)
            _rateEngineConnectionString = value
        End Set
    End Property

    Public Property DataSourceType As Enums.DataSourceType Implements IRateEngineConfigSettings.DataSourceType
        Get
            Return _dataSourceType
        End Get
        Set(value As Enums.DataSourceType)
            _dataSourceType = value
        End Set
    End Property

    Public Property CacheType As Enums.CachingType Implements IRateEngineConfigSettings.CacheType
        Get
            Return _cacheType
        End Get
        Set(value As Enums.CachingType)
            _cacheType = value
        End Set
    End Property

    Public Property LoggingEnabled As Boolean Implements IRateEngineConfigSettings.LoggingEnabled
        Get
            Return _loggingEnabled
        End Get
        Set(value As Boolean)
            _loggingEnabled = value
        End Set
    End Property

    Public Property XmlFilesPath As String Implements IRateEngineConfigSettings.XmlFilesPath
        Get
            Return _xmlFilesPath
        End Get
        Set(value As String)
            _xmlFilesPath = value
        End Set
    End Property

    Public Property CacheTimeoutInMinutes As Integer Implements IRateEngineConfigSettings.CacheTimeoutInMinutes
        Get
            Return _cacheTimeoutInMinutes
        End Get
        Set(value As Integer)
            _cacheTimeoutInMinutes = value
        End Set
    End Property

End Class
