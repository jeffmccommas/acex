
''' <summary>
''' Rate maintenance data. This class contains rate maintenance data properies. 
''' Add instances of this class to rate maintenance data collection.
''' </summary>
''' <remarks>None.</remarks>
<Serializable()> Public Class RateMaintData

#Region "Private Data Members"

    Private _sessionID As String
    Private _uploadDateTime As DateTime
    Private _rateMaintID As Integer
    Private _rateCompanyID As Integer
    Private _rateMasterID As Integer
    Private _sourceTypeID As Short
    Private _sourceID As Integer
    Private _clientRateID As String
    Private _rateDefID As Integer
    Private _effectiveDate As DateTime
    Private _chargeType As Enums.ChargeType
    Private _tier As Short
    Private _timeOfUse As Short
    Private _seasonID As Integer
    Private _stepID As Integer
    Private _partTypeID As Integer
    Private _costTypeID As Integer
    Private _chargeValue As Double
    Private _calcTypeID As Integer
    Private _errorText As String
    Private _importStatus As Enums.ImportStatus
    Private _importDateTime As DateTime
    Private _originalRateCompanyID As String
    Private _originalSourceType As String
    Private _originalSourceID As String
    Private _originalSourceName As String
    Private _originalRateClass As String
    Private _originalEffectiveDate As String
    Private _originalTypeOfCharge As String
    Private _originalTier As String
    Private _originalTimeOfUse As String
    Private _originalSeason As String
    Private _originalPartType As String
    Private _originalCostType As String
    Private _originalChargeValue As String
    Private _originalCalcType As String
    Private _originalStep As String

    'CR 20298, 20299, 20302 Oct 2011 - Rebate Class ID
    Private _rebateClassID As Integer
    Private _originalRebateClassID As String

#End Region

    Public Sub New()

        _uploadDateTime = Date.Now

    End Sub

#Region "Properties"

    Public Property SessionID() As String
        Get
            Return _sessionID
        End Get
        Set(ByVal value As String)
            _sessionID = value
        End Set
    End Property

    Public Property RateMaintID() As Integer
        Get
            Return _rateMaintID
        End Get
        Set(ByVal value As Integer)
            _rateMaintID = value
        End Set
    End Property

    Public Property UploadDateTime() As DateTime
        Get
            Return _uploadDateTime
        End Get
        Set(ByVal value As DateTime)
            _uploadDateTime = value
        End Set
    End Property

    Public Property RateCompanyID() As Integer
        Get
            Return _rateCompanyID
        End Get
        Set(ByVal value As Integer)
            _rateCompanyID = value
        End Set
    End Property

    Public Property RateMasterID() As Integer
        Get
            Return _rateMasterID
        End Get
        Set(ByVal value As Integer)
            _rateMasterID = value
        End Set
    End Property

    Public Property SourceTypeID() As Short
        Get
            Return _sourceTypeID
        End Get
        Set(ByVal value As Short)
            _sourceTypeID = value
        End Set
    End Property

    Public Property SourceID() As Integer
        Get
            Return _sourceID
        End Get
        Set(ByVal value As Integer)
            _sourceID = value
        End Set
    End Property

    Public Property ClientRateID() As String
        Get
            Return _clientRateID
        End Get
        Set(ByVal value As String)
            _clientRateID = value
        End Set
    End Property

    Public Property RateDefID() As Integer
        Get
            Return _rateDefID
        End Get
        Set(ByVal value As Integer)
            _rateDefID = value
        End Set
    End Property

    Public Property EffectiveDate() As DateTime
        Get
            Return _effectiveDate
        End Get
        Set(ByVal value As DateTime)
            _effectiveDate = value
        End Set
    End Property

    Public Property ChargeType() As Enums.ChargeType
        Get
            Return _chargeType
        End Get
        Set(ByVal value As Enums.ChargeType)
            _chargeType = value
        End Set
    End Property

    Public Property Tier() As Short
        Get
            Return _tier
        End Get
        Set(ByVal value As Short)
            _tier = value
        End Set
    End Property

    Public Property TimeOfUse() As Short
        Get
            Return _timeOfUse
        End Get
        Set(ByVal value As Short)
            _timeOfUse = value
        End Set
    End Property

    Public Property SeasonID() As Integer
        Get
            Return _seasonID
        End Get
        Set(ByVal value As Integer)
            _seasonID = value
        End Set
    End Property

    Public Property StepID() As Integer
        Get
            Return _stepID
        End Get
        Set(ByVal value As Integer)
            _stepID = value
        End Set
    End Property

    Public Property PartTypeID() As Integer
        Get
            Return _partTypeID
        End Get
        Set(ByVal value As Integer)
            _partTypeID = value
        End Set
    End Property

    Public Property CostTypeID() As Integer
        Get
            Return _costTypeID
        End Get
        Set(ByVal value As Integer)
            _costTypeID = value
        End Set
    End Property

    Public Property ChargeValue() As Double
        Get
            Return _chargeValue
        End Get
        Set(ByVal value As Double)
            _chargeValue = value
        End Set
    End Property

    Public Property CalcTypeID() As Integer
        Get
            Return _calcTypeID
        End Get
        Set(ByVal value As Integer)
            _calcTypeID = value
        End Set
    End Property

    Public Property ErrorText() As String
        Get
            Return _errorText
        End Get
        Set(ByVal value As String)
            _errorText = value
        End Set
    End Property

    Public Property ImportStatus() As Enums.ImportStatus
        Get
            Return _importStatus
        End Get
        Set(ByVal value As Enums.ImportStatus)
            _importStatus = value
        End Set
    End Property

    Public Property ImportDateTime() As DateTime
        Get
            Return _importDateTime
        End Get
        Set(ByVal value As DateTime)
            _importDateTime = value
        End Set
    End Property

    Public Property OriginalRateCompanyID() As String
        Get
            Return _originalRateCompanyID
        End Get
        Set(ByVal value As String)
            _originalRateCompanyID = value
        End Set
    End Property

    Public Property OriginalSourceType() As String
        Get
            Return _originalSourceType
        End Get
        Set(ByVal value As String)
            _originalSourceType = value
        End Set
    End Property

    Public Property OriginalSourceID() As String
        Get
            Return _originalSourceID
        End Get
        Set(ByVal value As String)
            _originalSourceID = value
        End Set
    End Property

    Public Property OriginalSourceName() As String
        Get
            Return _originalSourceName
        End Get
        Set(ByVal value As String)
            _originalSourceName = value
        End Set
    End Property

    Public Property OriginalRateClass() As String
        Get
            Return _originalRateClass
        End Get
        Set(ByVal value As String)
            _originalRateClass = value
        End Set
    End Property

    Public Property OriginalEffectiveDate() As String
        Get
            Return _originalEffectiveDate
        End Get
        Set(ByVal value As String)
            _originalEffectiveDate = value
        End Set
    End Property

    Public Property OriginalTypeOfCharge() As String
        Get
            Return _originalTypeOfCharge
        End Get
        Set(ByVal value As String)
            _originalTypeOfCharge = value
        End Set
    End Property

    Public Property OriginalTier() As String
        Get
            Return _originalTier
        End Get
        Set(ByVal value As String)
            _originalTier = value
        End Set
    End Property

    Public Property OriginalTimeOfUse() As String
        Get
            Return _originalTimeOfUse
        End Get
        Set(ByVal value As String)
            _originalTimeOfUse = value
        End Set
    End Property

    Public Property OriginalSeason() As String
        Get
            Return _originalSeason
        End Get
        Set(ByVal value As String)
            _originalSeason = value
        End Set
    End Property

    Public Property OriginalPartType() As String
        Get
            Return _originalPartType
        End Get
        Set(ByVal value As String)
            _originalPartType = value
        End Set
    End Property

    Public Property OriginalCostType() As String
        Get
            Return _originalCostType
        End Get
        Set(ByVal value As String)
            _originalCostType = value
        End Set
    End Property

    Public Property OriginalChargeValue() As String
        Get
            Return _originalChargeValue
        End Get
        Set(ByVal value As String)
            _originalChargeValue = value
        End Set
    End Property

    Public Property OriginalCalcType() As String
        Get
            Return _originalCalcType
        End Get
        Set(ByVal value As String)
            _originalCalcType = value
        End Set
    End Property

    Public Property OriginalStep() As String
        Get
            Return _originalStep
        End Get
        Set(ByVal value As String)
            _originalStep = value
        End Set
    End Property

    'CR 20298, 20299, 20302 Oct 2011 - Rebate class ID 
    Public Property RebateClassID() As Integer
        Get
            Return _rebateClassID
        End Get
        Set(value As Integer)
            _rebateClassID = value
        End Set
    End Property

    Public Property OriginalRebateClassID() As String
        Get
            Return _originalRebateClassID
        End Get
        Set(value As String)
            _originalRebateClassID = value
        End Set
    End Property

#End Region
End Class


