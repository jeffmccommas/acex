Public Class LogEntryCollection
	Inherits CollectionBase

	Public Sub New()
	End Sub

	Default ReadOnly Property Item(ByVal index As Integer) As LogEntry
		Get
			Return CType(Me.InnerList.Item(index), LogEntry)
		End Get
	End Property

	Public Function Add(ByVal value As LogEntry) As Integer
		Return Me.InnerList.Add(value)
	End Function

	Public Sub Insert(ByVal index As Integer, ByVal value As LogEntry)
		Me.InnerList.Insert(index, value)
	End Sub

	Public Sub AddRange(ByVal values() As LogEntry)
		Me.InnerList.AddRange(values)
	End Sub

	Public Sub Remove(ByVal value As LogEntry)
		Me.InnerList.Remove(value)
	End Sub
End Class