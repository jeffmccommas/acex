﻿Imports CE.RateEngine.Enums

Public Class CPPEvent

    Private m_startDate As DateTime
    Private m_endDate As DateTime
    Private m_capResEligible As Boolean
    Private m_crkw As Double?

    Public Sub New()
    End Sub

    Public Sub New(ByVal startDate As DateTime, ByVal endDate As DateTime)
        Me.New()
        m_startDate = startDate
        m_endDate = endDate
        m_capResEligible = False
        m_crkw = Nothing
    End Sub

#Region "Properties"

    Public Property StartDate() As DateTime
        Get
            Return m_startDate
        End Get
        Set(ByVal Value As DateTime)
            m_startDate = Value
        End Set
    End Property

    Public Property EndDate() As DateTime
        Get
            Return m_endDate
        End Get
        Set(ByVal Value As DateTime)
            m_endDate = Value
        End Set
    End Property

    Public Property CapacityReserveEligible() As Boolean
        Get
            Return m_capResEligible
        End Get
        Set(ByVal Value As Boolean)
            m_capResEligible = Value
        End Set
    End Property

    Public Property crkw() As Double?
        Get
            Return m_crkw
        End Get
        Set(ByVal Value As Double?)
            m_crkw = Value
        End Set
    End Property

#End Region

End Class


Public Class CPPEventCollection
    Inherits CollectionBase

    Public Sub New()
    End Sub

    Default ReadOnly Property Item(ByVal index As Integer) As CPPEvent
        Get
            Return CType(Me.InnerList.Item(index), CPPEvent)
        End Get
    End Property

    Public Function Add(ByVal value As CPPEvent) As Integer
        Return Me.InnerList.Add(value)
    End Function

    Public Sub Insert(ByVal index As Integer, ByVal value As CPPEvent)
        Me.InnerList.Insert(index, value)
    End Sub

    Public Sub AddRange(ByVal values() As CPPEvent)
        Me.InnerList.AddRange(values)
    End Sub

    Public Sub Remove(ByVal value As CPPEvent)
        Me.InnerList.Remove(value)
    End Sub
End Class

