﻿Imports CE.RateEngine.Enums

<Serializable()> Public Class ReadingAdjustFactors

    Private _generalConservation As Double
    Private _typicalResponse As Double
    Private _criticalResponse As Double
    Private _demandControl As Double
    Private _percentReductionAsShifted As Double
    Private _percentCriticalReductionAsShifted As Double

    Private Sub New()
        _generalConservation = 1.0
        _typicalResponse = 1.0
        _criticalResponse = 1.0
        _demandControl = 1.0
        _percentReductionAsShifted = 1.0
        _percentCriticalReductionAsShifted = 1.0
    End Sub

    Public Sub New(ByVal g As Double, ByVal t As Double, ByVal c As Double, ByVal d As Double, ByVal p As Double)
        _generalConservation = g
        _typicalResponse = t
        _criticalResponse = c
        _demandControl = d
        _percentReductionAsShifted = p
        _percentCriticalReductionAsShifted = 1.0
    End Sub

    Public Sub New(ByVal g As Double, ByVal t As Double, ByVal c As Double, ByVal d As Double, ByVal p As Double, ByVal pc As Double)
        _generalConservation = g
        _typicalResponse = t
        _criticalResponse = c
        _demandControl = d
        _percentReductionAsShifted = p
        _percentCriticalReductionAsShifted = pc
    End Sub


#Region "Properties"

    Public Property GeneralConservation() As Double
        Get
            Return _generalConservation
        End Get
        Set(ByVal Value As Double)
            _generalConservation = Value
        End Set
    End Property

    Public Property TypicalResponse() As Double
        Get
            Return _typicalResponse
        End Get
        Set(ByVal Value As Double)
            _typicalResponse = Value
        End Set
    End Property

    Public Property CriticalResponse() As Double
        Get
            Return _criticalResponse
        End Get
        Set(ByVal Value As Double)
            _criticalResponse = Value
        End Set
    End Property

    Public Property DemandControl() As Double
        Get
            Return _demandControl
        End Get
        Set(ByVal Value As Double)
            _demandControl = Value
        End Set
    End Property

    Public Property PercentReductionAsShifted() As Double
        Get
            Return _percentReductionAsShifted
        End Get
        Set(ByVal Value As Double)
            _percentReductionAsShifted = Value
        End Set
    End Property

    Public Property PercentCriticalReductionAsShifted() As Double
        Get
            Return _percentCriticalReductionAsShifted
        End Get
        Set(ByVal Value As Double)
            _percentCriticalReductionAsShifted = Value
        End Set
    End Property

#End Region


End Class
