﻿Imports CE.RateEngine.Enums

<Serializable()> Public Class CostToDateResult

    Private m_cost As Double
    Private m_projectedCost As Double
    Private m_startDate As DateTime
    Private m_endDate As DateTime
    Private m_projectedEndDate As DateTime
    Private m_numberOfDaysInCostToDate As Integer
    Private m_numberOfDaysInProjectedCost As Integer
    Private m_monthProrateFactorUsed As Double
    Private m_usageScaleFactorUsed As Double
    Private m_usageCollection As UsageCollection
    Private m_projectedUsageCollection As UsageCollection
    Private m_errCode As ErrCode
    Private m_minimumCostsApplied As Boolean
    Private m_averageDailyCost As Double
    Private m_lastReadingDate As DateTime?
    Private m_projectedCostErrCode As ErrCode
    Private m_ptrResult As PTRResult
    Private m_ptrProjectedResult As PTRResult
    Private m_costCollection As CostCollection
    Private m_projectedCostCollection As CostCollection
    Private m_netMeteringResult As NetMeteringMonthlyResult
    Private m_projectedNetMeteringResult As NetMeteringMonthlyResult

    Public Sub New()
        m_errCode = ErrCode.NoError
    End Sub

#Region "Properties"

    Public Property Cost() As Double
        Get
            Return m_cost
        End Get
        Set(ByVal Value As Double)
            m_cost = Value
        End Set
    End Property

    Public Property ProjectedCost() As Double
        Get
            Return m_projectedCost
        End Get
        Set(ByVal Value As Double)
            m_projectedCost = Value
        End Set
    End Property

    Public Property StartDate() As DateTime
        Get
            Return m_startDate
        End Get
        Set(ByVal Value As DateTime)
            m_startDate = Value
        End Set
    End Property


    Public Property EndDate() As DateTime
        Get
            Return m_endDate
        End Get
        Set(ByVal Value As DateTime)
            m_endDate = Value
        End Set
    End Property


    Public Property ProjectedEndDate() As DateTime
        Get
            Return m_projectedEndDate
        End Get
        Set(ByVal Value As DateTime)
            m_projectedEndDate = Value
        End Set
    End Property

    Public Property NumberOfDaysInCostToDate() As Integer
        Get
            Return m_numberOfDaysInCostToDate
        End Get
        Set(ByVal Value As Integer)
            m_numberOfDaysInCostToDate = Value
        End Set
    End Property

    Public Property NumberOfDaysInProjectedCost() As Integer
        Get
            Return m_numberOfDaysInProjectedCost
        End Get
        Set(ByVal Value As Integer)
            m_numberOfDaysInProjectedCost = Value
        End Set
    End Property

    Public Property MonthProrateFactor() As Double
        Get
            Return m_monthProrateFactorUsed
        End Get
        Set(ByVal Value As Double)
            m_monthProrateFactorUsed = Value
        End Set
    End Property

    Public Property UsageScaleFactor() As Double
        Get
            Return m_usageScaleFactorUsed
        End Get
        Set(ByVal Value As Double)
            m_usageScaleFactorUsed = Value
        End Set
    End Property

    Public Property Usages() As UsageCollection
        Get
            Return m_usageCollection
        End Get
        Set(ByVal Value As UsageCollection)
            m_usageCollection = Value
        End Set
    End Property

    Public Property ProjectedUsages() As UsageCollection
        Get
            Return m_projectedUsageCollection
        End Get
        Set(ByVal Value As UsageCollection)
            m_projectedUsageCollection = Value
        End Set
    End Property

    Public Property ErrorCode() As ErrCode
        Get
            Return m_errCode
        End Get
        Set(ByVal Value As ErrCode)
            m_errCode = Value
        End Set
    End Property

    Public Property ProjectedCostErrorCode() As ErrCode
        Get
            Return m_projectedCostErrCode
        End Get
        Set(ByVal Value As ErrCode)
            m_projectedCostErrCode = Value
        End Set
    End Property

    Public Property MinimumCostsApplied() As Boolean
        Get
            Return m_minimumCostsApplied
        End Get
        Set(ByVal Value As Boolean)
            m_minimumCostsApplied = Value
        End Set
    End Property

    Public Property AverageDailyCost() As Double
        Get
            Return m_averageDailyCost
        End Get
        Set(ByVal Value As Double)
            m_averageDailyCost = Value
        End Set
    End Property

    Public Property LastReadingDate() As DateTime?
        Get
            Return m_lastReadingDate
        End Get
        Set(ByVal Value As DateTime?)
            m_lastReadingDate = Value
        End Set
    End Property

    Public Property PeakTimeRebates() As PTRResult
        Get
            Return m_ptrResult
        End Get
        Set(ByVal Value As PTRResult)
            m_ptrResult = Value
        End Set
    End Property

    Public Property ProjectedPeakTimeRebates() As PTRResult
        Get
            Return m_ptrProjectedResult
        End Get
        Set(ByVal Value As PTRResult)
            m_ptrProjectedResult = Value
        End Set
    End Property

    Public Property Costs() As CostCollection
        Get
            Return m_costCollection
        End Get
        Set(ByVal Value As CostCollection)
            m_costCollection = Value
        End Set
    End Property

    Public Property ProjectedCosts() As CostCollection
        Get
            Return m_projectedCostCollection
        End Get
        Set(ByVal Value As CostCollection)
            m_projectedCostCollection = Value
        End Set
    End Property

    ' these are only use when net metering is applicable
    Public Property NetMeteringResult() As NetMeteringMonthlyResult
        Get
            Return m_netMeteringResult
        End Get
        Set(ByVal Value As NetMeteringMonthlyResult)
            m_netMeteringResult = Value
        End Set
    End Property

    Public Property ProjectedNetMeteringResult() As NetMeteringMonthlyResult
        Get
            Return m_projectedNetMeteringResult
        End Get
        Set(ByVal Value As NetMeteringMonthlyResult)
            m_projectedNetMeteringResult = Value
        End Set
    End Property

#End Region

End Class

