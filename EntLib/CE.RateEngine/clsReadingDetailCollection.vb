﻿Imports CE.RateEngine.Enums

<Serializable()> Public Class ReadingDetailCollection
    Inherits CollectionBase
    Implements ICloneable

    Public Sub New()
    End Sub

    Default ReadOnly Property Item(ByVal index As Integer) As ReadingDetail
        Get
            Return CType(Me.InnerList.Item(index), ReadingDetail)
        End Get
    End Property

    Public Function Add(ByVal value As ReadingDetail) As Integer
        Return Me.InnerList.Add(value)
    End Function

    Public Sub Insert(ByVal index As Integer, ByVal value As ReadingDetail)
        Me.InnerList.Insert(index, value)
    End Sub

    Public Sub AddRange(ByVal values() As ReadingDetail)
        Me.InnerList.AddRange(values)
    End Sub

    Public Sub Remove(ByVal value As ReadingDetail)
        Me.InnerList.Remove(value)
    End Sub

#Region "Properties"

#End Region




    ' This directly implements deep clone by serializing to a memory stream
    Public Function Clone() As Object Implements System.ICloneable.Clone
        Dim bf As System.Runtime.Serialization.Formatters.Binary.BinaryFormatter = _
         New System.Runtime.Serialization.Formatters.Binary.BinaryFormatter
        Dim ms As System.IO.MemoryStream = New System.IO.MemoryStream
        Dim obj As ReadingDetailCollection

        bf.Serialize(ms, Me)
        ms.Seek(0, System.IO.SeekOrigin.Begin)
        obj = CType(bf.Deserialize(ms), ReadingDetailCollection)
        ms.Close()

        Return (obj)
    End Function

    Public Function CalculateTotalPrice(ByVal totalquantiy As Double) As Double
        Dim totalPrice As Double = 0
        Dim i As Integer
        Dim rDetail As ReadingDetail

        If (Me.InnerList.Count > 0) Then
            For i = 0 To (Me.InnerList.Count - 1)
                rDetail = CType(Me.InnerList.Item(i), ReadingDetail)
                totalPrice = totalPrice + rDetail.TotalPrice * (rDetail.Quantity / totalquantiy)
            Next
        End If

        Return (totalPrice)
    End Function
End Class
