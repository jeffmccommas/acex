Public Class TOUBoundaryCollection
	Inherits CollectionBase

	Public Sub New()
	End Sub

	Default ReadOnly Property Item(ByVal index As Integer) As TOUBoundary
		Get
			Return CType(Me.InnerList.Item(index), TOUBoundary)
		End Get
	End Property

	Public Function Add(ByVal value As TOUBoundary) As Integer
		Return Me.InnerList.Add(value)
	End Function

	Public Sub Insert(ByVal index As Integer, ByVal value As TOUBoundary)
		Me.InnerList.Insert(index, value)
	End Sub

	Public Sub AddRange(ByVal values() As TOUBoundary)
		Me.InnerList.AddRange(values)
	End Sub

	Public Sub Remove(ByVal value As TOUBoundary)
		Me.InnerList.Remove(value)
	End Sub
    Public Function ContainsDayType(ByVal dayType As Enums.DayType) As Boolean
        Dim retval As Boolean = False
        For Each item As TOUBoundary In Me.InnerList
            If item.DayType = dayType Then
                retval = True
                Exit For
            End If
        Next
        Return retval
    End Function
    'Public Function FindInclusiveBoundary(ByVal dtDate As DateTime) As TOUBoundary
    '    Dim i As Integer
    '    Dim objTemp As TOUBoundary
    '    Dim objTOUBoundary As TOUBoundary

    '    If (Me.InnerList.Count > 0) Then

    '        For i = 0 To Me.InnerList.Count - 1
    '            objTemp = CType(Me.InnerList.Item(i), TOUBoundary)
    '            ' First comparison used to be simply "less than", now it is "less than or equal to"
    '            If ConvertDayToDate(objTemp.StartDay, dtDate.Year) <= dtDate And ConvertDayToDate(objTemp.EndDay, dtDate.Year) >= dtDate Then
    '                objSeasonBoundary = CType(Me.InnerList.Item(i), SeasonBoundary)
    '                Exit For
    '            End If
    '        Next

    '    End If

    '    Return (objSeasonBoundary)
    'End Function


    Public Sub Sort()
        Dim objSorter As IComparer = New StartTimeSorter
        Me.InnerList.Sort(objSorter)
    End Sub

    Private Class StartTimeSorter
        Implements IComparer

        Public Function Compare(ByVal x As Object, ByVal y As Object) As Integer Implements System.Collections.IComparer.Compare
            Dim tb As TOUBoundary = CType(x, TOUBoundary)
            Dim icA As IComparable = CType(tb.StartTime, IComparable)
            Dim tb2 As TOUBoundary = CType(y, TOUBoundary)
            Dim icB As IComparable = CType(tb2.StartTime, IComparable)

            ' swapping icB and icA makes it asc/desc switch
            Return (icA.CompareTo(icB))
        End Function

    End Class

End Class
