﻿Imports System.Collections.Generic
Imports System.Linq
Imports System.Runtime.Caching


Public Class CacheLayer

    Shared ReadOnly Cache As ObjectCache = MemoryCache.Default

    ''' <summary>
    ''' Retrieve cached item.
    ''' </summary>
    ''' <typeparam name="T">Type of cached item</typeparam>
    ''' <param name="key">Name of cached item</param>
    ''' <returns>Cached item as type</returns>
    Public Shared Function [Get](Of T As Class)(ByVal key As String) As T
        Try
            Return DirectCast(Cache(key), T)
        Catch
            Return Nothing
        End Try
    End Function

    ''' <summary>
    ''' Retrieve cached item as double.
    ''' </summary>
    ''' <param name="key"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function GetDouble(ByVal key As String) As Double?
        Try
            Return DirectCast(Cache(key), Double)
        Catch
            Return Nothing
        End Try
    End Function

    ''' <summary>
    ''' Retrieve cached item as integer.
    ''' </summary>
    ''' <param name="key"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function GetInteger(ByVal key As String) As Integer?
        Try
            Return DirectCast(Cache(key), Integer)
        Catch
            Return Nothing
        End Try
    End Function

    ''' <summary>
    ''' Retrieve cached item as boolean.
    ''' </summary>
    ''' <param name="key"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function GetBoolean(ByVal key As String) As Boolean?
        Try
            Return DirectCast(Cache(key), Boolean)
        Catch
            Return Nothing
        End Try
    End Function

    ''' <summary>
    ''' Retrieve cached item as DateTime.
    ''' </summary>
    ''' <param name="key"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function GetDateTime(ByVal key As String) As DateTime?
        Try
            Return DirectCast(Cache(key), DateTime)
        Catch
            Return Nothing
        End Try
    End Function

    ''' <summary>
    ''' Insert value into the cache using appropriate name/value pair.
    ''' </summary>
    ''' <typeparam name="T">Type of cached item</typeparam>
    ''' <param name="objectToCache">Item to be cached</param>
    ''' <param name="key">Name of item</param>
    Public Shared Sub Add(Of T As Class)(objectToCache As T, key As String)
        Dim policy As New CacheItemPolicy()
        policy.Priority = CacheItemPriority.Default
        policy.AbsoluteExpiration = DateTimeOffset.Now.AddMinutes(60.0)
        Cache.Add(key, objectToCache, policy)
    End Sub

    ''' <summary>
    ''' Insert value into the cache using appropriate name/value pair.  Provide an absolute timeout.  
    ''' Can be evicted before the timeout by the system.
    ''' </summary>
    ''' <typeparam name="T"></typeparam>
    ''' <param name="objectToCache"></param>
    ''' <param name="key"></param>
    ''' <param name="timeoutInMinutes"></param>
    ''' <remarks></remarks>
    Public Shared Sub Add(Of T As Class)(objectToCache As T, key As String, timeoutInMinutes As Double)
        Dim policy As New CacheItemPolicy()
        policy.Priority = CacheItemPriority.Default
        policy.AbsoluteExpiration = DateTimeOffset.Now.AddMinutes(timeoutInMinutes)
        Cache.Add(key, objectToCache, policy)
    End Sub

    ''' <summary>
    ''' Insert value into the cache using appropriate name/value pair.
    ''' </summary>
    ''' <param name="objectToCache">Item to be cached</param>
    ''' <param name="key">Name of item</param>
    Public Shared Sub Add(objectToCache As Object, key As String)
        Cache.Add(key, objectToCache, DateTime.Now.AddDays(30))
    End Sub

    ''' <summary>
    ''' Insert value into the cache using appropriate name/value pair.  With timeout.
    ''' </summary>
    ''' <param name="objectToCache"></param>
    ''' <param name="key"></param>
    ''' <param name="timeoutInMinutes"></param>
    ''' <remarks></remarks>
    Public Shared Sub Add(objectToCache As Object, key As String, timeoutInMinutes As Double)
        Dim policy As New CacheItemPolicy()
        policy.Priority = CacheItemPriority.Default
        policy.AbsoluteExpiration = DateTimeOffset.Now.AddMinutes(timeoutInMinutes)
        Cache.Add(key, objectToCache, policy)
    End Sub

    ''' <summary>
    ''' Remove item from cache.
    ''' </summary>
    ''' <param name="key">Name of cached item</param>
    Public Shared Sub Clear(key As String)
        Cache.Remove(key)
    End Sub

    ''' <summary>
    ''' Gets all cached items as a list by their key.
    ''' </summary>
    ''' <returns></returns>
    Public Shared Function GetAll() As List(Of String)
        Return (Cache.Select(Function(kvp) kvp.Key).ToList())
    End Function

End Class

