﻿Imports CE.RateEngine.Enums

Public Class CostToDateExtParam
    Private m_prorateMonthlyServiceCharges As Boolean
    Private m_minimumChargeType As MinimumChargeType
    Private m_prorateDemandUsageDeterminants As Boolean
    Private m_smoothTiers As Boolean
    Private m_checkDaysInFullMonth As Boolean
    Private m_daysInFullMonth As Integer
    Private m_projectedNumDays As Integer
    Private Const DefaultMinimumChargeType As MinimumChargeType = Enums.MinimumChargeType.NoProrateMinimumCharge
    Private Const DefaultDaysInFullMonth As Integer = 32
    Private Const DefaultProjectedNumDays As Integer = 32
    Private m_sDSTEndDate As String = String.Empty
    Private m_allowRebateCalculations As Boolean                 'only applies to PTR
    Private m_allowBaselineCalculations As Boolean               'only applies to PTR
    Private m_useProjectedEndDateWhenFinalizingTierBoundaries As Boolean 'advanced tier handling
    Private m_omitProjectedCostCalculation As Boolean            'advanced
    Private m_inlinePeakEvents As Boolean                        'advanced
    Private m_adjustReadings As Boolean                          'advanced
    Private m_generalConservationRAF As Double                   'advanced
    Private m_typicalResponseRAF As Double                       'advanced
    Private m_criticalResponseRAF As Double                      'advanced
    Private m_demandControlRAF As Double                         'advanced
    Private m_percentReductionAsShiftedRAF As Double             'advanced
    Private m_percentCriticalReductionAsShiftedRAF As Double     'advanced
    Private Const DefaultFactorsRAF As Double = 0.2              'advanced
    Private m_peakEventInto As PeakEventInfo                     'advanced.  This is a custom provided feed of PeakEventDays and durations.
    Private m_rdp As Integer                                     'rounding decimal places
    Private m_bIsDailyOnlyReadings As Boolean
    Private m_bIncludeDailyDemandCalc As Boolean

    Public Sub New()
        m_prorateMonthlyServiceCharges = False
        m_minimumChargeType = DefaultMinimumChargeType
        m_prorateDemandUsageDeterminants = False
        m_smoothTiers = False
        m_checkDaysInFullMonth = False
        m_daysInFullMonth = DefaultDaysInFullMonth
        m_projectedNumDays = DefaultProjectedNumDays
        m_allowRebateCalculations = False
        m_allowBaselineCalculations = False
        m_useProjectedEndDateWhenFinalizingTierBoundaries = False
        m_omitProjectedCostCalculation = False
        m_inlinePeakEvents = False
        m_adjustReadings = False
        m_generalConservationRAF = DefaultFactorsRAF
        m_typicalResponseRAF = DefaultFactorsRAF
        m_criticalResponseRAF = DefaultFactorsRAF
        m_demandControlRAF = DefaultFactorsRAF
        m_percentReductionAsShiftedRAF = 1.0
        m_percentCriticalReductionAsShiftedRAF = 1.0
        m_peakEventInto = Nothing
        m_rdp = 3
    End Sub

    Public Sub New(ByVal prorateMonthlyServiceCharges As Boolean, _
                    ByVal minimumChargeType As MinimumChargeType, _
                    ByVal prorateDemandUsageDeterminants As Boolean, _
                    ByVal smoothTiers As Boolean, _
                    ByVal checkDaysInFullMonth As Boolean, _
                    ByVal daysInFullMonth As Integer, _
                    ByVal projectedNumDays As Integer,
                    ByVal dstEndDate As String)

        m_prorateMonthlyServiceCharges = prorateMonthlyServiceCharges
        m_minimumChargeType = minimumChargeType
        m_prorateDemandUsageDeterminants = prorateDemandUsageDeterminants
        m_smoothTiers = smoothTiers
        m_checkDaysInFullMonth = checkDaysInFullMonth
        m_daysInFullMonth = daysInFullMonth
        m_projectedNumDays = projectedNumDays
        m_sDSTEndDate = dstEndDate
        m_allowRebateCalculations = False
        m_allowBaselineCalculations = False
        m_useProjectedEndDateWhenFinalizingTierBoundaries = False
        m_omitProjectedCostCalculation = False
        m_inlinePeakEvents = False
        m_adjustReadings = False
        m_generalConservationRAF = DefaultFactorsRAF
        m_typicalResponseRAF = DefaultFactorsRAF
        m_criticalResponseRAF = DefaultFactorsRAF
        m_demandControlRAF = DefaultFactorsRAF
        m_percentReductionAsShiftedRAF = 1.0
        m_percentCriticalReductionAsShiftedRAF = 1.0
        m_peakEventInto = Nothing
        m_rdp = 3
    End Sub

    Public Sub New(ByVal prorateMonthlyServiceCharges As Boolean, _
                ByVal minimumChargeType As MinimumChargeType, _
                ByVal prorateDemandUsageDeterminants As Boolean, _
                ByVal smoothTiers As Boolean, _
                ByVal checkDaysInFullMonth As Boolean, _
                ByVal daysInFullMonth As Integer, _
                ByVal projectedNumDays As Integer,
                ByVal dstEndDate As String,
                ByVal allowRebateCalculations As Boolean,
                ByVal allowBaselineCalculations As Boolean)

        m_prorateMonthlyServiceCharges = prorateMonthlyServiceCharges
        m_minimumChargeType = minimumChargeType
        m_prorateDemandUsageDeterminants = prorateDemandUsageDeterminants
        m_smoothTiers = smoothTiers
        m_checkDaysInFullMonth = checkDaysInFullMonth
        m_daysInFullMonth = daysInFullMonth
        m_projectedNumDays = projectedNumDays
        m_allowRebateCalculations = allowRebateCalculations
        m_allowBaselineCalculations = allowBaselineCalculations
        m_useProjectedEndDateWhenFinalizingTierBoundaries = False
        m_omitProjectedCostCalculation = False
        m_inlinePeakEvents = False
        m_adjustReadings = False
        m_generalConservationRAF = DefaultFactorsRAF
        m_typicalResponseRAF = DefaultFactorsRAF
        m_criticalResponseRAF = DefaultFactorsRAF
        m_demandControlRAF = DefaultFactorsRAF
        m_percentReductionAsShiftedRAF = 1.0
        m_percentCriticalReductionAsShiftedRAF = 1.0
        m_peakEventInto = Nothing
        m_rdp = 3
    End Sub

    Public Sub New(ByVal prorateMonthlyServiceCharges As Boolean, _
                    ByVal minimumChargeType As MinimumChargeType)

        m_prorateMonthlyServiceCharges = prorateMonthlyServiceCharges
        m_minimumChargeType = minimumChargeType
        m_prorateDemandUsageDeterminants = False
        m_smoothTiers = False
        m_checkDaysInFullMonth = False
        m_daysInFullMonth = DefaultDaysInFullMonth
        m_projectedNumDays = DefaultProjectedNumDays
        m_allowRebateCalculations = False
        m_allowBaselineCalculations = False
        m_useProjectedEndDateWhenFinalizingTierBoundaries = False
        m_omitProjectedCostCalculation = False
        m_inlinePeakEvents = False
        m_adjustReadings = False
        m_generalConservationRAF = DefaultFactorsRAF
        m_typicalResponseRAF = DefaultFactorsRAF
        m_criticalResponseRAF = DefaultFactorsRAF
        m_demandControlRAF = DefaultFactorsRAF
        m_percentReductionAsShiftedRAF = 1.0
        m_percentCriticalReductionAsShiftedRAF = 1.0
        m_peakEventInto = Nothing
        m_rdp = 3
    End Sub

    Public ReadOnly Property ProrateMonthlyServiceCharges() As Boolean
        Get
            Return (m_prorateMonthlyServiceCharges)
        End Get
    End Property

    Public ReadOnly Property ProrateDemandUsageDeterminants() As Boolean
        Get
            Return (m_prorateDemandUsageDeterminants)
        End Get
    End Property

    Public ReadOnly Property SmoothTiers() As Boolean
        Get
            Return (m_smoothTiers)
        End Get
    End Property

    Public ReadOnly Property CheckDaysInFullMonth() As Boolean
        Get
            Return (m_checkDaysInFullMonth)
        End Get
    End Property

    Public ReadOnly Property MinimumChargeType() As MinimumChargeType
        Get
            Return (m_minimumChargeType)
        End Get
    End Property

    Public ReadOnly Property DaysInFullMonth() As Integer
        Get
            Return (m_daysInFullMonth)
        End Get
    End Property

    Public ReadOnly Property ProjectedNumDays() As Integer
        Get
            Return (m_projectedNumDays)
        End Get
    End Property

    Public ReadOnly Property DSTEndDate() As String
        Get
            Return (m_sDSTEndDate)
        End Get
    End Property

    Public Property AllowRebateCalculations() As Boolean
        Get
            Return (m_allowRebateCalculations)
        End Get
        Set(value As Boolean)
            m_allowRebateCalculations = value
        End Set
    End Property

    Public Property AllowBaselineCalculations() As Boolean
        Get
            Return (m_allowBaselineCalculations)
        End Get
        Set(value As Boolean)
            m_allowBaselineCalculations = value
        End Set
    End Property

    Public Property UseProjectedEndDateWhenFinalizingTierBoundaries() As Boolean
        Get
            Return (m_useProjectedEndDateWhenFinalizingTierBoundaries)
        End Get
        Set(value As Boolean)
            m_useProjectedEndDateWhenFinalizingTierBoundaries = value
        End Set
    End Property

    Public Property OmitProjectedCostCalculation() As Boolean
        Get
            Return (m_omitProjectedCostCalculation)
        End Get
        Set(value As Boolean)
            m_omitProjectedCostCalculation = value
        End Set
    End Property

    Public Property InlinePeakEvents() As Boolean
        Get
            Return (m_inlinePeakEvents)
        End Get
        Set(value As Boolean)
            m_inlinePeakEvents = value
        End Set
    End Property

    Public Property AdjustReadings() As Boolean
        Get
            Return (m_adjustReadings)
        End Get
        Set(value As Boolean)
            m_adjustReadings = value
        End Set
    End Property

    Public Property GeneralConservationRAF() As Double
        Get
            Return (m_generalConservationRAF)
        End Get
        Set(value As Double)
            m_generalConservationRAF = value
        End Set
    End Property

    Public Property TypicalResponseRAF() As Double
        Get
            Return (m_typicalResponseRAF)
        End Get
        Set(value As Double)
            m_typicalResponseRAF = value
        End Set
    End Property

    Public Property CriticalResponseRAF() As Double
        Get
            Return (m_criticalResponseRAF)
        End Get
        Set(value As Double)
            m_criticalResponseRAF = value
        End Set
    End Property

    Public Property DemandControlRAF() As Double
        Get
            Return (m_demandControlRAF)
        End Get
        Set(value As Double)
            m_demandControlRAF = value
        End Set
    End Property

    Public Property PercentReductionAsShiftedRAF() As Double
        Get
            Return (m_percentReductionAsShiftedRAF)
        End Get
        Set(value As Double)
            m_percentReductionAsShiftedRAF = value
        End Set
    End Property

    Public Property PercentCriticalReductionAsShiftedRAF() As Double
        Get
            Return (m_percentCriticalReductionAsShiftedRAF)
        End Get
        Set(value As Double)
            m_percentCriticalReductionAsShiftedRAF = value
        End Set
    End Property


    Public Property PeakEventInformation() As PeakEventInfo
        Get
            Return (m_peakEventInto)
        End Get
        Set(value As PeakEventInfo)
            m_peakEventInto = value
        End Set
    End Property

    Public Property RoundingDecimalPlaces() As Integer
        Get
            Return (m_rdp)
        End Get
        Set(value As Integer)
            m_rdp = value
        End Set
    End Property

    ''' <summary>
    ''' indicate if there's only daily reading for calc
    ''' CR 51610 Oct 2014
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property IsDailyOnlyReadings() As Boolean
        Get
            Return (m_bIsDailyOnlyReadings)
        End Get
        Set(value As Boolean)
            m_bIsDailyOnlyReadings = value
        End Set
    End Property

    ''' <summary>
    ''' indicate if cost to date include daily demand usage calc
    ''' should be true, except calls from alerts
    ''' CR 51610 Oct 2014
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property IncludeDailyDemandCalc() As Boolean
        Get
            Return (m_bIncludeDailyDemandCalc)
        End Get
        Set(value As Boolean)
            m_bIncludeDailyDemandCalc = value
        End Set
    End Property
End Class
