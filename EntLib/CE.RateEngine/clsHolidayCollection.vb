﻿Public Class HolidayCollection
    Inherits CollectionBase
    Implements ICloneable

    Public Sub New()
    End Sub

    Default ReadOnly Property Item(ByVal index As Integer) As Holiday
        Get
            Return CType(Me.InnerList.Item(index), Holiday)
        End Get
    End Property

    Public Function Add(ByVal value As Holiday) As Integer
        Return Me.InnerList.Add(value)
    End Function

    Public Sub Insert(ByVal index As Integer, ByVal value As Holiday)
        Me.InnerList.Insert(index, value)
    End Sub

    Public Sub AddRange(ByVal values() As Holiday)
        Me.InnerList.AddRange(values)
    End Sub

    Public Sub Remove(ByVal value As Holiday)
        Me.InnerList.Remove(value)
    End Sub

    Public Sub Sort()
        Dim objSorter As IComparer = New DateSorter
        Me.InnerList.Sort(objSorter)
    End Sub

    Public Sub Sort(ByVal bReverse As Boolean)
        Dim objSorter As IComparer = New DateSorter(bReverse)
        Me.InnerList.Sort(objSorter)
    End Sub

    Public Function IsHoliday(ByVal compareDate As DateTime) As Boolean
        Dim result As Boolean
        Dim holiday As Holiday

        Try

            result = False

            'Iterate holiday list.
            For Each holiday In Me.InnerList

                'Match found.
                If compareDate.Day = holiday.DateOfHoliday.Day And
                   compareDate.Month = holiday.DateOfHoliday.Month And
                   compareDate.Year = holiday.DateOfHoliday.Year Then
                    result = True
                    Exit For
                End If
            Next

        Catch

        End Try


        Return result

    End Function

    Public Function Clone() As Object Implements System.ICloneable.Clone
        Dim bf As System.Runtime.Serialization.Formatters.Binary.BinaryFormatter = _
               New System.Runtime.Serialization.Formatters.Binary.BinaryFormatter
        Dim ms As System.IO.MemoryStream = New System.IO.MemoryStream
        Dim obj As HolidayCollection

        bf.Serialize(ms, Me)
        ms.Seek(0, System.IO.SeekOrigin.Begin)
        obj = CType(bf.Deserialize(ms), HolidayCollection)
        ms.Close()

        Return (obj)
    End Function


    Private Class DateSorter
        Implements IComparer

        Private m_bReverse As Boolean

        Public Sub New()
        End Sub

        Public Sub New(ByVal bReverse As Boolean)
            m_bReverse = bReverse
        End Sub

        'This is used by some solutions; do not remove
        Public Property Reverse() As Boolean
            Get
                Return m_bReverse
            End Get
            Set(ByVal Value As Boolean)
                m_bReverse = Value
            End Set
        End Property

        Public Function Compare(ByVal x As Object, ByVal y As Object) As Integer Implements System.Collections.IComparer.Compare
            Dim i As Integer
            Dim tb As Holiday = CType(x, Holiday)
            Dim icA As IComparable = CType(tb.DateOfHoliday, IComparable)
            Dim tb2 As Holiday = CType(y, Holiday)
            Dim icB As IComparable = CType(tb2.DateOfHoliday, IComparable)

            ' swapping icB and icA makes it asc/desc switch
            If (Not m_bReverse) Then
                i = icA.CompareTo(icB)
            Else
                i = icB.CompareTo(icA)
            End If

            Return (i)
        End Function

    End Class

End Class
