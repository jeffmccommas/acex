Imports CE.RateEngine.Enums

''' <summary>
''' This class holds all the Extra methods that expand the business logic to encapsulate multiple
''' Rate Engine calls or perform more advanced logic,
''' Or, methods that don't quite belong in main Rate Engine.
''' </summary>
''' <remarks></remarks>
Public Class RateEngineExtras

    Private m_sRateEngineConnectionString As String = String.Empty
    Private m_dataSourceType As Enums.DataSourceType = DataSourceType.Database
    Private m_cachingType As Enums.CachingType = CachingType.NoCache
    Private m_xmlFilesPath As String = String.Empty
    Private m_cacheTimeoutInMinutes As Integer

    Public Sub New()
        Dim recs As New RateEngineConfigSettings()

        m_sRateEngineConnectionString = recs.RateEngineConnectionString
        m_dataSourceType = recs.DataSourceType
        m_xmlFilesPath = recs.XmlFilesPath
        m_cachingType = recs.CacheType
        m_cacheTimeoutInMinutes = recs.CacheTimeoutInMinutes

    End Sub

	' The last argument is the HubStatic Connection string, passed here special, since
	' this is the only special helper function that queries HubStatic
	Public Function GetRateCompanyID(ByVal nID As Integer, ByVal eSourceType As SourceType, ByVal sHubStaticConn As String) As Integer
		Dim nRateCompanyID As Integer
		Dim objDC As dctlRateEngine

        objDC = New dctlRateEngine(m_sRateEngineConnectionString, m_xmlFilesPath, m_dataSourceType, m_cachingType, m_cacheTimeoutInMinutes)
		nRateCompanyID = objDC.GetRateCompanyID(nID, eSourceType, sHubStaticConn)

		Return (nRateCompanyID)
	End Function

End Class
