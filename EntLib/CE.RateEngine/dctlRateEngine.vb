Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Text
Imports CE.RateEngine.Enums
Imports System.Runtime.Serialization.Formatters


' DO NOT CALL ANY OF THE FUNCTIONS IN THIS CLASS DIRECTLY FROM YOUR APPLICATION
' THIS IS A DATA CONTROLLER FOR USE BY THE RATE ENGINE ONLY
' Instead, all public use should be via the RateEngine or RateEngineExtras
' Any Questions, contact mganley@aclara.com
Public Class dctlRateEngine
    Private Const ksGenericFailure As String = "Rate Engine Data controller generic error."
    Private Const ksDataReaderError As String = "Rate Engine Data Reader error."
    Private Const ksBatchDataReaderError As String = "Rate Engine Batch Collection Data Reader error."
    Private Const ksBatchDataReaderMismatch As String = "Rate Engine Batch Collection Data Reader mismatch count error."
    Private Const ksRateEngineConnStringEmpty As String = "Rate Engine Connection String is empty."
    Private Const ksBaselineLookupFailed As String = "Baseline lookup was performed but no value was found."
    Private Const ksConvertRawUsageFailed As String = "Lookup in UseTypeMap failed trying to convert input usage into valid parts."
    Private Const ksLogError As String = "Logging Error."
    Private Const ksGetLogHistoryError As String = "Log History retrieval failed."
    Private Const ksGetLoggedReadingsError As String = "Logged Readings retrieval failed."
    Private Const ksBinaryDataIndicated As String = "binary data"
    Private Const ksUnimplementedDataSourceXmlFile As String = "Unimplemented DataSource: XmlFile"
    Private Const ksUnknownDataSource As String = "Unknown DataSource"
    Private Const ksUnknownCacheType As String = "Unknown CacheType"
    Private Const ksUnimplementedCacheTypeDistributedCache As String = "Unimplemented CacheType: DistributedCache"
    Private Const ksUnimplementedCacheTypeFlatLocalCache As String = "Unimplemented CacheType: FlatLocalCache"
    Private Const ksUnimplementedCacheTypeFlatDistributedCache As String = "Unimplemented CacheType: FlatDistributedCache"
    Private m_sRateEngineConnectionString As String = String.Empty
    Private m_dataSourceType As Enums.DataSourceType
    Private m_cachingType As Enums.CachingType
    Private m_xmlFilesPath As String = String.Empty
    Private m_cacheTimeoutInMinutes As Integer

#Region "Enums And Structures"

    Private Enum CollectionType
        UseCharges = 0
        ServiceCharges = 1
        MinimumCharges = 2
        TaxCharges = 3
        TierBoundaries = 4
        TOUBoundaries = 5
        SeasonBoundaries = 6
        StepBoundaries = 7
        Differences = 8
    End Enum

#End Region

    Public Sub New(ByVal sRateEngineConnectionString As String)
        m_sRateEngineConnectionString = sRateEngineConnectionString
        m_dataSourceType = DataSourceType.Database
        m_cachingType = CachingType.NoCache
    End Sub

    Public Sub New(ByVal sRateEngineConnectionString As String, ByVal sXmlFilesPath As String, ByVal eDataSource As Enums.DataSourceType, ByVal eCachingType As Enums.CachingType, ByVal cacheTimeoutInMinutes As Integer)
        m_sRateEngineConnectionString = sRateEngineConnectionString
        If (Not String.IsNullOrEmpty(sXmlFilesPath)) Then m_xmlFilesPath = sXmlFilesPath
        m_dataSourceType = eDataSource
        m_cachingType = eCachingType
        m_cacheTimeoutInMinutes = cacheTimeoutInMinutes
    End Sub

#Region "Properties"

    Public ReadOnly Property RateEngineConnectionString() As String
        Get
            Return (m_sRateEngineConnectionString)
        End Get
    End Property

    Public ReadOnly Property XmlFilesPath() As String
        Get
            Return (m_xmlFilesPath)
        End Get
    End Property

    Public ReadOnly Property CachingType() As Enums.CachingType
        Get
            Return (m_cachingType)
        End Get
    End Property

    Public ReadOnly Property DataSource() As Enums.DataSourceType
        Get
            Return (m_dataSourceType)
        End Get
    End Property

    Public ReadOnly Property CacheTimeoutInMinutes() As Integer
        Get
            Return (m_cacheTimeoutInMinutes)
        End Get
    End Property

#End Region


#Region "DB Helper Functions"

    ' Replacement for function of same name in VB namespace (removed that namespace)
    <System.ComponentModel.Browsable(False)> _
    Public Function IsDBNull(ByVal dbvalue As Object) As Boolean
        Return dbvalue Is DBNull.Value
    End Function

#End Region

#Region "Data Controller GetRateClassList Overloads (LocalCache implemented)"

#Region "GetRateClassList Overload A - (LocalCache implemented)"

    Public Function GetRateClassList(ByVal nRateCompanyID As Integer, ByVal dtStartDate As DateTime, ByVal dtEndDate As DateTime, ByVal eLanguage As Language) As SimpleRateDefinitionCollection
        Return (GetRateClassList_Cache(nRateCompanyID, dtStartDate, dtEndDate, eLanguage))
    End Function

    Private Function GetRateClassList_Cache(ByVal nRateCompanyID As Integer, ByVal dtStartDate As DateTime, ByVal dtEndDate As DateTime, ByVal eLanguage As Language) As SimpleRateDefinitionCollection
        Dim rateDefs As SimpleRateDefinitionCollection
        Const cacheKeyLocalTemplate As String = "GRCLA-{0}-{1}-{2}-{3}"

        Select Case m_cachingType
            Case Enums.CachingType.NoCache
                rateDefs = GetRateClassList_DataSource(nRateCompanyID, dtStartDate, dtEndDate, eLanguage)

            Case Enums.CachingType.LocalCache
                Dim cacheKeySB As New StringBuilder()
                Dim cacheKey As String

                cacheKey = cacheKeySB.AppendFormat(cacheKeyLocalTemplate, nRateCompanyID, DateTimeToEpoch(dtStartDate), DateTimeToEpoch(dtEndDate), CInt(eLanguage)).ToString()

                rateDefs = CacheLayer.Get(Of SimpleRateDefinitionCollection)(cacheKey)

                If (rateDefs Is Nothing) Then
                    rateDefs = GetRateClassList_DataSource(nRateCompanyID, dtStartDate, dtEndDate, eLanguage)
                    If (Not rateDefs Is Nothing) Then
                        CacheLayer.Add(rateDefs, cacheKey, m_cacheTimeoutInMinutes)
                    Else
                        ' did not get object; return an empty collection
                        rateDefs = New SimpleRateDefinitionCollection()
                    End If
                End If

            Case Enums.CachingType.DistributedCache
                Throw New ArgumentException(ksUnimplementedCacheTypeDistributedCache)

            Case Enums.CachingType.FlatLocalCache
                Throw New ArgumentException(ksUnimplementedCacheTypeFlatLocalCache)

            Case Enums.CachingType.FlatDistributedCache
                Throw New ArgumentException(ksUnimplementedCacheTypeFlatDistributedCache)

            Case Else
                Throw New ArgumentException(ksUnknownCacheType)

        End Select


        Return (rateDefs)
    End Function

    Private Function GetRateClassList_DataSource(ByVal nRateCompanyID As Integer, ByVal dtStartDate As DateTime, ByVal dtEndDate As DateTime, ByVal eLanguage As Language) As SimpleRateDefinitionCollection
        Dim objSimpleRateDefColl As SimpleRateDefinitionCollection

        Select Case m_dataSourceType

            Case DataSourceType.Database
                Dim sbSQL As New StringBuilder
                Dim arParams(3) As SqlParameter
                sbSQL.Append("rateengine.SelectRateClassListA")
                arParams(0) = New SqlParameter("@RateCompanyID", nRateCompanyID)
                arParams(0).Direction = ParameterDirection.Input
                arParams(1) = New SqlParameter("@StartDate", dtStartDate)
                arParams(1).Direction = ParameterDirection.Input
                arParams(2) = New SqlParameter("@EndDate", dtEndDate)
                arParams(2).Direction = ParameterDirection.Input
                arParams(3) = New SqlParameter("@Language", CType(eLanguage, Integer))
                arParams(3).Direction = ParameterDirection.Input

                objSimpleRateDefColl = MainQueryRateClassList(arParams, sbSQL)

            Case DataSourceType.XmlFile
                Throw New ArgumentException(ksUnimplementedDataSourceXmlFile)

            Case Else
                Throw New ArgumentException(ksUnknownDataSource)

        End Select

        Return (objSimpleRateDefColl)
    End Function

#End Region

#Region "GetRateClassList Overload B - (LocalCache implemented)"

    Public Function GetRateClassList(ByVal nRateCompanyID As Integer, ByVal dtStartDate As DateTime, ByVal dtEndDate As DateTime, ByVal eLanguage As Language, ByVal eCustomerType As CustomerType, ByVal eFuelType As FuelType, ByVal bDefaultOnly As Boolean) As SimpleRateDefinitionCollection
        Return (GetRateClassList_Cache(nRateCompanyID, dtStartDate, dtEndDate, eLanguage, eCustomerType, eFuelType, bDefaultOnly))
    End Function

    Private Function GetRateClassList_Cache(ByVal nRateCompanyID As Integer, ByVal dtStartDate As DateTime, ByVal dtEndDate As DateTime, ByVal eLanguage As Language, ByVal eCustomerType As CustomerType, ByVal eFuelType As FuelType, ByVal bDefaultOnly As Boolean) As SimpleRateDefinitionCollection
        Dim rateDefs As SimpleRateDefinitionCollection
        Const cacheKeyLocalTemplate As String = "GRCLB-{0}-{1}-{2}-{3}-{4}-{5}-{6}"

        Select Case m_cachingType
            Case Enums.CachingType.NoCache
                rateDefs = GetRateClassList_DataSource(nRateCompanyID, dtStartDate, dtEndDate, eLanguage, eCustomerType, eFuelType, bDefaultOnly)

            Case Enums.CachingType.LocalCache
                Dim cacheKeySB As New StringBuilder()
                Dim cacheKey As String

                cacheKey = cacheKeySB.AppendFormat(cacheKeyLocalTemplate, nRateCompanyID, DateTimeToEpoch(dtStartDate), DateTimeToEpoch(dtEndDate), CInt(eLanguage), CInt(eCustomerType), CInt(eFuelType), CInt(bDefaultOnly)).ToString()

                rateDefs = CacheLayer.Get(Of SimpleRateDefinitionCollection)(cacheKey)

                If (rateDefs Is Nothing) Then
                    rateDefs = GetRateClassList_DataSource(nRateCompanyID, dtStartDate, dtEndDate, eLanguage, eCustomerType, eFuelType, bDefaultOnly)
                    If (Not rateDefs Is Nothing) Then
                        CacheLayer.Add(rateDefs, cacheKey, m_cacheTimeoutInMinutes)
                    Else
                        ' did not get object; return an empty collection
                        rateDefs = New SimpleRateDefinitionCollection()
                    End If
                End If

            Case Enums.CachingType.DistributedCache
                Throw New ArgumentException(ksUnimplementedCacheTypeDistributedCache)

            Case Enums.CachingType.FlatLocalCache
                Throw New ArgumentException(ksUnimplementedCacheTypeFlatLocalCache)

            Case Enums.CachingType.FlatDistributedCache
                Throw New ArgumentException(ksUnimplementedCacheTypeFlatDistributedCache)

            Case Else
                Throw New ArgumentException(ksUnknownCacheType)

        End Select


        Return (rateDefs)
    End Function

    Private Function GetRateClassList_DataSource(ByVal nRateCompanyID As Integer, ByVal dtStartDate As DateTime, ByVal dtEndDate As DateTime, ByVal eLanguage As Language, ByVal eCustomerType As CustomerType, ByVal eFuelType As FuelType, ByVal bDefaultOnly As Boolean) As SimpleRateDefinitionCollection
        Dim objSimpleRateDefColl As SimpleRateDefinitionCollection

        Select Case m_dataSourceType

            Case DataSourceType.Database
                Dim sbSQL As New StringBuilder
                Dim arParams(6) As SqlParameter
                sbSQL.Append("rateengine.SelectRateClassListB")
                arParams(0) = New SqlParameter("@RateCompanyID", nRateCompanyID)
                arParams(0).Direction = ParameterDirection.Input
                arParams(1) = New SqlParameter("@StartDate", dtStartDate)
                arParams(1).Direction = ParameterDirection.Input
                arParams(2) = New SqlParameter("@EndDate", dtEndDate)
                arParams(2).Direction = ParameterDirection.Input
                arParams(3) = New SqlParameter("@Language", CType(eLanguage, Integer))
                arParams(3).Direction = ParameterDirection.Input
                arParams(4) = New SqlParameter("@CustomerType", eCustomerType)
                arParams(4).Direction = ParameterDirection.Input
                arParams(5) = New SqlParameter("@FuelType", eFuelType)
                arParams(5).Direction = ParameterDirection.Input
                arParams(6) = New SqlParameter("@Default", bDefaultOnly)
                arParams(6).Direction = ParameterDirection.Input

                objSimpleRateDefColl = MainQueryRateClassList(arParams, sbSQL)

            Case DataSourceType.XmlFile
                Throw New ArgumentException(ksUnimplementedDataSourceXmlFile)

            Case Else
                Throw New ArgumentException(ksUnknownDataSource)

        End Select

        Return (objSimpleRateDefColl)
    End Function

#End Region

#Region "GetRateClassList Overload C - (LocalCache implemented)"

    Public Function GetRateClassList(ByVal sServiceTerritory As String, ByVal eLanguage As Language) As SimpleRateDefinitionCollection
        Return (GetRateClassList_Cache(sServiceTerritory, eLanguage))
    End Function

    Private Function GetRateClassList_Cache(ByVal sServiceTerritory As String, ByVal eLanguage As Language) As SimpleRateDefinitionCollection
        Dim rateDefs As SimpleRateDefinitionCollection
        Const cacheKeyLocalTemplate As String = "GRCLC-{0}-{1}"

        Select Case m_cachingType
            Case Enums.CachingType.NoCache
                rateDefs = GetRateClassList_DataSource(sServiceTerritory, eLanguage)

            Case Enums.CachingType.LocalCache
                Dim cacheKeySB As New StringBuilder()
                Dim cacheKey As String

                cacheKey = cacheKeySB.AppendFormat(cacheKeyLocalTemplate, sServiceTerritory, CInt(eLanguage)).ToString()

                rateDefs = CacheLayer.Get(Of SimpleRateDefinitionCollection)(cacheKey)

                If (rateDefs Is Nothing) Then
                    rateDefs = GetRateClassList_DataSource(sServiceTerritory, eLanguage)
                    If (Not rateDefs Is Nothing) Then
                        CacheLayer.Add(rateDefs, cacheKey, m_cacheTimeoutInMinutes)
                    Else
                        ' did not get object; return an empty collection
                        rateDefs = New SimpleRateDefinitionCollection()
                    End If
                End If

            Case Enums.CachingType.DistributedCache
                Throw New ArgumentException(ksUnimplementedCacheTypeDistributedCache)

            Case Enums.CachingType.FlatLocalCache
                Throw New ArgumentException(ksUnimplementedCacheTypeFlatLocalCache)

            Case Enums.CachingType.FlatDistributedCache
                Throw New ArgumentException(ksUnimplementedCacheTypeFlatDistributedCache)

            Case Else
                Throw New ArgumentException(ksUnknownCacheType)

        End Select


        Return (rateDefs)
    End Function

    Private Function GetRateClassList_DataSource(ByVal sServiceTerritory As String, ByVal eLanguage As Language) As SimpleRateDefinitionCollection
        Dim objSimpleRateDefColl As SimpleRateDefinitionCollection

        Select Case m_dataSourceType

            Case DataSourceType.Database
                Dim sbSQL As New StringBuilder
                Dim arParams(1) As SqlParameter
                sbSQL.Append("rateengine.SelectRateClassListC")
                arParams(0) = New SqlParameter("@ServiceTerritoryID", sServiceTerritory)
                arParams(0).Direction = ParameterDirection.Input
                arParams(1) = New SqlParameter("@Language", CType(eLanguage, Integer))
                arParams(1).Direction = ParameterDirection.Input

                objSimpleRateDefColl = MainQueryRateClassList(arParams, sbSQL)

            Case DataSourceType.XmlFile
                Throw New ArgumentException(ksUnimplementedDataSourceXmlFile)

            Case Else
                Throw New ArgumentException(ksUnknownDataSource)

        End Select

        Return (objSimpleRateDefColl)
    End Function

#End Region

#Region "GetRateClassList Overload D - (LocalCache implemented)"

    Public Function GetRateClassList(ByVal sServiceTerritory As String, ByVal eLanguage As Language, ByVal eCustomerType As CustomerType, ByVal eFuelType As FuelType, ByVal bDefaultOnly As Boolean) As SimpleRateDefinitionCollection
        Return (GetRateClassList_Cache(sServiceTerritory, eLanguage, eCustomerType, eFuelType, bDefaultOnly))
    End Function

    Private Function GetRateClassList_Cache(ByVal sServiceTerritory As String, ByVal eLanguage As Language, ByVal eCustomerType As CustomerType, ByVal eFuelType As FuelType, ByVal bDefaultOnly As Boolean) As SimpleRateDefinitionCollection
        Dim rateDefs As SimpleRateDefinitionCollection
        Const cacheKeyLocalTemplate As String = "GRCLD-{0}-{1}-{2}-{3}-{4}"

        Select Case m_cachingType
            Case Enums.CachingType.NoCache
                rateDefs = GetRateClassList_DataSource(sServiceTerritory, eLanguage, eCustomerType, eFuelType, bDefaultOnly)

            Case Enums.CachingType.LocalCache
                Dim cacheKeySB As New StringBuilder()
                Dim cacheKey As String

                cacheKey = cacheKeySB.AppendFormat(cacheKeyLocalTemplate, sServiceTerritory, CInt(eLanguage), CInt(eCustomerType), CInt(eFuelType), CInt(bDefaultOnly)).ToString()

                rateDefs = CacheLayer.Get(Of SimpleRateDefinitionCollection)(cacheKey)

                If (rateDefs Is Nothing) Then
                    rateDefs = GetRateClassList_DataSource(sServiceTerritory, eLanguage, eCustomerType, eFuelType, bDefaultOnly)
                    If (Not rateDefs Is Nothing) Then
                        CacheLayer.Add(rateDefs, cacheKey, m_cacheTimeoutInMinutes)
                    Else
                        ' did not get object; return an empty collection
                        rateDefs = New SimpleRateDefinitionCollection()
                    End If
                End If

            Case Enums.CachingType.DistributedCache
                Throw New ArgumentException(ksUnimplementedCacheTypeDistributedCache)

            Case Enums.CachingType.FlatLocalCache
                Throw New ArgumentException(ksUnimplementedCacheTypeFlatLocalCache)

            Case Enums.CachingType.FlatDistributedCache
                Throw New ArgumentException(ksUnimplementedCacheTypeFlatDistributedCache)

            Case Else
                Throw New ArgumentException(ksUnknownCacheType)

        End Select


        Return (rateDefs)
    End Function

    Private Function GetRateClassList_DataSource(ByVal sServiceTerritory As String, ByVal eLanguage As Language, ByVal eCustomerType As CustomerType, ByVal eFuelType As FuelType, ByVal bDefaultOnly As Boolean) As SimpleRateDefinitionCollection
        Dim objSimpleRateDefColl As SimpleRateDefinitionCollection

        Select Case m_dataSourceType

            Case DataSourceType.Database
                Dim sbSQL As New StringBuilder
                Dim arParams(4) As SqlParameter
                sbSQL.Append("rateengine.SelectRateClassListD")
                arParams(0) = New SqlParameter("@ServiceTerritoryID", sServiceTerritory)
                arParams(0).Direction = ParameterDirection.Input
                arParams(1) = New SqlParameter("@Language", CType(eLanguage, Integer))
                arParams(1).Direction = ParameterDirection.Input
                arParams(2) = New SqlParameter("@CustomerType", eCustomerType)
                arParams(2).Direction = ParameterDirection.Input
                arParams(3) = New SqlParameter("@FuelType", eFuelType)
                arParams(3).Direction = ParameterDirection.Input
                arParams(4) = New SqlParameter("@Default", bDefaultOnly)
                arParams(4).Direction = ParameterDirection.Input

                objSimpleRateDefColl = MainQueryRateClassList(arParams, sbSQL)

            Case DataSourceType.XmlFile
                Throw New ArgumentException(ksUnimplementedDataSourceXmlFile)

            Case Else
                Throw New ArgumentException(ksUnknownDataSource)

        End Select

        Return (objSimpleRateDefColl)
    End Function

#End Region

#Region "GetRateClassList Overload E - (LocalCache implemented)"

    Public Function GetRateClassList(ByVal nRateCompanyID As Integer, ByVal sServiceTerritory As String, ByVal eLanguage As Language) As SimpleRateDefinitionCollection
        Return (GetRateClassList_Cache(nRateCompanyID, sServiceTerritory, eLanguage))
    End Function

    Private Function GetRateClassList_Cache(ByVal nRateCompanyID As Integer, ByVal sServiceTerritory As String, ByVal eLanguage As Language) As SimpleRateDefinitionCollection
        Dim rateDefs As SimpleRateDefinitionCollection
        Const cacheKeyLocalTemplate As String = "GRCLE-{0}-{1}-{2}"

        Select Case m_cachingType
            Case Enums.CachingType.NoCache
                rateDefs = GetRateClassList_DataSource(nRateCompanyID, sServiceTerritory, eLanguage)

            Case Enums.CachingType.LocalCache
                Dim cacheKeySB As New StringBuilder()
                Dim cacheKey As String

                cacheKey = cacheKeySB.AppendFormat(cacheKeyLocalTemplate, nRateCompanyID, sServiceTerritory, CInt(eLanguage)).ToString()

                rateDefs = CacheLayer.Get(Of SimpleRateDefinitionCollection)(cacheKey)

                If (rateDefs Is Nothing) Then
                    rateDefs = GetRateClassList_DataSource(nRateCompanyID, sServiceTerritory, eLanguage)
                    If (Not rateDefs Is Nothing) Then
                        CacheLayer.Add(rateDefs, cacheKey, m_cacheTimeoutInMinutes)
                    Else
                        ' did not get object; return an empty collection
                        rateDefs = New SimpleRateDefinitionCollection()
                    End If
                End If

            Case Enums.CachingType.DistributedCache
                Throw New ArgumentException(ksUnimplementedCacheTypeDistributedCache)

            Case Enums.CachingType.FlatLocalCache
                Throw New ArgumentException(ksUnimplementedCacheTypeFlatLocalCache)

            Case Enums.CachingType.FlatDistributedCache
                Throw New ArgumentException(ksUnimplementedCacheTypeFlatDistributedCache)

            Case Else
                Throw New ArgumentException(ksUnknownCacheType)

        End Select

        Return (rateDefs)
    End Function

    Private Function GetRateClassList_DataSource(ByVal nRateCompanyID As Integer, ByVal sServiceTerritory As String, ByVal eLanguage As Language) As SimpleRateDefinitionCollection
        Dim objSimpleRateDefColl As SimpleRateDefinitionCollection

        Select Case m_dataSourceType

            Case DataSourceType.Database
                Dim sbSQL As New StringBuilder
                Dim arParams(2) As SqlParameter
                sbSQL.Append("rateengine.SelectRateClassListE")
                arParams(0) = New SqlParameter("@RateCompanyID", nRateCompanyID)
                arParams(0).Direction = ParameterDirection.Input
                arParams(1) = New SqlParameter("@ServiceTerritoryID", sServiceTerritory)
                arParams(1).Direction = ParameterDirection.Input
                arParams(2) = New SqlParameter("@Language", CType(eLanguage, Integer))
                arParams(2).Direction = ParameterDirection.Input

                objSimpleRateDefColl = MainQueryRateClassList(arParams, sbSQL)

            Case DataSourceType.XmlFile
                Throw New ArgumentException(ksUnimplementedDataSourceXmlFile)

            Case Else
                Throw New ArgumentException(ksUnknownDataSource)

        End Select

        Return (objSimpleRateDefColl)
    End Function

#End Region

#Region "GetRateClassList Overload F - (LocalCache implemented)"

    Public Function GetRateClassList(ByVal nRateCompanyID As Integer, ByVal sServiceTerritory As String, ByVal eLanguage As Language, ByVal eCustomerType As CustomerType, ByVal eFuelType As FuelType, ByVal bDefaultOnly As Boolean) As SimpleRateDefinitionCollection
        Return (GetRateClassList_Cache(nRateCompanyID, sServiceTerritory, eLanguage, eCustomerType, eFuelType, bDefaultOnly))
    End Function

    Private Function GetRateClassList_Cache(ByVal nRateCompanyID As Integer, ByVal sServiceTerritory As String, ByVal eLanguage As Language, ByVal eCustomerType As CustomerType, ByVal eFuelType As FuelType, ByVal bDefaultOnly As Boolean) As SimpleRateDefinitionCollection
        Dim rateDefs As SimpleRateDefinitionCollection
        Const cacheKeyLocalTemplate As String = "GRCLF-{0}-{1}-{2}-{3}-{4}-{5}"

        Select Case m_cachingType
            Case Enums.CachingType.NoCache
                rateDefs = GetRateClassList_DataSource(nRateCompanyID, sServiceTerritory, eLanguage, eCustomerType, eFuelType, bDefaultOnly)

            Case Enums.CachingType.LocalCache
                Dim cacheKeySB As New StringBuilder()
                Dim cacheKey As String

                cacheKey = cacheKeySB.AppendFormat(cacheKeyLocalTemplate, nRateCompanyID, sServiceTerritory, CInt(eLanguage), CInt(eCustomerType), CInt(eFuelType), CInt(bDefaultOnly)).ToString()

                rateDefs = CacheLayer.Get(Of SimpleRateDefinitionCollection)(cacheKey)

                If (rateDefs Is Nothing) Then
                    rateDefs = GetRateClassList_DataSource(nRateCompanyID, sServiceTerritory, eLanguage, eCustomerType, eFuelType, bDefaultOnly)
                    If (Not rateDefs Is Nothing) Then
                        CacheLayer.Add(rateDefs, cacheKey, m_cacheTimeoutInMinutes)
                    Else
                        ' did not get object; return an empty collection
                        rateDefs = New SimpleRateDefinitionCollection()
                    End If
                End If

            Case Enums.CachingType.DistributedCache
                Throw New ArgumentException(ksUnimplementedCacheTypeDistributedCache)

            Case Enums.CachingType.FlatLocalCache
                Throw New ArgumentException(ksUnimplementedCacheTypeFlatLocalCache)

            Case Enums.CachingType.FlatDistributedCache
                Throw New ArgumentException(ksUnimplementedCacheTypeFlatDistributedCache)

            Case Else
                Throw New ArgumentException(ksUnknownCacheType)

        End Select

        Return (rateDefs)
    End Function

    Private Function GetRateClassList_DataSource(ByVal nRateCompanyID As Integer, ByVal sServiceTerritory As String, ByVal eLanguage As Language, ByVal eCustomerType As CustomerType, ByVal eFuelType As FuelType, ByVal bDefaultOnly As Boolean) As SimpleRateDefinitionCollection
        Dim objSimpleRateDefColl As SimpleRateDefinitionCollection

        Select Case m_dataSourceType

            Case DataSourceType.Database
                Dim sbSQL As New StringBuilder
                Dim arParams(5) As SqlParameter
                sbSQL.Append("rateengine.SelectRateClassListF")
                arParams(0) = New SqlParameter("@RateCompanyID", nRateCompanyID)
                arParams(0).Direction = ParameterDirection.Input
                arParams(1) = New SqlParameter("@ServiceTerritoryID", sServiceTerritory)
                arParams(1).Direction = ParameterDirection.Input
                arParams(2) = New SqlParameter("@Language", CType(eLanguage, Integer))
                arParams(2).Direction = ParameterDirection.Input
                arParams(3) = New SqlParameter("@CustomerType", eCustomerType)
                arParams(3).Direction = ParameterDirection.Input
                arParams(4) = New SqlParameter("@FuelType", eFuelType)
                arParams(4).Direction = ParameterDirection.Input
                arParams(5) = New SqlParameter("@Default", bDefaultOnly)
                arParams(5).Direction = ParameterDirection.Input

                objSimpleRateDefColl = MainQueryRateClassList(arParams, sbSQL)

            Case DataSourceType.XmlFile
                Throw New ArgumentException(ksUnimplementedDataSourceXmlFile)

            Case Else
                Throw New ArgumentException(ksUnknownDataSource)

        End Select

        Return (objSimpleRateDefColl)
    End Function

#End Region

    ' Private Core Function for all GetRateClassList functions above
    Private Function MainQueryRateClassList(ByVal arParams() As SqlParameter, ByVal sbSQL As StringBuilder) As SimpleRateDefinitionCollection
        Dim objSimpleRateDefColl As New SimpleRateDefinitionCollection
        Dim objSimpleRateDef As SimpleRateDefinition
        Dim dr As SqlDataReader= Nothing
        Dim sConn As String

        sConn = RateEngineConnectionString()

        Try
            dr = SqlHelper.ExecuteReader(sConn, CommandType.StoredProcedure, sbSQL.ToString(), arParams)

            Do While dr.Read()
                objSimpleRateDef = New SimpleRateDefinition
                If (Not IsDBNull(dr("RateDefID"))) Then objSimpleRateDef.DefinitionID = CType(dr("RateDefID"), Integer)
                If (Not IsDBNull(dr("RdRateMasterID"))) Then objSimpleRateDef.MasterID = CType(dr("RdRateMasterID"), Integer)
                If (Not IsDBNull(dr("RmRateCompanyID"))) Then objSimpleRateDef.RateCompanyID = CType(dr("RmRateCompanyID"), Integer)
                If (Not IsDBNull(dr("RmClientRateID"))) Then objSimpleRateDef.RateClass = CType(dr("RmClientRateID"), String)
                If (Not IsDBNull(dr("RmDefault"))) Then objSimpleRateDef.Default = CType(dr("RmDefault"), Boolean)
                If (Not IsDBNull(dr("RmChildrenInd"))) Then objSimpleRateDef.Children = CType(dr("RmChildrenInd"), Boolean)
                If (Not IsDBNull(dr("Name"))) Then objSimpleRateDef.Name = CType(dr("Name"), String)
                If (Not IsDBNull(dr("Description"))) Then objSimpleRateDef.Description = CType(dr("Description"), String)
                If (Not IsDBNull(dr("Eligibility"))) Then objSimpleRateDef.Eligibility = CType(dr("Eligibility"), String)
                If (Not IsDBNull(dr("RdDifferenceNote"))) Then objSimpleRateDef.DifferenceNote = CType(dr("RdDifferenceNote"), String) Else objSimpleRateDef.DifferenceNote = String.Empty
                If (Not IsDBNull(dr("RdSeasonalInd"))) Then objSimpleRateDef.Seasonal = CType(dr("RdSeasonalInd"), Boolean)
                If (Not IsDBNull(dr("RdTieredInd"))) Then objSimpleRateDef.Tiered = CType(dr("RdTieredInd"), Boolean)
                If (Not IsDBNull(dr("RdTimeOfUseInd"))) Then objSimpleRateDef.TimeOfUse = CType(dr("RdTimeOfUseInd"), Boolean)
                If (Not IsDBNull(dr("RdDemandInd"))) Then objSimpleRateDef.Demand = CType(dr("RdDemandInd"), Boolean)
                If (Not IsDBNull(dr("RdBasicInd"))) Then objSimpleRateDef.Basic = CType(dr("RdBasicInd"), Boolean)
                If (Not IsDBNull(dr("RdFuelAdjustmentInd"))) Then objSimpleRateDef.FuelAdjustment = CType(dr("RdFuelAdjustmentInd"), Boolean)
                If (Not IsDBNull(dr("RdStartDate"))) Then objSimpleRateDef.StartDate = CType(dr("RdStartDate"), DateTime)
                If (Not IsDBNull(dr("RdTsID"))) Then objSimpleRateDef.TierStructureType = CType(dr("RdTsID"), TierStructureType)
                If (Not IsDBNull(dr("RdDsID"))) Then objSimpleRateDef.DemandStructureType = CType(dr("RdDsID"), DemandStructureType)
                If (Not IsDBNull(dr("RdSpID"))) Then objSimpleRateDef.SeasonalProrateType = CType(dr("RdSpID"), SeasonalProrateType)
                If (Not IsDBNull(dr("RdSdpID"))) Then objSimpleRateDef.SeasonalDemandProrateType = CType(dr("RdSdpID"), SeasonalDemandProrateType)
                If (Not IsDBNull(dr("RmFuID"))) Then objSimpleRateDef.FuelType = CType(dr("RmFuID"), FuelType)
                If (Not IsDBNull(dr("RmSvID"))) Then objSimpleRateDef.ServiceType = CType(dr("RmSvID"), ServiceType)
                If (Not IsDBNull(dr("RmCtID"))) Then objSimpleRateDef.CustomerType = CType(dr("RmCtID"), CustomerType)
                If (Not IsDBNull(dr("RmUnID"))) Then objSimpleRateDef.UnitOfMeasure = CType(dr("RmUnID"), Integer)
                If (Not IsDBNull(dr("RmPrID"))) Then objSimpleRateDef.ProrateType = CType(dr("RmPrID"), ProrateType)
                If (Not IsDBNull(dr("RmDPrID"))) Then objSimpleRateDef.DemandProrateType = CType(dr("RmDPrID"), DemandProrateType)
                If (Not IsDBNull(dr("RmLtID"))) Then objSimpleRateDef.LoadType = CType(dr("RmLtID"), LoadType)
                If (Not IsDBNull(dr("RmBdID"))) Then objSimpleRateDef.BilledDemandType = CType(dr("RmBdID"), BilledDemandType)
                If (Not IsDBNull(dr("PoID"))) Then objSimpleRateDef.Pollution.PollutionID = CType(dr("PoID"), Integer)
                If (Not IsDBNull(dr("PoCO2"))) Then objSimpleRateDef.Pollution.CO2 = CType(dr("PoCO2"), Double)
                If (Not IsDBNull(dr("PoSO2"))) Then objSimpleRateDef.Pollution.SO2 = CType(dr("PoSO2"), Double)
                If (Not IsDBNull(dr("PoNOx"))) Then objSimpleRateDef.Pollution.NOx = CType(dr("PoNOx"), Double)
                If (Not IsDBNull(dr("RdTOUBoundariesConsistent"))) Then objSimpleRateDef.BoundariesConsistent = CType(dr("RdTOUBoundariesConsistent"), Boolean) Else objSimpleRateDef.BoundariesConsistent = False
                If (Not IsDBNull(dr("RdRTPInd"))) Then objSimpleRateDef.RTPRate = CType(dr("RdRTPInd"), Boolean) Else objSimpleRateDef.RTPRate = False
                If (Not IsDBNull(dr("RdRTPStreamId"))) Then objSimpleRateDef.RTPStream = CType(dr("RdRTPStreamId"), Enums.RTPStream) Else objSimpleRateDef.RTPStream = RTPStream.None
                If (Not IsDBNull(dr("RmHgID"))) Then objSimpleRateDef.HolidayGroupID = CType(dr("RmHgID"), Integer) Else objSimpleRateDef.HolidayGroupID = -1
                If (Not IsDBNull(dr("RdBrID"))) Then objSimpleRateDef.BaselineRule = CType(dr("RdBrID"), BaselineRule)
                If (Not IsDBNull(dr("RdNmID"))) Then objSimpleRateDef.NetMeteringType = CType(dr("RdNmID"), NetMeteringType)
                If (Not IsDBNull(dr("RdNetMeteringInd"))) Then objSimpleRateDef.NetMetering = CType(dr("RdNetMeteringInd"), Boolean)
                If (Not IsDBNull(dr("RdTierAccumByTOUInd"))) Then objSimpleRateDef.IndependentTierAccumulationsByTOU = CType(dr("RdTierAccumByTOUInd"), Boolean) Else objSimpleRateDef.IndependentTierAccumulationsByTOU = False
                If (Not IsDBNull(dr("RdCapResInd"))) Then objSimpleRateDef.CapacityReserve = CType(dr("RdCapResInd"), Boolean) Else objSimpleRateDef.CapacityReserve = False
                If (Not IsDBNull(dr("RdCPEventUsageAdderInd"))) Then objSimpleRateDef.CPEventUsageAdder = CType(dr("RdCPEventUsageAdderInd"), Boolean) Else objSimpleRateDef.CPEventUsageAdder = False

                objSimpleRateDefColl.Add(objSimpleRateDef)
            Loop

        Catch ex As Exception
            Throw
        Finally
            If (Not dr Is Nothing) Then
                If (Not dr.IsClosed) Then
                    dr.Close()
                End If
            End If
        End Try

        ' There may be associated child rate classes;
        objSimpleRateDefColl = DoMasterChildIteration(objSimpleRateDefColl)

        Return (objSimpleRateDefColl)
    End Function

#End Region

#Region "Data Controller GetSimpleRateInformation Overloads (LocalCache implemented)"

#Region "GetSimpleRateInformation Overload A - (LocalCache implemented)"

    Public Function GetSimpleRateInformation(ByVal nRateCompanyID As Integer, ByVal sRateClass As String, ByVal dtStartDate As DateTime, ByVal dtEndDate As DateTime, ByVal eLanguage As Language) As SimpleRateDefinitionCollection
        Return (GetSimpleRateInformation_Cache(nRateCompanyID, sRateClass, dtStartDate, dtEndDate, eLanguage))
    End Function

    Private Function GetSimpleRateInformation_Cache(ByVal nRateCompanyID As Integer, ByVal sRateClass As String, ByVal dtStartDate As DateTime, ByVal dtEndDate As DateTime, ByVal eLanguage As Language) As SimpleRateDefinitionCollection
        Dim rateDefs As SimpleRateDefinitionCollection
        Const cacheKeyLocalTemplate As String = "GSRIA-{0}-{1}-{2}-{3}-{4}"

        Select Case m_cachingType
            Case Enums.CachingType.NoCache
                rateDefs = GetSimpleRateInformation_DataSource(nRateCompanyID, sRateClass, dtStartDate, dtEndDate, eLanguage)

            Case Enums.CachingType.LocalCache
                Dim cacheKeySB As New StringBuilder()
                Dim cacheKey As String

                cacheKey = cacheKeySB.AppendFormat(cacheKeyLocalTemplate, nRateCompanyID, sRateClass, DateTimeToEpoch(dtStartDate), DateTimeToEpoch(dtEndDate), CInt(eLanguage)).ToString()

                rateDefs = CacheLayer.Get(Of SimpleRateDefinitionCollection)(cacheKey)

                If (rateDefs Is Nothing) Then
                    rateDefs = GetSimpleRateInformation_DataSource(nRateCompanyID, sRateClass, dtStartDate, dtEndDate, eLanguage)
                    If (Not rateDefs Is Nothing) Then
                        CacheLayer.Add(rateDefs, cacheKey, m_cacheTimeoutInMinutes)
                    Else
                        ' did not get object; return an empty collection
                        rateDefs = New SimpleRateDefinitionCollection()
                    End If
                End If

            Case Enums.CachingType.DistributedCache
                Throw New ArgumentException(ksUnimplementedCacheTypeDistributedCache)

            Case Enums.CachingType.FlatLocalCache
                Throw New ArgumentException(ksUnimplementedCacheTypeFlatLocalCache)

            Case Enums.CachingType.FlatDistributedCache
                Throw New ArgumentException(ksUnimplementedCacheTypeFlatDistributedCache)

            Case Else
                Throw New ArgumentException(ksUnknownCacheType)

        End Select

        Return (rateDefs)
    End Function

    Private Function GetSimpleRateInformation_DataSource(ByVal nRateCompanyID As Integer, ByVal sRateClass As String, ByVal dtStartDate As DateTime, ByVal dtEndDate As DateTime, ByVal eLanguage As Language) As SimpleRateDefinitionCollection
        Dim objSimpleRateDefColl As SimpleRateDefinitionCollection

        Select Case m_dataSourceType

            Case DataSourceType.Database
                Dim sbSQL As New StringBuilder
                Dim arParams(4) As SqlParameter
                sbSQL.Append("rateengine.SelectSimpleRateInformationA")
                arParams(0) = New SqlParameter("@RateCompanyID", nRateCompanyID)
                arParams(0).Direction = ParameterDirection.Input
                arParams(1) = New SqlParameter("@RateClass", sRateClass)
                arParams(1).Direction = ParameterDirection.Input
                arParams(2) = New SqlParameter("@StartDate", dtStartDate)
                arParams(2).Direction = ParameterDirection.Input
                arParams(3) = New SqlParameter("@EndDate", dtEndDate)
                arParams(3).Direction = ParameterDirection.Input
                arParams(4) = New SqlParameter("@Language", CType(eLanguage, Integer))
                arParams(4).Direction = ParameterDirection.Input

                objSimpleRateDefColl = MainQuerySimpleRateInformation(arParams, sbSQL)

            Case DataSourceType.XmlFile
                Throw New ArgumentException(ksUnimplementedDataSourceXmlFile)

            Case Else
                Throw New ArgumentException(ksUnknownDataSource)

        End Select

        Return (objSimpleRateDefColl)
    End Function

#End Region

#Region "GetSimpleRateInformation Overload B - (LocalCache implemented)"

    Public Function GetSimpleRateInformation(ByVal nRateCompanyID As Integer, ByVal sRateClass As String, ByVal eLanguage As Language) As SimpleRateDefinitionCollection
        Return (GetSimpleRateInformation_Cache(nRateCompanyID, sRateClass, eLanguage))
    End Function

    Private Function GetSimpleRateInformation_Cache(ByVal nRateCompanyID As Integer, ByVal sRateClass As String, ByVal eLanguage As Language) As SimpleRateDefinitionCollection
        Dim rateDefs As SimpleRateDefinitionCollection
        Const cacheKeyLocalTemplate As String = "GSRIB-{0}-{1}-{2}"

        Select Case m_cachingType
            Case Enums.CachingType.NoCache
                rateDefs = GetSimpleRateInformation_DataSource(nRateCompanyID, sRateClass, eLanguage)

            Case Enums.CachingType.LocalCache
                Dim cacheKeySB As New StringBuilder()
                Dim cacheKey As String

                cacheKey = cacheKeySB.AppendFormat(cacheKeyLocalTemplate, nRateCompanyID, sRateClass, CInt(eLanguage)).ToString()

                rateDefs = CacheLayer.Get(Of SimpleRateDefinitionCollection)(cacheKey)

                If (rateDefs Is Nothing) Then
                    rateDefs = GetSimpleRateInformation_DataSource(nRateCompanyID, sRateClass, eLanguage)
                    If (Not rateDefs Is Nothing) Then
                        CacheLayer.Add(rateDefs, cacheKey, m_cacheTimeoutInMinutes)
                    Else
                        ' did not get object; return an empty collection
                        rateDefs = New SimpleRateDefinitionCollection()
                    End If
                End If

            Case Enums.CachingType.DistributedCache
                Throw New ArgumentException(ksUnimplementedCacheTypeDistributedCache)

            Case Enums.CachingType.FlatLocalCache
                Throw New ArgumentException(ksUnimplementedCacheTypeFlatLocalCache)

            Case Enums.CachingType.FlatDistributedCache
                Throw New ArgumentException(ksUnimplementedCacheTypeFlatDistributedCache)

            Case Else
                Throw New ArgumentException(ksUnknownCacheType)

        End Select

        Return (rateDefs)
    End Function

    Private Function GetSimpleRateInformation_DataSource(ByVal nRateCompanyID As Integer, ByVal sRateClass As String, ByVal eLanguage As Language) As SimpleRateDefinitionCollection
        Dim objSimpleRateDefColl As SimpleRateDefinitionCollection

        Select Case m_dataSourceType

            Case DataSourceType.Database
                Dim sbSQL As New StringBuilder
                Dim arParams(2) As SqlParameter
                sbSQL.Append("rateengine.SelectSimpleRateInformationB")
                arParams(0) = New SqlParameter("@RateCompanyID", nRateCompanyID)
                arParams(0).Direction = ParameterDirection.Input
                arParams(1) = New SqlParameter("@RateClass", sRateClass)
                arParams(1).Direction = ParameterDirection.Input
                arParams(2) = New SqlParameter("@Language", CType(eLanguage, Integer))
                arParams(2).Direction = ParameterDirection.Input

                objSimpleRateDefColl = MainQuerySimpleRateInformation(arParams, sbSQL)

            Case DataSourceType.XmlFile
                Throw New ArgumentException(ksUnimplementedDataSourceXmlFile)

            Case Else
                Throw New ArgumentException(ksUnknownDataSource)

        End Select

        Return (objSimpleRateDefColl)
    End Function

#End Region

#Region "GetSimpleRateInformation Overload C - (LocalCache implemented)"

    Public Function GetSimpleRateInformation(ByVal nRateDefinitionID As Integer, ByVal eLanguage As Language) As SimpleRateDefinition
        Return (GetSimpleRateInformation_Cache(nRateDefinitionID, eLanguage))
    End Function

    Private Function GetSimpleRateInformation_Cache(ByVal nRateDefinitionID As Integer, ByVal eLanguage As Language) As SimpleRateDefinition
        Dim rateDef As SimpleRateDefinition
        Const cacheKeyLocalTemplate As String = "GSRIC-{0}-{1}"

        Select Case m_cachingType
            Case Enums.CachingType.NoCache
                rateDef = GetSimpleRateInformation_DataSource(nRateDefinitionID, eLanguage)

            Case Enums.CachingType.LocalCache
                Dim cacheKeySB As New StringBuilder()
                Dim cacheKey As String

                cacheKey = cacheKeySB.AppendFormat(cacheKeyLocalTemplate, nRateDefinitionID, CInt(eLanguage)).ToString()

                rateDef = CacheLayer.Get(Of SimpleRateDefinition)(cacheKey)

                If (rateDef Is Nothing) Then
                    rateDef = GetSimpleRateInformation_DataSource(nRateDefinitionID, eLanguage)
                    If (Not rateDef Is Nothing) Then
                        CacheLayer.Add(rateDef, cacheKey, m_cacheTimeoutInMinutes)
                    Else
                        ' did not get object; return nothing
                        rateDef = Nothing
                    End If
                End If

            Case Enums.CachingType.DistributedCache
                Throw New ArgumentException(ksUnimplementedCacheTypeDistributedCache)

            Case Enums.CachingType.FlatLocalCache
                Throw New ArgumentException(ksUnimplementedCacheTypeFlatLocalCache)

            Case Enums.CachingType.FlatDistributedCache
                Throw New ArgumentException(ksUnimplementedCacheTypeFlatDistributedCache)

            Case Else
                Throw New ArgumentException(ksUnknownCacheType)

        End Select

        Return (rateDef)
    End Function

    Private Function GetSimpleRateInformation_DataSource(ByVal nRateDefinitionID As Integer, ByVal eLanguage As Language) As SimpleRateDefinition
        Dim objSimpleRateDefColl As SimpleRateDefinitionCollection
        Dim objSimpleRateDef As SimpleRateDefinition

        Select Case m_dataSourceType

            Case DataSourceType.Database
                Dim sbSQL As New StringBuilder
                Dim arParams(1) As SqlParameter
                sbSQL.Append("rateengine.SelectSimpleRateInformationC")
                arParams(0) = New SqlParameter("@RateDefinitionID", nRateDefinitionID)
                arParams(0).Direction = ParameterDirection.Input
                arParams(1) = New SqlParameter("@Language", CType(eLanguage, Integer))
                arParams(1).Direction = ParameterDirection.Input

                objSimpleRateDefColl = MainQuerySimpleRateInformation(arParams, sbSQL)

                If (objSimpleRateDefColl.Count > 0) Then
                    objSimpleRateDef = objSimpleRateDefColl.Item(0)
                Else
                    objSimpleRateDef = Nothing
                End If

            Case DataSourceType.XmlFile
                Throw New ArgumentException(ksUnimplementedDataSourceXmlFile)

            Case Else
                Throw New ArgumentException(ksUnknownDataSource)

        End Select

        Return (objSimpleRateDef)
    End Function

#End Region

    ' Private Core Function for all GetSimpleRateInformation functions above
    Private Function MainQuerySimpleRateInformation(ByVal arParams() As SqlParameter, ByVal sbSQL As StringBuilder) As SimpleRateDefinitionCollection
        Dim objSimpleRateDefColl As New SimpleRateDefinitionCollection
        Dim objSimpleRateDef As SimpleRateDefinition
        Dim dr As SqlDataReader= Nothing
        Dim sConn As String

        sConn = RateEngineConnectionString()

        Try
            dr = SqlHelper.ExecuteReader(sConn, CommandType.StoredProcedure, sbSQL.ToString(), arParams)

            Do While dr.Read()
                objSimpleRateDef = New SimpleRateDefinition
                If (Not IsDBNull(dr("RateDefID"))) Then objSimpleRateDef.DefinitionID = CType(dr("RateDefID"), Integer)
                If (Not IsDBNull(dr("RdRateMasterID"))) Then objSimpleRateDef.MasterID = CType(dr("RdRateMasterID"), Integer)
                If (Not IsDBNull(dr("RmRateCompanyID"))) Then objSimpleRateDef.RateCompanyID = CType(dr("RmRateCompanyID"), Integer)
                If (Not IsDBNull(dr("RmClientRateID"))) Then objSimpleRateDef.RateClass = CType(dr("RmClientRateID"), String)
                If (Not IsDBNull(dr("RmDefault"))) Then objSimpleRateDef.Default = CType(dr("RmDefault"), Boolean)
                If (Not IsDBNull(dr("RmChildrenInd"))) Then objSimpleRateDef.Children = CType(dr("RmChildrenInd"), Boolean)
                If (Not IsDBNull(dr("Name"))) Then objSimpleRateDef.Name = CType(dr("Name"), String)
                If (Not IsDBNull(dr("Description"))) Then objSimpleRateDef.Description = CType(dr("Description"), String)
                If (Not IsDBNull(dr("Eligibility"))) Then objSimpleRateDef.Eligibility = CType(dr("Eligibility"), String)
                If (Not IsDBNull(dr("RdDifferenceNote"))) Then objSimpleRateDef.DifferenceNote = CType(dr("RdDifferenceNote"), String) Else objSimpleRateDef.DifferenceNote = String.Empty
                If (Not IsDBNull(dr("RdSeasonalInd"))) Then objSimpleRateDef.Seasonal = CType(dr("RdSeasonalInd"), Boolean)
                If (Not IsDBNull(dr("RdTieredInd"))) Then objSimpleRateDef.Tiered = CType(dr("RdTieredInd"), Boolean)
                If (Not IsDBNull(dr("RdTimeOfUseInd"))) Then objSimpleRateDef.TimeOfUse = CType(dr("RdTimeOfUseInd"), Boolean)
                If (Not IsDBNull(dr("RdDemandInd"))) Then objSimpleRateDef.Demand = CType(dr("RdDemandInd"), Boolean)
                If (Not IsDBNull(dr("RdBasicInd"))) Then objSimpleRateDef.Basic = CType(dr("RdBasicInd"), Boolean)
                If (Not IsDBNull(dr("RdFuelAdjustmentInd"))) Then objSimpleRateDef.FuelAdjustment = CType(dr("RdFuelAdjustmentInd"), Boolean)
                If (Not IsDBNull(dr("RdStartDate"))) Then objSimpleRateDef.StartDate = CType(dr("RdStartDate"), DateTime)
                If (Not IsDBNull(dr("RdTsID"))) Then objSimpleRateDef.TierStructureType = CType(dr("RdTsID"), TierStructureType)
                If (Not IsDBNull(dr("RdDsID"))) Then objSimpleRateDef.DemandStructureType = CType(dr("RdDsID"), DemandStructureType)
                If (Not IsDBNull(dr("RdSpID"))) Then objSimpleRateDef.SeasonalProrateType = CType(dr("RdSpID"), SeasonalProrateType)
                If (Not IsDBNull(dr("RdSdpID"))) Then objSimpleRateDef.SeasonalDemandProrateType = CType(dr("RdSdpID"), SeasonalDemandProrateType)
                If (Not IsDBNull(dr("RmFuID"))) Then objSimpleRateDef.FuelType = CType(dr("RmFuID"), FuelType)
                If (Not IsDBNull(dr("RmSvID"))) Then objSimpleRateDef.ServiceType = CType(dr("RmSvID"), ServiceType)
                If (Not IsDBNull(dr("RmCtID"))) Then objSimpleRateDef.CustomerType = CType(dr("RmCtID"), CustomerType)
                If (Not IsDBNull(dr("RmUnID"))) Then objSimpleRateDef.UnitOfMeasure = CType(dr("RmUnID"), Integer)
                If (Not IsDBNull(dr("RmPrID"))) Then objSimpleRateDef.ProrateType = CType(dr("RmPrID"), ProrateType)
                If (Not IsDBNull(dr("RmDPrID"))) Then objSimpleRateDef.DemandProrateType = CType(dr("RmDPrID"), DemandProrateType)
                If (Not IsDBNull(dr("RmLtID"))) Then objSimpleRateDef.LoadType = CType(dr("RmLtID"), LoadType)
                If (Not IsDBNull(dr("RmBdID"))) Then objSimpleRateDef.BilledDemandType = CType(dr("RmBdID"), BilledDemandType)
                If (Not IsDBNull(dr("PoID"))) Then objSimpleRateDef.Pollution.PollutionID = CType(dr("PoID"), Integer)
                If (Not IsDBNull(dr("PoCO2"))) Then objSimpleRateDef.Pollution.CO2 = CType(dr("PoCO2"), Double)
                If (Not IsDBNull(dr("PoSO2"))) Then objSimpleRateDef.Pollution.SO2 = CType(dr("PoSO2"), Double)
                If (Not IsDBNull(dr("PoNOx"))) Then objSimpleRateDef.Pollution.NOx = CType(dr("PoNOx"), Double)
                If (Not IsDBNull(dr("RdTOUBoundariesConsistent"))) Then objSimpleRateDef.BoundariesConsistent = CType(dr("RdTOUBoundariesConsistent"), Boolean) Else objSimpleRateDef.BoundariesConsistent = False
                If (Not IsDBNull(dr("RdRTPInd"))) Then objSimpleRateDef.RTPRate = CType(dr("RdRTPInd"), Boolean) Else objSimpleRateDef.RTPRate = False
                If (Not IsDBNull(dr("RdRTPStreamId"))) Then objSimpleRateDef.RTPStream = CType(dr("RdRTPStreamId"), Enums.RTPStream) Else objSimpleRateDef.RTPStream = RTPStream.None
                If (Not IsDBNull(dr("RmHgID"))) Then objSimpleRateDef.HolidayGroupID = CType(dr("RmHgID"), Integer) Else objSimpleRateDef.HolidayGroupID = -1
                If (Not IsDBNull(dr("RdBrID"))) Then objSimpleRateDef.BaselineRule = CType(dr("RdBrID"), BaselineRule)
                If (Not IsDBNull(dr("RdNmID"))) Then objSimpleRateDef.NetMeteringType = CType(dr("RdNmID"), NetMeteringType)
                If (Not IsDBNull(dr("RdNetMeteringInd"))) Then objSimpleRateDef.NetMetering = CType(dr("RdNetMeteringInd"), Boolean)
                If (Not IsDBNull(dr("RdTierAccumByTOUInd"))) Then objSimpleRateDef.IndependentTierAccumulationsByTOU = CType(dr("RdTierAccumByTOUInd"), Boolean) Else objSimpleRateDef.IndependentTierAccumulationsByTOU = False
                If (Not IsDBNull(dr("RdCapResInd"))) Then objSimpleRateDef.CapacityReserve = CType(dr("RdCapResInd"), Boolean) Else objSimpleRateDef.CapacityReserve = False
                If (Not IsDBNull(dr("RdCPEventUsageAdderInd"))) Then objSimpleRateDef.CPEventUsageAdder = CType(dr("RdCPEventUsageAdderInd"), Boolean) Else objSimpleRateDef.CPEventUsageAdder = False

                objSimpleRateDefColl.Add(objSimpleRateDef)
            Loop

        Catch ex As Exception
            Throw New Exception(ksDataReaderError, ex.InnerException)
        Finally
            If (Not dr Is Nothing) Then
                If (Not dr.IsClosed) Then
                    dr.Close()
                End If
            End If
        End Try

        ' There may be associated child rate classes;
        objSimpleRateDefColl = DoMasterChildIteration(objSimpleRateDefColl)

        Return (objSimpleRateDefColl)
    End Function

#End Region

#Region "Data Controller GetDetailedRateInformation Overloads (LocalCache implemented)"

#Region "GetDetailedRateInformation Overload A - (LocalCache implemented)"

    Public Function GetDetailedRateInformation(ByVal nRateCompanyID As Integer, ByVal sRateClass As String, ByVal dtStartDate As DateTime, ByVal dtEndDate As DateTime, ByVal eLanguage As Language) As DetailedRateDefinitionCollection
        Return (GetDetailedRateInformation_Cache(nRateCompanyID, sRateClass, dtStartDate, dtEndDate, eLanguage))
    End Function

    Private Function GetDetailedRateInformation_Cache(ByVal nRateCompanyID As Integer, ByVal sRateClass As String, ByVal dtStartDate As DateTime, ByVal dtEndDate As DateTime, ByVal eLanguage As Language) As DetailedRateDefinitionCollection
        Dim rateDefs As DetailedRateDefinitionCollection= Nothing
        Const cacheKeyLocalTemplate As String = "GDRIA-{0}-{1}-{2}-{3}-{4}"
        Const cacheKeyFlatLocalTemplate As String = "GDRIA-FLAT-{0}-{1}-{2}-{3}-{4}"

        Select Case m_cachingType
            Case Enums.CachingType.NoCache
                rateDefs = GetDetailedRateInformation_DataSource(nRateCompanyID, sRateClass, dtStartDate, dtEndDate, eLanguage)

            Case Enums.CachingType.LocalCache
                Dim cacheKeySB As New StringBuilder()
                Dim cacheKey As String

                cacheKey = cacheKeySB.AppendFormat(cacheKeyLocalTemplate, nRateCompanyID, sRateClass, DateTimeToEpoch(dtStartDate), DateTimeToEpoch(dtEndDate), CInt(eLanguage)).ToString()

                rateDefs = CacheLayer.Get(Of DetailedRateDefinitionCollection)(cacheKey)

                If (rateDefs Is Nothing) Then
                    rateDefs = GetDetailedRateInformation_DataSource(nRateCompanyID, sRateClass, dtStartDate, dtEndDate, eLanguage)
                    If (Not rateDefs Is Nothing) Then
                        CacheLayer.Add(rateDefs, cacheKey, m_cacheTimeoutInMinutes)
                    Else
                        ' did not get object; return an empty collection
                        rateDefs = New DetailedRateDefinitionCollection()
                    End If
                End If

            Case Enums.CachingType.DistributedCache
                Throw New ArgumentException(ksUnimplementedCacheTypeDistributedCache)

            Case Enums.CachingType.FlatLocalCache
                Dim cacheKeySB As New StringBuilder()
                Dim cacheKey As String
                Dim allRateDefs As DetailedRateDefinitionCollection

                cacheKey = cacheKeySB.AppendFormat(cacheKeyFlatLocalTemplate, nRateCompanyID, sRateClass, DateTimeToEpoch(dtStartDate), DateTimeToEpoch(dtEndDate), CInt(eLanguage)).ToString()

                allRateDefs = CacheLayer.Get(Of DetailedRateDefinitionCollection)(cacheKey)

                If (allRateDefs Is Nothing) Then
                    ' NOTE that the data source here is the overload that selects ALL the definitions for the rate class!  This is intentional.
                    allRateDefs = GetDetailedRateInformation_DataSource(nRateCompanyID, sRateClass, eLanguage)

                    If (Not allRateDefs Is Nothing) Then
                        CacheLayer.Add(allRateDefs, cacheKey, m_cacheTimeoutInMinutes)
                    Else
                        ' did not get object; return an empty collection
                        allRateDefs = New DetailedRateDefinitionCollection()
                    End If

                End If

                ' now select applicable rate defs (rateDefs) from the entire list of rate defs (allRateDefs)
                ' build a list of pointers basically. Use logic from original data source.
                ' requires Option Strict Off
                'rateDefs = (From rd As DetailedRateDefinition In allRateDefs
                '           Where rd.StartDate >= dtStartDate And rd.StartDate <= dtEndDate).Union _
                '          (From rd As DetailedRateDefinition In allRateDefs
                '           Where rd.StartDate <= dtStartDate).Take(1)


            Case Enums.CachingType.FlatDistributedCache
                Throw New ArgumentException(ksUnimplementedCacheTypeFlatDistributedCache)

            Case Else
                Throw New ArgumentException(ksUnknownCacheType)

        End Select

        Return (rateDefs)
    End Function

    Private Function GetDetailedRateInformation_DataSource(ByVal nRateCompanyID As Integer, ByVal sRateClass As String, ByVal dtStartDate As DateTime, ByVal dtEndDate As DateTime, ByVal eLanguage As Language) As DetailedRateDefinitionCollection
        Dim objDetailedRateDefColl As DetailedRateDefinitionCollection

        Select Case m_dataSourceType

            Case DataSourceType.Database
                Dim sbSQL As New StringBuilder
                Dim arParams(4) As SqlParameter
                ' This uses the SIMPLE stored procedure to grab the basic information, even though
                ' this function as a whole gets DETAILED information
                sbSQL.Append("rateengine.SelectSimpleRateInformationA")
                arParams(0) = New SqlParameter("@RateCompanyID", nRateCompanyID)
                arParams(0).Direction = ParameterDirection.Input
                arParams(1) = New SqlParameter("@RateClass", sRateClass)
                arParams(1).Direction = ParameterDirection.Input
                arParams(2) = New SqlParameter("@StartDate", dtStartDate)
                arParams(2).Direction = ParameterDirection.Input
                arParams(3) = New SqlParameter("@EndDate", dtEndDate)
                arParams(3).Direction = ParameterDirection.Input
                arParams(4) = New SqlParameter("@Language", CType(eLanguage, Integer))
                arParams(4).Direction = ParameterDirection.Input

                objDetailedRateDefColl = MainQueryDetailedRateInformation(arParams, sbSQL)

            Case DataSourceType.XmlFile
                Throw New ArgumentException(ksUnimplementedDataSourceXmlFile)

            Case Else
                Throw New ArgumentException(ksUnknownDataSource)

        End Select

        Return (objDetailedRateDefColl)
    End Function

#End Region

#Region "GetDetailedRateInformation Overload B - (LocalCache implemented)"

    Public Function GetDetailedRateInformation(ByVal nRateCompanyID As Integer, ByVal sRateClass As String, ByVal eLanguage As Language) As DetailedRateDefinitionCollection
        Return (GetDetailedRateInformation_Cache(nRateCompanyID, sRateClass, eLanguage))
    End Function

    Private Function GetDetailedRateInformation_Cache(ByVal nRateCompanyID As Integer, ByVal sRateClass As String, ByVal eLanguage As Language) As DetailedRateDefinitionCollection
        Dim rateDefs As DetailedRateDefinitionCollection
        Const cacheKeyLocalTemplate As String = "GDRIB-{0}-{1}-{2}"

        Select Case m_cachingType
            Case Enums.CachingType.NoCache
                rateDefs = GetDetailedRateInformation_DataSource(nRateCompanyID, sRateClass, eLanguage)

            Case Enums.CachingType.LocalCache
                Dim cacheKeySB As New StringBuilder()
                Dim cacheKey As String

                cacheKey = cacheKeySB.AppendFormat(cacheKeyLocalTemplate, nRateCompanyID, sRateClass, CInt(eLanguage)).ToString()

                rateDefs = CacheLayer.Get(Of DetailedRateDefinitionCollection)(cacheKey)

                If (rateDefs Is Nothing) Then
                    rateDefs = GetDetailedRateInformation_DataSource(nRateCompanyID, sRateClass, eLanguage)
                    If (Not rateDefs Is Nothing) Then
                        CacheLayer.Add(rateDefs, cacheKey, m_cacheTimeoutInMinutes)
                    Else
                        ' did not get object; return an empty collection
                        rateDefs = New DetailedRateDefinitionCollection()
                    End If
                End If

            Case Enums.CachingType.DistributedCache
                Throw New ArgumentException(ksUnimplementedCacheTypeDistributedCache)

            Case Enums.CachingType.FlatLocalCache
                Throw New ArgumentException(ksUnimplementedCacheTypeFlatLocalCache)

            Case Enums.CachingType.FlatDistributedCache
                Throw New ArgumentException(ksUnimplementedCacheTypeFlatDistributedCache)

            Case Else
                Throw New ArgumentException(ksUnknownCacheType)

        End Select

        Return (rateDefs)
    End Function

    Private Function GetDetailedRateInformation_DataSource(ByVal nRateCompanyID As Integer, ByVal sRateClass As String, ByVal eLanguage As Language) As DetailedRateDefinitionCollection
        Dim objDetailedRateDefColl As DetailedRateDefinitionCollection

        Select Case m_dataSourceType

            Case DataSourceType.Database
                Dim sbSQL As New StringBuilder
                Dim arParams(2) As SqlParameter
                ' This uses the SIMPLE stored procedure to grab the basic information, even though
                ' this function as a whole gets DETAILED information
                sbSQL.Append("rateengine.SelectSimpleRateInformationB")
                arParams(0) = New SqlParameter("@RateCompanyID", nRateCompanyID)
                arParams(0).Direction = ParameterDirection.Input
                arParams(1) = New SqlParameter("@RateClass", sRateClass)
                arParams(1).Direction = ParameterDirection.Input
                arParams(2) = New SqlParameter("@Language", CType(eLanguage, Integer))
                arParams(2).Direction = ParameterDirection.Input

                objDetailedRateDefColl = MainQueryDetailedRateInformation(arParams, sbSQL)

            Case DataSourceType.XmlFile
                Throw New ArgumentException(ksUnimplementedDataSourceXmlFile)

            Case Else
                Throw New ArgumentException(ksUnknownDataSource)

        End Select

        Return (objDetailedRateDefColl)
    End Function

#End Region

#Region "GetDetailedRateInformation Overload C - (LocalCache implemented)"

    Public Function GetDetailedRateInformation(ByVal nRateDefinitionID As Integer, ByVal eLanguage As Language) As DetailedRateDefinition
        Return (GetDetailedRateInformation_Cache(nRateDefinitionID, eLanguage))
    End Function

    Private Function GetDetailedRateInformation_Cache(ByVal nRateDefinitionID As Integer, ByVal eLanguage As Language) As DetailedRateDefinition
        Dim rateDef As DetailedRateDefinition
        Const cacheKeyLocalTemplate As String = "GDRIC-{0}-{1}"

        Select Case m_cachingType
            Case Enums.CachingType.NoCache
                rateDef = GetDetailedRateInformation_DataSource(nRateDefinitionID, eLanguage)

            Case Enums.CachingType.LocalCache
                Dim cacheKeySB As New StringBuilder()
                Dim cacheKey As String

                cacheKey = cacheKeySB.AppendFormat(cacheKeyLocalTemplate, nRateDefinitionID, CInt(eLanguage)).ToString()

                rateDef = CacheLayer.Get(Of DetailedRateDefinition)(cacheKey)

                If (rateDef Is Nothing) Then
                    rateDef = GetDetailedRateInformation_DataSource(nRateDefinitionID, eLanguage)
                    If (Not rateDef Is Nothing) Then
                        CacheLayer.Add(rateDef, cacheKey, m_cacheTimeoutInMinutes)
                    Else
                        ' did not get object; return nothing
                        rateDef = Nothing
                    End If
                End If

            Case Enums.CachingType.DistributedCache
                Throw New ArgumentException(ksUnimplementedCacheTypeDistributedCache)

            Case Enums.CachingType.FlatLocalCache
                Throw New ArgumentException(ksUnimplementedCacheTypeFlatLocalCache)

            Case Enums.CachingType.FlatDistributedCache
                Throw New ArgumentException(ksUnimplementedCacheTypeFlatDistributedCache)

            Case Else
                Throw New ArgumentException(ksUnknownCacheType)

        End Select

        Return (rateDef)
    End Function

    Private Function GetDetailedRateInformation_DataSource(ByVal nRateDefinitionID As Integer, ByVal eLanguage As Language) As DetailedRateDefinition
        Dim objDetailedRateDefColl As DetailedRateDefinitionCollection
        Dim objDetailedRateDef As DetailedRateDefinition

        Select Case m_dataSourceType

            Case DataSourceType.Database
                Dim sbSQL As New StringBuilder
                Dim arParams(1) As SqlParameter
                ' This uses the SIMPLE stored procedure to grab the basic information, even though
                ' this function as a whole gets DETAILED information
                sbSQL.Append("rateengine.SelectSimpleRateInformationC")
                arParams(0) = New SqlParameter("@RateDefinitionID", nRateDefinitionID)
                arParams(0).Direction = ParameterDirection.Input
                arParams(1) = New SqlParameter("@Language", CType(eLanguage, Integer))
                arParams(1).Direction = ParameterDirection.Input

                objDetailedRateDefColl = MainQueryDetailedRateInformation(arParams, sbSQL)

                If (objDetailedRateDefColl.Count > 0) Then
                    objDetailedRateDef = objDetailedRateDefColl.Item(0)
                Else
                    objDetailedRateDef = Nothing
                End If

            Case DataSourceType.XmlFile
                Throw New ArgumentException(ksUnimplementedDataSourceXmlFile)

            Case Else
                Throw New ArgumentException(ksUnknownDataSource)

        End Select

        Return (objDetailedRateDef)
    End Function

#End Region

#Region "GetDetailedRateInformation Overload D - (LocalCache implemented)"

    Public Function GetDetailedRateInformation(ByVal nRateCompanyID As Integer, ByVal sRateClass As String, ByVal dtStartDate As DateTime, ByVal dtEndDate As DateTime, ByVal resetStartDateByYears As Integer, ByVal eLanguage As Language) As DetailedRateDefinitionCollection
        Return (GetDetailedRateInformation_Cache(nRateCompanyID, sRateClass, dtStartDate, dtEndDate, resetStartDateByYears, eLanguage))
    End Function

    Private Function GetDetailedRateInformation_Cache(ByVal nRateCompanyID As Integer, ByVal sRateClass As String, ByVal dtStartDate As DateTime, ByVal dtEndDate As DateTime, ByVal resetStartDateByYears As Integer, ByVal eLanguage As Language) As DetailedRateDefinitionCollection
        Dim rateDefs As DetailedRateDefinitionCollection
        Const cacheKeyLocalTemplate As String = "GDRID-{0}-{1}-{2}-{3}-{4}-{5}"

        Select Case m_cachingType
            Case Enums.CachingType.NoCache
                rateDefs = GetDetailedRateInformation_DataSource(nRateCompanyID, sRateClass, dtStartDate, dtEndDate, resetStartDateByYears, eLanguage)

            Case Enums.CachingType.LocalCache
                Dim cacheKeySB As New StringBuilder()
                Dim cacheKey As String

                cacheKey = cacheKeySB.AppendFormat(cacheKeyLocalTemplate, nRateCompanyID, sRateClass, DateTimeToEpoch(dtStartDate), DateTimeToEpoch(dtEndDate), resetStartDateByYears, CInt(eLanguage)).ToString()

                rateDefs = CacheLayer.Get(Of DetailedRateDefinitionCollection)(cacheKey)

                If (rateDefs Is Nothing) Then
                    rateDefs = GetDetailedRateInformation_DataSource(nRateCompanyID, sRateClass, dtStartDate, dtEndDate, resetStartDateByYears, eLanguage)
                    If (Not rateDefs Is Nothing) Then
                        CacheLayer.Add(rateDefs, cacheKey, m_cacheTimeoutInMinutes)
                    Else
                        ' did not get object; return an empty collection
                        rateDefs = New DetailedRateDefinitionCollection()
                    End If
                End If

            Case Enums.CachingType.DistributedCache
                Throw New ArgumentException(ksUnimplementedCacheTypeDistributedCache)

            Case Enums.CachingType.FlatLocalCache
                Throw New ArgumentException(ksUnimplementedCacheTypeFlatLocalCache)

            Case Enums.CachingType.FlatDistributedCache
                Throw New ArgumentException(ksUnimplementedCacheTypeFlatDistributedCache)

            Case Else
                Throw New ArgumentException(ksUnknownCacheType)

        End Select

        Return (rateDefs)
    End Function

    Private Function GetDetailedRateInformation_DataSource(ByVal nRateCompanyID As Integer, ByVal sRateClass As String, ByVal dtStartDate As DateTime, ByVal dtEndDate As DateTime, ByVal resetStartDateByYears As Integer, ByVal eLanguage As Language) As DetailedRateDefinitionCollection
        Dim objDetailedRateDefColl As DetailedRateDefinitionCollection

        Select Case m_dataSourceType

            Case DataSourceType.Database
                Dim sbSQL As New StringBuilder
                Dim arParams(4) As SqlParameter
                ' This uses the SIMPLE stored procedure to grab the basic information, even though
                ' this function as a whole gets DETAILED information; note that the parameter resetStartDateByYears is not used in the database query; it is used below instead
                sbSQL.Append("rateengine.SelectSimpleRateInformationA")
                arParams(0) = New SqlParameter("@RateCompanyID", nRateCompanyID)
                arParams(0).Direction = ParameterDirection.Input
                arParams(1) = New SqlParameter("@RateClass", sRateClass)
                arParams(1).Direction = ParameterDirection.Input
                arParams(2) = New SqlParameter("@StartDate", dtStartDate)
                arParams(2).Direction = ParameterDirection.Input
                arParams(3) = New SqlParameter("@EndDate", dtEndDate)
                arParams(3).Direction = ParameterDirection.Input
                arParams(4) = New SqlParameter("@Language", CType(eLanguage, Integer))
                arParams(4).Direction = ParameterDirection.Input

                objDetailedRateDefColl = MainQueryDetailedRateInformation(arParams, sbSQL)

                ' artificially adjust the startDate of the rate definitions; usually the caller will provide negative number like -3 for resetStartDateByYears
                For Each def As DetailedRateDefinition In objDetailedRateDefColl
                    def.StartDate = def.StartDate.AddYears(resetStartDateByYears)
                    def.StartDateModified = True
                    def.StartDateSelector = dtStartDate
                    def.EndDateSelector = dtEndDate
                Next

            Case DataSourceType.XmlFile
                Throw New ArgumentException(ksUnimplementedDataSourceXmlFile)

            Case Else
                Throw New ArgumentException(ksUnknownDataSource)

        End Select

        Return (objDetailedRateDefColl)
    End Function

#End Region

    ' Private Core Function for all GetDetailedRateInformation functions above
    Private Function MainQueryDetailedRateInformation(ByVal arParams() As SqlParameter, ByVal sbSQL As StringBuilder) As DetailedRateDefinitionCollection
        Dim objDetailedRateDefColl As New DetailedRateDefinitionCollection
        Dim objDetailedRateDef As DetailedRateDefinition
        Dim dr As SqlDataReader= Nothing
        Dim sConn As String

        sConn = RateEngineConnectionString()

        Try
            dr = SqlHelper.ExecuteReader(sConn, CommandType.StoredProcedure, sbSQL.ToString(), arParams)

            Do While dr.Read()
                objDetailedRateDef = New DetailedRateDefinition
                If (Not IsDBNull(dr("RateDefID"))) Then objDetailedRateDef.DefinitionID = CType(dr("RateDefID"), Integer)
                If (Not IsDBNull(dr("RdRateMasterID"))) Then objDetailedRateDef.MasterID = CType(dr("RdRateMasterID"), Integer)
                If (Not IsDBNull(dr("RmRateCompanyID"))) Then objDetailedRateDef.RateCompanyID = CType(dr("RmRateCompanyID"), Integer)
                If (Not IsDBNull(dr("RmClientRateID"))) Then objDetailedRateDef.RateClass = CType(dr("RmClientRateID"), String)
                If (Not IsDBNull(dr("RmDefault"))) Then objDetailedRateDef.Default = CType(dr("RmDefault"), Boolean)
                If (Not IsDBNull(dr("RmChildrenInd"))) Then objDetailedRateDef.Children = CType(dr("RmChildrenInd"), Boolean)
                If (Not IsDBNull(dr("Name"))) Then objDetailedRateDef.Name = CType(dr("Name"), String)
                If (Not IsDBNull(dr("Description"))) Then objDetailedRateDef.Description = CType(dr("Description"), String)
                If (Not IsDBNull(dr("Eligibility"))) Then objDetailedRateDef.Eligibility = CType(dr("Eligibility"), String)
                If (Not IsDBNull(dr("RdDifferenceNote"))) Then objDetailedRateDef.DifferenceNote = CType(dr("RdDifferenceNote"), String) Else objDetailedRateDef.DifferenceNote = String.Empty
                If (Not IsDBNull(dr("RdSeasonalInd"))) Then objDetailedRateDef.Seasonal = CType(dr("RdSeasonalInd"), Boolean)
                If (Not IsDBNull(dr("RdTieredInd"))) Then objDetailedRateDef.Tiered = CType(dr("RdTieredInd"), Boolean)
                If (Not IsDBNull(dr("RdTimeOfUseInd"))) Then objDetailedRateDef.TimeOfUse = CType(dr("RdTimeOfUseInd"), Boolean)
                If (Not IsDBNull(dr("RdDemandInd"))) Then objDetailedRateDef.Demand = CType(dr("RdDemandInd"), Boolean)
                If (Not IsDBNull(dr("RdServiceStepsInd"))) Then objDetailedRateDef.Stepped = CType(dr("RdServiceStepsInd"), Boolean)
                If (Not IsDBNull(dr("RdBasicInd"))) Then objDetailedRateDef.Basic = CType(dr("RdBasicInd"), Boolean)
                If (Not IsDBNull(dr("RdFuelAdjustmentInd"))) Then objDetailedRateDef.FuelAdjustment = CType(dr("RdFuelAdjustmentInd"), Boolean)
                If (Not IsDBNull(dr("RdStartDate"))) Then objDetailedRateDef.StartDate = CType(dr("RdStartDate"), DateTime)
                If (Not IsDBNull(dr("RdTsID"))) Then objDetailedRateDef.TierStructureType = CType(dr("RdTsID"), TierStructureType)
                If (Not IsDBNull(dr("RdDsID"))) Then objDetailedRateDef.DemandStructureType = CType(dr("RdDsID"), DemandStructureType)
                If (Not IsDBNull(dr("RdSpID"))) Then objDetailedRateDef.SeasonalProrateType = CType(dr("RdSpID"), SeasonalProrateType)
                If (Not IsDBNull(dr("RdSdpID"))) Then objDetailedRateDef.SeasonalDemandProrateType = CType(dr("RdSdpID"), SeasonalDemandProrateType)
                If (Not IsDBNull(dr("RmFuID"))) Then objDetailedRateDef.FuelType = CType(dr("RmFuID"), FuelType)
                If (Not IsDBNull(dr("RmSvID"))) Then objDetailedRateDef.ServiceType = CType(dr("RmSvID"), ServiceType)
                If (Not IsDBNull(dr("RmCtID"))) Then objDetailedRateDef.CustomerType = CType(dr("RmCtID"), CustomerType)
                If (Not IsDBNull(dr("RmUnID"))) Then objDetailedRateDef.UnitOfMeasure = CType(dr("RmUnID"), Integer)
                If (Not IsDBNull(dr("RmPrID"))) Then objDetailedRateDef.ProrateType = CType(dr("RmPrID"), ProrateType)
                If (Not IsDBNull(dr("RmDPrID"))) Then objDetailedRateDef.DemandProrateType = CType(dr("RmDPrID"), DemandProrateType)
                If (Not IsDBNull(dr("RmLtID"))) Then objDetailedRateDef.LoadType = CType(dr("RmLtID"), LoadType)
                If (Not IsDBNull(dr("RmBdID"))) Then objDetailedRateDef.BilledDemandType = CType(dr("RmBdID"), BilledDemandType)
                If (Not IsDBNull(dr("PoID"))) Then objDetailedRateDef.Pollution.PollutionID = CType(dr("PoID"), Integer)
                If (Not IsDBNull(dr("PoCO2"))) Then objDetailedRateDef.Pollution.CO2 = CType(dr("PoCO2"), Double)
                If (Not IsDBNull(dr("PoSO2"))) Then objDetailedRateDef.Pollution.SO2 = CType(dr("PoSO2"), Double)
                If (Not IsDBNull(dr("PoNOx"))) Then objDetailedRateDef.Pollution.NOx = CType(dr("PoNOx"), Double)
                If (Not IsDBNull(dr("RdTOUBoundariesConsistent"))) Then objDetailedRateDef.BoundariesConsistent = CType(dr("RdTOUBoundariesConsistent"), Boolean) Else objDetailedRateDef.BoundariesConsistent = False
                If (Not IsDBNull(dr("RdRTPInd"))) Then objDetailedRateDef.RTPRate = CType(dr("RdRTPInd"), Boolean) Else objDetailedRateDef.RTPRate = False
                If (Not IsDBNull(dr("RdRTPStreamId"))) Then objDetailedRateDef.RTPStream = CType(dr("RdRTPStreamId"), Enums.RTPStream) Else objDetailedRateDef.RTPStream = RTPStream.None
                If (Not IsDBNull(dr("RmHgID"))) Then objDetailedRateDef.HolidayGroupID = CType(dr("RmHgID"), Integer) Else objDetailedRateDef.HolidayGroupID = -1
                If (Not IsDBNull(dr("RdBrID"))) Then objDetailedRateDef.BaselineRule = CType(dr("RdBrID"), BaselineRule)
                If (Not IsDBNull(dr("RdNmID"))) Then objDetailedRateDef.NetMeteringType = CType(dr("RdNmID"), NetMeteringType)
                If (Not IsDBNull(dr("RdNetMeteringInd"))) Then objDetailedRateDef.NetMetering = CType(dr("RdNetMeteringInd"), Boolean)
                If (Not IsDBNull(dr("RdTierAccumByTOUInd"))) Then objDetailedRateDef.IndependentTierAccumulationsByTOU = CType(dr("RdTierAccumByTOUInd"), Boolean) Else objDetailedRateDef.IndependentTierAccumulationsByTOU = False
                If (Not IsDBNull(dr("RdCapResInd"))) Then objDetailedRateDef.CapacityReserve = CType(dr("RdCapResInd"), Boolean) Else objDetailedRateDef.CapacityReserve = False
                If (Not IsDBNull(dr("RdCPEventUsageAdderInd"))) Then objDetailedRateDef.CPEventUsageAdder = CType(dr("RdCPEventUsageAdderInd"), Boolean) Else objDetailedRateDef.CPEventUsageAdder = False

                objDetailedRateDefColl.Add(objDetailedRateDef)
            Loop

        Catch ex As Exception
            Throw New Exception(ksDataReaderError, ex.InnerException)
        Finally
            If (Not dr Is Nothing) Then
                If (Not dr.IsClosed) Then
                    dr.Close()
                End If
            End If
        End Try

        objDetailedRateDefColl = DoSupportingCollectionIteration(objDetailedRateDefColl)

        ' There may be associated child rate classes;
        objDetailedRateDefColl = DoMasterChildIteration(objDetailedRateDefColl)

        Return (objDetailedRateDefColl)
    End Function

    Private Function DoSupportingCollectionIteration(ByVal objDetailedRateDefColl As DetailedRateDefinitionCollection) As DetailedRateDefinitionCollection
        Dim objDefinition As DetailedRateDefinition

        ' Loop through EACH definition then CREATE and FILL each 
        ' supporting CHARGE Collection, BOUNDARY Collection, and OTHER Collections
        For Each objDefinition In objDetailedRateDefColl
            objDefinition = GetAllSupportingCollections(objDefinition)
        Next

        Return (objDetailedRateDefColl)
    End Function

    Private Function GetAllSupportingCollections(ByVal objDefinition As DetailedRateDefinition) As DetailedRateDefinition
        Dim sbSQL As New StringBuilder
        Dim sConn As String
        Dim dr As SqlDataReader= Nothing
        Dim arParams(1) As SqlParameter
        Dim nResultIndex As Integer

        sConn = RateEngineConnectionString()

        sbSQL.Append("rateengine.SelectAllChargesAndBoundariesAndOther")
        arParams(0) = New SqlParameter("@RateDefinitionID", objDefinition.DefinitionID)
        arParams(0).Direction = ParameterDirection.Input

        Try
            dr = SqlHelper.ExecuteReader(sConn, CommandType.StoredProcedure, sbSQL.ToString(), arParams)
            nResultIndex = 0
            Do
                ' Keep in mind that each collection in objDefinition is initialized already
                ' in the constructor for objDefinition.  If there are any empty results here,
                ' then the collection will have zero items loaded, and this is therefore correct
                Select Case nResultIndex
                    Case CollectionType.UseCharges
                        ' UseCharge
                        objDefinition = FillUseCharges(dr, objDefinition)
                    Case CollectionType.ServiceCharges
                        ' ServiceCharge
                        objDefinition = FillServiceCharges(dr, objDefinition)
                    Case CollectionType.MinimumCharges
                        ' MinimumCharge
                        objDefinition = FillMinimumCharges(dr, objDefinition)
                    Case CollectionType.TaxCharges
                        ' TaxCharge
                        objDefinition = FillTaxCharges(dr, objDefinition)
                    Case CollectionType.TierBoundaries
                        ' TierBoundaries
                        objDefinition = FillTierBoundaries(dr, objDefinition)
                    Case CollectionType.TOUBoundaries
                        ' TOUBoundaries
                        objDefinition = FillTOUBoundaries(dr, objDefinition)
                    Case CollectionType.SeasonBoundaries
                        ' SeasonBoundaries
                        objDefinition = FillSeasonBoundaries(dr, objDefinition)
                    Case CollectionType.StepBoundaries
                        ' StepBoundaries
                        objDefinition = FillStepBoundaries(dr, objDefinition)
                    Case CollectionType.Differences
                        ' Differences
                        objDefinition = FillDifferences(dr, objDefinition)
                    Case Else
                        Throw New Exception(ksBatchDataReaderMismatch)
                End Select

                nResultIndex = nResultIndex + 1
            Loop While dr.NextResult()

        Catch ex As Exception
            Throw New Exception(ksBatchDataReaderError, ex.InnerException)
        Finally
            If (Not dr Is Nothing) Then
                If (Not dr.IsClosed) Then
                    dr.Close()
                End If
            End If
        End Try

        Return (objDefinition)
    End Function

    Private Function DoMasterChildIteration(ByVal objDetailedRateDefColl As DetailedRateDefinitionCollection) As DetailedRateDefinitionCollection
        Dim objDefinition As DetailedRateDefinition
        Dim objMasterChildColl As MasterChildCollection

        If (objDetailedRateDefColl.Count > 0) Then

            If (Not objDetailedRateDefColl.ContainsDifferentRateMasters) Then
                ' Does the Master Rate Class have child Rate Classes
                If (objDetailedRateDefColl(0).MasterHasChildren) Then
                    ' Call database *once* and set result to each definition 
                    objMasterChildColl = GetMasterChildren(objDetailedRateDefColl(0).MasterID)

                    For Each objDefinition In objDetailedRateDefColl
                        objDefinition.MasterChildren = objMasterChildColl
                    Next

                End If
            Else
                For Each objDefinition In objDetailedRateDefColl
                    If (objDefinition.MasterHasChildren) Then
                        objDefinition.MasterChildren = GetMasterChildren(objDefinition.MasterID)
                    End If
                Next
            End If

        End If

        Return (objDetailedRateDefColl)
    End Function

    Private Function DoMasterChildIteration(ByVal objSimpleRateDefColl As SimpleRateDefinitionCollection) As SimpleRateDefinitionCollection
        Dim objDefinition As SimpleRateDefinition
        Dim objMasterChildColl As MasterChildCollection

        If (objSimpleRateDefColl.Count > 0) Then

            If (Not objSimpleRateDefColl.ContainsDifferentRateMasters) Then
                ' Does the Master Rate Class have child Rate Classes
                If (objSimpleRateDefColl(0).MasterHasChildren) Then
                    ' Call database *once* and set result to each definition 
                    objMasterChildColl = GetMasterChildren(objSimpleRateDefColl(0).MasterID)

                    For Each objDefinition In objSimpleRateDefColl
                        objDefinition.MasterChildren = objMasterChildColl
                    Next

                End If
            Else
                For Each objDefinition In objSimpleRateDefColl
                    If (objDefinition.MasterHasChildren) Then
                        objDefinition.MasterChildren = GetMasterChildren(objDefinition.MasterID)
                    End If
                Next
            End If

        End If

        Return (objSimpleRateDefColl)
    End Function

    Private Function GetMasterChildren(ByVal nRateMasterID As Integer) As MasterChildCollection
        Const sSQL As String = "rateengine.SelectMasterChildInformation"
        Const sParamA As String = "@RateMasterID"
        Dim sConn As String
        Dim dr As SqlDataReader= Nothing
        Dim arParams(1) As SqlParameter
        Dim objMasterChildColl As MasterChildCollection
        Dim objMasterChild As MasterChild
        Dim bFound As Boolean = False
        Dim nRateMasterIDchild As Integer
        Dim sRateClass As String
        Dim eLoadType As LoadType

        sConn = RateEngineConnectionString()

        arParams(0) = New SqlParameter(sParamA, nRateMasterID)
        arParams(0).Direction = ParameterDirection.Input

        objMasterChildColl = New MasterChildCollection()

        Try
            dr = SqlHelper.ExecuteReader(sConn, CommandType.StoredProcedure, sSQL, arParams)
            Do While dr.Read()
                bFound = True

                If (Not IsDBNull(dr("RateMasterIDchild"))) Then nRateMasterIDchild = CInt(dr("RateMasterIDchild"))
                If (Not IsDBNull(dr("RmClientRateID"))) Then sRateClass = CStr(dr("RmClientRateID")) Else sRateClass = String.Empty
                If (Not IsDBNull(dr("RmLtID"))) Then eLoadType = CType(dr("RmLtID"), LoadType) Else eLoadType = LoadType.General

                objMasterChild = New MasterChild(nRateMasterID, nRateMasterIDchild, eLoadType, sRateClass)
                objMasterChildColl.Add(objMasterChild)
            Loop

        Catch ex As Exception
            Throw New Exception(ksDataReaderError, ex.InnerException)
        Finally
            If (Not dr Is Nothing) Then
                If (Not dr.IsClosed) Then
                    dr.Close()
                End If
            End If
        End Try

        Return (objMasterChildColl)
    End Function

    Private Function FillUseCharges(ByVal dr As SqlDataReader, ByVal objDefinition As DetailedRateDefinition) As DetailedRateDefinition
        Dim objRef As UseChargeCollection
        Dim objUseCharge As UseCharge
        Dim w As BaseOrTier
        Dim tu As TimeOfUse
        Dim se As Season
        Dim ct As CostType
        Dim pt As PartType
        Dim dblVal As Double
        'Dim sDesc As String
        Dim g As Integer
        Dim rtpg As Integer
        Dim bFuelCostRecovery As Boolean
        Dim bRealTimePricing As Boolean

        ' pointer to UseCharge Collection
        objRef = objDefinition.UseCharges

        Do While dr.Read()
            If (Not IsDBNull(dr("TierID"))) Then w = CType(dr("TierID"), BaseOrTier)
            If (Not IsDBNull(dr("TouID"))) Then tu = CType(dr("TouID"), TimeOfUse)
            If (Not IsDBNull(dr("SeasonID"))) Then se = CType(dr("SeasonID"), Season)
            If (Not IsDBNull(dr("CostTypeID"))) Then ct = CType(dr("CostTypeID"), CostType)
            If (Not IsDBNull(dr("PartTypeID"))) Then pt = CType(dr("PartTypeID"), PartType)
            If (Not IsDBNull(dr("ChargeValue"))) Then dblVal = CType(dr("ChargeValue"), Double)
            If (Not IsDBNull(dr("RgID"))) Then g = CType(dr("RgID"), Integer) Else g = 0
            If (Not IsDBNull(dr("RTPgId"))) Then rtpg = CType(dr("RTPgId"), Integer) Else rtpg = 0
            ' Description is unused at this time
            'If (Not IsDBNull(dr("Description"))) Then sDesc = CType(dr("Description"), String) Else sDesc = String.Empty

            If (g > 0) Then
                bFuelCostRecovery = True
            Else
                bFuelCostRecovery = False
            End If

            If (rtpg > 0) Then
                bRealTimePricing = True
            Else
                bRealTimePricing = False
            End If

            objUseCharge = New UseCharge(ct, w, tu, se, pt, bFuelCostRecovery, g, bRealTimePricing, rtpg, dblVal)
            objRef.Add(objUseCharge)
        Loop

        Return (objDefinition)
    End Function

    Private Function FillServiceCharges(ByVal dr As SqlDataReader, ByVal objDefinition As DetailedRateDefinition) As DetailedRateDefinition
        Dim objRef As ServiceChargeCollection
        Dim objServiceCharge As ServiceCharge
        Dim ca As ChargeCalcType
        Dim ct As CostType
        Dim st As ServiceStep
        Dim se As Season
        Dim dblVal As Double
        Dim rc As RebateClass

        ' pointer to ServiceCharge Collection
        objRef = objDefinition.ServiceCharges

        Do While dr.Read()
            If (Not IsDBNull(dr("CalcTypeID"))) Then ca = CType(dr("CalcTypeID"), ChargeCalcType)
            If (Not IsDBNull(dr("CostTypeID"))) Then ct = CType(dr("CostTypeID"), CostType)
            If (Not IsDBNull(dr("StepID"))) Then st = CType(dr("StepID"), ServiceStep)
            If (Not IsDBNull(dr("SeasonID"))) Then se = CType(dr("SeasonID"), Season)
            If (Not IsDBNull(dr("ChargeValue"))) Then dblVal = CType(dr("ChargeValue"), Double)
            'CR 20298, 20299, 20302 Oct 2011 - rebate class
            If (Not IsDBNull(dr("RebateClassID"))) Then rc = CType(dr("RebateClassID"), RebateClass)

            objServiceCharge = New ServiceCharge(ct, ca, st, se, dblVal, rc)
            objRef.Add(objServiceCharge)
        Loop

        Return (objDefinition)
    End Function

    Private Function FillMinimumCharges(ByVal dr As SqlDataReader, ByVal objDefinition As DetailedRateDefinition) As DetailedRateDefinition
        Dim objRef As MinimumChargeCollection
        Dim objMinimumCharge As MinimumCharge
        Dim ca As ChargeCalcType
        Dim ct As CostType
        Dim dblVal As Double

        ' pointer to MinimumCharge Collection
        objRef = objDefinition.MinimumCharges

        Do While dr.Read()
            If (Not IsDBNull(dr("CalcTypeID"))) Then ca = CType(dr("CalcTypeID"), ChargeCalcType)
            If (Not IsDBNull(dr("CostTypeID"))) Then ct = CType(dr("CostTypeID"), CostType)
            If (Not IsDBNull(dr("ChargeValue"))) Then dblVal = CType(dr("ChargeValue"), Double)

            objMinimumCharge = New MinimumCharge(ct, ca, dblVal)
            objRef.Add(objMinimumCharge)
        Loop

        Return (objDefinition)
    End Function

    Private Function FillTaxCharges(ByVal dr As SqlDataReader, ByVal objDefinition As DetailedRateDefinition) As DetailedRateDefinition
        Dim objRef As TaxChargeCollection
        Dim objTaxCharge As TaxCharge
        Dim ct As CostType
        Dim dblVal As Double

        ' pointer to TaxCharge Collection
        objRef = objDefinition.TaxCharges

        Do While dr.Read()
            If (Not IsDBNull(dr("CostTypeID"))) Then ct = CType(dr("CostTypeID"), CostType)
            If (Not IsDBNull(dr("ChargeValue"))) Then dblVal = CType(dr("ChargeValue"), Double)

            objTaxCharge = New TaxCharge(ct, dblVal)
            objRef.Add(objTaxCharge)
        Loop

        Return (objDefinition)
    End Function

    Private Function FillTierBoundaries(ByVal dr As SqlDataReader, ByVal objDefinition As DetailedRateDefinition) As DetailedRateDefinition
        Dim objRef As TierBoundaryCollection
        Dim objTierBoundary As TierBoundary
        Dim ti As BaseOrTier
        Dim tu As TimeOfUse
        Dim se As Season
        Dim dblVal As Double
        Dim dblSecVal As Double

        ' pointer to TierBoundary Collection
        objRef = objDefinition.TierBoundaries

        Do While dr.Read()
            If (Not IsDBNull(dr("TierID"))) Then ti = CType(dr("TierID"), BaseOrTier)
            If (Not IsDBNull(dr("TouID"))) Then tu = CType(dr("TouID"), TimeOfUse)
            If (Not IsDBNull(dr("SeasonID"))) Then se = CType(dr("SeasonID"), Season)
            If (Not IsDBNull(dr("BoundaryValue"))) Then dblVal = CType(dr("BoundaryValue"), Double)
            If (Not IsDBNull(dr("SecondaryBoundaryValue"))) Then dblSecVal = CType(dr("SecondaryBoundaryValue"), Double)

            objTierBoundary = New TierBoundary(ti, tu, se, dblVal, dblSecVal)
            objRef.Add(objTierBoundary)
        Loop

        Return (objDefinition)
    End Function

    Private Function FillTOUBoundaries(ByVal dr As SqlDataReader, ByVal objDefinition As DetailedRateDefinition) As DetailedRateDefinition
        Dim objRef As TOUBoundaryCollection
        Dim objTOUBoundary As TOUBoundary
        Dim tu As TimeOfUse
        Dim se As Season
        Dim dt As DayType
        Dim sStartTime As String= Nothing
        Dim sEndTime As String= Nothing

        ' pointer to TOUBoundary Collection
        objRef = objDefinition.TOUBoundaries

        Do While dr.Read()
            If (Not IsDBNull(dr("TouID"))) Then tu = CType(dr("TouID"), TimeOfUse)
            If (Not IsDBNull(dr("SeasonID"))) Then se = CType(dr("SeasonID"), Season)
            If (Not IsDBNull(dr("DayTypeID"))) Then dt = CType(dr("DayTypeID"), DayType)
            If (Not IsDBNull(dr("StartTime"))) Then sStartTime = CType(dr("StartTime"), String)
            If (Not IsDBNull(dr("EndTime"))) Then sEndTime = CType(dr("EndTime"), String)

            objTOUBoundary = New TOUBoundary(tu, se, dt, sStartTime, sEndTime)
            objRef.Add(objTOUBoundary)
        Loop

        Return (objDefinition)
    End Function

    Private Function FillSeasonBoundaries(ByVal dr As SqlDataReader, ByVal objDefinition As DetailedRateDefinition) As DetailedRateDefinition
        Dim objRef As SeasonBoundaryCollection
        Dim objSeasonBoundary As SeasonBoundary
        Dim se As Season
        Dim sStartDay As String= Nothing
        Dim sEndDay As String= Nothing

        ' pointer to SeasonBoundary Collection
        objRef = objDefinition.SeasonBoundaries

        Do While dr.Read()
            If (Not IsDBNull(dr("SeasonID"))) Then se = CType(dr("SeasonID"), Season)
            If (Not IsDBNull(dr("StartDay"))) Then sStartDay = CType(dr("StartDay"), String)
            If (Not IsDBNull(dr("EndDay"))) Then sEndDay = CType(dr("EndDay"), String)

            objSeasonBoundary = New SeasonBoundary(se, sStartDay, sEndDay)
            objRef.Add(objSeasonBoundary)
        Loop

        Return (objDefinition)
    End Function

    Private Function FillStepBoundaries(ByVal dr As SqlDataReader, ByVal objDefinition As DetailedRateDefinition) As DetailedRateDefinition
        Dim objRef As StepBoundaryCollection
        Dim objStepBoundary As StepBoundary
        Dim st As ServiceStep
        Dim se As Season
        Dim dblVal As Double

        ' pointer to StepBoundary Collection
        objRef = objDefinition.StepBoundaries

        Do While dr.Read()
            If (Not IsDBNull(dr("StepID"))) Then st = CType(dr("StepID"), ServiceStep)
            If (Not IsDBNull(dr("SeasonID"))) Then se = CType(dr("SeasonID"), Season)
            If (Not IsDBNull(dr("BoundaryValue"))) Then dblVal = CType(dr("BoundaryValue"), Double)

            objStepBoundary = New StepBoundary(st, se, dblVal)
            objRef.Add(objStepBoundary)
        Loop

        Return (objDefinition)
    End Function

    Private Function FillDifferences(ByVal dr As SqlDataReader, ByVal objDefinition As DetailedRateDefinition) As DetailedRateDefinition
        Dim objRef As DifferenceCollection
        Dim objDifference As Difference
        Dim df As DifferenceType
        Dim dc As DifferenceTypeCategory
        Dim sName As String= Nothing
        Dim sCategoryName As String= Nothing

        ' pointer to Difference Collection
        objRef = objDefinition.Differences

        Do While dr.Read()
            If (Not IsDBNull(dr("DfID"))) Then df = CType(dr("DfID"), DifferenceType)
            If (Not IsDBNull(dr("DfName"))) Then sName = CType(dr("DfName"), String)
            If (Not IsDBNull(dr("DcID"))) Then dc = CType(dr("DcID"), DifferenceTypeCategory)
            If (Not IsDBNull(dr("DcName"))) Then sCategoryName = CType(dr("DcName"), String)

            objDifference = New Difference(df, sName, dc, sCategoryName)
            objRef.Add(objDifference)
        Loop

        Return (objDefinition)
    End Function

#End Region

#Region "Data Controller for CalculateCost Helper Functions (LocalCache implemented)"

#Region "ConvertRawUsage - (LocalCache implemented)"

    ''' <summary>
    ''' Helper to map named usage to properties of a usage.
    ''' </summary>
    ''' <param name="objUsage"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function ConvertRawUsage(ByVal objUsage As Usage) As Usage
        Return (ConvertRawUsage_Cache(objUsage))
    End Function

    Private Function ConvertRawUsage_Cache(ByVal objUsage As Usage) As Usage
        Dim tempUsage As Usage
        Const cacheKeyLocalTemplate As String = "CRU-{0}"

        Select Case m_cachingType
            Case Enums.CachingType.NoCache
                objUsage = ConvertRawUsage_DataSource(objUsage)

            Case Enums.CachingType.LocalCache
                Dim cacheKeySB As New StringBuilder()
                Dim cacheKey As String

                cacheKey = cacheKeySB.AppendFormat(cacheKeyLocalTemplate, objUsage.UseTypeName).ToString()

                tempUsage = CacheLayer.Get(Of Usage)(cacheKey)

                If (tempUsage Is Nothing) Then
                    tempUsage = New Usage(objUsage.UseTypeName, 0.0)
                    tempUsage = ConvertRawUsage_DataSource(tempUsage)
                    If (Not tempUsage Is Nothing) Then
                        CacheLayer.Add(tempUsage, cacheKey, m_cacheTimeoutInMinutes)
                        ' now it is in cache, map properties from object
                        objUsage.AssignPartsAfterLookup(tempUsage.BaseOrTier, tempUsage.TimeOfUse, tempUsage.Season, tempUsage.LoadType, tempUsage.UseTypeName)
                    Else
                        ' error; thrown already by dataSource
                        tempUsage = Nothing
                    End If
                Else
                    ' in cache, map properties from object
                    objUsage.AssignPartsAfterLookup(tempUsage.BaseOrTier, tempUsage.TimeOfUse, tempUsage.Season, tempUsage.LoadType, tempUsage.UseTypeName)
                End If

            Case Enums.CachingType.DistributedCache
                Throw New ArgumentException(ksUnimplementedCacheTypeDistributedCache)

            Case Enums.CachingType.FlatLocalCache
                Throw New ArgumentException(ksUnimplementedCacheTypeFlatLocalCache)

            Case Enums.CachingType.FlatDistributedCache
                Throw New ArgumentException(ksUnimplementedCacheTypeFlatDistributedCache)

            Case Else
                Throw New ArgumentException(ksUnknownCacheType)

        End Select

        Return (objUsage)
    End Function

    Private Function ConvertRawUsage_DataSource(ByVal objUsage As Usage) As Usage
        Dim sbSQL As New StringBuilder
        Dim arParams(1) As SqlParameter
        Dim dr As SqlDataReader= Nothing
        Dim sConn As String
        Dim bFound As Boolean
        Dim TierID As BaseOrTier
        Dim TouID As TimeOfUse
        Dim SeasonID As Season
        Dim LoadTypeID As LoadType
        Dim sUseName As String= Nothing

        Select Case m_dataSourceType

            Case DataSourceType.Database
                sbSQL.Append("rateengine.GetUseTypeMapByName")
                arParams(0) = New SqlParameter("@UseName", objUsage.UseTypeName)
                arParams(0).Direction = ParameterDirection.Input

                sConn = RateEngineConnectionString()

                Try
                    dr = SqlHelper.ExecuteReader(sConn, CommandType.StoredProcedure, sbSQL.ToString(), arParams)
                    Do While dr.Read()
                        bFound = True
                        If (Not IsDBNull(dr("TierID"))) Then TierID = CType(dr("TierID"), BaseOrTier) Else bFound = False
                        If (Not IsDBNull(dr("TouID"))) Then TouID = CType(dr("TouID"), TimeOfUse) Else bFound = False
                        If (Not IsDBNull(dr("SeasonID"))) Then SeasonID = CType(dr("SeasonID"), Season) Else bFound = False
                        If (Not IsDBNull(dr("UseName"))) Then sUseName = CType(dr("UseName"), String) Else bFound = False
                        If (Not IsDBNull(dr("LoadTypeID"))) Then LoadTypeID = CType(dr("LoadTypeID"), LoadType) Else bFound = False
                        If (bFound) Then
                            objUsage.AssignPartsAfterLookup(TierID, TouID, SeasonID, LoadTypeID, sUseName)
                        End If
                    Loop

                    If (Not bFound) Then
                        Throw New Exception(ksConvertRawUsageFailed + "  (NexusType: " + sUseName + ").")
                    End If

                Catch ex As Exception
                    Throw New Exception(ksDataReaderError, ex)
                Finally
                    If (Not dr Is Nothing) Then
                        If (Not dr.IsClosed) Then
                            dr.Close()
                        End If
                    End If
                End Try

            Case DataSourceType.XmlFile
                Throw New ArgumentException(ksUnimplementedDataSourceXmlFile)

            Case Else
                Throw New ArgumentException(ksUnknownDataSource)

        End Select

        Return (objUsage)
    End Function

#End Region

#Region "GetBaseline - (LocalCache implemented)"

    ''' <summary>
    ''' PercentbaseTier1 geography based territory+code related call for baseline value for use by tier1 calc.
    ''' </summary>
    ''' <param name="nRateDefID"></param>
    ''' <param name="sCode"></param>
    ''' <param name="sTerritory"></param>
    ''' <param name="eSeason"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function GetBaseline(ByVal nRateDefID As Integer, ByVal sCode As String, ByVal sTerritory As String, ByVal eSeason As Season) As Double
        Return (GetBaseline_Cache(nRateDefID, sCode, sTerritory, eSeason))
    End Function

    Private Function GetBaseline_Cache(ByVal nRateDefID As Integer, ByVal sCode As String, ByVal sTerritory As String, ByVal eSeason As Season) As Double
        Dim dblBaseline? As Double
        Const cacheKeyLocalTemplate As String = "GB-{0}-{1}-{2}-{3}"

        Select Case m_cachingType

            Case Enums.CachingType.NoCache
                dblBaseline = GetBaseline_DataSource(nRateDefID, sCode, sTerritory, eSeason)

            Case Enums.CachingType.LocalCache
                Dim cacheKeySB As New StringBuilder()
                Dim cacheKey As String

                cacheKey = cacheKeySB.AppendFormat(cacheKeyLocalTemplate, nRateDefID, sCode, sTerritory, CInt(eSeason)).ToString()

                dblBaseline = CacheLayer.GetDouble(cacheKey)

                If (dblBaseline Is Nothing) Then
                    dblBaseline = GetBaseline_DataSource(nRateDefID, sCode, sTerritory, eSeason)
                    If (Not dblBaseline Is Nothing) Then
                        CacheLayer.Add(CDbl(dblBaseline), cacheKey, m_cacheTimeoutInMinutes)
                    Else
                        ' error; thrown already by dataSource
                        dblBaseline = Nothing
                    End If
                End If

            Case Enums.CachingType.DistributedCache
                Throw New ArgumentException(ksUnimplementedCacheTypeDistributedCache)

            Case Enums.CachingType.FlatLocalCache
                Throw New ArgumentException(ksUnimplementedCacheTypeFlatLocalCache)

            Case Enums.CachingType.FlatDistributedCache
                Throw New ArgumentException(ksUnimplementedCacheTypeFlatDistributedCache)

            Case Else
                Throw New ArgumentException(ksUnknownCacheType)

        End Select

        Return (CDbl(dblBaseline))
    End Function

    Private Function GetBaseline_DataSource(ByVal nRateDefID As Integer, ByVal sCode As String, ByVal sTerritory As String, ByVal eSeason As Season) As Double
        Dim dblBaseline As Double

        ' Override undefined seasons for territory lookups!  
        If (eSeason = Season.Undefined) Then eSeason = Season.Summer

        Select Case m_dataSourceType

            Case DataSourceType.Database
                Dim sbSQL As New StringBuilder
                Dim arParams(3) As SqlParameter
                Dim dr As SqlDataReader= Nothing
                Dim sConn As String
                Dim bFound As Boolean

                sbSQL.Append("rateengine.SelectBaselineTerritory")
                arParams(0) = New SqlParameter("@RateDefID", nRateDefID)
                arParams(0).Direction = ParameterDirection.Input
                arParams(1) = New SqlParameter("@Code", sCode)
                arParams(1).Direction = ParameterDirection.Input
                arParams(2) = New SqlParameter("@Territory", sTerritory)
                arParams(2).Direction = ParameterDirection.Input
                arParams(3) = New SqlParameter("@SeasonID", CType(eSeason, Integer))
                arParams(3).Direction = ParameterDirection.Input

                sConn = RateEngineConnectionString()

                Try
                    dr = SqlHelper.ExecuteReader(sConn, CommandType.StoredProcedure, sbSQL.ToString(), arParams)
                    Do While dr.Read()
                        bFound = True
                        If (Not IsDBNull(dr("TlValue"))) Then dblBaseline = CType(dr("TlValue"), Double) Else bFound = False
                    Loop

                    If (Not bFound) Then
                        Throw New Exception(ksBaselineLookupFailed & ";Code=" & sCode & ";Territory=" & sTerritory & ";")
                    End If

                Catch ex As Exception
                    Throw New Exception(ksBaselineLookupFailed & ";Code=" & sCode & ";Territory=" & sTerritory & ";", ex.InnerException)
                Finally
                    If (Not dr Is Nothing) Then
                        If (Not dr.IsClosed) Then
                            dr.Close()
                        End If
                    End If
                End Try

            Case DataSourceType.XmlFile
                Throw New ArgumentException(ksUnimplementedDataSourceXmlFile)

            Case Else
                Throw New ArgumentException(ksUnknownDataSource)

        End Select

        Return (dblBaseline)
    End Function

#End Region

#End Region

#Region "Data Controller for Additional Rate Engine Helper Functions - (LocalCache implemented)"

#Region "GetFuelCostRecoveryList - (LocalCache implemented)"

    ''' <summary>
    ''' FCR helper function.
    ''' </summary>
    ''' <param name="nRgID"></param>
    ''' <param name="dtStartDate"></param>
    ''' <param name="dtEndDate"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function GetFuelCostRecoveryList(ByVal nRgID As Integer, ByVal dtStartDate As DateTime, ByVal dtEndDate As DateTime) As FCRCollection
        Return (GetFuelCostRecoveryList_Cache(nRgID, dtStartDate, dtEndDate))
    End Function

    Private Function GetFuelCostRecoveryList_Cache(ByVal nRgID As Integer, ByVal dtStartDate As DateTime, ByVal dtEndDate As DateTime) As FCRCollection
        Dim fcrList As FCRCollection
        Const cacheKeyLocalTemplate As String = "GFCRL-{0}-{1}-{2}"

        Select Case m_cachingType
            Case Enums.CachingType.NoCache
                fcrList = GetFuelCostRecoveryList_DataSource(nRgID, dtStartDate, dtEndDate)

            Case Enums.CachingType.LocalCache
                Dim cacheKeySB As New StringBuilder()
                Dim cacheKey As String

                cacheKey = cacheKeySB.AppendFormat(cacheKeyLocalTemplate, nRgID, DateTimeToEpoch(dtStartDate), DateTimeToEpoch(dtEndDate)).ToString()

                fcrList = CacheLayer.Get(Of FCRCollection)(cacheKey)

                If (fcrList Is Nothing) Then
                    fcrList = GetFuelCostRecoveryList_DataSource(nRgID, dtStartDate, dtEndDate)
                    If (Not fcrList Is Nothing) Then
                        If (fcrList.Count > 0) Then
                            CacheLayer.Add(fcrList, cacheKey, m_cacheTimeoutInMinutes)
                        Else
                            'the empty object will be returned
                        End If
                    Else
                        'original function returns empty object
                        fcrList = New FCRCollection()
                    End If
                End If

            Case Enums.CachingType.DistributedCache
                Throw New ArgumentException(ksUnimplementedCacheTypeDistributedCache)

            Case Enums.CachingType.FlatLocalCache
                Throw New ArgumentException(ksUnimplementedCacheTypeFlatLocalCache)

            Case Enums.CachingType.FlatDistributedCache
                Throw New ArgumentException(ksUnimplementedCacheTypeFlatDistributedCache)

            Case Else
                Throw New ArgumentException(ksUnknownCacheType)

        End Select

        Return (fcrList)
    End Function

    Private Function GetFuelCostRecoveryList_DataSource(ByVal nRgID As Integer, ByVal dtStartDate As DateTime, ByVal dtEndDate As DateTime) As FCRCollection
        Dim objFCRCollection As FCRCollection

        Select Case m_dataSourceType

            Case DataSourceType.Database
                Dim objFCR As FCR
                Dim sbSQL As New StringBuilder
                Dim arParams(2) As SqlParameter
                Dim dr As SqlDataReader = Nothing
                Dim sConn As String
                Dim bFound As Boolean
                Dim dblChargeValue As Double
                Dim dtFCRStartDate As DateTime
                objFCRCollection = New FCRCollection
                sbSQL.Append("rateengine.GetFuelCostRecoveryList")
                arParams(0) = New SqlParameter("@RgID", nRgID)
                arParams(0).Direction = ParameterDirection.Input
                arParams(1) = New SqlParameter("@StartDate", dtStartDate)
                arParams(1).Direction = ParameterDirection.Input
                arParams(2) = New SqlParameter("@EndDate", dtEndDate)
                arParams(2).Direction = ParameterDirection.Input

                sConn = RateEngineConnectionString()

                Try
                    dr = SqlHelper.ExecuteReader(sConn, CommandType.StoredProcedure, sbSQL.ToString(), arParams)
                    Do While dr.Read()
                        bFound = True
                        If (Not IsDBNull(dr("FrChargeValue"))) Then dblChargeValue = CType(dr("FrChargeValue"), Double) Else bFound = False
                        If (Not IsDBNull(dr("FrStartDate"))) Then dtFCRStartDate = CType(dr("FrStartDate"), DateTime) Else bFound = False
                        If (bFound = False) Then
                            Exit Do
                        End If
                        objFCR = New FCR(dtFCRStartDate, dblChargeValue)
                        objFCRCollection.Add(objFCR)
                    Loop

                    If (Not bFound) Then
                        ' Collection should simply be empty
                    End If

                Catch ex As Exception
                    Throw New Exception(ksDataReaderError, ex.InnerException)
                Finally
                    If (Not dr Is Nothing) Then
                        If (Not dr.IsClosed) Then
                            dr.Close()
                        End If
                    End If
                End Try

            Case DataSourceType.XmlFile
                Throw New ArgumentException(ksUnimplementedDataSourceXmlFile)

            Case Else
                Throw New ArgumentException(ksUnknownDataSource)

        End Select

        Return (objFCRCollection)
    End Function

#End Region

#Region "GetRateClassInfo - (LocalCache implemented)"

    ''' <summary>
    ''' Fast helper function that simply reads the name, desc, and elig from RateMaster*RateMasterText.  
    ''' This is quicker because it does not use the core rate engine GetSimpleRateInformation function.
    ''' </summary>
    ''' <param name="nRateCompanyID"></param>
    ''' <param name="sRateClass"></param>
    ''' <param name="eLanguage"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function GetRateClassInfo(ByVal nRateCompanyID As Integer, ByVal sRateClass As String, ByVal eLanguage As Language) As RateClassInfo
        Return (GetRateClassInfo_Cache(nRateCompanyID, sRateClass, eLanguage))
    End Function

    Private Function GetRateClassInfo_Cache(ByVal nRateCompanyID As Integer, ByVal sRateClass As String, ByVal eLanguage As Language) As RateClassInfo
        Dim rci As RateClassInfo
        Const cacheKeyLocalTemplate As String = "GRCIX-{0}-{1}-{2}"

        Select Case m_cachingType
            Case Enums.CachingType.NoCache
                rci = GetRateClassInfo_DataSource(nRateCompanyID, sRateClass, eLanguage)

            Case Enums.CachingType.LocalCache
                Dim cacheKeySB As New StringBuilder()
                Dim cacheKey As String

                cacheKey = cacheKeySB.AppendFormat(cacheKeyLocalTemplate, nRateCompanyID, sRateClass, CInt(eLanguage)).ToString()

                rci = CacheLayer.Get(Of RateClassInfo)(cacheKey)

                If (rci Is Nothing) Then
                    rci = GetRateClassInfo_DataSource(nRateCompanyID, sRateClass, eLanguage)
                    If (Not rci Is Nothing) Then
                        CacheLayer.Add(rci, cacheKey, m_cacheTimeoutInMinutes)
                    Else
                        'original function returns object that is initialized with empty strings
                        rci = New RateClassInfo()
                    End If
                End If

            Case Enums.CachingType.DistributedCache
                Throw New ArgumentException(ksUnimplementedCacheTypeDistributedCache)

            Case Enums.CachingType.FlatLocalCache
                Throw New ArgumentException(ksUnimplementedCacheTypeFlatLocalCache)

            Case Enums.CachingType.FlatDistributedCache
                Throw New ArgumentException(ksUnimplementedCacheTypeFlatDistributedCache)

            Case Else
                Throw New ArgumentException(ksUnknownCacheType)

        End Select

        Return (rci)
    End Function

    Private Function GetRateClassInfo_DataSource(ByVal nRateCompanyID As Integer, ByVal sRateClass As String, ByVal eLanguage As Language) As RateClassInfo
        Dim objRateClassInfo As New RateClassInfo()

        Select Case m_dataSourceType

            Case DataSourceType.Database
                Dim sbSQL As New StringBuilder
                Dim arParams(2) As SqlParameter
                Dim dr As SqlDataReader = Nothing
                Dim sConn As String
                Dim bFound As Boolean
                sbSQL.Append("rateengine.GetRateClassInfo")
                arParams(0) = New SqlParameter("@RateCompanyID", nRateCompanyID)
                arParams(0).Direction = ParameterDirection.Input
                arParams(1) = New SqlParameter("@RateClass", sRateClass)
                arParams(1).Direction = ParameterDirection.Input
                arParams(2) = New SqlParameter("@Language", eLanguage)
                arParams(2).Direction = ParameterDirection.Input

                sConn = RateEngineConnectionString()

                Try
                    ' use data reader in case we want to return description and elibibility too in future
                    dr = SqlHelper.ExecuteReader(sConn, CommandType.StoredProcedure, sbSQL.ToString(), arParams)
                    Do While dr.Read()
                        bFound = True
                        If (Not IsDBNull(dr("Name"))) Then objRateClassInfo.Name = CType(dr("Name"), String)
                        If (Not IsDBNull(dr("Description"))) Then objRateClassInfo.Description = CType(dr("Description"), String)
                        If (Not IsDBNull(dr("Eligibility"))) Then objRateClassInfo.Eligibility = CType(dr("Eligibility"), String)
                        Exit Do
                    Loop

                    If (Not bFound) Then
                        ' Don't throw exception, just return object
                    End If

                Catch ex As Exception
                    Throw ex
                Finally
                    If (Not dr Is Nothing) Then
                        If (Not dr.IsClosed) Then
                            dr.Close()
                        End If
                    End If
                End Try

            Case DataSourceType.XmlFile
                Throw New ArgumentException(ksUnimplementedDataSourceXmlFile)

            Case Else
                Throw New ArgumentException(ksUnknownDataSource)

        End Select

        Return (objRateClassInfo)
    End Function

#End Region

#Region "GetRateClassFromRateMasterID - (LocalCache implemented)"

    ''' <summary>
    ''' Fast Helper function to get rate class string.
    ''' </summary>
    ''' <param name="nRateMasterID"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function GetRateClassFromRateMasterID(ByVal nRateMasterID As Integer) As String
        Return (GetRateClassFromRateMasterID_Cache(nRateMasterID))
    End Function

    Private Function GetRateClassFromRateMasterID_Cache(ByVal nRateMasterID As Integer) As String
        Dim clientRateID As String
        Const cacheKeyLocalTemplate As String = "GRCLFRMID-{0}"

        Select Case m_cachingType
            Case Enums.CachingType.NoCache
                clientRateID = GetRateClassFromRateMasterID_DataSource(nRateMasterID)

            Case Enums.CachingType.LocalCache
                Dim cacheKeySB As New StringBuilder()
                Dim cacheKey As String

                cacheKey = cacheKeySB.AppendFormat(cacheKeyLocalTemplate, nRateMasterID).ToString()

                clientRateID = CacheLayer.Get(Of String)(cacheKey)

                If (clientRateID Is Nothing) Then
                    clientRateID = GetRateClassFromRateMasterID_DataSource(nRateMasterID)
                    If (Not String.IsNullOrEmpty(clientRateID)) Then
                        CacheLayer.Add(clientRateID, cacheKey, m_cacheTimeoutInMinutes)
                    Else
                        ' did not get object; return empty string
                        clientRateID = String.Empty
                    End If
                End If

            Case Enums.CachingType.DistributedCache
                Throw New ArgumentException(ksUnimplementedCacheTypeDistributedCache)

            Case Enums.CachingType.FlatLocalCache
                Throw New ArgumentException(ksUnimplementedCacheTypeFlatLocalCache)

            Case Enums.CachingType.FlatDistributedCache
                Throw New ArgumentException(ksUnimplementedCacheTypeFlatDistributedCache)

            Case Else
                Throw New ArgumentException(ksUnknownCacheType)

        End Select

        Return (clientRateID)
    End Function

    Private Function GetRateClassFromRateMasterID_DataSource(ByVal nRateMasterID As Integer) As String
        Dim clientRateID As String = Nothing

        Select Case m_dataSourceType

            Case DataSourceType.Database
                Dim sbSQL As New StringBuilder
                Dim arParams(1) As SqlParameter
                Dim dr As SqlDataReader = Nothing
                Dim sConn As String
                Dim bFound As Boolean
                sbSQL.Append("rateengine.GetRateClassFromRateMasterID")
                arParams(0) = New SqlParameter("@RateMasterID", nRateMasterID)
                arParams(0).Direction = ParameterDirection.Input

                sConn = RateEngineConnectionString()

                Try
                    ' use data reader in case we want to return description and elibibility too in future
                    dr = SqlHelper.ExecuteReader(sConn, CommandType.StoredProcedure, sbSQL.ToString(), arParams)

                    Do While dr.Read()
                        If (Not IsDBNull(dr("RmClientRateID"))) Then clientRateID = CType(dr("RmClientRateID"), String)
                        bFound = True
                    Loop

                    If (Not bFound) Then
                        ' Don't throw exception in this case, simply return empty string
                        clientRateID = String.Empty
                    End If

                Catch ex As Exception
                    Throw ex
                Finally
                    If (Not dr Is Nothing) Then
                        If (Not dr.IsClosed) Then
                            dr.Close()
                        End If
                    End If
                End Try

            Case DataSourceType.XmlFile
                Throw New ArgumentException(ksUnimplementedDataSourceXmlFile)

            Case Else
                Throw New ArgumentException(ksUnknownDataSource)

        End Select

        Return (clientRateID)
    End Function

#End Region

#Region "GetOldestStartDate - (LocalCache implemented)"

    ''' <summary>
    ''' Fast Helper function to get oldest effective data of a rate class.
    ''' </summary>
    ''' <param name="nRateMasterID"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function GetOldestStartDate(ByVal nRateMasterID As Integer) As DateTime
        Return (GetOldestStartDate_Cache(nRateMasterID))
    End Function

    Private Function GetOldestStartDate_Cache(ByVal nRateMasterID As Integer) As DateTime
        Dim dtStartDate? As DateTime
        Const cacheKeyLocalTemplate As String = "GOSD-{0}"

        Select Case m_cachingType

            Case Enums.CachingType.NoCache
                dtStartDate = GetOldestStartDate_DataSource(nRateMasterID)

            Case Enums.CachingType.LocalCache
                Dim cacheKeySB As New StringBuilder()
                Dim cacheKey As String

                cacheKey = cacheKeySB.AppendFormat(cacheKeyLocalTemplate, nRateMasterID).ToString()

                dtStartDate = CacheLayer.GetDateTime(cacheKey)

                If (dtStartDate Is Nothing) Then
                    dtStartDate = GetOldestStartDate_DataSource(nRateMasterID)
                    If (Not dtStartDate Is Nothing AndAlso dtStartDate <> DateTime.MinValue) Then
                        CacheLayer.Add(dtStartDate, cacheKey, m_cacheTimeoutInMinutes)
                    Else
                        ' did not get object, or was got minValue; return MinValue per legacy
                        dtStartDate = DateTime.MinValue
                    End If
                End If

            Case Enums.CachingType.DistributedCache
                Throw New ArgumentException(ksUnimplementedCacheTypeDistributedCache)

            Case Enums.CachingType.FlatLocalCache
                Throw New ArgumentException(ksUnimplementedCacheTypeFlatLocalCache)

            Case Enums.CachingType.FlatDistributedCache
                Throw New ArgumentException(ksUnimplementedCacheTypeFlatDistributedCache)

            Case Else
                Throw New ArgumentException(ksUnknownCacheType)

        End Select

        Return (CDate(dtStartDate))
    End Function

    Private Function GetOldestStartDate_DataSource(ByVal nRateMasterID As Integer) As DateTime
        Dim dtStartDate As DateTime

        Select Case m_dataSourceType

            Case DataSourceType.Database
                Dim sbSQL As New StringBuilder
                Dim arParams(1) As SqlParameter
                Dim dr As SqlDataReader = Nothing
                Dim sConn As String
                Dim bFound As Boolean
                sbSQL.Append("rateengine.GetOldestStartDate")
                arParams(0) = New SqlParameter("@RateMasterID", nRateMasterID)
                arParams(0).Direction = ParameterDirection.Input

                sConn = RateEngineConnectionString()

                Try
                    dr = SqlHelper.ExecuteReader(sConn, CommandType.StoredProcedure, sbSQL.ToString(), arParams)

                    Do While dr.Read()
                        If (Not IsDBNull(dr("OldestEffectiveDate"))) Then dtStartDate = CType(dr("OldestEffectiveDate"), DateTime)
                        bFound = True
                    Loop

                    If (Not bFound) Then
                        ' Don't throw exception in this case, simply return oldest possible datetime for 'null' check later
                        dtStartDate = DateTime.MinValue
                    End If

                Catch ex As Exception
                    Throw ex
                Finally
                    If (Not dr Is Nothing) Then
                        If (Not dr.IsClosed) Then
                            dr.Close()
                        End If
                    End If
                End Try

            Case DataSourceType.XmlFile
                Throw New ArgumentException(ksUnimplementedDataSourceXmlFile)

            Case Else
                Throw New ArgumentException(ksUnknownDataSource)

        End Select

        Return (dtStartDate)
    End Function

#End Region

#Region "DoesRateClassExist - (LocalCache implemented)"

    ''' <summary>
    ''' Fast Helper function to check existance of a rate class.
    ''' </summary>
    ''' <param name="nRateCompanyID"></param>
    ''' <param name="sRateClass"></param>
    ''' <param name="eLanguage"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function DoesRateClassExist(ByVal nRateCompanyID As Integer, ByVal sRateClass As String, ByVal eLanguage As Language) As Boolean
        Return (DoesRateClassExist_Cache(nRateCompanyID, sRateClass, eLanguage))
    End Function

    Private Function DoesRateClassExist_Cache(ByVal nRateCompanyID As Integer, ByVal sRateClass As String, ByVal eLanguage As Language) As Boolean
        Dim bExists? As Boolean

        Const cacheKeyLocalTemplate As String = "DRCE-{0}-{1}-{2}"

        Select Case m_cachingType

            Case Enums.CachingType.NoCache
                bExists = DoesRateClassExist_DataSource(nRateCompanyID, sRateClass, eLanguage)

            Case Enums.CachingType.LocalCache
                Dim cacheKeySB As New StringBuilder()
                Dim cacheKey As String

                cacheKey = cacheKeySB.AppendFormat(cacheKeyLocalTemplate, nRateCompanyID, sRateClass, CInt(eLanguage)).ToString()

                bExists = CacheLayer.GetBoolean(cacheKey)

                If (bExists Is Nothing) Then
                    bExists = DoesRateClassExist_DataSource(nRateCompanyID, sRateClass, eLanguage)
                    CacheLayer.Add(CBool(bExists), cacheKey, m_cacheTimeoutInMinutes)
                End If

            Case Enums.CachingType.DistributedCache
                Throw New ArgumentException(ksUnimplementedCacheTypeDistributedCache)

            Case Enums.CachingType.FlatLocalCache
                Throw New ArgumentException(ksUnimplementedCacheTypeFlatLocalCache)

            Case Enums.CachingType.FlatDistributedCache
                Throw New ArgumentException(ksUnimplementedCacheTypeFlatDistributedCache)

            Case Else
                Throw New ArgumentException(ksUnknownCacheType)

        End Select

        Return (CBool(bExists))
    End Function

    Private Function DoesRateClassExist_DataSource(ByVal nRateCompanyID As Integer, ByVal sRateClass As String, ByVal eLanguage As Language) As Boolean
        Dim bExists As Boolean

        Select Case m_dataSourceType

            Case DataSourceType.Database
                Dim objRetval As Object
                Dim sbSQL As New StringBuilder
                Dim arParams(2) As SqlParameter
                Dim sConn As String
                sbSQL.Append("rateengine.DoesRateClassExist")
                arParams(0) = New SqlParameter("@RateCompanyID", nRateCompanyID)
                arParams(0).Direction = ParameterDirection.Input
                arParams(1) = New SqlParameter("@RateClass", sRateClass)
                arParams(1).Direction = ParameterDirection.Input
                arParams(2) = New SqlParameter("@Language", eLanguage)
                arParams(2).Direction = ParameterDirection.Input

                sConn = RateEngineConnectionString()

                Try
                    objRetval = SqlHelper.ExecuteScalar(sConn, CommandType.StoredProcedure, sbSQL.ToString(), arParams)

                    Try
                        bExists = System.Convert.ToBoolean(objRetval)
                    Catch ex As Exception
                        bExists = False
                    End Try

                Catch ex As Exception
                    ' failure in this manner would be rare
                    bExists = False
                End Try

            Case DataSourceType.XmlFile
                Throw New ArgumentException(ksUnimplementedDataSourceXmlFile)

            Case Else
                Throw New ArgumentException(ksUnknownDataSource)

        End Select

        Return (bExists)
    End Function

#End Region

#Region "GetFuelType - (LocalCache implemented)"

    ''' <summary>
    ''' Fast helper to get the fuel for a rate class.
    ''' </summary>
    ''' <param name="sRateClass"></param>
    ''' <param name="nRateCompanyID"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function GetFuelType(ByVal sRateClass As String, ByVal nRateCompanyID As Integer) As Integer
        Return (GetFuelType_Cache(sRateClass, nRateCompanyID))
    End Function

    Private Function GetFuelType_Cache(ByVal sRateClass As String, ByVal nRateCompanyID As Integer) As Integer
        Dim nFuelType? As Integer
        Const cacheKeyLocalTemplate As String = "GFT-{0}-{1}"

        Select Case m_cachingType

            Case Enums.CachingType.NoCache
                nFuelType = GetFuelType_DataSource(sRateClass, nRateCompanyID)

            Case Enums.CachingType.LocalCache
                Dim cacheKeySB As New StringBuilder()
                Dim cacheKey As String

                cacheKey = cacheKeySB.AppendFormat(cacheKeyLocalTemplate, sRateClass, nRateCompanyID).ToString()

                nFuelType = CacheLayer.GetInteger(cacheKey)

                If (nFuelType Is Nothing) Then
                    nFuelType = GetFuelType_DataSource(sRateClass, nRateCompanyID)
                    If (nFuelType <> 0) Then
                        CacheLayer.Add(CInt(nFuelType), cacheKey, m_cacheTimeoutInMinutes)
                    Else
                        nFuelType = 0
                    End If
                End If

            Case Enums.CachingType.DistributedCache
                Throw New ArgumentException(ksUnimplementedCacheTypeDistributedCache)

            Case Enums.CachingType.FlatLocalCache
                Throw New ArgumentException(ksUnimplementedCacheTypeFlatLocalCache)

            Case Enums.CachingType.FlatDistributedCache
                Throw New ArgumentException(ksUnimplementedCacheTypeFlatDistributedCache)

            Case Else
                Throw New ArgumentException(ksUnknownCacheType)

        End Select

        Return (CInt(nFuelType))
    End Function

    Private Function GetFuelType_DataSource(ByVal sRateClass As String, ByVal nRateCompanyID As Integer) As Integer
        Dim nFuelType As Integer

        Select Case m_dataSourceType

            Case DataSourceType.Database
                Dim objRetval As Object
                Dim sbSQL As New StringBuilder
                Dim arParams(1) As SqlParameter
                Dim sConn As String
                sbSQL.Append("rateengine.GetFuelTypebyRateClassRateCompanyID")
                arParams(0) = New SqlParameter("@RateCompanyID", nRateCompanyID)
                arParams(0).Direction = ParameterDirection.Input
                arParams(1) = New SqlParameter("@RateClass", sRateClass)
                arParams(1).Direction = ParameterDirection.Input

                sConn = RateEngineConnectionString()

                Try
                    objRetval = SqlHelper.ExecuteScalar(sConn, CommandType.StoredProcedure, sbSQL.ToString(), arParams)

                    Try
                        nFuelType = System.Convert.ToInt32(objRetval)
                    Catch ex As Exception
                        nFuelType = 0
                    End Try

                Catch ex As Exception
                    ' failure in this manner would be rare
                    nFuelType = 0
                End Try

            Case DataSourceType.XmlFile
                Throw New ArgumentException(ksUnimplementedDataSourceXmlFile)

            Case Else
                Throw New ArgumentException(ksUnknownDataSource)

        End Select

        Return (nFuelType)
    End Function

#End Region

#Region "GetUnitOfMeasure - (straight look-up only)"

    ''' <summary>
    ''' Unit-of-measure lookup.  Local case statement mapped from database table.
    ''' </summary>
    ''' <param name="unitOfMeasureID"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function GetUnitOfMeasure(ByVal unitOfMeasureID As Integer) As String
        Dim uom As String

        ' See the rate engine database table and re-map when updating needed.
        Select Case unitOfMeasureID
            Case 0
                uom = "KwH"
            Case 1
                uom = "KVA"
            Case 2
                uom = "Therm"
            Case 3
                uom = "DKTherm"
            Case 4
                uom = "CCF"
            Case 5
                uom = "MCF"
            Case 6
                uom = "M3"
            Case 7
                uom = "CCF"
            Case 8
                uom = "Gal"
            Case 9
                uom = "CGAL"
            Case 10
                uom = "CF"
            Case 11
                uom = "KGAL"
            Case 12
                uom = "hgal"
            Case 13
                uom = "DKTherm"
            Case 14
                uom = String.Empty
            Case 15
                uom = "kVAH"
            Case 16
                uom = "kVAR"
            Case 17
                uom = "kVARh"
            Case 18
                uom = "hcf"
            Case 19
                uom = "lb"
            Case 20
                uom = "klb"
            Case 21
                uom = "US gl"
            Case 22
                uom = "cubic meter"
            Case 23
                uom = "cubic foot"
            Case 24
                uom = "CCF"
            Case Else
                uom = String.Empty
        End Select

        Return (uom)
    End Function

#End Region

#End Region

#Region "Data Controller for RTP information - (LocalCache implemented)"

#Region "GetPriceData and GetPriceCollectionPreviousYear - (LocalCache implemented)"

    ''' <summary>
    ''' Accepts an rtp group id, although it is unused.
    ''' </summary>
    ''' <param name="nRateCompanyId"></param>
    ''' <param name="nRTPGroupID"></param>
    ''' <param name="sRateClass"></param>
    ''' <param name="dtStart"></param>
    ''' <param name="dtEnd"></param>
    ''' <param name="bFillMissing"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function GetPriceData(ByVal nRateCompanyId As Integer, ByVal nRTPGroupID As Integer, ByVal sRateClass As String, ByVal dtStart As DateTime, ByVal dtEnd As DateTime, ByVal bFillMissing As Boolean) As PriceCollection
        Return (GetPriceData(nRateCompanyId, sRateClass, dtStart, dtEnd, bFillMissing))
    End Function

    Public Function GetPriceData(ByVal nRateCompanyId As Integer, ByVal sRateClass As String, ByVal dtStart As DateTime, ByVal dtEnd As DateTime) As PriceCollection
        Return (GetPriceData(nRateCompanyId, sRateClass, dtStart, dtEnd, False))
    End Function

    Private Function GetPriceData(ByVal nRateCompanyId As Integer, ByVal sRateClass As String, ByVal dtStart As DateTime, ByVal dtEnd As DateTime, ByVal bFillMissing As Boolean) As PriceCollection
        Return (GetPriceData_Cache(nRateCompanyId, sRateClass, dtStart, dtEnd, bFillMissing))
    End Function

    Private Function GetPriceData_Cache(ByVal nRateCompanyId As Integer, ByVal sRateClass As String, ByVal dtStart As DateTime, ByVal dtEnd As DateTime, ByVal bFillMissing As Boolean) As PriceCollection
        Dim objPriceColl As PriceCollection
        Const cacheKeyLocalTemplate As String = "GPD-{0}-{1}-{2}-{3}-{4}"

        Select Case m_cachingType

            Case Enums.CachingType.NoCache
                objPriceColl = GetPriceData_DataSource(nRateCompanyId, sRateClass, dtStart, dtEnd, bFillMissing)

            Case Enums.CachingType.LocalCache
                Dim cacheKeySB As New StringBuilder()
                Dim cacheKey As String

                cacheKey = cacheKeySB.AppendFormat(cacheKeyLocalTemplate, nRateCompanyId, sRateClass, DateTimeToEpoch(dtStart), DateTimeToEpoch(dtEnd), CInt(bFillMissing)).ToString()

                objPriceColl = CacheLayer.Get(Of PriceCollection)(cacheKey)

                If (objPriceColl Is Nothing) Then
                    objPriceColl = GetPriceData_DataSource(nRateCompanyId, sRateClass, dtStart, dtEnd, bFillMissing)
                    If (Not objPriceColl Is Nothing) Then
                        CacheLayer.Add(objPriceColl, cacheKey, m_cacheTimeoutInMinutes)
                    Else
                        ' did not get object; return an empty collection
                        objPriceColl = New PriceCollection()
                    End If
                End If

            Case Enums.CachingType.DistributedCache
                Throw New ArgumentException(ksUnimplementedCacheTypeDistributedCache)

            Case Enums.CachingType.FlatLocalCache
                Throw New ArgumentException(ksUnimplementedCacheTypeFlatLocalCache)

            Case Enums.CachingType.FlatDistributedCache
                Throw New ArgumentException(ksUnimplementedCacheTypeFlatDistributedCache)

            Case Else
                Throw New ArgumentException(ksUnknownCacheType)

        End Select

        Return (objPriceColl)
    End Function

    Private Function GetPriceData_DataSource(ByVal nRateCompanyId As Integer, ByVal sRateClass As String, ByVal dtStart As DateTime, ByVal dtEnd As DateTime, ByVal bFillMissing As Boolean) As PriceCollection
        Dim objPriceColl As PriceCollection

        Select Case m_dataSourceType

            Case DataSourceType.Database
                Dim dr As SqlDataReader = Nothing
                'Dim objPrice As Price

                Try
                    objPriceColl = MainGetPriceData(nRateCompanyId, sRateClass, dtStart, dtEnd, bFillMissing)

                    'While dr.Read
                    '    objPrice = New Price
                    '    objPrice.DateStamp = CType(dr("TimeStart"), DateTime)
                    '    objPrice.Price = CType(dr("Price"), Double)
                    '    objPrice.PriceStream = CType(dr("ActualStream"), Enums.RTPActualStream)
                    '    objPriceColl.Add(objPrice)
                    'End While

                    '' If at least one price exists, then fill the rest if bFillMissing indicates to do so
                    'If (objPriceColl.Count > 0) Then
                    '    If (bFillMissing) Then
                    '        objPriceColl.FillMissingPrices(dtStart, dtEnd)
                    '    End If
                    'End If

                Catch ex As Exception
                    Throw ex
                Finally
                    If (Not dr Is Nothing) Then
                        If (Not dr.IsClosed) Then
                            dr.Close()
                        End If
                    End If
                End Try

            Case DataSourceType.XmlFile
                Throw New ArgumentException(ksUnimplementedDataSourceXmlFile)

            Case Else
                Throw New ArgumentException(ksUnknownDataSource)

        End Select

        Return (objPriceColl)
    End Function


    ''' <summary>
    ''' This method handles getting pricing data for dates in the future, using previous year data.  Strange name indeed.
    ''' </summary>
    ''' <param name="nRateCompanyID"></param>
    ''' <param name="nRTPGroup"></param>
    ''' <param name="RateClass"></param>
    ''' <param name="dtLocalStartDate"></param>
    ''' <param name="reformattedEndDate"></param>
    ''' <param name="compareDate"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function GetPriceCollectionPreviousYear(ByVal nRateCompanyID As Integer, ByVal nRTPGroup As Integer, ByVal RateClass As String, ByVal dtLocalStartDate As DateTime, ByVal reformattedEndDate As DateTime, ByVal compareDate As DateTime) As PriceCollection
        Return (GetPriceCollectionPreviousYear_Cache(nRateCompanyID, nRTPGroup, RateClass, dtLocalStartDate, reformattedEndDate, compareDate))
    End Function

    Private Function GetPriceCollectionPreviousYear_Cache(ByVal nRateCompanyID As Integer, ByVal nRTPGroup As Integer, ByVal rateClass As String, ByVal dtLocalStartDate As DateTime, ByVal reformattedEndDate As DateTime, ByVal compareDate As DateTime) As PriceCollection
        Dim objPriceColl As PriceCollection
        Const cacheKeyLocalTemplate As String = "GPCPY-{0}-{1}-{2}-{3}-{4}-{5}"

        Select Case m_cachingType

            Case Enums.CachingType.NoCache
                objPriceColl = GetPriceCollectionPreviousYear_DataSource(nRateCompanyID, nRTPGroup, rateClass, dtLocalStartDate, reformattedEndDate, compareDate)

            Case Enums.CachingType.LocalCache
                Dim cacheKeySB As New StringBuilder()
                Dim cacheKey As String

                cacheKey = cacheKeySB.AppendFormat(cacheKeyLocalTemplate, nRateCompanyID, nRTPGroup, rateClass, DateTimeToEpoch(dtLocalStartDate), DateTimeToEpoch(reformattedEndDate), DateTimeToEpoch(compareDate)).ToString()

                objPriceColl = CacheLayer.Get(Of PriceCollection)(cacheKey)

                If (objPriceColl Is Nothing) Then
                    objPriceColl = GetPriceCollectionPreviousYear_DataSource(nRateCompanyID, nRTPGroup, rateClass, dtLocalStartDate, reformattedEndDate, compareDate)
                    If (Not objPriceColl Is Nothing) Then
                        CacheLayer.Add(objPriceColl, cacheKey, m_cacheTimeoutInMinutes)
                    Else
                        ' did not get object; return an empty collection
                        objPriceColl = New PriceCollection()
                    End If
                End If

            Case Enums.CachingType.DistributedCache
                Throw New ArgumentException(ksUnimplementedCacheTypeDistributedCache)

            Case Enums.CachingType.FlatLocalCache
                Throw New ArgumentException(ksUnimplementedCacheTypeFlatLocalCache)

            Case Enums.CachingType.FlatDistributedCache
                Throw New ArgumentException(ksUnimplementedCacheTypeFlatDistributedCache)

            Case Else
                Throw New ArgumentException(ksUnknownCacheType)

        End Select

        Return objPriceColl
    End Function

    Private Function GetPriceCollectionPreviousYear_DataSource(ByVal nRateCompanyID As Integer, ByVal nRTPGroup As Integer, ByVal RateClass As String, ByVal dtLocalStartDate As DateTime, ByVal reformattedEndDate As DateTime, ByVal compareDate As DateTime) As PriceCollection
        Dim objPriceCollection As New PriceCollection
        Dim objPriceCollectionPart2 As PriceCollection
        Dim part2startdate As DateTime

        Select Case m_dataSourceType

            Case DataSourceType.Database
                If (dtLocalStartDate > compareDate) Then

                    objPriceCollection = MainGetPriceData(nRateCompanyID, nRTPGroup, RateClass, dtLocalStartDate.AddYears(-1), reformattedEndDate.AddYears(-1), False)

                    For index As Integer = 0 To objPriceCollection.Count - 1
                        Dim oprice As Price
                        oprice = objPriceCollection(index)
                        ' If we have a leap year in here then remove it
                        If oprice.DateStamp.ToString("MM/dd") = "02/29" Then
                            objPriceCollection.Remove(oprice)
                        Else
                            oprice.DateStamp = oprice.DateStamp.AddYears(1)
                        End If

                    Next

                    objPriceCollection.FillMissingPrices(dtLocalStartDate, reformattedEndDate)
                Else

                    'if our end date is in the future and our start date is not then we need to call getPriceData twice once for previous year and once for current year
                    If reformattedEndDate > compareDate Then
                        Dim newEndDate As New DateTime(compareDate.Year, compareDate.Month, compareDate.Day)

                        newEndDate = newEndDate.AddHours(23)
                        ' First get the stuff in the current year
                        objPriceCollection = MainGetPriceData(nRateCompanyID, nRTPGroup, RateClass, dtLocalStartDate, newEndDate, True)

                        'if the collection is nothing we need to force it to fill
                        If objPriceCollection.Count = 0 Then
                            objPriceCollection.FillMissingPrices(dtLocalStartDate, newEndDate)
                        End If

                        ' Next get the stuff that's in the future using last year's dates
                        ' In this case we'll not ask GetPriceData to fill in missing prices.  We'll do it ourselves using the current dates
                        part2startdate = New DateTime(newEndDate.Year, newEndDate.Month, newEndDate.Day)

                        objPriceCollectionPart2 = MainGetPriceData(nRateCompanyID, nRTPGroup, RateClass, part2startdate.AddDays(1).AddYears(-1), reformattedEndDate.AddYears(-1), False)

                        ' change the dates back to the present year
                        For index As Integer = 0 To objPriceCollectionPart2.Count - 1
                            Dim oprice As Price
                            oprice = objPriceCollectionPart2(index)
                            ' If we have a leap year in here then remove it
                            If oprice.DateStamp.ToString("MM/dd") = "02/29" Then
                                objPriceCollectionPart2.Remove(oprice)
                            Else
                                oprice.DateStamp = oprice.DateStamp.AddYears(1)
                            End If

                        Next
                        objPriceCollectionPart2.FillMissingPrices(part2startdate.AddDays(1), reformattedEndDate)

                        ' add part 2 to part 1
                        For Each oprice As Price In objPriceCollectionPart2
                            objPriceCollection.Add(oprice)
                        Next

                    End If

                End If

            Case DataSourceType.XmlFile
                Throw New ArgumentException(ksUnimplementedDataSourceXmlFile)

            Case Else
                Throw New ArgumentException(ksUnknownDataSource)

        End Select

        Return (objPriceCollection)
    End Function


    Private Function MainGetPriceData(ByVal nRateCompanyId As Integer, ByVal nRTPGroupID As Integer, ByVal sRateClass As String, ByVal dtStartDate As DateTime, ByVal dtEndDate As DateTime, ByVal bFillMissing As Boolean) As PriceCollection
        Return (MainGetPriceData(nRateCompanyId, sRateClass, dtStartDate, dtEndDate, bFillMissing))
    End Function

    Private Function MainGetPriceData(ByVal nRateCompanyId As Integer, ByVal sRateClass As String, ByVal dtStartDate As DateTime, ByVal dtEndDate As DateTime, ByVal bFillMissing As Boolean) As PriceCollection
        Dim objPriceColl As New PriceCollection
        Dim arParams(3) As SqlParameter
        Dim sConn As String
        Dim dr As SqlDataReader = Nothing
        Dim objPrice As Price

        arParams(0) = New SqlParameter("@RateCompanyID", nRateCompanyId)
        arParams(0).Direction = ParameterDirection.Input
        arParams(1) = New SqlParameter("@RateClass", sRateClass)
        arParams(1).Direction = ParameterDirection.Input
        arParams(2) = New SqlParameter("@StartDate", dtStartDate)
        arParams(2).Direction = ParameterDirection.Input
        arParams(3) = New SqlParameter("@EndDate", dtEndDate)
        arParams(3).Direction = ParameterDirection.Input
        sConn = RateEngineConnectionString()

        Try
            dr = SqlHelper.ExecuteReader(sConn, CommandType.StoredProcedure, "rateengine.GetRealTimePricing", arParams)

            While dr.Read
                objPrice = New Price
                objPrice.DateStamp = CType(dr("TimeStart"), DateTime)
                objPrice.Price = CType(dr("Price"), Double)
                objPrice.PriceStream = CType(dr("ActualStream"), Enums.RTPActualStream)
                objPriceColl.Add(objPrice)
            End While

            ' If at least one price exists, then fill the rest if bFillMissing indicates to do so
            If (objPriceColl.Count > 0) Then
                If (bFillMissing) Then
                    objPriceColl.FillMissingPrices(dtStartDate, dtEndDate)
                End If
            End If

        Catch
            Throw
        Finally
            If (Not dr Is Nothing) Then
                If (Not dr.IsClosed) Then
                    dr.Close()
                End If
            End If
        End Try

        Return (objPriceColl)
    End Function


#End Region

#Region "GetRTPgID - (LocalCache implemented)"

    Public Function GetRTPgID(ByVal nRateCompanyId As Integer, ByVal sRateClass As String, ByVal dtStart As DateTime, ByVal dtEnd As DateTime) As Integer
        Return (GetRTPgID_Cache(nRateCompanyId, sRateClass, dtStart, dtEnd))
    End Function

    Private Function GetRTPgID_Cache(ByVal nRateCompanyId As Integer, ByVal sRateClass As String, ByVal dtStart As DateTime, ByVal dtEnd As DateTime) As Integer
        Dim rtpRet? As Integer = -1
        Const cacheKeyLocalTemplate As String = "GRTPGID-{0}-{1}-{2}-{3}"

        Select Case m_cachingType

            Case Enums.CachingType.NoCache
                rtpRet = GetRTPgID_DataSource(nRateCompanyId, sRateClass, dtStart, dtEnd)

            Case Enums.CachingType.LocalCache
                Dim cacheKeySB As New StringBuilder()
                Dim cacheKey As String

                cacheKey = cacheKeySB.AppendFormat(cacheKeyLocalTemplate, nRateCompanyId, sRateClass, DateTimeToEpoch(dtStart), DateTimeToEpoch(dtEnd)).ToString()

                rtpRet = CacheLayer.GetInteger(cacheKey)

                If (rtpRet Is Nothing) Then
                    rtpRet = GetRTPgID_DataSource(nRateCompanyId, sRateClass, dtStart, dtEnd)
                    If ((Not rtpRet Is Nothing) AndAlso rtpRet <> -1) Then
                        CacheLayer.Add(rtpRet, cacheKey, m_cacheTimeoutInMinutes)
                    Else
                        ' original returns -1 when not found
                        rtpRet = -1
                    End If
                End If

            Case Enums.CachingType.DistributedCache
                Throw New ArgumentException(ksUnimplementedCacheTypeDistributedCache)

            Case Enums.CachingType.FlatLocalCache
                Throw New ArgumentException(ksUnimplementedCacheTypeFlatLocalCache)

            Case Enums.CachingType.FlatDistributedCache
                Throw New ArgumentException(ksUnimplementedCacheTypeFlatDistributedCache)

            Case Else
                Throw New ArgumentException(ksUnknownCacheType)

        End Select

        Return (CInt(rtpRet))
    End Function

    Private Function GetRTPgID_DataSource(ByVal nRateCompanyId As Integer, ByVal sRateClass As String, ByVal dtStart As DateTime, ByVal dtEnd As DateTime) As Integer
        Dim rtpRet As Integer = -1

        Select Case m_dataSourceType

            Case DataSourceType.Database
                Dim dr As SqlDataReader = Nothing
                Dim arParams(3) As SqlParameter
                Dim sConn As String

                Try
                    arParams(0) = New SqlParameter("@RateCompanyID", nRateCompanyId)
                    arParams(0).Direction = ParameterDirection.Input
                    arParams(1) = New SqlParameter("@RateClass", sRateClass)
                    arParams(1).Direction = ParameterDirection.Input
                    arParams(2) = New SqlParameter("@StartDate", dtStart)
                    arParams(2).Direction = ParameterDirection.Input
                    arParams(3) = New SqlParameter("@EndDate", dtEnd)
                    arParams(3).Direction = ParameterDirection.Input

                    sConn = RateEngineConnectionString()

                    dr = SqlHelper.ExecuteReader(sConn, CommandType.StoredProcedure, "rateengine.GetRealTimePricingRTPgID", arParams)

                    If dr.Read Then
                        If Not dr("RTPgid") Is System.DBNull.Value Then
                            rtpRet = dr.GetInt32(0)
                        End If
                    End If

                Catch ex As Exception
                    Throw ex
                Finally
                    If (Not dr Is Nothing) Then
                        If (Not dr.IsClosed) Then
                            dr.Close()
                        End If
                    End If
                End Try

            Case DataSourceType.XmlFile
                Throw New ArgumentException(ksUnimplementedDataSourceXmlFile)

            Case Else
                Throw New ArgumentException(ksUnknownDataSource)

        End Select

        Return (rtpRet)
    End Function

#End Region

#Region "GetAnyPeakEvents - (LocalCache implemented)"

    Public Function GetAnyPeakEvents(ByVal rtpGroupID As Integer, ByVal startDate As DateTime, ByVal endDate As DateTime) As CPPEventCollection
        Return (GetAnyPeakEvents_Cache(rtpGroupID, startDate, endDate))
    End Function

    Private Function GetAnyPeakEvents_Cache(ByVal rtpGroupID As Integer, ByVal startDate As DateTime, ByVal endDate As DateTime) As CPPEventCollection
        Dim events As CPPEventCollection
        Const cacheKeyLocalTemplate As String = "GAPE-{0}-{1}-{2}"

        Select Case m_cachingType
            Case Enums.CachingType.NoCache
                events = GetAnyPeakEvents_DataSource(rtpGroupID, startDate, endDate)

            Case Enums.CachingType.LocalCache
                Dim cacheKeySB As New StringBuilder()
                Dim cacheKey As String

                cacheKey = cacheKeySB.AppendFormat(cacheKeyLocalTemplate, rtpGroupID, DateTimeToEpoch(startDate), DateTimeToEpoch(endDate)).ToString()

                events = CacheLayer.Get(Of CPPEventCollection)(cacheKey)

                If (events Is Nothing) Then
                    events = GetAnyPeakEvents_DataSource(rtpGroupID, startDate, endDate)
                    If (Not events Is Nothing) Then
                        CacheLayer.Add(events, cacheKey, m_cacheTimeoutInMinutes)
                    Else
                        ' did not get object; return empty collection
                        events = New CPPEventCollection()
                    End If
                End If

            Case Enums.CachingType.DistributedCache
                Throw New ArgumentException(ksUnimplementedCacheTypeDistributedCache)

            Case Enums.CachingType.FlatLocalCache
                Throw New ArgumentException(ksUnimplementedCacheTypeFlatLocalCache)

            Case Enums.CachingType.FlatDistributedCache
                Throw New ArgumentException(ksUnimplementedCacheTypeFlatDistributedCache)

            Case Else
                Throw New ArgumentException(ksUnknownCacheType)

        End Select

        Return (events)
    End Function

    Private Function GetAnyPeakEvents_DataSource(ByVal rtpGroupID As Integer, ByVal startDate As DateTime, ByVal endDate As DateTime) As CPPEventCollection
        Dim events As CPPEventCollection = Nothing

        Select Case m_dataSourceType

            Case DataSourceType.Database
                Dim sbSQL As New StringBuilder
                Dim arParams(2) As SqlParameter
                Dim dr As SqlDataReader = Nothing
                Dim sConn As String
                Dim cppevent As CPPEvent

                sbSQL.Append("rateengine.GetCriticalPeakEventsByRTPGroupId")
                arParams(0) = New SqlParameter("@RTPgId", rtpGroupID)
                arParams(0).Direction = ParameterDirection.Input
                arParams(1) = New SqlParameter("@StartDate", startDate)
                arParams(1).Direction = ParameterDirection.Input
                arParams(2) = New SqlParameter("@EndDate", endDate)
                arParams(2).Direction = ParameterDirection.Input

                sConn = RateEngineConnectionString()

                Try
                    ' use data reader in case we want to return description and elibibility too in future
                    dr = SqlHelper.ExecuteReader(sConn, CommandType.StoredProcedure, sbSQL.ToString(), arParams)

                    Do While dr.Read()
                        If (events Is Nothing) Then events = New CPPEventCollection()

                        cppevent = New CPPEvent()

                        If (Not IsDBNull(dr("TimeStart"))) Then cppevent.StartDate = CType(dr("TimeStart"), DateTime)
                        If (Not IsDBNull(dr("TimeEnd"))) Then cppevent.EndDate = CType(dr("TimeEnd"), DateTime)

                        events.Add(cppevent)

                    Loop

                Catch ex As Exception
                    Throw ex
                Finally
                    If (Not dr Is Nothing) Then
                        If (Not dr.IsClosed) Then
                            dr.Close()
                        End If
                    End If
                End Try

            Case DataSourceType.XmlFile
                Throw New ArgumentException(ksUnimplementedDataSourceXmlFile)

            Case Else
                Throw New ArgumentException(ksUnknownDataSource)

        End Select

        Return (events)
    End Function

#End Region

#Region "GetRTPSimulatedEvents - (LocalCache implemented)"

    Public Function GetRTPSimulatedEvents(ByVal rateCompanyId As Integer, ByVal numberOfEvents As Integer) As SimEventCollection
        Return (GetRTPSimulatedEvents_Cache(rateCompanyId, numberOfEvents))
    End Function

    Private Function GetRTPSimulatedEvents_Cache(ByVal rateCompanyId As Integer, ByVal numberOfEvents As Integer) As SimEventCollection
        Dim events As SimEventCollection
        Const cacheKeyLocalTemplate As String = "GRTPSE-{0}-{1}"

        Select Case m_cachingType
            Case Enums.CachingType.NoCache
                events = GetRTPSimulatedEvents_DataSource(rateCompanyId, numberOfEvents)

            Case Enums.CachingType.LocalCache
                Dim cacheKeySB As New StringBuilder()
                Dim cacheKey As String

                cacheKey = cacheKeySB.AppendFormat(cacheKeyLocalTemplate, rateCompanyId, numberOfEvents).ToString()

                events = CacheLayer.Get(Of SimEventCollection)(cacheKey)

                If (events Is Nothing) Then
                    events = GetRTPSimulatedEvents_DataSource(rateCompanyId, numberOfEvents)
                    If (Not events Is Nothing) Then
                        CacheLayer.Add(events, cacheKey, m_cacheTimeoutInMinutes)
                    Else
                        ' did not get object; return empty list
                        events = New SimEventCollection()
                    End If
                End If

            Case Enums.CachingType.DistributedCache
                Throw New ArgumentException(ksUnimplementedCacheTypeDistributedCache)

            Case Enums.CachingType.FlatLocalCache
                Throw New ArgumentException(ksUnimplementedCacheTypeFlatLocalCache)

            Case Enums.CachingType.FlatDistributedCache
                Throw New ArgumentException(ksUnimplementedCacheTypeFlatDistributedCache)

            Case Else
                Throw New ArgumentException(ksUnknownCacheType)

        End Select

        Return (events)
    End Function

    Private Function GetRTPSimulatedEvents_DataSource(ByVal rateCompanyId As Integer, ByVal numberOfEvents As Integer) As SimEventCollection
        Dim events As SimEventCollection = Nothing

        Select Case m_dataSourceType

            Case DataSourceType.Database
                Dim sbSQL As New StringBuilder
                Dim arParams(1) As SqlParameter
                Dim dr As SqlDataReader = Nothing
                Dim sConn As String
                Dim simevent As SimEvent

                sbSQL.Append("rateengine.GetRTPSimulatedEvents")
                arParams(0) = New SqlParameter("@RateCompanyID", rateCompanyId)
                arParams(0).Direction = ParameterDirection.Input
                arParams(1) = New SqlParameter("@NumberOfEvents", numberOfEvents)
                arParams(1).Direction = ParameterDirection.Input

                sConn = RateEngineConnectionString()

                Try
                    dr = SqlHelper.ExecuteReader(sConn, CommandType.StoredProcedure, sbSQL.ToString(), arParams)

                    Do While dr.Read()
                        If (events Is Nothing) Then events = New SimEventCollection()

                        simevent = New SimEvent()

                        If (Not IsDBNull(dr("Priority"))) Then simevent.Priority = CType(dr("Priority"), Integer)
                        If (Not IsDBNull(dr("Month"))) Then simevent.Month = CType(dr("Month"), Integer)

                        events.Add(simevent)
                    Loop

                Catch ex As Exception
                    Throw ex
                Finally
                    If (Not dr Is Nothing) Then
                        If (Not dr.IsClosed) Then
                            dr.Close()
                        End If
                    End If
                End Try

            Case DataSourceType.XmlFile
                Throw New ArgumentException(ksUnimplementedDataSourceXmlFile)

            Case Else
                Throw New ArgumentException(ksUnknownDataSource)

        End Select

        Return (events)
    End Function

#End Region

#Region "GetRTPSimulatedEventDuration - (LocalCache implemented)"

    Public Function GetRTPSimulatedEventDuration(ByVal rtpGroupId As Integer) As SimDurationCollection
        Return (GetRTPSimulatedEventDuration_Cache(rtpGroupId))
    End Function

    Private Function GetRTPSimulatedEventDuration_Cache(ByVal rtpGroupId As Integer) As SimDurationCollection
        Dim durations As SimDurationCollection
        Const cacheKeyLocalTemplate As String = "GRTPSED-{0}"

        Select Case m_cachingType
            Case Enums.CachingType.NoCache
                durations = GetRTPSimulatedEventDuration_DataSource(rtpGroupId)

            Case Enums.CachingType.LocalCache
                Dim cacheKeySB As New StringBuilder()
                Dim cacheKey As String

                cacheKey = cacheKeySB.AppendFormat(cacheKeyLocalTemplate, rtpGroupId).ToString()

                durations = CacheLayer.Get(Of SimDurationCollection)(cacheKey)

                If (durations Is Nothing) Then
                    durations = GetRTPSimulatedEventDuration_DataSource(rtpGroupId)
                    If (Not durations Is Nothing) Then
                        CacheLayer.Add(durations, cacheKey, m_cacheTimeoutInMinutes)
                    Else
                        ' did not get object; return empty collection
                        durations = New SimDurationCollection()
                    End If
                End If

            Case Enums.CachingType.DistributedCache
                Throw New ArgumentException(ksUnimplementedCacheTypeDistributedCache)

            Case Enums.CachingType.FlatLocalCache
                Throw New ArgumentException(ksUnimplementedCacheTypeFlatLocalCache)

            Case Enums.CachingType.FlatDistributedCache
                Throw New ArgumentException(ksUnimplementedCacheTypeFlatDistributedCache)

            Case Else
                Throw New ArgumentException(ksUnknownCacheType)

        End Select

        Return (durations)
    End Function

    Private Function GetRTPSimulatedEventDuration_DataSource(ByVal rtpGroupId As Integer) As SimDurationCollection
        Dim durations As SimDurationCollection = Nothing

        Select Case m_dataSourceType

            Case DataSourceType.Database
                Dim sbSQL As New StringBuilder
                Dim arParams(0) As SqlParameter
                Dim dr As SqlDataReader = Nothing
                Dim sConn As String
                Dim simduration As SimDuration

                sbSQL.Append("rateengine.GetRTPSimulatedEventDurationByGroupId")
                arParams(0) = New SqlParameter("@RTPgId", rtpGroupId)
                arParams(0).Direction = ParameterDirection.Input

                sConn = RateEngineConnectionString()

                Try
                    dr = SqlHelper.ExecuteReader(sConn, CommandType.StoredProcedure, sbSQL.ToString(), arParams)

                    Do While dr.Read()
                        If (durations Is Nothing) Then durations = New SimDurationCollection()

                        simduration = New SimDuration()

                        If (Not IsDBNull(dr("SeasonID"))) Then simduration.Season = CType(dr("SeasonID"), Enums.Season)
                        If (Not IsDBNull(dr("StartHour"))) Then simduration.StartHour = CType(dr("StartHour"), Integer)
                        If (Not IsDBNull(dr("EndHour"))) Then simduration.EndHour = CType(dr("EndHour"), Integer)

                        durations.Add(simduration)

                    Loop

                Catch ex As Exception
                    Throw ex
                Finally
                    If (Not dr Is Nothing) Then
                        If (Not dr.IsClosed) Then
                            dr.Close()
                        End If
                    End If
                End Try

            Case DataSourceType.XmlFile
                Throw New ArgumentException(ksUnimplementedDataSourceXmlFile)

            Case Else
                Throw New ArgumentException(ksUnknownDataSource)

        End Select

        Return (durations)
    End Function

#End Region

#End Region

#Region "Data Controller for Holiday information (LocalCache implemented)"

#Region "GetHolidayList Overload A - (LocalCache implemented)"

    ''' <summary>
    ''' Get holiday information.
    ''' </summary>
    ''' <param name="holidayGroupID"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function GetHolidayList(ByVal holidayGroupID As Integer) As HolidayCollection
        Return (GetHolidayList_Cache(holidayGroupID))
    End Function

    Private Function GetHolidayList_Cache(ByVal holidayGroupID As Integer) As HolidayCollection
        Dim holidays As HolidayCollection
        Const cacheKeyLocalTemplate As String = "GHLA-{0}"

        Select Case m_cachingType

            Case Enums.CachingType.NoCache
                holidays = GetHolidayList_DataSource(holidayGroupID)

            Case Enums.CachingType.LocalCache
                Dim cacheKeySB As New StringBuilder()
                Dim cacheKey As String

                cacheKey = cacheKeySB.AppendFormat(cacheKeyLocalTemplate, holidayGroupID).ToString()

                holidays = CacheLayer.Get(Of HolidayCollection)(cacheKey)

                If (holidays Is Nothing) Then
                    holidays = GetHolidayList_DataSource(holidayGroupID)
                    If (Not holidays Is Nothing) Then
                        CacheLayer.Add(holidays, cacheKey, m_cacheTimeoutInMinutes)
                    Else
                        ' did not get object; return an empty collection
                        holidays = New HolidayCollection()
                    End If
                End If

            Case Enums.CachingType.DistributedCache
                Throw New ArgumentException(ksUnimplementedCacheTypeDistributedCache)

            Case Enums.CachingType.FlatLocalCache
                Throw New ArgumentException(ksUnimplementedCacheTypeFlatLocalCache)

            Case Enums.CachingType.FlatDistributedCache
                Throw New ArgumentException(ksUnimplementedCacheTypeFlatDistributedCache)

            Case Else
                Throw New ArgumentException(ksUnknownCacheType)

        End Select

        Return (holidays)
    End Function

    Private Function GetHolidayList_DataSource(ByVal holidayGroupID As Integer) As HolidayCollection
        Dim holidays As HolidayCollection

        Select Case m_dataSourceType

            Case DataSourceType.Database
                Dim sbSQL As New StringBuilder
                Dim arParams(1) As SqlParameter
                sbSQL.Append("rateengine.SelectHolidayList")
                arParams(0) = New SqlParameter("@HolidayGroupID", holidayGroupID)
                arParams(0).Direction = ParameterDirection.Input

                holidays = MainGetHolidayList(arParams, sbSQL)

            Case DataSourceType.XmlFile
                Throw New ArgumentException(ksUnimplementedDataSourceXmlFile)

            Case Else
                Throw New ArgumentException(ksUnknownDataSource)

        End Select

        Return (holidays)
    End Function

#End Region

#Region "GetHolidayList Overload B - (LocalCache implemented)"

    ''' <summary>
    ''' Get holiday information.
    ''' </summary>
    ''' <param name="holidayGroupID"></param>
    ''' <param name="startDate"></param>
    ''' <param name="endDate"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function GetHolidayList(ByVal holidayGroupID As Integer, ByVal startDate As DateTime, ByVal endDate As DateTime) As HolidayCollection
        Return (GetHolidayList_Cache(holidayGroupID, startDate, endDate))
    End Function

    Private Function GetHolidayList_Cache(ByVal holidayGroupID As Integer, ByVal startDate As DateTime, ByVal endDate As DateTime) As HolidayCollection
        Dim holidays As HolidayCollection
        Const cacheKeyLocalTemplate As String = "GHLB-{0}-{1}-{2}"

        Select Case m_cachingType

            Case Enums.CachingType.NoCache
                holidays = GetHolidayList_DataSource(holidayGroupID, startDate, endDate)

            Case Enums.CachingType.LocalCache
                Dim cacheKeySB As New StringBuilder()
                Dim cacheKey As String

                cacheKey = cacheKeySB.AppendFormat(cacheKeyLocalTemplate, holidayGroupID, DateTimeToEpoch(startDate), DateTimeToEpoch(endDate)).ToString()

                holidays = CacheLayer.Get(Of HolidayCollection)(cacheKey)

                If (holidays Is Nothing) Then
                    holidays = GetHolidayList_DataSource(holidayGroupID, startDate, endDate)
                    If (Not holidays Is Nothing) Then
                        CacheLayer.Add(holidays, cacheKey, m_cacheTimeoutInMinutes)
                    Else
                        ' did not get object; return an empty collection
                        holidays = New HolidayCollection()
                    End If
                End If

            Case Enums.CachingType.DistributedCache
                Throw New ArgumentException(ksUnimplementedCacheTypeDistributedCache)

            Case Enums.CachingType.FlatLocalCache
                Throw New ArgumentException(ksUnimplementedCacheTypeFlatLocalCache)

            Case Enums.CachingType.FlatDistributedCache
                Throw New ArgumentException(ksUnimplementedCacheTypeFlatDistributedCache)

            Case Else
                Throw New ArgumentException(ksUnknownCacheType)

        End Select

        Return (holidays)
    End Function

    Private Function GetHolidayList_DataSource(ByVal holidayGroupID As Integer, ByVal startDate As DateTime, ByVal endDate As DateTime) As HolidayCollection
        Dim holidays As HolidayCollection

        Select Case m_dataSourceType

            Case DataSourceType.Database
                Dim sbSQL As New StringBuilder
                Dim arParams(2) As SqlParameter
                sbSQL.Append("rateengine.SelectHolidayListB")
                arParams(0) = New SqlParameter("@HolidayGroupID", holidayGroupID)
                arParams(0).Direction = ParameterDirection.Input
                arParams(1) = New SqlParameter("@StartDate", startDate)
                arParams(1).Direction = ParameterDirection.Input
                arParams(2) = New SqlParameter("@EndDate", endDate)
                arParams(2).Direction = ParameterDirection.Input

                holidays = MainGetHolidayList(arParams, sbSQL)

            Case DataSourceType.XmlFile
                Throw New ArgumentException(ksUnimplementedDataSourceXmlFile)

            Case Else
                Throw New ArgumentException(ksUnknownDataSource)

        End Select

        Return (holidays)
    End Function

#End Region

    ' Private Core Function for all public GetHolidayList functions
    Private Function MainGetHolidayList(ByVal arParams() As SqlParameter, ByVal sbSQL As StringBuilder) As HolidayCollection
        Dim holidays As New HolidayCollection
        Dim dr As SqlDataReader = Nothing
        Dim sConn As String
        Dim holidayID As Integer = -1
        Dim holidayName As String = String.Empty
        Dim holidayGroupID As Integer = -1
        Dim holidayDate As DateTime

        sConn = RateEngineConnectionString()

        Try
            dr = SqlHelper.ExecuteReader(sConn, CommandType.StoredProcedure, sbSQL.ToString(), arParams)
            Do While dr.Read()
                If (Not IsDBNull(dr("HoID"))) Then holidayID = CType(dr("HoID"), Integer)
                If (Not IsDBNull(dr("HoHgID"))) Then holidayGroupID = CType(dr("HoHgID"), Integer)
                If (Not IsDBNull(dr("HoName"))) Then holidayName = CType(dr("HoName"), String)
                If (Not IsDBNull(dr("HoDate"))) Then holidayDate = CType(dr("HoDate"), DateTime)
                holidays.Add(New Holiday(holidayID, holidayGroupID, holidayName, holidayDate))
            Loop

        Catch ex As Exception
            Throw New Exception(ksDataReaderError, ex.InnerException)
        Finally
            If (Not dr Is Nothing) Then
                If (Not dr.IsClosed) Then
                    dr.Close()
                End If
            End If
        End Try

        Return (holidays)
    End Function

#End Region




#Region "Data Controller for GetRateCompanySettings information (Always Cached)"

#Region "GetRateCompanySettings (Always Cached)"

    ''' <summary>
    ''' Get Settings for the RateCompnayID.
    ''' </summary>
    Public Function GetRateCompanySettings(ByVal rateCompanyID As Integer) As RateCompanySettings
        Return (GetRateCompanySettings_Cache(rateCompanyID))
    End Function

    Private Function GetRateCompanySettings_Cache(ByVal rateCompanyID As Integer) As RateCompanySettings
        Dim settings As RateCompanySettings
        Const cacheKeyLocalTemplate As String = "GRCS-{0}"

        Select Case m_cachingType

            Case Enums.CachingType.NoCache, Enums.CachingType.LocalCache, Enums.CachingType.DistributedCache, Enums.CachingType.FlatLocalCache, Enums.CachingType.FlatDistributedCache
                Dim cacheKeySB As New StringBuilder()
                Dim cacheKey As String

                cacheKey = cacheKeySB.AppendFormat(cacheKeyLocalTemplate, rateCompanyID).ToString()

                settings = CacheLayer.Get(Of RateCompanySettings)(cacheKey)

                If (settings Is Nothing) Then
                    settings = GetRateCompanySettings_DataSource(rateCompanyID)
                    If (Not settings Is Nothing) Then
                        CacheLayer.Add(settings, cacheKey, m_cacheTimeoutInMinutes)
                    Else
                        ' did not get object; return an empty collection
                        settings = New RateCompanySettings()
                    End If
                End If

            Case Else
                Throw New ArgumentException(ksUnknownCacheType)

        End Select

        Return (settings)
    End Function

    Private Function GetRateCompanySettings_DataSource(ByVal rateCompanyID As Integer) As RateCompanySettings
        Dim settings As RateCompanySettings

        Select Case m_dataSourceType

            Case DataSourceType.Database
                Dim sbSQL As New StringBuilder
                Dim arParams(1) As SqlParameter
                sbSQL.Append("rateengine.GetRateCompanySettings")
                arParams(0) = New SqlParameter("@RateCompanyId", rateCompanyID)
                arParams(0).Direction = ParameterDirection.Input

                settings = MainGetRateCompanySettings(arParams, sbSQL)

            Case DataSourceType.XmlFile
                Throw New ArgumentException(ksUnimplementedDataSourceXmlFile)

            Case Else
                Throw New ArgumentException(ksUnknownDataSource)

        End Select

        Return (settings)
    End Function

#End Region

    ''' <summary>
    '''  Private Core Function for all public MainGetRateCompanySettings functions
    ''' </summary>
    ''' <param name="arParams"></param>
    ''' <param name="sbSQL"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function MainGetRateCompanySettings(ByVal arParams() As SqlParameter, ByVal sbSQL As StringBuilder) As RateCompanySettings
        Dim settings As New RateCompanySettings()
        Dim dr As SqlDataReader = Nothing
        Dim sConn As String
        Dim id As Integer = 0
        Const SETTINGID_VERSION As Integer = 0
        Const SETTINGID_NEM3CONSTANTD1 As Integer = 1
        Const SETTINGID_NEM3CONSTANTD2 As Integer = 2
        Const SETTINGID_NEM3COSTTYPES As Integer = 3
        Const SETTINGID_NEM4CONSTANTD1 As Integer = 4
        Const SETTINGID_NEM4CONSTANTD2 As Integer = 5
        Const SETTINGID_NEM4COSTTYPES As Integer = 6
        Const SETTINGID_NEM5CONSTANTD1 As Integer = 7
        Const SETTINGID_NEM5CONSTANTD2 As Integer = 8
        Const SETTINGID_NEM5COSTTYPES As Integer = 9
        Const column_SettingID As String = "SettingID"
        Const column_SettingValue As String = "SettingValue"

        sConn = RateEngineConnectionString()

        Try
            dr = SqlHelper.ExecuteReader(sConn, CommandType.StoredProcedure, sbSQL.ToString(), arParams)
            Do While dr.Read()

                If (Not IsDBNull(dr(column_SettingID))) Then id = CType(dr(column_SettingID), Integer)

                Select Case id

                    Case SETTINGID_VERSION
                        If (Not IsDBNull(dr(column_SettingValue))) Then settings.Version = CType(dr(column_SettingValue), String)

                    Case SETTINGID_NEM3CONSTANTD1
                        If (Not IsDBNull(dr(column_SettingValue))) Then settings.Nem3ConstantD1 = CType(dr(column_SettingValue), Integer)

                    Case SETTINGID_NEM3CONSTANTD2
                        If (Not IsDBNull(dr(column_SettingValue))) Then settings.Nem3ConstantD2 = CType(dr(column_SettingValue), Integer)

                    Case SETTINGID_NEM3COSTTYPES
                        If (Not IsDBNull(dr(column_SettingValue))) Then settings.Nem3CostTypes = CType(dr(column_SettingValue), String)

                    Case SETTINGID_NEM4CONSTANTD1
                        If (Not IsDBNull(dr(column_SettingValue))) Then settings.Nem4ConstantD1 = CType(dr(column_SettingValue), Integer)

                    Case SETTINGID_NEM4CONSTANTD2
                        If (Not IsDBNull(dr(column_SettingValue))) Then settings.Nem4ConstantD2 = CType(dr(column_SettingValue), Integer)

                    Case SETTINGID_NEM4COSTTYPES
                        If (Not IsDBNull(dr(column_SettingValue))) Then settings.Nem4CostTypes = CType(dr(column_SettingValue), String)

                    Case SETTINGID_NEM5CONSTANTD1
                        If (Not IsDBNull(dr(column_SettingValue))) Then settings.Nem5ConstantD1 = CType(dr(column_SettingValue), Integer)

                    Case SETTINGID_NEM5CONSTANTD2
                        If (Not IsDBNull(dr(column_SettingValue))) Then settings.Nem5ConstantD2 = CType(dr(column_SettingValue), Integer)

                    Case SETTINGID_NEM5COSTTYPES
                        If (Not IsDBNull(dr(column_SettingValue))) Then settings.Nem5CostTypes = CType(dr(column_SettingValue), String)

                End Select

            Loop

        Catch ex As Exception
            Throw New Exception(ksDataReaderError, ex.InnerException)
        Finally
            If (Not dr Is Nothing) Then
                If (Not dr.IsClosed) Then
                    dr.Close()
                End If
            End If
        End Try

        Return (settings)
    End Function

#End Region




#Region "Logging Functions"

    Public Function InsertLog(ByVal LogSessionID As System.Guid, ByVal sText As String) As Boolean
        Dim bRetval As Boolean = True
        Dim sbSQL As New StringBuilder
        Dim arParams(2) As SqlParameter
        Dim sConn As String

        sbSQL.Append("rateengine.InsertLog")
        arParams(0) = New SqlParameter("@LoSessionID", LogSessionID)
        arParams(0).Direction = ParameterDirection.Input
        arParams(1) = New SqlParameter("@LoText", sText)
        arParams(1).Direction = ParameterDirection.Input
        arParams(2) = New SqlParameter("@LoDate", DateTime.Now)
        arParams(2).Direction = ParameterDirection.Input

        sConn = RateEngineConnectionString()

        Try
            SqlHelper.ExecuteNonQuery(sConn, CommandType.StoredProcedure, sbSQL.ToString(), arParams)
        Catch ex As Exception
            bRetval = False
            ' Throw New Exception(ksLogError, ex.InnerException)
            ' Ignore logging errors (they should not be critical errors)
        Finally
            ' noop
        End Try

        Return (bRetval)
    End Function

    Public Function InsertLog(ByVal LogSessionID As System.Guid, ByVal obj As Object) As Boolean
        Dim bRetval As Boolean = True
        Dim sbSQL As New StringBuilder
        Dim arParams(3) As SqlParameter
        Dim sConn As String
        Dim bf As New Binary.BinaryFormatter
        Dim ms As New System.IO.MemoryStream

        bf.Serialize(ms, obj)

        sbSQL.Append("rateengine.InsertLogOfReadings")
        arParams(0) = New SqlParameter("@LoSessionID", LogSessionID)
        arParams(0).Direction = ParameterDirection.Input
        arParams(1) = New SqlParameter("@LoText", ksBinaryDataIndicated)
        arParams(1).Direction = ParameterDirection.Input
        arParams(2) = New SqlParameter("@LoDate", DateTime.Now)
        arParams(2).Direction = ParameterDirection.Input
        arParams(3) = New SqlParameter("@LoBinary", SqlDbType.VarBinary)
        arParams(3).Direction = ParameterDirection.Input
        arParams(3).Value = ms.ToArray()

        sConn = RateEngineConnectionString()

        Try
            SqlHelper.ExecuteNonQuery(sConn, CommandType.StoredProcedure, sbSQL.ToString(), arParams)
        Catch ex As Exception
            bRetval = False
            ' Throw New Exception(ksLogError, ex.InnerException)
            ' Ignore logging errors (they should not be critical errors)
        Finally
            ' noop
        End Try

        Return (bRetval)
    End Function

    Public Function GetLogHistory(ByVal guidSession As Guid) As LogEntryCollection
        Dim sbSQL As New StringBuilder
        Dim arParams(1) As SqlParameter
        Dim dr As SqlDataReader = Nothing
        Dim sConn As String
        Dim objLogEntries As New LogEntryCollection
        Dim objLogEntry As LogEntry
        Dim nLoID As Integer
        Dim sLoText As String = Nothing

        sbSQL.Append("rateengine.GetLogHistory")
        arParams(0) = New SqlParameter("@LoSessionID", guidSession)
        arParams(0).Direction = ParameterDirection.Input

        sConn = RateEngineConnectionString()

        Try
            dr = SqlHelper.ExecuteReader(sConn, CommandType.StoredProcedure, sbSQL.ToString(), arParams)
            Do While dr.Read()
                If (Not IsDBNull(dr("LoID"))) Then nLoID = CType(dr("LoID"), Integer)
                If (Not IsDBNull(dr("LoText"))) Then sLoText = CType(dr("LoText"), String)
                objLogEntry = New LogEntry(nLoID, sLoText)
                objLogEntries.Add(objLogEntry)
            Loop

        Catch ex As Exception
            Throw New Exception(ksGetLogHistoryError)
        Finally
            If (Not dr Is Nothing) Then
                If (Not dr.IsClosed) Then
                    dr.Close()
                End If
            End If
        End Try

        Return (objLogEntries)
    End Function

    Public Function GetLoggedReadings(ByVal guidSession As Guid) As ReadingCollection
        Dim sbSQL As New StringBuilder
        Dim arParams(1) As SqlParameter
        Dim dr As SqlDataReader = Nothing
        Dim sConn As String
        Dim nLoID As Integer
        Dim sLoText As String
        Dim binaryData As Byte() = Nothing
        Dim readings As ReadingCollection = Nothing
        Dim found As Boolean = False

        sbSQL.Append("rateengine.GetLoggedReadings")
        arParams(0) = New SqlParameter("@LoSessionID", guidSession)
        arParams(0).Direction = ParameterDirection.Input

        sConn = RateEngineConnectionString()

        Try
            dr = SqlHelper.ExecuteReader(sConn, CommandType.StoredProcedure, sbSQL.ToString(), arParams)
            Do While dr.Read()
                If (Not IsDBNull(dr("LoID"))) Then nLoID = CType(dr("LoID"), Integer)
                If (Not IsDBNull(dr("LoText"))) Then sLoText = CType(dr("LoText"), String)
                If (Not IsDBNull(dr("LoBinary"))) Then binaryData = CType(dr("LoBinary"), Byte())
                found = True
                Exit Do
            Loop

        Catch ex As Exception
            Throw New Exception(ksGetLoggedReadingsError)
        Finally
            If (Not dr Is Nothing) Then
                If (Not dr.IsClosed) Then
                    dr.Close()
                End If
            End If
        End Try

        If (found) Then
            Dim bf As System.Runtime.Serialization.Formatters.Binary.BinaryFormatter = New System.Runtime.Serialization.Formatters.Binary.BinaryFormatter
            Dim ms As System.IO.MemoryStream = New System.IO.MemoryStream(binaryData)
            Dim obj As Object

            ms.Position = 0
            obj = bf.Deserialize(ms)
            readings = CType(obj, ReadingCollection)

            ms.Close()
        End If

        Return (readings)
    End Function

    Public Function GetLoggedReadings(ByVal guidSession As Guid, ByVal skipFirstSet As Boolean) As ReadingCollection
        Dim sbSQL As New StringBuilder
        Dim arParams(1) As SqlParameter
        Dim dr As SqlDataReader = Nothing
        Dim sConn As String
        Dim nLoID As Integer
        Dim sLoText As String
        Dim binaryData As Byte() = Nothing
        Dim readings As ReadingCollection = Nothing
        Dim found As Boolean = False
        Dim skip As Boolean = False

        skip = skipFirstSet

        sbSQL.Append("rateengine.GetLoggedReadings")
        arParams(0) = New SqlParameter("@LoSessionID", guidSession)
        arParams(0).Direction = ParameterDirection.Input

        sConn = RateEngineConnectionString()

        Try
            dr = SqlHelper.ExecuteReader(sConn, CommandType.StoredProcedure, sbSQL.ToString(), arParams)
            Do While dr.Read()

                If (Not skip) Then
                    If (Not IsDBNull(dr("LoID"))) Then nLoID = CType(dr("LoID"), Integer)
                    If (Not IsDBNull(dr("LoText"))) Then sLoText = CType(dr("LoText"), String)
                    If (Not IsDBNull(dr("LoBinary"))) Then binaryData = CType(dr("LoBinary"), Byte())
                    found = True
                    Exit Do
                Else
                    skip = False
                End If

            Loop

        Catch ex As Exception
            Throw New Exception(ksGetLoggedReadingsError)
        Finally
            If (Not dr Is Nothing) Then
                If (Not dr.IsClosed) Then
                    dr.Close()
                End If
            End If
        End Try

        If (found) Then
            Dim bf As System.Runtime.Serialization.Formatters.Binary.BinaryFormatter = New System.Runtime.Serialization.Formatters.Binary.BinaryFormatter
            Dim ms As System.IO.MemoryStream = New System.IO.MemoryStream(binaryData)
            Dim obj As Object

            ms.Position = 0
            obj = bf.Deserialize(ms)
            readings = CType(obj, ReadingCollection)

            ms.Close()
        End If

        Return (readings)
    End Function

    Public Function GetLoggedReadings(ByVal guidSession As Guid, ByVal index As Integer) As ReadingCollection
        Dim sbSQL As New StringBuilder
        Dim arParams(1) As SqlParameter
        Dim dr As SqlDataReader = Nothing
        Dim sConn As String
        Dim nLoID As Integer
        Dim sLoText As String
        Dim binaryData As Byte() = Nothing
        Dim readings As ReadingCollection = Nothing
        Dim found As Boolean = False
        Dim indexCount As Integer = 0

        sbSQL.Append("rateengine.GetLoggedReadings")
        arParams(0) = New SqlParameter("@LoSessionID", guidSession)
        arParams(0).Direction = ParameterDirection.Input

        sConn = RateEngineConnectionString()

        Try
            dr = SqlHelper.ExecuteReader(sConn, CommandType.StoredProcedure, sbSQL.ToString(), arParams)
            Do While dr.Read()

                If (indexCount = index) Then
                    If (Not IsDBNull(dr("LoID"))) Then nLoID = CType(dr("LoID"), Integer)
                    If (Not IsDBNull(dr("LoText"))) Then sLoText = CType(dr("LoText"), String)
                    If (Not IsDBNull(dr("LoBinary"))) Then binaryData = CType(dr("LoBinary"), Byte())
                    found = True
                    Exit Do
                End If

                indexCount = indexCount + 1
            Loop

        Catch ex As Exception
            Throw New Exception(ksGetLoggedReadingsError)
        Finally
            If (Not dr Is Nothing) Then
                If (Not dr.IsClosed) Then
                    dr.Close()
                End If
            End If
        End Try

        If (found) Then
            Dim bf As System.Runtime.Serialization.Formatters.Binary.BinaryFormatter = New System.Runtime.Serialization.Formatters.Binary.BinaryFormatter
            Dim ms As System.IO.MemoryStream = New System.IO.MemoryStream(binaryData)
            Dim obj As Object

            ms.Position = 0
            obj = bf.Deserialize(ms)
            readings = CType(obj, ReadingCollection)

            ms.Close()
        End If

        Return (readings)
    End Function

#End Region

#Region "Functions for Extras Extensions (LocalCache implemented)"

    ' 
    ' 
    ''' <summary>
    ''' Helper extension to grab RateCompanyID from HubStatic.
    ''' Do not call directly. Use the RateEngineExtras class.
    ''' </summary>
    ''' <param name="nID"></param>
    ''' <param name="eSourceType"></param>
    ''' <param name="sHubStaticConn"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function GetRateCompanyID(ByVal nID As Integer, ByVal eSourceType As SourceType, ByVal sHubStaticConn As String) As Integer
        Return (GetRateCompanyID_Cache(nID, eSourceType, sHubStaticConn))
    End Function

    Private Function GetRateCompanyID_Cache(ByVal nID As Integer, ByVal eSourceType As SourceType, ByVal sHubStaticConn As String) As Integer
        Dim rateCompanyID? As Integer
        Const cacheKeyLocalTemplate As String = "GRCID-{0}-{1}"

        Select Case m_cachingType

            Case Enums.CachingType.NoCache
                rateCompanyID = GetRateCompanyID_DataSource(nID, eSourceType, sHubStaticConn)

            Case Enums.CachingType.LocalCache
                Dim cacheKeySB As New StringBuilder()
                Dim cacheKey As String

                cacheKey = cacheKeySB.AppendFormat(cacheKeyLocalTemplate, nID, CInt(eSourceType)).ToString()

                rateCompanyID = CacheLayer.GetInteger(cacheKey)

                If (rateCompanyID Is Nothing) Then
                    rateCompanyID = GetRateCompanyID_DataSource(nID, eSourceType, sHubStaticConn)
                    If (rateCompanyID <> -1) Then
                        CacheLayer.Add(CInt(rateCompanyID), cacheKey, m_cacheTimeoutInMinutes)
                    Else
                        rateCompanyID = -1
                    End If
                End If

            Case Enums.CachingType.DistributedCache
                Throw New ArgumentException(ksUnimplementedCacheTypeDistributedCache)

            Case Enums.CachingType.FlatLocalCache
                Throw New ArgumentException(ksUnimplementedCacheTypeFlatLocalCache)

            Case Enums.CachingType.FlatDistributedCache
                Throw New ArgumentException(ksUnimplementedCacheTypeFlatDistributedCache)

            Case Else
                Throw New ArgumentException(ksUnknownCacheType)

        End Select

        Return (CInt(rateCompanyID))
    End Function

    Private Function GetRateCompanyID_DataSource(ByVal nID As Integer, ByVal eSourceType As SourceType, ByVal sHubStaticConn As String) As Integer
        Dim nRateCompanyID As Integer

        Select Case m_dataSourceType

            Case DataSourceType.Database
                Dim sbSQL As New StringBuilder
                Dim arParams(1) As SqlParameter
                Dim dr As SqlDataReader = Nothing
                Dim sConn As String
                Dim bFound As Boolean

                sbSQL.Append("rateengine.GetRateCompanyID")
                arParams(0) = New SqlParameter("@SourceType", CType(eSourceType, Integer))
                arParams(0).Direction = ParameterDirection.Input
                arParams(1) = New SqlParameter("@ID", nID)
                arParams(1).Direction = ParameterDirection.Input

                sConn = sHubStaticConn

                Try
                    dr = SqlHelper.ExecuteReader(sConn, CommandType.StoredProcedure, sbSQL.ToString(), arParams)
                    Do While dr.Read()
                        If (Not IsDBNull(dr("RateCompanyID"))) Then nRateCompanyID = CType(dr("RateCompanyID"), Integer)
                        bFound = True
                    Loop

                    If (Not bFound) Then
                        ' Don't throw exception in this case, simply return -1
                        nRateCompanyID = -1
                    End If

                Catch ex As Exception
                    Throw ex
                Finally
                    If (Not dr Is Nothing) Then
                        If (Not dr.IsClosed) Then
                            dr.Close()
                        End If
                    End If
                End Try

            Case DataSourceType.XmlFile
                Throw New ArgumentException(ksUnimplementedDataSourceXmlFile)

            Case Else
                Throw New ArgumentException(ksUnknownDataSource)

        End Select

        Return (nRateCompanyID)
    End Function

#End Region

#Region "Datetime Helper"

    Private Function DateTimeToEpoch(ByVal theDate As Date) As Integer
        Const formatString As String = "1.1.1970 00:00:00"

        Try
            Return (CInt(theDate.Subtract(CDate(formatString)).TotalSeconds))
        Catch ex As System.OverflowException
            Return (-1)
        End Try

    End Function

#End Region

End Class
