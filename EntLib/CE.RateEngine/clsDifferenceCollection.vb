Public Class DifferenceCollection
	Inherits CollectionBase

	Public Sub New()
	End Sub

	Default ReadOnly Property Item(ByVal index As Integer) As Difference
		Get
			Return CType(Me.InnerList.Item(index), Difference)
		End Get
	End Property

	Public Function Add(ByVal value As Difference) As Integer
		Return Me.InnerList.Add(value)
	End Function

	Public Sub Insert(ByVal index As Integer, ByVal value As Difference)
		Me.InnerList.Insert(index, value)
	End Sub

	Public Sub AddRange(ByVal values() As Difference)
		Me.InnerList.AddRange(values)
	End Sub

	Public Sub Remove(ByVal value As Difference)
		Me.InnerList.Remove(value)
	End Sub

End Class
