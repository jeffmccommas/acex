﻿Imports CE.RateEngine.Enums

Public Class BinReadingsExtParam

    Private m_bEnhancedBinning As Boolean
    Private m_nDwellingCount As Integer
    Private m_nTierOverrideDaysInMonth As Integer
    Private m_dblConversionFactor As Double
    Private m_useIndependentTierAccumulations As Boolean
    Private m_useIndependentTierAccumulationsBySeason As Boolean
    Private m_tierAccumulationOffset As Double
    Private m_specialDateAdjustment As Boolean
    Private m_useBillDaysOverrideForFinalizingTierBoundaries As Boolean
    Private m_billDaysOverride As Integer
    Private m_peakEventInfo As PeakEventInfo = Nothing
    Private m_readingAdjustmentFactors As ReadingAdjustFactors = Nothing
    Private m_capResDemand As Double
    Private m_capResDemandSpecified As Boolean = False
    Private m_bSkipTOUBinning As Boolean = False
    Private Const knDefaultTierOverrideDaysInMonth As Integer = 31
    Private Const knDefaultDwellingUnits As Integer = 1
    Private Const kdConversionFactor As Double = 1
    Private Const kbEnhancedBinning As Boolean = False
    Private Const kbDefaultTierAccumulationOffset As Double = 0.0
    Private Const kbDefaultIndependentTierAccumulations As Boolean = False
    Private Const kbDefaultIndependentTierAccumulationsBySeason As Boolean = False
    Private Const kbDefaultSpecialDateAdjustment As Boolean = False
    Private Const kbDefaultUseBillDaysOverrideForFinalizingTierBoundaries As Boolean = False
    Private Const kdCapResDemand As Double = 0.0

    Public Sub New()
        AssignParams(kbEnhancedBinning, knDefaultDwellingUnits, knDefaultTierOverrideDaysInMonth, kdConversionFactor, kbDefaultTierAccumulationOffset)
    End Sub

    Public Sub New(ByVal enhancedBinning As Boolean, ByVal dwellingUnits As Integer, ByVal tierOverrideDaysInMonth As Integer, ByVal conversionFactor As Double)
        AssignParams(enhancedBinning, dwellingUnits, tierOverrideDaysInMonth, conversionFactor, kbDefaultTierAccumulationOffset)
    End Sub

    Public Sub New(ByVal enhancedBinning As Boolean)
        AssignParams(enhancedBinning, knDefaultDwellingUnits, knDefaultTierOverrideDaysInMonth, kdConversionFactor, kbDefaultTierAccumulationOffset)
    End Sub

    Public Sub New(ByVal enhancedBinning As Boolean, ByVal conversionFactor As Double)
        AssignParams(enhancedBinning, knDefaultDwellingUnits, knDefaultTierOverrideDaysInMonth, conversionFactor, kbDefaultTierAccumulationOffset)
    End Sub

    Public Sub New(ByVal enhancedBinning As Boolean, ByVal tierOverrideDaysInMonth As Integer, ByVal conversionFactor As Double)
        AssignParams(enhancedBinning, knDefaultDwellingUnits, tierOverrideDaysInMonth, conversionFactor, kbDefaultTierAccumulationOffset)
    End Sub

    Public Sub New(ByVal enhancedBinning As Boolean, ByVal tierOverrideDaysInMonth As Integer, ByVal tierAccumulationOffset As Double, ByVal conversionFactor As Double)
        AssignParams(enhancedBinning, knDefaultDwellingUnits, tierOverrideDaysInMonth, conversionFactor, tierAccumulationOffset)
    End Sub

    Public Sub New(ByVal enhancedBinning As Boolean, ByVal tierOverrideDaysInMonth As Integer, ByVal tierAccumulationOffset As Double, ByVal conversionFactor As Double, ByVal isSpecialDateAdjustment As Boolean)
        AssignParams(enhancedBinning, knDefaultDwellingUnits, tierOverrideDaysInMonth, conversionFactor, tierAccumulationOffset, isSpecialDateAdjustment)
    End Sub

    Public Sub New(ByVal tierAccumulationOffset As Double, ByVal enhancedBinning As Boolean)
        AssignParams(enhancedBinning, knDefaultDwellingUnits, knDefaultTierOverrideDaysInMonth, kdConversionFactor, tierAccumulationOffset)
    End Sub

    Public Sub New(ByVal tierAccumulationOffset As Double, ByVal enhancedBinning As Boolean, ByVal useBillDaysOverrideForFinalizingTierBoundaries As Boolean, ByVal billDaysOverride As Integer)
        AssignParams(enhancedBinning, knDefaultDwellingUnits, knDefaultTierOverrideDaysInMonth, kdConversionFactor, tierAccumulationOffset)
        m_useBillDaysOverrideForFinalizingTierBoundaries = useBillDaysOverrideForFinalizingTierBoundaries
        m_billDaysOverride = billDaysOverride
    End Sub


#Region "Properties"

    Public ReadOnly Property EnhancedBinning() As Boolean
        Get
            Return (m_bEnhancedBinning)
        End Get
    End Property

    Public ReadOnly Property DwellingCount() As Integer
        Get
            Return (m_nDwellingCount)
        End Get
    End Property

    Public ReadOnly Property TierOverrideDaysInMonth() As Integer
        Get
            Return (m_nTierOverrideDaysInMonth)
        End Get
    End Property

    Public ReadOnly Property ConversionFactor() As Double
        Get
            Return (m_dblConversionFactor)
        End Get
    End Property

    Public ReadOnly Property TierAccumulationOffset() As Double
        Get
            Return (m_tierAccumulationOffset)
        End Get
    End Property

    Public ReadOnly Property UseBillDaysOverrideForFinalizingTierBoundaries() As Boolean
        Get
            Return (m_useBillDaysOverrideForFinalizingTierBoundaries)
        End Get
    End Property

    Public ReadOnly Property BillDaysOverride() As Integer
        Get
            Return (m_billDaysOverride)
        End Get
    End Property


    ''' <summary>
    ''' Use independent tier accumulation if appropriate.
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property UseIndependentTierAccumulations() As Boolean
        Get
            Return m_useIndependentTierAccumulations
        End Get
        Set(ByVal Value As Boolean)
            m_useIndependentTierAccumulations = Value
        End Set
    End Property

    ''' <summary>
    ''' Use independent tier accumulation by season if appropriate.
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property UseIndependentTierAccumulationsBySeason() As Boolean
        Get
            Return m_useIndependentTierAccumulationsBySeason
        End Get
        Set(ByVal Value As Boolean)
            m_useIndependentTierAccumulationsBySeason = Value
        End Set
    End Property

    ' this cane be is set after creating main object.
    Public Property PeakEventInformation() As PeakEventInfo
        Get
            Return m_peakEventInfo
        End Get
        Set(ByVal Value As PeakEventInfo)
            m_peakEventInfo = Value
        End Set
    End Property

    ' this can be set after creating main object.
    Public Property ReadingAdjustmentFactors() As ReadingAdjustFactors
        Get
            Return m_readingAdjustmentFactors
        End Get
        Set(ByVal Value As ReadingAdjustFactors)
            m_readingAdjustmentFactors = Value
        End Set
    End Property

    ''' <summary>
    ''' Capacity Reserve Demand value.
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property CapResDemand() As Double
        Get
            Return m_capResDemand
        End Get
        Set(ByVal Value As Double)
            m_capResDemand = Value
        End Set
    End Property

    Public Property CapResDemandSpecified() As Boolean
        Get
            Return m_capResDemandSpecified
        End Get
        Set(ByVal Value As Boolean)
            m_capResDemandSpecified = Value
        End Set
    End Property

    ' this supports special needs of old rate comparison calculator caller
    Public ReadOnly Property SpecialDateAdjustment() As Boolean
        Get
            Return (m_specialDateAdjustment)
        End Get
    End Property

    Public Property SkipTOUBinning() As Boolean
        Get
            Return (m_bSkipTOUBinning)
        End Get
        Set(value As Boolean)
            m_bSkipTOUBinning = value
        End Set
    End Property

#End Region

#Region "Private Functions"

    ' Called by constructors
    Private Function AssignParams(ByVal enhancedBinning As Boolean, ByVal dwellingUnits As Integer, ByVal tierOverrideDaysInMonth As Integer, ByVal conversionFactor As Double, ByVal tierAccumulationOffset As Double) As Boolean
        Dim bRetval As Boolean = True

        m_bEnhancedBinning = enhancedBinning
        m_nDwellingCount = dwellingUnits
        m_nTierOverrideDaysInMonth = tierOverrideDaysInMonth
        m_dblConversionFactor = conversionFactor
        m_tierAccumulationOffset = tierAccumulationOffset
        m_useIndependentTierAccumulations = kbDefaultIndependentTierAccumulations   ' default to false; property used to override via rate def
        m_useIndependentTierAccumulationsBySeason = kbDefaultIndependentTierAccumulationsBySeason   ' default to false; property used to override via specific rate def properties
        m_specialDateAdjustment = kbDefaultSpecialDateAdjustment ' default to false for most callers
        m_useBillDaysOverrideForFinalizingTierBoundaries = kbDefaultUseBillDaysOverrideForFinalizingTierBoundaries
        m_billDaysOverride = knDefaultTierOverrideDaysInMonth

        Return (bRetval)
    End Function

    Private Function AssignParams(ByVal enhancedBinning As Boolean, ByVal dwellingUnits As Integer, ByVal tierOverrideDaysInMonth As Integer, ByVal conversionFactor As Double, ByVal tierAccumulationOffset As Double, ByVal isSpecialDateAdjustment As Boolean) As Boolean
        Dim bRetval As Boolean = True

        m_bEnhancedBinning = enhancedBinning
        m_nDwellingCount = dwellingUnits
        m_nTierOverrideDaysInMonth = tierOverrideDaysInMonth
        m_dblConversionFactor = conversionFactor
        m_tierAccumulationOffset = tierAccumulationOffset
        m_useIndependentTierAccumulations = kbDefaultIndependentTierAccumulations   ' default to false; property used to override via rate def
        m_useIndependentTierAccumulationsBySeason = kbDefaultIndependentTierAccumulationsBySeason   ' default to false; property used to override via specific rate def properties
        m_specialDateAdjustment = isSpecialDateAdjustment
        m_useBillDaysOverrideForFinalizingTierBoundaries = kbDefaultUseBillDaysOverrideForFinalizingTierBoundaries
        m_billDaysOverride = knDefaultTierOverrideDaysInMonth

        Return (bRetval)
    End Function

#End Region

End Class
