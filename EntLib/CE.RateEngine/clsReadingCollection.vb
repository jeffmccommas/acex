Imports CE.RateEngine.Enums
Imports System.Collections.Generic

<Serializable()> Public Class ReadingCollection
    Inherits CollectionBase
    Implements ICloneable

    Private m_bValidForRTP As Boolean
    Private m_bUseYearOldRTP As Boolean
    Private m_dblAdjustmentFactor As Double
    Private Const kDefaultAdjustmentFactor As Double = 1.0
    Private m_specialInterval As SpecialIntervalType = SpecialIntervalType.IntervalRegular
    Private m_IsDailyInterval As Boolean

    Public Sub New()
        m_bValidForRTP = False
        m_bUseYearOldRTP = False
        m_dblAdjustmentFactor = kDefaultAdjustmentFactor
    End Sub

    Default ReadOnly Property Item(ByVal index As Integer) As Reading
        Get
            Return CType(Me.InnerList.Item(index), Reading)
        End Get
    End Property

    Public Function Add(ByVal value As Reading) As Integer
        Return Me.InnerList.Add(value)
    End Function

    Public Sub Insert(ByVal index As Integer, ByVal value As Reading)
        Me.InnerList.Insert(index, value)
    End Sub

    Public Sub AddRange(ByVal values() As Reading)
        Me.InnerList.AddRange(values)
    End Sub

    Public Sub Remove(ByVal value As Reading)
        Me.InnerList.Remove(value)
    End Sub

#Region "Properties"

    Public Property ValidForRTP() As Boolean
        Get
            Return (m_bValidForRTP)
        End Get
        Set(ByVal Value As Boolean)
            m_bValidForRTP = Value
        End Set
    End Property

    Public Property UseYearOldRTP() As Boolean
        Get
            Return (m_bUseYearOldRTP)
        End Get
        Set(ByVal Value As Boolean)
            m_bUseYearOldRTP = Value
        End Set
    End Property

    Public Property AdjustmentFactor() As Double
        Get
            Return (m_dblAdjustmentFactor)
        End Get
        Set(ByVal Value As Double)
            m_dblAdjustmentFactor = Value
        End Set
    End Property
    Public Property IsDailyInterval() As Boolean
        Get
            Return (m_IsDailyInterval)
        End Get
        Set(ByVal Value As Boolean)
            m_IsDailyInterval = Value
        End Set
    End Property

    Public Property SpecialInterval As SpecialIntervalType
        Get
            Return m_specialInterval
        End Get
        Set(value As SpecialIntervalType)
            m_specialInterval = value
        End Set
    End Property
#End Region

    Public Function Sum() As Double
        Dim dblTotal As Double = 0
        Dim i As Integer
        Dim objR As Reading

        If (Me.InnerList.Count > 0) Then
            For i = 0 To (Me.InnerList.Count - 1)
                objR = CType(Me.InnerList.Item(i), Reading)
                dblTotal = dblTotal + objR.Quantity
            Next
        End If

        Return (dblTotal)
    End Function

    Public Function Sum(ByVal eSeason As Season) As Double
        Dim dblTotal As Double = 0
        Dim i As Integer
        Dim objR As Reading

        If (Me.InnerList.Count > 0) Then
            For i = 0 To (Me.InnerList.Count - 1)
                objR = CType(Me.InnerList.Item(i), Reading)
                If (objR.Season = eSeason) Then
                    dblTotal = dblTotal + objR.Quantity
                End If
            Next
        End If

        Return (dblTotal)
    End Function

    Public Function Sum(ByVal eTou As TimeOfUse) As Double
        Dim dblTotal As Double = 0
        Dim i As Integer
        Dim objR As Reading

        If (Me.InnerList.Count > 0) Then
            For i = 0 To (Me.InnerList.Count - 1)
                objR = CType(Me.InnerList.Item(i), Reading)
                If (objR.TimeOfUse = eTou) Then
                    dblTotal = dblTotal + objR.Quantity
                End If
            Next
        End If

        Return (dblTotal)
    End Function

    Public Function Sum(ByVal eBaseOrTier As BaseOrTier, ByVal eTou As TimeOfUse) As Double
        Dim dblTotal As Double = 0
        Dim i As Integer
        Dim objR As Reading

        If (Me.InnerList.Count > 0) Then
            For i = 0 To (Me.InnerList.Count - 1)
                objR = CType(Me.InnerList.Item(i), Reading)
                If (objR.BaseOrTier = eBaseOrTier And objR.TimeOfUse = eTou) Then
                    dblTotal = dblTotal + objR.Quantity
                End If
            Next
        End If

        Return (dblTotal)
    End Function

    Public Function Sum(ByVal eBaseOrTier As BaseOrTier, ByVal eSeason As Season) As Double
        Dim dblTotal As Double = 0
        Dim i As Integer
        Dim objR As Reading

        If (Me.InnerList.Count > 0) Then
            For i = 0 To (Me.InnerList.Count - 1)
                objR = CType(Me.InnerList.Item(i), Reading)
                If (objR.BaseOrTier = eBaseOrTier And objR.Season = eSeason) Then
                    dblTotal = dblTotal + objR.Quantity
                End If
            Next
        End If

        Return (dblTotal)
    End Function

    Public Function Sum(ByVal eBaseOrTier As BaseOrTier, ByVal eTou As TimeOfUse, ByVal eSeason As Season) As Double
        Dim dblTotal As Double = 0
        Dim i As Integer
        Dim objR As Reading

        If (Me.InnerList.Count > 0) Then
            For i = 0 To (Me.InnerList.Count - 1)
                objR = CType(Me.InnerList.Item(i), Reading)
                If (objR.BaseOrTier = eBaseOrTier And objR.TimeOfUse = eTou And objR.Season = eSeason) Then
                    dblTotal = dblTotal + objR.Quantity
                End If
            Next
        End If

        Return (dblTotal)
    End Function

    ''' <summary>
    ''' Sum the readings between and including the dateTime parameters
    ''' </summary>
    Public Function Sum(ByVal startDate As DateTime, ByVal endDate As DateTime) As Double
        Dim dblTotal As Double = 0
        Dim i As Integer
        Dim objR As Reading

        If (Me.InnerList.Count > 0) Then
            For i = 0 To (Me.InnerList.Count - 1)
                objR = CType(Me.InnerList.Item(i), Reading)
                If (objR.Datestamp >= startDate And objR.Datestamp <= endDate) Then
                    dblTotal = dblTotal + objR.Quantity
                End If
            Next
        End If

        Return (dblTotal)
    End Function


    Public Function EmptyReadingsExist(ByVal startDate As DateTime, ByVal endDate As DateTime) As Boolean
        Dim retval As Boolean = False
        Dim i As Integer
        Dim objR As Reading

        If (Me.InnerList.Count > 0) Then
            For i = 0 To (Me.InnerList.Count - 1)
                objR = CType(Me.InnerList.Item(i), Reading)
                If (objR.Datestamp >= startDate And objR.Datestamp <= endDate) Then
                    If (objR.IsEmpty) Then
                        retval = True
                        Exit For
                    End If
                End If
            Next
        End If

        Return (retval)
    End Function

    Public Function EmptyReadingsExist() As Boolean
        Dim retval As Boolean = False
        Dim i As Integer
        Dim objR As Reading

        If (Me.InnerList.Count > 0) Then
            For i = 0 To (Me.InnerList.Count - 1)
                objR = CType(Me.InnerList.Item(i), Reading)
                If (objR.IsEmpty) Then
                    retval = True
                    Exit For
                End If
            Next
        End If

        Return (retval)
    End Function

    Public Function Max() As Double
        Dim dblMax As Double = 0
        Dim i As Integer
        Dim objR As Reading

        If (Me.InnerList.Count > 0) Then
            For i = 0 To (Me.InnerList.Count - 1)
                objR = CType(Me.InnerList.Item(i), Reading)
                If (objR.Quantity > dblMax) Then
                    dblMax = objR.Quantity
                End If
            Next
        End If

        Return (dblMax)
    End Function


    Public Function MaxAtIndex() As Integer
        Dim dblMax As Double = 0
        Dim i As Integer
        Dim objR As Reading
        Dim maxIndex As Integer

        If (Me.InnerList.Count > 0) Then
            For i = 0 To (Me.InnerList.Count - 1)
                objR = CType(Me.InnerList.Item(i), Reading)
                If (objR.Quantity > dblMax) Then
                    dblMax = objR.Quantity
                    maxIndex = i
                End If
            Next
        End If

        Return (maxIndex)
    End Function

    Public Function Max(ByVal eTou As TimeOfUse) As Double
        Dim dblMax As Double = 0
        Dim i As Integer
        Dim objR As Reading

        If (Me.InnerList.Count > 0) Then
            For i = 0 To (Me.InnerList.Count - 1)
                objR = CType(Me.InnerList.Item(i), Reading)
                If (objR.TimeOfUse = eTou) Then
                    If (objR.Quantity > dblMax) Then
                        dblMax = objR.Quantity
                    End If
                End If
            Next
        End If

        Return (dblMax)
    End Function

    Public Function Max(ByVal eTou As TimeOfUse, ByVal eSeason As Season) As Double
        Dim dblMax As Double = 0
        Dim i As Integer
        Dim objR As Reading

        If (Me.InnerList.Count > 0) Then
            For i = 0 To (Me.InnerList.Count - 1)
                objR = CType(Me.InnerList.Item(i), Reading)
                If ((objR.TimeOfUse = eTou Or eTou = TimeOfUse.Undefined) And (objR.Season = eSeason Or eSeason = Season.Undefined)) Then
                    If (objR.Quantity > dblMax) Then
                        dblMax = objR.Quantity
                    End If
                End If
            Next
        End If

        Return (dblMax)
    End Function



    ''' <summary>
    ''' Max of grouped readings.  If there were multiple rate definitions at the time of binning, 
    ''' this will return an array that has max of all grouped readings.
    ''' </summary>
    ''' <param name="eTou"></param>
    ''' <param name="eSeason"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function MaxByGroupedReadings(ByVal eTou As TimeOfUse, ByVal eSeason As Season) As System.Collections.Generic.List(Of Double)
        Dim resultList As New System.Collections.Generic.List(Of Double)
        Dim i As Integer
        Dim r As Reading
        Dim max As Double = 0
        Dim groupNumber As Integer = -1
        Dim groupIndex As Integer = -1

        If (Me.InnerList.Count > 0) Then

            For i = 0 To (Me.InnerList.Count - 1)
                r = CType(Me.InnerList.Item(i), Reading)

                If (r.TimeOfUse = eTou And r.Season = eSeason) Then

                    If (groupNumber <> r.BinnedGroupNumber) Then
                        max = 0
                        resultList.Add(New Double())
                        groupIndex = groupIndex + 1
                        groupNumber = r.BinnedGroupNumber
                    End If

                    If (r.Quantity > max) Then
                        max = r.Quantity
                        resultList(groupIndex) = max
                    End If

                End If

            Next

            ' reverse the list so that the most recent max is at index 0
            resultList.Reverse()

        End If

        Return (resultList)
    End Function


    ''' <summary>
    ''' Get the maximum reading and optional datestamp by grouped readings.  See the related function named MaxByGroupedReadings.
    ''' </summary>
    ''' <param name="eTou"></param>
    ''' <param name="eSeason"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function GetMaxReadingDetailByGroupedReadings(ByVal eTou As TimeOfUse, ByVal eSeason As Season) As System.Collections.Generic.List(Of MaxReadingDetail)
        Dim resultList As New System.Collections.Generic.List(Of MaxReadingDetail)
        Dim i As Integer
        Dim r As Reading
        Dim max As Double = 0
        Dim groupNumber As Integer = -1
        Dim groupIndex As Integer = -1

        If (Me.InnerList.Count > 0) Then

            For i = 0 To (Me.InnerList.Count - 1)
                r = CType(Me.InnerList.Item(i), Reading)

                If (r.TimeOfUse = eTou And r.Season = eSeason) Then

                    If (groupNumber <> r.BinnedGroupNumber) Then
                        max = 0
                        resultList.Add(New MaxReadingDetail())
                        groupIndex = groupIndex + 1
                        groupNumber = r.BinnedGroupNumber
                    End If

                    If (r.Quantity > max) Then
                        max = r.Quantity
                        resultList(groupIndex).Quantity = max
                        resultList(groupIndex).Datestamp = r.Datestamp
                    End If

                End If

            Next

            ' reverse the list so that the most recent max is at index 0
            resultList.Reverse()

        End If

        Return (resultList)
    End Function


    ''' <summary>
    ''' Get the maximum reading with the optional datestamp.
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function GetMaxReadingDetail() As MaxReadingDetail
        Dim maxDetail As New MaxReadingDetail()
        Dim i As Integer
        Dim r As Reading

        If (Me.InnerList.Count > 0) Then
            For i = 0 To (Me.InnerList.Count - 1)
                r = CType(Me.InnerList.Item(i), Reading)
                If (r.Quantity > maxDetail.Quantity) Then
                    maxDetail.Quantity = r.Quantity
                    maxDetail.Datestamp = r.Datestamp
                End If
            Next
        End If

        Return (maxDetail)
    End Function

    ''' <summary>
    ''' Get the maximum reading with the optional datestamp. Overload.
    ''' </summary>
    ''' <param name="eTou"></param>
    ''' <param name="eSeason"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function GetMaxReadingDetail(ByVal eTou As TimeOfUse, ByVal eSeason As Season) As MaxReadingDetail
        Dim maxDetail As New MaxReadingDetail()
        Dim i As Integer
        Dim r As Reading

        If (Me.InnerList.Count > 0) Then
            For i = 0 To (Me.InnerList.Count - 1)
                r = CType(Me.InnerList.Item(i), Reading)
                If ((r.TimeOfUse = eTou Or eTou = TimeOfUse.Undefined) And (r.Season = eSeason Or eSeason = Season.Undefined)) Then
                    If (r.Quantity > maxDetail.Quantity) Then
                        maxDetail.Quantity = r.Quantity
                        maxDetail.Datestamp = r.Datestamp
                    End If
                End If
            Next
        End If

        Return (maxDetail)
    End Function

    ''' <summary>
    ''' This is used by NCD to get a special max readings value.  
    ''' The max reading that is not on an event day or on an event day in the event period.
    ''' </summary>
    ''' <param name="eTou"></param>
    ''' <param name="eSeason"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function GetMaxReadingDetailForNCD(ByVal eTou As TimeOfUse, ByVal eSeason As Season) As MaxReadingDetail
        Dim maxDetail As New MaxReadingDetail()
        Dim i As Integer
        Dim r As Reading

        If (Me.InnerList.Count > 0) Then
            For i = 0 To (Me.InnerList.Count - 1)
                r = CType(Me.InnerList.Item(i), Reading)
                If ((r.TimeOfUse = eTou Or eTou = TimeOfUse.Undefined) And (r.Season = eSeason Or eSeason = Season.Undefined)) Then

                    If ((Not r.OnEventDay) Or (r.OnEventDay AndAlso r.TimeOfUseModified)) Then

                        If (r.Quantity > maxDetail.Quantity) Then
                            maxDetail.Quantity = r.Quantity
                            maxDetail.Datestamp = r.Datestamp
                        End If

                    End If

                End If
            Next
        End If

        Return (maxDetail)
    End Function

    ''' <summary>
    ''' Set OnEventDays boolean in readings collection give a list of DayOfYear integers.
    ''' </summary>
    ''' <param name="eventDays"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function AssignEventDays(ByVal eventDays As List(Of Integer)) As Boolean
        Dim i As Integer
        Dim r As Reading

        If (Not eventDays Is Nothing) Then

            If (Me.InnerList.Count > 0) Then
                For i = 0 To (Me.InnerList.Count - 1)
                    r = CType(Me.InnerList.Item(i), Reading)

                    If (eventDays.Contains(r.Datestamp.DayOfYear)) Then
                        r.OnEventDay = True
                    End If

                Next
            End If

        End If

        Return (True)
    End Function

    ''' <summary>
    ''' Get a collection of usages from the readings that define the pre-modified TOU for the readings identified by TimeOfUseModified=true and matching passed-in tou.
    ''' </summary>
    ''' <param name="eTou"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function GetOriginalTOUModifiedUsagesFromReadings(ByVal eTou As TimeOfUse) As UsageCollection
        Dim usages As UsageCollection = Nothing
        Dim dblTotal As Double = 0
        Dim i As Integer
        Dim r As Reading

        If (Me.InnerList.Count > 0) Then
            For i = 0 To (Me.InnerList.Count - 1)
                r = CType(Me.InnerList.Item(i), Reading)
                If (r.TimeOfUseModified AndAlso r.TimeOfUse = eTou) Then
                    If (usages Is Nothing) Then
                        usages = New UsageCollection()
                    End If
                    usages.Combine(BaseOrTier.Undefined, r.TimeOfUseOriginal, r.Season, r.Quantity)
                End If
            Next
        End If

        Return (usages)
    End Function

    ''' <summary>
    ''' This will get capacity reserve usage collection, so we can get quantity, but also see origin of tou.
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function GetCapacityReserveUsageFromReadings() As UsageCollection
        Dim usages As UsageCollection = Nothing
        Dim dblTotal As Double = 0
        Dim i As Integer
        Dim r As Reading

        If (Me.InnerList.Count > 0) Then
            For i = 0 To (Me.InnerList.Count - 1)
                r = CType(Me.InnerList.Item(i), Reading)
                If (r.IsCapacityReserve) Then
                    If (usages Is Nothing) Then
                        usages = New UsageCollection()
                    End If
                    'usages.Combine(BaseOrTier.Undefined, TimeOfUse.CriticalPeak, Season.Undefined, r.CapacityReserveQuantity)
                    usages.Combine(r.BaseOrTier, r.TimeOfUse, r.Season, r.CapacityReserveQuantity)
                End If
            Next
        End If

        Return (usages)
    End Function


    Public Function DetermineIntervalOfReadings() As ReadingInterval
        Dim i As Integer
        Dim objFirstR As Reading = Nothing
        Dim objSecondR As Reading = Nothing
        Dim eReadingInterval As ReadingInterval = ReadingInterval.Daily
        Dim dblSpanMinutes As Double = 0.0

        If (Me.InnerList.Count >= 2) Then

            For i = 0 To 1
                If (i = 0) Then objFirstR = CType(Me.InnerList.Item(i), Reading)
                If (i = 1) Then
                    objSecondR = CType(Me.InnerList.Item(i), Reading)
                    dblSpanMinutes = objSecondR.Datestamp.Subtract(objFirstR.Datestamp).TotalMinutes

                    If (dblSpanMinutes = 0) Then
                        eReadingInterval = ReadingInterval.Daily    ' likely time of use with two TOU reads per day
                    ElseIf (dblSpanMinutes <= 5) Then 'CR 16664 May 2011 LC - 5 - mins
                        eReadingInterval = ReadingInterval.FiveMinute
                    ElseIf (dblSpanMinutes <= 15) Then
                        eReadingInterval = ReadingInterval.FifteenMinute
                    ElseIf (dblSpanMinutes <= 30) Then
                        eReadingInterval = ReadingInterval.HalfHourly
                    ElseIf (dblSpanMinutes <= 60) Then
                        eReadingInterval = ReadingInterval.Hourly
                    Else
                        eReadingInterval = ReadingInterval.Daily
                    End If

                    Exit For
                End If

            Next

        End If

        Return (eReadingInterval)
    End Function

    Public Function FindIndexOfReadingWithMonthAndDay(ByVal nMonth As Integer, ByVal nDay As Integer) As Integer
        Dim i As Integer
        Dim objR As Reading
        Dim bFound As Boolean

        If (Me.InnerList.Count > 0) Then
            For i = 0 To (Me.InnerList.Count - 1)
                objR = CType(Me.InnerList.Item(i), Reading)
                If (objR.Datestamp.Month = nMonth And objR.Datestamp.Day = nDay) Then
                    bFound = True
                    Exit For
                End If
            Next
        End If

        ' return a negative index when a valid index is not found;
        If (Not bFound) Then i = -1

        Return (i)
    End Function

    Public Function FindIndexOfReadingWithDate(ByVal dDate As Date) As Integer
        Dim i As Integer
        Dim objR As Reading
        Dim bFound As Boolean

        If (Me.InnerList.Count > 0) Then
            For i = 0 To (Me.InnerList.Count - 1)
                objR = CType(Me.InnerList.Item(i), Reading)
                If (objR.Datestamp = dDate) Then
                    bFound = True
                    Exit For
                End If
            Next
        End If

        ' return a negative index when a valid index is not found;
        If (Not bFound) Then i = -1

        Return (i)
    End Function

    Public Function ApplyScaleFactor(ByVal scaleFactor As Double) As Boolean
        Dim dblTotal As Double = 0
        Dim i As Integer
        Dim objR As Reading

        If (Me.InnerList.Count > 0) Then
            For i = 0 To (Me.InnerList.Count - 1)
                objR = CType(Me.InnerList.Item(i), Reading)
                objR.Quantity = objR.Quantity * scaleFactor
            Next
        End If

        Return (True)
    End Function

    Public Function ApplyScaleFactorToPositiveReadings(ByVal scaleFactor As Double) As Boolean
        Dim r As Reading

        If (Me.InnerList.Count > 0) Then
            For i As Integer = 0 To (Me.InnerList.Count - 1)
                r = CType(Me.InnerList.Item(i), Reading)
                If (r.Quantity > 0.0) Then
                    r.Quantity = r.Quantity * scaleFactor
                End If
            Next
        End If

        Return (True)
    End Function

    Public Function ApplyScaleFactorAndReturnTotal(ByVal scaleFactor As Double) As Double
        Dim dblTotal As Double = 0
        Dim dblQuantity As Double = 0
        Dim i As Integer
        Dim objR As Reading

        If (Me.InnerList.Count > 0) Then
            For i = 0 To (Me.InnerList.Count - 1)
                objR = CType(Me.InnerList.Item(i), Reading)
                dblQuantity = objR.Quantity - (objR.Quantity * scaleFactor)
                objR.Quantity = objR.Quantity * scaleFactor
                dblTotal = dblTotal + dblQuantity
            Next
        End If

        Return (dblTotal)
    End Function

    Public Function ApplyScaleFactorToPositiveReadingsAndReturnTotal(ByVal scaleFactor As Double) As Double
        Dim dblTotal As Double = 0
        Dim dblQuantity As Double = 0
        Dim objR As Reading

        If (Me.InnerList.Count > 0) Then
            For i As Integer = 0 To (Me.InnerList.Count - 1)
                objR = CType(Me.InnerList.Item(i), Reading)
                If (objR.Quantity > 0.0) Then
                    dblQuantity = objR.Quantity - (objR.Quantity * scaleFactor)
                    objR.Quantity = objR.Quantity * scaleFactor
                    dblTotal = dblTotal + dblQuantity
                End If
            Next
        End If

        Return (dblTotal)
    End Function

    Public Function ApplyScaleFactorAndReturnTotal(ByVal tou As TimeOfUse, ByVal scaleFactor As Double) As Double
        Dim dblTotal As Double = 0
        Dim dblQuantity As Double = 0
        Dim i As Integer
        Dim objR As Reading

        If (Me.InnerList.Count > 0) Then
            For i = 0 To (Me.InnerList.Count - 1)
                objR = CType(Me.InnerList.Item(i), Reading)
                If (objR.TimeOfUse = tou) Then
                    dblQuantity = objR.Quantity - (objR.Quantity * scaleFactor)
                    objR.Quantity = objR.Quantity * scaleFactor
                    dblTotal = dblTotal + dblQuantity
                End If
            Next
        End If

        Return (dblTotal)
    End Function

    Public Function ApplyScaleFactorToPositiveReadingsAndReturnTotal(ByVal tou As TimeOfUse, ByVal scaleFactor As Double) As Double
        Dim dblTotal As Double = 0
        Dim dblQuantity As Double = 0
        Dim i As Integer
        Dim objR As Reading

        If (Me.InnerList.Count > 0) Then
            For i = 0 To (Me.InnerList.Count - 1)
                objR = CType(Me.InnerList.Item(i), Reading)
                If (objR.TimeOfUse = tou AndAlso objR.Quantity > 0.0) Then
                    dblQuantity = objR.Quantity - (objR.Quantity * scaleFactor)
                    objR.Quantity = objR.Quantity * scaleFactor
                    dblTotal = dblTotal + dblQuantity
                End If
            Next
        End If

        Return (dblTotal)
    End Function


    Public Function Spread(ByVal tou As TimeOfUse, ByVal totalUsage As Double) As Boolean
        Dim i As Integer
        Dim r As Reading
        Dim count As Integer = 0

        If (Me.InnerList.Count > 0) Then

            For i = 0 To (Me.InnerList.Count - 1)
                ' Get count of tou
                r = CType(Me.InnerList.Item(i), Reading)
                If (r.TimeOfUse = tou) Then
                    count = count + 1
                End If
            Next

            If (count > 0) Then
                For i = 0 To (Me.InnerList.Count - 1)
                    r = CType(Me.InnerList.Item(i), Reading)
                    If (r.TimeOfUse = tou) Then
                        r.Quantity = r.Quantity + (totalUsage / CDbl(count))
                    End If
                Next
            End If

        End If

        Return (True)
    End Function


    Public Function ApplyScaleFactorToMaxPositiveReadingsAndPin(ByVal scaleFactor As Double) As Boolean
        Dim dblTotal As Double = 0
        Dim i As Integer
        Dim r As Reading
        Dim maxAtIndex As Integer
        Dim maxQuantity As Double

        If (Me.InnerList.Count > 0) Then

            maxAtIndex = Me.MaxAtIndex()
            maxQuantity = CType(Me.InnerList.Item(maxAtIndex), Reading).Quantity * scaleFactor

            For i = 0 To (Me.InnerList.Count - 1)
                r = CType(Me.InnerList.Item(i), Reading)
                If (r.Quantity > 0.0 AndAlso r.Quantity > maxQuantity) Then
                    r.Quantity = maxQuantity
                End If
            Next

        End If

        Return (True)
    End Function

    Public Function SumCost() As Double
        Dim dblTotalCost As Double = 0
        Dim i, nCount As Integer

        nCount = Me.InnerList.Count

        If (nCount > 0) Then
            For i = 0 To (Me.InnerList.Count - 1)
                dblTotalCost = dblTotalCost + CType(Me.InnerList.Item(i), Reading).TotalCost
            Next
        Else
            dblTotalCost = 0.0
        End If

        Return (dblTotalCost)
    End Function


    Public Function GetAvgHourlyReadingsFor(ByVal eDayOfWeekType As Enums.DayType) As ReadingCollection
        Dim objReadings, objTemp As ReadingCollection
        Dim objR As Reading
        Dim i, nHour As Integer
        Dim bFound As Boolean = False
        Dim dblTotal(24) As Double
        Dim nCount(24) As Integer
        Dim nTopIndex As Integer = 23
        Dim dblAvg As Double
        Dim bMarkEmpty As Boolean

        objReadings = New ReadingCollection

        If (Me.InnerList.Count > 0) Then
            objTemp = New ReadingCollection

            For i = 0 To (Me.InnerList.Count - 1)
                objR = CType(Me.InnerList.Item(i), Reading)

                If (eDayOfWeekType = DayType.Weekend) Then
                    If (objR.Datestamp.DayOfWeek = DayOfWeek.Saturday Or objR.Datestamp.DayOfWeek = DayOfWeek.Sunday) Then
                        objTemp.Add(New Reading(objR.BaseOrTier, objR.TimeOfUse, objR.Season, objR.Datestamp, objR.Quantity, objR.DayType, objR.IsEmpty))
                    End If
                Else
                    If (objR.Datestamp.DayOfWeek <> DayOfWeek.Saturday And objR.Datestamp.DayOfWeek <> DayOfWeek.Sunday) Then
                        objTemp.Add(New Reading(objR.BaseOrTier, objR.TimeOfUse, objR.Season, objR.Datestamp, objR.Quantity, objR.DayType, objR.IsEmpty))
                    End If
                End If

            Next

            If (objTemp.Count > 0) Then
                For i = 0 To (objTemp.Count - 1)
                    objR = CType(objTemp.Item(i), Reading)
                    ' Be sure not to include empty readings as part of average
                    If (Not objR.IsEmpty) Then
                        nHour = objR.Datestamp.Hour
                        dblTotal(nHour) = dblTotal(nHour) + objR.Quantity
                        nCount(nHour) = nCount(nHour) + 1
                    End If
                Next

                For i = 0 To nTopIndex
                    If (nCount(i) <> 0) Then
                        dblAvg = dblTotal(i) / nCount(i)
                        bMarkEmpty = False
                    Else
                        dblAvg = 0
                        bMarkEmpty = True
                    End If
                    objReadings.Add(New Reading(Enums.BaseOrTier.TotalServiceUse, Enums.TimeOfUse.Undefined, Enums.Season.Undefined, DateTime.Now(), dblAvg, Enums.DayType.Weekday, bMarkEmpty))

                Next
            End If

        End If

        Return (objReadings)
    End Function

    Public Function GetAvgHalfHourlyReadingsFor(ByVal eDayOfWeekType As Enums.DayType) As ReadingCollection
        Dim objReadings, objTemp As ReadingCollection
        Dim objR As Reading
        Dim i, nIndex, nHour, nMinute As Integer
        Dim bFound As Boolean = False
        Dim dblTotal(48) As Double
        Dim nCount(48) As Integer
        Dim nTopIndex As Integer = 47
        Dim dblAvg As Double
        Dim bMarkEmpty As Boolean

        objReadings = New ReadingCollection

        If (Me.InnerList.Count > 0) Then
            objTemp = New ReadingCollection

            For i = 0 To (Me.InnerList.Count - 1)
                objR = CType(Me.InnerList.Item(i), Reading)

                If (eDayOfWeekType = DayType.Weekend) Then
                    If (objR.Datestamp.DayOfWeek = DayOfWeek.Saturday Or objR.Datestamp.DayOfWeek = DayOfWeek.Sunday) Then
                        objTemp.Add(New Reading(objR.BaseOrTier, objR.TimeOfUse, objR.Season, objR.Datestamp, objR.Quantity, objR.DayType, objR.IsEmpty))
                    End If
                Else
                    If (objR.Datestamp.DayOfWeek <> DayOfWeek.Saturday And objR.Datestamp.DayOfWeek <> DayOfWeek.Sunday) Then
                        objTemp.Add(New Reading(objR.BaseOrTier, objR.TimeOfUse, objR.Season, objR.Datestamp, objR.Quantity, objR.DayType, objR.IsEmpty))
                    End If
                End If

            Next

            If (objTemp.Count > 0) Then
                For i = 0 To (objTemp.Count - 1)
                    objR = CType(objTemp.Item(i), Reading)
                    ' Be sure not to include empty readings as part of average
                    If (Not objR.IsEmpty) Then
                        nHour = objR.Datestamp.Hour
                        nMinute = objR.Datestamp.Minute
                        nIndex = (nHour * 2) + CType(nMinute / 30, Integer)
                        dblTotal(nIndex) = dblTotal(nIndex) + objR.Quantity
                        nCount(nIndex) = nCount(nIndex) + 1
                    End If
                Next

                For i = 0 To nTopIndex
                    If (nCount(i) <> 0) Then
                        dblAvg = dblTotal(i) / nCount(i)
                        bMarkEmpty = False
                    Else
                        dblAvg = 0
                        bMarkEmpty = True
                    End If
                    objReadings.Add(New Reading(Enums.BaseOrTier.TotalServiceUse, Enums.TimeOfUse.Undefined, Enums.Season.Undefined, DateTime.Now(), dblAvg, Enums.DayType.Weekday, bMarkEmpty))
                Next
            End If

        End If

        Return (objReadings)
    End Function

    Public Function GetAvg15MinuteReadingsFor(ByVal eDayOfWeekType As Enums.DayType) As ReadingCollection
        Dim objReadings, objTemp As ReadingCollection
        Dim objR As Reading
        Dim i, nIndex, nHour, nMinute As Integer
        Dim bFound As Boolean = False
        Dim dblTotal(96) As Double
        Dim nCount(96) As Integer
        Dim nTopIndex As Integer = 95
        Dim dblAvg As Double
        Dim bMarkEmpty As Boolean

        objReadings = New ReadingCollection

        If (Me.InnerList.Count > 0) Then
            objTemp = New ReadingCollection

            For i = 0 To (Me.InnerList.Count - 1)
                objR = CType(Me.InnerList.Item(i), Reading)

                If (eDayOfWeekType = DayType.Weekend) Then
                    If (objR.Datestamp.DayOfWeek = DayOfWeek.Saturday Or objR.Datestamp.DayOfWeek = DayOfWeek.Sunday) Then
                        objTemp.Add(New Reading(objR.BaseOrTier, objR.TimeOfUse, objR.Season, objR.Datestamp, objR.Quantity, objR.DayType, objR.IsEmpty))
                    End If
                Else
                    If (objR.Datestamp.DayOfWeek <> DayOfWeek.Saturday And objR.Datestamp.DayOfWeek <> DayOfWeek.Sunday) Then
                        objTemp.Add(New Reading(objR.BaseOrTier, objR.TimeOfUse, objR.Season, objR.Datestamp, objR.Quantity, objR.DayType, objR.IsEmpty))
                    End If
                End If

            Next

            If (objTemp.Count > 0) Then
                For i = 0 To (objTemp.Count - 1)
                    objR = CType(objTemp.Item(i), Reading)
                    ' Be sure not to include empty readings as part of average
                    If (Not objR.IsEmpty) Then
                        nHour = objR.Datestamp.Hour
                        nMinute = objR.Datestamp.Minute
                        nIndex = (nHour * 4) + CType(nMinute / 15, Integer)
                        dblTotal(nIndex) = dblTotal(nIndex) + objR.Quantity
                        nCount(nIndex) = nCount(nIndex) + 1
                    End If
                Next

                For i = 0 To nTopIndex
                    If (nCount(i) <> 0) Then
                        dblAvg = dblTotal(i) / nCount(i)
                        bMarkEmpty = False
                    Else
                        dblAvg = 0
                        bMarkEmpty = True
                    End If
                    objReadings.Add(New Reading(Enums.BaseOrTier.TotalServiceUse, Enums.TimeOfUse.Undefined, Enums.Season.Undefined, DateTime.Now(), dblAvg, Enums.DayType.Weekday, bMarkEmpty))
                Next
            End If

        End If

        Return (objReadings)
    End Function

    Public Function GetAvgFiveMinuteReadingsFor(ByVal eDayOfWeekType As Enums.DayType) As ReadingCollection
        Dim objReadings, objTemp As ReadingCollection
        Dim objR As Reading
        Dim i, nIndex, nHour, nMinute As Integer
        Dim bFound As Boolean = False
        Dim dblTotal(288) As Double
        Dim nCount(288) As Integer
        Dim nTopIndex As Integer = 287
        Dim dblAvg As Double
        Dim bMarkEmpty As Boolean

        objReadings = New ReadingCollection

        If (Me.InnerList.Count > 0) Then
            objTemp = New ReadingCollection

            For i = 0 To (Me.InnerList.Count - 1)
                objR = CType(Me.InnerList.Item(i), Reading)

                If (eDayOfWeekType = DayType.Weekend) Then
                    If (objR.Datestamp.DayOfWeek = DayOfWeek.Saturday Or objR.Datestamp.DayOfWeek = DayOfWeek.Sunday) Then
                        objTemp.Add(New Reading(objR.BaseOrTier, objR.TimeOfUse, objR.Season, objR.Datestamp, objR.Quantity, objR.DayType, objR.IsEmpty))
                    End If
                Else
                    If (objR.Datestamp.DayOfWeek <> DayOfWeek.Saturday And objR.Datestamp.DayOfWeek <> DayOfWeek.Sunday) Then
                        objTemp.Add(New Reading(objR.BaseOrTier, objR.TimeOfUse, objR.Season, objR.Datestamp, objR.Quantity, objR.DayType, objR.IsEmpty))
                    End If
                End If

            Next

            If (objTemp.Count > 0) Then
                For i = 0 To (objTemp.Count - 1)
                    objR = CType(objTemp.Item(i), Reading)
                    ' Be sure not to include empty readings as part of average
                    If (Not objR.IsEmpty) Then
                        nHour = objR.Datestamp.Hour
                        nMinute = objR.Datestamp.Minute
                        nIndex = (nHour * 12) + CType(nMinute / 5, Integer)
                        dblTotal(nIndex) = dblTotal(nIndex) + objR.Quantity
                        nCount(nIndex) = nCount(nIndex) + 1
                    End If
                Next

                For i = 0 To nTopIndex
                    If (nCount(i) <> 0) Then
                        dblAvg = dblTotal(i) / nCount(i)
                        bMarkEmpty = False
                    Else
                        dblAvg = 0
                        bMarkEmpty = True
                    End If
                    objReadings.Add(New Reading(Enums.BaseOrTier.TotalServiceUse, Enums.TimeOfUse.Undefined, Enums.Season.Undefined, DateTime.Now(), dblAvg, Enums.DayType.Weekday, bMarkEmpty))
                Next
            End If

        End If

        Return (objReadings)
    End Function




    Public Function GetReadingsForFullDateRange() As ReadingCollection
        Dim objReadings As New ReadingCollection
        Dim objR As Reading
        Dim i As Integer

        If (Me.InnerList.Count > 0) Then
            For i = 0 To (Me.InnerList.Count - 1)
                objR = CType(Me.InnerList.Item(i), Reading)
                objReadings.Add(New Reading(objR.BaseOrTier, objR.TimeOfUse, objR.Season, objR.Datestamp, objR.Quantity, objR.DayType, objR.IsEmpty))
            Next
        End If

        Return (objReadings)
    End Function

    Public Function FillMissingHourlyReadingsWithAverage(ByVal dtStartDate As DateTime, ByVal dtEndDate As DateTime, ByVal avgWeekday As ReadingCollection, ByVal avgWeekend As ReadingCollection, ByVal isEmpty As Boolean) As Boolean
        Dim bRetVal As Boolean = True
        Dim i, ii, readingIndex, nCount, nMaxDays As Integer
        Dim objReading As Reading
        Dim dtTemp As DateTime

        If (Me.InnerList.Count > 0) Then

            ' 24 Hours for each Day
            nCount = Me.InnerList.Count

            nMaxDays = dtEndDate.Subtract(dtStartDate).Days + 1
            For i = 0 To (nMaxDays - 1)
                dtTemp = dtStartDate.AddDays(i)

                For ii = 0 To 23
                    readingIndex = (i * 24) + ii

                    If (readingIndex < nCount) Then
                        objReading = CType(Me.InnerList.Item(readingIndex), Reading)

                        If (dtTemp.Day <> objReading.Datestamp.Day Or dtTemp.Month <> objReading.Datestamp.Month Or _
                        dtTemp.AddHours(ii).Hour <> objReading.Datestamp.Hour) Then
                            ' Day or hour does not match, so insert at this index
                            If (objReading.Datestamp.DayOfWeek = DayOfWeek.Saturday Or objReading.Datestamp.DayOfWeek = DayOfWeek.Sunday) Then
                                Me.InnerList.Insert(readingIndex, New Reading(avgWeekend(ii).BaseOrTier, avgWeekend(ii).TimeOfUse, avgWeekend(ii).Season, dtTemp.AddHours(ii), avgWeekend(ii).Quantity, avgWeekend(ii).DayType, isEmpty))
                            Else
                                Me.InnerList.Insert(readingIndex, New Reading(avgWeekday(ii).BaseOrTier, avgWeekday(ii).TimeOfUse, avgWeekday(ii).Season, dtTemp.AddHours(ii), avgWeekday(ii).Quantity, avgWeekday(ii).DayType, isEmpty))
                            End If
                            ' Insert into inner list moves the list up one notch, hence increase the count
                            nCount = nCount + 1
                        End If
                    Else

                        If (dtTemp.DayOfWeek = DayOfWeek.Saturday Or dtTemp.DayOfWeek = DayOfWeek.Sunday) Then
                            Me.InnerList.Add(New Reading(avgWeekend(ii).BaseOrTier, avgWeekend(ii).TimeOfUse, avgWeekend(ii).Season, dtTemp.AddHours(ii), avgWeekend(ii).Quantity, avgWeekend(ii).DayType, isEmpty))
                        Else
                            Me.InnerList.Add(New Reading(avgWeekday(ii).BaseOrTier, avgWeekday(ii).TimeOfUse, avgWeekday(ii).Season, dtTemp.AddHours(ii), avgWeekday(ii).Quantity, avgWeekday(ii).DayType, isEmpty))
                        End If

                        nCount = nCount + 1
                    End If

                Next

            Next

        End If

        Return (bRetVal)
    End Function

    Public Function FillMissingIntervalReadingsWithAverage(ByVal dtStartDate As DateTime, ByVal dtEndDate As DateTime, ByVal avgWeekday As ReadingCollection, ByVal avgWeekend As ReadingCollection, ByVal isEmpty As Boolean, ByVal eInterval As ReadingInterval) As Boolean
        Dim bRetVal As Boolean = True
        Dim i, ii, iii, readingIndex, nCount, nMaxDays As Integer
        Dim objReading As Reading
        Dim dtTemp As DateTime

        If (Me.InnerList.Count > 0) Then

            Select Case eInterval

                Case ReadingInterval.Hourly
                    ' 24 Hours for each Day
                    nCount = Me.InnerList.Count

                    nMaxDays = dtEndDate.Subtract(dtStartDate).Days + 1
                    For i = 0 To (nMaxDays - 1)
                        dtTemp = dtStartDate.AddDays(i)

                        For ii = 0 To 23
                            readingIndex = (i * 24) + ii

                            If (readingIndex < nCount) Then
                                objReading = CType(Me.InnerList.Item(readingIndex), Reading)

                                If (dtTemp.Day <> objReading.Datestamp.Day Or dtTemp.Month <> objReading.Datestamp.Month Or _
                                dtTemp.AddHours(ii).Hour <> objReading.Datestamp.Hour) Then
                                    ' Day or hour does not match, so insert at this index
                                    If (objReading.Datestamp.DayOfWeek = DayOfWeek.Saturday Or objReading.Datestamp.DayOfWeek = DayOfWeek.Sunday) Then
                                        'Me.InnerList.Insert(readingIndex, New Reading(isEmpty, AMREditCode.Other, avgWeekend(ii).AMRTou, avgWeekend(ii).AMRUom, avgWeekend(ii).AMRSeason, dtTemp.AddHours(ii), avgWeekend(ii).Reading))
                                        Me.InnerList.Insert(readingIndex, New Reading(avgWeekend(ii).BaseOrTier, avgWeekend(ii).TimeOfUse, avgWeekend(ii).Season, dtTemp.AddHours(ii), avgWeekend(ii).Quantity, avgWeekend(ii).DayType, isEmpty))
                                    Else
                                        'Me.InnerList.Insert(readingIndex, New EntAMRReading(isEmpty, AMREditCode.Other, avgWeekday(ii).AMRTou, avgWeekday(ii).AMRUom, avgWeekday(ii).AMRSeason, dtTemp.AddHours(ii), avgWeekday(ii).Reading))
                                        Me.InnerList.Insert(readingIndex, New Reading(avgWeekday(ii).BaseOrTier, avgWeekday(ii).TimeOfUse, avgWeekday(ii).Season, dtTemp.AddHours(ii), avgWeekday(ii).Quantity, avgWeekday(ii).DayType, isEmpty))
                                    End If
                                    ' Insert into inner list moves the list up one notch, hence increase the count
                                    nCount = nCount + 1
                                End If
                            Else

                                If (dtTemp.DayOfWeek = DayOfWeek.Saturday Or dtTemp.DayOfWeek = DayOfWeek.Sunday) Then
                                    'Me.InnerList.Add(New EntAMRReading(isEmpty, AMREditCode.Other, avgWeekend(ii).AMRTou, avgWeekend(ii).AMRUom, avgWeekend(ii).AMRSeason, dtTemp.AddHours(ii), avgWeekend(ii).Reading))
                                    Me.InnerList.Add(New Reading(avgWeekend(ii).BaseOrTier, avgWeekend(ii).TimeOfUse, avgWeekend(ii).Season, dtTemp.AddHours(ii), avgWeekend(ii).Quantity, avgWeekend(ii).DayType, isEmpty))
                                Else
                                    'Me.InnerList.Add(New EntAMRReading(isEmpty, AMREditCode.Other, avgWeekday(ii).AMRTou, avgWeekday(ii).AMRUom, avgWeekday(ii).AMRSeason, dtTemp.AddHours(ii), avgWeekday(ii).Reading))
                                    Me.InnerList.Add(New Reading(avgWeekday(ii).BaseOrTier, avgWeekday(ii).TimeOfUse, avgWeekday(ii).Season, dtTemp.AddHours(ii), avgWeekday(ii).Quantity, avgWeekday(ii).DayType, isEmpty))
                                End If

                                nCount = nCount + 1
                            End If

                        Next

                    Next

                Case ReadingInterval.FifteenMinute
                    ' 96 fifteen minute reads each Day; 4 each hour
                    nCount = Me.InnerList.Count

                    nMaxDays = dtEndDate.Subtract(dtStartDate).Days + 1
                    For i = 0 To (nMaxDays - 1)
                        dtTemp = dtStartDate.AddDays(i)

                        For ii = 0 To 23

                            For iii = 0 To 3                      ' four reads per hour

                                readingIndex = (i * 96) + ((ii * 4) + iii)

                                If (readingIndex < nCount) Then
                                    objReading = CType(Me.InnerList.Item(readingIndex), Reading)

                                    If (dtTemp.Day <> objReading.Datestamp.Day Or _
                                        dtTemp.Month <> objReading.Datestamp.Month Or _
                                    dtTemp.AddHours(ii).Hour <> objReading.Datestamp.Hour Or _
                                    dtTemp.AddHours(ii).AddMinutes(iii * 15).Minute <> objReading.Datestamp.Minute) Then
                                        ' Day, hour, or 15-minute do not match, so insert at this index
                                        If (objReading.Datestamp.DayOfWeek = DayOfWeek.Saturday Or objReading.Datestamp.DayOfWeek = DayOfWeek.Sunday) Then
                                            'Me.InnerList.Insert(readingIndex, New EntAMRReading(isEmpty, AMREditCode.Other, avgWeekend(((ii * 4) + iii)).AMRTou, avgWeekend(((ii * 4) + iii)).AMRUom, avgWeekend(((ii * 4) + iii)).AMRSeason, dtTemp.AddHours(ii).AddMinutes(iii * 15), avgWeekend(((ii * 4) + iii)).Reading))
                                            Me.InnerList.Insert(readingIndex, New Reading(avgWeekend(((ii * 4) + iii)).BaseOrTier, avgWeekend(((ii * 4) + iii)).TimeOfUse, avgWeekend(((ii * 4) + iii)).Season, dtTemp.AddHours(ii).AddMinutes(iii * 15), avgWeekend(((ii * 4) + iii)).Quantity, avgWeekend(((ii * 4) + iii)).DayType, isEmpty))
                                        Else
                                            'Me.InnerList.Insert(readingIndex, New EntAMRReading(isEmpty, AMREditCode.Other, avgWeekday(((ii * 4) + iii)).AMRTou, avgWeekday(((ii * 4) + iii)).AMRUom, avgWeekday(((ii * 4) + iii)).AMRSeason, dtTemp.AddHours(ii).AddMinutes(iii * 15), avgWeekday(((ii * 4) + iii)).Reading))
                                            Me.InnerList.Insert(readingIndex, New Reading(avgWeekday(((ii * 4) + iii)).BaseOrTier, avgWeekday(((ii * 4) + iii)).TimeOfUse, avgWeekday(((ii * 4) + iii)).Season, dtTemp.AddHours(ii).AddMinutes(iii * 15), avgWeekday(((ii * 4) + iii)).Quantity, avgWeekday(((ii * 4) + iii)).DayType, isEmpty))
                                        End If
                                        ' Insert into inner list moves the list up one notch, hence increase the count
                                        nCount = nCount + 1
                                    End If
                                Else
                                    ' over the top, add to end
                                    If (dtTemp.DayOfWeek = DayOfWeek.Saturday Or dtTemp.DayOfWeek = DayOfWeek.Sunday) Then
                                        ' Me.InnerList.Add(New EntAMRReading(isEmpty, AMREditCode.Other, avgWeekend(((ii * 4) + iii)).AMRTou, avgWeekend(((ii * 4) + iii)).AMRUom, avgWeekend(((ii * 4) + iii)).AMRSeason, dtTemp.AddHours(ii).AddMinutes(iii * 15), avgWeekend(((ii * 4) + iii)).Reading))
                                        Me.InnerList.Add(New Reading(avgWeekend(((ii * 4) + iii)).BaseOrTier, avgWeekend(((ii * 4) + iii)).TimeOfUse, avgWeekend(((ii * 4) + iii)).Season, dtTemp.AddHours(ii).AddMinutes(iii * 15), avgWeekend(((ii * 4) + iii)).Quantity, avgWeekend(((ii * 4) + iii)).DayType, isEmpty))
                                    Else
                                        'Me.InnerList.Add(New EntAMRReading(isEmpty, AMREditCode.Other, avgWeekday(((ii * 4) + iii)).AMRTou, avgWeekday(((ii * 4) + iii)).AMRUom, avgWeekday(((ii * 4) + iii)).AMRSeason, dtTemp.AddHours(ii).AddMinutes(iii * 15), avgWeekday(((ii * 4) + iii)).Reading))
                                        Me.InnerList.Add(New Reading(avgWeekday(((ii * 4) + iii)).BaseOrTier, avgWeekday(((ii * 4) + iii)).TimeOfUse, avgWeekday(((ii * 4) + iii)).Season, dtTemp.AddHours(ii).AddMinutes(iii * 15), avgWeekday(((ii * 4) + iii)).Quantity, avgWeekday(((ii * 4) + iii)).DayType, isEmpty))
                                    End If

                                    nCount = nCount + 1
                                End If

                            Next

                        Next

                    Next
                Case ReadingInterval.HalfHourly 'Foreman 12363 Sept 2009 LC - fill missing reading for 30 mins amr reading 
                    ' 48 30 minute reads each Day; 2 each hour
                    nCount = Me.InnerList.Count

                    nMaxDays = dtEndDate.Subtract(dtStartDate).Days + 1
                    For i = 0 To (nMaxDays - 1)
                        dtTemp = dtStartDate.AddDays(i)

                        For ii = 0 To 23

                            For iii = 0 To 1                      ' 2 reads per hour

                                readingIndex = (i * 48) + ((ii * 2) + iii)

                                If (readingIndex < nCount) Then
                                    objReading = CType(Me.InnerList.Item(readingIndex), Reading)

                                    If (dtTemp.Day <> objReading.Datestamp.Day Or _
                                        dtTemp.Month <> objReading.Datestamp.Month Or _
                                    dtTemp.AddHours(ii).Hour <> objReading.Datestamp.Hour Or _
                                    dtTemp.AddHours(ii).AddMinutes(iii * 30).Minute <> objReading.Datestamp.Minute) Then
                                        ' Day, hour, or 30-minute do not match, so insert at this index
                                        If (objReading.Datestamp.DayOfWeek = DayOfWeek.Saturday Or objReading.Datestamp.DayOfWeek = DayOfWeek.Sunday) Then
                                            'Me.InnerList.Insert(readingIndex, New EntAMRReading(isEmpty, AMREditCode.Other, avgWeekend(((ii * 2) + iii)).AMRTou, avgWeekend(((ii * 2) + iii)).AMRUom, avgWeekend(((ii * 2) + iii)).AMRSeason, dtTemp.AddHours(ii).AddMinutes(iii * 30), avgWeekend(((ii * 2) + iii)).Reading))
                                            Me.InnerList.Insert(readingIndex, New Reading(avgWeekend(((ii * 2) + iii)).BaseOrTier, avgWeekend(((ii * 2) + iii)).TimeOfUse, avgWeekend(((ii * 2) + iii)).Season, dtTemp.AddHours(ii).AddMinutes(iii * 30), avgWeekend(((ii * 2) + iii)).Quantity, avgWeekend(((ii * 2) + iii)).DayType, isEmpty))
                                        Else
                                            ' Me.InnerList.Insert(readingIndex, New EntAMRReading(isEmpty, AMREditCode.Other, avgWeekday(((ii * 2) + iii)).AMRTou, avgWeekday(((ii * 2) + iii)).AMRUom, avgWeekday(((ii * 2) + iii)).AMRSeason, dtTemp.AddHours(ii).AddMinutes(iii * 30), avgWeekday(((ii * 2) + iii)).Reading))
                                            Me.InnerList.Insert(readingIndex, New Reading(avgWeekday(((ii * 2) + iii)).BaseOrTier, avgWeekday(((ii * 2) + iii)).TimeOfUse, avgWeekday(((ii * 2) + iii)).Season, dtTemp.AddHours(ii).AddMinutes(iii * 30), avgWeekday(((ii * 2) + iii)).Quantity, avgWeekday(((ii * 2) + iii)).DayType, isEmpty))
                                        End If
                                        ' Insert into inner list moves the list up one notch, hence increase the count
                                        nCount = nCount + 1
                                    End If
                                Else
                                    ' over the top, add to end
                                    If (dtTemp.DayOfWeek = DayOfWeek.Saturday Or dtTemp.DayOfWeek = DayOfWeek.Sunday) Then
                                        'Me.InnerList.Add(New EntAMRReading(isEmpty, AMREditCode.Other, avgWeekend(((ii * 2) + iii)).AMRTou, avgWeekend(((ii * 2) + iii)).AMRUom, avgWeekend(((ii * 2) + iii)).AMRSeason, dtTemp.AddHours(ii).AddMinutes(iii * 30), avgWeekend(((ii * 2) + iii)).Reading))
                                        Me.InnerList.Add(New Reading(avgWeekend(((ii * 2) + iii)).BaseOrTier, avgWeekend(((ii * 2) + iii)).TimeOfUse, avgWeekend(((ii * 2) + iii)).Season, dtTemp.AddHours(ii).AddMinutes(iii * 30), avgWeekend(((ii * 2) + iii)).Quantity, avgWeekend(((ii * 2) + iii)).DayType, isEmpty))
                                    Else
                                        'Me.InnerList.Add(New EntAMRReading(isEmpty, AMREditCode.Other, avgWeekday(((ii * 2) + iii)).AMRTou, avgWeekday(((ii * 2) + iii)).AMRUom, avgWeekday(((ii * 2) + iii)).AMRSeason, dtTemp.AddHours(ii).AddMinutes(iii * 30), avgWeekday(((ii * 2) + iii)).Reading))
                                        Me.InnerList.Add(New Reading(avgWeekday(((ii * 2) + iii)).BaseOrTier, avgWeekday(((ii * 2) + iii)).TimeOfUse, avgWeekday(((ii * 2) + iii)).Season, dtTemp.AddHours(ii).AddMinutes(iii * 30), avgWeekday(((ii * 2) + iii)).Quantity, avgWeekday(((ii * 2) + iii)).DayType, isEmpty))
                                    End If

                                    nCount = nCount + 1
                                End If

                            Next

                        Next

                    Next
                Case ReadingInterval.FiveMinute 'CR 16664 May 2011 LC - fill missing 5-mins reading
                    ' 288 5 minute reads each Day; 12 each hour
                    nCount = Me.InnerList.Count

                    nMaxDays = dtEndDate.Subtract(dtStartDate).Days + 1
                    For i = 0 To (nMaxDays - 1)
                        dtTemp = dtStartDate.AddDays(i)

                        For ii = 0 To 23

                            For iii = 0 To 11                      ' 12 reads per hour

                                readingIndex = (i * 288) + ((ii * 12) + iii)

                                If (readingIndex < nCount) Then
                                    objReading = CType(Me.InnerList.Item(readingIndex), Reading)

                                    If (dtTemp.Day <> objReading.Datestamp.Day Or _
                                        dtTemp.Month <> objReading.Datestamp.Month Or _
                                    dtTemp.AddHours(ii).Hour <> objReading.Datestamp.Hour Or _
                                    dtTemp.AddHours(ii).AddMinutes(iii * 5).Minute <> objReading.Datestamp.Minute) Then
                                        ' Day, hour, or 30-minute do not match, so insert at this index
                                        If (objReading.Datestamp.DayOfWeek = DayOfWeek.Saturday Or objReading.Datestamp.DayOfWeek = DayOfWeek.Sunday) Then
                                            'Me.InnerList.Insert(readingIndex, New EntAMRReading(isEmpty, AMREditCode.Other, avgWeekend(((ii * 12) + iii)).AMRTou, avgWeekend(((ii * 12) + iii)).AMRUom, avgWeekend(((ii * 12) + iii)).AMRSeason, dtTemp.AddHours(ii).AddMinutes(iii * 5), avgWeekend(((ii * 12) + iii)).Reading))
                                            Me.InnerList.Insert(readingIndex, New Reading(avgWeekend(((ii * 12) + iii)).BaseOrTier, avgWeekend(((ii * 12) + iii)).TimeOfUse, avgWeekend(((ii * 12) + iii)).Season, dtTemp.AddHours(ii).AddMinutes(iii * 5), avgWeekend(((ii * 12) + iii)).Quantity, avgWeekend(((ii * 12) + iii)).DayType, isEmpty))
                                        Else
                                            'Me.InnerList.Insert(readingIndex, New EntAMRReading(isEmpty, AMREditCode.Other, avgWeekday(((ii * 12) + iii)).AMRTou, avgWeekday(((ii * 12) + iii)).AMRUom, avgWeekday(((ii * 12) + iii)).AMRSeason, dtTemp.AddHours(ii).AddMinutes(iii * 5), avgWeekday(((ii * 12) + iii)).Reading))
                                            Me.InnerList.Insert(readingIndex, New Reading(avgWeekday(((ii * 12) + iii)).BaseOrTier, avgWeekday(((ii * 12) + iii)).TimeOfUse, avgWeekday(((ii * 12) + iii)).Season, dtTemp.AddHours(ii).AddMinutes(iii * 5), avgWeekday(((ii * 12) + iii)).Quantity, avgWeekday(((ii * 12) + iii)).DayType, isEmpty))
                                        End If
                                        ' Insert into inner list moves the list up one notch, hence increase the count
                                        nCount = nCount + 1
                                    End If
                                Else
                                    ' over the top, add to end
                                    If (dtTemp.DayOfWeek = DayOfWeek.Saturday Or dtTemp.DayOfWeek = DayOfWeek.Sunday) Then
                                        'Me.InnerList.Add(New EntAMRReading(isEmpty, AMREditCode.Other, avgWeekend(((ii * 12) + iii)).AMRTou, avgWeekend(((ii * 12) + iii)).AMRUom, avgWeekend(((ii * 12) + iii)).AMRSeason, dtTemp.AddHours(ii).AddMinutes(iii * 5), avgWeekend(((ii * 12) + iii)).Reading))
                                        Me.InnerList.Add(New Reading(avgWeekend(((ii * 12) + iii)).BaseOrTier, avgWeekend(((ii * 12) + iii)).TimeOfUse, avgWeekend(((ii * 12) + iii)).Season, dtTemp.AddHours(ii).AddMinutes(iii * 5), avgWeekend(((ii * 12) + iii)).Quantity, avgWeekend(((ii * 12) + iii)).DayType, isEmpty))
                                    Else
                                        'Me.InnerList.Add(New EntAMRReading(isEmpty, AMREditCode.Other, avgWeekday(((ii * 12) + iii)).AMRTou, avgWeekday(((ii * 12) + iii)).AMRUom, avgWeekday(((ii * 12) + iii)).AMRSeason, dtTemp.AddHours(ii).AddMinutes(iii * 5), avgWeekday(((ii * 12) + iii)).Reading))
                                        Me.InnerList.Add(New Reading(avgWeekday(((ii * 12) + iii)).BaseOrTier, avgWeekday(((ii * 12) + iii)).TimeOfUse, avgWeekday(((ii * 12) + iii)).Season, dtTemp.AddHours(ii).AddMinutes(iii * 5), avgWeekday(((ii * 12) + iii)).Quantity, avgWeekday(((ii * 12) + iii)).DayType, isEmpty))
                                    End If

                                    nCount = nCount + 1
                                End If

                            Next

                        Next

                    Next
            End Select

        End If

        Return (bRetVal)
    End Function

    Public Function GetFirstDate() As DateTime
        Dim d As DateTime
        Dim i As Integer

        If (Me.InnerList.Count > 0) Then
            d = CType(Me.InnerList.Item(i), Reading).Datestamp
        Else
            d = DateTime.MinValue
        End If

        Return (d)
    End Function

    Public Function GetLastDate() As DateTime
        Dim d As DateTime
        If (Me.InnerList.Count > 0) Then
            d = CType(Me.InnerList.Item(InnerList.Count - 1), Reading).Datestamp
        Else
            d = DateTime.MinValue
        End If

        Return (d)
    End Function

    Public Function GetReadingsForDate(ByVal dtDate As DateTime) As ReadingCollection
        Dim readings As ReadingCollection = Nothing
        Dim r As Reading
        Dim i As Integer
        Dim bFound As Boolean = False

        If (Me.InnerList.Count > 0) Then

            For i = 0 To (Me.InnerList.Count - 1)
                r = CType(Me.InnerList.Item(i), Reading)
                If (r.Datestamp.Day = dtDate.Day And _
                r.Datestamp.Month = dtDate.Month And _
                r.Datestamp.Year = dtDate.Year) Then
                    If (Not bFound) Then
                        readings = New ReadingCollection
                        bFound = True
                    End If
                    readings.Add(New Reading(r.BaseOrTier, r.TimeOfUse, r.Season, r.Datestamp, r.Quantity, r.DayType, r.IsEmpty))
                End If
            Next

        End If

        Return (readings)
    End Function

    ''' <summary>
    ''' Retrieve reading with datestamp matching specified date/time.
    ''' </summary>
    ''' <param name="dtDateTime"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function GetReadForDateTime(ByVal dtDateTime As DateTime) As Reading

        Dim result As Reading
        Dim reading As Reading
        Dim index As Integer

        result = Nothing

        If (Me.InnerList.Count > 0) Then

            For index = 0 To (Me.InnerList.Count - 1)

                reading = CType(Me.InnerList.Item(index), Reading)

                If (reading.Datestamp.Day = dtDateTime.Day And _
                    reading.Datestamp.Month = dtDateTime.Month And _
                    reading.Datestamp.Year = dtDateTime.Year And _
                    reading.Datestamp.Hour = dtDateTime.Hour And _
                    reading.Datestamp.Minute = dtDateTime.Minute) Then

                    result = New Reading(reading.BaseOrTier, reading.TimeOfUse, reading.Season, reading.Datestamp, reading.Quantity, reading.DayType, reading.IsEmpty)

                    Exit For
                End If
            Next

        End If

        Return result

    End Function

    Public Function SumWithinDate(ByVal dtDate As DateTime, ByVal startTime As DateTime, ByVal endTime As DateTime) As Double
        Dim total As Double = 0
        Dim i As Integer
        Dim r As Reading

        If (Me.InnerList.Count > 0) Then
            For i = 0 To (Me.InnerList.Count - 1)
                r = CType(Me.InnerList.Item(i), Reading)

                If (r.Datestamp.Day = dtDate.Day And _
                 r.Datestamp.Month = dtDate.Month And _
                 r.Datestamp.Year = dtDate.Year) Then
                    If (r.Datestamp.TimeOfDay >= startTime.TimeOfDay And _
                    r.Datestamp.TimeOfDay <= endTime.TimeOfDay) Then

                        total = total + r.Quantity

                    End If

                End If
            Next
        End If

        Return (total)
    End Function


    Public Function Clone() As Object Implements ICloneable.Clone
        ' This directly implements deep clone by serializing to a memory stream

        Dim bf As System.Runtime.Serialization.Formatters.Binary.BinaryFormatter = _
         New System.Runtime.Serialization.Formatters.Binary.BinaryFormatter
        Dim ms As System.IO.MemoryStream = New System.IO.MemoryStream
        Dim obj As ReadingCollection

        bf.Serialize(ms, Me)
        ms.Seek(0, System.IO.SeekOrigin.Begin)
        obj = CType(bf.Deserialize(ms), ReadingCollection)
        ms.Close()

        Return (obj)
    End Function

    ''' <summary>
    ''' Calculate nearest previoius interval based on specified datestamp.
    ''' </summary>
    ''' <param name="datestamp"></param>
    ''' <param name="readingInterval"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function RoundToNearestPreviousInterval(datestamp As DateTime, readingInterval As ReadingInterval) As DateTime

        Dim result As DateTime

        Select Case readingInterval
            Case Enums.ReadingInterval.Daily
                result = DateTime.MinValue

            Case Enums.ReadingInterval.FifteenMinute
                result = New DateTime(datestamp.Year, datestamp.Month, datestamp.Day, datestamp.Hour, datestamp.Minute, 0).AddMinutes(-datestamp.Minute Mod 15)

            Case Enums.ReadingInterval.FiveMinute
                result = DateTime.MinValue

            Case Enums.ReadingInterval.HalfHourly
                result = New DateTime(datestamp.Year, datestamp.Month, datestamp.Day, datestamp.Hour, datestamp.Minute, 0).AddMinutes(-datestamp.Minute Mod 30)

            Case Enums.ReadingInterval.Hourly
                result = New DateTime(datestamp.Year, datestamp.Month, datestamp.Day, datestamp.Hour, datestamp.Minute, 0).AddMinutes(-datestamp.Minute Mod 60)

        End Select

        Return result

    End Function


End Class


