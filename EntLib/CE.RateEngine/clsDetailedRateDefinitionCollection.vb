Public Class DetailedRateDefinitionCollection
	Inherits CollectionBase
    Implements ICloneable

	Public Sub New()
	End Sub

	Default ReadOnly Property Item(ByVal index As Integer) As DetailedRateDefinition
		Get
			Return CType(Me.InnerList.Item(index), DetailedRateDefinition)
		End Get
	End Property

	Public Function Add(ByVal value As DetailedRateDefinition) As Integer
		Return Me.InnerList.Add(value)
	End Function

	Public Sub Insert(ByVal index As Integer, ByVal value As DetailedRateDefinition)
		Me.InnerList.Insert(index, value)
	End Sub

	Public Sub AddRange(ByVal values() As DetailedRateDefinition)
		Me.InnerList.AddRange(values)
	End Sub

    Public Sub Remove(ByVal value As DetailedRateDefinition)
        Me.InnerList.Remove(value)
    End Sub

    Public Function ContainsDifferentRateMasters() As Boolean
        Dim bRetval As Boolean = False
        Dim nLastMasterID As Integer
        Dim i As Integer
        Dim objTemp As DetailedRateDefinition

        If (Me.InnerList.Count > 0) Then

            For i = 0 To Me.InnerList.Count - 1
                objTemp = CType(Me.InnerList.Item(i), DetailedRateDefinition)
                If (i = 0) Then nLastMasterID = objTemp.MasterID
                If (objTemp.MasterID <> nLastMasterID) Then
                    bRetval = True
                    Exit For
                End If
            Next

        End If

        Return (bRetval)
    End Function

    Public Function GetDefinition(ByVal selectedDate As DateTime) As DetailedRateDefinition
        Dim def As DetailedRateDefinition = Nothing
        Dim i As Integer

        If (Me.InnerList.Count > 0) Then

            For i = 0 To Me.InnerList.Count - 1
                def = CType(Me.InnerList.Item(i), DetailedRateDefinition)
                If (def.StartDate < selectedDate) Then
                    Exit For
                End If
            Next

        End If

        Return (def)
    End Function

    ''' <summary>
    ''' Deep clone the entire collection.
    ''' This could be expensive.  Only do this when an absolutely unique object needs to be created.
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function Clone() As Object Implements System.ICloneable.Clone
        Dim bf As System.Runtime.Serialization.Formatters.Binary.BinaryFormatter = _
               New System.Runtime.Serialization.Formatters.Binary.BinaryFormatter
        Dim ms As System.IO.MemoryStream = New System.IO.MemoryStream
        Dim obj As DetailedRateDefinitionCollection

        bf.Serialize(ms, Me)
        ms.Seek(0, System.IO.SeekOrigin.Begin)
        obj = CType(bf.Deserialize(ms), DetailedRateDefinitionCollection)
        ms.Close()

        Return (obj)
    End Function

End Class
