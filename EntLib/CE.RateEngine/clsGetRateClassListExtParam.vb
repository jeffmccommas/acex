Imports CE.RateEngine.Enums

Public Class GetRateClassListExtParam

	Private m_eCustomerType As CustomerType
	Private m_eFuelType As FuelType
	Private m_eLanguage As Language
	Private m_bDefaultOnly As Boolean

	Public Sub New()
		m_eCustomerType = CustomerType.Residential
		m_eFuelType = FuelType.Electric
		m_eLanguage = Language.English
	End Sub

	Public Sub New(ByVal eCustomerType As CustomerType, ByVal eFuelType As FuelType, ByVal eLanguage As Language)
		m_eCustomerType = eCustomerType
		m_eFuelType = eFuelType
		m_eLanguage = eLanguage
	End Sub

	Public Sub New(ByVal eCustomerType As CustomerType, ByVal eFuelType As FuelType, ByVal eLanguage As Language, ByVal bDefaultOnly As Boolean)
		m_eCustomerType = eCustomerType
		m_eFuelType = eFuelType
		m_eLanguage = eLanguage
		m_bDefaultOnly = bDefaultOnly
	End Sub

#Region "Properties"

	Public Property CustomerType() As CustomerType
		Get
			Return m_eCustomerType
		End Get
		Set(ByVal Value As CustomerType)
			m_eCustomerType = Value
		End Set
	End Property

	Public Property FuelType() As FuelType
		Get
			Return m_eFuelType
		End Get
		Set(ByVal Value As FuelType)
			m_eFuelType = Value
		End Set
	End Property

	Public Property Language() As Language
		Get
			Return m_eLanguage
		End Get
		Set(ByVal Value As Language)
			m_eLanguage = Value
		End Set
	End Property

	Public Property DefaultOnly() As Boolean
		Get
			Return m_bDefaultOnly
		End Get
		Set(ByVal Value As Boolean)
			m_bDefaultOnly = Value
		End Set
	End Property

#End Region

End Class
