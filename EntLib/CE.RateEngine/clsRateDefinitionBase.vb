Imports CE.RateEngine.Enums

Public MustInherit Class RateDefinitionBase

	Private m_nMasterID As Integer
	Private m_nRateCompanyID As Integer
	Private m_sRateClass As String	 'RmClientRateID
	Private m_bDefault As Boolean
	Private m_nDefinitionID As Integer
	Private m_sDescription As String
	Private m_sName As String
	Private m_sEligibility As String
	Private m_eLanguage As Language
	Private m_eFuelType As FuelType
	Private m_eCustomerType As CustomerType
    Private m_eServiceType As ServiceType
    Private m_eLoadType As LoadType
    Private m_eBilledDemandType As BilledDemandType
	Private m_nUnitOfMeasure As Integer	 'actual enum based on fuel, that is why simply int
	Private m_dtStartDate As DateTime
	Private m_eTierStructureType As TierStructureType
	Private m_eDemandStructureType As DemandStructureType
    Private m_eProrateType As ProrateType
    Private m_eDemandProrateType As DemandProrateType
    Private m_eSeasonalProrateType As SeasonalProrateType
    Private m_eSeasonalDemandProrateType As SeasonalDemandProrateType
	Private m_bStepped As Boolean
	Private m_sDifferenceNote As String
	Private m_dblBaselineFactor As Double	'RateClass indicates what to lookup from DB and store here
	Private m_bSeasonal As Boolean
	Private m_bTiered As Boolean
	Private m_bTimeOfUse As Boolean
	Private m_bDemand As Boolean
    Private m_bBasic As Boolean
    Private m_bHasChildren As Boolean
	Private m_bFuelAdjustment As Boolean
    Private m_objPollution As Pollution  ' to hold future pollution factors
    Private m_bBoundariesConsistent As Boolean = True
    Private m_bRTPInd As Boolean
    Private m_eRTPStream As RTPStream
    Private m_objMasterChildren As MasterChildCollection 'do not initialize this in the constructor
    Private m_nHolidayGroupID As Integer
    Private m_eBaselineRule As BaselineRule
    Private m_eNetMeteringType As NetMeteringType
    Private m_bNetMetering As Boolean
    Private m_bIndependentTierAccumulationsByTOU As Boolean ' used by binning of tiered + tou rate definitions
    Private m_bCapacityReserve As Boolean
    Private m_bCPEventUsageAdder As Boolean
    Private m_bStartDateModified As Boolean     ' used via advanced callers that ask for start date to be modified after selecting
    Private m_dtStartDateSelector As DateTime   ' used via advanced callers that ask for start date to be modified after selecting
    Private m_dtEndDateSelector As DateTime     ' used via advanced callers that ask for start date to be modified after selecting

	Public Sub New()
        m_objPollution = New Pollution
	End Sub

#Region "Properties"

	Public Property MasterID() As Integer
		Get
			Return m_nMasterID
		End Get
		Set(ByVal Value As Integer)
			m_nMasterID = Value
		End Set
	End Property

	Public Property RateCompanyID() As Integer
		Get
			Return m_nRateCompanyID
		End Get
		Set(ByVal Value As Integer)
			m_nRateCompanyID = Value
		End Set
	End Property

	Public Property DefinitionID() As Integer
		Get
			Return m_nDefinitionID
		End Get
		Set(ByVal Value As Integer)
			m_nDefinitionID = Value
		End Set
	End Property

	Public Property RateClass() As String
		Get
			Return m_sRateClass
		End Get
		Set(ByVal Value As String)
			m_sRateClass = Value
		End Set
	End Property

	Public Property Description() As String
		Get
			Return m_sDescription
		End Get
		Set(ByVal Value As String)
			m_sDescription = Value
		End Set
	End Property

	Public Property BaselineFactor() As Double
		Get
			Return m_dblBaselineFactor
		End Get
		Set(ByVal Value As Double)
			m_dblBaselineFactor = Value
		End Set
	End Property

	Public Property DifferenceNote() As String
		Get
			Return m_sDifferenceNote
		End Get
		Set(ByVal Value As String)
			m_sDifferenceNote = Value
		End Set
	End Property

	Public Property ProrateType() As ProrateType
		Get
			Return m_eProrateType
		End Get
		Set(ByVal Value As ProrateType)
			m_eProrateType = Value
		End Set
    End Property

    Public Property DemandProrateType() As DemandProrateType
        Get
            Return m_eDemandProrateType
        End Get
        Set(ByVal Value As DemandProrateType)
            m_eDemandProrateType = Value
        End Set
    End Property

	Public Property SeasonalProrateType() As SeasonalProrateType
		Get
			Return m_eSeasonalProrateType
		End Get
		Set(ByVal Value As SeasonalProrateType)
			m_eSeasonalProrateType = Value
		End Set
	End Property

    Public Property SeasonalDemandProrateType() As SeasonalDemandProrateType
        Get
            Return m_eSeasonalDemandProrateType
        End Get
        Set(ByVal Value As SeasonalDemandProrateType)
            m_eSeasonalDemandProrateType = Value
        End Set
    End Property

	Public Property ServiceType() As ServiceType
		Get
			Return m_eServiceType
		End Get
		Set(ByVal Value As ServiceType)
			m_eServiceType = Value
		End Set
	End Property

	Public Property DemandStructureType() As DemandStructureType
		Get
			Return m_eDemandStructureType
		End Get
		Set(ByVal Value As DemandStructureType)
			m_eDemandStructureType = Value
		End Set
	End Property

	Public Property TierStructureType() As TierStructureType
		Get
			Return m_eTierStructureType
		End Get
		Set(ByVal Value As TierStructureType)
			m_eTierStructureType = Value
		End Set
	End Property

	Public Property StartDate() As DateTime
		Get
			Return m_dtStartDate
		End Get
		Set(ByVal Value As DateTime)
			m_dtStartDate = Value
		End Set
	End Property

	Public Property UnitOfMeasure() As Integer
		Get
			Return m_nUnitOfMeasure
		End Get
		Set(ByVal Value As Integer)
			m_nUnitOfMeasure = Value
		End Set
	End Property

	Public Property CustomerType() As CustomerType
		Get
			Return m_eCustomerType
		End Get
		Set(ByVal Value As CustomerType)
			m_eCustomerType = Value
		End Set
	End Property

	Public Property FuelType() As FuelType
		Get
			Return m_eFuelType
		End Get
		Set(ByVal Value As FuelType)
			m_eFuelType = Value
		End Set
	End Property

	Public Property Language() As Language
		Get
			Return m_eLanguage
		End Get
		Set(ByVal Value As Language)
			m_eLanguage = Value
		End Set
	End Property

	Public Property Eligibility() As String
		Get
			Return m_sEligibility
		End Get
		Set(ByVal Value As String)
			m_sEligibility = Value
		End Set
	End Property

	Public Property Name() As String
		Get
			Return m_sName
		End Get
		Set(ByVal Value As String)
			m_sName = Value
		End Set
	End Property

	Public Property Pollution() As Pollution
		Get
			Return m_objPollution
		End Get
		Set(ByVal Value As Pollution)
			m_objPollution = Value
		End Set
	End Property

    Public Property BoundariesConsistent() As Boolean       ' this is for TOU Boundaries
        Get
            Return m_bBoundariesConsistent
        End Get
        Set(ByVal Value As Boolean)
            m_bBoundariesConsistent = Value
        End Set
    End Property

    Public Property RTPStream() As RTPStream
        Get
            Return m_eRTPStream
        End Get
        Set(ByVal value As RTPStream)
            m_eRTPStream = value
        End Set
    End Property

    Public Property LoadType() As LoadType
        Get
            Return m_eLoadType
        End Get
        Set(ByVal Value As LoadType)
            m_eLoadType = Value
        End Set
    End Property

    Public Property BilledDemandType() As BilledDemandType
        Get
            Return m_eBilledDemandType
        End Get
        Set(ByVal Value As BilledDemandType)
            m_eBilledDemandType = Value
        End Set
    End Property

    Public Property MasterChildren() As MasterChildCollection
        Get
            Return m_objMasterChildren
        End Get
        Set(ByVal Value As MasterChildCollection)
            m_objMasterChildren = Value
        End Set
    End Property

    Public Property HolidayGroupID() As Integer
        Get
            Return m_nHolidayGroupID
        End Get
        Set(ByVal Value As Integer)
            m_nHolidayGroupID = Value
        End Set
    End Property

    Public Property BaselineRule() As BaselineRule
        Get
            Return m_eBaselineRule
        End Get
        Set(ByVal Value As BaselineRule)
            m_eBaselineRule = Value
        End Set
    End Property

    Public Property NetMeteringType() As NetMeteringType
        Get
            Return m_eNetMeteringType
        End Get
        Set(ByVal Value As NetMeteringType)
            m_eNetMeteringType = Value
        End Set
    End Property

#End Region

#Region "Other Properties"

	Public ReadOnly Property IsDefault() As Boolean
		Get
			Return (m_bDefault)
		End Get
	End Property

	Public WriteOnly Property [Default]() As Boolean
		Set(ByVal Value As Boolean)
			m_bDefault = Value
		End Set
	End Property

	Public ReadOnly Property IsSeasonal() As Boolean
		Get
			Return (m_bSeasonal)
		End Get
	End Property

	Public WriteOnly Property Seasonal() As Boolean
		Set(ByVal Value As Boolean)
			m_bSeasonal = Value
		End Set
	End Property

	Public ReadOnly Property IsTiered() As Boolean
		Get
			Return (m_bTiered)
		End Get
	End Property

	Public WriteOnly Property Tiered() As Boolean
		Set(ByVal Value As Boolean)
			m_bTiered = Value
		End Set
	End Property

	Public ReadOnly Property IsTimeOfUse() As Boolean
		Get
			Return (m_bTimeOfUse)
		End Get
	End Property

	Public WriteOnly Property TimeOfUse() As Boolean
		Set(ByVal Value As Boolean)
			m_bTimeOfUse = Value
		End Set
	End Property

	Public ReadOnly Property IsDemand() As Boolean
		Get
			Return (m_bDemand)
		End Get
	End Property

	Public WriteOnly Property Demand() As Boolean
		Set(ByVal Value As Boolean)
			m_bDemand = Value
		End Set
	End Property

	Public ReadOnly Property IsBasic() As Boolean
		Get
			Return (m_bBasic)
		End Get
	End Property

	Public WriteOnly Property Basic() As Boolean
		Set(ByVal Value As Boolean)
			m_bBasic = Value
		End Set
	End Property

	Public ReadOnly Property IsFuelAdjustment() As Boolean
		Get
			Return (m_bFuelAdjustment)
		End Get
	End Property

	Public WriteOnly Property FuelAdjustment() As Boolean
		Set(ByVal Value As Boolean)
			m_bFuelAdjustment = Value
		End Set
	End Property

	Public ReadOnly Property IsStepped() As Boolean
		Get
			Return (m_bStepped)
		End Get
	End Property

	Public WriteOnly Property Stepped() As Boolean
		Set(ByVal Value As Boolean)
			m_bStepped = Value
		End Set
    End Property

    Public ReadOnly Property IsRTPRate() As Boolean
        Get
            Return m_bRTPInd
        End Get
    End Property

    Public WriteOnly Property RTPRate() As Boolean
        Set(ByVal value As Boolean)
            m_bRTPInd = value
        End Set
    End Property

    Public ReadOnly Property MasterHasChildren() As Boolean
        Get
            Return (m_bHasChildren)
        End Get
    End Property

    Public WriteOnly Property Children() As Boolean
        Set(ByVal Value As Boolean)
            m_bHasChildren = Value
        End Set
    End Property

    Public ReadOnly Property IsNetMetering() As Boolean
        Get
            Return (m_bNetMetering)
        End Get
    End Property

    Public WriteOnly Property NetMetering() As Boolean
        Set(ByVal Value As Boolean)
            m_bNetMetering = Value
        End Set
    End Property

    Public ReadOnly Property IsIndependentTierAccumulationsByTOU() As Boolean
        Get
            Return (m_bIndependentTierAccumulationsByTOU)
        End Get
    End Property

    Public WriteOnly Property IndependentTierAccumulationsByTOU() As Boolean
        Set(ByVal Value As Boolean)
            m_bIndependentTierAccumulationsByTOU = Value
        End Set
    End Property

    Public ReadOnly Property IsCapacityReserve() As Boolean
        Get
            Return (m_bCapacityReserve)
        End Get
    End Property

    Public WriteOnly Property CapacityReserve() As Boolean
        Set(ByVal Value As Boolean)
            m_bCapacityReserve = Value
        End Set
    End Property

    Public ReadOnly Property IsCPEventUsageAdder() As Boolean
        Get
            Return (m_bCPEventUsageAdder)
        End Get
    End Property

    Public WriteOnly Property CPEventUsageAdder() As Boolean
        Set(ByVal Value As Boolean)
            m_bCPEventUsageAdder = Value
        End Set
    End Property

    Public ReadOnly Property IsStartDateModified() As Boolean
        Get
            Return (m_bStartDateModified)
        End Get
    End Property

    Public WriteOnly Property StartDateModified() As Boolean
        Set(ByVal Value As Boolean)
            m_bStartDateModified = Value
        End Set
    End Property


    Public Property StartDateSelector() As DateTime
        Get
            Return m_dtStartDateSelector
        End Get
        Set(ByVal Value As DateTime)
            m_dtStartDateSelector = Value
        End Set
    End Property

    Public Property EndDateSelector() As DateTime
        Get
            Return m_dtEndDateSelector
        End Get
        Set(ByVal Value As DateTime)
            m_dtEndDateSelector = Value
        End Set
    End Property

#End Region

#Region "Public Functions"

    Public Function HasLoadType(ByVal eLoadType As LoadType) As Boolean
        Dim bRetval As Boolean = False

        ' Check self first; then master children if applicable;
        If (m_eLoadType = eLoadType) Then
            bRetval = True
        Else
            If (m_bHasChildren) Then
                If (Not m_objMasterChildren Is Nothing) Then
                    bRetval = m_objMasterChildren.DoesLoadTypeExist(eLoadType)
                End If
            End If
        End If

        Return (bRetval)
    End Function

    ' This returns the *first* rate class of the requested load type; 
    ' If there are multiple children of one load type, the first in the master children list that
    ' matches the LoadType will get returned; 
    Public Function GetRateClassForLoadType(ByVal eLoadType As LoadType) As String
        Dim sRateClass As String = String.Empty

        ' Check self first; then master children if applicable;
        If (m_eLoadType = eLoadType) Then
            sRateClass = m_sRateClass
        Else
            If (m_bHasChildren) Then
                If (Not m_objMasterChildren Is Nothing) Then
                    sRateClass = m_objMasterChildren.GetRateClassForLoadType(eLoadType)
                End If
            End If
        End If

        Return (sRateClass)
    End Function

    Public Function HolidayGroupExists() As Boolean
        Dim retval As Boolean = False

        ' if the id is -1, then no id has been assigned to the master rate class
        If (m_nHolidayGroupID >= 0) Then
            retval = True
        Else
            retval = False
        End If

        Return (retval)
    End Function

#End Region

End Class
