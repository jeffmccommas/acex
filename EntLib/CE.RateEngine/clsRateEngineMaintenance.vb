Imports System.Text
Imports System.Data.SqlClient
Imports CE.RateEngine.Enums

''' <summary>
''' 
''' </summary>
''' <remarks></remarks>
Public Class RateEngineMaintenance

#Region "Constants"
    Private Const ValidationWarningEffectiveDatePostDateThreshold As Integer = 30
    Private Const ExceptionToBeImplementedError As String = "To be implemented error."
    Private Const ExceptionDataReaderError As String = "Rate Engine Maintenance data reader error."
    Private Const ExceptionUpdateError As String = "Rate Engine Maintenance update error."
    Private Const ExceptionInsertError As String = "Rate Engine Maintenance insert error."
    Private Const ExceptionUnexpectedChargeTypeError As String = "Unexpected charge type error."
    Private Const ImportErrorPrefix As String = "* "
    Private Const ImportErrorCouldNotCloneRateDefinition As String = "Could not clone rate definition."
    Private Const ValidationWarningPrefix As String = "* "
    Private Const ValidationWarningEffectiveDateIsNDaysInThePast As String = " Effective date post dated over {0} days."
    Private Const ValidationErrorPrefix As String = "* "
    Private Const ValidationErrorInvalidRateCompanyID As String = " invalid rate company ID"
    Private Const ValidationErrorInvalidSourceType As String = " invalid source type"
    Private Const ValidationErrorInvalidSourceID As String = " invalid source ID"
    Private Const ValidationErrorInvalidRateClass As String = " invalid rate class"
    Private Const ValidationErrorInvalidChargeType As String = " invalid charge type"
    Private Const ValidationErrorInvalidEffectiveDate As String = " invalid effective date"
    Private Const ValidationErrorInvalidCostType As String = " invalid cost type"
    Private Const ValidationErrorInvalidChargeCalcType As String = " invalid charge calc type"
    Private Const ValidationErrorInvalidStep As String = " invalid step"
    Private Const ValidationErrorInvalidSeason As String = " invalid season"
    Private Const ValidationErrorInvalidTier As String = " invalid tier"
    Private Const ValidationErrorInvalidTimeOfUse As String = " invalid time of use"
    Private Const ValidationErrorInvalidPartType As String = " invalid part type"
    Private Const ValidationErrorInvalidChargeValue As String = " invalid charge value"
    Private Const ValidationErrorInvalidRebateClass As String = " invalid rebate class"

#End Region

#Region "Private Data Members"

    Private _rateEngineConnectionString As String = String.Empty
    Private _hubStaticConnectionString As String = String.Empty
    Private _loggingEnabled As Boolean
    Private _logSessionID As System.Guid

#End Region

#Region "DB Helper Functions"

    ' Replacement for function of same name in VB namespace (removed that namespace)
    <System.ComponentModel.Browsable(False)> _
    Public Function IsDBNull(ByVal dbvalue As Object) As Boolean
        Return dbvalue Is DBNull.Value
    End Function

#End Region

#Region "Object Creation"
    Public Sub New(ByVal rateEngineConnectionString As String, _
                   ByVal hubStaticConnectionString As String)

        _rateEngineConnectionString = rateEngineConnectionString
        _hubStaticConnectionString = hubStaticConnectionString

    End Sub

    Public Sub New(ByVal rateEngineConnectionString As String, ByVal loggingEnabled As Boolean)

        _rateEngineConnectionString = rateEngineConnectionString
        _loggingEnabled = loggingEnabled

        BeginLogSession()

    End Sub

#End Region

#Region "Properties"

    Public ReadOnly Property RateEngineConnectionString() As String
        Get
            Return (_rateEngineConnectionString)
        End Get
    End Property

    Public ReadOnly Property HubStaticConnectionString() As String
        Get
            Return (_hubStaticConnectionString)
        End Get
    End Property

    Public ReadOnly Property IsLoggingEnabled() As Boolean
        Get
            Return (_loggingEnabled)
        End Get
    End Property

    Public ReadOnly Property LogSessionID() As System.Guid
        Get
            Return (_logSessionID)
        End Get
    End Property

#End Region

#Region "Rate Maintenance Data"

    Public Function PopulateRateMaintDataSession(ByVal sessionID As String, _
                                                 ByRef rateMaintDataList As RateMaintDataCollection) As String

        Dim dataRateEngineMaintenance As New dctlRateEngineMaintenance(RateEngineConnectionString)
        Dim rateMaintData As RateMaintData

        If sessionID Is Nothing Then

            sessionID = GenerateSessionID()

        Else

            dataRateEngineMaintenance.DeleteRateMaintDataSession(sessionID)

        End If

        For Each rateMaintData In rateMaintDataList
            ' CR 20298, 20299, 20302 Oct 2011 - add rebate class
            dataRateEngineMaintenance.InsertRateMaintData(sessionID, _
                                                          rateMaintData.UploadDateTime, _
                                                          rateMaintData.RateCompanyID, _
                                                          rateMaintData.RateMasterID, _
                                                          rateMaintData.SourceTypeID, _
                                                          rateMaintData.SourceID, _
                                                          rateMaintData.ClientRateID, _
                                                          rateMaintData.RateDefID, _
                                                          rateMaintData.EffectiveDate, _
                                                          rateMaintData.ChargeType, _
                                                          rateMaintData.Tier, _
                                                          rateMaintData.TimeOfUse, _
                                                          rateMaintData.SeasonID, _
                                                          rateMaintData.StepID, _
                                                          rateMaintData.PartTypeID, _
                                                          rateMaintData.CostTypeID, _
                                                          rateMaintData.ChargeValue, _
                                                          rateMaintData.CalcTypeID, _
                                                          rateMaintData.ErrorText, _
                                                          rateMaintData.ImportStatus, _
                                                          rateMaintData.ImportDateTime, _
                                                          rateMaintData.OriginalRateCompanyID, _
                                                          rateMaintData.OriginalSourceType, _
                                                          rateMaintData.OriginalSourceID, _
                                                          rateMaintData.OriginalSourceName, _
                                                          rateMaintData.OriginalRateClass, _
                                                          rateMaintData.OriginalEffectiveDate, _
                                                          rateMaintData.OriginalTypeOfCharge, _
                                                          rateMaintData.OriginalTier, _
                                                          rateMaintData.OriginalTimeOfUse, _
                                                          rateMaintData.OriginalSeason, _
                                                          rateMaintData.OriginalPartType, _
                                                          rateMaintData.OriginalCostType, _
                                                          rateMaintData.OriginalChargeValue, _
                                                          rateMaintData.OriginalCalcType, _
                                                          rateMaintData.OriginalStep, _
                                                          rateMaintData.RebateClassID, _
                                                          rateMaintData.OriginalRebateClassID)

        Next

        Return (sessionID)

    End Function

    Public Function GenerateSessionID() As String

        Return System.Guid.NewGuid.ToString

    End Function

    Public Sub ValidateRateMaintDataSession(ByVal sessionID As String)

        Dim rateEngine As New RateEngine(RateEngineConnectionString)
        Dim rateEngineExtras As New RateEngineExtras
        Dim rateMaintDataList As RateMaintDataCollection
        Dim rateMaintData As RateMaintData
        Dim simpleRateDefinitionCollection As SimpleRateDefinitionCollection
        Dim simpleRateDefinition As SimpleRateDefinition
        Dim rateMasterID As Integer
        Dim rateCompanyID As Integer
        Dim errorText As StringBuilder
        Dim warningText As StringBuilder

        rateMaintDataList = GetRateMaintDataSession(sessionID)

        'Validate each rate maintenance data record in specified session and
        'retrieve related data from database.
        For Each rateMaintData In rateMaintDataList

            If rateMaintData.ImportStatus = ImportStatus.Comitted Then
                'Bypass "commited" records.
                Continue For
            End If

            'Reset error condition (if any).
            rateMaintData.ErrorText = String.Empty
            errorText = Nothing
            errorText = New StringBuilder
            warningText = Nothing
            warningText = New StringBuilder

            'Validate source type.
            If IsValidSourceTypeID(rateMaintData.OriginalSourceType) Then
                rateMaintData.SourceTypeID = CShort(CType(ConvertEnumTextToValue( _
                                                        rateMaintData.OriginalSourceType, _
                                                        GetType(Enums.SourceType)), Enums.SourceType))
            Else
                AppendValidationErrorToErrorText(errorText, ValidationErrorInvalidSourceType)
            End If

            'Validate source ID.
            If IsTextValidForType(rateMaintData.OriginalSourceID, GetType(Integer)) Then
                rateMaintData.SourceID = Int32.Parse(rateMaintData.OriginalSourceID)
            Else
                AppendValidationErrorToErrorText(errorText, ValidationErrorInvalidSourceID)
            End If

            'Retrieve rate company ID.
            rateCompanyID = 0
            rateCompanyID = rateEngineExtras.GetRateCompanyID(rateMaintData.SourceID, _
                                                              CType(rateMaintData.SourceTypeID, SourceType), _
                                                              HubStaticConnectionString)

            'Validate rate company ID.
            If IsTextValidForType(rateMaintData.OriginalRateCompanyID, GetType(Integer)) Then
                If Int32.Parse(rateMaintData.OriginalRateCompanyID) = rateCompanyID Then
                    rateMaintData.RateCompanyID = Int32.Parse(rateMaintData.OriginalRateCompanyID)
                Else
                    AppendValidationErrorToErrorText(errorText, ValidationErrorInvalidRateCompanyID)
                End If
            Else
                AppendValidationErrorToErrorText(errorText, ValidationErrorInvalidRateCompanyID)
            End If

            'Retrieve rate master ID.
            simpleRateDefinitionCollection = Nothing
            simpleRateDefinitionCollection = rateEngine.GetSimpleRateInformation(rateMaintData.RateCompanyID, _
                                                                                 rateMaintData.OriginalRateClass)

            'Validate rate class.
            If rateMaintData.RateCompanyID > 0 And _
               simpleRateDefinitionCollection.Count > 0 Then

                'Rate master ID found.
                For Each simpleRateDefinition In simpleRateDefinitionCollection
                    rateMasterID = simpleRateDefinition.MasterID
                    rateMaintData.RateMasterID = rateMasterID
                    Exit For
                Next
                rateMaintData.ClientRateID = rateMaintData.OriginalRateClass
            Else
                'Rate class invalid.
                AppendValidationErrorToErrorText(errorText, ValidationErrorInvalidRateClass)
            End If

            'Validate charge type.
            If IsValidChargeType(rateMaintData.OriginalTypeOfCharge) Then
                rateMaintData.ChargeType = CType(ConvertEnumTextToValue( _
                                                        rateMaintData.OriginalTypeOfCharge, _
                                                        GetType(Enums.ChargeType)), Enums.ChargeType)
            Else
                AppendValidationErrorToErrorText(errorText, ValidationErrorInvalidChargeType)
            End If

            'Validate effective date.
            If IsTextValidForType(rateMaintData.OriginalEffectiveDate, GetType(DateTime)) Then
                rateMaintData.EffectiveDate = CType(rateMaintData.OriginalEffectiveDate, DateTime)

                'Effect date is more than N number of days in the past.
                If rateMaintData.EffectiveDate < (Date.Now.AddDays(ValidationWarningEffectiveDatePostDateThreshold * -1)) Then

                    AppendValidationErrorToErrorText(warningText, _
                                                     String.Format(ValidationWarningEffectiveDateIsNDaysInThePast, _
                                                                   ValidationWarningEffectiveDatePostDateThreshold))
                End If

            Else
                AppendValidationErrorToErrorText(errorText, ValidationErrorInvalidEffectiveDate)
            End If

            'Retrieve rate definition ID.
            If rateMaintData.EffectiveDate > DateTime.MinValue And _
               rateMaintData.EffectiveDate < DateTime.MaxValue Then

                rateMaintData.RateDefID = GetRateDefinitionByStartDate(rateMasterID, _
                                                                       rateMaintData.EffectiveDate)
            End If

            'Validate data required for specified charge type (minimum, service, tax, use).
            RetrieveValidateDataRequiredForChargeType(rateMaintData, errorText)

            'Validation error(s) detected.
            If Not errorText.Length = 0 Then
                rateMaintData.ImportStatus = ImportStatus.Error
            Else
                rateMaintData.ImportStatus = ImportStatus.Validated
            End If

            'Add any warnings to error text (if any).
            If Not warningText.Length = 0 Then
                AppendValidationErrorToErrorText(errorText, warningText.ToString)
            End If

            'Update rate maintenance data error text.
            rateMaintData.ErrorText = errorText.ToString

            'Update rate maintenance data for session.
            'CR 20298, 20299, 20302 Oct 2011 - rebate class ID
            UpdateRateMaintData(rateMaintData.RateMaintID, _
                                rateMaintData.SessionID, _
                                rateMaintData.UploadDateTime, _
                                rateMaintData.RateCompanyID, _
                                rateMaintData.RateMasterID, _
                                rateMaintData.SourceTypeID, _
                                rateMaintData.SourceID, _
                                rateMaintData.ClientRateID, _
                                rateMaintData.RateDefID, _
                                rateMaintData.EffectiveDate, _
                                rateMaintData.ChargeType, _
                                rateMaintData.Tier, _
                                rateMaintData.TimeOfUse, _
                                rateMaintData.SeasonID, _
                                rateMaintData.StepID, _
                                rateMaintData.PartTypeID, _
                                rateMaintData.CostTypeID, _
                                rateMaintData.ChargeValue, _
                                rateMaintData.CalcTypeID, _
                                rateMaintData.ErrorText, _
                                rateMaintData.ImportStatus, _
                                rateMaintData.ImportDateTime, _
                                rateMaintData.OriginalRateCompanyID, _
                                rateMaintData.OriginalSourceType, _
                                rateMaintData.OriginalSourceID, _
                                rateMaintData.OriginalSourceName, _
                                rateMaintData.OriginalRateClass, _
                                rateMaintData.OriginalEffectiveDate, _
                                rateMaintData.OriginalTypeOfCharge, _
                                rateMaintData.OriginalTier, _
                                rateMaintData.OriginalTimeOfUse, _
                                rateMaintData.OriginalSeason, _
                                rateMaintData.OriginalPartType, _
                                rateMaintData.OriginalCostType, _
                                rateMaintData.OriginalChargeValue, _
                                rateMaintData.OriginalCalcType, _
                                rateMaintData.OriginalStep,
                                rateMaintData.RebateClassID, _
                                rateMaintData.OriginalRebateClassID)

        Next rateMaintData

    End Sub

    Public Sub ImportRateMaintDataIntoRateEngine(ByVal sessionID As String)

        Dim rateMaintDataList As RateMaintDataCollection
        Dim rateMaintData As RateMaintData
        Dim sourceRateDefID As Integer = 0
        Dim sourceRateMasterID As Integer = 0
        Dim cloneEditDate As Date = Date.MinValue
        Dim cloneRateDefinitionEnabled As Boolean
        Dim cloneStartDateValid As Boolean
        Dim errorText As StringBuilder

        rateMaintDataList = GetRateMaintDataSession(sessionID)

        'Import each rate maintenance data record in specified session.
        For Each rateMaintData In rateMaintDataList

            If Not rateMaintData.ImportStatus = ImportStatus.Validated Then
                'Bypass non-validated records.
                Continue For
            End If

            'Reset error condition (if any).
            rateMaintData.ErrorText = String.Empty
            errorText = Nothing
            errorText = New StringBuilder

            'Retrieve rate definition.
            If rateMaintData.EffectiveDate > DateTime.MinValue And _
               rateMaintData.EffectiveDate < DateTime.MaxValue Then

                rateMaintData.RateDefID = GetRateDefinitionByStartDate(rateMaintData.RateMasterID, _
                                                                       rateMaintData.EffectiveDate)
            End If

            'Existing rate definition not available.
            If Not rateMaintData.RateDefID > 0 Then

                'Retrieve newest rate definition to clone.
                sourceRateDefID = GetNewestRateDefinition(rateMaintData.RateMasterID)

                'Clone rate definition.
                sourceRateMasterID = rateMaintData.RateMasterID
                cloneEditDate = Date.Now
                cloneRateDefinitionEnabled = True
                cloneStartDateValid = True
                rateMaintData.RateDefID = CloneRateDefinition(sourceRateDefID, _
                                                              sourceRateMasterID, _
                                                              rateMaintData.RateMasterID, _
                                                              rateMaintData.EffectiveDate, _
                                                              cloneEditDate, _
                                                              cloneRateDefinitionEnabled, _
                                                              cloneStartDateValid)

                If Not rateMaintData.RateDefID > 0 Then
                    AppendImportErrorToErrorText(errorText, _
                                                 ImportErrorCouldNotCloneRateDefinition)
                End If

            End If

            'Import charge data.
            Select Case rateMaintData.ChargeType

                Case ChargeType.Minimum
                    ImportMinimumCharge(rateMaintData)

                Case ChargeType.Service
                    ImportServiceCharge(rateMaintData)

                Case ChargeType.Tax
                    ImportTaxCharge(rateMaintData)

                Case ChargeType.Use
                    ImportUseCharge(rateMaintData)

                Case Else
                    Throw New Exception(ExceptionUnexpectedChargeTypeError)

            End Select

            'Import error(s) detected.
            If Not errorText.Length = 0 Then
                rateMaintData.ImportStatus = ImportStatus.Error
                rateMaintData.ErrorText = errorText.ToString
            Else
                rateMaintData.ImportStatus = ImportStatus.Comitted
                rateMaintData.ImportDateTime = Date.Now
            End If

            'Update rate maintenance data for session.
            'CR 20298, 20299, 20302 Oct 2011 - rebate class ID
            UpdateRateMaintData(rateMaintData.RateMaintID, _
                                rateMaintData.SessionID, _
                                rateMaintData.UploadDateTime, _
                                rateMaintData.RateCompanyID, _
                                rateMaintData.RateMaintID, _
                                rateMaintData.SourceTypeID, _
                                rateMaintData.SourceID, _
                                rateMaintData.ClientRateID, _
                                rateMaintData.RateDefID, _
                                rateMaintData.EffectiveDate, _
                                rateMaintData.ChargeType, _
                                rateMaintData.Tier, _
                                rateMaintData.TimeOfUse, _
                                rateMaintData.SeasonID, _
                                rateMaintData.StepID, _
                                rateMaintData.PartTypeID, _
                                rateMaintData.CostTypeID, _
                                rateMaintData.ChargeValue, _
                                rateMaintData.CalcTypeID, _
                                rateMaintData.ErrorText, _
                                rateMaintData.ImportStatus, _
                                rateMaintData.ImportDateTime, _
                                rateMaintData.OriginalRateCompanyID, _
                                rateMaintData.OriginalSourceType, _
                                rateMaintData.OriginalSourceID, _
                                rateMaintData.OriginalSourceName, _
                                rateMaintData.OriginalRateClass, _
                                rateMaintData.OriginalEffectiveDate, _
                                rateMaintData.OriginalTypeOfCharge, _
                                rateMaintData.OriginalTier, _
                                rateMaintData.OriginalTimeOfUse, _
                                rateMaintData.OriginalSeason, _
                                rateMaintData.OriginalPartType, _
                                rateMaintData.OriginalCostType, _
                                rateMaintData.OriginalChargeValue, _
                                rateMaintData.OriginalCalcType, _
                                rateMaintData.OriginalStep, _
                                rateMaintData.RebateClassID, _
                                rateMaintData.OriginalRebateClassID)

        Next rateMaintData

    End Sub

    Public Function GetRateMaintDataSession(ByVal sessionID As String) As RateMaintDataCollection

        Dim dataRateEngineMaintenance As New dctlRateEngineMaintenance(RateEngineConnectionString)
        Dim rateMaintDataSessionDataReader As SqlDataReader = Nothing
        Dim rateMaintDataColl As New RateMaintDataCollection
        Dim rateMaintData As RateMaintData

        Try

            rateMaintDataSessionDataReader = dataRateEngineMaintenance.GetRateMaintDataSession(sessionID)

            Do While rateMaintDataSessionDataReader.Read()

                rateMaintData = New RateMaintData

                ConvertRateMaintDataSessionDataReaderToRateMaintDataObject( _
                                            rateMaintDataSessionDataReader, _
                                            rateMaintData)

                rateMaintDataColl.Add(rateMaintData)
            Loop

        Catch ex As Exception

            Throw New Exception(ExceptionDataReaderError + ": " + ex.Message, ex.InnerException)

        Finally
            If (Not rateMaintDataSessionDataReader Is Nothing) Then
                If (Not rateMaintDataSessionDataReader.IsClosed) Then
                    rateMaintDataSessionDataReader.Close()
                End If
            End If
        End Try

        Return (rateMaintDataColl)

    End Function

    Public Function GetRateMaintDataSessionDataReader(ByVal sessionID As String) As SqlDataReader

        Dim dataRateEngineMaintenance As New dctlRateEngineMaintenance(RateEngineConnectionString)
        Dim rateMaintDataSessionDataReader As SqlDataReader

        Try

            rateMaintDataSessionDataReader = dataRateEngineMaintenance.GetRateMaintDataSession(sessionID)

        Catch ex As Exception

            Throw New Exception(ExceptionDataReaderError + ": " + ex.Message, ex.InnerException)

        Finally
            'NOTE: No cleanup necessary. Caller is expected to close data reader.
        End Try

        Return (rateMaintDataSessionDataReader)

    End Function

    Public Function InsertRateMaintData(ByVal sessionID As String, _
                                        ByVal uploadDateTime As DateTime, _
                                        ByVal rateCompanyID As Integer, _
                                        ByVal rateMasterID As Integer, _
                                        ByVal sourceTypeID As Integer, _
                                        ByVal sourceID As Integer, _
                                        ByVal clientRateID As String, _
                                        ByVal rateDefID As Integer, _
                                        ByVal effectiveDate As DateTime, _
                                        ByVal chargeType As Integer, _
                                        ByVal load As Integer, _
                                        ByVal timeOfUse As Integer, _
                                        ByVal seasonID As Integer, _
                                        ByVal stepID As Integer, _
                                        ByVal partTypeID As Integer, _
                                        ByVal costTypeID As Integer, _
                                        ByVal chargeValue As Double, _
                                        ByVal calcTypeID As Integer, _
                                        ByVal errorText As String, _
                                        ByVal importStatus As Integer, _
                                        ByVal importDateTime As DateTime, _
                                        ByVal originalRateCompanyID As String, _
                                        ByVal originalSourceType As String, _
                                        ByVal originalSourceID As String, _
                                        ByVal originalSourceName As String, _
                                        ByVal originalRateClass As String, _
                                        ByVal originalEffectiveDate As String, _
                                        ByVal originalTypeOfCharge As String, _
                                        ByVal originalTier As String, _
                                        ByVal originalTimeOfUse As String, _
                                        ByVal originalSeason As String, _
                                        ByVal originalPartType As String, _
                                        ByVal originalCostType As String, _
                                        ByVal originalChargeValue As String, _
                                        ByVal originalCalcType As String, _
                                        ByVal originalStep As String, _
                                        ByVal rebateClassID As Integer, _
                                        ByVal originalRebateClassID As String) As Integer

        Dim dataRateEngineMaintenance As New dctlRateEngineMaintenance(RateEngineConnectionString)
        Dim result As Integer

        Try
            result = dataRateEngineMaintenance.InsertRateMaintData(sessionID, _
                                                                   uploadDateTime, _
                                                                   rateCompanyID, _
                                                                   rateMasterID, _
                                                                   sourceTypeID, _
                                                                   sourceID, _
                                                                   clientRateID, _
                                                                   rateDefID, _
                                                                   effectiveDate, _
                                                                   chargeType, _
                                                                   load, _
                                                                   timeOfUse, _
                                                                   seasonID, _
                                                                   stepID, _
                                                                   partTypeID, _
                                                                   costTypeID, _
                                                                   chargeValue, _
                                                                   calcTypeID, _
                                                                   errorText, _
                                                                   importStatus, _
                                                                   importDateTime, _
                                                                   originalRateCompanyID, _
                                                                   originalSourceType, _
                                                                   originalSourceID, _
                                                                   originalSourceName, _
                                                                   originalRateClass, _
                                                                   originalEffectiveDate, _
                                                                   originalTypeOfCharge, _
                                                                   originalTier, _
                                                                   originalTimeOfUse, _
                                                                   originalSeason, _
                                                                   originalPartType, _
                                                                   originalCostType, _
                                                                   originalChargeValue, _
                                                                   originalCalcType, _
                                                                   originalStep, _
                                                                   rebateClassID, _
                                                                   originalRebateClassID)


        Catch ex As Exception

            Throw New Exception(ExceptionInsertError + ": " + ex.Message, ex.InnerException)

        Finally
        End Try

        Return (result)

    End Function

    Public Function UpdateRateMaintData(ByVal rateMainDataID As Integer, _
                                        ByVal sessionID As String, _
                                        ByVal uploadDateTime As DateTime, _
                                        ByVal rateCompanyID As Integer, _
                                        ByVal rateMasterID As Integer, _
                                        ByVal sourceTypeID As Integer, _
                                        ByVal sourceID As Integer, _
                                        ByVal clientRateID As String, _
                                        ByVal rateDefID As Integer, _
                                        ByVal effectiveDate As DateTime, _
                                        ByVal chargeType As Integer, _
                                        ByVal tier As Integer, _
                                        ByVal timeOfUse As Integer, _
                                        ByVal seasonID As Integer, _
                                        ByVal stepID As Integer, _
                                        ByVal partTypeID As Integer, _
                                        ByVal costTypeID As Integer, _
                                        ByVal chargeValue As Double, _
                                        ByVal calcTypeID As Integer, _
                                        ByVal errorText As String, _
                                        ByVal importStatus As Integer, _
                                        ByVal importDateTime As DateTime, _
                                        ByVal originalRateCompanyID As String, _
                                        ByVal originalSourceType As String, _
                                        ByVal originalSourceID As String, _
                                        ByVal originalSourceName As String, _
                                        ByVal originalRateClass As String, _
                                        ByVal originalEffectiveDate As String, _
                                        ByVal originalTypeOfCharge As String, _
                                        ByVal originalTier As String, _
                                        ByVal originalTimeOfUse As String, _
                                        ByVal originalSeason As String, _
                                        ByVal originalPartType As String, _
                                        ByVal originalCostType As String, _
                                        ByVal originalChargeValue As String, _
                                        ByVal originalCalcType As String, _
                                        ByVal originalStep As String, _
                                        ByVal rebateClassID As Integer, _
                                        ByVal originalRebateClassID As String) As Integer

        Dim dataRateEngineMaintenance As New dctlRateEngineMaintenance(RateEngineConnectionString)
        Dim result As Integer

        Try
            'CR 20298, 20299, 20302 Oct 2011 - rebate class ID
            result = dataRateEngineMaintenance.UpdateRateMaintData(rateMainDataID, _
                                                                   sessionID, _
                                                                   uploadDateTime, _
                                                                   rateCompanyID, _
                                                                   rateMasterID, _
                                                                   sourceTypeID, _
                                                                   sourceID, _
                                                                   clientRateID, _
                                                                   rateDefID, _
                                                                   effectiveDate, _
                                                                   chargeType, _
                                                                   tier, _
                                                                   timeOfUse, _
                                                                   seasonID, _
                                                                   stepID, _
                                                                   partTypeID, _
                                                                   costTypeID, _
                                                                   chargeValue, _
                                                                   calcTypeID, _
                                                                   errorText, _
                                                                   importStatus, _
                                                                   importDateTime, _
                                                                   originalRateCompanyID, _
                                                                   originalSourceType, _
                                                                   originalSourceID, _
                                                                   originalSourceName, _
                                                                   originalRateClass, _
                                                                   originalEffectiveDate, _
                                                                   originalTypeOfCharge, _
                                                                   originalTier, _
                                                                   originalTimeOfUse, _
                                                                   originalSeason, _
                                                                   originalPartType, _
                                                                   originalCostType, _
                                                                   originalChargeValue, _
                                                                   originalCalcType, _
                                                                   originalStep, _
                                                                   rebateClassID, _
                                                                   originalRebateClassID)

        Catch ex As Exception

            Throw New Exception(ExceptionUpdateError + ": " + ex.Message, ex.InnerException)

        Finally
        End Try

        Return (result)

    End Function

    Public Function DeleteRateMaintDataSession(ByVal sessionID As String) As Integer

        Dim dataRateEngineMaintenance As New dctlRateEngineMaintenance(RateEngineConnectionString)
        Dim result As Integer

        result = dataRateEngineMaintenance.DeleteRateMaintDataSession(sessionID)

        Return (result)

    End Function

    Public Function GetNewestRateDefinition(ByVal rateMasterID As Integer) As Integer

        Dim dataRateEngineMaintenance As New dctlRateEngineMaintenance(RateEngineConnectionString)
        Dim result As Integer

        result = dataRateEngineMaintenance.GetNewestRateDefinition(rateMasterID)

        Return (result)

    End Function

    Public Function CloneRateDefinition(ByVal sourceRdID As Integer, _
                                        ByVal sourceRmID As Integer, _
                                        ByVal rateMasterID As Integer, _
                                        ByVal startDate As DateTime, _
                                        ByVal editDate As DateTime, _
                                        ByVal enabled As Boolean, _
                                        ByVal starDateValid As Boolean) As Integer

        Dim dataRateEngineMaintenance As New dctlRateEngineMaintenance(RateEngineConnectionString)
        Dim result As Integer

        result = dataRateEngineMaintenance.CloneRateDefinition(sourceRdID, _
                                                               sourceRmID, _
                                                               rateMasterID, _
                                                               startDate, _
                                                               editDate, _
                                                               enabled, _
                                                               starDateValid)

        Return (result)

    End Function

    Public Function GetRateDefinitionByStartDate(ByVal rateMasterID As Integer, _
                                                 ByVal startDate As DateTime) As Integer

        Dim dataRateEngineMaintenance As New dctlRateEngineMaintenance(RateEngineConnectionString)
        Dim result As Integer

        result = dataRateEngineMaintenance.GetRateDefinitionByStartDate(rateMasterID, _
                                                                        startDate)
        Return (result)

    End Function


    Private Sub ConvertRateMaintDataSessionDataReaderToRateMaintDataObject( _
                                            ByRef rateMaintDataSessionDataReader As SqlDataReader, _
                                            ByRef rateMaintData As RateMaintData)

        Try

            If (Not IsDBNull(rateMaintDataSessionDataReader("RateMaintID"))) Then
                rateMaintData.RateMaintID = CType(rateMaintDataSessionDataReader("RateMaintID"), Integer)
            End If
            If (Not IsDBNull(rateMaintDataSessionDataReader("UploadDateTime"))) Then
                rateMaintData.UploadDateTime = CType(rateMaintDataSessionDataReader("UploadDateTime"), DateTime)
            End If
            If (Not IsDBNull(rateMaintDataSessionDataReader("SessionID"))) Then
                rateMaintData.SessionID = CType(rateMaintDataSessionDataReader("SessionID"), String)
            End If
            If (Not IsDBNull(rateMaintDataSessionDataReader("RateCompanyID"))) Then
                rateMaintData.RateCompanyID = CType(rateMaintDataSessionDataReader("RateCompanyID"), Integer)
            End If
            If (Not IsDBNull(rateMaintDataSessionDataReader("RateMasterID"))) Then
                rateMaintData.RateMasterID = CType(rateMaintDataSessionDataReader("RateMasterID"), Integer)
            End If
            If (Not IsDBNull(rateMaintDataSessionDataReader("SourceTypeID"))) Then
                rateMaintData.SourceTypeID = CType(rateMaintDataSessionDataReader("SourceTypeID"), Short)
            End If
            If (Not IsDBNull(rateMaintDataSessionDataReader("SourceID"))) Then
                rateMaintData.SourceID = CType(rateMaintDataSessionDataReader("SourceID"), Integer)
            End If
            If (Not IsDBNull(rateMaintDataSessionDataReader("ClientRateID"))) Then
                rateMaintData.ClientRateID = CType(rateMaintDataSessionDataReader("ClientRateID"), String)
            End If
            If (Not IsDBNull(rateMaintDataSessionDataReader("RateDefID"))) Then
                rateMaintData.RateDefID = CType(rateMaintDataSessionDataReader("RateDefID"), Integer)
            End If
            If (Not IsDBNull(rateMaintDataSessionDataReader("EffectiveDate"))) Then
                rateMaintData.EffectiveDate = CType(rateMaintDataSessionDataReader("EffectiveDate"), DateTime)
            End If
            If (Not IsDBNull(rateMaintDataSessionDataReader("ChargeType"))) Then
                rateMaintData.ChargeType = CType(rateMaintDataSessionDataReader("ChargeType"), Enums.ChargeType)
            End If
            If (Not IsDBNull(rateMaintDataSessionDataReader("Tier"))) Then
                rateMaintData.Tier = CType(rateMaintDataSessionDataReader("Tier"), Short)
            End If
            If (Not IsDBNull(rateMaintDataSessionDataReader("TimeOfUse"))) Then
                rateMaintData.TimeOfUse = CType(rateMaintDataSessionDataReader("TimeOfUse"), Short)
            End If
            If (Not IsDBNull(rateMaintDataSessionDataReader("SeasonID"))) Then
                rateMaintData.SeasonID = CType(rateMaintDataSessionDataReader("SeasonID"), Integer)
            End If
            If (Not IsDBNull(rateMaintDataSessionDataReader("StepID"))) Then
                rateMaintData.StepID = CType(rateMaintDataSessionDataReader("StepID"), Integer)
            End If
            If (Not IsDBNull(rateMaintDataSessionDataReader("PartTypeID"))) Then
                rateMaintData.PartTypeID = CType(rateMaintDataSessionDataReader("PartTypeID"), Integer)
            End If
            If (Not IsDBNull(rateMaintDataSessionDataReader("CostTypeID"))) Then
                rateMaintData.CostTypeID = CType(rateMaintDataSessionDataReader("CostTypeID"), Integer)
            End If
            If (Not IsDBNull(rateMaintDataSessionDataReader("ChargeValue"))) Then
                rateMaintData.ChargeValue = CType(rateMaintDataSessionDataReader("ChargeValue"), Double)
            End If
            If (Not IsDBNull(rateMaintDataSessionDataReader("CalcTypeID"))) Then
                rateMaintData.CalcTypeID = CType(rateMaintDataSessionDataReader("CalcTypeID"), Integer)
            End If
            If (Not IsDBNull(rateMaintDataSessionDataReader("ErrorText"))) Then
                rateMaintData.ErrorText = CType(rateMaintDataSessionDataReader("ErrorText"), String)
            End If
            If (Not IsDBNull(rateMaintDataSessionDataReader("ImportStatus"))) Then
                rateMaintData.ImportStatus = CType(rateMaintDataSessionDataReader("ImportStatus"), Enums.ImportStatus)
            End If
            If (Not IsDBNull(rateMaintDataSessionDataReader("ImportDateTime"))) Then
                rateMaintData.ImportDateTime = CType(rateMaintDataSessionDataReader("ImportDateTime"), DateTime)
            End If
            If (Not IsDBNull(rateMaintDataSessionDataReader("OriginalRateCompanyID"))) Then
                rateMaintData.OriginalRateCompanyID = CType(rateMaintDataSessionDataReader("OriginalRateCompanyID"), String)
            End If
            If (Not IsDBNull(rateMaintDataSessionDataReader("OriginalSourceType"))) Then
                rateMaintData.OriginalSourceType = CType(rateMaintDataSessionDataReader("OriginalSourceType"), String)
            End If
            If (Not IsDBNull(rateMaintDataSessionDataReader("OriginalSourceID"))) Then
                rateMaintData.OriginalSourceID = CType(rateMaintDataSessionDataReader("OriginalSourceID"), String)
            End If
            If (Not IsDBNull(rateMaintDataSessionDataReader("OriginalSourceName"))) Then
                rateMaintData.OriginalSourceName = CType(rateMaintDataSessionDataReader("OriginalSourceName"), String)
            End If
            If (Not IsDBNull(rateMaintDataSessionDataReader("OriginalRateClass"))) Then
                rateMaintData.OriginalRateClass = CType(rateMaintDataSessionDataReader("OriginalRateClass"), String)
            End If
            If (Not IsDBNull(rateMaintDataSessionDataReader("OriginalEffectiveDate"))) Then
                rateMaintData.OriginalEffectiveDate = CType(rateMaintDataSessionDataReader("OriginalEffectiveDate"), String)
            End If
            If (Not IsDBNull(rateMaintDataSessionDataReader("OriginalTypeOfCharge"))) Then
                rateMaintData.OriginalTypeOfCharge = CType(rateMaintDataSessionDataReader("OriginalTypeOfCharge"), String)
            End If
            If (Not IsDBNull(rateMaintDataSessionDataReader("OriginalTier"))) Then
                rateMaintData.OriginalTier = CType(rateMaintDataSessionDataReader("OriginalTier"), String)
            End If
            If (Not IsDBNull(rateMaintDataSessionDataReader("OriginalTimeOfUse"))) Then
                rateMaintData.OriginalTimeOfUse = CType(rateMaintDataSessionDataReader("OriginalTimeOfUse"), String)
            End If
            If (Not IsDBNull(rateMaintDataSessionDataReader("OriginalSeason"))) Then
                rateMaintData.OriginalSeason = CType(rateMaintDataSessionDataReader("OriginalSeason"), String)
            End If
            If (Not IsDBNull(rateMaintDataSessionDataReader("OriginalPartType"))) Then
                rateMaintData.OriginalPartType = CType(rateMaintDataSessionDataReader("OriginalPartType"), String)
            End If
            If (Not IsDBNull(rateMaintDataSessionDataReader("OriginalCostType"))) Then
                rateMaintData.OriginalCostType = CType(rateMaintDataSessionDataReader("OriginalCostType"), String)
            End If
            If (Not IsDBNull(rateMaintDataSessionDataReader("OriginalChargeValue"))) Then
                rateMaintData.OriginalChargeValue = CType(rateMaintDataSessionDataReader("OriginalChargeValue"), String)
            End If
            If (Not IsDBNull(rateMaintDataSessionDataReader("originalCalcType"))) Then
                rateMaintData.OriginalCalcType = CType(rateMaintDataSessionDataReader("originalCalcType"), String)
            End If
            If (Not IsDBNull(rateMaintDataSessionDataReader("originalStep"))) Then
                rateMaintData.OriginalStep = CType(rateMaintDataSessionDataReader("originalStep"), String)
            End If

            'CR 20298, 20299, 20302 Oct 2011 - rebate class ID
            If (Not IsDBNull(rateMaintDataSessionDataReader("RebateClassID"))) Then
                rateMaintData.RebateClassID = CType(rateMaintDataSessionDataReader("RebateClassID"), Integer)
            End If
            If (Not IsDBNull(rateMaintDataSessionDataReader("originalRebateClassID"))) Then
                rateMaintData.OriginalRebateClassID = CType(rateMaintDataSessionDataReader("originalRebateClassID"), String)
            End If

        Catch ex As Exception

            Throw New Exception(ExceptionDataReaderError + ": " + ex.Message, ex.InnerException)

        Finally
        End Try

    End Sub

#End Region

#Region "Minimum Charge"

    Public Function GetMinimumCharge(ByVal rateDefID As Integer, _
                                         ByVal calcTYpeID As Short, _
                                         ByVal costTypeID As Integer) As SqlDataReader

        Dim dataRateEngineMaintenance As New dctlRateEngineMaintenance(RateEngineConnectionString)
        Dim dataReader As SqlDataReader

        Try

            dataReader = dataRateEngineMaintenance.GetMinimumCharge(rateDefID, _
                                                                        calcTYpeID, _
                                                                        costTypeID)

        Catch ex As Exception

            Throw New Exception(ExceptionDataReaderError + ": " + ex.Message, ex.InnerException)

        Finally
            'NOTE: No cleanup necessary. Caller is expected to close data reader.
        End Try

        Return (dataReader)

    End Function

    Public Function InsertMinimumCharge(ByVal rateDefID As Integer, _
                                        ByVal calcTypeID As Short, _
                                        ByVal costTypeID As Integer, _
                                        ByVal chargeValue As Double) As Integer

        Dim dataRateEngineMaintenance As New dctlRateEngineMaintenance(RateEngineConnectionString)
        Dim result As Integer

        Try
            result = dataRateEngineMaintenance.InsertMinimumCharge(rateDefID, _
                                                                   calcTypeID, _
                                                                   costTypeID, _
                                                                   chargeValue)
        Catch ex As Exception

            Throw New Exception(ExceptionInsertError + ": " + ex.Message, ex.InnerException)

        Finally
        End Try

        Return (result)

    End Function

    Public Function UpdateMinimumCharge(ByVal rateDefID As Integer, _
                                        ByVal calcTypeID As Short, _
                                        ByVal costTypeID As Integer, _
                                        ByVal calcTypeID_New As Short, _
                                        ByVal costTypeID_New As Integer, _
                                        ByVal chargeValue_New As Double) As Integer

        Dim dataRateEngineMaintenance As New dctlRateEngineMaintenance(RateEngineConnectionString)
        Dim result As Integer

        Try
            result = dataRateEngineMaintenance.UpdateMinimumCharge(rateDefID, _
                                                                   calcTypeID, _
                                                                   costTypeID, _
                                                                   calcTypeID_New, _
                                                                   costTypeID_New, _
                                                                   chargeValue_New)

        Catch ex As Exception

            Throw New Exception(ExceptionUpdateError + ": " + ex.Message, ex.InnerException)

        Finally
        End Try

        Return (result)

    End Function

#End Region

#Region "Service Charge"

    Public Function GetServiceCharge(ByVal rateDefID As Integer, _
                                         ByVal calcTypeID As Short, _
                                         ByVal costTypeID As Integer, _
                                         ByVal stepID As Short, _
                                         ByVal seasonID As Short, _
                                         ByVal rebateClassID As Short) As SqlDataReader

        Dim dataRateEngineMaintenance As New dctlRateEngineMaintenance(RateEngineConnectionString)
        Dim dataReader As SqlDataReader

        Try
            'CR 20298, 20299, 20302 Oct 2011 - rebate class
            dataReader = dataRateEngineMaintenance.GetServiceCharge(rateDefID, _
                                                                        calcTypeID, _
                                                                        costTypeID, _
                                                                        stepID, _
                                                                        seasonID, _
                                                                        rebateClassID)

        Catch ex As Exception

            Throw New Exception(ExceptionDataReaderError + ": " + ex.Message, ex.InnerException)

        Finally
            'NOTE: No cleanup necessary. Caller is expected to close data reader.
        End Try

        Return (dataReader)

    End Function

    'CR 20298, 20299, 20302 Oct 2011 - insert service charge with rebate class ID
    Public Function InsertServiceCharge(ByVal rateDefID As Integer, _
                                        ByVal calcTypeID As Short, _
                                        ByVal costTypeID As Integer, _
                                        ByVal chargeValue As Double, _
                                        ByVal stepID As Short, _
                                        ByVal seasonID As Short, _
                                        ByVal rebateClassID As Short) As Integer

        Dim dataRateEngineMaintenance As New dctlRateEngineMaintenance(RateEngineConnectionString)
        Dim result As Integer
        Try
            result = dataRateEngineMaintenance.InsertServiceCharge(rateDefID, _
                                                                   calcTypeID, _
                                                                   costTypeID, _
                                                                   chargeValue, _
                                                                   stepID, _
                                                                   seasonID, _
                                                                   rebateClassID)

        Catch ex As Exception

            Throw New Exception(ExceptionInsertError + ": " + ex.Message, ex.InnerException)

        Finally
        End Try

        Return (result)

    End Function

    Public Function UpdateServiceCharge(ByVal rateDefID As Integer, _
                                        ByVal calcTypeID As Short, _
                                        ByVal costTypeID As Integer, _
                                        ByVal stepID As Short, _
                                        ByVal seasonID As Short, _
                                        ByVal calcTypeID_New As Short, _
                                        ByVal costTypeID_New As Integer, _
                                        ByVal chargeValue_New As Double, _
                                        ByVal stepID_New As Short, _
                                        ByVal seasonID_New As Short, _
                                        ByVal rebateClassID As Short, _
                                        ByVal rebateClassID_New As Short) As Integer

        Dim dataRateEngineMaintenance As New dctlRateEngineMaintenance(RateEngineConnectionString)
        Dim result As Integer

        Try
            'CR 20298, 20299, 20302 Oct 2011 - rebate class
            result = dataRateEngineMaintenance.UpdateServiceCharge(rateDefID, _
                                                                   calcTypeID, _
                                                                   costTypeID, _
                                                                   stepID, _
                                                                   seasonID, _
                                                                   calcTypeID_New, _
                                                                   costTypeID_New, _
                                                                   chargeValue_New, _
                                                                   stepID_New, _
                                                                   seasonID_New, _
                                                                   rebateClassID, _
                                                                   rebateClassID_New)


        Catch ex As Exception

            Throw New Exception(ExceptionUpdateError + ": " + ex.Message, ex.InnerException)

        Finally
        End Try

        Return (result)

    End Function

#End Region

#Region "Tax Charge"

    Public Function GetTaxCharge(ByVal rateDefID As Integer, _
                                     ByVal costTypeID As Integer) As SqlDataReader

        Dim dataRateEngineMaintenance As New dctlRateEngineMaintenance(RateEngineConnectionString)
        Dim dataReader As SqlDataReader

        Try

            dataReader = dataRateEngineMaintenance.GetTaxCharge(rateDefID, costTypeID)

        Catch ex As Exception

            Throw New Exception(ExceptionDataReaderError + ": " + ex.Message, ex.InnerException)

        Finally
            'NOTE: No cleanup necessary. Caller is expected to close data reader.
        End Try

        Return (dataReader)

    End Function

    Public Function InsertTaxCharge(ByVal rateDefID As Integer, _
                                    ByVal costTypeID As Integer, _
                                    ByVal chargeValue As Double) As Integer

        Dim dataRateEngineMaintenance As New dctlRateEngineMaintenance(RateEngineConnectionString)
        Dim result As Integer
        Try
            result = dataRateEngineMaintenance.InsertTaxCharge(rateDefID, _
                                                               costTypeID, _
                                                               chargeValue)

        Catch ex As Exception

            Throw New Exception(ExceptionInsertError + ": " + ex.Message, ex.InnerException)

        Finally
        End Try

        Return (result)

    End Function

    Public Function UpdateTaxCharge(ByVal rateDefID As Integer, _
                                    ByVal costTypeID As Integer, _
                                    ByVal costTypeID_New As Integer, _
                                    ByVal chargeValue_New As Double) As Integer

        Dim dataRateEngineMaintenance As New dctlRateEngineMaintenance(RateEngineConnectionString)
        Dim result As Integer

        Try

            result = dataRateEngineMaintenance.UpdateTaxCharge(rateDefID, _
                                                           costTypeID, _
                                                           costTypeID_New, _
                                                           chargeValue_New)


        Catch ex As Exception

            Throw New Exception(ExceptionUpdateError + ": " + ex.Message, ex.InnerException)

        Finally
        End Try

        Return (result)

    End Function

#End Region

#Region "Use Charge"

    Public Function GetUseCharge(ByVal rateDefID As Integer, _
                                     ByVal tierID As Short, _
                                     ByVal timeOfUse As Short, _
                                     ByVal seasonID As Short, _
                                     ByVal costTypeID As Integer, _
                                     ByVal partTypeID As Short) As SqlDataReader

        Dim dataRateEngineMaintenance As New dctlRateEngineMaintenance(RateEngineConnectionString)
        Dim dataReader As SqlDataReader

        Try

            dataReader = dataRateEngineMaintenance.GetUseCharge(rateDefID, _
                                                                    tierID, _
                                                                    timeOfUse, _
                                                                    seasonID, _
                                                                    costTypeID, _
                                                                    partTypeID)

        Catch ex As Exception

            Throw New Exception(ExceptionDataReaderError + ": " + ex.Message, ex.InnerException)

        Finally
            'NOTE: No cleanup necessary. Caller is expected to close data reader.
        End Try

        Return (dataReader)

    End Function

    Public Function InsertUseCharge(ByVal rateDefID As Integer, _
                                    ByVal tierID As Short, _
                                    ByVal touID As Short, _
                                    ByVal seasonID As Short, _
                                    ByVal costTypeID As Integer, _
                                    ByVal partTypeID As Short, _
                                    ByVal chargeValue As Double, _
                                    ByVal rgID As Integer, _
                                    ByVal description As String, _
                                    ByVal rTPgID As Integer) As Integer

        Dim dataRateEngineMaintenance As New dctlRateEngineMaintenance(RateEngineConnectionString)
        Dim result As Integer
        Try
            result = dataRateEngineMaintenance.InsertUseCharge(rateDefID, _
                                                               tierID, _
                                                               touID, _
                                                               seasonID, _
                                                               costTypeID, _
                                                               partTypeID, _
                                                               chargeValue, _
                                                               rgID, _
                                                               description, _
                                                               rTPgID)

        Catch ex As Exception

            Throw New Exception(ExceptionInsertError + ": " + ex.Message, ex.InnerException)

        Finally
        End Try

        Return (result)

    End Function

    Public Function UpdateUseCharge(ByVal rateDefID As Integer, _
                                    ByVal tierID As Short, _
                                    ByVal touID As Short, _
                                    ByVal seasonID As Short, _
                                    ByVal costTypeID As Integer, _
                                    ByVal partTypeID As Short, _
                                    ByVal tierID_New As Short, _
                                    ByVal touID_New As Short, _
                                    ByVal seasonID_New As Short, _
                                    ByVal costTypeID_New As Integer, _
                                    ByVal partTypeID_New As Short, _
                                    ByVal chargeValue_New As Double, _
                                    ByVal rgID_New As Integer, _
                                    ByVal description_New As String, _
                                    ByVal rTPgID_New As Integer) As Integer

        Dim dataRateEngineMaintenance As New dctlRateEngineMaintenance(RateEngineConnectionString)
        Dim result As Integer

        Try

            result = dataRateEngineMaintenance.UpdateUseCharge(rateDefID, _
                                                               tierID, _
                                                               touID, _
                                                               seasonID, _
                                                               costTypeID, _
                                                               partTypeID, _
                                                               tierID_New, _
                                                               touID_New, _
                                                               seasonID_New, _
                                                               costTypeID_New, _
                                                               partTypeID_New, _
                                                               chargeValue_New, _
                                                               rgID_New, _
                                                               description_New, _
                                                               rTPgID_New)

        Catch ex As Exception

            Throw New Exception(ExceptionUpdateError + ": " + ex.Message, ex.InnerException)

        Finally
        End Try

        Return (result)

    End Function

#End Region

#Region "Validation Helper Methods"

    Private Function IsValidSourceTypeID(ByVal sourceTypeID As String) As Boolean

        Return System.Enum.IsDefined(GetType(Enums.SourceType), sourceTypeID)

    End Function

    Private Function IsValidChargeType(ByVal chargeType As String) As Boolean

        Return System.Enum.IsDefined(GetType(Enums.ChargeType), chargeType)

    End Function

    Private Function ConvertEnumTextToValue(ByVal value As String, _
                                            ByVal type As Type) As System.Enum

        Return CType(System.Enum.Parse(type, value), System.Enum)

    End Function


    Private Sub AppendValidationErrorToErrorText(ByRef errorText As StringBuilder, _
                                                      ByVal validation As String)
        If errorText.Length = 0 Then

            'Append validation error for the first time.
            errorText.Append(ValidationErrorPrefix)
            errorText.Append(validation)

        Else

            'Append validation error to previous validation error(s).
            errorText.Append(", ")
            errorText.Append(validation)

        End If

    End Sub

    Private Sub AppendValidationWarningToWarningText(ByRef warningText As StringBuilder, _
                                                          ByVal validation As String)
        If warningText.Length = 0 Then

            'Append validation error for the first time.
            warningText.Append(ValidationWarningPrefix)
            warningText.Append(validation)

        Else

            'Append validation error to previous validation error(s).
            warningText.Append(", ")
            warningText.Append(validation)

        End If

    End Sub

    Private Sub AppendImportErrorToErrorText(ByRef errorText As StringBuilder, _
                                                  ByVal importError As String)
        If errorText.Length = 0 Then

            'Append import error for the first time.
            errorText.Append(ImportErrorPrefix)
            errorText.Append(importError)

        Else

            'Append import error to previous import error(s).
            errorText.Append(", ")
            errorText.Append(importError)

        End If

    End Sub

    Private Function IsTextValidForType(ByRef value As String, _
                                        ByVal type As Type) As Boolean

        Dim testInteger As Integer
        Dim testShort As Short
        Dim testDateTime As DateTime
        Dim result As Boolean

        Try

            Select Case type.ToString

                Case GetType(Integer).ToString

                    testInteger = CType(value, Integer)
                    result = True

                Case GetType(Double).ToString

                    testInteger = CType(value, Integer)
                    result = True

                Case GetType(Short).ToString
                    testShort = CType(value, Short)
                    result = True

                Case GetType(DateTime).ToString
                    testDateTime = CType(value, DateTime)
                    result = True

            End Select

        Catch ex As Exception

            result = False

        End Try

        Return (result)


    End Function

    Private Sub RetrieveValidateDataRequiredForChargeType(ByRef rateMaintData As RateMaintData, _
                                                               ByRef errorText As StringBuilder)


        Select Case rateMaintData.ChargeType

            Case Enums.ChargeType.Minimum

                RetrieveValidateChargeCalcType(rateMaintData, errorText)
                RetrieveValidateCostType(rateMaintData, errorText)

            Case Enums.ChargeType.Service

                RetrieveValidateChargeCalcType(rateMaintData, errorText)
                RetrieveValidateCostType(rateMaintData, errorText)
                RetrieveValidateStep(rateMaintData, errorText, True)
                RetrieveValidateSeason(rateMaintData, errorText, True)
                'CR 20298, 20299, 20302 Oct 2011 - rebate class
                RetrieveValidateRebateClass(rateMaintData, errorText, True)

            Case Enums.ChargeType.Tax

                RetrieveValidateCostType(rateMaintData, errorText)

            Case Enums.ChargeType.Use

                RetrieveValidateTier(rateMaintData, errorText, True)
                RetrieveValidateTimeOfUse(rateMaintData, errorText, True)
                RetrieveValidateSeason(rateMaintData, errorText, True)
                RetrieveValidateCostType(rateMaintData, errorText)
                RetrieveValidatePartType(rateMaintData, errorText, True)

            Case Else

                AppendValidationErrorToErrorText(errorText, ValidationErrorInvalidChargeType)

        End Select

        'Retrieve and validate charge value.
        If IsTextValidForType(rateMaintData.OriginalChargeValue, GetType(Double)) Then
            rateMaintData.ChargeValue = Double.Parse(rateMaintData.OriginalChargeValue)
        Else
            AppendValidationErrorToErrorText(errorText, ValidationErrorInvalidChargeValue)
        End If


    End Sub


    Private Sub RetrieveValidateChargeCalcType(ByRef rateMaintData As RateMaintData, _
                                       ByRef errorText As StringBuilder)

        Dim chargeCalcTypeListDataReader As SqlDataReader = Nothing
        Dim result As Boolean

        Try
            chargeCalcTypeListDataReader = GetChargeCalcTypeList()

            Do While chargeCalcTypeListDataReader.Read()

                If (Not IsDBNull(chargeCalcTypeListDataReader("CaName"))) Then

                    If CType(chargeCalcTypeListDataReader("CaName"), String) = rateMaintData.OriginalCalcType Then
                        rateMaintData.CalcTypeID = CType(chargeCalcTypeListDataReader("CalcTypeID"), Integer)
                        result = True
                        Exit Do
                    End If

                End If

            Loop

            If result = False Then
                AppendValidationErrorToErrorText(errorText, ValidationErrorInvalidChargeCalcType)
            End If

        Finally
            If (Not chargeCalcTypeListDataReader Is Nothing) Then
                If (Not chargeCalcTypeListDataReader.IsClosed) Then
                    chargeCalcTypeListDataReader.Close()
                End If
            End If
        End Try

    End Sub

    Private Sub RetrieveValidateStep(ByRef rateMaintData As RateMaintData, _
                                     ByRef errorText As StringBuilder, _
                                     ByVal required As Boolean)

        Dim stepListDataReader As SqlDataReader = Nothing
        Dim result As Boolean

        Try

            stepListDataReader = GetStep()

            Do While stepListDataReader.Read()

                If (Not IsDBNull(stepListDataReader("StName"))) Then

                    If CType(stepListDataReader("StName"), String) = rateMaintData.OriginalStep Then
                        rateMaintData.StepID = CType(stepListDataReader("StepID"), Integer)
                        result = True
                        Exit Do
                    End If

                End If

            Loop

            If required = True And result = False Then
                AppendValidationErrorToErrorText(errorText, ValidationErrorInvalidStep)
            End If

        Finally
            If (Not stepListDataReader Is Nothing) Then
                If (Not stepListDataReader.IsClosed) Then
                    stepListDataReader.Close()
                End If
            End If
        End Try

    End Sub

    Private Sub RetrieveValidateSeason(ByRef rateMaintData As RateMaintData, _
                                       ByRef errorText As StringBuilder, _
                                       ByVal required As Boolean)

        Dim seasonListDataReader As SqlDataReader = Nothing
        Dim result As Boolean

        Try

            seasonListDataReader = GetSeasonList()

            Do While seasonListDataReader.Read()

                If (Not IsDBNull(seasonListDataReader("SeName"))) Then

                    If CType(seasonListDataReader("SeName"), String) = rateMaintData.OriginalSeason Then
                        rateMaintData.SeasonID = CType(seasonListDataReader("SeasonID"), Integer)
                        result = True
                        Exit Do
                    End If

                End If

            Loop

            If required = True And result = False Then
                AppendValidationErrorToErrorText(errorText, ValidationErrorInvalidSeason)
            End If

        Finally
            If (Not seasonListDataReader Is Nothing) Then
                If (Not seasonListDataReader.IsClosed) Then
                    seasonListDataReader.Close()
                End If
            End If
        End Try

    End Sub

    'CR 20298, 20299, 20302 Oct 2011 - rebate class
    Private Sub RetrieveValidateRebateClass(ByRef rateMaintData As RateMaintData, _
                                   ByRef errorText As StringBuilder, _
                                   ByVal required As Boolean)

        Dim rebateClassListDataReader As SqlDataReader = Nothing
        Dim result As Boolean

        Try

            rebateClassListDataReader = GetRebateClassList()

            Do While rebateClassListDataReader.Read()

                If (Not IsDBNull(rebateClassListDataReader("RCName"))) Then

                    If CType(rebateClassListDataReader("RCName"), String).ToLower = rateMaintData.OriginalSeason.ToLower Then
                        rateMaintData.RebateClassID = CType(rebateClassListDataReader("RebateClassID"), Integer)
                        result = True
                        Exit Do
                    End If

                End If

            Loop

            If required = True And result = False Then
                AppendValidationErrorToErrorText(errorText, ValidationErrorInvalidRebateClass)
            End If

        Finally
            If (Not rebateClassListDataReader Is Nothing) Then
                If (Not rebateClassListDataReader.IsClosed) Then
                    rebateClassListDataReader.Close()
                End If
            End If
        End Try

    End Sub
    Private Sub RetrieveValidateTier(ByRef rateMaintData As RateMaintData, _
                                     ByRef errorText As StringBuilder, _
                                     ByVal required As Boolean)

        Dim tierListDataReader As SqlDataReader = Nothing
        Dim result As Boolean

        Try
            tierListDataReader = GetTierList()

            Do While tierListDataReader.Read()

                If (Not IsDBNull(tierListDataReader("TiName"))) Then

                    If CType(tierListDataReader("TiName"), String) = rateMaintData.OriginalTier Then
                        rateMaintData.Tier = CType(tierListDataReader("TierID"), Short)
                        result = True
                        Exit Do
                    End If

                End If

            Loop

            If required = True And result = False Then
                AppendValidationErrorToErrorText(errorText, ValidationErrorInvalidTier)
            End If

        Finally
            If (Not tierListDataReader Is Nothing) Then
                If (Not tierListDataReader.IsClosed) Then
                    tierListDataReader.Close()
                End If
            End If
        End Try

    End Sub

    Private Sub RetrieveValidateTimeOfUse(ByRef rateMaintData As RateMaintData, _
                                          ByRef errorText As StringBuilder, _
                                          ByVal required As Boolean)

        Dim timeOfUseListDataReader As SqlDataReader = Nothing
        Dim result As Boolean

        Try
            timeOfUseListDataReader = GetTimeOfUseList()

            Do While timeOfUseListDataReader.Read()

                If (Not IsDBNull(timeOfUseListDataReader("TuName"))) Then

                    If CType(timeOfUseListDataReader("TuName"), String) = rateMaintData.OriginalTimeOfUse Then
                        rateMaintData.TimeOfUse = CType(timeOfUseListDataReader("TouID"), Short)
                        result = True
                        Exit Do
                    End If

                End If

            Loop

            If required = True And result = False Then
                AppendValidationErrorToErrorText(errorText, ValidationErrorInvalidTimeOfUse)
            End If

        Finally
            If (Not timeOfUseListDataReader Is Nothing) Then
                If (Not timeOfUseListDataReader.IsClosed) Then
                    timeOfUseListDataReader.Close()
                End If
            End If
        End Try

    End Sub

    Private Sub RetrieveValidatePartType(ByRef rateMaintData As RateMaintData, _
                                         ByRef errorText As StringBuilder, _
                                         ByVal required As Boolean)

        Dim partTypeListDataReader As SqlDataReader = Nothing
        Dim result As Boolean

        Try
            partTypeListDataReader = GetPartTypeList()

            Do While partTypeListDataReader.Read()

                If (Not IsDBNull(partTypeListDataReader("PtName"))) Then

                    If CType(partTypeListDataReader("PtName"), String) = rateMaintData.OriginalPartType Then
                        rateMaintData.PartTypeID = CType(partTypeListDataReader("PartTypeID"), Short)
                        result = True
                        Exit Do
                    End If

                End If

            Loop

            If required = True And result = False Then
                AppendValidationErrorToErrorText(errorText, ValidationErrorInvalidPartType)
            End If

        Finally
            If (Not partTypeListDataReader Is Nothing) Then
                If (Not partTypeListDataReader.IsClosed) Then
                    partTypeListDataReader.Close()
                End If
            End If
        End Try

    End Sub

    Private Function RetrieveValidateCostType(ByRef rateMaintData As RateMaintData, _
                                      ByRef errorText As StringBuilder) As Boolean

        Dim costTypeListDataReader As SqlDataReader = Nothing
        Dim result As Boolean

        Try
            costTypeListDataReader = GetCostTypeList()

            Do While costTypeListDataReader.Read()

                If (Not IsDBNull(costTypeListDataReader("ctName"))) Then
                    If CType(costTypeListDataReader("ctName"), String) = rateMaintData.OriginalCostType Then
                        rateMaintData.CostTypeID = CType(costTypeListDataReader("CostTypeID"), Integer)
                        result = True
                        Exit Do
                    End If
                End If

            Loop

            If result = False Then
                AppendValidationErrorToErrorText(errorText, ValidationErrorInvalidCostType)
            End If

        Finally
            If (Not costTypeListDataReader Is Nothing) Then
                If (Not costTypeListDataReader.IsClosed) Then
                    costTypeListDataReader.Close()
                End If
            End If
        End Try

        Return result

    End Function

#End Region


#Region "Import Helper Methods"

    Private Sub ImportMinimumCharge(ByRef rateMaintData As RateMaintData)

        Dim minimumChargeList As SqlDataReader = Nothing

        Try
            minimumChargeList = GetMinimumCharge(rateMaintData.RateDefID, _
                                                     CShort(rateMaintData.CalcTypeID), _
                                                     rateMaintData.CostTypeID)

            If minimumChargeList.HasRows = True Then

                UpdateMinimumCharge(rateMaintData.RateDefID, _
                                    CShort(rateMaintData.CalcTypeID), _
                                    rateMaintData.CostTypeID, _
                                    CShort(rateMaintData.CalcTypeID), _
                                    rateMaintData.CostTypeID, _
                                    rateMaintData.ChargeValue)

            Else

                InsertMinimumCharge(rateMaintData.RateDefID, _
                                    CShort(rateMaintData.CalcTypeID), _
                                    rateMaintData.CostTypeID, _
                                    rateMaintData.ChargeValue)

            End If

        Finally
            If (Not minimumChargeList Is Nothing) Then
                If (Not minimumChargeList.IsClosed) Then
                    minimumChargeList.Close()
                End If
            End If
        End Try

    End Sub

    Private Sub ImportServiceCharge(ByRef rateMaintData As RateMaintData)

        Dim serviceChargeList As SqlDataReader = Nothing

        Try
            'CR 20298, 20299, 20302 Oct 2011 - rebate class
            serviceChargeList = GetServiceCharge(rateMaintData.RateDefID, _
                                                     CShort(rateMaintData.CalcTypeID), _
                                                     rateMaintData.CostTypeID, _
                                                     CShort(rateMaintData.StepID), _
                                                     CShort(rateMaintData.SeasonID), _
                                                     CShort(rateMaintData.RebateClassID))

            If serviceChargeList.HasRows = True Then
                'CR 20298, 20299, 20302 Oct 2011 - pass rebate class id to update
                UpdateServiceCharge(rateMaintData.RateDefID, _
                                    CShort(rateMaintData.CalcTypeID), _
                                    rateMaintData.CostTypeID, _
                                    CShort(rateMaintData.StepID), _
                                    CShort(rateMaintData.SeasonID), _
                                    CShort(rateMaintData.CalcTypeID), _
                                    rateMaintData.CostTypeID, _
                                    rateMaintData.ChargeValue, _
                                    CShort(rateMaintData.StepID), _
                                    CShort(rateMaintData.SeasonID), _
                                    CShort(rateMaintData.RebateClassID), _
                                    CShort(rateMaintData.RebateClassID))

            Else
                'CR 20298, 20299, 20302 Oct 2011 - pass rebate class ID to insert
                InsertServiceCharge(rateMaintData.RateDefID, _
                                    CShort(rateMaintData.CalcTypeID), _
                                    rateMaintData.CostTypeID, _
                                    rateMaintData.ChargeValue, _
                                    CShort(rateMaintData.StepID), _
                                    CShort(rateMaintData.SeasonID), _
                                    CShort(rateMaintData.RebateClassID))

            End If

        Finally
            If (Not serviceChargeList Is Nothing) Then
                If (Not serviceChargeList.IsClosed) Then
                    serviceChargeList.Close()
                End If
            End If
        End Try

    End Sub

    Private Sub ImportTaxCharge(ByRef rateMaintData As RateMaintData)

        Dim taxChargeList As SqlDataReader = Nothing

        Try
            taxChargeList = GetTaxCharge(rateMaintData.RateDefID, _
                                                 rateMaintData.CostTypeID)

            If taxChargeList.HasRows = True Then

                UpdateTaxCharge(rateMaintData.RateDefID, _
                                rateMaintData.CostTypeID, _
                                rateMaintData.CostTypeID, _
                                rateMaintData.ChargeValue)

            Else

                InsertTaxCharge(rateMaintData.RateDefID, _
                                rateMaintData.CostTypeID, _
                                rateMaintData.ChargeValue)

            End If

        Finally
            If (Not taxChargeList Is Nothing) Then
                If (Not taxChargeList.IsClosed) Then
                    taxChargeList.Close()
                End If
            End If
        End Try

    End Sub

    Private Sub ImportUseCharge(ByRef rateMaintData As RateMaintData)

        Dim useChargeList As SqlDataReader = Nothing
        Dim rgID As Integer = 0
        Dim rTPgID As Integer = 0
        Dim description As String = String.Empty

        Try
            useChargeList = GetUseCharge(rateMaintData.RateDefID, _
                                             rateMaintData.Tier, _
                                             rateMaintData.TimeOfUse, _
                                             CShort(rateMaintData.SeasonID), _
                                             rateMaintData.CostTypeID, _
                                             CShort(rateMaintData.PartTypeID))


            If useChargeList.HasRows = True Then

                Do While useChargeList.Read()

                    If Not IsDBNull(useChargeList("rgID")) Then
                        rgID = CType(useChargeList("rgID"), Integer)
                    End If

                    If Not IsDBNull(useChargeList("rTPgID")) Then
                        rTPgID = CType(useChargeList("rTPgID"), Integer)
                    End If

                    If Not IsDBNull(useChargeList("Description")) Then
                        description = CType(useChargeList("Description"), String)
                    End If

                    Exit Do

                Loop

                UpdateUseCharge(rateMaintData.RateDefID, _
                                rateMaintData.Tier, _
                                rateMaintData.TimeOfUse, _
                                CShort(rateMaintData.SeasonID), _
                                rateMaintData.CostTypeID, _
                                CShort(rateMaintData.PartTypeID), _
                                rateMaintData.Tier, _
                                rateMaintData.TimeOfUse, _
                                CShort(rateMaintData.SeasonID), _
                                CShort(rateMaintData.CostTypeID), _
                                CShort(rateMaintData.PartTypeID), _
                                rateMaintData.ChargeValue, _
                                rgID, _
                                description, _
                                rTPgID)

            Else

                InsertUseCharge(rateMaintData.RateDefID, _
                                rateMaintData.Tier, _
                                rateMaintData.TimeOfUse, _
                                CShort(rateMaintData.SeasonID), _
                                rateMaintData.CostTypeID, _
                                CShort(rateMaintData.PartTypeID), _
                                rateMaintData.ChargeValue, _
                                rgID, _
                                description, _
                                rTPgID)

            End If

        Finally
            If (Not useChargeList Is Nothing) Then
                If (Not useChargeList.IsClosed) Then
                    useChargeList.Close()
                End If
            End If
        End Try

    End Sub

#End Region

#Region "Cost Type"

    Public Function GetCostTypeList() As SqlDataReader

        Dim dataRateEngineMaintenance As New dctlRateEngineMaintenance(RateEngineConnectionString)
        Dim dataReader As SqlDataReader

        Try

            dataReader = dataRateEngineMaintenance.GetCostTypeList()

        Catch ex As Exception

            Throw New Exception(ExceptionDataReaderError + ": " + ex.Message, ex.InnerException)

        Finally
            'NOTE: No cleanup necessary. Caller is expected to close data reader.
        End Try

        Return (dataReader)

    End Function
#End Region

#Region "Chart Calculation Type"

    Public Function GetChargeCalcTypeList() As SqlDataReader

        Dim dataRateEngineMaintenance As New dctlRateEngineMaintenance(RateEngineConnectionString)
        Dim dataReader As SqlDataReader

        Try

            dataReader = dataRateEngineMaintenance.GetChargeCalcTypeList()

        Catch ex As Exception

            Throw New Exception(ExceptionDataReaderError + ": " + ex.Message, ex.InnerException)

        Finally
            'NOTE: No cleanup necessary. Caller is expected to close data reader.
        End Try

        Return (dataReader)

    End Function
#End Region

#Region "Step"

    Public Function GetStep() As SqlDataReader

        Dim dataRateEngineMaintenance As New dctlRateEngineMaintenance(RateEngineConnectionString)
        Dim dataReader As SqlDataReader

        Try

            dataReader = dataRateEngineMaintenance.GetStepList()

        Catch ex As Exception

            Throw New Exception(ExceptionDataReaderError + ": " + ex.Message, ex.InnerException)

        Finally
            'NOTE: No cleanup necessary. Caller is expected to close data reader.
        End Try

        Return (dataReader)

    End Function

#End Region

#Region "Season"

    Public Function GetSeasonList() As SqlDataReader

        Dim dataRateEngineMaintenance As New dctlRateEngineMaintenance(RateEngineConnectionString)
        Dim dataReader As SqlDataReader

        Try

            dataReader = dataRateEngineMaintenance.GetSeasonList()

        Catch ex As Exception

            Throw New Exception(ExceptionDataReaderError + ": " + ex.Message, ex.InnerException)

        Finally
            'NOTE: No cleanup necessary. Caller is expected to close data reader.
        End Try

        Return (dataReader)

    End Function

#End Region

#Region "Tier"

    Public Function GetTierList() As SqlDataReader

        Dim dataRateEngineMaintenance As New dctlRateEngineMaintenance(RateEngineConnectionString)
        Dim dataReader As SqlDataReader

        Try

            dataReader = dataRateEngineMaintenance.GetTierList()

        Catch ex As Exception

            Throw New Exception(ExceptionDataReaderError + ": " + ex.Message, ex.InnerException)

        Finally
            'NOTE: No cleanup necessary. Caller is expected to close data reader.
        End Try

        Return (dataReader)

    End Function

#End Region

#Region "Time of Use"

    Public Function GetTimeOfUseList() As SqlDataReader

        Dim dataRateEngineMaintenance As New dctlRateEngineMaintenance(RateEngineConnectionString)
        Dim dataReader As SqlDataReader

        Try

            dataReader = dataRateEngineMaintenance.GetTimeOfUseList()

        Catch ex As Exception

            Throw New Exception(ExceptionDataReaderError + ": " + ex.Message, ex.InnerException)

        Finally
            'NOTE: No cleanup necessary. Caller is expected to close data reader.
        End Try

        Return (dataReader)

    End Function

#End Region

#Region "Part Type"

    Public Function GetPartTypeList() As SqlDataReader

        Dim dataRateEngineMaintenance As New dctlRateEngineMaintenance(RateEngineConnectionString)
        Dim dataReader As SqlDataReader

        Try

            dataReader = dataRateEngineMaintenance.GetPartTypeList()

        Catch ex As Exception

            Throw New Exception(ExceptionDataReaderError + ": " + ex.Message, ex.InnerException)

        Finally
            'NOTE: No cleanup necessary. Caller is expected to close data reader.
        End Try

        Return (dataReader)

    End Function

#End Region

#Region "Rebate Class"
    ''' <summary>
    ''' CR 20298, 20299, 20302 Oct 2011 - get rebate class list
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function GetRebateClassList() As SqlDataReader

        Dim dataRateEngineMaintenance As New dctlRateEngineMaintenance(RateEngineConnectionString)
        Dim dataReader As SqlDataReader

        Try

            dataReader = dataRateEngineMaintenance.GetRebateClassList()

        Catch ex As Exception

            Throw New Exception(ExceptionDataReaderError + ": " + ex.Message, ex.InnerException)

        Finally
            'NOTE: No cleanup necessary. Caller is expected to close data reader.
        End Try

        Return (dataReader)

    End Function
#End Region

#Region "Logging Helper Methods"

    Public Function GetLogHistory(ByVal guidSession As Guid) As LogEntryCollection
        Dim objDC As dctlRateEngine
        Dim objLogEntries As LogEntryCollection

        objDC = New dctlRateEngine(_rateEngineConnectionString)
        objLogEntries = objDC.GetLogHistory(guidSession)

        Return (objLogEntries)
    End Function

    Private Function CreateLogSessionGUID() As System.Guid
        Return (Guid.NewGuid())
    End Function

    Private Function BeginLogSession() As Boolean
        Dim bRetval As Boolean

        If (IsLoggingEnabled) Then
            _logSessionID = CreateLogSessionGUID()
            bRetval = True
        End If

        Return (bRetval)
    End Function

    ' There may be several "Log" overloads, depending on what objects need to be concatenated into
    ' strings for logging.
    Private Function Log(ByVal sText As String) As Boolean
        Dim bRetval As Boolean
        Dim objDC As dctlRateEngine

        If (IsLoggingEnabled) Then
            objDC = New dctlRateEngine(_rateEngineConnectionString)
            objDC.InsertLog(LogSessionID, sText)
            bRetval = True
        End If

        Return (bRetval)
    End Function

    Private Function Log(ByVal sText As String, ByVal eCostType As CostType) As Boolean
        Dim bRetval As Boolean

        If (IsLoggingEnabled) Then
            Dim sb As New System.Text.StringBuilder
            sb.Append(sText)
            sb.Append(",")
            sb.Append(eCostType.ToString())
            Log(sb.ToString)
            sb.Remove(0, sb.Length)
            bRetval = True
        End If

        Return (bRetval)
    End Function

    Private Function Log(ByVal objUsages As UsageCollection) As Boolean
        Dim bRetval As Boolean
        Dim objUsage As Usage

        If (IsLoggingEnabled) Then
            Dim sb As New System.Text.StringBuilder
            For Each objUsage In objUsages
                sb.Append("Usage [")
                sb.Append(objUsage.BaseOrTier.ToString)
                sb.Append(",")
                sb.Append(objUsage.TimeOfUse.ToString)
                sb.Append(",")
                sb.Append(objUsage.Season.ToString)
                sb.Append(",")
                sb.Append(objUsage.Quantity)
                If (objUsage.UseTypeName <> String.Empty) Then
                    sb.Append(":mapped:")
                    sb.Append(objUsage.UseTypeName)
                End If
                sb.Append("]")
                Log(sb.ToString)
                sb.Remove(0, sb.Length)
            Next
            bRetval = True
        End If

        Return (bRetval)
    End Function

    Private Function Log(ByVal objCostResult As CostResult) As Boolean
        Dim bRetval As Boolean
        Dim objDE As DictionaryEntry
        Dim objCost As Cost

        If (IsLoggingEnabled) Then
            Dim sb As New System.Text.StringBuilder
            sb.Append("CostResult [TotalCost=")
            sb.Append(objCostResult.TotalCost)
            sb.Append(",AvgDailyUse=")
            sb.Append(objCostResult.AvgDailyEnergyUse)
            sb.Append(",BillDays=")
            sb.Append(objCostResult.BillDays)
            sb.Append(",RateCountApplied=")
            sb.Append(objCostResult.RateCountApplied)
            sb.Append(",TotalUse=")
            sb.Append(objCostResult.TotalUse)
            sb.Append("]")
            Log(sb.ToString)
            sb.Remove(0, sb.Length)

            For Each objDE In objCostResult.CostCollection
                objCost = CType(objDE.Value, Cost)
                sb.Append("Cost [")
                sb.Append(objCost.CostType.ToString)
                sb.Append(",")
                sb.Append(objCost.Amount)
                sb.Append("]")
                Log(sb.ToString)
                sb.Remove(0, sb.Length)
            Next
            bRetval = True
        End If

        Return (bRetval)
    End Function

    Private Function Log(ByVal objDef As DetailedRateDefinition) As Boolean
        Dim bRetval As Boolean

        If (IsLoggingEnabled) Then
            Dim sb As New System.Text.StringBuilder

            sb.Append("DetailedRateDefinition [ID=")
            sb.Append(objDef.DefinitionID)
            sb.Append(",masterID=")
            sb.Append(objDef.MasterID)
            sb.Append(",RateCompanyID=")
            sb.Append(objDef.RateCompanyID)
            sb.Append(",ProrateType=")
            sb.Append(objDef.ProrateType)
            sb.Append(",FuelType=")
            sb.Append(objDef.FuelType)
            sb.Append("]")
            Log(sb.ToString)
            sb.Remove(0, sb.Length)

            sb.Append("...[RateClass=")
            sb.Append(objDef.RateClass)
            sb.Append(",masterID=")
            sb.Append(objDef.MasterID)
            sb.Append(",IsBasic=")
            sb.Append(objDef.IsBasic)
            sb.Append(",IsTiered=")
            sb.Append(objDef.IsTiered)
            sb.Append(",IsTOU=")
            sb.Append(objDef.IsTimeOfUse)
            sb.Append(",IsSeasonal=")
            sb.Append(objDef.IsSeasonal)
            If (objDef.IsSeasonal) Then
                sb.Append(",SpID=")
                sb.Append(objDef.SeasonalProrateType)
            End If
            sb.Append(",StartDate=")
            sb.Append(objDef.StartDate.ToShortDateString)
            sb.Append("]")
            Log(sb.ToString)
            sb.Remove(0, sb.Length)

            bRetval = True
        End If

        Return (bRetval)
    End Function

    Private Function Log(ByVal arrProrateFactors() As Double) As Boolean
        Dim bRetval As Boolean
        Dim i As Integer
        Dim dblTotal As Double

        If (IsLoggingEnabled) Then
            Dim sb As New System.Text.StringBuilder

            sb.Append("...[ProrateFactors=")

            For i = 0 To arrProrateFactors.Length - 1
                sb.Append(arrProrateFactors(i).ToString())
                sb.Append(" : ")
                If (i Mod 3) = 1 Then
                    Log(sb.ToString)
                    sb.Remove(0, sb.Length)
                End If
                dblTotal = dblTotal + arrProrateFactors(i)
            Next

            sb.Append(":Sum=")
            sb.Append(dblTotal.ToString())
            sb.Append("]")
            Log(sb.ToString)
            sb.Remove(0, sb.Length)

            bRetval = True
        End If

        Return (bRetval)
    End Function

    Private Function Log(ByVal bUseSeasonalProrate As Boolean, ByVal eSeason As Season) As Boolean
        Dim bRetval As Boolean

        If (IsLoggingEnabled) Then
            Dim sb As New System.Text.StringBuilder

            sb.Append("...[SeasonalProrateOverride")

            sb.Append("=")
            sb.Append(bUseSeasonalProrate.ToString())
            If (bUseSeasonalProrate) Then
                sb.Append(":Season=")
                sb.Append(eSeason.ToString())
            End If
            sb.Append("]")
            Log(sb.ToString)
            sb.Remove(0, sb.Length)

            bRetval = True
        End If

        Return (bRetval)
    End Function

    Private Function Log(ByVal objUseCharges As UseChargeCollection) As Boolean
        Dim bRetval As Boolean
        Dim objUseCharge As UseCharge

        If (IsLoggingEnabled) Then
            Dim sb As New System.Text.StringBuilder
            For Each objUseCharge In objUseCharges
                sb.Append("...UseCharge [")
                sb.Append(objUseCharge.BaseOrTier.ToString)
                sb.Append(",")
                sb.Append(objUseCharge.TimeOfUse.ToString)
                sb.Append(",")
                sb.Append(objUseCharge.Season.ToString)
                sb.Append(",")
                sb.Append(objUseCharge.PartType.ToString)
                sb.Append(",FCR=")
                sb.Append(objUseCharge.FuelCostRecovery.ToString)
                If (objUseCharge.FuelCostRecovery) Then
                    sb.Append(",group=")
                    sb.Append(objUseCharge.FCRGroup.ToString)
                End If
                sb.Append(",charge=")
                sb.Append(objUseCharge.ChargeValue)
                sb.Append("]")
                Log(sb.ToString)
                sb.Remove(0, sb.Length)
            Next
            bRetval = True
        End If

        Return (bRetval)
    End Function

    Private Function Log(ByVal sText As String, ByVal dblAmount As Double, ByVal dblValA As Double, ByVal dblValB As Double, ByVal eCostType As CostType) As Boolean
        Dim bRetval As Boolean

        If (IsLoggingEnabled) Then
            Dim sb As New System.Text.StringBuilder

            sb.Append("...[")
            sb.Append(sText)
            sb.Append("][")
            sb.Append(eCostType.ToString())
            sb.Append("] Amount=")
            sb.Append(dblAmount.ToString)
            sb.Append(": ")
            sb.Append(dblValA.ToString)
            sb.Append("*")
            sb.Append(dblValB.ToString)
            Log(sb.ToString)
            sb.Remove(0, sb.Length)

            bRetval = True
        End If

        Return (bRetval)
    End Function

    Private Function Log(ByVal sText As String, ByVal eStep As ServiceStep, ByVal dblAmount As Double, ByVal dblValA As Double, ByVal dblValB As Double, ByVal dblValC As Double, ByVal eCostType As CostType) As Boolean
        Dim bRetval As Boolean

        If (IsLoggingEnabled) Then
            Dim sb As New System.Text.StringBuilder

            sb.Append("...[")
            sb.Append(sText)
            sb.Append("-")
            sb.Append(eStep.ToString)
            sb.Append("][")
            sb.Append(eCostType.ToString())
            sb.Append("] Amount=")
            sb.Append(dblAmount.ToString)
            sb.Append(": ")
            sb.Append(dblValA.ToString)
            sb.Append("*")
            sb.Append(dblValB.ToString)
            sb.Append("*")
            sb.Append(dblValC.ToString)
            Log(sb.ToString)
            sb.Remove(0, sb.Length)

            bRetval = True
        End If

        Return (bRetval)
    End Function

    Private Function Log(ByVal sText As String, ByVal dblAmount As Double, ByVal dblValA As Double, ByVal dblValB As Double, ByVal dblValC As Double, ByVal eCostType As CostType) As Boolean
        Dim bRetval As Boolean

        If (IsLoggingEnabled) Then
            Dim sb As New System.Text.StringBuilder

            sb.Append("...[")
            sb.Append(sText)
            sb.Append("][")
            sb.Append(eCostType.ToString())
            sb.Append("] Amount=")
            sb.Append(dblAmount.ToString)
            sb.Append(": ")
            sb.Append(dblValA.ToString)
            sb.Append("*")
            sb.Append(dblValB.ToString)
            sb.Append("*")
            sb.Append(dblValC.ToString)
            Log(sb.ToString)
            sb.Remove(0, sb.Length)

            bRetval = True
        End If

        Return (bRetval)
    End Function

    Private Function Log(ByVal sText As String, ByVal sTextStructType As String, ByVal dblAmount As Double, ByVal dblValA As Double, ByVal dblValB As Double, ByVal dblValC As Double, ByVal eCostType As CostType) As Boolean
        Dim bRetval As Boolean

        If (IsLoggingEnabled) Then
            Dim sb As New System.Text.StringBuilder

            sb.Append("...[")
            sb.Append(sText)
            sb.Append("-")
            sb.Append(sTextStructType)
            sb.Append("][")
            sb.Append(eCostType.ToString())
            sb.Append("] Amount=")
            sb.Append(dblAmount.ToString)
            sb.Append(": ")
            sb.Append(dblValA.ToString)
            sb.Append("*")
            sb.Append(dblValB.ToString)
            sb.Append("*")
            sb.Append(dblValC.ToString)
            Log(sb.ToString)
            sb.Remove(0, sb.Length)

            bRetval = True
        End If

        Return (bRetval)
    End Function

    Private Function Log(ByVal sText As String, ByVal sTextStructType As String, ByVal dblAmount As Double, ByVal dblValAA As Double, ByVal dblValA As Double, ByVal dblValB As Double, ByVal dblValC As Double, ByVal eCostType As CostType) As Boolean
        Dim bRetval As Boolean

        If (IsLoggingEnabled) Then
            Dim sb As New System.Text.StringBuilder

            sb.Append("...[")
            sb.Append(sText)
            sb.Append("-")
            sb.Append(sTextStructType)
            sb.Append("][")
            sb.Append(eCostType.ToString())
            sb.Append("] Amount=")
            sb.Append(dblAmount.ToString)
            sb.Append(": ((")
            sb.Append(dblValB.ToString)
            sb.Append("-")
            sb.Append(dblValAA.ToString)
            sb.Append(")*")
            sb.Append(dblValA.ToString)
            sb.Append(")*")
            sb.Append(dblValC.ToString)
            Log(sb.ToString)
            sb.Remove(0, sb.Length)

            bRetval = True
        End If

        Return (bRetval)
    End Function

    Private Function Log(ByVal sText As String, ByVal dblAmount As Double, ByVal dblValA As Double, ByVal dblValB As Double, ByVal dblValC As Double, ByVal dblValD As Double, ByVal eCostType As CostType) As Boolean
        Dim bRetval As Boolean

        If (IsLoggingEnabled) Then
            Dim sb As New System.Text.StringBuilder

            sb.Append("...[")
            sb.Append(sText)
            sb.Append("][")
            sb.Append(eCostType.ToString())
            sb.Append("] Amount=")
            sb.Append(dblAmount.ToString)
            sb.Append(": ")
            sb.Append(dblValA.ToString)
            sb.Append("*")
            sb.Append(dblValB.ToString)
            sb.Append("*")
            sb.Append(dblValC.ToString)
            sb.Append("*")
            sb.Append(dblValD.ToString)
            Log(sb.ToString)
            sb.Remove(0, sb.Length)

            bRetval = True
        End If

        Return (bRetval)
    End Function

    Private Function Log(ByVal sText As String, ByVal nValA As Integer, ByVal nValB As Integer, ByVal dblAmount As Double, ByVal dblResult As Double) As Boolean
        Dim bRetval As Boolean

        If (IsLoggingEnabled) Then
            Dim sb As New System.Text.StringBuilder

            sb.Append("...[")
            sb.Append(sText)
            sb.Append("] ")
            sb.Append(" = (")
            sb.Append(nValA.ToString)
            sb.Append(" / ")
            sb.Append(nValB.ToString)
            sb.Append(") * ")
            sb.Append(dblAmount.ToString)
            sb.Append(" = ")
            sb.Append(dblResult.ToString)
            Log(sb.ToString)
            sb.Remove(0, sb.Length)

            bRetval = True
        End If

        Return (bRetval)
    End Function

    Private Function Log(ByVal sText As String, ByVal dblResult As Double) As Boolean
        Dim bRetval As Boolean

        If (IsLoggingEnabled) Then
            Dim sb As New System.Text.StringBuilder

            sb.Append("...[")
            sb.Append(sText)
            sb.Append("] ")
            sb.Append(" = ")
            sb.Append(dblResult.ToString)
            Log(sb.ToString)
            sb.Remove(0, sb.Length)

            bRetval = True
        End If

        Return (bRetval)
    End Function

#End Region

End Class















