Imports CE.RateEngine.Enums

Public Class UseCharge
	Inherits ChargeBase

	Private m_eBaseOrTier As BaseOrTier
	Private m_eTimeOfUse As TimeOfUse
	Private m_eSeason As Season
	Private m_ePartType As PartType
    Private m_bFuelCostRecovery As Boolean
    Private m_nFCRGroup As Integer
    Private m_bRealTimePricing As Boolean
    Private m_nRTPGroup As Integer

	Private Sub New()
	End Sub

	Public Sub New(ByVal eCostType As CostType, ByVal eBaseOrTier As BaseOrTier, ByVal eTimeOfUse As TimeOfUse, ByVal eSeason As Season, ByVal ePartType As PartType, ByVal dblChargeValue As Double)
		MyBase.New(eCostType, dblChargeValue)
		m_eBaseOrTier = eBaseOrTier
		m_eTimeOfUse = eTimeOfUse
		m_eSeason = eSeason
		m_ePartType = ePartType
        'm_bFuelCostRecovery = False
        'm_nFCRGroup = 0
	End Sub

    Public Sub New(ByVal eCostType As CostType, ByVal eBaseOrTier As BaseOrTier, ByVal eTimeOfUse As TimeOfUse, ByVal eSeason As Season, ByVal ePartType As PartType, ByVal bFuelCostRecovery As Boolean, ByVal nFCRGroup As Integer, ByVal bRealTimePricing As Boolean, ByVal nRTPGroup As Integer, ByVal dblChargeValue As Double)
        MyBase.New(eCostType, dblChargeValue)
        m_eBaseOrTier = eBaseOrTier
        m_eTimeOfUse = eTimeOfUse
        m_eSeason = eSeason
        m_ePartType = ePartType
        m_bFuelCostRecovery = bFuelCostRecovery
        m_nFCRGroup = nFCRGroup
        m_bRealTimePricing = bRealTimePricing
        m_nRTPGroup = nRTPGroup
    End Sub

#Region "Properties"

	Public Property FuelCostRecovery() As Boolean
		Get
			Return m_bFuelCostRecovery
		End Get
		Set(ByVal Value As Boolean)
			m_bFuelCostRecovery = Value
		End Set
	End Property

	Public Property FCRGroup() As Integer
		Get
			Return m_nFCRGroup
		End Get
		Set(ByVal Value As Integer)
			m_nFCRGroup = Value
		End Set
    End Property

    Public Property RealTimePricing() As Boolean
        Get
            Return m_bRealTimePricing
        End Get
        Set(ByVal Value As Boolean)
            m_bRealTimePricing = Value
        End Set
    End Property

    Public Property RTPGroup() As Integer
        Get
            Return m_nRTPGroup
        End Get
        Set(ByVal Value As Integer)
            m_nRTPGroup = Value
        End Set
    End Property

	Public ReadOnly Property BaseOrTier() As BaseOrTier
		Get
			Return (m_eBaseOrTier)
		End Get
	End Property

	Public ReadOnly Property TimeOfUse() As TimeOfUse
		Get
			Return (m_eTimeOfUse)
		End Get
	End Property

	Public ReadOnly Property Season() As Season
		Get
			Return (m_eSeason)
		End Get
	End Property

	Public ReadOnly Property PartType() As PartType
		Get
			Return (m_ePartType)
		End Get
	End Property

#End Region

End Class
