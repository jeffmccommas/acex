﻿Imports CE.RateEngine.Enums

<Serializable()> Public Class PTREventRebate

    Private m_actualUsage As Double
    Private m_actualUsageProvided As Boolean        ' true when provided by caller
    Private m_actualUsageCalculated As Boolean
    Private m_missingActualUsage As Boolean
    Private m_baselineUsage As Double
    Private m_baselineUsageEmpty As Boolean         ' true when indeed empty; provided by caller; baseline is never calculated by RE
    Private m_savingsUsage As Double                ' this gets pinned to zero when there is no savings
    Private m_realSavingsUsage As Double            ' this is not pinned to zero; can be negative and positive savings
    Private m_unitPrice As Double
    Private m_rebateAmount As Double
    Private m_rebateAmountProvided As Boolean       ' true when provided by caller
    Private m_rebateAmountCalculated As Boolean
    Private m_valid As Boolean
    Private m_accountingRule As RebateAccountingRule
    Private m_costType As CostType                  ' rebate is assigned to this costType; from accounting Rule setup
    Private m_baselineCalculated As Boolean

    Public Sub New()
        m_baselineUsageEmpty = False
        m_missingActualUsage = False
        m_actualUsageCalculated = False
        m_rebateAmountCalculated = False
        m_baselineCalculated = False
        m_valid = False
        m_accountingRule = RebateAccountingRule.EachEvent
    End Sub

#Region "Properties"

    Public Property ActualUsageProvided() As Boolean
        Get
            Return m_actualUsageProvided
        End Get
        Set(ByVal Value As Boolean)
            m_actualUsageProvided = Value
        End Set
    End Property

    Public Property RebateAmountProvided() As Boolean
        Get
            Return m_rebateAmountProvided
        End Get
        Set(ByVal Value As Boolean)
            m_rebateAmountProvided = Value
        End Set
    End Property

    Public Property ActualUsage() As Double
        Get
            Return m_actualUsage
        End Get
        Set(ByVal Value As Double)
            m_actualUsage = Value
        End Set
    End Property

    Public Property BaselineUsage() As Double
        Get
            Return m_baselineUsage
        End Get
        Set(ByVal Value As Double)
            m_baselineUsage = Value
        End Set
    End Property

    Public Property SavingsUsage() As Double
        Get
            Return m_savingsUsage
        End Get
        Set(ByVal Value As Double)
            m_savingsUsage = Value
        End Set
    End Property

    Public Property UnitPrice() As Double
        Get
            Return m_unitPrice
        End Get
        Set(ByVal Value As Double)
            m_unitPrice = Value
        End Set
    End Property

    Public Property RebateAmount() As Double
        Get
            Return m_rebateAmount
        End Get
        Set(ByVal Value As Double)
            m_rebateAmount = Value
        End Set
    End Property

    Public Property ActualUsageCalculated() As Boolean
        Get
            Return m_actualUsageCalculated
        End Get
        Set(ByVal Value As Boolean)
            m_actualUsageCalculated = Value
        End Set
    End Property

    Public Property BaselineUsageEmpty() As Boolean
        Get
            Return m_baselineUsageEmpty
        End Get
        Set(ByVal Value As Boolean)
            m_baselineUsageEmpty = Value
        End Set
    End Property

    Public Property MissingActualUsage() As Boolean
        Get
            Return m_missingActualUsage
        End Get
        Set(ByVal Value As Boolean)
            m_missingActualUsage = Value
        End Set
    End Property

    Public Property RebateAmountCalculated() As Boolean
        Get
            Return m_rebateAmountCalculated
        End Get
        Set(ByVal Value As Boolean)
            m_rebateAmountCalculated = Value
        End Set
    End Property

    Public Property AccountingRule() As RebateAccountingRule
        Get
            Return m_accountingRule
        End Get
        Set(ByVal Value As RebateAccountingRule)
            m_accountingRule = Value
        End Set
    End Property

    Public Property Valid() As Boolean
        Get
            Return m_valid
        End Get
        Set(ByVal Value As Boolean)
            m_valid = Value
        End Set
    End Property

    Public Property CostType() As CostType
        Get
            Return m_costType
        End Get
        Set(ByVal Value As CostType)
            m_costType = Value
        End Set
    End Property

    Public Property BaselineCalculated() As Boolean
        Get
            Return m_baselineCalculated
        End Get
        Set(ByVal value As Boolean)
            m_baselineCalculated = value
        End Set
    End Property

    Public Property RealSavingsUsage() As Double
        Get
            Return m_realSavingsUsage
        End Get
        Set(ByVal Value As Double)
            m_realSavingsUsage = Value
        End Set
    End Property

#End Region

End Class
