Imports CE.RateEngine.Enums

Public Class Difference

	Private m_eDifferenceType As DifferenceType
	Private m_sName As String
	Private m_eCategory As DifferenceTypeCategory
	Private m_sCategoryName As String

	Private Sub New()
	End Sub

	Public Sub New(ByVal eDifferenceType As DifferenceType, ByVal sName As String, ByVal eCat As DifferenceTypeCategory, ByVal sCatName As String)
		m_eDifferenceType = eDifferenceType
		m_sName = sName
		m_eCategory = eCat
		m_sCategoryName = sCatName
	End Sub

#Region "Properties"

	Public ReadOnly Property DifferenceType() As DifferenceType
		Get
			Return (m_eDifferenceType)
		End Get
	End Property

	Public ReadOnly Property Name() As String
		Get
			Return (m_sName)
		End Get
	End Property

	Public ReadOnly Property Category() As DifferenceTypeCategory
		Get
			Return (m_eCategory)
		End Get
	End Property

	Public ReadOnly Property CategoryName() As String
		Get
			Return (m_sCategoryName)
		End Get
	End Property

#End Region

End Class
