<Serializable()> Public Class Price
    Private m_dblPrice As Double
    Private m_dtDateStamp As DateTime
    Private m_ePriceStream As Enums.RTPActualStream
    Private m_bEmpty As Boolean

    Public Sub New(ByVal bEmpty As Boolean, ByVal dtDateStamp As DateTime)
        m_bEmpty = bEmpty
        m_dtDateStamp = dtDateStamp
    End Sub

    Public Sub New()
    End Sub

    Public Sub New(ByVal dtDateStamp As DateTime, ByVal dblPrice As Double)
        m_dtDateStamp = dtDateStamp
        m_dblPrice = dblPrice
    End Sub
    Public Sub New(ByVal bEmpty As Boolean, ByVal dtDateStamp As DateTime, ByVal dblPrice As Double)
        m_bEmpty = bEmpty
        m_dtDateStamp = dtDateStamp
        m_dblPrice = dblPrice
    End Sub

#Region "Properties"
    Public Property DateStamp() As DateTime
        Get
            Return m_dtDateStamp
        End Get
        Set(ByVal value As DateTime)
            m_dtDateStamp = value
        End Set
    End Property

    Public Property Price() As Double
        Get
            Return m_dblPrice
        End Get
        Set(ByVal value As Double)
            m_dblPrice = value
        End Set
    End Property

    Public Property PriceStream() As Enums.RTPActualStream
        Get
            Return m_ePriceStream
        End Get
        Set(ByVal value As Enums.RTPActualStream)
            m_ePriceStream = value
        End Set
    End Property

    Public ReadOnly Property IsEmpty() As Boolean
        Get
            Return m_bEmpty
        End Get
    End Property
#End Region
End Class


