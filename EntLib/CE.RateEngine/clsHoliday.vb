﻿Imports CE.RateEngine.Enums

Public Class Holiday

    Private m_id As Integer
    Private m_groupID As Integer
    Private m_name As String
    Private m_date As DateTime

    Private Sub New()
    End Sub

    Public Sub New(ByVal id As Integer, ByVal groupID As Integer, ByVal name As String, ByVal dateOfHoliday As DateTime)
        m_id = id
        m_groupID = groupID
        m_name = name
        m_date = dateOfHoliday
    End Sub

    Public Sub New(ByVal name As String, ByVal dateOfHoliday As DateTime)
        m_id = -1
        m_groupID = -1
        m_name = name
        m_date = dateOfHoliday
    End Sub

#Region "Properties"

    Public ReadOnly Property Name() As String
        Get
            Return (m_name)
        End Get
    End Property

    Public ReadOnly Property DateOfHoliday() As DateTime
        Get
            Return (m_date)
        End Get
    End Property

    Public ReadOnly Property ID() As Integer
        Get
            Return (m_id)
        End Get
    End Property

    Public ReadOnly Property GroupID() As Integer
        Get
            Return (m_groupID)
        End Get
    End Property

#End Region

End Class
