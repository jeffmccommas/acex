Public Class MasterServiceTerritoryCollection
	Inherits CollectionBase

	Public Sub New()
	End Sub

	Default ReadOnly Property Item(ByVal index As Integer) As MasterServiceTerritory
		Get
			Return CType(Me.InnerList.Item(index), MasterServiceTerritory)
		End Get
	End Property

	Public Function Add(ByVal value As MasterServiceTerritory) As Integer
		Return Me.InnerList.Add(value)
	End Function

	Public Sub Insert(ByVal index As Integer, ByVal value As MasterServiceTerritory)
		Me.InnerList.Insert(index, value)
	End Sub

	Public Sub AddRange(ByVal values() As MasterServiceTerritory)
		Me.InnerList.AddRange(values)
	End Sub

	Public Sub Remove(ByVal value As MasterServiceTerritory)
		Me.InnerList.Remove(value)
	End Sub

End Class
