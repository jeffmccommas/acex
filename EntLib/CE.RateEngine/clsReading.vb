Imports CE.RateEngine.Enums

<Serializable()> Public Class Reading

    Private m_dtDate As DateTime
    Private m_dblQuantity As Double
    Private m_eBaseOrTier As BaseOrTier
    Private m_eTimeOfUse As TimeOfUse
    Private m_eSeason As Season
    Private m_eDayType As DayType
    Private m_bHoliday As Boolean
    Private m_bEmpty As Boolean
    Private m_totalPrice As Double      ' only used when ReadingDetail is used
    Private m_totalCost As Double       ' only used when ReadingDetail is used
    'Private m_detail As ReadingDetail   ' only used when price binning was performed
    Private m_detailCollection As ReadingDetailCollection   ' only used when price binning was performed
    Private m_touModified As Boolean    ' true when original tou bin was modified; usually because of Critical Peak pricing events
    Private m_originalTOU As TimeOfUse  ' if tou bin was modified, this indicates the original time-of-use
    Private m_bIsStartOfPeriod As Boolean ' indicate the reading is the beginning of a billing cycle
    Private m_binnedId As Integer         ' optionally used by binning to indicate the source definition used when data was binned
    Private m_binnedgroupNum As Integer   ' goes with above
    Private m_bIsGap As Boolean
    Private m_specialInterval As SpecialIntervalType
    Private m_bAdder As Boolean
    Private m_bCapacityReserve As Boolean
    Private m_dblCapacityReserveThreshold As Double
    Private m_dblCapacityReserveQuantity As Double
    Private m_bOnEventDay As Boolean
    Private m_ReadParts As Integer

    Private Sub New()
        m_bHoliday = False
        m_totalPrice = 0.0
        m_totalCost = 0.0
        ' m_detail = Nothing
        m_touModified = False
        m_bIsStartOfPeriod = False
        m_bIsGap = False
        m_bAdder = False
        m_bCapacityReserve = False
        m_dblCapacityReserveThreshold = 0.0
        m_dblCapacityReserveQuantity = 0.0
        m_bOnEventDay = False
    End Sub

    Public Sub New(ByVal eBaseOrTier As BaseOrTier, ByVal eTimeOfUse As TimeOfUse, ByVal eSeason As Season, ByVal dtDate As DateTime, ByVal dblQuantity As Double, ByVal eDayType As DayType, ByVal bEmpty As Boolean)
        Me.New()
        m_bIsGap = False
        m_bAdder = False
        m_bCapacityReserve = False
        m_dblCapacityReserveThreshold = 0.0
        m_dblCapacityReserveQuantity = 0.0
        m_specialInterval = SpecialIntervalType.IntervalRegular
        m_bIsStartOfPeriod = False
        m_eBaseOrTier = eBaseOrTier
        m_eTimeOfUse = eTimeOfUse
        m_eSeason = eSeason
        m_dtDate = dtDate
        m_dblQuantity = dblQuantity
        m_eDayType = eDayType
        m_bEmpty = bEmpty
        m_bOnEventDay = False
    End Sub

    Public Sub New(ByVal eBaseOrTier As BaseOrTier, ByVal eTimeOfUse As TimeOfUse, ByVal eSeason As Season, ByVal dtDate As DateTime, ByVal dblQuantity As Double, ByVal eDayType As DayType, ByVal bEmpty As Boolean, bIsStartOfPeriod As Boolean)
        Me.New()
        m_bIsGap = False
        m_bAdder = False
        m_bCapacityReserve = False
        m_dblCapacityReserveThreshold = 0.0
        m_dblCapacityReserveQuantity = 0.0
        m_specialInterval = SpecialIntervalType.IntervalRegular
        m_bIsStartOfPeriod = bIsStartOfPeriod
        m_eBaseOrTier = eBaseOrTier
        m_eTimeOfUse = eTimeOfUse
        m_eSeason = eSeason
        m_dtDate = dtDate
        m_dblQuantity = dblQuantity
        m_eDayType = eDayType
        m_bEmpty = bEmpty
        m_bOnEventDay = False
    End Sub

    Public Sub New(ByVal eBaseOrTier As BaseOrTier, ByVal eTimeOfUse As TimeOfUse, ByVal eSeason As Season, ByVal dtDate As DateTime, ByVal dblQuantity As Double, ByVal eDayType As DayType, ByVal bEmpty As Boolean, bIsStartOfPeriod As Boolean, bIsGap As Boolean, specialInterval As SpecialIntervalType)
        Me.New()
        m_bIsGap = bIsGap
        m_bAdder = False
        m_bCapacityReserve = False
        m_dblCapacityReserveThreshold = 0.0
        m_dblCapacityReserveQuantity = 0.0
        m_specialInterval = specialInterval
        m_bIsStartOfPeriod = bIsStartOfPeriod
        m_eBaseOrTier = eBaseOrTier
        m_eTimeOfUse = eTimeOfUse
        m_eSeason = eSeason
        m_dtDate = dtDate
        m_dblQuantity = dblQuantity
        m_eDayType = eDayType
        m_bEmpty = bEmpty
        m_bOnEventDay = False
    End Sub


    Public Sub New(ByVal eBaseOrTier As BaseOrTier, ByVal eTimeOfUse As TimeOfUse, ByVal eSeason As Season, ByVal dtDate As DateTime, ByVal dblQuantity As Double, ByVal eDayType As DayType, ByVal bEmpty As Boolean, bIsStartOfPeriod As Boolean, bIsGap As Boolean, specialInterval As SpecialIntervalType, ByVal readParts As Integer)
        Me.New()
        m_bIsGap = bIsGap
        m_bAdder = False
        m_bCapacityReserve = False
        m_dblCapacityReserveThreshold = 0.0
        m_dblCapacityReserveQuantity = 0.0
        m_specialInterval = specialInterval
        m_bIsStartOfPeriod = bIsStartOfPeriod
        m_eBaseOrTier = eBaseOrTier
        m_eTimeOfUse = eTimeOfUse
        m_eSeason = eSeason
        m_dtDate = dtDate
        m_dblQuantity = dblQuantity
        m_eDayType = eDayType
        m_bEmpty = bEmpty
        m_bOnEventDay = False
        m_ReadParts = readParts
    End Sub
#Region "Properties"


    Public Property ReadParts() As Integer
        Get
            Return (m_ReadParts)
        End Get
        Set(ByVal Value As Integer)
            m_ReadParts = Value
        End Set
    End Property

    Public Property BaseOrTier() As BaseOrTier
        Get
            Return (m_eBaseOrTier)
        End Get
        Set(ByVal Value As BaseOrTier)
            m_eBaseOrTier = Value
        End Set
    End Property

    Public Property TimeOfUse() As TimeOfUse
        Get
            Return (m_eTimeOfUse)
        End Get
        Set(ByVal Value As TimeOfUse)
            m_eTimeOfUse = Value
        End Set
    End Property

    Public Property Season() As Season
        Get
            Return (m_eSeason)
        End Get
        Set(ByVal Value As Season)
            m_eSeason = Value
        End Set
    End Property

    Public Property Datestamp() As DateTime
        Get
            Return m_dtDate
        End Get
        Set(ByVal Value As DateTime)
            m_dtDate = Value
        End Set
    End Property

    Public Property DayType() As DayType
        Get
            Return (m_eDayType)
        End Get
        Set(ByVal Value As DayType)
            m_eDayType = Value
        End Set
    End Property

    Public Property Quantity() As Double
        Get
            Return m_dblQuantity
        End Get
        Set(ByVal Value As Double)
            m_dblQuantity = Value
        End Set
    End Property

    Public ReadOnly Property IsEmpty() As Boolean
        Get
            Return (m_bEmpty)
        End Get
    End Property

    Public Property TotalPrice() As Double
        Get
            Return m_totalPrice
        End Get
        Set(ByVal Value As Double)
            m_totalPrice = Value
        End Set
    End Property

    Public Property TotalCost() As Double
        Get
            Return m_totalCost
        End Get
        Set(ByVal Value As Double)
            m_totalCost = Value
        End Set
    End Property

    'Public Property Detail() As ReadingDetail
    '    Get
    '        Return m_detail
    '    End Get
    '    Set(ByVal Value As ReadingDetail)
    '        m_detail = Value
    '    End Set
    'End Property

    Public Property DetailCollection() As ReadingDetailCollection
        Get
            Return m_detailCollection
        End Get
        Set(ByVal Value As ReadingDetailCollection)
            m_detailCollection = Value
        End Set
    End Property

    Public Property Holiday() As Boolean
        Get
            Return m_bHoliday
        End Get
        Set(ByVal Value As Boolean)
            m_bHoliday = Value
        End Set
    End Property

    Public Property TimeOfUseModified() As Boolean
        Get
            Return m_touModified
        End Get
        Set(ByVal Value As Boolean)
            m_touModified = Value
        End Set
    End Property

    Public Property TimeOfUseOriginal() As TimeOfUse
        Get
            Return (m_originalTOU)
        End Get
        Set(ByVal Value As TimeOfUse)
            m_originalTOU = Value
        End Set
    End Property

    Public Property IsStartOfPeriod() As Boolean
        Get
            Return m_bIsStartOfPeriod
        End Get
        Set(ByVal Value As Boolean)
            m_bIsStartOfPeriod = Value
        End Set
    End Property

    Public Property BinnedId() As Integer
        Get
            Return m_binnedId
        End Get
        Set(ByVal Value As Integer)
            m_binnedId = Value
        End Set
    End Property

    Public Property BinnedGroupNumber() As Integer
        Get
            Return m_binnedgroupNum
        End Get
        Set(ByVal Value As Integer)
            m_binnedgroupNum = Value
        End Set
    End Property

    Public Property IsGap As Boolean
        Get
            Return m_bIsGap
        End Get
        Set(value As Boolean)
            m_bIsGap = value
        End Set
    End Property

    Public Property SpecialInterval As SpecialIntervalType
        Get
            Return m_specialInterval
        End Get
        Set(value As SpecialIntervalType)
            m_specialInterval = value
        End Set
    End Property
#End Region

    'Public Function DoesReadingDetailExist() As Boolean
    '    Dim exists As Boolean = False

    '    If (Not m_detail Is Nothing) Then
    '        exists = True
    '    End If

    '    Return (exists)
    'End Function


    Public Property IsAdder() As Boolean
        Get
            Return m_bAdder
        End Get
        Set(ByVal Value As Boolean)
            m_bAdder = Value
        End Set
    End Property

    Public Property IsCapacityReserve() As Boolean
        Get
            Return m_bCapacityReserve
        End Get
        Set(ByVal Value As Boolean)
            m_bCapacityReserve = Value
        End Set
    End Property

    Public Property CapacityReserveQuantity() As Double
        Get
            Return m_dblCapacityReserveQuantity
        End Get
        Set(ByVal Value As Double)
            m_dblCapacityReserveQuantity = Value
        End Set
    End Property

    Public Property CapacityReserveThreshold() As Double
        Get
            Return m_dblCapacityReserveThreshold
        End Get
        Set(ByVal Value As Double)
            m_dblCapacityReserveThreshold = Value
        End Set
    End Property

    Public Property OnEventDay() As Boolean
        Get
            Return m_bOnEventDay
        End Get
        Set(ByVal Value As Boolean)
            m_bOnEventDay = Value
        End Set
    End Property

End Class


''' <summary>
''' Helper class to provide a datestamp and potential other details for a maximum reading.
''' </summary>
''' <remarks></remarks>
Public Class MaxReadingDetail
    Private m_dblQuantity As Double
    Private m_dtDatestamp As DateTime

    Public Sub New()
        m_dblQuantity = 0
        m_dtDatestamp = DateTime.MinValue
    End Sub

    Public Sub New(ByVal quantity As Double, ByVal dtDatestamp As DateTime)
        m_dblQuantity = quantity
        m_dtDatestamp = dtDatestamp
    End Sub

    Public ReadOnly Property IsValid() As Boolean
        Get
            If (m_dtDatestamp <> DateTime.MinValue) Then
                Return (True)
            Else
                Return (False)
            End If
        End Get
    End Property

    Public Property Quantity() As Double
        Get
            Return (m_dblQuantity)
        End Get
        Set(ByVal Value As Double)
            m_dblQuantity = Value
        End Set
    End Property

    Public Property Datestamp() As DateTime
        Get
            Return (m_dtDatestamp)
        End Get
        Set(ByVal Value As DateTime)
            m_dtDatestamp = Value
        End Set
    End Property

End Class
