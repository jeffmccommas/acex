Imports CE.RateEngine.Enums

Public Class CalculateCostExtParam

    Private m_nMeterCount As Integer
    Private m_nDwellingCount As Integer
    Private m_bAutoSelectFuelCostRecovery As Boolean
    Private m_bProrateMonthlyServiceCharges As Boolean
    Private m_bProrateDemandCharges As Boolean
    Private m_monthlyServiceChargeProrateFactor As Double
    Private m_eMinimumChargeType As MinimumChargeType
    Private m_useProjectedEndDateWhenFinalizingTierBoundaries As Boolean
    Private m_tierBoundaryProjectedEndDate As DateTime
    Private m_rebateExists As Boolean = False
    Private m_rebateAmount As Double = 0.0
    Private m_rebateApplied As Boolean = False
    Private Const knDefaultMeterCount As Integer = 1
    Private Const knDefaultDwellingUnits As Integer = 1
    Private Const kdDefaultMonthlyServiceChargeProrateFactor As Double = 1.0
    Private Const keDefaultMinimumChargeType As MinimumChargeType = Enums.MinimumChargeType.NoProrateMinimumCharge
    Private Const keUseProjectedEndDateWhenFinalizingTierBoundaries As Boolean = False      ' this is a special setting very important to CostToDate/BillToDate;  those functions will set this to true when applicable

    Public Sub New()
        AssignParams(knDefaultMeterCount, knDefaultDwellingUnits, kdDefaultMonthlyServiceChargeProrateFactor, False, False, keDefaultMinimumChargeType, keUseProjectedEndDateWhenFinalizingTierBoundaries, False)
    End Sub

    Public Sub New(ByVal nMeters As Integer, ByVal nDwellingUnits As Integer, ByVal bAutoSelectFuelCostRecovery As Boolean)
        AssignParams(nMeters, nDwellingUnits, kdDefaultMonthlyServiceChargeProrateFactor, bAutoSelectFuelCostRecovery, False, keDefaultMinimumChargeType, keUseProjectedEndDateWhenFinalizingTierBoundaries, False)
    End Sub

    Public Sub New(ByVal nMeters As Integer)
        AssignParams(nMeters, knDefaultDwellingUnits, kdDefaultMonthlyServiceChargeProrateFactor, False, False, keDefaultMinimumChargeType, keUseProjectedEndDateWhenFinalizingTierBoundaries, False)
    End Sub

    Public Sub New(ByVal nMeters As Integer, ByVal nDwellingUnits As Integer)
        AssignParams(nMeters, nDwellingUnits, kdDefaultMonthlyServiceChargeProrateFactor, False, False, keDefaultMinimumChargeType, keUseProjectedEndDateWhenFinalizingTierBoundaries, False)
    End Sub

    Public Sub New(ByVal doProrateMonthlyServiceCharges As Boolean, doProrateDemandCharges As Boolean, ByVal monthlyServiceChargeProrateFactor As Double)
        AssignParams(knDefaultMeterCount, knDefaultDwellingUnits, monthlyServiceChargeProrateFactor, False, doProrateMonthlyServiceCharges, keDefaultMinimumChargeType, keUseProjectedEndDateWhenFinalizingTierBoundaries, doProrateDemandCharges)
    End Sub

    Public Sub New(ByVal doProrateMonthlyServiceCharges As Boolean, doProrateDemandCharges As Boolean)
        AssignParams(knDefaultMeterCount, knDefaultDwellingUnits, kdDefaultMonthlyServiceChargeProrateFactor, False, doProrateMonthlyServiceCharges, keDefaultMinimumChargeType, keUseProjectedEndDateWhenFinalizingTierBoundaries, doProrateDemandCharges)
    End Sub

    Public Sub New(ByVal doProrateMonthlyServiceCharges As Boolean, doProrateDemandCharges As Boolean, ByVal monthlyServiceChargeProrateFactor As Double, ByVal minimumChargeType As MinimumChargeType)
        AssignParams(knDefaultMeterCount, knDefaultDwellingUnits, monthlyServiceChargeProrateFactor, False, doProrateMonthlyServiceCharges, minimumChargeType, keUseProjectedEndDateWhenFinalizingTierBoundaries, doProrateDemandCharges)
    End Sub

    Public Sub New(ByVal doProrateMonthlyServiceCharges As Boolean, doProrateDemandCharges As Boolean, ByVal minimumChargeType As MinimumChargeType)
        AssignParams(knDefaultMeterCount, knDefaultDwellingUnits, kdDefaultMonthlyServiceChargeProrateFactor, False, doProrateMonthlyServiceCharges, minimumChargeType, keUseProjectedEndDateWhenFinalizingTierBoundaries, doProrateDemandCharges)
    End Sub

    Public Sub New(ByVal doProrateMonthlyServiceCharges As Boolean, doProrateDemandCharges As Boolean, ByVal monthlyServiceChargeProrateFactor As Double, ByVal minChargeType As MinimumChargeType, ByVal useProjectedEndDateWhenFinalizingTierBoundaries As Boolean, ByVal projectedEndDate As DateTime)
        AssignParams(knDefaultMeterCount, knDefaultDwellingUnits, monthlyServiceChargeProrateFactor, False, doProrateMonthlyServiceCharges, minChargeType, useProjectedEndDateWhenFinalizingTierBoundaries, doProrateDemandCharges)
        m_tierBoundaryProjectedEndDate = projectedEndDate
    End Sub

#Region "Properties"

    Public ReadOnly Property MeterCount() As Integer
        Get
            Return (m_nMeterCount)
        End Get
    End Property

    Public ReadOnly Property DwellingCount() As Integer
        Get
            Return (m_nDwellingCount)
        End Get
    End Property

    Public ReadOnly Property ProrateMonthlyServiceCharges() As Boolean
        Get
            Return (m_bProrateMonthlyServiceCharges)
        End Get
    End Property

    ''' <summary>
    ''' prorate demand charges for cost calc
    ''' CR 56546 April 2015
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public ReadOnly Property ProrateDemandCharges() As Boolean
        Get
            Return (m_bProrateDemandCharges)
        End Get
    End Property

    Public ReadOnly Property MonthlyServiceChargeProrateFactor() As Double
        Get
            Return (m_monthlyServiceChargeProrateFactor)
        End Get
    End Property

    Public ReadOnly Property MinimumChargeType() As MinimumChargeType
        Get
            Return (m_eMinimumChargeType)
        End Get
    End Property

    Public ReadOnly Property AutoSelectFuelCostRecovery() As Boolean
        Get
            Return (m_bAutoSelectFuelCostRecovery)
        End Get
    End Property

    Public ReadOnly Property UseProjectedEndDateWhenFinalizingTierBoundaries() As Boolean
        Get
            Return (m_useProjectedEndDateWhenFinalizingTierBoundaries)
        End Get
    End Property

    Public ReadOnly Property ProjectedEndDate() As DateTime
        Get
            Return (m_tierBoundaryProjectedEndDate)
        End Get
    End Property

    Public Property RebateAmount() As Double
        Get
            Return (m_rebateAmount)
        End Get
        Set(ByVal value As Double)
            m_rebateAmount = value
        End Set
    End Property

    Public Property RebateApplied() As Boolean
        Get
            Return (m_rebateApplied)
        End Get
        Set(ByVal value As Boolean)
            m_rebateApplied = value
        End Set
    End Property

    Public Property RebateExists() As Boolean
        Get
            Return (m_rebateExists)
        End Get
        Set(ByVal value As Boolean)
            m_rebateExists = value
        End Set
    End Property

#End Region

#Region "Private Functions"

    ' Called by constructors
    Private Function AssignParams(ByVal nMeters As Integer, ByVal nDwellingUnits As Integer, ByVal monthlyServiceChargeProrateFactor As Double, ByVal bAutoSelectFuelCostRecovery As Boolean, ByVal bProrateMonthlyServiceCharges As Boolean, ByVal eMinimumChargeType As MinimumChargeType, ByVal useProjectedEndDateWhenFinalizingTierBoundaries As Boolean, bProrateDemandCharges As Boolean) As Boolean
        Dim bRetval As Boolean = True

        m_nMeterCount = nMeters
        m_nDwellingCount = nDwellingUnits
        m_monthlyServiceChargeProrateFactor = monthlyServiceChargeProrateFactor
        m_bAutoSelectFuelCostRecovery = bAutoSelectFuelCostRecovery
        m_bProrateMonthlyServiceCharges = bProrateMonthlyServiceCharges
        m_eMinimumChargeType = eMinimumChargeType
        m_useProjectedEndDateWhenFinalizingTierBoundaries = useProjectedEndDateWhenFinalizingTierBoundaries
        m_bProrateDemandCharges = bProrateDemandCharges

        Return (bRetval)
    End Function

#End Region

End Class
