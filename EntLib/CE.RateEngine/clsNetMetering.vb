﻿Imports CE.RateEngine.Enums
Imports System.Linq

<Serializable()> Public Class NetMeteringMonthlyResult

    Public Property NetMeteringType As NetMeteringType
    Public Property RegularCost As Double
    Public Property MinimumCost As Double
    Public Property MinimumCosts As CostCollection
    Public Property CarryoverCharge As Double

    Public Sub New()
    End Sub

    ''' <summary>
    ''' Return the appropriate total cost when in the context of Net Metering.
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function GetTotalCost() As Double
        Dim total As Double

        If (NetMeteringType = Enums.NetMeteringType.Nem3 Or NetMeteringType = Enums.NetMeteringType.Nem4 Or NetMeteringType = Enums.NetMeteringType.Nem5) Then
            total = Math.Max(MinimumCost, RegularCost)
        Else
            total = RegularCost
        End If

        Return (total)
    End Function

End Class


<Serializable()> Public Class NetMeteringMonthlyResultCollection
    Inherits CollectionBase

    Public Sub New()
    End Sub

    Default ReadOnly Property Item(ByVal index As Integer) As NetMeteringMonthlyResult
        Get
            Return CType(Me.InnerList.Item(index), NetMeteringMonthlyResult)
        End Get
    End Property

    Public Function Add(ByVal value As NetMeteringMonthlyResult) As Integer
        Return Me.InnerList.Add(value)
    End Function

    Public Sub Insert(ByVal index As Integer, ByVal value As NetMeteringMonthlyResult)
        Me.InnerList.Insert(index, value)
    End Sub

    Public Sub AddRange(ByVal values() As NetMeteringMonthlyResult)
        Me.InnerList.AddRange(values)
    End Sub

    Public Sub Remove(ByVal value As NetMeteringMonthlyResult)
        Me.InnerList.Remove(value)
    End Sub

End Class



<Serializable()> Public Class NetMeteringTotalResult

    Public Property TotalRegularCost As Double
    Public Property TotalMinimumCost As Double
    Public Property TotalCarryoverCharge As Double
    Public Property TotalCost As Double
    Public Property MonthlyResults As NetMeteringMonthlyResultCollection

    Public Sub New()
    End Sub

End Class


<Serializable()> Public Class NetMeteringSettings

    Public Property NEMType As NetMeteringType
    Public Property D1 As Double
    Public Property D2 As Double
    Public Property CostTypeCSV As String

    Public Sub New()
        NEMType = NetMeteringType.Unspecified
        D1 = 1.0
        D2 = 0.0
        CostTypeCSV = String.Empty
    End Sub

End Class

''' <summary>
''' Manager of classes for Net Metering. 
''' </summary>
''' <remarks></remarks>
Public Class NetMeteringManager

    Public Property RateCompanyID As Integer
    Public Property NMRValue As NetMeteringType
    Public Property Settings As RateCompanySettings

    Public Sub New(ByVal rateCompanyIdentifier As Integer, ByVal nmr As NetMeteringType, ByVal rateCompSettings As RateCompanySettings)
        NMRValue = nmr
        RateCompanyID = rateCompanyIdentifier
        Settings = rateCompSettings
    End Sub

    'Bug54232
    Public Function CreateNetMeteringMonthlyResult(ByVal totalCost As Double, ByVal costResult As CostResult) As NetMeteringMonthlyResult
        Dim result As NetMeteringMonthlyResult = New NetMeteringMonthlyResult()
        Dim nemSettings As NetMeteringSettings
        Dim localCosts As CostCollection = New CostCollection()
        Dim matchCosts As CostCollection

        nemSettings = Settings.GetNEMSettings(NMRValue)

        result.NetMeteringType = NMRValue

        ' passed in regular cost (aka. totalCost or BTDcost)
        result.RegularCost = totalCost

        ' minimum cost is the sum of the applicable NEM specific cost types
        matchCosts = CSVToCostCollection(nemSettings.CostTypeCSV)

        '14.10 bug 54810 - use the regular cost collection if there's no min charges configured; otherwise use the collection with min charges included for MMC
        Dim costCollection As CE.RateEngine.CostCollection
        If (costResult.MinimumChargesConfigured) Then
            costCollection = costResult.CostCollectionWithMinCost
        Else
            costCollection = costResult.CostCollection
        End If

        For Each objDE As DictionaryEntry In costCollection
            Dim c As Cost = CType(objDE.Value, Cost)

            If (matchCosts.Contains(c.CostType)) Then
                localCosts.SumAdd(c.CostType, New Cost(c.CostType, c.Amount))
            End If
        Next

        result.MinimumCost = localCosts.GetTotalCost()
        result.MinimumCosts = localCosts

        'carryover
        result.CarryoverCharge = result.RegularCost - result.MinimumCost

        If (result.CarryoverCharge < 0.0) Then
            result.CarryoverCharge = result.CarryoverCharge * nemSettings.D1
        End If

        Return (result)
    End Function


    Public Function CreateNetMeteringTotalResult(ByVal monthlyResults As NetMeteringMonthlyResultCollection) As NetMeteringTotalResult
        Dim result As NetMeteringTotalResult = New NetMeteringTotalResult()
        Dim nemSettings As NetMeteringSettings

        nemSettings = Settings.GetNEMSettings(NMRValue)

        ' total regular, total minimum, total carryover, and total cost
        For Each monthlyResult As NetMeteringMonthlyResult In monthlyResults
            result.TotalRegularCost = result.TotalRegularCost + monthlyResult.RegularCost
            result.TotalMinimumCost = result.TotalMinimumCost + monthlyResult.MinimumCost
            result.TotalCarryoverCharge = result.TotalCarryoverCharge + monthlyResult.CarryoverCharge
            result.TotalCost = result.TotalCost + monthlyResult.MinimumCost
        Next

        ' factor in the D2 constant
        If (result.TotalCarryoverCharge < 0) Then
            result.TotalCarryoverCharge = result.TotalCarryoverCharge * nemSettings.D2
        End If

        ' total cost final
        result.TotalCost = result.TotalCost + result.TotalCarryoverCharge

        Return (result)
    End Function


    Public Function CreateNetMeteringTotalResult(ByVal sumMonthlyResult As NetMeteringMonthlyResult) As NetMeteringTotalResult
        Dim result As NetMeteringTotalResult = New NetMeteringTotalResult()
        Dim nemSettings As NetMeteringSettings

        nemSettings = Settings.GetNEMSettings(NMRValue)

        ' the mrc, mmc, and mcc are all totaled already by the caller
        result.TotalRegularCost = sumMonthlyResult.RegularCost
        result.TotalMinimumCost = sumMonthlyResult.MinimumCost
        result.TotalCarryoverCharge = sumMonthlyResult.CarryoverCharge

        ' factor in the D2 constant
        If (result.TotalCarryoverCharge < 0) Then
            result.TotalCarryoverCharge = result.TotalCarryoverCharge * nemSettings.D2
        End If

        ' total cost final
        result.TotalCost = sumMonthlyResult.MinimumCost + result.TotalCarryoverCharge

        Return (result)
    End Function

    ''' <summary>
    ''' Helper for above.
    ''' </summary>
    ''' <param name="csv"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function CSVToCostCollection(ByVal csv As String) As CostCollection
        Dim costTypeList As CostCollection = New CostCollection()

        If (Not String.IsNullOrEmpty(csv)) Then
            Dim items As String() = csv.Split(New Char() {","c})
            Dim res As CostType

            For Each item As String In items
                If ([Enum].TryParse(item, True, res)) Then
                    costTypeList.SumAdd(res, New Cost(res, 0))
                End If
            Next

        End If

        Return (costTypeList)
    End Function
End Class

