﻿Imports CE.RateEngine.Enums
Imports System.Collections.Generic

Partial Public Class RateEngine
    Private Const Exception_Message_RateHelperCopyError As String = "Rate Helper Copy Error"
    '--- CR50560 BEGIN -------------------------------------------------------------------------------------------
    Private Const ksException_Message_RateHelper_CoincidenceChangeRequires_cpcf_Or_cptime As String = "Rate Helper: Either cpcf or cptime must be specified for coincidence charge. (cpcf: {0}, cptime: {1})."
    Private Const ksException_Message_RateHelper_cptime_MissingReadingOrBadValue As String = "Rate Helper: cptime - Missing reading or bad value (cptime: {0})."
    Private Const ksException_Message_RateHelper_cptime_DateTimeOutsideOfBillPeriod As String = "Rate Helper: cptime - Date/time outside of bill period. (cptime: {0:yyy-MM-dd HH:mm:ss}, Start: {1:yyy-MM-dd}, End: {2:yyy-MM-dd})."
    '--- CR50560 END ---------------------------------------------------------------------------------------------

    Private Const ksNEM1 As String = "nem1"
    Private Const ksNEM2 As String = "nem2"
    Private Const ksNEM3 As String = "nem3"
    Private Const ksNEM4 As String = "nem4"
    Private Const ksNEM5 As String = "nem5"

    Private Const ksnValuePair As String = "~"
    Private Const ksnDivider As String = "|"
    Private Const ksnRateClassHash As String = "#"

#Region "Rate Helpers - Public"

    ''' <summary>
    ''' Copy all rate helper attributes except NMR and rate class, NMR will be nem2
    ''' </summary>
    ''' <param name="ComparisonRateClass"></param>
    ''' <param name="MainRateClass"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function CopyRateHelper(ByVal ComparisonRateClass As String, ByVal MainRateClass As String) As String
        Dim rateHelperValue As String = String.Empty

        rateHelperValue = RateClassModifierCollection.GetModifierValueFromRateClassModifierString(MainRateClass, RateClassModifierCollection.ksnBC)
        If (Not rateHelperValue Is Nothing AndAlso Not rateHelperValue = String.Empty AndAlso rateHelperValue.Length > 0) Then
            ComparisonRateClass = AddMissingModifier(ComparisonRateClass, RateClassModifierCollection.ksnBC, rateHelperValue)
        End If
        rateHelperValue = String.Empty

        rateHelperValue = RateClassModifierCollection.GetModifierValueFromRateClassModifierString(MainRateClass, RateClassModifierCollection.ksnIRS)
        If (Not rateHelperValue Is Nothing AndAlso Not rateHelperValue = String.Empty AndAlso rateHelperValue.Length > 0) Then
            ComparisonRateClass = AddMissingModifier(ComparisonRateClass, RateClassModifierCollection.ksnIRS, rateHelperValue)
        End If
        rateHelperValue = String.Empty

        rateHelperValue = RateClassModifierCollection.GetModifierValueFromRateClassModifierString(MainRateClass, RateClassModifierCollection.ksnIDRE)
        If (Not rateHelperValue Is Nothing AndAlso Not rateHelperValue = String.Empty AndAlso rateHelperValue.Length > 0) Then
            ComparisonRateClass = AddMissingModifier(ComparisonRateClass, RateClassModifierCollection.ksnIDRE, rateHelperValue)
        End If
        rateHelperValue = String.Empty

        rateHelperValue = RateClassModifierCollection.GetModifierValueFromRateClassModifierString(MainRateClass, RateClassModifierCollection.ksnRenew)
        If (Not rateHelperValue Is Nothing AndAlso Not rateHelperValue = String.Empty AndAlso rateHelperValue.Length > 0) Then
            ComparisonRateClass = AddMissingModifier(ComparisonRateClass, RateClassModifierCollection.ksnRenew, rateHelperValue)
        End If
        rateHelperValue = String.Empty

        rateHelperValue = RateClassModifierCollection.GetModifierValueFromRateClassModifierString(MainRateClass, RateClassModifierCollection.ksnDRRC)
        If (Not rateHelperValue Is Nothing AndAlso Not rateHelperValue = String.Empty AndAlso rateHelperValue.Length > 0) Then
            ComparisonRateClass = AddMissingModifier(ComparisonRateClass, RateClassModifierCollection.ksnDRRC, rateHelperValue)
        End If
        rateHelperValue = String.Empty

        rateHelperValue = RateClassModifierCollection.GetModifierValueFromRateClassModifierString(MainRateClass, RateClassModifierCollection.ksnDRT)
        If (Not rateHelperValue Is Nothing AndAlso Not rateHelperValue = String.Empty AndAlso rateHelperValue.Length > 0) Then
            ComparisonRateClass = AddMissingModifier(ComparisonRateClass, RateClassModifierCollection.ksnDRT, rateHelperValue)
        End If
        rateHelperValue = String.Empty

        rateHelperValue = RateClassModifierCollection.GetModifierValueFromRateClassModifierString(MainRateClass, RateClassModifierCollection.ksnTCF)
        If (Not rateHelperValue Is Nothing AndAlso Not rateHelperValue = String.Empty AndAlso rateHelperValue.Length > 0) Then
            ComparisonRateClass = AddMissingModifier(ComparisonRateClass, RateClassModifierCollection.ksnTCF, rateHelperValue)
        End If
        rateHelperValue = String.Empty

        rateHelperValue = RateClassModifierCollection.GetModifierValueFromRateClassModifierString(MainRateClass, RateClassModifierCollection.ksnDATF)
        If (Not rateHelperValue Is Nothing AndAlso Not rateHelperValue = String.Empty AndAlso rateHelperValue.Length > 0) Then
            ComparisonRateClass = AddMissingModifier(ComparisonRateClass, RateClassModifierCollection.ksnDATF, rateHelperValue)
        End If
        rateHelperValue = String.Empty

        rateHelperValue = RateClassModifierCollection.GetModifierValueFromRateClassModifierString(MainRateClass, RateClassModifierCollection.ksnTUF)
        If (Not rateHelperValue Is Nothing AndAlso Not rateHelperValue = String.Empty AndAlso rateHelperValue.Length > 0) Then
            ComparisonRateClass = AddMissingModifier(ComparisonRateClass, RateClassModifierCollection.ksnTUF, rateHelperValue)
        End If
        rateHelperValue = String.Empty

        rateHelperValue = RateClassModifierCollection.GetModifierValueFromRateClassModifierString(MainRateClass, RateClassModifierCollection.ksnDBTF)
        If (Not rateHelperValue Is Nothing AndAlso Not rateHelperValue = String.Empty AndAlso rateHelperValue.Length > 0) Then
            ComparisonRateClass = AddMissingModifier(ComparisonRateClass, RateClassModifierCollection.ksnDBTF, rateHelperValue)
        End If
        rateHelperValue = String.Empty

        rateHelperValue = RateClassModifierCollection.GetModifierValueFromRateClassModifierString(MainRateClass, RateClassModifierCollection.ksnADBD)
        If (Not rateHelperValue Is Nothing AndAlso Not rateHelperValue = String.Empty AndAlso rateHelperValue.Length > 0) Then
            ComparisonRateClass = AddMissingModifier(ComparisonRateClass, RateClassModifierCollection.ksnADBD, rateHelperValue)
        End If
        rateHelperValue = String.Empty

        rateHelperValue = RateClassModifierCollection.GetModifierValueFromRateClassModifierString(MainRateClass, RateClassModifierCollection.ksnAVGU)
        If (Not rateHelperValue Is Nothing AndAlso Not rateHelperValue = String.Empty AndAlso rateHelperValue.Length > 0) Then
            ComparisonRateClass = AddMissingModifier(ComparisonRateClass, RateClassModifierCollection.ksnAVGU, rateHelperValue)
        End If
        rateHelperValue = String.Empty

        rateHelperValue = RateClassModifierCollection.GetModifierValueFromRateClassModifierString(MainRateClass, RateClassModifierCollection.ksnACFU)
        If (Not rateHelperValue Is Nothing AndAlso Not rateHelperValue = String.Empty AndAlso rateHelperValue.Length > 0) Then
            ComparisonRateClass = AddMissingModifier(ComparisonRateClass, RateClassModifierCollection.ksnACFU, rateHelperValue)
        End If
        rateHelperValue = String.Empty

        rateHelperValue = RateClassModifierCollection.GetModifierValueFromRateClassModifierString(MainRateClass, RateClassModifierCollection.ksnDAYU)
        If (Not rateHelperValue Is Nothing AndAlso Not rateHelperValue = String.Empty AndAlso rateHelperValue.Length > 0) Then
            ComparisonRateClass = AddMissingModifier(ComparisonRateClass, RateClassModifierCollection.ksnDAYU, rateHelperValue)
        End If
        rateHelperValue = String.Empty

        'rdp
        rateHelperValue = RateClassModifierCollection.GetModifierValueFromRateClassModifierString(MainRateClass, RateClassModifierCollection.ksnRDP)
        If (Not rateHelperValue Is Nothing AndAlso Not rateHelperValue = String.Empty AndAlso rateHelperValue.Length > 0) Then
            ComparisonRateClass = AddMissingModifier(ComparisonRateClass, RateClassModifierCollection.ksnRDP, rateHelperValue)
        End If
        rateHelperValue = String.Empty

        'crkw
        rateHelperValue = RateClassModifierCollection.GetModifierValueFromRateClassModifierString(MainRateClass, RateClassModifierCollection.ksnCRKW)
        If (Not rateHelperValue Is Nothing AndAlso Not rateHelperValue = String.Empty AndAlso rateHelperValue.Length > 0) Then
            ComparisonRateClass = AddMissingModifier(ComparisonRateClass, RateClassModifierCollection.ksnCRKW, rateHelperValue)
        End If
        rateHelperValue = String.Empty

        'crkwc
        rateHelperValue = RateClassModifierCollection.GetModifierValueFromRateClassModifierString(MainRateClass, RateClassModifierCollection.ksnCRKWC)
        If (Not rateHelperValue Is Nothing AndAlso Not rateHelperValue = String.Empty AndAlso rateHelperValue.Length > 0) Then
            ComparisonRateClass = AddMissingModifier(ComparisonRateClass, RateClassModifierCollection.ksnCRKWC, rateHelperValue)
        End If
        rateHelperValue = String.Empty

        'crkwhc
        rateHelperValue = RateClassModifierCollection.GetModifierValueFromRateClassModifierString(MainRateClass, RateClassModifierCollection.ksnCRKWHC)
        If (Not rateHelperValue Is Nothing AndAlso Not rateHelperValue = String.Empty AndAlso rateHelperValue.Length > 0) Then
            ComparisonRateClass = AddMissingModifier(ComparisonRateClass, RateClassModifierCollection.ksnCRKWHC, rateHelperValue)
        End If
        rateHelperValue = String.Empty

        'ncdm
        rateHelperValue = RateClassModifierCollection.GetModifierValueFromRateClassModifierString(MainRateClass, RateClassModifierCollection.ksnNCDM)
        If (Not rateHelperValue Is Nothing AndAlso Not rateHelperValue = String.Empty AndAlso rateHelperValue.Length > 0) Then
            ComparisonRateClass = AddMissingModifier(ComparisonRateClass, RateClassModifierCollection.ksnNCDM, rateHelperValue)
        End If
        rateHelperValue = String.Empty

        'nckwc
        rateHelperValue = RateClassModifierCollection.GetModifierValueFromRateClassModifierString(MainRateClass, RateClassModifierCollection.ksnNCKWC)
        If (Not rateHelperValue Is Nothing AndAlso Not rateHelperValue = String.Empty AndAlso rateHelperValue.Length > 0) Then
            ComparisonRateClass = AddMissingModifier(ComparisonRateClass, RateClassModifierCollection.ksnNCKWC, rateHelperValue)
        End If
        rateHelperValue = String.Empty

        'ncdf
        rateHelperValue = RateClassModifierCollection.GetModifierValueFromRateClassModifierString(MainRateClass, RateClassModifierCollection.ksnNCDF)
        If (Not rateHelperValue Is Nothing AndAlso Not rateHelperValue = String.Empty AndAlso rateHelperValue.Length > 0) Then
            ComparisonRateClass = AddMissingModifier(ComparisonRateClass, RateClassModifierCollection.ksnNCDF, rateHelperValue)
        End If
        rateHelperValue = String.Empty

        'nmr
        ComparisonRateClass = AddMissingModifier(ComparisonRateClass, RateClassModifierCollection.ksnNMR, ksNEM2)

        Return ComparisonRateClass
    End Function

    ''' <summary>
    ''' Copy all the rate helpers from main rate class except NEM, it will be nem2 for comparison
    ''' CR 23401 Jan 2012 
    ''' </summary>
    ''' <param name="mainRateClass"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function CopyRateHelperfromMain(ByVal mainRateClass As String) As String
        Dim updatedComparisonRateClass As String = String.Empty
        Dim mainRateHelper As Boolean

        Try
            mainRateHelper = RateClassModifierCollection.DoesRateClassContainModifiers(mainRateClass)

            ' create an updated comparison with main rate class' helper attributes PLUS nmr = nem2
            ' if main has rate helper
            If (mainRateHelper) Then
                updatedComparisonRateClass = AddMissingModifier(updatedComparisonRateClass, RateClassModifierCollection.ksnRC, RateClassModifierCollection.GetModifierValueFromRateClassModifierString(mainRateClass, RateClassModifierCollection.ksnRC))
                updatedComparisonRateClass = CopyRateHelper(updatedComparisonRateClass, mainRateClass)
                updatedComparisonRateClass = AddMissingModifier(updatedComparisonRateClass, RateClassModifierCollection.ksnNMR, ksNEM2)
                ' create an updated comparison with helper attibutes rate class PLUS nmr = nem2
                ' if main does't have rate helper
            Else
                updatedComparisonRateClass = AddMissingModifier(updatedComparisonRateClass, RateClassModifierCollection.ksnRC, mainRateClass)
                updatedComparisonRateClass = AddMissingModifier(updatedComparisonRateClass, RateClassModifierCollection.ksnNMR, ksNEM2)
            End If
            updatedComparisonRateClass = RateClassModifierCollection.ksnRateClassHash + updatedComparisonRateClass

        Catch ex As Exception
            Throw New Exception(Exception_Message_RateHelperCopyError, ex.InnerException)
        End Try

        Return updatedComparisonRateClass
    End Function

    ''' <summary>
    ''' CR 20298, 20299, 20302 Oct 2011 create rate class modifier collection
    ''' </summary>
    ''' <param name="sRateClass"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function CreateRateClassModifierCollection(ByVal sRateClass As String, ByVal nRateCompanyID As Integer) As RateClassModifierCollection
        Dim objRateClassModifierCol As New RateClassModifierCollection
        Dim objRateClassModifier As RateClassModifier = Nothing
        Dim aryAttributes() As String
        Dim aryPair() As String
        Dim i As Integer
        Dim sError As String = String.Empty

        If Not sRateClass Is Nothing AndAlso sRateClass.Length > 0 Then
            If (sRateClass.StartsWith(ksnRateClassHash)) Then
                sError = RateClassModifierCollection.ValidateRateClassModifierCollection(sRateClass)
                If sError <> String.Empty Then
                    Throw New Exception(sError)
                End If
                sRateClass = sRateClass.Remove(0, 1)

                'add all the missing modifiers
                sRateClass = AddAllMissingModifiers(sRateClass)
            Else
                'create rate class modifier string for regular rate class with no "#"
                sRateClass = CreateRateClassModifiersStringForNonHashRateClass(nRateCompanyID, sRateClass)
            End If
        Else
            sRateClass = AddAllMissingModifiers(sRateClass)
        End If

        aryAttributes = sRateClass.Split(CChar(ksnDivider))

        If aryAttributes.Length > 0 Then
            For i = 0 To aryAttributes.GetUpperBound(0)

                aryPair = aryAttributes(i).Split(CChar(ksnValuePair))
                If aryPair.Length > 1 Then
                    If aryPair.GetUpperBound(0) = 1 Then
                        Select Case aryPair(0)
                            Case RateClassModifierCollection.ksnRC
                                objRateClassModifier = New RateClassModifier(RateClassModifierType.RateClass, aryPair(1).Trim())
                            Case RateClassModifierCollection.ksnBC
                                objRateClassModifier = New RateClassModifier(RateClassModifierType.BaselineCode, aryPair(1).Trim())
                            Case RateClassModifierCollection.ksnTCF
                                objRateClassModifier = New RateClassModifier(RateClassModifierType.TaxCostFactor, aryPair(1).Trim())
                            Case RateClassModifierCollection.ksnTUF
                                objRateClassModifier = New RateClassModifier(RateClassModifierType.TaxUsageFactor, aryPair(1).Trim())
                            Case RateClassModifierCollection.ksnDBTF
                                objRateClassModifier = New RateClassModifier(RateClassModifierType.DiscountBeforeTaxFactor, aryPair(1).Trim())
                            Case RateClassModifierCollection.ksnDATF
                                objRateClassModifier = New RateClassModifier(RateClassModifierType.DiscountAfterTaxFactor, aryPair(1).Trim())
                            Case RateClassModifierCollection.ksnNMR
                                objRateClassModifier = New RateClassModifier(RateClassModifierType.NetMeteredRule, aryPair(1).Trim())
                            Case RateClassModifierCollection.ksnIRS
                                objRateClassModifier = New RateClassModifier(RateClassModifierType.IsRetailSupply, aryPair(1).Trim())
                            Case RateClassModifierCollection.ksnIDRE
                                objRateClassModifier = New RateClassModifier(RateClassModifierType.IsDemandResponseEligible, aryPair(1).Trim())
                            Case RateClassModifierCollection.ksnRenew
                                objRateClassModifier = New RateClassModifier(RateClassModifierType.RenewableFactor, aryPair(1).Trim())
                            Case RateClassModifierCollection.ksnDRRC
                                objRateClassModifier = New RateClassModifier(RateClassModifierType.DemandResponseRebateClass, aryPair(1).Trim())
                            Case RateClassModifierCollection.ksnDRT
                                objRateClassModifier = New RateClassModifier(RateClassModifierType.DemandResponseThreshold, aryPair(1).Trim())
                            Case RateClassModifierCollection.ksnBAA
                                objRateClassModifier = New RateClassModifier(RateClassModifierType.BTDAdjustmentAmount, aryPair(1).Trim())
                            Case RateClassModifierCollection.ksnSSWAP
                                objRateClassModifier = New RateClassModifier(RateClassModifierType.SupplyCostTypeSwap, aryPair(1).Trim())
                            Case RateClassModifierCollection.ksnEXCLCT
                                objRateClassModifier = New RateClassModifier(RateClassModifierType.ExcludeCostTypes, aryPair(1).Trim())
                            Case RateClassModifierCollection.ksnDEXCLCT
                                objRateClassModifier = New RateClassModifier(RateClassModifierType.DiscountExcludeCostTypes, aryPair(1).Trim())
                            Case RateClassModifierCollection.ksnTYPE
                                objRateClassModifier = New RateClassModifier(RateClassModifierType.TypeOfCollection, aryPair(1).Trim())
                            Case RateClassModifierCollection.ksnRPID
                                objRateClassModifier = New RateClassModifier(RateClassModifierType.RateProgramId, aryPair(1).Trim())
                            Case RateClassModifierCollection.ksnICRP
                                objRateClassModifier = New RateClassModifier(RateClassModifierType.InitialComparisonRateProg, aryPair(1).Trim())
                            Case RateClassModifierCollection.ksnADBD
                                objRateClassModifier = New RateClassModifier(RateClassModifierType.AnnualDemandBillDemand, aryPair(1).Trim())
                            Case RateClassModifierCollection.ksnADBDF
                                objRateClassModifier = New RateClassModifier(RateClassModifierType.ADBDFactor, aryPair(1).Trim())
                            Case RateClassModifierCollection.ksnADBDR
                                objRateClassModifier = New RateClassModifier(RateClassModifierType.ADBDRule, aryPair(1).Trim())
                            Case RateClassModifierCollection.ksnAVGU
                                objRateClassModifier = New RateClassModifier(RateClassModifierType.AverageUsage, aryPair(1).Trim())
                            Case RateClassModifierCollection.ksnACFU
                                objRateClassModifier = New RateClassModifier(RateClassModifierType.ActualConsumptionFactor, aryPair(1).Trim())
                            Case RateClassModifierCollection.ksnDAYU
                                objRateClassModifier = New RateClassModifier(RateClassModifierType.DayUsage, aryPair(1).Trim())
                            Case RateClassModifierCollection.ksnRDP
                                objRateClassModifier = New RateClassModifier(RateClassModifierType.RoundingDecimalPlaces, aryPair(1).Trim())
                            Case RateClassModifierCollection.ksnCRKW
                                objRateClassModifier = New RateClassModifier(RateClassModifierType.CapResDemandValue, aryPair(1).Trim())
                            Case RateClassModifierCollection.ksnCRKWC
                                objRateClassModifier = New RateClassModifier(RateClassModifierType.CapReskWCharge, aryPair(1).Trim())
                            Case RateClassModifierCollection.ksnCRKWHC
                                objRateClassModifier = New RateClassModifier(RateClassModifierType.CapResExcesskWhCharge, aryPair(1).Trim())
                            Case RateClassModifierCollection.ksnNCDM
                                objRateClassModifier = New RateClassModifier(RateClassModifierType.NonCoDemandAnnMax, aryPair(1).Trim())
                            Case RateClassModifierCollection.ksnNCKWC
                                objRateClassModifier = New RateClassModifier(RateClassModifierType.NonCokWCharge, aryPair(1).Trim())
                            Case RateClassModifierCollection.ksnNCDF
                                objRateClassModifier = New RateClassModifier(RateClassModifierType.NonCoMultFactor, aryPair(1).Trim())
                                '--- CR50560 BEGIN -------------------------------------------------------------------------------------------
                            Case RateClassModifierCollection.ksnCPKWC
                                objRateClassModifier = New RateClassModifier(RateClassModifierType.CokWCharge, aryPair(1).Trim())
                            Case RateClassModifierCollection.ksnCPCF
                                objRateClassModifier = New RateClassModifier(RateClassModifierType.CoMultFactor, aryPair(1).Trim())
                            Case RateClassModifierCollection.ksnCPTIME
                                objRateClassModifier = New RateClassModifier(RateClassModifierType.CoDateTime, aryPair(1).Trim())
                                '--- CR50560 END ---------------------------------------------------------------------------------------------
                        End Select
                        objRateClassModifierCol.Add(objRateClassModifier)
                    End If
                End If
            Next
        End If


        Return (objRateClassModifierCol)
    End Function

    ''' <summary>
    ''' Add individual missing modifier - CR 20298, 20299, 20302 Nov 2011
    ''' </summary>
    ''' <param name="sRateClass"></param>
    ''' <param name="sModifier"></param>
    ''' <param name="sValue"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function AddMissingModifier(ByVal sRateClass As String, ByVal sModifier As String, ByVal sValue As String) As String
        'Add missing modifier
        If sRateClass.Trim = String.Empty Then
            sRateClass = sModifier + ksnValuePair + sValue
        Else
            If Not sRateClass.Contains(sModifier + ksnValuePair) Then
                sRateClass = sRateClass + ksnDivider + sModifier + ksnValuePair + sValue
            End If
        End If

        Return sRateClass
    End Function

    ''' <summary>
    ''' Is demand-response eligible.  Non-hashed will always be eligible; hashed needs verification. 
    ''' </summary>
    ''' <param name="rateCompanyId"></param>
    ''' <param name="rateClass"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function CheckIfDemandResponseEligible(ByVal rateCompanyId As Integer, ByVal rateClass As String) As Boolean
        Dim isDemandResponseEligible As Boolean = False
        Dim idreValue As String = String.Empty

        ' if not even a hashed rate class, then by default this is eligible
        If (Not rateClass Is Nothing AndAlso rateClass.Length > 0 AndAlso (Not rateClass.StartsWith(ksnRateClassHash))) Then
            isDemandResponseEligible = True
        Else
            idreValue = RateClassModifierCollection.GetModifierValueFromRateClassModifierString(rateClass, RateClassModifierCollection.ksnIDRE)

            Dim success As Boolean = False
            success = System.Boolean.TryParse(idreValue, isDemandResponseEligible)
            If (Not success) Then
                isDemandResponseEligible = False
            End If

        End If

        Return (isDemandResponseEligible)
    End Function

    Public Function RenewableFactor(ByVal rateClass As String) As Single
        Dim factor As Single
        Dim renewValue As String = String.Empty

        If (Not rateClass Is Nothing AndAlso rateClass.Length > 0 AndAlso (Not rateClass.StartsWith(ksnRateClassHash))) Then
            factor = 0
        Else
            renewValue = RateClassModifierCollection.GetModifierValueFromRateClassModifierString(rateClass, RateClassModifierCollection.ksnRenew)

            Dim success As Boolean = False
            success = System.Single.TryParse(renewValue, factor)
            If (Not success) Then
                factor = 0
            End If

        End If

        Return (factor)
    End Function

    ''' <summary>
    ''' Check if an attribute exists at all.
    ''' </summary>
    ''' <param name="rateClass"></param>
    ''' <param name="attribute"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function DoesRateClassAttributeExist(ByVal rateClass As String, ByVal attribute As String) As Boolean
        Dim exists As Boolean = False
        Dim value As String = String.Empty

        If (Not rateClass Is Nothing AndAlso rateClass.Length > 0 AndAlso (rateClass.StartsWith(ksnRateClassHash))) Then
            value = RateClassModifierCollection.GetModifierValueFromRateClassModifierString(rateClass, attribute)
            If (Not String.IsNullOrEmpty(value)) Then
                exists = True
            End If
        End If

        Return (exists)
    End Function

    ''' <summary>
    '''  Get the indication of metering from the rate class if attributes exist.
    ''' </summary>
    ''' <param name="rateClass"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function RateClassAttributeIndicatesNetMetering(ByVal rateClass As String) As Boolean
        Dim isNetMetering As Boolean = False

        If (DoesRateClassAttributeExist(rateClass, RateClassModifierCollection.ksnNMR)) Then

            Dim nmrValue As String = GetNMRValue(rateClass)

            If (nmrValue = Nothing) Then
                isNetMetering = False
            Else
                Select Case nmrValue.ToLower
                    Case ksNEM2
                        isNetMetering = True
                    Case ksNEM3
                        isNetMetering = True
                    Case ksNEM4
                        isNetMetering = True
                    Case ksNEM5
                        isNetMetering = True
                    Case Else
                        isNetMetering = False
                End Select
            End If

        End If

        Return (isNetMetering)
    End Function

    ''' <summary>
    ''' Get the net metering type from the rate class if attributes exist.
    ''' </summary>
    ''' <param name="rateClass"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function RateClassAttributeIndicatesNetMeteringType(ByVal rateClass As String) As NetMeteringType
        Dim nmType As NetMeteringType = NetMeteringType.Unspecified
        Dim isNetMetering As Boolean = False

        If (DoesRateClassAttributeExist(rateClass, RateClassModifierCollection.ksnNMR)) Then

            Dim nmrValue As String = GetNMRValue(rateClass)

            If (nmrValue = Nothing) Then
                isNetMetering = False
                nmType = NetMeteringType.Unspecified
            Else
                Select Case nmrValue.ToLower
                    Case ksNEM2
                        isNetMetering = True
                        nmType = NetMeteringType.LikeEPR6
                    Case ksNEM3
                        isNetMetering = True
                        nmType = NetMeteringType.Nem3
                    Case ksNEM4
                        isNetMetering = True
                        nmType = NetMeteringType.Nem4
                    Case ksNEM5
                        isNetMetering = True
                        nmType = NetMeteringType.Nem5
                    Case Else
                        isNetMetering = False
                        nmType = NetMeteringType.Unspecified
                End Select
            End If

        End If

        Return (nmType)
    End Function

    ''' <summary>
    ''' Get Rebate charges if they exist from a rate definition.
    ''' </summary>
    ''' <param name="def"></param>
    ''' <remarks></remarks>
    Public Function GetRebateCharges(ByVal def As DetailedRateDefinition) As ServiceChargeCollection
        Dim charges As ServiceChargeCollection = Nothing

        If (Not def Is Nothing) Then
            charges = def.ServiceCharges.GetRebateCharges()
        End If

        Return (charges)
    End Function

    ''' <summary>
    ''' Try and get a Capacity Reserve Demand Value from a rate helper.  Special public function for some callers.
    ''' </summary>
    ''' <param name="rateClass"></param>
    ''' <param name="crkw"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function TryParseCapacityReserveDemandValue(ByVal rateClass As String, ByRef crkw As Double) As Boolean
        Dim parsedOkay As Boolean = False
        Dim d As Double?

        d = GetCapacityReserveDemandValue(rateClass)

        If (Not d Is Nothing) Then
            crkw = d.Value
            parsedOkay = True
        End If

        Return (parsedOkay)
    End Function

#End Region


#Region "Setup Rate Helpers - Private"

    ''' <summary>
    ''' Create rate class modifiers string for regular rate class with no "#" - CR 20298, 20299, 20302 Nov 2011
    ''' </summary>
    ''' <param name="nRateCompanyID"></param>
    ''' <param name="sRateClass"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function CreateRateClassModifiersStringForNonHashRateClass(ByVal nRateCompanyID As Integer, ByVal sRateClass As String) As String
        ' create rate class modifiers for non hash rate class
        If nRateCompanyID = 0 Then
            sRateClass = AddMissingModifier("", RateClassModifierCollection.ksnRC, sRateClass)
        Else
            Dim sBaseRateClass As String = Nothing
            Dim sCode As String = Nothing
            Dim sTerritory As String = Nothing
            Dim bRetVal As Boolean
            bRetVal = ParseRateClass(nRateCompanyID, sRateClass, sBaseRateClass, sCode, sTerritory)
            If bRetVal Then
                sRateClass = AddMissingModifier("", RateClassModifierCollection.ksnRC, sBaseRateClass)

                If Not sCode = String.Empty AndAlso Not sTerritory = String.Empty Then
                    sRateClass = AddMissingModifier(sRateClass, RateClassModifierCollection.ksnBC, sCode & sTerritory)
                End If
            Else
                If Not sRateClass = String.Empty Then
                    sRateClass = AddMissingModifier("", RateClassModifierCollection.ksnRC, sRateClass)
                End If
            End If
        End If

        'set IsDemandResponseEligible to true, so the logic won't bypass the PTR calculation for rate class with no rate class attributes
        sRateClass = AddMissingModifier(sRateClass, RateClassModifierCollection.ksnIDRE, "true")

        ' initialize renewable factor = 0
        sRateClass = AddMissingModifier(sRateClass, RateClassModifierCollection.ksnRenew, "0")

        'add all missing modifiers
        sRateClass = AddAllMissingModifiers(sRateClass)

        Return sRateClass
    End Function

    ''' <summary>
    ''' add all the missing modifiers into rate class string - CR 20298, 20299, 20302 Nov 2011
    ''' </summary>
    ''' <param name="sRateClass"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function AddAllMissingModifiers(ByVal sRateClass As String) As String
        'Rate Class
        sRateClass = AddMissingModifier(sRateClass, RateClassModifierCollection.ksnRC, String.Empty)
        'Tax Cost Factor
        sRateClass = AddMissingModifier(sRateClass, RateClassModifierCollection.ksnTCF, "0")
        'Tax Usage Factor
        sRateClass = AddMissingModifier(sRateClass, RateClassModifierCollection.ksnTUF, "0")
        'DiscountBeforeTaxFactor
        sRateClass = AddMissingModifier(sRateClass, RateClassModifierCollection.ksnDBTF, "0")
        'DiscountAfterTaxFactor
        sRateClass = AddMissingModifier(sRateClass, RateClassModifierCollection.ksnDATF, "0")
        'NetMeteredRule
        sRateClass = AddMissingModifier(sRateClass, RateClassModifierCollection.ksnNMR, "false")    'false is not a typical value; typically nem2, nem3, ne4, nem5
        'IsRetailSupply
        sRateClass = AddMissingModifier(sRateClass, RateClassModifierCollection.ksnIRS, "false")
        'IsDemandResponseEligible
        sRateClass = AddMissingModifier(sRateClass, RateClassModifierCollection.ksnIDRE, "false")
        'DemandResponseRebateClass
        sRateClass = AddMissingModifier(sRateClass, RateClassModifierCollection.ksnDRRC, "Undefined")
        'BaselineCode
        sRateClass = AddMissingModifier(sRateClass, RateClassModifierCollection.ksnBC, String.Empty)
        'DemandResponseThreshold
        sRateClass = AddMissingModifier(sRateClass, RateClassModifierCollection.ksnDRT, String.Empty)
        'RenewableFactor
        sRateClass = AddMissingModifier(sRateClass, RateClassModifierCollection.ksnRenew, "0")

        Return sRateClass
    End Function

#End Region


#Region "Apply Rate Helpers - Private"

    ''' <summary>
    ''' Remove charges with cost type supp_ - CR 20298, 20299, 20302 Oct 2011 
    ''' During computation of totalCost, if true then ignore the charges in the rateEngine that are set up with supp_cost type
    ''' 0 or 1 key-value pair
    ''' </summary>
    ''' <param name="objMaster"></param>
    ''' <remarks></remarks>
    Private Function RemoveSuppCostType(ByVal objMaster As CE.RateEngine.CostResult) As CE.RateEngine.CostResult
        Dim bReCalc As Boolean = False

        If objMaster.CostCollection.Contains(CostType.SUPP_CUST) Then
            bReCalc = True
            objMaster.CostCollection.Remove(CostType.SUPP_CUST)
            Log("[Remove Cost type SUPP_CUST]")
        End If
        If objMaster.CostCollection.Contains(CostType.SUPP_DMND) Then
            bReCalc = True
            objMaster.CostCollection.Remove(CostType.SUPP_DMND)
            Log("[Remove Cost type SUPP_DMND]")
        End If
        If objMaster.CostCollection.Contains(CostType.SUPP_DMND_ALLOTHER) Then
            bReCalc = True
            objMaster.CostCollection.Remove(CostType.SUPP_DMND_ALLOTHER)
            Log("[Remove Cost type SUPP_DMND_ALLOTHER]")
        End If
        If objMaster.CostCollection.Contains(CostType.SUPP_DMND_OFFPEAK) Then
            bReCalc = True
            objMaster.CostCollection.Remove(CostType.SUPP_DMND_OFFPEAK)
            Log("[Remove Cost type SUPP_DMND_OFFPEAK]")
        End If
        If objMaster.CostCollection.Contains(CostType.SUPP_DMND_ONPEAK) Then
            bReCalc = True
            objMaster.CostCollection.Remove(CostType.SUPP_DMND_ONPEAK)
            Log("[Remove Cost type SUPP_DMND_ONPEAK]")
        End If
        If objMaster.CostCollection.Contains(CostType.SUPP_DMND_SHOULDER1) Then
            bReCalc = True
            objMaster.CostCollection.Remove(CostType.SUPP_DMND_SHOULDER1)
            Log("[Remove Cost type SUPP_DMND_SHOULDER1]")
        End If
        If objMaster.CostCollection.Contains(CostType.SUPP_DMND_SHOULDER2) Then
            bReCalc = True
            objMaster.CostCollection.Remove(CostType.SUPP_DMND_SHOULDER2)
            Log("[Remove Cost type SUPP_DMND_SHOULDER2]")
        End If
        If objMaster.CostCollection.Contains(CostType.SUPP_ENGY) Then
            bReCalc = True
            objMaster.CostCollection.Remove(CostType.SUPP_ENGY)
            Log("[Remove Cost type SUPP_ENGY]")
        End If
        If objMaster.CostCollection.Contains(CostType.SUPP_ENGY_ALLOTHER) Then
            bReCalc = True
            objMaster.CostCollection.Remove(CostType.SUPP_ENGY_ALLOTHER)
            Log("[Remove Cost type SUPP_ENGY_ALLOTHER]")
        End If

        If objMaster.CostCollection.Contains(CostType.SUPP_ENGY_GREEN1) Then
            bReCalc = True
            objMaster.CostCollection.Remove(CostType.SUPP_ENGY_GREEN1)
            Log("[Remove Cost type SUPP_ENGY_GREEN1]")
        End If
        If objMaster.CostCollection.Contains(CostType.SUPP_ENGY_GREEN2) Then
            bReCalc = True
            objMaster.CostCollection.Remove(CostType.SUPP_ENGY_GREEN2)
            Log("[Remove Cost type SUPP_ENGY_GREEN2]")
        End If
        If objMaster.CostCollection.Contains(CostType.SUPP_ENGY_GREEN3) Then
            bReCalc = True
            objMaster.CostCollection.Remove(CostType.SUPP_ENGY_GREEN3)
            Log("[Remove Cost type SUPP_ENGY_GREEN3]")
        End If

        If objMaster.CostCollection.Contains(CostType.SUPP_ENGY_OFFPEAK) Then
            bReCalc = True
            objMaster.CostCollection.Remove(CostType.SUPP_ENGY_OFFPEAK)
            Log("[Remove Cost type SUPP_ENGY_OFFPEAK]")
        End If
        If objMaster.CostCollection.Contains(CostType.SUPP_ENGY_ONPEAK) Then
            bReCalc = True
            objMaster.CostCollection.Remove(CostType.SUPP_ENGY_ONPEAK)
            Log("[Remove Cost type SUPP_ENGY_ONPEAK]")
        End If
        If objMaster.CostCollection.Contains(CostType.SUPP_ENGY_SHOULDER1) Then
            bReCalc = True
            objMaster.CostCollection.Remove(CostType.SUPP_ENGY_SHOULDER1)
            Log("[Remove Cost type SUPP_ENGY_SHOULDER1]")
        End If
        If objMaster.CostCollection.Contains(CostType.SUPP_ENGY_SHOULDER2) Then
            bReCalc = True
            objMaster.CostCollection.Remove(CostType.SUPP_ENGY_SHOULDER2)
            Log("[Remove Cost type SUPP_ENGY_SHOULDER2]")
        End If
        If objMaster.CostCollection.Contains(CostType.SUPP_TAX) Then
            bReCalc = True
            objMaster.CostCollection.Remove(CostType.SUPP_TAX)
            Log("[Remove Cost type SUPP_TAX]")
        End If
        If objMaster.CostCollection.Contains(CostType.SUPP_ENGY_CRITPEAK) Then
            bReCalc = True
            objMaster.CostCollection.Remove(CostType.SUPP_ENGY_CRITPEAK)
            Log("[Remove Cost type SUPP_ENGY_CRITPEAK]")
        End If
        If bReCalc Then
            objMaster = FinalizeTotalCost(objMaster)
        End If

        Return (objMaster)
    End Function

    ''' <summary>
    ''' Remove SUPP_ENERGY cost types from cost collection.
    ''' </summary>
    ''' <param name="objMaster"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function RemoveSuppEngyCostType(ByVal objMaster As CE.RateEngine.CostResult) As CE.RateEngine.CostResult

        If objMaster.CostCollection.Contains(CostType.SUPP_ENGY) Then
            objMaster.CostCollection.Remove(CostType.SUPP_ENGY)
            Log("[Remove Cost type SUPP_ENGY]")
        End If

        If objMaster.CostCollection.Contains(CostType.SUPP_ENGY_ALLOTHER) Then
            objMaster.CostCollection.Remove(CostType.SUPP_ENGY_ALLOTHER)
            Log("[Remove Cost type SUPP_ENGY_ALLOTHER]")
        End If

        If objMaster.CostCollection.Contains(CostType.SUPP_ENGY_GREEN1) Then
            objMaster.CostCollection.Remove(CostType.SUPP_ENGY_GREEN1)
            Log("[Remove Cost type SUPP_ENGY_GREEN1]")
        End If

        If objMaster.CostCollection.Contains(CostType.SUPP_ENGY_GREEN2) Then
            objMaster.CostCollection.Remove(CostType.SUPP_ENGY_GREEN2)
            Log("[Remove Cost type SUPP_ENGY_GREEN2]")
        End If

        If objMaster.CostCollection.Contains(CostType.SUPP_ENGY_GREEN3) Then
            objMaster.CostCollection.Remove(CostType.SUPP_ENGY_GREEN3)
            Log("[Remove Cost type SUPP_ENGY_GREEN3]")
        End If

        If objMaster.CostCollection.Contains(CostType.SUPP_ENGY_OFFPEAK) Then
            objMaster.CostCollection.Remove(CostType.SUPP_ENGY_OFFPEAK)
            Log("[Remove Cost type SUPP_ENGY_OFFPEAK]")
        End If

        If objMaster.CostCollection.Contains(CostType.SUPP_ENGY_ONPEAK) Then
            objMaster.CostCollection.Remove(CostType.SUPP_ENGY_ONPEAK)
            Log("[Remove Cost type SUPP_ENGY_ONPEAK]")
        End If

        If objMaster.CostCollection.Contains(CostType.SUPP_ENGY_SHOULDER1) Then
            objMaster.CostCollection.Remove(CostType.SUPP_ENGY_SHOULDER1)
            Log("[Remove Cost type SUPP_ENGY_SHOULDER1]")
        End If

        If objMaster.CostCollection.Contains(CostType.SUPP_ENGY_SHOULDER2) Then
            objMaster.CostCollection.Remove(CostType.SUPP_ENGY_SHOULDER2)
            Log("[Remove Cost type SUPP_ENGY_SHOULDER2]")
        End If

        If objMaster.CostCollection.Contains(CostType.SUPP_ENGY_CRITPEAK) Then
            objMaster.CostCollection.Remove(CostType.SUPP_ENGY_CRITPEAK)
            Log("[Remove Cost type SUPP_ENGY_CRITPEAK]")
        End If

        objMaster = FinalizeTotalCost(objMaster)

        Return (objMaster)
    End Function

    ''' <summary>
    ''' Apply discount before tax factor to the total cost (excluding rebates)
    ''' Before Taxes are applied: Multiply sum of charges by (1+discountBeforeTax) to compute new TotalCosts
    ''' </summary>
    ''' <param name="objMaster"></param>
    ''' <param name="objDiscountBeforeTaxFactors"></param>
    ''' <remarks></remarks>
    Private Function ApplyDiscountBeforeTax(ByVal objMaster As CE.RateEngine.CostResult, ByVal objDiscountBeforeTaxFactors As RateClassModifierCollection) As CE.RateEngine.CostResult
        Dim dbtf As Double = 0.0
        Dim discountAmount As Double = 0.0
        Dim totalDiscountAmount As Double = 0.0
        Dim c As CE.RateEngine.Cost
        Dim rebateCost As Double = 0.0
        Dim rebateCostExists As Boolean = False

        ' Rebates must be excluded here before discount is calculated (rebates are negative numbers)
        If (objMaster.CostCollection.ContainsAnyRebates()) Then
            rebateCost = objMaster.CostCollection.GetRebateCost()
            rebateCost = Math.Abs(rebateCost)
            If (rebateCost > 0) Then rebateCostExists = True
        End If

        ' Add up all the discount before apply to the final total cost
        For Each objDiscountBeforeTax As RateClassModifier In objDiscountBeforeTaxFactors
            dbtf = CType(objDiscountBeforeTax.Value, Double)
            If (Not dbtf = 0) Then
                If (dbtf > 0) Then
                    dbtf = -1 * dbtf
                End If
                ' Any rebateCost is added back into the totalCost here for the discount calculation only
                discountAmount = ((objMaster.TotalCost + rebateCost) * (1 + dbtf)) - (objMaster.TotalCost + rebateCost)
                totalDiscountAmount = totalDiscountAmount + discountAmount
            End If
        Next

        If (totalDiscountAmount <> 0.0) Then
            c = New Cost(CostType.RCMA_DISCOUNTBEFORETAX, totalDiscountAmount)
            objMaster.CostCollection(CostType.RCMA_DISCOUNTBEFORETAX) = c

            Log("...[FinalizeMasterCost][" & CostType.RCMA_DISCOUNTBEFORETAX.ToString() & ", discount amount is " & totalDiscountAmount & "]")
            If (rebateCostExists) Then
                Log("...[FinalizeMasterCost][" & CostType.RCMA_DISCOUNTBEFORETAX.ToString() & ", rebate of " & rebateCost & " excluded in discount determination]")
            End If

            objMaster = FinalizeTotalCost(objMaster)
        End If

        Return (objMaster)
    End Function

    ''' <summary>
    ''' Apply tax cost to the total cost
    ''' Multiply cost by (1 + TaxCostFactor) to get new Total Cost
    ''' support 0, 1 or more key-value pairs
    ''' </summary>
    ''' <param name="objMaster"></param>
    ''' <param name="objTaxCostFactors"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function ApplyTaxCost(ByVal objMaster As CE.RateEngine.CostResult, ByVal objTaxCostFactors As RateClassModifierCollection) As CE.RateEngine.CostResult
        Dim dblTax As Double = 0.0
        Dim dblTotalCost As Double = 0.0
        Dim objCost As CE.RateEngine.Cost
        Dim dblTaxCostFactor As Double = 0.0

        ' add up all the tax cost before apply to the final total cost
        For Each objTaxCost As RateClassModifier In objTaxCostFactors
            dblTaxCostFactor = CType(objTaxCost.Value, Double)
            If Not dblTaxCostFactor = 0 Then
                dblTax = dblTax + (objMaster.TotalCost * dblTaxCostFactor)
            End If
        Next
        If Not dblTax = 0 Then
            objCost = New Cost(CostType.RCMA_TAXCOST, dblTax)
            objMaster.CostCollection(CostType.RCMA_TAXCOST) = objCost

            Log("...[FinalizeMasterCost][" & CostType.RCMA_TAXCOST.ToString() & ", " & dblTax & "]")

            objMaster = FinalizeTotalCost(objMaster)

        End If

        Return (objMaster)
    End Function

    ''' <summary>
    ''' Apply rate modifier cost adjustment(s)
    ''' </summary>
    ''' <param name="objCostResult"></param>
    ''' <param name="objCostAdjustmentRateClassModifiers"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function ApplyRateClassModifierCostAdjustment(ByVal objCostResult As CE.RateEngine.CostResult,
                                                          ByVal objCostAdjustmentRateClassModifiers As RateClassModifierCollection) As CE.RateEngine.CostResult

        Dim dblCostAdjustmentValue As Double
        Dim sRateClassModifierName As String = String.Empty

        Try

            'Iterate through list of rate class modifiers.
            For Each objCostAdjustmentRateClassModifier As RateClassModifier In objCostAdjustmentRateClassModifiers

                'Retrieve cost adjustment value.
                If Double.TryParse(objCostAdjustmentRateClassModifier.Value, dblCostAdjustmentValue) = False Then
                    dblCostAdjustmentValue = 0
                End If

                sRateClassModifierName = [Enum].GetName(objCostAdjustmentRateClassModifier.ModifierType.GetType(), objCostAdjustmentRateClassModifier.ModifierType)

                'Standard customer cost exists.
                If objCostResult.CostCollection.Contains(CostType.STD_CUST) Then

                    'Adjust standard customer cost with cost adjustment value.
                    objCostResult.CostCollection.SumAdd(CostType.STD_CUST, New Cost(CostType.STD_CUST, dblCostAdjustmentValue))
                    Log(String.Format("...Adjust STD_CUST with rate class modifier - cost adjustment. (RateClassModifier name: {0}, value: {1})",
                                      sRateClassModifierName, dblCostAdjustmentValue))


                Else
                    'Add standard customer cost with cost adjustment value.
                    objCostResult.CostCollection.Add(CostType.STD_CUST, New Cost(CostType.STD_CUST, dblCostAdjustmentValue))
                    Log(String.Format("...Add/Adjust STD_CUST with rate class modifier - cost adjustment. (RateClassModifier name: {0}, value: {1})",
                                      sRateClassModifierName, dblCostAdjustmentValue))
                End If

                Log(String.Format("...[FinalizeMasterCost][{0}, {1}]", CostType.STD_CUST.ToString(), dblCostAdjustmentValue))

                objCostResult = FinalizeTotalCost(objCostResult)

            Next

        Catch

            Throw

        End Try

        Return (objCostResult)

    End Function
    ''' <summary>
    ''' 'CR56757 -  Apply rate class modifier - Include Excluded CostTypes And Calculate Total Cost
    ''' </summary>
    ''' <param name="objCostResult"></param>
    ''' <param name="objAllList"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function IncludeExcludedCostTypesAndCalculateTotalCost(ByVal objCostResult As CE.RateEngine.CostResult,
                                                           ByVal objAllList As CostCollection) As CE.RateEngine.CostResult

        Dim bReCalc As Boolean = False

        For Each objCost As Cost In objAllList.Values

            'if Cost type doesnt  exists in master cost collection, then add it
            If Not (objCostResult.CostCollection.Contains(objCost.CostType)) Then

                objCostResult.CostCollection.Add(objCost.CostType, New Cost(objCost.CostType, objCost.Amount))
                bReCalc = True
                Log(String.Format("...[Include Discount Cost Type back to cost collection To Calculate Total Cost : (RateClassModifier name: {0}, value: {1})]",
                                  objCost.CostType, objCost.Amount))
            End If

        Next

        If bReCalc Then
            objCostResult = FinalizeTotalCost(objCostResult)
        End If

        Return objCostResult

    End Function

    ''' <summary>
    ''' Apply rate class modifier - exclude cost types.
    ''' </summary>
    ''' <param name="objCostResult"></param>
    ''' <param name="objExcludeCostTypeRateClassModifiers"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function ApplyRateClassModifierExcludeCostTypes(ByVal objCostResult As CE.RateEngine.CostResult,
                                                            ByVal objExcludeCostTypeRateClassModifiers As RateClassModifierCollection) As CE.RateEngine.CostResult

        Dim sarrExcludeCostTypeList As String() = Nothing
        Dim sExcludeCostTypes As String = String.Empty
        Dim eExcludeCostType As CostType
        Dim sRateClassModifierName As String = String.Empty
        Dim bReCalc As Boolean = False

        Try

            For Each objRateClassModifier As RateClassModifier In objExcludeCostTypeRateClassModifiers


                sRateClassModifierName = [Enum].GetName(objRateClassModifier.ModifierType.GetType(), objRateClassModifier.ModifierType)

                sExcludeCostTypes = objRateClassModifier.Value

                If (String.IsNullOrEmpty(sExcludeCostTypes) = False) Then

                    sarrExcludeCostTypeList = sExcludeCostTypes.Split(New Char() {","c})

                    For Each sExcludeCostType As String In sarrExcludeCostTypeList

                        Try
                            'Convert cost type name into cost type enumeration.
                            eExcludeCostType = CType([Enum].Parse(GetType(CostType), sExcludeCostType), CostType)

                        Catch
                            Log(String.Format("...[Unable to remove cost type: {0}.  Cost type not found. (RateClassModifier name: {1}, value: {2})]",
                                              sExcludeCostType, sRateClassModifierName, objRateClassModifier.Value))
                            Continue For
                        End Try

                        'Cost type to exclude exists in cost collection.
                        If (objCostResult.CostCollection.Contains(eExcludeCostType) = True) Then

                            'Remove cost type.
                            objCostResult.CostCollection.Remove(eExcludeCostType)

                            bReCalc = True

                            Log(String.Format("...[Remove cost type: {0} (RateClassModifier name: {1}, value: {2})]",
                                              sExcludeCostType, sRateClassModifierName, objRateClassModifier.Value))
                        End If

                    Next

                End If

            Next

            If bReCalc Then
                objCostResult = FinalizeTotalCost(objCostResult)
            End If

        Catch

            Throw

        End Try

        Return objCostResult

    End Function

    ''' <summary>
    ''' Adjust tax usage using the tax usage factor in the list
    ''' </summary>
    ''' <param name="objUsageColl"></param>
    ''' <param name="objTaxUsageFactors"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function AdjustTaxUsage(ByVal objUsageColl As UsageCollection, ByVal objTaxUsageFactors As RateClassModifierCollection) As UsageCollection
        Dim dblTaxUsageFactor As Double = 0.0
        Dim dblTotalTaxUsageFactor As Double = 0.0
        For Each objTaxUsageFactor As RateClassModifier In objTaxUsageFactors
            If Not objTaxUsageFactor.Value = "0" Then
                Double.TryParse(objTaxUsageFactor.Value, dblTaxUsageFactor)
                dblTotalTaxUsageFactor = dblTaxUsageFactor + dblTaxUsageFactor
            End If
        Next
        If Not dblTotalTaxUsageFactor = 0 Then
            ' Log the usages before adjust the tax usage factor
            objUsageColl.ScaleTaxUsageFactor(1 + dblTotalTaxUsageFactor)
            Log("[Tax Usage Factor (" + dblTotalTaxUsageFactor.ToString() + ") adjustment applied to Usages]")
        End If

        Return (objUsageColl)
    End Function

    ''' <summary>
    ''' apply discount after tax factor to total cost - CR 20298, 20299, 20302 Oct 2011
    ''' After Taxes are applied: Multiply sum of charges by (1+DiscountAfterTax) to compute new TotalCosts
    ''' Support 0, 1 or more key-value pairs
    ''' </summary>
    ''' <param name="objMaster"></param>
    ''' <param name="objDiscountAfterTaxFactors"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function ApplyDiscountAfterTax(ByVal objMaster As CE.RateEngine.CostResult, ByVal objDiscountAfterTaxFactors As RateClassModifierCollection) As CE.RateEngine.CostResult
        Dim dblDiscountAfterTax As Double = 0.0
        Dim dblTotalCost As Double = 0.0
        Dim objCost As CE.RateEngine.Cost
        Dim dblDiscountAfterTaxFactor As Double = 0.0

        ' Add up all the discount before apply to the final total cost
        For Each objDiscountAfterTax As RateClassModifier In objDiscountAfterTaxFactors
            dblDiscountAfterTaxFactor = CType(objDiscountAfterTax.Value, Double)
            If Not dblDiscountAfterTaxFactor = 0 Then
                If (dblDiscountAfterTaxFactor > 0) Then
                    dblDiscountAfterTaxFactor = -1 * dblDiscountAfterTaxFactor
                End If
                dblDiscountAfterTax = dblDiscountAfterTax + (objMaster.TotalCost * dblDiscountAfterTaxFactor)
            End If
        Next

        If Not dblDiscountAfterTax = 0 Then
            objCost = New Cost(CostType.RCMA_DISCOUNTAFTERTAX, dblDiscountAfterTax)
            objMaster.CostCollection.Add(CostType.RCMA_DISCOUNTAFTERTAX, objCost)

            Log("...[FinalizeMasterCost][" & CostType.RCMA_DISCOUNTAFTERTAX.ToString() & ", " & dblDiscountAfterTax & "]")

            objMaster = FinalizeTotalCost(objMaster)
        End If

        Return (objMaster)
    End Function
    ''' <summary>
    ''' 'Adjustment handler for annual demand bill demand rate helper.
    ''' 'CR56757 - Applied ADBD Factor and Rule
    ''' </summary>
    ''' <param name="uc"></param>
    ''' <param name="modifier"></param>
    ''' <param name="factor"></param>
    ''' <param name="rule"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function AdjustDemandBillUsage(ByVal uc As UsageCollection, ByVal modifier As RateClassModifier, ByVal factor As RateClassModifier, ByVal rule As RateClassModifier) As UsageCollection
        Dim annualDemandBillValue As Double = 0.0
        Dim adbdfactor As Double = 1.0
        Dim adbdRule As Integer = 0
        Dim adjust As Boolean = False

        If (Not modifier Is Nothing AndAlso modifier.ModifierType = RateClassModifierType.AnnualDemandBillDemand AndAlso Not String.IsNullOrEmpty(modifier.Value)) Then
            Dim result As Boolean = Double.TryParse(modifier.Value, annualDemandBillValue)
            If (result) Then
                adjust = True
                If factor.Value = "0" Then
                    adbdfactor = 1
                Else
                    Double.TryParse(factor.Value, adbdfactor)
                End If

                Integer.TryParse(rule.Value, adbdRule)
            End If
        End If

        If (adjust) Then
            ' Log the usages before adjust the tax usage factor
            Dim adjustmentMade As Boolean = uc.AdjustDemandBillQuantity(annualDemandBillValue, adbdfactor, adbdRule)
            If (adjustmentMade) Then
                Log("[Annual Demand Adjustment Applied to DemandBill Usage (" + annualDemandBillValue.ToString() + ")]")
                Log("[ADBD Factor Applied to DemandBill Usage (" + adbdfactor.ToString() + ")]")
                Log("[ADBD Rule Applied to DemandBill Usage (" + adbdRule.ToString() + ")]")
            End If
        End If

        Return (uc)
    End Function


#End Region


#Region "Get Rate Helper Value - Private"

    Private Function GetRoundingDecimalPlacesValue(ByVal rateClass As String) As Integer
        Dim value As Integer = 3

        If (Not rateClass Is Nothing AndAlso rateClass.Length > 0 AndAlso (rateClass.StartsWith(ksnRateClassHash))) Then
            Dim stringValue As String = RateClassModifierCollection.GetModifierValueFromRateClassModifierString(rateClass, RateClassModifierCollection.ksnRDP)
            If (Not String.IsNullOrEmpty(stringValue)) Then
                Dim parsed As Boolean = Integer.TryParse(stringValue, value)
                If (Not parsed) Then
                    value = 3
                End If
                If (value > 7) Then value = 7
                If (value < 0) Then value = 0
            End If
        End If

        Return (value)
    End Function

    ''' <summary>
    ''' Get the CRKW rate helper attribute value from rate class string.
    ''' </summary>
    ''' <param name="rateClass"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function GetCapacityReserveDemandValue(ByVal rateClass As String) As Double?
        Dim x As Double? = Nothing
        Dim xx As Double = 0.0

        If (Not rateClass Is Nothing AndAlso rateClass.Length > 0 AndAlso (rateClass.StartsWith(ksnRateClassHash))) Then
            Dim stringValue As String = RateClassModifierCollection.GetModifierValueFromRateClassModifierString(rateClass, RateClassModifierCollection.ksnCRKW)
            If (Not String.IsNullOrEmpty(stringValue)) Then
                Dim parsed As Boolean = Double.TryParse(stringValue, xx)
                If (Not parsed) Then
                    x = Nothing
                Else
                    x = xx
                End If
            End If
        End If

        Return (x)
    End Function

    ''' <summary>
    ''' Get the NCDM rate helper attribute value from rate class string.
    ''' </summary>
    ''' <param name="rateClass"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function GetNonCoincidentDemandValue(ByVal rateClass As String) As Double?
        Dim x As Double? = Nothing
        Dim xx As Double = 0.0

        If (Not rateClass Is Nothing AndAlso rateClass.Length > 0 AndAlso (rateClass.StartsWith(ksnRateClassHash))) Then
            Dim stringValue As String = RateClassModifierCollection.GetModifierValueFromRateClassModifierString(rateClass, RateClassModifierCollection.ksnNCDM)
            If (Not String.IsNullOrEmpty(stringValue)) Then
                Dim parsed As Boolean = Double.TryParse(stringValue, xx)
                If (Not parsed) Then
                    x = Nothing
                Else
                    x = xx
                End If
            End If
        End If

        Return (x)
    End Function

    '--- CR50560 BEGIN -------------------------------------------------------------------------------------------
    ''' <summary>
    ''' Get the CPKWC rate helper attribute value from rate class string.
    ''' </summary>
    ''' <param name="rateClass"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function GetCoincidentDemandValue(ByVal rateClass As String) As Double?
        Dim x As Double? = Nothing
        Dim xx As Double = 0.0

        If (Not rateClass Is Nothing AndAlso rateClass.Length > 0 AndAlso (rateClass.StartsWith(ksnRateClassHash))) Then
            Dim stringValue As String = RateClassModifierCollection.GetModifierValueFromRateClassModifierString(rateClass, RateClassModifierCollection.ksnCPKWC)
            If (Not String.IsNullOrEmpty(stringValue)) Then
                Dim parsed As Boolean = Double.TryParse(stringValue, xx)
                If (Not parsed) Then
                    x = Nothing
                Else
                    x = xx
                End If
            End If
        End If

        Return (x)
    End Function

    ''' <summary>
    ''' Get the cpcf rate helper attribute value from rate class string.
    ''' </summary>
    ''' <param name="rateClass"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function GetCoincidentPeakMultiplicationFactor(ByVal rateClass As String) As Double?
        Dim x As Double? = Nothing
        Dim xx As Double = 0.0

        If (Not rateClass Is Nothing AndAlso rateClass.Length > 0 AndAlso (rateClass.StartsWith(ksnRateClassHash))) Then
            Dim stringValue As String = RateClassModifierCollection.GetModifierValueFromRateClassModifierString(rateClass, RateClassModifierCollection.ksnCPCF)
            If (Not String.IsNullOrEmpty(stringValue)) Then
                Dim parsed As Boolean = Double.TryParse(stringValue, xx)
                If (Not parsed) Then
                    x = Nothing
                Else
                    x = xx
                End If
            End If
        End If

        Return (x)
    End Function

    ''' <summary>
    ''' Get the cptime rate helper attribute value from rate class string.
    ''' </summary>
    ''' <param name="rateClass"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function GetCoincidentPeakDateTime(ByVal rateClass As String) As DateTime?
        Dim x As DateTime? = DateTime.MinValue
        Dim xx As DateTime = DateTime.MinValue
        If (Not rateClass Is Nothing AndAlso rateClass.Length > 0 AndAlso (rateClass.StartsWith(ksnRateClassHash))) Then
            Dim stringValue As String = RateClassModifierCollection.GetModifierValueFromRateClassModifierString(rateClass, RateClassModifierCollection.ksnCPTIME)
            If (Not String.IsNullOrEmpty(stringValue)) Then
                Dim parsed As Boolean = DateTime.TryParse(stringValue, xx)
                If (Not parsed) Then
                    x = Nothing
                Else
                    x = xx
                End If
            End If
        End If

        Return (x)
    End Function
    '--- CR50560 END ---------------------------------------------------------------------------------------------

    ''' <summary>
    ''' Get the NMR rate helper attribute value from rate class string.
    ''' </summary>
    ''' <param name="rateClass"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function GetNMRValue(ByVal rateClass As String) As String
        Dim NMRValue As String = String.Empty

        If (Not rateClass Is Nothing AndAlso rateClass.Length > 0 AndAlso (rateClass.StartsWith(ksnRateClassHash))) Then
            NMRValue = RateClassModifierCollection.GetModifierValueFromRateClassModifierString(rateClass, RateClassModifierCollection.ksnNMR)
        End If

        Return (NMRValue)
    End Function

    ''' <summary>
    ''' Get annual demand bill demand value from rate class string.
    ''' </summary>
    ''' <param name="rateClass"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function GetAnnualDemandBillDemandValue(ByVal rateClass As String) As Double
        Dim value As Double = 0.0

        If (Not rateClass Is Nothing AndAlso rateClass.Length > 0 AndAlso (Not rateClass.StartsWith(ksnRateClassHash))) Then
            Dim stringValue As String = RateClassModifierCollection.GetModifierValueFromRateClassModifierString(rateClass, RateClassModifierCollection.ksnADBD)
            If (Not String.IsNullOrEmpty(stringValue)) Then
                Dim parsed As Boolean = Double.TryParse(stringValue, value)
                If (Not parsed) Then
                    value = 0.0
                End If
            End If
        End If

        Return (value)
    End Function

    ''' <summary>
    ''' Get average usage attribute value from rate class string.
    ''' </summary>
    ''' <param name="rateClass"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function GetAverageUsageValue(ByVal rateClass As String) As Double
        Dim value As Double = 0.0

        If (Not rateClass Is Nothing AndAlso rateClass.Length > 0 AndAlso (rateClass.StartsWith(ksnRateClassHash))) Then
            Dim stringValue As String = RateClassModifierCollection.GetModifierValueFromRateClassModifierString(rateClass, RateClassModifierCollection.ksnAVGU)
            If (Not String.IsNullOrEmpty(stringValue)) Then
                Dim parsed As Boolean = Double.TryParse(stringValue, value)
                If (Not parsed) Then
                    value = 0.0
                End If
            End If
        End If

        Return (value)
    End Function

    ''' <summary>
    ''' Get Actual Consumption Factor attribute value from rate class string.
    ''' </summary>
    ''' <param name="rateClass"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function GetActualConsumptionFactorValue(ByVal rateClass As String) As Double
        Dim value As Double = 0.0

        If (Not rateClass Is Nothing AndAlso rateClass.Length > 0 AndAlso (rateClass.StartsWith(ksnRateClassHash))) Then
            Dim stringValue As String = RateClassModifierCollection.GetModifierValueFromRateClassModifierString(rateClass, RateClassModifierCollection.ksnACFU)
            If (Not String.IsNullOrEmpty(stringValue)) Then
                Dim parsed As Boolean = Double.TryParse(stringValue, value)
                If (Not parsed) Then
                    value = 0.0
                End If
            End If
        End If

        Return (value)
    End Function

    ''' <summary>
    ''' Get tax cost factor value from rate class string.
    ''' </summary>
    ''' <param name="rateClass"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function GetTaxCostFactorValue(ByVal rateClass As String) As Double
        Dim value As Double = 0.0

        If (Not rateClass Is Nothing AndAlso rateClass.Length > 0 AndAlso (rateClass.StartsWith(ksnRateClassHash))) Then
            Dim stringValue As String = RateClassModifierCollection.GetModifierValueFromRateClassModifierString(rateClass, RateClassModifierCollection.ksnTCF)
            If (Not String.IsNullOrEmpty(stringValue)) Then
                Dim parsed As Boolean = Double.TryParse(stringValue, value)
                If (Not parsed) Then
                    value = 0.0
                End If
            End If
        End If

        Return (value)
    End Function

    ''' <summary>
    ''' Get Days Usage attribute value from rate class string.
    ''' </summary>
    ''' <param name="rateClass"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function GetDaysUsageValue(ByVal rateClass As String) As Integer
        Dim value As Integer = 0

        If (Not rateClass Is Nothing AndAlso rateClass.Length > 0 AndAlso (rateClass.StartsWith(ksnRateClassHash))) Then
            Dim stringValue As String = RateClassModifierCollection.GetModifierValueFromRateClassModifierString(rateClass, RateClassModifierCollection.ksnDAYU)
            If (Not String.IsNullOrEmpty(stringValue)) Then
                Dim parsed As Boolean = Integer.TryParse(stringValue, value)
                If (Not parsed) Then
                    value = 0
                End If
            End If
        End If

        Return (value)
    End Function


#End Region


End Class
