﻿Imports CE.RateEngine.Enums

''' <summary>
''' For settings that can be defined for a client.  Started using in 10/2013 when advanced NEM introduced.
''' </summary>
''' <remarks></remarks>
Public Class RateCompanySettings

    Public Property Version As String
    Public Property Nem3ConstantD1 As Double
    Public Property Nem3ConstantD2 As Double
    Public Property Nem3CostTypes As String
    Public Property Nem4ConstantD1 As Double
    Public Property Nem4ConstantD2 As Double
    Public Property Nem4CostTypes As String
    Public Property Nem5ConstantD1 As Double
    Public Property Nem5ConstantD2 As Double
    Public Property Nem5CostTypes As String

    Public Sub New()
    End Sub

    ''' <summary>
    ''' Get specific nem settings from overall settings.
    ''' </summary>
    ''' <param name="NMRValue"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function GetNEMSettings(ByVal NMRValue As NetMeteringType) As NetMeteringSettings
        Dim nemSettings As New NetMeteringSettings()

        Select Case NMRValue

            Case NetMeteringType.Nem3
                nemSettings.D1 = Nem3ConstantD1
                nemSettings.D2 = Nem3ConstantD2
                nemSettings.CostTypeCSV = Nem3CostTypes

            Case NetMeteringType.Nem4
                nemSettings.D1 = Nem4ConstantD1
                nemSettings.D2 = Nem4ConstantD2
                nemSettings.CostTypeCSV = Nem4CostTypes

            Case NetMeteringType.Nem5
                nemSettings.D1 = Nem5ConstantD1
                nemSettings.D2 = Nem5ConstantD2
                nemSettings.CostTypeCSV = Nem5CostTypes

            Case Else
                'noop 

        End Select

        Return (nemSettings)
    End Function
End Class
