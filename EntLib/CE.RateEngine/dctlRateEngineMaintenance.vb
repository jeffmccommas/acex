Imports System.Text
Imports System.Data.SqlClient
Imports System.Data.SqlTypes
Imports Microsoft.ApplicationBlocks.Data
Imports CE.RateEngine.Enums

''' <summary>
''' This class provides database access to support rate engine maintenance functionality.
''' </summary>
''' <remarks>None.</remarks>
Public Class dctlRateEngineMaintenance

#Region "Constants"
    Private Const ExceptionDataReaderError As String = "Rate Engine Maintenance data reader error."
    Private Const ExceptionConnectionStringNotSpecifiedError As String = _
                                                "Rate Engine Maintenance connection string not specified."
    Private Const ExceptionExecuteScalarError As String = "Rate Engine Maintenance execute scalar error."
#End Region

#Region "Enums And Structures"
#End Region

#Region "Private Data Members"

    Private _rateEngineMaintenanceConnectionString As String = String.Empty

#End Region

    Public Sub New(ByVal rateEngineMaintenanceConnectionString As String)
        _rateEngineMaintenanceConnectionString = rateEngineMaintenanceConnectionString
    End Sub

#Region "Properties"

    Public ReadOnly Property RateEngineMaintenanceConnectionString() As String
        Get
            'Connection string is required when this class is instantiated.
            If (String.IsNullOrEmpty(_rateEngineMaintenanceConnectionString) = True) Then
                Throw New Exception(ExceptionConnectionStringNotSpecifiedError)
            End If

            Return (_rateEngineMaintenanceConnectionString)
        End Get
    End Property

#End Region

#Region "Database Helper Functions"

    ' Replacement for function of same name in VB namespace (removed that namespace)
    <System.ComponentModel.Browsable(False)> _
    Public Function IsDBNull(ByVal dbvalue As Object) As Boolean
        Return dbvalue Is DBNull.Value
    End Function

#End Region

#Region "Rate Maintenance Data"

    Public Function GetRateMaintDataSession(ByVal sessionID As String) As SqlDataReader

        Const StoredProcedureName As String = "rateengine.GetRateMaintDataSession"

        Dim commandText As New StringBuilder
        Dim dataReader As SqlDataReader
        Dim commandParameters(1) As SqlParameter

        commandText.Append(StoredProcedureName)

        commandParameters(0) = New SqlParameter("@SessionID", sessionID)
        commandParameters(0).Direction = ParameterDirection.Input

        Try

            dataReader = SqlHelper.ExecuteReader(RateEngineMaintenanceConnectionString, _
                                                 CommandType.StoredProcedure, _
                                                 commandText.ToString(), _
                                                 commandParameters)

        Catch ex As Exception

            Throw New Exception(ExceptionDataReaderError + ": " + ex.Message, ex.InnerException)

        Finally
            'NOTE: No cleanup necessary. Caller is expected to close data reader.
        End Try

        Return (dataReader)

    End Function

    Public Function InsertRateMaintData(ByVal sessionID As String, _
                                        ByVal uploadDateTime As DateTime, _
                                        ByVal rateCompanyID As Integer, _
                                        ByVal rateMasterID As Integer, _
                                        ByVal sourceTypeID As Integer, _
                                        ByVal sourceID As Integer, _
                                        ByVal clientRateID As String, _
                                        ByVal rateDefID As Integer, _
                                        ByVal effectiveDate As DateTime, _
                                        ByVal chargeType As Integer, _
                                        ByVal tier As Integer, _
                                        ByVal timeOfUse As Integer, _
                                        ByVal seasonID As Integer, _
                                        ByVal stepID As Integer, _
                                        ByVal partTypeID As Integer, _
                                        ByVal costTypeID As Integer, _
                                        ByVal chargeValue As Double, _
                                        ByVal calcTypeID As Integer, _
                                        ByVal errorText As String, _
                                        ByVal importStatus As Integer, _
                                        ByVal importDateTime As DateTime, _
                                        ByVal originalRateCompanyID As String, _
                                        ByVal originalSourceType As String, _
                                        ByVal originalSourceID As String, _
                                        ByVal originalSourceName As String, _
                                        ByVal originalRateClass As String, _
                                        ByVal originalEffectiveDate As String, _
                                        ByVal originalTypeOfCharge As String, _
                                        ByVal originalTier As String, _
                                        ByVal originalTimeOfUse As String, _
                                        ByVal originalSeason As String, _
                                        ByVal originalPartType As String, _
                                        ByVal originalCostType As String, _
                                        ByVal originalChargeValue As String, _
                                        ByVal originalCalcType As String, _
                                        ByVal originalStep As String, _
                                        ByVal RebateClassID As Integer, _
                                        ByVal originalRebateClassID As String) As Integer

        Const StoredProcedureName As String = "InsertRateMaintData"

        Dim commandText As New StringBuilder
        Dim result As Integer
        Dim commandParameters(38) As SqlParameter

        commandText.Append(StoredProcedureName)

        Try

            commandParameters(0) = New SqlParameter("@SessionID", sessionID)
            commandParameters(0).Direction = ParameterDirection.Input
            commandParameters(1) = New SqlParameter("@UploadDateTime", uploadDateTime)
            commandParameters(1).Direction = ParameterDirection.Input
            commandParameters(2) = New SqlParameter("@RateCompanyID", rateCompanyID)
            commandParameters(2).Direction = ParameterDirection.Input
            commandParameters(3) = New SqlParameter("@RateMasterID", rateMasterID)
            commandParameters(3).Direction = ParameterDirection.Input
            commandParameters(4) = New SqlParameter("@SourceTypeID", sourceTypeID)
            commandParameters(4).Direction = ParameterDirection.Input
            commandParameters(5) = New SqlParameter("@SourceID", sourceID)
            commandParameters(5).Direction = ParameterDirection.Input
            commandParameters(6) = New SqlParameter("@ClientRateID", clientRateID)
            commandParameters(6).Direction = ParameterDirection.Input
            commandParameters(7) = New SqlParameter("@RateDefID", rateDefID)
            commandParameters(7).Direction = ParameterDirection.Input
            If effectiveDate = DateTime.MinValue Then
                commandParameters(8) = New SqlParameter("@EffectiveDate", SqlDateTime.Null)
            Else
                commandParameters(8) = New SqlParameter("@EffectiveDate", effectiveDate)
            End If
            commandParameters(8).Direction = ParameterDirection.Input
            commandParameters(8).IsNullable = True
            commandParameters(9) = New SqlParameter("@ChargeType", chargeType)
            commandParameters(9).Direction = ParameterDirection.Input
            commandParameters(10) = New SqlParameter("@Tier", tier)
            commandParameters(10).Direction = ParameterDirection.Input
            commandParameters(11) = New SqlParameter("@TimeOfUse", timeOfUse)
            commandParameters(11).Direction = ParameterDirection.Input
            commandParameters(12) = New SqlParameter("@SeasonID", seasonID)
            commandParameters(12).Direction = ParameterDirection.Input
            commandParameters(13) = New SqlParameter("@StepID", stepID)
            commandParameters(13).Direction = ParameterDirection.Input
            commandParameters(14) = New SqlParameter("@PartTypeID", partTypeID)
            commandParameters(14).Direction = ParameterDirection.Input
            commandParameters(15) = New SqlParameter("@CostTypeID", costTypeID)
            commandParameters(15).Direction = ParameterDirection.Input
            commandParameters(16) = New SqlParameter("@ChargeValue", chargeValue)
            commandParameters(16).Direction = ParameterDirection.Input
            commandParameters(17) = New SqlParameter("@CalcTypeID", calcTypeID)
            commandParameters(17).Direction = ParameterDirection.Input
            commandParameters(18) = New SqlParameter("@ErrorText", errorText)
            commandParameters(18).Direction = ParameterDirection.Input
            commandParameters(19) = New SqlParameter("@ImportStatus", importStatus)
            commandParameters(19).Direction = ParameterDirection.Input
            If importDateTime = DateTime.MinValue Then
                commandParameters(20) = New SqlParameter("@ImportDateTime", SqlDateTime.Null)
            Else
                commandParameters(20) = New SqlParameter("@ImportDateTime", importDateTime)
            End If
            commandParameters(20).Direction = ParameterDirection.Input
            commandParameters(20).IsNullable = True
            commandParameters(21) = New SqlParameter("@originalRateCompanyID", originalRateCompanyID)
            commandParameters(21).Direction = ParameterDirection.Input
            commandParameters(22) = New SqlParameter("@originalSourceType", originalSourceType)
            commandParameters(22).Direction = ParameterDirection.Input
            commandParameters(23) = New SqlParameter("@originalSourceID", originalSourceID)
            commandParameters(23).Direction = ParameterDirection.Input
            commandParameters(24) = New SqlParameter("@originalSourceName", originalSourceName)
            commandParameters(24).Direction = ParameterDirection.Input
            commandParameters(25) = New SqlParameter("@originalRateClass", originalRateClass)
            commandParameters(25).Direction = ParameterDirection.Input
            commandParameters(26) = New SqlParameter("@originalEffectiveDate", originalEffectiveDate)
            commandParameters(26).Direction = ParameterDirection.Input
            commandParameters(27) = New SqlParameter("@originalTypeOfCharge", originalTypeOfCharge)
            commandParameters(27).Direction = ParameterDirection.Input
            commandParameters(28) = New SqlParameter("@originalTier", originalTier)
            commandParameters(28).Direction = ParameterDirection.Input
            commandParameters(29) = New SqlParameter("@originalTimeOfUse", originalTimeOfUse)
            commandParameters(29).Direction = ParameterDirection.Input
            commandParameters(30) = New SqlParameter("@originalSeason", originalSeason)
            commandParameters(30).Direction = ParameterDirection.Input
            commandParameters(31) = New SqlParameter("@originalPartType", originalPartType)
            commandParameters(31).Direction = ParameterDirection.Input
            commandParameters(32) = New SqlParameter("@originalCostType", originalCostType)
            commandParameters(32).Direction = ParameterDirection.Input
            commandParameters(33) = New SqlParameter("@originalChargeValue", originalChargeValue)
            commandParameters(33).Direction = ParameterDirection.Input
            commandParameters(34) = New SqlParameter("@originalCalcType", originalCalcType)
            commandParameters(34).Direction = ParameterDirection.Input
            commandParameters(35) = New SqlParameter("@originalStep", originalStep)
            commandParameters(35).Direction = ParameterDirection.Input
            'CR 20298, 20299, 20302 Oct 2011 - add rebate class ID
            commandParameters(36) = New SqlParameter("@RebateClassID", RebateClassID)
            commandParameters(36).Direction = ParameterDirection.Input
            commandParameters(37) = New SqlParameter("@originalRebateClassID", originalRebateClassID)
            commandParameters(37).Direction = ParameterDirection.Input


            result = CInt(SqlHelper.ExecuteScalar(RateEngineMaintenanceConnectionString, _
                                                  CommandType.StoredProcedure, _
                                                  commandText.ToString(), _
                                                  commandParameters))

        Catch ex As Exception

            Throw New Exception(ExceptionExecuteScalarError + ": " + ex.Message, ex.InnerException)

        Finally

        End Try

        Return (result)

    End Function

    Public Function UpdateRateMaintData(ByVal rateMaintID As Integer, _
                                        ByVal sessionID_New As String, _
                                        ByVal uploadDateTime_New As DateTime, _
                                        ByVal rateCompanyID_New As Integer, _
                                        ByVal rateMasterID_New As Integer, _
                                        ByVal sourceTypeID_New As Integer, _
                                        ByVal sourceID_New As Integer, _
                                        ByVal clientRateID_New As String, _
                                        ByVal rateDefID_New As Integer, _
                                        ByVal effectiveDate_New As DateTime, _
                                        ByVal chargeType_New As Integer, _
                                        ByVal tier_New As Integer, _
                                        ByVal timeOfUse_New As Integer, _
                                        ByVal seasonID_New As Integer, _
                                        ByVal stepID_New As Integer, _
                                        ByVal partTypeID_New As Integer, _
                                        ByVal costTypeID_New As Integer, _
                                        ByVal chargeValue_New As Double, _
                                        ByVal calcTypeID_New As Integer, _
                                        ByVal errorText_New As String, _
                                        ByVal importStatus_New As Integer, _
                                        ByVal importDateTime_New As DateTime, _
                                        ByVal originalRateCompanyID_New As String, _
                                        ByVal originalSourceType_New As String, _
                                        ByVal originalSourceID_New As String, _
                                        ByVal originalSourceName_New As String, _
                                        ByVal originalRateClass_New As String, _
                                        ByVal originalEffectiveDate_New As String, _
                                        ByVal originalTypeOfCharge_New As String, _
                                        ByVal originalTier_New As String, _
                                        ByVal originalTimeOfUse_New As String, _
                                        ByVal originalSeason_New As String, _
                                        ByVal originalPartType_New As String, _
                                        ByVal originalCostType_New As String, _
                                        ByVal originalChargeValue_New As String, _
                                        ByVal originalCalcType_New As String, _
                                        ByVal originalStep_New As String, _
                                        ByVal rebateClassID_New As Integer, _
                                        ByVal originalRebateClassID_New As String) As Integer

        Const StoredProcedureName As String = "UpdateRateMaintData"

        Dim commandText As New StringBuilder
        Dim result As Integer
        Dim commandParameters(39) As SqlParameter

        commandText.Append(StoredProcedureName)

        commandParameters(0) = New SqlParameter("@RateMaintID", rateMaintID)
        commandParameters(0).Direction = ParameterDirection.Input
        commandParameters(1) = New SqlParameter("@SessionID_New", sessionID_New)
        commandParameters(1).Direction = ParameterDirection.Input
        commandParameters(2) = New SqlParameter("@UploadDateTime_New", uploadDateTime_New)
        commandParameters(2).Direction = ParameterDirection.Input
        commandParameters(3) = New SqlParameter("@RateCompanyID_New", rateCompanyID_New)
        commandParameters(3).Direction = ParameterDirection.Input
        commandParameters(4) = New SqlParameter("@RateMasterID_New", rateMasterID_New)
        commandParameters(4).Direction = ParameterDirection.Input
        commandParameters(5) = New SqlParameter("@SourceTypeID_New", sourceTypeID_New)
        commandParameters(5).Direction = ParameterDirection.Input
        commandParameters(6) = New SqlParameter("@SourceID_New", sourceID_New)
        commandParameters(6).Direction = ParameterDirection.Input
        commandParameters(7) = New SqlParameter("@ClientRateID_New", clientRateID_New)
        commandParameters(7).Direction = ParameterDirection.Input
        commandParameters(8) = New SqlParameter("@RateDefID_New", rateDefID_New)
        commandParameters(8).Direction = ParameterDirection.Input
        If effectiveDate_New = DateTime.MinValue Then
            commandParameters(9) = New SqlParameter("@EffectiveDate_New", SqlDateTime.Null)
        Else
            commandParameters(9) = New SqlParameter("@EffectiveDate_New", effectiveDate_New)
        End If
        commandParameters(9).Direction = ParameterDirection.Input
        commandParameters(10) = New SqlParameter("@ChargeType_New", chargeType_New)
        commandParameters(10).Direction = ParameterDirection.Input
        commandParameters(11) = New SqlParameter("@Tier_New", tier_New)
        commandParameters(11).Direction = ParameterDirection.Input
        commandParameters(12) = New SqlParameter("@TimeOfUse_New", timeOfUse_New)
        commandParameters(12).Direction = ParameterDirection.Input
        commandParameters(13) = New SqlParameter("@SeasonID_New", seasonID_New)
        commandParameters(13).Direction = ParameterDirection.Input
        commandParameters(14) = New SqlParameter("@StepID_New", stepID_New)
        commandParameters(14).Direction = ParameterDirection.Input
        commandParameters(15) = New SqlParameter("@PartTypeID_New", partTypeID_New)
        commandParameters(15).Direction = ParameterDirection.Input
        commandParameters(16) = New SqlParameter("@CostTypeID_New", costTypeID_New)
        commandParameters(16).Direction = ParameterDirection.Input
        commandParameters(17) = New SqlParameter("@ChargeValue_New", chargeValue_New)
        commandParameters(17).Direction = ParameterDirection.Input
        commandParameters(18) = New SqlParameter("@CalcTypeID_New", calcTypeID_New)
        commandParameters(18).Direction = ParameterDirection.Input
        commandParameters(19) = New SqlParameter("@ErrorText_New", errorText_New)
        commandParameters(19).Direction = ParameterDirection.Input
        commandParameters(20) = New SqlParameter("@ImportStatus_New", importStatus_New)
        commandParameters(20).Direction = ParameterDirection.Input
        If importDateTime_New = DateTime.MinValue Then
            commandParameters(21) = New SqlParameter("@ImportDateTime_New", SqlDateTime.Null)
        Else
            commandParameters(21) = New SqlParameter("@ImportDateTime_New", importDateTime_New)
        End If
        commandParameters(21).Direction = ParameterDirection.Input
        commandParameters(22) = New SqlParameter("@originalRateCompanyID_New", originalRateCompanyID_New)
        commandParameters(22).Direction = ParameterDirection.Input
        commandParameters(23) = New SqlParameter("@originalSourceType_New", originalSourceType_New)
        commandParameters(23).Direction = ParameterDirection.Input
        commandParameters(24) = New SqlParameter("@originalSourceID_New", originalSourceID_New)
        commandParameters(24).Direction = ParameterDirection.Input
        commandParameters(25) = New SqlParameter("@originalSourceName_New", originalSourceName_New)
        commandParameters(25).Direction = ParameterDirection.Input
        commandParameters(26) = New SqlParameter("@originalRateClass_New", originalRateClass_New)
        commandParameters(26).Direction = ParameterDirection.Input
        commandParameters(27) = New SqlParameter("@originalEffectiveDate_New", originalEffectiveDate_New)
        commandParameters(27).Direction = ParameterDirection.Input
        commandParameters(28) = New SqlParameter("@originalTypeOfCharge_New", originalTypeOfCharge_New)
        commandParameters(28).Direction = ParameterDirection.Input
        commandParameters(29) = New SqlParameter("@originalTier_New", originalTier_New)
        commandParameters(29).Direction = ParameterDirection.Input
        commandParameters(30) = New SqlParameter("@originalTimeOfUse_New", originalTimeOfUse_New)
        commandParameters(30).Direction = ParameterDirection.Input
        commandParameters(31) = New SqlParameter("@originalSeason_New", originalSeason_New)
        commandParameters(31).Direction = ParameterDirection.Input
        commandParameters(32) = New SqlParameter("@originalPartType_New", originalPartType_New)
        commandParameters(32).Direction = ParameterDirection.Input
        commandParameters(33) = New SqlParameter("@originalCostType_New", originalCostType_New)
        commandParameters(33).Direction = ParameterDirection.Input
        commandParameters(34) = New SqlParameter("@originalChargeValue_New", originalChargeValue_New)
        commandParameters(34).Direction = ParameterDirection.Input
        commandParameters(35) = New SqlParameter("@originalCalcType_New", originalCalcType_New)
        commandParameters(35).Direction = ParameterDirection.Input
        commandParameters(36) = New SqlParameter("@originalStep_New", originalStep_New)
        commandParameters(36).Direction = ParameterDirection.Input
        'CR 20298, 20299, 20302 Oct 2011 - add rebate class ID
        commandParameters(37) = New SqlParameter("@RebateClassID_New", rebateClassID_New)
        commandParameters(37).Direction = ParameterDirection.Input
        commandParameters(38) = New SqlParameter("@originalRebateClassID_New", originalRebateClassID_New)
        commandParameters(38).Direction = ParameterDirection.Input

        Try

            result = CInt(SqlHelper.ExecuteScalar(RateEngineMaintenanceConnectionString, _
                                                  CommandType.StoredProcedure, _
                                                  commandText.ToString(), _
                                                  commandParameters))

        Catch ex As Exception

            Throw New Exception(ExceptionExecuteScalarError + ": " + ex.Message, ex.InnerException)

        Finally

        End Try

        Return (result)

    End Function

    Public Function DeleteRateMaintDataSession(ByVal sessionID As String) As Integer

        Const StoredProcedureName As String = "DeleteRateMaintDataSession"

        Dim commandText As New StringBuilder
        Dim result As Integer
        Dim commandParameters(1) As SqlParameter

        commandText.Append(StoredProcedureName)

        commandParameters(0) = New SqlParameter("@SessionID", sessionID)
        commandParameters(0).Direction = ParameterDirection.Input

        Try

            result = CInt(SqlHelper.ExecuteScalar(RateEngineMaintenanceConnectionString, _
                                                  CommandType.StoredProcedure, _
                                                  commandText.ToString(), _
                                                  commandParameters))

        Catch ex As Exception

            Throw New Exception(ExceptionExecuteScalarError + ": " + ex.Message, ex.InnerException)

        Finally

        End Try

        Return (result)

    End Function

#End Region

#Region "Rate Definition"

    Public Function GetRateDefinitionByStartDate(ByVal rateMasterID As Integer, _
                                                 ByVal startDate As DateTime) As Integer

        Const StoredProcedureName As String = "GetRateDefinitionByStartDate"

        Dim commandText As New StringBuilder
        Dim dataReader As SqlDataReader
        Dim commandParameters(2) As SqlParameter
        Dim result As Integer

        commandText.Append(StoredProcedureName)

        commandParameters(0) = New SqlParameter("@RateMasterID", rateMasterID)
        commandParameters(0).Direction = ParameterDirection.Input
        commandParameters(1) = New SqlParameter("@StartDate", startDate)
        commandParameters(1).Direction = ParameterDirection.Input

        Try

            dataReader = SqlHelper.ExecuteReader(RateEngineMaintenanceConnectionString, _
                                                 CommandType.StoredProcedure, _
                                                 commandText.ToString(), _
                                                 commandParameters)
            result = 0
            Using dataReader

                'NOTE: Only one record is expected.
                Do While dataReader.Read()

                    If (Not IsDBNull(dataReader("RateDefID"))) Then
                        result = CInt(CType(dataReader("RateDefID"), String))
                    End If

                Loop

            End Using

        Catch ex As Exception

            Throw New Exception(ExceptionDataReaderError + ": " + ex.Message, ex.InnerException)

        End Try

        Return (result)
    End Function

    Public Function GetNewestRateDefinition(ByVal rateMasterID As Integer) As Integer

        Const StoredProcedureName As String = "GetNewestRateDefinition"

        Dim commandText As New StringBuilder
        Dim dataReader As SqlDataReader
        Dim commandParameters(2) As SqlParameter
        Dim result As Integer

        commandText.Append(StoredProcedureName)

        commandParameters(0) = New SqlParameter("@RateMasterID", rateMasterID)
        commandParameters(0).Direction = ParameterDirection.Input
        commandParameters(1) = New SqlParameter("@EndDate", DateTime.Now.AddYears(1))
        commandParameters(1).Direction = ParameterDirection.Input

        Try

            dataReader = SqlHelper.ExecuteReader(RateEngineMaintenanceConnectionString, _
                                                 CommandType.StoredProcedure, _
                                                 commandText.ToString(), _
                                                 commandParameters)
            result = 0
            Using dataReader

                'NOTE: Only one record is expected.
                Do While dataReader.Read()

                    If (Not IsDBNull(dataReader("RateDefID"))) Then
                        result = CInt(CType(dataReader("RateDefID"), String))
                    End If

                Loop

            End Using

        Catch ex As Exception

            Throw New Exception(ExceptionDataReaderError + ": " + ex.Message, ex.InnerException)

        End Try

        Return (result)

    End Function

    Public Function CloneRateDefinition(ByVal sourceRdID As Integer, _
                                        ByVal sourceRmID As Integer, _
                                        ByVal rateMasterID As Integer, _
                                        ByVal startDate As DateTime, _
                                        ByVal editDate As DateTime, _
                                        ByVal enabled As Boolean, _
                                        ByVal startDateValid As Boolean) As Integer


        Const StoredProcedureName As String = "AuthCloneRateDefinition"

        Dim commandText As New StringBuilder
        Dim commandParameters(7) As SqlParameter
        Dim result As Integer

        commandText.Append(StoredProcedureName)

        commandParameters(0) = New SqlParameter("@sourceRdID", sourceRdID)
        commandParameters(0).Direction = ParameterDirection.Input
        commandParameters(1) = New SqlParameter("@RateMasterID", rateMasterID)
        commandParameters(1).Direction = ParameterDirection.Input
        commandParameters(2) = New SqlParameter("@StartDate", startDate)
        commandParameters(2).Direction = ParameterDirection.Input
        commandParameters(3) = New SqlParameter("@EditDate", editDate)
        commandParameters(3).Direction = ParameterDirection.Input
        commandParameters(4) = New SqlParameter("@Enabled", enabled)
        commandParameters(4).Direction = ParameterDirection.Input
        commandParameters(5) = New SqlParameter("@StartDateValid", startDateValid)
        commandParameters(5).Direction = ParameterDirection.Input
        commandParameters(6) = New SqlParameter("@sourceRmID", sourceRmID)
        commandParameters(6).Direction = ParameterDirection.Input

        Try

            result = CInt(SqlHelper.ExecuteScalar(RateEngineMaintenanceConnectionString, _
                                                  CommandType.StoredProcedure, _
                                                  commandText.ToString(), _
                                                  commandParameters))

        Catch ex As Exception

            Throw New Exception(ExceptionExecuteScalarError + ": " + ex.Message, ex.InnerException)

        Finally

        End Try

        Return (result)

    End Function

#End Region

#Region "Minimum Charge"

    Public Function GetMinimumCharge(ByVal rateDefID As Integer, _
                                     ByVal calcTypeID As Short, _
                                     ByVal costTypeID As Integer) As SqlDataReader

        Const StoredProcedureName As String = "GetMinimumCharge"

        Dim commandText As New StringBuilder
        Dim dataReader As SqlDataReader
        Dim commandParameters(3) As SqlParameter

        commandText.Append(StoredProcedureName)

        commandParameters(0) = New SqlParameter("@RateDefID", rateDefID)
        commandParameters(0).Direction = ParameterDirection.Input
        commandParameters(1) = New SqlParameter("@CalcTypeID", calcTypeID)
        commandParameters(1).Direction = ParameterDirection.Input
        commandParameters(2) = New SqlParameter("@CostTypeID", costTypeID)
        commandParameters(2).Direction = ParameterDirection.Input

        Try

            dataReader = SqlHelper.ExecuteReader(RateEngineMaintenanceConnectionString, _
                                                 CommandType.StoredProcedure, _
                                                 commandText.ToString(), _
                                                 commandParameters)

        Catch ex As Exception

            Throw New Exception(ExceptionDataReaderError + ": " + ex.Message, ex.InnerException)

        Finally
            'NOTE: No cleanup necessary. Caller is expected to close data reader.
        End Try

        Return (dataReader)
    End Function

    Public Function InsertMinimumCharge(ByVal rateDefID As Integer, _
                                        ByVal calcTypeID As Short, _
                                        ByVal costTypeID As Integer, _
                                        ByVal chargeValue As Double) As Integer

        Const StoredProcedureName As String = "InsertMinimumCharge"

        Dim commandText As New StringBuilder
        Dim result As Integer
        Dim commandParameters(4) As SqlParameter

        commandText.Append(StoredProcedureName)

        commandParameters(0) = New SqlParameter("@RateDefID", rateDefID)
        commandParameters(0).Direction = ParameterDirection.Input
        commandParameters(1) = New SqlParameter("@CalcTypeID", calcTypeID)
        commandParameters(1).Direction = ParameterDirection.Input
        commandParameters(2) = New SqlParameter("@CostTypeID", costTypeID)
        commandParameters(2).Direction = ParameterDirection.Input
        commandParameters(3) = New SqlParameter("@ChargeValue", chargeValue)
        commandParameters(3).Direction = ParameterDirection.Input

        Try

            result = CInt(SqlHelper.ExecuteScalar(RateEngineMaintenanceConnectionString, _
                                                  CommandType.StoredProcedure, _
                                                  commandText.ToString(), _
                                                  commandParameters))

        Catch ex As Exception

            Throw New Exception(ExceptionExecuteScalarError + ": " + ex.Message, ex.InnerException)

        Finally

        End Try

        Return (result)

    End Function

    Public Function UpdateMinimumCharge(ByVal rateDefID As Integer, _
                                        ByVal calcTypeID As Short, _
                                        ByVal costTypeID As Integer, _
                                        ByVal calcTypeID_New As Short, _
                                        ByVal costTypeID_New As Integer, _
                                        ByVal chargeValue_New As Double) As Integer
        Const StoredProcedureName As String = "UpdateMinimumCharge"

        Dim commandText As New StringBuilder
        Dim result As Integer
        Dim commandParameters(6) As SqlParameter

        commandText.Append(StoredProcedureName)

        commandParameters(0) = New SqlParameter("@rateDefID", rateDefID)
        commandParameters(0).Direction = ParameterDirection.Input
        commandParameters(1) = New SqlParameter("@calcTypeID", calcTypeID)
        commandParameters(1).Direction = ParameterDirection.Input
        commandParameters(2) = New SqlParameter("@costTypeID", costTypeID)
        commandParameters(2).Direction = ParameterDirection.Input
        commandParameters(3) = New SqlParameter("@calcTypeID_New", calcTypeID_New)
        commandParameters(3).Direction = ParameterDirection.Input
        commandParameters(4) = New SqlParameter("@costTypeID_New", costTypeID_New)
        commandParameters(4).Direction = ParameterDirection.Input
        commandParameters(5) = New SqlParameter("@chargeValue_New", chargeValue_New)
        commandParameters(5).Direction = ParameterDirection.Input

        Try

            result = CInt(SqlHelper.ExecuteScalar(RateEngineMaintenanceConnectionString, _
                                                  CommandType.StoredProcedure, _
                                                  commandText.ToString(), _
                                                  commandParameters))

        Catch ex As Exception

            Throw New Exception(ExceptionExecuteScalarError + ": " + ex.Message, ex.InnerException)

        Finally

        End Try

        Return (result)

    End Function

#End Region

#Region "Service Charge"

    Public Function GetServiceCharge(ByVal rateDefID As Integer, _
                                     ByVal calcTypeID As Short, _
                                     ByVal costTypeID As Integer, _
                                     ByVal stepID As Short, _
                                     ByVal seasonID As Short, _
                                     ByVal rebateClassID As Short) As SqlDataReader

        Const StoredProcedureName As String = "GetServiceCharge"

        Dim commandText As New StringBuilder
        Dim dataReader As SqlDataReader
        Dim commandParameters(6) As SqlParameter

        commandText.Append(StoredProcedureName)

        commandParameters(0) = New SqlParameter("@RateDefID", rateDefID)
        commandParameters(0).Direction = ParameterDirection.Input
        commandParameters(1) = New SqlParameter("@calcTypeID", calcTypeID)
        commandParameters(1).Direction = ParameterDirection.Input
        commandParameters(2) = New SqlParameter("@costTypeID", costTypeID)
        commandParameters(2).Direction = ParameterDirection.Input
        commandParameters(3) = New SqlParameter("@stepID", stepID)
        commandParameters(3).Direction = ParameterDirection.Input
        commandParameters(4) = New SqlParameter("@seasonID", seasonID)
        commandParameters(4).Direction = ParameterDirection.Input
        'CR 20298, 20299, 20302 Oct 2011 - Rebate Class
        commandParameters(4) = New SqlParameter("@RebateClassID", rebateClassID)
        commandParameters(4).Direction = ParameterDirection.Input

        Try

            dataReader = SqlHelper.ExecuteReader(RateEngineMaintenanceConnectionString, _
                                                 CommandType.StoredProcedure, _
                                                 commandText.ToString(), _
                                                 commandParameters)

        Catch ex As Exception

            Throw New Exception(ExceptionDataReaderError + ": " + ex.Message, ex.InnerException)

        Finally
            'NOTE: No cleanup necessary. Caller is expected to close data reader.
        End Try

        Return (dataReader)
    End Function

    'CR 20298, 20299, 20302 Oct 2011 - insert service charge with rebate class ID
    Public Function InsertServiceCharge(ByVal rateDefID As Integer, _
                                        ByVal calcTypeID As Short, _
                                        ByVal costTypeID As Integer, _
                                        ByVal chargeValue As Double, _
                                        ByVal stepID As Short, _
                                        ByVal seasonID As Short, _
                                        ByVal rebateClassID As Short) As Integer

        Const StoredProcedureName As String = "InsertServiceCharge"

        Dim commandText As New StringBuilder
        Dim result As Integer
        Dim commandParameters(7) As SqlParameter

        commandText.Append(StoredProcedureName)

        commandParameters(0) = New SqlParameter("@RateDefID", rateDefID)
        commandParameters(0).Direction = ParameterDirection.Input
        commandParameters(1) = New SqlParameter("@CalcTypeID", calcTypeID)
        commandParameters(1).Direction = ParameterDirection.Input
        commandParameters(2) = New SqlParameter("@CostTypeID", costTypeID)
        commandParameters(2).Direction = ParameterDirection.Input
        commandParameters(3) = New SqlParameter("@ChargeValue", chargeValue)
        commandParameters(3).Direction = ParameterDirection.Input
        commandParameters(4) = New SqlParameter("@StepID", stepID)
        commandParameters(4).Direction = ParameterDirection.Input
        commandParameters(5) = New SqlParameter("@SeasonID", seasonID)
        commandParameters(5).Direction = ParameterDirection.Input
        commandParameters(6) = New SqlParameter("@RebateClassID", rebateClassID)
        commandParameters(6).Direction = ParameterDirection.Input


        Try

            result = CInt(SqlHelper.ExecuteScalar(RateEngineMaintenanceConnectionString, _
                                                  CommandType.StoredProcedure, _
                                                  commandText.ToString(), _
                                                  commandParameters))

        Catch ex As Exception

            Throw New Exception(ExceptionExecuteScalarError + ": " + ex.Message, ex.InnerException)

        Finally

        End Try

        Return (result)

    End Function

    'CR 20298, 20299, 20302 Oct 2011 - modified to add rebate class to update
    Public Function UpdateServiceCharge(ByVal rateDefID As Integer, _
                                        ByVal calcTypeID As Short, _
                                        ByVal costTypeID As Integer, _
                                        ByVal stepID As Short, _
                                        ByVal seasonID As Short, _
                                        ByVal calcTypeID_New As Short, _
                                        ByVal costTypeID_New As Integer, _
                                        ByVal chargeValue_New As Double, _
                                        ByVal stepID_New As Short, _
                                        ByVal seasonID_New As Short, _
                                        ByVal rebateClassID As Short, _
                                        ByVal rebateClassID_New As Short) As Integer

        Const StoredProcedureName As String = "UpdateServiceCharge"

        Dim commandText As New StringBuilder
        Dim result As Integer
        Dim commandParameters(12) As SqlParameter

        commandText.Append(StoredProcedureName)

        commandParameters(0) = New SqlParameter("@RateDefID", rateDefID)
        commandParameters(0).Direction = ParameterDirection.Input
        commandParameters(1) = New SqlParameter("@CalcTypeID", calcTypeID)
        commandParameters(1).Direction = ParameterDirection.Input
        commandParameters(2) = New SqlParameter("@CostTypeID", costTypeID)
        commandParameters(2).Direction = ParameterDirection.Input
        commandParameters(3) = New SqlParameter("@StepID", stepID)
        commandParameters(3).Direction = ParameterDirection.Input
        commandParameters(4) = New SqlParameter("@SeasonID", seasonID)
        commandParameters(4).Direction = ParameterDirection.Input
        commandParameters(5) = New SqlParameter("@RebateClassID", rebateClassID)
        commandParameters(5).Direction = ParameterDirection.Input
        commandParameters(6) = New SqlParameter("@CalcTypeID_New", calcTypeID_New)
        commandParameters(6).Direction = ParameterDirection.Input
        commandParameters(7) = New SqlParameter("@CostTypeID_New", costTypeID_New)
        commandParameters(7).Direction = ParameterDirection.Input
        commandParameters(8) = New SqlParameter("@ChargeValue_New", chargeValue_New)
        commandParameters(8).Direction = ParameterDirection.Input
        commandParameters(9) = New SqlParameter("@StepID_New", stepID_New)
        commandParameters(9).Direction = ParameterDirection.Input
        commandParameters(10) = New SqlParameter("@SeasonID_New", seasonID_New)
        commandParameters(10).Direction = ParameterDirection.Input
        commandParameters(11) = New SqlParameter("@RebateClassID_New", rebateClassID_New)
        commandParameters(11).Direction = ParameterDirection.Input

        Try

            result = CInt(SqlHelper.ExecuteScalar(RateEngineMaintenanceConnectionString, _
                                                  CommandType.StoredProcedure, _
                                                  commandText.ToString(), _
                                                  commandParameters))

        Catch ex As Exception

            Throw New Exception(ExceptionExecuteScalarError + ": " + ex.Message, ex.InnerException)

        Finally

        End Try

        Return (result)

    End Function

#End Region

#Region "Tax Charge"

    Public Function GetTaxCharge(ByVal rateDefID As Integer, _
                                 ByVal costTypeID As Integer) As SqlDataReader

        Const StoredProcedureName As String = "GetTaxCharge"

        Dim commandText As New StringBuilder
        Dim dataReader As SqlDataReader
        Dim commandParameters(2) As SqlParameter

        commandText.Append(StoredProcedureName)

        commandParameters(0) = New SqlParameter("@RateDefID", rateDefID)
        commandParameters(0).Direction = ParameterDirection.Input
        commandParameters(1) = New SqlParameter("@CostTypeID", costTypeID)
        commandParameters(1).Direction = ParameterDirection.Input

        Try

            dataReader = SqlHelper.ExecuteReader(RateEngineMaintenanceConnectionString, _
                                                 CommandType.StoredProcedure, _
                                                 commandText.ToString(), _
                                                 commandParameters)

        Catch ex As Exception

            Throw New Exception(ExceptionDataReaderError + ": " + ex.Message, ex.InnerException)

        Finally
            'NOTE: No cleanup necessary. Caller is expected to close data reader.
        End Try

        Return (dataReader)
    End Function

    Public Function InsertTaxCharge(ByVal rateDefID As Integer, _
                                    ByVal costTypeID As Integer, _
                                    ByVal chargeValue As Double) As Integer

        Const StoredProcedureName As String = "InsertTaxCharge"

        Dim commandText As New StringBuilder
        Dim result As Integer
        Dim commandParameters(3) As SqlParameter

        commandText.Append(StoredProcedureName)

        commandParameters(0) = New SqlParameter("@RateDefID", rateDefID)
        commandParameters(0).Direction = ParameterDirection.Input
        commandParameters(1) = New SqlParameter("@CostTypeID", costTypeID)
        commandParameters(1).Direction = ParameterDirection.Input
        commandParameters(2) = New SqlParameter("@ChargeValue", chargeValue)
        commandParameters(2).Direction = ParameterDirection.Input

        Try

            result = CInt(SqlHelper.ExecuteScalar(RateEngineMaintenanceConnectionString, _
                                                  CommandType.StoredProcedure, _
                                                  commandText.ToString(), _
                                                  commandParameters))

        Catch ex As Exception

            Throw New Exception(ExceptionExecuteScalarError + ": " + ex.Message, ex.InnerException)

        Finally

        End Try

        Return (result)

    End Function

    Public Function UpdateTaxCharge(ByVal rateDefID As Integer, _
                                    ByVal costTypeID As Integer, _
                                    ByVal costTypeID_New As Integer, _
                                    ByVal chargeValue_New As Double) As Integer

        Const StoredProcedureName As String = "UpdateTaxCharge"

        Dim commandText As New StringBuilder
        Dim result As Integer
        Dim commandParameters(4) As SqlParameter

        commandText.Append(StoredProcedureName)

        commandParameters(0) = New SqlParameter("@rateDefID", rateDefID)
        commandParameters(0).Direction = ParameterDirection.Input
        commandParameters(1) = New SqlParameter("@costTypeID", costTypeID)
        commandParameters(1).Direction = ParameterDirection.Input
        commandParameters(2) = New SqlParameter("@costTypeID_New", costTypeID_New)
        commandParameters(2).Direction = ParameterDirection.Input
        commandParameters(3) = New SqlParameter("@chargeValue_New", chargeValue_New)
        commandParameters(3).Direction = ParameterDirection.Input

        Try

            result = CInt(SqlHelper.ExecuteScalar(RateEngineMaintenanceConnectionString, _
                                                  CommandType.StoredProcedure, _
                                                  commandText.ToString(), _
                                                  commandParameters))

        Catch ex As Exception

            Throw New Exception(ExceptionExecuteScalarError + ": " + ex.Message, ex.InnerException)

        Finally

        End Try

        Return (result)

    End Function

#End Region

#Region "Use Charge"

    Public Function GetUseCharge(ByVal rateDefID As Integer, _
                                 ByVal tierID As Short, _
                                 ByVal touID As Short, _
                                 ByVal seasonID As Short, _
                                 ByVal costTypeID As Integer, _
                                 ByVal partTypeID As Short) As SqlDataReader

        Const StoredProcedureName As String = "GetUseCharge"

        Dim commandText As New StringBuilder
        Dim dataReader As SqlDataReader
        Dim commandParameters(6) As SqlParameter

        commandText.Append(StoredProcedureName)

        commandParameters(0) = New SqlParameter("@RateDefID", rateDefID)
        commandParameters(0).Direction = ParameterDirection.Input
        commandParameters(1) = New SqlParameter("@TierID", tierID)
        commandParameters(1).Direction = ParameterDirection.Input
        commandParameters(2) = New SqlParameter("@TouID", touID)
        commandParameters(2).Direction = ParameterDirection.Input
        commandParameters(3) = New SqlParameter("@SeasonID", seasonID)
        commandParameters(3).Direction = ParameterDirection.Input
        commandParameters(4) = New SqlParameter("@CostTypeID", costTypeID)
        commandParameters(4).Direction = ParameterDirection.Input
        commandParameters(5) = New SqlParameter("@PartTypeID", partTypeID)
        commandParameters(5).Direction = ParameterDirection.Input

        Try

            dataReader = SqlHelper.ExecuteReader(RateEngineMaintenanceConnectionString, _
                                                 CommandType.StoredProcedure, _
                                                 commandText.ToString(), _
                                                 commandParameters)

        Catch ex As Exception

            Throw New Exception(ExceptionDataReaderError + ": " + ex.Message, ex.InnerException)

        Finally
            'NOTE: No cleanup necessary. Caller is expected to close data reader.
        End Try

        Return (dataReader)
    End Function

    Public Function InsertUseCharge(ByVal rateDefID As Integer, _
                                    ByVal tierID As Short, _
                                    ByVal touID As Short, _
                                    ByVal seasonID As Short, _
                                    ByVal costTypeID As Integer, _
                                    ByVal partTypeID As Short, _
                                    ByVal chargeValue As Double, _
                                    ByVal rgID As Integer, _
                                    ByVal description As String, _
                                    ByVal rTPgID As Integer) As Integer

        Const StoredProcedureName As String = "InsertUseCharge"

        Dim commandText As New StringBuilder
        Dim result As Integer
        Dim commandParameters(10) As SqlParameter

        commandText.Append(StoredProcedureName)

        commandParameters(0) = New SqlParameter("@RateDefID", rateDefID)
        commandParameters(0).Direction = ParameterDirection.Input
        commandParameters(1) = New SqlParameter("@TierID", tierID)
        commandParameters(1).Direction = ParameterDirection.Input
        commandParameters(2) = New SqlParameter("@TouID", touID)
        commandParameters(2).Direction = ParameterDirection.Input
        commandParameters(3) = New SqlParameter("@SeasonID", seasonID)
        commandParameters(3).Direction = ParameterDirection.Input
        commandParameters(4) = New SqlParameter("@CostTypeID", costTypeID)
        commandParameters(4).Direction = ParameterDirection.Input
        commandParameters(5) = New SqlParameter("@PartTypeID", partTypeID)
        commandParameters(5).Direction = ParameterDirection.Input
        commandParameters(6) = New SqlParameter("@ChargeValue", chargeValue)
        commandParameters(6).Direction = ParameterDirection.Input
        If Not rgID > 0 Then
            commandParameters(7) = New SqlParameter("@RgID", DBNull.Value)
        Else
            commandParameters(7) = New SqlParameter("@RgID", rgID)
        End If
        commandParameters(7).Direction = ParameterDirection.Input
        commandParameters(8) = New SqlParameter("@Description", description)
        commandParameters(8).Direction = ParameterDirection.Input
        If Not rTPgID > 0 Then
            commandParameters(9) = New SqlParameter("@RTPgID", DBNull.Value)
        Else
            commandParameters(9) = New SqlParameter("@RTPgID", rTPgID)
        End If
        commandParameters(9).Direction = ParameterDirection.Input

        Try

            result = CInt(SqlHelper.ExecuteScalar(RateEngineMaintenanceConnectionString, _
                                                  CommandType.StoredProcedure, _
                                                  commandText.ToString(), _
                                                  commandParameters))

        Catch ex As Exception

            Throw New Exception(ExceptionExecuteScalarError + ": " + ex.Message, ex.InnerException)

        Finally

        End Try

        Return (result)

    End Function

    Public Function UpdateUseCharge(ByVal rateDefID As Integer, _
                                    ByVal tierID As Short, _
                                    ByVal touID As Short, _
                                    ByVal seasonID As Short, _
                                    ByVal costTypeID As Integer, _
                                    ByVal partTypeID As Short, _
                                    ByVal tierID_New As Short, _
                                    ByVal touID_New As Short, _
                                    ByVal seasonID_New As Short, _
                                    ByVal costTypeID_New As Integer, _
                                    ByVal partTypeID_New As Short, _
                                    ByVal chargeValue_New As Double, _
                                    ByVal rgID_New As Integer, _
                                    ByVal description_New As String, _
                                    ByVal rTPgID_New As Integer) As Integer

        Const StoredProcedureName As String = "UpdateUseCharge"

        Dim commandText As New StringBuilder
        Dim result As Integer
        Dim commandParameters(15) As SqlParameter

        commandText.Append(StoredProcedureName)

        commandParameters(0) = New SqlParameter("@rateDefID", rateDefID)
        commandParameters(0).Direction = ParameterDirection.Input
        commandParameters(1) = New SqlParameter("@tierID", tierID)
        commandParameters(1).Direction = ParameterDirection.Input
        commandParameters(2) = New SqlParameter("@touID", touID)
        commandParameters(2).Direction = ParameterDirection.Input
        commandParameters(3) = New SqlParameter("@seasonID", seasonID)
        commandParameters(3).Direction = ParameterDirection.Input
        commandParameters(4) = New SqlParameter("@costTypeID", costTypeID)
        commandParameters(4).Direction = ParameterDirection.Input
        commandParameters(5) = New SqlParameter("@partTypeID", partTypeID)
        commandParameters(5).Direction = ParameterDirection.Input
        commandParameters(6) = New SqlParameter("@tierID_New", tierID_New)
        commandParameters(6).Direction = ParameterDirection.Input
        commandParameters(7) = New SqlParameter("@touID_New", touID_New)
        commandParameters(7).Direction = ParameterDirection.Input
        commandParameters(8) = New SqlParameter("@seasonID_New", seasonID_New)
        commandParameters(8).Direction = ParameterDirection.Input
        commandParameters(9) = New SqlParameter("@costTypeID_New", costTypeID_New)
        commandParameters(9).Direction = ParameterDirection.Input
        commandParameters(10) = New SqlParameter("@partTypeID_New", partTypeID_New)
        commandParameters(10).Direction = ParameterDirection.Input
        commandParameters(11) = New SqlParameter("@chargeValue_New", chargeValue_New)
        commandParameters(11).Direction = ParameterDirection.Input
        If Not rgID_New > 0 Then
            commandParameters(12) = New SqlParameter("@rgID_New", DBNull.Value)
        Else
            commandParameters(12) = New SqlParameter("@rgID_New", rgID_New)
        End If
        commandParameters(12).Direction = ParameterDirection.Input
        commandParameters(13) = New SqlParameter("@description_New", description_New)
        commandParameters(13).Direction = ParameterDirection.Input
        If Not rgID_New > 0 Then
            commandParameters(14) = New SqlParameter("@rTPgID_New", DBNull.Value)
        Else
            commandParameters(14) = New SqlParameter("@rTPgID_New", rTPgID_New)
        End If
        commandParameters(14).Direction = ParameterDirection.Input

        Try

            result = CInt(SqlHelper.ExecuteScalar(RateEngineMaintenanceConnectionString, _
                                                  CommandType.StoredProcedure, _
                                                  commandText.ToString(), _
                                                  commandParameters))

        Catch ex As Exception

            Throw New Exception(ExceptionExecuteScalarError + ": " + ex.Message, ex.InnerException)

        Finally

        End Try

        Return (result)

    End Function

#End Region

#Region "Cost Type"


    Public Function GetCostTypeList() As SqlDataReader

        Const StoredProcedureName As String = "rateengine.GetCostTypeList"

        Dim commandText As New StringBuilder
        Dim dataReader As SqlDataReader

        commandText.Append(StoredProcedureName)

        Try

            dataReader = SqlHelper.ExecuteReader(RateEngineMaintenanceConnectionString, _
                                                 CommandType.StoredProcedure, _
                                                 commandText.ToString(), _
                                                 Nothing)

        Catch ex As Exception

            Throw New Exception(ExceptionDataReaderError + ": " + ex.Message, ex.InnerException)

        Finally
            'NOTE: No cleanup necessary. Caller is expected to close data reader.
        End Try

        Return (dataReader)
    End Function

#End Region

#Region "Charge Calc Type"


    Public Function GetChargeCalcTypeList() As SqlDataReader

        Const StoredProcedureName As String = "rateengine.GetChargeCalcTypeList"

        Dim commandText As New StringBuilder
        Dim dataReader As SqlDataReader

        commandText.Append(StoredProcedureName)

        Try

            dataReader = SqlHelper.ExecuteReader(RateEngineMaintenanceConnectionString, _
                                                 CommandType.StoredProcedure, _
                                                 commandText.ToString(), _
                                                 Nothing)

        Catch ex As Exception

            Throw New Exception(ExceptionDataReaderError + ": " + ex.Message, ex.InnerException)

        Finally
            'NOTE: No cleanup necessary. Caller is expected to close data reader.
        End Try

        Return (dataReader)
    End Function

#End Region

#Region "Step"

    Public Function GetStepList() As SqlDataReader

        Const StoredProcedureName As String = "rateengine.GetStepList"

        Dim commandText As New StringBuilder
        Dim dataReader As SqlDataReader

        commandText.Append(StoredProcedureName)

        Try

            dataReader = SqlHelper.ExecuteReader(RateEngineMaintenanceConnectionString, _
                                                 CommandType.StoredProcedure, _
                                                 commandText.ToString(), _
                                                 Nothing)

        Catch ex As Exception

            Throw New Exception(ExceptionDataReaderError + ": " + ex.Message, ex.InnerException)

        Finally
            'NOTE: No cleanup necessary. Caller is expected to close data reader.
        End Try

        Return (dataReader)
    End Function

#End Region

#Region "Season"

    Public Function GetSeasonList() As SqlDataReader

        Const StoredProcedureName As String = "rateengine.GetSeasonList"

        Dim commandText As New StringBuilder
        Dim dataReader As SqlDataReader

        commandText.Append(StoredProcedureName)

        Try

            dataReader = SqlHelper.ExecuteReader(RateEngineMaintenanceConnectionString, _
                                                 CommandType.StoredProcedure, _
                                                 commandText.ToString(), _
                                                 Nothing)

        Catch ex As Exception

            Throw New Exception(ExceptionDataReaderError + ": " + ex.Message, ex.InnerException)

        Finally
            'NOTE: No cleanup necessary. Caller is expected to close data reader.
        End Try

        Return (dataReader)

    End Function

#End Region

#Region "Tier"

    Public Function GetTierList() As SqlDataReader

        Const StoredProcedureName As String = "rateengine.GetTierList"

        Dim commandText As New StringBuilder
        Dim dataReader As SqlDataReader

        commandText.Append(StoredProcedureName)

        Try

            dataReader = SqlHelper.ExecuteReader(RateEngineMaintenanceConnectionString, _
                                                 CommandType.StoredProcedure, _
                                                 commandText.ToString(), _
                                                 Nothing)

        Catch ex As Exception

            Throw New Exception(ExceptionDataReaderError + ": " + ex.Message, ex.InnerException)

        Finally
            'NOTE: No cleanup necessary. Caller is expected to close data reader.
        End Try

        Return (dataReader)

    End Function

#End Region

#Region "Time of Use"

    Public Function GetTimeOfUseList() As SqlDataReader

        Const StoredProcedureName As String = "rateengine.GetTimeOfUseList"

        Dim commandText As New StringBuilder
        Dim dataReader As SqlDataReader

        commandText.Append(StoredProcedureName)

        Try

            dataReader = SqlHelper.ExecuteReader(RateEngineMaintenanceConnectionString, _
                                                 CommandType.StoredProcedure, _
                                                 commandText.ToString(), _
                                                 Nothing)

        Catch ex As Exception

            Throw New Exception(ExceptionDataReaderError + ": " + ex.Message, ex.InnerException)

        Finally
            'NOTE: No cleanup necessary. Caller is expected to close data reader.
        End Try

        Return (dataReader)

    End Function

#End Region

#Region "Part Type"

    Public Function GetPartTypeList() As SqlDataReader

        Const StoredProcedureName As String = "rateengine.GetPartTypeList"

        Dim commandText As New StringBuilder
        Dim dataReader As SqlDataReader

        commandText.Append(StoredProcedureName)

        Try

            dataReader = SqlHelper.ExecuteReader(RateEngineMaintenanceConnectionString, _
                                                 CommandType.StoredProcedure, _
                                                 commandText.ToString(), _
                                                 Nothing)

        Catch ex As Exception

            Throw New Exception(ExceptionDataReaderError + ": " + ex.Message, ex.InnerException)

        Finally
            'NOTE: No cleanup necessary. Caller is expected to close data reader.
        End Try

        Return (dataReader)

    End Function

#End Region

#Region "Rebate Class"
    ''' <summary>
    ''' CR 20298, 20299, 20302 Oct 2011 - Get rebate class list 
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function GetRebateClassList() As SqlDataReader

        Const StoredProcedureName As String = "rateengine.GetRebateClassList"

        Dim commandText As New StringBuilder
        Dim dataReader As SqlDataReader

        commandText.Append(StoredProcedureName)

        Try

            dataReader = SqlHelper.ExecuteReader(RateEngineMaintenanceConnectionString, _
                                                 CommandType.StoredProcedure, _
                                                 commandText.ToString(), _
                                                 Nothing)

        Catch ex As Exception

            Throw New Exception(ExceptionDataReaderError + ": " + ex.Message, ex.InnerException)

        Finally
            'NOTE: No cleanup necessary. Caller is expected to close data reader.
        End Try

        Return (dataReader)

    End Function
#End Region
End Class
