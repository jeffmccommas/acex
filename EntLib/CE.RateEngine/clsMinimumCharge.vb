Imports CE.RateEngine.Enums

Public Class MinimumCharge
	Inherits ChargeBase

	Private m_eCalculationType As ChargeCalcType

	Private Sub New()
	End Sub

	Public Sub New(ByVal eCostType As CostType, ByVal eCalculationType As ChargeCalcType, ByVal dblChargeValue As Double)
		MyBase.New(eCostType, dblChargeValue)
		m_eCalculationType = eCalculationType
	End Sub

#Region "Properties"

	Public ReadOnly Property CalculationType() As ChargeCalcType
		Get
			Return (m_eCalculationType)
		End Get
	End Property

#End Region

End Class
