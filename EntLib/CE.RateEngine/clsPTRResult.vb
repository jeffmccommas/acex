﻿Imports CE.RateEngine.Enums

<Serializable()> Public Class PTRResult

    Private m_overallAccountingRule As RebateAccountingRule
    Private m_rebateAmount As Double
    Private m_PTREvents As PTREventCollection

    Public Sub New()
        m_overallAccountingRule = RebateAccountingRule.EachEvent
        m_rebateAmount = 0.0
    End Sub

#Region "Properties"

    Public Property OverallAccountingRule() As RebateAccountingRule
        Get
            Return m_overallAccountingRule
        End Get
        Set(ByVal Value As RebateAccountingRule)
            m_overallAccountingRule = Value
        End Set
    End Property

    Public Property RebateAmount() As Double
        Get
            Return m_rebateAmount
        End Get
        Set(ByVal Value As Double)
            m_rebateAmount = Value
        End Set
    End Property

    Public Property PTREvents() As PTREventCollection
        Get
            Return m_PTREvents
        End Get
        Set(ByVal Value As PTREventCollection)
            m_PTREvents = Value
        End Set
    End Property

#End Region

    Public Function GetFinalRebateAmount() As Double
        Dim amount As Double = 0.0

        Select Case m_overallAccountingRule

            Case RebateAccountingRule.EachEvent

                If (Not m_PTREvents Is Nothing) Then
                    amount = m_PTREvents.SumRebateAmounts()
                End If

            Case RebateAccountingRule.EntireBillPeriod, RebateAccountingRule.Unspecified

                amount = m_rebateAmount

        End Select

        Return (amount)
    End Function

End Class
