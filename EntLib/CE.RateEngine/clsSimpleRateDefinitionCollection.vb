Public Class SimpleRateDefinitionCollection
	Inherits CollectionBase

	Public Sub New()
	End Sub

	Default ReadOnly Property Item(ByVal index As Integer) As SimpleRateDefinition
		Get
			Return CType(Me.InnerList.Item(index), SimpleRateDefinition)
		End Get
	End Property

	Public Function Add(ByVal value As SimpleRateDefinition) As Integer
		Return Me.InnerList.Add(value)
	End Function

	Public Sub Insert(ByVal index As Integer, ByVal value As SimpleRateDefinition)
		Me.InnerList.Insert(index, value)
	End Sub

	Public Sub AddRange(ByVal values() As SimpleRateDefinition)
		Me.InnerList.AddRange(values)
	End Sub

	Public Sub Remove(ByVal value As SimpleRateDefinition)
		Me.InnerList.Remove(value)
	End Sub

    Public Function ContainsDifferentRateMasters() As Boolean
        Dim bRetval As Boolean = False
        Dim nLastMasterID As Integer
        Dim i As Integer
        Dim objTemp As SimpleRateDefinition

        If (Me.InnerList.Count > 0) Then

            For i = 0 To Me.InnerList.Count - 1
                objTemp = CType(Me.InnerList.Item(i), SimpleRateDefinition)
                If (i = 0) Then nLastMasterID = objTemp.MasterID
                If (objTemp.MasterID <> nLastMasterID) Then
                    bRetval = True
                    Exit For
                End If
            Next

        End If

        Return (bRetval)
    End Function

End Class
