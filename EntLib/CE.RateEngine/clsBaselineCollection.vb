﻿Imports CE.RateEngine.Enums

<Serializable()> Public Class BaselineCollection
    Inherits CollectionBase
    Implements ICloneable

    Private m_allowBaselineUsageDetermination As Boolean
    Private m_allowActualUsageDetermination As Boolean
    Private m_useProvidedRebates As Boolean

    Public Sub New()
        m_allowBaselineUsageDetermination = False
        m_allowActualUsageDetermination = False
        m_useProvidedRebates = True
    End Sub

#Region "Properties"

    Public Property AllowBaselineUsageDetermination() As Boolean
        Get
            Return (m_allowBaselineUsageDetermination)
        End Get
        Set(ByVal Value As Boolean)
            m_allowBaselineUsageDetermination = Value
        End Set
    End Property

    Public Property AllowActualUsageDetermination() As Boolean
        Get
            Return (m_allowActualUsageDetermination)
        End Get
        Set(ByVal Value As Boolean)
            m_allowActualUsageDetermination = Value
        End Set
    End Property

    Public Property UseProvidedRebates() As Boolean
        Get
            Return (m_useProvidedRebates)
        End Get
        Set(ByVal Value As Boolean)
            m_useProvidedRebates = Value
        End Set
    End Property

#End Region

    Default ReadOnly Property Item(ByVal index As Integer) As Baseline
        Get
            Return CType(Me.InnerList.Item(index), Baseline)
        End Get
    End Property

    Public Function Add(ByVal value As Baseline) As Integer
        Return Me.InnerList.Add(value)
    End Function

    Public Sub Insert(ByVal index As Integer, ByVal value As Baseline)
        Me.InnerList.Insert(index, value)
    End Sub

    Public Sub AddRange(ByVal values() As Baseline)
        Me.InnerList.AddRange(values)
    End Sub

    Public Sub Remove(ByVal value As Baseline)
        Me.InnerList.Remove(value)
    End Sub

    Public Function FindByDate(ByVal d As Date) As Baseline
        Dim i As Integer
        Dim b As Baseline = Nothing
        Dim bFound As Boolean

        If (Me.InnerList.Count > 0) Then
            For i = 0 To (Me.InnerList.Count - 1)
                b = CType(Me.InnerList.Item(i), Baseline)
                If (b.UsageDate.Day = d.Day AndAlso b.UsageDate.Month = d.Month AndAlso b.UsageDate.Year = d.Year) Then
                    bFound = True
                    Exit For
                End If
            Next
        End If

        If (bFound) Then
            Return (b)
        Else
            Return Nothing
        End If
    End Function

    ''' <summary>
    ''' This directly implements deep clone by serializing to a memory stream
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function Clone() As Object Implements System.ICloneable.Clone
        Dim bf As System.Runtime.Serialization.Formatters.Binary.BinaryFormatter = New System.Runtime.Serialization.Formatters.Binary.BinaryFormatter
        Dim ms As System.IO.MemoryStream = New System.IO.MemoryStream
        Dim obj As BaselineCollection

        bf.Serialize(ms, Me)
        ms.Seek(0, System.IO.SeekOrigin.Begin)
        obj = CType(bf.Deserialize(ms), BaselineCollection)
        ms.Close()

        Return (obj)
    End Function

End Class
