Imports CE.RateEngine.Enums

Public Class CostCollection
	Inherits DictionaryBase


	Public Sub New()
	End Sub

	'special
	ReadOnly Property Values() As ICollection
		Get
			Return InnerHashTable.Values
		End Get
	End Property

	Default Property Item(ByVal eKey As CostType) As Cost
		Get
            Return CType(Me.InnerHashtable.Item(eKey), Cost)
		End Get
		Set(ByVal Value As Cost)
			Me.InnerHashtable.Item(eKey) = Value
		End Set
	End Property

	Public Sub SumAdd(ByVal eKey As CostType, ByVal objCost As Cost)
		If (Me.InnerHashtable.Contains(eKey)) Then
			Dim c As New Cost
			c = CType(Me.InnerHashtable.Item(eKey), Cost)
			c.Amount = c.Amount + objCost.Amount
		Else
			Me.InnerHashtable.Add(eKey, objCost)
		End If
	End Sub

	Public Sub Add(ByVal eKey As CostType, ByVal obj As Cost)
		Me.InnerHashtable.Add(eKey, obj)
	End Sub

	Public Sub Remove(ByVal eKey As CostType)
		Me.InnerHashtable.Remove(eKey)
	End Sub

	Public Function Contains(ByVal eKey As CostType) As Boolean
		Return (Me.InnerHashtable.Contains(eKey))
	End Function

    Public Function ContainsAnyRebates() As Boolean
        Return (Me.InnerHashtable.Contains(CostType.PTR_REBATE) Or Me.InnerHashtable.Contains(CostType.GEN_REBATE))
    End Function

    ' Functions to get totals for various kinds of cost types; 
    ' Examples for future might be GetSupplyCost, GetDistributionCost, etc.
    Public Function GetCustomerCost() As Double
        Return (GetCostByCostTypes(CostType.STD_CUST, _
                                    CostType.STD_CUST_LTALL, _
                                    CostType.STD_CUST_LTCOOLING, _
                                    CostType.STD_CUST_LTHEATING, _
                                    CostType.STD_CUST_LTWATERHEATING, _
                                    CostType.STD_DMND_CUST, _
                                    CostType.SUPP_CUST, _
                                    CostType.DIST_CUST))
    End Function

    Public Function GetRebateCost() As Double
        Return (GetCostByCostTypes(CostType.PTR_REBATE, CostType.GEN_REBATE))
    End Function

    Public Function GetCostByCostTypes(ByVal ParamArray eCostType() As CostType) As Double
        Dim dblResult As Double = 0
        Dim i As Integer

        If (Me.InnerHashtable.Count > 0) Then

            For i = 0 To eCostType.Length - 1
                If (Me.InnerHashtable.Contains(eCostType(i))) Then
                    dblResult = dblResult + CType(Me.InnerHashtable.Item(eCostType(i)), Cost).Amount
                End If
            Next

        End If

        Return (dblResult)
    End Function

    Public Function GetTotalCost() As Double
        Dim dblResult As Double = 0
        Dim c As Cost

        If (Me.InnerHashtable.Count > 0) Then
            For Each key As CostType In Me.InnerHashtable.Keys
                c = CType(Me.InnerHashtable.Item(key), Cost)
                dblResult = dblResult + c.Amount
            Next
        End If

        Return (dblResult)
    End Function

    Public Function Copy() As CostCollection
        Dim copyOfCosts As New CostCollection()
        Dim c As Cost

        If (Me.InnerHashtable.Count > 0) Then
            For Each key As CostType In Me.InnerHashtable.Keys
                c = CType(Me.InnerHashtable.Item(key), Cost)
                copyOfCosts.Add(c.CostType, New Cost(c.CostType, c.Amount))
            Next
        End If

        Return (copyOfCosts)
    End Function


End Class
