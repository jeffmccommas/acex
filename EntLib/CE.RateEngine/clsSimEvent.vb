﻿Imports CE.RateEngine.Enums

'This file contains simulated events and simulated duration related classes.


''' <summary>
''' Simulated event.  The month and priority only.
''' </summary>
''' <remarks></remarks>
Public Class SimEvent

    Private m_month As Integer
    Private m_priority As Integer

    Public Sub New()
    End Sub

    Public Sub New(ByVal month As Integer, ByVal priority As Integer)
        Me.New()
        m_month = month
        m_priority = priority
    End Sub

#Region "Properties"

    Public Property Month() As Integer
        Get
            Return m_month
        End Get
        Set(ByVal Value As Integer)
            m_month = Value
        End Set
    End Property

    Public Property Priority() As Integer
        Get
            Return m_priority
        End Get
        Set(ByVal Value As Integer)
            m_priority = Value
        End Set
    End Property

#End Region

End Class


Public Class SimEventCollection
    Inherits CollectionBase

    Public Sub New()
    End Sub

    Default ReadOnly Property Item(ByVal index As Integer) As SimEvent
        Get
            Return CType(Me.InnerList.Item(index), SimEvent)
        End Get
    End Property

    Public Function Add(ByVal value As SimEvent) As Integer
        Return Me.InnerList.Add(value)
    End Function

    Public Sub Insert(ByVal index As Integer, ByVal value As SimEvent)
        Me.InnerList.Insert(index, value)
    End Sub

    Public Sub AddRange(ByVal values() As SimEvent)
        Me.InnerList.AddRange(values)
    End Sub

    Public Sub Remove(ByVal value As SimEvent)
        Me.InnerList.Remove(value)
    End Sub
End Class


''' <summary>
''' Simulated duration.  Seasonal.
''' </summary>
''' <remarks></remarks>
Public Class SimDuration

    Private m_season As Season
    Private m_startHour As Integer
    Private m_endHour As Integer

    Public Sub New()
    End Sub

    Public Sub New(ByVal s As Season, ByVal startHour As Integer, ByVal endHour As Integer)
        Me.New()
        m_season = s
        m_startHour = startHour
        m_endHour = endHour
    End Sub

#Region "Properties"

    Public Property Season() As Season
        Get
            Return m_season
        End Get
        Set(ByVal Value As Season)
            m_season = Value
        End Set
    End Property

    Public Property StartHour() As Integer
        Get
            Return m_startHour
        End Get
        Set(ByVal Value As Integer)
            m_startHour = Value
        End Set
    End Property

    Public Property EndHour() As Integer
        Get
            Return m_endHour
        End Get
        Set(ByVal Value As Integer)
            m_endHour = Value
        End Set
    End Property

#End Region

End Class

Public Class SimDurationCollection
    Inherits CollectionBase

    Public Sub New()
    End Sub

    Default ReadOnly Property Item(ByVal index As Integer) As SimDuration
        Get
            Return CType(Me.InnerList.Item(index), SimDuration)
        End Get
    End Property

    Public Function Add(ByVal value As SimDuration) As Integer
        Return Me.InnerList.Add(value)
    End Function

    Public Sub Insert(ByVal index As Integer, ByVal value As SimDuration)
        Me.InnerList.Insert(index, value)
    End Sub

    Public Sub AddRange(ByVal values() As SimDuration)
        Me.InnerList.AddRange(values)
    End Sub

    Public Sub Remove(ByVal value As SimDuration)
        Me.InnerList.Remove(value)
    End Sub
End Class


''' <summary>
''' Peak event.
''' </summary>
''' <remarks></remarks>
Public Class PeakEvent

    Private m_date As DateTime
    Private m_durationCollection As SimDurationCollection

    Public Sub New()
        m_durationCollection = New SimDurationCollection()
    End Sub

    Public Sub New(ByVal d As DateTime)
        Me.New()
        m_date = d
    End Sub

#Region "Properties"

    Public Property Datestamp() As DateTime
        Get
            Return m_date
        End Get
        Set(ByVal Value As DateTime)
            m_date = Value
        End Set
    End Property

    Public Property Durations() As SimDurationCollection
        Get
            Return m_durationCollection
        End Get
        Set(ByVal Value As SimDurationCollection)
            m_durationCollection = Value
        End Set
    End Property

#End Region

End Class

Public Class PeakEventCollection
    Inherits CollectionBase

    Public Sub New()
    End Sub

    Default ReadOnly Property Item(ByVal index As Integer) As PeakEvent
        Get
            Return CType(Me.InnerList.Item(index), PeakEvent)
        End Get
    End Property

    Public Function Add(ByVal value As PeakEvent) As Integer
        Return Me.InnerList.Add(value)
    End Function

    Public Sub Insert(ByVal index As Integer, ByVal value As PeakEvent)
        Me.InnerList.Insert(index, value)
    End Sub

    Public Sub AddRange(ByVal values() As PeakEvent)
        Me.InnerList.AddRange(values)
    End Sub

    Public Sub Remove(ByVal value As PeakEvent)
        Me.InnerList.Remove(value)
    End Sub
End Class


''' <summary>
''' Higher level class that contains peak event information.
''' </summary>
''' <remarks></remarks>
Public Class PeakEventInfo

    Private m_peakEvents As PeakEventCollection
    Private m_useInlinePeakEvents As Boolean

    Public Sub New()
        m_peakEvents = New PeakEventCollection()
    End Sub

    Public Sub New(ByVal useInlinePeakEvents As Boolean)
        Me.New()
        m_useInlinePeakEvents = useInlinePeakEvents
    End Sub

#Region "Properties"

    Public Property PeakEvents() As PeakEventCollection
        Get
            Return m_peakEvents
        End Get
        Set(ByVal Value As PeakEventCollection)
            m_peakEvents = Value
        End Set
    End Property

    Public Property UseInlinePeakEvents() As Boolean
        Get
            Return m_useInlinePeakEvents
        End Get
        Set(ByVal Value As Boolean)
            m_useInlinePeakEvents = Value
        End Set
    End Property

#End Region

End Class