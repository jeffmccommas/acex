Imports CE.RateEngine.Enums

Public Class UseChargeCollection
	Inherits CollectionBase

	Public Sub New()
	End Sub

	Default ReadOnly Property Item(ByVal index As Integer) As UseCharge
		Get
			Return CType(Me.InnerList.Item(index), UseCharge)
		End Get
	End Property

	Public Function Add(ByVal value As UseCharge) As Integer
		Return Me.InnerList.Add(value)
	End Function

    Public Sub Insert(ByVal index As Integer, ByVal value As UseCharge)
        Me.InnerList.Insert(index, value)
    End Sub

	Public Sub AddRange(ByVal values() As UseCharge)
		Me.InnerList.AddRange(values)
	End Sub

	Public Sub Remove(ByVal value As UseCharge)
		Me.InnerList.Remove(value)
	End Sub

    ' Combine like use charges into new collection
    Public Function GetCombinedUseCharges() As UseChargeCollection
        Dim i As Integer
        Dim objTemp As UseCharge
        Dim objUseChargeRef As UseCharge
        Dim objUseChargeColl As New UseChargeCollection

        If (Me.InnerList.Count > 0) Then

            For i = 0 To Me.InnerList.Count - 1
                objTemp = CType(Me.InnerList.Item(i), UseCharge)
                If (objUseChargeColl.ExistsByParts(objTemp.BaseOrTier, objTemp.TimeOfUse, objTemp.Season, objTemp.FuelCostRecovery, objTemp.RealTimePricing)) AndAlso objTemp.FuelCostRecovery = False Then
                    objUseChargeRef = objUseChargeColl.FindFirstUseChargeRefByParts(objTemp.BaseOrTier, objTemp.TimeOfUse, objTemp.Season, objTemp.FuelCostRecovery, objTemp.RealTimePricing)
                    objUseChargeRef.ChargeValue += objTemp.ChargeValue
                Else
                    objUseChargeColl.Add(New UseCharge(objTemp.CostType, objTemp.BaseOrTier, objTemp.TimeOfUse, objTemp.Season, objTemp.PartType, objTemp.FuelCostRecovery, objTemp.FCRGroup, objTemp.RealTimePricing, objTemp.RTPGroup, objTemp.ChargeValue))
                End If
            Next

        End If

        Return (objUseChargeColl)
    End Function

	' Use Usage to find UseCharge (yes, usage to find usecharge)
	Public Function FindByUsageProperties(ByVal objUsage As Usage) As UseChargeCollection
		Dim i As Integer
		Dim objTemp As UseCharge
		Dim objUseCharge As UseCharge
		Dim objUseChargeColl As New UseChargeCollection

		If (Me.InnerList.Count > 0) Then

			For i = 0 To Me.InnerList.Count - 1
				objTemp = CType(Me.InnerList.Item(i), UseCharge)
				If objTemp.BaseOrTier = objUsage.BaseOrTier And objTemp.TimeOfUse = objUsage.TimeOfUse And objTemp.Season = objUsage.Season Then
                    objUseCharge = New UseCharge(objTemp.CostType, objTemp.BaseOrTier, objTemp.TimeOfUse, objTemp.Season, objTemp.PartType, objTemp.FuelCostRecovery, objTemp.FCRGroup, objTemp.RealTimePricing, objTemp.RTPGroup, objTemp.ChargeValue)
					objUseChargeColl.Add(objUseCharge)
					'Exit For
				End If
			Next

		End If

		Return (objUseChargeColl)
	End Function

	Public Function FindByParts(ByVal eBaseOrTier As BaseOrTier, ByVal eTimeOfUse As TimeOfUse, ByVal eSeason As Season) As UseChargeCollection
		Dim i As Integer
		Dim objTemp As UseCharge
		Dim objUseCharge As UseCharge
		Dim objUseChargeColl As New UseChargeCollection

		If (Me.InnerList.Count > 0) Then

			For i = 0 To Me.InnerList.Count - 1
				objTemp = CType(Me.InnerList.Item(i), UseCharge)
				If objTemp.BaseOrTier = eBaseOrTier And objTemp.TimeOfUse = eTimeOfUse And objTemp.Season = eSeason Then
                    objUseCharge = New UseCharge(objTemp.CostType, objTemp.BaseOrTier, objTemp.TimeOfUse, objTemp.Season, objTemp.PartType, objTemp.FuelCostRecovery, objTemp.FCRGroup, objTemp.RealTimePricing, objTemp.RTPGroup, objTemp.ChargeValue)
					objUseChargeColl.Add(objUseCharge)
					'Exit For
				End If
			Next

		End If

		Return (objUseChargeColl)
    End Function

    Public Function FindFirstUseChargeRefByParts(ByVal eBaseOrTier As BaseOrTier, ByVal eTimeOfUse As TimeOfUse, ByVal eSeason As Season) As UseCharge
        Dim i As Integer
        Dim objTemp As UseCharge
        Dim objUseCharge As UseCharge= Nothing
        Dim objUseChargeColl As New UseChargeCollection

        If (Me.InnerList.Count > 0) Then

            For i = 0 To Me.InnerList.Count - 1
                objTemp = CType(Me.InnerList.Item(i), UseCharge)
                If (objTemp.BaseOrTier = eBaseOrTier And objTemp.TimeOfUse = eTimeOfUse And objTemp.Season = eSeason) Then
                    objUseCharge = objTemp
                    Exit For
                End If
            Next

        End If

        Return (objUseCharge)
    End Function

    Public Function FindFirstUseChargeRefByParts(ByVal eBaseOrTier As BaseOrTier, ByVal eTimeOfUse As TimeOfUse, ByVal eSeason As Season, ByVal fuelCostRecovery As Boolean, ByVal realTimePricing As Boolean) As UseCharge
        Dim i As Integer
        Dim objTemp As UseCharge
        Dim objUseCharge As UseCharge= Nothing
        Dim objUseChargeColl As New UseChargeCollection

        If (Me.InnerList.Count > 0) Then

            For i = 0 To Me.InnerList.Count - 1
                objTemp = CType(Me.InnerList.Item(i), UseCharge)
                If (objTemp.BaseOrTier = eBaseOrTier And objTemp.TimeOfUse = eTimeOfUse And objTemp.Season = eSeason And objTemp.FuelCostRecovery = fuelCostRecovery And objTemp.RealTimePricing = realTimePricing) Then
                    objUseCharge = objTemp
                    Exit For
                End If
            Next

        End If

        Return (objUseCharge)
    End Function

	Public Function FindByParts(ByVal eBaseOrTier As BaseOrTier, ByVal eTimeOfUse As TimeOfUse, ByVal eSeason As Season, ByVal bFuelCostRecovery As Boolean) As UseChargeCollection
		Dim i As Integer
		Dim objTemp As UseCharge
		Dim objUseCharge As UseCharge
		Dim objUseChargeColl As New UseChargeCollection

		If (Me.InnerList.Count > 0) Then

			For i = 0 To Me.InnerList.Count - 1
				objTemp = CType(Me.InnerList.Item(i), UseCharge)
				If (objTemp.BaseOrTier = eBaseOrTier And objTemp.TimeOfUse = eTimeOfUse And objTemp.Season = eSeason And objTemp.FuelCostRecovery = bFuelCostRecovery) Then
                    objUseCharge = New UseCharge(objTemp.CostType, objTemp.BaseOrTier, objTemp.TimeOfUse, objTemp.Season, objTemp.PartType, objTemp.FuelCostRecovery, objTemp.FCRGroup, objTemp.RealTimePricing, objTemp.RTPGroup, objTemp.ChargeValue)
					objUseChargeColl.Add(objUseCharge)
					'Exit For
				End If
			Next

		End If

		Return (objUseChargeColl)
	End Function

    Public Function ExistsByParts(ByVal eBaseOrTier As BaseOrTier, ByVal eTimeOfUse As TimeOfUse, ByVal eSeason As Season) As Boolean
        Dim retval As Boolean = False
        Dim i As Integer
        Dim objTemp As UseCharge

        If (Me.InnerList.Count > 0) Then

            For i = 0 To Me.InnerList.Count - 1
                objTemp = CType(Me.InnerList.Item(i), UseCharge)
                If (objTemp.BaseOrTier = eBaseOrTier And objTemp.TimeOfUse = eTimeOfUse And objTemp.Season = eSeason) Then
                    retval = True
                    Exit For
                End If
            Next

        End If

        Return (retval)
    End Function

    Public Function ExistsByParts(ByVal eBaseOrTier As BaseOrTier, ByVal eTimeOfUse As TimeOfUse, ByVal eSeason As Season, ByVal fuelCostRecovery As Boolean, ByVal realTimePricing As Boolean) As Boolean
        Dim retval As Boolean = False
        Dim i As Integer
        Dim objTemp As UseCharge

        If (Me.InnerList.Count > 0) Then

            For i = 0 To Me.InnerList.Count - 1
                objTemp = CType(Me.InnerList.Item(i), UseCharge)
                If (objTemp.BaseOrTier = eBaseOrTier And objTemp.TimeOfUse = eTimeOfUse And objTemp.Season = eSeason And objTemp.FuelCostRecovery = fuelCostRecovery And objTemp.RealTimePricing = realTimePricing) Then
                    retval = True
                    Exit For
                End If
            Next

        End If

        Return (retval)
    End Function

    Public Function ExistsByTOU(ByVal tou As TimeOfUse) As Boolean
        Dim retval As Boolean = False
        Dim i As Integer
        Dim c As UseCharge

        If (Me.InnerList.Count > 0) Then

            For i = 0 To Me.InnerList.Count - 1
                c = CType(Me.InnerList.Item(i), UseCharge)

                If (c.TimeOfUse = tou) Then
                    retval = True
                    Exit For
                End If
            Next

        End If

        Return (retval)
    End Function

    ' Does a Tier exist with no TOU and seasonal
    Public Function ExistsAnyTierNoTOUSeasonal() As Boolean
        Dim retval As Boolean = False
        Dim i As Integer
        Dim objTemp As UseCharge

        If (Me.InnerList.Count > 0) Then

            For i = 0 To Me.InnerList.Count - 1
                objTemp = CType(Me.InnerList.Item(i), UseCharge)

                If ((objTemp.BaseOrTier = BaseOrTier.Tier1 Or _
                objTemp.BaseOrTier = BaseOrTier.Tier2 Or _
                objTemp.BaseOrTier = BaseOrTier.Tier3 Or _
                objTemp.BaseOrTier = BaseOrTier.Tier4 Or _
                objTemp.BaseOrTier = BaseOrTier.Tier5 Or _
                objTemp.BaseOrTier = BaseOrTier.Tier6) And _
                objTemp.TimeOfUse = TimeOfUse.Undefined And _
                objTemp.Season <> Season.Undefined) Then

                    retval = True
                    Exit For

                End If
            Next

        End If

        Return (retval)
    End Function

    ' Does a Tier exist with no TOU and non seasonal
    Public Function ExistsAnyTierNoTOUNonSeasonal() As Boolean
        Dim retval As Boolean = False
        Dim i As Integer
        Dim objTemp As UseCharge

        If (Me.InnerList.Count > 0) Then

            For i = 0 To Me.InnerList.Count - 1
                objTemp = CType(Me.InnerList.Item(i), UseCharge)

                If ((objTemp.BaseOrTier = BaseOrTier.Tier1 Or _
                objTemp.BaseOrTier = BaseOrTier.Tier2 Or _
                objTemp.BaseOrTier = BaseOrTier.Tier3 Or _
                objTemp.BaseOrTier = BaseOrTier.Tier4 Or _
                objTemp.BaseOrTier = BaseOrTier.Tier5 Or _
                objTemp.BaseOrTier = BaseOrTier.Tier6) And _
                objTemp.TimeOfUse = TimeOfUse.Undefined And _
                objTemp.Season = Season.Undefined) Then

                    retval = True
                    Exit For

                End If
            Next

        End If

        Return (retval)
    End Function

    ' Does a Tier exist with TOU and seasonal
    Public Function ExistsAnyTierTOUSeasonal() As Boolean
        Dim retval As Boolean = False
        Dim i As Integer
        Dim objTemp As UseCharge

        If (Me.InnerList.Count > 0) Then

            For i = 0 To Me.InnerList.Count - 1
                objTemp = CType(Me.InnerList.Item(i), UseCharge)

                If ((objTemp.BaseOrTier = BaseOrTier.Tier1 Or _
                objTemp.BaseOrTier = BaseOrTier.Tier2 Or _
                objTemp.BaseOrTier = BaseOrTier.Tier3 Or _
                objTemp.BaseOrTier = BaseOrTier.Tier4 Or _
                objTemp.BaseOrTier = BaseOrTier.Tier5 Or _
                objTemp.BaseOrTier = BaseOrTier.Tier6) And _
                objTemp.TimeOfUse <> TimeOfUse.Undefined And _
                objTemp.Season <> Season.Undefined) Then

                    retval = True
                    Exit For

                End If
            Next

        End If

        Return (retval)
    End Function

    ' Does a Tier exist with TOU and seasonal
    Public Function ExistsAnyTierTOUNonSeasonal() As Boolean
        Dim retval As Boolean = False
        Dim i As Integer
        Dim objTemp As UseCharge

        If (Me.InnerList.Count > 0) Then

            For i = 0 To Me.InnerList.Count - 1
                objTemp = CType(Me.InnerList.Item(i), UseCharge)

                If ((objTemp.BaseOrTier = BaseOrTier.Tier1 Or _
                objTemp.BaseOrTier = BaseOrTier.Tier2 Or _
                objTemp.BaseOrTier = BaseOrTier.Tier3 Or _
                objTemp.BaseOrTier = BaseOrTier.Tier4 Or _
                objTemp.BaseOrTier = BaseOrTier.Tier5 Or _
                objTemp.BaseOrTier = BaseOrTier.Tier6) And _
                objTemp.TimeOfUse <> TimeOfUse.Undefined And _
                objTemp.Season = Season.Undefined) Then

                    retval = True
                    Exit For

                End If
            Next

        End If

        Return (retval)
    End Function

    ' Does a Tier exist with any TOU
    Public Function ExistsAnyTierTOU() As Boolean
        Dim retval As Boolean = False
        Dim i As Integer
        Dim objTemp As UseCharge

        If (Me.InnerList.Count > 0) Then

            For i = 0 To Me.InnerList.Count - 1
                objTemp = CType(Me.InnerList.Item(i), UseCharge)

                If ((objTemp.BaseOrTier = BaseOrTier.Tier1 Or _
                objTemp.BaseOrTier = BaseOrTier.Tier2 Or _
                objTemp.BaseOrTier = BaseOrTier.Tier3 Or _
                objTemp.BaseOrTier = BaseOrTier.Tier4 Or _
                objTemp.BaseOrTier = BaseOrTier.Tier5 Or _
                objTemp.BaseOrTier = BaseOrTier.Tier6) And _
                objTemp.TimeOfUse <> TimeOfUse.Undefined ) Then

                    retval = True
                    Exit For

                End If
            Next

        End If

        Return (retval)
    End Function

    ' Does any Tier exist
    Public Function ExistsAnyTier() As Boolean
        Dim retval As Boolean = False
        Dim i As Integer
        Dim objTemp As UseCharge

        If (Me.InnerList.Count > 0) Then

            For i = 0 To Me.InnerList.Count - 1
                objTemp = CType(Me.InnerList.Item(i), UseCharge)

                If (objTemp.BaseOrTier = BaseOrTier.Tier1 Or _
                objTemp.BaseOrTier = BaseOrTier.Tier2 Or _
                objTemp.BaseOrTier = BaseOrTier.Tier3 Or _
                objTemp.BaseOrTier = BaseOrTier.Tier4 Or _
                objTemp.BaseOrTier = BaseOrTier.Tier5 Or _
                objTemp.BaseOrTier = BaseOrTier.Tier6) Then

                    retval = True
                    Exit For

                End If
            Next

        End If

        Return (retval)
    End Function

    ' Does seasonal tou exists, without any tiers
    Public Function ExistsSeasonalTOUAndNoTiers() As Boolean
        Dim retval As Boolean = False
        Dim i As Integer
        Dim objTemp As UseCharge

        If (Me.InnerList.Count > 0) Then

            For i = 0 To Me.InnerList.Count - 1
                objTemp = CType(Me.InnerList.Item(i), UseCharge)

                If (objTemp.BaseOrTier = BaseOrTier.Tier1 Or _
                objTemp.BaseOrTier = BaseOrTier.Tier2 Or _
                objTemp.BaseOrTier = BaseOrTier.Tier3 Or _
                objTemp.BaseOrTier = BaseOrTier.Tier4 Or _
                objTemp.BaseOrTier = BaseOrTier.Tier5 Or _
                objTemp.BaseOrTier = BaseOrTier.Tier6) Then
                    retval = False
                    Exit For
                End If

                If (objTemp.BaseOrTier = BaseOrTier.Undefined And _
                   objTemp.TimeOfUse <> TimeOfUse.Undefined And _
                   objTemp.Season <> Season.Undefined) Then
                    retval = True
                End If

            Next

        End If

        Return (retval)
    End Function

    

End Class
