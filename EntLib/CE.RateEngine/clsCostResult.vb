Imports CE.RateEngine.Enums

Public Class CostResult

	Private m_dblAvgDailyEnergyUse As Double
	Private m_dblTotalUse As Double
    Private m_dblTotalCost As Double
    Private m_dblTotalCostBeforeMinimumCostsApplied As Double
	Private m_nRateCountApplied As Integer
    Private m_nBillDays As Integer
    Private m_objCostCollection As CostCollection
    'Bug54232
    Private m_objCostCollectionWithMinCost As CostCollection
    Private m_nErrCode As ErrCode
    Private m_bCalculationsHadChildren As Boolean
    Private m_bMinimumCostsApplied As Boolean
    Private m_netMeteringType As NetMeteringType
    Private m_netMeteringResult As NetMeteringMonthlyResult
    'Bug54232
    Private m_bMinimumChargesConfigured As Boolean
    'Bug54232
    Private m_dblMinimumCharge As Double

    Public Sub New()
        m_netMeteringType = NetMeteringType.Unspecified
        m_bCalculationsHadChildren = False
        m_bMinimumCostsApplied = False
        m_netMeteringResult = Nothing
    End Sub

#Region "Properties"

    Public Property AvgDailyEnergyUse() As Double
        Get
            Return m_dblAvgDailyEnergyUse
        End Get
        Set(ByVal Value As Double)
            m_dblAvgDailyEnergyUse = Value
        End Set
    End Property

    Public Property TotalUse() As Double
        Get
            Return m_dblTotalUse
        End Get
        Set(ByVal Value As Double)
            m_dblTotalUse = Value
        End Set
    End Property

    Public Property TotalCost() As Double
        Get
            Return m_dblTotalCost
        End Get
        Set(ByVal Value As Double)
            m_dblTotalCost = Value
        End Set
    End Property

    Public Property RateCountApplied() As Integer
        Get
            Return m_nRateCountApplied
        End Get
        Set(ByVal Value As Integer)
            m_nRateCountApplied = Value
        End Set
    End Property

    Public Property BillDays() As Integer
        Get
            Return m_nBillDays
        End Get
        Set(ByVal Value As Integer)
            m_nBillDays = Value
        End Set
    End Property

    Public Property CostCollection() As CostCollection
        Get
            Return m_objCostCollection
        End Get
        Set(ByVal Value As CostCollection)
            m_objCostCollection = Value
        End Set
    End Property

    'Bug54232
    Public Property CostCollectionWithMinCost() As CostCollection
        Get
            Return m_objCostCollectionWithMinCost
        End Get
        Set(ByVal Value As CostCollection)
            m_objCostCollectionWithMinCost = Value
        End Set
    End Property

    Public Property CalculationsHadChildren() As Boolean
        Get
            Return m_bCalculationsHadChildren
        End Get
        Set(ByVal Value As Boolean)
            m_bCalculationsHadChildren = Value
        End Set
    End Property

    Public Property MinimumCostsApplied() As Boolean
        Get
            Return m_bMinimumCostsApplied
        End Get
        Set(ByVal Value As Boolean)
            m_bMinimumCostsApplied = Value
        End Set
    End Property

    Public Property ErrorCode() As ErrCode
        Get
            Return m_nErrCode
        End Get
        Set(ByVal Value As ErrCode)
            m_nErrCode = Value
        End Set
    End Property

    Public Property NetMeteringType() As NetMeteringType
        Get
            Return m_netMeteringType
        End Get
        Set(ByVal Value As NetMeteringType)
            m_netMeteringType = Value
        End Set
    End Property

    Public ReadOnly Property IsNetMetering() As Boolean
        Get
            If (NetMeteringType = Enums.NetMeteringType.Unspecified) Then
                Return (False)
            Else
                Return (True)
            End If
        End Get
    End Property

    Public Property NetMeteringResult() As NetMeteringMonthlyResult
        Get
            Return m_netMeteringResult
        End Get
        Set(ByVal Value As NetMeteringMonthlyResult)
            m_netMeteringResult = Value
        End Set
    End Property

    ' 14.09 bug 53743 
    Public Property TotalCostBeforeMinimumCostsApplied() As Double
        Get
            Return m_dblTotalCostBeforeMinimumCostsApplied
        End Get
        Set(ByVal Value As Double)
            m_dblTotalCostBeforeMinimumCostsApplied = Value
        End Set
    End Property

    'Bug54232
    Public Property MinimumChargesConfigured() As Boolean
        Get
            Return m_bMinimumChargesConfigured
        End Get
        Set(ByVal value As Boolean)
            m_bMinimumChargesConfigured = value
        End Set
    End Property

    'Bug54232
    Public Property MinimumCharge() As Double
        Get
            Return m_dblMinimumCharge
        End Get
        Set(ByVal value As Double)
            m_dblMinimumCharge = value
        End Set
    End Property


#End Region

End Class
