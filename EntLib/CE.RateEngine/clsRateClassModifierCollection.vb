﻿Imports CE.RateEngine.Enums
Imports System.Xml
Imports System.Collections.Generic

Public Class RateClassModifierCollection
    Inherits CollectionBase

    Const ksnValuePair As String = "~"
    Const ksnDivider As String = "|"
    Public Const ksnRateClassHash As String = "#"
    Public Const ksnRC As String = "rc"
    Public Const ksnBC As String = "bc"
    Public Const ksnTUF As String = "tuf"
    Public Const ksnIRS As String = "irs"
    Public Const ksnDBTF As String = "dbtf"
    Public Const ksnIDRE As String = "idre"
    Public Const ksnDRRC As String = "drrc"
    Public Const ksnDRT As String = "drt"
    Public Const ksnNMR As String = "nmr"
    Public Const ksnTCF As String = "tcf"
    Public Const ksnDATF As String = "datf"
    Public Const ksnBAA As String = "baa"
    Public Const ksnSSWAP As String = "sswap"
    Public Const ksnTYPE As String = "type"
    Public Const ksnRPID As String = "rpid"
    Public Const ksnICRP As String = "icrp"
    Public Const ksnADBD As String = "adbd"
    Public Const ksnADBDF As String = "adbdf"
    Public Const ksnADBDR As String = "adbdrule"
    Public Const ksnAVGU As String = "avgu"
    Public Const ksnACFU As String = "acfu"
    Public Const ksnDAYU As String = "dayu"
    Public Const ksnRDP As String = "rdp"
    Public Const ksnCRKW As String = "crkw"
    Public Const ksnCRKWC As String = "crkwc"
    Public Const ksnCRKWHC As String = "crkwhc"
    Public Const ksnNCDM As String = "ncdm"
    Public Const ksnNCKWC As String = "nckwc"
    Public Const ksnNCDF As String = "ncdf"
    '--- CR50560 BEGIN -------------------------------------------------------------------------------------------
    Public Const ksnCPKWC As String = "cpkwc"
    Public Const ksnCPCF As String = "cpcf"
    Public Const ksnCPTIME As String = "cptime"
    '--- CR50560 END ---------------------------------------------------------------------------------------------
    Public Const ksnEXCLCT As String = "exclct"
    Public Const ksnDEXCLCT As String = "dexclct"
    Public Const ksnClass1 As String = "CLASS1"
    Public Const ksnClass2 As String = "CLASS2"
    Public Const ksnClass3 As String = "CLASS3"
    Public Const ksnClass4 As String = "CLASS4"
    Public Const ksnClass5 As String = "CLASS5"
    Public Const ksnUndefined As String = "UNDEFINED"
    Public Const ksnRenew As String = "renew"
    Const ksnDuplicateModifierError As String = "Duplicate Rate Class Modifier: {0}"
    Const ksnMissingRateError As String = "Rate Class is missing"
    Const ksnModifierValueError As String = "Invalid Modifier Value: {0}"

    Public Enum RateClassModifierCopyFilter
        Unspecified = 0
        Include = 1
        Exclude = 2
        All = 3
    End Enum

    Public Sub New()
    End Sub

    Default ReadOnly Property Item(ByVal index As Integer) As RateClassModifier
        Get
            Return CType(Me.InnerList.Item(index), RateClassModifier)
        End Get
    End Property

    Public Function Add(ByVal value As RateClassModifier) As Integer
        Return Me.InnerList.Add(value)
    End Function

    Public Sub Insert(ByVal index As Integer, ByVal value As RateClassModifier)
        Me.InnerList.Insert(index, value)
    End Sub

    Public Sub AddRange(ByVal values() As RateClassModifier)
        Me.InnerList.AddRange(values)
    End Sub

    Public Sub Remove(ByVal value As RateClassModifier)
        Me.InnerList.Remove(value)
    End Sub

    Public Function Exists(ByVal modifierType As RateClassModifierType) As Boolean
        Dim found As Boolean = False
        Dim i As Integer
        Dim temp As RateClassModifier

        If (Me.InnerList.Count > 0) Then

            For i = 0 To Me.InnerList.Count - 1
                temp = CType(Me.InnerList.Item(i), RateClassModifier)
                If (temp.ModifierType = modifierType) Then
                    found = True
                    Exit For
                End If
            Next

        End If

        Return (found)
    End Function

    Public Function Find(ByVal modifierType As RateClassModifierType) As RateClassModifier
        Dim i As Integer
        Dim temp As RateClassModifier = Nothing

        ' if more than one item in collection of the same type, first occurance is returned only
        If (Me.InnerList.Count > 0) Then

            For i = 0 To Me.InnerList.Count - 1
                temp = CType(Me.InnerList.Item(i), RateClassModifier)
                If (temp.ModifierType = modifierType) Then
                    Exit For
                End If
            Next

        End If

        Return (temp)
    End Function

    ''' <summary>
    ''' Get Specific Modifier - IDRE
    ''' </summary>
    Public Function GetModifierIDRE() As Boolean
        Dim x As Boolean = True    'default when not present
        Dim modifier As RateClassModifier = Find(RateClassModifierType.IsDemandResponseEligible)

        If (Not modifier Is Nothing AndAlso Not String.IsNullOrEmpty(modifier.Value)) Then
            Dim result As Boolean = Boolean.TryParse(modifier.Value, x)
            If (Not result) Then
                x = True
            End If
        End If

        Return (x)
    End Function


    ''' <summary>
    ''' Get Specific Modifier - CRKW
    ''' </summary>
    Public Function GetModifierCRKW() As Double?
        Dim x As Double? = Nothing   'default when not present
        Dim xx As Double = 0.0
        Dim modifier As RateClassModifier = Find(RateClassModifierType.CapResDemandValue)

        If (Not modifier Is Nothing AndAlso Not String.IsNullOrEmpty(modifier.Value)) Then
            Dim result As Boolean = Double.TryParse(modifier.Value, xx)
            If (Not result) Then
                x = Nothing
            Else
                x = xx
            End If
        End If

        Return (x)
    End Function

    ''' <summary>
    ''' Get Specific Modifier - CRKWC
    ''' </summary>
    Public Function GetModifierCRKWC() As Double
        Dim x As Double = 0.0    'default when not present
        Dim modifier As RateClassModifier = Find(RateClassModifierType.CapReskWCharge)

        If (Not modifier Is Nothing AndAlso Not String.IsNullOrEmpty(modifier.Value)) Then
            Dim result As Boolean = Double.TryParse(modifier.Value, x)
            If (Not result) Then
                x = 0.0
            End If
        End If

        Return (x)
    End Function

    ''' <summary>
    ''' Get Specific Modifier - CRKWHC
    ''' </summary>
    Public Function GetModifierCRKWHC() As Double
        Dim x As Double = 0.0    'default when not present
        Dim modifier As RateClassModifier = Find(RateClassModifierType.CapResExcesskWhCharge)

        If (Not modifier Is Nothing AndAlso Not String.IsNullOrEmpty(modifier.Value)) Then
            Dim result As Boolean = Double.TryParse(modifier.Value, x)
            If (Not result) Then
                x = 0.0
            End If
        End If

        Return (x)
    End Function

    ''' <summary>
    ''' Get Specific Modifier - NCDM
    ''' </summary>
    Public Function GetModifierNCDM() As Double
        Dim x As Double = 0.0    'default when not present
        Dim modifier As RateClassModifier = Find(RateClassModifierType.NonCoDemandAnnMax)

        If (Not modifier Is Nothing AndAlso Not String.IsNullOrEmpty(modifier.Value)) Then
            Dim result As Boolean = Double.TryParse(modifier.Value, x)
            If (Not result) Then
                x = 0.0
            End If
        End If

        Return (x)
    End Function

    ''' <summary>
    ''' Get Specific Modifier - NCKWC
    ''' </summary>
    Public Function GetModifierNCKWC() As Double
        Dim x As Double = 0.0    'default when not present
        Dim modifier As RateClassModifier = Find(RateClassModifierType.NonCokWCharge)

        If (Not modifier Is Nothing AndAlso Not String.IsNullOrEmpty(modifier.Value)) Then
            Dim result As Boolean = Double.TryParse(modifier.Value, x)
            If (Not result) Then
                x = 0.0
            End If
        End If

        Return (x)
    End Function

    ''' <summary>
    ''' Get Specific Modifier - NCDF
    ''' </summary>
    Public Function GetModifierNCDF() As Double
        Dim x As Double = 0.0    'default when not present
        Dim modifier As RateClassModifier = Find(RateClassModifierType.NonCoMultFactor)

        If (Not modifier Is Nothing AndAlso Not String.IsNullOrEmpty(modifier.Value)) Then
            Dim result As Boolean = Double.TryParse(modifier.Value, x)
            If (Not result) Then
                x = 0.0
            End If
        End If

        Return (x)
    End Function

    '--- CR50560 BEGIN -------------------------------------------------------------------------------------------

    ''' <summary>
    ''' Get Specific Modifier - CPKWC
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function GetModifierCPKWC() As Double
        Dim x As Double = 0.0    'default when not present
        Dim modifier As RateClassModifier = Find(RateClassModifierType.CokWCharge)

        If (Not modifier Is Nothing AndAlso Not String.IsNullOrEmpty(modifier.Value)) Then
            Dim result As Boolean = Double.TryParse(modifier.Value, x)
            If (Not result) Then
                x = 0.0
            End If
        End If

        Return (x)
    End Function

    ''' <summary>
    ''' Get Specific Modifier - CPCF
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function GetModifierCPCF() As Double
        Dim x As Double = 0.0    'default when not present
        Dim modifier As RateClassModifier = Find(RateClassModifierType.CoMultFactor)

        If (Not modifier Is Nothing AndAlso Not String.IsNullOrEmpty(modifier.Value)) Then
            Dim result As Boolean = Double.TryParse(modifier.Value, x)
            If (Not result) Then
                x = 0.0
            End If
        End If

        Return (x)
    End Function

    ''' <summary>
    ''' Get Specific Modifier - CPTIME
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function GetModifierCPTIME() As DateTime
        Dim x As DateTime = DateTime.MinValue    'default when not present
        Dim modifier As RateClassModifier = Find(RateClassModifierType.CoDateTime)

        If (Not modifier Is Nothing AndAlso Not String.IsNullOrEmpty(modifier.Value)) Then
            Dim result As Boolean = DateTime.TryParse(modifier.Value, x)
            If (Not result) Then
                x = DateTime.MinValue
            End If
        End If

        Return (x)
    End Function

    '--- CR50560 END----------------------------------------------------------------------------------------------

    ''' <summary>
    ''' Get Specific Modifier - RDP
    ''' </summary>
    Public Function GetModifierRDP() As Integer
        Dim x As Integer = 3  'default when not present
        Dim modifier As RateClassModifier = Find(RateClassModifierType.RoundingDecimalPlaces)

        If (Not modifier Is Nothing AndAlso Not String.IsNullOrEmpty(modifier.Value)) Then
            Dim result As Boolean = Integer.TryParse(modifier.Value, x)
            If (Not result) Then
                x = 3
            End If
        End If

        Return (x)
    End Function

    ''' <summary>
    ''' Get Specific Modifier - AVGU
    ''' </summary>
    Public Function GetModifierAVGU() As Double
        Dim x As Double = 0.0

        Dim modifier As RateClassModifier = Find(RateClassModifierType.AverageUsage)

        If (Not modifier Is Nothing AndAlso Not String.IsNullOrEmpty(modifier.Value)) Then
            Dim result As Boolean = Double.TryParse(modifier.Value, x)
            If (Not result) Then
                x = 0.0
            End If
        End If

        Return (x)
    End Function

    ''' <summary>
    ''' Get Specific Modifier - ACFU
    ''' </summary>
    Public Function GetModifierACFU() As Double
        Dim x As Double = 1.0       'default when not present
        Dim modifier As RateClassModifier = Find(RateClassModifierType.ActualConsumptionFactor)

        If (Not modifier Is Nothing AndAlso Not String.IsNullOrEmpty(modifier.Value)) Then
            Dim result As Boolean = Double.TryParse(modifier.Value, x)
            If (Not result) Then
                x = 1.0
            End If
        End If

        Return (x)
    End Function

    ''' <summary>
    ''' Get Specific Modifier - DAYU
    ''' </summary>
    Public Function GetModifierDAYU() As Integer
        Dim x As Integer = 1    'default when not present
        Dim modifier As RateClassModifier = Find(RateClassModifierType.DayUsage)

        If (Not modifier Is Nothing AndAlso Not String.IsNullOrEmpty(modifier.Value)) Then
            Dim result As Boolean = Integer.TryParse(modifier.Value, x)
            If (Not result) Then
                x = 1
            End If
        End If

        Return (x)
    End Function

    ''' <summary>
    ''' Get the NetMeteringType - NMR
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function GetModifierNMR() As NetMeteringType
        Dim x As NetMeteringType = NetMeteringType.Unspecified
        Dim modifier As RateClassModifier = Find(RateClassModifierType.NetMeteredRule)

        If (Not modifier Is Nothing AndAlso Not String.IsNullOrEmpty(modifier.Value)) Then
            Dim result As NetMeteringType

            If ([Enum].TryParse(modifier.Value, True, result)) Then
                x = result
            Else
                x = NetMeteringType.Unspecified
            End If

        End If

        Return (x)
    End Function


    ''' <summary>
    ''' return a list of rate class modifiers by modifer type - CR 20298, 20299, 20302 Oct 2011
    ''' </summary>
    ''' <param name="modifierType"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function GetRateClassModifierList(modifierType As RateClassModifierType) As RateClassModifierCollection
        Dim i As Integer
        Dim objRateClassModifiers As New RateClassModifierCollection
        Dim temp As RateClassModifier = Nothing

        ' if more than one item in collection of the same type, first occurance is returned only
        If (Me.InnerList.Count > 0) Then

            For i = 0 To Me.InnerList.Count - 1
                temp = CType(Me.InnerList.Item(i), RateClassModifier)
                If (temp.ModifierType = modifierType) Then
                    objRateClassModifiers.Add(temp)
                End If
            Next

        End If

        Return (objRateClassModifiers)
    End Function

    ''' <summary>
    ''' checks if rate class starts with "#" - CR 20298, 20299, 20302 Oct 2011
    ''' </summary>
    ''' <param name="sRateClass"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function DoesRateClassContainModifiers(sRateClass As String) As Boolean
        Dim dbValid As Boolean = False
        If Not sRateClass Is Nothing AndAlso sRateClass.Length > 0 Then
            If sRateClass.StartsWith(ksnRateClassHash) Then
                dbValid = True
            End If
        End If
        Return (dbValid)
    End Function

    ''' <summary>
    ''' checks for any invalid/unknown attributes - used by download process to validate bill data - CR 20298, 20299, 20302 Oct 2011 
    ''' </summary>
    ''' <param name="sRateClass"></param>
    '''    #rc~23G|tcf~0.0077|idre~true|ddrc~class1
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function ValidateRateClassModifierCollection(sRateClass As String) As String
        Dim bValid As Boolean = False
        Dim aryAttributes() As String
        Dim aryPair() As String
        Dim bRateClassExists, bbc, birs, bidre, bdrrc, bdrt, bnmr, bbaa, bsswap, bexcct, bdexcct, btype, brpid, bicrp, badbd, badbdf, badbdrule, bavgu, bacfu, bdayu, brenew As Boolean
        Dim bcrkw, bcrkwc, bcrkwhc, bncdm, bnckwc, bncdf, brdp, cpkwc, cpcf, cptime As Boolean
        Dim bDuplicateModifier As Boolean = False
        Dim sbError As Text.StringBuilder = New Text.StringBuilder
        Dim sError As String
        Dim dblValue As Double
        Dim bValue As Boolean
        Dim i As Integer

        Try
            If sRateClass = String.Empty Then
                sbError.Append(ksnMissingRateError)
            Else
                sRateClass = sRateClass.Remove(0, 1)

                aryAttributes = sRateClass.Split(CChar(ksnDivider))

                If (aryAttributes.Length > 0) Then
                    For i = 0 To aryAttributes.GetUpperBound(0)
                        aryPair = aryAttributes(i).Split(CChar(ksnValuePair))
                        If (aryPair.Length > 1) Then
                            Select Case aryPair(0)
                                Case ksnRC
                                    sError = IsModifierDuplicated(ksnRC, bRateClassExists)
                                    If Not bRateClassExists Then
                                        sbError.Append(ksnMissingRateError)
                                        sbError.AppendLine()
                                    End If
                                    If Not sError = String.Empty Then
                                        sbError.Append(sError)
                                        sbError.AppendLine()
                                    End If
                                Case ksnBC
                                    sError = IsModifierDuplicated(ksnBC, bbc)
                                    If Not sError = String.Empty Then
                                        sbError.Append(sError)
                                        sbError.AppendLine()
                                    End If
                                Case ksnIRS
                                    sError = IsModifierDuplicated(ksnIRS, birs)
                                    If Not sError = String.Empty Then
                                        sbError.Append(sError)
                                        sbError.AppendLine()
                                    End If
                                    bValid = Boolean.TryParse(aryPair(1).Trim(), bValue)
                                    If Not bValid Then
                                        sbError.AppendFormat(ksnModifierValueError, aryAttributes(i))
                                        sbError.AppendLine()
                                    End If
                                Case ksnIDRE
                                    sError = IsModifierDuplicated(ksnIDRE, bidre)
                                    If Not sError = String.Empty Then
                                        sbError.Append(sError)
                                        sbError.AppendLine()
                                    End If
                                    bValid = Boolean.TryParse(aryPair(1).Trim(), bValue)
                                    If Not bValid Then
                                        sbError.AppendFormat(ksnModifierValueError, aryAttributes(i))
                                        sbError.AppendLine()
                                    End If
                                Case ksnRenew
                                    sError = IsModifierDuplicated(ksnRenew, brenew)
                                    If Not sError = String.Empty Then
                                        sbError.Append(sError)
                                        sbError.AppendLine()
                                    End If
                                    bValid = Double.TryParse(aryPair(1).Trim(), dblValue)
                                    If Not bValid Then
                                        sbError.AppendFormat(ksnModifierValueError, aryAttributes(i))
                                        sbError.AppendLine()
                                    End If
                                Case ksnDRRC
                                    sError = IsModifierDuplicated(ksnDRRC, bdrrc)
                                    If Not sError = String.Empty Then
                                        sbError.Append(sError)
                                        sbError.AppendLine()
                                    End If
                                    Select Case aryPair(1).Trim().ToUpper
                                        Case ksnClass1, ksnClass2, ksnClass3, ksnClass4, ksnClass5, ksnUndefined
                                            bValid = True
                                        Case Else
                                            sbError.AppendFormat(ksnModifierValueError, aryAttributes(i))
                                            sbError.AppendLine()
                                    End Select
                                Case ksnDRT
                                    sError = IsModifierDuplicated(ksnDRT, bdrt)
                                    If Not sError = String.Empty Then
                                        sbError.Append(sError)
                                        sbError.AppendLine()
                                    End If
                                Case ksnNMR
                                    sError = IsModifierDuplicated(ksnNMR, bnmr)
                                    If Not sError = String.Empty Then
                                        sbError.Append(sError)
                                        sbError.AppendLine()
                                    End If
                                Case ksnTCF, ksnDATF, ksnTUF, ksnDBTF
                                    bValid = Double.TryParse(aryPair(1).Trim(), dblValue)
                                    If Not bValid Then
                                        sbError.AppendFormat(ksnModifierValueError, aryAttributes(i))
                                        sbError.AppendLine()
                                    End If
                                Case ksnBAA
                                    sError = IsModifierDuplicated(ksnBAA, bbaa)
                                    If Not sError = String.Empty Then
                                        sbError.Append(sError)
                                        sbError.AppendLine()
                                    End If
                                Case ksnSSWAP
                                    sError = IsModifierDuplicated(ksnSSWAP, bsswap)
                                    If Not sError = String.Empty Then
                                        sbError.Append(sError)
                                        sbError.AppendLine()
                                    End If
                                Case ksnEXCLCT
                                    sError = IsModifierDuplicated(ksnEXCLCT, bexcct)
                                    If Not sError = String.Empty Then
                                        sbError.Append(sError)
                                        sbError.AppendLine()
                                    End If
                                Case ksnDEXCLCT
                                    sError = IsModifierDuplicated(ksnDEXCLCT, bdexcct)
                                    If Not sError = String.Empty Then
                                        sbError.Append(sError)
                                        sbError.AppendLine()
                                    End If
                                Case ksnTYPE
                                    sError = IsModifierDuplicated(ksnTYPE, btype)
                                    If Not sError = String.Empty Then
                                        sbError.Append(sError)
                                        sbError.AppendLine()
                                    End If
                                Case ksnRPID
                                    sError = IsModifierDuplicated(ksnRPID, brpid)
                                    If Not sError = String.Empty Then
                                        sbError.Append(sError)
                                        sbError.AppendLine()
                                    End If
                                Case ksnICRP
                                    sError = IsModifierDuplicated(ksnICRP, bicrp)
                                    If Not sError = String.Empty Then
                                        sbError.Append(sError)
                                        sbError.AppendLine()
                                    End If
                                Case ksnADBD
                                    sError = IsModifierDuplicated(ksnADBD, badbd)
                                    If Not sError = String.Empty Then
                                        sbError.Append(sError)
                                        sbError.AppendLine()
                                    End If
                                Case ksnADBDF
                                    sError = IsModifierDuplicated(ksnADBDF, badbdf)
                                    If Not sError = String.Empty Then
                                        sbError.Append(sError)
                                        sbError.AppendLine()
                                    End If
                                    bValid = Double.TryParse(aryPair(1).Trim(), dblValue)
                                    If Not bValid Then
                                        sbError.AppendFormat(ksnModifierValueError, aryAttributes(i))
                                        sbError.AppendLine()
                                    End If
                                Case ksnADBDR
                                    sError = IsModifierDuplicated(ksnADBDF, badbdrule)
                                    If Not sError = String.Empty Then
                                        sbError.Append(sError)
                                        sbError.AppendLine()
                                    End If
                                Case ksnAVGU
                                    sError = IsModifierDuplicated(ksnAVGU, bavgu)
                                    If Not sError = String.Empty Then
                                        sbError.Append(sError)
                                        sbError.AppendLine()
                                    End If
                                Case ksnACFU
                                    sError = IsModifierDuplicated(ksnACFU, bacfu)
                                    If Not sError = String.Empty Then
                                        sbError.Append(sError)
                                        sbError.AppendLine()
                                    End If
                                Case ksnDAYU
                                    sError = IsModifierDuplicated(ksnDAYU, bdayu)
                                    If Not sError = String.Empty Then
                                        sbError.Append(sError)
                                        sbError.AppendLine()
                                    End If
                                Case ksnCRKW
                                    sError = IsModifierDuplicated(ksnCRKW, bcrkw)
                                    If Not sError = String.Empty Then
                                        sbError.Append(sError)
                                        sbError.AppendLine()
                                    End If
                                Case ksnCRKWC
                                    sError = IsModifierDuplicated(ksnCRKWC, bcrkwc)
                                    If Not sError = String.Empty Then
                                        sbError.Append(sError)
                                        sbError.AppendLine()
                                    End If
                                Case ksnCRKWHC
                                    sError = IsModifierDuplicated(ksnCRKWHC, bcrkwhc)
                                    If Not sError = String.Empty Then
                                        sbError.Append(sError)
                                        sbError.AppendLine()
                                    End If
                                Case ksnNCDM
                                    sError = IsModifierDuplicated(ksnNCDM, bncdm)
                                    If Not sError = String.Empty Then
                                        sbError.Append(sError)
                                        sbError.AppendLine()
                                    End If
                                Case ksnNCKWC
                                    sError = IsModifierDuplicated(ksnNCKWC, bnckwc)
                                    If Not sError = String.Empty Then
                                        sbError.Append(sError)
                                        sbError.AppendLine()
                                    End If
                                Case ksnNCDF
                                    sError = IsModifierDuplicated(ksnNCDF, bncdf)
                                    If Not sError = String.Empty Then
                                        sbError.Append(sError)
                                        sbError.AppendLine()
                                    End If
                                    '--- CR50560 BEGIN -------------------------------------------------------------------------------------------
                                Case ksnCPKWC
                                    sError = IsModifierDuplicated(ksnCPKWC, cpkwc)
                                    If Not sError = String.Empty Then
                                        sbError.Append(sError)
                                        sbError.AppendLine()
                                    End If
                                Case ksnCPCF
                                    sError = IsModifierDuplicated(ksnCPCF, cpcf)
                                    If Not sError = String.Empty Then
                                        sbError.Append(sError)
                                        sbError.AppendLine()
                                    End If
                                Case ksnCPTIME
                                    sError = IsModifierDuplicated(ksnCPTIME, cptime)
                                    If Not sError = String.Empty Then
                                        sbError.Append(sError)
                                        sbError.AppendLine()
                                    End If
                                    '--- CR50560 END ---------------------------------------------------------------------------------------------
                                Case ksnRDP
                                    sError = IsModifierDuplicated(ksnRDP, brdp)
                                    If Not sError = String.Empty Then
                                        sbError.Append(sError)
                                        sbError.AppendLine()
                                    End If
                                Case Else
                                    sbError.AppendFormat(ksnModifierValueError, aryAttributes(i))
                                    sbError.AppendLine()
                            End Select
                        Else
                            sbError.AppendFormat(ksnModifierValueError, aryAttributes(i))
                            sbError.AppendLine()
                        End If
                    Next
                End If
            End If

        Catch ex As Exception
            Throw
        End Try
        Return (sbError.ToString)
    End Function

    ''' <summary>
    ''' Check if the modifier is duplicated or not - CR 20298, 20299, 20302
    ''' </summary>
    ''' <param name="sModifier"></param>
    ''' <param name="bModifier"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Shared Function IsModifierDuplicated(sModifier As String, ByRef bModifier As Boolean) As String
        Dim bError As String = String.Empty
        If Not bModifier Then
            bModifier = True
        Else
            bError = String.Format(ksnDuplicateModifierError, sModifier)
        End If

        Return bError
    End Function

    ''' <summary>
    ''' create # rate class string - CR 20298, 20299, 20302 Oct 2011
    ''' <RateClassAttributes key="rc" value="DR1" />
    ''' <RateClassAttributes key="tcf" value="0.007" />....
    ''' used by rate engine test tool - test calculate cost
    ''' </summary>
    ''' <param name="objRateClassAttributeList">
    ''' </param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function ConvertRateClassAttributestoRateClassString(objRateClassAttributeList As XmlNodeList) As String
        Dim sbRateClass As New System.Text.StringBuilder
        Dim sAttribute As String
        Dim sValue As String
        Dim objXMLNode As XmlNode
        If objRateClassAttributeList.Count > 0 Then
            For Each objXMLNode In objRateClassAttributeList
                If sbRateClass.Length = 0 Then
                    sbRateClass.Append(ksnRateClassHash)
                Else
                    sbRateClass.Append(ksnDivider)
                End If
                sAttribute = objXMLNode.Attributes("key").Value
                sValue = objXMLNode.Attributes("value").Value

                sbRateClass.Append(sAttribute)
                sbRateClass.Append(ksnValuePair)
                sbRateClass.Append(sValue)
            Next
        End If
        Return (sbRateClass.ToString)
    End Function

    Public Shared Function ConvertStringListToRateClassString(pairs As List(Of String)) As String
        Dim sbRateClass As New System.Text.StringBuilder
        Dim attr As String
        Dim val As String
        Dim aryPair() As String

        If (pairs.Count > 0) Then
            For Each p As String In pairs
                If sbRateClass.Length = 0 Then
                    sbRateClass.Append(ksnRateClassHash)
                Else
                    sbRateClass.Append(ksnDivider)
                End If
                aryPair = p.Split(":"c)
                attr = aryPair(0)
                val = aryPair(1)

                sbRateClass.Append(attr)
                sbRateClass.Append(ksnValuePair)
                sbRateClass.Append(val)
            Next
        End If

        Return (sbRateClass.ToString)
    End Function

    ''' <summary>
    ''' Copy source rate class with rate class modifiers to a new rate class string. 
    ''' Options:
    ''' 1. Copy all rate class modifiers.
    ''' Set eRateClassModifierCopyFilter = All.
    ''' 2. Copy only included rate class modifiers.
    ''' Set eRateClassModifierCopyFilter = Include and add included rate class modifiers in objRateClassModifierKeyList.
    ''' 3. Copy only rates class modifiers not excluded.
    ''' Set eRateClassModifierCopyFilter = Exclude and add excluded rate class modifiers in objRateClassModifierKeyList.
    ''' </summary>
    ''' <param name="sSourceRateClass"></param>
    ''' <param name="sTargetRateClass"></param>
    ''' <param name="objIncludeExcludeRateClassModifierKeyList"></param>
    ''' <param name="eRateClassModifierCopyFilter"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function CopyRateClassWithModifiers(ByVal sSourceRateClass As String,
                                                      ByVal sTargetRateClass As String,
                                                      ByVal objIncludeExcludeRateClassModifierKeyList As List(Of String),
                                                      ByVal eRateClassModifierCopyFilter As RateClassModifierCopyFilter) As String
        Dim sResult As String = String.Empty
        Dim objAllRateClassModifierKeyList As List(Of String)
        Dim sRateClassModifierValue As String
        Dim bInclude As Boolean = False

        Try

            'Validate input parameter: Source Rate Class
            If (String.IsNullOrEmpty(sSourceRateClass) = True) Then
                Throw New ArgumentException(String.Format("Source rate class is required."))
            End If

            'Validate input parameter: Target Rate Class
            If (String.IsNullOrEmpty(sTargetRateClass) = True) Then
                Throw New ArgumentException(String.Format("Target rate class is required."))
            End If

            'Source rate class does not have attributes.
            If (sSourceRateClass.StartsWith(ksnRateClassHash) = False) Then
                'Nothing to do. Return target rate class unmodified.
                sResult = sTargetRateClass
                Return sResult
            End If

            'Validate input parameter: Rate Class Modifier Key list.
            If ((eRateClassModifierCopyFilter = RateClassModifierCopyFilter.Exclude) Or
               (eRateClassModifierCopyFilter = RateClassModifierCopyFilter.Include)) And
               (objIncludeExcludeRateClassModifierKeyList Is Nothing) Then
                Throw New ArgumentException(String.Format("Rate class modifier key list is required."))
            End If

            objAllRateClassModifierKeyList = RateClassModifierCollection.GetAllRateClassModifierKeyList()

            For Each sRateClassModifierKey As String In objAllRateClassModifierKeyList

                sRateClassModifierValue = RateClassModifierCollection.GetModifierValueFromRateClassModifierString(sSourceRateClass, sRateClassModifierKey)

                'Rate class modifier is not valid for copying.
                If (sRateClassModifierValue Is Nothing Or
                    sRateClassModifierValue = String.Empty Or
                    sRateClassModifierValue.Length <= 0) Then
                    'Skip rate class modifier.
                    Continue For
                End If

                Select Case eRateClassModifierCopyFilter

                    Case RateClassModifierCopyFilter.All
                        bInclude = True

                    Case RateClassModifierCopyFilter.Include
                        If (objIncludeExcludeRateClassModifierKeyList.Contains(sRateClassModifierKey) = True) Then
                            bInclude = True
                        Else
                            bInclude = False
                        End If

                    Case RateClassModifierCopyFilter.Exclude
                        If (objIncludeExcludeRateClassModifierKeyList.Contains(sRateClassModifierKey) = True) Then
                            bInclude = False
                        Else
                            bInclude = True
                        End If

                    Case Else

                        Throw New ArgumentOutOfRangeException(String.Format("Unexpected Rate Class Modifier Copy Filter. (RateClassModifierCopyFilter: {0})",
                                                                            eRateClassModifierCopyFilter))

                End Select

                'Modifier should be included or modifier is "Rate Class" modifier.
                If (bInclude = True) Or
                    (sRateClassModifierKey = RateClassModifierCollection.ksnRC) Then

                    'First time adding rate class modifier to result rate class.
                    If sResult.Trim = String.Empty Then
                        sResult = ksnRateClassHash + sRateClassModifierKey + ksnValuePair + sTargetRateClass
                    Else
                        'Rate class modifier not already applied to result rate class.
                        If Not sResult.Contains(sRateClassModifierKey + ksnValuePair) Then
                            'Append rate class modifier to result rate class.
                            sResult = sResult + ksnDivider + sRateClassModifierKey + ksnValuePair + sRateClassModifierValue
                        End If
                    End If

                End If

            Next

        Catch
            Throw
        End Try

        Return sResult

    End Function

    ''' <summary>
    ''' Retrieve all rate class modifier key list.
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function GetAllRateClassModifierKeyList() As List(Of String)

        Dim objResult As List(Of String)

        Try
            objResult = New List(Of String)

            objResult.Add(RateClassModifierCollection.ksnRC)

            objResult.Add(RateClassModifierCollection.ksnBC)
            objResult.Add(RateClassModifierCollection.ksnDATF)
            objResult.Add(RateClassModifierCollection.ksnDBTF)
            objResult.Add(RateClassModifierCollection.ksnDRRC)
            objResult.Add(RateClassModifierCollection.ksnDRT)
            objResult.Add(RateClassModifierCollection.ksnEXCLCT)
            objResult.Add(RateClassModifierCollection.ksnDEXCLCT)
            objResult.Add(RateClassModifierCollection.ksnIDRE)
            objResult.Add(RateClassModifierCollection.ksnIRS)
            objResult.Add(RateClassModifierCollection.ksnNMR)
            objResult.Add(RateClassModifierCollection.ksnTCF)
            objResult.Add(RateClassModifierCollection.ksnTUF)
            objResult.Add(RateClassModifierCollection.ksnRenew)


        Catch
            Throw
        End Try

        Return objResult

    End Function

    ''' <summary>
    ''' Get modifier values from rate class with "#" - CR 20298, 20299, 20302 Nov 2011
    ''' </summary>
    ''' <param name="sRateClass"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function GetModifierValueFromRateClassModifierString(sRateClass As String, sModifier As String) As String
        Dim sValue As String = String.Empty
        Dim aryAttributes() As String
        Dim aryPair() As String
        Dim i As Integer

        If Not sRateClass Is Nothing AndAlso sRateClass.Length > 0 Then
            If sRateClass.StartsWith(ksnRateClassHash) Then
                sRateClass = sRateClass.Remove(0, 1)
            End If
            aryAttributes = sRateClass.Split("|"c)

            If aryAttributes.Length > 0 Then
                For i = 0 To aryAttributes.GetUpperBound(0)
                    If aryAttributes(i).StartsWith(sModifier + "~"c) Then
                        aryPair = aryAttributes(i).Split("~"c)
                        If aryPair.Length > 1 Then
                            sValue = aryPair(1)
                            Exit For
                        End If
                    End If
                Next
            End If
        End If

        Return sValue
    End Function

End Class
