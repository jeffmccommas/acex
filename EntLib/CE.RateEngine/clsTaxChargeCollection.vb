Imports CE.RateEngine.Enums

Public Class TaxChargeCollection
	Inherits CollectionBase

	Public Sub New()
	End Sub

	Default ReadOnly Property Item(ByVal index As Integer) As TaxCharge
		Get
			Return CType(Me.InnerList.Item(index), TaxCharge)
		End Get
	End Property

	Public Function Add(ByVal value As TaxCharge) As Integer
		Return Me.InnerList.Add(value)
	End Function

	Public Sub Insert(ByVal index As Integer, ByVal value As TaxCharge)
		Me.InnerList.Insert(index, value)
	End Sub

	Public Sub AddRange(ByVal values() As TaxCharge)
		Me.InnerList.AddRange(values)
	End Sub

	Public Sub Remove(ByVal value As TaxCharge)
		Me.InnerList.Remove(value)
	End Sub

	Public Function FindByCostType(ByVal ct As CostType) As TaxCharge
		Dim i As Integer
		Dim objTaxCharge As TaxCharge= Nothing

		If (Me.InnerList.Count > 0) Then

			For i = 0 To Me.InnerList.Count - 1
				If CType(Me.InnerList.Item(i), TaxCharge).CostType = ct Then
					objTaxCharge = CType(Me.InnerList.Item(i), TaxCharge)
					Exit For
				End If
			Next

		End If

		Return (objTaxCharge)
	End Function

End Class
