﻿Imports CE.RateEngine.Enums
Imports System.Collections.Generic

Partial Public Class RateEngine


#Region "GetUsageDeterminants - Public"

    Public Function GetUsageDeterminants(ByVal nRateCompanyID As Integer, ByVal objReadings As ReadingCollection, ByVal objDemandkVAReadings As ReadingCollection, ByVal sRateClass As String, ByVal dtStartDate As DateTime, ByVal dtEndDate As DateTime, ByVal dtDSTEndDate As DateTime) As UsageCollection
        Return (GetUsageDeterminants(nRateCompanyID, Nothing, objReadings, objDemandkVAReadings, sRateClass, dtStartDate, dtEndDate, dtDSTEndDate, False))
    End Function

    Public Function GetUsageDeterminants(ByVal nRateCompanyID As Integer, ByVal objReadings As ReadingCollection, ByVal sRateClass As String, ByVal dtStartDate As DateTime, ByVal dtEndDate As DateTime, ByVal dtDSTEndDate As DateTime) As UsageCollection
        Return (GetUsageDeterminants(nRateCompanyID, Nothing, objReadings, Nothing, sRateClass, dtStartDate, dtEndDate, dtDSTEndDate, False))
    End Function

    Public Function GetUsageDeterminants(ByVal nRateCompanyID As Integer, ByVal objDefinitions As DetailedRateDefinitionCollection, ByVal objReadings As ReadingCollection, ByVal objDemandkVAReadings As ReadingCollection, ByVal sRateClass As String, ByVal dtStartDate As DateTime, ByVal dtEndDate As DateTime, ByVal dtDSTEndDate As DateTime) As UsageCollection
        Return (GetUsageDeterminants(nRateCompanyID, objDefinitions, objReadings, objDemandkVAReadings, sRateClass, dtStartDate, dtEndDate, dtDSTEndDate, False))
    End Function

    Public Function GetUsageDeterminants(ByVal nRateCompanyID As Integer, ByVal objDefinitions As DetailedRateDefinitionCollection, ByVal objReadings As ReadingCollection, ByVal objDemandkVAReadings As ReadingCollection, ByVal sRateClass As String, ByVal dtStartDate As DateTime, ByVal dtEndDate As DateTime, ByVal dtDSTEndDate As DateTime, ByVal forCostToDate As Boolean) As UsageCollection
        Dim objUsages As New UsageCollection
        Dim objDetailedDef As DetailedRateDefinition = Nothing
        Dim objUseCharges As UseChargeCollection
        Dim objDemandTOU As TimeOfUse
        Dim dblQuantity As Double
        Dim maxDetail As MaxReadingDetail
        Dim eReadingInterval As ReadingInterval
        Dim dblMaxMultiplier As Double
        Dim bDemandDeterminantNeeded As Boolean = False
        Dim bDemandBillDeterminantNeeded As Boolean = False
        Dim eBillDemandType As BilledDemandType = BilledDemandType.CurrentBillMaxDemand
        Dim nDemand As Double = 0

        If (Not objReadings Is Nothing) Then
            If (objReadings.Count > 0) Then

                ' some callers will pass this, older callers will not, and thus this needs to be instantiated correctly
                If (objDefinitions Is Nothing) Then
                    objDefinitions = GetDetailedRateInformation(nRateCompanyID, sRateClass, dtStartDate, dtEndDate)
                End If

                If (objDefinitions.Count > 0) Then

                    objDetailedDef = objDefinitions(0)
                    objUseCharges = objDetailedDef.UseCharges
                    eBillDemandType = objDetailedDef.BilledDemandType

                    ' Always create a TotalService Use Usage
                    dblQuantity = objReadings.Sum()
                    objUsages.Add(New Usage(objDetailedDef.LoadType, BaseOrTier.TotalServiceUse, TimeOfUse.Undefined, Season.Undefined, dblQuantity))

                    For Each objC As UseCharge In objUseCharges
                        ' If a Use Charge with given attributes does NOT already exist, then add to the usage collection being populated;
                        ' However, only add if Quantity is non-zero, **and do not add seasonal TotalServiceUse for callers**
                        ' Skip demand, it's handled below
                        If Not (objC.BaseOrTier = BaseOrTier.Demand Or objC.BaseOrTier = BaseOrTier.DemandBill) Then

                            ' Check here if a Use Charge with given attributes does NOT already exist; often charges are broken into parts, 
                            ' and this insures that we don't create extra items just because use charges are broken into parts
                            If (Not objUsages.Exists(objC.BaseOrTier, objC.TimeOfUse, objC.Season)) Then

                                dblQuantity = objReadings.Sum(objC.BaseOrTier, objC.TimeOfUse, objC.Season)

                                If (dblQuantity <> 0.0 And (Not (objC.BaseOrTier = BaseOrTier.TotalServiceUse And objC.Season <> Season.Undefined))) Then
                                    objUsages.Add(New Usage(objDetailedDef.LoadType, objC.BaseOrTier, objC.TimeOfUse, objC.Season, dblQuantity))
                                End If

                            End If

                        End If
                    Next

                    ' PTR
                    ' If the caller is *cost-to-date*, and PTR is in-play, and there are no Critical Peak charges authored, but there is critical peak binned for usage... 
                    ' then get the Total Critical Peak quantity from the readings, re-assigned to its original TOU, and then add this to the first matching original TOU usage;  
                    ' this re-assigns the usage back to where it can again be used to calculate cost.  This is so that we do not lose usage when Peak-time-rebate is in-play, but
                    ' we also do not want to undo the readings binning, since the binned readings must be used by PTR logic for baseline and rebate logic.
                    If (forCostToDate AndAlso objDetailedDef.IsRTPRate AndAlso objDetailedDef.RTPStream = RTPStream.PeakTimeRebateOnly) Then

                        If (Not objUseCharges.ExistsByTOU(TimeOfUse.CriticalPeak)) Then

                            Dim originalUsages As UsageCollection = objReadings.GetOriginalTOUModifiedUsagesFromReadings(TimeOfUse.CriticalPeak)
                            If (Not originalUsages Is Nothing) Then
                                For Each originalUsage As Usage In originalUsages
                                    Dim u As Usage = objUsages.FindFirstUsageWithTOUAndSeason(originalUsage.TimeOfUse, originalUsage.Season)
                                    If (Not u Is Nothing) Then
                                        u.Quantity = u.Quantity + originalUsage.Quantity
                                    End If
                                Next

                            End If

                        End If

                    End If

                    ' CPP - IsAdder
                    ' If rate definition is a CPP Adder, and there are Critical Peak Charges, then create usage determinants 
                    ' from readings that are critical peak, but create them with their original TOU.  
                    ' Then add the quantities to existing usage determinants object.  This insures original quantity of TOU
                    ' is preserved, and the Critical Peak is an additional usage determinant.
                    If (forCostToDate AndAlso objDetailedDef.IsRTPRate AndAlso objDetailedDef.IsCPEventUsageAdder AndAlso objDetailedDef.RTPStream = RTPStream.CriticalPeakOnly) Then

                        If (objUseCharges.ExistsByTOU(TimeOfUse.CriticalPeak)) Then

                            Dim originalUsages As UsageCollection = objReadings.GetOriginalTOUModifiedUsagesFromReadings(TimeOfUse.CriticalPeak)
                            If (Not originalUsages Is Nothing) Then
                                For Each originalUsage As Usage In originalUsages
                                    ' this is a pointer to matching usage
                                    Dim u As Usage = objUsages.FindFirstUsageWithTOUAndSeason(originalUsage.TimeOfUse, originalUsage.Season)
                                    If (Not u Is Nothing) Then
                                        u.Quantity = u.Quantity + originalUsage.Quantity
                                    End If
                                Next

                            End If

                        End If

                    End If


                    ' CPP - Capacity Reserve 
                    ' Detect any readings that are indicated to have some capacity reserve and create Critical Peak usage for those; total only, not seasonal at this time
                    ' And, if not adder, need to reduce any usage determinants matching capacity reserve intervals
                    If (forCostToDate AndAlso objDetailedDef.IsRTPRate AndAlso objDetailedDef.IsCapacityReserve AndAlso objDetailedDef.RTPStream = RTPStream.CriticalPeakOnly) Then

                        Dim crUsages As UsageCollection = objReadings.GetCapacityReserveUsageFromReadings()
                        If (Not crUsages Is Nothing) Then

                            ' add the Critical peak Usage Determinant for Capacity Reserve
                            Dim t As Double = crUsages.GetTotalForCapacityReserve()
                            objUsages.Combine(BaseOrTier.Undefined, TimeOfUse.CriticalPeak, Season.Undefined, t)

                            'modify usage determinants that have quantity too high
                            If (Not objDetailedDef.IsCPEventUsageAdder) Then
                                For Each cru As Usage In crUsages
                                    objUsages.Combine(cru.BaseOrTier, cru.TimeOfUse, cru.Season, -1.0 * cru.Quantity)
                                Next
                            End If

                        End If

                    End If



                    ' begin demand checking
                    Select Case eBillDemandType
                        Case BilledDemandType.CurrentBillOnPeakDemand
                            objDemandTOU = TimeOfUse.OnPeak
                        Case BilledDemandType.CurrentBillOffPeakDemand
                            objDemandTOU = TimeOfUse.OffPeak
                        Case Else
                            'default to max reading
                            objDemandTOU = TimeOfUse.Undefined
                    End Select

                    'if this rate class has demand,
                    'add any demand usages required
                    If (objDetailedDef.IsDemand) Then

                        eReadingInterval = objReadings.DetermineIntervalOfReadings()

                        ' set default DST end date, first sunday of the nov of the reading collection yr
                        If dtDSTEndDate = DateTime.MinValue Or dtDSTEndDate.Year <> objReadings(0).Datestamp.Year Then
                            dtDSTEndDate = SetDefaultDSTEndDate(objReadings(0).Datestamp)
                        End If

                        ' remove DST end date from estimated demand algorithm for all interval
                        Dim nDSTEndDateStart, nDSTEndDateEnd As Integer
                        nDSTEndDateStart = objReadings.FindIndexOfReadingWithDate(dtDSTEndDate.AddHours(1))
                        nDSTEndDateEnd = objReadings.FindIndexOfReadingWithDate(dtDSTEndDate.AddHours(3))

                        ' the reading for 3am will not be excluded
                        If Not nDSTEndDateEnd = -1 AndAlso Not nDSTEndDateStart = -1 Then
                            For j As Integer = nDSTEndDateStart To nDSTEndDateEnd
                                objReadings(j).Quantity = 0
                            Next
                        End If

                        If Not objDemandkVAReadings Is Nothing Then
                            nDSTEndDateStart = -1
                            nDSTEndDateEnd = -1
                            nDSTEndDateStart = objDemandkVAReadings.FindIndexOfReadingWithDate(dtDSTEndDate.AddHours(1))
                            nDSTEndDateEnd = objDemandkVAReadings.FindIndexOfReadingWithDate(dtDSTEndDate.AddHours(3))
                            If Not nDSTEndDateEnd = -1 AndAlso Not nDSTEndDateStart = -1 Then
                                For j As Integer = nDSTEndDateStart To nDSTEndDateEnd
                                    objReadings(j).Quantity = 0
                                Next
                            End If
                        End If
                        If (eReadingInterval = ReadingInterval.Hourly) Then
                            dblMaxMultiplier = 1.0
                        ElseIf (eReadingInterval = ReadingInterval.FifteenMinute) Then
                            dblMaxMultiplier = 4.0
                        ElseIf (eReadingInterval = ReadingInterval.HalfHourly) Then
                            dblMaxMultiplier = 2.0
                        ElseIf (eReadingInterval = ReadingInterval.FiveMinute) Then

                            'max multiplier for 5-mins
                            'new 5-mins estimated demand algorithm 
                            Dim i As Integer

                            ' the reading will be in chronological order, and reading will be modified backward, so the average will be not affected
                            For i = objReadings.Count - 1 To 0 Step -1
                                If (i = 0) Or i = 1 Then
                                    objReadings(i).Quantity = 0
                                Else
                                    objReadings(i).Quantity = ((objReadings(i).Quantity * 12) + (objReadings(i - 1).Quantity * 12) + (objReadings(i - 2).Quantity * 12)) / 3
                                End If
                            Next

                            ' 5-min estimated demand algorithm for kVA demand
                            If (Not objDemandkVAReadings Is Nothing) Then
                                i = 0
                                For i = objDemandkVAReadings.Count - 1 To 0 Step -1
                                    If (i = 0) Or i = 1 Then
                                        objDemandkVAReadings(i).Quantity = 0
                                    Else
                                        objDemandkVAReadings(i).Quantity = ((objDemandkVAReadings(i).Quantity * 12) + (objDemandkVAReadings(i - 1).Quantity * 12) + (objDemandkVAReadings(i - 2).Quantity * 12)) / 3
                                    End If
                                Next
                            End If

                            dblMaxMultiplier = 1.0
                        Else
                            dblMaxMultiplier = 1.0
                        End If

                        For Each objC As UseCharge In objUseCharges
                            If objC.BaseOrTier = BaseOrTier.Demand Or objC.BaseOrTier = BaseOrTier.DemandBill Then
                                If (Not objUsages.Exists(objC.BaseOrTier, objC.TimeOfUse, objC.Season)) Then

                                    'get the max reading from demand kVA reading collection when the rate class has demand bill and bill demand type is maxDemandkVA and demand kVA reading collection is not empty
                                    If objC.BaseOrTier = BaseOrTier.DemandBill AndAlso eBillDemandType = BilledDemandType.CurrentBillMaxDemandkVA AndAlso (Not objDemandkVAReadings Is Nothing) Then
                                        maxDetail = objDemandkVAReadings.GetMaxReadingDetail(objC.TimeOfUse, objC.Season)
                                        dblQuantity = maxDetail.Quantity * dblMaxMultiplier
                                        If (dblQuantity > 0) Then
                                            objUsages.Add(New Usage(objDetailedDef.LoadType, objC.BaseOrTier, objC.TimeOfUse, objC.Season, dblQuantity, maxDetail.Datestamp))
                                        End If

                                    Else
                                        If (objDetailedDef.ProrateType = ProrateType.UseLatest Or objDetailedDef.ProrateType = ProrateType.UsePrevious Or (objDetailedDef.ProrateType = ProrateType.SplitBillDays And objDetailedDef.DemandProrateType = DemandProrateType.SplitBillDaysMaxDemand)) Then
                                            maxDetail = objReadings.GetMaxReadingDetail(objC.TimeOfUse, objC.Season)
                                            dblQuantity = maxDetail.Quantity * dblMaxMultiplier
                                            If (dblQuantity > 0) Then
                                                objUsages.Add(New Usage(objDetailedDef.LoadType, objC.BaseOrTier, objC.TimeOfUse, objC.Season, dblQuantity, maxDetail.Datestamp))
                                            End If
                                        Else
                                            ' this means that DemandProrateType.SplitBillDaysPeriodDemand is applicable
                                            Dim demandList As System.Collections.Generic.List(Of MaxReadingDetail) = objReadings.GetMaxReadingDetailByGroupedReadings(objC.TimeOfUse, objC.Season)
                                            If (demandList.Count > 0) Then
                                                dblQuantity = demandList(0).Quantity
                                                dblQuantity = dblQuantity * dblMaxMultiplier
                                                If (dblQuantity > 0) Then
                                                    Dim u As Usage = New Usage(objDetailedDef.LoadType, objC.BaseOrTier, objC.TimeOfUse, objC.Season, dblQuantity, demandList(0).Datestamp)
                                                    If (demandList.Count > 1) Then
                                                        u.VirtualQuantity = demandList(1).Quantity * dblMaxMultiplier
                                                    End If
                                                    objUsages.Add(u)
                                                End If
                                            End If
                                        End If

                                    End If


                                End If
                            End If
                        Next

                        ' Special check for demand service charges if the rate class has demand service charges
                        ' and there wasn't already a demand usage added above, we want to add a usage to the 
                        ' collection so that the service charge will be calc'd when calling calculate cost
                        If (Not objDetailedDef.ServiceCharges Is Nothing) Then

                            bDemandDeterminantNeeded = objDetailedDef.ServiceCharges.CalcTypeForSteppedDemandExists()
                            bDemandBillDeterminantNeeded = objDetailedDef.ServiceCharges.CalcTypeForSteppedDemandBillExists()

                            'Non-coincident demand may be needed if a charge that uses it exists. Get max of eligible readings.
                            'Get seasonal charges if seasonal is authored for NCD by travering all NCD charges.
                            If (objDetailedDef.ServiceCharges.CalcTypeExists(ChargeCalcType.NCDemandCharge)) Then
                                Dim ncdm As Double? = Nothing
                                ncdm = GetNonCoincidentDemandValue(sRateClass)
                                If (Not ncdm Is Nothing AndAlso ncdm <> 0.0) Then

                                    Dim ncdCharges As ServiceChargeCollection = objDetailedDef.ServiceCharges.GetServiceChargesNCD()
                                    For Each ncd As ServiceCharge In ncdCharges
                                        Dim max As MaxReadingDetail = objReadings.GetMaxReadingDetailForNCD(objDemandTOU, ncd.Season)
                                        max.Quantity = max.Quantity * dblMaxMultiplier
                                        objUsages.Add(New Usage(objDetailedDef.LoadType, BaseOrTier.DemandBill2, TimeOfUse.Undefined, ncd.Season, max.Quantity, max.Datestamp))
                                    Next

                                End If
                            End If
                        End If

                        '--- CR50560 BEGIN -------------------------------------------------------------------------------------------

                        If (Not objDetailedDef.ServiceCharges Is Nothing) Then

                            bDemandDeterminantNeeded = objDetailedDef.ServiceCharges.CalcTypeForSteppedDemandExists()
                            bDemandBillDeterminantNeeded = objDetailedDef.ServiceCharges.CalcTypeForSteppedDemandBillExists()

                            'Coincident demand may be needed if a charge that uses it exists.
                            If (objDetailedDef.ServiceCharges.CalcTypeExists(ChargeCalcType.CPDemandCharge)) Then

                                Dim cpcf As Double? = Nothing
                                Dim cptime As DateTime? = Nothing

                                cpcf = GetCoincidentPeakMultiplicationFactor(sRateClass)
                                cptime = GetCoincidentPeakDateTime(sRateClass)

                                'Check for CPTIME rate helper.
                                If (Not cptime Is Nothing AndAlso cptime <> DateTime.MinValue AndAlso cptime <> DateTime.MaxValue) Then

                                    Dim previousReadingInterval As DateTime = objReadings.RoundToNearestPreviousInterval(CDate(cptime), eReadingInterval)
                                    If (previousReadingInterval < dtStartDate Or previousReadingInterval > dtEndDate) Then

                                        Throw New Exception(String.Format(ksException_Message_RateHelper_cptime_DateTimeOutsideOfBillPeriod,
                                                                          cptime, dtStartDate, dtEndDate))
                                    End If
                                    Dim reading As Reading = objReadings.GetReadForDateTime(previousReadingInterval)
                                    Dim cpkW As Double?
                                    If (Not reading Is Nothing) Then
                                        cpkW = reading.Quantity * dblMaxMultiplier
                                    Else
                                        cpkW = Nothing
                                    End If
                                    If (Not cpkW Is Nothing AndAlso cpkW <> 0.0) Then

                                        'Calculate DemandBill3 from kW reading specified by rate helper cptime.
                                        Dim cpkWValue As Double
                                        cpkWValue = CDbl(cpkW)

                                        Dim cdCharges As ServiceChargeCollection = objDetailedDef.ServiceCharges.GetServiceChargesCD()

                                        For Each cd As ServiceCharge In cdCharges
                                            objUsages.Add(New Usage(objDetailedDef.LoadType, BaseOrTier.DemandBill3, TimeOfUse.Undefined, cd.Season, cpkWValue, DateTime.Now))
                                        Next

                                    Else
                                        Throw New Exception(String.Format(ksException_Message_RateHelper_cptime_MissingReadingOrBadValue,
                                                                          cptime.ToString()))
                                    End If

                                    'Check for CPCF rate helper.
                                ElseIf (Not cpcf Is Nothing AndAlso cpcf >= 0 AndAlso cpcf <= 1) Then
                                    'Calculate DemandBill3 from maximum kW reading.

                                    'Get seasonal charges if seasonal is authored for CD by travering all CD charges.
                                    Dim cdCharges As ServiceChargeCollection = objDetailedDef.ServiceCharges.GetServiceChargesCD()

                                    For Each cd As ServiceCharge In cdCharges
                                        maxDetail = objReadings.GetMaxReadingDetail(objDemandTOU, cd.Season)
                                        Dim quantityKW As Double = maxDetail.Quantity * dblMaxMultiplier
                                        Dim demandBill3Value As Double = CDbl(cpcf) * quantityKW
                                        objUsages.Add(New Usage(objDetailedDef.LoadType, BaseOrTier.DemandBill3, TimeOfUse.Undefined, cd.Season, demandBill3Value, maxDetail.Datestamp))
                                    Next


                                Else
                                    Throw New Exception(String.Format(ksException_Message_RateHelper_CoincidenceChangeRequires_cpcf_Or_cptime,
                                                                      cpcf, cptime.ToString()))
                                End If

                            End If

                        End If

                        '--- CR50560 END -------------------------------------------------------------------------------------------

                        If (bDemandBillDeterminantNeeded And Not objUsages.Exists(BaseOrTier.DemandBill)) Then
                            Dim max As MaxReadingDetail = objReadings.GetMaxReadingDetail(objDemandTOU, Season.Undefined)
                            max.Quantity = max.Quantity * dblMaxMultiplier
                            objUsages.Add(New Usage(objDetailedDef.LoadType, BaseOrTier.DemandBill, TimeOfUse.Undefined, Season.Undefined, max.Quantity, max.Datestamp))
                        End If

                        If (bDemandDeterminantNeeded And Not objUsages.Exists(BaseOrTier.Demand, TimeOfUse.Undefined, Season.Undefined)) Then
                            Dim max As MaxReadingDetail = objReadings.GetMaxReadingDetail(objDemandTOU, Season.Undefined)
                            max.Quantity = max.Quantity * dblMaxMultiplier
                            objUsages.Add(New Usage(objDetailedDef.LoadType, BaseOrTier.Demand, TimeOfUse.Undefined, Season.Undefined, max.Quantity, max.Datestamp))
                        End If

                        ' if SEASONAL demand prorate setting dictates, then set all seasonal demand charges to max
                        If (objDetailedDef.SeasonalProrateType = SeasonalProrateType.SplitBillDays AndAlso objDetailedDef.SeasonalDemandProrateType = SeasonalDemandProrateType.SplitBillDaysMaxDemand) Then
                            ' if usages indicate a crossing of seasons
                            If (objUsages.GetSeasonCount() > 1) Then
                                objUsages.SetSeasonalDemandToOverallMaximumDemand()
                            End If
                        End If


                    End If

                End If

                ' round only critical peak usage determinants when CPP was in-play; use rdp rate helper if available
                If (objDetailedDef.IsRTPRate AndAlso objDetailedDef.RTPStream = RTPStream.CriticalPeakOnly) Then
                    Dim rdp As Integer = GetRoundingDecimalPlacesValue(sRateClass)
                    objUsages.Round(TimeOfUse.CriticalPeak, rdp)
                End If

            End If

        End If


        ' A Usage collection is always returned; it could indeed be empty, but it will exist

        Return (objUsages)
    End Function

    Public Function GetComplexUsageDeterminants(ByVal nRateCompanyID As Integer, ByVal objDefinitions As DetailedRateDefinitionCollection, ByVal readings As ReadingCollection, ByVal sRateClass As String, ByVal dtStartDate As DateTime, ByVal dtEndDate As DateTime, ByVal dtDSTEndDate As DateTime, ByVal forCostToDate As Boolean) As UsageCollection
        Dim usages As New UsageCollection()
        Dim objDetailedDef As DetailedRateDefinition
        Dim objUseCharges As UseChargeCollection
        Dim i As Integer = 0
        Dim lastBaseOrTier As BaseOrTier
        Dim lastTimeOfUse As TimeOfUse
        Dim lastSeason As Season
        Dim demandUsages As UsageCollection


        If (Not readings Is Nothing) Then
            If (readings.Count > 0) Then

                ' some callers will pass this, older callers will not, and thus this needs to be instantiated correctly
                If (objDefinitions Is Nothing) Then
                    objDefinitions = GetDetailedRateInformation(nRateCompanyID, sRateClass, dtStartDate, dtEndDate)
                End If

                If (objDefinitions.Count > 0) Then

                    objDetailedDef = objDefinitions(0)
                    objUseCharges = objDetailedDef.UseCharges

                    For i = 0 To (readings.Count - 1)

                        For Each readingDetail As ReadingDetail In readings(i).DetailCollection


                            If (Not readingDetail Is Nothing) Then


                                lastBaseOrTier = BaseOrTier.Undefined
                                lastTimeOfUse = TimeOfUse.Undefined
                                lastSeason = Season.Undefined

                                ' generate usage determinants from each reading.  
                                ' The price details have duplicate entries for FCR and RTP, which are shipped when detected
                                For Each rpd As ReadingPriceDetail In readingDetail.PriceDetails

                                    If (lastBaseOrTier <> rpd.BaseOrTier Or lastTimeOfUse <> rpd.TimeOfUse Or lastSeason <> rpd.Season) Then
                                        usages.Combine(rpd.BaseOrTier, rpd.TimeOfUse, rpd.Season, readingDetail.Quantity)
                                    End If

                                    lastBaseOrTier = rpd.BaseOrTier
                                    lastTimeOfUse = rpd.TimeOfUse
                                    lastSeason = rpd.Season
                                Next

                            End If
                        Next
                        ' detect any readings that are indicated to be capacity reserve and create Critical Peak usage for those; total only, not seasonal at this time
                        If (readings(i).IsCapacityReserve) Then
                            usages.Combine(BaseOrTier.Undefined, TimeOfUse.CriticalPeak, Season.Undefined, readings(i).CapacityReserveQuantity)
                        End If

                    Next

                    'add demand usage determinants if applicable
                    If (objDetailedDef.IsDemand) Then
                        demandUsages = GetDemandUsageDeterminants(nRateCompanyID, objDefinitions, readings, sRateClass, dtStartDate, dtEndDate, dtDSTEndDate)
                        usages.Combine(demandUsages)
                    End If

                    ' NOTE: If the caller is *cost-to-date*, and PTR is in-play, there is nothing special needed to re-assign Crit Peak;  the usage ReadingPriceDetails 
                    ' already detected the "original" usage per reading, and the logic above included the original usage with the usage determinants.

                    ' NOTE: If CPP Adder is in-play, there is nothing more to do here.  The hard work was done in enhanced binning.

                    ' round only critical peak usage determinants when CPP was in-play; use rdp rate helper if available
                    If (objDetailedDef.IsRTPRate AndAlso objDetailedDef.RTPStream = RTPStream.CriticalPeakOnly) Then
                        Dim rdp As Integer = GetRoundingDecimalPlacesValue(sRateClass)
                        usages.Round(TimeOfUse.CriticalPeak, rdp)
                    End If

                End If

            End If
        End If

        Return (usages)
    End Function


    Public Function GetDemandUsageDeterminants(ByVal nRateCompanyID As Integer, ByVal objReadings As ReadingCollection, ByVal sRateClass As String, ByVal dtStartDate As DateTime, ByVal dtEndDate As DateTime, ByVal dtDSTEndDate As DateTime) As UsageCollection
        Return (GetDemandUsageDeterminants(nRateCompanyID, Nothing, objReadings, sRateClass, dtStartDate, dtEndDate, dtDSTEndDate))
    End Function

    Public Function GetDemandUsageDeterminants(ByVal nRateCompanyID As Integer, ByVal objDefinitions As DetailedRateDefinitionCollection, ByVal objReadings As ReadingCollection, ByVal sRateClass As String, ByVal dtStartDate As DateTime, ByVal dtEndDate As DateTime, ByVal dtDSTEndDate As DateTime) As UsageCollection
        Dim objUsages As New UsageCollection
        Dim objDetailedDef As DetailedRateDefinition
        Dim objUseCharges As UseChargeCollection
        Dim objDemandTOU As TimeOfUse
        Dim dblQuantity As Double
        Dim maxDetail As MaxReadingDetail
        Dim eReadingInterval As ReadingInterval
        Dim dblMaxMultiplier As Double
        Dim bDemandDeterminantNeeded As Boolean = False
        Dim bDemandBillDeterminantNeeded As Boolean = False
        Dim eBillDemandType As BilledDemandType = BilledDemandType.CurrentBillMaxDemand
        Dim nDemand As Double = 0

        If (Not objReadings Is Nothing) Then
            If (objReadings.Count > 0) Then

                ' some callers will pass this, older callers will not, and thus this needs to be instantiated correctly
                If (objDefinitions Is Nothing) Then
                    objDefinitions = GetDetailedRateInformation(nRateCompanyID, sRateClass, dtStartDate, dtEndDate)
                End If

                If (objDefinitions(0).IsDemand) Then
                    eBillDemandType = objDefinitions(0).BilledDemandType

                    Select Case eBillDemandType
                        Case BilledDemandType.CurrentBillOnPeakDemand
                            objDemandTOU = TimeOfUse.OnPeak
                        Case BilledDemandType.CurrentBillOffPeakDemand
                            objDemandTOU = TimeOfUse.OffPeak
                        Case Else
                            'default to max reading
                            objDemandTOU = TimeOfUse.Undefined
                    End Select

                    eReadingInterval = objReadings.DetermineIntervalOfReadings()
                    If dtDSTEndDate = DateTime.MinValue Or dtDSTEndDate.Year <> objReadings(0).Datestamp.Year Then
                        dtDSTEndDate = SetDefaultDSTEndDate(objReadings(0).Datestamp)
                    End If

                    Dim nDSTEndDateStart, nDSTEndDateEnd As Integer
                    nDSTEndDateStart = objReadings.FindIndexOfReadingWithDate(dtDSTEndDate.AddHours(1))
                    nDSTEndDateEnd = objReadings.FindIndexOfReadingWithDate(dtDSTEndDate.AddHours(3))

                    ' the reading for 3am will not be excluded
                    If Not nDSTEndDateEnd = -1 AndAlso Not nDSTEndDateStart = -1 Then
                        For j As Integer = nDSTEndDateStart To nDSTEndDateEnd
                            objReadings(j).Quantity = 0
                        Next
                    End If

                    If (eReadingInterval = ReadingInterval.Hourly) Then
                        dblMaxMultiplier = 1.0
                    ElseIf (eReadingInterval = ReadingInterval.FifteenMinute) Then
                        dblMaxMultiplier = 4.0
                    ElseIf (eReadingInterval = ReadingInterval.HalfHourly) Then
                        dblMaxMultiplier = 2.0
                    ElseIf (eReadingInterval = ReadingInterval.FiveMinute) Then
                        dblMaxMultiplier = 12.0
                        Dim i As Integer
                        ' the reading will be in chronological order, and reading will be modified backward, so the average will be not affected
                        For i = objReadings.Count - 1 To 0 Step -1
                            If (i = 0) Or i = 1 Then
                                objReadings(i).Quantity = 0
                            Else
                                objReadings(i).Quantity = ((objReadings(i).Quantity * 12) + (objReadings(i - 1).Quantity * 12) + (objReadings(i - 2).Quantity * 12)) / 3
                            End If
                        Next
                        dblMaxMultiplier = 1.0
                    Else
                        dblMaxMultiplier = 1.0
                    End If

                    If (objDefinitions.Count > 0) Then

                        objDetailedDef = objDefinitions(0)
                        objUseCharges = objDetailedDef.UseCharges
                        eBillDemandType = objDetailedDef.BilledDemandType

                        For Each objC As UseCharge In objUseCharges
                            If objC.BaseOrTier = BaseOrTier.Demand Or objC.BaseOrTier = BaseOrTier.DemandBill Then

                                If (Not objUsages.Exists(objC.BaseOrTier, objC.TimeOfUse, objC.Season)) Then

                                    If (objDetailedDef.ProrateType = ProrateType.UseLatest Or objDetailedDef.ProrateType = ProrateType.UsePrevious Or (objDetailedDef.ProrateType = ProrateType.SplitBillDays And objDetailedDef.DemandProrateType = DemandProrateType.SplitBillDaysMaxDemand)) Then
                                        maxDetail = objReadings.GetMaxReadingDetail(objC.TimeOfUse, objC.Season)
                                        dblQuantity = maxDetail.Quantity * dblMaxMultiplier
                                        If (dblQuantity > 0) Then
                                            objUsages.Add(New Usage(objDetailedDef.LoadType, objC.BaseOrTier, objC.TimeOfUse, objC.Season, dblQuantity, maxDetail.Datestamp))
                                        End If
                                    Else
                                        ' this means that DemandProrateType.SplitBillDaysPeriodDemand is applicable
                                        Dim demandList As System.Collections.Generic.List(Of MaxReadingDetail) = objReadings.GetMaxReadingDetailByGroupedReadings(objC.TimeOfUse, objC.Season)
                                        If (demandList.Count > 0) Then
                                            dblQuantity = demandList(0).Quantity
                                            dblQuantity = dblQuantity * dblMaxMultiplier
                                            If (dblQuantity > 0) Then
                                                Dim u As Usage = New Usage(objDetailedDef.LoadType, objC.BaseOrTier, objC.TimeOfUse, objC.Season, dblQuantity, demandList(0).Datestamp)
                                                If (demandList.Count > 1) Then
                                                    u.VirtualQuantity = demandList(1).Quantity * dblMaxMultiplier
                                                End If
                                                objUsages.Add(u)
                                            End If
                                        End If
                                    End If

                                End If

                            End If
                        Next


                        If (Not objDetailedDef.ServiceCharges Is Nothing) Then

                            bDemandDeterminantNeeded = objDetailedDef.ServiceCharges.CalcTypeForSteppedDemandExists()
                            bDemandBillDeterminantNeeded = objDetailedDef.ServiceCharges.CalcTypeForSteppedDemandBillExists()

                            'Non-coincident demand may be needed if a charge that uses it exists. Get max of eligible readings.
                            'Get seasonal charges if seasonal is authored for NCD by travering all NCD charges.
                            If (objDetailedDef.ServiceCharges.CalcTypeExists(ChargeCalcType.NCDemandCharge)) Then
                                Dim ncdm As Double? = Nothing
                                ncdm = GetNonCoincidentDemandValue(sRateClass)
                                If (Not ncdm Is Nothing AndAlso ncdm <> 0.0) Then

                                    Dim ncdCharges As ServiceChargeCollection = objDetailedDef.ServiceCharges.GetServiceChargesNCD()
                                    For Each ncd As ServiceCharge In ncdCharges
                                        Dim max As MaxReadingDetail = objReadings.GetMaxReadingDetailForNCD(objDemandTOU, ncd.Season)
                                        max.Quantity = max.Quantity * dblMaxMultiplier
                                        objUsages.Add(New Usage(objDetailedDef.LoadType, BaseOrTier.DemandBill2, TimeOfUse.Undefined, ncd.Season, max.Quantity, max.Datestamp))
                                    Next

                                End If
                            End If

                        End If

                        If (bDemandBillDeterminantNeeded And Not objUsages.Exists(BaseOrTier.DemandBill)) Then
                            Dim max As MaxReadingDetail = objReadings.GetMaxReadingDetail(objDemandTOU, Season.Undefined)
                            max.Quantity = max.Quantity * dblMaxMultiplier
                            objUsages.Add(New Usage(BaseOrTier.DemandBill, TimeOfUse.Undefined, Season.Undefined, max.Quantity, max.Datestamp))
                        End If

                        If (bDemandDeterminantNeeded And Not objUsages.Exists(BaseOrTier.Demand, TimeOfUse.Undefined, Season.Undefined)) Then
                            Dim max As MaxReadingDetail = objReadings.GetMaxReadingDetail(objDemandTOU, Season.Undefined)
                            max.Quantity = max.Quantity * dblMaxMultiplier
                            objUsages.Add(New Usage(BaseOrTier.Demand, TimeOfUse.Undefined, Season.Undefined, max.Quantity, max.Datestamp))
                        End If


                        ' if SEASONAL demand prorate setting dictates, then set all seasonal demand charges to max
                        If (objDetailedDef.SeasonalProrateType = SeasonalProrateType.SplitBillDays AndAlso objDetailedDef.SeasonalDemandProrateType = SeasonalDemandProrateType.SplitBillDaysMaxDemand) Then
                            ' if usages indicate a crossing of seasons
                            If (objUsages.GetSeasonCount() > 1) Then
                                objUsages.SetSeasonalDemandToOverallMaximumDemand()
                            End If
                        End If

                        '--- CR50560 BEGIN -------------------------------------------------------------------------------------------

                        If (Not objDetailedDef.ServiceCharges Is Nothing) Then

                            bDemandDeterminantNeeded = objDetailedDef.ServiceCharges.CalcTypeForSteppedDemandExists()
                            bDemandBillDeterminantNeeded = objDetailedDef.ServiceCharges.CalcTypeForSteppedDemandBillExists()

                            'Coincident demand may be needed if a charge that uses it exists.
                            If (objDetailedDef.ServiceCharges.CalcTypeExists(ChargeCalcType.CPDemandCharge)) Then

                                Dim cpcf As Double? = Nothing
                                Dim cptime As DateTime? = Nothing

                                cpcf = GetCoincidentPeakMultiplicationFactor(sRateClass)
                                cptime = GetCoincidentPeakDateTime(sRateClass)

                                'Check for CPTIME rate helper.
                                If (Not cptime Is Nothing AndAlso cptime <> DateTime.MinValue AndAlso cptime <> DateTime.MaxValue) Then

                                    Dim previousReadingInterval As DateTime = objReadings.RoundToNearestPreviousInterval(CDate(cptime), eReadingInterval)
                                    If (previousReadingInterval < dtStartDate Or previousReadingInterval > dtEndDate) Then

                                        Throw New Exception(String.Format(ksException_Message_RateHelper_cptime_DateTimeOutsideOfBillPeriod,
                                                                          cptime, dtStartDate, dtEndDate))
                                    End If
                                    Dim reading As Reading = objReadings.GetReadForDateTime(previousReadingInterval)
                                    Dim cpkW As Double?
                                    If (Not reading Is Nothing) Then
                                        cpkW = reading.Quantity * dblMaxMultiplier
                                    Else
                                        cpkW = Nothing
                                    End If
                                    If (Not cpkW Is Nothing AndAlso cpkW <> 0.0) Then

                                        'Calculate DemandBill3 from kW reading specified by rate helper cptime.
                                        Dim cpkWValue As Double
                                        cpkWValue = CDbl(cpkW)

                                        Dim cdCharges As ServiceChargeCollection = objDetailedDef.ServiceCharges.GetServiceChargesCD()

                                        For Each cd As ServiceCharge In cdCharges
                                            objUsages.Add(New Usage(objDetailedDef.LoadType, BaseOrTier.DemandBill3, TimeOfUse.Undefined, cd.Season, cpkWValue, DateTime.Now))
                                        Next

                                    Else
                                        Throw New Exception(String.Format(ksException_Message_RateHelper_cptime_MissingReadingOrBadValue,
                                                                          cptime.ToString()))
                                    End If

                                    'Check for CPCF rate helper.
                                ElseIf (Not cpcf Is Nothing AndAlso cpcf >= 0 AndAlso cpcf <= 1) Then
                                    'Calculate DemandBill3 from maximum kW reading.

                                    'Get seasonal charges if seasonal is authored for CD by travering all CD charges.
                                    Dim cdCharges As ServiceChargeCollection = objDetailedDef.ServiceCharges.GetServiceChargesCD()

                                    For Each cd As ServiceCharge In cdCharges
                                        maxDetail = objReadings.GetMaxReadingDetail(objDemandTOU, cd.Season)
                                        Dim quantityKW As Double = maxDetail.Quantity * dblMaxMultiplier
                                        Dim demandBill3Value As Double = CDbl(cpcf) * quantityKW
                                        objUsages.Add(New Usage(objDetailedDef.LoadType, BaseOrTier.DemandBill3, TimeOfUse.Undefined, cd.Season, demandBill3Value, maxDetail.Datestamp))
                                    Next


                                Else
                                    Throw New Exception(String.Format(ksException_Message_RateHelper_CoincidenceChangeRequires_cpcf_Or_cptime,
                                                                      cpcf, cptime.ToString()))
                                End If

                            End If

                        End If

                        '--- CR50560 END -------------------------------------------------------------------------------------------

                    End If
                End If
                

            End If

        End If

        ' A Usage collection is always returned; it could indeed be empty, but it will exist

        Return (objUsages)
    End Function
    ''' <summary>
    ''' create daily usage determinants
    ''' CR 51610 - Oct 2014
    ''' </summary>
    ''' <param name="readings"></param>
    ''' <param name="rateCompanyID"></param>
    ''' <param name="rateClass"></param>
    ''' <param name="startDate"></param>
    ''' <param name="endDate"></param>
    ''' <param name="dtDSTEndDate"></param>
    ''' <param name="detailedDefs"></param>
    ''' <param name="extParam"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function GetDailyUsageDeterminants(ByVal readings As ReadingCollection, ByVal rateCompanyID As Integer, ByVal rateClass As String, ByVal startDate As DateTime, ByVal endDate As DateTime, projectedEndDate As DateTime, ByVal dtDSTEndDate As DateTime, detailedDefs As DetailedRateDefinitionCollection, ByVal raf As ReadingAdjustFactors, ByVal extParam As CostToDateExtParam) As UsageCollection
        Dim usages As UsageCollection
        Dim applyFactors As Boolean = False
        Dim tierBoundaryInfo As TierBoundaryInformation = GetAllCalculatedTierBoundaryInformation(rateCompanyID, rateClass, startDate, endDate, projectedEndDate, extParam.UseProjectedEndDateWhenFinalizingTierBoundaries, detailedDefs(0))

        If (Not raf Is Nothing) Then applyFactors = True

        If (applyFactors) Then
            Log(readings) 'for advanced analysis
            ApplyReadingAdjustmentFactorToReadings(readings, ReadingAdjustmentFactorType.GeneralConservation, raf.GeneralConservation, raf.PercentReductionAsShifted)
        End If

        ' set up the season for each reading
        'SetupSeasonReadings(readings, rateCompanyID, rateClass, startDate, endDate, detailedDefs(0))
        Dim extParams As BinReadingsExtParam = New BinReadingsExtParam()
        extParams.SkipTOUBinning = True
        readings.IsDailyInterval = True
        BinReadings(rateCompanyID, detailedDefs, readings, rateClass, startDate, endDate, extParams)

        ' convert reading to usages
        usages = ConvertReadingsToUsagesEP(readings, detailedDefs(0), tierBoundaryInfo)

        ' get demand usage                    
        Dim demandUsages As UsageCollection = GetDemandUsageDeterminants(rateCompanyID, readings, rateClass, startDate, endDate, dtDSTEndDate)
        '// add demand usage to the main usage collection if exists
        If ((Not demandUsages Is Nothing) AndAlso demandUsages.Count > 0 AndAlso extParam.IncludeDailyDemandCalc) Then
            For Each demand As Usage In demandUsages
                usages.Add(demand)
            Next
        End If

        ' Check for TOU usages and create a total for consistency
        CheckAndCreateTotalUsage(usages)
        Return usages
    End Function



#End Region

#Region "Helper Functions - Private"
    ''' <summary>
    ''' convert readings into rate engine usage collection
    ''' CR 51610 Oct 2014
    ''' </summary>
    ''' <param name="readings"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function ConvertReadingsToUsagesEP(readings As ReadingCollection, detailedDef As DetailedRateDefinition, tierBoundaryInfo As TierBoundaryInformation) As CE.RateEngine.UsageCollection
        Dim usagesEP As CE.RateEngine.UsageCollection
        usagesEP = New CE.RateEngine.UsageCollection()

        Dim total As Double = 0.0
        ' create total service use based on use charge authoring if not already exist in the collection
        GetDailyTotalServieUses(usagesEP, readings, detailedDef)

        ' get usages based on tou, season
        GetDailyUses(usagesEP, readings, detailedDef, tierBoundaryInfo)

        Return usagesEP
    End Function

    ''' <summary>
    ''' create total service use based on use charge authoring if not already exist in the collection
    ''' CR 51610 Oct 2014
    ''' </summary>
    ''' <param name="usagesEP"></param>
    ''' <param name="detailedDef"></param>
    ''' <remarks></remarks>
    Private Sub GetDailyTotalServieUses(ByRef usagesEP As CE.RateEngine.UsageCollection, readings As ReadingCollection, detailedDef As DetailedRateDefinition)
        Dim total As Double = 0.0
        ' 14.12 bug 55786 - add total service with undefined season and tou if it's not in the usages collection based on the use charge set up in rate engine
        If (Not usagesEP.Exists(BaseOrTier.TotalServiceUse, TimeOfUse.Undefined, Season.Undefined)) Then
            total = readings.Sum()
            If (total <> 0) Then
                usagesEP.Add(New CE.RateEngine.Usage(CE.RateEngine.Enums.BaseOrTier.TotalServiceUse, TimeOfUse.Undefined, Season.Undefined, total))
            End If
        End If
    End Sub

    ''' <summary>
    ''' get usages collection based on season, tou 
    ''' CR 51601 Oct 2014
    ''' </summary>
    ''' <param name="usagesEP"></param>
    ''' <param name="readings"></param>
    ''' <param name="detailedDef"></param>
    ''' <param name="tierBoundaryInfo"></param>
    ''' <remarks></remarks>
    Private Sub GetDailyUses(ByRef usagesEP As CE.RateEngine.UsageCollection, readings As ReadingCollection, detailedDef As DetailedRateDefinition, tierBoundaryInfo As TierBoundaryInformation)
        For i As Enums.TimeOfUse = Enums.TimeOfUse.Undefined To Enums.TimeOfUse.CriticalPeak
            'get readings for the same tou
            Dim touReadings As List(Of Reading) = New List(Of Reading)
            For Each r As Reading In readings
                If (r.TimeOfUse = i AndAlso Not r.IsEmpty AndAlso Not r.IsGap) Then
                    touReadings.Add(r)
                End If
            Next

            If (touReadings.Count > 0) Then
                Dim aryTouSeason As Double() = New Double() {0, 0, 0, 0, 0}
                Dim aryTouSeasonExist As Boolean() = New Boolean() {False, False, False, False, False}
                'set up TOU by season
                For theSeason As Enums.Season = Enums.Season.Undefined To Enums.Season.SeasonB
                    Dim tempSeason = theSeason
                    If (touReadings.Exists(Function(t) t.Season = tempSeason)) Then
                        aryTouSeasonExist(Convert.ToInt32(theSeason)) = True
                    End If
                Next
                'sum the quantity by season for the same tou
                For Each reading As Reading In touReadings
                    aryTouSeason(Convert.ToInt32(reading.Season)) = aryTouSeason(Convert.ToInt32(reading.Season)) + reading.Quantity
                Next

                For j As Integer = 0 To 4
                    If (aryTouSeasonExist(j)) Then
                        ' convert season to rate engine season
                        Dim season As CE.RateEngine.Enums.Season = CE.RateEngine.Enums.Season.Undefined
                        Select Case (j)
                            Case 0
                                season = CE.RateEngine.Enums.Season.Undefined
                            Case 1
                                season = CE.RateEngine.Enums.Season.Summer
                            Case 2
                                season = CE.RateEngine.Enums.Season.Winter
                            Case 3
                                season = CE.RateEngine.Enums.Season.SeasonA
                            Case 4
                                season = CE.RateEngine.Enums.Season.SeasonB
                        End Select

                        ' create usage,  and add to usage collection                       
                        If (i <> TimeOfUse.Undefined) Then
                            usagesEP.Add(New CE.RateEngine.Usage(CE.RateEngine.Enums.BaseOrTier.Undefined, i, season, aryTouSeason(j)))
                        End If
                    End If
                Next
            End If
        Next
    End Sub
    ' ''' <summary>
    ' ''' set up the season for the readings if rate class has season boundaries
    ' ''' CR 51610 Oct 2014
    ' ''' </summary>
    ' ''' <param name="readings"></param>
    ' ''' <param name="rateCompanyId"></param>
    ' ''' <param name="rateClass"></param>
    ' ''' <param name="startDate"></param>
    ' ''' <param name="endDate"></param>
    ' ''' <param name="detailedDef"></param>
    ' ''' <remarks></remarks>
    'Private Sub SetupSeasonReadings(readings As ReadingCollection, rateCompanyId As Integer, rateClass As String, startDate As DateTime, endDate As DateTime, detailedDef As DetailedRateDefinition)
    '    Dim seasonBoundaries As SeasonBoundaryCollection = detailedDef.SeasonBoundaries

    '    If (seasonBoundaries IsNot Nothing AndAlso seasonBoundaries.Count > 0) Then
    '        For Each r As Reading In readings
    '            Dim seasonBoundary As SeasonBoundary = seasonBoundaries.FindInclusiveBoundary(r.Datestamp)
    '            Dim season As Enums.Season = Enums.Season.Undefined
    '            Select Case (seasonBoundary.Season)
    '                Case CE.RateEngine.Enums.Season.SeasonA
    '                    season = Enums.Season.SeasonA
    '                Case CE.RateEngine.Enums.Season.SeasonB
    '                    season = Enums.Season.SeasonB
    '                Case CE.RateEngine.Enums.Season.Summer
    '                    season = Enums.Season.Summer
    '                Case CE.RateEngine.Enums.Season.Winter
    '                    season = Enums.Season.Winter
    '            End Select
    '            r.Season = season
    '        Next
    '    End If
    'End Sub

    ''' <summary>
    ''' return collection of total usage for each tou
    ''' if seasonal, return collection of total by season for each tou
    ''' if no usage collecion return for TOU, falls back to use NoTou
    ''' CR 52497 Nov 2014
    ''' </summary>
    ''' <param name="usages"></param>
    ''' <param name="detailedDef"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function GetTotalTouUsages(usages As UsageCollection, detailedDef As DetailedRateDefinition) As UsageCollection
        Dim totalUsages As UsageCollection = New UsageCollection
        Dim total As Double
        ' get total usage for each tou
        For tou As Enums.TimeOfUse = TimeOfUse.OnPeak To TimeOfUse.CriticalPeak
            If (usages.Exists(tou)) Then
                Dim touTotal As Double = 0
                'exclude CPP adder
                If tou = TimeOfUse.CriticalPeak AndAlso detailedDef.IsRTPRate AndAlso detailedDef.IsCPEventUsageAdder AndAlso detailedDef.RTPStream = RTPStream.CriticalPeakOnly Then
                    ' no nothing
                Else
                    If (detailedDef.IsSeasonal) Then
                        ' get total tou by seadon
                        For season As Enums.Season = Enums.Season.Summer To Enums.Season.SeasonB
                            If (usages.Exists(season)) Then
                                total = 0
                                Dim touSeasonUsages As UsageCollection = Nothing
                                If (usages.ExistsAnyTierWithAnyTOU()) Then
                                    Dim touUsage As Usage = usages.FindFirstUsageWithTOUAndSeason(tou, season)
                                    If Not touUsage Is Nothing Then
                                        touSeasonUsages = New UsageCollection
                                        touSeasonUsages.Add(touUsage)
                                    End If
                                Else
                                    touSeasonUsages = usages.GetAllUsagesWithTimeOfUseAndSeason(tou, season)
                                End If
                                If (Not touSeasonUsages Is Nothing AndAlso touSeasonUsages.Count > 0) Then
                                    For Each usage As Usage In touSeasonUsages
                                        total = total + usage.Quantity
                                    Next

                                    totalUsages.Add(New Usage(Enums.BaseOrTier.TotalServiceUse, tou, season, total))
                                End If
                            End If
                        Next
                        ' if no usage collection return for the combination of tou and season
                        ' fall back to return usage collection for just the tou
                        If (totalUsages.Count = 0) Then
                            'get total by tou only
                            If (usages.Exists(tou)) Then
                                total = 0
                                Dim touUsages As UsageCollection = Nothing
                                If (usages.ExistsAnyTierWithAnyTOU()) Then
                                    Dim touUsage As Usage = usages.FindFirstUsageWithTimeOfUse(tou)
                                    If Not touUsage Is Nothing Then
                                        touUsages = New UsageCollection
                                        touUsages.Add(touUsage)
                                    End If
                                Else
                                    touUsages = usages.GetAllUsagesWithTimeOfUse(tou)
                                End If
                                If (Not touUsages Is Nothing AndAlso touUsages.Count > 0) Then
                                    For Each usage As Usage In touUsages
                                        total = total + usage.Quantity
                                    Next
                                    totalUsages.Add(New Usage(Enums.BaseOrTier.TotalServiceUse, tou, total))
                                End If
                            End If
                        End If
                    Else
                        'get total by tou only
                        If (usages.Exists(tou)) Then
                            total = 0
                            Dim touUsages As UsageCollection = Nothing
                            If (usages.ExistsAnyTierWithAnyTOU()) Then
                                Dim touUsage As Usage = usages.FindFirstUsageWithTimeOfUse(tou)
                                If Not touUsage Is Nothing Then
                                    touUsages = New UsageCollection
                                    touUsages.Add(touUsage)
                                End If
                            Else
                                touUsages = usages.GetAllUsagesWithTimeOfUse(tou)
                            End If
                            If (Not touUsages Is Nothing AndAlso touUsages.Count > 0) Then
                                For Each usage As Usage In touUsages
                                    total = total + usage.Quantity
                                Next
                                totalUsages.Add(New Usage(Enums.BaseOrTier.TotalServiceUse, tou, total))

                            End If
                        End If
                    End If
                End If
            End If
        Next

        ' if no usages return for tou, fall back to use no tou 
        If (totalUsages.Count = 0) Then
            totalUsages = GetTotalUsages(usages, detailedDef)
        End If
        Return totalUsages
    End Function

    ''' <summary>
    ''' return collection of total serivce use for undefined tou
    ''' if seasonal, return collection of total by season 
    ''' CR 52497 Nov 2014
    ''' </summary>
    ''' <param name="usages"></param>
    ''' <param name="detailedDef"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function GetTotalUsages(usages As UsageCollection, detailedDef As DetailedRateDefinition) As UsageCollection
        Dim totalUsages As UsageCollection = New UsageCollection()
        Dim total As Double
        If (detailedDef.IsSeasonal) Then
            ' get total by seaon
            For season As Enums.Season = Enums.Season.Summer To Enums.Season.SeasonB
                If (usages.Exists(season)) Then
                    total = 0
                    For Each usage As Usage In usages
                        If (usage.BaseOrTier = BaseOrTier.TotalServiceUse AndAlso usage.Season = season AndAlso usage.TimeOfUse = TimeOfUse.Undefined) Then
                            total = total + usage.Quantity
                        End If
                    Next
                    ' add to usage collection
                    totalUsages.Add(New Usage(Enums.BaseOrTier.TotalServiceUse, season, total))
                End If
            Next
            If (totalUsages.Count = 0) Then
                'get total service use only
                total = usages.GetTotalServiceUse()
                totalUsages.Add(New Usage(Enums.BaseOrTier.TotalServiceUse, total))
            End If
        Else
            'get total service use only
            total = usages.GetTotalServiceUse()
            totalUsages.Add(New Usage(Enums.BaseOrTier.TotalServiceUse, total))
        End If
        Return totalUsages
    End Function

    ''' <summary>
    ''' return the adjusted tier usage collection for TOU
    ''' CR 52497 Nov 2014
    ''' </summary>
    ''' <param name="usages"></param>
    ''' <param name="detailedDef"></param>
    ''' <param name="rateCompanyID"></param>
    ''' <param name="rateClass"></param>
    ''' <param name="startDate"></param>
    ''' <param name="endDate"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function GetTouTiersUsagesAdjustedByAbsoluteTotal(usages As UsageCollection, detailedDef As DetailedRateDefinition, rateCompanyID As Integer, rateClass As String, startDate As DateTime, endDate As DateTime, projectedEndDate As DateTime, useProjectedEndDateToFinalizeTierBoundaries As Boolean) As UsageCollection
        Dim adjustedUsages As UsageCollection = New UsageCollection
        Dim touPercentage(Enums.TimeOfUse.CriticalPeak, Enums.Season.SeasonB) As Double
        Dim touPercentageExist(Enums.TimeOfUse.CriticalPeak, Enums.Season.SeasonB) As Boolean
        ' get tier boundary info
        Dim tierBoundaryInfo As TierBoundaryInformation = GetAllCalculatedTierBoundaryInformation(rateCompanyID, rateClass, startDate, endDate, projectedEndDate, useProjectedEndDateToFinalizeTierBoundaries, detailedDef)
        For season As Season = Enums.Season.Undefined To Enums.Season.SeasonB
            If (usages.Exists(season)) Then
                Dim seasonTotal As Double = 0

                ' get absolute total by season
                For Each usage As Usage In usages
                    If (usage.Season = season) Then
                        seasonTotal = seasonTotal + Math.Abs(usage.Quantity)
                    End If
                Next

                ' get tou % by absolute total season
                For Each usage As Usage In usages
                    If (usage.Season = season) Then
                        touPercentage(usage.TimeOfUse, season) = usage.Quantity / seasonTotal
                        touPercentageExist(usage.TimeOfUse, season) = True
                    End If
                Next

                ' get the tiers based on season absolute total
                Dim seasonBoundary As TierBoundaryCollection
                If (season = Enums.Season.Undefined) Then
                    seasonBoundary = tierBoundaryInfo.TierBoundaryCollectionsList(0)
                Else
                    seasonBoundary = tierBoundaryInfo.GetTierBoundaryCollectionBySeason(season)
                End If
                Dim seasonTier As Enums.BaseOrTier = seasonBoundary.GetInclusiveTier(seasonTotal)

                ' compute the new tier usage for each tou and season
                Dim previousTierUsage As Double = 0
                For tier As Enums.BaseOrTier = Enums.BaseOrTier.Tier1 To seasonTier
                    Dim currentTierUsage As Double = 0
                    Dim Boundary As TierBoundary = seasonBoundary.GetTierBoundaryByBaseOrTier(tier)
                    If (Boundary Is Nothing) Then
                        currentTierUsage = seasonTotal - previousTierUsage
                    Else
                        If (seasonTotal > Boundary.Threshold) Then
                            Dim threshold As Double = Boundary.Threshold
                            currentTierUsage = threshold - previousTierUsage
                            previousTierUsage = threshold
                        Else
                            currentTierUsage = seasonTotal - previousTierUsage
                        End If
                    End If

                    For tou As Enums.TimeOfUse = Enums.TimeOfUse.OnPeak To Enums.TimeOfUse.CriticalPeak
                        If (touPercentageExist(tou, season)) Then
                            Dim tierUsage As Double = touPercentage(tou, season) * currentTierUsage
                            ' add usage into collection
                            adjustedUsages.Add(New Usage(tier, tou, season, tierUsage))
                        End If
                    Next
                Next
            End If
        Next
        'return usage collection
        Return adjustedUsages
    End Function

    ''' <summary>
    ''' return the adjusted tier usage collection for NoTOU
    ''' CR 52497 Nov 2014
    ''' </summary>
    ''' <param name="usages"></param>
    ''' <param name="detailedDef"></param>
    ''' <param name="rateCompanyID"></param>
    ''' <param name="rateClass"></param>
    ''' <param name="startDate"></param>
    ''' <param name="endDate"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function GetTiersUsagesAdjustedByAbsoluteTotal(usages As UsageCollection, detailedDef As DetailedRateDefinition, rateCompanyID As Integer, rateClass As String, startDate As DateTime, endDate As DateTime, projectedEndDate As DateTime, useProjectedEndDateToFinalizeTierBoundaries As Boolean) As UsageCollection
        Dim adjustedUsages As UsageCollection = New UsageCollection

        ' get tier boundary info
        Dim tierBoundaryInfo As TierBoundaryInformation = GetAllCalculatedTierBoundaryInformation(rateCompanyID, rateClass, startDate, endDate, projectedEndDate, useProjectedEndDateToFinalizeTierBoundaries, detailedDef)
        For season As Season = Enums.Season.Undefined To Enums.Season.SeasonB
            If (usages.Exists(season)) Then
                Dim seasonTotal As Double = 0
                ' get absolute total by season
                For Each usage As Usage In usages
                    If (usage.Season = season) Then
                        seasonTotal = seasonTotal + Math.Abs(usage.Quantity)
                    End If
                Next

                ' get the tiers based on season absolute total
                Dim seasonBoundary As TierBoundaryCollection
                If (season = Enums.Season.Undefined) Then
                    seasonBoundary = tierBoundaryInfo.TierBoundaryCollectionsList(0)
                Else
                    seasonBoundary = tierBoundaryInfo.GetTierBoundaryCollectionBySeason(season)
                End If
                Dim seasonTier As Enums.BaseOrTier = seasonBoundary.GetInclusiveTier(seasonTotal)

                ' compute the new tier usage for season
                Dim previousTierUsage As Double = 0
                For tier As Enums.BaseOrTier = Enums.BaseOrTier.Tier1 To seasonTier
                    Dim currentTierUsage As Double = 0
                    Dim Boundary As TierBoundary = seasonBoundary.GetTierBoundaryByBaseOrTier(tier)
                    If (Boundary Is Nothing) Then
                        currentTierUsage = seasonTotal - previousTierUsage
                    Else
                        If (seasonTotal > Boundary.Threshold) Then
                            Dim threshold As Double = Boundary.Threshold
                            currentTierUsage = threshold - previousTierUsage
                            previousTierUsage = threshold
                        Else
                            currentTierUsage = seasonTotal - previousTierUsage
                        End If
                    End If

                    Dim tierUsage As Double = -1 * currentTierUsage
                    ' add usage into collection
                    adjustedUsages.Add(New Usage(tier, season, tierUsage))
                Next
            End If
        Next
        'return usage collection
        Return adjustedUsages
    End Function

    ''' <summary>
    ''' adjusted tier usages with absolute total logic
    ''' CR 52497 Nov 2014
    ''' </summary>
    ''' <param name="usages"></param>
    ''' <param name="detailedDef"></param>
    ''' <param name="rateCompanyID"></param>
    ''' <param name="rateClass"></param>
    ''' <param name="startDate"></param>
    ''' <param name="endDate"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function AdjustTierUsageWithAbsoluteTotal(usages As UsageCollection, detailedDef As DetailedRateDefinition, rateCompanyID As Integer, rateClass As String, startDate As DateTime, endDate As DateTime, projectedEndDate As DateTime, useProjectedEndDateToFinalizeTierBoundaries As Boolean) As UsageCollection
        Dim adjustedTierUsages As UsageCollection
        Dim usage As Usage
        If detailedDef.IsTimeOfUse Then
            ' gather all tou usage colleciton
            Dim touUsages As UsageCollection = GetTotalTouUsages(usages, detailedDef)
            ' get the adjusted tiers usage collection for tou
            If (touUsages.ExistsAnyTOU()) Then
                adjustedTierUsages = GetTouTiersUsagesAdjustedByAbsoluteTotal(touUsages, detailedDef, rateCompanyID, rateClass, startDate, endDate, projectedEndDate, useProjectedEndDateToFinalizeTierBoundaries)
            Else
                ' fall back to no tou if there's no tou usages
                adjustedTierUsages = GetTiersUsagesAdjustedByAbsoluteTotal(touUsages, detailedDef, rateCompanyID, rateClass, startDate, endDate, projectedEndDate, useProjectedEndDateToFinalizeTierBoundaries)
            End If

        Else
            ' gather all usage colleciton
            Dim touUsages As UsageCollection = GetTotalUsages(usages, detailedDef)
            ' get the adjusted tiers usage collection
            adjustedTierUsages = GetTiersUsagesAdjustedByAbsoluteTotal(touUsages, detailedDef, rateCompanyID, rateClass, startDate, endDate, projectedEndDate, useProjectedEndDateToFinalizeTierBoundaries)
        End If

        ' gather all tiers usages and remove them from main usage collection
        Dim tierUsages As UsageCollection = usages.GetAllUsagesWithTiers()
        For Each usage In tierUsages
            usages.Remove(usage)
        Next

        ' add the ajusted tiers usages into collection
        For Each usage In adjustedTierUsages
            usages.Add(usage)
        Next

        ' add the total by each tier and season
        For season As Season = Enums.Season.Summer To Enums.Season.SeasonB
            If (adjustedTierUsages.Exists(season)) Then
                For tier As BaseOrTier = BaseOrTier.Tier1 To BaseOrTier.Tier6
                    If (adjustedTierUsages.Exists(tier)) Then
                        If Not usages.Exists(tier, TimeOfUse.Undefined, season) Then
                            Dim total As Double = 0
                            Dim totalTierUsages As UsageCollection = adjustedTierUsages.GetTieredUsageForTierAssignToSeason(tier, season)
                            For Each tierUsage As Usage In totalTierUsages
                                total = total + tierUsage.Quantity
                            Next
                            usages.Add(New Usage(tier, season, total))
                        End If
                    End If
                Next
            End If
        Next

        ' add the total by each tier for undefined season
        For tier As BaseOrTier = BaseOrTier.Tier1 To BaseOrTier.Tier6
            If (adjustedTierUsages.Exists(tier)) Then
                Dim total As Double = adjustedTierUsages.GetTieredTotalUsageForTier(tier)
                If (Not usages.Exists(tier, TimeOfUse.Undefined, Season.Undefined)) Then
                    usages.Add(New Usage(tier, total))
                End If
            End If
        Next

        ' add the total service use for each season
        If (detailedDef.IsSeasonal) Then
            For season As Season = Enums.Season.Summer To Enums.Season.SeasonB
                If (adjustedTierUsages.Exists(season)) Then
                    If (Not usages.Exists(BaseOrTier.TotalServiceUse, TimeOfUse.Undefined, season)) Then
                        Dim total As Double = 0
                        For Each tierUsage As Usage In adjustedTierUsages
                            If (tierUsage.Season = season) Then
                                total = total + tierUsage.Quantity
                            End If
                        Next
                        usages.Add(New Usage(BaseOrTier.TotalServiceUse, season, total))
                    End If
                End If
            Next
        End If

        ' return collection
        Return usages
    End Function


#End Region


End Class
