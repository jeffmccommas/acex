Imports CE.RateEngine.Enums

Public Class ServiceChargeCollection
	Inherits CollectionBase

	Public Sub New()
	End Sub

	Default ReadOnly Property Item(ByVal index As Integer) As ServiceCharge
		Get
			Return CType(Me.InnerList.Item(index), ServiceCharge)
		End Get
	End Property

	Public Function Add(ByVal value As ServiceCharge) As Integer
		Return Me.InnerList.Add(value)
	End Function

	Public Sub Insert(ByVal index As Integer, ByVal value As ServiceCharge)
		Me.InnerList.Insert(index, value)
	End Sub

	Public Sub AddRange(ByVal values() As ServiceCharge)
		Me.InnerList.AddRange(values)
	End Sub

	Public Sub Remove(ByVal value As ServiceCharge)
		Me.InnerList.Remove(value)
	End Sub

	Public Function GetNonStepped() As ServiceChargeCollection
		Dim i As Integer
		Dim objTemp As ServiceCharge
        Dim objServiceChargeColl As New ServiceChargeCollection

		If (Me.InnerList.Count > 0) Then

			For i = 0 To Me.InnerList.Count - 1
				objTemp = CType(Me.InnerList.Item(i), ServiceCharge)
				If (objTemp.Step = Enums.ServiceStep.Undefined) Then
					objServiceChargeColl.Add(objTemp)
				End If
			Next

		End If

		Return (objServiceChargeColl)
	End Function

	Public Function FindByParts(ByVal eStep As ServiceStep, ByVal eSeason As Season, ByVal eCalcType As ChargeCalcType) As ServiceChargeCollection
		Dim i As Integer
		Dim objTemp As ServiceCharge
        Dim objServiceChargeColl As New ServiceChargeCollection

		If (Me.InnerList.Count > 0) Then

			For i = 0 To Me.InnerList.Count - 1
				objTemp = CType(Me.InnerList.Item(i), ServiceCharge)
				If (objTemp.Step = eStep And objTemp.Season = eSeason And objTemp.CalculationType = eCalcType) Then
					objServiceChargeColl.Add(objTemp)
				End If
			Next

		End If

		Return (objServiceChargeColl)
	End Function

	Public Function FindByParts(ByVal eStep As ServiceStep, ByVal eSeason As Season) As ServiceChargeCollection
		Dim i As Integer
		Dim objTemp As ServiceCharge
        Dim objServiceChargeColl As New ServiceChargeCollection

		If (Me.InnerList.Count > 0) Then

			For i = 0 To Me.InnerList.Count - 1
				objTemp = CType(Me.InnerList.Item(i), ServiceCharge)
				If (objTemp.Step = eStep And objTemp.Season = eSeason) Then
					objServiceChargeColl.Add(objTemp)
				End If
			Next

		End If

		Return (objServiceChargeColl)
	End Function

	Public Function CalcTypeExists(ByVal eCalcType As ChargeCalcType) As Boolean
        Dim bExists As Boolean
		Dim i As Integer
		Dim objTemp As ServiceCharge

		If (Me.InnerList.Count > 0) Then

			For i = 0 To Me.InnerList.Count - 1
				objTemp = CType(Me.InnerList.Item(i), ServiceCharge)
				If (objTemp.CalculationType = eCalcType) Then
					bExists = True
					Exit For
				End If
			Next

		End If

		Return (bExists)
    End Function

    Public Function CalcTypeExists(ByVal eCalcType As ChargeCalcType, ByVal eSeason As Season) As Boolean
        Dim bExists As Boolean
        Dim i As Integer
        Dim objTemp As ServiceCharge

        If (Me.InnerList.Count > 0) Then

            For i = 0 To Me.InnerList.Count - 1
                objTemp = CType(Me.InnerList.Item(i), ServiceCharge)
                If (objTemp.CalculationType = eCalcType AndAlso objTemp.Season = eSeason) Then
                    bExists = True
                    Exit For
                End If
            Next

        End If

        Return (bExists)
    End Function

    Public Function CalcTypeForSteppedDemandExists() As Boolean
        Dim bExists As Boolean = False
        Dim i As Integer
        Dim objTemp As ServiceCharge

        If (Me.InnerList.Count > 0) Then

            For i = 0 To Me.InnerList.Count - 1
                objTemp = CType(Me.InnerList.Item(i), ServiceCharge)
                If (objTemp.CalculationType = ChargeCalcType.SteppedDiffStepDemand) Then
                    bExists = True
                    Exit For
                End If
            Next

        End If

        Return (bExists)
    End Function

    Public Function CalcTypeForSteppedDemandBillExists() As Boolean
        Dim bExists As Boolean = False
        Dim i As Integer
        Dim objTemp As ServiceCharge

        If (Me.InnerList.Count > 0) Then

            For i = 0 To Me.InnerList.Count - 1
                objTemp = CType(Me.InnerList.Item(i), ServiceCharge)
                If (objTemp.CalculationType = ChargeCalcType.SteppedDailyDemandBill _
                    Or objTemp.CalculationType = ChargeCalcType.SteppedDiffStepDemandBill _
                    Or objTemp.CalculationType = ChargeCalcType.SteppedMonthlyDemandBill) Then
                    bExists = True
                    Exit For
                End If
            Next

        End If

        Return (bExists)
    End Function

	Public Function GetSteppedCalcType() As ChargeCalcType
		Dim eCalcType As ChargeCalcType = ChargeCalcType.None
		Dim i As Integer
		Dim objTemp As ServiceCharge

		If (Me.InnerList.Count > 0) Then

			For i = 0 To Me.InnerList.Count - 1
				objTemp = CType(Me.InnerList.Item(i), ServiceCharge)
				If (objTemp.CalculationType = ChargeCalcType.SteppedMonthlyDemandBill) Then
					eCalcType = ChargeCalcType.SteppedMonthlyDemandBill
					Exit For
				ElseIf (objTemp.CalculationType = ChargeCalcType.SteppedDailyDemandBill) Then
					eCalcType = ChargeCalcType.SteppedDailyDemandBill
					Exit For
				ElseIf (objTemp.CalculationType = ChargeCalcType.SteppedDiffStepDemandBill) Then
					eCalcType = ChargeCalcType.SteppedDiffStepDemandBill
                    Exit For
                ElseIf (objTemp.CalculationType = ChargeCalcType.SteppedDiffStepDemand) Then
                    eCalcType = ChargeCalcType.SteppedDiffStepDemand
                    Exit For
                End If
            Next

		End If

		Return (eCalcType)
	End Function


    Public Function GetRebateCharges() As ServiceChargeCollection
        Dim i As Integer
        Dim t As ServiceCharge
        Dim objServiceChargeColl As New ServiceChargeCollection

        If (Me.InnerList.Count > 0) Then

            For i = 0 To Me.InnerList.Count - 1
                t = CType(Me.InnerList.Item(i), ServiceCharge)
                If (t.CalculationType = ChargeCalcType.PTRRebateEachEvent Or t.CalculationType = ChargeCalcType.PTRRebateEntireBillPeriod) Then
                    objServiceChargeColl.Add(New ServiceCharge(t.CostType, t.CalculationType, t.Step, t.Season, t.ChargeValue, t.RebateClass))
                End If
            Next

        End If

        Return (objServiceChargeColl)
    End Function

    ''' <summary>
    ''' NCD Service Charges reference to sub collection.
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function GetServiceChargesNCD() As ServiceChargeCollection
        Dim i As Integer
        Dim objTemp As ServiceCharge
        Dim objServiceChargeColl As New ServiceChargeCollection

        If (Me.InnerList.Count > 0) Then

            For i = 0 To Me.InnerList.Count - 1
                objTemp = CType(Me.InnerList.Item(i), ServiceCharge)
                If (objTemp.CalculationType = ChargeCalcType.NCDemandCharge) Then
                    objServiceChargeColl.Add(objTemp)
                End If
            Next

        End If

        Return (objServiceChargeColl)
    End Function

    '--- CR50560 BEGIN -------------------------------------------------------------------------------------------
    ''' <summary>
    ''' Coincidence Demand (CD) Service Charges reference to sub collection.
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function GetServiceChargesCD() As ServiceChargeCollection
        Dim i As Integer
        Dim objTemp As ServiceCharge
        Dim objServiceChargeColl As New ServiceChargeCollection

        If (Me.InnerList.Count > 0) Then

            For i = 0 To Me.InnerList.Count - 1
                objTemp = CType(Me.InnerList.Item(i), ServiceCharge)
                If (objTemp.CalculationType = ChargeCalcType.CPDemandCharge) Then
                    objServiceChargeColl.Add(objTemp)
                End If
            Next

        End If

        Return (objServiceChargeColl)
    End Function
    '--- CR50560 END ---------------------------------------------------------------------------------------------

    ' specific special calc type exists
    Public Function CalcTypeForNCDemandFactorExists() As Boolean
        Dim bExists As Boolean = False
        Dim i As Integer
        Dim objTemp As ServiceCharge

        If (Me.InnerList.Count > 0) Then

            For i = 0 To Me.InnerList.Count - 1
                objTemp = CType(Me.InnerList.Item(i), ServiceCharge)
                If (objTemp.CalculationType = ChargeCalcType.NCDemandMultFactor) Then
                    bExists = True
                    Exit For
                End If
            Next

        End If

        Return (bExists)
    End Function

    ' specific special charge value
    Public Function FindNCDemandFactor(ByVal eSeason As Season, ByVal eCalcType As ChargeCalcType) As Double
        Dim result As Double = 0.0
        Dim i As Integer
        Dim objTemp As ServiceCharge

        If (Me.InnerList.Count > 0) Then

            For i = 0 To Me.InnerList.Count - 1
                objTemp = CType(Me.InnerList.Item(i), ServiceCharge)
                If (objTemp.Step = ServiceStep.Undefined And objTemp.Season = eSeason And objTemp.CalculationType = eCalcType) Then
                    result = objTemp.ChargeValue
                    Exit For
                End If
            Next

        End If

        Return (result)
    End Function

    Public Function FindCDemandFactor(ByVal eSeason As Season, ByVal eCalcType As ChargeCalcType) As Double
        Dim result As Double = 0.0
        Dim i As Integer
        Dim objTemp As ServiceCharge

        If (Me.InnerList.Count > 0) Then

            For i = 0 To Me.InnerList.Count - 1
                objTemp = CType(Me.InnerList.Item(i), ServiceCharge)
                If (objTemp.Step = ServiceStep.Undefined And objTemp.Season = eSeason And objTemp.CalculationType = eCalcType) Then
                    result = objTemp.ChargeValue
                    Exit For
                End If
            Next

        End If

        Return (result)
    End Function

End Class
