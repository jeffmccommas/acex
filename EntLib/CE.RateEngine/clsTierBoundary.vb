Imports CE.RateEngine.Enums

Public Class TierBoundary
    Inherits BoundaryBase

    Private m_eBaseOrTier As BaseOrTier
    Private m_eTimeOfUse As TimeOfUse
    Private m_eSeason As Season
    Private m_dblThreshold As Double
    Private m_dblSecondaryThreshold As Double

    Private Sub New()
    End Sub

    Public Sub New(ByVal eBaseOrTier As BaseOrTier, ByVal eTimeOfUse As TimeOfUse, ByVal eSeason As Season, ByVal dblThreshold As Double)
        MyBase.New()
        m_eBaseOrTier = eBaseOrTier
        m_eTimeOfUse = eTimeOfUse
        m_eSeason = eSeason
        m_dblThreshold = dblThreshold
        'm_dblSecondaryThreshold = 0
    End Sub

    Public Sub New(ByVal eBaseOrTier As BaseOrTier, ByVal eTimeOfUse As TimeOfUse, ByVal eSeason As Season, ByVal dblThreshold As Double, ByVal dblSecondaryThreshold As Double)
        Me.New(eBaseOrTier, eTimeOfUse, eSeason, dblThreshold)
        m_dblSecondaryThreshold = dblSecondaryThreshold
    End Sub

#Region "Properties"

    Public ReadOnly Property BaseOrTier() As BaseOrTier
        Get
            Return (m_eBaseOrTier)
        End Get
    End Property

    Public ReadOnly Property TimeOfUse() As TimeOfUse
        Get
            Return (m_eTimeOfUse)
        End Get
    End Property

    Public ReadOnly Property Season() As Season
        Get
            Return (m_eSeason)
        End Get
    End Property

    Public Property Threshold() As Double
        Get
            Return m_dblThreshold
        End Get
        Set(ByVal Value As Double)
            m_dblThreshold = Value
        End Set
    End Property

    Public Property SecondaryThreshold() As Double
        Get
            Return m_dblSecondaryThreshold
        End Get
        Set(ByVal Value As Double)
            m_dblSecondaryThreshold = Value
        End Set
    End Property

#End Region

End Class
