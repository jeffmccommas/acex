Imports CE.RateEngine.Enums

Public Class FCR
	Private m_dblChargeValue As Double
	Private m_dtStartDate As DateTime

	Public Sub New()
	End Sub

	Public Sub New(ByVal dtStartDate As DateTime, ByVal dblChargeValue As Double)
		m_dtStartDate = dtStartDate
		m_dblChargeValue = dblChargeValue
	End Sub

#Region "Properties"

	Public Property StartDate() As DateTime
		Get
			Return (m_dtStartDate)
		End Get
		Set(ByVal Value As DateTime)
			m_dtStartDate = Value
		End Set
	End Property

	Public Property ChargeValue() As Double
		Get
			Return (m_dblChargeValue)
		End Get
		Set(ByVal Value As Double)
			m_dblChargeValue = Value
		End Set
	End Property

#End Region
End Class
