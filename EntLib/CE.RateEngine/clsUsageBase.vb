Imports CE.RateEngine.Enums

<Serializable()> Public Class UsageBase

    Private m_objReadings As ReadingCollection
    Protected m_eLoadType As LoadType
    Private m_baselines As BaselineCollection
    Private m_virtualQuantity As Double     ' usually unused
    Private m_wasSwapped As Boolean ' usually unused
    Private m_datestamp As DateTime ' occasionally a usage determinant is assigned a specific datetime;

    Public Sub New()
        m_eLoadType = Enums.LoadType.General
    End Sub

    Public Sub New(ByVal eLoadType As LoadType)
        m_eLoadType = eLoadType
    End Sub

#Region "Properties"

    Public Property Readings() As ReadingCollection
        Get
            Return m_objReadings
        End Get
        Set(ByVal Value As ReadingCollection)
            m_objReadings = Value
        End Set
    End Property

    Public Property LoadType() As LoadType
        Get
            Return m_eLoadType
        End Get
        Set(ByVal Value As LoadType)
            m_eLoadType = Value
        End Set
    End Property

    Public Property Baselines() As BaselineCollection
        Get
            Return m_baselines
        End Get
        Set(ByVal Value As BaselineCollection)
            m_baselines = Value
        End Set
    End Property

    ''' <summary>
    ''' Usually unused, and therfore zero.
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property VirtualQuantity() As Double
        Get
            Return m_virtualQuantity
        End Get
        Set(ByVal Value As Double)
            m_virtualQuantity = Value
        End Set
    End Property

    Public Property VirtualQuantityWasSwapped() As Boolean
        Get
            Return m_wasSwapped
        End Get
        Set(ByVal Value As Boolean)
            m_wasSwapped = Value
        End Set
    End Property

    ''' <summary>
    ''' Datestamp is used for special usage determinants.
    ''' Demand usages have become more prominent and will usually be datestamped with the datestamp that the usage may have occured.
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property Datestamp() As DateTime
        Get
            Return m_datestamp
        End Get
        Set(ByVal Value As DateTime)
            m_datestamp = Value
        End Set
    End Property

#End Region

End Class
