﻿Imports CE.RateEngine.Enums

Public Class CalculateCostInputParam

    Private m_sRateClass As String
    Private m_dtStartDate As DateTime
    Private m_dtEndDate As DateTime
    Private m_objUsageCollection As UsageCollection

    Public Sub New()
    End Sub

    Private Sub New(ByVal sRateClass As String, ByVal dtStartDate As DateTime, ByVal dtEndDate As DateTime)
        m_sRateClass = sRateClass
        m_dtStartDate = dtStartDate
        m_dtEndDate = dtEndDate
    End Sub

    Public Sub New(ByVal sRateClass As String, ByVal dtStartDate As DateTime, ByVal dtEndDate As DateTime, ByVal objUsageCollection As UsageCollection)
        Me.New(sRateClass, dtStartDate, dtEndDate)
        m_objUsageCollection = objUsageCollection
    End Sub

    Public Sub New(ByVal sRateClass As String, ByVal dtStartDate As DateTime, ByVal dtEndDate As DateTime, ByVal objUsage As Usage)
        Me.New(sRateClass, dtStartDate, dtEndDate)
        m_objUsageCollection = New UsageCollection
        m_objUsageCollection.Add(objUsage)
    End Sub

#Region "Properties"

    Public Property RateClass() As String
        Get
            Return m_sRateClass
        End Get
        Set(ByVal Value As String)
            m_sRateClass = Value
        End Set
    End Property

    Public Property StartDate() As DateTime
        Get
            Return m_dtStartDate
        End Get
        Set(ByVal Value As DateTime)
            m_dtStartDate = Value
        End Set
    End Property

    Public Property EndDate() As DateTime
        Get
            Return m_dtEndDate
        End Get
        Set(ByVal Value As DateTime)
            m_dtEndDate = Value
        End Set
    End Property

    Public Property UsageCollection() As UsageCollection
        Get
            Return m_objUsageCollection
        End Get
        Set(ByVal Value As UsageCollection)
            m_objUsageCollection = Value
        End Set
    End Property

#End Region

End Class
