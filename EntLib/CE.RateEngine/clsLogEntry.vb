Public Class LogEntry
	Private m_nID As Integer
	Private m_sText As String

	Public Sub New()
	End Sub

	Public Sub New(ByVal nID As Integer, ByVal sName As String)
		m_nID = nID
		m_sText = sName
	End Sub

	Public Property ID() As Integer
		Get
			Return m_nID
		End Get
		Set(ByVal Value As Integer)
			m_nID = Value
		End Set
	End Property

	Public Property Text() As String
		Get
			Return m_sText
		End Get
		Set(ByVal Value As String)
			m_sText = Value
		End Set
	End Property

End Class
