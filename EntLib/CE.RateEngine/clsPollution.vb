Public Class Pollution

	Private m_nPollutionID As Integer
	Private m_dblCO2 As Double
	Private m_dblSO2 As Double
	Private m_dblNOx As Double

	Public Sub New()
	End Sub

#Region "Properties"

	Public Property PollutionID() As Integer
		Get
			Return (m_nPollutionID)
		End Get
		Set(ByVal Value As Integer)
			m_nPollutionID = Value
		End Set
	End Property

	Public Property CO2() As Double
		Get
			Return (m_dblCO2)
		End Get
		Set(ByVal Value As Double)
			m_dblCO2 = Value
		End Set
	End Property

	Public Property SO2() As Double
		Get
			Return (m_dblSO2)
		End Get
		Set(ByVal Value As Double)
			m_dblSO2 = Value
		End Set
	End Property

	Public Property NOx() As Double
		Get
			Return (m_dblNOx)
		End Get
		Set(ByVal Value As Double)
			m_dblNOx = Value
		End Set
	End Property

#End Region

End Class
