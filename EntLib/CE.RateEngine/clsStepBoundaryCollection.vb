Imports CE.RateEngine.Enums

Public Class StepBoundaryCollection
	Inherits CollectionBase

	Public Sub New()
	End Sub

	Default ReadOnly Property Item(ByVal index As Integer) As StepBoundary
		Get
			Return CType(Me.InnerList.Item(index), StepBoundary)
		End Get
	End Property

	Public Function Add(ByVal value As StepBoundary) As Integer
		Return Me.InnerList.Add(value)
	End Function

	Public Sub Insert(ByVal index As Integer, ByVal value As StepBoundary)
		Me.InnerList.Insert(index, value)
	End Sub

	Public Sub AddRange(ByVal values() As StepBoundary)
		Me.InnerList.AddRange(values)
	End Sub

	Public Sub Remove(ByVal value As StepBoundary)
		Me.InnerList.Remove(value)
	End Sub

	Public Function FindByParts(ByVal eSeason As Season) As StepBoundaryCollection
		Dim i As Integer
		Dim objTemp As StepBoundary
        Dim objStepBoundaryColl As New StepBoundaryCollection

		If (Me.InnerList.Count > 0) Then

			For i = 0 To Me.InnerList.Count - 1
				objTemp = CType(Me.InnerList.Item(i), StepBoundary)
				If (objTemp.Season = eSeason) Then
					objStepBoundaryColl.Add(objTemp)
				End If
			Next

		End If

		Return (objStepBoundaryColl)
	End Function

	Public Function FindByParts(ByVal eStep As ServiceStep, ByVal eSeason As Season) As StepBoundaryCollection
		Dim i As Integer
		Dim objTemp As StepBoundary
        Dim objStepBoundaryColl As New StepBoundaryCollection

		If (Me.InnerList.Count > 0) Then

			For i = 0 To Me.InnerList.Count - 1
				objTemp = CType(Me.InnerList.Item(i), StepBoundary)
				If (objTemp.Step = eStep And objTemp.Season = eSeason) Then
					objStepBoundaryColl.Add(objTemp)
				End If
			Next

		End If

		Return (objStepBoundaryColl)
	End Function

	Public Function GetByStep(ByVal eStep As ServiceStep) As StepBoundary
		Dim i As Integer
		Dim objTemp As StepBoundary
		Dim objStepBoundary As StepBoundary= Nothing

		If (Me.InnerList.Count > 0) Then

			For i = 0 To Me.InnerList.Count - 1
				objTemp = CType(Me.InnerList.Item(i), StepBoundary)
				If (objTemp.Step = eStep) Then
					objStepBoundary = CType(Me.InnerList.Item(i), StepBoundary)
					Exit For
				End If
			Next
		End If

		Return (objStepBoundary)
	End Function

	Public Function FindInclusiveBoundary(ByVal dblValue As Double) As StepBoundary
		Dim i As Integer
		Dim objTemp As StepBoundary= Nothing
		Dim objStepBoundary As StepBoundary= Nothing
        Dim bFound As Boolean

		If (Me.InnerList.Count > 0) Then

			For i = 0 To Me.InnerList.Count - 1
				objTemp = CType(Me.InnerList.Item(i), StepBoundary)

				If (dblValue < objTemp.Threshold) Then
					objStepBoundary = CType(Me.InnerList.Item(i), StepBoundary)
					bFound = True
					Exit For
				End If
			Next

			If (Not bFound) Then
				' Exceeded the top boundary; okay, so return topBoundary+1 with high Boundary Value
				objStepBoundary = New StepBoundary(CType(Me.InnerList.Count + 1, ServiceStep), objTemp.Season, 999999)
			End If

		End If

		Return (objStepBoundary)
	End Function

	Public Sub Sort()
		Dim stepSorter As IComparer = New AscendingStepSorter
		Me.InnerList.Sort(stepSorter)
	End Sub

	Private Class AscendingStepSorter
		Implements IComparer

		Public Function Compare(ByVal x As Object, ByVal y As Object) As Integer Implements System.Collections.IComparer.Compare
			Dim sb As StepBoundary = CType(x, StepBoundary)
			Dim icA As IComparable = CType(sb.Step, IComparable)
			Dim sb2 As StepBoundary = CType(y, StepBoundary)
			Dim icB As IComparable = CType(sb2.Step, IComparable)

			Return (icA.CompareTo(icB))
		End Function

	End Class

End Class
