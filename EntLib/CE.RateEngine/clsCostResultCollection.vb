﻿Imports CE.RateEngine.Enums

Public Class CostResultCollection
    Inherits CollectionBase

    Public Sub New()
    End Sub

    Default ReadOnly Property Item(ByVal index As Integer) As CostResult
        Get
            Return CType(Me.InnerList.Item(index), CostResult)
        End Get
    End Property

    Public Function Add(ByVal value As CostResult) As Integer
        Return Me.InnerList.Add(value)
    End Function

    Public Sub Insert(ByVal index As Integer, ByVal value As CostResult)
        Me.InnerList.Insert(index, value)
    End Sub

    Public Sub AddRange(ByVal values() As CostResult)
        Me.InnerList.AddRange(values)
    End Sub

    Public Sub Remove(ByVal value As CostResult)
        Me.InnerList.Remove(value)
    End Sub

    Public Function Sum() As CostResult
        Dim dblTotal As Double = 0
        Dim i As Integer
        Dim objTemp As CostResult
        Dim objCost As Cost
        Dim objDE As DictionaryEntry
        Dim objCostResult As New CostResult

        objCostResult.CostCollection = New CostCollection
        objCostResult.ErrorCode = ErrCode.NoError

        If (Me.InnerList.Count > 0) Then
            For i = 0 To (Me.InnerList.Count - 1)
                objTemp = CType(Me.InnerList.Item(i), CostResult)

                objCostResult.TotalCost = objCostResult.TotalCost + objTemp.TotalCost
                objCostResult.TotalUse = objCostResult.TotalUse + objTemp.TotalUse
                objCostResult.BillDays = objCostResult.BillDays + objTemp.BillDays
                objCostResult.RateCountApplied = objCostResult.RateCountApplied + objTemp.RateCountApplied

                If (objTemp.ErrorCode <> ErrCode.NoError) Then
                    objCostResult.ErrorCode = objTemp.ErrorCode
                End If

                For Each objDE In objTemp.CostCollection
                    objCost = CType(objDE.Value, Cost)
                    objCostResult.CostCollection.SumAdd(objCost.CostType, objCost)
                Next
            Next
        End If

        If (objCostResult.BillDays > 0) Then
            objCostResult.AvgDailyEnergyUse = objCostResult.TotalUse / objCostResult.BillDays
        Else
            objCostResult.AvgDailyEnergyUse = 0
        End If

        Return (objCostResult)
    End Function

End Class
