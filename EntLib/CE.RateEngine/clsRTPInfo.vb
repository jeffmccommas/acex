﻿
''' <summary>
''' Simple real-time-pricing information.  This is primarily used by advanced callers via the BillModel interface.
''' This class is filled via the regular simple and detailed rate definition calls.
''' </summary>
''' <remarks></remarks>
Public Class RTPInfo

    Private m_IsRTP As Boolean
    Private m_IsCPP As Boolean
    Private m_IsPTR As Boolean
    Private m_RTPGroupIdSpecified As Boolean
    Private m_RTPGroupId As Integer

    Public Sub New()
        m_IsRTP = False
        m_IsCPP = False
        m_IsPTR = False
        m_RTPGroupIdSpecified = False
        m_RTPGroupId = -1
    End Sub

    Public Sub New(ByVal isRealTimePricing As Boolean, ByVal isCriticalPeakPricing As Boolean, ByVal isPeakTimeRebate As Boolean)
        m_IsRTP = isRealTimePricing
        m_IsCPP = isCriticalPeakPricing
        m_IsPTR = isPeakTimeRebate
    End Sub

    Public Sub New(ByVal isRealTimePricing As Boolean, ByVal isCriticalPeakPricing As Boolean, ByVal isPeakTimeRebate As Boolean, ByVal RTPGroupId As Integer)
        m_IsRTP = isRealTimePricing
        m_IsCPP = isCriticalPeakPricing
        m_IsPTR = isPeakTimeRebate
        m_RTPGroupId = RTPGroupId
        m_RTPGroupIdSpecified = True
    End Sub


#Region "Properties"

    Public Property IsRTP() As Boolean
        Get
            Return m_IsRTP
        End Get
        Set(ByVal Value As Boolean)
            m_IsRTP = Value
        End Set
    End Property

    Public Property IsCPP() As Boolean
        Get
            Return m_IsCPP
        End Get
        Set(ByVal Value As Boolean)
            m_IsCPP = Value
        End Set
    End Property

    Public Property IsPTR() As Boolean
        Get
            Return m_IsPTR
        End Get
        Set(ByVal Value As Boolean)
            m_IsPTR = Value
        End Set
    End Property

    Public Property RTPGroupId() As Integer
        Get
            Return m_RTPGroupId
        End Get
        Set(ByVal Value As Integer)
            m_RTPGroupId = Value
        End Set
    End Property

    Public Property RTPGroupIdSpecified() As Boolean
        Get
            Return m_RTPGroupIdSpecified
        End Get
        Set(ByVal Value As Boolean)
            m_RTPGroupIdSpecified = Value
        End Set
    End Property

#End Region

End Class



