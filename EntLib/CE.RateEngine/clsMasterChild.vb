﻿Imports CE.RateEngine.Enums

Public Class MasterChild

    Private m_nMasterID As Integer
    Private m_nChildID As Integer
    Private m_sDescription As String
    Private m_sRateClass As String
    Private m_eLoadType As LoadType     ' this exists as a property to help callers determine load type of any children easily

    Private Sub New()
    End Sub

    Public Sub New(ByVal nMasterID As Integer, ByVal nChildID As Integer, ByVal eLoadType As LoadType)
        m_nMasterID = nMasterID
        m_nChildID = nChildID
        m_eLoadType = eLoadType
        m_sDescription = String.Empty
    End Sub

    Public Sub New(ByVal nMasterID As Integer, ByVal nChildID As Integer, ByVal eLoadType As LoadType, ByVal sRateClass As String)
        Me.New(nMasterID, nChildID, eLoadType)
        m_sRateClass = sRateClass
    End Sub

    Public Sub New(ByVal nMasterID As Integer, ByVal nChildID As Integer, ByVal eLoadType As LoadType, ByVal sRateClass As String, ByVal sDescription As String)
        Me.New(nMasterID, nChildID, eLoadType)
        m_sRateClass = sRateClass
        m_sDescription = sDescription
    End Sub

#Region "Properties"

    Public ReadOnly Property RateMasterID() As Integer
        Get
            Return (m_nMasterID)
        End Get
    End Property

    Public ReadOnly Property ChildID() As Integer
        Get
            Return (m_nChildID)
        End Get
    End Property

    Public ReadOnly Property Description() As String
        Get
            Return (m_sDescription)
        End Get
    End Property

    Public ReadOnly Property RateClass() As String
        Get
            Return (m_sRateClass)
        End Get
    End Property

    Public ReadOnly Property LoadType() As LoadType
        Get
            Return (m_eLoadType)
        End Get
    End Property

#End Region

End Class
