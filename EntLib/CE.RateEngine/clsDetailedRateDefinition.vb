Imports CE.RateEngine.Enums

Public Class DetailedRateDefinition
	Inherits RateDefinitionBase

	Private m_objServiceCharges As ServiceChargeCollection
	Private m_objUseCharges As UseChargeCollection
	Private m_objTaxCharges As TaxChargeCollection
	Private m_objMinimumCharges As MinimumChargeCollection
	Private m_objTierBoundaries As TierBoundaryCollection
	Private m_objTOUBoundaries As TOUBoundaryCollection
	Private m_objSeasonBoundaries As SeasonBoundaryCollection
	Private m_objStepBoundaries As StepBoundaryCollection
	Private m_objDifferences As DifferenceCollection

	Public Sub New()
		MyBase.New()
		m_objServiceCharges = New ServiceChargeCollection
		m_objUseCharges = New UseChargeCollection
		m_objTaxCharges = New TaxChargeCollection
		m_objMinimumCharges = New MinimumChargeCollection
		m_objTierBoundaries = New TierBoundaryCollection
		m_objTOUBoundaries = New TOUBoundaryCollection
		m_objSeasonBoundaries = New SeasonBoundaryCollection
		m_objStepBoundaries = New StepBoundaryCollection
		m_objDifferences = New DifferenceCollection
	End Sub

#Region "Properties"

	Public ReadOnly Property ServiceCharges() As ServiceChargeCollection
		Get
			Return (m_objServiceCharges)
		End Get
	End Property

	Public ReadOnly Property UseCharges() As UseChargeCollection
		Get
			Return (m_objUseCharges)
		End Get
	End Property

	Public ReadOnly Property TaxCharges() As TaxChargeCollection
		Get
			Return (m_objTaxCharges)
		End Get
	End Property
	Public ReadOnly Property MinimumCharges() As MinimumChargeCollection
		Get
			Return (m_objMinimumCharges)
		End Get
	End Property

	Public ReadOnly Property TierBoundaries() As TierBoundaryCollection
		Get
			Return (m_objTierBoundaries)
		End Get
	End Property

	Public ReadOnly Property TOUBoundaries() As TOUBoundaryCollection
		Get
			Return (m_objTOUBoundaries)
		End Get
	End Property

	Public ReadOnly Property SeasonBoundaries() As SeasonBoundaryCollection
		Get
			Return (m_objSeasonBoundaries)
		End Get
	End Property

	Public ReadOnly Property StepBoundaries() As StepBoundaryCollection
		Get
			Return (m_objStepBoundaries)
		End Get
	End Property

	Public ReadOnly Property Differences() As DifferenceCollection
		Get
			Return (m_objDifferences)
		End Get
	End Property


#End Region

End Class
