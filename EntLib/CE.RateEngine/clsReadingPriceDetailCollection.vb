﻿Imports CE.RateEngine.Enums

<Serializable()> Public Class ReadingPriceDetailCollection
    Inherits CollectionBase
    Implements ICloneable

    Public Sub New()
    End Sub

    Default ReadOnly Property Item(ByVal index As Integer) As ReadingPriceDetail
        Get
            Return CType(Me.InnerList.Item(index), ReadingPriceDetail)
        End Get
    End Property

    Public Function Add(ByVal value As ReadingPriceDetail) As Integer
        Return Me.InnerList.Add(value)
    End Function

    Public Sub Insert(ByVal index As Integer, ByVal value As ReadingPriceDetail)
        Me.InnerList.Insert(index, value)
    End Sub

    Public Sub AddRange(ByVal values() As ReadingPriceDetail)
        Me.InnerList.AddRange(values)
    End Sub

    Public Sub Remove(ByVal value As ReadingPriceDetail)
        Me.InnerList.Remove(value)
    End Sub

#Region "Properties"

#End Region

    Public Function Sum() As Double
        Dim totalPrice As Double = 0
        Dim i As Integer
        Dim priceDetail As ReadingPriceDetail

        If (Me.InnerList.Count > 0) Then
            For i = 0 To (Me.InnerList.Count - 1)
                priceDetail = CType(Me.InnerList.Item(i), ReadingPriceDetail)
                totalPrice = totalPrice + priceDetail.Price
            Next
        End If

        Return (totalPrice)
    End Function


    ' This directly implements deep clone by serializing to a memory stream
    Public Function Clone() As Object Implements System.ICloneable.Clone
        Dim bf As System.Runtime.Serialization.Formatters.Binary.BinaryFormatter = _
         New System.Runtime.Serialization.Formatters.Binary.BinaryFormatter
        Dim ms As System.IO.MemoryStream = New System.IO.MemoryStream
        Dim obj As ReadingPriceDetailCollection

        bf.Serialize(ms, Me)
        ms.Seek(0, System.IO.SeekOrigin.Begin)
        obj = CType(bf.Deserialize(ms), ReadingPriceDetailCollection)
        ms.Close()

        Return (obj)
    End Function

End Class
