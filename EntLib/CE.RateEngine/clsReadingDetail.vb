﻿Imports CE.RateEngine.Enums


<Serializable()> Public Class ReadingDetail

    ''original
    'Private m_tier As BaseOrTier
    'Private m_tierSeasonal As BaseOrTier
    'Private m_tierTOU As BaseOrTier
    'Private m_tierTOUSeasonal As BaseOrTier

    ''adder specific
    'Private m_adderTier As BaseOrTier
    'Private m_adderTierSeasonal As BaseOrTier
    'Private m_adderTierTOU As BaseOrTier
    'Private m_adderTierTOUSeasonal As BaseOrTier
    Private m_TierType As TierType
    Private m_Tier As BaseOrTier
    Private m_Quantity As Double
    Private m_totalPrice As Double      ' only used when ReadingDetail is used
    Private m_totalCost As Double       ' only used when ReadingDetail is used
    Private m_priceDetails As ReadingPriceDetailCollection

    Public Sub New()
        m_priceDetails = New ReadingPriceDetailCollection()
    End Sub

    'Public Sub New(ByVal tier As BaseOrTier)
    '    Me.New(tier, BaseOrTier.Undefined, BaseOrTier.Undefined, BaseOrTier.Undefined)
    'End Sub

    Public Sub New(ByVal Tier As BaseOrTier, ByVal TierType As TierType, ByVal Quantity As Double)
        Me.New()
        m_TierType = TierType
        m_Tier = Tier
        m_Quantity = Quantity
        'm_tier = tier
        'm_tierSeasonal = tierSeasonal
        'm_tierTOU = tierTOU
        'm_tierTOUSeasonal = tierTOUSeasonal
    End Sub


#Region "Properties"

    Public Property PriceDetails() As ReadingPriceDetailCollection
        Get
            Return (m_priceDetails)
        End Get
        Set(ByVal value As ReadingPriceDetailCollection)
            m_priceDetails = value
        End Set
    End Property

    Public Property Tier() As BaseOrTier
        Get
            Return (m_Tier)
        End Get
        Set(ByVal value As BaseOrTier)
            m_Tier = value
        End Set
    End Property

    Public Property TierType() As TierType
        Get
            Return (m_TierType)
        End Get
        Set(ByVal value As TierType)
            m_TierType = value
        End Set
    End Property

    Public Property Quantity() As Double
        Get
            Return (m_Quantity)
        End Get
        Set(ByVal value As Double)
            m_Quantity = value
        End Set
    End Property
    Public Property TotalPrice() As Double
        Get
            Return m_totalPrice
        End Get
        Set(ByVal Value As Double)
            m_totalPrice = Value
        End Set
    End Property

    Public Property TotalCost() As Double
        Get
            Return m_totalCost
        End Get
        Set(ByVal Value As Double)
            m_totalCost = Value
        End Set
    End Property
    'Public Property TierSeasonal() As BaseOrTier
    '    Get
    '        Return (m_tierSeasonal)
    '    End Get
    '    Set(ByVal value As BaseOrTier)
    '        m_tierSeasonal = value
    '    End Set
    'End Property

    'Public Property TierTOU() As BaseOrTier
    '    Get
    '        Return (m_tierTOU)
    '    End Get
    '    Set(ByVal value As BaseOrTier)
    '        m_tierTOU = value
    '    End Set
    'End Property

    'Public Property TierTOUSeasonal() As BaseOrTier
    '    Get
    '        Return (m_tierTOUSeasonal)
    '    End Get
    '    Set(ByVal value As BaseOrTier)
    '        m_tierTOUSeasonal = value
    '    End Set
    'End Property

    'Public Property TierRegular() As BaseOrTier
    '    Get
    '        Return (m_tier)
    '    End Get
    '    Set(ByVal value As BaseOrTier)
    '        m_tier = value
    '    End Set
    'End Property

    'Public Property AdderTierSeasonal() As BaseOrTier
    '    Get
    '        Return (m_adderTierSeasonal)
    '    End Get
    '    Set(ByVal value As BaseOrTier)
    '        m_adderTierSeasonal = value
    '    End Set
    'End Property

    'Public Property AdderTierTOU() As BaseOrTier
    '    Get
    '        Return (m_adderTierTOU)
    '    End Get
    '    Set(ByVal value As BaseOrTier)
    '        m_adderTierTOU = value
    '    End Set
    'End Property

    'Public Property AdderTierTOUSeasonal() As BaseOrTier
    '    Get
    '        Return (m_adderTierTOUSeasonal)
    '    End Get
    '    Set(ByVal value As BaseOrTier)
    '        m_adderTierTOUSeasonal = value
    '    End Set
    'End Property

    'Public Property AdderTierRegular() As BaseOrTier
    '    Get
    '        Return (m_adderTier)
    '    End Get
    '    Set(ByVal value As BaseOrTier)
    '        m_adderTier = value
    '    End Set
    'End Property

#End Region

    Public Function GetTier() As BaseOrTier

        Return m_Tier
        'If (Me.TierRegular <> BaseOrTier.Undefined) Then
        '    Return (Me.TierRegular)
        'ElseIf (Me.TierSeasonal <> BaseOrTier.Undefined) Then
        '    Return (Me.TierSeasonal)
        'ElseIf (Me.TierTOU <> BaseOrTier.Undefined) Then
        '    Return (Me.TierTOU)
        'ElseIf (Me.TierTOUSeasonal <> BaseOrTier.Undefined) Then
        '    Return (Me.TierTOUSeasonal)
        'Else
        '    Return (BaseOrTier.Undefined)
        'End If

    End Function

End Class
