Imports CE.RateEngine.Enums

Public Class TierBoundaryCollection
    Inherits CollectionBase

    Public Sub New()
    End Sub

    Default ReadOnly Property Item(ByVal index As Integer) As TierBoundary
        Get
            Return CType(Me.InnerList.Item(index), TierBoundary)
        End Get
    End Property

    Public Function Add(ByVal value As TierBoundary) As Integer
        Return Me.InnerList.Add(value)
    End Function

    Public Sub Insert(ByVal index As Integer, ByVal value As TierBoundary)
        Me.InnerList.Insert(index, value)
    End Sub

    Public Sub AddRange(ByVal values() As TierBoundary)
        Me.InnerList.AddRange(values)
    End Sub

    Public Sub Remove(ByVal value As TierBoundary)
        Me.InnerList.Remove(value)
    End Sub

    Public Function Scale(ByVal factor As Double) As Boolean
        Dim objTierBoundaryColl As New TierBoundaryCollection
        Dim i As Integer
        Dim tb As TierBoundary

        For i = 0 To (Me.InnerList.Count - 1)
            tb = CType(Me.InnerList.Item(i), TierBoundary)
            tb.Threshold = tb.Threshold * factor
            tb.SecondaryThreshold = tb.SecondaryThreshold * factor
        Next

        Return (True)
    End Function

    Public Function GetForSeason(ByVal eSeason As Season) As TierBoundaryCollection
        Dim objTierBoundaryColl As New TierBoundaryCollection
        Dim i As Integer
        Dim objTierBoundary As TierBoundary

        For i = 0 To (Me.InnerList.Count - 1)
            objTierBoundary = CType(Me.InnerList.Item(i), TierBoundary)
            If (objTierBoundary.Season = eSeason) Then
                objTierBoundaryColl.Add(objTierBoundary)
            End If
        Next

        Return (objTierBoundaryColl)
    End Function

    Public Function GetTierBoundaryByBaseOrTier(BaseOrTier As BaseOrTier) As TierBoundary
        Dim i As Integer
        Dim objTierBoundary As TierBoundary
        Dim objRetTierBoundary As TierBoundary= Nothing


        For i = 0 To (Me.InnerList.Count - 1)
            objTierBoundary = CType(Me.InnerList.Item(i), TierBoundary)
            If (objTierBoundary.BaseOrTier = BaseOrTier) Then
                objRetTierBoundary = CType(Me.InnerList.Item(i), TierBoundary)
                Exit For
            End If
        Next
        Return objRetTierBoundary
    End Function

    Public Function GetForSeasonAndTOU(ByVal eSeason As Season, ByVal eTOU As TimeOfUse) As TierBoundaryCollection
        Dim objTierBoundaryColl As New TierBoundaryCollection
        Dim i As Integer
        Dim objTierBoundary As TierBoundary

        For i = 0 To (Me.InnerList.Count - 1)
            objTierBoundary = CType(Me.InnerList.Item(i), TierBoundary)
            If (objTierBoundary.Season = eSeason And objTierBoundary.TimeOfUse = eTOU) Then
                objTierBoundaryColl.Add(objTierBoundary)
            End If
        Next

        Return (objTierBoundaryColl)
    End Function

    Public Function GetFirstAvailable() As TierBoundaryCollection
        Dim objTierBoundaryColl As New TierBoundaryCollection
        Dim i As Integer
        Dim s As Integer
        Dim t As Integer
        Dim objTierBoundary As TierBoundary
        Dim eSeason As Season
        Dim eTOU As TimeOfUse
        Dim found As Boolean = False

        ' iterate through possible authored sets of boundaries for first available
        For s = Season.Undefined To Season.Winter

            For t = TimeOfUse.Undefined To TimeOfUse.OffPeak
                If (ExistsForSeasonAndTOU(CType(s, Season), CType(t, TimeOfUse))) Then
                    eSeason = CType(s, Season)
                    eTOU = CType(t, TimeOfUse)
                    found = True
                    Exit For
                End If
            Next

            If (found) Then
                Exit For
            End If

        Next

        If (found) Then
            For i = 0 To (Me.InnerList.Count - 1)
                objTierBoundary = CType(Me.InnerList.Item(i), TierBoundary)
                If (objTierBoundary.Season = eSeason And objTierBoundary.TimeOfUse = eTOU) Then
                    objTierBoundaryColl.Add(objTierBoundary)
                End If
            Next
        End If

        Return (objTierBoundaryColl)
    End Function

    Public Function GetFirstAvailableForSeason(ByVal eSeason As Season) As TierBoundaryCollection
        Dim objTierBoundaryColl As New TierBoundaryCollection
        Dim i As Integer
        Dim t As Integer
        Dim objTierBoundary As TierBoundary
        Dim eTOU As TimeOfUse
        Dim found As Boolean = False

        For t = TimeOfUse.Undefined To TimeOfUse.OffPeak
            If (ExistsForSeasonAndTOU(eSeason, CType(t, TimeOfUse))) Then
                eTOU = CType(t, TimeOfUse)
                found = True
                Exit For
            End If
        Next

        If (found) Then
            For i = 0 To (Me.InnerList.Count - 1)
                objTierBoundary = CType(Me.InnerList.Item(i), TierBoundary)
                If (objTierBoundary.Season = eSeason And objTierBoundary.TimeOfUse = eTOU) Then
                    objTierBoundaryColl.Add(objTierBoundary)
                End If
            Next
        End If

        Return (objTierBoundaryColl)
    End Function

    Public Function ExistsForSeasonAndTOU(ByVal eSeason As Season, ByVal eTOU As TimeOfUse) As Boolean
        Dim result As Boolean = False
        Dim i As Integer
        Dim objTierBoundary As TierBoundary

        For i = 0 To (Me.InnerList.Count - 1)
            objTierBoundary = CType(Me.InnerList.Item(i), TierBoundary)
            If (objTierBoundary.Season = eSeason And objTierBoundary.TimeOfUse = eTOU) Then
                result = True
                Exit For
            End If
        Next

        Return (result)
    End Function

    Public Function GetInclusiveTier(ByVal quantity As Double) As BaseOrTier
        Dim inclusiveTier As BaseOrTier = BaseOrTier.Undefined
        Dim i As Integer
        Dim objTierBoundary As TierBoundary= Nothing
        Dim found As Boolean

        For i = 0 To (Me.InnerList.Count - 1)
            objTierBoundary = CType(Me.InnerList.Item(i), TierBoundary)
            ' If the inclusive tier is exactly the same as the quantity, it is part of the tier; mganley
            If (objTierBoundary.Threshold >= quantity) Then
                inclusiveTier = objTierBoundary.BaseOrTier
                found = True
                Exit For
            End If
        Next

        If (Not found) Then inclusiveTier = CType(CInt(objTierBoundary.BaseOrTier) + 1, BaseOrTier)

        Return (inclusiveTier)
    End Function

    Public Sub Sort()
        Dim tierSorter As IComparer = New AscendingTierSorter
        Me.InnerList.Sort(tierSorter)
    End Sub

    Private Class AscendingTierSorter
        Implements IComparer

        Public Function Compare(ByVal x As Object, ByVal y As Object) As Integer Implements System.Collections.IComparer.Compare
            Dim tb As TierBoundary = CType(x, TierBoundary)
            Dim icA As IComparable = CType(tb.BaseOrTier, IComparable)
            Dim tb2 As TierBoundary = CType(y, TierBoundary)
            Dim icB As IComparable = CType(tb2.BaseOrTier, IComparable)

            ' swapping icB and icA makes it asc/desc switch
            Return (icA.CompareTo(icB))
        End Function

    End Class

End Class


''' <summary>
''' Information class to help with advanced tier boundary related needs.
''' </summary>
''' <remarks></remarks>
Public Class TierBoundaryInformation
    Private _listOfTierBoundaryCollections As TierBoundaryCollectionList
    Private _seasonFactorCollection As SeasonFactorCollection

    Public Sub New()
        _listOfTierBoundaryCollections = New TierBoundaryCollectionList()
    End Sub

    Public ReadOnly Property TierBoundariesExist() As Boolean
        Get
            If (Not _listOfTierBoundaryCollections Is Nothing AndAlso _listOfTierBoundaryCollections.Count > 0) Then
                Return (True)
            Else
                Return (False)
            End If
        End Get
    End Property

    Public ReadOnly Property TierBoundaryCollectionsList() As TierBoundaryCollectionList
        Get
            Return _listOfTierBoundaryCollections
        End Get
    End Property

    Public Function GetTierBoundaryCollectionBySeason(ByVal season As Season) As TierBoundaryCollection
        Dim p As TierBoundaryCollection = Nothing

        For Each tbc As TierBoundaryCollection In _listOfTierBoundaryCollections
            If (tbc.Count > 0) Then
                If (tbc(0).Season = season) Then
                    p = tbc
                    Exit For
                End If
            End If
        Next

        Return (p)
    End Function

    Public Property SeasonFactors() As SeasonFactorCollection
        Get
            Return (_seasonFactorCollection)
        End Get
        Set(value As SeasonFactorCollection)
            _seasonFactorCollection = value
        End Set
    End Property


    Public Function GetSeasonFactorValue(ByVal season As Season) As Double
        Dim val As Double = 1.0
        Dim sf As SeasonFactor

        If (Not _seasonFactorCollection Is Nothing) Then
            sf = _seasonFactorCollection.Item(season)
            If (Not sf Is Nothing) Then
                val = sf.Factor
            End If
        End If

        Return (val)
    End Function

End Class


''' <summary>
''' List of TierBoundaryCollection.
''' </summary>
''' <remarks></remarks>
Public Class TierBoundaryCollectionList
    Inherits System.Collections.Generic.List(Of TierBoundaryCollection)

End Class