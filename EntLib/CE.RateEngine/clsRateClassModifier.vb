﻿Imports CE.RateEngine.Enums

Public Class RateClassModifier
    Private _modifierType As RateClassModifierType
    Private _value As String

    Private Sub New()
    End Sub

    Public Sub New(ByVal modifierType As RateClassModifierType, ByVal value As String)
        _modifierType = modifierType
        _value = value
    End Sub

    Public ReadOnly Property ModifierType() As RateClassModifierType
        Get
            Return (_modifierType)
        End Get
    End Property

    Public ReadOnly Property Value() As String
        Get
            Return (_value)
        End Get
    End Property

End Class
