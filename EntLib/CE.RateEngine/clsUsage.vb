Imports CE.RateEngine.Enums

<Serializable()> Public Class Usage
    Inherits UsageBase

	Private m_eBaseOrTier As BaseOrTier
	Private m_eTimeOfUse As TimeOfUse
	Private m_eSeason As Season
	Private m_dblQuantity As Double
	Private m_sUseTypeName As String
    Private m_bProcessedUseTypeName As Boolean

	Private Sub New()
	End Sub

    Public Sub New(ByVal eBaseOrTier As BaseOrTier, ByVal eTimeOfUse As TimeOfUse, ByVal eSeason As Season, ByVal dblQuantity As Double)
        MyBase.New()
        AssignParts(eBaseOrTier, eTimeOfUse, eSeason, dblQuantity)
    End Sub

    Public Sub New(ByVal eBaseOrTier As BaseOrTier, ByVal eTimeOfUse As TimeOfUse, ByVal eSeason As Season, ByVal dblQuantity As Double, ByVal dtDatestamp As DateTime)
        MyBase.New()
        AssignParts(eBaseOrTier, eTimeOfUse, eSeason, dblQuantity)
        Datestamp = dtDatestamp
    End Sub

    Public Sub New(ByVal eBaseOrTier As BaseOrTier, ByVal eTimeOfUse As TimeOfUse, ByVal dblQuantity As Double)
        MyBase.New()
        AssignParts(eBaseOrTier, eTimeOfUse, Season.Undefined, dblQuantity)
    End Sub

    Public Sub New(ByVal eBaseOrTier As BaseOrTier, ByVal dblQuantity As Double)
        MyBase.New()
        AssignParts(eBaseOrTier, TimeOfUse.Undefined, Season.Undefined, dblQuantity)
    End Sub

    Public Sub New(ByVal eBaseOrTier As BaseOrTier, ByVal eSeason As Season, ByVal dblQuantity As Double)
        MyBase.New()
        AssignParts(eBaseOrTier, TimeOfUse.Undefined, eSeason, dblQuantity)
    End Sub

    Public Sub New(ByVal eTimeOfUse As TimeOfUse, ByVal eSeason As Season, ByVal dblQuantity As Double)
        MyBase.New()
        AssignParts(BaseOrTier.Undefined, eTimeOfUse, eSeason, dblQuantity)
    End Sub

    Public Sub New(ByVal eTimeOfUse As TimeOfUse, ByVal dblQuantity As Double)
        MyBase.New()
        AssignParts(BaseOrTier.Undefined, eTimeOfUse, Season.Undefined, dblQuantity)
    End Sub

    Public Sub New(ByVal dblQuantity As Double)
        MyBase.New()
        AssignParts(BaseOrTier.TotalServiceUse, TimeOfUse.Undefined, Season.Undefined, dblQuantity)
    End Sub

    ' constructors with LoadType
    Public Sub New(ByVal eLoadType As LoadType, ByVal dblQuantity As Double)
        MyBase.New(eLoadType)
        AssignParts(BaseOrTier.TotalServiceUse, TimeOfUse.Undefined, Season.Undefined, dblQuantity)
    End Sub

    Public Sub New(ByVal eLoadType As LoadType, ByVal eBaseOrTier As BaseOrTier, ByVal dblQuantity As Double)
        MyBase.New(eLoadType)
        AssignParts(eBaseOrTier, TimeOfUse.Undefined, Season.Undefined, dblQuantity)
    End Sub

    Public Sub New(ByVal eLoadType As LoadType, ByVal eBaseOrTier As BaseOrTier, ByVal eSeason As Season, ByVal dblQuantity As Double)
        MyBase.New(eLoadType)
        AssignParts(eBaseOrTier, TimeOfUse.Undefined, eSeason, dblQuantity)
    End Sub

    Public Sub New(ByVal eLoadType As LoadType, ByVal eBaseOrTier As BaseOrTier, ByVal eTimeOfUse As TimeOfUse, ByVal eSeason As Season, ByVal dblQuantity As Double)
        MyBase.New(eLoadType)
        AssignParts(eBaseOrTier, eTimeOfUse, eSeason, dblQuantity)
    End Sub


    Public Sub New(ByVal eLoadType As LoadType, ByVal eBaseOrTier As BaseOrTier, ByVal eTimeOfUse As TimeOfUse, ByVal eSeason As Season, ByVal dblQuantity As Double, ByVal dtDatestamp As DateTime)
        MyBase.New(eLoadType)
        AssignParts(eBaseOrTier, eTimeOfUse, eSeason, dblQuantity)
        Datestamp = dtDatestamp
    End Sub

	' The lookup for the UseTypeName mappings occurs in the functions that
	' accept UsageCollections.  This constructor simply stores the UseTypeName
    Public Sub New(ByVal sUseTypeName As String, ByVal dblQuantity As Double)
        MyBase.New()
        m_sUseTypeName = sUseTypeName
        m_dblQuantity = dblQuantity
    End Sub

    Public Sub New(ByVal u As Usage)
        Me.New(u.BaseOrTier, u.TimeOfUse, u.Season, u.Quantity)
    End Sub

#Region "Properties"

	Public Property Quantity() As Double
		Get
			Return m_dblQuantity
		End Get
		Set(ByVal Value As Double)
			m_dblQuantity = Value
		End Set
	End Property

	Public ReadOnly Property BaseOrTier() As BaseOrTier
		Get
			Return (m_eBaseOrTier)
		End Get
	End Property

	Public ReadOnly Property TimeOfUse() As TimeOfUse
		Get
			Return (m_eTimeOfUse)
		End Get
	End Property

	Public ReadOnly Property Season() As Season
		Get
			Return (m_eSeason)
		End Get
	End Property

	Public ReadOnly Property UseTypeName() As String
		Get
			Return (m_sUseTypeName)
		End Get
    End Property

    Public ReadOnly Property ProcessedUseTypeName() As Boolean
        Get
            Return (m_bProcessedUseTypeName)
        End Get
    End Property

#End Region

#Region "Public Functions"

    Public Function AssignPartsAfterLookup(ByVal eBaseOrTier As BaseOrTier, ByVal eTimeOfUse As TimeOfUse, ByVal eSeason As Season, ByVal eLoadType As LoadType, ByVal sVerifiedUseTypeName As String) As Boolean
        Dim bRetval As Boolean

        If (sVerifiedUseTypeName.ToUpper() = m_sUseTypeName.ToUpper()) Then
            m_eBaseOrTier = eBaseOrTier
            m_eTimeOfUse = eTimeOfUse
            m_eSeason = eSeason
            m_eLoadType = eLoadType
            m_bProcessedUseTypeName = True
            bRetval = True
        End If

        Return (bRetval)
    End Function

    ''' <summary>
    ''' Reassign an existing tier to the new tier.  Infrequently used.
    ''' </summary>
    ''' <param name="newTier"></param>
    ''' <remarks></remarks>
    Public Sub ReassignTier(ByVal newTier As BaseOrTier)
        m_eBaseOrTier = newTier
    End Sub

    ''' <summary>
    ''' Reassign an existing season to the new season.  Infrequently used.
    ''' </summary>
    ''' <param name="newSeason"></param>
    ''' <remarks></remarks>
    Public Sub ReassignSeason(ByVal newSeason As Season)
        m_eSeason = newSeason
    End Sub

    ''' <summary>
    ''' ReassignTOU
    ''' </summary>
    ''' <param name="newTOU"></param>
    Public Sub ReassignTOU(ByVal newTOU As TimeOfUse)
        m_eTimeOfUse = newTOU
    End Sub

#End Region

#Region "Private Functions"

    ' Called by constructors
    Private Function AssignParts(ByVal eBaseOrTier As BaseOrTier, ByVal eTimeOfUse As TimeOfUse, ByVal eSeason As Season, ByVal dblQuantity As Double) As Boolean
		Dim bRetval As Boolean = True

		m_eBaseOrTier = eBaseOrTier
		m_eTimeOfUse = eTimeOfUse
		m_eSeason = eSeason
		m_dblQuantity = dblQuantity
		m_sUseTypeName = String.Empty
        m_bProcessedUseTypeName = False

		Return (bRetval)
	End Function

#End Region

End Class
