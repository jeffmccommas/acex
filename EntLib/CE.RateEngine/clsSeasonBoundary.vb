Imports CE.RateEngine.Enums

Public Class SeasonBoundary
	Inherits BoundaryBase

	Private m_eSeason As Season
	Private m_sStartDay As String
	Private m_sEndDay As String

	Private Sub New()
	End Sub


	Public Sub New(ByVal eSeason As Season, ByVal sStartDay As String, ByVal sEndDay As String)
		MyBase.New()
		m_eSeason = eSeason
		m_sStartDay = sStartDay
		m_sEndDay = sEndDay
	End Sub

#Region "Properties"

	Public ReadOnly Property Season() As Season
		Get
			Return (m_eSeason)
		End Get
	End Property

	Public ReadOnly Property StartDay() As String
		Get
			Return (m_sStartDay)
		End Get
	End Property

	Public ReadOnly Property EndDay() As String
		Get
			Return (m_sEndDay)
		End Get
	End Property

#End Region

End Class



