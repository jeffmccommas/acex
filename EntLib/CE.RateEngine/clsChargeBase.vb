Imports CE.RateEngine.Enums

Public MustInherit Class ChargeBase

	Private m_eCostType As CostType
	Private m_dblChargeValue As Double

	' This allows classes that inherit to lock out default constructor
	Public Sub New()
	End Sub

	Public Sub New(ByVal eCostType As CostType, ByVal dblChargeValue As Double)
		m_eCostType = eCostType
		m_dblChargeValue = dblChargeValue
	End Sub

#Region "Properties"

	Public Property ChargeValue() As Double
		Get
			Return (m_dblChargeValue)
		End Get
		Set(ByVal Value As Double)
			m_dblChargeValue = Value
		End Set
	End Property

	Public ReadOnly Property CostType() As CostType
		Get
			Return (m_eCostType)
		End Get
	End Property

#End Region


End Class
