﻿Imports CE.RateEngine.Enums

<Serializable()> Public Class Baseline

    Private m_usageDate As DateTime
    Private m_baselineUsage As Double
    Private m_actualUsage As Double
    Private m_rebateAmount As Double
    Private m_actualUsageMissing As Boolean
    Private m_baselineMissing As Boolean
    Private m_rebateMissing As Boolean
    Private m_units As Integer  'zero is kWh, usually would be zero

    Public Sub New()
    End Sub

    Public Sub New(ByVal usageDate As DateTime, ByVal baselineUsage As Double, ByVal actualUsage As Double, _
                   ByVal rebateAmount As Double, ByVal actualUsageMissing As Boolean, _
                   ByVal baselineMissing As Boolean, ByVal rebateMissing As Boolean, ByVal units As Integer)
        m_usageDate = usageDate
        m_baselineUsage = baselineUsage
        m_actualUsage = actualUsage
        m_rebateAmount = rebateAmount
        m_actualUsageMissing = actualUsageMissing
        m_baselineMissing = baselineMissing
        m_rebateMissing = rebateMissing
        m_units = units
    End Sub

    Property UsageDate() As DateTime
        Get
            Return m_usageDate
        End Get
        Set(ByVal value As DateTime)
            m_usageDate = value
        End Set
    End Property

    Property BaselineUsage() As Double
        Get
            Return m_baselineUsage
        End Get
        Set(ByVal value As Double)
            m_baselineUsage = value
        End Set
    End Property

    Property ActualUsage() As Double
        Get
            Return m_actualUsage
        End Get
        Set(ByVal value As Double)
            m_actualUsage = value
        End Set
    End Property

    Property RebateAmount() As Double
        Get
            Return m_rebateAmount
        End Get
        Set(ByVal value As Double)
            m_rebateAmount = value
        End Set
    End Property


    Public Property ActualUsageMissing() As Boolean
        Get
            Return m_actualUsageMissing
        End Get
        Set(ByVal value As Boolean)
            m_actualUsageMissing = value
        End Set
    End Property

    Public Property BaselineMissing() As Boolean
        Get
            Return m_baselineMissing
        End Get
        Set(ByVal value As Boolean)
            m_baselineMissing = value
        End Set
    End Property

    Public Property RebateMissing() As Boolean
        Get
            Return m_rebateMissing
        End Get
        Set(ByVal value As Boolean)
            m_rebateMissing = value
        End Set
    End Property

    Public Property Units() As Integer
        Get
            Return m_units
        End Get
        Set(ByVal value As Integer)
            m_units = value
        End Set
    End Property

End Class
