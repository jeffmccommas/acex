Imports CE.RateEngine.Enums

Public Class MinimumChargeCollection
	Inherits CollectionBase

	Public Sub New()
	End Sub

	Default ReadOnly Property Item(ByVal index As Integer) As MinimumCharge
		Get
			Return CType(Me.InnerList.Item(index), MinimumCharge)
		End Get
	End Property

	Public Function Add(ByVal value As MinimumCharge) As Integer
		Return Me.InnerList.Add(value)
	End Function

	Public Sub Insert(ByVal index As Integer, ByVal value As MinimumCharge)
		Me.InnerList.Insert(index, value)
	End Sub

	Public Sub AddRange(ByVal values() As MinimumCharge)
		Me.InnerList.AddRange(values)
	End Sub

	Public Sub Remove(ByVal value As MinimumCharge)
		Me.InnerList.Remove(value)
	End Sub

	Public Function FindByCostTypeAndCalcType(ByVal ct As CostType, ByVal cct As ChargeCalcType) As MinimumCharge
		Dim i As Integer
        Dim objMinCharge As MinimumCharge = Nothing

		If (Me.InnerList.Count > 0) Then

			For i = 0 To Me.InnerList.Count - 1
				If CType(Me.InnerList.Item(i), MinimumCharge).CostType = ct And _
				 CType(Me.InnerList.Item(i), MinimumCharge).CalculationType = cct Then
					objMinCharge = CType(Me.InnerList.Item(i), MinimumCharge)
					Exit For
				End If
			Next

		End If

		Return (objMinCharge)
	End Function

End Class
