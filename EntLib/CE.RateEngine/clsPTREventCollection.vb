﻿Imports CE.RateEngine.Enums

<Serializable()> Public Class PTREventCollection
    Inherits CollectionBase

    Public Sub New()
    End Sub

    Default ReadOnly Property Item(ByVal index As Integer) As PTREvent
        Get
            Return CType(Me.InnerList.Item(index), PTREvent)
        End Get
    End Property

    Public Function Add(ByVal value As PTREvent) As Integer
        Return Me.InnerList.Add(value)
    End Function

    Public Sub Insert(ByVal index As Integer, ByVal value As PTREvent)
        Me.InnerList.Insert(index, value)
    End Sub

    Public Sub AddRange(ByVal values() As PTREvent)
        Me.InnerList.AddRange(values)
    End Sub

    Public Sub Remove(ByVal value As PTREvent)
        Me.InnerList.Remove(value)
    End Sub

    Public Function SumRebateRealSavingsUsage() As Double
        Dim total As Double = 0
        Dim i As Integer
        Dim e As PTREvent

        If (Me.InnerList.Count > 0) Then
            For i = 0 To (Me.InnerList.Count - 1)
                e = CType(Me.InnerList.Item(i), PTREvent)
                total = total + e.Rebate.RealSavingsUsage
            Next
        End If

        Return (total)
    End Function

    Public Function SumRebateAmounts() As Double
        Dim total As Double = 0
        Dim i As Integer
        Dim e As PTREvent

        If (Me.InnerList.Count > 0) Then
            For i = 0 To (Me.InnerList.Count - 1)
                e = CType(Me.InnerList.Item(i), PTREvent)
                total = total + e.Rebate.RebateAmount
            Next
        End If

        Return (total)
    End Function

End Class
