Imports CE.RateEngine.Enums

Public Class RateClassInfo

	Private m_sName As String
	Private m_sDescription As String
	Private m_sEligibility As String

	Public Sub New()
		m_sName = String.Empty
		m_sDescription = String.Empty
		m_sEligibility = String.Empty
	End Sub

#Region "Properties"

	Public Property Name() As String
		Get
			Return m_sName
		End Get
		Set(ByVal Value As String)
			m_sName = Value
		End Set
	End Property

	Public Property Description() As String
		Get
			Return m_sDescription
		End Get
		Set(ByVal Value As String)
			m_sDescription = Value
		End Set
	End Property

	Public Property Eligibility() As String
		Get
			Return m_sEligibility
		End Get
		Set(ByVal Value As String)
			m_sEligibility = Value
		End Set
	End Property

#End Region

End Class
