﻿Imports CE.RateEngine.Enums

<Serializable()> Public Class ReadingPriceDetail

    Private m_name As String        'example: Tier1-OnPeak-Summer
    Private m_price As Double
    Private m_baseOrTier As BaseOrTier
    Private m_timeOfuse As TimeOfUse
    Private m_season As Season

    Private Sub New()
    End Sub

    Public Sub New(ByVal name As String, ByVal price As Double)
        m_name = name
        m_price = price
        m_baseOrTier = Enums.BaseOrTier.Undefined
        m_timeOfuse = Enums.TimeOfUse.Undefined
        m_season = Enums.Season.Undefined
    End Sub

    Public Sub New(ByVal b As BaseOrTier, ByVal t As TimeOfUse, ByVal s As Season, ByVal price As Double)
        Dim sb As New System.Text.StringBuilder()
        sb.Append(b.ToString())
        sb.Append("-")
        sb.Append(t.ToString())
        sb.Append("-")
        sb.Append(s.ToString())
        m_name = sb.ToString()
        m_price = price
        m_baseOrTier = b
        m_timeOfuse = t
        m_season = s
    End Sub

    Public Sub New(ByVal b As BaseOrTier, ByVal t As TimeOfUse, ByVal s As Season, ByVal extra As String, ByVal price As Double)
        Dim sb As New System.Text.StringBuilder()
        sb.Append(b.ToString())
        sb.Append("-")
        sb.Append(t.ToString())
        sb.Append("-")
        sb.Append(s.ToString())
        sb.Append("-")
        sb.Append(extra)
        m_name = sb.ToString()
        m_price = price
        m_baseOrTier = b
        m_timeOfuse = t
        m_season = s
    End Sub

#Region "Properties"

    Public Property Name() As String
        Get
            Return (m_name)
        End Get
        Set(ByVal Value As String)
            m_name = Value
        End Set
    End Property

    Public Property Price() As Double
        Get
            Return (m_price)
        End Get
        Set(ByVal Value As Double)
            m_price = Value
        End Set
    End Property

    Public Property BaseOrTier() As BaseOrTier
        Get
            Return (m_baseOrTier)
        End Get
        Set(ByVal Value As BaseOrTier)
            m_baseOrTier = Value
        End Set
    End Property

    Public Property TimeOfUse() As TimeOfUse
        Get
            Return (m_timeOfuse)
        End Get
        Set(ByVal Value As TimeOfUse)
            m_timeOfuse = Value
        End Set
    End Property

    Public Property Season() As Season
        Get
            Return (m_season)
        End Get
        Set(ByVal Value As Season)
            m_season = Value
        End Set
    End Property

#End Region

End Class
