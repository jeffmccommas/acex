﻿Imports CE.RateEngine.Enums

Public Class MasterChildCollection
    Inherits CollectionBase

    Public Sub New()
    End Sub

    Default ReadOnly Property Item(ByVal index As Integer) As MasterChild
        Get
            Return CType(Me.InnerList.Item(index), MasterChild)
        End Get
    End Property

    Public Function Add(ByVal value As MasterChild) As Integer
        Return Me.InnerList.Add(value)
    End Function

    Public Sub Insert(ByVal index As Integer, ByVal value As MasterChild)
        Me.InnerList.Insert(index, value)
    End Sub

    Public Sub AddRange(ByVal values() As MasterChild)
        Me.InnerList.AddRange(values)
    End Sub

    Public Sub Remove(ByVal value As MasterChild)
        Me.InnerList.Remove(value)
    End Sub

    Public Function DoesLoadTypeExist(ByVal eLoadType As LoadType) As Boolean
        Dim i As Integer
        Dim objTemp As MasterChild
        Dim bExists As Boolean = False

        If (Me.InnerList.Count > 0) Then

            For i = 0 To Me.InnerList.Count - 1
                objTemp = CType(Me.InnerList.Item(i), MasterChild)
                If (objTemp.LoadType = eLoadType) Then
                    bExists = True
                    Exit For
                End If
            Next

        End If

        Return (bExists)
    End Function

    Public Function GetRateClassForLoadType(ByVal eLoadType As LoadType) As String
        Dim i As Integer
        Dim objTemp As MasterChild
        Dim sRateClass As String = String.Empty

        If (Me.InnerList.Count > 0) Then

            For i = 0 To Me.InnerList.Count - 1
                objTemp = CType(Me.InnerList.Item(i), MasterChild)
                If (objTemp.LoadType = eLoadType) Then
                    sRateClass = objTemp.RateClass
                    Exit For
                End If
            Next

        End If

        Return (sRateClass)
    End Function

End Class
