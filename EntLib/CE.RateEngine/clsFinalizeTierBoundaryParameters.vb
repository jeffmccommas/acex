﻿Imports CE.RateEngine.Enums

Public Class FinalizeTierBoundaryParameters

    Public Property RateCompanyID As Integer
    Public Property RateDefID As Integer
    Public Property RateMasterID As Integer
    Public Property TierBoundaries As TierBoundaryCollection
    Public Property TierStructureType As TierStructureType
    Public Property RateClass As String
    Public Property RateClassModifiers As RateClassModifierCollection
    Public Property Season As Season
    Public Property TimeOfUse As TimeOfUse
    Public Property DwellingUnits As Integer
    Public Property BillDays As Integer
    Public Property Usages As UsageCollection
    Public Property SeasonScaleFactor As Double

    Public Sub New()
    End Sub

End Class
