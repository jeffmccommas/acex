Imports CE.RateEngine.Enums

Public Class Cost

	Private m_eCostType As CostType
	Private m_dblAmount As Double

	Public Sub New()
	End Sub

	Public Sub New(ByVal eCostType As CostType, ByVal dblAmount As Double)
		m_eCostType = eCostType
		m_dblAmount = dblAmount
	End Sub

#Region "Properties"

	Public Property Amount() As Double
		Get
			Return m_dblAmount
		End Get
		Set(ByVal Value As Double)
			m_dblAmount = Value
		End Set
	End Property

	Public ReadOnly Property CostType() As CostType
		Get
			Return (m_eCostType)
		End Get
	End Property

#End Region

End Class
