
<Serializable()> Public Class PriceCollection
    Inherits CollectionBase
    Implements ICloneable

    Public Function Clone() As Object Implements System.ICloneable.Clone
        Dim bf As System.Runtime.Serialization.Formatters.Binary.BinaryFormatter = _
               New System.Runtime.Serialization.Formatters.Binary.BinaryFormatter
        Dim ms As System.IO.MemoryStream = New System.IO.MemoryStream
        Dim obj As PriceCollection

        bf.Serialize(ms, Me)
        ms.Seek(0, System.IO.SeekOrigin.Begin)
        obj = CType(bf.Deserialize(ms), PriceCollection)
        ms.Close()

        Return (obj)
    End Function

    Default ReadOnly Property Item(ByVal index As Integer) As Price
        Get
            Return CType(Me.InnerList.Item(index), Price)
        End Get
    End Property

    Public Function Add(ByVal value As Price) As Integer
        Return Me.InnerList.Add(value)
    End Function

    Public Sub Insert(ByVal index As Integer, ByVal value As Price)
        Me.InnerList.Insert(index, value)
    End Sub

    Public Sub AddRange(ByVal values() As Price)
        Me.InnerList.AddRange(values)
    End Sub

    Public Sub Remove(ByVal value As Price)
        Me.InnerList.Remove(value)
    End Sub

    Public Sub Sort()
        Dim objSorter As IComparer = New DateSorter
        Me.InnerList.Sort(objSorter)
    End Sub

    Public Sub Sort(ByVal bReverse As Boolean)
        Dim objSorter As IComparer = New DateSorter(bReverse)
        Me.InnerList.Sort(objSorter)
    End Sub

    Public Function FillMissingPrices() As Boolean
        Dim bRetVal As Boolean = True
        Dim dtStartDate, dtEndDate As DateTime

        If (Me.InnerList.Count > 0) Then
            dtStartDate = CType(Me.InnerList.Item(0), Price).DateStamp
            dtEndDate = CType(Me.InnerList.Item(Me.InnerList.Count - 1), Price).DateStamp
            FillMissingPrices(dtStartDate, dtEndDate)
        End If

        Return (bRetVal)
    End Function

    Public Function FillMissingPrices(ByVal dtStartDate As DateTime, ByVal dtEndDate As DateTime) As Boolean
        Dim i, ii, readingIndex, nCount, nMaxDays As Integer
        Dim bRetVal As Boolean = True
        Dim dtTemp As DateTime
        Dim objPrice As Price

        ' 24 Hours for each Day
        nCount = Me.InnerList.Count

        nMaxDays = dtEndDate.Subtract(dtStartDate).Days + 1
        For i = 0 To (nMaxDays - 1)
            dtTemp = dtStartDate.AddDays(i)

            For ii = 0 To 23
                readingIndex = (i * 24) + ii

                If (readingIndex < nCount) Then
                    objPrice = CType(Me.InnerList.Item(readingIndex), Price)

                    If (dtTemp.Day <> objPrice.DateStamp.Day Or _
                    dtTemp.AddHours(ii).Hour <> objPrice.DateStamp.Hour Or _
                    dtTemp.Month <> objPrice.DateStamp.Month Or _
                    dtTemp.Year <> objPrice.DateStamp.Year) Then
                        ' Day or hour does not match, so insert at this index
                        ' insert if month doesn't  match
                        Me.InnerList.Insert(readingIndex, New Price(True, dtTemp.AddHours(ii)))
                        ' Insert into inner list moves the list up one notch, hence increase the count
                        nCount = nCount + 1
                    End If
                Else
                    Me.InnerList.Add(New Price(True, dtTemp.AddHours(ii)))
                    nCount = nCount + 1
                End If

            Next
        Next

        Return (bRetVal)
    End Function


    Private Class DateSorter
        Implements IComparer

        Private m_bReverse As Boolean

		Public Sub New()
		End Sub

        Public Sub New(ByVal bReverse As Boolean)
            m_bReverse = bReverse
        End Sub

        'This is used by some solutions; do not remove
        Public Property Reverse() As Boolean
            Get
                Return m_bReverse
            End Get
            Set(ByVal Value As Boolean)
                m_bReverse = Value
            End Set
        End Property

        Public Function Compare(ByVal x As Object, ByVal y As Object) As Integer Implements System.Collections.IComparer.Compare
            Dim i As Integer
            Dim tb As Price = CType(x, Price)
            Dim icA As IComparable = CType(tb.DateStamp, IComparable)
            Dim tb2 As Price = CType(y, Price)
            Dim icB As IComparable = CType(tb2.DateStamp, IComparable)

            ' swapping icB and icA makes it asc/desc switch
            If (Not m_bReverse) Then
                i = icA.CompareTo(icB)
            Else
                i = icB.CompareTo(icA)
            End If

            Return (i)
        End Function

    End Class


    Public Function GetPricesForDate(ByVal dtDate As DateTime) As PriceCollection
        Dim objPrices As PriceCollection = Nothing
        Dim objP As Price
        Dim i As Integer
        Dim bFound As Boolean

        If (Me.InnerList.Count > 0) Then

            For i = 0 To (Me.InnerList.Count - 1)
                objP = CType(Me.InnerList.Item(i), Price)
                If (objP.DateStamp.Day = dtDate.Day And _
                objP.DateStamp.Month = dtDate.Month And _
                objP.DateStamp.Year = dtDate.Year) Then
                    If (Not bFound) Then
                        objPrices = New PriceCollection
                        bFound = True
                    End If
                    objPrices.Add(New Price(objP.IsEmpty, objP.DateStamp, objP.Price))
                End If
            Next

        End If

        Return (objPrices)
    End Function

    Public Function EmptyCount() As Integer
        Dim nCount As Integer
        Dim i As Integer

        For i = 0 To (Me.InnerList.Count - 1)
            If (CType(Me.InnerList.Item(i), Price).IsEmpty) Then
                nCount = nCount + 1
            End If
        Next

        Return (nCount)
    End Function

End Class