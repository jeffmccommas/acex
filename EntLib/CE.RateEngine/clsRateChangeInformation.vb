Imports CE.RateEngine.Enums

Public Class RateChangeInformation

	Private m_bRateChange As Boolean
	Private m_bSeasonalRateChange As Boolean

	Public Sub New()
        'm_bRateChange = False
        'm_bSeasonalRateChange = False
	End Sub

	Public Sub New(ByVal bRateChange As Boolean, ByVal bSeasonalRateChange As Boolean)
		m_bRateChange = bRateChange
		m_bSeasonalRateChange = bSeasonalRateChange
	End Sub

#Region "Properties"

	Public Property RateChange() As Boolean
		Get
			Return m_bRateChange
		End Get
		Set(ByVal Value As Boolean)
			m_bRateChange = Value
		End Set
	End Property

	Public Property SeasonalRateChange() As Boolean
		Get
			Return m_bSeasonalRateChange
		End Get
		Set(ByVal Value As Boolean)
			m_bSeasonalRateChange = Value
		End Set
	End Property

#End Region

End Class
