Imports CE.RateEngine.Enums

Public Class SeasonFactor
	Private m_eSeason As Season
	Private m_dblFactor As Double
    Private m_dblDays As Integer

	Public Sub New()
	End Sub

	Public Sub New(ByVal eSeason As Season, ByVal dblFactor As Double)
		m_eSeason = eSeason
		m_dblFactor = dblFactor
    End Sub

    Public Sub New(ByVal eSeason As Season, ByVal dblFactor As Double, ByVal days As Integer)
        m_eSeason = eSeason
        m_dblFactor = dblFactor
        m_dblDays = days
    End Sub

#Region "Properties"

	Public Property Season() As Season
		Get
			Return (m_eSeason)
		End Get
		Set(ByVal Value As Season)
			m_eSeason = Value
		End Set
	End Property

	Public Property Factor() As Double
		Get
			Return (m_dblFactor)
		End Get
		Set(ByVal Value As Double)
			m_dblFactor = Value
		End Set
	End Property

    ''' <summary>
    ''' Optional detail that is often not used.
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property Days() As Integer
        Get
            Return (m_dblDays)
        End Get
        Set(ByVal Value As Integer)
            m_dblDays = Value
        End Set
    End Property

#End Region

End Class
