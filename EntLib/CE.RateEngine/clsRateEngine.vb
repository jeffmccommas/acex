Imports CE.RateEngine.Enums
Imports System.Collections.Generic

Partial Public Class RateEngine
    Private Const knDefaultBillDays As Integer = 30
    Private Const ksIllegalProrateError As String = "Prorate value is illegal."
    Private Const ksNoValidUseChargesError As String = "Use Charges and Usages did not correspond and could not be processed."
    Private Const ksBadDate As String = "Date is Nothing.  Cannot process date range for seasonal rate."
    Private Const ksBadFCRDate As String = "Date is Nothing.  Cannot process date range for FCR rate."
    Private Const ksBadRTPDate As String = "Date is Nothing.  Cannot process date range for RTP rate."
    Private Const ksSeasonalBoundaryError As String = "Seasonal boundary error."
    Private Const ksFCRError As String = "FCR prorate error."
    Private Const ksTierBoundaryError As String = "Tier boundary error."
    Private Const ksRateClassParsingError As String = "Parsing of RateClass could not be completed."
    Private Const ksBaseTerritoryParsingError As String = "Parsing of Code and Territory from RateClass could not be completed."
    Private Const ksRateClassBreakCharPrimary As String = "_"
    Private Const ksRateClassBreakCharSecondary As String = "|"
    Private Const knRecursionEntryLevel As Integer = 0
    Private Const knMaximumRecursionLevel As Integer = 2
    Private Const TierStructureHybrid1_Base As Integer = 30
    Private Const TierStructureHybrid1_Min As Integer = 27
    Private Const TierStructureHybrid1_Max As Integer = 34
    Private Const ksOriginalUsageCollection As String = "Usages before NEM Tiers Adjustment"
    Private Const ksNEMTierAdjustedUsageCollection As String = "Usages after NEM Tiers Adjustment"

    Private m_sRateEngineConnectionString As String = String.Empty
    Private m_bLogging As Boolean
    Private m_LogSessionID As System.Guid
    Private m_dataSourceType As Enums.DataSourceType = DataSourceType.Database
    Private m_cachingType As Enums.CachingType = CachingType.NoCache
    Private m_xmlFilesPath As String = String.Empty
    Private m_cacheTimeoutInMinutes As Integer

    ''' <summary>
    ''' Instantiate Rate Engine.
    ''' All settings read from app settings.
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub New()
        Dim recs As New RateEngineConfigSettings()

        m_sRateEngineConnectionString = recs.RateEngineConnectionString
        m_dataSourceType = recs.DataSourceType
        m_xmlFilesPath = recs.XmlFilesPath
        m_cachingType = recs.CacheType
        m_cacheTimeoutInMinutes = recs.CacheTimeoutInMinutes
        m_bLogging = recs.LoggingEnabled

        BeginLogSession()
    End Sub

    ''' <summary>
    ''' Instantiate Rate Engine.  
    ''' Backward compatible.  Settings read from app settings.  Connection string overridden by caller.
    ''' </summary>
    ''' <param name="sRateEngineConnectionString"></param>
    ''' <remarks></remarks>
    Public Sub New(ByVal sRateEngineConnectionString As String)
        Dim recs As New RateEngineConfigSettings()

        m_sRateEngineConnectionString = sRateEngineConnectionString

        m_dataSourceType = recs.DataSourceType
        m_cachingType = recs.CacheType
        m_cacheTimeoutInMinutes = recs.CacheTimeoutInMinutes
        m_xmlFilesPath = recs.XmlFilesPath
        m_bLogging = recs.LoggingEnabled
    End Sub

    ''' <summary>
    ''' Instantiate Rate Engine.
    ''' Backward compatible. Settings read from app settings.  Connection string and logging indicator overridden by caller.
    ''' </summary>
    ''' <param name="sRateEngineConnectionString"></param>
    ''' <param name="bLoggingEnabled"></param>
    ''' <remarks></remarks>
    Public Sub New(ByVal sRateEngineConnectionString As String, ByVal bLoggingEnabled As Boolean)
        Dim recs As New RateEngineConfigSettings()

        m_sRateEngineConnectionString = sRateEngineConnectionString
        m_bLogging = bLoggingEnabled

        m_dataSourceType = recs.DataSourceType
        m_cachingType = recs.CacheType
        m_cacheTimeoutInMinutes = recs.CacheTimeoutInMinutes
        m_xmlFilesPath = recs.XmlFilesPath

        BeginLogSession()
    End Sub

    ''' <summary>
    ''' Instantiate Rate Engine.
    ''' Settings object provided.
    ''' </summary>
    ''' <param name="recs"></param>
    ''' <remarks></remarks>
    Public Sub New(ByVal recs As IRateEngineConfigSettings)

        m_sRateEngineConnectionString = recs.RateEngineConnectionString
        m_dataSourceType = recs.DataSourceType
        m_xmlFilesPath = recs.XmlFilesPath
        m_cachingType = recs.CacheType
        m_cacheTimeoutInMinutes = recs.CacheTimeoutInMinutes
        m_bLogging = recs.LoggingEnabled

        BeginLogSession()
    End Sub


#Region "Properties"

    Public ReadOnly Property RateEngineConnectionString() As String
        Get
            Return (m_sRateEngineConnectionString)
        End Get
    End Property

    Public ReadOnly Property IsLoggingEnabled() As Boolean
        Get
            Return (m_bLogging)
        End Get
    End Property

    Public ReadOnly Property LogSessionID() As System.Guid
        Get
            Return (m_LogSessionID)
        End Get
    End Property

    Public ReadOnly Property CachingType() As Enums.CachingType
        Get
            Return (m_cachingType)
        End Get
    End Property

    Public ReadOnly Property DataSourceType() As Enums.DataSourceType
        Get
            Return (m_dataSourceType)
        End Get
    End Property

    Public ReadOnly Property XmlFilesPath() As String
        Get
            Return (m_xmlFilesPath)
        End Get
    End Property

    Public ReadOnly Property CacheTimeoutInMinutes() As Integer
        Get
            Return (m_cacheTimeoutInMinutes)
        End Get
    End Property

    'Public Property RccRateEngineBinnDate() As Boolean
    '    Get
    '        Return m_nRateEngineRccBinnDate
    '    End Get
    '    Set(ByVal Value As Boolean)
    '        m_nRateEngineRccBinnDate = Value
    '    End Set
    'End Property

    Private Property avgUsagePerDay As Double

#End Region


#Region "GetRateClassList Overloads"

    Public Function GetRateClassList(ByVal nRateCompanyID As Integer, ByVal dtStartDate As DateTime, ByVal dtEndDate As DateTime) As SimpleRateDefinitionCollection
        Return (MasterGetRateClassList(nRateCompanyID, dtStartDate, dtEndDate, New GetRateClassListExtParam, False))
    End Function

    Public Function GetRateClassList(ByVal nRateCompanyID As Integer, ByVal dtStartDate As DateTime, ByVal dtEndDate As DateTime, ByVal objExtParams As GetRateClassListExtParam) As SimpleRateDefinitionCollection
        Return (MasterGetRateClassList(nRateCompanyID, dtStartDate, dtEndDate, objExtParams, True))
    End Function

    Public Function GetRateClassList(ByVal nRateCompanyID As Integer, ByVal nBillDays As Integer, ByVal dtEndDate As DateTime) As SimpleRateDefinitionCollection
        Dim dtStartDate As DateTime
        dtStartDate = dtEndDate.AddDays(-nBillDays + 1)
        Return (MasterGetRateClassList(nRateCompanyID, dtStartDate, dtEndDate, New GetRateClassListExtParam, False))
    End Function

    Public Function GetRateClassList(ByVal nRateCompanyID As Integer, ByVal nBillDays As Integer, ByVal dtEndDate As DateTime, ByVal objExtParams As GetRateClassListExtParam) As SimpleRateDefinitionCollection
        Dim dtStartDate As DateTime
        dtStartDate = dtEndDate.AddDays(-nBillDays + 1)
        Return (MasterGetRateClassList(nRateCompanyID, dtStartDate, dtEndDate, objExtParams, True))
    End Function

    Public Function GetRateClassList(ByVal sServiceTerritoryID As String) As SimpleRateDefinitionCollection
        Return (MasterGetRateClassList(sServiceTerritoryID, New GetRateClassListExtParam, False))
    End Function

    Public Function GetRateClassList(ByVal sServiceTerritoryID As String, ByVal objExtParams As GetRateClassListExtParam) As SimpleRateDefinitionCollection
        Return (MasterGetRateClassList(sServiceTerritoryID, objExtParams, True))
    End Function

    Public Function GetRateClassList(ByVal nRateCompanyID As Integer, ByVal sServiceTerritoryID As String) As SimpleRateDefinitionCollection
        Return (MasterGetRateClassList(nRateCompanyID, sServiceTerritoryID, New GetRateClassListExtParam, False))
    End Function

    Public Function GetRateClassList(ByVal nRateCompanyID As Integer, ByVal sServiceTerritoryID As String, ByVal objExtParams As GetRateClassListExtParam) As SimpleRateDefinitionCollection
        Return (MasterGetRateClassList(nRateCompanyID, sServiceTerritoryID, objExtParams, True))
    End Function

    ' Private Helper Functions for GetRateClassList overloads
    Private Function MasterGetRateClassList(ByVal nRateCompanyID As Integer, ByVal dtStartDate As DateTime, ByVal dtEndDate As DateTime, ByVal objExtParams As GetRateClassListExtParam, ByVal bUseExtendedParams As Boolean) As SimpleRateDefinitionCollection
        Dim objDC As New dctlRateEngine(m_sRateEngineConnectionString, m_xmlFilesPath, m_dataSourceType, m_cachingType, m_cacheTimeoutInMinutes)
        Dim objDefinitions As SimpleRateDefinitionCollection

        If (Not bUseExtendedParams) Then
            objDefinitions = objDC.GetRateClassList(nRateCompanyID, dtStartDate, dtEndDate, Language.English)
        Else
            objDefinitions = objDC.GetRateClassList(nRateCompanyID, dtStartDate, dtEndDate, objExtParams.Language, objExtParams.CustomerType, objExtParams.FuelType, objExtParams.DefaultOnly)
        End If

        Return (objDefinitions)
    End Function

    Private Function MasterGetRateClassList(ByVal sServiceTerritory As String, ByVal objExtParams As GetRateClassListExtParam, ByVal bUseExtendedParams As Boolean) As SimpleRateDefinitionCollection
        Dim objDC As New dctlRateEngine(m_sRateEngineConnectionString, m_xmlFilesPath, m_dataSourceType, m_cachingType, m_cacheTimeoutInMinutes)
        Dim objDefinitions As SimpleRateDefinitionCollection

        If (Not bUseExtendedParams) Then
            objDefinitions = objDC.GetRateClassList(sServiceTerritory, Language.English)
        Else
            objDefinitions = objDC.GetRateClassList(sServiceTerritory, objExtParams.Language, objExtParams.CustomerType, objExtParams.FuelType, objExtParams.DefaultOnly)
        End If

        Return (objDefinitions)
    End Function

    Private Function MasterGetRateClassList(ByVal nRateCompanyID As Integer, ByVal sServiceTerritory As String, ByVal objExtParams As GetRateClassListExtParam, ByVal bUseExtendedParams As Boolean) As SimpleRateDefinitionCollection
        Dim objDC As New dctlRateEngine(m_sRateEngineConnectionString, m_xmlFilesPath, m_dataSourceType, m_cachingType, m_cacheTimeoutInMinutes)
        Dim objDefinitions As SimpleRateDefinitionCollection

        If (Not bUseExtendedParams) Then
            objDefinitions = objDC.GetRateClassList(nRateCompanyID, sServiceTerritory, Language.English)
        Else
            objDefinitions = objDC.GetRateClassList(nRateCompanyID, sServiceTerritory, objExtParams.Language, objExtParams.CustomerType, objExtParams.FuelType, objExtParams.DefaultOnly)
        End If

        Return (objDefinitions)
    End Function

#End Region

#Region "GetSimpleRateInformation Overloads"

    Public Function GetSimpleRateInformation(ByVal nRateCompanyID As Integer, ByVal sRateClass As String, ByVal dtStartDate As DateTime, ByVal dtEndDate As DateTime) As SimpleRateDefinitionCollection
        Return (MasterGetSimpleRateInformation(nRateCompanyID, sRateClass, dtStartDate, dtEndDate, Language.English))
    End Function

    Public Function GetSimpleRateInformation(ByVal nRateCompanyID As Integer, ByVal sRateClass As String, ByVal dtStartDate As DateTime, ByVal dtEndDate As DateTime, ByVal eLanguage As Language) As SimpleRateDefinitionCollection
        Return (MasterGetSimpleRateInformation(nRateCompanyID, sRateClass, dtStartDate, dtEndDate, eLanguage))
    End Function

    Public Function GetSimpleRateInformation(ByVal nRateCompanyID As Integer, ByVal sRateClass As String, ByVal nBillDays As Integer, ByVal dtEndDate As DateTime) As SimpleRateDefinitionCollection
        Dim dtStartDate As DateTime
        dtStartDate = dtEndDate.AddDays(-nBillDays + 1)
        Return (MasterGetSimpleRateInformation(nRateCompanyID, sRateClass, dtStartDate, dtEndDate, Language.English))
    End Function

    Public Function GetSimpleRateInformation(ByVal nRateCompanyID As Integer, ByVal sRateClass As String, ByVal nBillDays As Integer, ByVal dtEndDate As DateTime, ByVal eLanguage As Language) As SimpleRateDefinitionCollection
        Dim dtStartDate As DateTime
        dtStartDate = dtEndDate.AddDays(-nBillDays + 1)
        Return (MasterGetSimpleRateInformation(nRateCompanyID, sRateClass, dtStartDate, dtEndDate, eLanguage))
    End Function

    Public Function GetSimpleRateInformation(ByVal nRateCompanyID As Integer, ByVal sRateClass As String) As SimpleRateDefinitionCollection
        Return (MasterGetSimpleRateInformation(nRateCompanyID, sRateClass, Language.English))
    End Function

    Public Function GetSimpleRateInformation(ByVal nRateCompanyID As Integer, ByVal sRateClass As String, ByVal eLanguage As Language) As SimpleRateDefinitionCollection
        Return (MasterGetSimpleRateInformation(nRateCompanyID, sRateClass, eLanguage))
    End Function

    Public Function GetSimpleRateInformation(ByVal nRateDefinitionID As Integer) As SimpleRateDefinition
        Return (MasterGetSimpleRateInformation(nRateDefinitionID, Language.English))
    End Function

    Public Function GetSimpleRateInformation(ByVal nRateDefinitionID As Integer, ByVal eLanguage As Language) As SimpleRateDefinition
        Return (MasterGetSimpleRateInformation(nRateDefinitionID, eLanguage))
    End Function

    ' Private Helper Functions for MasterGetSimpleRateInformation overloads
    Private Function MasterGetSimpleRateInformation(ByVal nRateCompanyID As Integer, ByVal sRateClass As String, ByVal dtStartDate As DateTime, ByVal dtEndDate As DateTime, ByVal eLanguage As Language) As SimpleRateDefinitionCollection
        Dim objDC As New dctlRateEngine(m_sRateEngineConnectionString, m_xmlFilesPath, m_dataSourceType, m_cachingType, m_cacheTimeoutInMinutes)
        Dim objDefinitions As SimpleRateDefinitionCollection

        objDefinitions = objDC.GetSimpleRateInformation(nRateCompanyID, GetBaseRateClass(nRateCompanyID, sRateClass), dtStartDate, dtEndDate, eLanguage)

        Return (objDefinitions)
    End Function

    Private Function MasterGetSimpleRateInformation(ByVal nRateCompanyID As Integer, ByVal sRateClass As String, ByVal eLanguage As Language) As SimpleRateDefinitionCollection
        Dim objDC As New dctlRateEngine(m_sRateEngineConnectionString, m_xmlFilesPath, m_dataSourceType, m_cachingType, m_cacheTimeoutInMinutes)
        Dim objDefinitions As SimpleRateDefinitionCollection

        objDefinitions = objDC.GetSimpleRateInformation(nRateCompanyID, GetBaseRateClass(nRateCompanyID, sRateClass), eLanguage)

        Return (objDefinitions)
    End Function

    Private Function MasterGetSimpleRateInformation(ByVal nRateDefinitionID As Integer, ByVal eLanguage As Language) As SimpleRateDefinition
        Dim objDC As New dctlRateEngine(m_sRateEngineConnectionString, m_xmlFilesPath, m_dataSourceType, m_cachingType, m_cacheTimeoutInMinutes)
        Dim objDefinition As SimpleRateDefinition

        objDefinition = objDC.GetSimpleRateInformation(nRateDefinitionID, eLanguage)

        Return (objDefinition)
    End Function


#End Region

#Region "GetDetailedRateInformation Overloads"

    Public Function GetDetailedRateInformation(ByVal nRateCompanyID As Integer, ByVal sRateClass As String, ByVal dtStartDate As DateTime, ByVal dtEndDate As DateTime) As DetailedRateDefinitionCollection
        Return (MasterGetDetailedRateInformation(nRateCompanyID, sRateClass, dtStartDate, dtEndDate, Language.English))
    End Function

    Public Function GetDetailedRateInformation(ByVal nRateCompanyID As Integer, ByVal sRateClass As String, ByVal dtStartDate As DateTime, ByVal dtEndDate As DateTime, ByVal eLanguage As Language) As DetailedRateDefinitionCollection
        Return (MasterGetDetailedRateInformation(nRateCompanyID, sRateClass, dtStartDate, dtEndDate, eLanguage))
    End Function

    Public Function GetDetailedRateInformation(ByVal nRateCompanyID As Integer, ByVal sRateClass As String, ByVal nBillDays As Integer, ByVal dtEndDate As DateTime) As DetailedRateDefinitionCollection
        Dim dtStartDate As DateTime
        dtStartDate = dtEndDate.AddDays(-nBillDays + 1)
        Return (MasterGetDetailedRateInformation(nRateCompanyID, sRateClass, dtStartDate, dtEndDate, Language.English))
    End Function

    Public Function GetDetailedRateInformation(ByVal nRateCompanyID As Integer, ByVal sRateClass As String, ByVal nBillDays As Integer, ByVal dtEndDate As DateTime, ByVal eLanguage As Language) As DetailedRateDefinitionCollection
        Dim dtStartDate As DateTime
        dtStartDate = dtEndDate.AddDays(-nBillDays + 1)
        Return (MasterGetDetailedRateInformation(nRateCompanyID, sRateClass, dtStartDate, dtEndDate, eLanguage))
    End Function

    Public Function GetDetailedRateInformation(ByVal nRateCompanyID As Integer, ByVal sRateClass As String) As DetailedRateDefinitionCollection
        Return (MasterGetDetailedRateInformation(nRateCompanyID, sRateClass, Language.English))
    End Function

    Public Function GetDetailedRateInformation(ByVal nRateCompanyID As Integer, ByVal sRateClass As String, ByVal eLanguage As Language) As DetailedRateDefinitionCollection
        Return (MasterGetDetailedRateInformation(nRateCompanyID, sRateClass, eLanguage))
    End Function

    Public Function GetDetailedRateInformation(ByVal nRateDefinitionID As Integer) As DetailedRateDefinition
        Return (MasterGetDetailedRateInformation(nRateDefinitionID, Language.English))
    End Function

    Public Function GetDetailedRateInformation(ByVal nRateDefinitionID As Integer, ByVal eLanguage As Language) As DetailedRateDefinition
        Return (MasterGetDetailedRateInformation(nRateDefinitionID, eLanguage))
    End Function

    Public Function GetDetailedRateInformation(ByVal nRateCompanyID As Integer, ByVal sRateClass As String, ByVal dtStartDate As DateTime, ByVal dtEndDate As DateTime, ByVal resetStartDateByYears As Integer) As DetailedRateDefinitionCollection
        Return (MasterGetDetailedRateInformation(nRateCompanyID, sRateClass, dtStartDate, dtEndDate, resetStartDateByYears, Language.English))
    End Function

    ' Private Helper Functions for MasterGetDetailedRateInformation overloads
    Private Function MasterGetDetailedRateInformation(ByVal nRateCompanyID As Integer, ByVal sRateClass As String, ByVal dtStartDate As DateTime, ByVal dtEndDate As DateTime, ByVal eLanguage As Language) As DetailedRateDefinitionCollection
        Dim objDC As New dctlRateEngine(m_sRateEngineConnectionString, m_xmlFilesPath, m_dataSourceType, m_cachingType, m_cacheTimeoutInMinutes)
        Dim objDefinitions As DetailedRateDefinitionCollection

        objDefinitions = objDC.GetDetailedRateInformation(nRateCompanyID, GetBaseRateClass(nRateCompanyID, sRateClass), dtStartDate, dtEndDate, eLanguage)

        Return (objDefinitions)
    End Function

    Private Function MasterGetDetailedRateInformation(ByVal nRateCompanyID As Integer, ByVal sRateClass As String, ByVal eLanguage As Language) As DetailedRateDefinitionCollection
        Dim objDC As New dctlRateEngine(m_sRateEngineConnectionString, m_xmlFilesPath, m_dataSourceType, m_cachingType, m_cacheTimeoutInMinutes)
        Dim objDefinitions As DetailedRateDefinitionCollection

        objDefinitions = objDC.GetDetailedRateInformation(nRateCompanyID, GetBaseRateClass(nRateCompanyID, sRateClass), eLanguage)

        Return (objDefinitions)
    End Function

    Private Function MasterGetDetailedRateInformation(ByVal nRateDefinitionID As Integer, ByVal eLanguage As Language) As DetailedRateDefinition
        Dim objDC As New dctlRateEngine(m_sRateEngineConnectionString, m_xmlFilesPath, m_dataSourceType, m_cachingType, m_cacheTimeoutInMinutes)
        Dim objDefinition As DetailedRateDefinition

        objDefinition = objDC.GetDetailedRateInformation(nRateDefinitionID, eLanguage)

        Return (objDefinition)
    End Function

    Private Function MasterGetDetailedRateInformation(ByVal nRateCompanyID As Integer, ByVal sRateClass As String, ByVal dtStartDate As DateTime, ByVal dtEndDate As DateTime, ByVal resetStartDateByYears As Integer, ByVal eLanguage As Language) As DetailedRateDefinitionCollection
        Dim objDC As New dctlRateEngine(m_sRateEngineConnectionString, m_xmlFilesPath, m_dataSourceType, m_cachingType, m_cacheTimeoutInMinutes)
        Dim objDefinitions As DetailedRateDefinitionCollection

        objDefinitions = objDC.GetDetailedRateInformation(nRateCompanyID, GetBaseRateClass(nRateCompanyID, sRateClass), dtStartDate, dtEndDate, resetStartDateByYears, eLanguage)

        Return (objDefinitions)
    End Function

#End Region

#Region "GetSimpleRTPInfo and GetDetailedRTPIinfo functions"

    ''' <summary>
    ''' Gets just boolean indicators.
    ''' </summary>
    ''' <param name="nRateCompanyID"></param>
    ''' <param name="sRateClass"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function GetSimpleRTPInfo(ByVal nRateCompanyID As Integer, ByVal sRateClass As String) As RTPInfo
        Dim info As RTPInfo = Nothing
        Dim defs As SimpleRateDefinitionCollection
        Dim def As SimpleRateDefinition

        defs = MasterGetSimpleRateInformation(nRateCompanyID, sRateClass, Language.English)

        If (defs.Count > 0) Then
            def = defs(0)
            Dim isRTP As Boolean = False
            Dim isCPP As Boolean = False
            Dim isPTR As Boolean = False
            isRTP = def.IsRTPRate

            If (isRTP) Then
                If (def.RTPStream = RTPStream.CriticalPeakOnly) Then isCPP = True
                If (def.RTPStream = RTPStream.PeakTimeRebateOnly) Then isPTR = True
            End If

            info = New RTPInfo(isRTP, isCPP, isPTR)

        End If

        Return (info)
    End Function

    ''' <summary>
    ''' Gets boolean indiators and applicable rtpGroupId.
    ''' </summary>
    ''' <param name="nRateCompanyID"></param>
    ''' <param name="sRateClass"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function GetDetailedRTPInfo(ByVal nRateCompanyID As Integer, ByVal sRateClass As String) As RTPInfo
        Dim info As RTPInfo = Nothing
        Dim defs As DetailedRateDefinitionCollection
        Dim def As DetailedRateDefinition

        defs = MasterGetDetailedRateInformation(nRateCompanyID, sRateClass, Language.English)

        If (defs.Count > 0) Then
            def = defs(0)
            Dim isRTP As Boolean = False
            Dim isCPP As Boolean = False
            Dim isPTR As Boolean = False
            Dim rtpGroupId As Integer = -1

            isRTP = def.IsRTPRate

            If (isRTP) Then
                If (def.RTPStream = RTPStream.CriticalPeakOnly) Then isCPP = True
                If (def.RTPStream = RTPStream.PeakTimeRebateOnly) Then isPTR = True

                ' Get valid RTPGroup from UseCharges 
                For Each objCharge As UseCharge In def.UseCharges

                    If (objCharge.RealTimePricing = True) Then
                        rtpGroupId = objCharge.RTPGroup
                        Exit For
                    End If

                Next

            End If

            info = New RTPInfo(isRTP, isCPP, isPTR, rtpGroupId)

        End If

        Return (info)
    End Function


#End Region

#Region "CalculateCost Overloads"

    ''' <summary>
    ''' Calculate cost.
    ''' </summary>
    ''' <param name="nRateCompanyID"></param>
    ''' <param name="sRateClass"></param>
    ''' <param name="dtStartDate"></param>
    ''' <param name="dtEndDate"></param>
    ''' <param name="objUsageCollection"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function CalculateCost(ByVal nRateCompanyID As Integer, ByVal sRateClass As String, ByVal dtStartDate As DateTime, ByVal dtEndDate As DateTime, ByVal objUsageCollection As UsageCollection) As CostResult
        Return (MainCalculateCost(nRateCompanyID, sRateClass, dtStartDate, dtEndDate, objUsageCollection, New CalculateCostExtParam, knRecursionEntryLevel))
    End Function

    ''' <summary>
    ''' Calculate cost.
    ''' </summary>
    ''' <param name="nRateCompanyID"></param>
    ''' <param name="sRateClass"></param>
    ''' <param name="dtStartDate"></param>
    ''' <param name="dtEndDate"></param>
    ''' <param name="objUsageCollection"></param>
    ''' <param name="objExtParams"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function CalculateCost(ByVal nRateCompanyID As Integer, ByVal sRateClass As String, ByVal dtStartDate As DateTime, ByVal dtEndDate As DateTime, ByVal objUsageCollection As UsageCollection, ByVal objExtParams As CalculateCostExtParam) As CostResult
        Return (MainCalculateCost(nRateCompanyID, sRateClass, dtStartDate, dtEndDate, objUsageCollection, objExtParams, knRecursionEntryLevel))
    End Function

    ''' <summary>
    ''' Calculate cost.
    ''' </summary>
    ''' <param name="nRateCompanyID"></param>
    ''' <param name="sRateClass"></param>
    ''' <param name="nBillDays"></param>
    ''' <param name="dtEndDate"></param>
    ''' <param name="objUsageCollection"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function CalculateCost(ByVal nRateCompanyID As Integer, ByVal sRateClass As String, ByVal nBillDays As Integer, ByVal dtEndDate As DateTime, ByVal objUsageCollection As UsageCollection) As CostResult
        Dim dtStartDate As DateTime
        dtStartDate = dtEndDate.AddDays(-nBillDays + 1)
        Return (MainCalculateCost(nRateCompanyID, sRateClass, dtStartDate, dtEndDate, objUsageCollection, New CalculateCostExtParam, knRecursionEntryLevel))
    End Function

    ''' <summary>
    ''' Calculate cost.
    ''' </summary>
    ''' <param name="nRateCompanyID"></param>
    ''' <param name="sRateClass"></param>
    ''' <param name="nBillDays"></param>
    ''' <param name="dtEndDate"></param>
    ''' <param name="objUsageCollection"></param>
    ''' <param name="objExtParams"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function CalculateCost(ByVal nRateCompanyID As Integer, ByVal sRateClass As String, ByVal nBillDays As Integer, ByVal dtEndDate As DateTime, ByVal objUsageCollection As UsageCollection, ByVal objExtParams As CalculateCostExtParam) As CostResult
        Dim dtStartDate As DateTime
        dtStartDate = dtEndDate.AddDays(-nBillDays + 1)
        Return (MainCalculateCost(nRateCompanyID, sRateClass, dtStartDate, dtEndDate, objUsageCollection, objExtParams, knRecursionEntryLevel))
    End Function

    ''' <summary>
    ''' Calculate cost.
    ''' </summary>
    ''' <param name="nRateDefinitionID"></param>
    ''' <param name="objUsageCollection"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function CalculateCost(ByVal nRateDefinitionID As Integer, ByVal objUsageCollection As UsageCollection) As CostResult
        Return (MainCalculateCost(nRateDefinitionID, objUsageCollection, New CalculateCostExtParam, knRecursionEntryLevel))
    End Function

    ''' <summary>
    ''' Calculate cost.
    ''' </summary>
    ''' <param name="nRateDefinitionID"></param>
    ''' <param name="objUsageCollection"></param>
    ''' <param name="objExtParams"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function CalculateCost(ByVal nRateDefinitionID As Integer, ByVal objUsageCollection As UsageCollection, ByVal objExtParams As CalculateCostExtParam) As CostResult
        Return (MainCalculateCost(nRateDefinitionID, objUsageCollection, objExtParams, knRecursionEntryLevel))
    End Function

    ''' <summary>
    ''' Calculate cost.
    ''' </summary>
    ''' <param name="dtStartDate"></param>
    ''' <param name="dtEndDate"></param>
    ''' <param name="nRateDefinitionID"></param>
    ''' <param name="objUsageCollection"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function CalculateCost(ByVal dtStartDate As DateTime, ByVal dtEndDate As DateTime, ByVal nRateDefinitionID As Integer, ByVal objUsageCollection As UsageCollection) As CostResult
        Return (MainCalculateCost(dtStartDate, dtEndDate, nRateDefinitionID, objUsageCollection, New CalculateCostExtParam, knRecursionEntryLevel))
    End Function

    ''' <summary>
    ''' Calculate cost.
    ''' </summary>
    ''' <param name="dtStartDate"></param>
    ''' <param name="dtEndDate"></param>
    ''' <param name="nRateDefinitionID"></param>
    ''' <param name="objUsageCollection"></param>
    ''' <param name="objExtParams"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function CalculateCost(ByVal dtStartDate As DateTime, ByVal dtEndDate As DateTime, ByVal nRateDefinitionID As Integer, ByVal objUsageCollection As UsageCollection, ByVal objExtParams As CalculateCostExtParam) As CostResult
        Return (MainCalculateCost(dtStartDate, dtEndDate, nRateDefinitionID, objUsageCollection, objExtParams, knRecursionEntryLevel))
    End Function

    ''' <summary>
    ''' Calculate cost for multiple calculations using inputs collection.  
    ''' Usually used by callers that need annual totals based upon monthly calculations.
    ''' </summary>
    ''' <param name="nRateCompanyID"></param>
    ''' <param name="InputParams"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function CalculateCost(ByVal nRateCompanyID As Integer, ByVal ParamArray InputParams() As CalculateCostInputParam) As CostResultCollection
        Dim i As Integer
        Dim objCostResults As New CostResultCollection
        Dim objCostResult As CostResult

        For i = 0 To InputParams.GetUpperBound(0)
            objCostResult = MainCalculateCost(nRateCompanyID, InputParams(i).RateClass, InputParams(i).StartDate, InputParams(i).EndDate, InputParams(i).UsageCollection, New CalculateCostExtParam, knRecursionEntryLevel)
            objCostResults.Add(objCostResult)
        Next

        Return (objCostResults)
    End Function

    ''' <summary>
    ''' Calculate cost for multiple calculations using inputs collection.
    ''' This overload is optimized to allow the re-use a single definitions collection for the entire collection of inputs.
    ''' Usually used by callers that need annual totals based upon monthly calculations.
    ''' </summary>
    ''' <param name="nRateCompanyID"></param>
    ''' <param name="bQueryDefinitionsForEachInput"></param>
    ''' <param name="InputParams"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function CalculateCost(ByVal nRateCompanyID As Integer, ByVal bQueryDefinitionsForEachInput As Boolean, ByVal ParamArray InputParams() As CalculateCostInputParam) As CostResultCollection
        Dim i As Integer
        Dim objDefinitions As DetailedRateDefinitionCollection
        Dim objCostResults As CostResultCollection
        Dim objCostResult As CostResult
        Dim defStartDate As DateTime
        Dim defEndDate As DateTime
        Dim nBillDays As Integer
        Const resetStartDateBackYears As Integer = -2

        If (bQueryDefinitionsForEachInput) Then
            objCostResults = CalculateCost(nRateCompanyID, InputParams)
        Else
            objCostResults = New CostResultCollection

            ' Always jump a year ahead and get definition(s) at that time period so only the newest is being retrieved;
            ' It would be unlikely that more than one definition would get retrieved; assume 1 !!
            defStartDate = DateTime.Now.AddYears(1)
            defEndDate = defStartDate.AddDays(1)

            ' Only get the rate definition based on the rate class in the first input parameter;  
            ' Provide a parameter that artificially changes the rate definition effective date internally; 
            ' This overload was added to aid in proper caching support, since callers should not change the start date after definitions are returned (since they may come from cache!)
            objDefinitions = GetDetailedRateInformation(nRateCompanyID, InputParams(0).RateClass, defStartDate, defEndDate, resetStartDateBackYears)

            ' Process the array of input parameters, re-using the Definition Collection for each calculation;
            For i = 0 To InputParams.GetUpperBound(0)
                nBillDays = InputParams(i).EndDate.Subtract(InputParams(i).StartDate).Days + 1
                objCostResult = SetupWork(objDefinitions, InputParams(i).RateClass, nBillDays, InputParams(i).StartDate, InputParams(i).EndDate, InputParams(i).UsageCollection, New CalculateCostExtParam, knRecursionEntryLevel)
                objCostResults.Add(objCostResult)
            Next

        End If

        Return (objCostResults)
    End Function

    ''' <summary>
    ''' Calculate cost for when definitions are supplied as input.
    ''' </summary>
    ''' <param name="nRateCompanyID"></param>
    ''' <param name="definitions"></param>
    ''' <param name="sRateClass"></param>
    ''' <param name="dtStartDate"></param>
    ''' <param name="dtEndDate"></param>
    ''' <param name="objUsageCollection"></param>
    ''' <param name="objExtParams"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function CalculateCost(ByVal nRateCompanyID As Integer, ByVal definitions As DetailedRateDefinitionCollection, ByVal sRateClass As String, ByVal dtStartDate As DateTime, ByVal dtEndDate As DateTime, ByVal objUsageCollection As UsageCollection, ByVal objExtParams As CalculateCostExtParam) As CostResult
        Return (MainCalculateCost(nRateCompanyID, definitions, sRateClass, dtStartDate, dtEndDate, objUsageCollection, objExtParams, knRecursionEntryLevel))
    End Function

    ''' <summary>
    ''' Calculate cost overload, given an xml node list for a rate class.
    ''' </summary>
    ''' <param name="nRateCompanyID"></param>
    ''' <param name="xmlRateClassAttributes"></param>
    ''' <param name="dtStartDate"></param>
    ''' <param name="dtEndDate"></param>
    ''' <param name="objUsageCollection"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function CalculateCost(ByVal nRateCompanyID As Integer, ByVal xmlRateClassAttributes As System.Xml.XmlNodeList, ByVal dtStartDate As DateTime, ByVal dtEndDate As DateTime, ByVal objUsageCollection As UsageCollection) As CostResult
        Dim rateClass As String
        Dim err As String

        rateClass = RateClassModifierCollection.ConvertRateClassAttributestoRateClassString(xmlRateClassAttributes)
        err = RateClassModifierCollection.ValidateRateClassModifierCollection(rateClass)

        If (err <> String.Empty) Then
            Throw New Exception(err)
        End If

        Return (MainCalculateCost(nRateCompanyID, rateClass, dtStartDate, dtEndDate, objUsageCollection, New CalculateCostExtParam, knRecursionEntryLevel))
    End Function

    ' Private Helper Functions for CalculateCost overloads
    Private Function MainCalculateCost(ByVal nRateCompanyID As Integer, ByVal sRateClass As String, ByVal dtStartDate As DateTime, ByVal dtEndDate As DateTime, ByVal objUsageCollection As UsageCollection, ByVal objExtParams As CalculateCostExtParam, ByVal nRecursionLevel As Integer) As CostResult
        Dim objCostResult As CostResult
        Dim objDefinitions As DetailedRateDefinitionCollection
        Dim nBillDays As Integer

        nBillDays = dtEndDate.Subtract(dtStartDate).Days + 1

        objDefinitions = GetDetailedRateInformation(nRateCompanyID, sRateClass, dtStartDate, dtEndDate)
        objCostResult = SetupWork(objDefinitions, sRateClass, nBillDays, dtStartDate, dtEndDate, objUsageCollection, objExtParams, nRecursionLevel)

        Return (objCostResult)
    End Function

    Private Function MainCalculateCost(ByVal nRateDefinitionID As Integer, ByVal objUsageCollection As UsageCollection, ByVal objExtParams As CalculateCostExtParam, ByVal nRecursionLevel As Integer) As CostResult
        Dim objCostResult As CostResult
        Dim objDefinition As DetailedRateDefinition
        Dim nBillDays As Integer

        ' No Dates provided, simply use BillDays, 
        ' SetupWork then handles "Nothing dates" to master function call by using RateDef startDate w/ BillDays
        nBillDays = knDefaultBillDays

        objDefinition = GetDetailedRateInformation(nRateDefinitionID)
        objCostResult = SetupWork(objDefinition, nBillDays, objUsageCollection, objExtParams, nRecursionLevel)

        Return (objCostResult)
    End Function

    Private Function MainCalculateCost(ByVal dtStartDate As DateTime, ByVal dtEndDate As DateTime, ByVal nRateDefinitionID As Integer, ByVal objUsageCollection As UsageCollection, ByVal objExtParams As CalculateCostExtParam, ByVal nRecursionLevel As Integer) As CostResult
        Dim objCostResult As CostResult
        Dim objDefinition As DetailedRateDefinition
        Dim objDefinitions As New DetailedRateDefinitionCollection
        Dim nBillDays As Integer

        nBillDays = dtEndDate.Subtract(dtStartDate).Days + 1

        objDefinition = GetDetailedRateInformation(nRateDefinitionID)
        objDefinitions.Add(objDefinition)
        objCostResult = SetupWork(objDefinitions, String.Empty, nBillDays, dtStartDate, dtEndDate, objUsageCollection, objExtParams, nRecursionLevel)

        Return (objCostResult)
    End Function

    Private Function MainCalculateCost(ByVal nRateCompanyID As Integer, ByVal objDefinitions As DetailedRateDefinitionCollection, ByVal sRateClass As String, ByVal dtStartDate As DateTime, ByVal dtEndDate As DateTime, ByVal objUsageCollection As UsageCollection, ByVal objExtParams As CalculateCostExtParam, ByVal nRecursionLevel As Integer) As CostResult
        Dim objCostResult As CostResult
        Dim nBillDays As Integer

        nBillDays = dtEndDate.Subtract(dtStartDate).Days + 1

        objCostResult = SetupWork(objDefinitions, sRateClass, nBillDays, dtStartDate, dtEndDate, objUsageCollection, objExtParams, nRecursionLevel)

        Return (objCostResult)
    End Function


    Private Function SetupWork(ByVal objDefinitions As DetailedRateDefinitionCollection, ByVal sRateClassArg As String, ByVal nBillDays As Integer, ByVal dtStartDate As DateTime, ByVal dtEndDate As DateTime, ByVal objUsageCollection As UsageCollection, ByVal objExtParams As CalculateCostExtParam, ByVal nRecursionLevel As Integer) As CostResult
        Dim objCostResult As CostResult

        objCostResult = ProcessParentAndChildren(objDefinitions, nBillDays, sRateClassArg, dtStartDate, dtEndDate, True, objUsageCollection, objExtParams, nRecursionLevel)

        Return (objCostResult)
    End Function

    Private Function SetupWork(ByVal objDefinition As DetailedRateDefinition, ByVal nBillDays As Integer, ByVal objUsageCollection As UsageCollection, ByVal objExtParams As CalculateCostExtParam, ByVal nRecursionLevel As Integer) As CostResult
        Dim objCostResult As CostResult
        Dim objDefinitions As New DetailedRateDefinitionCollection

        ' Add the definition to a collection of ONE to pass to MasterCalculateCost function
        ' This little bit of overhead simplifies work needed in function
        objDefinitions.Add(objDefinition)

        ' StartDate And EndDate are really unspecified, use the RateDefinition startDate and StartDate + BillDays
        ' This is necessary in case the RateDefinition uses FCR
        objCostResult = ProcessParentAndChildren(objDefinitions, nBillDays, String.Empty, objDefinition.StartDate, objDefinition.StartDate.AddDays(nBillDays), False, objUsageCollection, objExtParams, nRecursionLevel)

        Return (objCostResult)
    End Function

    ''' <summary>
    ''' This calling layer exists for processing children of master rate class if required
    ''' </summary>
    ''' <param name="objDefinitions"></param>
    ''' <param name="nBillDays"></param>
    ''' <param name="sRateClassArg"></param>
    ''' <param name="dtStartDate"></param>
    ''' <param name="dtEndDate"></param>
    ''' <param name="bDateRangeExists"></param>
    ''' <param name="objUsageColl"></param>
    ''' <param name="objExtParams"></param>
    ''' <param name="nRecursionLevel"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function ProcessParentAndChildren(ByVal objDefinitions As DetailedRateDefinitionCollection,
                                              ByVal nBillDays As Integer,
                                              ByVal sRateClassArg As String,
                                              ByVal dtStartDate As DateTime,
                                              ByVal dtEndDate As DateTime,
                                              ByVal bDateRangeExists As Boolean,
                                              ByVal objUsageColl As UsageCollection,
                                              ByVal objExtParams As CalculateCostExtParam,
                                              ByVal nRecursionLevel As Integer) As CostResult

        Dim ksFunctionName As String = "CalculateCost-ProcessParentAndChildren"
        Dim objCostResult As CostResult
        Dim objCostResultFromChild As CostResult
        Dim objCostResultTemp As CostResult = Nothing
        Dim child As MasterChild
        Dim objLocalUsageColl As UsageCollection
        Dim bMasterHasChildren As Boolean = False
        Dim sSSWapRateClass As String
        Dim eRateClassModifierCopyFilter As RateClassModifierCollection.RateClassModifierCopyFilter
        Dim objIncludeExcludeRateClassModifierKeyList As List(Of String)
        Dim sChildRateClass As String = String.Empty
        Dim sswapMasterChildCollection As MasterChildCollection
        Dim tempStartDate As DateTime
        Dim tempEndDate As DateTime

        Log(ksFunctionName)

        ' Lookup and convert usages if they were passed in as UseNames (unmapped usage)
        objUsageColl = VerifyUsages(objUsageColl)

        ' Log the usages at this point
        Log(objUsageColl)

        If (objDefinitions.Count > 0) Then

            bMasterHasChildren = objDefinitions(0).MasterHasChildren

            'Rate class attribute - Supply Swap exists and this is the first recursion.
            If (DoesRateClassAttributeExist(sRateClassArg, RateClassModifierCollection.ksnSSWAP) = True) And
               (nRecursionLevel = 0) Then

                'Master has no children. (Supply swap is not supported on rate classes that have children).
                If (bMasterHasChildren = False) Then

                    'Retrieve rate class attribute - supply swap value.
                    sSSWapRateClass = RateClassModifierCollection.GetModifierValueFromRateClassModifierString(sRateClassArg, RateClassModifierCollection.ksnSSWAP)
                    If (String.IsNullOrEmpty(sSSWapRateClass) = True) Then
                        Log(String.Format("...Could not apply rate class modifier. Rate class modifier value invalid. (RateClassModifier name: {0}, value: {1})",
                                          RateClassModifierCollection.ksnSSWAP.ToString(), sSSWapRateClass))
                    End If

                    'Create dynamic master child relationship between master rate class and supply swap rate class.
                    'Some callers modified the start date (RC WS most likely) and originally selected a different rate definition.
                    'Need to use the same startDate and endDate used to originally select the definitions(s),
                    If (objDefinitions(0).IsStartDateModified) Then
                        tempStartDate = objDefinitions(0).StartDateSelector
                        tempEndDate = objDefinitions(0).EndDateSelector
                    Else
                        tempStartDate = dtStartDate
                        tempEndDate = dtEndDate
                    End If

                    sswapMasterChildCollection = CreateDynamicMasterChild(objDefinitions(0).RateCompanyID,
                                                                          objDefinitions(0).MasterID,
                                                                          sSSWapRateClass,
                                                                          tempStartDate,
                                                                          tempEndDate)
                    If (sswapMasterChildCollection.Count > 0) Then
                        objDefinitions(0).MasterChildren = sswapMasterChildCollection
                        objDefinitions(0).Children = True
                        bMasterHasChildren = True
                    End If

                Else
                    'Master already has children.

                    Log(String.Format("...Could not apply rate class modifier because rate class has children. (If performing BTD calculations, RateClassModifier {0} already applied.) (RateClassModifier name: {0})",
                                      RateClassModifierCollection.ksnSSWAP.ToString()))

                End If

            End If

            If (bMasterHasChildren) Then

                objCostResultTemp = New CostResult
                objCostResultTemp.CostCollection = New CostCollection

                If (nRecursionLevel <= knMaximumRecursionLevel) Then

                    For Each child In objDefinitions(0).MasterChildren

                        objIncludeExcludeRateClassModifierKeyList = New List(Of String)

                        If (DoesRateClassAttributeExist(sRateClassArg, RateClassModifierCollection.ksnSSWAP) = True) And
                           (nRecursionLevel = 0) Then

                            'Specify to copy only Included rate class modifiers.
                            eRateClassModifierCopyFilter = RateClassModifierCollection.RateClassModifierCopyFilter.Include
                            objIncludeExcludeRateClassModifierKeyList.Add(RateClassModifierCollection.ksnDATF)
                            objIncludeExcludeRateClassModifierKeyList.Add(RateClassModifierCollection.ksnDBTF)
                            objIncludeExcludeRateClassModifierKeyList.Add(RateClassModifierCollection.ksnEXCLCT)
                            objIncludeExcludeRateClassModifierKeyList.Add(RateClassModifierCollection.ksnDEXCLCT)
                            objIncludeExcludeRateClassModifierKeyList.Add(RateClassModifierCollection.ksnIRS)
                            objIncludeExcludeRateClassModifierKeyList.Add(RateClassModifierCollection.ksnTCF)
                            objIncludeExcludeRateClassModifierKeyList.Add(RateClassModifierCollection.ksnTUF)
                            sChildRateClass = RateClassModifierCollection.CopyRateClassWithModifiers(sRateClassArg,
                                                                                                       child.RateClass,
                                                                                                       objIncludeExcludeRateClassModifierKeyList,
                                                                                                       eRateClassModifierCopyFilter)

                        Else

                            'Specify to copy only Included rate class modifiers.
                            eRateClassModifierCopyFilter = RateClassModifierCollection.RateClassModifierCopyFilter.Include
                            objIncludeExcludeRateClassModifierKeyList.Add(RateClassModifierCollection.ksnBC)
                            sChildRateClass = RateClassModifierCollection.CopyRateClassWithModifiers(sRateClassArg,
                                                                                                       child.RateClass,
                                                                                                       objIncludeExcludeRateClassModifierKeyList,
                                                                                                       eRateClassModifierCopyFilter)

                        End If

                        ' Calculate cost with the child's rate class and use the correct set of usage;
                        Select Case child.LoadType
                            Case LoadType.General, LoadType.All
                                objLocalUsageColl = objUsageColl

                            Case LoadType.Heating, LoadType.Cooling, LoadType.WaterHeating, LoadType.Irrigation
                                objLocalUsageColl = objUsageColl.GetLoadTypeUsage(child.LoadType)

                            Case Else
                                objLocalUsageColl = objUsageColl
                        End Select

                        ' Recursive
                        Dim childDefs As DetailedRateDefinitionCollection

                        If (objDefinitions(0).IsStartDateModified) Then
                            childDefs = GetDetailedRateInformationWithReset(objDefinitions(0).RateCompanyID, sChildRateClass, objDefinitions(0).StartDateSelector, objDefinitions(0).EndDateSelector)
                        Else
                            childDefs = GetDetailedRateInformation(objDefinitions(0).RateCompanyID, sChildRateClass, dtStartDate, dtEndDate)
                        End If

                        objCostResultFromChild = MainCalculateCost(objDefinitions(0).RateCompanyID, childDefs, sChildRateClass, dtStartDate, dtEndDate, objLocalUsageColl, objExtParams, (nRecursionLevel + 1))

                        objCostResultTemp = CombineResults(objCostResultTemp, objCostResultFromChild)
                    Next

                End If

            End If

            ' Only desire usage for the LoadType of the Master Rate Class; 
            objLocalUsageColl = objUsageColl.GetLoadTypeUsage(objDefinitions(0).LoadType)

        Else
            objLocalUsageColl = objUsageColl
        End If

        ' Main call for calculating cost;
        objCostResult = MasterCalculateCost(objDefinitions, nBillDays, sRateClassArg, dtStartDate, dtEndDate, bDateRangeExists, objLocalUsageColl, objExtParams)

        ' Create an overall result when there were children processed above
        If (bMasterHasChildren) Then
            objCostResult = CombineResults(objCostResult, objCostResultTemp)
            objCostResult.CalculationsHadChildren = True
        End If

        'If this was a NEM rate, then there is additional logic to apply, and it will apply to the parent result and any combined child results here
        objCostResult = NetMeteringHandler(objDefinitions, objCostResult, sRateClassArg)

        Return (objCostResult)
    End Function

    ''' <summary>
    ''' This detects and calculates special net metering results
    ''' </summary>
    ''' <param name="defs"></param>
    ''' <param name="costResult"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function NetMeteringHandler(ByVal defs As DetailedRateDefinitionCollection, ByVal costResult As CostResult, ByVal sRateClassArg As String) As CostResult
        Dim mgr As NetMeteringManager = Nothing
        Dim settings As RateCompanySettings = Nothing
        Dim netMeteringResult As NetMeteringMonthlyResult

        Dim objRateClassModifiers As RateClassModifierCollection
        Dim objDiscountBeforeTaxFactors As RateClassModifierCollection
        Dim objDiscountAfterTaxFactors As RateClassModifierCollection
        Dim dblDiscountFactor As Double = 0.0
        Dim dblTotalDiscountBeforeTaxFactor As Double = 0.0
        Dim dblTotalDiscountAfterTaxFactor As Double = 0.0

        Try
            If (defs.Count > 0 AndAlso _
                costResult.IsNetMetering AndAlso _
                (costResult.NetMeteringType = NetMeteringType.Nem3 Or _
                costResult.NetMeteringType = NetMeteringType.Nem4 Or _
                costResult.NetMeteringType = NetMeteringType.Nem5)) Then

                settings = GetRateCompanySettings(defs(0).RateCompanyID)

                mgr = New NetMeteringManager(defs(0).RateCompanyID, costResult.NetMeteringType, settings)

                ' 14.09 Bug 53743 - use the total cost before min cost
                'Bug54232
                If (costResult.MinimumCostsApplied) Then
                    netMeteringResult = mgr.CreateNetMeteringMonthlyResult(costResult.TotalCostBeforeMinimumCostsApplied, costResult)
                Else
                    netMeteringResult = mgr.CreateNetMeteringMonthlyResult(costResult.TotalCost, costResult)
                End If

                'Getting tax cost factor from rate class modifier collection
                objRateClassModifiers = CreateRateClassModifierCollection(sRateClassArg, defs(0).RateCompanyID)
                'DBTF
                objDiscountBeforeTaxFactors = objRateClassModifiers.GetRateClassModifierList(RateClassModifierType.DiscountBeforeTaxFactor)
                For Each objDiscountFactor As RateClassModifier In objDiscountBeforeTaxFactors
                    If Not objDiscountFactor.Value = "0" Then
                        Double.TryParse(objDiscountFactor.Value, dblDiscountFactor)
                        dblTotalDiscountBeforeTaxFactor = dblTotalDiscountBeforeTaxFactor + dblDiscountFactor
                    End If
                Next
                'DATF
                objDiscountAfterTaxFactors = objRateClassModifiers.GetRateClassModifierList(RateClassModifierType.DiscountAfterTaxFactor)
                For Each objDiscountFactor As RateClassModifier In objDiscountAfterTaxFactors
                    If Not objDiscountFactor.Value = "0" Then
                        Double.TryParse(objDiscountFactor.Value, dblDiscountFactor)
                        dblTotalDiscountAfterTaxFactor = dblTotalDiscountAfterTaxFactor + dblDiscountFactor
                    End If
                Next

                If (Not dblTotalDiscountBeforeTaxFactor = 0) OrElse (Not dblTotalDiscountAfterTaxFactor = 0) Then
                    'CR58687 - Applying Discount to MMC/ NetMetering Minimum cost                  
                    Dim discount As Double = 0.0
                    Dim excludedDiscounts As Double = 0.0
                    Dim sarrExcludeCostTypeList As String() = Nothing
                    Dim sExcludeCostTypes As String = String.Empty
                    Dim eExcludeCostType As CostType

                    'Get the list of discount cost types to be excluded
                    Dim objRateClassModifierDiscountExcludeCostType As RateClassModifierCollection
                    objRateClassModifierDiscountExcludeCostType = objRateClassModifiers.GetRateClassModifierList(RateClassModifierType.DiscountExcludeCostTypes)
                    For Each objRateClassModifier As RateClassModifier In objRateClassModifierDiscountExcludeCostType
                        sExcludeCostTypes = objRateClassModifier.Value
                        If (String.IsNullOrEmpty(sExcludeCostTypes) = False) Then
                            sarrExcludeCostTypeList = sExcludeCostTypes.Split(New Char() {","c})
                            For Each sExcludeCostType As String In sarrExcludeCostTypeList
                                'Convert cost type name into cost type enumeration.
                                eExcludeCostType = CType([Enum].Parse(GetType(CostType), sExcludeCostType), CostType)
                                'Cost type to exclude exists in cost collection.
                                If (costResult.CostCollection.Contains(eExcludeCostType) = True) Then
                                    'sum of amounts of dexclct cost type.
                                    excludedDiscounts = excludedDiscounts + costResult.CostCollection(eExcludeCostType).Amount
                                    Log("[Excluded the Cost Type (" + costResult.CostCollection(eExcludeCostType).CostType.ToString() + " ) before applying discount to NET Min Cost]")
                                End If
                            Next

                        End If
                    Next

                    Log("[Initial MMC is (" + netMeteringResult.MinimumCost.ToString() + ") before applying discount to Net Metering Minimum cost]")

                    'Applying Discount to MMC/ NetMetering Minimum cost  after subtracting cost types mentioned as DEXCLCT
                    discount = ((netMeteringResult.MinimumCost - excludedDiscounts) * (dblTotalDiscountBeforeTaxFactor + dblTotalDiscountAfterTaxFactor))
                    Log("[Discount Before Tax Factor (" + dblTotalDiscountBeforeTaxFactor.ToString() + ") adjustment applied to Net Metering Minimum cost]")
                    Log("[Discount After Tax Factor (" + dblTotalDiscountAfterTaxFactor.ToString() + ") adjustment applied to Net Metering Minimum cost]")

                    Log("[Discount value to be applied to Net Minimum cost  is (" + discount.ToString() + ")")
                    netMeteringResult.MinimumCost = netMeteringResult.MinimumCost + discount
                    Log("[Discounted MMC after applying Discount is (" + netMeteringResult.MinimumCost.ToString() + ")]")

                    'Bug Fix for 58712, 58750, 58751 - Apply discount to all NEM Minimum cost except the discount exclude/DEXCLCT cost types
                    For Each objDE As DictionaryEntry In netMeteringResult.MinimumCosts
                        Dim c As Cost = CType(objDE.Value, Cost)
                        If ((String.IsNullOrEmpty(sExcludeCostTypes)) Or (Not sExcludeCostTypes.Contains(c.CostType.ToString()))) Then
                            c.Amount = c.Amount + (c.Amount * (dblTotalDiscountBeforeTaxFactor + dblTotalDiscountAfterTaxFactor))
                        End If
                    Next

                    'Bug Fix 58847 - Recomputing mcc with the discounted net min cost
                    netMeteringResult.CarryoverCharge = netMeteringResult.RegularCost - netMeteringResult.MinimumCost
                    Dim nemSettings As NetMeteringSettings
                    nemSettings = settings.GetNEMSettings(costResult.NetMeteringType)
                    If (netMeteringResult.CarryoverCharge < 0.0) Then
                        netMeteringResult.CarryoverCharge = netMeteringResult.CarryoverCharge * nemSettings.D1
                    End If
                End If

                'reset the costResult if Minimum Net Cost is greater than regular cost!
                'Bug Fix 58847
                If (netMeteringResult.MinimumCost > netMeteringResult.RegularCost) OrElse (costResult.MinimumCharge > netMeteringResult.RegularCost) Then
                    costResult.TotalCost = netMeteringResult.MinimumCost
                    costResult.CostCollection.Clear()
                    costResult.CostCollection = netMeteringResult.MinimumCosts
                Else
                    costResult.TotalCost = netMeteringResult.RegularCost
                End If

                costResult.NetMeteringResult = netMeteringResult

                'log the fact that the cost collection was modified
                Log(String.Format("NetMetering Modified CostResult (NetMeteringType: {0})", costResult.NetMeteringType.ToString()))
                For Each de As DictionaryEntry In netMeteringResult.MinimumCosts
                    Dim cost As Cost = CType(de.Value, Cost)
                    Log(String.Format("...NEM Cost[{0}]", cost.Amount.ToString()), cost.CostType)
                Next
                Log(String.Format("...[NEM mrc={0} mmc={1} mcc={2}]", netMeteringResult.RegularCost, netMeteringResult.MinimumCost, netMeteringResult.CarryoverCharge))
                Log(costResult)

            End If
        Catch
            ' do not fail the result because of faulty net metering cases
        End Try

        Return (costResult)
    End Function

    ''' <summary>
    ''' Master Function for CalculateCost
    ''' </summary>
    ''' <param name="objDefinitions"></param>
    ''' <param name="nBillDays"></param>
    ''' <param name="sRateClassArg"></param>
    ''' <param name="dtStartDate"></param>
    ''' <param name="dtEndDate"></param>
    ''' <param name="bDateRangeExists"></param>
    ''' <param name="objUsageColl"></param>
    ''' <param name="objExtParams"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function MasterCalculateCost(ByVal objDefinitions As DetailedRateDefinitionCollection,
                                         ByVal nBillDays As Integer,
                                         ByVal sRateClassArg As String,
                                         ByVal dtStartDate As DateTime,
                                         ByVal dtEndDate As DateTime,
                                         ByVal bDateRangeExists As Boolean,
                                         ByVal objUsageColl As UsageCollection,
                                         ByVal objExtParams As CalculateCostExtParam) As CostResult

        Dim ksFunctionName As String = "*MasterCalculateCost*"
        Dim objMasterCostResult As CostResult
        Dim objMasterMinCostResult As CostResult
        Dim objCostResult As CostResult
        Dim objMinCostResult As CostResult
        Dim objDetailedDef As DetailedRateDefinition
        Dim objCostColl As CostCollection
        Dim objMinCostColl As CostCollection
        Dim arrProrateFactors() As Double = Nothing
        Dim objSeasonFactors As SeasonFactorCollection
        Dim i As Integer = 0
        Dim bFinalDefinition As Boolean
        Dim bUseSeasonProrateOverride As Boolean
        Dim eOverrideSeason As Season
        Dim bRebin As Boolean
        Dim eErrCode As ErrCode = ErrCode.NoError
        Dim objRateClassModifiers As RateClassModifierCollection
        Dim sbRateClass As System.Text.StringBuilder
        Dim bIsDemandResponseEligible As Boolean
        Dim sSSWapRateClass As String
        Dim demandSwapped As Boolean = False
        Dim nDefaultRateCompanyID As Integer = 0

        Log(ksFunctionName)

        'Process rate class with "#" and all the attribute modifiers
        If (objDefinitions.Count > 0) Then
            nDefaultRateCompanyID = objDefinitions(0).RateCompanyID
        End If

        If (RateClassModifierCollection.DoesRateClassContainModifiers(sRateClassArg)) Then
            Try
                objRateClassModifiers = CreateRateClassModifierCollection(sRateClassArg, nDefaultRateCompanyID)

                sbRateClass = New System.Text.StringBuilder

                ' Get rate class from modifier collection
                sbRateClass.Append(objRateClassModifiers.Find(RateClassModifierType.RateClass).Value)

                ' Get baseline code from modifier collection if exists
                Dim sRateClassBreak, sBaseslineCode As String
                sBaseslineCode = objRateClassModifiers.Find(RateClassModifierType.BaselineCode).Value
                If Not sBaseslineCode = String.Empty Then
                    sRateClassBreak = GetRateClassBreakCharacter(nDefaultRateCompanyID)
                    sbRateClass.Append(sRateClassBreak)
                    sbRateClass.Append(sBaseslineCode)
                End If

                sRateClassArg = sbRateClass.ToString()

            Catch ex As Exception
                Throw ex
            End Try
        Else
            objRateClassModifiers = CreateRateClassModifierCollection(sRateClassArg, nDefaultRateCompanyID)
        End If

        ' Log the Rate Class Modifiers
        Log(objRateClassModifiers)

        ' Adjust DemandBill usage determinants if adbd rate helper is being provided.
        Dim annualDemandBillDemandModifier As RateClassModifier = objRateClassModifiers.Find(RateClassModifierType.AnnualDemandBillDemand)
        'CR56757 - Add ADBD Factor and Rule to existing ADBD Computations
        Dim annualDemandBillDemandFactor As RateClassModifier = objRateClassModifiers.Find(RateClassModifierType.ADBDFactor)
        Dim annualDemandBillDemandRule As RateClassModifier = objRateClassModifiers.Find(RateClassModifierType.ADBDRule)
        objUsageColl = AdjustDemandBillUsage(objUsageColl, annualDemandBillDemandModifier, annualDemandBillDemandFactor, annualDemandBillDemandRule)

        ' CR 56546 - clone the usage before demand prorate, and pass it to processServiceCharges
        Dim objServiceChargeUsageColl As UsageCollection = CType(objUsageColl.Clone(), UsageCollection)

        ' CR 56546 - prorate demand usage after annual demand bill demand adjustment
        If (objExtParams.ProrateDemandCharges) Then
            ProrateDemandUsage(objUsageColl, objExtParams.MonthlyServiceChargeProrateFactor)
        End If

        ' Scale Tax Usage Factor if not 0
        ' Multiply each usage determinant by (1+TaxUsageFactore to get new usage for purpose of computing costs
        ' apply the tax Usage factors, if any, before applying the taxCostFactor
        Dim objTaxUsageFactors As RateClassModifierCollection
        objTaxUsageFactors = objRateClassModifiers.GetRateClassModifierList(RateClassModifierType.TaxUsageFactor)
        objUsageColl = AdjustTaxUsage(objUsageColl, objTaxUsageFactors)

        ' Log the usages being used within this function
        Log(objUsageColl)

        ' StartDate And EndDate will exist; nBillDays will have real BillDays or defaulted to thirty days
        objDefinitions = ProcessProrate(objDefinitions, nBillDays, dtStartDate, dtEndDate, arrProrateFactors)
        Log(arrProrateFactors)

        ' Determine possible seasonal proration information
        bUseSeasonProrateOverride = ProcessSeasonalProrate(objDefinitions, dtStartDate, dtEndDate, eOverrideSeason)
        Log(bUseSeasonProrateOverride, eOverrideSeason)

        ' Special rebinning in TOU cases
        bRebin = CheckForSeasonalTOUWithUsage(objUsageColl, objDefinitions)
        If (bRebin) Then objUsageColl = RebinUsagesForSeasonalTOU(objUsageColl, objDefinitions, dtStartDate, dtEndDate, bUseSeasonProrateOverride, eOverrideSeason)

        ' Primary
        objMasterCostResult = New CostResult
        objMasterCostResult.CostCollection = New CostCollection
        objMasterCostResult = SumNonDemandUsagesAndInitCostResult(objUsageColl, objMasterCostResult, nBillDays)

        'Rebates from PTR are calculated externally and passed-in and added here so taxes and other rate helpers can be applied to this cost/credit
        If (objExtParams.RebateExists AndAlso (Not objExtParams.RebateApplied)) Then
            objMasterCostResult.CostCollection.SumAdd(CostType.PTR_REBATE, New Cost(CostType.PTR_REBATE, objExtParams.RebateAmount))
            objMasterCostResult.TotalCost = objMasterCostResult.TotalCost + objExtParams.RebateAmount
            objExtParams.RebateApplied = True
        End If

        ' Minimum
        objMasterMinCostResult = New CostResult
        objMasterMinCostResult.CostCollection = New CostCollection
        objMasterMinCostResult = SumNonDemandUsagesAndInitCostResult(objUsageColl, objMasterMinCostResult, nBillDays)

        Dim objCostCollection As List(Of Cost) = New List(Of Cost)

        For Each objDetailedDef In objDefinitions
            objCostColl = New CostCollection
            objMinCostColl = New CostCollection
            If (i = (objDefinitions.Count - 1)) Then bFinalDefinition = True

            Log(objDetailedDef)

            ' Build SeasonFactors for UseCharges and ServiceCharges to use in processing
            If (objDetailedDef.IsSeasonal) Then
                objSeasonFactors = DetermineSeasonFactors(objDetailedDef, nBillDays, dtStartDate, dtEndDate, bFinalDefinition, arrProrateFactors(i), bUseSeasonProrateOverride, eOverrideSeason)
            Else
                objSeasonFactors = DetermineSeasonFactors()
            End If

            ' Time-Of-Use check for season and consistent tou boundaries across seasons
            eErrCode = TOUBoundaryCheck(objDetailedDef, objSeasonFactors, eErrCode)

            ' Is Demand Response Eligible, and pass it to ProcessUseCharges, and ultimately pass to calculateCostForRTP to determine if it should proceed with RTP
            bIsDemandResponseEligible = CType(objRateClassModifiers.Find(RateClassModifierType.IsDemandResponseEligible).Value, Boolean)

            ' Is special handling for Demand prorating applicable
            If (i = 1 AndAlso objDetailedDef.IsDemand AndAlso objDetailedDef.DemandProrateType = DemandProrateType.SplitBillDaysPeriodDemand) Then
                objUsageColl.SwapVirtualQuantityOfDemandUsages()
                demandSwapped = True
            End If

            ' Primary
            objCostColl = ProcessServiceCharges(objRateClassModifiers, objServiceChargeUsageColl, objCostColl, objDetailedDef, objExtParams.MeterCount, objExtParams.DwellingCount, objMasterCostResult.TotalUse, nBillDays, dtStartDate, dtEndDate, bFinalDefinition, arrProrateFactors(i), bUseSeasonProrateOverride, eOverrideSeason, objSeasonFactors, objExtParams.ProrateMonthlyServiceCharges, objExtParams.ProrateDemandCharges, objExtParams.MonthlyServiceChargeProrateFactor)
            objCostColl = ProcessUseCharges(objRateClassModifiers, objUsageColl, objCostColl, objDetailedDef, sRateClassArg, objExtParams.MeterCount, objExtParams.DwellingCount, objMasterCostResult.TotalUse, nBillDays, dtStartDate, dtEndDate, bFinalDefinition, arrProrateFactors(i), bUseSeasonProrateOverride, eOverrideSeason, objSeasonFactors, bIsDemandResponseEligible, objExtParams)
            objCostResult = FinalizeCosts(objCostColl, objDetailedDef, arrProrateFactors(i))
            objMasterCostResult = CombineResults(objMasterCostResult, objCostResult)
            objMasterCostResult.RateCountApplied = objMasterCostResult.RateCountApplied + 1

            ' Minimum
            objMinCostColl = ProcessMinimumCharges(objUsageColl, objMinCostColl, objDetailedDef, objExtParams.MeterCount, objExtParams.DwellingCount, objMasterCostResult.TotalUse, nBillDays, arrProrateFactors(i), objExtParams.MinimumChargeType, objExtParams.MonthlyServiceChargeProrateFactor)
            objMinCostResult = FinalizeCosts(objMinCostColl, objDetailedDef, arrProrateFactors(i))
            objMasterMinCostResult = CombineResults(objMasterMinCostResult, objMinCostResult)
            objMasterMinCostResult.RateCountApplied = objMasterMinCostResult.RateCountApplied + 1

            ' 15.06 Bug 58148 - store all the costs in list without combining the amount for the same cost type
            ' used later to display log info to clarify costtype combine for definition more than 1
            For Each de As DictionaryEntry In objCostResult.CostCollection
                Dim objCost As Cost = CType(de.Value, Cost)
                Dim cost As Cost = New Cost(objCost.CostType, objCost.Amount)
                objCostCollection.Add(cost)
            Next

            objSeasonFactors.Clear()
            objSeasonFactors = Nothing
            i = i + 1
        Next

        If (demandSwapped) Then
            'swap back again
            objUsageColl.SwapVirtualQuantityOfDemandUsages(True)
        End If

        'Rate class modifier - Supply cost type swap.
        If objRateClassModifiers.Exists(RateClassModifierType.SupplyCostTypeSwap) = True Then

            'Retrieve supply cost type swap rate class.
            sSSWapRateClass = objRateClassModifiers.Find(RateClassModifierType.SupplyCostTypeSwap).Value

            'Current rate class is NOT the supply cost type swap rate class.
            If (String.IsNullOrEmpty(sSSWapRateClass) = False) And
               (sRateClassArg <> sSSWapRateClass) Then
                'Remove supply cost type costs from both cost type collections.
                objMasterCostResult = RemoveSuppEngyCostType(objMasterCostResult)
                objMasterMinCostResult = RemoveSuppEngyCostType(objMasterMinCostResult)
            End If

        End If

        ' 15.06 bug 58148 - log the cost type combine info when there're more than 1 definition to clarify cost result confusion
        If (i > 1) Then
            For Each de As DictionaryEntry In objMasterCostResult.CostCollection
                Dim objCost As Cost = CType(de.Value, Cost)
                Dim costs As List(Of Cost) = objCostCollection.FindAll(Function(c) (c.CostType = objCost.CostType))
                If (Not costs Is Nothing) Then
                    Log(costs, objCost.Amount)
                    objCostCollection.RemoveAll(Function(c) (c.CostType = objCost.CostType))
                End If
            Next

            If (objCostCollection.Count > 0) Then
                Dim temp As List(Of Cost) = New List(Of Cost)()
                For Each objCost As Cost In objCostCollection
                    temp.Add(objCost)
                Next

                For Each objCost As Cost In objCostCollection
                    Dim costs As List(Of Cost) = temp.FindAll(Function(c) (c.CostType = objCost.CostType))
                    If (Not costs Is Nothing) Then
                        Log(costs, objCost.Amount)
                        temp.RemoveAll(Function(c) (c.CostType = objCost.CostType))
        End If
                Next
            End If
        End If

        ' Pass rate class modifier collection to finalize cost
        objMasterCostResult = FinalizeMasterCosts(objMasterCostResult, objMasterMinCostResult, objRateClassModifiers)
        objMasterCostResult.ErrorCode = eErrCode

        Log(objMasterCostResult)

        Return (objMasterCostResult)
    End Function


#End Region

#Region "Older CostToDate Overloads temporary"

    <Obsolete("Use the newer overloads.  This exists for temporary compatibility with older alerts logic.")> _
    Public Function CostToDate(ByVal rateCompanyID As Integer, ByVal readings As ReadingCollection, ByVal rateClass As String, ByVal startDate As DateTime, ByVal endDate As DateTime, ByVal projectedEndDate As DateTime, ByVal minChargeType As Enums.MinimumChargeType, ByVal prorateMonthlyServiceCharges As Boolean) As CostToDateResult
        Return (CostToDate(rateCompanyID, readings, rateClass, startDate, endDate, projectedEndDate, New CostToDateExtParam(prorateMonthlyServiceCharges, minChargeType)))
    End Function

    <Obsolete("Use the newer overloads.  This exists for temporary compatibility with older alerts logic.")> _
    Public Function CostToDate(ByVal rateCompanyID As Integer, ByVal readings As ReadingCollection, ByVal currentUsages As UsageCollection, ByVal rateClass As String, ByVal startDate As DateTime, ByVal endDate As DateTime, ByVal projectedEndDate As DateTime, ByVal minChargeType As Enums.MinimumChargeType, ByVal prorateMonthlyServiceCharges As Boolean) As CostToDateResult
        Return (CostToDate(rateCompanyID, readings, currentUsages, rateClass, startDate, endDate, projectedEndDate, New CostToDateExtParam(prorateMonthlyServiceCharges, minChargeType)))
    End Function

    <Obsolete("Use the newer overloads.  This exists for temporary compatibility with older alerts logic.")> _
    Public Function CostToDate(ByVal rateCompanyID As Integer, ByVal usages As UsageCollection, ByVal rateClass As String, ByVal startDate As DateTime, ByVal endDate As DateTime, ByVal projectedEndDate As DateTime, ByVal minChargeType As Enums.MinimumChargeType, ByVal prorateMonthlyServiceCharges As Boolean) As CostToDateResult
        Return (CostToDate(rateCompanyID, usages, rateClass, startDate, endDate, projectedEndDate, New CostToDateExtParam(prorateMonthlyServiceCharges, minChargeType)))
    End Function

#End Region

#Region "CostToDate Overloads"

    ''' <summary>
    ''' This cost-to-date expects the full readings collection, since this call does ALL the work in a single call
    ''' This bins, gets usage determinants, and then calculates prorated cost, as well as projected cost
    ''' This is the call that should be used for best projected cost calculation for when TOU+tier and RTP are in-play, or when tou boundaries are inconsistent
    ''' </summary>
    ''' <param name="rateCompanyID"></param>
    ''' <param name="readings"></param>
    ''' <param name="rateClass"></param>
    ''' <param name="startDate"></param>
    ''' <param name="endDate"></param>
    ''' <param name="projectedEndDate"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function CostToDate(ByVal rateCompanyID As Integer, ByVal readings As ReadingCollection, ByVal rateClass As String, ByVal startDate As DateTime, ByVal endDate As DateTime, ByVal projectedEndDate As DateTime, ByVal extParam As CostToDateExtParam) As CostToDateResult
        Dim baselines As BaselineCollection = Nothing
        Return (CostToDate(rateCompanyID, readings, baselines, rateClass, startDate, endDate, projectedEndDate, extParam))
    End Function

    Public Function CostToDate(ByVal rateCompanyID As Integer, ByVal readings As ReadingCollection, ByVal baselines As BaselineCollection, ByVal rateClass As String, ByVal startDate As DateTime, ByVal endDate As DateTime, ByVal projectedEndDate As DateTime, ByVal extParam As CostToDateExtParam) As CostToDateResult
        Return (CostToDate(rateCompanyID, readings, baselines, rateClass, startDate, endDate, projectedEndDate, extParam, False))
    End Function

    ''' <summary>
    ''' This cost-to-date expects the full readings collection, and it can also except baselines, for when PTR (Peak-Time-Rebates) are desirable
    ''' This bins, gets usage determinants, and then calculates prorated cost, as well as projected cost, and rebates are included if applicable
    ''' </summary>
    ''' <param name="rateCompanyID"></param>
    ''' <param name="readings"></param>
    ''' <param name="baselines"></param>
    ''' <param name="rateClass"></param>
    ''' <param name="startDate"></param>
    ''' <param name="endDate"></param>
    ''' <param name="projectedEndDate"></param>
    ''' <param name="extParam"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function CostToDate(ByVal rateCompanyID As Integer, ByVal readings As ReadingCollection, ByVal baselines As BaselineCollection, ByVal rateClass As String, ByVal startDate As DateTime, ByVal endDate As DateTime, ByVal projectedEndDate As DateTime, ByVal extParam As CostToDateExtParam, ByVal getNewestDefinitionAndResetStartDate As Boolean) As CostToDateResult
        Dim result As New CostToDateResult()
        Dim detailedDefs As DetailedRateDefinitionCollection
        Dim usages As UsageCollection
        Dim dtDSTEndDate As DateTime
        Dim raf As ReadingAdjustFactors = Nothing

        dtDSTEndDate = ConvertDSTDate(extParam.DSTEndDate)

        If (Not readings Is Nothing AndAlso readings.Count > 0) Then

            detailedDefs = GetDetailedRateInformationUsingReadingsDateRange(rateCompanyID, readings, rateClass, getNewestDefinitionAndResetStartDate)

            If (detailedDefs.Count > 0) Then

                ' Get Usage Determinants from binned readings
                If (extParam.AdjustReadings) Then
                    raf = New ReadingAdjustFactors(extParam.GeneralConservationRAF, extParam.TypicalResponseRAF, extParam.CriticalResponseRAF, extParam.DemandControlRAF, extParam.PercentReductionAsShiftedRAF, extParam.PercentCriticalReductionAsShiftedRAF)
                End If

                ' CR 51610 Oct 2014 - set up the usages collection for daily readings
                If (readings.DetermineIntervalOfReadings = ReadingInterval.Daily) Then
                    usages = GetDailyUsageDeterminants(readings, rateCompanyID, rateClass, startDate, endDate, projectedEndDate, dtDSTEndDate, detailedDefs, raf, extParam)
                Else
                usages = BinReadingsAndGetUsageDeterminants(rateCompanyID, detailedDefs, readings, 0.0, rateClass, startDate, endDate, projectedEndDate, extParam.UseProjectedEndDateWhenFinalizingTierBoundaries, dtDSTEndDate, raf, extParam.PeakEventInformation)

                If (detailedDefs(0).IsRTPRate Or (detailedDefs(0).IsTimeOfUse AndAlso detailedDefs(0).IsTiered AndAlso detailedDefs(0).UseCharges.ExistsAnyTierTOU()) Or _
                    (detailedDefs(0).IsTimeOfUse AndAlso (Not detailedDefs(0).BoundariesConsistent)) Or _
                    (detailedDefs(0).IsTiered AndAlso detailedDefs(0).TierStructureType = TierStructureType.MonthlyBoundsHybrid1)) Then

                    ' Attach readings to first usage if Real-Time-Pricing OR if TOU+Tiered OR Tiered+Hybrid OR of TOU boundaries are not consistent
                    usages(0).Readings = readings
                    usages(0).Readings.ValidForRTP = True

                    ' Attach baselines to first usage if baselines were passed, and if PTR in-play
                    If (Not baselines Is Nothing AndAlso detailedDefs(0).RTPStream = RTPStream.PeakTimeRebateOnly) Then
                        If (CheckIfDemandResponseEligible(rateCompanyID, rateClass) = True) Then
                            usages(0).Baselines = baselines
                        End If
                    End If

                End If
                End If

                ' CR 52497 Nov 2014 special logic for Absolute NEM Tiers Meter
                Dim continueToMainCostToDate As Boolean = True
                ' check if the new tiers logic should apply to NEM meters
                If IsNegativeUsageExistsForNEMTiers(usages, detailedDefs(0), rateClass) Then
                    ' check if the interval is 15, 30 or 60  mins
                    Dim interval As Enums.ReadingInterval = readings.DetermineIntervalOfReadings
                    If (interval = ReadingInterval.FifteenMinute OrElse interval = ReadingInterval.HalfHourly OrElse interval = ReadingInterval.Hourly) AndAlso detailedDefs(0).FuelType = FuelType.Electric Then
                        ' log usage before adjustment
                        Log(ksOriginalUsageCollection)
                        Log(usages)

                        ' call AdjustTierUsageWithAbsoluteTotal to get the new usage collection
                        usages = AdjustTierUsageWithAbsoluteTotal(usages, detailedDefs(0), rateCompanyID, rateClass, startDate, endDate, projectedEndDate, extParam.UseProjectedEndDateWhenFinalizingTierBoundaries)

                        ' log usage after adjustment
                        Log(ksNEMTierAdjustedUsageCollection)
                        Log(usages)
                    Else
                        result.ErrorCode = ErrCode.ErrUnsupportedCalculation
                        continueToMainCostToDate = False
                    End If
                End If

                If (continueToMainCostToDate) Then
                result = MainCostToDate(rateCompanyID, detailedDefs, usages, rateClass, startDate, endDate, projectedEndDate, extParam)
                End If

                ' set this value on result since may be useful to some callers
                result.LastReadingDate = GetLastReadingDate(readings)

            Else
                result.ErrorCode = ErrCode.ErrRateDefinitionsNotFound
            End If

        Else
            result.ErrorCode = ErrCode.ErrRequiredReadingsAreMissing
        End If

        Return (result)
    End Function

    ''' <summary>
    ''' This call accepts readings and existing usage.  The readings are binned and the usages combined.  Then CostToDate is calculated.
    ''' Cannot be used with RTP rates classes, return error if that is the case, any RTP rate class
    ''' Cannot be used with rate classes that have inconsistent TOU boundaries in different seasons.
    ''' </summary>
    ''' <param name="rateCompanyID"></param>
    ''' <param name="readings"></param>
    ''' <param name="currentUsages"></param>
    ''' <param name="rateClass"></param>
    ''' <param name="startDate"></param>
    ''' <param name="endDate"></param>
    ''' <param name="projectedEndDate"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function CostToDate(ByVal rateCompanyID As Integer, ByVal readings As ReadingCollection, ByVal currentUsages As UsageCollection, ByVal rateClass As String, ByVal startDate As DateTime, ByVal endDate As DateTime, ByVal projectedEndDate As DateTime, ByVal extParam As CostToDateExtParam) As CostToDateResult
        Dim result As New CostToDateResult()
        Dim detailedDefs As DetailedRateDefinitionCollection
        Dim usageDeterminantsFromReadings As UsageCollection
        Dim totalCurrentUsage As Double
        Dim dtDSTEndDate As DateTime

        dtDSTEndDate = ConvertDSTDate(extParam.DSTEndDate)

        detailedDefs = GetDetailedRateInformation(rateCompanyID, rateClass, startDate, endDate)

        If (detailedDefs.Count > 0) Then

            If (Not detailedDefs(0).IsRTPRate AndAlso detailedDefs(0).BoundariesConsistent) Then

                ' for complex tiered tou rate class provide the offset from currentUsage
                If (Not detailedDefs(0).UseCharges.ExistsAnyTierTOU()) Then
                    totalCurrentUsage = 0.0
                Else
                    totalCurrentUsage = currentUsages.GetTotalServiceUse()
                End If

                ' Get Usage Determinants from binned readings
                If (readings.DetermineIntervalOfReadings() = ReadingInterval.Daily) Then
                    usageDeterminantsFromReadings = GetDailyUsageDeterminants(readings, rateCompanyID, rateClass, startDate, endDate, projectedEndDate, dtDSTEndDate, detailedDefs, Nothing, extParam)
                Else
                usageDeterminantsFromReadings = BinReadingsAndGetUsageDeterminants(rateCompanyID, detailedDefs, readings, totalCurrentUsage, rateClass, startDate, endDate, dtDSTEndDate)
                End If

                ' Check for TOU usages and create a total for consistency
                CheckAndCreateTotalUsage(currentUsages)

                ' Combine with existing usage passed-in
                currentUsages.Combine(usageDeterminantsFromReadings)

                ' CR 52497 Nov 2014 special logic for Absolute NEM Tiers Meter
                Dim continueToMainCostToDate As Boolean = True
                ' check if the new tiers logic should apply to NEM meters
                If IsNegativeUsageExistsForNEMTiers(currentUsages, detailedDefs(0), rateClass) Then
                    ' check if the interval is 15, 30 or 60  mins
                    Dim interval As Enums.ReadingInterval = readings.DetermineIntervalOfReadings
                    If (interval = ReadingInterval.FifteenMinute OrElse interval = ReadingInterval.HalfHourly OrElse interval = ReadingInterval.Hourly) AndAlso detailedDefs(0).FuelType = FuelType.Electric Then
                        ' log usage before adjustment
                        Log(ksOriginalUsageCollection)
                        Log(currentUsages)

                        ' call AdjustTierUsageWithAbsoluteTotal to get the new usage collection
                        currentUsages = AdjustTierUsageWithAbsoluteTotal(currentUsages, detailedDefs(0), rateCompanyID, rateClass, startDate, endDate, projectedEndDate, extParam.UseProjectedEndDateWhenFinalizingTierBoundaries)

                        ' log usage after adjustment
                        Log(ksNEMTierAdjustedUsageCollection)
                        Log(currentUsages)
                    Else
                        result.ErrorCode = ErrCode.ErrUnsupportedCalculation
                        continueToMainCostToDate = False
                    End If
                End If

                If (continueToMainCostToDate) Then
                result = MainCostToDate(rateCompanyID, detailedDefs, currentUsages, rateClass, startDate, endDate, projectedEndDate, extParam)
                End If

                ' set this value on result since may be useful to some callers
                result.LastReadingDate = GetLastReadingDate(readings)

            Else
                'need readings for RTP and when tou boundaries are not seasonaly constant
                result.ErrorCode = ErrCode.ErrAllReadingsNotProvidedForRTP
            End If

        Else
            result.ErrorCode = ErrCode.ErrRateDefinitionsNotFound
        End If

        Return (result)
    End Function

    ''' <summary>
    ''' This call simply uses the provided usage and calculates CostToDate
    ''' Cannot be used with RTP rates classes, return error if that is the case, any RTP rate class
    ''' </summary>
    ''' <param name="rateCompanyID"></param>
    ''' <param name="usages"></param>
    ''' <param name="rateClass"></param>
    ''' <param name="startDate"></param>
    ''' <param name="endDate"></param>
    ''' <param name="projectedEndDate"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function CostToDate(ByVal rateCompanyID As Integer, ByVal usages As UsageCollection, ByVal rateClass As String, ByVal startDate As DateTime, ByVal endDate As DateTime, ByVal projectedEndDate As DateTime, ByVal extParam As CostToDateExtParam) As CostToDateResult
        Return (CostToDate(rateCompanyID, usages, rateClass, startDate, endDate, projectedEndDate, extParam, False))
    End Function

    Public Function CostToDate(ByVal rateCompanyID As Integer, ByVal usages As UsageCollection, ByVal rateClass As String, ByVal startDate As DateTime, ByVal endDate As DateTime, ByVal projectedEndDate As DateTime, ByVal extParam As CostToDateExtParam, ByVal getNewestDefinitionAndResetStartDate As Boolean) As CostToDateResult
        Dim result As CostToDateResult = Nothing
        Dim detailedDefs As DetailedRateDefinitionCollection
        If (getNewestDefinitionAndResetStartDate) Then

            Const yearOffset As Integer = 5
            Dim defStartDate As DateTime = DateTime.Now.AddYears(yearOffset)
            Dim defEndDate As DateTime = defStartDate.AddDays(2)

            ' Get the newest rate definition by adding 5 years to the startDate, and then reset the definition startDate back 5 years from the effective date! 
            ' Use a parameter that artificially changes the rate definition effective date internally; 
            ' This overload was added to aid in proper caching support, since callers should not change the start date after definitions are returned (since they may come from cache!)
            detailedDefs = GetDetailedRateInformation(rateCompanyID, rateClass, defStartDate, defEndDate, -yearOffset)

        Else
        detailedDefs = GetDetailedRateInformation(rateCompanyID, rateClass, startDate, endDate)
        End If

        If (detailedDefs.Count > 0) Then

            If (Not detailedDefs(0).IsRTPRate AndAlso detailedDefs(0).BoundariesConsistent) Then

                ' Check for TOU usages and create a total for consistency
                CheckAndCreateTotalUsage(usages)

                ' CR 52497 Nov 2014 special logic for Absolute NEM Tiers Meter
                Dim continueToMainCostToDate As Boolean = True
                ' check if the new tiers logic should apply to NEM meters
                If IsNegativeUsageExistsForNEMTiers(usages, detailedDefs(0), rateClass) Then
                    ' check if the interval is 15, 30 or 60  mins
                    If detailedDefs(0).FuelType = FuelType.Electric Then
                        ' log usage before adjustment
                        Log(ksOriginalUsageCollection)
                        Log(usages)

                        ' call AdjustTierUsageWithAbsoluteTotal to get the new usage collection
                        usages = AdjustTierUsageWithAbsoluteTotal(usages, detailedDefs(0), rateCompanyID, rateClass, startDate, endDate, projectedEndDate, extParam.UseProjectedEndDateWhenFinalizingTierBoundaries)

                        ' log usage after adjustment
                        Log(ksNEMTierAdjustedUsageCollection)
                        Log(usages)
            Else
                        result.ErrorCode = ErrCode.ErrUnsupportedCalculation
                        continueToMainCostToDate = False
                    End If
                End If

                If (continueToMainCostToDate) Then
                    result = MainCostToDate(rateCompanyID, detailedDefs, usages, rateClass, startDate, endDate, projectedEndDate, extParam)
                End If
            Else
                'need readings for RTP and when tou boundaries are not seasonaly constant
                result.ErrorCode = ErrCode.ErrAllReadingsNotProvidedForRTP
            End If

        Else
            result.ErrorCode = ErrCode.ErrRateDefinitionsNotFound
        End If

        Return (result)
    End Function

    Private Function MainCostToDate(ByVal rateCompanyID As Integer, ByVal detailedDefs As DetailedRateDefinitionCollection, ByVal usages As UsageCollection, ByVal rateClass As String, ByVal startDate As DateTime, ByVal endDate As DateTime, ByVal projectedEndDate As DateTime, ByVal extParam As CostToDateExtParam) As CostToDateResult
        Dim result As New CostToDateResult()
        Dim calculateCostResult As CostResult
        Dim projectedCalculateCostResult As CostResult
        Dim extraParams As CalculateCostExtParam
        Dim projectedExtraParams As CalculateCostExtParam
        Dim projectedNumDays As Double = 0
        Dim currNumDays As Double = 0
        Dim currSpan As TimeSpan
        Dim projectedSpan As TimeSpan
        Dim monthProrateFactor As Double = 1.0
        Dim usageScaleFactor As Double = 1.0
        Dim projectedUsages As UsageCollection = Nothing
        Dim avgDailyCost As Double = 0.0
        Dim projectedUsageSuccess As Boolean = True
        Dim projectedCostValid As Boolean = True
        Dim currentPointInBillCycle As Double = 0
        Dim projectedReadings As ReadingCollection = Nothing
        Dim peakTimeRebateResult As PTRResult = Nothing
        Dim totalPeakTimeRebateAmount As Double = 0.0
        Dim touBoundaryInconsistentWithProjectedOverlap As Boolean

        ' calculate day spans from provided dates; endpoints are inclusive
        currSpan = endDate.Subtract(startDate)
        currNumDays = Math.Floor(currSpan.TotalDays) + 1

        'projectedNumDays = extParam.ProjectedNumDays <-- was original code; was using old settings based number; use the one passed-in!
        projectedSpan = projectedEndDate.Subtract(startDate)
        projectedNumDays = Math.Floor(projectedSpan.TotalDays) + 1
        'If (projectedNumDays < extParam.ProjectedNumDays) Then
        '    projectedNumDays = extParam.ProjectedNumDays
        'End If

        If (projectedNumDays > 0) Then
            'set the currNumber of days to full month (projected num days) when special properties in play
            If (extParam.CheckDaysInFullMonth AndAlso currNumDays >= extParam.DaysInFullMonth) Then
                monthProrateFactor = 1
            Else
                monthProrateFactor = (currNumDays / projectedNumDays)
            End If
        End If

        '' lessen the demand usage determinants if enabled;
        'If (extParam.ProrateDemandUsageDeterminants) Then
        '    ProrateDemandUsage(usages, monthProrateFactor)
        'End If


        ' CURRENT CostToDate calculation; 
        ' bug 33447: mganley; FOR TIERED RATE CLASSES ONLY, the endDate should be the projectedEndDate when calling CalculateCost from within this CostToDate function; 
        ' This was a long-standing bug whereby tiered cost calculations from this CostToDate API for percentBaseTier1 and Daily bounded 
        ' rate classes were using incorrect tier boundaries; the date range should always be the full range, its the usages that are varied between
        ' this call to CalculateCost and the next call to CalculateCost, based upon the factors used to scale for the simpler rates, and 
        ' the more complex determinants calculated after projecting readings.  Added logic to take in the projected end date for use within the CalculateCost ProcessUseCharges.
        If (detailedDefs(0).IsTiered) Then
            extraParams = New CalculateCostExtParam(extParam.ProrateMonthlyServiceCharges, extParam.ProrateDemandUsageDeterminants, monthProrateFactor, extParam.MinimumChargeType, extParam.UseProjectedEndDateWhenFinalizingTierBoundaries, projectedEndDate)
        Else
            extraParams = New CalculateCostExtParam(extParam.ProrateMonthlyServiceCharges, extParam.ProrateDemandUsageDeterminants, monthProrateFactor, extParam.MinimumChargeType)
        End If



        ' Now do extra ADD-ON calculations that are not part of primary CalculateCost
        ' First possibility is PTR; note that this same result will used with projected cost result, since projection will not contain additional PTR at this time
        If (DoesPTRExist(detailedDefs, startDate, endDate)) Then

            If (Not usages(0).Readings Is Nothing AndAlso Not usages(0).Baselines Is Nothing) Then

                Dim ptrEvents As PTREventCollection = GetPTREvents(detailedDefs, startDate, endDate, extParam.PeakEventInformation, usages(0).Readings)

                extParam.RoundingDecimalPlaces = GetRoundingDecimalPlacesValue(rateClass)

                ' AllowBaselineCalculations indicates whether to allow readings to be used to calculate a baseline value if that value is not available; default is FALSE
                ptrEvents = FinalizePTREvents(ptrEvents, usages(0).Readings, usages(0).Baselines, extParam.AllowBaselineCalculations, extParam.RoundingDecimalPlaces, detailedDefs(0).HolidayGroupID)

                ' AllowRebateCalculations indicates whether to allow a calculation of a rebate, or to use what is provided with baseline data; default is FALSE
                peakTimeRebateResult = CalculatePTREventRebates(detailedDefs, rateCompanyID, rateClass, startDate, endDate, usages(0).Readings, ptrEvents, extParam.AllowRebateCalculations, extParam.RoundingDecimalPlaces)

                ' get totalRebate to exclude from resulting cost and projected cost
                If (Not peakTimeRebateResult Is Nothing) Then
                    totalPeakTimeRebateAmount = peakTimeRebateResult.GetFinalRebateAmount()

                    'If (Not extParam.PeakEventInformation Is Nothing) Then

                    If (Not peakTimeRebateResult.PTREvents Is Nothing) Then

                        For Each pe As PTREvent In peakTimeRebateResult.PTREvents
                            If (Not pe.Rebate Is Nothing) Then
                                extraParams.RebateAmount = extraParams.RebateAmount + pe.Rebate.RebateAmount
                                extraParams.RebateExists = True
                            End If
                        Next

                        extraParams.RebateAmount = Math.Abs(extraParams.RebateAmount) * -1

                    End If

                    'End If

                End If

            End If

        End If


        ' CR 51610 Oct 2014
        ' if rate class is complex rate and there's only daily readings, 
        ' then we set the error instead of making the call to calculate
        If (extParam.IsDailyOnlyReadings AndAlso IsComplexRate(detailedDefs(0))) Then
            calculateCostResult = New CostResult()
            calculateCostResult.ErrorCode = ErrCode.ErrUnsupportedComplexTieredTOU
        Else
        calculateCostResult = CalculateCost(rateCompanyID, detailedDefs, rateClass, startDate, endDate, usages, extraParams)
        End If

        ' must reset the error code from CalculateCost when it indicated ErrTOUComplex. 
        ' that is not really an error, and only an indicator of complexity, added in the past for use by some callers,
        ' but not useful here.  That should have been implemented more clearly in the past.
        If (calculateCostResult.ErrorCode = ErrCode.ErrTOUComplex) Then
            calculateCostResult.ErrorCode = ErrCode.NoError
        End If

        If (Not extParam.OmitProjectedCostCalculation) Then

            ' PROJECTED CostToDate Calculation

            '' reset any previously prorated demand at top of btd above
            'If (extParam.ProrateDemandUsageDeterminants) Then
            '    ProrateDemandUsage(usages, (1.0 / monthProrateFactor))
            'End If

            ' adjust usages
            If (monthProrateFactor = 1) Then
                currentPointInBillCycle = currNumDays / projectedNumDays
            Else
                currentPointInBillCycle = monthProrateFactor
            End If
            If (currentPointInBillCycle > 0) Then
                usageScaleFactor = 1.0 / currentPointInBillCycle
            End If

            touBoundaryInconsistentWithProjectedOverlap = DoesProjectedEndDateOverlapSeasonBoundaryWithInconsistentTOU(detailedDefs, startDate, endDate, projectedEndDate)
            Dim bComplexRateClass As Boolean = False
            ' CR 51610 Oct 2014 - check for if it's complex rate before projecting the usage
            If (Not (IsComplexRate(detailedDefs(0)) Or touBoundaryInconsistentWithProjectedOverlap)) Then
                ' Do the simple projection
                If (Not detailedDefs(0).UseCharges.ExistsSeasonalTOUAndNoTiers()) Then
                    ' Create Copy to use for scaling
                    projectedUsages = usages.Copy()

                    ' Scale usage for projection
                    projectedUsages.Scale(usageScaleFactor)
                    projectedUsageSuccess = True

                Else
                    Log("[Projected Usages Adjusted]")

                    projectedUsages = AdjustSeasonalUsageDeterminants(rateCompanyID, detailedDefs, usages, rateClass, startDate, endDate, projectedEndDate, usageScaleFactor, extraParams.DwellingCount, extParam.DaysInFullMonth)

                    If (Not projectedUsages Is Nothing) Then
                        If (projectedUsages.Count = 0) Then
                            projectedUsageSuccess = False
                        End If
                    Else
                        projectedUsageSuccess = False
                    End If

                End If

            Else
                ' CR 51610 Oct 2014
                ' Else Weekday/weekend interval projection if it's not only daily reading
                ' if daily only, mark as complex rate class and projected usage not success
                If (extParam.IsDailyOnlyReadings) Then
                    bComplexRateClass = True
                    projectedUsageSuccess = False
                Else
                ' This is a Tiered+TOU and/or Real-time-pricing rate class or tou boundaries inconsistent across seasons for projected date range
                ' Or ComplexHybrid which means tiered and MonthyBoundsHybrid1
                ' Therefore, for the best projected bill-to-date, we (1) project the readings out to the projectedEndDate, 
                ' (2) bin and create usage determinants again, (3) attach readings to first usage again, 
                ' (4) and then calculate cost 

                If (Not usages(0).Readings Is Nothing) Then
                    Dim avgWeekdayDay As ReadingCollection = Nothing
                    Dim avgWeekendDay As ReadingCollection = Nothing
                    projectedReadings = usages(0).Readings.GetReadingsForFullDateRange()
                    Dim interval As ReadingInterval = projectedReadings.DetermineIntervalOfReadings()
                    Dim readingsStartDate As DateTime = projectedReadings(0).Datestamp

                    Select Case interval
                        Case ReadingInterval.Hourly
                            avgWeekdayDay = projectedReadings.GetAvgHourlyReadingsFor(DayType.Weekday)
                            avgWeekendDay = projectedReadings.GetAvgHourlyReadingsFor(DayType.Weekend)

                        Case ReadingInterval.HalfHourly
                            avgWeekdayDay = projectedReadings.GetAvgHalfHourlyReadingsFor(DayType.Weekday)
                            avgWeekendDay = projectedReadings.GetAvgHalfHourlyReadingsFor(DayType.Weekend)

                        Case ReadingInterval.FifteenMinute
                            avgWeekdayDay = projectedReadings.GetAvg15MinuteReadingsFor(DayType.Weekday)
                            avgWeekendDay = projectedReadings.GetAvg15MinuteReadingsFor(DayType.Weekend)

                        Case ReadingInterval.FiveMinute
                            avgWeekdayDay = projectedReadings.GetAvgFiveMinuteReadingsFor(DayType.Weekday)
                            avgWeekendDay = projectedReadings.GetAvgFiveMinuteReadingsFor(DayType.Weekend)

                        Case ReadingInterval.Daily
                            projectedUsageSuccess = False

                        Case Else
                            projectedUsageSuccess = False

                    End Select

                    If (projectedUsageSuccess) Then

                        If (avgWeekdayDay.Count = 0) Then
                            avgWeekdayDay = avgWeekendDay
                        End If

                        If (avgWeekendDay.Count = 0) Then
                            avgWeekendDay = avgWeekdayDay
                        End If

                        If (projectedEndDate > projectedReadings(projectedReadings.Count - 1).Datestamp) Then

                            Select Case interval
                                Case ReadingInterval.Hourly
                                    projectedReadings.FillMissingIntervalReadingsWithAverage(readingsStartDate, projectedEndDate, avgWeekdayDay, avgWeekendDay, False, ReadingInterval.Hourly)

                                Case ReadingInterval.HalfHourly
                                    projectedReadings.FillMissingIntervalReadingsWithAverage(readingsStartDate, projectedEndDate, avgWeekdayDay, avgWeekendDay, False, ReadingInterval.HalfHourly)

                                Case ReadingInterval.FifteenMinute
                                    projectedReadings.FillMissingIntervalReadingsWithAverage(readingsStartDate, projectedEndDate, avgWeekdayDay, avgWeekendDay, False, ReadingInterval.FifteenMinute)

                                Case ReadingInterval.FiveMinute
                                    projectedReadings.FillMissingIntervalReadingsWithAverage(readingsStartDate, projectedEndDate, avgWeekdayDay, avgWeekendDay, False, ReadingInterval.FiveMinute)

                            End Select

                        End If

                        ' bin the total projectedReadings;  in most cases additional readings were added in the if statement above.
                        ' On the last day of a cycle, no additional readings are added, and the usages projected are straight from the unmodified projected readings.
                        projectedUsages = BinReadingsAndGetUsageDeterminants(rateCompanyID, detailedDefs, projectedReadings, rateClass, readingsStartDate, projectedEndDate, ConvertDSTDate(extParam.DSTEndDate))


                    End If

                Else
                    projectedUsageSuccess = False
                End If

                ' Get new adjusted usages for complex rate classes
                'projectedUsages = AdjustComplexUsageDeterminants(rateCompanyID, detailedDefs, usages, rateClass, startDate, endDate, projectedEndDate, usageScaleFactor, extraParams.DwellingCount, extParam.DaysInFullMonth)

                If (Not projectedUsages Is Nothing) Then
                    If (projectedUsages.Count = 0) Then
                        projectedUsageSuccess = False
                    End If
                Else
                    projectedUsageSuccess = False
                End If
            End If
            End If

            ' Projected CostToDate Calculation
            If (projectedUsageSuccess) Then

                If (detailedDefs(0).IsRTPRate Or touBoundaryInconsistentWithProjectedOverlap) Then
                    projectedUsages(0).Readings = projectedReadings
                    projectedUsages(0).Readings.ValidForRTP = True
                End If

                projectedExtraParams = New CalculateCostExtParam(False, False, extParam.MinimumChargeType)

                ' add rebate into to cost collection if applicable;  total used from above already when providing the total costs at end of this function.
                If (Not peakTimeRebateResult Is Nothing) Then

                    'If (Not extParam.PeakEventInformation Is Nothing) Then

                    If (Not peakTimeRebateResult.PTREvents Is Nothing) Then

                        For Each pe As PTREvent In peakTimeRebateResult.PTREvents
                            If (Not pe.Rebate Is Nothing) Then
                                projectedExtraParams.RebateAmount = projectedExtraParams.RebateAmount + pe.Rebate.RebateAmount
                                projectedExtraParams.RebateExists = True
                            End If
                        Next

                        projectedExtraParams.RebateAmount = Math.Abs(projectedExtraParams.RebateAmount) * -1

                    End If

                    'End If

                End If



                projectedCalculateCostResult = CalculateCost(rateCompanyID, detailedDefs, rateClass, startDate, projectedEndDate, projectedUsages, projectedExtraParams)

                ' must reset the error code from CalculateCost when it indicated ErrTOUComplex. 
                ' that is not really an error, and only an indicator of complexity, added in the past for use by some callers,
                ' but not useful here.  That should have been implemented more clearly in the past.
                If (projectedCalculateCostResult.ErrorCode = ErrCode.ErrTOUComplex) Then
                    projectedCalculateCostResult.ErrorCode = ErrCode.NoError
                End If

            ElseIf (Not projectedUsageSuccess AndAlso bComplexRateClass) Then
                ' CR 51610 Oct 2014
                ' if it's complex rate class, set the error for projected calculation
                projectedCalculateCostResult = New CostResult()
                projectedCalculateCostResult.ErrorCode = ErrCode.ErrUnsupportedComplexTieredTOU
            Else
                projectedCalculateCostResult = New CostResult()
                projectedCalculateCostResult.ErrorCode = ErrCode.ErrUnsupportedProjectedCalculation
            End If

        Else
            ' projected cost calculation was omitted by the caller.  Create an empty result.
            projectedCalculateCostResult = New CostResult()
        End If


        ' Set average cost
        If (currNumDays > 0) Then
            avgDailyCost = calculateCostResult.TotalCost / currNumDays
        End If

        ' Initialize result object
        result.Cost = calculateCostResult.TotalCost
        result.ProjectedCost = projectedCalculateCostResult.TotalCost
        result.AverageDailyCost = avgDailyCost
        result.StartDate = startDate
        result.EndDate = endDate
        result.ProjectedEndDate = projectedEndDate
        result.NumberOfDaysInCostToDate = CInt(currNumDays)
        result.NumberOfDaysInProjectedCost = CInt(projectedNumDays)
        result.MonthProrateFactor = monthProrateFactor
        result.UsageScaleFactor = usageScaleFactor
        result.Usages = usages
        result.ProjectedUsages = projectedUsages
        result.MinimumCostsApplied = calculateCostResult.MinimumCostsApplied
        result.ProjectedCostErrorCode = projectedCalculateCostResult.ErrorCode
        result.ErrorCode = calculateCostResult.ErrorCode
        result.PeakTimeRebates = peakTimeRebateResult
        result.ProjectedPeakTimeRebates = peakTimeRebateResult
        result.Costs = calculateCostResult.CostCollection
        result.ProjectedCosts = projectedCalculateCostResult.CostCollection
        result.NetMeteringResult = calculateCostResult.NetMeteringResult
        result.ProjectedNetMeteringResult = projectedCalculateCostResult.NetMeteringResult

        Return (result)
    End Function

    Private Function ProrateDemandUsage(ByVal currentUsages As UsageCollection, ByVal prorateFactor As Double) As Boolean
        currentUsages.ScaleDemand(prorateFactor)
        Return (True)
    End Function

    Private Function CheckAndCreateTotalUsage(ByVal currentUsages As UsageCollection) As Boolean
        Dim tempTotal As Double = 0.0

        ' If passed-in usage does not have a TotalServiceUse, combine the TOU Usage and create Total
        ' This creates a consistent collection of usages to combine below
        If (Not currentUsages.Exists(BaseOrTier.TotalServiceUse, TimeOfUse.Undefined, Season.Undefined)) Then
            tempTotal = currentUsages.GetTotalTimeOfUseUsage()
            If (tempTotal <> 0) Then
                currentUsages.Insert(0, New Usage(tempTotal))
            End If
        End If

        Return (True)
    End Function

    Private Function GetLastReadingDate(ByVal readings As ReadingCollection) As DateTime?
        Dim dt As DateTime? = Nothing
        If (Not readings Is Nothing) Then
            If (readings.Count > 0) Then
                For i As Integer = 1 To readings.Count
                    If Not readings(readings.Count - i).IsEmpty Then
                        dt = readings(readings.Count - i).Datestamp
                        Exit For
                    End If
                Next
            End If
        End If
        Return (dt)
    End Function

#End Region

#Region "CalculateExcessGenerationRebate Overloads"

    ''' <summary>
    ''' Returns the total excess generation rebate amount.  Used for Net Metering.
    ''' </summary>
    ''' <param name="rateCompanyID"></param>
    ''' <param name="usages"></param>
    ''' <param name="rateClass"></param>
    ''' <param name="startDate"></param>
    ''' <param name="endDate"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function CalculateExcessGenerationRebate(ByVal rateCompanyID As Integer, ByVal usages As UsageCollection, ByVal rateClass As String, ByVal startDate As DateTime, ByVal endDate As DateTime) As Double
        Dim result As Double = 0.0
        Dim detailedDefs As DetailedRateDefinitionCollection

        detailedDefs = GetDetailedRateInformation(rateCompanyID, rateClass, startDate, endDate)

        If (detailedDefs.Count > 0) Then
            If ((detailedDefs(0).IsNetMetering AndAlso Not rateClass.StartsWith(ksnRateClassHash)) Or (RateClassAttributeIndicatesNetMetering(rateClass) AndAlso rateClass.StartsWith(ksnRateClassHash))) Then
                result = MainCalculateExcessGenerationRebate(rateCompanyID, detailedDefs, usages, rateClass, startDate, endDate)
            End If
        End If

        Return (result)
    End Function

    ''' <summary>
    ''' Returns the total excess generation rebate amount.  Used for Net Metering.
    ''' </summary>
    ''' <param name="rateCompanyID"></param>
    ''' <param name="detailedDefs"></param>
    ''' <param name="usages"></param>
    ''' <param name="rateClass"></param>
    ''' <param name="startDate"></param>
    ''' <param name="endDate"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function CalculateExcessGenerationRebate(ByVal rateCompanyID As Integer, ByVal detailedDefs As DetailedRateDefinitionCollection, ByVal usages As UsageCollection, ByVal rateClass As String, ByVal startDate As DateTime, ByVal endDate As DateTime) As Double
        Dim result As Double = 0.0

        If (Not detailedDefs Is Nothing) Then
            If (detailedDefs.Count > 0) Then
                If ((detailedDefs(0).IsNetMetering AndAlso Not rateClass.StartsWith(ksnRateClassHash)) Or (RateClassAttributeIndicatesNetMetering(rateClass) AndAlso rateClass.StartsWith(ksnRateClassHash))) Then
                    result = MainCalculateExcessGenerationRebate(rateCompanyID, detailedDefs, usages, rateClass, startDate, endDate)
                End If
            End If
        End If

        Return (result)
    End Function

    ''' <summary>
    ''' Returns the total excess generation rebate amount.  Used for Net Metering.
    ''' </summary>
    ''' <param name="rateCompanyID"></param>
    ''' <param name="detailedDefs"></param>
    ''' <param name="usages"></param>
    ''' <param name="rateClass"></param>
    ''' <param name="startDate"></param>
    ''' <param name="endDate"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function MainCalculateExcessGenerationRebate(ByVal rateCompanyID As Integer, ByVal detailedDefs As DetailedRateDefinitionCollection, ByVal usages As UsageCollection, ByVal rateClass As String, ByVal startDate As DateTime, ByVal endDate As DateTime) As Double
        Dim result As Double = 0.0
        Dim quantity As Double

        For Each charge As ServiceCharge In detailedDefs(0).ServiceCharges

            Select Case charge.CalculationType
                Case ChargeCalcType.AnnExcGen
                    quantity = usages.GetTotalServiceUse()

                Case ChargeCalcType.AnnExcGenP
                    quantity = usages.GetTotalTimeOfUseUsage(Enums.TimeOfUse.OnPeak)

                Case ChargeCalcType.AnnExcGenOP
                    quantity = usages.GetTotalTimeOfUseUsage(Enums.TimeOfUse.OffPeak)

                Case ChargeCalcType.AnnExcGenS1
                    quantity = usages.GetTotalTimeOfUseUsage(Enums.TimeOfUse.Shoulder1)

                Case ChargeCalcType.AnnExcGenS2
                    quantity = usages.GetTotalTimeOfUseUsage(Enums.TimeOfUse.Shoulder2)

                Case ChargeCalcType.AnnExcGenCP
                    quantity = usages.GetTotalTimeOfUseUsage(Enums.TimeOfUse.CriticalPeak)

                Case Else
                    quantity = 0    'not an excess generation charge

            End Select

            result = result + (quantity * charge.ChargeValue)

        Next

        Return (result)
    End Function

#End Region

#Region "GetRateClassInfo Overloads"

    Public Function GetRateClassInfo(ByVal nRateCompanyID As Integer, ByVal sRateClass As String, ByVal eLanguage As Language) As RateClassInfo
        Return (MasterGetRateClassInfo(nRateCompanyID, sRateClass, eLanguage))
    End Function

    Public Function GetRateClassInfo(ByVal nRateCompanyID As Integer, ByVal sRateClass As String) As RateClassInfo
        Return (MasterGetRateClassInfo(nRateCompanyID, sRateClass, Language.English))
    End Function

    ' Private Helper Function for GetRateClassName overloads
    Private Function MasterGetRateClassInfo(ByVal nRateCompanyID As Integer, ByVal sRateClass As String, ByVal eLanguage As Language) As RateClassInfo
        Dim objDC As New dctlRateEngine(m_sRateEngineConnectionString, m_xmlFilesPath, m_dataSourceType, m_cachingType, m_cacheTimeoutInMinutes)
        Dim objRateClassInfo As RateClassInfo

        objRateClassInfo = objDC.GetRateClassInfo(nRateCompanyID, GetBaseRateClass(nRateCompanyID, sRateClass), eLanguage)

        Return (objRateClassInfo)
    End Function

#End Region

#Region "CheckFuelCostRecovery Overloads"

    Public Function CheckFuelCostRecovery(ByVal objDefinitions As DetailedRateDefinitionCollection, ByVal dtStartDate As DateTime, ByVal dtEndDate As DateTime) As Boolean
        Return (DoesFCRChange(objDefinitions, dtStartDate, dtEndDate))
    End Function

    Public Function CheckFuelCostRecovery(ByVal nRateCompanyID As Integer, ByVal sRateClass As String, ByVal dtStartDate As DateTime, ByVal dtEndDate As DateTime) As Boolean
        Dim objDefinitions As DetailedRateDefinitionCollection

        ' Must grab objDefinitions and loop to find FCRGroups in UseCharges
        objDefinitions = GetDetailedRateInformation(nRateCompanyID, sRateClass, dtStartDate, dtEndDate, Language.English)

        Return (DoesFCRChange(objDefinitions, dtStartDate, dtEndDate))
    End Function



#End Region

#Region "GetFuelCostRecoveryHistory Overloads"

    ' Function for grabbing FCR history
    Public Function GetFuelCostRecoveryHistory(ByVal nFCRGroup As Integer, ByVal dtStartDate As DateTime, ByVal dtEndDate As DateTime) As FCRCollection
        Dim objFCRColl As FCRCollection
        Dim objDC As New dctlRateEngine(m_sRateEngineConnectionString, m_xmlFilesPath, m_dataSourceType, m_cachingType, m_cacheTimeoutInMinutes)

        objFCRColl = objDC.GetFuelCostRecoveryList(nFCRGroup, dtStartDate, dtEndDate)

        Return (objFCRColl)
    End Function

#End Region

#Region "CheckUsageCollection Public Overloads"

    Public Function CheckUsageCollection(ByVal objUsageColl As UsageCollection) As UsageCollection

        objUsageColl = VerifyUsages(objUsageColl)

        Return (objUsageColl)
    End Function

#End Region

#Region "GetRateClassFromRateMasterID Public Overloads"

    Public Function GetRateClassFromRateMasterID(ByVal nRateMasterID As Integer) As String
        Dim objDC As New dctlRateEngine(m_sRateEngineConnectionString, m_xmlFilesPath, m_dataSourceType, m_cachingType, m_cacheTimeoutInMinutes)
        Dim sClientRateID As String

        sClientRateID = objDC.GetRateClassFromRateMasterID(nRateMasterID)

        Return (sClientRateID)
    End Function

#End Region

#Region "GetOldestStartDate Public Overloads"

    Public Function GetOldestStartDate(ByVal nRateMasterID As Integer) As DateTime
        Dim objDC As New dctlRateEngine(m_sRateEngineConnectionString, m_xmlFilesPath, m_dataSourceType, m_cachingType, m_cacheTimeoutInMinutes)
        Dim dtStartDate As DateTime

        dtStartDate = objDC.GetOldestStartDate(nRateMasterID)

        Return (dtStartDate)
    End Function

#End Region

#Region "DoesRateClassExist Overloads"

    Public Function DoesRateClassExist(ByVal nRateCompanyID As Integer, ByVal sRateClass As String, ByVal eLanguage As Language) As Boolean
        Return (MasterDoesRateClassExist(nRateCompanyID, sRateClass, eLanguage))
    End Function

    Public Function DoesRateClassExist(ByVal nRateCompanyID As Integer, ByVal sRateClass As String) As Boolean
        Return (MasterDoesRateClassExist(nRateCompanyID, sRateClass, Language.English))
    End Function

    ' Private Helper Function for DoesRateClassExist overloads
    Private Function MasterDoesRateClassExist(ByVal nRateCompanyID As Integer, ByVal sRateClass As String, ByVal eLanguage As Language) As Boolean
        Dim objDC As New dctlRateEngine(m_sRateEngineConnectionString, m_xmlFilesPath, m_dataSourceType, m_cachingType, m_cacheTimeoutInMinutes)
        Return (objDC.DoesRateClassExist(nRateCompanyID, GetBaseRateClass(nRateCompanyID, sRateClass), eLanguage))
    End Function

#End Region

#Region "GetRateChangeInformation Overloads"

    Public Function GetRateChangeInformation(ByVal objDefinitions As DetailedRateDefinitionCollection, ByVal dtOldStartDate As DateTime, ByVal dtOldEndDate As DateTime, ByVal dtNewStartDate As DateTime, ByVal dtNewEndDate As DateTime) As RateChangeInformation
        Dim objRateChangeInfo As New RateChangeInformation
        Dim objDef As DetailedRateDefinition
        Dim eProrate As ProrateType
        Dim eSeasonalProrate As SeasonalProrateType

        If (Not objDefinitions Is Nothing) Then

            If (objDefinitions.Count > 0) Then

                ' ** REGULAR RATE
                If (objDefinitions.Count > 1) Then
                    ' Perhaps a rate change, since there is more than one rate definition
                    ' Most recent is always at index zero (only get this one, see CalculateCost for why we use this one)
                    eProrate = objDefinitions.Item(0).ProrateType

                    Select Case eProrate
                        Case ProrateType.UsePrevious
                            For Each objDef In objDefinitions
                                If (objDef.StartDate > dtOldStartDate And objDef.StartDate <= dtNewStartDate) Then
                                    objRateChangeInfo.RateChange = True
                                End If
                            Next

                        Case ProrateType.UseLatest
                            For Each objDef In objDefinitions
                                If (objDef.StartDate > dtOldEndDate And objDef.StartDate <= dtNewEndDate) Then
                                    objRateChangeInfo.RateChange = True
                                End If
                            Next

                        Case ProrateType.SplitBillDays
                            ' Rather than comparison logic, the fact that the count > 1 means there is a Rate Change
                            objRateChangeInfo.RateChange = True

                    End Select

                End If

                ' ** FCR
                If (Not objRateChangeInfo.RateChange) Then
                    ' Test for FCR realted rate change
                    If (objDefinitions.Item(0).IsFuelAdjustment()) Then
                        objRateChangeInfo.RateChange = CheckFuelCostRecovery(objDefinitions, dtOldStartDate, dtNewEndDate)
                    End If
                End If

                ' ** SEASONAL
                If (objDefinitions.Item(0).IsSeasonal()) Then
                    Dim objSeasonBoundaries As SeasonBoundaryCollection = objDefinitions.Item(0).SeasonBoundaries()
                    Dim objSeasonBoundary, objSeasonBoundary2 As SeasonBoundary

                    ' Most recent is always at index zero, but for seasonal prorate, get from oldest
                    eSeasonalProrate = objDefinitions.Item(objDefinitions.Count - 1).SeasonalProrateType

                    Select Case eSeasonalProrate
                        Case SeasonalProrateType.UsePrevious
                            objSeasonBoundary = objSeasonBoundaries.FindInclusiveBoundary(dtOldStartDate)
                            objSeasonBoundary2 = objSeasonBoundaries.FindInclusiveBoundary(dtNewStartDate)
                            If (objSeasonBoundary.Season <> objSeasonBoundary2.Season) Then
                                objRateChangeInfo.SeasonalRateChange = True
                                objRateChangeInfo.RateChange = True
                            End If

                        Case SeasonalProrateType.UseLatest
                            objSeasonBoundary = objSeasonBoundaries.FindInclusiveBoundary(dtOldEndDate)
                            objSeasonBoundary2 = objSeasonBoundaries.FindInclusiveBoundary(dtNewEndDate)
                            If (objSeasonBoundary.Season <> objSeasonBoundary2.Season) Then
                                objRateChangeInfo.SeasonalRateChange = True
                                objRateChangeInfo.RateChange = True
                            End If

                        Case SeasonalProrateType.SplitBillDays
                            objSeasonBoundary = objSeasonBoundaries.FindInclusiveBoundary(dtOldStartDate)
                            objSeasonBoundary2 = objSeasonBoundaries.FindInclusiveBoundary(dtNewEndDate)
                            If (objSeasonBoundary.Season <> objSeasonBoundary2.Season) Then
                                objRateChangeInfo.SeasonalRateChange = True
                                objRateChangeInfo.RateChange = True
                            End If

                    End Select

                End If

            End If

        End If

        Return (objRateChangeInfo)
    End Function

#End Region



#Region "BinReadings Overloads"


    Public Function BinReadings(ByVal nRateCompanyID As Integer, ByVal objReadings As ReadingCollection, ByVal sRateClass As String, ByVal dtStartDate As DateTime, ByVal dtEndDate As DateTime) As ReadingCollection
        Return (BinReadings(nRateCompanyID, objReadings, sRateClass, dtStartDate, dtEndDate, New BinReadingsExtParam()))
    End Function

    Public Function BinReadings(ByVal nRateCompanyID As Integer, ByVal objReadings As ReadingCollection, ByVal sRateClass As String, ByVal dtStartDate As DateTime, ByVal dtEndDate As DateTime, ByVal extParams As BinReadingsExtParam) As ReadingCollection
        Return (BinReadings(nRateCompanyID, Nothing, objReadings, sRateClass, dtStartDate, dtEndDate, extParams))
    End Function

    Public Function BinReadings(ByVal nRateCompanyID As Integer, ByVal objDefinitions As DetailedRateDefinitionCollection, ByVal objReadings As ReadingCollection, ByVal sRateClass As String, ByVal dtStartDate As DateTime, ByVal dtEndDate As DateTime) As ReadingCollection
        Return (BinReadings(nRateCompanyID, objDefinitions, objReadings, sRateClass, dtStartDate, dtEndDate, New BinReadingsExtParam()))
    End Function

    Public Function BinReadings(ByVal nRateCompanyID As Integer, ByVal objDefinitions As DetailedRateDefinitionCollection, ByVal objReadings As ReadingCollection, ByVal sRateClass As String, ByVal dtStartDate As DateTime, ByVal dtEndDate As DateTime, ByVal extParams As BinReadingsExtParam) As ReadingCollection
        Dim objDetailedDef As DetailedRateDefinition = Nothing
         Dim objNextDetailedDef As DetailedRateDefinition = Nothing
        Dim nBillDays As Integer
        Dim arrProrateFactors() As Double = Nothing
        Dim bUseSeasonProrateOverride As Boolean
        Dim eOverrideSeason As Season
        Dim objTOUBoundaries As TOUBoundaryCollection
        Dim eArrayDayTypeTOU(2, 95) As TimeOfUse
        Dim eArraySeason(4, 366) As Season  'allow for 4 years of seasonal lookup
        Dim objSeasonBoundaries As SeasonBoundaryCollection
        Dim objReading As Reading
        Dim bNextDefExists As Boolean
        Dim bNewDef As Boolean
        Dim bNewSeason As Boolean
        Dim eLastSeason As Season = Season.Undefined
        Dim ii, touIndex As Integer
        Dim dtReadingsStartDate As DateTime
        Dim dtReadingsEndDate As DateTime
        Const StartDayForYear As String = "0101"
        Const EndDayForYear As String = "1231"
        Dim holidaysExist As Boolean = False
        Dim holidays As HolidayCollection = Nothing
        Dim bCriticalPeakBinned As Boolean = False
        Dim tempRccBinnStartDate As String = String.Empty
        Dim tempRccBinnEndDate As String = String.Empty
        Dim tempdtReadingsStartDate As DateTime
        Dim tempdtReadingsEndDate As DateTime
        Dim overrideHolidayDayType As DayType
        Dim defId As Integer = 0
        Dim groupNum As Integer = -1
        Dim isDailyInterval As Boolean

        If (objReadings.Count > 0) Then

            ' Determine the date range needed for seasonal lookups below; this is necessary for all types of callers;
            ' From Load Analysis callers with new meter readings to those callers in Rate Comparison calc needing binning of old proxy data;
            ' Also use this as range rather than the startDate and endDate passed in to this function;  Those are often way too expansive;
            ' Best to use range of readings provided here;
            dtReadingsStartDate = objReadings(0).Datestamp
            dtReadingsEndDate = objReadings(objReadings.Count - 1).Datestamp
            nBillDays = dtReadingsEndDate.Subtract(dtReadingsStartDate).Days + 1
            isDailyInterval = objReadings.IsDailyInterval

            ' some callers will pass this, older callers will not, and thus this needs to be instantiated correctly
            If (objDefinitions Is Nothing) Then
                If (extParams.SpecialDateAdjustment) Then
                    'bug 21973 AO, for RCC use rate definition date for the following year; TODO: CHANGE THIS, THIS SHOULD NEVER HAVE BEEN ADDED HERE!
                    tempdtReadingsStartDate = dtReadingsStartDate
                    tempdtReadingsEndDate = dtReadingsEndDate

                    tempRccBinnStartDate = dtReadingsStartDate.Month.ToString & "/" & dtReadingsStartDate.Day & "/" & dtStartDate.Year.ToString
                    tempRccBinnEndDate = dtReadingsEndDate.Month.ToString & "/" & dtReadingsEndDate.Day & "/" & dtEndDate.Year.ToString

                    dtReadingsStartDate = CType(tempRccBinnStartDate, DateTime)
                    dtReadingsEndDate = CType(tempRccBinnEndDate, DateTime)
                    objDefinitions = GetDetailedRateInformation(nRateCompanyID, sRateClass, dtReadingsStartDate, dtReadingsEndDate)
                    'set back to readings start and enddate
                    'after getting the following year rate definination
                    dtReadingsStartDate = tempdtReadingsStartDate
                    dtReadingsEndDate = tempdtReadingsEndDate
                Else
                    objDefinitions = GetDetailedRateInformation(nRateCompanyID, sRateClass, dtReadingsStartDate, dtReadingsEndDate)
                End If
            End If


            If (objDefinitions.Count > 0) Then
                ' This will trim the definitions based on pro-rate if necessary; the arrProrateFactors returned are not used
                objDefinitions = ProcessProrate(objDefinitions, nBillDays, dtReadingsStartDate, dtReadingsEndDate, arrProrateFactors)
                bUseSeasonProrateOverride = ProcessSeasonalProrate(objDefinitions, dtReadingsStartDate, dtReadingsEndDate, eOverrideSeason)

                ' The oldest definition is the last definition in the collection
                ii = objDefinitions.Count - 1

                objDetailedDef = objDefinitions(ii)
                objSeasonBoundaries = objDetailedDef.SeasonBoundaries
                If (objSeasonBoundaries Is Nothing) Then objSeasonBoundaries.Add(New SeasonBoundary(Season.Undefined, StartDayForYear, EndDayForYear))
                eArraySeason = GetSeasonBoundaryArrayForYear(objSeasonBoundaries, dtReadingsStartDate.Year, dtReadingsEndDate.Year)
                objTOUBoundaries = objDetailedDef.TOUBoundaries
                bNewDef = True
                defId = objDetailedDef.DefinitionID
                groupNum = groupNum + 1

                If (ii - 1 >= 0) Then
                    objNextDetailedDef = objDefinitions(ii - 1)
                    bNextDefExists = True
                Else
                    bNextDefExists = False
                End If

                ' Assign Holidays of rate class uses holidays; only have to do this once since assigned to master rate class
                If (objDetailedDef.HolidayGroupExists()) Then
                    holidays = GetHolidayList(objDetailedDef.HolidayGroupID, dtReadingsStartDate, dtReadingsEndDate)
                    If (Not holidays Is Nothing) Then
                        If (holidays.Count > 0) Then holidaysExist = True Else holidaysExist = False
                    Else
                        holidaysExist = False
                    End If
                    If objDetailedDef.TOUBoundaries.ContainsDayType(DayType.Holiday) Then
                        overrideHolidayDayType = DayType.Holiday
                    Else
                        overrideHolidayDayType = DayType.WeekendHoliday
                    End If
                End If


                If (holidaysExist) Then
                    AssignHolidaysToReadings(holidays, objReadings, overrideHolidayDayType)
                End If

                'Set the independent tier accumulation here from rate definition; override anything that initialized this property
                extParams.UseIndependentTierAccumulations = objDetailedDef.IsIndependentTierAccumulationsByTOU

                'Seasonal accumulation for daily tiers override; most cases require tou with other combinations; also allowed tier w/ hybrid monthly bounds
                If (Not extParams.UseIndependentTierAccumulations) Then
                    If ((objDetailedDef.IsTiered AndAlso objDetailedDef.IsTimeOfUse AndAlso objDetailedDef.IsSeasonal AndAlso objDetailedDef.SeasonalProrateType = SeasonalProrateType.SplitBillDays AndAlso _
                       (objDetailedDef.TierStructureType = TierStructureType.DailyBounds Or objDetailedDef.TierStructureType = TierStructureType.DailyBoundsPerDwellingUnit Or _
                        objDetailedDef.TierStructureType = TierStructureType.PercentBaseTier1 Or objDetailedDef.TierStructureType = TierStructureType.PercentBaseTier1WithDwellingUnits)) _
                        Or _
                       (objDetailedDef.IsTiered AndAlso objDetailedDef.TierStructureType = TierStructureType.MonthlyBoundsHybrid1)) Then

                        extParams.UseIndependentTierAccumulationsBySeason = True

                    End If
                End If

                ' Oldest readings are the first readings in the For/Each; this works out well
                For Each objReading In objReadings

                    If (bNextDefExists) Then

                        If (objNextDetailedDef.StartDate <= objReading.Datestamp) Then

                            CheckDefinitionAndAdjustIndex(objDefinitions, ii, bNextDefExists, objNextDetailedDef, objReading.Datestamp)

                            objDetailedDef = objDefinitions(ii)
                            objSeasonBoundaries = objDetailedDef.SeasonBoundaries
                            If (objSeasonBoundaries Is Nothing) Then objSeasonBoundaries.Add(New SeasonBoundary(Season.Undefined, StartDayForYear, EndDayForYear))
                            eArraySeason = GetSeasonBoundaryArrayForYear(objSeasonBoundaries, dtReadingsStartDate.Year, dtReadingsEndDate.Year)
                            objTOUBoundaries = objDetailedDef.TOUBoundaries
                            bNewDef = True
                            defId = objDetailedDef.DefinitionID
                            groupNum = groupNum + 1

                        End If

                    End If

                    ' Get the Season, since this effects the TOU Boundary; it may be Undefined
                    ' Critical that the datestamp be reformatted so that the TIME portion is not used in the Find Inclusive Boundary;
                    If (Not bUseSeasonProrateOverride) Then

                        objReading.Season = eArraySeason(objReading.Datestamp.Year Mod dtReadingsStartDate.Year, objReading.Datestamp.DayOfYear)

                        If (objReading.Season <> eLastSeason) Then
                            bNewSeason = True
                            eLastSeason = objReading.Season
                        End If

                    Else
                        objReading.Season = eOverrideSeason
                    End If

                    If (bNewDef Or bNewSeason) Then
                        bCriticalPeakBinned = False

                        If (Not objTOUBoundaries Is Nothing) Then
                            eArrayDayTypeTOU = GetTOUDayOfWeekBoundaryArray(objDetailedDef, objReading.Season)
                        End If

                        bNewDef = False
                        bNewSeason = False
                    End If


                    ' Get the TOU; but ONLY if indeed TOU; if not TOU, it remains Undefined
                    ' No Need to Assign tou's for daily reading using below method
                    If (Not extParams.SkipTOUBinning AndAlso Not isDailyInterval) Then
                    If (Not objTOUBoundaries Is Nothing) Then

                        touIndex = (objReading.Datestamp.Hour * 4) + CInt(Math.Floor(objReading.Datestamp.Minute / 15))

                        If (holidaysExist) Then
                            If (objReading.Holiday) Then
                                If objDetailedDef.TOUBoundaries.ContainsDayType(DayType.Holiday) Then
                                    objReading.TimeOfUse = eArrayDayTypeTOU(7, touIndex)
                                Else
                                    ' use Sunday as the overriding first index into this array
                                    objReading.TimeOfUse = eArrayDayTypeTOU(System.DayOfWeek.Sunday, touIndex)

                                End If
                            Else
                                objReading.TimeOfUse = eArrayDayTypeTOU(objReading.Datestamp.DayOfWeek, touIndex)
                            End If
                        Else
                            'This is usually the code path;
                            objReading.TimeOfUse = eArrayDayTypeTOU(objReading.Datestamp.DayOfWeek, touIndex)
                        End If

                        ' This is cleared to Undefined here, since it shall NOT remain TotalServiceUse once TOU is applied
                        If (objReading.TimeOfUse <> TimeOfUse.Undefined) Then
                            objReading.BaseOrTier = BaseOrTier.Undefined
                        End If

                    End If
                    End If

                    ' Use these to keep track of details to be used by callers
                    objReading.BinnedId = defId
                    objReading.BinnedGroupNumber = groupNum
                Next


                ' Do critical peak binning here now, with last valid definition from loop above, so we only do it once; rather than multiple times above
                ' Do the regular binning except the critical peak if idre is false.  Also do extra work for Capacity Reserve when applicable.
                If (Not extParams.SkipTOUBinning) Then
                If (CheckIfDemandResponseEligible(nRateCompanyID, sRateClass) = True) Then
                    If (objDetailedDef.IsRTPRate) Then

                        If (objDetailedDef.RTPStream = RTPStream.CriticalPeakOnly Or objDetailedDef.RTPStream = RTPStream.PeakTimeRebateOnly) Then

                            If (objDetailedDef.IsCapacityReserve) Then
                                Dim crkw As Double? = Nothing
                                crkw = GetCapacityReserveDemandValue(sRateClass)
                                If (Not crkw Is Nothing) Then
                                    extParams.CapResDemandSpecified = True
                                    extParams.CapResDemand = crkw.Value
                                End If

                            End If

                            BinningForCriticalPeak(objReadings, objDetailedDef, extParams)

                        End If

                    End If
                End If
                End If

                ' Modifications to readings must be made if specified via reading adjustment factors.
                ' This is for advanced callers that would like to shed and shift usage.
                If (Not extParams.ReadingAdjustmentFactors Is Nothing) Then
                    Dim raf As ReadingAdjustFactors = extParams.ReadingAdjustmentFactors

                    If (objDetailedDef.IsRTPRate AndAlso (objDetailedDef.RTPStream = RTPStream.CriticalPeakOnly Or objDetailedDef.RTPStream = RTPStream.PeakTimeRebateOnly)) Then
                        ApplyReadingAdjustmentFactorToReadings(objReadings, ReadingAdjustmentFactorType.CriticalResponse, raf.CriticalResponse, raf.PercentCriticalReductionAsShifted, True, True)
                    End If
                    ApplyReadingAdjustmentFactorToReadings(objReadings, ReadingAdjustmentFactorType.TypicalResponse, raf.TypicalResponse, raf.PercentReductionAsShifted)
                    ApplyReadingAdjustmentFactorToReadings(objReadings, ReadingAdjustmentFactorType.DemandControl, raf.DemandControl, raf.PercentReductionAsShifted)

                    Log(objReadings) 'for advanced analysis
                End If


                ' Enhanced binning; start enhanced processing here.
                If (extParams.EnhancedBinning AndAlso (Not extParams.SkipTOUBinning)) Then
                    objReadings = BinReadingsEnhanced(objReadings, objDefinitions, dtStartDate, sRateClass, extParams)
                End If


            End If

        End If

        Return (objReadings)
    End Function


    ''' <summary>
    ''' Binning for tiers with price information per reading provided (unless indicated to exclude.)
    ''' Three types of accumulations supported now for tiered seasonal tou.
    ''' </summary>
    ''' <param name="readings"></param>
    ''' <param name="detailedDefinitions"></param>
    ''' <param name="rateClassArg"></param>
    ''' <param name="extParams"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function BinReadingsEnhanced(ByVal readings As ReadingCollection, ByVal detailedDefinitions As DetailedRateDefinitionCollection, ByVal startDate As DateTime, ByVal rateClassArg As String, ByVal extParams As BinReadingsExtParam) As ReadingCollection
        Dim detailedDef As DetailedRateDefinition
        Dim nextDetailedDef As DetailedRateDefinition = Nothing
        Dim ii As Integer
        Dim objReading As Reading
        Dim bNextDefExists As Boolean
        Dim bNewDef As Boolean
        Dim timeOfUseList As TOUList
        Dim tou As TimeOfUse
        Dim tierBoundaryColl(5, 4) As TierBoundaryCollection    'TOU,season
        ' Dim tierBoundaries As TierBoundaryCollection
        Dim dictEntrySeason As DictionaryEntry
        Dim theSeason As SeasonFactor
        Dim seasonList As SeasonFactorCollection= Nothing
        Dim accumulatedUsageTOUSeason(5, 4) As Double           'TimeOfUse, Season

        Dim combinedUseCharges As UseChargeCollection
        Dim r As Reading
        Dim FCRColl As FCRCollection = Nothing
        Dim fcrCharge As Double
        Const kFCR As String = "fcr"
        Const kRTP As String = "rtp"
        Dim rtprices As PriceCollection = Nothing
        Dim rtpCharge As Double
        Dim rtpValid As Boolean
        Dim readingInterval As ReadingInterval
        Dim priceIndex As Integer
        Dim readingIndex As Integer
        Dim j As Integer = 0
        Dim fcrCount As Integer = 0
        Dim ssCharge As Double

        Dim billDays As Integer
        Dim factor As Double = 1.0
        Dim overallSeasonFactors As SeasonFactorCollection = Nothing

        readingInterval = readings.DetermineIntervalOfReadings()

        ' Same loop logic from calling function above
        ii = detailedDefinitions.Count - 1
        detailedDef = detailedDefinitions(ii)
        bNewDef = False

        If (ii - 1 >= 0) Then
            nextDetailedDef = detailedDefinitions(ii - 1)
            bNextDefExists = True
        Else
            bNextDefExists = False
        End If

        'billDays to use if/when finalizing boundaries; UseIndependentTierAccumulationsBySeason trumps this below for a small subset of rate classes
        If (extParams.UseBillDaysOverrideForFinalizingTierBoundaries) Then
            billDays = extParams.BillDaysOverride
        Else
            billDays = extParams.TierOverrideDaysInMonth
        End If

        If (detailedDef.IsTiered) Then
            ' Create season list for the purpose of having list of seasons present in the readings collection;  do this once; also get number of days per season
            seasonList = DetermineSeasonFactors(readings)

            ' Create true season factors given the startDate to projectedEndDate
            Dim seasonOverride As Season
            Dim projectedEndDate As DateTime = startDate.AddDays(billDays - 1)
            Dim useSeasonOverride As Boolean = ProcessSeasonalProrate(detailedDefinitions, startDate, projectedEndDate, seasonOverride)
            If (detailedDef.IsSeasonal) Then
                overallSeasonFactors = DetermineSeasonFactors(detailedDef, startDate, projectedEndDate, useSeasonOverride, seasonOverride)
            Else
                overallSeasonFactors = New SeasonFactorCollection()
            End If
            overallSeasonFactors.Add(Season.Undefined, New SeasonFactor(Season.Undefined, 1.0, billDays)) 'always add an undefined season


            ' get the time-of-use list for use below;
            timeOfUseList = CreateTOUListFromTierBoundaries(detailedDef.TierBoundaries)

            ' calculate the tierBoundaries;
            For Each dictEntrySeason In seasonList

                theSeason = CType(dictEntrySeason.Value, SeasonFactor)

                If (extParams.UseIndependentTierAccumulationsBySeason) Then
                    billDays = overallSeasonFactors(theSeason.Season).Days
                End If

                ' special handling for hybrid tiers; override the days and provide the seasonFactor to FinalizeTierBoundaries
                factor = 1.0
                If (extParams.UseIndependentTierAccumulationsBySeason AndAlso detailedDef.TierStructureType = TierStructureType.MonthlyBoundsHybrid1) Then
                    factor = CDbl(overallSeasonFactors(theSeason.Season).Days) / extParams.BillDaysOverride
                    billDays = extParams.BillDaysOverride
                End If

                For Each tou In timeOfUseList
                    tierBoundaryColl(tou, theSeason.Season) = FinalizeTierBoundaries(detailedDef.RateCompanyID, detailedDef.DefinitionID, detailedDef.MasterID, _
                                                                 detailedDef.TierBoundaries, detailedDef.TierStructureType, _
                                                                 rateClassArg, theSeason.Season, tou, extParams.DwellingCount, billDays, factor)
                Next

            Next

        End If

        'Get combined list of use charges; FCRs retrieved for use if applicable
        combinedUseCharges = detailedDef.UseCharges.GetCombinedUseCharges()
        If (detailedDef.IsFuelAdjustment) Then
            For Each charge As UseCharge In combinedUseCharges
                If (charge.FuelCostRecovery) Then
                    FCRColl = GetFuelCostRecoveryHistory(charge.FCRGroup, detailedDef.StartDate, readings(readings.Count - 1).Datestamp)
                End If
            Next
        End If
        If (detailedDef.IsRTPRate) Then
            For Each charge As UseCharge In combinedUseCharges
                If (charge.RealTimePricing) Then
                    rtprices = GetPriceData(detailedDef.RateCompanyID, charge.RTPGroup, rateClassArg, readings(0).Datestamp, readings(readings.Count - 1).Datestamp, True)
                    If (Not rtprices Is Nothing AndAlso rtprices.Count > 0) Then rtpValid = True Else rtpValid = False
                    Exit For
                End If
            Next
        End If

        'Scale readings if non-zero and applicable conversion factor available;
        If (extParams.ConversionFactor <> 0 AndAlso extParams.ConversionFactor <> 1) Then
            readings.ApplyScaleFactor(extParams.ConversionFactor)
        End If

        ' set offset of total accumulation; usually offset is zero, but CostToDate callers will have offset sometimes
        accumulatedUsageTOUSeason(Enums.TimeOfUse.Undefined, Enums.Season.Undefined) += extParams.TierAccumulationOffset

        'Iterate Readings for Reading Details and Tier processing (if tiers are in-play)
        readingIndex = 0
        For Each objReading In readings

            If (bNextDefExists) Then

                If (nextDetailedDef.StartDate <= objReading.Datestamp) Then

                    CheckDefinitionAndAdjustIndex(detailedDefinitions, ii, bNextDefExists, nextDetailedDef, objReading.Datestamp)

                    detailedDef = detailedDefinitions(ii)
                    bNewDef = True

                End If

            End If

            If (bNewDef) Then

                If (detailedDef.IsTiered) Then
                    ' new definition is in play; again get the time-of-use list for use below; usually this would not change;
                    timeOfUseList = CreateTOUListFromTierBoundaries(detailedDef.TierBoundaries)

                    ' recalculate the tierBoundaries once again; in case they changed when the rateDef changed; usually this would not change
                    For Each dictEntrySeason In seasonList

                        theSeason = CType(dictEntrySeason.Value, SeasonFactor)

                        If (extParams.UseIndependentTierAccumulationsBySeason) Then
                            billDays = overallSeasonFactors(theSeason.Season).Days
                        End If

                        ' special handling for hybrid tiers; override the days and provide the seasonFactor to FinalizeTierBoundaries
                        factor = 1.0
                        If (extParams.UseIndependentTierAccumulationsBySeason AndAlso detailedDef.TierStructureType = TierStructureType.MonthlyBoundsHybrid1) Then
                            factor = CDbl(overallSeasonFactors(theSeason.Season).Days) / extParams.BillDaysOverride
                            billDays = extParams.BillDaysOverride
                        End If

                        For Each tou In timeOfUseList
                            tierBoundaryColl(tou, theSeason.Season) = FinalizeTierBoundaries(detailedDef.RateCompanyID, detailedDef.DefinitionID, detailedDef.MasterID, _
                                                                         detailedDef.TierBoundaries, detailedDef.TierStructureType, _
                                                                         rateClassArg, theSeason.Season, tou, extParams.DwellingCount, billDays, factor)
                        Next

                    Next
                End If

                ' again, get the combined charges since they are likely a bit different now; FCRs retrieved for use if applicable
                combinedUseCharges = detailedDef.UseCharges.GetCombinedUseCharges()
                If (detailedDef.IsFuelAdjustment) Then
                    For Each charge As UseCharge In combinedUseCharges
                        If (charge.FuelCostRecovery) Then
                            FCRColl = GetFuelCostRecoveryHistory(charge.FCRGroup, detailedDef.StartDate, readings(readings.Count - 1).Datestamp)
                        End If
                    Next
                End If
                ssCharge = 0.0
                j = 0
                fcrCount = 0
                If (detailedDef.IsRTPRate) Then
                    For Each charge As UseCharge In combinedUseCharges
                        If (charge.RealTimePricing) Then
                            rtprices = GetPriceData(detailedDef.RateCompanyID, charge.RTPGroup, rateClassArg, readings(0).Datestamp, readings(readings.Count - 1).Datestamp, True)
                            If (Not rtprices Is Nothing AndAlso rtprices.Count > 0) Then rtpValid = True Else rtpValid = False
                            Exit For
                        End If
                    Next
                End If

                bNewDef = False
            End If

            r = UpdateTierDetails(detailedDef, objReading, accumulatedUsageTOUSeason, tierBoundaryColl, extParams)
            ' if it's the start of bill cycle, clear the accumulated usage
            If (objReading.IsStartOfPeriod) Then

                accumulatedUsageTOUSeason(Enums.TimeOfUse.Undefined, Enums.Season.Undefined) = 0

                If (extParams.UseIndependentTierAccumulations) Then
                    If (objReading.TimeOfUse <> TimeOfUse.Undefined) Then
                        accumulatedUsageTOUSeason(objReading.TimeOfUse, Enums.Season.Undefined) = 0
                        If (objReading.Season <> Season.Undefined) Then
                            accumulatedUsageTOUSeason(objReading.TimeOfUse, objReading.Season) = 0
                            accumulatedUsageTOUSeason(Enums.TimeOfUse.Undefined, objReading.Season) = 0
                        End If
                    Else
                        If (objReading.Season <> Season.Undefined) Then
                            accumulatedUsageTOUSeason(Enums.TimeOfUse.Undefined, objReading.Season) = 0
                        End If
                    End If

                End If
            End If

            ' accumulate overall total
            accumulatedUsageTOUSeason(Enums.TimeOfUse.Undefined, Enums.Season.Undefined) += objReading.Quantity

            ' accumulate specific running totals only when enabled to do so
            If (extParams.UseIndependentTierAccumulations) Then

                ' most advanced
                If (objReading.TimeOfUse <> TimeOfUse.Undefined) Then
                    accumulatedUsageTOUSeason(objReading.TimeOfUse, Enums.Season.Undefined) += objReading.Quantity
                    If (objReading.Season <> Season.Undefined) Then
                        accumulatedUsageTOUSeason(objReading.TimeOfUse, objReading.Season) += objReading.Quantity
                        accumulatedUsageTOUSeason(Enums.TimeOfUse.Undefined, objReading.Season) += objReading.Quantity
                    End If
                Else
                    If (objReading.Season <> Season.Undefined) Then
                        accumulatedUsageTOUSeason(Enums.TimeOfUse.Undefined, objReading.Season) += objReading.Quantity
                    End If
                End If

            ElseIf (extParams.UseIndependentTierAccumulationsBySeason) Then

                ' moderate
                If (objReading.Season <> Season.Undefined) Then
                    accumulatedUsageTOUSeason(Enums.TimeOfUse.Undefined, objReading.Season) += objReading.Quantity
                End If

            End If


            'objReading = UpdateTierDetails(detailedDef, objReading, accumulatedUsageTOUSeason, tierBoundaryColl, extParams)

            ' Always add Detail to the readings; price details are attached to this below;
            If objReading.DetailCollection Is Nothing Then
                objReading.DetailCollection = New ReadingDetailCollection()
                If (objReading.Quantity > 0) Then
                    objReading.DetailCollection.Add(New ReadingDetail(BaseOrTier.Undefined, TierType.Undefined, objReading.Quantity))
                End If

            End If


            ' Assign tier(s) to readings here if tiered;
            'If (detailedDef.IsTiered) Then

            '    'basic tiers if exist
            '    tierBoundaries = tierBoundaryColl(Enums.TimeOfUse.Undefined, Enums.Season.Undefined)
            '    If (Not tierBoundaries Is Nothing) Then
            '        If (tierBoundaries.Count > 0) Then
            '            tier = tierBoundaries.GetInclusiveTier(GetAccumulatedTotal(accumulatedUsageTOUSeason, TimeOfUse.Undefined, Enums.Season.Undefined, extParams.UseIndependentTierAccumulations, extParams.UseIndependentTierAccumulationsBySeason))
            '            If (tier <> BaseOrTier.Undefined) Then
            '                objReading.Detail.TierRegular = tier
            '            End If
            '        End If
            '    End If

            '    'seasonal tiers only if exist
            '    If (objReading.Season <> Season.Undefined) Then
            '        tierBoundaries = tierBoundaryColl(Enums.TimeOfUse.Undefined, objReading.Season)
            '        If (Not tierBoundaries Is Nothing) Then
            '            If (tierBoundaries.Count > 0) Then
            '                tier = tierBoundaries.GetInclusiveTier(GetAccumulatedTotal(accumulatedUsageTOUSeason, TimeOfUse.Undefined, objReading.Season, extParams.UseIndependentTierAccumulations, extParams.UseIndependentTierAccumulationsBySeason))
            '                If (tier <> BaseOrTier.Undefined) Then
            '                    objReading.Detail.TierSeasonal = tier
            '                End If
            '            End If
            '        End If
            '    End If

            '    'tou tiers only if exist
            '    If (objReading.TimeOfUse <> TimeOfUse.Undefined) Then

            '        'Normal or PTR
            '        If (Not (objReading.TimeOfUseModified And detailedDef.RTPStream = RTPStream.PeakTimeRebateOnly)) Then activeTOU = objReading.TimeOfUse Else activeTOU = objReading.TimeOfUseOriginal

            '        tierBoundaries = tierBoundaryColl(activeTOU, Enums.Season.Undefined)
            '        If (Not tierBoundaries Is Nothing) Then
            '            If (tierBoundaries.Count > 0) Then
            '                tier = tierBoundaries.GetInclusiveTier(GetAccumulatedTotal(accumulatedUsageTOUSeason, activeTOU, Enums.Season.Undefined, extParams.UseIndependentTierAccumulations, extParams.UseIndependentTierAccumulationsBySeason))
            '                If (tier <> BaseOrTier.Undefined) Then
            '                    objReading.Detail.TierTOU = tier
            '                End If
            '            End If
            '        End If

            '        'CPP IsAdder
            '        If (objReading.IsAdder AndAlso objReading.TimeOfUseModified AndAlso detailedDef.RTPStream = RTPStream.CriticalPeakOnly) Then

            '            tierBoundaries = tierBoundaryColl(objReading.TimeOfUseOriginal, Enums.Season.Undefined)
            '            If (Not tierBoundaries Is Nothing) Then
            '                If (tierBoundaries.Count > 0) Then
            '                    tier = tierBoundaries.GetInclusiveTier(GetAccumulatedTotal(accumulatedUsageTOUSeason, objReading.TimeOfUseOriginal, Enums.Season.Undefined, extParams.UseIndependentTierAccumulations, extParams.UseIndependentTierAccumulationsBySeason))
            '                    If (tier <> BaseOrTier.Undefined) Then
            '                        objReading.Detail.AdderTierTOU = tier
            '                    End If
            '                End If
            '            End If

            '        End If

            '        'CPP Capacity Reserve and IsAdder.  Note that TOUModified = false in reading that is capacity reserve.  Only the cap res quantity needs to be added as a detail if it is also a adder!
            '        If (objReading.IsCapacityReserve AndAlso objReading.IsAdder AndAlso detailedDef.RTPStream = RTPStream.CriticalPeakOnly) Then

            '            tierBoundaries = tierBoundaryColl(objReading.TimeOfUse, Enums.Season.Undefined)
            '            If (Not tierBoundaries Is Nothing) Then
            '                If (tierBoundaries.Count > 0) Then
            '                    tier = tierBoundaries.GetInclusiveTier(GetAccumulatedTotal(accumulatedUsageTOUSeason, objReading.TimeOfUse, Enums.Season.Undefined, extParams.UseIndependentTierAccumulations, extParams.UseIndependentTierAccumulationsBySeason))
            '                    If (tier <> BaseOrTier.Undefined) Then
            '                        objReading.Detail.AdderTierTOU = tier
            '                    End If
            '                End If
            '            End If

            '        End If

            '        'CPP Capacity Reserve - no adder - nothing special in this case.


            '    End If

            '    'seasonal tou tiers if exist; be careful not to count and set to wrong Detail if already handled above
            '    If (objReading.TimeOfUse <> TimeOfUse.Undefined And objReading.Season <> Season.Undefined) Then

            '        'Normal or PTR
            '        If (Not (objReading.TimeOfUseModified And detailedDef.RTPStream = RTPStream.PeakTimeRebateOnly)) Then activeTOU = objReading.TimeOfUse Else activeTOU = objReading.TimeOfUseOriginal

            '        tierBoundaries = tierBoundaryColl(activeTOU, objReading.Season)
            '        If (Not tierBoundaries Is Nothing) Then
            '            If (tierBoundaries.Count > 0) Then
            '                tier = tierBoundaries.GetInclusiveTier(GetAccumulatedTotal(accumulatedUsageTOUSeason, activeTOU, objReading.Season, extParams.UseIndependentTierAccumulations, extParams.UseIndependentTierAccumulationsBySeason))
            '                If (tier <> BaseOrTier.Undefined) Then
            '                    objReading.Detail.TierTOUSeasonal = tier
            '                End If
            '            End If
            '        End If

            '        'CPP IsAdder
            '        If (objReading.IsAdder AndAlso objReading.TimeOfUseModified AndAlso detailedDef.RTPStream = RTPStream.CriticalPeakOnly) Then

            '            tierBoundaries = tierBoundaryColl(objReading.TimeOfUseOriginal, objReading.Season)
            '            If (Not tierBoundaries Is Nothing) Then
            '                If (tierBoundaries.Count > 0) Then
            '                    tier = tierBoundaries.GetInclusiveTier(GetAccumulatedTotal(accumulatedUsageTOUSeason, objReading.TimeOfUseOriginal, objReading.Season, extParams.UseIndependentTierAccumulations, extParams.UseIndependentTierAccumulationsBySeason))
            '                    If (tier <> BaseOrTier.Undefined) Then
            '                        objReading.Detail.AdderTierTOUSeasonal = tier
            '                    End If
            '                End If
            '            End If

            '        End If

            '        'CPP Capacity Reserve and IsAdder.  Note that TOUModified = false in reading that is capacity reserve.  Only the cap res quantity needs to be added as a detail if it is also a adder!
            '        If (objReading.IsCapacityReserve AndAlso objReading.IsAdder AndAlso detailedDef.RTPStream = RTPStream.CriticalPeakOnly) Then

            '            tierBoundaries = tierBoundaryColl(objReading.TimeOfUse, objReading.Season)
            '            If (Not tierBoundaries Is Nothing) Then
            '                If (tierBoundaries.Count > 0) Then
            '                    tier = tierBoundaries.GetInclusiveTier(GetAccumulatedTotal(accumulatedUsageTOUSeason, objReading.TimeOfUse, objReading.Season, extParams.UseIndependentTierAccumulations, extParams.UseIndependentTierAccumulationsBySeason))
            '                    If (tier <> BaseOrTier.Undefined) Then
            '                        objReading.Detail.AdderTierTOUSeasonal = tier
            '                    End If
            '                End If
            '            End If

            '        End If

            '        'CPP Capacity Reserve - no adder - nothing special in this case.


            '    End If

            'End If

            ' Add price details to the reading; traverse combinedUseCharges

            For Each charge As UseCharge In combinedUseCharges

                For Each readingdetail As ReadingDetail In r.DetailCollection
                ' TotalServiceUse only, seasonal and non-seasonal; TOTALSERVICEUSE MAY or MAY NOT be the baseOrTier value
                If (Not charge.FuelCostRecovery And Not charge.RealTimePricing) Then
                    If (charge.BaseOrTier = BaseOrTier.TotalServiceUse And charge.Season = r.Season And charge.Season <> Season.Undefined) Then
                            readingdetail.PriceDetails.Add(New ReadingPriceDetail(charge.BaseOrTier, charge.TimeOfUse, charge.Season, charge.ChargeValue))
                    End If
                    If (charge.BaseOrTier = BaseOrTier.TotalServiceUse And charge.Season = Season.Undefined) Then
                            readingdetail.PriceDetails.Add(New ReadingPriceDetail(charge.BaseOrTier, charge.TimeOfUse, charge.Season, charge.ChargeValue))
                    End If

                ElseIf (charge.FuelCostRecovery) Then
                    fcrCharge = FCRColl.FindInclusiveFCRChargeValue(r.Datestamp)
                        readingdetail.PriceDetails.Add(New ReadingPriceDetail(charge.BaseOrTier, charge.TimeOfUse, charge.Season, kFCR, fcrCharge))

                ElseIf (charge.RealTimePricing) Then
                    If (rtpValid) Then
                        Select Case readingInterval
                            Case Enums.ReadingInterval.Hourly
                                priceIndex = readingIndex
                            Case Enums.ReadingInterval.HalfHourly
                                priceIndex = CInt(Math.Floor(readingIndex / 2))
                            Case Enums.ReadingInterval.FifteenMinute
                                priceIndex = CInt(Math.Floor(readingIndex / 4))
                            Case Enums.ReadingInterval.FiveMinute 'CR 16664 May 2011 LC - 5-mins
                                priceIndex = CInt(Math.Floor(readingIndex / 12))
                        End Select
                        rtpCharge = rtprices(priceIndex).Price
                        If (rtpCharge <> 0) Then
                                readingdetail.PriceDetails.Add(New ReadingPriceDetail(charge.BaseOrTier, charge.TimeOfUse, charge.Season, kRTP, rtpCharge))
                        End If
                    End If
                End If

                'TOU only, seasonal and non-seasonal
                If (r.TimeOfUse <> TimeOfUse.Undefined) Then
                    If (charge.BaseOrTier = BaseOrTier.Undefined And charge.TimeOfUse = r.TimeOfUse And charge.Season = r.Season) Then
                            readingdetail.PriceDetails.Add(New ReadingPriceDetail(charge.BaseOrTier, charge.TimeOfUse, charge.Season, charge.ChargeValue))
                    End If
                End If

                'Tiers, basic non-seasonal
                    If (readingdetail.TierType = TierType.RegularTier) Then
                        If (charge.BaseOrTier = readingdetail.Tier And charge.TimeOfUse = TimeOfUse.Undefined And charge.Season = Season.Undefined) Then
                            readingdetail.PriceDetails.Add(New ReadingPriceDetail(charge.BaseOrTier, charge.TimeOfUse, charge.Season, charge.ChargeValue))
                    End If
                End If

                'Tiers, seasonal only
                    If (readingdetail.TierType = TierType.SeasonalTier) Then
                        If (charge.BaseOrTier = readingdetail.Tier And charge.Season = r.Season And charge.TimeOfUse = TimeOfUse.Undefined) Then
                            readingdetail.PriceDetails.Add(New ReadingPriceDetail(charge.BaseOrTier, charge.TimeOfUse, charge.Season, charge.ChargeValue))
                    End If
                End If

                'Tiers, TOU only
                    If (readingdetail.TierType = TierType.TOUTier) Then
                        If (charge.BaseOrTier = readingdetail.Tier And charge.TimeOfUse = r.TimeOfUse And charge.Season = Season.Undefined) Then
                            readingdetail.PriceDetails.Add(New ReadingPriceDetail(charge.BaseOrTier, charge.TimeOfUse, charge.Season, charge.ChargeValue))
                    End If
                End If

                'Tiers, seasonal TOU
                    If (readingdetail.TierType = TierType.SeasonalTOUTier) Then
                        If (charge.BaseOrTier = readingdetail.Tier And charge.TimeOfUse = r.TimeOfUse And charge.Season = r.Season) Then
                            readingdetail.PriceDetails.Add(New ReadingPriceDetail(charge.BaseOrTier, charge.TimeOfUse, charge.Season, charge.ChargeValue))
                    End If
                End If


                ' Add additional details for modified time-of-use special cases; PTR
                If (r.TimeOfUseModified And detailedDef.RTPStream = RTPStream.PeakTimeRebateOnly) Then

                    'TOU only, seasonal and non-seasonal; primary straight from reading property
                    If (r.TimeOfUseOriginal <> TimeOfUse.Undefined) Then
                        If (charge.BaseOrTier = BaseOrTier.Undefined And charge.TimeOfUse = r.TimeOfUseOriginal And charge.Season = r.Season) Then
                                readingdetail.PriceDetails.Add(New ReadingPriceDetail(charge.BaseOrTier, charge.TimeOfUse, charge.Season, charge.ChargeValue))
                        End If
                    End If

                    'Tiers, TOU only
                        If (readingdetail.TierType = TierType.TOUTier) Then
                            If (charge.BaseOrTier = readingdetail.Tier And charge.TimeOfUse = r.TimeOfUseOriginal And charge.Season = Season.Undefined) Then
                                readingdetail.PriceDetails.Add(New ReadingPriceDetail(charge.BaseOrTier, charge.TimeOfUse, charge.Season, charge.ChargeValue))
                        End If
                    End If

                    'Tiers, seasonal TOU
                        If (readingdetail.TierType = TierType.SeasonalTOUTier) Then
                            If (charge.BaseOrTier = readingdetail.Tier And charge.TimeOfUse = r.TimeOfUseOriginal And charge.Season = r.Season) Then
                                readingdetail.PriceDetails.Add(New ReadingPriceDetail(charge.BaseOrTier, charge.TimeOfUse, charge.Season, charge.ChargeValue))
                        End If
                    End If

                End If

                ' special case CPP IsAdder
                If (r.IsAdder AndAlso r.TimeOfUseModified AndAlso detailedDef.RTPStream = RTPStream.CriticalPeakOnly) Then

                    'TOU only, seasonal and non-seasonal; primary straight from reading property
                    If (r.TimeOfUseOriginal <> TimeOfUse.Undefined) Then
                        If (charge.BaseOrTier = BaseOrTier.Undefined And charge.TimeOfUse = r.TimeOfUseOriginal And charge.Season = r.Season) Then
                                readingdetail.PriceDetails.Add(New ReadingPriceDetail(charge.BaseOrTier, charge.TimeOfUse, charge.Season, charge.ChargeValue))
                        End If
                    End If

                    'Tiers, TOU only
                        If (readingdetail.TierType = TierType.AdderTOUTier) Then
                            If (charge.BaseOrTier = readingdetail.Tier And charge.TimeOfUse = r.TimeOfUseOriginal And charge.Season = Season.Undefined) Then
                                readingdetail.PriceDetails.Add(New ReadingPriceDetail(charge.BaseOrTier, charge.TimeOfUse, charge.Season, charge.ChargeValue))
                        End If
                    End If

                    'Tiers, seasonal TOU
                        If (readingdetail.TierType = TierType.AdderTOUSeasonalTier) Then
                            If (charge.BaseOrTier = readingdetail.Tier And charge.TimeOfUse = r.TimeOfUseOriginal And charge.Season = r.Season) Then
                                readingdetail.PriceDetails.Add(New ReadingPriceDetail(charge.BaseOrTier, charge.TimeOfUse, charge.Season, charge.ChargeValue))
                        End If
                    End If

                End If

                ' special case Capacity Reserve with IsAdder
                If (r.IsCapacityReserve AndAlso r.IsAdder AndAlso detailedDef.RTPStream = RTPStream.CriticalPeakOnly) Then

                    If (r.TimeOfUseOriginal <> TimeOfUse.Undefined) Then
                        If (charge.BaseOrTier = BaseOrTier.Undefined And charge.TimeOfUse = r.TimeOfUse And charge.Season = r.Season) Then
                                readingdetail.PriceDetails.Add(New ReadingPriceDetail(charge.BaseOrTier, charge.TimeOfUse, charge.Season, charge.ChargeValue))
                        End If
                    End If

                    'Tiers, TOU only
                        If (readingdetail.TierType = TierType.AdderTOUTier) Then
                            If (charge.BaseOrTier = readingdetail.Tier And charge.TimeOfUse = r.TimeOfUse And charge.Season = Season.Undefined) Then
                                readingdetail.PriceDetails.Add(New ReadingPriceDetail(charge.BaseOrTier, charge.TimeOfUse, charge.Season, charge.ChargeValue))
                        End If
                    End If

                    'Tiers, seasonal TOU
                        If (readingdetail.TierType = TierType.AdderTOUSeasonalTier) Then
                            If (charge.BaseOrTier = readingdetail.Tier And charge.TimeOfUse = r.TimeOfUse And charge.Season = r.Season) Then
                                readingdetail.PriceDetails.Add(New ReadingPriceDetail(charge.BaseOrTier, charge.TimeOfUse, charge.Season, charge.ChargeValue))
                        End If
                    End If

                End If

                ' Calculate the TotalPrice and Total Cost for the reading; 
                    readingdetail.TotalPrice = readingdetail.PriceDetails.Sum()
                    readingdetail.TotalCost = readingdetail.TotalPrice * readingdetail.Quantity
                Next
                r.TotalPrice = r.DetailCollection.CalculateTotalPrice(r.Quantity)
                r.TotalCost = r.TotalPrice * r.Quantity
            Next

            readingIndex += 1
        Next

        ' Serialize binary object and log for inspection; only if logging is enabled of course
        Log(readings)

        ' Unscale the readings if a scale factor was applied above; or else the original readings will not be available to caller
        If (extParams.ConversionFactor <> 0 AndAlso extParams.ConversionFactor <> 1) Then
            readings.ApplyScaleFactor((1.0 / extParams.ConversionFactor))
        End If

        Return (readings)
    End Function

    Private Function SplitCostIntoTiers(ByVal tierBoundaries As TierBoundaryCollection, ByVal reading As Reading, ByVal totalaccumulated As Double, ByVal TierType As TierType) As Reading
        reading.DetailCollection = New ReadingDetailCollection()
        Dim readingdetail As ReadingDetail
        Dim totalquantityremaining As Double = reading.Quantity
        'tier = tierBoundaries.GetInclusiveTier(GetAccumulatedTotal(accumulatedUsageTOUSeason, TimeOfUse.Undefined, Enums.Season.Undefined, extParams.UseIndependentTierAccumulations, extParams.UseIndependentTierAccumulationsBySeason))
        Dim index As Integer

        For Each boundary As TierBoundary In tierBoundaries
            If (totalquantityremaining > 0) Then


                If (totalaccumulated + totalquantityremaining > boundary.Threshold And boundary.Threshold - totalaccumulated > 0) Then
                    readingdetail = New ReadingDetail(boundary.BaseOrTier, TierType, boundary.Threshold - totalaccumulated)
                    reading.DetailCollection.Add(readingdetail)
                    totalquantityremaining = totalaccumulated + totalquantityremaining - boundary.Threshold
                    totalaccumulated = boundary.Threshold
                ElseIf (boundary.Threshold - totalaccumulated > 0) Then
                    readingdetail = New ReadingDetail(boundary.BaseOrTier, TierType, totalquantityremaining)
                    reading.DetailCollection.Add(readingdetail)
                    totalquantityremaining = totalaccumulated + totalquantityremaining - boundary.Threshold
                    totalaccumulated = totalaccumulated + totalquantityremaining
                    'ElseIf index = tierBoundaries.Count - 1 Then
                    '    readingdetail = New ReadingDetail(CType(CInt(boundary.BaseOrTier) + 1, BaseOrTier), TierType, totalquantityremaining)
                    '    reading.DetailCollection.Add(readingdetail)
                    '    totalaccumulated = totalaccumulated + totalquantityremaining
                    '    totalquantityremaining = 0

                End If
            Else
                Exit For
            End If
            index = index + 1


        Next

        If (totalquantityremaining > 0) Then
            readingdetail = New ReadingDetail(CType(CInt(tierBoundaries(tierBoundaries.Count - 1).BaseOrTier) + 1, BaseOrTier), TierType, totalquantityremaining)
            reading.DetailCollection.Add(readingdetail)
            totalquantityremaining = 0
        ElseIf (reading.Quantity < 0) Then
            ' if negative
            readingdetail = New ReadingDetail(BaseOrTier.Tier1, TierType, totalquantityremaining)
            reading.DetailCollection.Add(readingdetail)
        End If

        'For Each boundary As TierBoundary In tierBoundaries
        '    If totalaccumulated < boundary.Threshold And reading.Quantity + totalaccumulated > boundary.Threshold And totalquantityremaining > 0 Then
        '        readingdetail = New ReadingDetail(boundary.BaseOrTier, TierType, boundary.Threshold - totalaccumulated)

        '        totalquantityremaining = totalquantityremaining + totalaccumulated - boundary.Threshold
        '        totalaccumulated = boundary.Threshold
        '        reading.DetailCollection.Add(readingdetail)

        '    ElseIf totalaccumulated < boundary.Threshold AndAlso reading.Quantity + totalaccumulated < boundary.Threshold AndAlso totalquantityremaining > 0 Then
        '        readingdetail = New ReadingDetail(boundary.BaseOrTier, TierType, reading.Quantity)
        '        totalquantityremaining = totalquantityremaining - boundary.Threshold

        '        reading.DetailCollection.Add(readingdetail)
        '    End If

        'Next

        Return reading
    End Function
    Private Function UpdateTierDetails(ByVal detailedDef As DetailedRateDefinition, ByVal reading As Reading, ByVal accumulatedUsageTOUSeason(,) As Double, ByVal tierBoundaryColl(,) As TierBoundaryCollection, ByVal extParams As BinReadingsExtParam) As Reading

        Dim tierBoundaries As TierBoundaryCollection
        Dim tier As BaseOrTier
        Dim activeTOU As TimeOfUse
        'reading.Detail = New ReadingDetail()

        If (detailedDef.IsTiered) Then

            'basic tiers if exist
            tierBoundaries = tierBoundaryColl(Enums.TimeOfUse.Undefined, Enums.Season.Undefined)
            If (Not tierBoundaries Is Nothing) Then
                If (tierBoundaries.Count > 0) Then
                    ' tier = tierBoundaries.GetInclusiveTier(GetAccumulatedTotal(accumulatedUsageTOUSeason, TimeOfUse.Undefined, Enums.Season.Undefined, extParams.UseIndependentTierAccumulations, extParams.UseIndependentTierAccumulationsBySeason))
                    'If (tier <> BaseOrTier.Undefined) Then
                    'reading.Detail.TierRegular = tier
                    reading = SplitCostIntoTiers(tierBoundaries, reading, GetAccumulatedTotal(accumulatedUsageTOUSeason, TimeOfUse.Undefined, Enums.Season.Undefined, extParams.UseIndependentTierAccumulations, extParams.UseIndependentTierAccumulationsBySeason), TierType.RegularTier)
                    'End If
                End If
            End If

            'seasonal tiers only if exist
            If (reading.Season <> Season.Undefined) Then
                tierBoundaries = tierBoundaryColl(Enums.TimeOfUse.Undefined, reading.Season)
                If (Not tierBoundaries Is Nothing) Then
                    If (tierBoundaries.Count > 0) Then
                        'tier = tierBoundaries.GetInclusiveTier(GetAccumulatedTotal(accumulatedUsageTOUSeason, TimeOfUse.Undefined, reading.Season, extParams.UseIndependentTierAccumulations, extParams.UseIndependentTierAccumulationsBySeason))
                        ' If (tier <> BaseOrTier.Undefined) Then
                        '   reading.Detail.TierSeasonal = tier
                        reading = SplitCostIntoTiers(tierBoundaries, reading, GetAccumulatedTotal(accumulatedUsageTOUSeason, TimeOfUse.Undefined, reading.Season, extParams.UseIndependentTierAccumulations, extParams.UseIndependentTierAccumulationsBySeason), TierType.SeasonalTier)
                        'End If
                    End If
                End If
            End If

            'tou tiers only if exist
            If (reading.TimeOfUse <> TimeOfUse.Undefined) Then

                'Normal or PTR
                If (Not (reading.TimeOfUseModified And detailedDef.RTPStream = RTPStream.PeakTimeRebateOnly)) Then activeTOU = reading.TimeOfUse Else activeTOU = reading.TimeOfUseOriginal

                tierBoundaries = tierBoundaryColl(activeTOU, Enums.Season.Undefined)
                If (Not tierBoundaries Is Nothing) Then
                    If (tierBoundaries.Count > 0) Then
                        'tier = tierBoundaries.GetInclusiveTier(GetAccumulatedTotal(accumulatedUsageTOUSeason, activeTOU, Enums.Season.Undefined, extParams.UseIndependentTierAccumulations, extParams.UseIndependentTierAccumulationsBySeason))
                        'If (tier <> BaseOrTier.Undefined) Then
                        reading = SplitCostIntoTiers(tierBoundaries, reading, GetAccumulatedTotal(accumulatedUsageTOUSeason, activeTOU, Enums.Season.Undefined, extParams.UseIndependentTierAccumulations, extParams.UseIndependentTierAccumulationsBySeason), TierType.TOUTier)
                        'reading.Detail.TierTOU = tier
                        'End If
                    End If
                End If

                'CPP IsAdder
                If (reading.IsAdder AndAlso reading.TimeOfUseModified AndAlso detailedDef.RTPStream = RTPStream.CriticalPeakOnly) Then

                    tierBoundaries = tierBoundaryColl(reading.TimeOfUseOriginal, Enums.Season.Undefined)
                    If (Not tierBoundaries Is Nothing) Then
                        If (tierBoundaries.Count > 0) Then
                            'tier = tierBoundaries.GetInclusiveTier(GetAccumulatedTotal(accumulatedUsageTOUSeason, reading.TimeOfUseOriginal, Enums.Season.Undefined, extParams.UseIndependentTierAccumulations, extParams.UseIndependentTierAccumulationsBySeason))
                            ''If (tier <> BaseOrTier.Undefined) Then
                            reading = SplitCostIntoTiers(tierBoundaries, reading, GetAccumulatedTotal(accumulatedUsageTOUSeason, reading.TimeOfUseOriginal, Enums.Season.Undefined, extParams.UseIndependentTierAccumulations, extParams.UseIndependentTierAccumulationsBySeason), TierType.AdderTOUTier)
                            'reading.Detail.AdderTierTOU = tier
                            'End If
                        End If
                    End If

                End If

                'CPP Capacity Reserve and IsAdder.  Note that TOUModified = false in reading that is capacity reserve.  Only the cap res quantity needs to be added as a detail if it is also a adder!
                If (reading.IsCapacityReserve AndAlso reading.IsAdder AndAlso detailedDef.RTPStream = RTPStream.CriticalPeakOnly) Then

                    tierBoundaries = tierBoundaryColl(reading.TimeOfUse, Enums.Season.Undefined)
                    If (Not tierBoundaries Is Nothing) Then
                        If (tierBoundaries.Count > 0) Then
                            'tier = tierBoundaries.GetInclusiveTier(GetAccumulatedTotal(accumulatedUsageTOUSeason, reading.TimeOfUse, Enums.Season.Undefined, extParams.UseIndependentTierAccumulations, extParams.UseIndependentTierAccumulationsBySeason))
                            'If (tier <> BaseOrTier.Undefined) Then
                            reading = SplitCostIntoTiers(tierBoundaries, reading, GetAccumulatedTotal(accumulatedUsageTOUSeason, reading.TimeOfUse, Enums.Season.Undefined, extParams.UseIndependentTierAccumulations, extParams.UseIndependentTierAccumulationsBySeason), TierType.AdderTOUTier)
                            'reading.Detail.AdderTierTOU = tier
                            'End If
                        End If
                    End If

                End If

                'CPP Capacity Reserve - no adder - nothing special in this case.


            End If

            'seasonal tou tiers if exist; be careful not to count and set to wrong Detail if already handled above
            If (reading.TimeOfUse <> TimeOfUse.Undefined And reading.Season <> Season.Undefined) Then

                'Normal or PTR
                If (Not (reading.TimeOfUseModified And detailedDef.RTPStream = RTPStream.PeakTimeRebateOnly)) Then activeTOU = reading.TimeOfUse Else activeTOU = reading.TimeOfUseOriginal

                tierBoundaries = tierBoundaryColl(activeTOU, reading.Season)
                If (Not tierBoundaries Is Nothing) Then
                    If (tierBoundaries.Count > 0) Then
                        tier = tierBoundaries.GetInclusiveTier(GetAccumulatedTotal(accumulatedUsageTOUSeason, activeTOU, reading.Season, extParams.UseIndependentTierAccumulations, extParams.UseIndependentTierAccumulationsBySeason))
                        ' If (tier <> BaseOrTier.Undefined) Then
                        'reading.Detail.TierTOUSeasonal = tier
                        reading = SplitCostIntoTiers(tierBoundaries, reading, GetAccumulatedTotal(accumulatedUsageTOUSeason, activeTOU, reading.Season, extParams.UseIndependentTierAccumulations, extParams.UseIndependentTierAccumulationsBySeason), TierType.SeasonalTOUTier)
                        'End If
                    End If
                End If

                'CPP IsAdder
                If (reading.IsAdder AndAlso reading.TimeOfUseModified AndAlso detailedDef.RTPStream = RTPStream.CriticalPeakOnly) Then

                    tierBoundaries = tierBoundaryColl(reading.TimeOfUseOriginal, reading.Season)
                    If (Not tierBoundaries Is Nothing) Then
                        If (tierBoundaries.Count > 0) Then
                            'tier = tierBoundaries.GetInclusiveTier(GetAccumulatedTotal(accumulatedUsageTOUSeason, reading.TimeOfUseOriginal, reading.Season, extParams.UseIndependentTierAccumulations, extParams.UseIndependentTierAccumulationsBySeason))
                            'If (tier <> BaseOrTier.Undefined) Then
                            ' reading.Detail.AdderTierTOUSeasonal = tier
                            reading = SplitCostIntoTiers(tierBoundaries, reading, GetAccumulatedTotal(accumulatedUsageTOUSeason, reading.TimeOfUseOriginal, reading.Season, extParams.UseIndependentTierAccumulations, extParams.UseIndependentTierAccumulationsBySeason), TierType.AdderTOUSeasonalTier)
                            'End If
                        End If
                    End If

                End If

                'CPP Capacity Reserve and IsAdder.  Note that TOUModified = false in reading that is capacity reserve.  Only the cap res quantity needs to be added as a detail if it is also a adder!
                If (reading.IsCapacityReserve AndAlso reading.IsAdder AndAlso detailedDef.RTPStream = RTPStream.CriticalPeakOnly) Then

                    tierBoundaries = tierBoundaryColl(reading.TimeOfUse, reading.Season)
                    If (Not tierBoundaries Is Nothing) Then
                        If (tierBoundaries.Count > 0) Then
                            ' tier = tierBoundaries.GetInclusiveTier(GetAccumulatedTotal(accumulatedUsageTOUSeason, reading.TimeOfUse, reading.Season, extParams.UseIndependentTierAccumulations, extParams.UseIndependentTierAccumulationsBySeason))
                            'If (tier <> BaseOrTier.Undefined) Then
                            reading = SplitCostIntoTiers(tierBoundaries, reading, GetAccumulatedTotal(accumulatedUsageTOUSeason, reading.TimeOfUse, reading.Season, extParams.UseIndependentTierAccumulations, extParams.UseIndependentTierAccumulationsBySeason), TierType.AdderTOUSeasonalTier)
                            ' reading.Detail.AdderTierTOUSeasonal = tier
                            'End If
                        End If
                    End If

                End If

                'CPP Capacity Reserve - no adder - nothing special in this case.


            End If

        End If
        Return reading
    End Function
    ''' <summary>
    ''' This helps control the tiering aspect of the readings, to enable accurate creation of usage determinants.
    ''' </summary>
    ''' <param name="accumulatedUsageTOUSeason"></param>
    ''' <param name="tou"></param>
    ''' <param name="season"></param>
    ''' <param name="independentTieredAccumulations"></param>
    ''' <param name="independentTieredAccumulationsBySeason"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function GetAccumulatedTotal(ByVal accumulatedUsageTOUSeason As Double(,), ByVal tou As TimeOfUse, ByVal season As Season, ByVal independentTieredAccumulations As Boolean, ByVal independentTieredAccumulationsBySeason As Boolean) As Double
        Dim result As Double = 0.0

        If (Not independentTieredAccumulations AndAlso Not independentTieredAccumulationsBySeason) Then
            'typical 
            result = accumulatedUsageTOUSeason(TimeOfUse.Undefined, season.Undefined)

        ElseIf (independentTieredAccumulations) Then
            'for tiered seasonal tou where authored specifically
            result = accumulatedUsageTOUSeason(tou, season)

        Else
            ' for tiered seasonal tou with Daily/percentBaseTier1 bounds
            result = accumulatedUsageTOUSeason(TimeOfUse.Undefined, season)

        End If

        Return (result)
    End Function

    Private Function CheckDefinitionAndAdjustIndex(ByVal defs As DetailedRateDefinitionCollection, ByRef ii As Integer, ByRef bNextDefExists As Boolean, ByRef nextDef As DetailedRateDefinition, ByVal readingDate As DateTime) As Boolean
        Dim continueCheck As Boolean = False

        Do
            ii = ii - 1

            If (ii - 1 >= 0) Then
                nextDef = defs(ii - 1)
                bNextDefExists = True

                If (nextDef.StartDate <= readingDate) Then
                    continueCheck = True
                Else
                    continueCheck = False
                End If
            Else
                bNextDefExists = False
                continueCheck = False
            End If

        Loop While (continueCheck)

        Return (True)
    End Function

    Private Function AssignHolidaysToReadings(ByVal holidays As HolidayCollection, ByVal readings As ReadingCollection, ByVal overrideHolidayDayType As DayType) As Boolean
        Dim h As Holiday
        Dim index As Integer
        Dim topIndex As Integer
        Dim dayOfMonth As Integer

        topIndex = readings.Count - 1

        For Each h In holidays
            index = readings.FindIndexOfReadingWithDate(h.DateOfHoliday)
            If (index <> -1) Then
                dayOfMonth = readings(index).Datestamp.Day

                Do
                    readings(index).DayType = overrideHolidayDayType
                    readings(index).Holiday = True
                    index = index + 1
                    If (index > topIndex) Then Exit Do
                Loop While (dayOfMonth = readings(index).Datestamp.Day)

            End If
        Next

        Return (True)
    End Function

    Private Function BinningForCriticalPeak(ByVal objReadings As ReadingCollection, ByVal objDetailedDef As DetailedRateDefinition, ByVal extParams As BinReadingsExtParam) As ReadingCollection
        Dim objPriceCollection As PriceCollection = Nothing
        Dim objDC As dctlRateEngine
        Dim dtLocalStartDate As DateTime
        Dim dtLocalEndDate As DateTime
        Dim x, i, j, d, k As Integer
        Dim dtFirstPrice As DateTime
        Dim nReadingsUpperBound As Integer
        Dim eReadingInterval As Enums.ReadingInterval
        Dim nRTPGroup As Integer
        Dim reformattedEndDate As DateTime
        Dim reformattedStartDate As DateTime
        Dim useInlinePeakEventInfo As Boolean = False
        Dim isAdder As Boolean = False
        Dim isCapRes As Boolean = False
        Dim isCapResDemandSpecified As Boolean = False
        Dim capResDemandVal As Double = 0.0
        Dim eventDays As List(Of Integer) = Nothing
        Dim eventDayAdded As Boolean = False

        If (Not objReadings Is Nothing) Then
            If (objReadings.Count > 0) Then

                eReadingInterval = objReadings.DetermineIntervalOfReadings()

                If (eReadingInterval <> ReadingInterval.Daily) Then

                    dtLocalStartDate = objReadings(0).Datestamp
                    dtLocalEndDate = objReadings(objReadings.Count - 1).Datestamp

                    ' need to add 23 hours to endDate in order to get all needed prices for the entire day; only use it for this query.  Strip off time and add 23 hours.
                    reformattedEndDate = New DateTime(dtLocalEndDate.Year, dtLocalEndDate.Month, dtLocalEndDate.Day)
                    reformattedEndDate = reformattedEndDate.AddHours(23)
                    reformattedStartDate = New DateTime(dtLocalStartDate.Year, dtLocalStartDate.Month, dtLocalStartDate.Day)


                    ' use inline peak events?
                    If (Not extParams Is Nothing AndAlso _
                        Not extParams.PeakEventInformation Is Nothing AndAlso _
                        extParams.PeakEventInformation.UseInlinePeakEvents = True AndAlso _
                        Not extParams.PeakEventInformation.PeakEvents Is Nothing) _
                    Then
                        useInlinePeakEventInfo = True
                    End If

                    ' This is the REGULAR PRICE FEED EVENTS way of handling here, by querying the database for peak event information.
                    If (Not useInlinePeakEventInfo) Then

                        ' use date ranges here
                        objDC = New dctlRateEngine(m_sRateEngineConnectionString, m_xmlFilesPath, m_dataSourceType, m_cachingType, m_cacheTimeoutInMinutes)

                        ' Get valid RTPGroup from UseCharges 
                        For Each objCharge As UseCharge In objDetailedDef.UseCharges

                            If objCharge.RealTimePricing = True Then
                                nRTPGroup = objCharge.RTPGroup
                                Exit For
                            End If

                        Next

                        ' If we have a future date in our range then we need to use last year's date when we get pricing events.  It is possible to have some dates in the past and 
                        ' some in the future when working in the current month.
                        If reformattedStartDate > DateTime.Now Or reformattedEndDate > DateTime.Now Then
                            objPriceCollection = objDC.GetPriceCollectionPreviousYear(objDetailedDef.RateCompanyID, nRTPGroup, objDetailedDef.RateClass, dtLocalStartDate, reformattedEndDate, DateTime.Now)
                        Else
                            ' don't need to do anything with the dates
                            objPriceCollection = objDC.GetPriceData(objDetailedDef.RateCompanyID, nRTPGroup, objDetailedDef.RateClass, dtLocalStartDate, reformattedEndDate, True)
                        End If

                    Else
                        If (Not extParams Is Nothing AndAlso _
                            Not extParams.PeakEventInformation Is Nothing AndAlso _
                            extParams.PeakEventInformation.PeakEvents.Count > 0) Then

                            ' Initialize price collection using the INLINE PEAK EVENTS.  These are initialized as RTPActualStream.CriticalPeakOnly (for CPP and PTR)                       
                            For Each pe As PeakEvent In extParams.PeakEventInformation.PeakEvents

                                If (pe.Datestamp >= reformattedStartDate AndAlso pe.Datestamp <= reformattedEndDate) Then

                                    ' get a reading in which the event resides, so we can determine the season, since inline events are seasonal.
                                    Dim indexOfReading As Integer = objReadings.FindIndexOfReadingWithMonthAndDay(pe.Datestamp.Month, pe.Datestamp.Day)

                                    If (indexOfReading <> -1) Then
                                        Dim r As Reading = objReadings(indexOfReading)
                                        Dim s As Integer = CInt(r.Season)
                                        ' if we do not have all seasons represented here, bring index down.
                                        If (s > pe.Durations.Count - 1) Then
                                            s = pe.Durations.Count - 1
                                        End If

                                        If (objPriceCollection Is Nothing) Then objPriceCollection = New PriceCollection()

                                        For h As Integer = pe.Durations(s).StartHour To pe.Durations(s).EndHour
                                            Dim price As New Price()
                                            price.DateStamp = New DateTime(pe.Datestamp.Year, pe.Datestamp.Month, pe.Datestamp.Day, h, 0, 0)
                                            price.Price = 0.0
                                            price.PriceStream = RTPActualStream.CriticalPeakOnly
                                            objPriceCollection.Add(price)
                                        Next

                                    End If

                                End If

                            Next

                            ' sort the prices in case the peak events were not ordered.  Fill missing prices for use here.
                            If (Not objPriceCollection Is Nothing AndAlso objPriceCollection.Count > 0) Then
                                objPriceCollection.Sort()
                                objPriceCollection.FillMissingPrices(reformattedStartDate, reformattedEndDate)
                            End If

                        End If
                    End If

                End If

                ' perhaps RTP actually does not exist; unlikely, but may occur if attempt was made to manually provide peak events
                If ((Not objPriceCollection Is Nothing) AndAlso objPriceCollection.Count > 0) Then

                    'Adder preparation
                    isAdder = objDetailedDef.IsCPEventUsageAdder

                    'CapRes preparation
                    isCapRes = objDetailedDef.IsCapacityReserve
                    isCapResDemandSpecified = extParams.CapResDemandSpecified

                    If (isCapRes AndAlso isCapResDemandSpecified) Then
                        Select Case eReadingInterval
                            Case ReadingInterval.Hourly
                                capResDemandVal = extParams.CapResDemand
                            Case ReadingInterval.HalfHourly
                                capResDemandVal = extParams.CapResDemand / 2.0
                            Case ReadingInterval.FifteenMinute
                                capResDemandVal = extParams.CapResDemand / 4.0
                            Case ReadingInterval.FiveMinute
                                capResDemandVal = extParams.CapResDemand / 12.0
                            Case Else
                                capResDemandVal = extParams.CapResDemand
                        End Select
                    End If

                    ' begin
                    dtFirstPrice = objPriceCollection(0).DateStamp

                    nReadingsUpperBound = objReadings.Count
                    d = 0

                    For i = 0 To objPriceCollection.Count - 1
                        j = i Mod 24

                        eventDayAdded = False

                        If (i < nReadingsUpperBound) Then

                            ' When the actual stream is CriticalPeakOnly, it means it is for both CPP and PeakTimeRebate (special case of CPP)
                            If objPriceCollection(i).PriceStream = RTPActualStream.CriticalPeakOnly Then

                                If (eventDays Is Nothing) Then eventDays = New List(Of Integer)
                                If (Not eventDayAdded) Then
                                    If (Not eventDays.Contains(objPriceCollection(i).DateStamp.DayOfYear)) Then
                                        eventDays.Add(objPriceCollection(i).DateStamp.DayOfYear)
                                        eventDayAdded = True
                                    End If
                                End If

                                Select Case eReadingInterval

                                    Case ReadingInterval.Hourly

                                        If (Not objReadings(i).TimeOfUseModified) Then
                                            objReadings(i).TimeOfUseModified = True
                                            objReadings(i).TimeOfUseOriginal = objReadings(i).TimeOfUse
                                        End If
                                        objReadings(i).TimeOfUse = TimeOfUse.CriticalPeak

                                        objReadings(i).IsAdder = isAdder
                                        If (isCapRes) Then
                                            objReadings(i).TimeOfUse = objReadings(i).TimeOfUseOriginal
                                            objReadings(i).TimeOfUseModified = False
                                            objReadings(i).CapacityReserveThreshold = capResDemandVal
                                            If (isCapResDemandSpecified AndAlso objReadings(i).Quantity > capResDemandVal) Then
                                                objReadings(i).IsCapacityReserve = True
                                                objReadings(i).CapacityReserveQuantity = objReadings(i).Quantity - capResDemandVal
                                            End If
                                        End If

                                    Case ReadingInterval.HalfHourly

                                        For k = 0 To 1
                                            x = (d * 48) + (j * 2) + k
                                            If (Not objReadings(x).TimeOfUseModified) Then
                                                objReadings(x).TimeOfUseModified = True
                                                objReadings(x).TimeOfUseOriginal = objReadings(x).TimeOfUse
                                            End If
                                            objReadings(x).TimeOfUse = TimeOfUse.CriticalPeak

                                            objReadings(x).IsAdder = isAdder
                                            If (isCapRes) Then
                                                objReadings(x).TimeOfUse = objReadings(x).TimeOfUseOriginal
                                                objReadings(x).TimeOfUseModified = False
                                                objReadings(x).CapacityReserveThreshold = capResDemandVal
                                                If (isCapResDemandSpecified AndAlso objReadings(x).Quantity > capResDemandVal) Then
                                                    objReadings(x).IsCapacityReserve = True
                                                    objReadings(x).CapacityReserveQuantity = objReadings(x).Quantity - capResDemandVal
                                                End If
                                            End If

                                        Next

                                    Case ReadingInterval.FifteenMinute

                                        For k = 0 To 3
                                            x = (d * 96) + (j * 4) + k
                                            If (Not objReadings(x).TimeOfUseModified) Then
                                                objReadings(x).TimeOfUseModified = True
                                                objReadings(x).TimeOfUseOriginal = objReadings(x).TimeOfUse
                                            End If
                                            objReadings(x).TimeOfUse = TimeOfUse.CriticalPeak

                                            objReadings(x).IsAdder = isAdder
                                            If (isCapRes) Then
                                                objReadings(x).TimeOfUse = objReadings(x).TimeOfUseOriginal
                                                objReadings(x).TimeOfUseModified = False
                                                objReadings(x).CapacityReserveThreshold = capResDemandVal
                                                If (isCapResDemandSpecified AndAlso objReadings(x).Quantity > capResDemandVal) Then
                                                    objReadings(x).IsCapacityReserve = True
                                                    objReadings(x).CapacityReserveQuantity = objReadings(x).Quantity - capResDemandVal
                                                End If
                                            End If

                                        Next

                                    Case ReadingInterval.FiveMinute

                                        For k = 0 To 11
                                            x = (d * 288) + (j * 12) + k
                                            If (Not objReadings(x).TimeOfUseModified) Then
                                                objReadings(x).TimeOfUseModified = True
                                                objReadings(x).TimeOfUseOriginal = objReadings(x).TimeOfUse
                                            End If
                                            objReadings(x).TimeOfUse = TimeOfUse.CriticalPeak

                                            objReadings(x).IsAdder = isAdder
                                            If (isCapRes) Then
                                                objReadings(x).TimeOfUse = objReadings(x).TimeOfUseOriginal
                                                objReadings(x).TimeOfUseModified = False
                                                objReadings(x).CapacityReserveThreshold = capResDemandVal
                                                If (isCapResDemandSpecified AndAlso objReadings(x).Quantity > capResDemandVal) Then
                                                    objReadings(x).IsCapacityReserve = True
                                                    objReadings(x).CapacityReserveQuantity = objReadings(x).Quantity - capResDemandVal
                                                End If
                                            End If

                                        Next

                                    Case Else
                                        'noop

                                End Select

                            End If

                        End If

                        If (j = 23) Then d = d + 1

                    Next

                    ' Process OnEventDay list here.
                    If (Not eventDays Is Nothing AndAlso eventDays.Count > 0) Then
                        objReadings.AssignEventDays(eventDays)
                    End If

                End If
            End If
        End If

        Return (objReadings)

    End Function


    Private Function GetSeasonBoundaryArrayForYear(ByVal objSeasonBoundaries As SeasonBoundaryCollection, ByVal nStartYear As Integer, ByVal nEndYear As Integer) As Season(,)
        Dim eSeason(4, 366) As Season  ' need two extra; index 0 not used, last index to accomodate leap year
        Dim i As Integer
        'Dim j As Integer
        Dim nStartIndex, nEndIndex As Integer
        Dim year As Integer
        Dim yearLookupIndex As Integer

        objSeasonBoundaries.SortAscending()

        For year = nStartYear To nEndYear
            yearLookupIndex = year Mod nStartYear

            For Each objB As SeasonBoundary In objSeasonBoundaries
                nStartIndex = objSeasonBoundaries.ConvertDayToDate(objB.StartDay, year).DayOfYear
                nEndIndex = objSeasonBoundaries.ConvertDayToDate(objB.EndDay, year).DayOfYear
                For i = nStartIndex To nEndIndex
                    eSeason(yearLookupIndex, i) = objB.Season
                Next
            Next

        Next

        Return (eSeason)
    End Function

    ''' <summary>
    ''' This method returns TOU boundary array for every day of the week i.e. Sunday, Monday, Tuesday, Wednesday, Thursday, Friday, and Saturday
    ''' even if the rate class is defined only for weekday, weekend or all.
    ''' </summary>
    ''' <param name="objDetailedDef"></param>
    ''' <param name="eSeason"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function GetTOUDayOfWeekBoundaryArray(ByVal objDetailedDef As DetailedRateDefinition, ByVal eSeason As Season) As TimeOfUse(,)
        Dim eRETOU(7, 95) As TimeOfUse '(eDayOfWeek, 15 min interval number for 24 hours i.e. 24x4)
        Dim objBoundaries As TOUBoundaryCollection
        Dim objBoundary As TOUBoundary
        Dim dtStart, dtEnd As DateTime
        Dim sEndTime As String
        Dim sb As New System.Text.StringBuilder
        Dim i, jj, jindex As Integer
        Dim d As CE.RateEngine.Enums.DayType
        Dim initialHour As Integer
        Dim doneWithInitialHour As Boolean

        objBoundaries = objDetailedDef.TOUBoundaries
        objBoundaries.Sort() ' Sort by StartTime

        If (objBoundaries.Count > 0) Then

            For Each objBoundary In objBoundaries

                ' from 0 to 11
                For d = CE.RateEngine.Enums.DayType.Weekday To CE.RateEngine.Enums.DayType.Friday

                    If (objBoundary.DayType = d And objBoundary.Season = eSeason) Then
                        sb.Remove(0, sb.Length)

                        sb.Append("1/1/2004 ") : sb.Append(objBoundary.StartTime) : sb.Append(":00")
                        dtStart = DateTime.Parse(sb.ToString)
                        sb.Remove(0, sb.Length)

                        If (objBoundary.EndTime = "00:00") Then sEndTime = "23:59" Else sEndTime = objBoundary.EndTime
                        sb.Append("1/1/2004 ") : sb.Append(sEndTime) : sb.Append(":00")
                        dtEnd = DateTime.Parse(sb.ToString)

                        ' Note that the last hour is overwritten by the next boundary
                        ' (ex) 00:00 -> 07:00 Offpeak, 07:00 --> 18:00 OnPeak; the OnPeak overwrites the previous Offpeak at hour 07:00

                        ' Iterate through 15 minute intervals for each hour in boundary
                        initialHour = dtStart.Hour
                        doneWithInitialHour = False
                        For i = dtStart.Hour To dtEnd.Hour
                            jindex = 0
                            For jj = 0 To 45 Step 15 '15 minute intervals from start to 45th minute in hour
                                If (i = initialHour And doneWithInitialHour = False) Then
                                    If (dtStart.Minute = 15) Then
                                        jj = 15 : jindex = 1
                                    End If
                                    If (dtStart.Minute = 30) Then
                                        jj = 30 : jindex = 2
                                    End If
                                    If (dtStart.Minute = 45) Then
                                        jj = 45 : jindex = 3
                                    End If
                                    doneWithInitialHour = True
                                End If

                                ' Note: treat Weekend As WeekendHol, override 2 to 1
                                Select Case d
                                    Case DayType.Weekday
                                        eRETOU(DayOfWeek.Monday, (i * 4) + jindex) = objBoundary.TimeOfUse
                                        eRETOU(DayOfWeek.Tuesday, (i * 4) + jindex) = objBoundary.TimeOfUse
                                        eRETOU(DayOfWeek.Wednesday, (i * 4) + jindex) = objBoundary.TimeOfUse
                                        eRETOU(DayOfWeek.Thursday, (i * 4) + jindex) = objBoundary.TimeOfUse
                                        eRETOU(DayOfWeek.Friday, (i * 4) + jindex) = objBoundary.TimeOfUse

                                    Case DayType.WeekendHoliday, DayType.Weekend
                                        eRETOU(DayOfWeek.Saturday, (i * 4) + jindex) = objBoundary.TimeOfUse
                                        eRETOU(DayOfWeek.Sunday, (i * 4) + jindex) = objBoundary.TimeOfUse

                                    Case DayType.Monday
                                        eRETOU(DayOfWeek.Monday, (i * 4) + jindex) = objBoundary.TimeOfUse

                                    Case DayType.Tuesday
                                        eRETOU(DayOfWeek.Tuesday, (i * 4) + jindex) = objBoundary.TimeOfUse

                                    Case DayType.Wednesday
                                        eRETOU(DayOfWeek.Wednesday, (i * 4) + jindex) = objBoundary.TimeOfUse

                                    Case DayType.Thursday
                                        eRETOU(DayOfWeek.Thursday, (i * 4) + jindex) = objBoundary.TimeOfUse

                                    Case DayType.Friday
                                        eRETOU(DayOfWeek.Friday, (i * 4) + jindex) = objBoundary.TimeOfUse

                                    Case DayType.Saturday
                                        eRETOU(DayOfWeek.Saturday, (i * 4) + jindex) = objBoundary.TimeOfUse

                                    Case DayType.Sunday
                                        eRETOU(DayOfWeek.Sunday, (i * 4) + jindex) = objBoundary.TimeOfUse
                                    Case DayType.Holiday
                                        eRETOU(7, (i * 4) + jindex) = objBoundary.TimeOfUse
                                    Case DayType.All
                                        eRETOU(DayOfWeek.Monday, (i * 4) + jindex) = objBoundary.TimeOfUse
                                        eRETOU(DayOfWeek.Tuesday, (i * 4) + jindex) = objBoundary.TimeOfUse
                                        eRETOU(DayOfWeek.Wednesday, (i * 4) + jindex) = objBoundary.TimeOfUse
                                        eRETOU(DayOfWeek.Thursday, (i * 4) + jindex) = objBoundary.TimeOfUse
                                        eRETOU(DayOfWeek.Friday, (i * 4) + jindex) = objBoundary.TimeOfUse
                                        eRETOU(DayOfWeek.Saturday, (i * 4) + jindex) = objBoundary.TimeOfUse
                                        eRETOU(DayOfWeek.Sunday, (i * 4) + jindex) = objBoundary.TimeOfUse

                                End Select

                                jindex = jindex + 1
                            Next
                        Next

                    End If

                Next

            Next

        End If

        ' This will return two dayTypes with TOU information for each 15 minute interval
        Return (eRETOU)
    End Function


    ''' <summary>
    '''  GetDetailedRateInformationUsingReadingsDateRange
    ''' </summary>
    ''' <param name="nRateCompanyID"></param>
    ''' <param name="objReadings"></param>
    ''' <param name="sRateClass"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function GetDetailedRateInformationUsingReadingsDateRange(ByVal nRateCompanyID As Integer, ByVal objReadings As ReadingCollection, ByVal sRateClass As String) As DetailedRateDefinitionCollection
        Return (GetDetailedRateInformationUsingReadingsDateRange(nRateCompanyID, objReadings, sRateClass, False))
    End Function

    Public Function GetDetailedRateInformationUsingReadingsDateRange(ByVal nRateCompanyID As Integer, ByVal objReadings As ReadingCollection, ByVal sRateClass As String, ByVal getNewestDefinitionAndResetStartDate As Boolean) As DetailedRateDefinitionCollection
        Dim objDefinitions As DetailedRateDefinitionCollection

        If (getNewestDefinitionAndResetStartDate) Then

            Const yearOffset As Integer = 5
            Dim defStartDate As DateTime = DateTime.Now.AddYears(yearOffset)
            Dim defEndDate As DateTime = defStartDate.AddDays(2)

            ' Get the newest rate definition by adding 5 years to the startDate, and then reset the definition startDate back 5 years from the effective date! 
            ' Use a parameter that artificially changes the rate definition effective date internally; 
            ' This overload was added to aid in proper caching support, since callers should not change the start date after definitions are returned (since they may come from cache!)
            objDefinitions = GetDetailedRateInformation(nRateCompanyID, sRateClass, defStartDate, defEndDate, -yearOffset)

        Else
            Dim dtReadingsStartDate As DateTime
            Dim dtReadingsEndDate As DateTime

            dtReadingsStartDate = objReadings(0).Datestamp
            dtReadingsEndDate = objReadings(objReadings.Count - 1).Datestamp

            objDefinitions = GetDetailedRateInformation(nRateCompanyID, sRateClass, dtReadingsStartDate, dtReadingsEndDate)
        End If


        Return (objDefinitions)
    End Function

    Public Function GetDetailedRateInformationWithReset(ByVal nRateCompanyID As Integer, ByVal sRateClass As String, ByVal selStartDate As DateTime, ByVal selEndDate As DateTime) As DetailedRateDefinitionCollection
        Dim objDefinitions As DetailedRateDefinitionCollection
        Const yearOffset As Integer = 5

        ' Get the rate definition via passed-in dates, and then reset the definition startDate back 5 years from the effective date, using the overloaded call here
        objDefinitions = GetDetailedRateInformation(nRateCompanyID, sRateClass, selStartDate, selEndDate, -yearOffset)

        Return (objDefinitions)
    End Function

#End Region

#Region "BinReadingsAndGetUsageDeterminants and support functions for COST TO DATE"

    Public Function BinReadingsAndGetUsageDeterminants(ByVal nRateCompanyID As Integer, ByVal detailedDefinitions As DetailedRateDefinitionCollection, ByVal readings As ReadingCollection, ByVal rateClass As String, ByVal startDate As DateTime, ByVal endDate As DateTime, ByVal DSTEndDate As DateTime) As UsageCollection
        Return (BinReadingsAndGetUsageDeterminants(nRateCompanyID, detailedDefinitions, readings, 0.0, rateClass, startDate, endDate, endDate, False, DSTEndDate, Nothing, Nothing))
    End Function

    Public Function BinReadingsAndGetUsageDeterminants(ByVal nRateCompanyID As Integer, ByVal detailedDefinitions As DetailedRateDefinitionCollection, ByVal readings As ReadingCollection, ByVal usageOffsetQuantity As Double, ByVal rateClass As String, ByVal startDate As DateTime, ByVal endDate As DateTime, ByVal DSTEndDate As DateTime) As UsageCollection
        Return (BinReadingsAndGetUsageDeterminants(nRateCompanyID, detailedDefinitions, readings, usageOffsetQuantity, rateClass, startDate, endDate, endDate, False, DSTEndDate, Nothing, Nothing))
    End Function

    Public Function BinReadingsAndGetUsageDeterminants(ByVal nRateCompanyID As Integer, ByVal detailedDefinitions As DetailedRateDefinitionCollection, ByVal readings As ReadingCollection, ByVal usageOffsetQuantity As Double, ByVal rateClass As String, ByVal startDate As DateTime, ByVal endDate As DateTime, ByVal projectedEndDate As DateTime, ByVal useProjectedEndDateWhenTiered As Boolean, ByVal DSTEndDate As DateTime, ByVal raf As ReadingAdjustFactors, ByVal pei As PeakEventInfo) As UsageCollection
        Dim usages As New UsageCollection()
        Dim billDaysOverrideForFinalizingTiers As Integer
        Dim applyFactors As Boolean = False
        Dim isRTP As Boolean = False
        Dim isCPP As Boolean = False
        Dim usePeakEventInfo As Boolean = False

        If (Not raf Is Nothing) Then applyFactors = True
        isRTP = detailedDefinitions(0).IsRTPRate
        If (isRTP And detailedDefinitions(0).RTPStream = RTPStream.CriticalPeakOnly) Then isCPP = True ' used for CPP or Peak-time-rebate.

        If (Not pei Is Nothing AndAlso Not pei.PeakEvents Is Nothing) Then usePeakEventInfo = True

        If (readings.DetermineIntervalOfReadings() <> ReadingInterval.Daily) Then

            ' Most complex is tiered tou; special case of complex is Hybrid tier structure type
            If ((Not detailedDefinitions(0).UseCharges.ExistsAnyTierTOU()) AndAlso (Not IsComplexHybrid(detailedDefinitions(0)))) Then
                ' regular call to bin

                If (applyFactors) Then
                    Log(readings) 'for advanced analysis
                    ApplyReadingAdjustmentFactorToReadings(readings, ReadingAdjustmentFactorType.GeneralConservation, raf.GeneralConservation, raf.PercentReductionAsShifted)
                End If

                Dim extParam As BinReadingsExtParam = New BinReadingsExtParam()
                If (usePeakEventInfo) Then extParam.PeakEventInformation = pei
                If (applyFactors) Then extParam.ReadingAdjustmentFactors = raf

                readings = BinReadings(nRateCompanyID, detailedDefinitions, readings, rateClass, startDate, endDate, extParam)

                If (applyFactors) Then Log(readings) 'for advanced analysis

                usages = GetUsageDeterminants(nRateCompanyID, detailedDefinitions, readings, Nothing, rateClass, startDate, endDate, DSTEndDate, True)

            Else
                ' do enhanced binning to support complex tiered and tou
                If (useProjectedEndDateWhenTiered) Then
                    billDaysOverrideForFinalizingTiers = projectedEndDate.Subtract(startDate).Days + 1
                Else
                    billDaysOverrideForFinalizingTiers = endDate.Subtract(startDate).Days + 1
                End If

                If (applyFactors) Then
                    Log(readings) 'for advanced analysis
                    ApplyReadingAdjustmentFactorToReadings(readings, ReadingAdjustmentFactorType.GeneralConservation, raf.GeneralConservation, raf.PercentReductionAsShifted)
                End If

                Dim extParam As BinReadingsExtParam = New BinReadingsExtParam(usageOffsetQuantity, True, True, billDaysOverrideForFinalizingTiers)
                If (usePeakEventInfo) Then extParam.PeakEventInformation = pei
                If (applyFactors) Then extParam.ReadingAdjustmentFactors = raf

                readings = BinReadings(nRateCompanyID, detailedDefinitions, readings, rateClass, startDate, endDate, extParam)

                ' use the price details in the enhanced readings to build list of usage determinants.
                usages = GetComplexUsageDeterminants(nRateCompanyID, detailedDefinitions, readings, rateClass, startDate, endDate, DSTEndDate, True)

            End If

        Else

            If (applyFactors) Then
                ApplyReadingAdjustmentFactorToReadings(readings, ReadingAdjustmentFactorType.GeneralConservation, raf.GeneralConservation, raf.PercentReductionAsShifted)
            End If

            usages = GetUsageDeterminants(nRateCompanyID, detailedDefinitions, readings, Nothing, rateClass, startDate, endDate, DSTEndDate, True)

        End If

        Return (usages)
    End Function

    ''' <summary>
    ''' For semi complex adjustment of usage determinants for use by CostToDate.
    ''' </summary>
    Public Function AdjustSeasonalUsageDeterminants(ByVal nRateCompanyID As Integer, ByVal objDefinitions As DetailedRateDefinitionCollection, ByVal currentUsages As UsageCollection, ByVal sRateClass As String, ByVal dtStartDate As DateTime, ByVal dtEndDate As DateTime, ByVal dtProjectedEndDate As DateTime, ByVal usageScaleFactor As Double, ByVal dwellingCount As Integer, ByVal tierDaysInMonth As Integer) As UsageCollection
        Dim usages As UsageCollection = Nothing
        Dim diffScaledTOUUsages As UsageCollection
        Dim detailedDef As DetailedRateDefinition
        Dim i As Integer = 0
        Dim bUseSeasonProrateOverride As Boolean
        Dim eOverrideSeason As Season
        Dim seasonFactors As SeasonFactorCollection
        Dim billDays As Integer
        Dim total As Double
        Dim originalTotal As Double
        Dim complexityIsTooHigh As Boolean = False

        ' some callers will pass this, older callers will not, and thus this needs to be instantiated correctly
        If (objDefinitions Is Nothing) Then
            objDefinitions = GetDetailedRateInformation(nRateCompanyID, sRateClass, dtStartDate, dtProjectedEndDate)
        End If

        If (objDefinitions.Count > 0) Then

            detailedDef = objDefinitions(0)

            bUseSeasonProrateOverride = ProcessSeasonalProrate(objDefinitions, dtStartDate, dtProjectedEndDate, eOverrideSeason)

            ' get season factors using remaing days; endDate+1 to projectedEndDate
            If (detailedDef.IsSeasonal) Then
                billDays = dtProjectedEndDate.Subtract(dtEndDate).Days
                seasonFactors = DetermineSeasonFactors(detailedDef, billDays, dtEndDate.AddDays(1), dtProjectedEndDate, True, 1.0, bUseSeasonProrateOverride, eOverrideSeason)

                If (seasonFactors.Count > 1 And (Not detailedDef.BoundariesConsistent)) Then
                    ' cannot handle seasonal complexity with varying time of use boundaries in seasons since it is too inaccurate;
                    ' callerse should be passing readings and using the enhanced projection logic rather than ending up here
                    complexityIsTooHigh = True
                End If

            Else
                seasonFactors = DetermineSeasonFactors()
            End If


            If (Not complexityIsTooHigh) Then

                'copy currentUsage to local usages in order to scale and add additional determinants as needed
                usages = currentUsages.Copy()
                originalTotal = usages.GetTotalServiceUse()

                'scale the total
                usages.ScaleTotal(usageScaleFactor)
                total = usages.GetTotalServiceUse()

                'apply scale to tou, get diff of usages; reassign as-if non seasonal and rebin to seasonal
                diffScaledTOUUsages = usages.DifferenceOfScaleNonTieredTimeOfUse(usageScaleFactor)
                diffScaledTOUUsages.ReassignSeason(Season.Undefined)
                diffScaledTOUUsages = RebinUsagesForSeasonalTOU(diffScaledTOUUsages, objDefinitions, dtEndDate.AddDays(1), dtProjectedEndDate, bUseSeasonProrateOverride, eOverrideSeason)

                usages.Combine(diffScaledTOUUsages)

            End If

        End If

        Return (usages)
    End Function

    Public Function AdjustComplexUsageDeterminants(ByVal nRateCompanyID As Integer, ByVal objDefinitions As DetailedRateDefinitionCollection, ByVal currentUsages As UsageCollection, ByVal sRateClass As String, ByVal dtStartDate As DateTime, ByVal dtEndDate As DateTime, ByVal dtProjectedEndDate As DateTime, ByVal usageScaleFactor As Double, ByVal dwellingCount As Integer, ByVal tierDaysInMonth As Integer) As UsageCollection
        Dim finalUsages As New UsageCollection()
        Dim usages As UsageCollection
        Dim detailedDef As DetailedRateDefinition
        Dim i As Integer = 0
        Dim bUseSeasonProrateOverride As Boolean
        Dim eOverrideSeason As Season
        Dim seasonFactors As SeasonFactorCollection
        Dim sf As SeasonFactor
        Dim billDays As Integer
        Dim tierBoundaries As TierBoundaryCollection
        Dim finalTierBoundaries As TierBoundaryCollection
        Dim total As Double
        Dim totalLeft As Double
        Dim q As Double
        Dim tieredUsageQuantities As New System.Collections.Generic.List(Of Double)
        Dim highTierNum As Integer
        Dim currentTier As Integer
        Dim tieredUsages As UsageCollection
        Dim firstTieredUsages As UsageCollection = Nothing
        Dim totalTieredUsageQuantity As Double
        Dim usageRatios As New System.Collections.Generic.List(Of Double)
        Dim availableQuantity As Double
        Dim postProcessHighTierNum As Integer = 1
        Dim complexityIsTooHigh As Boolean = False
        Dim totalTieredUsage As Double

        ' some callers will pass this, older callers will not, and thus this needs to be instantiated correctly
        If (objDefinitions Is Nothing) Then
            objDefinitions = GetDetailedRateInformation(nRateCompanyID, sRateClass, dtStartDate, dtProjectedEndDate)
        End If

        If (objDefinitions.Count > 0) Then

            detailedDef = objDefinitions(0)

            'must be a complex rate definition to proceed
            If (detailedDef.UseCharges.ExistsAnyTierTOU()) Then

                bUseSeasonProrateOverride = ProcessSeasonalProrate(objDefinitions, dtStartDate, dtProjectedEndDate, eOverrideSeason)

                ' get season factors using remaing days; endDate to projectedEndDate
                If (detailedDef.IsSeasonal) Then
                    billDays = dtProjectedEndDate.Subtract(dtEndDate).Days
                    seasonFactors = DetermineSeasonFactors(detailedDef, billDays, dtEndDate.AddDays(1), dtProjectedEndDate, True, 1.0, bUseSeasonProrateOverride, eOverrideSeason)

                    If (seasonFactors.Count > 1) Then
                        ' cannot handle seasonal complexity for seasonal tiered tou when we have two or more seasons in-play for the 
                        ' projected portion of the calculation
                        complexityIsTooHigh = True
                    End If

                    seasonFactors.Add(Season.Undefined, New SeasonFactor(Season.Undefined, 1.0))
                Else
                    seasonFactors = DetermineSeasonFactors()
                End If


                If (Not complexityIsTooHigh) Then

                    tierBoundaries = detailedDef.TierBoundaries.GetFirstAvailable()

                    finalTierBoundaries = FinalizeTierBoundaries(detailedDef.RateCompanyID, detailedDef.DefinitionID, detailedDef.MasterID, _
                                                                    tierBoundaries, detailedDef.TierStructureType, _
                                                                    sRateClass, tierBoundaries(0).Season, tierBoundaries(0).TimeOfUse, _
                                                                    dwellingCount, tierDaysInMonth, currentUsages)

                    'copy currentUsage to local usages in order to scale and add additional determinants as needed
                    usages = currentUsages.Copy()

                    'finally examine current usages and apply a scale factor to the total, tou, and highest tier
                    usages.ScaleTotal(usageScaleFactor)
                    total = usages.GetTotalServiceUse()

                    'apply scale to non-tiered tou
                    usages.ScaleNonTieredTimeOfUse(usageScaleFactor)

                    'tiering using total after it was scaled; this gives us the needed real boundaries for usage
                    totalLeft = total
                    For i = 0 To finalTierBoundaries.Count - 1

                        If (totalLeft > 0) Then
                            If (i = 0) Then
                                q = Math.Min(totalLeft, finalTierBoundaries(i).Threshold)
                            Else
                                q = Math.Min(totalLeft, finalTierBoundaries(i).Threshold - finalTierBoundaries(i - 1).Threshold)
                            End If
                            tieredUsageQuantities.Add(q)

                            totalLeft = totalLeft - q
                        End If

                    Next

                    If (totalLeft > 0) Then
                        tieredUsageQuantities.Add(totalLeft)
                    End If


                    '*** Apply scale to tiers; for at most one season and undefined season
                    ' season handles one season and undefined only; multiple seasons will short-circuit; too complex
                    For Each sfd As DictionaryEntry In seasonFactors
                        sf = CType(sfd.Value, SeasonFactor)

                        highTierNum = usages.GetHighestTierNumber(sf.Season)
                        postProcessHighTierNum = Math.Max(postProcessHighTierNum, highTierNum)

                        ' highTierNum is zero when tiered usage does not exist for the provided season
                        If (highTierNum > 0) Then

                            ' process tiers; note that undefined tou and tou are treated separately
                            For currentTier = highTierNum To tieredUsageQuantities.Count

                                tieredUsages = usages.GetTieredUsageForTier(CType(currentTier, BaseOrTier), sf.Season)
                                totalTieredUsage = usages.GetTieredTotalUsageForTier(CType(currentTier, BaseOrTier))

                                If (tieredUsages.Count = 0) Then
                                    ' this means we have entered a tier where no usage determinants yet exist; use previous tier ratios
                                    ' and initialize tieredUsages with zero quantities to start
                                    tieredUsages = firstTieredUsages.Copy()
                                    tieredUsages.ReassignTier(CType(CInt(currentTier), BaseOrTier)) 'tick up the tier
                                    tieredUsages.ZeroAllUsage()
                                    totalTieredUsage = 0
                                Else
                                    ' may need this in subsequent iterations
                                    firstTieredUsages = tieredUsages.Copy()
                                End If

                                totalTieredUsageQuantity = tieredUsages.GetTotalExcludeTOU(TimeOfUse.Undefined)
                                'totalTieredUsageQuantity = totalTieredUsage
                                availableQuantity = tieredUsageQuantities(currentTier - 1) - totalTieredUsageQuantity

                                For index As Integer = 0 To tieredUsages.Count - 1

                                    ' only create ratios first time through
                                    If (currentTier = highTierNum) Then
                                        If (totalTieredUsageQuantity <> 0) Then
                                            usageRatios.Add(tieredUsages(index).Quantity / totalTieredUsageQuantity)
                                        Else
                                            usageRatios.Add(1.0)
                                        End If
                                    End If

                                    tieredUsages(index).Quantity = tieredUsages(index).Quantity + (availableQuantity * usageRatios(index))

                                Next

                                ' ADD USAGES to final collection of usages; CRITICAL
                                For Each u As Usage In tieredUsages
                                    finalUsages.Add(u)
                                Next

                            Next

                        End If

                    Next

                    ' if no determinants were created above, it was a season boundary switchover, so it was still too complex
                    If (finalUsages.Count > 0) Then

                        ' ADD USAGES; INSERT at top; add all tiered usages that were not touched; CRITICAL

                        If (postProcessHighTierNum > 1) Then
                            For currentTier = 1 To postProcessHighTierNum - 1

                                For Each u As Usage In usages
                                    If (u.BaseOrTier = currentTier) Then
                                        finalUsages.Insert(0, u)
                                    End If
                                Next

                            Next
                        End If

                        ' ADD USAGES;  INSERT at top; add each non-tiered usage that was not processed in main loop to final usages; CRITICAL
                        For Each u As Usage In usages
                            If (u.BaseOrTier <> BaseOrTier.Tier1 And _
                                u.BaseOrTier <> BaseOrTier.Tier2 And _
                                u.BaseOrTier <> BaseOrTier.Tier3 And _
                                u.BaseOrTier <> BaseOrTier.Tier4 And _
                                u.BaseOrTier <> BaseOrTier.Tier5 And _
                                u.BaseOrTier <> BaseOrTier.Tier6) Then

                                finalUsages.Insert(0, u)

                            End If
                        Next

                    Else
                        complexityIsTooHigh = True
                    End If


                End If

            End If

        End If

        Return (finalUsages)
    End Function

    ''' <summary>
    ''' Apply Adjustment factor to readings.  Readings are changed by this function.
    ''' </summary>
    ''' <param name="readings"></param>
    ''' <param name="factorType"></param>
    ''' <param name="percentAsShifted"></param>
    ''' <param name="isRTP"></param>
    ''' <param name="isCPP"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function ApplyReadingAdjustmentFactorToReadings(ByVal readings As ReadingCollection, ByVal factorType As ReadingAdjustmentFactorType, ByVal factor As Double, ByVal percentAsShifted As Double, ByVal isRTP As Boolean, ByVal isCPP As Boolean) As Boolean
        Dim adjustedFactor As Double = 0.0
        Dim total As Double = 0.0

        If (factor <= 0.0 Or factor > 1.0) Then Return (True)


        Select Case factorType
            Case ReadingAdjustmentFactorType.GeneralConservation
                ' Reduce all readings by the General factor percentage.
                adjustedFactor = 1.0 - factor
                readings.ApplyScaleFactorToPositiveReadings(adjustedFactor)

            Case ReadingAdjustmentFactorType.CriticalResponse
                ' Shift from critical peak event; some shifted, some discarded
                If (isCPP) Then
                    DoCriticalPeak(readings, factor, percentAsShifted)
                End If

            Case ReadingAdjustmentFactorType.TypicalResponse
                ' Reduce by total factor from OnPeak and disperse percentAsShifted to OffPeak; 
                adjustedFactor = 1.0 - factor
                total = readings.ApplyScaleFactorToPositiveReadingsAndReturnTotal(TimeOfUse.OnPeak, adjustedFactor)     'reduce OnPeak
                total = total * percentAsShifted                                                                        'how much of total to keep and disperse
                readings.Spread(TimeOfUse.OffPeak, total)                                                               'spread evenly to OffPeak

            Case ReadingAdjustmentFactorType.DemandControl
                ' Reduces any energy values that may have been adjusted for demand by the Demand Control factor percentage
                ' Find the demand, and reduce it.  Pin all other readings at that max.
                adjustedFactor = 1.0 - factor
                readings.ApplyScaleFactorToMaxPositiveReadingsAndPin(adjustedFactor)

        End Select

        Return (True)
    End Function

    Private Function ApplyReadingAdjustmentFactorToReadings(ByVal readings As ReadingCollection, ByVal factorType As ReadingAdjustmentFactorType, ByVal factor As Double, ByVal percentAsShifted As Double) As Boolean
        Return (ApplyReadingAdjustmentFactorToReadings(readings, factorType, factor, percentAsShifted, False, False))
    End Function

    ''' <summary>
    ''' Critical peak shed and shift work.
    ''' </summary>
    Private Function DoCriticalPeak(ByVal objWorkingR As ReadingCollection, ByVal factor As Double, ByVal pctShifted As Double) As Boolean
        Dim criticalPeakDatesList As New List(Of String)
        Const shiftNumberCriticalPeak As Integer = 8

        Try
            For i As Integer = 0 To objWorkingR.Count - 1
                If objWorkingR(i).TimeOfUse = TimeOfUse.CriticalPeak Then
                    criticalPeakDatesList.Add(objWorkingR(i).Datestamp.ToString())
                End If
            Next

            ' first reduce by critical factor; of that, some is shifted, some is discarded completely
            If Not (criticalPeakDatesList.Count = 0) Then
                DoCriticalShedAndShift(objWorkingR, shiftNumberCriticalPeak, criticalPeakDatesList.ToArray(), factor, pctShifted)
            End If

        Catch ex As Exception
            ' error with Critical Peak 
        End Try

        Return (True)
    End Function

    Private Function DoCriticalShedAndShift(ByVal objWorkingR As ReadingCollection, ByVal nShift As Integer, ByVal criticalPeakDates() As String, ByVal factor As Double, ByVal percentToShift As Double) As Boolean
        Dim dateAsStringOfCriticalPeak As String
        Dim dblQuantity As Double
        Dim dblQuantityToRedistribute As Double
        Dim i As Integer

        Try

            For Each dateAsStringOfCriticalPeak In criticalPeakDates

                For i = 0 To objWorkingR.Count - 1

                    If (objWorkingR(i).Datestamp.ToString() = dateAsStringOfCriticalPeak) Then

                        ' Only applies to positive reading quantities
                        If (objWorkingR(i).Quantity > 0.0) Then

                            ' First take out total factor;  this is actually the total of shift and eliminate
                            dblQuantity = (objWorkingR(i).Quantity * factor)
                            objWorkingR(i).Quantity = objWorkingR(i).Quantity - dblQuantity

                            ' Calculate quantity to *redistribute* into reading at -index or +index of shift; 
                            ' ex:  if percentToShift = 1.0, then all redistributed, if 0.0 then none redistributed

                            dblQuantityToRedistribute = dblQuantity * percentToShift

                            ' try and shift to intervals before event; if cannot, shift to intervals after event; (hourly is always interval in this context)
                            If ((i - nShift) >= 0) Then
                                objWorkingR(i - nShift).Quantity = objWorkingR(i - nShift).Quantity + dblQuantityToRedistribute
                            ElseIf ((i + nShift) < objWorkingR.Count) Then
                                objWorkingR(i + nShift).Quantity = objWorkingR(i + nShift).Quantity + dblQuantityToRedistribute
                            End If

                        End If

                    End If

                Next

            Next

        Catch
            'noop
        End Try

        Return (True)
    End Function


#End Region



#Region "GetUnitOfMeasure Overloads"

    Public Function GetUnitOfMeasure(ByVal rateCompanyID As Integer, ByVal rateClass As String) As String
        Dim defs As SimpleRateDefinitionCollection
        Dim uom As String = String.Empty
        Dim dt As DateTime = DateTime.Now()

        defs = GetSimpleRateInformation(rateCompanyID, rateClass, dt, dt.AddDays(1))
        If (Not defs Is Nothing) Then
            If (defs.Count > 0) Then
                uom = GetUnitOfMeasure(defs(0).UnitOfMeasure)
            End If
        End If

        Return (uom)
    End Function

    Public Function GetUnitOfMeasure(ByVal unitOfMeasureID As Integer, ByVal lookupInDatabase As Boolean) As String
        Dim uom As String
        Dim objDC As dctlRateEngine

        If (lookupInDatabase) Then
            objDC = New dctlRateEngine(m_sRateEngineConnectionString, m_xmlFilesPath, m_dataSourceType, m_cachingType, m_cacheTimeoutInMinutes)
            Try
                uom = objDC.GetUnitOfMeasure(unitOfMeasureID)
            Catch ex As Exception
                uom = String.Empty
            End Try
        Else
            uom = GetUnitOfMeasure(unitOfMeasureID)
        End If

        Return (uom)
    End Function

    Public Function GetUnitOfMeasure(ByVal unitOfMeasureID As Integer) As String
        Dim uom As String
        Const uom_kwh As String = "kWH"
        Const uom_kva As String = "KVA"
        Const uom_therm As String = "Therm"
        Const uom_dktherm As String = "DKTherm"
        Const uom_ccf As String = "CCF"
        Const uom_mcf As String = "MCF"
        Const uom_m3 As String = "M3"
        Const uom_gal As String = "Gal"
        Const uom_cgal As String = "CGAL"
        Const uom_cf As String = "CF"
        Const uom_kgal As String = "KGAL"
        Const uom_hgal As String = "hgal"
        Const uom_kvah As String = "kVAH"
        Const uom_kvar As String = "kVAR"
        Const uom_kvarh As String = "kVARh"
        Const uom_hcf As String = "hcf"
        Const uom_lb As String = "lb"
        Const uom_klb As String = "klb"
        Const uom_usgl As String = "US gl"
        Const uom_cubicmeter As String = "cubic meter"
        Const uom_cubicfoot As String = "cubic foot"

        Select Case unitOfMeasureID
            Case 0
                uom = uom_kwh
            Case 1
                uom = uom_kva
            Case 2
                uom = uom_therm
            Case 3, 13
                uom = uom_dktherm
            Case 4, 7, 24
                uom = uom_ccf
            Case 5
                uom = uom_mcf
            Case 6
                uom = uom_m3
            Case 8
                uom = uom_gal
            Case 9
                uom = uom_cgal
            Case 10
                uom = uom_cf
            Case 11
                uom = uom_kgal
            Case 12
                uom = uom_hgal
            Case 15
                uom = uom_kvah
            Case 16
                uom = uom_kvar
            Case 17
                uom = uom_kvarh
            Case 18
                uom = uom_hcf
            Case 19
                uom = uom_lb
            Case 20
                uom = uom_klb
            Case 21
                uom = uom_usgl
            Case 22
                uom = uom_cubicmeter
            Case 23
                uom = uom_cubicfoot
            Case Else
                uom = String.Empty
        End Select

        Return (uom)
    End Function

#End Region

#Region "GetHolidayList Overloads"

    Private Function GetHolidayList(ByVal holidayGroupID As Integer) As HolidayCollection
        Dim holidays As HolidayCollection = Nothing
        Dim objDC As dctlRateEngine

        objDC = New dctlRateEngine(m_sRateEngineConnectionString, m_xmlFilesPath, m_dataSourceType, m_cachingType, m_cacheTimeoutInMinutes)

        Try
            holidays = objDC.GetHolidayList(holidayGroupID)

        Catch ex As Exception
            ' return empty collection if there was an error
            holidays = Nothing
            holidays = New HolidayCollection()
        End Try

        Return (holidays)
    End Function

    Private Function GetHolidayList(ByVal holidayGroupID As Integer, ByVal startDate As DateTime, ByVal endDate As DateTime) As HolidayCollection
        Dim holidays As HolidayCollection = Nothing
        Dim objDC As dctlRateEngine

        objDC = New dctlRateEngine(m_sRateEngineConnectionString, m_xmlFilesPath, m_dataSourceType, m_cachingType, m_cacheTimeoutInMinutes)

        Try
            holidays = objDC.GetHolidayList(holidayGroupID, startDate, endDate)

        Catch ex As Exception
            ' return empty collection if there was an error
            holidays = Nothing
            holidays = New HolidayCollection()
        End Try

        Return (holidays)
    End Function

#End Region



#Region "GetRateCompanySettings"

    Private Function GetRateCompanySettings(ByVal rateCompanyID As Integer) As RateCompanySettings
        Dim settings As RateCompanySettings = Nothing
        Dim objDC As dctlRateEngine

        objDC = New dctlRateEngine(m_sRateEngineConnectionString, m_xmlFilesPath, m_dataSourceType, m_cachingType, m_cacheTimeoutInMinutes)

        Try
            settings = objDC.GetRateCompanySettings(rateCompanyID)
        Catch
            Throw
        End Try

        Return (settings)
    End Function

#End Region

#Region "GetNetMeteringTotalResult and overload - For Public Use"

    ''' <summary>
    ''' Get a net metering total result public function.  This is used by some callers that actually have monthly results and need a total.  
    ''' This is likely called through the RateModel interface from RC WS.  Any caller can use this directly as well for unit tests perhaps.
    ''' </summary>
    ''' <param name="rateCompanyID"></param>
    ''' <param name="monthlyResults"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function GetNetMeteringTotalResult(ByVal rateCompanyID As Integer, ByVal monthlyResults As List(Of NetMeteringMonthlyResult)) As NetMeteringTotalResult
        Dim mgr As NetMeteringManager = Nothing
        Dim settings As RateCompanySettings = Nothing
        Dim netMeteringResult As NetMeteringTotalResult = Nothing

        Try
            If (Not monthlyResults Is Nothing AndAlso monthlyResults.Count > 0) Then

                Dim netType As NetMeteringType = monthlyResults(0).NetMeteringType

                If (netType = NetMeteringType.Nem3 Or netType = NetMeteringType.Nem4 Or netType = NetMeteringType.Nem5) Then

                    settings = GetRateCompanySettings(rateCompanyID)
                    mgr = New NetMeteringManager(rateCompanyID, netType, settings)

                    Dim monthlyColl As NetMeteringMonthlyResultCollection = New NetMeteringMonthlyResultCollection()
                    For Each mr As NetMeteringMonthlyResult In monthlyResults
                        monthlyColl.Add(mr)
                    Next

                    netMeteringResult = mgr.CreateNetMeteringTotalResult(monthlyColl)

                End If

            End If
        Catch
            ' do not fail the result because of faulty net metering cases
        End Try

        Return (netMeteringResult)
    End Function

    ''' <summary>
    '''  Get a net metering total result public function overload.  This is used by some callers that actually have a totalled monthly result and need a final total.  
    ''' This is likely called through the RateModel interface from RC WS.  Any caller can use this directly as well for unit tests perhaps.
    ''' </summary>
    ''' <param name="rateCompanyID"></param>
    ''' <param name="sumMonthlyResult"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function GetNetMeteringTotalResult(ByVal rateCompanyID As Integer, ByVal sumMonthlyResult As NetMeteringMonthlyResult) As NetMeteringTotalResult
        Dim mgr As NetMeteringManager = Nothing
        Dim settings As RateCompanySettings = Nothing
        Dim netMeteringResult As NetMeteringTotalResult = Nothing

        Try
            If (Not sumMonthlyResult Is Nothing) Then

                Dim netType As NetMeteringType = sumMonthlyResult.NetMeteringType

                If (netType = NetMeteringType.Nem3 Or netType = NetMeteringType.Nem4 Or netType = NetMeteringType.Nem5) Then

                    settings = GetRateCompanySettings(rateCompanyID)
                    mgr = New NetMeteringManager(rateCompanyID, netType, settings)

                    netMeteringResult = mgr.CreateNetMeteringTotalResult(sumMonthlyResult)

                End If

            End If
        Catch
            ' do not fail the result because of faulty net metering cases
        End Try

        Return (netMeteringResult)
    End Function

#End Region



#Region "Peak-Time-Rebate (PTR) Public and Private helper Functions"

    ''' <summary>
    ''' Figures out if Real-Time-Pricing and 'Peak Time Rebate Only' is defined in a range of definitions determined by rate class and date range
    ''' </summary>
    ''' <param name="rateCompanyID"></param>
    ''' <param name="rateClass"></param>
    ''' <param name="startDate"></param>
    ''' <param name="endDate"></param>
    ''' <returns>boolean</returns>
    ''' <remarks></remarks>
    Public Function DoesPTRExist(ByVal rateCompanyID As Integer, ByVal rateClass As String, ByVal startDate As DateTime, ByVal endDate As DateTime) As Boolean
        Dim retVal As Boolean = False
        Dim defs As SimpleRateDefinitionCollection
        Dim def As SimpleRateDefinition

        defs = GetSimpleRateInformation(rateCompanyID, rateClass, startDate, endDate)

        If (Not defs Is Nothing) Then
            If (defs.Count > 0) Then

                For Each def In defs
                    If (def.IsRTPRate And def.RTPStream = RTPStream.PeakTimeRebateOnly) Then
                        retVal = True
                        Exit For
                    End If
                Next

            End If
        End If

        Return (retVal)
    End Function

    Public Function DoesPTRExist(ByVal defs As DetailedRateDefinitionCollection, ByVal startDate As DateTime, ByVal endDate As DateTime) As Boolean
        Dim retVal As Boolean = False
        Dim def As DetailedRateDefinition

        If (Not defs Is Nothing) Then
            If (defs.Count > 0) Then

                For Each def In defs
                    If (def.IsRTPRate And def.RTPStream = RTPStream.PeakTimeRebateOnly) Then
                        retVal = True
                        Exit For
                    End If
                Next

            End If
        End If

        Return (retVal)
    End Function

    Public Function DoesPTRExist(ByVal defs As SimpleRateDefinitionCollection, ByVal startDate As DateTime, ByVal endDate As DateTime) As Boolean
        Dim retVal As Boolean = False
        Dim def As SimpleRateDefinition

        If (Not defs Is Nothing) Then
            If (defs.Count > 0) Then

                For Each def In defs
                    If (def.IsRTPRate And def.RTPStream = RTPStream.PeakTimeRebateOnly) Then
                        retVal = True
                        Exit For
                    End If
                Next

            End If
        End If

        Return (retVal)
    End Function

    Public Sub DoesPTR_OR_CPPExist(ByVal rateCompanyID As Integer, ByVal rateClass As String, ByVal startDate As DateTime, ByVal endDate As DateTime, ByRef bIsPTRRate As Boolean, ByRef bIsCPPRate As Boolean)
        Dim defs As SimpleRateDefinitionCollection
        Dim def As SimpleRateDefinition

        defs = GetSimpleRateInformation(rateCompanyID, rateClass, startDate, endDate)

        If (Not defs Is Nothing) Then
            If (defs.Count > 0) Then

                For Each def In defs
                    If (def.IsRTPRate And def.RTPStream = RTPStream.PeakTimeRebateOnly) Then
                        bIsPTRRate = True
                        Exit For
                    End If
                    If (def.IsRTPRate And def.RTPStream = RTPStream.CriticalPeakOnly) Then
                        bIsCPPRate = True
                        Exit For
                    End If
                Next

            End If
        End If
    End Sub

    Public Sub DoesPTR_OR_CPPExist(ByVal defs As DetailedRateDefinitionCollection, ByVal startDate As DateTime, ByVal endDate As DateTime, ByRef bIsPTRRate As Boolean, ByRef bIsCPPRate As Boolean)
        Dim def As DetailedRateDefinition

        If (Not defs Is Nothing) Then
            If (defs.Count > 0) Then

                For Each def In defs
                    If (def.IsRTPRate And def.RTPStream = RTPStream.PeakTimeRebateOnly) Then
                        bIsPTRRate = True
                        Exit For
                    End If
                    If (def.IsRTPRate And def.RTPStream = RTPStream.CriticalPeakOnly) Then
                        bIsCPPRate = True
                        Exit For
                    End If
                Next

            End If
        End If
    End Sub

    Public Sub DoesPTR_OR_CPPExist(ByVal defs As SimpleRateDefinitionCollection, ByVal startDate As DateTime, ByVal endDate As DateTime, ByRef bIsPTRRate As Boolean, ByRef bIsCPPRate As Boolean)
        Dim def As SimpleRateDefinition

        If (Not defs Is Nothing) Then
            If (defs.Count > 0) Then

                For Each def In defs
                    If (def.IsRTPRate And def.RTPStream = RTPStream.PeakTimeRebateOnly) Then
                        bIsPTRRate = True
                        Exit For
                    End If
                    If (def.IsRTPRate And def.RTPStream = RTPStream.CriticalPeakOnly) Then
                        bIsCPPRate = True
                        Exit For
                    End If
                Next

            End If
        End If
    End Sub

    ''' <summary>
    ''' Get collection of Peak Time Rebate events from the real time pricing infrustructure
    ''' </summary>
    ''' <param name="rateCompanyID"></param>
    ''' <param name="rateClass"></param>
    ''' <param name="startDate"></param>
    ''' <param name="endDate"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function GetPTREvents(ByVal rateCompanyID As Integer, ByVal rateClass As String, ByVal startDate As DateTime, ByVal endDate As DateTime) As PTREventCollection
        Dim PTREvents As PTREventCollection
        Dim defs As DetailedRateDefinitionCollection

        defs = GetDetailedRateInformation(rateCompanyID, rateClass, startDate, endDate)
        PTREvents = GetPTREvents(defs, startDate, endDate)

        Return (PTREvents)
    End Function

    Public Function GetPTREvents(ByVal defs As DetailedRateDefinitionCollection, ByVal startDate As DateTime, ByVal endDate As DateTime) As PTREventCollection
        Return (GetPTREvents(defs, startDate, endDate, Nothing, Nothing))
    End Function

    Public Function GetPTREvents(ByVal defs As DetailedRateDefinitionCollection, ByVal startDate As DateTime, ByVal endDate As DateTime, ByVal pei As PeakEventInfo, ByVal readings As ReadingCollection) As PTREventCollection
        Dim PTREvents As PTREventCollection
        Dim PTREvent As PTREvent
        Dim rtpGroupID As Integer
        Dim rtpGroupExists As Boolean = False
        Dim rtpValid As Boolean = False
        Dim prices As PriceCollection
        Dim usedGroupIDs As ArrayList
        Dim startEvent As DateTime
        Dim endEvent As DateTime
        Dim newEventStarted As Boolean = False
        Dim reformattedStartDate As DateTime
        Dim reformattedEndDate As DateTime
        Dim useInlinePeakEventInfo As Boolean = False

        PTREvents = New PTREventCollection()

        ' Callers might be sending in the time component of the DateTime;  this is not compatible with the GetPriceData
        ' call later in this function;  pin the dates at midnight within this function;
        reformattedStartDate = New DateTime(startDate.Year, startDate.Month, startDate.Day)
        reformattedEndDate = New DateTime(endDate.Year, endDate.Month, endDate.Day)

        ' use inline peak events?
        If (Not pei Is Nothing AndAlso _
            pei.UseInlinePeakEvents = True AndAlso _
            Not pei.PeakEvents Is Nothing AndAlso _
            Not readings Is Nothing) _
        Then
            useInlinePeakEventInfo = True
        End If

        ' This is the REGULAR handling of peak event retrieval
        If (Not useInlinePeakEventInfo) Then

            If (Not defs Is Nothing) Then
                If (defs.Count > 0) Then

                    usedGroupIDs = New ArrayList()

                    ' Check all definitions for a Real Time Pricing charge
                    ' When RTP exists, grab events from the price data, only if the group has not been previously processed
                    For Each def As DetailedRateDefinition In defs

                        rtpGroupExists = False
                        rtpValid = False
                        For Each charge As UseCharge In def.UseCharges
                            If (charge.RealTimePricing) Then
                                rtpGroupID = charge.RTPGroup
                                rtpGroupExists = True
                                Exit For
                            End If
                        Next

                        If (rtpGroupExists) Then
                            If (Not usedGroupIDs.Contains(rtpGroupID)) Then

                                usedGroupIDs.Add(rtpGroupID)

                                prices = GetPriceData(def.RateCompanyID, rtpGroupID, def.RateClass, reformattedStartDate, reformattedEndDate, True)
                                If (Not prices Is Nothing AndAlso prices.Count > 0) Then rtpValid = True Else rtpValid = False

                                If (rtpValid) Then

                                    newEventStarted = False
                                    For Each p As Price In prices
                                        ' Note that CriticalPeakOnly here is correct.  The RTPActualStream is indeed still CriticalPeakOnly; PeakTimeRebate is a sub-category of CPP;
                                        If (p.PriceStream = RTPActualStream.CriticalPeakOnly) Then
                                            If (Not newEventStarted) Then
                                                newEventStarted = True
                                                startEvent = p.DateStamp
                                            End If
                                        Else
                                            If (newEventStarted) Then
                                                endEvent = p.DateStamp.AddMinutes(-1) 'PTR-NOTE: Event actually ended in the last hour; last whole hour is inclusive; this is the first non-event hour here.
                                                newEventStarted = False
                                                PTREvent = New PTREvent(startEvent, endEvent, def.BaselineRule)
                                                PTREvents.Add(PTREvent)
                                            End If
                                        End If
                                    Next

                                End If


                            End If
                        End If

                    Next

                End If

            End If

        Else

            ' INLINE PEAK EVENTS
            If (Not pei Is Nothing AndAlso _
                pei.PeakEvents.Count > 0) Then
                If (Not defs Is Nothing AndAlso defs.Count > 0 AndAlso Not readings Is Nothing) Then

                    For Each pe As PeakEvent In pei.PeakEvents

                        If (pe.Datestamp >= reformattedStartDate AndAlso pe.Datestamp <= reformattedEndDate) Then

                            ' get a reading in which the event resides, so we can determine the season, since inline events are seasonal.
                            Dim indexOfReading As Integer = readings.FindIndexOfReadingWithMonthAndDay(pe.Datestamp.Month, pe.Datestamp.Day)

                            If (indexOfReading <> -1) Then
                                Dim r As Reading = readings(indexOfReading)
                                Dim s As Integer = CInt(r.Season)
                                ' if we do not have all seasons represented here, bring index down.
                                If (s > pe.Durations.Count - 1) Then
                                    s = pe.Durations.Count - 1
                                End If

                                Dim startStamp As DateTime = New DateTime(pe.Datestamp.Year, pe.Datestamp.Month, pe.Datestamp.Day, pe.Durations(s).StartHour, 0, 0)
                                Dim endStamp As DateTime = New DateTime(pe.Datestamp.Year, pe.Datestamp.Month, pe.Datestamp.Day, pe.Durations(s).EndHour, 0, 0)

                                PTREvent = New PTREvent(startStamp, endStamp, defs(0).BaselineRule)
                                PTREvents.Add(PTREvent)

                            End If


                        End If

                    Next

                End If
            End If

            End If


            Return (PTREvents)
    End Function


    Private Function FinalizePTREvents(ByVal ptrEvents As PTREventCollection, ByVal readings As ReadingCollection, ByVal baselines As BaselineCollection, ByVal calculateBaselineAsNeeded As Boolean, ByVal rdp As Integer, ByVal holidayGroupID As Integer) As PTREventCollection

        If (Not ptrEvents Is Nothing) Then
            For Each ptrEvent As PTREvent In ptrEvents
                Dim b As Baseline

                b = baselines.FindByDate(ptrEvent.StartDate)

                If (b Is Nothing Or (Not b Is Nothing AndAlso b.BaselineMissing)) Then
                    b = New Baseline()

                    If (Not readings Is Nothing AndAlso readings.Count > 0) Then
                        b.Units = 0 'default at this time
                    End If

                    b.UsageDate = ptrEvent.StartDate
                    b.ActualUsageMissing = True
                    b.BaselineMissing = True
                    b.RebateMissing = True

                    ptrEvent.Rebate.BaselineUsageEmpty = True

                    If (calculateBaselineAsNeeded) Then
                        b.BaselineUsage = ComputeBaseline(ptrEvents, readings, ptrEvent, b.BaselineMissing, rdp, holidayGroupID)    'rounding occurs inside this function when computed
                        ptrEvent.Rebate.BaselineUsage = b.BaselineUsage
                        ptrEvent.Rebate.BaselineCalculated = True

                        If (Not b.BaselineMissing) Then
                            ptrEvent.Rebate.BaselineUsageEmpty = False
                        End If

                    End If

                Else

                    If (Not b.ActualUsageMissing) Then
                        ptrEvent.Rebate.ActualUsageProvided = True
                        ptrEvent.Rebate.ActualUsage = Math.Round(b.ActualUsage, rdp)
                    End If

                    If (Not b.RebateMissing) Then
                        ptrEvent.Rebate.RebateAmountProvided = True
                        ptrEvent.Rebate.RebateAmount = b.RebateAmount
                    End If

                    If (Not b.BaselineMissing) Then
                        ptrEvent.Rebate.BaselineUsage = Math.Round(b.BaselineUsage, rdp)
                    Else
                        ptrEvent.Rebate.BaselineUsageEmpty = True
                    End If

                End If

            Next

        End If

        Return (ptrEvents)
    End Function

    Private Function ComputeBaseline(ByVal objPTREventCollection As PTREventCollection, ByVal readings As ReadingCollection, ByVal ptrEvent As PTREvent, ByRef bMissingBaseline As Boolean, ByVal rdp As Integer, ByVal holidayGroupID As Integer) As Double
        Dim actualUsageOverBaselineDay As Double() = Nothing
        Dim baselineQuantity As Double
        Dim qualifiedDay As Boolean
        Dim totalUsage As Double
        Dim count, nGood, nSampleSizeGood, nMax As Integer
        Dim nGoodCount As Integer = 0
        Dim isEventWeekendDay As Boolean = False
        Dim nDayRange As Integer
        Dim dtStartDate, dtEndDate As Date
        Dim objBaselineDay As ReadingCollection

        dtStartDate = readings.GetFirstDate()
        dtEndDate = readings.GetLastDate()

        If (ptrEvent.StartDate.Date < dtStartDate.Date Or ptrEvent.StartDate.Date > dtEndDate.Date) Then
            bMissingBaseline = True
            baselineQuantity = 0
        Else

            Dim eventDay As ReadingCollection = readings.GetReadingsForDate(ptrEvent.StartDate)
            Dim isEventHol As Boolean = eventDay(0).Holiday

            Select Case ptrEvent.BaselineRule
                Case BaselineRule.ThreeOfTen    ' change this to be "highest 3 of 5 then avg" for weekdays, and "highest 1 of 3" for weekend/holiday.
                    If (Not EventDayOfWeekIsWeekend(ptrEvent.StartDate, isEventHol)) Then
                        'weekday
                        nGood = 3
                        nSampleSizeGood = 5
                        nMax = 30
                    Else
                        'weekend
                        isEventWeekendDay = True
                        nGood = 1
                        nSampleSizeGood = 3
                        nMax = 30
                    End If
                Case BaselineRule.FiveOfThirty  ' really average of five
                    nGood = 5
                    nSampleSizeGood = 5
                    nMax = 5
                Case Else
                    nGood = 5
                    nSampleSizeGood = 5
                    nMax = 5
            End Select

            'ReDim actualUsageOverBaselineDay(nMax)
            nDayRange = ptrEvent.StartDate.Subtract(dtStartDate).Days

            For i As Integer = 1 To nDayRange
                If ((count < nMax) AndAlso (nGoodCount < nSampleSizeGood)) Then
                    objBaselineDay = readings.GetReadingsForDate(ptrEvent.StartDate.AddDays(-1 * i))
                    qualifiedDay = IsQualifiedBaselineDay(objPTREventCollection, objBaselineDay, ptrEvent.StartDate, isEventHol, holidayGroupID)

                    If (qualifiedDay) Then
                        totalUsage = readings.SumWithinDate(objBaselineDay(0).Datestamp, CType(ptrEvent.StartDate, DateTime), CType(ptrEvent.EndDate, DateTime))
                        totalUsage = Math.Round(totalUsage, rdp)
                    End If

                    Select Case ptrEvent.BaselineRule
                        Case BaselineRule.ThreeOfTen    ' really best three of five averaged
                            If (qualifiedDay) Then
                                ReDim Preserve actualUsageOverBaselineDay(nGoodCount)
                                actualUsageOverBaselineDay(nGoodCount) = totalUsage
                                nGoodCount = nGoodCount + 1
                                'Else
                                '    actualUsageOverBaselineDay(count) = 0
                            End If
                            count = count + 1
                        Case BaselineRule.FiveOfThirty, BaselineRule.Unspecified ' really average of five
                            If (qualifiedDay) Then
                                actualUsageOverBaselineDay(count) = totalUsage
                                nGoodCount = nGoodCount + 1
                                count = count + 1
                            End If
                    End Select
                Else
                    Exit For
                End If
            Next

            ' If there are not nSampleSizeGood good days going backward, look forward
            If (nGoodCount < nSampleSizeGood) Then
                nDayRange = dtEndDate.Subtract(ptrEvent.StartDate).Days
                For i As Integer = 1 To nDayRange
                    If ((count < nMax) AndAlso (nGoodCount < nSampleSizeGood)) Then
                        objBaselineDay = readings.GetReadingsForDate(ptrEvent.StartDate.AddDays(i))
                        qualifiedDay = IsQualifiedBaselineDay(objPTREventCollection, objBaselineDay, ptrEvent.StartDate, isEventHol, holidayGroupID)

                        If (qualifiedDay) Then
                            totalUsage = readings.SumWithinDate(objBaselineDay(0).Datestamp, CType(ptrEvent.StartDate, DateTime), CType(ptrEvent.EndDate, DateTime))
                            totalUsage = Math.Round(totalUsage, rdp)
                        End If

                        Select Case ptrEvent.BaselineRule
                            Case BaselineRule.ThreeOfTen    ' really best three of five
                                If (qualifiedDay) Then
                                    ReDim Preserve actualUsageOverBaselineDay(nGoodCount)
                                    actualUsageOverBaselineDay(nGoodCount) = totalUsage
                                    nGoodCount = nGoodCount + 1
                                    'Else
                                    '    actualUsageOverBaselineDay(count) = 0.0
                                End If
                                count = count + 1
                            Case BaselineRule.FiveOfThirty, BaselineRule.Unspecified  ' really average of five
                                If (qualifiedDay) Then
                                    actualUsageOverBaselineDay(count) = totalUsage
                                    nGoodCount = nGoodCount + 1
                                    count = count + 1
                                End If
                        End Select

                    Else
                        Exit For
                    End If
                Next
            End If

            If (nGoodCount >= nGood) Then
                Dim objSorter As IComparer = New QuantitySorter()
                Array.Sort(actualUsageOverBaselineDay, objSorter)
                If ptrEvent.BaselineRule = BaselineRule.ThreeOfTen And
                    isEventWeekendDay = True Then
                    baselineQuantity = ComputeMaxActualUsage(actualUsageOverBaselineDay)
                Else
                    baselineQuantity = ComputeAverageActualUsage(actualUsageOverBaselineDay, ptrEvent.BaselineRule, isEventWeekendDay)
                End If
                baselineQuantity = Math.Round(baselineQuantity, rdp)
                bMissingBaseline = False
            Else
                baselineQuantity = 0.0
                bMissingBaseline = True
            End If

        End If

        Return (baselineQuantity)
    End Function

    Private Function EventDayOfWeekIsWeekend(ByVal eventDate As Date, ByVal isHoliday As Boolean) As Boolean
        Dim ret As Boolean = False

        If (Not isHoliday) Then
            Select Case eventDate.DayOfWeek
                Case DayOfWeek.Saturday, DayOfWeek.Sunday
                    ret = True
                Case Else
                    ret = False
            End Select
        Else
            ' an event on a holiday shall be treated as a weekend
            ret = True
        End If

        Return (ret)
    End Function

    Private Function IsQualifiedBaselineDay(ByVal ptrEvents As PTREventCollection, ByVal baselineDayReadings As ReadingCollection, ByVal EventDate As Date, ByVal eventIsHoliday As Boolean, ByVal holidayGroupID As Integer) As Boolean
        Dim ret As Boolean
        Dim isOtherPTREventDay As Boolean

        ' check if the baseline day is the same type of week day of event date
        If ((EventDayOfWeekIsWeekend(EventDate, eventIsHoliday) AndAlso IsWeekend(baselineDayReadings(0).Datestamp, holidayGroupID)) Or _
             (Not EventDayOfWeekIsWeekend(EventDate, eventIsHoliday) AndAlso Not IsWeekend(baselineDayReadings(0).Datestamp, holidayGroupID))) Then

            ' Loop through PTR Event collection, to find if the baseline day is another event date
            ' once it's found exit for loop
            For Each objPTREvent As PTREvent In ptrEvents
                If baselineDayReadings(0).Datestamp.Date = objPTREvent.StartDate.Date Then
                    isOtherPTREventDay = True
                    Exit For
                End If
            Next

            If (Not isOtherPTREventDay) Then

                If (Not baselineDayReadings(0).Holiday) Then
                    'check if there's any missing read in the baseline day.
                    If (Not baselineDayReadings.EmptyReadingsExist()) Then
                        ret = True
                    Else
                        ret = False
                    End If
                Else
                    ret = False
                End If
            Else
                ret = False
            End If

        Else
            ret = False
        End If

        Return (ret)
    End Function

    Private Function IsWeekend(ByVal d As Date, ByVal holidayGroupID As Integer) As Boolean
        Dim ret As Boolean = False
        Dim holidays As HolidayCollection
        Dim isHoliday As Boolean

        Select Case d.DayOfWeek
            Case DayOfWeek.Saturday, DayOfWeek.Sunday
                ret = True
            Case Else
                holidays = GetHolidayList(holidayGroupID)
                isHoliday = False
                isHoliday = holidays.IsHoliday(d)
                If isHoliday = True Then
                    ret = True
                End If
        End Select

        Return (ret)
    End Function

    Private Function ComputeAverageActualUsage(ByVal arrUsage As Double(), ByVal baselineRule As BaselineRule, ByVal isWeekend As Boolean) As Double
        Dim c As Integer
        Dim t As Double
        Dim avgUsage As Double

        Select Case baselineRule
            Case baselineRule.ThreeOfTen    ' really best three of five averaged
                If (Not isWeekend) Then
                    ' weekday
                    c = 3
                Else
                    ' weekend
                    c = 1
                End If

            Case baselineRule.FiveOfThirty  ' really average of five
                c = 5
            Case Else
                c = 5
        End Select

        For i As Integer = 0 To c - 1
            t = t + arrUsage(i)
        Next

        avgUsage = t / CDbl(c)

        Return (avgUsage)
    End Function

    Private Function ComputeMaxActualUsage(ByVal arrUsage As Double()) As Double
        Dim maxUsage As Double

        maxUsage = arrUsage(0)

        Return (maxUsage)

    End Function

    Public Function CalculatePTREventRebates(ByVal rateCompanyID As Integer, ByVal rateClass As String, ByVal startDate As DateTime, ByVal endDate As DateTime, ByVal readings As ReadingCollection, ByVal PTREvents As PTREventCollection) As PTRResult
        Return (CalculatePTREventRebates(rateCompanyID, rateClass, startDate, endDate, readings, PTREvents, True))
    End Function

    Public Function CalculatePTREventRebates(ByVal rateCompanyID As Integer, ByVal rateClass As String, ByVal startDate As DateTime, ByVal endDate As DateTime, ByVal readings As ReadingCollection, ByVal PTREvents As PTREventCollection, ByVal allowRebateCalculations As Boolean) As PTRResult
        Dim result As PTRResult = Nothing
        Dim defs As DetailedRateDefinitionCollection

        If (Not readings Is Nothing AndAlso readings.Count > 0) Then
            If (Not PTREvents Is Nothing AndAlso PTREvents.Count > 0) Then

                defs = GetDetailedRateInformation(rateCompanyID, rateClass, startDate, endDate)
                result = CalculatePTREventRebates(defs, rateCompanyID, rateClass, startDate, endDate, readings, PTREvents, allowRebateCalculations, 3)

            End If
        End If

        Return (result)
    End Function

    Public Function CalculatePTREventRebates(ByVal defs As DetailedRateDefinitionCollection, ByVal rateCompanyID As Integer, ByVal rateClass As String, ByVal startDate As DateTime, ByVal endDate As DateTime, ByVal readings As ReadingCollection, ByVal PTREvents As PTREventCollection) As PTRResult
        Return (CalculatePTREventRebates(defs, rateCompanyID, rateClass, startDate, endDate, readings, PTREvents, True, 3))
    End Function

    Public Function CalculatePTREventRebates(ByVal defs As DetailedRateDefinitionCollection, ByVal rateCompanytID As Integer, ByVal rateClass As String, ByVal startDate As DateTime, ByVal endDate As DateTime, ByVal readings As ReadingCollection, ByVal PTREvents As PTREventCollection, ByVal allowRebateCalculations As Boolean, ByVal rdp As Integer) As PTRResult
        Dim result As PTRResult = Nothing
        Dim objRateClassModifiers As RateClassModifierCollection
        Dim eRebateClass As RebateClass

        If (Not defs Is Nothing AndAlso Not readings Is Nothing AndAlso readings.Count > 0) Then
            If (Not PTREvents Is Nothing AndAlso PTREvents.Count > 0) Then

                objRateClassModifiers = CreateRateClassModifierCollection(rateClass, rateCompanytID)

                Select Case objRateClassModifiers.Find(RateClassModifierType.DemandResponseRebateClass).Value.ToLower
                    Case "class1"
                        eRebateClass = RebateClass.Class1
                    Case "class2"
                        eRebateClass = RebateClass.Class2
                    Case "class3"
                        eRebateClass = RebateClass.Class3
                    Case "class4"
                        eRebateClass = RebateClass.Class4
                    Case "class5"
                        eRebateClass = RebateClass.Class5
                    Case Else
                        eRebateClass = RebateClass.Undefined
                End Select

                result = CalculatePTREventRebates(defs, startDate, endDate, readings, PTREvents, eRebateClass, allowRebateCalculations, rdp)

            End If
        End If

        Return (result)
    End Function

    Public Function CalculatePTREventRebates(ByVal defs As DetailedRateDefinitionCollection, ByVal startDate As DateTime, ByVal endDate As DateTime, ByVal readings As ReadingCollection, ByVal PTREvents As PTREventCollection, ByVal objDemandRebateClass As RebateClass) As PTRResult
        Return (CalculatePTREventRebates(defs, startDate, endDate, readings, PTREvents, objDemandRebateClass, True, 3))
    End Function

    Public Function CalculatePTREventRebates(ByVal defs As DetailedRateDefinitionCollection, ByVal startDate As DateTime, ByVal endDate As DateTime, ByVal readings As ReadingCollection, ByVal PTREvents As PTREventCollection, ByVal objDemandRebateClass As RebateClass, ByVal allowRebateCalculations As Boolean, ByVal rdp As Integer) As PTRResult
        Dim result As New PTRResult()
        Dim def As DetailedRateDefinition
        Dim newestEvent As PTREvent

        If (Not defs Is Nothing AndAlso defs.Count > 0) Then
            If (Not readings Is Nothing AndAlso readings.Count > 0) Then
                If (Not PTREvents Is Nothing AndAlso PTREvents.Count > 0) Then

                    For Each e As PTREvent In PTREvents

                        ' This instantiates to false, set to true here, 
                        ' and only set again to false at bottom of this function if criteria indicate
                        e.Rebate.Valid = True

                        def = defs.GetDefinition(e.StartDate)   ' events are in the same day, no need to use e.EndDate as well

                        If (Not def Is Nothing) Then

                            ' From authored service charge for PTR
                            ' Get the "Rebate Accounting Rule" 
                            ' and "rebate unit price, a.k.a rebate charge/credit"
                            ' and "cost type"
                            For Each charge As ServiceCharge In def.ServiceCharges
                                If (charge.CalculationType = ChargeCalcType.PTRRebateEachEvent Or charge.CalculationType = ChargeCalcType.PTRRebateEntireBillPeriod) Then

                                    ' CR 20298, 20299, 20302 Oct 2011 - get the rebate info based on the rebate class passed
                                    ' Refers to rebate class defined in Rate Engine.
                                    If objDemandRebateClass = charge.RebateClass Then
                                        Select Case charge.CalculationType
                                            Case ChargeCalcType.PTRRebateEachEvent
                                                e.Rebate.AccountingRule = RebateAccountingRule.EachEvent

                                            Case ChargeCalcType.PTRRebateEntireBillPeriod
                                                e.Rebate.AccountingRule = RebateAccountingRule.EntireBillPeriod

                                            Case Else
                                                'default to EachEvent;
                                                e.Rebate.AccountingRule = RebateAccountingRule.EachEvent
                                        End Select

                                        e.Rebate.UnitPrice = charge.ChargeValue
                                        e.Rebate.CostType = charge.CostType

                                        Exit For
                                    End If
                                End If
                            Next

                            ' if actual usage was not provided, then calculate, but only if we are told we can indeed calculate
                            If ((Not e.Rebate.ActualUsageProvided) AndAlso allowRebateCalculations) Then

                                e.Rebate.ActualUsageCalculated = True
                                e.Rebate.ActualUsage = Math.Round(readings.Sum(e.StartDate, e.EndDate), rdp)
                                e.Rebate.MissingActualUsage = readings.EmptyReadingsExist(e.StartDate, e.EndDate)

                            End If

                            ' Set this to false prior to logic that may change this
                            e.Rebate.RebateAmountCalculated = False

                            ' make sure the baseline is provided, and actual usage is good
                            If ((Not e.Rebate.BaselineUsageEmpty And (e.Rebate.ActualUsageProvided Or (Not e.Rebate.MissingActualUsage))) AndAlso (allowRebateCalculations)) Then

                                e.Rebate.SavingsUsage = Math.Round(e.Rebate.BaselineUsage - e.Rebate.ActualUsage, rdp)
                                e.Rebate.RealSavingsUsage = e.Rebate.SavingsUsage               ' this is not pinned, for use later
                                If (e.Rebate.SavingsUsage <= 0) Then e.Rebate.SavingsUsage = 0 ' this is pinned to zero if negative

                                ' if rebate amount not provided, calculate rebate information
                                If (Not e.Rebate.RebateAmountProvided) Then
                                    e.Rebate.RebateAmount = e.Rebate.UnitPrice * e.Rebate.SavingsUsage
                                    e.Rebate.RebateAmountCalculated = True
                                End If

                            End If

                            ' Check results and set overall validity boolean to false if applicable
                            If (e.Rebate.MissingActualUsage) Then e.Rebate.Valid = False
                            If (e.Rebate.BaselineUsageEmpty And (Not e.Rebate.RebateAmountProvided)) Then e.Rebate.Valid = False

                        End If

                    Next

                    ' Populate PTRResult object and do extra calculation if needed
                    newestEvent = PTREvents(PTREvents.Count - 1)
                    result.OverallAccountingRule = newestEvent.Rebate.AccountingRule
                    result.PTREvents = PTREvents

                    ' if accounting rule was EachEvent, then the collection of events has everything that is needed for caller; HOWEVER, method exists to get RebateAmount total from collection
                    ' if accounting rule was EntireBillPeriod, then calculate the overallRebateAmount and store it at top level object for caller
                    If (result.OverallAccountingRule = RebateAccountingRule.EntireBillPeriod) Then
                        If (newestEvent.Rebate.RebateAmountProvided) Then
                            result.RebateAmount = newestEvent.Rebate.RebateAmount
                        Else
                            'calculate the entire bill period rebate amount; if not a rebate (greater than zero) then pin this to zero
                            result.RebateAmount = result.PTREvents.SumRebateRealSavingsUsage() * newestEvent.Rebate.UnitPrice
                            If (result.RebateAmount > 0) Then result.RebateAmount = 0
                        End If
                    End If

                End If
            End If
        End If

        Return (result)
    End Function


#End Region

#Region "Critical-Peak-Pricing (CPP) Public Functions and GetAnyPeakEvents"

    ''' <summary>
    ''' Figures out if Real-Time-Pricing and 'CriticalPeak Only' is defined in a range of definitions - CR 11321 Dec 2011
    ''' </summary>
    ''' <param name="rateCompanyID"></param>
    ''' <param name="rateClass"></param>
    ''' <param name="startDate"></param>
    ''' <param name="endDate"></param>
    ''' <returns>boolean</returns>
    ''' <remarks></remarks>
    Public Function DoesCPPExist(ByVal rateCompanyID As Integer, ByVal rateClass As String, ByVal startDate As DateTime, ByVal endDate As DateTime) As Boolean
        Dim retVal As Boolean = False
        Dim defs As SimpleRateDefinitionCollection
        Dim def As SimpleRateDefinition

        defs = GetSimpleRateInformation(rateCompanyID, rateClass, startDate, endDate)

        If (Not defs Is Nothing) Then
            If (defs.Count > 0) Then

                For Each def In defs
                    If (def.IsRTPRate And def.RTPStream = RTPStream.CriticalPeakOnly) Then
                        retVal = True
                        Exit For
                    End If
                Next

            End If
        End If

        Return (retVal)
    End Function

    ''' <summary>
    ''' Get collection of CPP events from the real time pricing infrustructure - CR 11321 Dec 2011
    ''' </summary>
    ''' <param name="rateCompanyID"></param>
    ''' <param name="rateClass"></param>
    ''' <param name="startDate"></param>
    ''' <param name="endDate"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function GetCPPEvents(ByVal rateCompanyID As Integer, ByVal rateClass As String, ByVal startDate As DateTime, ByVal endDate As DateTime) As CPPEventCollection
        Dim CPPEvents As CPPEventCollection
        Dim defs As DetailedRateDefinitionCollection

        defs = GetDetailedRateInformation(rateCompanyID, rateClass, startDate, endDate)
        CPPEvents = GetCPPEvents(defs, startDate, endDate)

        Return (CPPEvents)
    End Function

    Public Function GetCPPEvents(ByVal detailedDefs As DetailedRateDefinitionCollection, ByVal startDate As DateTime, ByVal endDate As DateTime) As CPPEventCollection
        Dim CPPEvents As CPPEventCollection
        Dim CPPEvent As CPPEvent
        Dim rtpGroupID As Integer
        Dim rtpGroupExists As Boolean = False
        Dim rtpValid As Boolean = False
        Dim prices As PriceCollection
        Dim usedGroupIDs As ArrayList
        Dim startEvent As DateTime
        Dim endEvent As DateTime
        Dim newEventStarted As Boolean = False
        Dim reformattedStartDate As DateTime
        Dim reformattedEndDate As DateTime
        Dim isCapResEligible As Boolean = False

        CPPEvents = New CPPEventCollection()

        ' Callers might be sending in the time component of the DateTime;  this is not compatible with the GetPriceData
        ' call later in this function;  pin the dates at midnight within this function;
        reformattedStartDate = New DateTime(startDate.Year, startDate.Month, startDate.Day)
        reformattedEndDate = New DateTime(endDate.Year, endDate.Month, endDate.Day)

        If (Not detailedDefs Is Nothing) Then
            If (detailedDefs.Count > 0) Then

                isCapResEligible = detailedDefs(0).IsCapacityReserve

                usedGroupIDs = New ArrayList()

                ' Check all definitions for a Real Time Pricing charge
                ' When RTP exists, grab events from the price data, only if the group has not been previously processed
                For Each def As DetailedRateDefinition In detailedDefs

                    rtpGroupExists = False
                    rtpValid = False
                    For Each charge As UseCharge In def.UseCharges
                        If (charge.RealTimePricing) Then
                            rtpGroupID = charge.RTPGroup
                            rtpGroupExists = True
                            Exit For
                        End If
                    Next

                    If (rtpGroupExists) Then
                        If (Not usedGroupIDs.Contains(rtpGroupID)) Then

                            usedGroupIDs.Add(rtpGroupID)

                            prices = GetPriceData(def.RateCompanyID, rtpGroupID, def.RateClass, reformattedStartDate, reformattedEndDate, True)
                            If (Not prices Is Nothing AndAlso prices.Count > 0) Then rtpValid = True Else rtpValid = False

                            If (rtpValid) Then

                                newEventStarted = False

                                For Each p As Price In prices
                                    If (p.PriceStream = RTPActualStream.CriticalPeakOnly) Then

                                        If (Not newEventStarted) Then
                                            newEventStarted = True
                                            startEvent = p.DateStamp
                                        End If

                                    Else

                                        If (newEventStarted) Then
                                            endEvent = p.DateStamp          'CPP-NOTE: Event actually ended in the last second of last hour; may need a .AddHours(-1) in future
                                            newEventStarted = False
                                            CPPEvent = New CPPEvent(startEvent, endEvent)
                                            CPPEvent.CapacityReserveEligible = isCapResEligible
                                            CPPEvents.Add(CPPEvent)
                                        End If

                                    End If
                                Next

                            End If


                        End If
                    End If


                Next

            End If
        End If

        Return (CPPEvents)
    End Function

    ''' <summary>
    ''' Get any peak events over the given date range for the rtpGroupId.  Used by advanced callers.
    ''' </summary>
    ''' <param name="RTPGroupID"></param>
    ''' <param name="startDate"></param>
    ''' <param name="endDate"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function GetAnyPeakEvents(ByVal RTPGroupID As Integer, ByVal startDate As DateTime, ByVal endDate As DateTime) As CPPEventCollection
        Dim objDC As New dctlRateEngine(m_sRateEngineConnectionString, m_xmlFilesPath, m_dataSourceType, m_cachingType, m_cacheTimeoutInMinutes)
        Dim events As CPPEventCollection = Nothing

        events = objDC.GetAnyPeakEvents(RTPGroupID, startDate, endDate)

        Return (events)
    End Function

    ''' <summary>
    ''' Get collection of prioritized simulated events for months.
    ''' </summary>
    ''' <param name="rateCompanyId"></param>
    ''' <param name="numberOfEvents"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function GetRTPSimulatedEvents(ByVal rateCompanyId As Integer, ByVal numberOfEvents As Integer) As SimEventCollection
        Dim objDC As New dctlRateEngine(m_sRateEngineConnectionString, m_xmlFilesPath, m_dataSourceType, m_cachingType, m_cacheTimeoutInMinutes)
        Dim events As SimEventCollection = Nothing

        events = objDC.GetRTPSimulatedEvents(rateCompanyId, numberOfEvents)

        Return (events)
    End Function

    ''' <summary>
    ''' Get list of durations for an event feed.  For simulation.
    ''' </summary>
    ''' <param name="RTPGroupID"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function GetRTPSimulatedEventDuration(ByVal RTPGroupID As Integer) As SimDurationCollection
        Dim objDC As New dctlRateEngine(m_sRateEngineConnectionString, m_xmlFilesPath, m_dataSourceType, m_cachingType, m_cacheTimeoutInMinutes)
        Dim durations As SimDurationCollection = Nothing

        durations = objDC.GetRTPSimulatedEventDuration(RTPGroupID)

        Return (durations)
    End Function

#End Region

#Region "Misc Functions for Public Use"

    Public Function GetPercentTier1Baseline(ByVal nRateCompanyID As Integer, ByVal nRateDefID As Integer, ByVal sRateClass As String, ByVal eSeason As Season) As Double
        Dim sClass As String = Nothing
        Dim sCode As String = Nothing
        Dim sTerritory As String = Nothing
        Dim objDC As dctlRateEngine
        Dim dblBaseline As Double = 0
        Dim bParseOkay As Boolean = ParseRateClass(nRateCompanyID, sRateClass, sClass, sCode, sTerritory)

        If (bParseOkay) Then
            ' Parsed okay, now lookup Baseline value
            objDC = New dctlRateEngine(m_sRateEngineConnectionString, m_xmlFilesPath, m_dataSourceType, m_cachingType, m_cacheTimeoutInMinutes)
            Try
                dblBaseline = objDC.GetBaseline(nRateDefID, sCode, sTerritory, eSeason)
            Catch
                dblBaseline = 0
            End Try
        End If

        Return (dblBaseline)
    End Function

    ''' <summary>
    ''' This method returns TOU boundary array for every day of the week i.e. Sunday, Monday, Tuesday, Wednesday, Thursday, Friday, and Saturday
    ''' even if the rate class is defined only for weekday, weekend or all.
    ''' </summary>
    ''' <param name="objDetails"></param>
    ''' <param name="eSeason"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function GetTOUBoundaryArray(ByVal objDetails As DetailedRateDefinitionCollection, ByVal eSeason As Season) As TimeOfUse(,)

        Dim eRETOU(,) As TimeOfUse = Nothing '(eDayOfWeek, 15 min interval number)

        If (Not objDetails Is Nothing) Then

            If (objDetails.Count > 0) Then

                eRETOU = GetTOUDayOfWeekBoundaryArray(objDetails.Item(0), eSeason)

            End If

        End If

        Return (eRETOU)
    End Function

    ''' <summary>
    ''' This method takes two TOU Boundary Arrays of size (6,95) and compares them.
    ''' </summary>
    ''' <param name="touArray"></param>
    ''' <param name="touArray2"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function DoTOUBoundaryArraysMatch(ByVal touArray As TimeOfUse(,), ByVal touArray2 As TimeOfUse(,)) As Boolean
        Dim result As Boolean

        If (touArray.Length = touArray2.Length) Then
            result = True
            For day As Integer = DayOfWeek.Sunday To DayOfWeek.Saturday
                For index As Integer = 0 To 95
                    If (touArray(day, index) <> touArray2(day, index)) Then
                        result = False
                        Exit For
                    End If
                Next
                If (Not result) Then
                    Exit For
                End If
            Next
        End If

        Return (result)
    End Function

    ''' <summary>
    ''' There are callers that need BaseRateClass sometimes.  Geoegraphy based rate classes can be stripped using this by outside callers.
    ''' </summary>
    ''' <param name="nRateCompanyID"></param>
    ''' <param name="sRateClass"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function GetBaseRateClass(ByVal nRateCompanyID As Integer, ByVal sRateClass As String) As String
        Dim sBaseRateClass As String
        Dim nBreakIndex As Integer

        ' CR 20298, 20299, 20302 Oct 2011 - modified to support "#" rate class with modifiers
        If RateClassModifierCollection.DoesRateClassContainModifiers(sRateClass) Then
            sBaseRateClass = RateClassModifierCollection.GetModifierValueFromRateClassModifierString(sRateClass, RateClassModifierCollection.ksnRC)
        Else
            nBreakIndex = sRateClass.IndexOf(GetRateClassBreakCharacter(nRateCompanyID))

            If (nBreakIndex >= 0) Then
                sBaseRateClass = sRateClass.Substring(0, nBreakIndex)
            Else
                sBaseRateClass = sRateClass
            End If
        End If

        Return (sBaseRateClass)
    End Function

    ''' <summary>
    ''' Get the fuel of the specified rate class.  Straight call to data controller helper function.
    ''' </summary>
    ''' <param name="sRateClass"></param>
    ''' <param name="nRateCompanyID"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function GetFuelType(ByVal sRateClass As String, ByVal nRateCompanyID As Integer) As Integer
        Dim objDC As New dctlRateEngine(m_sRateEngineConnectionString, m_xmlFilesPath, m_dataSourceType, m_cachingType, m_cacheTimeoutInMinutes)
        Dim nFuelType As Integer

        nFuelType = objDC.GetFuelType(GetBaseRateClass(nRateCompanyID, sRateClass), nRateCompanyID)

        Return nFuelType
    End Function


#End Region



#Region "Tier Boundaries Retrieval - and Tier Boundary Information - for Public use"

    ''' <summary>
    ''' Gets information on tier boundaries.
    ''' </summary>
    ''' <param name="rateCompanyID"></param>
    ''' <param name="rateClass"></param>
    ''' <param name="startDate"></param>
    ''' <param name="endDate"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function GetCalculatedTierBoundaryInformation(ByVal rateCompanyID As Integer, ByVal rateClass As String, ByVal startDate As DateTime, ByVal endDate As DateTime) As TierBoundaryInformation
        Dim definitions As DetailedRateDefinitionCollection
        definitions = GetDetailedRateInformation(rateCompanyID, rateClass, startDate, endDate)
        Return (GetCalculatedTierBoundaryInformation(definitions, rateClass, startDate, endDate))
    End Function

    ''' <summary>
    ''' Gets information on tier boundaries.
    ''' </summary>
    ''' <param name="rateCompanyID"></param>
    ''' <param name="rateClass"></param>
    ''' <param name="startDate"></param>
    ''' <param name="endDate"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function GetAllCalculatedTierBoundaryInformation(ByVal rateCompanyID As Integer, ByVal rateClass As String, ByVal startDate As DateTime, ByVal endDate As DateTime, projectedEndDate As Date, useProjectedEndDateToFinalizeTierBoundaries As Boolean, Optional ByVal definition As DetailedRateDefinition = Nothing) As TierBoundaryInformation
        Dim definitions As DetailedRateDefinitionCollection = New DetailedRateDefinitionCollection()
        If Not definition Is Nothing Then
            definitions.Add(definition)
        Else
            definitions = GetDetailedRateInformation(rateCompanyID, rateClass, startDate, endDate)
        End If

        Return (GetAllCalculatedTierBoundaryInformation(definitions, rateClass, startDate, endDate, projectedEndDate, useProjectedEndDateToFinalizeTierBoundaries))
    End Function


    ''' <summary>
    ''' Gets information on tier boundaries.
    ''' </summary>
    ''' <param name="definitions"></param>
    ''' <param name="rateClass"></param>
    ''' <param name="startDate"></param>
    ''' <param name="endDate"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function GetCalculatedTierBoundaryInformation(ByVal definitions As DetailedRateDefinitionCollection, ByVal rateClass As String, ByVal startDate As DateTime, ByVal endDate As DateTime) As TierBoundaryInformation
        Dim tierBoundaryInfo As TierBoundaryInformation = New TierBoundaryInformation()
        Dim workingTierBoundaryCollList As TierBoundaryCollectionList = New TierBoundaryCollectionList()
        Dim finalTierBoundaries As TierBoundaryCollection = Nothing
        Dim tierBoundaries As TierBoundaryCollection = Nothing
        Dim definition As DetailedRateDefinition
        Dim days As Integer
        Dim valid As Boolean = True
        Dim seasonFactors As SeasonFactorCollection
        Dim useSeasonOverride As Boolean
        Dim eOverrideSeason As Season

        If (Not definitions Is Nothing) Then

            If (definitions.Count > 0) Then

                definition = definitions(0)

                If (definition.IsTiered) Then

                    If (definition.IsSeasonal) Then
                        'seasonal
                        useSeasonOverride = ProcessSeasonalProrate(definitions, startDate, endDate, eOverrideSeason)

                        seasonFactors = DetermineSeasonFactors(definition, startDate, endDate, useSeasonOverride, eOverrideSeason)
                        tierBoundaryInfo.SeasonFactors = seasonFactors

                        For Each dictEntrySeason As DictionaryEntry In seasonFactors
                            Dim sf As SeasonFactor = CType(dictEntrySeason.Value, SeasonFactor)

                            tierBoundaries = definition.TierBoundaries.GetFirstAvailableForSeason(sf.Season)

                            If (Not tierBoundaries Is Nothing AndAlso tierBoundaries.Count > 0) Then
                                workingTierBoundaryCollList.Add(tierBoundaries)
                            End If
                        Next

                    Else
                        ' not seasonal
                        seasonFactors = DetermineSeasonFactors(startDate, endDate)    'season is undefined, but still set this property
                        tierBoundaryInfo.SeasonFactors = seasonFactors

                        tierBoundaries = definition.TierBoundaries.GetFirstAvailable()

                        If (Not tierBoundaries Is Nothing AndAlso tierBoundaries.Count > 0) Then
                            workingTierBoundaryCollList.Add(tierBoundaries)
                        End If

                    End If

                    If (valid) Then
                        days = endDate.Subtract(startDate).Days + 1

                        If (Not workingTierBoundaryCollList Is Nothing AndAlso workingTierBoundaryCollList.Count > 0) Then

                            For Each tbc As TierBoundaryCollection In workingTierBoundaryCollList

                                ' Each seasonal scaleFactor is applied within the FinalizeTierBoundaries.  This allows the crossing of a season to be prorated properly
                                Dim seasonScaleFactor As Double = tierBoundaryInfo.GetSeasonFactorValue(tbc(0).Season)

                                finalTierBoundaries = FinalizeTierBoundaries(definition.RateCompanyID, definition.DefinitionID, definition.MasterID, tbc, definition.TierStructureType, rateClass, tbc(0).Season, tbc(0).TimeOfUse, 1, days, seasonScaleFactor)

                                tierBoundaryInfo.TierBoundaryCollectionsList.Add(finalTierBoundaries)
                            Next

                        End If

                    End If

                End If

            End If

        End If

        Return (tierBoundaryInfo)
    End Function

    ''' <summary>
    ''' Gets information on tier boundaries.
    ''' </summary>
    ''' <param name="definitions"></param>
    ''' <param name="rateClass"></param>
    ''' <param name="startDate"></param>
    ''' <param name="endDate"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function GetAllCalculatedTierBoundaryInformation(ByVal definitions As DetailedRateDefinitionCollection, ByVal rateClass As String, ByVal startDate As DateTime, ByVal endDate As DateTime, projectedEndDate As DateTime, useProjectedEndDateWhenFinalizingTierBoundaries As Boolean) As TierBoundaryInformation
        Dim tierBoundaryInfo As TierBoundaryInformation = New TierBoundaryInformation()
        Dim workingTierBoundaryCollList As TierBoundaryCollectionList = New TierBoundaryCollectionList()
        Dim finalTierBoundaries As TierBoundaryCollection = Nothing
        Dim tierBoundaries As TierBoundaryCollection = Nothing
        Dim definition As DetailedRateDefinition
        Dim days As Integer
        Dim valid As Boolean = True
        Dim seasonFactors As SeasonFactorCollection
        Dim useSeasonOverride As Boolean
        Dim eOverrideSeason As Season

        If (Not definitions Is Nothing) Then

            If (definitions.Count > 0) Then

                definition = definitions(0)

                If (definition.IsTiered) Then

                    If (definition.IsSeasonal) Then
                        'seasonal
                        useSeasonOverride = False
                        seasonFactors = DetermineSeasonFactors(definition, startDate, endDate, useSeasonOverride, eOverrideSeason)
                        tierBoundaryInfo.SeasonFactors = seasonFactors

                        For Each dictEntrySeason As DictionaryEntry In seasonFactors
                            Dim sf As SeasonFactor = CType(dictEntrySeason.Value, SeasonFactor)

                            tierBoundaries = definition.TierBoundaries.GetFirstAvailableForSeason(sf.Season)

                            If (Not tierBoundaries Is Nothing AndAlso tierBoundaries.Count > 0) Then
                                workingTierBoundaryCollList.Add(tierBoundaries)
                            End If
                        Next

                    Else
                        ' not seasonal
                        seasonFactors = DetermineSeasonFactors(startDate, endDate)    'season is undefined, but still set this property
                        tierBoundaryInfo.SeasonFactors = seasonFactors

                        tierBoundaries = definition.TierBoundaries.GetFirstAvailable()

                        If (Not tierBoundaries Is Nothing AndAlso tierBoundaries.Count > 0) Then
                            workingTierBoundaryCollList.Add(tierBoundaries)
                        End If

                    End If

                    If (valid) Then
                        ' 15.03 Bug 56979 - Calculate tiers based on projected usage at the end of the bill cycle, 
                        ' or based on actual usage to date calculated from available AMI intervals. 
                        ' (Otherwise referred to as �progressive� versus �cash-me-out�)
                        If useProjectedEndDateWhenFinalizingTierBoundaries AndAlso projectedEndDate <> DateTime.MinValue Then
                            days = projectedEndDate.Subtract(startDate).Days + 1
                        Else
                        days = endDate.Subtract(startDate).Days + 1
                        End If

                        If (Not workingTierBoundaryCollList Is Nothing AndAlso workingTierBoundaryCollList.Count > 0) Then

                            For Each tbc As TierBoundaryCollection In workingTierBoundaryCollList

                                ' Each seasonal scaleFactor is applied within the FinalizeTierBoundaries.  This allows the crossing of a season to be prorated properly
                                Dim seasonScaleFactor As Double = tierBoundaryInfo.GetSeasonFactorValue(tbc(0).Season)

                                finalTierBoundaries = FinalizeTierBoundaries(definition.RateCompanyID, definition.DefinitionID, definition.MasterID, tbc, definition.TierStructureType, rateClass, tbc(0).Season, tbc(0).TimeOfUse, 1, days, seasonScaleFactor)

                                tierBoundaryInfo.TierBoundaryCollectionsList.Add(finalTierBoundaries)
                            Next

                        End If

                    End If

                End If

            End If

        End If

        Return (tierBoundaryInfo)
    End Function


    ''' <summary>
    ''' Get a set of tier boundaries for a given rate class and date range.
    ''' This will always return the first set when there are multiple sets and/or multiple rate definitions.
    ''' This will calculate the final boundaries when the tier structure requires such a calculation.
    ''' </summary>
    ''' <param name="rateCompanyID"></param>
    ''' <param name="rateClass"></param>
    ''' <param name="startDate"></param>
    ''' <param name="endDate"></param>
    ''' <returns>TierBoundaryCollection</returns>
    ''' <remarks></remarks>
    Public Function GetCalculatedTierBoundaries(ByVal rateCompanyID As Integer, ByVal rateClass As String, ByVal startDate As DateTime, ByVal endDate As DateTime) As TierBoundaryCollection
        Dim definitions As DetailedRateDefinitionCollection
        definitions = GetDetailedRateInformation(rateCompanyID, rateClass, startDate, endDate)
        Return (GetCalculatedTierBoundaries(definitions, rateClass, startDate, endDate))
    End Function

    ''' <summary>
    ''' Get a set of tier boundaries for a given definition and the rate class and date range.
    ''' This will always return the first set of boundaries when there are multiple sets.
    ''' This will calculate the final boundaries when the tier structure requires such a calculation. 
    ''' </summary>
    ''' <param name="definition"></param>
    ''' <param name="rateClass"></param>
    ''' <param name="startDate"></param>
    ''' <param name="endDate"></param>
    ''' <returns>TierBoundaryCollection</returns>
    ''' <remarks></remarks>
    Public Function GetCalculatedTierBoundaries(ByVal definition As DetailedRateDefinition, ByVal rateClass As String, ByVal startDate As DateTime, ByVal endDate As DateTime) As TierBoundaryCollection
        Dim finalTierBoundaries As TierBoundaryCollection = Nothing
        Dim definitions As DetailedRateDefinitionCollection

        If (Not definition Is Nothing) Then
            definitions = New DetailedRateDefinitionCollection()
            definitions.Add(definition)
            finalTierBoundaries = GetCalculatedTierBoundaries(definitions, rateClass, startDate, endDate)
        End If

        Return (finalTierBoundaries)
    End Function

    ''' <summary>
    ''' Get a set of tier boundaries for the given definitions and the rate class and date range. 
    ''' This will always return the first set when there are multiple sets and/or multiple rate definitions.
    ''' This will calculate the final boundaries when the tier structure requires such a calculation.
    ''' An empty collection will be returned when the rate class is not tiered.
    ''' The endDate is being used to get the season when applicable.  This could instead be startDate, or a midDate 
    ''' calculated instead; however, that  advanced functionality.  This simply uses endDate currently.
    ''' </summary>
    ''' <param name="definitions"></param>
    ''' <param name="rateClass"></param>
    ''' <param name="startDate"></param>
    ''' <param name="endDate"></param>
    ''' <returns>TierBoundaryCollection</returns>
    ''' <remarks></remarks>
    Public Function GetCalculatedTierBoundaries(ByVal definitions As DetailedRateDefinitionCollection, ByVal rateClass As String, ByVal startDate As DateTime, ByVal endDate As DateTime) As TierBoundaryCollection
        Dim finalTierBoundaries As TierBoundaryCollection = Nothing
        Dim tierBoundaries As TierBoundaryCollection = Nothing
        Dim definition As DetailedRateDefinition
        Dim billDays As Integer

        If (Not definitions Is Nothing) Then
            If (definitions.Count > 0) Then
                definition = definitions(0)

                If (definition.IsTiered) Then
                    If (definition.IsSeasonal) Then
                        If (Not definition.SeasonBoundaries Is Nothing) Then
                            Dim sb As SeasonBoundary = definition.SeasonBoundaries.FindInclusiveBoundary(endDate)
                            If (Not sb Is Nothing) Then
                                tierBoundaries = definition.TierBoundaries.GetFirstAvailableForSeason(sb.Season)
                            Else
                                tierBoundaries = definition.TierBoundaries.GetFirstAvailable()
                            End If
                        End If
                    Else
                        tierBoundaries = definition.TierBoundaries.GetFirstAvailable()
                    End If
                    billDays = endDate.Subtract(startDate).Days + 1
                    finalTierBoundaries = FinalizeTierBoundaries(definition.RateCompanyID, definition.DefinitionID, definition.MasterID, tierBoundaries, definition.TierStructureType, rateClass, tierBoundaries(0).Season, tierBoundaries(0).TimeOfUse, 1, billDays)
                Else
                    ' return a collection with no boundaries when we are not dealing with a tiered rate class
                    finalTierBoundaries = New TierBoundaryCollection()
                End If

            End If
        End If

        Return (finalTierBoundaries)
    End Function

    ''' <summary>
    ''' Get a set of tier boundaries given a set of very specific parameters.
    ''' This is meant to be used by advanced callers.  Rarely used.
    ''' </summary>
    ''' <param name="nRateCompanyID"></param>
    ''' <param name="nRateDefID"></param>
    ''' <param name="nRateMasterID"></param>
    ''' <param name="objTierBoundaries"></param>
    ''' <param name="eTierStructureType"></param>
    ''' <param name="sRateClass"></param>
    ''' <param name="eSeason"></param>
    ''' <param name="eTimeOfUse"></param>
    ''' <param name="nDwellingUnits"></param>
    ''' <param name="nBillDays"></param>
    ''' <param name="objUsageColl"></param>
    ''' <returns>TierBoundaryCollection</returns>
    ''' <remarks></remarks>
    Public Function GetCalculatedTierBoundaries(ByVal nRateCompanyID As Integer, ByVal nRateDefID As Integer, ByVal nRateMasterID As Integer, ByVal objTierBoundaries As TierBoundaryCollection, ByVal eTierStructureType As TierStructureType, ByVal sRateClass As String, ByVal eSeason As Season, ByVal eTimeOfUse As TimeOfUse, ByVal nDwellingUnits As Integer, ByVal nBillDays As Integer, ByVal objUsageColl As UsageCollection) As TierBoundaryCollection
        Return (FinalizeTierBoundaries(nRateCompanyID, nRateDefID, nRateMasterID, objTierBoundaries, eTierStructureType, sRateClass, eSeason, eTimeOfUse, nDwellingUnits, nBillDays, objUsageColl))
    End Function

#End Region

#Region "GetPriceCollection for Public use"

    Public Function GetPriceCollection(ByVal nRateCompanyID As Integer, ByVal sRateClass As String, ByVal dtStartDate As DateTime, ByVal dtEndDate As DateTime, ByVal bFillMissing As Boolean) As PriceCollection
        Dim prices As PriceCollection = Nothing
        Dim objDC As dctlRateEngine

        objDC = New dctlRateEngine(m_sRateEngineConnectionString, m_xmlFilesPath, m_dataSourceType, m_cachingType, m_cacheTimeoutInMinutes)

        Try
            prices = objDC.GetPriceData(nRateCompanyID, 0, sRateClass, dtStartDate, dtEndDate, True)

        Catch ex As Exception
            ' return empty collection if there was an error
            prices = Nothing
            prices = New PriceCollection()
        End Try

        Return (prices)
    End Function

#End Region



#Region "Helper Functions for General Work"

    ''' <summary>
    ''' This indicates that a time-of-use boundaries are complex, but it does NOT indicate that there is a true error.
    ''' This was added for some callers to get the indication of complexity in the result when calling CalculateCost()
    ''' This should have been implemented more clearly.
    ''' </summary>
    ''' <param name="objDetailedDef"></param>
    ''' <param name="objSeasonFactors"></param>
    ''' <param name="eTOUError"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function TOUBoundaryCheck(ByVal objDetailedDef As DetailedRateDefinition, ByVal objSeasonFactors As SeasonFactorCollection, ByVal eTOUError As Enums.ErrCode) As Enums.ErrCode
        Dim tb As TOUBoundary
        Dim de As DictionaryEntry
        Dim sf As SeasonFactor
        Dim seas As Season
        Dim lasttou As TimeOfUse

        ' foreman 10871
        ' check if:
        '       1) this is a tou rate, 
        '       2) we've crossed a season
        '       3) the boundaries consistent flag = true
        '       4) each season has tou
        If (objDetailedDef.IsTimeOfUse) Then
            If (objSeasonFactors.Count > 1) Then
                If (Not objDetailedDef.BoundariesConsistent) Then
                    eTOUError = ErrCode.ErrTOUComplex
                Else
                    lasttou = objDetailedDef.TOUBoundaries(0).TimeOfUse

                    For Each tb In objDetailedDef.TOUBoundaries
                        If (tb.TimeOfUse <> TimeOfUse.Undefined) Then
                            If (eTOUError <> ErrCode.NoError) Then
                                Exit For
                            End If
                            If (lasttou <> tb.TimeOfUse) Then
                                eTOUError = ErrCode.ErrTOUComplex
                                Exit For
                            End If
                            seas = tb.Season
                            For Each de In objSeasonFactors
                                sf = CType(de.Value, SeasonFactor)
                                If (seas <> sf.Season) Then
                                    eTOUError = ErrCode.ErrTOUComplex
                                    Exit For
                                End If
                            Next
                        Else
                            eTOUError = ErrCode.ErrTOUComplex
                            Exit For
                        End If
                        lasttou = tb.TimeOfUse
                    Next
                End If
            End If
        End If

        Return (eTOUError)
    End Function

    'primary use charges work
    Private Function ProcessUseCharges(ByVal rateClassModifiers As RateClassModifierCollection, ByVal objUsageColl As UsageCollection, ByVal objCostColl As CostCollection, ByVal objDef As DetailedRateDefinition, ByVal sRateClassArg As String, ByVal nMeters As Integer, ByVal nDwellingUnits As Integer, ByVal dblTotalUse As Double, ByVal nBillDays As Integer, ByVal dtStartDate As DateTime, ByVal dtEndDate As DateTime, ByVal bFinalDefinition As Boolean, ByVal dblProrateFactor As Double, ByVal bUseSeasonProrateOverride As Boolean, ByVal eOverrideSeason As Season, ByVal objSeasonFactors As SeasonFactorCollection, ByVal bIsDemandResponseEligible As Boolean, ByVal extParams As CalculateCostExtParam) As CostCollection
        Dim objUseCharges As UseChargeCollection

        ' Usages are indeed used here
        ' Main guts of tier, tou, seasonal logic.
        ' Prorate factor passed in.  Used to help determine Seasonal Prorating.  Although, prorating is actually applied in finalize function.
        objUseCharges = objDef.UseCharges
        Log(objUseCharges)

        If (objUseCharges.Count > 0 And objUsageColl.Count > 0) Then
            ' There do EXIST UseCharges to process against Usages

            If (objDef.IsTimeOfUse Or (objDef.IsRTPRate And objDef.RTPStream = RTPStream.CriticalPeakOnly)) Then
                ' TOU
                ' This artificially injects total into Usage Collection - do this when input is parts only
                objUsageColl = SumNonDemandUsagePartsAndInject(objUsageColl)

                If (objDef.IsTiered) Then
                    'TOU and TIERED;

                    If (objUsageColl.ExistsAnyTierWithAnyTOU()) Then

                        ' If even a single tier with tou usage determinant exists, this triggers a 1 to 1 processing of all determinants as provided. 
                        ' Complex rate classes coming from bill-to-date callers where many usage determinants are created should be processed here. (original complex tou + tier)
                        ' There is no reason to try and recompute any tiers or add any special season handling.
                        objCostColl = CalcCostsWithUsagesAndUseCharges(objDef, objUsageColl, objUseCharges, objCostColl, nBillDays, objDef.StartDate, dtStartDate, dtEndDate, bFinalDefinition, dblProrateFactor, False, False, bIsDemandResponseEligible)

                    Else
                        ' Will do tiering from set of usage determinants provided. 
                        If (objDef.IsSeasonal) Then

                            If (objUseCharges.ExistsAnyTierNoTOUSeasonal()) Then
                                ' TOU Charges, TIERED charges, exist seperately; SEASONAL
                                objCostColl = CalcTieredCosts(rateClassModifiers, objUsageColl, objUseCharges, objSeasonFactors, objDef, sRateClassArg, nDwellingUnits, nBillDays, objCostColl, objDef.StartDate, dtStartDate, dtEndDate, bFinalDefinition, dblProrateFactor, False, extParams.UseProjectedEndDateWhenFinalizingTierBoundaries, extParams.ProjectedEndDate)
                            End If

                            If (objUseCharges.ExistsAnyTierTOUSeasonal()) Then
                                ' TOU Charges, TIERED charges, exist together; SEASONAL
                                objCostColl = CalcTieredCosts(rateClassModifiers, objUsageColl, objUseCharges, objSeasonFactors, objDef, sRateClassArg, nDwellingUnits, nBillDays, objCostColl, objDef.StartDate, dtStartDate, dtEndDate, bFinalDefinition, dblProrateFactor, True, extParams.UseProjectedEndDateWhenFinalizingTierBoundaries, extParams.ProjectedEndDate)
                            End If

                            objCostColl = CalcSeasonalCosts(objDef, objUsageColl, objUseCharges, objSeasonFactors, objCostColl, nBillDays, objDef.StartDate, dtStartDate, dtEndDate, bFinalDefinition, dblProrateFactor, bIsDemandResponseEligible)
                            objCostColl = CalcCostsWithUsagesAndUseCharges(objDef, objUsageColl, objUseCharges, objCostColl, nBillDays, objDef.StartDate, dtStartDate, dtEndDate, bFinalDefinition, dblProrateFactor, True, True, bIsDemandResponseEligible)
                        Else

                            If (objUseCharges.ExistsAnyTierNoTOUNonSeasonal()) Then
                                ' TOU Charges, TIERED charges, exist seperately; NON-SEASONAL
                                objCostColl = CalcTieredCosts(rateClassModifiers, objUsageColl, objUseCharges, objSeasonFactors, objDef, sRateClassArg, nDwellingUnits, nBillDays, objCostColl, objDef.StartDate, dtStartDate, dtEndDate, bFinalDefinition, dblProrateFactor, False, extParams.UseProjectedEndDateWhenFinalizingTierBoundaries, extParams.ProjectedEndDate)
                            End If

                            If (objUseCharges.ExistsAnyTierTOUNonSeasonal()) Then
                                ' TOU Charges, TIERED charges, exist together; NON-SEASONAL
                                objCostColl = CalcTieredCosts(rateClassModifiers, objUsageColl, objUseCharges, objSeasonFactors, objDef, sRateClassArg, nDwellingUnits, nBillDays, objCostColl, objDef.StartDate, dtStartDate, dtEndDate, bFinalDefinition, dblProrateFactor, True, extParams.UseProjectedEndDateWhenFinalizingTierBoundaries, extParams.ProjectedEndDate)
                            End If

                            objCostColl = CalcCostsWithUsagesAndUseCharges(objDef, objUsageColl, objUseCharges, objCostColl, nBillDays, objDef.StartDate, dtStartDate, dtEndDate, bFinalDefinition, dblProrateFactor, False, True, bIsDemandResponseEligible)
                        End If
                    End If

                Else
                    'TOU but NOT TIERED
                    If (objDef.IsSeasonal) Then
                        ' TOU Charges; SEASONAL
                        objCostColl = CalcSeasonalCosts(objDef, objUsageColl, objUseCharges, objSeasonFactors, objCostColl, nBillDays, objDef.StartDate, dtStartDate, dtEndDate, bFinalDefinition, dblProrateFactor, bIsDemandResponseEligible)
                        objCostColl = CalcCostsWithUsagesAndUseCharges(objDef, objUsageColl, objUseCharges, objCostColl, nBillDays, objDef.StartDate, dtStartDate, dtEndDate, bFinalDefinition, dblProrateFactor, True, False, bIsDemandResponseEligible)
                    Else
                        ' TOU Charges; NON-SEASONAL
                        objCostColl = CalcCostsWithUsagesAndUseCharges(objDef, objUsageColl, objUseCharges, objCostColl, nBillDays, objDef.StartDate, dtStartDate, dtEndDate, bFinalDefinition, dblProrateFactor, False, False, bIsDemandResponseEligible)
                    End If
                End If

            Else
                ' Not TOU 

                If (objDef.IsTiered) Then
                    'NOT TOU but TIERED

                    ' Need to sum and replace TOU with TotalServiceUse ONLY when parts are not Tiered and happen to be TOU;
                    ' this is for when TOU is passed-in and the rate class is non-tou and tiered
                    objUsageColl = SumNonTieredNonDemandTOUUsagePartsAndReplace(objUsageColl)

                    If (TotalServiceUseExistsWithOptionalDemandOnly(objUsageColl)) Then
                        ' Simple TotalServiceUse present as usage input, need to cut this into tiered usage; it may also be Seasonal
                        objCostColl = CalcTieredCosts(rateClassModifiers, objUsageColl, objUseCharges, objSeasonFactors, objDef, sRateClassArg, nDwellingUnits, nBillDays, objCostColl, objDef.StartDate, dtStartDate, dtEndDate, bFinalDefinition, dblProrateFactor, False, extParams.UseProjectedEndDateWhenFinalizingTierBoundaries, extParams.ProjectedEndDate)

                        'This handles all non-tiered UseCharges not handled in CalcTieredCosts;
                        If (objDef.IsSeasonal) Then
                            objCostColl = CalcSeasonalCosts(objDef, objUsageColl, objUseCharges, objSeasonFactors, objCostColl, nBillDays, objDef.StartDate, dtStartDate, dtEndDate, bFinalDefinition, dblProrateFactor, bIsDemandResponseEligible)
                        Else
                            objCostColl = CalcCostsWithUsagesAndUseCharges(objDef, objUsageColl, objUseCharges, objCostColl, nBillDays, objDef.StartDate, dtStartDate, dtEndDate, bFinalDefinition, dblProrateFactor, False, False, bIsDemandResponseEligible)
                        End If
                    Else
                        ' This artificially injects total into Usage Collection - do this when input is parts only
                        objUsageColl = SumNonDemandUsagePartsAndInject(objUsageColl)

                        If (objUsageColl.ExistsAnyTierWithAnySeason()) Then
                            ' Some special cases of seasonal tiers with no TOU are subject to advanced binning
                            ' Usually coming from bill-to-date callers.  Process assuming all usage determinants are present.
                            ' There is no reason to try and recompute any tiers or add any special season handling.
                            objCostColl = CalcCostsWithUsagesAndUseCharges(objDef, objUsageColl, objUseCharges, objCostColl, nBillDays, objDef.StartDate, dtStartDate, dtEndDate, bFinalDefinition, dblProrateFactor, False, False, bIsDemandResponseEligible)

                        Else
                            If (objDef.IsSeasonal) Then
                                objCostColl = CalcSeasonalCosts(objDef, objUsageColl, objUseCharges, objSeasonFactors, objCostColl, nBillDays, objDef.StartDate, dtStartDate, dtEndDate, bFinalDefinition, dblProrateFactor, bIsDemandResponseEligible)
                            Else
                                ' Each Tier usage must be explicitely present in usages, to 1 to 1 calcs
                                objCostColl = CalcCostsWithUsagesAndUseCharges(objDef, objUsageColl, objUseCharges, objCostColl, nBillDays, objDef.StartDate, dtStartDate, dtEndDate, bFinalDefinition, dblProrateFactor, False, False, bIsDemandResponseEligible)
                            End If
                        End If

                    End If

                Else
                    ' NOT TOU and NOT TIERED

                    objUsageColl = SumNonTieredNonDemandTOUUsagePartsAndReplace(objUsageColl)

                    If (TotalServiceUseExistsWithOptionalDemandOnly(objUsageColl)) Then
                        ' Simple TotalServiceUse present as usage input
                        If (objDef.IsSeasonal) Then
                            ' SEASONAL
                            objCostColl = CalcSeasonalCosts(objDef, objUsageColl, objUseCharges, objSeasonFactors, objCostColl, nBillDays, objDef.StartDate, dtStartDate, dtEndDate, bFinalDefinition, dblProrateFactor, bIsDemandResponseEligible)
                        Else
                            ' Not SEASONAL, simple case of just flat total usage supplied (with perhaps Demand?)
                            objCostColl = CalcCostsWithUsagesAndUseCharges(objDef, objUsageColl, objUseCharges, objCostColl, nBillDays, objDef.StartDate, dtStartDate, dtEndDate, bFinalDefinition, dblProrateFactor, False, True, bIsDemandResponseEligible)
                        End If

                    Else
                        objUsageColl = SumNonDemandUsagePartsAndInject(objUsageColl)
                        ' Could be TotalServiceUse in defined seasons (probably rare)
                        ' or nothing for usage, in which case, nothing is calculated.
                        ' 13.01 bug 36729 - check if rate class is seasonal or not, if so, apply sesanal calc
                        If (objDef.IsSeasonal) Then
                            objCostColl = CalcSeasonalCosts(objDef, objUsageColl, objUseCharges, objSeasonFactors, objCostColl, nBillDays, objDef.StartDate, dtStartDate, dtEndDate, bFinalDefinition, dblProrateFactor, bIsDemandResponseEligible)
                        Else
                            ' Each Tier usage must be explicitely present in usages, to 1 to 1 calcs
                            objCostColl = CalcCostsWithUsagesAndUseCharges(objDef, objUsageColl, objUseCharges, objCostColl, nBillDays, objDef.StartDate, dtStartDate, dtEndDate, bFinalDefinition, dblProrateFactor, False, True, bIsDemandResponseEligible)
                        End If
                    End If

                End If

            End If

        End If

        Return (objCostColl)
    End Function

    ''' <summary>
    ''' Finalize tier boundaries.
    ''' </summary>
    ''' <remarks></remarks>
    Private Function FinalizeTierBoundaries(ByVal nRateCompanyID As Integer, ByVal nRateDefID As Integer, ByVal nRateMasterID As Integer, ByVal objTierBoundaries As TierBoundaryCollection, ByVal eTierStructureType As TierStructureType, ByVal sRateClass As String, ByVal eSeason As Season, ByVal eTimeOfUse As TimeOfUse, ByVal nDwellingUnits As Integer, ByVal nBillDays As Integer) As TierBoundaryCollection
        Return (FinalizeTierBoundaries(nRateCompanyID, nRateDefID, nRateMasterID, objTierBoundaries, eTierStructureType, sRateClass, eSeason, eTimeOfUse, nDwellingUnits, nBillDays, New UsageCollection(), 1.0))
    End Function

    Private Function FinalizeTierBoundaries(ByVal nRateCompanyID As Integer, ByVal nRateDefID As Integer, ByVal nRateMasterID As Integer, ByVal objTierBoundaries As TierBoundaryCollection, ByVal eTierStructureType As TierStructureType, ByVal sRateClass As String, ByVal eSeason As Season, ByVal eTimeOfUse As TimeOfUse, ByVal nDwellingUnits As Integer, ByVal nBillDays As Integer, ByVal seasonScaleFactor As Double) As TierBoundaryCollection
        Return (FinalizeTierBoundaries(nRateCompanyID, nRateDefID, nRateMasterID, objTierBoundaries, eTierStructureType, sRateClass, eSeason, eTimeOfUse, nDwellingUnits, nBillDays, New UsageCollection(), seasonScaleFactor))
    End Function

    Private Function FinalizeTierBoundaries(ByVal nRateCompanyID As Integer, ByVal nRateDefID As Integer, ByVal nRateMasterID As Integer, ByVal objTierBoundaries As TierBoundaryCollection, ByVal eTierStructureType As TierStructureType, ByVal sRateClass As String, ByVal eSeason As Season, ByVal eTimeOfUse As TimeOfUse, ByVal nDwellingUnits As Integer, ByVal nBillDays As Integer, ByVal objUsageColl As UsageCollection) As TierBoundaryCollection
        Return (FinalizeTierBoundaries(nRateCompanyID, nRateDefID, nRateMasterID, objTierBoundaries, eTierStructureType, sRateClass, eSeason, eTimeOfUse, nDwellingUnits, nBillDays, objUsageColl, 1.0))
    End Function

    Private Function FinalizeTierBoundaries(ByVal nRateCompanyID As Integer, ByVal nRateDefID As Integer, ByVal nRateMasterID As Integer, _
                                        ByVal objTierBoundaries As TierBoundaryCollection, ByVal eTierStructureType As TierStructureType, _
                                        ByVal sRateClass As String, ByVal eSeason As Season, ByVal eTimeOfUse As TimeOfUse, _
                                        ByVal nDwellingUnits As Integer, ByVal nBillDays As Integer, ByVal objUsageColl As UsageCollection, _
                                        ByVal seasonScaleFactor As Double) As TierBoundaryCollection
        Return (FinalizeTierBoundaries(Nothing, nRateCompanyID, nRateDefID, nRateMasterID, objTierBoundaries, eTierStructureType, sRateClass, eSeason, eTimeOfUse, nDwellingUnits, nBillDays, objUsageColl, seasonScaleFactor))
    End Function

    Private Function FinalizeTierBoundaries(ByVal rateClassModifiers As RateClassModifierCollection, _
                                            ByVal nRateCompanyID As Integer, ByVal nRateDefID As Integer, ByVal nRateMasterID As Integer, _
                                            ByVal objTierBoundaries As TierBoundaryCollection, ByVal eTierStructureType As TierStructureType, _
                                            ByVal sRateClass As String, ByVal eSeason As Season, ByVal eTimeOfUse As TimeOfUse, _
                                            ByVal nDwellingUnits As Integer, ByVal nBillDays As Integer, ByVal objUsageColl As UsageCollection, _
                                            ByVal seasonScaleFactor As Double) As TierBoundaryCollection

        Dim params As New FinalizeTierBoundaryParameters()

        params.RateCompanyID = nRateCompanyID
        params.RateDefID = nRateDefID
        params.RateMasterID = nRateMasterID
        params.TierBoundaries = objTierBoundaries
        params.TierStructureType = eTierStructureType
        params.RateClass = sRateClass
        params.Season = eSeason
        params.TimeOfUse = eTimeOfUse
        params.DwellingUnits = nDwellingUnits
        params.BillDays = nBillDays
        params.Usages = objUsageColl
        params.SeasonScaleFactor = seasonScaleFactor
        params.RateClassModifiers = rateClassModifiers

        Return (FinalizeTierBoundaries(params))
    End Function


    Private Function FinalizeTierBoundaries(ByVal p As FinalizeTierBoundaryParameters) As TierBoundaryCollection
        Dim objFinalBoundaries As TierBoundaryCollection
        Dim objBoundary As TierBoundary = Nothing
        Dim objNewBoundary As TierBoundary
        Dim dblThreshold, dblBaseX As Double
        Dim sClass As String = Nothing
        Dim sCode As String = Nothing
        Dim sTerritory As String = Nothing
        Dim dblBaseline As Double
        Dim objDC As dctlRateEngine
        Dim objTierBoundarySubset As New TierBoundaryCollection
        Dim factor As Double

        '***** GRAB ONLY THE TIERBOUNDARIES FROM THE CORRECT SEASON, and the specified TOU
        objTierBoundarySubset = p.TierBoundaries.GetForSeasonAndTOU(p.Season, p.TimeOfUse)

        Select Case p.TierStructureType
            Case TierStructureType.MonthlyBounds, TierStructureType.Undefined
                objFinalBoundaries = objTierBoundarySubset

            Case TierStructureType.MonthlyBoundsHybrid1
                ' Adjust boundaries by extension using a base of 30 days, and when days provided is *outside* of range of 27 to 34.  These are constants for Hybrid1.
                If (p.BillDays >= TierStructureHybrid1_Min And p.BillDays <= TierStructureHybrid1_Max) Then
                    objFinalBoundaries = objTierBoundarySubset
                Else
                    objFinalBoundaries = New TierBoundaryCollection

                    factor = (CDbl(p.BillDays) / TierStructureHybrid1_Base)

                    For Each objBoundary In objTierBoundarySubset
                        dblThreshold = objBoundary.Threshold * factor
                        objNewBoundary = New TierBoundary(objBoundary.BaseOrTier, objBoundary.TimeOfUse, objBoundary.Season, dblThreshold)
                        objFinalBoundaries.Add(objNewBoundary)
                    Next

                End If

            Case TierStructureType.MonthlyBoundsFromAVGU
                ' calculate boundaries from average usage and a days usage passed via rate helper;  AVGU, DAYU
                objFinalBoundaries = New TierBoundaryCollection

                Dim avgu As Double = 0.0
                Dim dayu As Integer = 1
                Dim modifiers As RateClassModifierCollection

                ' some callers will be able to provide modifiers, some will not, and thus, we will generate modifiers if not provided
                If (Not p.RateClassModifiers Is Nothing) Then
                    modifiers = p.RateClassModifiers
                Else
                    modifiers = CreateRateClassModifierCollection(p.RateClass, p.RateCompanyID)
                End If

                avgu = modifiers.GetModifierAVGU()
                dayu = modifiers.GetModifierDAYU()

                For Each objBoundary In objTierBoundarySubset
                    dblThreshold = objBoundary.Threshold + (avgu * dayu)
                    objNewBoundary = New TierBoundary(objBoundary.BaseOrTier, objBoundary.TimeOfUse, objBoundary.Season, dblThreshold)
                    objFinalBoundaries.Add(objNewBoundary)
                Next

            Case TierStructureType.DailyBounds, TierStructureType.DailyBoundsPerDwellingUnit
                objFinalBoundaries = New TierBoundaryCollection

                For Each objBoundary In objTierBoundarySubset
                    dblThreshold = objBoundary.Threshold * p.BillDays
                    If (p.TierStructureType = TierStructureType.DailyBoundsPerDwellingUnit) Then
                        dblThreshold = dblThreshold * p.DwellingUnits
                    End If
                    objNewBoundary = New TierBoundary(objBoundary.BaseOrTier, objBoundary.TimeOfUse, objBoundary.Season, dblThreshold)
                    objFinalBoundaries.Add(objNewBoundary)
                Next

            Case TierStructureType.PercentBaseTier1, TierStructureType.PercentBaseTier1WithDwellingUnits
                objFinalBoundaries = New TierBoundaryCollection
                ' Need to grab the territory and code from the RateClass (underscore seperates)
                ' Pass parts ByRef
                Dim bParseOkay As Boolean = ParseRateClass(p.RateCompanyID, p.RateClass, sClass, sCode, sTerritory)

                If (bParseOkay) Then
                    ' Parsed okay, now lookup Baseline value
                    objDC = New dctlRateEngine(m_sRateEngineConnectionString, m_xmlFilesPath, m_dataSourceType, m_cachingType, m_cacheTimeoutInMinutes)
                    dblBaseline = objDC.GetBaseline(p.RateDefID, sCode, sTerritory, p.Season)

                    ' **** Boundaries must be in order from here - Tier1, Tier2, etc
                    objTierBoundarySubset.Sort()
                    For Each objBoundary In objTierBoundarySubset

                        If (objBoundary.BaseOrTier = BaseOrTier.Tier1) Then
                            dblThreshold = dblBaseline * p.BillDays
                            If (p.TierStructureType = TierStructureType.PercentBaseTier1WithDwellingUnits) Then
                                dblThreshold = dblThreshold * p.DwellingUnits
                            End If
                            dblBaseX = dblThreshold
                        Else
                            dblThreshold = (objBoundary.Threshold / 100) * dblBaseX
                            If (p.TierStructureType = TierStructureType.PercentBaseTier1WithDwellingUnits) Then
                                dblThreshold = dblThreshold * p.DwellingUnits
                            End If
                        End If

                        objNewBoundary = New TierBoundary(objBoundary.BaseOrTier, objBoundary.TimeOfUse, objBoundary.Season, dblThreshold)
                        objFinalBoundaries.Add(objNewBoundary)
                    Next


                Else
                    ' The structureIndicated that we need to parse the RateClass, yet
                    ' the RateClass did not contain valid underscore (break char), or other parse error
                    Throw New Exception(ksRateClassParsingError)
                End If

            Case TierStructureType.DemandHours
                objFinalBoundaries = New TierBoundaryCollection

                Dim objUsage As Usage
                Dim dblDemand As Double

                ' Get Demand from Usage collection; get demand based on season if applicable; fallback to Demand w/ undefined season; then to 1.0
                objUsage = p.Usages.FindByParts(BaseOrTier.Demand, TimeOfUse.Undefined, p.Season)
                If (Not objUsage Is Nothing) Then
                    dblDemand = objUsage.Quantity
                Else
                    objUsage = p.Usages.FindByParts(BaseOrTier.Demand, TimeOfUse.Undefined, Season.Undefined)
                    If (Not objUsage Is Nothing) Then dblDemand = objUsage.Quantity Else dblDemand = 0
                End If

                ' If demand is not available at all, or it is ZERO, force the first boundary to be very high 
                ' by using an artificially HIGH demand amount;  this should force all usage to fall below first threshold
                If (dblDemand = 0) Then dblDemand = 5000

                For Each objBoundary In objTierBoundarySubset
                    dblThreshold = objBoundary.Threshold * dblDemand
                    objNewBoundary = New TierBoundary(objBoundary.BaseOrTier, objBoundary.TimeOfUse, objBoundary.Season, dblThreshold)
                    objFinalBoundaries.Add(objNewBoundary)
                Next


            Case TierStructureType.DemandKWhCombo
                objFinalBoundaries = New TierBoundaryCollection

                Dim objUsage As Usage
                Dim dblDemand, dblTotalServiceUse As Double
                Dim dblArtificialGrandTotal, dblTotalTierEnergy, dblTierEnergy, dblLastBoundaryValue, dblLeftover As Double
                Dim dblTemp As Double
                Dim arrExtra(7) As Double
                Dim nCount As Integer
                Dim dblTier1SecondaryBoundaryValue As Double

                ' Get Demand from Usage collection; get demand based on season if applicable; fallback to Demand w/ undefined season; then to 1.0
                objUsage = p.Usages.FindByParts(BaseOrTier.Demand, TimeOfUse.Undefined, p.Season)
                If (Not objUsage Is Nothing) Then
                    dblDemand = objUsage.Quantity
                Else
                    objUsage = p.Usages.FindByParts(BaseOrTier.Demand, TimeOfUse.Undefined, Season.Undefined)
                    If (Not objUsage Is Nothing) Then dblDemand = objUsage.Quantity Else dblDemand = 0
                End If

                ' Get actual total service usage
                objUsage = p.Usages.FindByParts(BaseOrTier.TotalServiceUse, TimeOfUse.Undefined, Season.Undefined)
                If (Not objUsage Is Nothing) Then
                    dblTotalServiceUse = objUsage.Quantity
                Else
                    dblTotalServiceUse = 0
                End If

                ' Calculate extras for tiers
                dblArtificialGrandTotal = dblTotalServiceUse
                nCount = 1              ' not zero based here to make it easier to read; 1 = tier1, etc.
                For Each objBoundary In objTierBoundarySubset
                    If (nCount = 1) Then
                        dblTier1SecondaryBoundaryValue = objBoundary.SecondaryThreshold
                        arrExtra(nCount) = 0.0
                    Else
                        dblTemp = (dblDemand - dblTier1SecondaryBoundaryValue) * objBoundary.SecondaryThreshold
                        If (dblTemp < 0.0) Then dblTemp = 0.0
                        'If (dblTemp > dblTotalServiceUse) Then dblTemp = dblTotalServiceUse ' TEMPORARY
                        arrExtra(nCount) = dblTemp
                    End If
                    dblArtificialGrandTotal = dblArtificialGrandTotal + arrExtra(nCount)
                    nCount = nCount + 1
                Next

                dblLastBoundaryValue = 0
                dblTotalTierEnergy = 0
                nCount = 1
                ' Loop again and use calculated extras
                For Each objBoundary In objTierBoundarySubset
                    If (dblArtificialGrandTotal < objBoundary.Threshold) Then
                        dblTierEnergy = dblArtificialGrandTotal - dblTotalTierEnergy
                        If (dblTierEnergy < 0) Then dblTierEnergy = 0
                    Else
                        dblTierEnergy = (objBoundary.Threshold - dblLastBoundaryValue) + arrExtra(nCount)
                    End If

                    objNewBoundary = New TierBoundary(objBoundary.BaseOrTier, objBoundary.TimeOfUse, objBoundary.Season, dblTierEnergy, 0.0)
                    objFinalBoundaries.Add(objNewBoundary)

                    dblTotalTierEnergy = dblTotalTierEnergy + dblTierEnergy
                    dblLastBoundaryValue = objBoundary.Threshold
                    nCount = nCount + 1
                Next
                ' leftover
                dblLeftover = dblTotalServiceUse - dblTotalTierEnergy
                If (dblLeftover < 0) Then dblLeftover = 0
                objNewBoundary = New TierBoundary(CType(nCount, BaseOrTier), objBoundary.TimeOfUse, objBoundary.Season, dblLeftover, 0.0)
                objFinalBoundaries.Add(objNewBoundary)


            Case Else
                ' Sure really not happen, but just in case
                Throw New Exception(ksTierBoundaryError)

        End Select

        ' Some callers will provide a seasonal scale factor that shall be applied to every boundary before returning to the caller.
        If (p.SeasonScaleFactor <> 1.0) Then
            objFinalBoundaries.Scale(p.SeasonScaleFactor)
        End If

        Return (objFinalBoundaries)
    End Function

    ' Season factors
    Private Function DetermineSeasonFactors() As SeasonFactorCollection
        Dim objSeasonFactor As SeasonFactor
        Dim objFactorColl As New SeasonFactorCollection

        objSeasonFactor = New SeasonFactor(Season.Undefined, 1.0)
        objFactorColl.Add(objSeasonFactor.Season, objSeasonFactor)

        Return (objFactorColl)
    End Function

    Private Function DetermineSeasonFactors(ByVal dtStartDate As DateTime, ByVal dtEndDate As DateTime) As SeasonFactorCollection
        Dim objSeasonFactor As SeasonFactor
        Dim objFactorColl As New SeasonFactorCollection

        objSeasonFactor = New SeasonFactor(Season.Undefined, 1.0, dtEndDate.Subtract(dtStartDate).Days + 1) 'inclusive start and end date
        objFactorColl.Add(objSeasonFactor.Season, objSeasonFactor)

        Return (objFactorColl)
    End Function


    Private Function DetermineSeasonFactors(ByVal objDef As DetailedRateDefinition, ByVal dtStartDate As DateTime, ByVal dtEndDate As DateTime) As SeasonFactorCollection
        Return (DetermineSeasonFactors(objDef, dtEndDate.Subtract(dtStartDate).Days + 1, dtStartDate, dtEndDate, True, 1.0, False, Season.Undefined))
    End Function

    Private Function DetermineSeasonFactors(ByVal objDef As DetailedRateDefinition, ByVal dtStartDate As DateTime, ByVal dtEndDate As DateTime, ByVal bUseSeasonProrateOverride As Boolean, ByVal eOverrideSeason As Season) As SeasonFactorCollection
        Return (DetermineSeasonFactors(objDef, dtEndDate.Subtract(dtStartDate).Days + 1, dtStartDate, dtEndDate, True, 1.0, bUseSeasonProrateOverride, eOverrideSeason))
    End Function

    Private Function DetermineSeasonFactors(ByVal objDef As DetailedRateDefinition, ByVal nBillDays As Integer, ByVal dtStartDate As DateTime, ByVal dtEndDate As DateTime, ByVal bFinalDefinition As Boolean, ByVal dblProrateFactor As Double, ByVal bUseSeasonProrateOverride As Boolean, ByVal eOverrideSeason As Season) As SeasonFactorCollection
        Dim nLocalBillDays As Integer
        Dim dtLocalStartDate As DateTime
        Dim dtLocalEndDate As DateTime
        Dim dtWorkingEndDate As DateTime
        Dim bDone As Boolean
        Dim objSeasonBoundary As SeasonBoundary
        Dim objSeasonFactor As SeasonFactor
        Dim objFactorColl As New SeasonFactorCollection

        If (bUseSeasonProrateOverride) Then
            objSeasonFactor = New SeasonFactor(eOverrideSeason, 1.0, nBillDays)
            objFactorColl.Add(objSeasonFactor.Season, objSeasonFactor)
        Else

            nLocalBillDays = CType(Math.Round(nBillDays * dblProrateFactor), Integer)
            objDef.SeasonBoundaries.Sort()           ' descending sort, newest first, right to left

            If (bFinalDefinition) Then
                Try
                    dtLocalStartDate = dtStartDate
                    If (dtLocalStartDate = DateTime.MinValue) Then
                        Throw New Exception(ksBadDate)
                    End If
                Catch ex As Exception
                    Throw New Exception(ksBadDate)
                End Try
            Else
                dtLocalStartDate = objDef.StartDate
            End If
            dtLocalEndDate = dtLocalStartDate.AddDays(CType(nLocalBillDays, Double) - 1)

            ' Main logic begins here
            dtWorkingEndDate = dtLocalEndDate
            Do While (Not bDone)
                objSeasonBoundary = objDef.SeasonBoundaries.FindInclusiveBoundary(dtWorkingEndDate)
                If (Not objSeasonBoundary Is Nothing) Then
                    ' Found inclusive boundary
                    Dim dblFactor As Double
                    Dim nXTotalDays As Integer
                    Dim nXDays As Integer = dtWorkingEndDate.Subtract(ConvertDayToDate(objSeasonBoundary.StartDay, dtWorkingEndDate.Year)).Days + 1
                    nXTotalDays = nXTotalDays + nXDays
                    If (nXTotalDays > nLocalBillDays) Then
                        nXDays = nXDays - (dtLocalStartDate.Subtract(ConvertDayToDate(objSeasonBoundary.StartDay, dtLocalStartDate.Year)).Days)
                    End If

                    dblFactor = nXDays / nLocalBillDays
                    objSeasonFactor = New SeasonFactor(objSeasonBoundary.Season, dblFactor, nXDays)

                    '*** add the SeasonFactor ***
                    objFactorColl.SumAdd(objSeasonFactor.Season, objSeasonFactor)

                    dtWorkingEndDate = ConvertDayToDate(objSeasonBoundary.StartDay, dtWorkingEndDate.Year).AddDays(-1)
                    If (dtWorkingEndDate <= dtLocalStartDate) Then bDone = True
                Else
                    ' For some reason, no boundary contains the given date, should be impossible.
                    Throw New Exception(ksSeasonalBoundaryError)
                End If
            Loop

        End If

        Return (objFactorColl)
    End Function

    ''' <summary>
    ''' Determine which seasons are present, always including undefined season, and how many days exist per season.  
    ''' If one hour for a day exists, it considers a full day.
    ''' </summary>
    ''' <param name="readings"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function DetermineSeasonFactors(ByVal readings As ReadingCollection) As SeasonFactorCollection
        Dim objSeasonFactor As SeasonFactor
        Dim objFactorColl As New SeasonFactorCollection
        Dim seasonExists() As Boolean = {False, False, False, False, False}
        Dim seasonDaysCount() As Integer = {0, 0, 0, 0, 0}
        Dim prevDayOfYear As Integer = -1
        Dim undefinedSeasonDayCount As Integer = 0
        Dim totalDays As Integer = 0

        objSeasonFactor = New SeasonFactor(Season.Undefined, 1.0)
        objFactorColl.Add(objSeasonFactor.Season, objSeasonFactor)
        seasonExists(Season.Undefined) = True

        For Each r As Reading In readings
            If (r.Datestamp.DayOfYear <> prevDayOfYear) Then
                seasonDaysCount(r.Season) = seasonDaysCount(r.Season) + 1
                undefinedSeasonDayCount = undefinedSeasonDayCount + 1
            End If

            If (Not seasonExists(r.Season)) Then
                objSeasonFactor = New SeasonFactor(r.Season, 1.0)
                objFactorColl.Add(objSeasonFactor.Season, objSeasonFactor)
                seasonExists(r.Season) = True
            End If

            prevDayOfYear = r.Datestamp.DayOfYear
        Next

        ' save the day counts into the season factors
        For Each dictEntrySeason As DictionaryEntry In objFactorColl
            Dim f As SeasonFactor = CType(dictEntrySeason.Value, SeasonFactor)
            f.Days = seasonDaysCount(f.Season)
            If (f.Season = Season.Undefined AndAlso f.Days = 0) Then f.Days = undefinedSeasonDayCount

            If (undefinedSeasonDayCount <> 0) Then
                f.Factor = CDbl(f.Days) / undefinedSeasonDayCount
            End If
        Next

        Return (objFactorColl)
    End Function

    Private Function ConvertDayToDate(ByVal sMonthDay As String, ByVal nYear As Integer) As DateTime
        Dim sMonth As String = sMonthDay.Substring(0, 2)
        Dim sDay As String = sMonthDay.Substring(2, 2)
        Dim sYear As String = nYear.ToString
        Dim sbParseDate As New System.Text.StringBuilder
        Dim dtDate As DateTime

        sbParseDate.Append(sMonth) : sbParseDate.Append("/")
        sbParseDate.Append(sDay) : sbParseDate.Append("/")
        sbParseDate.Append(sYear)
        dtDate = DateTime.Parse(sbParseDate.ToString)

        Return (dtDate)
    End Function

    Private Function CreateTOUListFromTierBoundaries(ByVal tierBoundaries As TierBoundaryCollection, ByVal calculateTieredAndTOU As Boolean) As TOUList
        Dim touL As New TOUList
        Dim tierB As TierBoundary
        Dim i As Integer = 0

        If (calculateTieredAndTOU) Then
            ' Seasonal not important here; just the TOU portion of tiers is used to create list of TOU's
            For Each tierB In tierBoundaries
                If (tierB.TimeOfUse <> TimeOfUse.Undefined) Then
                    If (Not touL.Contains(tierB.TimeOfUse)) Then
                        touL.Add(tierB.TimeOfUse)
                    End If
                End If
            Next
        End If

        ' If there are no TOU in the TierBoundaries then be sure to add an undefined TOU to the TOUList
        If (touL.Count = 0) Then
            touL.Add(TimeOfUse.Undefined)
        End If

        touL.Sort()

        Return (touL)
    End Function

    Private Function CreateTOUListFromTierBoundaries(ByVal tierBoundaries As TierBoundaryCollection) As TOUList
        Dim touL As New TOUList
        Dim tierB As TierBoundary
        Dim i As Integer = 0

        ' Seasonal not important here; just the TOU portion of tiers is used to create list of TOU's
        For Each tierB In tierBoundaries
            If (tierB.TimeOfUse <> TimeOfUse.Undefined) Then
                If (Not touL.Contains(tierB.TimeOfUse)) Then
                    touL.Add(tierB.TimeOfUse)
                End If
            End If
        Next

        ' ALways add an undefined TOU to the TOUList
        touL.Add(TimeOfUse.Undefined)

        touL.Sort()

        Return (touL)
    End Function

    Private Function CalcTieredCosts(ByVal rateClassModifiers As RateClassModifierCollection, _
                                     ByVal objUsageColl As UsageCollection, ByVal objUseCharges As UseChargeCollection, _
                                     ByVal objSeasonFactors As SeasonFactorCollection, ByVal objDef As DetailedRateDefinition, _
                                     ByVal sRateClassArg As String, ByVal nDwellingUnits As Integer, ByVal nBillDays As Integer, _
                                     ByVal objCostColl As CostCollection, ByVal dtDefinitionStartDate As DateTime, ByVal dtStartDate As DateTime, _
                                     ByVal dtEndDate As DateTime, ByVal bFinalDefinition As Boolean, ByVal dblProrateFactor As Double, _
                                     ByVal bCalculateTieredAndTOU As Boolean, _
                                     ByVal useProjectedEndDateToFinalizeTierBoundaries As Boolean, ByVal projectedEndDate As DateTime) As CostCollection
        Dim ksFunctionName As String = "CalcTieredCosts"
        Dim objDE As DictionaryEntry
        Dim objTierBoundaryColl As TierBoundaryCollection
        Dim objSeasonFactor As SeasonFactor
        Dim objUseCharge As UseCharge
        Dim objUseChargeColl As UseChargeCollection
        Dim objTierBoundary As TierBoundary
        Dim i As Integer
        Dim dblTotalUsage As Double
        Dim dblPartOfUsage As Double
        Dim dblRatioUsage As Double
        Dim dblAmount, dblLastTierThreshold, dblTierThresholdTotal As Double
        Dim dblTempA As Double
        Dim dblLeftover As Double
        Dim timeOfUseList As TOUList
        Dim tou As TimeOfUse
        Dim adjustedBillDays As Integer

        adjustedBillDays = nBillDays

        If (useProjectedEndDateToFinalizeTierBoundaries AndAlso projectedEndDate <> DateTime.MinValue) Then
            adjustedBillDays = projectedEndDate.Subtract(dtStartDate).Days + 1
        End If

        ' This MUST loop through Seasons and apply TotalServiceUse across Tiers
        ' Also handles cases where Tiered and TOU exists at the same time
        For Each objDE In objSeasonFactors
            objSeasonFactor = CType(objDE.Value, SeasonFactor)

            ' Get Time of Use List from tier boundaries; this returns undefined TOU if no TOU in tier boundaries;
            timeOfUseList = CreateTOUListFromTierBoundaries(objDef.TierBoundaries, bCalculateTieredAndTOU)

            For Each tou In timeOfUseList

                objTierBoundaryColl = FinalizeTierBoundaries(rateClassModifiers, objDef.RateCompanyID, objDef.DefinitionID, objDef.MasterID, objDef.TierBoundaries, objDef.TierStructureType, sRateClassArg, objSeasonFactor.Season, tou, nDwellingUnits, adjustedBillDays, objUsageColl, 1.0)
                Log(objTierBoundaryColl)

                Select Case objDef.TierStructureType

                    Case TierStructureType.DailyBounds, TierStructureType.DailyBoundsPerDwellingUnit, _
                     TierStructureType.DemandHours, TierStructureType.MonthlyBounds, _
                     TierStructureType.PercentBaseTier1, TierStructureType.PercentBaseTier1WithDwellingUnits, _
                     TierStructureType.MonthlyBoundsHybrid1, TierStructureType.MonthlyBoundsFromAVGU, _
                     TierStructureType.Undefined
                        ' REGULAR PROCESSING

                        dblLastTierThreshold = 0
                        dblTierThresholdTotal = 0

                        dblTotalUsage = objUsageColl.GetTotalServiceUse()
                        If (bCalculateTieredAndTOU) Then
                            dblPartOfUsage = objUsageColl.GetTotalTimeOfUseUsageForSeason(tou, objSeasonFactor.Season)
                            ' Set SeasonFactor to 1.0, since we will have seasonal usage here;
                            objSeasonFactor.Factor = 1.0
                        Else
                            dblPartOfUsage = dblTotalUsage
                        End If

                        ' BOUNDARIES are now setup, need to take real usage for TotalServiceUse and apply to boundaries

                        If (dblTotalUsage <> 0) Then
                            ' The TotalUsage will be divided across calculated tiers; may be adjusted it tiered with tou

                            dblRatioUsage = dblPartOfUsage / dblTotalUsage

                            ' Loop Boundaries to find Tiered Amounts - to leftover one at bottom too!
                            For i = 0 To objTierBoundaryColl.Count - 1
                                objTierBoundary = objTierBoundaryColl(i)
                                objUseChargeColl = objUseCharges.FindByParts(objTierBoundary.BaseOrTier, tou, objSeasonFactor.Season)

                                If (objUseChargeColl.Count > 0) Then

                                    For Each objUseCharge In objUseChargeColl
                                        If (dblTotalUsage <= objTierBoundary.Threshold) Then
                                            If (i = 0) Then
                                                dblAmount = dblTotalUsage * objUseCharge.ChargeValue
                                                Log(ksFunctionName, dblAmount * dblRatioUsage * objSeasonFactor.Factor, dblTotalUsage, dblRatioUsage, objSeasonFactor.Factor, objUseCharge.ChargeValue, objUseCharge.CostType, True)
                                            Else
                                                dblTempA = Math.Max(dblTotalUsage - dblLastTierThreshold, 0)
                                                dblAmount = (dblTempA) * objUseCharge.ChargeValue
                                                Log(ksFunctionName, dblAmount * dblRatioUsage * objSeasonFactor.Factor, dblTempA, dblRatioUsage, objSeasonFactor.Factor, objUseCharge.ChargeValue, objUseCharge.CostType, True)
                                            End If
                                        Else
                                            If (i = 0) Then
                                                dblAmount = objTierBoundary.Threshold * objUseCharge.ChargeValue
                                                Log(ksFunctionName, dblAmount * dblRatioUsage * objSeasonFactor.Factor, objTierBoundary.Threshold, dblRatioUsage, objSeasonFactor.Factor, objUseCharge.ChargeValue, objUseCharge.CostType, True)
                                            Else
                                                dblTempA = objTierBoundary.Threshold - dblLastTierThreshold
                                                dblAmount = (dblTempA) * objUseCharge.ChargeValue
                                                Log(ksFunctionName, dblAmount * dblRatioUsage * objSeasonFactor.Factor, dblTempA, dblRatioUsage, objSeasonFactor.Factor, objUseCharge.ChargeValue, objUseCharge.CostType, True)
                                            End If
                                        End If

                                        ' Adjust for the season factor; adjust for the Ratio of Usage do to possible TOU
                                        dblAmount = dblAmount * dblRatioUsage * objSeasonFactor.Factor
                                        objCostColl.SumAdd(objUseCharge.CostType, New Cost(objUseCharge.CostType, dblAmount))
                                    Next

                                    dblTierThresholdTotal = dblTierThresholdTotal + (objTierBoundary.Threshold - dblLastTierThreshold)
                                    dblLastTierThreshold = objTierBoundary.Threshold
                                End If

                            Next

                            ' Any leftover ?
                            dblLeftover = (dblTotalUsage - dblTierThresholdTotal)
                            If (dblLeftover < 0.0) Then dblLeftover = 0.0

                            objUseChargeColl = objUseCharges.FindByParts(CType(i + 1, BaseOrTier), tou, objSeasonFactor.Season)

                            If (objUseChargeColl.Count > 0) Then
                                For Each objUseCharge In objUseChargeColl
                                    dblAmount = dblLeftover * dblRatioUsage * objUseCharge.ChargeValue * objSeasonFactor.Factor
                                    objCostColl.SumAdd(objUseCharge.CostType, New Cost(objUseCharge.CostType, dblAmount))
                                    Log(ksFunctionName, dblAmount, dblLeftover, dblRatioUsage, objSeasonFactor.Factor, objUseCharge.ChargeValue, objUseCharge.CostType, True)
                                Next
                            End If

                        End If


                    Case TierStructureType.DemandKWhCombo

                        ' Use TimeOfUse.Undefined here, not tou from loop;
                        For i = 0 To objTierBoundaryColl.Count - 1
                            objTierBoundary = objTierBoundaryColl(i)
                            objUseChargeColl = objUseCharges.FindByParts(objTierBoundary.BaseOrTier, TimeOfUse.Undefined, objSeasonFactor.Season)

                            If (objUseChargeColl.Count > 0) Then
                                For Each objUseCharge In objUseChargeColl
                                    dblAmount = objTierBoundary.Threshold * objUseCharge.ChargeValue * objSeasonFactor.Factor
                                    objCostColl.SumAdd(objUseCharge.CostType, New Cost(objUseCharge.CostType, dblAmount))
                                    Log(ksFunctionName, TierStructureType.DemandKWhCombo.ToString, dblAmount, objTierBoundary.Threshold, objUseCharge.ChargeValue, objSeasonFactor.Factor, objUseCharge.CostType)
                                Next
                            End If

                        Next


                    Case Else
                        ' noop; should not get here

                End Select

            Next

        Next

        Return (objCostColl)
    End Function

    Private Function CalcSeasonalCosts(ByVal objDef As DetailedRateDefinition, ByVal objUsageColl As UsageCollection, ByVal objUseCharges As UseChargeCollection, ByVal objSeasonFactors As SeasonFactorCollection, ByVal objCostColl As CostCollection, ByVal nBillDays As Integer, ByVal dtDefinitionStartDate As DateTime, ByVal dtStartDate As DateTime, ByVal dtEndDate As DateTime, ByVal bFinalDefinition As Boolean, ByVal dblProrateFactor As Double, ByVal bIsDemandResponseEligible As Boolean) As CostCollection
        Dim ksFunctionName As String = "CalcSeasonalCosts"
        Dim ksFunctionWithRTP As String = "CalcCostsWithUsagesAndUseCharges:RTP"
        Dim objUseCharge As UseCharge
        Dim objUseChargeColl As UseChargeCollection = Nothing
        Dim objSeasonFactor As SeasonFactor
        Dim objUsage As Usage
        Dim dblAmount As Double
        Dim objDE As DictionaryEntry
        Dim bRemoveSF As Boolean
        Dim bSpecialDemandHandling As Boolean

        ' Only if not present already, add an undefined season factor (to force handling of Demand or fcr if present)
        If (Not objSeasonFactors.Contains(Season.Undefined)) Then
            objSeasonFactors.Add(Season.Undefined, New SeasonFactor(Season.Undefined, 1.0))
            bRemoveSF = True
        End If

        If (objDef.IsTiered And objDef.TierStructureType = TierStructureType.DemandKWhCombo) Then bSpecialDemandHandling = True Else bSpecialDemandHandling = False

        ' This loops through usages (usually simple TotalServiceUse, sometimes direct tiers too, and could include demand at times)
        For Each objUsage In objUsageColl

            For Each objDE In objSeasonFactors
                objSeasonFactor = CType(objDE.Value, SeasonFactor)

                If (objUsage.Season = Season.Undefined) Then
                    ' non-seasonal usage determinants get applied against each season factor for existing applicable charges
                    objUseChargeColl = objUseCharges.FindByParts(objUsage.BaseOrTier, TimeOfUse.Undefined, objSeasonFactor.Season)
                ElseIf (objUsage.Season = objSeasonFactor.Season) Then
                    If (objDef.IsDemand AndAlso _
                        (objUsage.BaseOrTier = BaseOrTier.Demand) AndAlso _
                        objDef.SeasonalProrateType = SeasonalProrateType.SplitBillDays AndAlso (objDef.SeasonalDemandProrateType = SeasonalDemandProrateType.SplitBillDaysMaxDemand Or objDef.SeasonalDemandProrateType = SeasonalDemandProrateType.SplitBillDaysPeriodDemand)) Then
                        ' 15.06 bug 58179 - if it's demand usage, and seasonal demand prorate is splitdaysmaxdemand or splitedaysperioddemand, then apply the charges with season factor 
                        objUseChargeColl = objUseCharges.FindByParts(objUsage.BaseOrTier, objUsage.TimeOfUse, objUsage.Season)
                    Else
                        ' if usage determinant is already seasonal, only directly apply once
                        objUseChargeColl = objUseCharges.FindByParts(objUsage.BaseOrTier, TimeOfUse.Undefined, objUsage.Season)
                    End If
                Else
                    objUseChargeColl = Nothing
                End If

                If ((Not objUseChargeColl Is Nothing) AndAlso objUseChargeColl.Count > 0) Then

                    ' Can be multiple Use Charges that match -> ti, tu, se (categorize into same or diff CostTypes!)
                    For Each objUseCharge In objUseChargeColl

                        If (objUseCharge.FuelCostRecovery = True) Then
                            ' see CalculateChargeForFCR function notes
                            objUseCharge.ChargeValue = CalculateChargeForFCR(objUseCharge.FCRGroup, nBillDays, dtDefinitionStartDate, dtStartDate, dtEndDate, bFinalDefinition, dblProrateFactor, objDef.ProrateType)
                        End If

                        ' Must check for Real-Time Pricing if applicable; most of time NOT real-time, but regular pricing instead
                        If (Not objUseCharge.RealTimePricing) Then
                            If (bSpecialDemandHandling And objUseCharge.BaseOrTier = BaseOrTier.Demand) Then
                                ' Tier1 Secondary Threshold is the free portion of demand to subtract
                                dblAmount = ((objUsage.Quantity - objDef.TierBoundaries(0).SecondaryThreshold) * objSeasonFactor.Factor) * objUseCharge.ChargeValue
                                Log(ksFunctionName, objDef.TierStructureType.ToString(), dblAmount, objDef.TierBoundaries(0).SecondaryThreshold, objSeasonFactor.Factor, objUsage.Quantity, objUseCharge.ChargeValue, objUseCharge.CostType)
                            Else
                                ' regular calcultion here
                                dblAmount = objUseCharge.ChargeValue * (objUsage.Quantity * objSeasonFactor.Factor)
                                Log(ksFunctionName, dblAmount, objSeasonFactor.Factor, objUsage.Quantity, objUseCharge.ChargeValue, objUseCharge.CostType)
                            End If
                        Else
                            ' see CalculateChargeForRTP function notes
                            dblAmount = CalculateCostForRTP(objDef.RateCompanyID, objUseCharge.RTPGroup, objUsage, objDef.RateClass, nBillDays, dtDefinitionStartDate, dtStartDate, dtEndDate, bFinalDefinition, dblProrateFactor, objDef.ProrateType, bIsDemandResponseEligible)
                            Log(ksFunctionWithRTP, dblAmount, objSeasonFactor.Factor, objUsage.Quantity, objUseCharge.ChargeValue, objUseCharge.CostType)
                        End If

                        objCostColl.SumAdd(objUseCharge.CostType, New Cost(objUseCharge.CostType, dblAmount))

                    Next

                End If
            Next
        Next

        ' Remove the undefined season factor that was added above;  subsequent operations might not want this present
        If (bRemoveSF) Then objSeasonFactors.Remove(Season.Undefined)

        Return (objCostColl)
    End Function

    Private Function CalcCostsWithUsagesAndUseCharges(ByVal objDef As DetailedRateDefinition, ByVal objUsageColl As UsageCollection, ByVal objUseCharges As UseChargeCollection, ByVal objCostColl As CostCollection, ByVal nBillDays As Integer, ByVal dtDefinitionStartDate As DateTime, ByVal dtStartDate As DateTime, ByVal dtEndDate As DateTime, ByVal bFinalDefinition As Boolean, ByVal dblProrateFactor As Double, ByVal bCalcTOUUsagesOnly As Boolean, ByVal ignoreTieredCharges As Boolean, ByVal bIsDemandResponseEligible As Boolean) As CostCollection
        Dim ksFunctionName As String = "CalcCostsWithUsagesAndUseCharges"
        Dim ksFunctionWithRTP As String = "CalcCostsWithUsagesAndUseCharges:RTP"
        Dim objUseCharge As UseCharge
        Dim objUseChargeColl As UseChargeCollection
        Dim objUsage As Usage
        Dim dblAmount As Double
        Dim bProcessUsage As Boolean
        Dim bSpecialDemandHandling As Boolean

        If (objDef.IsTiered And objDef.TierStructureType = TierStructureType.DemandKWhCombo) Then bSpecialDemandHandling = True Else bSpecialDemandHandling = False

        ' This simply does Usage*UseCharge = Cost
        For Each objUsage In objUsageColl

            If (Not bCalcTOUUsagesOnly) Then
                bProcessUsage = True
            Else
                
                If (objUsage.TimeOfUse <> TimeOfUse.Undefined) Then
                    If (objDef.IsDemand AndAlso objUsage.Season <> TimeOfUse.Undefined AndAlso _
                        (objUsage.BaseOrTier = BaseOrTier.Demand) AndAlso _
                        objDef.SeasonalProrateType = SeasonalProrateType.SplitBillDays AndAlso (objDef.SeasonalDemandProrateType = SeasonalDemandProrateType.SplitBillDaysMaxDemand Or objDef.SeasonalDemandProrateType = SeasonalDemandProrateType.SplitBillDaysPeriodDemand)) Then
                        ' 15.06 Bug 58179 - should already processed in CalcSeasonCost for season demand with splitdaysmaxdemand or splitdaysperioddemand
                        bProcessUsage = False
                    Else
                        ' Only calculate cost for Usages that have a defined TOU component; since these usages will not have been processed by previous seasonal calc
                        bProcessUsage = True
                    End If
            Else
                ' Only calculate cost for Usages that have a defined TOU component; since these usages will not have been processed by previous seasonal calc
                    bProcessUsage = False
                End If
            End If

            If (bProcessUsage) Then
                objUseChargeColl = objUseCharges.FindByUsageProperties(objUsage)
                Dim existsTieredCharges As Boolean = objUseChargeColl.ExistsAnyTier()

                If (Not (ignoreTieredCharges AndAlso existsTieredCharges)) Then

                    If (objUseChargeColl.Count > 0) Then
                        For Each objUseCharge In objUseChargeColl
                            ' Must check for FCR adjustment if applicable
                            If (objUseCharge.FuelCostRecovery = True) Then
                                ' see CalculateChargeForFCR function notes
                                objUseCharge.ChargeValue = CalculateChargeForFCR(objUseCharge.FCRGroup, nBillDays, dtDefinitionStartDate, dtStartDate, dtEndDate, bFinalDefinition, dblProrateFactor, objDef.ProrateType)
                            End If
                            ' Must check for Real-Time Pricing if applicable;  most of time NOT real-time, but regular pricing instead
                            If (Not objUseCharge.RealTimePricing) Then
                                If (bSpecialDemandHandling And objUseCharge.BaseOrTier = BaseOrTier.Demand) Then
                                    ' Tier1 Secondary Threshold is the free portion of demand to subtract
                                    dblAmount = (objUsage.Quantity - objDef.TierBoundaries(0).SecondaryThreshold) * objUseCharge.ChargeValue
                                    Log(ksFunctionName, objDef.TierStructureType.ToString(), dblAmount, objDef.TierBoundaries(0).SecondaryThreshold, 1.0, objUsage.Quantity, objUseCharge.ChargeValue, objUseCharge.CostType)
                                Else
                                    ' regular calculation here
                                    dblAmount = objUseCharge.ChargeValue * objUsage.Quantity
                                    Log(ksFunctionName, dblAmount, objUsage.Quantity, objUseCharge.ChargeValue, objUseCharge.CostType)
                                End If
                            Else
                                ' see CalculateChargeForRTP function notes
                                dblAmount = CalculateCostForRTP(objDef.RateCompanyID, objUseCharge.RTPGroup, objUsage, objDef.RateClass, nBillDays, dtDefinitionStartDate, dtStartDate, dtEndDate, bFinalDefinition, dblProrateFactor, objDef.ProrateType, bIsDemandResponseEligible)
                                Log(ksFunctionWithRTP, dblAmount, objUsage.Quantity, objUseCharge.ChargeValue, objUseCharge.CostType)
                            End If

                            objCostColl.SumAdd(objUseCharge.CostType, New Cost(objUseCharge.CostType, dblAmount))

                        Next

                    End If

                End If     'tiered use charge check

            End If

        Next

        Return (objCostColl)
    End Function


    ' This is a FCR UseCharge, must calculate an adjusted ChargeValue based on lookup into FCR, given date ranges, etc
    ' The Prorate factor is just used internally to help figure out the localBillDays, and is not actually being applied to the UseCharge (this all happens in finalize code)
    Private Function CalculateChargeForFCR(ByVal nFCRGroup As Integer, ByVal nBillDays As Integer, ByVal dtDefinitionStartDate As DateTime, ByVal dtStartDate As DateTime, ByVal dtEndDate As DateTime, ByVal bFinalDefinition As Boolean, ByVal dblProrateFactor As Double, ByVal eProrateType As ProrateType) As Double
        Dim ksFunctionName As String = "CalculateChargeForFCR"
        Dim dblRunningCharge As Double = 0.0
        Dim objFCRCollection As FCRCollection
        Dim objDC As dctlRateEngine
        Dim nLocalBillDays As Integer
        Dim dtLocalStartDate As DateTime
        Dim dtLocalEndDate As DateTime
        Dim dtWorkingEndDate As DateTime
        Dim bDone As Boolean
        Dim objFCR As FCR

        Log(ksFunctionName)

        nLocalBillDays = CType(Math.Round(nBillDays * dblProrateFactor), Integer)

        If (bFinalDefinition) Then
            Try
                dtLocalStartDate = dtStartDate
                If (dtLocalStartDate = DateTime.MinValue) Then
                    Throw New Exception(ksBadFCRDate)
                End If
            Catch ex As Exception
                Throw New Exception(ksBadFCRDate)
            End Try
        Else
            dtLocalStartDate = dtDefinitionStartDate             ' x = objDef.StartDate
        End If
        dtLocalEndDate = dtLocalStartDate.AddDays(CType(nLocalBillDays, Double) - 1)
        dtWorkingEndDate = dtLocalEndDate

        ' use date ranges here
        objDC = New dctlRateEngine(m_sRateEngineConnectionString, m_xmlFilesPath, m_dataSourceType, m_cachingType, m_cacheTimeoutInMinutes)
        'objFCRCollection = objDC.GetFuelCostRecoveryList(nFCRGroup, dtStartDate, dtEndDate)
        objFCRCollection = objDC.GetFuelCostRecoveryList(nFCRGroup, dtLocalStartDate, dtLocalEndDate)

        ' Most of the time FCR's will exist, if not, drop out with adjusted charge of 0.0
        If (objFCRCollection.Count > 0) Then
            Select Case eProrateType
                Case ProrateType.UseLatest
                    objFCR = objFCRCollection.FindInclusiveFCR(dtWorkingEndDate)
                    If (Not objFCR Is Nothing) Then
                        dblRunningCharge = objFCR.ChargeValue
                        Log("FCRCalc", nLocalBillDays, nLocalBillDays, objFCR.ChargeValue, objFCR.ChargeValue)
                    End If
                Case ProrateType.UsePrevious
                    objFCR = objFCRCollection.FindInclusiveFCR(dtLocalStartDate)
                    If (Not objFCR Is Nothing) Then
                        dblRunningCharge = objFCR.ChargeValue
                        Log("FCRCalc", nLocalBillDays, nLocalBillDays, objFCR.ChargeValue, objFCR.ChargeValue)
                    End If
                Case ProrateType.SplitBillDays
                    Do While (Not bDone)
                        objFCR = objFCRCollection.FindInclusiveFCR(dtWorkingEndDate)
                        If (Not objFCR Is Nothing) Then
                            ' Found inclusive FCR (the FCR that is <= WorkingEndDate
                            Dim dblFactor As Double
                            Dim dblAdjustedCharge As Double
                            Dim nXTotalDays As Integer
                            Dim nXDays As Integer = dtWorkingEndDate.Subtract(objFCR.StartDate).Days + 1
                            nXTotalDays = nXTotalDays + nXDays
                            If (nXTotalDays > nLocalBillDays) Then
                                nXDays = nXDays - (dtLocalStartDate.Subtract(objFCR.StartDate).Days)
                            End If

                            dblFactor = nXDays / nLocalBillDays
                            dblAdjustedCharge = objFCR.ChargeValue * dblFactor
                            dblRunningCharge = dblRunningCharge + dblAdjustedCharge

                            Log("FCRCalc", nXDays, nLocalBillDays, objFCR.ChargeValue, dblAdjustedCharge)

                            dtWorkingEndDate = objFCR.StartDate.AddDays(-1)
                            ' This next line is critical; changed from <= to just <
                            If (dtWorkingEndDate < dtLocalStartDate) Then bDone = True
                        Else
                            ' For some reason, FCR item was not found
                            ' The FCR likely does not go back far enough - just a partial FCR
                            ' No need to error, simply exit do loop - Throw New Exception(ksFCRError)
                            Exit Do
                        End If
                    Loop
            End Select

        End If

        Return (dblRunningCharge)
    End Function

    ' This is a Real-Time-Pricing UseCharge, must calculate a total COST based on lookup into RTP, given date ranges, etc
    ' The Prorate factor is just used internally to help figure out the localBillDays, and is not actually being applied to the UseCharge (this all happens in finalize code)
    Private Function CalculateCostForRTP(ByVal nRateCompanyID As Integer, ByVal nRTPGroup As Integer, ByVal objUsage As Usage, ByVal sRateClass As String, ByVal nBillDays As Integer, ByVal dtDefinitionStartDate As DateTime, ByVal dtStartDate As DateTime, ByVal dtEndDate As DateTime, ByVal bFinalDefinition As Boolean, ByVal dblProrateFactor As Double, ByVal eProrateType As ProrateType, ByVal bIsDemandResponseEligible As Boolean) As Double
        Dim ksFunctionName As String = "CalculateCostForRTP"
        Dim dblRunningCost As Double = 0.0
        Dim objPriceCollection As PriceCollection
        Dim objDC As dctlRateEngine
        Dim nLocalBillDays As Integer
        Dim dtLocalStartDate As DateTime
        Dim dtLocalEndDate As DateTime
        Dim i, j, d, k, iReadingOffset As Integer
        Dim dtFirstPrice As DateTime
        Dim dblPrice, dblQuantity As Double
        Dim nReadingsUpperBound As Integer
        Dim dblAdjustmentFactor As Double
        Dim eReadingInterval As Enums.ReadingInterval

        Log(ksFunctionName)

        If (Not objUsage Is Nothing) Then
            If (Not objUsage.Readings Is Nothing) Then
                If (objUsage.Readings.Count > 0) Then

                    eReadingInterval = objUsage.Readings.DetermineIntervalOfReadings()

                    If (eReadingInterval <> ReadingInterval.Daily And objUsage.Readings.ValidForRTP) Then

                        ' Usage readings exists that are interval readings
                        nLocalBillDays = CType(Math.Round(nBillDays * dblProrateFactor), Integer)

                        If (bFinalDefinition) Then
                            Try
                                dtLocalStartDate = dtStartDate
                                If (dtLocalStartDate = DateTime.MinValue) Then
                                    Throw New Exception(ksBadRTPDate)
                                End If
                            Catch ex As Exception
                                Throw New Exception(ksBadRTPDate)
                            End Try
                        Else
                            dtLocalStartDate = dtDefinitionStartDate
                        End If

                        ' Sometimes caller asks for prices that are a year prior to actual dates;
                        If (objUsage.Readings.UseYearOldRTP) Then
                            dtLocalStartDate = dtLocalStartDate.AddYears(-1)
                        End If

                        dtLocalEndDate = dtLocalStartDate.AddDays(CType(nLocalBillDays, Double) - 1)

                        ' use date ranges here
                        objDC = New dctlRateEngine(m_sRateEngineConnectionString, m_xmlFilesPath, m_dataSourceType, m_cachingType, m_cacheTimeoutInMinutes)

                        Try
                            ' need to add 23 hours to endDate in order to get all needed prices for the entire day; only use it for this query
                            objPriceCollection = objDC.GetPriceData(nRateCompanyID, nRTPGroup, sRateClass, dtLocalStartDate, dtLocalEndDate.AddHours(23), True)

                            ' if RTP does not exist, drop out with adjusted charge of 0.0
                            ' CR 20298, 20299, 20302 Oct 2011 - proceed with RTP only when Is Demand REsponse Eligible is true
                            ' if false, then ignore "event feed" and disregard theat the RTP attributes that are set up in the rate. (any rate may have a peak event component to it.  This attribute tells you whether or not to ignore it for customer)
                            ' if tur then apply the peak event + ptr parts of the rate as per current functionality
                            ' 0 or 1 key value pair
                            If (objPriceCollection.Count > 0 AndAlso bIsDemandResponseEligible) Then

                                dtFirstPrice = objPriceCollection(0).DateStamp
                                'If (objUsage.Readings.UseYearOldRTP) Then
                                '    dtFirstPrice = dtFirstPrice.AddYears(+1)
                                'End If
                                iReadingOffset = objUsage.Readings.FindIndexOfReadingWithMonthAndDay(dtFirstPrice.Month, dtFirstPrice.Day)
                                nReadingsUpperBound = objUsage.Readings.Count
                                dblAdjustmentFactor = objUsage.Readings.AdjustmentFactor

                                If (iReadingOffset >= 0) Then

                                    ' need to multiply corresponding usage hour with price hour
                                    d = 0
                                    For i = 0 To objPriceCollection.Count - 1
                                        j = i Mod 24
                                        If (iReadingOffset + i < nReadingsUpperBound) Then
                                            dblPrice = objPriceCollection(i).Price

                                            If (eReadingInterval = ReadingInterval.Hourly) Then
                                                dblQuantity = objUsage.Readings(iReadingOffset + i).Quantity
                                                dblRunningCost = dblRunningCost + (dblPrice * dblQuantity * dblAdjustmentFactor)
                                            ElseIf (eReadingInterval = ReadingInterval.FifteenMinute) Then
                                                For k = 0 To 3
                                                    dblQuantity = objUsage.Readings(iReadingOffset + (d * 24) + (j * 4) + k).Quantity
                                                    dblRunningCost = dblRunningCost + (dblPrice * dblQuantity * dblAdjustmentFactor)
                                                Next
                                            ElseIf (eReadingInterval = ReadingInterval.HalfHourly) Then
                                                For k = 0 To 1
                                                    dblQuantity = objUsage.Readings(iReadingOffset + (d * 24) + (j * 2) + k).Quantity
                                                    dblRunningCost = dblRunningCost + (dblPrice * dblQuantity * dblAdjustmentFactor)
                                                Next
                                            ElseIf (eReadingInterval = ReadingInterval.FiveMinute) Then 'CR 16664 May 2011 LC - quantity for 5-mins
                                                For k = 0 To 11
                                                    dblQuantity = objUsage.Readings(iReadingOffset + (d * 24) + (j * 12) + k).Quantity
                                                    dblRunningCost = dblRunningCost + (dblPrice * dblQuantity * dblAdjustmentFactor)
                                                Next
                                            End If

                                        End If
                                        If (j = 23) Then d = d + 1
                                    Next

                                Else
                                    ' no starting point found for readings; a result of zero will be returned
                                End If

                            End If

                        Catch ex As Exception
                            'Set an error here; a result of zero will be returned
                        End Try

                    End If
                End If
            End If
        End If

        Log("RTPCalc Cost", dblRunningCost)

        Return (dblRunningCost)
    End Function

    Private Function CheckForUsageByParts(ByVal objUsageColl As UsageCollection, ByVal eBaseOrTier As BaseOrTier, ByVal eTimeOfUse As TimeOfUse, ByVal eSeason As Season) As Boolean
        Dim bRetval As Boolean
        Dim objUsage As Usage

        objUsage = objUsageColl.FindByParts(eBaseOrTier, eTimeOfUse, eSeason)

        If (Not objUsage Is Nothing) Then
            bRetval = True
        End If

        Return (bRetval)
    End Function

    Private Function TotalServiceUseExistsWithOptionalDemandOnly(ByVal objUsageColl As UsageCollection) As Boolean
        Dim bRetval As Boolean = True
        Dim objUsage As Usage
        Dim bTotalUseExists As Boolean

        For Each objUsage In objUsageColl
            If (objUsage.BaseOrTier = BaseOrTier.TotalServiceUse And objUsage.TimeOfUse = TimeOfUse.Undefined And objUsage.Season = Season.Undefined) Then
                bTotalUseExists = True
            End If

            If (objUsage.BaseOrTier <> BaseOrTier.TotalServiceUse And objUsage.BaseOrTier <> BaseOrTier.Demand And objUsage.BaseOrTier <> BaseOrTier.DemandBill) Then
                bRetval = False
                Exit For
            Else
                If (objUsage.BaseOrTier = BaseOrTier.TotalServiceUse And (objUsage.TimeOfUse <> TimeOfUse.Undefined Or objUsage.Season <> Season.Undefined)) Then
                    bRetval = False
                    Exit For
                End If
            End If
        Next

        If (Not bTotalUseExists) Then bRetval = False

        Return (bRetval)
    End Function

    Private Function FinalizeMasterCosts(ByVal objMaster As CostResult, ByVal objMin As CostResult, ByVal objRateClassModifiers As RateClassModifierCollection) As CostResult
        Dim objCost As Cost
        Dim objMinCost As Cost
        Dim objMasCost As Cost
        Dim objDE As DictionaryEntry
        Dim dblTotalCost As Double = 0.0
        Dim bRecalc As Boolean = False
        Dim bFullReplacement As Boolean = True  ' min charges handling will have more options in the future
        Dim dblDiscountBeforeTaxFactor As Double = 0.0

        ' simply use this to set a property indicating special nmr of nem3,nem4, or nem5
        objMaster.NetMeteringType = objRateClassModifiers.GetModifierNMR()

        If (objMaster.IsNetMetering) Then
            ' 14.09 Bug 53743 - set up the total cost before min cost is applied
            Dim objCostResultBeforeMinimumCostApplied As New CostResult
            objCostResultBeforeMinimumCostApplied.TotalCost = objMaster.TotalCost
            objCostResultBeforeMinimumCostApplied.TotalUse = objMaster.TotalUse
            objCostResultBeforeMinimumCostApplied.CostCollection = objMaster.CostCollection.Copy()
            objCostResultBeforeMinimumCostApplied.NetMeteringType = objMaster.NetMeteringType
            objCostResultBeforeMinimumCostApplied = FinalizedCostWithRateClassModifider(objCostResultBeforeMinimumCostApplied, objRateClassModifiers)
            objMaster.TotalCostBeforeMinimumCostsApplied = objCostResultBeforeMinimumCostApplied.TotalCost
        End If

        ' Only attempt work if min collection contains costs
        If (objMin.CostCollection.Count > 0) Then

            'Bug54232
            'Minimum charges in collection therefore minumum charges configured.
            objMaster.MinimumChargesConfigured = True

            objMaster.CostCollectionWithMinCost = New CostCollection()
            objMaster.MinimumCharge = objMin.TotalCost

            'Bug54232
            'Populate "cost collection with minimum cost" with minumum cost.
            For Each objDE In objMin.CostCollection

                objMinCost = CType(objDE.Value, Cost)
                'Bug 55008 Fix -Included If Condition
                If Not objMaster.CostCollectionWithMinCost.Contains(objMinCost.CostType) Then
                    objMaster.CostCollectionWithMinCost.Add(objMinCost.CostType, objMinCost)
                End If
                'objMaster.CostCollectionWithMinCost.Add(objMinCost.CostType, objMinCost)

            Next
            'Bug54232
            'Populate "cost collection with minimum cost" with cost.
            For Each objDE In objMaster.CostCollection

                objMasCost = CType(objDE.Value, Cost)
                'Bug 55008 Fix -Included If Condition
                If Not objMaster.CostCollectionWithMinCost.Contains(objMasCost.CostType) Then
                    objMaster.CostCollectionWithMinCost.Add(objMasCost.CostType, objMasCost)
                End If
            Next

            If (bFullReplacement) Then

                'Entire set of costs replaced
                'If the master result does not meet or exceed the minimum cost total, then clear the Master, and repopulate with the minimum result
                If (objMaster.TotalCost < objMin.TotalCost) Then

                    objMaster.CostCollection.Clear()
                    Log("The cost result does not exceed the authored minimum.  Minimum costs will be used.")

                    For Each objDE In objMin.CostCollection
                        objMinCost = CType(objDE.Value, Cost)
                        objMaster.CostCollection.Add(objMinCost.CostType, objMinCost)
                        Log("...Added from MinimumCost", objMinCost.CostType)
                    Next

                    bRecalc = True

                End If

            Else

                ' Each cost is replaced when cost types match; for use again in future, will be applied first
                For Each objDE In objMin.CostCollection
                    objMinCost = CType(objDE.Value, Cost)

                    If (objMaster.CostCollection.Contains(objMinCost.CostType)) Then
                        objCost = objMaster.CostCollection.Item(objMinCost.CostType)
                        If (objCost.Amount < objMinCost.Amount) Then
                            ' Remove the Cost and add the new min cost which is bigger
                            objMaster.CostCollection.Remove(objMinCost.CostType)
                            objMaster.CostCollection.Add(objMinCost.CostType, objMinCost)
                            Log("...Replacement from MinimumCost", objMinCost.CostType)
                            bRecalc = True
                        End If
                    End If
                Next

            End If

            ' Recalculate the TotalCost of collection
            If (bRecalc) Then

                objMaster.MinimumCostsApplied = True

                objMaster = FinalizeTotalCost(objMaster)
            End If

        End If

        ' apply rate class modifier to finalize cost
        objMaster = FinalizedCostWithRateClassModifider(objMaster, objRateClassModifiers)

        Return (objMaster)
    End Function

    Private Function FinalizedCostWithRateClassModifider(ByVal objMaster As CostResult, ByVal objRateClassModifiers As RateClassModifierCollection) As CostResult
        'CR 20298, 20299, 20302 Oct 2011 - remove cost_type with supp_ when isRetailSupply is true, and re-calc the total cost
        If CType(objRateClassModifiers.Find(RateClassModifierType.IsRetailSupply).Value, Boolean) Then
            objMaster = RemoveSuppCostType(objMaster)
        End If

        'Apply rate class modifier - exclude cost types.
        Dim objRateClassModifierExcludeCostTypes As RateClassModifierCollection
        objRateClassModifierExcludeCostTypes = objRateClassModifiers.GetRateClassModifierList(RateClassModifierType.ExcludeCostTypes)
        objMaster = ApplyRateClassModifierExcludeCostTypes(objMaster, objRateClassModifierExcludeCostTypes)

        'CR56757 - Apply rate class modifier - exclude cost types for discount before dbtf and datf computations, but include in total cost calc
        Dim objRateClassModifierDiscountExcludeCostType As RateClassModifierCollection
        'Get a copy of cost collection
        Dim objAllCostTypes As CostCollection = objMaster.CostCollection.Copy()
        'Get the list of discount cost types to be excluded
        objRateClassModifierDiscountExcludeCostType = objRateClassModifiers.GetRateClassModifierList(RateClassModifierType.DiscountExcludeCostTypes)

        'CR56757 - Exclude/remove the cost types mentioned as 'dexclct' ratehelper for discount calculation - dbtf
        If (objRateClassModifierDiscountExcludeCostType.Count > 0) Then
            objMaster = ApplyRateClassModifierExcludeCostTypes(objMaster, objRateClassModifierDiscountExcludeCostType)
        End If

        ' Discount Before Tax Factor
        Dim objDiscountBeforeTaxFactor As RateClassModifierCollection
        objDiscountBeforeTaxFactor = objRateClassModifiers.GetRateClassModifierList(RateClassModifierType.DiscountBeforeTaxFactor)
        objMaster = ApplyDiscountBeforeTax(objMaster, objDiscountBeforeTaxFactor)

        'CR56757 - Include the discount excluded cost types for calculating total cost again after discount calculation - dbtf
        If (objRateClassModifierDiscountExcludeCostType.Count > 0) Then
            objMaster = IncludeExcludedCostTypesAndCalculateTotalCost(objMaster, objAllCostTypes)
        End If

        'Apply rate class modifier - cost adjustment(s).
        Dim objRateClassModifierCostAdjustment As RateClassModifierCollection
        objRateClassModifierCostAdjustment = objRateClassModifiers.GetRateClassModifierList(RateClassModifierType.BTDAdjustmentAmount)
        objMaster = ApplyRateClassModifierCostAdjustment(objMaster, objRateClassModifierCostAdjustment)

        ' Tax Cost Factor
        Dim objTaxCostFactorList As RateClassModifierCollection
        objTaxCostFactorList = objRateClassModifiers.GetRateClassModifierList(RateClassModifierType.TaxCostFactor)
        objMaster = ApplyTaxCost(objMaster, objTaxCostFactorList)

        'CR56757 -  Exclude/remove the cost types mentioned as 'dexclct' ratehelper for discount calculation - datf
        If (objRateClassModifierDiscountExcludeCostType.Count > 0) Then
            objAllCostTypes = objMaster.CostCollection.Copy()
            objMaster = ApplyRateClassModifierExcludeCostTypes(objMaster, objRateClassModifierDiscountExcludeCostType)
        End If

        ' Discount After Tax Factor
        Dim objDiscountAfterTaxFactor As RateClassModifierCollection
        objDiscountAfterTaxFactor = objRateClassModifiers.GetRateClassModifierList(RateClassModifierType.DiscountAfterTaxFactor)
        objMaster = ApplyDiscountAfterTax(objMaster, objDiscountAfterTaxFactor)

        'CR56757 - Include the discount excluded cost types for calculating total cost again after discount calculation - datf
        If (objRateClassModifierDiscountExcludeCostType.Count > 0) Then
            objMaster = IncludeExcludedCostTypesAndCalculateTotalCost(objMaster, objAllCostTypes)
        End If

        Return objMaster
    End Function


    ''' <summary>
    ''' Create dynamic master child rate class relationships.
    ''' </summary>
    Private Function CreateDynamicMasterChild(ByVal nRateCompanyID As Integer,
                                              ByVal masterID As Integer,
                                              ByVal sChildRateClass As String,
                                              ByVal dtStartDate As DateTime,
                                              ByVal dtEndDate As DateTime) As MasterChildCollection

        Dim objResult As MasterChildCollection = Nothing
        Dim objMasterChild As MasterChild = Nothing
        Dim objChildDetailedRateDefinitionCollection As DetailedRateDefinitionCollection
        Dim eLoadType As LoadType
        Dim nMasterID As Integer
        Dim nChildID As Integer

        Try

            objResult = New MasterChildCollection

            'Validate input parameter: Child rate class.
            If (String.IsNullOrEmpty(sChildRateClass) = True) Then
                Log(String.Format("...Dynamic master child NOT created. Child rate class name invalid. (ChildRateClass name: {0})",
                                  sChildRateClass))
                Return objResult
            End If

            objChildDetailedRateDefinitionCollection = GetDetailedRateInformation(nRateCompanyID, sChildRateClass, dtStartDate, dtEndDate)

            'Could not retrieve child rate definition.
            If (objChildDetailedRateDefinitionCollection.Count <= 0) Then
                Log(String.Format("...Dynamic master child NOT created. . Could not retrieve child detailed rate definition. (ChildRateClass name: {0})",
                                  sChildRateClass))
                Return objResult
            End If

            'Create master/child relationship.
            nMasterID = masterID
            nChildID = objChildDetailedRateDefinitionCollection(0).MasterID
            eLoadType = objChildDetailedRateDefinitionCollection(0).LoadType
            objMasterChild = New MasterChild(nMasterID, nChildID, eLoadType, sChildRateClass)

            objResult.Add(objMasterChild)

        Catch

        End Try

        Return objResult

    End Function


    ''' <summary>
    ''' Finalize the total cost.
    ''' </summary>
    ''' <param name="objMaster"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function FinalizeTotalCost(ByVal objMaster As CE.RateEngine.CostResult) As CE.RateEngine.CostResult
        Dim objDE As DictionaryEntry
        Dim objCost As Cost
        Dim dblTotalCost As Double = 0.0
        For Each objDE In objMaster.CostCollection
            objCost = CType(objDE.Value, Cost)
            dblTotalCost = dblTotalCost + objCost.Amount
        Next
        objMaster.TotalCost = dblTotalCost

        Return (objMaster)
    End Function

    Private Function ProcessMinimumCharges(ByVal objUsageColl As UsageCollection, ByVal objCostColl As CostCollection, ByVal objDef As DetailedRateDefinition, ByVal nMeters As Integer, ByVal nDwellingUnits As Integer, ByVal dblTotalUse As Double, ByVal nBillDays As Integer, ByVal dblProrateFactor As Double, ByVal minimumChargeType As MinimumChargeType, ByVal monthlyProrateFactor As Double) As CostCollection
        Dim ksFunctionName As String = "ProcessMinimumCharges"
        Dim ksMinimumChargeProrateUsed As String = "Minimum charge prorate used"
        Dim ksMinimumChargeOmitted As String = "Minimum charges omitted, MinimumChargeType = NoMinimumCharge"
        Dim objMinCharge As MinimumCharge
        Dim dblResult As Double
        Dim dblOp As Double
        Dim skipAdd As Boolean = False

        For Each objMinCharge In objDef.MinimumCharges

            Select Case objMinCharge.CalculationType
                Case ChargeCalcType.PerMeterPerDay
                    dblOp = nBillDays * nMeters
                    dblResult = objMinCharge.ChargeValue * dblOp

                Case ChargeCalcType.AvgDailyKWh
                    dblOp = dblTotalUse / nBillDays
                    dblResult = objMinCharge.ChargeValue * (dblOp)

                Case ChargeCalcType.Daily
                    dblOp = nBillDays
                    dblResult = objMinCharge.ChargeValue * dblOp

                Case ChargeCalcType.Monthly
                    dblOp = 1.0
                    If (minimumChargeType = Enums.MinimumChargeType.ProrateMinimumCharge) Then
                        dblOp = monthlyProrateFactor
                        Log(ksFunctionName, ksMinimumChargeProrateUsed)
                    End If

                    dblResult = objMinCharge.ChargeValue * dblOp

                Case ChargeCalcType.PerDwellingUnitPerDay
                    dblOp = nBillDays * nDwellingUnits
                    dblResult = objMinCharge.ChargeValue * dblOp

                Case Else
                    'none, same as monthly
                    dblOp = 1.0
                    dblResult = objMinCharge.ChargeValue

            End Select


            ' Apply overall prorate in FinalizeCost;  dblResult = dblResult * dblProrateFactor
            ' Only add minimum charges to result when argument indicates not to skip
            If (minimumChargeType <> Enums.MinimumChargeType.NoMinimumCharge) Then
                objCostColl.SumAdd(objMinCharge.CostType, New Cost(objMinCharge.CostType, dblResult))
                Log(ksFunctionName, dblResult, objMinCharge.ChargeValue, dblOp, objMinCharge.CostType)
            Else
                Log(ksFunctionName, ksMinimumChargeOmitted)
            End If

        Next

        Return (objCostColl)
    End Function

    Private Function FinalizeCosts(ByVal objCostColl As CostCollection, ByVal objDef As DetailedRateDefinition, ByVal dblProrateFactor As Double) As CostResult
        Dim ksFunctionNameA As String = "FinalizeCostsPR"
        Dim ksFunctionNameB As String = "FinalizeCostsTAX(+)"
        Dim objCostResult As New CostResult
        Dim objCost As Cost
        Dim objDE As DictionaryEntry
        Dim dblTotalCost As Double
        Dim dblTemp As Double

        For Each objDE In objCostColl
            objCost = CType(objDE.Value, Cost)
            ' Apply ProrateFactor to each cost amount in collection
            dblTemp = objCost.Amount
            objCost.Amount = objCost.Amount * dblProrateFactor
            Log(ksFunctionNameA, objCost.Amount, dblTemp, dblProrateFactor, objCost.CostType)

            'Apply potential tax charges to this cost if tax charges exist
            If (objDef.TaxCharges.Count > 0) Then
                Dim objTaxCharge As TaxCharge
                objTaxCharge = objDef.TaxCharges.FindByCostType(objCost.CostType)
                If (Not objTaxCharge Is Nothing) Then
                    ' Tax charge exists for CostType
                    ' Add the existing amount to calculated tax cost
                    dblTemp = objCost.Amount
                    objCost.Amount = objCost.Amount + (objCost.Amount * objTaxCharge.ChargeValue)
                    Log(ksFunctionNameB, objCost.Amount, dblTemp, objTaxCharge.ChargeValue, objCost.CostType)
                End If
            End If

            dblTotalCost = dblTotalCost + objCost.Amount
        Next

        objCostResult.TotalCost = dblTotalCost
        objCostResult.CostCollection = objCostColl

        Return (objCostResult)
    End Function

    Private Function CombineResults(ByVal objMaster As CostResult, ByVal objCostResult As CostResult) As CostResult
        Dim ksFunctionNameA As String = "CombineCostsResult"
        Dim objCost As Cost
        Dim objDE As DictionaryEntry

        For Each objDE In objCostResult.CostCollection
            objCost = CType(objDE.Value, Cost)
            objMaster.CostCollection.SumAdd(objCost.CostType, objCost)
        Next

        objMaster.TotalCost = objMaster.TotalCost + objCostResult.TotalCost

        Return (objMaster)
    End Function

    Private Function ProcessServiceCharges(ByVal rateClassModifiers As RateClassModifierCollection, ByVal objUsageColl As UsageCollection, ByVal objCostColl As CostCollection, ByVal objDef As DetailedRateDefinition, ByVal nMeters As Integer, ByVal nDwellingUnits As Integer, ByVal dblTotalUse As Double, ByVal nBillDays As Integer, ByVal dtStartDate As DateTime, ByVal dtEndDate As DateTime, ByVal bFinalDefinition As Boolean, ByVal dblProrateFactor As Double, ByVal bUseSeasonProrateOverride As Boolean, ByVal eOverrideSeason As Season, ByVal objSeasonFactors As SeasonFactorCollection, ByVal prorateMonthlyCharges As Boolean, prorateDemandCharges As Boolean, ByVal monthlyProrateFactor As Double) As CostCollection
        Const ksFunctionName As String = "ProcessServiceCharges"
        Const ksCRCDemandCharge As String = "ProcessServiceCharges-Special-CRCDemandCharge"
        Const ksCRCExcesskWh As String = "ProcessServiceCharges-Special-CRCExcesskWh"
        Const ksNCDemandCharge As String = "ProcessServiceCharges-Special-NCDemandCharge"
        Const ksNCDemandNote As String = "Rate Helper ncdm is not present or zero"
        Const ksNoCRKWNote As String = "Rate Helper crkw is not present"
        Const ksMonthlyProrateUsed As String = "Monthly prorate factor used"
        Const ksDemandProrateUsed As String = "Demand prorate factor used"
        Const ksCPDemandCharge As String = "ProcessServiceCharges-Special-CPDemandCharge"
        Dim objServiceCharge As ServiceCharge
        Dim objServiceCharges As ServiceChargeCollection
        Dim objSubServiceCharges As ServiceChargeCollection
        Dim objSeasonFactor As SeasonFactor
        Dim objDE As DictionaryEntry
        Dim dblResult As Double
        Dim dblOp As Double
        Dim bRemoveSF As Boolean
        Dim skip As Boolean
        Dim skipMainLog As Boolean

        ' Only if not present already, add an undefined season factor to handle service charges that are not seasonal
        If (Not objSeasonFactors.Contains(Season.Undefined)) Then
            objSeasonFactors.Add(Season.Undefined, New SeasonFactor(Season.Undefined, 1.0))
            bRemoveSF = True
        End If

        ' If Service Charges are stepped, be sure to grab correct service charges first
        If (Not objDef.IsStepped) Then
            objServiceCharges = objDef.ServiceCharges
        Else
            objServiceCharges = objDef.ServiceCharges.GetNonStepped()
        End If

        ' Loop through available seasons;  might be that it is essentially non-seasonal, whereby only the undefined factor is present
        For Each objDE In objSeasonFactors
            objSeasonFactor = CType(objDE.Value, SeasonFactor)

            objSubServiceCharges = objServiceCharges.FindByParts(ServiceStep.Undefined, objSeasonFactor.Season)

            ' Loop through subset of Regular Service Charges
            For Each objServiceCharge In objSubServiceCharges

                skip = False
                skipMainLog = False

                Select Case objServiceCharge.CalculationType
                    Case ChargeCalcType.PerMeterPerDay
                        dblOp = nBillDays * nMeters
                        dblResult = objServiceCharge.ChargeValue * dblOp

                    Case ChargeCalcType.AvgDailyKWh
                        dblOp = dblTotalUse / nBillDays
                        dblResult = objServiceCharge.ChargeValue * dblOp

                    Case ChargeCalcType.Daily
                        dblOp = nBillDays
                        dblResult = objServiceCharge.ChargeValue * dblOp

                    Case ChargeCalcType.Monthly
                        dblOp = 1.0
                        If (prorateMonthlyCharges) Then
                            dblOp = monthlyProrateFactor
                            Log(ksFunctionName, ksMonthlyProrateUsed)
                        End If

                        dblResult = objServiceCharge.ChargeValue * dblOp

                    Case ChargeCalcType.PerDwellingUnitPerDay
                        dblOp = nBillDays * nDwellingUnits
                        dblResult = objServiceCharge.ChargeValue * dblOp

                    Case ChargeCalcType.PTRRebateEachEvent, ChargeCalcType.PTRRebateEntireBillPeriod
                        ' These are used just for PeakTimeRebates, and to store a rebate amount
                        ' This is not a true service charges, and should not be added to the cost collection below
                        skip = True

                    Case ChargeCalcType.AnnExcGen, ChargeCalcType.AnnExcGenP, ChargeCalcType.AnnExcGenOP, ChargeCalcType.AnnExcGenS1, ChargeCalcType.AnnExcGenS2, ChargeCalcType.AnnExcGenCP
                        ' Just used for Net Metering (for excess generation); nothing to do here, should not be added
                        skip = True

                    Case ChargeCalcType.MonthlySeasonAdjustedAVGU
                        ' This is used for average usage passed via a rate helper, with actual consumption factor, and days if usage;  AVGU, ACFU, DAYU
                        Dim avgu As Double = rateClassModifiers.GetModifierAVGU()
                        Dim acfu As Double = rateClassModifiers.GetModifierACFU()
                        Dim dayu As Integer = rateClassModifiers.GetModifierDAYU()
                        avgu = avgu * dayu
                        Dim pct_of_total As Double = dblTotalUse * acfu
                        Dim min1 As Double = Math.Min(dblTotalUse, avgu)
                        Dim max2 As Double = Math.Max(min1, pct_of_total)

                        dblOp = max2
                        dblResult = objServiceCharge.ChargeValue * dblOp

                    Case ChargeCalcType.CRCDemandCharge
                        ' This uses rate helpers
                        skipMainLog = True

                        If (rateClassModifiers.GetModifierIDRE()) Then

                            If (objDef.IsCapacityReserve) Then

                                Dim crkw As Double? = rateClassModifiers.GetModifierCRKW()

                                ' crkw must be passed for crc customers; if not, do not continue here
                                If (Not crkw Is Nothing) Then
                                    If (prorateDemandCharges) Then
                                        dblOp = monthlyProrateFactor
                                        Log(ksCRCDemandCharge, String.Format(" {0} {1}", ksDemandProrateUsed, dblOp.ToString))
                                    Else
                                        dblOp = 1.0
                                    End If


                                    Dim crkwc As Double = rateClassModifiers.GetModifierCRKWC()

                                    ' if no rate helper, use authored value instead
                                    If (crkwc = 0.0) Then
                                        crkwc = objServiceCharge.ChargeValue
                                    End If

                                    dblResult = crkw.Value * crkwc * dblOp
                                    If (prorateDemandCharges) Then
                                        Log(ksCRCDemandCharge, dblResult * objSeasonFactor.Factor, crkwc, crkw.Value, objSeasonFactor.Factor, dblOp, objServiceCharge.CostType)
                                    Else
                                    Log(ksCRCDemandCharge, dblResult * objSeasonFactor.Factor, crkwc, crkw.Value, objSeasonFactor.Factor, objServiceCharge.CostType)
                                    End If


                                Else
                                    Log(ksCRCDemandCharge, ksNoCRKWNote)
                                    skip = True
                                End If
                            Else
                                skip = True
                            End If
                        Else
                            skip = True
                        End If

                    Case ChargeCalcType.CRCExcessKWh
                        ' This uses rate helpers
                        skipMainLog = True

                        If (rateClassModifiers.GetModifierIDRE()) Then

                            If (objDef.IsCapacityReserve) Then

                                Dim crkw As Double? = rateClassModifiers.GetModifierCRKW()

                                ' crkw must be passed for crc customers; if not, do not continue here; crkw not used in calculation here 
                                If (Not crkw Is Nothing) Then
                                    Dim crkwhc As Double = rateClassModifiers.GetModifierCRKWHC()
                                    Dim criticalPeakUsage As Double

                                    ' if no rate helper, use authored value instead
                                    If (crkwhc = 0.0) Then
                                        crkwhc = objServiceCharge.ChargeValue
                                    End If

                                    criticalPeakUsage = objUsageColl.GetTotalTimeOfUseUsageForSeason(TimeOfUse.CriticalPeak, objSeasonFactor.Season)

                                    'criticalPeakUsage is essentially the excess kWh in the context of Capacity Reserve
                                    dblResult = criticalPeakUsage * crkwhc
                                    Log(ksCRCExcesskWh, dblResult * objSeasonFactor.Factor, crkwhc, criticalPeakUsage, objSeasonFactor.Factor, objServiceCharge.CostType)

                                Else
                                    Log(ksCRCExcesskWh, ksNoCRKWNote)
                                    skip = True
                                End If
                            Else
                                skip = True
                            End If
                        Else
                            skip = True
                        End If

                    Case ChargeCalcType.NCDemandCharge
                        ' This uses rate helpers
                        ' This uses NCDemandMultFactor as well
                        skipMainLog = True

                        Dim ncdm As Double = rateClassModifiers.GetModifierNCDM()

                        If (ncdm <> 0.0) Then
                            If (prorateDemandCharges) Then
                                dblOp = monthlyProrateFactor
                                Log(ksNCDemandCharge, String.Format(" {0} {1}", ksDemandProrateUsed, dblOp.ToString))
                            Else
                                dblOp = 1.0
                            End If

                            Dim nckwc As Double = rateClassModifiers.GetModifierNCKWC()
                            Dim ncdf As Double = rateClassModifiers.GetModifierNCDF()
                            Dim finalDmnd As Double
                            Dim demandBill2 As Double

                            ' if no rate helper, use authored value instead
                            If (nckwc = 0.0) Then
                                nckwc = objServiceCharge.ChargeValue
                            End If

                            ' if no rate helper, use authored value instead
                            If (ncdf = 0.0) Then
                                ncdf = objSubServiceCharges.FindNCDemandFactor(objServiceCharge.Season, ChargeCalcType.NCDemandMultFactor)
                            End If

                            ' the usage determinant used here is DemandBill2, essentially the eligible non-coincident demand (already passed the forgiveness criteria)
                            demandBill2 = objUsageColl.GetTotalDemandBill2Usage(objSeasonFactor.Season)

                            finalDmnd = Math.Max(ncdm * ncdf, demandBill2)
                            dblResult = finalDmnd * nckwc * dblOp
                            If (prorateDemandCharges) Then
                                Log(ksNCDemandCharge, dblResult * objSeasonFactor.Factor, nckwc, finalDmnd, objSeasonFactor.Factor, dblOp, objServiceCharge.CostType)
                            Else
                            Log(ksNCDemandCharge, dblResult * objSeasonFactor.Factor, nckwc, finalDmnd, objSeasonFactor.Factor, objServiceCharge.CostType)
                            End If

                        Else
                            Log(ksNCDemandCharge, ksNCDemandNote)
                            skip = True
                        End If

                    Case ChargeCalcType.NCDemandMultFactor
                        ' Used for NCDemandCharge processing above, there is no independent logic here.
                        skip = True

                        '--- CR50560 BEGIN -------------------------------------------------------------------------------------------
                    Case ChargeCalcType.CPDemandCharge
                        If (prorateDemandCharges) Then
                            dblOp = monthlyProrateFactor
                            Log(ksCPDemandCharge, String.Format(" {0} {1}", ksDemandProrateUsed, dblOp.ToString))
                        Else
                            dblOp = 1.0
                        End If

                        Dim demandBill3Value As Double

                        Dim cpkwc As Double = rateClassModifiers.GetModifierCPKWC()

                        If (cpkwc = 0.0) Then
                            cpkwc = objSubServiceCharges.FindCDemandFactor(objServiceCharge.Season, ChargeCalcType.CPDemandCharge)
                        End If

                        demandBill3Value = objUsageColl.GetTotalDemandBill3Usage(objSeasonFactor.Season)

                        dblResult = demandBill3Value * cpkwc * dblOp

                        If (prorateDemandCharges) Then
                            Log(ksCPDemandCharge, dblResult * objSeasonFactor.Factor, cpkwc, objSeasonFactor.Factor, dblOp, objServiceCharge.CostType)
                        Else
                            Log(ksCPDemandCharge, dblResult * objSeasonFactor.Factor, cpkwc, objSeasonFactor.Factor, objServiceCharge.CostType)
                        End If

                        '--- CR50560 END ---------------------------------------------------------------------------------------------

                    Case Else
                        'none, same as monthly
                        dblOp = 1.0
                        dblResult = objServiceCharge.ChargeValue

                End Select

                If (Not skip) Then
                    dblResult = dblResult * objSeasonFactor.Factor  ' Adjust for SeasonFactor
                    objCostColl.SumAdd(objServiceCharge.CostType, New Cost(objServiceCharge.CostType, dblResult))

                    If (Not skipMainLog) Then
                        Log(ksFunctionName, dblResult, objServiceCharge.ChargeValue, dblOp, objSeasonFactor.Factor, objServiceCharge.CostType)
                    End If

                End If

            Next

        Next

        ' Stepped Service Charges
        ' Unlike tiers with UseCharges, not all stepped charges are applied for ServiceCharges; 
        ' Must determine which step to use for Monthly, Daily, and DiffStep calculation types;
        ' Note that only one set of Stepped Charges can exist at one time
        If (objDef.IsStepped) Then
            Dim eCalcType As ChargeCalcType
            Dim objStepBoundary As StepBoundary
            Dim dblDemandBill As Double
            Dim objServiceChargesForStep As ServiceChargeCollection
            Dim objCharge As ServiceCharge

            eCalcType = objDef.ServiceCharges.GetSteppedCalcType()

            dblResult = 0.0
            Select Case eCalcType
                Case ChargeCalcType.SteppedMonthlyDemandBill
                    ' Loop through available seasons;  might be that it is essentially non-seasonal, whereby only the undefined factor is present
                    For Each objDE In objSeasonFactors
                        objSeasonFactor = CType(objDE.Value, SeasonFactor)

                        dblDemandBill = objUsageColl.GetTotalDemandBillUsage(objSeasonFactor.Season)
                        objStepBoundary = objDef.StepBoundaries.FindByParts(objSeasonFactor.Season).FindInclusiveBoundary(dblDemandBill)
                        If (Not objStepBoundary Is Nothing) Then
                            objServiceChargesForStep = objDef.ServiceCharges.FindByParts(objStepBoundary.Step, objSeasonFactor.Season, eCalcType)
                            For Each objCharge In objServiceChargesForStep
                                dblOp = 1.0
                                If (prorateMonthlyCharges) Then
                                    dblOp = monthlyProrateFactor
                                    Log(ksFunctionName, ksMonthlyProrateUsed)
                                End If
                                dblResult = objCharge.ChargeValue * dblOp
                                dblResult = dblResult * objSeasonFactor.Factor
                                objCostColl.SumAdd(objCharge.CostType, New Cost(objCharge.CostType, dblResult))
                                Log(ksFunctionName, objStepBoundary.Step, dblResult, objCharge.ChargeValue, dblOp, objSeasonFactor.Factor, objCharge.CostType)
                            Next
                        End If

                    Next

                Case ChargeCalcType.SteppedDailyDemandBill
                    ' Loop through available seasons;  might be that it is essentially non-seasonal, whereby only the undefined factor is present
                    ' per day DemandBill may have to be estimated/calculated
                    For Each objDE In objSeasonFactors
                        objSeasonFactor = CType(objDE.Value, SeasonFactor)

                        dblDemandBill = objUsageColl.GetTotalDemandBillUsage(objSeasonFactor.Season)
                        If (dblDemandBill = 0) Then
                            dblDemandBill = objUsageColl.GetTotalUsage() / nBillDays
                        End If
                        objStepBoundary = objDef.StepBoundaries.FindByParts(objSeasonFactor.Season).FindInclusiveBoundary(dblDemandBill)
                        If (Not objStepBoundary Is Nothing) Then
                            objServiceChargesForStep = objDef.ServiceCharges.FindByParts(objStepBoundary.Step, objSeasonFactor.Season, eCalcType)
                            For Each objCharge In objServiceChargesForStep
                                dblOp = nBillDays
                                dblResult = objCharge.ChargeValue * dblOp
                                dblResult = dblResult * objSeasonFactor.Factor
                                objCostColl.SumAdd(objCharge.CostType, New Cost(objCharge.CostType, dblResult))
                                Log(ksFunctionName, objStepBoundary.Step, dblResult, objCharge.ChargeValue, dblOp, objSeasonFactor.Factor, objCharge.CostType)
                            Next
                        End If

                    Next

                Case ChargeCalcType.SteppedDiffStepDemandBill, ChargeCalcType.SteppedDiffStepDemand
                    Dim objSubStepBoundaries As StepBoundaryCollection
                    ' Loop through available seasons;  might be that it is essentially non-seasonal, whereby only the undefined factor is present

                    For Each objDE In objSeasonFactors
                        objSeasonFactor = CType(objDE.Value, SeasonFactor)

                        If eCalcType = ChargeCalcType.SteppedDiffStepDemandBill Then
                            dblDemandBill = objUsageColl.GetTotalDemandBillUsage(objSeasonFactor.Season)
                        ElseIf eCalcType = ChargeCalcType.SteppedDiffStepDemand Then
                            dblDemandBill = objUsageColl.GetTotalDemandUsage(objSeasonFactor.Season)
                        End If

                        objSubStepBoundaries = objDef.StepBoundaries.FindByParts(objSeasonFactor.Season)
                        If (objSubStepBoundaries.Count > 0) Then objSubStepBoundaries.Sort()
                        objStepBoundary = objSubStepBoundaries.FindInclusiveBoundary(dblDemandBill)
                        If (Not objStepBoundary Is Nothing) Then
                            objServiceChargesForStep = objDef.ServiceCharges.FindByParts(objStepBoundary.Step, objSeasonFactor.Season, eCalcType)
                            If (objStepBoundary.Step = ServiceStep.Step1) Then
                                dblOp = 1.0
                            Else
                                dblOp = dblDemandBill - objSubStepBoundaries.GetByStep(CType(CInt(objStepBoundary.Step) - 1, ServiceStep)).Threshold
                            End If
                            For Each objCharge In objServiceChargesForStep
                                dblResult = objCharge.ChargeValue * dblOp
                                dblResult = dblResult * objSeasonFactor.Factor
                                objCostColl.SumAdd(objCharge.CostType, New Cost(objCharge.CostType, dblResult))
                                Log(ksFunctionName, objStepBoundary.Step, dblResult, objCharge.ChargeValue, dblOp, objSeasonFactor.Factor, objCharge.CostType)
                            Next
                        End If

                    Next

                Case Else
                    ' skip
            End Select

        End If

        ' Remove the undefined season factor that was added above;  subsequent operations might not want this present
        If (bRemoveSF) Then objSeasonFactors.Remove(Season.Undefined)

        Return (objCostColl)
    End Function

    ''' <summary>
    ''' New initializer.
    ''' </summary>
    ''' <param name="objUsageColl"></param>
    ''' <param name="objCostResult"></param>
    ''' <param name="nBillDays"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function SumNonDemandUsagesAndInitCostResult(ByVal objUsageColl As UsageCollection, ByVal objCostResult As CostResult, ByVal nBillDays As Integer) As CostResult
        Dim objUsage As Usage
        Dim dblTotal As Double = 0.0
        Dim bTotalExists As Boolean
        Dim tieredExists As Boolean = False
        Dim nonTieredExists As Boolean = False
        Dim computeTotal As Boolean = False

        For Each objUsage In objUsageColl
            ' Be sure the part is not TotalServiceUse already, to avoid erroneously adding a total to collection
            'If (objUsage.BaseOrTier = BaseOrTier.TotalServiceUse AndAlso objUsage.TimeOfUse = TimeOfUse.Undefined AndAlso objUsage.Season = Season.Undefined) Then
            If (objUsage.BaseOrTier = BaseOrTier.TotalServiceUse AndAlso objUsage.TimeOfUse = TimeOfUse.Undefined) Then

                bTotalExists = True
                Exit For
            Else
                ' Be sure the parts are not Demand parts
                If objUsage.BaseOrTier <> BaseOrTier.Demand And objUsage.BaseOrTier <> BaseOrTier.DemandBill Then
                    dblTotal = dblTotal + objUsage.Quantity
                End If

                ' set these parameters as iterations continue; do not break out after this point, since a total still may exist at the end!
                If (objUsage.BaseOrTier <> BaseOrTier.TotalServiceUse And objUsage.BaseOrTier <> BaseOrTier.Demand) Then
                    tieredExists = True
                End If

                If (objUsage.BaseOrTier = BaseOrTier.Undefined And objUsage.BaseOrTier <> BaseOrTier.Demand And objUsage.TimeOfUse <> TimeOfUse.Undefined) Then
                    nonTieredExists = True
                End If

                If (tieredExists = True And nonTieredExists) Then
                    computeTotal = True
                End If
            End If
        Next

        If (bTotalExists) Then
            'might have been out-of-order above; recalculate and set local variable here.
            dblTotal = objUsageColl.GetTotalServiceUse()    'this handles seasonal and non-seasonal if they exist at the same time.
        End If

        If (Not bTotalExists And computeTotal) Then
            dblTotal = objUsageColl.GetTieredTotalUsage()
        End If

        If (nBillDays = 0) Then nBillDays = 1

        objCostResult.TotalUse = dblTotal
        objCostResult.AvgDailyEnergyUse = dblTotal / nBillDays
        objCostResult.BillDays = nBillDays

        Return (objCostResult)
    End Function

    ''' <summary>
    ''' Original function to help initialize cost result.  Replaced.
    ''' </summary>
    ''' <param name="objUsageColl"></param>
    ''' <param name="objCostResult"></param>
    ''' <param name="nBillDays"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function SumNonDemandUsagesAndInitCostResult_Original(ByVal objUsageColl As UsageCollection, ByVal objCostResult As CostResult, ByVal nBillDays As Integer) As CostResult
        Dim objUsage As Usage
        Dim overallNonDemandTotal As Double = 0.0
        Dim totalServiceUseTotal As Double = 0.0
        Dim nonDemandUsageCount As Integer = 0
        Dim totalServiceUseTotalExists As Boolean

        For Each objUsage In objUsageColl
            If (objUsage.BaseOrTier = BaseOrTier.TotalServiceUse And objUsage.TimeOfUse = TimeOfUse.Undefined And objUsage.Season = Season.Undefined) Then
                totalServiceUseTotalExists = True
                totalServiceUseTotal = totalServiceUseTotal + objUsage.Quantity
            End If

            If objUsage.BaseOrTier <> BaseOrTier.Demand And objUsage.BaseOrTier <> BaseOrTier.DemandBill Then
                overallNonDemandTotal = overallNonDemandTotal + objUsage.Quantity
                nonDemandUsageCount = nonDemandUsageCount + 1
            End If
        Next

        If (totalServiceUseTotalExists And nonDemandUsageCount > 1) Then
            ' Subtract the extra total from calculated total (since it must have been passed in with parts)
            overallNonDemandTotal = overallNonDemandTotal - totalServiceUseTotal

            ' If the parts fall short, simple consider the total as the overall total, for use below
            If (totalServiceUseTotal > overallNonDemandTotal) Then
                overallNonDemandTotal = totalServiceUseTotal
            End If

        End If

        If (nBillDays = 0) Then nBillDays = 1

        objCostResult.TotalUse = overallNonDemandTotal
        objCostResult.AvgDailyEnergyUse = overallNonDemandTotal / nBillDays
        objCostResult.BillDays = nBillDays

        Return (objCostResult)
    End Function

    Private Function SumNonDemandUsagePartsAndInject(ByVal objUsageColl As UsageCollection) As UsageCollection
        Dim objUsage As Usage
        Dim dblTotal As Double = 0.0
        Dim bTotalExists As Boolean
        Dim tieredExists As Boolean = False
        Dim nonTieredExists As Boolean = False
        Dim computeTotal As Boolean = False

        For Each objUsage In objUsageColl
            ' Be sure the part is not TotalServiceUse already, to avoid erroneously adding a total to collection
            'If (objUsage.BaseOrTier = BaseOrTier.TotalServiceUse AndAlso objUsage.TimeOfUse = TimeOfUse.Undefined AndAlso objUsage.Season = Season.Undefined) Then
            If (objUsage.BaseOrTier = BaseOrTier.TotalServiceUse AndAlso objUsage.TimeOfUse = TimeOfUse.Undefined) Then

                bTotalExists = True
                Exit For
            Else
                ' Be sure the parts are not Demand parts
                If objUsage.BaseOrTier <> BaseOrTier.Demand And objUsage.BaseOrTier <> BaseOrTier.DemandBill Then
                    dblTotal = dblTotal + objUsage.Quantity
                End If

                ' set these parameters as iterations continue; do not break out after this point, since a total still may exist at the end!
                If (objUsage.BaseOrTier <> BaseOrTier.TotalServiceUse And objUsage.BaseOrTier <> BaseOrTier.Demand) Then
                    tieredExists = True
                End If

                If (objUsage.BaseOrTier = BaseOrTier.Undefined And objUsage.BaseOrTier <> BaseOrTier.Demand And objUsage.TimeOfUse <> TimeOfUse.Undefined) Then
                    nonTieredExists = True
                End If

                If (tieredExists = True And nonTieredExists) Then
                    computeTotal = True
                End If
            End If
        Next

        If (Not bTotalExists And computeTotal) Then
            dblTotal = objUsageColl.GetTieredTotalUsage()
        End If

        ' Inject the TotalServiceUse into the Usage Collection so that the parts can trigger 
        ' any potential Use Charges, but only if it doesn't already exist, and usage is greater than zero
        If (Not bTotalExists) Then
            If (dblTotal > 0) Then
                objUsageColl.Add(New Usage(BaseOrTier.TotalServiceUse, dblTotal))
            End If
        End If

        Return (objUsageColl)
    End Function

    Private Function SumNonTieredNonDemandTOUUsagePartsAndReplace(ByVal objUsageColl As UsageCollection) As UsageCollection
        Dim objUsage As Usage
        Dim dblTotal As Double = 0.0
        Dim bTotalExists As Boolean

        For Each objUsage In objUsageColl
            ' Be sure the part is not TotalServiceUse already, to avoid erroneously adding a total to collection
            If (objUsage.BaseOrTier = BaseOrTier.TotalServiceUse And objUsage.TimeOfUse = TimeOfUse.Undefined And objUsage.Season = Season.Undefined) Then
                bTotalExists = True
                Exit For
            Else
                ' Be sure the parts are NOT Demand parts and NOT Tier parts BUT ARE indeed TOU parts
                If objUsage.BaseOrTier <> BaseOrTier.Demand And _
                    objUsage.BaseOrTier <> BaseOrTier.DemandBill And _
                    objUsage.BaseOrTier <> BaseOrTier.Tier1 And _
                    objUsage.BaseOrTier <> BaseOrTier.Tier2 And _
                    objUsage.BaseOrTier <> BaseOrTier.Tier3 And _
                    objUsage.BaseOrTier <> BaseOrTier.Tier4 And _
                    objUsage.BaseOrTier <> BaseOrTier.Tier5 And _
                    objUsage.BaseOrTier <> BaseOrTier.Tier6 And _
                    objUsage.TimeOfUse <> TimeOfUse.Undefined Then

                    dblTotal = dblTotal + objUsage.Quantity

                End If
            End If
        Next

        ' Add the new total; Remove the TOU parts 
        If (Not bTotalExists) Then
            If (dblTotal > 0) Then
                objUsageColl.ClearTOUUsage()
                objUsageColl.Add(New Usage(BaseOrTier.TotalServiceUse, dblTotal))
            End If
        End If

        Return (objUsageColl)
    End Function

    Private Function ProcessProrate(ByVal objDefinitions As DetailedRateDefinitionCollection, ByVal nBillDays As Integer, ByVal dtStartDate As DateTime, ByVal dtEndDate As DateTime, ByRef arrFactors() As Double) As DetailedRateDefinitionCollection
        Dim eProrate As ProrateType
        Dim objDef As DetailedRateDefinition
        Dim dtXDate, dtLastUsedStartDate As DateTime
        Dim i As Integer
        Dim nDays As Integer
        Dim defs As DetailedRateDefinitionCollection    'used when caching is in-play

        If (objDefinitions.Count > 1) Then
            ' Grab Prorate rule from most recent detailed rate definition (this is what we decided as rule)
            ' Most recent is always at index zero
            ' TODO: Review this, perhaps use oldest to match what SeasonalProrate uses ** ObjDef.Item(objDef.Count-1).ProrateType **
            eProrate = objDefinitions.Item(0).ProrateType

            Select Case eProrate
                Case ProrateType.SplitBillDays
                    ' 15.06 Bug 58179 - if both rate definition prorate and season prorate are set to be splite bill days
                    ' then we should use the latest rate definition instead of splite while season prorate remains the same (splite)
                    ' Bug 59081 - Added condition to check for both season and rate change
                    Dim objRateChangeInformation As RateChangeInformation = GetRateChangeInformation(objDefinitions, dtStartDate, dtEndDate, dtStartDate, dtEndDate)
                    If (objDefinitions(0).IsSeasonal AndAlso objDefinitions(0).SeasonalProrateType = SeasonalProrateType.SplitBillDays _
                        AndAlso objRateChangeInformation.RateChange AndAlso objRateChangeInformation.SeasonalRateChange) Then
                        ' use the newest rate definition,  remove the others
                        arrFactors = New Double(0) {1.0}

                        If (m_cachingType = Enums.CachingType.NoCache) Then
                            ' original way 
                            For i = 1 To objDefinitions.Count - 1
                                'always remove at index 1, since it reorders
                                objDefinitions.RemoveAt(1)
                            Next
                        Else
                            ' Create new collection and add newest to this collection;  this insures caching is unaffected 
                            defs = New DetailedRateDefinitionCollection()
                            defs.Add(objDefinitions(0))
                            Return (defs)
                        End If
                    Else
                        arrFactors = New Double(objDefinitions.Count - 1) {}
                        ' use all rate definitions, load the arrFactors
                        ' Must determine arrFactors using date ranges (they will always exist when multiple defs)
                        ' Remember that index 0 is the NEWEST rate definition
                        For Each objDef In objDefinitions
                            If i = 0 Then
                                dtXDate = dtEndDate
                                nDays = dtXDate.Subtract(objDef.StartDate).Days + 1
                                dtLastUsedStartDate = objDef.StartDate
                            Else
                                If (objDef.StartDate < dtStartDate) Then
                                    ' This should be last section processed
                                    dtXDate = dtLastUsedStartDate
                                    nDays = dtXDate.Subtract(dtStartDate).Days
                                Else
                                    dtXDate = dtLastUsedStartDate
                                    nDays = dtXDate.Subtract(objDef.StartDate).Days
                                    dtLastUsedStartDate = objDef.StartDate
                                End If
                            End If

                            arrFactors(i) = nDays / nBillDays
                            i = i + 1
                        Next
                    End If


                Case ProrateType.UseLatest
                    ' use the newest rate definition,  remove the others
                    arrFactors = New Double(0) {1.0}

                    If (m_cachingType = Enums.CachingType.NoCache) Then
                        ' original way 
                        For i = 1 To objDefinitions.Count - 1
                            'always remove at index 1, since it reorders
                            objDefinitions.RemoveAt(1)
                        Next
                    Else
                        ' Create new collection and add newest to this collection;  this insures caching is unaffected 
                        defs = New DetailedRateDefinitionCollection()
                        defs.Add(objDefinitions(0))
                        Return (defs)
                    End If

                Case ProrateType.UsePrevious
                    ' use the oldest rate definition, remove the others
                    arrFactors = New Double(0) {1.0}

                    If (m_cachingType = Enums.CachingType.NoCache) Then
                        ' original way
                        For i = 1 To objDefinitions.Count - 1
                            'always remove at index 0, since it reorders
                            objDefinitions.RemoveAt(0)
                        Next
                    Else
                        ' Create new collection and add oldest to this collection;  this insures caching is unaffected 
                        defs = New DetailedRateDefinitionCollection()
                        defs.Add(objDefinitions(objDefinitions.Count - 1))
                        Return (defs)
                    End If

                Case Else
                    ' throw exception, somehow bad prorate present
                    Throw New Exception(ksIllegalProrateError)

            End Select

        Else
            ' Only one, fill array of one
            arrFactors = New Double(0) {1.0}
        End If

        Return (objDefinitions)
    End Function

    Private Function ProcessSeasonalProrate(ByVal objDefinitions As DetailedRateDefinitionCollection, ByVal dtStartDate As DateTime, ByVal dtEndDate As DateTime, ByRef eSeason As Season) As Boolean
        Dim bRetval As Boolean = True

        If (objDefinitions.Count > 0) Then
            ' Must already be seasonal
            If (objDefinitions(0).IsSeasonal) Then
                Try
                    ' Most recent definition is always at index zero
                    ' Get the SeasonalProrateType from the oldest definition
                    Select Case objDefinitions(objDefinitions.Count - 1).SeasonalProrateType

                        Case SeasonalProrateType.SplitBillDays
                            bRetval = False

                        Case SeasonalProrateType.UsePrevious
                            eSeason = objDefinitions(objDefinitions.Count - 1).SeasonBoundaries.FindInclusiveBoundary(dtStartDate).Season

                        Case SeasonalProrateType.UseLatest
                            eSeason = objDefinitions(0).SeasonBoundaries.FindInclusiveBoundary(dtEndDate).Season

                    End Select

                Catch ex As Exception
                    ' In rare case that this fails, assume SplitBillsDays (false)
                    bRetval = False
                End Try
            Else
                bRetval = False
            End If
        Else
            bRetval = False
        End If

        Return (bRetval)
    End Function

    Private Function DoesProjectedEndDateOverlapSeasonBoundaryWithInconsistentTOU(ByVal objDefinitions As DetailedRateDefinitionCollection, ByVal dtStartDate As DateTime, ByVal dtEndDate As DateTime, ByVal dtProjectedEndDate As DateTime) As Boolean
        Dim overlaps As Boolean = False
        Dim bUseSeasonProrateOverride As Boolean
        Dim detailedDef As DetailedRateDefinition
        Dim eOverrideSeason As Season
        Dim seasonFactors As SeasonFactorCollection
        Dim billDays As Integer

        detailedDef = objDefinitions(0)

        ' Be sure that the definition is time-of-use, because the BoundariesConsistent property is for time-of-use rate classes
        If (detailedDef.IsTimeOfUse And (Not detailedDef.BoundariesConsistent)) Then

            bUseSeasonProrateOverride = ProcessSeasonalProrate(objDefinitions, dtStartDate, dtProjectedEndDate, eOverrideSeason)

            If (detailedDef.IsSeasonal And (Not bUseSeasonProrateOverride)) Then

                billDays = dtProjectedEndDate.Subtract(dtEndDate).Days
                seasonFactors = DetermineSeasonFactors(detailedDef, billDays, dtEndDate.AddDays(1), dtProjectedEndDate, True, 1.0, bUseSeasonProrateOverride, eOverrideSeason)

                If (seasonFactors.Count > 1) Then
                    overlaps = True
                End If

            End If

        End If

        Return (overlaps)
    End Function

    Private Function ParseRateClass(ByVal nRateCompanyID As Integer, ByVal sFullRateClass As String, ByRef sBaseRateClass As String, ByRef sCode As String, ByRef sTerritory As String) As Boolean
        Dim bRetval As Boolean = True
        Dim nBreakIndex As Integer
        Dim objRateClassModifiers As RateClassModifierCollection
        Dim sBaseslineCode As String

        If sFullRateClass.StartsWith(ksnRateClassHash) Then
            objRateClassModifiers = CreateRateClassModifierCollection(sFullRateClass, nRateCompanyID)
            sBaseRateClass = objRateClassModifiers.Find(RateClassModifierType.RateClass).Value
            sBaseslineCode = objRateClassModifiers.Find(RateClassModifierType.BaselineCode).Value
            sCode = sBaseslineCode.Substring(0, 1)
            sTerritory = sBaseslineCode.Substring(1, 1)
        Else
            nBreakIndex = sFullRateClass.IndexOf(GetRateClassBreakCharacter(nRateCompanyID))

            If (nBreakIndex >= 0) Then
                sBaseRateClass = sFullRateClass.Substring(0, nBreakIndex)
                sCode = sFullRateClass.Substring(nBreakIndex + 1, 1)
                sTerritory = sFullRateClass.Substring(nBreakIndex + 2, 1)
            Else
                bRetval = False
            End If
        End If

        If (sBaseRateClass = String.Empty Or sCode = String.Empty Or sTerritory = String.Empty) Then
            bRetval = False
        End If

        Return (bRetval)
    End Function

    Private Function GetRateClassBreakCharacter(ByVal nRateCompanyID As Integer) As String
        Dim sBreak As String

        ' TODO: Change in future when we have additional configurable items for a config table
        ' Special case simplified handling of the break character.  This can be in a configuration table in the
        ' future for lookup.  However, since this is only new config item, and it is California Territory customer specific
        ' we are doing it this way for now (a rare break from totally configurable)
        ' MKG - This was okayed with SS
        Select Case nRateCompanyID
            Case 102, 366             'PG&E only uses underscore :CR 17799 Also, sdge uses underscore
                sBreak = ksRateClassBreakCharPrimary
            Case Else            'Everyone else uses a pipe
                sBreak = ksRateClassBreakCharSecondary
        End Select

        Return (sBreak)
    End Function

    Private Function VerifyUsages(ByVal objUsageColl As UsageCollection) As UsageCollection
        Dim objDC As New dctlRateEngine(m_sRateEngineConnectionString, m_xmlFilesPath, m_dataSourceType, m_cachingType, m_cacheTimeoutInMinutes)
        Dim objUsage As Usage

        ' Loop Usage Collection, and lookup usages that only have UseNames specified
        For Each objUsage In objUsageColl
            If (objUsage.UseTypeName <> String.Empty And objUsage.ProcessedUseTypeName = False) Then
                objUsage = objDC.ConvertRawUsage(objUsage)
            End If
        Next

        objUsageColl.DiscardUnmappedUsage()

        Return (objUsageColl)
    End Function

    ' Used by FCR call extra function
    Private Function DoesFCRChange(ByVal objDefintions As DetailedRateDefinitionCollection, ByVal dtStartDate As DateTime, ByVal dtEndDate As DateTime) As Boolean
        Dim bChange As Boolean
        Dim objDef As DetailedRateDefinition
        Dim objUseCharge As UseCharge
        Dim bFCRGroupChange As Boolean
        Dim nLastFCRGroupID As Integer
        Dim nFCRGroupID As Integer = 0
        Dim nDefCount As Integer = 0

        ' Find potential FCRGroup change
        For Each objDef In objDefintions
            For Each objUseCharge In objDef.UseCharges
                If (objUseCharge.FCRGroup > 0) Then
                    nFCRGroupID = objUseCharge.FCRGroup
                    Exit For
                End If
            Next

            If (nLastFCRGroupID = 0) Then
                nLastFCRGroupID = nFCRGroupID
                If (nDefCount > 0 And nFCRGroupID > 0) Then
                    bFCRGroupChange = True
                    Exit For
                End If
            ElseIf (nFCRGroupID <> nLastFCRGroupID) Then
                bFCRGroupChange = True
                Exit For
            End If

            nDefCount = nDefCount + 1
        Next

        If (bFCRGroupChange) Then
            bChange = True
        Else
            If (nFCRGroupID > 0) Then
                'Get FCR list - If more than one item in list then likely an FCR related change of rate over the time period
                Dim objDC As New dctlRateEngine(m_sRateEngineConnectionString, m_xmlFilesPath, m_dataSourceType, m_cachingType, m_cacheTimeoutInMinutes)
                Dim objFCRCollection As New FCRCollection
                objFCRCollection = objDC.GetFuelCostRecoveryList(nFCRGroupID, dtStartDate, dtEndDate)
                If (objFCRCollection.Count > 1) Then
                    bChange = True
                End If
            End If

        End If

        Return (bChange)
    End Function

    ' Used by enhanced binning initially;
    Private Function GetPriceData(ByVal nRateCompanyID As Integer, ByVal nRTPGroup As Integer, ByVal sRateClass As String, ByVal dtStartDate As DateTime, ByVal dtEndDate As DateTime, ByVal bFillMissing As Boolean) As PriceCollection
        Dim prices As PriceCollection = Nothing
        Dim objDC As dctlRateEngine

        ' use date ranges here
        objDC = New dctlRateEngine(m_sRateEngineConnectionString, m_xmlFilesPath, m_dataSourceType, m_cachingType, m_cacheTimeoutInMinutes)

        Try
            ' need to add 23 hours to endDate in order to get all needed prices for the entire day; only use it for this query
            prices = objDC.GetPriceData(nRateCompanyID, nRTPGroup, sRateClass, dtStartDate, dtEndDate.AddHours(23), True)

        Catch ex As Exception
            'real-time pricing is sometimes problematic;
            ' do not kill entire caller with excpetion here;  just return nothing
        End Try

        Return (prices)
    End Function

    ''' <summary>
    ''' set the default DST end date - first sunday of nov
    ''' CR 21578 Jan 2012
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function SetDefaultDSTEndDate(ByVal dtNow As DateTime) As DateTime
        Dim dateNov As DateTime = New DateTime(dtNow.Year, 11, 1)
        If dateNov.DayOfWeek = DayOfWeek.Sunday Then
            Return dateNov
        Else
            Return dateNov.AddDays(7 - CInt(dateNov.DayOfWeek))
        End If
    End Function

    ''' <summary>
    ''' convert dst end date string to datetime 
    ''' CR 21578 Jan 2012
    ''' </summary>
    ''' <param name="sDSTEndDate"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function ConvertDSTDate(ByVal sDSTEndDate As String) As DateTime
        Dim dtDSTEndDate As DateTime
        If sDSTEndDate = String.Empty Then
            dtDSTEndDate = DateTime.MinValue
        Else
            dtDSTEndDate = CType(sDSTEndDate, DateTime)
        End If
        Return dtDSTEndDate
    End Function

    ''' <summary>
    ''' Detemrine if rate definition is a hybrid tier strucuture type.
    ''' These are a special case that require complex binnning for bill-to-date.
    ''' </summary>
    ''' <param name="def"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function IsComplexHybrid(ByVal def As RateDefinitionBase) As Boolean
        Dim ret As Boolean = False

        If (Not def Is Nothing) Then
            If (def.IsTiered = True AndAlso def.TierStructureType = TierStructureType.MonthlyBoundsHybrid1) Then
                ret = True
            End If
        End If

        Return (ret)
    End Function

    ''' <summary>
    ''' determine if the rate class is set up to be complex rate
    ''' Tiered + TOU
    ''' RTP
    ''' Hybrid Tiered
    ''' CR 51610 Oct 2014
    ''' </summary>
    ''' <param name="detailedDef"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function IsComplexRate(detailedDef As DetailedRateDefinition) As Boolean
        Dim bResult As Boolean = False
        If (detailedDef.UseCharges.ExistsAnyTierTOU() Or detailedDef.IsRTPRate Or IsComplexHybrid(detailedDef)) Then
            bResult = True
        End If
        Return bResult
    End Function

    ''' <summary>
    ''' check if there's negative usage for NEM tiers meter
    ''' CR 52497 Nov 2014
    ''' </summary>
    ''' <param name="usages"></param>
    ''' <param name="detailedDef"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function IsNegativeUsageExistsForNEMTiers(usages As UsageCollection, detailedDef As DetailedRateDefinition, rateClass As String) As Boolean
        Dim hasNegativeUsage As Boolean = False
        Dim touUsage As UsageCollection
        Dim nmType As NetMeteringType = NetMeteringType.Unspecified

        Dim total As Double = 0
        ' check if rate class is Tiers
        If Not detailedDef.IsTiered Then
            Return False
        End If

        ' check if rate class is nem 3, 4, or 5
        If (rateClass.StartsWith(ksnRateClassHash) AndAlso RateClassAttributeIndicatesNetMetering(rateClass)) Then
            nmType = RateClassAttributeIndicatesNetMeteringType(rateClass)
        ElseIf (detailedDef.IsNetMetering) Then
            nmType = detailedDef.NetMeteringType
        End If
        If (Not nmType = NetMeteringType.Nem3 AndAlso Not nmType = NetMeteringType.Nem4 AndAlso Not nmType = NetMeteringType.Nem5) Then
            Return False
        End If

        ' check if there's negative usage by TOU/NoTOU
        If (detailedDef.IsTimeOfUse) Then
            For tou As Enums.TimeOfUse = TimeOfUse.OnPeak To TimeOfUse.CriticalPeak
                'exclude CPP adder
                If tou = TimeOfUse.CriticalPeak AndAlso detailedDef.IsRTPRate AndAlso detailedDef.IsCPEventUsageAdder AndAlso detailedDef.RTPStream = RTPStream.CriticalPeakOnly Then
                    'do nothing
                Else
                    If (usages.GetSeasonCount() > 0) Then
                        For season As Enums.Season = Enums.Season.Summer To Enums.Season.SeasonB
                            total = 0
                            ' sum up each tou by season
                            touUsage = usages.GetAllUsagesWithTimeOfUseAndSeason(tou, season)
                            For Each usage As Usage In touUsage
                                total = total + usage.Quantity
                            Next
                            ' check if negative
                            If (total < 0) Then
                                hasNegativeUsage = True
                                Exit For
                            End If
                        Next

                        If (hasNegativeUsage) Then
                            Exit For
                        End If
                    Else
                        total = 0
                        ' sum up each tou
                        touUsage = usages.GetAllUsagesWithTimeOfUse(tou)
                        For Each usage As Usage In touUsage
                            total = total + usage.Quantity
                        Next
                        ' check if negative
                        If (total < 0) Then
                            hasNegativeUsage = True
                            Exit For
                        End If
                    End If
                End If
            Next
        Else
            If (usages.GetSeasonCount() > 0) Then
                For season As Enums.Season = Enums.Season.Summer To Enums.Season.SeasonB
                    total = usages.GetTotalServiceUse(season)
                    ' check if negative
                    If (total < 0) Then
                        hasNegativeUsage = True
                        Exit For
                    End If
                Next
            Else
                total = usages.GetTotalServiceUse()
                ' check if negative
                If (total < 0) Then
                    hasNegativeUsage = True
                End If
            End If
        End If

        Return hasNegativeUsage
    End Function


#End Region



#Region "Helper Functions for Rebinning"

    ' Used to check for Seasonal TOU in MasterCalculateCost
    Private Function CheckForSeasonalTOUWithUsage(ByVal objUsageColl As UsageCollection, ByVal objDefinitions As DetailedRateDefinitionCollection) As Boolean
        Dim bRebin As Boolean
        Dim objDef As DetailedRateDefinition
        Dim objUsage As Usage

        If (objDefinitions.Count > 0) Then
            objDef = objDefinitions(0)

            ' If it is TOU and Seasonal and NOT Tiered, since Tiered Seasonal TOU cannot be rebinned
            If (objDef.IsTimeOfUse And objDef.IsSeasonal) Then
                bRebin = True

                ' Check each part, looking for a disqualifying usage
                For Each objUsage In objUsageColl
                    ' Ignore Demand and TotalServiceUse and Tiers;  
                    ' only when BaseOrTier is Undefined (since TotalServiceUse may actually be present for some clients)
                    If (objUsage.BaseOrTier = BaseOrTier.Undefined) Then
                        ' Non-Demand usages must be TOU and non seasonal to be eligible for rebinning
                        If (objUsage.TimeOfUse <> TimeOfUse.Undefined) Then
                            ' It is TOU, but a specific season is supplied; this is ineligible for rebinning
                            If (objUsage.Season <> Season.Undefined) Then
                                bRebin = False
                                Exit For
                            End If
                        Else
                            bRebin = False
                            Exit For
                        End If
                    Else
                        ' Anything Demand or TotalServiceUse or Tier is fine 
                    End If
                Next
            End If

        End If

        Return (bRebin)
    End Function

    ' Used for Rebinning of TOU into Seasonal TOU in MasterCalculateCost
    Private Function RebinUsagesForSeasonalTOU(ByVal objUsageColl As UsageCollection, ByVal objDefinitions As DetailedRateDefinitionCollection, ByVal dtStartDate As DateTime, ByVal dtEndDate As DateTime, ByVal bUseSeasonProrateOverride As Boolean, ByVal eOverrideSeason As Season) As UsageCollection
        Dim arrSeasonDayTypeCount(,) As Integer
        Dim arrSeasonDayTypeTOUHours(,,) As Integer
        Dim arrSeasonTOUHours(,) As Integer
        Dim arrTOUHoursTotal() As Integer
        Dim objRebinnedUsageColl As UsageCollection
        Dim objPtr As UsageCollection
        Dim holidays As HolidayCollection
        Dim holidaysUsed As Boolean = False

        If (objDefinitions.Count > 0) Then
            arrSeasonDayTypeCount = BuildDaysOfWeekPerSeason(objDefinitions, dtStartDate, dtEndDate, bUseSeasonProrateOverride, eOverrideSeason)

            If (objDefinitions(0).HolidayGroupExists()) Then
                holidays = GetHolidayList(objDefinitions(0).HolidayGroupID, dtStartDate, dtEndDate)
                If (Not holidays Is Nothing) Then
                    If (holidays.Count > 0) Then
                        AdjustDayTypeCountForHolidays(objDefinitions, arrSeasonDayTypeCount, holidays, bUseSeasonProrateOverride, eOverrideSeason)
                        holidaysUsed = True
                    End If
                End If
            End If

            arrSeasonDayTypeTOUHours = BuildDayOfWeekHoursPerSeason(objDefinitions)
            arrSeasonTOUHours = BuildTOUHoursPerSeason(arrSeasonDayTypeCount, arrSeasonDayTypeTOUHours)
            arrTOUHoursTotal = BuildTOUHoursTotal(arrSeasonTOUHours)
            objRebinnedUsageColl = DoRebinning(objUsageColl, arrSeasonTOUHours, arrTOUHoursTotal)
            objPtr = objRebinnedUsageColl
            Log("...[Rebinned Usages for Seasonal TOU]")
            If (holidaysUsed) Then Log("[Holiday adjustment applied to Rebinned Usages for Seasonal TOU]")
            Log(objPtr)
        Else
            objPtr = objUsageColl
        End If

        Return (objPtr)
    End Function

    ''' <summary>
    ''' Execute the rebinning of usage based upon attributes.
    ''' </summary>
    ''' <param name="objUsageColl"></param>
    ''' <param name="arrSeasonTOUHours"></param>
    ''' <param name="arrTOUHoursTotal"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function DoRebinning(ByVal objUsageColl As UsageCollection, ByVal arrSeasonTOUHours As Integer(,), ByVal arrTOUHoursTotal As Integer()) As UsageCollection
        Dim objRebinnedUsageColl As New UsageCollection
        Dim objUsage, objNewUsage As Usage
        Dim eSeason As Season
        Dim dblPercent, dblQuantity As Double

        For Each objUsage In objUsageColl

            dblPercent = 0.0
            Select Case objUsage.BaseOrTier

                Case BaseOrTier.Undefined
                    '  TOU handling here
                    For eSeason = Season.Summer To Season.SeasonB
                        If (arrTOUHoursTotal(objUsage.TimeOfUse) > 0) Then
                            dblPercent = arrSeasonTOUHours(eSeason, objUsage.TimeOfUse) / arrTOUHoursTotal(objUsage.TimeOfUse)
                        End If

                        dblQuantity = objUsage.Quantity * dblPercent
                        If (dblQuantity <> 0) Then
                            objNewUsage = New Usage(BaseOrTier.Undefined, objUsage.TimeOfUse, eSeason, dblQuantity)
                            objRebinnedUsageColl.Add(objNewUsage)
                        End If

                    Next

                Case BaseOrTier.Demand, BaseOrTier.DemandBill
                    ' Demand handling, in case it is special in the future
                    If (objUsage.TimeOfUse <> TimeOfUse.Undefined) Then
                        For eSeason = Season.Summer To Season.SeasonB
                            If (arrTOUHoursTotal(objUsage.TimeOfUse) > 0) Then
                                dblPercent = arrSeasonTOUHours(eSeason, objUsage.TimeOfUse) / arrTOUHoursTotal(objUsage.TimeOfUse)
                            End If

                            dblQuantity = objUsage.Quantity * dblPercent
                            If (dblQuantity <> 0) Then
                                ' The BaseOrTier is used here, since it is Demand or DemandBill
                                objNewUsage = New Usage(objUsage.BaseOrTier, objUsage.TimeOfUse, eSeason, dblQuantity)
                                objRebinnedUsageColl.Add(objNewUsage)
                            End If

                        Next
                    Else
                        ' Demand, Undefined, season passed in

                        ' Create seasonal Demand/DemandBill if the seasonal part is undefined
                        ' Use the zero index for TOU in arrays, which contains the total
                        If (objUsage.Season = Season.Undefined) Then
                            For eSeason = Season.Summer To Season.SeasonB
                                If (arrTOUHoursTotal(0) > 0) Then
                                    dblPercent = arrSeasonTOUHours(eSeason, 0) / arrTOUHoursTotal(0)
                                End If

                                dblQuantity = objUsage.Quantity * dblPercent
                                If (dblQuantity <> 0) Then
                                    ' The BaseOrTier is used here, since it is Demand or DemandBill
                                    objNewUsage = New Usage(objUsage.BaseOrTier, objUsage.TimeOfUse, eSeason, dblQuantity)
                                    objRebinnedUsageColl.Add(objNewUsage)
                                End If

                            Next
                        Else
                            ' The BaseOrTier is used here, since it is Demand or DemandBill
                            objNewUsage = New Usage(objUsage.BaseOrTier, objUsage.TimeOfUse, objUsage.Season, objUsage.Quantity)
                            objRebinnedUsageColl.Add(objNewUsage)
                        End If

                    End If


                Case Else
                    ' All others are preserved into new *rebinned* collection
                    objNewUsage = New Usage(objUsage.BaseOrTier, objUsage.TimeOfUse, objUsage.Season, objUsage.Quantity)
                    objRebinnedUsageColl.Add(objNewUsage)
            End Select

        Next

        Return (objRebinnedUsageColl)
    End Function

    ''' <summary>
    ''' This method returns Day of week count for a different seasons.
    ''' </summary>
    ''' <param name="objDefinitions"></param>
    ''' <param name="dtStartDate"></param>
    ''' <param name="dtEndDate"></param>
    ''' <param name="bUseSeasonProrateOverride"></param>
    ''' <param name="eOverrideSeason"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function BuildDaysOfWeekPerSeason(ByVal objDefinitions As DetailedRateDefinitionCollection, ByVal dtStartDate As DateTime, ByVal dtEndDate As DateTime, ByVal bUseSeasonProrateOverride As Boolean, ByVal eOverrideSeason As Season) As Integer(,)
        Dim arrSeasonDayOfWeekCount(5, 7) As Integer        ' (eSeason, eDayOfWeek)
        Dim objSeasonBoundaries As SeasonBoundaryCollection
        Dim eSeason As Season
        Dim nDayCount, i As Integer
        Dim dtTemp As DateTime

        If (objDefinitions.Count > 0) Then
            objSeasonBoundaries = objDefinitions.Item(0).SeasonBoundaries

            If (objSeasonBoundaries.Count > 0) Then
                nDayCount = dtEndDate.Subtract(dtStartDate).Days + 1

                ' This builds the array of counts for seasonal DayTypes
                For i = 0 To (nDayCount - 1)

                    dtTemp = dtStartDate.AddDays(i)
                    eSeason = objSeasonBoundaries.FindInclusiveBoundary(dtTemp).Season
                    If (bUseSeasonProrateOverride) Then eSeason = eOverrideSeason

                    arrSeasonDayOfWeekCount(eSeason, dtTemp.DayOfWeek) = arrSeasonDayOfWeekCount(eSeason, dtTemp.DayOfWeek) + 1

                Next

            End If

        End If

        Return (arrSeasonDayOfWeekCount)
    End Function

    ''' <summary>
    ''' This changes the weekday and weekend counts based on holidays that fall on weekdays;  weekday -=1 and weekend +=1
    ''' </summary>
    ''' <param name="objDefinitions"></param>
    ''' <param name="arrSeasonDayTypeCount"></param>
    ''' <param name="holidays"></param>
    ''' <param name="bUseSeasonProrateOverride"></param>
    ''' <param name="eOverrideSeason"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function AdjustDayTypeCountForHolidays(ByVal objDefinitions As DetailedRateDefinitionCollection, ByVal arrSeasonDayTypeCount As Integer(,), ByVal holidays As HolidayCollection, ByVal bUseSeasonProrateOverride As Boolean, ByVal eOverrideSeason As Season) As Boolean
        Dim h As Holiday
        Dim eSeason As Season
        Dim objSeasonBoundaries As SeasonBoundaryCollection

        If (objDefinitions.Count > 0) Then
            objSeasonBoundaries = objDefinitions.Item(0).SeasonBoundaries

            If (objSeasonBoundaries.Count > 0) Then

                ' note input 2-dim array is arrSeasonDayTypeCount(Season, DayType)
                If (holidays.Count > 0) Then
                    If objDefinitions.Item(0).TOUBoundaries.ContainsDayType(DayType.Holiday) Then
                        For Each h In holidays
                            eSeason = objSeasonBoundaries.FindInclusiveBoundary(h.DateOfHoliday).Season
                            If (bUseSeasonProrateOverride) Then eSeason = eOverrideSeason
                            arrSeasonDayTypeCount(eSeason, 7) += 1
                            'subtract out the day that the holiday was represented in
                            arrSeasonDayTypeCount(eSeason, h.DateOfHoliday.DayOfWeek) -= 1
                        Next
                    Else
                        For Each h In holidays
                            If (h.DateOfHoliday.DayOfWeek <> DayOfWeek.Saturday And h.DateOfHoliday.DayOfWeek <> DayOfWeek.Sunday) Then

                                eSeason = objSeasonBoundaries.FindInclusiveBoundary(h.DateOfHoliday).Season
                                If (bUseSeasonProrateOverride) Then eSeason = eOverrideSeason

                                ' decrement the weekday by one
                                If (arrSeasonDayTypeCount(eSeason, h.DateOfHoliday.DayOfWeek) <> 0) Then
                                    arrSeasonDayTypeCount(eSeason, h.DateOfHoliday.DayOfWeek) -= 1
                                End If
                                ' and add one to a weekend (Sunday)
                                arrSeasonDayTypeCount(eSeason, System.DayOfWeek.Sunday) += 1

                            End If
                        Next

                    End If

                End If

            Else
                ' no seasons; no adjustment; this function should only be called when seasonal;
            End If

        End If

        Return (True)
    End Function

    ''' <summary>
    ''' This method returns an count for season, Day of Week and TOUType
    ''' </summary>
    ''' <param name="objDefinitions"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function BuildDayOfWeekHoursPerSeason(ByVal objDefinitions As DetailedRateDefinitionCollection) As Integer(,,)
        Dim arrSeasonDayOfWeekTOUHours(5, 7, 6) As Integer         ' (eSeason, eDayOfWeek, TOU)
        Dim objSeasonBoundaries As SeasonBoundaryCollection
        Dim objSeasonBoundary As SeasonBoundary
        Dim eSeason As Season
        Dim eIndexDayofWeekType As Integer
        Dim i As Integer
        Dim eTOUArray(,) As TimeOfUse
        Dim arrUsedSeasons() As Integer = {0, 0, 0, 0, 0}

        If (objDefinitions.Count > 0) Then
            objSeasonBoundaries = objDefinitions.Item(0).SeasonBoundaries

            If (objSeasonBoundaries.Count > 0 And objDefinitions.Item(0).TOUBoundaries.Count > 0) Then

                ' Loop available seasons
                For Each objSeasonBoundary In objSeasonBoundaries
                    eSeason = objSeasonBoundary.Season

                    ' This prevents a season from being counted twice
                    If (arrUsedSeasons(eSeason) = 0) Then

                        eTOUArray = GetTOUBoundaryArray(objDefinitions, eSeason)

                        ' need to include holiday in this which doesn't have a day of week associated with it.
                        For eIndexDayofWeekType = 0 To 7
                            ' eTOUArray(eDayOfWeekType, i) contains the TOU value, like OnPeak, OffPeak, etc
                            For i = 0 To 95
                                arrSeasonDayOfWeekTOUHours(eSeason, eIndexDayofWeekType, eTOUArray(eIndexDayofWeekType, i)) = arrSeasonDayOfWeekTOUHours(eSeason, eIndexDayofWeekType, eTOUArray(eIndexDayofWeekType, i)) + 1
                            Next
                        Next

                    End If

                    ' Mark current season as used
                    arrUsedSeasons(eSeason) = 1
                Next
            End If
        End If

        Return (arrSeasonDayOfWeekTOUHours)
    End Function

    ''' <summary>
    ''' This method returns count for season and TOU type
    ''' </summary>
    ''' <param name="arrSeasonDayTypeCount"></param>
    ''' <param name="arrSeasonDayTypeTOUHours"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function BuildTOUHoursPerSeason(ByVal arrSeasonDayTypeCount As Integer(,), ByVal arrSeasonDayTypeTOUHours As Integer(,,)) As Integer(,)
        Dim arrSeasonTOUHours(5, 6) As Integer        ' (eSeason, TimeOfUse)
        Dim nTotal As Integer
        Dim eIndexTOU As TimeOfUse
        Dim eIndexSeason As Season
        Dim eIndexDayofWeekType As Integer

        For eIndexSeason = Season.Summer To Season.SeasonB
            nTotal = 0
            For eIndexTOU = TimeOfUse.OnPeak To TimeOfUse.CriticalPeak
                For eIndexDayofWeekType = 0 To 7 ' 0 to 6
                    arrSeasonTOUHours(eIndexSeason, eIndexTOU) = arrSeasonTOUHours(eIndexSeason, eIndexTOU) + (arrSeasonDayTypeCount(eIndexSeason, eIndexDayofWeekType) * arrSeasonDayTypeTOUHours(eIndexSeason, eIndexDayofWeekType, eIndexTOU))
                Next
                nTotal = nTotal + arrSeasonTOUHours(eIndexSeason, eIndexTOU)
            Next
            ' TOU Index zero is the master total for the season 
            arrSeasonTOUHours(eIndexSeason, 0) = nTotal
        Next

        Return (arrSeasonTOUHours)
    End Function

    ''' <summary>
    ''' Count of total hours by tou.
    ''' </summary>
    ''' <param name="arrSeasonTOUHours"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function BuildTOUHoursTotal(ByVal arrSeasonTOUHours As Integer(,)) As Integer()
        Dim arrTOUHoursTotal(6) As Integer
        Dim nTotal As Integer
        Dim eIndexTOU As TimeOfUse
        Dim eIndexSeason As Season

        For eIndexSeason = Season.Summer To Season.SeasonB
            For eIndexTOU = TimeOfUse.OnPeak To TimeOfUse.CriticalPeak
                arrTOUHoursTotal(eIndexTOU) = arrTOUHoursTotal(eIndexTOU) + arrSeasonTOUHours(eIndexSeason, eIndexTOU)
            Next
        Next
        ' TOU Index zero is the master total 
        For eIndexTOU = TimeOfUse.OnPeak To TimeOfUse.CriticalPeak
            nTotal = nTotal + arrTOUHoursTotal(eIndexTOU)
        Next
        arrTOUHoursTotal(0) = nTotal

        Return (arrTOUHoursTotal)
    End Function

#End Region

#Region "Logging Helper Functions"

    Public Function GetLogHistory(ByVal guidSession As Guid) As LogEntryCollection
        Dim objDC As dctlRateEngine
        Dim objLogEntries As LogEntryCollection

        objDC = New dctlRateEngine(m_sRateEngineConnectionString, m_xmlFilesPath, m_dataSourceType, m_cachingType, m_cacheTimeoutInMinutes)
        objLogEntries = objDC.GetLogHistory(guidSession)

        Return (objLogEntries)
    End Function

    Public Function GetLoggedReadings(ByVal guidSession As Guid) As ReadingCollection
        Dim objDC As dctlRateEngine
        Dim readings As ReadingCollection

        objDC = New dctlRateEngine(m_sRateEngineConnectionString, m_xmlFilesPath, m_dataSourceType, m_cachingType, m_cacheTimeoutInMinutes)
        readings = objDC.GetLoggedReadings(guidSession)

        Return (readings)
    End Function

    Public Function GetLoggedReadings(ByVal guidSession As Guid, ByVal skipFirstSet As Boolean) As ReadingCollection
        Dim objDC As dctlRateEngine
        Dim readings As ReadingCollection

        objDC = New dctlRateEngine(m_sRateEngineConnectionString, m_xmlFilesPath, m_dataSourceType, m_cachingType, m_cacheTimeoutInMinutes)
        readings = objDC.GetLoggedReadings(guidSession, skipFirstSet)

        Return (readings)
    End Function

    Public Function GetLoggedReadings(ByVal guidSession As Guid, ByVal index As Integer) As ReadingCollection
        Dim objDC As dctlRateEngine
        Dim readings As ReadingCollection

        objDC = New dctlRateEngine(m_sRateEngineConnectionString, m_xmlFilesPath, m_dataSourceType, m_cachingType, m_cacheTimeoutInMinutes)
        readings = objDC.GetLoggedReadings(guidSession, index)

        Return (readings)
    End Function

    Private Function CreateLogSessionGUID() As System.Guid
        Return (Guid.NewGuid())
    End Function

    Private Function BeginLogSession() As Boolean
        Dim bRetval As Boolean

        If (IsLoggingEnabled) Then
            m_LogSessionID = CreateLogSessionGUID()
            bRetval = True
        End If

        Return (bRetval)
    End Function

    ' There may be several "Log" overloads, depending on what objects need to be concatenated into
    ' strings for logging.
    Private Function Log(ByVal sText As String) As Boolean
        Dim bRetval As Boolean
        Dim objDC As dctlRateEngine

        If (IsLoggingEnabled) Then
            objDC = New dctlRateEngine(m_sRateEngineConnectionString, m_xmlFilesPath, m_dataSourceType, m_cachingType, m_cacheTimeoutInMinutes)
            objDC.InsertLog(LogSessionID, sText)
            bRetval = True
        End If

        Return (bRetval)
    End Function

    ' This serializes a reading collection to the log binary field
    Private Function Log(ByVal readings As ReadingCollection) As Boolean
        Dim bRetval As Boolean
        Dim objDC As dctlRateEngine

        If (IsLoggingEnabled) Then
            objDC = New dctlRateEngine(m_sRateEngineConnectionString, m_xmlFilesPath, m_dataSourceType, m_cachingType, m_cacheTimeoutInMinutes)
            objDC.InsertLog(LogSessionID, readings)
            bRetval = True
        End If

        Return (bRetval)
    End Function

    Private Function Log(ByVal sText As String, ByVal eCostType As CostType) As Boolean
        Dim bRetval As Boolean

        If (IsLoggingEnabled) Then
            Dim sb As New System.Text.StringBuilder
            sb.Append(sText)
            sb.Append(",")
            sb.Append(eCostType.ToString())
            Log(sb.ToString)
            sb.Remove(0, sb.Length)
            bRetval = True
        End If

        Return (bRetval)
    End Function

    Private Function Log(ByVal objUsages As UsageCollection) As Boolean
        Dim bRetval As Boolean
        Dim objUsage As Usage

        If (IsLoggingEnabled) Then
            Dim sb As New System.Text.StringBuilder
            For Each objUsage In objUsages
                sb.Append("Usage [")
                sb.Append(objUsage.BaseOrTier.ToString)
                sb.Append(",")
                sb.Append(objUsage.TimeOfUse.ToString)
                sb.Append(",")
                sb.Append(objUsage.Season.ToString)
                sb.Append(",")
                sb.Append(objUsage.LoadType.ToString)
                sb.Append(",")
                sb.Append(objUsage.Quantity)
                If (objUsage.VirtualQuantity <> 0.0) Then
                    sb.Append(":LastPeriodQuantity:")
                    sb.Append(objUsage.VirtualQuantity)
                End If
                If (objUsage.UseTypeName <> String.Empty) Then
                    sb.Append(":mapped:")
                    sb.Append(objUsage.UseTypeName)
                End If
                sb.Append("]")
                Log(sb.ToString)
                sb.Remove(0, sb.Length)
            Next
            bRetval = True
        End If

        Return (bRetval)
    End Function

    ''' <summary>
    ''' CR 20298, 20299, 20302 Oct 2011 - log the rate class modifiers
    ''' </summary>
    ''' <param name="objRateClassModifiers"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function Log(ByVal objRateClassModifiers As RateClassModifierCollection) As Boolean
        Dim bRetval As Boolean
        Dim objRateClassModfier As RateClassModifier
        If (IsLoggingEnabled) Then
            Dim sb As New System.Text.StringBuilder
            For Each objRateClassModfier In objRateClassModifiers
                sb.Append("RateClassModifier [")
                sb.Append(objRateClassModfier.ModifierType.ToString)
                sb.Append("=")
                sb.Append(objRateClassModfier.Value.ToString)
                sb.Append("]")
                Log(sb.ToString)
                sb.Remove(0, sb.Length)
            Next
            bRetval = True
        End If

        Return (bRetval)
    End Function

    Private Function Log(ByVal objCostResult As CostResult) As Boolean
        Dim bRetval As Boolean
        Dim objDE As DictionaryEntry
        Dim objCost As Cost

        If (IsLoggingEnabled) Then
            Dim sb As New System.Text.StringBuilder
            sb.Append("CostResult [TotalCost=")
            sb.Append(objCostResult.TotalCost)
            sb.Append(",AvgDailyUse=")
            sb.Append(objCostResult.AvgDailyEnergyUse)
            sb.Append(",BillDays=")
            sb.Append(objCostResult.BillDays)
            sb.Append(",RateCountApplied=")
            sb.Append(objCostResult.RateCountApplied)
            sb.Append(",TotalUse=")
            sb.Append(objCostResult.TotalUse)
            sb.Append("]")
            Log(sb.ToString)
            sb.Remove(0, sb.Length)

            For Each objDE In objCostResult.CostCollection
                objCost = CType(objDE.Value, Cost)
                sb.Append("Cost [")
                sb.Append(objCost.CostType.ToString)
                sb.Append(",")
                sb.Append(objCost.Amount)
                sb.Append("]")
                Log(sb.ToString)
                sb.Remove(0, sb.Length)
            Next
            bRetval = True
        End If

        Return (bRetval)
    End Function

    Private Function Log(ByVal objDef As DetailedRateDefinition) As Boolean
        Dim bRetval As Boolean

        If (IsLoggingEnabled) Then
            Dim sb As New System.Text.StringBuilder

            sb.Append("DetailedRateDefinition [ID=")
            sb.Append(objDef.DefinitionID)
            sb.Append(",masterID=")
            sb.Append(objDef.MasterID)
            sb.Append(",RateCompanyID=")
            sb.Append(objDef.RateCompanyID)
            sb.Append(",ProrateType=")
            sb.Append(objDef.ProrateType)
            sb.Append(",FuelType=")
            sb.Append(objDef.FuelType)
            sb.Append("]")
            Log(sb.ToString)
            sb.Remove(0, sb.Length)

            sb.Append("...[RateClass=")
            sb.Append(objDef.RateClass)
            sb.Append(",masterID=")
            sb.Append(objDef.MasterID)
            sb.Append(",IsBasic=")
            sb.Append(objDef.IsBasic)
            sb.Append(",IsTiered=")
            sb.Append(objDef.IsTiered)
            sb.Append(",IsTOU=")
            sb.Append(objDef.IsTimeOfUse)
            sb.Append(",IsSeasonal=")
            sb.Append(objDef.IsSeasonal)
            If (objDef.IsSeasonal) Then
                sb.Append(",SpID=")
                sb.Append(objDef.SeasonalProrateType)
            End If
            sb.Append(",LoadType=")
            sb.Append(objDef.LoadType.ToString)
            sb.Append(",StartDate=")
            sb.Append(objDef.StartDate.ToShortDateString)
            sb.Append("]")
            Log(sb.ToString)
            sb.Remove(0, sb.Length)

            bRetval = True
        End If

        Return (bRetval)
    End Function

    Private Function Log(ByVal arrProrateFactors() As Double) As Boolean
        Dim bRetval As Boolean
        Dim i As Integer
        Dim dblTotal As Double

        If (IsLoggingEnabled) Then
            Dim sb As New System.Text.StringBuilder

            sb.Append("...[ProrateFactors=")

            For i = 0 To arrProrateFactors.Length - 1
                sb.Append(arrProrateFactors(i).ToString())
                sb.Append(" : ")
                If (i Mod 3) = 1 Then
                    Log(sb.ToString)
                    sb.Remove(0, sb.Length)
                End If
                dblTotal = dblTotal + arrProrateFactors(i)
            Next

            sb.Append(":Sum=")
            sb.Append(dblTotal.ToString())
            sb.Append("]")
            Log(sb.ToString)
            sb.Remove(0, sb.Length)

            bRetval = True
        End If

        Return (bRetval)
    End Function

    Private Function Log(ByVal bUseSeasonalProrate As Boolean, ByVal eSeason As Season) As Boolean
        Dim bRetval As Boolean

        If (IsLoggingEnabled) Then
            Dim sb As New System.Text.StringBuilder

            sb.Append("...[SeasonalProrateOverride")

            sb.Append("=")
            sb.Append(bUseSeasonalProrate.ToString())
            If (bUseSeasonalProrate) Then
                sb.Append(":Season=")
                sb.Append(eSeason.ToString())
            End If
            sb.Append("]")
            Log(sb.ToString)
            sb.Remove(0, sb.Length)

            bRetval = True
        End If

        Return (bRetval)
    End Function

    Private Function Log(ByVal objUseCharges As UseChargeCollection) As Boolean
        Dim bRetval As Boolean
        Dim objUseCharge As UseCharge

        If (IsLoggingEnabled) Then
            Dim sb As New System.Text.StringBuilder
            For Each objUseCharge In objUseCharges
                sb.Append("...UseCharge [")
                sb.Append(objUseCharge.BaseOrTier.ToString)
                sb.Append(",")
                sb.Append(objUseCharge.TimeOfUse.ToString)
                sb.Append(",")
                sb.Append(objUseCharge.Season.ToString)
                sb.Append(",")
                sb.Append(objUseCharge.PartType.ToString)
                sb.Append(",FCR=")
                sb.Append(objUseCharge.FuelCostRecovery.ToString)
                If (objUseCharge.FuelCostRecovery) Then
                    sb.Append(",group=")
                    sb.Append(objUseCharge.FCRGroup.ToString)
                End If
                sb.Append(",charge=")
                sb.Append(objUseCharge.ChargeValue)
                sb.Append("]")
                Log(sb.ToString)
                sb.Remove(0, sb.Length)
            Next
            bRetval = True
        End If

        Return (bRetval)
    End Function

    ' Bug 15.06 Bug 58148
    ' Log the cost result combine info when there're than 1 definition to clarify confustion
    Private Function Log(Costs As List(Of Cost), Amount As Double) As Boolean
        Dim bRet As Boolean
        Dim sb As New System.Text.StringBuilder

        sb.Append("...[CombineCostResult]")
        sb.Append("[")
        sb.Append(Costs(0).CostType.ToString())
        sb.Append("]")
        sb.Append("Sum=")
        sb.Append(Amount.ToString())
        Dim index As Integer = 0
        For Each objCost As Cost In Costs
            If (index = 0) Then
                sb.Append(" : ")
            Else
                sb.Append("+")
            End If
            sb.Append(objCost.Amount.ToString())
            index = index + 1
        Next
        Log(sb.ToString)
        sb.Remove(0, sb.Length)
        bRet = True

        Return (bRet)
    End Function

    Private Function Log(ByVal sText As String, ByVal text2 As String) As Boolean
        Dim bRetval As Boolean

        If (IsLoggingEnabled) Then
            Dim sb As New System.Text.StringBuilder

            sb.Append("...[")
            sb.Append(sText)
            sb.Append("][")
            sb.Append(text2)
            sb.Append("] ")
            Log(sb.ToString)
            sb.Remove(0, sb.Length)

            bRetval = True
        End If

        Return (bRetval)
    End Function

    Private Function Log(ByVal sText As String, ByVal dblAmount As Double, ByVal dblValA As Double, ByVal dblValB As Double, ByVal eCostType As CostType) As Boolean
        Dim bRetval As Boolean

        If (IsLoggingEnabled) Then
            Dim sb As New System.Text.StringBuilder

            sb.Append("...[")
            sb.Append(sText)
            sb.Append("][")
            sb.Append(eCostType.ToString())
            sb.Append("] Amount=")
            sb.Append(dblAmount.ToString)
            sb.Append(": ")
            sb.Append(dblValA.ToString)
            sb.Append("*")
            sb.Append(dblValB.ToString)
            Log(sb.ToString)
            sb.Remove(0, sb.Length)

            bRetval = True
        End If

        Return (bRetval)
    End Function

    Private Function Log(ByVal sText As String, ByVal eStep As ServiceStep, ByVal dblAmount As Double, ByVal dblValA As Double, ByVal dblValB As Double, ByVal dblValC As Double, ByVal eCostType As CostType) As Boolean
        Dim bRetval As Boolean

        If (IsLoggingEnabled) Then
            Dim sb As New System.Text.StringBuilder

            sb.Append("...[")
            sb.Append(sText)
            sb.Append("-")
            sb.Append(eStep.ToString)
            sb.Append("][")
            sb.Append(eCostType.ToString())
            sb.Append("] Amount=")
            sb.Append(dblAmount.ToString)
            sb.Append(": ")
            sb.Append(dblValA.ToString)
            sb.Append("*")
            sb.Append(dblValB.ToString)
            sb.Append("*")
            sb.Append(dblValC.ToString)
            Log(sb.ToString)
            sb.Remove(0, sb.Length)

            bRetval = True
        End If

        Return (bRetval)
    End Function

    Private Function Log(ByVal sText As String, ByVal dblAmount As Double, ByVal dblValA As Double, ByVal dblValB As Double, ByVal dblValC As Double, ByVal eCostType As CostType) As Boolean
        Dim bRetval As Boolean

        If (IsLoggingEnabled) Then
            Dim sb As New System.Text.StringBuilder

            sb.Append("...[")
            sb.Append(sText)
            sb.Append("][")
            sb.Append(eCostType.ToString())
            sb.Append("] Amount=")
            sb.Append(dblAmount.ToString)
            sb.Append(": ")
            sb.Append(dblValA.ToString)
            sb.Append("*")
            sb.Append(dblValB.ToString)
            sb.Append("*")
            sb.Append(dblValC.ToString)
            Log(sb.ToString)
            sb.Remove(0, sb.Length)

            bRetval = True
        End If

        Return (bRetval)
    End Function

    Private Function Log(ByVal sText As String, ByVal dblAmount As Double, ByVal dblValA As Double, ByVal dblValB As Double, ByVal dblValC As Double, ByVal dblValD As Double, ByVal eCostType As CostType, ByVal bIncludesRatio As Boolean) As Boolean
        Dim bRetval As Boolean

        If (IsLoggingEnabled) Then
            Dim sb As New System.Text.StringBuilder

            sb.Append("...[")
            sb.Append(sText)
            sb.Append("][")
            sb.Append(eCostType.ToString())
            sb.Append("] Amount=")
            sb.Append(dblAmount.ToString)
            sb.Append(": ")
            sb.Append(dblValA.ToString)
            sb.Append("*(r)")
            sb.Append(dblValB.ToString)
            sb.Append("*")
            sb.Append(dblValC.ToString)
            sb.Append("*")
            sb.Append(dblValD.ToString)
            Log(sb.ToString)
            sb.Remove(0, sb.Length)

            bRetval = True
        End If

        Return (bRetval)
    End Function

    Private Function Log(ByVal sText As String, ByVal sTextStructType As String, ByVal dblAmount As Double, ByVal dblValA As Double, ByVal dblValB As Double, ByVal dblValC As Double, ByVal eCostType As CostType) As Boolean
        Dim bRetval As Boolean

        If (IsLoggingEnabled) Then
            Dim sb As New System.Text.StringBuilder

            sb.Append("...[")
            sb.Append(sText)
            sb.Append("-")
            sb.Append(sTextStructType)
            sb.Append("][")
            sb.Append(eCostType.ToString())
            sb.Append("] Amount=")
            sb.Append(dblAmount.ToString)
            sb.Append(": ")
            sb.Append(dblValA.ToString)
            sb.Append("*")
            sb.Append(dblValB.ToString)
            sb.Append("*")
            sb.Append(dblValC.ToString)
            Log(sb.ToString)
            sb.Remove(0, sb.Length)

            bRetval = True
        End If

        Return (bRetval)
    End Function

    Private Function Log(ByVal sText As String, ByVal sTextStructType As String, ByVal dblAmount As Double, ByVal dblValAA As Double, ByVal dblValA As Double, ByVal dblValB As Double, ByVal dblValC As Double, ByVal eCostType As CostType) As Boolean
        Dim bRetval As Boolean

        If (IsLoggingEnabled) Then
            Dim sb As New System.Text.StringBuilder

            sb.Append("...[")
            sb.Append(sText)
            sb.Append("-")
            sb.Append(sTextStructType)
            sb.Append("][")
            sb.Append(eCostType.ToString())
            sb.Append("] Amount=")
            sb.Append(dblAmount.ToString)
            sb.Append(": ((")
            sb.Append(dblValB.ToString)
            sb.Append("-")
            sb.Append(dblValAA.ToString)
            sb.Append(")*")
            sb.Append(dblValA.ToString)
            sb.Append(")*")
            sb.Append(dblValC.ToString)
            Log(sb.ToString)
            sb.Remove(0, sb.Length)

            bRetval = True
        End If

        Return (bRetval)
    End Function

    Private Function Log(ByVal sText As String, ByVal dblAmount As Double, ByVal dblValA As Double, ByVal dblValB As Double, ByVal dblValC As Double, ByVal dblValD As Double, ByVal eCostType As CostType) As Boolean
        Dim bRetval As Boolean

        If (IsLoggingEnabled) Then
            Dim sb As New System.Text.StringBuilder

            sb.Append("...[")
            sb.Append(sText)
            sb.Append("][")
            sb.Append(eCostType.ToString())
            sb.Append("] Amount=")
            sb.Append(dblAmount.ToString)
            sb.Append(": ")
            sb.Append(dblValA.ToString)
            sb.Append("*")
            sb.Append(dblValB.ToString)
            sb.Append("*")
            sb.Append(dblValC.ToString)
            sb.Append("*")
            sb.Append(dblValD.ToString)
            Log(sb.ToString)
            sb.Remove(0, sb.Length)

            bRetval = True
        End If

        Return (bRetval)
    End Function

    Private Function Log(ByVal sText As String, ByVal nValA As Integer, ByVal nValB As Integer, ByVal dblAmount As Double, ByVal dblResult As Double) As Boolean
        Dim bRetval As Boolean

        If (IsLoggingEnabled) Then
            Dim sb As New System.Text.StringBuilder

            sb.Append("...[")
            sb.Append(sText)
            sb.Append("] ")
            sb.Append(" = (")
            sb.Append(nValA.ToString)
            sb.Append(" / ")
            sb.Append(nValB.ToString)
            sb.Append(") * ")
            sb.Append(dblAmount.ToString)
            sb.Append(" = ")
            sb.Append(dblResult.ToString)
            Log(sb.ToString)
            sb.Remove(0, sb.Length)

            bRetval = True
        End If

        Return (bRetval)
    End Function

    Private Function Log(ByVal sText As String, ByVal dblResult As Double) As Boolean
        Dim bRetval As Boolean

        If (IsLoggingEnabled) Then
            Dim sb As New System.Text.StringBuilder

            sb.Append("...[")
            sb.Append(sText)
            sb.Append("] ")
            sb.Append(" = ")
            sb.Append(dblResult.ToString)
            Log(sb.ToString)
            sb.Remove(0, sb.Length)

            bRetval = True
        End If

        Return (bRetval)
    End Function

    Private Function Log(ByVal objTB As TierBoundaryCollection) As Boolean
        Dim bRetval As Boolean
        Dim t As TierBoundary

        If (IsLoggingEnabled) Then
            Dim sb As New System.Text.StringBuilder
            For Each t In objTB
                sb.Append("FinalTierBoundaries [")
                sb.Append(t.BaseOrTier.ToString())
                sb.Append(",")
                sb.Append(t.TimeOfUse.ToString())
                sb.Append(",")
                sb.Append(t.Season.ToString())
                sb.Append(",")
                sb.Append(t.Threshold.ToString())
                sb.Append("]")
                Log(sb.ToString)
                sb.Remove(0, sb.Length)
            Next
            bRetval = True
        End If

        Return (bRetval)
    End Function

#End Region


    ' Class for a list of time-of-use values; used internally
    Private Class TOUList
        Inherits System.Collections.Generic.List(Of TimeOfUse)
    End Class

    ' Class for special sorting; used internally
    Private Class QuantitySorter
        Implements IComparer

        Public Sub New()
        End Sub

        Public Function Compare(ByVal x As Object, ByVal y As Object) As Integer Implements System.Collections.IComparer.Compare
            Dim i As Integer
            Dim icA As IComparable = CType(x, IComparable)
            Dim icB As IComparable = CType(y, IComparable)

            i = icB.CompareTo(icA)

            Return (i)
        End Function

    End Class

End Class
