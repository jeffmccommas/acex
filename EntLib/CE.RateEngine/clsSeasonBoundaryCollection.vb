Public Class SeasonBoundaryCollection
	Inherits CollectionBase

	Public Sub New()
	End Sub

	Default ReadOnly Property Item(ByVal index As Integer) As SeasonBoundary
		Get
			Return CType(Me.InnerList.Item(index), SeasonBoundary)
		End Get
	End Property

	Public Function Add(ByVal value As SeasonBoundary) As Integer
		Return Me.InnerList.Add(value)
	End Function

	Public Sub Insert(ByVal index As Integer, ByVal value As SeasonBoundary)
		Me.InnerList.Insert(index, value)
	End Sub

	Public Sub AddRange(ByVal values() As SeasonBoundary)
		Me.InnerList.AddRange(values)
	End Sub

	Public Sub Remove(ByVal value As SeasonBoundary)
		Me.InnerList.Remove(value)
	End Sub

	Public Function FindInclusiveBoundary(ByVal dtDate As DateTime) As SeasonBoundary
		Dim i As Integer
		Dim objTemp As SeasonBoundary
		Dim objSeasonBoundary As SeasonBoundary= Nothing

		If (Me.InnerList.Count > 0) Then

			For i = 0 To Me.InnerList.Count - 1
				objTemp = CType(Me.InnerList.Item(i), SeasonBoundary)
				' First comparison used to be simply "less than", now it is "less than or equal to"
				If ConvertDayToDate(objTemp.StartDay, dtDate.Year) <= dtDate And ConvertDayToDate(objTemp.EndDay, dtDate.Year) >= dtDate Then
					objSeasonBoundary = CType(Me.InnerList.Item(i), SeasonBoundary)
					Exit For
				End If
			Next

		End If

		Return (objSeasonBoundary)
    End Function

    Public Function FindAllInclusiveBoundaries(ByVal startDate As DateTime, ByVal endDate As DateTime) As SeasonBoundaryCollection
        Dim coll As SeasonBoundaryCollection = Nothing
        Dim i As Integer
        Dim temp As SeasonBoundary
        Dim sbA As SeasonBoundary
        Dim sbB As SeasonBoundary
        Dim skip As Boolean = False

        If (Me.InnerList.Count > 0) Then

            sbA = FindInclusiveBoundary(startDate)
            sbB = FindInclusiveBoundary(endDate)

            If ((Not sbA Is Nothing) AndAlso (Not sbB Is Nothing)) Then
                coll = New SeasonBoundaryCollection()
                coll.Add(sbA)

                If (sbA.Season <> sbB.Season) Then
                    coll.Add(sbB)
                End If

                ' finally, any boundaries that are wholly between startDate and endDate
                For i = 0 To Me.InnerList.Count - 1
                    temp = CType(Me.InnerList.Item(i), SeasonBoundary)

                    If (ConvertDayToDate(temp.StartDay, startDate.Year) >= startDate And ConvertDayToDate(temp.EndDay, startDate.Year) >= startDate) AndAlso _
                        (ConvertDayToDate(temp.StartDay, endDate.Year) <= endDate And ConvertDayToDate(temp.EndDay, endDate.Year) <= endDate) Then

                        ' found potentional season boundary wholly within dates; check to be sure not already added for sanity.
                        For Each x As SeasonBoundary In coll
                            If (x.Season = temp.Season) Then skip = True
                        Next

                        If (Not skip) Then
                            coll.Add(temp)
                        End If

                    End If

                Next

            End If

        End If

        Return (coll)
    End Function

    Public Function ConvertDayToDate(ByVal sMonthDay As String, ByVal nYear As Integer) As DateTime
        Dim sMonth As String = sMonthDay.Substring(0, 2)
        Dim sDay As String = sMonthDay.Substring(2, 2)
        Dim sYear As String = nYear.ToString
        Dim sbParseDate As New System.Text.StringBuilder
        Dim dtDate As DateTime

        sbParseDate.Append(sMonth) : sbParseDate.Append("/")
        sbParseDate.Append(sDay) : sbParseDate.Append("/")
        sbParseDate.Append(sYear)
        dtDate = DateTime.Parse(sbParseDate.ToString)

        Return (dtDate)
    End Function


    Public Sub Sort()
        Dim dateSorter As IComparer = New DescendingDateSorter
        Me.InnerList.Sort(dateSorter)
    End Sub

    Public Sub SortAscending()
        Dim dateSorter As IComparer = New AscendingDateSorter
        Me.InnerList.Sort(dateSorter)
    End Sub

    Private Class DescendingDateSorter
        Implements IComparer

        Public Function Compare(ByVal x As Object, ByVal y As Object) As Integer Implements System.Collections.IComparer.Compare
            Dim sb As SeasonBoundary = CType(x, SeasonBoundary)
            Dim sMonth As String = sb.StartDay.Substring(0, 2)
            Dim sDay As String = sb.StartDay.Substring(2, 2)
            Dim sYear As String = DateTime.Now.Year.ToString()           'Actual Year does not matter (ACTUALLY - it might - but not for now!)
            Dim sbParseDate As New System.Text.StringBuilder
            sbParseDate.Append(sMonth) : sbParseDate.Append("/")
            sbParseDate.Append(sDay) : sbParseDate.Append("/")
            sbParseDate.Append(sYear)
            Dim dtStartDate As DateTime = DateTime.Parse(sbParseDate.ToString)
            Dim icA As IComparable = CType(dtStartDate, IComparable)

            Dim sb2 As SeasonBoundary = CType(y, SeasonBoundary)
            Dim sMonth2 As String = sb2.StartDay.Substring(0, 2)
            Dim sDay2 As String = sb2.StartDay.Substring(2, 2)
            Dim sbParseDate2 As New System.Text.StringBuilder
            sbParseDate2.Append(sMonth2) : sbParseDate2.Append("/")
            sbParseDate2.Append(sDay2) : sbParseDate2.Append("/")
            sbParseDate2.Append(sYear)
            Dim dtStartDate2 As DateTime = DateTime.Parse(sbParseDate2.ToString)
            Dim icB As IComparable = CType(dtStartDate2, IComparable)

            ' swapping icB and icA makes it ascending rather than descending
            Return (icB.CompareTo(icA))
        End Function

    End Class

    Private Class AscendingDateSorter
        Implements IComparer

        Public Function Compare(ByVal x As Object, ByVal y As Object) As Integer Implements System.Collections.IComparer.Compare
            Dim sb As SeasonBoundary = CType(x, SeasonBoundary)
            Dim sMonth As String = sb.StartDay.Substring(0, 2)
            Dim sDay As String = sb.StartDay.Substring(2, 2)
            Dim sYear As String = DateTime.Now.Year.ToString()           'Actual Year does not matter (ACTUALLY - it might - but not for now!)
            Dim sbParseDate As New System.Text.StringBuilder
            sbParseDate.Append(sMonth) : sbParseDate.Append("/")
            sbParseDate.Append(sDay) : sbParseDate.Append("/")
            sbParseDate.Append(sYear)
            Dim dtStartDate As DateTime = DateTime.Parse(sbParseDate.ToString)
            Dim icA As IComparable = CType(dtStartDate, IComparable)

            Dim sb2 As SeasonBoundary = CType(y, SeasonBoundary)
            Dim sMonth2 As String = sb2.StartDay.Substring(0, 2)
            Dim sDay2 As String = sb2.StartDay.Substring(2, 2)
            Dim sbParseDate2 As New System.Text.StringBuilder
            sbParseDate2.Append(sMonth2) : sbParseDate2.Append("/")
            sbParseDate2.Append(sDay2) : sbParseDate2.Append("/")
            sbParseDate2.Append(sYear)
            Dim dtStartDate2 As DateTime = DateTime.Parse(sbParseDate2.ToString)
            Dim icB As IComparable = CType(dtStartDate2, IComparable)

            Return (icA.CompareTo(icB))
        End Function

    End Class


End Class
