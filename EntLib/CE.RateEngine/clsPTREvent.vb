﻿Imports CE.RateEngine.Enums

<Serializable()> Public Class PTREvent

    Private m_startDate As DateTime
    Private m_endDate As DateTime
    Private m_baselineRule As BaselineRule
    Private m_rebate As PTREventRebate

    Public Sub New()
        m_rebate = New PTREventRebate()
    End Sub

    Public Sub New(ByVal startDate As DateTime, ByVal endDate As DateTime)
        Me.New()
        m_startDate = startDate
        m_endDate = endDate
        m_baselineRule = Enums.BaselineRule.Unspecified
    End Sub

    Public Sub New(ByVal startDate As DateTime, ByVal endDate As DateTime, ByVal baselineRule As BaselineRule)
        Me.New()
        m_startDate = startDate
        m_endDate = endDate
        m_baselineRule = baselineRule
    End Sub

#Region "Properties"

    Public Property StartDate() As DateTime
        Get
            Return m_startDate
        End Get
        Set(ByVal Value As DateTime)
            m_startDate = Value
        End Set
    End Property

    Public Property EndDate() As DateTime
        Get
            Return m_endDate
        End Get
        Set(ByVal Value As DateTime)
            m_endDate = Value
        End Set
    End Property

    Public Property BaselineRule() As BaselineRule
        Get
            Return m_baselineRule
        End Get
        Set(ByVal Value As BaselineRule)
            m_baselineRule = Value
        End Set
    End Property

    Public Property Rebate() As PTREventRebate
        Get
            Return m_rebate
        End Get
        Set(ByVal Value As PTREventRebate)
            m_rebate = Value
        End Set
    End Property

#End Region

End Class
