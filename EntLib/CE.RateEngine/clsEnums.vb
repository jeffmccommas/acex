Public Class Enums

	' All enumerations are Integers by default
	' Do not change the assigned numbers - they are critical in some cases
	' because they match database pick list tables

	Public Enum CustomerType
		Commercial = 0
		Residential = 1
		Farm = 2
	End Enum

	Public Enum FuelType
		Electric = 1
		Gas = 2
		Oil = 3
		Propane = 4
        Water = 9       ' changed to match hubstatic FuelType
        Steam = 10      ' changed to match hubstatic FuelType
    End Enum

	Public Enum ElectricUOM
		KWh = 0
        KVA = 1
        kVAH = 15
        kVAR = 16
        kVARh = 17
	End Enum

	Public Enum GasUOM
		Therm = 2
		DKTherm = 3
		CCF = 4
		MCF = 5
		M3 = 6
	End Enum

	Public Enum WaterUOM
		CCF = 7
        Gal = 8
        CGal = 9 'Foreman 13684 - new water unit
        CF = 10
        KGAL = 11
        hgal = 12
        DKTherms = 13
        hcf = 18
        lb = 19
        klb = 20
        USGL = 21
        CubicMeter = 22
        CubicFoot = 23
	End Enum

	' More to add to this enum
	Public Enum DifferenceType
		Undefined = 0
		EnergyCharges = 1
		OtherCharges = 9
	End Enum

	Public Enum DifferenceTypeCategory
		Undefined = 0
		EnergyRelated = 1
		NonEnergyRelated = 2
		TimeOfUse = 3
		Other = 4
	End Enum

    Public Enum TierType
        Undefined = 0
        RegularTier
        SeasonalTier
        TOUTier
        SeasonalTOUTier
        AdderSeasonalTier
        AdderTOUTier
        AdderTOUSeasonalTier
        AdderRegularTier
    End Enum

	Public Enum ChargeCalcType
		None = 0
		AvgDailyKWh = 1
		Daily = 2
		Monthly = 3
		PerMeterPerDay = 4
		PerDwellingUnitPerDay = 5
		SteppedDailyDemandBill = 6
		SteppedMonthlyDemandBill = 7
        SteppedDiffStepDemandBill = 8
        SteppedDiffStepDemand = 9
        PTRRebateEachEvent = 10         ' to support PeakTimeRebate, and maps to enum RebateAccountingRule
        PTRRebateEntireBillPeriod = 11  ' to support PeakTimeRebate, and maps to enum RebateAccountingRule
        AnnExcGen = 12
        AnnExcGenP = 13
        AnnExcGenOP = 14
        AnnExcGenS1 = 15
        AnnExcGenS2 = 16
        AnnExcGenCP = 17
        MonthlySeasonAdjustedAVGU = 18
        CRCDemandCharge = 19
        CRCExcessKWh = 20
        NCDemandCharge = 21
        NCDemandMultFactor = 22
        '--- CR50560 BEGIN -------------------------------------------------------------------------------------------
        CPDemandCharge = 23
        '--- CR50560 END ---------------------------------------------------------------------------------------------
    End Enum

	' Tier, BaseOrTier
	Public Enum BaseOrTier
		Undefined = 0
		Tier1 = 1
		Tier2 = 2
		Tier3 = 3
		Tier4 = 4
		Tier5 = 5
		Tier6 = 6
		TotalServiceUse = 10
		Demand = 11
        DemandBill = 12
        DemandBill2 = 13
        '--- CR50560 BEGIN -------------------------------------------------------------------------------------------
        DemandBill3 = 14
        '--- CR50560 END ---------------------------------------------------------------------------------------------
    End Enum

	Public Enum TimeOfUse
		Undefined = 0
		OnPeak = 1
		OffPeak = 2
		Shoulder1 = 3
		Shoulder2 = 4
		CriticalPeak = 5
	End Enum

	Public Enum Season
		Undefined = 0
		Summer = 1
		Winter = 2
		SeasonA = 3
		SeasonB = 4
	End Enum

	Public Enum ServiceStep
		Undefined = 0
		Step1 = 1
		Step2 = 2
		Step3 = 3
		Step4 = 4
		Step5 = 5
		Step6 = 6
		Step7 = 7
		Step8 = 8
	End Enum

	Public Enum PartType
		Primary = 0
		Secondary = 1
		Tertiary = 2
		Quaternary = 3
		Quinary = 4
		Senary = 5
		Septenary = 6
		Octonary = 7
	End Enum

	Public Enum DayType
        Weekday = 0
		WeekendHoliday = 1
		Weekend = 2
		Saturday = 3
		Sunday = 4
		Holiday = 5
        All = 6
        Monday = 7
        Tuesday = 8
        Wednesday = 9
        Thursday = 10
        Friday = 11
	End Enum

	Public Enum ProrateType
		UseLatest = 1
		UsePrevious = 2
		SplitBillDays = 3
	End Enum

    Public Enum DemandProrateType
        UseLatest = 1
        UsePrevious = 2
        SplitBillDaysMaxDemand = 3
        SplitBillDaysPeriodDemand = 4
    End Enum

	Public Enum Language
		English = 0
		Spanish = 1
		Chinese = 2
	End Enum

	Public Enum ServiceType
		Distribution = 1
		Supply = 2
		Both = 3
	End Enum

	Public Enum TierStructureType
		Undefined = 0
		MonthlyBounds = 1
		DailyBounds = 2
		DailyBoundsPerDwellingUnit = 3
		PercentBaseTier1 = 4
		PercentBaseTier1WithDwellingUnits = 5
		DemandHours = 6
        DemandKWhCombo = 7
        MonthlyBoundsHybrid1 = 8
        MonthlyBoundsFromAVGU = 9
	End Enum

	Public Enum DemandStructureType
		Undefined = 0
		Demand = 1
	End Enum

	Public Enum SeasonalProrateType
		UseLatest = 1
		UsePrevious = 2
		SplitBillDays = 3
    End Enum

    Public Enum SeasonalDemandProrateType
        UseLatest = 1
        UsePrevious = 2
        SplitBillDaysMaxDemand = 3
        SplitBillDaysPeriodDemand = 4
    End Enum

    Public Enum LoadType
        General = 0
        Heating = 1
        Cooling = 2
        WaterHeating = 3
        Irrigation = 4 'Foreman 13684 - new load type for Irrigation
        All = 10
    End Enum

    Public Enum BilledDemandType
        Undefined = 0
        CurrentBillOnPeakDemand = 1
        CurrentBillOffPeakDemand = 2
        CurrentBillMaxDemand = 3
        CurrentBillMaxDemandkVA = 4 ' CR 15285 Aug 2011 - new billed demand type for Demand kVA
    End Enum

	' They are out of order so they could be grouped easier for reading
	' This corresponds directly to the CostType table (Do NOT change w/o speaking with mganley)
	Public Enum CostType
		DIST = 6
		DIST_CUST = 29
		DIST_DMND = 12
		DIST_DMND_ALLOTHER = 23
		DIST_DMND_OFFPEAK = 22
		DIST_DMND_ONPEAK = 21
		DIST_DMND_SHOULDER1 = 19
		DIST_DMND_SHOULDER2 = 20
		DIST_ENGY = 7
		DIST_ENGY_ALLOTHER = 28
		DIST_ENGY_OFFPEAK = 9
		DIST_ENGY_ONPEAK = 8
		DIST_ENGY_SHOULDER1 = 26
		DIST_ENGY_SHOULDER2 = 27
		DIST_TAX = 30
        GOVT = 500
        GOVT_OTHR_BOND = 340
		GOVT_TAX = 505
		GOVT_TAX_CNTY = 501
		GOVT_TAX_MUNI = 503
		GOVT_TAX_STAT = 502
		STD = 0
		STD_AOTH = 3
		STD_CUST = 1
		STD_DMND = 24
		STD_DMND_ALLOTHER = 18
		STD_DMND_CUST = 13
		STD_DMND_OFFPEAK = 15
		STD_DMND_ONPEAK = 14
		STD_DMND_SHOULDER1 = 16
		STD_DMND_SHOULDER2 = 17
		STD_ENGY = 2
		STD_ENGY_ALLOTHER = 140
		STD_ENGY_OFFPEAK = 101
		STD_ENGY_ONPEAK = 100
		STD_ENGY_SHOULDER1 = 102
		STD_ENGY_SHOULDER2 = 103
		STD_TAX = 25
		STD_XLIT = 4
		SUPP_CUST = 43
		SUPP_DMND = 31
		SUPP_DMND_ALLOTHER = 32
		SUPP_DMND_OFFPEAK = 33
		SUPP_DMND_ONPEAK = 34
		SUPP_DMND_SHOULDER1 = 35
		SUPP_DMND_SHOULDER2 = 36
		SUPP_ENGY = 38
        SUPP_ENGY_ALLOTHER = 39
        SUPP_ENGY_GREEN1 = 341
        SUPP_ENGY_GREEN2 = 342
        SUPP_ENGY_GREEN3 = 343
        SUPP_ENGY_OFFPEAK = 40
		SUPP_ENGY_ONPEAK = 41
		SUPP_ENGY_SHOULDER1 = 42
		SUPP_ENGY_SHOULDER2 = 52
		SUPP_TAX = 37
		STD_ENGY_CRITPEAK = 150
		SUPP_ENGY_CRITPEAK = 55
		DIST_ENGY_CRITPEAK = 58
		GOVT_OTHR_ONPEAK = 509
		GOVT_TRSN_ONPEAK = 510
		GOVT_OTHR_OFFPEAK = 511
		GOVT_TRSN_OFFPEAK = 512
		GOVT_OTHR_SHOULDER1 = 513
		GOVT_TRSN_SHOULDER1 = 514
		GOVT_OTHR_SHOULDER2 = 515
		GOVT_TRSN_SHOULDER2 = 516
		GEN_ENGY_ONPEAK = 180
		GEN_OTHR_ONPEAK = 181
		GEN_ENGY_OFFPEAK = 182
		GEN_OTHR_OFFPEAK = 183
		GEN_ENGY_SHOULDER1 = 184
		GEN_OTHR_SHOULDER1 = 185
		GEN_ENGY_SHOULDER2 = 186
        GEN_OTHR_SHOULDER2 = 187
        GEN_ENGY = 188
		TRAN_ENGY_SHOULDER2 = 200
		TRAN_OTHR_SHOULDER2 = 201
		TRAN_ENGY_OFFPEAK = 202
		TRAN_OTHR_OFFPEAK = 203
		TRAN_ENGY_SHOULDER1 = 204
		TRAN_OTHR_SHOULDER1 = 205
		TRAN_ENGY_ONPEAK = 206
		TRAN_OTHR_ONPEAK = 207
		STD_AOTH_SHOULDER2 = 160
		STD_AOTH_SHOULDER1 = 162
		STD_AOTH_OFFPEAK = 163
		STD_AOTH_ONPEAK = 165
		STD_ENGY_TIER4 = 166
		STD_DMND_TIER1 = 167
		STD_DMND_TIER2 = 168
		REV_FADJ_SHOULDER2 = 220
		REV_FADJ_SHOULDER1 = 221
		REV_FADJ_OFFPEAK = 222
		REV_FADJ_ONPEAK = 223
		SUPP_CREDIT = 517
		DIST_CREDIT = 518
        STD_CREDIT = 519
        STD_ENGY_LTHEATING = 151
        STD_ENGY_LTCOOLING = 152
        STD_ENGY_LTWATERHEATING = 153
        STD_ENGY_LTALL = 154
        STD_CUST_LTHEATING = 600
        STD_CUST_LTCOOLING = 601
        STD_CUST_LTWATERHEATING = 602
        STD_CUST_LTALL = 603
        STD_DMND_LTHEATING = 610
        STD_DMND_LTCOOLING = 611
        STD_DMND_LTWATERHEATING = 612
        STD_DMND_LTALL = 613
        STD_DMND_CRC = 614
        STD_DMND_NCD = 615
        '--- CR50560 BEGIN -------------------------------------------------------------------------------------------
        STD_DMND_CD = 616
        '--- CR50560 END ---------------------------------------------------------------------------------------------
        PTR_REBATE = 800
        GEN_REBATE = 801
        RCMA_TAXCOST = 910             'used to store tax/discount from rate class modifier attributes
        RCMA_DISCOUNTBEFORETAX = 911   'used to store tax/discount from rate class modifier attributes
        RCMA_DISCOUNTAFTERTAX = 912    'used to store tax/discount from rate class modifier attributes
	End Enum

	' Used by Extras for RateCompanyMap lookup in HubStatic 
	' (this is the only non direct RateEngine Enum)
	Public Enum SourceType
		Referrer = 1
		Supplier = 2
    End Enum

    Public Enum ErrCode
        NoError = 0
        ErrGeneric
        ErrTOUComplex
        ErrAllReadingsNotProvidedForRTP
        ErrUnsupportedComplexTieredTOU
        ErrUnsupportedCalculation
        ErrRateDefinitionsNotFound
        ErrUnsupportedProjectedCalculation
        ErrUnableToApplyRateClassModifier
        ErrRequiredReadingsAreMissing
    End Enum

    Public Enum RTPStream
        None = 0
        RealTimeOnly = 1
        DayAheadOnly = 2
        RealTimeAndDayAhead = 3
        CriticalPeakOnly = 4
        PeakTimeRebateOnly = 5
    End Enum

    Public Enum RTPActualStream
        DayAhead = 1
        RealTimePrelim = 2
        RealTimeFinal = 3
        CriticalPeakOnly = 4    ' Note that this is also used for PeakTimeRebateOnly as well, since that is a special sub-category of CPP
    End Enum

    ' The numbers here match the AMRInterval outside of the rate engine;
    Public Enum ReadingInterval
        FifteenMinute = 1
        Hourly
        Daily
        HalfHourly 'Foreman 12636 Sept 2009 LC - 30-mins reading interval
        FiveMinute 'CR 16664 May 2011 LC - 5-mins reading interval
    End Enum

    'Rate engine maintenance: charge types.
    Public Enum ChargeType
        Unspecified = 0
        Minimum = 1
        Service = 2
        Tax = 3
        Use = 4
    End Enum

    'Rate engine maintenance: import status.
    Public Enum ImportStatus
        Pending = 0
        Validated = 1
        [Error] = 2
        Comitted = 3
    End Enum

    ' For Peak Time Rebate Support
    Public Enum BaselineRule
        Unspecified = 0
        ThreeOfTen = 1
        FiveOfThirty = 2
    End Enum

    ' For Peak Time Rebate Support
    Public Enum RebateAccountingRule
        Unspecified = 0
        EachEvent = 1
        EntireBillPeriod = 2
    End Enum

    ' For CostToDate Support
    Public Enum MinimumChargeType
        NoMinimumCharge = 0
        ProrateMinimumCharge = 1
        NoProrateMinimumCharge = 2
    End Enum

    'For Net Metering Support, first primarily solar
    Public Enum NetMeteringType
        Unspecified = 0
        LikeEPR6 = 1
        Nem3 = 2
        Nem4 = 3
        Nem5 = 4
    End Enum

    'For use with functionality that allows what we called "rate engine attributes or rate helpers" to
    'be supplied in bill data or provided directly from web services or other calling applications.
    Public Enum RateClassModifierType
        RateClass                   'rc     'rate class
        TaxCostFactor               'tcf    'tax cost factor
        TaxUsageFactor              'tuf    'tax usage factor
        DiscountBeforeTaxFactor     'dbtf   'discount before tax factor
        DiscountAfterTaxFactor      'datf   'discount after tax factor
        NetMeteredRule              'nmr    'net metered rule
        IsRetailSupply              'irs    'is retail supply
        IsDemandResponseEligible    'idre   'is demand response eligible
        DemandResponseRebateClass   'drrc   'demand response rebate class 
        BaselineCode                'bc     'baseline code (_xx)
        DemandResponseThreshold     'drt    'demand response threshold
        BTDAdjustmentAmount         'baa    'btd adustment amount
        SupplyCostTypeSwap          'sswap  'supply cost type swap
        ExcludeCostTypes            'exclct 'exclude cost types for cost and discount
        DiscountExcludeCostTypes    'dexclct 'exclude cost types for discount only
        RateProgramId               'rpid   'can be used by an application and content management, pass-through
        TypeOfCollection            'type   'used to describe what the collection of rate class helper attributes applies to
        InitialComparisonRateProg   'icrp   'Initial comparison rate program, pass through
        AnnualDemandBillDemand      'adbd   'Annual Demand replacement for DemandBill usage
        ADBDFactor                  'adbdf  'Annual Demand Bill Demand Factor 
        ADBDRule                    'adbdrule 'Annual Demand Bill Demand Rule for excluding offpeak and shoulder2 TOU's
        AverageUsage                'avgu   'Average Usage for seasonal tiered AVGU and Seasonal ChargeCalc AVGU
        ActualConsumptionFactor     'acfu   'Actual Consumption factor for Seasonal ChargeCalc AVGU
        DayUsage                    'dayu   'Day Usage for use with AVGU
        RoundingDecimalPlaces       'rdp    'Rounding decimal places.  internal default is 3.
        CapResDemandValue           'crkw   'Capacity Reserve Demand Value.  No default.  Must be passed for CRC customers.
        CapReskWCharge              'crkwc  'Capacity Reserve kW charge.  Default can be authored.  This value would override.
        CapResExcesskWhCharge       'crkwhc 'Capacity Reserve Excess kWh charge.  Default can be authored. This value would override.
        NonCoDemandAnnMax           'ncdm   'Non-coincident Demand Annual Maximum kW.  No default.  Must be passed for non-coinc demand customers.
        NonCokWCharge               'nckwc  'Non-coincident Demand kW charge.  Default can be authored.  This value would override.
        NonCoMultFactor             'ncdf   'Non-coincident Demand multiplication factor. Default can be authored.  This value would override.
        '--- CR50560 BEGIN -------------------------------------------------------------------------------------------
        CokWCharge                  'cpkwc  'Coincident Peak Demand kW charge. Default can be authored. This value would override.
        CoMultFactor                'cpcf   'Coincident Peak Demand multiplication factor. No default available from authoring.
        CoDateTime                  'cptime 'Coincident Peak Demand date/time. No default. No default available from authoring.
        '--- CR50560 END ---------------------------------------------------------------------------------------------
        RenewableFactor             'renew  ' co2 renewable factor
    End Enum

    ' CR 20298, 20299, 20302 Oct 2011 - enum for Demand Response Rebate Class
    Public Enum RebateClass
        Undefined = 0
        Class1 = 1
        Class2 = 2
        Class3 = 3
        Class4 = 4
        Class5 = 5
    End Enum

    Public Enum DataSourceType
        Database = 0    'customary data source
        XmlFile = 1     'unsupported data source at this time
    End Enum


    ''' <summary>
    ''' CachingType for caching functionality.
    ''' </summary>
    ''' <remarks>
    ''' 0 - NoCache
    ''' 1 - Uses System.Runtime.Caching.
    ''' 2 - Uses AppFabric and caches locally through fabric.
    ''' 3 - Uses larger cache objects locally for entire lists of objects.
    ''' 4 - Uses larger cache objects via distributed AppFabric for entire lists of objects.
    ''' </remarks>
    Public Enum CachingType
        NoCache = 0
        LocalCache = 1
        DistributedCache = 2
        FlatLocalCache = 3
        FlatDistributedCache = 4
    End Enum

    ''' <summary>
    ''' Used for adjusting readings before and after binning within the usage determinants determination logic.
    ''' </summary>
    ''' <remarks></remarks>
    Public Enum ReadingAdjustmentFactorType
        GeneralConservation
        TypicalResponse
        CriticalResponse
        DemandControl
    End Enum

    '12.03 CR 27352, 33212 - new enum for special hourly interval type
    Public Enum SpecialIntervalType
        IntervalRegular = 0 'to indicate regular interval
        Interval2Hour 'to indicate special 2 hour interval
        Interval4Hour 'to indicate special 4 hour interval
        Interval6Hour 'to indicate special 6 hour interval
        Interval12Hour 'to indicate special 12 hour interval
    End Enum

End Class



