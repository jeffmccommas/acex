﻿using System;
using System.Collections.Generic;
using System.Linq;
using AO.DataAccess.Contracts;
using AO.VEE.DataAccess.Contracts;
using AO.VEE.Domain;
using AO.VEE.DTO;
using AutoMapper;
using CE.AO.Models;
using Constants = AO.VEE.Domain.Constants;

namespace AO.VEE.DataAccess
{
	/// <summary>
	/// VeeConsumptionByMeterRepository class is used to fetch/insert/update data from/to the database
	/// </summary>
	public class VeeConsumptionByMeterRepository : IVeeConsumptionByMeterRepository
	{
		public VeeConsumptionByMeterRepository(LogModel logModel, Lazy<ICassandraRepository> cassandraRepository)
		{
			LogModel = logModel;
			CassandraRepository = cassandraRepository;
		}

		public LogModel LogModel { get; set; }
		private Lazy<ICassandraRepository> CassandraRepository { get; }

		/// <summary>
		/// This method is used to fetch the VeeConsumptionByMeter details.
		/// </summary>
		/// <param name="clientId">Client Id of the data to be fetched</param>
		/// <param name="meterId">Meter Id of the data to be fetched</param>
		public VeeConsumptionByMeterDto GetLastVeeConsumptionByMeter(int clientId, string meterId)
		{
			var veeConsumptionByMeterDomain = CassandraRepository.Value.GetSingle<VeeConsumptionByMeterDomain>(Constants.CassandraTableNames.VeeConsumptionByMeter, a => a.ClientId == clientId && a.MeterId == meterId);
			var veeConsumptionByMeterDto = Mapper.Map<VeeConsumptionByMeterDomain, VeeConsumptionByMeterDto>(veeConsumptionByMeterDomain);
			return veeConsumptionByMeterDto;
		}

		/// <summary>
		/// To insert/update the VeeConsumptionByMeter data in batch to the database
		/// </summary>
		/// <param name="veeConsumptions">List of VeeConsumptionByMeter data which is to be inserted/updated</param>
		public void InsertOrMergeBatchVeeConsumptionByMeterAsync(List<VeeConsumptionByMeterDto> veeConsumptions)
		{
			IList<VeeConsumptionByMeterDomain> entities = Mapper.Map<IList<VeeConsumptionByMeterDto>, IList<VeeConsumptionByMeterDomain>>(veeConsumptions);
			CassandraRepository.Value.InsertOrUpdateBatchAsync(entities.ToList(), Constants.CassandraTableNames.VeeConsumptionByMeter).ConfigureAwait(false);
		}

		/// <summary>
		/// To delete the VeeConsumptionByMeter data in batch to the database
		/// </summary>
		/// <param name="veeConsumptions">VeeConsumptionByMeter data which is to be deleted</param>
		public void DeleteVeeConsumptionByMeter(VeeConsumptionByMeterDto veeConsumptions)
		{
			var entities = Mapper.Map<VeeConsumptionByMeterDto, VeeConsumptionByMeterDomain>(veeConsumptions);
			CassandraRepository.Value.Delete<VeeConsumptionByMeterDomain>(Constants.CassandraTableNames.VeeConsumptionByMeter,
				v => v.ClientId == entities.ClientId && v.MeterId == entities.MeterId);
		}
	}
}
