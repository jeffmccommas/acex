﻿using System;
using System.Collections.Generic;
using AO.DataAccess.Contracts;
using AO.VEE.DataAccess.Contracts;
using AO.VEE.Domain;
using AO.VEE.DTO;
using AutoMapper;
using CE.AO.Models;
using Constants = AO.VEE.Domain.Constants;

namespace AO.VEE.DataAccess
{
	/// <summary>
	/// VeeStagedClientMeterByDateRepository class is used to fetch/insert/update data from/to the database
	/// </summary>
	public class VeeStagedClientMeterByDateRepository : IVeeStagedClientMeterByDateRepository
	{
		public VeeStagedClientMeterByDateRepository(LogModel logModel, Lazy<ICassandraRepository> cassandraRepository)
		{
			LogModel = logModel;
			CassandraRepository = cassandraRepository;
		}

		public LogModel LogModel { get; set; }
		private Lazy<ICassandraRepository> CassandraRepository { get; }

		/// <summary>
		/// This method is used to fetch the VeeStagedClientMeterByDate details.
		/// </summary>
		/// <param name="clientId">Client Id of the data to be fetched</param>
		/// <param name="meterId">Meter Id of the data to be fetched</param>
		/// <returns>List of model</returns>
		public IList<VeeStagedClientMeterByDateDto> GetVeeStagedDataList(int clientId, string meterId)
		{
			IList<VeeStagedClientMeterByDateDomain> veeStagedClientMeterByDateDomainList = CassandraRepository.Value.GetAsync<VeeStagedClientMeterByDateDomain>(Constants.CassandraTableNames.VeeStagedClientMeterByDate, a => a.ClientId == clientId && a.MeterId == meterId).Result;
			IList<VeeStagedClientMeterByDateDto> veeStagedClientMeterByDateDtoList = Mapper.Map<IList<VeeStagedClientMeterByDateDomain>, IList<VeeStagedClientMeterByDateDto>>(veeStagedClientMeterByDateDomainList);
			return veeStagedClientMeterByDateDtoList.Count > 0 ? veeStagedClientMeterByDateDtoList : null;
		}

		/// <summary>
		/// To insert/update the VeeStagedClientMeterByDateDto model to the database
		/// </summary>
		/// <param name="model">VeeStagedClientMeterByDateDto model which is to be inserted/updated</param>
		public void InsertOrMergeVeeStagedDataAsync(VeeStagedClientMeterByDateDto model)
		{
			var entityModel = Mapper.Map<VeeStagedClientMeterByDateDto, VeeStagedClientMeterByDateDomain>(model);
			CassandraRepository.Value.InsertOrUpdateAsync(entityModel, Constants.CassandraTableNames.VeeStagedClientMeterByDate).ConfigureAwait(false);
		}

		/// <summary>
		/// To delete the data from database
		/// </summary>
		/// <param name="model">VeeStagedClientMeterByDateDto model which is to be inserted/updated</param>
		public void DeleteVeeStagedData(VeeStagedClientMeterByDateDto model)
		{
			CassandraRepository.Value.Delete<VeeStagedClientMeterByDateDomain>(Constants.CassandraTableNames.VeeStagedClientMeterByDate,
					a => a.ClientId == model.ClientId && a.MeterId == model.MeterId && a.AmiDate == model.AmiDate);
		}
	}
}
