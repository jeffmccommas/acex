using Microsoft.WindowsAzure.ServiceRuntime;
using System;
using System.Diagnostics;
using System.Net;
using System.Threading;
using AO.BusinessContracts;
using AO.Registrar;
using CE.AO.Logging;
using Microsoft.Practices.Unity;


namespace CE.AO.DataImport.EventProcessWorker {
    public class WorkerRole : RoleEntryPoint, IDisposable
    {
        private ITriggerSubscriber _eventProcessSubscriber;
        public static IUnityContainer UnityContainer { get; set; }
        private CancellationTokenSource _cancellationTokenSource = new CancellationTokenSource();
        private ManualResetEvent _runCompleteEvent = new ManualResetEvent(false);

        public override void Run()
        {
            Trace.TraceInformation("CE.AO.DataImport.EventProcessWorker is running");

            try {
                if (!_cancellationTokenSource.Token.IsCancellationRequested)
                {
                    UnityContainer = new UnityContainer();
                    new DataStorageRegistrar().Initialize<TransientLifetimeManager>(UnityContainer);
                    _eventProcessSubscriber = UnityContainer.Resolve<ITriggerSubscriber>("EventProcessTriggerSubscriber");
                    _eventProcessSubscriber.Subscribe();
                }
                //Thread.Sleep(30000);
                //SimpleWebServerRole.ProcessDataFileTest();


                //Wait for shutdown to be called, else the role will recycle
                _runCompleteEvent.WaitOne();
            }
            finally
            {
                _runCompleteEvent.Set();
            }
        }

        public override bool OnStart()
        {
            // Set the maximum number of concurrent connections
            ServicePointManager.DefaultConnectionLimit = 12;

            // For information on handling configuration changes
            // see the MSDN topic at http://go.microsoft.com/fwlink/?LinkId=166357.

            bool result = base.OnStart();

            Trace.TraceInformation("CE.AO.DataImport.EventProcessWorker has been started");

            return result;
        }

        public override void OnStop()
        {
            Trace.TraceInformation("CE.AO.DataImport.EventProcessWorker is stopping");

            _cancellationTokenSource.Cancel();
            _runCompleteEvent.WaitOne();

            try
            {
                _eventProcessSubscriber.UnSubscribe();
            }
            catch (Exception oops)
            {
                Trace.TraceError(oops.Message);
            }
            finally
            {
                Logger.Fatal($"Event Process Worker (Instance: {RoleEnvironment.CurrentRoleInstance.Id}) stopped", null);
            }

            base.OnStop();

            Trace.TraceInformation("CE.AO.DataImport.EventProcessWorker has stopped");
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (_cancellationTokenSource != null)
            {
                _cancellationTokenSource.Cancel();
                _cancellationTokenSource.Dispose();
                _cancellationTokenSource = null;
            }
            if (_runCompleteEvent != null)
            {
                _runCompleteEvent.Dispose();
                _runCompleteEvent = null;
            }
        }
    }


}
