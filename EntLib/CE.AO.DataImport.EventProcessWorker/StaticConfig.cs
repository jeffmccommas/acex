﻿namespace CE.AO.DataImport.EventProcessWorker
{
    /// <summary>
    /// Constant variables are defined in this class
    /// </summary>
    public static class StaticConfig
    {
        public const string ImportDataEventHubManageConnectionString = "ImportDataEventHubManageConnectionString";
        public const string ImportDataEventHubName = "ImportDataEventHubName";
        public const string ConsumerGroupNameForImportDataEventHub = "ConsumerGroupNameForImportDataEventHub";
        public const string NumberOfPartitionsForImportDataEventHub = "NumberOfPartitionsForImportDataEventHub";
        public const string AclaraOneStorageConnectionString = "AclaraOneStorageConnectionString";
    }
}
