<?xml version="1.0" encoding="utf-8"?>
<configuration>
  <configSections>
    <section name="log4net" type="log4net.Config.Log4NetConfigurationSectionHandler,log4net"/>
    <section name="unity" type="Microsoft.Practices.Unity.Configuration.UnityConfigurationSection, Microsoft.Practices.Unity.Configuration"/>
  </configSections>
  <unity xmlns="http://schemas.microsoft.com/practices/2010/unity">

    <assembly name="CE.AO.Business"/>
    <assembly name="CE.AO.DataAccess"/>
    <namespace name="CE.AO.Business"/>
    <namespace name="CE.AO.Business.Contract"/>
    <namespace name="CE.AO.DataAccess"/>

    <container>
      <register type="ISubscription" mapTo="Subscription"/>
      <register name="LogEnableSubscription" type="ISubscription" mapTo="Subscription">
        <constructor>
          <param name="logModel"/>
        </constructor>
      </register>

      <register type="IAccountUpdatesFacade" mapTo="AccountUpdatesFacade"/>
      <register name="LogEnableAccountUpdatesFacade" type="IAccountUpdatesFacade" mapTo="AccountUpdatesFacade">
        <constructor>
          <param name="logModel"/>
        </constructor>
      </register>

      <register type="ITableRepository" mapTo="TableRepository"></register>

      <register type="IAccountUpdates" mapTo="AccountUpdates"/>
      <register name="LogEnableAccountUpdates" type="IAccountUpdates" mapTo="AccountUpdates">
        <constructor>
          <param name="logModel"/>
        </constructor>
      </register>

      <register type="IBilling" mapTo="Billing"/>
      <register name="LogEnableBilling" type="IBilling" mapTo="Billing">
        <constructor>
          <param name="logModel"/>
        </constructor>
      </register>

      <register type="ICustomer" mapTo="Customer"/>
      <register name="LogEnableCustomer" type="ICustomer" mapTo="Customer">
        <constructor>
          <param name="logModel"/>
        </constructor>
      </register>

      <register type="IClientAccount" mapTo="ClientAccount"/>
      <register name="LogEnableClientAccount" type="IClientAccount" mapTo="ClientAccount">
        <constructor>
          <param name="logModel"/>
        </constructor>
      </register>

      <register type="IAccountLookup" mapTo="AccountLookup"/>
      <register name="LogEnableAccountLookup" type="IAccountLookup" mapTo="AccountLookup">
        <constructor>
          <param name="logModel"/>
        </constructor>
      </register>

      <register type="IAMI" mapTo="AMI"/>
      <register name="LogEnableAMI" type="IAMI" mapTo="AMI">
        <constructor>
          <param name="logModel"/>
        </constructor>
      </register>

      <register type="ITallAMI" mapTo="TallAMI"/>
      <register name="LogEnableTallAMI" type="ITallAMI" mapTo="TallAMI">
        <constructor>
          <param name="logModel"/>
        </constructor>
      </register>

      <register type="IAmiReading" mapTo="AmiReading"/>
      <register name="LogEnableAmiReading" type="IAmiReading" mapTo="AmiReading">
        <constructor>
          <param name="logModel"/>
        </constructor>
      </register>

      <register type="IBillingFacade" mapTo="BillingFacade"/>
      <register name="LogEnableBillingFacade" type="IBillingFacade" mapTo="BillingFacade">
        <constructor>
          <param name="logModel"/>
        </constructor>
      </register>

      <register type="IPremise" mapTo="Premise"/>
      <register name="LogEnablePremise" type="IPremise" mapTo="Premise">
        <constructor>
          <param name="logModel"/>
        </constructor>
      </register>

      <register type="ICustomerMobileNumberLookup" mapTo="CustomerMobileNumberLookup"/>
      <register name="LogEnableCustomerMobileNumberLookup" type="ICustomerMobileNumberLookup" mapTo="CustomerMobileNumberLookup">
        <constructor>
          <param name="logModel"/>
        </constructor>
      </register>

      <register type="INotification" mapTo="Notification"/>
      <register name="LogEnableNotification" type="INotification" mapTo="Notification">
        <constructor>
          <param name="logModel"/>
        </constructor>
      </register>

      <register type="ITrumpiaRequestDetail" mapTo="TrumpiaRequestDetail"/>
      <register type="IQueueRepository" mapTo="QueueRepository"></register>
    </container>
  </unity>
  <system.diagnostics>
    <trace>
      <listeners>
        <add type="Microsoft.WindowsAzure.Diagnostics.DiagnosticMonitorTraceListener, Microsoft.WindowsAzure.Diagnostics, Version=2.8.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" name="AzureDiagnostics">
          <filter type=""/>
        </add>
      </listeners>
    </trace>
  </system.diagnostics>
  <runtime>
    <assemblyBinding xmlns="urn:schemas-microsoft-com:asm.v1">
      <dependentAssembly>
        <assemblyIdentity name="AutoMapper" publicKeyToken="BE96CD2C38EF1005" culture="neutral"/>
        <bindingRedirect oldVersion="0.0.0.0-3.3.1.0" newVersion="3.3.1.0"/>
      </dependentAssembly>
      <dependentAssembly>
        <assemblyIdentity name="Newtonsoft.Json" culture="neutral" publicKeyToken="30ad4fe6b2a6aeed" />
        <bindingRedirect oldVersion="0.0.0.0-8.0.0.0" newVersion="8.0.0.0" />
      </dependentAssembly>
      <dependentAssembly>
        <assemblyIdentity name="Microsoft.WindowsAzure.Storage" publicKeyToken="31bf3856ad364e35" culture="neutral"/>
        <bindingRedirect oldVersion="0.0.0.0-6.2.0.0" newVersion="6.2.0.0"/>
      </dependentAssembly>
      <dependentAssembly>
        <assemblyIdentity name="Microsoft.Data.Services.Client" publicKeyToken="31bf3856ad364e35" culture="neutral"/>
        <bindingRedirect oldVersion="0.0.0.0-5.7.0.0" newVersion="5.7.0.0"/>
      </dependentAssembly>
      <dependentAssembly>
        <assemblyIdentity name="Microsoft.Data.OData" publicKeyToken="31bf3856ad364e35" culture="neutral"/>
        <bindingRedirect oldVersion="0.0.0.0-5.7.0.0" newVersion="5.7.0.0"/>
      </dependentAssembly>
      <dependentAssembly>
        <assemblyIdentity name="Microsoft.Data.Edm" publicKeyToken="31bf3856ad364e35" culture="neutral"/>
        <bindingRedirect oldVersion="0.0.0.0-5.7.0.0" newVersion="5.7.0.0"/>
      </dependentAssembly>
      <dependentAssembly>
        <assemblyIdentity name="System.Spatial" publicKeyToken="31bf3856ad364e35" culture="neutral"/>
        <bindingRedirect oldVersion="0.0.0.0-5.6.0.0" newVersion="5.6.0.0"/>
      </dependentAssembly>
      <dependentAssembly>
        <assemblyIdentity name="System.IO.Compression" publicKeyToken="b77a5c561934e089" culture="neutral"/>
        <bindingRedirect oldVersion="0.0.0.0-4.1.2.0" newVersion="4.1.2.0"/>
      </dependentAssembly>
      <dependentAssembly>
        <assemblyIdentity name="System.Diagnostics.DiagnosticSource" publicKeyToken="cc7b13ffcd2ddd51" culture="neutral"/>
        <bindingRedirect oldVersion="0.0.0.0-4.0.1.0" newVersion="4.0.1.0"/>
      </dependentAssembly>
      <dependentAssembly>
        <assemblyIdentity name="Microsoft.Extensions.Logging.Abstractions" publicKeyToken="adb9793829ddae60" culture="neutral" />
        <bindingRedirect oldVersion="0.0.0.0-1.1.2.0" newVersion="1.1.2.0" />
      </dependentAssembly>
      <dependentAssembly>
        <assemblyIdentity name="Microsoft.Extensions.Logging" publicKeyToken="adb9793829ddae60" culture="neutral" />
        <bindingRedirect oldVersion="0.0.0.0-1.1.2.0" newVersion="1.1.2.0" />
      </dependentAssembly>
    </assemblyBinding>
  </runtime>
  <system.serviceModel>
    <extensions>
      <!-- In this extension section we are introducing all known service bus extensions. User can remove the ones they don't need. -->
      <behaviorExtensions>
        <add name="connectionStatusBehavior" type="Microsoft.ServiceBus.Configuration.ConnectionStatusElement, Microsoft.ServiceBus, Culture=neutral, PublicKeyToken=31bf3856ad364e35"/>
        <add name="transportClientEndpointBehavior" type="Microsoft.ServiceBus.Configuration.TransportClientEndpointBehaviorElement, Microsoft.ServiceBus, Culture=neutral, PublicKeyToken=31bf3856ad364e35"/>
        <add name="serviceRegistrySettings" type="Microsoft.ServiceBus.Configuration.ServiceRegistrySettingsElement, Microsoft.ServiceBus, Culture=neutral, PublicKeyToken=31bf3856ad364e35"/>
      </behaviorExtensions>
      <bindingElementExtensions>
        <add name="netMessagingTransport" type="Microsoft.ServiceBus.Messaging.Configuration.NetMessagingTransportExtensionElement, Microsoft.ServiceBus,  Culture=neutral, PublicKeyToken=31bf3856ad364e35"/>
        <add name="tcpRelayTransport" type="Microsoft.ServiceBus.Configuration.TcpRelayTransportElement, Microsoft.ServiceBus, Culture=neutral, PublicKeyToken=31bf3856ad364e35"/>
        <add name="httpRelayTransport" type="Microsoft.ServiceBus.Configuration.HttpRelayTransportElement, Microsoft.ServiceBus, Culture=neutral, PublicKeyToken=31bf3856ad364e35"/>
        <add name="httpsRelayTransport" type="Microsoft.ServiceBus.Configuration.HttpsRelayTransportElement, Microsoft.ServiceBus, Culture=neutral, PublicKeyToken=31bf3856ad364e35"/>
        <add name="onewayRelayTransport" type="Microsoft.ServiceBus.Configuration.RelayedOnewayTransportElement, Microsoft.ServiceBus, Culture=neutral, PublicKeyToken=31bf3856ad364e35"/>
      </bindingElementExtensions>
      <bindingExtensions>
        <add name="basicHttpRelayBinding" type="Microsoft.ServiceBus.Configuration.BasicHttpRelayBindingCollectionElement, Microsoft.ServiceBus, Culture=neutral, PublicKeyToken=31bf3856ad364e35"/>
        <add name="webHttpRelayBinding" type="Microsoft.ServiceBus.Configuration.WebHttpRelayBindingCollectionElement, Microsoft.ServiceBus, Culture=neutral, PublicKeyToken=31bf3856ad364e35"/>
        <add name="ws2007HttpRelayBinding" type="Microsoft.ServiceBus.Configuration.WS2007HttpRelayBindingCollectionElement, Microsoft.ServiceBus, Culture=neutral, PublicKeyToken=31bf3856ad364e35"/>
        <add name="netTcpRelayBinding" type="Microsoft.ServiceBus.Configuration.NetTcpRelayBindingCollectionElement, Microsoft.ServiceBus, Culture=neutral, PublicKeyToken=31bf3856ad364e35"/>
        <add name="netOnewayRelayBinding" type="Microsoft.ServiceBus.Configuration.NetOnewayRelayBindingCollectionElement, Microsoft.ServiceBus, Culture=neutral, PublicKeyToken=31bf3856ad364e35"/>
        <add name="netEventRelayBinding" type="Microsoft.ServiceBus.Configuration.NetEventRelayBindingCollectionElement, Microsoft.ServiceBus, Culture=neutral, PublicKeyToken=31bf3856ad364e35"/>
        <add name="netMessagingBinding" type="Microsoft.ServiceBus.Messaging.Configuration.NetMessagingBindingCollectionElement, Microsoft.ServiceBus, Culture=neutral, PublicKeyToken=31bf3856ad364e35"/>
      </bindingExtensions>
    </extensions>
  </system.serviceModel>
  <connectionStrings>
    <clear/>
    <add name="InsightsMetaDataConn" providerName="System.Data.SqlClient" connectionString="Server=tcp:acedsql1c0.database.windows.net,1433;Database=InsightsMetaData;User ID=AclaraCEAdmin@acedsql1c0;Password=Acl@r@282;Encrypt=True;TrustServerCertificate=False;Connection Timeout=30;Min Pool Size=5;"/>
    <add name="RateEngineConn" providerName="System.Data.SqlClient" connectionString="Server=tcp:acedsql1c0.database.windows.net,1433;Database=InsightsMetaData;User ID=AclaraCEAdmin@acedsql1c0;Password=Acl@r@282;Encrypt=True;TrustServerCertificate=False;Connection Timeout=30;Min Pool Size=5;"/>
  </connectionStrings>
  <appSettings file="appSettings_dev.config">
    <add key="advAmiImportEnable" value="yes"/>

    <add key="outputPath" value="[useTempPath]"/>
    <add key="outputWeb" value="https://acl10507l/TestDataFiles/"/>

    <!-- Service Bus specific app setings for messaging connections -->
    <add key="Microsoft.ServiceBus.ConnectionString" value="Endpoint=sb://[your namespace].servicebus.windows.net;SharedAccessKeyName=RootManageSharedAccessKey;SharedAccessKey=[your secret]"/>
    <add key="ClientSettingsProvider.ServiceUri" value=""/>

    <!--<add key="AzureStorageConnectionString" value="DefaultEndpointsProtocol=https;AccountName=aclaoaclaraonestgprod;AccountKey=3lgnwo3Zxg3upUxQupXrLRtW+MsRjaGk3PRRdzD6Z4v/HuvA+eWue2GuUqC8nFX7hprdxP5r5BhkJYiobv4/vQ=="  />
    <add key="AzureTableLoggingEventHubSendConnectionString" value="Endpoint=sb://aclaoaclaraone.servicebus.windows.net/;SharedAccessKeyName=Send;SharedAccessKey=u3LEsvlQhuee0L8d5Tf+73LbfVe5ZCyZGlr3hvbtk0o=;TransportType=Amqp" />
    <add key="AzureTableLoggingEventHubPath" value="aclaoazuretableloggingehprod" />
    <add key="AzureStorageLogTableConnectionString" value="DefaultEndpointsProtocol=https;AccountName=aclaoaclaraonestgprod;AccountKey=3lgnwo3Zxg3upUxQupXrLRtW+MsRjaGk3PRRdzD6Z4v/HuvA+eWue2GuUqC8nFX7hprdxP5r5BhkJYiobv4/vQ==" />-->

    <add key="AzureStorageConnectionString" value="DefaultEndpointsProtocol=https;AccountName=aclaoaclaraonestgdev;AccountKey=5Kv12Tw2/O9+jFaHqfy9yrWf9b0uQM5xa0K9559guWhsb6ugUmS/u5FN86qCS8LtZfAYsDqtOvi8VisNaaRnXQ=="/>
    <add key="AzureTableLoggingEventHubSendConnectionString" value="Endpoint=sb://aclaoaclaraonesbnsdev.servicebus.windows.net/;SharedAccessKeyName=Send;SharedAccessKey=q530n4AmCK3MSYSh5j6299pO8vsKt4pP6zLALMy6Tv8=;TransportType=Amqp"/>
    <add key="AzureTableLoggingEventHubPath" value="aclaoazuretableloggingehdev"/>
    <add key="AzureStorageLogTableConnectionString" value="DefaultEndpointsProtocol=https;AccountName=aclaoaclaraonestgdev;AccountKey=5Kv12Tw2/O9+jFaHqfy9yrWf9b0uQM5xa0K9559guWhsb6ugUmS/u5FN86qCS8LtZfAYsDqtOvi8VisNaaRnXQ=="/>
    
    <add key="SendGridUserName" value="azure_65efb1e6264f510d34d09a73761a7c13@azure.com"/>
    <add key="SendGridPassword" value="Aclaraone1"/>
    <add key="EmailOverride" value=""/>
    <add key="FromEmail" value="Support@aclara.com"/>
    <add key="TrumpiaApiKey" value="79032678c0193f71ef98405ea63befb9"/>
    <add key="TrumpiaUserName" value="abagdasarian"/>
    <add key="RateEngineLogging" value="false"/>
    <add key="CacheExpireInSeconds" value="1800"/>
    <add key="DataStorage" value="Cassandra"/>
    <add key="CassandraNodes" value="dc0vm0zdwm6qg55fs3e.eastus.cloudapp.azure.com,dc0vm1zdwm6qg55fs3e.eastus.cloudapp.azure.com,dc0vm2zdwm6qg55fs3e.eastus.cloudapp.azure.com,dc0vm3zdwm6qg55fs3e.eastus.cloudapp.azure.com"/>
    <add key="CassandraNodeUsername" value="aclaraone"/>
    <add key="CassandraNodePassword" value="Aclara2016"/>
    <add key="CassandraKeyspace" value="aclara_one_dev"/>
    <add key="PreAggCalculationEventHubName" value="TBD"/>
    <add key="PreAggCalculationEventHubSendConnectionString" value="TBD"/>
  </appSettings>
  <system.web>
    <membership defaultProvider="ClientAuthenticationMembershipProvider">
      <providers>
        <add name="ClientAuthenticationMembershipProvider" type="System.Web.ClientServices.Providers.ClientFormsAuthenticationMembershipProvider, System.Web.Extensions, Version=4.0.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" serviceUri=""/>
      </providers>
    </membership>
    <roleManager defaultProvider="ClientRoleProvider" enabled="true">
      <providers>
        <add name="ClientRoleProvider" type="System.Web.ClientServices.Providers.ClientRoleProvider, System.Web.Extensions, Version=4.0.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" serviceUri="" cacheTimeout="86400"/>
      </providers>
    </roleManager>
  </system.web>
  <log4net>
    <root>
      <level value="WARN"/>
      <appender name="SbEventHubAppender" type="CE.AO.Logging.SbEventHubAppender, CE.AO.Logging"></appender>
    </root>
  </log4net>
  <startup>
    <supportedRuntime version="v4.0" sku=".NETFramework,Version=v4.7"/>
  </startup>
</configuration>
