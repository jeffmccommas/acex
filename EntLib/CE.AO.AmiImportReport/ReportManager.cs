﻿using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Blob;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CE.AO.AmiImportReport {

    public class ReportManager {

        private const string CsvHeaderRow = "FileName,Status,StartTime,EndTime,TotalRows,MissingRows,TotalReads,ImportedReads,MissingReads,SampleValidationErrors";
        private readonly ProcessingArgs _pa;
        private const string AclaraOneStorageConnectionString = "AclaraOneStorageConnectionString";
        /// <summary>
        /// CloudBlobClient private variable
        /// </summary>
        private CloudBlobClient _cloudBlobClient;

        public ReportManager() {
            _pa = new ProcessingArgs();
        }

        /// <summary>
        /// processing args passed from program
        /// </summary>
        /// <param name="pa"></param>
        public ReportManager(ProcessingArgs pa) {
            _pa = pa;
        }

        /// <summary>
        /// Generate ami report for selected client
        /// </summary>
        /// <returns></returns>
        public bool GenerateClientAmiReport() {
            var result = false;
            var clientId = _pa.GetControlVal("clientid", 0);
            Console.WriteLine($"GenerateClientAmiReport for {clientId} starts");
            var processStartTime = DateTime.Now;
            var containerName = $"client{clientId}";
            try {
                var maxParallelThread = _pa.GetControlVal("maxParallelThread", 100);
                DateTimeOffset endTime = _pa.GetControlVal("endTime", DateTime.UtcNow);
                DateTimeOffset startTime = endTime.AddHours(-24);
                var overwriteStartTime = _pa.GetControlVal("startTime", String.Empty);
                if (!string.IsNullOrEmpty(overwriteStartTime)) {
                    startTime = Convert.ToDateTime(overwriteStartTime);
                }
                CloudBlobContainer container = CloudBlobClient.GetContainerReference($"{containerName}");
                var directory = container.GetDirectoryReference(@"log/advami/");
                var blobList = directory.ListBlobs(true);
                var details = new List<ProcessDetail>();
                Parallel.ForEach(blobList, new ParallelOptions { MaxDegreeOfParallelism = maxParallelThread }, item => {
                    if (item is CloudBlockBlob) {
                        CloudBlockBlob blob = (CloudBlockBlob)item;
                        var filename = blob.Name;
                        if (filename.Substring(filename.Length - 11, 11) == "context.txt" &&
                            (filename.Contains("cassimport") || filename.Contains("validate") || 
                            filename.Contains("avpreproc") || filename.Contains("cvtCsv2Tall"))) {
                            var timestamp = blob.Properties.LastModified;
                            if (timestamp >= startTime && timestamp < endTime) {
                                StringWriter sw = new StringWriter();
                                using (StreamReader reader = new StreamReader(blob.OpenRead())) {
                                    while (!reader.EndOfStream) {
                                        string row = reader.ReadLine();
                                        if (!string.IsNullOrEmpty(row)) {
                                            sw.WriteLine(row);
                                        }
                                    }
                                }
                                var detail = GetProcessDetails(blob.Name, sw.ToString());
                                if (detail != null) {
                                    details.Add(detail);
                                }
                            }
                        }
                    }
                }
                );
                
                var summaries = GenerateAmiSummary(details);

                if (summaries != null && summaries.Any()) {
                    var filepath = GenerateSummaryReport(summaries);

                    if (!string.IsNullOrEmpty(filepath)) {
                        // send email
                        var errorCount = summaries.FindAll(s => s.Status == "failure").Count;
                        result = SendEmail(filepath, errorCount);
                    }
                }

            }
            catch (Exception ex) {
                Console.WriteLine(ex.Message);
            }
            Console.WriteLine($"GenerateClientAmiReport for clientId {clientId} ends, process time = {DateTime.Now.Subtract(processStartTime).TotalMilliseconds} ms");
            return result;

        }



        /// <summary>
        /// Generate List of Ami Summary, one summary per processed file
        /// ProcessDetails are grouped together by the processed file name
        /// </summary>
        /// <param name="processDetails"></param>
        /// <returns></returns>
        public List<AmiSummary> GenerateAmiSummary(List<ProcessDetail> processDetails) {
            var clientId = _pa.GetControlVal("clientid", 0);
            Console.WriteLine($"GenerateAmiSummary for {clientId} starts");
            var processStartTime = DateTime.Now;
            if (processDetails == null || !processDetails.Any()) {
                Console.WriteLine($"GenerateAmiSummary for {clientId} ends, no detail to generate summary. Process time = {DateTime.Now.Subtract(processStartTime).TotalMilliseconds} ms");
                return null;
            }

            List<AmiSummary> summaryList = null;
            try {
                summaryList = new List<AmiSummary>();
                var groupedDetails = processDetails.GroupBy(d => d.FileName);
                foreach (var details in groupedDetails) {
                    var detailList = details.OrderByDescending(d => Convert.ToDateTime(d.EndTime)).ToList();

                    var summary = new AmiSummary {
                        FileName = details.Key,
                        Processes = detailList,
                        Status = detailList.Exists(p => p.HasErrors) ? "failure" : "success",
                        StartTime = detailList.Aggregate((a, b) => Convert.ToDateTime(b.StartTime) < Convert.ToDateTime(a.StartTime) ? b : a).StartTime,
                        EndTime = detailList.Aggregate((a, b) => Convert.ToDateTime(b.EndTime) >= Convert.ToDateTime(a.EndTime) ? b : a).EndTime,
                        Total = detailList.Find(p => p.Type == ProcessType.Perproc)?.Total ?? details.ToList().Find(p => p.Type == ProcessType.Import)?.Total ?? 0,
                        MissingConvertedReads = detailList.Find(p => p.Type == ProcessType.Perproc)?.Missing ?? 0,
                        ConvertedReads = detailList.Find(p => p.Type == ProcessType.Perproc)?.Processed ?? 0,
                        ImportedReads = detailList.Find(p => p.Type == ProcessType.Import)?.Processed ?? 0,
                        MissingsReads = detailList.Find(p => p.Type == ProcessType.Import)?.Missing ?? 0,
                        ValidationError = detailList.Find(p => p.Type == ProcessType.Validate)?.Missing ?? 0
                    };

                    summaryList.Add(summary);
                }
            }
            catch (Exception ex) {
                Console.WriteLine(ex.Message);
            }

            Console.WriteLine($"GenerateAmiSummary for {clientId} ends, process time = {DateTime.Now.Subtract(processStartTime).TotalMilliseconds} ms");
            return summaryList;
        }

        /// <summary>
        /// Generate Summary Report in csv format based on the list of AmiSummary
        /// </summary>
        /// <param name="summaries"></param>
        /// <returns></returns>
        public string GenerateSummaryReport(List<AmiSummary> summaries) {
            var processStartTime = DateTime.Now;
            string filepath;
            var filename = $"AmiImportSummaryReport_{processStartTime:yyyy-MM-dd_HHmmssfff}.csv";
            Console.WriteLine($"GenerateSummaryReport for {filename} starts");
            if (summaries == null || !summaries.Any()) {
                Console.WriteLine($"GenerateSummaryReport ends, no summary report is generated. Process time = {DateTime.Now.Subtract(processStartTime).TotalMilliseconds} ms");
                return string.Empty;
            }
            
            try {
                var clientId = _pa.GetControlVal("clientid", 0);
                var env = _pa.GetControlVal("aclenv", "dev");
                var outputLoc = _pa.GetControlVal($"{env}outputLocation", string.Empty);

                if (outputLoc == string.Empty)
                    throw new Exception("output location configuration is missing, no summary generated");

                var path = string.Format(outputLoc, clientId);
                filepath = $"{path}{filename}";
                if (!Directory.Exists(path)) Directory.CreateDirectory(path);

                var sw = new StringWriter();
                sw.WriteLine(CsvHeaderRow);
                foreach (var summary in summaries) {
                    sw.WriteLine(
                        $"{summary.FileName},{summary.Status},{summary.StartTime},{summary.EndTime},{summary.Total},{summary.MissingConvertedReads},{summary.ConvertedReads},{summary.ImportedReads},{summary.MissingsReads},{summary.ValidationError}");
                }

                File.WriteAllText(filepath, sw.ToString());
            }
            catch (Exception ex) {
                Console.WriteLine(ex.Message);
                filepath = null;
            }
            Console.WriteLine($"GenerateSummaryReport for {filename} ends, process time = {DateTime.Now.Subtract(processStartTime).TotalMilliseconds} ms");
            return filepath;
        }

        /// <summary>
        /// Get the process info from the content of log file
        /// </summary>
        /// <param name="filename"></param>
        /// <param name="content"></param>
        /// <returns></returns>
        public ProcessDetail GetProcessDetails(string filename, string content) {

            Console.WriteLine($"GetProcessReports for {filename} starts");
            var processStartTime = DateTime.Now;
            if (string.IsNullOrEmpty(content)) {
                Console.WriteLine($"GetProcessReports for {filename} ends, content is empty. Process time = {DateTime.Now.Subtract(processStartTime).TotalMilliseconds} ms");
                return null;
            }

            ProcessDetail detail;
            try {
                detail = new ProcessDetail {
                    Type = filename.Contains("cassimport")
                        ? ProcessType.Import
                        : filename.Contains("validate")
                            ? ProcessType.Validate
                            : ProcessType.Perproc,
                    StartTime = DateTime.MaxValue.ToString(CultureInfo.InvariantCulture),
                    EndTime = DateTime.MinValue.ToString(CultureInfo.InvariantCulture)
                };
                using (StringReader reader = new StringReader(content)) {
                    string line;
                    while ((line = reader.ReadLine()) != null) {
                        var lineAry = line.Trim().Split(']');
                        if (lineAry.GetUpperBound(0) > 0) {
                            var time = lineAry[0].Replace("[", "").Trim();
                            var logInfo = lineAry[1].Trim();
                            // get file name of the processed file
                            if (logInfo.Contains("**processFile**")) {
                                logInfo = logInfo.Replace(@"/", @"\");
                                var processFileAry = logInfo.Split('\\');
                                if (processFileAry.GetUpperBound(0) > 0) {
                                    detail.FileName = processFileAry[processFileAry.GetUpperBound(0)];
                                }
                            }

                            // get start and end for the process
                            GetStartAndEndTime(time, logInfo, ref detail);

                            // get counter info for rows/reads/missing/processed
                            if (logInfo.Contains("**EndCounters**")) {
                                GetCountersInfo(logInfo, ref detail);
                            }
                        }

                    }
                }
            }
            catch (Exception ex) {
                Console.WriteLine(ex.Message);
                detail = null;
            }
            Console.WriteLine($"GetProcessReports for {filename} ends, process time = {DateTime.Now.Subtract(processStartTime).TotalMilliseconds} ms");
            return detail;
        }

        /// <summary>
        /// Get the start and end Time from the log file content 
        /// prefix for start and end in the log file is different per process
        /// </summary>
        /// <param name="time"></param>
        /// <param name="logInfo"></param>
        /// <param name="detail"></param>
        private void GetStartAndEndTime(string time, string logInfo, ref ProcessDetail detail) {
            if (detail.Type == ProcessType.Perproc) {
                // get start time of the process
                if (logInfo.Contains("**start ")) {
                    detail.StartTime = time;
                }
                // get end time of the process 
                if (logInfo.Contains("**end ampreproc") || logInfo.Contains("**end avpreproc")) {
                    detail.EndTime = time;
                }
            }
            else if (detail.Type == ProcessType.Import) {
                // get start time of the process
                if (logInfo.Contains("**start ")) {
                    detail.StartTime = time;
                }
                // get end time of the process 
                if (logInfo.Contains("**end cassimport")) {
                    detail.EndTime = time;
                }
            }
            else {
                // get start time of the process
                if (logInfo.Contains("**analStart**")) {
                    detail.StartTime = time;
                }
                // get end time of the process 
                if (logInfo.Contains("**analComplete")) {
                    detail.EndTime = time;
                }
            }
        }

        /// <summary>
        /// Get the counters info (rows/reads/missing/processed)
        /// counter info key in the log file is different per process
        /// </summary>
        /// <param name="logInfo"></param>
        /// <param name="detail"></param>
        private void GetCountersInfo(string logInfo, ref ProcessDetail detail) {
            var endCounters = logInfo.Replace("**EndCounters**", "").Trim();
            if (endCounters[endCounters.Length - 1] == ',') {
                endCounters = endCounters.Remove(endCounters.Length - 1, 1);
            }
            var endCountersAry = endCounters.Split(',');
            var countPairs = new Dictionary<string, int>();
            for (int i = 0; i < endCountersAry.Length; i++) {
                var counterAry = endCountersAry[i].Split('=');
                countPairs.Add(counterAry[0].ToLower(), Convert.ToInt32(counterAry[1]));
            }
            if (countPairs.Any()) {
                // check if there's error in the process
                detail.HasErrors = countPairs.ContainsKey("errors") && countPairs["errors"] != 0;

                if (detail.Type == ProcessType.Perproc) {
                    // get converted rows
                    detail.Processed = countPairs.ContainsKey("xformrows")
                        ? countPairs["xformrows"]
                        : countPairs.ContainsKey("unzip2tallreccnt")
                            ? countPairs["unzip2tallreccnt"]
                            : 0;
                    detail.Missing = countPairs.ContainsKey("totalrows") ? countPairs["totalrows"] - detail.Processed :  countPairs.ContainsKey("errors") ? countPairs["errors"] : 0;
                    // get total rows for preproce
                    detail.Total = countPairs.ContainsKey("totalrows") ? countPairs["totalrows"] : detail.Processed + detail.Missing;

                }
                else if (detail.Type == ProcessType.Import) {
                    // get total reads for import
                    detail.Processed = countPairs.ContainsKey("refreadrows") ? countPairs["refreadrows"] : 0;
                    detail.Missing = countPairs.ContainsKey("errors") ? countPairs["errors"] : 0;
                    detail.Total = detail.Processed + detail.Missing;
                }
                else {
                    // get total reads for validation
                    detail.Total = countPairs.ContainsKey("refreadrows") ? countPairs["refreadrows"] : 0;
                    detail.Missing = countPairs.ContainsKey("autofix") ? countPairs["autofix"] : 0;
                    detail.Processed = countPairs.ContainsKey("nofix") ? countPairs["nofix"] : 0;
                    detail.HasErrors = detail.Missing > 0;
                }
            }
        }

        private CloudBlobClient CloudBlobClient {
            get {
                if (_cloudBlobClient != null) return _cloudBlobClient;
                var env = _pa.GetControlVal("aclenv", "dev");
                var storageConn = _pa.GetControlVal($"{env}{AclaraOneStorageConnectionString}", "");
                if(string.IsNullOrEmpty(storageConn)) throw new Exception("Storage Connection is missing.");
                var storageAccount =
                    CloudStorageAccount.Parse(storageConn);
                _cloudBlobClient = storageAccount.CreateCloudBlobClient();
                return _cloudBlobClient;
            }
        }

        /// <summary>
        /// send email using nlog
        /// </summary>
        /// <param name="filepath"></param>
        /// <param name="errorCount"></param>
        /// <returns></returns>
        private bool SendEmail(string filepath, int errorCount) {
            var result = true;
            var clientId = _pa.GetControlVal("clientid", 0);
            Console.WriteLine($"SendEmail for {clientId} starts");
            var processStartTime = DateTime.Now;
            try {
                var logger = new NLogger.ASyncLogger(clientId, string.Empty);
                

                var subject = $"Client {clientId} AmiImportSummaryReport - {errorCount} Error(s)";
                var body = new StringBuilder();
                body.Append($"Summary Report Locates at: {filepath}");
                body.AppendLine();
                body.AppendLine();

                logger.SendMail(subject, body.ToString());
                //logger.SendMail("THIS IS A TEST - PLEASE DISREGARD", "test");
            }
            catch(Exception ex) {
                Console.WriteLine(ex.Message);
                result = false;
            }
            Console.WriteLine($"SendEmail for clientId {clientId} ends, process time = {DateTime.Now.Subtract(processStartTime).TotalMilliseconds} ms");
            return result;
        }
    }
}
