﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CE.AO.AmiImportReport {
    public class AmiSummary {
        public string FileName { get; set; }
        public string Status { get; set; }
        public List<ProcessDetail> Processes { get; set; } 
        public string StartTime { get; set; }
        public string EndTime {get; set; }
        public int Total { get; set; }
        public int MissingConvertedReads { get; set; }
        public int ConvertedReads { get; set; }
        public int ImportedReads { get; set; }
        public int MissingsReads { get; set; }
        public int ValidationError { get; set; }
    }
}
