﻿using System;
using System.Collections.Generic;

namespace CE.AO.AmiImportReport {
    public class ProcessDetail {
        public string StartTime { get; set; }
        public string EndTime { get; set; }
        public String FileName { get; set; }
        public int Total { get; set; }
        public int Missing { get; set; }
        public int Processed { get; set; }
        public List<string> Errors { get; set; }
        public ProcessType Type { get; set; }
        public bool HasErrors { get; set; }

    }

    public enum ProcessType {
        Perproc,
        Import,
        Validate
    }
}
