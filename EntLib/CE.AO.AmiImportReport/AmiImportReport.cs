﻿using System;

namespace CE.AO.AmiImportReport {
    public class AmiImportReport {

        private ProcessingArgs _pa;


        /// <summary>
        /// generate reports
        /// </summary>
        /// <param name="args">overwirte configuration settings; if none, use default settings</param>
        /// args options: aclenv, clientid, maxparallelThread, startTime, endTime
        /// aclenv=dev clientid=210 startTime=1/1/2017 endTime=1/19/2018 maxParallelThread = 100
        public void GenerateAmiReports(string[] args) {
            Console.WriteLine("GenerateAmiReports starts");
            var processStartTime = DateTime.Now;

            try {
                _pa = new ProcessingArgs(args);
                _pa.ApplyCustomConfig();
                var clientId = _pa.GetControlVal("clientid", string.Empty);
                var clientList = !string.IsNullOrEmpty(clientId) ? clientId : _pa.GetControlVal("clientsForReport", string.Empty);
                if (string.IsNullOrEmpty(clientList)) throw new Exception("clientsForReport is empty. No report is generated.");

                var clients = clientList.Split(',');
                foreach (var client in clients) {
                    _pa["clientid"] = client;
                    var reportManager = new ReportManager(_pa);

                    reportManager.GenerateClientAmiReport();
                }
            }
            catch (Exception ex) {
                Console.WriteLine(ex.Message);
            }
            Console.WriteLine($"GenerateAmiReports ends, process time = {DateTime.Now.Subtract(processStartTime).TotalMilliseconds} ms");
        }
    }
}
