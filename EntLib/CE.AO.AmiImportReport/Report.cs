﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CE.AO.AmiImportReport {
    public class Report {
        public string Filename { get; set; }
        public string CsvContent { get; set; }
    }
}
