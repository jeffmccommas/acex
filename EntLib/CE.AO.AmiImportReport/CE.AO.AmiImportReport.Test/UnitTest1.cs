﻿using System;
using System.Collections.Generic;
using System.IO;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace CE.AO.AmiImportReport.Test {
    [TestClass]
    public class GetProcessReport {

        public TestContext TestContext { get; set; }

        [TestMethod]
        [DeploymentItem("TestData")]
        [DataSource("Microsoft.VisualStudio.TestTools.DataSource.XML",
            "|DataDirectory|\\GetProcessReportTestData.xml", "ProcessFile", DataAccessMethod.Sequential)]
        public void GetProcessDetailsTest() {
            try {
                var filename = TestContext.DataRow["Filename"].ToString();
                var hasError = Convert.ToBoolean(TestContext.DataRow["Error"].ToString());
                var total = Convert.ToInt32(TestContext.DataRow["Total"].ToString());
                var processed = Convert.ToInt32(TestContext.DataRow["Processed"].ToString());
                var missing = Convert.ToInt32(TestContext.DataRow["Missing"].ToString());
                var start = TestContext.DataRow["Start"].ToString();
                var end = TestContext.DataRow["End"].ToString();
                var processedFilename = TestContext.DataRow["processFileName"].ToString();

                StringWriter sw = new StringWriter();
                using (StreamReader reader = new StreamReader(filename)) {
                    while (!reader.EndOfStream) {
                        string row = reader.ReadLine();
                        if (!string.IsNullOrEmpty(row)) {
                            sw.WriteLine(row);
                        }
                    }
                }

                var report = new ReportManager();
                var result = report.GetProcessDetails(filename, sw.ToString());

                Assert.IsNotNull(result);
                Assert.AreEqual(hasError, result.HasErrors);
                Assert.AreEqual(total, result.Total);
                Assert.AreEqual(processed, result.Processed);
                Assert.AreEqual(missing, result.Missing);
                Assert.AreEqual(start, result.StartTime);
                Assert.AreEqual(end, result.EndTime);
                Assert.AreEqual(processedFilename, result.FileName);
            }
            catch (Exception ex) {
                Assert.Fail(ex.Message);
            }
        }

        [TestMethod]
        [DeploymentItem("TestData")]
        [DataSource("Microsoft.VisualStudio.TestTools.DataSource.XML",
            "|DataDirectory|\\GenerateAmiSummaryTestData.xml", "ProcessDetails", DataAccessMethod.Sequential)]
        public void GenerateAmiSummaryTest() {
            try {
                var logfiles = TestContext.DataRow["LogFiles"].ToString();
                var logfilesCount = Convert.ToInt32(TestContext.DataRow["LogFileCount"].ToString());
                var resultStatus = Convert.ToBoolean(TestContext.DataRow["Result"].ToString());
                var summaryCount = Convert.ToInt32(TestContext.DataRow["SummaryCount"].ToString());
                var success = Convert.ToInt32(TestContext.DataRow["Success"].ToString());
                var failure = Convert.ToInt32(TestContext.DataRow["Failure"].ToString());
                var fileNames = TestContext.DataRow["FileNames"].ToString();


                var report = new ReportManager();
                var logfileAry = logfiles.Split(',');
                List<ProcessDetail> details = null;
                foreach (var logfile in logfileAry) {
                    if (!string.IsNullOrEmpty(logfile)) {
                        if (details == null) details = new List<ProcessDetail>();
                        StringWriter sw = new StringWriter();
                        using (StreamReader reader = new StreamReader(logfile)) {
                            while (!reader.EndOfStream) {
                                string row = reader.ReadLine();
                                if (!string.IsNullOrEmpty(row)) {
                                    sw.WriteLine(row);
                                }
                            }
                        }
                        var detail = report.GetProcessDetails(logfile, sw.ToString());

                        Assert.IsNotNull(detail);
                        details.Add(detail);
                    }
                }

                Assert.AreEqual(logfilesCount, details?.Count ?? 0);
                var result = report.GenerateAmiSummary(details);
                if (resultStatus) {
                    Assert.IsNotNull(result);
                    Assert.AreEqual(summaryCount, result.Count);
                    Assert.AreEqual(success, result.FindAll(a => a.Status == "success").Count);
                    Assert.AreEqual(failure, result.FindAll(a => a.Status == "failure").Count);
                    var expectedFilenames = fileNames.Split(',');
                    foreach (var filename in expectedFilenames) {
                        if (!string.IsNullOrEmpty(filename)) {
                            Assert.IsTrue(result.Exists(r => r.FileName == filename));
                        }
                    }
                }
                else {
                    Assert.IsNull(result);
                }
            }
            catch (Exception ex) {
                Assert.Fail(ex.Message);
            }
        }

        [TestMethod]
        [DeploymentItem("TestData")]
        [DataSource("Microsoft.VisualStudio.TestTools.DataSource.XML",
            "|DataDirectory|\\GenerateSummaryReportTestData.xml", "AmiSummary", DataAccessMethod.Sequential)]
        public void GenerateSummaryReportTest() {
            try {
                var filename = TestContext.DataRow["Filename"].ToString();
                var status = TestContext.DataRow["Status"].ToString();
                var start = TestContext.DataRow["Start"].ToString();
                var end = TestContext.DataRow["End"].ToString();
                var total = Convert.ToInt32(TestContext.DataRow["Total"].ToString());
                var missingConverted = Convert.ToInt32(TestContext.DataRow["MissingConverted"].ToString());
                var missing = Convert.ToInt32(TestContext.DataRow["Missing"].ToString());
                var converted = Convert.ToInt32(TestContext.DataRow["Converted"].ToString());
                var imported = Convert.ToInt32(TestContext.DataRow["Imported"].ToString());
                var validationError = Convert.ToInt32(TestContext.DataRow["ValidateError"].ToString());
                var outputloc = TestContext.DataRow["devoutputLoc"].ToString();

                var pa = new ProcessingArgs();
                pa["devoutputLocation"] = outputloc;

                var summary = new AmiSummary {
                    FileName = filename,
                    Status = status,
                    StartTime = start,
                    EndTime = end,
                    Total = total,
                    MissingConvertedReads = missingConverted,
                    MissingsReads = missing,
                    ConvertedReads = converted,
                    ImportedReads = imported,
                    ValidationError = validationError
                };

                var report = new ReportManager(pa);
                var result = report.GenerateSummaryReport(new List<AmiSummary> { summary });

                Assert.IsNotNull(result);

                using (StreamReader reader = new StreamReader(result)) {
                    var count = 0;
                    while (!reader.EndOfStream) {
                        string row = reader.ReadLine();
                        if (count > 0) {
                            if (!string.IsNullOrEmpty(row)) {
                                var data = row.Split(',');
                                Assert.AreEqual(filename, data[0]);
                                Assert.AreEqual(status, data[1]);
                                Assert.AreEqual(start, data[2]);
                                Assert.AreEqual(end, data[3]);
                                Assert.AreEqual(total, Convert.ToInt32(data[4]));
                                Assert.AreEqual(missingConverted, Convert.ToInt32(data[5]));
                                Assert.AreEqual(converted, Convert.ToInt32(data[6]));
                                Assert.AreEqual(imported, Convert.ToInt32(data[7]));
                                Assert.AreEqual(missing, Convert.ToInt32(data[8]));
                                Assert.AreEqual(validationError, Convert.ToInt32(data[9]));
                            }
                        }
                        count++;
                    }
                }
            }
            catch (Exception ex) {
                Assert.Fail(ex.Message);
            }
        }

        [TestMethod]
        [DeploymentItem("TestData")]
        [DataSource("Microsoft.VisualStudio.TestTools.DataSource.XML",
            "|DataDirectory|\\GenerateClientAmiReportTestData.xml", "Client", DataAccessMethod.Sequential)]
        public void GenerateClientAmiReportTest() {
            try {
                var clientId = TestContext.DataRow["ClientId"].ToString();
                var start = TestContext.DataRow["Start"].ToString();
                var end = TestContext.DataRow["End"].ToString();
                var maxParallelThread = TestContext.DataRow["MaxParallelThread"].ToString();
                var hasError = Convert.ToBoolean(TestContext.DataRow["Result"].ToString());

                var pa = new ProcessingArgs();
                pa["clientid"] = clientId;
                pa["endTime"] = end;
                pa["startTime"] = start;
                pa["maxParallelThread"] = maxParallelThread;

                var report = new ReportManager(pa);
                var result = report.GenerateClientAmiReport();

                Assert.IsNotNull(result);
                Assert.AreEqual(hasError, result);


            }
            catch (Exception ex) {
                Assert.Fail(ex.Message);
            }
        }
    }
}
