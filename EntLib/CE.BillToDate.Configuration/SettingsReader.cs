﻿using System;
using System.Collections.Generic;
using CE.ContentModel;
using CE.ContentModel.Entities;

namespace CE.BillToDate.Configuration
{
    public class SettingsReader
    {
        private int _clientId;
        private IContentProvider _contentProvider;

        private SettingsReader()
        {
        }

        public SettingsReader(int clientId) : this()
        {
            _clientId = clientId;
            InitContentProvider();
        }


        public BillToDateSettings GetSettings()
        {
            var settingsList = _contentProvider.GetContentConfiguration("billtodateengine,common", string.Empty);

            return GetSettings(settingsList);
        }
        /// <summary>
        /// Populate bill-to-date settings from the content provider, which is the content model.
        /// </summary>
        public BillToDateSettings GetSettings(List<ClientConfiguration> settingsList)
        {
            BillToDateSettings btdSettings = null;
            int variable;
            bool bvariable;

            if (settingsList != null)
            {
                btdSettings = new BillToDateSettings();

                foreach (var item in settingsList)
                {
                    switch (item.Key.ToLower())
                    {
                        case "ratecompanyid":
                            btdSettings.RateCompanyId = int.TryParse(item.Value, out variable) ? variable : 0;
                            break;

                        case "general.proratemonthlyservicecharges":
                            btdSettings.Settings.General.ProrateMonthlyServiceCharges = bool.TryParse(item.Value, out bvariable) ? bvariable : false;
                            break;
                        case "general.proratenonmeteredcharges":
                            btdSettings.Settings.General.ProrateNonMeteredCharges = Convert.ToBoolean(item.Value);
                            break;
                        case "general.proratedemandusagedeterminants":
                            btdSettings.Settings.General.ProrateDemandUsageDeterminants = Convert.ToBoolean(item.Value);
                            break;
                        case "general.minimumchargetype":
                            btdSettings.Settings.General.MinimumChargeType = ConvertToMinChargeType(int.TryParse(item.Value, out variable) ? variable : 0);
                            break;
                        case "general.useconversionfactorforgas":
                            btdSettings.Settings.General.UseConversionFactorForGas = Convert.ToBoolean(item.Value);
                            break;
                        case "general.useconversionfactorforwater":
                            btdSettings.Settings.General.UseConversionFactorForWater = Convert.ToBoolean(item.Value);
                            break;
                        case "general.conversionfactorforgas":
                            btdSettings.Settings.General.ConversionFactorForGas = Convert.ToDouble(item.Value);
                            break;
                        case "general.conversionfactorforwater":
                            btdSettings.Settings.General.ConversionFactorForWater = Convert.ToDouble(item.Value);
                            break;
                        case "general.projectednumberofdays":
                            btdSettings.Settings.General.ProjectedNumberOfDays = Convert.ToInt32(item.Value);
                            break;
                        case "general.smoothtiers":
                            btdSettings.Settings.General.SmoothTiers = Convert.ToBoolean(item.Value);
                            break;
                        case "general.allowmultiplemetersperservice":
                            btdSettings.Settings.General.AllowMultipleMetersPerService = Convert.ToBoolean(item.Value);
                            break;
                        case "general.dstenddate":
                            btdSettings.Settings.General.DSTEndDate = item.Value;
                            break;
                        case "general.allowrebatecalculations":
                            btdSettings.Settings.General.AllowRebateCalculations = Convert.ToBoolean(item.Value);
                            break;
                        case "general.allowbaselinecalculations":
                            btdSettings.Settings.General.AllowRebateCalculations = Convert.ToBoolean(item.Value);
                            break;
                        case "general.dailytierbilldaystype":
                            btdSettings.Settings.General.DailyTierBillDaysType = ConvertToDailyTierBillDaysType(Convert.ToInt32(item.Value));
                            break;
                        case "general.useprojectednumdaysforbilltodate":
                            btdSettings.Settings.General.UseProjectedNumDaysforBilltoDate = Convert.ToBoolean(item.Value);
                            break;
                        case "general.supportdailydemand":
                            btdSettings.Settings.General.SupportDailyDemand = Convert.ToBoolean(item.Value);
                            break;
                        case "general.handlemixedintervalreading":
                            btdSettings.Settings.General.HandleMixedInterval = Convert.ToBoolean(item.Value);
                            break;

                        case "validate.checkmaxmissingdays":
                            btdSettings.Settings.Validate.CheckMaximumMissingDays = Convert.ToBoolean(item.Value);
                            break;
                        case "validate.checkdaysinfullmonth":
                            btdSettings.Settings.Validate.CheckDaysInFullMonth = Convert.ToBoolean(item.Value);
                            break;
                        case "validate.checkminbilldays":
                            btdSettings.Settings.Validate.CheckMinimumBillDays = Convert.ToBoolean(item.Value);
                            break;
                        case "validate.checkminusage":
                            btdSettings.Settings.Validate.CheckMinimumUsage = Convert.ToBoolean(item.Value);
                            break;
                        case "validate.maxmissingdays":
                            btdSettings.Settings.Validate.MaximumMissingDays = Convert.ToInt32(item.Value);
                            break;
                        case "validate.daysinfullmonth":
                            btdSettings.Settings.Validate.DaysInFullMonth = Convert.ToInt32(item.Value);
                            break;
                        case "validate.minbilldayselectric":
                            btdSettings.Settings.Validate.MinimumBillDaysElectric = Convert.ToInt32(item.Value);
                            break;
                        case "validate.minbilldaysgas":
                            btdSettings.Settings.Validate.MinimumBillDaysGas = Convert.ToInt32(item.Value);
                            break;
                        case "validate.minbilldayswater":
                            btdSettings.Settings.Validate.MinimumBillDaysWater = Convert.ToInt32(item.Value);
                            break;
                        case "validate.minusage":
                            btdSettings.Settings.Validate.MinimumUsage = Convert.ToInt32(item.Value);
                            break;

                        case "sewer.proratesewermaxmonthsusagedeterminants":
                            btdSettings.Settings.Sewer.ProrateSewerMaximumMonthsUsageDeterminants = Convert.ToBoolean(item.Value);
                            break;
                        case "sewer.usesewermaxmonthsusageforresidential":
                            btdSettings.Settings.Sewer.UseSewerMaximumMonthsUsageForResidential = Convert.ToBoolean(item.Value);
                            break;
                        case "sewer.usesewermaxmonthsusageforcommercial":
                            btdSettings.Settings.Sewer.UseSewerMaximumMonthsUsageForCommercial = Convert.ToBoolean(item.Value);
                            break;
                        case "sewer.sewermaxmonthsusageforresidential":
                            btdSettings.Settings.Sewer.SewerMaximumMonthsUsageForResidential = Convert.ToInt32(item.Value);
                            break;
                        case "sewer.sewermaxmonthsusageforcommercial":
                            btdSettings.Settings.Sewer.SewerMaximumMonthsUsageForCommercial = Convert.ToInt32(item.Value);
                            break;

                    }

                }

            }

            return (btdSettings);
            /*
            general.proratemonthlyservicecharges = false
            general.proratenonmeteredcharges = false
            general.proratedemandusagedeterminants = false
            general.minimumchargetype = 0
            general.useconversionfactorforgas = false
            general.useconversionfactorforwater = false
            general.conversionfactorforgas = 1.0
            general.conversionfactorforwater = 1.0
            general.projectednumberofdays = 31
            general.smoothtiers = false
            general.allowmultiplemetersperservice = false
            general.dstenddate = string.empty
            general.allowrebatecalculations = false
            general.allowbaselinecalculations = false
            general.dailytierbilldaystype = 0
            general.useprojectednumdaysforbilltodate = false
            general.supportdailydemand = true
            general.handlemixedintervalreading = false
            validate.checkmaxmissingdays = false
            validate.checkdaysinfullmonth = false
            validate.checkminbilldays = false
            validate.checkminusage = false
            validate.maxmissingdays = 8
            validate.daysinfullmonth = 25
            validate.minbilldayselectric = 5
            validate.minbilldaysgas  =5
            validate.minbilldayswater = 5
            validate.minusage = 5
            sewer.proratesewermaxmonthsusagedeterminants = false
            sewer.usesewermaxmonthsusageforresidential = false
            sewer.usesewermaxmonthsusageforcommercial = false
            sewer.sewermaxmonthsusageforresidential = 80
            sewer.sewermaxmonthsusageforcommercial = 80
            */
        }

        public void MergeSettings(BillToDateSettings btdSettings, List<ClientConfiguration> settingsList)
        {
            if (btdSettings == null) throw new ArgumentNullException(nameof(btdSettings));
            int variable;
            bool bvariable;
            

            if (settingsList != null)
            {
                btdSettings = new BillToDateSettings();

                foreach (var item in settingsList)
                {
                    switch (item.Key.ToLower())
                    {
                        case "ratecompanyid":
                            btdSettings.RateCompanyId = int.TryParse(item.Value, out variable) ? variable : 0;
                            break;

                        case "general.proratemonthlyservicecharges":
                            btdSettings.Settings.General.ProrateMonthlyServiceCharges = bool.TryParse(item.Value, out bvariable) ? bvariable : false;
                            break;
                        case "general.proratenonmeteredcharges":
                            btdSettings.Settings.General.ProrateMonthlyServiceCharges = Convert.ToBoolean(item.Value);
                            break;
                        case "general.proratedemandusagedeterminants":
                            btdSettings.Settings.General.ProrateMonthlyServiceCharges = Convert.ToBoolean(item.Value);
                            break;
                        case "general.minimumchargetype":
                            btdSettings.Settings.General.MinimumChargeType = ConvertToMinChargeType(int.TryParse(item.Value, out variable) ? variable : 0);
                            break;
                        case "general.useconversionfactorforgas":
                            btdSettings.Settings.General.UseConversionFactorForGas = Convert.ToBoolean(item.Value);
                            break;
                        case "general.useconversionfactorforwater":
                            btdSettings.Settings.General.UseConversionFactorForWater = Convert.ToBoolean(item.Value);
                            break;
                        case "general.projectednumberofdays":
                            btdSettings.Settings.General.ProjectedNumberOfDays = Convert.ToInt32(item.Value);
                            break;
                        case "general.smoothtiers":
                            btdSettings.Settings.General.SmoothTiers = Convert.ToBoolean(item.Value);
                            break;
                        case "general.allowmultiplemetersperservice":
                            btdSettings.Settings.General.AllowMultipleMetersPerService = Convert.ToBoolean(item.Value);
                            break;
                        case "general.dstenddate":
                            btdSettings.Settings.General.DSTEndDate = item.Value;
                            break;
                        case "general.allowrebatecalculations":
                            btdSettings.Settings.General.AllowRebateCalculations = Convert.ToBoolean(item.Value);
                            break;
                        case "general.allowbaselinecalculations":
                            btdSettings.Settings.General.AllowRebateCalculations = Convert.ToBoolean(item.Value);
                            break;
                        case "general.dailytierbilldaystype":
                            btdSettings.Settings.General.DailyTierBillDaysType = ConvertToDailyTierBillDaysType(Convert.ToInt32(item.Value));
                            break;
                        case "general.useprojectednumdaysforbilltodate":
                            btdSettings.Settings.General.UseProjectedNumDaysforBilltoDate = Convert.ToBoolean(item.Value);
                            break;
                        case "general.supportdailydemand":
                            btdSettings.Settings.General.SupportDailyDemand = Convert.ToBoolean(item.Value);
                            break;
                        case "general.handlemixedintervalreading":
                            btdSettings.Settings.General.HandleMixedInterval = Convert.ToBoolean(item.Value);
                            break;

                        case "validate.checkmaxmissingdays":
                            btdSettings.Settings.Validate.CheckMaximumMissingDays = Convert.ToBoolean(item.Value);
                            break;
                        case "validate.checkdaysinfullmonth":
                            btdSettings.Settings.Validate.CheckDaysInFullMonth = Convert.ToBoolean(item.Value);
                            break;
                        case "validate.checkminbilldays":
                            btdSettings.Settings.Validate.CheckMinimumBillDays = Convert.ToBoolean(item.Value);
                            break;
                        case "validate.checkminusage":
                            btdSettings.Settings.Validate.CheckMinimumUsage = Convert.ToBoolean(item.Value);
                            break;
                        case "validate.maxmissingdays":
                            btdSettings.Settings.Validate.MaximumMissingDays = Convert.ToInt32(item.Value);
                            break;
                        case "validate.daysinfullmonth":
                            btdSettings.Settings.Validate.DaysInFullMonth = Convert.ToInt32(item.Value);
                            break;
                        case "validate.minbilldayselectric":
                            btdSettings.Settings.Validate.MinimumBillDaysElectric = Convert.ToInt32(item.Value);
                            break;
                        case "validate.minbilldaysgas":
                            btdSettings.Settings.Validate.MinimumBillDaysGas = Convert.ToInt32(item.Value);
                            break;
                        case "validate.minbilldayswater":
                            btdSettings.Settings.Validate.MinimumBillDaysWater = Convert.ToInt32(item.Value);
                            break;
                        case "validate.minusage":
                            btdSettings.Settings.Validate.MinimumUsage = Convert.ToInt32(item.Value);
                            break;

                        case "sewer.proratesewermaxmonthsusagedeterminants":
                            btdSettings.Settings.Sewer.ProrateSewerMaximumMonthsUsageDeterminants = Convert.ToBoolean(item.Value);
                            break;
                        case "sewer.usesewermaxmonthsusageforresidential":
                            btdSettings.Settings.Sewer.UseSewerMaximumMonthsUsageForResidential = Convert.ToBoolean(item.Value);
                            break;
                        case "sewer.usesewermaxmonthsusageforcommercial":
                            btdSettings.Settings.Sewer.UseSewerMaximumMonthsUsageForCommercial = Convert.ToBoolean(item.Value);
                            break;
                        case "sewer.sewermaxmonthsusageforresidential":
                            btdSettings.Settings.Sewer.SewerMaximumMonthsUsageForResidential = Convert.ToInt32(item.Value);
                            break;
                        case "sewer.sewermaxmonthsusageforcommercial":
                            btdSettings.Settings.Sewer.SewerMaximumMonthsUsageForCommercial = Convert.ToInt32(item.Value);
                            break;

                    }

                }

            }
            
        }


        /// <summary>
        /// Conversion to MinimumChargeType.
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        private RateModel.Enums.MinimumChargeType ConvertToMinChargeType(int value)
        {
            RateModel.Enums.MinimumChargeType result;

            switch (value)
            {
                case 0:
                    result = RateModel.Enums.MinimumChargeType.NoMinimumCharge;
                    break;

                case 1:
                    result = RateModel.Enums.MinimumChargeType.ProrateMinimumCharge;
                    break;

                case 2:
                    result = RateModel.Enums.MinimumChargeType.NoProrateMinimumCharge;
                    break;

                default:
                    result = RateModel.Enums.MinimumChargeType.NoMinimumCharge;
                    break;
            }

            return (result);
        }

        /// <summary>
        /// Conversion to DailyTierBillDaysType.
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        private RateModel.Enums.DailyTierBillDaysType ConvertToDailyTierBillDaysType(int value)
        {
            RateModel.Enums.DailyTierBillDaysType result;

            switch (value)
            {
                case 0:
                    result = RateModel.Enums.DailyTierBillDaysType.UseProjectedBillDays;
                    break;

                case 1:
                    result = RateModel.Enums.DailyTierBillDaysType.UseCurrentBillDays;
                    break;

                default:
                    result = RateModel.Enums.DailyTierBillDaysType.UseProjectedBillDays;
                    break;
            }

            return (result);
        }

        /// <summary>
        /// Initialize the content provider as Content Model.
        /// </summary>
        private void InitContentProvider()
        {
            var factory = new ContentModelFactoryContentful();
            _contentProvider = factory.CreateContentProvider(_clientId);
        }

    }

}
