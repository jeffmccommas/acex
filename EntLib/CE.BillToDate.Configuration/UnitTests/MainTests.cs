﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace UnitTests
{
    [TestClass]
    public class MainTests 
    {
        [TestInitialize]
        public void TestInit()
        {
            // This initializes the automapper!
            new CE.ContentModel.AutoMapperBootStrapper().BootStrap();
        }

        [TestMethod]
        public void Constructor_Test_87()
        {
            var clientId = 87;

            var reader = new CE.BillToDate.Configuration.SettingsReader(clientId);
            var settings = reader.GetSettings();

            //rateCompanyId should = 100
        }

        [TestMethod]
        public void Constructor_Test_256()
        {
            var clientId = 256;

            var reader = new CE.BillToDate.Configuration.SettingsReader(clientId);
            var settings = reader.GetSettings();

            //rateCompanyId should = 356
        }
    }
}
