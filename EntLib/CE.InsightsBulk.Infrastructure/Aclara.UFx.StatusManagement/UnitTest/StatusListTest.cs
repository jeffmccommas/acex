﻿using System;
using System.Text;
using Aclara.UFx.StatusManagement;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace UnitTest
{


    /// <summary>
    ///This is a test class for StatusListTest and is intended
    ///to contain all StatusListTest Unit Tests
    ///</summary>
    [TestClass()]
    public class StatusListTest
    {


        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion


        /// <summary>
        ///A test for HasStatusSeverity
        ///</summary>
        ///<remark>Positive test.
        ///</remark>
        [TestMethod()]
        public void HasStatusSeverityTest()
        {

            Status status;
            StatusList target;
            bool expected = true;
            bool actual;

            target = new StatusList();
            status = new Status(new ArgumentException("Test"), StatusTypes.StatusSeverity.Error);

            target.Add(status);

            actual = target.HasStatusSeverity(StatusTypes.StatusSeverity.Error);

            Assert.AreEqual(expected, actual, "Expected Result: HasStatusSeverity() methods returns True.");

        }

        /// <summary>
        ///A test for HasStatusSeverity.
        ///</summary>
        ///<remark>Negative test.
        ///</remark>
        [TestMethod()]
        public void HasStatusSeverityTest1()
        {

            Status status;
            StatusList target;
            bool expected = false;
            bool actual;

            target = new StatusList();
            status = new Status(new ArgumentException("Test"), StatusTypes.StatusSeverity.Error);

            target.Add(status);

            actual = target.HasStatusSeverity(StatusTypes.StatusSeverity.Informational);

            Assert.AreEqual(expected, actual, "Expected Result: HasStatusSeverity() methods returns False.");

        }

        /// <summary>
        ///A test for HasStatusSeverity
        ///</summary>
        ///<remark>Positive test.
        ///</remark>
        [TestMethod()]
        public void HasStatusSeverityTest2()
        {

            Status status;
            StatusList target;
            bool expected = true;
            bool actual;

            target = new StatusList();

            status = new Status(new ArgumentException("Test"), StatusTypes.StatusSeverity.Error);
            target.Add(status);

            status = new Status(new ArgumentException("Test1"), StatusTypes.StatusSeverity.Warning);
            target.Add(status);

            status = new Status(new ArgumentException("Test2"), StatusTypes.StatusSeverity.Informational);
            target.Add(status);

            actual = target.HasStatusSeverity(StatusTypes.StatusSeverity.Error);

            Assert.AreEqual(expected, actual, "Expected Result: HasStatusSeverity() methods returns True.");

        }

        /// <summary>
        ///A test for GetStatusListByStatusSeverity
        ///</summary>
        ///<remarks>Positive test.
        ///</remarks>
        [TestMethod()]
        public void GetStatusListByStatusSeverityTest()
        {


            Status status;
            StatusList target;
            StatusList actual;

            target = new StatusList();

            status = new Status(new ArgumentException("Test"), StatusTypes.StatusSeverity.Error);
            target.Add(status);

            status = new Status(new ArgumentException("Test1"), StatusTypes.StatusSeverity.Warning);
            target.Add(status);

            status = new Status(new ArgumentException("Test2"), StatusTypes.StatusSeverity.Informational);
            target.Add(status);

            status = new Status(new ArgumentException("Test3"), StatusTypes.StatusSeverity.Error);
            target.Add(status);

            actual = target.GetStatusListByStatusSeverity(StatusTypes.StatusSeverity.Error);

            Assert.IsNotNull(actual, "Expected Result: Non-null StatusList.");
            Assert.IsTrue(actual.Count > 0, "Expected Result: Status list not empty.");

        }

        /// <summary>
        ///A test for GetStatusListByStatusSeverity
        ///</summary>
        ///<remarks>Negative test.
        ///</remarks>
        [TestMethod()]
        public void GetStatusListByStatusSeverityTest1()
        {


            Status status;
            StatusList target;
            StatusList actual;

            target = new StatusList();

            status = new Status(new ArgumentException("Test"), StatusTypes.StatusSeverity.Error);
            target.Add(status);

            status = new Status(new ArgumentException("Test1"), StatusTypes.StatusSeverity.Warning);
            target.Add(status);

            status = new Status(new ArgumentException("Test2"), StatusTypes.StatusSeverity.Warning);
            target.Add(status);

            status = new Status(new ArgumentException("Test3"), StatusTypes.StatusSeverity.Error);
            target.Add(status);

            actual = target.GetStatusListByStatusSeverity(StatusTypes.StatusSeverity.Informational);

            Assert.IsNotNull(actual, "Expected Result: Non-null StatusList.");
            Assert.IsTrue(actual.Count == 0, "Expected Result: Status list empty.");

        }

        /// <summary>
        ///A test for GetStatusListByStatusSeverity
        ///</summary>
        ///<remarks>Positive test.
        ///</remarks>
        [TestMethod()]
        public void GetStatusListByStatusSeverityTest2()
        {


            Status status;
            StatusList target;
            StatusList actual;

            target = new StatusList();

            status = new Status(new ArgumentException("Test"), StatusTypes.StatusSeverity.Error);
            target.Add(status);

            status = new Status(new ArgumentException("Test1"), StatusTypes.StatusSeverity.Warning);
            target.Add(status);

            status = new Status(new ArgumentException("Test2"), StatusTypes.StatusSeverity.Informational);
            target.Add(status);

            status = new Status(new ArgumentException("Test3"), StatusTypes.StatusSeverity.Error);
            target.Add(status);

            actual = target.GetStatusListByStatusSeverity(StatusTypes.StatusSeverity.Error);
            Assert.IsNotNull(actual, "Expected Result: Non-null StatusList.");
            Assert.IsTrue(actual.Count > 0, "Expected Result: Status list not empty.");

            foreach (Status statusItem in actual)
            {
                string message = string.Empty;
                StatusTypes.StatusSeverity statusServerity;

                message = statusItem.Exception.Message;
                statusServerity = statusItem.StatusServerity;

                Assert.IsFalse(string.IsNullOrEmpty(message), "Expected Result: Exception message is not empty nor null.");
                Assert.IsFalse(statusServerity == StatusTypes.StatusSeverity.Unspecified, "Expected Result: Status severity is not Unspecified.");
            }


        }

        /// <summary>
        ///A test for HasException
        ///</summary>
        [TestMethod()]
        public void HasExceptionTest()
        {

            Status status;
            StatusList target;
            bool actual;

            target = new StatusList();

            status = new Status(new ArgumentOutOfRangeException("Test"), StatusTypes.StatusSeverity.Error);
            target.Add(status);

            status = new Status(new ArgumentNullException("Test1"), StatusTypes.StatusSeverity.Warning);
            target.Add(status);

            status = new Status(new ArgumentException("Test2"), StatusTypes.StatusSeverity.Informational);
            target.Add(status);

            status = new Status(new DivideByZeroException("Test3"), StatusTypes.StatusSeverity.Error);
            target.Add(status);

            actual = target.HasException(typeof(DivideByZeroException));
            Assert.IsTrue(actual, string.Format("Expected exception of type {0} in status list.",
                                                typeof(DivideByZeroException).ToString()));

        }


        /// <summary>
        ///A test for HasException
        ///</summary>
        ///<remarks>
        ///Negative test.
        ///</remarks>
        [TestMethod()]
        public void HasExceptionTest1()
        {

            Status status;
            StatusList target;
            bool actual;

            target = new StatusList();

            status = new Status(new ArgumentOutOfRangeException("Test"), StatusTypes.StatusSeverity.Error);
            target.Add(status);

            status = new Status(new ArgumentNullException("Test1"), StatusTypes.StatusSeverity.Warning);
            target.Add(status);

            status = new Status(new ArgumentException("Test2"), StatusTypes.StatusSeverity.Informational);
            target.Add(status);

            actual = target.HasException(typeof(DivideByZeroException));
            Assert.IsFalse(actual, string.Format("Expected exception of type {0} NOT in status list.",
                                                 typeof(DivideByZeroException).ToString()));

        }

        /// <summary>
        ///A test for GetStatusListAsExceptionString.
        ///</summary>
        [TestMethod()]
        public void GetStatusListAsExceptionStringTest()
        {
            const int MaximumStringLength = 3000;

            Status status;
            StatusList target;
            StatusTypes.StatusSeverity statusSeverity = StatusTypes.StatusSeverity.Informational;
            int maximumStringLength;
            string exceptionMessage = string.Empty;
            string actual = string.Empty;

            target = new StatusList();

            exceptionMessage = GenerateRandomText(999);
            status = new Status(new ArgumentOutOfRangeException(exceptionMessage), StatusTypes.StatusSeverity.Error);
            target.Add(status);
            exceptionMessage = GenerateRandomText(999);

            exceptionMessage = GenerateRandomText(999);
            status = new Status(new ArgumentNullException(exceptionMessage), StatusTypes.StatusSeverity.Warning);
            target.Add(status);

            exceptionMessage = GenerateRandomText(999);
            status = new Status(new ArgumentException(exceptionMessage), StatusTypes.StatusSeverity.Informational);
            target.Add(status);

            maximumStringLength = MaximumStringLength;
            statusSeverity = StatusTypes.StatusSeverity.Error;
            actual = target.GetStatusListAsExceptionString(statusSeverity, maximumStringLength);

            Assert.IsFalse(string.IsNullOrEmpty(actual), "Expected non-null and non-empty string.");
        }

        /// <summary>
        ///A test for GetStatusListAsExceptionString. Exceed maximum string length.
        ///</summary>
        [TestMethod()]
        public void GetStatusListAsExceptionString_ExceedMaxStringLenth_Test()
        {
            const int MaximumStringLength = 3000;

            Status status;
            StatusList target;
            StatusTypes.StatusSeverity statusSeverity = StatusTypes.StatusSeverity.Informational;
            int maximumStringLength;
            string exceptionMessage = string.Empty;
            string actual = string.Empty;

            target = new StatusList();

            exceptionMessage = GenerateRandomText(999);
            status = new Status(new ArgumentOutOfRangeException(exceptionMessage), StatusTypes.StatusSeverity.Error);
            target.Add(status);
            exceptionMessage = GenerateRandomText(999);
            status = new Status(new ArgumentOutOfRangeException(exceptionMessage), StatusTypes.StatusSeverity.Error);
            target.Add(status);
            exceptionMessage = GenerateRandomText(999);
            status = new Status(new ArgumentOutOfRangeException(exceptionMessage), StatusTypes.StatusSeverity.Error);
            target.Add(status);

            exceptionMessage = GenerateRandomText(999);
            status = new Status(new ArgumentNullException(exceptionMessage), StatusTypes.StatusSeverity.Warning);
            target.Add(status);

            exceptionMessage = GenerateRandomText(999);
            status = new Status(new ArgumentException(exceptionMessage), StatusTypes.StatusSeverity.Informational);
            target.Add(status);

            maximumStringLength = MaximumStringLength;
            statusSeverity = StatusTypes.StatusSeverity.Error;
            actual = target.GetStatusListAsExceptionString(statusSeverity, maximumStringLength);
            Assert.IsFalse(string.IsNullOrEmpty(actual), "Expected non-null and non-empty string truncated to specified maximum string length.");
            Assert.IsTrue((actual.Length <= MaximumStringLength), "Expected non-null and non-empty string truncated to specified maximum string length.");
        }

        /// <summary>
        /// Generate random text of specified size.
        /// </summary>
        /// <param name="size"></param>
        /// <returns></returns>
        public string GenerateRandomText(int size)
        {

            const int AlphabetMaxCharacter = 26;
            const int AsciiCapitalLeterStart = 65;
            Random random = null;
            StringBuilder builder = null;

            try
            {
                random = new Random();
                builder = new StringBuilder();
                for (int index = 0; index < size; index++)
                {
                    builder.Append(Convert.ToChar(Convert.ToInt32(Math.Floor(AlphabetMaxCharacter * random.NextDouble() + AsciiCapitalLeterStart))));

                }

            }
            catch (Exception)
            {
                throw;
            } 

            return builder.ToString();

        }
    }
}
