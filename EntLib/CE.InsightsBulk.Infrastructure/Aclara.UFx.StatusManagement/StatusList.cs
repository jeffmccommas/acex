﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Aclara.UFx.StatusManagement
{
    /// <summary>
    /// Status list.
    /// </summary>
    public class StatusList : List<Status>
    {
        private const int DefaultMaximumLength = 2000;
        private const string TruncationIndicator = "...";
        private const int LfcrLength = 2;

        /// <summary>
        /// Determine if status list containts specified status severity.
        /// </summary>
        /// <returns></returns>
        public bool HasStatusSeverity(StatusTypes.StatusSeverity statusSeverity)
        {
            //Find status with matching status severity.
            var result = false;
            var status = Find(statusToFind => statusToFind.StatusServerity == statusSeverity);

            //Status with matching status severtiy found.
            if (status != null)
            {
                result = true;
            }

            return result;
        }

        /// <summary>
        /// Determine if status list containts specified type.
        /// </summary>
        /// <returns></returns>
        public bool HasException(Type comparisonType)
        {
            //Find status with matching exception subtype.
            var result = false;
            var status = Find(statusToFind => statusToFind.Exception != null && statusToFind.Exception.GetType() == comparisonType);

            //Status with matching exception subtype found.
            if (status != null)
            {
                result = true;
            }

            return result;
        }

        /// <summary>
        /// Return status list with all status objects that match specified status severity.
        /// If no statuses match then empty status list is returned.
        /// </summary>
        /// <param name="statusSeverity"></param>
        /// <returns></returns>
        public StatusList GetStatusListByStatusSeverity(StatusTypes.StatusSeverity statusSeverity)
        {
            var matchingStatuslist = FindAll(statusToFind => statusToFind.StatusServerity == statusSeverity);

            var result = new StatusList();

            result.AddRange(matchingStatuslist);

            return result;
        }

        /// <summary>
        ///  Adds the elements of "Status List to Merge" to the end of this Status List  
        /// </summary>
        /// <param name="statusListToMerge"></param>
        public void Merge(StatusList statusListToMerge)
        {
            if (statusListToMerge.Any())
            {
                AddRange(statusListToMerge);
            }
        }

        /// <summary>
        /// Convert status list exceptions to string.
        /// </summary>
        /// <param name="statusSeverity"></param>
        /// <returns></returns>
        public string GetStatusListAsExceptionString(StatusTypes.StatusSeverity statusSeverity)
        {
            return GetStatusListAsExceptionString(statusSeverity, DefaultMaximumLength);
        }

        /// <summary>
        /// Convert status list exceptions to string.
        /// </summary>
        /// <param name="statusSeverity"></param>
        /// <param name="maximumStringLength">Maximum string length. Defaults to 100 if not specified.</param>
        /// <returns></returns>
        public string GetStatusListAsExceptionString(StatusTypes.StatusSeverity statusSeverity,
                                                     int maximumStringLength)
        {
            //Ensure maximum string length is valid.
            if(maximumStringLength < DefaultMaximumLength)
            {
                maximumStringLength = DefaultMaximumLength;
            }

            var statusList = GetStatusListByStatusSeverity(statusSeverity);

            var exceptionInfoStringBuilder = new StringBuilder();

            foreach (var status in statusList)
            {
                var exceptionAsText = status.StatusDesc;

                if (status.Exception != null)
                {
                    exceptionAsText += ":" + status.Exception;
                }

                //Maximum length will be exceeded.
                if (exceptionInfoStringBuilder.Length + exceptionAsText.Length > maximumStringLength)
                {
                    //Calculate available string length.
                    var availableStringLength = maximumStringLength - exceptionInfoStringBuilder.Length -
                                                TruncationIndicator.Length - LfcrLength;

                    //Appendix up to maximum length.
                    exceptionInfoStringBuilder.AppendLine(
                        $"{exceptionAsText.Substring(0, availableStringLength)}{TruncationIndicator}");
                    break;
                }


                exceptionInfoStringBuilder.AppendLine(exceptionAsText);
            }

            return exceptionInfoStringBuilder.ToString();
        }
    }
}
