﻿
namespace Aclara.UFx.StatusManagement
{

    /// <summary>
    /// 
    /// </summary>
    public class StatusTypes
    {

        /// <summary>
        /// Enumeration: Status level.
        /// </summary>
        public enum StatusSeverity
        {
            Unspecified = 0,
            Error = 1,
            Warning = 2,
            Informational = 3
        }

    }
}
