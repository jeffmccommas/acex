﻿using System;

namespace Aclara.UFx.StatusManagement
{
    /// <summary>
    /// 
    /// </summary>
    public class Status
    {

        #region "Private Constants"

        #endregion

        #region "Private Data Members"

        Exception _exception;
        StatusTypes.StatusSeverity _statusServerity;
        string _statusDesc;

        #endregion

        #region "Public Properties"

        public string StatusDesc
        {
            get
            {
                return _statusDesc;
            }
            set
            {
                _statusDesc = value;
            }
        }
        /// <summary>
        /// Property: Exception
        /// </summary>
        public Exception Exception 
        {
            get
            {
                return _exception;
            }
            set
            {
                _exception = value;
            }
        }

        /// <summary>
        /// Property: Status serverity.
        /// </summary>
        public StatusTypes.StatusSeverity StatusServerity 
        { 
            get
            {
                return _statusServerity;
            }
            set
            {
                _statusServerity = value;
            }
        }

        #endregion

        #region "Protected Properties"

        #endregion

        #region "Public Constructors"

        /// <summary>
        /// Default constructor.
        /// </summary>
        public Status()
        {
            _exception = null;
            _statusServerity = StatusTypes.StatusSeverity.Unspecified;
        }

        /// <summary>
        /// Constructor - Exception.
        /// </summary>
        public Status(Exception exception, StatusTypes.StatusSeverity statusServerity)
        {
            _exception = exception;
            _statusServerity = statusServerity;
        }

        /// <summary>
        /// Constructor - Informational.
        /// </summary>
        public Status(string message, StatusTypes.StatusSeverity statusServerity)
        {
            _statusDesc = message;
            _statusServerity = statusServerity;
        }


        #endregion

        #region "Protected Constructors"

        #endregion

        #region "Public Methods"

        #endregion

        #region "Protected Methods"

        #endregion





    }
}
