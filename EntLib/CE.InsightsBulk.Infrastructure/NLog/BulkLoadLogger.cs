﻿using System;
using System.IO;
using NLog;
using NLog.Targets;
using CE.InsightsBulk.Infrastructure.Interfaces;

namespace CE.InsightsBulk.Infrastructure.NLog
{
    public class BulkLoadLogger :ILogger
    {
        private static readonly Logger _logger = LogManager.GetCurrentClassLogger();
        private const string PropTrackingId = "TrackingId";
        private const string PropClientId = "ClientId";

        public string TrackingId { get; set; }

        public int ClientId { get; set; }


        public BulkLoadLogger(string logFileName, string trackingId, int clientId)
        {
            var target = LogManager.Configuration.FindTargetByName("textFile") as FileTarget;
            if (target != null)
            {
                target.FileName = logFileName;
            }
            TrackingId = trackingId;
            ClientId = clientId;
        }

        public void SetName(string logFileName)
        {
            var target = LogManager.Configuration.FindTargetByName("textFile") as FileTarget;
            if (target != null)
            {
                target.FileName = logFileName;
            }
        }

        public string GetName()
        {
            var target = LogManager.Configuration.FindTargetByName("textFile") as FileTarget;
            if (target != null)
            {
                return target.FileName.ToString();
            }

            return string.Empty;
        }

        private void LogEvent(LogLevel logLevel, string message)
      {
          var theEvent = new LogEventInfo(logLevel,"", message);
          theEvent.Properties[PropTrackingId] = TrackingId;
          theEvent.Properties[PropClientId] = ClientId;
          _logger.Log(theEvent);
      }

      private void LogEventException(LogLevel logLevel, Exception ex)
      {
          var theEvent = new LogEventInfo(logLevel, "", ex.ToString()) {Exception = ex};
          theEvent.Properties[PropTrackingId] = TrackingId;
          theEvent.Properties[PropClientId] = ClientId;
          _logger.Log(theEvent);
      }

      public void Info(string trackingid,string message)
        {
            LogEvent(LogLevel.Info, message);
        }

        public void Warn(string trackingid,string message)
        {
            throw new NotImplementedException();
        }

        public void Debug(string trackingid,string message)
        {
            throw new NotImplementedException();
        }

        public void Error(string trackingid,string message)
        {
            LogEvent(LogLevel.Error, message);

        }

        public void ErrorException(string trackingid,Exception ex)
        {
            LogEventException(LogLevel.Error, ex);

        }

        public void Fatal(string trackingid,string message)
        {
            throw new NotImplementedException();
        }
    }
}
