﻿using Aclara.UFx.StatusManagement;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CE.InsightsBulk.Infrastructure.Interfaces
{
    public interface IBulkFile
    {
        string ProcessFile(FileInfo custFile, out StatusList statusList);
       
    }
}
