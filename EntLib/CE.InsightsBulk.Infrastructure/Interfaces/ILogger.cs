﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CE.InsightsBulk.Infrastructure.Interfaces
{
    public interface ILogger
    {
        void Info(string trackingid, string message);
        void Warn(string trackingid, string message);
        void Debug(string trackingid,string message);
        void Error(string trackingid,string message);
        void ErrorException(string trackingid, Exception ex);
        void Fatal(string trackingid, string message);
    }
}
