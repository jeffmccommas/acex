﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CE.InsightsBulk.Infrastructure.Interfaces
{
    public interface IConfig
    {
        void LoadBillingConfiguration();
        void LoadCustomerConfiguration();
        void LoadProfileConfiguration();
        void LoadActionItemConfiguration();
        string InputXSD
        {
            get;
            set;
        }
        string GeneralErrorLog
        {
            get;
            set;
        }
        int MaxBatchSize
        {
            get;
            set;
        }
        string InputFilePath
        {
            get;
            set;
        }
        string FileSearchPattern
        {
            get;
            set;
        }
        string FileOutputExtension
        {
            get;
            set;
        }
        string FileWorkingExtension
        {
            get;
            set;
        }
        string ArchivePath
        {
            get;
            set;
        }
        string FileErrorExtension
        {
            get;
            set;
        }
        string FileDoneExtension
        {
            get;
            set;
        }
        bool ImportSpecificLoggingOn
        {
            get;
            set;
        }

    }
}
