﻿namespace CE.InsightsBulk.Infrastructure.Interfaces
{
    public interface IMetaDataAccess
    {
        bool IsETLRunning(int clientID, bool verbose);
        void LogDBEvent(string Message, int ClientId);
    }
}
