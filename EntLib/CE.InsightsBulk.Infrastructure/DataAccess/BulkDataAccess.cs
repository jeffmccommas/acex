﻿using Aclara.UFx.StatusManagement;
using CE.InsightsBulk.Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;

namespace CE.InsightsBulk.Infrastructure.DataAccess
{
    public class BulkDataAccess
    {
        private const string Customertabletypestring = "Holding.CustomerTableType";
        private const string Billingtabletypestring = "Holding.BillingTableType";
        private const string Premisetabletypestring = "Holding.PremiseType";
        private const string Premiseattributetabletypestring = "Holding.PremiseAttributesType";
        private const string Actionitemtabletypestring = "Holding.ActionItemType";
        private const string CustomertabletypeNameOnlyString = "CustomerTableType";
        private const string BillingtabletypeNameOnlyString = "BillingTableType";
        private const string PremisetabletypeNameOnlyString = "PremiseType";
        private const string PremiseattributetabletypeNameOnlyString = "PremiseAttributesType";
        private const string ActionkeytabletypeNameOnlyString = "ActionItemType";

        private const string StoredProcedureName_CustomerInsert = "Holding.InsertCustomerData";
        private const string StoredProcedureName_BillInsert = "Holding.InsertBillingData";
        private const string StoredProcedureName_PremiseAttributeInsert = "Holding.InsertPremiseAttributeData";
        private const string StoredProcedureName_ActionItemInsert = "Holding.InsertActionItemData";

        //Generic Columns
        private const string ColumnName_ColumnID = "ColumnID";
        private const string ColumnName_ColumnName = "ColumnName";
        private const string ColumnName_MaximumLength = "MaximumLength";
        private const string ColumnName_SystemTypeID = "SystemTypeID";
        private const string ColumnName_SystemTypeName = "SystemTypeName";

        //Customer Columns
        private const string CustomerColumn_CustomerId = "CustomerId";
        private const string CustomerColumn_City = "City";
        private const string CustomerColumn_ClientId = "ClientId";
        private const string CustomerColumn_Street2 = "Street2";
        private const string CustomerColumn_Street1 = "Street1";
        private const string CustomerColumn_State = "State";
        private const string CustomerColumn_MobileNumber = "MobilePhoneNumber";
        private const string CustomerColumn_PhoneNumber = "PhoneNumber";
        private const string CustomerColumn_LastName = "LastName";
        private const string CustomerColumn_IsBusiness = "IsBusiness";
        private const string CustomerColumn_IsAuthenticated = "IsAuthenticated";
        private const string CustomerColumn_FirstName = "FirstName";
        private const string CustomerColumn_EmailAddress = "EmailAddress";
        private const string CustomerColumn_Country = "Country";
        private const string CustomerColumn_AlternateEmailAddress = "AlternateEmailAddress";
        private const string CustomerColumn_Source = "SourceId";
        private const string CustomerColumn_PostalCode = "PostalCode";
        private const string CustomerColumn_TrackingId = "TrackingId";
        private const string CustomerColumn_TrackingDate = "TrackingDate";

        //Billing Columns
        private const string BillingColumn_ClientID = "ClientID";
        private const string BillingColumn_RateClass = "RateClass";
        private const string BillingColumn_Customerid = "Customerid";
        private const string BillingColumn_AccountId = "AccountId";
        private const string BillingColumn_PremiseId = "PremiseId";
        private const string BillingColumn_BillDate = "BillStartDate";
        private const string BillingColumn_BillEndDate = "BillEndDate";
        private const string BillingColumn_TotalUnits = "TotalUnits";
        private const string BillingColumn_TotalCost = "TotalCost";
        private const string BillingColumn_CommodityId = "CommodityId";
        private const string BillingColumn_BillPeriodTypeId = "BillPeriodTypeId";
        private const string BillingColumn_BillCycleScheduleId = "BillCycleScheduleId";
        private const string BillingColumn_UOM = "UOM";
        private const string BillingColumn_TrackingId = "TrackingId";
        private const string BillingColumn_TrackingDate = "TrackingDate";
        private const string BillingColumn_Source = "SourceId";
        private const string BillingColumn_AMIStartDate = "AMIStartDate";
        private const string BillingColumn_AMIEndDate = "AMIEndDate";
        private const string BillingColumn_MeterId = "MeterId";
        private const string BillingColumn_ServicePointId = "ServicePointId";
        private const string BillingColumn_ServiceContractId = "ServiceContractId";
        private const string BillingColumn_MeterType = "MeterType";
        private const string BillingColumn_ReplacedMeterId = "ReplacedMeterId";
        private const string BillingColumn_ReadQuality = "ReadQuality";
        private const string BillingColumn_ReadDate = "ReadDate";
        private const string BillingColumn_DueDate = "DueDate";

        //Premise Columns
        private const string PremiseColumn_AccountId = "AccountId";
        private const string PremiseColumn_PremiseId = "PremiseId";
        private const string PremiseColumn_Street2 = "Street2";
        private const string PremiseColumn_Street1 = "Street1";
        private const string PremiseColumn_City = "City";
        private const string PremiseColumn_State = "State";
        private const string PremiseColumn_PostalCode = "PostalCode";
        private const string PremiseColumn_HasGasService = "GasService";
        private const string PremiseColumn_HasElectricService = "ElectricService";
        private const string PremiseColumn_HasWaterService = "WaterService";
        private const string PremiseColumn_Country = "Country";
        private const string PremiseColumn_Latitude = "Latitude";
        private const string PremiseColumn_Longitude = "Longitude";

        //Attribute Columns
        private const string PremiseAttributeColumn_ClientId  = "ClientID";
        private const string PremiseAttributeColumn_CustomerID = "CustomerId";
        private const string PremiseAttributeColumn_AccountID = "AccountId";
        private const string PremiseAttributeColumn_PremiseID = "PremiseId";
        private const string PremiseAttributeColumn_AttributeId = "AttributeId";
        private const string PremiseAttributeColumn_OptionValue= "OptionValue";
        private const string PremiseAttributeColumn_SourceId = "SourceId";
        private const string PremiseAttributeColumn_UpdateDate = "UpdateDate";
        private const string PremiseAttributeColumn_CreateDate = "CreateDate";
        private const string PremiseAttributeColumn_AttributeKey = "AttributeKey";
        private const string PremiseAttributeColumn_TrackingID = "TrackingId";
        private const string PremiseAttributeColumn_TrackingDate = "TrackingDate";

        //ActionItem Columns
        private const string ActionItemColumn_ClientId = "ClientID";
        private const string ActionItemColumn_CustomerID = "CustomerId";
        private const string ActionItemColumn_AccountID = "AccountId";
        private const string ActionItemColumn_PremiseID = "PremiseId";
        private const string ActionItemColumn_StatusId = "StatusId";
        private const string ActionItemColumn_StatusDate = "StatusDate";
        private const string ActionItemColumn_SourceId = "SourceId";
        private const string ActionItemColumn_ActionKey = "ActionKey";
        private const string ActionItemColumn_SubActionKey = "SubActionKey";
        private const string ActionItemColumn_ActionData = "ActionData";
        private const string ActionItemColumn_ActionDataValue = "ActionDataValue";
        private const string ActionItemColumn_TrackingID = "TrackingId";
        private const string ActionItemColumn_TrackingDate = "TrackingDate";

        private DataTable _premiseDataTable;
        private DataTable _premiseAttributeDataTable;
        private DataTable _actionOutputDataTable;
        private DataTable _billingDataTable;
        private DataTable _customerDataTable;


        /// <summary>
        /// WriteCustomerData
        /// </summary>
        /// <param name="customerTable"></param>
        /// <param name="premiseTable"></param>
        /// <param name="statusList"></param>
        /// <returns></returns>
        public int WriteCustomerData(List<Customer> customerTable , List<Premise> premiseTable, ref StatusList statusList)
       {
           if (_customerDataTable == null)
           { 
               //create the table
               _customerDataTable = CreateDataTable(CustomertabletypeNameOnlyString);
               
           }
           else
           {
               _customerDataTable.Clear();
           }

           foreach (var cust in customerTable)
           {
               var customerDatarow = _customerDataTable.NewRow();
               customerDatarow[CustomerColumn_CustomerId] = cust.CustomerID;
               customerDatarow[CustomerColumn_ClientId] = cust.ClientID;
               customerDatarow[CustomerColumn_Street2] = cust.Street2;
               customerDatarow[CustomerColumn_Street1] = cust.Street1;
               customerDatarow[CustomerColumn_State] = cust.State;
               customerDatarow[CustomerColumn_MobileNumber] = cust.MobilePhoneNumber;
               customerDatarow[CustomerColumn_PhoneNumber] = cust.PhoneNumber;
               customerDatarow[CustomerColumn_LastName] = cust.LastName;
               customerDatarow[CustomerColumn_IsBusiness] = cust.IsBusiness ?? (object)DBNull.Value;
               customerDatarow[CustomerColumn_IsAuthenticated] = cust.IsAuthenticated ?? (object)DBNull.Value;
               customerDatarow[CustomerColumn_FirstName] = cust.FirstName;
               customerDatarow[CustomerColumn_EmailAddress] = cust.EmailAddress;
               customerDatarow[CustomerColumn_City] = cust.City;
               customerDatarow[CustomerColumn_Country] = cust.Country;
               customerDatarow[CustomerColumn_AlternateEmailAddress] = cust.AlternateEmailAddress;
               customerDatarow[CustomerColumn_Source] = cust.SourceId;
               customerDatarow[CustomerColumn_PostalCode] = cust.postalcode;
               customerDatarow[CustomerColumn_TrackingId] = cust.TrackingId;
               customerDatarow[CustomerColumn_TrackingDate] = cust.TrackingDate.ToString("yyyy-MM-dd HH:mm:ss");

               _customerDataTable.Rows.Add(customerDatarow);
           }

            //Process Premise Data
           if (_premiseDataTable == null)
           {
               //create the table
               _premiseDataTable = CreateDataTable(PremisetabletypeNameOnlyString);

           }
           else
           {
               _premiseDataTable.Clear();
           }

           foreach (var premise in premiseTable)
           {
               var premiseDataRow = _premiseDataTable.NewRow();
               premiseDataRow[CustomerColumn_CustomerId] = premise.CustomerID;
               premiseDataRow[CustomerColumn_TrackingId] = premise.TrackingID;
               premiseDataRow[CustomerColumn_ClientId] = premise.ClientID;
               premiseDataRow[CustomerColumn_TrackingDate] = premise.TrackingDate.ToString("yyyy-MM-dd HH:mm:ss");
               premiseDataRow[PremiseColumn_AccountId] = premise.AccountID;
              premiseDataRow[PremiseColumn_PremiseId] = premise.PremiseID;
              premiseDataRow[PremiseColumn_Street2] = premise.Street2;
              premiseDataRow[PremiseColumn_Street1] = premise.Street1;
              premiseDataRow[PremiseColumn_City] = premise.City; 

               if (premise.Latitude.HasValue)
               {
                   premiseDataRow[PremiseColumn_Latitude] = premise.Latitude;
               }
               else
               {
                   premiseDataRow[PremiseColumn_Latitude] = DBNull.Value;
               }

               if (premise.Longitude.HasValue)
               {
                   premiseDataRow[PremiseColumn_Longitude] = premise.Longitude;
               }
               else
               {
                   premiseDataRow[PremiseColumn_Longitude] = DBNull.Value;
               }

               premiseDataRow[PremiseColumn_State] = premise.State;
              premiseDataRow[PremiseColumn_PostalCode] = premise.postalcode;
              premiseDataRow[PremiseColumn_HasGasService] = premise.GasService;
              premiseDataRow[PremiseColumn_HasElectricService] = premise.ElectricService ;
              premiseDataRow[PremiseColumn_HasWaterService] = premise.WaterService;
              premiseDataRow[PremiseColumn_Country] = premise.Country;
              premiseDataRow[CustomerColumn_Source] = premise.SourceId;

               _premiseDataTable.Rows.Add(premiseDataRow);
           }

           using (var ent = new InsightsBulkEntities())
           {
               // If using Code First we need to make sure the model is built before we open the connection 
               // This isn't required for models created with the EF Designer 
               ent.Database.Initialize(false);

               var cmd = ent.Database.Connection.CreateCommand();
               cmd.CommandText = StoredProcedureName_CustomerInsert;
               cmd.CommandType = CommandType.StoredProcedure;

               SqlParameter custtableParm = new SqlParameter("@cust", typeof(string))
               {
                   TypeName = Customertabletypestring,
                   Value = _customerDataTable
               };
               cmd.Parameters.Add(custtableParm);

               SqlParameter premisetableParm = new SqlParameter("@Premise", typeof(string))
               {
                   TypeName = Premisetabletypestring,
                   Value = _premiseDataTable
               };
               cmd.Parameters.Add(premisetableParm);

               int rowsAffected;
               try
               {
                   ent.Database.Connection.Open();
                   var result = cmd.ExecuteScalar();
                   if (result == null || result.ToString() == string.Empty)
                   {
                       rowsAffected = 0;
                   }
                   else
                       rowsAffected = Convert.ToInt32(result);


               }
               finally
               {
                   ent.Database.Connection.Close();
               }
               return rowsAffected;
           }
       }

        /// <summary>
        /// WriteBillingData
        /// </summary>
        /// <param name="billingTable"></param>
        /// <param name="statusList"></param>
        /// <returns></returns>
        public int WriteBillingData(List<Billing> billingTable, ref StatusList statusList)
        {
            if (_billingDataTable == null)
            {
                //create the table
                _billingDataTable = CreateDataTable(BillingtabletypeNameOnlyString);

            }
            else
            {
                _billingDataTable.Clear();
            }
            foreach (var bill in billingTable)
            {
                var billingDatarow = _billingDataTable.NewRow();


                billingDatarow[BillingColumn_ClientID] = bill.ClientId;
                billingDatarow[BillingColumn_Customerid] = bill.CustomerId;
                billingDatarow[BillingColumn_AccountId] = bill.AccountId;
                billingDatarow[BillingColumn_PremiseId] = bill.PremiseId;
                billingDatarow[BillingColumn_BillDate] = bill.BillStartDate.ToString("yyyy-MM-dd HH:mm:ss"); 
                billingDatarow[BillingColumn_BillEndDate] = bill.BillEndDate.ToString("yyyy-MM-dd HH:mm:ss");
                billingDatarow[BillingColumn_RateClass] = bill.RateClass;
                billingDatarow[BillingColumn_TotalUnits] = bill.TotalUnits;
                billingDatarow[BillingColumn_TotalCost] = bill.TotalCost;
                billingDatarow[BillingColumn_CommodityId] = bill.CommodityId;
                billingDatarow[BillingColumn_BillPeriodTypeId] = bill.BillPeriodTypeId;
                billingDatarow[BillingColumn_BillCycleScheduleId] = bill.BillCycleScheduleId;
                billingDatarow[BillingColumn_UOM] = bill.UOMId;
                billingDatarow[BillingColumn_TrackingId] = bill.TrackingId;
                billingDatarow[BillingColumn_TrackingDate] = bill.TrackingDate.ToString("yyyy-MM-dd HH:mm:ss");
                billingDatarow[BillingColumn_Source] = bill.SourceId;
                billingDatarow[BillingColumn_AMIStartDate] = bill.AMIStartDate?.ToString("yyyy-MM-dd HH:mm:ss");
                billingDatarow[BillingColumn_AMIEndDate] = bill.AMIEndDate?.ToString("yyyy-MM-dd HH:mm:ss");
                billingDatarow[BillingColumn_MeterId] = bill.MeterId;
                billingDatarow[BillingColumn_ServicePointId] = bill.ServicePointId;
                billingDatarow[BillingColumn_ServiceContractId] = bill.ServiceContractId;
                billingDatarow[BillingColumn_MeterType] = bill.MeterType;
                billingDatarow[BillingColumn_ReplacedMeterId] = bill.ReplacedMeterId;
                billingDatarow[BillingColumn_ReadQuality] = bill.ReadQuality;
                billingDatarow[BillingColumn_ReadDate] = bill.ReadDate?.ToString("yyyy-MM-dd HH:mm:ss");
                billingDatarow[BillingColumn_DueDate] = bill.DueDate?.ToString("yyyy-MM-dd");

                _billingDataTable.Rows.Add(billingDatarow);
            }

            using (var ent = new InsightsBulkEntities())
            {
                // If using Code First we need to make sure the model is built before we open the connection 
                // This isn't required for models created with the EF Designer 
                ent.Database.Initialize(false);

                var cmd = ent.Database.Connection.CreateCommand();
                cmd.CommandText = StoredProcedureName_BillInsert;
                cmd.CommandType = CommandType.StoredProcedure;

                SqlParameter billtableParm = new SqlParameter("@billtbl", typeof(string))
                {
                    TypeName = Billingtabletypestring,
                    Value = _billingDataTable
                };
                cmd.Parameters.Add(billtableParm);

                int rowsAffected;
                try
                {
                    ent.Database.Connection.Open();
                    var result = cmd.ExecuteScalar();
                    if (result == null || result.ToString() == string.Empty)
                    {
                        rowsAffected = 0;
                    }
                    else
                        rowsAffected = Convert.ToInt32(result);
                }
                finally
                {
                    ent.Database.Connection.Close();
                }
                return rowsAffected;
            }
        }

        /// <summary>
        /// WritePremiseAttributeData
        /// </summary>
        /// <param name="premiseAttributeTable"></param>
        /// <param name="statusList"></param>
        /// <returns></returns>
        public int WritePremiseAttributeData(List<PremiseAttribute> premiseAttributeTable, ref StatusList statusList)
        {
            if (_premiseAttributeDataTable == null)
            {
                //create the table
                _premiseAttributeDataTable = CreateDataTable(PremiseattributetabletypeNameOnlyString);
            }
            else
            {
                _premiseAttributeDataTable.Clear();
            }

            foreach (var attribute in premiseAttributeTable)
            {
                var premiseAttributeDatarow = _premiseAttributeDataTable.NewRow();

                premiseAttributeDatarow[PremiseAttributeColumn_ClientId] = attribute.ClientID;
                premiseAttributeDatarow[PremiseAttributeColumn_CustomerID] = attribute.CustomerID;
                premiseAttributeDatarow[PremiseAttributeColumn_AccountID] = attribute.AccountID;
                premiseAttributeDatarow[PremiseAttributeColumn_PremiseID] = attribute.PremiseID;
                premiseAttributeDatarow[PremiseAttributeColumn_UpdateDate] = attribute.UpdateDate.ToString("yyyy-MM-dd HH:mm:ss");
                premiseAttributeDatarow[PremiseAttributeColumn_CreateDate] = attribute.CreateDate?.ToString("yyyy-MM-dd HH:mm:ss");
                premiseAttributeDatarow[PremiseAttributeColumn_AttributeKey] = attribute.AttributeKey;
                premiseAttributeDatarow[PremiseAttributeColumn_OptionValue] = attribute.OptionValue;
                premiseAttributeDatarow[PremiseAttributeColumn_TrackingID] = attribute.TrackingID;
                premiseAttributeDatarow[PremiseAttributeColumn_TrackingDate] = attribute.TrackingDate.ToString("yyyy-MM-dd HH:mm:ss");
                premiseAttributeDatarow[PremiseAttributeColumn_SourceId] = attribute.SourceId;
                premiseAttributeDatarow[PremiseAttributeColumn_AttributeId] = -1;

                _premiseAttributeDataTable.Rows.Add(premiseAttributeDatarow);
            }

            int rowsAffected;

            using (var ent = new InsightsBulkEntities())
            {
                // If using Code First we need to make sure the model is built before we open the connection 
                // This isn't required for models created with the EF Designer 
                ent.Database.Initialize(false);

                var cmd = ent.Database.Connection.CreateCommand();
                cmd.CommandText = StoredProcedureName_PremiseAttributeInsert;
                cmd.CommandType = CommandType.StoredProcedure;

                SqlParameter premiseAttributeTableParm = new SqlParameter("@premiseattributetbl", typeof(string))
                {
                    TypeName = Premiseattributetabletypestring,
                    Value = _premiseAttributeDataTable
                };
                cmd.Parameters.Add(premiseAttributeTableParm);

                try
                {
                    ent.Database.Connection.Open();
                    var result = cmd.ExecuteScalar();
                    if (result == null || result.ToString() == string.Empty)
                    {
                        rowsAffected = 0;
                    }
                    else
                    {
                        rowsAffected = Convert.ToInt32(result);
                    }
                }
                finally
                {
                    ent.Database.Connection.Close();
                }
            }

            return rowsAffected;
        }

        /// <summary>
        /// WriteActionKeyData
        /// </summary>
        /// <param name="actionItemTable"></param>
        /// <param name="statusList"></param>
        /// <returns></returns>
        public int WriteActionKeyData (List<ActionItem> actionItemTable, ref StatusList statusList)
        {
            if (_actionOutputDataTable == null)
            {
                //create the table
                _actionOutputDataTable = CreateDataTable(ActionkeytabletypeNameOnlyString);
            }
            else
            {
                _actionOutputDataTable.Clear();
            }

            foreach (var attribute in actionItemTable)
            {
                var actionKeyDataRow = _actionOutputDataTable.NewRow();

                actionKeyDataRow[ActionItemColumn_ClientId] = attribute.ClientId;
                actionKeyDataRow[ActionItemColumn_CustomerID] = attribute.CustomerId;
                actionKeyDataRow[ActionItemColumn_AccountID] = attribute.AccountID;
                actionKeyDataRow[ActionItemColumn_PremiseID] = attribute.PremiseId;
                actionKeyDataRow[ActionItemColumn_StatusId] = attribute.StatusId;
                actionKeyDataRow[ActionItemColumn_StatusDate] = attribute.StatusDate.ToString("yyyy-MM-dd HH:mm:ss"); 
                actionKeyDataRow[ActionItemColumn_SourceId] = attribute.SourceId;
                actionKeyDataRow[ActionItemColumn_ActionKey] = attribute.ActionKey;
                actionKeyDataRow[ActionItemColumn_SubActionKey] = attribute.SubActionKey ?? string.Empty;
                actionKeyDataRow[ActionItemColumn_ActionData] = attribute.ActionData;
                actionKeyDataRow[ActionItemColumn_ActionDataValue] = attribute.ActionDataValue; 
                actionKeyDataRow[ActionItemColumn_TrackingID] = attribute.SourceId;
                actionKeyDataRow[ActionItemColumn_TrackingDate] = attribute.TrackingDate.ToString("yyyy-MM-dd HH:mm:ss");

                _actionOutputDataTable.Rows.Add(actionKeyDataRow);
            }

            int rowsAffected;
            using (var ent = new InsightsBulkEntities())
            {
                // If using Code First we need to make sure the model is built before we open the connection 
                // This isn't required for models created with the EF Designer 
                ent.Database.Initialize(false);

                var cmd = ent.Database.Connection.CreateCommand();
                cmd.CommandText = StoredProcedureName_ActionItemInsert;
                cmd.CommandType = CommandType.StoredProcedure;

                SqlParameter actionItemTableParm = new SqlParameter("@actionItemtbl", typeof(string))
                {
                    TypeName = Actionitemtabletypestring,
                    Value = _actionOutputDataTable
                };
                cmd.Parameters.Add(actionItemTableParm);

                try
                {
                    ent.Database.Connection.Open();
                    var result = cmd.ExecuteScalar();
                    if (result == null || result.ToString() == string.Empty)
                    {
                        rowsAffected = 0;
                    }
                    else
                    {
                        rowsAffected = Convert.ToInt32(result);
                    }
                }
                finally
                {
                    ent.Database.Connection.Close();
                }
            }

            return rowsAffected;
        }

        /// <summary>
        /// CreateDataTable
        /// </summary>
        /// <param name="userDefinedTableTypeName"></param>
        /// <returns></returns>
        public DataTable CreateDataTable(string userDefinedTableTypeName)
        {
            var userDefinedTableTypeColumnList = GetUserDefinedTableTypeColumnList(userDefinedTableTypeName);

            var result = new DataTable(userDefinedTableTypeName);

            foreach (var userDefinedTableTypeColumn in userDefinedTableTypeColumnList)
            {
                var dataTypeAsText = ConvertDatabaseDataTypeToSystemDataType(userDefinedTableTypeColumn.SystemTypeName);
                var dataType = Type.GetType(dataTypeAsText);

                if (dataType == null)
                {
                    throw new ArgumentNullException(
                        $"Could not convert database data type ({dataTypeAsText}) to system type.");
                }

                var dataColumn = new DataColumn(userDefinedTableTypeColumn.ColumnName, dataType);

                switch (dataTypeAsText)
                {
                    case "System.String":
                        dataColumn.MaxLength = userDefinedTableTypeColumn.MaxLength;
                        break;

                }

                result.Columns.Add(dataColumn);
            }

            return result;
        }

        /// <summary>
        /// ConvertDatabaseDataTypeToSystemDataType
        /// </summary>
        /// <param name="databaseSystemType"></param>
        /// <returns></returns>
        protected string ConvertDatabaseDataTypeToSystemDataType(string databaseSystemType)
        {
            string result;

            switch (databaseSystemType.ToLower())
            {
                case "bigint":
                    result = "System.Int64";
                    break;
                case "bit":
                    result = "System.Boolean";
                    break;
                case "char":
                    result = "System.String";
                    break;
                case "float":
                    result = "System.Double";
                    break;
                case "decimal":
                    result = "System.Double";
                    break;
                case "int":
                    result = "System.Int32";
                    break;
                case "nchar":
                    result = "System.String";
                    break;
                case "ntext":
                    result = "System.String";
                    break;
                case "nvarchar":
                    result = "System.String";
                    break;
                case "real":
                    result = "System.Single";
                    break;
                case "smallint":
                    result = "System.Int16";
                    break;
                case "tinyint":
                    result = "System.Byte";
                    break;
                case "text":
                    result = "System.String";
                    break;
                case "varchar":
                    result = "System.String";
                    break;
                case "xml":
                    result = "System.String";
                    break;
                case "datetime":
                    result = "System.String";
                    break;
                default:
                    throw new ArgumentOutOfRangeException($"Unexpected database system type: {databaseSystemType}.");
            }

            return result;
        }

        /// <summary>
        /// UserDefinedTableTypeColumnList
        /// </summary>
        /// <param name="userDefinedTableTypeName"></param>
        /// <returns></returns>
        private UserDefinedTableTypeColumnList GetUserDefinedTableTypeColumnList(string userDefinedTableTypeName)
        {
            using (var ent = new InsightsBulkEntities())
            {
                // If using Code First we need to make sure the model is built before we open the connection 
                // This isn't required for models created with the EF Designer 
                ent.Database.Initialize(false);

                var cmd = ent.Database.Connection.CreateCommand();
                cmd.CommandText = "[Holding].[SelectUserDefinedTableTypeColumns]";
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.Add(new SqlParameter("@UserDefinedTableTypeName", typeof(string)) { Value = userDefinedTableTypeName });
                UserDefinedTableTypeColumnList result;
                try
                {
                    ent.Database.Connection.Open();
                    var sqlDataReader = cmd.ExecuteReader();

                    //Retrieve command text.
                    result = ConvertSqlDataReaderToUserDefinedTableTypeColumnList(sqlDataReader);

                }
                finally
                {
                    ent.Database.Connection.Close();
                }
                return result;
            }
        }

        /// <summary>
        /// UserDefinedTableTypeColumnList
        /// </summary>
        /// <param name="sqlDataReader"></param>
        /// <returns></returns>
        protected UserDefinedTableTypeColumnList ConvertSqlDataReaderToUserDefinedTableTypeColumnList(DbDataReader sqlDataReader)
        {
            //Validate parameter: sql data reader.
            string exceptionMessage;

            if (sqlDataReader == null)
            {
                exceptionMessage = "Sql data reader required.";
                throw new ArgumentException(exceptionMessage);
            }

            if (sqlDataReader.HasRows == false)
            {
                exceptionMessage = "Sql data reader has no records.";
                throw new ArgumentException(exceptionMessage);
            }

            var result = new UserDefinedTableTypeColumnList();

            //Iterate through list of user defined table type column definitions.
            while (sqlDataReader.Read())
            {

                var userDefinedTableTypeColumn = new UserDefinedTableTypeColumn();

                //Iterate through data reader fields.
                for (int fieldIndex = 0; fieldIndex < sqlDataReader.FieldCount; fieldIndex++)
                {

                    //Field (column) value is NULL.
                    if (sqlDataReader.IsDBNull(fieldIndex))
                    {
                        //Skip NULL database values.
                        continue;
                    }

                    var columnName = sqlDataReader.GetName(fieldIndex);

                    switch (columnName)
                    {

                        case ColumnName_ColumnName:
                            userDefinedTableTypeColumn.ColumnName = sqlDataReader.GetString(
                                sqlDataReader.GetOrdinal(ColumnName_ColumnName));
                            break;

                        case ColumnName_ColumnID:
                            userDefinedTableTypeColumn.ColumnID = sqlDataReader.GetInt32(
                                sqlDataReader.GetOrdinal(ColumnName_ColumnID));

                            break;
                        case ColumnName_SystemTypeID:
                            var tempByte = sqlDataReader.GetByte(sqlDataReader.GetOrdinal(ColumnName_SystemTypeID));
                            userDefinedTableTypeColumn.SystemTypeID = tempByte;
                            break;

                        case ColumnName_SystemTypeName:
                            userDefinedTableTypeColumn.SystemTypeName = sqlDataReader.GetString(
                                sqlDataReader.GetOrdinal(ColumnName_SystemTypeName));
                            break;

                        case ColumnName_MaximumLength:
                            userDefinedTableTypeColumn.MaxLength = sqlDataReader.GetInt16(
                                sqlDataReader.GetOrdinal(ColumnName_MaximumLength));
                            break;

                        default:
                            exceptionMessage = $"Unexpected column name: {columnName}";
                            throw new ArgumentOutOfRangeException(exceptionMessage);

                    }
                }

                result.Add(userDefinedTableTypeColumn);
            }

            return result;
        }
    }
}
