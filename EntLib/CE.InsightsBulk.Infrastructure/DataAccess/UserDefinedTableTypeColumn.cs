﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CE.InsightsBulk.Infrastructure.DataAccess
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

 
        /// <summary>
        /// User defined table type column.
        /// </summary>
        public class UserDefinedTableTypeColumn
        {

            #region "Protected Constants"
            #endregion

            #region "Private data members"

            private string _columnName;
            private int _columnID;
            private int _systemTypeID;
            private string _systemTypeName;
            private int _maxLength;

            public string ColumnName
            {
                get { return _columnName; }
                set { _columnName = value; }
            }

            public int ColumnID
            {
                get { return _columnID; }
                set { _columnID = value; }
            }

            public int SystemTypeID
            {
                get { return _systemTypeID; }
                set { _systemTypeID = value; }
            }

            public string SystemTypeName
            {
                get { return _systemTypeName; }
                set { _systemTypeName = value; }
            }

            public int MaxLength
            {
                get { return _maxLength; }
                set { _maxLength = value; }
            }

            #endregion

            #region "Public Properties"
            #endregion

            #region "Public Constructors"
            #endregion

            #region "Public Methods"
            #endregion

            #region "Protected Methods"
            #endregion


    }

}
