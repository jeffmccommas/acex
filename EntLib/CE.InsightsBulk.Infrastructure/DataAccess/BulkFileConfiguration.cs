﻿using CE.InsightsMeta.Model;
using System;
using System.Globalization;
using System.Linq;

namespace CE.InsightsBulk.Infrastructure.DataAccess
{
    public class BulkFileConfiguration
    {
        /// <summary>
        /// Get the client configuration file for a specific component name and enviornment.
        /// </summary>
        /// <param name="clientID">The client Id </param>
        /// <param name="componentName">The component name</param>
        /// <param name="environment">The enviornment</param>
        /// <param name="configuration">The found configuration</param>
        public void GetConfiguration(int clientID, string componentName, string environment, out string configuration)
        {
            var db = new InsightsMetadataEntities();
            configuration = "";

            var query = db.ETLBulkConfigurations.Where(c => c.ClientComponent == componentName &&
                                                            c.ClientID == clientID &&
                                                            c.Environment == environment);


            foreach (var item in query)
            {
                configuration =  item.XMLConfiguration;
            }
        }
    }
}
