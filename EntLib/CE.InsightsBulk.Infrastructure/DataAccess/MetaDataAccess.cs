﻿using CE.InsightsBulk.Infrastructure.Interfaces;
using CE.InsightsMeta.Model;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using CE.InsightsBulk.Infrastructure.SharedClasses;

// ReSharper disable once CheckNamespace
namespace CE.InsightsBulk.Infrastructure
{
    public class MetaDataAccess : IMetaDataAccess
    {
        public bool IsETLRunning(int clientId, bool verbose)
        {
            var returnValue = false;
            var appSettings = ConfigurationManager.AppSettings;

            try
            {
                var sqlAgentJobName = appSettings["ETLJobName"];
                sqlAgentJobName = sqlAgentJobName + clientId;
                SqlConnection jobConnection;
                var settings = ConfigurationManager.ConnectionStrings["InsightsSqlAgent"];
                string jobConnectionString = settings.ConnectionString;


                using (jobConnection = new SqlConnection(jobConnectionString))
                {
                    var jobCommand = new SqlCommand("sp_help_job", jobConnection)
                    {
                        CommandType = CommandType.StoredProcedure
                    };

                    var jobReturnValue = new SqlParameter("@RETURN_VALUE", SqlDbType.Int)
                    {
                        Direction = ParameterDirection.ReturnValue
                    };
                    jobCommand.Parameters.Add(jobReturnValue);

                    var jobParameter = new SqlParameter("@job_name", SqlDbType.VarChar)
                    {
                        Direction = ParameterDirection.Input,
                        Value = sqlAgentJobName
                    };
                    jobCommand.Parameters.Add(jobParameter);

                    jobConnection.Open();

                    using (IDataReader reader = jobCommand.ExecuteReader())
                    {
                        //enabled
                        //current_execution_status

                        //Value Description
                        //0   Returns only those jobs that are not idle or suspended.
                        //1   Executing.
                        //2   Waiting for thread.
                        //3   Between retries.
                        //4   Idle.
                        //5   Suspended.
                        //7   Performing completion actions.
                        while (reader.Read())
                        {
                            var currentExecutionStatus =
                                                                Convert.ToInt32(reader["current_execution_status"].ToString());

                            if (verbose)
                            {
                                Console.WriteLine("ETL current_execution_status ordinal= " +
                                                  reader.GetOrdinal("current_execution_status"));
                                Console.WriteLine("ETL current_execution_status = " + currentExecutionStatus);
                            }

                            if (currentExecutionStatus == 1)
                            {
                                returnValue = true;
                            }
                        }

                    }
                    jobConnection.Close();
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Cannot determine whether ETL is running : " +  ex);
            }

            return returnValue;
        }


        public void LogDBEvent(string message, int clientId)
        {
            using (InsightsMetadataEntities ent = new InsightsMetadataEntities())
            {
                ent.WriteExecutionError("Gen Error", "CE.InsightsBulk.LoadData",
                    DateTime.Now.ToString("yyyy-MM-dd H:mm:ss.fff"), message, clientId, message);
            }

        }

        public void LogErrorsByInputType(Dictionary<string, string> dict, string message,
            BulkLoadConstants.RunType runType, bool LogErrors)
        {
            if (LogErrors)
            {
                using (InsightsMetadataEntities ent = new InsightsMetadataEntities())
                {
                    switch (runType)
                    {
                        case BulkLoadConstants.RunType.Customer:
                            ent.InsertCustomerImportErrors(dict[BulkLoadConstants.TRACKINGID],
                                Convert.ToInt32(dict[BulkLoadConstants.CLIENTID]), DateTime.Now,
                                dict[BulkLoadConstants.CUSTOMERID], dict[BulkLoadConstants.ACCOUNTID],
                                dict[BulkLoadConstants.PREMISEID], message);
                            break;
                        case BulkLoadConstants.RunType.Bill:
                            ent.InsertBillImportErrors(dict[BulkLoadConstants.TRACKINGID],
                                Convert.ToInt32(dict[BulkLoadConstants.CLIENTID]), DateTime.Now,
                                dict[BulkLoadConstants.ACCOUNTID], dict[BulkLoadConstants.PREMISEID],
                                dict[BulkLoadConstants.SERVICEPOINTID], dict[BulkLoadConstants.STARTDATE],
                                dict[BulkLoadConstants.ENDDATE], message);
                            break;
                        case BulkLoadConstants.RunType.Profile:
                            ent.InsertProfileImportErrors(dict[BulkLoadConstants.TRACKINGID],
                                Convert.ToInt32(dict[BulkLoadConstants.CLIENTID]), DateTime.Now,
                                dict[BulkLoadConstants.ACCOUNTID], dict[BulkLoadConstants.PREMISEID],
                                dict[BulkLoadConstants.ATTRIBUTEKEY], dict[BulkLoadConstants.ATTRIBUTEVALUE], message);
                            break;
                        case BulkLoadConstants.RunType.ActionItem:
                            ent.InsertActionItemImportErrors(dict[BulkLoadConstants.TRACKINGID],
                                Convert.ToInt32(dict[BulkLoadConstants.CLIENTID]), DateTime.Now,
                                dict[BulkLoadConstants.CUSTOMERID], dict[BulkLoadConstants.ACCOUNTID],
                                dict[BulkLoadConstants.PREMISEID], dict[BulkLoadConstants.ACTIONKEY], message);
                            break;
                    }

                }
            }
        }



    }
}
