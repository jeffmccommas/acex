﻿using System;
using System.Linq;
using System.Configuration;
using CE.InsightsBulk.Infrastructure.DataAccess;
using System.Xml.Linq;
using CE.InsightsBulk.Infrastructure.Interfaces;
using CE.InsightsBulk.Infrastructure.SharedClasses;

namespace CE.InsightsBulk.LoadData
{
    public class FileLoadConfiguration : IConfig
    {
        const string CONFIG_FILEWORKINGEXTENSION = "FileWorkingExtension";
        const string CONFIG_FILEDONEEXTENSION = "FileDoneExtension";

        private const string ComponentName = "BulkLoad";
        private const string XmlRoot = "BulkLoad";

        private readonly int _clientId;
        private string _environment;

        private const string XML_CUSTOMER_FILESEARCHPATTERN = "CustomerFileSearchPattern";
        private const string XML_CUSTOMER_FILEPATH = "CustomerFilePath";
        private const string XML_CUSTOMER_ARCHIVE_PATH = "CustomerArchivePath";
        private const string XML_CUSTOMER_XSD = "CustomerXSD";

        private const string XML_BILL_FILESEARCHPATTERN = "BillFileSearchPattern";
        private const string XML_BILL_FILEPATH = "BillFilePath";
        private const string XML_BILL_ARCHIVE_PATH = "BillArchivePath";
        private const string XML_BILLING_XSD = "BillingXSD";

        private const string XML_PROFILE_FILESEARCHPATTERN = "ProfileFileSearchPattern";
        private const string XML_PROFILE_FILEPATH = "ProfileFilePath";
        private const string XML_PROFILE_ARCHIVE_PATH = "ProfileArchivePath";
        private const string XML_PROFILE_XSD = "ProfileXSD";

        private const string XML_ACTIONITEM_FILESEARCHPATTERN = "ActionItemFileSearchPattern";
        private const string XML_ACTIONITEM_FILEPATH = "ActionItemFilePath";
        private const string XML_ACTIONITEM_ARCHIVE_PATH = "ActionItemArchivePath";
        private const string XML_ACTIONITEM_XSD = "ActionItemXSD";

        private const string XML_FILE_OUTPUT_EXTENSION = "FileOutputExtension";

        private const string CONFIG_ENVIRONMENT = "Environment";

        private const string MAX_BATCHSIZE = "MaxBatchSize";

        private const string FILE_COMPLETE_WITHERRORS_EXT = "FileDoneWithErrorsExtension";
        private const string FILE_ERRORLOG = "ErrorLogFile";

        private const string IMPORTSPECIFICLOGGINGON = "ImportSpecificLoggingOn";

        public FileLoadConfiguration(int clientId)
        {
            _clientId = clientId;
        }

        protected FileLoadConfiguration()
        {
        }

        public string GeneralErrorLog { get; set; }

        public string FileOutputExtension { get; set; }

        private bool _importSpecificLoggingOn;

        public bool ImportSpecificLoggingOn
        {
            get { return _importSpecificLoggingOn; }
            set { _importSpecificLoggingOn = value; }
        }

        public string InputFilePath { get; set; }

        public string ArchivePath { get; set; }

        public string FileSearchPattern { get; set; }

        public string FileWorkingExtension { get; set; }

        public int MaxBatchSize { get; set; }

        public string FileDoneExtension { get; set; }

        public string InputXSD { get; set; }

        public string FileErrorExtension { get; set; }

        /// <summary>
        /// Load the application settings
        /// </summary>
        private void LoadAppSettings()
        {
            var appSettings = ConfigurationManager.AppSettings;
            FileDoneExtension = appSettings[CONFIG_FILEDONEEXTENSION];
            FileWorkingExtension = appSettings[CONFIG_FILEWORKINGEXTENSION];
            _environment = appSettings[CONFIG_ENVIRONMENT];
            MaxBatchSize = Convert.ToInt32(appSettings[MAX_BATCHSIZE]);
            FileErrorExtension = appSettings[FILE_COMPLETE_WITHERRORS_EXT];
            GeneralErrorLog = appSettings[FILE_ERRORLOG];
        }

        /// <summary>
        /// Load the Customer configuration
        /// </summary>
        public void LoadCustomerConfiguration()
        {
            var config = new BulkFileConfiguration();
            string xmlConfig;

            //load settings from appconfig
            LoadAppSettings();

            //Load Settings from file
            config.GetConfiguration(_clientId, ComponentName, _environment, out xmlConfig);
            if (string.IsNullOrEmpty(xmlConfig))
            {
                throw new ConfigurationErrorsException($"Customer Configuration is missing for client {_clientId}");
            }

            var xdoc = XDocument.Parse(xmlConfig);

            InputFilePath = xdoc.Elements(XmlRoot)
                .Select(x => new {x, xElement = x.Element(XML_CUSTOMER_FILEPATH)})
                .Where(t => t.xElement != null)
                .Select(t => t.xElement.Value).ToList().FirstOrDefault();

            FileSearchPattern = (xdoc.Elements(XmlRoot)
                .Select(x => new {x, element = x.Element(XML_CUSTOMER_FILESEARCHPATTERN)})
                .Where(t => t.element != null)
                .Select(t => t.element.Value)).ToList().FirstOrDefault();

            FileOutputExtension = (xdoc.Elements(XmlRoot)
                .Select(x => new {x, o = x.Element(XML_FILE_OUTPUT_EXTENSION)})
                .Where(t => t.o != null)
                .Select(t => t.o.Value)).ToList().FirstOrDefault();

            ArchivePath = (xdoc.Elements(XmlRoot)
                .Select(x => new {x, xElement1 = x.Element(XML_CUSTOMER_ARCHIVE_PATH)})
                .Where(t => t.xElement1 != null)
                .Select(t => t.xElement1.Value)).ToList().FirstOrDefault();

            InputXSD = (xdoc.Elements(XmlRoot)
                .Select(x => x.Element(XML_CUSTOMER_XSD))
                .Where(element1 => element1 != null)
                .Select(element1 => element1.Value)).ToList().FirstOrDefault();

            var loggingon = xdoc.Elements(XmlRoot).Select(x => x.Element(IMPORTSPECIFICLOGGINGON).ElementValueNull()).FirstOrDefault();

            if (!(bool.TryParse(loggingon,out _importSpecificLoggingOn)))
            { 
                _importSpecificLoggingOn = true;
            }
        }

        /// <summary>
        /// Load the Billing configuration
        /// </summary>
        public void LoadBillingConfiguration()
        {

            var config = new BulkFileConfiguration();
            string xmlConfig;

            //load settings from appconfig
            LoadAppSettings();

            //Load Settings from file
            config.GetConfiguration(_clientId, ComponentName, _environment, out xmlConfig);
            if (string.IsNullOrEmpty(xmlConfig))
            {
                throw new ConfigurationErrorsException($"Billing Configuration is missing for client {_clientId}");
            }

            var xdoc = XDocument.Parse(xmlConfig);

            InputFilePath = xdoc.Elements(XmlRoot)
                .Select(x => new {x, xElement = x.Element(XML_BILL_FILEPATH)})
                .Where(t => t.xElement != null)
                .Select(t => t.xElement.Value).ToList().FirstOrDefault();

            FileSearchPattern = xdoc.Elements(XmlRoot)
                .Select(x => new {x, element = x.Element(XML_BILL_FILESEARCHPATTERN)})
                .Where(t => t.element != null)
                .Select(t => t.element.Value).ToList().FirstOrDefault();

            FileOutputExtension = xdoc.Elements(XmlRoot)
                .Select(x => new {x, o = x.Element(XML_FILE_OUTPUT_EXTENSION)})
                .Where(t => t.o != null)
                .Select(t => t.o.Value).ToList().FirstOrDefault();

            ArchivePath = xdoc.Elements(XmlRoot)
                .Select(x => new {x, xElement1 = x.Element(XML_BILL_ARCHIVE_PATH)})
                .Where(t => t.xElement1 != null)
                .Select(t => t.xElement1.Value).ToList().FirstOrDefault();

            InputXSD = xdoc.Elements(XmlRoot)
                .Select(x => new {x, element1 = x.Element(XML_BILLING_XSD)})
                .Where(t => t.element1 != null)
                .Select(t => t.element1.Value).ToList().FirstOrDefault();

            var loggingon = xdoc.Elements(XmlRoot).Select(x => x.Element(IMPORTSPECIFICLOGGINGON).ElementValueNull()).FirstOrDefault();

            if (!(bool.TryParse(loggingon, out _importSpecificLoggingOn)))
            {
                _importSpecificLoggingOn = true;
            }
        }

        /// <summary>
        /// Load the Billing configuration
        /// </summary>
        public void LoadProfileConfiguration()
        {
            var config = new BulkFileConfiguration();
            string xmlConfig;

            //load settings from appconfig
            LoadAppSettings();

            //Load Settings from file
            config.GetConfiguration(_clientId, ComponentName, _environment, out xmlConfig);

            if (string.IsNullOrEmpty(xmlConfig))
            {
                throw new ConfigurationErrorsException($"Profile Configuration is missing for client {_clientId}");
            }

            var xdoc = XDocument.Parse(xmlConfig);

            InputFilePath = xdoc.Elements(XmlRoot)
                .Select(x => new {x, xElement = x.Element(XML_PROFILE_FILEPATH)})
                .Where(t => t.xElement != null)
                .Select(t => t.xElement.Value).ToList().FirstOrDefault();

            FileSearchPattern = xdoc.Elements(XmlRoot)
                .Select(x => x.Element(XML_PROFILE_FILESEARCHPATTERN))
                .Where(element => element != null)
                .Select(element => element.Value).ToList().FirstOrDefault();

            FileOutputExtension = (xdoc.Elements(XmlRoot)
                .Select(x => new {x, o = x.Element(XML_FILE_OUTPUT_EXTENSION)})
                .Where(t => t.o != null)
                .Select(t => t.o.Value)).ToList().FirstOrDefault();

            ArchivePath = xdoc.Elements(XmlRoot)
                .Select(x => new {x, xElement1 = x.Element(XML_PROFILE_ARCHIVE_PATH)})
                .Where(t => t.xElement1 != null)
                .Select(t => t.xElement1.Value).ToList().FirstOrDefault();

            InputXSD = xdoc.Elements(XmlRoot)
                .Select(x => new {x, element1 = x.Element(XML_PROFILE_XSD)})
                .Where(t => t.element1 != null)
                .Select(t => t.element1.Value).ToList().FirstOrDefault();

            var loggingon = xdoc.Elements(XmlRoot).Select(x => x.Element(IMPORTSPECIFICLOGGINGON).ElementValueNull()).ToList().FirstOrDefault();

            if (!bool.TryParse(loggingon, out _importSpecificLoggingOn))
            {
                _importSpecificLoggingOn = true;
            }
        }

        /// <summary>
        /// Load the Action Item configuration
        /// </summary>
        public void LoadActionItemConfiguration()
        {
            var config = new BulkFileConfiguration();
            string xmlConfig;

            //load settings from appconfig
            LoadAppSettings();

            //Load Settings from file
            config.GetConfiguration(_clientId, ComponentName, _environment, out xmlConfig);

            if (string.IsNullOrEmpty(xmlConfig))
            {
                throw new ConfigurationErrorsException($"Action Item Configuration is missing for client {_clientId}");
            }

            var xdoc = XDocument.Parse(xmlConfig);

            InputFilePath = xdoc.Elements(XmlRoot)
                .Select(x => new {x, xElement = x.Element(XML_ACTIONITEM_FILEPATH)})
                .Where(t => t.xElement != null)
                .Select(t => t.xElement.Value).ToList().FirstOrDefault();

            FileSearchPattern = xdoc.Elements(XmlRoot)
                .Select(x => new {x, element = x.Element(XML_ACTIONITEM_FILESEARCHPATTERN)})
                .Where(t => t.element != null)
                .Select(t => t.element.Value).ToList().FirstOrDefault();

            FileOutputExtension = xdoc.Elements(XmlRoot)
                .Select(x => new {x, o = x.Element(XML_FILE_OUTPUT_EXTENSION)})
                .Where(t => t.o != null)
                .Select(t => t.o.Value).ToList().FirstOrDefault();

            ArchivePath = xdoc.Elements(XmlRoot)
                .Select(x => new {x, xElement1 = x.Element(XML_ACTIONITEM_ARCHIVE_PATH)})
                .Where(t => t.xElement1 != null)
                .Select(t => t.xElement1.Value).ToList().FirstOrDefault();

            InputXSD = xdoc.Elements(XmlRoot)
                .Select(x => new {x, element1 = x.Element(XML_ACTIONITEM_XSD)})
                .Where(t => t.element1 != null)
                .Select(t => t.element1.Value).ToList().FirstOrDefault();

            var loggingon = (xdoc.Elements(XmlRoot).Select(x => x.Element(IMPORTSPECIFICLOGGINGON).ElementValueNull())).ToList().FirstOrDefault();

            if (!bool.TryParse(loggingon, out _importSpecificLoggingOn))
            {
                _importSpecificLoggingOn = true;
            }
        }
    }
}
