﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace CE.InsightsBulk.Infrastructure.SharedClasses
{
        [Serializable]
    public class InsightsBulkLoadException : Exception
    {

            #region "Public Constructors"

            public InsightsBulkLoadException()
                : base()
            {
            }

            public InsightsBulkLoadException(string message)
                : base(message)
            {
            }

            public InsightsBulkLoadException(string message, Exception inner)
                : base(message, inner)
            {
            }

            #endregion

            #region "Protected Constructors"

            protected InsightsBulkLoadException(SerializationInfo info, StreamingContext context)
                : base(info, context)
            {
            }

            #endregion


    
    }
}
