﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CommandLine;
using CommandLine.Text;

namespace CE.InsightsBulk.Infrastructure.SharedClasses
{
    public class Options
    {
  [Option("c",HelpText = "ClientID Id", Required = true)]
  public int ClientId { get; set; }

  [Option("t", HelpText = "File Type: cust = customer, bill = billing,prof = profile", Required = true)]
  public string FileType { get; set; }


  [Option("v", HelpText = "Print details during execution.")]
  public bool Verbose { get; set; }

   [HelpOption]
  public string GetUsage()
  {
    // this without using CommandLine.Text
    var usage = new StringBuilder();
    usage.AppendLine("Load Customer Bulk Data usage:  LoadCustomer -c87 -tcust ");
    return usage.ToString();
  }
    }
}
