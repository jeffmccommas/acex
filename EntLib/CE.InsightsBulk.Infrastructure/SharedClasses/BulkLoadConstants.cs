﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CE.InsightsBulk.Infrastructure.SharedClasses
{
    public class BulkLoadConstants
    {
        public enum RunType
        {
            Customer,
            Bill,
            Profile,
            ActionItem
        }

        public const string PREMISEID = "PremiseId";
        public const string ACCOUNTID = "AccountId";
        public const string CUSTOMERID = "CustomerId";
        public const string TRACKINGID = "TrackingId";
        public const string CLIENTID = "ClientId";
        public const string ACTIONKEY = "ActionKey";
        //values for Profile Data
        public const string ATTRIBUTEKEY = "AttributeKey";
        public const string ATTRIBUTEVALUE = "AttributeValue";

        //Values for Billing Data
        public const string SERVICEPOINTID = "ServicePointId";
        public const string STARTDATE = "StartDate";
        public const string ENDDATE = "EndDate";

    }
}
