﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace CE.InsightsMeta.Model
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Infrastructure;
    using System.Data.Entity.Core.Objects;
    using System.Linq;
    
    public partial class InsightsMetadataEntities : DbContext
    {
        public InsightsMetadataEntities()
            : base("name=InsightsMetadataEntities")
        {
        }
    
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            throw new UnintentionalCodeFirstException();
        }
    
        public virtual DbSet<ETLBulkConfiguration> ETLBulkConfigurations { get; set; }
        public virtual DbSet<ETLInfo> ETLInfoes { get; set; }
        public virtual DbSet<ClientAction> ClientActions { get; set; }
        public virtual DbSet<TypeActionStatu> TypeActionStatus { get; set; }
        public virtual DbSet<ClientProfileAttribute> ClientProfileAttributes { get; set; }
    
        public virtual int WriteExecutionError(string trackingId, string projectName, string dateTime, string messageDescription, Nullable<int> clientId, string errorMessage)
        {
            var trackingIdParameter = trackingId != null ?
                new ObjectParameter("TrackingId", trackingId) :
                new ObjectParameter("TrackingId", typeof(string));
    
            var projectNameParameter = projectName != null ?
                new ObjectParameter("ProjectName", projectName) :
                new ObjectParameter("ProjectName", typeof(string));
    
            var dateTimeParameter = dateTime != null ?
                new ObjectParameter("DateTime", dateTime) :
                new ObjectParameter("DateTime", typeof(string));
    
            var messageDescriptionParameter = messageDescription != null ?
                new ObjectParameter("MessageDescription", messageDescription) :
                new ObjectParameter("MessageDescription", typeof(string));
    
            var clientIdParameter = clientId.HasValue ?
                new ObjectParameter("ClientId", clientId) :
                new ObjectParameter("ClientId", typeof(int));
    
            var errorMessageParameter = errorMessage != null ?
                new ObjectParameter("ErrorMessage", errorMessage) :
                new ObjectParameter("ErrorMessage", typeof(string));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("WriteExecutionError", trackingIdParameter, projectNameParameter, dateTimeParameter, messageDescriptionParameter, clientIdParameter, errorMessageParameter);
        }
    
        public virtual int InsertCustomerErrors(string trackingId, Nullable<int> clientId, Nullable<System.DateTime> errorDateTime, string customerId, string accountId, string premiseId, string messageText)
        {
            var trackingIdParameter = trackingId != null ?
                new ObjectParameter("TrackingId", trackingId) :
                new ObjectParameter("TrackingId", typeof(string));
    
            var clientIdParameter = clientId.HasValue ?
                new ObjectParameter("ClientId", clientId) :
                new ObjectParameter("ClientId", typeof(int));
    
            var errorDateTimeParameter = errorDateTime.HasValue ?
                new ObjectParameter("ErrorDateTime", errorDateTime) :
                new ObjectParameter("ErrorDateTime", typeof(System.DateTime));
    
            var customerIdParameter = customerId != null ?
                new ObjectParameter("CustomerId", customerId) :
                new ObjectParameter("CustomerId", typeof(string));
    
            var accountIdParameter = accountId != null ?
                new ObjectParameter("AccountId", accountId) :
                new ObjectParameter("AccountId", typeof(string));
    
            var premiseIdParameter = premiseId != null ?
                new ObjectParameter("PremiseId", premiseId) :
                new ObjectParameter("PremiseId", typeof(string));
    
            var messageTextParameter = messageText != null ?
                new ObjectParameter("MessageText", messageText) :
                new ObjectParameter("MessageText", typeof(string));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("InsertCustomerErrors", trackingIdParameter, clientIdParameter, errorDateTimeParameter, customerIdParameter, accountIdParameter, premiseIdParameter, messageTextParameter);
        }
    
        public virtual int WriteCustomerErrors(string trackingId, Nullable<System.DateTime> errorDateTime, Nullable<int> clientId, string customerId, string accountId, string premiseId, string messageText)
        {
            var trackingIdParameter = trackingId != null ?
                new ObjectParameter("TrackingId", trackingId) :
                new ObjectParameter("TrackingId", typeof(string));
    
            var errorDateTimeParameter = errorDateTime.HasValue ?
                new ObjectParameter("ErrorDateTime", errorDateTime) :
                new ObjectParameter("ErrorDateTime", typeof(System.DateTime));
    
            var clientIdParameter = clientId.HasValue ?
                new ObjectParameter("ClientId", clientId) :
                new ObjectParameter("ClientId", typeof(int));
    
            var customerIdParameter = customerId != null ?
                new ObjectParameter("CustomerId", customerId) :
                new ObjectParameter("CustomerId", typeof(string));
    
            var accountIdParameter = accountId != null ?
                new ObjectParameter("AccountId", accountId) :
                new ObjectParameter("AccountId", typeof(string));
    
            var premiseIdParameter = premiseId != null ?
                new ObjectParameter("PremiseId", premiseId) :
                new ObjectParameter("PremiseId", typeof(string));
    
            var messageTextParameter = messageText != null ?
                new ObjectParameter("MessageText", messageText) :
                new ObjectParameter("MessageText", typeof(string));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("WriteCustomerErrors", trackingIdParameter, errorDateTimeParameter, clientIdParameter, customerIdParameter, accountIdParameter, premiseIdParameter, messageTextParameter);
        }
    
        public virtual int InsertBillImportErrors(string trackingId, Nullable<int> clientId, Nullable<System.DateTime> errorDateTime, string accountId, string premiseId, string servicePointId, string startDate, string endDate, string messageText)
        {
            var trackingIdParameter = trackingId != null ?
                new ObjectParameter("TrackingId", trackingId) :
                new ObjectParameter("TrackingId", typeof(string));
    
            var clientIdParameter = clientId.HasValue ?
                new ObjectParameter("ClientId", clientId) :
                new ObjectParameter("ClientId", typeof(int));
    
            var errorDateTimeParameter = errorDateTime.HasValue ?
                new ObjectParameter("ErrorDateTime", errorDateTime) :
                new ObjectParameter("ErrorDateTime", typeof(System.DateTime));
    
            var accountIdParameter = accountId != null ?
                new ObjectParameter("AccountId", accountId) :
                new ObjectParameter("AccountId", typeof(string));
    
            var premiseIdParameter = premiseId != null ?
                new ObjectParameter("PremiseId", premiseId) :
                new ObjectParameter("PremiseId", typeof(string));
    
            var servicePointIdParameter = servicePointId != null ?
                new ObjectParameter("ServicePointId", servicePointId) :
                new ObjectParameter("ServicePointId", typeof(string));
    
            var startDateParameter = startDate != null ?
                new ObjectParameter("StartDate", startDate) :
                new ObjectParameter("StartDate", typeof(string));
    
            var endDateParameter = endDate != null ?
                new ObjectParameter("EndDate", endDate) :
                new ObjectParameter("EndDate", typeof(string));
    
            var messageTextParameter = messageText != null ?
                new ObjectParameter("MessageText", messageText) :
                new ObjectParameter("MessageText", typeof(string));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("InsertBillImportErrors", trackingIdParameter, clientIdParameter, errorDateTimeParameter, accountIdParameter, premiseIdParameter, servicePointIdParameter, startDateParameter, endDateParameter, messageTextParameter);
        }
    
        public virtual int InsertProfileImportErrors(string trackingId, Nullable<int> clientId, Nullable<System.DateTime> errorDateTime, string accountId, string premiseId, string attributeKey, string attributeValue, string messageText)
        {
            var trackingIdParameter = trackingId != null ?
                new ObjectParameter("TrackingId", trackingId) :
                new ObjectParameter("TrackingId", typeof(string));
    
            var clientIdParameter = clientId.HasValue ?
                new ObjectParameter("ClientId", clientId) :
                new ObjectParameter("ClientId", typeof(int));
    
            var errorDateTimeParameter = errorDateTime.HasValue ?
                new ObjectParameter("ErrorDateTime", errorDateTime) :
                new ObjectParameter("ErrorDateTime", typeof(System.DateTime));
    
            var accountIdParameter = accountId != null ?
                new ObjectParameter("AccountId", accountId) :
                new ObjectParameter("AccountId", typeof(string));
    
            var premiseIdParameter = premiseId != null ?
                new ObjectParameter("PremiseId", premiseId) :
                new ObjectParameter("PremiseId", typeof(string));
    
            var attributeKeyParameter = attributeKey != null ?
                new ObjectParameter("AttributeKey", attributeKey) :
                new ObjectParameter("AttributeKey", typeof(string));
    
            var attributeValueParameter = attributeValue != null ?
                new ObjectParameter("AttributeValue", attributeValue) :
                new ObjectParameter("AttributeValue", typeof(string));
    
            var messageTextParameter = messageText != null ?
                new ObjectParameter("MessageText", messageText) :
                new ObjectParameter("MessageText", typeof(string));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("InsertProfileImportErrors", trackingIdParameter, clientIdParameter, errorDateTimeParameter, accountIdParameter, premiseIdParameter, attributeKeyParameter, attributeValueParameter, messageTextParameter);
        }
    
        public virtual int InsertCustomerImportErrors(string trackingId, Nullable<int> clientId, Nullable<System.DateTime> errorDateTime, string customerId, string accountId, string premiseId, string messageText)
        {
            var trackingIdParameter = trackingId != null ?
                new ObjectParameter("TrackingId", trackingId) :
                new ObjectParameter("TrackingId", typeof(string));
    
            var clientIdParameter = clientId.HasValue ?
                new ObjectParameter("ClientId", clientId) :
                new ObjectParameter("ClientId", typeof(int));
    
            var errorDateTimeParameter = errorDateTime.HasValue ?
                new ObjectParameter("ErrorDateTime", errorDateTime) :
                new ObjectParameter("ErrorDateTime", typeof(System.DateTime));
    
            var customerIdParameter = customerId != null ?
                new ObjectParameter("CustomerId", customerId) :
                new ObjectParameter("CustomerId", typeof(string));
    
            var accountIdParameter = accountId != null ?
                new ObjectParameter("AccountId", accountId) :
                new ObjectParameter("AccountId", typeof(string));
    
            var premiseIdParameter = premiseId != null ?
                new ObjectParameter("PremiseId", premiseId) :
                new ObjectParameter("PremiseId", typeof(string));
    
            var messageTextParameter = messageText != null ?
                new ObjectParameter("MessageText", messageText) :
                new ObjectParameter("MessageText", typeof(string));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("InsertCustomerImportErrors", trackingIdParameter, clientIdParameter, errorDateTimeParameter, customerIdParameter, accountIdParameter, premiseIdParameter, messageTextParameter);
        }
    
        public virtual int InsertActionItemImportErrors(string trackingId, Nullable<int> clientId, Nullable<System.DateTime> errorDateTime, string customerId, string accountId, string premiseId, string actionKey, string messageText)
        {
            var trackingIdParameter = trackingId != null ?
                new ObjectParameter("TrackingId", trackingId) :
                new ObjectParameter("TrackingId", typeof(string));
    
            var clientIdParameter = clientId.HasValue ?
                new ObjectParameter("ClientId", clientId) :
                new ObjectParameter("ClientId", typeof(int));
    
            var errorDateTimeParameter = errorDateTime.HasValue ?
                new ObjectParameter("ErrorDateTime", errorDateTime) :
                new ObjectParameter("ErrorDateTime", typeof(System.DateTime));
    
            var customerIdParameter = customerId != null ?
                new ObjectParameter("CustomerId", customerId) :
                new ObjectParameter("CustomerId", typeof(string));
    
            var accountIdParameter = accountId != null ?
                new ObjectParameter("AccountId", accountId) :
                new ObjectParameter("AccountId", typeof(string));
    
            var premiseIdParameter = premiseId != null ?
                new ObjectParameter("PremiseId", premiseId) :
                new ObjectParameter("PremiseId", typeof(string));
    
            var actionKeyParameter = actionKey != null ?
                new ObjectParameter("ActionKey", actionKey) :
                new ObjectParameter("ActionKey", typeof(string));
    
            var messageTextParameter = messageText != null ?
                new ObjectParameter("MessageText", messageText) :
                new ObjectParameter("MessageText", typeof(string));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("InsertActionItemImportErrors", trackingIdParameter, clientIdParameter, errorDateTimeParameter, customerIdParameter, accountIdParameter, premiseIdParameter, actionKeyParameter, messageTextParameter);
        }
    }
}
