//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace CE.InsightsMeta.Model
{
    using System;
    using System.Collections.Generic;
    
    public partial class ETLBulkConfiguration
    {
        public int ClientID { get; set; }
        public string Environment { get; set; }
        public string ClientComponent { get; set; }
        public string XMLConfiguration { get; set; }
    }
}
