﻿using System.Collections.Generic;
using System.Linq;
using AO.VEE.Business.Contracts;
using AO.VEE.DTO;

namespace AO.VEE.Business
{
    /// <summary>
    /// This class contains methods that are used to do Spike Validation. The validate method of this class contains the main logic for Spike Validation check.
    /// </summary>
    public class SpikeValueValidator : IVeeValidator
    {
        /// <summary>
        /// This constant is part of Spike calculation algorithm which will never change. 
        /// </summary>
        private const double SpikeCalculationConstant = 1.5;

        /// <summary>
        /// The SequenceId property is used to specify the Sequence, in which Spike Vadation will run. The Sequence of Spike Validation is 5.
        /// </summary>
        public int SequenceId { get; set; }

        /// <summary>
        /// The constructor sets the Sequence for Spike Validation
        /// </summary>
        public SpikeValueValidator(int sequenceId)
        {
            SequenceId = sequenceId;
        }

        /// <summary>
        /// This method contains the logic for Spike Validation check. It internally calls FindQuartile method to find the FirstQuartile and ThirdQuartile.
        /// 
        /// </summary>
        /// <param name="veeConsumptions"></param>
        /// <param name="veeConfigurations"></param>
        /// <returns>True; if data is valid after Spike validation check</returns>
        public bool Validate(List<VeeConsumptionByMeterDto> veeConsumptions, VeeConfigurationsDto veeConfigurations)
        {
            var isValid = true;
            if (veeConfigurations.SpikeCheck.Required)
            {
                var sortedVeeConsumptions = veeConsumptions.OrderBy(v => v.StandardUOMConsumption).ToList();
                var firstQuartile = FindQuartile(sortedVeeConsumptions, 1);
                var thirdQuartile = FindQuartile(sortedVeeConsumptions, 3);
                var interQuartile = thirdQuartile - firstQuartile;
                var calculatedSpikeThreshold = (SpikeCalculationConstant * interQuartile) + thirdQuartile;

                var count = veeConsumptions.Count(consumption => consumption.StandardUOMConsumption > calculatedSpikeThreshold);
                if (count >= veeConfigurations.SpikeCheck.IntervalThreshold)
                {
                    veeConsumptions.Where(tallAmi => tallAmi.StandardUOMConsumption > calculatedSpikeThreshold).ToList().ForEach(tallAmi => tallAmi.IsSpike = true);
                    isValid = false;
                }
            }
            return isValid;
        }

        /// <summary>
        /// This method is used to find the First and Third Quartile of a list.
        /// The formula used to find index of first Quartile and third Quartile is (n+1)*0.25 and (n+1)*0.75, where n is the number of elements in list.
        /// </summary>
        /// <param name="sortedVeeConsumptions"></param>
        /// <param name="quartileId">If quartileId comes as 1, we will find First Quartile. If quartileId comes as 3, we will find Third Quartile.</param>
        /// <returns></returns>
        private double FindQuartile(List<VeeConsumptionByMeterDto> sortedVeeConsumptions, int quartileId)
        {
            double quartile = 0;
            int count = sortedVeeConsumptions.Count;
            int midIndex = (int)((count + 1) * 0.25);
            if (quartileId == 3)
            {
                midIndex = (int)((count + 1) * 0.75);
            }
            var firstMidConsumption = sortedVeeConsumptions[midIndex - 1].StandardUOMConsumption;
            var secondMidConsumption = sortedVeeConsumptions[midIndex].StandardUOMConsumption;
            if (firstMidConsumption != null && secondMidConsumption != null)
            {
                quartile = (double)((firstMidConsumption + secondMidConsumption) / 2);
            }
            return quartile;
        }
    }
}
