﻿using System.Collections.Generic;
using System.Linq;
using AO.VEE.Business.Contracts;
using AO.VEE.DTO;

namespace AO.VEE.Business
{
    /// <summary>
	/// MissingValueValidator class is used for missing value check validation
	/// </summary>
    public class MissingValueValidator : IVeeValidator
    {
        /// <summary>
		/// To set the sequence id for missing value check validation
		/// </summary>
		public MissingValueValidator(int sequenceId)
        {
            SequenceId = sequenceId;
        }

        public int SequenceId { get; set; }

        /// <summary>
		/// To validate list of VeeConsumptionByMeterDto against missing value check
		/// </summary>
		/// <param name="veeConsumptions">List of VeeConsumptionByMeterDto to be validated</param>
		/// <param name="veeConfigurations">Configuration required for missing value check validation</param>
		/// <returns>True if data is valid after missing validation check</returns>
		public bool Validate(List<VeeConsumptionByMeterDto> veeConsumptions, VeeConfigurationsDto veeConfigurations)
        {
            var isValid = true;
            // ReSharper disable once InvertIf
            if (veeConfigurations.MissingIntervalCheck.Required && veeConsumptions.Count(tallAmi => tallAmi.StandardUOMConsumption == null) >= veeConfigurations.MissingIntervalCheck.IntervalThreshold)
            {
                veeConsumptions.Where(veeValidationDto => veeValidationDto.StandardUOMConsumption == null)
                    .ToList()
                    .ForEach(c => c.IsMissing = true);
                isValid = false;
            }
            return isValid;
        }
    }
}