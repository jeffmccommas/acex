﻿namespace AO.VEE.Business
{
    static class StaticConfig
    {
        public const string PageSize = "PageSize";
        public const string VeeEventHubSendConnectionString = "VeeEventHubSendConnectionString";
        public const string VeeEventHubPath = "VeeEventHubPath";
    }
}
