﻿using System;
using System.Collections.Generic;
using System.Text;
using AO.VEE.DTO;
using Microsoft.ServiceBus.Messaging;
using Newtonsoft.Json;
using CE.AO.Models;
using AO.BusinessContracts;
using System.Configuration;
using System.Linq;
using AutoMapper;
using Microsoft.Azure;

namespace AO.VEE.Business
{
    /// <summary>
    /// This class contains method which is used for triggering the VEE process.
    /// </summary>
    public class ValidationEstimationEventHubTrigger : IProcessTrigger<VeeValidationEstimationTriggerDto>
    {
        public LogModel LogModel { get; set; }

        public IMeterHistoryByMeter MeterHistoryByMeter { get; set; }

        public ValidationEstimationEventHubTrigger(LogModel logModel, IMeterHistoryByMeter meterHistoryByMeter)
        {
            LogModel = logModel;
            MeterHistoryByMeter = meterHistoryByMeter;
        }

        /// <summary>
        /// This method Triggers the VEE process. It internally calls the GetMeterHistoryByMeterList method of AO.Business to fetch the Meters based on Client Id.
        /// </summary>
        /// <param name="triggerDto"></param>
        public void Trigger(VeeValidationEstimationTriggerDto triggerDto)
        {
            byte[] token = null;
            var client = GetEventHubClient();
            try
            {
                do
                {
                    List<MeterHistoryByMeterModel> meterHistoryByMeterList =
                        MeterHistoryByMeter.GetMeterHistoryByMeterList(
                            Convert.ToInt32(ConfigurationManager.AppSettings[StaticConfig.PageSize]), ref token,
                            triggerDto.ClientId)?.ToList();

                    if (meterHistoryByMeterList != null && meterHistoryByMeterList.Any())
                    {
                        List<VeeValidationEstimationTriggerDto> veeValidationEstimationTriggers =
                            Mapper.Map<List<MeterHistoryByMeterModel>, List<VeeValidationEstimationTriggerDto>>(
                                meterHistoryByMeterList);
                        //Send data to event hub only if meters are active
                        List<VeeValidationEstimationTriggerDto> veeValidationEstimationRequestDtos =
                            veeValidationEstimationTriggers.Where(v => v.IsActive).ToList();
                        if (veeValidationEstimationRequestDtos.Any())
                        {
                            var eventsList = new List<EventData>();
                            foreach (var veeValidationEstimationRequestDto in veeValidationEstimationRequestDtos)
                            {
                                if (triggerDto.AmiDate != null)
                                    veeValidationEstimationRequestDto.AmiDate = triggerDto.AmiDate;
                                //single dto(model) will be single event
                                var serializedEventData = JsonConvert.SerializeObject(veeValidationEstimationRequestDto);
                                var data = new EventData(Encoding.UTF8.GetBytes(serializedEventData));
                                eventsList.Add(data);
                            }
                            CE.AO.Utilities.EventHubSender.SendEventsInBatch(client, eventsList);
                        }
                    }
                } while (token != null);
            }
            finally
            {
                CloseEventHubClient(client);
            }
        }

        /// <summary>
        /// This method will make connection with VEE Event Hub
        /// </summary>
        /// <returns></returns>
        private static EventHubClient GetEventHubClient()
        {
            var connectionString = CloudConfigurationManager.GetSetting(StaticConfig.VeeEventHubSendConnectionString);
            var eventHubPath = CloudConfigurationManager.GetSetting(StaticConfig.VeeEventHubPath);
            var messagingFactory = MessagingFactory.CreateFromConnectionString(connectionString);
            messagingFactory.GetSettings().OperationTimeout = TimeSpan.FromHours(1);
            var client = messagingFactory.CreateEventHubClient(eventHubPath);
            return client;
        }

        /// <summary>
        /// This method will close connection with VEE Event Hub
        /// </summary>
        /// <param name="client"></param>
        private static void CloseEventHubClient(EventHubClient client)
        {
            client.Close();
        }
    }
}