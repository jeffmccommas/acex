﻿using System;
using AO.Entities;
using AO.VEE.Domain;
using AO.VEE.DTO;
using AutoMapper;
using CE.AO.Models;

namespace AO.VEE.Business
{
	/// <summary>
	/// AutoMapper configuration are configured in this class for VEE
	/// </summary>
	public static class AutoMapperConfig
	{
		/// <summary>
		/// AutoMapper configuration are configured in this method
		/// </summary>
		public static void AutoMapperMappings()
		{
			Mapper.CreateMap<string, bool?>()
				.ConvertUsing(str => !string.IsNullOrWhiteSpace(str) ? Convert.ToBoolean(str.Trim()) : (bool?) null);

			Mapper.CreateMap<string, DateTime?>()
				.ConvertUsing(str => !string.IsNullOrWhiteSpace(str) ? Convert.ToDateTime(str.Trim()) : (DateTime?) null);

			Mapper.CreateMap<string, double?>()
				.ConvertUsing(str => !string.IsNullOrWhiteSpace(str) ? Convert.ToDouble(str.Trim()) : (double?) null);

			Mapper.CreateMap<string, Guid?>()
				.ConvertUsing(str => !string.IsNullOrWhiteSpace(str) ? Guid.Parse(str.Trim()) : (Guid?) null);

			Mapper.CreateMap<string, int?>()
				.ConvertUsing(str => !string.IsNullOrWhiteSpace(str) ? Convert.ToInt32(str.Trim()) : (int?) null);

			Mapper.CreateMap<string, string>()
				.ConvertUsing(str => !string.IsNullOrWhiteSpace(str) ? str.Trim() : str);

			#region VeeStagedClientMeterByDate

			Mapper.CreateMap<VeeStagedClientMeterByDateDto, VeeStagedClientMeterByDateDomain>()
				.ForAllUnmappedMembers(o => o.Ignore());

			Mapper.CreateMap<VeeStagedClientMeterByDateDomain, VeeStagedClientMeterByDateDto>()
				.ForAllUnmappedMembers(o => o.Ignore());

			Mapper.CreateMap<VeeValidationEstimationTriggerDto, VeeStagedClientMeterByDateDto>()
				.ForAllUnmappedMembers(o => o.Ignore());

			Mapper.CreateMap<MeterReadModel, VeeStagedClientMeterByDateDto>()
				.ForMember(m => m.AmiDate, m => m.MapFrom(src => src.AmiTimeStamp.Date))
				.ForAllUnmappedMembers(o => o.Ignore());

			#endregion

			#region VeeConsumptionByMeter

			Mapper.CreateMap<VeeConsumptionByMeterDto, VeeConsumptionByMeterDomain>()
				.ForAllUnmappedMembers(o => o.Ignore());

			Mapper.CreateMap<VeeConsumptionByMeterDomain, VeeConsumptionByMeterDto>()
				.ForAllUnmappedMembers(o => o.Ignore());

			Mapper.CreateMap<TallAmiModel, VeeConsumptionByMeterDto>()
				.ForAllUnmappedMembers(o => o.Ignore());

            Mapper.CreateMap<VeeConsumptionByMeterDto, VeeConsumptionByMeterDto>()
                .ForAllUnmappedMembers(o => o.Ignore());
			#endregion

			#region VeeValidationEstimationTriggerDto

			Mapper.CreateMap<MeterHistoryByMeterModel, VeeValidationEstimationTriggerDto>()
				.ForMember(dest => dest.IsActive, opts => opts.MapFrom(src => src.Active))
				.ForMember(dest => dest.MeterId, opts => opts.MapFrom(src => src.MeterSerialNumber))
				.ForAllUnmappedMembers(o => o.Ignore());

			#endregion

			#region MeterHistoryByMeter

			Mapper.CreateMap<MeterHistoryByMeterModel, MeterHistoryByMeterEntity>()
				.ForAllUnmappedMembers(o => o.Ignore());

			Mapper.CreateMap<MeterHistoryByMeterEntity, MeterHistoryByMeterModel>()
				.ForAllUnmappedMembers(o => o.Ignore());

			#endregion

			Mapper.AssertConfigurationIsValid();
		}

		/// <summary>
		/// Extension Method for configuring unmapped members
		/// </summary>
		/// <typeparam name="TSource">Type to map from</typeparam>
		/// <typeparam name="TDestination">Type to map to</typeparam>
		/// <param name="mapping">Mapping Expression</param>
		/// <param name="memberOptions">MemberConfigurationExpression</param>
		public static void ForAllUnmappedMembers<TSource, TDestination>(this IMappingExpression<TSource, TDestination> mapping, Action<IMemberConfigurationExpression<TSource>> memberOptions)
		{
			var typeMap = Mapper.FindTypeMapFor<TSource, TDestination>();
			foreach (var memberName in typeMap.GetUnmappedPropertyNames())
				mapping.ForMember(memberName, memberOptions);
		}
	}
}
