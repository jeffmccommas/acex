﻿using System.Collections.Generic;
using System.Linq;
using AO.VEE.Business.Contracts;
using AO.VEE.DTO;

namespace AO.VEE.Business
{
    /// <summary>
	/// StaticValueValidator class is used for static value check validation
	/// </summary>
    public class StaticValueValidator : IVeeValidator
    {
        /// <summary>
		/// To set the sequence id for static value check validation
		/// </summary>
		public StaticValueValidator(int sequenceId)
        {
            SequenceId = sequenceId;
        }

        public int SequenceId { get; set; }


        /// <summary>
		/// To validate list of VeeConsumptionByMeterDto against static value check
		/// </summary>
		/// <param name="veeConsumptionByMeterList">List of VeeConsumptionByMeterDto to be validated</param>
		/// <param name="veeConfigurations">Configuration required for static value check validation</param>
		/// <returns>True; if data is valid after Static validation check</returns>
		public bool Validate(List<VeeConsumptionByMeterDto> veeConsumptionByMeterList, VeeConfigurationsDto veeConfigurations)
        {
            var isValid = true;
            var count = 1;
            if (veeConfigurations.StaticCheck.Required)
            {
                for (var i = 0; i < veeConsumptionByMeterList.Count - 1; i++)
                {

                    if (veeConsumptionByMeterList[i].StandardUOMConsumption <= (veeConsumptionByMeterList[i + 1].StandardUOMConsumption+veeConfigurations.StaticCheck.ConsumptionThreshold) && veeConsumptionByMeterList[i].StandardUOMConsumption >= (veeConsumptionByMeterList[i + 1].StandardUOMConsumption - veeConfigurations.StaticCheck.ConsumptionThreshold))
                    {
                        count++;
                        if (count == veeConfigurations.StaticCheck.IntervalThreshold)
                        {
                            for (var j = count - 2; j >= 0; j--)
                            {
                                veeConsumptionByMeterList[i - j].IsStatic = true;
                            }
                            veeConsumptionByMeterList[i + 1].IsStatic = true;
                            isValid = false;
                        }
                        else if (count > veeConfigurations.StaticCheck.IntervalThreshold)
                            veeConsumptionByMeterList[i + 1].IsStatic = true;
                    }
                    else
                    {
                        count = 1;
                    }
                }
            }
            return isValid;
        }
    }
}
