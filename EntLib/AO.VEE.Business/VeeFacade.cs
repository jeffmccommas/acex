﻿using System;
using System.Collections.Generic;
using System.Linq;
using AO.BusinessContracts;
using AO.VEE.Business.Contracts;
using AO.VEE.DataAccess.Contracts;
using AO.VEE.DTO;
using AutoMapper;
using CE.AO.Logging;
using CE.AO.Models;
using Enums = CE.AO.Utilities.Enums;

namespace AO.VEE.Business
{
    /// <summary>
    /// To do validation & estimation on the data
    /// </summary>
    public class VeeFacade : IVeeFacade
    {
        private readonly Lazy<IVeeStagedClientMeterByDateRepository> _veeStagedClientMeterByDateRepository;
        private readonly Lazy<IClientConfigFacade> _clientConfigFacade;
        private Lazy<IVeeConsumptionByMeterRepository> VeeConsumptionByMeterRepository { get; }
        private Lazy<ITallAMI> TallAmi { get; }
        private Lazy<IList<IVeeValidator>> Validators { get; }
        private Lazy<IVeeEstimatorFacade> VeeEstimatorFacade { get; }

        public VeeFacade(Lazy<IVeeStagedClientMeterByDateRepository> veeStagedClientMeterByDateRepository, Lazy<IList<IVeeValidator>> validators, Lazy<IClientConfigFacade> clientConfigFacade, Lazy<ITallAMI> tallAmi, Lazy<IVeeEstimatorFacade> veeEstimatorFacade, Lazy<IVeeConsumptionByMeterRepository> veeConsumptionByMeterRepository, LogModel logModel)
        {
            _veeStagedClientMeterByDateRepository = veeStagedClientMeterByDateRepository;
            _clientConfigFacade = clientConfigFacade;
            TallAmi = tallAmi;
            Validators = validators;
            VeeEstimatorFacade = veeEstimatorFacade;
            VeeConsumptionByMeterRepository = veeConsumptionByMeterRepository;
            LogModel = logModel;

        }

        /// <summary>
        /// For logging info,warning,error.
        /// </summary>
        public LogModel LogModel { get; set; }

        /// <summary>
        /// To do validation and estimation process
        /// </summary>
        /// <param name="veeValidationEstimationTrigger">VeeClientMeterDto to fetch consumption details</param>
        public void ExecuteValidationsAndEstimations(VeeValidationEstimationTriggerDto veeValidationEstimationTrigger)
        {
            LogModel.Module = Enums.Module.ValidationEstimationProcess;
            //Get VEE settings
            var veeConfigurations = _clientConfigFacade.Value.GetClientVeeSettings(veeValidationEstimationTrigger.ClientId);
            if (veeConfigurations == null)
            {
                Logger.Warn("No vee settings found for client.", LogModel);
                return;
            }
            IList<VeeStagedClientMeterByDateDto> veeStagedDataList;
            IEnumerable<DateTime> veeRunDates = GetDatesToRunVee(veeValidationEstimationTrigger, out veeStagedDataList);

            foreach (var veeRunDate in veeRunDates)
            {
                //Get Consumptions to process VEE
                List<VeeConsumptionByMeterDto> consumptions = GetConsumptionForVee(veeRunDate, veeValidationEstimationTrigger);

                Validators.Value.OrderBy(v => v.SequenceId).ToList().ForEach(v =>
                {
                    if (!v.Validate(consumptions, veeConfigurations))
                    {                       
                        VeeEstimatorFacade.Value.Estimate(consumptions, v.GetType());
                    }
                });

                // insert VEE consumption in VEE_Consumption_by_meter
                VeeConsumptionByMeterRepository.Value.InsertOrMergeBatchVeeConsumptionByMeterAsync(consumptions);              

                //Delete data from VeeStagedClientMeterByDate table
                var veeStagedData = veeStagedDataList.FirstOrDefault(v => v.AmiDate == veeRunDate);
                _veeStagedClientMeterByDateRepository.Value.DeleteVeeStagedData(veeStagedData);               
            }

        }

        /// <summary>
        /// To Get the Consumptions to process VEE
        /// </summary>
        /// <param name="veeRunDate">Date to get consumptions for</param>
        /// <param name="veeValidationEstimationTrigger">VEE Request object</param>
        /// <returns>List of Consumptions</returns>
        private List<VeeConsumptionByMeterDto> GetConsumptionForVee(DateTime veeRunDate, VeeValidationEstimationTriggerDto veeValidationEstimationTrigger)
        {
            //get the data from Consumption by meter
            var startDate = new DateTime(veeRunDate.Year, veeRunDate.Month, veeRunDate.Day, 00, 00, 00);
            var endDate = new DateTime(veeRunDate.Year, veeRunDate.Month, veeRunDate.Day, 23, 59, 59);
            List<TallAmiModel> rawAmiConsumptions = TallAmi.Value.GetAmiList(startDate, endDate, veeValidationEstimationTrigger.MeterId, veeValidationEstimationTrigger.ClientId)?.ToList();
            List<VeeConsumptionByMeterDto> veeConsumptions = Mapper.Map<List<TallAmiModel>, List<VeeConsumptionByMeterDto>>(rawAmiConsumptions);

            var firstElement = veeConsumptions.First();
            var nextDate = firstElement.AmiTimeStamp.Date.AddDays(1);
            var today = firstElement.AmiTimeStamp.Date;
            var intervalList = new List<DateTime>();
            var intervalType = firstElement.IntervalType;
            while (today < nextDate)
            {
                intervalList.Add(today);
                today = today.AddMinutes(intervalType);
            }
            List<DateTime> consumptionTimeList = veeConsumptions.Select(e => e.AmiTimeStamp).ToList();
            IEnumerable<DateTime> missingIntervalList = intervalList.Where(i => !consumptionTimeList.Contains(i));

            List<VeeConsumptionByMeterDto> consumptions = veeConsumptions;
            foreach (var missingInterval in missingIntervalList)
            {
                var veeConsumptionByMeterDto = Mapper.Map<VeeConsumptionByMeterDto, VeeConsumptionByMeterDto>(firstElement);
                veeConsumptionByMeterDto.Consumption = null;
                veeConsumptionByMeterDto.StandardUOMConsumption = null;
                veeConsumptionByMeterDto.AmiTimeStamp = missingInterval;

                consumptions.Add(veeConsumptionByMeterDto);
            }
            consumptions = consumptions.OrderBy(i => i.AmiTimeStamp).ToList();
            return consumptions;
        }

        /// <summary>
        /// To get the dates for which we want to run VEE process
        /// </summary>
        /// <param name="veeValidationEstimationTrigger">VeeClientMeterDto to fetch consumption & date details</param>
        /// <param name="veeStagedDataList"></param>
        /// <returns>List of dates</returns>
        private IEnumerable<DateTime> GetDatesToRunVee(VeeValidationEstimationTriggerDto veeValidationEstimationTrigger,
            out IList<VeeStagedClientMeterByDateDto> veeStagedDataList)
        {
            veeStagedDataList = new List<VeeStagedClientMeterByDateDto>();
            var veeRunDates = new List<DateTime>();
            //If AmiDate is null then its a Regular scheduled VEE run, and we need to run VEE for all the dates from staging + for all the dates from last VEE run
            if (veeValidationEstimationTrigger.AmiDate == null)
            {
                Logger.Info("Vee will run for all the dates from staging and also for all dates after last vee run.", LogModel);
                //Get data from Staging table
                veeStagedDataList =
                    _veeStagedClientMeterByDateRepository.Value.GetVeeStagedDataList(
                        veeValidationEstimationTrigger.ClientId,
                        veeValidationEstimationTrigger.MeterId);
                //Get last consumption from VEE_Consumption_by_meter to track when last VEE Ran for the meter
                var veeConsumptionByMeter =
                    VeeConsumptionByMeterRepository.Value.GetLastVeeConsumptionByMeter(
                        veeValidationEstimationTrigger.ClientId,
                        veeValidationEstimationTrigger.MeterId);

                // Make sure all dates from Last VEE run are covered with dates in staging table
                veeStagedDataList?.ToList().ForEach(d => veeRunDates.Add(d.AmiDate));
                var date = veeConsumptionByMeter.AmiTimeStamp.Date.AddDays(1);
                while (date < DateTime.UtcNow)
                {
                    veeRunDates.Add(date);
                    date = date.AddDays(1);
                }
            }
            else
            {
                Logger.Info("vee scheduled for specific day", LogModel);
                veeRunDates.Add((DateTime)veeValidationEstimationTrigger.AmiDate); // For Specific Day VEE run
            }
            return

            veeRunDates;
        }

        /// <summary>
        /// To insert or update Vee staged data to database
        /// </summary>
        /// <param name="veeStagedClientMeterByDateDto">model to be inserted</param>
        public void InsertOrMergeVeeStagedDataAsync(VeeStagedClientMeterByDateDto veeStagedClientMeterByDateDto)
        {
            var clientSettings = _clientConfigFacade.Value.GetClientSettings(veeStagedClientMeterByDateDto.ClientId);
            if (clientSettings.IsVeeSubscribed)
            {
                _veeStagedClientMeterByDateRepository.Value.InsertOrMergeVeeStagedDataAsync(veeStagedClientMeterByDateDto);
            }
        }
    }
}
