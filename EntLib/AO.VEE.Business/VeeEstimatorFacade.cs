﻿using System;
using System.Collections.Generic;
using AO.VEE.Business.Contracts;
using AO.VEE.DTO;

namespace AO.VEE.Business
{
	/// <summary>
	/// VeeEstimatorFacade class used to do estimation for invalid data
	/// </summary>
	public class VeeEstimatorFacade : IVeeEstimatorFacade
	{
		//Note: Need to implement
		private Lazy<IList<IVeeEstimator>> Estimators { get; }

		public VeeEstimatorFacade(Lazy<IList<IVeeEstimator>> estimators)
		{
			Estimators = estimators;
		}

		/// <summary>
		/// To do estimation for invalid data
		/// </summary>
		/// <param name="veeValidationDtoList">veeValidationDtoList to be estimated</param>
		/// <param name="validator">validator type</param>
		public void Estimate(List<VeeConsumptionByMeterDto> veeValidationDtoList, Type validator)
		{
			switch (validator.Name)
			{
				case "NegativeValueValidator":
				{
					// depending upon the criteria call Linear or Time substute Estimators
					break;
				}
			}
		}
	}
}