﻿using System.Collections.Generic;
using System.Linq;
using AO.VEE.Business.Contracts;
using AO.VEE.DTO;

namespace AO.VEE.Business
{
    /// <summary>
	/// ZeroValueValidator class is used for zero value check validation
	/// </summary>
	public class ZeroValueValidator : IVeeValidator
    {
        /// <summary>
		/// To set the sequence id for zero value check validation
		/// </summary>
		public ZeroValueValidator(int sequenceId)
        {
            SequenceId = sequenceId;
        }

        public int SequenceId { get; set; }

        /// <summary>
		/// To validate list of VeeConsumptionByMeterDto against zero value check
		/// </summary>
		/// <param name="veeConsumptions">List of VeeConsumptionByMeterDto to be validated</param>
		/// <param name="veeConfigurations">Configuration required for zero value check validation</param>
		/// <returns>True if data is valid after zero validation check</returns>
		public bool Validate(List<VeeConsumptionByMeterDto> veeConsumptions, VeeConfigurationsDto veeConfigurations)
        {
            var isValid = true;
            // ReSharper disable once InvertIf
            if (veeConfigurations.ZeroCheck.Required && veeConsumptions.Count(tallAmi => tallAmi.StandardUOMConsumption >= 0 && tallAmi.StandardUOMConsumption <= veeConfigurations.ZeroCheck.ConsumptionThreshold) >= veeConfigurations.ZeroCheck.IntervalThreshold)
            {
                veeConsumptions.Where(veeValidationDto => veeValidationDto.StandardUOMConsumption >= 0 && veeValidationDto.StandardUOMConsumption <= veeConfigurations.ZeroCheck.ConsumptionThreshold)
                    .ToList()
                    .ForEach(c => c.IsZero = true);
                isValid = false;
            }
            return isValid;
        }
    }
}