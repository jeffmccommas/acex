﻿using System.Collections.Generic;
using System.Linq;
using AO.VEE.Business.Contracts;
using AO.VEE.DTO;

namespace AO.VEE.Business
{
	/// <summary>
	/// NegativeValueValidator class is used for negative value check validation
	/// </summary>
	public class NegativeValueValidator : IVeeValidator
	{
		/// <summary>
		/// To set the sequence id for negative value check validation
		/// </summary>
		public NegativeValueValidator(int sequenceId)
		{
			SequenceId = sequenceId;
		}

		public int SequenceId { get; set; }

        /// <summary>
        /// To validate list of VeeConsumptionByMeterDto against negative value check
        /// </summary>
        /// <param name="veeConsumptions">List of VeeConsumptionByMeterDto to be validated</param>
        /// <param name="veeConfigurations">Configuration required for negative value check validation</param>
        /// <returns>True; if data is valid after Negative validation check</returns>
        public bool Validate(List<VeeConsumptionByMeterDto> veeConsumptions, VeeConfigurationsDto veeConfigurations)
		{
			var isValid = true;
            // ReSharper disable once InvertIf
			if (veeConfigurations.NegativeValueCheck.Required && veeConsumptions.Count(c => c.StandardUOMConsumption <= veeConfigurations.NegativeValueCheck.ConsumptionThreshold) >= veeConfigurations.NegativeValueCheck.IntervalThreshold)
			{
					veeConsumptions.Where(veeValidationDto => veeValidationDto.StandardUOMConsumption <= veeConfigurations.NegativeValueCheck.ConsumptionThreshold)
						.ToList()
						.ForEach(c => c.IsNegative = true);
                    isValid = false;
			}
			return isValid;
		}
	}
}