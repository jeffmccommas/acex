﻿using System;
using System.Text;
using System.Security.Cryptography;

namespace CE.Portals.Integrations.Security
{
    public class Encryptor
    {
        public readonly String UnEncryptedText;

        /// <summary>
        /// Instantiate an encryptor that uses the specified key password.
        /// </summary>
        /// <param name="strPlainText">The text on which to derive the hashed value</param>
        public Encryptor(string strPlainText)
        {
            UnEncryptedText = strPlainText;
        }
        /// <summary>
        /// Generates a salt for login using System.Guid.NewGuid().ToString() 
        /// that is hashed together with the supplied control panel login user password
        /// When a user record is created store that salt value in a column in the table.
        /// Note that if password is changed, the salt should be regenerated and updated on the table.
        /// </summary>
        public string GenerateSalt()
        {
            string salt = Guid.NewGuid().ToString();
            return salt;
        }

        /// <summary>
        /// Computes a salted hash of the password and salt provided and returns as a base64 encoded string.
        /// </summary>
        /// <param name="password">The password to hash.</param>
        /// <param name="salt">The salt to use in the hash.</param>
        public string GenerateHashWithSalt(string password, string salt)
        {
            // merge password and salt together
            string sHashWithSalt = salt + password;
            // convert this merged value to a byte array
            byte[] saltedHashBytes = Encoding.UTF8.GetBytes(sHashWithSalt);
            // convert merged bytes to a hash as byte array
            byte[] hash = SHA256.Create().ComputeHash(saltedHashBytes);
            // return as a base 64 encoded string
            return Convert.ToBase64String(hash);
        }

        public bool VerifyHash(String inputText, String salt, String storedHash)
        {
            string strEncrypt = GenerateHashWithSalt(inputText, salt);
            return (strEncrypt == storedHash);
        }

    }
}
