﻿using System.Threading.Tasks;
using CE.Portals.Integrations.Models;
using System.Net.Http;
using System.Net;
using System.Text;
using System.Linq;
using System;
using CE.Portals.Integrations.WebServiceClient;
using Newtonsoft.Json;
using System.Security.Cryptography;
using System.IO;

namespace CE.Portals.Integrations
{
    public class APIResponse
    {
        public const string DateTimeHeader = "X-DateTime";
        public const string UsernameHeader = "X-CE-AccessKeyId";
        public const string MessageIdHeader = "X-CE-MessageId";
        public const string ChannelHeader = "X-CE-Channel";
        public const string LocaleHeader = "X-CE-Locale";
        public const string MetaHeader = "X-CE-Meta";  ////on end also include "#cecp:1#" to simulate control panel caller so record would be excluded in reported results
        public const string ContentType = "Content-Type";
        public HttpResponseMessage GetAPIResponse(QueryTool query, HttpMessageHandler httpmessagehandler = null)
        {
            var client = new RestClient(query.BaseUrl, httpmessagehandler, null, query.Content);
            var user = query.selectedCEAccessKeyID;
            HttpMethod httpMethod;
            var queryString = query.Parameters;
            var encQueryString = string.Empty;




            switch (query.SelectedMethodApi.ToUpper())
            {
                case "GET":
                    httpMethod = HttpMethod.Get;
                    encQueryString = WebUtility.UrlEncode(queryString);
                    break;
                case "POST":
                    httpMethod = HttpMethod.Post;
                    encQueryString = queryString;
                    break;
                case "PUT":
                    httpMethod = HttpMethod.Put;
                    encQueryString = queryString;
                    break;
                default:
                    httpMethod = HttpMethod.Get;
                    break;
            }
            client._body = query.Content;
            client.ContentYype = query.ContentType;
            if (query.incEnc)
            {
                client._params.Add("enc", encQueryString);
            }
            else
            {
                if (!string.IsNullOrEmpty(queryString))
                {
                    //get after the ? on the first character
                    var querystringParsed = queryString.Substring(queryString.IndexOf('?') + 1);
                    //split into key values
                    {
                        var p = querystringParsed.Split('&');
                        foreach (var temp in from s in p where s.IndexOf('=') > -1 select s.Split('='))
                        {
                            client._params.Add(temp[0], temp[1]);
                        }
                    }
                }
                //if (query.selectedEndpoint.Contains("{0}") && client._params.Count == 1)
                //{
                //    query.selectedEndpoint = String.Format(query.selectedEndpoint, client._params.FirstOrDefault().Value);
                //}
                if (query.selectedEndpoint.Contains("{0}") && client._params.Count > 0)
                {
                    query.selectedEndpoint = String.Format(query.selectedEndpoint, client._params.FirstOrDefault().Value);
                }
            }
            if (!query.AllowAnonymousAccess)
            {
                var basicKey = query.BasicKey;

                var authHeader = $"Basic {basicKey}";
                //client._params.Add("Authorization", authHeader);
                client.AddHeader("Authorization", authHeader);
                client.AddHeader(UsernameHeader, query.selectedCEAccessKeyID);
                client.AddHeader(DateTimeHeader, new DateTime().ToString("MM/dd/yyyy HH:mm:ss"));
                client.AddHeader("MessageIdHeader", query.MessageIdHeader);
                client.AddHeader("ChannelHeader", query.ChannelHeader);
                client.AddHeader("LocaleHeader", query.LocaleHeader);
                client.AddHeader("MetaHeader", query.MetaHeader);
                if (query.Headers?.Count  > 0)
                    foreach (var keyValuePair in query.Headers)
                    {
                        client.AddHeader(keyValuePair.Key, keyValuePair.Value);
                    }
            }
            var r = client.Request<Task>(httpMethod, query.selectedEndpoint);
            if (r.Content != null)
            return new HttpResponseMessage() { StatusCode = r.StatusCode, Content = new StringContent(r.Content) };
            else
                return new HttpResponseMessage() { StatusCode = r.StatusCode};
        }

        private static string Base64Encode(string plainText)
        {
            var plainTextBytes = Encoding.UTF8.GetBytes(plainText);
            return Convert.ToBase64String(plainTextBytes);
        }

        public ResponseModel PostEvents(QueryTool query)
        {
            var key = Encoding.ASCII.GetBytes(query.BasicKey);
            query.ContentType = "application/json";
            var uri = "https:" + query.BaseUrl;
            var expireInSeconds = DateTime.UtcNow.ToUniversalTime().Subtract(new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc)).TotalSeconds;
            var utfBytes = Encoding.UTF8.GetBytes(WebUtility.UrlEncode(uri) + "\n" + expireInSeconds);
            var plainSignature = Encoding.UTF8.GetString(utfBytes);
            var token = "SharedAccessSignature sr=" + WebUtility.UrlEncode(uri) + "&sig=" + WebUtility.UrlEncode(Encode(plainSignature, key)) + "&se=" + expireInSeconds + "&skn=Send";
            //Client
            var client = new RestClient("https:" + query.BaseUrl + "/messages/?timeout=60", null, null);
            client.AddHeader("Content-Type", "application/json");
            client.AddHeader("Authorization", token);
            client._body = query.Parameters;
            client.ContentYype = query.ContentType;
            //var eventData = (Dictionary<string, string>)JsonConvert.DeserializeObject(query.Parameters, typeof(Dictionary<string, string>));
            client.Add(JsonConvert.SerializeObject(query.Parameters));

            var r = new ResponseModel();
            //TODO Validate Event Data before sending the request. 
            var response = client.Request<Task>(HttpMethod.Post, query.selectedEndpoint);
            r.Content = response.Content;
            r.StatusCode = response.StatusCode;
            r.StatusDescription = response.StatusDescription;

            return r;
        }
        private static string Encode(string input, byte[] key)
        {
            var myhmacsha256 = new HMACSHA256(key);
            var byteArray = Encoding.ASCII.GetBytes(input);
            var stream = new MemoryStream(byteArray);
            var hash = myhmacsha256.ComputeHash(stream);
            return Convert.ToBase64String(hash);
        }

        
    }
}
