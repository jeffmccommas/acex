﻿namespace CE.Portals.Integrations.Models
{
    public enum EnvironmentType : int
    {
        prod,
        uat,
        qa,
        dev,
        localdev
    }
}
