﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CE.Portals.WebApi.Core.Models
{
    public class ApplicationUser
    {
        public string UserName { get; set; }
        public string Password { get; set; }

        public string FirstName { get; set; }
        public string LastName { get; set; }
        public Boolean Administrator { get; set; }
        public Boolean Enabled { get; set; }
        public int UserId { get; set; }
    }
}
