﻿using System;

namespace CE.Portals.Integrations.Models
{
    public class QueryTool :QueryToolForm
    {
        public string UsernameHeader { get; set; }
        public string MessageIdHeader { get; set; }
        public string ChannelHeader { get; set; }
        public string LocaleHeader { get; set; }
        public string MetaHeader { get; set; }
        public DateTime DateTime { get; set; }
        public Boolean AllowAnonymousAccess { get; set; } = false;

        public string ContentType { get; set; } = "application/xml";
    }
}
