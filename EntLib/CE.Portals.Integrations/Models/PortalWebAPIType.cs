﻿namespace CE.Portals.Integrations.Models
{

    public enum PortalWebAPIType
        {
            InsightsApi,
            ESPMApi,
            PortalApi
        }
   
}
