﻿using System.Net;

namespace CE.Portals.Integrations.Models
{
    public class ResponseModel
    {
        public string Content { get; set; }
        public HttpStatusCode StatusCode { get; set; }
        public string StatusDescription { get; set; }
        public string FileName { get; set; }
        public byte[] RawBytes { get; set; }
    }
}

