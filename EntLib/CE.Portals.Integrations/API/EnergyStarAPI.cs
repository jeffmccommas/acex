﻿using CE.Portals.Integrations.Models;
using CE.Portals.Integrations.Common;
using System.Net.Http;

namespace CE.Portals.Integrations.API
{
    public class EnergyStarAPI
    {
        public const string POST = "POST";
        public const string GET = "GET";
        public const string DELETE = "DELETE";
        public const string PUT = "PUT";

        // public static Response GetAccountResponse(QueryTool query)
        // {
        //     query.SelectedMethodApi = GET;
        //     query.selectedEndpoint = "wstest/account";

        //     var t = new APIResponse();
        //     var r = t.GetAPIResponse(query);
        //     return r;
        // }

        // public static Response AccountResponse(QueryTool query)
        //{ 
        //     query.SelectedMethodApi = POST;
        //     query.selectedEndpoint = "wstest/account";

        //     var t = new APIResponse();
        //     var r = t.GetAPIResponse(query);
        //     return r;
        // }
        public static ResponseModel GetAccountAcceptedList(QueryTool query, HttpMessageHandler httpmessagehandler = null)
        {

            query.SelectedMethodApi = GET;
            query.selectedEndpoint = EnergyStar.ENERGYSTAR_GET_ACCEPTED_ACCOUNT_LIST_URL;

            var t = new APIResponse();
            var r = t.GetAPIResponse(query, httpmessagehandler);
            return new ResponseModel() { Content = r.Content.ReadAsStringAsync().Result, StatusCode = r.StatusCode };

        }
        public static ResponseModel GetAccountPendingList(QueryTool query, HttpMessageHandler httpmessagehandler = null)
        {

            query.SelectedMethodApi = GET;
            query.selectedEndpoint = EnergyStar.ENERGYSTAR_GET_ACCOUNT_LIST_URL;

            var t = new APIResponse();
            var r = t.GetAPIResponse(query, httpmessagehandler);
            return new ResponseModel() { Content = r.Content.ReadAsStringAsync().Result, StatusCode = r.StatusCode };

        }
        public static ResponseModel GetAcceptedAccountDetails(QueryTool query, HttpMessageHandler httpmessagehandler = null)
        {

            query.SelectedMethodApi = GET;
            query.selectedEndpoint = EnergyStar.ENERGYSTAR_GET_ACCEPTED_ACCOUNT_DETAILS_URL;

            var t = new APIResponse();
            var r = t.GetAPIResponse(query, httpmessagehandler);
            return new ResponseModel() { Content = r.Content.ReadAsStringAsync().Result, StatusCode = r.StatusCode };

        }

        public static ResponseModel AcceptMeters(QueryTool query, HttpMessageHandler httpmessagehandler = null)
        {

            query.SelectedMethodApi = POST;
            query.selectedEndpoint = EnergyStar.ENERGYSTAR_APPROVE_REJECT_METER;
            query.Content = "<sharingResponse><action>Accept</action><note>Your sharing request has been verified and accepted.</note></sharingResponse>";
            var t = new APIResponse();
            var r = t.GetAPIResponse(query, httpmessagehandler);
            return new ResponseModel() { Content = r.Content.ReadAsStringAsync().Result, StatusCode = r.StatusCode };

        }

        public static ResponseModel RejectMeters(QueryTool query, HttpMessageHandler httpmessagehandler = null)
        {

            query.SelectedMethodApi = POST;
            query.selectedEndpoint = EnergyStar.ENERGYSTAR_APPROVE_REJECT_METER;
            query.Content = "<sharingResponse><action>Reject</action><note>Your sharing request has been rejected.</note></sharingResponse>";
            var t = new APIResponse();
            var r = t.GetAPIResponse(query, httpmessagehandler);
            return new ResponseModel() { Content = r.Content.ReadAsStringAsync().Result, StatusCode = r.StatusCode };

        }

        public static ResponseModel AcceptAccounts(QueryTool query, HttpMessageHandler httpmessagehandler = null)
        {

            query.SelectedMethodApi = POST;
            query.selectedEndpoint = EnergyStar.ENERGYSTAR_APPROVE_REJECT_ACCOUNT;
            query.Content = "<sharingResponse><action>Accept</action><note>Your connection request has been verified and accepted.</note></sharingResponse>";
            var t = new APIResponse();
            var r = t.GetAPIResponse(query, httpmessagehandler);
            return new ResponseModel() { Content = r.Content.ReadAsStringAsync().Result, StatusCode = r.StatusCode };

        }
        public static ResponseModel AcceptedPropertyList(QueryTool query, HttpMessageHandler httpmessagehandler = null)
        {

            query.SelectedMethodApi = GET;
            query.selectedEndpoint = EnergyStar.ENERGYSTAR_GET_ACCEPTED_PROPERTY_LIST_URL;
            var t = new APIResponse();
            var r = t.GetAPIResponse(query, httpmessagehandler);
            return new ResponseModel() { Content = r.Content.ReadAsStringAsync().Result, StatusCode = r.StatusCode };

        }

        public static ResponseModel AcceptedMeterList(QueryTool query, HttpMessageHandler httpmessagehandler = null)
        {

            query.SelectedMethodApi = GET;
            query.selectedEndpoint = EnergyStar.ENERGYSTAR_GET_ACCEPTED_METER_LIST_URL;
            var t = new APIResponse();
            var r = t.GetAPIResponse(query, httpmessagehandler);
            return new ResponseModel() { Content = r.Content.ReadAsStringAsync().Result, StatusCode = r.StatusCode };

        }


        public static ResponseModel RejectAccounts(QueryTool query, HttpMessageHandler httpmessagehandler = null)
        {

            query.SelectedMethodApi = POST;
            query.selectedEndpoint = EnergyStar.ENERGYSTAR_APPROVE_REJECT_ACCOUNT;
            query.Content = "<sharingResponse><action>Reject</action><note>Unfortunately we cannot provide services for you at this time.</note ></sharingResponse>";
   
            var t = new APIResponse();
            var r = t.GetAPIResponse(query, httpmessagehandler);
            return new ResponseModel() { Content = r.Content.ReadAsStringAsync().Result, StatusCode = r.StatusCode };

        }
        public static ResponseModel GetMetersPendingList(QueryTool query, HttpMessageHandler httpmessagehandler = null)
        {

            query.SelectedMethodApi = GET;
            query.selectedEndpoint = EnergyStar.ENERGYSTAR_GET_PENDING_METERS_LIST_URL;

            var t = new APIResponse();
            var r = t.GetAPIResponse(query, httpmessagehandler);
            return new ResponseModel() { Content = r.Content.ReadAsStringAsync().Result, StatusCode = r.StatusCode };

        }

        public static ResponseModel GetMeterDetails(QueryTool query, HttpMessageHandler httpmessagehandler = null)
        {

            query.SelectedMethodApi = GET;
            query.selectedEndpoint = EnergyStar.ENERGYSTAR_GET_METER_DETAILS_URL;

            var t = new APIResponse();
            var r = t.GetAPIResponse(query, httpmessagehandler);
            return new ResponseModel() { Content = r.Content.ReadAsStringAsync().Result, StatusCode = r.StatusCode };

        }

        public static ResponseModel GetPropertyList(QueryTool query, HttpMessageHandler httpmessagehandler = null)
        {

            query.SelectedMethodApi = GET;
            query.selectedEndpoint = EnergyStar.ENERGYSTAR_GET_PROPERTY_LIST_URL;

            var t = new APIResponse();
            var r = t.GetAPIResponse(query, httpmessagehandler);
            return new ResponseModel() { Content = r.Content.ReadAsStringAsync().Result, StatusCode = r.StatusCode };

        }

        public static ResponseModel GetPropertyDetails(QueryTool query, HttpMessageHandler httpmessagehandler = null)
        {

            query.SelectedMethodApi = GET;
            query.selectedEndpoint = EnergyStar.ENERGYSTAR_GET_PROPERTY_DETAILS_URL;

            var t = new APIResponse();
            var r = t.GetAPIResponse(query, httpmessagehandler);
            return new ResponseModel() { Content = r.Content.ReadAsStringAsync().Result, StatusCode = r.StatusCode };

        }

        public static ResponseModel GetCustomFields(QueryTool query, HttpMessageHandler httpmessagehandler = null)
        {

            query.SelectedMethodApi = GET;
            query.selectedEndpoint = EnergyStar.ENERGYSTAR_GET_CUSTOMFIELDS_URL;

            var t = new APIResponse();
            var r = t.GetAPIResponse(query, httpmessagehandler);
            return new ResponseModel() { Content = r.Content.ReadAsStringAsync().Result, StatusCode = r.StatusCode };

        }
        public static ResponseModel GetMeterConsumptionData(QueryTool query, HttpMessageHandler httpmessagehandler = null)
        {

            query.SelectedMethodApi = GET;
            query.selectedEndpoint = EnergyStar.ENERGYSTAR_GET_METERENDDATE_URL;

            var t = new APIResponse();
            var r = t.GetAPIResponse(query, httpmessagehandler);
            return new ResponseModel() { Content = r.Content.ReadAsStringAsync().Result, StatusCode = r.StatusCode };

        }
        public static ResponseModel PostMeterConsumptionData(QueryTool query, HttpMessageHandler httpmessagehandler = null)
        {

            query.SelectedMethodApi = POST;
            query.selectedEndpoint = EnergyStar.ENERGYSTAR_POST_METERCONSUMPTIONDATA_URL;

            var t = new APIResponse();
            var r = t.GetAPIResponse(query, httpmessagehandler);
            return new ResponseModel() { Content = r.Content.ReadAsStringAsync().Result, StatusCode = r.StatusCode };

        }

        public static ResponseModel GetBaseLineAndTarget(QueryTool query, HttpMessageHandler httpmessagehandler = null)
        {

            query.SelectedMethodApi = GET;
            query.selectedEndpoint = EnergyStar.ENERGYSTAR_GET_BASELINEANDTARGET;
            var t = new APIResponse();
            var r = t.GetAPIResponse(query, httpmessagehandler);
            return new ResponseModel() { Content = r.Content.ReadAsStringAsync().Result, StatusCode = r.StatusCode };

        }

        public static ResponseModel GetPropertyMetrics(QueryTool query, HttpMessageHandler httpmessagehandler = null)
        {

            query.SelectedMethodApi = GET;
            query.selectedEndpoint = EnergyStar.ENERGYSTAR_GET_PROPERTYMETRICS;
            var t = new APIResponse();
            var r = t.GetAPIResponse(query, httpmessagehandler);
            return new ResponseModel() { Content = r.Content.ReadAsStringAsync().Result, StatusCode = r.StatusCode };

        }

        public static ResponseModel GetPropertyMetricsNoScoreReason(QueryTool query, HttpMessageHandler httpmessagehandler = null)
        {

            query.SelectedMethodApi = GET;
            query.selectedEndpoint = EnergyStar.ENERGYSTAR_GET_PROPERTYMETRICS_NOSCORE;
            var t = new APIResponse();
            var r = t.GetAPIResponse(query, httpmessagehandler);
            return new ResponseModel() { Content = r.Content.ReadAsStringAsync().Result, StatusCode = r.StatusCode };

        }

        //public static Response GetCustomer(QueryTool query)
        //{
        //    query.SelectedMethodApi = GET;
        //    query.selectedEndpoint = "wstest/customer";
        //    var t = new APIResponse();
        //    var r = t.GetAPIResponse(query);
        //    return r;
        //}

        //public static Response GetCustomerList(QueryTool query)
        //{
        //    query.SelectedMethodApi = GET;
        //    query.selectedEndpoint = "wstest/customer/list";
        //    var t = new APIResponse();
        //    var r = t.GetAPIResponse(query);
        //    return r;
        //}


        //public static Response GetNotificationList(QueryTool query)
        //{

        //    query.SelectedMethodApi = GET;
        //    query.selectedEndpoint = "wstest/notification/list";

        //    var t = new APIResponse();
        //    var r = t.GetAPIResponse(query);
        //    return r;
        //}

    }
}
