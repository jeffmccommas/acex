﻿using CE.Portals.Integrations.Models;

namespace CE.Portals.Integrations.API
{
    public class PortalAPI
    {
        public const string POST = "POST";
        public const string GET = "GET";
        public const string DELETE = "DELETE";
        public const string PUT = "PUT";

        public static ResponseModel GetUserAuth(QueryTool query)
        {
            query.SelectedMethodApi = POST;
            query.selectedEndpoint = "api/PortalsApi/UserAuth";

            var t = new APIResponse();
            var r = t.GetAPIResponse(query);
            if (r.StatusCode == System.Net.HttpStatusCode.OK)
                return new ResponseModel() { Content = r.Content.ReadAsStringAsync().Result, StatusCode = r.StatusCode };
            else
                return new ResponseModel() { StatusCode = r.StatusCode };
        }


        public static ResponseModel GetESPMSecretKeys(QueryTool query)
        {
            query.SelectedMethodApi = GET;
            query.selectedEndpoint = "/api/PortalsApi/SecretKey";

            var t = new APIResponse();
            var r = t.GetAPIResponse(query);
            return new ResponseModel() { Content = r.Content.ReadAsStringAsync().Result, StatusCode = r.StatusCode };
        }

        public static ResponseModel GetESPMAuthKeys(QueryTool query)
        {
            query.SelectedMethodApi = GET;
            query.selectedEndpoint = "/api/PortalsApi/ESPMAuthKey";
            var t = new APIResponse();
            var r = t.GetAPIResponse(query);
            return new ResponseModel() { Content = r.Content.ReadAsStringAsync().Result, StatusCode = r.StatusCode };
        }

        public static ResponseModel GetInsightsAuthHeaders(QueryTool query)
        {
            query.SelectedMethodApi = GET;
            query.selectedEndpoint = "/api/PortalsApi/InsightsAuthheader";
            var t = new APIResponse();
            var r = t.GetAPIResponse(query);
            return new ResponseModel() { Content = r.Content.ReadAsStringAsync().Result, StatusCode = r.StatusCode };
        }

        public static ResponseModel GetInsightsAuthHeaders2(QueryTool query)
        {
            query.SelectedMethodApi = GET;
            query.selectedEndpoint = "/api/PortalsApi/InsightsAuthheader";
            var t = new APIResponse();
            var r = t.GetAPIResponse(query);
            return new ResponseModel() { Content = r.Content.ReadAsStringAsync().Result, StatusCode = r.StatusCode };
        }

        public static ResponseModel GetAcceptedMeterList(QueryTool query)
        {
            query.SelectedMethodApi = GET;
            query.selectedEndpoint = "/api/PortalsApi/AcceptedMeters";
            var t = new APIResponse();
            var r = t.GetAPIResponse(query);
            return new ResponseModel() { Content = r.Content.ReadAsStringAsync().Result, StatusCode = r.StatusCode };
        }

        

        public static ResponseModel EventTrackingResponse(QueryTool query)
        {
            var t = new APIResponse();
            var r = t.PostEvents(query);
            return r;
        }
        
    }
}
