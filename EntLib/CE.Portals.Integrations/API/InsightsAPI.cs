﻿using CE.Portals.Integrations.Models;

namespace CE.Portals.Integrations.API
{
    public class InsightsAPI
    {
        public const string POST = "POST";
        public const string GET = "GET";
        public const string DELETE = "DELETE";
        public const string PUT = "PUT";

        public static ResponseModel GetContentResponse(QueryTool query)
        {
            query.SelectedMethodApi = GET;
            query.selectedEndpoint = "api/v1/content";
            query.AllowAnonymousAccess = false;
            var t = new APIResponse();
            var r = t.GetAPIResponse(query);
            return new ResponseModel() { Content = r.Content.ReadAsStringAsync().Result, StatusCode = r.StatusCode };
        }


        public static ResponseModel GetBillingdata(QueryTool query)
        {
            query.SelectedMethodApi = GET;
            query.selectedEndpoint = "api/v1/Bill";
            query.AllowAnonymousAccess = false;
            var t = new APIResponse();
            var r = t.GetAPIResponse(query);
            return new ResponseModel() { Content = r.Content.ReadAsStringAsync().Result, StatusCode = r.StatusCode };
        }
    }
}
