﻿using System;

namespace CE.Portals.Integrations.Common
{
    public class ApplicationConstants
    {
        public const string ESPMSecterKey = "ESPMSECRETKEY";
        public const string WEBTOKEN = "WebToken";
        public const string MAXINCORRECTLOGINS = "INCORRECTLOGINS";
    }

    public class EnergyStar
    {
        public static string ENERGYSTAR_GET_ACCOUNT_LIST_URL = "connect/account/pending/list";
        public static string ENERGYSTAR_GET_ACCEPTED_ACCOUNT_LIST_URL = "customer/list";
        public static string ENERGYSTAR_GET_ACCEPTED_ACCOUNT_DETAILS_URL = "customer/{0}";



        public static string ENERGYSTAR_APPROVE_REJECT_ACCOUNT = "connect/account/{0}";
        public static string ENERGYSTAR_APPROVE_REJECT_METER = "share/meter/{0}";

        public static string ENERGYSTAR_GET_PENDING_METERS_LIST_URL = "share/meter/pending/list";

        public static string ENERGYSTAR_GET_PROPERTY_LIST_URL = "account/{0}/property/list";
        public static string ENERGYSTAR_GET_ACCOUNT_DETAILS_URL = "account";

        public static string ENERGYSTAR_GET_METER_DETAILS_URL = "meter/{0}";

        public static string ENERGYSTAR_GET_ACCEPTED_PROPERTY_LIST_URL = "account/{0}/property/list";

        public static string ENERGYSTAR_GET_ACCEPTED_METER_LIST_URL = "property/{0}/meter/list?myAccessOnly=true";

        public static string ENERGYSTAR_GET_PROPERTY_DETAILS_URL = "property/{0}";

        public static string ENERGYSTAR_GET_CUSTOMFIELDS_URL = "meter/{0}/customFieldList";

        public static string ENERGYSTAR_GET_METERENDDATE_URL = "meter/{0}" + $"/consumptionData?startDate={DateTime.Now.AddMonths(-12):yyyy-MM-dd}&endDate={DateTime.Now:yyyy-MM-dd}";

        public static string ENERGYSTAR_POST_METERCONSUMPTIONDATA_URL = "meter/{0}/consumptionData";
        
        public static string ENERGYSTAR_GET_BASELINEANDTARGET = "property/{0}/baselineAndTarget";

        public static string ENERGYSTAR_GET_PROPERTYMETRICS = "property/{0}/metrics?";

        public static string ENERGYSTAR_GET_PROPERTYMETRICS_NOSCORE = "property/{0}/reasonsForNoScore?";


    }

 
}
