﻿using System.Collections.Generic;

namespace CE.Portals.Integrations.Common
{
    public static class QueryStringHelpers
    {
        public static string QueryStringBuilder(IDictionary<object, object> dict)
        {
            var list = new List<string>();
           foreach (var item in dict)
            {
                list.Add(item.Key + "=" + item.Value);
            }
            return string.Join("&", list);
        }

    }
}
