﻿using System;
using System.Net.Http;
using System.Threading.Tasks;
using CE.Portals.Integrations.Models;

namespace CE.Portals.Integrations.WebServiceClient
{
    public interface IRestClient
    {
        Uri BaseUri { get; set; }
        string RequestUrlPath { get; set; }

        void Add(string key, string value);
        void Add(string key, object value);
        void Add(object value);

        void AddHeader(string header, string value, bool allowMultipleValues = false);
        void AddBearerToken(string token);


        ResponseModel Request<T>(HttpMethod method, string keyOrRelativePath = null);
        Task<ResponseModel> RequestAsync<T>(HttpMethod method, string keyOrRelativePath = null);

        ResponseModel Get<T>(string keyOrRelativePath = null);
        Task<ResponseModel> GetAsync<T>(string keyOrRelativePath = null);
        ResponseModel Get(string keyOrRelativePath = null);
        Task<ResponseModel> GetAsync(string keyOrRelativePath = null);

        ResponseModel Post<T>(string keyOrRelativePath = null, T dataObject = default(T));
        Task<ResponseModel> PostAsync<T>(string keyOrRelativePath = null, T dataObject = default(T));
        ResponseModel Post(string keyOrRelativePath = null, string dataValue = null);
        Task<ResponseModel> PostAsync(string keyOrRelativePath = null, string dataValue = null);

        ResponseModel Delete<T>(string keyOrRelativePath = null);
        Task<ResponseModel> DeleteAsync<T>(string keyOrRelativePath = null);
        ResponseModel Delete(string keyOrRelativePath = null);
        Task<ResponseModel> DeleteAsync(string keyOrRelativePAth = null);
    }
}