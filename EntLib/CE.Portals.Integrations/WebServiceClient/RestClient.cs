﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using CE.Portals.Integrations.Models;
using CE.Portals.Integrations.Utils;

namespace CE.Portals.Integrations.WebServiceClient
{
    public partial class RestClient : IRestClient
    {
        internal Dictionary<string, string> _params;
        internal Dictionary<string, List<string>> _headers;
        internal object _objAsParam;
        public RestConfig Configuration { get; set; }

        public Uri BaseUri { get; set; }
        public string ConnectionString { get; set; }
        public string RequestUrlPath { get; set; }
        public HttpMessageHandler RestHttpMessageHandler { get; set; }
        public string _body;

        public string ContentYype { get; set; }

        private void Init(RestConfig config = null)
        {
            _params = new Dictionary<string, string>();
            _headers = new Dictionary<string, List<string>>();
            _body = String.Empty;
            this.Configuration = config ?? new RestConfig();
            //this.PrepareRestMode();
            this.CurrentMode = RestMode.HTTPClient;            
        }

        public RestClient(Uri baseUri = null, string urlPath = null, RestConfig config = null, HttpMessageHandler httpMessageHandler = null)
        {
            this.BaseUri = baseUri;
            this.RequestUrlPath = urlPath;
            if (httpMessageHandler!= null)
            this.RestHttpMessageHandler = httpMessageHandler;
            Init(config);
        }
        
        public RestClient(string endPointOrConnectionString, RestConfig configuration = null)
        {
            var lowerConn = endPointOrConnectionString?.ToLower();
            if (lowerConn != null && lowerConn.StartsWith("http"))
                this.BaseUri = new Uri(endPointOrConnectionString);
            else
                ConnectionString = endPointOrConnectionString;
            Init(configuration);
        }

        public RestClient(string endPointOrConnectionString, HttpMessageHandler httpMessageHandler = null, RestConfig config = null, string body = null)
        {
            var lowerConn = endPointOrConnectionString?.ToLower();
            if (lowerConn != null && lowerConn.StartsWith("http"))
                this.BaseUri = new Uri(endPointOrConnectionString);
            else
                ConnectionString = endPointOrConnectionString;
            this.RestHttpMessageHandler = httpMessageHandler;
            this._body = body;
            Init(config);
        }

        private void PrepareHttpRestme()
        {
        }

        public RestMode CurrentMode
        {
            get { return Configuration.OperationMode; }
            set
            {
                Configuration.OperationMode = value;
                switch (value)
                {
                    case RestMode.HTTPClient:
                    default:
                        PrepareHttpRestme();
                        break;
                }
            }
        }



        public void Add(object value)
        {
            if (_params?.Count > 0)
                throw new InvalidOperationException(
                    "Additional parameters have been added, try use Add(string key, object value) instead of Add(object value).");
            else
                _objAsParam = value;
        }

        public void Add(string key, string value)
        {
            _params = _params ?? new Dictionary<string, string>();

            if (_params.ContainsKey(key))
                _params[key] = value;
            else
                _params.Add(key, value);
        }

        public void Add(string key, object value)
        {
            _params = _params ?? new Dictionary<string, string>();

            if (_params.ContainsKey(key))
                _params[key] = JsonConvert.SerializeObject(value, Configuration.SerializerSettings);
            else
                _params.Add(key,JsonConvert.SerializeObject(value, Configuration.SerializerSettings                   
                        ));
        }

        public void AddHeader(string header, string value, bool allowMultipleValues = false)
        {
            _headers = _headers ?? new Dictionary<string, List<string>>();
            if (_headers.ContainsKey(header))
            {
                _headers[header] = _headers[header] ?? new List<string>();
                if (allowMultipleValues)
                    _headers[header].Add(value);
                else
                    _headers[header] = new List<string>() { value };
            }
            else
                _headers.Add(header, new List<string>() { value });
        }

        public void AddBearerToken(string token)
        {
            AddHeader("Authorization", $"Bearer {token}");
        }

        public ResponseModel Request<T>(HttpMethod method, string relativeUrlPath = null)
        {
            return Task.Run(async () => await RequestAsync<ResponseModel>(method, relativeUrlPath)).Result;
        }

        public async Task<ResponseModel> RequestAsync<T>(HttpMethod method, string relativePath = null)
        {
            switch (CurrentMode)
            {
                case RestMode.HTTPClient:
                    return await this.HttpRequestAsync<T>(method, relativePath, RestHttpMessageHandler);
                default:
                    throw new NotSupportedException(
                        "Generic request async method only supports HTTP requests, please use other extension methods or switch operation RestMode to HTTPClient");
            }
        }

        public ResponseModel Get<T>(string keyOrRelativeUrlPath = null)
        {
            return Task.Run(async () => await GetAsync<T>(keyOrRelativeUrlPath)).Result;
        }

        public async Task<ResponseModel> GetAsync<T>(string keyOrRelativeUrlPath = null)
        {
            switch (CurrentMode)
            {
                case RestMode.HTTPClient:
                    return await this.HttpGetAsync<T>(keyOrRelativeUrlPath);
                default:
                    throw new NotSupportedException(
                        "Generic request async method only supports HTTP requests, please use other extension methods or switch operation RestMode to HTTPClient");
            }
        }

        public ResponseModel Get(string keyOrRelativePath = null)
        {
            return Task.Run(async () => await GetAsync(keyOrRelativePath)).Result;
        }

        public async Task<ResponseModel> GetAsync(string keyOrRelativePath = null)
        {
            return await GetAsync<string>(keyOrRelativePath);
        }


        public ResponseModel Post<T>(string keyOrRelativeUrlPath = null, T dataObject = default(T))
        {
            return PostAsync<T>(keyOrRelativeUrlPath, dataObject).Result;
        }

        public Task<ResponseModel> PostAsync<T>(string keyOrRelativeUrlPath = null, T dataObject = default(T))
        {
            switch (CurrentMode)
            {
                case RestMode.HTTPClient:
                    if (dataObject != null)
                        throw new NotSupportedException(
                            "dataObject is not expected when using Http Client, please check your RestMode");
                    return RequestAsync<ResponseModel>(HttpMethod.Post, keyOrRelativeUrlPath);
                default:
                    throw new NotSupportedException("Unexpected RestMode, let me call it a break!");
            }
        }

        public Task<ResponseModel> PostAsync(string keyOrRelativeUrlPath = null, string dataObject = null)
        {
            return PostAsync<string>(keyOrRelativeUrlPath, dataObject);
        }

        public ResponseModel Post(string keyOrRelativeUrlPath = null, string dataObject = null)
        {
            return PostAsync(keyOrRelativeUrlPath, dataObject).Result;
        }

        public ResponseModel Delete<T>(string keyOrRelativeUrlPath = null)
        {
            return Task.Run(async () => await DeleteAsync<T>(keyOrRelativeUrlPath)).Result;
        }

        public Task<ResponseModel> DeleteAsync<T>(string keyOrRelativeUrlPath = null)
        {
            switch (CurrentMode)
            {
                case RestMode.HTTPClient:
                    return RequestAsync<T>(HttpMethod.Delete, keyOrRelativeUrlPath);
                default:
                    throw new NotSupportedException("Unexpected RestMode, let me call it a break!");
            }
        }

        public Task<ResponseModel> DeleteAsync(string keyOrRelativeUrlPath = null)
        {
            return DeleteAsync<bool>(keyOrRelativeUrlPath);
        }

        public ResponseModel Delete(string keyOrRelativeUrlPath = null)
        {
            return DeleteAsync(keyOrRelativeUrlPath).Result;
        }

        #region Private Methods        

        public void PrepareHeaders(HttpRequestHeaders headers)
        {
            if (!(_headers?.Count > 0)) return;

            foreach (var item in _headers)
            {
                try
                {
                    headers.Add(item.Key, item.Value);
                }
                catch (Exception)
                {
                    //ignore
                }
            }
        }

        public string PrepareInjectParamsIntoQuery(string urlPath)
        {
            urlPath = urlPath ?? string.Empty;
            var nvc = urlPath.IdentifyQueryParams();
            if (_params?.Count > 0)
            {
                foreach (var k in _params.Keys)
                {
                    nvc.Add(k, _params[k]);
                }
            }
            var indexOfQuestionMark = urlPath.IndexOf('?');
            if (indexOfQuestionMark > 0)
                return urlPath.Substring(0, indexOfQuestionMark) + nvc.ParseIntoQueryString();
            else
                return urlPath + nvc.ParseIntoQueryString();
        }

       
        #endregion
    }
}