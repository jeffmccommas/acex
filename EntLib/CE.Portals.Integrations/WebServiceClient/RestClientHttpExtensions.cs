﻿using System;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Linq;
using CE.Portals.Integrations.Models;
using Newtonsoft.Json;
using System.Text;

namespace CE.Portals.Integrations.WebServiceClient
{
    public static class RestClientHttpExtensions
    {
        public static ResponseModel HttpRequest<T>(this RestClient restClient, HttpMethod method, string relativeUrlPath = null, HttpMessageHandler httpMessageHandler = null)
        {
            return Task.Run(async () => await restClient.HttpRequestAsync<T>(method, relativeUrlPath)).Result;
        }

        public static async Task<ResponseModel> HttpRequestAsync<T>(this RestClient restClient, HttpMethod method, string relativePath = null, HttpMessageHandler httpMessageHandler = null)
        {
            HttpClient httpClient;

            if (httpMessageHandler == null)
                 httpClient = new HttpClient { BaseAddress = restClient.BaseUri };
            else
                httpClient = new HttpClient(httpMessageHandler) { BaseAddress = restClient.BaseUri };

            restClient.PrepareHeaders(httpClient.DefaultRequestHeaders);
            HttpResponseMessage response = null;
            
            if (method == HttpMethod.Post)
            {
                if (restClient._body != null)
                {
                    var content = new StringContent(restClient._body, UnicodeEncoding.UTF8, restClient.ContentYype);
                    response = await httpClient.PostAsync(new Uri(restClient.BaseUri, relativePath), content);
                }
            }
            else if (method == HttpMethod.Put)
            {
                response =
                    await
                        httpClient.PutAsync(new Uri(restClient.BaseUri, relativePath),
                            new FormUrlEncodedContent(restClient._params));
            }
            else if (method == HttpMethod.Get)
            {
                response =
                    await
                        httpClient.GetAsync(new Uri(restClient.BaseUri, restClient.PrepareInjectParamsIntoQuery(relativePath)));
            }
            else if (method == HttpMethod.Delete)
            {
                response =
                    await
                        httpClient.DeleteAsync(new Uri(restClient.BaseUri, restClient.PrepareInjectParamsIntoQuery(relativePath)));
            }

            if (response != null)
            {
                var contentObject = await response.Content.ReadAsStringAsync();
                ResponseModel contentResponse = new ResponseModel();

                if (!response.IsSuccessStatusCode)
                {
                    contentResponse.StatusCode = response.StatusCode;
                    contentResponse.Content = contentObject;
                    contentResponse.StatusDescription = "Error occurred while executing the request";

                }
                else
                {


                    if ((response.Content.Headers.ContentType.MediaType == "application/xml" ||
                        response.Content.Headers.ContentType.MediaType == "text/xml") && response.RequestMessage.Headers.Authorization.Scheme != "SharedAccessSignature")
                    {
                        //contentResponse = JsonConvert.DeserializeObject<Response>(contentObject);
                        var doc = new XmlDocument {PreserveWhitespace = false};
                        doc.LoadXml(contentObject);

                        XNode translatedNode = XDocument.Parse((doc.FirstChild.NextSibling as XmlNode).OuterXml);

                        var jsonText = JsonConvert.SerializeXNode(translatedNode, Formatting.Indented);
                        jsonText = FixJsonText(jsonText);

                        contentResponse.Content = jsonText;

                        contentResponse.StatusCode = HttpStatusCode.OK;
                        /*return new ContentResult
                    {
                        ContentType = "application/json",
                        Content = jsonText,
                        StatusCode = 200
                    }; */
                    }
                    else
                    {
                        contentResponse.StatusCode = response.StatusCode;
                        contentResponse.Content = contentObject;
                    }
                }
                try
                {
                    if (contentObject.Length > 0 || response.RequestMessage.Headers.Authorization.Scheme == "SharedAccessSignature")
                    {
                        var responseModel = new ResponseModel()
                            {Content = contentResponse.Content, StatusCode = contentResponse.StatusCode};

                        return responseModel;
                    }
                    else
                    {
                        var responseModel = new ResponseModel()
                        {
                            Content = null,
                            StatusCode = contentResponse.StatusCode,
                            StatusDescription = "Error occurred while executing the request"
                        };

                        return responseModel;
                    }
                }
                catch (Exception ex)
                {
                    var responseModel = new ResponseModel()
                    {
                        Content = null,
                        StatusCode = System.Net.HttpStatusCode.InternalServerError,
                        StatusDescription = "Error occurred while executing the request"
                    };

                    return responseModel;
                }
            }

            {
                var responseModel = new ResponseModel()
                {
                    Content = null,
                    StatusCode = System.Net.HttpStatusCode.InternalServerError,
                    StatusDescription = "Error occurred while executing the request"
                };

                return responseModel;
            }
        }

        private static string FixJsonText(string text)
        {
            text = text.Replace("#text", "text");
            text = text.Replace("@name", "name");
            text = text.Replace("@address1", "address1");
            text = text.Replace("@city", "city");
            text = text.Replace("@country", "country");
            text = text.Replace("@postalCode", "postalCode");
            text = text.Replace("@state", "state");
            text = text.Replace("@linkDescription", "linkDescription");
            text = text.Replace("@link", "link");
            text = text.Replace("@httpMethod", "httpMethod");
            text = text.Replace("@units", "units");
            text = text.Replace("@temporary", "temporary");
            text = text.Replace("@default", "default");
            text = text.Replace("@id", "id");
            text = text.Replace("@hint", "hint");
            text = text.Replace("wasteMeterInfo", "meterInfo");
            text = text.Replace("wasteMeter", "meter");
            text = text.Replace("@estimatedValue", "estimatedValue");


            /* text = text.Replace("@httpMethod", "httpMethod");
             text = text.Replace("@httpMethod", "httpMethod");
             text = text.Replace("@httpMethod", "httpMethod");
             text = text.Replace("@httpMethod", "httpMethod");
             text = text.Replace("@httpMethod", "httpMethod");
             text = text.Replace("@httpMethod", "httpMethod");
             text = text.Replace("@httpMethod", "httpMethod");*/


            return text;

        }
        public static ResponseModel HttpGet<T>(this RestClient restClient, string relativeUrlPath = null)
        {
            return Task.Run(async () => await restClient.HttpGetAsync<T>(relativeUrlPath)).Result;
        }

        public static async Task<ResponseModel> HttpGetAsync<T>(this RestClient restClient, string relativeUrlPath = null)
        {
            return await restClient.RequestAsync<T>(HttpMethod.Get, relativeUrlPath);
        }

        public static ResponseModel HttpPost<T>(this RestClient restClient, string relativeUrlPath = null)
        {
            return restClient.HttpPostAsync<T>(relativeUrlPath).Result;
        }

        public static Task<ResponseModel> HttpPostAsync<T>(this RestClient restClient, string relativeUrlPath = null)
        {
            return restClient.RequestAsync<T>(HttpMethod.Post, relativeUrlPath);
        }
    }
}