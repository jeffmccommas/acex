﻿using Aclara.Tools.Common.StatusManagement;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CE.AO.ClientEmulator.Interfaces
{

    /// <summary>
    /// Generate client test data status interface.
    /// </summary>
    public interface IGenerateClientTestDataStatus
    {
        string StatusMessage { get; set; }
        StatusList StatusList { get; }
    }
}
