﻿using Aclara.Tools.Common.StatusManagement;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CE.AO.ClientEmulator.Interfaces
{
    /// <summary>
    /// Generate client test data result interface.
    /// </summary>
    public interface IGenerateClientTestDataResult
    {
        string ResultMessage { get; set; }
        StatusList StatusList { get; }
    }
}
