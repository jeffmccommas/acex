﻿using CE.AO.ClientEmulator.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CE.AO.ClientEmulator.Results
{
    /// <summary>
    /// Create generate client test data factory.
    /// </summary>
    public static class GenerateClientTestDataStatusFactory
    {

        /// <summary>
        /// Create generate client test data status.
        /// </summary>
        /// <returns></returns>
        static public IGenerateClientTestDataStatus CreateGenerateClientTestDataStatus()
        {
            IGenerateClientTestDataStatus result = null;
            GenerateClientTestDataStatus generateClientTestDataStatus = null;

            try
            {
                generateClientTestDataStatus = new GenerateClientTestDataStatus();

                result = generateClientTestDataStatus;

                return result;
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
