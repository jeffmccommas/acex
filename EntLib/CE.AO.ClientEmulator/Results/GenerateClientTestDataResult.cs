﻿using Aclara.Tools.Common.StatusManagement;
using CE.AO.ClientEmulator.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CE.AO.ClientEmulator.Results
{
    /// <summary>
    /// Generate client test data result.
    /// </summary>
    public class GenerateClientTestDataResult : IGenerateClientTestDataResult
    {

        #region Private Constants
        #endregion

        #region Private Data  Members

        private string _resultMessage;

        StatusList _statusList;

        #endregion

        #region Public Properties

        /// <summary>
        /// Property: Result message.
        /// </summary>
        public string ResultMessage
        {
            get { return _resultMessage; }
            set { _resultMessage = value; }
        }

        /// <summary>
        /// Property: Status list.
        /// </summary>
        public StatusList StatusList
        {
            get
            {
                return _statusList;
            }
        }

        #endregion

        #region Public Constructors

        /// <summary>
        /// Default constructor.
        /// </summary>
        public GenerateClientTestDataResult()
        {
            _statusList = new StatusList();
        }

        #endregion

    }
}
