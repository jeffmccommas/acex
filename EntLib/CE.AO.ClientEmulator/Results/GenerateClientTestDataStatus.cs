﻿using Aclara.Tools.Common.StatusManagement;
using CE.AO.ClientEmulator.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CE.AO.ClientEmulator.Results
{
    public class GenerateClientTestDataStatus : IGenerateClientTestDataStatus
    {

        #region Private Constants
        #endregion

        #region Private Data  Members

        private string _statusMessage = string.Empty;
        private StatusList _statusList;

        #endregion

        #region Public Properties

        /// <summary>
        /// Status message.
        /// </summary>
        public string StatusMessage
        {
            get { return _statusMessage; }
            set { _statusMessage = value; }
        }

        /// <summary>
        /// Property: Status list.
        /// </summary>
        public StatusList StatusList
        {
            get
            {
                return _statusList;
            }
        }

        #endregion

        #region Public Constructors

        /// <summary>
        /// Default constructor.
        /// </summary>
        public GenerateClientTestDataStatus()
        {
            _statusList = new StatusList();
        }

        #endregion

    }
}
