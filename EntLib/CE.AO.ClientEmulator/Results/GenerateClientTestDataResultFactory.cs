﻿using Aclara.Tools.Common.StatusManagement;
using CE.AO.ClientEmulator.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CE.AO.ClientEmulator.Results
{

    /// <summary>
    /// Generate client test data result factory.
    /// </summary>

    /// <summary>
    /// Create generate client test data factory.
    /// </summary>
    public static class GenerateClientTestDataResultFactory
    {

        /// <summary>
        /// Create generate client test data status.
        /// </summary>
        /// <returns></returns>
        static public IGenerateClientTestDataResult CreateGenerateClientTestDataResult()
        {
            IGenerateClientTestDataResult result = null;
            GenerateClientTestDataResult generateClientTestDataResult = null;

            try
            {
                generateClientTestDataResult = new GenerateClientTestDataResult();

                result = generateClientTestDataResult;

                return result;
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
