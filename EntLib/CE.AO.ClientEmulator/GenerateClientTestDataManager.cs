﻿using Aclara.Tools.Common.StatusManagement;
using CE.AO.ClientEmulator.Events;
using CE.AO.ClientEmulator.Interfaces;
using CE.AO.ClientEmulator.Results;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace CE.AO.ClientEmulator
{

    /// <summary>
    /// Generate cleint test data manager.
    /// </summary>
    public class GenerateClientTestDataManager
    {

        #region Private Constants
        #endregion

        #region Private Data Members

        #endregion

        #region Public Delegates

        public event EventHandler<GenerateClientTestDataStatusEventArgs> ClientTestDataGenerationStatus;
        public event EventHandler<GenerateClientTestDataResultEventArgs> ClientTestDataGenerated;

        #endregion

        #region Public Properties


        #endregion

        #region Public Constructors

        /// <summary>
        /// Preferred constructor.
        /// </summary>
        public GenerateClientTestDataManager()
        {
        }

        #endregion

        #region Private Constructors

        #endregion

        #region Public Methods


        /// <summary>
        /// Generate client test data.
        /// </summary>
        /// <param name="simulatedDurationPerItemInSecounds"></param>
        /// <param name="simulatedDataItemCount"></param>
        /// <param name="cancellationToken"></param>
        public void GenerateClientTestData(int simulatedDataItemCount, 
                                           int simulatedDurationPerItemInSecounds, 
                                           CancellationToken cancellationToken)
        {
            IGenerateClientTestDataResult generateClientTestDataResult = null;
            GenerateClientTestDataStatusEventArgs generateClientTestDataStatusEventArgs = null;
            IGenerateClientTestDataStatus generateClientTestDataStatus = null;
            GenerateClientTestDataResultEventArgs generateClientTestDataResultEventArgs = null;
            string statusMessage = string.Empty;
            string resultMessage = string.Empty;

            try
            {
                                                                                                                   
                for (int dataIndex = 0; dataIndex < simulatedDataItemCount; dataIndex++)
                {
                    generateClientTestDataStatusEventArgs = new GenerateClientTestDataStatusEventArgs();

                    //Check for cancellation.
                    cancellationToken.ThrowIfCancellationRequested();

                    //Generate client test data.
                    GenerateClientTestDataHelper(simulatedDurationPerItemInSecounds);

                    //Report: Status.
                    statusMessage = string.Format("Generate Client Test Data Status: Data item {0} of {1} generated.",
                                                  dataIndex + 1, simulatedDataItemCount);

                    generateClientTestDataStatus = GenerateClientTestDataStatusFactory.CreateGenerateClientTestDataStatus();
                    generateClientTestDataStatus.StatusMessage = statusMessage;
                    generateClientTestDataStatusEventArgs.GenerateClientTestDataStatus = generateClientTestDataStatus;

                    if (ClientTestDataGenerationStatus != null)
                    {
                        ClientTestDataGenerationStatus(this, generateClientTestDataStatusEventArgs);
                    }

                }

                //Report: Client test data generation completed.
                generateClientTestDataResult = GenerateClientTestDataResultFactory.CreateGenerateClientTestDataResult();
                resultMessage = string.Format("Generate Client Test Data Result: All requested data items generated.");
                generateClientTestDataResult.ResultMessage = resultMessage;
                generateClientTestDataResultEventArgs = new GenerateClientTestDataResultEventArgs();
                generateClientTestDataResultEventArgs.GenerateClientTestDataResult = generateClientTestDataResult;
                if (ClientTestDataGenerated != null)
                {
                    ClientTestDataGenerated(this, generateClientTestDataResultEventArgs);
                }

            }
            catch (OperationCanceledException)
            {
                throw;
            }
            catch (Exception ex)
            {
                Status status = null;
                generateClientTestDataResult = GenerateClientTestDataResultFactory.CreateGenerateClientTestDataResult();
                generateClientTestDataResultEventArgs = new GenerateClientTestDataResultEventArgs();

                status = new Status();
                status.Exception = ex;
                status.StatusServerity = StatusTypes.StatusSeverity.Error;
                generateClientTestDataResult.StatusList.Add(status);
                generateClientTestDataResultEventArgs.GenerateClientTestDataResult = generateClientTestDataResult;
                if (ClientTestDataGenerated != null)
                {
                    ClientTestDataGenerated(this, generateClientTestDataResultEventArgs);
                }
            }

        }

        /// <summary>
        /// GenerateClientTestDataHelper
        /// </summary>
        /// <returns></returns>
        public void GenerateClientTestDataHelper(int simulatedDurationPerItemInSecounds)
        {
            int millisecondsTimeout = 0;

            try
            {
                 millisecondsTimeout = (int)TimeSpan.FromSeconds(simulatedDurationPerItemInSecounds).TotalMilliseconds;

                Thread.Sleep(millisecondsTimeout);

            }
            catch (Exception)
            {
                throw;
            }

        }

        #endregion

        #region Protected methods

        #endregion

        #region Private methods

        #endregion

    }
}
