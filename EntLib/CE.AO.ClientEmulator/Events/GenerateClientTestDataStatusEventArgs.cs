﻿using CE.AO.ClientEmulator.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CE.AO.ClientEmulator.Events
{
    /// <summary>
    /// Generate client test data status event arguments.
    /// </summary>
    public class GenerateClientTestDataStatusEventArgs : EventArgs
    {

        #region Private Constants
        #endregion

        #region Private Data Members

        private IGenerateClientTestDataStatus _generateClientTestDataStatus;

        #endregion

        #region Public Properties

        public IGenerateClientTestDataStatus GenerateClientTestDataStatus
        {
            get { return _generateClientTestDataStatus; }
            set { _generateClientTestDataStatus = value; }
        }

        #endregion

        #region Public Constructors

        /// <summary>
        /// Default constructor.
        /// </summary>
        public GenerateClientTestDataStatusEventArgs()
        {
        }

        /// <summary>
        /// Overridden constructor.
        /// </summary>
        /// <param name="generateClientTestDataStatus"></param>
        public GenerateClientTestDataStatusEventArgs(IGenerateClientTestDataStatus generateClientTestDataStatus)
        {
            _generateClientTestDataStatus = generateClientTestDataStatus;
        }

        #endregion

    }
}
