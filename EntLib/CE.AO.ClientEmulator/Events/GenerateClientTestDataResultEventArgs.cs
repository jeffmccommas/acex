﻿using CE.AO.ClientEmulator.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CE.AO.ClientEmulator.Events
{
    /// <summary>
    /// Generate client test data result event arguments.
    /// </summary>
    public class GenerateClientTestDataResultEventArgs
    {

        #region Private Constants
        #endregion

        #region Private Data Members

        private IGenerateClientTestDataResult _generateClientTestDataResult;

        #endregion

        #region Public Properties

        public IGenerateClientTestDataResult GenerateClientTestDataResult
        {
            get { return _generateClientTestDataResult; }
            set { _generateClientTestDataResult = value; }
        }

        #endregion

        #region Public Constructors

        /// <summary>
        /// Default constructor.
        /// </summary>
        public GenerateClientTestDataResultEventArgs()
        {
        }

        /// <summary>
        /// Overridden constructor.
        /// </summary>
        /// <param name="generateClientTestDataResult"></param>
        public GenerateClientTestDataResultEventArgs(IGenerateClientTestDataResult generateClientTestDataResult)
        {
            _generateClientTestDataResult = generateClientTestDataResult;
        }

        #endregion
    }
}
