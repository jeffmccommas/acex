﻿using System.Collections.Generic;

namespace AO.DataAccess.Contracts
{
   public interface IOdbcRepository
   {
       List<object[]> Get(out string logDetails,out List<string> columnslList,string filterQuery);
   }
}
