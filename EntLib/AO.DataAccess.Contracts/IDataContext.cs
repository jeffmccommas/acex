﻿using System.Collections;
using System.Threading;
using System.Threading.Tasks;

namespace AO.DataAccess.Contracts
{
    public interface IDataContext
    {
        int SaveChanges();
        Task<int> SaveChangesAsync(CancellationToken cancellationToken);
        Task<int> SaveChangesAsync();

        /// <summary>
        /// Force a refresh of the context with data from the data store.
        /// </summary>
        void Refresh();

        /// <summary>
        /// Force a refresh of the context with specified data.
        /// </summary>
        void Refresh(IEnumerable refreshableObjects);
    }
}