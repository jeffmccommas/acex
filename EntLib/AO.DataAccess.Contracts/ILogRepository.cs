﻿using System.Collections.Generic;
using System.Threading.Tasks;
using AO.DTO;
using AO.Entities;

namespace AO.DataAccess.Contracts
{
    /// <summary>
    /// Interface for LogRepository
    /// </summary>
    public interface ILogRepository
    {
        /// <summary>
        /// To insert/update the LogEntity in Log table.
        /// </summary>
        /// <param name="logEntity">LogViewModel which is to be inserted/updated</param>
        /// <returns>True</returns>
        Task<bool> InsertOrMergeLogAsync(LogEntity logEntity);

        /// <summary>
        /// To fetch the list of logs from the database
        /// </summary>
        /// <param name="filterParams">Filter params for logs to be fetched</param>
        /// <returns>List of LogEntity</returns>
        List<LogEntity> GetLogHistory(LogHistoryRequestDTO filterParams);

        /// <summary>
        /// To delete the LogEntity in log table.
        /// </summary>
        /// <param name="logEntity">LogViewModel which is to be deleted</param>
        /// <returns>True</returns>
        Task<bool> DeleteLogAsync(LogEntity logEntity);
    }
}