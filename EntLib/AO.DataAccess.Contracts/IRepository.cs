﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading;
using System.Threading.Tasks;

namespace AO.DataAccess.Contracts
{
    public interface IRepository<TEntity> where TEntity : class
    {
        TEntity Find(params object[] keyValues);

        void Insert(TEntity entity);

        void InsertRange(IEnumerable<TEntity> entities);

        /// <summary>
        /// Update entity.
        /// </summary>
        void Update(TEntity entity);

        /// <summary>
        /// A cascade delete will be performed for all related in-memory child entities,
        /// assuming the required relationships are defined in the model. This method sets the State of
        /// the entities for deletion, but SaveChanges() is still required to commit the change(s) to the data store.
        /// </summary>
        /// <param name="entity">Entity to remove from the context.</param>
        void Delete(TEntity entity);
        void Refresh();

        IQuery<TEntity> Query(Expression<Func<TEntity, bool>> query);
        IQuery<TEntity> Query();
        IQueryable<TEntity> Queryable();
        Task<TEntity> FindAsync(params object[] keyValues);

        Task<TEntity> FindAsync(CancellationToken cancellationToken, params object[] keyValues);
        Task<bool> DeleteAsync(params object[] keyValues);
        Task<bool> DeleteAsync(CancellationToken cancellationToken, params object[] keyValues);
        void SaveChanges();

    }
}