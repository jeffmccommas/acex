﻿using System.Collections.Generic;
using AO.Domain;

namespace AO.DataAccess.Contracts
{
    /// <summary>
    /// Interface for the Model-First TenantRepository
    /// </summary>
    public interface ITenantRepository : IRepository<Tenant>
    {
        /// <summary>
        /// Gets the tenant by tenant ID.
        /// </summary>
        /// <param name="tenantId">The ID of the tenant.</param>
        /// <returns></returns>
        Tenant GetByTenantId(string tenantId);

        /// <summary>
        /// Gets the list of Clients
        /// </summary>
        /// <returns></returns>
        List<Client> GetClients();
    }
}