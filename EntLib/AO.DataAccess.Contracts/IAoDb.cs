﻿using System.Data.Entity;
using AO.Domain;

namespace AO.DataAccess.Contracts
{
    /// <summary>
    /// Context for AclaraOne.
    /// </summary>
    public interface IAoDb : IDataContext
    {
        IDbSet<Tenant> Tenants { get; set; }
    }
}