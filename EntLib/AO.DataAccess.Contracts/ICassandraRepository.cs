﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace AO.DataAccess.Contracts
{
    /// <summary>
    /// User defined action to perform while paging through data store result set. 
    /// </summary>
    /// <param name="rowNumber"></param>
    /// <param name="columnValues"></param>
    /// <param name="columnNames"></param>
    public delegate void AoRowAction(long rowNumber, string[] columnValues, string[] columnNames);

    public interface ICassandraRepository
    {

        void ExecutePagedQuery(string cqlQuery, AoRowAction aoRowAction, int pageSize = 1000);

        bool InsertOrUpdateBatch<T>(List<T> tableModel, string tableName) where T : class;

        Task InsertOrUpdateBatchAsync<T>(List<T> tableModel, string tableName) where T : class;

        bool InsertOrUpdate<T>(T tableModel, string tableName) where T : class;

        Task InsertOrUpdateAsync<T>(T tableModel, string tableName) where T : class;
        Task InsertOrUpdateAsync<T>(T tableModel, string tableName, char consistency) where T : class;

        bool InsertOrUpdate<T>(T tableModel, string tableName, char consistency) where T : class;
        bool InsertOrUpdate<T>(T tableModel, string tableName, int ttlInSeconds) where T : class;

        Task InsertOrUpdateAsync<T>(T tableModel, string tableName, int ttlInSeconds) where T : class;

        IList<T> Get<T>(string tableName, Expression<Func<T, bool>> filter = null) where T : class;

        IList<T> Get<T>(string tableName, char consistency, Expression<Func<T, bool>> filter = null) where T : class;

        IList<T> Get<T>(string tableName, int pageSize, ref byte[] token, Expression<Func<T, bool>> filter = null)
            where T : class;

        Task<IList<T>> GetAsync<T>(string tableName, Expression<Func<T, bool>> filter = null) where T : class;

        Task<IList<T>> GetAsync<T>(string tableName, char consistency, Expression<Func<T, bool>> filter = null)
            where T : class;

        T GetSingle<T>(string tableName, Expression<Func<T, bool>> filter = null) where T : class;

        Task<T> GetSingleAsync<T>(string tableName, Expression<Func<T, bool>> filter = null) where T : class;

        Task<T> GetSingleAsync<T>(string tableName, char consistency, Expression<Func<T, bool>> filter = null) where T : class;

        void ExecuteUpdateQuery(string updateQuery, params object[] properties);

        Task DeleteAsync<T>(string tableName, Expression<Func<T, bool>> filter) where T : class;

        Task DeleteAsync<T>(string tableName, Expression<Func<T, bool>> filter, char consistency) where T : class;

        void Delete<T>(string tableName, Expression<Func<T, bool>> filter) where T : class;

        void Delete<T>(string tableName, Expression<Func<T, bool>> filter, char consistency) where T : class;

    }
}
