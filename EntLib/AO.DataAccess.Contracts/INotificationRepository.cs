﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using AO.Entities;
using CE.AO.Models;

namespace AO.DataAccess.Contracts
{
    /// <summary>
    /// Interface for NotificationRepository
    /// </summary>
    public interface INotificationRepository
    {
        /// <summary>
        /// Inserts a new notification into data store.  If the notification alreadys exists
        /// then a merge is performed.
        /// </summary>
        /// <param name="notification">Notification which is to be inserted/merged</param>
        Task InsertOrMergeNotification(NotificationEntity notification);

        /// <summary>
        /// Inserts a new notification into report data store.  If the report notification alreadys exists
        /// then a merge is performed.
        /// </summary>
        /// <param name="notificationReport">Notification which is to be inserted/merged</param>
        Task InsertOrMergeNotificationReport(NotificationReportEntity notificationReport);

        /// <summary>
        /// Deletes a notification from the data store. 
        /// </summary>
        /// <param name="clientId">Client Id of the notification that should be deleted</param>
        /// <param name="customerId">Customer Id of the notification that should be deleted</param>
        /// <param name="accountId">Acount Id of the notification that should be deleted</param>
        /// <returns></returns>
        Task DeleteNotification(int clientId,string customerId, string accountId);

        /// <summary>
        /// Delete notification based on the supplied notification model
        /// </summary>
        /// <param name="notification">notification</param>
        Task DeleteNotification(NotificationEntity notification);

        /// <summary>
        /// Retrieves fully hydrated Notification based on the supplied InsightModel and InsightSettings values.
        /// </summary>
        /// <param name="evaluation">InsightModel</param>
        /// <param name="insightSettings">InsightSettings</param>
        Task<NotificationEntity> GetNotification(InsightModel evaluation, InsightSettings insightSettings);

        /// <summary>
        /// Retrieves fully hydrated Notification based on the supplied NotificationEntity;
        /// </summary>
        /// <param name="notification"></param>
        /// <returns>Notification</returns>
        Task<NotificationEntity> GetNotification(NotificationEntity notification);

        /// <summary>
        /// Retrieves all notifications in the data store based on the supplied <paramref name="clientId"/> 
        /// and <paramref name="accountId"/>
        /// </summary>
        /// <param name="clientId">ClientId that notifications should be retrieved for.</param>
        /// <param name="customerId">CustomerId that notifications should be retrieved for.</param>
        /// <param name="accountId">AccountId that notifications should be retrieved for.</param>
        /// <returns>List of notifications</returns>
        Task<IList<NotificationEntity>> GetNotifications(int clientId, string customerId, string accountId);

        /// <summary>
        /// Retreives all notifications for a specified client that fall within the <paramref name="startDate"/> 
        /// and <paramref name="endDate"/>
        /// </summary>
        /// <param name="clientId">Client Id that notifications should be returned for</param>
        /// <param name="startDate">Start date range for the notification search</param>
        /// <param name="endDate">End date range for the notification search</param>
        /// <returns>List of notifications</returns>
        Task<IList<NotificationEntity>> GetNotifications(int clientId, DateTime startDate, DateTime endDate);

        /// <summary>
        /// Retrieves all notifications for all clients and accounts.  
        /// NOTE: This call can be quite expensive if a large number of notifications exist.  Use a 
        ///       more selective notificaiton retrieval method if possible.
        /// </summary>
        /// <returns>List of notifications</returns>
        Task<IList<NotificationEntity>> GetNotifications();
    }
}