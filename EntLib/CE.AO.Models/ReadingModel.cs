﻿using System;
using CE.RateModel;

namespace CE.AO.Models
{
    public class ReadingModel: Reading
    {
        public ReadingModel(Enums.TimeOfUse timeOfUse, DateTime timestamp, double quantity) : base(timeOfUse, timestamp, quantity)
        {
        }
        public ReadingModel(DateTime timestamp,double quantity) :base(timestamp, quantity)
        { 
        }
        public string TimestampString { get; set; }
        // ReSharper disable once InconsistentNaming
        public int UOMId { get; set; }
    }
}
