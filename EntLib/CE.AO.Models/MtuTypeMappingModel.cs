﻿using System.ComponentModel.DataAnnotations;

namespace CE.AO.Models
{
	public class MtuTypeModel
	{
		/// <summary>
		/// Id for this MTU type mapping
		/// </summary>
		[Required]
		public int MtuTypeId { get; set; }

		/// <summary>
		/// User friendly MTU Type
		/// </summary>
		public string MtuType { get; set; }

		/// <summary>
		/// Description for MTU type mapping
		/// </summary>
		public string Description { get; set; }
	}
}
