﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CE.AO.Models
{
    public  class CostModel
    {
        public double ServiceCtd { get; set; }
        public double ServiceProjectedCost { get; set; }
    }
}
