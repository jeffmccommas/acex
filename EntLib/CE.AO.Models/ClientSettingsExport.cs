﻿using System.Collections.Generic;

namespace CE.AO.Models
{
   public class ClientSettingsExport
    {
       public int ClientId { get; set; }
       public List<Filter> Filters { get; set; }
    }

    public class Filter
    {
        public string OutputFileName { get; set; }
        public string FilterQuery { get; set; }
        public string ExportLocation { get; set; }
        public string FilterName { get; set; }
        public string ContainerName { get; set; }
        public string BlobConnectionString { get; set; }
    }

}
