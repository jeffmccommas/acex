﻿namespace CE.AO.Models
{
    public class ClientAccountReportModel
    {
        public string RowCreateDate { get; set; }
        public int ClientId { get; set; }
        public string AccountId { get; set; }
        public string CustomerId { get; set; }
    }
}
