﻿namespace CE.AO.Models
{
    public class AmiRequestModel
    {
        public int ClientId { get; set; }
        public string AccountNumber { get; set; }
        public string ServiceId { get; set; }
        public string PremiseId { get; set; }
        public string ServicePointId { get; set; }
        public string MeterId { get; set; }
        public string StartDate { get; set; }
        public string EndDate { get; set; }
        public string BillingPeriodStartDate { get; set; }
        public string FuelId { get; set; }
    }
}
