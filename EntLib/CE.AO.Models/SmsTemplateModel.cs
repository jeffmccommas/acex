﻿namespace CE.AO.Models
{
    public class SmsTemplateModel
    {
        public int ClientId { get; set; }
        public string InsightTypeName { get; set; }
        public string Body { get; set; }
        public string TemplateId { get; set; }
    }
}
