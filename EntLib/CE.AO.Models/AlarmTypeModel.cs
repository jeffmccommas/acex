﻿using System.ComponentModel.DataAnnotations;

namespace CE.AO.Models
{
    /// <summary>
    /// View Model for AlarmType
    /// </summary>

    public class AlarmTypeModel
    {
        /// <summary>
        /// Alarm Source Type Id
        /// </summary>
        [Required]
        public int AlarmSourceTypeId { get; set; }

        /// <summary>
        /// Alarm Group Id
        /// </summary>
        [Required]
        public int AlarmGroupId { get; set; }

        /// <summary>
        /// Alarm Type Id
        /// </summary>
        [Required]
        public int AlarmTypeId { get; set; }

        /// <summary>
        /// Alarm Group Name
        /// </summary>
        [Required]
        public string AlarmGroupName { get; set; }

        /// <summary>
        /// Alarm Source Type
        /// </summary>
        [Required]
        public string AlarmSourceType { get; set; }

        /// <summary>
        /// Alarm Type Name
        /// </summary>
        [Required]
        public string AlarmTypeName { get; set; }
    }
}
