﻿using System.Collections.Generic;
using CE.RateModel;

namespace CE.AO.Models
{
    public class AmiModelForCTD
    {
        public List<Reading> listofReadings { get; set; }
        public int CommodityId { get; set; }
        public double CtdDays { get; set; }
        public double AverageUsage { get; set; }
        public double AverageCost { get; set; }
        public double meterCost { get; set; }
        public double meterProjectedCost { get; set; }
        //public  FuelType { get; set; }
        public double DailyAmiSum { get; set; }
        public int unitId { get; set; }

    }
}
