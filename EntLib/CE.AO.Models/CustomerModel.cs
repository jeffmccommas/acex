﻿namespace CE.AO.Models
{
    public class CustomerModel
    {
        public int ClientId { get; set; }
        public string CustomerId { get; set; }
        public string Country { get; set; }
        public string Source { get; set; }
        public bool IsBusiness { get; set; }
        public string AddressLine1 { get; set; }
        public string AddressLine2 { get; set; }
        public string AddressLine3 { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string PostalCode { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Phone1 { get; set; }
        public string Phone2 { get; set; }
        public string EmailAddress { get; set; }
        public string CustomerType { get; set; }
        public string TrumpiaSubscriptionId { get; set; }
        public string TrumpiaRequestDetailPkRk { get; set; }
        public string TrumpiaRequestDetailId { get; set; }
    }
}
