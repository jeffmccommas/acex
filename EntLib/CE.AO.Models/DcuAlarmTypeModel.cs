﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CE.AO.Models
{
    public class DcuAlarmTypeModel
	{
        /// <summary>
        /// Hard coded DCU Alarm Type
        /// </summary>
        [Required]
        public string TableName { get; set; }

        /// <summary>
        /// Alarm Code for Mapping
        /// </summary>
        [Required]
		public string AlarmCode { get; set; }

        /// <summary>
        /// Description for Alarm Code
        /// </summary>
		public string UserFriendlyDescription { get; set; }

        /// <summary>
        ///  Severity
        /// </summary>
		public int? Severity { get; set; }

        /// <summary>
        /// Notification Type
        /// </summary>
		public string NotificationType { get; set; }

        /// <summary>
        /// Component Type
        /// </summary>
		public string ComponentType { get; set; }
	}
}
