﻿namespace CE.AO.Models
{
    /// <summary>
    /// Class is Responsible for Holding Deserialize Standard UOM JSON object
    /// </summary>
    public class StandardUomModel
    {
        /// <summary>
        /// It will hold Water's standard Uom
        /// </summary>
        public string Water { get; set; }

        /// <summary>
        /// It will hold Electric's standard Uom
        /// </summary>
        public string Electric { get; set; }

        /// <summary>
        /// It will hold Gas' standard Uom
        /// </summary>
        public string Gas { get; set; }
    }
}