﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CE.AO.Models
{
    public class TrumpiaRequestDetailReportModel
    {
        public string RowCreateDate { get; set; }
        public string MessageId { get; set; }
        public string RequestId { get; set; }
        public string Description { get; set; }
        public string StatusCode { get; set; }
        public string ErrorMessage { get; set; }
        public string SmsBody { get; set; }
    }
}
