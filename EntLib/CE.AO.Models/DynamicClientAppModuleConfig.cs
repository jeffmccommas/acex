﻿using System.Collections.Generic;

namespace CE.AO.Models
{
    /// <summary>Class used to contain all required information to dynamically load one specific application module.</summary>
    public class DynamicClientAppModuleConfig
    {
        /// <summary>The ID of of the system module to be loaded.</summary>
        public int Id { get; set; }

        /// <summary>The ordinal position of which the module should appear in the top-level of the menu service.</summary>
        public int Position { get; set; }

        /// <summary>The collection of custom menu items configured for this client.</summary>
        public IList<DynamicAppMenuItem> CustomMenuItems { get; set; }
    }
}