﻿using CE.AO.Utilities;
using System;
using System.ComponentModel.DataAnnotations;
using WindowsAzure.Table.Attributes;
// ReSharper disable InconsistentNaming

namespace CE.AO.Models
{
    public class AmiDailyIntervalModel
    {
        [PartitionKey]
        // ReSharper disable once UnusedAutoPropertyAccessor.Local
        public string PartitionKey { get; private set; }
        [RowKey]
        // ReSharper disable once UnusedAutoPropertyAccessor.Local
        public string RowKey { get; private set; }
        [Required]
        public int ClientId { get; set; }
        [Required]
        public string AccountNumber { get; set; }
        [Required]
        public string MeterId { get; set; }
        public string ServicePointId { get; set; }
        [Required]
        [EnumDataType(typeof(Enums.CommodityType))]
        public int CommodityId { get; set; }
        [Required]
        [EnumDataType(typeof(Enums.UomType))]
        // ReSharper disable once InconsistentNaming
        public int UOMId { get; set; }
        public double? VolumeFactor { get; set; }
        [EnumDataType(typeof(Enums.Direction))]
        public int? Direction { get; set; }
        public DateTime? ProjectedReadDate { get; set; }
        public int IntervalType { get; set; }
        [EnumDataType(typeof(Enums.TOUType))]
        public int TOUID { get; set; }
        public string Timezone { get; set; }
        [Required]
        public DateTime AmiTimeStamp { get; set; }
        public double? IntValue0000 { get; set; }
        public double? TOU_Regular { get; set; }
        public double? TOU_OnPeak { get; set; }
        public double? TOU_OffPeak { get; set; }
        public double? TOU_Shoulder1 { get; set; }
        public double? TOU_Shoulder2 { get; set; }
        public double? TOU_CriticalPeak { get; set; }
    }
}
