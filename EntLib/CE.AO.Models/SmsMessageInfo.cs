﻿using System.Diagnostics.CodeAnalysis;
using Newtonsoft.Json.Linq;

namespace CE.AO.Models
{
    [SuppressMessage("ReSharper", "InconsistentNaming")]
    public class SmsMessageInfo
    {
        public string description { get; set; }
        public JObject sms { get; set; }
        public JObject recipients { get; set; }
        public string send_date { get; set; }        
    }
}
