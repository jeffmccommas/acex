﻿using System;
using System.ComponentModel.DataAnnotations;
using CE.AO.Utilities;

namespace CE.AO.Models
{
    public class Ami15MinuteIntervalModel : AmiModel
    {
        public string ServicePointId { get; set; }        
        public double? VolumeFactor { get; set; }
        [EnumDataType(typeof(Enums.Direction))]
        public int? Direction { get; set; }
        public DateTime? ProjectedReadDate { get; set; }
        public int IntervalType { get; set; }        
        public double? IntValue0000 { get; set; }
        public double? IntValue0015 { get; set; }
        public double? IntValue0030 { get; set; }
        public double? IntValue0045 { get; set; }
        public double? IntValue0100 { get; set; }
        public double? IntValue0115 { get; set; }
        public double? IntValue0130 { get; set; }
        public double? IntValue0145 { get; set; }
        public double? IntValue0200 { get; set; }
        public double? IntValue0215 { get; set; }
        public double? IntValue0230 { get; set; }
        public double? IntValue0245 { get; set; }
        public double? IntValue0300 { get; set; }
        public double? IntValue0315 { get; set; }
        public double? IntValue0330 { get; set; }
        public double? IntValue0345 { get; set; }
        public double? IntValue0400 { get; set; }
        public double? IntValue0415 { get; set; }
        public double? IntValue0430 { get; set; }
        public double? IntValue0445 { get; set; }
        public double? IntValue0500 { get; set; }
        public double? IntValue0515 { get; set; }
        public double? IntValue0530 { get; set; }
        public double? IntValue0545 { get; set; }
        public double? IntValue0600 { get; set; }
        public double? IntValue0615 { get; set; }
        public double? IntValue0630 { get; set; }
        public double? IntValue0645 { get; set; }
        public double? IntValue0700 { get; set; }
        public double? IntValue0715 { get; set; }
        public double? IntValue0730 { get; set; }
        public double? IntValue0745 { get; set; }
        public double? IntValue0800 { get; set; }
        public double? IntValue0815 { get; set; }
        public double? IntValue0830 { get; set; }
        public double? IntValue0845 { get; set; }
        public double? IntValue0900 { get; set; }
        public double? IntValue0915 { get; set; }
        public double? IntValue0930 { get; set; }
        public double? IntValue0945 { get; set; }
        public double? IntValue1000 { get; set; }
        public double? IntValue1015 { get; set; }
        public double? IntValue1030 { get; set; }
        public double? IntValue1045 { get; set; }
        public double? IntValue1100 { get; set; }
        public double? IntValue1115 { get; set; }
        public double? IntValue1130 { get; set; }
        public double? IntValue1145 { get; set; }
        public double? IntValue1200 { get; set; }
        public double? IntValue1215 { get; set; }
        public double? IntValue1230 { get; set; }
        public double? IntValue1245 { get; set; }
        public double? IntValue1300 { get; set; }
        public double? IntValue1315 { get; set; }
        public double? IntValue1330 { get; set; }
        public double? IntValue1345 { get; set; }
        public double? IntValue1400 { get; set; }
        public double? IntValue1415 { get; set; }
        public double? IntValue1430 { get; set; }
        public double? IntValue1445 { get; set; }
        public double? IntValue1500 { get; set; }
        public double? IntValue1515 { get; set; }
        public double? IntValue1530 { get; set; }
        public double? IntValue1545 { get; set; }
        public double? IntValue1600 { get; set; }
        public double? IntValue1615 { get; set; }
        public double? IntValue1630 { get; set; }
        public double? IntValue1645 { get; set; }
        public double? IntValue1700 { get; set; }
        public double? IntValue1715 { get; set; }
        public double? IntValue1730 { get; set; }
        public double? IntValue1745 { get; set; }
        public double? IntValue1800 { get; set; }
        public double? IntValue1815 { get; set; }
        public double? IntValue1830 { get; set; }
        public double? IntValue1845 { get; set; }
        public double? IntValue1900 { get; set; }
        public double? IntValue1915 { get; set; }
        public double? IntValue1930 { get; set; }
        public double? IntValue1945 { get; set; }
        public double? IntValue2000 { get; set; }
        public double? IntValue2015 { get; set; }
        public double? IntValue2030 { get; set; }
        public double? IntValue2045 { get; set; }
        public double? IntValue2100 { get; set; }
        public double? IntValue2115 { get; set; }
        public double? IntValue2130 { get; set; }
        public double? IntValue2145 { get; set; }
        public double? IntValue2200 { get; set; }
        public double? IntValue2215 { get; set; }
        public double? IntValue2230 { get; set; }
        public double? IntValue2245 { get; set; }
        public double? IntValue2300 { get; set; }
        public double? IntValue2315 { get; set; }
        public double? IntValue2330 { get; set; }
        public double? IntValue2345 { get; set; }
    }
}
