﻿using System;
using System.ComponentModel.DataAnnotations;
using CE.AO.Utilities;

namespace CE.AO.Models
{
    public class NccAmiReadingModel : AmiModel
    {
        [Required]
        public int TransponderId { get; set; }
        [Required]
        public int TransponderPort { get; set; }
        public string CustomerId { get; set; }
        [Required]
        public double ReadingValue { get; set; }
        public string BatteryVoltage { get; set; }
    }
}
