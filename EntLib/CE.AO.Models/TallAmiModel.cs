﻿using System;
using System.ComponentModel.DataAnnotations;
using CE.AO.Utilities;
// ReSharper disable InconsistentNaming

namespace CE.AO.Models
{
    public class TallAmiModel
    {
        [Required]
        public int ClientId { get; set; }
        public string AccountNumber { get; set; }
        [Required]
        public string MeterId { get; set; }
        public string ServicePointId { get; set; }
        [Required]
        [EnumDataType(typeof(Enums.CommodityType))]
        public int CommodityId { get; set; }
        [Required]
        [EnumDataType(typeof(Enums.UomType))]
        public int UOMId { get; set; }
        public double? VolumeFactor { get; set; }
        [EnumDataType(typeof(Enums.Direction))]
        public int? Direction { get; set; }
        public DateTime? ProjectedReadDate { get; set; }
        [EnumDataType(typeof(Enums.Timezone))]
        public string Timezone { get; set; }
        [Required]
        public DateTime AmiTimeStamp { get; set; }
        public double? Consumption { get; set; }

        /// <summary>
        /// Consumption converted to Standard Unit of Measure
        /// </summary>
        // ReSharper disable once InconsistentNaming
        public double? StandardUOMConsumption { get; set; }

        /// <summary>
        /// Standard Unit of Measure to which all the reads will be converted
        /// </summary>
        // ReSharper disable once InconsistentNaming
        public int? StandardUOMId { get; set; }

        public string BatteryVoltage { get; set; }
        [EnumDataType(typeof(Enums.TOUType))]
        public int TOUID { get; set; }
        public string TransponderId { get; set; }
        public string TransponderPort { get; set; }
        public string DialRead { get; set; }
        public string QualityCode { get; set; }
        public string Scenario { get; set; }
        public string ReadingType { get; set; }
        public double? TOU_Regular { get; set; }
        public double? TOU_OnPeak { get; set; }
        public double? TOU_OffPeak { get; set; }
        public double? TOU_Shoulder1 { get; set; }
        public double? TOU_Shoulder2 { get; set; }
        public double? TOU_CriticalPeak { get; set; }
        public string CustomerId { get; set; }
        public int IntervalType { get; set; }
    }
}
