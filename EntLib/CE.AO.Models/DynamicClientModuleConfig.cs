﻿using System.Collections.Generic;

namespace CE.AO.Models
{
    /// <summary>Class for storing all information required for system to load and configure an application module in the portal.</summary>
    public class DynamicClientModuleConfig
    {
        /// <summary>The app state to which the application should default to upon load.</summary>
        public string DefaultState { get; set; }

        /// <summary>Collection of modules to be loaded.</summary>
        public List<DynamicClientAppModuleConfig> ConfiguredModules { get; set; }
    }
}