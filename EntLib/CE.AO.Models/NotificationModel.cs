﻿using System;

namespace CE.AO.Models
{
    public class NotificationModel
    {
        public int ClientId { get; set; }
        public string AccountId { get; set; }
        public string ServiceContractId { get; set; }
        public string ProgramName { get; set; }
        public string Insight { get; set; }
        public bool? IsNotified { get; set; }
        public bool? IsDelivered { get; set; }
        public bool? IsDropped { get; set; }
        public bool? IsBounced { get; set; }
        public bool? IsOpened { get; set; }
        public bool? IsClicked { get; set; }
        public bool? IsScheduled { get; set; }
        public string Channel { get; set; }
        public string TemplateId { get; set; }
        public string CustomerId { get; set; }
        // ReSharper disable once InconsistentNaming
        public string EvaluationPK { get; set; }
        // ReSharper disable once InconsistentNaming
        public string EvaluationRK { get; set; }
        public string EvaluationType { get; set; }
        public DateTime NotifiedDateTime { get; set; }
        public string TrumpiaRequestDetailPkRk { get; set; }

        public string TrumpiaRequestDetailId { get; set; }
        public string Tier { get; set; }
        public bool IsLatest { get; set; }
    }
}
