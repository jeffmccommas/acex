﻿using System.Collections.Generic;
using System.Collections.Specialized;
using System.Diagnostics;

namespace CE.AO.Models
{
    public class ClientSettings
    {
        public int ClientId { get; set; }
        public string TimeZone { get; set; }
        public bool IsAmiIntervalStart { get; set; }
        public bool IsDstHandled { get; set; }
        public List<ProgramSettings> Programs { get; set; }
        public int MinimumDaysForInsightNotification { get; set; }
        public string CalculationScheduleTime { get; set; }
        public string NotificationReportScheduleTime { get; set; }
        public string InsightReportScheduleTime { get; set; }
        public string ForgotPasswordEmailTemplateId { get; set; }
        public string OptInEmailUserName { get; set; }
        public string OptInEmailPassword { get; set; }
        public string OptInEmailFrom { get; set; }
        public string RegistrationConfirmationEmailTemplateId { get; set; }
        public string OptInEmailConfirmationTemplateId { get; set; }
        public string OptInSmsConfirmationTemplateId { get; set; }
        public string TrumpiaShortCode { get; set; }
        public string TrumpiaApiKey { get; set; }
        public string TrumpiaUserName { get; set; }
        public string TrumpiaContactList { get; set; }
        public int UnmaskedAccountIdEndingDigit { get; set; }
		public bool IsVeeSubscribed { get; set; }
        public string ExtendedAttributes { get; set; }  // a csv name/value set of parameters


        private StringDictionary _extendedAttributesDict;  // a set of name/value pairs for alternate formats
        private readonly string _padLock = "padLock";  // a lock for initialization of the above
        private const string OldNumericFormat = "0.00";

        /// <summary>
        /// Provide a way of supporting additional attributes without having to change the class or signatures up the line.
        /// Uses the ExtendedAttributes which is a csv n/v set.  
        /// e.g.  "numFmt=0.00,usgFmt=0,some=other
        /// This is a retrofit to be a little more flexible in using the json content
        /// </summary>
        /// <param name="attrName">name of attribute we want</param>
        /// <param name="defaultFormat"> what to return if not in config</param>
        /// <returns>a partial format string</returns>
        public string GetExtendedAttr(string attrName, string defaultFormat = null) {
            if (_extendedAttributesDict == null && !string.IsNullOrEmpty(ExtendedAttributes)) {  // initialize 
                lock (_padLock) {
                    if (_extendedAttributesDict == null && !string.IsNullOrEmpty(ExtendedAttributes)) {
                        _extendedAttributesDict = new StringDictionary();
                        if (!string.IsNullOrEmpty(ExtendedAttributes)){
                            string[] nameValues = ExtendedAttributes.Replace("\\,", ",").Split(',');  // allow escaping of the comma seperator
                            foreach (string nameValue in nameValues) {
                                string[] nv = nameValue.Split('=');
                                if (nv.Length > 1) {
                                    if (_extendedAttributesDict.ContainsKey(nv[0]))
                                        _extendedAttributesDict[nv[0]] = nv[1];
                                    else
                                        _extendedAttributesDict.Add(nv[0], nv[1]);
                                }
                                else {
                                    if (nv.Length > 0){
                                        if (_extendedAttributesDict[nv[0]] == null)
                                            _extendedAttributesDict.Add(nv[0], "");
                                        else
                                            _extendedAttributesDict[nv[0]] = "";
                                    }
                                }
                            }
                        }
                    }
                }
            }
            if (_extendedAttributesDict == null) {
                return defaultFormat ?? OldNumericFormat;
            }
            else {
                return _extendedAttributesDict[attrName] ?? defaultFormat ?? OldNumericFormat;
            }
        }

        /// <summary>
        /// Format a numeric value with a configurable format or a default format.
        /// This is a retrofit so assume it might end up being used other places and without configured values in the json.
        /// Note that this should only be called if the intention is to change default behavior or enable configuration changs for 
        /// data items.
        /// This will likely be extended to include additional types.
        /// </summary>
        /// <param name="val"> value to convert</param>
        /// <param name="defaultFormat"> what to return if not in config</param>
        /// <param name="attrName">name of attribut that has format string</param>
        /// <returns>a formatted value</returns>
        public string GetFormattedValue(double val, string defaultFormat = "0.00", string attrName = "normalDecimalFormat") {
            string formatStg;
            if (attrName != null) {
                formatStg = "{0:" + GetExtendedAttr(attrName, defaultFormat) + "}";
            }
            else {
                formatStg = "{0:" + (defaultFormat ?? OldNumericFormat) + "}";
            }
            string formattedValue = string.Format(formatStg, val);
#if DEBUG
            Debug.WriteLine($"ClientSettings.GFV fmt='{formatStg}' in='{val}' out='{formattedValue}' ");
#endif
            return formattedValue;
        }



    }

    public class ProgramSettings
    {
        public string ProgramName { get; set; }
        public string UtilityProgramName { get; set; }
        public bool FileExportRequired { get; set; }
        public List<InsightSettings> Insights { get; set; }
        public bool DoubleOptInRequired { get; set; }
        public string TrumpiaKeyword { get; set; }
    }

    public class InsightSettings
    {
        public string InsightName { get; set; }
        public string UtilityInsightName { get; set; }
        public string AllowedCommunicationChannel { get; set; }
        public string DefaultCommunicationChannel { get; set; }
        // Allowed Frequency- Daily, Weekly, Monthly, Threshold - Check - enum NotificationFrequency
        public string DefaultFrequency { get; set; }
        public string DefaultValue { get; set; }
        public string EmailTemplateId { get; set; }
        public string SmsTemplateId { get; set; }
        public string NotifyTime { get; set; }
        public string NotificationDay { get; set; }
        public string TrumpiaKeyword { get; set; }
        public string Level { get; set; }
        public string CommodityType { get; set; }
        public string ThresholdType { get; set; }
        public string ThresholdMin { get; set; }
        public string ThresholdMax { get; set; }
    }
}
