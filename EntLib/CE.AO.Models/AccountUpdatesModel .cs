﻿using System.ComponentModel.DataAnnotations;
using CE.AO.Utilities;
using System;
namespace CE.AO.Models
{
    public class AccountUpdatesModel
    {
        [Required]
        public int ClientId { get; set; }
        [Required]
        public string CustomerId { get; set; }
        [RequiredIf("UpdateType", new object[] { Enums.AccountUpdateType.RateClass, Enums.AccountUpdateType.MeterReplacement }, "AccountId is required.")]
        public string AccountId { get; set; }
        public string PremiseId { get; set; }
        [RequiredIf("UpdateType", new object[] { Enums.AccountUpdateType.RateClass, Enums.AccountUpdateType.MeterReplacement }, "MeterId is required.")]
        public string MeterId { get; set; }
        [RequiredIf("UpdateType", new object[] { Enums.AccountUpdateType.RateClass, Enums.AccountUpdateType.MeterReplacement, Enums.AccountUpdateType.ReadStartDate, Enums.AccountUpdateType.ReadEndDate }, "ServiceContractId is required.")]
        public string ServiceContractId { get; set; }
        [Required]
        [EnumDataType(typeof(Enums.AccountUpdateType))]
        public string UpdateType { get; set; }
        [RequiredIf("UpdateType", Enums.AccountUpdateType.MeterReplacement, "OldValue is required.")]
        public string OldValue { get; set; }
        [Required]
        public string NewValue { get; set; }
        public DateTime? UpdateSentDate { get; set; }
        public bool IsLatest { get; set; }
        public DateTime? UpdatedDate { get; set; }
    }
}
