﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace CE.AO.Models
{
    public static class Utilities
    {
        public static bool Validate<T>(T model, out List<ValidationResult> validationResults)
        {
            var context = new ValidationContext(model, null, null);
            validationResults = new List<ValidationResult>();
            Validator.TryValidateObject(model, context, validationResults, true);

            var propertyList = model.GetType()
               .GetProperties().Where(p => p.PropertyType == typeof(DateTime)).ToList();

            validationResults.AddRange(from property in propertyList
                                       where Convert.ToDateTime(property.GetValue(model)) == DateTime.MinValue
                                       select new ValidationResult($"{property.Name} is required.", new List<string> { $"{property.Name}" }));

            return validationResults.Count <= 0;
        }
    }
}
