﻿using System;
using System.ComponentModel.DataAnnotations;

namespace CE.AO.Models
{
    /// <summary>
    /// DcuProvisioning model
    /// </summary>
    public class DcuProvisioningModel
    {
        /// <summary>
        /// Client Identifier
        /// </summary>        
        [Required]
        public int ClientId { get; set; }

        /// <summary>        
        /// Dcu Identifier
        /// </summary>
        [Required]
        public int DcuId { get; set; }

        /// <summary>
        /// Date that DCU was installed on Star network
        /// </summary>
        public string InstalledDate { get; set; }

        /// <summary>
        /// The DCU longitude coordinate.
        /// </summary>
        public string Longitude { get; set; }

        /// <summary>
        /// The DCU latitude coordinate.
        /// </summary>
        public string Latitude { get; set; }

        /// <summary>
        /// Indicates the name of DCU
        /// </summary>
        public string Name { get; set; }
    }
}
