﻿using System;
using System.ComponentModel.DataAnnotations;

namespace CE.AO.Models
{
	public class DcuAlarmModel
	{
        /// <summary>
        /// Data Collector Unit 
        /// </summary>
		[Required]
		public int DcuId { get; set; }

        /// <summary>
        /// Alarm Code for Mapping
        /// </summary>
        [Required]
		public string AlarmCode { get; set; }

        /// <summary>
        /// Alarm Time
        /// </summary>
		[Required]
		public DateTime AlarmTime  { get; set; }

        /// <summary>
        /// Client Id 
        /// </summary>
		[Required]
		public int ClientId { get; set; }

        /// <summary>
        /// Description for Alarm Code
        /// </summary>
		public string UserFriendlyDescription { get; set; }

        /// <summary>
        ///  Severity
        /// </summary>
        public int? Severity { get; set; }

        /// <summary>
        /// Notification Type
        /// </summary>
		public string NotificationType { get; set; }

        /// <summary>
        /// Component Type
        /// </summary>
        public string ComponentType { get; set; }

	}
}
