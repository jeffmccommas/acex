﻿using System;

namespace CE.AO.Models
{
    public class InsightModel
    {
        public int ClientId { get; set; }
        public string CustomerId { get; set; }
        public string AccountId { get; set; }
        public string ServiceContractId { get; set; }
        public double? BilllDays { get; set; }
        public double? BtdCost { get; set; }
        public double? BtdProjectedCost { get; set; }
        public double? Usage { get; set; }
        public bool? SupressInsight { get; set; }
        public bool? AccountLevelCostThresholdExceed { get; set; }
        public bool? ServiceLevelUsageThresholdExceed { get; set; }
        public bool? ServiceLevelCostThresholdExceed { get; set; }
        public bool? DayThresholdExceed { get; set; }
        public bool? ServiceLevelTiered1ThresholdApproaching { get; set; }
        public bool? ServiceLevelTiered2ThresholdApproaching { get; set; }
        public bool? ServiceLevelTiered3ThresholdApproaching { get; set; }
        public bool? ServiceLevelTiered4ThresholdApproaching { get; set; }
        public bool? ServiceLevelTiered5ThresholdApproaching { get; set; }
        public bool? ServiceLevelTiered6ThresholdApproaching { get; set; }
        public bool? ServiceLevelTieredThresholdExceed { get; set; }
        public double? ServiceDays { get; set; }
        public double? CtdCost { get; set; }
        public double? CtdProjectedCost { get; set; }
        public string NotifyTime { get; set; }
        public string ProgramName { get; set; }
        public DateTime AsOfEvaluationDate { get; set; }
        public Guid AsOfEvaluationId { get; set; }
        public string BillCycleStartDate { get; set; }
        public string BillCycleEndDate { get; set; }
        public string Type { get; set; }
        public bool IsLatest { get; set; }
    }
}
