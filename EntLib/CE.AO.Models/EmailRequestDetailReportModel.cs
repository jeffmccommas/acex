﻿using System;

namespace CE.AO.Models
{
    public class EmailRequestDetailReportModel
    {
        public string RowCreateDate { get; set; }
        public int ClientId { get; set; }
        public string AccountId { get; set; }
        public string ServiceContractId { get; set; }
        public string ProgramName { get; set; }
        public string Insight { get; set; }
        public string Channel { get; set; }
        public DateTime NotifiedDateTime { get; set; }
        public string Description { get; set; }
        public string ErrorMessage { get; set; }
        public string Event { get; set; }
        public string Url { get; set; }
    }
}
