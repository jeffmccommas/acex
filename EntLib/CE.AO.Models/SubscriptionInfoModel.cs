﻿using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using Newtonsoft.Json.Linq;

namespace CE.AO.Models
{
    [SuppressMessage("ReSharper", "InconsistentNaming")]
    public class SubscriptionInfoModel
    {
        public string list_name { get; set; }
        public JArray subscriptions { get; set; }
    }

    [SuppressMessage("ReSharper", "InconsistentNaming")]
    public class SubscriptionResultModel : MessageResultModel
    {
        public int subscription_id { get; set; }
        public int push_id { get; set; }
    }

    [SuppressMessage("ReSharper", "InconsistentNaming")]
    public class SubscriptionSearchResultModel : MessageResultModel
    {
        public List<string> subscription_id_list { get; set; }
    }

    [SuppressMessage("ReSharper", "InconsistentNaming")]
    public class MessageResultModel
    {
        public string message_id { get; set; }
        public string request_id { get; set; }
        public string status_code { get; set; }
        public string error_message { get; set; }

    }

    [SuppressMessage("ReSharper", "InconsistentNaming")]
    public class SubscriptionInfoResultModel
    {
        public string last_name { get; set; }
        public string first_name { get; set; }
        public SubscriptionMobileResultModel mobile { get; set; }
        public string error_message { get; set; }
        public string status_code { get; set; }
    }

    [SuppressMessage("ReSharper", "InconsistentNaming")]
    public class SubscriptionMobileResultModel
    {
        public string verified { get; set; }
        public string country_code { get; set; }
        public string value { get; set; }
    }

}
