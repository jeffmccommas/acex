﻿namespace CE.AO.Models
{
    public class TrumpiaRequestDetailModel
    {
        public string MessageId { get; set; }
        public string RequestId { get; set; }
        public string Description { get; set; }
        public string StatusCode { get; set; }
        public string ErrorMessage { get; set; }
        public string SmsBody { get; set; }
    }
}
