﻿using System.Diagnostics.CodeAnalysis;
using Newtonsoft.Json;

namespace CE.AO.Models
{
    [SuppressMessage("ReSharper", "InconsistentNaming")]
    public class EmailStatsModel
    {
        public string email { get; set; }
        public int timestamp { get; set; }
        [JsonProperty("__invalid_name__smtp-id")]
        public string invalidnamesmtpid { get; set; }
        public string @event { get; set; }
        public string category { get; set; }
        public string sg_event_id { get; set; }
        public string sg_message_id { get; set; }
        public string response { get; set; }
        public string attempt { get; set; }
        public string useragent { get; set; }
        public string ip { get; set; }
        public string url { get; set; }
        public string reason { get; set; }
        public string status { get; set; }
        public int? asm_group_id { get; set; }
        public string programName { get; set; }
        public int ClientId { get; set; }
        public string customKey { get; set; }
        public int tls { get; set; }

    }
}
