﻿namespace CE.AO.Models
{
    public class ClientWidgetConfigurationModel
    {
        public int ClientId { get; set; }
        public string WebTokenUrl { get; set; }
        public string WidgetPageUrl { get; set; }
        public string AccessKeyId { get; set; }
        public string Password { get; set; }
    }
}
