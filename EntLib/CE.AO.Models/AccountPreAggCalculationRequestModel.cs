﻿using System;
using System.ComponentModel.DataAnnotations;
using AO.DTO;
using CE.AO.Utilities;

namespace CE.AO.Models
{
    /// <summary>
    /// AccountPreAggCalculationRequestModel Class
    /// </summary>
    public class AccountPreAggCalculationRequestModel : TriggerDto
    {
        /// <summary>
        /// Client Identifier
        /// </summary>
        [Required]
        public int ClientId { get; set; }

        /// <summary>
        /// Account Identifier
        /// </summary>
        [Required]
        public string AccountId { get; set; }

        /// <summary>
        /// Meter Read Time stamp
        /// </summary>
        [Required]
        public DateTime AmiTimestamp { get; set; }

        /// <summary>
        /// Range at which Calculation needs to be done
        /// </summary>
        [EnumDataType(typeof(Enums.PreAggCalculationRange))]
        public Enums.PreAggCalculationRange CalculationRange { get; set; }

        /// <summary>
        /// Client's local timezone
        /// </summary>
        [EnumDataType(typeof(Enums.Timezone))]
        public string Timezone { get; set; }
    }
}
