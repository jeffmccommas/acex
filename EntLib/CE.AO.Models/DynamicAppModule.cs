﻿using System.Collections.Generic;

namespace CE.AO.Models
{
    /// <summary>Class used to contain all required information to dynamically load one specific application module.</summary>
    public class DynamicAppModule
    {
        /// <summary>The system ID of the module.</summary>
        public int Id { get; set; }

        /// <summary>The name of the module to load.</summary>
        public string Name { get; set; }

        /// <summary>A collection of resources to load for this module.</summary>
        public string[] Resources { get; set; }

        /// <summary>The collection of menu items associated with this module.</summary>
        public IList<DynamicAppMenuItem> MenuItems { get; set; }
    }
}