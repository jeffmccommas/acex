﻿using System;
using System.Diagnostics.CodeAnalysis;
using System.Xml.Serialization;

namespace CE.AO.Models
{
    [SuppressMessage("ReSharper", "InconsistentNaming")]
    [Serializable, XmlRoot("TRUMPIA")]
    public class TrumpiaInboundMessageModel
    {
        public string PUSH_ID { get; set; }
        public string INBOUND_ID { get; set; }
        public long PHONENUMBER { get; set; }
        public string KEYWORD { get; set; }
        public long SUBSCRIPTION_UID { get; set; }
        public string CONTENTS { get; set; }
    }
}
