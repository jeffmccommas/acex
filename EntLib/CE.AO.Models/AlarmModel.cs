﻿using System;
using System.ComponentModel.DataAnnotations;

namespace CE.AO.Models
{
    /// <summary>
    /// View Model for Alarm
    /// </summary>
    public class AlarmModel
    {
        /// <summary>
        /// Client Id 
        /// </summary>
        [Required]
        public int ClientId { get; set; }

        /// <summary>
        /// Alarm Source Type Id
        /// </summary>
        [Required]
        public int AlarmSourceTypeId { get; set; }

        /// <summary>
        /// alarm Time
        /// </summary>
        [Required]
        public DateTime AlarmTime { get; set; }

        /// <summary>
        /// Alarm Group Id
        /// </summary>
        [Required]
        public int AlarmGroupId { get; set; }

        /// <summary>
        /// Alarm Type Id
        /// </summary>
        [Required]
        public int AlarmTypeId { get; set; }

        /// <summary>
        /// Alarm_Id
        /// </summary>
        [Required]
        public int AlarmId { get; set; }

        /// <summary>
        /// Alarm Group Name
        /// </summary>
        public string AlarmGroupName { get; set; }

        /// <summary>
        /// Alarm Source Id
        /// </summary>
        [Required]
        public int AlarmSourceId { get; set; }

        /// <summary>
        /// Alarm Source Type
        /// </summary>
        public string  AlarmSourceType { get; set; }

        /// <summary>
        /// Alarm Type Name
        /// </summary>
        public string AlarmTypeName { get; set; }

        /// <summary>
        /// Details
        /// </summary>
        public string Details { get; set; }

    }
}
