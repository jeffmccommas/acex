﻿using System;

namespace CE.AO.Models
{
    /// <summary>
    /// This class is the model for MeterHistoryByMeterEntity. The properties of this class is used for representing model entities for meter_history_by_meter table.
    /// </summary>
    public class ClientMeterModel
    {      
            public int ClientId { get; set; }         
            public string MeterId { get; set; }                
            public string IsActive { get; set; }    
            //The AmiDate will represent the date of ami readings.        
            public DateTime? AmiDate { get; set; }         
    }
}
