﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Diagnostics.CodeAnalysis;
using CE.AO.Utilities;

namespace CE.AO.Models
{
    [SuppressMessage("ReSharper", "InconsistentNaming")]
    public class BillingCustomerPremiseModel
    {
        [Required]
        public string customer_id { get; set; }
        [Required]
        public string premise_id { get; set; }
        public string mail_address_line_1 { get; set; }
        public string mail_address_line_2 { get; set; }
        public string mail_address_line_3 { get; set; }
        public string mail_city { get; set; }
        public string mail_state { get; set; }
        public string mail_zip_code { get; set; }
        // removed required field for first name
        public string first_name { get; set; }
        [Required]
        public string last_name { get; set; }
        public string phone_1 { get; set; }
        public string phone_2 { get; set; }
        public string email { get; set; }
        [EnumDataType(typeof(Enums.CustomerType))]
        public string customer_type { get; set; }
        [Required]
        public string account_id { get; set; }
        public DateTime? active_date { get; set; }
        public DateTime? inactive_date { get; set; }
        public string read_cycle { get; set; }
        [RequiredIf("meter_type", new object[] { Enums.MeterType.ami, Enums.MeterType.regular }, "rate_code is required.")]
        public string rate_code { get; set; }
        public string service_point_id { get; set; }
        [Required]
        public string site_addressline1 { get; set; }
        // removed required field for address line 2
        public string site_addressline2 { get; set; }
        public string site_addressline3 { get; set; }
        [Required]
        public string site_city { get; set; }
        [Required]
        public string site_state { get; set; }
        [Required]
        public string site_zip_code { get; set; }
        [EnumDataType(typeof(Enums.MeterType))]
        public string meter_type { get; set; }
        [RequiredIf("meter_type", new object[] { Enums.MeterType.ami, Enums.MeterType.regular }, "meter_units is required.")]
        [EnumDataType(typeof(Enums.UomType))]
        public int? meter_units { get; set; }
        public string bldg_sq_foot { get; set; }
        public string year_built { get; set; }
        public string bedrooms { get; set; }
        public string assess_value { get; set; }
        [RequiredIf("meter_type", new object[] { Enums.MeterType.ami, Enums.MeterType.regular }, "usage_value is required.")]
        public double? usage_value { get; set; }
        [Required]
        public DateTime bill_enddate { get; set; }
        [Required]
        public int bill_days { get; set; }
        [EnumDataType(typeof(Enums.IsEstimate))]
        public string is_estimate { get; set; }
        [Required]
        public double usage_charge { get; set; }
        [Required]
        public int ClientId { get; set; }
        [Required]
        [EnumDataType(typeof(Enums.CommodityType))]
        public int service_commodity { get; set; }
        [EnumDataType(typeof(Enums.AccountStructureType))]
        public string account_structure_type { get; set; }
        [RequiredIf("meter_type", new object[] { Enums.MeterType.ami, Enums.MeterType.regular }, "meter_id is required.")]
        public string meter_id { get; set; }
        [Required]
        [EnumDataType(typeof(Enums.BillPeriodType))]
        public int billperiod_type { get; set; }
        public string meter_replaces_meterid { get; set; }
        [RequiredIf("meter_type", new object[] { Enums.MeterType.ami, Enums.MeterType.regular }, "service_read_date is required.")]
        public DateTime? service_read_date { get; set; } 
        public string Service_contract { get; set; }
        public string Programs { get; set; }
        public bool? IsFault { get; set; }
    }
}
