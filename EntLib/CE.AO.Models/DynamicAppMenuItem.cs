﻿using System.Collections.Generic;

namespace CE.AO.Models
{
    /// <summary>Class used to represent and allow for the dynamic display of a menu item hierarchy.</summary>
    public class DynamicAppMenuItem
    {
        /// <summary>The ordinal position of the menu item within the collection.</summary>
        public int Position { get; set; }

        /// <summary>The label that is displayed for the menu item.</summary>
        public string Label { get; set; }

        /// <summary>The graphical icon that should be displayed next to the menu item label.</summary>
        public string Icon { get; set; }

        /// <summary>The UI state that is associated with this menu item.</summary>
        public string State { get; set; }

        /// <summary>A list of keywords that should invoke the display of this menu item in the menu's search functionality.</summary>
        public string[] Keywords { get; set; }

        /// <summary>The security resource identifier associated with this menu item and its state.</summary>
        public string SecurityResource { get; set; }

        /// <summary>A collection of name/value pairs that are passed to the state.</summary>
        public Dictionary<string, object> Metadata { get; set; }

        /// <summary>The collection of child menu items associated with this menu item.</summary>
        public IList<DynamicAppMenuItem> MenuItems { get; set; }
    }
}
