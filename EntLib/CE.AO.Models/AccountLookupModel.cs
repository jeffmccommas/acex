﻿using System;

namespace CE.AO.Models
{
   public class AccountLookupModel
    {
        public int ClientId { get; set; }
        public string AccountId { get; set; }
        public string MeterId { get; set; }
        public bool? IsActive { get; set; }
        public string ServiceContractId { get; set; }
        public string CustomerId { get; set; }
        public bool IsServiceContractCreatedBySystem { get; set; }
        public DateTime AccountMeterTimeStamp { get; set; }
        
        public static string GetMaskedAccountId(string accountId, int unmaskedAccountIdEndingDigit)
        {
            var accountNumber = string.Concat("".PadLeft(accountId.Length - unmaskedAccountIdEndingDigit, '*'),
                    accountId.Substring(accountId.Length - unmaskedAccountIdEndingDigit,
                        unmaskedAccountIdEndingDigit));

            return accountNumber;
        }
    }
}
