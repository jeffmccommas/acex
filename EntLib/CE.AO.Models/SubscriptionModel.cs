﻿using System;
using System.ComponentModel.DataAnnotations;
using CE.AO.Utilities;

namespace CE.AO.Models
{
    public class SubscriptionModel
    {
        [Required]
        public int ClientId { get; set; }
        [Required]
        public string CustomerId { get; set; }
        [Required]
        public string AccountId { get; set; }
        [Required]
        public string ProgramName { get; set; }
        public string InsightTypeName { get; set; }
        public string PremiseId { get; set; }
        public string ServiceContractId { get; set; }
        public string ServicePointId { get; set; }
        public bool? IsEmailOptInCompleted { get; set; }
        public bool? IsSmsOptInCompleted { get; set; }
        [Required]
        public bool IsSelected { get; set; }
        [EnumDataType(typeof(Enums.SubscriptionChannel))]
        public string Channel { get; set; }
        public string AllowedChannels { get; set; }
        public double? SubscriberThresholdValue { get; set; }
        public string CommodityKey { get; set; }
        public string ThresholdType { get; set; }
        public double? ThresholdMin { get; set; }
        public double? ThresholdMax { get; set; }
        public bool IsDoubleOptIn { get; set; }
        public int? UomId { get; set; }
        public string Type { get; set; }
        public bool IsLatest { get; set; }

        public int? EmailConfirmationCount { get; set; }
        public int? SmsConfirmationCount { get; set; }
        public DateTime? SmsOptInCompletedDate { get; set; }
        public DateTime? EmailOptInCompletedDate { get; set; }
        public DateTime? SubscriptionDate { get; set; }
        public DateTime? OptOutDate { get; set; }
    }
}
