﻿namespace CE.AO.Models
{
    public class CustomInsightModel
    {
        public string InsightName { get; set; }
        public double? ThresholdValue { get; set; }
        public bool IsAccountLevel { get; set; }
        public int? UomId { get; set; }
    }
}
