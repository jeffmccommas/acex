﻿using CE.AO.Utilities;

namespace CE.AO.Models
{
    public class LogModel
    {
        public string ClientId { get; set; }
        public string CustomerId { get; set; }
        public string AccountId { get; set; }
        public string PremiseId { get; set; }
        public string ServiceContractId { get; set; }
        public string MeterId { get; set; }
        public string Metadata { get; set; }
        public int? ProcessingEventCount { get; set; }
        public string Source { get; set; }
        public Enums.Module Module { get; set; }
        public Enums.ProcessingType ProcessingType { get; set; }
        public string RowIndex { get; set; }
        public bool? DisableLog { get; set; }
    }
}
