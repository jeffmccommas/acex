﻿using System;
using System.Diagnostics;

namespace CE.AO.Models
{
    /// <summary>
    /// Model for calculating metrics
    /// </summary>
    public class MetricsModel
    {
        /// <summary>
        /// Name of metric to calculate
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// StopWatch to calculate time taken to perform an operation
        /// </summary>
        public readonly Stopwatch StopWatch = new Stopwatch();

        /// <summary>
        /// Log after performing a number of operations
        /// </summary>
        public int LogAfterNumberOfOperations = 100000;

        /// <summary>
        /// Total Number of operations performed
        /// </summary>
        public int NumberOfOperationsPerformed;

        /// <summary>
        /// Total time elapsed to perform operations
        /// </summary>
        public TimeSpan TimeElapsed = TimeSpan.Zero;

        /// <summary>
        /// true if user wants to log each operation
        /// </summary>
        public bool LogEachOperation = false;
    }
}