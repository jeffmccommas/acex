﻿using System;
using System.ComponentModel.DataAnnotations;

namespace CE.AO.Models
{
    public class DcuCallDataModel
    {
        /// <summary>
        /// Client Id 
        /// </summary>
        [Required]
        public int ClientId { get; set; }

        /// <summary>
        /// Data Collector Unit 
        /// </summary>
        [Required]
        public int DcuId { get; set; }

        /// <summary>
        /// Time when the call started
        /// </summary>
        [Required]
        public DateTime CallStart { get; set; }

        /// <summary>
        /// Time when the call ended
        /// </summary>
        [Required]
        public DateTime CallEnd { get; set; }

        /// <summary>
        ///  Type of Call
        /// </summary>
        [Required]
        public string CallType { get; set; }

        /// <summary>
        /// Number of bytes received
        /// </summary>
        [Required]
        public int Received { get; set; }

        /// <summary>
        /// Number of bytes expected
        /// </summary>
        [Required]
        public int Expected { get; set; }

        /// <summary>
        /// Is the call hung up
        /// </summary>
        [Required]
        public bool HungUp { get; set; }

        /// <summary>
        /// Is the dcu's memory cleared
        /// </summary>
        [Required]
        public bool Cleared { get; set; }
    }
}
