﻿using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;

namespace CE.AO.Models
{
    [SuppressMessage("ReSharper", "InconsistentNaming")]
    public class SmsStatsModel
    {
        public string push_id { get; set; }
        public string request_id { get; set; }
        public string message_id { get; set; }
        public string status { get; set; }
        public string error_message { get; set; }
        public string status_code { get; set; }
        public Sms sms { get; set; }
    }

    [SuppressMessage("ReSharper", "InconsistentNaming")]
    public class Sms
    {
        public int total { get; set; }
        public int sent { get; set; }
        public int failed { get; set; }
    }
    
}
