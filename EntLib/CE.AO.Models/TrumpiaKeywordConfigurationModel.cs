﻿namespace CE.AO.Models
{
    public class TrumpiaKeywordConfigurationModel
    {
        public string ClientId { get; set; }
        public string TrumpiaKeyword { get; set; }
    }
}
