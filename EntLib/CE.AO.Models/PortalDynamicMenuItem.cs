﻿using System.Collections.Generic;

namespace CE.AO.Models
{
    /// <summary>Class used to contain all required information to dynamically display a menu item and its structure.</summary>
    public class PortalDynamicMenuItem
    {
        /// <summary>The ordinal position of which the menu item should appear within the collection.</summary>
        public int Position { get; set; }

        /// <summary>The label that will be displayed to represent this item within the menu.</summary>
        public string Label { get; set; }

        /// <summary>The icon to display next to this menu item.</summary>
        public string Icon { get; set; }

        /// <summary>Additional keywords that should trigger the return of this menu item from a search.</summary>
        public string Keywords { get; set; }

        /// <summary>The state associated with this menu item.</summary>
        public string State { get; set; }

        /// <summary>A collection of metadata that is associated with this menu item.</summary>
        public Dictionary<string, string> MetaData { get; set; }

        /// <summary>A collection of child menu items to appear underneath this menu item.</summary>
        public List<PortalDynamicMenuItem> MenuItems { get; set; }
    }
}
