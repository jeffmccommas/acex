﻿using System;

namespace CE.AO.Models
{
    public class CalculationModel
    {
        public int ClientId { get; set; }
        public string AccountId { get; set; }
        public string CustomerId { get; set; }
        public string MeterId { get; set; }
        public string ServiceContractId { get; set; }
        public int? CommodityId { get; set; } // 
        public int? Unit { get; set; } // Id from Enum UomType
        public double? Cost { get; set; }
        public double? ProjectedCost { get; set; } // MeterProjectedCost
        public double? ProjectedUsage { get; set; }
        public double? Usage { get; set; }
        public double? BillDays { get; set; }
        public string BillCycleStartDate { get; set; }
        public string BillCycleEndDate { get; set; }
        public DateTime? AsOfAmiDate { get; set; }
        public DateTime AsOfCalculationDate { get; set; }
        public string RateClass { get; set; }
        public double? AverageDailyUsage { get; set; }
        public double? AverageDailyCost { get; set; }
        public string Type { get; set; }
        public bool IsLatest { get; set; }
    }
}
