﻿// ReSharper disable InconsistentNaming
namespace CE.AO.Models.RawFileModels
{
    /// <summary>
    /// MeterRead file headers
    /// </summary>
    public class RawMeterReadModel : RawBaseReadModel
    {
        /// <summary>
        /// Client Id 
        /// </summary>
        public string TENANT_ID { get; set; }

        /// <summary>
        /// Account Number 
        /// </summary>
        public string ACCOUNT_NUMBER { get; set; }

        /// <summary>
        /// Interval of the Read
        /// </summary>
        public string READ_INTERVAL { get; set; }
    }
}