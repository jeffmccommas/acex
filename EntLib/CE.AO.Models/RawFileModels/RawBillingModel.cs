﻿// ReSharper disable InconsistentNaming
namespace CE.AO.Models.RawFileModels
{
    public class RawBillingModel : IRawFileModel
    {
        // All attributes would be like billing raw file

        public string customer_id { get; set; }
        // ReSharper disable once InconsistentNaming
        public string premise_id { get; set; }
        public string mail_address_line_1 { get; set; }
        public string mail_address_line_2 { get; set; }
        public string mail_address_line_3 { get; set; }
        public string mail_city { get; set; }
        public string mail_state { get; set; }
        public string mail_zip_code { get; set; }
        public string first_name { get; set; }
        public string last_name { get; set; }
        public string phone_1 { get; set; }
        public string phone_2 { get; set; }
        public string email { get; set; }
        public string customer_type { get; set; }
        public string account_id { get; set; }
        public string active_date { get; set; }
        public string inactive_date { get; set; }
        public string read_cycle { get; set; }
        public string rate_code { get; set; }
        public string service_point_id { get; set; }
        public string site_addressline1 { get; set; }
        public string site_addressline2 { get; set; }
        public string site_addressline3 { get; set; }
        public string site_city { get; set; }
        public string site_state { get; set; }
        public string site_zip_code { get; set; }
        public string meter_type { get; set; }
        public string meter_units { get; set; }
        public string bldg_sq_foot { get; set; }
        public string year_built { get; set; }
        public string bedrooms { get; set; }
        public string assess_value { get; set; }
        public string usage_value { get; set; }
        public string bill_enddate { get; set; }
        public string bill_days { get; set; }
        public string is_estimate { get; set; }
        public string usage_charge { get; set; }
        public string ClientId { get; set; }
        public string service_commodity { get; set; }
        public string account_structure_type { get; set; }
        public string meter_id { get; set; }
        public string billperiod_type { get; set; }
        public string meter_replaces_meterid { get; set; }
        public string service_read_date { get; set; }
        public string Service_contract { get; set; }
        public string Programs { get; set; }
        

    }
}
