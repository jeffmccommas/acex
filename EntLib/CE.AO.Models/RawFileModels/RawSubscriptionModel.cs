﻿namespace CE.AO.Models.RawFileModels
{
    public class RawSubscriptionModel : IRawFileModel
    {
        public string ClientId { get; set; }
        public string CustomerId { get; set; }
        public string AccountId { get; set; }
        public string ProgramName { get; set; }
        public string InsightTypeName { get; set; }
        public string ServiceContractId { get; set; }
        public string IsEmailOptInCompleted { get; set; }
        public string IsSmsOptInCompleted { get; set; }
        public string IsSelected { get; set; }
        public string Channel { get; set; }
        public string SubscriberThresholdValue { get; set; }
       
    }
}
