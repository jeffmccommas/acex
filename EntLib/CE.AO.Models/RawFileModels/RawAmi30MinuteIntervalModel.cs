﻿namespace CE.AO.Models.RawFileModels
{
    public class RawAmi30MinuteIntervalModel : IRawFileModel
    {
        public string ClientId { get; set; }
        public string AccountNumber { get; set; }
        public string MeterId { get; set; }
        public string ServicePointId { get; set; }
        public string CommodityId { get; set; }
        // ReSharper disable once InconsistentNaming
        public string UOMId { get; set; }
        public string VolumeFactor { get; set; }
        public string Direction { get; set; }
        public string ProjectedReadDate { get; set; }
        public string TimeStamp { get; set; }
        public string Timezone { get; set; }
        public string IntValue0000 { get; set; }
        public string IntValue0030 { get; set; }
        public string IntValue0100 { get; set; }
        public string IntValue0130 { get; set; }
        public string IntValue0200 { get; set; }
        public string IntValue0230 { get; set; }
        public string IntValue0300 { get; set; }
        public string IntValue0330 { get; set; }
        public string IntValue0400 { get; set; }
        public string IntValue0430 { get; set; }
        public string IntValue0500 { get; set; }
        public string IntValue0530 { get; set; }
        public string IntValue0600 { get; set; }
        public string IntValue0630 { get; set; }
        public string IntValue0700 { get; set; }
        public string IntValue0730 { get; set; }
        public string IntValue0800 { get; set; }
        public string IntValue0830 { get; set; }
        public string IntValue0900 { get; set; }
        public string IntValue0930 { get; set; }
        public string IntValue1000 { get; set; }
        public string IntValue1030 { get; set; }
        public string IntValue1100 { get; set; }
        public string IntValue1130 { get; set; }
        public string IntValue1200 { get; set; }
        public string IntValue1230 { get; set; }
        public string IntValue1300 { get; set; }
        public string IntValue1330 { get; set; }
        public string IntValue1400 { get; set; }
        public string IntValue1430 { get; set; }
        public string IntValue1500 { get; set; }
        public string IntValue1530 { get; set; }
        public string IntValue1600 { get; set; }
        public string IntValue1630 { get; set; }
        public string IntValue1700 { get; set; }
        public string IntValue1730 { get; set; }
        public string IntValue1800 { get; set; }
        public string IntValue1830 { get; set; }
        public string IntValue1900 { get; set; }
        public string IntValue1930 { get; set; }
        public string IntValue2000 { get; set; }
        public string IntValue2030 { get; set; }
        public string IntValue2100 { get; set; }
        public string IntValue2130 { get; set; }
        public string IntValue2200 { get; set; }
        public string IntValue2230 { get; set; }
        public string IntValue2300 { get; set; }
        public string IntValue2330 { get; set; }
    }
}
