﻿// ReSharper disable InconsistentNaming
namespace CE.AO.Models.RawFileModels
{
    /// <summary>
    /// MeterAccountMtu file headers
    /// </summary>
    public class RawMeterAccountMtuModel : IRawFileModel
    {
        /// <summary>
        /// Client Id 
        /// </summary>
        public string TENANT_ID { get; set; }

        /// <summary>
        /// Uniquely defineds the association between an Account, Meter, 
        /// and MTU in NCC head end system. 
        /// </summary>
        public string CROSS_REFERENCE_ID { get; set; }

        /// <summary>
        /// Account Number 
        /// </summary>
        public string ACCOUNT_NUMBER { get; set; }

        /// <summary>
        /// Meter Identifier
        /// </summary>
        public string METER_ID { get; set; }

        /// <summary>
        /// MTU Identifier
        /// </summary>
        public string MTU_ID { get; set; }

        /// <summary>
        /// Type of this MTU.
        /// </summary>
        public int MTU_TYPE_ID { get; set; }

        /// <summary>
        /// Date that MTU was installed on Star network
        /// </summary>
        public string MTU_INSTALL_DATE { get; set; }

        /// <summary>
        /// Date that Meter was installed on Star network
        /// </summary>
        public string METER_INSTALL_DATE { get; set; }

        /// <summary>
        /// Indicates if this Account and Meter/MTU combination 
        /// is currently Active.  Example: An account has a meter replaced
        /// and the old Account/Meter combination is placed in inactive state
        /// while the new Account/Meter combination is placed in active state.
        /// </summary>
        public string ACTIVE { get; set; }

        /// <summary>
        /// The MTU longitude coordinate.
        /// </summary>
        public string LONGITUDE { get; set; }

        /// <summary>
        /// The MTU latitude coordinate.
        /// </summary>
        public string LATITUDE { get; set; }

        /// <summary>
        /// Indicates the type of Meter associated with this account.
        /// </summary>
        public string METER_TYPE_ID { get; set; }

        /// <summary>
        /// MeterSerialNumber associated with this meter
        /// </summary>
        public string METER_SERIAL_NUMBER { get; set; }
    }
}