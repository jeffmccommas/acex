﻿// ReSharper disable InconsistentNaming

namespace CE.AO.Models.RawFileModels
{
    public class RawDcuCallDataModel : IRawFileModel
    {
        /// <summary>
        /// Client Id 
        /// </summary>        
        public string TENANT_ID { get; set; }

        /// <summary>
        /// Data Collector Unit 
        /// </summary>
        public string DCU_ID { get; set; }

        /// <summary>
        /// Time when the call started
        /// </summary>
        public string CALL_START { get; set; }

        /// <summary>
        /// Time when the call ended
        /// </summary>
        public string CALL_END { get; set; }

        /// <summary>
        ///  Type of Call
        /// </summary>
        public string CALL_TYPE { get; set; }

        /// <summary>
        /// Number of bytes received
        /// </summary>
        public string RECEIVED { get; set; }

        /// <summary>
        /// Number of bytes expected
        /// </summary>
        public string EXPECTED { get; set; }

        /// <summary>
        /// Is the call hung up
        /// </summary>
        public string HUNG_UP { get; set; }

        /// <summary>
        /// Is the dcu's memory cleared
        /// </summary>
        public string CLEARED { get; set; }
    }
}
