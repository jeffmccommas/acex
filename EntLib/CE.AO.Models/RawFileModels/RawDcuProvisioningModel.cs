﻿// ReSharper disable InconsistentNaming
namespace CE.AO.Models.RawFileModels
{
    /// <summary>
    /// DcuProvisioning file headers
    /// </summary>
    public class RawDcuProvisioningModel : IRawFileModel
    {
        /// <summary>
        /// Client Identifier
        /// </summary>
        public string TENANT_ID { get; set; }

        /// <summary>        
        /// Dcu Identifier
        /// </summary>
        public string DCU_ID { get; set; }

        /// <summary>
        /// Date that DCU was installed on Star network
        /// </summary>
        public string DATE_INSTALLED { get; set; }

        /// <summary>
        /// The DCU longitude coordinate.
        /// </summary>
        public string LONGITUDE { get; set; }

        /// <summary>
        /// The DCU latitude coordinate.
        /// </summary>
        public string LATITUDE { get; set; }

        /// <summary>
        /// Indicates the name of DCU
        /// </summary>
        public string NAME { get; set; }
    }
}