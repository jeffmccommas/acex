﻿namespace CE.AO.Models.RawFileModels
{
    public class RawAccountUpdatesModel : IRawFileModel
    {
        public string ClientId { get; set; }
        public string CustomerId { get; set; }
        public string AccountId { get; set; }
        public string PremiseId { get; set; }
        public string MeterId { get; set; }
        public string ServiceContractId { get; set; }
        public string UpdateType { get; set; }
        public string OldValue { get; set; }
        public string NewValue { get; set; }
        public string UpdateSentDate { get; set; }

    }
}
