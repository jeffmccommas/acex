﻿namespace CE.AO.Models.RawFileModels
{
    public class RawDcuAlarmModel : IRawFileModel
    {
        /// <summary>
        /// Data Consumption Unit
        /// </summary>
        public string DCU_ID { get; set; }

        /// <summary>
        /// Alarm Code for Mapping
        /// </summary>
        public string  ALARM_CODE { get; set; }

        /// <summary>
        /// Alarm Time
        /// </summary>
        public string ALARM_TIME { get; set; }

        /// <summary>
        /// Client Id 
        /// </summary>
        public string  TENANT_ID { get; set; }
    }
}
