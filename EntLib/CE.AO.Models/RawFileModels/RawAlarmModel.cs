﻿
namespace CE.AO.Models.RawFileModels
{
    /// <summary>
    /// Class for Mtu Alarm Excel File Mapping
    /// </summary>
    public class RawAlarmModel : IRawFileModel
    {
        /// <summary>
        /// Alarm Id
        /// </summary>
        public string ALARM_ID { get; set; }

        /// <summary>
        /// ALARM Type Id
        /// </summary>
        public string ALARM_TYPE_ID { get; set; }

        /// <summary>
        /// ALARM GROUP ID
        /// </summary>
        public string ALARM_GROUP_ID { get; set; }

        /// <summary>
        /// ALARM TIME
        /// </summary>
        public string ALARM_TIME { get; set; }

        /// <summary>
        /// ALARM SOURCE ID
        /// </summary>
        public string ALARM_SOURCE_ID { get; set; }

        /// <summary>
        /// ALARM SOURCE TYPE ID
        /// </summary>
        public string ALARM_SOURCE_TYPE_ID { get; set; }

        /// <summary>
        /// DETAILS of ALarm
        /// </summary>
        public string DETAILS { get; set; }

        /// <summary>
        /// Client Id
        /// </summary>
        public string TENANT_ID { get; set; }

    }
}
