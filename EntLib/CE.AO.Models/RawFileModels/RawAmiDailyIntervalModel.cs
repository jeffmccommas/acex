﻿namespace CE.AO.Models.RawFileModels
{
    public class RawAmiDailyIntervalModel : IRawFileModel
    {
        public string ClientId { get; set; }
        public string AccountNumber { get; set; }
        public string MeterId { get; set; }
        public string ServicePointId { get; set; }
        public string CommodityId { get; set; }
        // ReSharper disable once InconsistentNaming
        public string UOMId { get; set; }
        public string VolumeFactor { get; set; }
        public string Direction { get; set; }
        // ReSharper disable once InconsistentNaming
        public string TOUID { get; set; }
        public string ProjectedReadDate { get; set; }
        public string TimeStamp { get; set; }
        public string TimeZone { get; set; }
        public string IntValue0000 { get; set; }
    }
}
