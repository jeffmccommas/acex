﻿using System.Diagnostics.CodeAnalysis;

namespace CE.AO.Models.RawFileModels
{
    [SuppressMessage("ReSharper", "InconsistentNaming")]
    public class RawBaseReadModel : IRawFileModel
    {
        /// <summary>
        /// Meter Identifier
        /// </summary>
        public string METER_ID { get; set; }

        /// <summary>
        /// Transponder Identifier
        /// </summary>
        public string TRANSPONDER_ID { get; set; }

        /// <summary>
        /// Transponder Port
        /// </summary>
        public string TRANSPONDER_PORT { get; set; }

        /// <summary>
        /// Customer Identifier
        /// </summary>
        public string CUSTOMER_ID { get; set; }

        /// <summary>
        /// Meter AMI reading value (consumption)
        /// </summary>
        public string READING_VALUE { get; set; }

        /// <summary>
        /// Unit Of Measure
        /// </summary>
        public string UNIT_OF_MEASURE { get; set; }

        /// <summary>
        /// Indicates what time the reading was taken
        /// </summary>
        public string READING_DATETIME { get; set; }

        /// <summary>
        /// Timezone where the reading was taken
        /// </summary>
        public string TIMEZONE { get; set; }

        /// <summary>
        /// Voltage of the battery
        /// </summary>
        public string BATTERY_VOLTAGE { get; set; }

        /// <summary>
        /// Commodity Identifier
        /// </summary>
        public string COMMODITY_ID { get; set; }
    }
}
