﻿using WindowsAzure.Table.Attributes;

namespace CE.AO.Models
{
    public class ClientAccountModel
    {
        [PartitionKey]
        // ReSharper disable once UnusedAutoPropertyAccessor.Local
        public string PartitionKey { get; private set; }
        [RowKey]
        // ReSharper disable once UnusedAutoPropertyAccessor.Local
        public string RowKey { get; private set; }
        public int ClientId { get; set; }
        public string AccountId { get; set; }
        public string CustomerId { get; set; }
    }
}
