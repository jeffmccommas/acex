﻿using System;
using System.ComponentModel.DataAnnotations;
using CE.AO.Utilities;

namespace CE.AO.Models
{
    public class AmiModel
    {
        [Required]
        public int ClientId { get; set; }
        public string AccountNumber { get; set; }
        [Required]
        public string MeterId { get; set; }
        [Required]
        [EnumDataType(typeof(Enums.CommodityType))]
        public int CommodityId { get; set; }
        // ReSharper disable once InconsistentNaming
        [Required]
        [EnumDataType(typeof(Enums.UomType))]
        public int UOMId { get; set; }
        [Required]
        public DateTime AmiTimeStamp { get; set; }
        [EnumDataType(typeof(Enums.Timezone))]
        public string Timezone { get; set; }
    }
}
