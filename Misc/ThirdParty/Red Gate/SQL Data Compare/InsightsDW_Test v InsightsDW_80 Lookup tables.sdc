<?xml version="1.0" encoding="utf-16" standalone="yes"?>
<!--
SQL Data Compare
SQL Data Compare
Version:11.1.3.23-->
<Project version="3" type="SQLComparisonToolsProject">
  <DataSource1 version="3" type="LiveDatabaseSource">
    <ServerName>acedsql1c0.database.windows.net</ServerName>
    <DatabaseName>InsightsDW_Test</DatabaseName>
    <Username>AclaraCEAdmin@acedsql1c0</Username>
    <SavePassword>True</SavePassword>
    <Password encrypted="1">DjgY3mz+gVwyIhIgppWTyw==</Password>
    <ScriptFolderLocation />
    <MigrationsFolderLocation />
    <IntegratedSecurity>False</IntegratedSecurity>
  </DataSource1>
  <DataSource2 version="3" type="LiveDatabaseSource">
    <ServerName>acedsql1c0.database.windows.net</ServerName>
    <DatabaseName>InsightsDW_80</DatabaseName>
    <Username>AclaraCEAdmin@acedsql1c0</Username>
    <SavePassword>True</SavePassword>
    <Password encrypted="1">DjgY3mz+gVwyIhIgppWTyw==</Password>
    <ScriptFolderLocation />
    <MigrationsFolderLocation />
    <IntegratedSecurity>False</IntegratedSecurity>
  </DataSource2>
  <LastCompared>06/17/2016 14:20:34</LastCompared>
  <Options>Default</Options>
  <InRecycleBin>False</InRecycleBin>
  <Direction>0</Direction>
  <ProjectFilter version="1" type="DifferenceFilter">
    <FilterCaseSensitive>False</FilterCaseSensitive>
    <Filters version="1">
      <None version="1">
        <Include>True</Include>
        <Expression>TRUE</Expression>
      </None>
      <Assembly version="1">
        <Include>True</Include>
        <Expression>TRUE</Expression>
      </Assembly>
      <AsymmetricKey version="1">
        <Include>True</Include>
        <Expression>TRUE</Expression>
      </AsymmetricKey>
      <Certificate version="1">
        <Include>True</Include>
        <Expression>TRUE</Expression>
      </Certificate>
      <Contract version="1">
        <Include>True</Include>
        <Expression>TRUE</Expression>
      </Contract>
      <DdlTrigger version="1">
        <Include>True</Include>
        <Expression>TRUE</Expression>
      </DdlTrigger>
      <Default version="1">
        <Include>True</Include>
        <Expression>TRUE</Expression>
      </Default>
      <ExtendedProperty version="1">
        <Include>True</Include>
        <Expression>TRUE</Expression>
      </ExtendedProperty>
      <EventNotification version="1">
        <Include>True</Include>
        <Expression>TRUE</Expression>
      </EventNotification>
      <FullTextCatalog version="1">
        <Include>True</Include>
        <Expression>TRUE</Expression>
      </FullTextCatalog>
      <FullTextStoplist version="1">
        <Include>True</Include>
        <Expression>TRUE</Expression>
      </FullTextStoplist>
      <Function version="1">
        <Include>True</Include>
        <Expression>TRUE</Expression>
      </Function>
      <MessageType version="1">
        <Include>True</Include>
        <Expression>TRUE</Expression>
      </MessageType>
      <PartitionFunction version="1">
        <Include>True</Include>
        <Expression>TRUE</Expression>
      </PartitionFunction>
      <PartitionScheme version="1">
        <Include>True</Include>
        <Expression>TRUE</Expression>
      </PartitionScheme>
      <Queue version="1">
        <Include>True</Include>
        <Expression>TRUE</Expression>
      </Queue>
      <Role version="1">
        <Include>True</Include>
        <Expression>TRUE</Expression>
      </Role>
      <Route version="1">
        <Include>True</Include>
        <Expression>TRUE</Expression>
      </Route>
      <Rule version="1">
        <Include>True</Include>
        <Expression>TRUE</Expression>
      </Rule>
      <Schema version="1">
        <Include>True</Include>
        <Expression>TRUE</Expression>
      </Schema>
      <SearchPropertyList version="1">
        <Include>True</Include>
        <Expression>TRUE</Expression>
      </SearchPropertyList>
      <Sequence version="1">
        <Include>True</Include>
        <Expression>TRUE</Expression>
      </Sequence>
      <Service version="1">
        <Include>True</Include>
        <Expression>TRUE</Expression>
      </Service>
      <ServiceBinding version="1">
        <Include>True</Include>
        <Expression>TRUE</Expression>
      </ServiceBinding>
      <StoredProcedure version="1">
        <Include>True</Include>
        <Expression>TRUE</Expression>
      </StoredProcedure>
      <SymmetricKey version="1">
        <Include>True</Include>
        <Expression>TRUE</Expression>
      </SymmetricKey>
      <Synonym version="1">
        <Include>True</Include>
        <Expression>TRUE</Expression>
      </Synonym>
      <Table version="1">
        <Include>True</Include>
        <Expression>TRUE</Expression>
      </Table>
      <User version="1">
        <Include>True</Include>
        <Expression>TRUE</Expression>
      </User>
      <UserDefinedType version="1">
        <Include>True</Include>
        <Expression>TRUE</Expression>
      </UserDefinedType>
      <View version="1">
        <Include>True</Include>
        <Expression>TRUE</Expression>
      </View>
      <XmlSchemaCollection version="1">
        <Include>True</Include>
        <Expression>TRUE</Expression>
      </XmlSchemaCollection>
    </Filters>
  </ProjectFilter>
  <ProjectFilterName />
  <UserNote />
  <SelectedSyncObjects version="1" type="SelectedSyncObjects">
    <Schemas type="ListString" version="2" />
    <Grouping type="ListByte" version="2">
      <value type="Byte">0</value>
      <value type="Byte">0</value>
      <value type="Byte">0</value>
      <value type="Byte">0</value>
      <value type="Byte">0</value>
      <value type="Byte">0</value>
    </Grouping>
    <SelectAll>False</SelectAll>
  </SelectedSyncObjects>
  <SCGroupingStyle>0</SCGroupingStyle>
  <SQLOptions>10</SQLOptions>
  <MappingOptions>82</MappingOptions>
  <ComparisonOptions>0</ComparisonOptions>
  <TableActions type="ArrayList" version="1">
    <value version="1" type="SelectTableEvent">
      <action>SelectColumnAsKey</action>
      <ColumnName>ClientKey:ClientKey</ColumnName>
      <TableName>[dbo].[DimClient]:[dbo].[DimClient]</TableName>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>SelectColumnAsKey</action>
      <ColumnName>CommodityKey:CommodityKey</ColumnName>
      <TableName>[dbo].[DimCommodity]:[dbo].[DimCommodity]</TableName>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>SelectColumnAsKey</action>
      <ColumnName>CountryRegionKey:CountryRegionKey</ColumnName>
      <TableName>[dbo].[DimCountryRegion]:[dbo].[DimCountryRegion]</TableName>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>SelectColumnAsKey</action>
      <ColumnName>DateKey:DateKey</ColumnName>
      <TableName>[dbo].[DimDate]:[dbo].[DimDate]</TableName>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>SelectColumnAsKey</action>
      <ColumnName>SourceKey:SourceKey</ColumnName>
      <TableName>[dbo].[DimSource]:[dbo].[DimSource]</TableName>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>SelectColumnAsKey</action>
      <ColumnName>StateProvinceKey:StateProvinceKey</ColumnName>
      <TableName>[dbo].[DimStateProvince]:[dbo].[DimStateProvince]</TableName>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>SelectColumnAsKey</action>
      <ColumnName>UOMKey:UOMKey</ColumnName>
      <TableName>[dbo].[DimUOM]:[dbo].[DimUOM]</TableName>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>SelectColumnAsKey</action>
      <ColumnName>CityKey:CityKey</ColumnName>
      <TableName>[dbo].[DimCity]:[dbo].[DimCity]</TableName>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>SelectColumnAsKey</action>
      <ColumnName>ChannelKey:ChannelKey</ColumnName>
      <TableName>[dbo].[DimChannel]:[dbo].[DimChannel]</TableName>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>DeselectItem</action>
      <val>[dbo].[DimChannel]:[dbo].[DimChannel]</val>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>SelectColumnAsKey</action>
      <ColumnName>BillPeriodTypeKey:BillPeriodTypeKey</ColumnName>
      <TableName>[dbo].[DimBillPeriodType]:[dbo].[DimBillPeriodType]</TableName>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>SelectColumnAsKey</action>
      <ColumnName>IntervalLengthKey:IntervalLengthKey</ColumnName>
      <TableName>[dbo].[DimMeterIntervalLength]:[dbo].[DimMeterIntervalLength]</TableName>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>SelectColumnAsKey</action>
      <ColumnName>MeterTypeKey:MeterTypeKey</ColumnName>
      <TableName>[dbo].[DimMeterType]:[dbo].[DimMeterType]</TableName>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>SelectColumnAsKey</action>
      <ColumnName>SecviceContractTypeKey:SecviceContractTypeKey</ColumnName>
      <TableName>[dbo].[DimServiceContractType]:[dbo].[DimServiceContractType]</TableName>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>SelectColumnAsKey</action>
      <ColumnName>TimePeriodKey:TimePeriodKey</ColumnName>
      <TableName>[dbo].[DimTimePeriod]:[dbo].[DimTimePeriod]</TableName>
    </value>
  </TableActions>
  <SessionSettings>14</SessionSettings>
  <DCGroupingStyle>0</DCGroupingStyle>
  <SC_DeploymentOptions version="1" type="SC_DeploymentOptions">
    <BackupOptions version="1" type="BackupOptions">
      <BackupProvider>Native</BackupProvider>
      <TypeOfBackup>Full</TypeOfBackup>
      <Folder />
      <Filename />
      <SqbLicenseType>None</SqbLicenseType>
      <SqbVersion>0</SqbVersion>
      <DefaultNativeFolder />
      <DefaultSqbFolder />
      <Password encrypted="1" />
      <NameFileAutomatically>False</NameFileAutomatically>
      <OverwriteIfExists>False</OverwriteIfExists>
      <CompressionLevel>0</CompressionLevel>
      <EncryptionLevel>None</EncryptionLevel>
      <ThreadCount>0</ThreadCount>
      <BackupEnabled>False</BackupEnabled>
    </BackupOptions>
  </SC_DeploymentOptions>
</Project>