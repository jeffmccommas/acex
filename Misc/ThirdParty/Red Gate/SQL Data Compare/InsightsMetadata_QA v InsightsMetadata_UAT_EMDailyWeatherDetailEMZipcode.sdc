<?xml version="1.0" encoding="utf-16" standalone="yes"?>
<!--
SQL Data Compare
SQL Data Compare
Version:11.1.3.23-->
<Project version="3" type="SQLComparisonToolsProject">
  <DataSource1 version="3" type="LiveDatabaseSource">
    <ServerName>azuqsql001</ServerName>
    <DatabaseName>InsightsMetadata</DatabaseName>
    <Username>CeSqlUser</Username>
    <SavePassword>True</SavePassword>
    <Password encrypted="1">nxmLQqGqNiYi+VAoInFPoA==</Password>
    <ScriptFolderLocation />
    <MigrationsFolderLocation />
    <IntegratedSecurity>False</IntegratedSecurity>
  </DataSource1>
  <DataSource2 version="3" type="LiveDatabaseSource">
    <ServerName>azuusql001</ServerName>
    <DatabaseName>InsightsMetadata</DatabaseName>
    <Username>CeSqlUser</Username>
    <SavePassword>True</SavePassword>
    <Password encrypted="1">nxmLQqGqNiYi+VAoInFPoA==</Password>
    <ScriptFolderLocation />
    <MigrationsFolderLocation />
    <IntegratedSecurity>False</IntegratedSecurity>
  </DataSource2>
  <LastCompared>08/06/2015 11:03:53</LastCompared>
  <Options>Default</Options>
  <InRecycleBin>False</InRecycleBin>
  <Direction>0</Direction>
  <ProjectFilter version="1" type="DifferenceFilter">
    <FilterCaseSensitive>False</FilterCaseSensitive>
    <Filters version="1">
      <None version="1">
        <Include>True</Include>
        <Expression>TRUE</Expression>
      </None>
      <Assembly version="1">
        <Include>True</Include>
        <Expression>TRUE</Expression>
      </Assembly>
      <AsymmetricKey version="1">
        <Include>True</Include>
        <Expression>TRUE</Expression>
      </AsymmetricKey>
      <Certificate version="1">
        <Include>True</Include>
        <Expression>TRUE</Expression>
      </Certificate>
      <Contract version="1">
        <Include>True</Include>
        <Expression>TRUE</Expression>
      </Contract>
      <DdlTrigger version="1">
        <Include>True</Include>
        <Expression>TRUE</Expression>
      </DdlTrigger>
      <Default version="1">
        <Include>True</Include>
        <Expression>TRUE</Expression>
      </Default>
      <ExtendedProperty version="1">
        <Include>True</Include>
        <Expression>TRUE</Expression>
      </ExtendedProperty>
      <EventNotification version="1">
        <Include>True</Include>
        <Expression>TRUE</Expression>
      </EventNotification>
      <FullTextCatalog version="1">
        <Include>True</Include>
        <Expression>TRUE</Expression>
      </FullTextCatalog>
      <FullTextStoplist version="1">
        <Include>True</Include>
        <Expression>TRUE</Expression>
      </FullTextStoplist>
      <Function version="1">
        <Include>True</Include>
        <Expression>TRUE</Expression>
      </Function>
      <MessageType version="1">
        <Include>True</Include>
        <Expression>TRUE</Expression>
      </MessageType>
      <PartitionFunction version="1">
        <Include>True</Include>
        <Expression>TRUE</Expression>
      </PartitionFunction>
      <PartitionScheme version="1">
        <Include>True</Include>
        <Expression>TRUE</Expression>
      </PartitionScheme>
      <Queue version="1">
        <Include>True</Include>
        <Expression>TRUE</Expression>
      </Queue>
      <Role version="1">
        <Include>True</Include>
        <Expression>TRUE</Expression>
      </Role>
      <Route version="1">
        <Include>True</Include>
        <Expression>TRUE</Expression>
      </Route>
      <Rule version="1">
        <Include>True</Include>
        <Expression>TRUE</Expression>
      </Rule>
      <Schema version="1">
        <Include>True</Include>
        <Expression>TRUE</Expression>
      </Schema>
      <SearchPropertyList version="1">
        <Include>True</Include>
        <Expression>TRUE</Expression>
      </SearchPropertyList>
      <Sequence version="1">
        <Include>True</Include>
        <Expression>TRUE</Expression>
      </Sequence>
      <Service version="1">
        <Include>True</Include>
        <Expression>TRUE</Expression>
      </Service>
      <ServiceBinding version="1">
        <Include>True</Include>
        <Expression>TRUE</Expression>
      </ServiceBinding>
      <StoredProcedure version="1">
        <Include>True</Include>
        <Expression>TRUE</Expression>
      </StoredProcedure>
      <SymmetricKey version="1">
        <Include>True</Include>
        <Expression>TRUE</Expression>
      </SymmetricKey>
      <Synonym version="1">
        <Include>True</Include>
        <Expression>TRUE</Expression>
      </Synonym>
      <Table version="1">
        <Include>True</Include>
        <Expression>TRUE</Expression>
      </Table>
      <User version="1">
        <Include>True</Include>
        <Expression>TRUE</Expression>
      </User>
      <UserDefinedType version="1">
        <Include>True</Include>
        <Expression>TRUE</Expression>
      </UserDefinedType>
      <View version="1">
        <Include>True</Include>
        <Expression>TRUE</Expression>
      </View>
      <XmlSchemaCollection version="1">
        <Include>True</Include>
        <Expression>TRUE</Expression>
      </XmlSchemaCollection>
    </Filters>
  </ProjectFilter>
  <ProjectFilterName />
  <UserNote />
  <SelectedSyncObjects version="1" type="SelectedSyncObjects">
    <Schemas type="ListString" version="2">
      <value type="string">UgBlAGQARwBhAHQAZQBfAFMAUQBMAEMAbwBtAHAAYQByAGUAXwBEAGUAZgBhAHUAbAB0AF8AUwBjAGgAZQBtAGEA</value>
    </Schemas>
    <UgBlAGQARwBhAHQAZQBfAFMAUQBMAEMAbwBtAHAAYQByAGUAXwBEAGUAZgBhAHUAbAB0AF8AUwBjAGgAZQBtAGEA>V4lv1e067I8aKBLHWsq0SQgCAOUQ0mCesTRnxjZlnqq3b+EIAgDK5431DOkibLfZAmc/Hb4+CAIAct0SUp55lD4VcgypyFWexAgCAPolfIsenJn8f1aLZ/LamswIAgAb4Qtlq5+qEnCazGJyDqEICAIA/Le/q9uMcj7AK98p9Az//QoAAA__</UgBlAGQARwBhAHQAZQBfAFMAUQBMAEMAbwBtAHAAYQByAGUAXwBEAGUAZgBhAHUAbAB0AF8AUwBjAGgAZQBtAGEA>
    <Grouping type="ListByte" version="2">
      <value type="Byte">0</value>
      <value type="Byte">0</value>
      <value type="Byte">0</value>
      <value type="Byte">0</value>
      <value type="Byte">0</value>
      <value type="Byte">0</value>
    </Grouping>
    <SelectAll>False</SelectAll>
  </SelectedSyncObjects>
  <SCGroupingStyle>0</SCGroupingStyle>
  <SQLOptions>10</SQLOptions>
  <MappingOptions>82</MappingOptions>
  <ComparisonOptions>0</ComparisonOptions>
  <TableActions type="ArrayList" version="1">
    <value version="1" type="SelectTableEvent">
      <action>DeselectItem</action>
      <val>[dbo].[Client]:[dbo].[Client]</val>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>DeselectItem</action>
      <val>[dbo].[ClientFilePurgePolicy]:[dbo].[ClientFilePurgePolicy]</val>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>DeselectItem</action>
      <val>[dbo].[ClientPropertyData]:[dbo].[ClientPropertyData]</val>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>DeselectItem</action>
      <val>[dbo].[UtilityPrograms]:[dbo].[UtilityPrograms]</val>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>DeselectItem</action>
      <val>[admin].[AppTaskHeartbeat]:[admin].[AppTaskHeartbeat]</val>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>DeselectItem</action>
      <val>[admin].[AppTaskRunStatus]:[admin].[AppTaskRunStatus]</val>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>DeselectItem</action>
      <val>[admin].[AppTasks]:[admin].[AppTasks]</val>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>DeselectItem</action>
      <val>[admin].[CustomerExportConfiguration]:[admin].[CustomerExportConfiguration]</val>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>DeselectItem</action>
      <val>[admin].[ETLBulkConfiguration]:[admin].[ETLBulkConfiguration]</val>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>DeselectItem</action>
      <val>[admin].[ETLInfo]:[admin].[ETLInfo]</val>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>DeselectItem</action>
      <val>[audit].[ActionItemImportErrors]:[audit].[ActionItemImportErrors]</val>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>DeselectItem</action>
      <val>[audit].[BillImportErrors]:[audit].[BillImportErrors]</val>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>DeselectItem</action>
      <val>[audit].[CustomerImportErrors]:[audit].[CustomerImportErrors]</val>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>DeselectItem</action>
      <val>[audit].[DisAggErrorLog]:[audit].[DisAggErrorLog]</val>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>DeselectItem</action>
      <val>[audit].[ExecutionLog]:[audit].[ExecutionLog]</val>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>DeselectItem</action>
      <val>[audit].[MeasureSavingsErrorLog]:[audit].[MeasureSavingsErrorLog]</val>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>DeselectItem</action>
      <val>[audit].[ProfileImportErrors]:[audit].[ProfileImportErrors]</val>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>DeselectItem</action>
      <val>[cm].[DatabaseDiagrams]:[cm].[DatabaseDiagrams]</val>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>DeselectItem</action>
      <val>[cm].[EMApplianceRegion]:[cm].[EMApplianceRegion]</val>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>DeselectItem</action>
      <val>[cm].[EMDegreeDay]:[cm].[EMDegreeDay]</val>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>DeselectItem</action>
      <val>[cm].[EMEvaporation]:[cm].[EMEvaporation]</val>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>DeselectItem</action>
      <val>[cm].[EMSolar]:[cm].[EMSolar]</val>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>DeselectItem</action>
      <val>[cm].[ContentCache]:[cm].[ContentCache]</val>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>DeselectItem</action>
      <val>[cm].[EMTemperatureProfile]:[cm].[EMTemperatureProfile]</val>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>DeselectItem</action>
      <val>[cm].[EMTemperatureRegion]:[cm].[EMTemperatureRegion]</val>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>DeselectItem</action>
      <val>[cm].[EnvironmentConfiguration]:[cm].[EnvironmentConfiguration]</val>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>DeselectItem</action>
      <val>[cm].[ClientActionAppliance]:[cm].[ClientActionAppliance]</val>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>DeselectItem</action>
      <val>[cm].[ClientAction]:[cm].[ClientAction]</val>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>DeselectItem</action>
      <val>[cm].[ClientActionCondition]:[cm].[ClientActionCondition]</val>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>DeselectItem</action>
      <val>[cm].[ClientActionSavings]:[cm].[ClientActionSavings]</val>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>DeselectItem</action>
      <val>[cm].[ClientActionSavingsCondition]:[cm].[ClientActionSavingsCondition]</val>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>DeselectItem</action>
      <val>[cm].[ClientActionSeason]:[cm].[ClientActionSeason]</val>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>DeselectItem</action>
      <val>[cm].[ClientActionWhatIfData]:[cm].[ClientActionWhatIfData]</val>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>DeselectItem</action>
      <val>[cm].[ClientAppliance]:[cm].[ClientAppliance]</val>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>DeselectItem</action>
      <val>[cm].[ClientApplianceExpression]:[cm].[ClientApplianceExpression]</val>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>DeselectItem</action>
      <val>[cm].[ClientApplianceProfileAttribute]:[cm].[ClientApplianceProfileAttribute]</val>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>DeselectItem</action>
      <val>[cm].[ClientAsset]:[cm].[ClientAsset]</val>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>DeselectItem</action>
      <val>[cm].[ClientAssetLocaleContent]:[cm].[ClientAssetLocaleContent]</val>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>DeselectItem</action>
      <val>[cm].[ClientBenchmarkGroup]:[cm].[ClientBenchmarkGroup]</val>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>DeselectItem</action>
      <val>[cm].[ClientCommodity]:[cm].[ClientCommodity]</val>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>DeselectItem</action>
      <val>[cm].[ClientCondition]:[cm].[ClientCondition]</val>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>DeselectItem</action>
      <val>[cm].[ClientConfiguration]:[cm].[ClientConfiguration]</val>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>DeselectItem</action>
      <val>[cm].[ClientCurrency]:[cm].[ClientCurrency]</val>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>DeselectItem</action>
      <val>[cm].[ClientEnduse]:[cm].[ClientEnduse]</val>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>DeselectItem</action>
      <val>[cm].[ClientEnduseAppliance]:[cm].[ClientEnduseAppliance]</val>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>DeselectItem</action>
      <val>[cm].[ClientEnumeration]:[cm].[ClientEnumeration]</val>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>DeselectItem</action>
      <val>[cm].[ClientEnumerationEnumerationItem]:[cm].[ClientEnumerationEnumerationItem]</val>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>DeselectItem</action>
      <val>[cm].[ClientEnumerationItem]:[cm].[ClientEnumerationItem]</val>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>DeselectItem</action>
      <val>[cm].[ClientExpression]:[cm].[ClientExpression]</val>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>DeselectItem</action>
      <val>[cm].[ClientFileContent]:[cm].[ClientFileContent]</val>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>DeselectItem</action>
      <val>[cm].[ClientLayout]:[cm].[ClientLayout]</val>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>DeselectItem</action>
      <val>[cm].[ClientMeasurement]:[cm].[ClientMeasurement]</val>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>DeselectItem</action>
      <val>[cm].[ClientProfileAttribute]:[cm].[ClientProfileAttribute]</val>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>DeselectItem</action>
      <val>[cm].[ClientProfileAttributeCondition]:[cm].[ClientProfileAttributeCondition]</val>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>DeselectItem</action>
      <val>[cm].[ClientProfileAttributeProfileOption]:[cm].[ClientProfileAttributeProfileOption]</val>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>DeselectItem</action>
      <val>[cm].[ClientProfileDefault]:[cm].[ClientProfileDefault]</val>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>DeselectItem</action>
      <val>[cm].[ClientProfileDefaultCollection]:[cm].[ClientProfileDefaultCollection]</val>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>DeselectItem</action>
      <val>[cm].[ClientProfileOption]:[cm].[ClientProfileOption]</val>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>DeselectItem</action>
      <val>[cm].[ClientProfileSection]:[cm].[ClientProfileSection]</val>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>DeselectItem</action>
      <val>[cm].[ClientProfileSectionAttribute]:[cm].[ClientProfileSectionAttribute]</val>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>DeselectItem</action>
      <val>[cm].[ClientProfileSectionCondition]:[cm].[ClientProfileSectionCondition]</val>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>DeselectItem</action>
      <val>[cm].[ClientSeason]:[cm].[ClientSeason]</val>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>DeselectItem</action>
      <val>[cm].[ClientTab]:[cm].[ClientTab]</val>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>DeselectItem</action>
      <val>[cm].[ClientTabAction]:[cm].[ClientTabAction]</val>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>DeselectItem</action>
      <val>[cm].[ClientTabChildTab]:[cm].[ClientTabChildTab]</val>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>DeselectItem</action>
      <val>[cm].[ClientTabCondition]:[cm].[ClientTabCondition]</val>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>DeselectItem</action>
      <val>[cm].[ClientTabConfiguration]:[cm].[ClientTabConfiguration]</val>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>DeselectItem</action>
      <val>[cm].[ClientTabProfileSection]:[cm].[ClientTabProfileSection]</val>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>DeselectItem</action>
      <val>[cm].[ClientTabTextContent]:[cm].[ClientTabTextContent]</val>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>DeselectItem</action>
      <val>[cm].[ClientTextContent]:[cm].[ClientTextContent]</val>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>DeselectItem</action>
      <val>[cm].[ClientTextContentLocaleContent]:[cm].[ClientTextContentLocaleContent]</val>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>DeselectItem</action>
      <val>[cm].[ClientUOM]:[cm].[ClientUOM]</val>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>DeselectItem</action>
      <val>[cm].[ClientWhatIfData]:[cm].[ClientWhatIfData]</val>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>DeselectItem</action>
      <val>[cm].[ClientWidget]:[cm].[ClientWidget]</val>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>DeselectItem</action>
      <val>[cm].[ClientWidgetCondition]:[cm].[ClientWidgetCondition]</val>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>DeselectItem</action>
      <val>[cm].[ClientWidgetConfiguration]:[cm].[ClientWidgetConfiguration]</val>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>DeselectItem</action>
      <val>[cm].[ClientWidgetTextContent]:[cm].[ClientWidgetTextContent]</val>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>DeselectItem</action>
      <val>[cm].[TypeAction]:[cm].[TypeAction]</val>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>DeselectItem</action>
      <val>[cm].[TypeActionStatus]:[cm].[TypeActionStatus]</val>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>DeselectItem</action>
      <val>[cm].[TypeActionType]:[cm].[TypeActionType]</val>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>DeselectItem</action>
      <val>[cm].[TypeAppliance]:[cm].[TypeAppliance]</val>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>DeselectItem</action>
      <val>[cm].[TypeAsset]:[cm].[TypeAsset]</val>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>DeselectItem</action>
      <val>[cm].[TypeBenchmarkGroup]:[cm].[TypeBenchmarkGroup]</val>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>DeselectItem</action>
      <val>[cm].[TypeCategory]:[cm].[TypeCategory]</val>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>DeselectItem</action>
      <val>[cm].[TypeCommodity]:[cm].[TypeCommodity]</val>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>DeselectItem</action>
      <val>[cm].[TypeCondition]:[cm].[TypeCondition]</val>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>DeselectItem</action>
      <val>[cm].[TypeConfiguration]:[cm].[TypeConfiguration]</val>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>DeselectItem</action>
      <val>[cm].[TypeCurrency]:[cm].[TypeCurrency]</val>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>DeselectItem</action>
      <val>[cm].[TypeDifficulty]:[cm].[TypeDifficulty]</val>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>DeselectItem</action>
      <val>[cm].[TypeEnduse]:[cm].[TypeEnduse]</val>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>DeselectItem</action>
      <val>[cm].[TypeEnduseCategory]:[cm].[TypeEnduseCategory]</val>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>DeselectItem</action>
      <val>[cm].[TypeEntityLevel]:[cm].[TypeEntityLevel]</val>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>DeselectItem</action>
      <val>[cm].[TypeEnumeration]:[cm].[TypeEnumeration]</val>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>DeselectItem</action>
      <val>[cm].[TypeEnumerationItem]:[cm].[TypeEnumerationItem]</val>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>DeselectItem</action>
      <val>[cm].[TypeEnvironment]:[cm].[TypeEnvironment]</val>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>DeselectItem</action>
      <val>[cm].[TypeExpression]:[cm].[TypeExpression]</val>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>DeselectItem</action>
      <val>[cm].[TypeFileContent]:[cm].[TypeFileContent]</val>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>DeselectItem</action>
      <val>[cm].[TypeHabitInterval]:[cm].[TypeHabitInterval]</val>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>DeselectItem</action>
      <val>[cm].[TypeLayout]:[cm].[TypeLayout]</val>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>DeselectItem</action>
      <val>[cm].[TypeLocale]:[cm].[TypeLocale]</val>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>DeselectItem</action>
      <val>[cm].[TypeMeasurement]:[cm].[TypeMeasurement]</val>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>DeselectItem</action>
      <val>[cm].[TypeProfileAttribute]:[cm].[TypeProfileAttribute]</val>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>DeselectItem</action>
      <val>[cm].[TypeProfileAttributeType]:[cm].[TypeProfileAttributeType]</val>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>DeselectItem</action>
      <val>[cm].[TypeProfileDefault]:[cm].[TypeProfileDefault]</val>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>DeselectItem</action>
      <val>[cm].[TypeProfileDefaultCollection]:[cm].[TypeProfileDefaultCollection]</val>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>DeselectItem</action>
      <val>[cm].[TypeProfileOption]:[cm].[TypeProfileOption]</val>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>DeselectItem</action>
      <val>[cm].[TypeProfileSection]:[cm].[TypeProfileSection]</val>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>DeselectItem</action>
      <val>[cm].[TypeProfileSource]:[cm].[TypeProfileSource]</val>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>DeselectItem</action>
      <val>[cm].[TypeQuality]:[cm].[TypeQuality]</val>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>DeselectItem</action>
      <val>[cm].[TypeSeason]:[cm].[TypeSeason]</val>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>DeselectItem</action>
      <val>[cm].[TypeTab]:[cm].[TypeTab]</val>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>DeselectItem</action>
      <val>[cm].[TypeTabType]:[cm].[TypeTabType]</val>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>DeselectItem</action>
      <val>[cm].[TypeTextContent]:[cm].[TypeTextContent]</val>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>DeselectItem</action>
      <val>[cm].[TypeUom]:[cm].[TypeUom]</val>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>DeselectItem</action>
      <val>[cm].[TypeWhatIfData]:[cm].[TypeWhatIfData]</val>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>DeselectItem</action>
      <val>[cm].[TypeWidget]:[cm].[TypeWidget]</val>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>DeselectItem</action>
      <val>[cm].[TypeWidgetType]:[cm].[TypeWidgetType]</val>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>SelectItem</action>
      <val>[cm].[EMDailyWeatherDetail]:[cm].[EMDailyWeatherDetail]</val>
    </value>
    <value version="1" type="SelectTableEvent">
      <action>SelectItem</action>
      <val>[cm].[EMZipcode]:[cm].[EMZipcode]</val>
    </value>
  </TableActions>
  <SessionSettings>15</SessionSettings>
  <DCGroupingStyle>0</DCGroupingStyle>
  <SC_DeploymentOptions version="1" type="SC_DeploymentOptions">
    <BackupOptions version="1" type="BackupOptions">
      <BackupProvider>Native</BackupProvider>
      <TypeOfBackup>Full</TypeOfBackup>
      <Folder>\\NONPRDSQLBU\SQLBackup\UAT\AZUUSQL001BI1</Folder>
      <Filename />
      <SqbLicenseType>None</SqbLicenseType>
      <SqbVersion>0</SqbVersion>
      <DefaultNativeFolder>\\NONPRDSQLBU\SQLBackup\UAT\AZUUSQL001BI1</DefaultNativeFolder>
      <DefaultSqbFolder />
      <Password encrypted="1" />
      <NameFileAutomatically>False</NameFileAutomatically>
      <OverwriteIfExists>False</OverwriteIfExists>
      <CompressionLevel>0</CompressionLevel>
      <EncryptionLevel>None</EncryptionLevel>
      <ThreadCount>0</ThreadCount>
      <BackupEnabled>False</BackupEnabled>
    </BackupOptions>
  </SC_DeploymentOptions>
</Project>