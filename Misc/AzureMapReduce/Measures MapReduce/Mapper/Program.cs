﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CE.MeasuresEngine;
using CE.ContentModel;

using System.Data;
using System.Data.SqlClient;
using System.Configuration;

namespace Mapper
{
    #region Measure Engine Wrapper
    /// <summary>
    /// 
    /// </summary>
    internal class MyMeasure
    {        
        private MeasuresEngineManagerFactory _factory = null;
        private IMeasuresEngine _manager = null;        
        
        ////*************************************************************
        ////This needs to be changed to args or read them from config table
        //int _ClientID = 224;
        //string _InsightsDWCnString = @"Data Source=tcp:aceusql1c0.database.windows.net,1433;Initial Catalog=INSIGHTSDW_224;Integrated Security=False;User ID=AclaraCEAdmin@aceUsql1c0;Password=Acl@r@282;Connect Timeout=120;Encrypt=True";
        ////string _InsightsBulkCnString = @"Data Source=tcp:acedsql1c0.database.windows.net,1433;Initial Catalog=INSIGHTSBULK;Integrated Security=False;User ID=AclaraCEAdmin@aceDsql1c0;Password=Acl@r@282;Connect Timeout=120;Encrypt=True";
        //string _InsightsMetaDataCnString = @"Data Source=tcp:aceUsql1c0.database.windows.net,1433;Initial Catalog=INSIGHTSMETADATA;Integrated Security=False;User ID=AclaraCEAdmin@aceUsql1c0;Password=Acl@r@282;Connect Timeout=120;Encrypt=True";
        ////*************************************************************

        private int _clientId = int.Parse(ConfigurationManager.AppSettings["CLIENTID"].ToString());
        private string _insightsDWCnString = ConfigurationManager.AppSettings["INSIGHTSDW"].ToString();
        private string _insightsMetaDataCnString = ConfigurationManager.AppSettings["INSIGHTSMETADATA"].ToString();

        /// <summary>
        /// this data table contains the results after a call to the measure's engine is made.  If the call to the engine fails
        /// the data table will be empty (0 rows).
        /// </summary>
        public DataTable Data { get; private set; }

        public MyMeasure()
        {
            new AutoMapperBootStrapper().BootStrap();
            _factory = new MeasuresEngineManagerFactory();


            _manager = _factory.CreateMeasuresEngineManager(_clientId, _insightsDWCnString, _insightsMetaDataCnString, string.Empty);

            BuildOutputDataTable();

        }
        
        /// <summary>
        /// process the input row for teh customer/account/premise
        /// </summary>
        /// <param name="customerId"></param>
        /// <param name="accountId"></param>
        /// <param name="premiseId"></param>
        /// <returns>true if successfully processed; otherwise returns false</returns>
        public bool ProcessInputRow(string customerId, string accountId, string premiseId)
        {
            bool success = false;       // assume failure unless told otherwise

            try
            {
                // clear our internal table ... if we fail our table will be empty
                Data.Rows.Clear();

                MeasuresResult result = _manager.ExecuteMeasures(customerId, accountId, premiseId);

                // copy the results returned from the engine to our own table
                DataTable returned_data = result.FlatResultTable;

                foreach (DataRow row in returned_data.Rows)
                {
                    Data.ImportRow(row);
                }


                // we got this far...everything is OK
                success = true;
            }
            catch (Exception excp)
            {
                // log it to std error                
                Logger.LogError(
                    string.Format(
                        @"failed to process input row.  CustomerId:""{0}""; AccountId:""{1}""; PremiseId:""{2}""",
                        customerId, accountId, premiseId
                    ),
                    excp
                );
            }

            return success;
        }

        /// <summary>
        /// build the data table so that we have the column definitions
        /// </summary>
        private void BuildOutputDataTable()
        {

            Data = new DataTable();

            // This matches the schema of the user-defined table type
            Data.Columns.Add("ClientId", typeof(int));         //PK
            Data.Columns.Add("CustomerId", typeof(string));    //PK
            Data.Columns.Add("AccountId", typeof(string));     //PK
            Data.Columns.Add("PremiseId", typeof(string));     //PK

            Data.Columns.Add("ActionKey", typeof(string));
            Data.Columns.Add("ActionTypeKey", typeof(string));

            Data.Columns.Add("AnnualCost", typeof(decimal));
            Data.Columns.Add("AnnualCostVariancePercent", typeof(decimal));
            Data.Columns.Add("AnnualSavingsEstimate", typeof(decimal));
            Data.Columns.Add("AnnualSavingsEstimateCurrencyKey", typeof(string));

            Data.Columns.Add("Roi", typeof(decimal));
            Data.Columns.Add("Payback", typeof(decimal));
            Data.Columns.Add("UpfrontCost", typeof(decimal));
            Data.Columns.Add("CommodityKey", typeof(string));

            Data.Columns.Add("ElecSavEst", typeof(decimal));
            Data.Columns.Add("ElecSavEstCurrencyKey", typeof(string));
            Data.Columns.Add("ElecUsgSavEst", typeof(decimal));
            Data.Columns.Add("ElecUsgSavEstUomKey", typeof(string));

            Data.Columns.Add("GasSavEst", typeof(decimal));
            Data.Columns.Add("GasSavEstCurrencyKey", typeof(string));
            Data.Columns.Add("GasUsgSavEst", typeof(decimal));
            Data.Columns.Add("GasUsgSavEstUomKey", typeof(string));

            Data.Columns.Add("WaterSavEst", typeof(decimal));
            Data.Columns.Add("WaterSavEstCurrencyKey", typeof(string));
            Data.Columns.Add("WaterUsgSavEst", typeof(decimal));
            Data.Columns.Add("WaterUsgSavEstUomKey", typeof(string));

            Data.Columns.Add("Priority", typeof(int));

            Data.Columns.Add("NewDate", typeof(DateTime));

            Data.Columns.Add("RebateAmount", typeof(decimal));

        }
    }
    #endregion

    #region logging
    /// <summary>
    /// rudimentary logger
    /// </summary>
    static class Logger
    {
        /// <summary>
        /// write error message
        /// </summary>
        /// <param name="msg"></param>
        public static void LogError(string msg, Exception excp)
        {
            Console.Error.WriteLine(@"[{0}] [ERROR:""{1}""] [{2}] details to follow...", DateTime.Now, msg, excp.Message);
            Console.Error.WriteLine(@"[{0}] [{1}]", DateTime.Now, excp);
        }
    }
    #endregion

    #region Mainline

    class Program
    {

        /// <summary>
        /// this is used to build the output line.  we are attempting to reduce the number of memory allocations required to build a string
        /// </summary>
        static StringBuilder _lineBuilder = new StringBuilder(512);    // preallocate 512 bytes

        /// <summary>
        /// 
        /// </summary>
        /// <param name="args"></param>
        static void Main(string[] args)
        {   
            
            if (args.Length > 0)
            {
                Console.SetIn(new StreamReader(args[0]));
            }
          //  Console.SetIn(new StreamReader("testinput.txt"));
            
            MyMeasure myclass = new Mapper.MyMeasure();


            string line;
            string[] values;
            int rowCount = 0;
            int processingErrorCount = 0;            
            int writeErrorCount= 0;

            while (((line = Console.ReadLine()) != null))
            {
                //line = Console.ReadLine(); //test
                rowCount++;
                values = line.Split('\t');

                string clientId = values[0];
                string premiseId = values[1];
                string accountId = values[2];
                string customerId = values[3];


                if (myclass.ProcessInputRow(customerId, accountId, premiseId))
                {
                    foreach (DataRow row in myclass.Data.Rows)
                    {
                        if (!WriteRow(row))
                        {
                            writeErrorCount++;
                        }
                    }
                }
                else
                {
                    processingErrorCount++;
                }

            }            
        }

        /// <summary>
        /// write the row to console.  
        /// </summary>
        /// <param name="row"></param>
        /// <returns>true if successful; otherwise false</returns>
        private static bool WriteRow(DataRow row)
        {
            bool success = false;   // assume false unless told otherwise
            try
            {
                            
                _lineBuilder.Clear();

                /*  1 */ _lineBuilder.AppendFormat("{0}", row["ClientId"] == DBNull.Value ? String.Empty : row["ClientId"]);
                /*  2 */ _lineBuilder.AppendFormat("\t{0}", row["CustomerId"] == DBNull.Value ? String.Empty : row["CustomerId"]);
                /*  3 */ _lineBuilder.AppendFormat("\t{0}", row["AccountId"] == DBNull.Value ? String.Empty : row["AccountId"]);
                /*  4 */ _lineBuilder.AppendFormat("\t{0}", row["PremiseId"] == DBNull.Value ? String.Empty : row["PremiseId"]);

                /*  5 */ _lineBuilder.AppendFormat("\t{0}", row["ActionKey"] == DBNull.Value ? String.Empty : row["ActionKey"]);
                /*  6 */ _lineBuilder.AppendFormat("\t{0}", row["ActionTypeKey"] == DBNull.Value ? String.Empty : row["ActionTypeKey"]);

                /*  7 */ _lineBuilder.AppendFormat("\t{0}", row["AnnualCost"] == DBNull.Value ? String.Empty : row["AnnualCost"]);
                /*  8 */ _lineBuilder.AppendFormat("\t{0}", row["AnnualCostVariancePercent"] == DBNull.Value ? String.Empty : row["AnnualCostVariancePercent"]);
                /*  9 */ _lineBuilder.AppendFormat("\t{0}", row["AnnualSavingsEstimate"] == DBNull.Value ? String.Empty : row["AnnualSavingsEstimate"]);
                /* 10 */ _lineBuilder.AppendFormat("\t{0}", row["AnnualSavingsEstimateCurrencyKey"] == DBNull.Value ? String.Empty : row["AnnualSavingsEstimateCurrencyKey"]);

                /* 11 */ _lineBuilder.AppendFormat("\t{0}", row["Roi"] == DBNull.Value ? String.Empty : row["Roi"]);
                /* 12 */ _lineBuilder.AppendFormat("\t{0}", row["Payback"] == DBNull.Value ? String.Empty : row["Payback"]);
                /* 13 */ _lineBuilder.AppendFormat("\t{0}", row["UpfrontCost"] == DBNull.Value ? String.Empty : row["UpfrontCost"]);
                /* 14 */ _lineBuilder.AppendFormat("\t{0}", row["CommodityKey"] == DBNull.Value ? String.Empty : row["CommodityKey"]);

                /* 15 */ _lineBuilder.AppendFormat("\t{0}", row["ElecSavEst"] == DBNull.Value ? String.Empty : row["ElecSavEst"]);
                /* 16 */ _lineBuilder.AppendFormat("\t{0}", row["ElecSavEstCurrencyKey"] == DBNull.Value ? String.Empty : row["ElecSavEstCurrencyKey"]);
                /* 17 */ _lineBuilder.AppendFormat("\t{0}", row["ElecUsgSavEst"] == DBNull.Value ? String.Empty : row["ElecUsgSavEst"]);
                /* 18 */ _lineBuilder.AppendFormat("\t{0}", row["ElecUsgSavEstUomKey"] == DBNull.Value ? String.Empty : row["ElecUsgSavEstUomKey"]);

                /* 19 */ _lineBuilder.AppendFormat("\t{0}", row["GasSavEst"] == DBNull.Value ? String.Empty : row["GasSavEst"]);
                /* 20 */ _lineBuilder.AppendFormat("\t{0}", row["GasSavEstCurrencyKey"] == DBNull.Value ? String.Empty : row["GasSavEstCurrencyKey"]);
                /* 21 */ _lineBuilder.AppendFormat("\t{0}", row["GasUsgSavEst"] == DBNull.Value ? String.Empty : row["GasUsgSavEst"]);
                /* 22 */ _lineBuilder.AppendFormat("\t{0}", row["GasUsgSavEstUomKey"] == DBNull.Value ? String.Empty : row["GasUsgSavEstUomKey"]);

                /* 23 */ _lineBuilder.AppendFormat("\t{0}", row["WaterSavEst"] == DBNull.Value ? String.Empty : row["WaterSavEst"]);
                /* 24 */ _lineBuilder.AppendFormat("\t{0}", row["WaterSavEstCurrencyKey"] == DBNull.Value ? String.Empty : row["WaterSavEstCurrencyKey"]);
                /* 25 */ _lineBuilder.AppendFormat("\t{0}", row["WaterUsgSavEst"] == DBNull.Value ? String.Empty : row["WaterUsgSavEst"]);
                /* 26 */ _lineBuilder.AppendFormat("\t{0}", row["WaterUsgSavEstUomKey"] == DBNull.Value ? String.Empty : row["WaterUsgSavEstUomKey"]);

                /* 27 */ _lineBuilder.AppendFormat("\t{0}", row["Priority"] == DBNull.Value ? String.Empty : row["Priority"]);
                /* 28 */ _lineBuilder.AppendFormat("\t{0:s}", row["NewDate"] == DBNull.Value ? String.Empty : row["NewDate"]);
                /* 29 */ _lineBuilder.AppendFormat("\t{0}", row["RebateAmount"] == DBNull.Value ? String.Empty : row["RebateAmount"]);

                Console.WriteLine(_lineBuilder.ToString());
                success = true;
            }
            catch (Exception excp)
            {
                // log it to std error                
                Logger.LogError("failed to write output.", excp);
            }

            return success;

        }
    }

    #endregion
}
