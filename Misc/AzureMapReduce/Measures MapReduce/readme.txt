This solution contains the projects required to build the map/reduce binaries to calculate measures.

Update ACE binaries
-------------------
  Whenever there is a new release of the ACE measure binaries you will need to do the following:
    + copy the dll(s) to their respective debug/release folder under "Measures MapReduce\ThirdParty\ACE\{Debug, Release}"
    + build the debug and/or release version of the mapper and reducer.
  
Local Testing
-------------
  You can test the mapper by running it using Visual Studio.  It has been setup to use the TestData\TestInput.txt file.  This fill contains the accounts that
  can be used for testing...adjust the contents of the file as you see fit.

Azure Cloud Deployment
----------------------
  The build will copy the binaries that are to be deployed to the azure cloud under "Measures MapReduce\Deploy\{Debug, Release}" folder.  To deploy to the azure cloud
  you should copy the contents of either the Deploy\Debug or Deploy\Release (preferrable Release) to the appropriate blob container used by the ADF Activity that
  performs the measures map/reduce.  How do to this is beyond the scope of this readme file; However, DO NOT COPY the mapper.exe.config or the reducer.exe.config to 
  the azure cloud!!!

Checkin
-------
  When you checkin your modifications you will be checkin in your modifications (if any) along with the updated ACE
  binaries in "Measures MapReduce\ThirdParty\ACE\{Debug, Release}".

Misc
----
  + do not add any unecessary references unless the dll and/or package is required to build the Mapper or Reducer
  + if assemblies are required by the ACE components used by the Mapper or Redecure they should be provided as part of the ACE build and 
    copied to the appopriate "Measures MapReduce\ThirdParty\ACE\{Debug, Release}" folder
	- NOTE: Ensure that if you add a new dll that you also add it to TFS.