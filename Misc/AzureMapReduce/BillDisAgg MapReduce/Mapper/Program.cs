﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CE.BillDisagg;
using CE.ContentModel;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

namespace Mapper
{
    //static class mystuff
    //{
    //    public static void UploadText(this ICloudBlob blob, string text)
    //    {
    //        var bytesToUpload = Encoding.UTF8.GetBytes(text);

    //        using (var ms = new MemoryStream(bytesToUpload))
    //        {
    //            blob.UploadFromStream(ms);
    //        }

    //    }
    //}
        
    internal class MyDisagg
    {
        int verbose = 0;
        internal int disagg_error = 0;
        internal int disagg_error_count = 0;
        internal int disagg_successful_count = 0;
        internal BillDisaggManagerFactory factory = null;
        IBillDisagg manager = null;
        BillDisaggResult result = null;
        internal DataTable data = null;
        ////*************************************************************
        ////This needs to be changed to args or read them from config table
        //int _ClientID = 224;
        //string _InsightsDWCnString = @"Data Source=tcp:aceusql1c0.database.windows.net,1433;Initial Catalog=INSIGHTSDW_224;Integrated Security=False;User ID=AclaraCEAdmin@aceUsql1c0;Password=Acl@r@282;Connect Timeout=120;Encrypt=True";
        ////string _InsightsBulkCnString = @"Data Source=tcp:acedsql1c0.database.windows.net,1433;Initial Catalog=INSIGHTSBULK;Integrated Security=False;User ID=AclaraCEAdmin@aceDsql1c0;Password=Acl@r@282;Connect Timeout=120;Encrypt=True";
        //string _InsightsMetaDataCnString = @"Data Source=tcp:aceUsql1c0.database.windows.net,1433;Initial Catalog=INSIGHTSMETADATA;Integrated Security=False;User ID=AclaraCEAdmin@aceUsql1c0;Password=Acl@r@282;Connect Timeout=120;Encrypt=True";
        ////*************************************************************

        int _ClientID = int.Parse(ConfigurationManager.AppSettings["CLIENTID"].ToString());
        string _InsightsDWCnString = ConfigurationManager.AppSettings["INSIGHTSDW"].ToString();
        string _InsightsMetaDataCnString = ConfigurationManager.AppSettings["INSIGHTSMETADATA"].ToString();

        public MyDisagg()
        {

            new AutoMapperBootStrapper().BootStrap();
            factory = new BillDisaggManagerFactory();


            manager = factory.CreateBillDisaggManager(_ClientID, _InsightsDWCnString, _InsightsMetaDataCnString,
                                                                string.Empty);

        }

        internal void ProcessInputRow(string customerId, string accountId, string premiseId)
        {

            try
            {
                disagg_error = 0;
                result = manager.ExecuteBillDisagg(customerId, accountId, premiseId);
                disagg_successful_count++;
            }
            catch (Exception ex)
            {

                
                data = null;
                disagg_error = 1;
                disagg_error_count++;
                return;
            }
            disagg_error = 0;
            var returned_data = result.FlatResultTable;
            data = BuildOutputDataTable();
            foreach (DataRow myinrow in returned_data.Rows)
            {

                data.ImportRow(myinrow);

            }

            return;


        }
        internal DataTable BuildOutputDataTable()
        {
            DataTable _data = new DataTable();

            // This matches the schema of the user-defined table type
            _data.Columns.Add("ClientId", typeof(int));
            _data.Columns.Add("CustomerId", typeof(string));
            _data.Columns.Add("AccountId", typeof(string));
            _data.Columns.Add("PremiseId", typeof(string));
            _data.Columns.Add("BillDate", typeof(DateTime));
            _data.Columns.Add("EndUseKey", typeof(string));
            _data.Columns.Add("ApplianceKey", typeof(string));
            _data.Columns.Add("CommodityKey", typeof(string));
            _data.Columns.Add("PeriodId", typeof(int));
            _data.Columns.Add("Usage", typeof(decimal));
            _data.Columns.Add("Cost", typeof(decimal));
            _data.Columns.Add("UomKey", typeof(string));
            _data.Columns.Add("CurrencyKey", typeof(string));
            _data.Columns.Add("Confidence", typeof(int));
            _data.Columns.Add("ReconciliationRatio", typeof(decimal));
            _data.Columns.Add("StatusId", typeof(int));
            _data.Columns.Add("NewDate", typeof(DateTime));

            return _data;
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
          

            int output_write_error = 0;
            if (args.Length > 0)
            {
                Console.SetIn(new StreamReader(args[0]));
            }
          
            
            MyDisagg myclass = new Mapper.MyDisagg();


            string line;
            string[] values;
            int i = 0;
            while (((line = Console.ReadLine()) != null))
            {
                i = i + 1;
                values = line.Split('\t');

                string clientId = values[0];
                string premiseId = values[1];
                string accountId = values[2];
                string customerId = values[3];


                

                try
                {
                    myclass.ProcessInputRow(customerId, accountId, premiseId);
                }
                catch
                {
                    
                    continue;
                }


                if ((myclass.data == null) || (myclass.data.Rows.Count == 0))
                {
                    continue;
                }
                else if ((myclass.disagg_error == 0) && (myclass.data.Rows.Count > 0))
                {
                    foreach (DataRow arow in myclass.data.Rows)
                    {
                        try
                        {
                            Console.WriteLine("{0}\t{1}\t{2}\t{3}\t{4}\t{5}\t{6}\t{7}\t{8}\t{9}\t{10}\t{11}\t{12}\t{13}\t{14}\t{15}\t{16}",
                                arow["ClientId"] == DBNull.Value ? String.Empty : arow["ClientId"].ToString(),
                                arow["CustomerId"] == DBNull.Value ? String.Empty : arow["CustomerId"].ToString(),
                                arow["AccountId"] == DBNull.Value ? String.Empty : arow["AccountId"].ToString(),
                                arow["PremiseId"] == DBNull.Value ? String.Empty : arow["PremiseId"].ToString(),
                                arow["BillDate"] == DBNull.Value ? String.Empty : String.Format("{0:s}", arow["BillDate"]),
                                arow["EndUseKey"] == DBNull.Value ? String.Empty : arow["EndUseKey"].ToString(),
                                arow["ApplianceKey"] == DBNull.Value ? String.Empty : arow["ApplianceKey"].ToString(),
                                arow["CommodityKey"] == DBNull.Value ? String.Empty : arow["CommodityKey"].ToString(),
                                arow["PeriodId"] == DBNull.Value ? String.Empty : arow["PeriodId"].ToString(),
                                arow["Usage"] == DBNull.Value ? String.Empty : arow["Usage"].ToString(),
                                arow["Cost"] == DBNull.Value ? String.Empty : arow["Cost"].ToString(),
                                arow["UomKey"] == DBNull.Value ? String.Empty : arow["UomKey"].ToString(),
                                arow["CurrencyKey"] == DBNull.Value ? String.Empty : arow["CurrencyKey"].ToString(),
                                arow["Confidence"] == DBNull.Value ? String.Empty : arow["Confidence"].ToString(),
                                arow["ReconciliationRatio"] == DBNull.Value ? String.Empty : arow["ReconciliationRatio"].ToString(),
                                arow["StatusId"] == DBNull.Value ? String.Empty : arow["StatusId"].ToString(),
                                arow["NewDate"] == DBNull.Value ? String.Empty : String.Format("{0:s}", arow["NewDate"])
                                );
                        }
                        catch (Exception ex)
                        {
                            output_write_error++;
                            
                            continue;
                        }
                    }
                }
                else
                {
                    
                    continue;
                }



            }
            
        }



    }
}
