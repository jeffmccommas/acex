SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Srini Ambati
-- Create date:	2.15.2015
-- Description:	Returns the content environment
-- Usage: 
-- EXEC [dbo].[uspSelectContentEnvironment]
-- =============================================
CREATE PROC [dbo].[uspSelectContentEnvironment]
AS
    BEGIN

        SET NOCOUNT OFF

        BEGIN TRY
			SELECT TOP 1 EnvironmentKey FROM cm.EnvironmentConfiguration ORDER BY EnvironmentKey
            SET NOCOUNT ON;
        END TRY
        BEGIN CATCH
            THROW; --http://msdn.microsoft.com/en-us/library/ee677615.aspx
        END CATCH
    END

GO
