SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [cm].[uspSelectContentSystemConfigurationBulks]
AS
BEGIN
    BEGIN
        DECLARE @environment VARCHAR(256);
	
        SELECT  @environment = [EnvironmentKey]
        FROM    [cm].[EnvironmentConfiguration];
        
        WITH    SystemConfig
                  AS ( SELECT   tc.ConfigurationID ,
                                tc.[ConfigurationBulkKey] AS [Key] ,
                                sc.[CategoryKey] AS [Category] ,
                                sc.[JSONConfiguration] AS [JsonValue]
                       FROM     [cm].[SystemConfigurationBulk] AS sc WITH ( NOLOCK )
                                INNER JOIN [cm].TypeConfigurationBulk AS tc WITH ( NOLOCK ) ON sc.ConfigurationBulkKey = tc.ConfigurationBulkKey
                       WHERE    EnvironmentKey IN ( 'prod', @environment )
                     )
            SELECT  [Key] ,
                    Category ,
                    JsonValue
            FROM    SystemConfig
    END
END


GO
