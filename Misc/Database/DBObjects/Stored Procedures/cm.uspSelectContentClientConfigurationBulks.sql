SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- Author:		Lee Chu
-- Create date: 08/03/2016
-- Description:	This proc returns Configuration Bulk Content specific to client
-- Usage Example:
/*
BEGIN TRANSACTION

EXEC [cm].[uspSelectContentClientConfigurationBulks] 87

select * from cm.ClientConfigurationBulk

ROLLBACK TRANSACTION
*/
-- =============================================

CREATE PROCEDURE [cm].[uspSelectContentClientConfigurationBulks]
    (
      @clientID INT ,
      @categoryKey AS VARCHAR(256) = ''
    )
AS
    BEGIN

        DECLARE @environment VARCHAR(256);
	
        SELECT  @environment = [EnvironmentKey]
        FROM    [cm].[EnvironmentConfiguration];

        PRINT @environment;
        
        WITH    EnvironmentConfig
                  AS ( SELECT   tc.ConfigurationID ,
                                tc.[ConfigurationBulkKey] AS [Key] ,
                                cc.[CategoryKey] AS [Category] ,
                                cc.[JSONConfiguration] AS [JsonValue] ,
                                cc.[Description] AS [Description] ,
                                ROW_NUMBER() OVER ( PARTITION BY ConfigurationID ORDER BY IIF(EnvironmentKey = @Environment, 1, 0)DESC, ClientID DESC ) AS SortId
                       FROM     [cm].[ClientConfigurationBulk] AS cc WITH ( NOLOCK )
                                INNER JOIN [cm].TypeConfigurationBulk AS tc WITH ( NOLOCK ) ON cc.ConfigurationBulkKey = tc.ConfigurationBulkKey
                       WHERE    cc.ClientID IN ( 0, @clientID )
                                AND EnvironmentKey IN ( 'prod', @environment )
                                AND ( @categoryKey = ''
                                      OR cc.CategoryKey = @categoryKey
                                    )
                     )
            SELECT  [Key] ,
                    Category ,
                    JsonValue
            FROM    EnvironmentConfig
            WHERE   SortId = 1
    END



GO
