IF NOT EXISTS (SELECT * FROM master.dbo.syslogins WHERE loginname = N'mdm_admin')
CREATE LOGIN [mdm_admin] WITH PASSWORD = 'p@ssw0rd'
GO
CREATE USER [mdm_admin] FOR LOGIN [mdm_admin]
GO
