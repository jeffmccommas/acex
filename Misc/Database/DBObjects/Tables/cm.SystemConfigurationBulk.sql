CREATE TABLE [cm].[SystemConfigurationBulk]
(
[SystemConfigurationBulkID] [int] NOT NULL IDENTITY(1, 1),
[EnvironmentKey] [varchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ConfigurationBulkKey] [varchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CategoryKey] [varchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[JSONConfiguration] [varchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[DateUpdated] [datetime] NOT NULL CONSTRAINT [DF_SystemConfigurationBulk_DateUpdated] DEFAULT (getdate())
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [cm].[SystemConfigurationBulk] ADD CONSTRAINT [PK_SystemConfigurationBulk] PRIMARY KEY CLUSTERED  ([SystemConfigurationBulkID]) ON [PRIMARY]
GO
ALTER TABLE [cm].[SystemConfigurationBulk] ADD CONSTRAINT [FK_SystemConfigurationBulk_TypeCategory] FOREIGN KEY ([CategoryKey]) REFERENCES [cm].[TypeCategory] ([CategoryKey])
GO
ALTER TABLE [cm].[SystemConfigurationBulk] ADD CONSTRAINT [FK_SystemConfigurationBulk_TypeConfigurationBulk] FOREIGN KEY ([ConfigurationBulkKey]) REFERENCES [cm].[TypeConfigurationBulk] ([ConfigurationBulkKey])
GO
ALTER TABLE [cm].[SystemConfigurationBulk] ADD CONSTRAINT [FK_SystemConfigurationBulk_TypeEnvironment] FOREIGN KEY ([EnvironmentKey]) REFERENCES [cm].[TypeEnvironment] ([EnvironmentKey])
GO
