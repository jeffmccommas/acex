CREATE TABLE [cm].[EnvironmentConfiguration]
(
[EnvironmentKey] [varchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [cm].[EnvironmentConfiguration] ADD CONSTRAINT [PK_EnvironmentConfiguration] PRIMARY KEY CLUSTERED  ([EnvironmentKey]) ON [PRIMARY]
GO
