CREATE TABLE [ao].[Tenant]
(
[TenantId] [varchar] (36) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ClientId] [int] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [ao].[Tenant] ADD CONSTRAINT [PK_Tenant] PRIMARY KEY CLUSTERED  ([TenantId]) ON [PRIMARY]
GO
ALTER TABLE [ao].[Tenant] ADD CONSTRAINT [FK_Tenant_Client] FOREIGN KEY ([ClientId]) REFERENCES [dbo].[Client] ([ClientID])
GO
