CREATE TABLE [cm].[ClientConfigurationBulk]
(
[ClientConfigurationBulkID] [int] NOT NULL IDENTITY(1, 1),
[ClientID] [int] NOT NULL,
[EnvironmentKey] [varchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ConfigurationBulkKey] [varchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Name] [varchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CategoryKey] [varchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Description] [varchar] (4000) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[JSONConfiguration] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[DateUpdated] [datetime] NOT NULL CONSTRAINT [DF_ClientConfigurationBulk_DateUpdated] DEFAULT (getdate())
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [cm].[ClientConfigurationBulk] ADD CONSTRAINT [PK_ClientConfigurationBulk] PRIMARY KEY CLUSTERED  ([ClientConfigurationBulkID]) ON [PRIMARY]
GO
ALTER TABLE [cm].[ClientConfigurationBulk] ADD CONSTRAINT [FK_ClientConfigurationBulk_Client] FOREIGN KEY ([ClientID]) REFERENCES [dbo].[Client] ([ClientID])
GO
ALTER TABLE [cm].[ClientConfigurationBulk] ADD CONSTRAINT [FK_ClientConfigurationBulk_TypeCategory] FOREIGN KEY ([CategoryKey]) REFERENCES [cm].[TypeCategory] ([CategoryKey])
GO
ALTER TABLE [cm].[ClientConfigurationBulk] ADD CONSTRAINT [FK_ClientConfigurationBulk_TypeConfigurationBulk] FOREIGN KEY ([ConfigurationBulkKey]) REFERENCES [cm].[TypeConfigurationBulk] ([ConfigurationBulkKey])
GO
ALTER TABLE [cm].[ClientConfigurationBulk] ADD CONSTRAINT [FK_ClientConfigurationBulk_TypeEnvironment] FOREIGN KEY ([EnvironmentKey]) REFERENCES [cm].[TypeEnvironment] ([EnvironmentKey])
GO
