CREATE TABLE [dbo].[Client]
(
[ClientID] [int] NOT NULL,
[Name] [varchar] (64) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Description] [varchar] (128) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RateCompanyID] [int] NULL,
[ReferrerID] [int] NULL,
[NewDate] [datetime] NOT NULL CONSTRAINT [DF_Client_NewDate] DEFAULT (getdate()),
[UpdDate] [datetime] NOT NULL CONSTRAINT [DF_Client_UpdDate] DEFAULT (getdate()),
[AuthType] [tinyint] NOT NULL CONSTRAINT [DF_Client_AuthType] DEFAULT ((0)),
[EnableInd] [bit] NOT NULL CONSTRAINT [DF_Client_EnableInd] DEFAULT ((1))
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Client] ADD CONSTRAINT [PK_Client] PRIMARY KEY CLUSTERED  ([ClientID]) ON [PRIMARY]
GO
