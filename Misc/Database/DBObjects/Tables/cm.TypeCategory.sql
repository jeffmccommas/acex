CREATE TABLE [cm].[TypeCategory]
(
[CategoryKey] [varchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CategoryID] [int] NOT NULL,
[DateUpdated] [datetime] NOT NULL CONSTRAINT [DF_TypeCategory_DateUpdated] DEFAULT (getdate())
) ON [PRIMARY]
GO
ALTER TABLE [cm].[TypeCategory] ADD CONSTRAINT [PK_Category] PRIMARY KEY CLUSTERED  ([CategoryKey]) ON [PRIMARY]
GO
