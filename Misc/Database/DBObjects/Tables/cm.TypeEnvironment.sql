CREATE TABLE [cm].[TypeEnvironment]
(
[EnvironmentKey] [varchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[EnvironmentID] [int] NOT NULL,
[DateUpdated] [datetime] NOT NULL CONSTRAINT [DF_TypeEnvironment_DateUpdated] DEFAULT (getdate())
) ON [PRIMARY]
GO
ALTER TABLE [cm].[TypeEnvironment] ADD CONSTRAINT [PK_Environment] PRIMARY KEY CLUSTERED  ([EnvironmentKey]) ON [PRIMARY]
GO
