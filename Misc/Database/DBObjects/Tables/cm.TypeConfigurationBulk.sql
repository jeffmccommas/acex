CREATE TABLE [cm].[TypeConfigurationBulk]
(
[ConfigurationBulkKey] [varchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ConfigurationID] [int] NOT NULL,
[DateUpdated] [datetime] NOT NULL CONSTRAINT [DF_TypeConfigurationBulk_DateUpdated] DEFAULT (getdate())
) ON [PRIMARY]
GO
ALTER TABLE [cm].[TypeConfigurationBulk] ADD CONSTRAINT [PK_ConfigurationBulk] PRIMARY KEY CLUSTERED  ([ConfigurationBulkKey]) WITH (STATISTICS_NORECOMPUTE=ON) ON [PRIMARY]
GO
