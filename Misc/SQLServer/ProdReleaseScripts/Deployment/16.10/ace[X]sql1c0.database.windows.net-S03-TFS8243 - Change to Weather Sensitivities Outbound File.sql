
/****** RUN ONLY ON INSIGHTSDW_101  SHARD ON UAT AND PROD******/


USE [InsightsDW_101]
GO
/****** Object:  StoredProcedure [Export].[WeatherReport]    Script Date: 10/5/2016 10:59:54 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		Ubaid
-- Create date: 9/11/2015
-- Description:	monthly extract for socal consumption data
-- =============================================
ALTER PROCEDURE [Export].[WeatherReport]
    @CampaignId AS INT ,
    @ReportNumber AS INT ,
    @ReportMonth AS VARCHAR(50) ,
    @Channel AS VARCHAR(50)
AS
    BEGIN
        SET NOCOUNT ON;

        DECLARE @channelAttribute AS VARCHAR(50);

        IF @Channel = 'print'
            SET @channelAttribute = 'weathersensitivitiesprogram.channel.printonly';
        ELSE
            IF @Channel = 'email'
                SET @channelAttribute = 'weathersensitivitiesprogram.channel.emailonly';


        SELECT  a.PremiseKey
        INTO    #subscribers
        FROM    ( SELECT    PremiseKey ,
                            ContentAttributeKey ,
                            Value ,
                            ROW_NUMBER() OVER ( PARTITION BY PremiseKey ORDER BY EffectiveDate DESC, TrackingDate DESC ) AS SortId
                  FROM      dbo.FactPremiseAttribute
                  WHERE     ContentAttributeKey = 'weathersensitivitiesprogram.channel'
                ) a
        WHERE   a.SortId = 1
                AND ( a.Value = @channelAttribute
                      OR a.Value = 'weathersensitivitiesprogram.channel.printandemail'
                    ); 

        SELECT  *
        INTO    #Cells
        FROM    ( SELECT    pa.PremiseKey ,
                            pa.ContentAttributeKey ,
                            pa.Value ,
                            ROW_NUMBER() OVER ( PARTITION BY pa.PremiseKey ORDER BY pa.EffectiveDate DESC, pa.TrackingDate DESC ) AS SortId
                  FROM      #subscribers s
                            INNER JOIN dbo.FactPremiseAttribute pa ON pa.PremiseKey = s.PremiseKey
                  WHERE     pa.ContentAttributeKey = 'weathersensitivitiesprogram.treatmentgroup.groupnumber'
                ) a
        WHERE   a.SortId = 1;

        SELECT  a.AccountID AS Account_Number ,
                a.CustomerID ,
                a.ReferrerID ,
                a.CellID ,
                REPLACE(a.FirstName, ',', '') AS FirstName ,
				REPLACE(a.LastName, ',', '') AS LastName ,
                a.Mailing_Address ,
                a.Mailing_Address2 ,
                a.Mailing_City ,
                a.Mailing_State ,
                a.Mailing_Zip ,
                a.EmailAddress ,
                @ReportMonth AS Report_Month_Year ,
                WSYourHomeUsage ,
                WSComparetoTypicalUsage ,
                WSComparetoEfficientUsage ,
                CASE WHEN YourHomeComparedToNeighborUsage = 1 THEN ''
                     WHEN YourHomeComparedToNeighborUsage >= 1.95
                     THEN CONVERT(VARCHAR(5), CONVERT(FLOAT, ROUND(YourHomeComparedToNeighborUsage
                                                              * 2, 0) / 2, 0))
                          + 'X'
                     ELSE CONVERT(VARCHAR(5), CONVERT(FLOAT, ROUND(ABS(YourHomeComparedToNeighborUsage
                                                              - 1) * 10, 0)
                          * 10)) + '%'
                END AS YourHomeComparedToNeighborUsage ,
                CASE WHEN YourHomeComparedToNeighborUsage = 1 THEN ''
                     WHEN YourHomeComparedToNeighborUsage >= 1.95
                     THEN CONVERT(VARCHAR(5), CONVERT(FLOAT, ROUND(YourHomeComparedToNeighborUsage
                                                              * 2, 0) / 2, 0))
                          + 'X'
                     ELSE CONVERT(VARCHAR(5), CONVERT(FLOAT, ROUND(ABS(YourHomeComparedToNeighborUsage
                                                              - 1) * 10, 0)
                          * 10)) + ' percent'
                END AS YourHomeComparedToNeighborUsageTextValue ,
                WSYourHomeNormalDayUsage ,
                WSYourHomeColdDayUsage ,
                CASE WHEN a.CompareToMyselfUsage >= 1.95
                     THEN CONVERT(VARCHAR(5), CONVERT(FLOAT, ROUND(CompareToMyselfUsage
                                                              * 2, 0) / 2, 0))
                          + 'X'
                     ELSE CONVERT(VARCHAR(5), CONVERT(FLOAT, ROUND(ABS(CompareToMyselfUsage
                                                              - 1) * 10, 0)
                          * 10)) + '%'
                END AS CompareToMyselfUsage ,
                CASE WHEN a.CompareToMyselfUsage >= 1.95
                     THEN CONVERT(VARCHAR(5), CONVERT(FLOAT, ROUND(CompareToMyselfUsage
                                                              * 2, 0) / 2, 0))
                          + 'X'
                     ELSE CONVERT(VARCHAR(5), CONVERT(FLOAT, ROUND(ABS(CompareToMyselfUsage
                                                              - 1) * 10, 0)
                          * 10)) + ' percent'
                END AS CompareToMyselfUsageTextValue ,
                @ReportNumber AS ProgramReportNumber ,
                '*****' + SUBSTRING(a.AccountID, LEN(a.AccountID) - 4, 5) AS MaskedAccountNumber
        FROM    ( SELECT    * ,
                            CASE WHEN WSYourHomeUsage > WSComparetoTypicalUsage
                                 THEN ( IIF(WSYourHomeUsage
                                        / WSComparetoTypicalUsage >= 1.05, WSYourHomeUsage
                                        / WSComparetoTypicalUsage, WSYourHomeUsage
                                        / WSComparetoEfficientUsage))
                                 WHEN WSYourHomeUsage <= WSComparetoTypicalUsage
                                      AND WSYourHomeUsage > WSComparetoEfficientUsage
                                 THEN ( IIF(WSYourHomeUsage
                                        / WSComparetoEfficientUsage >= 1.05, WSYourHomeUsage
                                        / WSComparetoEfficientUsage, 1))
                                 WHEN WSYourHomeUsage = WSComparetoEfficientUsage
                                 THEN 1
                                 WHEN WSYourHomeUsage < WSComparetoEfficientUsage
                                 THEN ( IIF(WSYourHomeUsage
                                        / WSComparetoEfficientUsage <= 0.95, WSYourHomeUsage
                                        / WSComparetoEfficientUsage, 1))
                            END AS YourHomeComparedToNeighborUsage ,
                            ( WSYourHomeColdDayUsage
                              / WSYourHomeNormalDayUsage ) AS CompareToMyselfUsage
                  FROM      ( SELECT    101 AS ReferrerID ,
                                        cd.CustomerID ,
                                        cd.AccountID ,
                                        c.Value AS CellID ,
                                        dc.FirstName AS FirstName ,
										dc.LastName AS LastName ,
                                        LTRIM(RTRIM(dc.Street1)) AS Mailing_Address ,
                                        LTRIM(RTRIM(dc.Street2)) AS Mailing_Address2 ,
                                        dc.City AS Mailing_City ,
                                        dc.StateProvince AS Mailing_State ,
                                        dc.PostalCode AS Mailing_Zip ,
                                        dc.EmailAddress ,
                                        CONVERT(FLOAT, ROUND(IIF(WSYourHomeUsage < 0.5, 1, WSYourHomeUsage),
                                                             0)) AS WSYourHomeUsage ,
                                        CONVERT(FLOAT, ROUND(IIF(WSComparetoTypicalUsage < 0.5, 1, WSComparetoTypicalUsage),
                                                             0)) AS WSComparetoTypicalUsage ,
                                        CONVERT(FLOAT, ROUND(IIF(WSComparetoEfficientUsage < 0.5, 1, WSComparetoEfficientUsage),
                                                             0)) AS WSComparetoEfficientUsage ,
                                        CONVERT(FLOAT, ROUND(IIF(WSYourHomeNormalDayUsage < 0.5, 1, WSYourHomeNormalDayUsage),
                                                             0)) AS WSYourHomeNormalDayUsage ,
                                        CONVERT(FLOAT, ROUND(IIF(WSYourHomeColdDayUsage < 0.5, 1, WSYourHomeColdDayUsage),
                                                             0)) AS WSYourHomeColdDayUsage
                              FROM      dbo.DimCustomer dc
                                        INNER JOIN dbo.FactCustomerPremise fcp ON dc.CustomerKey = fcp.CustomerKey
                                        INNER	JOIN dbo.DimPremise dp ON dp.PremiseKey = fcp.PremiseKey
                                        INNER JOIN #Cells c ON c.PremiseKey = dp.PremiseKey
                                        INNER JOIN dbo.ConsumptionData cd ON cd.CustomerID = dc.CustomerId
                                                              AND cd.AccountID = dp.AccountId
                                                              AND cd.PremiseID = dp.PremiseId
                              WHERE     cd.CampaignID = @CampaignId
                            ) b
                ) a;

        DROP TABLE #Cells;
        DROP TABLE #subscribers;

    END;



