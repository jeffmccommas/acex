USE [InsightsMetaData]
GO

/****** Object:  StoredProcedure [cm].[new2_uspSelectDefaultProfileAttributeKeys]    Script Date: 10/4/2016 9:54:59 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

IF EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND name = 'uspSelectDefaultProfileAttributeKeys_nofn')
BEGIN
	DROP PROCEDURE cm.uspSelectDefaultProfileAttributeKeys_nofn
	SELECT 'is there' as 'result'
END
ELSE 
	SELECT 'not there' as 'result'
GO



-- =============================================
-- Author:		Srini Ambati 
-- Create date: 07/14/2014
-- Description:	This proc returns Action Content specific to client
-- update:   Chris Hackett TFS7749 copy/rename/mod for perf modifications.  replace db Fn call with c# management
-- Usage Example:
/*
BEGIN TRANSACTION

DECLARE @p1 [cm].[KeyTable]
--INSERT INTO @P1 VALUES('house.people')
--INSERT INTO @P1 VALUES('house.roofarea')

DECLARE @p2 [cm].[KeyTable]
--INSERT  INTO @P2 VALUES  ( 'heating' );
DECLARE @p3 [cm].[KeyTable]
--INSERT  INTO @P3 VALUES  ( 'clotheswasher' );

EXEC [cm].[new_uspSelectDefaultProfileAttributeKeys] 256, @p1, @p2, @p3, null

ROLLBACK TRANSACTION
*/
-- =============================================

CREATE PROCEDURE [cm].[uspSelectDefaultProfileAttributeKeys_nofn]
    (
      @clientID INT ,
      @attributeKeysFilterTable [cm].[KeyTable] READONLY ,
      @enduseKeysFilterTable [cm].[KeyTable] READONLY ,
      @applianceKeysFilterTable [cm].[KeyTable] READONLY ,
      @entitylevelFilter VARCHAR(256)
    )
AS
    BEGIN
	
        DECLARE @attributeKeysExist AS BIT = 0;
        DECLARE @enduseKeysExist AS BIT = 0
        DECLARE @applianceKeysExist AS BIT = 0


        IF EXISTS ( SELECT TOP 1 [Key] FROM @attributeKeysFilterTable )
            BEGIN
                SET @attributeKeysExist = 1;
            END

        IF EXISTS ( SELECT TOP 1 [Key] FROM @enduseKeysFilterTable )
            BEGIN
                SET @enduseKeysExist = 1;
            END

        IF EXISTS ( SELECT TOP 1 [Key] FROM @applianceKeysFilterTable )
            BEGIN
                SET @applianceKeysExist = 1;
            END

        IF ( @entitylevelFilter = '' )
            BEGIN
                SET @entitylevelFilter = NULL;
            END

        SELECT  cpa.[ProfileAttributeKey]  AS AttributeKey,
				cpa.[MinValue] ,
                cpa.[MaxValue] ,
				cpa.ClientProfileAttributeID AS OptionKeys ,  -- no more function call expects c# code to generate the csv string
				--[cm].[new_fnGetOptionFieldValues](cpa.ClientProfileAttributeID) AS OptionKeys ,
                tpa.[ProfileAttributeTypeKey] AS Type ,
                tpa.[EntityLevelKey] AS EntityLevel
        FROM    [cm].[ClientProfileAttribute] AS cpa WITH ( NOLOCK )
                INNER JOIN [cm].TypeProfileAttribute AS tpa WITH ( NOLOCK ) ON cpa.ProfileAttributeKey = tpa.ProfileAttributeKey
				INNER JOIN ( SELECT MAX(clientid) AS clientid , ProfileAttributeKey
                FROM   cm.ClientProfileAttribute WITH ( NOLOCK )
                WHERE  clientid IN ( 0, @clientID )
                GROUP BY ProfileAttributeKey
					) AS ba ON ba.clientid = cpa.ClientID
								AND ba.ProfileAttributeKey = cpa.ProfileAttributeKey
        WHERE   [Disable] = 0
                AND ( @attributeKeysExist = 0
                      OR cpa.[ProfileAttributeKey] IN (
                      SELECT    [Key]
                      FROM      @attributeKeysFilterTable )
                    )
                AND ( @enduseKeysExist = 0
                      OR cpa.[ProfileAttributeKey] IN (
                      SELECT    capa.ProfileAttributeKey
                      FROM      cm.ClientEnduseAppliance AS cea WITH ( NOLOCK )
                                INNER JOIN cm.ClientEnduse AS ce WITH ( NOLOCK ) ON cea.ClientEnduseID = ce.ClientEnduseID
                                INNER JOIN cm.ClientApplianceProfileAttribute AS capa WITH ( NOLOCK ) 
                                INNER JOIN cm.ClientAppliance AS ca WITH ( NOLOCK )  ON capa.ClientApplianceID = ca.ClientApplianceID ON cea.ApplianceKey = ca.ApplianceKey
                                INNER JOIN @enduseKeysFilterTable AS p2 ON ce.EnduseKey = p2.[Key] )
                    )
                AND ( @applianceKeysExist = 0
                      OR cpa.[ProfileAttributeKey] IN (
                      SELECT    capa.ProfileAttributeKey
                      FROM      cm.ClientApplianceProfileAttribute AS capa WITH ( NOLOCK )
                                INNER JOIN cm.ClientAppliance AS ca WITH ( NOLOCK ) ON capa.ClientApplianceID = ca.ClientApplianceID
                                INNER JOIN @applianceKeysFilterTable AS p3 ON ca.ApplianceKey = p3.[Key] )
                    )
                AND tpa.EntityLevelKey = ISNULL(@entitylevelFilter, tpa.EntityLevelKey)
    END





GO


