
/******RUN ONLY ON INSIGHTSDW_224 SHARD ON UAT AND PROD  ******/




USE [InsightsDW_224]
GO
/****** Object:  StoredProcedure [dbo].[CreateBillBenchmarks]    Script Date: 10/5/2016 2:03:04 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[CreateBillBenchmarks] @ClientId AS INT
AS
    BEGIN
        SET NOCOUNT ON;

        DECLARE @MinBillDays AS INT;
        DECLARE @MaxBillDays AS INT;
        DECLARE @MinBillAmount AS INT;
        DECLARE @MaxBillAmount AS INT;
        DECLARE @EfficientPeerPercentage AS INT;
        DECLARE @BenchmarkDaysRange AS INT = 10;
        DECLARE @BenchmarkDaysRangeOffset AS INT= 12;
        DECLARE @BenchmarkMinGroupCount AS INT= 100;
        DECLARE @BenchmarkStartDate AS DATE = '2015-05-01';
        DECLARE @QuarterlyBillWindowSize AS INT = 68;
        DECLARE @LastRunDate AS VARCHAR(256) = '1900-01-01';
        DECLARE @MinGroupSize AS INT = 50;

        CREATE TABLE #RunDate
            (
              LastRunDate VARCHAR(256)
            );

        CREATE TABLE #Bills
            (
              PremiseKey INT ,
              GroupId SMALLINT ,
              BillEndDate DATE ,
              CommodityId TINYINT ,
              EndDate DATE ,
              StartDate DATE ,
              TotalUsage DECIMAL(10, 2) ,
              CostOfUsage DECIMAL(10, 2) ,
              UOMId TINYINT
            );

        CREATE TABLE #BDates
            (
              GroupId SMALLINT ,
              EndDate DATE
            );

        CREATE TABLE #Groups
            (
              PremiseKey INT ,
              GroupId SMALLINT ,
              BillEndDate DATE
            );

        CREATE TABLE #ContentConfiguration
            (
              ConfigurationKey VARCHAR(256) ,
              CategoryKey VARCHAR(256) ,
              Value VARCHAR(256)
            );
	
        INSERT  INTO #RunDate
                EXEC [dbo].[GetClientAttributeValue] @ClientId = @ClientId,
                    @AttributeName = 'benchmark.rundate',
                    @DefaultValue = @LastRunDate;

        INSERT  INTO #ContentConfiguration
                EXEC [uspSelectContentClientConfigurations] @clientID = @ClientId,
                    @categoryKey = 'benchmark';

        SELECT  @LastRunDate = LastRunDate
        FROM    #RunDate;

        SELECT  @EfficientPeerPercentage = ISNULL(Value,
                                                  @EfficientPeerPercentage)
        FROM    #ContentConfiguration
        WHERE   [ConfigurationKey] = 'efficientpeerpercentage';

        SELECT  @BenchmarkDaysRange = ISNULL(Value, @BenchmarkDaysRange)
        FROM    #ContentConfiguration
        WHERE   [ConfigurationKey] = 'benchmarkbilldaysrange';

        SELECT  @BenchmarkDaysRangeOffset = ISNULL(Value,
                                                   @BenchmarkDaysRangeOffset)
        FROM    #ContentConfiguration
        WHERE   [ConfigurationKey] = 'benchmarkbilldaysrangeadder';

        SELECT  @BenchmarkMinGroupCount = ISNULL(Value,
                                                 @BenchmarkMinGroupCount)
        FROM    #ContentConfiguration
        WHERE   [ConfigurationKey] = 'benchmarkbilldaysgroupsize';

        SELECT  @BenchmarkStartDate = ISNULL(Value, @BenchmarkStartDate)
        FROM    #ContentConfiguration
        WHERE   [ConfigurationKey] = 'benchmarkprocessstartdate';

        SELECT  @MinBillDays = Value
        FROM    #ContentConfiguration
        WHERE   [ConfigurationKey] = 'benchmarkminbilldays.month';

        SELECT  @MaxBillDays = Value
        FROM    #ContentConfiguration
        WHERE   [ConfigurationKey] = 'benchmarkmaxbilldays.month';

        SELECT  @MinBillAmount = Value
        FROM    #ContentConfiguration
        WHERE   [ConfigurationKey] = 'benchmarkminbillamount.month';

        SELECT  @MaxBillAmount = Value
        FROM    #ContentConfiguration
        WHERE   [ConfigurationKey] = 'benchmarkmaxbillamount.month';

        DECLARE @BenchmarkMaxRange AS INT= @BenchmarkDaysRange
            + @BenchmarkDaysRangeOffset;

        INSERT  INTO #BDates
                SELECT DISTINCT
                        GroupId ,
                        EndDate
                FROM    dbo.FactServicePointBilling b
                        INNER JOIN dbo.DimServiceContract sc ON sc.ServiceContractKey = b.ServiceContractKey
                        INNER JOIN BillBenchmarkGroups g ON g.PremiseKey = sc.PremiseKey
                WHERE   EndDate >= @BenchmarkStartDate
                        AND b.CreateDate > @LastRunDate
                ORDER BY GroupId ,
                        b.EndDate;

        INSERT  INTO #Groups
                SELECT  PremiseKey ,
                        g.GroupId ,
                        b.EndDate AS BillEndDate
                FROM    [dbo].[BillBenchmarkGroups] g
                        INNER JOIN #BDates b ON b.GroupId = g.GroupId
                ORDER BY GroupId ,
                        b.EndDate;

        SET @LastRunDate = CONVERT(VARCHAR(256), GETDATE(), 120); 


        DECLARE @MinBillDate AS DATE;

        SELECT  @MinBillDate = DATEADD(DAY, -1 * @BenchmarkMaxRange,
                                       MIN(BillEndDate))
        FROM    #Groups;

        INSERT  INTO #Bills
                SELECT  sc.PremiseKey ,
                        GroupId ,
                        g.BillEndDate ,
                        b.CommodityId ,
                        b.EndDate ,
                        MIN(b.StartDate) AS StartDate ,
                        SUM(b.TotalUsage) AS TotalUsage ,
                        SUM(b.CostOfUsage) AS CostOfUsage ,
                        MIN(b.UOMId) AS UOMId
                FROM    #Groups g
                        INNER JOIN dbo.DimServiceContract sc ON sc.PremiseKey = g.PremiseKey
                        INNER JOIN dbo.FactServicePointBilling b ON b.ServiceContractKey = sc.ServiceContractKey
                                                              AND b.EndDate <= g.BillEndDate
                                                              AND b.EndDate >= DATEADD(DAY,
                                                              -1
                                                              * @BenchmarkMaxRange,
                                                              g.BillEndDate)
                WHERE   b.EndDate >= @MinBillDate
                        AND b.BillDays >= @MinBillDays
                        AND b.BillDays <= @MaxBillDays
                        AND b.CostOfUsage >= @MinBillAmount
                        AND b.CostOfUsage <= @MaxBillAmount
                GROUP BY sc.PremiseKey ,
                        GroupId ,
                        g.BillEndDate ,
                        b.CommodityId ,
                        b.EndDate
                ORDER BY GroupId ,
                        g.BillEndDate ,
                        b.CommodityId ,
                        TotalUsage;
						
        SELECT  GroupId ,
                BillEndDate ,
                CommodityId ,
                DATEADD(DAY,
                        -1
                        * IIF(COUNT(*) < @BenchmarkMinGroupCount, @BenchmarkMaxRange, @BenchmarkDaysRange),
                        BillEndDate) AS BillStartDate
        INTO    #GroupRange
        FROM    #Bills
        WHERE   EndDate >= DATEADD(DAY, -1 * @BenchmarkDaysRange, BillEndDate)
        GROUP BY GroupId ,
                BillEndDate ,
                CommodityId
        ORDER BY GroupId;

        DELETE  r
        FROM    dbo.BillBenchmarkGroupResults r
                INNER JOIN ( SELECT DISTINCT
                                    GroupId ,
                                    BillEndDate
                             FROM   #Groups
                           ) g ON g.GroupId = r.GroupId
                                  AND g.BillEndDate = r.EndDate;

        INSERT  INTO BillBenchmarkGroupResults
                SELECT  @ClientId ,
                        GroupId ,
                        CommodityId ,
                        a.BillEndDate ,
                        MIN(a.BillStartDate) ,
                        AVG(IIF(a.SortIdUsage >= ( CEILING(GroupCount / 100.0 * 45) )
                            AND a.SortIdUsage <= ( CEILING(GroupCount / 100.0 * 55) ), a.TotalUsage, NULL)) ,
                        IIF((AVG(IIF(a.SortIdUsage >= ( GroupCount / 100.0
                                                   * ( 15
                                                       - 5 ) )				
                            AND a.SortIdUsage <= ( GroupCount / 100.0
                                                   * ( 15
                                                       + 5 ) ), a.TotalUsage, NULL))) < 1.0, 1.0, AVG(IIF(a.SortIdUsage >= ( GroupCount / 100.0
                                                   * ( 15
                                                       - 5 ) )
                            AND a.SortIdUsage <= ( GroupCount / 100.0
                                                   * ( 15
                                                       + 5 ) ), a.TotalUsage, NULL))) ,
                        AVG(IIF(a.SortIdCost >= ( CEILING(GroupCount / 100.0 * 45) )
                            AND a.SortIdCost <= ( CEILING(GroupCount / 100.0 * 55) ), a.CostOfUsage, NULL)) ,
                        AVG(IIF(a.SortIdCost >= ( GroupCount / 100.0
                                                  * ( @EfficientPeerPercentage
                                                      - 5 ) )
                            AND a.SortIdCost <= ( GroupCount / 100.0
                                                  * ( @EfficientPeerPercentage
                                                      + 5 ) ), a.CostOfUsage, NULL)) ,
                        COUNT(*) ,
                        GETDATE()
                FROM    ( SELECT    b.GroupId ,
                                    b.BillEndDate ,
                                    r.BillStartDate ,
                                    b.CommodityId ,
                                    b.TotalUsage ,
                                    b.CostOfUsage ,
                                    COUNT(*) OVER ( PARTITION BY b.GroupId,
                                                    b.BillEndDate,
                                                    b.CommodityId ) AS GroupCount ,
                                    ROW_NUMBER() OVER ( PARTITION BY b.GroupId,
                                                        b.BillEndDate,
                                                        b.CommodityId ORDER BY b.TotalUsage ) AS SortIdUsage ,
                                    ROW_NUMBER() OVER ( PARTITION BY b.GroupId,
                                                        b.BillEndDate,
                                                        b.CommodityId ORDER BY b.CostOfUsage ASC ) AS SortIdCost
                          FROM      #Bills b
                                    INNER JOIN #GroupRange r ON b.GroupId = r.GroupId
                                                              AND b.BillEndDate = r.BillEndDate
                                                              AND b.CommodityId = r.CommodityId
                          WHERE     b.EndDate >= r.BillStartDate
                        ) a
                GROUP BY GroupId ,
                        a.BillEndDate ,
                        CommodityId
                ORDER BY GroupId ,
                        a.BillEndDate;

		UPDATE BillBenchmarkGroupResults SET AverageUsage = 1 WHERE AverageUsage = 0
		UPDATE BillBenchmarkGroupResults SET EfficientUsage = 1 WHERE EfficientUsage = 0
		UPDATE BillBenchmarkGroupResults SET AverageCost = 1 WHERE AverageCost = 0
		UPDATE BillBenchmarkGroupResults SET EfficientCost = 1 WHERE EfficientCost = 0

        DELETE  r
        FROM    dbo.BillBenchmarkResults r
                INNER JOIN ( SELECT DISTINCT
                                    GroupId ,
                                    BillEndDate
                             FROM   #Groups
                           ) g ON g.GroupId = r.GroupId
                                  AND g.BillEndDate = r.EndDate;

        INSERT  INTO [dbo].[BillBenchmarkResults]
                SELECT  b.PremiseKey ,
                        @ClientId ,
                        b.CommodityId ,
                        b.EndDate ,
                        1 ,
                        b.GroupId ,
                        b.StartDate ,
                        b.TotalUsage ,
                        r.AverageUsage ,
                        EfficientUsage ,
                        IIF(TotalUsage > AverageUsage, TotalUsage
                        / AverageUsage, TotalUsage / EfficientUsage) ,
                        CASE WHEN TotalUsage > AverageUsage THEN 1
                             WHEN TotalUsage <= AverageUsage
                                  AND TotalUsage >= EfficientUsage THEN 2
                             ELSE 3
                        END ,
                        b.UOMId ,
                        b.CostOfUsage ,
                        AverageCost ,
                        EfficientCost ,
                        IIF(CostOfUsage > AverageCost, CostOfUsage
                        / AverageCost, CostOfUsage / EfficientCost) ,
                        CASE WHEN CostOfUsage > AverageCost THEN 1
                             WHEN CostOfUsage <= AverageCost
                                  AND CostOfUsage >= EfficientCost THEN 2
                             ELSE 3
                        END ,
                        GETDATE()
                FROM    #Bills b
                        INNER JOIN BillBenchmarkGroupResults r ON b.GroupId = r.GroupId
                                                              AND b.BillEndDate = r.EndDate
                WHERE   r.GroupCount >= @MinGroupSize
                        AND b.BillEndDate = b.EndDate
                ORDER BY b.GroupId ,
                        BillEndDate;

        INSERT  INTO [dbo].[BillBenchmarkResults]
                SELECT  b.PremiseKey ,
                        @ClientId ,
                        b.CommodityId ,
                        b.EndDate ,
                        3 ,
                        MIN(b.GroupId) ,
                        IIF(COUNT(*) = 3, MIN(r.StartDate), NULL) ,
                        IIF(COUNT(*) = 3, SUM([MyUsage]), NULL) ,
                        IIF(COUNT(*) = 3, SUM([AverageUsage]), NULL) ,
                        IIF(COUNT(*) = 3, SUM([EfficientUsage]), NULL) ,
                        IIF(COUNT(*) = 3, IIF(SUM([MyUsage]) > SUM([AverageUsage]), SUM([MyUsage])
                        / SUM([AverageUsage]), SUM([MyUsage])
                        / SUM([EfficientUsage])), NULL) ,
                        IIF(COUNT(*) = 3, CASE WHEN SUM([MyUsage]) > SUM([AverageUsage])
                                               THEN 1
                                               WHEN SUM([MyUsage]) <= SUM([AverageUsage])
                                                    AND SUM([MyUsage]) >= SUM([EfficientUsage])
                                               THEN 2
                                               ELSE 3
                                          END, NULL) ,
                        IIF(COUNT(*) = 3, MIN(b.UOMId), NULL) ,
                        IIF(COUNT(*) = 3, SUM([MyCost]), NULL) ,
                        IIF(COUNT(*) = 3, SUM([AverageCost]), NULL) ,
                        IIF(COUNT(*) = 3, SUM([EfficientCost]), NULL) ,
                        IIF(COUNT(*) = 3, IIF(SUM([MyCost]) > SUM([AverageCost]), SUM([MyCost])
                        / SUM([AverageCost]), SUM([MyCost])
                        / SUM([EfficientCost])), NULL) ,
                        IIF(COUNT(*) = 3, CASE WHEN SUM([MyCost]) > SUM([AverageCost])
                                               THEN 1
                                               WHEN SUM([MyCost]) <= SUM([AverageCost])
                                                    AND SUM([MyCost]) >= SUM([EfficientCost])
                                               THEN 2
                                               ELSE 3
                                          END, NULL) ,
                        GETDATE()
                FROM    #Bills b
                        INNER JOIN [dbo].[BillBenchmarkResults] r ON r.PremiseKey = b.PremiseKey
                                                              AND r.CommodityId = b.CommodityId
                                                              AND r.EndDate <= b.EndDate
                                                              AND r.EndDate >= DATEADD(DAY,
                                                              -1
                                                              * @QuarterlyBillWindowSize,
                                                              b.EndDate)
                WHERE   b.BillEndDate = b.EndDate
                        AND r.PeriodType = 1
                GROUP BY b.PremiseKey ,
                        b.CommodityId ,
                        b.EndDate;

        EXEC [dbo].[SetClientAttributeValue] @ClientId = @ClientId,
            @AttributeName = 'benchmark.rundate',
            @AttributeValue = @LastRunDate;

        DROP TABLE #Bills;
        DROP TABLE #Groups;
        DROP TABLE #GroupRange;
        DROP TABLE #ContentConfiguration;
        DROP TABLE #RunDate;
        DROP TABLE #BDates;

    END;
