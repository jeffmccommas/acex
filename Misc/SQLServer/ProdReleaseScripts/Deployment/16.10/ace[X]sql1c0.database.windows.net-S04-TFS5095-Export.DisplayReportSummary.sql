﻿
/******RUN ONLY ON INSIGHTSDW_224 SHARD ON UAT AND PROD  ******/



USE [InsightsDW_224]
GO
/****** Object:  StoredProcedure [Export].[DisplayReportSummary]    Script Date: 9/29/2016 1:27:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER procedure [Export].[DisplayReportSummary]
    @reportNumber       [int],
    @reportAlias        [varchar](100) = null,
    @clientId           [int] = 224
as
-------------------------------------------------------------------------------
--  Name:               DisplayReportSummary
--  Version:            2
--  Author:             George Lambert
--  Description :       Displays the summary totals for a report
--  Notes:
--      For UIL the alias/reportNumber uniquely identifies a report.
--      We will use the snapshot tables for displaying the summary.  If the report number has a release snapshot table we will use it, otherwise we will attempt to use
--      the daily snapshot table (if the report number is still effective).
--
--  Parameters:
--      @reportNumber       -   identifies the report number
--      @reportAlias        -   the report alias that identifies which report we should summarize
--                              if this is null then all reports will be summarized
--      @clientId           -   identifies the client
--
--
--  Examples:
--      display summary for all report # 2
--          [export].[DisplayReportSummary] @reportNumber = 2
--
--      display summary for report #1 for email_1
--          [export].[DisplayReportSummary] @reportNumber = 2, @reportAlias='email_1';
--
--
--  Change History:
--      20160121    glambert    -   use new setting tables
--                                  use all 3 measure columns (controlled by report profile setting measure_count)
--      20151229    glambert    -   added a total column to the action summary query
--                              -   added a new query to summarise by report alias
--                              -   refactored the query building
--      20151215    glambert    -   initial version
-----------------------------------------------------------------------------

begin
    set nocount on;
    -- "pivot" the report settings so we can manipulate them
    declare @report_setting table   (
                                        [profile_name]      [varchar](100),
                                        [alias]             [varchar](100),
                                        [effective_start]   [date],
                                        [effective_end]     [date],
                                        [report_number]     [int]
                                    );

    declare @measures_with_value table ([measure_number] [int] not null);

    insert into @report_setting
        ([profile_name], [alias], [effective_start], [effective_end], [report_number])
        select
            [report_profile].[profile_name],
            [profile_alias].[alias],
            [profile_start].[effective_date],
            [profile_end].[effective_date],
            [profile_report_number].[report_number]
        from
            (
                select distinct [ProfileName] as [profile_name] from [export].[HomeEnergyReportSetting] where [ClientId] = @clientId
            ) as [report_profile]
            inner join
            (
                -- effective start date for a report profile
                select
                    ProfileName as [profile_name],
                    cast([Value] as date) as [effective_date]
                    from
                        [export].[HomeEnergyReportSetting]
                    where
                        [Name] = 'effective_start' and
                        [ClientId] = @clientId
            ) as [profile_start] on [profile_start].[profile_name] = [report_profile].[profile_name]
            inner join
            (
                -- effective end date for a report profile
                select
                    ProfileName as [profile_name],
                    cast([Value] as date) as [effective_date]
                    from
                        [export].[HomeEnergyReportSetting]
                    where
                        [Name] = 'effective_end' and
                        [ClientId] = @clientId
            ) as [profile_end] on [profile_end].[profile_name] = [report_profile].[profile_name]
            inner join
            (
                -- alias for a report configuration
                select
                    ProfileName as [profile_name],
                    [Value] as [alias]
                    from
                        [export].[HomeEnergyReportSetting]
                    where
                        [Name] = 'report_alias' and
                        [ClientId] = @clientId
            ) as [profile_alias] on [profile_alias].[profile_name] = [report_profile].[profile_name]
            inner join
            (
                -- report number for a report configuration
                select
                    ProfileName as [profile_name],
                    cast([Value] as int) as [report_number]
                    from
                        [export].[HomeEnergyReportSetting]
                    where
                        [Name] = 'report_number' and
                        [ClientId] = @clientId
            ) as [profile_report_number] on [profile_report_number].[profile_name] = [report_profile].[profile_name]
        where
            [profile_alias].[alias] = isnull(@reportAlias, [profile_alias].[alias]);

    declare @summary_criteria table (
                                        [alias]             [varchar](100),
                                        [profile_name]      [varchar](100),
                                        [report_number]     [int]
                                    );

    -- grab the profiles associated with the report number.
    insert into @summary_criteria
        (   [alias],            [profile_name],         [report_number])
        select
            [setting].[alias],  [setting].[profile_name],   @reportNumber
            from
                (select [profile_name], [alias] from @report_setting where [report_number] = @reportNumber) as [setting];


    -- okay...we now know the report profiles we will be summarizing
    declare @alias                  [varchar](100),
            @profile_name           [varchar](100),
            @source_tablename       [varchar](400),
            @is_current             [bit] = 0,
            @sql_cmd                [varchar](4000),
            @measure_count          [int],
            @measure_number         [int],
            @measure_literal        [varchar](2),
            @new_line               [char](2) =  char(13) + char(10);


    create table #summary_data ([ReportAlias] [varchar](100), [MeasureNumber] [int], [ActionKey] [varchar](100), [GroupId] [varchar](100), [Saving] int);

    declare summary_cursor cursor local static forward_only for
        select [alias], [profile_name] from @summary_criteria;
    open summary_cursor;

    fetch next from summary_cursor into @alias, @profile_name;
    while @@fetch_status = 0 begin

        select
            @is_current = case
                when cast(getdate() as date) between [effective_start] and [effective_end] then 1
                else 0 end
            from @report_setting
            where [profile_name] = @profile_name;

        --set @source_tablename = '[export].[home_energy_report_snapshot_release_' + @alias + '_' + cast(@reportNumber as varchar) + ']';


        -- if the release snapshot does not exist then attempt to use the daily snapshot if the profile is currently in effective
        if object_id(@source_tablename) is null and @is_current = 1 set @source_tablename = '[export].[home_energy_report_snapshot_daily_' + @alias + ']';

        if object_id(@source_tablename) is null begin
            print '*** error *** data does not exist for alias ''' + @alias + ''' report number ' + cast(@reportNumber as varchar);
        end
        else begin

            select @measure_count = cast([Value] as [int])
                from [Export].[HomeEnergyReportSetting]
                where [ClientId] = @clientId and [ProfileName] = @profile_name and [Name] = 'measure_count';

            set @measure_number = 1;
            while @measure_number <= @measure_count begin

                set @measure_literal = cast(@measure_number as [varchar]);

                --  populate our content tables (note: the source table(s) columns are hard coded...this is okay because they are also hard coded in the usp_HE_Report_Create_Staging_Allcols sproc...IE
                --  these columns are not as configurable as we would like them to be)
                set @sql_cmd =
                    'insert into #summary_data([ReportAlias], [MeasureNumber], [ActionKey], [GroupId], [Saving])
                        select ''
                        ' + @alias + ''',
                        ' + @measure_literal + ',
                        [MTitle' + @measure_literal + '],
                        [GroupID],
                        [MSavings' + @measure_literal + ']
                        from ' + @source_tablename +
                    ' where isnumeric(isnull(MSavings' + @measure_literal + ', '''')) = 1;';

                exec(@sql_cmd);

                if @@rowcount > 0 and not exists (select * from @measures_with_value where [measure_number] = @measure_number) begin
                    insert into @measures_with_value([measure_number]) select @measure_number;
                end

                set @measure_number += 1;

            end

        end

        fetch next from summary_cursor into @alias, @profile_name;
    end
    close summary_cursor;
    deallocate summary_cursor;

    -- okay, we have populated our temp table...lets summarize for each measure


    declare @pivot_columns [varchar](4000),
            @pivot_totals [varchar](4000),
            @output_columns [varchar](4000),
            @summary_columns [varchar](4000),
            @total_calculation [varchar](4000),
            @sql_template [varchar](8000);

    declare measure_cursor cursor local static forward_only for
        select [measure_number] from @measures_with_value order by [measure_number];
    open measure_cursor;

    fetch next from measure_cursor into @measure_number;
    while @@fetch_status = 0 begin

        set @measure_literal = cast(@measure_number as [varchar]);

        set @pivot_columns = null;
        set @pivot_totals = null;
        set @output_columns = null;
        set @summary_columns = null;
        set @total_calculation = null;
        set @sql_template = null;

        select
            @pivot_columns = isnull(@pivot_columns + ', ' ,'') + '[' + [GroupId] + ']',
            @pivot_totals = isnull(@pivot_totals + ', ' ,'') +
                'sum(isnull([pivot].[' + [GroupId] + '], 0)) as ['  + [GroupId] + ']',

            @output_columns = isnull(@output_columns + @new_line +',' ,'') +
                'format(isnull([output].[' + [GroupId] + '], 0), ''N0'') as ['  + [GroupId] + ' Count]',
            @summary_columns = isnull(@summary_columns + @new_line +',' ,'') +
                '[pivot].[' + [GroupId] + '] as ['  + [GroupId] + ']',
            @total_calculation = isnull(@total_calculation + ' + ' , '') +
                'isnull([output].[' + [GroupId] + '], 0)'
            from #summary_data
            group by [GroupId]
            order by [GroupId];


        set @sql_template =
            ' select
                [output].[__SUMMARY_COLUMN__] as [__SUMMARY_HEADING__],' + @new_line + @output_columns + @new_line +
                ',format(' + @total_calculation + ', ''N0'') as [Total (M' + @measure_literal + ')] ' +
            '
                from
                    (
                        (
                            select
                                0 as [rank],
                                [pivot].[__SUMMARY_COLUMN__], ' + @new_line + @summary_columns +
                '
                            from
                                (
                                    select
                                        [__SUMMARY_COLUMN__],
                                        [GroupId],
                                        [Saving]
                                    from
                                        #summary_data
                                    where [MeasureNumber] = ' + @measure_literal + '
                                ) as [data]
                                pivot
                                (
                                    count([Saving])
                                    for [GroupId]  in (' + @pivot_columns + ')
                                ) as [pivot]
                        )
                        union all
                        (
                            select
                                1 as [rank],
                                ''Total (M' + @measure_literal + ')'' as [__SUMMARY_COLUMN__], ' + @new_line + @pivot_totals +
                '
                            from
                                (
                                    select
                                        [__SUMMARY_COLUMN__],
                                        [GroupId],
                                        [Saving]
                                    from
                                        #summary_data
                                    where [MeasureNumber] = ' + @measure_literal + '
                                ) as [data]
                                pivot
                                (
                                    count([Saving])
                                    for [GroupId]  in (' + @pivot_columns + ')
                                ) as [pivot]
                        )
                    ) as [output]
                order by
                    [output].[rank] asc,
                    [output].[__SUMMARY_COLUMN__] asc;';

        -- the first query will return a summary for each action...therefore replace __SUMMARY_COLUMN__ with ActionKey and __SUMMARY_HEADING__ with Action to create our sql command
        set @sql_cmd = replace( replace(@sql_template, '__SUMMARY_COLUMN__', 'ActionKey' ), '__SUMMARY_HEADING__', 'Action');
        exec(@sql_cmd);

        -- the second query will return a summary for each alias
        -- the first query will return a summary for each report alias...therefore replace __SUMMARY_COLUMN__ with ReportAlias and __SUMMARY_HEADING__ with Report to create our sql command
        set @sql_cmd = replace( replace(@sql_template, '__SUMMARY_COLUMN__', 'ReportAlias'), '__SUMMARY_HEADING__', 'Report');
        exec(@sql_cmd);


        fetch next from measure_cursor into @measure_number;
    end
end
