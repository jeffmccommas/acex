

/********************************************************/
/***** RUN ONLY ON THE InsightsDW_80 DATABASE ***/
/********************************************************/

USE [InsightsDW_80];
GO

/****** Object:  StoredProcedure [dbo].[ConvertBillDataToXML_80]    Script Date: 7/17/2017 11:18:46 AM ******/
DROP PROCEDURE IF EXISTS [dbo].[ConvertBillDataToXML_80];
GO

/****** Object:  StoredProcedure [dbo].[ConvertBillDataToXML_224]    Script Date: 7/17/2017 11:18:46 AM ******/
DROP PROCEDURE IF EXISTS [dbo].[ConvertBillDataToXML_224];
GO

/****** Object:  StoredProcedure [dbo].[ConvertBillDataToXML_210]    Script Date: 7/17/2017 11:18:46 AM ******/
DROP PROCEDURE IF EXISTS [dbo].[ConvertBillDataToXML_210];
GO

/****** Object:  Table [dbo].[RawBillTemp_87]    Script Date: 7/17/2017 11:18:46 AM ******/
DROP TABLE IF EXISTS [dbo].[RawBillTemp_87];
GO

/****** Object:  Table [dbo].[RawBillTemp_80]    Script Date: 7/17/2017 11:18:46 AM ******/
DROP TABLE IF EXISTS [dbo].[RawBillTemp_80];
GO

/****** Object:  Table [dbo].[RawBillTemp_276]    Script Date: 7/17/2017 11:18:46 AM ******/
DROP TABLE IF EXISTS [dbo].[RawBillTemp_276];
GO

/****** Object:  Table [dbo].[RawBillTemp_256]    Script Date: 7/17/2017 11:18:46 AM ******/
DROP TABLE IF EXISTS [dbo].[RawBillTemp_256];
GO

/****** Object:  Table [dbo].[RawBillTemp_224]    Script Date: 7/17/2017 11:18:46 AM ******/
DROP TABLE IF EXISTS [dbo].[RawBillTemp_224];
GO


/****** Object:  Table [dbo].[RawBillTemp_80]    Script Date: 7/17/2017 11:18:47 AM ******/
SET ANSI_NULLS ON;
GO
SET QUOTED_IDENTIFIER ON;
GO
IF NOT EXISTS (
                  SELECT *
                  FROM   [sys].[objects]
                  WHERE  [object_id] = OBJECT_ID(N'[dbo].[RawBillTemp_80]')
                         AND [type] IN ( N'U' )
              )
BEGIN
    CREATE TABLE [dbo].[RawBillTemp_80]
        (
            [TrackingId] [VARCHAR](255) NULL
          , [ExtractDate] [VARCHAR](255) NULL
          , [AccStatus] [VARCHAR](255) NULL
          , [ContractAcct] [VARCHAR](255) NULL
          , [BusPartner] [VARCHAR](255) NULL
          , [ParentBusPartner] [VARCHAR](255) NULL
          , [AssignedCorpAccManager] [VARCHAR](255) NULL
          , [CustType] [VARCHAR](255) NULL
          , [CustClass] [VARCHAR](255) NULL
          , [GasRateCode] [VARCHAR](255) NULL
          , [ElectricRateCode] [VARCHAR](255) NULL
          , [CustFirstNameCompanyName] [VARCHAR](255) NULL
          , [CustLastName] [VARCHAR](255) NULL
          , [HouseNum] [VARCHAR](255) NULL
          , [Street] [VARCHAR](255) NULL
          , [AptNum] [VARCHAR](255) NULL
          , [City] [VARCHAR](255) NULL
          , [State] [VARCHAR](255) NULL
          , [Zipcode] [VARCHAR](255) NULL
          , [MailingAddress] [VARCHAR](255) NULL
          , [MailingCity] [VARCHAR](255) NULL
          , [MailingState] [VARCHAR](255) NULL
          , [MailingZipcode] [VARCHAR](255) NULL
          , [TelephoneNumber] [VARCHAR](255) NULL
          , [CountyName] [VARCHAR](255) NULL
          , [PlanningPlant] [VARCHAR](255) NULL
          , [EmailAddress] [VARCHAR](255) NULL
          , [DoNotSolicit] [VARCHAR](255) NULL
          , [ThreatsOfViolence] [VARCHAR](255) NULL
          , [ElecCon01] [VARCHAR](50) NULL
          , [ElecCon02] [VARCHAR](50) NULL
          , [ElecCon03] [VARCHAR](50) NULL
          , [ElecCon04] [VARCHAR](50) NULL
          , [ElecCon05] [VARCHAR](50) NULL
          , [ElecCon06] [VARCHAR](50) NULL
          , [ElecCon07] [VARCHAR](50) NULL
          , [ElecCon08] [VARCHAR](50) NULL
          , [ElecCon09] [VARCHAR](50) NULL
          , [ElecCon10] [VARCHAR](50) NULL
          , [ElecCon11] [VARCHAR](50) NULL
          , [ElecCon12] [VARCHAR](50) NULL
          , [GasCon01] [VARCHAR](50) NULL
          , [GasCon02] [VARCHAR](50) NULL
          , [GasCon03] [VARCHAR](50) NULL
          , [GasCon04] [VARCHAR](50) NULL
          , [GasCon05] [VARCHAR](50) NULL
          , [GasCon06] [VARCHAR](50) NULL
          , [GasCon07] [VARCHAR](50) NULL
          , [GasCon08] [VARCHAR](50) NULL
          , [GasCon09] [VARCHAR](50) NULL
          , [GasCon10] [VARCHAR](50) NULL
          , [GasCon11] [VARCHAR](50) NULL
          , [GasCon12] [VARCHAR](50) NULL
          , [GasEligibility] [VARCHAR](50) NULL
          , [ElectricEligibility] [VARCHAR](50) NULL
          , [ElectricPortion] [VARCHAR](8) NULL
          , [GasPortion] [VARCHAR](8) NULL
          , [PremiseNumber] [VARCHAR](255) NULL
        );
END;
GO

/****** Object:  StoredProcedure [dbo].[ConvertBillDataToXML_80]    Script Date: 7/17/2017 11:18:47 AM ******/
SET ANSI_NULLS ON;
GO
SET QUOTED_IDENTIFIER ON;
GO
IF NOT EXISTS (
                  SELECT *
                  FROM   [sys].[objects]
                  WHERE  [object_id] = OBJECT_ID(N'[dbo].[ConvertBillDataToXML_80]')
                         AND [type] IN ( N'P', N'PC' )
              )
BEGIN
    EXEC [sys].[sp_executesql] @statement = N'CREATE PROCEDURE [dbo].[ConvertBillDataToXML_80] AS';
END;
GO
-- =================================================================================================================
-- Author:      Steele
-- Create date: 06/04/2015
-- Modificatons 11/16/2016:  changed table column names to match what client is using.  Modified RawBillTemp_80 table
-- Description: This will create customer, billing and profile xml
--              for input  into bulk import
-- ==================================================================================================================
ALTER PROCEDURE [dbo].[ConvertBillDataToXML_80]
    @ClientId AS INT
  , @TrackingId AS VARCHAR(255)
AS
BEGIN
    SET NOCOUNT ON;

    BEGIN TRY
        WITH XMLNAMESPACES (
                               DEFAULT 'Aclara:Insights'
                           )
        SELECT (
                   SELECT [BusPartner] AS '@CustomerId'
                        --firstname.  If the lastname is blank, move firstname to lastname
                        , CASE WHEN LEN(LTRIM(RTRIM([CustLastName]))) = 0 THEN ''
                               ELSE [CustFirstNameCompanyName]
                          END AS '@FirstName'
                        --lastname can't be blank.  If it is blank, move the firstname into the lastname.  Firstname is optional
                        , CASE WHEN LEN(LTRIM(RTRIM([CustLastName]))) = 0 THEN [CustFirstNameCompanyName]
                               ELSE [CustLastName]
                          END AS '@LastName'
                        , [MailingAddress] AS '@Street1'
                        , [MailingCity] AS '@City'
                        , [MailingState] AS '@State'
                        , 'US' AS '@Country'
                        , CASE WHEN LEN(LTRIM(RTRIM([MailingZipcode]))) = 5 THEN
                                   CAST(SUBSTRING([MailingZipcode], 1, 5) AS VARCHAR(5))
                               ELSE
                                   CAST((SUBSTRING([MailingZipcode], 1, 5) + SUBSTRING([MailingZipcode], 7, 4)) AS VARCHAR(9))
                          END AS '@PostalCode'
                        , NULLIF(REPLACE([TelephoneNumber], '-', ''), '') AS '@PhoneNumber'
                        , CASE WHEN LEN([EmailAddress]) < 6 THEN NULL
                               ELSE COALESCE(NULLIF(LTRIM(RTRIM([EmailAddress])), ''), [EmailAddress])
                          END AS '@EmailAddress'
                        , NULL AS '@AlternateEmailAddress'
                        , 'utility' AS '@Source'
                        , CASE WHEN [CustClass] = 'Commercial' THEN 'true'
                               ELSE 'false'
                          END AS '@IsBusiness'
                        , 'true' AS '@IsAuthenticated'
                        , [ContractAcct] AS 'Account/@AccountId'
                        --Consumers does not always provide premiseid.  If they don't have the premiseid, use account number for premiseid
                        , [PremiseNumber] AS 'Account/Premise/@PremiseId'
                        , LTRIM(RTRIM([HouseNum])) + RTRIM(ISNULL(' ' + [Street], ''))
                          + RTRIM(ISNULL(' ' + [HouseNum], '')) AS 'Account/Premise/@PremiseStreet1'
                        , [City] AS 'Account/Premise/@PremiseCity'
                        , CASE WHEN LEN(LTRIM(RTRIM([Zipcode]))) < 5 THEN NULL
                               WHEN LEN(LTRIM(RTRIM([Zipcode]))) = 5 THEN
                                   CAST(SUBSTRING([Zipcode], 1, 5) AS VARCHAR(5))
                               WHEN LEN(LTRIM(RTRIM([Zipcode]))) = 10 THEN
                                   CAST((SUBSTRING([Zipcode], 1, 5) + SUBSTRING([Zipcode], 7, 4)) AS VARCHAR(9))
                               ELSE NULL
                          END AS 'Account/Premise/@PremisePostalCode'
                        , CAST(LTRIM(RTRIM([State])) AS VARCHAR(2)) AS 'Account/Premise/@PremiseState'
                        , 'US' AS 'Account/Premise/@PremiseCountry'
                        , 'true' AS 'Account/Premise/@HasElectricService'
                        , 'true' AS 'Account/Premise/@HasGasService'
                        , 'false' AS 'Account/Premise/@HasWaterService'
                   FROM   [dbo].[RawBillTemp_80]
                   WHERE  [TrackingId] = @TrackingId
                   FOR XML PATH('Customer'), ELEMENTS, ROOT('Customers')
               ) AS [XmlData]
             , 'Customer' AS [XmlType]
        UNION
        --Electric Account Billing

        SELECT (
                   SELECT *
                   FROM   (
                              SELECT [BusPartner] AS '@CustomerId'
                                   , 'utility' AS '@Source'
                                   , [ContractAcct] AS 'Account/@AccountId'
                                   , '1' AS 'Account/Bill/@BillPeriodType'
                                   , CASE WHEN [ElectricPortion] = '99' THEN '01'
                                          ELSE [ElectricPortion]
                                     END AS 'Account/Bill/@BillCycleScheduleId'
                                   --Start Date
                                   , (
                                         SELECT CAST(([bill_enddate] - [bill_days]) AS DATETIME)
                                         FROM   [dbo].[RawBillCycleSchedule_80] [sched]
                                         WHERE  [schedule_id] = CASE WHEN [ElectricPortion] = '99' THEN '01'
                                                                     ELSE [ElectricPortion]
                                                                END
                                                AND ([bill_month]) = RIGHT([Consumptions], 2)
                                                AND [bill_year] = CASE WHEN (DATEPART(mm, [ExtractDate])
                                                                             - RIGHT([Consumptions], 2)
                                                                            ) < 1 THEN
                                                                           DATEPART(YEAR, DATEADD(YEAR, -1, [ExtractDate]))
                                                                       ELSE DATEPART(YEAR, [ExtractDate])
                                                                  END
                                     ) AS 'Account/Bill/@StartDate'

                                   --End Date
                                   , (
                                         SELECT CAST([bill_enddate] AS DATETIME)
                                         FROM   [dbo].[RawBillCycleSchedule_80] [sched]
                                         WHERE  [schedule_id] = CASE WHEN [ElectricPortion] = '99' THEN '01'
                                                                     ELSE [ElectricPortion]
                                                                END
                                                AND ([bill_month]) = RIGHT([Consumptions], 2)
                                                AND [bill_year] = CASE WHEN (DATEPART(mm, [ExtractDate])
                                                                             - RIGHT([Consumptions], 2)
                                                                            ) < 1 THEN
                                                                           DATEPART(YEAR, DATEADD(YEAR, -1, [ExtractDate]))
                                                                       ELSE DATEPART(YEAR, [ExtractDate])
                                                                  END
                                     ) AS 'Account/Bill/@EndDate'
                                   --PremiseID
                                   , [PremiseNumber] AS 'Account/Bill/Premise/@PremiseId'
                                   --Total Usage
                                   , (
                                         SELECT CASE WHEN ISNUMERIC([ElectricConsumption]) = 1 THEN
                                                         CAST([ElectricConsumption] AS DECIMAL(18, 2))
                                                     ELSE 0
                                                END
                                     ) AS 'Account/Bill/Premise/Service/@TotalUsage'
                                   --Total Cost
                                   , (
                                         SELECT CASE WHEN ISNUMERIC([ElectricConsumption]) = 1 THEN
                                                         CASE WHEN (
                                                                       SELECT CAST([bill_enddate] AS DATE)
                                                                       FROM   [dbo].[RawBillCycleSchedule_80] [sched]
                                                                       WHERE  [schedule_id] = [ElectricPortion]
                                                                              AND SUBSTRING([Consumptions], 8, 2) = [bill_month]
                                                                                                                    - 1
                                                                              AND [bill_year] = DATEPART(
                                                                                                            YEAR
                                                                                                          , [ExtractDate]
                                                                                                        )
                                                                   ) > CAST('2016-04-21' AS DATE) THEN
                                                                  CAST([ElectricConsumption] AS DECIMAL(18, 2)) * 2.7937
                                                              ELSE CAST([ElectricConsumption] AS DECIMAL(18, 2)) * 2.7021
                                                         END
                                                     ELSE 0
                                                END
                                     ) AS 'Account/Bill/Premise/Service/@TotalCost'
                                   --Commodity
                                   , '1' AS 'Account/Bill/Premise/Service/@Commodity'
                                   --UOM
                                   , '1' AS 'Account/Bill/Premise/Service/@UOM'
                                   --Servicepoint and servicecontract.  Both are made up for Consumers
                                   , [PremiseNumber] AS 'Account/Bill/Premise/Service/@ServicePointId'
                                   , 'SC_' + [PremiseNumber] + '_1' AS 'Account/Bill/Premise/Service/@ServiceContractId'
                                   --Rate Class
                                   , [ElectricRateCode] AS 'Account/Bill/Premise/Service/@RateClass'
                              FROM   (
                                         SELECT *
                                         FROM   [dbo].[RawBillTemp_80]
                                         WHERE  [TrackingId] = @TrackingId
                                     ) AS [e] UNPIVOT([ElectricConsumption] FOR [Consumptions] IN([ElecCon01], [ElecCon02], [ElecCon03], [ElecCon04], [ElecCon05], [ElecCon06], [ElecCon07], [ElecCon08], [ElecCon09], [ElecCon10], [ElecCon11], [ElecCon12])) AS [elec]
                              WHERE  (
                                         [CustType] = 'E'
                                         OR [CustType] = 'C'
                                     )
                              UNION
                              --Gas Account Billing
                              SELECT [BusPartner] AS '@CustomerId'
                                   , 'utility' AS '@Source'
                                   , [ContractAcct] AS 'Account/@AccountId'
                                   , '1' AS 'Account/Bill/@BillPeriodType'
                                   , CASE WHEN [GasPortion] = '99' THEN '01'
                                          ELSE [GasPortion]
                                     END AS 'Account/Bill/@BillCycleScheduleId'
                                   --Start Date
                                   , (
                                         SELECT CAST(([bill_enddate] - [bill_days]) AS DATETIME)
                                         FROM   [dbo].[RawBillCycleSchedule_80] [sched]
                                         WHERE  [schedule_id] = CASE WHEN [GasPortion] = '99' THEN '01'
                                                                     ELSE [GasPortion]
                                                                END
                                                AND ([bill_month]) = RIGHT([Consumptions], 2)
                                                AND [bill_year] = CASE WHEN (DATEPART(mm, [ExtractDate])
                                                                             - RIGHT([Consumptions], 2)
                                                                            ) < 1 THEN
                                                                           DATEPART(YEAR, DATEADD(YEAR, -1, [ExtractDate]))
                                                                       ELSE DATEPART(YEAR, [ExtractDate])
                                                                  END
                                     ) AS 'Account/Bill/@StartDate'
                                   --End Date
                                   , (
                                         SELECT CAST([bill_enddate] AS DATETIME)
                                         FROM   [dbo].[RawBillCycleSchedule_80] [sched]
                                         WHERE  [schedule_id] = CASE WHEN [GasPortion] = '99' THEN '01'
                                                                     ELSE [GasPortion]
                                                                END
                                                AND ([bill_month]) = RIGHT([Consumptions], 2)
                                                AND [bill_year] = CASE WHEN (DATEPART(mm, [ExtractDate])
                                                                             - RIGHT([Consumptions], 2)
                                                                            ) < 1 THEN
                                                                           DATEPART(YEAR, DATEADD(YEAR, -1, [ExtractDate]))
                                                                       ELSE DATEPART(YEAR, [ExtractDate])
                                                                  END
                                     ) AS 'Account/Bill/@EndDate'

                                   --PremiseID
                                   , [PremiseNumber] AS 'Account/Bill/Premise/@PremiseId'
                                   --Total Usage
                                   , (
                                         SELECT CASE WHEN ISNUMERIC([GasConsumption]) = 1 THEN
                                                         CAST([GasConsumption] AS DECIMAL(18, 2))
                                                     ELSE 0
                                                END
                                     ) AS 'Account/Bill/Premise/Service/@TotalUsage'
                                   --Total Cost
                                   , (
                                         SELECT CASE WHEN ISNUMERIC([GasConsumption]) = 1 THEN
                                                         CASE WHEN (
                                                                       SELECT CAST([bill_enddate] AS DATE)
                                                                       FROM   [dbo].[RawBillCycleSchedule_80] [sched]
                                                                       WHERE  [schedule_id] = [GasPortion]
                                                                              AND SUBSTRING([Consumptions], 8, 2) = [bill_month]
                                                                              AND [bill_year] = DATEPART(
                                                                                                            YEAR
                                                                                                          , [ExtractDate]
                                                                                                        )
                                                                   ) > CAST('2016-04-21' AS DATE) THEN
                                                                  CAST([GasConsumption] AS DECIMAL(18, 2)) * 2.7937
                                                              ELSE CAST([GasConsumption] AS DECIMAL(18, 2)) * 2.7021
                                                         END
                                                     ELSE 0
                                                END
                                     ) AS 'Account/Bill/Premise/Service/@TotalCost'
                                   --Commodity
                                   , '2' AS 'Account/Bill/Premise/Service/@Commodity'
                                   --UOM
                                   , '2' AS 'Account/Bill/Premise/Service/@UOM'
                                   --Servicepoint and servicecontract.  Both are made up for Consumers
                                   , [PremiseNumber] AS 'Account/Bill/Premise/Service/@ServicePointId'
                                   , 'SC_' + [PremiseNumber] + '_1' AS 'Account/Bill/Premise/Service/@ServiceContractId'
                                   --Rate Class
                                   , [GasRateCode] AS 'Account/Bill/Premise/Service/@RateClass'
                              FROM   (
                                         SELECT *
                                         FROM   [dbo].[RawBillTemp_80]
                                         WHERE  [TrackingId] = @TrackingId
                                     ) AS [g] UNPIVOT([GasConsumption] FOR [Consumptions] IN([GasCon01], [GasCon02], [GasCon03], [GasCon04], [GasCon05], [GasCon06], [GasCon07], [GasCon08], [GasCon09], [GasCon10], [GasCon11], [GasCon12])) AS [gas]
                              WHERE  (
                                         [CustType] = 'G'
                                         OR [CustType] = 'C'
                                     )
                          ) [a]
                   FOR XML PATH('Customer'), ELEMENTS, ROOT('Customers')
               ) AS [XmlData]
             , 'Bill' AS [XmlType]
        UNION
        --Premise Data
        SELECT
            /** Customer Element **/
            (
                SELECT [BusPartner] AS '@CustomerId'
                     /** Account Element **/
                     , [ContractAcct] AS 'Account/@AccountId'
                     , [PremiseNumber] AS 'Account/Premise/@PremiseId'
                     /** ProfileItem Element **/
                     , (
                           SELECT 'premisetype' AS 'ProfileItem/@AttributeKey'
                                , CASE WHEN LTRIM(RTRIM([CustClass])) = 'COM' THEN 'premisetype.business'
                                       ELSE 'premisetype.residential'
                                  END AS 'ProfileItem/@AttributeValue'
                                , 'utility' AS 'ProfileItem/@Source'
                                , GETDATE() AS 'ProfileItem/@ModifiedDate'
                           FOR XML PATH(''), TYPE, ELEMENTS
                       ) AS 'Account/Premise'
                     , (
                           SELECT 'customer.segment' AS 'ProfileItem/@AttributeKey'
                                , LTRIM(RTRIM([CustType])) AS 'ProfileItem/@AttributeValue'
                                , 'utility' AS 'ProfileItem/@Source'
                                , GETDATE() AS 'ProfileItem/@ModifiedDate'
                           FOR XML PATH(''), TYPE, ELEMENTS
                       ) AS 'Account/Premise'
                FROM   [dbo].[RawBillTemp_80]
                WHERE  [TrackingId] = @TrackingId
                FOR XML PATH('Customer'), ELEMENTS, ROOT('Customers')
            ) AS [XmlData]
          , 'Profile' AS [XmlType];

    END TRY
    BEGIN CATCH
        THROW;
    END CATCH;

END;



GO
