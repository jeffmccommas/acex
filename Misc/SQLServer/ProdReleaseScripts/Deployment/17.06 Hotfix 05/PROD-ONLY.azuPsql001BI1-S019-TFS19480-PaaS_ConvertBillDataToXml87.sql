USE [msdb]
GO

/****** Object:  Job [PaaS_ConvertBillDataToXml_87]    Script Date: 7/17/2017 3:07:46 PM ******/
EXEC msdb.dbo.sp_delete_job @job_id=N'f7d53f56-7ae1-4ab6-8c02-1226bd856964', @delete_unused_schedule=1
GO

/****** Object:  Job [PaaS_ConvertBillDataToXml_87]    Script Date: 7/17/2017 3:07:46 PM ******/
BEGIN TRANSACTION
DECLARE @ReturnCode INT
SELECT @ReturnCode = 0
/****** Object:  JobCategory [CE_UserApp]    Script Date: 7/17/2017 3:07:46 PM ******/
IF NOT EXISTS (SELECT name FROM msdb.dbo.syscategories WHERE name=N'CE_UserApp' AND category_class=1)
BEGIN
EXEC @ReturnCode = msdb.dbo.sp_add_category @class=N'JOB', @type=N'LOCAL', @name=N'CE_UserApp'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback

END

DECLARE @jobId BINARY(16)
EXEC @ReturnCode =  msdb.dbo.sp_add_job @job_name=N'PaaS_ConvertBillDataToXml_87', 
		@enabled=1, 
		@notify_level_eventlog=0, 
		@notify_level_email=2, 
		@notify_level_netsend=0, 
		@notify_level_page=0, 
		@delete_level=0, 
		@description=N'No description available.', 
		@category_name=N'CE_UserApp', 
		@owner_login_name=N'sa', 
		@notify_email_operator_name=N'ACE BI PROD', @job_id = @jobId OUTPUT
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
/****** Object:  Step [Execute package]    Script Date: 7/17/2017 3:07:46 PM ******/
EXEC @ReturnCode = msdb.dbo.sp_add_jobstep @job_id=@jobId, @step_name=N'Execute package', 
		@step_id=1, 
		@cmdexec_success_code=0, 
		@on_success_action=1, 
		@on_success_step_id=0, 
		@on_fail_action=2, 
		@on_fail_step_id=0, 
		@retry_attempts=0, 
		@retry_interval=0, 
		@os_run_priority=0, @subsystem=N'SSIS', 
		@command=N'/ISSERVER "\"\SSISDB\BI\Aclara.Preprocess.BillingData\BillingData.dtsx\"" /SERVER "\"azuPsql001\bi1\"" /Par "\"$Project::ClientID(Int32)\"";87 /Par "\"$Project::CM_InsightsBulk_ConnectionString\"";"\"Data Source=acePsql1c0.database.windows.net;User ID=AclaraCEAdmin@acePsql1c0;Password=0S3att!30;Initial Catalog=InsightsDW_Test;Encrypt=true;Persist Security Info=True;\"" /Par "\"$Project::CM.InsightsBulk.InitialCatalog\"";"\"InsightsDW_Test\"" /Par "\"$Project::CM.InsightsBulk.Password\"";"\"0S3att!30\"" /Par "\"$Project::CM.InsightsBulk.ServerName\"";"\"acePsql1c0.database.windows.net\"" /Par "\"$Project::CM.InsightsBulk.UserName\"";"\"AclaraCEAdmin@acePsql1c0\"" /Par "\"CM.InsightsMetadata.ConnectionString\"";"\"Data Source=acePsql1c0.database.windows.Net;Initial Catalog=InsightsMetaData;User ID=AclaraCEAdmin@acePsql1c0;Password=0S3att!30;Encrypt=true;Persist Security Info=True;\"" /Par "\"CM.InsightsMetadata.Password\"";"\"0S3att!30\"" /Par "\"CM.InsightsMetadata.ServerName\"";"\"acePsql1c0.database.windows.net\"" /Par "\"CM.InsightsMetadata.UserName\"";"\"AclaraCEAdmin@acePsql1c0\"" /Par "\"$ServerOption::LOGGING_LEVEL(Int16)\"";2 /Par "\"$ServerOption::SYNCHRONIZED(Boolean)\"";True /CALLERINFO SQLAGENT /REPORTING E',  
		@database_name=N'master', 
		@flags=0
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_update_job @job_id = @jobId, @start_step_id = 1
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_add_jobschedule @job_id=@jobId, @name=N'Every 5 minutes', 
		@enabled=1, 
		@freq_type=4, 
		@freq_interval=1, 
		@freq_subday_type=4, 
		@freq_subday_interval=5, 
		@freq_relative_interval=0, 
		@freq_recurrence_factor=0, 
		@active_start_date=20150902, 
		@active_end_date=99991231, 
		@active_start_time=0, 
		@active_end_time=235959, 
		@schedule_uid=N'f1907652-b919-4417-9666-ebac97843652'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_add_jobserver @job_id = @jobId, @server_name = N'(local)'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
COMMIT TRANSACTION
GOTO EndSave
QuitWithRollback:
    IF (@@TRANCOUNT > 0) ROLLBACK TRANSACTION
EndSave:
GO


