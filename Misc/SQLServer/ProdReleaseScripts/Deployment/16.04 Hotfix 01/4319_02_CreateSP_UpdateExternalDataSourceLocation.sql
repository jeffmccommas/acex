
/****** Object:  StoredProcedure [dbo].[UpdateExternalDataSourceLocation]    Script Date: 5/17/2016 2:09:04 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ubaid
-- Create date: May 4th, 2016
-- Description:	This script updates external data source location by dropping and re-creating external tables
-- Pass complete server name "acedsql1c0.database.windows.net"
-- =============================================
CREATE PROCEDURE [dbo].[UpdateExternalDataSourceLocation]
    @ServerLocation AS VARCHAR(256)
AS
    BEGIN
        SET NOCOUNT ON;

        DECLARE @dropDSScript AS NVARCHAR(MAX);
        DECLARE @createDSScript AS NVARCHAR(MAX);
        DECLARE @dropExternTblScript AS NVARCHAR(MAX);
        DECLARE @createExternTblScript AS NVARCHAR(MAX);

		-- Run script only if the existing server location doesn't match with the new location
        IF EXISTS ( SELECT  *
                    FROM    sys.external_data_sources
                    WHERE   LOWER(location) != LOWER(@ServerLocation) )
            BEGIN

				-- Read exiting external data sources and create external data source creation script pointing to new server location
                SELECT  @createDSScript = STUFF(( SELECT    'CREATE EXTERNAL DATA SOURCE '
                                                            + ds.name
                                                            + ' WITH (TYPE='
                                                            + ds.type_desc
                                                            + ',LOCATION='''
                                                            + @ServerLocation
                                                            + ''',DATABASE_NAME='''
                                                            + database_name
                                                            + ''',CREDENTIAL='
                                                            + dc.name
                                                            + ',SHARD_MAP_NAME='''
                                                            + ds.shard_map_name
                                                            + ''');'
                                                  FROM      sys.external_data_sources ds
                                                            INNER JOIN sys.database_credentials dc ON dc.credential_id = ds.credential_id
                                                FOR
                                                  XML PATH('')
                                                ), 1, 0, '');

				--Create drop script for existing external data sources
                SELECT  @dropDSScript = STUFF(( SELECT  'DROP EXTERNAL DATA SOURCE '
                                                        + ds.name + ';'
                                                FROM    sys.external_data_sources ds
                                              FOR
                                                XML PATH('')
                                              ), 1, 0, '');

				--Create drop script for existing external tables
                SELECT  @dropExternTblScript = STUFF(( SELECT 'DROP EXTERNAL TABLE '
                                                              + '[' + s.name
                                                              + '].[' + e.name
                                                              + ']' + ';'
                                                       FROM   sys.external_tables e
                                                              INNER JOIN sys.schemas s ON s.schema_id = e.schema_id
                                                     FOR
                                                       XML PATH('')
                                                     ), 1, 0, '');

				--Read existing external tables and create external tables creation script
                WITH    ExternTables
                          AS ( SELECT   e.object_id ,
                                        '[' + s.name + '].[' + e.name + ']' AS TableName ,
                                        ds.name AS DataSource ,
                                        IIF(e.distribution_type = 1, e.distribution_desc, e.distribution_desc
                                        + '(' + c.name + ')') AS Distribution
                               FROM     sys.external_tables e
                                        INNER JOIN sys.schemas s ON s.schema_id = e.schema_id
                                        INNER JOIN sys.external_data_sources ds ON ds.data_source_id = e.data_source_id
                                        LEFT JOIN sys.columns c ON c.object_id = e.object_id
                                                              AND c.column_id = e.sharding_col_id
                             )
                    SELECT  ' CREATE EXTERNAL TABLE ' + e.TableName + ' ('
                            + ( SELECT  STUFF(( SELECT  ',[' + c.name + '] '
                                                        + t.name
                                                        + CASE
                                                              WHEN t.name = 'decimal'
                                                              THEN '('
                                                              + CONVERT(VARCHAR(5), c.precision)
                                                              + ','
                                                              + CONVERT(VARCHAR(5), c.scale)
                                                              + ')'
                                                              WHEN t.name = 'nvarchar'
                                                              OR t.name = 'nchar'
                                                              THEN '('
                                                              + IIF(c.max_length = -1, 'max', CONVERT(VARCHAR(5), c.max_length
                                                              / 2)) + ')'
                                                              WHEN t.name = 'varchar'
                                                              OR t.name = 'char'
                                                              OR t.name = 'varbinary'
                                                              THEN '('
                                                              + IIF(c.max_length = -1, 'max', CONVERT(VARCHAR(5), c.max_length))
                                                              + ')'
                                                              ELSE ''
                                                          END + ' '
                                                        + IIF(c.is_nullable = 1, 'NULL', 'NOT NULL')
                                                FROM    sys.columns c
                                                        INNER JOIN sys.types t ON t.system_type_id = c.system_type_id
                                                              AND t.user_type_id = c.user_type_id
                                                WHERE   object_id = e.object_id
                                                ORDER BY c.column_id
                                              FOR
                                                XML PATH('')
                                              ), 1, 1, '')
                              ) + ') WITH(DATA_SOURCE = ' + e.DataSource
                            + ',DISTRIBUTION=' + e.Distribution + ');' AS SqlScript
                    INTO    #CreateExternalTables
                    FROM    ExternTables e;


                SELECT  @dropExternTblScript;
                SELECT  @dropDSScript;
                SELECT  @createDSScript;

                EXEC(@dropExternTblScript);	-- drop external tables
                EXEC(@dropDSScript);		-- drop external data sources
                EXEC(@createDSScript);		-- create external data sources, they will be pointing to new server location

                DECLARE createExternTbl_Cursor CURSOR
                FOR
                    SELECT  SqlScript
                    FROM    #CreateExternalTables;

                OPEN createExternTbl_Cursor;
                FETCH NEXT FROM createExternTbl_Cursor INTO @createExternTblScript;

				-- loops through external tables stored in temp table and execute the create script. 
                WHILE @@FETCH_STATUS = 0
                    BEGIN 

                        SELECT  @createExternTblScript;
                        EXEC(@createExternTblScript);

                        FETCH NEXT FROM createExternTbl_Cursor INTO @createExternTblScript;
                    END;
        
                CLOSE createExternTbl_Cursor;
                DEALLOCATE createExternTbl_Cursor;

                DROP TABLE #CreateExternalTables;
		
            END;
    END;
