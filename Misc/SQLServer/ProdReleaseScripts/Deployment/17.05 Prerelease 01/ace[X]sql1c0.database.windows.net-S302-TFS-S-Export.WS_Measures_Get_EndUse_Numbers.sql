﻿

/********************************************************/
/***** RUN ON THE INSIGHTSDW_XXX DATABASE SHARDS ***/
/***** including InsightsDW_Test ************************/
/********************************************************/

/****** Object:  StoredProcedure [Export].[WS_Get_EndUse_Numbers]    Script Date: 5/3/2017 9:30:58 AM ******/
IF EXISTS ( SELECT  *
            FROM    [sys].[objects]
            WHERE   [object_id] = OBJECT_ID(N'[Export].[WS_Measures_Get_EndUse_Numbers]')
                    AND [type] IN (N'P', N'PC') )
    DROP PROCEDURE [Export].[WS_Measures_Get_EndUse_Numbers];
GO

/****** Object:  StoredProcedure [Export].[WS_Measures_Get_EndUse_Numbers]    Script Date: 5/3/2017 9:30:58 AM ******/
SET ANSI_NULLS ON;
GO
SET QUOTED_IDENTIFIER ON;
GO

IF NOT EXISTS ( SELECT  *
                FROM    [sys].[objects]
                WHERE   [object_id] = OBJECT_ID(N'[Export].[WS_Measures_Get_EndUse_Numbers]')
                        AND [type] IN (N'P', N'PC') )
BEGIN
    EXEC [sys].[sp_executesql] @statement = N'CREATE PROCEDURE [Export].[WS_Measures_Get_EndUse_Numbers] AS';
END;
GO

-------------------------------------------------------------------------------
--  Name:               [Export].[WS_Measures_Get_EndUse_Numbers]
--  Author:             Philip Victor
--
-------------------------------------------------------------------------------
ALTER PROCEDURE [Export].[WS_Measures_Get_EndUse_Numbers]
    @profile_name VARCHAR(50)
   ,@run_id UNIQUEIDENTIFIER
    WITH RECOMPILE  -- force regeneration of query plan
AS

    BEGIN

        SET NOCOUNT ON;

        DECLARE @count INT = 1
           ,@fuel_type VARCHAR(50)
           ,@sqlcmd VARCHAR(8000)
           ,@energyObjectCategory VARCHAR(50);

        DECLARE @clientID INT
           ,@fuel_type_list VARCHAR(100)
           ,@energyobjectcategory_List VARCHAR(1024)
           ,@period VARCHAR(10)
           ,@season_List VARCHAR(100)
           ,@component VARCHAR(400) = '[Export].[WS_Get_EndUse_numbers]'
           ,@progress_msg VARCHAR(5000);


        SELECT  @clientID = [ClientId]
               ,@fuel_type_list = [FuelType]        --  this sproc is treating this field as a list of fuels...further investigation is required to see if the other sprocs do the same!
               ,@energyobjectcategory_List = [EnergyObjectCategoryList]
               ,@period = [period]
               ,@season_List = [SeasonList]
        FROM    Export.[WSReportProfile]
        WHERE   [ProfileName] = @profile_name;


        IF OBJECT_ID('tempdb..#WS_EnergyObjectCategoryList') IS NOT NULL
            DROP TABLE [#WS_EnergyObjectCategoryList];

        IF OBJECT_ID('tempdb..#WS_EnergyObject_Cost') IS NOT NULL
            DROP TABLE [#WS_EnergyObject_Cost];

        IF OBJECT_ID('tempdb..#WS_EnergyObject_TotalCost') IS NOT NULL
            DROP TABLE [#WS_EnergyObject_TotalCost];

        IF OBJECT_ID('tempdb..#WS_Fuel_Type_List') IS NOT NULL
            DROP TABLE [#WS_Fuel_Type_List];

        CREATE TABLE [#WS_EnergyObjectCategoryList]
            (
             [energyObjectCategory] VARCHAR(50)
            ,[TotalDollars] DECIMAL(18, 2)
            );
        INSERT  [#WS_EnergyObjectCategoryList]
                (
                 [energyObjectCategory]
                )
                SELECT  CAST ([param] AS VARCHAR(50))
                FROM    [dbo].[ufn_split_values](@energyobjectcategory_List, ',');

        CREATE TABLE [#WS_Fuel_Type_List]
            (
             [fuel_type] VARCHAR(50)
            );
        INSERT  [#WS_Fuel_Type_List]
                (
                 [fuel_type]
                )
                SELECT  CAST ([param] AS VARCHAR(50))
                FROM    [dbo].[ufn_split_values](@fuel_type_list, ',');

        CREATE TABLE [#WS_EnergyObject_Cost]
            (
             [PremiseKey] [INT]
            ,[EnergyObjectCategory] [VARCHAR](50)
            ,[Dollars] [DECIMAL](18, 2)
            ,[Percentage] [DECIMAL](18, 2)
            );
        CREATE UNIQUE CLUSTERED INDEX [IX_WS_EnergyObject_Cost] ON [#WS_EnergyObject_Cost]([PremiseKey], [EnergyObjectCategory]);

        CREATE TABLE [#WS_EnergyObject_TotalCost]
            (
             [PremiseKey] [INT]
            ,[TotalDollars] [DECIMAL](18, 2)
            );
        CREATE UNIQUE CLUSTERED INDEX [IX_WS_EnergyObject_TotalCost] ON [#WS_EnergyObject_TotalCost]([PremiseKey]);

        IF OBJECT_ID('[Export].[WS_Fact_EnergyObject]') IS NOT NULL
            DROP TABLE [Export].[WS_Fact_EnergyObject];

        CREATE TABLE [Export].[WS_Fact_EnergyObject]
            (
             [EnergyObjectKey] VARCHAR(256) NOT NULL
            ,[PremiseKey] [INT] NOT NULL
            ,[EnergyObjectGroupKey] VARCHAR(256) NOT NULL
            ,[CostElectric] [DECIMAL](18, 4) NULL
            ,[CostGas] [DECIMAL](18, 4) NULL
            ,[CostWater] [DECIMAL](18, 4) NULL
            ,[CostPropane] [DECIMAL](18, 4) NULL
            ,[CostOil] [DECIMAL](18, 4) NULL
            ,[CostWood] [DECIMAL](18, 4) NULL
            ,[Cost] [DECIMAL](18, 4) NOT NULL
            ,[UsageElectric] [DECIMAL](18, 4) NOT NULL   DEFAULT 0
            ,[UsageGas] [DECIMAL](18, 4) NOT NULL DEFAULT 0
            ,[UsageWater] [DECIMAL](18, 4) NOT NULL DEFAULT 0
            ,[UsagePropane] [DECIMAL](18, 4) NOT NULL DEFAULT 0
            ,[UsageOil] [DECIMAL](18, 4) NOT NULL DEFAULT 0
            ,[UsageWood] [DECIMAL](18, 4) NOT NULL DEFAULT 0
            ,[Count] [INT] NULL
            ,[UnitCount] [INT] NULL
            ,[BaseUsageElectric] [DECIMAL](18, 4) NULL
            ,[BaseUsageGas] [DECIMAL](18, 4) NULL
            ,[BaseUsageWater] [DECIMAL](18, 4) NULL
            ,[BaseUsagePropane] [DECIMAL](18, 4) NULL
            ,[BaseUsageOil] [DECIMAL](18, 4) NULL
            ,[BaseUsageWood] [DECIMAL](18, 4) NULL
            ,[BaseCost] [DECIMAL](18, 4) NULL
            ,[BaseType] [VARCHAR](20) NULL
            ,[Points] [INT] NULL
            ,[CO2] [NCHAR](10) NULL
            );

-- create entries in the [Export].[WS_Fact_EnergyObject] table for the premise based on the report profile's time period.
        SET @progress_msg = 'start   populate [Export].[WS_Fact_EnergyObject] @period:''' + @period + '''';
        EXEC [Export].[WS_Measures_RecordProgress] @runId = @run_id, @component = @component, @text = @progress_msg;


        INSERT  INTO [Export].[WS_Fact_EnergyObject]
                (
                 [EnergyObjectKey]
                ,[PremiseKey]
                ,[EnergyObjectGroupKey]
                ,[CostElectric]
                ,   -- we have hard code CommodityKey = 'electrict'...therefore this is the column to update.
                 [CostGas]
                ,[Cost]
                )
                SELECT  REPLACE([enduse].[EnduseKey], 'enduse.', '') AS [EnergyObjectKey]
                       ,[premise].[Premisekey] AS [PremiseKey]
                       ,[enduse].[EnduseCategoryKey] AS [EnergyObjectGroupKey]
                       ,CASE
                             WHEN @period = 'monthly' THEN [measure].[UpfrontCost] / 12 -- monthly report
                             WHEN @period = 'annual' THEN [measure].[UpfrontCost]       -- annual report
                             ELSE 0
                        END AS [CostElectric]
                       ,CASE WHEN @period = 'monthly' THEN [measure].[UpfrontCost] / 12 -- monthly report
                             WHEN @period = 'annual' THEN [measure].[UpfrontCost]       -- annual report
                             ELSE 0
                        END AS [CostGas]
                       ,[measure].[UpfrontCost] AS [Cost]
                FROM  [dbo].[FactMeasure] AS [measure]
                        INNER JOIN [dbo].[DimPremise] AS [premise] ON [premise].[PremiseId] = [measure].[PremiseId]
                                                                      AND [premise].[AccountId] = [measure].[AccountId]
                        INNER JOIN [Export].[WS_Measures_StagingAllColumns] AS [staging] -- we will only look at actions for premises that will be on the report
                        ON [staging].[PremiseId] = [premise].[PremiseId]
                           AND [staging].[Account_Number] = [premise].[AccountId]
                        INNER JOIN [Export].[WS_ClientAction] AS [action] -- note: export.ClientAction will contain data for a single client
                        ON [action].[ActionKey] = [measure].[ActionKey]
                        INNER JOIN [Export].[WS_ClientActionAppliance] AS [appliance] ON [appliance].[ClientActionID] = [action].[ClientActionID]
                        INNER JOIN [Export].[WS_ClientEnduseAppliance] AS [enduse_appliance] ON [enduse_appliance].[ApplianceKey] = [appliance].[ApplianceKey]
                        INNER JOIN [Export].[WS_ClientEnduse] AS [enduse] ON [enduse].[ClientEnduseID] = [enduse_appliance].[ClientEnduseID]
                WHERE   [measure].[CommodityKey] IN (@fuel_type_list, 'all')
                        AND [measure].[ClientId] = @clientID;


        SELECT  @count = COUNT(*)
        FROM [Export].[WS_Fact_EnergyObject];
        SET @progress_msg = 'success populate FactEnergyObject @period:''' + @period + '''; @count:'
            + CAST(@count AS VARCHAR);
        EXEC [Export].[WS_Measures_RecordProgress] @runId = @run_id, @component = @component, @text = @progress_msg;

        ---------------------------------------------------------------------------------------------------------------
        DECLARE [fuel_type_cursor] CURSOR LOCAL STATIC FORWARD_ONLY
        FOR
            SELECT  [fuel_type]
            FROM    [#WS_Fuel_Type_List];

        OPEN [fuel_type_cursor];
        FETCH NEXT FROM [fuel_type_cursor] INTO @fuel_type;
        WHILE @@fetch_status = 0
        BEGIN

            SELECT  @sqlcmd = '
                insert #WS_EnergyObject_Cost (Dollars, PremiseKey, energyObjectCategory)
                select  sum(feo.Cost' + @fuel_type + ') as Dollars, feo.PremiseKey, eol.energyObjectCategory
                from
                        [Export].[WS_Fact_EnergyObject] feo
                        inner join #WS_EnergyObjectCategoryList eol on eol.energyObjectCategory = feo.EnergyObjectKey
                group by feo.PremiseKey, eol.energyObjectCategory';


            SELECT  @progress_msg = 'start   for ' + @fuel_type + ' @sqlcmd:' + @sqlcmd;
            EXEC [Export].[WS_Measures_RecordProgress] @runId = @run_id, @component = @component,
                @text = @progress_msg;

            EXEC (@sqlcmd);

            SELECT  @progress_msg = 'success for ' + @fuel_type + ' @sqlcmd:' + @sqlcmd;
            EXEC [Export].[WS_Measures_RecordProgress] @runId = @run_id, @component = @component,
                @text = @progress_msg;

            FETCH NEXT FROM [fuel_type_cursor] INTO @fuel_type;

        END;
        CLOSE [fuel_type_cursor];
        DEALLOCATE [fuel_type_cursor];

        ---------------------------------------------------------------------------------------------------------------
        INSERT  INTO [#WS_EnergyObject_TotalCost]
                (
                 [PremiseKey]
                ,[TotalDollars]
                )
                SELECT  [PremiseKey]
                       ,SUM([Dollars]) AS [TotalDollars]
                FROM    [#WS_EnergyObject_Cost]
                GROUP BY [PremiseKey];

        UPDATE  [eoc]
        SET     [eoc].[Percentage] = CASE WHEN [eoct].[TotalDollars] = 0 THEN 0
                                    ELSE ROUND(100 * [eoc].[Dollars] / [eoct].[TotalDollars], 0)
                               END
        FROM    [#WS_EnergyObject_Cost] AS [eoc]
                INNER JOIN [#WS_EnergyObject_TotalCost] AS [eoct] ON [eoc].[PremiseKey] = [eoct].[PremiseKey];


        ---------------------------------------------------------------------------------------------------------------
        DECLARE [energyObjectCategory_cursor] CURSOR LOCAL STATIC FORWARD_ONLY
        FOR
            SELECT  [energyObjectCategory]
            FROM    [#WS_EnergyObjectCategoryList];

        SET @count = 1;
        OPEN [energyObjectCategory_cursor];
        FETCH NEXT FROM [energyObjectCategory_cursor] INTO @energyObjectCategory;
        WHILE @@fetch_status = 0
        BEGIN

            SELECT  @progress_msg = 'start   @period:' + @period + '; @energyObjectCategory:''' + @energyObjectCategory
                    + '''';
            EXEC [Export].[WS_Measures_RecordProgress] @runId = @run_id, @component = @component,
                @text = @progress_msg;

        ---------------------------------------------------------------------------------------------------------------
            IF @period = 'annual'
            BEGIN
                SELECT  @sqlcmd = '
                        update  he
                        set EnergyObjectCategory' + CONVERT (VARCHAR, @count) + ' = ''' + @energyObjectCategory + ''' '
                                        + 'from [Export].[WS_Measures_StagingAllColumns] he';

                SELECT  @progress_msg = 'start   @sqlcmd:' + @sqlcmd;
                EXEC [Export].[WS_Measures_RecordProgress] @runId = @run_id, @component = @component,
                    @text = @progress_msg;

                EXEC (@sqlcmd);

                SELECT  @progress_msg = 'success @sqlcmd:' + @sqlcmd;
                EXEC [Export].[WS_Measures_RecordProgress] @runId = @run_id, @component = @component,
                    @text = @progress_msg;

                SELECT  @sqlcmd = '
                        update  he
                        set EnergyObjectCategoryDollar' + CONVERT (VARCHAR, @count)
                                        + ' =  convert (varchar, convert (int, round (coalesce (ec.Dollars, 0), 0))) '
                                        + ' from [Export].[WS_Measures_StagingAllColumns]  he
                                inner join [dbo].[DimPremise] p1  on p1.PremiseId = he.PremiseID
                                left join [#WS_EnergyObject_Cost] ec
                                on ec.PremiseKey = p1.PremiseKey and ec.energyObjectCategory = ''' + @energyObjectCategory + '''';

                SELECT  @progress_msg = 'start   @sqlcmd:' + @sqlcmd;
                EXEC [Export].[WS_Measures_RecordProgress] @runId = @run_id, @component = @component,
                    @text = @progress_msg;

                EXEC (@sqlcmd);

                SELECT  @progress_msg = 'success @sqlcmd:' + @sqlcmd;
                EXEC [Export].[WS_Measures_RecordProgress] @runId = @run_id, @component = @component,
                    @text = @progress_msg;

                SELECT  @sqlcmd = '
                        update  he
                        set EnergyObjectCategoryPercent' + CONVERT (VARCHAR, @count)
                                        + ' = convert (varchar, convert (int, coalesce (ec.Percentage, 0))) '
                                        + ' from [Export].[WS_Measures_StagingAllColumns] he
                                inner join [dbo].[DimPremise] p1
                                on p1.PremiseId = he.PremiseID
                                left join [#WS_EnergyObject_Cost] ec
                                on ec.PremiseKey = p1.PremiseKey and ec.energyObjectCategory = ''' + @energyObjectCategory + '''';

                SELECT  @progress_msg = 'start   @sqlcmd:' + @sqlcmd;
                EXEC [Export].[WS_Measures_RecordProgress] @runId = @run_id, @component = @component,
                    @text = @progress_msg;

                EXEC (@sqlcmd);

                SELECT  @progress_msg = 'success @sqlcmd:' + @sqlcmd;
                EXEC [Export].[WS_Measures_RecordProgress] @runId = @run_id, @component = @component,
                    @text = @progress_msg;

            END;
            ELSE            ---------------------------------------------------------------------------------------------------------------
            BEGIN

                SELECT  @sqlcmd = '
                    update  he
                    set EnergyObjectCategoryMonthly' + CONVERT (VARCHAR, @count) + ' = ''' + @energyObjectCategory + ''' '
                                    + 'from [Export].[WS_Measures_StagingAllColumns] he';

                SELECT  @progress_msg = 'start   @sqlcmd:' + @sqlcmd;
                EXEC [Export].[WS_Measures_RecordProgress] @runId = @run_id, @component = @component,
                    @text = @progress_msg;

                EXEC (@sqlcmd);

                SELECT  @progress_msg = 'success @sqlcmd:' + @sqlcmd;
                EXEC [Export].[WS_Measures_RecordProgress] @runId = @run_id, @component = @component,
                    @text = @progress_msg;

                SELECT  @sqlcmd = '
                        update  he
                        set EnergyObjectCategoryDollarMonthly' + CONVERT (VARCHAR, @count)
                                        + ' =  convert (varchar, convert (int, round (coalesce (ec.Dollars, 0), 0))) '
                                        + ' from [Export].[WS_Measures_StagingAllColumns] he
                                inner join [dbo].[DimPremise] p1 on p1.PremiseId = he.PremiseID
                                left join [#WS_EnergyObject_Cost] ec on ec.PremiseKey = p1.PremiseKey and ec.energyObjectCategory = '''
                                        + @energyObjectCategory + '''';

                SELECT  @progress_msg = 'start   @sqlcmd:' + @sqlcmd;
                EXEC [Export].[WS_Measures_RecordProgress] @runId = @run_id, @component = @component,
                    @text = @progress_msg;

                EXEC (@sqlcmd);

                SELECT  @progress_msg = 'success @sqlcmd:' + @sqlcmd;
                EXEC [Export].[WS_Measures_RecordProgress] @runId = @run_id, @component = @component,
                    @text = @progress_msg;

                SELECT  @sqlcmd = '
                    update  he
                    set EnergyObjectCategoryPercentMonthly' + CONVERT (VARCHAR, @count)
                                    + ' = convert (varchar, convert (int, coalesce (ec.Percentage, 0))) '
                                    + ' from [Export].[WS_Measures_StagingAllColumns] he
                            inner join [dbo].[DimPremise] p1
                            on p1.PremiseId = he.PremiseID
                            left join [#WS_EnergyObject_Cost]  ec
                            on ec.PremiseKey = p1.PremiseKey and ec.energyObjectCategory = ''' + @energyObjectCategory + '''';

                SELECT  @progress_msg = 'start   @sqlcmd:' + @sqlcmd;
                EXEC [Export].[WS_Measures_RecordProgress] @runId = @run_id, @component = @component,
                    @text = @progress_msg;

                EXEC (@sqlcmd);

                SELECT  @progress_msg = 'success @sqlcmd:' + @sqlcmd;
                EXEC [Export].[WS_Measures_RecordProgress] @runId = @run_id, @component = @component,
                    @text = @progress_msg;
        ---------------------------------------------------------------------------------------------------------------
            END;

            SELECT  @progress_msg = 'success @period:' + @period + '; @energyObjectCategory:''' + @energyObjectCategory
                    + '''';
            EXEC [Export].[WS_Measures_RecordProgress] @runId = @run_id, @component = @component,
                @text = @progress_msg;


            SET @count += 1;

            FETCH NEXT FROM [energyObjectCategory_cursor] INTO @energyObjectCategory;

        END;

        CLOSE [energyObjectCategory_cursor];
        DEALLOCATE [energyObjectCategory_cursor];


        SET NOCOUNT OFF;

    END;




GO
