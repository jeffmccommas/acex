﻿

/********************************************************/
/***** RUN ON THE INSIGHTSDW_XXX DATABASE SHARDS ***/
/***** including InsightsDW_Test ************************/
/********************************************************/


/****** Object:  StoredProcedure [Export].[WS_Measures_Fetch_External_Data]    Script Date: 5/3/2017 8:49:49 AM ******/
IF EXISTS ( SELECT  *
            FROM    sys.objects
            WHERE   object_id = OBJECT_ID(N'[Export].[WS_Measures_Fetch_External_Data]')
                    AND type IN ( N'P', N'PC' ) )
    DROP PROCEDURE [Export].[WS_Measures_Fetch_External_Data];
GO
/****** Object:  StoredProcedure [Export].[WS_Measures_Fetch_External_Data]    Script Date: 5/3/2017 8:49:49 AM ******/
SET ANSI_NULLS ON;
GO
SET QUOTED_IDENTIFIER ON;
GO
IF NOT EXISTS ( SELECT  *
                FROM    sys.objects
                WHERE   object_id = OBJECT_ID(N'[Export].[WS_Measures_Fetch_External_Data]')
                        AND type IN ( N'P', N'PC' ) )
    BEGIN
        EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [Export].[WS_Measures_Fetch_External_Data] AS';
    END;
GO




-- =============================================
-- Author:      Philip Victor
-- Create date: 5/3/2017
-- Description: copy external data to local tables
--
-- =============================================
-- =============================================
ALTER PROCEDURE [Export].[WS_Measures_Fetch_External_Data]
    @client_id INT ,
    @run_id UNIQUEIDENTIFIER = NULL
AS
    BEGIN
        SET NOCOUNT ON;


    --
    --  [Export].[WS_ClientAction]
    --  NOTE: Comments is excluded because it causes problems when pulling the data out of the source database
    --  NOTE: we will force the clientID to whatever client id we a
        IF OBJECT_ID('[Export].[WS_ClientAction]') IS NOT NULL
            DROP TABLE [Export].[WS_ClientAction];

        SELECT  [ClientActionID] ,
                @client_id AS [ClientID] ,
                [ActionKey] ,
                [ActionTypeKey] ,
                [Hide] ,
                [Disable] ,
                [CommodityKey] ,
                [DifficultyKey] ,
                [HabitIntervalKey] ,
                [NameKey] ,
                [DescriptionKey] ,
                [AnnualCost] ,
                [UpfrontCost] ,
                [AnnualSavingsEstimate] ,
                [AnnualUsageSavingsEstimate] ,
                [AnnualUsageSavingsUomKey] ,
                [CostVariancePercent] ,
                [PaybackTime] ,
                [ROI] ,
                [RebateAvailable] ,
                [RebateAmount] ,
                [RebateUrl] ,
                [RebateImageKey] ,
                [ImageKey] ,
                [VideoKey] ,
                [DateUpdated] ,
                [ActionPriority] ,
                [SavingsCalcMethod] ,
                [SavingsAmount] ,
                [CostExpression] ,
                [RebateExpression]
        INTO    [Export].[WS_ClientAction]
        FROM    ( SELECT    [ClientActionID] ,
                            [ClientID] ,
                            [ActionKey] ,
                            [ActionTypeKey] ,
                            [Hide] ,
                            [Disable] ,
                            [CommodityKey] ,
                            [DifficultyKey] ,
                            [HabitIntervalKey] ,
                            [NameKey] ,
                            [DescriptionKey] ,
                            [AnnualCost] ,
                            [UpfrontCost] ,
                            [AnnualSavingsEstimate] ,
                            [AnnualUsageSavingsEstimate] ,
                            [AnnualUsageSavingsUomKey] ,
                            [CostVariancePercent] ,
                            [PaybackTime] ,
                            [ROI] ,
                            [RebateAvailable] ,
                            [RebateAmount] ,
                            [RebateUrl] ,
                            [RebateImageKey] ,
                            [ImageKey] ,
                            [VideoKey] ,
                            [DateUpdated] ,
                            [ActionPriority] ,
                            [SavingsCalcMethod] ,
                            [SavingsAmount] ,
                            [CostExpression] ,
                            [RebateExpression] ,
                            ROW_NUMBER() OVER ( PARTITION BY ActionKey ORDER BY ClientId DESC ) AS [rank]
                  FROM      [cm].[ClientAction]
                  WHERE     clientId IN ( 0, @client_id )
                ) AS filtered_set
        WHERE   filtered_set.rank = 1;

    --
    --  [Export].[WS_ClientActionAppliance]
    --
        IF OBJECT_ID('[Export].[WS_ClientActionAppliance]') IS NOT NULL
            DROP TABLE [Export].[WS_ClientActionAppliance];

        SELECT  *
        INTO    [Export].[WS_ClientActionAppliance]
        FROM    [cm].[ClientActionAppliance];

    --
    --  [Export].[WS_ClientEnduseAppliance]
    --
        IF OBJECT_ID('[Export].[WS_ClientEnduseAppliance]') IS NOT NULL
            DROP TABLE [Export].[WS_ClientEnduseAppliance];

        SELECT  *
        INTO    [Export].[WS_ClientEnduseAppliance]
        FROM    [cm].[ClientEnduseAppliance];

    --
    --  [Export].[WS_ClientEnduse]
    --
        IF OBJECT_ID('[Export].[WS_ClientEnduse]') IS NOT NULL
            DROP TABLE [Export].[WS_ClientEnduse];

        SELECT  *
        INTO    [Export].[WS_ClientEnduse]
        FROM    [cm].[ClientEnduse];

    END;

GO
